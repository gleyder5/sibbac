package sibbac.business.comunicaciones.pti.mo.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.dto.asignaciones.TitulosByInformacionAsignacionDTO;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;

@Service
public class AsignacionMovimientosDesglosesBo {

  protected static final Logger LOG = LoggerFactory.getLogger(AsignacionMovimientosDesglosesBo.class);

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  public AsignacionMovimientosDesglosesBo() {
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void asignarMovimientosEnDesglosesScriptDividend(boolean lockBooking, List<Long> listNudesgloses,
      String auditUser) throws SIBBACBusinessException {
    Tmct0bokId bookId;
    boolean imLock;
    Tmct0desgloseclitit dct;
    Tmct0movimientoecc mov;
    Tmct0movimientoeccFidessa movf;

    boolean engancharMovimiento;
    List<Tmct0movimientoecc> listMovs;
    List<Tmct0movimientooperacionnuevaecc> listmovn;
    List<Tmct0movimientoeccFidessa> listmovf;

    for (Long nudesglose : listNudesgloses) {
      imLock = false;
      engancharMovimiento = false;
      dct = dctBo.findById(nudesglose);
      if (dct != null) {
        listmovn = movnBo.findByNudesglose(nudesglose);
        if (listmovn.isEmpty()) {
          engancharMovimiento = true;
        }
        else {
          for (Tmct0movimientooperacionnuevaecc movn : listmovn) {
            if (movn.getTmct0movimientoecc().getCdestadomov() != null
                && !EstadosEccMovimiento.CANCELADO_INT.text
                    .equals(movn.getTmct0movimientoecc().getCdestadomov().trim())) {
              break;
            }
          }
        }
        if (engancharMovimiento) {
          listmovf = movfBo.findAllByNuoperacionprev(dct.getCdoperacionecc());
          if (!listmovf.isEmpty()) {
            movf = this.recuperarMovimientoTitulosDisponibles(listmovf);
            if (movf != null) {
              bookId = new Tmct0bokId(dct.getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct
                  .getTmct0desglosecamara().getTmct0alc().getId().getNuorden());
              try {
                if (lockBooking) {
                  bokBo.bloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), auditUser);
                  imLock = true;
                }
                mov = this.engancharMovimiento(dct.getTmct0desglosecamara().getTmct0alc(),
                    dct.getTmct0desglosecamara(), dct, movf, false, false, auditUser);
                if (mov != null) {
                  listMovs = new ArrayList<Tmct0movimientoecc>();
                  listMovs.add(mov);
                  movBo.escribirTmct0logForMovimientos(dct.getTmct0desglosecamara().getTmct0alc().getId(), listMovs,
                      "Encontrado Movimiento", "Encontrado Movimiento", auditUser);
                }

              }
              catch (LockUserException e) {
                LOG.warn(e.getMessage());
              }
              catch (Exception e) {
                LOG.warn(e.getMessage());
              }
              finally {
                if (imLock) {
                  try {
                    bokBo.desBloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), auditUser);
                  }
                  catch (LockUserException e) {
                    LOG.warn(e.getMessage());
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void asignarMovimientosEnDesglosesScriptDividendByBookingId(Tmct0bokId bookId, List<Long> listNudesgloses,
      String auditUser) throws SIBBACBusinessException {
    if (listNudesgloses != null && !listNudesgloses.isEmpty()) {
      boolean imLock = false;
      try {
        bokBo.bloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), auditUser);
        imLock = true;
        this.asignarMovimientosEnDesglosesScriptDividend(false, listNudesgloses, auditUser);

      }
      catch (LockUserException e) {
        LOG.warn(e.getMessage());
      }
      catch (Exception e) {
        LOG.warn(e.getMessage());
      }
      finally {
        if (imLock) {
          try {
            bokBo.desBloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), auditUser);
          }
          catch (LockUserException e) {
            LOG.warn(e.getMessage());
          }
        }
      }
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void asignarMovimientosEnDesgloses(Tmct0alcId alcId, String auditUser) throws SIBBACBusinessException {
    LOG.debug("[asignarMovimientosEnDesgloses] buscando movimientos ecc fidessa...");
    List<Tmct0movimientoeccFidessa> listMovf = movfBo.findAllByTmct0alcIdAndTitulosDisponibles(alcId);
    if (listMovf != null && !listMovf.isEmpty()) {
      LOG.debug("[asignarMovimientosEnDesgloses] encontrados {} movimientos ecc fidessa...", listMovf.size());
      LOG.debug("[asignarMovimientosEnDesgloses] buscando desglosesclitit de alc {}...", alcId);
      List<Tmct0desgloseclitit> listDesgloses = dctBo.findAllByTmct0alcId(alcId);
      if (listDesgloses != null && !listDesgloses.isEmpty()) {
        LOG.debug("[asignarMovimientosEnDesgloses] encontrados {} desglosesclitit de alc {}...", listDesgloses.size());
        Tmct0movimientoeccFidessa movFidessaAsignacionCorrecta = this.findAsignacionCorrecta(alcId, listDesgloses,
            listMovf);
        if (movFidessaAsignacionCorrecta != null) {
          // TODO CAÑA
          
//          this.engancharMovimiento(alc, dc, dct, movf, restarTitulos, disociarDesglose, auditUser);
        }
        else {

        }
      }
    }
  }

  private Tmct0movimientoeccFidessa findAsignacionCorrecta(Tmct0alcId alcId, List<Tmct0desgloseclitit> listDesgloses,
      List<Tmct0movimientoeccFidessa> listMovf) {
    String key;
    Tmct0movimientoeccFidessa res = null;
    Map<String, List<Tmct0movimientoeccFidessa>> movsByNuoperacionprev = new HashMap<String, List<Tmct0movimientoeccFidessa>>();
    Collection<String> nuoperacionprevMasUnMovimiento = new HashSet<String>();
    Map<String, List<Tmct0movimientoeccFidessa>> movimientosByAsignacion = new HashMap<String, List<Tmct0movimientoeccFidessa>>();
    Map<String, List<Tmct0movimientoeccFidessa>> movimientosByRefinternaAsig = new HashMap<String, List<Tmct0movimientoeccFidessa>>();
    Map<Tmct0desgloseclitit, Tmct0movimientoeccFidessa> mapMovimientosDesglose = new HashMap<Tmct0desgloseclitit, Tmct0movimientoeccFidessa>();
    for (Tmct0movimientoeccFidessa mov : listMovf) {
      if (mov.getCuentaCompensacionDestino() != null && !mov.getCuentaCompensacionDestino().trim().isEmpty()) {
        key = String.format("%s##%s##%s", StringUtils.trim(mov.getCuentaCompensacionDestino()), "", "");
      }
      else {
        key = String.format("%s##%s##%s", "", StringUtils.trim(mov.getMiembroCompensadorDestino()),
            StringUtils.trim(mov.getCdrefasignacion()));
      }
      if (movsByNuoperacionprev.get(mov.getNuoperacionprev().trim()) == null) {
        movsByNuoperacionprev.put(mov.getNuoperacionprev().trim(), new ArrayList<Tmct0movimientoeccFidessa>());
      }
      movsByNuoperacionprev.get(mov.getNuoperacionprev().trim()).add(mov);
      if (movsByNuoperacionprev.get(mov.getNuoperacionprev().trim()).size() > 1) {
        nuoperacionprevMasUnMovimiento.add(mov.getNuoperacionprev().trim());
      }

      if (movimientosByAsignacion.get(key) == null) {
        movsByNuoperacionprev.put(key, new ArrayList<Tmct0movimientoeccFidessa>());
      }
      movimientosByAsignacion.get(key).add(mov);
      if (mov.getCdrefinternaasig() != null && !mov.getCdrefinternaasig().trim().isEmpty()) {
        if (movimientosByRefinternaAsig.get(mov.getCdrefinternaasig().trim()) == null) {
          movimientosByRefinternaAsig.put(mov.getCdrefinternaasig().trim(), new ArrayList<Tmct0movimientoeccFidessa>());
        }
        movimientosByRefinternaAsig.get(mov.getCdrefinternaasig().trim()).add(mov);
      }
    }
    boolean terminado = false;
    if (movimientosByAsignacion.keySet().size() == 1) {
      return listMovf.get(0);
    }
    else {
      if (nuoperacionprevMasUnMovimiento.isEmpty()) {
        return null;
      }
      else {
        Tmct0infocompensacion ic;
        // 1.- igual ic que el dc
        for (Tmct0desgloseclitit dct : listDesgloses) {
          ic = dct.getTmct0desglosecamara().getTmct0infocompensacion();
          if (ic.getCdctacompensacion() != null && !ic.getCdctacompensacion().trim().isEmpty()) {
            key = String.format("%s##%s##%s", StringUtils.trim(ic.getCdctacompensacion()), "", "");
          }
          else {
            key = String.format("%s##%s##%s", "", StringUtils.trim(ic.getCdmiembrocompensador()),
                StringUtils.trim(ic.getCdrefasignacion()));
          }

          List<Tmct0movimientoeccFidessa> listMovsByAsignacion = movimientosByAsignacion.get(key);

          if (listMovsByAsignacion != null && !listMovsByAsignacion.isEmpty()) {
            for (Tmct0movimientoeccFidessa mov : listMovsByAsignacion) {
              if (dct.getCdoperacionecc().trim().equals(mov.getNuoperacionprev().trim())
                  && mov.getImtitulosPendientesAsignar().compareTo(dct.getImtitulos()) >= 0) {
                mapMovimientosDesglose.put(dct, mov);
              }
              else if (dct.getCdoperacionecc().trim().equals(mov.getNuoperacionprev().trim())
                  && mov.getImtitulosPendientesAsignar().compareTo(dct.getImtitulos()) < 0) {
                BigDecimal titulosMismaAsignacion = BigDecimal.ZERO;
                List<Tmct0movimientoeccFidessa> listMovsByNuoperacionprev = movsByNuoperacionprev.get(dct
                    .getCdoperacionecc().trim());
                for (Tmct0movimientoeccFidessa mov2 : listMovsByNuoperacionprev) {
                  if (mov.getCuentaCompensacionDestino() != null
                      && !mov.getCuentaCompensacionDestino().trim().isEmpty()
                      && mov2.getCuentaCompensacionDestino() != null
                      && mov.getCuentaCompensacionDestino().trim().equals(mov2.getCuentaCompensacionDestino().trim())
                      || mov.getMiembroCompensadorDestino() != null
                      && !mov.getMiembroCompensadorDestino().trim().isEmpty() && mov.getCdrefasignacion() != null
                      && mov2.getMiembroCompensadorDestino() != null && mov2.getCdrefasignacion() != null
                      && mov.getMiembroCompensadorDestino().trim().equals(mov2.getMiembroCompensadorDestino().trim())
                      && mov.getCdrefasignacion().trim().equals(mov2.getCdrefasignacion().trim())) {
                    titulosMismaAsignacion = titulosMismaAsignacion.add(mov2.getImtitulosPendientesAsignar());
                  }

                  if (titulosMismaAsignacion.compareTo(dct.getImtitulos()) >= 0) {
                    // disociar
                  }
                  else {
                    break;
                  }
                }
              }
            }

          }
        }
        // 2.- la ref interna asig coincide
        //

        for (Tmct0desgloseclitit dct : listDesgloses) {
          if (movimientosByRefinternaAsig.get(alcId.getNucnfclt().trim()) != null
              && movimientosByRefinternaAsig.get(alcId.getNucnfclt().trim()).size() > 0) {
            List<Tmct0movimientoeccFidessa> listMovsByRefinternaAsig = movsByNuoperacionprev.get(dct
                .getCdoperacionecc().trim());
            if (listMovsByRefinternaAsig != null && !listMovsByRefinternaAsig.isEmpty()) {

            }

          }
        }

        for (String keyFor : movimientosByAsignacion.keySet()) {
          List<Tmct0movimientoeccFidessa> listMovsByAsignacion = movimientosByAsignacion.get(keyFor);
          for (Tmct0desgloseclitit dct : listDesgloses) {
            for (Tmct0movimientoeccFidessa mov : listMovsByAsignacion) {
              if (mov.getNuoperacionprev().trim().equals(dct.getCdoperacionecc().trim())
                  && mov.getImtitulosPendientesAsignar().compareTo(dct.getImtitulos()) >= 0) {
                mapMovimientosDesglose.put(dct, mov);
              }
              else if (mov.getNuoperacionprev().trim().equals(dct.getCdoperacionecc().trim())
                  && nuoperacionprevMasUnMovimiento.contains(dct.getCdoperacionecc().trim())) {
                // TODO HAY QUE DISOCIAR
                List<Tmct0movimientoeccFidessa> listMovsByNuoperacionprev = movsByNuoperacionprev.get(dct
                    .getCdoperacionecc().trim());
                BigDecimal titulosMismaAsignacion = BigDecimal.ZERO;
                for (Tmct0movimientoeccFidessa mov2 : listMovsByNuoperacionprev) {
                  if (mov.getCuentaCompensacionDestino() != null
                      && !mov.getCuentaCompensacionDestino().trim().isEmpty()
                      && mov2.getCuentaCompensacionDestino() != null
                      && mov.getCuentaCompensacionDestino().trim().equals(mov2.getCuentaCompensacionDestino().trim())
                      || mov.getMiembroCompensadorDestino() != null
                      && !mov.getMiembroCompensadorDestino().trim().isEmpty() && mov.getCdrefasignacion() != null
                      && mov2.getMiembroCompensadorDestino() != null && mov2.getCdrefasignacion() != null
                      && mov.getMiembroCompensadorDestino().trim().equals(mov2.getMiembroCompensadorDestino().trim())
                      && mov.getCdrefasignacion().trim().equals(mov2.getCdrefasignacion().trim())) {
                    titulosMismaAsignacion = titulosMismaAsignacion.add(mov2.getImtitulosPendientesAsignar());
                  }
                }

                if (titulosMismaAsignacion.compareTo(dct.getImtitulos()) >= 0) {
                  // disociar
                }
                else {
                  break;
                }
              }
              else {
                // no podemos disociar por tener mas de un codigo de op
                break;
              }
            }
          }
        }
      }
    }
    return res;

  }

  private Tmct0movimientoeccFidessa recuperarMovimientoTitulosDisponibles(List<Tmct0movimientoeccFidessa> listmovf) {
    Tmct0movimientoeccFidessa movf = null;
    BigDecimal titulosDesgloses;
    List<Tmct0movimientoeccFidessa> listVivos = new ArrayList<Tmct0movimientoeccFidessa>();
    for (Tmct0movimientoeccFidessa movfFor : listmovf) {
      if (movfFor.getCdestadomov() != null
          && !movfFor.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)) {
        listVivos.add(movfFor);
      }
    }
    if (!listVivos.isEmpty()) {
      if (listVivos.size() == 1) {
        movf = listVivos.get(0);
      }
      else {
        for (Tmct0movimientoeccFidessa movfFor : listVivos) {
          titulosDesgloses = dctBo.sumImtitulosByCdoperacionecc(movfFor.getCdoperacionecc());
          if (titulosDesgloses == null || titulosDesgloses.compareTo(movfFor.getImtitulos()) < 0) {
            movf = movfFor;
            break;
          }
        }
      }
    }
    listVivos.clear();
    return movf;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void restarTitulosMovimientoFidessa(List<Long> listIdMovimientoFidesa, String auditUser)
      throws SIBBACBusinessException {
    Tmct0movimientoeccFidessa movf;
    BigDecimal titulosDesgloses;
    BigDecimal titulosPendientes;
    for (Long idMovimientoFidessa : listIdMovimientoFidesa) {
      movf = movfBo.findById(idMovimientoFidessa);
      if (movf != null) {
        movnBo.updateIdMovimientoFidessaByCdoperacionecc(idMovimientoFidessa, movf.getCdoperacionecc(), auditUser);
        if (movf.getImtitulosPendientesAsignar().compareTo(BigDecimal.ZERO) > 0) {
          titulosDesgloses = dctBo.sumImtitulosByCdoperacionecc(movf.getCdoperacionecc());
          if (titulosDesgloses != null) {
            titulosPendientes = movf.getImtitulos().subtract(titulosDesgloses);
            if (movf.getImtitulosPendientesAsignar().compareTo(titulosPendientes) != 0) {
              if (titulosPendientes.compareTo(BigDecimal.ZERO) < 0) {
                movf.setImtitulosPendientesAsignar(BigDecimal.ZERO);
              }
              else {
                movf.setImtitulosPendientesAsignar(titulosPendientes);
              }
              movf.setAuditFechaCambio(new Date());
              movf.setAuditUser(auditUser);
              movfBo.save(movf);
            }
          }
        }
      }
    }
  }

  //
  // @Transactional(propagation = Propagation.REQUIRES_NEW, isolation =
  // Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  // public void asignarMovimientosEnDesgloses(Date fcontratacion, String
  // cdisin, char sentido, String camaraCompensacion)
  // throws SIBBACBusinessException {
  // List<Tmct0desglosecamara> dcs;
  // Tmct0infocompensacion ic;
  // List<Tmct0desgloseclitit> dcts;
  // List<Tmct0movimientoeccFidessa> movsf;
  // List<TitulosByInformacionAsignacionDTO> titulosDiponiblesPorAsignacion;
  // TitulosByInformacionAsignacionDTO asignacionAplicar;
  // BigDecimal titulosMovs = BigDecimal.ZERO;
  // BigDecimal titulosDisponibles;
  // BigDecimal titulosAsignar;
  // String miembroCompensador;
  // String refAsignacionExterna;
  // String cuentaCompensacion;
  // List<String[]> listInformacionCompensacionByAlc;
  // List<Tmct0infocompensacion> listInfocompensacionByAlc;
  // List<Tmct0desgloseclitit> listDesgloses;
  // List<Tmct0movimientoeccFidessa> listMovimientosFidessa;
  // LOG.debug("[asignarMovimientosEnDesgloses] inicio.");
  // List<Tmct0alc> alcs =
  // dcBo.findAllAlcsByFcontratacionAndCdisinAndSentidoAndCdestadoasigLt(fcontratacion,
  // cdisin,
  // sentido, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
  // List<String> estadosEccActivos = Arrays.asList(new String[] {
  // EstadosEccMovimiento.FINALIZADO.text,
  // EstadosEccMovimiento.ENVIANDOSE_INT.text,
  // EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text });
  //
  // for (Tmct0alc alc : alcs) {
  // listInformacionCompensacionByAlc =
  // icDao.findAllDistinctInformacionCompensacionByAlcId(alc.getId());
  // titulosMovs =
  // movBo.getSumNutitulosByTmct0alcIdAndInCdestadomov(alc.getId(),
  // estadosEccActivos);
  // titulosAsignar = alc.getNutitliq().subtract(titulosMovs);
  // titulosDiponiblesPorAsignacion = movfBo
  // .findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByAlcIdAndInCestadomov(
  // alc.getId(), estadosEccActivos);
  // listDesgloses = dctBo.findByTmct0alcId(alc.getId());
  // listMovimientosFidessa = movfBo.findAllByAlcIdAndInCestadomov(alc.getId(),
  // estadosEccActivos);
  // if (listInformacionCompensacionByAlc.size() == 1) {
  // listInfocompensacionByAlc =
  // icDao.findAllTmct0infocompensacionByAlcId(alc.getId());
  // ic = listInfocompensacionByAlc.get(0);
  // asignacionAplicar = this.buscarMejorAsignacionDisponible(titulosAsignar,
  // ic, titulosDiponiblesPorAsignacion);
  // if (asignacionAplicar != null) {
  // for (Tmct0desgloseclitit dct : listDesgloses) {
  // List<Tmct0movimientooperacionnuevaecc> listMovsDesglose =
  // movnBo.findByInCdestadomovAndNudesglose(
  // estadosEccActivos, dct.getNudesglose());
  // // si el desglose no tiene ya movimientos
  // if (CollectionUtils.isEmpty(listMovsDesglose)) {
  // Tmct0movimientoeccFidessa movf =
  // this.findMovimientoParaDesglose(asignacionAplicar, dct,
  // listMovimientosFidessa);
  // Tmct0movimientoecc mov = new Tmct0movimientoecc();
  // mov.setTmct0desglosecamara(dct.getTmct0desglosecamara());
  // CloneObjectHelper.copyBasicFields(movf, mov, null);
  // Tmct0movimientooperacionnuevaecc movn = new
  // Tmct0movimientooperacionnuevaecc();
  // movn.setTmct0movimientoecc(mov);
  // CloneObjectHelper.copyBasicFields(movf, movn, null);
  // mov.getTmct0movimientooperacionnuevaeccs().add(movn);
  // }
  // }
  // }
  // else {
  // // partir
  // }
  // }
  // else if (listInformacionCompensacionByAlc.size() > 1) {
  // asignacionAplicar = this.buscarMejorAsignacionDisponible(titulosAsignar,
  // null, titulosDiponiblesPorAsignacion);
  // if (asignacionAplicar != null) {
  //
  // }
  // else {
  // // partir
  // }
  // }
  // else {
  // // un alc sin desgloses de alta y sin ic no se puede dar
  // }
  //
  // dcs = dcBo.findAllByTmct0alcActivosIdAndCdestadoasig(alc.getId());
  // for (Tmct0desglosecamara dc : dcs) {
  // if (dc.getTmct0estadoByCdestadoasig().getIdestado() <
  // EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
  // ic = dc.getTmct0infocompensacion();
  // LOG.debug("[asignarMovimientosEnDesgloses] el dc.id: {} tiene una ic cta:{} miembro: {} ref: {}",
  // dc.getIddesglosecamara(), ic.getCdctacompensacion(),
  // ic.getCdmiembrocompensador(),
  // ic.getCdrefasignacion());
  // titulosMovs =
  // movBo.getSumNutitulosByIddesglosecamaraAndInCdestadomov(dc.getIddesglosecamara(),
  // estadosEccActivos);
  //
  // if (titulosMovs.compareTo(dc.getNutitulos()) <= 0) {
  // titulosAsignar = dc.getNutitulos().subtract(titulosMovs);
  // LOG.debug("[asignarMovimientosEnDesgloses] le faltan movimientos");
  // // TODO hay que mirar si hay en movf titulos disponibles en la misma
  // // cuenta para todos los desgloses
  // titulosDiponiblesPorAsignacion = movfBo
  // .findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByIddesglosecamaraAndInCestadomov(
  // dc.getIddesglosecamara(), estadosEccActivos);
  // if (CollectionUtils.isNotEmpty(titulosDiponiblesPorAsignacion)) {
  // asignacionAplicar = this.buscarMejorAsignacionDisponible(titulosAsignar,
  // ic,
  // titulosDiponiblesPorAsignacion);
  // for (TitulosByInformacionAsignacionDTO titulosByIC :
  // titulosDiponiblesPorAsignacion) {
  // LOG.debug("[asignarMovimientosEnDesgloses] datos de movimientos que cuadran con el dc: {}",
  // titulosByIC);
  // if (titulosByIC.getTitulos().compareTo(titulosAsignar) == 0) {
  // dcts = dctBo.findByIddesglosecamara(dc.getIddesglosecamara());
  // for (Tmct0desgloseclitit dct : dcts) {
  // if (StringUtils.isBlank(titulosByIC.getCuentaCompensacion())) {
  // movsf =
  // movfBo.findAllByNuoperacionprevAndMiembroCompensadorDestinoAndCdRefAsignacion(
  // dct.getNuoperacionprev(), titulosByIC.getMiembroCompensadorExterno(),
  // titulosByIC.getReferenciaAsignacionExterna());
  // }
  // else {
  // movsf =
  // movfBo.findAllByNuoperacionprevAndCuentaCompensacionDestino(dct.getNuoperacionprev(),
  // titulosByIC.getCuentaCompensacion());
  // }
  // boolean enganchado = false;
  // for (Tmct0movimientoeccFidessa movf : movsf) {
  // if (dct.getImtitulos().compareTo(movf.getImtitulosPendientesAsignar()) ==
  // 0) {
  // enganchado = true;
  // this.engancharMovimiento(alc, dc, dct, movf);
  // break;
  // }
  // }
  // if (!enganchado) {
  // for (Tmct0movimientoeccFidessa movf : movsf) {
  // if (dct.getImtitulos().compareTo(movf.getImtitulosPendientesAsignar()) < 0)
  // {
  // enganchado = true;
  // this.engancharMovimiento(alc, dc, dct, movf);
  // break;
  // }
  // }
  // if (!enganchado) {
  // LOG.debug("INCIDENCIA-Titulos en movimiento menor que en desglose...");
  // throw new
  // SIBBACBusinessException("INCIDENCIA-Titulos en movimiento menor que en desglose...");
  //
  // }
  // }
  // }
  // }
  // }
  // }
  // }
  // }
  // }
  // }
  // LOG.debug("[asignarMovimientosEnDesgloses] fin.");
  //
  // }

  private Tmct0movimientoeccFidessa findMovimientoParaDesglose(TitulosByInformacionAsignacionDTO asignacion,
      Tmct0desgloseclitit dct, List<Tmct0movimientoeccFidessa> listMovsf) {
    Tmct0movimientoeccFidessa mov = null;
    Collections.sort(listMovsf, new Comparator<Tmct0movimientoeccFidessa>() {

      @Override
      public int compare(Tmct0movimientoeccFidessa arg0, Tmct0movimientoeccFidessa arg1) {
        return arg0.getImtitulosPendientesAsignar().compareTo(arg1.getImtitulosPendientesAsignar());
      }
    });

    for (Tmct0movimientoeccFidessa tmct0movimientoeccFidessa : listMovsf) {
      if (tmct0movimientoeccFidessa.getNuoperacionprev().trim().equals(dct.getNuoperacionprev().trim())) {
        if (tmct0movimientoeccFidessa.getImtitulosPendientesAsignar().compareTo(dct.getImtitulos()) == 0
            && (asignacion == null || this.isMismaInformacionAsignacion(tmct0movimientoeccFidessa, asignacion))) {
          mov = tmct0movimientoeccFidessa;
          break;
        }
      }
    }
    if (mov == null) {
      for (Tmct0movimientoeccFidessa tmct0movimientoeccFidessa : listMovsf) {
        if (tmct0movimientoeccFidessa.getNuoperacionprev().trim().equals(dct.getNuoperacionprev().trim())) {
          if (tmct0movimientoeccFidessa.getImtitulosPendientesAsignar().compareTo(dct.getImtitulos()) > 0
              && (asignacion == null || this.isMismaInformacionAsignacion(tmct0movimientoeccFidessa, asignacion))) {
            mov = tmct0movimientoeccFidessa;
            break;
          }
        }
      }
    }
    return mov;
  }

  private TitulosByInformacionAsignacionDTO buscarMejorAsignacionDisponible(BigDecimal titulosAsignar,
      Tmct0infocompensacion icDesglose, List<TitulosByInformacionAsignacionDTO> titulosDisponibles) {

    /***
     * 1.- buscamos titulos igual, igual informacion asignacion 2.- buscamos
     * titulos mayores en movimientos igual asignacion 3.- buscamos titulos
     * igual distinta asignacion 4.- partir en alcs
     * */
    TitulosByInformacionAsignacionDTO res = null;
    for (TitulosByInformacionAsignacionDTO titByIc : titulosDisponibles) {
      if (titulosAsignar.compareTo(titByIc.getTitulos()) == 0) {
        if (icDesglose == null || this.isMismaInformacionAsignacion(icDesglose, titByIc)) {
          res = titByIc;
          break;
        }
      }
    }
    if (res == null) {
      Collections.sort(titulosDisponibles, new Comparator<TitulosByInformacionAsignacionDTO>() {

        @Override
        public int compare(TitulosByInformacionAsignacionDTO arg0, TitulosByInformacionAsignacionDTO arg1) {
          // ascendente
          return arg0.getTitulos().compareTo(arg1.getTitulos());
        }
      });
      for (TitulosByInformacionAsignacionDTO titByIc : titulosDisponibles) {
        if (titulosAsignar.compareTo(titByIc.getTitulos()) < 0) {
          if (icDesglose == null || this.isMismaInformacionAsignacion(icDesglose, titByIc)) {
            res = titByIc;
            break;
          }
        }
      }

      // if (res == null) {
      // Collections.sort(titulosDisponibles, new
      // Comparator<TitulosByInformacionAsignacionDTO>() {
      //
      // @Override
      // public int compare(TitulosByInformacionAsignacionDTO arg0,
      // TitulosByInformacionAsignacionDTO arg1) {
      // // descendente
      // return arg0.getTitulos().compareTo(arg1.getTitulos()) * -1;
      // }
      // });
      // BigDecimal titulosAcumulados = BigDecimal.ZERO;
      // for (TitulosByInformacionAsignacionDTO titByIc : titulosDisponibles) {
      // if (titulosAsignar.compareTo(titByIc.getTitulos()) > 0) {
      // if (this.isMismaInformacionAsignacion(icDesglose, titByIc)) {
      // res = titByIc;
      // break;
      // }
      // }
      // }
      // }

    }

    return res;
  }

  private boolean isMismaInformacionAsignacion(Tmct0infocompensacion icDesglose,
      TitulosByInformacionAsignacionDTO titByIc) {
    if (StringUtils.isNotBlank(icDesglose.getCdctacompensacion())
        && StringUtils.isNotBlank(titByIc.getCuentaCompensacion())
        && StringUtils.equals(icDesglose.getCdctacompensacion().trim(), titByIc.getCuentaCompensacion().trim())) {
      return true;
    }
    else if (StringUtils.isNotBlank(icDesglose.getCdmiembrocompensador())
        && StringUtils.isNotBlank(titByIc.getMiembroCompensadorExterno())
        && StringUtils.equals(icDesglose.getCdmiembrocompensador().trim(), titByIc.getMiembroCompensadorExterno()
            .trim()) && StringUtils.isNotBlank(icDesglose.getCdrefasignacion())
        && StringUtils.isNotBlank(titByIc.getReferenciaAsignacionExterna())
        && StringUtils.equals(icDesglose.getCdrefasignacion().trim(), titByIc.getReferenciaAsignacionExterna().trim())) {
      return true;
    }
    else {
      return false;
    }
  }

  private boolean isMismaInformacionAsignacion(Tmct0movimientoeccFidessa movimiento,
      TitulosByInformacionAsignacionDTO titByIc) {
    if (StringUtils.isNotBlank(movimiento.getCuentaCompensacionDestino())
        && StringUtils.isNotBlank(titByIc.getCuentaCompensacion())
        && StringUtils.equals(movimiento.getCuentaCompensacionDestino().trim(), titByIc.getCuentaCompensacion().trim())) {
      return true;
    }
    else if (StringUtils.isNotBlank(movimiento.getMiembroCompensadorDestino())
        && StringUtils.isNotBlank(titByIc.getMiembroCompensadorExterno())
        && StringUtils.equals(movimiento.getMiembroCompensadorDestino().trim(), titByIc.getMiembroCompensadorExterno()
            .trim()) && StringUtils.isNotBlank(movimiento.getCdrefasignacion())
        && StringUtils.isNotBlank(titByIc.getReferenciaAsignacionExterna())
        && StringUtils.equals(movimiento.getCdrefasignacion().trim(), titByIc.getReferenciaAsignacionExterna().trim())) {
      return true;
    }
    else {
      return false;
    }
  }

  private Tmct0movimientoecc engancharMovimiento(Tmct0alc alc, Tmct0desglosecamara dc, Tmct0desgloseclitit dct,
      Tmct0movimientoeccFidessa movf, boolean restarTitulos, boolean disociarDesglose, String auditUser) {

    if (restarTitulos) {
      if (movf.getImtitulosPendientesAsignar().compareTo(dct.getImtitulos()) >= 0) {
        movf.setImtitulosPendientesAsignar(movf.getImtitulosPendientesAsignar().subtract(dct.getImtitulos()));
        movf.setAuditFechaCambio(new Date());
        movf.setAuditUser(auditUser);
        movf = movfBo.save(movf);
      }
      else {
        if (disociarDesglose) {
          Tmct0desgloseclitit dctDisociado = new Tmct0desgloseclitit();
          CloneObjectHelper.copyBasicFields(dct, dctDisociado, null);
          dctDisociado.setTmct0desglosecamara(dc);

          dctBo.repartirCalculos(dct, dctDisociado, movf.getImtitulosPendientesAsignar());
          movf.setImtitulosPendientesAsignar(BigDecimal.ZERO);
          movf.setAuditFechaCambio(new Date());
          movf.setAuditUser(auditUser);
          movf = movfBo.save(movf);

          dctDisociado.setAuditFechaCambio(new Date());
          dctDisociado.setAuditUser(auditUser);
          dctBo.save(dctDisociado);
        }
        else {
          return null;
        }
      }
    }

    Tmct0movimientoecc mov = new Tmct0movimientoecc();
    CloneObjectHelper.copyBasicFields(movf, mov, null);
    Tmct0movimientooperacionnuevaecc mn = new Tmct0movimientooperacionnuevaecc();

    CloneObjectHelper.copyBasicFields(movf, mn, null);
    mn.setIdMovimientoFidessa(movf.getIdMovimientoFidessa());
    mn.setImefectivo(dct.getImefectivo());
    mn.setImtitulos(dct.getImtitulos());
    mn.setNudesglose(dct.getNudesglose());
    mn.setCorretaje(BigDecimal.ZERO);
    mov.setImefectivo(dct.getImefectivo());
    mov.setImtitulos(dct.getImtitulos());
    mov.setFechaLiquidacion(dc.getFeliquidacion());
    mov.setImputacionPropia('N');
    mov.setIsin(alc.getTmct0alo().getCdisin());
    mov.setTmct0desglosecamara(dc);
    mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());

    dct.setCdoperacionecc(movf.getCdoperacionecc());
    dct.setNuoperacionprev(movf.getNuoperacionprev());
    dct.setAuditFechaCambio(new Date());
    dct.setAuditUser(auditUser);
    mov.setAuditUser(auditUser);
    mov = movBo.save(mov);
    mn.setTmct0movimientoecc(mov);
    mn.setAuditUser(auditUser);
    movnBo.save(mn);
    return mov;

  }

}
