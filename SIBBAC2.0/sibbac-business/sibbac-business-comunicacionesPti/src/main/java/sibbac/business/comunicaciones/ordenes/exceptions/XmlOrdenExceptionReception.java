package sibbac.business.comunicaciones.ordenes.exceptions;

/**
 * The Class XmlOrdenExceptionReception.
 */
public class XmlOrdenExceptionReception extends Exception {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2162135735725972147L;

  /** cdbroker. */
  private String cdbroker;

  /** nbooking. */
  private String nbooking;

  /** nucnfclt. */
  private String nucnfclt;

  /** nuorden. */
  private String nuorden;

  /** dsobserv. */
  private String dsobserv;

  /**
   * Instantiates a new exception reception.
   * 
   * @param e the e
   */
  public XmlOrdenExceptionReception(Exception e) {
    super(e);
    setCdbroker(" ");
    setNbooking(" ");
    setNucnfclt(" ");
    setNuorden(" ");
    setDsobserv(e.getMessage());
  }

  public XmlOrdenExceptionReception() {
    setCdbroker(" ");
    setNbooking(" ");
    setNucnfclt(" ");
    setNuorden(" ");
    setDsobserv(" ");
  }

  public XmlOrdenExceptionReception(String e) {
    super(e);
    setCdbroker(" ");
    setNbooking(" ");
    setNucnfclt(" ");
    setNuorden(" ");
    setDsobserv(e);
  }

  public XmlOrdenExceptionReception(String msg, Exception e) {
    super(msg, e);
    setCdbroker(" ");
    setNbooking(" ");
    setNucnfclt(" ");
    setNuorden(" ");
    setDsobserv(msg);
  }

  public XmlOrdenExceptionReception(String nuorden, String nbooking, String cdbroker, String nucnfclt, String dsobserv) {
    super(dsobserv);
    setCdbroker(cdbroker);
    setNbooking(nbooking);
    setNucnfclt(nucnfclt);
    setNuorden(nuorden);
    setDsobserv(dsobserv);
  }

  public XmlOrdenExceptionReception(String nuorden,
                            String nbooking,
                            String cdbroker,
                            String nucnfclt,
                            String dsobserv,
                            Exception e) {
    super(e);
    setCdbroker(cdbroker);
    setNbooking(nbooking);
    setNucnfclt(nucnfclt);
    setNuorden(nuorden);
    setDsobserv(dsobserv);
  }

  public XmlOrdenExceptionReception(Exception e, String nuorden, String nbooking) {
    super(e);
    setCdbroker(" ");
    setNbooking(nbooking);
    setNucnfclt(" ");
    setNuorden(nuorden);
    setDsobserv(e.getMessage());
  }

  /**
   * Gets the cdbroker.
   * 
   * @return the cdbroker
   */
  public String getCdbroker() {
    return cdbroker;
  }

  /**
   * Sets the cdbroker.
   * 
   * @param cdbroker the new cdbroker
   */
  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  /**
   * Gets the nbooking.
   * 
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * Sets the nbooking.
   * 
   * @param nbooking the new nbooking
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * Gets the nucnfclt.
   * 
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * Sets the nucnfclt.
   * 
   * @param nucnfclt the new nucnfclt
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * Gets the nuorden.
   * 
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * Sets the nuorden.
   * 
   * @param nuorden the new nuorden
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * Gets the dsobserv.
   * 
   * @return the dsobserv
   */
  public String getDsobserv() {
    return dsobserv;
  }

  /**
   * Sets the dsobserv.
   * 
   * @param dsobserv the new dsobserv
   */
  public void setDsobserv(String dsobserv) {
    this.dsobserv = dsobserv;
  }
}
