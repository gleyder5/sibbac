package sibbac.business.comunicaciones.pti.cuadreecc.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

/**
 * Class that represents a {@link @link sibbac.business.ECCmessages.database.model.ECCMessageInTI_R2 }. Entity:
 * "ECCMessageInTI_R2".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */

@Table(name = DBConstants.CUADRE_ECC.CUADRE_ECC_TITULARES)
@Entity
@XmlRootElement
// @Audited
public class CuadreEccTitular extends ATable<CuadreEccTitular> implements java.io.Serializable {

  private static final long serialVersionUID = -6715375630277180870L;

  @Column(name = "ESTADO_PROCESADO")
  @XmlAttribute
  private Integer estadoProcesado;

  /**
   * Codigo de Error
   */
  @Column(name = "COD_ERROR", nullable = true, length = 3)
  @XmlAttribute
  private String codError;

  /**
   * Identificacion
   */
  @Column(name = "IDENTIFICACION", nullable = false, length = 40)
  @XmlAttribute
  private String identificacion;

  /**
   * Tipo de Titular
   */
  @Column(name = "TIPO_TITULAR", nullable = false, length = 1)
  @XmlAttribute
  private Character tipoTitular;

  /**
   * Tipo de Identificacion
   */
  @Column(name = "TIPO_IDENTIFICACION", nullable = false, length = 1)
  @XmlAttribute
  private Character tipoIdentificacion;

  /**
   * Indicador Persona Fisica / Juridica
   */
  @Column(name = "IND_PERSONA_FISICA_JURIDICA", nullable = false, length = 1)
  @XmlAttribute
  private Character indPersonaFisicaJuridica;

  /**
   * Indicador de Nacionalidad
   */
  @Column(name = "IND_NACIONALIDAD", nullable = false, length = 1)
  @XmlAttribute
  private Character indNacionalidad;

  /**
   * Pais de Nacionalidad
   */
  @Column(name = "PAIS_NACIONALIDAD", nullable = false, length = 3)
  @XmlAttribute
  private String paisNacionalidad;

  /**
   * REGISTRO R02
   */

  /**
   * Nombre/Razon Social
   */
  @Column(name = "NOMBRE_RAZON_SOCIAL", nullable = false, length = 140)
  @XmlAttribute
  private String nombreRazonSocial;

  /**
   * Primer Apellido
   */
  @Column(name = "PRIMER_APELLIDO", nullable = true, length = 40)
  @XmlAttribute
  private String primerApellido;

  /**
   * Segundo Apellido
   */
  @Column(name = "SEGUNDO_APELLIDO", nullable = true, length = 40)
  @XmlAttribute
  private String segundoApellido;

  /**
   * % de Participacion en Propiedad
   */
  @Column(name = "PORCENTAJE_PROPIEDAD", nullable = false, precision = 5, scale = 2)
  @XmlAttribute
  private BigDecimal porcentajePropiedad;

  /**
   * % de Participacion en Usufructo
   */
  @Column(name = "PORCENTAJE_USUFRUCTO", nullable = false, precision = 5, scale = 2)
  @XmlAttribute
  private BigDecimal porcentajeUsufructo;

  /**
   * Codigo Suscriptor
   */
  @Column(name = "SUSCRIPTOR", nullable = true)
  @XmlAttribute
  private Integer suscriptor;

  /**
   * Relacion con tabla ECCMSGINTIMAIN
   */
  @ManyToOne(optional = false, cascade = { CascadeType.PERSIST
  })
  @JoinColumn(name = "ID_CUADRE_ECC_REFERENCIA", nullable = false)
  // mappedby
  @XmlAttribute
  private CuadreEccReferencia cuadreEccReferencia;

  @Transient
  private List<CuadreEccLog> cuadreEccLog = new ArrayList<CuadreEccLog>();

  // ----------------------------------------------- Bean Constructors

  public CuadreEccTitular() {
    super();
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public String getCodError() {
    return codError;
  }

  public String getIdentificacion() {
    return identificacion;
  }

  public Character getTipoTitular() {
    return tipoTitular;
  }

  public Character getTipoIdentificacion() {
    return tipoIdentificacion;
  }

  public Character getIndPersonaFisicaJuridica() {
    return indPersonaFisicaJuridica;
  }

  public Character getIndNacionalidad() {
    return indNacionalidad;
  }

  public String getPaisNacionalidad() {
    return paisNacionalidad;
  }

  public String getNombreRazonSocial() {
    return nombreRazonSocial;
  }

  public String getPrimerApellido() {
    return primerApellido;
  }

  public String getSegundoApellido() {
    return segundoApellido;
  }

  public BigDecimal getPorcentajePropiedad() {
    return porcentajePropiedad;
  }

  public BigDecimal getPorcentajeUsufructo() {
    return porcentajeUsufructo;
  }

  public Integer getSuscriptor() {
    return suscriptor;
  }

  public CuadreEccReferencia getCuadreEccReferencia() {
    return cuadreEccReferencia;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setCodError(String codError) {
    this.codError = codError;
  }

  public void setIdentificacion(String identificacion) {
    this.identificacion = identificacion;
  }

  public void setTipoTitular(Character tipoTitular) {
    this.tipoTitular = tipoTitular;
  }

  public void setTipoIdentificacion(Character tipoIdentificacion) {
    this.tipoIdentificacion = tipoIdentificacion;
  }

  public void setIndPersonaFisicaJuridica(Character indPersonaFisicaJuridica) {
    this.indPersonaFisicaJuridica = indPersonaFisicaJuridica;
  }

  public void setIndNacionalidad(Character indNacionalidad) {
    this.indNacionalidad = indNacionalidad;
  }

  public void setPaisNacionalidad(String paisNacionalidad) {
    this.paisNacionalidad = paisNacionalidad;
  }

  public void setNombreRazonSocial(String nombreRazonSocial) {
    this.nombreRazonSocial = nombreRazonSocial;
  }

  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }

  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  public void setPorcentajePropiedad(BigDecimal porcentajePropiedad) {
    this.porcentajePropiedad = porcentajePropiedad;
  }

  public void setPorcentajeUsufructo(BigDecimal porcentajeUsufructo) {
    this.porcentajeUsufructo = porcentajeUsufructo;
  }

  public void setSuscriptor(Integer suscriptor) {
    this.suscriptor = suscriptor;
  }

  public void setCuadreEccReferencia(CuadreEccReferencia cuadreEccReferencia) {
    this.cuadreEccReferencia = cuadreEccReferencia;
  }

  public List<CuadreEccLog> getCuadreEccLog() {
    return cuadreEccLog;
  }

  public void setEstadoProcesado(Integer estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setCuadreEccLog(List<CuadreEccLog> cuadreEccLog) {
    this.cuadreEccLog = cuadreEccLog;
  }

  @Override
  public String toString() {
    return "CuadreEccTitular [estadoProcesado=" + estadoProcesado + ", codError=" + codError + ", identificacion="
           + identificacion + ", tipoTitular=" + tipoTitular + ", tipoIdentificacion=" + tipoIdentificacion
           + ", indPersonaFisicaJuridica=" + indPersonaFisicaJuridica + ", indNacionalidad=" + indNacionalidad
           + ", paisNacionalidad=" + paisNacionalidad + ", nombreRazonSocial=" + nombreRazonSocial
           + ", primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + ", porcentajePropiedad="
           + porcentajePropiedad + ", porcentajeUsufructo=" + porcentajeUsufructo + ", suscriptor=" + suscriptor
           + ", cuadreEccReferencia=" + cuadreEccReferencia + "]";
  }

}
