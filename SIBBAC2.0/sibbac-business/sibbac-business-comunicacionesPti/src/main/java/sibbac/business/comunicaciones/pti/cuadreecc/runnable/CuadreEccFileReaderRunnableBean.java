package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.dto.PtiMessageFileReaderDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccFileReaderSubList;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.ControlRevisionTitularesIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionCuadreEccDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccFileReaderRunnableBean")
public class CuadreEccFileReaderRunnableBean extends IRunnableBean<PtiMessageFileReaderDTO> {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccFileReaderRunnableBean.class);

  @Value("${sibbac.cuadreecc.file.reader.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadreecc.file.reader.subthread.pool.size:5}")
  private int threadSize;

  @Value("${sibbac.cuadreecc.file.reader.max.beans.in.transaction:500}")
  private int maxRegInTransaction;

  @Value("${sibbac.cuadreecc.file.reader.maxreg.simple.thread.execution:1000}")
  private int maxRegForSimpleThreadExecution;

  @Value("${sibbac.cuadreecc.file.reader.referencia.titular.aaunknown:AAUNKNOWN}")
  private String refTitularAAUNKNOWN;

  @Autowired
  private CuadreEccFileReaderSubList cuadreEccFileReaderSubList;

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {
    setObserver(observer);
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void run(ObserverProcess observer, List<PtiMessageFileReaderDTO> beans, int order) throws SIBBACBusinessException {
    setObserver(observer);
    LOG.trace("[CuadreEccFileReaderRunnableBean :: run] inicio");
    LOG.trace("[CuadreEccFileReaderRunnableBean :: run] order: {} beans: {}", order, beans);
    // int countProcesados = 0;
    ControlRevisionTitularesIFDTO myObserver = new ControlRevisionTitularesIFDTO();
    myObserver.setIdentificaciones(((ControlRevisionTitularesIFDTO) observer).getIdentificaciones());
    try {
      procesarBeans(beans, ((ControlRevisionTitularesIFDTO) observer).getIdentificaciones(), order);
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException(e);
    } finally {
      observer.sutDownThread(this);
    }
    LOG.trace("[CuadreEccFileReaderRunnableBean :: run] fin");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarBeans(List<PtiMessageFileReaderDTO> beans,
                             Collection<IdentificacionOperacionCuadreEccDTO> operacionesBorradas,
                             int order) throws SIBBACBusinessException {
    LOG.trace("[CuadreEccFileReaderRunnableBean :: procesarBeans] inicio");
    try {
      cuadreEccFileReaderSubList.grabarCuadreEccSibbacReferenciasAndTitulares(beans, operacionesBorradas,
                                                                              refTitularAAUNKNOWN,
                                                                              maxRegForSimpleThreadExecution);
    } catch (SIBBACBusinessException e) {
      int countExceptions = 0;
      LOG.warn("[CuadreEccFileReaderRunnableBean :: procesarBeans] Incidencia con el orden: " + order + " bean list : "
               + beans, e);
      Map<String, List<PtiMessageFileReaderDTO>> agrupadosPorNumeroOperacion = cuadreEccFileReaderSubList.agruparPorNumeroOperacion(beans);
      List<PtiMessageFileReaderDTO> beansAgrupados;
      for (String key : agrupadosPorNumeroOperacion.keySet()) {
        beansAgrupados = agrupadosPorNumeroOperacion.get(key);
        LOG.trace("[CuadreEccFileReaderRunnableBean :: run] pos-exception beans: {}", beansAgrupados);
        try {
          cuadreEccFileReaderSubList.grabarCuadreEccSibbacReferenciasAndTitulares(beansAgrupados, operacionesBorradas,
                                                                                  refTitularAAUNKNOWN,
                                                                                  maxRegForSimpleThreadExecution);
        } catch (SIBBACBusinessException e1) {
          countExceptions++;
          LOG.warn("[CuadreEccFileReaderRunnableBean :: procesarBeans] Incidencia con el orden: " + order
                   + " bean list : " + beansAgrupados, e1);
          if (countExceptions > transactionSize / 10) {
            LOG.warn("[CuadreEccFileReaderRunnableBean :: procesarBeans] demasiados errores en el bloque abortamos el proceso");
            throw new SIBBACBusinessException("Incidencia con el beanlist: " + beansAgrupados, e1);
          }
        }
      }
    }
    LOG.trace("[CuadreEccFileReaderRunnableBean :: procesarBeans] fin");
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<PtiMessageFileReaderDTO> clone() {
    return this;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

}
