package sibbac.business.canones.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.canones.bo.MercadoContratacionBo;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.StreamResult;

@RestController
@RequestMapping("/canones/mercado-contratacion")
public class SIBBACServiceMercadoContratacion {
  
  @Autowired
  private MercadoContratacionBo mercadoContratacionBo;
  
  @RequestMapping(value = "/{id}")
  public void getModeloContratacion(@PathVariable("id") String id, HttpServletResponse res) 
      throws IOException {
    final StreamResult rs;
    
    if(id == null) {
      res.sendError(404, "Se requiere el id del Mercado de Contratación");
      return;
    }
    rs = new HttpStreamResultHelper(res);
    mercadoContratacionBo.findById(id, rs);
  }
  
}
