package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.bo.Tmct0MensajesBo;
import sibbac.business.fase0.database.model.Tmct0Mensajes;
import sibbac.business.fase0.database.model.Tmct0MensajesR00;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.txt.R00;
import sibbac.pti.messages.txt.TXT;

@Service("ptiMessageProcessTxtBo")
public class PtiMessageProcessTxtBo extends PtiMessageProcess<TXT> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessTxtBo.class);

  @Autowired
  private Tmct0MensajesBo txtBo;

  public PtiMessageProcessTxtBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, TXT txt, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    R00 r00;
    List<PTIMessageRecord> r00s = txt.getAll(PTIMessageRecordType.R00);
    Tmct0Mensajes txtDataBase = new Tmct0Mensajes();
    try {
      txtDataBase.setHoraRecepcion(txt.getHeader().getFecha());
    }
    catch (PTIMessageException e) {
      txtDataBase.setHoraRecepcion(new Date());
    }
    if (CollectionUtils.isNotEmpty(r00s)) {
      for (PTIMessageRecord record : r00s) {
        r00 = (R00) record;
        LOG.trace("Receive msg: {}", r00.getTextoInformativo());
        Tmct0MensajesR00 txtR00DataBase = new Tmct0MensajesR00();

        txtR00DataBase.setTexto(r00.getTextoInformativo());
        txtR00DataBase.setTmct0Mensajes(txtDataBase);
        txtDataBase.getTmct0MensajesR00s().add(txtR00DataBase);
      }
      txtBo.save(txtDataBase);
    }
    else {
      LOG.warn("TXT NO TIENE R00'S");
    }
  }
}
