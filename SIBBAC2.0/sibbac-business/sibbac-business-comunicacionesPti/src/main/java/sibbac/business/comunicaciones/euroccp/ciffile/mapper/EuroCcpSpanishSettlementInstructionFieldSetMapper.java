package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpSettlementInstructionsFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCppSpanishSettlementInstruction;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpSpanishSettlementInstructionFieldSetMapper extends WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean>{

  public EuroCcpSpanishSettlementInstructionFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    //Los campos de los registros 450 y 452 son identicos a día de hoy.
    //Si esta situacion cambiase, entonces habría que escribir una enumeracion con los valores pertinentes.
    return EuroCcpSettlementInstructionsFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {
    return new EuroCppSpanishSettlementInstruction();
  }

}
