package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0DesgloseCamaraEnvioRtBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.CriteriosComunicacionTitulares;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.rt.R00;
import sibbac.pti.messages.rt.R01;
import sibbac.pti.messages.rt.RT;
import sibbac.pti.messages.rt.RTControlBlock;

@Service("ptiMessageProcessRtBo")
public class PtiMessageProcessRtBo extends PtiMessageProcess<RT> {
  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessRtBo.class);

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao rtDao;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtBo rtBo;

  @Autowired
  private Tmct0logBo logBo;

  public PtiMessageProcessRtBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, RT rt, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {

    String codigoErrorR01;
    R00 r00;
    R01 r01;
    String criterioComunicacionTitular;
    String identificacionCriterioComunicacionTitular;
    Date fechaNegociacion;
    String miembro;
    String rTitular;
    Character sentido;
    List<Tmct0DesgloseCamaraEnvioRt> desgloseCamaraEnvioRts;
    Tmct0alc alc;
    Tmct0desglosecamara dc;
    boolean rectificacion;
    String msgLog;
    String criterioLog;

    List<PTIMessageRecord> r00s = rt.getAll(PTIMessageRecordType.R00);
    List<PTIMessageRecord> r01s = rt.getAll(PTIMessageRecordType.R01);
    RTControlBlock rtControlBlock = (RTControlBlock) rt.getControlBlock();
    String codigoError = rtControlBlock.getCodigoDeError();
    String descripcionError = rtControlBlock.getTextoDeError();

    boolean error = !"000".equals(codigoError) && !"".equals(codigoError.trim());

    if (CollectionUtils.isNotEmpty(r00s) && CollectionUtils.isNotEmpty(r01s)) {

      for (PTIMessageRecord record : r00s) {
        r00 = (R00) record;
        criterioComunicacionTitular = r00.getCriterioComunicacionTitularidad().trim();
        identificacionCriterioComunicacionTitular = r00.getIdentificacionCriterioComunicacionTitularidad().trim();
        fechaNegociacion = new Date(r00.getFechaNegociacion().getTime());
        miembro = r00.getEntidadComunicadoraRefTitular().trim();
        if (StringUtils.isBlank(r00.getSentido())
            && identificacionCriterioComunicacionTitular.equals(CriteriosComunicacionTitulares.NUMERO_OPERACION_ECC)) {
          sentido = getSentidoFromOpEcc(criterioComunicacionTitular);
        }
        else {
          sentido = cache.getSentidoPtiSentidoSibbac(r00.getSentido());
        }

        for (PTIMessageRecord recordr01 : r01s) {
          r01 = (R01) recordr01;

          codigoErrorR01 = r01.getCodigoDeError();
          rTitular = r01.getReferenciaTitular().trim();
          switch (identificacionCriterioComunicacionTitular) {
            case CriteriosComunicacionTitulares.NUMERO_OPERACION_ECC:
              criterioLog = String.format("Op.ECC: '%s'", criterioComunicacionTitular);
              desgloseCamaraEnvioRts = rtBo.findAllForReceiveRt(rTitular, fechaNegociacion, miembro, cache.getIdPti(),
                  criterioComunicacionTitular, sentido);
              break;
            case CriteriosComunicacionTitulares.NUMERO_ORDEN_MERCADO:
              criterioLog = String.format("Num.Orden.Mkt: '%s'", criterioComunicacionTitular);
              desgloseCamaraEnvioRts = rtBo.findAllForReceiveRt(rTitular, fechaNegociacion, miembro, cache.getIdPti(),
                  criterioComunicacionTitular, sentido);
              break;
            case CriteriosComunicacionTitulares.NUMERO_OPERACION_DCV:
              criterioLog = String.format("Op.DCV: '%s'", criterioComunicacionTitular);
              desgloseCamaraEnvioRts = rtBo.findAllForReceiveRt(rTitular, fechaNegociacion, miembro, cache.getIdPti(),
                  criterioComunicacionTitular, sentido);
              break;
            default:
              criterioLog = "";
              desgloseCamaraEnvioRts = null;
              LOG.error("Identificacion del Criterio de Comunicacion del Titular no configurado: {}",
                  identificacionCriterioComunicacionTitular);
              break;
          }
          if (CollectionUtils.isNotEmpty(desgloseCamaraEnvioRts)) {
            for (Tmct0DesgloseCamaraEnvioRt desgloseCamaraEnvioRt : desgloseCamaraEnvioRts) {
              dc = desgloseCamaraEnvioRt.getDesgloseCamara();
              alc = dc.getTmct0alc();
              rectificacion = dc.getTmct0estadoByCdestadotit().getIdestado() == EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION
                  .getId();

              if (!error) {
                desgloseCamaraEnvioRt.setEstado('A');
                if (rectificacion) {
                  msgLog = String.format("Recepcion RT %s: '%s' %s %s: '%s' %s: '%s' rectificacion aceptado.",
                      "Ref.Tit", rTitular, criterioLog, "F.Trade",
                      FormatDataUtils.convertDateToString(fechaNegociacion), "Miembro", miembro);
                }
                else {

                  msgLog = String
                      .format("Recepcion RT %s: '%s' %s %s: '%s' %s: '%s' aceptado.", "Ref.Tit", rTitular, criterioLog,
                          "F.Trade", FormatDataUtils.convertDateToString(fechaNegociacion), "Miembro", miembro);
                }
              }
              else {
                desgloseCamaraEnvioRt.setEstado('R');

                if (rectificacion) {
                  msgLog = String.format("Recepcion RT %s: '%s' %s %s: '%s' %s: '%s' %s Err: '%s'-'%s'-'%s'.",
                      "Ref.Tit", rTitular, criterioLog, "F.Trade", fechaNegociacion, "Miembro", miembro,
                      "rectificacion rechazado", codigoError, descripcionError, codigoErrorR01);
                }
                else {
                  msgLog = String.format("Recepcion RT %s: '%s' %s %s: '%s' %s: '%s' %s Err: '%s'-'%s'-'%s'.",
                      "Ref.Tit", rTitular, criterioLog, "F.Trade",
                      FormatDataUtils.convertDateToString(fechaNegociacion), "Miembro", miembro, "rechazado",
                      codigoError, descripcionError, codigoErrorR01);
                }
              }
              LOG.debug(msgLog);

              if (alc.getTmct0alo().getTmct0bok().getTmct0ord().getIsscrdiv() != 'S') {
                logBo.insertarRegistro(alc, new Date(), new Date(), "", dc.getTmct0estadoByCdestadotit(), msgLog);
              }

              desgloseCamaraEnvioRt.setAudit_fecha_cambio(new Date());
              desgloseCamaraEnvioRt.setAudit_user("SIBBAC20");
              rtBo.save(desgloseCamaraEnvioRts);

            }// fin for

          }//
          else {
            LOG.warn("INCIDENCIA-RT no encuentra orden que lo ha enviado. {}", rt);
          }
        }
      }
    }
    else {
      LOG.warn("INCIDENCIA-RT no reconocido. {}", rt);
    }

  }

  private Character getSentidoFromOpEcc(String opEcc) {
    Character sentido;
    if (opEcc.substring(opEcc.length() - 2, opEcc.length() - 1).equals("B")) {
      sentido = 'C';
    }
    else {
      sentido = 'V';
    }
    return sentido;
  }
}
