package sibbac.business.comunicaciones.pti.task;

import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.pti.mo.bo.AsignacionMovimientosDesglosesMultithreadBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;

//@DisallowConcurrentExecution
//@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_ASIGNACION_ORDEN_SD, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-22 ? * MON-FRI")
public class TaskAsignacionOrdenSd extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private AsignacionMovimientosDesglosesMultithreadBo asignacionMovimientosDesglosesBo;

  @Override
  public void executeTask() throws Exception {
    asignacionMovimientosDesglosesBo.engancharMovimientosFidessaDesgloses(true, "SIBBAC20");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_ASIGNACION_ORDEN_SD;
  }

}
