package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.util.Date;

public class IdentificacionOperacionCuadreEccDTO extends IdentificacionOperacionDatosMercadoDTO {

  /**
   * 
   */
  private static final long serialVersionUID = -4310019456809647115L;

  private final String numeroOperacion;

  private boolean isSinEcc = false;
  private Date fechaLiquidacion = null;
  private String cuentaLiquidacionIberclear = null;
  private String tipoCuentaIberclear = null;

  public IdentificacionOperacionCuadreEccDTO(String numeroOperacion, Date fechaEjecucion, String cdisin,
      Character sentido, String cdcamara, String cdsegmento, String cdoperacionmkt, String cdmiembromkt,
      String nuordenmkt, String nuexemkt) {
    super(fechaEjecucion, cdisin, sentido, cdcamara, cdsegmento, cdoperacionmkt, cdmiembromkt, nuordenmkt, nuexemkt);
    this.numeroOperacion = numeroOperacion;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((numeroOperacion == null) ? 0 : numeroOperacion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null)
      return false;
    
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    IdentificacionOperacionCuadreEccDTO other = (IdentificacionOperacionCuadreEccDTO) obj;
    if (numeroOperacion == null) {
      if (other.numeroOperacion != null)
        return false;
    }
    else if (!numeroOperacion.equals(other.numeroOperacion))
      return false;
    return true;
  }

  public String getNumeroOperacion() {
    return numeroOperacion;
  }

  public boolean isSinEcc() {
    return isSinEcc;
  }

  public void setSinEcc(boolean isSinEcc) {
    this.isSinEcc = isSinEcc;
  }

  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public String getCuentaLiquidacionIberclear() {
    return cuentaLiquidacionIberclear;
  }

  public String getTipoCuentaIberclear() {
    return tipoCuentaIberclear;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  public void setCuentaLiquidacionIberclear(String cuentaLiquidacionIberclear) {
    this.cuentaLiquidacionIberclear = cuentaLiquidacionIberclear;
  }

  public void setTipoCuentaIberclear(String tipoCuentaIberclear) {
    this.tipoCuentaIberclear = tipoCuentaIberclear;
  }

  @Override
  public String toString() {
    return "IdentificacionOperacionRevisionDesglosesTitularesDTO [numeroOperacion=" + numeroOperacion
        + ", fechaEjecucion=" + fechaEjecucion + ", cdisin=" + cdisin + ", sentido=" + sentido + ", cdcamara="
        + cdcamara + ", cdsegmento=" + cdsegmento + ", cdoperacionmkt=" + cdoperacionmkt + ", cdmiembromkt="
        + cdmiembromkt + ", nuordenmkt=" + nuordenmkt + ", nuexemkt=" + nuexemkt + "]";
  }

}
