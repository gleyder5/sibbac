package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.titulares.runnable.EuroCcpOwnershipReportingGrossExecutionsReceiveRunnable;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpOwnershipReportingGrossExecutionsReceiveDbReader")
public class EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader extends WrapperMultiThreadedDbReader<EuroCcpIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader.class);

  @Value("${sibbac.euroccp.titulares.receive.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.euroccp.titulares.receive.pool.size}")
  private int threadSize;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionsReceiveRunnable")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsReceiveRunnable runnable;

  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsBo euroCcpOwnershipReportingGrossExecutionsBo;

  public EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<EuroCcpIdDTO> getBeanToProcessBlock() {
    return runnable;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    beanIterator = null;
    beanIterator = getListToProcess().iterator();
  }

  private List<EuroCcpIdDTO> getListToProcess() throws SIBBACBusinessException {
    LOG.debug("[EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader :: getListToProcess] inicio.");
    List<EuroCcpIdDTO> ids = euroCcpOwnershipReportingGrossExecutionsBo.findAllIdDesglosecamaraBySinceDateAndEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.RECIBIENDO.getId());

    LOG.debug("[EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader :: getListToProcess] fin ids: {}.", ids.size());
    return ids;
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
