package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.TRAILER)
@Entity
public class EuroCcpTrailer extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  

  @Column(name = "HOLDING_NUMBER", length = 10, nullable = true)
  private String holdingNumber;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Temporal(TemporalType.DATE)
  @Column(name = "REPORT_DATE", nullable = false)
  private Date reportDate;

  @Column(name = "TOTAL_NUMBER_RECORDS", nullable = false)
  private Integer totalNumberRecords;

  @Column(name = "EURO_CCP_BIC_CODE", length = 11, nullable = false)
  private String euroCcpBicCode;

  @Column(name = "DELTA_FILE_SEQUENCE_NUMBER", nullable = true)
  private Integer deltaFileSequenceNumber;

  @Column(name = "FILLER", length = 438, nullable = true)
  private String filler;

  public EuroCcpTrailer() {
  }

  public String getHoldingNumber() {
    return holdingNumber;
  }

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public Date getReportDate() {
    return reportDate;
  }

  public Integer getTotalNumberRecords() {
    return totalNumberRecords;
  }

  public String getEuroCcpBicCode() {
    return euroCcpBicCode;
  }

  public Integer getDeltaFileSequenceNumber() {
    return deltaFileSequenceNumber;
  }

  public String getFiller() {
    return filler;
  }

  public void setHoldingNumber(String holdingNumber) {
    this.holdingNumber = holdingNumber;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setReportDate(Date reportDate) {
    this.reportDate = reportDate;
  }

  public void setTotalNumberRecords(Integer totalNumberRecords) {
    this.totalNumberRecords = totalNumberRecords;
  }

  public void setEuroCcpBicCode(String euroCcpBicCode) {
    this.euroCcpBicCode = euroCcpBicCode;
  }

  public void setDeltaFileSequenceNumber(Integer deltaFileSequenceNumber) {
    this.deltaFileSequenceNumber = deltaFileSequenceNumber;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }
  
  
  

}
