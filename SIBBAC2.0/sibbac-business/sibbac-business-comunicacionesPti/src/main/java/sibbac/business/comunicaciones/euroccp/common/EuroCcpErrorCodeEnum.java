package sibbac.business.comunicaciones.euroccp.common;


/**
00 No error</br>
01  Invalid date, ex ref, MIC, client combination</br>
03  Invalid account</br>
04  Unknown or Incorrect Owner reference</br>
05  Number of shares too large</br>
06  Incorrect number of shares in ISIN</br>
07  Unable to process</br>
08  Execution is not a delivery</br>
09  Invalid H/R indicator</br>
10  File footer check failed (Trailer failure)</br>
11  Originator id from footer invalid</br>
12  Creation date should be equal to processing date</br>
13  Creation time exceeds the deadline</br>
14  Number of records incorrect</br>
15  Invalid data error in file name</br>
16  Wrong file sequence number</br>
17  Unsettled trades</br>
18  No correction possible as ISD+5 Passes</br>
98  Invalid data error (invalid character in numeric)</br>
99  General error code</br>
 * */

public enum EuroCcpErrorCodeEnum {

  NO_ERROR("00"),
  INVALID_DATE_REF_MIC_CLIENT_COMBINATION("01"),
  INVALID_ACCOUNT("03"),
  UNKNOWN_OWNER_REFERENCE("04"),
  NUMBER_OF_SHARES_TOO_LARGE("05"),
  INCORRECT_NUMBER_OF_SHARES_ISISN("06"),
  UNABLE_TO_PROCESS("07"),
  EXECUTION_NOT_DELIVERY("08"),
  INVALID_HR_INDICATOR("09"),
  FOOTER_FALIURE("10"),
  ORIGINATOR_FOOTER_INVALID("11"),
  CREATION_DATE_DISTINT_PROCESING_DATE("12"),
  CREATION_TIME_EXCEEDS_DEADLINE("13"),
  NUMBER_RECORDS_INCORRECT("14"),
  INVALID_DATA_ERROR_FILE_NAME("15"),
  WRONG_FILE_SEQUENCE_NUMBER("16"),
  UNSETTLED_TRADES("17"),
  NO_CORRECTION_POSSIBLE_ISD_PASSES("18"),
  INVALID_CHARACTER_IN_NUMERIC("98"),
  GENERAL_ERROR("99");
  

  public final String text;

  private EuroCcpErrorCodeEnum(String text) {
    this.text = text;
  }

}
