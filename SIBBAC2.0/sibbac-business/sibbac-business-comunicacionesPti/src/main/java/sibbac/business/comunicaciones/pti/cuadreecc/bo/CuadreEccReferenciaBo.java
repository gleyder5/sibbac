package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.CodigoErrorValidacionCuadreEcc;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccReferenciaDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccTitularDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionCuadreEccDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccLog;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccTitular;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ti.R00;
import sibbac.pti.messages.ti.R01;
import sibbac.pti.messages.ti.R02;
import sibbac.pti.messages.ti.R03;
import sibbac.pti.messages.ti.TI;

@Service
public class CuadreEccReferenciaBo extends AbstractBo<CuadreEccReferencia, Long, CuadreEccReferenciaDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccReferenciaBo.class);

  // @Value("")
  // private String AAUNKNOWN;

  private static final String AUTID_USER_ARREGLAR_TITULOS_REFERENCIAS = "CECC_ART_R";

  @Autowired
  private CuadreEccTitularDao cuadreEccTitularDao;

  @Autowired
  private CuadreEccTitularBo cuadreEccTitularBo;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Integer deleteById(Long id) {
    cuadreEccTitularBo.deleteByIdCuadreEccReferencia(id);
    cuadreEccLogDao.deleteByIdCuadreEccReferencia(id);
    return dao.deleteById(id);
  }

  /*********************************************************************** SIMPLE QUERYS ***********************************************************************************/
  public boolean existsECCMessageInTIByReferenciatitularAndFechaejecucionAndFumerooperacion(Date fechaejecucion,
      String numerooperacion, Character sentido, String referenciatitular) {

    Integer count = dao.countByfechaEjeAndNumOpAndSentidoAndRefTitular(fechaejecucion, numerooperacion, sentido,
        referenciatitular);
    return count != null && count > 0;

  }

  public CuadreEccReferencia findByFechaejecucionAndNumerooperacionAndSentidoAndReferenciatitular(Date fechaejecucion,
      String numerooperacion, Character sentido, String referenciatitular) {

    return dao.findByfechaEjeAndNumOpAndSentidoAndRefTitular(fechaejecucion, numerooperacion, sentido,
        referenciatitular);

  }

  public CuadreEccReferencia findByfechaEjeAndNumOpAndSentidoAndRefTitularAndNumValoresImpNominalPendienteDesglose(
      Date fechaEje, String numOp, Character sentido, String refTitular,
      BigDecimal numValoresImpNominalPendienteDesglose) {
    return dao.findByfechaEjeAndNumOpAndSentidoAndRefTitularAndNumValoresImpNominalPendienteDesglose(fechaEje, numOp,
        sentido, refTitular, numValoresImpNominalPendienteDesglose);
  }

  public CuadreEccReferencia findByfechaEjeAndNumOpAndSentidoAndNumValoresImpNominalPendienteDesglose(Date fechaEje,
      String numOp, Character sentido, BigDecimal numValoresImpNominalPendienteDesglose) {
    return dao.findByfechaEjeAndNumOpAndSentidoAndNumValoresImpNominalPendienteDesglose(fechaEje, numOp, sentido,
        numValoresImpNominalPendienteDesglose);
  }

  public List<CuadreEccReferencia> findByNuordenAndNbookingAndNucnfcltJoinWithReferenciaTitular(String nuorden,
      String nbooking, String nucnfclt) {
    return dao.findByNuordenAndNbookingAndNucnfcltJoinWithReferenciaTitular(nuorden, nbooking, nucnfclt);
  }

  public List<CuadreEccReferencia> getByNumerooperacionesAndFeoperacAndSentidoAndCdestadotit(Date fechaEjecucion,
      List<String> numerooperacion, Character sentido, int cdestadoTitGt) {
    return dao
        .findByNumOpesAndFeoperacAndSentidoAndCdestadotit(fechaEjecucion, numerooperacion, sentido, cdestadoTitGt);
  }

  public List<CuadreEccReferencia> findByNuordenAndNbookingAndNucnfcltAndCdestadotitLt(String nuorden, String nbooking,
      String nucnfclt, Integer cdestadotit, Pageable page) {
    return dao.findByNuordenAndNbookingAndNucnfcltAndCdestadotitLt(nuorden, nbooking, nucnfclt, cdestadotit, page);
  }

  public CuadreEccReferencia findById(final Long id) {
    return dao.findById(id);
  }

  public List<CuadreEccReferencia> findAllEccMessgeInTiCompletados() {
    return dao.findByEstado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
  }

  public List<CuadreEccIdDTO> findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiq(Integer estadoProcesado, Date fechaLiq,
      Pageable page) {
    return dao.findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiq(estadoProcesado, fechaLiq, page);
  }

  public List<CuadreEccIdDTO> findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte(Integer estadoProcesado,
      Date fechaLte, int maxRegs) {
    LOG.trace("[CuadreEccReferenciaBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte] inicio");
    Pageable page = new PageRequest(0, maxRegs);
    List<CuadreEccIdDTO> res = new ArrayList<CuadreEccIdDTO>();
    List<CuadreEccIdDTO> resAux;
    LOG.trace("[CuadreEccReferenciaBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte] buscando estado {}",
        estadoProcesado);
    List<Date> dates = dao.findAllDistinctFechaLiqByEstadoProcesado(estadoProcesado);
    if (CollectionUtils.isNotEmpty(dates)) {
      Date now = new Date();
      for (Date date : dates) {
        if (date.compareTo(now) <= 0) {
          LOG.debug("[CuadreEccReferenciaBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte] date: {} ", date);
          resAux = findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiq(estadoProcesado, date, page);
          LOG.debug(
              "[CuadreEccReferenciaBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte] date: {}, found: {} ",
              date, resAux.size());
          if (CollectionUtils.isNotEmpty(resAux)) {
            res.addAll(resAux);
          }

          if (res.size() >= maxRegs) {
            break;
          }
        }
      }
    }
    LOG.trace("[CuadreEccReferenciaBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte] fin");
    return res;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
  public void deleteAllMessgeInTi(List<CuadreEccReferencia> tis) {

    Collection<CuadreEccTitular> titulares;
    for (CuadreEccReferencia ti : tis) {
      titulares = cuadreEccTitularDao.findByCuadreEccReferencia(ti);
      if (!CollectionUtils.isEmpty(titulares)) {
        cuadreEccTitularDao.delete(titulares);
      }
    }

    dao.delete(tis);

  }

  public Integer deleteByFechaejecucionAndNumerooperacionAndSentido(Date fechaEjecucion, String numerooperacion,
      Character sentido) {
    cuadreEccTitularBo.deleteByFechaejecucionAndNumerooperacionAndSentido(fechaEjecucion, numerooperacion, sentido);

    return dao.deleteByFechaEjeAndNumOpAndSentido(fechaEjecucion, numerooperacion, sentido);
  }

  public List<Long> findAllIdsByFechaEjeAndNumOpAndSentido(Date fechaEje, String numOp1, Character sentido) {
    return dao.findAllIdsByFechaEjeAndNumOpAndSentido(fechaEje, numOp1, sentido);
  }

  public List<Long> findAllIdsByIdCuadreEccEjecucion(long idCuadreEccEjecucion) {
    return dao.findAllIdsByIdCuadreEccEjecucion(idCuadreEccEjecucion);
  }

  public List<CuadreEccReferencia> findAllByFechaEjeAndNumOpAndSentido(Date fechaEje, String numOp1, Character sentido) {
    return dao.findAllByFechaEjeAndNumOpAndSentido(fechaEje, numOp1, sentido);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Integer deleteByFechaejecucionAndNumerooperacionAndSentidoNewTransaction(Date fechaEjecucion,
      String numerooperacion, Character sentido) {
    return deleteByFechaejecucionAndNumerooperacionAndSentido(fechaEjecucion, numerooperacion, sentido);
  }

  public List<CuadreEccIdDTO> findAllCuadreEccReferenciaValidacionIdDTO(int maxRegs) {
    return findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiqLte(
        EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES.getId(),
        new Date(), maxRegs);
  }

  public Integer updateEstadoProcesadoByIdCuadreEccEjecucion(Integer estadoProcesado, Long idCuadreEccEjecucion) {
    return dao.updateEstadoProcesadoByIdCuadreEccEjecucion(estadoProcesado, new Date(), idCuadreEccEjecucion);
  }

  public Integer updateEstadoProcesadoByIdCuadreEccEjecucionAndNoTocadas(Integer estadoProcesado,
      Long idCuadreEccEjecucion) {
    return dao.updateEstadoProcesadoByIdCuadreEccEjecucionAndEstadoProcesadoGte(estadoProcesado, new Date(),
        idCuadreEccEjecucion,
        EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC.getId());
  }

  /*********************************************************************** SIMPLE QUERYS ***********************************************************************************/

  /*********************************************************************** BUSSINES METHODS ********************************************************************************/

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public CuadreEccReferencia grabarCuadreEccReferencia(TI ti, IdentificacionOperacionCuadreEccDTO id,
      Long idCuadreEccEjecucion, String refTitularAUnknown) throws SIBBACBusinessException {
    CuadreEccReferencia eccMsg = null;
    try {

      List<PTIMessageRecord> tiR00s = ti.getAll(PTIMessageRecordType.R00);
      String referenciatitular = null;
      if (CollectionUtils.isNotEmpty(tiR00s)) {
        R00 r00 = (R00) tiR00s.get(0);
        referenciatitular = r00.getReferenciaTitular();
      }
      if (StringUtils.isEmpty(referenciatitular) || id.getFechaEjecucion() == null
          || StringUtils.isEmpty(id.getNumeroOperacion())) {
        LOG.trace(
            "[prepararValoresYGuardar]: incomplete information -> referenciatitular {} - fechaejecucion {} - numerooperacion {}",
            referenciatitular, id.getFechaEjecucion(), id.getNumeroOperacion());
      }
      else {
        LOG.trace(
            "[prepararValoresYGuardar]: processing -> referenciatitular {} - fechaejecucion {} - numerooperacion {}",
            referenciatitular, id.getFechaEjecucion(), id.getNumeroOperacion());

        LOG.trace("[prepararValoresYGuardar] .... saving record");

        eccMsg = guardaValores(ti, id, idCuadreEccEjecucion, refTitularAUnknown);

        LOG.trace("[prepararValoresYGuardar]Almacenada la información de TI: {}", ti.toPTIFormat());

        // }
      }
    }
    catch (PTIMessageException e) {
      LOG.warn("INCIDENCIA- TI {}", ti, e);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("INCIDENCIA- TI {}" + ti, e);
    }
    return eccMsg;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public CuadreEccReferencia guardaValores(TI ti, IdentificacionOperacionCuadreEccDTO idOp, Long idCuadreEjecucion,
      String refTitularAUnknown) throws SIBBACBusinessException {
    try {
      boolean error = false;
      List<PTIMessageRecord> tiR00s;
      List<PTIMessageRecord> tiR01s;
      List<PTIMessageRecord> tiR02s;
      List<PTIMessageRecord> tiR03s;
      // List< PTIMessageRecord > tiR04s;

      /**
       * guardo primero los valores del TI impactado por intermediario
       * financiero
       */
      CuadreEccReferencia cuadreEccReferencia = new CuadreEccReferencia();
      cuadreEccReferencia
          .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC
              .getId());
      cuadreEccReferencia.setIdCuadreEccEjecucion(idCuadreEjecucion);
      cuadreEccReferencia.setIdEcc(idOp.getCdcamara());
      cuadreEccReferencia.setCdisin(idOp.getCdisin());
      cuadreEccReferencia.setCdsegmento(idOp.getCdsegmento());
      cuadreEccReferencia.setCdoperacionmkt(idOp.getCdoperacionmkt());
      cuadreEccReferencia.setCdmiembromkt(idOp.getCdmiembromkt());
      cuadreEccReferencia.setNuordenmkt(idOp.getNuordenmkt());
      cuadreEccReferencia.setNuexemkt(idOp.getNuexemkt());
      cuadreEccReferencia.setSentido(idOp.getSentido());
      cuadreEccReferencia.setCuentaLiquidacion(idOp.getCuentaLiquidacionIberclear());
      cuadreEccReferencia.setTipoCuentaLiquidacionIberclear(idOp.getTipoCuentaIberclear());
      // List< ECCMessageInTI_R2 > leccAux = new ArrayList< ECCMessageInTI_R2
      // >();

      tiR00s = ti.getAll(PTIMessageRecordType.R00);
      tiR01s = ti.getAll(PTIMessageRecordType.R01);
      tiR02s = ti.getAll(PTIMessageRecordType.R02);
      tiR03s = ti.getAll(PTIMessageRecordType.R03);
      // List< PTIMessageRecord > tiR04s = ti.getAll( PTIMessageRecordType.R04
      // );
      /*
       * Datos Generales: Referencia Titular
       */

      if (CollectionUtils.isNotEmpty(tiR00s)) {
        R00 r00 = (R00) tiR00s.get(0);
        cuadreEccReferencia.setEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
        cuadreEccReferencia.setFechaEje(r00.getFechaEjecucion());
        cuadreEccReferencia.setFechaLiq(idOp.getFechaLiquidacion());
        cuadreEccReferencia.setEntComunicadora(r00.getEntidadComunicadoraRefTitular());
        cuadreEccReferencia.setRefTitular(r00.getReferenciaTitular());
        cuadreEccReferencia.setRefAdicional(r00.getReferenciaAdicional());
        cuadreEccReferencia.setIndRectificacion(r00.getIndicadorRectificacion());

      }
      /*
       * Datos Domicilio
       */
      if (CollectionUtils.isNotEmpty(tiR01s)) {
        R01 r01 = (R01) tiR01s.get(0);

        cuadreEccReferencia.setDomicilio(r01.getDomicilio());
        cuadreEccReferencia.setPoblacion(r01.getPoblacion());
        cuadreEccReferencia.setCodPostal(r01.getCodigoPostal());
        cuadreEccReferencia.setPaisResidencia(r01.getPaisDeResidencia());
      }
      else if (refTitularAUnknown.equals(cuadreEccReferencia.getRefTitular())) {
        cuadreEccReferencia.setDomicilio("");
        cuadreEccReferencia.setPoblacion("");
        cuadreEccReferencia.setCodPostal("");
        cuadreEccReferencia.setPaisResidencia("");
      }
      else {
        error = true;
      }

      /*
       * Modo Titular/Operación
       */
      if (CollectionUtils.isNotEmpty(tiR03s) && !error) {
        R03 r03 = (R03) tiR03s.get(0);

        cuadreEccReferencia.setIdEcc(idOp.getCdcamara());
        cuadreEccReferencia.setSegmentoEcc(r03.getSegmentoECC());
        cuadreEccReferencia.setNumOp(r03.getNumeroOperacion());
        cuadreEccReferencia.setIndCotizacion(r03.getIndicadorCotizacion());
        cuadreEccReferencia.setNumValoresImpNominal(r03.getNumValoresImporteNominal());
        cuadreEccReferencia.setNumValoresImpNominalPendienteDesglose(r03.getNumValoresImporteNominal());
        cuadreEccReferencia.setImpEfectivo(r03.getImporteEfectivo());
        cuadreEccReferencia.setFallidoNumValoresImpNominal(r03.getNumValoresImporteNominalFallido());
        cuadreEccReferencia.setCanonTeorico(r03.getCanonTeorico());
        cuadreEccReferencia.setCanonAplicado(r03.getCanonAplicado());
      }
      else {
        error = true;
      }

      /*
       * Datos Titularidad Los R02 están limitados a 25 Cotitulares por registro
       * TI
       */

      if (CollectionUtils.isNotEmpty(tiR02s) && !error) {
        R02 r02;
        CuadreEccTitular cuadreEccTitular;
        for (int i = 0; i < tiR02s.size(); i++) {
          r02 = (R02) tiR02s.get(i);
          cuadreEccTitular = new CuadreEccTitular();
          cuadreEccTitular
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC
                  .getId());
          cuadreEccTitular.setNombreRazonSocial(r02.getNombreRazonSocial());
          cuadreEccTitular.setPrimerApellido(r02.getPrimerApellido());
          cuadreEccTitular.setSegundoApellido(r02.getSegundoApellido());
          cuadreEccTitular.setIdentificacion(r02.getIdentificacion());
          if (StringUtils.isNotBlank(r02.getTipoDeIdentificacion())) {
            cuadreEccTitular.setTipoIdentificacion(r02.getTipoDeIdentificacion().charAt(0));
          }
          else {
            cuadreEccTitular.setTipoIdentificacion(' ');
            addCuadreEccLog(cuadreEccReferencia, cuadreEccTitular,
                CodigoErrorValidacionCuadreEcc.TITULARES_ERROR_TIPO_IDENTIFICACION.text,
                MessageFormat.format("Tipo Identificacion no valido '{}'", r02.getTipoDeIdentificacion()));

            LOG.warn("[ECCMessageInTIBo :: guardaValores] INCIDENCIA-Tipo Identificacion no valido '{}' TI: {}",
                r02.getTipoDeIdentificacion(), ti);
          }

          if (StringUtils.isNotBlank(r02.getIndicadorPersonaFisicaJuridica())) {
            cuadreEccTitular.setIndPersonaFisicaJuridica(r02.getIndicadorPersonaFisicaJuridica().charAt(0));
          }
          else {
            cuadreEccTitular.setIndPersonaFisicaJuridica(' ');
            addCuadreEccLog(cuadreEccReferencia, cuadreEccTitular,
                CodigoErrorValidacionCuadreEcc.TITULARES_ERROR_TIPO_PERSONA.text,
                MessageFormat.format("Tipo Fisica/Juridica no valido '{}'", r02.getIndicadorPersonaFisicaJuridica()));

            LOG.warn("[ECCMessageInTIBo :: guardaValores] INCIDENCIA-Tipo Fisica/Juridica no valido '{}' TI: {}",
                r02.getIndicadorPersonaFisicaJuridica(), ti);
          }

          cuadreEccTitular.setPaisNacionalidad(r02.getPaisDeNacionalidad());

          if (StringUtils.isNotBlank(r02.getIndicadorDeNacionalidad())) {
            cuadreEccTitular.setIndNacionalidad(r02.getIndicadorDeNacionalidad().charAt(0));
          }
          else {
            cuadreEccTitular.setIndNacionalidad(' ');
            addCuadreEccLog(cuadreEccReferencia, cuadreEccTitular,
                CodigoErrorValidacionCuadreEcc.TITULARES_ERROR_TIPO_NACIONALIDAD.text,
                MessageFormat.format("Tipo Ind. Nacional/Extranjero no valido '{}'", r02.getIndicadorDeNacionalidad()));

            LOG.warn(
                "[ECCMessageInTIBo :: guardaValores] INCIDENCIA-Tipo Ind. Nacional/Extranjero no valido '{}' TI: {}",
                r02.getIndicadorDeNacionalidad(), ti);
          }
          if (StringUtils.isNotBlank(r02.getTipoDeTitular())) {
            cuadreEccTitular.setTipoTitular(r02.getTipoDeTitular().charAt(0));
          }
          else {
            cuadreEccTitular.setTipoTitular(' ');
            addCuadreEccLog(cuadreEccReferencia, cuadreEccTitular,
                CodigoErrorValidacionCuadreEcc.TITULARES_ERROR_TIPO_TITULAR.text,
                MessageFormat.format("Tipo Tipo Titular no valido '{}'", r02.getTipoDeTitular()));

            LOG.warn("[ECCMessageInTIBo :: guardaValores] INCIDENCIA-Tipo Tipo Titular no valido '{}' TI: {}",
                r02.getTipoDeTitular(), ti);
          }
          cuadreEccTitular.setPorcentajePropiedad(r02.getPctDeParticipacionEnPropiedad());
          cuadreEccTitular.setPorcentajeUsufructo(r02.getPctDeParticipacionEnUsufructo());

          cuadreEccTitular.setCodError(r02.getCodigoDeError());
          cuadreEccReferencia.addTitulares(cuadreEccTitular);
        }
      }
      else if (!refTitularAUnknown.equals(cuadreEccReferencia.getRefTitular())) {
        error = true;
      }

      if (!error) {

        cuadreEccReferencia = save(cuadreEccReferencia);

        LOG.debug("[ECCMessageInTIBo :: guardaValores] save: {}" + cuadreEccReferencia);
        try {
          LOG.trace("[guardaValores]Almacenado el ECCMessageInTI ({}) del \"TI\": {}", cuadreEccReferencia.getId(),
              ti.toPTIFormat());
        }
        catch (PTIMessageException e) {
          LOG.trace(e.getMessage(), e);
          // Nothing
        }
        if (CollectionUtils.isNotEmpty(cuadreEccReferencia.getTitulares())) {
          for (CuadreEccTitular titular : cuadreEccReferencia.getTitulares()) {
            for (CuadreEccLog cuadreEccLog : titular.getCuadreEccLog()) {
              if (cuadreEccLog.getCuadreEccTitular() != null) {
                cuadreEccLog.setIdCuadreEccTitular(titular.getId());
              }
            }
            cuadreEccLogDao.save(titular.getCuadreEccLog());
          }
        }
        if (CollectionUtils.isNotEmpty(cuadreEccReferencia.getCuadreEccLog())) {
          for (CuadreEccLog cuadreEccLog : cuadreEccReferencia.getCuadreEccLog()) {

            if (cuadreEccLog.getCuadreEccReferencia() != null) {
              cuadreEccLog.setIdCuadreEccReferencia(cuadreEccReferencia.getId());
            }
          }
          cuadreEccLogDao.save(cuadreEccReferencia.getCuadreEccLog());
        }
      }
      return cuadreEccReferencia;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("INCIDENCIA- TI" + ti, e);
    }
  }

  private void addCuadreEccLog(CuadreEccReferencia referencia, CuadreEccTitular titular, String codError, String msg) {
    if (titular != null) {
      titular.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULARES_ERRONEOS.getId());
      CuadreEccLog log = new CuadreEccLog();
      log.setCuadreEccTitular(titular);
      log.setCodError(codError);
      log.setMsg(msg);
      log.setAuditDate(new Date());
      log.setEstado(0);
      titular.getCuadreEccLog().add(log);
    }
    if (referencia != null) {
      referencia.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULARES_ERRONEOS.getId());
      CuadreEccLog log = new CuadreEccLog();
      log.setCuadreEccReferencia(referencia);
      log.setCodError(codError);
      log.setMsg(msg);
      log.setAuditDate(new Date());
      log.setEstado(0);
      referencia.getCuadreEccLog().add(log);
    }
  }

  /**
   * @param beans
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void validarCuadreEccReferencias(List<CuadreEccIdDTO> beans) {
    LOG.trace("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] inicio");
    CuadreEccReferencia cer;
    List<CuadreEccSibbac> cuadresEccSibbac;
    List<String> errores;
    List<String> msgErrores;
    BigDecimal nutitulos;
    boolean errorRefTitular = false;
    int updates;
    for (CuadreEccIdDTO id : beans) {

      LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] procesando bean: {}", id);
      cer = findById(id.getId());
      if (cer != null) {
        cer.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_REPARTO_DESGLOSES_USUARIO
            .getId());
        LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] borrando cuadre ecclogs by IdCuadreEccReferencia...");
        updates = cuadreEccLogDao.deleteByIdCuadreEccReferencia(id.getId());
        LOG.debug(
            "[CuadreEccReferenciaBo :: validarCuadreEccReferencias] borrados {} cuadre ecclogs de IdCuadreEccReferencia == {}.",
            updates, id.getId());
        errores = new ArrayList<String>();
        msgErrores = new ArrayList<String>();
        LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] procesando referencia: {}", cer);

        LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] buscando cuadre ecc sibbac by sentido, numop, fecha eje, ref titular, num valores...");
        cuadresEccSibbac = cuadreEccSibbacBo.findBySentidoAndNumOpAndFeoperacAndRefTitularAndNumTitulosDesPdte(
            cer.getSentido(), cer.getNumOp(), cer.getFechaEje(), cer.getRefTitular(), cer.getNumValoresImpNominal());

        LOG.debug(
            "[CuadreEccReferenciaBo :: validarCuadreEccReferencias] encontrados {} cuadre ecc sibbac by sentido, numop, fecha eje, ref titular, num valores.",
            cuadresEccSibbac.size());

        this.borrarCuadreEccLogsByCuadresEccSibbac(cuadresEccSibbac);

        if (cuadresEccSibbac.size() == 1) {
          errores.add("CECCRF0000");// Referencia OK
          msgErrores.add("Referencia Ok.");
        }
        else if (cuadresEccSibbac.size() > 1) {
          errores.add("CECCRF0001");// error titulos
          msgErrores.add("Numero Titulos Erroneo.");
        }
        else {
          LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] buscando cuadre ecc sibbac by sentido, numop, fecha eje, num valores...");
          cuadresEccSibbac = cuadreEccSibbacBo.findBySentidoAndNumOpAndFeoperacAndNumTitulosDesPdte(cer.getSentido(),
              cer.getNumOp(), cer.getFechaEje(), cer.getNumValoresImpNominal());
          LOG.debug(
              "[CuadreEccReferenciaBo :: validarCuadreEccReferencias] encontrados {} cuadre ecc sibbac by sentido, numop, fecha eje, num valores.",
              cuadresEccSibbac.size());
          this.borrarCuadreEccLogsByCuadresEccSibbac(cuadresEccSibbac);

          if (cuadresEccSibbac.size() == 1) {
            errores.add("CECCRF0004");// error referencia titular
            msgErrores.add("Titulos Correctos/Referencia Titular Erronea");
          }
          else if (cuadresEccSibbac.size() > 1) {
            errores.add("CECCRF0005");// error referencia titular
            msgErrores.add("Numero Titulos Erroneo/Referencia Titular Erronea");
          }
          else {
            LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] buscando cuadre ecc sibbac by sentido, numop, fecha eje, ref titular...");
            cuadresEccSibbac = cuadreEccSibbacBo.findBySentidoAndNumOpAndFeoperacAndRefTitular(cer.getSentido(),
                cer.getNumOp(), cer.getFechaEje(), cer.getRefTitular());

            LOG.debug(
                "[CuadreEccReferenciaBo :: validarCuadreEccReferencias] encontrados {} cuadre ecc sibbac by sentido, numop, fecha eje, ref titular.",
                cuadresEccSibbac.size());
            this.borrarCuadreEccLogsByCuadresEccSibbac(cuadresEccSibbac);

          }

          if (CollectionUtils.isNotEmpty(cuadresEccSibbac)) {
            nutitulos = BigDecimal.ZERO;
            for (CuadreEccSibbac cuadreSibbac : cuadresEccSibbac) {
              nutitulos = nutitulos.add(cuadreSibbac.getNumTitulosDesSibbac());
            }

            if (nutitulos.compareTo(cer.getNumValoresImpNominal()) != 0) {
              errores.add("CECCRF0001");// error titulos
              msgErrores.add("Numero Titulos Erroneo.");
            }
          }
          else {
            LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] buscando cuadre ecc sibbac by sentido, numop, fecha eje...");
            cuadresEccSibbac = cuadreEccSibbacBo.findBySentidoAndNumOpAndFeoperac(cer.getSentido(), cer.getNumOp(),
                cer.getFechaEje());
            LOG.debug(
                "[CuadreEccReferenciaBo :: validarCuadreEccReferencias] encontrados {} cuadre ecc sibbac by sentido, numop, fecha eje.",
                cuadresEccSibbac.size());
            this.borrarCuadreEccLogsByCuadresEccSibbac(cuadresEccSibbac);
          }
        }

        if (CollectionUtils.isNotEmpty(cuadresEccSibbac)) {
          nutitulos = BigDecimal.ZERO;
          errorRefTitular = false;
          for (CuadreEccSibbac cuadreSibbac : cuadresEccSibbac) {
            nutitulos = nutitulos.add(cuadreSibbac.getNumTitulosDesSibbac());

            if (cer.getRefTitular() != null && cuadreSibbac.getRefTitular() != null
                && !cer.getRefTitular().trim().equals(cuadreSibbac.getRefTitular().trim())) {
              errorRefTitular = true;
            }
          }

          if (nutitulos.compareTo(cer.getNumValoresImpNominal()) != 0) {
            errores.add("CECCRF0001");// error titulos
            msgErrores.add("Numero Titulos Erroneo.");
          }

          if (errorRefTitular) {
            errores.add("CECCRF0002");// error referencia titular
            msgErrores.add("Referencia Titular Erronea");
          }
        }
        else {
          errores.add("CECCRF0003");// codigo op ecc no encontrado
          msgErrores.add("Codigo Operacion No Econtrado");
          cer.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC
              .getId());
        }
        grabarLogsFinal(cer, cuadresEccSibbac, errores, msgErrores);

        cer.setAuditDate(new Date());
        cuadreEccLogDao.save(cer.getCuadreEccLog());
        LOG.debug(
            "[CuadreEccReferenciaBo :: validarCuadreEccReferencias] actualizando estado cuadre ecc ejecucion {} a {}",
            cer.getIdCuadreEccEjecucion(), cer.getEstadoProcesado());
        if (cer.getEstadoProcesado() != EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
            .getId()) {
          try {
            cuadreEccEjecucionBo.updateEstadoProcesadoById(cer.getIdCuadreEccEjecucion(), cer.getEstadoProcesado(),
                "CECCREV");
          }
          catch (Exception e) {
            LOG.warn(
                "[CuadreEccReferenciaBo :: validarCuadreEccReferencias]INCIDENCIA-No se ha podido actualizar estado de cuadre ecc ejecucion IdEjecucicion:{} - {}.",
                cer.getIdCuadreEccEjecucion(), cer, e);
          }
        }
        LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] actualizando estado ref {} a {}", cer,
            cer.getEstadoProcesado());
        save(cer);
      }
    }
    LOG.trace("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] fin");
  }

  private void borrarCuadreEccLogsByCuadresEccSibbac(List<CuadreEccSibbac> cuadresEccSibbac) {
    int updates;
    if (CollectionUtils.isNotEmpty(cuadresEccSibbac)) {
      LOG.debug("[CuadreEccReferenciaBo :: borrarCuadreEccLogsByCuadresEccSibbac] borrando cuadre ecclogs by IdCuadreEccSibbac...");
      updates = 0;
      for (CuadreEccSibbac cesibbac : cuadresEccSibbac) {
        updates += cuadreEccLogDao.deleteByIdCuadreEccSibbac(cesibbac.getId());
      }
      LOG.debug(
          "[CuadreEccReferenciaBo :: borrarCuadreEccLogsByCuadresEccSibbac] borrados {} cuadre ecclogs by IdCuadreEccSibbac.",
          updates);
    }
  }

  /**
   * @param cer
   * @param desgloses
   * @param errores
   * @param msgErrores
   */
  private void grabarLogsFinal(CuadreEccReferencia cer, List<CuadreEccSibbac> desgloses, List<String> errores,
      List<String> msgErrores) {

    if (CollectionUtils.isNotEmpty(desgloses)) {
      for (CuadreEccSibbac desglose : desgloses) {
        grabarLogsFinal(cer, desglose, errores, msgErrores);
      }
    }
    else {
      grabarLogsFinal(cer, (CuadreEccSibbac) null, errores, msgErrores);
    }
  }

  /**
   * @param cer
   * @param desglose
   * @param errores
   * @param msgErrores
   */
  private void grabarLogsFinal(CuadreEccReferencia cer, CuadreEccSibbac desglose, List<String> errores,
      List<String> msgErrores) {
    if (CollectionUtils.isNotEmpty(errores)) {
      int index = 0;

      for (String codigoError : errores) {
        String msg = msgErrores.get(index++);
        CuadreEccLog log = new CuadreEccLog();
        log.setEstado(0);
        log.setAuditDate(new Date());
        log.setAuditUser("CECCREV");
        log.setCodError(codigoError);
        log.setMsg(msg);
        log.setIdCuadreEccReferencia(cer.getId());
        log.setIdCuadreEccEjecucion(cer.getIdCuadreEccEjecucion());
        if (desglose != null) {
          log.setIdCuadreEccSibbac(desglose.getId());
        }
        LOG.debug("[CuadreEccReferenciaBo :: validarCuadreEccReferencias] codigo err : {}-{}", log.getCodError(),
            log.getMsg());
        cuadreEccLogDao.save(log);

      }

    }
    else {
      CuadreEccLog log = new CuadreEccLog();
      log.setEstado(0);
      log.setAuditDate(new Date());
      log.setAuditUser("CECCREV");
      log.setCodError("CECCRF0000");
      log.setIdCuadreEccReferencia(cer.getId());
      log.setNumOp(cer.getNumOp());
      log.setFechaEje(cer.getFechaEje());
      log.setSentido(cer.getSentido());
      if (desglose != null) {
        log.setIdCuadreEccSibbac(desglose.getId());
      }
      cuadreEccLogDao.save(log);

    }

  }

  /**
   * @param beans
   * @param entComunicadoraSYSTEM
   * @param entComunicadoraAAUNKNOWN
   * @param reftitulaAAUNKNOWN
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void arreglarReferenciasTitularFicheroEcc(List<CuadreEccIdDTO> beans, String entComunicadoraSYSTEM,
      String entComunicadoraAAUNKNOWN, String reftitulaAAUNKNOWN) throws SIBBACBusinessException {
    LOG.trace("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] inicio");
    CuadreEccEjecucion eje;
    for (CuadreEccIdDTO cuadreEccIdDTO : beans) {
      LOG.trace("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] procesando bean {}", cuadreEccIdDTO);
      eje = cuadreEccEjecucionBo.findById(cuadreEccIdDTO.getId());
      if (eje != null) {
        LOG.trace("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] procesando eje {}", eje);
        try {
          arreglarReferenciasTitularFicheroEcc(eje, entComunicadoraSYSTEM, entComunicadoraAAUNKNOWN, reftitulaAAUNKNOWN);
        }
        catch (Exception e) {
          throw new SIBBACBusinessException("INCIDENCIA- eje:" + eje, e);
        }
      }
    }
    LOG.trace("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] final");
  }

  /**
   * @param eje
   * @param entComunicadoraSYSTEM
   * @param entComunicadoraAAUNKNOWN
   * @param reftitulaAAUNKNOWN
   */
  private void arreglarReferenciasTitularFicheroEcc(CuadreEccEjecucion eje, String entComunicadoraSYSTEM,
      String entComunicadoraAAUNKNOWN, String reftitulaAAUNKNOWN) {
    LOG.trace("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] inicio");
    LOG.debug("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] eje num_op: {} titulos {}",
        eje.getNumOp(), eje.getValoresImpNominal());
    Long minId = dao.getMinIdActivoByIdCuadreEccEjecucion(eje.getId());
    if (minId != null) {

      CuadreEccReferencia minReferencia = dao.findById(minId);
      LOG.debug("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] min ref num_op: {} titulos {}",
          minReferencia.getNumOp(), minReferencia.getNumValoresImpNominal());
      if (minReferencia.getNumValoresImpNominal().compareTo(eje.getValoresImpNominal()) == 0
          && minReferencia.getEntComunicadora().equals(entComunicadoraSYSTEM)) {
        LOG.debug(
            "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] la primera referencia de titular es un {}",
            entComunicadoraSYSTEM);
        BigDecimal sumRestoReferencias = dao.getSumNumvaloresImpNominalActivoByIdCuadreEccEjecucionAndDistinctId(
            eje.getId(), minReferencia.getId());
        if (sumRestoReferencias == null) {
          sumRestoReferencias = BigDecimal.ZERO;
        }

        LOG.debug("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] sum resto referencias {}",
            sumRestoReferencias);
        if (sumRestoReferencias.compareTo(BigDecimal.ZERO) > 0) {
          if (sumRestoReferencias.compareTo(minReferencia.getNumValoresImpNominal()) == 0) {
            LOG.debug(
                "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] titulos coinciden doy de baja la referencia {}",
                minReferencia);
            minReferencia.setIndRectificacion("B");
            minReferencia.setAuditDate(new Date());
            minReferencia.setAuditUser(AUTID_USER_ARREGLAR_TITULOS_REFERENCIAS);

          }
          else {
            minReferencia
                .setNumValoresImpNominal(minReferencia.getNumValoresImpNominal().subtract(sumRestoReferencias));
            minReferencia.setNumValoresImpNominalPendienteDesglose(minReferencia.getNumValoresImpNominal());
            LOG.debug(
                "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] titulos no coinciden resto titulos la referencia {}",
                minReferencia);
            if (minReferencia.getNumValoresImpNominal().compareTo(BigDecimal.ZERO) == 0) {
              LOG.debug("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] damos de baja la ref {}",
                  minReferencia);
              minReferencia.setIndRectificacion("B");
              minReferencia.setAuditDate(new Date());
              minReferencia.setAuditUser(AUTID_USER_ARREGLAR_TITULOS_REFERENCIAS);

            }

            BigDecimal sumEfectivo = dao.getSumImpEfectivoActivoByIdCuadreEccEjecucion(eje.getId());

            LOG.debug(
                "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] eje.efectivo: {} ref.sum.efectivo: {}",
                eje.getEfectivo(), sumEfectivo);

            if (eje.getEfectivo().compareTo(sumEfectivo) < 0) {
              LOG.debug(
                  "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] antes de restar min.ref.efectivo: {}",
                  minReferencia.getImpEfectivo());
              BigDecimal resta = eje.getEfectivo().subtract(sumEfectivo);
              minReferencia.setImpEfectivo(minReferencia.getImpEfectivo().add(resta));
              LOG.debug(
                  "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] despues de restar min.ref.efectivo: {}",
                  minReferencia.getImpEfectivo());
            }

          }
          minReferencia = save(minReferencia);
        }
      }
      else {
        LOG.debug(
            "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] entComunicadora: {} tit.eje: {} tit.min.ref: {}",
            minReferencia.getEntComunicadora(), eje.getValoresImpNominal(), minReferencia.getNumValoresImpNominal());
      }
    }
    BigDecimal sumReferencias = dao.getSumNumvaloresImpNominalActivoByIdCuadreEccEjecucion(eje.getId());
    if (sumReferencias == null) {
      sumReferencias = BigDecimal.ZERO;
    }
    if (sumReferencias.compareTo(eje.getValoresImpNominal()) < 0) {
      LOG.debug(
          "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] titulos eje {} > titulos referencias{} - {}",
          eje.getValoresImpNominal(), sumReferencias, eje);
      BigDecimal sumEfectivo = dao.getSumImpEfectivoActivoByIdCuadreEccEjecucion(eje.getId());
      if (sumEfectivo == null) {
        sumEfectivo = BigDecimal.ZERO;
      }
      CuadreEccReferencia cuadreEccReferencia = new CuadreEccReferencia();
      cuadreEccReferencia.setIdCuadreEccEjecucion(eje.getId());
      cuadreEccReferencia
          .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC
              .getId());

      cuadreEccReferencia.setIdEcc(eje.getIdEcc());
      cuadreEccReferencia.setCdisin(eje.getIsin());
      cuadreEccReferencia.setCdsegmento(eje.getSegmentoNeg());
      cuadreEccReferencia.setCdoperacionmkt(eje.getCodOpMercado());
      cuadreEccReferencia.setCdmiembromkt(eje.getMiembroMercadoNeg());
      cuadreEccReferencia.setNuordenmkt(eje.getNumOrdenMercado());
      cuadreEccReferencia.setNuexemkt(eje.getNumEjeMercado());
      cuadreEccReferencia.setSentido(eje.getSentido());
      cuadreEccReferencia.setCuentaLiquidacion(eje.getCuentaLiquidacionMiembro());
      cuadreEccReferencia.setTipoCuentaLiquidacionIberclear(eje.getTipoCuentaLiquidacionMiembro());

      cuadreEccReferencia.setEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
      cuadreEccReferencia.setFechaEje(eje.getFechaNeg());
      cuadreEccReferencia.setFechaLiq(eje.getFechaLiq());
      cuadreEccReferencia.setEntComunicadora(entComunicadoraAAUNKNOWN);
      cuadreEccReferencia.setRefTitular(reftitulaAAUNKNOWN);
      cuadreEccReferencia.setRefAdicional("");
      cuadreEccReferencia.setIndRectificacion("X");

      cuadreEccReferencia.setDomicilio("");
      cuadreEccReferencia.setPoblacion("");
      cuadreEccReferencia.setCodPostal("");
      cuadreEccReferencia.setPaisResidencia("");

      cuadreEccReferencia.setSegmentoEcc(eje.getSegmentoEcc());
      if (StringUtils.isBlank(eje.getNumOp())) {
        cuadreEccReferencia.setNumOp(eje.getNumOpDcv());
      }
      else {
        cuadreEccReferencia.setNumOp(eje.getNumOp());
      }
      cuadreEccReferencia.setIndCotizacion(eje.getIndCotizacion() + "");
      cuadreEccReferencia.setNumValoresImpNominal(eje.getValoresImpNominal().subtract(sumReferencias));
      cuadreEccReferencia.setNumValoresImpNominalPendienteDesglose(cuadreEccReferencia.getNumValoresImpNominal());
      cuadreEccReferencia.setImpEfectivo(eje.getEfectivo().subtract(sumEfectivo));
      cuadreEccReferencia.setFallidoNumValoresImpNominal(BigDecimal.ZERO);
      cuadreEccReferencia.setCanonTeorico(BigDecimal.ZERO);
      cuadreEccReferencia.setCanonAplicado(BigDecimal.ZERO);
      cuadreEccReferencia.setAuditUser(AUTID_USER_ARREGLAR_TITULOS_REFERENCIAS);
      cuadreEccReferencia.setAuditDate(new Date());
      save(cuadreEccReferencia);
      LOG.debug(
          "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] grabada nueva referencia titulos {}  - {}",
          cuadreEccReferencia.getNumValoresImpNominal(), cuadreEccReferencia);

    }
    else {
      LOG.debug(
          "[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] titulos eje {} == titulos referencias: {} - {}",
          eje.getValoresImpNominal(), sumReferencias, eje);
    }
    eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId());
    eje.setAuditDate(new Date());
    eje.setAuditUser(AUTID_USER_ARREGLAR_TITULOS_REFERENCIAS);
    cuadreEccEjecucionBo.save(eje);
    LOG.trace("[CuadreEccReferenciaBo :: arreglarReferenciasTitularFicheroEcc] final");
  }
  /*********************************************************************** BUSSINES METHODS ********************************************************************************/
} // ECCMessageInTIBo
