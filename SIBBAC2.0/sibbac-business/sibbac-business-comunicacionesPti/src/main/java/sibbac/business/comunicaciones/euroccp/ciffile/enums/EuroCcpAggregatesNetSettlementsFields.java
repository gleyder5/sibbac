package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpAggregatesNetSettlementsFields implements WrapperFileReaderFieldEnumInterface {

  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  EXCHANGE_CODE_TRADE("exchangeCodeTrade", 4, 0, WrapperFileReaderFieldType.STRING),
  SYMBOL("symbol", 6, 0, WrapperFileReaderFieldType.STRING),
  ISIN_CODE("isinCode", 12, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_DATE("transactionDate", 8, 0, WrapperFileReaderFieldType.DATE),
  SETTLEMENT_DATE("settlementDate", 8, 0, WrapperFileReaderFieldType.DATE),
  TRANSACTION_ORIGIN("transactionOrigin", 4, 0, WrapperFileReaderFieldType.STRING),
  TRADE_GROUP_IDENTIFICATION("tradeGroupIdentification", 1, 0, WrapperFileReaderFieldType.CHAR),
  DEPOT_ID("depotId", 6, 0, WrapperFileReaderFieldType.STRING),
  SETTLEMENT_INSTRUCTION_REFERENCE("settlementInstructionReference", 9, 0, WrapperFileReaderFieldType.STRING),
  RECEIVE_CODE("receiveCode", 3, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_QUANTITY_TOTAL_BUY("transactionQuantityTotalBuy", 12, 2, WrapperFileReaderFieldType.NUMERIC),
  DELIVER_CODE("deliverCode", 3, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_QUANTITY_TOTAL_SELL("transactionQuantityTotalSell", 12, 2, WrapperFileReaderFieldType.NUMERIC),
  RECEIVE_DELIVER_CODE_NET("transactionOrigin", 3, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_QUANTITY_TOTAL_NET("transactionQuantityTotalNet", 12, 2, WrapperFileReaderFieldType.NUMERIC),
  AVERAGE_PRICE("averagePrice", 18, 7, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_AMOUNT_TOTAL_BUY("settlementAmountTotalBuy", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_AMOUNT_TOTAL_BUY_DC("settlementAmountTotalBuyDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  SETTLEMENT_AMOUNT_TOTAL_SELL("settlementAmountTotalSell", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_AMOUNT_TOTAL_SELL_DC("settlementAmountTotalSellDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  SETTLEMENT_AMOUNT_TOTAL_NET("settlementAmountTotalNet", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_AMOUNT_TOTAL_NET_DC("settlementAmountTotalNetDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  PLACE_SAFEKEEPING("placeSafekeeping", 11, 0, WrapperFileReaderFieldType.STRING),
  PLACE_SETTLEMENT("placeSettlement", 11, 0, WrapperFileReaderFieldType.STRING),
  BUYER_SELLER_CONTEXT("buyerSellerContext", 8, 0, WrapperFileReaderFieldType.STRING),
  BUYER_SELLER_CODE("buyerSellerCode", 11, 0, WrapperFileReaderFieldType.STRING),
  BUYER_SELLER_ACCOUNT_CODE("buyerSellerAccountCode", 12, 0, WrapperFileReaderFieldType.STRING),
  REC_DEL_AGENT_CONTEXT("recDelAgentContext", 8, 0, WrapperFileReaderFieldType.STRING),
  REC_DEL_AGENT_CODE("recDelAgentCode", 11, 0, WrapperFileReaderFieldType.STRING),
  REC_DEL_AGENT_ACCOUNT_CODE("recDelAgentAccountCode", 12, 0, WrapperFileReaderFieldType.STRING),
  FILLER("filler", 200, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpAggregatesNetSettlementsFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
