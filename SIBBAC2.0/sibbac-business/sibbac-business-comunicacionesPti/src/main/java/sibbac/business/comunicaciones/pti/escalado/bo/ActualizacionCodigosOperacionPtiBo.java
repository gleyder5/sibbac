package sibbac.business.comunicaciones.pti.escalado.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.traspasos.bo.TraspasosEccBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;

@Service
public class ActualizacionCodigosOperacionPtiBo {

  private static final Logger LOG = LoggerFactory.getLogger(ActualizacionCodigosOperacionPtiBo.class);

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private TraspasosEccBo traspasosBo;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Value("${sibbac.escalado.asignacion.cta.pdte.usuario}")
  private String listCtasPdteUsuario;

  @Value("${sibbac.escalado.asignacion.cta.imputada.propia}")
  private String listCtasInputadaPropia;

  public ActualizacionCodigosOperacionPtiBo() {
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public CallableResultDTO actualizarCodigosOperacionOrdenes(Tmct0bokId bookId, String auditUser)
      throws SIBBACBusinessException {
    LOG.debug("[actualizarCodigosOperacionOrdenes] inicio");

    CallableResultDTO res = new CallableResultDTO();
    List<Tmct0desgloseclitit> listDcts = dctBo.findAllByBookingIdAndSinTitulosDiponiblesAnotacionForDesglose(bookId);

    this.actualizarCodigosOperacionOrdenes(bookId, listDcts, auditUser);

    // despues de cargar los codigos de op teemos que ver si debe volver a
    // enviarse o tiene que cargar el movimiento
    LOG.debug("[actualizarCodigosOperacionOrdenes] fin");
    return res;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void actualizarCodigosOperacionOrdenes(Tmct0bokId bookId, Tmct0alc alc, String auditUser)
      throws SIBBACBusinessException {

    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    List<Tmct0desgloseclitit> listDcts;
    for (Tmct0desglosecamara dc : listDcs) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        listDcts = dc.getTmct0desgloseclitits();
        this.actualizarCodigosOperacionOrdenes(bookId, listDcts, auditUser);
      }
    }

  }

  private void actualizarCodigosOperacionOrdenes(Tmct0bokId bookId, List<Tmct0desgloseclitit> listDcts, String auditUser)
      throws SIBBACBusinessException {
    Tmct0desglosecamara dc;
    List<Tmct0movimientoecc> listMovimientosCreados;
    List<Tmct0desgloseclitit> listDesglosesDisociados = new ArrayList<Tmct0desgloseclitit>();
    String msg = "Creado Movimiento, destino parametrizacion igual que origen";
    if (CollectionUtils.isNotEmpty(listDcts)) {

      for (Tmct0desgloseclitit dct : listDcts) {
        if (dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() < EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
            .getId()) {
          this.actualizarCodigosOperacion(dct, listDesglosesDisociados, auditUser);
        }
      }
    }

    if (CollectionUtils.isNotEmpty(listDesglosesDisociados)) {
      actualizarCodigosOperacionOrdenes(bookId, listDesglosesDisociados, auditUser);
    }

    List<Tmct0desgloseclitit> todoDesgloses = new ArrayList<Tmct0desgloseclitit>(listDcts);
    todoDesgloses.addAll(listDesglosesDisociados);
    Map<Long, List<Tmct0desgloseclitit>> mapAgrupadoDc = new HashMap<Long, List<Tmct0desgloseclitit>>();
    for (Tmct0desgloseclitit dt : todoDesgloses) {
      if (dt.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() < EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
          .getId()) {
        if (mapAgrupadoDc.get(dt.getTmct0desglosecamara().getIddesglosecamara()) == null) {
          mapAgrupadoDc.put(dt.getTmct0desglosecamara().getIddesglosecamara(), new ArrayList<Tmct0desgloseclitit>());
        }
        mapAgrupadoDc.get(dt.getTmct0desglosecamara().getIddesglosecamara()).add(dt);
      }
    }
    for (List<Tmct0desgloseclitit> values : mapAgrupadoDc.values()) {
      if (values != null && !values.isEmpty()) {
        dc = values.get(0).getTmct0desglosecamara();
        listMovimientosCreados = movBo.crearMovimientosOrigenDestinoIgual(dc, values, auditUser);
        movBo.save(listMovimientosCreados);
        movBo.escribirTmct0logForMovimientos(dc.getTmct0alc().getId(), listMovimientosCreados, msg, msg, auditUser);
      }
    }

    mapAgrupadoDc.clear();
    todoDesgloses.clear();
    listDesglosesDisociados.clear();
  }

  private BigDecimal getTitulosDisponiblesAnotacionSinDesglose(Tmct0anotacionecc an,
      Map<String, BigDecimal> titulosByCdoperacionecc) {
    BigDecimal titulosDesglosados = titulosByCdoperacionecc.get(an.getCdoperacionecc().trim());
    BigDecimal titulosDisponiblesAnotacion = an.getImtitulosdisponibles();
    if (titulosDesglosados != null) {
      titulosDisponiblesAnotacion = titulosDisponiblesAnotacion.subtract(titulosDesglosados);
    }

    return titulosDisponiblesAnotacion;

  }

  private BigDecimal getTitulosAnotacionSinDesglose(Tmct0anotacionecc an,
      Map<String, BigDecimal> titulosByCdoperacionecc) {
    BigDecimal titulosDesglosados = titulosByCdoperacionecc.get(an.getCdoperacionecc().trim());
    BigDecimal titulosDisponiblesAnotacion = an.getNutitulos();
    if (titulosDesglosados != null) {
      titulosDisponiblesAnotacion = titulosDisponiblesAnotacion.subtract(titulosDesglosados);
    }

    return titulosDisponiblesAnotacion;

  }

  private Tmct0desgloseclitit disociarDesgloseParaAnotacion(BigDecimal titulosRealesAnotacion, Tmct0desgloseclitit dct)
      throws SIBBACBusinessException {
    Tmct0desgloseclitit dct2 = null;

    if (dct.getImtitulos().compareTo(titulosRealesAnotacion) > 0) {
      dct2 = new Tmct0desgloseclitit();
      CloneObjectHelper.copyBasicFields(dct, dct2, null);
      dct2.setNudesglose(null);
      dct2.setTmct0desglosecamara(dct.getTmct0desglosecamara());
      dctBo.repartirCalculos(dct, dct2, titulosRealesAnotacion);
      dctBo.save(dct2);
    }
    if (dct != null && dct.getImtitulos().compareTo(BigDecimal.ZERO) <= 0 || dct2 != null
        && dct2.getImtitulos().compareTo(BigDecimal.ZERO) <= 0) {
      throw new SIBBACBusinessException("Titulos del desglose no pueden ser menor que cero.");
    }
    return dct2;
  }

  private Tmct0anotacionecc buscarAnotaciones(Tmct0desgloseclitit dct, List<Tmct0desgloseclitit> listDesglosesDisociados)
      throws SIBBACBusinessException {
    BigDecimal titulosDisponiles;
    Tmct0anotacionecc an = null;
    Tmct0desgloseclitit dct2;
    Map<String, BigDecimal> titulosByCdoperacionecc;
    List<Tmct0anotacionecc> listAnotaciones = anBo.findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(
        dct.getNuoperacioninic(), dct.getCdsentido(), BigDecimal.ZERO);
    if (CollectionUtils.isNotEmpty(listAnotaciones)) {
      titulosByCdoperacionecc = getMapTitulosDesglosadosByCdoperacionecc(listAnotaciones);
      an = this.buscarAnotaciones(dct, listAnotaciones, titulosByCdoperacionecc);
      if (an != null) {
        titulosDisponiles = this.getTitulosDisponiblesAnotacionSinDesglose(an, titulosByCdoperacionecc);
        if (titulosDisponiles.compareTo(dct.getImtitulos()) < 0 && titulosDisponiles.compareTo(BigDecimal.ZERO) > 0) {
          dct2 = this.disociarDesgloseParaAnotacion(titulosDisponiles, dct);
          if (dct2 != null) {
            listDesglosesDisociados.add(dct2);
          }
        }
        else if (titulosDisponiles.compareTo(dct.getImtitulos()) < 0) {
          an = null;
        }
      }
    }

    if (an == null) {
      listAnotaciones = anBo.findAllByNuoperacioninicOrderDescendente(dct.getNuoperacioninic(), dct.getCdsentido(),
          BigDecimal.ZERO);
      if (CollectionUtils.isNotEmpty(listAnotaciones)) {
        titulosByCdoperacionecc = getMapTitulosDesglosadosByCdoperacionecc(listAnotaciones);
        an = this.buscarAnotaciones(dct, listAnotaciones, titulosByCdoperacionecc);
        if (an != null) {
          titulosDisponiles = this.getTitulosAnotacionSinDesglose(an, titulosByCdoperacionecc);
          if (titulosDisponiles.compareTo(dct.getImtitulos()) < 0 && titulosDisponiles.compareTo(BigDecimal.ZERO) > 0) {
            dct2 = this.disociarDesgloseParaAnotacion(titulosDisponiles, dct);
            if (dct2 != null) {
              listDesglosesDisociados.add(dct2);
            }
          }
          else if (titulosDisponiles.compareTo(dct.getImtitulos()) < 0) {
            an = null;
          }
        }
      }

    }

    return an;
  }

  private Map<String, BigDecimal> getMapTitulosDesglosadosByCdoperacionecc(List<Tmct0anotacionecc> listAnotaciones) {
    BigDecimal sumTitulos;
    Map<String, BigDecimal> res = new HashMap<String, BigDecimal>();
    for (Tmct0anotacionecc tmct0anotacionecc : listAnotaciones) {
      sumTitulos = dctBo.sumImtitulosByCdoperacionecc(tmct0anotacionecc.getCdoperacionecc());
      if (sumTitulos == null) {
        sumTitulos = BigDecimal.ZERO;
      }
      res.put(tmct0anotacionecc.getCdoperacionecc().trim(), sumTitulos);
    }
    return res;
  }

  private Tmct0anotacionecc buscarAnotaciones(Tmct0desgloseclitit dct, List<Tmct0anotacionecc> listAnotaciones,
      Map<String, BigDecimal> titulosByCdoperacionecc) {
    BigDecimal sumTitulos;

    if (CollectionUtils.isNotEmpty(listAnotaciones)) {
      // TITULOS COINCIDEN
      for (Tmct0anotacionecc an : listAnotaciones) {

        sumTitulos = titulosByCdoperacionecc.get(an.getCdoperacionecc().trim());

        if (sumTitulos.add(dct.getImtitulos()).compareTo(an.getImtitulosdisponibles()) == 0) {
          return an;
        }
      }

      // TITULOS DESGLOSES MENOR QUE TITULOS AN
      for (Tmct0anotacionecc an : listAnotaciones) {

        sumTitulos = titulosByCdoperacionecc.get(an.getCdoperacionecc().trim());
        if (sumTitulos.add(dct.getImtitulos()).compareTo(an.getImtitulosdisponibles()) < 0) {
          return an;
        }
      }

      // QUEDAN TITULOS DISPONIBLES
      for (Tmct0anotacionecc an : listAnotaciones) {

        sumTitulos = titulosByCdoperacionecc.get(an.getCdoperacionecc().trim());
        if (an.getImtitulosdisponibles().subtract(sumTitulos).compareTo(BigDecimal.ZERO) > 0) {
          return an;
        }
      }

    }

    return null;
  }

  private void actualizarCodigosOperacion(Tmct0desgloseclitit dct, List<Tmct0desgloseclitit> listDesglosesDisociados,
      String auditUser) throws SIBBACBusinessException {

    // voy a empezar el proceso asumiendo que solo esta este desglose para cada
    // anotacion, en la practica se sabe que no es asi
    LOG.debug("[actualizarCodigosOperacion] inicio");

    // String nuoperacionprev = dct.getNuoperacionprev();
    // String nuoperacioninic = dct.getNuoperacioninic();

    if (StringUtils.isBlank(dct.getCdoperacionecc())) {
      throw new SIBBACBusinessException("Los codigos de operacion ecc deben estar informados para este proceso.");
    }

    // cargamos la informacion de compensacion en el desglose
    if (StringUtils.isBlank(dct.getCuentaCompensacionDestino())
        && StringUtils.isBlank(dct.getCodigoReferenciaAsignacion())) {
      this.cargarInformacionCompensacionInicial(dct, auditUser);
    }

    Tmct0anotacionecc an = this.buscarAnotaciones(dct, listDesglosesDisociados);

    if (an != null) {
      dct.setAnotacion(an);
      List<String> ctasPdteusuario = Arrays.asList(listCtasPdteUsuario.split(","));
      if (ctasPdteusuario.contains(an.getTmct0CuentasDeCompensacion().getCdCodigo().trim())) {
        this.actualizarCodigosParaCuentaDefault(dct, an, auditUser);
      }
      else {
        this.actualizarCodigosParaCuentaNoDefault(dct, an, auditUser);
      }
    }
    else {
      List<Tmct0movimientooperacionnuevaecc> listMovn = movnBo.findByNudesglose(dct.getNudesglose());
      if (CollectionUtils.isNotEmpty(listMovn)) {
        for (Tmct0movimientooperacionnuevaecc movn : listMovn) {
          if (movn.getTmct0movimientoecc().getCdestadomov() != null
              && EstadosEccMovimiento.CANCELADO_INT.text.equals(movn.getTmct0movimientoecc().getCdestadomov())) {
            movn.getTmct0movimientoecc()
                .setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
            movn.getTmct0movimientoecc().setAuditFechaCambio(new Date());
            movn.getTmct0movimientoecc().setAuditUser(auditUser);
          }
        }
      }
      LOG.warn(
          "[actualizarCodigosOperacion] no se ha encontrado anotacion con titulos disponibles desglose: {} op: {}",
          dct.getNudesglose(), dct.getCdoperacionecc());
    }

    LOG.debug("[actualizarCodigosOperacion] fin");
  }

  private void actualizarCodigosParaCuentaDefault(Tmct0desgloseclitit dct, Tmct0anotacionecc an, String auditUser)
      throws SIBBACBusinessException {
    String msg = String.format("Actualizado codigos de op desglose '%s' op.ecc.: '%s' op.ecc.prev.: '%s' cta: '%s'",
        dct.getNudesglose(), an.getCdoperacionecc(), an.getNuoperacionprev(), an.getTmct0CuentasDeCompensacion()
            .getCdCodigo());
    logBo
        .insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct.getTmct0desglosecamara()
            .getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc().getId().getNucnfclt(),
            msg, dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(), auditUser);

    dct.setCdoperacionecc(an.getCdoperacionecc());
    dct.setNuoperacionprev(an.getNuoperacionprev());

    dct.setCuentaCompensacionDestino(an.getTmct0CuentasDeCompensacion().getCdCodigo());
    dct.setMiembroCompensadorDestino("");
    dct.setCodigoReferenciaAsignacion("");

    dct.setAuditFechaCambio(new Date());
    dct.setAuditUser(auditUser);
    this.calcularEstadoParaReenvio(dct, auditUser);

    List<Tmct0movimientoeccFidessa> listMovsf = movfBo.findAllByCdoperacionecc(an.getCdoperacionecc());
    if (CollectionUtils.isNotEmpty(listMovsf)) {
      for (Tmct0movimientoeccFidessa movf : listMovsf) {
        if (movf.getImtitulosPendientesAsignar().compareTo(BigDecimal.ZERO) > 0) {
          movf.setImtitulosPendientesAsignar(BigDecimal.ZERO);
          movf.setAuditFechaCambio(new Date());
          movf.setAuditUser(auditUser);
        }
      }
      movfBo.save(listMovsf);
    }
    dctBo.save(dct);
  }

  private void actualizarCodigosParaCuentaNoDefault(Tmct0desgloseclitit dct, Tmct0anotacionecc an, String auditUser)
      throws SIBBACBusinessException {
    if (an.getImtitulosdisponibles().compareTo(BigDecimal.ZERO) < 0) {
      dct.setCdoperacionecc(an.getCdoperacionecc());
      dct.setNuoperacionprev(an.getCdoperacionecc());
      String msg = String.format("Actualizado codigos de op desglose '%s' op.ecc.: '%s' op.ecc.prev.: '%s' cta: '%s'",
          an.getCdoperacionecc(), an.getCdoperacionecc(), an.getTmct0CuentasDeCompensacion().getCdCodigo());
      logBo.insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct
          .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc()
          .getId().getNucnfclt(), msg, dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
          auditUser);
      // dct.setCdoperacionecc(an.getCdoperacionecc());
      // dct.setNuoperacionprev(an.getNuoperacionprev());
      //
      // // TODO VER SI PODEMOS CREAR AQUI LOS MOVIMIENTOS
      // List<Tmct0movimientoeccFidessa> listMovimientosFidessa =
      // movfBo.findAllByNuoperacionprev(an
      // .getCdoperacionecc());

    }
    else {
      dct.setCdoperacionecc(an.getCdoperacionecc());
      dct.setNuoperacionprev(an.getNuoperacionprev());
      String msg = String.format("Actualizado codigos de op desglose '%s' op.ecc.: '%s' op.ecc.prev.: '%s' cta: '%s'",
          dct.getNudesglose(), an.getCdoperacionecc(), an.getNuoperacionprev(), an.getTmct0CuentasDeCompensacion()
              .getCdCodigo());
      logBo.insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct
          .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc()
          .getId().getNucnfclt(), msg, dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
          auditUser);
    }
    dct.setCuentaCompensacionDestino(an.getTmct0CuentasDeCompensacion().getCdCodigo());
    dct.setMiembroCompensadorDestino("");
    dct.setCodigoReferenciaAsignacion("");

    dct.setAuditFechaCambio(new Date());
    dct.setAuditUser(auditUser);
    if (dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.ACTUALIZADOS_CODIGOS_OPERACION
        .getId()) {
      logBo.insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct
          .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc()
          .getId().getNucnfclt(), "Cambiado estado a ACTUALIZADOS_CODIGOS_OPERACION.", dct.getTmct0desglosecamara()
          .getTmct0estadoByCdestadoasig().getNombre(), auditUser);
      dcBo.ponerEstadoAsignacion(dct.getTmct0desglosecamara(),
          EstadosEnumerados.ASIGNACIONES.ACTUALIZADOS_CODIGOS_OPERACION.getId(), auditUser);
    }
    dctBo.save(dct);
  }

  private void calcularEstadoParaReenvio(Tmct0desgloseclitit dct, String auditUser) throws SIBBACBusinessException {
    String msg;
    SettlementData sd = settlementDataBo.getSettlementDataNacional(dct.getTmct0desglosecamara().getTmct0alc()
        .getTmct0alo());
    if (sd.getTmct0ilq() != null && sd.getTmct0ilq().getDesglose() != null && sd.getTmct0ilq().getDesglose() == 'D') {
      if (dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR
          .getId()) {
        msg = "Cambiado estado a PDTE_ENVIAR.";
        logBo.insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct
            .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc()
            .getId().getNucnfclt(), msg, dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
            auditUser);
        dcBo.ponerEstadoAsignacion(dct.getTmct0desglosecamara(), EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId(),
            auditUser);
      }
    }
    else {

      if (!sd.isDataIL()) {
        msg = String.format("No hay instrucciones de liquidacion para el alias '%s' y fecha '%s'.", dct
            .getTmct0desglosecamara().getTmct0alc().getTmct0alo().getCdalias(), dct.getTmct0desglosecamara()
            .getTmct0alc().getTmct0alo().getFeoperac());
        logBo.insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct
            .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc()
            .getId().getNucnfclt(), msg, dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
            auditUser);
      }
      if (dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO
          .getId()) {
        msg = "Cambiado estado a PDTE_REVISAR_USUARIO.";
        logBo.insertarRegistro(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), dct
            .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), dct.getTmct0desglosecamara().getTmct0alc()
            .getId().getNucnfclt(), msg, dct.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
            auditUser);
        dcBo.ponerEstadoAsignacion(dct.getTmct0desglosecamara(),
            EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId(), auditUser);
      }
    }
  }

  private boolean cargarInformacionCompensacionInicial(Tmct0desgloseclitit dct, String auditUser) {
    boolean save = false;
    Tmct0anotacionecc an = anBo.findByCdoperacionecc(dct.getCdoperacionecc());
    if (an != null) {
      LOG.debug("[actualizarCodigosOperacion] cargamos la informacion de compensacion en el desglose");
      dct.setCdoperacionecc(an.getCdoperacionecc());
      dct.setNuoperacionprev(an.getNuoperacionprev());
      dct.setCuentaCompensacionDestino(an.getTmct0CuentasDeCompensacion().getCdCodigo());
      dct.setAuditFechaCambio(new Date());
      dct.setAuditUser(auditUser);
      save = true;
    }
    return save;
  }
}
