package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.escalado.bo.EscalaEstadoContabilidadBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_ESCALADO_CONTABILIDAD_SD, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0,30 7-23 ? * MON-FRI")
public class TaskEscaladoContabilidadSD extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEscaladoContabilidadSD.class);

  @Autowired
  private EscalaEstadoContabilidadBo escalaEstados;

  @Override
  public void executeTask() throws Exception {
    escalaEstados.escalarEstados(true);
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_DAEMONS_ESCALADO_ESTADO_CONTABILIDAD_SD;
  }
}
