package sibbac.business.desglose.query;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.page.query.BookingsRechazablesPageQuery;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;

@Component("DesgloseManualPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DesgloseManualPageQuery extends BookingsRechazablesPageQuery {

  private static final String GROUP_BY = " GROUP BY A.NUORDEN, A.NBOOKING, A.NUCNFCLT, A.REF_CLIENTE, EST.NOMBRE, B.CDALIAS, B.DESCRALI, "
      + "B.CDTPOPER, B.CDISIN, B.NBVALORS, A.NUTITCLI, B.CASEPTI ";

  @Autowired
  private Tmct0cfgBo cfgBo;

  public DesgloseManualPageQuery() {
  }

  public String getSelect() {
    StringBuilder sb = new StringBuilder("SELECT");
    sb.append(
        " A.NUORDEN, A.NBOOKING, A.NUCNFCLT, A.REF_CLIENTE, EST.NOMBRE ESTADO, B.CDALIAS ALIAS, B.DESCRALI DESC_ALIAS, ")
        .append("B.CDTPOPER SENTIDO, B.CDISIN ISIN, B.NBVALORS DESC_ISIN, A.NUTITCLI TITULOS, B.CASEPTI CASE_PTI");
    return sb.toString();
  }

  @Override
  public String getFrom() {
    StringBuilder sb = new StringBuilder("FROM");
    sb.append(" TMCT0BOK B INNER JOIN TMCT0ALO A ON A.NUORDEN = B.NUORDEN AND A.NBOOKING = B.NBOOKING ").append(
        "INNER JOIN TMCT0ESTADO EST ON A.CDESTADOASIG=EST.IDESTADO ");
    return sb.toString();
  }

  @Override
  public String getPostWhereConditions() {

    String cdestadosAsigBloquean = cfgBo.getCdestadoasigLockUserActivity();
    Integer cdestadoasigGtBloquea = cfgBo.getCdestadoasigGtLockUserActivity();
    Integer cdestadoEntRecGtBloquea = cfgBo.getCdestadoentrecGtLockUserActivity();
    String where = " B.CDESTADOASIG > {0} AND A.CDESTADOASIG > {0} ";
    where = MessageFormat.format(where, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
    if (cdestadosAsigBloquean != null && !cdestadosAsigBloquean.trim().isEmpty()) {
      cdestadosAsigBloquean = String.format("%s,%s", cdestadosAsigBloquean,
          EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    }
    else {
      cdestadosAsigBloquean = String.format("%s,%s", EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(),
          EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId());
    }

    if (cdestadosAsigBloquean != null && !cdestadosAsigBloquean.trim().isEmpty()) {

      where = MessageFormat.format(" {0} AND B.CDESTADOASIG NOT IN ({1}) AND A.CDESTADOASIG NOT IN ({1}) ", where,
          cdestadosAsigBloquean);
    }

    if (cdestadoasigGtBloquea != null) {
      where = MessageFormat.format(" {0} AND A.CDESTADOASIG < {1} ", where, cdestadoasigGtBloquea);
    }

    if (cdestadoEntRecGtBloquea != null) {
      where = MessageFormat
          .format(
              " {0} AND NOT EXISTS (SELECT 1 FROM TMCT0ALC C WHERE A.NUORDEN = C.NUORDEN AND A.NBOOKING = C.NBOOKING AND A.NUCNFCLT = C.NUCNFCLT AND C.CDESTADOASIG > {1} AND C.CDESTADOENTREC > {2} )",
              where, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(), cdestadoEntRecGtBloquea);
    }

    return where;
  }

  @Override
  public String getGroup() {
    return GROUP_BY;
  }

}
