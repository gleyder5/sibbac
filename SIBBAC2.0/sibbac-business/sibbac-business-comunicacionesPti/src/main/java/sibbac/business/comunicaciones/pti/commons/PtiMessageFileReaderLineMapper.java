package sibbac.business.comunicaciones.pti.commons;

import org.springframework.batch.item.file.LineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.commons.dto.PtiMessageFileReaderDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.Header;

@Service
public class PtiMessageFileReaderLineMapper implements LineMapper<PtiMessageFileReaderDTO> {

  @Autowired
  private Tmct0cfgBo cfgBo;

  public PtiMessageFileReaderLineMapper() {
  }

  @Override
  public PtiMessageFileReaderDTO mapLine(String line, int lineNumber) throws Exception {
    try {
      Header head = (Header) PTIMessageFactory.parseHeader(line);
      PTIMessageVersion version = cfgBo.getPtiMessageVersionFromTmct0cfg(head.getFechaDeEnvio());
      try {

        return new PtiMessageFileReaderDTO(PTIMessageFactory.parse(version, line));
      }
      catch (PTIMessageException e) {
        if (version == PTIMessageVersion.VERSION_1) {
          return new PtiMessageFileReaderDTO(PTIMessageFactory.parse(PTIMessageVersion.VERSION_T2S, line));
        }
        else if (version == PTIMessageVersion.VERSION_T2S) {
          return new PtiMessageFileReaderDTO(PTIMessageFactory.parse(PTIMessageVersion.VERSION_1, line));
        }
        else {
          throw new SIBBACBusinessException(String.format("Inciencia parseando linea nº: %s linea: %s", lineNumber,
              line), e);
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(
            String.format("Inciencia parseando linea nº: %s linea: %s", lineNumber, line), e);
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("Inciencia parseando linea nº: %s linea: %s", lineNumber, line),
          e);
    }
  }
}
