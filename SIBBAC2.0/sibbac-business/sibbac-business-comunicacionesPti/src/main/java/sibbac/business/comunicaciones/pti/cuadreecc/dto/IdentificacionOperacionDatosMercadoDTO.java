package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;
import java.util.Date;

public class IdentificacionOperacionDatosMercadoDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2060848712875924254L;
  protected final Date fechaEjecucion;
  protected final String cdisin;
  protected final char sentido;
  protected final String cdcamara;
  protected final String cdsegmento;
  protected final String cdoperacionmkt;
  protected final String cdmiembromkt;
  protected final String nuordenmkt;
  protected final String nuexemkt;

  public IdentificacionOperacionDatosMercadoDTO(Date fechaEjecucion,
                                                String cdisin,
                                                char sentido,
                                                String cdcamara,
                                                String cdsegmento,
                                                String cdoperacionmkt,
                                                String cdmiembromkt,
                                                String nuordenmkt,
                                                String nuexemkt) {
    super();
    this.fechaEjecucion = fechaEjecucion;
    this.cdisin = cdisin;
    this.sentido = sentido;
    this.cdcamara = cdcamara;
    this.cdsegmento = cdsegmento;
    this.cdoperacionmkt = cdoperacionmkt;
    this.cdmiembromkt = cdmiembromkt;
    this.nuordenmkt = nuordenmkt;
    this.nuexemkt = nuexemkt;
  }

  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  public String getCdisin() {
    return cdisin;
  }

  public char getSentido() {
    return sentido;
  }

  public String getCdcamara() {
    return cdcamara;
  }

  public String getCdsegmento() {
    return cdsegmento;
  }

  public String getCdoperacionmkt() {
    return cdoperacionmkt;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdcamara == null) ? 0 : cdcamara.hashCode());
    result = prime * result + ((cdisin == null) ? 0 : cdisin.hashCode());
    result = prime * result + ((cdmiembromkt == null) ? 0 : cdmiembromkt.hashCode());
    result = prime * result + ((cdoperacionmkt == null) ? 0 : cdoperacionmkt.hashCode());
    result = prime * result + ((cdsegmento == null) ? 0 : cdsegmento.hashCode());
    result = prime * result + ((fechaEjecucion == null) ? 0 : fechaEjecucion.hashCode());
    result = prime * result + ((nuexemkt == null) ? 0 : nuexemkt.hashCode());
    result = prime * result + ((nuordenmkt == null) ? 0 : nuordenmkt.hashCode());
    result = prime * result + sentido;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    IdentificacionOperacionDatosMercadoDTO other = (IdentificacionOperacionDatosMercadoDTO) obj;
    if (cdcamara == null) {
      if (other.cdcamara != null)
        return false;
    } else if (!cdcamara.equals(other.cdcamara))
      return false;
    if (cdisin == null) {
      if (other.cdisin != null)
        return false;
    } else if (!cdisin.equals(other.cdisin))
      return false;
    if (cdmiembromkt == null) {
      if (other.cdmiembromkt != null)
        return false;
    } else if (!cdmiembromkt.equals(other.cdmiembromkt))
      return false;
    if (cdoperacionmkt == null) {
      if (other.cdoperacionmkt != null)
        return false;
    } else if (!cdoperacionmkt.equals(other.cdoperacionmkt))
      return false;
    if (cdsegmento == null) {
      if (other.cdsegmento != null)
        return false;
    } else if (!cdsegmento.equals(other.cdsegmento))
      return false;
    if (fechaEjecucion == null) {
      if (other.fechaEjecucion != null)
        return false;
    } else if (!fechaEjecucion.equals(other.fechaEjecucion))
      return false;
    if (nuexemkt == null) {
      if (other.nuexemkt != null)
        return false;
    } else if (!nuexemkt.equals(other.nuexemkt))
      return false;
    if (nuordenmkt == null) {
      if (other.nuordenmkt != null)
        return false;
    } else if (!nuordenmkt.equals(other.nuordenmkt))
      return false;
    if (sentido != other.sentido)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "IdentificacionOperacionDatosMercadoDTO [fechaEjecucion=" + fechaEjecucion + ", cdisin=" + cdisin
           + ", sentido=" + sentido + ", cdcamara=" + cdcamara + ", cdsegmento=" + cdsegmento + ", cdoperacionmkt="
           + cdoperacionmkt + ", cdmiembromkt=" + cdmiembromkt + ", nuordenmkt=" + nuordenmkt + ", nuexemkt="
           + nuexemkt + "]";
  }

}
