package sibbac.business.comunicaciones.euroccp.ciffile.runnable;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.bo.EuroCcpProcessedUnsettledMovementBo;
import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpProcessedUnsettledMovementRunnable")
public class EuroCcpProcessedUnsettledMovementRunnable extends IRunnableBean<EuroCcpIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpProcessedUnsettledMovementRunnable.class);

  @Autowired
  private EuroCcpProcessedUnsettledMovementBo euroCcpProcessedUntsettledMovementBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  public EuroCcpProcessedUnsettledMovementRunnable() {
    super();
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<EuroCcpIdDTO> beans, int order) throws SIBBACBusinessException {
    String idEutroCcp = cfgBo.getIdEuroCcp();
    LOG.trace("[EuroCcpProcessedUnsettledMovementRunnable :: run] idEuroCcp: {}", idEutroCcp);
    if (StringUtils.isBlank(idEutroCcp)) {
      throw new SIBBACBusinessException("INCIDNECIA- Id Euro Ccp no configurado.");
    }
    try {
      euroCcpProcessedUntsettledMovementBo.grabarOperacioncdeccAndCase(beans, idEutroCcp, "SIBBAC20");
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException(e);
    } finally {
      getObserver().sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<EuroCcpIdDTO> clone() {
    return this;
  }

}
