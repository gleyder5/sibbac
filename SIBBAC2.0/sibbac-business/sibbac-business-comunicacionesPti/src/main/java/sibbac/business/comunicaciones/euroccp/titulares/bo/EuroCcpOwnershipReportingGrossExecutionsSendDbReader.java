package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpRangoFechasHorasInicioProcesosDTO;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO;
import sibbac.business.comunicaciones.euroccp.titulares.runnable.EuroCcpOwnershipReportingGrossExecutionsSendRunnable;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TITULARIDADES;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpOwnershipReportingGrossExecutionsSendDbReader")
public class EuroCcpOwnershipReportingGrossExecutionsSendDbReader extends
    EuroCcpAbstractDbReader<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory
      .getLogger(EuroCcpOwnershipReportingGrossExecutionsSendDbReader.class);

  @Value("${sibbac.euroccp.titulares.org.file.date.pattern}")
  private String fileDatePattern;

  @Value("${sibbac.euroccp.titulares.org.txt.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.euroccp.titulares.org.txt.pool.size}")
  private int threadSize;

  @Value("${sibbac.euroccp.titulares.org.file.charset}")
  private Charset fileCharset;

  @Value("${sibbac.euroccp.out.path}")
  private String workPathSt;

  @Value("${sibbac.euroccp.out.tmp.folder}")
  private String tmpFolder;

  @Value("${sibbac.euroccp.out.pendientes.folder}")
  private String pendientesFolder;

  @Value("${sibbac.euroccp.out.enviados.folder}")
  private String enviadosFolder;

  @Value("${sibbac.euroccp.out.tratados.folder}")
  private String tratadosFolder;

  @Value("${sibbac.euroccp.titulares.org.txt.file.name}")
  private String tmpFileName;

  @Value("${sibbac.euroccp.titulares.org.file.name.client.number.last.positions}")
  private int clientNumberLastPositions;

  @Value("${sibbac.euroccp.titulares.org.file.send.first.sd.date}")
  private int dDaysFirstSendDate;

  @Value("${sibbac.euroccp.titulares.org.file.send.first.sd.date.init.hour}")
  private String initSendHourFirstDate;

  @Value("${sibbac.euroccp.titulares.org.file.send.last.sd.date}")
  private int dDaysLastDate;

  @Value("${sibbac.euroccp.titulares.org.file.send.last.sd.date.end.hour}")
  private String endSendHourLastDate;

  @Value("${sibbac.euroccp.titulares.org.file.send.mail.compress.extension}")
  private String excelSendMailCompressExtension;

  @Value("${sibbac.euroccp.titulares.org.file.send.by.mail}")
  private String principalFileSentMail;

  @Value("${sibbac.euroccp.titulares.org.file.send.to}")
  private List<String> to;

  @Value("${sibbac.euroccp.titulares.org.file.send.cc}")
  private List<String> cc;

  @Value("${sibbac.euroccp.titulares.org.file.send.subjet}")
  private String subject;

  @Value("${sibbac.euroccp.titulares.org.file.send.body}")
  private String body;

  @Value("${sibbac.euroccp.titulares.org.file.send.timezone}")
  private TimeZone timeZone;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao desgloseCamaraEnvioRtDao;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private FestivoBo festivoBo;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionsSendRunnable")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsSendRunnable runnable;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionsFileWriterBo")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsFileWriterBo euroCcpOwnershipReportingGrossExecutionsBo;

  @Autowired
  private EuroCcpFileSentBo euroCcpFileSentBo;

  public EuroCcpOwnershipReportingGrossExecutionsSendDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> getBeanToProcessBlock() {
    return runnable;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    beanIterator = null;
    beanIterator = getListToProcess().iterator();
  }

  private List<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> getListToProcess() throws SIBBACBusinessException {
    List<Date> holidays = festivoBo.findAllFestivosNacionales();
    List<Integer> workDaysOfWeek = cfgBo.getWorkDaysOfWeek();

    String euroCcpId = cfgBo.getIdEuroCcp();
    if (euroCcpId == null) {
      throw new SIBBACBusinessException("INCIDENCIA Camara no configurada.");
    }
    Tmct0CamaraCompensacion camara = camaraCompensacionBo.findByCdCodigo(euroCcpId);
    if (camara == null) {
      throw new SIBBACBusinessException("INCIDENCIA Camara no no encontrada " + euroCcpId);
    }

    List<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> ids = new ArrayList<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO>();
    Collection<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> idsHash = new HashSet<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO>();
    List<Object[]> idsAux = new ArrayList<Object[]>();

    EuroCcpRangoFechasHorasInicioProcesosDTO fechaHoraCorteProcesos = getConfigHorasCorteEnvioTitulares();
    LOG.info("[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] config: {}",
        fechaHoraCorteProcesos);

    Date fechaLiquidacion1 = getFechaLiquidacion(fechaHoraCorteProcesos.getdDaysLastDate(), workDaysOfWeek, holidays);

    Date fechaLiquidacion2 = getFechaLiquidacion(fechaHoraCorteProcesos.getdDaysFirstSendDate(), workDaysOfWeek,
        holidays);

    if (fechaLiquidacion1.compareTo(fechaLiquidacion2) > 0) {
      Date fechaliquidacion1Aux = fechaLiquidacion1;
      fechaLiquidacion1 = fechaLiquidacion2;
      fechaLiquidacion2 = fechaliquidacion1Aux;
    }
    LOG.debug(
        "[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] buscando fechas entre {} y {}.",
        fechaLiquidacion1, fechaLiquidacion2);
    LOG.debug("[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] buscando fechas que enviar...");
    List<Date> dates = desgloseCamaraEnvioRtDao.findAllDistintsFecontratacionCamaraAndFinicioAndEstado(
        fechaLiquidacion1, fechaLiquidacion2, camara.getIdCamaraCompensacion(), 'P',
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(), EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());

    LOG.debug(
        "[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] encontradas {} fechas que enviar.",
        dates);
    for (Date date : dates) {

      LOG.debug(
          "[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] Buscando para el envio de fichero org fecha : {}",
          date);
      // ors enviado

      idsAux.addAll(desgloseCamaraEnvioRtDao.findAllIdsByCamaraAndFinicioAndEstado(date,
          camara.getIdCamaraCompensacion(), 'E', 'P', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
          EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId()));
      idsAux.addAll(desgloseCamaraEnvioRtDao.findAllIdsByCamaraAndFinicioAndEstado(date,
          camara.getIdCamaraCompensacion(), 'E', 'P', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
          TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId()));
      // ors aceptado
      idsAux.addAll(desgloseCamaraEnvioRtDao.findAllIdsByCamaraAndFinicioAndEstado(date,
          camara.getIdCamaraCompensacion(), 'A', 'P', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
          EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId()));
      idsAux.addAll(desgloseCamaraEnvioRtDao.findAllIdsByCamaraAndFinicioAndEstado(date,
          camara.getIdCamaraCompensacion(), 'A', 'P', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
          TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId()));

    }
    LOG.debug("[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] encontratdos {} que procesar.",
        idsAux.size());
    EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO id;
    if (CollectionUtils.isNotEmpty(idsAux)) {
      Collection<String> clientNumbers = new HashSet<String>();
      for (Object[] objects : idsAux) {
        id = new EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO((Date) objects[0], (String) objects[1],
            (String) objects[2]);
        if (idsHash.add(id)) {
          ids.add(id);
        }

        crearFicheroPrincipal(clientNumbers, tmpFileName, id.getCdmiembromkt(), clientNumberLastPositions);
      }
      clientNumbers.clear();
      idsHash.clear();
      idsAux.clear();
    }
    return ids;
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {
    euroCcpOwnershipReportingGrossExecutionsBo.appendTmpFiles(workPathSt, tmpFolder, pendientesFolder, enviadosFolder,
        tratadosFolder, excelSendMailCompressExtension, fileCharset, timeZone);
    if (principalFileSentMail != null && "S".equalsIgnoreCase(principalFileSentMail)) {
      euroCcpOwnershipReportingGrossExecutionsBo.enviarFilesByEmail(workPathSt, tmpFolder, pendientesFolder,
          enviadosFolder, tratadosFolder, to, cc, subject, body);
    }
    else {
      euroCcpOwnershipReportingGrossExecutionsBo.moverRutaEnvio(workPathSt);
    }
  }

  protected void crearFicheroPrincipal(Collection<String> clientNumbersCreados, String tmpFileName,
      String clientNumber, int clientNumberLastPositions) {

    if (clientNumbersCreados.add(clientNumber)) {
      euroCcpFileSentBo.saveNewFileRegisterIfNotExistsWithoutSentNewTransacction(tmpFileName, clientNumber,
          clientNumberLastPositions, fileDatePattern, EuroCcpFileTypeEnum.ORG);
    }

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public int getdDaysFirstSendDate() {
    return dDaysFirstSendDate;
  }

  @Override
  public String getInitSendHourFirstDate() {
    return initSendHourFirstDate;
  }

  @Override
  public int getdDaysLastDate() {
    return dDaysLastDate;
  }

  @Override
  public String getEndSendHourLastDate() {
    return endSendHourLastDate;
  }

  @Override
  public TimeZone getTimeZone() {
    return timeZone;
  }

}
