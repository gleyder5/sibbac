package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpProcessedSettledMovementFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedSettledMovement;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpProcessedSettledMovementFieldSetMapper extends
                                                          WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean> {

  public EuroCcpProcessedSettledMovementFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpProcessedSettledMovementFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {

    return new EuroCcpProcessedSettledMovement();
  }

}
