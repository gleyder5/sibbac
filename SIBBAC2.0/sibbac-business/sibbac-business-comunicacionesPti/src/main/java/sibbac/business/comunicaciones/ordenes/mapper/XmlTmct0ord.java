package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.broker.bo.PKBGenerateRouting;
import sibbac.business.comunicaciones.contabilidad.bo.ContabilidadInternacionalBo;
import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenForeingMarketReceptionException;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenInternalReceptionException;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.bo.Tgrl0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.dao.Tmct0ordDynamicValuesDao;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0ordDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0ordDynamicValuesId;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

import com.sv.sibbac.multicenter.Allocation;
import com.sv.sibbac.multicenter.BookingEconomicDataType;
import com.sv.sibbac.multicenter.BookingType;
import com.sv.sibbac.multicenter.Cargos;
import com.sv.sibbac.multicenter.Commission;
import com.sv.sibbac.multicenter.DatesType;
import com.sv.sibbac.multicenter.DynamicValue;
import com.sv.sibbac.multicenter.Impuesto;
import com.sv.sibbac.multicenter.NetConsideration;
import com.sv.sibbac.multicenter.OrderPrice;
import com.sv.sibbac.multicenter.OrderType;
import com.sv.sibbac.multicenter.Orders;
import com.sv.sibbac.multicenter.VolumeType;

@Service
public class XmlTmct0ord {

  private static final Logger LOG = LoggerFactory.getLogger(XmlTmct0ord.class);

  @Autowired
  private Tmct0xasBo xasBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private XmlTmct0log xmlTmct0log;

  @Autowired
  private XmlTmct0bok xmlTmct0bok;

  @Autowired
  private Tmct0bokBo tmct0bokBo;

  @Autowired
  private Tgrl0ordBo tgrl0ordBo;

  @Autowired
  private Accumulators accumulatorsCentros;

  @Qualifier(value = "PKBGenerateRouting")
  @Autowired
  private PKBGenerateRouting pkbGenerateRouting;

  @Autowired
  private ContabilidadInternacionalBo contabilidadInternacional;

  @Autowired
  private Tmct0ordDynamicValuesDao tmct0ordDynamicValuesDao;

  public XmlTmct0ord() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tmct0bok xml_mapping(Tmct0ord tmct0ord, Orders orders, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws XmlOrdenForeingMarketReceptionException, XmlOrdenExceptionReception, XmlOrdenInternalReceptionException {

    OrderType order = orders.getOrder();

    String cdclsmdo = cachedConfig.getSettlementExchangeCdclsmdo(xasBo, order.getSettlementExchange());
    LOG.trace(" tmct0xas NBCAMXML : 'CDMERCAD' NBCAMAS4 : 'CDMERCAD' converted cdclsmdo : {}", cdclsmdo);
    if (StringUtils.isNotBlank(cdclsmdo)) {
      tmct0ord.setCdclsmdo(cdclsmdo.charAt(0));
    }
    else {
      if (!cachedConfig.isLoadInternacional()) {
        throw new XmlOrdenForeingMarketReceptionException("No configurado para cargar xml de internacional.");
      }
      tmct0ord.setCdclsmdo('E');
    }

    if (StringUtils.isNotBlank(orders.getCdentdd())) {
      tmct0ord.setCdentdd(orders.getCdentdd().charAt(0));
    }
    else {
      tmct0ord.setCdentdd(' ');
    }
    tmct0ord.setNureford(order.getRefCliente());
    tmct0ord.setDsobserv(order.getDsobserv());
    tmct0ord.setCdtpoper(order.getTpoperac().toUpperCase().charAt(0));
    tmct0ord.setCdorigen(order.getCdorigen());
    tmct0ord.setCdgesord(order.getTporden().toUpperCase());
    tmct0ord.setCdisin(order.getCdisin());
    tmct0ord.setNbvalors(order.getIsinnom());
    tmct0ord.setCdcuenta(order.getCdcuenta());
    tmct0ord.setCdclente(order.getCdclente());
    tmct0ord.setCdcanal(order.getCdcanal());
    tmct0ord.setCdtipord(order.getCdtipord());
    tmct0ord.setCdcaptur(order.getCdcaptur().charAt(0));
    tmct0ord.setCdusuari(order.getCdusuari());
    tmct0ord.setCdtpmerc(order.getCdTipoMkt());
    if (StringUtils.isNotBlank(order.getCdwareho())) {
      tmct0ord.setCdwareho(order.getCdwareho().charAt(0));
    }
    else {
      tmct0ord.setCdwareho(' ');
    }
    String ccv = order.getCcv();
    LOG.trace("ccv :{}", ccv);
    if (ccv.length() >= 7) {
      tmct0ord.setCdsucliq(ccv.substring(4, 7));
      if (ccv.length() >= 14) {
        tmct0ord.setNuctacli(ccv.substring(8, 14));
      }
    }
    LOG.trace("xml order.SettlementExchange : {}", order.getSettlementExchange());
    String cdmercad = cachedConfig.getSettlementExchangeCdmercad(xasBo, order.getSettlementExchange());
    if (StringUtils.isBlank(cdmercad)) {
      cdmercad = order.getSettlementExchange();
    }
    LOG.trace(" tmct0xas NBCAMXML : 'SettlementExchange' NBCAMAS4 : 'CDMERCAD' converted cdmercad : {}", cdmercad);

    tmct0ord.setCdmercad(cdmercad);
    tmct0ord.setCcv(ccv);
    tmct0ord.setSegmentclte(order.getSegmentClte());
    tmct0ord.setCdmodnego(order.getCdmodnego());

    /*
     * multicenter: datos economicos orden
     */
    BookingEconomicDataType economicData = order.getBooking().getEconomicDataBooking();
    tmct0ord.setImtpcamb(new BigDecimal(economicData.getImtpcamb()).setScale(8, RoundingMode.HALF_UP));
    tmct0ord.setCdmoniso(economicData.getCdmoniso());
    tmct0ord.setImnomval(new BigDecimal(economicData.getImnomval()).setScale(8, RoundingMode.HALF_UP));
    tmct0ord.setCdtpcamb('P');

    Cargos cargos = economicData.getMarketCharges().getCargos();
    if (cargos != null) {
      if (tmct0ord.getImbrmerc() == null) {
        tmct0ord.setImbrmerc(BigDecimal.ZERO);
      }
      tmct0ord.setImbrmerc(tmct0ord.getImbrmerc().add(
          new BigDecimal(cargos.getImBrutoMktDv()).setScale(8, RoundingMode.HALF_UP)));

      if (tmct0ord.getImbrmreu() == null) {
        tmct0ord.setImbrmreu(BigDecimal.ZERO);
      }
      tmct0ord.setImbrmreu(tmct0ord.getImbrmreu().add(
          new BigDecimal(cargos.getImBrutoMktEu()).setScale(8, RoundingMode.HALF_UP)));
      if (tmct0ord.getImgasgesdv() == null) {
        tmct0ord.setImgasgesdv(BigDecimal.ZERO);
      }
      tmct0ord.setImgasgesdv(tmct0ord.getImgasgesdv().add(
          new BigDecimal(cargos.getImCanonContrDv()).setScale(8, RoundingMode.HALF_UP)));
      if (tmct0ord.getImgasgeseu() == null) {
        tmct0ord.setImgasgeseu(BigDecimal.ZERO);
      }
      tmct0ord.setImgasgeseu(tmct0ord.getImgasgeseu().add(
          new BigDecimal(cargos.getImCanonContrEu()).setScale(8, RoundingMode.HALF_UP)));
      if (tmct0ord.getImgasliqdv() == null) {
        tmct0ord.setImgasliqdv(BigDecimal.ZERO);
      }
      tmct0ord.setImgasliqdv(tmct0ord.getImgasliqdv().add(
          new BigDecimal(cargos.getImCanonLiqDv()).setScale(8, RoundingMode.HALF_UP)));
      if (tmct0ord.getImgasliqeu() == null) {
        tmct0ord.setImgasliqeu(BigDecimal.ZERO);
      }
      tmct0ord.setImgasliqeu(tmct0ord.getImgasliqeu().add(
          new BigDecimal(cargos.getImCanonLiqEu()).setScale(8, RoundingMode.HALF_UP)));

    }
    BigDecimal impuestosAcumuladosEuroBooking = BigDecimal.ZERO;
    BigDecimal impuestosAcumuladosDivisaBooking = BigDecimal.ZERO;
    BigDecimal impuestosAcumuladosEurosAllo = BigDecimal.ZERO;
    BigDecimal impuestosAcumuladosDivisaAllo = BigDecimal.ZERO;
    for (Impuesto impuesto : economicData.getMarketCharges().getImpuesto()) {
      impuestosAcumuladosEuroBooking = impuestosAcumuladosEuroBooking.add(new BigDecimal(impuesto.getImporteEu())
          .setScale(8, RoundingMode.HALF_UP));
      impuestosAcumuladosDivisaBooking = impuestosAcumuladosDivisaBooking.add(new BigDecimal(impuesto.getImporteDv())
          .setScale(8, RoundingMode.HALF_UP));
    }

    for (Allocation alloId : order.getBooking().getAllocations()) {
      if (alloId.getEconomicDataAlloc() != null && alloId.getEconomicDataAlloc().getMarketCharges() != null
          && alloId.getEconomicDataAlloc().getMarketCharges().getImpuesto() != null) {
        for (Impuesto impuesto : alloId.getEconomicDataAlloc().getMarketCharges().getImpuesto()) {
          impuestosAcumuladosEurosAllo = impuestosAcumuladosEurosAllo.add(new BigDecimal(impuesto.getImporteEu())
              .setScale(8, RoundingMode.HALF_UP));
          impuestosAcumuladosDivisaAllo = impuestosAcumuladosDivisaAllo.add(new BigDecimal(impuesto.getImporteDv())
              .setScale(8, RoundingMode.HALF_UP));

        }
      }
    }
    if (impuestosAcumuladosDivisaAllo.compareTo(impuestosAcumuladosDivisaBooking) != 0) {
      LOG.warn("Impuestos distintos DIVISA a nivel de booking: {} y de allo: {}", impuestosAcumuladosDivisaBooking,
          impuestosAcumuladosDivisaAllo);
    }
    tmct0ord.setImimpdvs(tmct0ord.getImimpdvs().add(impuestosAcumuladosDivisaAllo));

    if (impuestosAcumuladosEurosAllo.compareTo(impuestosAcumuladosEuroBooking) != 0) {
      LOG.warn("Impuestos distintos EURO a nivel de booking: {} y de allo: {}", impuestosAcumuladosEuroBooking,
          impuestosAcumuladosEurosAllo);
    }
    tmct0ord.setImimpeur(tmct0ord.getImimpeur().add(impuestosAcumuladosEurosAllo));

    /*
     * multicenter:volume
     */
    VolumeType volume = economicData.getVolume();
    tmct0ord.setNutitord(new BigDecimal(volume.getNutitord()).setScale(5, RoundingMode.HALF_UP));

    /*
     * cargar siempre el maximo de los booking
     */
    tmct0ord.setNutiteje(tmct0ord.getNutiteje().add(
        new BigDecimal(volume.getNutiteje()).setScale(5, RoundingMode.HALF_UP)));

    if (tmct0ord.getNutiteje().compareTo(tmct0ord.getNutitord()) > 0) {
      throw new XmlOrdenExceptionReception(String.format("Quantity executed: %s greater than quantity ordered: %s ",
          tmct0ord.getNutiteje(), tmct0ord.getNutitord()));
    }

    /*
     * multicenter:dates
     */
    DatesType dates = economicData.getDates();
    tmct0ord.setFealta(new Date(dates.getFhalta().toGregorianCalendar().getTimeInMillis()));
    tmct0ord.setHoalta(new Time(dates.getFhalta().toGregorianCalendar().getTimeInMillis()));
    tmct0ord.setFecontra(new Date(dates.getFecontra().toGregorianCalendar().getTimeInMillis()));
    tmct0ord.setFeejecuc(new Date(dates.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    tmct0ord.setHoejecuc(new Time(dates.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    tmct0ord.setFevalide(new Date(dates.getFevalide().toGregorianCalendar().getTimeInMillis()));
    tmct0ord.setFevalor(new Date(dates.getFevalor().toGregorianCalendar().getTimeInMillis()));

    /*
     * multicenter:Net
     */
    NetConsideration net = economicData.getNet();

    tmct0ord.setImnetobr(tmct0ord.getImnetobr().add(
        new BigDecimal(net.getImNetoMktDv()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImnetocl(tmct0ord.getImnetocl().add(
        new BigDecimal(net.getImNetoClteDv()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImntbreu(tmct0ord.getImntbreu().add(
        new BigDecimal(net.getImNetoMktEu()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImntcleu(tmct0ord.getImntcleu().add(
        new BigDecimal(net.getImNetoClteEu()).setScale(8, RoundingMode.HALF_UP)));

    BigDecimal precionetoCliente = new BigDecimal(net.getPrecioNetoClte()).setScale(8, RoundingMode.HALF_UP);
    if (tmct0ord.getImcbntcl().compareTo(BigDecimal.ZERO) != 0
        && precionetoCliente.compareTo(tmct0ord.getImcbntcl()) != 0) {
      precionetoCliente = tmct0ord.getImnetocl().divide(tmct0ord.getNutiteje(), 8, RoundingMode.HALF_UP);
    }
    tmct0ord.setImcbntcl(precionetoCliente);

    /*
     * multicenter:commisions
     */
    Commission comisions = economicData.getCommisions();
    tmct0ord.setPodevolu(new BigDecimal(comisions.getPcComOrdenante()).setScale(6, RoundingMode.HALF_UP));
    double pofpacli = comisions.getPcComClte();
    if (pofpacli > 100.00)
      pofpacli = 100.00;    
    tmct0ord.setPofpacli(new BigDecimal(pofpacli).setScale(6, RoundingMode.HALF_UP));
    tmct0ord.setImdevcdv(tmct0ord.getImdevcdv().add(
        new BigDecimal(comisions.getImComOrdenanteDv()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImdevcli(tmct0ord.getImdevcli().add(
        new BigDecimal(comisions.getImComOrdenanteEu()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImfibrdv(tmct0ord.getImfibrdv().add(
        new BigDecimal(comisions.getImComBrkDv()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImfibreu(tmct0ord.getImfibreu().add(
        new BigDecimal(comisions.getImComBrkEu()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImsvb(tmct0ord.getImsvb().add(
        new BigDecimal(comisions.getImCorretajeSVDv()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImsvbeu(tmct0ord.getImsvbeu().add(
        new BigDecimal(comisions.getImCorretajeSVEu()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImfpacdv(tmct0ord.getImfpacdv().add(
        new BigDecimal(comisions.getImComClteDv()).setScale(8, RoundingMode.HALF_UP)));
    tmct0ord.setImbensvb(tmct0ord.getImbensvb().add(
        new BigDecimal(comisions.getImCorretajeSVFinalEu()).setScale(8, RoundingMode.HALF_UP)));

    /*
     * multicenter:price
     */
    OrderPrice price = economicData.getPrice();
    tmct0ord.setImlimite(new BigDecimal(price.getPrecioLimiteClte()).setScale(8, RoundingMode.HALF_UP));
    BigDecimal precioBrutoMkt = new BigDecimal(price.getPrecioBrutoMkt()).setScale(8, RoundingMode.HALF_UP);

    if (tmct0ord.getImbrmerc().divide(tmct0ord.getNutiteje(), 8, RoundingMode.HALF_UP).compareTo(precioBrutoMkt) != 0) {
      precioBrutoMkt = tmct0ord.getImbrmerc().divide(tmct0ord.getNutiteje(), 8, RoundingMode.HALF_UP);
    }
    tmct0ord.setImcbmerc(precioBrutoMkt);

    BigDecimal precioBrutoClte = new BigDecimal(price.getPrecioBrutoClte()).setScale(8, RoundingMode.HALF_UP);
    //Si es orden de extranjero Imcbclie será siempre precioBrutoClte
    if (precioBrutoClte.compareTo(precioBrutoMkt) != 0 && tmct0ord.getCdclsmdo() == 'N') {
      tmct0ord.setImcbclie(precioBrutoMkt);
    }
    else {
      tmct0ord.setImcbclie(precioBrutoClte);
    }

    tmct0ord.setCdvia("");
    tmct0ord.setNuejecnt(BigDecimal.ZERO.intValue());
    tmct0ord.setRfparten("");
    tmct0ord.setCdbroker(order.getBooking().getExecutionGroups().get(0).getTradingVenue());
    LOG.trace("xml order.SettlementExchange : {}", order.getSettlementExchange());

    if (tmct0ord.getNuoprout() == null || tmct0ord.getNuoprout() == 0) {
      tmct0ord.setNuoprout(ordBo.getNewNuoproutSequence().intValue());
    }

    // clearer for routing operations
    tmct0ord.setCdcleare(order.getClearer());

    // nif for standard canal
    tmct0ord.setNif(order.getNif());
    // standard canal client reference ccv
    tmct0ord.setContrprr(order.getEstandarCanal());
    // province code for standard canal
    tmct0ord.setCodprod(order.getCodprod());

    tmct0ord.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0ord.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);
    tmct0ord.setTmct0sta(new Tmct0sta(new Tmct0staId(TypeCdestado.NEW_REGISTER.getValue(), TypeCdtipest.TMCT0ORD
        .getValue())));

    // 2015-08-18
    // Se añade indicador de gastos
    tmct0ord.setCdindgas('S');

    if (cachedConfig.isScriptDividendInstance() || cachedConfig.isAliasScriptDividend(tmct0ord.getCdcuenta())) {
      LOG.trace(" xml_mapping() TMCT0ORD.ISSCRDIV ='S' ");
      tmct0ord.setIsscrdiv('S');
    }
    else {
      LOG.trace(" xml_mapping() TMCT0ORD.ISSCRDIV ='N' ");
      tmct0ord.setIsscrdiv('N');
    }

    this.mappDynamicValues(order, tmct0ord);
    List<Tmct0ordDynamicValues> dynamicValues = new ArrayList<Tmct0ordDynamicValues>(tmct0ord.getDynamicValues());
    tmct0ord.getDynamicValues().clear();
    tmct0ord = ordBo.save(tmct0ord);

    dynamicValues = this.tmct0ordDynamicValuesDao.save(dynamicValues);
    tmct0ord.getDynamicValues().addAll(dynamicValues);
    dynamicValues.clear();
    /*
     * carga en tmct0bok del elemento BookingType
     */
    BookingType booking = order.getBooking();

    /*
     * carga en tmct0bok del elemento log
     */
    xmlTmct0log.xml_mapping(tmct0ord, booking.getNbooking());

    if ('S' == tmct0ord.getIsscrdiv()) {
      xmlTmct0log.xml_mapping(tmct0ord, booking.getNbooking(),
          String.format("Orden cargada como Script dividend, alias: '%s'", tmct0ord.getCdcuenta()));
    }

    Tmct0bok tmct0bok = xmlTmct0bok.xml_mapping(tmct0ord, booking, cachedConfig);

    if (tmct0ord.getCdcleare() != null && !tmct0ord.getCdcleare().trim().isEmpty()) {
      String txtMsg = String.format("Receive clearer '%s' from fidessa. ", tmct0ord.getCdcleare());
      xmlTmct0log.xml_mapping(tmct0ord, booking.getNbooking(), txtMsg);
    }

    if ('E' == tmct0ord.getCdclsmdo()) {
      this.operativaInternacional(order, tmct0bok);
    }
    return tmct0bok;

  }

  @Transactional
  public void operativaInternacional(OrderType order, Tmct0bok tmct0bok) throws XmlOrdenExceptionReception {

    if (tmct0bok != null) {
      Tmct0ord tmct0ord = tmct0bok.getTmct0ord();

      this.operativaInternacionalCentros(tmct0ord, tmct0bok);

      // si es routing
      if (ordBo.isRouting(tmct0ord)) {
        this.operativaInternacionalRouting(tmct0ord, tmct0bok);
      }
      else {
        this.operativaInternacionalNoRouting(tmct0ord, tmct0bok);
      }
    }
    else {
      throw new XmlOrdenExceptionReception(order.getNuorden(), order.getBooking().getNbooking(), "", "",
          "Not found booking in ddbb for accumulate centers/confirm broker and accounting.");
    }
  }

  private void operativaInternacionalCentros(Tmct0ord tmct0ord, Tmct0bok tmct0bok) throws XmlOrdenExceptionReception {
    LOG.trace("[xml_mapping] Internacional acumulando en los cto-ctg...");
    try {
      accumulatorsCentros.accumulateAllValues(tmct0bok);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), tmct0bok.getId().getNbooking(), "", "",
          String.format("%s %s", "INCIDENCIA-Generando CENTROS extranjero.", e.getMessage()), e);
    }
  }

  private void operativaInternacionalRouting(Tmct0ord tmct0ord, Tmct0bok tmct0bok) throws XmlOrdenExceptionReception {
    // 1.- generar pkb
    try {
      LOG.trace("[xml_mapping] Internacional-ROUTING creando pkb-pkc-pkd-pke...");
      pkbGenerateRouting.createPkbs(tmct0bok);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), tmct0bok.getId().getNbooking(), "", "",
          String.format("%s %s", "INCIDENCIA-Generando PKB extranjero.", e.getMessage()), e);
    }
    LOG.trace("[xml_mapping] Internacional-ROUTING actualiznado estado a FOREING_WAIT_RESPONSE_CLEARER...");
    // 2.- update estado FOREING_WAIT_RESPONSE_CLEARER
    try {
      tmct0bokBo.updateCdestadoInternacional(tmct0bok, TypeCdestado.FOREING_WAIT_RESPONSE_CLEARER, "SIBBAC20");
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), tmct0bok.getId().getNbooking(), "", "",
          String.format("%s %s", "INCIDENCIA-Cambiando estado extranjero.", e.getMessage()), e);
    }
    // 3.- contabilidad
    try {
      LOG.trace("[xml_mapping] Internacional-ROUTING contabilizando...");
      contabilidadInternacional.contabilizarAloTipoAForeignRouting(false, tmct0bok);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), tmct0bok.getId().getNbooking(), "", "",
          String.format("%s %s", "INCIDENCIA-Contabilidad extranjero.", e.getMessage()), e);
    }
  }

  private void operativaInternacionalNoRouting(Tmct0ord tmct0ord, Tmct0bok tmct0bok) throws XmlOrdenExceptionReception {
    // 1.- mapeo tablas AS400
    try {
      tgrl0ordBo.mapearTablasAS400CargaInicialNoRouting(tmct0bok, "SIBBAC20");
    }
    // TODO DESCOMENTAR
    // catch (SIBBACBusinessException e) {
    // throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(),
    // tmct0bok.getId().getNbooking(), "", "",
    // String.format("%s %s",
    // "INCIDENCIA-Cargando titulares extranjero no routing.",
    // e.getMessage()), e);
    // }
    catch (Exception e) {
      LOG.warn("INCIDENCIA carga inicial no routing de internacional {}", e.getMessage(), e);
    }

    LOG.trace("[xml_mapping] Internacional-NO ROUTING actualiznado estado a FOREING_PENDIENTE_RECONCILIAR...");
    tmct0bokBo.updateCdestadoInternacional(tmct0bok, TypeCdestado.FOREING_PENDIENTE_RECONCILIAR, "SIBBAC20");
  }

  private void mappDynamicValues(OrderType orderType, Tmct0ord tmct0ord) {
    Tmct0ordDynamicValues dynamic;
    Tmct0ordDynamicValuesId id;
    if (orderType.getDynamicValues() != null && orderType.getDynamicValues().getDynamicValues() != null) {

      for (DynamicValue value : orderType.getDynamicValues().getDynamicValues()) {

        id = new Tmct0ordDynamicValuesId(tmct0ord.getNuorden(), value.getName());
        dynamic = tmct0ordDynamicValuesDao.findOne(id);

        LOG.trace("[mappDynamicValues] recepcion dynamic value id: {} valor:{}-{}", id, value.getValue(),
            value.getValueSt());

        if (dynamic == null) {
          dynamic = new Tmct0ordDynamicValues();
          dynamic.setId(id);
          tmct0ord.getDynamicValues().add(dynamic);
        }
        else {
          LOG.warn(
              "[mappDynamicValues] encontrado dynamic value existente id: {} nombre: {} valor old: {}-{} valor new: {}-{}",
              id, value.getName(), dynamic.getValor(), dynamic.getValorSt(), value.getValue(), value.getValueSt());
        }
        dynamic.setValorSt(value.getValueSt());
        if (value.getValue() != null) {
          // if (dynamic.getValor() != null) {
          // dynamic.setValor(dynamic.getValor().add(BigDecimal.valueOf(value.getValue())));
          // }
          // else {
          dynamic.setValor(BigDecimal.valueOf(value.getValue()));
          // }
        }
        dynamic.setAuditDate(new java.util.Date());
        dynamic.setAuditUser("SIBBAC20");
      }
    }
  }

}
