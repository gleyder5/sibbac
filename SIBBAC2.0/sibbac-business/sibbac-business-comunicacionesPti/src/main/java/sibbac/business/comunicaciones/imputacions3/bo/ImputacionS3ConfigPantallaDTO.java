package sibbac.business.comunicaciones.imputacions3.bo;

import java.io.Serializable;

public class ImputacionS3ConfigPantallaDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1045170789464998429L;
  
  private String cuentaCompensacionDestinoS3;
  private String horaInicioImputacionS3PtiMab;
  private String horaFinalImputacionS3PtiMab;
  private String horaInicioImputacionS3PTiNoMab;
  private String horaFinalImputacionS3PTiNoMab;
  private String horaInicioImputacionS3EuroCcp;
  private String horaFinalImputacionS3EuroCcp;

  public String getCuentaCompensacionDestinoS3() {
    return cuentaCompensacionDestinoS3;
  }

  public String getHoraInicioImputacionS3PtiMab() {
    return horaInicioImputacionS3PtiMab;
  }

  public String getHoraFinalImputacionS3PtiMab() {
    return horaFinalImputacionS3PtiMab;
  }

  public String getHoraInicioImputacionS3PTiNoMab() {
    return horaInicioImputacionS3PTiNoMab;
  }

  public String getHoraFinalImputacionS3PTiNoMab() {
    return horaFinalImputacionS3PTiNoMab;
  }

  public String getHoraInicioImputacionS3EuroCcp() {
    return horaInicioImputacionS3EuroCcp;
  }

  public String getHoraFinalImputacionS3EuroCcp() {
    return horaFinalImputacionS3EuroCcp;
  }

  public void setCuentaCompensacionDestinoS3(String cuentaCompensacionDestinoS3) {
    this.cuentaCompensacionDestinoS3 = cuentaCompensacionDestinoS3;
  }

  public void setHoraInicioImputacionS3PtiMab(String horaInicioImputacionS3PtiMab) {
    this.horaInicioImputacionS3PtiMab = horaInicioImputacionS3PtiMab;
  }

  public void setHoraFinalImputacionS3PtiMab(String horaFinalImputacionS3PtiMab) {
    this.horaFinalImputacionS3PtiMab = horaFinalImputacionS3PtiMab;
  }

  public void setHoraInicioImputacionS3PTiNoMab(String horaInicioImputacionS3PTiNoMab) {
    this.horaInicioImputacionS3PTiNoMab = horaInicioImputacionS3PTiNoMab;
  }

  public void setHoraFinalImputacionS3PTiNoMab(String horaFinalImputacionS3PTiNoMab) {
    this.horaFinalImputacionS3PTiNoMab = horaFinalImputacionS3PTiNoMab;
  }

  public void setHoraInicioImputacionS3EuroCcp(String horaInicioImputacionS3EuroCcp) {
    this.horaInicioImputacionS3EuroCcp = horaInicioImputacionS3EuroCcp;
  }

  public void setHoraFinalImputacionS3EuroCcp(String horaFinalImputacionS3EuroCcp) {
    this.horaFinalImputacionS3EuroCcp = horaFinalImputacionS3EuroCcp;
  }

}
