package sibbac.business.comunicaciones.euroccp.titulares.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;

@Component
public interface EuroCcpFileSentDao extends JpaRepository<EuroCcpFileSent, Long> {

  EuroCcpFileSent findByFileName(String fileName);

  EuroCcpFileSent findByFileBaseName(String fileBaseName);

  @Query("SELECT s FROM EuroCcpFileSent s WHERE s.sentDate = :psentDate AND s.fileType.cdCodigo = :pFileType"
      + " AND s.sequenceNumber = :pSequenceNumber AND s.clientNumber = :pclientNumber  ")
  EuroCcpFileSent findBySentDateAndFileTypeAndSequenceNumberAndClientNumber(@Param("psentDate") Date sentDate,
      @Param("pFileType") String fileType, @Param("pSequenceNumber") int sequenceNumber,
      @Param("pclientNumber") String clientNumber);

  List<EuroCcpFileSent> findAllByEstadoProcesado(int estadoProcesado);

  @Query("SELECT s FROM EuroCcpFileSent s WHERE s.estadoProcesado = :pEstadoProcesado "
      + " AND s.fileType.cdCodigo = :pFileType ORDER BY s.id ASC ")
  List<EuroCcpFileSent> findAllByEstadoProcesadoAndFileType(@Param("pEstadoProcesado") int estadoProcesado,
      @Param("pFileType") String fileType);

  @Query("SELECT MAX(s.sequenceNumber) FROM EuroCcpFileSent s WHERE s.sentDate = :psentDate AND s.fileType.cdCodigo = :pFileType ")
  Integer findMaxSequenceNumberBySentDateAndFileType(@Param("psentDate") Date sentDate,
      @Param("pFileType") String fileType);

  @Query("SELECT s FROM EuroCcpFileSent s WHERE s.sentDate = :psentDate AND s.fileType.cdCodigo = :pFileType "
      + " AND s.estadoProcesado = :pEstadoProcesado ORDER BY s.id desc ")
  List<EuroCcpFileSent> findAllBySentDateAndFileTypeAndEstadoProcesado(@Param("psentDate") Date sentDate,
      @Param("pFileType") String fileType, @Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT s FROM EuroCcpFileSent s WHERE s.sentDate = :psentDate AND s.fileType.cdCodigo = :pFileType "
      + " AND s.estadoProcesado = :pEstadoProcesado AND s.clientNumber = :pclientNumber " + " ORDER BY s.id desc ")
  List<EuroCcpFileSent> findAllBySentDateAndFileTypeAndEstadoProcesadoAndClientNumber(
      @Param("psentDate") Date sentDate, @Param("pFileType") String fileType,
      @Param("pEstadoProcesado") int estadoProcesado, @Param("pclientNumber") String clientNumber);

  @Query("SELECT COUNT(s) FROM EuroCcpFileSent s WHERE s.sentDate = :psentDate AND s.fileType.cdCodigo = :pFileType "
      + " AND s.estadoProcesado = :pEstadoProcesado AND s.clientNumber = :pclientNumber ")
  Integer countAllBySentDateAndFileTypeAndEstadoProcesado(@Param("psentDate") Date sentDate,
      @Param("pFileType") String fileType, @Param("pEstadoProcesado") int estadoProcesado,
      @Param("pclientNumber") String clientNumber);

  @Query("SELECT s FROM EuroCcpFileSent s WHERE s.parentFile.id = :pIdParentFile ORDER BY s.id ASC ")
  List<EuroCcpFileSent> findAllByIdParentFile(@Param("pIdParentFile") long idParentFile);

  @Query("SELECT s FROM EuroCcpFileSent s WHERE s.parentFile.id = :pIdParentFile "
      + " AND s.estadoProcesado = :pEstadoProcesado ORDER BY s.id ASC ")
  List<EuroCcpFileSent> findAllByIdParentFileAndEstadoProcesado(@Param("pIdParentFile") long idParentFile,
      @Param("pEstadoProcesado") int estadoProcesado);

}
