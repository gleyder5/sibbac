package sibbac.business.comunicaciones.rest;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.comunicaciones.commons.dto.PeticionTraspasoEccDTO;
import sibbac.business.comunicaciones.ordenes.bo.RejectOrdenFidessaMultiThreadBo;
import sibbac.business.comunicaciones.page.query.BookingsRechazablesPageQuery;
import sibbac.business.comunicaciones.page.query.DesglosesclititPendienteActualizarCodigosOperacionPageQuery;
import sibbac.business.comunicaciones.page.query.DesglosesclititTraspasosPageQuery;
import sibbac.business.comunicaciones.page.query.MovimientosEntradaPdteAceptarTraspaso;
import sibbac.business.comunicaciones.page.query.MovimientosSalidaPdteAceptarTraspaso;
import sibbac.business.comunicaciones.page.query.Tmct0alcByInformacionCompensacionTraspasosPageQuery;
import sibbac.business.comunicaciones.pti.ac.bo.AceptacionRechazoMovimientosPtiWithUserLockBo;
import sibbac.business.comunicaciones.pti.ac.dto.AceptacionRechazoMovimientosDTO;
import sibbac.business.comunicaciones.traspasos.bo.TraspasosEccMultithreadBo;
import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.FilterInfo;
import sibbac.database.bo.QueryBo;
import sibbac.database.toformat.JDBCtoFormat;
import sibbac.webapp.security.session.UserSession;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

@RestController
@RequestMapping("/traspasos/ecc")
public class SIBBACServiceTraspasosEcc {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceTraspasosEcc.class);

  private static final String JSON_UTF8 = "application/json;charset=UTF-8";

  private static final String SUBQUERY_CLASS_PREFIX = "%sMovimientosEntradaPageQuery";

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private HttpSession session;

  /**
   * Realiza los traspasos de salida a la ecc
   * */
  @Autowired
  private TraspasosEccMultithreadBo traspasosMultithreadBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentasDeCompensacionBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;

  @Autowired
  private Tmct0ValoresBo valoresBo;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private RejectOrdenFidessaMultiThreadBo rejectOrdenesBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0estadoBo estadoBo;
  
  @Autowired
  private QueryBo queryBo;

  /**
   * Permite mandar la aceptacion o el rechazo de un movimiento realizado en pti
   * */
  @Autowired
  private AceptacionRechazoMovimientosPtiWithUserLockBo aceptacioRechazoMovimientosPtiBo;

  @RequestMapping(produces = JSON_UTF8, method = RequestMethod.GET)
  public void getQueries(HttpServletResponse servletResponse) throws IOException {
    servletResponse.setCharacterEncoding("UTF-8");
    servletResponse.setContentType("application/json;charset=UTF-8");
    try (JsonGenerator writer = new JsonFactory().createGenerator(servletResponse.getWriter())) {
      writer.writeStartArray();
      writeQuery(writer, BookingsRechazablesPageQuery.class.getSimpleName(), "Bookings Pdte. Rechazar",
          "bookingsRechazables");
      writeQuery(writer, Tmct0alcByInformacionCompensacionTraspasosPageQuery.class.getSimpleName(),
          "Crear Traspasos Desglose", "traspasoAlcCuenta");
      writeQuery(writer, DesglosesclititTraspasosPageQuery.class.getSimpleName(), "Crear Traspasos Ejecución",
          "trapasoDesgloseClitit");
      writeQuery(writer, MovimientosEntradaPdteAceptarTraspaso.class.getSimpleName(),
          "Traspasos Entrada Pdte. Aceptar/Rechazar", "traspasoEntrada");
      writeQuery(writer, MovimientosSalidaPdteAceptarTraspaso.class.getSimpleName(),
          "Trapasos Salida Pdte. Aceptar Miembro Destino", "traspasoSalidaPdteAceptar");
      writeQuery(writer, DesglosesclititPendienteActualizarCodigosOperacionPageQuery.class.getSimpleName(),
          "Traspasos Fuera de Hora", "desglosesPendienteActualizarCodigosOperacionEcc");
      writer.writeEndArray();
    }
  }

  @RequestMapping(value = "/{name}/filters", produces = JSON_UTF8, method = RequestMethod.GET)
  public List<FilterInfo> getFilters(@PathVariable("name") String name) throws Exception {
    final AbstractDynamicPageQuery query;

    query = getDynamicQuery(name);
    return query.getFiltersInfo();
  }

  @RequestMapping(value = "/{name}/columns", produces = JSON_UTF8, method = RequestMethod.GET)
  public List<DynamicColumn> getColumns(@PathVariable("name") String name) throws Exception {
    final AbstractDynamicPageQuery query;

    query = getDynamicQuery(name);
    return query.getColumns();
  }

  @RequestMapping(value = "/{name}", method = RequestMethod.GET)
  public void executeQuery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws Exception {
    final AbstractDynamicPageQuery adpq;
    final StreamResult streamResult;

    adpq = getFilledDynamicQuery(name, request.getParameterMap());
    adpq.setIsoFormat(true);
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.JSON_OBJECTS, streamResult);
  }

  @RequestMapping(value = "/{name}/excel", method = RequestMethod.GET)
  public void executeExcelQuery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws Exception {
    final StreamResult streamResult;
    final AbstractDynamicPageQuery adpq;
    
    adpq = getFilledDynamicQuery(name, request.getParameterMap());
    adpq.setSingleFilter(getSingleFilter(request.getParameterMap()));
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.EXCEL, streamResult);
  }

  @RequestMapping(value = "/{name}/csv", method = RequestMethod.GET)
  public void executeCSVQuery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws Exception {
    final StreamResult streamResult;
    final AbstractDynamicPageQuery adpq;
    
    adpq = getFilledDynamicQuery(name, request.getParameterMap());
    adpq.setSingleFilter(getSingleFilter(request.getParameterMap()));
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.CSV, streamResult);
  }

  @RequestMapping(value = "/autocompleter/cuentaCompensacion", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<LabelValueObject> cuentaCompensacionList() {
    return cuentasDeCompensacionBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/miembroCompensador", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<LabelValueObject> miembroCompensadorList() {
    return compensadorBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/isin", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<LabelValueObject> isinList() {
    return valoresBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/alias", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<LabelValueObject> aliasList() {
    return aliBo.labelValueList();
  }

  @RequestMapping(value = "/subquery/{name}", method = RequestMethod.GET)
  public void executeSubquery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws Exception {
    final AbstractDynamicPageQuery adpq;
    final String className;
    final StreamResult streamResult;

    className = String.format(SUBQUERY_CLASS_PREFIX, name);
    adpq = getFilledDynamicQuery(className, request.getParameterMap());
    adpq.setIsoFormat(true);
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.JSON_OBJECTS, streamResult);
  }

  @RequestMapping(value = "/subquery/{name}/excel", method = RequestMethod.GET)
  public void executeExcelSubquery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws Exception {
    final AbstractDynamicPageQuery adpq;
    final String className;
    final StreamResult streamResult;

    className = String.format(SUBQUERY_CLASS_PREFIX, name);
    adpq = getFilledDynamicQuery(className, request.getParameterMap());
    adpq.setSingleFilter(getSingleFilter(request.getParameterMap()));
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.EXCEL, streamResult);
  }

  @RequestMapping(value = "/subquery/{name}/csv", method = RequestMethod.GET)
  public void executeCSVSubquery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws Exception {
    final AbstractDynamicPageQuery adpq;
    final String className;
    final StreamResult streamResult;

    className = String.format(SUBQUERY_CLASS_PREFIX, name);
    adpq = getFilledDynamicQuery(className, request.getParameterMap());
    adpq.setSingleFilter(getSingleFilter(request.getParameterMap()));
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.CSV, streamResult);
  }

  @RequestMapping(value = "/subquery/{name}/columns", produces = JSON_UTF8, method = RequestMethod.GET)
  public List<DynamicColumn> getSubqueryColumns(@PathVariable("name") String name) throws Exception {
    final String className;

    className = String.format(SUBQUERY_CLASS_PREFIX, name);
    return getDynamicQuery(className).getColumns();
  }

  @RequestMapping(value = "/{name}", consumes = JSON_UTF8, method = RequestMethod.POST)
  public CallableResultDTO executeAction(@PathVariable("name") String name,
      @RequestBody PeticionTraspasoEccDTO solicitud) {
    final CallableResultDTO result;
    String usuarioAuditoria = "traspaso";

    SimpleDateFormat sdf = new SimpleDateFormat(JDBCtoFormat.ISO_DATE_FORMAT);
    String cdrefmov;
    Date fliquidacion;
    String ctaDestino;
    String miembroDestino;
    String referenciaGiveUp;

    String rejectComment;
    List<AceptacionRechazoMovimientosDTO> listCdrefmovimientoEcc;
    List<Tmct0bokId> listBookings;
    List<Tmct0alcId> listAlcIds;
    Tmct0bokId bookId;
    Tmct0alcId alcId;

    List<Long> listNudesgloses;
    List<String> listCdoperacionecc;
    TraspasosEccConfigDTO config;

    LOG.debug("Accion: [{}], con parametros [{}] e {} items", name, solicitud.getParams(), solicitud.getItems().size());
    result = new CallableResultDTO();
    try {
      usuarioAuditoria = StringUtils.substring(((UserSession) session.getAttribute("UserSession")).getName(), 0, 10);
      if (solicitud != null) {
        switch (name) {
          case "bookingsRechazables":
            listBookings = new ArrayList<Tmct0bokId>();

            rejectComment = (String) solicitud.getParams().get("rejectComment");

            for (Map<String, Object> map : solicitud.getItems()) {
              bookId = new Tmct0bokId((String) map.get("NBOOKING"), (String) map.get("NUORDEN"));
              if (!listBookings.contains(bookId)) {
                listBookings.add(bookId);
              }
            }
            if (StringUtils.isBlank(rejectComment)) {
              rejectComment = "Manual reject";
            }
            return rejectOrdenesBo.rejectBookings(listBookings, rejectComment, usuarioAuditoria);
          case "trapasoDesgloseClitit":
            // salida
          case "traspasoAlcCuenta":
            // salida
            listAlcIds = new ArrayList<Tmct0alcId>();
            listNudesgloses = new ArrayList<Long>();
            listCdoperacionecc = new ArrayList<String>();
            ctaDestino = (String) solicitud.getParams().get("cuentaCompensacion");
            miembroDestino = (String) solicitud.getParams().get("miembroCompensador");
            referenciaGiveUp = (String) solicitud.getParams().get("refGiveUp");
            for (Map<String, Object> map : solicitud.getItems()) {
              alcId = new Tmct0alcId((String) map.get("NBOOKING"), (String) map.get("NUORDEN"),
                  map.get("NUCNFLIQ") != null ? new Short(map.get("NUCNFLIQ").toString()) : (short) 0,
                  (String) map.get("NUCNFCLT"));
              listAlcIds.add(alcId);

              if (name.equals("trapasoDesgloseClitit")) {
                if (StringUtils.isNotBlank((String) map.get("NUDESGLOSE"))) {
                  listNudesgloses.add(Long.valueOf((String) map.get("NUDESGLOSE")));
                }

                if (StringUtils.isNotBlank((String) map.get("CDOPERACIONECC_DESGLOSE"))) {
                  listCdoperacionecc.add((String) map.get("CDOPERACIONECC_DESGLOSE"));
                }
              }

            }

            if (CollectionUtils.isNotEmpty(listAlcIds)) {
              try {
                config = traspasosMultithreadBo.getNewConfig(listNudesgloses, listCdoperacionecc, null, ctaDestino,
                    miembroDestino, referenciaGiveUp, usuarioAuditoria);
              }
              catch (Exception e) {
                throw new SIBBACBusinessException(String.format(
                    "Incidencia recuperando la configuracion necesaria para hacer el traspaso %s", e.getMessage()), e);
              }
              return traspasosMultithreadBo.crearTraspasosMultithread(listAlcIds, config);
            }
            break;
          case "traspasoEntrada":

            listCdrefmovimientoEcc = new ArrayList<AceptacionRechazoMovimientosDTO>();
            for (Map<String, Object> map : solicitud.getItems()) {
              cdrefmov = (String) map.get("CDREFMOVIMIENTOECC");
              fliquidacion = (map.get("FEVALOR") != null ? sdf.parse((String) map.get("FEVALOR")) : null);
              if (StringUtils.isNotBlank(cdrefmov) && fliquidacion != null) {
                listCdrefmovimientoEcc.add(new AceptacionRechazoMovimientosDTO(cdrefmov, fliquidacion));
              }
            }
            if (CollectionUtils.isNotEmpty(listCdrefmovimientoEcc)) {
              if ((boolean) solicitud.getParams().get("acepta")) {
                ctaDestino = (String) solicitud.getParams().get("cuentaCompensacion");
                result.addAllMessage(aceptacioRechazoMovimientosPtiBo
                    .enviarAceptacionMovimientosEntradaByCdrefmovimientoecc(listCdrefmovimientoEcc, ctaDestino,
                        usuarioAuditoria));
              }
              else {
                result.addAllMessage(aceptacioRechazoMovimientosPtiBo
                    .enviarRechazoMovimientosEntradaByCdrefmovimientoecc(listCdrefmovimientoEcc, usuarioAuditoria));
              }
            }

            // entrada
            break;
          case "traspasoSalidaPdteAceptar":
            // salida
            listCdrefmovimientoEcc = new ArrayList<AceptacionRechazoMovimientosDTO>();
            for (Map<String, Object> map : solicitud.getItems()) {
              cdrefmov = (String) map.get("CDREFMOVIMIENTOECC");
              fliquidacion = (map.get("FEVALOR") != null ? sdf.parse((String) map.get("FEVALOR")) : null);
              if (StringUtils.isNotBlank(cdrefmov) && fliquidacion != null) {
                listCdrefmovimientoEcc.add(new AceptacionRechazoMovimientosDTO(cdrefmov, fliquidacion));
              }
            }
            result.addAllMessage(aceptacioRechazoMovimientosPtiBo
                .enviarCancelacionMovimientosSalidaByCdrefmovimientoecc(listCdrefmovimientoEcc, usuarioAuditoria));
            break;
          case "desglosesPendienteActualizarCodigosOperacionEcc":
            listBookings = new ArrayList<Tmct0bokId>();
            for (Map<String, Object> map : solicitud.getItems()) {
              bookId = new Tmct0bokId((String) map.get("NBOOKING"), (String) map.get("NUORDEN"));
              if (!listBookings.contains(bookId)) {
                listBookings.add(bookId);
              }
            }

            if (CollectionUtils.isNotEmpty(listBookings)) {
              Tmct0estado est = estadoBo.findById(EstadosEnumerados.ASIGNACIONES.PDTE_ACTUALIZAR_CODIGOS_OPERACION
                  .getId());
              if (est == null) {
                result.addErrorMessage("No encontrado el estado necesario para el proceso");
              }
              else {
                for (Tmct0bokId bookId2 : listBookings) {
                  try {
                    bokBo.updateCdestadoasigDownAllNewTransaction(bookId2,
                        EstadosEnumerados.ASIGNACIONES.PDTE_ACTUALIZAR_CODIGOS_OPERACION.getId(), usuarioAuditoria);
                    result.addMessage(MessageFormat.format("[{0}-{1}] Cambiado a estado {2}", bookId2.getNuorden(),
                        bookId2.getNbooking(), est.getNombre()));
                  }
                  catch (Exception e) {
                    result.addErrorMessage(MessageFormat.format("[{0}-{1}] No se ha podido cambiar el estado a {2}",
                        bookId2.getNuorden(), bookId2.getNbooking(), est.getNombre()));
                  }
                }
              }
            }

            break;
          default:
            break;
        }
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.warn(e.getMessage(), e);
      result.addErrorMessage(e.getMessage());
    }
    catch (RuntimeException rex) {
      LOG.error("[executeAction] Error inesperado al intentar ejecutar la accion {}", name, rex);
      result.addErrorMessage("Error inesperado, consulte con administrador");
    }
    catch (ParseException e) {
      LOG.error("[executeAction] Error parseando fecha al intentar ejecutar la accion {}", name, e);
      result.addErrorMessage("Error parseando fecha al intentar ejecutar la accion");
    }
    return result;
  }

  private void writeQuery(JsonGenerator writer, String className, String desc, String accion) throws IOException {
    writer.writeStartObject();
    writer.writeFieldName("name");
    writer.writeString(className);
    writer.writeFieldName("desc");
    writer.writeString(desc);
    writer.writeFieldName("accion");
    writer.writeString(accion);
    writer.writeEndObject();
  }

  private AbstractDynamicPageQuery getDynamicQuery(String name) throws Exception {
    return ctx.getBean(name, AbstractDynamicPageQuery.class);
  }

  private AbstractDynamicPageQuery getFilledDynamicQuery(String name, Map<String, String[]> params) throws Exception {
    final AbstractDynamicPageQuery adpq;

    adpq = getDynamicQuery(name);
    adpq.fillDynamicQuery(params);
    return adpq;
  }

  private String getSingleFilter(Map<String, String[]> params) {
    final String[] values;

    if (params.containsKey("singleFilter")) {
      values = params.get("singleFilter");
      if (values.length > 0) {
        return values[0].toUpperCase();
      }
    }
    return null;
  }

}
