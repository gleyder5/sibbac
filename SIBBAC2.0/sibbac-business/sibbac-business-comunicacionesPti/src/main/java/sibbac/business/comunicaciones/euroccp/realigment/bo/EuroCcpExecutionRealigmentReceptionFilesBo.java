package sibbac.business.comunicaciones.euroccp.realigment.bo;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpResponseFileProcessBo;

@Service
public class EuroCcpExecutionRealigmentReceptionFilesBo extends EuroCcpResponseFileProcessBo {

  @Value("${sibbac.euroccp.realigment.erg.in.line.pattern}")
  private String linePattern;

  @Value("${sibbac.euroccp.realigment.erg.in.footer.pattern}")
  private String footerLinePattern;

  @Value("${sibbac.euroccp.realigment.erg.file.date.pattern}")
  private String fileDatePattern;

  @Value("${sibbac.euroccp.realigment.receive.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.euroccp.realigment.receive.pool.size}")
  private int threadSize;

  @Value("${sibbac.euroccp.realigment.erg.file.charset}")
  private Charset fileCharset;

  @Value("${sibbac.euroccp.realigment.erg.in.file.pattern}")
  private String inFilePattern;

  public EuroCcpExecutionRealigmentReceptionFilesBo() {
  }

  @Override
  public int getThreadSize() {
    return this.threadSize;
  }

  @Override
  public int getTransactionSize() {
    return this.transactionSize;
  }

  @Override
  public Charset getFileCharset() {
    return this.fileCharset;
  }

  @Override
  public String getLinePattern() {
    return this.linePattern;
  }

  @Override
  public String getFooterPattern() {
    return this.footerLinePattern;
  }

  @Override
  public String getFileDatePattern() {
    return this.fileDatePattern;
  }

  @Override
  public String getFilePattern() {
    return this.inFilePattern;
  }

}
