package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCppSpanishSettlementInstruction;

@Component
public interface EuroCppSpanishSettlementInstructionDao 
    extends JpaRepository<EuroCppSpanishSettlementInstruction, Long> {

}
