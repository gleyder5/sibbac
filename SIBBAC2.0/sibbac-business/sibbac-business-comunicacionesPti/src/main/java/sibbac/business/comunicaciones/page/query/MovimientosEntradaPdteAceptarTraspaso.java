package sibbac.business.comunicaciones.page.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.commons.enums.TipoMovimiento;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;

@Component("MovimientosEntradaPdteAceptarTraspaso")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MovimientosEntradaPdteAceptarTraspaso extends AbstractDynamicPageQuery {

  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("Movimiento", "CDREFMOVIMIENTOECC"),
      new DynamicColumn("Cámara", "CAMARA"), new DynamicColumn("Compensador Origen", "MIEMBRO_ORIGEN"),
      new DynamicColumn("Desc. Compensador Origen", "DESC_MIEMBRO_ORIGEN"), new DynamicColumn("Ref. Give-Up", "CD_REF_ASIGNACION"),
      new DynamicColumn("F. Contratación", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("F. Liquidación", "FEVALOR", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Sentido", "SENTIDO"), new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Desc. Isin", "DESC_ISIN"),
      new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Precio", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Efectivo", "EFECTIVO", DynamicColumn.ColumnType.N4) };

  public MovimientosEntradaPdteAceptarTraspaso() {
    super(new ArrayList<SimpleDynamicFilter<?>>(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    StringBuilder sb = new StringBuilder(
        "SELECT MF.CDREFMOVIMIENTOECC,  MF.CDCAMARA CAMARA, MF.MIEMBRO_ORIGEN, MORI.NB_DESCRIPCION DESC_MIEMBRO_ORIGEN, MF.CD_REF_ASIGNACION, MF.FCONTRATACION FETRADE, ");
    sb.append(" MF.FLIQUIDACION FEVALOR, MF.SENTIDO, MF.CDISIN ISIN, VAL.DESCRIPCION DESC_ISIN, SUM(MF.IMTITULOS) TITULOS, MF.PRECIO, SUM(MF.IMEFECTIVO) EFECTIVO");
    return sb.toString();
  }

  @Override
  public String getFrom() {
    String valorActivo = "'S'";
    StringBuilder sb = new StringBuilder(
        "FROM TMCT0MOVIMIENTOECC_FIDESSA MF LEFT OUTER JOIN TMCT0_COMPENSADOR MORI ON MF.MIEMBRO_ORIGEN=MORI.NB_NOMBRE ");
    sb.append(" LEFT OUTER JOIN TMCT0_VALORES VAL ON MF.CDISIN = VAL.CODIGO_DE_VALOR AND VAL.ACTIVO = {0}");

    return MessageFormat.format(sb.toString(), valorActivo);
  }

  @Override
  public String getWhere() {

    return String.format(" WHERE MF.CDTIPOMOV='%s' AND MF.CDESTADOMOV='%s' ", TipoMovimiento.TAKE_UP.text,
        EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text);
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    StringBuilder sb = new StringBuilder(
        "GROUP BY MF.CDREFMOVIMIENTOECC, MF.CDCAMARA, MF.MIEMBRO_ORIGEN, MORI.NB_DESCRIPCION, MF.CD_REF_ASIGNACION, ");
    sb.append("MF.FCONTRATACION, MF.FLIQUIDACION, MF.SENTIDO, MF.CDISIN, VAL.DESCRIPCION , MF.PRECIO");
    return sb.toString();
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }

}
