package sibbac.business.comunicaciones.page.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

@Component("DetalleMovimientosEntradaPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DetalleMovimientosEntradaPageQuery extends AbstractDynamicPageQuery {

  private static final String SELECT = "SELECT "
      + "M.CDCAMARA CAMARA, M.MIEMBRO_MERCADO, M.FCONTRATACION FETRADE, M.SENTIDO, M.CDISIN ISIN, M.PRECIO, SUM(M.IMTITULOS) TITULOS";
  
  private static final String FROM = "FROM TMCT0MOVIMIENTOECC_FIDESSA M ";
  
  private static final String GROUP_BY = "GROUP BY M.CDCAMARA, M.MIEMBRO_MERCADO, M.FCONTRATACION, M.SENTIDO, "
      + "M.CDISIN, M.PRECIO";
  
  private static final DynamicColumn[] COLUMNS = {
      new DynamicColumn("Cámara", "CAMARA"),
      new DynamicColumn("Miembro Mercado", "MIEMBRO_MERCADO"),
      new DynamicColumn("F. Contratacion", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Sentido","SENTIDO"),
      new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Precio", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT)
  };

  private static List<SimpleDynamicFilter<?>> makeFilters()  {
    final List<SimpleDynamicFilter<?>> res;
    
    res = new ArrayList<>();
    res.add(new StringDynamicFilter("CDREFMOVIMIENTOECC", "Movimiento"));
    return res;
  }
  
  public DetalleMovimientosEntradaPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return GROUP_BY;
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
  }
  

}
