package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class InformacionCompensacionDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1997249111731400055L;
  private final String miembroPropietarioCuentaCompensacion;
  private final String cuentaCompensacion;

  private final String miembroCompensador;
  private final String referenciaAsignacionExterna;

  public InformacionCompensacionDTO(String miembroPropietarioCuentaCompensacion,
                                    String cuentaCompensacion,
                                    String miembroCompensador,
                                    String referenciaAsignacionExterna) {
    super();
    this.miembroPropietarioCuentaCompensacion = miembroPropietarioCuentaCompensacion;
    this.cuentaCompensacion = cuentaCompensacion;
    this.miembroCompensador = miembroCompensador;
    this.referenciaAsignacionExterna = referenciaAsignacionExterna;
  }

  public String getMiembroPropietarioCuentaCompensacion() {
    return miembroPropietarioCuentaCompensacion;
  }

  public String getCuentaCompensacion() {
    return cuentaCompensacion;
  }

  public String getMiembroCompensador() {
    return miembroCompensador;
  }

  public String getReferenciaAsignacionExterna() {
    return referenciaAsignacionExterna;
  }

  public boolean isBlanck() {
    return StringUtils.isBlank(miembroPropietarioCuentaCompensacion) && StringUtils.isBlank(cuentaCompensacion)
           && StringUtils.isBlank(miembroCompensador) && StringUtils.isBlank(referenciaAsignacionExterna);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cuentaCompensacion == null) ? 0 : cuentaCompensacion.hashCode());
    result = prime * result + ((miembroCompensador == null) ? 0 : miembroCompensador.hashCode());
    result = prime * result
             + ((miembroPropietarioCuentaCompensacion == null) ? 0 : miembroPropietarioCuentaCompensacion.hashCode());
    result = prime * result + ((referenciaAsignacionExterna == null) ? 0 : referenciaAsignacionExterna.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    InformacionCompensacionDTO other = (InformacionCompensacionDTO) obj;
    if (cuentaCompensacion == null) {
      if (other.cuentaCompensacion != null)
        return false;
    } else if (!cuentaCompensacion.equals(other.cuentaCompensacion))
      return false;
    if (miembroCompensador == null) {
      if (other.miembroCompensador != null)
        return false;
    } else if (!miembroCompensador.equals(other.miembroCompensador))
      return false;
    if (miembroPropietarioCuentaCompensacion == null) {
      if (other.miembroPropietarioCuentaCompensacion != null)
        return false;
    } else if (!miembroPropietarioCuentaCompensacion.equals(other.miembroPropietarioCuentaCompensacion))
      return false;
    if (referenciaAsignacionExterna == null) {
      if (other.referenciaAsignacionExterna != null)
        return false;
    } else if (!referenciaAsignacionExterna.equals(other.referenciaAsignacionExterna))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "InformacionCompensacionDTO [miembroPropietarioCuentaCompensacion=" + miembroPropietarioCuentaCompensacion
           + ", cuentaCompensacion=" + cuentaCompensacion + ", miembroCompensador=" + miembroCompensador
           + ", referenciaAsignacionExterna=" + referenciaAsignacionExterna + "]";
  }

}
