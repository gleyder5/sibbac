package sibbac.business.comunicaciones.traspasos.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class TraspasosEccWithUserLockBo {

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private TraspasosEccBo traspasosEccBo;

  @Autowired
  private TraspasosEccNoTransaccionalBo traspasosNoTransaccionalEccBo;

  @Autowired
  private Tmct0logBo logBo;

  public TraspasosEccWithUserLockBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED, rollbackFor = Exception.class)
  public CallableResultDTO crearTraspasosWithUserLock(Tmct0bokId bookId, List<Tmct0alcId> listAlcId,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {

    boolean imLock = false;
    try {
      bokBo.bloquearBooking(bookId, config.getAuditUser());
      imLock = true;
      CallableResultDTO res = new CallableResultDTO();
      for (Tmct0alcId tmct0alcId : listAlcId) {
        res.append(traspasosEccBo.crearTraspasos(tmct0alcId, config));
      }
      return res;
    }
    catch (LockUserException e) {
      logBo.insertarRegistroNewTransaction(bookId.getNuorden(), bookId.getNbooking(), e.getMessage(), "",
          config.getAuditUser());
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    catch (SIBBACBusinessException e) {
      logBo.insertarRegistroNewTransaction(bookId.getNuorden(), bookId.getNbooking(), e.getMessage(), "",
          config.getAuditUser());
      throw e;
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(bookId, config.getAuditUser());
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED, rollbackFor = Exception.class)
  public CallableResultDTO crearAsignacionesWithUserLock(Tmct0bokId bookId, List<Tmct0alcId> listAlcId,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {

    boolean imLock = false;
    try {
      bokBo.bloquearBooking(bookId, config.getAuditUser());
      imLock = true;
      CallableResultDTO res = new CallableResultDTO();
      for (Tmct0alcId tmct0alcId : listAlcId) {
        res.append(traspasosNoTransaccionalEccBo.procesoEnvioAsignaciones(tmct0alcId, config));
      }
      return res;
    }
    catch (LockUserException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    catch (SIBBACBusinessException e) {
      throw e;
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(bookId, config.getAuditUser());
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED, rollbackFor = Exception.class)
  public CallableResultDTO crearAsignacionesSinTitularWithUserLock(Tmct0bokId bookId, List<Tmct0aloId> listAloId,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {

    boolean imLock = false;
    try {
      bokBo.bloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), config.getAuditUser());
      imLock = true;
      CallableResultDTO res = new CallableResultDTO();
      for (Tmct0aloId tmct0aloId : listAloId) {
        res.append(traspasosEccBo.crearTraspasosSinTitular(tmct0aloId, config));
      }
      return res;
    }
    catch (LockUserException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    catch (SIBBACBusinessException e) {
      throw e;
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), config.getAuditUser());
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
    }
  }
}
