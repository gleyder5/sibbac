package sibbac.business.desglose.commons;

import java.math.BigDecimal;

public class DesgloseAlcBajaDTO {
  
  private String nuorden, nbooking, nucnfclt;
  private short nucnfliq;
  private String titular;
  private String identificacionTitular;
  private BigDecimal titulos;
  private String estadoAsignacion, estadoTitularidad, estadoContabilidad, estadoEntregaRecepccion;
  

}
