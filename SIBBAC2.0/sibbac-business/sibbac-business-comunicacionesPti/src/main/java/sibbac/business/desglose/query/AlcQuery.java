package sibbac.business.desglose.query;

import java.util.Date;
import java.util.List;
import java.util.Map;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.DynamicColumn;
import sibbac.database.QueryFilter;

public class AlcQuery implements QueryFilter {
  
  private static final String SELECT = "SELECT * FROM Tmct0alc WHERE nuorden = '%s' "
      + "AND nbooking = '%s' AND nucnfclt = '%s'";

  private final Tmct0aloId aloId;
  
  public AlcQuery(Tmct0aloId aloId) {
    this.aloId = aloId;
  }

  @Override
  public String getCountQuery() {
    return null;
  }

  @Override
  public String getSelectQuery() throws SIBBACBusinessException {
    return String.format(SELECT, aloId.getNuorden().trim(), aloId.getNbooking().trim(), aloId.getNucnfclt().trim());
  }

  @Override
  public int setCountResults(Map<Date, Long> dates) {
    return 0;
  }

  @Override
  public boolean isCountNecessary() {
    return false;
  }

  @Override
  public boolean isIsoFormat() {
    return false;
  }

  @Override
  public String getSingleFilter() {
    return null;
  }

  @Override
  public List<DynamicColumn> getColumns() {
    return null;
  }

}
