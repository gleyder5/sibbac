package sibbac.business.comunicaciones.pti.mo.exceptions;

import sibbac.common.SIBBACBusinessException;

public class CodigosOperacionEccDesactualizadosException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = -670913067658700456L;

  public CodigosOperacionEccDesactualizadosException() {
  }

  public CodigosOperacionEccDesactualizadosException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public CodigosOperacionEccDesactualizadosException(String message, Throwable cause) {
    super(message, cause);
  }

  public CodigosOperacionEccDesactualizadosException(String message) {
    super(message);
  }

  public CodigosOperacionEccDesactualizadosException(Throwable cause) {
    super(cause);
  }

}
