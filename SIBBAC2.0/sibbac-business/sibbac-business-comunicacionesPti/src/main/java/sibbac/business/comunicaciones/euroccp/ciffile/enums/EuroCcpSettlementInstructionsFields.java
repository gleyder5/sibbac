package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpSettlementInstructionsFields implements WrapperFileReaderFieldEnumInterface {

  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  EXCHANGE_CODE_TRADE("exchangeCodeTrade", 4, 0, WrapperFileReaderFieldType.STRING),
  SYMBOL("symbol", 6, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  DELIVER_RECEIVE_CODE("deliverReceiveCode", 3, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_QUANTITY("transactionQuantity", 12, 2, WrapperFileReaderFieldType.NUMERIC),
  STAMP_DUTY_IND("stampDutyInd", 1, 0, WrapperFileReaderFieldType.CHAR),
  SETTLEMENT_AMOUNT("settlementAmount", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_AMOUNT_DC("settlementAmountDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  TRANSACTION_DATE("transactionDate", 8, 0, WrapperFileReaderFieldType.DATE),
  SETTLEMENT_DATE("settlementDate", 8, 0, WrapperFileReaderFieldType.DATE),
  ISIN_CODE("isinCode", 12, 0, WrapperFileReaderFieldType.STRING),
  SETTLEMENT_INSTRUCTION_REFERENCE("settlementInstructionReference", 9, 0, WrapperFileReaderFieldType.STRING),
  DEPOT_ID("depotId", 6, 0, WrapperFileReaderFieldType.STRING),
  PLACE_SAFEKEEPING("placeSafekeeping", 11, 0, WrapperFileReaderFieldType.STRING),
  PLACE_SETTLEMENT("placeSettlement", 11, 0, WrapperFileReaderFieldType.STRING),
  BUYER_SELLER_CONTEXT("buyerSellerContext", 8, 0, WrapperFileReaderFieldType.STRING),
  BUYER_SELLER_CODE("buyerSellerCode", 11, 0, WrapperFileReaderFieldType.STRING),
  BUYER_SELLER_ACCOUNT_CODE("buyerSellerAccountCode", 12, 0, WrapperFileReaderFieldType.STRING),
  REC_DEL_AGENT_CONTEXT("recDelAgentContext", 8, 0, WrapperFileReaderFieldType.STRING),
  REC_DEL_AGENT_CODE("recDelAgentCode", 11, 0, WrapperFileReaderFieldType.STRING),
  REC_DEL_AGENT_ACCOUNT_CODE("recDelAgentAccountCode", 12, 0, WrapperFileReaderFieldType.STRING),
  GSI_STATUS("gsiStatus", 9, 0, WrapperFileReaderFieldType.STRING),
  GSI_STATUS_REASON("gsiStatusReason", 9, 0, WrapperFileReaderFieldType.STRING),
  GSI_TYPE("gsiType", 2, 0, WrapperFileReaderFieldType.STRING),
  SEND_INDICATOR("sendIndicator", 1, 0, WrapperFileReaderFieldType.CHAR),
  ORIGINAL_INSTRUCTION_REFERENCE("originalInstructionReference", 9, 0, WrapperFileReaderFieldType.STRING),
  PREVIOUS_INSTRUCTION_REFERENCE("previousInstructionReference", 9, 0, WrapperFileReaderFieldType.STRING),
  INSTRUCTION_ID("instructionId", 35, 0, WrapperFileReaderFieldType.STRING),
  REFERENCE_CUSTODIAN("referenceCustodian", 35, 0, WrapperFileReaderFieldType.STRING),
  AVERAGE_PRICE("averagePrice", 18, 7, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_FEE("settlementFee", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  SETTLEMENT_FEE_DC("settlementFeeDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  SETTLEMENT_FEE_CURRENCY_CODE("settlementFeeCurrencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  FAIL_FEE("failFee", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  FAIL_FEE_DC("failFeeDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  FAIL_FEE_CURRENCY_CODE("failFeeCurrencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  TYPE("typeField", 1, 0, WrapperFileReaderFieldType.CHAR),
  EXPIRATION_DATE("expirationDate", 8, 0, WrapperFileReaderFieldType.DATE),
  EXERCISE_PRICE("exercisePrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  FILLER("filler", 95, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpSettlementInstructionsFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
