package sibbac.business.comunicaciones.pti.cuadreecc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

@Table(name = DBConstants.CUADRE_ECC.CUADRE_ECC_LOG)
@Entity
@XmlRootElement
public class CuadreEccLog extends ATable<CuadreEccLog> implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6171490089661222612L;

  @XmlAttribute
  @Column(name = "ESTADO", nullable = false)
  private Integer estado = 0;

  @XmlAttribute
  @Column(name = "CDALIAS", nullable = true, length = 32)
  private String cdalias;

  /**
   * Numero de operacion de la ECC o del DCV segun corresponda
   */
  @Column(name = "NUM_OP", nullable = true, length = 35)
  @XmlAttribute
  private String numOp;

  /**
   * campo de fecha de ejecucion - formato AAAA-MM-DD
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_EJE", nullable = true)
  @XmlAttribute
  private Date fechaEje;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_LIQ")
  @XmlAttribute
  private Date fechaLiq;

  @Column(name = "ID_ECC", length = 11)
  @XmlAttribute
  private String idEcc;

  @Column(name = "SEGMENTO_ECC", length = 2)
  @XmlAttribute
  private String segmentoEcc;

  @Column(name = "ISIN", length = 12)
  @XmlAttribute
  private String isin;

  @Column(name = "SENTIDO", length = 1)
  @XmlAttribute
  private Character sentido;

  @Column(name = "PLATAFORMA_NEG", length = 4)
  @XmlAttribute
  private String plataformaNeg;

  @Column(name = "SEGMENTO_NEG", length = 2)
  @XmlAttribute
  private String segmentoNeg;

  @Column(name = "COD_OP_MERCADO", length = 2)
  @XmlAttribute
  private String codOpMercado;

  @Column(name = "MIEMBRO_MERCADO", length = 4)
  @XmlAttribute
  private String miembroMercado;

  @Column(name = "NUM_ORDEN_MERCADO", length = 32)
  @XmlAttribute
  private String numOrdenMercado;

  @Column(name = "NUM_EJE_MERCADO", length = 32)
  @XmlAttribute
  private String numEjeMercado;

  @Column(name = "TITULOS", length = 18, scale = 4)
  @XmlAttribute
  private BigDecimal titulos;

  @Column(name = "PRECIO", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal precio;

  @Column(name = "ID_COD_ERROR")
  @XmlAttribute
  private Long idCodError;

  @Column(name = "COD_ERROR", length = 10)
  @XmlAttribute
  private String codError;

  @XmlAttribute
  @Column(name = "NUORDEN", nullable = true, length = 20)
  private String nuorden;

  @XmlAttribute
  @Column(name = "NBOOKING", nullable = true, length = 20)
  private String nbooking;

  @XmlAttribute
  @Column(name = "NUCNFCLT", nullable = true, length = 20)
  private String nucnfclt;

  @XmlAttribute
  @Column(name = "NUCNFLIQ", nullable = true, precision = 4)
  private short nucnfliq;

  @XmlAttribute
  @Column(name = "IDDESGLOSECAMARA", nullable = true)
  private Long iddesglosecamara;

  @XmlAttribute
  @Column(name = "NUDESGLOSE", nullable = true)
  private Long nudesglose;

  @XmlAttribute
  @Column(name = "MSG", nullable = true, length = 2048)
  private String msg;

  @XmlAttribute
  @Column(name = "ID_CUADRE_ECC_EJECUCION", nullable = true)
  private Long idCuadreEccEjecucion;

  @XmlAttribute
  @Column(name = "ID_CUADRE_ECC_SIBBAC", nullable = true)
  private Long idCuadreEccSibbac;

  @XmlAttribute
  @Column(name = "ID_CUADRE_ECC_REFERENCIA", nullable = true)
  private Long idCuadreEccReferencia;

  @XmlAttribute
  @Column(name = "ID_CUADRE_ECC_TITULAR", nullable = true)
  private Long idCuadreEccTitular;

  @Transient
  private CuadreEccEjecucion cuadreEccEjecucion;

  @Transient
  private CuadreEccReferencia cuadreEccReferencia;

  @Transient
  private CuadreEccTitular cuadreEccTitular;

  @Transient
  private CuadreEccSibbac cuadreEccSibbac;

  public CuadreEccLog() {
  }

  public Integer getEstado() {
    return estado;
  }

  public String getCdalias() {
    return cdalias;
  }

  public String getNumOp() {
    return numOp;
  }

  public Date getFechaEje() {
    return fechaEje;
  }

  public Date getFechaLiq() {
    return fechaLiq;
  }

  public String getIdEcc() {
    return idEcc;
  }

  public String getSegmentoEcc() {
    return segmentoEcc;
  }

  public String getIsin() {
    return isin;
  }

  public Character getSentido() {
    return sentido;
  }

  public String getPlataformaNeg() {
    return plataformaNeg;
  }

  public String getSegmentoNeg() {
    return segmentoNeg;
  }

  public String getCodOpMercado() {
    return codOpMercado;
  }

  public String getMiembroMercado() {
    return miembroMercado;
  }

  public String getNumOrdenMercado() {
    return numOrdenMercado;
  }

  public String getNumEjeMercado() {
    return numEjeMercado;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public Long getIdCodError() {
    return idCodError;
  }

  public String getCodError() {
    return codError;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public short getNucnfliq() {
    return nucnfliq;
  }

  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  public Long getNudesglose() {
    return nudesglose;
  }

  public String getMsg() {
    return msg;
  }

  public Long getIdCuadreEccEjecucion() {
    return idCuadreEccEjecucion;
  }

  public Long getIdCuadreEccSibbac() {
    return idCuadreEccSibbac;
  }

  public Long getIdCuadreEccReferencia() {
    return idCuadreEccReferencia;
  }

  public Long getIdCuadreEccTitular() {
    return idCuadreEccTitular;
  }

  public void setEstado(Integer estado) {
    this.estado = estado;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  public void setNumOp(String numOp) {
    this.numOp = numOp;
  }

  public void setFechaEje(Date fechaEje) {
    this.fechaEje = fechaEje;
  }

  public void setFechaLiq(Date fechaLiq) {
    this.fechaLiq = fechaLiq;
  }

  public void setIdEcc(String idEcc) {
    this.idEcc = idEcc;
  }

  public void setSegmentoEcc(String segmentoEcc) {
    this.segmentoEcc = segmentoEcc;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setPlataformaNeg(String plataformaNeg) {
    this.plataformaNeg = plataformaNeg;
  }

  public void setSegmentoNeg(String segmentoNeg) {
    this.segmentoNeg = segmentoNeg;
  }

  public void setCodOpMercado(String codOpMercado) {
    this.codOpMercado = codOpMercado;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  public void setNumOrdenMercado(String numOrdenMercado) {
    this.numOrdenMercado = numOrdenMercado;
  }

  public void setNumEjeMercado(String numEjeMercado) {
    this.numEjeMercado = numEjeMercado;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setIdCodError(Long idCodError) {
    this.idCodError = idCodError;
  }

  public void setCodError(String codError) {
    this.codError = codError;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNucnfliq(short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public void setIdCuadreEccEjecucion(Long idCuadreEccEjecucion) {
    this.idCuadreEccEjecucion = idCuadreEccEjecucion;
  }

  public void setIdCuadreEccSibbac(Long idCuadreEccSibbac) {
    this.idCuadreEccSibbac = idCuadreEccSibbac;
  }

  public void setIdCuadreEccReferencia(Long idCuadreEccReferencia) {
    this.idCuadreEccReferencia = idCuadreEccReferencia;
  }

  public void setIdCuadreEccTitular(Long idCuadreEccTitular) {
    this.idCuadreEccTitular = idCuadreEccTitular;
  }

  public CuadreEccEjecucion getCuadreEccEjecucion() {
    return cuadreEccEjecucion;
  }

  public CuadreEccReferencia getCuadreEccReferencia() {
    return cuadreEccReferencia;
  }

  public CuadreEccTitular getCuadreEccTitular() {
    return cuadreEccTitular;
  }

  public void setCuadreEccTitular(CuadreEccTitular cuadreEccTitular) {
    this.cuadreEccTitular = cuadreEccTitular;
  }

  public CuadreEccSibbac getCuadreEccSibbac() {
    return cuadreEccSibbac;
  }

  public void setCuadreEccEjecucion(CuadreEccEjecucion cuadreEccEjecucion) {
    this.cuadreEccEjecucion = cuadreEccEjecucion;
  }

  public void setCuadreEccReferencia(CuadreEccReferencia cuadreEccReferencia) {
    this.cuadreEccReferencia = cuadreEccReferencia;
  }

  public void setCuadreEccSibbac(CuadreEccSibbac cuadreEccSibbac) {
    this.cuadreEccSibbac = cuadreEccSibbac;
  }

  @Override
  public String toString() {
    return "CuadreEccLog [estado=" + estado + ", cdalias=" + cdalias + ", numOp=" + numOp + ", fechaEje=" + fechaEje
           + ", fechaLiq=" + fechaLiq + ", idEcc=" + idEcc + ", segmentoEcc=" + segmentoEcc + ", isin=" + isin
           + ", sentido=" + sentido + ", plataformaNeg=" + plataformaNeg + ", segmentoNeg=" + segmentoNeg
           + ", codOpMercado=" + codOpMercado + ", miembroMercado=" + miembroMercado + ", numOrdenMercado="
           + numOrdenMercado + ", numEjeMercado=" + numEjeMercado + ", titulos=" + titulos + ", precio=" + precio
           + ", idCodError=" + idCodError + ", codError=" + codError + ", nuorden=" + nuorden + ", nbooking="
           + nbooking + ", nucnfclt=" + nucnfclt + ", nucnfliq=" + nucnfliq + ", iddesglosecamara=" + iddesglosecamara
           + ", nudesglose=" + nudesglose + ", msg=" + msg + ", idCuadreEccEjecucion=" + idCuadreEccEjecucion
           + ", idCuadreEccSibbac=" + idCuadreEccSibbac + ", idCuadreEccReferencia=" + idCuadreEccReferencia
           + ", idCuadreEccTitular=" + idCuadreEccTitular + "]";
  }

}
