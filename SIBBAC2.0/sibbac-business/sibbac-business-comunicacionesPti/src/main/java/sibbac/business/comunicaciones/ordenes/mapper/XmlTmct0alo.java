package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenInternalReceptionException;
import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0grupoejecucionBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBicNameAllocation;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0aloDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0aloDynamicValuesId;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

import com.sv.sibbac.multicenter.AllcationClearer;
import com.sv.sibbac.multicenter.Allocation;
import com.sv.sibbac.multicenter.AllocationCustodian;
import com.sv.sibbac.multicenter.AllocationEconomicDataType;
import com.sv.sibbac.multicenter.AllocationID;
import com.sv.sibbac.multicenter.AllocationNet;
import com.sv.sibbac.multicenter.AllocationSentInfo;
import com.sv.sibbac.multicenter.BookingType;
import com.sv.sibbac.multicenter.Cargos;
import com.sv.sibbac.multicenter.Commission;
import com.sv.sibbac.multicenter.DynamicValue;
import com.sv.sibbac.multicenter.ExecutionGroupType;

@Service
public class XmlTmct0alo {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(XmlTmct0alo.class);

  @Autowired
  private XmlTmct0log xmlTmct0log;

  @Autowired
  private Tmct0brkBo brokerBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0actBo actBo;

  @Autowired
  private Tmct0tfiBo tfiBo;

  @Autowired
  private Tmct0grupoejecucionBo gruposBo;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private XmlGrupoEjecucion xmlGrupoEjecucion;

  public XmlTmct0alo() {
    super();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void xml_mapping(BookingType booking, Allocation allocation, Tmct0alo tmct0alo,
      Map<String, Tmct0eje> mExecution, CachedValuesLoadXmlOrdenDTO cachedConfig) throws XmlOrdenExceptionReception,
      XmlOrdenInternalReceptionException {
    /*
     * multicenter:Allocation
     */
    tmct0alo.getId().setNucnfclt(allocation.getNucnfclt());

    LOG.trace(" allocation.cdalias = " + allocation.getCdalias());
    // OPERATIVA NUEVA DESGLOSE LIQUIDADORA DESDE FIDESSA POR TITULAR
    if (allocation.getCdalias().toUpperCase().contains("|FINALHOLDERS")) {
      xmlTmct0log.xml_mapping(tmct0alo.getTmct0bok().getTmct0ord(), tmct0alo.getId().getNbooking(),
          String.format("Desglose por final holder: '%s'.", allocation.getCdalias()));
      try {
        tmct0alo.setCdalias(tmct0alo.getTmct0bok().getCdalias());
        String[] dataHolder = allocation.getCdalias().split("\\|");
        tmct0alo.setCdholder(dataHolder[0]);

      }
      catch (Exception e) {
        LOG.trace(e.getMessage(), e);
        tmct0alo.setCdalias(allocation.getCdalias());
      }
    }
    else {
      tmct0alo.setCdalias(allocation.getCdalias());
    }
    if (tmct0alo.getCdholder() == null) {
      tmct0alo.setCdholder("");
    }
    final String holderCode = tmct0alo.getCdholder().trim();
    BigDecimal feoperacBd;
    try {
      feoperacBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(tmct0alo.getTmct0bok()
          .getFecontra()));
    }
    catch (SIBBACBusinessException e1) {
      throw new XmlOrdenExceptionReception(tmct0alo.getTmct0bok().getTmct0ord().getNuorden(), tmct0alo.getId()
          .getNbooking(), "", "", "Fecontra booking no es correcta.", e1);
    }
    if (!"".equals(holderCode)) {
      // Tmct0fis holder = JdoGetObject.getFgrl0tit(holderCode, hpm);
      List<Holder> holders = cachedConfig.getHolders(tfiBo, holderCode, feoperacBd);
      if (CollectionUtils.isEmpty(holders)) {
        throw new XmlOrdenExceptionReception(tmct0alo.getTmct0bok().getTmct0ord().getNuorden(), tmct0alo.getId()
            .getNbooking(), "", "", String.format("Titular No encontrado  Titular: '%s'", holderCode));
      }
    }

    if (tmct0alo.getCdalias().trim().contains("|")) {
      if (!tmct0alo.getCdalias().trim().split("\\|")[1].equals(tmct0alo.getTmct0bok().getCdalias().trim())) {
        throw new XmlOrdenExceptionReception(tmct0alo.getTmct0bok().getTmct0ord().getNuorden(), tmct0alo.getId()
            .getNbooking(), "", "", "Alias no valido");
      }
    }
    else {
      if (!tmct0alo.getCdalias().trim().equals(tmct0alo.getTmct0bok().getCdalias().trim())) {
        throw new XmlOrdenExceptionReception(tmct0alo.getId().getNuorden(), tmct0alo.getId().getNbooking(), "", "",
            "Alias no valido");
      }
    }
    // Tmct0ord ord = tmct0alo.getTmct0bok().getTmct0ord();

    /*
     * Carga descrali Tabla Fmeg0ali clave primaria cdalias.
     */
    // BigDecimal fecha =
    // FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(tmct0alo
    // .getFeoperac()));
    // ClientDniName clientDniName =
    // settlementDataBo.getClientDniName(tmct0alo.getCdholder(), fecha);
    if (StringUtils.isNotBlank(allocation.getNbtitulr())) {
      tmct0alo.setDescrali(StringUtils.substring(allocation.getNbtitulr(), 0, 130));
    }
    else if (tmct0alo.getCdalias().contains("|")) {
      String descraliSubcuenta = cachedConfig.getDescraliSubcuenta(actBo, tmct0alo.getTmct0bok().getCdalias(), tmct0alo
          .getCdalias().split("\\|")[0], feoperacBd.intValue());
      if (StringUtils.isNotBlank(descraliSubcuenta)) {
        tmct0alo.setDescrali(descraliSubcuenta);
      }
    }

    if (StringUtils.isBlank(tmct0alo.getDescrali())) {
      tmct0alo.setDescrali(tmct0alo.getTmct0bok().getDescrali());
    }
    tmct0alo.setNbtitulr(allocation.getNbtitulr());
    tmct0alo.setNutitcli(new BigDecimal(allocation.getNutitcli()).setScale(5, RoundingMode.HALF_UP));
    tmct0alo.setNutitpen(new BigDecimal(allocation.getNutitcli()).setScale(5, RoundingMode.HALF_UP));
    tmct0alo.setFeoperac(new Date(allocation.getFeoperac().toGregorianCalendar().getTimeInMillis()));
    tmct0alo.setHooperac(new Time(allocation.getFeoperac().toGregorianCalendar().getTimeInMillis()));
    tmct0alo.setFevalor(new Date(allocation.getFevalor().toGregorianCalendar().getTimeInMillis()));
    tmct0alo.setFevalorr(null);
    tmct0alo.setCdusuges(allocation.getCdusuges());
    tmct0alo.setReftitular(allocation.getRefTitular());
    tmct0alo.setCdisin(tmct0alo.getTmct0bok().getCdisin());
    tmct0alo.setCdtpoper(tmct0alo.getTmct0bok().getCdtpoper());

    /*
     * Allocation setinfo
     */
    AllocationSentInfo allocationSentInfo = allocation.getConfirmationSent();
    tmct0alo.setCdenvcnf((allocationSentInfo.isCdenvcnf() ? 'S' : 'N'));
    tmct0alo.setCdenvseq((allocationSentInfo.isCdenvsec() ? 'S' : 'N'));

    tmct0alo.setFhenvseq(new Date(allocationSentInfo.getFhenvseq().toGregorianCalendar().getTimeInMillis()));
    tmct0alo.setHoenvseq(new Time(allocationSentInfo.getFhenvseq().toGregorianCalendar().getTimeInMillis()));
    tmct0alo.setCdenvoas((allocationSentInfo.isCdenvoas() ? 'S' : 'N'));
    if (StringUtils.isNotBlank(allocationSentInfo.getCdestoas())) {
      tmct0alo.setCdestoas(allocationSentInfo.getCdestoas().charAt(0));
    }
    else {
      tmct0alo.setCdestoas(' ');
    }
    tmct0alo.setCdrefoas(allocationSentInfo.getCdrefoas());
    tmct0alo.setFhenvoas(new Date(allocationSentInfo.getFhenvoas().toGregorianCalendar().getTimeInMillis()));
    tmct0alo.setHoenvoas(new Time(allocationSentInfo.getFhenvoas().toGregorianCalendar().getTimeInMillis()));

    /*
     * Allocation custodian
     */
    AllocationCustodian allocationCustodian = allocation.getCustodian();
    tmct0alo.setCdcustod(allocationCustodian.getCustodian());
    tmct0alo.setAcctatcus(allocationCustodian.getAcctat());
    tmct0alo.setCdbencus(allocationCustodian.getBenefcode());
    tmct0alo.setAcbencus(allocationCustodian.getBenefacct());
    tmct0alo.setIdbencus(allocationCustodian.getBenefid());
    tmct0alo.setGcuscode(allocationCustodian.getGcuscode());
    tmct0alo.setGcusacct(allocationCustodian.getGcusacct().toString());
    tmct0alo.setGcusid(allocationCustodian.getGcusid());
    tmct0alo.setLcuscode(allocationCustodian.getLcuscode());
    tmct0alo.setLcusacct(allocationCustodian.getLcusacct());
    tmct0alo.setLcusid(allocationCustodian.getLcusid());
    tmct0alo.setPccusset(allocationCustodian.getPcesett());
    tmct0alo.setSendbecus((allocationCustodian.isSendbenef() ? 'S' : 'N'));
    tmct0alo.setTpsetcus(allocationCustodian.getSetttyp().toString());
    tmct0alo.setTysetcus(allocationCustodian.getSettype().toString());
    boolean isLoadDatosLiquidacionInternacional = 'E' == tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo();
    if (isLoadDatosLiquidacionInternacional) {

      /*
       * Allocation clearer
       */
      AllcationClearer allocationClearer = allocation.getClearer();
      String clearer = allocationClearer.getClearer();
      if (StringUtils.isNotBlank(clearer) && StringUtils.isNumeric(clearer)) {
        clearer = allocationClearer.getClearer();
      }
      else {
        if ('E' == tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo()
            && ordBo.isRouting(tmct0alo.getTmct0bok().getTmct0ord()) && clearer.trim().isEmpty()) {
          clearer = "BCH.ESMM";
        }
        else if ('E' == tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo()
            && brokerBo.isCdbrokerMTF(tmct0alo.getTmct0bok().getTmct0ord().getCdbroker()) && clearer.trim().isEmpty()) {
          clearer = "0049";
        }

      }
      tmct0alo.setCdcleare(clearer);

      tmct0alo.setAcctatcle(allocationClearer.getAcctat());
      tmct0alo.setCdbencle(allocationClearer.getBenefcode());
      tmct0alo.setAcbencle(allocationClearer.getBenefacct());
      tmct0alo.setIdbencle(allocationClearer.getBenefid());
      tmct0alo.setGclecode(allocationClearer.getGcuscode());
      tmct0alo.setGcleacct(allocationClearer.getGcusacct().toString());
      tmct0alo.setGcleid(allocationClearer.getGcusid());
      tmct0alo.setLclecode(allocationClearer.getLcuscode());
      tmct0alo.setLcleacct(allocationClearer.getLcusacct());
      tmct0alo.setLcleid(allocationClearer.getLcusid());
      tmct0alo.setPccleset(allocationClearer.getPcesett());
      tmct0alo.setSendbecle((allocationClearer.isSendbenef() ? 'S' : 'N'));
      tmct0alo.setTpsetcle(allocationClearer.getSetttyp().toString());
      tmct0alo.setTysetcle(allocationClearer.getSettype().toString());

      try {
        SettlementChainBicNameAllocation bicNameDataCleCus = settlementDataBo
            .getSettlementChainBicNameAllocation(tmct0alo);

        /*
         * BIC Y NAME CLEARER
         */
        tmct0alo.setBiccle(bicNameDataCleCus.getSclbic().trim());
        tmct0alo.setNamecle(bicNameDataCleCus.getSclname().trim());
        tmct0alo.setLclebic(bicNameDataCleCus.getScllcbic().trim());
        tmct0alo.setCdloccle(bicNameDataCleCus.getScllcname().trim());
        tmct0alo.setGclebic(bicNameDataCleCus.getSclgcbic().trim());
        tmct0alo.setCdglocle(bicNameDataCleCus.getSclgcname().trim());
        tmct0alo.setBclebic(bicNameDataCleCus.getSclbebic().trim());
        tmct0alo.setCdnbcle(bicNameDataCleCus.getSclbename().trim());

        /*
         * BIC Y NAME CUSTODIAN
         */
        tmct0alo.setBiccus(bicNameDataCleCus.getScubic().trim());
        tmct0alo.setNamecus(bicNameDataCleCus.getScuname().trim());
        tmct0alo.setLcusbic(bicNameDataCleCus.getSculcbic().trim());
        tmct0alo.setCdloccus(bicNameDataCleCus.getSculcname().trim());
        tmct0alo.setGcusbic(bicNameDataCleCus.getScugcbic().trim());
        tmct0alo.setCdglocus(bicNameDataCleCus.getScugcname().trim());
        tmct0alo.setBcusbic(bicNameDataCleCus.getScubebic().trim());
        tmct0alo.setCdnbcus(bicNameDataCleCus.getScubename().trim());
      }
      catch (SIBBACBusinessException e) {
        throw new XmlOrdenExceptionReception(tmct0alo.getTmct0bok().getTmct0ord().getNuorden(), tmct0alo.getId()
            .getNbooking(), "", "", e.getMessage(), e);
      }
    }
    /*
     * EconomicDataType
     */
    AllocationEconomicDataType economicData = allocation.getEconomicDataAlloc();
    tmct0alo.setImtpcamb(new BigDecimal(economicData.getImtpcamb()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setCdmoniso(economicData.getCdmoniso().toString());

    /*
     * Net
     */
    AllocationNet net = economicData.getNet();
    tmct0alo.setImcammed(new BigDecimal(net.getPrecioBrutoClte()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImcamnet(new BigDecimal(net.getPrecioNetoClte()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImtotbru(new BigDecimal(net.getImBrutoClteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImtotnet(new BigDecimal(net.getImNetoClteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setEutotnet(new BigDecimal(net.getImNetoClteEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setEutotbru(new BigDecimal(net.getImBrutoClteEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setEunetbrk(new BigDecimal(net.getImNetoMktEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImnetbrk(new BigDecimal(net.getImNetoMktDv()).setScale(8, RoundingMode.HALF_UP));

    if (BigDecimal.ZERO.compareTo(tmct0alo.getImtotbru()) == 0) {
      throw new XmlOrdenExceptionReception(tmct0alo.getTmct0bok().getTmct0ord().getNuorden(), tmct0alo.getTmct0bok()
          .getId().getNbooking(), "", tmct0alo.getId().getNucnfclt(), "Importe Cliente Bruto total no puede ser 0.");
    }

    /*
     * commisions
     */
    Commission comisions = economicData.getCommision();
    tmct0alo.setImcomisn(new BigDecimal(comisions.getImComClteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImcomieu(new BigDecimal(comisions.getImComClteEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setPccomisn(new BigDecimal(comisions.getPcComClte()).setScale(6, RoundingMode.HALF_UP));
    tmct0alo.setImcomdev(new BigDecimal(comisions.getImComOrdenanteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setEucomdev(new BigDecimal(comisions.getImComOrdenanteEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImfibrdv(new BigDecimal(comisions.getImComBrkDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImfibreu(new BigDecimal(comisions.getImComBrkEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImsvb(new BigDecimal(comisions.getImCorretajeSVDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImsvbeu(new BigDecimal(comisions.getImCorretajeSVEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0alo.setImcorretajesvfinaldv(new BigDecimal(comisions.getImCorretajeSVFinalDv()).setScale(8,
        RoundingMode.HALF_UP));
    tmct0alo.setImcomclteeu(new BigDecimal(comisions.getImComClteEu()).setScale(8, RoundingMode.HALF_UP));

    /*
     * SpanishMarket or ForeingMarket
     */
    // Tarea 507 Eliminar Spanish/Foreing Market
    Cargos cargos = economicData.getMarketCharges().getCargos();
    tmct0alo.setXtdcm(' ');
    tmct0alo.setCdindimp(' ');
    if (cargos != null) {

      /*
       * multicenter:MarketCharges - SpanishMarket
       */
      tmct0alo.setImcliqeu(new BigDecimal(cargos.getImCanonLiqEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImcconeu(BigDecimal.ZERO.setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImdereeu(new BigDecimal(cargos.getImCanonContrEu()).setScale(8, RoundingMode.HALF_UP));

      /*
       * multicenter:MarketCharges - ForeingMarket
       */
      tmct0alo.setImbrmerc(new BigDecimal(cargos.getImBrutoMktDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImbrmreu(new BigDecimal(cargos.getImBrutoMktEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImgasliqdv(new BigDecimal(cargos.getImCanonLiqDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImgasliqeu(new BigDecimal(cargos.getImCanonLiqEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImgasgesdv(new BigDecimal(cargos.getImCanonContrDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImgasgeseu(new BigDecimal(cargos.getImCanonContrEu()).setScale(8, RoundingMode.HALF_UP));

      /*
       * HORNÉS :: Se añaden los campos necesarios para los cálculos económicos
       * :: Inicio
       */
      tmct0alo.setImcanoncontrdv(new BigDecimal(cargos.getImCanonContrDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImcanoncontreu(new BigDecimal(cargos.getImCanonContrEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImcanonliqdv(new BigDecimal(cargos.getImCanonLiqDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImcanonliqeu(new BigDecimal(cargos.getImCanonLiqEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImcanoncompdv(new BigDecimal(cargos.getImCanonCompDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setImcanoncompeu(new BigDecimal(cargos.getImCanonCompEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0alo.setCanoncontrpagoclte(cargos.isCanonContrPagoClte() ? 1 : 0);
      tmct0alo.setCanoncontrpagomkt(cargos.isCanonContrPagoMkt() ? 1 : 0);
      tmct0alo.setCanonliqpagoclte(cargos.isCanonLiqPagoClte() ? 1 : 0);
      tmct0alo.setCanonliqpagomkt(cargos.isCanonLiqPagoMkt() ? 1 : 0);
      tmct0alo.setCanoncomppagoclte(cargos.isCanonCompPagoClte() ? 1 : 0);
      tmct0alo.setCanoncomppagomkt(cargos.isCanonCompPagoMkt() ? 1 : 0);
      /*
       * HORNÉS :: Se añaden los campos necesarios para los cálculos económicos
       * :: Fin
       */
    }
    else {
      /*
       * multicenter:MarketCharges - SpanishMarket
       */
      tmct0alo.setImcliqeu(BigDecimal.ZERO);
      tmct0alo.setImcconeu(BigDecimal.ZERO);
      tmct0alo.setImdereeu(BigDecimal.ZERO);

      /*
       * multicenter:MarketCharges - ForeingMarket
       */
      tmct0alo.setImbrmerc(BigDecimal.ZERO);
      tmct0alo.setImbrmreu(BigDecimal.ZERO);
      tmct0alo.setImgasliqdv(BigDecimal.ZERO);
      tmct0alo.setImgasliqeu(BigDecimal.ZERO);

      tmct0alo.setImgasgesdv(BigDecimal.ZERO);
      tmct0alo.setImgasgeseu(BigDecimal.ZERO);

      /*
       * HORNÉS :: Se añaden los campos necesarios para los cálculos económicos
       * :: Inicio
       */
      tmct0alo.setImcanoncontrdv(BigDecimal.ZERO);
      tmct0alo.setImcanoncontreu(BigDecimal.ZERO);
      tmct0alo.setImcanonliqdv(BigDecimal.ZERO);
      tmct0alo.setImcanonliqeu(BigDecimal.ZERO);
      tmct0alo.setImcanoncompdv(BigDecimal.ZERO);
      tmct0alo.setImcanoncompeu(BigDecimal.ZERO);
      tmct0alo.setCanoncontrpagoclte(0);
      tmct0alo.setCanoncontrpagomkt(0);
      tmct0alo.setCanonliqpagoclte(0);
      tmct0alo.setCanonliqpagomkt(0);
      tmct0alo.setCanoncomppagoclte(0);
      tmct0alo.setCanoncomppagomkt(0);
      /*
       * HORNÉS :: Se añaden los campos necesarios para los cálculos económicos
       * :: Fin
       */
    }

    /*
     * campos no informados en xml
     */

    tmct0alo.setRfparten("");
    tmct0alo.setIndatfis('N');
    tmct0alo.setErdatfis("");

    tmct0alo.setCdbloque(' ');
    tmct0alo.setDsobserv("");
    tmct0alo.setCdusublo("");

    tmct0alo.setPkentity("");

    /*
     * campos de diferencias
     */
    tmct0alo.setImliq(BigDecimal.ZERO);
    tmct0alo.setImeurliq(BigDecimal.ZERO);
    tmct0alo.setImajliq(BigDecimal.ZERO);
    tmct0alo.setImajeliq(BigDecimal.ZERO);

    /*
     * cddatliq
     */
    if (isLoadDatosLiquidacionInternacional) {
      if ("".equals(tmct0alo.getCdcleare().trim()) || "".equals(tmct0alo.getTpsetcle().trim())
          || "".equals(tmct0alo.getLclecode().trim()) || "".equals(tmct0alo.getCdcustod().trim())
          || "".equals(tmct0alo.getTpsetcus().trim()) || "".equals(tmct0alo.getLcuscode().trim())) {
        tmct0alo.setCddatliq('N');
        tmct0alo.getTmct0bok().setCddatliq('N');
      }
      else {
        tmct0alo.setCddatliq('S');
      }
    }
    else {
      tmct0alo.setCddatliq('S');
      if (tmct0alo.getTmct0bok().getCddatliq() == null) {
        tmct0alo.getTmct0bok().setCddatliq('S');
      }
    }

    tmct0alo.setCdtpoper(tmct0alo.getTmct0bok().getCdtpoper());
    tmct0alo.setCdisin(tmct0alo.getTmct0bok().getCdisin());
    tmct0alo.setNbvalors(tmct0alo.getTmct0bok().getNbvalors());

    tmct0alo.setNuoprout(tmct0alo.getTmct0bok().getTmct0ord().getNuoprout());

    tmct0alo.setNuagrpkb(0);

    tmct0alo.setNupareje((short) 0);

    tmct0alo.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0alo.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);

    tmct0alo.setTmct0sta(tmct0alo.getTmct0bok().getTmct0sta());
    // si es nacional y el estado no es null y el estado es pdte enviar sin
    // titular, buscamos las ilq, si estan en
    // definitivo se enviara
    // el mo directamente si no en pdte usuario sin titular
    if ('N' == tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo()
        && tmct0alo.getTmct0bok().getTmct0estadoByCdestadoasig() != null
        && EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR_SIN_TITULAR.getId() == tmct0alo.getTmct0bok()
            .getTmct0estadoByCdestadoasig().getIdestado()) {
      try {
        // buscamos las ilq de nacional
        SettlementData stn = Objects.requireNonNull(settlementDataBo.getSettlementDataNacional(tmct0alo),
            "Instruciones de liquidacion para operacion sin titular requeridas");

        boolean isDesglose = stn.getTmct0ilq() != null && stn.getTmct0ilq().getDesglose() != null;
        if (isDesglose) {
          xmlTmct0log.xml_mapping(tmct0alo.getTmct0bok().getTmct0ord(), tmct0alo.getId().getNbooking(), String.format(
              "Position Break Down sin titular para el aliass '%s' is '%s'.", tmct0alo.getCdalias(), stn.getTmct0ilq()
                  .getDesglose()));
        }
        // si tenemos las ilq d enacional y estan en definitivo dejamos el
        // estado como pdte enviar
        if (isDesglose && stn.getTmct0ilq().getDesglose() != null && stn.getTmct0ilq().getDesglose() == 'D') {
          tmct0alo.setTmct0estadoByCdestadoasig(tmct0alo.getTmct0bok().getTmct0estadoByCdestadoasig());
          // si no estan en definitivo ponemos en pdte usuario y el booking en
          // curso por si hubiera mas de un allo y
          // tuvieran estados
          // diferentes.
        }
        else {
          tmct0alo.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR
              .getId()));
          if (tmct0alo.getTmct0bok().getTmct0estadoByCdestadoasig() != null
              && EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId() != tmct0alo.getTmct0bok()
                  .getTmct0estadoByCdestadoasig().getIdestado()) {
            tmct0alo.getTmct0bok().setTmct0estadoByCdestadoasig(
                new Tmct0estado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()));
          }
        }
      }
      catch (SIBBACBusinessException e) {
        throw new XmlOrdenInternalReceptionException(e.getMessage(), e);
      }

    }
    else {
      tmct0alo.setTmct0estadoByCdestadoasig(tmct0alo.getTmct0bok().getTmct0estadoByCdestadoasig());
    }
    tmct0alo.setCdestadotit(tmct0alo.getTmct0bok().getCdestadotit());
    tmct0alo.setTmct0estadoByCdestadocont(tmct0alo.getTmct0bok().getTmct0estadoByCdestadocont());

    // Se añade el campo refbanif
    LOG.trace(" allocation.refClteAlloc = {}", allocation.getRefClteAlloc());
    if (allocation.getRefClteAlloc() != null) {
      tmct0alo.setRefbanif(allocation.getRefClteAlloc());
    }
    else {
      tmct0alo.setRefbanif("");
    }
    LOG.trace(" allocation.ccv = {}", allocation.getCcvAlloc());
    if (allocation.getCcvAlloc() != null) {
      tmct0alo.setCcv(allocation.getCcvAlloc());
    }
    else {
      tmct0alo.setCcv("");
    }

    // 2015-08-18
    // Se añade indicador de gastos
    if (tmct0alo.getCanoncomppagoclte() != null && tmct0alo.getCanoncomppagoclte() == 1
        || tmct0alo.getCanoncontrpagoclte() != null && tmct0alo.getCanoncontrpagoclte() == 1
        || tmct0alo.getCanonliqpagoclte() != null && tmct0alo.getCanonliqpagoclte() == 1) {
      tmct0alo.setCdindgas('S');
    }
    else {
      tmct0alo.setCdindgas('N');
    }

    if (StringUtils.isNotBlank(tmct0alo.getRefbanif())) {
      tmct0alo.setRefCliente(tmct0alo.getRefbanif());
    }
    else if (StringUtils.isNotBlank(tmct0alo.getTmct0bok().getTmct0ord().getNureford())) {
      tmct0alo.setRefCliente(tmct0alo.getTmct0bok().getTmct0ord().getNureford());
    }

    List<Tmct0grupoejecucion> gruposEjecucion = new ArrayList<Tmct0grupoejecucion>();
    for (ExecutionGroupType executionId : booking.getExecutionGroups()) {
      for (AllocationID allocationId : executionId.getAllocation()) {
        if (allocationId.getNucnfclt().equals(tmct0alo.getId().getNucnfclt())) {
          Tmct0grupoejecucion gr = new Tmct0grupoejecucion();

          gr.setTmct0alo(tmct0alo);
          xmlGrupoEjecucion.xml_mapping(gr, executionId, allocationId, mExecution, cachedConfig);
          gruposEjecucion.add(gr);

        }
      }
    }

    tmct0alo.getTmct0grupoejecucions().addAll(gruposEjecucion);

    this.mappDynamicValues(allocation, tmct0alo);

  }

  private void mappDynamicValues(Allocation allocation, Tmct0alo tmct0alo) {
    Tmct0aloDynamicValues dynamic;
    Tmct0aloDynamicValuesId id;
    if (allocation.getDynamicValues() != null && allocation.getDynamicValues().getDynamicValues() != null) {
      for (DynamicValue value : allocation.getDynamicValues().getDynamicValues()) {
        id = new Tmct0aloDynamicValuesId(tmct0alo.getId().getNuorden(), tmct0alo.getId().getNbooking(), tmct0alo
            .getId().getNucnfclt(), value.getName());
        dynamic = new Tmct0aloDynamicValues();
        dynamic.setId(id);
        LOG.trace("[mappDynamicValues] recepcion dynamic value id: {} valor:{}-{}", id, value.getValue(),
            value.getValueSt());
        if (value.getValue() != null) {
          dynamic.setValor(BigDecimal.valueOf(value.getValue()));
        }
        dynamic.setValorSt(value.getValueSt());
        dynamic.setAuditDate(new java.util.Date());
        dynamic.setAuditUser("SIBBAC20");
        tmct0alo.getDynamicValues().add(dynamic);
      }
    }
  }
}
