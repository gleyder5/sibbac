package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpClearedGrossTradesSpainFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpClearedGrossTradesSpain;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpClearedGrossTradesSpainFieldSetMapper extends WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean>{

  public EuroCcpClearedGrossTradesSpainFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpClearedGrossTradesSpainFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {
    return new EuroCcpClearedGrossTradesSpain();
  }

}
