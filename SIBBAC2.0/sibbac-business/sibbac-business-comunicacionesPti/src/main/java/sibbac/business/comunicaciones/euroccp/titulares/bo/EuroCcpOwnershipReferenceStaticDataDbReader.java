package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpRangoFechasHorasInicioProcesosDTO;
import sibbac.business.comunicaciones.euroccp.titulares.runnable.EuroCcpOwnershipReferenceStaticDataTmpTxtRunnable;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0enviotitularidadBo;
import sibbac.business.wrappers.database.dto.EnvioTitularidadIdAndCdmiembromktDTO;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpOwnershipReferenceStaticDataDbReader")
public class EuroCcpOwnershipReferenceStaticDataDbReader extends EuroCcpAbstractDbReader<EuroCcpIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpOwnershipReferenceStaticDataDbReader.class);

  @Value("${sibbac.euroccp.titulares.ors.file.date.pattern}")
  private String fileDatePattern;

  @Value("${sibbac.euroccp.titulares.ors.txt.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.euroccp.titulares.ors.txt.pool.size}")
  private int threadSize;

  @Value("${sibbac.euroccp.titulares.ors.file.charset}")
  private Charset fileCharset;

  @Value("${sibbac.euroccp.out.path}")
  private String workPathSt;

  @Value("${sibbac.euroccp.out.tmp.folder}")
  private String tmpFolder;

  @Value("${sibbac.euroccp.out.pendientes.folder}")
  private String pendientesFolder;

  @Value("${sibbac.euroccp.out.enviados.folder}")
  private String enviadosFolder;

  @Value("${sibbac.euroccp.out.tratados.folder}")
  private String tratadosFolder;

  @Value("${sibbac.euroccp.titulares.ors.file.name.client.number.last.positions}")
  private int clientNumberLastPositions;

  @Value("${sibbac.euroccp.titulares.ors.file.send.first.sd.date}")
  private int dDaysFirstSendDate;

  @Value("${sibbac.euroccp.titulares.ors.file.send.first.sd.date.init.hour}")
  private String initSendHourFirstDate;

  @Value("${sibbac.euroccp.titulares.ors.file.send.last.sd.date}")
  private int dDaysLastDate;

  @Value("${sibbac.euroccp.titulares.ors.file.send.last.sd.date.end.hour}")
  private String endSendHourLastDate;

  @Value("${sibbac.euroccp.titulares.ors.txt.file.name}")
  private String tmpFileName;

  @Value("${sibbac.euroccp.titulares.ors.txt.file.pattern}")
  private String tmpFileNamePattern;

  @Value("${sibbac.euroccp.titulares.ors.excel.file.name}")
  private String excelFinalFileName;

  @Value("${sibbac.euroccp.titulares.ors.excel.file.name.pattern}")
  private String excelFinalPatternFileName;

  @Value("${sibbac.euroccp.titulares.ors.excel.send.mail.compress.extension}")
  private String excelSendMailCompressExtension;

  @Value("${sibbac.euroccp.titulares.ors.excel.send.by.mail}")
  private String principalFileSentMail;

  @Value("${sibbac.euroccp.titulares.ors.excel.send.to}")
  private List<String> to;

  @Value("${sibbac.euroccp.titulares.ors.excel.send.cc}")
  private List<String> cc;

  @Value("${sibbac.euroccp.titulares.ors.excel.send.subjet}")
  private String subject;

  @Value("${sibbac.euroccp.titulares.ors.excel.send.body}")
  private String body;

  @Value("${sibbac.euroccp.titulares.ors.file.send.timezone}")
  private TimeZone timeZone;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private FestivoBo festivoBo;

  @Autowired
  private Tmct0enviotitularidadBo envioTitularidadBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private EuroCcpFileSentBo euroCcpSentFileBo;

  @Qualifier(value = "euroCcpOwnershipReferenceStaticDataFileWriterBo")
  @Autowired
  private EuroCcpOwnershipReferenceStaticDataFileWriterBo euroCcpOwnershipReferenceStaticDataBo;

  @Qualifier(value = "euroCcpOwnershipReferenceStaticDataTmpTxtRunnable")
  @Autowired
  private EuroCcpOwnershipReferenceStaticDataTmpTxtRunnable euroCcpGeneracionFicheroExcelTitularesTmpTxtRunnable;

  public EuroCcpOwnershipReferenceStaticDataDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<EuroCcpIdDTO> getBeanToProcessBlock() {
    return euroCcpGeneracionFicheroExcelTitularesTmpTxtRunnable;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    beanIterator = null;
    List<Date> holidays = festivoBo.findAllFestivosNacionales();
    List<Integer> workDaysOfWeek = cfgBo.getWorkDaysOfWeek();

    String euroCcpId = cfgBo.getIdEuroCcp();
    if (euroCcpId == null) {
      throw new SIBBACBusinessException("INCIDENCIA Camara no configurada.");
    }
    Tmct0CamaraCompensacion camara = camaraCompensacionBo.findByCdCodigo(euroCcpId);
    if (camara == null) {
      throw new SIBBACBusinessException("INCIDENCIA Camara no no encontrada " + euroCcpId);
    }

    List<EuroCcpIdDTO> idsEuroCcp = new ArrayList<EuroCcpIdDTO>();
    EuroCcpRangoFechasHorasInicioProcesosDTO config = getConfigHorasCorteEnvioTitulares();

    Date feliquidacion1 = getFechaLiquidacion(config.getdDaysFirstSendDate(), workDaysOfWeek, holidays);
    Date feliquidacion2 = getFechaLiquidacion(config.getdDaysLastDate(), workDaysOfWeek, holidays);

    if (feliquidacion1.compareTo(feliquidacion2) > 0) {
      Date feliquidacio1Aux = feliquidacion1;
      feliquidacion1 = feliquidacion2;
      feliquidacion2 = feliquidacio1Aux;
    }

    LOG.debug("Buscando fechas para el envio ors...");
    LOG.debug(
        "[EuroCcpOwnershipReportingGrossExecutionsSendDbReader :: preExecuteTask] buscando fechas entre {} y {}.",
        feliquidacion1, feliquidacion2);
    List<Date> dates = envioTitularidadBo.findAllDistinctFeinicioByCamaraAndBetweenFeliquidacionAndPdteEnviar(
        feliquidacion1, feliquidacion2, camara.getIdCamaraCompensacion());
    LOG.debug("Encontradas {} fechas para el envio ors...", dates);
    Collection<String> euroCcpClientNumbers = new HashSet<String>();
    List<EnvioTitularidadIdAndCdmiembromktDTO> listEnvioTitularidadByCdmiembromkt = new ArrayList<EnvioTitularidadIdAndCdmiembromktDTO>();
    for (Date date : dates) {
      LOG.debug("Buscando operaciones que enviar fecha: {}", date);
      List<EnvioTitularidadIdAndCdmiembromktDTO> idsFecha = envioTitularidadBo.findAllEnvioTitularidadForOrsFileSend(
          date, camara.getIdCamaraCompensacion());
      listEnvioTitularidadByCdmiembromkt.addAll(idsFecha);
      LOG.debug("Encontradas {} operaciones que enviar fecha: {}", idsFecha.size(), date);
    }

    LOG.debug("Encontradas {} operaciones que enviar", listEnvioTitularidadByCdmiembromkt.size());
    Collections.sort(listEnvioTitularidadByCdmiembromkt, new Comparator<EnvioTitularidadIdAndCdmiembromktDTO>() {

      @Override
      public int compare(EnvioTitularidadIdAndCdmiembromktDTO arg0, EnvioTitularidadIdAndCdmiembromktDTO arg1) {
        int compareCdmiembro = arg0.getCdmiembromkt().compareTo(arg1.getCdmiembromkt());
        if (compareCdmiembro == 0) {
          return arg0.getId().compareTo(arg1.getId());
        }
        else {
          return compareCdmiembro;
        }

      }
    });
    if (CollectionUtils.isNotEmpty(listEnvioTitularidadByCdmiembromkt)) {
      for (EnvioTitularidadIdAndCdmiembromktDTO envioTitularidadByCdmiembromkt : listEnvioTitularidadByCdmiembromkt) {
        idsEuroCcp.add(new EuroCcpIdDTO(envioTitularidadByCdmiembromkt.getId()));
        if (euroCcpClientNumbers.add(envioTitularidadByCdmiembromkt.getCdmiembromkt())) {
          LOG.info("Creando reg envio fichero ors {} - clientNumber: {}", excelFinalFileName,
              envioTitularidadByCdmiembromkt.getCdmiembromkt());
          euroCcpSentFileBo.saveNewFileRegisterIfNotExistsWithoutSentNewTransacction(excelFinalFileName,
              envioTitularidadByCdmiembromkt.getCdmiembromkt(), clientNumberLastPositions, fileDatePattern,
              EuroCcpFileTypeEnum.ORS);
        }
      }
      beanIterator = idsEuroCcp.iterator();
    }
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

    euroCcpOwnershipReferenceStaticDataBo.appendTmpFiles(workPathSt, tmpFolder, pendientesFolder, enviadosFolder,
        tratadosFolder, excelSendMailCompressExtension, fileCharset, timeZone);

    if (principalFileSentMail != null && "S".equalsIgnoreCase(principalFileSentMail)) {
      euroCcpOwnershipReferenceStaticDataBo.enviarFilesByEmail(workPathSt, tmpFolder, pendientesFolder, enviadosFolder,
          tratadosFolder, to, cc, subject, body);
    }
    else {
      euroCcpOwnershipReferenceStaticDataBo.moverRutaEnvio(workPathSt);
    }
  }

  /**
   * @throws SIBBACBusinessException
   */
  public void createPathsIfNotExists() throws SIBBACBusinessException {
    euroCcpOwnershipReferenceStaticDataBo.createPathsIfNotExists(workPathSt, tmpFolder, pendientesFolder,
        enviadosFolder, tratadosFolder);
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public int getdDaysFirstSendDate() {
    return dDaysFirstSendDate;
  }

  @Override
  public String getInitSendHourFirstDate() {
    return initSendHourFirstDate;
  }

  @Override
  public int getdDaysLastDate() {
    return dDaysLastDate;
  }

  @Override
  public String getEndSendHourLastDate() {
    return endSendHourLastDate;
  }

  @Override
  public TimeZone getTimeZone() {
    return timeZone;
  }
}
