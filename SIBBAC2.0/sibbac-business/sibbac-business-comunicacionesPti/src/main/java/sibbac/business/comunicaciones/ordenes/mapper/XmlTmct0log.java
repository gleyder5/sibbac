package sibbac.business.comunicaciones.ordenes.mapper;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.model.Tmct0log;
import sibbac.business.wrappers.database.model.Tmct0logId;
import sibbac.business.wrappers.database.model.Tmct0ord;

@Service
public class XmlTmct0log {

  private static String lgcodest = "";
  private static String lgdesest = "";
  private static String lgdescri = "Recepcion SIBBAC, load database.";

  @Autowired
  private Tmct0logBo logBo;

  /**
   * 
   * @param tmct0ord
   */
  public XmlTmct0log() {
    super();

  }

  /**
   * Carga los atributos del nuevo objeto tmct0log
   * 
   * @param nbooking
   * @param hpm
   * @param app
   * @param msgBundle
   * @throws Exception
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void xml_mapping(Tmct0ord tmct0ord, String nbooking, String lgdescri) {
    Tmct0log tmct0log = new Tmct0log();
    tmct0log.setId(new Tmct0logId());
    tmct0log.getId().setNuorden(tmct0ord.getNuorden());
    tmct0log.getId().setLgnumsec(logBo.getSequence());
    tmct0log.setTmct0ord(tmct0ord);
    // tmct0ord.getTmct0logs().add(tmct0log);

    tmct0log.setNbooking(nbooking);

    tmct0log.setLgfecha(new Date(System.currentTimeMillis()));
    tmct0log.setLghora(new Time(System.currentTimeMillis()));

    tmct0log.setLgcodest(lgcodest);
    tmct0log.setLgdesest(lgdesest);
    tmct0log.setLgdescri(lgdescri);

    tmct0log.setCdbroker("");
    tmct0log.setCdmercad("");

    tmct0log.setNucnfclt("");
    tmct0log.setNuejecuc("");
    tmct0log.setNualose1(0);
    tmct0log.setNuholder(new Short("0"));

    tmct0log.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0log.setCdusuaud("SIBBAC20");
    tmct0log = logBo.save(tmct0log);

  }

  /**
   * Carga los atributos del nuevo objeto tmct0log
   * 
   * @param nbooking
   * @param hpm
   * @param app
   * @param msgBundle
   * @throws Exception
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void xml_mapping(Tmct0ord tmct0ord, String nbooking) {
    Tmct0log tmct0log = new Tmct0log();
    tmct0log.setId(new Tmct0logId());
    tmct0log.getId().setNuorden(tmct0ord.getNuorden());
    tmct0log.getId().setLgnumsec(logBo.getSequence());

    tmct0log.setTmct0ord(tmct0ord);
    // tmct0ord.getTmct0logs().add(tmct0log);
    tmct0log.setNbooking(nbooking);

    tmct0log.setLgfecha(new Date(System.currentTimeMillis()));
    tmct0log.setLghora(new Time(System.currentTimeMillis()));

    tmct0log.setLgcodest(lgcodest);
    tmct0log.setLgdesest(lgdesest);
    tmct0log.setLgdescri(lgdescri);

    tmct0log.setCdbroker("");
    tmct0log.setCdmercad("");

    tmct0log.setNucnfclt("");
    tmct0log.setNuejecuc("");
    tmct0log.setNualose1(0);
    tmct0log.setNuholder(new Short("0"));

    tmct0log.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0log.setCdusuaud("SIBBAC20");
    tmct0log = logBo.save(tmct0log);

  }

}