package sibbac.business.comunicaciones.ordenes.mapper;

import java.io.Serializable;

import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;

public class ClientDniName implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String cdholder;
  private String nbclient;
  private String nb1apell;
  private String nb2apell;
  private String nbcomple;
  private String nudnicif;
  private String nuclient;
  private String cddepais;

  public ClientDniName(Holder holder) {

    this.cdholder = holder.getCdholder();

    this.nbclient = holder.getNbclient();
    this.nb1apell = holder.getNbclien1();
    this.nb2apell = holder.getNbclien2();
    this.nbcomple = String.format("%s %s %s ", this.nbclient, this.nb1apell, this.nb2apell);
    this.nudnicif = holder.getNudnicif();
    this.nuclient = holder.getNuclient();
    this.cddepais = holder.getCddepais();
  }

  public String getNbclient() {
    return this.nbclient;
  }

  public String getNb1paell() {
    return this.nb1apell;
  }

  public String getNb2paell() {
    return this.nb2apell;
  }

  public String getNbcomple() {
    return this.nbcomple;
  }

  public String getNudnicif() {
    return this.nudnicif;
  }

  public String getNuclient() {
    return this.nuclient;
  }

  public String getCddepais() {
    return this.cddepais;
  }

}
