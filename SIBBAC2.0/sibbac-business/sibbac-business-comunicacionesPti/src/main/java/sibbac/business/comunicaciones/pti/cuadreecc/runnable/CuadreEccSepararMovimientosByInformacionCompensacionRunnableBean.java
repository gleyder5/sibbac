package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccSepararMovimientosBo;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service("cuadreEccSepararMovimientosByInformacionCompensacionRunnableBean")
public class CuadreEccSepararMovimientosByInformacionCompensacionRunnableBean extends
                                                                             IRunnableBean<CuadreEccDbReaderRecordBeanDTO> {

  @Value("${sibbac.cuadre.ecc.separar.movimientos.page.size:10000}")
  private int revisarImportarDesglosesPageSize;

  @Autowired
  private CuadreEccSepararMovimientosBo revisarDesglosesBo;

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void run(ObserverProcess observer, List<CuadreEccDbReaderRecordBeanDTO> beans, int order) throws SIBBACBusinessException {
    try {
      revisarDesglosesBo.separarMovimientos(beans);
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException(e);
    } finally {
      observer.sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<CuadreEccDbReaderRecordBeanDTO> clone() {
    return this;
  }

}
