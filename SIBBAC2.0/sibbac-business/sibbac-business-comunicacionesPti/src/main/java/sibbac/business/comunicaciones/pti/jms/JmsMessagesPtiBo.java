package sibbac.business.comunicaciones.pti.jms;

import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0xas;
import sibbac.common.jms.JmsMessagesBo;
import sibbac.pti.PTICredentials;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;

import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.jms.MQDestination;
import com.ibm.mq.jms.MQQueue;
import com.ibm.msg.client.wmq.WMQConstants;

@Service(value = "JmsMessagesPtiBo")
public class JmsMessagesPtiBo extends JmsMessagesBo {

  private static final Logger LOG = LoggerFactory.getLogger(JmsMessagesPtiBo.class);

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${sibbac20.script.dividen}")
  private Boolean sibbac20ScriptDividen;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private Tmct0xasBo tmct0xasBo;

  @Transactional
  public void sendListPtiMessageOnline(JmsTemplate jmsTemplate, String destination, List<PTIMessage> ptiMessages)
      throws PTIMessageException, JMSException {
    for (PTIMessage ptiMessage : ptiMessages) {
      sendPtiMessageOnline(jmsTemplate, destination, ptiMessage);
    }
  }

  @Transactional
  public void sendPtiMessageOnline(JmsTemplate jmsTemplate, String destination, PTIMessage ptiMessage)
      throws PTIMessageException, JMSException {
    ptiMessage.setFechaDeEnvio();
    sendPtiJmsTextMessage(jmsTemplate, destination, false, null, 1, false,
        getAppIdentityData(ptiMessage.getTipo().name(), ptiMessage.getHeader().getEntidadDestino(), false),
        ptiMessage.toPTIFormat());
  }

  @Transactional
  public void sendPtiMessageOnline(Session session, String destination, List<PTIMessage> listptiMessage)
      throws PTIMessageException, JMSException {
    try (MessageProducer producer = this.getNewPtiMessageProducer(session, destination)) {
      for (PTIMessage ptiMessage : listptiMessage) {
        ptiMessage.setFechaDeEnvio();
        sendPtiJmsTextMessage(session, producer, false, null, 1, false,
            getAppIdentityData(ptiMessage.getTipo().name(), ptiMessage.getHeader().getEntidadDestino(), false),
            ptiMessage.toPTIFormat());
      }
    }
  }

  @Transactional
  public void sendPtiMessageOnline(Session session, MessageProducer producer, PTIMessage ptiMessage)
      throws PTIMessageException, JMSException {
    ptiMessage.setFechaDeEnvio();
    sendPtiJmsTextMessage(session, producer, false, null, 1, false,
        getAppIdentityData(ptiMessage.getTipo().name(), ptiMessage.getHeader().getEntidadDestino(), false),
        ptiMessage.toPTIFormat());
  }

  @Transactional
  public void sendPtiMessageBatch(JmsTemplate jmsTemplate, String destination, List<PTIMessage> ptiMessages)
      throws PTIMessageException, JMSException {
    int i = 0;
    byte[] groupId = ("" + System.currentTimeMillis() + ptiMessages.hashCode() + this.hashCode()).getBytes();
    PTIMessage fistMessage = ptiMessages.get(0);
    String appidentityData = getAppIdentityData(fistMessage.getTipo().name(), fistMessage.getHeader()
        .getEntidadDestino(), true);
    for (PTIMessage ptiMessage : ptiMessages) {
      i++;
      ptiMessage.setFechaDeEnvio();
      sendPtiJmsTextMessage(jmsTemplate, destination, true, groupId, i, true, appidentityData, ptiMessage.toPTIFormat());
    }
  }

  @Transactional
  public void sendPtiMessageBatch(Session session, String destination, List<PTIMessage> ptiMessages)
      throws PTIMessageException, JMSException {
    int i = 0;
    byte[] groupId = ("" + System.currentTimeMillis() + ptiMessages.hashCode() + this.hashCode()).getBytes();
    PTIMessage fistMessage = ptiMessages.get(0);
    String appidentityData = getAppIdentityData(fistMessage.getTipo().name(), fistMessage.getHeader()
        .getEntidadDestino(), true);
    try (MessageProducer producer = getNewPtiMessageProducer(session, destination)) {
      for (PTIMessage ptiMessage : ptiMessages) {
        i++;
        ptiMessage.setFechaDeEnvio();
        sendPtiJmsTextMessage(session, producer, true, groupId, i, false, appidentityData, ptiMessage.toPTIFormat());
      }
    }
  }

  @Transactional
  public void sendPtiJmsTextMessage(Session session, MessageProducer producer, final boolean batchMode,
      final byte[] groupId, final int index, final boolean hasNext, final String appIddentityDataFinal, final String msg)
      throws JMSException {
    TextMessage message = session.createTextMessage(msg);
    setPtiHeaderInformation(message, batchMode, groupId, index, hasNext, appIddentityDataFinal);
    producer.send(message);
    message.acknowledge();
    // try (MessageProducer produccer = this.getNewPtiMessageProducer(session,
    // destination)) {
    // produccer.send(message);
    // }
  }

  @Transactional
  public void sendPtiJmsTextMessage(JmsTemplate ptiJmsTemplate, String destinationSt, final boolean batchMode,
      final byte[] groupId, final int index, final boolean hasNext, final String appIddentityDataFinal, final String msg)
      throws JMSException {
    LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] inicio.");
    if (StringUtils.isNotBlank(msg) && StringUtils.length(msg) > 100) {

      // factory.setTransportType(WMQConstants.WMQ_CLIENT_NONJMS_MQ);

      MQDestination destination = new MQQueue(destinationSt);
      // TODO revisar que estos datos se puedan setear al crear el destination
      // Activamos la posibilidad de cambiar la cabecera del msg MQMD
      ((MQDestination) destination).setMQMDWriteEnabled(true);
      // Le decimos al que el gestor destino no es jms si no mq para que no
      // ponga cabeceras jms
      ((MQDestination) destination).setMessageBodyStyle(WMQConstants.WMQ_MESSAGE_BODY_MQ);
      ((MQDestination) destination).setTargetClient(WMQConstants.WMQ_TARGET_DEST_MQ);
      // le decimos que nosotros manualmente vamos a poner el campos
      // JMS_IBM_MQMD_ApplIdentityData
      ((MQDestination) destination).setMQMDMessageContext(WMQConstants.WMQ_MDCTX_SET_IDENTITY_CONTEXT);

      /**
       * The following properties require WMQ_MQMD_MESSAGE_CONTEXT to be set to
       * WMQ_MDCTX_SET_IDENTITY_CONTEXT or WMQ_MDCTX_SET_ALL_CONTEXT:
       * 
       * JMS_IBM_MQMD_UserIdentifier JMS_IBM_MQMD_AccountingToken
       * JMS_IBM_MQMD_ApplIdentityData
       * 
       * The following properties require WMQ_MQMD_MESSAGE_CONTEXT to be set to
       * WMQ_MDCTX_SET_ALL_CONTEXT :
       * 
       * JMS_IBM_MQMD_PutApplType JMS_IBM_MQMD_PutApplName JMS_IBM_MQMD_PutDate
       * JMS_IBM_MQMD_PutTime JMS_IBM_MQMD_ApplOriginData
       */

      LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] WMQ {}", ptiJmsTemplate);
      LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] Destination {}", destination);

      // if (this.ptiJmsQueueTemplate == null) {
      // throw new
      // JMSException("No esta inicializado la variable ptiJmsQueueTemplate");
      // }

      ptiJmsTemplate.send(destination, new MessageCreator() {
        @Override
        public Message createMessage(Session session) throws JMSException {
          LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] BATCH : {} INDEX : {} MSG: {}.", batchMode, index,
              msg);
          TextMessage message = session.createTextMessage(msg);
          setPtiHeaderInformation(message, batchMode, groupId, index, hasNext, appIddentityDataFinal);
          LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] JMSMessage: {}.", message);
          return message;
        }

      });

    }
    else {
      throw new JMSException("El mensaque que se quiere enviar no es valido: " + msg);
    }
    LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] fin.");
  }

  @Transactional
  public void sendStPtiMessageBatch(JmsTemplate jmsTemplate, String destination, List<String> ptiMessages)
      throws JMSException {
    int i = 0;
    byte[] groupId = ("" + System.currentTimeMillis() + ptiMessages.hashCode() + this.hashCode()).getBytes();
    String fistMessage = ptiMessages.get(0);
    String appidentityData = getAppIdentityData(getTipoMensaje(fistMessage), getMiembro(fistMessage), true);
    for (String ptiMessage : ptiMessages) {
      i++;
      sendPtiJmsTextMessage(jmsTemplate, destination, true, groupId, i, true, appidentityData, ptiMessage);
    }
  }

  @Transactional
  public void sendStPtiMessageOnline(JmsTemplate jmsTemplate, String destination, String ptiMessage)
      throws JMSException {
    sendPtiJmsTextMessage(jmsTemplate, destination, false, null, 1, false,
        getAppIdentityData(getTipoMensaje(ptiMessage), getMiembro(ptiMessage), false), ptiMessage);
  }

  private void setPtiHeaderInformation(Message jmsMessage, boolean batchMode, byte[] groupId, int index,
      boolean hasNext, String appIddentityData) throws JMSException {
    LOG.trace("[FilesystemToPtiMqBo :: setPtiHeaderInformation] inicio.");

    this.setBasicJmsMQMDProperties(jmsMessage);

    jmsMessage.setStringProperty(WMQConstants.JMS_IBM_MQMD_APPLIDENTITYDATA, appIddentityData);

    if (batchMode) {

      jmsMessage.setObjectProperty(WMQConstants.JMS_IBM_MQMD_GROUPID, groupId);
      jmsMessage.setObjectProperty(WMQConstants.JMS_IBM_MQMD_MSGSEQNUMBER, index);
      if (hasNext) {
        jmsMessage.setIntProperty(WMQConstants.JMS_IBM_MQMD_MSGFLAGS, MQConstants.MQMF_MSG_IN_GROUP);
      }
      else {
        jmsMessage.setIntProperty(WMQConstants.JMS_IBM_MQMD_MSGFLAGS, MQConstants.MQMF_LAST_MSG_IN_GROUP);
      }
    }

    jmsMessage.setIntProperty(WMQConstants.JMS_IBM_MQMD_PERSISTENCE, MQConstants.MQPER_PERSISTENT);

    jmsMessage.setIntProperty(WMQConstants.JMS_IBM_MQMD_ENCODING, MQConstants.MQCCSI_Q_MGR);
    jmsMessage.setObjectProperty(WMQConstants.JMS_IBM_MQMD_CORRELID, MQConstants.MQCI_NONE);
    if (groupId != null) {
      jmsMessage.setObjectProperty(WMQConstants.JMS_IBM_MQMD_MSGID,
          (groupId + "_" + index + "_" + jmsMessage.hashCode()).getBytes());
    }
    else {
      jmsMessage.setObjectProperty(WMQConstants.JMS_IBM_MQMD_MSGID, (System.currentTimeMillis() + "_" + index + "_"
          + jmsMessage.hashCode() + "_" + this.hashCode()).getBytes());
    }
    LOG.trace("[FilesystemToPtiMqBo :: setPtiHeaderInformation] fin.");
  }

  public String getAppIdentityData(String msg, boolean batch) {
    return getAppIdentityData(getTipoMensaje(msg), getMiembro(msg), batch);
  }

  public String getAppIdentityData(String messageType, String miembroDestino, boolean batch) {

    Tmct0cfg origenCfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        PTICredentials.KEY_PROCESS, PTICredentials.KEY_ORIGEN);

    Tmct0cfg entidadCfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
        PTICredentials.KEY_PROCESS, PTICredentials.KEY_DESTINO);

    String appIdentityData = "";
    String origen = "SAIN";

    if (origenCfg != null) {
      origen = origenCfg.getKeyvalue().trim();
    }
    if (miembroDestino == null) {
      String entidad = "V025";
      if (entidadCfg != null) {
        entidad = entidadCfg.getKeyvalue().trim();
      }
      miembroDestino = entidad;
    }
    String tipoFichero = messageType.trim();
    // ('TIPOMENSAJEPTI', 'MO', 'DESTINOPTI','BMCL'),
    Tmct0xas destinoCfg = tmct0xasBo.findByNameAndInforAndNbcamas("TIPOMENSAJEPTI", messageType.trim(), "DESTINOPTI");

    String destino = destinoCfg.getInforas4().trim();

    StringBuffer sb = new StringBuffer();
    try {
      sb.append(StringUtils.rightPad(origen.trim(), 6));
      sb.append(StringUtils.rightPad(destino.trim(), 6));

    }
    catch (Exception e) {
      sb = new StringBuffer();
      sb.append("SAIN  PTIR  ");
      LOG.warn("", e);
    }
    if (batch) {
      try {
        sb.append(StringUtils.rightPad(miembroDestino.trim(), 11));
        sb.append(StringUtils.rightPad(tipoFichero.trim(), 8));
      }
      catch (Exception e) {
        sb = new StringBuffer();
        sb.append("SAIN  PTIR  V025               ");
        LOG.warn("", e);
      }
      // ORIGEN(6)+DESTINO(6)+ENTIDAD(11)+TIPOFICHERO(8)
    }
    else {

    }
    appIdentityData = sb.toString();
    return appIdentityData;
  }

  private String getTipoMensaje(String msg) {
    return msg.substring(0, 0 + 8);
  }

  private String getMiembro(String msg) {
    return msg.substring(25, 25 + 11);
  }

}
