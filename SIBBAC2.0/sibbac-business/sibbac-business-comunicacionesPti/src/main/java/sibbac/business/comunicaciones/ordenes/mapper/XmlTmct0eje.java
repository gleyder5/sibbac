package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0ejeDynamicValuesId;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchema;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchemaId;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.utils.SibbacEnums;

import com.sv.sibbac.multicenter.CargosExecution;
import com.sv.sibbac.multicenter.DynamicValue;
import com.sv.sibbac.multicenter.ExecutionType;

@Service
public class XmlTmct0eje {

  private static Logger LOG = LoggerFactory.getLogger(XmlTmct0eje.class);

  @Autowired
  private Tmct0brkBo brkBo;

  @Autowired
  private Tmct0xasBo xasBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  public XmlTmct0eje() {
    super();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void xml_mapping(Tmct0bok tmct0bok, Tmct0eje tmct0eje, ExecutionType execution,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws XmlOrdenExceptionReception {

    if (tmct0eje.getNutiteje().compareTo(BigDecimal.ZERO) == 0) {

      Tmct0ord tmct0ord = tmct0bok.getTmct0ord();

      tmct0eje.setNuagreje("");
      tmct0eje.setDsobserv(execution.getDsobserv());
      if (StringUtils.isBlank(execution.getTradingVenue())) {
        tmct0eje.setCdbroker(execution.getSegmentTradingVenue());
      }
      else {
        tmct0eje.setCdbroker(execution.getTradingVenue());
      }
      boolean isMTF = cachedConfig.isMtf(brkBo, execution.getTradingVenue(), execution.getExecutionTradingVenue());
      String clearingPlatform = execution.getClearingPlatform();

      String clearingPlatform2 = cachedConfig.getClearingPlatformClearingPlatform(xasBo, clearingPlatform);
      if (clearingPlatform2 != null && !clearingPlatform2.trim().isEmpty()) {
        clearingPlatform = clearingPlatform2;
      }
      LOG.trace(" nuejecuc : {} xml clearingPlatform : {}", execution.getNuejecuc(), clearingPlatform);
      if (StringUtils.isBlank(clearingPlatform)) {
        throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), tmct0bok.getId().getNbooking(), "", null,
            "Cámara de compensación no informada en ejecución " + execution.getNuejecuc());
      }
      else {
        // TODO YA NO NECESITAMOS CONVERSION
        // if (!isMTF) {
        // xasBo.getClearingPlatformCamaraCompensacion(clearingPlatform);
        // clearingPlatform =
        // Tools_tmct0xas.convertTmct0xas3("CLEARINGPLATFORM", clearingPlatform,
        // "CLEARINGPLATFORM",
        // hpm);
        // LOG.trace(PREFIX
        // +
        // " tmct0xas NBCAMXML : 'CLEARINGPLATFORM' NBCAMAS4 : 'CLEARINGPLATFORM' converted clearingPlatform : "
        // + clearingPlatform);
        // }
        tmct0eje.setClearingplatform(clearingPlatform);
      }

      if (!isMTF && tmct0ord.getCdclsmdo() == 'N') {
        LOG.trace(" xml TradingVenue : {}", execution.getTradingVenue());

        String cdmiembroMkt = cachedConfig.getCdbrokerCdmiembro(xasBo, execution.getTradingVenue());
        if (cdmiembroMkt == null) {
          cdmiembroMkt = "";
        }
        LOG.trace(" tmct0xas NBCAMXML : 'CDBROKER' NBCAMAS4 : 'CDMIEMBRO' converted TradingVenue : {}", cdmiembroMkt);
        tmct0eje.setCdMiembroMkt(cdmiembroMkt);
      }
      else {
        tmct0eje.setCdMiembroMkt("");
      }
      tmct0eje.setCdcaptur(execution.getCdcaptur().charAt(0));
      LOG.trace(" xml SettlementExchange : {}", execution.getSettlementExchange());
      String cdmercad = cachedConfig.getSettlementExchangeCdmercad(xasBo, execution.getSettlementExchange());
      LOG.trace(" tmct0xas NBCAMXML : 'SettlementExchange' NBCAMAS4 : 'CDMERCAD' converted SettlementExchange : {}",
          cdmercad);
      tmct0eje.setCdmercad(cdmercad);
      tmct0eje.setCdmoniso(execution.getCdmoniso());
      tmct0eje.setCdordctp(execution.getCdordctp());
      tmct0eje.setCdvia("");
      tmct0eje.setFeejecuc(execution.getFhejecuc().toGregorianCalendar().getTime());
      tmct0eje.setHoejecuc(execution.getFhejecuc().toGregorianCalendar().getTime());
      tmct0eje.setFechaHoraEjec(execution.getFhejecuc().toGregorianCalendar().getTime());
      tmct0eje.setFevalor(new Date(execution.getFevalor().toGregorianCalendar().getTimeInMillis()));

      tmct0eje.setNuopemer(execution.getRefOrdenMkt());
      tmct0eje.setNurefere(execution.getRefEjecucMkt());

      if (execution.getFeintmer() != null) {
        tmct0eje.setFeintmer(new Date(execution.getFeintmer().toGregorianCalendar().getTimeInMillis()));
        tmct0eje.setFeordenmkt(new Date(execution.getFeintmer().toGregorianCalendar().getTimeInMillis()));
        tmct0eje.setHoordenmkt(new Time(execution.getFeintmer().toGregorianCalendar().getTimeInMillis()));
      }
      tmct0eje.setSegmenttradingvenue(execution.getSegmentTradingVenue());
      tmct0eje.setAsignadocomp(execution.isAsignadoComp() ? 1 : 0);
      tmct0eje.setNemotecnico(execution.getNemotecnico());
      tmct0eje.setCtacomp(execution.getCtaComp());
      tmct0eje.setMiembrocomp(execution.getMiembroComp());
      tmct0eje.setRefasignacion(execution.getRefAsignacion());
      tmct0eje.setRefadicional(execution.getRefAdicional());
      tmct0eje.setCdisin(tmct0bok.getCdisin());
      tmct0eje.setCdtpoper(tmct0bok.getCdtpoper());
      tmct0eje.setCdalias(tmct0ord.getCdcuenta());

      tmct0eje.setCdusucon(execution.getCdusucon());
      tmct0eje.setNupareje(null);
      tmct0eje.setTpopebol(execution.getTpopebol());
      tmct0eje.setPccambio(new BigDecimal(execution.getPccambio()).setScale(4, RoundingMode.HALF_UP));
      tmct0eje.setPcefecup(new BigDecimal(execution.getPcefecup()).setScale(4, RoundingMode.HALF_UP));

      tmct0eje.setFhaudit(new Timestamp(System.currentTimeMillis()));
      tmct0eje.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);
      tmct0eje.setPclimcam(new BigDecimal(0));
      tmct0eje.setNupareje((short) 0);
      tmct0eje.setCdvia("");

      tmct0eje.setNutiteje(new BigDecimal(execution.getNutiteje()).setScale(5, RoundingMode.HALF_UP));

      tmct0eje.setImcbmerc(new BigDecimal(execution.getPrecioBrutoMkt()).setScale(8, RoundingMode.HALF_UP));

      tmct0eje.setNutittotal(new BigDecimal(execution.getNuTitTotal()).setScale(5, RoundingMode.HALF_UP));

      String mic = cachedConfig.getDarkPoolMicToMic(xasBo, execution.getExecutionTradingVenue());
      if (mic != null && !mic.trim().isEmpty()) {
        LOG.trace(" tmct0xas NBCAMXML : 'DARKPOOLMIC' NBCAMAS4 : 'mic' converted ExecutionTradingVenue : {}", mic);
        tmct0eje.setExecutionTradingVenue(mic);
      }
      else {
        tmct0eje.setExecutionTradingVenue(execution.getExecutionTradingVenue());
      }

      tmct0eje.setRealExecutionTradingVenue(execution.getExecutionTradingVenue());

      tmct0eje.setIndCotizacion(execution.getIndCotizacion().charAt(0));

      if (StringUtils.isNotBlank(execution.getNoEccRealTimeInd())) {
        tmct0eje.setNoEccRealTimeInd(execution.getNoEccRealTimeInd().charAt(0));
      }
      else {
        tmct0eje.setNoEccRealTimeInd(' ');
      }
      if (StringUtils.isNotBlank(execution.getNoEccPartialSettleInd())) {
        tmct0eje.setNoEccPartialSettleInd(execution.getNoEccPartialSettleInd().charAt(0));
      }
      else {
        tmct0eje.setNoEccPartialSettleInd(' ');
      }

      if (execution.getNoEccSettleDate() != null) {
        tmct0eje.setNoEccSettleDay(new Date(execution.getNoEccSettleDate().toGregorianCalendar().getTimeInMillis()));
      }

      tmct0eje.setNoEccEntity(execution.getNoEccEntity());
      tmct0eje.setNoEccSettleAccount(execution.getNoEccSettleAccount());
      tmct0eje.setNoEccSettleClientCcv(execution.getNoEccSettleClientCCV());
      // si es nacional
      if ('N' == tmct0ord.getCdclsmdo()) {

        tmct0eje.setCuadreau(SibbacEnums.SI_NO.SI.getValue());

        if (cachedConfig.isTpopebolSinEcc(tmct0eje.getTpopebol())) {
          tmct0eje.setCasepti(SibbacEnums.SI_NO.SI.getValue());
        }
        else {
          tmct0eje.setCasepti(SibbacEnums.SI_NO.NO.getValue());
        }
        // si es extranjero
      }
      else {
        tmct0eje.setCuadreau(SibbacEnums.SI_NO.SI.getValue());
        tmct0eje.setCasepti(SibbacEnums.SI_NO.SI.getValue());
      }
      if (execution.getMarketCharges() != null && execution.getMarketCharges().getCargos() != null) {
        CargosExecution cargos = execution.getMarketCharges().getCargos();
        if (cargos.getImCanonCompDv() != null)
          tmct0eje.setImCanonCompDv(new BigDecimal(cargos.getImCanonCompDv()));

        if (cargos.getImCanonCompEu() != null)
          tmct0eje.setImCanonCompEu(new BigDecimal(cargos.getImCanonCompEu()));

        if (cargos.getImCanonContrDv() != null)
          tmct0eje.setImCanonContrDv(new BigDecimal(cargos.getImCanonContrDv()));

        if (cargos.getImCanonContrEu() != null)
          tmct0eje.setImCanonContrEu(new BigDecimal(cargos.getImCanonContrEu()));

        if (cargos.getImCanonLiqDv() != null)
          tmct0eje.setImCanonLiqDv(new BigDecimal(cargos.getImCanonLiqDv()));

        if (cargos.getImCanonLiqEu() != null)
          tmct0eje.setImCanonLiqEu(new BigDecimal(cargos.getImCanonLiqEu()));

      }

      this.mappFeeSchema(execution, tmct0eje);

    }
    else {// grouping tmct0eje
      tmct0eje.setNutiteje(tmct0eje.getNutiteje().add(
          new BigDecimal(execution.getNutiteje()).setScale(5, RoundingMode.HALF_UP)));

      if (execution.getMarketCharges() != null && execution.getMarketCharges().getCargos() != null) {
        CargosExecution cargos = execution.getMarketCharges().getCargos();
        if (cargos.getImCanonCompDv() != null)
          tmct0eje.setImCanonCompDv(tmct0eje.getImCanonCompDv().add(new BigDecimal(cargos.getImCanonCompDv())));

        if (cargos.getImCanonCompEu() != null)
          tmct0eje.setImCanonCompEu(tmct0eje.getImCanonCompEu().add(new BigDecimal(cargos.getImCanonCompEu())));

        if (cargos.getImCanonContrDv() != null)
          tmct0eje.setImCanonContrDv(tmct0eje.getImCanonContrDv().add(new BigDecimal(cargos.getImCanonContrDv())));

        if (cargos.getImCanonContrEu() != null)
          tmct0eje.setImCanonContrEu(tmct0eje.getImCanonContrEu().add(new BigDecimal(cargos.getImCanonContrEu())));

        if (cargos.getImCanonLiqDv() != null)
          tmct0eje.setImCanonLiqDv(tmct0eje.getImCanonLiqDv().add(new BigDecimal(cargos.getImCanonLiqDv())));

        if (cargos.getImCanonLiqEu() != null)
          tmct0eje.setImCanonLiqEu(tmct0eje.getImCanonLiqEu().add(new BigDecimal(cargos.getImCanonLiqEu())));

      }

    }
    this.mappDynamicValues(execution, tmct0eje);
  }

  private void mappFeeSchema(ExecutionType execution, Tmct0eje tmct0eje) {
    if (tmct0eje.getTmct0ejeFeeSchema() == null && execution.getFeeSchema() != null
        && execution.getFeeSchema().length() == 10) {
      LOG.debug("[xml_mapping] NUEJECUC: {}, FEESCHEMA: {}", execution.getNuejecuc(), execution.getFeeSchema());
      Tmct0ejeFeeSchema feeSchema = new Tmct0ejeFeeSchema();
      feeSchema.setId(new Tmct0ejeFeeSchemaId(tmct0eje.getId().getNuorden(), tmct0eje.getId().getNbooking(), tmct0eje
          .getId().getNuejecuc()));
      feeSchema.setTmct0eje(tmct0eje);

      feeSchema.setTipoTarifa(execution.getFeeSchema().charAt(0));
      feeSchema.setSubastaApertura(execution.getFeeSchema().charAt(2));
      feeSchema.setSubastaVolatilidad(execution.getFeeSchema().charAt(3));
      feeSchema.setSubastaCierre(execution.getFeeSchema().charAt(4));
      feeSchema.setRestriccionOrden(execution.getFeeSchema().charAt(5));
      feeSchema.setOrdenVolumenOculto(execution.getFeeSchema().charAt(6));
      feeSchema.setOrdenPuntoMedio(execution.getFeeSchema().charAt(7));
      feeSchema.setOrdenOculta(execution.getFeeSchema().charAt(8));
      feeSchema.setOrdenBloqueCombinado(execution.getFeeSchema().charAt(9));
      feeSchema.setFhaudit(new Date());
      feeSchema.setCdusuaud(tmct0eje.getCdusuaud());
      tmct0eje.setTmct0ejeFeeSchema(feeSchema);
    }
  }

  private void mappDynamicValues(ExecutionType executionType, Tmct0eje tmct0eje) {
    Tmct0ejeDynamicValues dynamic;
    Tmct0ejeDynamicValuesId id;
    if (executionType.getDynamicValues() != null && executionType.getDynamicValues().getDynamicValues() != null) {
      for (DynamicValue value : executionType.getDynamicValues().getDynamicValues()) {
        dynamic = null;
        id = new Tmct0ejeDynamicValuesId(tmct0eje.getId().getNuorden(), tmct0eje.getId().getNbooking(), tmct0eje
            .getId().getNuejecuc(), value.getName());
        LOG.trace("[mappDynamicValues] recepcion dynamic value id: {} valor:{}-{}", id, value.getValue(),
            value.getValueSt());
        for (Tmct0ejeDynamicValues dynamicEje : tmct0eje.getDynamicValues()) {
          if (dynamicEje.getId().equals(id)) {
            dynamic = dynamicEje;
            LOG.warn(
                "[mappDynamicValues] encontrado dynamic value existente id: {} nombre: {} valor old: {}-{} valor new: {}-{}",
                id, value.getName(), dynamic.getValor(), dynamic.getValorSt(), value.getValue(), value.getValueSt());
            break;
          }
        }
        if (dynamic == null) {
          dynamic = new Tmct0ejeDynamicValues();
          dynamic.setId(id);
          tmct0eje.getDynamicValues().add(dynamic);
        }
        dynamic.setValorSt(value.getValueSt());
        if (value.getValue() != null) {
          if (dynamic.getValor() != null) {
            dynamic.setValor(dynamic.getValor().add(BigDecimal.valueOf(value.getValue())));
          }
          else {
            dynamic.setValor(BigDecimal.valueOf(value.getValue()));
          }
        }

        dynamic.setAuditDate(new java.util.Date());
        dynamic.setAuditUser("SIBBAC20");
      }
    }
  }

}
