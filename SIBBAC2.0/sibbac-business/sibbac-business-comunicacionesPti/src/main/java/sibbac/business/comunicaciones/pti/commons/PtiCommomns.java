package sibbac.business.comunicaciones.pti.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.pti.PTICredentials;
import sibbac.pti.PTIException;
import sibbac.pti.PTIMessageFactory;

@Service
public class PtiCommomns {

  protected static final Logger LOG = LoggerFactory.getLogger(PtiCommomns.class);

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${sibbac20.script.dividen}")
  private Boolean sibbac20ScriptDividen;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  public PtiCommomns() {
  }

  public PTICredentials getNewPTICredentials(String applicationName, boolean isScriptDividend) throws PTIException {
    LOG.info("[PtiCommomns :: initPTICredentials] ## Loading PTI Credentials...");
    PTICredentials pti = null;
    try {
      pti = new PTICredentials();
      Tmct0cfg origen = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName, PTICredentials.KEY_PROCESS,
                                                                        PTICredentials.KEY_ORIGEN);
      Tmct0cfg usuarioOrigen = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName,
                                                                               PTICredentials.KEY_PROCESS,
                                                                               PTICredentials.KEY_USUARIO_ORIGEN);
      Tmct0cfg destino = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName, PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_DESTINO);
      Tmct0cfg usurioDestino = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName,
                                                                               PTICredentials.KEY_PROCESS,
                                                                               PTICredentials.KEY_USUARIO_DESTINO);
      Tmct0cfg miembro = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName, PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_MIEMBRO);

      Tmct0cfg usuarioMiembro = null;

      if (isScriptDividend) {
        usuarioMiembro = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName,
                                                                         PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_USUARIO_MIEMBRO_SCRIPT_DIVIDEND);
      } else {
        usuarioMiembro = tmct0cfgBo.findByAplicationAndProcessAndKeyname(applicationName, PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_USUARIO_MIEMBRO_NORMAL);
      }
      pti.setOrigen(origen.getKeyvalue());
      pti.setUsuarioOrigen(usuarioOrigen.getKeyvalue());
      pti.setDestino(destino.getKeyvalue());
      pti.setUsuarioDestino(usurioDestino.getKeyvalue());
      pti.setMiembro(miembro.getKeyvalue());
      pti.setUsuarioMiembro(usuarioMiembro.getKeyvalue());
      PTIMessageFactory.setCredentials(pti);
      LOG.info("[PtiCommomns :: initPTICredentials] ## Loaded PTI Credentials {}", pti);
    } catch (Exception e) {
      LOG.warn("[PtiCommomns :: initPTICredentials] ## Incidencia al inicializar las credenciales de pti ", e);
      throw new PTIException(e);
    }
    return pti;
  }

  public synchronized void initPTICredentials() throws PTIException {
    if (PTIMessageFactory.getCredentials() == null) {
      LOG.info("[PtiCommomns :: initPTICredentials] ## Loading PTI Credentials...");
      PTIMessageFactory.setCredentials(getNewPTICredentials(sibbac20ApplicationName, sibbac20ScriptDividen));
      LOG.info("[PtiCommomns :: initPTICredentials] ## Loaded PTI Credentials {}", PTIMessageFactory.getCredentials());
    }
  }
}
