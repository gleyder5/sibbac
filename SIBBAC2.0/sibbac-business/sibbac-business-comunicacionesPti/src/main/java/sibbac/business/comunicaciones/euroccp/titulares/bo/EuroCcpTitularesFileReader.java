package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpResponseFileProcessBo;

/**
 * sibbac.euroccp.titulares.org.file.date.pattern=yyyyMMdd
 * sibbac.euroccp.titulares.org.txt.transaction.size=100
 * sibbac.euroccp.titulares.org.txt.pool.size=25
 * sibbac.euroccp.titulares.org.file.charset=ISO-8859-1
 * sibbac.euroccp.titulares.org.txt.file.name=SVB_ORG.txt
 * sibbac.euroccp.titulares.org.file.name.client.number.last.positions=4
 * sibbac.euroccp.titulares.org.file.send.first.sd.date=-2
 * sibbac.euroccp.titulares.org.file.send.first.sd.date.init.hour=130000
 * sibbac.euroccp.titulares.org.file.send.last.sd.date=-1
 * sibbac.euroccp.titulares.org.file.send.last.sd.date.end.hour=145800
 * sibbac.euroccp.titulares.org.file.send.mail.compress.extension=zip
 * sibbac.euroccp.titulares.org.file.send.by.mail=N sibbac.euroccp.titulares.org
 * .file.send.to=jonatan.sanandres@extern.isban.com sibbac.euroccp.titulares.org
 * .file.send.cc=jonatan.sanandres@extern.isban.com
 * sibbac.euroccp.titulares.org.file.send.subjet=Banco Santander S.A. (ORG)
 * Ownership Reporting of Gross executions
 * sibbac.euroccp.titulares.org.file.send.body=Banco Santander S.A. (ORG)
 * Ownership Reporting of Gross executions
 * sibbac.euroccp.titulares.org.file.send.timezone=CET
 * */
@Service
public class EuroCcpTitularesFileReader extends EuroCcpResponseFileProcessBo {

  @Value("${sibbac.euroccp.titulares.org.crg.in.line.pattern}")
  private String linePattern;

  @Value("${sibbac.euroccp.titulares.org.crg.in.footer.pattern}")
  private String footerLinePattern;

  @Value("${sibbac.euroccp.titulares.receive.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.euroccp.titulares.receive.pool.size}")
  private int threadSize;

  @Value("${sibbac.euroccp.titulares.org.file.charset}")
  private Charset fileCharset;

  @Value("${sibbac.euroccp.titulares.org.file.date.pattern}")
  private String fileDatePattern;

  @Value("${sibbac.euroccp.titulares.file.in.pattern}")
  private String filePattern;

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public Charset getFileCharset() {
    return this.fileCharset;
  }

  @Override
  public String getLinePattern() {
    return this.linePattern;
  }

  @Override
  public String getFooterPattern() {
    return this.footerLinePattern;
  }

  @Override
  public String getFileDatePattern() {
    return this.fileDatePattern;
  }

  @Override
  public String getFilePattern() {
    return this.filePattern;
  }

}
