package sibbac.business.comunicaciones.imputacions3.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.realigment.bo.EuroCcpExecutionRealigmentBo;
import sibbac.business.comunicaciones.pti.mo.bo.MovimientosBo;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoAssigmentDataException;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoPtiMensajeDataException;
import sibbac.business.wrappers.database.bo.MovimientoEccS3Bo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.model.MovimientoEccS3;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;

@Service
public class ImputacionS3Bo {

  protected static final Logger LOG = LoggerFactory.getLogger(ImputacionS3Bo.class);

  @Autowired
  private MovimientosBo movimientosBo;

  @Autowired
  private EuroCcpExecutionRealigmentBo euroCcpExecutionRealigmentBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0operacioncdeccBo opBo;

  @Autowired
  private Tmct0movimientoeccBo movEccBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNuevaBo;

  @Autowired
  private MovimientoEccS3Bo movS3Bo;

  public void validarHoraInicioFinBotonazo(Date horaIniImputacionesDate, Date horaEndImputacionesDate)
      throws SIBBACBusinessException {
    if (horaIniImputacionesDate == null) {
      LOG.warn("[validarHoraInicioFinBotonazo] INCIDENCIA No se ha encontrado la hora de inicio del BOTONAZO.");
      throw new SIBBACBusinessException("Hora inicio botonazo no configurada.");
    }

    if (horaEndImputacionesDate == null) {
      LOG.warn("[validarHoraInicioFinBotonazo] INCIDENCIA No se ha encontrado la hora de fin del BOTONAZO.");
      throw new SIBBACBusinessException("Hora fin botonazo no configurada.");
    }
  }

  /**
   * Realiza el envio por MQ de los MO con titulos que le quedan disponibles a
   * las operaciones ecc
   * 
   * @param cdoperacionecc
   * @param cuentaCompensacionS3
   * @return
   * @throws PTIMessageException
   * @throws JMSException
   * @throws SIBBACBusinessException
   * @throws NoPtiMensajeDataException
   * @throws NoAssigmentDataException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public BigDecimal crearImputacionesS3(List<String> listCdoperacionecc, ImputacionS3ConfigDTO config)
      throws PTIMessageException, JMSException, SIBBACBusinessException, NoPtiMensajeDataException,
      NoAssigmentDataException {
    LOG.trace("[crearImputacionesS3] inicio.");
    Tmct0operacioncdecc op;
    Tmct0anotacionecc anAux;
    BigDecimal titulosEuroCcp;
    List<Tmct0movimientoecc> movs;
    BigDecimal titulosNacional = BigDecimal.ZERO;
    BigDecimal titulosTotales = BigDecimal.ZERO;
    List<PTIMessage> msgs = new ArrayList<PTIMessage>();
    List<Tmct0anotacionecc> listAllOps = new ArrayList<Tmct0anotacionecc>();
    List<Tmct0anotacionecc> listOpsNacional = new ArrayList<Tmct0anotacionecc>();
    List<Tmct0anotacionecc> listOpsEuroccp = new ArrayList<Tmct0anotacionecc>();

    for (String cdoperacionecc : listCdoperacionecc) {
      anAux = anBo.findByCdoperacionecc(cdoperacionecc);
      if (anAux != null) {
        if (anAux.getCdcamara().trim().equals(config.getCamaraNacional().trim())) {
          listOpsNacional.add(anAux);
        }
        else if (anAux.getCdcamara().trim().equals(config.getCamaraEuroCcp().trim())) {
          listOpsEuroccp.add(anAux);
        }
        else {
          LOG.warn("[crearImputacionesS3] cdoperacion: {} no pertenece a una camara {} configurada para el botonazo.",
              cdoperacionecc, anAux.getCdcamara().trim());
        }
        listAllOps.add(anAux);
      }
    }
    if (CollectionUtils.isNotEmpty(listAllOps)) {
      movs = crearMovimientosEccS3(listOpsNacional, config.getCuentaCompensacionS3(), config.getCamaraNacional(),
          config.getAuditUser());

      for (Tmct0movimientoecc mov : movs) {
        titulosNacional = titulosNacional.add(mov.getImtitulos());
      }

      msgs.addAll(movimientosBo.crearMensajesMoPti(movs, config.getVersion()));

      titulosEuroCcp = euroCcpExecutionRealigmentBo.insertTraspasoEuroCcpS3(listOpsEuroccp,
          config.getCuentaCompensacionS3(), config.getCamaraEuroCcp(), config.getAuditUser());

      for (Tmct0anotacionecc an : listAllOps) {
        if (an.getEnvios3() != 'S') {
          an.setEnvios3('S');
          an.setAuditFechaCambio(new Date());
          an.setAuditUser(config.getAuditUser());
          anBo.save(an);
          op = opBo.findByCdoperacionecc(an.getCdoperacionecc());
          if (op != null && (op.getEnvioS3() == null || !op.getEnvioS3().trim().equals("S"))) {
            op.setEnvioS3("S");
            op.setAuditFechaCambio(new Date());
            op.setAuditUser(config.getAuditUser());
            opBo.save(op);
          }
        }
      }

      anBo.flush();

      if (!msgs.isEmpty()) {
        movimientosBo.sendMensajesOnline(config.getCf(), msgs);
      }

      titulosTotales = titulosNacional.add(titulosEuroCcp);
    }
    listAllOps.clear();
    listOpsEuroccp.clear();
    listOpsNacional.clear();

    LOG.trace("[crearImputacionesS3] fin.");
    return titulosTotales;
  }

  // private Callable<CallableResultDTO> getNewCallableEnvioS3(final
  // ConnectionFactory cf,
  // final List<String> listCdoperacionecc, final String cuentaCompensacionS3,
  // final PTIMessageVersion version) {
  // Callable<CallableResultDTO> c = new Callable<CallableResultDTO>() {
  //
  // @Override
  // public CallableResultDTO call() throws Exception {
  // CallableResultDTO res = new CallableResultDTO();
  // movimientosSubListBo.sendMensajesMoS3(cf, listCdoperacionecc,
  // cuentaCompensacionS3, version);
  // return res;
  // }
  // };
  // return c;
  // }

  /**
   * 
   * Devuelve la lista de movimientos que se van a grabar en bbdd
   * 
   * @param ic
   * @param alc
   * @param dc
   * @param dcts
   * @param corretaje
   * @return
   * @throws SIBBACBusinessException
   */
  private List<Tmct0movimientoecc> crearMovimientosEccS3(List<Tmct0anotacionecc> ops, String cuentaCompensacionS3,
      String camaraNacional, String auditUser) throws SIBBACBusinessException {

    LOG.trace("[MovimientosSubListBo :: crearMovimientosEccS3] inicio.");
    List<Tmct0movimientoecc> movs = new ArrayList<Tmct0movimientoecc>();
    Tmct0movimientoecc mov = null;
    MovimientoEccS3 movS3;
    Tmct0movimientooperacionnuevaecc movNueva = null;
    String cdrefmovimiento;
    for (Tmct0anotacionecc op : ops) {
      if (op.getCdcamara() != null && camaraNacional != null && op.getCdcamara().trim().equals(camaraNacional.trim())) {
        if (op.getEnvios3() != 'S') {
          mov = new Tmct0movimientoecc();
          cdrefmovimiento = movEccBo.getNewMOCdrefmovimientoFromMOSequence();
          movEccBo.inizializeTmct0movimientoEccS3(op, cdrefmovimiento, "", cuentaCompensacionS3, mov);
          mov.setAuditUser(auditUser);
          movs.add(mov);

          movNueva = new Tmct0movimientooperacionnuevaecc();
          movNuevaBo.inizializeMovimientoOperacioNueva(op, movNueva);
          movNueva.setAuditUser(auditUser);
          movNueva.setTmct0movimientoecc(mov);
          addTitulosAndEfectivo(mov, movNueva);
          mov.getTmct0movimientooperacionnuevaeccs().add(movNueva);

          movS3 = new MovimientoEccS3();
          movS3Bo.inizializeMovimientoEccS3(op, mov, movNueva, movS3);
          movS3.setAudit_user(auditUser);
          movS3Bo.save(movS3);
        }
        else {
          LOG.warn("[MovimientosSubListBo :: crearMovimientosEccS3] op enviada anteriormente a s3: {}",
              op.getCdoperacionecc());
        }
      }
    }// fin for
    LOG.trace("[MovimientosSubListBo :: crearMovimientosEccS3] fin.");
    return movs;
  }

  private void addTitulosAndEfectivo(Tmct0movimientoecc mov, Tmct0movimientooperacionnuevaecc movn) {
    mov.setImtitulos(mov.getImtitulos().add(movn.getImtitulos()));
    mov.setImefectivo(mov.getImefectivo().add(movn.getImefectivo()));
  }

}
