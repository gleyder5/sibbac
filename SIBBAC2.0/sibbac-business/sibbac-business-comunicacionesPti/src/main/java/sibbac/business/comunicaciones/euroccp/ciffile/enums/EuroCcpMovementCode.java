package sibbac.business.comunicaciones.euroccp.ciffile.enums;

public enum EuroCcpMovementCode {

  TRADE_01("01"),
  TRADE_CORRECTION_04("04"),
  TRADE_TRANSFER_05("05"),
  TRADE_BALANCE_06("06"),
  TRADE_OUT_07("07"),
  TRADE_IN_08("08"),
  POSITION_OUT_15("15"),
  POSITION_IN_16("16");

  public String text;

  private EuroCcpMovementCode(String text) {
    this.text = text;
  }

}
