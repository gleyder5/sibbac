package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccReferenciaValidacionRunnableBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccReferenciaValidacionDbReader")
public class CuadreEccReferenciaValidacionDbReader extends WrapperMultiThreadedDbReader<CuadreEccIdDTO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CuadreEccReferenciaValidacionDbReader.class);

  @Value("${sibbac.cuadre.ecc.max.reg.process.in.execution:250}")
  private int maxRegToProcess;

  @Value("${sibbac.cuadre.ecc.validacion.referencias.transaction.size:2}")
  private int transactionSize;

  @Value("${sibbac.cuadre.ecc.validacion.referencias.thread.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  @Qualifier(value = "cuadreEccValidacionReferenciasEccRunnableBean")
  @Autowired
  private CuadreEccReferenciaValidacionRunnableBean cuadreEccRefValidaRunnBean;

  public CuadreEccReferenciaValidacionDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<CuadreEccIdDTO> getBeanToProcessBlock() {
    return cuadreEccRefValidaRunnBean;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.debug("[CuadreEccReferenciaValidacionDbReader :: preExecuteTask] inicio.");

    LOG.debug("[CuadreEccReferenciaValidacionDbReader :: preExecuteTask] buscando referencias ecc que validar...");
    List<CuadreEccIdDTO> l = cuadreEccReferenciaBo.findAllCuadreEccReferenciaValidacionIdDTO(maxRegToProcess);
    LOG.debug("[CuadreEccReferenciaValidacionDbReader :: preExecuteTask] encontrados {} referencias ecc que validar.",
        l.size());
    beanIterator = l.iterator();
    LOG.debug("[CuadreEccReferenciaValidacionDbReader :: preExecuteTask] fin.");
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
