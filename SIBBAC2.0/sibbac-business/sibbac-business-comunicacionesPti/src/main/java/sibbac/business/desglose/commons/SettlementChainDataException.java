package sibbac.business.desglose.commons;

public class SettlementChainDataException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 6920154261742876910L;

  public SettlementChainDataException() {
  }

  public SettlementChainDataException(String arg0) {
    super(arg0);
  }

  public SettlementChainDataException(Throwable arg0) {
    super(arg0);
  }

  public SettlementChainDataException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public SettlementChainDataException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

}
