package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.bo.EuroCcpProcessedUnsettledMovementBo;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpOwnershipReferenceStaticDataDao;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpOwnershipReportingGrossExecutionsDao;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReportingGrossExecutionsDTO;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO;
import sibbac.business.comunicaciones.euroccp.titulares.mapper.EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReportingGrossExecutions;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReportingGrossExecutionsParentReference;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Service(value = "euroCcpOwnershipReportingGrossExecutionsFileWriterBo")
public class EuroCcpOwnershipReportingGrossExecutionsFileWriterBo extends
    EuroCcpAbstractFileGenerationBo<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO, EuroCcpCommonSendRegisterDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory
      .getLogger(EuroCcpOwnershipReportingGrossExecutionsFileWriterBo.class);

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao envioRtDao;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private EuroCcpFileSentBo fileSentBo;

  @Autowired
  private EuroCcpOwnershipReferenceStaticDataDao fileSentLineDao;

  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsDao orgDao;

  @Autowired
  private EuroCcpProcessedUnsettledMovementBo unsettledMovementBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;

  @Autowired
  private EuroCcpOwnershipReferenceStaticDataBo staticDataBo;

  public EuroCcpOwnershipReportingGrossExecutionsFileWriterBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void processBeansAndWriteTmpFile(Path tmpPath, String tmpFileName, Date sentDate,
      List<EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO> beans,
      Map<String, FlatFileItemWriter<EuroCcpCommonSendRegisterDTO>> mapFicherosByClientNumber,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosPrincipal,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosTmp, int lastClientNumberPositions, Charset charset)
      throws SIBBACBusinessException, NullPointerException {

    String msg;
    String auditUser = "SIBBAC20";

    Tmct0CuentasDeCompensacion cuentaCompensacionHb;
    String ctaCompensacion;
    String accountNumber;
    String clientNumber;
    BigDecimal numberShares;
    BigDecimal numberSharesAcumulado;
    Path file = null;
    List<Tmct0DesgloseCamaraEnvioRt> rts;
    Long nudesglose;
    Tmct0desgloseclitit dct;
    EuroCcpOwnershipReportingGrossExecutions org;
    EuroCcpOwnershipReportingGrossExecutionsDTO orgDto;
    List<EuroCcpOwnershipReportingGrossExecutions> parents;
    List<EuroCcpOwnershipReportingGrossExecutions> parentsTitlesAvailable;
    EuroCcpOwnershipReportingGrossExecutionsParentReference reference;
    EuroCcpFileSent principalFileSent;
    EuroCcpFileType fileType = euroCcpFileTypeDao.findByCdCodigo(getTmpFileType().name());
    EuroCcpFileSent tmpFileSent = null;

    FlatFileItemWriter<EuroCcpCommonSendRegisterDTO> writer = null;
    Pageable page = new PageRequest(0, 100);

    List<EuroCcpOwnershipReportingGrossExecutions> regsToSave = new ArrayList<EuroCcpOwnershipReportingGrossExecutions>();
    List<EuroCcpOwnershipReportingGrossExecutionsDTO> regsToWrite = new ArrayList<EuroCcpOwnershipReportingGrossExecutionsDTO>();
    Map<String, EuroCcpOwnershipReportingGrossExecutions> orgsByOwnerReferenceFrom = new HashMap<String, EuroCcpOwnershipReportingGrossExecutions>();
    Map<String, Tmct0CuentasDeCompensacion> cuentasByCdCodigo = new HashMap<String, Tmct0CuentasDeCompensacion>();
    for (EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO bean : beans) {

      LOG.debug("[processBeansAndWriteTmpFile] bean: {}", bean);

      do {
        LOG.debug("[processBeansAndWriteTmpFile] buscando rt's que enviar...");
        rts = envioRtDao.findAllByFecontratacionAndCriterioComunicacionTitularAndCdmiembromktAndEstado(
            bean.getFecontratacion(), bean.getCriterioComunicacionTitular(), bean.getCdmiembromkt(), 'P', page);

        for (Tmct0DesgloseCamaraEnvioRt rt : rts) {

          orgsByOwnerReferenceFrom.clear();
          regsToSave.clear();
          regsToWrite.clear();

          if (!this.isOrsInEuroCcp(rt.getReferenciaTitular())) {
            continue;
          }

          nudesglose = Objects.requireNonNull(rt.getNudesglose(), MessageFormat.format(
              "INCIDENCIA- numero desglose requerido id rt: {0}.", rt.getIdDesgloseCamaraEnvioRt()));

          dct = Objects.requireNonNull(dctBo.findById(nudesglose),
              MessageFormat.format("INCIDENCIA- nudesglose {0} no encontrado.", nudesglose));

          Objects.requireNonNull(rt.getDesgloseCamara().getTmct0infocompensacion(), MessageFormat.format(
              "INCIDENCIA- No encontrada cta para id.dc: {0}", rt.getDesgloseCamara().getIddesglosecamara()));

          ctaCompensacion = Objects.requireNonNull(rt.getDesgloseCamara().getTmct0infocompensacion()
              .getCdctacompensacion(), MessageFormat.format("INCIDENCIA- No encontrada cta para id.dc: {0}", rt
              .getDesgloseCamara().getIddesglosecamara()));

          if (StringUtils.isNotBlank(dct.getCuentaCompensacionDestino())
              && !ctaCompensacion.trim().equals(dct.getCuentaCompensacionDestino().trim())) {
            LOG.warn("[processBeansAndWriteTmpFile] cta ic: {} <> cta desglose {} usamos la del desglose.",
                ctaCompensacion, dct.getCuentaCompensacionDestino());
            ctaCompensacion = dct.getCuentaCompensacionDestino();

          }
          if ((cuentaCompensacionHb = cuentasByCdCodigo.get(ctaCompensacion)) == null) {
            cuentaCompensacionHb = Objects.requireNonNull(cuentaCompensacionBo.findByCdCodigo(ctaCompensacion),
                MessageFormat.format("INCIDENCIA- No encontrada cta para id.dc: {0} - cta: {1}", rt.getDesgloseCamara()
                    .getIddesglosecamara(), ctaCompensacion));
            cuentasByCdCodigo.put(ctaCompensacion, cuentaCompensacionHb);
          }
          clientNumber = Objects.requireNonNull(cuentaCompensacionHb.getClientNumber(), MessageFormat.format(
              "INCIDENCIA- No encontrado Client Number id.dc: {0} - cta: {1}", rt.getDesgloseCamara()
                  .getIddesglosecamara(), ctaCompensacion));

          accountNumber = Objects.requireNonNull(cuentaCompensacionHb.getAccountNumber(), MessageFormat.format(
              "INCIDENCIA- No encontrado Account Number id.dc: {0} - cta: {1}", rt.getDesgloseCamara()
                  .getIddesglosecamara(), ctaCompensacion));

          principalFileSent = Objects
              .requireNonNull(
                  mapRegistrosFicherosEnviadosPrincipal.get(clientNumber),
                  MessageFormat
                      .format(
                          "INCIDENCIA- Reg principal file sent no disponible para el envio. ClientNumber: {0} Date: {1} type: {2}",
                          clientNumber, sentDate, getPrincipalFileType()));

          tmpFileSent = mapRegistrosFicherosEnviadosTmp.get(clientNumber);
          writer = mapFicherosByClientNumber.get(clientNumber);
          if (tmpFileSent == null) {
            String fileBaseName = FilenameUtils.getBaseName(tmpFileName);
            String extension = FilenameUtils.getExtension(tmpFileName);

            String tmpName = fileBaseName + clientNumber.substring(clientNumber.length() - lastClientNumberPositions)
                + "_" + rt.getIdDesgloseCamaraEnvioRt() + "_"
                + FormatDataUtils.convertDateToConcurrentFileName(new Date()) + "." + extension;
            file = Paths.get(tmpPath.toString(), tmpName);
            tmpFileSent = fileSentBo.insertTxtTmpFileSent(principalFileSent, fileType, file, sentDate);
            mapRegistrosFicherosEnviadosTmp.put(clientNumber, tmpFileSent);
            writer = getNewWriter(file, charset, new EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper());
            mapFicherosByClientNumber.put(clientNumber, writer);
          }

          // por cada org buscamos los orgs antiguos que han sido enviados pero
          // su desglose esta dado de baja, hay que
          // tener en cuenta que un org puede salir de N owner references from

          // utilizamos sus referencias primero para montar los nuevos org's
          numberShares = rt.getImtitulos();

          parents = new ArrayList<EuroCcpOwnershipReportingGrossExecutions>();

          parentsTitlesAvailable = orgDao
              .findAllByTradeDateAndExecutionReferenceAndMicAndAccountNumberAndEstadoProcesadoGtAndNumberSharesAvailableGte(
                  rt.getFecontratacion(), rt.getCriterioComunicacionTitular(), rt.getCdPlataformaNegociacion(),
                  accountNumber, EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId(), BigDecimal.ZERO);
          numberSharesAcumulado = BigDecimal.ZERO;
          for (EuroCcpOwnershipReportingGrossExecutions parentTitlesAvailable : parentsTitlesAvailable) {
            if (parentTitlesAvailable.getDesgloseCamaraEnvioRt().getEstado() == 'C'
                || parentTitlesAvailable.getDesgloseCamaraEnvioRt().getDesgloseCamara().getTmct0estadoByCdestadoasig()
                    .getIdestado() == EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              LOG.debug("[processBeansAndWriteTmpFile] encontrado ref. padre enviada.");
              parents.add(parentTitlesAvailable);
              numberSharesAcumulado = numberSharesAcumulado.add(parentTitlesAvailable.getNumberSharesAvailable());
              if (numberSharesAcumulado.compareTo(numberShares) >= 0) {
                break;
              }
            }
            else if (parentTitlesAvailable.getDesgloseCamaraEnvioRt().getIdDesgloseCamaraEnvioRt() == rt
                .getIdDesgloseCamaraEnvioRt()) {
              LOG.debug("[processBeansAndWriteTmpFile] los titulos ya han sido enviados con la misma ref. tit.");
              numberShares = numberShares.subtract(parentTitlesAvailable.getNumberShares());
            }
          }

          parentsTitlesAvailable.clear();

          if (numberShares.compareTo(BigDecimal.ZERO) > 0) {
            numberSharesAcumulado = BigDecimal.ZERO;
            // si no tenemos referencias padres
            if (CollectionUtils.isNotEmpty(parents)) {
              // ordenamos para que siempre sea el mismo resultado
              for (EuroCcpOwnershipReportingGrossExecutions orgParentFor : parents) {
                if ((org = orgsByOwnerReferenceFrom.get(orgParentFor.getOwnerReferenceTo())) == null) {

                  if (!this.isOrsInEuroCcp(orgParentFor.getOwnerReferenceTo())) {
                    continue;
                  }

                  // mandamos nuevo org y grabamos en bbd
                  org = new EuroCcpOwnershipReportingGrossExecutions();
                  org.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
                  org.setDesgloseCamaraEnvioRt(rt);
                  org.setExecutionReference(rt.getCriterioComunicacionTitular());
                  org.setMic(rt.getCdPlataformaNegociacion());
                  org.setAccountNumber(accountNumber);
                  org.setOwnerReferenceTo(rt.getReferenciaTitular());
                  org.setTradeDate(rt.getFecontratacion());
                  org.setFileSent(tmpFileSent);
                  org.setOwnerReferenceFrom(orgParentFor.getOwnerReferenceTo());
                  org.setNumberShares(BigDecimal.ZERO);
                  org.setNumberSharesAvailable(BigDecimal.ZERO);
                  orgsByOwnerReferenceFrom.put(orgParentFor.getOwnerReferenceTo(), org);
                }

                reference = new EuroCcpOwnershipReportingGrossExecutionsParentReference();
                reference.setChildReference(org);
                reference.setParentReference(orgParentFor);
                reference.setAuditDate(new Date());
                reference.setAuditUser(auditUser);

                numberShares = orgParentFor.getNumberSharesAvailable();
                if (numberShares.add(numberSharesAcumulado).compareTo(rt.getImtitulos()) > 0) {
                  numberShares = rt.getImtitulos().subtract(numberSharesAcumulado);
                }
                reference.setNumberShares(numberShares);
                org.setNumberShares(org.getNumberShares().add(numberShares));
                org.setNumberSharesAvailable(org.getNumberShares());

                numberSharesAcumulado = numberSharesAcumulado.add(numberShares);

                orgParentFor.setNumberSharesAvailable(orgParentFor.getNumberSharesAvailable().subtract(numberShares));
                orgParentFor.setAuditDate(new Date());
                orgParentFor.setAuditUser(auditUser);

                org.getParentReferences().add(reference);

                if (rt.getImtitulos().compareTo(numberSharesAcumulado) == 0) {
                  break;
                }
              }// fin for
              regsToSave.addAll(orgsByOwnerReferenceFrom.values());
              if (!regsToSave.isEmpty()) {
                regsToSave = orgDao.save(regsToSave);
              }

              if (!parents.isEmpty()) {
                parents = orgDao.save(parents);
              }

            }// fin else

            if (numberSharesAcumulado.compareTo(rt.getImtitulos()) < 0) {

              // mandamos nuevo org y grabamos en bbd
              org = new EuroCcpOwnershipReportingGrossExecutions();
              org.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
              org.setDesgloseCamaraEnvioRt(rt);
              org.setExecutionReference(rt.getCriterioComunicacionTitular());
              org.setMic(rt.getCdPlataformaNegociacion());
              org.setOwnerReferenceFrom("");
              org.setAccountNumber(accountNumber);
              org.setNumberShares(rt.getImtitulos().subtract(numberSharesAcumulado));
              org.setNumberSharesAvailable(org.getNumberShares());
              org.setOwnerReferenceTo(rt.getReferenciaTitular());
              org.setTradeDate(rt.getFecontratacion());
              org.setFileSent(tmpFileSent);
              org = orgDao.save(org);
              regsToSave.add(org);
            }

            for (EuroCcpOwnershipReportingGrossExecutions reg : regsToSave) {
              orgDto = new EuroCcpOwnershipReportingGrossExecutionsDTO(reg);
              orgDto.setAccountNumber(StringUtils.substring(reg.getAccountNumber(), reg.getAccountNumber().length()
                  - lastClientNumberPositions));
              regsToWrite.add(orgDto);
            }
            try {
              writer.write(regsToWrite);
            }
            catch (Exception e) {
              throw new SIBBACBusinessException("INCIDENCIA- Escribiendo registros.", e);
            }
          }
          else {
            LOG.debug("[processBeansAndWriteTmpFile] No hay nada que volver a enviar.");
          }

          rt.setEstado('E');
          rt.setAudit_fecha_cambio(new Date());
          rt.setAudit_user(auditUser);
          envioRtDao.save(rt);
          msg = MessageFormat
              .format(
                  "Generado fichero ({4})-Ownership Reporting of Gross executions Ref.Tit:{0}, Mic:{1}, Crt.Tit:{2}, Cli.Num:{3}",
                  rt.getReferenciaTitular(), rt.getCdPlataformaNegociacion(), rt.getCriterioComunicacionTitular(),
                  rt.getCdmiembromkt(), getPrincipalFileType());
          logBo.insertarRegistro(rt.getDesgloseCamara().getTmct0alc(), new Date(), new Date(), "", rt
              .getDesgloseCamara().getTmct0estadoByCdestadotit(), msg);
        }

        // tmpFileSent.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
      }
      while (page.getPageSize() == rts.size());
    }
    cuentasByCdCodigo.clear();
  }

  private boolean isOrsInEuroCcp(String ownerReference) throws SIBBACBusinessException {
    Date firstSentDate = staticDataBo.findFistDateSentValid(ownerReference);
    if (firstSentDate == null) {
      LOG.warn("ORS {} not found in euroccp database.", ownerReference);
      return false;
    }
    else {
      BigDecimal firstSentDateBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
          .convertDateToString(firstSentDate));
      BigDecimal nowBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
      if (firstSentDateBd.compareTo(nowBd) >= 0) {
        LOG.warn("ORS {} not found in euroccp database.", ownerReference);
        return false;
      }
    }
    return true;
  }

  @Override
  protected EuroCcpFileTypeEnum getPrincipalFileType() {
    return EuroCcpFileTypeEnum.ORG;
  }

  @Override
  protected EuroCcpFileTypeEnum getTmpFileType() {
    return EuroCcpFileTypeEnum.ORG_TMP;
  }

  @Override
  protected boolean getWriteFooterPrincipalFile() {
    return true;
  }

  @Override
  public void updateEstadoProcesado(EuroCcpFileSent principalFile, int estadoProcesado, String auditUser) {
    // TODO Auto-generated method stub

  }
}
