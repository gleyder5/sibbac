package sibbac.business.comunicaciones.euroccp.titulares.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAttribute;

import sibbac.database.DBConstants;
import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.ERROR_CODE)
@Entity
public class EuroCcpErrorCode implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8376994713011910011L;

  @Id
  @Column(name = "ERROR_CODE", length = 2, nullable = false)
  private String errorCode;

  /**
   * The version of the record.
   */
  @Version
  @Column(name = "VERSION", nullable = false)
  @XmlAttribute
  private Long version = 0L;

  /**
   * The user that performs the change.
   */
  @Column(name = "AUDIT_USER", nullable = false)
  @XmlAttribute
  private String auditUser = DBConstants.AUDIT_USER_DEFAULT;

  /**
   * The date instance when the change is performed.
   */
  @Column(name = "AUDIT_DATE", nullable = false)
  @XmlAttribute
  private Date auditDate = new Date();

  @Column(name = "ERROR_MESSAGE", length = 255, nullable = false)
  private String errorMessage;

  public EuroCcpErrorCode() {
  }

  public EuroCcpErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public Long getVersion() {
    return version;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  @Override
  public String toString() {
    return "EuroCcpErrorCode [errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
  }

}
