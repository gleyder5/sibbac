package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.commons.enums.TipoMovimiento;
import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.MovimientoEccS3Bo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.model.MovimientoEccS3;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TiposCuentaCompensacion;
import sibbac.common.TiposSibbac.TiposMovimiento;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.mo.MO;
import sibbac.pti.messages.mo.MOCommonDataBlock;
import sibbac.pti.messages.mo.MOControlBlock;
import sibbac.pti.messages.mo.R00;
import sibbac.pti.messages.mo.R03;
import sibbac.pti.messages.mo.R04;

@Service("ptiMessageProcessMoBo")
public class PtiMessageProcessMoBo extends PtiMessageProcess<MO> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessMoBo.class);

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private MovimientoEccS3Bo movS3Bo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCcBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  public PtiMessageProcessMoBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, MO mo, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[process] inicio");
    String outputPattern;
    String errorMsg = "";
    String msgLog = "";
    R00 r00;
    R03 r03;
    String referenciaMovimiento;
    String referenciaMovimientoEcc;
    Date fechaLiquidacion;
    Character sentido;
    String isin;
    List<Tmct0movimientoeccFidessa> listMovimientosFidessa;
    List<Tmct0movimientoecc> listMovimientosEcc;

    String cdestadomov = "";

    MOControlBlock controlblock = (MOControlBlock) mo.getControlBlock();
    MOCommonDataBlock moCommonDataBlock = (MOCommonDataBlock) mo.getCommonDataBlock();
    List<PTIMessageRecord> r00s = mo.getAll(PTIMessageRecordType.R00);
    // List< PTIMessageRecord > r01s = mo.getAll( PTIMessageRecordType.R01 );
    List<PTIMessageRecord> r03s = mo.getAll(PTIMessageRecordType.R03);
    List<PTIMessageRecord> r04s = mo.getAll(PTIMessageRecordType.R04);
    boolean error = false;
    if (controlblock.getCodigoDeError() != null && !controlblock.getCodigoDeError().equals("000")
        && !"".equals(controlblock.getCodigoDeError().trim()) && controlblock.getTextoDeError() != null
        && !"".equals(controlblock.getTextoDeError().trim())) {
      error = true;
      outputPattern = "%s: '%s' %s: '%s'";
      errorMsg = String.format(outputPattern, "Error MSG", controlblock.getCodigoDeError(), "Texto MSG",
          controlblock.getTextoDeError());

    }
    if (CollectionUtils.isNotEmpty(r00s)) {
      r00 = (R00) r00s.get(0);
      if (error) {
        try {
          r00.setEstado(EstadosEccMovimiento.RECHAZADO_ECC.text);
        }
        catch (PTIMessageException e) {
        }
      }
      cdestadomov = r00.getEstado();
      referenciaMovimiento = r00.getReferenciaDeMovimiento();
      referenciaMovimientoEcc = r00.getReferenciaDeMovimientoECC();
      fechaLiquidacion = moCommonDataBlock.getFechaLiquidacion();
      isin = r00.getCodigoValor();
      sentido = cache.getSentidoPtiSentidoSibbac(moCommonDataBlock.getSentido());

      if (StringUtils.isNotBlank(r00.getCuentaCompensacionDestino())) {
        outputPattern = "Recepcion Movimiento interno %s Ref.Mov: '%s' Ref.Mov.Ecc.: '%s' estado: '%s' Cta: '%s' Ref.Interna: '%s'";
        msgLog = String.format(outputPattern, errorMsg, referenciaMovimiento, referenciaMovimientoEcc, r00.getEstado(),
            r00.getCuentaCompensacionDestino(), r00.getReferenciaInternaDeAsignacion());
      }
      else {
        outputPattern = "Recepcion Movimiento Give-up %s Ref.Mov: '%s' Ref.Mov.Ecc.: '%s' estado: '%s' Miembro: '%s' Ref: '%s' Ref.Interna: '%s'";
        msgLog = String.format(outputPattern, errorMsg, referenciaMovimiento, referenciaMovimientoEcc, r00.getEstado(),
            r00.getMiembroDestinoDeLaAsignacion(), r00.getReferenciaDeAsignacion(),
            r00.getReferenciaInternaDeAsignacion());
      }
      LOG.debug("[process] {}", msgLog);

    }
    else if (CollectionUtils.isNotEmpty(r03s)) {
      r03 = (R03) r03s.get(0);
      if (error) {
        try {
          r03.setEstado(EstadosEccMovimiento.RECHAZADO_ECC.text);
        }
        catch (PTIMessageException e) {
        }
      }
      cdestadomov = r03.getEstado();
      referenciaMovimiento = r03.getReferenciaMovimiento();
      referenciaMovimientoEcc = r03.getReferenciaMovimientoEcc();
      fechaLiquidacion = moCommonDataBlock.getFechaLiquidacion();
      isin = r03.getCodigoValor();
      sentido = cache.getSentidoPtiSentidoSibbac(moCommonDataBlock.getSentido());
      if (r03.getTipoMovimiento().equals(TipoMovimiento.TAKE_UP.text)) {

        outputPattern = "Recepcion Movimiento Take-up %s Ref.Mov: '%s' Ref.Mov.Ecc.: '%s' estado: '%s' MiembroOrigen: '%s' MiembroDestino: '%s' Ref: '%s' Cta: '%s' Ref.Interna: '%s'";
        msgLog = String.format(outputPattern, errorMsg, referenciaMovimiento, referenciaMovimientoEcc, r03.getEstado(),
            r03.getMiembroOrigen(), r03.getMiembroDestino(), r03.getCuentaDeCompensacionDestino(),
            r03.getReferenciaAsignacion(), "");

        LOG.debug("[process] {}", msgLog);
      }

    }
    else {
      throw new SIBBACBusinessException("INCIDENCIA-MO formato no valido.");
    }
    if (StringUtils.isNotBlank(referenciaMovimientoEcc) || StringUtils.isNotBlank(referenciaMovimiento)) {

      listMovimientosEcc = new ArrayList<Tmct0movimientoecc>();
      listMovimientosFidessa = new ArrayList<Tmct0movimientoeccFidessa>();
      this.getAllMovimientos(fechaLiquidacion, referenciaMovimiento, referenciaMovimientoEcc, listMovimientosEcc,
          listMovimientosFidessa);

      if (CollectionUtils.isNotEmpty(r00s)) {
        r00 = (R00) r00s.get(0);
        this.procesarMovsf(sentido, isin, fechaLiquidacion, r00, r04s, listMovimientosFidessa, cache);
        this.engancharMovimientosDeOrdenConFidessa(sentido, isin, fechaLiquidacion, controlblock, r00, r04s,
            listMovimientosFidessa, listMovimientosEcc, msgLog, cache);
      }
      else if (CollectionUtils.isNotEmpty(r03s)) {
        r03 = (R03) r03s.get(0);
        this.procesarMovsf(sentido, isin, fechaLiquidacion, r03, r04s, listMovimientosFidessa, listMovimientosEcc,
            cache);

        this.engancharMovimientosDeOrdenConFidessa(sentido, isin, fechaLiquidacion, controlblock, r03, r04s,
            listMovimientosFidessa, listMovimientosEcc, msgLog, cache);
      }

      if (cdestadomov != null && cdestadomov.trim().equals(EstadosEccMovimiento.RECHAZADO_ECC.text)
      // || cdestadomov.trim().equals(EstadosEccMovimiento.CANCELADO.text)
      ) {
        this.bajaMovimientosRechazados(listMovimientosFidessa);
      }
    }
    LOG.debug("[process] fin");
  }

  @Transactional
  private void bajaMovimientosRechazados(List<Tmct0movimientoeccFidessa> listMovimientosFidessa) {
    for (Tmct0movimientoeccFidessa movf : listMovimientosFidessa) {
      movf.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
      movf.setAuditFechaCambio(new Date());
      movf.setAuditUser("SIBBAC20");
    }
    movfBo.save(listMovimientosFidessa);
  }

  @Transactional
  private void getAllMovimientos(Date fechaLiquidacion, String referenciaMovimiento, String referenciaMovimientoEcc,
      List<Tmct0movimientoecc> listMovimientosEcc, List<Tmct0movimientoeccFidessa> listMovimientosFidessa)
      throws SIBBACBusinessException {
    List<Tmct0movimientoecc> listMovimientosEccDesglosesActivos = new ArrayList<Tmct0movimientoecc>();
    if (referenciaMovimiento.startsWith(TiposMovimiento.MO)) {
      if (fechaLiquidacion == null) {
        listMovimientosFidessa.addAll(movfBo.findAllByCdrefmovimiento(referenciaMovimiento));
        listMovimientosEcc.addAll(movBo.findAllByCdrefmovimiento(referenciaMovimiento));
      }
      else {
        listMovimientosFidessa.addAll(movfBo.findAllByCdrefmovimientoAndFliquidacion(referenciaMovimiento,
            fechaLiquidacion));
        listMovimientosEcc.addAll(movBo.findAllByCdrefmovimiento(referenciaMovimiento));
      }
    }
    else {
      if (fechaLiquidacion == null) {
        listMovimientosFidessa.addAll(movfBo.findAllByCdrefmovimientoecc(referenciaMovimientoEcc));
      }
      else {
        listMovimientosFidessa.addAll(movfBo.findAllByCdrefmovimientoeccAndFliquidacion(referenciaMovimientoEcc,
            fechaLiquidacion));
      }
      listMovimientosEcc.addAll(movBo.findAllByCdrefmovimientoecc(referenciaMovimientoEcc));
    }

    BigDecimal fechaLiquidacionBd = null;
    if (fechaLiquidacion != null) {
      fechaLiquidacionBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
          .convertDateToString(fechaLiquidacion));
    }
    BigDecimal fechaLiquidacionDesgloseBd;
    for (Tmct0movimientoecc mov : listMovimientosEcc) {
      if (!mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)) {
        if (mov.getTmct0alo() != null
            && mov.getTmct0alo().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {
          fechaLiquidacionDesgloseBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
              .convertDateToString(mov.getTmct0alo().getFevalor()));
          if (fechaLiquidacionBd == null || fechaLiquidacionBd.compareTo(fechaLiquidacionDesgloseBd) == 0) {
            listMovimientosEccDesglosesActivos.add(mov);
          }
        }
        else if (mov.getTmct0desglosecamara() != null
            && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {
          fechaLiquidacionDesgloseBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
              .convertDateToString(mov.getTmct0desglosecamara().getFeliquidacion()));
          if (fechaLiquidacionBd == null || fechaLiquidacionBd.compareTo(fechaLiquidacionDesgloseBd) == 0) {
            listMovimientosEccDesglosesActivos.add(mov);
          }
        }
      }
    }
    listMovimientosEcc.clear();
    listMovimientosEcc.addAll(listMovimientosEccDesglosesActivos);
    listMovimientosEccDesglosesActivos.clear();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarMovsf(Character sentido, String isin, Date fechaLiquidacion, R00 r00,
      List<PTIMessageRecord> r04s, List<Tmct0movimientoeccFidessa> movsf, Tmct0xasAndTmct0cfgConfig cache) {
    LOG.debug("[procesarMovsf] inicio");
    boolean refMovNotInBBDD = CollectionUtils.isEmpty(movsf);
    R04 r04;
    for (PTIMessageRecord record : r04s) {
      r04 = (R04) record;
      if (refMovNotInBBDD) {
        LOG.debug("[procesarMovsf] Creando MovF Nuoperacionprev: {}", r04.getNumeroOperacion());
        Tmct0movimientoeccFidessa mov = new Tmct0movimientoeccFidessa();

        mov.setCdcamara(cache.getIdPti());
        mov.setSentido(sentido);
        mov.setCdisin(isin);
        mov.setFliquidacion(fechaLiquidacion);

        this.mappingData(r00, mov, cache);
        this.mappingData(r00, r04, mov);

        movsf.add(mov);
      }
      else {
        for (Tmct0movimientoeccFidessa mov : movsf) {
          if (StringUtils.isNotBlank(mov.getNuoperacionprev())
              && mov.getNuoperacionprev().trim().equals(r04.getNumeroOperacion())) {
            LOG.debug("[procesarMovsf] Actualizando MovF Nuoperacionprev: {}", r04.getNumeroOperacion());
            this.mappingData(r00, mov, cache);
            // this.mappingData(r00, r04, mov);
            mov.setCdoperacionecc(r04.getNumeroOperacionNuevo());
            mov.setNuoperacionprev(r04.getNumeroOperacion());
            break;
          }
        }
      }
    }
    movfBo.save(movsf);
    LOG.debug("[procesarMovsf] fin");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarMovsf(Character sentido, String isin, Date fechaLiquidacion, R03 r00,
      List<PTIMessageRecord> r04s, List<Tmct0movimientoeccFidessa> movsf, List<Tmct0movimientoecc> listMovimientosEcc,
      Tmct0xasAndTmct0cfgConfig cache) {
    LOG.debug("[procesarMovsf] inicio");
    boolean refMovNotInBBDD = CollectionUtils.isEmpty(movsf);
    boolean movimientosEccNotInBBDD = CollectionUtils.isEmpty(listMovimientosEcc);
    R04 r04;
    for (PTIMessageRecord record : r04s) {
      r04 = (R04) record;

      // movfBo.

      if (refMovNotInBBDD) {
        LOG.debug("[procesarMovsf] Creando MovF Nuoperacionprev: {}", r04.getNumeroOperacion());
        Tmct0movimientoeccFidessa mov = new Tmct0movimientoeccFidessa();

        mov.setCdcamara(cache.getIdPti());
        mov.setSentido(sentido);
        mov.setCdisin(isin);
        mov.setFliquidacion(fechaLiquidacion);

        this.mappingData(r00, mov, cache);
        this.mappingData(r00, r04, mov);
        mov = movfBo.save(mov);

        this.crearMovimientosTakeUp(mov, listMovimientosEcc);

        movsf.add(mov);
      }
      else {
        for (Tmct0movimientoeccFidessa mov : movsf) {
          if (StringUtils.isNotBlank(mov.getNuoperacionprev())
              && mov.getNuoperacionprev().trim().equals(r04.getNumeroOperacion())) {
            LOG.debug("[procesarMovsf] Actualizando MovF Nuoperacionprev: {}", r04.getNumeroOperacion());
            this.mappingData(r00, mov, cache);

            // if (r00.getEstado().equals(EstadosEccMovimiento.FINALIZADO.text))
            // {
            // this.mappingData(r00, r04, mov);
            // }
            // else {
            // mov.setImtitulosPendientesAsignar(BigDecimal.ZERO);
            // }
            mov.setCdoperacionecc(r04.getNumeroOperacionNuevo());
            mov.setNuoperacionprev(r04.getNumeroOperacion());
            if (movimientosEccNotInBBDD) {
              this.crearMovimientosTakeUp(mov, listMovimientosEcc);
            }
            break;
          }
        }
      }
    }
    movfBo.save(movsf);
    LOG.debug("[procesarMovsf] fin");
  }

  private void crearMovimientosTakeUp(Tmct0movimientoeccFidessa mov, List<Tmct0movimientoecc> listMovimientosEcc) {
    List<Tmct0desgloseclitit> listDesgloses = dctBo.findAllByCdoperacionecc(mov.getNuoperacionprev());
    for (Tmct0desgloseclitit desglose : listDesgloses) {
      Tmct0movimientoecc movEcc = crearMovimientoEcc(desglose, mov);

      Tmct0movimientooperacionnuevaecc movn = this.crearMovimientoOperacionNuevaEcc(desglose, mov, movEcc);
      movEcc.getTmct0movimientooperacionnuevaeccs().add(movn);
      listMovimientosEcc.add(movEcc);
    }

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void engancharMovimientosDeOrdenConFidessa(Character sentido, String isin, Date fechaLiquidacion,
      MOControlBlock moControlBlock, R00 r00, List<PTIMessageRecord> r04s, List<Tmct0movimientoeccFidessa> movsf,
      List<Tmct0movimientoecc> movs, String msgLog, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    String msgLogNoPermite;
    if (CollectionUtils.isNotEmpty(movs)) {
      Collection<Tmct0desglosecamara> dcs = new HashSet<Tmct0desglosecamara>();

      Collection<Tmct0alo> alos = new HashSet<Tmct0alo>();

      Collection<Object> ids = new HashSet<Object>();
      for (Tmct0movimientoecc mov : movs) {
        if (mov.getTmct0alo() != null
            && mov.getTmct0alo().getTmct0estadoByCdestadoasig().getIdestado() < EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
                .getId()
            || mov.getTmct0desglosecamara() != null
            && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() < EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
                .getId()) {

          if (StringUtils.isBlank(mov.getCdestadomov())
              || (mov.getCdestadomov().trim().equals(EstadosEccMovimiento.ENVIANDOSE_INT.text) || mov.getCdestadomov()
                  .trim().equals(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text))) {

            LOG.debug("[engancharMovimientosDeOrdenConFidessa] cambiando estado de mov id: {} de {} a {}",
                mov.getIdmovimiento(), mov.getCdestadomov(), r00.getEstado());
            mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
            mov.setCdestadomov(r00.getEstado());
            mov.setCdtipomov(r00.getTipoDeMovimiento());
            mov.setCdrefmovimientoecc(r00.getReferenciaDeMovimientoECC());
            mov.setCdrefnotificacion(r00.getReferenciaDeNotificacion());
            mov.setAuditFechaCambio(new Date());
            mov.setAuditUser("SIBBAC20");
            mov.setCderror(moControlBlock.getCodigoDeError());
            mov.setNberror(moControlBlock.getTextoDeError());
            mov = movBo.save(mov);

            if (mov.getTmct0alo() != null) {
              if (ids.add(mov.getTmct0alo().getId())) {
                alos.add(mov.getTmct0alo());
                logBo.insertarRegistro(mov.getTmct0alo(), new Date(), new Date(), "", mov.getTmct0alo()
                    .getTmct0estadoByCdestadoasig(), msgLog);
              }

            }
            else {
              if (ids.add(mov.getTmct0desglosecamara().getIddesglosecamara())) {
                dcs.add(mov.getTmct0desglosecamara());
                logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc(), new Date(), new Date(), "", mov
                    .getTmct0desglosecamara().getTmct0estadoByCdestadoasig(), msgLog);
              }
            }

            this.cargarCodigosOperacionMovimientos(r00, mov, movsf);
          }
          else {
            msgLogNoPermite = String.format("Movimiento:'%s' en estado '%s' no permite modificacion.",
                mov.getCdrefmovimiento(), mov.getCdestadomov());
            if (mov.getTmct0alo() != null) {
              logBo.insertarRegistro(mov.getTmct0alo(), new Date(), new Date(), "", mov.getTmct0alo()
                  .getTmct0estadoByCdestadoasig(), msgLogNoPermite);
            }
            else {
              logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc(), new Date(), new Date(), "", mov
                  .getTmct0desglosecamara().getTmct0estadoByCdestadoasig(), msgLogNoPermite);
            }
            LOG.warn(
                "[engancharMovimientosDeOrdenConFidessa] movimiento id: {} en estado: {} no permite su modificacion.",
                mov.getIdmovimiento(), mov.getCdestadomov());
          }
        }
        else {
          LOG.trace("[engancharMovimientosDeOrdenConFidessa] encontrado desglose que no permite la modificacion, se ignora.");
        }
      }
      movfBo.save(movsf);

      // for (Tmct0desglosecamara dc : dcs) {
      // if (dc.getTmct0estadoByCdestadoasig().getIdestado() !=
      // EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()) {
      // dc.setTmct0estadoByCdestadoasig(new
      // Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()));
      // dc.setAuditFechaCambio(new Date());
      // dc.setAuditUser("SIBBAC20");
      // }
      //
      // }
      // for (Tmct0alo tmct0alo : alos) {
      // if (tmct0alo.getTmct0estadoByCdestadoasig().getIdestado() !=
      // EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()) {
      // tmct0alo.setTmct0estadoByCdestadoasig(new
      // Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()));
      // tmct0alo.setFhaudit(new Date());
      // tmct0alo.setCdusuaud("SIBBAC20");
      // }
      // }
      this.revisarInformacionCompensacion(r00, new ArrayList<Tmct0desglosecamara>(dcs));

      aloBo.save(alos);
      ids.clear();
      dcs.clear();
      alos.clear();
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void engancharMovimientosDeOrdenConFidessa(Character sentido, String isin, Date fechaLiquidacion,
      MOControlBlock moControlBlock, R03 r03, List<PTIMessageRecord> r04s, List<Tmct0movimientoeccFidessa> movsf,
      List<Tmct0movimientoecc> movs, String msgLog, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    String msgLogNoPermite;
    if (CollectionUtils.isNotEmpty(movs)) {
      Collection<Tmct0desglosecamara> dcs = new HashSet<Tmct0desglosecamara>();

      Collection<Tmct0alo> alos = new HashSet<Tmct0alo>();

      Collection<Object> ids = new HashSet<Object>();
      for (Tmct0movimientoecc mov : movs) {
        if (mov.getTmct0alo() != null
            && mov.getTmct0alo().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()
            || mov.getTmct0desglosecamara() != null
            && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {

          if (StringUtils.isBlank(mov.getCdestadomov())
              || (mov.getCdestadomov().trim().equals(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR_ORIGEN.text) || mov
                  .getCdestadomov().trim().equals(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text))) {

            LOG.debug("[engancharMovimientosDeOrdenConFidessa] cambiando estado de mov id: {} de {} a {}",
                mov.getIdmovimiento(), mov.getCdestadomov(), r03.getEstado());
            mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
            mov.setCdestadomov(r03.getEstado());
            mov.setCdtipomov(r03.getTipoMovimiento());
            mov.setCdrefmovimientoecc(r03.getReferenciaMovimientoEcc());
            mov.setCdrefnotificacion(r03.getRefNotificacion());
            mov.setCderror(moControlBlock.getCodigoDeError());
            mov.setNberror(moControlBlock.getTextoDeError());
            mov.setCuentaCompensacionDestino(r03.getCuentaDeCompensacionDestino());
            mov.setMiembroCompensadorDestino(r03.getMiembroDestino());
            mov.setCodigoReferenciaAsignacion(r03.getReferenciaAsignacion());
            mov.setAuditFechaCambio(new Date());
            mov.setAuditUser("SIBBAC20");
            mov = movBo.save(mov);

            if (mov.getTmct0alo() != null) {
              if (ids.add(mov.getTmct0alo().getId())) {
                alos.add(mov.getTmct0alo());
                logBo.insertarRegistro(mov.getTmct0alo(), new Date(), new Date(), "", mov.getTmct0alo()
                    .getTmct0estadoByCdestadoasig(), msgLog);
              }

            }
            else {
              if (ids.add(mov.getTmct0desglosecamara().getIddesglosecamara())) {
                dcs.add(mov.getTmct0desglosecamara());
                logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc(), new Date(), new Date(), "", mov
                    .getTmct0desglosecamara().getTmct0estadoByCdestadoasig(), msgLog);
              }
            }

            this.cargarCodigosOperacionMovimientos(r03, mov, movsf);
          }
          else {
            msgLogNoPermite = String.format("Movimiento:'%s' en estado '%s' no permite modificacion.",
                mov.getCdrefmovimiento(), mov.getCdestadomov());
            if (mov.getTmct0alo() != null) {
              logBo.insertarRegistro(mov.getTmct0alo(), new Date(), new Date(), "", mov.getTmct0alo()
                  .getTmct0estadoByCdestadoasig(), msgLogNoPermite);
            }
            else {
              logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc(), new Date(), new Date(), "", mov
                  .getTmct0desglosecamara().getTmct0estadoByCdestadoasig(), msgLogNoPermite);
            }
            LOG.warn(
                "[engancharMovimientosDeOrdenConFidessa] movimiento id: {} en estado: {} no permite su modificacion.",
                mov.getIdmovimiento(), mov.getCdestadomov());
          }
        }
        else {
          LOG.trace("[engancharMovimientosDeOrdenConFidessa] encontrado desglose que no permite la modificacion, se ignora.");
        }
      }
      movfBo.save(movsf);

      // for (Tmct0desglosecamara dc : dcs) {
      // if (dc.getTmct0estadoByCdestadoasig().getIdestado() !=
      // EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()) {
      // dc.setTmct0estadoByCdestadoasig(new
      // Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()));
      // dc.setAuditFechaCambio(new Date());
      // dc.setAuditUser("SIBBAC20");
      // }
      //
      // }
      // for (Tmct0alo tmct0alo : alos) {
      // if (tmct0alo.getTmct0estadoByCdestadoasig().getIdestado() !=
      // EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()) {
      // tmct0alo.setTmct0estadoByCdestadoasig(new
      // Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
      // .getId()));
      // tmct0alo.setFhaudit(new Date());
      // tmct0alo.setCdusuaud("SIBBAC20");
      // }
      // }
      this.revisarInformacionCompensacion(r03, new ArrayList<Tmct0desglosecamara>(dcs));

      aloBo.save(alos);
      ids.clear();
      dcs.clear();
      alos.clear();
    }
  }

  @Transactional
  private void cargarCodigosOperacionMovimientos(R00 r00, Tmct0movimientoecc mov, List<Tmct0movimientoeccFidessa> movsf) {
    List<Tmct0movimientooperacionnuevaecc> movsn = movnBo.findByIdMovimiento(mov.getIdmovimiento());
    if (movsn != null && movsf != null) {
      for (Tmct0movimientooperacionnuevaecc movn : movsn) {
        for (Tmct0movimientoeccFidessa movf : movsf) {
          if (movf.getNuoperacionprev().trim().equals(movn.getNuoperacionprev().trim())
              || movf.getNuoperacionprev().trim().equals(movn.getCdoperacionecc().trim())) {
            // si no estaba enganchado lo enganchamos
            if (StringUtils.isNotBlank(movf.getCdoperacionecc())) {
              movn.setCdoperacionecc(movf.getCdoperacionecc());
              movn.setNuoperacionprev(movf.getNuoperacionprev());
              movn.setAuditFechaCambio(new Date());
              movn.setAuditUser("SIBBAC20");
            }

            if (mov.getTmct0alo() == null) {
              if (movn.getIdMovimientoFidessa() == null) {
                LOG.debug("[engancharMovimientosDeOrdenConFidessa] enganchando movn: {} con movf: {}",
                    movn.getIdmovimientooperacionnuevaec(), movf.getIdMovimientoFidessa());
                movf.setImtitulosPendientesAsignar(movf.getImtitulosPendientesAsignar().subtract(movn.getImtitulos()));
                movf.setAuditFechaCambio(new Date());
                movf.setAuditUser("SIBBAC20");
              }
              movn.setIdMovimientoFidessa(movf.getIdMovimientoFidessa());
              movn.setAuditFechaCambio(new Date());
              movn.setAuditUser("SIBBAC20");

              if (movn.getNudesglose() != null) {
                this.actualizarCodigosOperacionDesglose(movf, movn);

              }

            }
            break;
          }
        }
      }
      movnBo.save(movsn);
    }
  }

  @Transactional
  private void cargarCodigosOperacionMovimientos(R03 r03, Tmct0movimientoecc mov, List<Tmct0movimientoeccFidessa> movsf) {
    List<Tmct0movimientooperacionnuevaecc> movsn = movnBo.findByIdMovimiento(mov.getIdmovimiento());
    if (movsn != null && movsf != null) {
      for (Tmct0movimientooperacionnuevaecc movn : movsn) {
        for (Tmct0movimientoeccFidessa movf : movsf) {
          if (movf.getNuoperacionprev().trim().equals(movn.getNuoperacionprev())) {
            // si no estaba enganchado lo enganchamos
            if (StringUtils.isNotBlank(movf.getCdoperacionecc())) {
              movn.setCdoperacionecc(movf.getCdoperacionecc());
              movn.setAuditFechaCambio(new Date());
              movn.setAuditUser("SIBBAC20");
            }
            if (mov.getTmct0alo() == null) {
              if (movn.getIdMovimientoFidessa() == null) {
                LOG.debug("[engancharMovimientosDeOrdenConFidessa] enganchando movn: {} con movf: {}",
                    movn.getIdmovimientooperacionnuevaec(), movf.getIdMovimientoFidessa());
                movf.setImtitulosPendientesAsignar(movf.getImtitulosPendientesAsignar().subtract(movn.getImtitulos()));
                movf.setAuditFechaCambio(new Date());
                movf.setAuditUser("SIBBAC20");
              }
              movn.setIdMovimientoFidessa(movf.getIdMovimientoFidessa());
              movn.setAuditFechaCambio(new Date());
              movn.setAuditUser("SIBBAC20");
              if (movn.getNudesglose() != null) {
                this.actualizarCodigosOperacionDesglose(movf, movn);
              }
            }

            break;
          }
        }
      }
      movnBo.save(movsn);
    }
  }

  @Transactional
  private void actualizarCodigosOperacionDesglose(Tmct0movimientoeccFidessa movf, Tmct0movimientooperacionnuevaecc movn) {

    if (EstadosEccMovimiento.FINALIZADO.text.equals(movf.getCdestadomov().trim())
        || EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text.equals(movf.getCdestadomov().trim())) {
      LOG.debug("[engancharMovimientosDeOrdenConFidessa] buscando desglose {} para cargar los codigos de op ecc...",
          movn.getNudesglose());
      Tmct0desgloseclitit dct = dctBo.findById(movn.getNudesglose());
      if (dct != null) {
        if (EstadosEccMovimiento.FINALIZADO.text.equals(movf.getCdestadomov().trim())) {
          dct.setCdoperacionecc(movf.getCdoperacionecc());
          dct.setNuoperacionprev(movf.getNuoperacionprev());
          dct.setCuentaCompensacionDestino(movf.getCuentaCompensacionDestino());
          dct.setMiembroCompensadorDestino(movf.getMiembroCompensadorDestino());
          dct.setCodigoReferenciaAsignacion(movf.getCdRefAsignacion());
          dct.setAuditFechaCambio(new Date());
          dct.setAuditUser("SIBBAC20");
          dctBo.save(dct);
        }
        else {
          if (StringUtils.isBlank(dct.getCuentaCompensacionDestino())) {
            Tmct0anotacionecc an = anBo.findByCdoperacionecc(dct.getCdoperacionecc());
            if (an != null) {
              if(an.getTmct0CuentasDeCompensacion()!=null) {
                dct.setCuentaCompensacionDestino(an.getTmct0CuentasDeCompensacion().getCdCodigo());
                dct.setAuditFechaCambio(new Date());
                dct.setAuditUser("SIBBAC20");
                dctBo.save(dct);  
              }
              else {
                LOG.error("ERROR - La anotación no tiene cuenta de compensación");
              }
            }
          }
        }

      }
    }
  }

  @Transactional
  private void revisarInformacionCompensacion(R00 r00, List<Tmct0desglosecamara> dcs) throws SIBBACBusinessException {
    if (dcs != null && r00.getEstado().equals(EstadosEccMovimiento.FINALIZADO.text)) {
      for (Tmct0desglosecamara dc : dcs) {
        Tmct0infocompensacion ic = dc.getTmct0infocompensacion();
        if (ic != null) {
          if (ic.getCdctacompensacion() != null
              && !StringUtils.equals(ic.getCdctacompensacion().trim(), r00.getCuentaCompensacionDestino())
              || ic.getCdmiembrocompensador() != null
              && ic.getCdrefasignacion() != null
              && (!StringUtils.equals(ic.getCdmiembrocompensador().trim(), r00.getMiembroDestinoDeLaAsignacion()) || !StringUtils
                  .equals(ic.getCdrefasignacion().trim(), r00.getReferenciaDeAsignacion()))) {
            String msgCambioAsignacion = String
                .format(
                    "Cambiada informacion asignacion de cta: '%s' miembro: '%s' ref: '%s' a cta: '%s' miembro: '%s' ref: '%s'",
                    ic.getCdctacompensacion(), ic.getCdmiembrocompensador(), ic.getCdrefasignacion(),
                    r00.getCuentaCompensacionDestino(), r00.getMiembroDestinoDeLaAsignacion(),
                    r00.getReferenciaDeAsignacion());
            ic.setAuditFechaCambio(new Date());
            ic.setCdctacompensacion(r00.getCuentaCompensacionDestino());
            ic.setCdmiembrocompensador(r00.getMiembroDestinoDeLaAsignacion());
            ic.setCdrefasignacion(r00.getReferenciaDeAsignacion());
            ic = icDao.save(ic);
            LOG.debug(msgCambioAsignacion);
            logBo.insertarRegistro(dc.getTmct0alc(), new Date(), new Date(), "", dc.getTmct0estadoByCdestadoasig(),
                msgCambioAsignacion);
          }
        }
        dcBo.save(dc);
      }
    }
  }

  @Transactional
  private void revisarInformacionCompensacion(R03 r03, List<Tmct0desglosecamara> dcs) throws SIBBACBusinessException {
    if (dcs != null && r03.getEstado().equals(EstadosEccMovimiento.FINALIZADO.text)) {
      for (Tmct0desglosecamara dc : dcs) {
        Tmct0infocompensacion ic = dc.getTmct0infocompensacion();
        if (ic != null) {
          if (ic.getCdctacompensacion() != null
              && !StringUtils.equals(ic.getCdctacompensacion().trim(), r03.getCuentaDeCompensacionDestino())
              || ic.getCdmiembrocompensador() != null && ic.getCdrefasignacion() != null
              && (!StringUtils.equals(ic.getCdmiembrocompensador().trim(), r03.getMiembroDestino()))
              || !StringUtils.equals(ic.getCdrefasignacion().trim(), r03.getReferenciaAsignacion())) {
            String msgCambioAsignacion = String
                .format(
                    "Cambiada informacion asignacion de cta: '%s' miembro: '%s' ref: '%s' a cta: '%s' miembro: '%s' ref: '%s'",
                    ic.getCdctacompensacion(), ic.getCdmiembrocompensador(), ic.getCdrefasignacion(),
                    r03.getCuentaDeCompensacionDestino(), r03.getMiembroDestino(), r03.getReferenciaAsignacion());
            ic.setAuditFechaCambio(new Date());
            ic.setCdctacompensacion(r03.getCuentaDeCompensacionDestino());
            ic.setCdmiembrocompensador(r03.getMiembroDestino());
            ic.setCdrefasignacion(r03.getReferenciaAsignacion());
            ic = icDao.save(ic);
            LOG.debug(msgCambioAsignacion);
            logBo.insertarRegistro(dc.getTmct0alc(), new Date(), new Date(), "", dc.getTmct0estadoByCdestadoasig(),
                msgCambioAsignacion);
          }
        }
        dcBo.save(dc);
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingData(R00 r00, R04 r04, Tmct0movimientoeccFidessa mov) {
    Tmct0anotacionecc an = null;
    List<Tmct0anotacionecc> ans;
    mov.setFcontratacion(r04.getFechaNegociacion());
    mov.setFnegociacion(r04.getFechaNegociacion());
    mov.setFechaOrden(r04.getFechaDeLaOrdenDeMercado());
    mov.setCdoperacionecc(r04.getNumeroOperacionNuevo());
    mov.setNuoperacionprev(r04.getNumeroOperacion());

    if (r00.getReferenciaDeMovimiento().startsWith(TiposMovimiento.MO)) {
      MovimientoEccS3 movS3 = movS3Bo.findByCdrefmovimientoAndNuoperacionprev(r00.getReferenciaDeMovimiento(),
          r04.getNumeroOperacion());
      if (movS3 != null) {
        mov.setEnvios3('S');
        movS3.setAudit_fecha_cambio(new Date());
        movS3.setAudit_user("SIBBAC20");
        movS3.setCdoperacionecc(r04.getNumeroOperacionNuevo());
        movS3.setCdestadomov(r00.getEstado());
        movS3Bo.save(movS3);
      }
      else {
        mov.setEnvios3('N');
      }
    }
    else {
      mov.setEnvios3('N');
    }

    mov.setPlataforma(r04.getIdentificacionDeLaPlataforma());
    mov.setSegmento(r04.getSegmento());
    mov.setCdOperacionMercado(r04.getCodigoDeOperacionMercado());
    mov.setMiembroMercado(r04.getMiembroMercado());
    mov.setImtitulos(r04.getNumValoresImporteNominal());
    mov.setImtitulosPendientesAsignar(r04.getNumValoresImporteNominal());
    mov.setPrecio(r04.getPrecioOperacion());
    if (r00.getNumeroDeValoresImporteNominal().compareTo(r04.getNumValoresImporteNominal()) != 0) {
      mov.setImefectivo(r04.getNumValoresImporteNominal().multiply(r04.getPrecioOperacion())
          .setScale(2, BigDecimal.ROUND_HALF_UP));
    }
    else {
      mov.setImefectivo(r00.getEfectivo());
    }
    if (r04.getNumerDeLaOrdenDeMercado() != null && r04.getNumerDeLaOrdenDeMercado().compareTo(BigDecimal.ZERO) != 0) {
      mov.setNuordenmkt(r04.getNumerDeLaOrdenDeMercado() + "");
    }
    if (r04.getEjecucionDeMercado() != null && r04.getEjecucionDeMercado().compareTo(BigDecimal.ZERO) != 0) {
      mov.setNuexemkt(r04.getEjecucionDeMercado() + "");
    }

    if (mov.getFcontratacion() == null || mov.getFechaOrden() == null || mov.getNuexemkt() == null
        || mov.getNuordenmkt() == null || mov.getHoraOrden() == null) {
      ans = anBo.findByNuoperacionprev(r04.getNumeroOperacion());

      if (CollectionUtils.isNotEmpty(ans)) {
        an = ans.get(0);
        ans.clear();
      }
      if (an != null) {
        mov.setFcontratacion(an.getFcontratacion());
        mov.setFnegociacion(an.getFnegociacion());
        mov.setPlataforma(an.getCdPlataformaNegociacion());
        mov.setCdindcotizacion(an.getCdindcotizacion());
        mov.setHoraOrden(an.getHordenmkt());
        mov.setFechaOrden(an.getFordenmkt());
        mov.setIndicadorCapacidad(an.getCdindcapacidad());
        mov.setNuordenmkt(an.getNuordenmkt());
        mov.setNuexemkt(an.getNuexemkt());
      }
    }

    mov.setAuditFechaCambio(new Date());
    mov.setAuditUser("SIBBAC20");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingData(R00 r00, Tmct0movimientoeccFidessa mov, Tmct0xasAndTmct0cfgConfig cache) {
    mov.setCdtipomov(r00.getTipoDeMovimiento());

    mov.setCdrefmovimiento(r00.getReferenciaDeMovimiento());
    mov.setCdrefmovimientoecc(r00.getReferenciaDeMovimientoECC());
    mov.setCdrefnotificacion(r00.getReferenciaDeNotificacion());
    mov.setCdrefinternaasig(r00.getReferenciaInternaDeAsignacion());
    mov.setCdrefasignacion(r00.getReferenciaDeAsignacion());
    mov.setCdRefAsignacion(r00.getReferenciaDeAsignacion());
    mov.setUsuarioOrigen(r00.getUsuarioOrigen());
    mov.setMiembroDestino(r00.getMiembroDestino());
    mov.setUsuarioDestino(r00.getUsuarioDestino());
    mov.setMiembroCompensadorDestino(r00.getMiembroDestinoDeLaAsignacion());
    mov.setCdestadomov(r00.getEstado());
    mov.setCuentaCompensacionDestino(r00.getCuentaCompensacionDestino());
    mov.setMiembroDestino(r00.getMiembroDestino());

    if (isImputacionPropia(r00.getCuentaCompensacionDestino(), r00.getEstado(), r00.getTipoDeMovimiento(),
        r00.getUsuarioOrigen(), r00.getMiembroDestinoDeLaAsignacion(), cache)) {
      mov.setImputacionPropia('S');
    }
    else {
      mov.setImputacionPropia('N');
    }

    mov.setAuditFechaCambio(new Date());
    mov.setAuditUser("SIBBAC20");

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void mappingData(R03 r03, R04 r04, Tmct0movimientoeccFidessa mov) {
    Tmct0anotacionecc an = null;
    mov.setFcontratacion(r04.getFechaNegociacion());
    mov.setFnegociacion(r04.getFechaNegociacion());
    mov.setFechaOrden(r04.getFechaDeLaOrdenDeMercado());
    mov.setCdoperacionecc(r04.getNumeroOperacionNuevo());
    mov.setNuoperacionprev(r04.getNumeroOperacion());

    if (r03.getReferenciaMovimiento().startsWith(TiposMovimiento.MO)) {
      MovimientoEccS3 movS3 = movS3Bo.findByCdrefmovimientoAndNuoperacionprev(r03.getReferenciaMovimiento(),
          r04.getNumeroOperacion());
      if (movS3 != null) {
        mov.setEnvios3('S');
        movS3.setAudit_fecha_cambio(new Date());
        movS3.setAudit_user("SIBBAC20");
        movS3.setCdoperacionecc(r04.getNumeroOperacionNuevo());
        movS3.setCdestadomov(r03.getEstado());
        movS3Bo.save(movS3);
      }
      else {
        mov.setEnvios3('N');
      }
    }
    else {
      mov.setEnvios3('N');
    }

    mov.setPlataforma(r04.getIdentificacionDeLaPlataforma());
    mov.setSegmento(r04.getSegmento());
    mov.setCdOperacionMercado(r04.getCodigoDeOperacionMercado());
    mov.setMiembroMercado(r04.getMiembroMercado());
    mov.setImtitulos(r04.getNumValoresImporteNominal());
    mov.setImtitulosPendientesAsignar(r04.getNumValoresImporteNominal());
    mov.setPrecio(r04.getPrecioOperacion());
    if (r03.getAcumuladoNumeroValoresImporteNominal().compareTo(r04.getNumValoresImporteNominal()) != 0) {
      mov.setImefectivo(r04.getNumValoresImporteNominal().multiply(r04.getPrecioOperacion())
          .setScale(2, BigDecimal.ROUND_HALF_UP));
    }
    else {
      mov.setImefectivo(r03.getEfectivo());
    }
    if (r04.getNumerDeLaOrdenDeMercado() != null && r04.getNumerDeLaOrdenDeMercado().compareTo(BigDecimal.ZERO) != 0) {
      mov.setNuordenmkt(r04.getNumerDeLaOrdenDeMercado() + "");
    }
    if (r04.getEjecucionDeMercado() != null && r04.getEjecucionDeMercado().compareTo(BigDecimal.ZERO) != 0) {
      mov.setNuexemkt(r04.getEjecucionDeMercado() + "");
    }

    if (mov.getFcontratacion() == null || mov.getFechaOrden() == null || mov.getNuexemkt() == null
        || mov.getNuordenmkt() == null || mov.getHoraOrden() == null) {
      an = anBo.findByCdoperacionecc(r04.getNumeroOperacion());

      if (an != null) {
        mov.setFcontratacion(an.getFcontratacion());
        mov.setFnegociacion(an.getFnegociacion());
        mov.setPlataforma(an.getCdPlataformaNegociacion());
        mov.setCdindcotizacion(an.getCdindcotizacion());
        mov.setHoraOrden(an.getHordenmkt());
        mov.setFechaOrden(an.getFordenmkt());
        mov.setIndicadorCapacidad(an.getCdindcapacidad());
        mov.setNuordenmkt(an.getNuordenmkt());
        mov.setNuexemkt(an.getNuexemkt());
      }
    }

    mov.setAuditFechaCambio(new Date());
    mov.setAuditUser("SIBBAC20");
  }

  @Transactional
  private void mappingData(R03 r03, Tmct0movimientoeccFidessa mov, Tmct0xasAndTmct0cfgConfig cache) {
    mov.setCdtipomov(r03.getTipoMovimiento());

    mov.setCdrefmovimiento(r03.getReferenciaMovimiento());
    mov.setCdrefmovimientoecc(r03.getReferenciaMovimientoEcc());
    mov.setCdrefnotificacion(r03.getRefNotificacion());
    mov.setCdrefinternaasig("");
    mov.setCdrefasignacion(r03.getReferenciaAsignacion());
    mov.setCdRefAsignacion(r03.getReferenciaAsignacion());
    mov.setUsuarioOrigen(r03.getUsuarioOrigen());
    mov.setMiembroDestino(r03.getMiembroDestino());
    mov.setUsuarioDestino(r03.getUsuarioDestino());
    mov.setMiembroCompensadorDestino(r03.getMiembroDestino());
    mov.setCdestadomov(r03.getEstado());
    mov.setCuentaCompensacionDestino(r03.getCuentaDeCompensacionDestino());
    mov.setMiembroOrigen(r03.getMiembroOrigen());

    if (isImputacionPropia(r03.getCuentaDeCompensacionDestino(), r03.getEstado(), r03.getTipoMovimiento(),
        r03.getUsuarioOrigen(), r03.getMiembroDestino(), cache)) {
      mov.setImputacionPropia('S');
    }
    else {
      mov.setImputacionPropia('N');
    }

    mov.setAuditFechaCambio(new Date());
    mov.setAuditUser("SIBBAC20");

  }

  private Tmct0movimientoecc crearMovimientoEcc(Tmct0desgloseclitit desglose, Tmct0movimientoeccFidessa mov) {
    Tmct0movimientoecc movimientoEcc = new Tmct0movimientoecc();
    // movimientoEcc.setFk_movimientoecc_fidessa( mov );
    movimientoEcc.setTmct0desglosecamara(desglose.getTmct0desglosecamara());
    movimientoEcc.setCdestadomov(mov.getCdestadomov());
    movimientoEcc.setCdtipomov(mov.getCdtipomov());
    movimientoEcc.setCdindcotizacion(desglose.getCdindcotizacion());
    movimientoEcc.setCdrefmovimiento(mov.getCdrefmovimiento());
    movimientoEcc.setCdrefinternaasig(mov.getCdrefinternaasig());
    movimientoEcc.setCdrefmovimientoecc(mov.getCdrefmovimientoecc());
    movimientoEcc.setCdrefnotificacion(mov.getCdrefnotificacion());
    movimientoEcc.setCdrefnotificacionprev(mov.getCdrefnotificacionprev());
    movimientoEcc.setImtitulos(desglose.getImtitulos().setScale(2, BigDecimal.ROUND_HALF_UP));
    movimientoEcc.setImefectivo(desglose.getImefectivo());
    movimientoEcc.setAuditFechaCambio(new Date());
    movimientoEcc.setSentido(desglose.getCdsentido());
    movimientoEcc.setIsin(desglose.getCdisin());
    movimientoEcc.setMiembroCompensadorDestino(mov.getMiembroCompensadorDestino());
    movimientoEcc.setCodigoReferenciaAsignacion(mov.getCdRefAsignacion());
    movimientoEcc.setCuentaCompensacionDestino(mov.getCuentaCompensacionDestino());
    movimientoEcc.setAuditUser("SIBBAC20");
    return movimientoEcc;
  }

  private Tmct0movimientooperacionnuevaecc crearMovimientoOperacionNuevaEcc(Tmct0desgloseclitit desglose,
      Tmct0movimientoeccFidessa mov, Tmct0movimientoecc movimientoEcc) {
    Tmct0movimientooperacionnuevaecc operacionNuevaEcc = new Tmct0movimientooperacionnuevaecc();
    operacionNuevaEcc.setTmct0movimientoecc(movimientoEcc);
    operacionNuevaEcc.setCdmiembromkt(desglose.getCdmiembromkt());
    operacionNuevaEcc.setCdoperacionecc(mov.getCdoperacionecc());
    operacionNuevaEcc.setNuoperacionprev(mov.getNuoperacionprev());
    operacionNuevaEcc.setNuoperacioninic(desglose.getNuoperacioninic());
    operacionNuevaEcc.setImefectivo(desglose.getImefectivo().setScale(2, BigDecimal.ROUND_HALF_UP));
    operacionNuevaEcc.setImtitulos(desglose.getImtitulos().setScale(2, BigDecimal.ROUND_HALF_UP));
    operacionNuevaEcc.setImprecio(desglose.getImprecio().setScale(6, BigDecimal.ROUND_HALF_UP));
    operacionNuevaEcc.setNudesglose(desglose.getNudesglose());
    operacionNuevaEcc.setAuditFechaCambio(new Date());
    operacionNuevaEcc.setAuditUser("SIBBAC20");
    operacionNuevaEcc.setIdMovimientoFidessa(mov.getIdMovimientoFidessa());
    return operacionNuevaEcc;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean isImputacionPropia(String cuentaCompensacion, String cdestadomov, String tipomov,
      String usuarioOrigen, String miembroDestino, Tmct0xasAndTmct0cfgConfig cache) {

    if (StringUtils.isNotBlank(cuentaCompensacion) && cdestadomov != null && tipomov != null && miembroDestino != null) {
      Tmct0CuentasDeCompensacion cuentaCompensacionJdo = cuentaCcBo.findByCdCodigo(cuentaCompensacion);

      String entidad = cache.getPtiAppIdentityDataEntidad();

      String usuarioOridenImputacionPropia = cache.getPtiUsuarioOrigenImputacionPropia();

      if (cuentaCompensacionJdo != null && cuentaCompensacionJdo.getTipoCuentaLiquidacion() != null
          && usuarioOrigen != null) {
        if (TiposCuentaCompensacion.PROPIA
            .equals(cuentaCompensacionJdo.getTipoCuentaLiquidacion().getCdCodigo().trim())
            && EstadosEccMovimiento.FINALIZADO.text.equals(cdestadomov.trim())
            && TiposMovimiento.ASIGNACION_EXTERNA.equals(tipomov.trim())
            && entidad.equals(miembroDestino.trim())
            && usuarioOridenImputacionPropia.equals(usuarioOrigen.trim())) {
          return true;
        }
      }
      return false;
    }
    else {
      return false;
    }

  }
}
