package sibbac.business.comunicaciones.ordenes.bo;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.GZIPInputStream;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenForeingMarketReceptionException;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenInternalReceptionException;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessageType;
import sibbac.common.jms.JmsMessagesBo;

import com.sv.sibbac.multicenter.Orders;

@Service
public class LoadXmlOrdenFidessaFromInputsSubListBo {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(LoadXmlOrdenFidessaFromInputsSubListBo.class);

  private static final Logger LOG_XML_RECEIVE = LoggerFactory.getLogger("XMLS_RECEIVE");
  private static final Logger LOG_XML_REJECT = LoggerFactory.getLogger("XMLS_REJECT");

  @Value("${sibbac.fidessa.xml.in.charset:UTF8}")
  private Charset xmlCharset;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsMessagesBo;

  @Autowired
  private LoadXmlOrdenFidessaInputStreamBo processXml;

  public LoadXmlOrdenFidessaFromInputsSubListBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean consumesFromMq(final Session session, final MessageConsumer messageConsumer,
      final MessageProducer messageProducer, int transactionSize, int consumeTimeout,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws IOException, JMSException, JAXBException,
      SIBBACBusinessException {
    boolean res = true;
    for (int i = 0; i < transactionSize; i++) {

      Message message = jmsMessagesBo.receiveMessage(messageConsumer, consumeTimeout);
      if (message != null) {
        // Files.createTempFile(arg0, arg1, arg2, arg3)
        try (InputStream is = jmsMessagesBo.getInputStream(message)) {
          res &= this.processGz(session, messageProducer, is, cachedConfig);
        }
        message.acknowledge();
        session.commit();
      }
      else {
        break;
      }

    }

    return res;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean processGz(final Session session, final MessageProducer messageProducer, List<InputStream> iss,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws IOException, JMSException, JAXBException {
    boolean res = true;
    for (InputStream inputStream : iss) {
      res &= this.processGz(session, messageProducer, inputStream, cachedConfig);
    }
    return res;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean processIs(final Session session, final MessageProducer messageProducer, List<InputStream> iss,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws JMSException, IOException, JAXBException {
    boolean res = true;
    for (InputStream inputStream : iss) {
      res &= this.processIs(session, messageProducer, inputStream, cachedConfig);
    }
    return res;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean processGz(final Session session, final MessageProducer messageProducer, InputStream is,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws IOException, JMSException, JAXBException {
    boolean res = true;
    try (GZIPInputStream gis = new GZIPInputStream(is);) {
      res &= this.processIs(session, messageProducer, gis, cachedConfig);
    }
    return res;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean processIs(final Session session, final MessageProducer messageProducer, InputStream is,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws JMSException, IOException, JAXBException {

    LOG.debug("[processIs] leyendo fichero...");
    boolean res = false;

    StringBuffer sb = new StringBuffer();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(is, xmlCharset))) {

      String linea;
      while ((linea = br.readLine()) != null) {
        sb.append(linea).append("\n");
      }
    }
    LOG_XML_RECEIVE.info(sb.toString());
    Orders order = null;
    Tmct0bok bok;
    try (InputStream isToload = new ByteArrayInputStream(sb.toString().getBytes())) {
      try {
        LOG.debug("[processIs] parseando fichero...");
        order = processXml.parseAndValidate(isToload, cachedConfig);
        bok = processXml.loadxmlNewTransaction(order, cachedConfig);
        // if (bok.getTmct0ord().getCdclsmdo() == 'E') {
        // processXml.operativaInternacionalCargaXml(order.getOrder(),
        // bok.getId());
        // }
        res = true;
      }
      catch (XmlOrdenExceptionReception e) {

        String reject;
        LOG.debug(e.getMessage(), e);
        reject = processXml.createRejection(e.getNuorden(), e.getNbooking(), e.getNucnfclt(), e.getDsobserv());
        sb.append("\n").append(reject).append("\n").append(e.getMessage()).append("\n").append(e).append("\n");

        jmsMessagesBo.sendJmsMessage(messageProducer,
            jmsMessagesBo.createMessage(session, JmsMessageType.TEXT, new ByteArrayInputStream(reject.getBytes())));
        LOG_XML_REJECT.error(sb.toString());
      }
      catch (XmlOrdenInternalReceptionException e) {

        sb.append("\n").append(e.getMessage()).append("\n").append(e).append("\n");
        LOG.warn(sb.toString(), e);
        // LOG_XML_REJECT.error(sb.toString());
      }
      catch (XmlOrdenForeingMarketReceptionException e) {
        sb.append("\n").append(e.getMessage()).append("\n").append(e).append("\n");
        LOG.warn(sb.toString(), e);
        // LOG_XML_REJECT.error(sb.toString());
      }
      catch (Exception e) {
        if (order != null && order.getOrder() != null) {
          String nuorden = order.getOrder().getNuorden();
          String nbooking = "";
          if (order.getOrder().getBooking() != null) {
            nbooking = order.getOrder().getBooking().getNbooking();
          }
          String reject;
          reject = processXml.createRejection(nuorden, nbooking, "", e.getMessage());
          sb.append("\n").append(reject).append("\n").append(e.getMessage()).append("\n").append(e).append("\n");

          jmsMessagesBo.sendJmsMessage(messageProducer,
              jmsMessagesBo.createMessage(session, JmsMessageType.TEXT, new ByteArrayInputStream(reject.getBytes())));
          LOG.debug(sb.toString(), e);
          LOG_XML_REJECT.error(sb.toString());
        }
        else {
          LOG.debug(sb.toString(), e);
        }

      }
      finally {
      }

    }
    return res;
  }
}
