package sibbac.business.comunicaciones.pti.commons.dto;

import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.pti.PTIMessage;

public class PtiMessageFileReaderDTO extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 670007794531571902L;

  private PTIMessage msg;

  public PtiMessageFileReaderDTO(PTIMessage msg) {
    this.setMsg(msg);
  }

  public PTIMessage getMsg() {
    return msg;
  }

  public void setMsg(PTIMessage msg) {
    this.msg = msg;
  }

  @Override
  public String getTipoRegistro() {
    return msg.getTipo().name();
  }

  @Override
  public String toString() {
    return "PtiMessageFileReaderDTO [msg=" + msg + ", toString()=" + super.toString() + "]";
  }

  
  
}
