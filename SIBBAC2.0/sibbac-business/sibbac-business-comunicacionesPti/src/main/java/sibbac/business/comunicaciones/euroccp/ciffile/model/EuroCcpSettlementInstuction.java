package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.SETTLEMENT_INSTRUCTION)
@Entity
public class EuroCcpSettlementInstuction extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = true)
  private String accountNumber;

  @Column(name = "PRODUCT_GROUP_CODE", length = 2, nullable = false)
  private String productGroupCode;

  @Column(name = "EXCHANGE_CODE_TRADE", length = 4, nullable = true)
  private String exchangeCodeTrade;

  @Column(name = "SYMBOL", length = 6, nullable = false)
  private String symbol;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;

  @Column(name = "DELIVER_RECEIVE_CODE", length = 3, nullable = false)
  private String deliverReceiveCode;

  @Column(name = "TRANSACTION_QUANTITY", length = 12, scale = 2, nullable = true)
  private BigDecimal transactionQuantity;

  @Column(name = "STAMP_DUTY_IND", length = 1, nullable = true)
  private Character stampDutyInd;

  @Column(name = "SETTLEMENT_AMOUNT", length = 18, scale = 2, nullable = true)
  private BigDecimal settlementAmount;

  @Column(name = "SETTLEMENT_AMOUNT_DC", length = 1, nullable = true)
  private Character settlementAmountDC;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRANSACTION_DATE", nullable = false)
  private Date transactionDate;

  @Temporal(TemporalType.DATE)
  @Column(name = "SETTLEMENT_DATE", nullable = false)
  private Date settlementDate;

  @Column(name = "ISIN_CODE", length = 12, nullable = false)
  private String isinCode;

  @Column(name = "SETTLEMENT_INSTRUCTION_REFERENCE", length = 9, nullable = true)
  private String settlementInstructionReference;

  @Column(name = "DEPOT_ID", length = 6, nullable = false)
  private String depotId;

  @Column(name = "PLACE_SAFEKEEPING", length = 11, nullable = false)
  private String placeSafekeeping;

  @Column(name = "PLACE_SETTLEMENT", length = 11, nullable = false)
  private String placeSettlement;

  @Column(name = "BUYER_SELLER_CONTEXT", length = 8, nullable = true)
  private String buyerSellerContext;

  @Column(name = "BUYER_SELLER_CODE", length = 11, nullable = true)
  private String buyerSellerCode;

  @Column(name = "BUYER_SELLER_ACCOUNT_CODE", length = 12, nullable = true)
  private String buyerSellerAccountCode;

  @Column(name = "REC_DEL_AGENT_CONTEXT", length = 8, nullable = true)
  private String recDelAgentContext;

  @Column(name = "REC_DEL_AGENT_CODE", length = 11, nullable = true)
  private String recDelAgentCode;

  @Column(name = "REC_DEL_AGENT_ACCOUNT_CODE", length = 12, nullable = true)
  private String recDelAgentAccountCode;

  @Column(name = "GSI_STATUS", length = 9, nullable = true)
  private String gsiStatus;

  @Column(name = "GSI_STATUS_REASON", length = 9, nullable = true)
  private String gsiStatusReason;

  @Column(name = "GSI_TYPE", length = 2, nullable = false)
  private String gsiType;

  @Column(name = "SEND_INDICATOR", length = 1, nullable = false)
  private Character sendIndicator;

  @Column(name = "ORIGINAL_INSTRUCTION_REFERENCE", length = 9, nullable = true)
  private String originalInstructionReference;

  @Column(name = "PREVIOUS_INSTRUCTION_REFERENCE", length = 9, nullable = true)
  private String previousInstructionReference;

  @Column(name = "INSTRUCTION_ID", length = 35, nullable = true)
  private String instructionId;

  @Column(name = "REFERENCE_CUSTODIAN", length = 35, nullable = true)
  private String referenceCustodian;

  @Column(name = "AVERAGE_PRICE", length = 18, scale = 7, nullable = true)
  private BigDecimal averagePrice;

  @Column(name = "SETTLEMENT_FEE", length = 18, scale = 2, nullable = true)
  private BigDecimal settlementFee;

  @Column(name = "SETTLEMENT_FEE_DC", length = 1, nullable = true)
  private Character settlementFeeDC;

  @Column(name = "SETTLEMENT_FEE_CURRENCY_CODE", length = 3, nullable = true)
  private String settlementFeeCurrencyCode;

  @Column(name = "FAIL_FEE", length = 18, scale = 2, nullable = true)
  private BigDecimal failFee;

  @Column(name = "FAIL_FEE_DC", length = 1, nullable = true)
  private Character failFeeDC;

  @Column(name = "FAIL_FEE_CURRENCY_CODE", length = 3, nullable = true)
  private String failFeeCurrencyCode;

  @Column(name = "TYPE_FIELD", length = 1, nullable = true)
  private Character typeField;

  @Temporal(TemporalType.DATE)
  @Column(name = "EXPIRATION_DATE", nullable = true)
  private Date expirationDate;

  @Column(name = "EXERCISE_PRICE", length = 15, scale = 7, nullable = true)
  private BigDecimal exercisePrice;

  @Column(name = "FILLER", length = 95, nullable = true)
  private String filler;

  public EuroCcpSettlementInstuction() {
  }

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public String getExchangeCodeTrade() {
    return exchangeCodeTrade;
  }

  public String getSymbol() {
    return symbol;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getDeliverReceiveCode() {
    return deliverReceiveCode;
  }

  public BigDecimal getTransactionQuantity() {
    return transactionQuantity;
  }

  public Character getStampDutyInd() {
    return stampDutyInd;
  }

  public BigDecimal getSettlementAmount() {
    return settlementAmount;
  }

  public Character getSettlementAmountDC() {
    return settlementAmountDC;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public Date getSettlementDate() {
    return settlementDate;
  }

  public String getIsinCode() {
    return isinCode;
  }

  public String getSettlementInstructionReference() {
    return settlementInstructionReference;
  }

  public String getDepotId() {
    return depotId;
  }

  public String getPlaceSafekeeping() {
    return placeSafekeeping;
  }

  public String getPlaceSettlement() {
    return placeSettlement;
  }

  public String getBuyerSellerContext() {
    return buyerSellerContext;
  }

  public String getBuyerSellerCode() {
    return buyerSellerCode;
  }

  public String getBuyerSellerAccountCode() {
    return buyerSellerAccountCode;
  }

  public String getRecDelAgentContext() {
    return recDelAgentContext;
  }

  public String getRecDelAgentCode() {
    return recDelAgentCode;
  }

  public String getRecDelAgentAccountCode() {
    return recDelAgentAccountCode;
  }

  public String getGsiStatus() {
    return gsiStatus;
  }

  public String getGsiStatusReason() {
    return gsiStatusReason;
  }

  public String getGsiType() {
    return gsiType;
  }

  public Character getSendIndicator() {
    return sendIndicator;
  }

  public String getOriginalInstructionReference() {
    return originalInstructionReference;
  }

  public String getPreviousInstructionReference() {
    return previousInstructionReference;
  }

  public String getInstructionId() {
    return instructionId;
  }

  public String getReferenceCustodian() {
    return referenceCustodian;
  }

  public BigDecimal getAveragePrice() {
    return averagePrice;
  }

  public BigDecimal getSettlementFee() {
    return settlementFee;
  }

  public Character getSettlementFeeDC() {
    return settlementFeeDC;
  }

  public String getSettlementFeeCurrencyCode() {
    return settlementFeeCurrencyCode;
  }

  public BigDecimal getFailFee() {
    return failFee;
  }

  public Character getFailFeeDC() {
    return failFeeDC;
  }

  public String getFailFeeCurrencyCode() {
    return failFeeCurrencyCode;
  }

  public Character getTypeField() {
    return typeField;
  }

  public Date getExpirationDate() {
    return expirationDate;
  }

  public BigDecimal getExercisePrice() {
    return exercisePrice;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public void setExchangeCodeTrade(String exchangeCodeTrade) {
    this.exchangeCodeTrade = exchangeCodeTrade;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setDeliverReceiveCode(String deliverReceiveCode) {
    this.deliverReceiveCode = deliverReceiveCode;
  }

  public void setTransactionQuantity(BigDecimal transactionQuantity) {
    this.transactionQuantity = transactionQuantity;
  }

  public void setStampDutyInd(Character stampDutyInd) {
    this.stampDutyInd = stampDutyInd;
  }

  public void setSettlementAmount(BigDecimal settlementAmount) {
    this.settlementAmount = settlementAmount;
  }

  public void setSettlementAmountDC(Character settlementAmountDC) {
    this.settlementAmountDC = settlementAmountDC;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setSettlementDate(Date settlementDate) {
    this.settlementDate = settlementDate;
  }

  public void setIsinCode(String isinCode) {
    this.isinCode = isinCode;
  }

  public void setSettlementInstructionReference(String settlementInstructionReference) {
    this.settlementInstructionReference = settlementInstructionReference;
  }

  public void setDepotId(String depotId) {
    this.depotId = depotId;
  }

  public void setPlaceSafekeeping(String placeSafekeeping) {
    this.placeSafekeeping = placeSafekeeping;
  }

  public void setPlaceSettlement(String placeSettlement) {
    this.placeSettlement = placeSettlement;
  }

  public void setBuyerSellerContext(String buyerSellerContext) {
    this.buyerSellerContext = buyerSellerContext;
  }

  public void setBuyerSellerCode(String buyerSellerCode) {
    this.buyerSellerCode = buyerSellerCode;
  }

  public void setBuyerSellerAccountCode(String buyerSellerAccountCode) {
    this.buyerSellerAccountCode = buyerSellerAccountCode;
  }

  public void setRecDelAgentContext(String recDelAgentContext) {
    this.recDelAgentContext = recDelAgentContext;
  }

  public void setRecDelAgentCode(String recDelAgentCode) {
    this.recDelAgentCode = recDelAgentCode;
  }

  public void setRecDelAgentAccountCode(String recDelAgentAccountCode) {
    this.recDelAgentAccountCode = recDelAgentAccountCode;
  }

  public void setGsiStatus(String gsiStatus) {
    this.gsiStatus = gsiStatus;
  }

  public void setGsiStatusReason(String gsiStatusReason) {
    this.gsiStatusReason = gsiStatusReason;
  }

  public void setGsiType(String gsiType) {
    this.gsiType = gsiType;
  }

  public void setSendIndicator(Character sendIndicator) {
    this.sendIndicator = sendIndicator;
  }

  public void setOriginalInstructionReference(String originalInstructionReference) {
    this.originalInstructionReference = originalInstructionReference;
  }

  public void setPreviousInstructionReference(String previousInstructionReference) {
    this.previousInstructionReference = previousInstructionReference;
  }

  public void setInstructionId(String instructionId) {
    this.instructionId = instructionId;
  }

  public void setReferenceCustodian(String referenceCustodian) {
    this.referenceCustodian = referenceCustodian;
  }

  public void setAveragePrice(BigDecimal averagePrice) {
    this.averagePrice = averagePrice;
  }

  public void setSettlementFee(BigDecimal settlementFee) {
    this.settlementFee = settlementFee;
  }

  public void setSettlementFeeDC(Character settlementFeeDC) {
    this.settlementFeeDC = settlementFeeDC;
  }

  public void setSettlementFeeCurrencyCode(String settlementFeeCurrencyCode) {
    this.settlementFeeCurrencyCode = settlementFeeCurrencyCode;
  }

  public void setFailFee(BigDecimal failFee) {
    this.failFee = failFee;
  }

  public void setFailFeeDC(Character failFeeDC) {
    this.failFeeDC = failFeeDC;
  }

  public void setFailFeeCurrencyCode(String failFeeCurrencyCode) {
    this.failFeeCurrencyCode = failFeeCurrencyCode;
  }

  public void setTypeField(Character typeField) {
    this.typeField = typeField;
  }

  public void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }

  public void setExercisePrice(BigDecimal exercisePrice) {
    this.exercisePrice = exercisePrice;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }
  
  
}
