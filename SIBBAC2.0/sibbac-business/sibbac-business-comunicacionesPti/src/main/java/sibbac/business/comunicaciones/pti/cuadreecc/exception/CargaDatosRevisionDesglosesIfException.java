package sibbac.business.comunicaciones.pti.cuadreecc.exception;

import sibbac.common.SIBBACBusinessException;

public class CargaDatosRevisionDesglosesIfException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = 4578938144499352624L;

  public CargaDatosRevisionDesglosesIfException() {
    // TODO Auto-generated constructor stub
  }

  public CargaDatosRevisionDesglosesIfException(String message,
                                                Throwable cause,
                                                boolean enableSuppression,
                                                boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

  public CargaDatosRevisionDesglosesIfException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public CargaDatosRevisionDesglosesIfException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public CargaDatosRevisionDesglosesIfException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
