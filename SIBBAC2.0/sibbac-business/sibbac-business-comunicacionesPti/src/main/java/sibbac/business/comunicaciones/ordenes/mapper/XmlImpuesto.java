package sibbac.business.comunicaciones.ordenes.mapper;

import java.util.Map.Entry;

import sibbac.business.wrappers.database.model.Impuesto;
import sibbac.business.wrappers.database.model.ImpuestoId;
import sibbac.business.wrappers.database.model.Tmct0gal;

public class XmlImpuesto {

  private Impuesto impuesto;

  public XmlImpuesto(Tmct0gal tmct0gal) {
    impuesto = new Impuesto();
    impuesto.setId(new ImpuestoId(tmct0gal.getId().getNuorden(), tmct0gal.getId().getNbooking(),
                                  tmct0gal.getId().getNuagreje(), tmct0gal.getId().getNucnfclt(), null));
    impuesto.setTmct0gal(tmct0gal);

  }

  public void xml_mapping(Entry<String, Object> entry) {
    impuesto.getId().setNombre(entry.getKey());
  }

  public Impuesto getJDOObject() {
    return impuesto;
  }
}
