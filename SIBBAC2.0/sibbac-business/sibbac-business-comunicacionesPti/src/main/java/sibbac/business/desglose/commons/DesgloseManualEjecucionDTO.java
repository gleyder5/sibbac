package sibbac.business.desglose.commons;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;

public class DesgloseManualEjecucionDTO implements java.io.Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 8721882628649563988L;
  private long idgrupoeje;
  private String nuejecuc;
  private String cdoperacion;
  private String nurefexemkt;
  private String cdcamara;
  private String cdsegmento;
  private char cdsentido;
  private BigDecimal imtitulos = BigDecimal.ZERO;
  private BigDecimal imprecio = BigDecimal.ZERO;
  private BigDecimal imefectivo = BigDecimal.ZERO;
  private Character cdindcotizacion;
  private String cddivisa;
  private String cdisin;
  private Date hoordenmkt;
  private Date feordenmkt;
  private String nuordenmkt;
  private String cdPlataformaNegociacion;
  private BigDecimal imtotbru = BigDecimal.ZERO;
  private BigDecimal imcanoncompdv = BigDecimal.ZERO;
  private BigDecimal imcanoncompeu = BigDecimal.ZERO;
  private BigDecimal imcanoncontrdv = BigDecimal.ZERO;
  private BigDecimal imcanoncontreu = BigDecimal.ZERO;
  private BigDecimal imcanonliqdv = BigDecimal.ZERO;
  private BigDecimal imcanonliqeu = BigDecimal.ZERO;
  private String cdmiembromkt;
  private String tpopebol;
  private String numeroOperacionDCV;
  private String miembroCompensadorDestino;
  private String cuentaCompensacionDestino;
  private String codigoReferenciaAsignacion;

  protected java.lang.String cdoperacionecc;
  protected java.lang.String nuoperacionprev;

  protected java.lang.String nuoperacioninic;

  protected BigDecimal imcomisn = BigDecimal.ZERO;

  /**
   * Numero de titulos fallidos si el sentido de la operacion es 'venta' y
   * numero de titulos afectados si el sentido de la operacion es 'compra'.
   */
  private BigDecimal nutitfallidos = BigDecimal.ZERO;

  /** Fecha de ejecucion. */
  private Date feejecuc;

  /** Hora de ejecucion. */
  private Date hoejecuc;

  private BigDecimal imfinsvb = BigDecimal.ZERO;

  private BigDecimal imcomsvb = BigDecimal.ZERO;
  private BigDecimal imcombco = BigDecimal.ZERO;
  private BigDecimal imcomdvo = BigDecimal.ZERO;
  private BigDecimal imntbrok = BigDecimal.ZERO;
  private BigDecimal imnetliq = BigDecimal.ZERO;
  private BigDecimal imnfiliq = BigDecimal.ZERO;
  private BigDecimal imcombrk = BigDecimal.ZERO;
  private BigDecimal imajusvb = BigDecimal.ZERO;
  private BigDecimal imtotnet = BigDecimal.ZERO;
  private BigDecimal imcomsis = BigDecimal.ZERO;
  private BigDecimal imbansis = BigDecimal.ZERO;

  protected Character isEjecucionPartida = 'N';

  private Integer asignadocomp;

  private Date feliquidacion;

  public DesgloseManualEjecucionDTO() {

  }

  public DesgloseManualEjecucionDTO(Tmct0desgloseclitit other) {
    this.idgrupoeje = other.getIdgrupoeje();
    this.nuejecuc = other.getNuejecuc();
    this.nurefexemkt = other.getNurefexemkt();
    this.nuordenmkt = other.getNuordenmkt();
    this.cdcamara = other.getCdcamara();
    this.cdsegmento = other.getCdsegmento();
    this.cdmiembromkt = other.getCdmiembromkt();
    this.tpopebol = other.getTpopebol();
    this.numeroOperacionDCV = other.getNumeroOperacionDCV();
    this.miembroCompensadorDestino = other.getMiembroCompensadorDestino();
    this.codigoReferenciaAsignacion = other.getCodigoReferenciaAsignacion();
    this.cuentaCompensacionDestino = other.getCuentaCompensacionDestino();
    this.cdsentido = other.getCdsentido();
    this.imtitulos = other.getImtitulos();
    this.imprecio = other.getImprecio();
    this.imefectivo = other.getImefectivo();
    this.cdindcotizacion = other.getCdindcotizacion();
    this.cddivisa = other.getCddivisa();
    this.cdisin = other.getCdisin();
    this.feejecuc = other.getFeejecuc();
    this.hoejecuc = other.getHoejecuc();
    this.hoordenmkt = other.getHoordenmkt();
    this.feordenmkt = other.getFeordenmkt();
    this.cdPlataformaNegociacion = other.getCdPlataformaNegociacion();
    this.cdoperacion = other.getCdoperacionecc();
    this.cdoperacionecc = other.getCdoperacionecc();
    this.nuoperacionprev = other.getCdoperacionecc();
    this.nuoperacioninic = other.getCdoperacionecc();

    this.imcanoncompdv = other.getImcanoncompdv();
    this.imcanoncompeu = other.getImcanoncompeu();
    this.imcanoncontrdv = other.getImcanoncontrdv();
    this.imcanoncontreu = other.getImcanoncontreu();
    this.imcanonliqdv = other.getImcanonliqdv();
    this.imcanonliqeu = other.getImcanonliqeu();
    this.imajusvb = other.getImajusvb();
    this.imbansis = other.getImbansis();
    this.imcombco = other.getImcombco();
    this.imcombrk = other.getImcombrk();
    this.imcomdvo = other.getImcomdvo();
    this.imcomisn = other.getImcomisn();
    this.imcomsis = other.getImcomsis();
    this.imcomsvb = other.getImcomsvb();
    this.imfinsvb = other.getImfinsvb();
    this.imcomsvb = other.getImcomsvb();
    this.imnetliq = other.getImnetliq();
    this.imnfiliq = other.getImnfiliq();
    this.imntbrok = other.getImntbrok();
    this.imtotbru = other.getImtotbru();
    this.imtotnet = other.getImtotnet();
    if (other.getTmct0desglosecamara() != null) {
      this.feliquidacion = other.getTmct0desglosecamara().getFeliquidacion();
    }
  }

  public DesgloseManualEjecucionDTO(DesgloseManualEjecucionDTO other) {
    this.idgrupoeje = other.getIdgrupoeje();
    this.nuejecuc = other.getNuejecuc();
    this.nurefexemkt = other.getNurefexemkt();
    this.nuordenmkt = other.getNuordenmkt();
    this.cdcamara = other.getCdcamara();
    this.cdsegmento = other.getCdsegmento();
    this.cdmiembromkt = other.getCdmiembromkt();
    this.tpopebol = other.getTpopebol();
    this.numeroOperacionDCV = other.getNumeroOperacionDCV();
    this.miembroCompensadorDestino = other.getMiembroCompensadorDestino();
    this.codigoReferenciaAsignacion = other.getCodigoReferenciaAsignacion();
    this.cuentaCompensacionDestino = other.getCuentaCompensacionDestino();
    this.cdsentido = other.getCdsentido();
    this.imtitulos = other.getImtitulos();
    this.imprecio = other.getImprecio();
    this.imefectivo = other.getImefectivo();
    this.cdindcotizacion = other.getCdindcotizacion();
    this.cddivisa = other.getCddivisa();
    this.cdisin = other.getCdisin();
    this.feejecuc = other.getFeejecuc();
    this.hoejecuc = other.getHoejecuc();
    this.hoordenmkt = other.getHoordenmkt();
    this.feordenmkt = other.getFeordenmkt();
    this.cdPlataformaNegociacion = other.getCdPlataformaNegociacion();
    this.cdoperacion = other.getCdoperacionecc();
    this.cdoperacionecc = other.getCdoperacionecc();
    this.nuoperacionprev = other.getCdoperacionecc();
    this.nuoperacioninic = other.getCdoperacionecc();

    this.imcanoncompdv = other.getImcanoncompdv();
    this.imcanoncompeu = other.getImcanoncompeu();
    this.imcanoncontrdv = other.getImcanoncontrdv();
    this.imcanoncontreu = other.getImcanoncontreu();
    this.imcanonliqdv = other.getImcanonliqdv();
    this.imcanonliqeu = other.getImcanonliqeu();
    this.imajusvb = other.getImajusvb();
    this.imbansis = other.getImbansis();
    this.imcombco = other.getImcombco();
    this.imcombrk = other.getImcombrk();
    this.imcomdvo = other.getImcomdvo();
    this.imcomisn = other.getImcomisn();
    this.imcomsis = other.getImcomsis();
    this.imcomsvb = other.getImcomsvb();
    this.imfinsvb = other.getImfinsvb();
    this.imcomsvb = other.getImcomsvb();
    this.imnetliq = other.getImnetliq();
    this.imnfiliq = other.getImnfiliq();
    this.imntbrok = other.getImntbrok();
    this.imtotbru = other.getImtotbru();
    this.imtotnet = other.getImtotnet();
    this.feliquidacion = other.getFevalor();
  }

  public DesgloseManualEjecucionDTO(Tmct0grupoejecucion gr, Tmct0ejecucionalocation ealo, Tmct0eje eje,
      Tmct0caseoperacioneje caseOp, Tmct0operacioncdecc op, Tmct0CuentasDeCompensacion cuenta) {
    this.idgrupoeje = gr.getIdgrupoeje();
    this.nuejecuc = eje.getId().getNuejecuc();
    this.nurefexemkt = eje.getNurefere();
    this.nuordenmkt = eje.getNuopemer();
    this.cdcamara = eje.getClearingplatform();
    this.cdsegmento = eje.getSegmenttradingvenue();
    this.cdmiembromkt = eje.getCdMiembroMkt();
    this.tpopebol = eje.getTpopebol();
    this.numeroOperacionDCV = "";
    this.miembroCompensadorDestino = "";
    this.codigoReferenciaAsignacion = "";
    this.cuentaCompensacionDestino = "";
    this.cdsentido = eje.getCdtpoper();
    this.imtitulos = ealo.getNutitulos();
    this.imprecio = eje.getImcbmerc();
    this.imefectivo = imtitulos.multiply(imprecio).setScale(2, RoundingMode.HALF_UP);
    this.cdindcotizacion = eje.getIndCotizacion();
    this.cddivisa = eje.getCdmoniso();
    this.cdisin = eje.getCdisin();
    this.feejecuc = eje.getFeejecuc();
    this.hoejecuc = eje.getHoejecuc();
    this.hoordenmkt = eje.getHoordenmkt();
    this.feordenmkt = eje.getFeordenmkt();
    this.feliquidacion = eje.getFevalor();
    this.cdPlataformaNegociacion = eje.getExecutionTradingVenue();
    if (op != null) {
      this.cdoperacion = op.getCdoperacionecc();
      this.cdoperacionecc = op.getCdoperacionecc();
      this.nuoperacionprev = op.getCdoperacionecc();
      this.nuoperacioninic = op.getCdoperacionecc();
    }
    if (cuenta != null) {
      this.cuentaCompensacionDestino = cuenta.getCdCodigo();
    }

    // TODO hay que hacer el reparto de canones en la carga del xml, los campos
    // en ejealo ya existen
    this.asignarPorcentajeCanones(eje);
  }

  private void asignarPorcentajeCanones(Tmct0eje eje) {
    BigDecimal porcentaje = imtitulos.divide(eje.getNutiteje(), 16, RoundingMode.HALF_UP);
    if (eje.getImCanonCompDv() != null) {
      this.imcanoncompdv = eje.getImCanonCompDv().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP);
    }
    if (eje.getImCanonCompEu() != null) {
      this.imcanoncompeu = eje.getImCanonCompEu().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP);
    }
    if (eje.getImCanonContrDv() != null) {
      this.imcanoncontrdv = eje.getImCanonContrDv().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP);
    }
    if (eje.getImCanonContrEu() != null) {
      this.imcanoncontreu = eje.getImCanonContrEu().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP);
    }
    if (eje.getImCanonLiqDv() != null) {
      this.imcanonliqdv = eje.getImCanonLiqDv().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP);
    }
    if (eje.getImCanonLiqEu() != null) {
      this.imcanonliqeu = eje.getImCanonLiqEu().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP);
    }
  }

  public long getIdgrupoeje() {
    return idgrupoeje;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public String getCdoperacion() {
    return cdoperacion;
  }

  public String getNurefexemkt() {
    return nurefexemkt;
  }

  public String getCdcamara() {
    return cdcamara;
  }

  public String getCdsegmento() {
    return cdsegmento;
  }

  public char getCdsentido() {
    return cdsentido;
  }

  public BigDecimal getImtitulos() {
    return imtitulos;
  }

  public BigDecimal getImprecio() {
    return imprecio;
  }

  public BigDecimal getImefectivo() {
    return imefectivo;
  }

  public Character getCdindcotizacion() {
    return cdindcotizacion;
  }

  public String getCddivisa() {
    return cddivisa;
  }

  public String getCdisin() {
    return cdisin;
  }

  public Date getHoordenmkt() {
    return hoordenmkt;
  }

  public Date getFeordenmkt() {
    return feordenmkt;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getCdPlataformaNegociacion() {
    return cdPlataformaNegociacion;
  }

  public BigDecimal getImtotbru() {
    return imtotbru;
  }

  public BigDecimal getImcanoncompdv() {
    return imcanoncompdv;
  }

  public BigDecimal getImcanoncompeu() {
    return imcanoncompeu;
  }

  public BigDecimal getImcanoncontrdv() {
    return imcanoncontrdv;
  }

  public BigDecimal getImcanoncontreu() {
    return imcanoncontreu;
  }

  public BigDecimal getImcanonliqdv() {
    return imcanonliqdv;
  }

  public BigDecimal getImcanonliqeu() {
    return imcanonliqeu;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public String getTpopebol() {
    return tpopebol;
  }

  public String getNumeroOperacionDCV() {
    return numeroOperacionDCV;
  }

  public String getMiembroCompensadorDestino() {
    return miembroCompensadorDestino;
  }

  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public String getCodigoReferenciaAsignacion() {
    return codigoReferenciaAsignacion;
  }

  public java.lang.String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public java.lang.String getNuoperacionprev() {
    return nuoperacionprev;
  }

  public java.lang.String getNuoperacioninic() {
    return nuoperacioninic;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public BigDecimal getNutitfallidos() {
    return nutitfallidos;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public Date getHoejecuc() {
    return hoejecuc;
  }

  public BigDecimal getImfinsvb() {
    return imfinsvb;
  }

  public BigDecimal getImcomsvb() {
    return imcomsvb;
  }

  public BigDecimal getImcombco() {
    return imcombco;
  }

  public BigDecimal getImcomdvo() {
    return imcomdvo;
  }

  public BigDecimal getImntbrok() {
    return imntbrok;
  }

  public BigDecimal getImnetliq() {
    return imnetliq;
  }

  public BigDecimal getImnfiliq() {
    return imnfiliq;
  }

  public BigDecimal getImcombrk() {
    return imcombrk;
  }

  public BigDecimal getImajusvb() {
    return imajusvb;
  }

  public BigDecimal getImtotnet() {
    return imtotnet;
  }

  public BigDecimal getImcomsis() {
    return imcomsis;
  }

  public BigDecimal getImbansis() {
    return imbansis;
  }

  public Character getIsEjecucionPartida() {
    return isEjecucionPartida;
  }

  public void setIdgrupoeje(long idgrupoeje) {
    this.idgrupoeje = idgrupoeje;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public void setCdoperacion(String cdoperacion) {
    this.cdoperacion = cdoperacion;
  }

  public void setNurefexemkt(String nurefexemkt) {
    this.nurefexemkt = nurefexemkt;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  public void setCdsentido(char cdsentido) {
    this.cdsentido = cdsentido;
  }

  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  public void setImefectivo(BigDecimal imefectivo) {
    this.imefectivo = imefectivo;
  }

  public void setCdindcotizacion(Character cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public void setHoordenmkt(Date hoordenmkt) {
    this.hoordenmkt = hoordenmkt;
  }

  public void setFeordenmkt(Date feordenmkt) {
    this.feordenmkt = feordenmkt;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  public void setImtotbru(BigDecimal imtotbru) {
    this.imtotbru = imtotbru;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  public void setTpopebol(String tpopebol) {
    this.tpopebol = tpopebol;
  }

  public void setNumeroOperacionDCV(String numeroOperacionDCV) {
    this.numeroOperacionDCV = numeroOperacionDCV;
  }

  public void setMiembroCompensadorDestino(String miembroCompensadorDestino) {
    this.miembroCompensadorDestino = miembroCompensadorDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

  public void setCodigoReferenciaAsignacion(String codigoReferenciaAsignacion) {
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
  }

  public void setCdoperacionecc(java.lang.String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public void setNuoperacionprev(java.lang.String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  public void setNuoperacioninic(java.lang.String nuoperacioninic) {
    this.nuoperacioninic = nuoperacioninic;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public void setNutitfallidos(BigDecimal nutitfallidos) {
    this.nutitfallidos = nutitfallidos;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public void setImfinsvb(BigDecimal imfinsvb) {
    this.imfinsvb = imfinsvb;
  }

  public void setImcomsvb(BigDecimal imcomsvb) {
    this.imcomsvb = imcomsvb;
  }

  public void setImcombco(BigDecimal imcombco) {
    this.imcombco = imcombco;
  }

  public void setImcomdvo(BigDecimal imcomdvo) {
    this.imcomdvo = imcomdvo;
  }

  public void setImntbrok(BigDecimal imntbrok) {
    this.imntbrok = imntbrok;
  }

  public void setImnetliq(BigDecimal imnetliq) {
    this.imnetliq = imnetliq;
  }

  public void setImnfiliq(BigDecimal imnfiliq) {
    this.imnfiliq = imnfiliq;
  }

  public void setImcombrk(BigDecimal imcombrk) {
    this.imcombrk = imcombrk;
  }

  public void setImajusvb(BigDecimal imajusvb) {
    this.imajusvb = imajusvb;
  }

  public void setImtotnet(BigDecimal imtotnet) {
    this.imtotnet = imtotnet;
  }

  public void setImcomsis(BigDecimal imcomsis) {
    this.imcomsis = imcomsis;
  }

  public void setImbansis(BigDecimal imbansis) {
    this.imbansis = imbansis;
  }

  public void setIsEjecucionPartida(Character isEjecucionPartida) {
    this.isEjecucionPartida = isEjecucionPartida;
  }

  public Integer getAsignadocomp() {
    return asignadocomp;
  }

  public void setAsignadocomp(Integer asignadocomp) {
    this.asignadocomp = asignadocomp;
  }

  public Date getFevalor() {
    return feliquidacion;
  }

  public void setFevalor(Date fevalor) {
    this.feliquidacion = fevalor;
  }

  public Tmct0desgloseclitit getNewDesglose() {
    Tmct0desgloseclitit dct = new Tmct0desgloseclitit();
    dct.setIdgrupoeje(getIdgrupoeje());
    dct.setNuejecuc(getNuejecuc());
    dct.setNurefexemkt(getNurefexemkt());
    dct.setNuordenmkt(getNuordenmkt());
    dct.setCdcamara(getCdcamara());
    dct.setCdsegmento(getCdsegmento());
    dct.setCdmiembromkt(getCdmiembromkt());
    dct.setTpopebol(getTpopebol());
    dct.setNumeroOperacionDCV(getNumeroOperacionDCV());
    dct.setMiembroCompensadorDestino(getMiembroCompensadorDestino());
    dct.setCodigoReferenciaAsignacion(getCodigoReferenciaAsignacion());
    dct.setCuentaCompensacionDestino(getCuentaCompensacionDestino());
    dct.setCdsentido(getCdsentido());
    dct.setImtitulos(getImtitulos());
    dct.setImprecio(getImprecio());
    dct.setImefectivo(getImefectivo());
    dct.setCdindcotizacion(getCdindcotizacion());
    dct.setCddivisa(getCddivisa());
    dct.setCdisin(getCdisin());
    dct.setFeejecuc(getFeejecuc());
    dct.setHoejecuc(getHoejecuc());
    dct.setHoordenmkt(getHoordenmkt());
    dct.setFeordenmkt(getFeordenmkt());
    dct.setCdPlataformaNegociacion(getCdPlataformaNegociacion());
    dct.setCdoperacion(getCdoperacionecc());
    dct.setCdoperacionecc(getCdoperacionecc());
    dct.setNuoperacionprev(getNuoperacionprev());
    dct.setNuoperacioninic(getNuoperacioninic());

    dct.setImcanoncompdv(getImcanoncompdv());
    dct.setImcanoncompeu(getImcanoncompeu());
    dct.setImcanoncontrdv(getImcanoncontrdv());
    dct.setImcanoncontreu(getImcanoncontreu());
    dct.setImcanonliqdv(getImcanonliqdv());
    dct.setImcanonliqeu(getImcanonliqeu());
    dct.setImajusvb(getImajusvb());
    dct.setImbansis(getImbansis());
    dct.setImcombco(getImcombco());
    dct.setImcombrk(getImcombrk());
    dct.setImcomdvo(getImcomdvo());
    dct.setImcomisn(getImcomisn());
    dct.setImcomsis(getImcomsis());
    dct.setImcomsvb(getImcomsvb());
    dct.setImfinsvb(getImfinsvb());
    dct.setImcomsvb(getImcomsvb());
    dct.setImnetliq(getImnetliq());
    dct.setImnfiliq(getImnfiliq());
    dct.setImntbrok(getImntbrok());
    dct.setImtotbru(getImtotbru());
    dct.setImtotnet(getImtotnet());
    return dct;

  }

  @Override
  public String toString() {
    return "DesgloseManualEjecucionDTO [ idgrupoeje=" + idgrupoeje + ", nuejecuc=" + nuejecuc + ", cdoperacion="
        + cdoperacion + ", nurefexemkt=" + nurefexemkt + ", cdcamara=" + cdcamara + ", cdsegmento=" + cdsegmento
        + ", cdsentido=" + cdsentido + ", imtitulos=" + imtitulos + ", imprecio=" + imprecio + ", cddivisa=" + cddivisa
        + ", cdisin=" + cdisin + ", cdPlataformaNegociacion=" + cdPlataformaNegociacion + ", tpopebol=" + tpopebol
        + ", numeroOperacionDCV=" + numeroOperacionDCV + ", cdoperacionecc=" + cdoperacionecc + ", nuoperacionprev="
        + nuoperacionprev + ", nuoperacioninic=" + nuoperacioninic + ", isEjecucionPartida=" + isEjecucionPartida + "]";
  }

}
