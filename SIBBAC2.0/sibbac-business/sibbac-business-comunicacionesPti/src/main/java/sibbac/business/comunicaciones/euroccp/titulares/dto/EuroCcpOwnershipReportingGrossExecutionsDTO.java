package sibbac.business.comunicaciones.euroccp.titulares.dto;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReportingGrossExecutions;

public class EuroCcpOwnershipReportingGrossExecutionsDTO extends EuroCcpCommonSendRegisterDTO {

  private Date tradeDate;

  private String executionReference;

  private String mic;

  private String accountNumber;

  private String ownerReferenceFrom = "";

  private String ownerReferenceTo;

  private BigDecimal numberShares;

  private Character processingStatus;

  private String errorCode;

  private String errorMessage;

  private String filler;

  public EuroCcpOwnershipReportingGrossExecutionsDTO() {
    super(EuroCcpCommonSendRegisterTypeEnum.RECORD);
  }

  public EuroCcpOwnershipReportingGrossExecutionsDTO(EuroCcpOwnershipReportingGrossExecutions hb) {
    this();
    setTradeDate(hb.getTradeDate());
    setExecutionReference(hb.getExecutionReference());
    setMic(hb.getMic());
    setAccountNumber(hb.getAccountNumber());
    setOwnerReferenceFrom(hb.getOwnerReferenceFrom());
    setOwnerReferenceTo(hb.getOwnerReferenceTo());
    setNumberShares(hb.getNumberShares());
    setProcessingStatus(hb.getProcessingStatus());
    if (hb.getErrorCode() != null) {
      setErrorCode(hb.getErrorCode().getErrorCode());
      setErrorMessage(hb.getErrorCode().getErrorMessage());
    }
    setFiller(hb.getFiller());
  }

  public Date getTradeDate() {
    return tradeDate;
  }

  public String getExecutionReference() {
    return executionReference;
  }

  public String getMic() {
    return mic;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getOwnerReferenceFrom() {
    return ownerReferenceFrom;
  }

  public String getOwnerReferenceTo() {
    return ownerReferenceTo;
  }

  public BigDecimal getNumberShares() {
    return numberShares;
  }

  public Character getProcessingStatus() {
    return processingStatus;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public String getFiller() {
    return filler;
  }

  public void setTradeDate(Date tradeDate) {
    this.tradeDate = tradeDate;
  }

  public void setExecutionReference(String executionReference) {
    this.executionReference = executionReference;
  }

  public void setMic(String mic) {
    this.mic = mic;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setOwnerReferenceFrom(String ownerReferenceFrom) {
    this.ownerReferenceFrom = ownerReferenceFrom;
  }

  public void setOwnerReferenceTo(String ownerReferenceTo) {
    this.ownerReferenceTo = ownerReferenceTo;
  }

  public void setNumberShares(BigDecimal numberShares) {
    this.numberShares = numberShares;
  }

  public void setProcessingStatus(Character processingStatus) {
    this.processingStatus = processingStatus;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

  @Override
  public String toString() {
    return "EuroCcpOwnershipReportingGrossExecutionsDTO [tradeDate=" + tradeDate + ", executionReference="
        + executionReference + ", mic=" + mic + ", accountNumber=" + accountNumber + ", ownerReferenceFrom="
        + ownerReferenceFrom + ", ownerReferenceTo=" + ownerReferenceTo + ", numberShares=" + numberShares
        + ", processingStatus=" + processingStatus + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage
        + "]";
  }

}
