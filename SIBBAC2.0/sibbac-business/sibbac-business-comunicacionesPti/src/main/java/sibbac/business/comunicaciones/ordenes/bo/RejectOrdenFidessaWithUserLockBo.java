package sibbac.business.comunicaciones.ordenes.bo;

import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class RejectOrdenFidessaWithUserLockBo {

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private RejectOrdenFidessaBo rejectOrdenBo;

  public RejectOrdenFidessaWithUserLockBo() {
  }

  @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.NOT_SUPPORTED, rollbackFor = Exception.class)
  public CallableResultDTO rejectBookingWithUserLock(ConnectionFactory cf, String destination, Tmct0bokId bookId,
      String comment, String auditUser) throws SIBBACBusinessException {
    boolean imLock = false;
    try {
      bokBo.bloquearBooking(bookId, auditUser);
      imLock = true;
      return rejectOrdenBo.rejectBooking(cf, destination, bookId, comment, auditUser);
    }
    catch (LockUserException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    catch (SIBBACBusinessException e) {
      throw e;
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(bookId, auditUser);
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
    }
  }

}
