package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.comunicaciones.pti.commons.EnumeradosComunicacionesPti.TipoCuentaIberclear;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.DesgloseIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionDatosMercadoDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.ReferenciaTitularIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.ReferenciaTitularIFDesglosesDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.AltaTitularRevisionTitularesException;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.CambiarReferenciaTitularRevisionTitularesException;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.CargaDatosRevisionDesglosesIfException;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.InactivacionTitularesRevisionTitularesException;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccTitular;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.common.RunnableProcess;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.bo.Tmct0grupoejecucionBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.bo.Tmct0referenciaTitularBo;
import sibbac.business.wrappers.database.bo.calculos.AllocateNacionalTmct0alcEconomicData;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.data.Tmct0AloDataOperacionesEspecialesPartenon;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0alcHelper;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0aloHelper;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0afiId;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.TipoCodigoIdentificacion;

@Service(value = "cuadreEccRevisarAndImportarDesglosesSubListBo")
public class CuadreEccRevisarAndImportarDesglosesSubListBo extends WrapperExecutor {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccRevisarAndImportarDesglosesSubListBo.class);

  @Value("${sibbac.cuadre.ecc.revision.desgloses.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadre.ecc.revision.desgloses.subthread.pool.size:25}")
  private int threadSize;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0grupoejecucionBo gruBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejealoBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private DesgloseTmct0alcHelper desgloseAlcHelper;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0referenciaTitularBo refTitBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  @Autowired
  private CuadreEccTitularBo eccmsgTiR2Bo;

  @Autowired
  private Tmct0operacioncdeccBo opEccBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0xasBo conversionesBo;

  @Autowired
  private ApplicationContext ctx;

  public CuadreEccRevisarAndImportarDesglosesSubListBo() {
  }

  /**
   * @param aloData
   * @param ejecucionesDatosMercado
   * @param altas
   * @param tablaCanones
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void sustituirAloOriginalMultiplesAloNewTransaction(
      Tmct0AloDataOperacionesEspecialesPartenon aloData,
      Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesDatosMercado,
      Collection<ReferenciaTitularIFDesglosesDTO> altas, CanonesConfigDTO canonesConfig)
      throws SIBBACBusinessException {
    DesgloseRecordDTO desgloseDto = null;
    List<DesgloseIFDTO> desglosesEccAlta = new ArrayList<DesgloseIFDTO>();
    List<DesgloseIFDTO> bajasRealiadas = new ArrayList<DesgloseIFDTO>();
    ObserverProcess myObserver = new ObserverProcess();
    try {
      bajaTmct0alo(aloData.getAlo(), bajasRealiadas);
      int index = 1;
      List<DesgloseRecordDTO> desgloses = new ArrayList<DesgloseRecordDTO>();
      List<ReferenciaTitularIFDesglosesDTO> referencias = new ArrayList<ReferenciaTitularIFDesglosesDTO>();

      for (ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO : altas) {
        LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarDesglosesIf] procesando referencia: {}",
            referenciaTitularIFDesglosesDTO);
        // creamos el objeto que le vamos a pasar para crear el nuevo alc
        desgloseDto = crearDesgloseRecordDTOParaDesglosePorAlo(aloData, referenciaTitularIFDesglosesDTO,
            ejecucionesDatosMercado, canonesConfig);

        desgloseDto.setIndex(new Integer(index++));

        desgloses.add(desgloseDto);

        referencias.add(referenciaTitularIFDesglosesDTO);
        referenciaTitularIFDesglosesDTO.setDatosDesgloseIF(desgloseDto);
        if (desgloses.size() >= getTransactionSize()) {
          if (myObserver.getException() != null) {
            throw myObserver.getException();
          }
          ejecutarCreacionAlosMultithreaded(myObserver, desgloses, referencias, desglosesEccAlta, bajasRealiadas);
          desgloses.clear();
          referencias.clear();
        }

      }// fin for altas

      if (CollectionUtils.isNotEmpty(desgloses)) {
        ejecutarCreacionAlosMultithreaded(myObserver, desgloses, referencias, desglosesEccAlta, bajasRealiadas);
        desgloses.clear();
        referencias.clear();
      }
    }
    catch (CargaDatosRevisionDesglosesIfException | AltaTitularRevisionTitularesException
        | InactivacionTitularesRevisionTitularesException | CambiarReferenciaTitularRevisionTitularesException e) {
      // TODO EN LA BAJA DEL ALO SOLO HAY QUE APUNTARSE LO QUE HAY QUE DAR DE
      // BAJA, LA BAJA SE HACE AL FINAL
      // DEL PROCESO

      LOG.warn(
          "[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarDesglosesIf] INCIDENCIA CONTROLADA DESHACER DESGLOSES HECHOS.",
          e);
      closeExecutorNow();
      throw new SIBBACBusinessException("Incidencia ejecutando la creacion de alos en multithread", e);
    }
    catch (Exception e) {
      closeExecutorNow();
      throw new SIBBACBusinessException("Incidencia ejecutando la creacion de alos en multithread", e);
    }
    finally {
      closeExecutor();
    }
  }

  /**
   * @param myObserver
   * @param ejealos
   * @param beans
   * @param referencias
   * @param altasRealizadas
   */
  @Transactional
  private void ejecutarCreacionAlosMultithreaded(final ObserverProcess myObserver, final List<DesgloseRecordDTO> beans,
      final List<ReferenciaTitularIFDesglosesDTO> referencias, final List<DesgloseIFDTO> altasRealizadas,
      final List<DesgloseIFDTO> bajasRealiadas) {
    final List<DesgloseRecordDTO> mybeans = new ArrayList<DesgloseRecordDTO>(beans);
    final List<ReferenciaTitularIFDesglosesDTO> myreferencias = new ArrayList<ReferenciaTitularIFDesglosesDTO>(
        referencias);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: ejecutarCreacionAlosMultithreaded] inicio.");
    RunnableProcess<DesgloseRecordDTO> process = new RunnableProcess<DesgloseRecordDTO>(myObserver) {

      @Override
      public void run() {
        LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: ejecutarCreacionAlosMultithreaded :: run] inicio.");
        try {
          LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: ejecutarCreacionAlosMultithreaded :: run] procesando registros.");
          crearAlcsByAloNewTransaction(mybeans, myreferencias, altasRealizadas, bajasRealiadas);

          LOG.debug(
              "[CuadreEccRevisarAndImportarDesglosesSubListBo :: ejecutarCreacionAlosMultithreaded :: run] lista total despues de añadir {} desgloses.",
              altasRealizadas.size());
        }
        catch (Exception e) {
          myObserver.addErrorProcess(new SIBBACBusinessException("Incidencia con sublista : " + beans, e));
        }
        finally {
          myObserver.sutDownThread(this);
        }
        LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: ejecutarCreacionAlosMultithreaded :: run] fin.");
      }
    };

    executeProcessInThread(myObserver, process);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: ejecutarCreacionAlosMultithreaded] fin.");
  }

  /**
   * @param ejealos
   * @param beans
   * @param referencias
   * @param altasRealizadas
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void crearAlcsByAloNewTransaction(List<DesgloseRecordDTO> beans,
      List<ReferenciaTitularIFDesglosesDTO> referencias, List<DesgloseIFDTO> altasRealizadas,
      List<DesgloseIFDTO> bajasRealiadas) throws SIBBACBusinessException {
    this.crearAlcsByAlo(beans, referencias, altasRealizadas, bajasRealiadas);

  }

  /**
   * @param ejealos
   * @param beans
   * @param referencias
   * @param altasRealizadas
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void crearAlcsByAlo(List<DesgloseRecordDTO> beans, List<ReferenciaTitularIFDesglosesDTO> referencias,
      List<DesgloseIFDTO> altasRealizadas, List<DesgloseIFDTO> bajasRealiadas) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearAlcsByAlo] inicio.");
    for (ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO : referencias) {
      try {
        List<Tmct0grupoejecucion> tmct0grupoejecucions = new ArrayList<Tmct0grupoejecucion>();
        List<Tmct0ejecucionalocation> myejealos = new ArrayList<Tmct0ejecucionalocation>();

        Tmct0alo aloCopia = DesgloseTmct0aloHelper.getNewCopiaTmct0aloDesgloseIf(referenciaTitularIFDesglosesDTO
            .getDatosDesgloseIF(), referenciaTitularIFDesglosesDTO.getDatosDesgloseIF().getIndex(),
            tmct0grupoejecucions, myejealos);
        aloCopia.setCdestadotit(EstadosEnumerados.TITULARIDADES.TRATANDOSE_IF.getId());

        Tmct0referenciatitular referenciaTitular = getOrCreateReferenciaTitular(referenciaTitularIFDesglosesDTO
            .getReferenciaTitular());

        aloBo.save(aloCopia);

        gruBo.save(tmct0grupoejecucions);
        ejealoBo.save(myejealos);

        altaAlcUnico(aloCopia, myejealos, referenciaTitularIFDesglosesDTO, referenciaTitular,
            referenciaTitularIFDesglosesDTO, bajasRealiadas, altasRealizadas);
        altasRealizadas.addAll(referenciaTitularIFDesglosesDTO.getDesgloses());
        persistenciaAlcUnico(aloCopia, referenciaTitularIFDesglosesDTO);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException("Incidencia con el bean: "
            + referenciaTitularIFDesglosesDTO.getDatosDesgloseIF(), e);
      }
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearAlcsByAlo] fin.");
  }

  /**
   * @param aloData
   * @param altas
   * @param ejecucionesDatosMercado
   * @param tablaCanones
   * @param altasRealizadas
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void crearDesglosesAlcNewTransaction(
      Tmct0AloDataOperacionesEspecialesPartenon aloData,
      List<ReferenciaTitularIFDesglosesDTO> altas,
      Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesDatosMercado,
      CanonesConfigDTO canonesConfig, List<DesgloseIFDTO> bajasRealiadas, List<DesgloseIFDTO> altasRealizadas)
      throws SIBBACBusinessException {
    this.crearDesglosesAlc(aloData, altas, ejecucionesDatosMercado, canonesConfig, bajasRealiadas, altasRealizadas);

  }

  /**
   * @param aloData
   * @param altas
   * @param ejecucionesDatosMercado
   * @param tablaCanones
   * @param altasRealizadas
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void crearDesglosesAlc(
      Tmct0AloDataOperacionesEspecialesPartenon aloData,
      Collection<ReferenciaTitularIFDesglosesDTO> altas,
      Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesDatosMercado,
      CanonesConfigDTO listReglasCanones, List<DesgloseIFDTO> bajasRealiadas, List<DesgloseIFDTO> altasRealizadas)
      throws SIBBACBusinessException {

    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearDesglosesAlc] inicio.");
    List<Tmct0ejecucionalocation> ejealos = ejealoBo.findAllByAlloId(aloData.getAloData().getId());
    DesgloseRecordDTO desgloseDto = null;

    for (ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO : altas) {
      desgloseDto = crearDesgloseRecordDTOParaDesglosePorAlo(aloData, referenciaTitularIFDesglosesDTO,
          ejecucionesDatosMercado, listReglasCanones);

      if (CollectionUtils.isEmpty(desgloseDto.getEjecucionesUtilizadas())) {
        throw new SIBBACBusinessException("Incidencia no se han recuperado ejecuciones para el desglose.");
      }

      Tmct0referenciatitular referenciaTitular = getOrCreateReferenciaTitular(referenciaTitularIFDesglosesDTO
          .getReferenciaTitular());

      altaAlcMultiple(aloData.getAlo(), ejealos, desgloseDto, referenciaTitular, referenciaTitularIFDesglosesDTO,
          bajasRealiadas);
      altasRealizadas.addAll(referenciaTitularIFDesglosesDTO.getDesgloses());

    }

    persistenciaAlcMultiple(aloData.getAlo(), desgloseDto, bajasRealiadas, altasRealizadas);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearDesglosesAlc] fin.");
  }

  /**
   * @param refTitular
   * @return
   */
  private Tmct0referenciatitular getOrCreateReferenciaTitular(ReferenciaTitularIFDTO refTitular) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: getOrCreateReferenciaTitular] inicio.");
    Tmct0referenciatitular referenciaTitular = refTitBo.findByCdreftitularptiAndReferenciaAdicional(
        refTitular.getReferenciaTitular(), refTitular.getReferenciaAdicional());
    if (referenciaTitular == null) {
      referenciaTitular = refTitBo.saveSyncronizedNewTransaction(refTitular.getReferenciaTitular(),
          refTitular.getReferenciaAdicional(), "IF");
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: getOrCreateReferenciaTitular] fin.");
    return referenciaTitular;
  }

  /**
   * @param aloData
   * @param referenciaTitularIFDesglosesDTO
   * @param ejecucionesDatosMercado
   * @param tablaCanones
   * @return
   */
  private DesgloseRecordDTO crearDesgloseRecordDTOParaDesglosePorAlo(
      Tmct0AloDataOperacionesEspecialesPartenon aloData,
      ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO,
      Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesDatosMercado,
      CanonesConfigDTO canonesConfig) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearDesgloseRecordDTOParaDesglosePorAlo] inicio.");
    DesgloseRecordDTO desgloseDto = new DesgloseRecordDTO(aloData.getAloData().getId().getNuorden());
    // le ponemos los titulos
    desgloseDto.setTitulosEjecutados(referenciaTitularIFDesglosesDTO.getTitulos());
    // recuperamos los corretajes que le tocan
    aloBo.recoverEconomicDataToDesgloseDto(aloData, desgloseDto);

    desgloseDto.setAloDataOperacionesEspecialesPartenon(aloData);
    desgloseDto.setEjecucionesUtilizadas(new ArrayList<EjecucionAlocationProcesarDesglosesOpEspecialesDTO>());

    EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucion;
    EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionAux;

    for (DesgloseIFDTO desglose : referenciaTitularIFDesglosesDTO.getDesgloses()) {
      ejecucion = ejecucionesDatosMercado.get(desglose.getDatosMercado());
      if (ejecucion != null) {
        ejecucionAux = new EjecucionAlocationProcesarDesglosesOpEspecialesDTO(ejecucion);
        ejecucionAux.setNutitulos(desglose.getNumeroTitulos());
        ejecucionAux.setCdoperacionecc(desglose.getNumeroOperacion());
        ejecucionAux.setIdCamara(ejecucion.getIdCamara());
        ejecucionAux.setCanonContratacionAplicado(desglose.getCanonContratacionAplicado());
        ejecucionAux.setCanonContratacionTeorico(desglose.getCanonContratacionTeorico());
        desgloseDto.getEjecucionesUtilizadas().add(ejecucionAux);
        desglose.setEjecucionUtilizada(ejecucionAux);

      }
      else {
        LOG.warn(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearDesgloseRecordDTOParaDesglosePorAlo] Incidencia no se han encontrado ejecuciones para los datos de mercado: {}",
            desglose.getDatosMercado());
      }
    }
    desgloseDto.setCanonesConfig(canonesConfig);
    desgloseDto.setTclicomDto(new Tcli0comDTO("", (short) 0, BigDecimal.ZERO, BigDecimal.ZERO));
    desgloseDto.setRefAdic(referenciaTitularIFDesglosesDTO.getReferenciaTitular().getReferenciaAdicional());
    desgloseDto.setRefTitu(referenciaTitularIFDesglosesDTO.getReferenciaTitular().getReferenciaTitular());
    desgloseDto.setCcv(referenciaTitularIFDesglosesDTO.getReferenciaTitular().getReferenciaAdicional());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: crearDesgloseRecordDTOParaDesglosePorAlo] fin.");
    referenciaTitularIFDesglosesDTO.setDatosDesgloseIF(desgloseDto);
    return desgloseDto;
  }

  /**
   * @param aloCopia
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void persistirAlcs(Tmct0alo aloCopia) {
    LOG.trace("{}::{}  inicio.", "persistirAlcs");
    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>(aloCopia.getTmct0alcs());
    List<Tmct0desglosecamara> dcs;
    List<Tmct0desgloseclitit> dcts;
    Tmct0referenciatitular refTit;
    for (Tmct0alc tmct0alc : alcs) {
      tmct0alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
      tmct0alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());
      dcs = new ArrayList<Tmct0desglosecamara>(tmct0alc.getTmct0desglosecamaras());
      tmct0alc.getTmct0desglosecamaras().clear();
      refTit = tmct0alc.getReferenciaTitular();
      refTit = refTitBo.save(refTit);

      tmct0alc = alcBo.save(tmct0alc);
      for (Tmct0desglosecamara dc : dcs) {
        dcts = new ArrayList<Tmct0desgloseclitit>(dc.getTmct0desgloseclitits());
        dc.getTmct0desgloseclitits().clear();
        dc.getTmct0DesgloseCamaraEnvioRt().clear();

        dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
        dc.getTmct0movimientos().clear();
        if (dc.getTmct0infocompensacion() != null) {
          icDao.save(dc.getTmct0infocompensacion());
        }
        dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());

        dc = dcBo.save(dc);
        dctBo.save(dcts);

      }

    }
  }

  /**
   * @param alc
   * @param refTitDto
   * @throws CambiarReferenciaTitularRevisionTitularesException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void cambiarReferenciaTitular(Tmct0alc alc, ReferenciaTitularIFDTO refTitDto)
      throws CambiarReferenciaTitularRevisionTitularesException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: cambiarReferenciaTitular] inicio.");
    try {
      Tmct0referenciatitular refTit = refTitBo.findByCdreftitularptiAndReferenciaAdicional(
          refTitDto.getReferenciaTitular(), refTitDto.getReferenciaAdicional());
      if (refTit == null) {
        refTit = refTitBo.saveSyncronizedNewTransaction(refTitDto.getReferenciaTitular(),
            refTitDto.getReferenciaAdicional(), "IF");
      }
      alc.setRftitaud(refTitDto.getReferenciaTitular());
      alc.setCdrefban(refTitDto.getReferenciaAdicional());
      alc.setReferenciaTitular(refTit);
      alc.setCdusuaud("IF");
      alc.setFhaudit(new Date());
      alcBo.save(alc);
    }
    catch (Exception e) {
      throw new CambiarReferenciaTitularRevisionTitularesException("INCIDNENCIA Cambiando Ref.Tit: " + refTitDto
          + " alc.id: " + alc.getId(), e);
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: cambiarReferenciaTitular] fin.");
  }

  /**
   * @param alc
   * @param cdbloque
   * @param ti
   * @throws AltaTitularRevisionTitularesException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void altaTitulares(Tmct0alc alc, short cdbloque, CuadreEccReferencia ti)
      throws AltaTitularRevisionTitularesException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaTitulares] inicio.");

    try {
      if (ti != null) {

        List<CuadreEccTitular> titulares = eccmsgTiR2Bo.findByEccMessageInTiId(ti.getId());

        List<Tmct0afi> afis = new ArrayList<Tmct0afi>();
        // long seq = System.currentTimeMillis();
        // i = 1;
        int i = 0;
        for (CuadreEccTitular tit : titulares) {
          Tmct0afi afi = new Tmct0afi();
          afi.setId(new Tmct0afiId(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), alc
              .getId().getNucnfliq(), afiBo.getNewTmct0afiSequence()));
          afi.setCdholder(tit.getIdentificacion());
          afi.setTptiprep(tit.getTipoIdentificacion());

          if ('U' == tit.getTipoIdentificacion()) {
            afi.setParticip(tit.getPorcentajeUsufructo());
          }
          else {
            if (tit.getPorcentajePropiedad() == null) {
              afi.setParticip(BigDecimal.ZERO);
            }
            else {
              afi.setParticip(tit.getPorcentajePropiedad());
            }
          }

          afi.setTpidenti(tit.getTipoIdentificacion());
          afi.setTpfisjur(tit.getTipoIdentificacion());
          afi.setCdddebic("");
          afi.setNudnicif("");
          if (TipoCodigoIdentificacion.BIC.getValue().charAt(0) == tit.getTipoIdentificacion()) {
            afi.setCdddebic(tit.getIdentificacion());
          }
          else if (TipoCodigoIdentificacion.OTROS.getValue().charAt(0) != tit.getTipoIdentificacion()) {
            afi.setNudnicif(tit.getIdentificacion());

          }
          afi.setCdenvaud('S');
          afi.setFhdenvio(new Date());
          afi.setFechaenvioti(new Date());
          afi.setCdnactit(tit.getPaisNacionalidad());
          afi.setCdpostal(ti.getCodPostal());

          afi.setTpsocied(tit.getIndPersonaFisicaJuridica());
          afi.setIdproced("IF");

          afi.setCddepais(ti.getPaisResidencia());

          afi.setNbciudad(ti.getPoblacion());
          afi.setNbdomici(ti.getDomicilio());

          if (tit.getIndPersonaFisicaJuridica() == 'J') {
            afi.setNbclient("");
            afi.setNbclient1(tit.getNombreRazonSocial());
            afi.setNbclient2("");
            if (i++ == 0) {
              alc.setNbtitliq(tit.getNombreRazonSocial());
              if (TipoCodigoIdentificacion.BIC.getValue().charAt(0) == tit.getTipoIdentificacion()) {
                alc.setCdordtit(StringUtils.substring(tit.getIdentificacion().trim(), 0, 25));
              }
              else {
                alc.setCdniftit(StringUtils.substring(tit.getIdentificacion().trim(), 0, 11));
              }
            }
          }
          else {
            afi.setNbclient(tit.getNombreRazonSocial());
            afi.setNbclient1(tit.getPrimerApellido());
            afi.setNbclient2(tit.getSegundoApellido());

            if (i++ == 0) {
              alc.setNbtitliq(tit.getNombreRazonSocial().trim() + " * " + tit.getPrimerApellido().trim() + " * "
                  + tit.getSegundoApellido().trim());

              if (TipoCodigoIdentificacion.BIC.getValue().charAt(0) == tit.getTipoIdentificacion()) {
                alc.setCdordtit(StringUtils.substring(tit.getIdentificacion().trim(), 0, 25));
              }
              else {
                alc.setCdniftit(StringUtils.substring(tit.getIdentificacion().trim(), 0, 11));
              }
            }
          }
          if (alc.getNbtitliq().trim().length() > 60) {
            alc.setNbtitliq(alc.getNbtitliq().substring(0, 60));
          }
          if (afi.getNbclient1().trim().length() > 50) {
            afi.setNbclient1(afi.getNbclient1().substring(0, 50));
          }

          if (afi.getNbclient().trim().length() > 30) {
            afi.setNbclient(afi.getNbclient().substring(0, 30));
          }
          if (afi.getNbclient2().trim().length() > 30) {
            afi.setNbclient2(afi.getNbclient2().substring(0, 30));
          }
          // TODO SI ES ESPAÑA RECUPERAR LAS PROVINCIAS
          afi.setNbprovin("");
          afi.setNudomici("");
          afi.setNuoprout(0);
          afi.setNupareje(new Short("0"));
          afi.setNusecnbr(new Short("0"));
          afi.setTpdomici("");
          afi.setTpnactit(tit.getIndNacionalidad());

          afi.setRftitaud(ti.getRefTitular());
          afi.setCdreferenciatitular(ti.getRefTitular());
          afi.setCcv(ti.getRefAdicional());
          afi.setCdinacti('A');
          afi.setCdbloque(cdbloque);

          afi.setFhaudit(new Date());
          afi.setCdusuaud("IF");

          afi.setCdusuina("");
          afis.add(afi);

        }
        afiBo.save(afis);
      }
      else {

      }
    }
    catch (Exception e) {
      throw new AltaTitularRevisionTitularesException("INCIDENCIA: Alta titulares EccMsgTi : " + ti, e);
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaTitulares] fin.");
  }

  /**
   * @param alc
   * @param desglose
   * @throws AltaTitularRevisionTitularesException,
   * 
   * InactivacionTitularesRevisionTitularesException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void revisarTitulares(Tmct0alc alc, ReferenciaTitularIFDesglosesDTO desglose)
      throws AltaTitularRevisionTitularesException,

      InactivacionTitularesRevisionTitularesException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarTitulares] inicio.");
    try {
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        boolean cambiarTitulares = false;
        List<Tmct0afi> afis = afiBo.findActivosByAlcData(alc.getId().getNuorden(), alc.getId().getNbooking(), alc
            .getId().getNucnfclt(), alc.getId().getNucnfliq());
        if (CollectionUtils.isEmpty(afis)) {
          cambiarTitulares = true;
        }
        // if (CollectionUtils.isNotEmpty(afis)) {

        CuadreEccReferencia ti = cuadreEccReferenciaBo
            .findByFechaejecucionAndNumerooperacionAndSentidoAndReferenciatitular(alc.getFeejeliq(), desglose
                .getDesgloses().get(0).getNumeroOperacion(), alc.getCdtpoper(), desglose.getReferenciaTitular()
                .getReferenciaTitular());
        if (ti != null) {

          Collection<String> identificacionesAfis = new HashSet<String>();
          int i = 0;

          for (Tmct0afi tmct0afi : afis) {
            if (i++ == 0) {
              if (!tmct0afi.getNbdomici().trim().equals(ti.getDomicilio().trim())) {
                cambiarTitulares = true;
              }

              if (!tmct0afi.getNbciudad().trim().equals(ti.getPoblacion().trim())) {
                cambiarTitulares = true;
              }

            }
            identificacionesAfis.add(tmct0afi.getCdholder().trim() + tmct0afi.getTptiprep());
          }

          List<CuadreEccTitular> titulares = eccmsgTiR2Bo.findByEccMessageInTiId(ti.getId());
          Collection<String> identificacionesTis = new HashSet<String>();
          for (CuadreEccTitular eccMessageInTI_R2 : titulares) {
            identificacionesTis.add(eccMessageInTI_R2.getIdentificacion() + eccMessageInTI_R2.getTipoIdentificacion());
          }

          if (cambiarTitulares || !CollectionUtils.containsAll(identificacionesAfis, identificacionesTis)) {
            LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarTitulares] Los titulares NO son correctos.");
            short cdbloque = 0;
            try {
              if (CollectionUtils.isNotEmpty(afis)) {
                bajaAfis(afis);
                cdbloque = afis.get(0).getCdbloque();
              }
              cdbloque += 1;
            }
            catch (Exception e) {
              throw new InactivacionTitularesRevisionTitularesException("INCIDENCIA Inactivacion titulares alc.id: "
                  + alc.getId(), e);
            }

            altaTitulares(alc, cdbloque, ti);

          }
          else {
            LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarTitulares] Los titulares son correctos.");
          }
        }
        else {
          LOG.warn(
              "[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarTitulares] Incidencia no se ha encontrado el titular para ref.tit: {} feejecuc: {} n.operacion: {}",
              desglose.getReferenciaTitular().getReferenciaTitular(), alc.getFeejeliq(), desglose.getDesgloses().get(0)
                  .getNumeroOperacion());

        }
      }
    }
    catch (AltaTitularRevisionTitularesException | InactivacionTitularesRevisionTitularesException e) {
      throw e;
    }
    catch (Exception e) {
      throw new AltaTitularRevisionTitularesException("INCIDENCIA Revision titulares alc.id: " + alc.getId(), e);
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: revisarTitulares] fin.");
  }

  /**
   * @param aloId
   * @param desglosesAllo
   * @param desglosesEcc
   * @param fechaCuadre
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void actualizarDesglosesAloAndDesglosesPti(Tmct0aloId aloId, List<CuadreEccSibbac> desglosesAllo,
      List<CuadreEccReferencia> desglosesEcc, Date fechaCuadre) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: actualizarDesglosesAloAndDesglosesPti] inicio.");

    Collection<Integer> estadosTitularidades = new HashSet<Integer>();
    Integer cdestadoTitSettear = null;
    BigDecimal fechaCuadreBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
        .convertDateToString(fechaCuadre));

    BigDecimal fevalorCierre;
    boolean isCierreCuentaTerceros;
    boolean isCierreCuentaNoTerceros;
    for (CuadreEccSibbac eleIf : desglosesAllo) {
      fevalorCierre = FormatDataUtils
          .convertStringToBigDecimal(FormatDataUtils.convertDateToString(eleIf.getFevalor()));
      isCierreCuentaNoTerceros = fevalorCierre.compareTo(fechaCuadreBd) <= 0;
      // TODO RECUPERAR LOS FESTIVOS Y FINES DE SEMANA Y LOS DIAS NECESARIOS
      // DESDE FTL PARA QUE UNA ITULARIDAD SEA
      // DEFINITIVA
      fevalorCierre = fevalorCierre.add(new BigDecimal(6));

      isCierreCuentaTerceros = fevalorCierre.compareTo(fechaCuadreBd) <= 0;
      cdestadoTitSettear = eleIf.getEstado();

      if (!eleIf.getTipoCuentaLiquidacionIberclear().equals(TipoCuentaIberclear.CUENTA_TERCEROS.name())
          && !eleIf.getTipoCuentaLiquidacionIberclear().equals(TipoCuentaIberclear.CUENTA_INDIVIDUAL_IF.name())) {
        if (isCierreCuentaNoTerceros) {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId();
        }
      }
      else if (eleIf.getTipoCuentaLiquidacionIberclear().equals(TipoCuentaIberclear.CUENTA_INDIVIDUAL_IF.name())) {
        if (isCierreCuentaTerceros) {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.CIERRE_IF.getId();
        }
        else {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.PROVISIONAL_IF.getId();
        }
      }
      else if (eleIf.getTipoCuentaLiquidacionIberclear().equals(TipoCuentaIberclear.CUENTA_TERCEROS.name())) {
        if (isCierreCuentaTerceros) {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId();
        }
      }

      if (eleIf.getEstado() != cdestadoTitSettear) {
        eleIf.setEstado(cdestadoTitSettear);
        eleIf.setAuditDate(new Date());
        eleIf.setAuditUser("IF");
      }
      estadosTitularidades.add(cdestadoTitSettear);
    }

    if (estadosTitularidades.size() == 1) {
      // LOG.warn("[CuadreEccRevisarAndImportarDesglosesSubListBo :: actualizarDesglosesAloAndDesglosesPti] COMENTADO CAMBIO DE ESTADOS DE LOS ALCS");
      LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: actualizarDesglosesAloAndDesglosesPti] cambiando estados de la orden");
      dcBo.updateCdestadotitByTmct0aloId(aloId, cdestadoTitSettear);
      alcBo.updateCdestadotitByTmct0aloId(aloId, cdestadoTitSettear);
      aloBo.updateCdestadotitByTmct0aloId(aloId, cdestadoTitSettear);

    }
    else {
      // TODO INCIDENCIA DISTINTOS TIPOS DE CUENTA
    }

    cuadreEccSibbacBo.save(desglosesAllo);

    for (CuadreEccReferencia ti : desglosesEcc) {
      ti.setFechaCuadre(fechaCuadre);
      ti.setAuditDate(new Date());
      ti.setAuditUser("IF");
    }
    cuadreEccReferenciaBo.save(desglosesEcc);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: actualizarDesglosesAloAndDesglosesPti] fin.");
  }

  /**
   * @param alo
   * @param bajasRealiadas
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void bajaTmct0alo(Tmct0alo alo, List<DesgloseIFDTO> bajasRealiadas) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaTmct0alo] inicio.");

    List<Tmct0movimientoecc> movs = movBo.findByTmct0aloId(alo.getId());
    if (CollectionUtils.isNotEmpty(movs)) {
      bajaMovs(movs);
    }
    bajaAlcs(alo, bajasRealiadas);
    alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
    alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
    alo.setCdestadotit(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
    alo.setFhaudit(new Date());
    alo.setCdusuaud("IF");
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaTmct0alo] fin.");
  }

  /**
   * @param alo
   * @param bajasRealiadas
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void bajaAlcs(Tmct0alo alo, List<DesgloseIFDTO> bajasRealiadas) {
    // 1.-dar de baja allos y todo lo que hay debajo
    List<Tmct0alc> alcs = alcBo.findByAlloId(alo.getId());
    if (!CollectionUtils.isEmpty(alcs)) {

      for (Tmct0alc alc : alcs) {
        if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          bajaAlc(alc, bajasRealiadas);
        }// alc vivo
      }// fin for alc

    }// fin bajas
  }

  /**
   * @param aloCopia
   * @param ejealos
   * @param desgloseDto
   * @param referenciaTitular
   * @param referenciaTitularIFDesglosesDTO
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void altaAlcUnico(Tmct0alo aloCopia, List<Tmct0ejecucionalocation> ejealos,
      ReferenciaTitularIFDesglosesDTO desgloseDto, Tmct0referenciatitular referenciaTitular,
      ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO, List<DesgloseIFDTO> bajasRealiadas,
      List<DesgloseIFDTO> altasRealiadas) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaAlcUnico] inicio.");

    altaAlcMultiple(aloCopia, ejealos, desgloseDto.getDatosDesgloseIF(), referenciaTitular,
        referenciaTitularIFDesglosesDTO, bajasRealiadas);

    calculosEconomicos(aloCopia, desgloseDto.getDatosDesgloseIF(), bajasRealiadas, altasRealiadas);

    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaAlcUnico] fin.");
  }

  /**
   * @param aloCopia
   * @param referenciaTitularIFDesglosesDTO
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void persistenciaAlcUnico(Tmct0alo aloCopia, ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: persistenciaAlcUnico] inicio.");
    persistirAlcs(aloCopia);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: persistenciaAlcUnico] fin.");
  }

  /**
   * @param aloCopia
   * @param ejealos
   * @param desgloseDto
   * @param referenciaTitular
   * @param referenciaTitularIFDesglosesDTO
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void altaAlcMultiple(Tmct0alo aloCopia, List<Tmct0ejecucionalocation> ejealos, DesgloseRecordDTO desgloseDto,
      Tmct0referenciatitular referenciaTitular, ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO,
      List<DesgloseIFDTO> bajasRealiadas) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaAlcMultiple] inicio.");
    Tmct0alc alcCopia = desgloseAlcHelper.crearDesglosesAlcRevisionDesgloseIf(desgloseDto, ejealos, aloCopia,
        referenciaTitular, null);

    // List<Tmct0desglosecamara> dcs = alcCopia.getTmct0desglosecamaras();
    // List<Tmct0desgloseclitit> dcts;
    // String camaraDesglose;
    // for (Tmct0desglosecamara dc : dcs) {
    // dcts = dc.getTmct0desgloseclitits();
    // for (Tmct0desgloseclitit dct : dcts) {
    // camaraDesglose =
    // conversionesBo.getCamaraCompensacionClearingPlatform(dct.getCdcamara());
    // dct.setCdcamara(camaraDesglose);
    // }
    // }

    alcCopia.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.TRATANDOSE_IF.getId());

    referenciaTitularIFDesglosesDTO.setNuorden(alcCopia.getId().getNuorden());
    referenciaTitularIFDesglosesDTO.setNbooking(alcCopia.getId().getNbooking());
    referenciaTitularIFDesglosesDTO.setNucnflt(alcCopia.getId().getNucnfclt());
    referenciaTitularIFDesglosesDTO.setNucnfliq(alcCopia.getId().getNucnfliq());

    aloCopia.getTmct0alcs().add(alcCopia);
    CuadreEccReferencia ti = cuadreEccReferenciaBo
        .findByFechaejecucionAndNumerooperacionAndSentidoAndReferenciatitular(alcCopia.getFeejeliq(),
            referenciaTitularIFDesglosesDTO.getDesgloses().get(0).getNumeroOperacion(), alcCopia.getCdtpoper(),
            referenciaTitularIFDesglosesDTO.getReferenciaTitular().getReferenciaTitular());
    try {
      altaTitulares(alcCopia, new Short("1"), ti);
    }
    catch (Exception e) {
      LOG.warn(
          "[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaAlcMultiple :: altaTitulares] INCIDENCIA. Ecc.TI: "
              + ti, e);
    }
    referenciaTitularIFDesglosesDTO.setDesgloseRelacionado(alcCopia);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: altaAlcMultiple] fin.");
  }

  /**
   * @param aloCopia
   * @param desgloseDto
   * @param bajasRealiadas
   * @throws SIBBACBusinessException
   */
  private void calculosEconomicos(Tmct0alo aloCopia, DesgloseRecordDTO desgloseDto, List<DesgloseIFDTO> bajasRealiadas,
      List<DesgloseIFDTO> altasRealiadas) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: calculosEconomicos] inicio.");
    boolean repartirCalculos;
    List<Tmct0alc> alcs = aloCopia.getTmct0alcs();
    List<Tmct0desglosecamara> dcs;
    List<Tmct0desgloseclitit> dcts;
    List<Tmct0desgloseclitit> dctsAcumular;
    DesgloseIFDTO desgloseBaja;
    DesgloseIFDTO desgloseAlta;

    boolean cltePayCanonComp = aloCopia.getCanoncomppagoclte() == 1;
    boolean cltePayCanonContr = aloCopia.getCanoncontrpagoclte() == 1;
    boolean cltePayCanonLiq = aloCopia.getCanonliqpagoclte() == 1;

    boolean clientPayRight = cltePayCanonComp || cltePayCanonContr || cltePayCanonLiq;

    for (Tmct0alc alc : alcs) {
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {

        boolean ponerEstados = false;
        repartirCalculos = isRepartirCalculos(alc, bajasRealiadas);

        if (repartirCalculos) {
          dctsAcumular = new ArrayList<Tmct0desgloseclitit>();
          dcs = alc.getTmct0desglosecamaras();
          for (Tmct0desglosecamara dc : dcs) {
            if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              dcts = dc.getTmct0desgloseclitits();
              dctsAcumular.addAll(dcts);
              for (Tmct0desgloseclitit dct : dcts) {

                desgloseBaja = getDesgloseBaja(dct, bajasRealiadas);
                // FIXME NECESITO EL DESGLOSE DONDE VIENE EL CANON TEORICO Y EL
                // APLICADO
                desgloseAlta = getDesgloseBaja(dct, altasRealiadas);
                if (desgloseBaja != null) {
                  if (ponerEstados) {
                    ponerEstados = false;
                    if (desgloseBaja.getCdestadocont() > EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_COBRO_BME
                        .getId()) {
                      alc.getEstadocont().setIdestado(desgloseBaja.getCdestadocont());
                      dc.getTmct0estadoByCdestadocont().setIdestado(desgloseBaja.getCdestadocont());
                    }
                    else {
                      alc.getEstadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
                      dc.getTmct0estadoByCdestadocont()
                          .setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
                    }
                    alc.setCdestadoasig(desgloseBaja.getCdestadoasig());
                    dc.getTmct0estadoByCdestadoasig().setIdestado(desgloseBaja.getCdestadoasig());
                  }

                  repartirCalculosEconomicos(desgloseBaja, desgloseAlta, dct);

                }// hay desglose de baja
              }// for dcts
            }// dc no rechazado
          }// for dcs

          try {
            // acumulamos de los dcts al alc
            AllocateNacionalTmct0alcEconomicData.acumulateFromExecutionGroup(alc, dctsAcumular);

            // AllocateNacionalTmct0alcEconomicData.calcularCanones(desgloseDto.isIsinDerechos(),
            // desgloseDto.isAliasBonificado(), new BigDecimal("20"),
            // new BigDecimal("0.25"), desgloseDto.getTablaCanones(),
            // alc, dctsAcumular);

            if (clientPayRight) {

              AllocateNacionalTmct0alcEconomicData.calcularComisionLiquidacion(clientPayRight, alc, dctsAcumular);

              AllocateNacionalTmct0alcEconomicData.calcularProfitCommission(alc, dctsAcumular);
              AllocateNacionalTmct0alcEconomicData.calcularNetoClienteFinal(cltePayCanonComp, cltePayCanonContr,
                  cltePayCanonLiq, alc, dctsAcumular);
            }
            else {
              // FIXME recuperar efectivo minimo bonificado, y canon bonificado

              AllocateNacionalTmct0alcEconomicData.calcularProfitCommission(alc, dctsAcumular);
            }
          }
          catch (Exception e) {
            throw new SIBBACBusinessException("Incidencia ocurrida con aloid: " + aloCopia.getId(), e);
          }

        }// repartir calculos
      }// alc no rechazado

    }// fin for alcs

    CalculosEconomicosNacionalBo calculos = ctx.getBean(CalculosEconomicosNacionalBo.class, aloCopia,
        aloCopia.getTmct0alcs(), desgloseDto.getCanonesConfig(), desgloseDto.getTclicomDto());
    try {

      calculos.calcularDatosEconomicosSinCanones();

    }
    catch (EconomicDataException e) {
      throw new SIBBACBusinessException("Incidencia ocurrida con aloid: " + aloCopia.getId(), e);
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: calculosEconomicos] fin.");
  }

  private void repartirCalculosEconomicos(DesgloseIFDTO desgloseBaja, DesgloseIFDTO desgloseAlta,
      Tmct0desgloseclitit dct) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] inicio.");
    if (desgloseBaja.getNumeroTitulos().compareTo(dct.getImtitulos()) == 0) {

      asignarTotalDatosEconomicos(desgloseBaja, desgloseAlta, dct);

    }
    else {
      desgloseBaja.setNumeroTitulosReparto(desgloseBaja.getNumeroTitulos().subtract(dct.getImtitulos()));
      if (desgloseBaja.getNumeroTitulosReparto().compareTo(BigDecimal.ZERO) == 0) {
        asignarTotalDatosEconomicos(desgloseBaja, desgloseAlta, dct);
      }
      else {

        dct.setImefectivo(dct.getImprecio().multiply(dct.getImtitulos()).setScale(2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setBrutoMercado(desgloseBaja.getBrutoMercado().subtract(dct.getImefectivo()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImefectivo == {}.",
            dct.getImefectivo());

        dct.setImajusvb(dct.getImefectivo().multiply(desgloseBaja.getComisionAjuste())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionAjusteReparto(desgloseBaja.getComisionAjusteReparto().subtract(dct.getImajusvb()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImajusvb == {}.",
            dct.getImajusvb());

        dct.setImbansis(dct.getImefectivo().multiply(desgloseBaja.getComisionImbansis())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionImbansisReparto(desgloseBaja.getComisionImbansisReparto().subtract(dct.getImbansis()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImbansis == {}.",
            dct.getImbansis());

        dct.setImcomsis(dct.getImefectivo().multiply(desgloseBaja.getComisionImcomsis())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionImcomsisReparto(desgloseBaja.getComisionImcomsisReparto().subtract(dct.getImcomsis()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcomsis == {}.",
            dct.getImcomsis());

        dct.setImcomisn(dct.getImefectivo().multiply(desgloseBaja.getComisionCliente())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionClienteReparto(desgloseBaja.getComisionClienteReparto().subtract(dct.getImcomisn()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcomsis == {}.",
            dct.getImcomsis());

        dct.setImcombco(dct.getImefectivo().multiply(desgloseBaja.getComisionBanco())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionBancoReparto(desgloseBaja.getComisionBancoReparto().subtract(dct.getImcombco()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcombco == {}.",
            dct.getImcombco());

        dct.setImcombrk(dct.getImefectivo().multiply(desgloseBaja.getComisionDevolucion())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionDevolucionReparto(desgloseBaja.getComisionDevolucionReparto().subtract(
            dct.getImcombrk()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcombrk == {}.",
            dct.getImcombrk());

        dct.setImcomdvo(dct.getImefectivo().multiply(desgloseBaja.getComisionOrdenante())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja
            .setComisionOrdenanteReparto(desgloseBaja.getComisionOrdenanteReparto().subtract(dct.getImcomdvo()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcomdvo == {}.",
            dct.getImcomdvo());

        dct.setImfinsvb(dct.getImefectivo().multiply(desgloseBaja.getComisionLiquidacion())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setComisionLiquidacionReparto(desgloseBaja.getComisionLiquidacionReparto().subtract(
            dct.getImfinsvb()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImfinsvb == {}.",
            dct.getImfinsvb());

        dct.setImcomsvb(dct.getImefectivo().multiply(desgloseBaja.getComisionBeneficio())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja
            .setComisionBeneficioReparto(desgloseBaja.getComisionBeneficioReparto().subtract(dct.getImcomsvb()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcomsvb == {}.",
            dct.getImcomsvb());

        dct.setImtotbru(dct.getImefectivo().multiply(desgloseBaja.getBrutoCliente())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setBrutoClienteReparto(desgloseBaja.getBrutoClienteReparto().subtract(dct.getImtotbru()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImtotbru == {}.",
            dct.getImtotbru());

        dct.setImtotnet(dct.getImefectivo().multiply(desgloseBaja.getNetoClienteFidessa())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setNetoClienteFidessa(desgloseBaja.getNetoClienteFidessa().subtract(dct.getImtotnet()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImtotnet == {}.",
            dct.getImtotnet());

        dct.setImnetliq(dct.getImefectivo().multiply(desgloseBaja.getNetoCliente())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setNetoClienteReparto(desgloseBaja.getNetoClienteReparto().subtract(dct.getImnetliq()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImnetliq == {}.",
            dct.getImnetliq());

        dct.setImnfiliq(dct.getImnetliq());
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImnfiliq == {}.",
            dct.getImnfiliq());

        dct.setImntbrok(dct.getImefectivo().multiply(desgloseBaja.getAjustePrecio())
            .divide(desgloseBaja.getBrutoMercado(), 2, BigDecimal.ROUND_HALF_UP));
        desgloseBaja.setAjustePrecioReparto(desgloseBaja.getAjustePrecioReparto().subtract(dct.getImntbrok()));
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImntbrok == {}.",
            dct.getImntbrok());

        // dct.setImcanoncontreu(desgloseAlta.getCanonContratacionAplicado());
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcanoncontreu == {}.",
            dct.getImcanoncontreu());
        // dct.setImcanoncontrdv(desgloseAlta.getCanonContratacionTeorico());
        LOG.debug(
            "[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] dct.getImcanoncontrdv == {}.",
            dct.getImcanoncontrdv());
      }
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: repartirCalculosEconomicos] fin.");
  }

  private void asignarTotalDatosEconomicos(DesgloseIFDTO desgloseBaja, DesgloseIFDTO desgloseAlta,
      Tmct0desgloseclitit dct) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] inicio.");
    dct.setImefectivo(desgloseBaja.getBrutoMercadoReparto());
    LOG.debug(
        "[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImefectivo == {}.",
        dct.getImefectivo());

    dct.setImajusvb(desgloseBaja.getComisionAjusteReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImajusvb == {}.",
        dct.getImajusvb());

    dct.setImbansis(desgloseBaja.getComisionImbansisReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImbansis == {}.",
        dct.getImbansis());

    dct.setImcomsis(desgloseBaja.getComisionImcomsisReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcomsis == {}.",
        dct.getImcomsis());

    dct.setImcomisn(desgloseBaja.getComisionClienteReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcomisn == {}.",
        dct.getImcomisn());

    dct.setImcombco(desgloseBaja.getComisionBancoReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcombco == {}.",
        dct.getImcombco());

    dct.setImcombrk(desgloseBaja.getComisionDevolucionReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcombrk == {}.",
        dct.getImcombrk());

    dct.setImcomdvo(desgloseBaja.getComisionOrdenanteReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcomdvo == {}.",
        dct.getImcomdvo());

    dct.setImfinsvb(desgloseBaja.getComisionLiquidacionReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImfinsvb == {}.",
        dct.getImfinsvb());

    dct.setImcomsvb(desgloseBaja.getComisionBeneficioReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcomsvb == {}.",
        dct.getImcomsvb());

    dct.setImtotbru(desgloseBaja.getBrutoClienteReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImtotbru == {}.",
        dct.getImtotbru());

    dct.setImtotnet(desgloseBaja.getNetoClienteFidessaReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImtotnet == {}.",
        dct.getImtotnet());

    dct.setImnetliq(desgloseBaja.getNetoClienteReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImnetliq == {}.",
        dct.getImnetliq());

    dct.setImnfiliq(desgloseBaja.getNetoClienteReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImnfiliq == {}.",
        dct.getImnfiliq());

    dct.setImntbrok(desgloseBaja.getAjustePrecioReparto());
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImntbrok == {}.",
        dct.getImntbrok());

    if (desgloseAlta != null) {
      // dct.setImcanoncontreu(desgloseAlta.getCanonContratacionAplicado());
      LOG.debug(
          "[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcanoncontreu == {}.",
          dct.getImcanoncontreu());
      // dct.setImcanoncontrdv(desgloseAlta.getCanonContratacionTeorico());
      LOG.debug(
          "[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] dct.getImcanoncontrdv == {}.",
          dct.getImcanoncontrdv());
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: asignarTotalDatosEconomicos] fin.");
  }

  /**
   * @param aloCopia
   * @param desgloseDto
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void persistenciaAlcMultiple(Tmct0alo aloCopia, DesgloseRecordDTO desgloseDto,
      List<DesgloseIFDTO> bajasRealiadas, List<DesgloseIFDTO> altasRealiadas) throws SIBBACBusinessException {

    calculosEconomicos(aloCopia, desgloseDto, bajasRealiadas, altasRealiadas);

    persistirAlcs(aloCopia);

  }

  /**
   * 
   * @param alc
   * @param bajasRealiadas
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected void bajaAlc(Tmct0alc alc, List<DesgloseIFDTO> bajasRealiadas) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaAlc] inicio.");
    Tmct0referenciatitular referenciaTitular = alcBo.findTmct0referenciatitular(alc.getId());
    // TODO HACER CONFIGURABLE LAS CAMARAS QUE VAMOS A DAR DE BAJA
    List<String> camaras = new ArrayList<String>(Arrays.asList(new String[] { "BME" }));

    // FIXME LANZAR EXCEPCION SI HAY UNA CAMARA QUE NO ES NACIONAL PARA QUE SE
    // TRATE DE OTRA MANERA
    List<Tmct0desglosecamara> dcs = dcBo.findAllByTmct0alcActivosIdAndCdestadoasigAndInCamarasCompensacion(alc.getId(),
        camaras);
    List<Tmct0desgloseclitit> dcts;
    List<Tmct0movimientoecc> movs;

    DesgloseIFDTO desgloseIF;
    String camaraDesglose;
    if (CollectionUtils.isNotEmpty(dcs)) {
      for (Tmct0desglosecamara dc : dcs) {

        ReferenciaTitularIFDTO refTitular = null;

        if (referenciaTitular == null) {
          refTitular = new ReferenciaTitularIFDTO("", StringUtils.trim(alc.getCdrefban()));
        }
        else {
          refTitular = new ReferenciaTitularIFDTO(referenciaTitular.getCdreftitularpti(),
              referenciaTitular.getReferenciaAdicional());
        }

        movs = movBo.findByIdDesgloseCamara(dc.getIddesglosecamara());
        if (CollectionUtils.isNotEmpty(movs)) {
          bajaMovs(movs);
        }

        dcts = dctBo.findByIddesglosecamara(dc.getIddesglosecamara());
        if (CollectionUtils.isNotEmpty(dcts)) {
          for (Tmct0desgloseclitit dct : dcts) {
            String numeroOperacion = "";

            if (StringUtils.isNotBlank(dct.getNumeroOperacionDCV())) {
              numeroOperacion = dct.getNumeroOperacionDCV();
            }
            else if (StringUtils.isNotBlank(dct.getCdoperacionecc())) {
              numeroOperacion = dct.getCdoperacionecc();
            }
            camaraDesglose = dct.getCdcamara();
            desgloseIF = new DesgloseIFDTO(numeroOperacion, refTitular, dct.getImtitulos(),
                new IdentificacionOperacionDatosMercadoDTO(dct.getFeejecuc(), dct.getCdisin(), dct.getCdsentido(),
                    camaraDesglose, dct.getCdsegmento(), dct.getTpopebol(), dct.getCdmiembromkt(), dct.getNuordenmkt(),
                    dct.getNurefexemkt()));
            if (StringUtils.isNotBlank(alc.getCdentliq())) {
              desgloseIF.setEntidadLiquidadora(alc.getCdentliq());
            }
            else if (StringUtils.isNotBlank(alc.getCdentdep())) {
              desgloseIF.setEntidadLiquidadora(alc.getCdentdep());
            }

            desgloseIF.setCdestadoasig(dc.getTmct0estadoByCdestadoasig().getIdestado());
            desgloseIF.setCdestadotit(dc.getTmct0estadoByCdestadotit().getIdestado());
            desgloseIF.setCdestadocont(dc.getTmct0estadoByCdestadocont().getIdestado());
            desgloseIF.setCdestadoentrec(alc.getEstadoentrec().getIdestado());

            desgloseIF.loadEconomicData(dct);

            bajasRealiadas.add(desgloseIF);

            dct.setAuditFechaCambio(new Date());
            dct.setAuditUser("IF");
          }
          dctBo.save(dcts);
        }

        dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
        dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());

        dc.setTmct0estadoByCdestadotit(new Tmct0estado());
        dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
        dc.setAuditFechaCambio(new Date());
        dc.setAuditUser("IF");
      }// fin for dcs
      dcBo.save(dcs);

    }
    alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());

    alc.setEstadotit(new Tmct0estado());
    alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
    alc.setFhaudit(new Date());
    alc.setCdusuaud("IF");

    List<Tmct0afi> afis = afiBo.findActivosByAlcData(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId()
        .getNucnfclt(), alc.getId().getNucnfliq());
    bajaAfis(afis);
    alcBo.save(alc);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaAlc] fin.");
  }

  /**
   * @param afis
   */
  private void bajaAfis(List<Tmct0afi> afis) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaAfis] inicio.");
    if (CollectionUtils.isNotEmpty(afis)) {
      for (Tmct0afi tmct0afi : afis) {
        tmct0afi.setCdusuina("IF");
        tmct0afi.setCdinacti('I');
        tmct0afi.setFhinacti(new Date());
        tmct0afi.setCdusuaud("IF");
        tmct0afi.setFhaudit(new Date());
      }
      afiBo.save(afis);
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaAfis] fin.");
  }

  /**
   * @param alc
   * @param bajasRealiadas
   * @return
   */
  private DesgloseIFDTO getDesgloseBaja(Tmct0desgloseclitit dct, List<DesgloseIFDTO> bajasRealiadas) {
    if (CollectionUtils.isNotEmpty(bajasRealiadas)) {

      IdentificacionOperacionDatosMercadoDTO datosMercado;
      String camaraDesglose = dct.getCdcamara();

      datosMercado = new IdentificacionOperacionDatosMercadoDTO(dct.getFeejecuc(), dct.getCdisin(), dct.getCdsentido(),
          camaraDesglose, dct.getCdsegmento(), dct.getTpopebol(), dct.getCdmiembromkt(), dct.getNuordenmkt(),
          dct.getNurefexemkt());

      for (DesgloseIFDTO desglose : bajasRealiadas) {
        if (datosMercado.equals(desglose.getDatosMercado())
            && (desglose.getNumeroOperacion().equals(dct.getCdoperacionecc()) || desglose.getNumeroOperacion().equals(
                dct.getNumeroOperacionDCV()))) {
          return desglose;
        }

      }
    }
    return null;
  }

  /**
   * @param alc
   * @param bajasRealiadas
   * @return
   */
  private boolean isRepartirCalculos(Tmct0alc alc, List<DesgloseIFDTO> bajasRealiadas) {
    boolean res = false;
    if (CollectionUtils.isNotEmpty(bajasRealiadas)) {

      List<Tmct0desglosecamara> dcs = alc.getTmct0desglosecamaras();
      List<Tmct0desgloseclitit> dcts;
      DesgloseIFDTO desgloseBaja;
      for (Tmct0desglosecamara dc : dcs) {
        dcts = dc.getTmct0desgloseclitits();
        for (Tmct0desgloseclitit dct : dcts) {
          desgloseBaja = getDesgloseBaja(dct, bajasRealiadas);
          if (desgloseBaja != null) {
            if (desgloseBaja.getCdestadocont() > EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_COBRO_BME.getId()) {
              res = true;
              return res;
            }
          }

        }

      }
    }

    return res;
  }

  /**
   * @param movs
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void bajaMovs(List<Tmct0movimientoecc> movs) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaMovs] inicio.");
    for (Tmct0movimientoecc mov : movs) {
      if (!mov.getCdestadomov().trim().equals("21")) {
        mov.setCdestadomov("21");
        mov.setAuditFechaCambio(new Date());
        mov.setAuditUser("IF");
      }
    }// fin for movs
    movBo.save(movs);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesSubListBo :: bajaMovs] fin.");
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
