package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpProcessedSettledMovementFields implements WrapperFileReaderFieldEnumInterface {
  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  SUBACCOUNT_NUMBER("subaccountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  OPPOSITE_PARTY_CODE("oppositePartyCode", 6, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  SYMBOL("symbol", 6, 0, WrapperFileReaderFieldType.STRING),
  TYPE("typeField", 1, 0, WrapperFileReaderFieldType.CHAR),
  EXPIRATION_DATE("expirationDate", 8, 0, WrapperFileReaderFieldType.DATE),
  EXERCISE_PRICE("exercisePrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  MOVEMENT_CODE("movementCode", 2, 0, WrapperFileReaderFieldType.STRING),
  PROCESSED_QUANTITY_LONG("processedQuantityLong", 13, 2, WrapperFileReaderFieldType.NUMERIC),
  PROCESSED_QUANTITY_SHORT("processedQuantityShort", 13, 2, WrapperFileReaderFieldType.NUMERIC),
  COUNTER_VALUE("counterValue", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  COUNTER_VALUE_DC("counterValueDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  COUNTER_VALUE_CURRENCY_CODE("counterValueCurrencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  CUPON_INTEREST("cuponInterest", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  CUPON_INTEREST_DC("cuponInterestDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  TRANSACTION_DATE("transactionDate", 8, 0, WrapperFileReaderFieldType.DATE),
  ISIN_CODE("isinCode", 12, 0, WrapperFileReaderFieldType.STRING),
  DEPOT_SETTLED_REFERENCE("depotSettledReference", 9, 0, WrapperFileReaderFieldType.STRING),
  VALUE_DATE("valueDate", 8, 0, WrapperFileReaderFieldType.DATE),
  COMMENT("commentField", 21, 0, WrapperFileReaderFieldType.STRING),
  DEPOT_ID("depotId", 6, 0, WrapperFileReaderFieldType.STRING),
  SAFE_KEEPING_ID("safeKeepingId", 2, 0, WrapperFileReaderFieldType.STRING),
  FILLER("filler", 281, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpProcessedSettledMovementFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
