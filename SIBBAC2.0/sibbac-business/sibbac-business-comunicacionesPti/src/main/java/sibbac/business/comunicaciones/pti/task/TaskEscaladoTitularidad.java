package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.escalado.bo.EscalaEstadoTitularidadBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_ESCALADO_TITULARIDAD, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-23 ? * MON-FRI")
public class TaskEscaladoTitularidad extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEscaladoTitularidad.class);

  @Autowired
  private EscalaEstadoTitularidadBo escalaestadoTitularidadBo;

  @Override
  public void executeTask() throws Exception {
    escalaestadoTitularidadBo.escalarEstados(false);
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_DAEMONS_ESCALADO_ESTADO_TITULARIDAD;
  }

}
