package sibbac.business.titulares.bo;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class CreacionTablasTitularesByTmct0alcCallable implements Callable<Void> {

  private Tmct0alcId alcId;
  private TitularesConfigDTO config;

  @Autowired
  private CreacionTablasTiularesWriteLog creacionTablasTitularesBo;

  @Autowired
  private Tmct0logBo logBo;

  public CreacionTablasTitularesByTmct0alcCallable(Tmct0alcId alcId, TitularesConfigDTO config) {
    super();
    this.alcId = alcId;
    this.config = config;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    creacionTablasTitularesBo.crearTablasForProcess(alcId, config);
    return null;
  }

  public Tmct0alcId getTmct0alcId() {
    return this.alcId;
  }
}
