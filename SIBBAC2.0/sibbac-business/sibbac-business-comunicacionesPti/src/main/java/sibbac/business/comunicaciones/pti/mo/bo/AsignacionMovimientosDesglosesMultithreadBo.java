package sibbac.business.comunicaciones.pti.mo.bo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class AsignacionMovimientosDesglosesMultithreadBo {

  protected static final Logger LOG = LoggerFactory.getLogger(AsignacionMovimientosDesglosesMultithreadBo.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  public AsignacionMovimientosDesglosesMultithreadBo() {
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void engancharMovimientosFidessaDesgloses(boolean sd, String auditUser) {
    if (sd) {
      this.engancharMovimientosFidessaDesglosesScriptDividend(auditUser);
    }
    else {
      this.engancharMovimientosFidessaDesgloses(auditUser);
    }
  }

  private void engancharMovimientosFidessaDesgloses(String auditUser) {
    char isscrdiv = 'N';
    List<Long> listIds;
    List<Tmct0bokId> listBookingId;
    List<Date> listFechas = movfBo.findAllFliquidacionByAndTitulosDisponibles();
    if (!listFechas.isEmpty()) {

      for (Date fliquidacion : listFechas) {
        listBookingId = dctBo.findAllBookingIdByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion,
            isscrdiv);
        if (!listBookingId.isEmpty()) {
          this.engancharMovimientosFidessaDesglosesByBookingId(fliquidacion, listBookingId, auditUser);
        }

        listIds = movfBo.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
        if (!listIds.isEmpty()) {
          this.restarTitulosMovimientos(fliquidacion, listIds, auditUser);
          listIds.clear();
        }
      }

    }
  }

  private void engancharMovimientosFidessaDesglosesScriptDividend(String auditUser) {
    char isscrdiv = 'S';
    List<Long> listIds;
    List<Tmct0bokId> listBookingId;
    List<Date> listFechas = movfBo.findAllFliquidacionByAndTitulosDisponibles();
    if (!listFechas.isEmpty()) {

      for (Date fliquidacion : listFechas) {
        LOG.debug("[engancharMovimientosFidessaDesglosesScriptDividend] procesando fecha liquidacion == {}",
            fliquidacion);

        listIds = movfBo.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
        if (!listIds.isEmpty()) {
          this.restarTitulosMovimientos(fliquidacion, listIds, auditUser);
          listIds.clear();
        }

        listBookingId = dctBo.findAllBookingIdByFliquidacionAndTitulosDisponiblesAndMasDe1DesglosesDeAlta(fliquidacion,
            isscrdiv);
        if (!listBookingId.isEmpty()) {
          this.engancharMovimientosFidessaDesglosesByBookingIdScriptDividend(fliquidacion, listBookingId, auditUser);
          listIds = movfBo.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
          if (!listIds.isEmpty()) {
            this.restarTitulosMovimientos(fliquidacion, listIds, auditUser);
            listIds.clear();
          }
        }

        listIds = dctBo.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
        if (!listIds.isEmpty()) {
          this.engancharMovimientosFidessaDesglosesSd(fliquidacion, listIds, auditUser);
          listIds.clear();
          listIds = movfBo.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
          if (!listIds.isEmpty()) {
            this.restarTitulosMovimientos(fliquidacion, listIds, auditUser);
            listIds.clear();
          }
        }

      }

    }
  }

  private void engancharMovimientosFidessaDesglosesByBookingId(Date fliquidacion, List<Tmct0bokId> listIds,
      String auditUser) {
    Future<Void> future;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    AsignacionMovimientosDesglosesByBookingIdCallable callable;
    tfb.setNameFormat(String.format("Thread-AsignacionDesdeOrden-%s-%s-%s", auditUser, sdf.format(fliquidacion), "%d"));
    ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());

    for (int i = 0; i < listIds.size(); i++) {
      callable = ctx.getBean(AsignacionMovimientosDesglosesByBookingIdCallable.class, listIds.subList(i, i + 1),
          auditUser);
      future = executor.submit(callable);
      listFutures.add(future);
    }
    listIds.clear();
    executor.shutdown();

    try {
      executor.awaitTermination((long) listFutures.size() + 30, TimeUnit.MINUTES);
    }
    catch (InterruptedException e) {
      LOG.warn("[engancharMovimientosFidessaDesglosesSd] el proceso se ha interrumpido, {}", e.getMessage());
      LOG.trace(e.getMessage(), e);
      executor.shutdownNow();
    }
    for (Future<Void> future2 : listFutures) {
      try {
        future2.get(1, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (ExecutionException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (TimeoutException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
        future2.cancel(true);
      }

    }
    listFutures.clear();
  }

  private void engancharMovimientosFidessaDesglosesByBookingIdScriptDividend(Date fliquidacion,
      List<Tmct0bokId> listIds, String auditUser) {
    Future<Void> future;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    AsignacionMovimientosDesglosesScriptDividendByBookingIdCallable callable;
    tfb.setNameFormat(String.format("Thread-AsignacionDesdeOrdenByBookingId-%s-%s-SD-%s", auditUser,
        sdf.format(fliquidacion), "%d"));
    ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());

    for (int i = 0; i < listIds.size(); i++) {
      callable = ctx.getBean(AsignacionMovimientosDesglosesScriptDividendByBookingIdCallable.class,
          listIds.subList(i, i + 1), auditUser);
      future = executor.submit(callable);
      listFutures.add(future);
    }
    listIds.clear();
    executor.shutdown();

    try {
      executor.awaitTermination((long) listFutures.size() + 30, TimeUnit.MINUTES);
    }
    catch (InterruptedException e) {
      LOG.warn("[engancharMovimientosFidessaDesglosesSd] el proceso se ha interrumpido, {}", e.getMessage());
      LOG.trace(e.getMessage(), e);
      executor.shutdownNow();
    }
    for (Future<Void> future2 : listFutures) {
      try {
        future2.get(1, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (ExecutionException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (TimeoutException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
        future2.cancel(true);
      }

    }
    listFutures.clear();
  }

  private void engancharMovimientosFidessaDesglosesSd(Date fliquidacion, List<Long> listIds, String auditUser) {
    int pos = 0;
    int posEnd = 0;
    Future<Void> future;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    AsignacionMovimientosDesglosesScriptDividendCallable callable;
    tfb.setNameFormat(String.format("Thread-AsignacionDesdeOrden-%s-%s-SD-%s", auditUser, sdf.format(fliquidacion),
        "%d"));
    ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());

    while (pos < listIds.size()) {
      posEnd = Math.min(pos + 100, listIds.size());
      callable = ctx.getBean(AsignacionMovimientosDesglosesScriptDividendCallable.class, listIds.subList(pos, posEnd),
          auditUser);
      future = executor.submit(callable);
      listFutures.add(future);
      pos = posEnd;
    }
    listIds.clear();
    executor.shutdown();

    try {
      executor.awaitTermination((long) listFutures.size() + 30, TimeUnit.MINUTES);
    }
    catch (InterruptedException e) {
      LOG.warn("[engancharMovimientosFidessaDesglosesSd] el proceso se ha interrumpido, {}", e.getMessage());
      LOG.trace(e.getMessage(), e);
      executor.shutdownNow();
    }
    for (Future<Void> future2 : listFutures) {
      try {
        future2.get(1, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (ExecutionException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (TimeoutException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
        future2.cancel(true);
      }

    }
    listFutures.clear();
  }

  private void restarTitulosMovimientos(Date fliquidacion, List<Long> listIds, String auditUser) {
    int pos = 0;
    int posEnd = 0;
    Future<Void> future;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    RestarTitulosMovimientoFidessaCallable callable;
    tfb.setNameFormat(String.format("Thread-RestarTitulosMovimientosFidessa-%s-%s-SD-%s", auditUser,
        sdf.format(fliquidacion), "%d"));
    ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());

    while (pos < listIds.size()) {
      posEnd = Math.min(pos + 100, listIds.size());
      callable = ctx.getBean(RestarTitulosMovimientoFidessaCallable.class, listIds.subList(pos, posEnd), auditUser);
      future = executor.submit(callable);
      listFutures.add(future);
      pos = posEnd;
    }
    listIds.clear();
    executor.shutdown();

    try {
      executor.awaitTermination((long) listFutures.size() + 30, TimeUnit.MINUTES);
    }
    catch (InterruptedException e) {
      LOG.warn("[engancharMovimientosFidessaDesglosesSd] el proceso se ha interrumpido, {}", e.getMessage());
      LOG.trace(e.getMessage(), e);
      executor.shutdownNow();
    }
    for (Future<Void> future2 : listFutures) {
      try {
        future2.get(1, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (ExecutionException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (TimeoutException e) {
        LOG.warn("[engancharMovimientosFidessaDesglosesSd] ha ocurrido una incidencia en el proceso, {}",
            e.getMessage());
        LOG.trace(e.getMessage(), e);
        future2.cancel(true);
      }

    }
    listFutures.clear();
  }
}
