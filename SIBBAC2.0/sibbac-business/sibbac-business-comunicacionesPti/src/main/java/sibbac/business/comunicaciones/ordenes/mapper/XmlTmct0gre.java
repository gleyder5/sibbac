package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0gre;
import sibbac.business.wrappers.database.model.Tmct0greId;

import com.sv.sibbac.multicenter.AllocationID;
import com.sv.sibbac.multicenter.ExecutionGroupType;

public class XmlTmct0gre {
  private Tmct0gre tmct0gre;

  public XmlTmct0gre(Tmct0bok tmct0bok) {
    super();
    tmct0gre = new Tmct0gre();
    tmct0gre.setId(new Tmct0greId());
    tmct0gre.getId().setNuorden(tmct0bok.getId().getNuorden());
    tmct0gre.getId().setNbooking(tmct0bok.getId().getNbooking());
    tmct0gre.setTmct0bok(tmct0bok);
  }

  public void xml_mapping(String nbooking, ExecutionGroupType executionsGroup,
      Map<String, Map<String, Object>> mBooking, Map<String, Tmct0eje> mExecution) throws XmlOrdenExceptionReception {
    Map<String, Object> mAllocation;
    String nucnfclt;
    String nuagreje = executionsGroup.getNuagreje();
    tmct0gre.getId().setNuagreje(nuagreje);
    tmct0gre.setCdbroker(executionsGroup.getTradingVenue());
    tmct0gre.setFhaudit(new java.util.Date());
    tmct0gre.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);
    tmct0gre.setFevalor(new Date(executionsGroup.getFevalor().toGregorianCalendar().getTimeInMillis()));
    tmct0gre.setNutitagr(new BigDecimal(executionsGroup.getNutitagr()).setScale(6, RoundingMode.HALF_UP));
    tmct0gre.setPrecioBrutoMkt(new BigDecimal(executionsGroup.getPrecioBrutoMkt()).setScale(8, RoundingMode.HALF_UP));
    tmct0gre.setTpopebol(executionsGroup.getTpopebol());
    tmct0gre.setAsignadoComp(executionsGroup.isAsignadoComp() ? true : false);

    /*
     * carga en tmct0gal de la lista de elementos AllocationID
     */
    List<Tmct0gal> collection1 = new ArrayList<Tmct0gal>();
    for (AllocationID allocationId : executionsGroup.getAllocation()) {

      tmct0gre.setCdmercad(mExecution.get(allocationId.getExecutions().get(0).getNuejecuc()).getCdmercad());
      XmlTmct0gal xmlTmct0gal = new XmlTmct0gal(tmct0gre);

      if ((mAllocation = mBooking.get(nucnfclt = allocationId.getNucnfclt())) == null) {
        throw new XmlOrdenExceptionReception(tmct0gre.getId().getNuorden(), tmct0gre.getId().getNbooking(), "", "",
            String.format("No se informa el Booking/Allocation '%s'/'%s'", nbooking, nucnfclt));
      }
      xmlTmct0gal.xml_mapping(executionsGroup, allocationId, mAllocation);
      collection1.add(xmlTmct0gal.getJDOObject());

      // Por cada Allocation hay que saber qué ExecutionGroup/Allocation es
      // la última, pues es la que se llevará el total del resto por redondeo
      mAllocation.put("ultima", xmlTmct0gal.getJDOObject());
    }
    tmct0gre.getTmct0gals().addAll(collection1);
  }

  public Tmct0gre getJDOObject() {
    return tmct0gre;
  }

}
