package sibbac.business.comunicaciones.euroccp.common;

import java.io.Serializable;
import java.util.Date;

import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;

public class EuroCcpRangoFechasHorasInicioProcesosDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3091288834289095014L;
  private EuroCcpFileType euroCcpFileType;
  private int dDaysLastDate;
  private Date endSendHourLastDate;
  private int dDaysFirstSendDate;
  private Date initSendHourFirstDate;

  public EuroCcpRangoFechasHorasInicioProcesosDTO() {
  }

  public EuroCcpFileType getEuroCcpFileType() {
    return euroCcpFileType;
  }

  public int getdDaysLastDate() {
    return dDaysLastDate;
  }

  public Date getEndSendHourLastDate() {
    return endSendHourLastDate;
  }

  public int getdDaysFirstSendDate() {
    return dDaysFirstSendDate;
  }

  public Date getInitSendHourFirstDate() {
    return initSendHourFirstDate;
  }

  public void setEuroCcpFileType(EuroCcpFileType euroCcpFileType) {
    this.euroCcpFileType = euroCcpFileType;
  }

  public void setdDaysLastDate(int dDaysLastDate) {
    this.dDaysLastDate = dDaysLastDate;
  }

  public void setEndSendHourLastDate(Date endSendHourLastDate) {
    this.endSendHourLastDate = endSendHourLastDate;
  }

  public void setdDaysFirstSendDate(int dDaysFirstSendDate) {
    this.dDaysFirstSendDate = dDaysFirstSendDate;
  }

  public void setInitSendHourFirstDate(Date initSendHourFirstDate) {
    this.initSendHourFirstDate = initSendHourFirstDate;
  }

  @Override
  public String toString() {
    return "EuroCcpRangoFechasHorasInicioProcesosDTO [euroCcpFileType=" + euroCcpFileType + ", dDaysLastDate="
           + dDaysLastDate + ", endSendHourLastDate=" + endSendHourLastDate + ", dDaysFirstSendDate="
           + dDaysFirstSendDate + ", initSendHourFirstDate=" + initSendHourFirstDate + "]";
  }
  
  
  

}
