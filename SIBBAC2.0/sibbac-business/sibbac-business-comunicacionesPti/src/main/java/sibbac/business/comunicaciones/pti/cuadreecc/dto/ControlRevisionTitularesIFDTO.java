package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import sibbac.business.wrappers.common.ObserverProcess;

public class ControlRevisionTitularesIFDTO extends ObserverProcess {

  /**
   * 
   */
  private static final long serialVersionUID = 3971947043452839435L;

  private Collection<IdentificacionOperacionCuadreEccDTO> identificaciones = Collections.synchronizedCollection(new HashSet<IdentificacionOperacionCuadreEccDTO>());

  public ControlRevisionTitularesIFDTO() {
    super();
  }

  public Collection<IdentificacionOperacionCuadreEccDTO> getIdentificaciones() {
    return identificaciones;
  }

  public void setIdentificaciones(Collection<IdentificacionOperacionCuadreEccDTO> identificaciones) {
    this.identificaciones = identificaciones;
  }

}
