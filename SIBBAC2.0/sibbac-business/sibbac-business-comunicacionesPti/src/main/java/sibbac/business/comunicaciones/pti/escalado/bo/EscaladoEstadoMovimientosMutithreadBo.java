package sibbac.business.comunicaciones.pti.escalado.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.titulares.bo.TitularesBo;
import sibbac.business.titulares.bo.TitularesConfigDTO;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class EscaladoEstadoMovimientosMutithreadBo {

  private static final Logger LOG = LoggerFactory.getLogger(EscaladoEstadoMovimientosMutithreadBo.class);

  @Value("${sibbac.nacional.check.status.movimientos.threadpool.size:10}")
  private int threadSize;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private EscaladoEstadoMovimientosBo escaladoBo;

  @Autowired
  private TitularesBo titularesBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  public EscaladoEstadoMovimientosMutithreadBo() {
  }

  @Transactional(readOnly = true)
  public void escalarEstadosMovimientos(final boolean isScripDividend, final String auditUser)
      throws SIBBACBusinessException {
    Future<CallableResultDTO> futureTask;
    CallableResultDTO resultTask;
    int transactionSize = 1;
    LOG.trace("[escalarEstadosMovimientos] inicio.");
    char isSd = 'N';
    if (isScripDividend) {
      isSd = 'S';
    }
    List<Tmct0bokId> listBookingsId = movBo.findAllBookingWithMovimientosRecibiendose(isSd);
    LOG.trace("[escalarEstadosMovimientos] encontrados {} bookings con movimientos recibiendose...",
        listBookingsId.size());
    if (CollectionUtils.isNotEmpty(listBookingsId)) {
      try (TitularesConfigDTO config = titularesBo.getConfig(auditUser)) {
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        if (isScripDividend) {
          tfb.setNameFormat("Thread-EscaladoMovimientos-SD-%d");
        }
        else {
          tfb.setNameFormat("Thread-EscaladoMovimientos-%d");
        }
        ExecutorService executor = Executors.newFixedThreadPool(threadSize, tfb.build());
        List<Future<CallableResultDTO>> taskList = new ArrayList<Future<CallableResultDTO>>();
        // ExecutorService executor = Executors.newFixedThreadPool(threadSize);
        try {

          int pos = 0;
          int posEnd = 0;

          while (pos < listBookingsId.size()) {

            posEnd += transactionSize;
            if (posEnd > listBookingsId.size()) {
              posEnd = listBookingsId.size();
            }
            LOG.trace("[escalarEstadosMovimientos] pos: {} posEnd: {}", pos, posEnd);
            futureTask = executor.submit(getCallableProcessEscalarEstadosMovimientos(
                listBookingsId.subList(pos, posEnd), config));
            taskList.add(futureTask);
            pos = posEnd;
          }

        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
        finally {
          if (!executor.isShutdown()) {
            executor.shutdown();
          }
          if (!executor.isTerminated()) {
            try {
              executor.awaitTermination(12, TimeUnit.HOURS);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
              throw new SIBBACBusinessException(e.getMessage(), e);
            }
          }
          if (!executor.isTerminated()) {
            executor.shutdownNow();
            try {
              executor.awaitTermination(5, TimeUnit.MINUTES);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
              throw new SIBBACBusinessException(e.getMessage(), e);
            }
          }
        }
        try {
          for (Future<CallableResultDTO> future : taskList) {
            try {
              resultTask = future.get(0, TimeUnit.MINUTES);
              LOG.trace("[escalarEstadosMovimientos] result: {}", resultTask);
            }
            catch (TimeoutException e) {
              LOG.warn(e.getMessage(), e);
              future.cancel(true);
            }

          }
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }

    }
    LOG.trace("[escalarEstadosMovimientos] fin.");
  }

  private Callable<CallableResultDTO> getCallableProcessEscalarEstadosMovimientos(final List<Tmct0bokId> listBookId,
      final TitularesConfigDTO config) {
    Callable<CallableResultDTO> callableTask = ctx.getBean(EscaladoEstadoMovimientosByBookingCallable.class,
        listBookId, config);
    return callableTask;
  }

}
