package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIConstants.PV_CONSTANTS;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.pv.PV;
import sibbac.pti.messages.pv.R00;
import sibbac.pti.messages.pv.R01;
import sibbac.pti.messages.pv.t2s.R01T2s;

@Service("ptiMessageProcessPvBo")
public class PtiMessageProcessPvBo extends PtiMessageProcess<PV> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessPvBo.class);

  @Autowired
  private Tmct0ValoresBo vaBo;

  public PtiMessageProcessPvBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, PV pv, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    List<PTIMessageRecord> r00s = pv.getAll(PTIMessageRecordType.R00);
    List<PTIMessageRecord> r01s = pv.getAll(PTIMessageRecordType.R01);
    if (CollectionUtils.isEmpty(r00s)) {
      LOG.warn("PV NO TIENE R00'S");
    }
    else {
      // Tratamos el R00 primero.
      R00 r00 = (R00) r00s.get(0);
      String codigoDeValor = r00.getCodigoDeValor();
      String codigoDeContratacion = r00.getCodigoContratacion();

      Tmct0Valores valor = null;
      // Buscamos el valor.
      List<Tmct0Valores> valores = vaBo.findByCodigoDeValorOrderByFechaEmisionDesc(codigoDeValor);
      if (CollectionUtils.isEmpty(valores)) {
        throw new SIBBACBusinessException(String.format("NO se han encontrado (%s) registros para dicho valor (%s/%s)",
            valores.size(), codigoDeValor, codigoDeContratacion));
      }
      else if (valores.size() > 1) {
        // Como no pueden haber mas de 1, salimos.
        throw new SIBBACBusinessException(String.format("Se han encontrado (%s) registros para dicho valor (%s/%s)",
            valores.size(), codigoDeValor, codigoDeContratacion));
      }

      // Tenemos el valor.
      valor = valores.get(0);

      // Actualizamos los datos de los R01's.
      if (CollectionUtils.isEmpty(r01s)) {
        LOG.warn("PV NO TIENE R01'S");
        // Nada que hacer. No hay informacion.
      }
      else {
        R01 r01 = null;
        String tipo = null;
        for (PTIMessageRecord record : r01s) {
          r01 = (R01) record;

          // Que tipo de informacion viene...
          tipo = r01.getTipoPrecioCierre();
          if (tipo.equalsIgnoreCase(PV_CONSTANTS.TIPO_PRECIO_CIERRE.PRECIO_CIERRE_SESION_ACTUAL)) {
            valor.setPrecioCierreSesionActual(r01.getPrecio());
          }
          else if (tipo.equalsIgnoreCase(PV_CONSTANTS.TIPO_PRECIO_CIERRE.PRECIO_CIERRE_SESION_ANTERIOR)) {
            valor.setPrecioCierreSesionAnterior(r01.getPrecio());
          }

          if (version == PTIMessageVersion.VERSION_T2S) {
            valor.setPorcentajeCuponCorridoRf(((R01T2s) r01).getCuponCorridoRf());
          }
        }
        vaBo.save(valor);
      }

    }
  }
}
