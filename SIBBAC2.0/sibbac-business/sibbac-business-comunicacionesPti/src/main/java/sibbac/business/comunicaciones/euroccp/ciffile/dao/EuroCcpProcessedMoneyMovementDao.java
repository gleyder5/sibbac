package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedMoneyMovement;

@Component
public interface EuroCcpProcessedMoneyMovementDao extends JpaRepository<EuroCcpProcessedMoneyMovement, Long> {

  EuroCcpProcessedMoneyMovement findByProcessingDateAndClientNumberAndAccountNumberAndSubaccountNumber(
      Date processingDate, String clientNumber, String accountNumber, String subaccountNumber);

}