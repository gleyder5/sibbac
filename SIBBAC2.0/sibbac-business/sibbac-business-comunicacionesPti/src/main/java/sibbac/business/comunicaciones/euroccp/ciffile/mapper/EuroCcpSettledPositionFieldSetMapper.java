package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpSettledPositionFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettledPosition;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpSettledPositionFieldSetMapper extends WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean> {

  public EuroCcpSettledPositionFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpSettledPositionFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {

    return new EuroCcpSettledPosition();
  }

}
