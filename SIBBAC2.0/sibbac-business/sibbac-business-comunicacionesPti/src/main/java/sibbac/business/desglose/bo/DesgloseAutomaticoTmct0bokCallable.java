package sibbac.business.desglose.bo;

import java.util.concurrent.Callable;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

class DesgloseAutomaticoTmct0bokCallable implements Callable<Void> {

  private DesgloseWithUserLock desgloseAutomatico;
  private DesgloseConfigDTO config;
  private Tmct0bokId bookId;

  DesgloseAutomaticoTmct0bokCallable(DesgloseWithUserLock desgloseAutomatico, DesgloseConfigDTO config,
      Tmct0bokId bookId) {
    this.config = config;
    this.desgloseAutomatico = desgloseAutomatico;
    this.bookId = bookId;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    desgloseAutomatico.desgloseAutomatico(bookId, config);
    return null;
  }

  Tmct0bokId getBookId() {
    return bookId;
  }

  void setBookId(Tmct0bokId bookId) {
    this.bookId = bookId;
  }

}
