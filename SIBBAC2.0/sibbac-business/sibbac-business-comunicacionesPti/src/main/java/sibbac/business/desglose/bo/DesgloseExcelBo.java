package sibbac.business.desglose.bo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.desglose.commons.AlgoritmoDesgloseManual;
import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.DesgloseManualDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.HolderTipoOrigenDatosType;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClearerDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class DesgloseExcelBo {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(DesgloseBo.class);

  @Value("${sibbac.nacional.desglose.excel.work.path:}")
  private String workPath;

  @Value("${sibbac.nacional.desglose.excel.read.workbook.thread.size:5}")
  private int threadSize;

  private int posRowType = 0;

  private int posId = 1;

  private int posAlgoritmo = 2;

  private int posTitulos = 3;

  private int posHolder = 4;
  private int posHolderType = 5;

  private int posBankReference = 6;
  private int posAccountAtClearer = 7;

  private int posPositionBreakDown = 8;

  private int posClearer = 9;
  private int posBankComissionPercent = 10;
  private int posPayerComissionPercent = 11;
  private int posCustodian = 12;

  private int posPriceFrom = 13;
  private int posClearingMemberFrom = 14;
  private int posGiveUpReferenceFrom = 15;
  private int posClearingAccountCodeFrom = 16;

  private int posClearingRuleTo = 17;

  private int posClearingMemberTo = 18;
  private int posGiveUpReferenceTo = 19;
  private int posClearingAccountCodeTo = 20;

  private int posSentClearingCommissionMethod = 21;

  // private String columnasNoInformadasRowAllo = "2";
  // private String columnasInformadasRowAllo = "1,3";
  // private String columnasNoInformadasRowDesglose = "1";
  // private String columnasInformadasRowDesglose = "2,3,4";
  // private String columnasNoInformadasRowHolder = "1,2,3";
  // private String columnasInformadasRowHolder = "4,5";

  private String tramaPattern = ".*TRMA.*";
  private String camaPattern = ".*CAMA.*";
  private String nurefordPattern = "\\d";

  // private int maxLengthRow = 20;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0tfiBo titularesInstruccionLiquidacionBo;

  @Autowired
  private DesgloseMultithread desgloseMultithread;

  @Autowired
  private ApplicationContext ctx;

  /**
   * 
   * CUANDO HAY VARIOS ALOS EN UN BOOKING DEBEN IR CONSECUTIVOS
   * @param is
   * @param file
   * @param config
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public CallableResultDTO desglosarExcel(InputStream is, String file, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    LOG.info("[desglosarExcel] inicio fichero: {} user: {}.", file, config.getAuditUser());
    CallableResultDTO res = new CallableResultDTO();
    Path ficheroBackup = this.pasarABackupFichero(is, file, config);
    List<SolicitudDesgloseManual> listSolicitudDesgloseManual = Collections
        .synchronizedList(new ArrayList<SolicitudDesgloseManual>());
    String extension = FilenameUtils.getExtension(file);
    try {
      LOG.trace("[desglosarExcel] abriendo fichero excel...");
      if (extension == null || extension.trim().isEmpty() || extension.trim().toUpperCase().equals("XLSX")) {
        try (InputStream myis = Files.newInputStream(ficheroBackup, StandardOpenOption.READ);
            GZIPInputStream mygis = new GZIPInputStream(myis);
            Workbook workbook = new XSSFWorkbook(mygis);) {
          res.append(this.leerWorkbook(workbook, listSolicitudDesgloseManual, config));
        }
      }
      else if (extension.trim().toUpperCase().equals("XLS")) {
        try (InputStream myis = Files.newInputStream(ficheroBackup, StandardOpenOption.READ);
            GZIPInputStream mygis = new GZIPInputStream(myis);
            Workbook workbook = new HSSFWorkbook(mygis);) {
          res.append(this.leerWorkbook(workbook, listSolicitudDesgloseManual, config));
        }
      }
      else {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Extension de fichero: %s no reconocida, debe ser xlsx o xls.", file));
      }
      if (CollectionUtils.isNotEmpty(listSolicitudDesgloseManual)) {
        LOG.debug("[desglosarExcel] lanzar proceso desglose manual EXCEL...");
        res.append(desgloseMultithread.desgloseManual(listSolicitudDesgloseManual, config));
      }
    }
    catch (IOException e) {
      LOG.warn(e.getMessage(), e);
      res.addErrorMessage("No se ha podido leer el fichero, compruebe el formato");
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
      res.addErrorMessage(e.getMessage());
    }
    finally {
      listSolicitudDesgloseManual.clear();
    }
    LOG.info("[desglosarExcel] fin fichero: {} user: {} respuesta: {}.", file, config.getAuditUser(), res);
    return res;
  }

  private CallableResultDTO leerWorkbook(Workbook workbook, List<SolicitudDesgloseManual> listSolicitudDesgloseManual,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.debug("[leerWorkbook] inicio.");
    ExecutorService executor;
    Sheet sheet = workbook.getSheetAt(0);
    CallableResultDTO res = new CallableResultDTO();
    List<Future<CallableResultDTO>> listFutures = new ArrayList<Future<CallableResultDTO>>();
    if (sheet != null) {

      List<Integer> listPosAlos = this.buscarPosicionesAllos(sheet);
      if (CollectionUtils.isNotEmpty(listPosAlos)) {
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        tfb.setNameFormat(String.format("Thread-Leer-%s-%s-%s", config.getTipoDesgloseManual(), config.getAuditUser(),
            "%d"));
        executor = Executors.newFixedThreadPool(threadSize, tfb.build());

        try {
          this.lanzarProcesos(executor, sheet, listPosAlos, listSolicitudDesgloseManual, listFutures, config);
          executor.shutdown();
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();
        }
        finally {
          this.recuperarResultado(executor, sheet, listPosAlos, listSolicitudDesgloseManual, listFutures, res, config);
        }

      }
    }
    LOG.debug("[leerWorkbook] fin res: {}.", res);
    return res;
  }

  private void lanzarProcesos(ExecutorService executor, Sheet sheet, List<Integer> listPosAlos,
      List<SolicitudDesgloseManual> listSolicitudDesgloseManual, List<Future<CallableResultDTO>> listFutures,
      DesgloseConfigDTO config) {
    DesgloseExcelLeerWorkBookCallable proceso;
    Future<CallableResultDTO> future;
    for (Integer posicionAllocationEnExcel : listPosAlos) {

      LOG.debug("[procesarWorkbook] buscando row de allocation pos: {} index: {}", posicionAllocationEnExcel,
          listPosAlos.indexOf(posicionAllocationEnExcel));
      proceso = new DesgloseExcelLeerWorkBookCallable(listPosAlos, posicionAllocationEnExcel, sheet,
          listSolicitudDesgloseManual, config, this);
      future = executor.submit(proceso);
      listFutures.add(future);

    }// fin for pos alos

  }

  private void recuperarResultado(ExecutorService executor, Sheet sheet, List<Integer> listPosAlos,
      List<SolicitudDesgloseManual> listSolicitudDesgloseManual, List<Future<CallableResultDTO>> listFutures,
      CallableResultDTO res, DesgloseConfigDTO config) {
    int index;
    try {
      executor.awaitTermination((long) listPosAlos.size() + 5, TimeUnit.MINUTES);
    }
    catch (InterruptedException e) {
      LOG.warn(e.getMessage(), e);
    }

    for (Future<CallableResultDTO> future2 : listFutures) {
      try {
        index = listFutures.indexOf(future2);
        try {
          res.append(future2.get(10, TimeUnit.SECONDS));
        }
        catch (InterruptedException | ExecutionException e) {
          LOG.warn("[desgloseWithUserLock] incidencia con id {} {}", getId(sheet.getRow(listPosAlos.get(index))),
              e.getMessage(), e);
        }
        catch (TimeoutException e) {
          if (!future2.isDone() && future2.cancel(true)) {
            LOG.warn("[desgloseWithUserLock] cancelando el future: {} id: {}", future2,
                getId(sheet.getRow(listPosAlos.get(index))));
          }
          else {
            LOG.warn("[desgloseWithUserLock] incidencia con id {} {}", getId(sheet.getRow(listPosAlos.get(index))),
                e.getMessage(), e);
          }
        }
        catch (Exception e) {
          LOG.warn("[desgloseWithUserLock] incidencia con id {} {}", getId(sheet.getRow(listPosAlos.get(index))),
              e.getMessage(), e);
        }
      }
      catch (SIBBACBusinessException e) {
        LOG.warn(e.getMessage(), e);
        res.addErrorMessage(e.getMessage());
      }
    }

    if (!executor.isTerminated()) {
      executor.shutdownNow();
      try {
        executor.awaitTermination(1, TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage(), e);
      }
    }
  }

  @Transactional
  public CallableResultDTO leerDatosAllo(List<Integer> listPosAlos, int posicionAllocationEnExcel, Sheet sheet,
      List<SolicitudDesgloseManual> listSolicitudDesgloseManual, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    String msg;
    Tmct0alo alo;
    int indexDesglosesAllo;
    CallableResultDTO res = new CallableResultDTO();

    Row rowAllocation = sheet.getRow(posicionAllocationEnExcel);
    try {
      LOG.debug("[procesarWorkbook] buscando allo...");
      alo = getAlo(rowAllocation);
    }
    catch (SIBBACBusinessException | NumberFormatException | IllegalStateException e) {
      LOG.warn(e.getMessage(), e);
      res.addErrorMessage(e.getMessage());
      return res;
    }

    if (alo != null) {
      LOG.debug("[procesarWorkbook] encontrado allo: {}.", alo.getId());
      indexDesglosesAllo = posicionAllocationEnExcel + 1;
      try {
        SolicitudDesgloseManual solicitudDesglose = this.crearSolicitudDesgloseManual(alo, rowAllocation);

        Row siguiente = sheet.getRow(indexDesglosesAllo);

        if (isEndOfDesglose(siguiente)) {
          this.leerDesgloseAndTitularFilaAlo(alo, solicitudDesglose, sheet, rowAllocation, siguiente,
              indexDesglosesAllo, config);
        }
        else if (isHolderRow(siguiente)) {
          indexDesglosesAllo = this.leerTitularesAlo(alo, solicitudDesglose, sheet, rowAllocation, siguiente,
              indexDesglosesAllo, config);
        }
        else if (isDesgloseRow(siguiente)) {
          indexDesglosesAllo = this.leerDesglosesAndTitularesAlo(alo, solicitudDesglose, sheet, rowAllocation,
              siguiente, indexDesglosesAllo, config);

        }
        else {
          // ERROR FORMATO
          throw new SIBBACBusinessException(MessageFormat.format("INCIDENCIA-Secuencia de datos no valida. Fila: {0}",
              siguiente.getRowNum()));
        }

        if (listPosAlos.indexOf(posicionAllocationEnExcel) == listPosAlos.size() - 1
            || listPosAlos.contains(siguiente.getRowNum())) {
          this.rellenarDatosIncompletosInferibles(alo, solicitudDesglose, config);
          this.validarSolicitudDesgloseManual(solicitudDesglose, config);
          LOG.debug("[procesarWorkbook] aniandiendo solicidud a lista...");
          listSolicitudDesgloseManual.add(solicitudDesglose);
          // desgloseAutomaticoBo.desgloseNacionalManual(solicitudDesglose,
          // config);
        }
        else {
          // ERROR FORMATO
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA-Secuencia de datos no valida, id: '%s' no se desglosa. Fila: %s", getId(rowAllocation),
              siguiente.getRowNum()));
        }
      }
      catch (Exception e) {
        logBo.insertarRegistroNewTransaction(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId()
            .getNucnfclt(), e.getMessage(), alo.getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
        msg = String.format("[%s-%s-%s] %s", StringUtils.trim(alo.getId().getNuorden()),
            StringUtils.trim(alo.getId().getNucnfclt()), StringUtils.trim(alo.getId().getNucnfclt()), e.getMessage());
        LOG.warn(msg, e);
        res.addErrorMessage(msg);
      }
    }
    else {
      msg = String.format("[%s] Allocation no encontrada. Fila: %s.", getId(rowAllocation), rowAllocation.getRowNum());
      LOG.warn(msg);
      res.addErrorMessage(msg);
    }
    return res;
  }

  private boolean isEndOfDesglose(Row siguiente) throws SIBBACBusinessException {
    return siguiente == null || getRowType(siguiente) == null || getRowType(siguiente) == ' ' || isAloRow(siguiente);
  }

  private SolicitudDesgloseManual crearSolicitudDesgloseManual(Tmct0alo alo, Row rowAllocation)
      throws SIBBACBusinessException {
    LOG.debug("[crearSolicitudDesgloseManual] inicio.");
    SolicitudDesgloseManual solicitudDesglose = new SolicitudDesgloseManual();
    setAlgoritmoDesglose(solicitudDesglose, rowAllocation);
    solicitudDesglose.setTmct0aloId(alo.getId());
    solicitudDesglose.setBajas(new ArrayList<Tmct0alcId>());
    solicitudDesglose.setAltas(new ArrayList<DesgloseManualDTO>());
    Character positionBreakDown = getPositionBreakDown(rowAllocation);
    solicitudDesglose.setPositionBreakDown(positionBreakDown);
    LOG.debug("[crearSolicitudDesgloseManual] fin.");
    return solicitudDesglose;
  }

  private int leerDesgloseAndTitularFilaAlo(Tmct0alo alo, SolicitudDesgloseManual solicitudDesglose, Sheet sheet,
      Row rowAllocation, Row siguiente, int indexDesglosesAllo, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    LOG.debug("[leerDesgloseAndTitularFilaAlo] inicio.");
    DesgloseManualDTO desgloseManual = recuperarDesglose(sheet, rowAllocation, alo, solicitudDesglose, config);
    if (StringUtils.isNotBlank(getHolder(rowAllocation))) {
      desgloseManual.getSettlementData().setHolder(null);
      desgloseManual.getSettlementData().getListHolder().clear();
      desgloseManual.getSettlementData().setDataTFI(false);
      recuperarTitular(sheet, rowAllocation, alo, desgloseManual, config);
      if (CollectionUtils.isNotEmpty(desgloseManual.getSettlementData().getListHolder())) {
        desgloseManual.getSettlementData().setHolder(desgloseManual.getSettlementData().getListHolder().get(0));
      }
    }
    LOG.debug("[leerDesgloseAndTitularFilaAlo] fin.");
    return indexDesglosesAllo;
  }

  private int leerDesglosesAndTitularesAlo(Tmct0alo alo, SolicitudDesgloseManual solicitudDesglose, Sheet sheet,
      Row rowAllocation, Row siguiente, int indexDesglosesAllo, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    LOG.debug("[leerDesglosesAndTitularesAlo] inicio.");
    while (isDesgloseRow(siguiente)) {
      DesgloseManualDTO desgloseManual = recuperarDesglose(sheet, siguiente, alo, solicitudDesglose, config);

      boolean desgloseConTitular = StringUtils.isNotEmpty(getHolder(siguiente));
      if (desgloseConTitular) {
        desgloseManual.getSettlementData().setHolder(null);
        desgloseManual.getSettlementData().getListHolder().clear();
        desgloseManual.getSettlementData().setDataTFI(false);
        recuperarTitular(sheet, siguiente, alo, desgloseManual, config);

      }
      indexDesglosesAllo++;
      siguiente = sheet.getRow(indexDesglosesAllo);

      if (isHolderRow(siguiente) && !desgloseConTitular) {
        desgloseManual.getSettlementData().setHolder(null);
        desgloseManual.getSettlementData().getListHolder().clear();
        desgloseManual.getSettlementData().setDataTFI(false);
      }
      while (isHolderRow(siguiente)) {
        recuperarTitular(sheet, siguiente, alo, desgloseManual, config);
        indexDesglosesAllo++;
        siguiente = sheet.getRow(indexDesglosesAllo);
      }
      if (CollectionUtils.isNotEmpty(desgloseManual.getSettlementData().getListHolder())) {
        desgloseManual.getSettlementData().setHolder(desgloseManual.getSettlementData().getListHolder().get(0));
      }
    }
    LOG.debug("[leerDesglosesAndTitularesAlo] fin.");
    return indexDesglosesAllo;
  }

  private int leerTitularesAlo(Tmct0alo alo, SolicitudDesgloseManual solicitudDesglose, Sheet sheet, Row rowAllocation,
      Row siguiente, int indexDesglosesAllo, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.debug("[leerTitularesAlo] inicio.");
    DesgloseManualDTO desgloseManual = recuperarDesglose(sheet, rowAllocation, alo, solicitudDesglose, config);
    desgloseManual.getSettlementData().setHolder(null);
    desgloseManual.getSettlementData().getListHolder().clear();
    desgloseManual.getSettlementData().setDataTFI(false);
    if (StringUtils.isNotBlank(getHolder(rowAllocation))) {
      recuperarTitular(sheet, rowAllocation, alo, desgloseManual, config);
    }

    while (isHolderRow(siguiente)) {
      recuperarTitular(sheet, siguiente, alo, desgloseManual, config);
      indexDesglosesAllo++;
      siguiente = sheet.getRow(indexDesglosesAllo);

    }
    if (CollectionUtils.isNotEmpty(desgloseManual.getSettlementData().getListHolder())) {
      desgloseManual.getSettlementData().setHolder(desgloseManual.getSettlementData().getListHolder().get(0));
    }
    LOG.debug("[leerTitularesAlo] fin.");
    return indexDesglosesAllo;
  }

  private Path pasarABackupFichero(InputStream is, String file, DesgloseConfigDTO config)
      throws SIBBACBusinessException {

    String fileExtension = FilenameUtils.getExtension(file);
    String baseName = FilenameUtils.getBaseName(file);
    if (fileExtension == null || fileExtension.trim().isEmpty()) {
      fileExtension = "xlsx";
      LOG.warn("[pasarABackupFichero] no esta llegando la extension del fichero: {}, usaremos: {} por defecto.", file,
          fileExtension);
    }
    Path rutaBackupFicheros = Paths.get(workPath, config.getAuditUser());

    if (Files.notExists(rutaBackupFicheros)) {
      try {
        Files.createDirectories(rutaBackupFicheros);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-No se ha podido crear la ruta: '%s' para guardar el backup de los ficheros.",
            rutaBackupFicheros.toString()));
      }
    }

    String fileName = String.format("%s_%s_%s.%s.gz", baseName, config.getAuditUser(),
        FormatDataUtils.convertDateToConcurrentFileName(new Date()), fileExtension);

    Path ficheroBackup = Paths.get(rutaBackupFicheros.toString(), fileName);
    LOG.info("[desglosarExcel] fichero backup: {} user: {}", ficheroBackup.toString(), config.getAuditUser());
    try (OutputStream os = Files
        .newOutputStream(ficheroBackup, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW);
        GZIPOutputStream gos = new GZIPOutputStream(os);) {
      IOUtils.copyLarge(is, gos);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA-No se ha podido crear la el fichero: '%s' como backup.", ficheroBackup.toString()));
    }

    return ficheroBackup;
  }

  private List<Integer> buscarPosicionesAllos(Sheet sheet) throws SIBBACBusinessException {
    LOG.debug("[buscarPosicionesAllos] inicio.");
    List<Integer> res = new ArrayList<Integer>();
    int rowNumber = sheet.getPhysicalNumberOfRows();
    Row row;
    for (int i = 1; i < rowNumber; i++) {
      row = sheet.getRow(i);
      if (row != null && isAloRow(row)) {
        res.add(i);
      }
    }
    LOG.debug("[buscarPosicionesAllos] fin respuesta: {}.", res);
    return res;
  }

  private DesgloseManualDTO recuperarDesglose(Sheet sheet, Row row, Tmct0alo alo, SolicitudDesgloseManual solicitud,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    DesgloseManualDTO desgloseManual = new DesgloseManualDTO();
    desgloseManual.setTitulos(getTitulos(row));
    desgloseManual.setClearingAccountCodeFrom(getClearingAccountCodeFrom(row));
    desgloseManual.setClearingAccountCodeTo(getClearingAccountCodeTo(row));
    desgloseManual.setClearingAccountTo(null);// TODO
    desgloseManual.setClearingMemberCodeFrom(getClearingMemberFrom(row));
    desgloseManual.setClearingMemberCodeTo(getClearingMemberTo(row));
    desgloseManual.setClearingMemberTo(null); // TODO
    desgloseManual.setClearingRuleDescriptionTo("");

    // desgloseManual.setClearingRuleTo(getClearingRuleTo(row)); //TODO
    desgloseManual.setGiveUpReferenceFrom(getGiveUpReferenceFrom(row));
    desgloseManual.setGiveUpReferenceTo(getGiveUpReferenceTo(row));

    desgloseManual.setPayerComission(null); // TODO

    desgloseManual.setPrecioFrom(getPriceFrom(row));
    desgloseManual.setSettlementData(config.getSettlementDataNacional(alo));
    BigDecimal bankComissionPercent = getBankComissionPercent(row);
    if (bankComissionPercent == null) {
      bankComissionPercent = desgloseManual.getSettlementData().getTmct0ilq().getPccombco();
    }
    desgloseManual.setBankComissionPercent(bankComissionPercent);
    BigDecimal payerComissionPercent = getPayerComissionPercent(row);
    if (payerComissionPercent == null) {
      payerComissionPercent = desgloseManual.getSettlementData().getTmct0ilq().getPccombrk();
    }
    desgloseManual.setPayerComissionPercent(payerComissionPercent);

    desgloseManual.setTipoEnvioComision(getSentClearingComissionMethod(row));
    desgloseManual.setUniqueCustomerReference(false); // TODO

    if (getClearingRuleTo(row) != null) {
      desgloseManual.setClearingRuleTo(new BigInteger(getClearingRuleTo(row).toString()));
    }
    else {
      if (StringUtils.isBlank(desgloseManual.getClearingAccountCodeTo())
          && StringUtils.isBlank(desgloseManual.getClearingMemberCodeTo())
          && StringUtils.isBlank(desgloseManual.getGiveUpReferenceTo())) {
        // Tmct0Regla regla = config.getRegla(alo);
        // if (regla != null) {
        // desgloseManual.setClearingRuleTo(regla.getIdRegla());
        // desgloseManual.setClearingRuleDescriptionTo(regla.getNbDescripcion());
        // }
      }
    }
    desgloseManual.getSettlementData().setClearerMultiple(false);
    String clearer = getClearer(row);
    if (StringUtils.isNotBlank(clearer)) {
      desgloseManual.getSettlementData().setDataClearer(false);

      List<SettlementDataClearerDTO> listClearers = config.getClearerNacional(alo, clearer);
      if (CollectionUtils.isEmpty(listClearers)) {

        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Clearer '%s' no encontrado en el maestro de clearers. Fila: %s", clearer, row.getRowNum()));
      }
      else if (listClearers.size() > 1) {
        desgloseManual.getSettlementData().setClearerMultiple(true);
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Multiple Clearer's '%s' en el maestro de clearers. Fila: %s", clearer, row.getRowNum()));
      }
      else {
        desgloseManual.getSettlementData().setClearer(listClearers.get(0));
        desgloseManual.getSettlementData().setDataClearer(true);
      }
    }
    String custodian = getCustodian(row);
    if (StringUtils.isNotBlank(custodian)) {
      desgloseManual.getSettlementData().setDataCustodian(false);
      List<SettlementDataClearerDTO> listCustodians = config.getClearerNacional(alo, custodian);
      if (CollectionUtils.isEmpty(listCustodians)) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Custodian '%s' no encontrado en el maestro de clearers. Fila: %s", custodian, row.getRowNum()));
      }
      else if (listCustodians.size() > 1) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Multiple Clustodian's '%s' en el maestro de clearers. Fila: %s", custodian, row.getRowNum()));
      }
      else {
        desgloseManual.getSettlementData().setCustodian(listCustodians.get(0));
        desgloseManual.getSettlementData().setDataCustodian(true);
      }
    }

    Character positionBreakDown = solicitud.getPositionBreakDown();
    if (positionBreakDown == null || positionBreakDown == ' ') {
      if (desgloseManual.getSettlementData().getTmct0ilq().getDesglose() == ' '
          || desgloseManual.getSettlementData().getTmct0ilq().getDesglose() == null) {
        desgloseManual.getSettlementData().getTmct0ilq().setDesglose('P');
      }
    }
    else {
      desgloseManual.getSettlementData().getTmct0ilq().setDesglose(positionBreakDown);
    }

    desgloseManual.getSettlementData().setDataIL(true);

    desgloseManual.setBankReference(getBankReference(row));
    desgloseManual.setAccountAtClearer(getAccountAtClearer(row));

    solicitud.getAltas().add(desgloseManual);

    return desgloseManual;
  }

  private void rellenarDatosIncompletosInferibles(Tmct0alo alo, SolicitudDesgloseManual solicitudDesglose,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.debug("[rellenarDatosIncompletosInferibles] inicio.");
    if (solicitudDesglose.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.FEWEST
        && solicitudDesglose.getAltas().size() == 1
        && (solicitudDesglose.getAltas().get(0).getTitulos() == null || solicitudDesglose.getAltas().get(0)
            .getTitulos().compareTo(BigDecimal.ZERO) == 0)) {
      solicitudDesglose.getAltas().get(0).setTitulos(alo.getNutitcli());
    }
    LOG.debug("[rellenarDatosIncompletosInferibles] fin.");
  }

  private void validarSolicitudDesgloseManual(SolicitudDesgloseManual solicitud, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    LOG.debug("[validarDatosDesgloseManualInformados] inicio");
    for (DesgloseManualDTO desglose : solicitud.getAltas()) {

      if (desglose.getClearingAccountCodeFrom() != null) {
        if (config.getCuentaCompensacion(desglose.getClearingAccountCodeFrom()) == null) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA- Clearing Account Code Form '%s' no encontrada en el maestro.",
              desglose.getClearingAccountCodeFrom()));
        }
      }
      if (desglose.getClearingAccountCodeTo() != null) {
        if (config.getCuentaCompensacion(desglose.getClearingAccountCodeTo()) == null) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA- Clearing Account Code to '%s' no encontrada en el maestro.",
              desglose.getClearingAccountCodeTo()));
        }

      }

      if (desglose.getClearingMemberCodeFrom() != null) {
        if (config.getCompensador(desglose.getClearingAccountCodeFrom()) == null) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA- Clearing Member from '%s' no encontrada en el maestro.",
              desglose.getClearingAccountCodeFrom()));
        }
      }

      if (desglose.getClearingMemberCodeTo() != null) {
        if (config.getCompensador(desglose.getClearingAccountCodeTo()) == null) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA- Clearing Member to '%s' no encontrada en el maestro.", desglose.getClearingAccountCodeTo()));
        }
      }

      if (solicitud.getAltas().size() > 1
          && (desglose.getTitulos() == null || desglose.getTitulos().compareTo(BigDecimal.ZERO) == 0)
          && (solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.FEWEST || solicitud
              .getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.PROPORTIONAL)) {
        throw new SIBBACBusinessException(
            "INCIDENCIA-Los titulos son obligatorios para el algoritmo FEWEST o PROPORCIONAL al haber mas de un desglose");
      }

      if (desglose.getGiveUpReferenceFrom() != null) {

      }

      if (desglose.getGiveUpReferenceTo() != null) {

      }
      if (solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.FEWEST && desglose.getTitulos() == null) {
        throw new SIBBACBusinessException("INCIDENCIA-Algoritmo FEWEST necesita el numero de titulos.");
      }

      if (solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.PROPORTIONAL
          && desglose.getTitulos() == null) {
        throw new SIBBACBusinessException("INCIDENCIA-Algoritmo PROPORTIONAL necesita el numero de titulos.");
      }

    }
    if (solicitud.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.PROPORTIONAL
        && solicitud.getAltas().size() < 2) {
      throw new SIBBACBusinessException("INCIDENCIA-Algoritmo PROPORTIONAL necesita al menos 2 desgloses.");
    }
    LOG.debug("[validarDatosDesgloseManualInformados] final");

  }

  private void recuperarTitular(Sheet sheet, Row row, Tmct0alo alo, DesgloseManualDTO desgloseManual,
      DesgloseConfigDTO config) throws SIBBACBusinessException {

    BigDecimal fetrade = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alo
        .getFeoperac()));
    String cdholder = getHolder(row);
    if (StringUtils.isNotBlank(cdholder)) {

      List<Holder> listHolders = config.findAllHoldersByIndentificacion(cdholder, fetrade);

      if (CollectionUtils.isEmpty(listHolders)) {
        listHolders = config.findAllHoldersByRazonSocialAndJuridico(cdholder, fetrade);
      }

      if (CollectionUtils.isEmpty(listHolders)) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Holder '%s' de fila '%s' no encontrado en el maestro de holders.", cdholder, row.getRowNum()));
      }
      else if (listHolders.size() > 1) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Holder '%s' de fila '%s' esta repetido en el maestro de holders.", cdholder, row.getRowNum()));
      }
      else {
        Holder holder = listHolders.get(0);
        holder.setTpproced(HolderTipoOrigenDatosType.EXCEL);

        Character holderType = getHolderType(row);
        if (holderType != null) {
          holder.setTptiprep(holderType);
        }
        else {
          LOG.warn("[recuperarDesglosesYtitulares] holder type no informado para holder: {}. Fila: {}.", cdholder,
              row.getRowNum());
          holder.setTptiprep('T');
        }
        desgloseManual.getSettlementData().getListHolder().add(holder);
        desgloseManual.getSettlementData().setDataTFI(true);
      }
    }

  }

  private void setAlgoritmoDesglose(SolicitudDesgloseManual desgloseManual, Row row) throws SIBBACBusinessException {
    Character algoritmoDesglose = getAlgoritmoDesglose(row);
    switch (algoritmoDesglose) {
      case 'E':// precio
        desgloseManual.setAlgoritmoDesgloseManual(AlgoritmoDesgloseManual.PROPORTIONAL);
        break;
      case 'P':// precio
        desgloseManual.setAlgoritmoDesgloseManual(AlgoritmoDesgloseManual.PRICE);
        break;
      case 'C':// asignacion
        desgloseManual.setAlgoritmoDesgloseManual(AlgoritmoDesgloseManual.CLEARING_INFORMATION);
        break;
      default:
        desgloseManual.setAlgoritmoDesgloseManual(AlgoritmoDesgloseManual.FEWEST);
        break;
    }
  }

  private Tmct0alo getAlo(Row row) throws IllegalStateException, NumberFormatException, SIBBACBusinessException {
    LOG.debug("[getAlo] inicio.");
    List<Tmct0alo> listAlos;
    String id = getId(row);
    LOG.debug("[getAlo] buscando allo Id: {}.", id);
    if (id.contains(",")) {
      String[] idSplited = id.split(",");
      if (idSplited.length == 3) {
        Tmct0aloId aloId = new Tmct0aloId(idSplited[2], idSplited[1], idSplited[0]);
        return aloBo.findById(aloId);
      }
      else if (idSplited.length == 2) {
        listAlos = aloBo.findAllByNuordenAndNbooking(idSplited[0], idSplited[1]);
        listAlos = this.getTmct0alosAlta(listAlos);
        if (listAlos.size() == 1) {
          return listAlos.get(0);
        }
        else {
          return getTmct0aloTitulos(listAlos, row);
        }
      }
      else if (idSplited.length == 1) {
        listAlos = aloBo.findAllByNuorden(idSplited[0]);
        listAlos = this.getTmct0alosAlta(listAlos);
        if (listAlos.size() == 1) {
          return listAlos.get(0);
        }
        else {
          return getTmct0aloTitulos(listAlos, row);
        }
      }
      else {
        throw new SIBBACBusinessException(MessageFormat.format("INCIDENCIA-Formato campo id {0} invalido", id));
      }
    }
    else if (id.matches(tramaPattern)) {
      listAlos = aloBo.findAllByNbooking(id);
      listAlos = this.getTmct0alosAlta(listAlos);
      if (listAlos.size() == 1) {
        return listAlos.get(0);
      }
      else {
        return getTmct0aloTitulos(listAlos, row);
      }
    }
    else if (id.matches(camaPattern)) {
      listAlos = aloBo.findAllByNucnfclt(id);
      listAlos = this.getTmct0alosAlta(listAlos);
      if (listAlos.size() == 1) {
        return listAlos.get(0);
      }
      else {
        return getTmct0aloTitulos(listAlos, row);
      }
    }
    else if (id.matches(nurefordPattern)) {
      listAlos = aloBo.findAllByRefCliente(id);
      listAlos = this.getTmct0alosAlta(listAlos);
      if (listAlos.isEmpty()) {
        listAlos = this.getTmct0alosByTmct0ordNureford(id);
        listAlos = this.getTmct0alosAlta(listAlos);
      }
      if (listAlos.size() == 1) {
        return listAlos.get(0);
      }
      else {
        return getTmct0aloTitulos(listAlos, row);
      }

    }
    else {
      throw new SIBBACBusinessException(MessageFormat.format("INCIDENCIA-Formato campo id {0} invalido", id));
    }
  }

  private List<Tmct0alo> getTmct0alosByTmct0ordNureford(String nureford) {
    List<Tmct0alo> res = new ArrayList<Tmct0alo>();
    Tmct0ord ord = ordBo.findByNureford(nureford);
    if (ord != null) {
      res = aloBo.findAllByNuorden(ord.getNuorden());
    }
    return res;
  }

  private List<Tmct0alo> getTmct0alosAlta(List<Tmct0alo> listAlos) {

    List<Tmct0alo> res = new ArrayList<Tmct0alo>();
    for (Tmct0alo tmct0alo : listAlos) {
      if (tmct0alo.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        res.add(tmct0alo);
      }
    }
    return res;
  }

  private Tmct0alo getTmct0aloTitulos(List<Tmct0alo> listAlos, Row row) throws SIBBACBusinessException {
    Tmct0alo alo = null;
    BigDecimal titulos = getTitulos(row);
    if (titulos == null) {
      throw new SIBBACBusinessException(
          String
              .format(
                  "INCIDENCIA-No es posible identificar un solo Allocation con el id: '%s' sin informar los titulos. Fila: '%s'.",
                  getId(row), row.getRowNum()));
    }
    if (CollectionUtils.isEmpty(listAlos)) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Allocation no encontrado id: '%s'. Fila: '%s' ",
          getId(row), row.getRowNum()));
    }
    else if (listAlos.size() > 1) {
      alo = null;
      for (Tmct0alo alof : listAlos) {
        if (alof.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
            && alof.getNutitcli().compareTo(titulos) == 0) {
          if (alo != null) {
            throw new SIBBACBusinessException(String.format(
                "INCIDENCIA-Hay mas de un Allocation con el campo id: '%s' y titulos: '%s'. Fila: '%s'", getId(row),
                titulos, row.getRowNum()));
          }
          alo = alof;
        }
      }
    }
    return alo;
  }

  private Character getRowType(Row row) throws SIBBACBusinessException {
    try {
      LOG.trace("[getRowType] buscando ROW TYPE llegado de la excel... Fila: {}", row.getRowNum());
      Cell cell = row.getCell(posRowType);
      return getCharacter(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando ROW TYPE. Fila: %s", row.getRowNum()));
    }
  }

  private String getId(Row row) throws SIBBACBusinessException {
    LOG.trace("[getId] buscando id llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posId);
      return getText(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando ROW ID. Fila: %s", row.getRowNum()));
    }
  }

  private BigDecimal getTitulos(Row row) throws SIBBACBusinessException {
    LOG.trace("[getTitulos] buscando titulos llegados de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posTitulos);
      return getNumeric(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando QUANTITY. Fila: %s", row.getRowNum()));
    }
  }

  private Character getAlgoritmoDesglose(Row row) throws SIBBACBusinessException {
    LOG.trace("[getAlgoritmoDesglose] buscando ALGORITMO DESGLOSE llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posAlgoritmo);
      return getCharacter(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando ALGORITM. Fila: %s", row.getRowNum()));
    }
  }

  private String getHolder(Row row) throws SIBBACBusinessException {
    LOG.trace("[getHolder] buscando HOLDER llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posHolder);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (value != null && value.length() > 40) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando HOLDER. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(), value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando HOLDER. Fila: %s", row.getRowNum()));
    }
  }

  private Character getHolderType(Row row) throws SIBBACBusinessException {
    LOG.trace("[getHolderType] buscando HOLDER TYPE llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posHolderType);
      return getCharacter(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando HOLDER TYPE. Fila: %s", row.getRowNum()));
    }
  }

  private String getBankReference(Row row) throws SIBBACBusinessException {
    LOG.trace("[getBankReference] buscando BANK REFERENCE llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posBankReference);
      String value = getText(cell);
      value = removeSpecialCharaters(value);
      if (value != null && value.length() > 20) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando BANK REFERENCE. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(), value));
      }
      return value;

    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando BANK REFERENCE. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getAccountAtClearer(Row row) throws SIBBACBusinessException {
    LOG.trace("[getAccountToClearer] buscando ACCOUNT AT CLEARER llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posAccountAtClearer);
      String value = getText(cell);
      value = removeSpecialCharaters(value);
      if (value != null && value.length() > 35) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando ACCOUNT AT CLEARER. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(), value));

      }
      return value;

    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando ACCOUNT AT CLEARER. Fila: %s",
          row.getRowNum()));
    }
  }

  private Character getPositionBreakDown(Row row) throws SIBBACBusinessException {
    LOG.trace("[getPositionBreakDown] buscando POSITION BREAK DOWN llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posPositionBreakDown);
      return getCharacter(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando POSITION BREAK DOWN. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getClearer(Row row) throws SIBBACBusinessException {
    LOG.trace("[getClearer] buscando CLEARER llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posClearer);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);

      if (StringUtils.isNotBlank(value)) {
        value = StringUtils.leftPad(value, 4, "0");
        if (value.length() > 4) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA-Recuperando CLEARER. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(), value));
        }
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CLEARER. Fila: %s", row.getRowNum()));
    }
  }

  private BigDecimal getBankComissionPercent(Row row) throws SIBBACBusinessException {
    LOG.trace("[getBankComissionPercent] buscando BANK COMISSION PERCENT llegado de la excel... Fila: {}",
        row.getRowNum());
    try {
      Cell cell = row.getCell(posBankComissionPercent);
      return getNumeric(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando BANK COMISSION PERCENT. Fila: %s",
          row.getRowNum()));
    }
  }

  private BigDecimal getPayerComissionPercent(Row row) throws SIBBACBusinessException {
    LOG.trace("[getPayerComissionPercent] buscando payer COMISSION PERCENT llegado de la excel... Fila: {}",
        row.getRowNum());
    try {
      Cell cell = row.getCell(posPayerComissionPercent);
      return getNumeric(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando PAYER COMMISION PERCENT. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getCustodian(Row row) throws SIBBACBusinessException {
    LOG.trace("[getCustodian] buscando CUSTODIAN llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posCustodian);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (StringUtils.isNotBlank(value)) {
        value = StringUtils.leftPad(value, 4, "0");
        if (value.length() > 4) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA-Recuperando CUSTODIAN. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(), value));
        }
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CUSTODIAN. Fila: %s", row.getRowNum()));
    }
  }

  private Long getClearingRuleTo(Row row) throws SIBBACBusinessException {
    LOG.trace("[getClearingRuleTo] buscando payer CLEARING RULE TO llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posClearingRuleTo);
      BigDecimal val = getNumeric(cell);
      if (val != null) {
        return val.longValue();
      }
      return null;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CLEARING RULE TO. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getClearingMemberTo(Row row) throws SIBBACBusinessException {
    LOG.trace("[getClearingMemberTo] buscando CLEARING MEMBER TO llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posClearingMemberTo);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (value != null && value.length() > 4) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando CLEARING MEMBER TO. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(), value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CLEARING MEMBER TO. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getGiveUpReferenceTo(Row row) throws SIBBACBusinessException {
    LOG.trace("[getGiveUpReferenceTo] buscando GIVE-UP REFERENCE TO llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posGiveUpReferenceTo);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (value != null && value.length() > 20) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando GIVE-UP REFERENCE TO. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(),
            value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando GIVE UP REFERENDCE TO. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getClearingAccountCodeTo(Row row) throws SIBBACBusinessException {
    LOG.trace("[getClearingAccountCodeTo] buscando CLEARING ACCOUNT CODE TO llegado de la excel... Fila: {}",
        row.getRowNum());
    try {
      Cell cell = row.getCell(posClearingAccountCodeTo);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (value != null && value.length() > 3) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando CLEARING ACCOUNT CODE TO. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(),
            value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CLEARING ACCOUNT CODE TO. Fila: %s",
          row.getRowNum()));
    }
  }

  private BigDecimal getPriceFrom(Row row) throws SIBBACBusinessException {
    LOG.trace("[getPriceFrom] buscando PRICE FROM llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posPriceFrom);
      return getNumeric(cell);
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando PRICE FORM. Fila: %s", row.getRowNum()));
    }
  }

  private String getClearingAccountCodeFrom(Row row) throws SIBBACBusinessException {
    LOG.trace("[getClearingAccountCodeFrom] buscando CLEARING ACCOUNT CODE FROM llegado de la excel... Fila: {}",
        row.getRowNum());
    try {
      Cell cell = row.getCell(posClearingAccountCodeFrom);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (value != null && value.length() > 3) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando CLEARING ACCOUNT CODE FROM. Fila: %s, valor: '%s' demasiado largo",
            row.getRowNum(), value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CLEARING ACCOUNT FROM. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getClearingMemberFrom(Row row) throws SIBBACBusinessException {
    LOG.trace("[getClearingMemberFrom] buscando CLEARING MEMBER FROM llegado de la excel... Fila: {}", row.getRowNum());
    try {
      Cell cell = row.getCell(posClearingMemberFrom);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);

      if (value != null && value.length() > 4) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando CLEARING MEMBER FROM . Fila: %s, valor: '%s' demasiado largo", row.getRowNum(),
            value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando CLEARING MEMBER FROM. Fila: %s",
          row.getRowNum()));
    }
  }

  private String getGiveUpReferenceFrom(Row row) throws SIBBACBusinessException {
    LOG.trace("[getGiveUpReferenceFrom] buscando GIVE-UP REFERENCE FROM llegado de la excel... Fila: {}",
        row.getRowNum());
    try {
      Cell cell = row.getCell(posGiveUpReferenceFrom);
      String value = getText(cell);
      value = this.removeSpecialCharaters(value);
      if (value != null && value.length() > 20) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Recuperando GIVE-UP REFERENCE FROM. Fila: %s, valor: '%s' demasiado largo", row.getRowNum(),
            value));
      }
      return value;
    }
    catch (IllegalStateException | NumberFormatException e) {
      throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando GIVE UP REFERENCE FROM. Fila: %s",
          row.getRowNum()));
    }
  }

  private Character getSentClearingComissionMethod(Row row) throws SIBBACBusinessException {
    LOG.trace(
        "[getSentClearingComissionMethod] buscando SENT CLEARING COMMISSION METHOD llegado de la excel... Fila: {}",
        row.getRowNum());
    try {
      Cell cell = row.getCell(posSentClearingCommissionMethod);
      return getCharacter(cell);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA-Recuperando SENT CLEARING COMMISION METHOD. Fila: %s", row.getRowNum()));
    }
  }

  private boolean isAloRow(Row row) throws SIBBACBusinessException {
    if (row == null) {
      return false;
    }
    else {
      LOG.trace("[isAloRow]  Fila: {}", row.getRowNum());

      Character rowType = getRowType(row);
      return rowType != null && rowType == 'A';
    }

  }

  private boolean isDesgloseRow(Row row) throws SIBBACBusinessException {
    if (row == null) {
      return false;
    }
    else {
      LOG.trace("[isDesgloseRow]  Fila: {}", row.getRowNum());

      Character rowType = getRowType(row);
      return rowType != null && rowType == 'D';
    }
  }

  private boolean isHolderRow(Row row) throws SIBBACBusinessException {

    if (row == null) {
      return false;
    }
    else {
      LOG.trace("[isHolderRow]  Fila: {}", row.getRowNum());
      Character rowType = getRowType(row);
      return rowType != null && rowType == 'H';
    }
  }

  private String removeSpecialCharaters(String value) {
    // [^a-zA-Z0-9]+
    if (value != null) {
      return value.replaceAll("\\W", "").trim();
    }
    return null;
  }

  private BigDecimal getNumeric(Cell cell) {
    if (cell != null) {
      if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
        return new BigDecimal(cell.getNumericCellValue());
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
        return new BigDecimal(cell.getStringCellValue());
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {

      }
    }
    return null;
  }

  private String getText(Cell cell) {
    if (cell != null) {
      if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
        return new BigDecimal(cell.getNumericCellValue()).setScale(0, RoundingMode.HALF_DOWN).toString();
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
        return cell.getStringCellValue();
      }
      else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
        return "";
      }
    }
    return null;
  }

  private Character getCharacter(Cell cell) {
    String value = getText(cell);
    if (StringUtils.isNotBlank(value)) {
      return value.charAt(0);
    }
    return null;
  }
}
