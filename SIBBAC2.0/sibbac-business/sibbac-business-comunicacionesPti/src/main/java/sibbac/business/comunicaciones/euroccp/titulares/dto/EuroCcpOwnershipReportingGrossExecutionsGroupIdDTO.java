package sibbac.business.comunicaciones.euroccp.titulares.dto;

import java.util.Date;

import sibbac.business.wrappers.database.model.RecordBean;

public class EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 2093612491719665267L;

  private final Date fecontratacion;
  private final String criterioComunicacionTitular;
  private final String cdmiembromkt;

  public EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO(Date fecontratacion,
                                                     String criterioComunicacionTitular,
                                                     String cdmiembromkt) {
    super();
    this.fecontratacion = fecontratacion;
    this.criterioComunicacionTitular = criterioComunicacionTitular;
    this.cdmiembromkt = cdmiembromkt;
  }

  public Date getFecontratacion() {
    return fecontratacion;
  }

  public String getCriterioComunicacionTitular() {
    return criterioComunicacionTitular;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }
  
  

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdmiembromkt == null) ? 0 : cdmiembromkt.hashCode());
    result = prime * result + ((criterioComunicacionTitular == null) ? 0 : criterioComunicacionTitular.hashCode());
    result = prime * result + ((fecontratacion == null) ? 0 : fecontratacion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO other = (EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO) obj;
    if (cdmiembromkt == null) {
      if (other.cdmiembromkt != null)
        return false;
    } else if (!cdmiembromkt.equals(other.cdmiembromkt))
      return false;
    if (criterioComunicacionTitular == null) {
      if (other.criterioComunicacionTitular != null)
        return false;
    } else if (!criterioComunicacionTitular.equals(other.criterioComunicacionTitular))
      return false;
    if (fecontratacion == null) {
      if (other.fecontratacion != null)
        return false;
    } else if (!fecontratacion.equals(other.fecontratacion))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "EuroCcpOwnershipReportingGrossExecutionsGroupIdDTO [fecontratacion=" + fecontratacion
           + ", criterioComunicacionTitular=" + criterioComunicacionTitular + ", cdmiembromkt=" + cdmiembromkt + "]";
  }

}
