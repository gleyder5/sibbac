package sibbac.business.comunicaciones.euroccp.ciffile.mapper;

import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpAggregatesNetSettlementsFields;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpAggregatesNetSettlements;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpAggregatesNetSettlementsFieldSetMapper extends
                                                          WrapperAbstractFieldSetMapper<EuroCcpLineaFicheroRecordBean> {

  public EuroCcpAggregatesNetSettlementsFieldSetMapper() {
    super();
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpAggregatesNetSettlementsFields.values();
  }

  @Override
  public EuroCcpLineaFicheroRecordBean getNewInstance() {

    return new EuroCcpAggregatesNetSettlements();
  }

}
