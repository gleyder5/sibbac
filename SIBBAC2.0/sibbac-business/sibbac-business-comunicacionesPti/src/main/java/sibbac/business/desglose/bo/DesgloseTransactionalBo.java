package sibbac.business.desglose.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.SettlementChainDataException;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class DesgloseTransactionalBo {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(DesgloseTransactionalBo.class);

  @Autowired
  private DesgloseBo desgloseBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0logBo logBo;

  public DesgloseTransactionalBo() {
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void procesoDesgloseNacionalAutomatico(Tmct0aloId id, DesgloseConfigDTO config) throws SIBBACBusinessException {
    try {
      desgloseBo.desgloseNacionalAutomatico(id, config);
    }
    catch (SIBBACBusinessException e) {
      LOG.warn("[procesoDesgloseNacionalAutomatico] {} - {}", id, e.getMessage(), e);
      aloBo.ponerEstadoAsignacionPorExcepcion(id, EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSE_MANUAL.getId(),
          e.getMessage(), config.getAuditUser());
    }
    catch (SettlementChainDataException e) {
      LOG.warn("[procesoDesgloseNacionalAutomatico] {} - {}", id, e.getMessage(), e);
      aloBo.ponerEstadoAsignacionPorExcepcion(id, EstadosEnumerados.ASIGNACIONES.SIN_INSTRUCCIONES.getId(),
          e.getMessage(), config.getAuditUser());
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("%s - %s", id, e.getMessage()), e);
    }
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public CallableResultDTO procesoDesgloseNacionalManual(SolicitudDesgloseManual solicitud, DesgloseConfigDTO config) {
    CallableResultDTO res = new CallableResultDTO();
    Tmct0aloId id = solicitud.getTmct0aloId();
    try {
      desgloseBo.desgloseNacionalManual(solicitud, config);
      res.addMessage(String.format("[%s/%s/%s] - Desglosado Ok.", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim()));
    }
    catch (SIBBACBusinessException e) {
      LOG.warn("[procesoDesgloseNacionalManual] [{}/{}/{}] - {}", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim(), e.getMessage(), e);
      res.addErrorMessage(String.format("[%s/%s/%s] - %s", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim(), e.getMessage()));
      logBo.insertarRegistroNewTransaction(id.getNuorden(), id.getNbooking(), id.getNucnfclt(), e.getMessage(), "",
          config.getAuditUser());
    }
    catch (SettlementChainDataException e) {
      LOG.warn("[procesoDesgloseNacionalManual] [{}/{}/{}] - {}", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim(), e.getMessage(), e);
      res.addErrorMessage(String.format("[%s-%s-%s] - %s", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim(), e.getMessage()));
      logBo.insertarRegistroNewTransaction(id.getNuorden(), id.getNbooking(), id.getNucnfclt(), e.getMessage(), "",
          config.getAuditUser());

    }
    catch (Exception e) {
      LOG.warn("[procesoDesgloseNacionalManual] [{}/{}/{}] - {}", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim(), e.getMessage(), e);
      res.addErrorMessage(String.format("[%s/%s/%s] - %s", id.getNuorden().trim(), id.getNbooking().trim(), id
          .getNucnfclt().trim(), e.getMessage()));
      logBo.insertarRegistroNewTransaction(id.getNuorden(), id.getNbooking(), id.getNucnfclt(), e.getMessage(), "",
          config.getAuditUser());
    }
    return res;
  }

}
