package sibbac.business.comunicaciones.euroccp.realigment.bo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpFileSentBo;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionsBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class EuroCcpExecutionRealigmentSendFilesBo extends WrapperExecutor {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpExecutionRealigmentSendFilesBo.class);

  @Value("${sibbac.euroccp.realigment.erg.in.line.pattern}")
  private String realigmentLinePattern;

  @Value("${sibbac.euroccp.realigment.erg.in.footer.pattern}")
  private String footerLinePattern;

  @Value("${sibbac.euroccp.realigment.erg.file.date.pattern}")
  private String fileDatePattern;

  @Value("${sibbac.euroccp.realigment.erg.txt.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.euroccp.realigment.erg.txt.pool.size}")
  private int threadSize;

  @Value("${sibbac.euroccp.realigment.erg.file.charset}")
  private Charset fileCharset;

  @Value("${sibbac.euroccp.out.path}")
  private String workPathOutSt;

  @Value("${sibbac.euroccp.out.tmp.folder}")
  private String tmpFolder;

  @Value("${sibbac.euroccp.out.pendientes.folder}")
  private String pendientesFolder;

  @Value("${sibbac.euroccp.out.enviados.folder}")
  private String enviadosFolder;

  @Value("${sibbac.euroccp.out.tratados.folder}")
  private String tratadosFolder;

  @Value("${sibbac.euroccp.realigment.erg.txt.file.name}")
  private String tmpFileName;

  @Value("${sibbac.euroccp.realigment.erg.file.name.client.number.last.positions}")
  private int clientNumberLastPositions;

  @Value("${sibbac.euroccp.realigment.erg.file.send.first.sd.date}")
  private int dDaysFirstSendDate;

  @Value("${sibbac.euroccp.realigment.erg.file.send.first.sd.date.init.hour}")
  private String initSendHourFirstDate;

  @Value("${sibbac.euroccp.realigment.erg.file.send.last.sd.date}")
  private int dDaysLastDate;

  @Value("${sibbac.euroccp.realigment.erg.file.send.last.sd.date.end.hour}")
  private String endSendHourLastDate;

  @Value("${sibbac.euroccp.realigment.erg.file.send.mail.compress.extension}")
  private String excelSendMailCompressExtension;

  @Value("${sibbac.euroccp.realigment.erg.file.send.by.mail}")
  private String principalFileSentMail;

  @Value("${sibbac.euroccp.realigment.erg.file.send.to}")
  private List<String> to;

  @Value("${sibbac.euroccp.realigment.erg.file.send.cc}")
  private List<String> cc;

  @Value("${sibbac.euroccp.realigment.erg.file.send.subjet}")
  private String subject;

  @Value("${sibbac.euroccp.realigment.erg.file.send.body}")
  private String body;

  @Value("${sibbac.euroccp.realigment.erg.file.send.timezone}")
  private TimeZone timeZone;

  @Value("${sibbac.euroccp.in.path}")
  private String workPathInSt;

  @Value("${sibbac.euroccp.realigment.erg.in.file.pattern}")
  private String inFilePattern;

  @Autowired
  private EuroCcpFileSentBo fileSentBo;

  @Autowired
  private EuroCcpExecutionRealigmentFileWriterBo writer;

  @Autowired
  private EuroCcpExecutionRealigmentBo executionRealigmentBo;

  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsBo euroCcpOwnershipReportingGrossExecutionsBo;

  public EuroCcpExecutionRealigmentSendFilesBo() {
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void enviarFicheros() throws SIBBACBusinessException {
    List<Object[]> listDistinctClientNumberProcessingAndTransactionDate = executionRealigmentBo
        .findAllClientNumberByEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());
    Path tmpPath = Paths.get(workPathOutSt, tmpFolder);

    if (Files.notExists(tmpPath)) {
      try {
        Files.createDirectories(tmpPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException(
            String.format("No se ha podido crear el directorio: '%s'", tmpPath.toString()), e);
      }
    }

    for (Object[] o : listDistinctClientNumberProcessingAndTransactionDate) {
      this.crearFicheroErg((String) o[0], (Date) o[1], (Date) o[2], tmpPath);
    }

    if (principalFileSentMail != null && "S".equalsIgnoreCase(principalFileSentMail)) {
      writer.enviarFilesByEmail(workPathOutSt, tmpFolder, pendientesFolder, enviadosFolder, tratadosFolder, to, cc,
          subject, body);
    }
    else {
      writer.moverRutaEnvio(workPathOutSt);
    }
  }

  private void crearFicheroErg(String clientNumber, Date procesingDate, Date transactionDate, Path tmpPath)
      throws SIBBACBusinessException {
    int pos = 0;
    int posEnd = 0;
    int tam = getTransactionSize();
    List<EuroCcpIdDTO> beans = executionRealigmentBo.findAllEuroCcpIdDTOByEstadoProcesadoAndClientNumber(
        ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId(), clientNumber, procesingDate, transactionDate);
    if (CollectionUtils.isNotEmpty(beans)) {
      fileSentBo.saveNewFileRegisterIfNotExistsWithoutSentNewTransacction(tmpFileName, clientNumber,
          clientNumberLastPositions, fileDatePattern, EuroCcpFileTypeEnum.ERG);

      List<FutureTask<CallableResultDTO>> futures = new ArrayList<FutureTask<CallableResultDTO>>();
      ExecutorService executor = getNewExecutor();
      try {
        while (posEnd < beans.size()) {

          posEnd = Math.min(posEnd + tam, beans.size());

          FutureTask<CallableResultDTO> task = new FutureTask<CallableResultDTO>(
              getNewCallableProcessCreacionFicheroTmp(beans.subList(pos, posEnd), tmpPath, tmpFileName,
                  clientNumberLastPositions, fileCharset));
          executor.execute(task);

          futures.add(task);
          pos = posEnd;
        }

        for (FutureTask<CallableResultDTO> future : futures) {
          try {
            CallableResultDTO result = future.get(5, TimeUnit.MINUTES);
            if (result == null && !future.isDone() && !future.isCancelled() && future.cancel(true)) {
              LOG.warn("El fufure ha sido cancelado {}", future);
            }
            else if (result != null) {
            }
          }
          catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw e;
          }

        }

      }
      catch (Exception e) {
        closeExecutorNow(executor);
        throw new SIBBACBusinessException(e);
      }
      finally {
        closeExecutor(executor);
      }

      writer.appendTmpFiles(workPathOutSt, tmpFolder, pendientesFolder, enviadosFolder, tratadosFolder,
          excelSendMailCompressExtension, fileCharset, timeZone);

    }
  }

  private Callable<CallableResultDTO> getNewCallableProcessCreacionFicheroTmp(final List<EuroCcpIdDTO> beans,
      final Path tmpPath, final String fileName, final int lastClientNumberPos, final Charset charset) {
    Callable<CallableResultDTO> c = new Callable<CallableResultDTO>() {

      @Override
      public CallableResultDTO call() throws SIBBACBusinessException {
        CallableResultDTO fin = new CallableResultDTO();
        try {
          writer.creacionFicheroTmp(beans, tmpPath, fileName, lastClientNumberPos, charset);
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          fin.addErrorMessage("Error en el servidor, por favor intente de nuevo");
          throw new SIBBACBusinessException(
              String.format("INCIDENCIA-Creando fichero temporal para beans: '%s'", beans), e);
        }
        return fin;
      }
    };
    return c;
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
