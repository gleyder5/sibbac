package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.TptiInBo;
import sibbac.business.fase0.database.model.TptiIn;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageHeader;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.ac.AC;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.ce.CE;
import sibbac.pti.messages.fs.FS;
import sibbac.pti.messages.mo.MO;
import sibbac.pti.messages.pt.PT;
import sibbac.pti.messages.pv.PV;
import sibbac.pti.messages.rt.RT;
import sibbac.pti.messages.ti.TI;
import sibbac.pti.messages.txt.TXT;
import sibbac.pti.messages.va.VA;

@Service
public class PtiMessageProcessSelectorBo {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessSelectorBo.class);

  @Autowired
  private TptiInBo inBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Qualifier(value = "ptiMessageProcessAcBo")
  @Autowired
  private PtiMessageProcessAcBo processAc;

  @Qualifier(value = "ptiMessageProcessAnBo")
  @Autowired
  private PtiMessageProcessAnBo processAn;

  @Qualifier(value = "ptiMessageProcessCeBo")
  @Autowired
  private PtiMessageProcessCeBo processCe;

  @Qualifier(value = "ptiMessageProcessFsBo")
  @Autowired
  private PtiMessageProcessFsBo processFs;

  @Qualifier(value = "ptiMessageProcessMoBo")
  @Autowired
  private PtiMessageProcessMoBo processMo;

  @Qualifier(value = "ptiMessageProcessRtBo")
  @Autowired
  private PtiMessageProcessRtBo prcessRt;

  @Qualifier(value = "ptiMessageProcessTiBo")
  @Autowired
  private PtiMessageProcessTiBo processTi;

  @Qualifier(value = "ptiMessageProcessVaBo")
  @Autowired
  private PtiMessageProcessVaBo processVa;

  @Qualifier(value = "ptiMessageProcessPvBo")
  @Autowired
  private PtiMessageProcessPvBo processPv;

  @Qualifier(value = "ptiMessageProcessTxtBo")
  @Autowired
  private PtiMessageProcessTxtBo processTxt;

  public PtiMessageProcessSelectorBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void parseAndProcessSelectorPtiMessage(Long id, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[parseAndProcessSelectorPtiMessage] inicio.");
    TptiIn in = inBo.findById(id);
    in.setProcessed('S');
    in.setWithoutError('S');
    in.setAudtUser("SIBBAC20");
    PTIMessage ptiMessage;
    PTIMessageHeader head;
    PTIMessageVersion version;
    String strMessage = in.getMessage();
    LOG.trace("[parseAndProcessSelectorPtiMessage] procesando msg: {}", strMessage);
    try {
      head = PTIMessageFactory.parseHeader(strMessage);
      LOG.trace("[parseAndProcessSelectorPtiMessage] head: {}", head);
      version = cache.getPtiMessageVersionFromTmct0cfg(head.getFecha());
      try {
        ptiMessage = PTIMessageFactory.parse(version, strMessage);
      }
      catch (Exception e) {
        if (version == PTIMessageVersion.VERSION_1) {
          ptiMessage = PTIMessageFactory.parse(PTIMessageVersion.VERSION_T2S, strMessage);
          version = PTIMessageVersion.VERSION_T2S;
        }
        else if (version == PTIMessageVersion.VERSION_T2S) {
          ptiMessage = PTIMessageFactory.parse(PTIMessageVersion.VERSION_1, strMessage);
          version = PTIMessageVersion.VERSION_1;
        }
        else {
          throw e;
        }
      }

      LOG.trace("[parseAndProcessSelectorPtiMessage] ptiMessage: {}", ptiMessage);

      switch (ptiMessage.getTipo().name()) {
        case "AC":
          this.procesarAC(version, (AC) ptiMessage, cache);
          break;
        case "AN":
          this.procesarAN(version, (AN) ptiMessage, cache);
          break;
        case "CE":
          this.procesarCE(version, (CE) ptiMessage, cache);
          break;
        case "FS":
          this.procesarFS(version, (FS) ptiMessage, cache);
          break;
        // case "GA01":
        // case "GA02":
        // case "GA03":
        // case "GA04":
        // LOG.warn("[parseAndProcessSelectorPtiMessage] LOS MSG GAXX NO SE PROCESAN.\n{}",
        // ptiMessage);
        // break;
        case "MO":
          this.procesarMO(version, (MO) ptiMessage, cache);
          break;
        case "PT":
          this.procesarPT(version, (PT) ptiMessage, cache);
          break;
        case "PV":
          this.procesarPV(version, (PV) ptiMessage, cache);
          break;
        case "RT":
          this.procesarRT(version, (RT) ptiMessage, cache);
          break;
        case "TI":
          this.procesarTI(version, (TI) ptiMessage, cache);
          break;
        case "TXT":
          this.procesarTXT(version, (TXT) ptiMessage, cache);
          break;
        case "VA":
          this.procesarVA(version, (VA) ptiMessage, cache);
          break;
        default:
          throw new SIBBACBusinessException(String.format("INCIDENCIA-Tipo de mensaje no soportado: %s", strMessage));
      }
    }
    catch (Exception e) {
      LOG.warn("INCIDENCIA-Procesando PtiMessage: '{}' - '{}'", e.getMessage(), strMessage, e);
      inBo.crearError(e, in, "SIBBAC20");
      in.setWithoutError('N');
    }

    try {
      in.setAudtFechaCambio(new Date());
      in = inBo.save(in);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA-Actualizando estado procesado de PtiMessage Err: '%s' msg: '%s' ", e.getMessage(), strMessage), e);
    }
    LOG.debug(
        "[parseAndProcessSelectorPtiMessage] procesado id: {} ## MsgType: {} ## MessageDate: {} ## Processed: {} ## WithOutErr: {}.",
        in.getId(), in.getMessageType(), in.getMessageDate(), in.getProcessed(), in.getWithoutError());
    LOG.debug("[parseAndProcessSelectorPtiMessage] fin.");
  }

  // @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation =
  // Propagation.REQUIRES_NEW)
  // public void parseAndProcessSelectorPtiMessage(List<Long> ids, String
  // version, Tmct0xasAndTmct0cfgConfig cache) {
  // LOG.debug("[parseAndProcessSelectorPtiMessage] inicio.");
  // for (Long id : ids) {
  // this.parseAndProcessSelectorPtiMessage(id, version, cache);
  // }
  //
  // LOG.debug("[parseAndProcessSelectorPtiMessage] fin.");
  // }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarAC(PTIMessageVersion version, AC ac, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarAC] inicio.");
    processAc.process(version, ac, cache);
    LOG.debug("[procesarAC] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarAN(PTIMessageVersion version, AN an, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarAN] inicio.");
    processAn.process(version, an, cache);
    LOG.debug("[procesarAN] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarCE(PTIMessageVersion version, CE ce, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarCE] inicio.");
    processCe.process(version, ce, cache);
    LOG.debug("[procesarCE] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarFS(PTIMessageVersion version, FS fs, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarFS] inicio.");
    processFs.process(version, fs, cache);
    LOG.debug("[procesarFS] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarMO(PTIMessageVersion version, MO mo, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarMO] inicio.");
    processMo.process(version, mo, cache);
    LOG.debug("[procesarMO] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarPT(PTIMessageVersion version, PT pt, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarPT] inicio.");

    LOG.debug("[procesarPT] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarPV(PTIMessageVersion version, PV pv, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarPV] inicio.");
    processPv.process(version, pv, cache);
    LOG.debug("[procesarPV] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarRT(PTIMessageVersion version, RT rt, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarRT] inicio.");
    prcessRt.process(version, rt, cache);
    LOG.debug("[procesarRT] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarTI(PTIMessageVersion version, TI ti, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarTI] inicio.");
    processTi.process(version, ti, cache);
    LOG.debug("[procesarTI] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarTXT(PTIMessageVersion version, TXT txt, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarTXT] inicio.");
    processTxt.process(version, txt, cache);
    LOG.debug("[procesarTXT] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarVA(PTIMessageVersion version, VA va, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    LOG.debug("[procesarVA] inicio.");
    processVa.process(version, va, cache);
    LOG.debug("[procesarVA] fin.");
  }
}