package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.RecordBean;

public class CuadreEccDbReaderRecordBeanDTO extends RecordBean implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6799539401300026154L;
  private Tmct0aloId tmct0aloId;

  public CuadreEccDbReaderRecordBeanDTO(Tmct0aloId aloId) {
    this.tmct0aloId = aloId;
  }

  public CuadreEccDbReaderRecordBeanDTO(String nuorden, String nbooking, String nucnfclt) {
    this.tmct0aloId = new Tmct0aloId(nucnfclt, nbooking, nuorden);
  }

  public Tmct0aloId getTmct0aloId() {
    return tmct0aloId;
  }

  public void setTmct0aloId(Tmct0aloId tmct0aloId) {
    this.tmct0aloId = tmct0aloId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((tmct0aloId == null) ? 0 : tmct0aloId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CuadreEccDbReaderRecordBeanDTO other = (CuadreEccDbReaderRecordBeanDTO) obj;
    if (tmct0aloId == null) {
      if (other.tmct0aloId != null)
        return false;
    } else if (!tmct0aloId.equals(other.tmct0aloId))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "RevisarDesglosesIFDTO [tmct0aloId=" + tmct0aloId + "]";
  }

}
