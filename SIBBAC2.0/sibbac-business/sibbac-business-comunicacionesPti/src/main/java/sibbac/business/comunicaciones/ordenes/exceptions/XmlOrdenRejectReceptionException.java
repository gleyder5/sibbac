package sibbac.business.comunicaciones.ordenes.exceptions;

public class XmlOrdenRejectReceptionException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 2665170754605343505L;

  public XmlOrdenRejectReceptionException() {
    // TODO Auto-generated constructor stub
  }

  public XmlOrdenRejectReceptionException(String arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  public XmlOrdenRejectReceptionException(Throwable arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  public XmlOrdenRejectReceptionException(String arg0, Throwable arg1) {
    super(arg0, arg1);
    // TODO Auto-generated constructor stub
  }

  public XmlOrdenRejectReceptionException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
    // TODO Auto-generated constructor stub
  }

}
