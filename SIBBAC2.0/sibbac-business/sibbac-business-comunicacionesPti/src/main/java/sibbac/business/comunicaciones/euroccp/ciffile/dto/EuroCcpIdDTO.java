package sibbac.business.comunicaciones.euroccp.ciffile.dto;

import sibbac.business.wrappers.database.model.RecordBean;

public class EuroCcpIdDTO extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -2165504575197163223L;

  public EuroCcpIdDTO(long id) {
    setId(id);
  }

}
