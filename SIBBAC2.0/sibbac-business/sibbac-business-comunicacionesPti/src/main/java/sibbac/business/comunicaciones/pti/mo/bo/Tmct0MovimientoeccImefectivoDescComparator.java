package sibbac.business.comunicaciones.pti.mo.bo;

import java.util.Comparator;

import sibbac.business.wrappers.database.model.Tmct0movimientoecc;

public class Tmct0MovimientoeccImefectivoDescComparator implements Comparator<Tmct0movimientoecc> {

  @Override
  public int compare(Tmct0movimientoecc arg0, Tmct0movimientoecc arg1) {
    return arg0.getImefectivo().compareTo(arg1.getImefectivo()) * -1;
  }

}
