package sibbac.business.comunicaciones.euroccp.titulares.runnable;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionsBo;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpOwnershipReportingGrossExecutionsReceiveRunnable")
public class EuroCcpOwnershipReportingGrossExecutionsReceiveRunnable extends IRunnableBean<EuroCcpIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory
      .getLogger(EuroCcpOwnershipReportingGrossExecutionsReceiveRunnable.class);

  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsBo euroCcpOwnershipReportingGrossExecutionsBo;

  public EuroCcpOwnershipReportingGrossExecutionsReceiveRunnable() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<EuroCcpIdDTO> beans, int order) throws SIBBACBusinessException {

    if (CollectionUtils.isNotEmpty(beans)) {
      try {
        euroCcpOwnershipReportingGrossExecutionsBo.revisarEstadosTitulares(beans);
      }
      catch (NullPointerException e) {
        getObserver().setException(e);
        LOG.debug(e.getMessage(), e);
      }
      finally {
        getObserver().sutDownThread(this);
      }

    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<EuroCcpIdDTO> clone() {
    return this;
  }

}
