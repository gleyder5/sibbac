package sibbac.business.comunicaciones.euroccp.titulares.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;

@Component
public interface EuroCcpFileTypeDao extends JpaRepository<EuroCcpFileType, Long> {

  EuroCcpFileType findByCdCodigo(String cdcodigo);

  EuroCcpFileType findByDescription(String description);

}
