package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;

import com.sv.sibbac.multicenter.AllocationID;
import com.sv.sibbac.multicenter.ExecutionGroupType;
import com.sv.sibbac.multicenter.ExecutionID;

@Service
public class XmlGrupoEjecucion {

  protected static final Logger LOG = LoggerFactory.getLogger(XmlGrupoEjecucion.class);

  @Autowired
  private Tmct0xasBo conversiones;

  public XmlGrupoEjecucion() {

  }

  public void xml_mapping(Tmct0grupoejecucion jdoObject, ExecutionGroupType executionsGroup, AllocationID allocationId,
      Map<String, Tmct0eje> mExecution, CachedValuesLoadXmlOrdenDTO cachedConfig) throws XmlOrdenExceptionReception {
    XmlEjecucionAlocation xmlTmct0gal = new XmlEjecucionAlocation();
    Tmct0ejecucionalocation ejeAlo;

    jdoObject.setFevalor(new Date(executionsGroup.getFevalor().toGregorianCalendar().getTimeInMillis()));

    jdoObject.setCdtipoop(executionsGroup.getTpopebol());
    jdoObject.setImpreciomkt(new BigDecimal(executionsGroup.getPrecioBrutoMkt()).setScale(8, RoundingMode.HALF_UP));
    jdoObject.setNuagreje(executionsGroup.getNuagreje());
    jdoObject.setAsigcomp(executionsGroup.isAsignadoComp() ? 'S' : 'N');
    jdoObject.setTradingvenue(executionsGroup.getTradingVenue());
    jdoObject.setFeejecuc(executionsGroup.getFhejecuc() == null ? mExecution.values().iterator().next().getFeejecuc()
        : new Date(executionsGroup.getFhejecuc().toGregorianCalendar().getTimeInMillis()));

    String clearingPlatform = executionsGroup.getClearingPlatform();
    LOG.warn(" xml executionsGroup.getClearingPlatform() : " + clearingPlatform);

    if (StringUtils.isBlank(clearingPlatform)) {

      String nuejecuc = executionsGroup.getAllocation().get(0).getExecutions().get(0).getNuejecuc();
      clearingPlatform = mExecution.get(nuejecuc).getClearingplatform();

      LOG.trace(" nuejecuc : " + nuejecuc + " clearing platform : " + clearingPlatform);
    }
    String clearingPlatform2 = cachedConfig.getClearingPlatformClearingPlatform(conversiones, clearingPlatform);
    if (clearingPlatform2 != null && !clearingPlatform2.trim().isEmpty()) {
      clearingPlatform = clearingPlatform2;
    }
    jdoObject.setCdcamara(clearingPlatform);

    /*
     * carga en tmct0GrupoEjecucion de la lista de elementos AllocationID
     */
    List<Tmct0ejecucionalocation> ejealos = new ArrayList<Tmct0ejecucionalocation>();
    double acu = 0;
    Map<String, Tmct0ejecucionalocation> mEjeAlo = new HashMap<String, Tmct0ejecucionalocation>();
    for (ExecutionID executionID : allocationId.getExecutions()) {
      if ((ejeAlo = mEjeAlo.get(executionID.getNuejecuc())) == null) {
        ejeAlo = new Tmct0ejecucionalocation();
        ejeAlo.setTmct0grupoejecucion(jdoObject);
        ejeAlo.setTmct0eje(mExecution.get(executionID.getNuejecuc()));
      }
      jdoObject.setCdsettmkt(mExecution.get(executionID.getNuejecuc()).getCdmercad());
      xmlTmct0gal.xml_mapping(ejeAlo, executionID);
      mEjeAlo.put(executionID.getNuejecuc(), ejeAlo);
      ejealos.add(ejeAlo);
      acu += executionID.getNumtitasig();
    }
    mEjeAlo.clear();
    jdoObject.setNutitulos(BigDecimal.valueOf(acu).setScale(5, RoundingMode.HALF_UP));
    jdoObject.getTmct0ejecucionalocations().addAll(ejealos);
    jdoObject.setAuditFechaCambio(new java.util.Date());
  }
}
