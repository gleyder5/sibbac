package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.dao.Tmct0GruposValoresDao;
import sibbac.business.wrappers.database.dao.Tmct0TiposProductoDao;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIConstants.VA_CONSTANTS;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.va.R00;
import sibbac.pti.messages.va.R01;
import sibbac.pti.messages.va.VA;
import sibbac.pti.messages.va.t2s.R00T2s;
import sibbac.pti.messages.va.t2s.R01T2s;

@Service("ptiMessageProcessVaBo")
public class PtiMessageProcessVaBo extends PtiMessageProcess<VA> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessVaBo.class);

  @Autowired
  private Tmct0ValoresBo vaBo;

  @Autowired
  private Tmct0GruposValoresDao grupoValoresDao;

  @Autowired
  private Tmct0TiposProductoDao tiposProductoDao;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCcBo;

  public PtiMessageProcessVaBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, VA va, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    // 1(R00) + N(R01)
    List<PTIMessageRecord> r00s = va.getAll(PTIMessageRecordType.R00);
    List<PTIMessageRecord> r01s = va.getAll(PTIMessageRecordType.R01);
    if (CollectionUtils.isEmpty(r00s)) {
      LOG.warn("VA no tiene r00s.");
    }
    else {
      // Tratamos el R00 primero.
      R00 r00 = (R00) r00s.get(0);
      String codigo = r00.getCodigoDeValor();

      // Procesamiento del mensaje recibido de tipo VA
      List<Tmct0Valores> valores = vaBo.findByCodigoDeValorOrderByFechaEmisionDesc(codigo);
      Tmct0Valores valor = null;
      // boolean esNuevo = false;
      if (CollectionUtils.isEmpty(valores)) {
        // Como no existe, creamos un nuevo objeto.
        valor = new Tmct0Valores();
        // esNuevo = true;
      }
      else if (valores.size() > 1) {
        // Como no pueden haber mas de 1, salimos.
        throw new SIBBACBusinessException(String.format("Se han encontrado (%s) registros para dicho valor (%s)",
            valores.size(), codigo));
      }
      else {
        // Como existe, cogemos el primer objeto
        valor = valores.get(0);
      }

      // Tenemos un registro, ya sea nuevo o existente.
      // Actualizamos el valor con la informacion recibida.

      valor.setActivo("S");
      valor.setCodigoDeValor(r00.getCodigoDeValor());
      valor.setDivisa(r00.getDivisa());
      valor.setDescripcion(r00.getDescripcionDelValor());
      valor.setDescripcionCorta(r00.getDescripcionCortaDelValor());
      valor.setCodigoContratacion(r00.getCodigoContratacion());
      valor.setTmct0GruposValores(grupoValoresDao.findByCdCodigo(r00.getGrupoDeValores()));
      valor.setTmct0TiposProducto(tiposProductoDao.findByCdCodigo(r00.getTipoDeProducto()));
      if (r00.getFechaEmision() != null) {
        valor.setFechaEmision(r00.getFechaEmision());
      }
      valor.setPrecioEjercicio(r00.getPrecioEjercicio());
      valor.setUnidadDeContratacion(r00.getUnidadDeContratacion());
      valor.setEstiloEjercicio(r00.getEstiloEjercicio());
      valor.setFactorConversion(r00.getFactorConversion());
      valor.setIndCallPut(r00.getIndCallPut());
      valor.setEntidadEmisoraGestora(r00.getEntidadEmisoraGestora());

      valor.setTmct0CamaraCompensacion(camaraCcBo.findByCdCodigo(cache.getIdPti()));
      if (version == PTIMessageVersion.VERSION_T2S) {
        valor.setPorcentajeCuponRf(((R00T2s) r00).getCuponRf());
        if (((R00T2s) r00).getFechaInicioDevengoRf() != null) {
          valor.setFechaInicioDevengoCuponRf(new java.sql.Date(((R00T2s) r00).getFechaInicioDevengoRf().getTime()));
        }
        valor.setGrupoCompensacionRf(((R00T2s) r00).getGrupoCompensacionRf());
      }
      BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
      BigDecimal ultimaNegociacionBd;

      // Estos valores son por "R01".
      if (CollectionUtils.isEmpty(r01s)) {
        // Nada que hacer. No hay informacion.
      }
      else {
        R01 r01 = null;
        String tipo = null;
        for (PTIMessageRecord record : r01s) {
          r01 = (R01) record;

          // Que tipo de informacion viene...
          tipo = r01.getTipoInformacionAdicional();

          if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.BARRERA_INFERIOR_INLINES)) {
            valor.setBarreraInferiorInlines(r01.getPrecio());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.FECHA_ULTIMO_DIA_NEGOCIACION)) {
            if (r01.getFechaUltimoDiaNegociacion() != null) {
              ultimaNegociacionBd = FormatDataUtils.convertStringToBigDecimal(r01.getFechaUltimoDiaNegociacion());
              if (now.compareTo(ultimaNegociacionBd) > 0) {
                valor.setActivo("N");
              }
              valor.setFechaUltimoDiaNegociacion(r01.getFechaUltimoDiaNegociacion());
            }
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.BARRERA_SUPERIOR_INLINES)) {
            valor.setBarreraSuperiorInlines(r01.getPrecio());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.BARRERA_INFERIOR_ACTIVACION)) {
            valor.setBarreraInferiorActivacion(r01.getPrecio());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.BARRERA_SUPERIOR_ACTIVACION)) {
            valor.setBarreraSuperiorActivacion(r01.getPrecio());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.NUMERO_DECIMALES)) {
            valor.setNumeroDecimales(r01.getNumeroDeDecimales().intValue());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.CODIGO_DCV)) {
            valor.setCodigoDcv(r01.getCodDCV());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.BARRERA_BONUS)) {
            valor.setBarreraBonus(r01.getPrecio());
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.PERIODICIDAD_CUPON_RF)) {
            if (version == PTIMessageVersion.VERSION_T2S) {
              if (((R01T2s) r01).getPeriodicidadCuponRf() != null) {
                valor.setPeriodicidadCuponRf(Integer.parseInt(((R01T2s) r01).getPeriodicidadCuponRf().toString()));
              }
            }
            else {
              LOG.warn("INCIDENCIA- Tipo: {} no contemplado para la version del objecto: {}", tipo, r01);
            }
          }
          else if (tipo.equalsIgnoreCase(VA_CONSTANTS.INDICADOR_ADICIONAL.METODO_CALCULO_CUPON_RF)) {
            if (version == PTIMessageVersion.VERSION_T2S) {
              if (StringUtils.isNotBlank(((R01T2s) r01).getMetodoCalculoCuponCorridoRf())) {

                valor.setMetodoCalculoCuponRf(((R01T2s) r01).getMetodoCalculoCuponCorridoRf().charAt(0));
              }
            }
            else {
              LOG.warn("INCIDENCIA- Tipo: {} no contemplado para la version del objecto: {}", tipo, r01);
            }
          }
        }
      }

      vaBo.save(valor);
    }
  }
}
