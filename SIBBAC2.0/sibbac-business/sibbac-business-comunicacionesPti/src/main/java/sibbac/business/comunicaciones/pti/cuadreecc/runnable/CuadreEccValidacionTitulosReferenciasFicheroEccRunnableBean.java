package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccReferenciaBo;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean")
public class CuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean extends IRunnableBean<CuadreEccIdDTO> {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean.class);

  @Value("${sibbac.cuadreecc.file.reader.referencia.titular.aaunknown:AAUNKNOWN}")
  private String refTitularAAUNKNOWN;

  @Value("${sibbac.cuadreecc.file.reader.entidad.comunicadora.system:SYSTEM}")
  private String entComunicadoraSYSTEM;

  @Value("${sibbac.cuadreecc.file.reader.entidad.comunicadora.aaunknown:SIBBAC}")
  private String entComunicadoraAAUNKNOWN;

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  public CuadreEccValidacionTitulosReferenciasFicheroEccRunnableBean() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<CuadreEccIdDTO> beans, int order) throws SIBBACBusinessException {
    try {
      cuadreEccReferenciaBo.arreglarReferenciasTitularFicheroEcc(beans, entComunicadoraSYSTEM,
                                                                 entComunicadoraAAUNKNOWN, refTitularAAUNKNOWN);
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException("INCIDENCIA orden: " + order + " BEANS: " + beans, e);
    } finally {
      getObserver().sutDownThread(this);
    }

  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<CuadreEccIdDTO> clone() {
    return this;
  }

}
