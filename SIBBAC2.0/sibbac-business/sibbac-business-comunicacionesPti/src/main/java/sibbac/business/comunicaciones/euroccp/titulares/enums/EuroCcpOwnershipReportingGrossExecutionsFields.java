package sibbac.business.comunicaciones.euroccp.titulares.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpOwnershipReportingGrossExecutionsFields implements WrapperFileReaderFieldEnumInterface {

  TRADE_DATE("tradeDate", 8, 0, WrapperFileReaderFieldType.DATE),
  EXECUTION_REFERENCE("executionReference", 20, 0, WrapperFileReaderFieldType.STRING),
  EXCHANGE_CODE_TRADE("mic", 4, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 4, 0, WrapperFileReaderFieldType.STRING),
  OWNER_REFERENCE_FROM("ownerReferenceFrom", 20, 0, WrapperFileReaderFieldType.STRING),
  OWNER_REFERENCE_TO("ownerReferenceTo", 20, 0, WrapperFileReaderFieldType.STRING),
  NUMBER_SHARES("numberShares", 10, 0, WrapperFileReaderFieldType.NUMERIC),
  PROCESSING_STATUS("processingStatus", 1, 0, WrapperFileReaderFieldType.CHAR),
  ERROR_CODE("errorCode", 2, 0, WrapperFileReaderFieldType.STRING),
  ERROR_MESSAGE("errorMessage", 45, 0, WrapperFileReaderFieldType.STRING),
  FILLER("filler", 121, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpOwnershipReportingGrossExecutionsFields(String name,
                                                         int length,
                                                         int scale,
                                                         WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
