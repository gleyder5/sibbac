package sibbac.business.titulares.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class CreacionTablasTitularesMultithread {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CreacionTablasTitularesMultithread.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private TitularesBo titularesBo;
  
  @Value("${sibbac.nacional.creacion.tablas.titulares.thread.pool.size:5}")
  private int threadPoolSize;

  @Transactional
  public void crearTablasTitulares(boolean isScriptDividend, String auditUser) throws SIBBACBusinessException {

    try (TitularesConfigDTO config = titularesBo.getConfig(auditUser)) {

      this.crearTablasTitularesSinReferenciaTitular(true, isScriptDividend, config);
      this.crearTablasTitularesSinReferenciaTitular(false, isScriptDividend, config);
      this.crearTablasTitularesConReferenciaSinRtTi(isScriptDividend, config);
    }

  }

  private void crearTablasTitularesSinReferenciaTitular(boolean routing, boolean isScriptDividend,
      TitularesConfigDTO config) {

    int pageNumer = 0;
    int listbookingSize;
    List<Tmct0bokId> listBookings;
    PageRequest page = new PageRequest(0, 100000);
    char isscrdiv = isScriptDividend ? 'S' : 'N';
    LOG.debug("[crearTablasTitularesSinReferenciaTitular] buscando fechas sin referencia de titular...");
    List<Date> listFechas = alcBo.findAllFeejeliqBySinReferenciaAndNotCdestadotitAndCdestadotitLt(
        EstadosEnumerados.TITULARIDADES.INCIDENCIAS.getId(),
        EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId());

    LOG.debug("[crearTablasTitularesSinReferenciaTitular] encontradas {}-{} fechas sin referencia de titular.",
        listFechas.size(), listFechas);
    for (Date fecha : listFechas) {
      pageNumer = 0;
      do {
        LOG.debug(
            "[crearTablasTitularesSinReferenciaTitular] buscando bookings {} fecha: {} page: {}, tamanio pagina: {} ...",
            routing ? "ROUTING" : "NO ROUTING", fecha, pageNumer, page.getPageSize());
        if (routing) {
          listBookings = alcBo.findAllTmct0bokIdByFeejeliqSinReferenciaTitularRouting(fecha, isscrdiv,
              EstadosEnumerados.TITULARIDADES.INCIDENCIAS.getId(), page);
        }
        else {

          listBookings = alcBo.findAllTmct0bokIdByFeejeliqAndSinReferenciaTitularNoRouting(fecha, isscrdiv,
              EstadosEnumerados.TITULARIDADES.INCIDENCIAS.getId(), page);
        }

        listbookingSize = listBookings.size();

        LOG.debug(
            "[crearTablasTitularesSinReferenciaTitular] encontrados {} bookings {} fecha: {} page: {} posInicio: {} posFinal: {} ...",
            listbookingSize, routing ? "ROUTING" : "NO ROUTING", fecha, pageNumer, pageNumer * page.getPageSize(),
            pageNumer * page.getPageSize() + listbookingSize);

        if (CollectionUtils.isNotEmpty(listBookings)) {

          this.crearTablasTitularesBookings(isScriptDividend, listBookings, config);

        }
        pageNumer++;
      }
      while (listbookingSize == page.getPageSize());
    }
  }

  private void crearTablasTitularesConReferenciaSinRtTi(boolean isScriptDividend, TitularesConfigDTO config) {

    int pageNumer = 0;
    int listbookingSize;
    List<Tmct0bokId> listBookings;
    PageRequest page = new PageRequest(0, 100000);
    char isscrdiv = isScriptDividend ? 'S' : 'N';
    Integer[] estados = { EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId(),
        EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR_MODIFICACION.getId(),
        EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR_RECTIFICACION.getId() };
    List<Integer> listEstados = Arrays.asList(estados);

    LOG.debug("[crearTablasTitularesConReferenciaSinRtTi] buscando fechas con referencia de titular y pdte enviar titulares...");
    List<Date> listFechas = alcBo.findAllFeejeliqByConReferenciaAndInCdestadotitAndCdestadoasigGte(listEstados,
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());

    LOG.debug(
        "[crearTablasTitularesConReferenciaSinRtTi] encontradas {}-{} fechas con referencia de titular y pdte enviar titulares.",
        listFechas.size(), listFechas);
    for (Date fecha : listFechas) {
      pageNumer = 0;
      do {
        LOG.debug(
            "[crearTablasTitularesConReferenciaSinRtTi] buscando bookings fecha: {} page: {}, tamanio pagina: {} ...",
            fecha, pageNumer, page.getPageSize());

        listBookings = alcBo.findAllTmct0bokIdByFeejeliqInCdestadotitAndCdestadoasigGteConReferenciaTitular(fecha,
            isscrdiv, listEstados, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(), page);

        listbookingSize = listBookings.size();

        LOG.debug(
            "[crearTablasTitularesConReferenciaSinRtTi] encontrados {} bookings fecha: {} page: {} posInicio: {} posFinal: {} ...",
            listbookingSize, fecha, pageNumer, pageNumer * page.getPageSize(), pageNumer * page.getPageSize()
                + listbookingSize);

        if (CollectionUtils.isNotEmpty(listBookings)) {

          this.crearTablasTitularesBookings(isScriptDividend, listBookings, config);

        }
        pageNumer++;
      }
      while (listbookingSize == page.getPageSize());
    }
  }

  private void crearTablasTitularesBookings(boolean isScriptDividend, List<Tmct0bokId> listBookings,
      TitularesConfigDTO config) {
    long size;
    String msg;
    Tmct0bokId bokId;

    CreacionTablasTitularesByBookingCallable desgloseAutomaticoCallable;

    if (CollectionUtils.isNotEmpty(listBookings)) {

      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      if (isScriptDividend) {
        tfb.setNameFormat("Thread-CreacionTablasTitulares-SD-%d");
      }
      else {
        tfb.setNameFormat("Thread-CreacionTablasTitulares-%d");
      }

      ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize, tfb.build());
      Future<Void> future;
      List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
      Map<Future<Void>, CreacionTablasTitularesByBookingCallable> mapCallables = new HashMap<Future<Void>, CreacionTablasTitularesByBookingCallable>();
      try {
        for (Tmct0bokId tmct0bokId : listBookings) {
          LOG.debug("[crearTablasTitulares] encolando proceso para crear tablas titulares booking: {}", tmct0bokId);
          try {
            desgloseAutomaticoCallable = ctx
                .getBean(CreacionTablasTitularesByBookingCallable.class, tmct0bokId, config);

            future = executor.submit(desgloseAutomaticoCallable);
            listFutures.add(future);
            mapCallables.put(future, desgloseAutomaticoCallable);
            size = ((ThreadPoolExecutor) executor).getTaskCount()
                - ((ThreadPoolExecutor) executor).getCompletedTaskCount();
            if (size > 10000) {
              Thread.sleep(500);
            }
          }
          catch (Exception e) {
            msg = String.format("Incidencia lanzando hilo para booking %s %s", tmct0bokId, e.getMessage());
            throw new SIBBACBusinessException(msg, e);
          }
        }

        executor.shutdown();
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();

      }
      finally {

        try {
          // esperamos a los hilos 10 minutos por hilo + 30 minutos de minimo
          executor.awaitTermination(10 * listBookings.size() + 30, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }

        for (Future<Void> future2 : listFutures) {
          desgloseAutomaticoCallable = mapCallables.get(future2);
          bokId = null;
          if (desgloseAutomaticoCallable != null) {
            bokId = desgloseAutomaticoCallable.getId();
            LOG.trace("[crearTablasTitulares] revisando resultado hilo booking: {}", bokId);
          }
          try {
            future2.get(2, TimeUnit.MINUTES);
          }
          catch (InterruptedException | ExecutionException e) {
            LOG.warn("[crearTablasTitulares] incidencia con booking {} {}", bokId, e.getMessage(), e);
          }
          catch (TimeoutException e) {
            if (!future2.isDone() && future2.cancel(true)) {
              LOG.warn("[crearTablasTitulares] cancelando el future: {} booking: {}", future2, bokId);
            }
            else {
              LOG.warn("[crearTablasTitulares] incidencia con booking {} {}", bokId, e.getMessage(), e);
            }
          }
          catch (Exception e) {
            LOG.warn("[crearTablasTitulares] incidencia con booking {} {}", bokId, e.getMessage(), e);
            // throw new SIBBACBusinessException(e.getMessage(), e);
          }
        }

        if (!executor.isTerminated()) {
          executor.shutdownNow();
          try {
            executor.awaitTermination(1, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
        mapCallables.clear();
        listBookings.clear();
        listFutures.clear();
      }

    }

  }
}
