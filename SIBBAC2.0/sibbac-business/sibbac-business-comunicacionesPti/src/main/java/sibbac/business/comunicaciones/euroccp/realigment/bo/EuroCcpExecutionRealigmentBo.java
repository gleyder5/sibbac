package sibbac.business.comunicaciones.euroccp.realigment.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.bo.EuroCcpProcessedUnsettledMovementBo;
import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpErrorCodeEnum;
import sibbac.business.comunicaciones.euroccp.realigment.dao.EuroCcpExecutionRealigmentDao;
import sibbac.business.comunicaciones.euroccp.realigment.dto.EuroCcpExecutionRealigmentDTO;
import sibbac.business.comunicaciones.euroccp.realigment.model.EuroCcpExecutionRealigment;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpErrorCode;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.wrappers.database.bo.MovimientoEccS3Bo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.model.MovimientoEccS3;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class EuroCcpExecutionRealigmentBo extends
    AbstractBo<EuroCcpExecutionRealigment, Long, EuroCcpExecutionRealigmentDao> {

  /**
   * A common logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(EuroCcpExecutionRealigmentBo.class);

  @Autowired
  private Tmct0CuentasDeCompensacionBo ccBo;

  @Autowired
  private Tmct0xasBo conversionesBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private EuroCcpProcessedUnsettledMovementBo unsettledMovementBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private MovimientoEccS3Bo movS3Bo;

  public EuroCcpExecutionRealigmentBo() {
  }

  /*************************** QUERYS *********************************/

  //TEST 
  public List<Object[]> findAllClientNumberByEstadoProcesado(int estadoProcesado) {
    return dao.findAllClientNumberByEstadoProcesado(estadoProcesado);
  }

  public List<EuroCcpExecutionRealigment> findAllByEstadoProcesadoAndClientNumber(int estadoProcesado,
      String clientNumber) {
    return dao.findAllByEstadoProcesadoAndClientNumber(estadoProcesado, clientNumber);
  }

  public List<EuroCcpIdDTO> findAllEuroCcpIdDTOByEstadoProcesadoAndClientNumber(int estadoProcesado,
      String clientNumber, Date processingDate, Date transactionDate) {
    return dao.findAllEuroCcpIdDTOByEstadoProcesadoAndClientNumber(estadoProcesado, clientNumber, processingDate,
        transactionDate);
  }

  public List<EuroCcpExecutionRealigment> findAllByTradeDateAndExecutionReferenceAndMicAndAccNumberFromAndAccNumberToAndNumberSheresAndClientNumberAndFileSent(
      Date tradeDate, String executionReference, String mic, String accountNumberFrom, String accountNumberTo,
      BigDecimal numberShares, String clientNumber, long idFileSent) {
    return dao
        .findAllByTradeDateAndExecutionReferenceAndMicAndAccNumberFromAndAccNumberToAndNumberSheresAndClientNumberAndFileSent(
            tradeDate, executionReference, mic, accountNumberFrom, accountNumberTo, numberShares, clientNumber,
            idFileSent);
  }

  /***************************
   * QUERYS
   * @throws SIBBACBusinessException
   *********************************/

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.MANDATORY)
  public void insertTraspasoEuroCcp(Tmct0alcId alcId, List<Tmct0desgloseclitit> listDcts, TraspasosEccConfigDTO config,
      CallableResultDTO res) throws SIBBACBusinessException {
    if (CollectionUtils.isNotEmpty(listDcts)) {
      LOG.trace("[insertTraspasoEuroCcp] inicio modulo traspasos EuroCcp {}...", config.getCamaraEuroccp());
      String msg;
      String ctaEuroCcpOrigen;
      String ctaEuroCcpDestino;
      String ctaOrigenDesgloseSt;
      Tmct0CuentasDeCompensacion ctaOrigen;
      Tmct0CuentasDeCompensacion ctaDestino = ccBo.findByCdCodigo(config.getCtaCompensacionDestino());
      Tmct0anotacionecc an;
      Map<String, String> mapCtaEuroEccByCtaCompensacion = new HashMap<String, String>();

      LOG.trace("[insertTraspasoEuroCcp] validando cuenta compensacion destino: {}...",
          config.getCtaCompensacionDestino());
      if (ctaDestino == null || StringUtils.isBlank(ctaDestino.getAccountNumber())) {
        msg = String.format("No encontrada cta destino euro ccp para la cuenta '%s'",
            config.getCtaCompensacionDestino());
        throw new SIBBACBusinessException(msg);
      }

      ctaEuroCcpDestino = ctaDestino.getAccountNumber();
      LOG.trace("[insertTraspasoEuroCcp] validando cuenta euroccp destino: {}...", ctaEuroCcpDestino);
      BigDecimal titulosTraspasos = BigDecimal.ZERO;
      Tmct0desglosecamara dc;
      String buySellCode = conversionesBo.getSentidoSibbacSentidoEuroCcp(listDcts.get(0).getTmct0desglosecamara()
          .getTmct0alc().getTmct0alo().getCdtpoper()
          + "");
      int countInsert = 0;
      Collection<Long> idsDesgloseCamara = new HashSet<Long>();
      List<Tmct0desglosecamara> listDesglosesCamara = new ArrayList<Tmct0desglosecamara>();
      for (Tmct0desgloseclitit dct : listDcts) {
        dc = dct.getTmct0desglosecamara();
        if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(config.getCamaraEuroccp())) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO
              .getId()
              || dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
                  .getId()) {
            msg = String.format("El desglose en estado '%s' no pertite el traspaso.", dc.getTmct0estadoByCdestadoasig()
                .getNombre());
            throw new SIBBACBusinessException(msg);
          }
          LOG.trace("[insertTraspasoEuroCcp] buscando an euroccp...");

          if (StringUtils.isBlank(dct.getCdoperacionecc())) {
            msg = "EL codigo de operacion no puede ser vacio";
            throw new SIBBACBusinessException(msg);
          }

          an = anBo.findByCdoperacionecc(dct.getCdoperacionecc());

          if (an == null) {
            msg = String.format("Anotacion para op '%s' no encontrada.", dct.getCdoperacionecc());
            throw new SIBBACBusinessException(msg);
          }
          ctaOrigenDesgloseSt = dct.getCuentaCompensacionDestino();

          dctBo.cargarCuentaCompensacionFromAn(alcId, dct, an, config.getAuditUser());
          ctaOrigenDesgloseSt = dct.getCuentaCompensacionDestino();

          if ((ctaEuroCcpOrigen = mapCtaEuroEccByCtaCompensacion.get(ctaOrigenDesgloseSt)) == null) {
            ctaOrigen = ccBo.findByCdCodigo(ctaOrigenDesgloseSt);
            if (ctaOrigen == null || StringUtils.isBlank(ctaOrigen.getAccountNumber())) {
              msg = String.format("No encontrada cta euro ccp para el desglose cta '%s'", ctaOrigenDesgloseSt);
              throw new SIBBACBusinessException(msg);
            }
            ctaEuroCcpOrigen = ctaOrigen.getAccountNumber();
            mapCtaEuroEccByCtaCompensacion.put(dct.getCuentaCompensacionDestino().trim(), ctaEuroCcpOrigen);
          }

          if (StringUtils.equals(ctaOrigenDesgloseSt, config.getCtaCompensacionDestino())) {
            msg = String.format("La cta origen del desglose '%s' no puede ser igual a la destino '%s'",
                ctaOrigenDesgloseSt, config.getCtaCompensacionDestino());
            throw new SIBBACBusinessException(msg);
          }

          if (an.getImtitulosdisponibles().compareTo(dct.getImtitulos()) >= 0) {
            LOG.trace("[insertTraspasoEuroCcp] hay titulos suficientes para traspasar...");

            LOG.trace("[insertTraspasoEuroCcp]buscando movimiento euroccp {}...", dct.getCdoperacionecc());

            this.crearReasignacion(dct, an, buySellCode, ctaEuroCcpOrigen, config.getCtaCompensacionDestino(),
                ctaEuroCcpDestino, config.getAuditUser());
            titulosTraspasos = titulosTraspasos.add(dct.getImtitulos());
            countInsert++;
            if (idsDesgloseCamara.add(dc.getIddesglosecamara())) {
              listDesglosesCamara.add(dc);
            }
          }
          else {
            msg = String
                .format(
                    "En la ejecucion '%s'-'%s' quedan '%s' titulos cta '%s' que no son suficientes para traspasar el desglose de '%s' ",
                    dct.getCdPlataformaNegociacion(), dct.getNurefexemkt(),
                    an.getImtitulosdisponibles().setScale(2, RoundingMode.HALF_UP), ctaOrigenDesgloseSt, dct
                        .getImtitulos().setScale(2, RoundingMode.HALF_UP));
            throw new SIBBACBusinessException(msg);
          }
        }
      }// fin for

      if (countInsert > 0) {
        LOG.trace("[insertTraspasoEuroCcp]hemos creado registros, necesitamos cambiar los estados...");
        for (Tmct0desglosecamara dcFor : listDesglosesCamara) {
          LOG.trace("[insertTraspasoEuroCcp]cambiando estado de desglose camara: {} a ENVIANDOSE_TRASPASO...",
              dcFor.getIddesglosecamara());
          dcBo.ponerEstadoAsignacion(dcFor, config.getEstadoEnviandose(), config.getAuditUser());
          dcBo.save(dcFor);
          alcBo.save(dcFor.getTmct0alc());
          aloBo.save(dcFor.getTmct0alc().getTmct0alo());
          bokBo.save(dcFor.getTmct0alc().getTmct0alo().getTmct0bok());
        }
        msg = String.format("Orden de traspaso de '%s' titulos de la camara: '%s' a la Cta: '%s'",
            titulosTraspasos.setScale(2, RoundingMode.HALF_UP), config.getCamaraEuroccp(),
            config.getCtaCompensacionDestino());
        LOG.trace("[insertTraspasoEuroCcp] insertando mensaje en tmct0log con informacion {}...", msg);
        logBo.insertarRegistroNewTransaction(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "",
            config.getAuditUser());

        msg = String.format("'%s/%s/%s/%s' %s", alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
            alcId.getNucnfliq(), msg);
        LOG.debug("[insertTraspasoEuroCcp] {}", msg);
        res.addMessage(msg);

      }
      else {
        msg = String.format("'%s/%s/%s/%s' No se ha generado orden de traspaso para la camara: '%s'",
            alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), alcId.getNucnfliq(),
            config.getCamaraEuroccp());
        LOG.debug("[insertTraspasoEuroCcp] {}", msg);
        res.addErrorMessage(msg);

      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.MANDATORY)
  public BigDecimal insertTraspasoEuroCcpS3(List<Tmct0anotacionecc> listAnotaciones,
      String cuentaCompensacionDestinoSt, String camaraEuroccp, String auditUser) throws SIBBACBusinessException {
    BigDecimal titulosTraspasos = BigDecimal.ZERO;
    if (CollectionUtils.isNotEmpty(listAnotaciones)) {
      LOG.trace("[insertTraspasoEuroCcp] inicio modulo traspasos EuroCcp {}...", camaraEuroccp);
      String msg;
      // int countInsert = 0;
      String ctaEuroCcpOrigen;
      String ctaEuroCcpDestino;
      String ctaOrigenDesgloseSt;

      Tmct0CuentasDeCompensacion ctaOrigen;

      Tmct0CuentasDeCompensacion ctaDestino = ccBo.findByCdCodigo(cuentaCompensacionDestinoSt);
      Map<String, String> mapCtaEuroEccByCtaCompensacion = new HashMap<String, String>();

      LOG.trace("[insertTraspasoEuroCcp] validando cuenta compensacion destino: {}...", cuentaCompensacionDestinoSt);
      if (ctaDestino == null || StringUtils.isBlank(ctaDestino.getAccountNumber())) {
        msg = String.format("No encontrada cta destino euro ccp para la cuenta '%s'", cuentaCompensacionDestinoSt);
        throw new SIBBACBusinessException(msg);
      }

      ctaEuroCcpDestino = ctaDestino.getAccountNumber();
      LOG.trace("[insertTraspasoEuroCcp] validando cuenta euroccp destino: {}...", ctaEuroCcpDestino);

      for (Tmct0anotacionecc an : listAnotaciones) {

        if (an.getCdcamara() != null && camaraEuroccp != null && an.getCdcamara().trim().equals(camaraEuroccp)) {

          if (an.getEnvios3() != 'S') {

            ctaOrigenDesgloseSt = an.getTmct0CuentasDeCompensacion().getCdCodigo();

            if ((ctaEuroCcpOrigen = mapCtaEuroEccByCtaCompensacion.get(ctaOrigenDesgloseSt)) == null) {
              ctaOrigen = an.getTmct0CuentasDeCompensacion();
              if (ctaOrigen == null || StringUtils.isBlank(ctaOrigen.getAccountNumber())) {
                msg = String.format("No encontrada cta euro ccp para el desglose cta '%s'", ctaOrigenDesgloseSt);
                throw new SIBBACBusinessException(msg);
              }
              ctaEuroCcpOrigen = ctaOrigen.getAccountNumber();
              mapCtaEuroEccByCtaCompensacion.put(ctaOrigenDesgloseSt, ctaEuroCcpOrigen);
            }

            if (StringUtils.equals(ctaOrigenDesgloseSt, cuentaCompensacionDestinoSt)) {
              msg = String.format("La cta origen del desglose '%s' no puede ser igual a la destino '%s'",
                  ctaOrigenDesgloseSt, cuentaCompensacionDestinoSt);
              throw new SIBBACBusinessException(msg);
            }

            if (an.getImtitulosdisponibles().compareTo(BigDecimal.ZERO) > 0) {
              LOG.trace("[insertTraspasoEuroCcp] hay titulos suficientes para traspasar...");

              LOG.trace("[insertTraspasoEuroCcp]hemos creado registros, necesitamos cambiar los estados...");

              msg = String.format("Orden de traspaso de '%s' titulos de la camara: '%s' a la Cta: '%s'", an
                  .getImtitulosdisponibles().setScale(2, RoundingMode.HALF_UP), camaraEuroccp,
                  cuentaCompensacionDestinoSt);

              LOG.debug("[insertTraspasoEuroCcp] {}", msg);

              this.crearReasignacionS3(an, ctaDestino.getCdCodigo().trim(), ctaEuroCcpDestino, auditUser);
              titulosTraspasos = titulosTraspasos.add(an.getImtitulosdisponibles());
              // countInsert++;
            }

            else {
              msg = String
                  .format(
                      "En la ejecucion '%s'-'%s' quedan '%s' titulos cta '%s' que no son suficientes para traspasar el desglose de '%s' ",
                      an.getCdPlataformaNegociacion(), an.getNuexemkt(),
                      an.getImtitulosdisponibles().setScale(2, RoundingMode.HALF_UP), ctaOrigenDesgloseSt, an
                          .getImtitulosdisponibles().setScale(2, RoundingMode.HALF_UP));
              throw new SIBBACBusinessException(msg);
            }
          }
        }
      }// fin for

      msg = String.format("Se han traspasado en total '%s' titulos de la camara: '%s' a la Cta: '%s'",
          titulosTraspasos.setScale(2, RoundingMode.HALF_UP), camaraEuroccp, cuentaCompensacionDestinoSt);

      LOG.debug("[insertTraspasoEuroCcp] {}", msg);

    }

    return titulosTraspasos;
  }

  private EuroCcpExecutionRealigment crearReasignacion(Tmct0desgloseclitit dct, Tmct0anotacionecc an,
      String buySellCode, String ctaEuroCcpOrigen, String ctaCompensacionDestino, String ctaEuroCcpDestino,
      String auditUser) {
    EuroCcpProcessedUnsettledMovement euroCcpMovement = unsettledMovementBo
        .findByUnsettledReferenceTradeOrTransferOrBalance(dct.getCdoperacionecc());

    EuroCcpExecutionRealigment mov = new EuroCcpExecutionRealigment();
    mov.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());

    mov.setAccountNumberFrom(ctaEuroCcpOrigen);
    mov.setAccountNumberTo(ctaEuroCcpDestino);
    mov.setDesglose(dct);
    mov.setExecutionReference(euroCcpMovement.getExternalTransactionIdExchange());
    mov.setNumberShares(dct.getImtitulos());
    mov.setUnsettledMovement(euroCcpMovement);
    mov.setClientNumber(euroCcpMovement.getClientNumber());
    mov.setMic(euroCcpMovement.getExchangeCodeTrade());
    mov.setAccountNumberFrom(ctaEuroCcpOrigen);
    mov.setAccountNumberTo(ctaEuroCcpDestino);
    mov.setTradeDate(euroCcpMovement.getTransactionDate());
    mov.setAuditUser(auditUser);
    mov.setAuditDate(new Date());
    mov.setAnotacion(an);
    LOG.trace("[insertTraspasoEuroCcp]grabando registro para el envio del traspaso a euroccp...");
    Tmct0movimientoecc movEcc = this.crearMovimientoEcc(dct, auditUser);
    movEcc.setCuentaCompensacionDestino(ctaCompensacionDestino);
    movEcc.setMiembroCompensadorDestino("");
    movEcc.setCodigoReferenciaAsignacion("");
    movBo.save(movEcc);

    mov.setMovimientoEccOrden(movEcc);
    mov = save(mov);
    return mov;
  }

  private EuroCcpExecutionRealigment crearReasignacionS3(Tmct0anotacionecc an, String ctaCompensacionDestino,
      String ctaEuroCcpDestino, String auditUser) {
    LOG.trace("[crearReasignacionS3]buscando movimiento euroccp {}...", an.getCdoperacionecc());
    EuroCcpProcessedUnsettledMovement euroCcpMovement = unsettledMovementBo
        .findByUnsettledReferenceTradeOrTransferOrBalance(an.getCdoperacionecc());

    EuroCcpExecutionRealigment mov = new EuroCcpExecutionRealigment();
    mov.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());

    mov.setDesglose(null);
    mov.setExecutionReference(euroCcpMovement.getExternalTransactionIdExchange());
    mov.setNumberShares(an.getImtitulosdisponibles());
    mov.setUnsettledMovement(euroCcpMovement);
    mov.setClientNumber(euroCcpMovement.getClientNumber());
    mov.setMic(euroCcpMovement.getExchangeCodeTrade());
    mov.setAccountNumberFrom(euroCcpMovement.getAccountNumber());
    mov.setAccountNumberTo(ctaEuroCcpDestino);
    mov.setTradeDate(euroCcpMovement.getTransactionDate());
    mov.setAuditUser(auditUser);
    mov.setAuditDate(new Date());
    mov.setAnotacion(an);
    LOG.trace("[insertTraspasoEuroCcp]grabando registro para el envio del traspaso a euroccp...");

    mov.setMovimientoEccOrden(null);
    MovimientoEccS3 ms3 = new MovimientoEccS3();
    movS3Bo.inizializeMovimientoEccS3(an, an.getCdoperacionecc(), ctaCompensacionDestino, ms3);
    ms3.setAudit_fecha_cambio(new Date());
    ms3.setAudit_user(auditUser);
    movS3Bo.save(ms3);
    mov.setMovimientoEccS3(ms3);
    mov = save(mov);
    return mov;
  }

  private Tmct0movimientoecc crearMovimientoEcc(Tmct0desgloseclitit dct, String auditUser) {

    Tmct0movimientoecc movEcc = new Tmct0movimientoecc();

    Tmct0movimientooperacionnuevaecc movn = new Tmct0movimientooperacionnuevaecc();
    movBo.inizializeTmct0movimientoEcc(dct, movEcc, auditUser);
    movEcc.setImtitulos(movEcc.getImtitulos().add(dct.getImtitulos()));
    movEcc.setImefectivo(movEcc.getImefectivo().add(dct.getImefectivo()));
    movEcc.setTmct0desglosecamara(dct.getTmct0desglosecamara());
    movn.setTmct0movimientoecc(movEcc);
    movnBo.inizializeMovimientoOperacioNueva(dct, BigDecimal.ZERO, movn);
    movn.setAuditUser(auditUser);
    movEcc.getTmct0movimientooperacionnuevaeccs().add(movn);

    return movEcc;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void procesarBeansErgEntrada(EuroCcpFileSent file, List<EuroCcpCommonSendRegisterDTO> beans)
      throws SIBBACBusinessException {
    String accNumberFrom;
    String accNumberTo;
    String clientNumber;

    List<EuroCcpExecutionRealigment> listExecutioRealigment;
    EuroCcpExecutionRealigmentDTO bean;
    for (EuroCcpCommonSendRegisterDTO commomBean : beans) {
      bean = (EuroCcpExecutionRealigmentDTO) commomBean;
      LOG.trace("[procesarBeansErgEntrada] realigment {}", bean);
      accNumberFrom = StringUtils.leftPad(bean.getAccountNumberFrom(), 10, '0');
      accNumberTo = StringUtils.leftPad(bean.getAccountNumberTo(), 10, '0');
      clientNumber = file.getClientNumber();

      LOG.trace("[procesarBeansErgEntrada] buscando registros enviados en bbdd...");
      listExecutioRealigment = findAllByTradeDateAndExecutionReferenceAndMicAndAccNumberFromAndAccNumberToAndNumberSheresAndClientNumberAndFileSent(
          bean.getTradeDate(), bean.getExecutionReference(),
          bean.getMic(), accNumberFrom, accNumberTo,
          bean.getNumberShares(), clientNumber, file.getId());
      if (CollectionUtils.isNotEmpty(listExecutioRealigment)) {
        for (EuroCcpExecutionRealigment euroCcpExecutionRealigment : listExecutioRealigment) {

          LOG.trace("[procesarBeansErgEntrada] procesando registro {}...", euroCcpExecutionRealigment);

          this.validateReligmentIn(bean, euroCcpExecutionRealigment);

          if (StringUtils.isNotBlank(bean.getErrorCode())
              && !EuroCcpErrorCodeEnum.NO_ERROR.text.equals(bean.getErrorCode())) {
            this.procesarEntradaRealigmentError(bean, euroCcpExecutionRealigment);
          }
          else {
            this.procesarEntradaRealigment(euroCcpExecutionRealigment);
          }

          LOG.trace("[procesarBeansErgEntrada] cambiando estado a procesado...");
          // euroCcpExecutionRealigment.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
          euroCcpExecutionRealigment.setAuditUser("SIBBAC20");
          euroCcpExecutionRealigment.setAuditDate(new Date());
          LOG.trace("[procesarBeansErgEntrada] grabando registro...");
          save(euroCcpExecutionRealigment);
        }
      }
      else {
        LOG.warn("[procesarBeansErgEntrada] no encontrado registro en bbdd para el registro de entrada {}...",
            bean);
      }
    }

  }

  private void procesarEntradaRealigmentError(EuroCcpExecutionRealigmentDTO euroCcpExecutionRealigmentDTO,
      EuroCcpExecutionRealigment realigment) throws SIBBACBusinessException {

    LOG.warn("[procesarBeansErgEntrada] recepcion registro con codigo de error {}-{} - {}...",
        euroCcpExecutionRealigmentDTO.getErrorCode(), euroCcpExecutionRealigmentDTO.getErrorMessage(), realigment);
    realigment.setErrorCode(new EuroCcpErrorCode(euroCcpExecutionRealigmentDTO.getErrorCode()));

    Tmct0movimientoecc mov = realigment.getMovimientoEccOrden();
    MovimientoEccS3 movs3 = realigment.getMovimientoEccS3();

    if (mov != null) {
      mov.setCderror(euroCcpExecutionRealigmentDTO.getErrorCode());
      mov.setNberror(StringUtils.substring(euroCcpExecutionRealigmentDTO.getErrorMessage(), 0, 40));
      mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
      mov.setCdestadomov(EstadosEccMovimiento.RECHAZADO_ECC.text);
      mov.setAuditFechaCambio(new Date());
      mov.setAuditUser("SIBBAC20");
      movBo.save(mov);

      String msg = MessageFormat.format(
          "Recepcion Movimiento Ref.Mov: {0} estado: {1} Cta: {2} Ref.Interna: {3} Err: {4}-{5}", mov
              .getCdrefmovimiento().trim(), mov.getCdestadomov().trim(), mov.getCuentaCompensacionDestino(), mov
              .getCdrefinternaasig().trim(), euroCcpExecutionRealigmentDTO.getErrorCode().trim(),
          euroCcpExecutionRealigmentDTO.getErrorMessage().trim());

      logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), mov
          .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), mov.getTmct0desglosecamara().getTmct0alc()
          .getId().getNucnfclt(), msg, mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
          "SIBBAC20");
    }
    else if (movs3 != null) {
      // movs3.setCderror(euroCcpExecutionRealigmentDTO.getErrorCode());
      // movs3.setNberror(StringUtils.substring(euroCcpExecutionRealigmentDTO.getErrorMessage(),
      // 0, 40));
      movs3.setCdestadomov(EstadosEccMovimiento.RECHAZADO_ECC.text);
      movs3.setAudit_fecha_cambio(new Date());
      movs3.setAudit_user("SIBBAC20");
      movS3Bo.save(movs3);
    }
    realigment.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
    realigment.setAuditUser("SIBBAC20");
    realigment.setAuditDate(new Date());

  }

  private void validateReligmentIn(EuroCcpExecutionRealigmentDTO dto, EuroCcpExecutionRealigment realigment)
      throws SIBBACBusinessException {
    Tmct0anotacionecc an = realigment.getAnotacion();
    Tmct0desgloseclitit dct = realigment.getDesglose();
    Tmct0movimientoecc mov = realigment.getMovimientoEccOrden();
    MovimientoEccS3 movs3 = realigment.getMovimientoEccS3();
    EuroCcpProcessedUnsettledMovement movimientoEuroCcp = realigment.getUnsettledMovement();

    if (an == null) {
      throw new SIBBACBusinessException(MessageFormat.format("Anotacion ecc para realigment id: {0} no encontrado",
          realigment.getId()));
    }
    if (movimientoEuroCcp == null) {
      throw new SIBBACBusinessException(MessageFormat.format(
          "Unsettled Movement para realigment id: {0} no encontrado", realigment.getId()));
    }
    if (movs3 == null) {
      if (dct == null) {
        throw new SIBBACBusinessException(MessageFormat.format("Desglose para realigment id: {0} no encontrado",
            realigment.getId()));
      }

      if (mov == null) {
        throw new SIBBACBusinessException(MessageFormat.format("Movimiento ecc realigment id: {0} no encontrado",
            realigment.getId()));
      }
    }
  }

  private void procesarEntradaRealigment(EuroCcpExecutionRealigment realigment) throws SIBBACBusinessException {
    String msg;
    Tmct0movimientoecc mov = realigment.getMovimientoEccOrden();
    MovimientoEccS3 movs3 = realigment.getMovimientoEccS3();
    if (mov != null) {
      mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
      mov.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
      mov.setAuditFechaCambio(new Date());
      mov.setAuditUser("SIBBAC20");
      movBo.save(mov);

      msg = MessageFormat.format("Recepcion Movimiento Ref.Mov: {0} camara: {4} estado: {1} Cta: {2} Ref.Interna: {3}",
          mov.getCdrefmovimiento().trim(), mov.getCdestadomov().trim(), mov.getCuentaCompensacionDestino().trim(), mov
              .getCdrefinternaasig().trim(), realigment.getDesglose().getCdcamara().trim());

      LOG.debug("[procesarEntradaRealigment] {} - {}", mov.getTmct0desglosecamara().getTmct0alc().getId(), msg);

      logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), mov
          .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), mov.getTmct0desglosecamara().getTmct0alc()
          .getId().getNucnfclt(), msg, mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
          "SIBBAC20");

      msg = "Esperando fichero delta con el codigo de transferencia para escalar el estado.";

      logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), mov
          .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), mov.getTmct0desglosecamara().getTmct0alc()
          .getId().getNucnfclt(), msg, mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getNombre(),
          "SIBBAC20");
    }
    else if (movs3 != null) {
      movs3.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
      movs3.setAudit_fecha_cambio(new Date());
      movs3.setAudit_user("SIBBAC20");
      movS3Bo.save(movs3);
      msg = MessageFormat.format("Recepcion Movimiento Ref.Mov: {0} estado: {1} Cta: {2}", movs3.getCdrefmovimiento()
          .trim(), movs3.getCdestadomov().trim(), movs3.getCuentaCompensacionDestino().trim());
      LOG.debug("[procesarEntradaRealigment] {} - {}", movs3.getCdoperacionecc(), msg);
    }
    realigment.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
    realigment.setAuditUser("SIBBAC20");
    realigment.setAuditDate(new Date());

  }

}
