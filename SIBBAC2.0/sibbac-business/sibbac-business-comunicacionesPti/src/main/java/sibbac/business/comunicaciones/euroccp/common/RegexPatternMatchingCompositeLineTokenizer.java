package sibbac.business.comunicaciones.euroccp.common;

import java.util.Map;

import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

public class RegexPatternMatchingCompositeLineTokenizer implements LineTokenizer, InitializingBean  {

  public RegexPatternMatchingCompositeLineTokenizer() {
    super();
  }
  

  private RegexPatternMatcher<LineTokenizer> tokenizers = null;

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.batch.item.file.transform.LineTokenizer#tokenize(
   * java.lang.String)
   */
    @Override
  public FieldSet tokenize(String line) {
    return tokenizers.match(line).tokenize(line);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
   */
    @Override
  public void afterPropertiesSet() throws Exception {
    Assert.isTrue(this.tokenizers != null, "The 'tokenizers' property must be non-empty");
  }

  public void setTokenizers(Map<String, LineTokenizer> tokenizers) {
    Assert.isTrue(!tokenizers.isEmpty(), "The 'tokenizers' property must be non-empty");
    this.tokenizers = new RegexPatternMatcher<LineTokenizer>(tokenizers);
  }

}
