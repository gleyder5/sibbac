package sibbac.business.comunicaciones.pti.cuadreecc.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccLog;

@Component
public interface CuadreEccLogDao extends JpaRepository<CuadreEccLog, Long> {

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccEjecucion = :idCuadreEccEjecucion")
  Integer deleteByIdCuadreEccEjecucion(@Param("idCuadreEccEjecucion") Long idCuadreEccEjecucion);

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccSibbac = :idCuadreEccSibbac")
  Integer deleteByIdCuadreEccSibbac(@Param("idCuadreEccSibbac") Long idCuadreEccSibbac);

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccReferencia = :idCuadreEccReferencia")
  Integer deleteByIdCuadreEccReferencia(@Param("idCuadreEccReferencia") Long idCuadreEccReferencia);

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccTitular = :idCuadreEccTitular")
  Integer deleteByIdCuadreEccTitular(@Param("idCuadreEccTitular") Long idCuadreEccTitular);

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccTitular in "
         + "    (SELECT t.id FROM CuadreEccReferencia r, CuadreEccTitular t  WHERE r.id = t.cuadreEccReferencia.id AND r.fechaEje = :fechaEje AND r.numOp = :noper1 AND r.sentido = :sentido ) ")
  Integer deleteByCuadreEccTitularFechaNegAndNumOpAndSentido(@Param("fechaEje") Date fechaEje,
                                                             @Param("noper1") String numOp1,
                                                             @Param("sentido") Character sentido);

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccReferencia in "
         + "    (SELECT r.id FROM CuadreEccReferencia r WHERE r.fechaEje = :fechaEje AND r.numOp = :noper1 AND r.sentido = :sentido ) ")
  Integer deleteByCuadreEccReferenciaFechaNegAndNumOpAndSentido(@Param("fechaEje") Date fechaEje,
                                                                @Param("noper1") String numOp1,
                                                                @Param("sentido") Character sentido);

  @Modifying
  @Query("DELETE FROM CuadreEccLog l WHERE l.idCuadreEccSibbac in "
         + "    (SELECT s.id FROM CuadreEccSibbac s WHERE s.feoperac = :fechaEje AND s.numOp = :noper1 AND s.sentido = :sentido ) ")
  Integer deleteByCuadreEccSibbacFechaNegAndNumOpAndSentido(@Param("fechaEje") Date fechaEje,
                                                            @Param("noper1") String numOp1,
                                                            @Param("sentido") Character sentido);

  List<CuadreEccLog> findAllByIdCuadreEccSibbacAndCodError(Long idCuadreEccSibbac, String codError);

}
