package sibbac.business.comunicaciones.euroccp.ciffile.bo;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.ciffile.runnable.EuroCcpProcessedUnsettledMovementRunnable;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpProcessedUnsettledMovementDbReader")
public class EuroCcpProcessedUnsettledMovementDbReader extends WrapperMultiThreadedDbReader<EuroCcpIdDTO> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpProcessedUnsettledMovementBo.class);

  @Autowired
  private EuroCcpProcessedUnsettledMovementBo euroCcpProcessedUnsettledMovementBo;

  @Qualifier(value = "euroCcpProcessedUnsettledMovementRunnable")
  @Autowired
  private EuroCcpProcessedUnsettledMovementRunnable euroCcpProcessedUnsettledMovementRunnable;

  public EuroCcpProcessedUnsettledMovementDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<EuroCcpIdDTO> getBeanToProcessBlock() {
    return euroCcpProcessedUnsettledMovementRunnable;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    beanIterator = null;
    LOG.info("[EuroCcpProcessedUnsettledMovementDbReader :: preExecuteTask] buscando  operaciones en ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR-{}...",
             EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());
    List<EuroCcpIdDTO> l = euroCcpProcessedUnsettledMovementBo.findEuroCcpIdDTOByEstadoProcesadoAndTransactionDateLte(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId(),
                                                                                                                      new Date(),
                                                                                                                      100000);
    LOG.info("[EuroCcpProcessedUnsettledMovementDbReader :: preExecuteTask] encontradas {} operaciones.", l.size());
    beanIterator = l.iterator();
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return 1;
  }

  @Override
  public int getTransactionSize() {
    return 1;
  }

}
