/**
 * 
 */
package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xIS16630
 *
 */
public class DatosDesgloseIntermediarioFinancieroDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -8494463942120888674L;

  private String codigoEntidadLiquidadora;
  private String codigoEntidadCustoria;
  private String referenciaBancaria;
  private String cuentaLiquidacion;

  private Character corretajePti;

  private BigDecimal porcentajeComisionBanco;

  private BigDecimal porcentajeComisionOrdenante;

  private DatosAsignacionIntermediarioFinancieroDTO datosAsignacion;

  public String getCodigoEntidadLiquidadora() {
    return codigoEntidadLiquidadora;
  }

  public void setCodigoEntidadLiquidadora(String codigoEntidadLiquidadora) {
    this.codigoEntidadLiquidadora = codigoEntidadLiquidadora;
  }

  public String getCodigoEntidadCustoria() {
    return codigoEntidadCustoria;
  }

  public void setCodigoEntidadCustoria(String codigoEntidadCustoria) {
    this.codigoEntidadCustoria = codigoEntidadCustoria;
  }

  public String getReferenciaBancaria() {
    return referenciaBancaria;
  }

  public void setReferenciaBancaria(String referenciaBancaria) {
    this.referenciaBancaria = referenciaBancaria;
  }

  public String getCuentaLiquidacion() {
    return cuentaLiquidacion;
  }

  public void setCuentaLiquidacion(String cuentaLiquidacion) {
    this.cuentaLiquidacion = cuentaLiquidacion;
  }

  public BigDecimal getPorcentajeComisionBanco() {
    return porcentajeComisionBanco;
  }

  public void setPorcentajeComisionBanco(BigDecimal porcentajeComisionBanco) {
    this.porcentajeComisionBanco = porcentajeComisionBanco;
  }

  public BigDecimal getPorcentajeComisionOrdenante() {
    return porcentajeComisionOrdenante;
  }

  public void setPorcentajeComisionOrdenante(BigDecimal porcentajeComisionOrdenante) {
    this.porcentajeComisionOrdenante = porcentajeComisionOrdenante;
  }

  public DatosAsignacionIntermediarioFinancieroDTO getDatosAsignacion() {
    return datosAsignacion;
  }

  public void setDatosAsignacion(DatosAsignacionIntermediarioFinancieroDTO datosAsignacion) {
    this.datosAsignacion = datosAsignacion;
  }

  public Character getCorretajePti() {
    return corretajePti;
  }

  public void setCorretajePti(Character corretajePti) {
    this.corretajePti = corretajePti;
  }

  @Override
  public String toString() {
    return "DatosDesgloseIntermediarioFinancieroDTO [codigoEntidadLiquidadora=" + codigoEntidadLiquidadora
           + ", codigoEntidadCustoria=" + codigoEntidadCustoria + ", referenciaBancaria=" + referenciaBancaria
           + ", cuentaLiquidacion=" + cuentaLiquidacion + ", corretajePti=" + corretajePti
           + ", porcentajeComisionBanco=" + porcentajeComisionBanco + ", porcentajeComisionOrdenante="
           + porcentajeComisionOrdenante + ", datosAsignacion=" + datosAsignacion + "]";
  }

}
