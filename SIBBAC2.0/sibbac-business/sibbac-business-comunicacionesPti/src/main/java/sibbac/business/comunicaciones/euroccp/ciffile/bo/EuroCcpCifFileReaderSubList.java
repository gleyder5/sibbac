package sibbac.business.comunicaciones.euroccp.ciffile.bo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpAggregatesNetSettlementsDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpCashPositionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpProcessedMoneyMovementDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpProcessedSettledMovementDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpSettledPositionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpSettlementInstructionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpTrailerDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpUnsettledPositionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCppClearedGrossTradesSpainDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCppSpanishSettlementInstructionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpMessageType;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpAggregatesNetSettlements;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpCashPosition;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpClearedGrossTradesSpain;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedMoneyMovement;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedSettledMovement;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettledPosition;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettlementInstuction;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpTrailer;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpUnsettledPosition;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCppSpanishSettlementInstruction;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;

@Service
public class EuroCcpCifFileReaderSubList {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpCifFileReaderSubList.class);

  @Autowired
  private EuroCcpProcessedUnsettledMovementBo euroCcpProcessedUntsettledMovementBo;

  @Autowired
  private EuroCcpProcessedSettledMovementDao euroCcpProcessedSettledMovementDao;

  @Autowired
  private EuroCcpAggregatesNetSettlementsDao euroCcpAggregatesNetSettlementsDao;

  @Autowired
  private EuroCcpUnsettledPositionDao euroCcpUnsettledPositionDao;

  @Autowired
  private EuroCcpSettlementInstructionDao euroCcpSettlementInstructionDao;

  @Autowired
  private EuroCcpSettledPositionDao euroCcpSettledPositionDao;

  @Autowired
  private EuroCcpProcessedMoneyMovementDao euroCcpProcessedMoneyMovementDao;

  @Autowired
  private EuroCcpCashPositionDao euroCcpCashPositionDao;

  @Autowired
  private EuroCcpTrailerDao euroCcpTrailerDao;

  @Autowired
  private EuroCppClearedGrossTradesSpainDao euroCppClearedGrossTradesSpainDao;

  @Autowired
  private EuroCppSpanishSettlementInstructionDao euroCppSpanishSettlementInstructionDao;

  public EuroCcpCifFileReaderSubList() {
  }

  private String getUnsettletReferenceWithDate(EuroCcpProcessedUnsettledMovement mov) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String compositeUnsettledReference = "";
    String aux;
    if (mov.getTransactionDate() != null) {
      aux = Long.toString(Long.valueOf(sdf.format(mov.getTransactionDate())), Character.MAX_RADIX);
      aux = StringUtils.leftPad(aux, 6, "0");
      compositeUnsettledReference = String.format("%s%s%s", aux, mov.getBuySellCode(), mov.getUnsettledReference());
    }
    else {
      aux = Long.toString(Long.valueOf(sdf.format(new Date())), Character.MAX_RADIX);
      aux = StringUtils.leftPad(aux, 6, "0");
      compositeUnsettledReference = String.format("%s%s%s", aux, mov.getBuySellCode(), mov.getUnsettledReference());
    }
    return compositeUnsettledReference.toUpperCase();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void grabarRegistros(List<EuroCcpLineaFicheroRecordBean> beans, int order) throws SIBBACBusinessException {
    EuroCcpProcessedUnsettledMovement unsettledMovement;
    EuroCcpSettlementInstuction settletInstruction;
    EuroCcpProcessedMoneyMovement moneyMovement;
    EuroCcpCashPosition cashPosition;
    EuroCcpUnsettledPosition unsettledPosition;
    String compositeUnsettledReference;

    for (EuroCcpLineaFicheroRecordBean linea : beans) {
      try {
        LOG.trace("[EuroCcpCifFileReaderSubList :: run] bean.tipoRegistro == {}", linea.getTipoRegistro());
        LOG.trace("[EuroCcpCifFileReaderSubList :: run] bean.releaseCode == {}", linea.getReleaseCode());
        LOG.trace("[EuroCcpCifFileReaderSubList :: run] bean.processingDate == {}", linea.getProcessingDate());
        LOG.trace("[EuroCcpCifFileReaderSubList :: run] bean.claringSiteCode == {}", linea.getClearingSiteCode());
        linea.setAuditDate(new Date());
        linea.setAuditUser("SIBBAC20");
        linea.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());
        if (EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_409.text.equals(linea.getTipoRegistro())) {

          unsettledMovement = (EuroCcpProcessedUnsettledMovement) linea;
          compositeUnsettledReference = this.getUnsettletReferenceWithDate(unsettledMovement);
          unsettledMovement.setUnsettledReferenceOriginal(unsettledMovement.getUnsettledReference());
          unsettledMovement.setUnsettledReference(compositeUnsettledReference);
          euroCcpProcessedUntsettledMovementBo.save(unsettledMovement);
        }
        else if (EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_410.text.equals(linea.getTipoRegistro())) {
          compositeUnsettledReference = this.getUnsettletReferenceWithDate((EuroCcpProcessedUnsettledMovement) linea);
          unsettledMovement = euroCcpProcessedUntsettledMovementBo
              .findByUnsettledReferenceTradeOrTransferOrBalance(compositeUnsettledReference);
          if (unsettledMovement == null) {
            unsettledMovement = (EuroCcpProcessedUnsettledMovement) linea;
          }
          else {
            CloneObjectHelper.copyBasicFields((EuroCcpProcessedUnsettledMovement) linea, unsettledMovement, null);
            unsettledMovement.setAuditDate(new Date());
            unsettledMovement.setAuditUser("SIBBAC20");
          }

          unsettledMovement.setUnsettledReferenceOriginal(unsettledMovement.getUnsettledReference());
          unsettledMovement.setUnsettledReference(compositeUnsettledReference);
          euroCcpProcessedUntsettledMovementBo.save(unsettledMovement);
        }
        else if (EuroCcpMessageType.TYPE_PROCESSED_SETTLED_MOVEMENT_411.text.equals(linea.getTipoRegistro())) {
          euroCcpProcessedSettledMovementDao.save((EuroCcpProcessedSettledMovement) linea);
        }
        else if (EuroCcpMessageType.TYPE_CLEARED_GROSS_TRADES_SPAIN_412.text.equals(linea.getTipoRegistro())) {
          euroCppClearedGrossTradesSpainDao.save((EuroCcpClearedGrossTradesSpain) linea);
        }
        else if (EuroCcpMessageType.TYPE_AGGREGATES_NET_SETTLEMENTS_415.text.equals(linea.getTipoRegistro())) {
          euroCcpAggregatesNetSettlementsDao.save((EuroCcpAggregatesNetSettlements) linea);
        }
        else if (EuroCcpMessageType.TYPE_UNSETTLED_POSITION_420.text.equals(linea.getTipoRegistro())) {
          unsettledPosition = (EuroCcpUnsettledPosition) linea;
          euroCcpUnsettledPositionDao.save(unsettledPosition);
        }
        else if (EuroCcpMessageType.TYPE_SETTLED_POSITION_421.text.equals(linea.getTipoRegistro())) {
          euroCcpSettledPositionDao.save((EuroCcpSettledPosition) linea);
        }
        else if (EuroCcpMessageType.TYPE_SETTLEMENT_INSTRUCTION_450.text.equals(linea.getTipoRegistro())) {
          settletInstruction = euroCcpSettlementInstructionDao
              .findBySettlementInstructionReference(((EuroCcpSettlementInstuction) linea)
                  .getSettlementInstructionReference());
          if (settletInstruction == null) {
            settletInstruction = (EuroCcpSettlementInstuction) linea;
          }
          else {
            CloneObjectHelper.copyBasicFields((EuroCcpSettlementInstuction) linea, settletInstruction, null);
            settletInstruction.setAuditDate(new Date());
            settletInstruction.setAuditUser("SIBBAC20");
          }
          euroCcpSettlementInstructionDao.save(settletInstruction);
        }
        else if (EuroCcpMessageType.TYPE_SPANISH_SETTLEMENT_INSTRUCTION_452.text.equals(linea.getTipoRegistro())) {
          euroCppSpanishSettlementInstructionDao.save((EuroCppSpanishSettlementInstruction) linea);

        }
        else if (EuroCcpMessageType.TYPE_PROCESSED_MONEY_MOVEMENT_600.text.equals(linea.getTipoRegistro())) {
          moneyMovement = (EuroCcpProcessedMoneyMovement) linea;
          euroCcpProcessedMoneyMovementDao.save(moneyMovement);
        }
        else if (EuroCcpMessageType.TYPE_CASH_POSITION_610.text.equals(linea.getTipoRegistro())) {
          cashPosition = (EuroCcpCashPosition) linea;
          euroCcpCashPositionDao.save(cashPosition);
        }
        else if (EuroCcpMessageType.TYPE_TRAILER_910.text.equals(linea.getTipoRegistro())) {
          euroCcpTrailerDao.save((EuroCcpTrailer) linea);
        }
        else {
          LOG.warn("[EuroCcpCifFileReaderSubList :: run] bean.tipoRegistro == {} no contemplado",
              linea.getTipoRegistro());
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("No se ha podido grabar el registro %s por %s", linea,
            e.getMessage()), e);
      }

    }

  }
}
