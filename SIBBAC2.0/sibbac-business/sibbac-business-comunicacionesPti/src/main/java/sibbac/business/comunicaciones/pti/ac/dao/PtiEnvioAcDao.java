package sibbac.business.comunicaciones.pti.ac.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.comunicaciones.pti.ac.model.PtiEnvioAc;

@Repository
public interface PtiEnvioAcDao extends JpaRepository<PtiEnvioAc, Long> {

  @Query(value = "SELECT NEXT VALUE FOR AC_PTI_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  BigDecimal getNewACPTISequence();

  List<PtiEnvioAc> findAllByCdrefmovimiento(String cdrefmovimiento);

  List<PtiEnvioAc> findAllByCdrefmovimientoeccAndCdrefnotificacionAndFliquidacion(String cdrefmovimientoecc,
      String cdrefnotificacion, Date fliquidacion);

}
