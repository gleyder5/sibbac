package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import sibbac.business.wrappers.database.model.RecordBean;

public class CuadreEccIdDTO extends RecordBean {

  private final IdentificacionOperacionCuadreEccDTO identificacionOperacionCuadreEcc;

  /**
   * 
   */
  private static final long serialVersionUID = -1458313512649691011L;

  public CuadreEccIdDTO(long id) {
    setId(id);
    identificacionOperacionCuadreEcc = null;
  }

  public CuadreEccIdDTO(String numeroOperacion,
                        Date fechaEjecucion,
                        String cdisin,
                        Character sentido,
                        String cdcamara,
                        String cdsegmento,
                        String cdoperacionmkt,
                        String cdmiembromkt,
                        String nuordenmkt,
                        String nuexemkt,
                        String numeroOperacionDcv) {
    if (StringUtils.isBlank(numeroOperacion) && StringUtils.isNotBlank(numeroOperacionDcv)) {
      numeroOperacion = numeroOperacionDcv;
    }
    this.identificacionOperacionCuadreEcc = new IdentificacionOperacionCuadreEccDTO(numeroOperacion, fechaEjecucion,
                                                                                    cdisin, sentido, cdcamara,
                                                                                    cdsegmento, cdoperacionmkt,
                                                                                    cdmiembromkt, nuordenmkt, nuexemkt);
  }

  public IdentificacionOperacionCuadreEccDTO getIdentificacionOperacionCuadreEcc() {
    return identificacionOperacionCuadreEcc;
  }

  @Override
  public String toString() {
    return "CuadreEccIdDTO [identificacionOperacionCuadreEcc=" + identificacionOperacionCuadreEcc + ", id=" + id + "]";
  }

}
