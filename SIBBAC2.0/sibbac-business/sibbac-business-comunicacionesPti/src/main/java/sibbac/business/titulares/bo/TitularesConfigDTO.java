package sibbac.business.titulares.bo;

import sibbac.common.BasicConfigDTO;

public class TitularesConfigDTO extends BasicConfigDTO {

  /**
   * 
   */
  private static final long serialVersionUID = 8446202905526686471L;

  protected String camaraNacional;
  protected String camaraEuroccp;
  protected int limiteDiasEnvioTitularesNacional;
  protected int limiteDiasEnvioTitularesEuroccp;

  public String getCamaraNacional() {
    return camaraNacional;
  }

  public String getCamaraEuroccp() {
    return camaraEuroccp;
  }

  public int getLimiteDiasEnvioTitularesNacional() {
    return limiteDiasEnvioTitularesNacional;
  }

  public int getLimiteDiasEnvioTitularesEuroccp() {
    return limiteDiasEnvioTitularesEuroccp;
  }

  public void setCamaraNacional(String camaraNacional) {
    this.camaraNacional = camaraNacional;
  }

  public void setCamaraEuroccp(String camaraEuroccp) {
    this.camaraEuroccp = camaraEuroccp;
  }

  public void setLimiteDiasEnvioTitularesNacional(int limiteDiasEnvioTitularesNacional) {
    this.limiteDiasEnvioTitularesNacional = limiteDiasEnvioTitularesNacional;
  }

  public void setLimiteDiasEnvioTitularesEuroccp(int limiteDiasEnvioTitularesEuroccp) {
    this.limiteDiasEnvioTitularesEuroccp = limiteDiasEnvioTitularesEuroccp;
  }

  @Override
  public String toString() {
    return String
        .format(
            "TitularesConfigDTO [camaraNacional=%s, camaraEuroccp=%s, limiteDiasEnvioTitularesNacional=%s, limiteDiasEnvioTitularesEuroccp=%s, getHolidays()=%s, getWorkDaysOfWeek()=%s, getAuditUser()=%s]",
            camaraNacional, camaraEuroccp, limiteDiasEnvioTitularesNacional, limiteDiasEnvioTitularesEuroccp,
            getHolidays(), getWorkDaysOfWeek(), getAuditUser());
  }

}
