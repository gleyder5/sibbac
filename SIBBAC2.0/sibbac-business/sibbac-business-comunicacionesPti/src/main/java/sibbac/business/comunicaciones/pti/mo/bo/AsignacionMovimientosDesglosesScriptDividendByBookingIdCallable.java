package sibbac.business.comunicaciones.pti.mo.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class AsignacionMovimientosDesglosesScriptDividendByBookingIdCallable implements Callable<Void> {

  @Autowired
  private AsignacionMovimientosDesglosesBo asignacionMovimientoDesgloseBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  private List<Tmct0bokId> listIds;

  private String auditUser;

  public AsignacionMovimientosDesglosesScriptDividendByBookingIdCallable(List<Tmct0bokId> ids, String auditUser) {
    this.listIds = new ArrayList<Tmct0bokId>(ids);
    this.auditUser = auditUser;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    int pos;
    int posEnd;
    List<Long> listNudesgloses;
    for (Tmct0bokId tmct0bokId : listIds) {

      listNudesgloses = dctBo.findAllIdsByNuordenAndNbookingAndTitulosDisponiblesAndDesglosesDeAlta(tmct0bokId);

      if (listNudesgloses.size() > 100) {
        pos = 0;
        while (pos < listNudesgloses.size()) {
          posEnd = Math.min(pos + 100, listNudesgloses.size());
          asignacionMovimientoDesgloseBo.asignarMovimientosEnDesglosesScriptDividendByBookingId(tmct0bokId,
              listNudesgloses.subList(pos, posEnd), auditUser);
          pos = posEnd;
        }
      }
      else {

        asignacionMovimientoDesgloseBo.asignarMovimientosEnDesglosesScriptDividendByBookingId(tmct0bokId,
            listNudesgloses, auditUser);
      }
      listNudesgloses.clear();
    }
    return null;
  }

}
