package sibbac.business.comunicaciones.ordenes.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jms.ConnectionFactory;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class RejectOrdenFidessaMultiThreadBo {

  protected static final Logger LOG = LoggerFactory.getLogger(RejectOrdenFidessaMultiThreadBo.class);

  @Autowired
  private RejectOrdenFidessaBo rejectOrdenFidessaBo;

  @Autowired
  private RejectOrdenFidessaWithUserLockBo rejectOrdenFidessaWithUserLockBo;

  public RejectOrdenFidessaMultiThreadBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true)
  public CallableResultDTO rejectBookings(List<Tmct0bokId> listBookings, String comment, String auditUser)
      throws SIBBACBusinessException {
    CallableResultDTO res = null;
    List<Future<CallableResultDTO>> listProcesosLanzados = new ArrayList<Future<CallableResultDTO>>();
    Map<Future<CallableResultDTO>, Tmct0bokId> relacionBokFuture = new HashMap<Future<CallableResultDTO>, Tmct0bokId>();
    try {
      this.lanzarProcesos(listBookings, comment, auditUser, listProcesosLanzados, relacionBokFuture);
      res = this.recuperarResultadoProcesos(listProcesosLanzados, relacionBokFuture);
    }
    catch (SIBBACBusinessException e) {
      throw e;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {
      listProcesosLanzados.clear();
      relacionBokFuture.clear();
    }

    return res;
  }

  private void lanzarProcesos(List<Tmct0bokId> listBookings, String comment, String auditUser,
      List<Future<CallableResultDTO>> listProcesosLanzados, Map<Future<CallableResultDTO>, Tmct0bokId> relacionBokFuture)
      throws SIBBACBusinessException {
    LOG.trace("[lanzarProcesos] inicio");
    ExecutorService executor;
    ConnectionFactory connectionFactory;
    Callable<CallableResultDTO> callableReject;
    Future<CallableResultDTO> proceso;

    executor = Executors.newSingleThreadExecutor();
    connectionFactory = rejectOrdenFidessaBo.getConnectionFactory();
    try {
      for (Tmct0bokId bookId : listBookings) {
        LOG.trace("[lanzarProcesos] encolando proceso para bok: {}", bookId);
        callableReject = getNewCallableRejectBooking(connectionFactory,
            rejectOrdenFidessaBo.getRejectBookingDestination(), bookId, comment, auditUser);
        proceso = executor.submit(callableReject);
        listProcesosLanzados.add(proceso);
        relacionBokFuture.put(proceso, bookId);
      }

    }
    catch (Exception e) {
      LOG.warn("[lanzarProcesos] ha ocurrido una excepcion lanzando los hilos, paramos el ejecutor: {}", executor, e);
      executor.shutdownNow();
      throw new SIBBACBusinessException(String.format("Incidencia lanzando los procesos. %s", e.getMessage()), e);
    }
    finally {
      if (!executor.isShutdown()) {
        LOG.trace("[lanzarProcesos] hemos terminado de lanzar procesos, paramos el ejecutor. {}", executor);
        executor.shutdown();
      }
      if (!executor.isTerminated()) {
        LOG.trace("[lanzarProcesos] todos los procesos no han teminado, los esperamos. {}", executor);
        try {
          executor.awaitTermination(listBookings.size() < 1000 ? 5 : 10, TimeUnit.MINUTES);
          LOG.trace("[lanzarProcesos] ha terminado el tiempo de espera, continuamos. {}", executor);
        }
        catch (InterruptedException e) {
          throw new SIBBACBusinessException(String.format("Incidencia parando el ejecutor de procesos. %s",
              e.getMessage()), e);
        }
      }
    }

    LOG.trace("[lanzarProcesos] fin");
  }

  private Callable<CallableResultDTO> getNewCallableRejectBooking(final ConnectionFactory cf, final String destination,
      final Tmct0bokId bookId, final String comment, final String auditUser) {
    Callable<CallableResultDTO> c = new Callable<CallableResultDTO>() {

      @Override
      public CallableResultDTO call() throws Exception {
        CallableResultDTO res = null;
        String msg;
        try {
          res = rejectOrdenFidessaWithUserLockBo.rejectBookingWithUserLock(cf, destination, bookId, comment, auditUser);
        }
        catch (Exception e) {
          res = new CallableResultDTO();
          msg = String.format("[%s/%s] %s", bookId.getNuorden(), bookId.getNbooking(), e.getMessage());
          res.addErrorMessage(msg);
          LOG.info("[call] {} comment: {} user:{}", msg, comment, auditUser);
          LOG.warn(msg, e);
        }
        return res;
      }
    };
    return c;
  }

  private CallableResultDTO recuperarResultadoProcesos(List<Future<CallableResultDTO>> listProcesosLanzados,
      Map<Future<CallableResultDTO>, Tmct0bokId> relacionBokFuture) throws SIBBACBusinessException {

    CallableResultDTO res = new CallableResultDTO();
    CallableResultDTO resultadoProceso;
    String msg;
    if (CollectionUtils.isNotEmpty(listProcesosLanzados)) {
      for (Future<CallableResultDTO> procesoFor : listProcesosLanzados) {
        LOG.trace("[recuperarResultadoProcesos] recuperando resultado proceso: {}", procesoFor);
        try {
          try {
            LOG.trace("[recuperarResultadoProcesos] esperaremos por cada resultado 10 segundos, pasado el tiempo daremos excepcion...");
            resultadoProceso = procesoFor.get(1, TimeUnit.SECONDS);
            if (resultadoProceso != null) {
              res.append(resultadoProceso);
            }
          }
          catch (TimeoutException e) {
            LOG.warn(e.getMessage(), e);
            if (relacionBokFuture.get(procesoFor) != null) {
              msg = String.format("%s/%s los traspasos no se han podido generar por timeout.",
                  relacionBokFuture.get(procesoFor).getNuorden(), relacionBokFuture.get(procesoFor).getNbooking());
              res.addErrorMessage(msg);
              LOG.warn("[recuperarResultadoProcesos] {}", msg);
            }
            procesoFor.cancel(true);
          }

        }
        catch (ExecutionException e) {
          LOG.warn(e.getMessage(), e);
          if (e.getCause() != null) {
            res.addErrorMessage(e.getCause().getMessage());
          }
          else {
            res.addErrorMessage(e.getMessage());
          }
        }
        catch (InterruptedException e) {
          LOG.warn("[recuperarResultadoProcesos] Se ha cancelado un proceso {}", procesoFor, e);
          LOG.warn(e.getMessage(), e);
          if (relacionBokFuture.get(procesoFor) != null) {
            msg = String.format("%s/%s El proceso encargado se ha cancelado.", relacionBokFuture.get(procesoFor)
                .getNuorden(), relacionBokFuture.get(procesoFor).getNbooking());
            res.addErrorMessage(msg);
            LOG.warn("[recuperarResultadoProcesos] {}", msg);
          }
        }
        catch (RuntimeException e) {
          LOG.warn("[recuperarResultadoProcesos] incidencia inesperado en la evaluacion del resultado del proceso {}",
              procesoFor, e);
          LOG.warn(e.getMessage(), e);
          if (relacionBokFuture.get(procesoFor) != null) {
            msg = String.format("%s/%s Error inesperado en uno de los procesos. %s", relacionBokFuture.get(procesoFor)
                .getNuorden(), relacionBokFuture.get(procesoFor).getNbooking(), e.getMessage());
            res.addErrorMessage(msg);
            LOG.warn("[recuperarResultadoProcesos] {}", msg);
          }
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }

      }
    }
    return res;
  }

}
