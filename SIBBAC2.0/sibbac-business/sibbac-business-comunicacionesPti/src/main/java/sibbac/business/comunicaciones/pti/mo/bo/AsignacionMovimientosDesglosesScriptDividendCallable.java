package sibbac.business.comunicaciones.pti.mo.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class AsignacionMovimientosDesglosesScriptDividendCallable implements Callable<Void> {

  @Autowired
  private AsignacionMovimientosDesglosesBo asignacionMovimientoDesgloseBo;

  private List<Long> listIds;

  private String auditUser;

  public AsignacionMovimientosDesglosesScriptDividendCallable(List<Long> ids, String auditUser) {
    this.listIds = new ArrayList<Long>(ids);
    this.auditUser = auditUser;
  }

  @Override
  public Void call() throws SIBBACBusinessException {
    asignacionMovimientoDesgloseBo.asignarMovimientosEnDesglosesScriptDividend(true, this.listIds, this.auditUser);
    return null;
  }

}
