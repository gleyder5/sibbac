package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.wrappers.database.model.RecordBean;

@MappedSuperclass
public class EuroCcpLineaFicheroRecordBean extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 5348569030046779201L;

  @Column(name = "ESTADO_PROCESADO", nullable = false)
  private int estadoProcesado;

  @Column(name = "RELEASE_CODE", length = 3, nullable = false)
  private String releaseCode;

  @Temporal(TemporalType.DATE)
  @Column(name = "PROCESSING_DATE", nullable = false)
  private Date processingDate;

  @Column(name = "CLEARING_SITE_CODE", length = 5, nullable = false)
  private String clearingSiteCode;

  public EuroCcpLineaFicheroRecordBean() {
    super();
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public String getReleaseCode() {
    return releaseCode;
  }

  public Date getProcessingDate() {
    return processingDate;
  }

  public String getClearingSiteCode() {
    return clearingSiteCode;
  }

  public void setReleaseCode(String releaseCode) {
    this.releaseCode = releaseCode;
  }

  public void setProcessingDate(Date processingDate) {
    this.processingDate = processingDate;
  }

  public void setClearingSiteCode(String clearingSiteCode) {
    this.clearingSiteCode = clearingSiteCode;
  }

}
