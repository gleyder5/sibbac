package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.wrappers.database.model.Tmct0gae;
import sibbac.business.wrappers.database.model.Tmct0gaeId;
import sibbac.business.wrappers.database.model.Tmct0gal;

import com.sv.sibbac.multicenter.ExecutionID;

public class XmlTmct0gae {

  private Tmct0gae tmct0gae;
  private boolean group = false;

  public XmlTmct0gae(Tmct0gal tmct0gal) {
    super();
    tmct0gae = new Tmct0gae();
    tmct0gae.setId(new Tmct0gaeId(tmct0gal.getId().getNuorden(), tmct0gal.getId().getNbooking(), tmct0gal.getId()
        .getNuagreje(), tmct0gal.getId().getNucnfclt(), null));
    tmct0gae.setTmct0gal(tmct0gal);
  }

  public XmlTmct0gae(Tmct0gal tmct0gal, Tmct0gae gae) {
    super();
    group = true;
    tmct0gae = gae;
    // tmct0gae.setTmct0gal(tmct0gal);
    // tmct0gae.setId(new Tmct0gaeId(tmct0gal.getId().getNuorden(),
    // tmct0gal.getId().getNbooking(),
    // tmct0gal.getId().getNuagreje(), tmct0gal.getId().getNucnfclt(), null));
  }

  public void xml_mapping(ExecutionID executionID) throws XmlOrdenExceptionReception {

    if (group) {
      tmct0gae.setNutitasig(tmct0gae.getNutitasig().add(
          new BigDecimal(executionID.getNumtitasig()).setScale(5, RoundingMode.HALF_UP)));
    }
    else {

      tmct0gae.getId().setNuejecuc(executionID.getNuejecuc());
      if (executionID.getNumtitasig() <= 0) {
        throw new XmlOrdenExceptionReception(tmct0gae.getId().getNuorden(), tmct0gae.getId().getNbooking(), "", "",
            String.format("Error there are one Execution Group with 0 tittles. '%s'", executionID.getNuejecuc()));
      }
      tmct0gae.setNutitasig(new BigDecimal(executionID.getNumtitasig()).setScale(5, RoundingMode.HALF_UP));

      tmct0gae.setFhaudit(new Timestamp(System.currentTimeMillis()));
      tmct0gae.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);
    }
  }

  public Tmct0gae getJDOObject() {
    return tmct0gae;
  }

}
