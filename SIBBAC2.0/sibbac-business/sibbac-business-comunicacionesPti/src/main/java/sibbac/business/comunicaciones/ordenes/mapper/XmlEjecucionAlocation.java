package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;

import com.sv.sibbac.multicenter.ExecutionID;

public class XmlEjecucionAlocation {
  protected static final Logger LOG = LoggerFactory.getLogger(XmlEjecucionAlocation.class);

  public XmlEjecucionAlocation() {
  }

  public void xml_mapping(Tmct0ejecucionalocation ejeAlo, ExecutionID executionID) {
    LOG.trace(" executionID.getNumtitasig() : {}", executionID.getNumtitasig());

    ejeAlo.setNutitulos(ejeAlo.getNutitulos().add(
        new BigDecimal(executionID.getNumtitasig()).setScale(5, RoundingMode.HALF_UP)));
    ejeAlo.setNutitulosPendientesDesglose(ejeAlo.getNutitulosPendientesDesglose().add(
        new BigDecimal(executionID.getNumtitasig()).setScale(5, RoundingMode.HALF_UP)));

  }

}
