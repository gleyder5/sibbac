package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.CASH_POSITION)
@Entity
public class EuroCcpCashPosition extends EuroCcpLineaFicheroRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -7484166819399909969L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "SUBACCOUNT_NUMBER", length = 10, nullable = false)
  private String subaccountNumber;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;

  @Column(name = "CASH_AMOUNT_IDENTIFIER", length = 8, nullable = false)
  private String cashAmountIdentifier;

  @Column(name = "CASH_POSITION_CHANGE", length = 18, scale = 2, nullable = true)
  private BigDecimal cashPositionChange;

  @Column(name = "CASH_POSITION_CHANGE_DC", length = 1, nullable = true)
  private Character cashPositionChangeDC;

  @Column(name = "CASH_POSITION_NEW", length = 18, scale = 2, nullable = true)
  private BigDecimal cashPositionNew;

  @Column(name = "CASH_POSITION_NEW_DC", length = 1, nullable = true)
  private Character cashPositionNewDC;

  @Column(name = "CASH_POSITION_DESCRIPTION", length = 40, nullable = true)
  private String cashPositionDescription;

  @Column(name = "CURRENCY_PRICE", length = 15, scale = 7, nullable = true)
  private String currencyPrice;

  @Column(name = "FILLER", length = 353, nullable = true)
  private String filler;

  public EuroCcpCashPosition() {
  }

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getSubaccountNumber() {
    return subaccountNumber;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCashAmountIdentifier() {
    return cashAmountIdentifier;
  }

  public BigDecimal getCashPositionChange() {
    return cashPositionChange;
  }

  public Character getCashPositionChangeDC() {
    return cashPositionChangeDC;
  }

  public BigDecimal getCashPositionNew() {
    return cashPositionNew;
  }

  public Character getCashPositionNewDC() {
    return cashPositionNewDC;
  }

  public String getCashPositionDescription() {
    return cashPositionDescription;
  }

  public String getCurrencyPrice() {
    return currencyPrice;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setSubaccountNumber(String subaccountNumber) {
    this.subaccountNumber = subaccountNumber;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setCashAmountIdentifier(String cashAmountIdentifier) {
    this.cashAmountIdentifier = cashAmountIdentifier;
  }

  public void setCashPositionChange(BigDecimal cashPositionChange) {
    this.cashPositionChange = cashPositionChange;
  }

  public void setCashPositionChangeDC(Character cashPositionChangeDC) {
    this.cashPositionChangeDC = cashPositionChangeDC;
  }

  public void setCashPositionNew(BigDecimal cashPositionNew) {
    this.cashPositionNew = cashPositionNew;
  }

  public void setCashPositionNewDC(Character cashPositionNewDC) {
    this.cashPositionNewDC = cashPositionNewDC;
  }

  public void setCashPositionDescription(String cashPositionDescription) {
    this.cashPositionDescription = cashPositionDescription;
  }

  public void setCurrencyPrice(String currencyPrice) {
    this.currencyPrice = currencyPrice;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }
  
  
  

}
