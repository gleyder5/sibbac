package sibbac.business.comunicaciones.pti.cuadreecc.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;

@Repository
public interface Tmct0ejecucionalocationDao extends sibbac.business.wrappers.database.dao.Tmct0ejecucionalocationDao {

  @Modifying
  @Query("UPDATE Tmct0ejecucionalocation ealo "
      + " set ealo.nutitulosPendientesDesglose = (ealo.nutitulos - (SELECT coalesce(sum(dct.imtitulos),0) "
      + " FROM Tmct0desgloseclitit dct "
      + " WHERE dct.tmct0desglosecamara.tmct0alc.id.nuorden = ealo.tmct0eje.id.nuorden "
      + " and dct.tmct0desglosecamara.tmct0alc.id.nbooking = ealo.tmct0eje.id.nbooking "
      + " and dct.nuejecuc=ealo.tmct0eje.id.nuejecuc "
      + " and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado >0 ))"
      + "WHERE ealo.tmct0eje.cdisin = :cdisin AND ealo.tmct0eje.feejecuc = :feejecuc AND ealo.tmct0eje.cdtpoper = :sentido "
      + " AND ealo.tmct0eje.clearingplatform = :camara AND ealo.tmct0eje.segmenttradingvenue = :segmento "
      + " AND ealo.tmct0eje.tpopebol = :cdoperacionmkt AND ealo.tmct0eje.cdMiembroMkt = :cdmiembromkt "
      + " AND ealo.tmct0eje.nuopemer = :nuordenmkt AND ealo.tmct0eje.nurefere = :nuexemkt "
      + " AND ealo.tmct0grupoejecucion.tmct0alo.tmct0estadoByCdestadoasig.idestado > 0")
  int updateNutitulosPendientesDesgloseByInformacionMercado(@Param("cdisin") String cdisin,
      @Param("feejecuc") Date feejecuc, @Param("sentido") char sentido, @Param("camara") String camara,
      @Param("segmento") String segmento, @Param("cdoperacionmkt") String cdoperacionmkt,
      @Param("cdmiembromkt") String cdmiembromkt, @Param("nuordenmkt") String nuordenmkt,
      @Param("nuexemkt") String nuexemkt);

  @Query("SELECT ealo FROM Tmct0ejecucionalocation ealo "
      + "WHERE ealo.tmct0eje.cdisin = :cdisin AND ealo.tmct0eje.feejecuc = :feejecuc AND ealo.tmct0eje.cdtpoper = :sentido "
      + " AND ealo.tmct0eje.clearingplatform = :camara AND ealo.tmct0eje.segmenttradingvenue = :segmento "
      + " AND ealo.tmct0eje.tpopebol = :cdoperacionmkt AND ealo.tmct0eje.cdMiembroMkt = :cdmiembromkt "
      + " AND ealo.tmct0eje.nuopemer = :nuordenmkt AND ealo.tmct0eje.nurefere = :nuexemkt "
      + " AND ealo.tmct0grupoejecucion.tmct0alo.tmct0estadoByCdestadoasig.idestado > 0 "
      + " and  ealo.nutitulosPendientesDesglose > 0")
  List<Tmct0ejecucionalocation> findAllByDatosMercadoAndNutitulosPendientesDesgloseDisponibles(
      @Param("cdisin") String cdisin, @Param("feejecuc") Date feejecuc, @Param("sentido") char sentido,
      @Param("camara") String camara, @Param("segmento") String segmento,
      @Param("cdoperacionmkt") String cdoperacionmkt, @Param("cdmiembromkt") String cdmiembromkt,
      @Param("nuordenmkt") String nuordenmkt, @Param("nuexemkt") String nuexemkt, Pageable page);

}
