package sibbac.business.desglose.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.desglose.bo.DesgloseMultithread;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_DESGLOSE_AUTOMATICO, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-23 ? * MON-FRI")
public class TaskDesgloseAutomatico extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskDesgloseAutomatico.class);

  @Autowired
  private DesgloseMultithread desgloseAutomatico;

  public TaskDesgloseAutomatico() {
  }

  @Override
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio.");
    desgloseAutomatico.desgloseAutomatico(false);
    LOG.info("[executeTask] fin.");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_DESGLOSE_AUTOMATICO;
  }

}
