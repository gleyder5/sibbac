package sibbac.business.comunicaciones.imputacions3.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class ImputacionS3Callable implements Callable<BigDecimal> {

  private final ImputacionS3ConfigDTO config;
  private final List<String> listCdoperacionecc;

  @Autowired
  private ImputacionS3Bo imputacionBo;

  public ImputacionS3Callable(ImputacionS3ConfigDTO config, List<String> cdoperacionEcc) {
    super();
    this.config = config;
    this.listCdoperacionecc = new ArrayList<String>();

    this.listCdoperacionecc.addAll(cdoperacionEcc);
  }

  @Override
  public BigDecimal call() throws Exception {
    return imputacionBo.crearImputacionesS3(listCdoperacionecc, config);
  }

}
