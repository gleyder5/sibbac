package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0monDao;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0mon;

@Service
public class XmlOrdenHelper {

  @Autowired
  private Tmct0monDao monedaDao;

  private BigDecimal calculateGrossCenter(String cdmoniso,
                                          Character cdtpoper,
                                          Character cdindgas,
                                          BigDecimal imbrmerc,
                                          BigDecimal imgatdvs,
                                          BigDecimal imimpdvs,
                                          BigDecimal imfibrdv,
                                          BigDecimal imgasliqdv,
                                          BigDecimal imgasgesdv) throws NullPointerException {
    BigDecimal imnetobr = new BigDecimal(0);
    Tmct0mon mon = Objects.requireNonNull(monedaDao.findOne(cdmoniso),
                                          String.format("Currency not found: %s in tmct0mon.", cdmoniso));
    Integer precision = Objects.requireNonNull(mon.getCdnumdec(),
                                               String.format("Currency precision NULL VALUE : %s in tmct0mon.",
                                                             cdmoniso));
    if ('C' == cdtpoper) {
      if ('S' == cdindgas) {
        imnetobr = imbrmerc.add(imfibrdv).add(imgatdvs).add(imimpdvs).add(imgasliqdv).add(imgasgesdv)
                           .setScale(precision, RoundingMode.HALF_UP);
      } else {
        imnetobr = imbrmerc.add(imfibrdv).setScale(precision, RoundingMode.HALF_UP);
      }
    } else {
      if ('S' == cdindgas) {
        imnetobr = imbrmerc.subtract(imgatdvs).subtract(imimpdvs).subtract(imfibrdv).subtract(imgasliqdv)
                           .subtract(imgasgesdv).setScale(precision, RoundingMode.HALF_UP);
      } else {
        imnetobr = imbrmerc.subtract(imfibrdv).setScale(precision, RoundingMode.HALF_UP);
      }
    }
    return imnetobr;
  }

  public BigDecimal calculateGrossCenter(Tmct0gal gal, Character cdindgas) {
    return calculateGrossCenter(gal.getCdmoniso(), gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdtpoper(),
                                cdindgas, gal.getImbrmerc(), gal.getImgatdvs(), gal.getImimpdvs(), gal.getImfibrdv(),
                                gal.getImgasliqdv(), gal.getImgasgesdv());
  }

  public BigDecimal calculateGrossCenter(Tmct0cta cta) {
    return calculateGrossCenter(cta.getCdmoniso(), cta.getCdtpoper(), cta.getCdindgas(), cta.getImbrmerc(),
                                cta.getImgatdvs(), cta.getImimpdvs(), cta.getImfibrdv(), cta.getImgasliqdv(),
                                cta.getImgasgesdv());
  }

  public BigDecimal calculateGrossCenter(Tmct0ctg ctg) {
    return calculateGrossCenter(ctg.getCdmoniso(), ctg.getCdtpoper(), ctg.getCdindgas(), ctg.getImbrmerc(),
                                ctg.getImgatdvs(), ctg.getImimpdvs(), ctg.getImfibrdv(), ctg.getImgasliqdv(),
                                ctg.getImgasgesdv());
  }

  public BigDecimal calculateGrossCenterEur(Tmct0ctg ctg) {
    return calculateGrossCenter("EUR", ctg.getCdtpoper(), ctg.getCdindgas(), ctg.getImbrmreu(), ctg.getImgastos(),
                                ctg.getImimpeur(), ctg.getImfibreu(), ctg.getImgasliqeu(), ctg.getImgasgeseu());
  }

}
