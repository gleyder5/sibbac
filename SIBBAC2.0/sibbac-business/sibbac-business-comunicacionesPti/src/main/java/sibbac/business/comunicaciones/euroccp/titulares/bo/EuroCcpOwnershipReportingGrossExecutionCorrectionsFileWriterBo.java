package sibbac.business.comunicaciones.euroccp.titulares.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;

@Service(value = "euroCcpOwnershipReportingGrossExecutionCorrectionsFileWriterBo")
public class EuroCcpOwnershipReportingGrossExecutionCorrectionsFileWriterBo extends
                                                                           EuroCcpOwnershipReportingGrossExecutionsFileWriterBo {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpOwnershipReportingGrossExecutionCorrectionsFileWriterBo.class);

  @Override
  protected EuroCcpFileTypeEnum getPrincipalFileType() {
    return EuroCcpFileTypeEnum.CRG;
  }

  @Override
  protected EuroCcpFileTypeEnum getTmpFileType() {
    return EuroCcpFileTypeEnum.CRG_TMP;
  }

  @Override
  protected boolean getWriteFooterPrincipalFile() {
    return true;
  }

  @Override
  public void updateEstadoProcesado(EuroCcpFileSent principalFile, int estadoProcesado, String auditUser) {
    // TODO Auto-generated method stub

  }
}
