package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.CodigoErrorValidacionCuadreEcc;
import sibbac.business.comunicaciones.pti.commons.enums.CodigoMiembroCompensadorSV;
import sibbac.business.comunicaciones.pti.commons.enums.TipoOperacionTraspasoTitulos;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccEjecucionDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionCuadreEccDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccLog;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.Header;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R00;
import sibbac.pti.messages.an.R01;
import sibbac.pti.messages.an.R02;
import sibbac.pti.messages.an.R05;
import sibbac.pti.messages.an.R06;
import sibbac.pti.messages.an.R07;

@Service
public class CuadreEccEjecucionBo extends AbstractBo<CuadreEccEjecucion, Long, CuadreEccEjecucionDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccEjecucionBo.class);

  @Value("${sibbac.cuadreecc.revision.titulos.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadreecc.revision.titulos.subthread.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccEjecucionSubListBo cuadreEccEjecucionSubListBo;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movOpNuevaBo;

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0xasBo xasBo;

  @Value("${sibbac.pti.cuadreecc.idecc:MEFFESBBCRV}")
  private String idEccCuadreEcc;

  public CuadreEccEjecucionBo() {
    super();
  }

  /**
   * @param estadoProcesado
   * @param fechaLiqLte
   * @return
   */
  private List<CuadreEccIdDTO> findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiqLte(
      int estadoProcesado, Date fechaLiqLte, int maxRegs) {
    List<CuadreEccIdDTO> res = new ArrayList<CuadreEccIdDTO>();
    List<CuadreEccIdDTO> resAux;
    List<Date> fechasLiquidacion = dao.findDistinctFechaLiqByEstadoProcesado(estadoProcesado);
    Pageable page = new PageRequest(0, maxRegs);
    if (CollectionUtils.isNotEmpty(fechasLiquidacion)) {
      String cdoperacionMktSinEcc = cfgBo.getTpopebolsSinEcc();
      boolean revisarSinEcc = StringUtils.isNotBlank(cdoperacionMktSinEcc);
      for (Date fechaLiq : fechasLiquidacion) {
        if (fechaLiq.compareTo(fechaLiqLte) <= 0) {
          LOG.debug(
              "[CuadreEccEjecucionBo :: findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiqLte] Buscando ejecuciones de fecha Liq.: {}",
              fechaLiq);
          resAux = dao.findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiq(estadoProcesado, fechaLiq,
              page);

          LOG.debug(
              "[CuadreEccEjecucionBo :: findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiqLte] Encontradas {} ejecuciones de fecha Liq.: {}",
              resAux.size(), fechaLiq);
          if (revisarSinEcc) {
            for (CuadreEccIdDTO dto : resAux) {
              if (StringUtils.isNotBlank(dto.getIdentificacionOperacionCuadreEcc().getCdoperacionmkt())) {
                dto.getIdentificacionOperacionCuadreEcc().setSinEcc(
                    cdoperacionMktSinEcc.contains(dto.getIdentificacionOperacionCuadreEcc().getCdoperacionmkt()));
              }
            }
          }
          if (CollectionUtils.isNotEmpty(resAux)) {

            res.addAll(resAux);
            if (res.size() >= maxRegs) {
              break;
            }
          }
        }
      }
    }
    return res;
  }

  public List<CuadreEccIdDTO> findIdByEstadoProcesadoAndFechaLiq(int estadoProcesado, Date fechaLiqLte, int maxRegs) {
    List<CuadreEccIdDTO> res = new ArrayList<CuadreEccIdDTO>();
    List<CuadreEccIdDTO> resAux;
    List<Date> fechasLiquidacion = dao.findDistinctFechaLiqByEstadoProcesado(estadoProcesado);
    Pageable page = new PageRequest(0, maxRegs);
    if (CollectionUtils.isNotEmpty(fechasLiquidacion)) {
      for (Date fechaLiq : fechasLiquidacion) {
        if (fechaLiq.compareTo(fechaLiqLte) <= 0) {
          LOG.debug(
              "[CuadreEccEjecucionBo :: findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiqLte] Buscando ejecuciones de fecha Liq.: {}",
              fechaLiq);
          resAux = dao.findIdByEstadoProcesadoAndFechaLiq(estadoProcesado, fechaLiq, page);

          LOG.debug(
              "[CuadreEccEjecucionBo :: findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiqLte] Encontradas {} ejecuciones de fecha Liq.: {}",
              resAux.size(), fechaLiq);
          if (CollectionUtils.isNotEmpty(resAux)) {

            res.addAll(resAux);
            if (res.size() >= maxRegs) {
              break;
            }
          }
        }
      }
    }
    return res;
  }

  /**
   * @return
   */
  public List<CuadreEccIdDTO> findIdentificacionOperacionCuadreEccDTOForRevisionTitulos(int maxRegs) {
    return findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiqLte(
        EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId(), new Date(), maxRegs);
  }

  /**
   * @return CuadreEccValidacionTitulosReferenciasFicheroEccDbReader
   */
  public List<CuadreEccIdDTO> findIdentificacionOperacionCuadreEccDTOForValidacionTitulosReferenciasFicheroEcc(
      int maxRegs) {

    return findIdByEstadoProcesadoAndFechaLiq(
        EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC.getId(), new Date(),
        maxRegs);
    // return findIdByEstadoProcesadoAndFechaLiq(2120, new Date(), maxRegs);
  }

  public CuadreEccEjecucion findByNumOpAndIdEcc(String numOp, String idEcc) {
    return dao.findByNumOpAndIdEcc(numOp, idEcc);
  }

  public List<CuadreEccEjecucion> findByNumOpInicialAndIdEcc(String numOpInicial, String idEcc) {
    return dao.findByNumOpInicialAndIdEcc(numOpInicial, idEcc);
  }

  public CuadreEccEjecucion findByIdEccAndNumOpDcvAndFechaNegAndSentido(String idEcc, String numOpDcv, Date fechaNeg,
      Character sentido) {
    return dao.findByIdEccAndNumOpDcvAndFechaNegAndSentido(idEcc, numOpDcv, fechaNeg, sentido);
  }

  public CuadreEccEjecucion findByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNeg(
      Date FechaNeg, String idEcc, String isin, String miembroMercadoNeg, Character sentido, String segmentoNeg,
      String codOpMercado, String numOrdenMercado, String numEjeMercado, String plataformaNeg) {
    return dao
        .findByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNeg(
            FechaNeg, idEcc, isin, miembroMercadoNeg, sentido, segmentoNeg, codOpMercado, numOrdenMercado,
            numEjeMercado, plataformaNeg);

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public CuadreEccEjecucion saveCuadreEccEjecucion(AN an) throws SIBBACBusinessException, PTIMessageException {
    return cuadreEccEjecucionSubListBo.saveCuadreEccEjecucion(an);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void saveEjecucionesFromPtiEccFile(Path file) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccEjecucionBo :: saveEjecucionesFromPtiEccFile] inicio.");
    try {
      LOG.debug("[CuadreEccEjecucionBo :: saveEjecucionesFromPtiEccFile] leyendo fichero{}.", file.toString());
      if (file.toString().toUpperCase().endsWith(".GZ")) {
        try (InputStream is = new GZIPInputStream(Files.newInputStream(file, StandardOpenOption.READ));
            BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1));) {
          this.saveEjecucionesFromPtiEccFileBufferedReader(br);
        }
      }
      else {
        try (InputStream is = Files.newInputStream(file, StandardOpenOption.READ);
            BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.ISO_8859_1));) {
          this.saveEjecucionesFromPtiEccFileBufferedReader(br);
        }
      }

    }
    catch (IOException e) {
      LOG.warn(e.getMessage(), e);
    }
    catch (PTIMessageException e) {
      LOG.warn(e.getMessage(), e);
    }
    LOG.debug("[CuadreEccEjecucionBo :: saveEjecucionesFromPtiEccFile] final.");
  }

  private void saveEjecucionesFromPtiEccFileBufferedReader(BufferedReader br) throws IOException, PTIMessageException,
      SIBBACBusinessException {
    String linea;
    List<AN> ans = new ArrayList<AN>();
    int countAns = 0;
    if (br != null) {
      while ((linea = br.readLine()) != null) {
        Header head = (Header) PTIMessageFactory.parseHeader(linea);
        PTIMessage msg = PTIMessageFactory.parse(cfgBo.getPtiMessageVersionFromTmct0cfg(head.getFechaDeEnvio()), linea);
        if (msg != null && msg.getTipo() == PTIMessageType.AN) {
          countAns++;
          LOG.trace("[CuadreEccEjecucionBo :: saveEjecucionesFromPtiEccFile] Encontrado AN: {}.", msg);
          AN an = (AN) msg;
          ans.add(an);
          if (ans.size() > 100) {
            LOG.trace("[CuadreEccEjecucionBo :: saveEjecucionesFromPtiEccFile] Encontrados {} AN's.", countAns);
            cuadreEccEjecucionSubListBo.saveEjecucionesFromPtiEccFile(ans);
            ans.clear();
          }
        }
      }
    }
    if (ans.size() > 0) {
      LOG.trace("[CuadreEccEjecucionBo :: saveEjecucionesFromPtiEccFile] Encontrados {} AN's.", countAns);
      cuadreEccEjecucionSubListBo.saveEjecucionesFromPtiEccFile(ans);
      ans.clear();
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void revisarTitulosEjecuciones(List<CuadreEccIdDTO> ids, int pageSize, int maxRegsInSimpleThread) {
    LOG.trace("[CuadreEccEjecucionBo :: revisarTitulosEjecuciones] inicio.");
    for (CuadreEccIdDTO id : ids) {
      revisarTitulosEjecucion(id, pageSize, maxRegsInSimpleThread);
    }
    LOG.trace("[CuadreEccEjecucionBo :: revisarTitulosEjecuciones] final.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void revisarTitulosEjecucion(CuadreEccIdDTO id, int pageSize, int maxRegsInSimpleThread) {
    LOG.trace("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] inicio.");

    LOG.debug("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] procesando id: {}.", id);
    try {
      List<CuadreEccEjecucion> ejes = new ArrayList<CuadreEccEjecucion>();

      List<CuadreEccSibbac> desgloses = new ArrayList<CuadreEccSibbac>();

      // 1.- Grabar por codigo de operacion mercado
      // 2.- Repasar los desgloses creados y ver si coinciden los codigos de
      // operacion ecc
      // 3.- En el caso de que un desglose se relacione con varias ejecuciones
      // debo relacionarlo con la tabla
      // tmct0_cuadre_ecc_log

      if (id.getId() != null) {
        CuadreEccEjecucion eje = dao.findOne(id.getId());

        ejes.add(eje);
      }
      else {
        ejes.addAll(findByIdentificacionNumOpInicial(id.getIdentificacionOperacionCuadreEcc()));
      }
      BigDecimal acumulado = BigDecimal.ZERO;
      for (CuadreEccEjecucion eje : ejes) {
        acumulado = acumulado.add(eje.getValoresImpNominal());

      }
      for (CuadreEccEjecucion eje : ejes) {
        if (eje.getValoresImpNominalEje() == null) {
          eje.setValoresImpNominalEje(acumulado);
          eje = save(eje);
        }
      }

      if (CollectionUtils.isNotEmpty(ejes)) {
        this.deleteAllLogsPreviosEjecucion(ejes);
        this.devolverTitulosCuadre(ejes);
        this.quitarEjecucionesInservibles(ejes);

        if (CollectionUtils.isNotEmpty(ejes)) {
          Map<Long, CuadreEccEjecucion> ejecucionesById = new HashMap<Long, CuadreEccEjecucion>();
          Map<String, CuadreEccEjecucion> ejecucionesByNumOp = new HashMap<String, CuadreEccEjecucion>();
          Map<String, List<CuadreEccEjecucion>> ejecucionesByNumOpPrev = new HashMap<String, List<CuadreEccEjecucion>>();
          this.organizarInformacion(ejes, ejecucionesById, ejecucionesByNumOp, ejecucionesByNumOpPrev);

          LOG.debug("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] ");

          CuadreEccEjecucion ejeModelo = ejes.get(0);
          IdentificacionOperacionCuadreEccDTO identificacionModelo = getIdentificacionOperacionCuadreEccDTO(ejeModelo);

          deletePreviousDataByCuadreEccEjecucion(ejeModelo, false, true, true, maxRegsInSimpleThread);

          desgloses = cuadreEccSibbacBo.crearCuadreEccSibbacByInformacionMercado(ejeModelo, identificacionModelo,
              pageSize, maxRegsInSimpleThread);

          if (CollectionUtils.isEmpty(desgloses)) {
            this.errorNoEncontradosDesgloses(ejes, desgloses);

          }
          else {

            desgloses = cuadreEccSibbacBo.save(desgloses);

            if (identificacionModelo.isSinEcc()) {
              LOG.debug("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] SIN ECC");

              this.repartirTitulosEjecucionesEnDesglosesSinEcc(ejes, desgloses, ejecucionesById, ejecucionesByNumOp,
                  ejecucionesByNumOpPrev);

            }
            else {
              LOG.debug("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] CON ECC");

              // buscamos los codigos de op ecc que coinciden totalmente
              this.repartirTitulosEjecucionesEnDesglosesConEcc(ejes, desgloses, ejecucionesById, ejecucionesByNumOp,
                  ejecucionesByNumOpPrev);
            }// fin con ecc

            this.validarTitulosEjecuciones(ejes, desgloses, ejecucionesById, ejecucionesByNumOp, ejecucionesByNumOpPrev);
            this.validarTitulosDesgloses(ejes, desgloses, ejecucionesById, ejecucionesByNumOp, ejecucionesByNumOpPrev);
            ejes = save(ejes);
            desgloses = cuadreEccSibbacBo.save(desgloses);
          }
          int countUpdates;
          for (CuadreEccEjecucion eje : ejes) {
            // countUpdates =
            // cuadreEccReferenciaBo.updateEstadoProcesadoByIdCuadreEccEjecucion(eje.getEstadoProcesado(),
            // eje.getId());
            countUpdates = cuadreEccReferenciaBo.updateEstadoProcesadoByIdCuadreEccEjecucionAndNoTocadas(
                eje.getEstadoProcesado(), eje.getId());
            LOG.debug("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] Actualizadas {} referencias a estado {}",
                countUpdates, eje.getEstadoProcesado());
          }

          ejecucionesById.clear();
          ejecucionesByNumOp.clear();
          ejecucionesByNumOpPrev.clear();
        }// fin despues quitar ejes inervibles
      }
    }
    catch (Exception e) {
      LOG.warn("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] INCIDENCIA {}.", id, e);
    }
    LOG.trace("[CuadreEccEjecucionBo :: revisarTitulosEjecucion] final.");
  }

  /**
   * @param ejes
   * @param desgloses
   */
  private void errorNoEncontradosDesgloses(List<CuadreEccEjecucion> ejes, List<CuadreEccSibbac> desgloses) {
    LOG.trace("[CuadreEccEjecucionBo :: errorNoEncontradosDesgloses] inicio");
    for (CuadreEccEjecucion eje : ejes) {
      String msg = MessageFormat.format("INCIDENCIA-Numero operacion no encontrado {0}", eje.getNumOp());
      grabarLogs(eje, desgloses, CodigoErrorValidacionCuadreEcc.TITULOS_NUEMERO_OPERACION_NO_ENCONTRADO.text, msg,
          "CECCTITULOS");
      eje.setAuditDate(new Date());
      eje.setAuditUser("CECCTITULOS");
      eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_ERRONEO.getId());
    }
    LOG.trace("[CuadreEccEjecucionBo :: errorNoEncontradosDesgloses] fin");
  }

  /**
   * @param ejes
   */
  private void deleteAllLogsPreviosEjecucion(List<CuadreEccEjecucion> ejes) {
    LOG.trace("[CuadreEccEjecucionBo :: deleteAllLogsPreviosEjecucion] inicio");
    for (CuadreEccEjecucion eje : ejes) {
      LOG.debug("[CuadreEccEjecucionBo :: deleteAllLogsPreviosEjecucion] {}", eje);
      deleteLogsPrevios(eje.getId(), null);
    }
    LOG.trace("[CuadreEccEjecucionBo :: deleteAllLogsPreviosEjecucion] fin");
  }

  /**
   * @param ejes
   */
  private void devolverTitulosCuadre(List<CuadreEccEjecucion> ejes) {
    LOG.trace("[CuadreEccEjecucionBo :: devolverTitulosCuadre] inicio");
    for (CuadreEccEjecucion eje : ejes) {
      LOG.trace("[CuadreEccEjecucionBo :: devolverTitulosCuadre] {}", eje);
      eje.setValoresImpNominalPendienteCuadre(eje.getValoresImpNominal());
    }// fin for delete logs viejos de ejecucion
    LOG.trace("[CuadreEccEjecucionBo :: devolverTitulosCuadre] fin");
  }

  /**
   * @param ejes
   */
  private void quitarEjecucionesInservibles(List<CuadreEccEjecucion> ejes) {
    LOG.trace("[CuadreEccEjecucionBo :: quitarEjecucionesInservibles] inicio");
    List<CuadreEccEjecucion> ejesToRemove = new ArrayList<CuadreEccEjecucion>();
    for (CuadreEccEjecucion eje : ejes) {
      LOG.trace("[CuadreEccEjecucionBo :: quitarEjecucionesInservibles] {}", eje);
      if (BigDecimal.ZERO.compareTo(eje.getValoresImpNominalPendienteCuadre()) == 0) {
        ejesToRemove.add(eje);
        LOG.warn("[CuadreEccEjecucionBo :: quitarEjecucionesInservibles] Ejecucion sin titulos pdte de cuadre. {}", eje);
      }
      else if (StringUtils.isBlank(eje.getNumOp()) && StringUtils.isBlank(eje.getNumOpDcv())) {
        ejesToRemove.add(eje);
        LOG.warn("[CuadreEccEjecucionBo :: quitarEjecucionesInservibles] Ejecucion sin codigos de op. {}", eje);
      }
      else if (eje.getValoresImpNominalPendienteCuadre() == null) {
        ejesToRemove.add(eje);
        LOG.warn("[CuadreEccEjecucionBo :: quitarEjecucionesInservibles] Ejecucion con titulos a NUll. {}", eje);
      }

    }// fin for delete logs viejos de ejecucion
    ejes.removeAll(ejesToRemove);
    LOG.trace("[CuadreEccEjecucionBo :: quitarEjecucionesInservibles] fin");
  }

  /**
   * @param ejes
   * @param ejecucionesById
   * @param ejecucionesByNumOp
   * @param ejecucionesByNumOpPrev
   */
  private void organizarInformacion(List<CuadreEccEjecucion> ejes, Map<Long, CuadreEccEjecucion> ejecucionesById,
      Map<String, CuadreEccEjecucion> ejecucionesByNumOp, Map<String, List<CuadreEccEjecucion>> ejecucionesByNumOpPrev) {
    LOG.trace("[CuadreEccEjecucionBo :: organizarInformacion] inicio");
    for (CuadreEccEjecucion eje : ejes) {
      LOG.trace("[CuadreEccEjecucionBo :: organizarInformacion] {}", eje);
      deleteLogsPrevios(eje.getId(), null);

      ejecucionesById.put(eje.getId(), eje);
      if (StringUtils.isNotBlank(eje.getNumOp())) {
        ejecucionesByNumOp.put(eje.getNumOp(), eje);
        if (ejecucionesByNumOpPrev.get(eje.getNumOpPrevia()) == null) {
          ejecucionesByNumOpPrev.put(eje.getNumOpPrevia(), new ArrayList<CuadreEccEjecucion>());
        }
        ejecucionesByNumOpPrev.get(eje.getNumOpPrevia()).add(eje);
      }

    }// fin for delete logs viejos de ejecucion
    LOG.trace("[CuadreEccEjecucionBo :: organizarInformacion] fin");
  }

  /**
   * @param ejes
   * @param desgloses
   * @param ejecucionesById
   * @param ejecucionesByNumOp
   * @param ejecucionesByNumOpPrev
   */
  private void repartirTitulosEjecucionesEnDesglosesSinEcc(List<CuadreEccEjecucion> ejes,
      List<CuadreEccSibbac> desgloses, Map<Long, CuadreEccEjecucion> ejecucionesById,
      Map<String, CuadreEccEjecucion> ejecucionesByNumOp, Map<String, List<CuadreEccEjecucion>> ejecucionesByNumOpPrev) {
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesSinEcc] inicio");
    for (CuadreEccEjecucion eje : ejes) {
      if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) > 0) {
        for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
          if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) > 0
              && eje.getValoresImpNominalPendienteCuadre().compareTo(cuadreEccSibbac.getNumTitulosDesSibbacPdte()) >= 0) {
            cuadreEccSibbac.setIdCuadreEccEjecucion(eje.getId());
            eje.setValoresImpNominalPendienteCuadre(eje.getValoresImpNominalPendienteCuadre().subtract(
                cuadreEccSibbac.getNumTitulosDesSibbacPdte()));
            cuadreEccSibbac.setNumTitulosDesSibbacPdte(BigDecimal.ZERO);
            cuadreEccSibbac
                .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
                    .getId());

            cuadreEccSibbac.setAuditDate(new Date());
            cuadreEccSibbac.setAuditUser("CECCTITULOS");

            // eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES.getId());
            // eje.setAuditDate(new Date());
            // eje.setAuditUser("CECCTITULOS");
          }
        }
      }
    }
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesSinEcc] fin");
  }

  /**
   * @param ejes
   * @param desgloses
   * @param ejecucionesById
   * @param ejecucionesByNumOp
   * @param ejecucionesByNumOpPrev
   */
  private void repartirTitulosEjecucionesEnDesglosesConEcc(List<CuadreEccEjecucion> ejes,
      List<CuadreEccSibbac> desgloses, Map<Long, CuadreEccEjecucion> ejecucionesById,
      Map<String, CuadreEccEjecucion> ejecucionesByNumOp, Map<String, List<CuadreEccEjecucion>> ejecucionesByNumOpPrev) {
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] inicio");

    // buscamos los codigos de op ecc que coinciden totalmente
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] 1.- Buscamos cod op igual y titulos iguales.");
    for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
      if (cuadreEccSibbac.getNumOp() != null
          && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) > 0) {

        CuadreEccEjecucion ejeAux = ejecucionesByNumOp.get(cuadreEccSibbac.getNumOp());
        if (ejeAux != null
            && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(ejeAux.getValoresImpNominalPendienteCuadre()) == 0) {
          LOG.debug("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] desglose y ejecucion coinciden.");
          cuadreEccSibbac.setIdCuadreEccEjecucion(ejeAux.getId());
          cuadreEccSibbac.setNumTitulosDesSibbacPdte(BigDecimal.ZERO);
          ejeAux.setValoresImpNominalPendienteCuadre(BigDecimal.ZERO);
          cuadreEccSibbac
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
                  .getId());
          cuadreEccSibbac.setAuditDate(new Date());
          cuadreEccSibbac.setAuditUser("CECCTITULOS");

          ejeAux
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
                  .getId());
          ejeAux.setAuditDate(new Date());
          ejeAux.setAuditUser("CECCTITULOS");
        }
        else {
          cuadreEccSibbac.setIdCuadreEccEjecucion(null);
        }
      }
    }

    // buscamos los codigos de op con igual codigo y distintos titulos
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] 2.- Buscamos cod op igual y titulos eje > titulos desglose.");
    for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
      if (cuadreEccSibbac.getNumOp() != null
          && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) > 0) {

        CuadreEccEjecucion ejeAux = ejecucionesByNumOp.get(cuadreEccSibbac.getNumOp());
        if (ejeAux != null
            && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(ejeAux.getValoresImpNominalPendienteCuadre()) <= 0) {
          LOG.debug("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] desglose y ejecucion tienen el mismo codigo ecc pero la ejeuccion tiene mas titulos.");
          cuadreEccSibbac.setIdCuadreEccEjecucion(ejeAux.getId());
          ejeAux.setValoresImpNominalPendienteCuadre(ejeAux.getValoresImpNominalPendienteCuadre().subtract(
              cuadreEccSibbac.getNumTitulosDesSibbacPdte()));
          cuadreEccSibbac.setNumTitulosDesSibbacPdte(BigDecimal.ZERO);
          cuadreEccSibbac
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
                  .getId());
          cuadreEccSibbac.setAuditDate(new Date());
          cuadreEccSibbac.setAuditUser("CECCTITULOS");
        }
        else {
          cuadreEccSibbac.setIdCuadreEccEjecucion(null);
        }
      }
    }

    // num op igual que el previo y titulos iguales
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] 3.- Buscamos cod op previo eje = cod op desglose y titulos eje e igual titulos.");
    for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
      if (cuadreEccSibbac.getNumOp() != null
          && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) > 0) {

        List<CuadreEccEjecucion> ejesAux = ejecucionesByNumOpPrev.get(cuadreEccSibbac.getNumOp());
        if (CollectionUtils.isNotEmpty(ejesAux)) {
          for (CuadreEccEjecucion eje : ejesAux) {
            if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) > 0
                && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(eje.getValoresImpNominalPendienteCuadre()) == 0) {
              LOG.warn("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] el desglose tiene el numero de op previa de la ejecucion.");
              cuadreEccSibbac.setIdCuadreEccEjecucion(eje.getId());

              CuadreEccLog log = new CuadreEccLog();
              log.setEstado(0);
              log.setAuditDate(new Date());
              log.setAuditUser("CECCTITULOS");
              log.setIdCuadreEccEjecucion(eje.getId());
              log.setIdCuadreEccSibbac(cuadreEccSibbac.getId());
              log.setCodError(CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text);
              log.setTitulos(cuadreEccSibbac.getNumTitulosDesSibbacPdte());

              eje.setValoresImpNominalPendienteCuadre(eje.getValoresImpNominalPendienteCuadre().subtract(
                  cuadreEccSibbac.getNumTitulosDesSibbacPdte()));

              cuadreEccSibbac.setNumTitulosDesSibbacPdte(BigDecimal.ZERO);
              log = cuadreEccLogDao.save(log);
              cuadreEccSibbac.getCuadreEccLogs().add(log);

              cuadreEccSibbac
                  .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                      .getId());
              cuadreEccSibbac.setAuditDate(new Date());
              cuadreEccSibbac.setAuditUser("CECCTITULOS");
              break;
            }
            else {
              cuadreEccSibbac.setIdCuadreEccEjecucion(null);
            }
          }
        }
      }
    }// fin for cuadre ecc sibbac

    // num op igual que el previo y titulos distintos
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] 4.- Buscamos cod op previo eje = cod op desglose y titulos eje > titulos desglose.");
    for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
      if (cuadreEccSibbac.getNumOp() != null
          && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) > 0) {

        List<CuadreEccEjecucion> ejesAux = ejecucionesByNumOpPrev.get(cuadreEccSibbac.getNumOp());
        if (CollectionUtils.isNotEmpty(ejesAux)) {
          for (CuadreEccEjecucion eje : ejesAux) {
            if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) > 0
                && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(eje.getValoresImpNominalPendienteCuadre()) <= 0) {
              LOG.warn("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] el desglose tiene el numero de op previa de la ejecucion.");
              cuadreEccSibbac.setIdCuadreEccEjecucion(eje.getId());

              CuadreEccLog log = new CuadreEccLog();
              log.setEstado(0);
              log.setAuditDate(new Date());
              log.setAuditUser("CECCTITULOS");
              log.setIdCuadreEccEjecucion(eje.getId());
              log.setIdCuadreEccSibbac(cuadreEccSibbac.getId());
              log.setCodError(CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text);
              log.setTitulos(cuadreEccSibbac.getNumTitulosDesSibbacPdte());

              eje.setValoresImpNominalPendienteCuadre(eje.getValoresImpNominalPendienteCuadre().subtract(
                  cuadreEccSibbac.getNumTitulosDesSibbacPdte()));

              cuadreEccSibbac.setNumTitulosDesSibbacPdte(BigDecimal.ZERO);
              log = cuadreEccLogDao.save(log);
              cuadreEccSibbac.getCuadreEccLogs().add(log);

              cuadreEccSibbac
                  .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                      .getId());
              cuadreEccSibbac.setAuditDate(new Date());
              cuadreEccSibbac.setAuditUser("CECCTITULOS");
              break;
            }
            else {
              cuadreEccSibbac.setIdCuadreEccEjecucion(null);
            }
          }
        }
      }
    }// fin for cuadre ecc sibbac
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] 5.- Reparto de las ejecuciones con titulos iguales que los desgloses.");
    for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
      if (cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) > 0) {
        for (CuadreEccEjecucion eje : ejes) {

          if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) > 0
              && cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(eje.getValoresImpNominalPendienteCuadre()) == 0) {
            BigDecimal titulos = cuadreEccSibbac.getNumTitulosDesSibbacPdte();

            cuadreEccSibbac.setIdCuadreEccEjecucion(null);
            CuadreEccLog log = new CuadreEccLog();
            log.setEstado(0);
            log.setAuditDate(new Date());
            log.setAuditUser("CECCTITULOS");
            log.setIdCuadreEccEjecucion(eje.getId());
            log.setIdCuadreEccSibbac(cuadreEccSibbac.getId());
            log.setCodError(CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text);
            log.setTitulos(titulos);

            eje.setValoresImpNominalPendienteCuadre(eje.getValoresImpNominalPendienteCuadre().subtract(titulos));

            cuadreEccSibbac.setNumTitulosDesSibbacPdte(cuadreEccSibbac.getNumTitulosDesSibbacPdte().subtract(titulos));
            log = cuadreEccLogDao.save(log);
            cuadreEccSibbac.getCuadreEccLogs().add(log);

            cuadreEccSibbac
                .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                    .getId());
            cuadreEccSibbac.setAuditDate(new Date());
            cuadreEccSibbac.setAuditUser("CECCTITULOS");
            cuadreEccSibbac.setIdCuadreEccEjecucion(null);
          }
        }
      }
    }
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] 6.- Reparto final de lo los titulos de ejecucion que queden.");
    for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
      if (cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) > 0) {

        for (CuadreEccEjecucion eje : ejes) {
          if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal titulos = cuadreEccSibbac.getNumTitulosDesSibbacPdte();
            if (cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(eje.getValoresImpNominalPendienteCuadre()) > 0) {
              LOG.warn("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] El desglose tiene mas titulos que la ejecucion.");
              titulos = eje.getValoresImpNominalPendienteCuadre();
            }
            else {
            }

            cuadreEccSibbac.setIdCuadreEccEjecucion(null);
            CuadreEccLog log = new CuadreEccLog();
            log.setEstado(0);
            log.setAuditDate(new Date());
            log.setAuditUser("CECCTITULOS");
            log.setIdCuadreEccEjecucion(eje.getId());
            log.setIdCuadreEccSibbac(cuadreEccSibbac.getId());
            log.setCodError(CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text);
            log.setTitulos(titulos);

            eje.setValoresImpNominalPendienteCuadre(eje.getValoresImpNominalPendienteCuadre().subtract(titulos));

            cuadreEccSibbac.setNumTitulosDesSibbacPdte(cuadreEccSibbac.getNumTitulosDesSibbacPdte().subtract(titulos));
            log = cuadreEccLogDao.save(log);
            cuadreEccSibbac.getCuadreEccLogs().add(log);

            cuadreEccSibbac
                .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                    .getId());
            cuadreEccSibbac.setAuditDate(new Date());
            cuadreEccSibbac.setAuditUser("CECCTITULOS");
            cuadreEccSibbac.setIdCuadreEccEjecucion(null);
          }
        }
      }
    }
    LOG.trace("[CuadreEccEjecucionBo :: repartirTitulosEjecucionesEnDesglosesConEcc] final");
  }

  /**
   * @param ejes
   * @param desgloses
   * @param ejecucionesById
   * @param ejecucionesByNumOp
   * @param ejecucionesByNumOpPrev
   */
  private void validarTitulosEjecuciones(List<CuadreEccEjecucion> ejes, List<CuadreEccSibbac> desgloses,
      Map<Long, CuadreEccEjecucion> ejecucionesById, Map<String, CuadreEccEjecucion> ejecucionesByNumOp,
      Map<String, List<CuadreEccEjecucion>> ejecucionesByNumOpPrev) {

    if (CollectionUtils.isNotEmpty(ejes)) {
      for (CuadreEccEjecucion eje : ejes) {
        eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
            .getId());
        if (eje.getValoresImpNominalPendienteCuadre().compareTo(BigDecimal.ZERO) != 0) {
          eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_ERRONEO.getId());
          String msg = MessageFormat.format("INCIDENCIA-Titulos ejecucion: {0} titulos sibbac: {1}", eje
              .getValoresImpNominal(), eje.getValoresImpNominal().subtract(eje.getValoresImpNominalPendienteCuadre()));
          grabarLogs(eje, desgloses, CodigoErrorValidacionCuadreEcc.TITULOS_EJECUCION_DISTINTO_SIBBAC.text, msg,
              "CECCTITULOS");

          if (eje.getMiembroPropietarioCuenta() != null
              && !eje.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)
              && TipoOperacionTraspasoTitulos.TRANSFER.text.equals(eje.getCodOp())) {
            eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_ERRONEO.getId());
            msg = MessageFormat.format("INCIDENCIA-Numero operacion traspasado despues de GIVE UP encontrado {0}",
                eje.getNumOp());
            grabarLogs(eje, desgloses, CodigoErrorValidacionCuadreEcc.TITULOS_TRASPASO_EXTERNO_POST_GIVE_UP.text, msg,
                "CECCTITULOS");
          }
          else if (eje.getMiembroPropietarioCuenta() != null
              && eje.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)
              && TipoOperacionTraspasoTitulos.GIVE_UP.text.equals(eje.getCodOp())) {
            eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_ERRONEO.getId());
            msg = MessageFormat.format("INCIDENCIA- Give up con destino la SV {0}", eje.getNumOp());
            grabarLogs(eje, desgloses, CodigoErrorValidacionCuadreEcc.TITULOS_GIVE_UP_A_SV.text, msg, "CECCTITULOS");
          }
        }
      }

    }
  }

  /**
   * @param ejes
   * @param desgloses
   * @param ejecucionesById
   * @param ejecucionesByNumOp
   * @param ejecucionesByNumOpPrev
   */
  private void validarTitulosDesgloses(List<CuadreEccEjecucion> ejes, List<CuadreEccSibbac> desgloses,
      Map<Long, CuadreEccEjecucion> ejecucionesById, Map<String, CuadreEccEjecucion> ejecucionesByNumOp,
      Map<String, List<CuadreEccEjecucion>> ejecucionesByNumOpPrev) {

    if (CollectionUtils.isNotEmpty(desgloses)) {
      List<CuadreEccSibbac> desglosesAux;
      List<CuadreEccLog> joins;
      CuadreEccEjecucion ejeAux;
      Map<CuadreEccSibbac, List<CuadreEccLog>> mapJoins = new HashMap<CuadreEccSibbac, List<CuadreEccLog>>();

      for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
        joins = new ArrayList<CuadreEccLog>();
        for (CuadreEccLog cuadreEccLog : cuadreEccSibbac.getCuadreEccLogs()) {
          if (cuadreEccLog.getCodError() != null
              && cuadreEccLog.getCodError().equals(CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text)) {
            joins.add(cuadreEccLog);
          }
        }

        mapJoins.put(cuadreEccSibbac, joins);
        for (CuadreEccLog cuadreEccLog : joins) {
          if (cuadreEccLog.getIdCuadreEccEjecucion() != null) {
            ejeAux = ejecucionesById.get(cuadreEccLog.getIdCuadreEccEjecucion());
            if (ejeAux != null && ejeAux.getNumOp() != null
                && (cuadreEccSibbac.getNumOp() == null || !cuadreEccSibbac.getNumOp().equals(ejeAux.getNumOp()))) {
              if (ejeAux.getMiembroPropietarioCuenta() != null
                  && !ejeAux.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)
                  && TipoOperacionTraspasoTitulos.TRANSFER.text.equals(ejeAux.getCodOp())) {
                ejeAux.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_ERRONEO.getId());
                String msg = MessageFormat.format(
                    "INCIDENCIA-Numero operacion traspasado despues de GIVE UP encontrado {0}", ejeAux.getNumOp());
                grabarLogs(ejeAux, desgloses,
                    CodigoErrorValidacionCuadreEcc.TITULOS_TRASPASO_EXTERNO_POST_GIVE_UP.text, msg, "CECCTITULOS");
              }
              else if (ejeAux.getMiembroPropietarioCuenta() != null
                  && ejeAux.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)
                  && TipoOperacionTraspasoTitulos.GIVE_UP.text.equals(ejeAux.getCodOp())) {
                ejeAux.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_ERRONEO.getId());
                String msg = MessageFormat.format("INCIDENCIA- Give up con destino la SV {0}", ejeAux.getNumOp());
                grabarLogs(ejeAux, desgloses, CodigoErrorValidacionCuadreEcc.TITULOS_GIVE_UP_A_SV.text, msg,
                    "CECCTITULOS");
              }
            }
          }
        }

      }

      for (CuadreEccSibbac cuadreEccSibbac : desgloses) {

        cuadreEccSibbac
            .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
                .getId());
        desglosesAux = new ArrayList<CuadreEccSibbac>();
        desglosesAux.add(cuadreEccSibbac);

        joins = mapJoins.get(cuadreEccSibbac);

        if (cuadreEccSibbac.getNudesglose() == null && cuadreEccSibbac.getIdejecucionalocation() != null) {
          cuadreEccSibbac
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_DESGLOSE_USUARIO.getId());
          cuadreEccSibbac.setAuditDate(new Date());
          cuadreEccSibbac.setAuditUser("CECCTITULOS");
          String msg = "INCIDENCIA-No desglosado";

          grabarLogs(null, desglosesAux, CodigoErrorValidacionCuadreEcc.TITULOS_NO_DESGLOSADO.text, msg, "CECCTITULOS");
        }
        else

        if (joins.size() > 1) {
          cuadreEccSibbac
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                  .getId());
          cuadreEccSibbac.setAuditDate(new Date());
          cuadreEccSibbac.setAuditUser("CECCTITULOS");
          String msg = "INCIDENCIA-Titulos desglose corresponden varios codigos de op ecc";
          grabarLogs(null, desglosesAux, CodigoErrorValidacionCuadreEcc.TITULOS_DESGLOSE_MAS_1_CODIGO_OP_ECC.text, msg,
              "CECCTITULOS");
        }
        else if (CollectionUtils.isNotEmpty(joins)) {
          cuadreEccSibbac
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                  .getId());
          cuadreEccSibbac.setAuditDate(new Date());
          cuadreEccSibbac.setAuditUser("CECCTITULOS");
          String msg = "INCIDENCIA-Traspaso no cargado";
          grabarLogs(null, desglosesAux, CodigoErrorValidacionCuadreEcc.TITULOS_TRASPASO_INTERNO_NO_CARGADO.text, msg,
              "CECCTITULOS");
        }

        if (cuadreEccSibbac.getNumTitulosDesSibbacPdte().compareTo(BigDecimal.ZERO) != 0) {
          cuadreEccSibbac
              .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_CODIGOS_OPERACION_USUARIO
                  .getId());
          cuadreEccSibbac.setAuditDate(new Date());
          cuadreEccSibbac.setAuditUser("CECCTITULOS");
          String msg = "INCIDENCIA-Faltan ejecuciones para el desglose";
          grabarLogs(null, desglosesAux, CodigoErrorValidacionCuadreEcc.TITULOS_DESGLOSE_NO_ENCUENTRA_EJECUCION.text,
              msg, "CECCTITULOS");
        }

        if (cuadreEccSibbac.getEstadoProcesado() != EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULOS_OK_PDTE_REVISAR_REFERENCIAS_TITULARES_DESGLOSES
            .getId()) {
          joins = mapJoins.get(cuadreEccSibbac);
          if (CollectionUtils.isNotEmpty(joins)) {
            for (CuadreEccLog cuadreEccLog : joins) {
              if (cuadreEccLog.getIdCuadreEccEjecucion() != null) {
                ejeAux = ejecucionesById.get(cuadreEccLog.getIdCuadreEccEjecucion());
                ejeAux.setEstadoProcesado(cuadreEccSibbac.getEstadoProcesado());
                ejeAux.setAuditDate(new Date());
                save(ejeAux);
              }
            }
          }
          else {
            if (cuadreEccSibbac.getIdCuadreEccEjecucion() != null) {
              CuadreEccEjecucion eje = ejecucionesById.get(cuadreEccSibbac.getIdCuadreEccEjecucion());
              if (eje != null) {
                eje.setEstadoProcesado(cuadreEccSibbac.getEstadoProcesado());
                eje.setAuditDate(new Date());
                save(eje);
              }
            }
          }

        }
      }
      mapJoins.clear();
    }
  }

  private void deleteLogsPrevios(long idCuadreEccEjecucion, List<CuadreEccSibbac> desgloses) {
    LOG.trace("[CuadreEccEjecucionBo :: deleteLogsPrevios] inicio.");
    cuadreEccLogDao.deleteByIdCuadreEccEjecucion(idCuadreEccEjecucion);
    if (CollectionUtils.isNotEmpty(desgloses)) {
      for (CuadreEccSibbac cuadreEccSibbac : desgloses) {
        cuadreEccLogDao.deleteByIdCuadreEccSibbac(cuadreEccSibbac.getId());
      }
    }
    else {
      LOG.trace("[CuadreEccEjecucionBo :: deleteLogsPrevios] No hay desgloses previos.");
    }
    LOG.trace("[CuadreEccEjecucionBo :: deleteLogsPrevios] final.");
  }

  private void grabarLogs(CuadreEccEjecucion eje, List<CuadreEccSibbac> desgloses, String codigoError, String msg,
      String auditUser) {
    CuadreEccLog log;
    if (eje != null) {
      log = new CuadreEccLog();
      log.setEstado(0);
      log.setCodError(codigoError);
      log.setIdCuadreEccEjecucion(eje.getId());
      log.setMsg(msg);
      log.setAuditDate(new Date());
      log.setAuditUser(auditUser);
      log.setCodOpMercado(eje.getCodOpMercado());
      log.setFechaEje(eje.getFechaNeg());
      log.setFechaLiq(eje.getFechaLiq());
      log.setIdEcc(eje.getIdEcc());
      log.setIsin(eje.getIsin());
      log.setMiembroMercado(eje.getMiembroMercadoNeg());
      log.setNumEjeMercado(eje.getNumEjeMercado());
      if (StringUtils.isBlank(eje.getNumOp())) {
        log.setNumOp(eje.getNumOpDcv());
      }
      else {
        log.setNumOp(eje.getNumOp());
      }
      log.setNumOrdenMercado(eje.getNumOrdenMercado());
      log.setPlataformaNeg(eje.getPlataformaNeg());
      log.setPrecio(eje.getPrecio());
      log.setSegmentoEcc(eje.getSegmentoEcc());
      log.setSegmentoNeg(eje.getSegmentoNeg());
      log.setSentido(eje.getSentido());
      log.setTitulos(eje.getValoresImpNominal());
      cuadreEccLogDao.save(log);
    }
    if (CollectionUtils.isNotEmpty(desgloses)) {
      for (CuadreEccSibbac desglose : desgloses) {
        log = new CuadreEccLog();
        log.setEstado(desglose.getEstado());
        log.setCodError(codigoError);
        if (eje != null) {
          log.setIdCuadreEccEjecucion(eje.getId());
        }
        log.setIdCuadreEccSibbac(desglose.getId());

        log.setMsg(msg);
        log.setAuditDate(new Date());
        log.setAuditUser(auditUser);

        log.setNuorden(desglose.getNuorden());
        log.setNbooking(desglose.getNbooking());
        log.setNucnfclt(desglose.getNucnfclt());
        log.setNucnfliq(desglose.getNucnfliq());
        log.setIddesglosecamara(desglose.getIddesglosecamara());
        log.setNudesglose(desglose.getNudesglose());

        log.setCodOpMercado(desglose.getCdoperacionmkt());
        log.setFechaEje(desglose.getFeoperac());
        log.setFechaLiq(desglose.getFevalor());
        log.setIdEcc(desglose.getCdcamara());
        log.setIsin(desglose.getCdisin());
        log.setMiembroMercado(desglose.getCdmiembromkt());
        log.setNumEjeMercado(desglose.getNuexemkt());
        log.setNumOp(desglose.getNumOp());
        log.setNumOrdenMercado(desglose.getNuordenmkt());
        if (eje != null) {
          log.setPlataformaNeg(eje.getPlataformaNeg());
        }

        if (eje != null) {
          log.setPrecio(eje.getPrecio());
          log.setSegmentoEcc(eje.getSegmentoEcc());
        }
        log.setSegmentoNeg(desglose.getCdsegmento());
        log.setSentido(desglose.getSentido());
        log.setTitulos(desglose.getNumTitulosDesSibbac());
        cuadreEccLogDao.save(log);
      }// fin for desgloses
    }
  }

  public IdentificacionOperacionCuadreEccDTO getIdentificacionOperacionCuadreEccDTO(AN an) {

    IdentificacionOperacionCuadreEccDTO id = null;

    R00 r00 = null;// datos de situacion en la cuenta
    R01 r01 = null;// datos operacion ecc
    R02 r02 = null;// datos ejecucion de mercado

    R05 r05 = null;
    R06 r06 = null;
    R07 r07 = null;

    if (an.hasRecords(PTIMessageRecordType.R00)) {
      r00 = (R00) an.getAll(PTIMessageRecordType.R00).get(0);
    }

    if (an.hasRecords(PTIMessageRecordType.R01)) {
      r01 = (R01) an.getAll(PTIMessageRecordType.R01).get(0);
    }

    if (an.hasRecords(PTIMessageRecordType.R02)) {
      r02 = (R02) an.getAll(PTIMessageRecordType.R02).get(0);
    }

    if (an.hasRecords(PTIMessageRecordType.R05)) {
      r05 = (R05) an.getAll(PTIMessageRecordType.R05).get(0);
    }

    if (an.hasRecords(PTIMessageRecordType.R06)) {
      r06 = (R06) an.getAll(PTIMessageRecordType.R06).get(0);
    }

    if (an.hasRecords(PTIMessageRecordType.R07)) {
      r07 = (R07) an.getAll(PTIMessageRecordType.R07).get(0);
    }

    if (r02 == null) {
      try {
        LOG.warn(
            "[CuadreEccEjecucionBo :: getIdentificacionOperacionCuadreEccDTO] no es un AN valido no tiene R02.\n{}\n{}",
            an, an.toPTIFormat());
      }
      catch (PTIMessageException e) {
        LOG.warn(
            "[CuadreEccEjecucionBo :: getIdentificacionOperacionCuadreEccDTO] no es un AN valido no tiene R02.\n{}\n",
            an, e);
      }
    }
    else {
      Character sentido = xasBo.getSentidoPtiSentidoSibbac(r02.getSentido()).charAt(0);

      // String tipo;
      Date fechaEjecucion = r02.getFechaNegociacion();
      String cdisin = r02.getCodigoDeValor();
      String cdcamara = r06.getIdECC();
      String cdsegmento = r02.getSegmento();
      String cdoperacionmkt = r02.getCodigoDeOperacionMercado();
      String cdmiembromkt = r02.getMiembroMercado();
      String nuordenmkt = r02.getNumeroDeLaOrdenDeMercado() + "";
      String nuexemkt = r02.getNumeroDeEjecucionDeMercado() + "";

      Date fechaLiquidacion = null;

      String cuentaLiquidacionIberclear = null;

      String numeroOperacion = null;
      String tipoCuentaIberclear = null;

      if (r01 != null) {
        fechaLiquidacion = r01.getFechaLiquidacionTeorica();
        numeroOperacion = r01.getNumeroOperacion();

      }
      else if (r05 != null) {
        fechaLiquidacion = r05.getFechaLiquidacionTeorica();
      }

      if (numeroOperacion == null && r07 != null) {
        numeroOperacion = r07.getNumeroOperacionDCV();
      }

      if (r00 != null && StringUtils.isNotBlank(r00.getCuentaLiquidacion())) {
        cuentaLiquidacionIberclear = r00.getCuentaLiquidacion();
      }
      else if (r05 != null && StringUtils.isNotBlank(r05.getCuentaLiquidacion())) {
        cuentaLiquidacionIberclear = r05.getCuentaLiquidacion();
      }

      if (cuentaLiquidacionIberclear != null && cuentaLiquidacionIberclear.length() >= 26) {
        tipoCuentaIberclear = cuentaLiquidacionIberclear.substring(24, 26);
      }
      String cdoperacionSinEcc = cfgBo.getTpopebolsSinEcc();
      boolean isSinEcc = StringUtils.isNotBlank(cdoperacionSinEcc) && cdoperacionSinEcc.contains(cdoperacionmkt);

      if (isSinEcc) {
        cdcamara = idEccCuadreEcc;
      }

      id = new IdentificacionOperacionCuadreEccDTO(numeroOperacion, fechaEjecucion, cdisin, sentido, cdcamara,
          cdsegmento, cdoperacionmkt, cdmiembromkt, nuordenmkt, nuexemkt);

      id.setSinEcc(isSinEcc);
      id.setTipoCuentaIberclear(tipoCuentaIberclear);
      id.setCuentaLiquidacionIberclear(cuentaLiquidacionIberclear);
      id.setFechaLiquidacion(fechaLiquidacion);
    }
    return id;

  }

  public IdentificacionOperacionCuadreEccDTO getIdentificacionOperacionCuadreEccDTO(CuadreEccEjecucion ejecucion) {

    Character sentido = ejecucion.getSentido();
    // String tipo;
    Date fechaEjecucion = ejecucion.getFechaNeg();
    String cdisin = ejecucion.getIsin();
    String cdcamara = ejecucion.getIdEcc();
    String cdsegmento = ejecucion.getSegmentoNeg();
    String cdoperacionmkt = ejecucion.getCodOpMercado();
    String cdmiembromkt = ejecucion.getMiembroMercadoNeg();
    String nuordenmkt = ejecucion.getNumOrdenMercado();
    String nuexemkt = ejecucion.getNumEjeMercado();

    Date fechaLiquidacion = ejecucion.getFechaLiq();

    String cuentaLiquidacionIberclear = ejecucion.getCuentaLiquidacionMiembro();
    String tipoCuentaIberclear = ejecucion.getTipoCuentaLiquidacionMiembro();

    String cdoperacionSinEcc = cfgBo.getTpopebolsSinEcc();
    boolean isSinEcc = StringUtils.isNotBlank(cdoperacionSinEcc) && cdoperacionSinEcc.contains(cdoperacionmkt);
    String numeroOperacion = ejecucion.getNumOp();
    if (isSinEcc) {
      numeroOperacion = ejecucion.getNumOpDcv();
    }

    IdentificacionOperacionCuadreEccDTO id = new IdentificacionOperacionCuadreEccDTO(numeroOperacion, fechaEjecucion,
        cdisin, sentido, cdcamara, cdsegmento, cdoperacionmkt, cdmiembromkt, nuordenmkt, nuexemkt);

    id.setSinEcc(isSinEcc);
    id.setTipoCuentaIberclear(tipoCuentaIberclear);
    id.setCuentaLiquidacionIberclear(cuentaLiquidacionIberclear);
    id.setFechaLiquidacion(fechaLiquidacion);

    return id;

  }

  public CuadreEccEjecucion findByIdentificacionNumOpNuevo(IdentificacionOperacionCuadreEccDTO identificacionOperacion) {
    CuadreEccEjecucion ejecucion = null;
    if (identificacionOperacion.isSinEcc()) {
      ejecucion = findByIdEccAndNumOpDcvAndFechaNegAndSentido(identificacionOperacion.getCdcamara(),
          identificacionOperacion.getNumeroOperacion(), identificacionOperacion.getFechaEjecucion(),
          identificacionOperacion.getSentido());
    }
    else {
      ejecucion = findByNumOpAndIdEcc(identificacionOperacion.getNumeroOperacion(),
          identificacionOperacion.getCdcamara());
    }

    return ejecucion;
  }

  public List<CuadreEccEjecucion> findByIdentificacionNumOpInicial(
      IdentificacionOperacionCuadreEccDTO identificacionOperacion) {
    List<CuadreEccEjecucion> ejecuciones = new ArrayList<CuadreEccEjecucion>();
    CuadreEccEjecucion ejecucion = null;
    if (identificacionOperacion.isSinEcc()) {
      ejecucion = findByIdEccAndNumOpDcvAndFechaNegAndSentido(identificacionOperacion.getCdcamara(),
          identificacionOperacion.getNumeroOperacion(), identificacionOperacion.getFechaEjecucion(),
          identificacionOperacion.getSentido());
      if (ejecucion != null) {
        ejecuciones.add(ejecucion);
      }
    }
    else {
      ejecuciones.addAll(findByNumOpInicialAndIdEcc(identificacionOperacion.getNumeroOperacion(),
          identificacionOperacion.getCdcamara()));
    }

    return ejecuciones;
  }

  /**
   * @param fechaEjecucion
   * @param numeroOperacion
   * @param sentido
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Integer deletePreviousDataByCuadreEccEjecucion(CuadreEccEjecucion ejecucion, boolean deleteReferencias,
      boolean deleteSibbac, boolean deleteByInformacionMercado, int maxRegsInSimpleThread)
      throws SIBBACBusinessException {

    LOG.debug("[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] inicio.");
    ExecutorService executor = null;
    int countDeletesReferencias = 0;
    int countDeletesDesgloses = 0;

    Date fechaEjecucion = ejecucion.getFechaNeg();
    String numeroOperacion = ejecucion.getNumOp();
    if (StringUtils.isBlank(numeroOperacion)) {
      numeroOperacion = ejecucion.getNumOpDcv();
    }
    Character sentido = ejecucion.getSentido();
    if (deleteReferencias) {
      LOG.debug("[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] Buscando refrencias de ficheros anteriores...");
      List<Long> idsRef = cuadreEccReferenciaBo.findAllIdsByIdCuadreEccEjecucion(ejecucion.getId());

      // List<Long> idsRef =
      // cuadreEccReferenciaBo.findAllIdsByFechaEjeAndNumOpAndSentido(fechaEjecucion,
      // numeroOperacion,
      // sentido);

      // if (idsRef.size() < 100) {

      LOG.debug(
          "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] Encontradas {} refrencias de ficheros anteriores.",
          idsRef.size());

      if (CollectionUtils.isNotEmpty(idsRef)) {
        boolean executeInThreads = idsRef.size() > maxRegsInSimpleThread;
        if (executeInThreads) {

          final List<Integer> listBorrados = new ArrayList<Integer>();
          int posIni = 0;
          int posEnd = 0;
          try {
            executor = Executors.newFixedThreadPool(threadSize);
            while (posEnd < idsRef.size()) {
              posEnd += transactionSize;
              if (posEnd > idsRef.size()) {
                posEnd = idsRef.size();
              }
              final List<Long> idsSubList = new ArrayList<Long>(idsRef.subList(posIni, posEnd));
              executor.execute(new Runnable() {

                @Override
                public void run() {
                  int countDeletesReferencias = 0;
                  for (Long idRef : idsSubList) {
                    cuadreEccReferenciaBo.deleteById(idRef);
                    countDeletesReferencias++;
                    if (countDeletesReferencias % transactionSize == 0) {
                      LOG.info(
                          "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] borrando {} referencias.",
                          countDeletesReferencias);
                    }
                  }// fin for
                  listBorrados.add(countDeletesReferencias);
                }
              });
              posIni = posEnd;
            }
          }
          catch (Exception e) {
            if (executor != null && !executor.isShutdown()) {
              executor.shutdownNow();
            }
            throw new SIBBACBusinessException(e);
          }
          finally {
            try {
              if (executor != null) {
                if (!executor.isShutdown()) {
                  executor.shutdown();
                }

                while (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                  LOG.info(
                      "[esperarPorThreadPool] *** ESPERANDO POR HILOS ACTIVOS {} - QUEUED TASK {} - COMPLETED {} ..... ***  {}",
                      ((ThreadPoolExecutor) executor).getActiveCount(), ((ThreadPoolExecutor) executor).getQueue()
                          .size(), ((ThreadPoolExecutor) executor).getCompletedTaskCount(), executor);
                }
              }
            }
            catch (InterruptedException e) {
              LOG.warn("INCIDENCIA- esperando por los hilos", e);
              throw new SIBBACBusinessException(e.getMessage(), e);
            }
            finally {
              for (Integer borrados : listBorrados) {
                countDeletesReferencias += borrados;
              }
              LOG.info(
                  "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] borrando {} referencias.",
                  countDeletesReferencias);
            }

          }

        }
        else {
          for (Long idRef : idsRef) {
            cuadreEccReferenciaBo.deleteById(idRef);
            countDeletesReferencias++;
            if (countDeletesReferencias % transactionSize == 0) {
              LOG.info(
                  "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] borrando {} referencias.",
                  countDeletesReferencias);
            }
          }// fin for
        }

      }

    }
    if (deleteSibbac) {
      LOG.debug("[CuadreEccEjecucionBo :: grabarCuadreEccSibbacReferenciasAndTitulares] Buscando desgloses de ficheros anteriores...");

      List<Long> idsSibbac = new ArrayList<Long>();
      if (deleteByInformacionMercado) {
        idsSibbac
            .addAll(cuadreEccSibbacBo
                .findAllIdsByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(
                    fechaEjecucion, ejecucion.getIdEcc(), ejecucion.getIsin(), ejecucion.getMiembroMercadoNeg(),
                    ejecucion.getSentido(), ejecucion.getSegmentoNeg(), ejecucion.getCodOpMercado(),
                    ejecucion.getNumOrdenMercado(), ejecucion.getNumEjeMercado()));
      }
      else {
        // se suba el indice que falta a pro
        idsSibbac.addAll(cuadreEccSibbacBo.findAllIdsByFeoperacAndNumerooperacionAndSentidoAndIdCuadreEccEjecucion(
            fechaEjecucion, numeroOperacion, sentido, ejecucion.getId()));
      }

      LOG.debug(
          "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] Encontrados {} desgloses de ficheros anteriores.",
          idsSibbac.size());
      if (CollectionUtils.isNotEmpty(idsSibbac)) {

        boolean executeInThreads = idsSibbac.size() > 1000;
        if (executeInThreads) {

          final List<Integer> listBorrados = new ArrayList<Integer>();
          int posIni = 0;
          int posEnd = 0;
          int bloque = 100;
          try {
            executor = Executors.newFixedThreadPool(25);
            while (posEnd < idsSibbac.size()) {
              posEnd += bloque;
              if (posEnd > idsSibbac.size()) {
                posEnd = idsSibbac.size();
              }
              final List<Long> idsSubList = new ArrayList<Long>(idsSibbac.subList(posIni, posEnd));
              executor.execute(new Runnable() {

                @Override
                public void run() {
                  int countDeletesDesgloses = 0;
                  for (Long idSibbac : idsSubList) {
                    cuadreEccSibbacBo.deleteById(idSibbac);
                    countDeletesDesgloses++;
                    if (countDeletesDesgloses % 100 == 0) {
                      LOG.info(
                          "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] borrando {} desgloses.",
                          countDeletesDesgloses);
                    }
                  }// fin for
                  listBorrados.add(countDeletesDesgloses);
                }
              });
              posIni = posEnd;
            }
          }
          catch (Exception e) {
            if (executor != null && !executor.isShutdown()) {
              executor.shutdownNow();
            }
            throw new SIBBACBusinessException(e);
          }
          finally {
            try {
              if (executor != null) {
                if (!executor.isShutdown()) {
                  executor.shutdown();
                }

                while (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                  LOG.info(
                      "[esperarPorThreadPool] *** ESPERANDO POR HILOS ACTIVOS {} - QUEUED TASK {} - COMPLETED {} ..... ***  {}",
                      ((ThreadPoolExecutor) executor).getActiveCount(), ((ThreadPoolExecutor) executor).getQueue()
                          .size(), ((ThreadPoolExecutor) executor).getCompletedTaskCount(), executor);
                }

              }

            }
            catch (InterruptedException e) {
              LOG.warn("INCIDENCIA- esperando por los hilos", e);
              throw new SIBBACBusinessException(e.getMessage(), e);
            }
            finally {
              for (Integer borrados : listBorrados) {
                countDeletesDesgloses += borrados;
              }
              LOG.info(
                  "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] borrando {} desgloses.",
                  countDeletesDesgloses);
            }
          }

        }
        else {

          for (Long idSibbac : idsSibbac) {
            cuadreEccSibbacBo.deleteById(idSibbac);
            countDeletesDesgloses++;
            if (countDeletesDesgloses % 100 == 0) {
              LOG.info(
                  "[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] borrando {} desgloses.",
                  countDeletesDesgloses);
            }
          }

        }

      }
    }
    LOG.debug("[CuadreEccEjecucionBo :: deletePreviousDataByFechaEjecucionAndNumOpAndSentido] fin.");
    return countDeletesReferencias + countDeletesDesgloses;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public int updateEstadoProcesadoByNuordenAndNbookingAndNucnflctNewTransaction(String nuorden, String nbooking,
      String nucnfclt, int estadoProcesado) {
    return updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(nuorden, nbooking, nucnfclt, estadoProcesado);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public int updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(String nuorden, String nbooking, String nucnfclt,
      int estadoProcesado) {
    LOG.debug("[CuadreEccEjecucionBo :: updateEstadoProcesadoByNuordenAndNbookingAndNucnflct] inicio");

    int updates = 0;
    List<CuadreEccSibbac> cuadresEccSibbac = cuadreEccSibbacBo.findAlos(nuorden, nbooking, nucnfclt);
    List<CuadreEccLog> joins;
    for (CuadreEccSibbac cuadreEccSibbac : cuadresEccSibbac) {
      LOG.debug(
          "[CuadreEccEjecucionBo :: updateEstadoProcesadoByNuordenAndNbookingAndNucnflct] actualizando {} a estado {}",
          cuadreEccSibbac, estadoProcesado);
      cuadreEccSibbac.setEstadoProcesado(estadoProcesado);
      cuadreEccSibbac.setAuditDate(new Date());

      if (cuadreEccSibbac.getIdCuadreEccEjecucion() == null) {
        joins = cuadreEccLogDao.findAllByIdCuadreEccSibbacAndCodError(cuadreEccSibbac.getId(),
            CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text);
        for (CuadreEccLog cuadreEccLog : joins) {
          if (cuadreEccLog.getIdCuadreEccEjecucion() != null) {
            updates += dao.updateEstadoProcesadoById(estadoProcesado, new Date(), "",
                cuadreEccLog.getIdCuadreEccEjecucion());
          }
        }
      }
      else {
        updates += dao.updateEstadoProcesadoById(estadoProcesado, new Date(), "",
            cuadreEccSibbac.getIdCuadreEccEjecucion());
      }

    }
    cuadreEccSibbacBo.save(cuadresEccSibbac);
    LOG.debug("[CuadreEccEjecucionBo :: updateEstadoProcesadoByNuordenAndNbookingAndNucnflct] fin {}", updates);
    return updates;
  }

  public int updateEstadoProcesadoById(long id, int estadoProcesado, String audituser) {
    return dao.updateEstadoProcesadoById(estadoProcesado, new Date(), audituser, id);
  }

}
