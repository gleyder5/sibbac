package sibbac.business.comunicaciones.pti.ac.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.ac.dto.AceptacionRechazoMovimientosDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.common.SIBBACBusinessException;

@Service
public class AceptacionRechazoMovimientosPtiWithUserLockBo {

  protected static final Logger LOG = LoggerFactory.getLogger(AceptacionRechazoMovimientosPtiWithUserLockBo.class);

  @Autowired
  private AceptacionRechazoMovimientosPtiBo aceptacionRechazoMovimientosBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  public AceptacionRechazoMovimientosPtiWithUserLockBo() {
  }

  /**
   * Envia el rechazo de los movimientos que estan listos para entrar
   * @param listCdrefmov
   * @param version
   * @param auditUser este dato sale de {@link Tmct0cfgBo.getPtiMessageVersion}
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED)
  public List<String> enviarRechazoMovimientosEntradaByCdrefmovimientoecc(
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, String auditUser) throws SIBBACBusinessException {

    Collection<Tmct0bokId> bookingsBloqueados = new HashSet<Tmct0bokId>();
    List<Tmct0movimientoecc> listMovs = getListMovimientos(listCdrefmov);
    boolean imLock = false;
    try {
      bloquearBookings(listMovs, bookingsBloqueados, auditUser);
      imLock = true;
      return aceptacionRechazoMovimientosBo
          .enviarRechazoMovimientosEntradaByCdrefmovimientoecc(listCdrefmov, auditUser);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {
      if (imLock)
        this.desBloquearBookigs(bookingsBloqueados, auditUser);
    }
  }

  /**
   * Envia la aceptacion del movimiento que esta pendiente de entrada, a la
   * cuenta de compensacion que se pasa por parametro
   * 
   * @param listCdrefmov lista de cdrefmovimientoecc
   * @param ctaDestino cuenta de compensacion destino del movimiento
   * @param version este dato sale de {@link Tmct0cfgBo.getPtiMessageVersion}
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED)
  public List<String> enviarAceptacionMovimientosEntradaByCdrefmovimientoecc(
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, String ctaDestino, String auditUser)
      throws SIBBACBusinessException {
    Collection<Tmct0bokId> bookingsBloqueados = new HashSet<Tmct0bokId>();
    List<Tmct0movimientoecc> listMovs = getListMovimientos(listCdrefmov);
    boolean imLock = false;
    try {
      bloquearBookings(listMovs, bookingsBloqueados, auditUser);
      imLock = true;
      return aceptacionRechazoMovimientosBo.enviarAceptacionMovimientosEntradaByCdrefmovimientoecc(listCdrefmov,
          ctaDestino, auditUser);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {
      if (imLock)
        this.desBloquearBookigs(bookingsBloqueados, auditUser);
    }
  }

  /**
   * @param listCdrefmov lista de cdrefmovimientoecc para los que se van a
   * enviar las peticiones de cancelacion
   * @param cfgVersion este dato sale de {@link Tmct0cfgBo.getPtiMessageVersion}
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED)
  public List<String> enviarCancelacionMovimientosSalidaByCdrefmovimientoecc(
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, String auditUser) throws SIBBACBusinessException {
    Collection<Tmct0bokId> bookingsBloqueados = new HashSet<Tmct0bokId>();
    List<Tmct0movimientoecc> listMovs = getListMovimientos(listCdrefmov);
    boolean imLock = false;
    try {
      bloquearBookings(listMovs, bookingsBloqueados, auditUser);
      imLock = true;
      return aceptacionRechazoMovimientosBo.enviarCancelacionMovimientosSalidaByCdrefmovimientoecc(listCdrefmov,
          auditUser);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {
      if (imLock) {
        this.desBloquearBookigs(bookingsBloqueados, auditUser);
      }
    }

  }

  private List<Tmct0movimientoecc> getListMovimientos(List<AceptacionRechazoMovimientosDTO> listCdrefmovEcc) {
    List<Tmct0movimientoecc> listMovs = new ArrayList<Tmct0movimientoecc>();
    List<Tmct0movimientoecc> subList;
    for (AceptacionRechazoMovimientosDTO d : listCdrefmovEcc) {
      subList = movBo.findAllByCdrefmovimientoeccAndFeliquidacionAndDesgloseActivo(d.getCdrefmovimientoecc(),
          d.getFliquidacion());
      listMovs.addAll(subList);
      subList = movBo.findAllByCdrefmovimientoeccAndFeliquidacionAndAloActivo(d.getCdrefmovimientoecc(),
          d.getFliquidacion());
      listMovs.addAll(subList);
    }
    return listMovs;
  }

  private void bloquearBookings(List<Tmct0movimientoecc> listMovs, Collection<Tmct0bokId> bookingsBloqueados,
      String usuarioAuditoria) throws SIBBACBusinessException {
    for (Tmct0movimientoecc mov : listMovs) {
      if (mov.getTmct0alo() != null) {
        if (!bookingsBloqueados.contains(mov.getTmct0alo().getTmct0bok().getId())) {
          try {
            bokBo.bloquearBooking(mov.getTmct0alo().getTmct0bok().getId(), usuarioAuditoria);
            bookingsBloqueados.add(mov.getTmct0alo().getTmct0bok().getId());
          }
          catch (LockUserException e) {
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
        }
      }
      else if (mov.getTmct0desglosecamara() != null) {
        if (!bookingsBloqueados
            .contains(mov.getTmct0desglosecamara().getTmct0alc().getTmct0alo().getTmct0bok().getId())) {
          try {
            bokBo.bloquearBooking(mov.getTmct0desglosecamara().getTmct0alc().getTmct0alo().getTmct0bok().getId(),
                usuarioAuditoria);
            bookingsBloqueados.add(mov.getTmct0desglosecamara().getTmct0alc().getTmct0alo().getTmct0bok().getId());
          }
          catch (LockUserException e) {
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
        }
      }
    }
  }

  private void desBloquearBookigs(Collection<Tmct0bokId> bookingsBloqueados, String usuarioAuditoria)
      throws SIBBACBusinessException {
    for (Tmct0bokId tmct0bokId : bookingsBloqueados) {
      try {
        bokBo.desBloquearBooking(tmct0bokId, usuarioAuditoria);
      }
      catch (LockUserException e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
    }
  }

}
