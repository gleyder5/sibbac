package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccArreglarDesglosesRunnableBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccArreglarDesglosesDbReader")
public class CuadreEccArreglarDesglosesDbReader extends WrapperMultiThreadedDbReader<CuadreEccDbReaderRecordBeanDTO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CuadreEccArreglarDesglosesDbReader.class);
  
  @Value("${sibbac.cuadreecc.max.reg.process.in.execution:100000}")
  private int maxRegToProcess;
  
  @Value("${sibbac.cuadreecc.arreglar.desgloses.transaction.size:5}")
  private int transactionSize;

  @Value("${sibbac.cuadreecc.arreglar.desgloses.thread.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Qualifier(value = "cuadreEccArreglarDesglosesRunnableBean")
  @Autowired
  private CuadreEccArreglarDesglosesRunnableBean runnableBean;

  public CuadreEccArreglarDesglosesDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    boolean res = true;
    if (previousBean != null && bean != null) {
      CuadreEccDbReaderRecordBeanDTO previousBeanDbReader = (CuadreEccDbReaderRecordBeanDTO) previousBean;
      CuadreEccDbReaderRecordBeanDTO beanDbReader = (CuadreEccDbReaderRecordBeanDTO) bean;

      if (!previousBeanDbReader.getTmct0aloId().getNuorden().equals(beanDbReader.getTmct0aloId().getNuorden())
          || !previousBeanDbReader.getTmct0aloId().getNbooking().equals(beanDbReader.getTmct0aloId().getNbooking())) {
        res = false;
      }

    }
    return res;
  }

  @Override
  public IRunnableBean<CuadreEccDbReaderRecordBeanDTO> getBeanToProcessBlock() {
    return runnableBean;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.debug("[CuadreEccArreglarDesglosesDbReader :: preExecuteTask] buscando alos que procesar...");
    List<CuadreEccDbReaderRecordBeanDTO> beans = cuadreEccSibbacBo.findAllCuadreEccIdDTOForArreglarDesglosesSibbacTask(maxRegToProcess);
    LOG.debug("[CuadreEccArreglarDesglosesDbReader :: preExecuteTask] encontrados {} alos que procesar.", beans.size());
    beanIterator = beans.iterator();
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
