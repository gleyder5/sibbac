package sibbac.business.comunicaciones.euroccp.ciffile.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.CLEARED_GROSS_TRADE_SPAIN)
@Entity
public class EuroCcpClearedGrossTradesSpain extends EuroCcpLineaFicheroRecordBean {

  /**
   * Serial version UID
   */
  private static final long serialVersionUID = -8211185795469500418L;

  @Column(name = "ACCOUNT_TYPE", length = 5, nullable = false)
  private String accountType;

  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "SUBACCOUNT_NUMBER", length = 10, nullable = false)
  private String subaccountNumber;

  @Column(name = "OPPOSITE_PARTY_CODE", length = 6, nullable = false)
  private String oppositePartyCode;

  @Column(name = "PRODUCT_GROUP_CODE", length = 2, nullable = false)
  private String productGroupCode;

  @Column(name = "EXCHANGE_CODE_TRADE", length = 4, nullable = false)
  private String exchangeCodeTrade;

  @Column(name = "SYMBOL", length = 6, nullable = false)
  private String symbol;

  @Column(name = "TYPE", length =1, nullable = true)
  private Character type;
  
  @Temporal(TemporalType.DATE)
  @Column(name = "EXPIRATION_DATE", nullable = true)
  private Date expirationDate;

  @Column(name = "EXERCISE_PRICE", length = 15, scale = 7, nullable = true)
  private BigDecimal exercisePrice;

  @Column(name = "EXTERNAL_MEMBER", length = 10, nullable = true)
  private String externalMember;

  @Column(name = "EXTERNAL_ACCOUNT", length = 15, nullable = true)
  private String externalAccount;

  @Column(name = "CURRENCY_CODE", length = 3, nullable = false)
  private String currencyCode;
  
  @Column(name = "MOVEMENT_CODE",  length = 2, nullable = true)
  private String movementCode;
  
  @Column(name = "BUY_SELL_CODE", length = 1, nullable = false)
  private char buySellCode;
  
  @Column(name = "QUANTITY_LONG_SIGN", length = 1, nullable = true) 
  private Character quantityLongSign;
  
  @Column(name = "PROCESSED_QUANTITY_LONG", length = 12, scale = 2, nullable = true)
  private BigDecimal processedQuantityLong;
  
  @Column(name = "QUANTITY_SHORT_SIGN", length = 1, nullable = true)
  private Character quantityShortSign;
  
  @Column(name = "PROCESSED_QUANTITY_SHORT", length = 12, scale = 2, nullable = true)
  private BigDecimal processedQuantityShort;
  
  @Column(name = "CLEARING_FEE", length = 12, scale = 4, nullable = true)
  private BigDecimal clearingFee;
  
  @Column(name = "CLEARING_FEE_DC", length = 1, nullable = true)
  private Character clearingFeeDc;
  
  @Column(name = "CLEARING_FEE_CUR_CODE", length = 3, nullable = true)
  private String clearingFeeCurCode;
  
  @Column(name = "COUNTER_VALUE", length = 18, scale = 2, nullable = true)
  private BigDecimal counterValue;
  
  @Column(name = "COUNTER_VALUE_DC", length = 1, nullable = true)
  private Character counterValueDc;
  
  @Column(name = "COUNTER_VALUE_CUR_CODE", length = 3, nullable = true)
  private String counterValueCurCode;
  
  @Column(name = "COUPON_INTEREST", length = 18, scale = 2, nullable = true)
  private BigDecimal couponInterest;
  
  @Column(name = "COUPON_INTEREST_DC", length = 1, nullable = true)
  private Character couponInterestDc;
  
  @Column(name = "EFFECTIVE_VALUE", length = 18, scale = 2, nullable = false)
  private BigDecimal effectiveValue;
  
  @Column(name = "EFFECTIVE_VALUE_DC", length = 1, nullable = false)
  private char effectiveValueDc;
  
  @Column(name = "TRANSACTION_PRICE", length = 15, scale = 7, nullable = false)
  private BigDecimal transactionPrice;
  
  @Temporal(TemporalType.DATE)
  @Column(name = "TRANSACTION_DATE", nullable = false)
  private Date transactionDate;
  
  @Temporal(TemporalType.DATE)
  @Column(name = "SETTLEMENT_DATE", nullable = true)
  private Date settlementDate;
  
  @Column(name = "UNSETTLED_REFERENCE", length = 9, nullable = false)
  private String unsettledReference;
  
  @Column(name = "EXTERNAL_TRANSACTION_ID_EXCHANGE", length = 20, nullable = false)
  private String externalTransactionIdExchange;
  
  @Column(name = "SETTLEMENT_INSTRUCTION_REFERENCE", length = 9, nullable = true)
  private String settlementInstructionReference;
  
  @Column(name = "ORDER_NUMBER", length = 10, nullable = true) 
  private String orderNumber;
  
  @Column(name = "ISIN_CODE", length = 12, nullable = false)
  private String isinCode;
  
  @Column(name = "TRADER_INITIALS", length = 6, nullable = true)
  private String traderInitials;
  
  @Column(name = "ULV_TRADING_UNIT", length = 11, scale = 4, nullable = true)
  private BigDecimal ulvTradingUnit;
  
  @Column(name = "TRANSACTION_ORIGIN", length = 4, nullable = true)
  private String transactionOrigin;
  
  @Column(name = "EXEC_TRADING_ID", length = 6, nullable = true)
  private String execTradingId;
  
  @Column(name = "DEPOT_ID", length = 6, nullable = false)
  private String depotId;
  
  @Column(name = "SAFE_KEEPING_ID", length = 2, nullable = false)
  private String safeKeepingId;
  
  @Column(name = "COMMENT", length = 21, nullable = true)
  private String comment;
  
  @Column(name = "TIMESTAMP", length = 6, nullable = true)
  private String timestamp;
  
  @Column(name = "TRANSACTION_TYPE_CODE", length = 3, nullable = false)
  private String transactionTypeCode;
  
  @Column(name = "EXTERNAL_POSITION_ACCOUNT_ID", length = 30, nullable = true)
  private String externalPositionAccountId;
  
  @Column(name = "CLEARING_ACCOUNT", length = 8, nullable = false)
  private String clearingAccount;
  
  @Column(name = "SPANISH_CSD_ACCOUNT_TYPE", length = 1, nullable = false)
  private char spanishCsdAccountType;
  
  @Column(name = "OWNER_REFERENCE", length = 20, nullable = false)
  private String ownerReference;
  
  @Column(name = "HOLD_OR_RELEASE_STATUS", length = 1, nullable = false)
  private char holdOrReleaseStatus;
  
  @Column(name = "FILLER", length = 66, nullable = true)
  private String filler;

  public String getAccountType() {
    return accountType;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getSubaccountNumber() {
    return subaccountNumber;
  }

  public String getOppositePartyCode() {
    return oppositePartyCode;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public String getExchangeCodeTrade() {
    return exchangeCodeTrade;
  }

  public String getSymbol() {
    return symbol;
  }

  public Character getType() {
    return type;
  }

  public Date getExpirationDate() {
    return expirationDate;
  }

  public BigDecimal getExercisePrice() {
    return exercisePrice;
  }

  public String getExternalMember() {
    return externalMember;
  }

  public String getExternalAccount() {
    return externalAccount;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getMovementCode() {
    return movementCode;
  }

  public char getBuySellCode() {
    return buySellCode;
  }

  public Character getQuantityLongSign() {
    return quantityLongSign;
  }

  public BigDecimal getProcessedQuantityLong() {
    return processedQuantityLong;
  }

  public Character getQuantityShortSign() {
    return quantityShortSign;
  }

  public BigDecimal getProcessedQuantityShort() {
    return processedQuantityShort;
  }

  public BigDecimal getClearingFee() {
    return clearingFee;
  }

  public Character getClearingFeeDc() {
    return clearingFeeDc;
  }

  public String getClearingFeeCurCode() {
    return clearingFeeCurCode;
  }

  public BigDecimal getCounterValue() {
    return counterValue;
  }

  public Character getCounterValueDc() {
    return counterValueDc;
  }

  public String getCounterValueCurCode() {
    return counterValueCurCode;
  }

  public BigDecimal getCouponInterest() {
    return couponInterest;
  }

  public Character getCouponInterestDc() {
    return couponInterestDc;
  }

  public BigDecimal getEffectiveValue() {
    return effectiveValue;
  }

  public char getEffectiveValueDc() {
    return effectiveValueDc;
  }

  public BigDecimal getTransactionPrice() {
    return transactionPrice;
  }

  public Date getTransactionDate() {
    return transactionDate;
  }

  public Date getSettlementDate() {
    return settlementDate;
  }

  public String getUnsettledReference() {
    return unsettledReference;
  }

  public String getExternalTransactionIdExchange() {
    return externalTransactionIdExchange;
  }

  public String getSettlementInstructionReference() {
    return settlementInstructionReference;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public String getIsinCode() {
    return isinCode;
  }

  public String getTraderInitials() {
    return traderInitials;
  }

  public BigDecimal getUlvTradingUnit() {
    return ulvTradingUnit;
  }

  public String getTransactionOrigin() {
    return transactionOrigin;
  }

  public String getExecTradingId() {
    return execTradingId;
  }

  public String getDepotId() {
    return depotId;
  }

  public String getSafeKeepingId() {
    return safeKeepingId;
  }

  public String getComment() {
    return comment;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public String getTransactionTypeCode() {
    return transactionTypeCode;
  }

  public String getExternalPositionAccountId() {
    return externalPositionAccountId;
  }

  public String getClearingAccount() {
    return clearingAccount;
  }

  public char getSpanishCsdAccountType() {
    return spanishCsdAccountType;
  }

  public String getOwnerReference() {
    return ownerReference;
  }

  public char getHoldOrReleaseStatus() {
    return holdOrReleaseStatus;
  }

  public String getFiller() {
    return filler;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setSubaccountNumber(String subaccountNumber) {
    this.subaccountNumber = subaccountNumber;
  }

  public void setOppositePartyCode(String oppositePartyCode) {
    this.oppositePartyCode = oppositePartyCode;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public void setExchangeCodeTrade(String exchangeCodeTrade) {
    this.exchangeCodeTrade = exchangeCodeTrade;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  public void setType(Character type) {
    this.type = type;
  }

  public void setExpirationDate(Date expirationDate) {
    this.expirationDate = expirationDate;
  }

  public void setExercisePrice(BigDecimal exercisePrice) {
    this.exercisePrice = exercisePrice;
  }

  public void setExternalMember(String externalMember) {
    this.externalMember = externalMember;
  }

  public void setExternalAccount(String externalAccount) {
    this.externalAccount = externalAccount;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setMovementCode(String movementCode) {
    this.movementCode = movementCode;
  }

  public void setBuySellCode(char buySellCode) {
    this.buySellCode = buySellCode;
  }

  public void setQuantityLongSign(Character quantityLongSign) {
    this.quantityLongSign = quantityLongSign;
  }

  public void setProcessedQuantityLong(BigDecimal processedQuantityLong) {
    this.processedQuantityLong = processedQuantityLong;
  }

  public void setQuantityShortSign(Character quantityShortSign) {
    this.quantityShortSign = quantityShortSign;
  }

  public void setProcessedQuantityShort(BigDecimal processedQuantityShort) {
    this.processedQuantityShort = processedQuantityShort;
  }

  public void setClearingFee(BigDecimal clearingFee) {
    this.clearingFee = clearingFee;
  }

  public void setClearingFeeDc(Character clearingFeeDc) {
    this.clearingFeeDc = clearingFeeDc;
  }

  public void setClearingFeeCurCode(String clearingFeeCurCode) {
    this.clearingFeeCurCode = clearingFeeCurCode;
  }

  public void setCounterValue(BigDecimal counterValue) {
    this.counterValue = counterValue;
  }

  public void setCounterValueDc(Character counterValueDc) {
    this.counterValueDc = counterValueDc;
  }

  public void setCounterValueCurCode(String counterValueCurCode) {
    this.counterValueCurCode = counterValueCurCode;
  }

  public void setCouponInterest(BigDecimal couponInterest) {
    this.couponInterest = couponInterest;
  }

  public void setCouponInterestDc(Character couponInterestDc) {
    this.couponInterestDc = couponInterestDc;
  }

  public void setEffectiveValue(BigDecimal effectiveValue) {
    this.effectiveValue = effectiveValue;
  }

  public void setEffectiveValueDc(char effectiveValueDc) {
    this.effectiveValueDc = effectiveValueDc;
  }

  public void setTransactionPrice(BigDecimal transactionPrice) {
    this.transactionPrice = transactionPrice;
  }

  public void setTransactionDate(Date transactionDate) {
    this.transactionDate = transactionDate;
  }

  public void setSettlementDate(Date settlementDate) {
    this.settlementDate = settlementDate;
  }

  public void setUnsettledReference(String unsettledReference) {
    this.unsettledReference = unsettledReference;
  }

  public void setExternalTransactionIdExchange(String externalTransactionIdExchange) {
    this.externalTransactionIdExchange = externalTransactionIdExchange;
  }

  public void setSettlementInstructionReference(String settlementInstructionReference) {
    this.settlementInstructionReference = settlementInstructionReference;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public void setIsinCode(String isinCode) {
    this.isinCode = isinCode;
  }

  public void setTraderInitials(String traderInitials) {
    this.traderInitials = traderInitials;
  }

  public void setUlvTradingUnit(BigDecimal ulvTradingUnit) {
    this.ulvTradingUnit = ulvTradingUnit;
  }

  public void setTransactionOrigin(String transactionOrigin) {
    this.transactionOrigin = transactionOrigin;
  }

  public void setExecTradingId(String execTradingId) {
    this.execTradingId = execTradingId;
  }

  public void setDepotId(String depotId) {
    this.depotId = depotId;
  }

  public void setSafeKeepingId(String safeKeepingId) {
    this.safeKeepingId = safeKeepingId;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public void setTransactionTypeCode(String transactionTypeCode) {
    this.transactionTypeCode = transactionTypeCode;
  }

  public void setExternalPositionAccountId(String externalPositionAccountId) {
    this.externalPositionAccountId = externalPositionAccountId;
  }

  public void setClearingAccount(String clearingAccount) {
    this.clearingAccount = clearingAccount;
  }

  public void setSpanishCsdAccountType(char spanishCsdAccountType) {
    this.spanishCsdAccountType = spanishCsdAccountType;
  }

  public void setOwnerReference(String ownerReference) {
    this.ownerReference = ownerReference;
  }

  public void setHoldOrReleaseStatus(char holdOrReleaseStatus) {
    this.holdOrReleaseStatus = holdOrReleaseStatus;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }
  
}
