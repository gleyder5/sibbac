package sibbac.business.comunicaciones.euroccp.titulares.mapper;

import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReferenceStaticDataDTO;
import sibbac.business.comunicaciones.euroccp.titulares.enums.EuroCcpOwnershipReferenceStaticDataFields;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpOwnershipReferenceStaticDataFieldSetMapper extends
                                                                WrapperAbstractFieldSetMapper<EuroCcpOwnershipReferenceStaticDataDTO> {

  public EuroCcpOwnershipReferenceStaticDataFieldSetMapper() {
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpOwnershipReferenceStaticDataFields.values();
  }

  @Override
  public EuroCcpOwnershipReferenceStaticDataDTO getNewInstance() {
    return new EuroCcpOwnershipReferenceStaticDataDTO();
  }

}
