package sibbac.business.comunicaciones.ordenes.bo;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXParseException;

import sibbac.business.comunicaciones.ordenes.dto.Asignacion;
import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenDuplicateBookingReceptionException;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenForeingMarketReceptionException;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenInternalReceptionException;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenRejectReceptionException;
import sibbac.business.comunicaciones.ordenes.mapper.XmlTmct0ord;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.SalesTraderDao;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0NemotecnicosBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Nemotecnicos;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0ord;

import com.sv.sibbac.multicenter.Allocation;
import com.sv.sibbac.multicenter.AllocationID;
import com.sv.sibbac.multicenter.BookingType;
import com.sv.sibbac.multicenter.DynamicValue;
import com.sv.sibbac.multicenter.ExecutionGroupType;
import com.sv.sibbac.multicenter.ExecutionID;
import com.sv.sibbac.multicenter.ExecutionType;
import com.sv.sibbac.multicenter.OrderType;
import com.sv.sibbac.multicenter.Orders;
import com.sv.sibbac.multicenter.util.MulticenterOrdersUtils;
import com.sv.sibbac.multicenter.util.MulticenterRejectionsUtils;

/**
 * LoadXmlOrdenFidessaInputStreamBo
 * 
 */
@Service
public class LoadXmlOrdenFidessaInputStreamBo {

  protected static final Logger LOG = LoggerFactory.getLogger(LoadXmlOrdenFidessaInputStreamBo.class);

  public static final String CDUSUAUD = "SIBBAC20";

  @Autowired
  private XmlTmct0ord xmlTmct0ord;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0brkBo brkBo;

  @Autowired
  private Tmct0xasBo xasBo;

  @Autowired
  private Tmct0NemotecnicosBo nmotecnicoBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;

  @Autowired
  private SalesTraderDao salesTraderDao;

  /**
   * LoadXmlOrdenFidessaInputStreamBo
   * 
   */
  public LoadXmlOrdenFidessaInputStreamBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Orders parseAndValidate(InputStream file, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws XmlOrdenExceptionReception, XmlOrdenInternalReceptionException, XmlOrdenForeingMarketReceptionException {
    LOG.trace("[LoadXmlOrdenFidessaInputStreamBo] Inicio carga codigo de seguimiento: {}", file.hashCode());
    MulticenterOrdersUtils multicenterOrdersUtils = new MulticenterOrdersUtils();
    Orders orders = readDocument(multicenterOrdersUtils, file);

    try {
      this.diagnosticControl(multicenterOrdersUtils, file, orders, cachedConfig);
    }
    catch (XmlOrdenRejectReceptionException e) {
      throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(),
          orders.getOrder().getBooking().getNbooking(), "", "", e.getMessage(), e);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(),
          orders.getOrder().getBooking().getNbooking(), "", "", String.format("%s %s",
              "Error inesperado haciendo el diagnostico.", e.getMessage()), e);
    }
    String nuorden = orders.getOrder().getNuorden();

    Tmct0ord tmct0ord = null;
    try {
      tmct0ord = ordBo.findById(nuorden);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(),
          orders.getOrder().getBooking().getNbooking(), "", "", String.format("%s: '%s' %s",
              "No se ha podido leer la Orma", nuorden, e.getMessage()), e);
    }

    if (tmct0ord != null) {

      LOG.trace("[loadxml] Se ha encontrado la orden ya cargada.");
      try {
        this.duplicateControl(orders.getOrder().getNuorden(), orders.getOrder().getBooking().getNbooking());
      }
      catch (XmlOrdenDuplicateBookingReceptionException e) {
        throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(), orders.getOrder().getBooking()
            .getNbooking(), "", "", e.getMessage(), e);
      }
      catch (DataIntegrityViolationException e) {
        throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(), orders.getOrder().getBooking()
            .getNbooking(), "", "", String.format("%s %s",
            "La orden no se puede borrar en sibbac, cree un nuevo booking.", e.getMessage()), e);
      }
      catch (Exception e) {
        throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(), orders.getOrder().getBooking()
            .getNbooking(), "", "", String.format("%s %s", "Error inesperado haciendo el control de duplicados.",
            e.getMessage()), e);
      }

    }
    return orders;
  }

  /**
   * Loadxml
   * 
   * Carga del documento xml en la base de datos.
   * 
   * @param InputStream file
   * @throws XmlOrdenExceptionReception the exception reception
   * @throws XmlOrdenForeingMarketReceptionException
   * @throws XmlOrdenInternalReceptionException
   * @throws
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public Tmct0bok loadxmlNewTransaction(Orders orders, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws XmlOrdenExceptionReception, XmlOrdenInternalReceptionException, XmlOrdenForeingMarketReceptionException {

    String nuorden = orders.getOrder().getNuorden();

    Tmct0ord tmct0ord = null;
    try {
      tmct0ord = ordBo.findById(nuorden);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(),
          orders.getOrder().getBooking().getNbooking(), "", "", String.format("%s: '%s' %s",
              "No se ha podido leer la Orma", nuorden, e.getMessage()), e);
    }

    if (tmct0ord == null) {
      tmct0ord = new Tmct0ord();
      tmct0ord.setNuorden(nuorden);
      LOG.trace("[loadxml] La orden es nueva.");
    }

    long timeMappingXmlIni = System.currentTimeMillis();
    Tmct0bok bok = null;
    try {
      bok = xmlTmct0ord.xml_mapping(tmct0ord, orders, cachedConfig);
    }
    catch (XmlOrdenForeingMarketReceptionException e) {
      throw e;
    }
    catch (XmlOrdenExceptionReception e) {
      throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(),
          orders.getOrder().getBooking().getNbooking(), "", "", e.getMessage(), e);
    }
    catch (DataIntegrityViolationException e) {
      throw new XmlOrdenInternalReceptionException(e);
    }
    catch (Exception e) {
      throw new XmlOrdenExceptionReception(orders.getOrder().getNuorden(),
          orders.getOrder().getBooking().getNbooking(), "", "", String.format("%s %s",
              "Error inesperado mapeando el xml en bbdd.", e.getMessage()), e);
    }
    long timeMappingXmlEnd = System.currentTimeMillis();
    LOG.trace("[LoadXmlOrdenFidessaInputStreamBo][ORDEN:{}] Mapeo de objeto Tmct0ord JDO finalizado ({}) In {} ms.",
        tmct0ord.getNuorden(), orders.hashCode(), timeMappingXmlEnd - timeMappingXmlIni);

    LOG.trace("[LoadXmlOrdenFidessaInputStreamBo][ORDEN:{}] Inicio carga en BBDD ({}) makepersistent ",
        tmct0ord.getNuorden(), orders.hashCode());

    LOG.trace("[LoadXmlOrdenFidessaInputStreamBo][ORDEN:{} Inicio carga en BBDD ({}) 'makepersistent '",
        tmct0ord.getNuorden(), orders.hashCode());
    // tmct0ord = ordBo.save(tmct0ord);
    long timeCommitXmlIni = System.currentTimeMillis();
    LOG.trace("[LoadXmlOrdenFidessaInputStreamBo][ORDEN:{}] Inicio carga en BBDD ({}) 'commit'", tmct0ord.getNuorden(),
        orders.hashCode());
    long timeCommitXmlEnd = System.currentTimeMillis();
    LOG.trace("[LoadXmlOrdenFidessaInputStreamBo][ORDEN:{}] Carga en BBDD finalizada ({}) 'commit' In {} ms.",
        tmct0ord.getNuorden(), orders.hashCode(), timeCommitXmlEnd - timeCommitXmlIni);

    // ordBo.flush();

    return bok;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void operativaInternacionalCargaXml(OrderType order, Tmct0bokId id) throws XmlOrdenExceptionReception {
    Tmct0bok bok = bokBo.findById(id);
    xmlTmct0ord.operativaInternacional(order, bok);
  }

  private Orders readDocument(MulticenterOrdersUtils multicenterOrdersUtils, InputStream file)
      throws XmlOrdenExceptionReception {
    try {

      return multicenterOrdersUtils.getOrders(file);

    }
    catch (JAXBException e) {

      String nuorden = "";
      String nbooking = "";
      String nucnfclt = "";

      StringBuffer sb = new StringBuffer();

      char[] cbuf = new char[1024];
      try {
        file.reset();
      }
      catch (Exception ex) {
        LOG.warn(ex.getMessage(), ex);
      }
      try (InputStreamReader isr = new InputStreamReader(file);) {
        int len = isr.read(cbuf);

        if (len > 0) {
          String linea = new String(cbuf);
          int pos = 0;
          if (linea.toUpperCase().indexOf("NUORDEN") > 1) {
            pos = linea.toUpperCase().indexOf("NUORDEN") + "NUORDEN".length() + 2;
            nuorden = linea.substring(pos, linea.indexOf("\"", pos));
          }
          else {
            nuorden = "";
            sb.append("Order not found. ");
          }
          if (linea.toUpperCase().indexOf("NBOOKING") > 1) {
            pos = linea.toUpperCase().indexOf("NBOOKING") + "NBOOKING".length() + 2;
            nbooking = linea.substring(pos, linea.indexOf("\"", pos));
          }
          else {
            nbooking = "";
            sb.append("Booking not found. ");
          }
          if (linea.toUpperCase().indexOf("NUCNFCLT") > 1) {
            pos = linea.toUpperCase().indexOf("NUCNFCLT") + "NUCNFCLT".length() + 2;
            nucnfclt = linea.substring(pos, linea.indexOf("\"", pos));
          }
          else {
            nucnfclt = "";
            sb.append("Allocation not found. ");
          }
        }
      }
      catch (IOException ex) {
        LOG.warn(String.format("INCIDENCIA- buscando nuorden:%s and nbooking:%s.", nuorden, nbooking), ex);
      }

      if (e instanceof UnmarshalException) {
        sb.append("XML FORMAT ERROR : ").append(e.getMessage());
        throw new XmlOrdenExceptionReception(nuorden, nbooking, "", nucnfclt, sb.toString(), (Exception) e);
      }
      else if (e.getLinkedException() != null
          && (e.getLinkedException() instanceof SAXParseException || e.getLinkedException() instanceof UnmarshalException)) {
        sb.append("XML FORMAT ERROR : ").append(e.getLinkedException().getMessage());
        throw new XmlOrdenExceptionReception(nuorden, nbooking, "", nucnfclt, sb.toString(),
            (Exception) e.getLinkedException());
      }
      else {
        sb.append(e.getMessage());
        throw new XmlOrdenExceptionReception(nuorden, nbooking, "", nucnfclt, sb.toString(), (Exception) e);
      }

    }
    catch (Exception e) {
      String nuorden = "";
      String nbooking = "";
      String nucnfclt = "";

      StringBuffer sb = new StringBuffer();

      char[] cbuf = new char[1024];
      try {
        file.reset();
      }
      catch (Exception ex) {
        LOG.warn(ex.getMessage(), ex);
      }
      try (InputStreamReader isr = new InputStreamReader(file);) {
        int len = isr.read(cbuf);

        if (len > 0) {
          String linea = new String(cbuf);
          int pos = 0;
          if (linea.toUpperCase().indexOf("NUORDEN") > 1) {
            pos = linea.toUpperCase().indexOf("NUORDEN") + "NUORDEN".length() + 2;
            nuorden = linea.substring(pos, linea.indexOf("\"", pos));
          }
          else {
            nuorden = "";
            sb.append("Order not found. ");
          }
          if (linea.toUpperCase().indexOf("NBOOKING") > 1) {
            pos = linea.toUpperCase().indexOf("NBOOKING") + "NBOOKING".length() + 2;
            nbooking = linea.substring(pos, linea.indexOf("\"", pos));
          }
          else {
            nbooking = "";
            sb.append("Booking not found. ");
          }
          if (linea.toUpperCase().indexOf("NUCNFCLT") > 1) {
            pos = linea.toUpperCase().indexOf("NUCNFCLT") + "NUCNFCLT".length() + 2;
            nucnfclt = linea.substring(pos, linea.indexOf("\"", pos));
          }
          else {
            nucnfclt = "";
            sb.append("Allocation not found. ");
          }
        }
      }
      catch (IOException ex) {
        LOG.warn(String.format("INCIDENCIA- buscando nuorden:%s and nbooking:%s.", nuorden, nbooking), ex);
      }

      throw new XmlOrdenExceptionReception(nuorden, nbooking, "", nucnfclt, sb.toString(), (Exception) e);
    }
  }

  /**
   * Diagnostic control.
   * 
   * Control de documentos xml con errores de formato
   * 
   * @return the int
   * @throws XmlOrdenExceptionReception
   */
  private void diagnosticControl(MulticenterOrdersUtils multicenterOrdersUtils, InputStream inputStreamFile,
      Orders orders, CachedValuesLoadXmlOrdenDTO cachedConfig) throws NullPointerException,
      XmlOrdenRejectReceptionException {

    String diagnosticResult = multicenterOrdersUtils.getErrorProcesing();

    if (StringUtils.isNotBlank(diagnosticResult)) {
      throw new XmlOrdenRejectReceptionException(diagnosticResult);
    }

    this.validateCcv(orders);

    // diagnosticControlExecutionsWithExecutionsGrups(inputStreamFile, orders);
    String cdclsmdo = cachedConfig.getSettlementExchangeCdclsmdo(xasBo, orders.getOrder().getSettlementExchange());
    validateTitlesAndDates(orders.getOrder(), orders.getOrder().getBooking(),cdclsmdo);

    

    if (StringUtils.isNotBlank(cdclsmdo) && "N".equalsIgnoreCase(cdclsmdo)) {
      // nacional
      for (ExecutionType execution : orders.getOrder().getBooking().getExecutions()) {
        if (execution.isAsignadoComp()) {
          this.validateAsigmentData(execution);
        }
        if (!cachedConfig.isMtf(brkBo, execution.getTradingVenue(), execution.getExecutionTradingVenue())) {
          this.validateMarketNumbersData(execution);
        }
        this.validateSegmentTradingVenue(execution);
        this.validateClearingPlatform(execution);
        this.validateExecutionTypeDynamicValues(execution, cachedConfig);
      }
    }
    else {
      // extranjero
      for (ExecutionType execution : orders.getOrder().getBooking().getExecutions()) {
        this.validateCdbrokerExtranjero(execution, cachedConfig);
        this.validateExecutionTypeDynamicValues(execution, cachedConfig);
      }

    }
  }

  private void validateCdbrokerExtranjero(ExecutionType execution, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws XmlOrdenRejectReceptionException {
    if (execution.getTradingVenue() == null || execution.getTradingVenue().trim().isEmpty()
        || !cachedConfig.existBroker(brkBo, execution.getTradingVenue())) {
      throw new XmlOrdenRejectReceptionException(String.format("Broker '%s' not found.", execution.getTradingVenue()));
    }
  }

  private void validateCcv(Orders orders) throws XmlOrdenRejectReceptionException {
    if (StringUtils.length(orders.getOrder().getCcv()) > 20) {
      throw new XmlOrdenRejectReceptionException(String.format("Longitud CCV incorrecta: '%s' ", orders.getOrder()
          .getCcv()));
    }
  }

  /**
   * @param order
   * @param booking
   * @param cdclsmdo 
   * @throws NullPointerException
   * @throws XmlOrdenExceptionReception
   */
  private void validateTitlesAndDates(OrderType order, BookingType booking, String cdclsmdo) throws XmlOrdenRejectReceptionException {
    String msgFinal;
    // BigDecimal feejecuc;
    // BigDecimal fevalor;
    String nucnfclt;
    String nuejecuc;
    Double nutitGr;
    Double nutitEjecucionesAcumulado = 0.0;
    Double nutitAlosAcumulado = 0.0;
    Double nutitGruposAcumulado = 0.0;
    Map<String, Double> titulosAcumuladosEaloByNuejecuc = new HashMap<String, Double>();
    Map<String, Double> titulosAcumuladosGrByNucnfclt = new HashMap<String, Double>();
    Map<String, Double> titulosEjesAcumuladosByNuejecuc = new HashMap<String, Double>();

    StringBuffer sb = new StringBuffer();
    if (booking.getEconomicDataBooking().getDates().getFhalta() == null) {
      sb.append("BookingType##BookingEconomicDataType##BookingDatesType##Fhalta required. ");

    }

    if (booking.getEconomicDataBooking().getDates().getFecontra() == null) {
      sb.append("BookingType##BookingEconomicDataType##BookingDatesType##Fecontra required. ");
    }

    if (booking.getEconomicDataBooking().getDates().getFevalide() == null) {
      sb.append("BookingType##BookingEconomicDataType##BookingDatesType##Fevalide required. ");
    }

    if (booking.getEconomicDataBooking().getDates().getFhejecuc() == null) {
      sb.append("BookingType##BookingEconomicDataType##BookingDatesType##Fhejecuc required. ");
    }

    XMLGregorianCalendar fhejecucionBooking = booking.getEconomicDataBooking().getDates().getFhejecuc();
    XMLGregorianCalendar fevalorBooking = booking.getEconomicDataBooking().getDates().getFevalor();

    Double nutitEjeBooking = booking.getEconomicDataBooking().getVolume().getNutiteje();

    for (ExecutionType exe : booking.getExecutions()) {
      nuejecuc = exe.getNuejecuc();
      nutitEjecucionesAcumulado = nutitEjecucionesAcumulado + exe.getNutiteje();
      if (titulosEjesAcumuladosByNuejecuc.get(nuejecuc) == null) {
        titulosEjesAcumuladosByNuejecuc.put(nuejecuc, 0.0);
      }
      titulosEjesAcumuladosByNuejecuc.put(nuejecuc, titulosEjesAcumuladosByNuejecuc.get(nuejecuc) + exe.getNutiteje());

      if (exe.getFhejecuc() == null || fhejecucionBooking == null
          || !this.equalsOnlyDate(exe.getFhejecuc(), fhejecucionBooking)) {
        sb.append(" BookingType: ").append(booking.getNbooking()).append(" Fhejecuc: ").append(fhejecucionBooking)
            .append(" distinct from ExecutionType : ").append(exe.getFhejecuc()).append(". ");
      }

      if ((exe.getFevalor() == null || fevalorBooking == null || exe.getFevalor().compare(fevalorBooking) != 0)  && "N".equalsIgnoreCase(cdclsmdo)) {
        sb.append(" BookingType: ").append(booking.getNbooking()).append(" Fevalor: ").append(fevalorBooking)
            .append(" distinct from ExecutionType : ").append(exe.getFevalor()).append(". ");
      }
    }

    if (nutitEjeBooking == null || nutitEjecucionesAcumulado.compareTo(nutitEjeBooking) != 0) {
      sb.append(" BookingType: ").append(booking.getNbooking()).append(" Quantity: ").append(nutitEjeBooking)
          .append(" distinct from ExecutionType : ").append(nutitEjecucionesAcumulado).append(". ");
    }

    for (ExecutionGroupType gr : booking.getExecutionGroups()) {
      nutitGr = gr.getNutitagr();
      nutitGruposAcumulado = nutitGruposAcumulado + nutitGr;
      for (AllocationID ealo : gr.getAllocation()) {
        nucnfclt = ealo.getNucnfclt();
        if (titulosAcumuladosGrByNucnfclt.get(nucnfclt) == null) {
          titulosAcumuladosGrByNucnfclt.put(nucnfclt, 0.0);
        }

        for (ExecutionID ealoEje : ealo.getExecutions()) {
          nuejecuc = ealoEje.getNuejecuc();
          titulosAcumuladosGrByNucnfclt.put(nucnfclt,
              titulosAcumuladosGrByNucnfclt.get(nucnfclt) + ealoEje.getNumtitasig());
          nutitGr = nutitGr - ealoEje.getNumtitasig();
          if (titulosAcumuladosEaloByNuejecuc.get(nuejecuc) == null) {
            titulosAcumuladosEaloByNuejecuc.put(nuejecuc, 0.0);
          }
          titulosAcumuladosEaloByNuejecuc.put(nuejecuc,
              titulosAcumuladosEaloByNuejecuc.get(nuejecuc) + ealoEje.getNumtitasig());
        }

        if (gr.getFhejecuc() == null || fhejecucionBooking == null
            || !this.equalsOnlyDate(gr.getFhejecuc(), fhejecucionBooking)) {
          sb.append(" BookingType: ").append(booking.getNbooking()).append(" Fhejecuc: ").append(fhejecucionBooking)
              .append(" distinct from ExecutionGroupType : ").append(gr.getFhejecuc()).append(". ");
        }


        
        //Solo se valida la fechaValor de booking y grupo ejecución si la orden es de nacional
        if ( (gr.getFevalor() == null || fevalorBooking == null ||gr.getFevalor().compare(fevalorBooking) != 0) && "N".equalsIgnoreCase(cdclsmdo)) {

          sb.append(" BookingType: ").append(booking.getNbooking()).append(" Fevalor: ").append(fevalorBooking)
              .append(" distinct from ExecutionGroupType : ").append(gr.getFevalor()).append(". ");
        }
      }
      if (nutitGr.compareTo(0.0) != 0) {
        sb.append("ExecutionGroupType: ").append(gr.getNuagreje()).append(" Quantity: ").append(gr.getNutitagr())
            .append(" distinct from ExecutionID's in: ").append(nutitGr).append(" titles of difference. ");
      }
    }// fin

    if (nutitEjeBooking == null || nutitGruposAcumulado.compareTo(nutitEjeBooking) != 0) {
      sb.append(" BookingType: ").append(booking.getNbooking()).append(" nutiteje: ").append(nutitEjeBooking)
          .append(" distinct from ExecutionGroupType : ").append(nutitGruposAcumulado).append(". ");
    }

    for (Allocation allo : booking.getAllocations()) {
      nucnfclt = allo.getNucnfclt();
      nutitAlosAcumulado = nutitAlosAcumulado + allo.getNutitcli();

      if (titulosAcumuladosGrByNucnfclt.get(nucnfclt) == null) {
        sb.append("ExecutionGroupType's ").append(" not contains Allocation: ").append(nucnfclt).append(". ");
      }
      else if (titulosAcumuladosGrByNucnfclt.get(nucnfclt).compareTo(allo.getNutitcli()) != 0) {
        sb.append("Allocation: ").append(nucnfclt).append(" quantity: ").append(allo.getNutitcli())
            .append(" distinct from ExecutionGroupType: ").append(titulosAcumuladosGrByNucnfclt.get(nucnfclt))
            .append(". ");
      }

      if (allo.getFeoperac() == null || fhejecucionBooking == null
          || !this.equalsOnlyDate(allo.getFeoperac(), fhejecucionBooking)) {
        sb.append(" BookingType: ").append(booking.getNbooking()).append(" Fhejecuc: ").append(fhejecucionBooking)
            .append(" distinct from Allocation : ").append(allo.getFeoperac()).append(". ");
      }

      if (allo.getFevalor() == null || fevalorBooking == null || allo.getFevalor().compare(fevalorBooking) != 0) {
        sb.append(" BookingType: ").append(booking.getNbooking()).append(" Fevalor: ").append(fevalorBooking)
            .append(" distinct from Allocation : ").append(allo.getFevalor()).append(". ");
      }

    }

    if (nutitAlosAcumulado.compareTo(nutitEjeBooking) != 0) {
      sb.append("Quantity BookingType: ").append(nutitEjeBooking).append(" distinct from Allocation : ")
          .append(nutitAlosAcumulado).append(". ");
    }

    for (ExecutionType exe : booking.getExecutions()) {
      nuejecuc = exe.getNuejecuc();
      if (titulosAcumuladosEaloByNuejecuc.get(nuejecuc) == null) {
        sb.append("ExecutionGroupType's ").append(" not contains ExecutionType: ").append(nuejecuc).append(". ");
      }
      else if (titulosEjesAcumuladosByNuejecuc.get(nuejecuc).compareTo(titulosAcumuladosEaloByNuejecuc.get(nuejecuc)) != 0) {
        sb.append("ExecutionType: ").append(nuejecuc).append(" quantity: ").append(exe.getNutiteje())
            .append(" distinct from ExecutionGroupType: ").append(titulosAcumuladosEaloByNuejecuc.get(nuejecuc))
            .append(". ");
      }
    }
    msgFinal = sb.toString();
    if (StringUtils.isNotBlank(msgFinal)) {
      throw new XmlOrdenRejectReceptionException(msgFinal);
    }
  }

  private boolean equalsOnlyDate(XMLGregorianCalendar d1, XMLGregorianCalendar d2) {
    return d1.getYear() == d2.getYear() && d1.getMonth() == d2.getMonth() && d1.getDay() == d2.getDay();
  }

  private void validateAsigmentData(ExecutionType execution) throws XmlOrdenRejectReceptionException {
    String msg;
    Asignacion asignacion = new Asignacion();
    asignacion.setCodigoCuentaCompensacion(execution.getCtaComp());
    asignacion.setNemotecnico(execution.getNemotecnico());
    asignacion.setNombreCompensador(execution.getMiembroComp());
    asignacion.setReferenciaAsignacion(execution.getRefAsignacion());
    if (!asignacion.isOk()) {
      msg = String
          .format(
              "Clearing data is not valid-Nuejecuc:'%s'-RefOrdenMkt:'%s'-RefEjecucMkt:'%s'-CtaComp:'%s'-Nemotecnico:'%s'-MiembroComp:'%s'-RefAsignacion:'%s'.",
              execution.getNuejecuc(), execution.getRefOrdenMkt(), execution.getRefEjecucMkt(), execution.getCtaComp(),
              execution.getNemotecnico(), execution.getMiembroComp(), execution.getRefAsignacion());
      throw new XmlOrdenRejectReceptionException(msg);
    }
    else {
      if (StringUtils.isNotBlank(execution.getNemotecnico())) {
        Tmct0Nemotecnicos nemotecnico = nmotecnicoBo.findByNombre(execution.getNemotecnico());
        if (nemotecnico == null) {
          msg = String
              .format(
                  "Clearing data is not valid-Nuejecuc:'%s'-RefOrdenMkt:'%s'-RefEjecucMkt:'%s'-Nemotecnico:'%s' Not in database.",
                  execution.getNuejecuc(), execution.getRefOrdenMkt(), execution.getRefEjecucMkt(),
                  execution.getNemotecnico());
          throw new XmlOrdenRejectReceptionException(msg);
        }
      }
      else if (StringUtils.isNotBlank(execution.getMiembroComp())) {
        Tmct0Compensador compensador = compensadorBo.findByNbNombre(execution.getMiembroComp());
        if (compensador == null) {

          msg = String
              .format(
                  "Clearing data is not valid-Nuejecuc:'%s'-RefOrdenMkt:'%s'-RefEjecucMkt:'%s'-MiembroComp:'%s'-RefAsignacion:'%s' Not in database.",
                  execution.getNuejecuc(), execution.getRefOrdenMkt(), execution.getRefEjecucMkt(),
                  execution.getMiembroComp(), execution.getRefAsignacion());
          throw new XmlOrdenRejectReceptionException(msg);
        }

      }
      else if (StringUtils.isNotBlank(execution.getCtaComp())) {
        Tmct0CuentasDeCompensacion cuentaCompensacion = cuentaCompensacionBo.findByCdCodigo(execution.getCtaComp());
        if (cuentaCompensacion == null) {
          msg = String
              .format(
                  "Clearing data is not valid-Nuejecuc:'%s'-RefOrdenMkt:'%s'-RefEjecucMkt:'%s'-CtaComp:'%s' Not in database.",
                  execution.getNuejecuc(), execution.getRefOrdenMkt(), execution.getRefEjecucMkt(),
                  execution.getCtaComp());
          throw new XmlOrdenRejectReceptionException(msg);
        }
      }
    }
  }

  private void validateMarketNumbersData(ExecutionType execution) throws XmlOrdenRejectReceptionException {
    String msg;
    if (StringUtils.isBlank(execution.getRefOrdenMkt())) {
      msg = String.format("Error the field RefOrdenMkt must come fill in execution '%s'.", execution.getNuejecuc()
          .trim());
      throw new XmlOrdenRejectReceptionException(msg);
    }
    else {
      if (!StringUtils.isNumeric(execution.getRefOrdenMkt())) {
        msg = String.format("Error the field RefOrdenMkt: '%s' must come fill in Nuejecuc: '%s' and must be numeric.",
            execution.getRefOrdenMkt(), execution.getNuejecuc());
        throw new XmlOrdenRejectReceptionException(msg);
      }

    }

    if (StringUtils.isBlank(execution.getRefEjecucMkt())) {
      msg = String.format("Error the field RefEjecucMkt must come fill in execution '%s'.", execution.getNuejecuc()
          .trim());
      throw new XmlOrdenRejectReceptionException(msg);
    }
    else {
      if (!StringUtils.isNumeric(execution.getRefEjecucMkt())) {
        msg = String.format("Error the field RefEjecucMkt: '%s' must come fill in Nuejecuc: '%s' and must be numeric.",
            execution.getRefEjecucMkt(), execution.getNuejecuc());
        throw new XmlOrdenRejectReceptionException(msg);
      }

    }

  }

  private void validateSegmentTradingVenue(ExecutionType execution) throws XmlOrdenRejectReceptionException {

    if (StringUtils.isBlank(execution.getSegmentTradingVenue())) {
      String msg = String.format("Error the field SegmentTradingVenue: must come fill in Nuejecuc: '%s'.",
          execution.getNuejecuc());
      throw new XmlOrdenRejectReceptionException(msg);
    }
  }

  private void validateClearingPlatform(ExecutionType execution) throws XmlOrdenRejectReceptionException {
    if (StringUtils.isBlank(execution.getClearingPlatform())) {
      String msg = String.format("Error the field ClearingPlatform: must come fill in Nuejecuc: '%s'.",
          execution.getNuejecuc());
      throw new XmlOrdenRejectReceptionException(msg);
    }
  }

  private void validateExecutionTypeDynamicValues(ExecutionType execution, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws XmlOrdenRejectReceptionException {
    if (execution.getDynamicValues() != null && execution.getDynamicValues().getDynamicValues() != null) {
      Date feejecuc = execution.getFhejecuc().toGregorianCalendar().getTime();
      boolean excludeExecDecision = false;
      boolean excludeInvDecision = false;
      for (DynamicValue dynamicValue : execution.getDynamicValues().getDynamicValues()) {
        // typeInvDecision
        if (dynamicValue.getName().equals("typeExecDecision") && dynamicValue.getValueSt().equals("A")) {
          LOG.trace("[validateDynamicValue] typeExecDecision == 'A' abort executionDecision dynamic values validation.");
          excludeExecDecision = true;
        }

        if (dynamicValue.getName().equals("typeInvDecision") && dynamicValue.getValueSt().equals("A")) {
          LOG.trace("[validateDynamicValue] typeInvDecision == 'A' abort investmentDecision dynamic values validation.");
          excludeInvDecision = true;
        }
      }

      for (DynamicValue dynamicValue : execution.getDynamicValues().getDynamicValues()) {
        LOG.trace("[validateDynamicValue] name == {}, value=={}, valueSt=={}", dynamicValue.getName(),
            dynamicValue.getValue(), dynamicValue.getValueSt());
        switch (dynamicValue.getName()) {
          case "executionDecision":
            if (excludeExecDecision) {
              break;
            }
            if (dynamicValue.getValueSt() != null && !dynamicValue.getValueSt().isEmpty()) {
              Integer valueInteger;
              try {
                valueInteger = Integer.parseInt(dynamicValue.getValueSt());
              }
              catch (NumberFormatException e) {
                LOG.warn(e.getMessage());
                LOG.trace(e.getMessage(), e);
                throw new XmlOrdenRejectReceptionException(String.format(
                    "Value of 'executionDecision' == '%s' not valid. Must be numeric.", dynamicValue.getValueSt()));
              }
              if (valueInteger >= 100) {
                if (!cachedConfig.existsSalesTrade(salesTraderDao, valueInteger, feejecuc)) {
                  throw new XmlOrdenRejectReceptionException(String.format(
                      "Value of executionDecision == '%s' not found in static data.", dynamicValue.getValueSt()));
                }
              }
            }
            break;
          case "investmentDecision":
            if (excludeInvDecision) {
              break;
            }
            if (dynamicValue.getValueSt() != null && !dynamicValue.getValueSt().isEmpty()) {
              Integer valueInteger;
              try {
                valueInteger = Integer.parseInt(dynamicValue.getValueSt());
              }
              catch (NumberFormatException e) {
                LOG.warn(e.getMessage());
                LOG.trace(e.getMessage(), e);
                throw new XmlOrdenRejectReceptionException(String.format(
                    "Value of 'investmentDecision' == '%s' not valid. Must be numeric.", dynamicValue.getValueSt()));
              }
              if (valueInteger >= 100) {
                if (!cachedConfig.existsSalesTrade(salesTraderDao, valueInteger, feejecuc)) {
                  throw new XmlOrdenRejectReceptionException(String.format(
                      "Value of investmentDecision == '%s' not found in static data.", dynamicValue.getValueSt()));
                }
              }

            }
            break;

          default:
            break;
        }
      }
    }
  }

  /**
   * Duplicate control.
   * 
   * Control de documentos xml duplicados return DC_COD0, orden no existe en la
   * base de datos return DC_COD1, orden existe en la base de datos, nuevo
   * booking return DC_DUP, orden-booking duplicado en la base de datos
   * 
   * @return the int
   * @throws XmlOrdenExceptionReception
   * @throws XmlOrdenInternalReceptionException
   */
  private Tmct0bok duplicateControl(String nuorden, String nbooking) throws XmlOrdenDuplicateBookingReceptionException,
      XmlOrdenInternalReceptionException {

    /*
     * Control de duplicados 1.- comprobamos si esta o no la orden en la base de
     * datos 2.- si no esta en la base de datos return DC_COD0 3.- si esta en la
     * base de datos 4.- comprobamos si existe el booking 5.- si no esta en la
     * base de datos return DC_COD1 6.- si esta en la base de datos 7.-
     * comprobamos estados causistica, (por definir) 8.- relacion orden booking
     * duplicado return DC_COD2
     */
    String msg;
    Tmct0bok tmct0bok = bokBo.findById(new Tmct0bokId(nbooking, nuorden));
    boolean duplicateBooking = false;
    boolean nacional = true;
    if (tmct0bok != null) {
      // posible duplicado
      if ('E' == tmct0bok.getTmct0ord().getCdclsmdo()) {
        nacional = false;
        if (tmct0bok.getTmct0sta() == null) {
          msg = String.format("Estado internacional requerido Nuorden:%s Nbooking:%s.", nuorden, nbooking);
          throw new XmlOrdenInternalReceptionException(msg);
        }

        if (!"002".equals(tmct0bok.getTmct0sta().getId().getCdestado().trim())
            && !"003".equals(tmct0bok.getTmct0sta().getId().getCdestado().trim())) {
          duplicateBooking = true;
        }
      }
      else if ('N' == tmct0bok.getTmct0ord().getCdclsmdo()) {
        if (tmct0bok.getTmct0estadoByCdestadoasig() == null) {
          msg = String.format("Estado nacional requerido Nuorden:%s Nbooking:%s.", nuorden, nbooking);
          throw new XmlOrdenInternalReceptionException(msg);
        }
        if (EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId() != tmct0bok.getTmct0estadoByCdestadoasig().getIdestado()) {
          duplicateBooking = true;
        }

      }
      else {
        msg = String.format("Valor cdclsmod no reconocido: %s", tmct0bok.getTmct0ord().getCdclsmdo());
        throw new XmlOrdenInternalReceptionException(msg);
      }

      if (duplicateBooking) {
        if (nacional) {
          msg = String.format("Duplicate Booking. Existent booking is in status: %s.", tmct0bok
              .getTmct0estadoByCdestadoasig().getNombre());
        }
        else {
          msg = String.format("Duplicate Booking. Existent booking is in status: %s.", tmct0bok.getTmct0sta()
              .getDsestado());
        }
        throw new XmlOrdenDuplicateBookingReceptionException(nuorden, nbooking, "", msg);
      }
      else {
        bokBo.deleteTmct0bokNewTransaction(tmct0bok.getId());
      }
    }

    return tmct0bok;

  }

  /**
   * Gets the document.
   * 
   * @return the document
   */
  public String getDocument(MulticenterOrdersUtils multicenterOrdersUtils, Orders orders) throws JAXBException,
      IOException {
    return multicenterOrdersUtils.createOrders(orders);
  }

  public String createRejection(String nuorden, String nbooking, String nucnfclt, String msg) throws JAXBException,
      IOException {

    MulticenterRejectionsUtils rejectutils = new MulticenterRejectionsUtils();
    String reject = rejectutils.createRejection(nuorden, nbooking, nucnfclt, "", msg);

    return reject;
  }
}
