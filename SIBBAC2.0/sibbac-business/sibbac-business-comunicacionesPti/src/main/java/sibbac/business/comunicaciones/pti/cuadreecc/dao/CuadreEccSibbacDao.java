package sibbac.business.comunicaciones.pti.cuadreecc.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;

@Component
public interface CuadreEccSibbacDao extends JpaRepository<CuadreEccSibbac, Long> {

  CuadreEccSibbac findById(Long id);

  @Modifying
  @Query("DELETE FROM CuadreEccSibbac s WHERE s.id = :id")
  Integer deleteById(@Param("id") Long id);

  List<CuadreEccSibbac> findAll();

  Integer countByEstado(Integer cdestadotit);

  Set<CuadreEccSibbac> findByEstado(Integer estado);

  List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperac(Character sentido, String numOp, java.util.Date feoperac);

  List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndIdCuadreEccEjecucion(Character sentido,
                                                                                String numOp,
                                                                                java.util.Date feoperac,
                                                                                long idCuadreEccEjecucion);

  List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndNumTitulosDesSibbacPdte(Character sentido,
                                                                                   String numOp,
                                                                                   java.util.Date feoperac,
                                                                                   BigDecimal numTitulosDesPdte);

  List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndRefTitular(Character sentido,
                                                                      String numOp,
                                                                      java.util.Date feoperac,
                                                                      String refTitular);

  List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndRefTitularAndNumTitulosDesSibbacPdte(Character sentido,
                                                                                                String numOp,
                                                                                                java.util.Date feoperac,
                                                                                                String refTitular,
                                                                                                BigDecimal numTitulosDesPdte);

  List<CuadreEccSibbac> findByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(Date feoperac,
                                                                                                                                          String cdcamara,
                                                                                                                                          String cdisin,
                                                                                                                                          String cdmiembromkt,
                                                                                                                                          Character sentido,
                                                                                                                                          String cdsegmento,
                                                                                                                                          String cdoperacionmkt,
                                                                                                                                          String nuordenmkt,
                                                                                                                                          String nuexemkt);

  @Query("SELECT o.id FROM CuadreEccSibbac o WHERE o.nuorden = :nuorden AND o.nbooking = :nbooking AND o.nucnfclt = :nucnfclt ")
  List<Long> findAllIdsByNuordenAndNbookingAndNucnfclt(@Param("nuorden") String nuorden,
                                                       @Param("nbooking") String nbooking,
                                                       @Param("nucnfclt") String nucnflct);

  @Query("SELECT DISTINCT o.fevalor FROM CuadreEccSibbac o WHERE o.estadoProcesado = :estadoProcesado ")
  List<Date> findAllDistinctFevalorByEstadoProcesado(@Param("estadoProcesado") Integer estadoProcesado);

  @Query("SELECT new sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO( o.id ) "
         + " FROM CuadreEccSibbac o WHERE o.estadoProcesado = :estadoProcesado AND o.fevalor = :fevalor")
  List<CuadreEccIdDTO> findAllCuadreEccIdDTOByEstadoProcesadoAndFevalor(@Param("estadoProcesado") Integer estadoProcesado,
                                                                        @Param("fevalor") Date fevalor);

  @Query("SELECT DISTINCT new sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO( o.nuorden, o.nbooking, o.nucnfclt ) "
         + " FROM CuadreEccSibbac o WHERE o.estadoProcesado = :estadoProcesado AND o.fevalor = :fevalor ORDER BY o.nuorden ASC, o.nbooking ASC, o.nucnfclt ASC")
  List<CuadreEccDbReaderRecordBeanDTO> findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalor(@Param("estadoProcesado") Integer estadoProcesado,
                                                                                                                @Param("fevalor") Date fevalor,
                                                                                                                Pageable page);

  @Query("SELECT o FROM CuadreEccSibbac o WHERE o.estado =:cdestadotit")
  List<CuadreEccSibbac> findAllByEstado(@Param("cdestadotit") Integer cdestadotit);

  @Query("SELECT sum(o.numTitulosDesSibbac) FROM CuadreEccSibbac o WHERE o.feoperac = :feoperac "
         + " AND o.numOp = :numerooperacion AND o.sentido = :sentido")
  BigDecimal sumNumTitulosDesSibbacBySentidoAndNumOpAndFeoperac(@Param("sentido") Character sentido,
                                                                @Param("numerooperacion") String numOp,
                                                                @Param("feoperac") java.util.Date feoperac);

  @Modifying
  @Query("UPDATE CuadreEccSibbac o SET o.estadoProcesado = :pEstadoProcesado, o.auditDate =:pAuditDate "
         + " WHERE o.feoperac = :feoperac AND o.numOp = :numerooperacion AND o.sentido = :sentido ")
  Integer updateEstadoProcesadoBySentidoAndNumOpAndFeoperac(@Param("pEstadoProcesado") Integer setEstadoProcesado,
                                                            @Param("pAuditDate") Date auditDate,
                                                            @Param("sentido") Character sentido,
                                                            @Param("numerooperacion") String numOp,
                                                            @Param("feoperac") java.util.Date feoperac);

  @Modifying
  @Query("UPDATE CuadreEccSibbac o SET o.estadoProcesado = :pEstadoProcesado, o.auditDate =:pAuditDate "
         + " WHERE o.nuorden = :nuorden AND o.nbooking = :nbooking AND o.nucnfclt = :nucnfclt ")
  Integer updateEstadoProcesadoByNuordenAndNbookingAndNucnfclt(@Param("pEstadoProcesado") Integer setEstadoProcesado,
                                                               @Param("pAuditDate") Date auditDate,
                                                               @Param("nuorden") String nuorden,
                                                               @Param("nbooking") String nbooking,
                                                               @Param("nucnfclt") String nucnflct);

  @Query("SELECT o.id FROM CuadreEccSibbac o "
         + " WHERE o.feoperac = :feoperac AND o.cdcamara = :cdcamara AND o.cdisin = :cdisin AND o.cdmiembromkt = :cdmiembromkt "
         + " AND o.sentido = :sentido AND o.cdsegmento = :cdsegmento AND o.cdoperacionmkt = :cdmoperacionmkt "
         + " AND o.nuordenmkt = :nuordenmkt AND o.nuexemkt = :nuexemkt")
  List<Long> findAllIdsByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(@Param("feoperac") Date feoperac,
                                                                                                                                     @Param("cdcamara") String cdcamara,
                                                                                                                                     @Param("cdisin") String cdisin,
                                                                                                                                     @Param("cdmiembromkt") String cdmiembromkt,
                                                                                                                                     @Param("sentido") Character sentido,
                                                                                                                                     @Param("cdsegmento") String cdsegmento,
                                                                                                                                     @Param("cdmoperacionmkt") String cdoperacionmkt,
                                                                                                                                     @Param("nuordenmkt") String nuordenmkt,
                                                                                                                                     @Param("nuexemkt") String nuexemkt);

  // Set<CuadreEccSibbac> findAllGroupByNuordenAndNbookingAndNucnfclt();

  @Query("SELECT o FROM CuadreEccSibbac o WHERE o.nuorden =:nuorden AND o.nbooking=:nbooking AND o.nucnfclt=:nucnfclt ORDER BY o.id ASC")
  List<CuadreEccSibbac> findAllCuadreEccSibbacByNuordenAndNbookingAndNucnfclt(@Param("nuorden") String nuorden,
                                                                              @Param("nbooking") String nbooking,
                                                                              @Param("nucnfclt") String nucnfclt);

  @Query("SELECT o FROM CuadreEccSibbac o WHERE o.nuorden =:nuorden AND o.nbooking=:nbooking AND o.nucnfclt=:nucnfclt "
         + " AND o.estadoProcesado = :estadoProcesado ORDER BY o.id ASC")
  List<CuadreEccSibbac> findAllCuadreEccSibbacByNuordenAndNbookingAndNucnfcltAndEstadoProcesado(@Param("nuorden") String nuorden,
                                                                                                @Param("nbooking") String nbooking,
                                                                                                @Param("nucnfclt") String nucnfclt,
                                                                                                @Param("estadoProcesado") int estadoProcesado);

  @Query("SELECT o FROM Tmct0desglosecamara o WHERE o.tmct0alc.id.nuorden =:nuorden AND o.tmct0alc.id.nbooking=:nbooking AND o.tmct0alc.id.nucnfclt=:nucnfclt AND o.tmct0alc.id.nucnfliq=:nucnfliq ")
  List<Tmct0desglosecamara> getDcs(@Param("nuorden") String nuorden,
                                   @Param("nbooking") String nbooking,
                                   @Param("nucnfclt") String nucnfclt,
                                   @Param("nucnfliq") short nucnfliq);

  @Query("SELECT o FROM Tmct0CamaraCompensacion o , Tmct0desglosecamara d WHERE d.tmct0alc.id.nuorden =:nuorden AND d.tmct0alc.id.nbooking=:nbooking AND d.tmct0alc.id.nucnfclt=:nucnfclt AND d.tmct0alc.id.nucnfliq=:nucnfliq "
         + " and o.idCamaraCompensacion=d.tmct0CamaraCompensacion.idCamaraCompensacion")
  Tmct0CamaraCompensacion getCamara(@Param("nuorden") String nuorden,
                                    @Param("nbooking") String nbooking,
                                    @Param("nucnfclt") String nucnfclt,
                                    @Param("nucnfliq") short nucnfliq);

  @Query("SELECT o FROM Tmct0infocompensacion o , Tmct0desglosecamara d WHERE d.tmct0alc.id.nuorden =:nuorden AND d.tmct0alc.id.nbooking=:nbooking AND d.tmct0alc.id.nucnfclt=:nucnfclt AND d.tmct0alc.id.nucnfliq=:nucnfliq "
         + " and o.idinfocomp=d.tmct0infocompensacion.idinfocomp")
  Tmct0infocompensacion getInfoCompensacion(@Param("nuorden") String nuorden,
                                            @Param("nbooking") String nbooking,
                                            @Param("nucnfclt") String nucnfclt,
                                            @Param("nucnfliq") short nucnfliq);

  @Query("SELECT o FROM Tmct0movimientooperacionnuevaecc o WHERE o.nudesglose = (:nudesglose )")
  Set<Tmct0movimientooperacionnuevaecc> getMvs(@Param("nudesglose") Long nudesglose);

  @Query("SELECT o FROM CuadreEccSibbac o WHERE o.nuorden =:nuorden AND o.nbooking=:nbooking AND o.nucnfclt=:nucnfclt")
  List<CuadreEccSibbac> findAlos(@Param("nuorden") String nuorden,
                                 @Param("nbooking") String nbooking,
                                 @Param("nucnfclt") String nucnfclt);

  @Query("SELECT new sibbac.business.fase0.database.model.Tmct0aloId(o.nucnfclt, o.nbooking, o.nuorden) FROM CuadreEccSibbac o "
         + " WHERE o.numTitulosDesSibbacPdte > 0 AND o.estado < :estado AND o.feoperac < :feoperacLt "
         + " GROUP BY o.nuorden, o.nbooking, o.nucnfclt")
  // GROUP BY nuorden, nbooking, nucnfclt, numerotitulosalo ")
  List<Tmct0aloId> findAllAlosCrearDesglosesIntermediarioFinanciero(@Param("estado") int estado,
                                                                    @Param("feoperacLt") Date feoperacLt);

  @Query("SELECT count(*) FROM CuadreEccSibbac o WHERE o.feoperac = :fecha AND o.numOp = :operacion AND o.numTitulosDesSibbacPdte > :titulospdte ")
  Integer countByFeoperacAndNumerooperacionAndNumeroTitulosDespdteGt(@Param("fecha") java.util.Date feoperac,
                                                                     @Param("operacion") String numerooperacion,
                                                                     @Param("titulospdte") BigDecimal titulospdte);

  @Modifying
  @Query("DELETE FROM CuadreEccSibbac o WHERE o.estado = :cdestadotitEleIF "
         + " AND EXISTS (SELECT 1 FROM Tmct0alo a WHERE a.id.nuorden=o.nuorden AND a.id.nbooking=o.nbooking AND a.id.nucnfclt=o.nucnfclt AND a.cdestadotit >= :cdestadotitAloGte )")
  Integer deleteByEstadoAndTmct0aloCdestadotitGt(@Param("cdestadotitEleIF") Integer cdestadotitEleIf,
                                                 @Param("cdestadotitAloGte") Integer cdestadotitAloGte);

  @Modifying
  @Query("DELETE FROM CuadreEccSibbac o WHERE o.feoperac = :feoperac "
         + " AND o.numOp = :numerooperacion AND o.sentido = :sentido")
  Integer deleteByFeoperacAndNumerooperacionAndSentido(@Param("feoperac") Date feoperac,
                                                       @Param("numerooperacion") String numerooperacion,
                                                       @Param("sentido") Character sentido);

  @Query("SELECT o.id FROM CuadreEccSibbac o WHERE o.feoperac = :feoperac "
         + " AND o.numOp = :numerooperacion AND o.sentido = :sentido")
  List<Long> findAllIdsByFeoperacAndNumerooperacionAndSentido(@Param("feoperac") Date feoperac,
                                                              @Param("numerooperacion") String numerooperacion,
                                                              @Param("sentido") Character sentido);

  @Query("SELECT o.id FROM CuadreEccSibbac o WHERE o.feoperac = :feoperac "
         + " AND o.numOp = :numerooperacion AND o.sentido = :sentido AND o.idCuadreEccEjecucion = :idCuadreEccEjecucion ")
  List<Long> findAllIdsByFeoperacAndNumerooperacionAndSentidoAndIdCuadreEccEjecucion(@Param("feoperac") Date feoperac,
                                                                                     @Param("numerooperacion") String numerooperacion,
                                                                                     @Param("sentido") Character sentido,
                                                                                     @Param("idCuadreEccEjecucion") Long idCuadreEccEjecucion);

  @Query("SELECT o.id FROM CuadreEccSibbac o WHERE o.idCuadreEccEjecucion =  :idCuadreEccEjecucion ")
  List<Long> findAllIdsByIdCuadreEccEjecucion(@Param("idCuadreEccEjecucion") long idCuadreEccEjecucion);

} // CuadreEccSibbacDao
