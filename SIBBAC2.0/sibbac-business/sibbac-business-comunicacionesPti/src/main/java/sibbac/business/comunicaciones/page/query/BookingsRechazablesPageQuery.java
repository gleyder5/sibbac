package sibbac.business.comunicaciones.page.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.DynamicColumn.ColumnType;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

@Component("BookingsRechazablesPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BookingsRechazablesPageQuery extends AbstractDynamicPageQuery {

  private static final String NBOOKING = "B.NBOOKING";
  private static final String REF_CLIENTE = "A.REF_CLIENTE";
  private static final String FECHA_CONTRATACION = "B.FETRADE";
  private static final String FECHA_LIQUIDACION = "B.FEVALOR";
  private static final String ALIAS = "B.CDALIAS";
  private static final String ISIN = "B.CDISIN";
  private static final String COMPRA_VENTA = "B.CDTPOPER";

  private static final String SELECT = "SELECT "
      + "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, A.REF_CLIENTE, EST.NOMBRE ESTADO, B.CDALIAS ALIAS, B.DESCRALI DESC_ALIAS, "
      + "B.CDTPOPER SENTIDO, B.CDISIN ISIN, B.NBVALORS DESC_ISIN, B.NUTITEJE TITULOS, B.CASEPTI CASE_PTI, COALESCE(DCT.CUENTA_COMPENSACION_DESTINO, '') CUENTA_COMPENSACION_DESTINO";

  private static final String FROM = "FROM TMCT0BOK B "
      + "INNER JOIN TMCT0ALO A ON A.NUORDEN = B.NUORDEN AND A.NBOOKING = B.NBOOKING "
      + "INNER JOIN TMCT0ESTADO EST ON B.CDESTADOASIG=EST.IDESTADO "
      + "LEFT JOIN TMCT0DESGLOSECAMARA DC ON  DC.NUORDEN = A.NUORDEN AND DC.NBOOKING = A.NBOOKING AND DC.NUCNFCLT = A.NUCNFCLT AND DC.CDESTADOASIG > {0} "
      + "LEFT JOIN TMCT0DESGLOSECLITIT DCT ON DCT.IDDESGLOSECAMARA = DC.IDDESGLOSECAMARA ";

  private static final String WHERE = " ( B.CDESTADOASIG = {0} OR B.CDESTADOASIG = {1} OR B.CDESTADOASIG = {2} OR B.CDESTADOASIG = {3} OR B.CDESTADOASIG = {4} "
      + " OR B.CDESTADOASIG = {5} OR B.CDESTADOASIG = {6} OR B.CDESTADOASIG = {7} OR B.CDESTADOASIG = {8} "
      + " OR B.CDESTADOASIG > {9} AND (B.CASEPTI <> {10} OR B.CASEPTI IS NULL) )";

  private static final String GROUP_BY = " GROUP BY A.NUORDEN, A.NBOOKING, A.NUCNFCLT, A.REF_CLIENTE, EST.NOMBRE, B.CDALIAS, B.DESCRALI, "
      + "B.CDTPOPER, B.CDISIN, B.NBVALORS, B.NUTITEJE,B.CASEPTI, DCT.CUENTA_COMPENSACION_DESTINO ";

  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("N. Orden", "NUORDEN"),
      new DynamicColumn("N. Booking", "NBOOKING"), new DynamicColumn("N. Desg. Cliente", "NUCNFCLT"),
      new DynamicColumn("Ref. Cliente", "REF_CLIENTE"), new DynamicColumn("Estado", "ESTADO"),
      new DynamicColumn("Alias", "ALIAS"), new DynamicColumn("Desc. Alias", "DESC_ALIAS"),
      new DynamicColumn("Sentido", "SENTIDO"), new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Desc. Isin", "DESC_ISIN"), new DynamicColumn("Titulos", "TITULOS", ColumnType.INT),
      new DynamicColumn("Case", "CASE_PTI"), new DynamicColumn("Cuenta", "CUENTA_COMPENSACION_DESTINO") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new DateDynamicFilter(FECHA_CONTRATACION, "F. Contratación"));
    res.add(new DateDynamicFilter(FECHA_LIQUIDACION, "F. Liquidación"));
    res.add(new StringDynamicFilter(COMPRA_VENTA, "Sentido"));
    res.add(new StringDynamicFilter(REF_CLIENTE, "Referencia cliente"));
    res.add(new StringDynamicFilter(NBOOKING, "Número booking"));
    res.add(new StringDynamicFilter(ALIAS, "Alias"));
    res.add(new StringDynamicFilter(ISIN, "ISIN"));

    return res;
  }

  public BookingsRechazablesPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return MessageFormat.format(FROM, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {

    return MessageFormat
        .format(WHERE, EstadosEnumerados.ASIGNACIONES.ERROR_DATOS_ASIGNACION.getId(),
            EstadosEnumerados.ASIGNACIONES.ERROR_DATOS_ANULACION.getId(),
            EstadosEnumerados.ASIGNACIONES.RECHAZADO_COMPENSADOR.getId(),
            EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR.getId(),
            EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL.getId(),
            EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSE_MANUAL.getId(),
            EstadosEnumerados.ASIGNACIONES.SIN_INSTRUCCIONES.getId(),
            EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId(),
            EstadosEnumerados.ASIGNACIONES.IMPUTADA_PROPIA.getId(), EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(),
            "'S'");
  }

  @Override
  public String getGroup() {
    return GROUP_BY;
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    boolean lanzarEx = true;
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      switch (filter.getField()) {
        case NBOOKING:
        case REF_CLIENTE:
        case FECHA_CONTRATACION:
        case FECHA_LIQUIDACION:

          if (!filter.getValues().isEmpty()) {
            lanzarEx = false;
          }
          break;

        default:
          break;
      }
      if (!lanzarEx) {
        break;
      }
    }
    if (lanzarEx) {
      throw new SIBBACBusinessException("Mandatory filter not set.");
    }
  }

}
