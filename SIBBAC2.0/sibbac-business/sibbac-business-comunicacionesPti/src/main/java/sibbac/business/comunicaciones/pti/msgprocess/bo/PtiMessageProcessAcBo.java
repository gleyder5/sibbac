package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.ac.dao.PtiEnvioAcDao;
import sibbac.business.comunicaciones.pti.ac.model.PtiEnvioAc;
import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.dao.CancelacionEccDao;
import sibbac.business.wrappers.database.model.CancelacionEcc;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.ReferenciasActuacion;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.ac.AC;
import sibbac.pti.messages.ac.ACControlBlock;
import sibbac.pti.messages.ac.R00;

@Service("ptiMessageProcessAcBo")
public class PtiMessageProcessAcBo extends PtiMessageProcess<AC> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessAcBo.class);

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private CancelacionEccDao acDao;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private PtiEnvioAcDao ptiAcDao;

  public PtiMessageProcessAcBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, AC ac, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {

    // Tenemos chicha.
    String referenciaDelMovimiento;
    String actuacion;

    R00 r00;

    // Miramos los diversos codigos de error...
    boolean respuestaOK = !hayAlgunError(ac);
    // Exploramos los mensajes de tipo R00.
    List<PTIMessageRecord> r00s = ac.getAll(PTIMessageRecordType.R00);
    if (CollectionUtils.isNotEmpty(r00s)) {

      for (PTIMessageRecord record : r00s) {
        r00 = (R00) record;

        // Cogemos los datos basicos.
        actuacion = r00.getActuacion();
        referenciaDelMovimiento = r00.getReferenciaDeMovimiento();
        this.procesarCancelacion(ac, referenciaDelMovimiento, actuacion, respuestaOK);
        this.procesarPtiEnvioAc(ac, referenciaDelMovimiento, actuacion, respuestaOK);
      }
    }
    else {
      LOG.error(". La recepcion de los mensaje R00 es nulo o vacio.");
    }
  }

  @Transactional
  private void procesarCancelacion(AC ac, String referenciaDelMovimiento, String actuacion, boolean respuestaOK) {
    String referenciaDelMovimientoEcc;

    List<CancelacionEcc> cancelacionesEcc = acDao.findAllByCdrefmovimiento(referenciaDelMovimiento);

    for (CancelacionEcc cancelacionEcc : cancelacionesEcc) {
      referenciaDelMovimientoEcc = cancelacionEcc.getCdrefmovimientoecc();
      this.procesarReferenciaMovimientoEcc(referenciaDelMovimientoEcc, actuacion, null);
      acDao.save(cancelacionEcc);
    }
  }

  @Transactional
  private void procesarPtiEnvioAc(AC ac, String referenciaDelMovimiento, String actuacion, boolean respuestaOK) {
    String referenciaDelMovimientoEcc;

    Collection<Object> listAlcsLog = new HashSet<Object>();

    String msg;

    msg = String.format("Recepcion AC rechazado ref.mov.ecc: '%s' err: '%s'", ac.getReferenciaECC(),
        ac.getErrorDeMensaje());

    List<PtiEnvioAc> acs = ptiAcDao.findAllByCdrefmovimiento(referenciaDelMovimiento);
    for (PtiEnvioAc ptiEnvioAc : acs) {
      referenciaDelMovimientoEcc = ptiEnvioAc.getCdrefmovimientoecc();
      this.procesarReferenciaMovimientoEcc(referenciaDelMovimientoEcc, actuacion, ptiEnvioAc.getFliquidacion());
      ptiEnvioAc.setCodigoError(ac.getErrorGeneral());
      if (respuestaOK) {
        ptiEnvioAc.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
      }
      else {
        ptiEnvioAc.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId());
      }
      ptiAcDao.save(ptiEnvioAc);

      if (!respuestaOK) {
        List<Tmct0movimientoecc> movs = movBo.findAllByCdrefmovimientoecc(referenciaDelMovimientoEcc);

        if (CollectionUtils.isNotEmpty(movs)) {
          for (Tmct0movimientoecc mov : movs) {
            if (mov.getTmct0alo() != null
                && mov.getTmct0alo().getTmct0estadoByCdestadoasig() != null
                && mov.getTmct0alo().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                    .getId()) {
              if (listAlcsLog.add(mov.getTmct0alo().getId())) {
                logBo.insertarRegistro(mov.getTmct0alo().getId().getNuorden(), mov.getTmct0alo().getId().getNbooking(),
                    mov.getTmct0alo().getId().getNucnfclt(), msg, mov.getTmct0alo().getTmct0estadoByCdestadoasig()
                        .getNombre(), "SIBBAC20");
              }
            }
            else if (mov.getTmct0desglosecamara() != null
                && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig() != null
                && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                    .getId()) {
              if (listAlcsLog.add(mov.getTmct0desglosecamara().getTmct0alc().getId())) {
                logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), mov
                    .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), mov.getTmct0desglosecamara()
                    .getTmct0alc().getId().getNucnfclt(), msg, mov.getTmct0desglosecamara()
                    .getTmct0estadoByCdestadoasig().getNombre(), "SIBBAC20");
              }
            }

          }
        }
      }
    }
  }

  @Transactional
  private void procesarReferenciaMovimientoEcc(String cdrefmovmientoecc, String actuacion, Date fechaliquidacion) {
    Tmct0alc alc;
    Tmct0desglosecamara desglosecamara;
    Tmct0alo alo;
    String dsEstado;
    String nuorden;
    String nbooking;
    String nucnfclt;
    StringBuilder sb;
    List<Tmct0movimientoecc> movimientosEcc = movBo.findAllByCdrefmovimientoecc(cdrefmovmientoecc);
    if (CollectionUtils.isEmpty(movimientosEcc)) {
      LOG.warn("AC NO ENCONTRAMOS REG QUE LO ENVIO.");
    }
    else {
      for (Tmct0movimientoecc movimientoEcc : movimientosEcc) {
        desglosecamara = movimientoEcc.getTmct0desglosecamara();

        alo = movimientoEcc.getTmct0alo();
        if (desglosecamara != null && alo == null) {
          alc = desglosecamara.getTmct0alc();
          alo = alc.getTmct0alo();
          dsEstado = desglosecamara.getTmct0estadoByCdestadoasig().getNombre();

          /*
           * recojo el check del desglose camara imputacion S3
           */

        }
        else {
          dsEstado = alo.getTmct0estadoByCdestadoasig().getNombre();
        }

        nuorden = alo.getId().getNuorden();
        nbooking = alo.getId().getNbooking();
        nucnfclt = alo.getId().getNucnfclt();
        sb = new StringBuilder("Recepcion AC ");
        sb.append("Ref.AC:").append(cdrefmovmientoecc.trim());
        sb.append("-Ref.Mov:").append(movimientoEcc.getCdrefmovimiento().trim());
        sb.append("-Ref.Mov.ECC:").append(movimientoEcc.getCdrefmovimientoecc().trim());
        if (actuacion.equals(ReferenciasActuacion.ACEPTADA)) {
          sb.append(ReferenciasActuacion.ACEPTADA).append("-ACEPTACION");
        }
        else if (actuacion.equals(ReferenciasActuacion.RECHAZADA)) {
          sb.append(ReferenciasActuacion.RECHAZADA).append("-RECHAZO");
        }
        else if (actuacion.equals(ReferenciasActuacion.ACEPTADA_COMPENSADOR_DESTINO)) {
          sb.append(ReferenciasActuacion.ACEPTADA_COMPENSADOR_DESTINO).append("-ACT. COMPENSADOR DESTINO");
        }
        else if (actuacion.equals(ReferenciasActuacion.RECHAZADA_COMPENSADOR_DESTINO)) {
          sb.append(ReferenciasActuacion.RECHAZADA_COMPENSADOR_DESTINO).append("-REC. COMPENSADOR DESTINO");
        }
        else if (actuacion.equals(ReferenciasActuacion.CANCELADO)) {
          sb.append(ReferenciasActuacion.CANCELADO).append("-CANCELACION");
        }

        if (desglosecamara != null
            && desglosecamara.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {
          desglosecamara.setTmct0estadoByCdestadoasig(new Tmct0estado(
              EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId()));
          desglosecamara.setAuditFechaCambio(new Date());
          desglosecamara.setAuditUser("SIBBAC20");
          dcBo.save(desglosecamara);
          logBo.insertarRegistro(nuorden, nbooking, nucnfclt, sb.toString(), dsEstado, "SIBBAC20");
        }
        else if (alo != null
            && alo.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          alo.setFhaudit(new Date());
          alo.setCdusuaud("SIBBAC20");
          alo.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES
              .getId()));
          aloBo.save(alo);
          logBo.insertarRegistro(nuorden, nbooking, nucnfclt, sb.toString(), dsEstado, "SIBBAC20");
        }
        movimientoEcc.setAuditFechaCambio(new Date());
        movimientoEcc.setAuditUser("SIBBAC20");
      }// fin for movimientos

      movBo.save(movimientosEcc);
    }
  }

  private boolean hayAlgunError(final AC ac) {
    String headerErrCode = ac.getHeader().getErrorCode();
    ACControlBlock cb = (ACControlBlock) ac.getControlBlock();
    String cbErrCode = cb.getCodigoDeError();
    String cbErrText = cb.getTextoDeError();
    LOG.debug("Informaciones de err recibidas:");
    LOG.debug("> [HEADER  ERR Code== {}]", headerErrCode);
    LOG.debug("> [CTRLBLK ERR Code=={}]", cbErrCode);
    LOG.debug("> [CTRLBLK ERR Text=={}]", cbErrText);

    if (headerErrCode != null && !headerErrCode.isEmpty() && !headerErrCode.trim().equalsIgnoreCase("000")) {
      LOG.warn("ERR en el encabezado del mensaje: [{}].", headerErrCode);
      return true;
    }
    else if (cbErrCode != null && !cbErrCode.isEmpty() && cbErrText != null && !cbErrText.isEmpty()
        && (cbErrText.trim().length() > 0)) {
      LOG.warn("ERR del mensaje: [{}::{}].", cbErrCode, cbErrText);
      return true;
    }
    else {
      return false;
    }
  }
}