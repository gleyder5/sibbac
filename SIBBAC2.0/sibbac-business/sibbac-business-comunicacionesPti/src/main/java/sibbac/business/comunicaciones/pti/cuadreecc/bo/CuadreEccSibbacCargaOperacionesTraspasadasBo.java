package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.CodigoErrorValidacionCuadreEcc;
import sibbac.business.comunicaciones.pti.commons.enums.CodigoMiembroCompensadorSV;
import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.commons.enums.TipoOperacionTraspasoTitulos;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccSibbacDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccSibbacCargaOpDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.InformacionCompensacionDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccLog;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;

@Service
public class CuadreEccSibbacCargaOperacionesTraspasadasBo {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CuadreEccSibbacCargaOperacionesTraspasadasBo.class);

  private static final String AUDIT_USER = "CECCLOADOP";

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0logBo tmct0logBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNuevaBo;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  @Autowired
  private CuadreEccSibbacDao cuadreEccSibbacDao;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  public CuadreEccSibbacCargaOperacionesTraspasadasBo() {
  }

  /**
   * 
   * FIXME INCIDENCIA CON CONCURRENCIA UN DESGLOSE SIBBAC ACTUALIZA EL ESTADO DE
   * LA EJEUCCION
   * 
   * @param beans
   * @throws EconomicDataException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void cargarCodigosOperacionEccFromCuadreEccEjecucionNewTransacction(List<CuadreEccDbReaderRecordBeanDTO> beans)
      throws SIBBACBusinessException, EconomicDataException {
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarCodigosOperacionEccNewTransacction] inicio");
    List<CuadreEccSibbac> cuadresEccSibbac;
    List<String> cdestadomovActivos = cfgBo.findEstadosEccActivos();
    for (CuadreEccDbReaderRecordBeanDTO cuadreEccIdDTO : beans) {
      LOG.trace(
          "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarCodigosOperacionEccNewTransacction] procesando {}",
          cuadreEccIdDTO);

      if (cuadreEccIdDTO.getId() != null) {
        CuadreEccSibbac cuadreEccSibbac = cuadreEccSibbacDao.findById(cuadreEccIdDTO.getId());
        cuadresEccSibbac = new ArrayList<CuadreEccSibbac>();
        cuadresEccSibbac.add(cuadreEccSibbac);
      }
      else if (cuadreEccIdDTO.getTmct0aloId() != null) {
        cuadresEccSibbac = new ArrayList<CuadreEccSibbac>();

        cuadresEccSibbac.addAll(cuadreEccSibbacDao.findAllCuadreEccSibbacByNuordenAndNbookingAndNucnfclt(cuadreEccIdDTO
            .getTmct0aloId().getNuorden(), cuadreEccIdDTO.getTmct0aloId().getNbooking(), cuadreEccIdDTO.getTmct0aloId()
            .getNucnfclt()));
      }
      else {
        LOG.warn("INCIDENCIA- Bean inservible: {}", cuadreEccIdDTO);
        continue;
      }
      LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarCodigosOperacionEccNewTransacction] leyendo alo...");
      Tmct0alo alo = aloBo.findById(cuadreEccIdDTO.getTmct0aloId());
      if (alo != null) {
        try {
          LOG.debug(
              "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarCodigosOperacionEccNewTransacction] alo: {}", alo);
          List<CuadreEccSibbacCargaOpDTO> loadData = new ArrayList<CuadreEccSibbacCargaOpDTO>();
          this.recuperarDesglosesCargaOps(alo, cdestadomovActivos, cuadresEccSibbac, loadData, AUDIT_USER);
          this.bajaMovimientosDesglosesCargaOps(loadData, AUDIT_USER);
          this.altaDesglosesAndMovimientosCargaOps(loadData, AUDIT_USER);

          this.deleteCuadreEccSibbacAndUpdateEstadoProcesadoCuadreEccEjeucciones(loadData,
              EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId(), AUDIT_USER);

          String msg = "CUADRE_ECC-CARGA_OPS-. Ejecutada carga de codigos de op ";
          tmct0logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(),
              msg, "", AUDIT_USER);

        }
        catch (Exception e) {
          throw new SIBBACBusinessException("INCIDENCIA- alo: " + alo.getId() + " bean : " + cuadreEccIdDTO, e);
        }
      }
      else {
        String msg = "CUADRE_ECC-CARGA_OPS-INCIDENCIA- No encontrado Alo.";
        tmct0logBo.insertarRegistro(cuadreEccIdDTO.getTmct0aloId().getNuorden(), cuadreEccIdDTO.getTmct0aloId()
            .getNbooking(), cuadreEccIdDTO.getTmct0aloId().getNucnfclt(), msg, "", AUDIT_USER);
      }

    }
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarCodigosOperacionEccNewTransacction] fin");
  }

  /**
   * 
   * lee los datos necesarios para procesar la carga de codigos de op ecc
   * 
   * @param alo
   * @param cdestadosmov
   * @param cuadresEccSibbac
   * @param loadData
   */
  private void recuperarDesglosesCargaOps(Tmct0alo alo, List<String> cdestadosmov,
      List<CuadreEccSibbac> cuadresEccSibbac, List<CuadreEccSibbacCargaOpDTO> loadData, String auditUser)
      throws SIBBACBusinessException {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] inicio.");
    Tmct0desgloseclitit dct;
    Tmct0movimientoecc mov;
    List<Tmct0movimientooperacionnuevaecc> movsN;
    CuadreEccSibbacCargaOpDTO load;
    Map<Long, Tmct0movimientoecc> movsById = new HashMap<Long, Tmct0movimientoecc>();
    for (CuadreEccSibbac cuadreEccSibbac : cuadresEccSibbac) {

      if (cuadreEccSibbac.getNudesglose() == null) {
        LOG.warn("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] NO DESGLOSADO {}",
            cuadreEccSibbac);
        CuadreEccLog log = new CuadreEccLog();
        log.setCodError(CodigoErrorValidacionCuadreEcc.TITULOS_NO_DESGLOSADO.text);
        log.setMsg("INCIDENCIA-No desglosado.");
        log.setAuditDate(new Date());
        log.setAuditUser(auditUser);
        log.setEstado(0);
        log.setIdCuadreEccSibbac(cuadreEccSibbac.getId());
        log = cuadreEccLogDao.save(log);
        cuadreEccSibbac
            .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC
                .getId());
        cuadreEccSibbac.setAuditDate(new Date());
        cuadreEccSibbac.setAuditUser(auditUser);
        cuadreEccSibbac = cuadreEccSibbacBo.save(cuadreEccSibbac);

        // cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflctNewTransaction(cuadreEccSibbac.getNuorden(),
        // cuadreEccSibbac.getNbooking(),
        // cuadreEccSibbac.getNucnfclt(),
        // EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId());
        //
        throw new SIBBACBusinessException("INCIDENCIA- No desglosado. " + cuadreEccSibbac);

        // List<CuadreEccLog> codigosOperacion =
        // cuadreEccLogDao.findAllByIdCuadreEccSibbacAndCodError(cuadreEccSibbac.getId(),
        // CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text);
        // for (CuadreEccLog cuadreEccLog : codigosOperacion) {
        // LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] buscando cuadre ecc ejeuccion...");
        //
        // CuadreEccEjecucion eje =
        // cuadreEccEjecucionBo.findById(cuadreEccLog.getIdCuadreEccEjecucion());
        // if (eje != null) {
        // eje.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId());
        // eje.setAuditDate(new Date());
        // eje.setAuditUser(auditUser);
        // eje = cuadreEccEjecucionBo.save(eje);
        // }
        // }
      }
      else {

        dct = dctBo.findById(cuadreEccSibbac.getNudesglose());

        if (dct != null) {
          load = new CuadreEccSibbacCargaOpDTO();
          load.setTmct0alo(alo);
          load.setDesgloseCamaraOriginal(dct.getTmct0desglosecamara());
          load.setCuadreEccSibbac(cuadreEccSibbac);
          load.setDesgloseOriginal(dct);
          load.setJoins(cuadreEccLogDao.findAllByIdCuadreEccSibbacAndCodError(cuadreEccSibbac.getId(),
              CodigoErrorValidacionCuadreEcc.JOIN_TABLES.text));
          if (CollectionUtils.isEmpty(load.getJoins())) {
            CuadreEccEjecucion eje = null;
            if (cuadreEccSibbac.getIdCuadreEccEjecucion() != null) {
              eje = cuadreEccEjecucionBo.findById(cuadreEccSibbac.getIdCuadreEccEjecucion());
            }
            else {
              eje = cuadreEccEjecucionBo.findByNumOpAndIdEcc(dct.getCdoperacionecc(), dct.getCdcamara());
            }

            if (eje == null) {
              throw new SIBBACBusinessException("INCIDENCIA- Utilizando num op: " + dct.getCdoperacionecc()
                  + " no encontrado eje . dct: " + dct);
            }
            else {
              load.setCuadreEccEjecucion(eje);
            }

          }
          else {
            for (CuadreEccLog join : load.getJoins()) {
              join.setCuadreEccEjecucion(cuadreEccEjecucionBo.findById(join.getIdCuadreEccEjecucion()));
              if (join.getCuadreEccEjecucion() == null) {
                // cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflctNewTransaction(cuadreEccSibbac.getNuorden(),
                // cuadreEccSibbac.getNbooking(),
                // cuadreEccSibbac.getNucnfclt(),
                // EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId());
                throw new SIBBACBusinessException("INCIDENCIA- Utilizando join eje no encontrado. " + join);
              }
            }
          }

          movsN = movNuevaBo.findByInCdestadomovAndNudesglose(cdestadosmov, dct.getNudesglose());
          if (CollectionUtils.isNotEmpty(movsN)) {
            if (movsN.size() > 1) {
              LOG.warn(
                  "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] El desglose {} tiene mas de un movimiento activo {}",
                  dct, movsN);
            }
            else {
              load.setMovimientoNuevoOriginal(movsN.get(0));
              mov = movsById.get(load.getMovimientoNuevoOriginal().getTmct0movimientoecc().getIdmovimiento());
              if (mov == null) {
                mov = load.getMovimientoNuevoOriginal().getTmct0movimientoecc();
                movsById.put(mov.getIdmovimiento(), mov);
              }
              load.setMovimientoOriginal(mov);
            }
          }
          else {
            LOG.warn(
                "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] no tiene movimientos. {}",
                dct);
          }
          loadData.add(load);
        }
        else {
          LOG.warn(
              "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] INCIDENCIA- DESGLOSE NO ENCONTRADO- {}",
              cuadreEccSibbac);
          // cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflctNewTransaction(cuadreEccSibbac.getNuorden(),
          // cuadreEccSibbac.getNbooking(),
          // cuadreEccSibbac.getNucnfclt(),
          // EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId());
          throw new SIBBACBusinessException("INCIDENCIA- Utilizando join dct no encontrado. " + cuadreEccSibbac);
        }

      }
    }
    movsById.clear();
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: recuperarDesglosesCargaOps] final.");
  }

  /**
   * @param dataList
   * @param auditUser
   */
  private void bajaMovimientosDesglosesCargaOps(List<CuadreEccSibbacCargaOpDTO> dataList, String auditUser) {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] inicio.");
    // 1.- AGRUPAR POR CAMARA Y ALC
    // 2.- CREAR DC POR CAMARA Y ALC
    // 3.- PASAR DCT'S Y MOVS A NUEVO DC
    // 4.-
    Tmct0alcId alcId;
    Tmct0desglosecamara dcBaja;
    String key;
    Map<String, Tmct0desglosecamara> dcsBajaByAlcAndCamara = new HashMap<String, Tmct0desglosecamara>();
    Map<Long, BigDecimal> titulosMovsOpNuevaPorMov = new HashMap<Long, BigDecimal>();
    Map<Long, Tmct0movimientoecc> movimientosDeBajaPorIdMovimientoAntiguo = new HashMap<Long, Tmct0movimientoecc>();
    Collection<Long> idMovsTratados = new HashSet<Long>();
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] sumando los titulos de los movimientos que vamos a dar de baja...");

    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] dando de baja todo lo que se va a cargar de ecc...");
    for (CuadreEccSibbacCargaOpDTO data : dataList) {
      alcId = new Tmct0alcId(data.getCuadreEccSibbac().getNbooking(), data.getCuadreEccSibbac().getNuorden(), data
          .getCuadreEccSibbac().getNucnfliq(), data.getCuadreEccSibbac().getNucnfclt());
      LOG.debug(
          "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] procesando alc: {}",
          alcId);
      key = alcId.toString() + data.getCuadreEccSibbac().getCdcamara();

      dcBaja = dcsBajaByAlcAndCamara.get(key);
      if (dcBaja == null) {
        LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] Creando dc nuevo para agrupar los dct y movs que vamos a dar de baja.");
        dcBaja = dcBo.inizializeTmct0DesgloseCamaraRechazado(data.getDesgloseCamaraOriginal());
      }

      // cambiamos el desglose de dc
      dcBaja.getTmct0desgloseclitits().add(data.getDesgloseOriginal());
      data.getDesgloseOriginal().setTmct0desglosecamara(dcBaja);
      // aniadimos titulos
      dcBaja.setNutitulos(dcBaja.getNutitulos().add(data.getDesgloseOriginal().getImtitulos()));

      dcsBajaByAlcAndCamara.put(key, dcBaja);
      if (data.getMovimientoOriginal() != null) {
        // si todos los titulos se van a dar de baja y no lo hemos hecho todavia
        if (!idMovsTratados.contains(data.getMovimientoOriginal().getIdmovimiento())) {
          LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] Aniadido todo el movimiento al nuevo dc de baja.");
          dcBaja.getTmct0movimientos().add(data.getMovimientoOriginal());
          data.getMovimientoOriginal().setTmct0desglosecamara(dcBaja);

          if (data.getMovimientoOriginal().getCuentaCompensacionDestino() == null) {
            data.getMovimientoOriginal().setCuentaCompensacionDestino("");
          }
          if (data.getMovimientoOriginal().getCodigoReferenciaAsignacion() == null) {
            data.getMovimientoOriginal().setCodigoReferenciaAsignacion("");
          }

          data.getMovimientoOriginal().setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
          data.getMovimientoOriginal().setAuditFechaCambio(new Date());
          data.getMovimientoOriginal().setAuditUser(auditUser);
          idMovsTratados.add(data.getMovimientoOriginal().getIdmovimiento());

        }
      }
      else {
        LOG.info(
            "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] no tiene movimiento dado de alta. {}",
            data);
      }

    }// fin for

    for (Tmct0desglosecamara dc : dcsBajaByAlcAndCamara.values()) {
      dcBo.persistAllInOrden(dc);
    }

    dcsBajaByAlcAndCamara.clear();
    titulosMovsOpNuevaPorMov.clear();
    movimientosDeBajaPorIdMovimientoAntiguo.clear();
    idMovsTratados.clear();
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: bajaMovimientosDesglosesCargaOps] fin.");
  }

  /**
   * @param dataList
   * @throws EconomicDataException
   */
  private void altaDesglosesAndMovimientosCargaOps(List<CuadreEccSibbacCargaOpDTO> dataList, String auditUser)
      throws EconomicDataException {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] inicio.");
    List<Tmct0desglosecamara> dcs;
    List<CuadreEccLog> joins;
    CuadreEccEjecucion eje;
    InformacionCompensacionDTO icDesgloseOriginal;
    boolean titulosDistintos;

    Tmct0alcId alcId;
    String key;
    // alta desgloses

    for (CuadreEccSibbacCargaOpDTO data : dataList) {

      LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] procesando {}.",
          data);

      alcId = new Tmct0alcId(data.getCuadreEccSibbac().getNbooking(), data.getCuadreEccSibbac().getNuorden(), data
          .getCuadreEccSibbac().getNucnfliq(), data.getCuadreEccSibbac().getNucnfclt());
      LOG.debug(
          "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] procesando alc: {}",
          alcId);
      key = alcId.toString() + data.getCuadreEccSibbac().getCdcamara();

      LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] key {}.", key);

      joins = data.getJoins();
      titulosDistintos = joins.size() > 1;

      if (data.getMovimientoOriginal() == null) {
        icDesgloseOriginal = new InformacionCompensacionDTO("", "", "", "");
      }
      else {
        icDesgloseOriginal = new InformacionCompensacionDTO(

        data.getMovimientoOriginal().getMiembroCompensadorDestino(), data.getMovimientoOriginal()
            .getCuentaCompensacionDestino(), data.getMovimientoOriginal().getMiembroCompensadorDestino(), data
            .getMovimientoOriginal().getCodigoReferenciaAsignacion());
      }

      if (CollectionUtils.isEmpty(joins)) {
        eje = data.getCuadreEccEjecucion();
        this.clonarDesgloseClitit(data, eje, icDesgloseOriginal, data.getDesgloseOriginal().getImtitulos(), auditUser);

      }
      else {
        for (CuadreEccLog join : joins) {
          LOG.trace(
              "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] procesando {}.",
              join);
          eje = join.getCuadreEccEjecucion();

          this.clonarDesgloseClitit(data, eje, icDesgloseOriginal, join.getTitulos(), auditUser);

        }// fin joins

        if (titulosDistintos) {
          this.repartoCalculosEconomicos(data);
        }
      }
    }

    // ponemos el dct en el dc correcto
    this.altaMovimientosAgrupadosInformacionCompensacion(dataList, auditUser);

    // ponemos el dct en el dc correcto
    for (CuadreEccSibbacCargaOpDTO data : dataList) {
      alcId = new Tmct0alcId(data.getCuadreEccSibbac().getNbooking(), data.getCuadreEccSibbac().getNuorden(), data
          .getCuadreEccSibbac().getNucnfliq(), data.getCuadreEccSibbac().getNucnfclt());
      dcs = dcBo.findAllByTmct0alcId(alcId);
      if (CollectionUtils.isNotEmpty(dcs)) {
        for (Tmct0desglosecamara dc : dcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            cuadreEccSibbacBo.escalarInformacionCompensacionFromMovimientos(dc, auditUser);

            cuadreEccSibbacBo.escalarEstadoFromMovimientos(dc, auditUser);
          }
        }
      }
    }

    // escalamos la ic desde los movs al dc

    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] fin.");
  }

  private void deleteCuadreEccSibbacAndUpdateEstadoProcesadoCuadreEccEjeucciones(
      List<CuadreEccSibbacCargaOpDTO> loadData, int estadoProcesado, String auditUser) {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: updateEstadoProcesadoPdteInicio] inicio.");

    for (CuadreEccSibbacCargaOpDTO data : loadData) {
      // data.getCuadreEccSibbac().setEstadoProcesado(estadoProcesado);
      // data.getCuadreEccSibbac().setAuditDate(new Date());
      // data.getCuadreEccSibbac().setAuditUser(auditUser);
      // save(data.getCuadreEccSibbac());
      if (CollectionUtils.isEmpty(data.getJoins())) {
        if (data.getCuadreEccSibbac().getIdCuadreEccEjecucion() != null) {
          cuadreEccEjecucionBo.updateEstadoProcesadoById(data.getCuadreEccSibbac().getIdCuadreEccEjecucion(),
              estadoProcesado, auditUser);
        }
      }
      else {
        for (CuadreEccLog join : data.getJoins()) {
          if (join.getCuadreEccEjecucion() != null) {
            join.getCuadreEccEjecucion().setEstadoProcesado(estadoProcesado);
            join.getCuadreEccEjecucion().setAuditDate(new Date());
            join.getCuadreEccEjecucion().setAuditUser(auditUser);
            cuadreEccEjecucionBo.save(join.getCuadreEccEjecucion());
          }
          cuadreEccLogDao.delete(join);
        }
        cuadreEccSibbacBo.delete(data.getCuadreEccSibbac());
      }

    }
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: updateEstadoProcesadoPdteInicio] fin.");
  }

  private void clonarDesgloseClitit(CuadreEccSibbacCargaOpDTO data, CuadreEccEjecucion eje,
      InformacionCompensacionDTO icDesgloseOriginal, BigDecimal titulos, String auditUser) {
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] procesando {}.",
        eje);
    InformacionCompensacionDTO icEjecucion = new InformacionCompensacionDTO(eje.getMiembroPropietarioCuenta(),
        eje.getCuentaCompensacionMiembro(), eje.getMiembroPropietarioCuenta(), eje.getRefAsignacionExt());

    LOG.debug(
        "[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] alta dct titulos: {} cod. op: {} ibo: {}.",
        titulos, eje.getNumOp(), eje.getNumOpDcv());

    Tmct0desgloseclitit dctCopia = new Tmct0desgloseclitit();
    CloneObjectHelper.copyBasicFields(data.getDesgloseOriginal(), dctCopia, null);
    if (titulos.compareTo(data.getDesgloseOriginal().getImtitulos()) != 0) {

      dctCopia.setImtitulos(titulos);
      dctCopia.setImefectivo(dctCopia.getImtitulos().multiply(dctCopia.getImprecio())
          .setScale(2, BigDecimal.ROUND_HALF_UP));

    }
    dctCopia.setNudesglose(null);
    dctCopia.setCdoperacion(eje.getNumOp());
    dctCopia.setCdoperacionecc(eje.getNumOp());
    dctCopia.setNuoperacionprev(eje.getNumOpPrevia());
    dctCopia.setNuoperacioninic(eje.getNumOpInicial());

    if (icDesgloseOriginal.isBlanck()) {
      icDesgloseOriginal = icEjecucion;
    }
    this.cargarInformacionCompensacion(eje, icDesgloseOriginal, icEjecucion, dctCopia);

    dctCopia.setTmct0desglosecamara(data.getDesgloseCamaraOriginal());
    data.getDesglosesCreados().add(dctCopia);
    data.getDesgloseCamaraOriginal().getTmct0desgloseclitits().add(dctCopia);
    dctCopia.setAuditFechaCambio(new Date());
    dctCopia.setAuditUser(auditUser);
    dctCopia = dctBo.save(dctCopia);
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] save {}.",
        dctCopia);
  }

  private void repartoCalculosEconomicos(CuadreEccSibbacCargaOpDTO data) throws EconomicDataException {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: repartoCalculosEconomicos] inicio.");
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaDesglosesAndMovimientosCargaOps] reparto datos economicos, mas de un dc creado...");
    // repartimos calculos
    Tmct0alcId alcIdDummy = new Tmct0alcId(data.getCuadreEccSibbac().getNbooking(), data.getCuadreEccSibbac()
        .getNuorden(), new Short("0"), data.getCuadreEccSibbac().getNucnfclt());
    Tmct0alc alcDummy = new Tmct0alc();
    alcDummy.setId(alcIdDummy);
    CloneObjectHelper.copyBasicFields(data.getDesgloseOriginal(), alcDummy, null);
    alcDummy.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    Tmct0desglosecamara dcDummy = new Tmct0desglosecamara();
    dcDummy.setTmct0estadoByCdestadoasig(new Tmct0estado());
    dcDummy.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    dcDummy.setTmct0desgloseclitits(data.getDesglosesCreados());
    dcDummy.setTmct0alc(alcDummy);
    alcDummy.getTmct0desglosecamaras().add(dcDummy);
    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
    alcs.add(alcDummy);
    CalculosEconomicosNacionalBo calculos = new CalculosEconomicosNacionalBo(data.getTmct0alo(), alcs);
    calculos.repartirCalculosAlcEnDct();
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: repartoCalculosEconomicos] fin.");
  }

  private void altaMovimientosAgrupadosInformacionCompensacion(List<CuadreEccSibbacCargaOpDTO> dataList,
      String auditUser) {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaMovimientosAgrupadosInformacionCompensacion] inicio.");
    InformacionCompensacionDTO icUtilizar;
    Tmct0movimientoecc mov;
    Tmct0movimientooperacionnuevaecc movn;
    List<Tmct0movimientooperacionnuevaecc> movsn;
    Map<String, Map<InformacionCompensacionDTO, Tmct0movimientoecc>> movimientosPorAlcAndCamaraAndIc = new HashMap<String, Map<InformacionCompensacionDTO, Tmct0movimientoecc>>();
    Map<InformacionCompensacionDTO, Tmct0movimientoecc> movimientosByIc;
    for (CuadreEccSibbacCargaOpDTO data : dataList) {

      Tmct0alcId alcId = new Tmct0alcId(data.getCuadreEccSibbac().getNbooking(),
          data.getCuadreEccSibbac().getNuorden(), data.getCuadreEccSibbac().getNucnfliq(), data.getCuadreEccSibbac()
              .getNucnfclt());
      String key = alcId.toString() + data.getCuadreEccSibbac().getCdcamara();

      for (Tmct0desgloseclitit dct : data.getDesglosesCreados()) {
        dct.setTmct0desglosecamara(data.getDesgloseCamaraOriginal());

        icUtilizar = new InformacionCompensacionDTO(dct.getMiembroCompensadorDestino(),
            dct.getCuentaCompensacionDestino(), dct.getMiembroCompensadorDestino(), dct.getCodigoReferenciaAsignacion());

        movimientosByIc = movimientosPorAlcAndCamaraAndIc.get(key);
        if (movimientosByIc == null) {
          movimientosByIc = new HashMap<InformacionCompensacionDTO, Tmct0movimientoecc>();
          movimientosPorAlcAndCamaraAndIc.put(key, movimientosByIc);
        }
        mov = movimientosByIc.get(icUtilizar);
        if (mov == null) {
          mov = new Tmct0movimientoecc();
          if (data.getMovimientoOriginal() != null) {
            CloneObjectHelper.copyBasicFields(data.getMovimientoOriginal(), mov, null);
          }
          mov.setIdmovimiento(null);
          mov.setImtitulos(BigDecimal.ZERO);
          mov.setImefectivo(BigDecimal.ZERO);
          mov.setTmct0movimientooperacionnuevaeccs(new ArrayList<Tmct0movimientooperacionnuevaecc>());
          movimientosByIc.put(icUtilizar, mov);
          mov.setTmct0desglosecamara(data.getDesgloseCamaraOriginal());
          mov.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
          mov.setAuditFechaCambio(new Date());
          mov.setAuditUser(auditUser);

          mov.setCuentaCompensacionDestino(icUtilizar.getCuentaCompensacion());
          mov.setMiembroCompensadorDestino(icUtilizar.getMiembroPropietarioCuentaCompensacion());
          mov.setCodigoReferenciaAsignacion(icUtilizar.getReferenciaAsignacionExterna());
        }

        movn = new Tmct0movimientooperacionnuevaecc();
        movn.setTmct0movimientoecc(mov);
        // put el corretaje
        movNuevaBo.inizializeMovimientoOperacioNueva(dct, dct.getImfinsvb(), movn);

        mov.setImtitulos(mov.getImtitulos().add(movn.getImtitulos()));
        mov.setImefectivo(mov.getImefectivo().add(movn.getImefectivo()));

        mov.getTmct0movimientooperacionnuevaeccs().add(movn);
      }
    }

    for (String key2 : movimientosPorAlcAndCamaraAndIc.keySet()) {
      movimientosByIc = movimientosPorAlcAndCamaraAndIc.get(key2);
      for (Tmct0movimientoecc mov2 : movimientosByIc.values()) {
        movsn = new ArrayList<Tmct0movimientooperacionnuevaecc>(mov2.getTmct0movimientooperacionnuevaeccs());
        mov2.getTmct0movimientooperacionnuevaeccs().clear();
        mov2 = movBo.save(mov2);
        movsn = movNuevaBo.save(movsn);
      }
    }
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: altaMovimientosAgrupadosInformacionCompensacion] fin.");
  }

  /**
   * @param eje
   * @param icDesgloseOriginal
   * @param icEjecucion
   * @param dctCopia
   */
  private void cargarInformacionCompensacion(CuadreEccEjecucion eje, InformacionCompensacionDTO icDesgloseOriginal,
      InformacionCompensacionDTO icEjecucion, Tmct0desgloseclitit dctCopia) {
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarInformacionCompensacion] inicio.");
    InformacionCompensacionDTO icUtilizar;
    dctCopia.setMiembroCompensadorDestino("");
    dctCopia.setCuentaCompensacionDestino("");
    dctCopia.setCodigoReferenciaAsignacion("");
    // se trata de un give up hacia la sv
    if (eje.getCodOp() == TipoOperacionTraspasoTitulos.GIVE_UP.text.charAt(0)
        && !eje.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)) {
      LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarInformacionCompensacion] Es un Give-up destino no SV.");
      icUtilizar = icEjecucion;
      dctCopia.setMiembroCompensadorDestino(icUtilizar.getMiembroPropietarioCuentaCompensacion());
      if (StringUtils.isNotBlank(icUtilizar.getReferenciaAsignacionExterna())) {
        dctCopia.setCodigoReferenciaAsignacion(icUtilizar.getReferenciaAsignacionExterna());
      }
      else {
        dctCopia.setCuentaCompensacionDestino(icUtilizar.getCuentaCompensacion());
      }
      // se trata de una transfer dentro de la sv
    }
    else if (eje.getCodOp() == TipoOperacionTraspasoTitulos.TRANSFER.text.charAt(0)
        && eje.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)) {
      LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarInformacionCompensacion] Es una Transfer dentro de la SV.");
      icUtilizar = icEjecucion;
      dctCopia.setCuentaCompensacionDestino(icUtilizar.getCuentaCompensacion());
      dctCopia.setMiembroCompensadorDestino(icUtilizar.getMiembroPropietarioCuentaCompensacion());
      dctCopia.setCodigoReferenciaAsignacion("");
      // se trata de una transfer fuera de la sv, entre las cuentas del miembro
      // externo
    }
    else if (eje.getCodOp() == TipoOperacionTraspasoTitulos.TRANSFER.text.charAt(0)
        && !eje.getMiembroPropietarioCuenta().equals(CodigoMiembroCompensadorSV.SV.text)) {
      LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarInformacionCompensacion] Es una Transfer fuera de la SV.");
      icUtilizar = icDesgloseOriginal;
      dctCopia.setMiembroCompensadorDestino(icUtilizar.getMiembroPropietarioCuentaCompensacion());

      if (StringUtils.isNotBlank(icUtilizar.getReferenciaAsignacionExterna())) {
        dctCopia.setCodigoReferenciaAsignacion(icUtilizar.getReferenciaAsignacionExterna());
      }
      else {
        dctCopia.setCuentaCompensacionDestino(icUtilizar.getCuentaCompensacion());
      }
    }
    else {
      icUtilizar = icEjecucion;
      dctCopia.setCuentaCompensacionDestino(icUtilizar.getCuentaCompensacion());
      dctCopia.setMiembroCompensadorDestino(icUtilizar.getMiembroPropietarioCuentaCompensacion());
      dctCopia.setCodigoReferenciaAsignacion(icUtilizar.getReferenciaAsignacionExterna());
    }
    LOG.trace("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarInformacionCompensacion] ic {}.", icUtilizar);
    LOG.debug("[CuadreEccSibbacCargaOperacionesTraspasadasBo :: cargarInformacionCompensacion] fin.");
  }

}
