package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccSepararMovimientosByInformacionCompensacionRunnableBean;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccSepararMovimientosByInformacionCompensacionDbReader")
public class CuadreEccSepararMovimientosByInformacionCompensacionDbReader extends
    WrapperMultiThreadedDbReader<CuadreEccDbReaderRecordBeanDTO> {

  protected static final Logger LOG = LoggerFactory
      .getLogger(CuadreEccSepararMovimientosByInformacionCompensacionDbReader.class);

  @Value("${sibbac.cuadre.ecc.max.reg.process.in.execution:100000}")
  private int maxRegToProcess;

  @Value("${sibbac.cuadre.ecc.separar.movimientos.transaction.size:1}")
  private int transactionSize;

  @Value("${sibbac.cuadre.ecc.separar.movimientos.thread.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Qualifier(value = "cuadreEccSepararMovimientosByInformacionCompensacionRunnableBean")
  @Autowired
  private CuadreEccSepararMovimientosByInformacionCompensacionRunnableBean cuadreEccRunnable;

  public CuadreEccSepararMovimientosByInformacionCompensacionDbReader() {
    super();
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<CuadreEccDbReaderRecordBeanDTO> getBeanToProcessBlock() {
    return cuadreEccRunnable;
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    List<CuadreEccDbReaderRecordBeanDTO> beanList = cuadreEccSibbacBo
        .findAllCuadreEccDbReaderToSepararMovimientos(maxRegToProcess);

    LOG.debug(
        "[CuadreEccSepararMovimientosByInformacionCompensacionDbReader :: preExecuteTask] Encontrados {} Alos que procesar",
        beanList.size());
    beanIterator = beanList.iterator();
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

}
