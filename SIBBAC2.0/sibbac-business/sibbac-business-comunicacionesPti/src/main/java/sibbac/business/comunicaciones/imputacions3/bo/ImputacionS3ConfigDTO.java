package sibbac.business.comunicaciones.imputacions3.bo;

import javax.jms.ConnectionFactory;

import sibbac.pti.PTIMessageVersion;

public class ImputacionS3ConfigDTO {

  private final ConnectionFactory cf;
  private final String cuentaCompensacionS3;
  private final PTIMessageVersion version;
  private final String camaraEuroCcp;
  private final String camaraNacional;
  private final String auditUser;

  public ImputacionS3ConfigDTO(ConnectionFactory cf, String cuentaCompensacionS3, PTIMessageVersion version,
      String camaraEuroCcp, String camaraNacional, String auditUser) {
    super();
    this.cf = cf;
    this.cuentaCompensacionS3 = cuentaCompensacionS3;
    this.version = version;
    this.camaraEuroCcp = camaraEuroCcp;
    this.camaraNacional = camaraNacional;
    this.auditUser = auditUser;
  }

  public ConnectionFactory getCf() {
    return cf;
  }

  public String getCuentaCompensacionS3() {
    return cuentaCompensacionS3;
  }

  public PTIMessageVersion getVersion() {
    return version;
  }

  public String getCamaraEuroCcp() {
    return camaraEuroCcp;
  }

  public String getCamaraNacional() {
    return camaraNacional;
  }

  public String getAuditUser() {
    return auditUser;
  }

}
