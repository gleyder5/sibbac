package sibbac.business.comunicaciones.contabilidad.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.bo.Tgrl0cmsBo;
import sibbac.business.wrappers.database.bo.Tgrl0ejeBo;
import sibbac.business.wrappers.database.bo.Tgrl0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0ctaBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.dao.Tgrl0cmsDao;
import sibbac.business.wrappers.database.dao.Tgrl0ejeDao;
import sibbac.business.wrappers.database.dao.Tgrl0ordDao;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cms;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cmsId;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ejeId;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ord;

@Service
public class ContabilidadInternacionalBo {

  protected static final Logger LOG = LoggerFactory.getLogger(ContabilidadInternacionalBo.class);

  private static final String COMISION_SVB = "P";
  private static final String COMISION_BRK = "K";
  private static final String TIPO_A = "A";
  private static final String TIPO_C = "C";
  private static final String TIPO_ALL = "ALL";
  private static final String LIQ_MEGARA = "0036";
  private static final String LIQ_PARTENON = "0049";
  private static final String SVB = "SVB";
  private static final String BRK = "BRK";
  private static final String DEV = "DEV";
  private static final String BAN = "BAN";
  private static final String MAB = "MAB";
  private static final String USER_AJUSTE = "AJUS-GES";

  @Autowired
  private Tmct0xasBo xasBo;

  @Autowired
  private Tmct0logBo loggerSibbac;

  @Autowired
  private Tgrl0ordDao tgrl0OrdDao;

  @Autowired
  private Tgrl0ordBo tgrl0ordBo;

  @Autowired
  private Tgrl0ejeBo tgrl0ejeBo;

  @Autowired
  private Tgrl0cmsBo tgrl0cmdBo;

  @Autowired
  private Tgrl0ejeDao tgrl0ejeDao;

  @Autowired
  private Tgrl0cmsDao cmsDao;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0ctaBo ctaBo;

  @Autowired
  private Tmct0dbsBo dbsBo;

  /**
   * CONTABILIZAR ORDENES
   * 
   * @param hpm persistent manager
   * @param app aplicacion
   * @param msgbundle mensajes
   * @generated
   */
  public ContabilidadInternacionalBo() {
  }

  /**
   * CONTABILIZAR TIPOS "A-C-D" PARA MEGARA-PARTENON COMISION SVB
   * 
   * @param anular si true contravalor
   * @param tipo "A" Inicial "C" Cobrada "D" Diferencias-Ajustes
   * @param tmct0alo registro cliente allocation
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void contabilizarLiq(boolean anular, String tipo, Tmct0alo tmct0alo) throws SIBBACBusinessException {

    boolean foreignMarket = tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo() == 'E';

    // Control de si existe Liquidacion
    boolean liquidacion = false;
    if ((tmct0alo.getImliq().compareTo(BigDecimal.ZERO) != 0)
        || (tmct0alo.getImeurliq().compareTo(BigDecimal.ZERO) != 0)
        || (tmct0alo.getImajliq().compareTo(BigDecimal.ZERO) != 0)
        || (tmct0alo.getImajeliq().compareTo(BigDecimal.ZERO) != 0)) {
      liquidacion = true;
    }
    String liquidadora = "";
    if (foreignMarket) {
      liquidadora = xasBo.getCdcleareClearer(tmct0alo.getCdcleare());
    }
    else {
      liquidadora = tmct0alo.getCdcleare().trim();
    }
    if ("".equalsIgnoreCase(liquidadora.trim())) {
      // loggerSibbac.insertarRegistroNewTransaction(tmct0alo.getId().getNuorden(),
      // tmct0alo.getId().getNbooking(),
      // tmct0alo.getId().getNucnfclt(),
      // String.format("[ContabilidadInternacionalBo] Liquidadora no encontrada -%s",
      // tmct0alo.getCdcleare()),
      // tmct0alo.getTmct0sta().getDsestado(), "SIBBAC20");

      throw new SIBBACBusinessException(String.format("[ContabilidadInternacionalBo] Liquidadora no encontrada -'%s'",
          tmct0alo.getCdcleare().trim()));
    }
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      final Date actualTime = new Date(System.currentTimeMillis());

      final Integer nuoprout = tmct0alo.getNuoprout();
      final Short nupareje = tmct0alo.getNupareje();
      final short nuregcom = tgrl0cmdBo.getId(tmct0alo.getNuoprout(), tmct0alo.getNupareje());
      final Character tpregcom = tipo.charAt(0);
      final Integer fhcolicm = new Integer(sdf.format(actualTime));
      final Character cdestado = 'A';
      final Character cdsvbbrk = 'P';
      final Integer fhentcon = 0;

      final String cdmoniso = tmct0alo.getCdmoniso();
      final String cdbroker = "";
      Tgrl0cms tgrl0cms = new Tgrl0cms();
      tgrl0cms.setId(new Tgrl0cmsId(nuoprout, nupareje, nuregcom));
      tgrl0cms.setTgrl0eje(tgrl0ejeDao.findOne(new Tgrl0ejeId(nuoprout, nupareje)));
      tgrl0cms.setCdsvbbrk(cdsvbbrk);
      tgrl0cms.setTpregcom(tpregcom);
      //
      BigDecimal imregcom = BigDecimal.ZERO;
      BigDecimal imregdiv = BigDecimal.ZERO;
      // Especificos segun tipo
      Integer fhmovcom = 0;
      String cdusrcom = "ROUTING";
      if (ContabilidadInternacionalBo.TIPO_A.equalsIgnoreCase(tipo.trim())) {
        cdusrcom = "ROUTING";
        fhmovcom = new Integer(sdf.format(actualTime));
        imregcom = tmct0alo.getImsvbeu().setScale(2, RoundingMode.HALF_UP);
        imregdiv = tmct0alo.getImsvb().setScale(2, RoundingMode.HALF_UP);
        // Cargar Tgrl0ord y Tgrl0eje si procede
        // loadTgrl0ord(tmct0alo.getFk_tmct0bok_1().getFk_tmct0ord_1().getNuorden());
        // loadTgrl0eje(tmct0alo);
      }
      else if (ContabilidadInternacionalBo.TIPO_C.equalsIgnoreCase(tipo.trim())) {
        cdusrcom = "LIQ-" + liquidadora.trim();
        // FIX:Inicio
        if (tmct0alo.getFevalorr() != null) {
          fhmovcom = new Integer(sdf.format(tmct0alo.getFevalorr()));
        }
        else {
          fhmovcom = new Integer(sdf.format(tmct0alo.getFevalor()));
        }
        // FIX:Fin
        // Si no Liquidacion
        if (!liquidacion) {
          imregcom = tmct0alo.getImsvbeu().setScale(2, RoundingMode.HALF_UP);
          imregdiv = tmct0alo.getImsvb().setScale(2, RoundingMode.HALF_UP);
          // Si Liquidacion
        }
        else {
          imregcom = tmct0alo.getImeurliq().setScale(2, RoundingMode.HALF_UP);
          imregdiv = tmct0alo.getImliq().setScale(2, RoundingMode.HALF_UP);
        }
      }

      // Si anular cambiar signo
      if (anular) {
        imregcom = imregcom.multiply(new BigDecimal(-1));
        imregdiv = imregdiv.multiply(new BigDecimal(-1));
      }
      tgrl0cms.setImregcom(imregcom.setScale(2, RoundingMode.HALF_UP));
      tgrl0cms.setImregdiv(imregdiv.setScale(2, RoundingMode.HALF_UP));
      tgrl0cms.setFhmovcom(fhmovcom);
      tgrl0cms.setFhcolicm(fhcolicm);
      tgrl0cms.setCdestado(cdestado);
      tgrl0cms.setCdusrcom(cdusrcom);
      tgrl0cms.setFhentcon(fhentcon);
      tgrl0cms.setCdmoniso(cdmoniso);
      tgrl0cms.setCdbroker(cdbroker);
      tgrl0cms.setCdcleare(liquidadora);

      tgrl0cms = cmsDao.save(tgrl0cms);
      dbsBo.insertRegister(tgrl0cms, true);

      // Contabilizar Diferencias si las hay Solo Tipos C
      if (ContabilidadInternacionalBo.TIPO_C.equalsIgnoreCase(tipo.trim())) {
        if ((tmct0alo.getImajeliq().compareTo(BigDecimal.ZERO) != 0)
            || (tmct0alo.getImajliq().compareTo(BigDecimal.ZERO) != 0)) {
          contabilizarAjuste(anular, tmct0alo);
        }
      }

    }
    catch (Exception ex) {
      String error = String.format(
          "[ContabilidadInternacionalBo]NUCNFCLT: %s PKENTITY: %s CONTABILIDAD %s NO REALIZADA", tmct0alo.getPkentity()
              .trim(), tmct0alo.getId().getNucnfclt().trim(), tmct0alo.getPkentity().trim(), liquidadora.trim());
      LOG.warn(error, ex);

      throw new SIBBACBusinessException(String.format("%s %s", error, ex.getMessage()), ex);
    }

  }

  /**
   * CONTABILIZAR AJUSTES "D" PARA MEGARA-PARTENON
   * 
   * @param anular si true contravalor
   * @param ctdSdo registro ctd
   * @param adiSdo registro adi - diferencias
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void contabilizarAjuste(boolean anular, Tmct0alo tmct0alo) throws SIBBACBusinessException {

    String liquidadora = "";
    boolean foreignMarket = 'E' == tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo();
    if (foreignMarket) {
      liquidadora = xasBo.getCdcleareClearer(tmct0alo.getCdcleare());
    }
    else {
      liquidadora = tmct0alo.getCdcleare().trim();
    }

    if ("".equalsIgnoreCase(liquidadora.trim())) {
      throw new SIBBACBusinessException("[ContabilidadInternacionalBo] Liquidadora no encontrada");
    }
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      final Date actualTime = new Date(System.currentTimeMillis());

      final Integer nuoprout = tmct0alo.getNuoprout();
      final short nupareje = tmct0alo.getNupareje();
      final short nuregcom = tgrl0cmdBo.getId(nuoprout, nupareje);
      final Character cdsvbbrk = 'P';
      final Character tpregcom = 'D';

      BigDecimal imregcom = tmct0alo.getImajeliq().setScale(2, RoundingMode.HALF_UP);
      // FIX:Inicio
      final Date fecliqui;
      if (tmct0alo.getFevalorr() != null) {
        fecliqui = tmct0alo.getFevalorr();
      }
      else {
        fecliqui = tmct0alo.getFevalor();
      }
      // FIX:Fin
      final Integer fhmovcom = new Integer(sdf.format(fecliqui));
      final Integer fhcolicm = new Integer(sdf.format(actualTime));
      final Character cdestado = 'A';
      final String cdusrcom = "LIQ-" + liquidadora.trim();
      final Integer fhentcon = 0;

      BigDecimal imregdiv = tmct0alo.getImajliq().setScale(2, RoundingMode.HALF_UP);
      final String cdmoniso = tmct0alo.getCdmoniso();
      final String cdbroker = "";

      Tgrl0cms tgrl0cms = new Tgrl0cms();
      tgrl0cms.setId(new Tgrl0cmsId(nuoprout, nupareje, nuregcom));
      tgrl0cms.setTgrl0eje(tgrl0ejeDao.findOne(new Tgrl0ejeId(nuoprout, nupareje)));

      if (anular) {
        imregcom = imregcom.multiply(new BigDecimal(-1));
        imregdiv = imregdiv.multiply(new BigDecimal(-1));
      }
      tgrl0cms.setCdsvbbrk(cdsvbbrk);
      tgrl0cms.setTpregcom(tpregcom);
      tgrl0cms.setImregcom(imregcom);
      tgrl0cms.setFhmovcom(fhmovcom);
      tgrl0cms.setFhcolicm(fhcolicm);
      tgrl0cms.setCdestado(cdestado);
      tgrl0cms.setCdusrcom(cdusrcom);
      tgrl0cms.setFhentcon(fhentcon);
      tgrl0cms.setImregdiv(imregdiv);
      tgrl0cms.setCdmoniso(cdmoniso);
      tgrl0cms.setCdbroker(cdbroker);
      tgrl0cms.setCdcleare(liquidadora);

      if ((tgrl0cms.getImregcom().compareTo(BigDecimal.ZERO) != 0)
          || (tgrl0cms.getImregdiv().compareTo(BigDecimal.ZERO) != 0)) {
        tgrl0cms = cmsDao.save(tgrl0cms);
        dbsBo.insertRegister(tgrl0cms, true);
      }

    }
    catch (Exception ex) {
      String error = String.format(
          "[ContabilidadInternacionalBo]NUCNFCLT: %s PKENTITY: %s CONTABILIDAD DIFERENCIAS %s NO REALIZADA"
              + tmct0alo.getPkentity().trim(), tmct0alo.getId().getNucnfclt().trim(), tmct0alo.getPkentity().trim(),
          liquidadora.trim());
      LOG.warn(error, ex);

      throw new SIBBACBusinessException(String.format("%s %s", error, ex.getMessage()), ex);
    }
  }

  /**
   * CONTABILIZAR BROKER TIPOS "C" PARA MEGARA-PARTENON
   * 
   * @param anular si true contravalor
   * @param tipo "C" Cobrada
   * @param tmct0cta center booking allocation
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void contabilizarLiqMercado(boolean anular, String tipo, Tmct0cta tmct0cta) throws SIBBACBusinessException {

    boolean foreignMarket = 'E' == tmct0cta.getTmct0ctg().getTmct0cto().getTmct0ord().getCdclsmdo();
    String liquidadora = "";
    if (foreignMarket) {
      liquidadora = xasBo.getCdcleareClearer(tmct0cta.getCdcleare());
    }
    else {
      liquidadora = tmct0cta.getCdcleare().trim();
    }

    if ("".equalsIgnoreCase(liquidadora.trim())) {
      String error = String.format("[ContabilidadInternacionalBo] Liquidadora no encontrada -%s",
          tmct0cta.getCdcleare());

      LOG.warn(error);
      loggerSibbac.insertarRegistroNewTransaction(tmct0cta.getId().getNuorden(), tmct0cta.getId().getNbooking(),
          tmct0cta.getId().getNucnfclt(), error, tmct0cta.getTmct0sta().getDsestado(), "SIBBAC20");
      throw new SIBBACBusinessException(error);
    }
    try {
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      final Date actualTime = new Date(System.currentTimeMillis());

      final Integer nuoprout = tmct0cta.getNuoprout();
      final short nupareje = tmct0cta.getNupareje();
      final short nuregcom = tgrl0cmdBo.getId(nuoprout, nupareje);
      final Character cdsvbbrk = 'K';
      final Character tpregcom = tipo.charAt(0);
      final Integer fhcolicm = new Integer(sdf.format(actualTime));
      final Character cdestado = 'A';

      final Integer fhentcon = 0;

      final String cdmoniso = tmct0cta.getCdmoniso();
      final String cdbroker = tmct0cta.getId().getCdbroker();
      Tgrl0cms tgrl0cms = new Tgrl0cms();
      tgrl0cms.setId(new Tgrl0cmsId(nuoprout, nupareje, nuregcom));
      tgrl0cms.setTgrl0eje(tgrl0ejeDao.findOne(new Tgrl0ejeId(nuoprout, nupareje)));
      tgrl0cms.setCdsvbbrk(cdsvbbrk);
      tgrl0cms.setTpregcom(tpregcom);
      //
      BigDecimal imregcom = BigDecimal.ZERO;
      BigDecimal imregdiv = BigDecimal.ZERO;
      // Especificos segun tipo
      Integer fhmovcom = 0;
      String cdusrcom = "ROUTING";
      if (ContabilidadInternacionalBo.TIPO_A.equalsIgnoreCase(tipo.trim())) {
        // cdusrcom = "ROUTING";
        cdusrcom = "LIQ-" + liquidadora.trim();
        fhmovcom = new Integer(sdf.format(actualTime));
        imregcom = tmct0cta.getImfibreu().setScale(2, RoundingMode.HALF_UP);
        imregdiv = tmct0cta.getImfibrdv().setScale(2, RoundingMode.HALF_UP);
        // Cargar Tgrl0ord y Tgrl0eje si procede
        // loadTgrl0ord(tmct0cta.getFk_tmct0ctg_1().getFk_tmct0cto_1().getFk_tmct0ord_2().getNuorden());
        // loadTgrl0eje(JdoGetObject.getTmct0alo(tmct0cta.getFk_tmct0ctg_1().getFk_tmct0cto_1().getFk_tmct0ord_2()
        // .getNuorden(), tmct0cta.getFk_tmct0ctg_1().getNbooking(),
        // tmct0cta.getNucnfclt(), hpm));
      }
      else if (ContabilidadInternacionalBo.TIPO_C.equalsIgnoreCase(tipo.trim())) {
        cdusrcom = "LIQ-" + liquidadora.trim();
        // FIX:Inicio
        fhmovcom = new Integer(sdf.format(tmct0cta.getFevalor()));
        // FIX:Fin
        imregcom = tmct0cta.getImfibreu().setScale(2, RoundingMode.HALF_UP);
        imregdiv = tmct0cta.getImfibrdv().setScale(2, RoundingMode.HALF_UP);
      }

      // Si anular cambiar signo
      if (anular) {
        imregcom = imregcom.multiply(new BigDecimal(-1));
        imregdiv = imregdiv.multiply(new BigDecimal(-1));
      }
      tgrl0cms.setImregcom(imregcom.setScale(2, RoundingMode.HALF_UP));
      tgrl0cms.setImregdiv(imregdiv.setScale(2, RoundingMode.HALF_UP));
      tgrl0cms.setFhmovcom(fhmovcom);
      tgrl0cms.setFhcolicm(fhcolicm);
      tgrl0cms.setCdestado(cdestado);
      tgrl0cms.setCdusrcom(cdusrcom);
      tgrl0cms.setFhentcon(fhentcon);
      tgrl0cms.setCdmoniso(cdmoniso);
      tgrl0cms.setCdbroker(cdbroker);
      tgrl0cms.setCdcleare(liquidadora);

      if ((tgrl0cms.getImregcom().compareTo(BigDecimal.ZERO) != 0)
          || (tgrl0cms.getImregdiv().compareTo(BigDecimal.ZERO) != 0)) {
        tgrl0cms = cmsDao.save(tgrl0cms);
        dbsBo.insertRegister(tgrl0cms, true);
      }

    }
    catch (Exception ex) {

      String error = String
          .format(
              "[ContabilidadInternacionalBo]PKBROKER:%s NBOOKING: %s BROKER: %s MERCADO: %s ALLOCATION: %s CONTABILIDAD %s NO REALIZADA",
              tmct0cta.getPkbroker1(), tmct0cta.getId().getNbooking(), tmct0cta.getId().getCdbroker(), tmct0cta.getId()
                  .getCdmercad(), tmct0cta.getId().getNucnfclt(), liquidadora);
      LOG.warn(error, ex);
      loggerSibbac.insertarRegistroNewTransaction(tmct0cta.getId().getNuorden(), tmct0cta.getId().getNbooking(),
          tmct0cta.getId().getNucnfclt(), error, tmct0cta.getTmct0sta().getDsestado(), "SIBBAC20");
      throw new SIBBACBusinessException(String.format("%s %s", error, ex.getMessage()), ex);
    }

  }

  /**
   * CONTABILIZAR TIPOS "A" PARA MEGARA-PARTENON DEVOLUCION
   * 
   * @param anular si true contravalor
   * @param tipo "A" Inicial
   * @param tmct0alo registro cliente allocation
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void contabilizarDev(boolean anular, String tipo, Tmct0alo tmct0alo) throws SIBBACBusinessException {

    String liquidadora = "";

    boolean foreignMarket = 'E' == tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo();
    if (foreignMarket) {
      liquidadora = xasBo.getCdcleareClearer(tmct0alo.getCdcleare());
    }
    else {
      liquidadora = tmct0alo.getCdcleare().trim();
    }
    if ("".equalsIgnoreCase(liquidadora.trim())) {

      throw new SIBBACBusinessException("[ContabilidadInternacionalBo] Liquidadora no encontrada -"
          + tmct0alo.getCdcleare().trim());
    }
    try {

      final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      final Date actualTime = new Date(System.currentTimeMillis());

      final Integer nuoprout = tmct0alo.getNuoprout();
      final short nupareje = tmct0alo.getNupareje();
      final short nuregcom = tgrl0cmdBo.getId(nuoprout, nupareje);
      final Character tpregcom = tipo.charAt(0);
      final Integer fhcolicm = new Integer(sdf.format(actualTime));
      final Character cdestado = 'A';
      final Character cdsvbbrk = 'V';
      final Integer fhentcon = 0;

      final String cdmoniso = tmct0alo.getCdmoniso();
      final String cdbroker = "";
      Tgrl0cms tgrl0cms = new Tgrl0cms();
      tgrl0cms.setId(new Tgrl0cmsId(nuoprout, nupareje, nuregcom));
      tgrl0cms.setTgrl0eje(tgrl0ejeDao.findOne(new Tgrl0ejeId(nuoprout, nupareje)));
      tgrl0cms.setCdsvbbrk(cdsvbbrk);
      tgrl0cms.setTpregcom(tpregcom);
      //
      BigDecimal imregcom = BigDecimal.ZERO;
      BigDecimal imregdiv = BigDecimal.ZERO;
      // Especificos segun tipo
      Integer fhmovcom = 0;
      String cdusrcom = "ROUTING";
      if (ContabilidadInternacionalBo.TIPO_A.equalsIgnoreCase(tipo.trim())) {
        cdusrcom = "ROUTING";
        fhmovcom = new Integer(sdf.format(actualTime));
        imregcom = tmct0alo.getEucomdev().setScale(2, RoundingMode.HALF_UP);
        imregdiv = tmct0alo.getImcomdev().setScale(2, RoundingMode.HALF_UP);
        // Cargar Tgrl0ord y Tgrl0eje si procede
        // loadTgrl0ord(tmct0alo.getFk_tmct0bok_1().getFk_tmct0ord_1().getNuorden());
        // loadTgrl0eje(tmct0alo);
      }
      else if (ContabilidadInternacionalBo.TIPO_C.equalsIgnoreCase(tipo.trim())) {
        // cdusrcom = "LIQ-" + liquidadora.trim();
        // fhmovcom = new BigDecimal(sdf.format(tmct0alo.getFevalorr()));
        // imregcom = tmct0alo.getEucomdev().setScale(2, RoundingMode.HALF_UP);
        // imregdiv = tmct0alo.getImcomdev().setScale(2, RoundingMode.HALF_UP);
      }

      // Si anular cambiar signo
      if (anular) {
        imregcom = imregcom.multiply(new BigDecimal(-1));
        imregdiv = imregdiv.multiply(new BigDecimal(-1));
      }
      tgrl0cms.setImregcom(imregcom.setScale(2, RoundingMode.HALF_UP));
      tgrl0cms.setImregdiv(imregdiv.setScale(2, RoundingMode.HALF_UP));
      tgrl0cms.setFhmovcom(fhmovcom);
      tgrl0cms.setFhcolicm(fhcolicm);
      tgrl0cms.setCdestado(cdestado);
      tgrl0cms.setCdusrcom(cdusrcom);
      tgrl0cms.setFhentcon(fhentcon);
      tgrl0cms.setCdmoniso(cdmoniso);
      tgrl0cms.setCdbroker(cdbroker);
      tgrl0cms.setCdcleare(liquidadora);

      if ((tgrl0cms.getImregcom().compareTo(BigDecimal.ZERO) != 0)
          || (tgrl0cms.getImregdiv().compareTo(BigDecimal.ZERO) != 0)) {
        tgrl0cms = cmsDao.save(tgrl0cms);
        dbsBo.insertRegister(tgrl0cms, true);
      }

    }
    catch (Exception ex) {

      String error = String.format(
          "[ContabilidadInternacionalBo]DEVOLUCION NUCNFCLT: %s CONTABILIDAD DIFERENCIAS %s NO REALIZADA"
              + tmct0alo.getPkentity().trim(), tmct0alo.getId().getNucnfclt().trim(), liquidadora.trim());
      LOG.warn(error, ex);

      throw new SIBBACBusinessException(String.format("%s %s", error, ex.getMessage()), ex);
    }

  }

  /**
   * CONTABILIZAR DESGLOSE ALO TIPOS "A"
   * 
   * @param anular true contabiliza en negativo false contabiliza normal
   * @param anular lsttmct0alo lista de allocations a contabilizar
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void contabilizarAloTipoAForeignRouting(boolean anular, Tmct0bok bok) throws SIBBACBusinessException {
    try {
      List<Tmct0alo> lstTmct0alo = aloBo.findAllByNuordenAndNbookingActivosInternacional(bok.getId().getNuorden(), bok
          .getId().getNbooking());
      this.contabilizarAloTipoA(anular, lstTmct0alo);
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * CONTABILIZAR DESGLOSE ALO TIPOS "A"
   * 
   * @param anular true contabiliza en negativo false contabiliza normal
   * @param anular lsttmct0alo lista de allocations a contabilizar
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void contabilizarAloTipoA(boolean anular, List<Tmct0alo> lstTmct0alo) throws SIBBACBusinessException {
    try {
      Map<String, Tgrl0ord> ordByNuorden = new HashMap<String, Tgrl0ord>();
      Tgrl0ord tgrl0ord;

      List<Tmct0cta> listTmct0cta;
      if (CollectionUtils.isNotEmpty(lstTmct0alo)) {

        for (Tmct0alo tmct0alo : lstTmct0alo) {
          // Cargar Tgrl0ord si procede
          tgrl0ord = ordByNuorden.get(tmct0alo.getId().getNuorden());
          if (tgrl0ord == null) {
            tgrl0ord = tgrl0ordBo.loadTgrl0ord(tmct0alo.getId().getNuorden());
            ordByNuorden.put(tmct0alo.getId().getNuorden(), tgrl0ord);

          }
          // Cargar Tgrl0eje si procede
          tgrl0ejeBo.loadTgrl0eje(tgrl0ord, tmct0alo);

          contabilizarLiq(anular, ContabilidadInternacionalBo.TIPO_A, tmct0alo);
          // Contabilizar Tipos A BROKER
          listTmct0cta = ctaBo.findAllByNuodenAndNbookingAndNucnfclt(tmct0alo.getId().getNuorden(), tmct0alo.getId()
              .getNbooking(), tmct0alo.getId().getNucnfclt());
          for (Tmct0cta tmct0cta : listTmct0cta) {
            contabilizarLiqMercado(anular, ContabilidadInternacionalBo.TIPO_A, tmct0cta);
          }
          // Contabilizar Tipos A DEV
          contabilizarDev(anular, ContabilidadInternacionalBo.TIPO_A, tmct0alo);
          // }
        }
        ordByNuorden.clear();
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
  }

  /**
   * CONTABILIZAR DESGLOSE ALO TIPOS "C"
   * 
   * @param anular true contabiliza en negativo false contabiliza normal
   * @param lsttmct0alo lsttmct0alo lista de allocations a contabilizar
   * 
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  public void contabilizarAloTipoC(boolean anular, List<Tmct0alo> lstTmct0alo) throws SIBBACBusinessException {
    try {
      for (Tmct0alo tmct0alo : lstTmct0alo) {
        if (anular) {
          // Solo si Liquidado / Pendiente Contabilizar -400
          if (tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.FOREING_CONTABILIZADO.getValue())) {
            contabilizarLiq(anular, ContabilidadInternacionalBo.TIPO_C, tmct0alo);
          }
        }
        else {
          // Solo si NO Liquidado / Pendiente Contabilizar -400
          if (!tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.FOREING_CONTABILIZADO.getValue())) {
            contabilizarLiq(anular, ContabilidadInternacionalBo.TIPO_C, tmct0alo);
          }
        }
      }
    }
    catch (Exception e) {
      throw e;
    }
  }

  // /**
  // * CONTABILIZAR MERCADO TIPOS "C"
  // *
  // * @param anular true contabiliza en negativo false contabiliza normal
  // * @param Tmct0pkb tmct0pkb a contabilizar
  // *
  // * @throws SIBBACBusinessException
  // *
  // * @generated
  // */
  // public void contabilizarPKB_C(boolean anular, Tmct0pkb tmct0pkb) throws
  // Exception {
  // try {
  // Collection<Tmct0cta> listTmct0cta =
  // JdoGetRelation.getListTmct0cta(tmct0pkb, hpm);
  // for (Tmct0cta tmct0cta : listTmct0cta) {
  // if (anular) {
  // // Solo si Liquidado / Pendiente Contabilizar -400
  // if (tmct0pkb.getFk_tmct0sta_23().getCdestado().trim()
  // .equalsIgnoreCase(TypeCdestado.FOREING_CONTABILIZADO.getValue())) {
  // contabilizarLiqMercado(anular, ContabilidadInternacionalBo.TIPO_C,
  // tmct0cta);
  // }
  // }
  // else {
  // // Solo si NO Liquidado / Pendiente Contabilizar -400
  // if (!tmct0pkb.getFk_tmct0sta_23().getCdestado().trim()
  // .equalsIgnoreCase(TypeCdestado.FOREING_CONTABILIZADO.getValue())) {
  // contabilizarLiqMercado(anular, ContabilidadInternacionalBo.TIPO_C,
  // tmct0cta);
  // }
  // }
  // }
  // }
  // catch (Exception e) {
  // throw e;
  // }
  // }

  /**
   * CANCEL DONE ACCOUNTING-RETROTRAER COMISIONES ORDENES (TYPES "C" y "D")
   * 
   * @param nbooking num booking
   * 
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  public void backCmsCd(Tmct0bok tmctbok) throws SIBBACBusinessException {
    try {

      boolean isForeignMarket = tmctbok.getTmct0ord().getCdclsmdo() == 'E';
      if (isForeignMarket) {
        // Recuperar Allocations del booking
        List<Tmct0alo> tmct0aloList = tmctbok.getTmct0alos();
        for (Iterator<Tmct0alo> iterator = tmct0aloList.iterator(); iterator.hasNext();) {
          Tmct0alo tmct0alo = (Tmct0alo) iterator.next();

          backComisionSVBLIQ(tmct0alo, 'P', 'C');
          backComisionSVBLIQ(tmct0alo, 'P', 'D');
          backComisionSVBLIQK(tmct0alo, 'K', 'C');
        }
      }
      else {
        // Recuperar Allocations del booking
        List<Tmct0alo> tmct0aloList = tmctbok.getTmct0alos();
        for (Iterator<Tmct0alo> iterator = tmct0aloList.iterator(); iterator.hasNext();) {
          Tmct0alo tmct0alo = (Tmct0alo) iterator.next();
          // SVB
          backComision(tmct0alo, 'S', 'C', "ROUTING"); // Sociedad
          backComision(tmct0alo, 'R', 'C', "ROUTING"); // Rectora
          // Devolucion
          backComision(tmct0alo, 'B', 'C', "ROUTING"); // Devolucion
          // Banco
          backComision(tmct0alo, 'N', 'C', "ROUTING"); // Banco
        }
      }
      // }
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * CANCEL DONE ACCOUNTING-RETROTRAER COMISIONES ORDENES (TYPES "A"-"C" y "D")
   * 
   * @param tmct0alo allocation client
   * @param devengo true elimina tipos "A"/false no elimina tipos "A"
   * 
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  public void backCmsCd(Tmct0alo tmct0alo, boolean devengo) throws SIBBACBusinessException {

    try {
      boolean isForeignMarket = tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo() == 'E';
      if (tmct0alo.getTmct0sta().getId().getCdestado().trim()
          .equalsIgnoreCase(TypeCdestado.FOREING_CONTABILIZADO.getValue())
          || tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.CANCEL_DONE_ACCOUNTING.getValue())
          || tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.FOREING_WAIT_RESPONSE_CLEARER.getValue())
          || tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.CLIENT_PARCIALMENTE_LIQUIDADO.getValue())
          || tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.CLIENT_PENDIENTE_RESPUESTA_LIQUIDADORA.getValue())
          || tmct0alo.getTmct0sta().getId().getCdestado().trim()
              .equalsIgnoreCase(TypeCdestado.DIFERENCIAS_LIQUIDADORA.getValue())) {
        // Para Extranjero
        if (isForeignMarket) {
          if (devengo) {
            backComision(tmct0alo, 'P', 'A', "ROUTING");
          }
          backComisionSVBLIQ(tmct0alo, 'P', 'C');
          backComisionSVBLIQ(tmct0alo, 'P', 'D');

          // Cuando se trate de una operación de Routing, en la tabla de
          // comisiones (TGRL0CMS) también
          // se debe retroceder la pata de mercado liquidada
          if (ordBo.isRouting(tmct0alo.getTmct0bok().getTmct0ord())) {
            backComisionSVBLIQ(tmct0alo, 'K', 'C');
          }

        }
        else {
          // SVB
          backComision(tmct0alo, 'S', 'C', "ROUTING"); // Sociedad
          backComision(tmct0alo, 'R', 'C', "ROUTING"); // Rectora
          // Devolucion
          backComision(tmct0alo, 'B', 'C', "ROUTING"); // Devolucion
          // Banco
          backComision(tmct0alo, 'N', 'C', "ROUTING"); // Banco
        }

      }
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * RETROTRAER COMISIONES ORDENES (ALL TYPES)
   * 
   * @param nbooking num booking
   * 
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  public void backCMS(Tmct0bok tmctbok) throws SIBBACBusinessException {
    try {
      // Recuperar Allocations del booking
      Collection<Tmct0alo> tmct0aloList = tmctbok.getTmct0alos();
      // Para Extranjero
      boolean isForeignMarket = tmctbok.getTmct0ord().getCdclsmdo() == 'E';
      if (isForeignMarket) {
        for (Iterator<Tmct0alo> iterator = tmct0aloList.iterator(); iterator.hasNext();) {
          Tmct0alo tmct0alo = (Tmct0alo) iterator.next();
          // SVB
          backComision(tmct0alo, 'P', 'A', "ROUTING");
          backComisionSVBLIQ(tmct0alo, 'P', 'C');
          backComisionSVBLIQ(tmct0alo, 'P', 'D');
          // BROKER
          backComisionSVBLIQK(tmct0alo, 'K', 'A');
          backComisionSVBLIQK(tmct0alo, 'K', 'C');
          backComisionSVBLIQK(tmct0alo, 'K', 'D');
          // backComision(tmct0alo, "K", "D", "ROUTING");
          // Devolucion
          backComision(tmct0alo, 'V', 'A', "ROUTING");
          backComision(tmct0alo, 'V', 'P', "ROUTING");
          backComision(tmct0alo, 'V', 'L', "ROUTING");
        }
      }
      else {
        // Para Nacional - No se utiliza y !Atencion anula por Alo no por Ald
        // (pendiente aclaracion)¡
        // for (Iterator iterator = tmct0aloList.iterator();
        // iterator.hasNext();) {
        // Tmct0alo tmct0alo = (Tmct0alo) iterator.next();
        // // SVB
        // backComision(tmct0alo, "S", "A", "ROUTING"); // Sociedad
        // backComision(tmct0alo, "S", "C", "ROUTING"); // Sociedad
        // backComision(tmct0alo, "R", "A", "ROUTING"); // Rectora
        // backComision(tmct0alo, "R", "C", "ROUTING"); // Rectora
        // // Devolucion
        // backComision(tmct0alo, "B", "A", "ROUTING"); // Broker
        // backComision(tmct0alo, "B", "C", "ROUTING"); // Broker
        // // Banco
        // backComision(tmct0alo, "N", "A", "ROUTING"); // Banco
        // backComision(tmct0alo, "N", "C", "ROUTING"); // Banco
        // // Banco
        // backComision(tmct0alo, "D", "A", "ROUTING"); // Devolucion
        // backComision(tmct0alo, "D", "C", "ROUTING"); // Devolucion
        // // Banco
        // backComision(tmct0alo, "O", "A", "ROUTING"); // MAB
        // backComision(tmct0alo, "O", "C", "ROUTING"); // MAB
        // }
      }

    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * RETROTRAER COMISIONES ORDENES
   * 
   * @param tmctalo allocation del booking
   * @param cdsvbbrk indicativo comision
   * @param tpregcom tipo registro
   * @param cdusrcom usuario
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  private void backComision(Tmct0alo tmctalo, Character cdsvbbrk, Character tpregcom, String cdusrcom)
      throws SIBBACBusinessException {
    BigDecimal sumImregcom = BigDecimal.ZERO;
    BigDecimal sumImregdiv = BigDecimal.ZERO;
    try {
      List<Object[]> sumList = cmsDao.findImregcomImregdivByNuoproutAndNuparejeAndCdsvbbrkAndTpregcom(
          tmctalo.getNuoprout(), tmctalo.getNupareje(), cdsvbbrk, tpregcom);
      for (Object[] o : sumList) {
        sumImregcom = (BigDecimal) o[0];
        sumImregdiv = (BigDecimal) o[1];

        // Si nulos volver
        if ((sumImregcom == null) || (sumImregdiv == null)) {
          return;
        }
        // Si los dos a cero no hacer nada
        if ((sumImregcom.compareTo(BigDecimal.ZERO) == 0) && (sumImregdiv.compareTo(BigDecimal.ZERO) == 0)) {
          return;
        }
        writeCms(tmctalo, cdsvbbrk, tpregcom, cdusrcom, sumImregcom, sumImregdiv);
      }
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * RETROTRAER COMISIONES ORDENES
   * 
   * @param tmctalo allocation del booking
   * @param cdsvbbrk indicativo comision
   * @param tpregcom tipo registro
   * @param cdusrcom usuario
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  private void backComisionSVBLIQ(Tmct0alo tmctalo, Character cdsvbbrk, Character tpregcom)
      throws SIBBACBusinessException {
    BigDecimal sumImregcom = BigDecimal.ZERO;
    BigDecimal sumImregdiv = BigDecimal.ZERO;
    try {

      List<Object[]> sumList = cmsDao.findImregcomImregdivByNuoproutAndNuparejeAndCdsvbbrkAndTpregcom(
          tmctalo.getNuoprout(), tmctalo.getNupareje(), cdsvbbrk, tpregcom);

      Iterator<Object[]> itr = sumList.iterator();
      while (itr.hasNext()) {
        Object[] object = (Object[]) itr.next();
        sumImregcom = (BigDecimal) object[0];
        sumImregdiv = (BigDecimal) object[1];
        break;
      }
      // Si nulos volver
      if ((sumImregcom == null) || (sumImregdiv == null)) {
        return;
      }
      // Si los dos a cero no hacer nada
      if ((sumImregcom.compareTo(BigDecimal.ZERO) == 0) && (sumImregdiv.compareTo(BigDecimal.ZERO) == 0)) {
        return;
      }
      writeCms(tmctalo, cdsvbbrk, tpregcom, null, sumImregcom, sumImregdiv);
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * RETROTRAER COMISIONES ORDENES TIPOS K AGRUPAR POR CLEARER-BROKER PARA TENER
   * DESGLOSE
   * 
   * @param tmctalo allocation del booking
   * @param cdsvbbrk indicativo comision
   * @param tpregcom tipo registro
   * @param cdusrcom usuario
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  private void backComisionSVBLIQK(Tmct0alo tmctalo, Character cdsvbbrk, Character tpregcom)
      throws SIBBACBusinessException {
    try {

      List<Object[]> sumList = cmsDao
          .findImregcomImregdivByNuoproutAndNuparejeAndCdsvbbrkAndTpregcomGroupByCdcleareCdbroker(
              tmctalo.getNuoprout(), tmctalo.getNupareje(), cdsvbbrk, tpregcom);

      for (Object[] object : sumList) {
        String cdcleare = (String) object[0];
        String cdbroker = (String) object[1];
        BigDecimal sumImregcom = (BigDecimal) object[2];
        BigDecimal sumImregdiv = (BigDecimal) object[3];
        // Si nulos
        if ((sumImregcom == null) || (sumImregdiv == null)) {
          continue;
        }
        // Si los dos a cero no hacer nada
        if ((sumImregcom.compareTo(BigDecimal.ZERO) == 0) && (sumImregdiv.compareTo(BigDecimal.ZERO) == 0)) {
          continue;
        }
        writeCmsK(tmctalo, cdcleare, cdbroker, cdsvbbrk, tpregcom, sumImregcom, sumImregdiv);
      }
    }
    catch (Exception e) {
      throw e;
    }
  }

  /**
   * GRABAR REGISTRO COMISION
   * 
   * @param tmctaloSdo allocation del booking
   * @param cdsvbbrk indicativo comision
   * @param tpregcom tipo registro
   * @param cdusrcom usuario
   * @param sumImregcom suma de comisiones
   * @param sumImregdiv suma de comisiones divisa
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  private void writeCms(Tmct0alo tmctalo, Character cdsvbbrk, Character tpregcom, String cdusrcom,
      BigDecimal sumImregcom, BigDecimal sumImregdiv) throws SIBBACBusinessException {

    try {
      String liquidadora = "";
      boolean isForeignMarket = tmctalo.getTmct0bok().getTmct0ord().getCdclsmdo() == 'E';
      if (isForeignMarket) {
        liquidadora = xasBo.getCdcleareClearer(tmctalo.getCdcleare());
      }
      else {
        liquidadora = tmctalo.getCdcleare().trim();
      }

      final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      final Date actualTime = new Date(System.currentTimeMillis());

      final Integer nuoprout = tmctalo.getNuoprout();
      final short nupareje = tmctalo.getNupareje();
      final short nuregcom = tgrl0cmdBo.getId(nuoprout, nupareje);
      final Integer fhmovcom = new Integer(sdf.format(actualTime));
      final Integer fhcolicm = new Integer(sdf.format(actualTime));
      final Character cdestado = 'A';
      final Integer fhentcon = 0;
      final String cdmoniso = tmctalo.getCdmoniso();

      Tgrl0cms tgrl0cms = new Tgrl0cms();
      tgrl0cms.setId(new Tgrl0cmsId(nuoprout, nupareje, nuregcom));

      tgrl0cms.setCdsvbbrk(cdsvbbrk);
      tgrl0cms.setTpregcom(tpregcom);
      tgrl0cms.setImregcom(sumImregcom.setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
      tgrl0cms.setFhmovcom(fhmovcom);
      tgrl0cms.setFhcolicm(fhcolicm);
      tgrl0cms.setCdestado(cdestado);
      if (cdusrcom == null) {
        tgrl0cms.setCdusrcom("LIQ" + liquidadora.trim());
      }
      else {
        tgrl0cms.setCdusrcom(cdusrcom);
      }
      tgrl0cms.setFhentcon(fhentcon);
      tgrl0cms.setImregdiv(sumImregdiv.setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
      tgrl0cms.setCdmoniso(cdmoniso);
      tgrl0cms.setCdbroker("");
      tgrl0cms.setCdcleare(liquidadora); // Ojo para Nacional es la liquidadora
                                         // del alc esta no vale

      tgrl0cms = cmsDao.save(tgrl0cms);
      dbsBo.insertRegister(tgrl0cms, true);
    }
    catch (Exception ex) {
      LOG.warn(ex.getMessage(), ex);
      throw ex;
    }
  }

  /**
   * GRABAR REGISTRO COMISION TIPOS K
   * 
   * @param tmctaloSdo allocation del booking
   * @param cdcleare liquidadora
   * @param cdbroker broker
   * @param cdsvbbrk indicativo comision
   * @param tpregcom tipo registro
   * @param cdusrcom usuario
   * @param sumImregcom suma de comisiones
   * @param sumImregdiv suma de comisiones divisa
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  private void writeCmsK(Tmct0alo tmctalo, String cdcleare, String cdbroker, Character cdsvbbrk, Character tpregcom,
      BigDecimal sumImregcom, BigDecimal sumImregdiv) throws SIBBACBusinessException {

    try {
      final String liquidadora = cdcleare.trim();
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
      final Date actualTime = new Date(System.currentTimeMillis());

      final Integer nuoprout = tmctalo.getNuoprout();
      final short nupareje = tmctalo.getNupareje();
      final short nuregcom = tgrl0cmdBo.getId(nuoprout, nupareje);
      final Integer fhmovcom = new Integer(sdf.format(actualTime));
      final Integer fhcolicm = new Integer(sdf.format(actualTime));
      final Character cdestado = 'A';
      final Integer fhentcon = 0;
      final String cdmoniso = tmctalo.getCdmoniso();

      Tgrl0cms tgrl0cms = new Tgrl0cms();
      tgrl0cms.setId(new Tgrl0cmsId(nuoprout, nupareje, nuregcom));

      tgrl0cms.setCdsvbbrk(cdsvbbrk);
      tgrl0cms.setTpregcom(tpregcom);
      tgrl0cms.setImregcom(sumImregcom.setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
      tgrl0cms.setFhmovcom(fhmovcom);
      tgrl0cms.setFhcolicm(fhcolicm);
      tgrl0cms.setCdestado(cdestado);
      tgrl0cms.setCdusrcom("LIQ" + cdcleare.trim());
      tgrl0cms.setFhentcon(fhentcon);
      tgrl0cms.setImregdiv(sumImregdiv.setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
      tgrl0cms.setCdmoniso(cdmoniso);
      tgrl0cms.setCdbroker(cdbroker.trim());
      tgrl0cms.setCdcleare(liquidadora);

      tgrl0cms = cmsDao.save(tgrl0cms);
      dbsBo.insertRegister(tgrl0cms, true);

    }
    catch (Exception ex) {
      LOG.warn(ex.getMessage(), ex);
      throw ex;
    }
  }

  /**
   * Control exite Liquidacion
   * 
   * @param tmctalo allocation del booking
   * @param cdsvbbrk indicativo comision
   * @param tpregcom tipo registro
   * @param cdusrcom usuario
   * @throws SIBBACBusinessException
   * 
   * @generated
   */
  public boolean exitComisionSVBLIQ(sibbac.business.wrappers.database.model.Tmct0alo tmctalo, Character cdsvbbrk,
      Character tpregcom) throws SIBBACBusinessException {
    BigDecimal sumImregcom = BigDecimal.ZERO;
    BigDecimal sumImregdiv = BigDecimal.ZERO;
    boolean exit = false;

    List<Object[]> sumList = cmsDao.findImregcomImregdivByNuoproutAndNuparejeAndCdsvbbrkAndTpregcom(
        tmctalo.getNuoprout(), tmctalo.getNupareje(), cdsvbbrk, tpregcom);

    Iterator<Object[]> itr = sumList.iterator();
    while (itr.hasNext()) {
      Object[] object = (Object[]) itr.next();
      sumImregcom = (BigDecimal) object[0];
      sumImregdiv = (BigDecimal) object[1];
      break;
    }
    if (tmctalo.getImsvbeu() != null && tmctalo.getImsvb() != null && tmctalo.getImsvbeu().compareTo(sumImregcom) == 0
        && tmctalo.getImsvb().compareTo(sumImregdiv) == 0) {
      exit = true;
    }

    return exit;
  }
}
