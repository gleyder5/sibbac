package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.mq.bo.JmsMessagePtiToDataBaseBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.pti.PTIMessageFactory;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_LECTURA_PTI, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 * 7-22 ? * MON-FRI")
public class TaskLecturaPti extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskLecturaPti.class);

  @Autowired
  private JmsMessagePtiToDataBaseBo ptiToDatabaseBo;

  @Autowired
  private PtiCommomns ptiCommons;

  public TaskLecturaPti() {
  }

  @Override
  public void executeTask() throws Exception {
    LOG.debug("[executeTask] inicio.");
    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }
    LOG.debug("[executeTask] Leyendo msg pti...");
    ptiToDatabaseBo.executeTask(false);
    LOG.debug("[executeTask] fin.");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_LECTURA_PTI;
  }

}
