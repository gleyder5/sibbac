package sibbac.business.operaciones.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

public class PtiExecutionPageQuery extends AbstractDynamicPageQuery {

  private static final String FECHA_CONTRATACION = "OP.FCONTRATACION";
  private static final String CAMARA = "OP.CDCAMARA";
  private static final String ISIN = "OP.CDISIN";
  private static final String COMPRA_VENTA = "OP.CDSENTIDO";
  private static final String MIEMBRO_NEGOCIADOR = "OP.CDMIEMBROMKT";

  private Character filtroRecibidosPdteRecibir;

  public PtiExecutionPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public PtiExecutionPageQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns,
      List<SimpleDynamicFilter<?>> staticFilters) {
    super(filters, columns, staticFilters);
  }

  @Override
  public String getSelect() {
    return "SELECT OP.CDCAMARA CAMARA, OP.NUORDENMKT AS NUORDENMKT,  OP.NUEXEMKT AS NUEXEMKT, OP.FCONTRATACION FETRADE, "
        + " OP.CDMIEMBROMKT AS CDMIEMBROMKT, OP.CDSENTIDO SENTIDO, OP.CD_PLATAFORMA_NEGOCIACION EXECUTION_TRADING_VENUE, OP.CDSEGMENTO AS CDSEGMENTO,  OP.CDOPERACIONMKT, "
        + " OP.CDISIN ISIN, OP.IMPRECIO PRECIO, OP.NUTITULOS TITULOS, CA.NUTITPENDCASE, OP.AUDIT_FECHA_CAMBIO ";
  }

  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("CCP", "CAMARA"),
      new DynamicColumn("Market Order", "NUORDENMKT"), new DynamicColumn("Trade Number", "NUEXEMKT"),
      new DynamicColumn("Trade Date", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Member", "CDMIEMBROMKT"), new DynamicColumn("Exec. Type", "SENTIDO"),
      new DynamicColumn("Execution Segment", "EXECUTION_TRADING_VENUE"),
      new DynamicColumn("Market Segment", "CDSEGMENTO"), new DynamicColumn("Trade Type", "CDOPERACIONMKT"),
      new DynamicColumn("Isin", "ISIN"), new DynamicColumn("Price", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Quantity", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Quantity Left", "NUTITPENDCASE", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Fhaudit", "AUDIT_FECHA_CAMBIO", DynamicColumn.ColumnType.DATE) };

  @Override
  public String getFrom() {
    return " FROM TMCT0OPERACIONCDECC AS OP INNER JOIN TMCT0CASEOPERACIONEJE AS CA ON OP.IDOPERACIONCDECC = CA.IDOPERACIONECC "

    ;
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {
    StringBuilder postWhere = new StringBuilder("");
    if (filtroRecibidosPdteRecibir != null) {
      switch (filtroRecibidosPdteRecibir) {
        case 'P':
          postWhere.append("CA.NUTITPENDCASE > 0");
          break;
        case 'R':
          postWhere.append("CA.NUTITPENDCASE = 0");
          break;

        default:
          break;
      }
    }
    return postWhere.toString();
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    boolean lanzarEx = true;
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      switch (filter.getField()) {

        case FECHA_CONTRATACION:

          if (!filter.getValues().isEmpty()) {
            lanzarEx = false;
          }
          break;

        default:
          break;
      }
      if (!lanzarEx) {
        break;
      }
    }
    if (lanzarEx) {
      throw new SIBBACBusinessException("Mandatory filter not set.");
    }
  }

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new DateDynamicFilter(FECHA_CONTRATACION, "F. Contratación"));
    res.add(new StringDynamicFilter(COMPRA_VENTA, "Sentido"));
    res.add(new StringDynamicFilter(CAMARA, "CCP"));
    res.add(new StringDynamicFilter(ISIN, "ISIN"));
    res.add(new StringDynamicFilter(MIEMBRO_NEGOCIADOR, "Miembro"));

    return res;
  }

  public Character getFiltroRecibidosPdteRecibir() {
    return filtroRecibidosPdteRecibir;
  }

  public void setFiltroRecibidosPdteRecibir(Character filtroRecibidosPdteRecibir) {
    this.filtroRecibidosPdteRecibir = filtroRecibidosPdteRecibir;
  }

}
