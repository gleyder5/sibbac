package sibbac.business.comunicaciones.euroccp.realigment.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.realigment.model.EuroCcpExecutionRealigment;

@Component
public interface EuroCcpExecutionRealigmentDao extends JpaRepository<EuroCcpExecutionRealigment, Long> {

  @Query("select distinct r.clientNumber, r.unsettledMovement.processingDate, r.unsettledMovement.transactionDate "
      + " from EuroCcpExecutionRealigment r where r.estadoProcesado = :estadoProcesado ")
  List<Object[]> findAllClientNumberByEstadoProcesado(@Param("estadoProcesado") int estadoProcesado);

  @Query("select r from EuroCcpExecutionRealigment r where r.estadoProcesado = :estadoProcesado and r.clientNumber = :clientNumber ")
  List<EuroCcpExecutionRealigment> findAllByEstadoProcesadoAndClientNumber(
      @Param("estadoProcesado") int estadoProcesado, @Param("clientNumber") String clientNumber);

  @Query("select new sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO(r.id) "
      + " from EuroCcpExecutionRealigment r where r.estadoProcesado = :estadoProcesado and r.clientNumber = :clientNumber and r.unsettledMovement.processingDate = :pprocessingDate "
      + " and r.unsettledMovement.transactionDate = :ptransactionDate")
  List<EuroCcpIdDTO> findAllEuroCcpIdDTOByEstadoProcesadoAndClientNumber(@Param("estadoProcesado") int estadoProcesado,
      @Param("clientNumber") String clientNumber, @Param("pprocessingDate") Date processingDate,
      @Param("ptransactionDate") Date transactionDate);

  @Query("select r from EuroCcpExecutionRealigment r where r.tradeDate = :tradeDate and r.executionReference = :executionReference "
      + " and r.mic = :mic and r.accountNumberFrom = :accountNumberFrom and r.accountNumberTo = :accountNumberTo "
      + " and r.numberShares = :numberShares and r.clientNumber = :clientNumber "
      + " and r.fileSent.parentFile.id = :idFileSent ")
  List<EuroCcpExecutionRealigment> findAllByTradeDateAndExecutionReferenceAndMicAndAccNumberFromAndAccNumberToAndNumberSheresAndClientNumberAndFileSent(
      @Param("tradeDate") Date tradeDate, @Param("executionReference") String executionReference,
      @Param("mic") String mic, @Param("accountNumberFrom") String accountNumberFrom,
      @Param("accountNumberTo") String accountNumberTo, @Param("numberShares") BigDecimal numberShares,
      @Param("clientNumber") String clientNumber, @Param("idFileSent") long idFileSent);

}
