package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileSentDao;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileTypeDao;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReferenceStaticDataDTO;
import sibbac.business.comunicaciones.euroccp.titulares.mapper.EuroCcpOwnershipReferenceStaticDataFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReferenceStaticData;
import sibbac.business.comunicaciones.euroccp.titulares.writer.EuroCcpOwnershipReferenceStaticDataItemWriter;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean.TipoPersona;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.TipoNacionalidad;

@Service(value = "euroCcpOwnershipReferenceStaticDataFileWriterBo")
public class EuroCcpOwnershipReferenceStaticDataFileWriterBo extends
    EuroCcpAbstractFileGenerationBo<EuroCcpIdDTO, EuroCcpOwnershipReferenceStaticDataDTO> {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpOwnershipReferenceStaticDataFileWriterBo.class);

  @Value("${sibbac.euroccp.titulares.ors.file.send.header.text}")
  private List<String> headerTexts;

  @Value("${sibbac.euroccp.titulares.ors.birth.date.format}")
  private String birthDateFormat;

  @Autowired
  private EuroCcpFileSentDao euroCcpFileSentDao;

  @Autowired
  private EuroCcpFileSentBo euroCcpFileSentBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0estadoBo estBo;

  @Autowired
  private Tmct0enviotitularidadDao envioTitularidadDao;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao desgloseCamaraEnvioRtDao;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0paisesDao paisesDao;

  @Autowired
  private Tmct0afiDao afiDao;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private EuroCcpFileTypeDao euroCcpFileTypeDao;

  @Autowired
  private EuroCcpFileSentDao euroCcpSentFileDao;

  @Autowired
  private EuroCcpOwnershipReferenceStaticDataBo euroCcpOwnershipReferenceStaticDataBo;

  @Autowired
  private Tmct0xasBo conversionesBo;

  public EuroCcpOwnershipReferenceStaticDataFileWriterBo() {
  }

  @Override
  protected EuroCcpFileTypeEnum getPrincipalFileType() {
    return EuroCcpFileTypeEnum.ORS;
  }

  @Override
  protected EuroCcpFileTypeEnum getTmpFileType() {
    return EuroCcpFileTypeEnum.ORS_TMP;
  }

  @Override
  protected boolean getWriteFooterPrincipalFile() {
    return false;
  }

  /**
   * @param ti
   * @param alcs
   * @param afisEnviar
   * @param afisActualizar
   */
  private void recuperarDatosGeneracionTxt(Tmct0enviotitularidad ti, List<Tmct0alc> alcs, List<Tmct0afi> afisEnviar,
      List<Tmct0afi> afisActualizar) {
    LOG.debug("[recuperarDatosGeneracionTxt] buscando Tmct0DesgloseCamaraEnvioRt de reg titular: {}...", ti);
    List<Tmct0DesgloseCamaraEnvioRt> rts = desgloseCamaraEnvioRtDao.findAllByIdEnvioTitularidadAndNotCdestado(
        ti.getIdenviotitularidad(), 'C', 'A');

    // hacemos dos recorridos de los registros, primero para coger los que estan
    // enviandose y asi que sea ese el titular
    // que vamos a enviar
    for (Tmct0DesgloseCamaraEnvioRt rt : rts) {
      if (rt.getDesgloseCamara().getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
          .getId()
          && rt.getDesgloseCamara().getTmct0alc().getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && (rt.getDesgloseCamara().getTmct0estadoByCdestadotit().getIdestado() == EstadosEnumerados.TITULARIDADES.ENVIANDOSE
              .getId() || rt.getDesgloseCamara().getTmct0estadoByCdestadotit().getIdestado() == EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION
              .getId())) {
        LOG.debug("[recuperarDatosGeneracionTxt] buscando afis : {}...", rt.getDesgloseCamara().getTmct0alc().getId());
        if (CollectionUtils.isEmpty(afisEnviar)) {

          afisEnviar.addAll(afiDao.findActivosByAlcData(rt.getDesgloseCamara().getTmct0alc().getId().getNuorden(), rt
              .getDesgloseCamara().getTmct0alc().getId().getNbooking(), rt.getDesgloseCamara().getTmct0alc()
              .getNucnfclt(), rt.getDesgloseCamara().getTmct0alc().getNucnfliq()));
          afisActualizar.addAll(afisEnviar);

        }
        else {
          afisActualizar.addAll(afiDao.findActivosByAlcData(rt.getDesgloseCamara().getTmct0alc().getId().getNuorden(),
              rt.getDesgloseCamara().getTmct0alc().getId().getNbooking(), rt.getDesgloseCamara().getTmct0alc()
                  .getNucnfclt(), rt.getDesgloseCamara().getTmct0alc().getNucnfliq()));
        }
        alcs.add(rt.getDesgloseCamara().getTmct0alc());
      }
    }
    // cogemos el resto
    for (Tmct0DesgloseCamaraEnvioRt rt : rts) {
      if (rt.getDesgloseCamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
          .getId()
          && rt.getDesgloseCamara().getTmct0alc().getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && rt.getDesgloseCamara().getTmct0estadoByCdestadotit().getIdestado() != EstadosEnumerados.TITULARIDADES.ENVIANDOSE
              .getId()
          && rt.getDesgloseCamara().getTmct0estadoByCdestadotit().getIdestado() != EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION
              .getId()) {
        LOG.debug("[recuperarDatosGeneracionTxt] buscando afis : {}...", rt.getDesgloseCamara().getTmct0alc().getId());
        if (CollectionUtils.isEmpty(afisEnviar)) {

          afisEnviar.addAll(afiDao.findActivosByAlcData(rt.getDesgloseCamara().getTmct0alc().getId().getNuorden(), rt
              .getDesgloseCamara().getTmct0alc().getId().getNbooking(), rt.getDesgloseCamara().getTmct0alc()
              .getNucnfclt(), rt.getDesgloseCamara().getTmct0alc().getNucnfliq()));
          afisActualizar.addAll(afisEnviar);

        }
        else {
          afisActualizar.addAll(afiDao.findActivosByAlcData(rt.getDesgloseCamara().getTmct0alc().getId().getNuorden(),
              rt.getDesgloseCamara().getTmct0alc().getId().getNbooking(), rt.getDesgloseCamara().getTmct0alc()
                  .getNucnfclt(), rt.getDesgloseCamara().getTmct0alc().getNucnfliq()));
        }
        alcs.add(rt.getDesgloseCamara().getTmct0alc());
      }
    }
    LOG.debug("[recuperarDatosGeneracionTxt] Alcs: {} AfisEnviar: {} AfisActualizar: {}", alcs.size(),
        afisEnviar.size(), afisActualizar.size());

  }

  /**
   * @param ti
   * @param afiEnviar
   * @return
   * @throws SIBBACBusinessException
   */
  private EuroCcpOwnershipReferenceStaticDataDTO mapTmct0afiIntoEuroCcpExcelTitularDTO(Tmct0enviotitularidad ti,
      Tmct0afi afiEnviar, Map<String, String> conversionesTxt) throws SIBBACBusinessException {
    SimpleDateFormat sdf = new SimpleDateFormat(birthDateFormat);
    EuroCcpOwnershipReferenceStaticDataDTO dto = new EuroCcpOwnershipReferenceStaticDataDTO();
    dto.setOwnerReference(ti.getTmct0referenciatitular().getCdreftitularpti());
    if (TipoPersona.JURIDICA.getTipoPersona() == afiEnviar.getTpsocied()) {
      dto.setOwnerName(StringUtils.substring(afiEnviar.getNbclient1().trim(), 0, 140));
      dto.setFirstSurname("");
      dto.setSecondName("");
    }
    else {
      dto.setOwnerName(StringUtils.substring(afiEnviar.getNbclient().trim(), 0, 140));
      dto.setFirstSurname(StringUtils.substring(afiEnviar.getNbclient1().trim(), 0, 40));
      dto.setSecondName(StringUtils.substring(afiEnviar.getNbclient2().trim(), 0, 40));
    }

    dto.setTypeIdentificacion(afiEnviar.getTpidenti());
    if (afiEnviar.getCdholder().trim().length() > 11) {
      String msg = MessageFormat.format("INCIDENCIA-Identificacion no se puede mandar a EUROCCP {0} afi: {1}",
          afiEnviar.getCdholder().trim(), afiEnviar.getId());
      logBo.insertarRegistroNewTransaction(afiEnviar.getId().getNuorden(), afiEnviar.getId().getNbooking(), afiEnviar
          .getId().getNucnfclt(), msg, "", "EUROC_CCP");
      throw new SIBBACBusinessException(msg);
    }
    dto.setIdentificationCode(StringUtils.substring(afiEnviar.getCdholder().trim(), 0, 11));
    dto.setNaturalLegalInd(afiEnviar.getTpsocied());

    dto.setNationality(afiEnviar.getCdnactit().trim());
    dto.setNationalForeignInd(afiEnviar.getTpnactit());
    dto.setOwnerType(afiEnviar.getTptiprep());

    dto.setCity(StringUtils.substring(afiEnviar.getNbciudad().trim(), 0, 20));
    Tmct0paises paisResidencia = paisesDao.findByCdHaciendaActivo(afiEnviar.getCddepais());
    if (paisResidencia == null) {
      String msg = MessageFormat.format("INCIDENCIA-Pais residencia no encontrado en tabla de paises {0} afi: {1}",
          afiEnviar.getCddepais(), afiEnviar.getId());
      logBo.insertarRegistroNewTransaction(afiEnviar.getId().getNuorden(), afiEnviar.getId().getNbooking(), afiEnviar
          .getId().getNucnfclt(), msg, "", "EUROC_CCP");
      throw new SIBBACBusinessException(msg);
    }

    if (TipoNacionalidad.NACIONAL_COD_ISO.getValue().equals(paisResidencia.getId().getCdisonum().trim())) {
      dto.setAddress(StringUtils.substring(afiEnviar.getTpdomici().trim() + " " + afiEnviar.getNbdomici().trim() + " "
          + afiEnviar.getNudomici().trim(), 0, 40));
      dto.setPostalCode(afiEnviar.getCdpostal().trim());
    }
    else {
      dto.setAddress(StringUtils
          .substring(afiEnviar.getNbdomici().trim() + " " + afiEnviar.getNudomici().trim(), 0, 40));
      dto.setPostalCode("");
    }
    dto.setCountryResidence(paisResidencia.getId().getCdisonum().trim());
    if (afiEnviar.getFechaNacimiento() != null) {
      dto.setBirthDate(sdf.format(afiEnviar.getFechaNacimiento()));
    }
    else {
      dto.setBirthDate("");
    }
    for (String key : conversionesTxt.keySet()) {
      dto.setOwnerName(dto.getOwnerName().replaceAll(key, conversionesTxt.get(key)));
      dto.setFirstSurname(dto.getFirstSurname().replaceAll(key, conversionesTxt.get(key)));
      dto.setSecondName(dto.getSecondName().replaceAll(key, conversionesTxt.get(key)));
      dto.setCity(dto.getCity().replaceAll(key, conversionesTxt.get(key)));
      dto.setAddress(dto.getAddress().replaceAll(key, conversionesTxt.get(key)));
    }
    return dto;
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.business.comunicaciones.euroccp.titulares.bo.
   * EuroCcpAbstractFileGenerationBo#appendTmpFiles(sibbac.business
   * .comunicaciones.euroccp.titulares.model.EuroCcpFileSent, java.util.List,
   * java.nio.file.Path, java.nio.file.Path, java.nio.file.Path,
   * java.lang.String, java.nio.charset.Charset, boolean, java.lang.String)
   */
  @Override
  protected void appendTmpFiles(EuroCcpFileSent principalFileRecord, List<EuroCcpFileSent> ficherosTmp, Path tmpPath,
      Path tratadosPath, Path pendientesPath, String compressExtension, Charset fileCharset, boolean writeFooter,
      TimeZone timeZone) throws SIBBACBusinessException, IOException {
    int lineNumber;

    String line;
    Path file;
    EuroCcpOwnershipReferenceStaticDataItemWriter excelWriter = null;

    EuroCcpOwnershipReferenceStaticDataDTO titular = null;
    List<EuroCcpOwnershipReferenceStaticDataDTO> titulares = new ArrayList<EuroCcpOwnershipReferenceStaticDataDTO>();

    if (CollectionUtils.isNotEmpty(ficherosTmp)) {
      String excelFinalName = principalFileRecord.getFileName();
      String excelFinalBaseName = FilenameUtils.getBaseName(excelFinalName);
      String pendienteEnviarName = null;
      boolean compress = StringUtils.isNotBlank(compressExtension)
          && (compressExtension.equalsIgnoreCase("ZIP") || compressExtension.equalsIgnoreCase("GZ"));

      if (compress) {
        if (compressExtension.equalsIgnoreCase("ZIP")) {
          pendienteEnviarName = excelFinalBaseName + "." + compressExtension;
        }
        else if (compressExtension.equalsIgnoreCase("GZ")) {
          pendienteEnviarName = excelFinalName + "." + compressExtension;
        }
        else {
          throw new SIBBACBusinessException("INCIDENCIA- Metodo de compresion no valido: " + compressExtension);
        }
      }
      else {
        pendienteEnviarName = excelFinalName;
      }

      LOG.debug("[crearExcelFromTmpFiles] creando fichero: {}", excelFinalName);

      Path excelTmpFile = Paths.get(tmpPath.toString(), excelFinalName);
      LOG.debug("[crearExcelFromTmpFiles] tmp File: {}", excelTmpFile.toString());

      Path excelTmpPendienteEnviarFile = Paths.get(tmpPath.toString(), pendienteEnviarName);
      LOG.debug("[crearExcelFromTmpFiles] tmp File: {}", excelTmpPendienteEnviarFile.toString());
      Path excelPendienteEnviarFile = Paths.get(pendientesPath.toString(), pendienteEnviarName);
      LOG.debug("[crearExcelFromTmpFiles] pdt enviar File: {}", excelFinalName.toString());

      if (Files.exists(excelTmpFile)) {
        throw new SIBBACBusinessException("INCIDENCIA- Fichero: " + excelTmpFile.toString() + " existe en: "
            + tmpPath.toString());
      }

      if (Files.exists(excelTmpPendienteEnviarFile)) {
        throw new SIBBACBusinessException("INCIDENCIA- Fichero: " + excelTmpPendienteEnviarFile.toString()
            + " existe en: " + tmpPath.toString());
      }

      if (Files.exists(excelPendienteEnviarFile)) {
        throw new SIBBACBusinessException("INCIDENCIA- Fichero: " + excelPendienteEnviarFile.toString()
            + " existe en: " + pendientesPath.toString());
      }

      LOG.debug("[crearExcelFromTmpFiles] Abriendo Output stream: {} ...", excelTmpFile.toString());

      excelWriter = new EuroCcpOwnershipReferenceStaticDataItemWriter(principalFileRecord.getFileName(), fileCharset);

      try (Workbook workbook = excelWriter.initWorkbook(headerTexts);
          OutputStream os = Files
              .newOutputStream(excelTmpFile, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);) {

        LOG.debug("[crearExcelFromTmpFiles] Inicializando line Mapper...");
        DefaultLineMapper<EuroCcpOwnershipReferenceStaticDataDTO> ficherosTxtTitulareslineMapper = new DefaultLineMapper<EuroCcpOwnershipReferenceStaticDataDTO>();
        EuroCcpOwnershipReferenceStaticDataFieldSetMapper mapper = new EuroCcpOwnershipReferenceStaticDataFieldSetMapper();
        ficherosTxtTitulareslineMapper.setFieldSetMapper(mapper);
        FixedLengthTokenizer tokenizer = new FixedLengthTokenizer();
        tokenizer.setStrict(true);
        tokenizer.setColumns(mapper.getColumnsRanges());
        tokenizer.setNames(mapper.getFieldNames());
        ficherosTxtTitulareslineMapper.setLineTokenizer(tokenizer);

        LOG.debug("[crearExcelFromTmpFiles] Inicializando excel writer...");

        for (EuroCcpFileSent euroCcpFileSentTxt : ficherosTmp) {
          file = Paths.get(euroCcpFileSentTxt.getFilePath());
          if (Files.notExists(file)) {
            throw new SIBBACBusinessException("INCIDENCIA- Fichero no encontrado: " + file.toString());
          }
          lineNumber = 0;
          try (InputStream is = Files.newInputStream(file, StandardOpenOption.READ);
              InputStreamReader isr = new InputStreamReader(is, fileCharset);
              BufferedReader br = new BufferedReader(isr);) {
            while ((line = br.readLine()) != null) {
              lineNumber++;

              LOG.debug("[crearExcelFromTmpFiles] file: {} lineNumber: {} line: {}", file.toString(), lineNumber, line);
              try {
                LOG.debug("[crearExcelFromTmpFiles] mapeando linea desde tmp file...");
                titular = ficherosTxtTitulareslineMapper.mapLine(line, lineNumber);
              }
              catch (Exception e) {
                throw new SIBBACBusinessException(
                    "INCIDENCIA- Parseando lineNumber: " + lineNumber + " linea: " + line, e);
              }
              if (titular != null) {
                LOG.debug("[crearExcelFromTmpFiles] escribiendo titular: {} ...", titular);
                titulares.add(titular);
                try {
                  excelWriter.write(titulares);
                }
                catch (Exception e) {
                  throw new SIBBACBusinessException("INCIDENCIA- Escribiendo el titular en excel, fichero: "
                      + file.toString() + " lineNumber: " + lineNumber + " linea: " + line, e);
                }
                titulares.clear();
              }
            }// fin while lines

          }
          catch (Exception e) {
            throw new SIBBACBusinessException(e);
          }
          LOG.debug("[crearExcelFromTmpFiles] Cambiando estadoProcesado a : {} ...",
              EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
          euroCcpFileSentTxt.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
          euroCcpFileSentTxt.setAuditDate(new Date());
          euroCcpFileSentTxt.setAuditUser("EURO_CCP_E");
          euroCcpFileSentTxt = euroCcpFileSentDao.save(euroCcpFileSentTxt);
        }// fin while files
        workbook.write(os);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e);
      }

      this.comprimirPrincipalFileAndMoverPdteEnviar(principalFileRecord, excelTmpFile, excelTmpPendienteEnviarFile,
          excelPendienteEnviarFile, compress, compressExtension, fileCharset);

      LOG.trace("[crearExcelAndSentMail] comprimir tmps...");
      this.comprimirTemporalesAndMoverTratados(principalFileRecord, ficherosTmp, tratadosPath, fileCharset);

    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.business.comunicaciones.euroccp.titulares.bo.
   * EuroCcpAbstractFileGenerationBo#processBeans(java.nio.file. Path,
   * java.lang.String, java.util.Date, java.util.List, java.util.Map,
   * java.util.Map, java.util.Map, java.nio.charset.Charset, int)
   */
  @Override
  protected void processBeansAndWriteTmpFile(Path tmpPath, String tmpFileName, Date sentDate, List<EuroCcpIdDTO> beans,
      Map<String, FlatFileItemWriter<EuroCcpOwnershipReferenceStaticDataDTO>> mapFicherosByClientNumber,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosPrincipal,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosTmp, int lastClientNumberPositions, Charset charset)
      throws SIBBACBusinessException {

    String msg;
    String clientNumber;
    Tmct0enviotitularidad ti;
    String auditUser = "SIBBAC20";

    EuroCcpFileSent excelFileSent = null;
    EuroCcpFileSent txtTmpFileSent = null;

    EuroCcpOwnershipReferenceStaticData registroBbddHb;
    EuroCcpOwnershipReferenceStaticDataDTO registroPdteEnviarEnExcelDto;

    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
    List<Tmct0afi> afisEnviar = new ArrayList<Tmct0afi>();
    List<Tmct0afi> afisActualizar = new ArrayList<Tmct0afi>();
    List<EuroCcpOwnershipReferenceStaticDataDTO> titulares = new ArrayList<EuroCcpOwnershipReferenceStaticDataDTO>();

    EuroCcpFileType fileType = euroCcpFileTypeDao.findByCdCodigo(getTmpFileType().name());

    FlatFileItemWriter<EuroCcpOwnershipReferenceStaticDataDTO> writer = null;

    boolean enviadoOrs = false;

    Map<String, String> conversionesTxt = conversionesBo.getEuroccpTxtConversion();

    for (EuroCcpIdDTO euroCcpIdDTO : beans) {
      LOG.trace("[crearFicheroTxtExcelGeneracionTitulares] procesando bean: {}", euroCcpIdDTO);
      alcs.clear();
      afisActualizar.clear();
      afisEnviar.clear();
      titulares.clear();
      enviadoOrs = false;
      LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] buscando reg envio titularidad...");
      ti = envioTitularidadDao.findOne(euroCcpIdDTO.getId());
      if (ti != null) {
        clientNumber = ti.getCdmiembromkt();
        excelFileSent = mapRegistrosFicherosEnviadosPrincipal.get(clientNumber);
        if (excelFileSent == null) {
          msg = MessageFormat.format(
              "INCIDENCIA- Reg excel file sent no disponible para el envio. ClientNumber: {0} Date: {1} type: {2}",
              clientNumber, sentDate, EuroCcpFileTypeEnum.ORS);
          throw new SIBBACBusinessException(msg);
        }
        LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] buscando alc's y afis del titular: {}...", ti);
        this.recuperarDatosGeneracionTxt(ti, alcs, afisEnviar, afisActualizar);

        if (CollectionUtils.isNotEmpty(alcs) && CollectionUtils.isNotEmpty(afisEnviar)) {

          txtTmpFileSent = mapRegistrosFicherosEnviadosTmp.get(clientNumber);
          for (Tmct0afi afiEnviar : afisEnviar) {
            LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Enviando titular ref. tit: {} tiutlar: {}", ti
                .getTmct0referenciatitular().getCdreftitularpti(), afiEnviar);

            registroPdteEnviarEnExcelDto = mapTmct0afiIntoEuroCcpExcelTitularDTO(ti, afiEnviar, conversionesTxt);

            registroBbddHb = euroCcpOwnershipReferenceStaticDataBo
                .findByOwnerReferenceAndIdentificationCodeAndEstadoProcesadoGte(
                    registroPdteEnviarEnExcelDto.getOwnerReference(),
                    registroPdteEnviarEnExcelDto.getIdentificationCode(),
                    EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());

            if (registroBbddHb != null && equalsRegistrosEnviados(registroBbddHb, registroPdteEnviarEnExcelDto)) {
              // no se tiene que enviar por que esta enviado
              registroPdteEnviarEnExcelDto = null;
              LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Marcando registro de titular como Aceptado {}...",
                  ti);
              ti.setEstado('A');
            }
            else { // reg null o no igual
              // al ser un registro comun a muchas ordenes la creacion debe ser
              // syncronized
              LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] La creacion de un nuevo registro ors es syncronized...");

              registroBbddHb = euroCcpOwnershipReferenceStaticDataBo
                  .findByOwnerReferenceAndIdentificationCodeAndEstadoProcesadoGte(
                      registroPdteEnviarEnExcelDto.getOwnerReference(),
                      registroPdteEnviarEnExcelDto.getIdentificationCode(),
                      EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());

              if (registroBbddHb == null) {
                LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] generamos el registro que va a ir a bbdd, el titular no fue enviado...");
              }
              else if (equalsRegistrosEnviados(registroBbddHb, registroPdteEnviarEnExcelDto)) {
                LOG.debug(
                    "[crearFicheroTxtExcelGeneracionTitulares] Marcando registro de titular como enviandose {}, el titular ya fue enviado pero se vuelve a enviar...",
                    ti);
              }

              if (registroPdteEnviarEnExcelDto != null) {
                if ((writer = mapFicherosByClientNumber.get(clientNumber)) == null) {
                  this.putNewWriterAndEuroccpFileSentInMaps(clientNumber, tmpPath, tmpFileName, beans.get(0).getId(),
                      mapFicherosByClientNumber, mapRegistrosFicherosEnviadosTmp, excelFileSent, fileType, sentDate,
                      lastClientNumberPositions, charset);
                }

                if (registroBbddHb != null) {
                  LOG.info("[crearFicheroTxtExcelGeneracionTitulares]Procedemos a cancelar el registro antiguo...");
                  registroBbddHb.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO
                      .getId());
                  registroBbddHb.setAuditDate(new Date());
                  registroBbddHb.setAuditUser(auditUser);
                  registroBbddHb = euroCcpOwnershipReferenceStaticDataBo.save(registroBbddHb);
                }

                writer = mapFicherosByClientNumber.get(clientNumber);
                txtTmpFileSent = mapRegistrosFicherosEnviadosTmp.get(clientNumber);

                registroBbddHb = euroCcpOwnershipReferenceStaticDataBo.saveNew(txtTmpFileSent,
                    registroPdteEnviarEnExcelDto, birthDateFormat);
                LOG.info("[crearFicheroTxtExcelGeneracionTitulares] El registro ya ha sido enviado con anterioridad y es igual.");

                titulares.add(registroPdteEnviarEnExcelDto);
                try {
                  LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Excribiendo titular en fichero txt {}...",
                      registroPdteEnviarEnExcelDto);
                  writer.write(titulares);
                  enviadoOrs = true;
                }
                catch (Exception e) {
                  throw new SIBBACBusinessException("INCIDENCIA- Escribiendo fichero.", e);
                }

                titulares.clear();
                registroBbddHb
                    .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
                registroBbddHb.setFileSent(txtTmpFileSent);
                registroBbddHb.setAuditDate(new Date());
                registroBbddHb.setAuditUser(auditUser);
                registroBbddHb = euroCcpOwnershipReferenceStaticDataBo.save(registroBbddHb);
                LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Marcando registro de titular como enviado {}...",
                    ti);
                ti.setEstado('E');
              }
            }
          }

          LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Marcando afi's de titular como enviado...");
          for (Tmct0afi afi : afisActualizar) {
            LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Marcando afi enviado {}...", afi);
            afi.setCdreferenciatitular(ti.getTmct0referenciatitular().getCdreftitularpti());
            afi.setEnviadopti('S');
            afi.setFhdenvio(new Date());
            afi.setFhaudit(new Date());
            afi.setCdusuaud(auditUser);
          }
          afiDao.save(afisActualizar);
          for (Tmct0afi afiEnviar : afisEnviar) {
            if (enviadoOrs) {
              msg = MessageFormat.format("Generado Excel titular Euro Ccp. (ORS). Ref.Tit: {0} - Holder: {1}", ti
                  .getTmct0referenciatitular().getCdreftitularpti(), afiEnviar.getCdholder());
            }
            else {
              msg = MessageFormat.format(
                  "Excel ya fue enviado con anterioridad titular Euro Ccp. (ORS). Ref.Tit: {0} - Holder: {1}", ti
                      .getTmct0referenciatitular().getCdreftitularpti(), afiEnviar.getCdholder());
            }
            LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Escribiendo tmct0log's...");
            for (Tmct0alc alc : alcs) {
              LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] Escribiendo tmct0log alc {}...", alc);
              logBo.insertarRegistro(alc, new Date(), new Date(), "", alc.getEstadotit(), msg);
            }
            LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] insertando file sent line...");
          }

          ti.setAuditFechaCambio(new Date());
          ti.setAuditUser(auditUser);
          ti = envioTitularidadDao.save(ti);
        }
        else {
          LOG.warn("[crearFicheroTxtExcelGeneracionTitulares] No encontrados afis que enviar para el reg titular: {}",
              ti);
        }

      }

    }// fin for

  }

  private void putNewWriterAndEuroccpFileSentInMaps(String clientNumber, Path tmpPath, String tmpFileName, long seq,
      Map<String, FlatFileItemWriter<EuroCcpOwnershipReferenceStaticDataDTO>> mapFicherosByClientNumber,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosTmp, EuroCcpFileSent excelFileSent,
      EuroCcpFileType fileType, Date sentDate, int lastClientNumberPositions, Charset charset)
      throws SIBBACBusinessException {
    String baseName = FilenameUtils.getBaseName(tmpFileName);
    String extension = FilenameUtils.getExtension(tmpFileName);
    String fileName = baseName + clientNumber.substring(clientNumber.length() - lastClientNumberPositions) + "_" + seq
        + "_" + FormatDataUtils.convertDateToConcurrentFileName(new Date()) + "." + extension;
    Path file = Paths.get(tmpPath.toString(), fileName);
    if (Files.exists(file)) {
      throw new SIBBACBusinessException("INCIDENCIA- Fichero ya existe: " + file.toString());
    }
    LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] init writer file: {}", file.toString());
    FlatFileItemWriter<EuroCcpOwnershipReferenceStaticDataDTO> writer = getNewWriter(file, charset,
        new EuroCcpOwnershipReferenceStaticDataFieldSetMapper());

    mapFicherosByClientNumber.put(clientNumber, writer);

    LOG.debug("[crearFicheroTxtExcelGeneracionTitulares] insert tmp file reg...");
    EuroCcpFileSent txtTmpFileSent = euroCcpFileSentBo.insertTxtTmpFileSent(excelFileSent, fileType, file, sentDate);
    mapRegistrosFicherosEnviadosTmp.put(clientNumber, txtTmpFileSent);

  }

  private boolean equalsRegistrosEnviados(EuroCcpOwnershipReferenceStaticData enviado,
      EuroCcpOwnershipReferenceStaticDataDTO pdte) {
    SimpleDateFormat sdf = new SimpleDateFormat(this.birthDateFormat);
    boolean equals = enviado.getOwnerReference().equals(pdte.getOwnerReference())
        && enviado.getAddress().equals(pdte.getAddress())
        && enviado.getCity().equals(pdte.getCity())
        && enviado.getCountryResidence().equals(pdte.getCountryResidence())
        && enviado.getFirstSurname().equals(pdte.getFirstSurname())
        && enviado.getIdentificationCode().equals(pdte.getIdentificationCode())
        && enviado.getNationalForeignInd() == pdte.getNationalForeignInd()
        && enviado.getNationality().equals(pdte.getNationality())
        && enviado.getNaturalLegalInd() == pdte.getNaturalLegalInd()
        && enviado.getOwnerName().equals(pdte.getOwnerName())
        && enviado.getOwnerType() == pdte.getOwnerType()
        && enviado.getPostalCode().equals(pdte.getPostalCode())
        && enviado.getSecondName().equals(pdte.getSecondName())
        && enviado.getTypeIdentificacion() == pdte.getTypeIdentificacion()
        && (enviado.getBirthDate() == null && pdte.getBirthDate().isEmpty() || enviado.getBirthDate() != null
            && sdf.format(enviado.getBirthDate()).equals(pdte.getBirthDate()));
    return equals;

  }

  @Override
  public void updateEstadoProcesado(EuroCcpFileSent principalFile, int estadoProcesado, String auditUser) {
    // TODO Auto-generated method stub

  }

}
