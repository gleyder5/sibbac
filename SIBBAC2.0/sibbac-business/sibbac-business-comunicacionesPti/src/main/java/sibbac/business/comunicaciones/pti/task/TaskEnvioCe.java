package sibbac.business.comunicaciones.pti.task;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.comunicaciones.pti.ce.bo.EnvioCeCallable;
import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dao.Tmct0operacioncdeccDao;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessagesBo;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageVersion;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class TaskEnvioCe extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEnvioCe.class);

  @Value("${jms.pti.queue.online.output.queue}")
  private String destinationSt;

  @Value("${jms.pti.qmanager.name}")
  private String ptiJndiName;

  @Value("${jms.pti.qmanager.host}")
  private String ptiMqHost;

  @Value("${jms.pti.qmanager.port}")
  private int ptiMqPort;

  @Value("${jms.pti.qmanager.channel}")
  private String ptiMqChannel;

  @Value("${jms.pti.qmanager.qmgrname}")
  private String ptiMqManagerName;

  @Autowired
  private Tmct0operacioncdeccDao opDao;

  @Autowired
  private PtiCommomns ptiCommons;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsMessageBo;

  @Override
  public void executeTask() throws Exception {

    LOG.info("[executeTask]inicio.");

    if (PTIMessageFactory.getCredentials() == null) {
      LOG.info("[executeTask]inicializando pti credentials...");
      ptiCommons.initPTICredentials();
    }
    LOG.trace("[executeTask]inicializando xas cfg config...");
    try (Tmct0xasAndTmct0cfgConfig config = ctx.getBean(Tmct0xasAndTmct0cfgConfig.class)) {
      LOG.trace("[executeTask]buscando dias activacion proceso ce...");
      List<Integer> activationDaysCfg = config.findDiasActivacionProcesos();

      Calendar now = Calendar.getInstance();

      if (!activationDaysCfg.contains(now.get(Calendar.DAY_OF_WEEK))) {
        LOG.warn("[TaskCreacionMosAsignacion :: executeTask] No es un dia de activacion del proceso.");
        return;
      }

      if (!tmct0menBo.isSessionOpened(false)) {
        LOG.warn("[TaskCreacionMosAsignacion :: executeTask] LA SESION PTI ESTA CERRADA.");
        return;
      }
      LOG.trace("[executeTask]recuperando conection factory...");
      ConnectionFactory cf = getConnectionFactory();
      if (StringUtils.isBlank(destinationSt) || cf == null) {
        throw new SIBBACBusinessException("Conexion para enviar el Ce no esta configurada.");
      }

      this.enviarCes(cf, config);
    }
    LOG.info("[executeTask]fin.");
  }

  private void enviarCes(ConnectionFactory cf, Tmct0xasAndTmct0cfgConfig config) throws SIBBACBusinessException {
    LOG.trace("[enviarCes]inicio...");
    Calendar sinceDateCalendar;
    BigDecimal sinceDateBd;
    Integer daysEnvioCe = config.getDaysEnvioCe();

    Date fechaContratacion;
    try {
      fechaContratacion = DateHelper.getPreviousWorkDate(new Date(), daysEnvioCe, config.getWorkDaysOfWeek(),
          config.getHolidays());
    }
    catch (ParseException e1) {
      throw new SIBBACBusinessException("No se ha podido obtener la configuracion" + e1.getMessage(), e1);
    }

    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));

    do {
      this.enviarCes(cf, fechaContratacion, config);
      sinceDateCalendar = Calendar.getInstance();
      sinceDateCalendar.setTime(fechaContratacion);
      sinceDateCalendar.add(Calendar.DAY_OF_YEAR, 1);
      fechaContratacion = sinceDateCalendar.getTime();
      sinceDateBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(fechaContratacion));
    }
    while (sinceDateBd.compareTo(now) <= 0);
    LOG.trace("[enviarCes]fin...");
  }

  private void enviarCes(ConnectionFactory cf, Date fecha, Tmct0xasAndTmct0cfgConfig config)
      throws SIBBACBusinessException {
    EnvioCeCallable callable;
    List<String> listOperaciones = opDao.findAllCdoperacioneccForEnvioCe(fecha, config.getIdPti());

    if (!listOperaciones.isEmpty()) {
      List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat("Thread-EnvioCe-%d");
      ExecutorService executor = Executors.newFixedThreadPool(5, tfb.build());

      PTIMessageVersion version = config.getPtiMessageVersionFromTmct0cfg(new Date());
      try {
        for (String op : listOperaciones) {
          callable = ctx.getBean(EnvioCeCallable.class, cf, destinationSt, op, version, config);
          listFutures.add(executor.submit(callable));
        }
        executor.shutdown();
      }
      catch (Exception e) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
        executor.shutdownNow();
      }
      finally {

      }

      try {
        executor.awaitTermination((long) 1 * listFutures.size() + 30, TimeUnit.MINUTES);

        for (Future<Void> future : listFutures) {
          future.get(2, TimeUnit.SECONDS);
        }
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (ExecutionException e) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      catch (TimeoutException e) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
      finally {
        listFutures.clear();
        listOperaciones.clear();
      }
    }
  }

  protected ConnectionFactory getConnectionFactory() throws SIBBACBusinessException {
    LOG.trace("[getConnectionFactory] buscando conexion jms con pti...");
    ConnectionFactory cf;
    try {
      cf = jmsMessageBo.getConnectionFactoryFromContext(ptiJndiName);
    }
    catch (NamingException e) {
      try {
        cf = jmsMessageBo.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
      }
      catch (JMSException e1) {
        throw new SIBBACBusinessException("Imposible recuperar conexion con camara nacional.", e1);
      }
    }
    return cf;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_APUNTE.TASK_ENVIO_CE;
  }

}
