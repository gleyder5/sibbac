package sibbac.business.desglose.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0alc;

public class DesgloseManualDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6446845944464890336L;

  private BigDecimal titulos;
  private BigDecimal precioFrom;
  private String clearingMemberCodeFrom;
  private String giveUpReferenceFrom;
  private String clearingAccountCodeFrom;

  private boolean uniqueCustomerReference = false;
  private Character tipoEnvioComision;
  private BigDecimal bankComissionPercent = BigDecimal.ZERO;
  private BigDecimal bankComission = BigDecimal.ZERO;
  private BigDecimal payerComissionPercent = BigDecimal.ZERO;
  private BigDecimal payerComission = BigDecimal.ZERO;

  private BigInteger clearingRuleTo;
  private String clearingRuleDescriptionTo;
  private Long clearingMemberTo;
  private String clearingMemberCodeTo;
  private String clearingMemberDescriptionTo;
  private String giveUpReferenceTo;
  private Long clearingAccountTo;
  private String clearingAccountCodeTo;

  private String bankReference;
  private String accountAtClearer;

  private SettlementData settlementData;

  private AlgoritmoDesgloseManual algoritmoRepartoDesglose;

  private List<DesgloseManualEjecucionDTO> repartoEjecuciones;

  private Tmct0alc tmct0alcCreado;

  public DesgloseManualDTO() {
    this.repartoEjecuciones = new ArrayList<DesgloseManualEjecucionDTO>();
  }

  public DesgloseManualDTO(DesgloseManualDTO other) {
    this();
    titulos = other.getTitulos();

    uniqueCustomerReference = other.isUniqueCustomerReference();
    tipoEnvioComision = other.getTipoEnvioComision();
    bankComissionPercent = other.getBankComissionPercent();
    bankComission = other.getBankComission();
    payerComission = other.getPayerComission();
    payerComissionPercent = other.getPayerComissionPercent();
    clearingRuleTo = other.getClearingRuleTo();
    clearingRuleDescriptionTo = other.getClearingRuleDescriptionTo();
    clearingMemberTo = other.getClearingMemberTo();
    clearingMemberCodeTo = other.getClearingMemberCodeTo();
    clearingMemberDescriptionTo = other.getClearingMemberDescriptionTo();
    clearingAccountTo = other.getClearingAccountTo();
    clearingAccountCodeTo = other.getClearingAccountCodeTo();

    // Datos para buscar los desgloses a hacer
    precioFrom = other.getPrecioFrom();
    clearingAccountCodeFrom = other.getClearingAccountCodeFrom();
    clearingMemberCodeFrom = other.getClearingMemberCodeFrom();
    giveUpReferenceFrom = other.getGiveUpReferenceFrom();

    settlementData = new SettlementData(other.getSettlementData());
    algoritmoRepartoDesglose = other.getAlgoritmoRepartoDesglose();
    repartoEjecuciones.addAll(other.getRepartoEjecuciones());

  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public BigDecimal getPrecioFrom() {
    return precioFrom;
  }

  public String getClearingMemberCodeFrom() {
    return clearingMemberCodeFrom;
  }

  public String getGiveUpReferenceFrom() {
    return giveUpReferenceFrom;
  }

  public String getClearingAccountCodeFrom() {
    return clearingAccountCodeFrom;
  }

  public boolean isUniqueCustomerReference() {
    return uniqueCustomerReference;
  }

  public Character getTipoEnvioComision() {
    return tipoEnvioComision;
  }

  public BigDecimal getBankComissionPercent() {
    return bankComissionPercent;
  }

  public BigDecimal getBankComission() {
    return bankComission;
  }

  public BigDecimal getPayerComissionPercent() {
    return payerComissionPercent;
  }

  public BigDecimal getPayerComission() {
    return payerComission;
  }

  public BigInteger getClearingRuleTo() {
    return clearingRuleTo;
  }

  public String getClearingRuleDescriptionTo() {
    return clearingRuleDescriptionTo;
  }

  public Long getClearingMemberTo() {
    return clearingMemberTo;
  }

  public String getClearingMemberCodeTo() {
    return clearingMemberCodeTo;
  }

  public String getClearingMemberDescriptionTo() {
    return clearingMemberDescriptionTo;
  }

  public String getGiveUpReferenceTo() {
    return giveUpReferenceTo;
  }

  public Long getClearingAccountTo() {
    return clearingAccountTo;
  }

  public String getClearingAccountCodeTo() {
    return clearingAccountCodeTo;
  }

  public SettlementData getSettlementData() {
    return settlementData;
  }

  public AlgoritmoDesgloseManual getAlgoritmoRepartoDesglose() {
    return algoritmoRepartoDesglose;
  }

  public List<DesgloseManualEjecucionDTO> getRepartoEjecuciones() {
    return repartoEjecuciones;
  }

  public Tmct0alc getTmct0alcCreado() {
    return tmct0alcCreado;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setPrecioFrom(BigDecimal precioFrom) {
    this.precioFrom = precioFrom;
  }

  public void setClearingMemberCodeFrom(String clearingMemberCodeFrom) {
    this.clearingMemberCodeFrom = clearingMemberCodeFrom;
  }

  public void setGiveUpReferenceFrom(String giveUpReferenceFrom) {
    this.giveUpReferenceFrom = giveUpReferenceFrom;
  }

  public void setClearingAccountCodeFrom(String clearingAccountCodeFrom) {
    this.clearingAccountCodeFrom = clearingAccountCodeFrom;
  }

  public void setUniqueCustomerReference(boolean uniqueCustomerReference) {
    this.uniqueCustomerReference = uniqueCustomerReference;
  }

  public void setTipoEnvioComision(Character tipoEnvioComision) {
    this.tipoEnvioComision = tipoEnvioComision;
  }

  public void setBankComissionPercent(BigDecimal bankComissionPercent) {
    this.bankComissionPercent = bankComissionPercent;
  }

  public void setBankComission(BigDecimal bankComission) {
    this.bankComission = bankComission;
  }

  public void setPayerComissionPercent(BigDecimal payerComissionPercent) {
    this.payerComissionPercent = payerComissionPercent;
  }

  public void setPayerComission(BigDecimal payerComission) {
    this.payerComission = payerComission;
  }

  public void setClearingRuleTo(BigInteger clearingRuleTo) {
    this.clearingRuleTo = clearingRuleTo;
  }

  public void setClearingRuleDescriptionTo(String clearingRuleDescriptionTo) {
    this.clearingRuleDescriptionTo = clearingRuleDescriptionTo;
  }

  public void setClearingMemberTo(Long clearingMemberTo) {
    this.clearingMemberTo = clearingMemberTo;
  }

  public void setClearingMemberCodeTo(String clearingMemberCodeTo) {
    this.clearingMemberCodeTo = clearingMemberCodeTo;
  }

  public void setClearingMemberDescriptionTo(String clearingMemberDescriptionTo) {
    this.clearingMemberDescriptionTo = clearingMemberDescriptionTo;
  }

  public void setGiveUpReferenceTo(String giveUpReferenceTo) {
    this.giveUpReferenceTo = giveUpReferenceTo;
  }

  public void setClearingAccountTo(Long clearingAccountTo) {
    this.clearingAccountTo = clearingAccountTo;
  }

  public void setClearingAccountCodeTo(String clearingAccountCodeTo) {
    this.clearingAccountCodeTo = clearingAccountCodeTo;
  }

  public String getBankReference() {
    return bankReference;
  }

  public String getAccountAtClearer() {
    return accountAtClearer;
  }

  public void setBankReference(String bankReference) {
    this.bankReference = bankReference;
  }

  public void setAccountAtClearer(String accountAtClearer) {
    this.accountAtClearer = accountAtClearer;
  }

  public void setSettlementData(SettlementData settlementData) {
    this.settlementData = settlementData;
  }

  public void setAlgoritmoRepartoDesglose(AlgoritmoDesgloseManual algoritmoRepartoDesglose) {
    this.algoritmoRepartoDesglose = algoritmoRepartoDesglose;
  }

  public void setRepartoEjecuciones(List<DesgloseManualEjecucionDTO> repartoEjecuciones) {
    this.repartoEjecuciones = repartoEjecuciones;
  }

  public void setTmct0alcCreado(Tmct0alc tmct0alcCreado) {
    this.tmct0alcCreado = tmct0alcCreado;
  }

  public boolean isTerminado() {
    return titulos != null && getTitulosRepartidos().compareTo(this.titulos) >= 0;
  }

  public BigDecimal getTitulosRepartidos() {
    BigDecimal titulosRepartidos = BigDecimal.ZERO;
    if (repartoEjecuciones != null)
      for (DesgloseManualEjecucionDTO desgloseManualEjecucionDTO : repartoEjecuciones) {
        titulosRepartidos = titulosRepartidos.add(desgloseManualEjecucionDTO.getImtitulos());
      }
    return titulosRepartidos;
  }

}
