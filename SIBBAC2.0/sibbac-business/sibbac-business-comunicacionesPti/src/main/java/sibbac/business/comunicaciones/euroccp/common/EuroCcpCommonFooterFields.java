package sibbac.business.comunicaciones.euroccp.common;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpCommonFooterFields implements WrapperFileReaderFieldEnumInterface {

  CLIENT_NUMBER("clientNumber", 4, 0, WrapperFileReaderFieldType.STRING), 
  TIME("sendDate", 14, 0, WrapperFileReaderFieldType.TIMESTAMP), 
  RECORD_COUNT("recordCount", 10, 0, WrapperFileReaderFieldType.NUMERIC), 
  FILLER("filler", 229, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpCommonFooterFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
