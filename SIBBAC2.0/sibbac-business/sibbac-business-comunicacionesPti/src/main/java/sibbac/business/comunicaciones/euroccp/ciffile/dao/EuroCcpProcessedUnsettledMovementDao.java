package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;

@Component
public interface EuroCcpProcessedUnsettledMovementDao extends JpaRepository<EuroCcpProcessedUnsettledMovement, Long> {

  @Query("SELECT DISTINCT m.settlementDate FROM EuroCcpProcessedUnsettledMovement m WHERE m.estadoProcesado= :pEstadoProcesado ORDER BY m.settlementDate ASC ")
  List<Date> findDistinctSettlementDateByEstadoProcesado(@Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT DISTINCT m.transactionDate FROM EuroCcpProcessedUnsettledMovement m WHERE m.estadoProcesado= :pEstadoProcesado ORDER BY m.transactionDate ASC ")
  List<Date> findDistinctTransactionDateByEstadoProcesado(@Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT new sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO( m.id) FROM EuroCcpProcessedUnsettledMovement m WHERE m.settlementDate = :pFechaLiq AND m.estadoProcesado= :pEstadoProcesado")
  List<EuroCcpIdDTO> findIdByEstadoProcesadoAndSettlementDate(@Param("pEstadoProcesado") int estadoProcesado,
      @Param("pFechaLiq") Date fechaLiq, Pageable page);

  @Query("SELECT new sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO( m.id) FROM EuroCcpProcessedUnsettledMovement m WHERE m.transactionDate = :ptransactionDate AND m.estadoProcesado= :pEstadoProcesado")
  List<EuroCcpIdDTO> findIdByEstadoProcesadoAndTransactionDate(@Param("pEstadoProcesado") int estadoProcesado,
      @Param("ptransactionDate") Date transactionDate, Pageable page);

  @Query("SELECT m FROM EuroCcpProcessedUnsettledMovement m WHERE m.unsettledReference = :unsettledReference and (m.movementCode ='01' or m.movementCode = '05' or m.movementCode = '06') "
      + " AND m.estadoProcesado > :pEstadoProcesado ORDER BY m.id DESC ")
  List<EuroCcpProcessedUnsettledMovement> findByUnsettledReferenceTradeOrTransferOrBalance(
      @Param("unsettledReference") String unsettledReference, @Param("pEstadoProcesado") int estadoProcesadoGt);

  @Query("SELECT max(m.id) FROM EuroCcpProcessedUnsettledMovement m WHERE m.id < :id ")
  Long getMaxIdByIdLt(@Param("id") long id);

  @Query("SELECT m FROM EuroCcpProcessedUnsettledMovement m WHERE m.transactionDate = :ptransactionDate AND m.estadoProcesado > :pEstadoProcesado "
      + " AND m.exchangeCodeTrade = :pexchangeCodeTrade AND m.externalTransactionIdExchange = :pexternalTransactionIdExchange "
      + " AND m.movementCode = :pmovementCode ORDER BY m.id DESC ")
  List<EuroCcpProcessedUnsettledMovement> findAllByTransactionDateAndEstadoProcesadoGtAndExchangeCodeTradeAndExternalTransactionIdExchangeAndMovementCode(
      @Param("ptransactionDate") Date transactionDate, @Param("pEstadoProcesado") int estadoProcesadoGt,
      @Param("pexchangeCodeTrade") String exchangeCodeTrade,
      @Param("pexternalTransactionIdExchange") String externalTransactionIdExchange,
      @Param("pmovementCode") String movementCode);

  @Query("SELECT m FROM EuroCcpProcessedUnsettledMovement m WHERE m.transactionDate = :ptransactionDate AND m.estadoProcesado > :pEstadoProcesado "
      + " AND m.exchangeCodeTrade = :pexchangeCodeTrade AND m.externalTransactionIdExchange = :pexternalTransactionIdExchange "
      + " AND m.movementCode = :pmovementCode AND (m.id < :pid1 OR m.id <= :pid2) ORDER BY m.id DESC ")
  List<EuroCcpProcessedUnsettledMovement> findAllByTransactionDateAndEstadoProcesadoGtAndExchangeCodeTradeAndExternalTransactionIdExchangeAndMovementCodeAndIdLt(
      @Param("ptransactionDate") Date transactionDate, @Param("pEstadoProcesado") int estadoProcesadoGt,
      @Param("pexchangeCodeTrade") String exchangeCodeTrade,
      @Param("pexternalTransactionIdExchange") String externalTransactionIdExchange,
      @Param("pmovementCode") String movementCode, @Param("pid1") long idLt1, @Param("pid2") long idLt2);

}