package sibbac.business.comunicaciones.euroccp.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.realigment.bo.EuroCcpExecutionRealigmentBo;
import sibbac.business.comunicaciones.euroccp.realigment.bo.EuroCcpExecutionRealigmentFileWriterBo;
import sibbac.business.comunicaciones.euroccp.realigment.mapper.EuroCcpExecutionRealigmentFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpFileSentBo;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionsBo;
import sibbac.business.comunicaciones.euroccp.titulares.mapper.EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

public abstract class EuroCcpResponseFileProcessBo {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpResponseFileProcessBo.class);

  /**
   * Common Euroccp Paths
   * */

  @Value("${sibbac.euroccp.out.path}")
  private String workPathOutSt;

  @Value("${sibbac.euroccp.out.tmp.folder}")
  private String tmpFolder;

  @Value("${sibbac.euroccp.out.pendientes.folder}")
  private String pendientesFolder;

  @Value("${sibbac.euroccp.out.enviados.folder}")
  private String enviadosFolder;

  @Value("${sibbac.euroccp.out.tratados.folder}")
  private String tratadosFolder;

  @Value("${sibbac.euroccp.in.path}")
  private String workPathInSt;

  @Autowired
  private EuroCcpFileSentBo fileSentBo;

  @Autowired
  private EuroCcpExecutionRealigmentFileWriterBo writer;

  @Autowired
  private EuroCcpExecutionRealigmentBo executionRealigmentBo;

  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsBo euroCcpOwnershipReportingGrossExecutionsBo;

  public EuroCcpResponseFileProcessBo() {
  }

  public abstract Charset getFileCharset();

  public abstract String getLinePattern();

  public abstract String getFooterPattern();

  public abstract int getThreadSize();

  public abstract int getTransactionSize();

  public abstract String getFileDatePattern();

  public abstract String getFilePattern();

  @Transactional
  public void procesarFicherosRespuestaEuroCcp() throws SIBBACBusinessException {
    LOG.trace("[procesarFicherosRespuestaEuroCcp] inicio");
    LOG.trace("[procesarFicherosRespuestaEuroCcp] recuperando rutas de trabajo para fichero...");

    Path inWorkPath = Paths.get(workPathInSt);

    if (Files.notExists(inWorkPath)) {
      try {
        Files.createDirectories(inWorkPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("No se ha podido crear la ruta de ficheros de entrada", e);
      }
    }

    Path inProcesadosPath = Paths.get(workPathInSt, tratadosFolder);
    if (Files.notExists(inProcesadosPath)) {
      try {
        Files.createDirectories(inProcesadosPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException("No se ha podido crear la ruta de ficheros de entrada", e);
      }
    }

    LOG.info("[procesarFicherosRespuestaEuroCcp] BUSCANDO FICHEROS {} EN LA RUTA {}", getFilePattern(),
        inWorkPath.toString());
    List<Path> listPath = new ArrayList<Path>();

    try (DirectoryStream<Path> ficherosEntradaDirectoryStream = Files.newDirectoryStream(inWorkPath, getFilePattern())) {
      for (Path ficheroEntrada : ficherosEntradaDirectoryStream) {
        LOG.trace("[procesarFicherosRespuestaEuroCcp] contado ficheros, {}", ficheroEntrada.toString());
        listPath.add(ficheroEntrada);
      }
    }
    catch (IOException e) {
      throw new SIBBACBusinessException("No se ha podido buscar en la ruta de entrada", e);
    }

    if (CollectionUtils.isNotEmpty(listPath)) {

      LOG.info("[procesarFicherosRespuestaEuroCcp] ruta trabajo: {}, ruta tratados: {}", inWorkPath.toString(),
          inProcesadosPath.toString());

      try {
        Collections.sort(listPath, new Comparator<Path>() {

          @Override
          public int compare(Path arg0, Path arg1) {
            return arg0.toString().compareTo(arg1.toString());
          }
        });
      }
      catch (Exception e1) {
        LOG.warn("[procesarFicherosRespuestaEuroCcp] no se ha podido ordenar los ficheros de entrada {}",
            e1.getMessage(), e1);
      }

      for (Path ficheroEntrada : listPath) {
        this.procesarFicherosRespuestaEuroCcp(inWorkPath, inProcesadosPath, ficheroEntrada);
      }

    }
    else {
      LOG.trace("[procesarFicherosEntrada] no se han encontrado ficheros que procesar...");
    }
    LOG.trace("[procesarFicherosEntrada] fin");
  }

  private void procesarFicherosRespuestaEuroCcp(Path inWorkPath, Path inProcesadosPath, Path ficheroEntrada)
      throws SIBBACBusinessException {
    LOG.info("[procesarFicherosEntrada] procesando fichero {}", ficheroEntrada.toString());

    String originalFileName = ficheroEntrada.getFileName().toString();
    String baseName = FilenameUtils.getBaseName(originalFileName);
    String extension = FilenameUtils.getExtension(originalFileName);
    Path ficheroProcesandose = Paths.get(inWorkPath.toString(), String.format("%s.procesandose", baseName));

    try {

      this.moverFichero(ficheroEntrada, ficheroProcesandose);

      LOG.info("[procesarFicherosRespuestaEuroCcp] cambiando estado de fichero a recibiendose {} ...", originalFileName);
      EuroCcpFileSent ficheroRecibiendose = fileSentBo.updateEstadoProcesadoFicheroInFileNameNewTransaction(
          originalFileName, getFileDatePattern(), ESTADO_PROCESADO_EURO_CCP.RECIBIENDO.getId());

      if (ficheroProcesandose == null) {
        throw new SIBBACBusinessException(String.format(
            "No encontrada correspondiencia de fichero enviado con nombre: '%s'.", originalFileName));
      }
      ficheroRecibiendose = fileSentBo.findById(ficheroRecibiendose.getId());
      EuroCcpFileTypeEnum fileType = EuroCcpFileTypeEnum
          .valueOf(ficheroRecibiendose.getFileType().getCdCodigo().trim());

      LOG.trace("[procesarFicherosRespuestaEuroCcp] buscando el fichero tipo {} recibiendose {} ...", fileType,
          originalFileName);
      ficheroRecibiendose = this.getEuroEccFileSentRecibiendose(fileType);

      LOG.trace("[procesarFicherosRespuestaEuroCcp] inicializando mappeador de fichero {}...", fileType);
      LineMapper<EuroCcpCommonSendRegisterDTO> mapper = initLineMapper(fileType);

      try {
        if (ficheroEntrada.toString().toUpperCase().endsWith(".ZIP")) {
          this.procesarFicheroZip(ficheroRecibiendose, originalFileName, ficheroProcesandose, mapper);
        }
        else if (ficheroEntrada.toString().toUpperCase().endsWith(".GZ")) {
          this.procesarFicheroGzip(ficheroRecibiendose, originalFileName, ficheroProcesandose, mapper);
        }
        else {
          this.procesarFicheroTxt(ficheroRecibiendose, originalFileName, ficheroProcesandose, mapper);
        }
      }
      catch (IOException e) {
        throw new SIBBACBusinessException(String.format("Incidencia leyendo el fichero %s %s",
            ficheroEntrada.toString(), e.getMessage()), e);
      }
      Path ficheroTratado = Paths.get(inProcesadosPath.toString(),
          String.format("%s_%s.%s", baseName, FormatDataUtils.convertDateToConcurrentFileName(new Date()), extension));
      this.moverFichero(ficheroProcesandose, ficheroTratado);

      LOG.info("[procesarFicherosRespuestaEuroCcp] cambiando estado de fichero a procesado {} ...", originalFileName);
      fileSentBo.updateEstadoProcesadoFicheroInFileNameNewTransaction(ficheroRecibiendose,
          ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId(), "SIBBAC20");

    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(String.format("Incidencia procesando el fichero %s %s",
          ficheroEntrada.toString(), e.getMessage()), e);
    }
  }

  private void procesarFicheroZip(EuroCcpFileSent ficheroRecibiendose, String originalFileName,
      Path ficheroProcesandose, LineMapper<EuroCcpCommonSendRegisterDTO> mapper) throws IOException,
      SIBBACBusinessException {
    ZipEntry ze;
    EuroCcpCommonSendFooterDTO footer;
    Map<String, EuroCcpCommonSendFooterDTO> mapFooters = new HashMap<String, EuroCcpCommonSendFooterDTO>();
    try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(ficheroProcesandose, StandardOpenOption.READ),
        getFileCharset()); BufferedReader br = new BufferedReader(new InputStreamReader(zis));) {
      while ((ze = zis.getNextEntry()) != null) {
        try {
          footer = this.validateFileIn(ze.getName(), br, mapper);
          mapFooters.put(ze.getName(), footer);
        }
        catch (SIBBACBusinessException e) {
          throw e;
        }
        finally {
          zis.closeEntry();
        }
      }
    }// fin try resource

    try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(ficheroProcesandose, StandardOpenOption.READ),
        getFileCharset()); BufferedReader br = new BufferedReader(new InputStreamReader(zis));) {
      while ((ze = zis.getNextEntry()) != null) {
        try {
          footer = mapFooters.get(ze.getName());
          this.procesarFicheroEntrada(ficheroRecibiendose, originalFileName, br, mapper, footer);
        }
        catch (SIBBACBusinessException e) {
          throw e;
        }
        finally {
          zis.closeEntry();
        }
      }
    }// fin try resource
    mapFooters.clear();
  }

  private void procesarFicheroGzip(EuroCcpFileSent ficheroRecibiendose, String originalFileName,
      Path ficheroProcesandose, LineMapper<EuroCcpCommonSendRegisterDTO> mapper) throws IOException,
      SIBBACBusinessException {
    EuroCcpCommonSendFooterDTO footer;
    try (GZIPInputStream gis = new GZIPInputStream(Files.newInputStream(ficheroProcesandose, StandardOpenOption.READ));
        BufferedReader br = new BufferedReader(new InputStreamReader(gis, getFileCharset()));
        GZIPInputStream gis2 = new GZIPInputStream(Files.newInputStream(ficheroProcesandose, StandardOpenOption.READ));
        BufferedReader br2 = new BufferedReader(new InputStreamReader(gis2, getFileCharset()));) {
      footer = this.validateFileIn(originalFileName, br, mapper);
      this.procesarFicheroEntrada(ficheroRecibiendose, originalFileName, br2, mapper, footer);
    }

  }

  private void procesarFicheroTxt(EuroCcpFileSent ficheroRecibiendose, String originalFileName,
      Path ficheroProcesandose, LineMapper<EuroCcpCommonSendRegisterDTO> mapper) throws IOException,
      SIBBACBusinessException {
    EuroCcpCommonSendFooterDTO footer;
    try (BufferedReader br = Files.newBufferedReader(ficheroProcesandose, getFileCharset());
        BufferedReader br2 = Files.newBufferedReader(ficheroProcesandose, getFileCharset());) {
      footer = this.validateFileIn(originalFileName, br, mapper);
      this.procesarFicheroEntrada(ficheroRecibiendose, originalFileName, br2, mapper, footer);
    }
  }

  private void moverFichero(Path origen, Path destino) throws SIBBACBusinessException {
    try {
      LOG.info("[moverFichero] moviendo fichero de {} a {}", origen.toString(), destino.toString());
      Files.move(origen, destino, StandardCopyOption.ATOMIC_MOVE);
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format("Incidencia moviendo fichero %s a %s %s", origen.toString(),
          destino.toString(), e.getMessage()), e);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("Incidencia moviendo fichero %s a %s %s", origen.toString(),
          destino.toString(), e.getMessage()), e);
    }
  }

  private EuroCcpFileSent getEuroEccFileSentRecibiendose(EuroCcpFileTypeEnum fileType) throws SIBBACBusinessException {
    List<EuroCcpFileSent> filesSent = fileSentBo.findAllByEstadoProcesadoAndFileType(
        EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.RECIBIENDO.getId(), fileType.name());
    if (filesSent.size() <= 0) {
      throw new SIBBACBusinessException(String.format("No hay ningun fichero %s en estado recibiendose", fileType));
    }
    else if (filesSent.size() > 1) {
      throw new SIBBACBusinessException(String.format("Hay mas de un fichero %s en estado recibiendose", fileType));
    }

    return filesSent.get(0);
  }

  private EuroCcpCommonSendFooterDTO validateFileIn(String fileName, BufferedReader br,
      LineMapper<EuroCcpCommonSendRegisterDTO> mapper) throws SIBBACBusinessException {
    EuroCcpCommonSendFooterDTO footer = null;
    int recordCount = 0;
    int fileIndex = 0;
    String linea;
    EuroCcpCommonSendRegisterDTO reg;
    char caracterRaroTransmisiones = 26;

    try {

      while ((linea = br.readLine()) != null) {
        fileIndex++;
        if (!StringUtils.contains(linea, caracterRaroTransmisiones)) {
          try {
            reg = mapper.mapLine(linea, fileIndex);
            if (reg == null) {
              throw new SIBBACBusinessException(String.format(
                  "Inciencia mapeo de linea devuelve null en fichero: %s linea %s - %s ", fileName, fileIndex, linea));
            }
            else {
              if (reg.type == EuroCcpCommonSendRegisterTypeEnum.FOOTER) {
                footer = (EuroCcpCommonSendFooterDTO) reg;
                LOG.info("[validateFileIn] encontrado regitro footer ");
                if (footer.getRecordCount() != recordCount) {
                  throw new SIBBACBusinessException(String.format(
                      "Incidencia numero de registros %s no coincide con el footer %s fichero %s", recordCount,
                      footer.getRecordCount(), fileName));
                }
              }
              else if (reg.type == EuroCcpCommonSendRegisterTypeEnum.RECORD) {
                recordCount++;
              }
              else {
                LOG.warn("[validateFileIn] registro tipo {} no reconocido. linea {} - {}", reg.type, fileIndex, linea);
              }
            }
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(String.format(
                "Incidencia comprobando lineas del fichero entrada: %s linea %s - %s %s", fileName, fileIndex, linea,
                e.getMessage()), e);
          }
        }
        else {
          LOG.debug("[validateFileIn] fichero contiene caracter 26, {}", linea);
        }
      }// fin while
    }
    catch (IOException e) {
      throw new SIBBACBusinessException(String.format(
          "Incidencia contando el numero de lineas del fichero entrada: %s %s", fileName, e.getMessage()), e);
    }

    if (footer == null) {
      throw new SIBBACBusinessException(String.format(
          "Incidencia validando el fichero, no encontrada linea final del fichero (footer). %s", fileName));
    }

    LOG.debug("[validateFileIn] fichero tamaño {}", recordCount);

    return footer;
  }

  private void procesarFicheroEntrada(EuroCcpFileSent file, String fileName, BufferedReader br,
      LineMapper<EuroCcpCommonSendRegisterDTO> mapper, EuroCcpCommonSendFooterDTO footer)
      throws SIBBACBusinessException {

    String linea;
    int pos = 0;
    int posEnd = 0;
    EuroCcpCommonSendRegisterDTO reg;
    int transacctionTam = getTransactionSize();
    List<EuroCcpCommonSendRegisterDTO> registrosRealigment;

    if (footer.getRecordCount() > 0) {

      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat(String.format("Thread-EuroCcpResponseFileProcess-%s-%s", fileName, "%d"));
      ExecutorService executor = Executors.newFixedThreadPool(getThreadSize(), tfb.build());
      List<Future<Void>> listFutures = new ArrayList<Future<Void>>();

      try {
        while (pos < footer.getRecordCount()) {
          posEnd = Math.min(posEnd + transacctionTam, footer.getRecordCount());
          registrosRealigment = new ArrayList<EuroCcpCommonSendRegisterDTO>();
          for (int i = pos; i < posEnd; i++) {
            try {
              linea = br.readLine();
            }
            catch (IOException e) {
              throw new SIBBACBusinessException(String.format(
                  "Incidencia leyendo lineas del fichero ERG entrada: %s %s", fileName, e.getMessage()), e);
            }
            try {
              reg = mapper.mapLine(linea, i);
              if (reg.type == EuroCcpCommonSendRegisterTypeEnum.RECORD) {
                registrosRealigment.add(reg);
              }
              else {
                LOG.trace("[procesarFicheroEntrada] registro tipo {} no se procesa.", reg.type);
              }
            }
            catch (Exception e) {
              throw new SIBBACBusinessException(String.format(
                  "Incidencia mapeando lineas del fichero ERG entrada: %s %s %s", fileName, linea, e.getMessage()), e);
            }

          }
          if (CollectionUtils.isNotEmpty(registrosRealigment)) {
            listFutures.add(executor.submit(getNewCallableProcessFile(file, registrosRealigment)));
          }
          pos = posEnd;
        }
        executor.shutdown();
        executor.awaitTermination((long) 10 + listFutures.size() * 2, TimeUnit.MINUTES);
      }
      catch (Exception e) {
        executor.shutdownNow();
        throw new SIBBACBusinessException(e.getMessage(), e);
      }

      for (Future<Void> future : listFutures) {
        try {
          future.get(0, TimeUnit.SECONDS);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
          if (!future.isCancelled() && !future.isDone()) {
            future.cancel(true);
          }
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }

    }
    else {
      LOG.warn("[procesarFicheroEntrada] fichero {} no tiene traspasos", fileName);
    }
  }

  private Callable<Void> getNewCallableProcessFile(final EuroCcpFileSent file,
      final List<EuroCcpCommonSendRegisterDTO> beans) {
    final EuroCcpFileTypeEnum fileType = EuroCcpFileTypeEnum.valueOf(file.getFileType().getCdCodigo().trim());
    Callable<Void> c = new Callable<Void>() {

      @Override
      public Void call() throws SIBBACBusinessException {
        switch (fileType) {
          case ERG:
            executionRealigmentBo.procesarBeansErgEntrada(file, beans);
            break;
          case ORG:
          case CRG:
            euroCcpOwnershipReportingGrossExecutionsBo.procesarRecepcionTitulares(file, beans);
            break;
          default:

            break;
        }

        return null;
      }
    };
    return c;
  }

  private LineMapper<EuroCcpCommonSendRegisterDTO> initLineMapper(EuroCcpFileTypeEnum fileType) {

    switch (fileType) {
      case ERG:
        return getErgLineMapper();
      case ORG:
      case CRG:
        return getOrgCrgLineMapper();
      default:
        return null;
    }

  }

  private LineMapper<EuroCcpCommonSendRegisterDTO> getErgLineMapper() {
    RegexPatternMatchingCompositeLineMapper<EuroCcpCommonSendRegisterDTO> lineMapper = new RegexPatternMatchingCompositeLineMapper<EuroCcpCommonSendRegisterDTO>();
    Map<String, LineTokenizer> lineTokenizers = new HashMap<String, LineTokenizer>();
    EuroCcpExecutionRealigmentFieldSetMapper executionRealigmentFieldSetMapper = new EuroCcpExecutionRealigmentFieldSetMapper();
    final FixedLengthTokenizer executionRealigmentTokenizer = new FixedLengthTokenizer();
    executionRealigmentTokenizer.setNames(executionRealigmentFieldSetMapper.getFieldNames());
    executionRealigmentTokenizer.setColumns(executionRealigmentFieldSetMapper.getColumnsRanges());
    executionRealigmentTokenizer.setStrict(false);

    EuroCcpCommonSendFooterFieldSetMapper commonFooterFieldSetMapper = new EuroCcpCommonSendFooterFieldSetMapper();

    final FixedLengthTokenizer commonFooterlengthTokenizer = new FixedLengthTokenizer();
    commonFooterlengthTokenizer.setNames(commonFooterFieldSetMapper.getFieldNames());
    commonFooterlengthTokenizer.setColumns(commonFooterFieldSetMapper.getColumnsRanges());
    commonFooterlengthTokenizer.setStrict(false);

    lineTokenizers.put(getLinePattern(), executionRealigmentTokenizer);
    lineTokenizers.put(getFooterPattern(), commonFooterlengthTokenizer);

    lineMapper.setTokenizers(lineTokenizers);

    // inizialice field set mappers
    Map<String, FieldSetMapper<EuroCcpCommonSendRegisterDTO>> fieldSetMappers = new HashMap<String, FieldSetMapper<EuroCcpCommonSendRegisterDTO>>();
    fieldSetMappers.put(getLinePattern(), executionRealigmentFieldSetMapper);
    fieldSetMappers.put(getFooterPattern(), commonFooterFieldSetMapper);

    lineMapper.setFieldSetMappers(fieldSetMappers);
    return lineMapper;
  }

  private LineMapper<EuroCcpCommonSendRegisterDTO> getOrgCrgLineMapper() {
    RegexPatternMatchingCompositeLineMapper<EuroCcpCommonSendRegisterDTO> lineMapper = new RegexPatternMatchingCompositeLineMapper<EuroCcpCommonSendRegisterDTO>();
    Map<String, LineTokenizer> lineTokenizers = new HashMap<String, LineTokenizer>();

    EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper grossExecutionsFieldSetMapper = new EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper();

    final FixedLengthTokenizer grosExecutionLineTokenizer = new FixedLengthTokenizer();
    grosExecutionLineTokenizer.setNames(grossExecutionsFieldSetMapper.getFieldNames());
    grosExecutionLineTokenizer.setColumns(grossExecutionsFieldSetMapper.getColumnsRanges());
    grosExecutionLineTokenizer.setStrict(false);

    EuroCcpCommonSendFooterFieldSetMapper commonFooterFieldSetMapper = new EuroCcpCommonSendFooterFieldSetMapper();

    final FixedLengthTokenizer commonFooterTokenizer = new FixedLengthTokenizer();
    commonFooterTokenizer.setNames(commonFooterFieldSetMapper.getFieldNames());
    commonFooterTokenizer.setColumns(commonFooterFieldSetMapper.getColumnsRanges());
    commonFooterTokenizer.setStrict(false);

    lineTokenizers.put(getLinePattern(), grosExecutionLineTokenizer);
    lineTokenizers.put(getFooterPattern(), commonFooterTokenizer);

    lineMapper.setTokenizers(lineTokenizers);

    // inizialice field set mappers

    Map<String, FieldSetMapper<EuroCcpCommonSendRegisterDTO>> fieldSetMappers = new HashMap<String, FieldSetMapper<EuroCcpCommonSendRegisterDTO>>();
    fieldSetMappers.put(getLinePattern(), grossExecutionsFieldSetMapper);
    fieldSetMappers.put(getFooterPattern(), commonFooterFieldSetMapper);

    lineMapper.setFieldSetMappers(fieldSetMappers);

    return (LineMapper<EuroCcpCommonSendRegisterDTO>) lineMapper;
  }

}
