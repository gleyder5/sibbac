package sibbac.business.comunicaciones.pti.task;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_OPEN_PTI_SESSION, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 55 7 ? * MON-FRI")
public class TaskOpenPtiSession extends WrapperTaskConcurrencyPrevent {

  protected static final Logger LOG = LoggerFactory.getLogger(TaskOpenPtiSession.class);

  @Autowired
  private Tmct0menBo menBo;

  @Autowired
  private Tmct0ValoresBo valoresBo;

  public TaskOpenPtiSession() {
  }

  @Override
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio");

    LOG.info("[executeTask] inactivando valores con fecha ultimo dia negociacion pasada...");
    valoresBo.inactivarValoresUltimoDiaNegociacion();

    LOG.info("[executeTask] abriendo session pti...");
    menBo.openPtiSession();
    LOG.info("[executeTask] fin");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_OPEN_PTI_SESSION;
  }

}
