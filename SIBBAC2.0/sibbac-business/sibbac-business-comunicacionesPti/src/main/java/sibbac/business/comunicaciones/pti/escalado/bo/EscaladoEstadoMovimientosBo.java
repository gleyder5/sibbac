package sibbac.business.comunicaciones.pti.escalado.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.titulares.bo.TitularesConfigDTO;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.dto.DatosAsignacionDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TiposComision;
import sibbac.common.TiposSibbac.TiposEnvioComisionPti;
import sibbac.common.helper.CloneObjectHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIConstants.MO_CONSTANTS.TIPO_MOVIMIENTO;

@Service
public class EscaladoEstadoMovimientosBo {

  private static final Logger LOG = LoggerFactory.getLogger(EscaladoEstadoMovimientosBo.class);

  @Value("${sibbac.escalado.asignacion.cta.pdte.usuario}")
  private String listCtasPdteUsuario;

  @Value("${sibbac.escalado.asignacion.cta.imputada.propia}")
  private String listCtasInputadaPropia;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private Tmct0estadoBo estadoBo;

  @Autowired
  private Tmct0cfgBo configBo;

  @Autowired
  private FestivoBo festivoBo;

  public EscaladoEstadoMovimientosBo() {
  }

  

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void escalarBookings(List<Tmct0bokId> listBookId, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    LOG.trace("[escalarBookings] inicio");

    for (Tmct0bokId tmct0bokId : listBookId) {
      try {
        LOG.trace("[escalarBookings] booking: {}", tmct0bokId);
        this.tratarMovimientosTmct0alo(tmct0bokId, config);
        this.tratarMovimientosDesgloseCamara(tmct0bokId, config);

      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format(
            "Inciencia con booking %s, bookings que tampoco seran tratados %s msg: %s", tmct0bokId, listBookId,
            e.getMessage()), e);
      }
    }// fin for
    LOG.trace("[escalarBookings] fin");
  }

  @Transactional
  private void tratarMovimientosTmct0alo(Tmct0bokId tmct0bokId, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    List<Tmct0movimientoecc> listMovs;
    Map<String, BigDecimal> titulosPorEstado;
    Tmct0alo tmct0alo;
    List<Tmct0aloId> listAlosIds = movBo.findAllTmct0aloIdWithMovimientosRecibiendose(tmct0bokId);
    if (CollectionUtils.isNotEmpty(listAlosIds)) {
      for (Tmct0aloId tmct0aloId : listAlosIds) {
        tmct0alo = aloBo.findById(tmct0aloId);
        titulosPorEstado = new HashMap<String, BigDecimal>();
        listMovs = movBo.findByTmct0aloId(tmct0alo.getId());
        if (CollectionUtils.isNotEmpty(listMovs)) {
          if (tmct0alo.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
              && tmct0alo.getTmct0estadoByCdestadoasig().getIdestado() < EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
                  .getId()) {
            this.agruparTitulosEstado(listMovs, titulosPorEstado);
            this.cambiarEstadoByMovimientos(tmct0alo, listMovs, titulosPorEstado, config);
          }
          else {
            LOG.warn("[tratarMovimientosTmct0alo] allo {} en estado {} no permite escalado...", tmct0alo.getId(),
                tmct0alo.getTmct0estadoByCdestadoasig().getNombre());
          }
          this.darBajaMovimientosNoVivos(listMovs, config.getAuditUser());
          this.setEstadoEccInCdestadoasignacion(listMovs, config.getAuditUser());

        }
      }
    }
    //
  }

  @Transactional
  private void tratarMovimientosDesgloseCamara(Tmct0bokId tmct0bokId, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    Map<Long, Map<String, BigDecimal>> mapTitulosPorEstadoIdDesgloseCamara = new HashMap<Long, Map<String, BigDecimal>>();
    Map<String, BigDecimal> titulosPorEstado;
    List<Long> listIdsDesgloseCamara;
    List<Tmct0movimientoecc> listMovs;

    Tmct0desglosecamara dc;
    boolean isTraspaso;
    BigDecimal titulosVivos;
    listIdsDesgloseCamara = movBo.findAllIddesglosecamaraWithMovimientosRecibiendose(tmct0bokId);
    Collection<Tmct0alcId> listAlcsRevisarEstado = new HashSet<Tmct0alcId>();
    if (CollectionUtils.isNotEmpty(listIdsDesgloseCamara)) {
      for (Long iddesglosecamara : listIdsDesgloseCamara) {
        titulosPorEstado = new HashMap<String, BigDecimal>();

        LOG.trace("[escalarBookings] dc: {} con movimientos recibiendose...", iddesglosecamara);
        LOG.trace("[escalarBookings] buscando desglose camara...");
        dc = dcBo.findById(iddesglosecamara);

        LOG.trace("[escalarBookings] buscando movimientos...");
        listMovs = movBo.findByIdDesgloseCamara(iddesglosecamara);

        if (CollectionUtils.isNotEmpty(listMovs)) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            isTraspaso = dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO
                .getId();

            titulosVivos = this.agruparTitulosEstado(listMovs, titulosPorEstado);

            if (this.darBajaMovimientosInnecesarios(listMovs, titulosPorEstado, config.getAuditUser())) {
              titulosVivos = this.agruparTitulosEstado(listMovs, titulosPorEstado);
            }

            if (titulosVivos.compareTo(dc.getNutitulos()) > 0 || isTraspaso) {
              LOG.trace("[escalarBookings] desglose tiene menos {} titulos que movimientos {}", dc.getNutitulos(),
                  titulosVivos);

              // esta parte es mas delicada, si hay codigos de op que sustituyen
              // codigos antiguos hay que darlos de baja
            }

            if (this.cambiarEstadoByMovimientos(dc, listMovs, titulosPorEstado, config)) {
              this.darBajaMovimientosNoVivos(listMovs, config.getAuditUser());
              this.setEstadoEccInCdestadoasignacion(listMovs, config.getAuditUser());
              if (dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR_ESPERANDO_OTRAS_CAMARAS
                  .getId()) {
                listAlcsRevisarEstado.add(dc.getTmct0alc().getId());
              }

            }
          }

        }
        mapTitulosPorEstadoIdDesgloseCamara.put(iddesglosecamara, titulosPorEstado);
      }// fin for

      if (!listAlcsRevisarEstado.isEmpty()) {
        this.escalarEstadoPdteLiquidarEsperandoCamaras(listAlcsRevisarEstado, mapTitulosPorEstadoIdDesgloseCamara,
            config);
        listAlcsRevisarEstado.clear();
      }

      mapTitulosPorEstadoIdDesgloseCamara.clear();
    }
  }

  private void escalarEstadoPdteLiquidarEsperandoCamaras(Collection<Tmct0alcId> listAlcIds,
      Map<Long, Map<String, BigDecimal>> mapTitulosPorEstadoIdDesgloseCamara, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    String msg;
    Tmct0alc alc;
    int estadoAsignacion;
    List<Tmct0movimientoecc> listMovs;
    int countDcsPdteLiquidarEsperandoCamaras;
    Map<String, BigDecimal> titulosPorEstado;
    List<Tmct0desglosecamara> listDcs;
    List<DatosAsignacionDTO> listAsignacionesDistintas;
    for (Tmct0alcId alcId : listAlcIds) {

      estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId();

      if (movBo.hasMovimientoImputadoS3(alcId)) {
        estadoAsignacion = EstadosEnumerados.ASIGNACIONES.IMPUTADA_S3.getId();
      }

      countDcsPdteLiquidarEsperandoCamaras = dcBo.countDesglosesCamaraByAlcIdAndCdestadoasig(alcId,
          EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR_ESPERANDO_OTRAS_CAMARAS.getId());

      if (countDcsPdteLiquidarEsperandoCamaras > 0
          && countDcsPdteLiquidarEsperandoCamaras == dcBo.countDesglosesCamaraByAlcIdAndCdestadoasigAndAlta(alcId)) {

        listAsignacionesDistintas = movBo.findAllDistinctCtaMiembroRefAsigByAlcId(alcId);

        if (this.isDistintasAsignaciones(listAsignacionesDistintas)) {
          estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO_ASIGNACIONES_DISTINTAS.getId();
        }

        alc = alcBo.findById(alcId);
        listDcs = dcBo.findAllByTmct0alcActivosIdAndCdestadoasig(alcId);
        for (Tmct0desglosecamara dc : listDcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            titulosPorEstado = mapTitulosPorEstadoIdDesgloseCamara.get(dc.getIddesglosecamara());
            if (titulosPorEstado == null) {
              titulosPorEstado = new HashMap<String, BigDecimal>();
              listMovs = movBo.findByIdDesgloseCamara(dc.getIddesglosecamara());
              this.agruparTitulosEstado(listMovs, titulosPorEstado);
            }
            if (this.isPdteAceptarCompensadorToPdteLiquidarPorCancelacionOrRechazo(dc, estadoAsignacion,
                titulosPorEstado)) {
              alcBo.reenviarS3(dc.getTmct0alc(), dc, config.getAuditUser());
            }
            else {
              this.revisarReenviosPostPdteLiquidar(dc, config);
            }
          }
        }

        alcBo.ponerEstadoAsignacion(alc, estadoAsignacion, config.getAuditUser());
        dcBo.save(alc.getTmct0desglosecamaras());
        alcBo.save(alc);
        aloBo.save(alc.getTmct0alo());
        bokBo.save(alc.getTmct0alo().getTmct0bok());
        msg = "Terminada espera de la otra camara.";
        LOG.info("[escalarEstadoPdteLiquidarEsperandoCamaras] {}-{}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            "SIBBAC20");
      }
    }
  }

  @Transactional
  private BigDecimal agruparTitulosEstado(List<Tmct0movimientoecc> listMovs, Map<String, BigDecimal> titulosPorEstado)
      throws SIBBACBusinessException {
    try {
      titulosPorEstado.clear();
      BigDecimal titulosVivos = BigDecimal.ZERO;
      for (Tmct0movimientoecc mov : listMovs) {
        if (mov.getCdestadomov() != null
            && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)) {
          if (titulosPorEstado.containsKey(mov.getCdestadomov().trim())) {
            titulosPorEstado.put(mov.getCdestadomov().trim(),
                titulosPorEstado.get(mov.getCdestadomov().trim()).add(mov.getImtitulos()));
          }
          else {
            titulosPorEstado.put(mov.getCdestadomov().trim(), mov.getImtitulos());
          }

          if (!mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO.text)
              && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.RECHAZADO_ECC.text)) {
            titulosVivos = titulosVivos.add(mov.getImtitulos());
          }

        }
      }
      return titulosVivos;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(
          String.format("Incidencia agrupando movimientos por estado %s", e.getMessage()), e);
    }
  }

  @Transactional
  private boolean darBajaMovimientosInnecesarios(List<Tmct0movimientoecc> listMovs,
      Map<String, BigDecimal> titulosPorEstado, String auditUser) throws SIBBACBusinessException {
    LOG.trace("[darBajaMovimientosInnecesarios] inicio");
    boolean movimientoDadoBaja;
    try {
      movimientoDadoBaja = false;

      Map<Long, BigDecimal> titulosActivosByMovimiento = new HashMap<Long, BigDecimal>();
      Map<String, BigDecimal> titulosActivosByOpNueva = new HashMap<String, BigDecimal>();
      Map<String, Long> movimientoByOpNueva = new HashMap<String, Long>();
      Map<Long, List<Tmct0movimientooperacionnuevaecc>> movimientosByNudesglose = new HashMap<Long, List<Tmct0movimientooperacionnuevaecc>>();
      List<Tmct0movimientooperacionnuevaecc> listMovn;

      boolean seNecesitaDarDeBaja = false;

      // necesitamos los movimientos por operacion de ecc
      for (Tmct0movimientoecc mov : listMovs) {

        if (mov.getCdestadomov() != null
            && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)
            && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO.text)
            && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.RECHAZADO_ECC.text)) {
          titulosActivosByMovimiento.put(mov.getIdmovimiento(), mov.getImtitulos());
          listMovn = mov.getTmct0movimientooperacionnuevaeccs();
          for (Tmct0movimientooperacionnuevaecc movn : listMovn) {
            if (StringUtils.isNotBlank(movn.getCdoperacionecc())) {
              movimientoByOpNueva.put(movn.getCdoperacionecc(), movn.getTmct0movimientoecc().getIdmovimiento());
              titulosActivosByOpNueva.put(movn.getCdoperacionecc(), movn.getImtitulos());
              if (!movimientosByNudesglose.containsKey(movn.getNudesglose())) {
                movimientosByNudesglose.put(movn.getNudesglose(), new ArrayList<Tmct0movimientooperacionnuevaecc>());
              }
              movimientosByNudesglose.get(movn.getNudesglose()).add(movn);
              if (movimientosByNudesglose.get(movn.getNudesglose()).size() > 1) {
                LOG.trace(
                    "[darBajaMovimientosInnecesarios] encontrado un desglose {} con varios movimientos, alguno tenemos que dar de baja...",
                    movn.getNudesglose());
                seNecesitaDarDeBaja = true;
              }
            }
          }
        }
      }

      if (seNecesitaDarDeBaja) {
        // restamos los titulos de los movimientos que pueden estar solapados
        // por
        // un
        // traspaso
        for (Tmct0movimientoecc mov : listMovs) {
          if (mov.getCdestadomov() != null && mov.getCdestadomov().trim().equals(EstadosEccMovimiento.FINALIZADO.text)) {
            listMovn = mov.getTmct0movimientooperacionnuevaeccs();
            for (Tmct0movimientooperacionnuevaecc movn : listMovn) {
              if (movimientoByOpNueva.containsKey(movn.getNuoperacionprev())
                  && !movn.getCdoperacionecc().equals(movn.getNuoperacionprev())) {
                titulosActivosByMovimiento.put(
                    movimientoByOpNueva.get(movn.getNuoperacionprev()),
                    titulosActivosByMovimiento.get(movimientoByOpNueva.get(movn.getNuoperacionprev())).subtract(
                        movn.getImtitulos()));
                if (titulosActivosByOpNueva.containsKey(movn.getNuoperacionprev())) {
                  titulosActivosByOpNueva.put(movn.getNuoperacionprev(),
                      titulosActivosByOpNueva.get(movn.getNuoperacionprev()).subtract(movn.getImtitulos()));
                }
              }
            }
          }
        }

        // damos de baja aquellos movimientos que estan totalmente solapados por
        // un
        // movimiento
        // aqui falta por saber que vamos a hacer en el caso de que un
        // movimiento
        // solo este parcialmente solapado
        for (Tmct0movimientoecc mov : listMovs) {
          if (mov.getCdestadomov() != null
              && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)
              && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO.text)
              && !mov.getCdestadomov().trim().equals(EstadosEccMovimiento.RECHAZADO_ECC.text)) {
            if (titulosActivosByMovimiento.get(mov.getIdmovimiento()) != null
                && titulosActivosByMovimiento.get(mov.getIdmovimiento()).compareTo(BigDecimal.ZERO) <= 0) {
              LOG.trace("[darBajaMovimientosInnecesarios] todos los titulos del movimiento no son necesarios...");
              mov.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
              mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
              mov.setAuditFechaCambio(new Date());
              mov.setAuditUser(auditUser);
              movBo.save(mov);
              movimientoDadoBaja = true;
            }
            else if (titulosActivosByMovimiento.get(mov.getIdmovimiento()) != null
                && titulosActivosByMovimiento.get(mov.getIdmovimiento()).compareTo(mov.getImtitulos()) < 0) {
              if (titulosPorEstado.get(EstadosEccMovimiento.ENVIANDOSE_INT) != null
                  && titulosPorEstado.get(EstadosEccMovimiento.ENVIANDOSE_INT).compareTo(BigDecimal.ZERO) > 0
                  || titulosPorEstado.get(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR) != null
                  && titulosPorEstado.get(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR).compareTo(BigDecimal.ZERO) > 0) {
                LOG.trace("[darBajaMovimientosInnecesarios] aun quedan movimientos en transito, no se toca nada {}...",
                    titulosPorEstado);
              }
              else {
                LOG.trace("[darBajaMovimientosInnecesarios] alguno de los titulos del movimiento no son necesarios, tenemos que dar de baja parte...");
                movimientoDadoBaja = true;
                mov.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
                mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
                mov.setAuditFechaCambio(new Date());
                mov.setAuditUser(auditUser);
                movBo.save(mov);

                Tmct0movimientoecc movResto = new Tmct0movimientoecc();
                CloneObjectHelper.copyBasicFields(mov, movResto, null);
                movResto.setIdmovimiento(null);
                movResto.setTmct0desglosecamara(mov.getTmct0desglosecamara());
                movResto.setTmct0movimientooperacionnuevaeccs(new ArrayList<Tmct0movimientooperacionnuevaecc>());
                movResto.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
                movResto.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
                movResto = movBo.save(movResto);
                listMovn = mov.getTmct0movimientooperacionnuevaeccs();
                BigDecimal efectivo = BigDecimal.ZERO;
                BigDecimal titulos = BigDecimal.ZERO;
                for (Tmct0movimientooperacionnuevaecc movn : listMovn) {
                  if (titulosActivosByOpNueva.containsKey(movn.getCdoperacionecc())
                      && titulosActivosByOpNueva.get(movn.getCdoperacionecc()).compareTo(BigDecimal.ZERO) > 0) {
                    Tmct0movimientooperacionnuevaecc movnResto = new Tmct0movimientooperacionnuevaecc();
                    CloneObjectHelper.copyBasicFields(movn, movnResto, null);
                    movnResto.setIdmovimientooperacionnuevaec(null);
                    movnResto.setTmct0movimientoecc(movResto);
                    if (titulosActivosByOpNueva.get(movn.getCdoperacionecc()).compareTo(movn.getImtitulos()) != 0) {
                      movnResto.setImtitulos(titulosActivosByOpNueva.get(movn.getCdoperacionecc()));
                      movnResto.setImefectivo(movnResto.getImtitulos().multiply(movnResto.getImprecio())
                          .setScale(2, RoundingMode.HALF_UP));
                    }
                    movnResto = movnBo.save(movnResto);
                    movResto.getTmct0movimientooperacionnuevaeccs().add(movnResto);
                    efectivo = efectivo.add(movnResto.getImefectivo());
                    titulos = titulos.add(movnResto.getImtitulos());
                  }
                }

                if (titulos.compareTo(movResto.getImtitulos()) != 0) {
                  movResto.setImtitulos(titulos);
                  movResto.setImefectivo(efectivo);
                }
                movBo.save(movResto);
              }
            }
          }
        }
      }
      titulosActivosByMovimiento.clear();
      movimientoByOpNueva.clear();
      movimientosByNudesglose.clear();
      titulosActivosByOpNueva.clear();
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("Incidencia dando de baja movimientos viejos traspasados %s",
          e.getMessage()), e);
    }
    LOG.trace("[darBajaMovimientosInnecesarios] fin: {}", movimientoDadoBaja);
    return movimientoDadoBaja;
  }

  @Transactional
  private void darBajaMovimientosNoVivos(List<Tmct0movimientoecc> listMovs, String auditUser)
      throws SIBBACBusinessException {
    LOG.trace("[darBajaMovimientosNoVivos] inicio");
    try {
      for (Tmct0movimientoecc mov : listMovs) {

        if (mov.getCdestadomov() != null
            && (mov.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO.text) || mov.getCdestadomov().trim()
                .equals(EstadosEccMovimiento.RECHAZADO_ECC.text))) {
          mov.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
          mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
          mov.setAuditFechaCambio(new Date());
          mov.setAuditUser(auditUser);
          movBo.save(mov);
        }
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(String.format("Incidencia dando de baja movimientos que no estan vivos %s",
          e.getMessage()), e);
    }
    LOG.trace("[darBajaMovimientosNoVivos] fin");
  }

  @Transactional
  private Integer calculaEstadoAsignacionFromMovimientos(Tmct0alo alo, Tmct0desglosecamara dc,
      List<Tmct0movimientoecc> listMovs, BigDecimal titulos, int cdestadoasigActual,
      Map<String, BigDecimal> titulosPorEstado, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    LOG.trace("[cambiarEstadoByMovimientos] inicio");
    Integer estadoAsignacion = null;
    try {

      if (titulosPorEstado.get(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR_ORIGEN.text) != null
          && titulosPorEstado.get(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR_ORIGEN.text).compareTo(BigDecimal.ZERO) > 0) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a PDTE_ACEPTAR_COMPENSADOR...");
        estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId();
      }
      else if (titulosPorEstado.get(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text) != null
          && titulosPorEstado.get(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text).compareTo(BigDecimal.ZERO) > 0) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a PDTE_ACEPTAR_COMPENSADOR...");
        estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId();
      }
      else if (titulosPorEstado.get(EstadosEccMovimiento.FINALIZADO.text) != null
          && titulos.compareTo(titulosPorEstado.get(EstadosEccMovimiento.FINALIZADO.text)) == 0) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a PDTE_LIQUIDAR...");
        estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId();
        if (dc != null) {
          List<DatosAsignacionDTO> listDatosAsignacion = movBo.findAllDistinctCtaMiembroRefAsigByIddesglosecamara(dc
              .getIddesglosecamara());

          if (!this.isDistintasAsignaciones(listDatosAsignacion)) {

            this.actualizarInformacionCompensacion(dc.getTmct0alc().getTmct0alo().getId(), dc,
                listDatosAsignacion.get(0), dc.getTmct0infocompensacion(), config.getAuditUser());

            if (isCambioEstadoPdteUsuario(listMovs, listDatosAsignacion)) {
              BigDecimal fliquidacion = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
                  .convertDateToString(dc.getFeliquidacion()));
              BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
                  .convertDateToString(new Date()));
              if (now.compareTo(fliquidacion) < 0) {
                LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a PDTE_REVISAR_USUARIO...");
                estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId();
              }
            }
            else if (dcBo.countDesglosesCamaraByAlcIdAndCdestadoasigLteAndDistinctIddesgloseCamara(dc.getTmct0alc()
                .getId(), EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR_ESPERANDO_OTRAS_CAMARAS.getId(), dc
                .getIddesglosecamara()) > 0) {
              LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a PDTE_LIQUIDAR_ESPERANDO_OTRAS_CAMARAS...");
              estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR_ESPERANDO_OTRAS_CAMARAS.getId();
            }
            else if (isCambioEstadoPdteLiquidarCtaPropia(listMovs, listDatosAsignacion, config)) {
              LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a IMPUTADA_PROPIA...");
              estadoAsignacion = EstadosEnumerados.ASIGNACIONES.IMPUTADA_PROPIA.getId();
            }
            else if (movBo.hasMovimientoImputadoS3(dc.getIddesglosecamara())) {
              LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a IMPUTADA_S3...");
              estadoAsignacion = EstadosEnumerados.ASIGNACIONES.IMPUTADA_S3.getId();
            }
          }
          else {
            // DISTINTAS ASIGNACIONES
            LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a PDTE_REVISAR_USUARIO_ASIGNACIONES_DISTINTAS...");
            estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO_ASIGNACIONES_DISTINTAS.getId();
          }
        }
      }
      else if (titulosPorEstado.get(EstadosEccMovimiento.RECHAZADO_ECC.text) != null
          && titulosPorEstado.get(EstadosEccMovimiento.RECHAZADO_ECC.text).compareTo(BigDecimal.ZERO) > 0
          && cdestadoasigActual != EstadosEnumerados.ASIGNACIONES.RECHAZADO_COMPENSADOR.getId()) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a RECHAZADO_COMPENSADOR...");
        estadoAsignacion = EstadosEnumerados.ASIGNACIONES.RECHAZADO_COMPENSADOR.getId();
      }
      else if (titulosPorEstado.get(EstadosEccMovimiento.CANCELADO.text) != null
          && titulosPorEstado.get(EstadosEccMovimiento.CANCELADO.text).compareTo(BigDecimal.ZERO) > 0
          && cdestadoasigActual != EstadosEnumerados.ASIGNACIONES.RECHAZADO_COMPENSADOR.getId()) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado a RECHAZADO_COMPENSADOR...");
        estadoAsignacion = EstadosEnumerados.ASIGNACIONES.RECHAZADO_COMPENSADOR.getId();
      }

      if (estadoAsignacion != null && cdestadoasigActual == estadoAsignacion) {
        if (dc != null) {
          LOG.trace("[cambiarEstadoByMovimientos] cambiando estado id desglose: {} a {}", dc.getIddesglosecamara(),
              estadoAsignacion);
        }
        else if (alo != null) {
          LOG.trace("[cambiarEstadoByMovimientos] cambiando estado id desglose: {} a {}", alo.getId(), estadoAsignacion);
        }
        estadoAsignacion = null;

      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(
          String.format("Incidencia calculando el estado del desglose %s", e.getMessage()), e);
    }

    return estadoAsignacion;
  }

  private boolean isDistintasAsignaciones(List<DatosAsignacionDTO> listDatosAsignacion) {
    if (listDatosAsignacion.size() < 2) {
      return false;
    }
    else {
      String key;
      Map<String, Integer> ctas = new HashMap<String, Integer>();
      Map<String, Integer> giveups = new HashMap<String, Integer>();

      for (DatosAsignacionDTO datosAsignacionDTO : listDatosAsignacion) {
        if (StringUtils.isNotBlank(datosAsignacionDTO.getCuentaCompensacionDestino())) {
          key = datosAsignacionDTO.getCuentaCompensacionDestino().trim();
          if (ctas.get(key) == null) {
            ctas.put(key, 0);
          }
          ctas.put(key, ctas.get(key) + 1);
        }
        if (StringUtils.isNotBlank(datosAsignacionDTO.getMiembroCompensadorDestino())
            && StringUtils.isNotBlank(datosAsignacionDTO.getCodigoReferenciaAsignacion())) {
          key = String.format("%s-%s", datosAsignacionDTO.getMiembroCompensadorDestino().trim(), datosAsignacionDTO
              .getCodigoReferenciaAsignacion().trim());
          if (giveups.get(key) == null) {
            giveups.put(key, 0);
          }
          giveups.put(key, giveups.get(key) + 1);
        }
      }

      if (ctas.keySet().size() == 1) {
        key = ctas.keySet().iterator().next();
        if (ctas.get(key) == listDatosAsignacion.size()) {
          return false;
        }
      }

      if (giveups.keySet().size() == 1) {
        key = giveups.keySet().iterator().next();
        if (giveups.get(key) == listDatosAsignacion.size()) {
          return false;
        }
      }
      ctas.clear();
      giveups.clear();
      return true;

    }

  }

  private void actualizarInformacionCompensacion(Tmct0aloId aloId, Tmct0desglosecamara dc,
      DatosAsignacionDTO datosAsignacion, Tmct0infocompensacion ic, String auditUser) {
    if (aloId != null && ic != null && datosAsignacion != null && dc != null) {
      LOG.trace("[actualizarInformacionCompensacion] cambiando datos asignacion ic: {}...", ic.getIdinfocomp());

      if (!StringUtils.equals(StringUtils.trim(ic.getCdctacompensacion()),
          StringUtils.trim(datosAsignacion.getCuentaCompensacionDestino()))
          || !StringUtils.equals(StringUtils.trim(ic.getCdmiembrocompensador()),
              StringUtils.trim(datosAsignacion.getMiembroCompensadorDestino()))
          || !StringUtils.equals(StringUtils.trim(ic.getCdrefasignacion()),
              StringUtils.trim(datosAsignacion.getCodigoReferenciaAsignacion()))) {
        ic.setCdctacompensacion(datosAsignacion.getCuentaCompensacionDestino() == null ? "" : datosAsignacion
            .getCuentaCompensacionDestino().trim());
        ic.setCdmiembrocompensador(datosAsignacion.getMiembroCompensadorDestino() == null ? "" : datosAsignacion
            .getMiembroCompensadorDestino().trim());
        ic.setCdrefasignacion(datosAsignacion.getCodigoReferenciaAsignacion() == null ? "" : datosAsignacion
            .getCodigoReferenciaAsignacion().trim());
        ic.setAuditFechaCambio(new Date());
        ic.setAuditUser(auditUser);
        icDao.save(ic);
        String msg = String.format(
            "Actualizada informacion compensacion Dc: '%s' Cta: '%s' Compensador: '%s' Ref.Give-Up: '%s'",
            dc.getIddesglosecamara(), ic.getCdctacompensacion(), ic.getCdmiembrocompensador(), ic.getCdrefasignacion());
        logBo.insertarRegistro(aloId.getNuorden(), aloId.getNbooking(), aloId.getNucnfclt(), msg, "", auditUser);
      }
    }
  }

  private boolean isCambioEstadoPdteUsuario(List<Tmct0movimientoecc> listMovs,
      List<DatosAsignacionDTO> listDatosAsignacion) {
    List<String> ctas = Arrays.asList(listCtasPdteUsuario.split(","));
    boolean isPdteUsuario = false;
    if (listDatosAsignacion.get(0).getCuentaCompensacionDestino() != null
        && ctas.contains(listDatosAsignacion.get(0).getCuentaCompensacionDestino().trim())) {
      isPdteUsuario = true;
    }
    return isPdteUsuario;
  }

  private boolean isCambioEstadoPdteLiquidarCtaPropia(List<Tmct0movimientoecc> listMovs,
      List<DatosAsignacionDTO> listDatosAsignacion, TitularesConfigDTO config) {
    boolean isPdteLiquidarCtaPropia = false;
    List<String> ctas = Arrays.asList(listCtasInputadaPropia.split(","));
    if (CollectionUtils.isNotEmpty(listMovs)) {
      boolean allTakeUp = true;
      for (Tmct0movimientoecc mov : listMovs) {
        // SI MOV FINALIZADO Y (CAMARA EUROCCP O CAMARA NACIONAL Y TAKE UP)
        if (mov.getCdestadomov() != null
            && mov.getCdestadomov().trim().equals(EstadosEccMovimiento.FINALIZADO.text)
            && mov.getCdtipomov() != null
            && (mov.getTmct0desglosecamara().getTmct0CamaraCompensacion().getCdCodigo().trim()
                .equals(config.getCamaraEuroccp()) || !mov.getCdtipomov().trim().equals(TIPO_MOVIMIENTO.TAKE_UP))) {
          allTakeUp = false;
          break;
        }
      }

      if (allTakeUp) {
        if (listDatosAsignacion.get(0).getCuentaCompensacionDestino() != null
            && ctas.contains(listDatosAsignacion.get(0).getCuentaCompensacionDestino().trim())) {
          isPdteLiquidarCtaPropia = true;
        }
      }
    }

    return isPdteLiquidarCtaPropia;
  }

  private boolean cambiarEstadoByMovimientos(Tmct0desglosecamara dc, List<Tmct0movimientoecc> listMovs,
      Map<String, BigDecimal> titulosPorEstado, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    boolean cambiadoEstado = false;
    String msg;
    Tmct0estado estadoFinal;
    LOG.trace("[cambiarEstadoByMovimientos] inicio");
    try {
      Integer estadoAsignacion = this.calculaEstadoAsignacionFromMovimientos(null, dc, listMovs, dc.getNutitulos(), dc
          .getTmct0estadoByCdestadoasig().getIdestado(), titulosPorEstado, config);

      boolean escalarEstado = estadoAsignacion != null;

      if (escalarEstado) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado id desglose: {} a {}", dc.getIddesglosecamara(),
            estadoAsignacion);
        estadoFinal = estadoBo.findById(estadoAsignacion);
        if (estadoFinal == null) {
          throw new SIBBACBusinessException(String.format("Incidencia estado '%s' no encontrado.", estadoAsignacion));
        }
        else {
          msg = String.format("Escalando estado de desglosecamara '%s' de '%s' a '%s'", dc.getIddesglosecamara(), dc
              .getTmct0estadoByCdestadoasig().getNombre(), estadoFinal.getNombre());
          LOG.trace("[cambiarEstadoByMovimientos]{}", msg);
          logBo.insertarRegistro(dc.getTmct0alc(), new Date(), new Date(), "", dc.getTmct0estadoByCdestadoasig(), msg);
        }

        if (this.isPdteAceptarCompensadorToPdteLiquidarPorCancelacionOrRechazo(dc, estadoAsignacion, titulosPorEstado)) {
          alcBo.reenviarS3(dc.getTmct0alc(), dc, config.getAuditUser());
        }
        else if (estadoAsignacion >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
          this.revisarReenviosPostPdteLiquidar(dc, config);
        }

        if (estadoAsignacion == EstadosEnumerados.ASIGNACIONES.IMPUTADA_S3.getId()
            && dc.getTmct0alc().getCdenvliq() != null
            && !dc.getTmct0alc().getCdenvliq().trim().equals(TiposComision.CLEARING)) {
          msg = String.format("El tipo comision pasa de '%s' a '%s'-Clearing por imputacion S3.", dc.getTmct0alc()
              .getCdenvliq(), TiposComision.CLEARING);
          LOG.info("[cambiarEstadoByMovimientos] {}-{}", dc.getTmct0alc().getId(), msg);
          logBo.insertarRegistro(dc.getTmct0alc().getId().getNuorden(), dc.getTmct0alc().getId().getNbooking(), dc
              .getTmct0alc().getId().getNucnfclt(), msg, "", "SIBBAC20");
          dc.getTmct0alc().setCdenvliq(TiposComision.CLEARING);
          dc.getTmct0alc().setCorretajePti(TiposEnvioComisionPti.NOT_ALLOWED);
        }
        // puede que merezca la pena dar de baja todo lo que
        dcBo.ponerEstadoAsignacion(dc, estadoAsignacion, config.getAuditUser());
        dcBo.save(dc);
        alcBo.save(dc.getTmct0alc());
        aloBo.save(dc.getTmct0alc().getTmct0alo());
        bokBo.save(dc.getTmct0alc().getTmct0alo().getTmct0bok());
        cambiadoEstado = true;

      }
      else {
        LOG.warn(
            "[cambiarEstadoByMovimientos] no se va a escalar el desglose camara {} aun no han llegado todos los movimientos",
            dc.getIddesglosecamara());
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(
          String.format("Incidencia cambiando el estado del desglose %s", e.getMessage()), e);
    }
    LOG.trace("[cambiarEstadoByMovimientos] fin");
    return cambiadoEstado;
  }

  private boolean isPdteAceptarCompensadorToPdteLiquidarPorCancelacionOrRechazo(Tmct0desglosecamara dc,
      int estadoAsignacion, Map<String, BigDecimal> titulosPorEstado) {
    return dc != null
        && dc.getTmct0estadoByCdestadoasig() != null
        && dc.getTmct0estadoByCdestadoasig().getIdestado() != null
        && dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR
            .getId()
        && estadoAsignacion >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()
        && (titulosPorEstado.get(EstadosEccMovimiento.RECHAZADO_ECC.text) != null
            && titulosPorEstado.get(EstadosEccMovimiento.RECHAZADO_ECC.text).compareTo(BigDecimal.ZERO) > 0 || titulosPorEstado
            .get(EstadosEccMovimiento.CANCELADO.text) != null
            && titulosPorEstado.get(EstadosEccMovimiento.CANCELADO.text).compareTo(BigDecimal.ZERO) > 0);
  }

  private void revisarReenviosPostPdteLiquidar(Tmct0desglosecamara dc, TitularesConfigDTO config)
      throws SIBBACBusinessException {

    String msg;
    // dc.setTmct0estadoByCdestadotit(new
    // Tmct0estado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId()));

    if (dc.getTmct0alc().getTmct0alo().getTmct0bok().getCdindimp() != null
        && dc.getTmct0alc().getTmct0alo().getTmct0bok().getCdindimp() != ' ') {
      dc.getTmct0alc().getTmct0alo().getTmct0bok().setCdindimp(' ');
      dc.getTmct0alc().getTmct0alo().getTmct0bok().setCdauxili("");
      msg = "Marcado 16-17 para reenviar";
      LOG.trace("[cambiarEstadoByMovimientos]{}", msg);
      logBo.insertarRegistro(dc.getTmct0alc(), new Date(), new Date(), "", dc.getTmct0estadoByCdestadoasig(), msg);
    }

    if (dc.getTmct0estadoByCdestadotit().getIdestado() != EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId()) {
      msg = "Marcado titular para reenviar";
      LOG.trace("[cambiarEstadoByMovimientos]{}", msg);
      logBo.insertarRegistro(dc.getTmct0alc(), new Date(), new Date(), "", dc.getTmct0estadoByCdestadotit(), msg);
    }

    List<String> identificaciones = afiBo.findAllCdholderByAlcAndActivos(dc.getTmct0alc().getId().getNuorden(), dc
        .getTmct0alc().getId().getNbooking(), dc.getTmct0alc().getId().getNucnfclt(), dc.getTmct0alc().getId()
        .getNucnfliq());
    alcBo.updateAlcModificacionTitulares(dc.getTmct0alc(), dc, dc.getTmct0alc().getCdrefban(), identificaciones,
        config.getHolidays(), config.getAuditUser());
  }

  @Transactional
  private void cambiarEstadoByMovimientos(Tmct0alo tmct0alo, List<Tmct0movimientoecc> listMovs,
      Map<String, BigDecimal> titulosPorEstado, TitularesConfigDTO config)
      throws SIBBACBusinessException {
    LOG.trace("[cambiarEstadoByMovimientos] inicio");
    String msg = null;
    try {
      Integer estadoAsignacion = this.calculaEstadoAsignacionFromMovimientos(tmct0alo, null, listMovs,
          tmct0alo.getNutitcli(), tmct0alo.getTmct0estadoByCdestadoasig().getIdestado(), titulosPorEstado, config);

      boolean escalarEstado = estadoAsignacion != null;

      if (escalarEstado) {
        if (estadoAsignacion == EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {

          if (tmct0alo.getTmct0alcs().isEmpty()) {
            SettlementData settlementData = settlementDataBo.getSettlementDataNacional(tmct0alo);
            if (settlementData != null) {
              if (settlementData.getTmct0ilq().getDesglose() != null
                  && settlementData.getTmct0ilq().getDesglose() == 'D') {
                estadoAsignacion = EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSAR.getId();
                msg = "Alo de disociacion tiene instrucciones de liquidacion en final, marcamos para desglosar automaticamente.";
                LOG.trace("[cambiarEstadoByMovimientos]{}-{}", tmct0alo.getId(), msg);
                logBo.insertarRegistro(tmct0alo, new Date(), new Date(), "", tmct0alo.getTmct0estadoByCdestadoasig(),
                    msg);
              }
              else {
                estadoAsignacion = EstadosEnumerados.ASIGNACIONES.DISOCIADA_PDTE_DESGLOSAR.getId();
                msg = "Alo de disociacion tiene instrucciones de liquidacion en manual/provisional, marcamos para desglosar manualmente.";
                LOG.trace("[cambiarEstadoByMovimientos]{}-{}", tmct0alo.getId(), msg);
                logBo.insertarRegistro(tmct0alo, new Date(), new Date(), "", tmct0alo.getTmct0estadoByCdestadoasig(),
                    msg);

              }
            }
            else {
              estadoAsignacion = EstadosEnumerados.ASIGNACIONES.DISOCIADA_PDTE_DESGLOSAR.getId();
              msg = "Alo de disociacion no tiene instrucciones de liquidacion, marcamos para desglosar manualmente.";
              LOG.trace("[cambiarEstadoByMovimientos]{}-{}", tmct0alo.getId(), msg);
              logBo
                  .insertarRegistro(tmct0alo, new Date(), new Date(), "", tmct0alo.getTmct0estadoByCdestadoasig(), msg);

            }
          }
          else {
            escalarEstado = false;
            msg = "Alo tiene movimiento marcado para escalar, pero es de disociacion y tiene desgloses, no podemos escalar";
            logBo.insertarRegistro(tmct0alo, new Date(), new Date(), "", tmct0alo.getTmct0estadoByCdestadoasig(), msg);
            LOG.warn("[cambiarEstadoByMovimientos]{}-{}", tmct0alo.getId(), msg);
          }
        }
      }

      if (escalarEstado) {
        LOG.trace("[cambiarEstadoByMovimientos] cambiando estado id desglose: {} a {}", tmct0alo.getId(),
            estadoAsignacion);

        msg = String.format("Escalando estado de alo a estado %s", estadoAsignacion);
        LOG.trace("[cambiarEstadoByMovimientos]{}-{}", tmct0alo.getId(), msg);
        logBo.insertarRegistro(tmct0alo, new Date(), new Date(), "", tmct0alo.getTmct0estadoByCdestadoasig(), msg);
        tmct0alo.setTmct0estadoByCdestadoasig(new Tmct0estado(estadoAsignacion));
        tmct0alo.setFhaudit(new Date());
        tmct0alo.setCdusuaud(config.getAuditUser());
        if (tmct0alo.getTmct0bok().getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO
            .getId()) {
          tmct0alo.getTmct0bok().setTmct0estadoByCdestadoasig(
              new Tmct0estado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()));
          tmct0alo.getTmct0bok().setFhaudit(new Date());
          tmct0alo.getTmct0bok().setCdusuaud(config.getAuditUser());
        }

        aloBo.save(tmct0alo);
        bokBo.save(tmct0alo.getTmct0bok());
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(
          String.format("Incidencia cambiando el estado del desglose %s", e.getMessage()), e);
    }
    LOG.trace("[cambiarEstadoByMovimientos] fin");
  }

  @Transactional
  private void setEstadoEccInCdestadoasignacion(Tmct0movimientoecc mov) {
    if (StringUtils.isNotBlank(mov.getCdestadomov())) {
      if (EstadosEccMovimiento.FINALIZADO.text.equals(mov.getCdestadomov().trim())) {
        mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
      }
      else if (EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text.equals(mov.getCdestadomov().trim())
          || EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR_ORIGEN.text.equals(mov.getCdestadomov().trim())) {
        mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId());
      }
      else if (EstadosEccMovimiento.RECHAZADO_ECC.text.equals(mov.getCdestadomov().trim())
          || EstadosEccMovimiento.CANCELADO.text.equals(mov.getCdestadomov().trim())
          || EstadosEccMovimiento.CANCELADO_INT.text.equals(mov.getCdestadomov().trim())) {
        mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
      }
      else if (EstadosEccMovimiento.ENVIANDOSE_INT.text.equals(mov.getCdestadomov().trim())) {
        mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE.getId());
      }
    }
  }

  @Transactional
  private void setEstadoEccInCdestadoasignacion(List<Tmct0movimientoecc> listMovs, String auditUser) {
    for (Tmct0movimientoecc mov : listMovs) {
      if (mov.getCdestadoasig() != null
          && mov.getCdestadoasig() == EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId()) {

        if (mov.getTmct0alo() != null
            && mov.getTmct0alo().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()
            || mov.getTmct0desglosecamara() != null
            && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                .getId()) {
          this.setEstadoEccInCdestadoasignacion(mov);
          mov.setAuditFechaCambio(new Date());
          mov.setAuditUser(auditUser);
          movBo.save(mov);
        }
        else {
          mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
          mov.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
          mov.setAuditFechaCambio(new Date());
          mov.setAuditUser(auditUser);
          movBo.save(mov);
        }
      }
    } // fin for movs

  }

}
