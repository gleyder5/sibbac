package sibbac.business.comunicaciones.euroccp.ciffile.enums;

public enum EuroCcpTransactioOrigin {

  AGENT("AGNT"),
  PRINCIPAL("PRCP");
  public String text;

  private EuroCcpTransactioOrigin(String text) {
    this.text = text;
  }
}
