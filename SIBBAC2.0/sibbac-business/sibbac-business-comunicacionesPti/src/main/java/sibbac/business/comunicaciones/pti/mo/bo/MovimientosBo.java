package sibbac.business.comunicaciones.pti.mo.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.NamingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.jms.JmsMessagesPtiBo;
import sibbac.business.comunicaciones.pti.mo.exceptions.CodigosOperacionEccDesactualizadosException;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoAssigmentDataException;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoPtiMensajeDataException;
import sibbac.business.comunicaciones.traspasos.dto.TraspasosEccConfigDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.bo.Tmct0grupoejecucionBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0parametrizacionBo;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TiposComision;
import sibbac.common.TiposSibbac.TiposEnvioComisionPti;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.mo.MO;
import sibbac.pti.messages.mo.MOCommonDataBlock;
import sibbac.pti.messages.mo.R00;
import sibbac.pti.messages.mo.R01;

/**
 * @author xIS16630
 *
 */
@Service
public class MovimientosBo {

  protected static final Logger LOG = LoggerFactory.getLogger(MovimientosBo.class);
  protected static final Logger LOG_PTI_OUT = LoggerFactory.getLogger("PTI_OUT");

  @Value("${daemons.envio.pti.mo.max.r01}")
  private int maxR01InMovimiento;

  @Value("${jms.pti.queue.online.output.queue}")
  private String moDestinationSt;

  @Value("${jms.pti.qmanager.name}")
  private String ptiJndiName;

  @Value("${jms.pti.qmanager.host}")
  private String ptiMqHost;

  @Value("${jms.pti.qmanager.port}")
  private int ptiMqPort;

  @Value("${jms.pti.qmanager.channel}")
  private String ptiMqChannel;

  @Value("${jms.pti.qmanager.qmgrname}")
  private String ptiMqManagerName;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0grupoejecucionBo gruEjeBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejeAloBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0operacioncdeccBo opBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  @Qualifier(value = "JmsMessagesPtiBo")
  private JmsMessagesPtiBo jmsMessageBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0parametrizacionBo parametrizacionBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNuevaBo;

  public MovimientosBo() {
  }

  /**
   * @param cdestadmovActivos
   * @param aloid
   * @throws NoPtiMensajeDataException
   * @throws PTIMessageException
   * @throws JMSException
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void sendMensajesMoAsignacionSinTitular(Tmct0aloId aloid, TraspasosEccConfigDTO config)
      throws NoPtiMensajeDataException, NoAssigmentDataException, PTIMessageException, SIBBACBusinessException,
      JMSException {

    try {
      if (config.getConnectionFactory() == null) {
        throw new SIBBACBusinessException("No hemos podido conectar con el gestor Mq.");
      }
      LOG.trace("[MovimientosSubListBo :: sendMensajesMoAsignacionSinTitular] inicio.");
      List<PTIMessage> msgs = new ArrayList<PTIMessage>();
      Tmct0alo alo;
      Tmct0ord ord;
      Map<String, Tmct0parametrizacionDTO> parametrizaciones = new HashMap<String, Tmct0parametrizacionDTO>();
      Tmct0parametrizacionDTO parametrizacion;
      String key;
      alo = aloBo.findById(aloid);
      ord = ordBo.findById(aloid.getNuorden());
      key = alo.getCdalias().trim() + ord.getCdmodnego().trim();
      parametrizacion = parametrizaciones.get(key);
      if (parametrizacion == null) {
        parametrizacion = findParametrizacion(aloid, alo.getCdalias(), ord.getCdmodnego(), config);
        if (parametrizacion == null) {
          String msg = String.format(
              "Incidencia no se puede enviar MO sin informacion de compensacion Alias: {} Mod.Nego.:{}",
              alo.getCdalias(), ord.getCdmodnego());
          throw new NoAssigmentDataException(msg);
        }
      }
      crearMensajesMoAsignacionSinTitular(alo, parametrizacion, msgs, config);

      aloBo.flush();

      if (CollectionUtils.isNotEmpty(msgs)) {

        sendMessagesToMq(msgs, config);

      }
      LOG.trace("[MovimientosSubListBo :: sendMensajesMoAsignacionSinTitular] final.");
    }
    catch (SIBBACBusinessException e1) {
      throw e1;
    }
    catch (Exception e1) {
      LOG.warn("Incidencia enviando movimientos", e1);
      throw new SIBBACBusinessException(e1.getMessage(), e1);
    }

  }

  /**
   * Busca la parametrizacion necesaria para enviar los Movimientos dado un
   * subcuenta|cuenta
   * 
   * @param aloId
   * @param cdalias
   * @param modeloNegocio
   * @return
   */
  private Tmct0parametrizacionDTO findParametrizacion(Tmct0aloId aloId, String cdalias, String modeloNegocio,
      TraspasosEccConfigDTO config) {
    LOG.trace("[findParametrizacion] inicio");
    Tmct0parametrizacionDTO parametrizacion = null;
    LOG.trace("[findParametrizacion] buscando camaras de aloid: {}...", aloId);
    List<String> camaras = gruEjeBo.findAllCdcamaraByAloId(aloId);
    LOG.trace("[findParametrizacion] encontradas camaras {} de aloid: {}...", camaras, aloId);
    for (String camara : camaras) {
      if (config.getCamaraNacional().equals(camara.trim())) {
        LOG.trace("[findParametrizacion] buscando parametrizacion camara: {} aloid: {}", camara, aloId);
        parametrizacion = parametrizacionBo.findParametrizacionByCdaliass(cdalias, camara, null, modeloNegocio, null,
            null);
        if (parametrizacion != null) {
          break;
        }
      }
    }
    LOG.trace("[findParametrizacion] encontrada parametrizacion aloid: {} param: {}", aloId, parametrizacion);
    return parametrizacion;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.MANDATORY)
  public List<PTIMessage> crearMensajesTraspaso(Tmct0alcId alcId, List<Tmct0desgloseclitit> listDesgloses,
      TraspasosEccConfigDTO config, CallableResultDTO res) throws SIBBACBusinessException {
    // revisamos si se trata de una asignacion interna desde la orden
    boolean isAsigOrdenInterna = false;
    List<PTIMessage> movimientos = new ArrayList<PTIMessage>();
    if (CollectionUtils.isNotEmpty(listDesgloses)) {

      LOG.trace("[sendMensajesTraspaso] inicio modulo envio traspasos a PTI...");

      List<Tmct0movimientoecc> movimientosEccDefinitivos = new ArrayList<Tmct0movimientoecc>();
      Collection<Long> idsDesgloseCamara = new HashSet<Long>();
      List<Tmct0desglosecamara> listDesglosesCamara = new ArrayList<Tmct0desglosecamara>();

      // creamos los registros de movimiento
      BigDecimal titulosTraspasos = this.crearMovimientosTraspaso(alcId, listDesgloses, idsDesgloseCamara,
          listDesglosesCamara, movimientosEccDefinitivos, config);

      if (CollectionUtils.isNotEmpty(movimientosEccDefinitivos)) {
        if (!config.isTraspaso()) {
          isAsigOrdenInterna = isAsignacionInternaOrden(listDesglosesCamara.get(0));
        }

        // salvamos los datos
        this.saveStateSentTraspasos(alcId, listDesglosesCamara, titulosTraspasos, movimientosEccDefinitivos, config,
            res);

        // si es una asignacion interna desde la orden no enviamos nada
        if (!isAsigOrdenInterna) {

          try {
            LOG.trace("[sendMensajesTraspaso] creando los mensajes en formato PTI...");
            movimientos.addAll(crearMensajesMoPti(movimientosEccDefinitivos, config.getPtiMessageVersion()));
          }
          catch (PTIMessageException e) {
            throw new SIBBACBusinessException(e.getMessage(), e);
          }

        }

      }
      else {
        throw new NoPtiMensajeDataException("No hay movimientos que enviar.");
      }
    }
    return movimientos;
  }

  private BigDecimal crearMovimientosTraspaso(Tmct0alcId alcId, List<Tmct0desgloseclitit> listDesgloses,
      Collection<Long> idsDesgloseCamara, List<Tmct0desglosecamara> listDesglosesCamara,
      List<Tmct0movimientoecc> movimientosEccDefinitivos, TraspasosEccConfigDTO config) throws SIBBACBusinessException {
    BigDecimal titulosTraspasos = BigDecimal.ZERO;
    List<Tmct0movimientoecc> movimientosEccProvisionales = new ArrayList<Tmct0movimientoecc>();
    Tmct0desglosecamara dc;
    if (CollectionUtils.isNotEmpty(listDesgloses)) {
      for (Tmct0desgloseclitit dct : listDesgloses) {

        dc = dct.getTmct0desglosecamara();

        if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(config.getCamaraNacional())) {

          this.validarEnvioConAnotacion(alcId, dct, config);

          this.crearMovimiento(alcId, dc, dct, idsDesgloseCamara, listDesglosesCamara, movimientosEccProvisionales,
              movimientosEccDefinitivos, config);

          titulosTraspasos = titulosTraspasos.add(dct.getImtitulos());
        }
      }// fin for dcts

      LOG.trace("[sendMensajesTraspaso] aniadiendo traspasos provisionales {} a definitivos {} ...",
          movimientosEccProvisionales.size(), movimientosEccDefinitivos.size());
      movimientosEccDefinitivos.addAll(movimientosEccProvisionales);
      movimientosEccProvisionales.clear();

      if (!config.isTraspaso() && isAsignacionInternaOrden(listDesglosesCamara.get(0))) {
        this.marcarMovimientosAsignacionInternaOrden(movimientosEccDefinitivos);
      }
      else {
        this.aniadirCorretaje(listDesgloses, movimientosEccDefinitivos, config);

      }
    }
    return titulosTraspasos;
  }

  private BigDecimal crearMovimiento(Tmct0alcId alcId, Tmct0desglosecamara dc, Tmct0desgloseclitit dct,
      Collection<Long> idsDesgloseCamara, List<Tmct0desglosecamara> listDesglosesCamara,
      List<Tmct0movimientoecc> movimientosEccProvisionales, List<Tmct0movimientoecc> movimientosEccDefinitivos,
      TraspasosEccConfigDTO config) {

    Tmct0movimientoecc mov;
    Tmct0movimientooperacionnuevaecc movn;
    Tmct0anotacionecc an = dct.getAnotacion();
    LOG.trace("[sendMensajesTraspaso] comprobando si el traspaso debe viajar con un solo codigo de operacion...");
    boolean movimientoUnico = an.getImtitulosdisponibles().compareTo(dct.getImtitulos()) != 0;

    if (!movimientoUnico && CollectionUtils.isNotEmpty(movimientosEccProvisionales)) {
      LOG.trace("[sendMensajesTraspaso] recuperando traspaso provisional(no completo)...");
      mov = movimientosEccProvisionales.get(0);

      LOG.trace("[sendMensajesTraspaso] aniadiendole titulos y efectivo...");
      mov.setImefectivo(mov.getImefectivo().add(dct.getImefectivo()));
      mov.setImtitulos(mov.getImtitulos().add(dct.getImtitulos()));

      LOG.trace("[sendMensajesTraspaso] aniadiendole el codigo de operacion{}...", dct.getCdoperacionecc());
      movn = this.crearMovimientoOperacionNueva(mov, dct, config.getAuditUser());
      // movNuevaBo.engancharConMovFidessa(movn);
      mov.getTmct0movimientooperacionnuevaeccs().add(movn);

      if (mov.getTmct0movimientooperacionnuevaeccs().size() == maxR01InMovimiento) {
        LOG.trace(
            "[sendMensajesTraspaso] completado el traspaso por haber llegado al limite de capacidad del mensaje {}...",
            maxR01InMovimiento);
        movimientosEccDefinitivos.add(mov);
        movimientosEccProvisionales.clear();
      }
    }
    else {
      LOG.trace("[sendMensajesTraspaso] creando nuevo traspaso con nueva secuencia...");
      mov = this.crearMovimiento(dct, config.getCtaCompensacionDestino(), config.getMiembroDestino(),
          config.getRefAsignacionGiveUp(), config.getAuditUser());

      LOG.trace("[sendMensajesTraspaso] aniadiendo codigos de operacionecc{}...", dct.getCdoperacionecc());
      movn = this.crearMovimientoOperacionNueva(mov, dct, config.getAuditUser());
      // movNuevaBo.engancharConMovFidessa(movn);
      mov.getTmct0movimientooperacionnuevaeccs().add(movn);
      if (movimientoUnico) {
        LOG.trace("[sendMensajesTraspaso] es un traspaso con un solo codigo de operacion...");
        movimientosEccDefinitivos.add(mov);
      }
      else {
        LOG.trace("[sendMensajesTraspaso] pasando el traspaso a los provisionales...");
        movimientosEccProvisionales.add(mov);
      }
    }

    if (idsDesgloseCamara.add(dc.getIddesglosecamara())) {
      listDesglosesCamara.add(dc);
    }
    return dct.getImtitulos();
  }

  private Tmct0anotacionecc validarEnvioConAnotacion(Tmct0alcId alcId, Tmct0desgloseclitit dct,
      TraspasosEccConfigDTO config) throws SIBBACBusinessException {
    String msg;
    if (StringUtils.isBlank(dct.getCdoperacionecc())) {
      msg = "EL codigo de operacion no puede ser vacio";
      throw new SIBBACBusinessException(msg);
    }

    LOG.trace("[sendMensajesTraspaso] buscando AN de cdoperacionecc {}...", dct.getCdoperacionecc());
    Tmct0anotacionecc an = dct.getAnotacion();

    if (an == null) {
      msg = String.format("Anotacion para op '%s' no encontrada.", dct.getCdoperacionecc());
      throw new SIBBACBusinessException(msg);
    }

    String ctaOrigen = dct.getCuentaCompensacionDestino();

    dctBo.cargarCuentaCompensacionFromAn(alcId, dct, an, config.getAuditUser());
    ctaOrigen = dct.getCuentaCompensacionDestino();

    if (!StringUtils.equals(ctaOrigen, an.getTmct0CuentasDeCompensacion().getCdCodigo())) {
      msg = String.format("La cta origen del desglose '%s' no se correspone con la de la anotacion '%s'", ctaOrigen, an
          .getTmct0CuentasDeCompensacion().getCdCodigo());
      throw new SIBBACBusinessException(msg);
    }

    if (StringUtils.equals(ctaOrigen, config.getCtaCompensacionDestino())) {
      msg = String.format("La cta origen del desglose '%s' no puede ser igual a la destino '%s'", ctaOrigen,
          config.getCtaCompensacionDestino());
      throw new SIBBACBusinessException(msg);
    }

    if (an.getImtitulosdisponibles().compareTo(dct.getImtitulos()) < 0) {

      List<Tmct0anotacionecc> listAnotacionesPosteriores = anBo.findByNuoperacioninicAndSentidoAndNotNovacion(
          an.getNuoperacioninic(), an.getCdsentido());

      if (CollectionUtils.isNotEmpty(listAnotacionesPosteriores)) {
        msg = String
            .format(
                " En la ejecucion '%s'-'%s' op '%s' no hay titulos disponibles, los codigos de operacion ecc no estan actualizados ",
                dct.getCdPlataformaNegociacion(), dct.getNurefexemkt().trim(), dct.getCdoperacionecc());
        throw new CodigosOperacionEccDesactualizadosException(msg);
      }
      else {
        msg = String
            .format(
                " En la ejecucion '%s'-'%s' op '%s' quedan '%s' titulos cta '%s' que no son suficientes para traspasar el desglose de '%s' ",
                dct.getCdPlataformaNegociacion(), dct.getNurefexemkt().trim(), dct.getCdoperacionecc(), an
                    .getImtitulosdisponibles().setScale(2, RoundingMode.HALF_UP), ctaOrigen, dct.getImtitulos()
                    .setScale(2, RoundingMode.HALF_UP));
        throw new SIBBACBusinessException(msg);
      }
    }

    return an;
  }

  private void saveStateSentTraspasos(Tmct0alcId alcId, List<Tmct0desglosecamara> listDesglosesCamara,
      BigDecimal titulosTraspasos, List<Tmct0movimientoecc> movimientosEccDefinitivos, TraspasosEccConfigDTO config,
      CallableResultDTO res) {
    this.saveStateSentTraspasosWithMsgs(alcId, listDesglosesCamara, titulosTraspasos, movimientosEccDefinitivos,
        "Orden de traspaso", "Enviado movimiento", config, res);

  }

  private void saveStateSentTraspasosSinEnviar(Tmct0alcId alcId, List<Tmct0desglosecamara> listDesglosesCamara,
      BigDecimal titulosTraspasos, List<Tmct0movimientoecc> movimientosEccDefinitivos, TraspasosEccConfigDTO config,
      CallableResultDTO res) {
    this.saveStateSentTraspasosWithMsgs(alcId, listDesglosesCamara, titulosTraspasos, movimientosEccDefinitivos,
        "Creado registro movimiento", "Creado registro movimiento", config, res);

  }

  private void saveStateSentTraspasosWithMsgs(Tmct0alcId alcId, List<Tmct0desglosecamara> listDesglosesCamara,
      BigDecimal titulosTraspasos, List<Tmct0movimientoecc> movimientosEccDefinitivos, String msgOrden,
      String msgEnvio, TraspasosEccConfigDTO config, CallableResultDTO res) {
    LOG.trace("[sendMensajesTraspaso] grabando en bbdd los registros de traspaso...");
    movBo.save(movimientosEccDefinitivos);

    for (Tmct0desglosecamara dcFor : listDesglosesCamara) {
      LOG.trace("[sendMensajesTraspaso] cambiando el estado del desglose camara: {} a ENVIANDOSE_XXXXXXX- {}...",
          dcFor.getIddesglosecamara(), config.getEstadoEnviandose());
      dcBo.ponerEstadoAsignacion(dcFor, config.getEstadoEnviandose(), config.getAuditUser());
      dcBo.save(dcFor);
      alcBo.save(dcFor.getTmct0alc());
      aloBo.save(dcFor.getTmct0alc().getTmct0alo());
      bokBo.save(dcFor.getTmct0alc().getTmct0alo().getTmct0bok());
    }
    res.addAllMessage(movBo.escribirTmct0logForMovimientos(alcId, movimientosEccDefinitivos, msgOrden, msgEnvio,
        config.getAuditUser()));
    bokBo.flush();
  }

  @Transactional
  public void sendMessagesToMq(List<PTIMessage> ptiMessages, TraspasosEccConfigDTO config)
      throws SIBBACBusinessException {
    this.sendMessagesToMq(config.getConnectionFactory(), config.getPtiMessageDestintion(), config.getCamaraNacional(),
        ptiMessages);
  }

  private void sendMessagesToMq(ConnectionFactory cf, String queueDestination, String idCamara,
      List<PTIMessage> ptiMessages) throws SIBBACBusinessException {
    LOG.trace("[sendMessagesToMq] creando conexion jms con connectionfactory: {}", cf);

    try (Connection connection = jmsMessageBo.getNewConnection(cf)) {
      connection.start();
      LOG.trace("[sendMessagesToMq] creando sesion jms...");
      try (Session session = jmsMessageBo.getNewSession(connection, true)) {
        try {
          LOG.trace("[sendMessagesToMq] enviando mensajes por la cola {}...", queueDestination);
          jmsMessageBo.sendPtiMessageOnline(session, queueDestination, ptiMessages);
          LOG.trace("[sendMessagesToMq] commit jms...");
          session.commit();
          LOG.trace("[sendMessagesToMq] escribiendo en pti out log...");
          for (PTIMessage ptiMessage : ptiMessages) {
            LOG_PTI_OUT.info(ptiMessage.toPTIFormat());
          }

        }
        catch (JMSException e) {
          try {
            LOG.warn("[sendMessagesToMq] rollback transaction jms...");
            session.rollback();
          }
          catch (Exception e1) {
            LOG.warn(e1.getMessage(), e1);
          }
          throw new SIBBACBusinessException(String.format("Problema con la conexion camara: '%s'.", idCamara), e);
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          try {
            LOG.warn("[sendMessagesToMq] rollback transaction jms...");
            session.rollback();
          }
          catch (Exception e1) {
            LOG.warn(e1.getMessage(), e1);
          }
          throw new SIBBACBusinessException(String.format("Problema con la conexion camara: '%s'.", idCamara), e);
        }
      }
    }
    catch (JMSException e) {
      LOG.warn(e.getMessage(), e);
      throw new SIBBACBusinessException(String.format("Problema con la conexion camara: '%s'.", idCamara), e);
    }
  }

  private void aniadirCorretaje(List<Tmct0desgloseclitit> listDesgloses,
      List<Tmct0movimientoecc> movimientosEccDefinitivos, TraspasosEccConfigDTO config) {
    List<Tmct0movimientooperacionnuevaecc> listMonvsn;
    if (CollectionUtils.isNotEmpty(movimientosEccDefinitivos) && CollectionUtils.isNotEmpty(listDesgloses)) {
      Tmct0alc alc = listDesgloses.get(0).getTmct0desglosecamara().getTmct0alc();
      BigDecimal corretaje = getCorretajeEnviar(alc, config.getCorretajePti(), config.getEstadosEccVivos());
      if (corretaje.compareTo(BigDecimal.ZERO) != 0) {

        boolean terminado = false;

        Collections.sort(movimientosEccDefinitivos, new Tmct0MovimientoeccImefectivoDescComparator());
        for (Tmct0movimientoecc movCorretaje : movimientosEccDefinitivos) {
          listMonvsn = movCorretaje.getTmct0movimientooperacionnuevaeccs();
          Collections.sort(listMonvsn, new Tmct0MovimientooperacionnuevaImefectivoDescComparator());
          if (listMonvsn.size() > 1) {
            listMonvsn.get(0).setCorretaje(corretaje);
            terminado = true;
            break;
          }
        }
        if (!terminado) {
          movimientosEccDefinitivos.get(0).getTmct0movimientooperacionnuevaeccs().get(0).setCorretaje(corretaje);
        }
      }
    }
  }

  private Tmct0movimientoecc crearMovimiento(Tmct0desgloseclitit dct, String cuentaCompensacionDestino,
      String miembroDestino, String refAsignacionGiveUp, String auditUser) {
    Tmct0movimientoecc mov = new Tmct0movimientoecc();
    mov.setCdrefmovimiento(movBo.getNewMOCdrefmovimientoFromMOSequence());

    mov.setCdestadomov(EstadosEccMovimiento.ENVIANDOSE_INT.text);
    mov.setCdindcotizacion(dct.getCdindcotizacion());
    mov.setCdrefinternaasig(dct.getTmct0desglosecamara().getTmct0alc().getId().getNucnfclt());
    mov.setCdrefmovimientoecc("");
    mov.setCdrefnotificacion("");
    mov.setCdrefnotificacionprev("");
    mov.setCdtipomov("");

    if (StringUtils.isNotBlank(cuentaCompensacionDestino)) {
      mov.setCuentaCompensacionDestino(cuentaCompensacionDestino);
      mov.setMiembroCompensadorDestino("");
      mov.setCodigoReferenciaAsignacion("");
    }
    else {
      mov.setCuentaCompensacionDestino("");
      mov.setMiembroCompensadorDestino(miembroDestino != null ? miembroDestino : "");
      mov.setCodigoReferenciaAsignacion(refAsignacionGiveUp != null ? refAsignacionGiveUp : "");
    }

    mov.setFechaLiquidacion(dct.getTmct0desglosecamara().getFeliquidacion());
    mov.setImefectivo(mov.getImefectivo().add(dct.getImefectivo()));
    mov.setImputacionPropia('N');
    mov.setImtitulos(mov.getImtitulos().add(dct.getImtitulos()));
    mov.setIsin(dct.getCdisin());

    mov.setSentido(dct.getCdsentido());
    mov.setTmct0desglosecamara(dct.getTmct0desglosecamara());

    mov.setCdtipomov("");

    mov.setAuditFechaCambio(new Date());
    mov.setAuditUser(auditUser);
    return mov;
  }

  private Tmct0movimientooperacionnuevaecc crearMovimientoOperacionNueva(Tmct0movimientoecc mov,
      Tmct0desgloseclitit dct, String auditUser) {
    Tmct0movimientooperacionnuevaecc movn = new Tmct0movimientooperacionnuevaecc();
    movn.setCdmiembromkt(dct.getCdmiembromkt());
    movn.setCdoperacionecc(dct.getCdoperacionecc());

    movn.setCorretaje(BigDecimal.ZERO);
    movn.setImefectivo(dct.getImefectivo());
    movn.setImprecio(dct.getImprecio());
    movn.setImtitulos(dct.getImtitulos());
    movn.setNudesglose(dct.getNudesglose());
    movn.setNuoperacioninic(dct.getNuoperacioninic());
    movn.setNuoperacionprev(dct.getNuoperacionprev());
    movn.setTmct0movimientoecc(mov);
    movn.setAuditFechaCambio(new Date());
    movn.setAuditUser(auditUser);
    return movn;
  }

  /**
   * @param alc
   * @param cdestadomovActivos
   * @return
   */
  private BigDecimal getCorretajeEnviar(Tmct0alc alc, Character corretajePtiCompensador, List<String> cdestadomovActivos) {
    BigDecimal corretajeEnviar = BigDecimal.ZERO;
    BigDecimal corretajeEnviado;
    BigDecimal corretajeDesgloses;
    if (StringUtils.isNotBlank(alc.getCdenvliq().trim())
        && TiposComision.CORRETAJE_PTI.equals(alc.getCdenvliq().trim())) {

      if (alc.getCorretajePti() != null
          && TiposEnvioComisionPti.MESSAGE_PTI == alc.getCorretajePti()
          && corretajePtiCompensador != null
          && (TiposEnvioComisionPti.MESSAGE_PTI == corretajePtiCompensador || TiposEnvioComisionPti.ALL_METHOD_ALLOWED == corretajePtiCompensador)) {
        corretajeDesgloses = alc.getImfinsvb();
        // recuperamos el corretaje que ya hemos enviado en otros movimientos
        corretajeEnviado = movnBo.sumCorretajeByInCdestadomovAndTmct0alcId(cdestadomovActivos, alc.getId());

        if (corretajeEnviado == null) {
          corretajeEnviado = BigDecimal.ZERO;
        }

        corretajeEnviar = corretajeDesgloses.subtract(corretajeEnviado);
      }
    }

    return corretajeEnviar;
  }

  private void marcarMovimientosAsignacionInternaOrden(List<Tmct0movimientoecc> listMovs) {
    for (Tmct0movimientoecc mov : listMovs) {
      mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
      mov.setCdestadomov(EstadosEccMovimiento.FINALIZADO.text);
    }
  }

  private boolean isAsignacionInternaOrden(Tmct0desglosecamara dc) {
    return dc.getAsigorden() == 'S' && dc.getTmct0infocompensacion() != null
        && StringUtils.isNotBlank(dc.getTmct0infocompensacion().getCdctacompensacion());
  }

  public ConnectionFactory getConnectionFactory() throws SIBBACBusinessException {
    LOG.trace("[getConnectionFactory] buscando conexion jms con pti...");
    ConnectionFactory cf;
    try {
      cf = jmsMessageBo.getConnectionFactoryFromContext(ptiJndiName);
    }
    catch (NamingException e) {
      try {
        cf = jmsMessageBo.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
      }
      catch (JMSException e1) {
        LOG.trace(e1.getMessage(), e1);
        throw new SIBBACBusinessException("Imposible recuperar conexion con camara nacional.", e1);
      }
    }
    return cf;
  }

  /**
   * @param cdestadmovActivos
   * @param alc
   * @param messagesPti
   * @throws PTIMessageException
   * @throws SIBBACBusinessException
   * @throws NoPtiMensajeDataException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void crearMensajesMoAsignacionSinTitular(Tmct0alo alo, Tmct0parametrizacionDTO parametrizacion,
      List<PTIMessage> messagesPti, TraspasosEccConfigDTO config) throws PTIMessageException, SIBBACBusinessException,
      NoPtiMensajeDataException, NoAssigmentDataException {

    LOG.trace("[MovimientosSubListBo :: crearMensajesMoAsignacionSinTitular] inicio.");
    // Tmct0aloId aloId = alo.getId();
    // Tmct0alo alo = aloBo.findById(aloId);
    List<String> cdestadmovActivos = config.getEstadosEccVivos();
    List<PTIMessage> localMessagesPti = new ArrayList<PTIMessage>();

    List<Tmct0ejecucionalocation> ejealos;
    List<Tmct0ejecucionalocation> ejealosPartidos = new ArrayList<Tmct0ejecucionalocation>();
    List<Tmct0ejecucionalocation> ejealosNoPartidos = new ArrayList<Tmct0ejecucionalocation>();
    Tmct0infocompensacion ic = new Tmct0infocompensacion();
    ic.setCdmiembrocompensador(parametrizacion.getNbNombreCompensador());
    ic.setCdrefasignacion(parametrizacion.getReferenciaAsignacion());
    ic.setCdctacompensacion(parametrizacion.getCdCodigoCuentaCompensacion());

    this.checkDataFormInformacionCompensacion(alo, ic);
    List<Tmct0movimientoecc> movsByAllo = new ArrayList<Tmct0movimientoecc>();
    LOG.trace("[crearMensajesMoAsignacionSinTitular] buscando ejealos aloid: {}...", alo.getId());
    ejealos = ejeAloBo.findAllByAlloIdAndInCamarasCompensacion(alo.getId(), config.getCamaraNacional());
    LOG.trace("[crearMensajesMoAsignacionSinTitular] encontrados  ejealos {} aloid: {}...", ejealos.size(), alo.getId());
    this.separarDesglosesPartidosSinTitular(cdestadmovActivos, alo, ejealos, ejealosPartidos, ejealosNoPartidos);
    movsByAllo.addAll(this.grabarMovimientosEccSinTitular(ic, alo, ejealosPartidos));
    movsByAllo.addAll(this.grabarMovimientosEccSinTitular(ic, alo, ejealosNoPartidos));

    localMessagesPti.addAll(this.crearMensajesMoPtiSinTitular(alo, movsByAllo, config.getPtiMessageVersion()));
    if (CollectionUtils.isNotEmpty(localMessagesPti)) {
      alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
      alo.setFhaudit(new Date());
      alo.setCdusuaud(config.getAuditUser());
      alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_SIN_TITULAR.getId());
      aloBo.save(alo);
    }
    else {
      throw new NoPtiMensajeDataException();
    }

    messagesPti.addAll(localMessagesPti);
    LOG.trace("[MovimientosSubListBo :: crearMensajesMoAsignacionSinTitular] fin.");
  }

  private List<MO> crearMensajesMoPtiSinTitular(Tmct0alo alo, List<Tmct0movimientoecc> movs, PTIMessageVersion version)
      throws PTIMessageException, SIBBACBusinessException {
    List<MO> msgs = crearMensajesMoPti(movs, version);

    for (Tmct0movimientoecc mov : movs) {
      logMovimientoEnviadoSinTitular(alo, alo.getTmct0estadoByCdestadoasig(), mov);
    }
    return msgs;
  }

  private void logMovimientoEnviadoSinTitular(Tmct0alo alo, Tmct0estado estado, Tmct0movimientoecc mov)
      throws SIBBACBusinessException {
    if (StringUtils.isNotBlank(mov.getCuentaCompensacionDestino()) || "15".equals(mov.getCdtipomov())) {
      logBo.insertarRegistro(
          alo,
          new Date(),
          new Date(),
          "",
          estado,
          "Enviado  MO Sin Titular Asig. Ord. Int. RefMov: " + mov.getCdrefmovimiento() + " Cta: "
              + mov.getCuentaCompensacionDestino() + " Miembro: " + mov.getMiembroCompensadorDestino()
              + " Ref.Int.Asig: " + mov.getCdrefinternaasig());
    }
    else {
      logBo.insertarRegistro(
          alo,
          new Date(),
          new Date(),
          "",
          estado,
          "Enviado  MO Sin Titular Asig. Ord. Int. RefMov: " + mov.getCdrefmovimiento() + " Miembro: "
              + mov.getMiembroCompensadorDestino() + " Ref.Asig: " + mov.getCodigoReferenciaAsignacion()
              + " Ref.Int.Asig: " + mov.getCdrefinternaasig());
    }
  }

  /**
   * @param asignacionDesdeOrdenInterna si es una asignacion interna
   * @param cdestadmovActivos estados del movimiento que siguen vivos
   * @param alc
   * @param dc
   * @param dcts desgloses clitit origen
   * @param dctsPartidos lista resultado de los desgloses que no contienen todos
   * los titulos de la ejecucion
   * @throws SIBBACBusinessException
   * @param dctsNoPartidos lista resultado de los desgloses que si contienen
   * todos los titulos de la ejecucion
   */
  private void separarDesglosesPartidosSinTitular(List<String> cdestadmovActivos, Tmct0alo alo,
      List<Tmct0ejecucionalocation> dcts, List<Tmct0ejecucionalocation> dctsPartidos,
      List<Tmct0ejecucionalocation> dctsNoPartidos) throws SIBBACBusinessException {
    LOG.trace("[separarDesglosesPartidosSinTitular] inicio");
    List<Tmct0movimientooperacionnuevaecc> movsNueva;
    Tmct0eje eje;
    Tmct0operacioncdecc op;
    for (Tmct0ejecucionalocation dct : dcts) {
      LOG.trace("[separarDesglosesPartidosSinTitular] buscando movimientos ya enviados idejealo: {}...",
          dct.getIdejecucionalocation());
      movsNueva = movNuevaBo.findByInCdestadomovAndIdejecucionalocation(cdestadmovActivos,
          dct.getIdejecucionalocation());
      LOG.trace("[separarDesglosesPartidosSinTitular] encontrados {} movimientos ya enviados idejealo: {}...",
          movsNueva.size(), dct.getIdejecucionalocation());

      eje = dct.getTmct0eje();
      LOG.trace("[separarDesglosesPartidosSinTitular] recuperando case y opcdecc...");
      op = eje.getTmct0caseoperacioneje().getTmct0operacioncdecc();
      if (CollectionUtils.isEmpty(movsNueva)) {

        if (op.getImtitulosdisponibles().compareTo(dct.getNutitulos()) >= 0) {
          LOG.trace("[separarDesglosesPartidosSinTitular] quedan titulos disponibles...");
          if (dct.getNutitulos().compareTo(op.getNutitulos()) != 0) {
            LOG.trace("[separarDesglosesPartidosSinTitular] ES EJECUCION PARTIDA...");
            dctsPartidos.add(dct);
          }
          else {
            LOG.trace("[separarDesglosesPartidosSinTitular] NO es ejecucion partida...");
            dctsNoPartidos.add(dct);
          }
        }
        else {
          LOG.warn(
              "[CreacionMosAsignacionSubListBo :: crearMensajesMoAsignacion] El numero de titulos de la op {} en el desglose {} es mayor al disponible en la cuenta diaria {}",
              op.getCdoperacionecc(), dct.getNutitulos(), op.getImtitulosdisponibles());

          LOG.warn(
              "[CreacionMosAsignacionSubListBo :: crearMensajesMoAsignacion] Problema con los titulos en  ## {} ## idejecucionalocation: {} HAY QUE PARTIRLO",
              alo.getId(), dct.getIdejecucionalocation());
          logBo.insertarRegistroNewTransaction(
              alo,
              new Date(),
              new Date(),
              "",
              alo.getTmct0estadoByCdestadoasig(),
              "Incidencia OpEcc: " + op.getCdoperacionecc() + " Nutit.Desglose: "
                  + dct.getNutitulos().setScale(0, RoundingMode.HALF_UP) + " Nutit.Disponibles.Cd: "
                  + op.getImtitulosdisponibles().setScale(0, RoundingMode.HALF_UP));
        }
      }
      else {

        logBo.insertarRegistroNewTransaction(
            alo,
            new Date(),
            new Date(),
            "",
            alo.getTmct0estadoByCdestadoasig(),
            "Incidencia OpEcc: " + op.getCdoperacionecc() + " Nutit.Desglose: "
                + dct.getNutitulos().setScale(0, RoundingMode.HALF_UP) + " idejecucionalocation: "
                + dct.getIdejecucionalocation() + " ya tiene Mov enviado.");
      }
    }
    LOG.trace("[separarDesglosesPartidosSinTitular] fin");
  }

  /**
   * @param alc
   * @param dc
   * @param ic
   * @throws SIBBACBusinessException
   */
  private void checkDataFormInformacionCompensacion(Tmct0alo alo, Tmct0infocompensacion ic)
      throws SIBBACBusinessException {
    LOG.trace("[MovimientosSubListBo :: checkDataFormInformacionCompensacion] inicio.");
    String msg = "";
    msg = "No podemos mandar un movimiento de un alo : " + alo.getId() + " sin informacion de compensacion Alias: "
        + alo.getCdalias();
    if (ic == null) {
      logBo.insertarRegistroNewTransaction(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig(), msg);
      throw new NoAssigmentDataException(msg);
    }
    else if (StringUtils.isBlank(ic.getCdctacompensacion()) && StringUtils.isBlank(ic.getCdmiembrocompensador())) {
      logBo.insertarRegistroNewTransaction(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig(), msg);
      throw new NoAssigmentDataException(msg);
    }
    LOG.trace("[MovimientosSubListBo :: checkDataFormInformacionCompensacion] fin.");
  }

  /**
   * 
   * Devuelve la lista de movimientos que se van a grabar en bbdd
   * 
   * @param ic
   * @param alc
   * @param dc
   * @param dcts
   * @param corretaje
   * @return
   * @throws SIBBACBusinessException
   */
  private List<Tmct0movimientoecc> grabarMovimientosEccSinTitular(Tmct0infocompensacion ic, Tmct0alo alo,
      List<Tmct0ejecucionalocation> ejealos) throws SIBBACBusinessException {

    LOG.trace("[MovimientosSubListBo :: grabarMovimientosEccSinTitular] inicio.");
    List<Tmct0movimientoecc> movs = new ArrayList<Tmct0movimientoecc>();
    int countR01PorMessage = 0;
    Tmct0movimientoecc mov = null;
    Tmct0movimientooperacionnuevaecc movNueva = null;
    Tmct0eje eje;
    Tmct0operacioncdecc op;
    String cdrefmovimiento;
    for (Tmct0ejecucionalocation ejeAlo : ejealos) {
      eje = ejeAlo.getTmct0eje();
      op = eje.getTmct0caseoperacioneje().getTmct0operacioncdecc();
      if (op != null
          && (StringUtils.isBlank(ic.getCdctacompensacion()) || !op.getTmct0CuentasDeCompensacion().getCdCodigo()
              .trim().equals(ic.getCdctacompensacion().trim()))) {
        LOG.trace(
            "[MovimientosSubListBo :: grabarMovimientosEccSinTitular] ejeAlo.getNutitulos().compareTo(eje.getNutittotal()) == {}.",
            ejeAlo.getNutitulos().compareTo(eje.getNutittotal()));
        LOG.trace(
            "[MovimientosSubListBo :: grabarMovimientosEccSinTitular] countR01PorMessage++ % maxR01InMovimiento == 0 .",
            countR01PorMessage % maxR01InMovimiento == 0);
        if (ejeAlo.getNutitulos().compareTo(eje.getNutittotal()) != 0 || countR01PorMessage++ % maxR01InMovimiento == 0) {
          LOG.trace("[MovimientosSubListBo :: grabarMovimientosEccSinTitular] creamos MO nuevo.");
          mov = new Tmct0movimientoecc();
          cdrefmovimiento = movBo.getNewMOCdrefmovimientoFromMOSequence();
          movBo.inizializeTmct0movimientoEcc(ejeAlo, op, ic, cdrefmovimiento, alo.getId().getNucnfclt(), mov);
          mov.setTmct0alo(alo);
          mov = movBo.save(mov);

          logMovimientoCreadoSinTitular(alo, alo.getTmct0estadoByCdestadoasig(), mov);
          movs.add(mov);
        }
        LOG.trace("[MovimientosSubListBo :: grabarMovimientosEccSinTitular] creamos MO Op nueva.");
        movNueva = new Tmct0movimientooperacionnuevaecc();
        movNuevaBo.inizializeMovimientoOperacioNueva(ejeAlo, eje, op, movNueva);
        movNueva.setCdoperacionecc(op.getCdoperacionecc());
        movNueva.setTmct0movimientoecc(mov);
        movNueva = movNuevaBo.save(movNueva);
        addTitulosAndEfectivo(mov, movNueva);
        if (mov == null) {
          throw new SIBBACBusinessException(
              "INCIDENCIA-No hay registro Movimiento al que aniadir el codigo de operacion ecc.");
        }
        else {

          mov.getTmct0movimientooperacionnuevaeccs().add(movNueva);
          LOG.trace("[MovimientosSubListBo :: grabarMovimientosEccSinTitular] ops de mov {}.", mov
              .getTmct0movimientooperacionnuevaeccs().size());
        }
      }
      else {
        LOG.warn(
            "[MovimientosSubListBo :: grabarMovimientosEccSinTitular] el ejealo no sepuede enviar por op == null o la cta de compensacion destino es la misma que la cuenta de origen idEjealo: {}.",
            ejeAlo.getIdejecucionalocation());
      }
    }// fin for
    LOG.trace("[MovimientosSubListBo :: grabarMovimientosEccSinTitular] fin.");
    return movs;
  }

  private void logMovimientoCreadoSinTitular(Tmct0alo alo, Tmct0estado estado, Tmct0movimientoecc mov)
      throws SIBBACBusinessException {
    if (StringUtils.isNotBlank(mov.getCuentaCompensacionDestino()) || "15".equals(mov.getCdtipomov())) {
      logBo.insertarRegistro(
          alo,
          new Date(),
          new Date(),
          "",
          estado,
          "Creado  MO Sin Titular Asig. Ord. Int. RefMov: " + mov.getCdrefmovimiento() + " Cta: "
              + mov.getCuentaCompensacionDestino() + " Miembro: " + mov.getMiembroCompensadorDestino()
              + " Ref.Int.Asig: " + mov.getCdrefinternaasig());
    }
    else {
      logBo.insertarRegistro(
          alo,
          new Date(),
          new Date(),
          "",
          estado,
          "Creado  MO Sin Titular Asig. Ord. Int. RefMov: " + mov.getCdrefmovimiento() + " Miembro: "
              + mov.getMiembroCompensadorDestino() + " Ref.Asig: " + mov.getCodigoReferenciaAsignacion()
              + " Ref.Int.Asig: " + mov.getCdrefinternaasig());
    }
  }

  public List<MO> crearMensajesMoPti(List<Tmct0movimientoecc> movs, PTIMessageVersion version)
      throws PTIMessageException, SIBBACBusinessException {
    LOG.trace("[MovimientosSubListBo :: crearMensajesMoPti] inicio.");
    List<MO> msgs = new ArrayList<MO>();
    List<Tmct0movimientooperacionnuevaecc> movsn;
    MO mo;
    R00 r00;
    R01 r01;
    for (Tmct0movimientoecc mov : movs) {
      mo = (MO) PTIMessageFactory.getInstance(version, PTIMessageType.MO);
      fillHeaderMo(mov, mo);
      r00 = (R00) mo.getRecordInstance(PTIMessageRecordType.R00);
      fillR00Mo(mov, mo, r00);
      mo.addRecord(PTIMessageRecordType.R00, r00);
      movsn = mov.getTmct0movimientooperacionnuevaeccs();
      for (Tmct0movimientooperacionnuevaecc movn : movsn) {
        r01 = (R01) mo.getRecordInstance(PTIMessageRecordType.R01);
        fillR01Mo(mov, movn, r00, r01);
        mo.addRecord(PTIMessageRecordType.R01, r01);
      }
      msgs.add(mo);
    }
    LOG.trace("[MovimientosSubListBo :: crearMensajesMoPti] fin.");
    return msgs;
  }

  /**
   * @param dc
   * @param mo
   * @throws PTIMessageException
   */
  private void fillHeaderMo(Tmct0movimientoecc mov, PTIMessage mo) throws PTIMessageException {
    LOG.trace("[MovimientosSubListBo :: fillHeaderMo] fin.");
    MOCommonDataBlock commonDataBlock = (MOCommonDataBlock) mo.getCommonDataBlock();
    commonDataBlock.setFechaLiquidacion(mov.getFechaLiquidacion());
    if (mov.getSentido() == 'C') {
      commonDataBlock.setSentido("1");
    }
    else {
      commonDataBlock.setSentido("2");
    }
    LOG.trace("[MovimientosSubListBo :: fillHeaderMo] fin.");
  }

  /**
   * @param mov
   * @param dt
   * @param mo
   * @param r00
   * @throws PTIMessageException
   */
  private void fillR00Mo(Tmct0movimientoecc mov, MO mo, R00 r00) throws PTIMessageException {
    LOG.trace("[MovimientosSubListBo :: fillR00Mo] inicio.");
    r00.setNumeroDeValoresImporteNominal(mov.getImtitulos());
    r00.setEfectivo(mov.getImefectivo());
    r00.setCodigoValor(mov.getIsin());

    r00.setReferenciaInternaDeAsignacion(StringUtils.substring(mov.getCdrefinternaasig(), 0, 18));
    r00.setReferenciaDeMovimiento(mov.getCdrefmovimiento());
    r00.setCuentaCompensacionDestino(mov.getCuentaCompensacionDestino());
    r00.setReferenciaDeAsignacion(mov.getCodigoReferenciaAsignacion());
    r00.setMiembroDestino(mov.getMiembroCompensadorDestino());

    LOG.trace("[MovimientosSubListBo :: fillR00Mo] fin.");
  }

  /**
   * @param movn
   * @param r01
   * @throws PTIMessageException
   */
  private void fillR01Mo(Tmct0movimientoecc mov, Tmct0movimientooperacionnuevaecc movn, R00 r00, R01 r01)
      throws PTIMessageException {
    LOG.trace("[MovimientosSubListBo :: fillR01Mo] inicio.");
    if (StringUtils.isNotBlank(movn.getCdoperacionecc())) {
      r01.setNumeroDeOperacion(movn.getCdoperacionecc());
    }
    else if (StringUtils.isNotBlank(movn.getNuoperacionprev())) {
      r01.setNumeroDeOperacion(movn.getNuoperacionprev());
    }
    else {
      r01.setNumeroDeOperacion(movn.getNuoperacioninic());
    }

    if (movn.getCorretaje() != null) {
      r01.setCorretaje(movn.getCorretaje().setScale(2, BigDecimal.ROUND_HALF_UP));
    }
    else {
      r01.setCorretaje(BigDecimal.ZERO);
    }
    LOG.trace("[MovimientosSubListBo :: fillR01Mo] fin.");
  }

  private void addTitulosAndEfectivo(Tmct0movimientoecc mov, Tmct0movimientooperacionnuevaecc movn) {
    mov.setImtitulos(mov.getImtitulos().add(movn.getImtitulos()));
    mov.setImefectivo(mov.getImefectivo().add(movn.getImefectivo()));
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.MANDATORY)
  public CallableResultDTO crearMovimientosSinEnviarCuentaDestinoIgualOrigen(Tmct0alcId alcId, Tmct0desglosecamara dc,
      List<Tmct0desgloseclitit> listDesgloses, TraspasosEccConfigDTO config) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    if (CollectionUtils.isNotEmpty(listDesgloses)) {

      List<Tmct0movimientoecc> movimientosEccDefinitivos = new ArrayList<Tmct0movimientoecc>();
      Collection<Long> idsDesgloseCamara = new HashSet<Long>();
      idsDesgloseCamara.add(dc.getIddesglosecamara());
      List<Tmct0desglosecamara> listDesglosesCamara = new ArrayList<Tmct0desglosecamara>();
      listDesglosesCamara.add(dc);
      // creamos los registros de movimiento
      movimientosEccDefinitivos.addAll(movBo.crearMovimientosOrigenDestinoIgual(dc, listDesgloses,
          config.getAuditUser()));

      if (CollectionUtils.isNotEmpty(movimientosEccDefinitivos)) {

        movBo.save(movimientosEccDefinitivos);

        BigDecimal titulosTraspasos = BigDecimal.ZERO;
        for (Tmct0movimientoecc mov : movimientosEccDefinitivos) {
          titulosTraspasos = titulosTraspasos.add(mov.getImtitulos());
        }

        this.marcarMovimientosAsignacionInternaOrden(movimientosEccDefinitivos);

        // salvamos los datos
        this.saveStateSentTraspasosSinEnviar(alcId, listDesglosesCamara, titulosTraspasos, movimientosEccDefinitivos,
            config, res);
      }
    }
    return res;
  }

  public void sendMensajesOnline(ConnectionFactory cf, List<PTIMessage> msgs) throws JMSException, PTIMessageException,
      SIBBACBusinessException {
    if (CollectionUtils.isNotEmpty(msgs)) {
      try (Connection c = jmsMessageBo.getNewConnection(cf)) {
        c.start();
        try (Session s = jmsMessageBo.getNewSession(c, true);) {
          try {
            jmsMessageBo.sendPtiMessageOnline(s, moDestinationSt, msgs);
            s.commit();
          }
          catch (Exception e) {
            LOG.warn(e.getMessage(), e);
            if (s != null) {
              s.rollback();
            }
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
        }
      }
      for (PTIMessage ptiMessage : msgs) {
        LOG_PTI_OUT.info(ptiMessage.toPTIFormat());
      }

    }
  }
}
