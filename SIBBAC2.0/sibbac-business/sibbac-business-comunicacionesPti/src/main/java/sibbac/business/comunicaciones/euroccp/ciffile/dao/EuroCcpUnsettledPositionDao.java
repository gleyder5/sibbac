package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpUnsettledPosition;

@Component
public interface EuroCcpUnsettledPositionDao extends JpaRepository<EuroCcpUnsettledPosition, Long> {

  EuroCcpUnsettledPosition findByProcessingDateAndClientNumberAndAccountNumberAndSubaccountNumberAndExchangeCodeTradeAndSymbolAndValuationPrice(
      Date processingDate, String clientNumber, String accountNumber, String subaccountNumber,
      String exchangeCodeTrade, String symbol, BigDecimal valuationPrice);

}
