package sibbac.business.comunicaciones.pti.cuadreecc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;

@Table(name = DBConstants.CUADRE_ECC.CUADRE_ECC_EJECUCIONES)
@Entity
@XmlRootElement
// @Audited
public class CuadreEccEjecucion extends ATable<CuadreEccReferencia> implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7172187135017594750L;

  @Column(name = "ESTADO_PROCESADO")
  @XmlAttribute
  private Integer estadoProcesado;

  @Column(name = "MIEMBRO_PROPIETARIO_CUENTA", length = 4)
  @XmlAttribute
  private String miembroPropietarioCuenta;

  @Column(name = "CUENTA_COMPENSACION_MIEMBRO", length = 3)
  @XmlAttribute
  private String cuentaCompensacionMiembro;

  @Column(name = "MIEMBRO_COMPENSADOR_CUENTA", length = 4)
  @XmlAttribute
  private String miembroCompensadorCuenta;

  @Column(name = "ENT_PARTICIPANTE_MIEMBRO", length = 11)
  @XmlAttribute
  private String entParticipanteMiembro;

  @Column(name = "TIPO_CUENTA_LIQUIDACION_MIEMBRO", length = 2)
  @XmlAttribute
  private String tipoCuentaLiquidacionMiembro;

  @Column(name = "CUENTA_LIQUIDACION_MIEMBRO", length = 35)
  @XmlAttribute
  private String cuentaLiquidacionMiembro;

  @Column(name = "IND_ANOTACION", length = 1)
  @XmlAttribute
  private Character indAnotacion;

  @Column(name = "NUM_OP", length = 16)
  @XmlAttribute
  private String numOp;

  @Column(name = "IND_POSICION", length = 1)
  @XmlAttribute
  private Character indPosicion;

  @Column(name = "COD_OP", length = 1)
  @XmlAttribute
  private Character codOp;

  @Column(name = "REF_ASIGNACION_EXT", length = 18)
  @XmlAttribute
  private String refAsignacionExt;

  @Column(name = "MNEMOTECNICO", length = 10)
  @XmlAttribute
  private String mnemotecnico;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_CONTRATACION")
  @XmlAttribute
  private Date fechaContratacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_LIQ")
  @XmlAttribute
  private Date fechaLiq;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_REGISTRO_ECC")
  @XmlAttribute
  private Date fechaRegistroEcc;

  @Temporal(TemporalType.TIME)
  @Column(name = "HORA_REGISTRO")
  @XmlAttribute
  private Date horaRegistro;

  @Column(name = "VALORES_IMP_NOMINAL", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal valoresImpNominal;

  @Column(name = "NUM_VALORES_IMP_NOMINAL_PENDIENTE_CUADRE", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal valoresImpNominalPendienteCuadre;

  @Column(name = "DIVISA", length = 3)
  @XmlAttribute
  private String divisa;

  @Column(name = "VALORES_IMP_NOMINAL_DISPONIBLE", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal valoresImpNominalDisponible;

  @Column(name = "EFECTIVO_DISPONIBLE", length = 16, scale = 2)
  @XmlAttribute
  private BigDecimal efectivoDisponible;

  @Column(name = "VALORES_IMP_NOMINAL_RETENIDO", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal valoresImpNominalRetenido;

  @Column(name = "EFECTIVO_RETENIDO", length = 16, scale = 2)
  @XmlAttribute
  private BigDecimal efectivoRetenido;

  @Column(name = "NUM_OP_PREVIA", length = 16)
  @XmlAttribute
  private String numOpPrevia;

  @Column(name = "NUM_OP_INICIAL", length = 16)
  @XmlAttribute
  private String numOpInicial;

  @Column(name = "REF_COMUN", length = 16)
  @XmlAttribute
  private String refComun;

  @Column(name = "CORRETAJE", length = 16, scale = 2)
  @XmlAttribute
  private BigDecimal corretaje;

  @Column(name = "PLATAFORMA_NEG", length = 4)
  @XmlAttribute
  private String plataformaNeg;

  @Column(name = "SEGMENTO_NEG", length = 2)
  @XmlAttribute
  private String segmentoNeg;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_NEG")
  @XmlAttribute
  private Date fechaNeg;

  @Temporal(TemporalType.TIME)
  @Column(name = "HORA_NEG")
  @XmlAttribute
  private Date horaNeg;

  @Column(name = "NUM_EJE_MERCADO", length = 32)
  @XmlAttribute
  private String numEjeMercado;

  @Column(name = "COD_OP_MERCADO", length = 2)
  @XmlAttribute
  private String codOpMercado;

  @Column(name = "MIEMBRO_MERCADO_NEG", length = 4)
  @XmlAttribute
  private String miembroMercadoNeg;

  @Column(name = "IND_COTIZACION", length = 1)
  @XmlAttribute
  private Character indCotizacion;

  @Column(name = "ISIN", length = 12)
  @XmlAttribute
  private String isin;

  @Column(name = "SENTIDO", length = 1)
  @XmlAttribute
  private Character sentido;

  @Column(name = "PRECIO", length = 13, scale = 6)
  @XmlAttribute
  private BigDecimal precio;

  @Column(name = "EFECTIVO", length = 16, scale = 2)
  @XmlAttribute
  private BigDecimal efectivo;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_ORDEN_MERCADO")
  @XmlAttribute
  private Date fechaOrdenMercado;

  @Temporal(TemporalType.TIME)
  @Column(name = "HORA_ORDEN_MERCADO")
  @XmlAttribute
  private Date horaOrdenMercado;

  @Column(name = "NUM_ORDEN_MERCADO", length = 32)
  @XmlAttribute
  private String numOrdenMercado;

  @Column(name = "USUARIO", length = 3)
  @XmlAttribute
  private String usuario;

  @Column(name = "REF_CLIENTE", length = 16)
  @XmlAttribute
  private String refCliente;

  @Column(name = "REF_EXTERNA", length = 15)
  @XmlAttribute
  private String refExterna;

  @Column(name = "IND_CAPACIDAD", length = 1)
  @XmlAttribute
  private Character indCapacidad;

  @Column(name = "INF_ADICIONAL_ORD", length = 80)
  @XmlAttribute
  private String infAdicionalOrd;

  @Column(name = "CICLO_LIQUIDACION", length = 4)
  @XmlAttribute
  private String cicloLiquidacion;

  @Column(name = "TIPO_INSTRUCCION_LIQUIDACION", length = 4)
  @XmlAttribute
  private String tipoInstruccionLiquidacion;

  @Column(name = "REF_EVENTO_CORPORATIVO", length = 35)
  @XmlAttribute
  private String refEventoCorporativo;

  @Column(name = "ID_PLATAFORMA_NEG", length = 11)
  @XmlAttribute
  private String idPlataformaNeg;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_OP")
  @XmlAttribute
  private Date fechaOp;

  @Column(name = "REF_OP_DCV", length = 35)
  @XmlAttribute
  private String refOpDcv;

  @Column(name = "ENT_PARTICIPANTE_PLATAFORMA", length = 11)
  @XmlAttribute
  private String entParticipantePlataforma;

  @Temporal(TemporalType.DATE)
  @Column(name = "FECHA_CASE")
  @XmlAttribute
  private Date fechaCase;

  @Temporal(TemporalType.TIME)
  @Column(name = "HORA_CASE")
  @XmlAttribute
  private Date horaCase;

  @Column(name = "TIPO_OP_PLATAFORMA", length = 4)
  @XmlAttribute
  private String tipoOpPlataforma;

  @Column(name = "TIPO_CUENTA_LIQUIDACION_EJE", length = 2)
  @XmlAttribute
  private String tipoCuentaLiquidacionEje;

  @Column(name = "CUENTA_LIQUIDACION_EJE", length = 35)
  @XmlAttribute
  private String cuentaLiquidacionEje;

  @Column(name = "VALORES_IMP_NOMINAL_EJE", length = 16, scale = 6)
  @XmlAttribute
  private BigDecimal valoresImpNominalEje;

  @Column(name = "ORDENANTE_PARTICIPANTE", length = 35)
  @XmlAttribute
  private String ordenanteParticipante;

  @Column(name = "COD_PARTICIPANTE", length = 42)
  @XmlAttribute
  private String codParticipante;

  @Column(name = "CCV", length = 35)
  @XmlAttribute
  private String ccv;

  @Column(name = "LIQUIDACION_PARCIAL", length = 4)
  @XmlAttribute
  private String liquidacionParcial;

  @Column(name = "LIQUIDACION_TIEMPO_REAL", length = 4)
  @XmlAttribute
  private String liquidacionTiempoReal;

  @Column(name = "IND_INTERVENCICION_ECC", length = 1)
  @XmlAttribute
  private Character indIntervencicionEcc;

  @Column(name = "MIEMBRO_COMPENSADOR_ECC", length = 11)
  @XmlAttribute
  private String miembroCompensadorEcc;

  @Column(name = "TIPO_CUENTA_COMPENSACION_ECC", length = 2)
  @XmlAttribute
  private String tipoCuentaCompensacionEcc;

  @Column(name = "CUENTA_COMPENSACION_ECC", length = 35)
  @XmlAttribute
  private String cuentaCompensacionEcc;

  @Column(name = "ID_ECC", length = 11)
  @XmlAttribute
  private String idEcc;

  @Column(name = "SEGMENTO_ECC", length = 2)
  @XmlAttribute
  private String segmentoEcc;

  @Column(name = "CANON_TEORICO", length = 15, scale = 2)
  @XmlAttribute
  private BigDecimal canonTeorico;

  @Column(name = "CANON_APLICADO", length = 15, scale = 2)
  @XmlAttribute
  private BigDecimal canonAplicado;

  @Column(name = "SITUACION_OP", length = 1)
  @XmlAttribute
  private Character situacionOp;

  @Column(name = "TITULOS_FALLIDOS_OP", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal titulosFallidosOp;

  @Column(name = "NUM_OP_DCV", length = 35)
  @XmlAttribute
  private String numOpDcv;

  @Column(name = "NUM_OP_IL_ECC", length = 35)
  @XmlAttribute
  private String numOpIlEcc;

  @Column(name = "TIPO_OP_DCV", length = 4)
  @XmlAttribute
  private String tipoOpDcv;

  @Column(name = "ESTADO_IL", length = 4)
  @XmlAttribute
  private String estadoIl;

  @Column(name = "TITULOS_OP_IL", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal titulosOpIl;

  @Column(name = "EFECTIVO_OP_IL", length = 16, scale = 2)
  @XmlAttribute
  private BigDecimal efectivoOpIl;

  @Column(name = "TITULOS_FALLIDOS_OP_IL", length = 18, scale = 6)
  @XmlAttribute
  private BigDecimal titulosFallidosOpIl;

  public CuadreEccEjecucion() {
  }

  public Integer getEstadoProcesado() {
    return estadoProcesado;
  }

  public String getMiembroPropietarioCuenta() {
    return miembroPropietarioCuenta;
  }

  public String getCuentaCompensacionMiembro() {
    return cuentaCompensacionMiembro;
  }

  public String getMiembroCompensadorCuenta() {
    return miembroCompensadorCuenta;
  }

  public String getEntParticipanteMiembro() {
    return entParticipanteMiembro;
  }

  public String getTipoCuentaLiquidacionMiembro() {
    return tipoCuentaLiquidacionMiembro;
  }

  public String getCuentaLiquidacionMiembro() {
    return cuentaLiquidacionMiembro;
  }

  public Character getIndAnotacion() {
    return indAnotacion;
  }

  public String getNumOp() {
    return numOp;
  }

  public Character getIndPosicion() {
    return indPosicion;
  }

  public Character getCodOp() {
    return codOp;
  }

  public String getRefAsignacionExt() {
    return refAsignacionExt;
  }

  public String getMnemotecnico() {
    return mnemotecnico;
  }

  public Date getFechaContratacion() {
    return fechaContratacion;
  }

  public Date getFechaLiq() {
    return fechaLiq;
  }

  public Date getFechaRegistroEcc() {
    return fechaRegistroEcc;
  }

  public Date getHoraRegistro() {
    return horaRegistro;
  }

  public BigDecimal getValoresImpNominal() {
    return valoresImpNominal;
  }

  public BigDecimal getValoresImpNominalPendienteCuadre() {
    return valoresImpNominalPendienteCuadre;
  }

  public String getDivisa() {
    return divisa;
  }

  public BigDecimal getValoresImpNominalDisponible() {
    return valoresImpNominalDisponible;
  }

  public BigDecimal getEfectivoDisponible() {
    return efectivoDisponible;
  }

  public BigDecimal getValoresImpNominalRetenido() {
    return valoresImpNominalRetenido;
  }

  public BigDecimal getEfectivoRetenido() {
    return efectivoRetenido;
  }

  public String getNumOpPrevia() {
    return numOpPrevia;
  }

  public String getNumOpInicial() {
    return numOpInicial;
  }

  public String getRefComun() {
    return refComun;
  }

  public BigDecimal getCorretaje() {
    return corretaje;
  }

  public String getPlataformaNeg() {
    return plataformaNeg;
  }

  public String getSegmentoNeg() {
    return segmentoNeg;
  }

  public Date getFechaNeg() {
    return fechaNeg;
  }

  public Date getHoraNeg() {
    return horaNeg;
  }

  public String getNumEjeMercado() {
    return numEjeMercado;
  }

  public String getCodOpMercado() {
    return codOpMercado;
  }

  public String getMiembroMercadoNeg() {
    return miembroMercadoNeg;
  }

  public Character getIndCotizacion() {
    return indCotizacion;
  }

  public String getIsin() {
    return isin;
  }

  public Character getSentido() {
    return sentido;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public BigDecimal getEfectivo() {
    return efectivo;
  }

  public Date getFechaOrdenMercado() {
    return fechaOrdenMercado;
  }

  public Date getHoraOrdenMercado() {
    return horaOrdenMercado;
  }

  public String getNumOrdenMercado() {
    return numOrdenMercado;
  }

  public String getUsuario() {
    return usuario;
  }

  public String getRefCliente() {
    return refCliente;
  }

  public String getRefExterna() {
    return refExterna;
  }

  public Character getIndCapacidad() {
    return indCapacidad;
  }

  public String getInfAdicionalOrd() {
    return infAdicionalOrd;
  }

  public String getCicloLiquidacion() {
    return cicloLiquidacion;
  }

  public String getTipoInstruccionLiquidacion() {
    return tipoInstruccionLiquidacion;
  }

  public String getRefEventoCorporativo() {
    return refEventoCorporativo;
  }

  public String getIdPlataformaNeg() {
    return idPlataformaNeg;
  }

  public Date getFechaOp() {
    return fechaOp;
  }

  public String getRefOpDcv() {
    return refOpDcv;
  }

  public String getEntParticipantePlataforma() {
    return entParticipantePlataforma;
  }

  public Date getFechaCase() {
    return fechaCase;
  }

  public Date getHoraCase() {
    return horaCase;
  }

  public String getTipoOpPlataforma() {
    return tipoOpPlataforma;
  }

  public String getTipoCuentaLiquidacionEje() {
    return tipoCuentaLiquidacionEje;
  }

  public String getCuentaLiquidacionEje() {
    return cuentaLiquidacionEje;
  }

  public BigDecimal getValoresImpNominalEje() {
    return valoresImpNominalEje;
  }

  public String getOrdenanteParticipante() {
    return ordenanteParticipante;
  }

  public String getCodParticipante() {
    return codParticipante;
  }

  public String getCcv() {
    return ccv;
  }

  public String getLiquidacionParcial() {
    return liquidacionParcial;
  }

  public String getLiquidacionTiempoReal() {
    return liquidacionTiempoReal;
  }

  public Character getIndIntervencicionEcc() {
    return indIntervencicionEcc;
  }

  public String getMiembroCompensadorEcc() {
    return miembroCompensadorEcc;
  }

  public String getTipoCuentaCompensacionEcc() {
    return tipoCuentaCompensacionEcc;
  }

  public String getCuentaCompensacionEcc() {
    return cuentaCompensacionEcc;
  }

  public String getIdEcc() {
    return idEcc;
  }

  public String getSegmentoEcc() {
    return segmentoEcc;
  }

  public BigDecimal getCanonTeorico() {
    return canonTeorico;
  }

  public BigDecimal getCanonAplicado() {
    return canonAplicado;
  }

  public Character getSituacionOp() {
    return situacionOp;
  }

  public BigDecimal getTitulosFallidosOp() {
    return titulosFallidosOp;
  }

  public String getNumOpDcv() {
    return numOpDcv;
  }

  public String getNumOpIlEcc() {
    return numOpIlEcc;
  }

  public String getTipoOpDcv() {
    return tipoOpDcv;
  }

  public String getEstadoIl() {
    return estadoIl;
  }

  public BigDecimal getTitulosOpIl() {
    return titulosOpIl;
  }

  public BigDecimal getEfectivoOpIl() {
    return efectivoOpIl;
  }

  public BigDecimal getTitulosFallidosOpIl() {
    return titulosFallidosOpIl;
  }

  public void setEstadoProcesado(Integer estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setMiembroPropietarioCuenta(String miembroPropietarioCuenta) {
    this.miembroPropietarioCuenta = miembroPropietarioCuenta;
  }

  public void setCuentaCompensacionMiembro(String cuentaCompensacionMiembro) {
    this.cuentaCompensacionMiembro = cuentaCompensacionMiembro;
  }

  public void setMiembroCompensadorCuenta(String miembroCompensadorCuenta) {
    this.miembroCompensadorCuenta = miembroCompensadorCuenta;
  }

  public void setEntParticipanteMiembro(String entParticipanteMiembro) {
    this.entParticipanteMiembro = entParticipanteMiembro;
  }

  public void setTipoCuentaLiquidacionMiembro(String tipoCuentaLiquidacionMiembro) {
    this.tipoCuentaLiquidacionMiembro = tipoCuentaLiquidacionMiembro;
  }

  public void setCuentaLiquidacionMiembro(String cuentaLiquidacionMiembro) {
    this.cuentaLiquidacionMiembro = cuentaLiquidacionMiembro;
  }

  public void setIndAnotacion(Character indAnotacion) {
    this.indAnotacion = indAnotacion;
  }

  public void setNumOp(String numOp) {
    this.numOp = numOp;
  }

  public void setIndPosicion(Character indPosicion) {
    this.indPosicion = indPosicion;
  }

  public void setCodOp(Character codOp) {
    this.codOp = codOp;
  }

  public void setRefAsignacionExt(String refAsignacionExt) {
    this.refAsignacionExt = refAsignacionExt;
  }

  public void setMnemotecnico(String mnemotecnico) {
    this.mnemotecnico = mnemotecnico;
  }

  public void setFechaContratacion(Date fechaContratacion) {
    this.fechaContratacion = fechaContratacion;
  }

  public void setFechaLiq(Date fechaLiq) {
    this.fechaLiq = fechaLiq;
  }

  public void setFechaRegistroEcc(Date fechaRegistroEcc) {
    this.fechaRegistroEcc = fechaRegistroEcc;
  }

  public void setHoraRegistro(Date horaRegistro) {
    this.horaRegistro = horaRegistro;
  }

  public void setValoresImpNominal(BigDecimal valoresImpNominal) {
    this.valoresImpNominal = valoresImpNominal;
  }

  public void setValoresImpNominalPendienteCuadre(BigDecimal valoresImpNominalPendienteCuadre) {
    this.valoresImpNominalPendienteCuadre = valoresImpNominalPendienteCuadre;
  }

  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  public void setValoresImpNominalDisponible(BigDecimal valoresImpNominalDisponible) {
    this.valoresImpNominalDisponible = valoresImpNominalDisponible;
  }

  public void setEfectivoDisponible(BigDecimal efectivoDisponible) {
    this.efectivoDisponible = efectivoDisponible;
  }

  public void setValoresImpNominalRetenido(BigDecimal valoresImpNominalRetenido) {
    this.valoresImpNominalRetenido = valoresImpNominalRetenido;
  }

  public void setEfectivoRetenido(BigDecimal efectivoRetenido) {
    this.efectivoRetenido = efectivoRetenido;
  }

  public void setNumOpPrevia(String numOpPrevia) {
    this.numOpPrevia = numOpPrevia;
  }

  public void setNumOpInicial(String numOpInicial) {
    this.numOpInicial = numOpInicial;
  }

  public void setRefComun(String refComun) {
    this.refComun = refComun;
  }

  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }

  public void setPlataformaNeg(String plataformaNeg) {
    this.plataformaNeg = plataformaNeg;
  }

  public void setSegmentoNeg(String segmentoNeg) {
    this.segmentoNeg = segmentoNeg;
  }

  public void setFechaNeg(Date fechaNeg) {
    this.fechaNeg = fechaNeg;
  }

  public void setHoraNeg(Date horaNeg) {
    this.horaNeg = horaNeg;
  }

  public void setNumEjeMercado(String numEjeMercado) {
    this.numEjeMercado = numEjeMercado;
  }

  public void setCodOpMercado(String codOpMercado) {
    this.codOpMercado = codOpMercado;
  }

  public void setMiembroMercadoNeg(String miembroMercadoNeg) {
    this.miembroMercadoNeg = miembroMercadoNeg;
  }

  public void setIndCotizacion(Character indCotizacion) {
    this.indCotizacion = indCotizacion;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  public void setFechaOrdenMercado(Date fechaOrdenMercado) {
    this.fechaOrdenMercado = fechaOrdenMercado;
  }

  public void setHoraOrdenMercado(Date horaOrdenMercado) {
    this.horaOrdenMercado = horaOrdenMercado;
  }

  public void setNumOrdenMercado(String numOrdenMercado) {
    this.numOrdenMercado = numOrdenMercado;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  public void setRefCliente(String refCliente) {
    this.refCliente = refCliente;
  }

  public void setRefExterna(String refExterna) {
    this.refExterna = refExterna;
  }

  public void setIndCapacidad(Character indCapacidad) {
    this.indCapacidad = indCapacidad;
  }

  public void setInfAdicionalOrd(String infAdicionalOrd) {
    this.infAdicionalOrd = infAdicionalOrd;
  }

  public void setCicloLiquidacion(String cicloLiquidacion) {
    this.cicloLiquidacion = cicloLiquidacion;
  }

  public void setTipoInstruccionLiquidacion(String tipoInstruccionLiquidacion) {
    this.tipoInstruccionLiquidacion = tipoInstruccionLiquidacion;
  }

  public void setRefEventoCorporativo(String refEventoCorporativo) {
    this.refEventoCorporativo = refEventoCorporativo;
  }

  public void setIdPlataformaNeg(String idPlataformaNeg) {
    this.idPlataformaNeg = idPlataformaNeg;
  }

  public void setFechaOp(Date fechaOp) {
    this.fechaOp = fechaOp;
  }

  public void setRefOpDcv(String refOpDcv) {
    this.refOpDcv = refOpDcv;
  }

  public void setEntParticipantePlataforma(String entParticipantePlataforma) {
    this.entParticipantePlataforma = entParticipantePlataforma;
  }

  public void setFechaCase(Date fechaCase) {
    this.fechaCase = fechaCase;
  }

  public void setHoraCase(Date horaCase) {
    this.horaCase = horaCase;
  }

  public void setTipoOpPlataforma(String tipoOpPlataforma) {
    this.tipoOpPlataforma = tipoOpPlataforma;
  }

  public void setTipoCuentaLiquidacionEje(String tipoCuentaLiquidacionEje) {
    this.tipoCuentaLiquidacionEje = tipoCuentaLiquidacionEje;
  }

  public void setCuentaLiquidacionEje(String cuentaLiquidacionEje) {
    this.cuentaLiquidacionEje = cuentaLiquidacionEje;
  }

  public void setValoresImpNominalEje(BigDecimal valoresImpNominalEje) {
    this.valoresImpNominalEje = valoresImpNominalEje;
  }

  public void setOrdenanteParticipante(String ordenanteParticipante) {
    this.ordenanteParticipante = ordenanteParticipante;
  }

  public void setCodParticipante(String codParticipante) {
    this.codParticipante = codParticipante;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  public void setLiquidacionParcial(String liquidacionParcial) {
    this.liquidacionParcial = liquidacionParcial;
  }

  public void setLiquidacionTiempoReal(String liquidacionTiempoReal) {
    this.liquidacionTiempoReal = liquidacionTiempoReal;
  }

  public void setIndIntervencicionEcc(Character indIntervencicionEcc) {
    this.indIntervencicionEcc = indIntervencicionEcc;
  }

  public void setMiembroCompensadorEcc(String miembroCompensadorEcc) {
    this.miembroCompensadorEcc = miembroCompensadorEcc;
  }

  public void setTipoCuentaCompensacionEcc(String tipoCuentaCompensacionEcc) {
    this.tipoCuentaCompensacionEcc = tipoCuentaCompensacionEcc;
  }

  public void setCuentaCompensacionEcc(String cuentaCompensacionEcc) {
    this.cuentaCompensacionEcc = cuentaCompensacionEcc;
  }

  public void setIdEcc(String idEcc) {
    this.idEcc = idEcc;
  }

  public void setSegmentoEcc(String segmentoEcc) {
    this.segmentoEcc = segmentoEcc;
  }

  public void setCanonTeorico(BigDecimal canonTeorico) {
    this.canonTeorico = canonTeorico;
  }

  public void setCanonAplicado(BigDecimal canonAplicado) {
    this.canonAplicado = canonAplicado;
  }

  public void setSituacionOp(Character situacionOp) {
    this.situacionOp = situacionOp;
  }

  public void setTitulosFallidosOp(BigDecimal titulosFallidosOp) {
    this.titulosFallidosOp = titulosFallidosOp;
  }

  public void setNumOpDcv(String numOpDcv) {
    this.numOpDcv = numOpDcv;
  }

  public void setNumOpIlEcc(String numOpIlEcc) {
    this.numOpIlEcc = numOpIlEcc;
  }

  public void setTipoOpDcv(String tipoOpDcv) {
    this.tipoOpDcv = tipoOpDcv;
  }

  public void setEstadoIl(String estadoIl) {
    this.estadoIl = estadoIl;
  }

  public void setTitulosOpIl(BigDecimal titulosOpIl) {
    this.titulosOpIl = titulosOpIl;
  }

  public void setEfectivoOpIl(BigDecimal efectivoOpIl) {
    this.efectivoOpIl = efectivoOpIl;
  }

  public void setTitulosFallidosOpIl(BigDecimal titulosFallidosOpIl) {
    this.titulosFallidosOpIl = titulosFallidosOpIl;
  }

  @Override
  public String toString() {
    return "CuadreEccEjecucion [estadoProcesado=" + estadoProcesado + ", miembroPropietarioCuenta="
           + miembroPropietarioCuenta + ", cuentaCompensacionMiembro=" + cuentaCompensacionMiembro
           + ", miembroCompensadorCuenta=" + miembroCompensadorCuenta + ", entParticipanteMiembro="
           + entParticipanteMiembro + ", tipoCuentaLiquidacionMiembro=" + tipoCuentaLiquidacionMiembro
           + ", cuentaLiquidacionMiembro=" + cuentaLiquidacionMiembro + ", indAnotacion=" + indAnotacion + ", numOp="
           + numOp + ", indPosicion=" + indPosicion + ", codOp=" + codOp + ", refAsignacionExt=" + refAsignacionExt
           + ", mnemotecnico=" + mnemotecnico + ", fechaContratacion=" + fechaContratacion + ", fechaLiq=" + fechaLiq
           + ", fechaRegistroEcc=" + fechaRegistroEcc + ", horaRegistro=" + horaRegistro + ", valoresImpNominal="
           + valoresImpNominal + ", valoresImpNominalPendienteCuadre=" + valoresImpNominalPendienteCuadre + ", divisa="
           + divisa + ", valoresImpNominalDisponible=" + valoresImpNominalDisponible + ", efectivoDisponible="
           + efectivoDisponible + ", valoresImpNominalRetenido=" + valoresImpNominalRetenido + ", efectivoRetenido="
           + efectivoRetenido + ", numOpPrevia=" + numOpPrevia + ", numOpInicial=" + numOpInicial + ", refComun="
           + refComun + ", corretaje=" + corretaje + ", plataformaNeg=" + plataformaNeg + ", segmentoNeg="
           + segmentoNeg + ", fechaNeg=" + fechaNeg + ", horaNeg=" + horaNeg + ", numEjeMercado=" + numEjeMercado
           + ", codOpMercado=" + codOpMercado + ", miembroMercadoNeg=" + miembroMercadoNeg + ", indCotizacion="
           + indCotizacion + ", isin=" + isin + ", sentido=" + sentido + ", precio=" + precio + ", efectivo="
           + efectivo + ", fechaOrdenMercado=" + fechaOrdenMercado + ", horaOrdenMercado=" + horaOrdenMercado
           + ", numOrdenMercado=" + numOrdenMercado + ", usuario=" + usuario + ", refCliente=" + refCliente
           + ", refExterna=" + refExterna + ", indCapacidad=" + indCapacidad + ", infAdicionalOrd=" + infAdicionalOrd
           + ", cicloLiquidacion=" + cicloLiquidacion + ", tipoInstruccionLiquidacion=" + tipoInstruccionLiquidacion
           + ", refEventoCorporativo=" + refEventoCorporativo + ", idPlataformaNeg=" + idPlataformaNeg + ", fechaOp="
           + fechaOp + ", refOpDcv=" + refOpDcv + ", entParticipantePlataforma=" + entParticipantePlataforma
           + ", fechaCase=" + fechaCase + ", horaCase=" + horaCase + ", tipoOpPlataforma=" + tipoOpPlataforma
           + ", tipoCuentaLiquidacionEje=" + tipoCuentaLiquidacionEje + ", cuentaLiquidacionEje="
           + cuentaLiquidacionEje + ", valoresImpNominalEje=" + valoresImpNominalEje + ", ordenanteParticipante="
           + ordenanteParticipante + ", codParticipante=" + codParticipante + ", ccv=" + ccv + ", liquidacionParcial="
           + liquidacionParcial + ", liquidacionTiempoReal=" + liquidacionTiempoReal + ", indIntervencicionEcc="
           + indIntervencicionEcc + ", miembroCompensadorEcc=" + miembroCompensadorEcc + ", tipoCuentaCompensacionEcc="
           + tipoCuentaCompensacionEcc + ", cuentaCompensacionEcc=" + cuentaCompensacionEcc + ", idEcc=" + idEcc
           + ", segmentoEcc=" + segmentoEcc + ", canonTeorico=" + canonTeorico + ", canonAplicado=" + canonAplicado
           + ", situacionOp=" + situacionOp + ", titulosFallidosOp=" + titulosFallidosOp + ", numOpDcv=" + numOpDcv
           + ", numOpIlEcc=" + numOpIlEcc + ", tipoOpDcv=" + tipoOpDcv + ", estadoIl=" + estadoIl + ", titulosOpIl="
           + titulosOpIl + ", efectivoOpIl=" + efectivoOpIl + ", titulosFallidosOpIl=" + titulosFallidosOpIl + "]";
  }

}
