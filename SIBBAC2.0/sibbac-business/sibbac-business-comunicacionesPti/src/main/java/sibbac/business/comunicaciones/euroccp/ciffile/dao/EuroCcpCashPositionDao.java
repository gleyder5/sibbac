package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpCashPosition;

@Component
public interface EuroCcpCashPositionDao extends JpaRepository<EuroCcpCashPosition, Long> {

  EuroCcpCashPosition findByProcessingDateAndClientNumberAndAccountNumberAndSubaccountNumberAndCashAmountIdentifier(
      Date processingDate, String clientNumber, String accountNumber, String subaccountNumber,
      String cashAmountIdentifier);

}