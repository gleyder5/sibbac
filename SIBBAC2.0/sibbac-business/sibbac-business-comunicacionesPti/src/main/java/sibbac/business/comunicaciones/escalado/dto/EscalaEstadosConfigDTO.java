package sibbac.business.comunicaciones.escalado.dto;

import java.io.Serializable;
import java.util.Map;

public class EscalaEstadosConfigDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4855482105059038155L;

  private String sqlAllocation;
  private String sqlAllocationToClearer;
  private String sqlDesgloseCamara;
  private String sqlAllocationToClearerGrouping;
  private String sqlAllocationGrouping;
  private Long tiempoMaxProcesoBok;
  private Long tiempoMaxProcesoAlo;
  private Long tiempoMaxProcesoAlc;
  private Integer estadoIncidencias;
  private Integer estadoRechazado;
  private Integer estadoEnCurso;
  private Integer estadoTratandose;
  private String nombreCampoEstado;

  private Map<String, Integer> estadosEquivalentes;

  private boolean isScriptDividend;

  // private TIPO_TAREA semaforo;

  public EscalaEstadosConfigDTO() {
  }

  public String getSqlAllocation() {
    return sqlAllocation;
  }

  public String getSqlAllocationToClearer() {
    return sqlAllocationToClearer;
  }

  public String getSqlDesgloseCamara() {
    return sqlDesgloseCamara;
  }

  public String getSqlAllocationToClearerGrouping() {
    return sqlAllocationToClearerGrouping;
  }

  public String getSqlAllocationGrouping() {
    return sqlAllocationGrouping;
  }

  public Long getTiempoMaxProcesoBok() {
    return tiempoMaxProcesoBok;
  }

  public Long getTiempoMaxProcesoAlo() {
    return tiempoMaxProcesoAlo;
  }

  public Long getTiempoMaxProcesoAlc() {
    return tiempoMaxProcesoAlc;
  }

  public Integer getEstadoIncidencias() {
    return estadoIncidencias;
  }

  public Integer getEstadoRechazado() {
    return estadoRechazado;
  }

  public Integer getEstadoEnCurso() {
    return estadoEnCurso;
  }

  public Integer getEstadoTratandose() {
    return estadoTratandose;
  }

  public String getNombreCampoEstado() {
    return nombreCampoEstado;
  }

  public void setSqlAllocation(String sqlAllocation) {
    this.sqlAllocation = sqlAllocation;
  }

  public void setSqlAllocationToClearer(String sqlAllocationToClearer) {
    this.sqlAllocationToClearer = sqlAllocationToClearer;
  }

  public void setSqlDesgloseCamara(String sqlDesgloseCamara) {
    this.sqlDesgloseCamara = sqlDesgloseCamara;
  }

  public void setSqlAllocationToClearerGrouping(String sqlAllocationToClearerGrouping) {
    this.sqlAllocationToClearerGrouping = sqlAllocationToClearerGrouping;
  }

  public void setSqlAllocationGrouping(String sqlAllocationGrouping) {
    this.sqlAllocationGrouping = sqlAllocationGrouping;
  }

  public void setTiempoMaxProcesoBok(Long tiempoMaxProcesoBok) {
    this.tiempoMaxProcesoBok = tiempoMaxProcesoBok;
  }

  public void setTiempoMaxProcesoAlo(Long tiempoMaxProcesoAlo) {
    this.tiempoMaxProcesoAlo = tiempoMaxProcesoAlo;
  }

  public void setTiempoMaxProcesoAlc(Long tiempoMaxProcesoAlc) {
    this.tiempoMaxProcesoAlc = tiempoMaxProcesoAlc;
  }

  public void setEstadoIncidencias(Integer estadoIncidencias) {
    this.estadoIncidencias = estadoIncidencias;
  }

  public void setEstadoRechazado(Integer estadoRechazado) {
    this.estadoRechazado = estadoRechazado;
  }

  public void setEstadoEnCurso(Integer estadoEnCurso) {
    this.estadoEnCurso = estadoEnCurso;
  }

  public void setEstadoTratandose(Integer estadoTratandose) {
    this.estadoTratandose = estadoTratandose;
  }

  public void setNombreCampoEstado(String nombreCampoEstado) {
    this.nombreCampoEstado = nombreCampoEstado;
  }

  public Map<String, Integer> getEstadosEquivalentes() {
    return estadosEquivalentes;
  }

  public void setEstadosEquivalentes(Map<String, Integer> estadosEquivalentes) {
    this.estadosEquivalentes = estadosEquivalentes;
  }

  public boolean isScriptDividend() {
    return isScriptDividend;
  }

  public void setScriptDividend(boolean isScriptDividend) {
    this.isScriptDividend = isScriptDividend;
  }

}
