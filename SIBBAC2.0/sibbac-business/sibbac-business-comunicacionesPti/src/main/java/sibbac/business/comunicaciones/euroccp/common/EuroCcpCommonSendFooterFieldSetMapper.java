package sibbac.business.comunicaciones.euroccp.common;

import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;

import sibbac.business.comunicaciones.euroccp.titulares.mapper.EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpCommonSendFooterFieldSetMapper extends WrapperAbstractFieldSetMapper<EuroCcpCommonSendRegisterDTO> {

  public EuroCcpCommonSendFooterFieldSetMapper() {
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpCommonFooterFields.values();
  }

  @Override
  public EuroCcpCommonSendFooterDTO getNewInstance() {
    return new EuroCcpCommonSendFooterDTO();
  }

  @Override
  public LineAggregator<EuroCcpCommonSendRegisterDTO> getLineAggregator() {
    FormatterLineAggregator<EuroCcpCommonSendRegisterDTO> lineAggregator = new EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator<EuroCcpCommonSendRegisterDTO>(
        this);

    BeanWrapperFieldExtractor<EuroCcpCommonSendRegisterDTO> fieldExtractor = new BeanWrapperFieldExtractor<EuroCcpCommonSendRegisterDTO>();
    fieldExtractor.setNames(getFieldNames());
    lineAggregator.setFormat(getFormat());
    lineAggregator.setFieldExtractor(fieldExtractor);
    return lineAggregator;
  }

}
