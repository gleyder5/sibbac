package sibbac.business.comunicaciones.page.query;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.page.query.dto.DesgloseclitTraspasosDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.SimpleDynamicFilter.SimpleDynamicFilterComparatorType;
import sibbac.database.StringDynamicFilter;

@Component("DesglosesclititTraspasosPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DesglosesclititTraspasosPageQuery extends AbstractDynamicPageQuery {

  protected static final String NBOOKING = "B.NBOOKING";
  protected static final String REF_CLIENTE = "A.REF_CLIENTE";
  protected static final String FECHA_CONTRATACION = "B.FETRADE";
  protected static final String FECHA_LIQUIDACION = "B.FEVALOR";
  protected static final String ALIAS = "B.CDALIAS";
  protected static final String ISIN = "B.CDISIN";
  protected static final String COMPRA_VENTA = "B.CDTPOPER";

  private static final DynamicColumn[] COLUMNS = {
      new DynamicColumn("F. Contratación", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("F. Liquidación", "FEVALOR", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Operación ECC", "CDOPERACIONECC_DESGLOSE"),
      new DynamicColumn("N. Operac. Previa", "NUOPERACIONPREV_DESGLOSE"),
      new DynamicColumn("N. Operac. Inicio", "NUOPERACIONINIC"), new DynamicColumn("Id.Mercado", "NUORDENMKT"),
      new DynamicColumn("Id.Ejecucion", "NUREFEXEMKT"), new DynamicColumn("Sentido", "SENTIDO"),
      new DynamicColumn("Estado", "ESTADO"), new DynamicColumn("Alias", "ALIAS"),
      new DynamicColumn("Desc. Alias", "DESC_ALIAS"), new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Desc. Isin", "DESC_ISIN"), new DynamicColumn("Camara", "CAMARA"),
      new DynamicColumn("Cuenta", "CUENTA_COMPENSACION_DESTINO"),
      new DynamicColumn("Compensador", "MIEMBRO_COMPENSADOR_DESTINO"),
      new DynamicColumn("Desc. Compensador", "DESC_MIEMBRO_COMPENSADOR_DESTINO"),
      new DynamicColumn("Ref. Give-Up", "CD_REF_ASIGNACION"),
      new DynamicColumn("Precio", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Efectivo", "EFECTIVO", DynamicColumn.ColumnType.N4), new DynamicColumn("N. Orden", "NUORDEN"),
      new DynamicColumn("N. Booking", "NBOOKING"), new DynamicColumn("N. Desg. Cliente", "NUCNFCLT"),
      new DynamicColumn("N. Desg. Liquidador", "NUCNFLIQ") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new DateDynamicFilter(FECHA_CONTRATACION, "F. Contratación"));
    res.add(new DateDynamicFilter(FECHA_LIQUIDACION, "F. Liquidación"));
    res.add(new StringDynamicFilter(COMPRA_VENTA, "Sentido"));
    res.add(new StringDynamicFilter(REF_CLIENTE, "Referencia cliente"));
    res.add(new StringDynamicFilter(NBOOKING, "Número booking"));
    res.add(new StringDynamicFilter(ALIAS, "Alias"));
    res.add(new StringDynamicFilter(ISIN, "Isin"));
    return res;
  }

  public DesglosesclititTraspasosPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public DesglosesclititTraspasosPageQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public DesglosesclititTraspasosPageQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    StringBuilder sb = new StringBuilder()
        .append(
            "SELECT B.FETRADE, B.FEVALOR, DCT.CDOPERACIONECC CDOPERACIONECC_DESGLOSE, DCT.NUOPERACIONPREV NUOPERACIONPREV_DESGLOSE, DCT.NUOPERACIONINIC, DCT.NUORDENMKT, DCT.NUREFEXEMKT, B.CDTPOPER SENTIDO, ")
        .append(" E.NOMBRE AS ESTADO, B.CDALIAS ALIAS, B.DESCRALI DESC_ALIAS, ")
        .append(" B.CDISIN ISIN, B.NBVALORS DESC_ISIN,  CC.CD_CODIGO CAMARA, ")
        .append(
            " DCT.CUENTA_COMPENSACION_DESTINO, DCT.MIEMBRO_COMPENSADOR_DESTINO, COALESCE(MC.NB_DESCRIPCION,'') AS DESC_MIEMBRO_COMPENSADOR_DESTINO, DCT.CD_REF_ASIGNACION, DCT.IMPRECIO PRECIO, DCT.IMTITULOS TITULOS, DCT.IMEFECTIVO EFECTIVO,")
        .append(" B.NUORDEN, B.NBOOKING, A.NUCNFCLT, C.NUCNFLIQ  ");
    return sb.toString();
  }

  @Override
  public String getFrom() {
    StringBuilder sb = new StringBuilder()
        .append("FROM  TMCT0BOK B INNER JOIN TMCT0ALO A ON B.NUORDEN = A.NUORDEN AND B.NBOOKING = A.NBOOKING ")
        .append(
            " INNER JOIN  TMCT0ALC C ON A.NUORDEN = C.NUORDEN AND A.NBOOKING = C.NBOOKING AND A.NUCNFCLT = C.NUCNFCLT ")
        .append(
            " INNER JOIN TMCT0DESGLOSECAMARA DC ON C.NUORDEN = DC.NUORDEN AND C.NBOOKING = DC.NBOOKING AND C.NUCNFCLT=DC.NUCNFCLT AND C.NUCNFLIQ = DC.NUCNFLIQ ")
        .append(" INNER JOIN TMCT0_CAMARA_COMPENSACION CC ON  DC.IDCAMARA = CC.ID_CAMARA_COMPENSACION ")
        .append(" INNER JOIN TMCT0DESGLOSECLITIT DCT ON DC.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA ")
        .append(" INNER JOIN TMCT0ESTADO E ON B.CDESTADOASIG = E.IDESTADO ")
        .append(" LEFT OUTER JOIN TMCT0_COMPENSADOR MC ON DCT.MIEMBRO_COMPENSADOR_DESTINO =  MC.NB_NOMBRE ");
    return sb.toString();
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {
    String casePti = "'S'";
    String ctaCompensacion = "''";
    StringBuilder sb = new StringBuilder(" B.CDESTADOASIG > {0} AND B.CASEPTI = {1} AND DC.CDESTADOASIG > {0} ");
    sb.append(
        " AND DC.CDESTADOASIG <> {2} AND DC.CDESTADOASIG <> {3} AND DC.CDESTADOASIG <> {4} AND DC.CDESTADOASIG <> {5} ")
        .append(" AND DC.CDESTADOASIG <> {6} AND DC.CDESTADOASIG <> {7} AND DCT.CUENTA_COMPENSACION_DESTINO <> {8} ");
    String postWhere = MessageFormat.format(sb.toString(), EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(), casePti,
        EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId(),
        EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId(),
        EstadosEnumerados.ASIGNACIONES.ENVIANDOSE.getId(), EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_S3.getId(),
        EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO.getId(),
        EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_ANULACION.getId(), ctaCompensacion);
    return postWhere;
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @SuppressWarnings("unchecked")
  public void setNbookingFilter(String nbooking) {
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      if (filter.getField().equals(NBOOKING)) {
        filter.setComparator(SimpleDynamicFilterComparatorType.EQ);
        ((SimpleDynamicFilter<String>) filter).getValues().add(nbooking);
        break;
      }
    }
  }

  public void clearNbookingFilter() {
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      if (filter.getField().equals(NBOOKING)) {
        filter.setComparator(null);
        filter.getValues().clear();
        break;
      }
    }
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  protected DesgloseclitTraspasosDTO loadFromArray(Object[] o) {
    Date fechaContratacion = (Date) o[0];
    Date fechaLiquidacion = (Date) o[1];
    String cdoperacionecc = (String) o[2];
    String nuoperacionprev = (String) o[3];
    String nuoperacioninic = (String) o[4];
    String ordenMercado = (String) o[5];
    String ejecucionMercado = (String) o[6];
    Character sentido = (Character) o[7];
    String cdisin = (String) o[8];
    String descripcionIsin = (String) o[9];
    String camaraCompensacion = (String) o[10];
    String cuenta = (String) o[11];
    String miembroCompensador = (String) o[12];
    String nombreMiembroCompensador = (String) o[13];
    String referenciaAsignacionExterna = (String) o[14];
    BigDecimal titulos = (BigDecimal) o[15];
    BigDecimal precio = (BigDecimal) o[16];
    BigDecimal efectivo = (BigDecimal) o[17];
    String nuorden = (String) o[18];
    String nbooking = (String) o[19];
    String nucnfclt = (String) o[20];
    Short nucnfliq = (Short) o[21];
    Long iddesglosecamara = null;
    Long nudesglose = null;
    DesgloseclitTraspasosDTO dto = new DesgloseclitTraspasosDTO(fechaContratacion, fechaLiquidacion, cdoperacionecc,
        nuoperacionprev, nuoperacioninic, ordenMercado, ejecucionMercado, sentido, cdisin, descripcionIsin,
        camaraCompensacion, cuenta, miembroCompensador, nombreMiembroCompensador, referenciaAsignacionExterna, titulos,
        precio, efectivo, nuorden, nbooking, nucnfclt, nucnfliq, iddesglosecamara, nudesglose);

    return dto;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    boolean lanzarEx = true;
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      switch (filter.getField()) {
        case NBOOKING:
        case REF_CLIENTE:
        case FECHA_CONTRATACION:
        case FECHA_LIQUIDACION:

          if (!filter.getValues().isEmpty()) {
            lanzarEx = false;
          }
          break;

        default:
          break;
      }
      if (!lanzarEx) {
        break;
      }
    }
    if (lanzarEx) {
      throw new SIBBACBusinessException("Mandatory filter not set.");
    }

  }

  // @Override
  // protected void sortFilters() {
  //
  // List<SimpleDynamicFilter<?>> filtersSorted = new
  // ArrayList<SimpleDynamicFilter<?>>();
  //
  // for (SimpleDynamicFilter<?> filter : getAppliedFilers()) {
  // if (filter.getField().equals(REF_CLIENTE)) {
  // filtersSorted.add(filter);
  // }
  // }
  //
  // for (SimpleDynamicFilter<?> filter : getAppliedFilers()) {
  // if (filter.getField().equals(NBOOKING)) {
  // filtersSorted.add(filter);
  // }
  // }
  //
  // for (SimpleDynamicFilter<?> filter : getAppliedFilers()) {
  // if (filter.getField().equals(FECHA_CONTRATACION)) {
  // filtersSorted.add(filter);
  // }
  // }
  //
  // for (SimpleDynamicFilter<?> filter : getAppliedFilers()) {
  // if (filter.getField().equals(FECHA_LIQUIDACION)) {
  // filtersSorted.add(filter);
  // }
  // }
  //
  // for (SimpleDynamicFilter<?> filter : getAppliedFilers()) {
  // if (!filtersSorted.contains(filter)) {
  // filtersSorted.add(filter);
  // }
  // }
  //
  // getAppliedFilers().clear();
  // getAppliedFilers().addAll(filtersSorted);
  // }
}
