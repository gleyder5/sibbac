package sibbac.business.comunicaciones.imputacions3.rest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.comunicaciones.imputacions3.bo.IManualImputacionS3Pti;
import sibbac.business.comunicaciones.imputacions3.bo.IManualImputacionS3PtiMab;
import sibbac.business.comunicaciones.imputacions3.bo.IManualUserImputacionS3EuroCcp;
import sibbac.business.comunicaciones.imputacions3.bo.ImputacionS3ConfigPantallaDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.security.session.UserSession;

@RestController
@RequestMapping("/imputacions3")
public class SIBBACServiceImputacionS3 {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceImputacionS3.class);

  @Autowired
  private HttpSession session;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private IManualUserImputacionS3EuroCcp manualUserImputacionS3EuroCcp;

  @Autowired
  private IManualImputacionS3Pti manualImputacionS3Pti;

  @Autowired
  private IManualImputacionS3PtiMab manualImputacionS3PtiMab;

  @RequestMapping(value = "/all", method = RequestMethod.POST)
  public CallableResultDTO ejecutaImputacionS3Total() throws Exception {
    CallableResultDTO res = new CallableResultDTO();
    LOG.info("[ejecutaImputacionS3Total] inicio...");
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;
    LOG.info("[ejecutaImputacionS3Total] ejecucion parte PTI user: {}...", auditUser);

    res.append(manualImputacionS3Pti.executeManualUser(auditUser));
    LOG.info("[ejecutaImputacionS3Total] ejecucion parte EUROCCP user: {}...", auditUser);
    res.append(manualUserImputacionS3EuroCcp.executeManualUser(auditUser));
    LOG.info("[ejecutaImputacionS3Total] fin user: {}...", auditUser);
    return res;
  }

  @RequestMapping(value = "/euroccp", method = RequestMethod.POST)
  public CallableResultDTO ejecutaImputacionS3EuroCcp() throws Exception {
    LOG.info("[ejecutaImputacionS3EuroCcp] inicio...");
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;
    LOG.info("[ejecutaImputacionS3EuroCcp] ejecucion parte EUROCCP user: {}...", auditUser);
    CallableResultDTO res = manualUserImputacionS3EuroCcp.executeManualUser(auditUser);
    LOG.info("[ejecutaImputacionS3EuroCcp] fin user: {}...", auditUser);
    return res;
  }

  @RequestMapping(value = "/pti", method = RequestMethod.POST)
  public CallableResultDTO ejecutaImputacionS3Pti() {
    LOG.info("[ejecutaImputacionS3Pti] inicio...");
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;
    LOG.info("[ejecutaImputacionS3Pti] ejecucion parte PTI user: {}...", auditUser);
    CallableResultDTO res = manualImputacionS3Pti.executeManualUser(auditUser);
    LOG.info("[ejecutaImputacionS3Pti] fin user: {}...", auditUser);
    return res;
  }

  @RequestMapping(value = "/pti/mab", method = RequestMethod.POST)
  public CallableResultDTO ejecutaImputacionS3PtiMab() {

    LOG.info("[ejecutaImputacionS3PtiMab] inicio...");
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;
    LOG.info("[ejecutaImputacionS3PtiMab] ejecucion parte PTI user: {}...", auditUser);
    CallableResultDTO res = manualImputacionS3PtiMab.executeManualUser(auditUser);
    LOG.info("[ejecutaImputacionS3PtiMab] fin user: {}...", auditUser);
    return res;
  }

  @RequestMapping(value = "/get/config", method = RequestMethod.GET)
  public ImputacionS3ConfigPantallaDTO getConfigPantalla() {
    DateFormat dfPuntos = new SimpleDateFormat("HH:mm:ss");
    DateFormat dfSinPuntos = new SimpleDateFormat("HHmmss");
    ImputacionS3ConfigPantallaDTO config = new ImputacionS3ConfigPantallaDTO();
    String cuentaCompensacionS3 = cuentaCompensacionBo.findCuentaCompensacionS3();
    config.setCuentaCompensacionDestinoS3(cuentaCompensacionS3);
    try {
      config.setHoraInicioImputacionS3EuroCcp(dfPuntos.format(dfSinPuntos.parse(cfgBo
          .findHoraInicioImputacionesS3EuroCcp())));
    }
    catch (SIBBACBusinessException | ParseException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    try {
      config.setHoraInicioImputacionS3PtiMab(dfPuntos.format(dfSinPuntos.parse(cfgBo
          .findHoraInicioImputacionesMabS3Pti())));
    }
    catch (SIBBACBusinessException | ParseException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    try {
      config.setHoraInicioImputacionS3PTiNoMab(dfPuntos.format(dfSinPuntos.parse(cfgBo
          .findHoraInicioImputacionesS3Pti())));
    }
    catch (SIBBACBusinessException | ParseException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }
    try {
      config.setHoraFinalImputacionS3EuroCcp(dfPuntos.format(dfSinPuntos.parse(cfgBo
          .findHoraFinalImputacionesS3EuroCcp())));
    }
    catch (SIBBACBusinessException | ParseException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    try {
      config
          .setHoraFinalImputacionS3PtiMab(dfPuntos.format(dfSinPuntos.parse(cfgBo.findHoraFinalImputacionesMabS3Pti())));
    }
    catch (SIBBACBusinessException | ParseException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    try {
      config
          .setHoraFinalImputacionS3PTiNoMab(dfPuntos.format(dfSinPuntos.parse(cfgBo.findHoraFinalImputacionesS3Pti())));
    }
    catch (SIBBACBusinessException | ParseException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    return config;
  }

  @RequestMapping(value = "/update/config", method = RequestMethod.POST)
  public CallableResultDTO updateConfigPantalla(@RequestBody ImputacionS3ConfigPantallaDTO config) {
    CallableResultDTO res = new CallableResultDTO();
    String auditUser = ((UserSession) session.getAttribute("UserSession")).getName();
    auditUser = auditUser.length() >= 10 ? auditUser.substring(0, 10) : auditUser;
    res.append(this.updateConfigCuentaS3(config, auditUser));
    res.append(this.updateConfigHorasS3(config, auditUser));

    if (res.getMessage().isEmpty()) {
      res.addErrorMessage("No se ha modificado nada.");
    }
    return res;
  }

  private CallableResultDTO updateConfigCuentaS3(ImputacionS3ConfigPantallaDTO config, String auditUser) {
    CallableResultDTO res = new CallableResultDTO();
    LOG.info("[updateConfigCuentaS3] inicio user: {}", auditUser);
    String cuentaCompensacionS3 = cuentaCompensacionBo.findCuentaCompensacionS3();
    if (cuentaCompensacionS3 == null || config.getCuentaCompensacionDestinoS3() != null
        && !config.getCuentaCompensacionDestinoS3().trim().equals(cuentaCompensacionS3.trim())) {
      Tmct0CuentasDeCompensacion cuentaCompensacionDestinoHb = cuentaCompensacionBo.findByCdCodigo(config
          .getCuentaCompensacionDestinoS3());

      if (cuentaCompensacionDestinoHb == null) {
        res.addErrorMessage(String.format("No se encuentra la cuenta destino '%s'",
            config.getCuentaCompensacionDestinoS3()));
        LOG.warn("[updateConfigPantalla] no encontrada la cuenta de compensacion destino.");
      }
      else {
        LOG.info("[updateConfigHorasS3] modificacion cuenta S3 de {} a {} user: {}", cuentaCompensacionS3,
            config.getCuentaCompensacionDestinoS3(), auditUser);
        Tmct0CuentasDeCompensacion cuentaCompensacionOrigenHb = cuentaCompensacionBo
            .findByCdCodigo(cuentaCompensacionS3);
        cuentaCompensacionOrigenHb.setS3('N');
        cuentaCompensacionOrigenHb.setAuditFechaCambio(new Date());
        cuentaCompensacionOrigenHb.setAuditUser(auditUser);
        cuentaCompensacionBo.save(cuentaCompensacionOrigenHb);

        cuentaCompensacionDestinoHb.setS3('S');
        cuentaCompensacionDestinoHb.setAuditFechaCambio(new Date());
        cuentaCompensacionDestinoHb.setAuditUser(auditUser);
        cuentaCompensacionBo.save(cuentaCompensacionDestinoHb);

        res.addMessage(String.format("Se ha cambiado la cuenta de imputacion s3 de '%s' a '%s'", cuentaCompensacionS3,
            config.getCuentaCompensacionDestinoS3()));
      }
    }
    LOG.info("[updateConfigCuentaS3] fin user: {}", auditUser);
    return res;
  }

  // TODO puesto que no hemos sido capaces de
  private CallableResultDTO updateConfigHorasS3(ImputacionS3ConfigPantallaDTO config, String auditUser) {
    CallableResultDTO res = new CallableResultDTO();
    DateFormat dfPuntos = new SimpleDateFormat("HH:mm:ss");
    DateFormat dfSinPuntos = new SimpleDateFormat("HHmmss");
    LOG.info("[updateConfigHorasS3] inicio user: {}", auditUser);
    String value;
    Tmct0cfg cfg;
    try {
      value = dfSinPuntos.format(dfPuntos.parse(config.getHoraInicioImputacionS3EuroCcp()));
      cfg = cfgBo.getHoraInicioImputacionesS3EuroCcp();
      if (!value.trim().equals(cfg.getKeyvalue().trim())) {
        cfg.setKeyvalue(value);
        cfgBo.save(cfg);
        res.addMessage("Se ha modificado la hora inicio imputacion s3 para euroccp");
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      res.addErrorMessage(String.format("No se ha podido actualizar la hora inicio imputacion s3 de euroccp, %s",
          e.getMessage()));
    }

    try {
      value = dfSinPuntos.format(dfPuntos.parse(config.getHoraFinalImputacionS3EuroCcp()));
      cfg = cfgBo.getHoraFinalImputacionesS3EuroCcp();
      if (!value.trim().equals(cfg.getKeyvalue().trim())) {
        cfg.setKeyvalue(value);
        cfgBo.save(cfg);
        res.addMessage("Se ha modificado la hora final imputacion s3 para euroccp");
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      res.addErrorMessage(String.format("No se ha podido actualizar la hora inicio imputacion s3 de euroccp, %s",
          e.getMessage()));
    }

    try {
      cfg = cfgBo.getHoraInicioImputacionesS3Pti();
      value = dfSinPuntos.format(dfPuntos.parse(config.getHoraInicioImputacionS3PTiNoMab()));
      if (!value.trim().equals(cfg.getKeyvalue().trim())) {
        cfg.setKeyvalue(value);
        cfgBo.save(cfg);
        res.addMessage("Se ha modificado la hora inicio imputacion s3 para pti no mab");
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      res.addErrorMessage(String.format("No se ha podido actualizar la hora inicio imputacion s3 de pti no mab, %s",
          e.getMessage()));
    }

    try {
      cfg = cfgBo.getHoraFinalImputacionesS3Pti();
      value = dfSinPuntos.format(dfPuntos.parse(config.getHoraFinalImputacionS3PTiNoMab()));
      if (!value.trim().equals(cfg.getKeyvalue().trim())) {
        cfg.setKeyvalue(value);
        cfgBo.save(cfg);
        res.addMessage("Se ha modificado la hora final imputacion s3 para pti no mab");
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      res.addErrorMessage(String.format("No se ha podido actualizar la hora final imputacion s3 de pti no mab, %s",
          e.getMessage()));
    }
    try {
      cfg = cfgBo.getHoraInicioImputacionesMabS3Pti();
      value = dfSinPuntos.format(dfPuntos.parse(config.getHoraInicioImputacionS3PtiMab()));
      if (!value.trim().equals(cfg.getKeyvalue().trim())) {
        cfg.setKeyvalue(value);
        cfgBo.save(cfg);
        res.addMessage("Se ha modificado la hora inicio imputacion s3 para pti mab");
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      res.addErrorMessage(String.format("No se ha podido actualizar la hora inicio imputacion s3 de pti mab, %s",
          e.getMessage()));
    }

    try {
      cfg = cfgBo.getHoraFinalImputacionesMabS3Pti();
      value = dfSinPuntos.format(dfPuntos.parse(config.getHoraFinalImputacionS3PtiMab()));
      if (!value.trim().equals(cfg.getKeyvalue().trim())) {
        cfg.setKeyvalue(value);
        cfgBo.save(cfg);
        res.addMessage("Se ha modificado la hora final imputacion s3 para pti mab");
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      res.addErrorMessage(String.format("No se ha podido actualizar la hora inicio imputacion s3 de pti mab, %s",
          e.getMessage()));
    }
    LOG.info("[updateConfigHorasS3] fin user: {}", auditUser);
    return res;
  }
}
