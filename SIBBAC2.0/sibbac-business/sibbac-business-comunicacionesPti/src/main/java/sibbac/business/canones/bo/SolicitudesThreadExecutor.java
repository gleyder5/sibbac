package sibbac.business.canones.bo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.context.ApplicationContext;

import sibbac.business.canones.model.CanonSolicitaRecalculo;
import sibbac.common.LimitedThreadExecutor;

class SolicitudesThreadExecutor 
    extends LimitedThreadExecutor<CanonSolicitaRecalculo, SolicitudesThreadExecutor.SolicitudCallable> {

  private static Collection<SolicitudCallable> buildCallables(ApplicationContext ctx, final int threads) {
    final List<SolicitudCallable> l;
    
    l = new ArrayList<>(threads);
    for(int i = 0; i < threads; i++) {
      l.add(new SolicitudCallable(ctx));
    }
    return l;
  }
  
  SolicitudesThreadExecutor(ApplicationContext ctx, List<CanonSolicitaRecalculo> solicitudes, int threads) {
    super(new ArrayDeque<>(solicitudes), buildCallables(ctx, threads));
  }

  @Override
  protected boolean notIsFinal(CanonSolicitaRecalculo item) {
    if(item == null) {
      return false;
    }
    return item.getId() != null;
  }

  @Override
  protected void assignValue(CanonSolicitaRecalculo item, SolicitudCallable callable) {
    callable.setSolicitud(item);
  }

  static class SolicitudCallable implements Callable<SolicitudCallable> {
    
    private final ApplicationContext ctx;

    private CanonSolicitaRecalculo solicitud;
    
    private SolicitudCallable(ApplicationContext ctx) {
      this.ctx = ctx;
    }
    
    void setSolicitud(CanonSolicitaRecalculo solicitud) {
      this.solicitud = solicitud;
    }

    @Override
    public SolicitudCallable call() throws Exception {
      final CanonSolicitaRecalculoBo bo;
      
      bo = ctx.getBean(CanonSolicitaRecalculoBo.class);
      bo.resuelveSolicitud(solicitud);
      return this;
    }
    
  }

}
