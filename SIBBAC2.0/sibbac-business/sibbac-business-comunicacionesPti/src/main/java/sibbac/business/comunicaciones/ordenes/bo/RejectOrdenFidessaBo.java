package sibbac.business.comunicaciones.ordenes.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.naming.NamingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0DesgloseCamaraEnvioRtBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0caseoperacionejeBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.dto.DatosAsignacionDTO;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.business.wrappers.database.model.Tmct0stt;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessagesBo;

import com.sv.sibbac.multicenter.util.MulticenterRejectionsUtils;

@Service
public class RejectOrdenFidessaBo {
  protected static final Logger LOG = LoggerFactory.getLogger(RejectOrdenFidessaBo.class);

  private static final Logger LOG_XML_REJECT = LoggerFactory.getLogger("XMLS_REJECT");

  @Value("${jms.xml.fidessa.qmanager.name}")
  private String fidessaXmlJndiName;

  @Value("${jms.xml.fidessa.out.queue}")
  private String fidessaXmlOut;

  @Value("${jms.xml.fidessa.qmanager.host}")
  private String fidessaMqHost;

  @Value("${jms.xml.fidessa.qmanager.port}")
  private int fidessaMqPort;

  @Value("${jms.xml.fidessa.qmanager.channel}")
  private String fidessaMqChannel;

  @Value("${jms.xml.fidessa.qmanager.queuemanager}")
  private String fidessaMqManagerName;

  @Value("${sibbac.escalado.asignacion.cta.pdte.usuario}")
  private List<String> listCtasPdteUsuario;

  @Value("${sibbac.escalado.asignacion.cta.imputada.propia}")
  private List<String> listCtasInputadaPropia;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0ejeBo ejeBo;

  @Autowired
  private Tmct0caseoperacionejeBo caseBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtBo rtBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsBo;

  public RejectOrdenFidessaBo() {
  }

  public boolean canBeRejectBooking(Tmct0bokId bookId) {
    boolean canBeReject = false;

    Tmct0bok bok = bokBo.findById(bookId);

    boolean nacional = bok.getTmct0ord().getCdclsmdo() != null && bok.getTmct0ord().getCdclsmdo() == 'N';

    if (nacional) {
      // todos sus titulos estan en la TRN o 01P
    }
    else {
      // esta en un estado que se pueda rechazar?
    }
    return canBeReject;
  }

  @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public CallableResultDTO rejectBooking(ConnectionFactory jf, String destination, Tmct0bokId bookId, String comment,
      String auditUser) throws SIBBACBusinessException {
    String msg;
    CallableResultDTO res = new CallableResultDTO();
    LOG.debug("[rejectBooking] leyendo booking {} ...", bookId);

    boolean error = false;
    try (Connection connection = jmsBo.getNewConnection(jf);) {
      connection.start();
      try (Session session = jmsBo.getNewSession(connection, true);) {

        try (MessageProducer messageProduccer = jmsBo.getNewProducer(session, destination);) {

          try {
            this.darBajaBooking(bookId, comment, auditUser);
          }
          catch (SIBBACBusinessException e) {
            throw e;
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(String.format("Incidencia dando de baja el booking [%s - %s] %s",
                bookId.getNuorden(), bookId.getNbooking(), e.getMessage()), e);
          }

          msg = String.format("USER_MANUAL_REJECT-Comment: '%s' user: '%s' at: '%s'", comment, auditUser, new Date());

          MulticenterRejectionsUtils rejectutils = new MulticenterRejectionsUtils();
          String reject = rejectutils.createRejection(StringUtils.trim(bookId.getNuorden()),
              StringUtils.trim(bookId.getNbooking()), "", "", msg);
          LOG.info("[rejectBooking] mandando a {} reject de booking {}", destination, reject);
          jmsBo.sendJmsMessage(messageProduccer, jmsBo.createNewTextMessage(session, reject));

          session.commit();

          LOG_XML_REJECT.error(reject);

          msg = String.format("[%s/%s] Booking dado de baja.", bookId.getNuorden(), bookId.getNbooking());
          LOG.info("[rejectBooking] {} comment: {} user: {}", msg, comment, auditUser);
          res.addMessage(msg);

        }
        catch (JMSException e) {
          error = true;
          throw e;
        }
        catch (Exception e) {
          error = true;
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
        finally {
          if (error && session != null) {
            session.rollback();
          }
        }
      }
    }
    catch (SIBBACBusinessException e) {
      throw e;
    }
    catch (JMSException e) {
      throw new SIBBACBusinessException("No se ha podido conectar con fidessa para enviar el rechazo.", e);
    }
    catch (Exception e) {
      error = true;
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {

    }
    return res;

  }

  @Transactional
  private void darBajaMovimientosDesglose(Tmct0desglosecamara dc, String auditUser) {
    List<Tmct0movimientoecc> listMovs = dc.getTmct0movimientos();
    this.darBajaMovimientos(null, dc, listMovs, auditUser);
  }

  @Transactional
  private void darBajaMovimientos(Tmct0alo alo, Tmct0desglosecamara dc, List<Tmct0movimientoecc> listMovs,
      String auditUser) {
    Tmct0movimientoeccFidessa movf;
    List<Tmct0movimientooperacionnuevaecc> listMovsn;

    if (CollectionUtils.isNotEmpty(listMovs)) {
      if (dc != null) {
        LOG.debug("[rejectBooking] dando de baja los movimientos de desglose dc {} ...", dc.getIddesglosecamara());
      }
      else if (alo != null) {
        LOG.debug("[rejectBooking] dando de baja los movimientos de desglose alo {} ...", alo.getId());
      }

      for (Tmct0movimientoecc mov : listMovs) {
        if (movBo.isMovimientoAlta(mov)) {

          if (dc != null) {
            listMovsn = mov.getTmct0movimientooperacionnuevaeccs();
            for (Tmct0movimientooperacionnuevaecc movn : listMovsn) {
              if (movn.getIdMovimientoFidessa() != null) {
                LOG.debug("[rejectBooking] devolviendo titulos al mov fidessa {} - {} - {} ...",
                    mov.getCdrefmovimiento(), mov.getCdrefmovimientoecc(), movn.getCdoperacionecc());
                movf = movfBo.findById(movn.getIdMovimientoFidessa());
                if (movf != null) {
                  movf.setImtitulosPendientesAsignar(movf.getImtitulosPendientesAsignar().add(movn.getImtitulos()));
                  movf.setAuditFechaCambio(new Date());
                  movf.setAuditUser(auditUser);
                  movfBo.save(movf);
                }
              }
              movn.setIdMovimientoFidessa(null);
              movn.setAuditFechaCambio(new Date());
              movn.setAuditUser(auditUser);
              movnBo.save(movn);
            }
          }
          mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
          mov.setCdestadomov(EstadosEccMovimiento.CANCELADO_INT.text);
          mov.setAuditFechaCambio(new Date());
          mov.setAuditUser(auditUser);
          movBo.save(mov);
        }
      }
    }// fin movs
  }

  @Transactional
  private void darBajaRts(Tmct0desglosecamara dc, String auditUser) {
    List<Tmct0DesgloseCamaraEnvioRt> listRts = dc.getTmct0DesgloseCamaraEnvioRt();
    if (CollectionUtils.isNotEmpty(listRts)) {
      LOG.debug("[rejectBooking] dando de baja los rt's de desglose dc {} ...", dc.getIddesglosecamara());
      for (Tmct0DesgloseCamaraEnvioRt rt : listRts) {
        if (rt.getEstado() != null && rt.getEstado() != 'C') {
          rt.setEstado('C');
          rt.setAudit_fecha_cambio(new Date());
          rt.setAudit_user(auditUser);
          rtBo.save(rt);
        }
      }
    }
  }

  @Transactional
  private void darBajaDesglosesCamara(Tmct0alc alc, String auditUser) {
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listDcs) {

      dc.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()));
      dc.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()));
      dc.setAuditFechaCambio(new Date());
      dc.setAuditUser(auditUser);
      dcBo.save(dc);
      this.darBajaMovimientosDesglose(dc, auditUser);

      this.darBajaRts(dc, auditUser);

    } // fin dcs
  }

  @Transactional
  private void darBajaTitularesAlocationToClearer(Tmct0alc alc, String auditUser) {
    List<Tmct0afi> listAfis = afiBo.findByAlcData(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId()
        .getNucnfclt(), alc.getId().getNucnfliq());
    this.darBajaTitulares(listAfis, auditUser);
  }

  @Transactional
  private void darBajaAlocationsToClearer(Tmct0alo alo, String auditUser) {

    List<Tmct0alc> listAlcs = alo.getTmct0alcs();

    for (Tmct0alc alc : listAlcs) {

      alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
      alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId()));
      alc.setTmct0sta(alo.getTmct0sta());
      alc.setFhaudit(new Date());
      alc.setCdusuaud(auditUser);
      alcBo.save(alc);
      this.darBajaTitularesAlocationToClearer(alc, auditUser);

      this.darBajaDesglosesCamara(alc, auditUser);

    }
  }

  @Transactional
  private void darBajaTitularesAlocation(Tmct0alo alo, String auditUser) {
    LOG.debug("[rejectBooking] dando de baja los titulares alo {} ...", alo.getId());
    List<Tmct0afi> listAfis = afiBo.findActivosByAlloId(alo.getId());
    this.darBajaTitulares(listAfis, auditUser);
  }

  @Transactional
  private void darBajaTitulares(List<Tmct0afi> listAfis, String auditUser) {
    if (CollectionUtils.isNotEmpty(listAfis)) {
      for (Tmct0afi afi : listAfis) {
        afi.setCdusuina(auditUser);
        afi.setFhinacti(new Date());
        afi.setCdinacti('I');
        afi.setFhaudit(new Date());
        afi.setCdusuaud(auditUser);
      }
      afiBo.save(listAfis);
    }
  }

  @Transactional
  private void darBajaMovimientosAlocation(Tmct0alo alo, String auditUser) {
    List<Tmct0movimientoecc> listMovs = movBo.findByTmct0aloId(alo.getId());
    this.darBajaMovimientos(alo, null, listMovs, auditUser);
  }

  @Transactional
  private void darBajaAlocations(Tmct0bok bok, boolean nacional, String auditUser) {
    List<Tmct0alo> listAlos = bok.getTmct0alos();

    for (Tmct0alo alo : listAlos) {

      alo.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()));
      alo.setCdestadotit(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
      alo.setTmct0sta(bok.getTmct0sta());
      alo.setFhaudit(new Date());
      alo.setCdusuaud(auditUser);
      aloBo.save(alo);

      if (!nacional) {
        this.darBajaTitularesAlocation(alo, auditUser);
      }
      else {
        this.darBajaMovimientosAlocation(alo, auditUser);
        this.darBajaAlocationsToClearer(alo, auditUser);
      }

    }
  }

  @Transactional
  private void devolverTitulosCaseNacional(Tmct0bok bok, String auditUser) {
    LOG.debug("[rejectBooking] devolviendo titulos al case booking {} ...", bok.getId());
    List<Tmct0eje> listEjes = bok.getTmct0ejes();
    Tmct0caseoperacioneje op;
    for (Tmct0eje tmct0eje : listEjes) {
      op = tmct0eje.getTmct0caseoperacioneje();
      if (op != null && tmct0eje.getCasepti() != null && tmct0eje.getCasepti() == 'S') {
        op.setNutitpendcase(op.getNutitpendcase().add(tmct0eje.getNutiteje()));
        op.setAuditFechaCambio(new Date());
        op.setAuditUser(auditUser);
        caseBo.save(op);

        tmct0eje.setCasepti('N');
        tmct0eje.setFhaudit(new Date());
        tmct0eje.setCdusuaud(auditUser);
        tmct0eje.setTmct0caseoperacioneje(null);
        ejeBo.save(tmct0eje);

      }
    }
  }

  @Transactional
  private void canBeReject(Tmct0bok bok, boolean nacional, String auditUser) throws SIBBACBusinessException {
    List<DatosAsignacionDTO> datosAsignacionFromAlos;
    List<DatosAsignacionDTO> datosAsignacionFromDcs;
    if (nacional) {

      if (bok.getTmct0estadoByCdestadoasig() != null
          && bok.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && (bok.getCasepti() == null || bok.getCasepti() != 'S')) {
        LOG.debug("[canBeReject] la orden no esta casada se puede rechazar da igual el estado en el que este.");
        return;
      }

      if (EstadosEnumerados.ASIGNACIONES.ERROR_DATOS_ASIGNACION.getId() != bok.getTmct0estadoByCdestadoasig()
          .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.ERROR_DATOS_ANULACION.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.RECHAZADO_COMPENSADOR.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.PDTE_USUARIO_SIN_TITULAR.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSE_MANUAL.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.SIN_INSTRUCCIONES.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId() != bok.getTmct0estadoByCdestadoasig()
              .getIdestado()
          && EstadosEnumerados.ASIGNACIONES.IMPUTADA_PROPIA.getId() != bok.getTmct0estadoByCdestadoasig().getIdestado()) {
        throw new SIBBACBusinessException(String.format("Booking en estado %s no permite el rechazo", bok
            .getTmct0estadoByCdestadoasig().getNombre()));
      }
      else {

        datosAsignacionFromAlos = movBo.findAllDistinctCtaMiembroRefAsigFromAloByBookingId(bok.getId());
        if (CollectionUtils.isEmpty(datosAsignacionFromAlos)
            || datosAsignacionFromAlos.size() == 1
            && datosAsignacionFromAlos.get(0).getCuentaCompensacionDestino() != null
            && (listCtasPdteUsuario.contains(datosAsignacionFromAlos.get(0).getCuentaCompensacionDestino().trim()) || listCtasInputadaPropia
                .equals(datosAsignacionFromAlos.get(0).getCuentaCompensacionDestino().trim()))) {
          datosAsignacionFromDcs = movBo.findAllDistinctCtaMiembroRefAsigFromDesglosesCamaraByBookingId(bok.getId());
          if (CollectionUtils.isEmpty(datosAsignacionFromDcs)
              || datosAsignacionFromDcs.size() == 1
              && datosAsignacionFromDcs.get(0).getCuentaCompensacionDestino() != null
              && (listCtasPdteUsuario.contains(datosAsignacionFromDcs.get(0).getCuentaCompensacionDestino().trim()) || listCtasInputadaPropia
                  .contains(datosAsignacionFromDcs.get(0).getCuentaCompensacionDestino().trim()))) {

          }
          else {
            throw new SIBBACBusinessException("Los titulos estan asignados a cuentas que no permiten rechazo.");
          }
        }
        else {
          throw new SIBBACBusinessException("Los titulos estan asignados a cuentas que no permiten el rechazo.");
        }
      }
    }
    else {
      // internacional
    }
  }

  @Transactional
  private void darBajaBooking(Tmct0bokId bookId, String comment, String auditUser) throws SIBBACBusinessException {

    Tmct0sta rechazado = new Tmct0sta();
    rechazado.setId(new Tmct0staId("003", "TMCT0BOK"));
    rechazado.setTmct0stt(new Tmct0stt("TMCT0BOK"));

    Tmct0bok bok = bokBo.findById(bookId);
    if (bok == null) {
      throw new SIBBACBusinessException("Booking no encontrado");
    }

    Tmct0ord ord = bok.getTmct0ord();

    boolean nacional = ord.getCdclsmdo() != null && ord.getCdclsmdo() == 'N';

    this.canBeReject(bok, nacional, auditUser);

    String nombreEstadoAntesCambio = bok.getTmct0estadoByCdestadoasig().getNombre();

    this.substractEconomicDataFromBooking(ord, bok);

    if (nacional) {
      this.devolverTitulosCaseNacional(bok, auditUser);
    }

    bok.setCasepti('N');
    bok.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()));
    bok.setCdestadotit(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
    bok.setTmct0sta(rechazado);
    bok.setFhaudit(new Date());
    bok.setCdusuaud(auditUser);
    bok.setDsobserv(comment);

    this.darBajaAlocations(bok, nacional, auditUser);

    bokBo.save(bok);
    ordBo.save(ord);
    String msg = String.format("USER_MANUAL_REJECT - %s", comment);

    LOG.info("[rejectBooking] booking {} en estado {} usuario {} commentario {}", bookId, bok
        .getTmct0estadoByCdestadoasig().getNombre(), auditUser, comment);
    logBo.insertarRegistro(bookId.getNuorden(), bookId.getNbooking(), msg, nombreEstadoAntesCambio, auditUser);
    ordBo.flush();
  }

  protected ConnectionFactory getConnectionFactory() throws SIBBACBusinessException {
    LOG.trace("[getConnectionFactory] buscando conexion jms con pti...");
    ConnectionFactory cf;
    try {
      cf = jmsBo.getConnectionFactoryFromContext(fidessaXmlJndiName);
    }
    catch (NamingException e) {
      LOG.trace(e.getMessage(), e);
      try {
        cf = jmsBo.getNewJmsConnectionFactory(fidessaMqHost, fidessaMqPort, fidessaMqChannel, fidessaMqManagerName, "",
            "");
      }
      catch (JMSException e1) {
        throw new SIBBACBusinessException("Imposible recuperar conexion con camara nacional.", e1);
      }
    }
    return cf;
  }

  protected String getRejectBookingDestination() {
    return fidessaXmlOut;
  }

  @Transactional
  public void substractEconomicDataFromBooking(Tmct0ord tmct0ord, Tmct0bok tmct0bok) {
    LOG.trace("[substractEconomicDataFromBooking] inicio");
    if (tmct0ord.getImbrmerc() != null && tmct0bok.getImbrmerc() != null) {
      tmct0ord.setImbrmerc(tmct0ord.getImbrmerc().subtract(tmct0bok.getImbrmerc()));
    }
    if (tmct0ord.getImbrmreu() != null && tmct0bok.getImbrmreu() != null) {
      tmct0ord.setImbrmreu(tmct0ord.getImbrmreu().subtract(tmct0bok.getImbrmreu()));
    }

    if (tmct0ord.getImgasgesdv() != null && tmct0bok.getImgasgesdv() != null) {
      tmct0ord.setImgasgesdv(tmct0ord.getImgasgesdv().subtract(tmct0bok.getImgasgesdv()));
    }
    if (tmct0ord.getImgasgeseu() != null && tmct0bok.getImgasgeseu() != null) {
      tmct0ord.setImgasgeseu(tmct0ord.getImgasgeseu().subtract(tmct0bok.getImgasgeseu()));
    }
    if (tmct0ord.getImgasliqdv() != null && tmct0bok.getImgasliqdv() != null) {
      tmct0ord.setImgasliqdv(tmct0ord.getImgasliqdv().subtract(tmct0bok.getImgasliqdv()));
    }
    if (tmct0ord.getImgasliqeu() != null && tmct0bok.getImgasliqeu() != null) {
      tmct0ord.setImgasliqeu(tmct0ord.getImgasliqeu().subtract(tmct0bok.getImgasliqeu()));
    }
    if (tmct0ord.getImimpeur() != null && tmct0bok.getImimpeur() != null) {
      tmct0ord.setImimpeur(tmct0ord.getImimpeur().subtract(tmct0bok.getImimpeur()));
    }
    if (tmct0ord.getImimpdvs() != null && tmct0bok.getImimpdvs() != null) {
      tmct0ord.setImimpdvs(tmct0ord.getImimpdvs().subtract(tmct0bok.getImimpdvs()));
    }
    if (tmct0ord.getNutiteje() != null && tmct0bok.getNutiteje() != null) {
      tmct0ord.setNutiteje(tmct0ord.getNutiteje().subtract(tmct0bok.getNutiteje()));
    }
    if (tmct0ord.getImnetobr() != null && tmct0bok.getImnetobr() != null) {
      tmct0ord.setImnetobr(tmct0ord.getImnetobr().subtract(tmct0bok.getImnetobr()));
    }
    if (tmct0ord.getImnetocl() != null && tmct0bok.getImnetocl() != null) {
      tmct0ord.setImnetocl(tmct0ord.getImnetocl().subtract(tmct0bok.getImnetocl()));
    }
    if (tmct0ord.getImntbreu() != null && tmct0bok.getImntbreu() != null) {
      tmct0ord.setImntbreu(tmct0ord.getImntbreu().subtract(tmct0bok.getImntbreu()));
    }
    if (tmct0ord.getImntcleu() != null && tmct0bok.getImntcleu() != null) {
      tmct0ord.setImntcleu(tmct0ord.getImntcleu().subtract(tmct0bok.getImntcleu()));
    }
    if (tmct0ord.getImdevcdv() != null && tmct0bok.getImdevcdv() != null) {
      tmct0ord.setImdevcdv(tmct0ord.getImdevcdv().subtract(tmct0bok.getImdevcdv()));
    }
    if (tmct0ord.getImdevcli() != null && tmct0bok.getImdevcli() != null) {
      tmct0ord.setImdevcli(tmct0ord.getImdevcli().subtract(tmct0bok.getImdevcli()));
    }
    if (tmct0ord.getImfibrdv() != null && tmct0bok.getImfibrdv() != null) {
      tmct0ord.setImfibrdv(tmct0ord.getImfibrdv().subtract(tmct0bok.getImfibrdv()));
    }
    if (tmct0ord.getImfibreu() != null && tmct0bok.getImfibreu() != null) {
      tmct0ord.setImfibreu(tmct0ord.getImfibreu().subtract(tmct0bok.getImfibreu()));
    }
    if (tmct0ord.getImsvb() != null && tmct0bok.getImsvb() != null) {
      tmct0ord.setImsvb(tmct0ord.getImsvb().subtract(tmct0bok.getImsvb()));
    }
    if (tmct0ord.getImsvbeu() != null && tmct0bok.getImsvbeu() != null) {
      tmct0ord.setImsvbeu(tmct0ord.getImsvbeu().subtract(tmct0bok.getImsvbeu()));
    }
    if (tmct0ord.getImfpacdv() != null && tmct0bok.getImfpacdv() != null) {
      tmct0ord.setImfpacdv(tmct0ord.getImfpacdv().subtract(tmct0bok.getImfpacdv()));
    }
    if (tmct0ord.getImbensvb() != null && tmct0bok.getImbensvb() != null) {
      tmct0ord.setImbensvb(tmct0ord.getImbensvb().subtract(tmct0bok.getImbensvb()));
    }
    if (tmct0ord.getNutiteje().compareTo(BigDecimal.ZERO) > 0) {
      if (tmct0ord.getImbrmerc() != null) {
        tmct0ord.setImcbmerc(tmct0ord.getImbrmerc().divide(tmct0ord.getNutiteje(), 8, RoundingMode.HALF_UP));
        tmct0ord.setImcbclie(tmct0ord.getImbrmerc().divide(tmct0ord.getNutiteje(), 8, RoundingMode.HALF_UP));
      }
    }
    LOG.trace("[substractEconomicDataFromBooking] fin");
  }

}
