package sibbac.business.titulares.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Service
public class CreacionTablasTiularesWriteLog {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CreacionTablasTiularesWriteLog.class);

  @Autowired
  private CreacionTablasTitularesBo creacionTablasTitularesBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void crearTablasForProcess(Tmct0alcId alcId, TitularesConfigDTO config) {
    LOG.trace("[crearTablasForProcess] inicio {}", alcId);
    try {
      creacionTablasTitularesBo.crearTablasForProcessNewTransaction(alcId, config);
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      String msg = String.format("Alc: '%s' No se han podido crear las tablas de titulares. Err: '%s'",
          alcId.getNucnfliq(), e.getMessage());

      LOG.warn("{} {}", alcId, msg);
      logBo.insertarRegistroNewTransaction(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "",
          config.getAuditUser());
      alcBo.updateCdestadotitByTmct0alcIdAndDownNewTransaction(alcId,
          EstadosEnumerados.TITULARIDADES.INCIDENCIAS.getId());
    }
    LOG.trace("[crearTablasForProcess] fin {}", alcId);
  }
}
