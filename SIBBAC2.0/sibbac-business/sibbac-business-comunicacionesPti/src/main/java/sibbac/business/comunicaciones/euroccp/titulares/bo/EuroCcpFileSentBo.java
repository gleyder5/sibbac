package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.nio.file.Path;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileSentDao;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileTypeDao;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpOwnershipReferenceStaticDataDao;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;

@Service
public class EuroCcpFileSentBo extends AbstractBo<EuroCcpFileSent, Long, EuroCcpFileSentDao> {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpFileSentBo.class);

  @Value("${sibbac.euroccp.realigment.erg.in.file.type.pos}")
  private int POS_FILE_TYPE;
  @Value("${sibbac.euroccp.realigment.erg.in.file.type.length}")
  private int LENGTH_FILE_TYPE;

  @Value("${sibbac.euroccp.realigment.erg.in.file.client.number.pos}")
  private int POS_CLIENT_NUMBER;
  @Value("${sibbac.euroccp.realigment.erg.in.file.client.number.length}")
  private int LENGTH_CLIENT_NUMBER;

  @Value("${sibbac.euroccp.realigment.erg.in.file.sent.date.pos}")
  private int POS_SENT_DATE;
  @Value("${sibbac.euroccp.realigment.erg.in.file.sent.date.length}")
  private int LENGTH_SENT_DATE;

  @Value("${sibbac.euroccp.realigment.erg.in.file.sequence.number.pos}")
  private int POS_SEQUENCE_NUMBER;
  @Value("${sibbac.euroccp.realigment.erg.in.file.sequence.number.length}")
  private int LENGTH_SEQUENCE_NUMBER;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0estadoBo estBo;

  @Autowired
  private Tmct0enviotitularidadDao envioTitularidadDao;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao desgloseCamaraEnvioRtDao;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0paisesDao paisesDao;

  @Autowired
  private Tmct0afiDao afiDao;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private EuroCcpFileTypeDao euroCcpFileTypeDao;

  @Autowired
  private EuroCcpFileSentDao euroCcpSentFileDao;

  @Autowired
  private EuroCcpOwnershipReferenceStaticDataDao euroCcpFileSentLineDao;

  @Autowired
  private SendMail sendMail;

  public EuroCcpFileSentBo() {
  }

  /********************************************************************************* QUERYS ********************************************/

  public List<EuroCcpFileSent> findAllByEstadoProcesado(int estadoProcesado) {
    return dao.findAllByEstadoProcesado(estadoProcesado);
  }

  public List<EuroCcpFileSent> findAllByEstadoProcesadoAndFileType(int estadoProcesado, String fileType) {
    return dao.findAllByEstadoProcesadoAndFileType(estadoProcesado, fileType);
  }

  /********************************************************************************* QUERYS ********************************************/

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public EuroCcpFileSent saveNewFileRegisterIfNotExistsWithoutSentNewTransacction(String basicFileName,
      String clientNumber, int lastClienNumberPosition, String sentDatePattern, EuroCcpFileTypeEnum type) {

    return saveNewFileRegisterIfNotExistsWithoutSent(basicFileName, clientNumber, lastClienNumberPosition,
        sentDatePattern, type);
  }

  public String getEuroCcpOficialFileName(String basicFileName, String clientNumber, int lastClienNumberPosition,
      Date sendDate, String sendDatePattern, int sequence) {
    String baseName = FilenameUtils.getBaseName(basicFileName);
    String extension = FilenameUtils.getExtension(basicFileName);
    String name = String.format("%s%s%s%03d%s%s", baseName,
        clientNumber.substring(clientNumber.length() - lastClienNumberPosition),
        FormatDataUtils.convertDateToString(sendDate, sendDatePattern), sequence, ".", extension);

    return name;
  }

  public String getEuroCcpFileSentBaseName(String fileBaseName) {
    do {
      fileBaseName = FilenameUtils.getBaseName(fileBaseName);
    }
    while (fileBaseName.contains("."));
    return fileBaseName;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public EuroCcpFileSent saveNewFileRegisterIfNotExistsWithoutSent(String basicFileName, String clientNumber,
      int lastClienNumberPosition, String sentDatePattern, EuroCcpFileTypeEnum type) {
    Date sentDate = new Date();
    Integer count = dao.countAllBySentDateAndFileTypeAndEstadoProcesado(sentDate, type.name(),
        ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId(), clientNumber);
    if (count == null || count == 0) {
      return saveNewFileRegisterWithSequenceNumber(basicFileName, clientNumber, lastClienNumberPosition,
          sentDatePattern, type);
    }
    else {
      return dao.findAllBySentDateAndFileTypeAndEstadoProcesadoAndClientNumber(sentDate, type.name(),
          ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId(), clientNumber).get(0);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public EuroCcpFileSent saveNewFileRegister(String clientNumber, Date sentDate, int sequenceNumber, String fileName,
      EuroCcpFileTypeEnum type) {
    EuroCcpFileSent fileSent = new EuroCcpFileSent();
    fileSent.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());
    fileSent.setClientNumber(clientNumber);
    fileSent.setSentDate(sentDate);
    fileSent.setSequenceNumber(sequenceNumber);
    fileSent.setFileType(euroCcpFileTypeDao.findByCdCodigo(type.name()));
    fileSent.setFileBaseName(getEuroCcpFileSentBaseName(fileName));
    fileSent.setFileName(fileName);
    fileSent.setAuditDate(new Date());
    fileSent.setAuditUser("EURO_CCP_S");

    fileSent = save(fileSent);
    return fileSent;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private synchronized EuroCcpFileSent saveNewFileRegisterWithSequenceNumber(String basicFileName, String clientNumber,
      int lastClienNumberPosition, String sentDatePattern, EuroCcpFileTypeEnum type) {
    Date sentDate = new Date();

    Integer sequenceNumber = dao.findMaxSequenceNumberBySentDateAndFileType(sentDate, type.name());
    if (sequenceNumber == null) {
      sequenceNumber = 0;
    }
    else {
      sequenceNumber++;
    }

    String fileName = getEuroCcpOficialFileName(basicFileName, clientNumber, lastClienNumberPosition, sentDate,
        sentDatePattern, sequenceNumber);
    return saveNewFileRegister(clientNumber, sentDate, sequenceNumber, fileName, type);
  }

  /**
   * @param principalFileSent
   * @param fileType
   * @param file
   * @param sentDate
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public EuroCcpFileSent insertTxtTmpFileSentNewTransaction(EuroCcpFileSent principalFileSent,
      EuroCcpFileType fileType, Path file, Date sentDate) {
    return this.insertTxtTmpFileSent(principalFileSent, fileType, file, sentDate);
  }

  /**
   * @param principalFileSent
   * @param fileType
   * @param file
   * @param sentDate
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public EuroCcpFileSent insertTxtTmpFileSent(EuroCcpFileSent principalFileSent, EuroCcpFileType fileType, Path file,
      Date sentDate) {
    EuroCcpFileSent txtTmpFileSent = new EuroCcpFileSent();
    txtTmpFileSent.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
    txtTmpFileSent.setParentFile(principalFileSent);
    txtTmpFileSent.setClientNumber(principalFileSent.getClientNumber());
    txtTmpFileSent.setFilePath(file.toString());
    txtTmpFileSent.setFileName(file.getFileName().toString());
    txtTmpFileSent.setSentDate(principalFileSent.getSentDate());
    txtTmpFileSent.setFileType(fileType);
    txtTmpFileSent.setAuditDate(new Date());
    txtTmpFileSent.setSequenceNumber(0);
    txtTmpFileSent.setAuditUser("EURO_CCP_S");
    txtTmpFileSent = save(txtTmpFileSent);
    return txtTmpFileSent;
  }

  /**
   * @param fileName
   * @param estadoProcesado
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void updateEstadoProcesadoFicheroInFileNameNewTransaction(EuroCcpFileSent fileSent, int estadoProcesado,
      String auditUser) throws SIBBACBusinessException {
    fileSent.setEstadoProcesado(estadoProcesado);
    fileSent.setAuditUser(auditUser);
    fileSent.setAuditDate(new Date());
  }

  /**
   * @param fileName
   * @param estadoProcesado
   * @throws NullPointerException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public EuroCcpFileSent updateEstadoProcesadoNewTransaction(String fileName, int estadoProcesado)
      throws SIBBACBusinessException {
    String baseName = getEuroCcpFileSentBaseName(fileName);

    EuroCcpFileSent fileSent = dao.findByFileBaseName(baseName);
    if (fileSent == null) {
      throw new SIBBACBusinessException(MessageFormat.format("INCIDENCIA-Registro fichero no encontrado {0}", fileName));
    }
    this.updateEstadoProcesadoFicheroInFileNameNewTransaction(fileSent, estadoProcesado, "SIBBAC20");
    return fileSent;
  }

  /**
   * @param fileName
   * @param estadoProcesado
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public EuroCcpFileSent updateEstadoProcesadoFicheroInFileNameNewTransaction(String fileName,
      String sentDateFilePatterh, int estadoProcesado) throws SIBBACBusinessException {
    String fileType = this.getFileTypeFromEuroEccInFileName(fileName);
    String clientNumber = StringUtils.leftPad(this.getClientNumberFromEuroEccInFileName(fileName), 10, "0");
    Date sentDate = null;
    try {
      sentDate = this.getSentDateFromEuroEccInFileName(fileName, sentDateFilePatterh);
    }
    catch (ParseException e) {
      throw new SIBBACBusinessException(MessageFormat.format(
          "INCIDENCIA-No se ha podido recuperar la fecha de envio para el fichero {0} con date pattern {1}", fileName,
          sentDateFilePatterh));
    }
    String sequenceNumber = this.getSequenceNumberFromEuroEccInFileName(fileName);

    Integer sequenceNumberI;
    try {
      sequenceNumberI = Integer.parseInt(sequenceNumber);
    }
    catch (NumberFormatException e) {
      throw new SIBBACBusinessException(
          MessageFormat.format(
              "INCIDENCIA-Registro fichero no encontrado {0} - FileType: {1} - ClientNumber: {2} - SentDate: {3} - SequenceNumber: {4} - {5}",
              fileName, fileType, clientNumber, sentDate, sequenceNumber, e.getMessage()), e);
    }
    LOG.debug(
        "[updateEstadoProcesadoFicheroInFileNameNewTransaction] buscando fichero {} FileType: {} - ClientNumber: {} - SentDate: {} - SequenceNumber: {}",
        fileName, fileType, clientNumber, sentDate, sequenceNumber);
    EuroCcpFileSent fileSent = dao.findBySentDateAndFileTypeAndSequenceNumberAndClientNumber(sentDate, fileType,
        sequenceNumberI, clientNumber);
    if (fileSent == null) {
      throw new SIBBACBusinessException(
          MessageFormat
              .format(
                  "INCIDENCIA-Registro fichero 'EuroCcpFileSent' no encontrado {0} - FileType: {1} - ClientNumber: {2} - SentDate: {3} - SequenceNumber: {4}",
                  fileName, fileType, clientNumber, sentDate, sequenceNumber));
    }
    this.updateEstadoProcesadoFicheroInFileNameNewTransaction(fileSent, estadoProcesado, "SIBBAC20");
    return fileSent;
  }

  private String getSequenceNumberFromEuroEccInFileName(String fileName) {
    String baseName = FilenameUtils.getBaseName(fileName);
    return StringUtils.substring(baseName, POS_SEQUENCE_NUMBER, POS_SEQUENCE_NUMBER + LENGTH_SEQUENCE_NUMBER);
  }

  private String getFileTypeFromEuroEccInFileName(String fileName) {
    String baseName = FilenameUtils.getBaseName(fileName);
    return StringUtils.substring(baseName, POS_FILE_TYPE, POS_FILE_TYPE + LENGTH_FILE_TYPE);
  }

  private String getClientNumberFromEuroEccInFileName(String fileName) {
    String baseName = FilenameUtils.getBaseName(fileName);
    return StringUtils.substring(baseName, POS_CLIENT_NUMBER, POS_CLIENT_NUMBER + LENGTH_CLIENT_NUMBER);
  }

  private String getSentDateFromEuroEccInFileName(String fileName) {
    String baseName = FilenameUtils.getBaseName(fileName);
    return StringUtils.substring(baseName, POS_SENT_DATE, POS_SENT_DATE + LENGTH_SENT_DATE);
  }

  private Date getSentDateFromEuroEccInFileName(String fileName, String pattern) throws ParseException {
    String dateSt = this.getSentDateFromEuroEccInFileName(fileName);
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    return sdf.parse(dateSt);
  }

}