package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccEjecucionBo;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccEjecucionRevisionTitulosRunnableBean")
public class CuadreEccEjecucionRevisionTitulosRunnableBean extends IRunnableBean<CuadreEccIdDTO> {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccEjecucionRevisionTitulosRunnableBean.class);

  @Value("${sibbac.cuadreecc.revision.titulos.crear.desgloses.page.size:10000}")
  private int revisionTitulosCrearDesglosesPageSize;

  @Value("${sibbac.cuadreecc.revision.titulos.maxreg.simple.thread.execution:500}")
  private int maxRegForSimpleThreadExecution;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  public CuadreEccEjecucionRevisionTitulosRunnableBean() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<CuadreEccIdDTO> beans, int order) throws SIBBACBusinessException {
    try {
      cuadreEccEjecucionBo.revisarTitulosEjecuciones(beans, revisionTitulosCrearDesglosesPageSize,
                                                     maxRegForSimpleThreadExecution);
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException("INCIDENCIA - orden: " + order + " beans: " + beans, e);
    } finally {
      getObserver().sutDownThread(this);
    }

  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<CuadreEccIdDTO> clone() {
    return this;
  }

}
