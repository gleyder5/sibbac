package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import sibbac.business.comunicaciones.pti.commons.dto.PtiMessageFileReaderDTO;
import sibbac.pti.PTIMessage;
import sibbac.pti.messages.ti.TI;

public class RevisionTitularesPtiDTO extends PtiMessageFileReaderDTO {

  /**
   * 
   */
  private static final long serialVersionUID = -6553663222448658249L;

  private PTIMessage an;

  public RevisionTitularesPtiDTO(PTIMessage msg) {
    super(msg);
  }

  public void setTi(TI ti) {
    setMsg(ti);
  }

  public TI getTi() {
    return (TI) getMsg();
  }

  public PTIMessage getAn() {
    return an;
  }

  public void setAn(PTIMessage an) {
    this.an = an;
  }

  @Override
  public String toString() {
    return "RevisionTitularesPtiDTO [an=" + an + ", toString()=" + super.toString() + "]";
  }

}
