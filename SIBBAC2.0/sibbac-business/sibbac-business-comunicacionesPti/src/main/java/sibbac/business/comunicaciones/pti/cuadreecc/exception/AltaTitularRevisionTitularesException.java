package sibbac.business.comunicaciones.pti.cuadreecc.exception;

import sibbac.common.SIBBACBusinessException;

public class AltaTitularRevisionTitularesException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = 4269242590343506993L;

  public AltaTitularRevisionTitularesException() {
    // TODO Auto-generated constructor stub
  }

  public AltaTitularRevisionTitularesException(String message,
                                               Throwable cause,
                                               boolean enableSuppression,
                                               boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

  public AltaTitularRevisionTitularesException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public AltaTitularRevisionTitularesException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public AltaTitularRevisionTitularesException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
