package sibbac.business.comunicaciones.traspasos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jms.ConnectionFactory;

import sibbac.pti.PTIMessageVersion;

public class TraspasosEccConfigDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -44730660247005373L;

  private List<Long> listNudesgloses;
  private List<String> listCdoperacionecc;

  private String ctaCompensacionOrigen;
  private String ctaCompensacionDestino;

  private String accountNumberEuroCcpDestino;

  private String miembroDestino;
  private String refAsignacionGiveUp;

  private ConnectionFactory ptiConnectionFactory;
  private String ptiMessageDestintion;
  private PTIMessageVersion ptiMessageVersion;
  private String camaraNacional;
  private boolean ptiSessionOpen;

  private BigDecimal horaCorteTraspaso;
  private BigDecimal horaCorteGiveUp;
  private BigDecimal horaCorteTraspasoMab;
  private BigDecimal horaCorteGiveUpMab;

  private String camaraEuroccp;

  private BigDecimal horaCorteTraspasoEuroccp;

  private String auditUser;

  private int estadoEnviandose;

  private final List<String> estadosEccVivos = new ArrayList<String>();

  private boolean isTraspaso = true;

  private Character corretajePti;

  private List<Date> holydays;

  private List<Integer> workDaysOfWeek;

  private List<Integer> estadosAsignacionNoEnviarTraspasos;

  private Integer cdestadoentrecGtNoEnviarTraspasos;

  private List<String> segmentosMab;

  private List<String> segmentosSinEcc;

  public TraspasosEccConfigDTO() {
  }

  public ConnectionFactory getConnectionFactory() {
    return ptiConnectionFactory;
  }

  public List<Long> getListNudesgloses() {
    return listNudesgloses;
  }

  public List<String> getListCdoperacionecc() {
    return listCdoperacionecc;
  }

  public String getCtaCompensacionOrigen() {
    return ctaCompensacionOrigen;
  }

  public String getCtaCompensacionDestino() {
    return ctaCompensacionDestino;
  }

  public String getMiembroDestino() {
    return miembroDestino;
  }

  public String getRefAsignacionGiveUp() {
    return refAsignacionGiveUp;
  }

  public PTIMessageVersion getPtiMessageVersion() {
    return ptiMessageVersion;
  }

  public String getCamaraNacional() {
    return camaraNacional;
  }

  public boolean isPtiSessionOpen() {
    return ptiSessionOpen;
  }

  public BigDecimal getHoraCorteTraspaso() {
    return horaCorteTraspaso;
  }

  public BigDecimal getHoraCorteGiveUp() {
    return horaCorteGiveUp;
  }

  public BigDecimal getHoraCorteTraspasoMab() {
    return horaCorteTraspasoMab;
  }

  public BigDecimal getHoraCorteGiveUpMab() {
    return horaCorteGiveUpMab;
  }

  public String getCamaraEuroccp() {
    return camaraEuroccp;
  }

  public BigDecimal getHoraCorteTraspasoEuroccp() {
    return horaCorteTraspasoEuroccp;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public void setConnectionFactory(ConnectionFactory connectionFactory) {
    this.ptiConnectionFactory = connectionFactory;
  }

  public void setListNudesgloses(List<Long> listNudesgloses) {
    this.listNudesgloses = listNudesgloses;
  }

  public void setListCdoperacionecc(List<String> listCdoperacionecc) {
    this.listCdoperacionecc = listCdoperacionecc;
  }

  public void setCtaCompensacionOrigen(String ctaCompensacionOrigen) {
    this.ctaCompensacionOrigen = ctaCompensacionOrigen;
  }

  public void setCtaCompensacionDestino(String ctaCompensacionDestino) {
    this.ctaCompensacionDestino = ctaCompensacionDestino;
  }

  public void setMiembroDestino(String miembroDestino) {
    this.miembroDestino = miembroDestino;
  }

  public void setRefAsignacionGiveUp(String refAsignacionGiveUp) {
    this.refAsignacionGiveUp = refAsignacionGiveUp;
  }

  public void setPtiMessageVersion(PTIMessageVersion ptiMessageVersion) {
    this.ptiMessageVersion = ptiMessageVersion;
  }

  public void setCamaraNacional(String camaraNacional) {
    this.camaraNacional = camaraNacional;
  }

  public void setPtiSessionOpen(boolean ptiSessionOpen) {
    this.ptiSessionOpen = ptiSessionOpen;
  }

  public void setHoraCorteTraspaso(BigDecimal horaCorteTraspaso) {
    this.horaCorteTraspaso = horaCorteTraspaso;
  }

  public void setHoraCorteGiveUp(BigDecimal horaCorteGiveUp) {
    this.horaCorteGiveUp = horaCorteGiveUp;
  }

  public void setHoraCorteTraspasoMab(BigDecimal horaCorteTraspasoMab) {
    this.horaCorteTraspasoMab = horaCorteTraspasoMab;
  }

  public void setHoraCorteGiveUpMab(BigDecimal horaCorteGiveUpMab) {
    this.horaCorteGiveUpMab = horaCorteGiveUpMab;
  }

  public void setCamaraEuroccp(String camaraEuroccp) {
    this.camaraEuroccp = camaraEuroccp;
  }

  public void setHoraCorteTraspasoEuroccp(BigDecimal horaCorteTraspasoEuroccp) {
    this.horaCorteTraspasoEuroccp = horaCorteTraspasoEuroccp;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public String getPtiMessageDestintion() {
    return ptiMessageDestintion;
  }

  public void setPtiMessageDestintion(String ptiMessageDestintion) {
    this.ptiMessageDestintion = ptiMessageDestintion;
  }

  public String getAccountNumberEuroCcpDestino() {
    return accountNumberEuroCcpDestino;
  }

  public void setAccountNumberEuroCcpDestino(String accountNumberEuroCcpDestino) {
    this.accountNumberEuroCcpDestino = accountNumberEuroCcpDestino;
  }

  public int getEstadoEnviandose() {
    return estadoEnviandose;
  }

  public void setEstadoEnviandose(int estadoEnviandose) {
    this.estadoEnviandose = estadoEnviandose;
  }

  public List<String> getEstadosEccVivos() {
    return estadosEccVivos;
  }

  public boolean isTraspaso() {
    return isTraspaso;
  }

  public void setTraspaso(boolean isTraspaso) {
    this.isTraspaso = isTraspaso;
  }

  public Character getCorretajePti() {
    return corretajePti;
  }

  public void setCorretajePti(Character corretajePti) {
    this.corretajePti = corretajePti;
  }

  public List<Date> getHolydays() {
    return holydays;
  }

  public List<Integer> getWorkDaysOfWeek() {
    return workDaysOfWeek;
  }

  public void setHolydays(List<Date> holydays) {
    this.holydays = holydays;
  }

  public void setWorkDaysOfWeek(List<Integer> workDaysOfWeek) {
    this.workDaysOfWeek = workDaysOfWeek;
  }

  public List<Integer> getEstadosAsignacionNoEnviarTraspasos() {
    return estadosAsignacionNoEnviarTraspasos;
  }

  public Integer getCdestadoentrecGtNoEnviarTraspasos() {
    return cdestadoentrecGtNoEnviarTraspasos;
  }

  public void setEstadosAsignacionNoEnviarTraspasos(List<Integer> estadosAsignacionNoEnviarTraspasos) {
    this.estadosAsignacionNoEnviarTraspasos = estadosAsignacionNoEnviarTraspasos;
  }

  public void setCdestadoentrecGtNoEnviarTraspasos(Integer cdestadoentrecGtNoEnviarTraspasos) {
    this.cdestadoentrecGtNoEnviarTraspasos = cdestadoentrecGtNoEnviarTraspasos;
  }

  public List<String> getSegmentosMab() {
    return segmentosMab;
  }

  public void setSegmentosMab(List<String> segmentosMab) {
    this.segmentosMab = segmentosMab;
  }

  public List<String> getSegmentosSinEcc() {
    return segmentosSinEcc;
  }

  public void setSegmentosSinEcc(List<String> segmentosSinEcc) {
    this.segmentosSinEcc = segmentosSinEcc;
  }

}
