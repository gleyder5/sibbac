package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.bo.ReglasCanonesBo;
import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.comunicaciones.pti.commons.EnumeradosComunicacionesPti.TipoCuentaIberclear;
import sibbac.business.comunicaciones.pti.commons.enums.CodigoMiembroCompensadorSV;
import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccSibbacDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.DatosAsignacionIntermediarioFinancieroDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.DatosDesgloseIntermediarioFinancieroDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionCuadreEccDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.InformacionCompensacionDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccTitular;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.bo.Tval0ps2Bo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.DesgloseTitularBO;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0TipoCuentaDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientooperacionnuevaeccBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0referenciaTitularBo;
import sibbac.business.wrappers.database.bo.Tmct0tdcBo;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0alcHelper;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Desglose;
import sibbac.business.wrappers.database.model.Tmct0DesgloseTitular;
import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0afiId;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.helper.CloneObjectHelper;
import sibbac.database.bo.AbstractBo;

@Service
public class CuadreEccSibbacBo extends AbstractBo<CuadreEccSibbac, Long, CuadreEccSibbacDao> {

  private static final String INTERMEDIARIO_FINANCIERO = "IF";

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccSibbacBo.class);

  @Autowired
  private Tmct0ValoresBo valoresBo;
  @Autowired
  private Tmct0aliBo aliasBo;

  /**
   * Bussines Object para la cabecera de los mensajes TI de IF
   * */
  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;
  /**
   * Bussines Object para los datos de titulares de TI de IF
   * */
  @Autowired
  private CuadreEccTitularBo cuadreEccTitularBo;

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  /**
   * Bussines Object para la creacion de los titulares de los desgloses
   * */
  @Autowired
  private DesgloseTitularBO desgloseTitularBO;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0ejeBo ejeBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0referenciaTitularBo referenciaTitularBo;

  @Autowired
  private Tmct0tdcBo tmct0tdcBo;

  @Autowired
  private DesgloseTmct0alcHelper desgloseAlcHelper;

  @Autowired
  private Tmct0estadoBo estBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNuevaBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentasDeCompensacionBO;

  @Autowired
  private Tmct0TipoCuentaDeCompensacionBo tiposCuentaCompensacionBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;
  @Autowired
  private Tmct0logBo tmct0logBo;
  @Autowired
  private Tval0ps2Bo tval0ps2Bo;
  @Autowired
  private Tmct0aloBo tmct0aloBo;
  @Autowired
  private Tmct0alcBo tmct0alcBo;
  @Autowired
  private Tmct0desgloseclititBo tmct0desgloseclititBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejealoBo;

  @Autowired
  private Tmct0xasBo conversionesBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private ReglasCanonesBo reglasCanonesBo;

  @Autowired
  private ApplicationContext ctx;

  public CuadreEccSibbacBo() {
    super();
  }

  public List<CuadreEccSibbac> findAll() {
    return dao.findAll();
  }

  public List<CuadreEccSibbac> findAllEIF() {
    return dao.findAllByEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
  }

  public Integer countByCdestadotitPdteIf() {
    return dao.countByEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
  }

  public CuadreEccSibbac findById(final Long id) {
    return dao.findById(id);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Integer deleteById(Long id) {
    cuadreEccLogDao.deleteByIdCuadreEccSibbac(id);
    return dao.deleteById(id);
  }

  public List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperac(Character sentido, String numOp, java.util.Date feoperac) {
    return dao.findBySentidoAndNumOpAndFeoperac(sentido, numOp, feoperac);
  }

  public List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndIdCuadreEccEjecucion(Character sentido, String numOp,
      java.util.Date feoperac, long idCuadreEccEjecucion) {
    return dao.findBySentidoAndNumOpAndFeoperacAndIdCuadreEccEjecucion(sentido, numOp, feoperac, idCuadreEccEjecucion);
  }

  public List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndNumTitulosDesPdte(Character sentido, String numOp,
      java.util.Date feoperac, BigDecimal numTitulosDesPdte) {
    return dao.findBySentidoAndNumOpAndFeoperacAndNumTitulosDesSibbacPdte(sentido, numOp, feoperac, numTitulosDesPdte);
  }

  public List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndRefTitular(Character sentido, String numOp,
      java.util.Date feoperac, String refTitular) {
    return dao.findBySentidoAndNumOpAndFeoperacAndRefTitular(sentido, numOp, feoperac, refTitular);
  }

  public List<CuadreEccSibbac> findBySentidoAndNumOpAndFeoperacAndRefTitularAndNumTitulosDesPdte(Character sentido,
      String numOp, java.util.Date feoperac, String refTitular, BigDecimal numTitulosDesPdte) {
    return dao.findBySentidoAndNumOpAndFeoperacAndRefTitularAndNumTitulosDesSibbacPdte(sentido, numOp, feoperac,
        refTitular, numTitulosDesPdte);
  }

  public List<Long> findAllIdsByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(
      Date feoperac, String cdcamara, String cdisin, String cdmiembromkt, Character sentido, String cdsegmento,
      String cdoperacionmkt, String nuordenmkt, String nuexemkt) {
    return dao
        .findAllIdsByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(
            feoperac, cdcamara, cdisin, cdmiembromkt, sentido, cdsegmento, cdoperacionmkt, nuordenmkt, nuexemkt);

  }

  public List<CuadreEccSibbac> findByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(
      Date feoperac, String cdcamara, String cdisin, String cdmiembromkt, Character sentido, String cdsegmento,
      String cdoperacionmkt, String nuordenmkt, String nuexemkt) {
    return dao
        .findByFeoperacAndCdcamaraAndCdisinAndCdmiembromktAndSentidoAndCdsegmentoAndCdoperacionmktAndNuordenmktAndNuexemkt(
            feoperac, cdcamara, cdisin, cdmiembromkt, sentido, cdsegmento, cdoperacionmkt, nuordenmkt, nuexemkt);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Integer deleteByNuordenAndNbookingAndNucnflct(final String nuorden, final String nbooking,
      final String nucnfclt) {
    int deletes = 0;
    List<Long> ids = dao.findAllIdsByNuordenAndNbookingAndNucnfclt(nuorden, nbooking, nucnfclt);
    for (Long long1 : ids) {
      cuadreEccLogDao.deleteByIdCuadreEccSibbac(long1);
      deletes += dao.deleteById(long1);
    }
    return deletes;
  }

  /**
   * Encuentra los CuadreEccSibbac by allo
   * */
  public List<CuadreEccSibbac> findAlos(final String nuorden, final String nbooking, final String nucnfclt) {
    return dao.findAlos(nuorden, nbooking, nucnfclt);
  }

  /*
   * public List< Tmct0alc > getAlcs( final String nuorden, final String
   * nbooking, final String nucnfclt ) { return dao.getAlcs( nuorden, nbooking,
   * nucnfclt ); }
   */

  public Tmct0alc getAlc(final String nuorden, final String nbooking, final String nucnfclt, final short nucnfliq) {
    return this.tmct0alcBo.getAlc(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  public List<Tmct0desglosecamara> getDcs(final String nuorden, final String nbooking, final String nucnfclt,
      final short nucnfliq) {
    return dao.getDcs(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  public List<CuadreEccDbReaderRecordBeanDTO> findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalor(
      Integer estadoProcesado, Date fevalor, Pageable page) {
    return dao.findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalor(estadoProcesado, fevalor, page);
  }

  public Integer updateEstadoProcesadoByNuordenAndNbookingAndNucnfclt(Integer setEstadoProcesado, String nuorden,
      String nbooking, String nucnflct) {
    return dao.updateEstadoProcesadoByNuordenAndNbookingAndNucnfclt(setEstadoProcesado, new Date(), nuorden, nbooking,
        nucnflct);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Integer updateEstadoProcesadoByNuordenAndNbookingAndNucnfcltNewTransaction(Integer setEstadoProcesado,
      String nuorden, String nbooking, String nucnflct) {
    return dao.updateEstadoProcesadoByNuordenAndNbookingAndNucnfclt(setEstadoProcesado, new Date(), nuorden, nbooking,
        nucnflct);
  }

  public List<CuadreEccDbReaderRecordBeanDTO> findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte(
      Integer estadoProcesado, Date fevalorLte, int maxRegs) {

    LOG.trace("[CuadreEccSibbacBo :: findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte] inicio");
    Pageable page = new PageRequest(0, maxRegs);
    List<CuadreEccDbReaderRecordBeanDTO> res = new ArrayList<CuadreEccDbReaderRecordBeanDTO>();
    List<CuadreEccDbReaderRecordBeanDTO> resAux;
    LOG.trace(
        "[CuadreEccSibbacBo :: findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte] buscando estado {}",
        estadoProcesado);
    List<Date> dates = dao.findAllDistinctFevalorByEstadoProcesado(estadoProcesado);
    if (CollectionUtils.isNotEmpty(dates)) {
      Date now = new Date();
      for (Date date : dates) {
        if (date.compareTo(now) <= 0) {
          LOG.debug(
              "[CuadreEccSibbacBo :: findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte] date: {} estadoProcesado: {}",
              date, estadoProcesado);
          resAux = findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalor(estadoProcesado, date, page);
          LOG.debug(
              "[CuadreEccSibbacBo :: findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte] date: {}, found: {} estadoProcesado: {}",
              date, resAux.size(), estadoProcesado);
          if (CollectionUtils.isNotEmpty(resAux)) {
            res.addAll(resAux);
          }
          if (res.size() >= maxRegs) {
            break;
          }
        }
      }
    }
    LOG.trace("[CuadreEccSibbacBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte] fin");
    return res;
  }

  public List<CuadreEccIdDTO> findAllCuadreEccIdDTOByEstadoProcesadoAndFevalor(Integer estadoProcesado, Date fevalor) {
    return dao.findAllCuadreEccIdDTOByEstadoProcesadoAndFevalor(estadoProcesado, fevalor);
  }

  public List<CuadreEccIdDTO> findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte(Integer estadoProcesado,
      Date fevalorLte) {
    LOG.trace("[CuadreEccSibbacBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte] inicio");

    List<CuadreEccIdDTO> res = new ArrayList<CuadreEccIdDTO>();
    List<CuadreEccIdDTO> resAux;
    LOG.trace("[CuadreEccSibbacBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte] buscando estado {}",
        estadoProcesado);
    List<Date> dates = dao.findAllDistinctFevalorByEstadoProcesado(estadoProcesado);
    if (CollectionUtils.isNotEmpty(dates)) {
      Date now = new Date();
      for (Date date : dates) {
        if (date.compareTo(now) <= 0) {
          LOG.debug(
              "[CuadreEccSibbacBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte] date: {} estadoProcesado: {}",
              date, estadoProcesado);
          resAux = findAllCuadreEccIdDTOByEstadoProcesadoAndFevalor(estadoProcesado, date);
          LOG.debug(
              "[CuadreEccSibbacBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte] date: {}, found: {} estadoProcesado: {}",
              date, resAux.size(), estadoProcesado);
          if (CollectionUtils.isNotEmpty(resAux)) {
            res.addAll(resAux);
          }
        }
      }
    }
    LOG.trace("[CuadreEccSibbacBo :: findAllCuadreEccIdDTOByEstadoProcesadoAndFevalorLte] fin");
    return res;
  }

  public List<CuadreEccDbReaderRecordBeanDTO> findAllCuadreEccIdDTOForCargaOperacionesSibbacTask(int maxRegs) {
    List<CuadreEccDbReaderRecordBeanDTO> beans = findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte(
        EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_CARGAR_CODIGOS_OP_MOVIMIENTOS.getId(), new Date(), maxRegs);
    return beans;
  }

  public List<CuadreEccDbReaderRecordBeanDTO> findAllCuadreEccIdDTOForArreglarDesglosesSibbacTask(int maxRegs) {
    List<CuadreEccDbReaderRecordBeanDTO> beans = findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte(
        EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_ARREGLAR_DESGLOSES.getId(), new Date(), maxRegs);
    return beans;
  }

  /**
   * Ecuentra o crea elementos IFinancieros
   * 
   * @throws Exception
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = { Exception.class })
  public void crearCuadreEccSibbac(List<Tmct0aloId> alos) throws Exception {

    /**
     * Se tiene que evaluar si se tienen que grabar los registros por alo Se
     * podría comprobar si ya existe el elemento ( y actualizar algunos valores
     * porque ya se lleva la contabilidad de los encontrados)
     * 
     * Buscamos en la tabla de ALO's...
     * 
     */

    if (CollectionUtils.isNotEmpty(alos)) {
      LOG.trace("[CuadreEccSibbacBo] alos found {}", alos.size());
      List<CuadreEccSibbac> lifinanciero;
      CuadreEccSibbac ifinanciero;
      List<Tmct0alc> alcs;
      String nuorden, nbooking, nucnfclt;
      Tmct0alo alo;
      for (Tmct0aloId id : alos) {
        try {
          nuorden = id.getNuorden();
          nbooking = id.getNbooking();
          nucnfclt = id.getNucnfclt();

          lifinanciero = new ArrayList<CuadreEccSibbac>();
          /* Se busca una lista con elementes ifinancieros dada su clave */
          List<CuadreEccSibbac> lelementos_if = dao.findAllCuadreEccSibbacByNuordenAndNbookingAndNucnfclt(nuorden,
              nbooking, nucnfclt);
          /*
           * si hay elementos en la lista, quiere dicí que esos elementos
           * ifinancieros ya están insertados, por tanto no se sigue procesando
           * y se avanza al siguiente elemento en el for
           */
          if (CollectionUtils.isNotEmpty(lelementos_if)) {
            LOG.trace(
                "[CuadreEccSibbacBo] alo [ NUORDEN-{} - NBOOKING:{} - NUCNFCLT:{}] in Intermediarios Financieros already inserted",
                nuorden, nbooking, nucnfclt);
          }
          else {
            alo = tmct0aloBo.findById(id);
            // NO esta en la tabla. Lo creamos.
            ifinanciero = creaElementoIFinancieroPorAlo(nuorden, nbooking, nucnfclt, alo);
            LOG.trace("[CuadreEccSibbacBo] processing alo [ NUORDEN-{} - NBOOKING:{} - NUCNFCLT:{}] ", nuorden,
                nbooking, nucnfclt);

            /**
             * Todos los que NO esten RECHAZADOS.
             */
            alcs = tmct0alcBo.getAlcs(nuorden, nbooking, nucnfclt, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
            if (CollectionUtils.isEmpty(alcs)) {
              // Hay IF pero no ALC. ERROR.
              LOG.trace("[CuadreEccSibbacBo] ponemos error=true porque no hay alcs");
            }
            else {
              LOG.trace("[CuadreEccSibbacBo] {} alcs found ", alcs.size());
              for (Tmct0alc alc : alcs) {
                /**
                 * consultar si el alo esta grabado en Elementos_ifinanciero si
                 * esta continue, si no pues continuamos
                 */
                createCuadreEccSibbac(lifinanciero, ifinanciero, alc);
              }

            }

          }
          if (lifinanciero.size() > 0) {
            dao.save(lifinanciero);
          }
          else {
            LOG.trace("[CuadreEccSibbacBo] not inserted new elements \"Intermediario Financiero\" ");
          }
        }
        catch (Exception e) {
          throw new SIBBACBusinessException("[CuadreEccSibbacBo] incidencia con el alo.id == " + id, e);
        }
      } // del for de los alos

    }
    else {
      LOG.trace("[CuadreEccSibbacBo]  alo`s  in estado PDTE_IF ");
    }
  }

  @Transactional
  private List<CuadreEccSibbac> saveByTransaction(List<CuadreEccSibbac> lifinanciero) {
    try {
      lifinanciero = dao.save(lifinanciero);
    }
    catch (PersistenceException ex) {

      LOG.warn("[saveByTransaction] Incidencia Cause: {} msg: {}", ex.getCause(), ex.getMessage());
      LOG.trace(ex.getMessage(), ex);
    }
    return lifinanciero;
  }

  /**
   * Crea elementos ifinancieros tomando como patrón el pasado por parámetros y
   * los mete en la lista que también es pasada por parámetro lifinanciero
   * 
   * @param lCuadreEccSibbac
   * @paramifinanciero
   * @param alo
   */
  @Transactional
  private void createCuadreEccSibbac(List<CuadreEccSibbac> lCuadreEccSibbac, CuadreEccSibbac cuadreEccSibbac,
      Tmct0alc alc) {
    List<Tmct0desglosecamara> ldesglosecamaras;
    List<Tmct0desgloseclitit> ldesgloseclitits;
    String nuorden, nbooking, nucnfclt;
    List<Long> liddesglose;
    boolean error = false;
    /* inicializar variables del id */
    nuorden = cuadreEccSibbac.getNuorden();
    nbooking = cuadreEccSibbac.getNbooking();
    nucnfclt = cuadreEccSibbac.getNucnfclt();
    /* END */
    cuadreEccSibbac.setRefTitular(alc.getCdreftit());
    int valor = new Integer(alc.getId().getNucnfliq());
    ldesglosecamaras = dao.getDcs(nuorden, nbooking, nucnfclt, (short) valor);
    if (CollectionUtils.isEmpty(ldesglosecamaras)) {
      error = true;
      LOG.trace("[createCuadreEccSibbac] ponemos error=true porque no hay ldesglosecamaras");
    }
    else {
      liddesglose = new ArrayList<Long>();
      for (Tmct0desglosecamara dc : ldesglosecamaras) {
        liddesglose.add(dc.getIddesglosecamara());
        LOG.trace("[createCuadreEccSibbac] clitit for iddesglosecamara ", dc.getIddesglosecamara());
      }
      ldesgloseclitits = tmct0desgloseclititBo.findByIddesglosecamaraIn(liddesglose);
      if (CollectionUtils.isEmpty(ldesgloseclitits)) {
        error = true;
        LOG.trace("[createCuadreEccSibbac] ponemos error=true porque no hay ldesgloseclitits");
      }
      else {
        for (Tmct0desgloseclitit dct : ldesgloseclitits) {
          LOG.trace("[createCuadreEccSibbac] desglose clitit {}", dct.getNudesglose());
          if (dct.getFeejecuc() != null) {
            error = cumplimentarDatosCuadreEccSibbacPorDesgloseClitit(cuadreEccSibbac, dct);
          }
          else {
            error = true;
            LOG.trace("[createCuadreEccSibbac] ponemos error=true porque dct.Feejecuc es nulo");
          }
          if (!error) {

            CuadreEccSibbac elemento_if = completarElementoIFinanciero(cuadreEccSibbac);
            lCuadreEccSibbac.add(elemento_if);
          }
          else {
            LOG.trace("[createCuadreEccSibbac] not enough information to generate a CuadreEccSibbac item ");
          }
        }
      }
    }
  }

  /**
   * setea valores en el objeto ifinanciero dado un objeto desgloseclitit
   * 
   * @param ifinanciero
   * @param dct
   * @return
   */
  @Transactional
  private boolean cumplimentarDatosCuadreEccSibbacPorDesgloseClitit(CuadreEccSibbac ifinanciero, Tmct0desgloseclitit dct) {
    boolean error = false;
    // Set<Tmct0movimientooperacionnuevaecc> loperacionesecc;
    String cdoperacion;
    ifinanciero.setFeoperac(dct.getFeejecuc());
    ifinanciero.setNumTitulosDesSibbac(dct.getImtitulos());
    ifinanciero.setNumTitulosDesSibbacPdte(dct.getImtitulos());
    ifinanciero.setSentido(dct.getCdsentido());

    cdoperacion = dct.getNuoperacionprev().trim();

    String cdoperacioNueva = dct.getCdoperacionecc();

    /**
     * Este caso puede darse en un funcionamiento correcto que el cdoperacion
     * venga a nulo. hay que revisar si hay que concatenar los valores de fecha
     * FEORDENMKT NUORDENMKT NUREFEXEMKT
     */
    if (StringUtils.isEmpty(cdoperacion)) {
      error = true;
      LOG.trace("[cumplimentarDatosCuadreEccSibbacPorDesgloseClitit] ponemos error=true porque no hay cdoperacion");
    }
    else {
      ifinanciero.setNumOp(cdoperacion);
      if (cdoperacioNueva != null) {
        ifinanciero.setNumOp(cdoperacioNueva);
      }
      else {
        ifinanciero.setNumOp("");
      }
      // loperacionesecc = dao.getMvs(dct.getNudesglose());

      // setearCdoperacionECCNueva(ifinanciero, loperacionesecc, cdoperacion);
    }
    return error;
  }

  /**
   * Crea un nuevo elemento IFinanciero a partir del que se ha pasado por
   * parámetros
   * 
   * @param ifinanciero
   * @return
   */
  @Transactional
  private CuadreEccSibbac completarElementoIFinanciero(CuadreEccSibbac ifinanciero) {
    CuadreEccSibbac elemento_if = new CuadreEccSibbac();
    elemento_if.setEstado(ifinanciero.getEstado());
    elemento_if.setNuorden(ifinanciero.getNuorden());
    elemento_if.setNbooking(ifinanciero.getNbooking());
    elemento_if.setNucnfclt(ifinanciero.getNucnfclt());
    elemento_if.setNumTitulosDesSibbac(ifinanciero.getNumTitulosDesSibbac());
    elemento_if.setNumTitulosDesSibbacPdte(ifinanciero.getNumTitulosDesSibbacPdte());
    elemento_if.setRefTitular(ifinanciero.getRefTitular());
    elemento_if.setFeoperac(ifinanciero.getFeoperac());
    elemento_if.setNumTitulosDesEcc(BigDecimal.ZERO);
    elemento_if.setNumTitulosDesEccPdte(BigDecimal.ZERO);
    elemento_if.setNumOp(ifinanciero.getNumOp());
    return elemento_if;
  }

  /**
   * Instancia CuadreEccSibbac por allo
   */
  @Transactional
  private CuadreEccSibbac creaElementoIFinancieroPorAlo(String nuorden, String nbooking, String nucnfclt, Tmct0alo alo) {
    CuadreEccSibbac ifinanciero;
    ifinanciero = new CuadreEccSibbac();
    ifinanciero.setEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
    ifinanciero.setNuorden(nuorden);
    ifinanciero.setNbooking(nbooking);
    ifinanciero.setNucnfclt(nucnfclt);
    ifinanciero.setNumTitulosDesSibbac(alo.getNutitcli());
    ifinanciero.setNumTitulosDesSibbacPdte(alo.getNutitcli());
    return ifinanciero;
  }

  /**
   * Inserta los datos en un DesgloseTitular
   * 
   * @param now
   * @param auditUser
   * @param eccmsg
   * @param desglose
   * @param msgTit
   * @param titular
   */
  @Transactional
  private void cumplimentarDatosTitulares(Date now, String auditUser, CuadreEccReferencia eccmsg,
      Tmct0Desglose desglose, CuadreEccTitular msgTit, Tmct0DesgloseTitular titular) {
    titular.setIdDesglose(desglose);
    titular.setAuditDate(now);
    titular.setAuditUser(auditUser);
    /* Este dato está en ISO y hay que pasarlo al código del Banco de España */
    String codPaisIso = convertirCodigoResidenciaAEstandardSpanishBank(eccmsg);
    titular.setCddepais(codPaisIso);
    titular.setCdestado('A');
    titular.setCdnactit(msgTit.getPaisNacionalidad());
    titular.setCdpostal(eccmsg.getCodPostal());
    titular.setNbciudad(extractMunicipio(eccmsg.getPoblacion()));
    if (msgTit.getIndPersonaFisicaJuridica() != null && 'F' == msgTit.getIndPersonaFisicaJuridica()) { // F-FISICA/J-JURIDICA
      titular.setNbclient(msgTit.getNombreRazonSocial());
      titular.setNbclient1(msgTit.getPrimerApellido());
      titular.setNbclient2(msgTit.getSegundoApellido());
    }
    else {
      titular.setNbclient1(msgTit.getNombreRazonSocial());
      // titular.setNbclient("");
      // titular.setNbclient2("");
    }

    titular.setNbdomici(eccmsg.getDomicilio());
    String provincia = extractProvincia(eccmsg.getPoblacion());
    titular.setNbprovin(provincia);
    titular.setNudomici("");

    titular.setTpdomici(eccmsg.getDomicilio().substring(0, 2));
    if (msgTit.getIndNacionalidad() != null) {
      titular.setTpnactit(msgTit.getIndNacionalidad());
      if (msgTit.getIndNacionalidad() == 'U') {// T-TITULAR/R-REPRESENTANTE/U-USUFRUCTUARIO/I-INCAPACITADO
        titular.setParticip(msgTit.getPorcentajeUsufructo());
      }
      else {
        titular.setParticip(msgTit.getPorcentajePropiedad());
      }
    }
    if (msgTit.getTipoIdentificacion() != null) {
      titular.setTpidenti(msgTit.getTipoIdentificacion());
    }
    if (msgTit.getIndPersonaFisicaJuridica() != null) {
      titular.setTpsocied(msgTit.getIndPersonaFisicaJuridica());
    }
    if (msgTit.getTipoIdentificacion() != null) {
      titular.setTptiprep(msgTit.getTipoTitular());
    }
    titular.setNbidenti(msgTit.getIdentificacion());
  }

  /**
   * Inserta los datos en un Desglose
   * 
   * @param alo
   * @param idCuentaCompensacion
   * @param now
   * @param auditUser
   * @param eccmsg
   * @param desglose
   */
  @Transactional
  private void cumplimentarDesglose(String nuorden, String nbooking, String nucnfclt,
      DatosDesgloseIntermediarioFinancieroDTO datosDesglose, Date now, String auditUser, CuadreEccReferencia eccmsg,
      Tmct0Desglose desglose) {
    DatosAsignacionIntermediarioFinancieroDTO datosAsignacion = datosDesglose.getDatosAsignacion();
    desglose.setIdProcedencia(INTERMEDIARIO_FINANCIERO);
    desglose.setCdestado('A');// A-ALTA/B-BAJA/M-MODIFICACION/X-FINALIZADO
    desglose.setNuorden(nuorden);
    desglose.setNbooking(nbooking);
    desglose.setNucnfclt(nucnfclt);
    desglose.setDesglose('D'); // valores
    // M-MANUAL/P-PROVISIONAL/D-DEFINITIVO
    desglose.setReferenciaTitular(eccmsg.getRefTitular());
    desglose.setNutitulos(eccmsg.getNumValoresImpNominal());
    desglose.setTmct0DesgloseTitularList(new LinkedList<Tmct0DesgloseTitular>());
    desglose.setAuditDate(now);
    desglose.setAuditUser(auditUser);
    if (datosAsignacion.getIdCuentaCompensacion() != null) {
      desglose.setIdCuentaCompensacion(new BigInteger(datosAsignacion.getIdCuentaCompensacion() + ""));
    }
    if (datosAsignacion.getIdMiembroCompensador() != null) {
      desglose.setIdCompensador(new BigInteger(datosAsignacion.getIdMiembroCompensador() + ""));
    }

    desglose.setCdReferenciaAsignacion(datosAsignacion.getReferenciaAsignacionMiembroCompensador());

    if (StringUtils.isNotEmpty(datosDesglose.getCodigoEntidadCustoria())
        && StringUtils.isNotBlank(datosDesglose.getCodigoEntidadCustoria())) {
      desglose.setCustodian(datosDesglose.getCodigoEntidadCustoria());
    }
    else if (StringUtils.isNotEmpty(datosDesglose.getCodigoEntidadLiquidadora())
        && StringUtils.isNotBlank(datosDesglose.getCodigoEntidadLiquidadora())) {
      desglose.setCustodian(datosDesglose.getCodigoEntidadLiquidadora());
    }
    desglose.setCdrefban(datosDesglose.getReferenciaBancaria());
    desglose.setAcctatcle(datosDesglose.getCuentaLiquidacion());
    desglose.setPccombco(datosDesglose.getPorcentajeComisionBanco());
    desglose.setPccomdev(datosDesglose.getPorcentajeComisionOrdenante());
    desglose.setCorretajePti(datosDesglose.getCorretajePti());

  }

  /**
   * Extrae la provincia dada la cadena que contiene el municipio y la provincia
   * entre paréntesis
   * 
   * @param poblacion
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public String extractProvincia(String poblacion) {
    int firstIndex = poblacion.indexOf("(");
    int length = poblacion.indexOf(")");
    // si no hay cierre de paréntesis entonces se toma la longitud
    // de la cadena, ya que se presupone que tampoco hay apertura de
    // paréntesis
    // lo cual el valor de firstIndex sería -1 que al sumarle 1 en el
    // substring,
    // es 0
    if (length == -1) {
      length = poblacion.length();
    }
    String provincia = poblacion.substring(firstIndex + 1, length);
    // si el tamaño de la cadena resultante es mayor que 25, se cora
    if (provincia.length() > 25) {
      provincia = provincia.substring(0, 25);
    }
    return provincia;
  }

  /**
   * Obtiene el municipio dada una cadena que contiene el municipio y la
   * provincia entre paréntesis
   * 
   * @param cadena
   * @return
   */
  public String extractMunicipio(String cadena) {
    String municipio = cadena;
    int length = cadena.indexOf("(");
    if (length != -1) {
      length = length > 40 ? 40 : length;
      municipio = cadena.substring(0, length);
    }
    return municipio;
  }

  /**
   * Obtiene la lista de objetos en estado Pendientes IF y Provisional IF
   * 
   * @return
   */
  public List<Tmct0aloId> findAlosPdteIf(Date feoperacLt) {
    List<Tmct0aloId> leif_oPdte = dao.findAllAlosCrearDesglosesIntermediarioFinanciero(
        EstadosEnumerados.TITULARIDADES.PDTE_IF.getId(), feoperacLt);
    List<Tmct0aloId> leif_oProv = dao.findAllAlosCrearDesglosesIntermediarioFinanciero(
        EstadosEnumerados.TITULARIDADES.PROVISIONAL_IF.getId(), feoperacLt);
    /*
     * Si la lista de elementos en estado pendiente no es nula y la lista de
     * elementos en estado provisional, no está vacia, se juntan
     */
    if (leif_oPdte != null && CollectionUtils.isNotEmpty(leif_oProv)) {
      leif_oPdte.addAll(leif_oProv);
    }
    else if (leif_oPdte == null && CollectionUtils.isNotEmpty(leif_oProv)) {
      /*
       * Si la lista de elementos con estado pendiente es nula, y la lista de
       * elementos con estado provisional no está vacía, se devuelve
       */
      return leif_oProv;
    }
    return leif_oPdte;
  }

  /**
   * Obtiene la lista de objetos en estado Pendientes IF y Provisional IF
   * 
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public List<CuadreEccDbReaderRecordBeanDTO> findAllCuadreEccDbReaderToImportDesglosesEcc(int maxRegs) {
    return findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte(
        ESTADO_PROCESADO_CUADRE_ECC.PDTE_IMPORTAR_DESGLOSES_ECC.getId(), new Date(), maxRegs);
  }

  /**
   * Obtiene la lista de objetos en estado Pendientes IF y Provisional IF
   * 
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public List<CuadreEccDbReaderRecordBeanDTO> findAllCuadreEccDbReaderToSepararMovimientos(int maxRegs) {
    return findAllDistinctCuadreEccDbReaderRecordBeanDTOByEstadoProcesadoAndFevalorLte(
        ESTADO_PROCESADO_CUADRE_ECC.TITULARES_DESGLOSES_OK_PDTE_REVISAR_INFORMACION_ASIGNACION_MOVIMIENTOS.getId(),
        new Date(), maxRegs);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tmct0afi cargaAFI(Tmct0afiId afiid, CuadreEccReferencia eccmsg, CuadreEccTitular titular) {

    Tmct0afi afi = new Tmct0afi();

    afi.setCdreferenciatitular(eccmsg.getRefTitular().trim());

    afi.setNbdomici(eccmsg.getDomicilio().trim());
    afi.setCdpostal(eccmsg.getCodPostal().trim());
    /* Este dato está en ISO y hay que pasarlo al código del Banco de España */
    String codPaisIso = convertirCodigoResidenciaAEstandardSpanishBank(eccmsg);
    afi.setCddepais(codPaisIso);
    afi.setNbciudad(eccmsg.getPoblacion().trim());

    afi.setCdnactit(titular.getPaisNacionalidad().trim());

    // afi.setCdddebic( cdddebic );
    if (titular.getIndNacionalidad() != null) {
      afi.setTpnactit(titular.getIndNacionalidad());
    }
    else {
      afi.setTpnactit(' ');
    }

    if ('J' == titular.getIndPersonaFisicaJuridica()) {
      if (titular.getNombreRazonSocial().trim().length() > 50) {
        afi.setNbclient1(titular.getNombreRazonSocial().trim().substring(0, 50));
      }
      else {
        afi.setNbclient1(titular.getNombreRazonSocial().trim());
      }
      afi.setNbclient(" ");
      afi.setNbclient2(" ");
    }
    else {
      if (titular.getNombreRazonSocial().trim().length() > 40) {
        afi.setNbclient(titular.getNombreRazonSocial().trim().substring(0, 40)); // grabo
                                                                                 // el
                                                                                 // apellido1
      }
      else {
        afi.setNbclient(titular.getNombreRazonSocial().trim()); // grabo
        // el
        // apellido1
      }
      afi.setNbclient1(titular.getPrimerApellido().trim()); // grabo el
      // nombre
      // razon
      // social
      // hasta 50
      // posiciones
      if (titular.getSegundoApellido().trim().length() > 30) {
        afi.setNbclient2(titular.getSegundoApellido().trim().substring(0, 30)); // grabo
                                                                                // el
                                                                                // apellido2
      }
      else {
        afi.setNbclient2(titular.getSegundoApellido().trim());
      }

    }

    if (titular.getIndPersonaFisicaJuridica() != null) {
      afi.setTpfisjur(titular.getIndPersonaFisicaJuridica());
    }
    else {
      afi.setTpfisjur(' ');
    }

    if (titular.getTipoTitular() != null) {
      afi.setTpsocied(titular.getTipoTitular());
    }
    else {
      afi.setTpsocied(' ');
    }

    if (titular.getTipoIdentificacion() != null) {
      afi.setTpidenti(titular.getTipoIdentificacion());
    }
    else {
      afi.setTpidenti(' ');
    }

    if ('B' == afi.getTpidenti()) {
      if (titular.getIdentificacion().trim().length() > 11) {
        afi.setCdddebic(titular.getIdentificacion().trim().substring(0, 11));
      }
      else {
        afi.setCdddebic(titular.getIdentificacion().trim());
      }
    }
    else {
      if (titular.getIdentificacion().trim().length() > 11) {
        afi.setNudnicif(titular.getIdentificacion().trim().substring(0, 11));
      }
      else {
        afi.setNudnicif(titular.getIdentificacion().trim());
      }
      afi.setCdddebic(" ");
    }

    // hay que cambiar el campo en la BBDD y luego descomentar la siguiente
    // linea
    afi.setParticip(titular.getPorcentajePropiedad());

    // titular.getPorcentaje_usufructo(); NO hay campo para el porcentaje de
    // usufructo
    // afi.setTmct0alo( alo );

    afi.setId(afiid);

    afi.setCdbloque((short) 1);
    afi.setCdenvaud(' ');
    afi.setCdestado(' ');
    afi.setCdinacti('A');// A/I
    afi.setCdusuina("");
    afi.setIdproced(INTERMEDIARIO_FINANCIERO);
    if (eccmsg.getCodPostal().trim().length() > 2) {
      afi.setNbprovin(eccmsg.getCodPostal().trim().substring(0, 2));
    }
    else {
      afi.setNbprovin(eccmsg.getCodPostal().trim());
    }

    afi.setNudomici("");

    afi.setNuoprout(0);
    afi.setNupareje((short) 0);
    afi.setNusecnbr((short) 0);

    if (eccmsg.getDomicilio().trim().length() > 2) {
      afi.setTpdomici(eccmsg.getDomicilio().trim().substring(0, 2));
    }
    else {
      afi.setTpdomici(eccmsg.getDomicilio().trim());
    }

    afi.setTptiprep('A');

    afi.setId(afiid);

    afi.setCdusuaud(INTERMEDIARIO_FINANCIERO);
    afi.setFhaudit(new Date());

    return afi;
  }

  @Transactional
  private String convertirCodigoResidenciaAEstandardSpanishBank(CuadreEccReferencia eccmsg) {
    String codPaisIso = eccmsg.getPaisResidencia();
    if (codPaisIso != null && !"".equals(codPaisIso)) {
      codPaisIso = codPaisIso.trim();
      String resultCodBE = tval0ps2Bo.castCodigoPaisFromISOToSpanishBank(codPaisIso);
      codPaisIso = (!"".equals(resultCodBE)) ? resultCodBE : codPaisIso;
    }
    else {
      codPaisIso = (eccmsg.getPaisResidencia() != null) ? eccmsg.getPaisResidencia().trim() : "";
    }
    return codPaisIso;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
  public void deleteAllCompletedCierreIf() {
    LOG.info("[deleteAllCompletedCierreIf] ## Inicio.");
    Integer count = dao.deleteByEstadoAndTmct0aloCdestadotitGt(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId(),
        EstadosEnumerados.TITULARIDADES.CIERRE_IF.getId());
    LOG.info("[deleteAllCompletedCierreIf] ## Fin. ElementoIFinanciero borrados == {}", count);
  }

  public Tmct0alc inicializaALC(Tmct0alc alc) {
    if (alc != null) {
      alc.setTmct0desglosecamaras(new ArrayList<Tmct0desglosecamara>());
      alc.setImajusvb(BigDecimal.ZERO);
      alc.setImcanoncompdv(BigDecimal.ZERO);
      alc.setImcanoncompeu(BigDecimal.ZERO);
      alc.setImcanonliqdv(BigDecimal.ZERO);
      alc.setImcanonliqeu(BigDecimal.ZERO);
      alc.setImcombco(BigDecimal.ZERO);
      alc.setImcombrk(BigDecimal.ZERO);
      alc.setImcomdvo(BigDecimal.ZERO);
      alc.setImcomisn(BigDecimal.ZERO);
      alc.setImcomsvb(BigDecimal.ZERO);
      alc.setImderges(BigDecimal.ZERO);
      alc.setImderliq(BigDecimal.ZERO);
      alc.setImefeagr(BigDecimal.ZERO);
      alc.setImfinsvb(BigDecimal.ZERO);
      alc.setImnetliq(BigDecimal.ZERO);
      alc.setImnfiliq(BigDecimal.ZERO);
      alc.setImntbrok(BigDecimal.ZERO);
      alc.setImtotbru(BigDecimal.ZERO);
      alc.setImtotnet(BigDecimal.ZERO);
    }
    return alc;
  }

  public Integer deleteByFeoperacAndNumerooperacionAndSentido(Date feoperac, String cdoperacionecc, Character sentido) {
    return dao.deleteByFeoperacAndNumerooperacionAndSentido(feoperac, cdoperacionecc, sentido);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Integer deleteByFeoperacAndNumerooperacionAndSentidoNewTransaction(Date feoperac, String cdoperacionecc,
      Character sentido) {
    return dao.deleteByFeoperacAndNumerooperacionAndSentido(feoperac, cdoperacionecc, sentido);
  }

  public List<Long> findAllIdsByFeoperacAndNumerooperacionAndSentido(Date feoperac, String numerooperacion,
      Character sentido) {
    return dao.findAllIdsByFeoperacAndNumerooperacionAndSentido(feoperac, numerooperacion, sentido);
  }

  public List<Long> findAllIdsByFeoperacAndNumerooperacionAndSentidoAndIdCuadreEccEjecucion(Date feoperac,
      String numerooperacion, Character sentido, Long idCuadreEccEjecucion) {
    return dao.findAllIdsByFeoperacAndNumerooperacionAndSentidoAndIdCuadreEccEjecucion(feoperac, numerooperacion,
        sentido, idCuadreEccEjecucion);
  }

  public List<Long> findAllIdsByIdCuadreEccEjecucion(long idCuadreEccEjecucion) {
    return dao.findAllIdsByIdCuadreEccEjecucion(idCuadreEccEjecucion);
  }

  private BigDecimal getTitulosCuadreEccSibbac(List<CuadreEccSibbac> cuadresEccSibbac) {
    BigDecimal res = BigDecimal.ZERO;
    for (CuadreEccSibbac cuadreEccSibbac : cuadresEccSibbac) {
      res = res.add(cuadreEccSibbac.getNumTitulosDesSibbacPdte());
    }
    return res;
  }

  /**
   * @param cuadreEccEjecucion
   * @param idOP
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public List<CuadreEccSibbac> crearCuadreEccSibbacByInformacionMercado(final CuadreEccEjecucion cuadreEccEjecucion,
      final IdentificacionOperacionCuadreEccDTO idOP, int pageSize, int maxRegsInSimpleThread)
      throws SIBBACBusinessException {
    LOG.debug(
        "[CuadreEccSibbacBo :: grabarCuadreEccSibbac] buscando dcts por isin: {} fecha: {} sentido: {} camara: {} segmento: {} "
            + " op.Mer: {} miembro: {} n.orden: {} n.ejecu: {} ", idOP.getCdisin(), idOP.getFechaEjecucion(),
        idOP.getSentido(), idOP.getCdcamara(), idOP.getCdsegmento(), idOP.getCdoperacionmkt(), idOP.getCdmiembromkt(),
        idOP.getNuordenmkt(), idOP.getNuexemkt());
    Pageable page = new PageRequest(0, pageSize);
    int queryResultSize;
    BigDecimal titulosDesgloses;
    List<Tmct0desgloseclitit> desglosesClitit;
    List<Tmct0ejecucionalocation> ejealos;
    final List<CuadreEccSibbac> desglosesSibbac = Collections.synchronizedList(new ArrayList<CuadreEccSibbac>());
    List<Tmct0ejecucionalocation> ejealosToRemove = new ArrayList<Tmct0ejecucionalocation>();
    List<Long> nudesgloses = tmct0desgloseclititBo.findAllNudesgloseByDatosMercado(idOP.getCdisin(),
        idOP.getFechaEjecucion(), idOP.getSentido(), idOP.getCdcamara(), idOP.getCdsegmento(),
        idOP.getCdoperacionmkt(), idOP.getCdmiembromkt(), idOP.getNuordenmkt(), idOP.getNuexemkt());

    LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] Encontrados {} nudesgloses", nudesgloses.size());
    if (CollectionUtils.isNotEmpty(nudesgloses)) {
      // ejecucion multithread
      if (nudesgloses.size() > maxRegsInSimpleThread) {
        ExecutorService executor = Executors.newFixedThreadPool(25);
        int posIni = 0;
        int posEnd = 0;
        try {
          while (posIni < nudesgloses.size()) {
            posEnd += maxRegsInSimpleThread;
            if (posEnd > nudesgloses.size()) {
              posEnd = nudesgloses.size();
            }
            final List<Long> nudesglosessubList = new ArrayList<Long>(nudesgloses.subList(posIni, posEnd));

            Runnable process = new Runnable() {
              public void run() {
                LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] buscando dct's : {}", nudesglosessubList);
                List<Tmct0desgloseclitit> desglosesSublist = tmct0desgloseclititBo
                    .findAllByNudesgloses(nudesglosessubList);
                LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] encontrados dct's : {}", desglosesSublist);
                List<CuadreEccSibbac> cuadreEccSibbacThread = createAndSaveNewTransaction(idOP, cuadreEccEjecucion,
                    nudesglosessubList);
                LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] antes de aniadir a la lista principal: {} ",
                    desglosesSibbac.size());
                for (CuadreEccSibbac ceccs : cuadreEccSibbacThread) {
                  desglosesSibbac.add(ceccs);
                }
                LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] despues de aniadir a la lista principal: {} ",
                    desglosesSibbac.size());
              }
            };
            executor.execute(process);
            posIni = posEnd;
          }
        }
        catch (Exception e) {
          if (executor != null) {
            executor.shutdownNow();
          }
          throw new SIBBACBusinessException("INCIDENCIA- Creando los desgloses sibbac.", e);
        }
        finally {
          if (executor != null) {
            if (!executor.isShutdown()) {
              executor.shutdown();
            }

            try {
              LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] esperando por hilos: {}", executor);
              while (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] esperando por hilos: {}", executor);
              }
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
              throw new SIBBACBusinessException(e.getMessage(), e);
            }

          }
        }
      }
      else {
        // do {
        LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] buscando dct's pagina: {}", page);

        desglosesClitit = tmct0desgloseclititBo.findAllByNudesgloses(nudesgloses);

        if (CollectionUtils.isNotEmpty(desglosesClitit)) {
          page = page.next();
          List<CuadreEccSibbac> desglosesSibbacCreados = crearCuadreEccSibbacByDesglosesClitit(idOP,
              cuadreEccEjecucion, desglosesClitit);
          desglosesSibbacCreados = save(desglosesSibbacCreados);
          desglosesSibbac.addAll(desglosesSibbacCreados);

        }

      }
    }

    titulosDesgloses = getTitulosCuadreEccSibbac(desglosesSibbac);

    if (cuadreEccEjecucion.getValoresImpNominalEje() != null
        && titulosDesgloses.compareTo(cuadreEccEjecucion.getValoresImpNominalEje()) < 0) {
      LOG.warn("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] mas titulos en desgloses {} que en ejecuciones {}",
          titulosDesgloses, cuadreEccEjecucion.getValoresImpNominalEje());
      LOG.debug(
          "[CuadreEccSibbacBo :: grabarCuadreEccSibbac] buscando eje alos por isin: {} fecha: {} sentido: {} camara: {} segmento: {} "
              + " op.Mer: {} miembro: {} n.orden: {} n.ejecu: {} ", idOP.getCdisin(), idOP.getFechaEjecucion(),
          idOP.getSentido(), idOP.getCdcamara(), idOP.getCdsegmento(), idOP.getCdoperacionmkt(),
          idOP.getCdmiembromkt(), idOP.getNuordenmkt(), idOP.getNuexemkt());
      page = new PageRequest(0, pageSize);
      do {
        LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] buscando ejealos's pagina: {}", page);
        ejealos = ejealoBo.findAllByDatosMercado(idOP.getCdisin(), idOP.getFechaEjecucion(), idOP.getSentido(),
            idOP.getCdcamara(), idOP.getCdsegmento(), idOP.getCdoperacionmkt(), idOP.getCdmiembromkt(),
            idOP.getNuordenmkt(), idOP.getNuexemkt(), page);
        queryResultSize = ejealos.size();
        if (CollectionUtils.isNotEmpty(ejealos)) {

          page = page.next();

          if (titulosDesgloses.compareTo(BigDecimal.ZERO) != 0) {
            LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] titulos desgloses != ejecuciones.");
            for (Tmct0ejecucionalocation ejealo : ejealos) {

              ejealo.setNutitulosPendientesDesglose(ejealo.getNutitulos());
              for (CuadreEccSibbac dct : desglosesSibbac) {
                if (ejealo.getTmct0grupoejecucion().getTmct0alo().getId().getNuorden().equals(dct.getNuorden())
                    && ejealo.getTmct0grupoejecucion().getTmct0alo().getId().getNbooking().equals(dct.getNbooking())
                    && ejealo.getTmct0eje().getId().getNuejecuc().equals(dct.getNuejecuc())) {

                  ejealo.setNutitulosPendientesDesglose(ejealo.getNutitulosPendientesDesglose().subtract(
                      dct.getNumTitulosDesSibbac()));
                }
              }// fin for dct
              if (ejealo.getNutitulosPendientesDesglose().compareTo(BigDecimal.ZERO) == 0) {
                ejealosToRemove.add(ejealo);
              }
            }

            ejealos = ejealoBo.save(ejealos);
            ejealos.removeAll(ejealosToRemove);
            ejealosToRemove.clear();

            if (CollectionUtils.isNotEmpty(ejealos)) {
              LOG.debug(
                  "[CuadreEccSibbacBo :: grabarCuadreEccSibbac] econtrados {} grupos los que aplicar los desgloses.",
                  ejealos.size());
              List<CuadreEccSibbac> desglosesSibbacCreados = crearCuadreEccSibbacByEjecucionAlocation(idOP,
                  cuadreEccEjecucion, ejealos);
              desglosesSibbacCreados = save(desglosesSibbacCreados);
              desglosesSibbac.addAll(desglosesSibbacCreados);
              titulosDesgloses = getTitulosCuadreEccSibbac(desglosesSibbac);
            }
          }
        }
      }
      while (queryResultSize == page.getPageSize());

    }
    else {
      LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] titulos en desgloses {} que en ejecuciones {}",
          titulosDesgloses, cuadreEccEjecucion.getValoresImpNominalEje());
    }
    if (cuadreEccEjecucion.getValoresImpNominalEje() != null
        && titulosDesgloses.compareTo(cuadreEccEjecucion.getValoresImpNominalEje()) != 0) {
      LOG.warn("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] titulos desloses: {} != titulos eje: {}",
          titulosDesgloses, cuadreEccEjecucion.getValoresImpNominalEje());

    }
    else {
      LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbac] titulos desloses: {} == titulos eje: {}",
          titulosDesgloses, cuadreEccEjecucion.getValoresImpNominalEje());
    }

    return desglosesSibbac;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private List<CuadreEccSibbac> createAndSaveNewTransaction(IdentificacionOperacionCuadreEccDTO idOp,
      CuadreEccEjecucion cuadreEccEjecucion, List<Long> nudesgloses) {
    List<Tmct0desgloseclitit> desgloses = tmct0desgloseclititBo.findAllByNudesgloses(nudesgloses);
    List<CuadreEccSibbac> cuadreEccSibbacThread = crearCuadreEccSibbacByDesglosesClitit(idOp, cuadreEccEjecucion,
        desgloses);
    cuadreEccSibbacThread = save(cuadreEccSibbacThread);
    return cuadreEccSibbacThread;
  }

  /**
   * @param idOp
   * @param cuadreEccEjecucion
   * @param desgloses
   * @return
   */
  public List<CuadreEccSibbac> crearCuadreEccSibbacByDesglosesClitit(IdentificacionOperacionCuadreEccDTO idOp,
      CuadreEccEjecucion cuadreEccEjecucion, List<Tmct0desgloseclitit> desgloses) {
    LOG.debug("[CuadreEccSibbacBo :: crearCuadreEccSibbacByDesglosesClitit] inicio.");

    List<CuadreEccSibbac> res = new ArrayList<CuadreEccSibbac>();
    CuadreEccSibbac cuadreEccSibbac;
    Tmct0desglosecamara dc;
    Tmct0alc alc;
    Tmct0alo alo;
    Tmct0referenciatitular referenciaTitular;

    for (Tmct0desgloseclitit dct : desgloses) {
      LOG.debug("[CuadreEccSibbacBo :: crearCuadreEccSibbacByDesglosesClitit] creando cuadre ecc sibbac desde dct: {}",
          dct);
      cuadreEccSibbac = new CuadreEccSibbac();

      CloneObjectHelper.copyBasicFields(dct, cuadreEccSibbac, null);

      cuadreEccSibbac
          .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC
              .getId());

      if (cuadreEccEjecucion != null) {
        cuadreEccSibbac.setIdCuadreEccEjecucion(cuadreEccEjecucion.getId());
      }
      dc = dcBo.findById(dct.getTmct0desglosecamara().getIddesglosecamara());
      alc = alcBo.findById(dc.getTmct0alc().getId());
      alo = aloBo.findById(alc.getTmct0alo().getId());
      cuadreEccSibbac.setNuorden(alc.getId().getNuorden());
      cuadreEccSibbac.setNbooking(alc.getId().getNbooking());
      cuadreEccSibbac.setNucnfclt(alc.getId().getNucnfclt());
      cuadreEccSibbac.setNucnfliq(alc.getId().getNucnfliq());
      cuadreEccSibbac.setIddesglosecamara(dc.getIddesglosecamara());

      // numero de titulos por parte del ti
      cuadreEccSibbac.setNumTitulosDesSibbac(dct.getImtitulos());
      cuadreEccSibbac.setNumTitulosDesSibbacPdte(dct.getImtitulos());

      cuadreEccSibbac.setFeoperac(dct.getFeejecuc());
      cuadreEccSibbac.setFevalor(alc.getFeopeliq());
      if (idOp.isSinEcc()) {
        boolean changed = false;
        if (StringUtils.isNotBlank(dct.getCdoperacionecc())) {
          changed = true;
          dct.setCdoperacionecc("");
          dct.setNuoperacioninic("");
          dct.setNuoperacionprev("");
          dct.setAuditFechaCambio(new Date());
          dct.setAuditUser("CECC_VTIT");
        }
        if (StringUtils.isBlank(dct.getNumeroOperacionDCV())) {
          changed = true;
          dct.setNumeroOperacionDCV(cuadreEccEjecucion.getNumOpDcv());
          dct.setAuditFechaCambio(new Date());
          dct.setAuditUser("CECC_VTIT");
        }
        if (changed) {
          dctBo.save(dct);
        }
      }

      if (StringUtils.isNotBlank(dct.getCdoperacionecc())) {
        cuadreEccSibbac.setNumOp(dct.getCdoperacionecc());
      }
      else if (StringUtils.isNotBlank(dct.getNumeroOperacionDCV())) {
        cuadreEccSibbac.setNumOp(dct.getNumeroOperacionDCV());
      }
      else if (idOp.getNumeroOperacion().startsWith("IBO")) {
        cuadreEccSibbac.setNumOp(idOp.getNumeroOperacion());
      }
      else {
        LOG.warn(
            "[CuadreEccSibbacBo :: crearCuadreEccSibbacByDesglosesClitit] desglose no tiene codigo de operacion valido. {}",
            dct);
      }
      cuadreEccSibbac.setNumOpPrevia(dct.getNuoperacionprev());
      cuadreEccSibbac.setNumOpInicial(dct.getNuoperacioninic());
      cuadreEccSibbac.setSentido(idOp.getSentido());
      if (idOp.getTipoCuentaIberclear() != null
          && idOp.getTipoCuentaIberclear().equals(TipoCuentaIberclear.CUENTA_INDIVIDUAL_IF.getTipo())) {
        if (alo.getCdestadotit() < EstadosEnumerados.TITULARIDADES.CIERRE_IF.getId()) {
          cuadreEccSibbac.setEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
        }
        else {
          cuadreEccSibbac.setEstado(alo.getCdestadotit());
        }
      }
      else {
        cuadreEccSibbac.setEstado(alo.getCdestadotit());
      }
      if (alc.getReferenciaTitular() != null) {
        referenciaTitular = referenciaTitularBo.findById(alc.getReferenciaTitular().getIdreferencia());
        if (referenciaTitular != null) {
          cuadreEccSibbac.setRefTitular(referenciaTitular.getCdreftitularpti());
          cuadreEccSibbac.setRefAdicional(referenciaTitular.getReferenciaAdicional());
        }
      }
      if (cuadreEccSibbac.getRefTitular() == null) {
        cuadreEccSibbac.setRefTitular("");
        cuadreEccSibbac.setRefAdicional("");
      }

      cuadreEccSibbac.setTipoCuentaLiquidacionIberclear(idOp.getTipoCuentaIberclear());
      cuadreEccSibbac.setCuentaLiquidacion(idOp.getCuentaLiquidacionIberclear());

      cuadreEccSibbac.setNuejecuc(dct.getNuejecuc());
      cuadreEccSibbac.setNudesglose(dct.getNudesglose());
      cuadreEccSibbac.setIdgrupoeje(dct.getIdgrupoeje());

      cuadreEccSibbac.setCdcamara(idOp.getCdcamara());
      cuadreEccSibbac.setCdisin(idOp.getCdisin());
      cuadreEccSibbac.setCdsegmento(idOp.getCdsegmento());
      cuadreEccSibbac.setCdoperacionmkt(idOp.getCdoperacionmkt());
      cuadreEccSibbac.setCdmiembromkt(idOp.getCdmiembromkt());
      cuadreEccSibbac.setNuordenmkt(idOp.getNuordenmkt());
      cuadreEccSibbac.setNuexemkt(idOp.getNuexemkt());

      cuadreEccSibbac.setAuditDate(new Date());
      cuadreEccSibbac.setAuditUser("CECC_VTIT");
      res.add(cuadreEccSibbac);
      LOG.debug(
          "[CuadreEccSibbacBo :: crearCuadreEccSibbacByDesglosesClitit] fin creando cuadre ecc sibbac desde dct: {}",
          dct);

    }
    LOG.debug("[CuadreEccSibbacBo :: crearCuadreEccSibbacByDesglosesClitit] fin.");
    return res;
  }

  /**
   * @param idOp
   * @param cuadreEccEjecucion
   * @param desgloses
   * @return
   */
  public List<CuadreEccSibbac> grabarCuadreEccSibbacByDesglosesClitit(IdentificacionOperacionCuadreEccDTO idOp,
      CuadreEccEjecucion cuadreEccEjecucion, List<Tmct0desgloseclitit> desgloses) {
    LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbacByDesglosesClitit] inicio.");
    List<CuadreEccSibbac> res = crearCuadreEccSibbacByDesglosesClitit(idOp, cuadreEccEjecucion, desgloses);
    res = save(res);
    LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbacByDesglosesClitit] fin.");
    return res;
  }

  /**
   * @param idOp
   * @param cuadreEccEjecucion
   * @param desgloses
   * @return
   */
  public List<CuadreEccSibbac> crearCuadreEccSibbacByEjecucionAlocation(IdentificacionOperacionCuadreEccDTO idOp,
      CuadreEccEjecucion cuadreEccEjecucion, List<Tmct0ejecucionalocation> desgloses) {
    LOG.debug("[CuadreEccSibbacBo :: crearCuadreEccSibbacByEjecucionAlocation] inicio.");
    List<CuadreEccSibbac> res = new ArrayList<CuadreEccSibbac>();
    CuadreEccSibbac cuadreEccSibbac;
    Tmct0alo alo;

    for (Tmct0ejecucionalocation dct : desgloses) {
      if (dct.getNutitulosPendientesDesglose().compareTo(BigDecimal.ZERO) > 0) {
        LOG.debug(
            "[CuadreEccSibbacBo :: crearCuadreEccSibbacByDesglosesClitit] insertando cuadre ecc sibbac desde eje alo: {}",
            dct);
        cuadreEccSibbac = new CuadreEccSibbac();
        cuadreEccSibbac
            .setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_REFERENCIAS_FICHERO_ECC
                .getId());
        if (cuadreEccEjecucion != null) {
          cuadreEccSibbac.setIdCuadreEccEjecucion(cuadreEccEjecucion.getId());
          cuadreEccSibbac.setNumOpInicial(cuadreEccEjecucion.getNumOpInicial());
        }
        alo = dct.getTmct0grupoejecucion().getTmct0alo();

        cuadreEccSibbac.setNuorden(alo.getId().getNuorden());
        cuadreEccSibbac.setNbooking(alo.getId().getNbooking());
        cuadreEccSibbac.setNucnfclt(alo.getId().getNucnfclt());

        // numero de titulos por parte del alo
        // cuadreEccSibbac.setNumTitulosAlo(alo.getNutitcli());
        // cuadreEccSibbac.setNumTitulosAloPdte(alo.getNutitcli());
        // numero de titulos por parte del ti
        cuadreEccSibbac.setNumTitulosDesSibbac(dct.getNutitulosPendientesDesglose());
        cuadreEccSibbac.setNumTitulosDesSibbacPdte(dct.getNutitulosPendientesDesglose());

        cuadreEccSibbac.setFeoperac(alo.getFeoperac());
        cuadreEccSibbac.setFevalor(alo.getFevalor());

        // cuadreEccSibbac.setNumOp(idOp.getNumeroOperacion());

        cuadreEccSibbac.setSentido(idOp.getSentido());
        if (idOp.getTipoCuentaIberclear() != null
            && idOp.getTipoCuentaIberclear().equals(TipoCuentaIberclear.CUENTA_INDIVIDUAL_IF.getTipo())) {
          cuadreEccSibbac.setEstado(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId());
        }
        else {
          cuadreEccSibbac.setEstado(alo.getCdestadotit());
        }

        cuadreEccSibbac.setRefTitular("");
        cuadreEccSibbac.setRefAdicional("");

        cuadreEccSibbac.setTipoCuentaLiquidacionIberclear(idOp.getTipoCuentaIberclear());
        cuadreEccSibbac.setCuentaLiquidacion(idOp.getCuentaLiquidacionIberclear());

        cuadreEccSibbac.setCdcamara(idOp.getCdcamara());
        cuadreEccSibbac.setCdisin(idOp.getCdisin());
        cuadreEccSibbac.setCdsegmento(idOp.getCdsegmento());
        cuadreEccSibbac.setCdoperacionmkt(idOp.getCdoperacionmkt());
        cuadreEccSibbac.setCdmiembromkt(idOp.getCdmiembromkt());
        cuadreEccSibbac.setNuordenmkt(idOp.getNuordenmkt());
        cuadreEccSibbac.setNuexemkt(idOp.getNuexemkt());

        cuadreEccSibbac.setNuejecuc(dct.getTmct0eje().getId().getNuejecuc());
        cuadreEccSibbac.setIdejecucionalocation(dct.getIdejecucionalocation());
        cuadreEccSibbac.setAuditDate(new Date());
        cuadreEccSibbac.setAuditUser("IF");
        res.add(cuadreEccSibbac);
      }
    }
    LOG.debug("[CuadreEccSibbacBo :: crearCuadreEccSibbacByEjecucionAlocation] fin.");
    return res;
  }

  /**
   * @param idOp
   * @param cuadreEccEjecucion
   * @param desgloses
   * @return
   */
  public List<CuadreEccSibbac> grabarCuadreEccSibbacByEjecucionAlocation(IdentificacionOperacionCuadreEccDTO idOp,
      CuadreEccEjecucion cuadreEccEjecucion, List<Tmct0ejecucionalocation> desgloses) {
    LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbacByEjecucionAlocation] inicio.");
    List<CuadreEccSibbac> res = crearCuadreEccSibbacByEjecucionAlocation(idOp, cuadreEccEjecucion, desgloses);
    res = save(res);
    LOG.debug("[CuadreEccSibbacBo :: grabarCuadreEccSibbacByEjecucionAlocation] fin.");
    return res;
  }

  /**
   * Agrupa los movimientos por ic distintas y los devuelve en un map de listas
   * 
   * @param dc
   * @return
   */
  private Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> agruparMovimientosByInformacionCompensacion(
      Tmct0desglosecamara dc) {
    LOG.debug("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] inicio.");
    LOG.trace("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] procesando dc: {}", dc);
    LOG.trace("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] procesando dc: {}",
        dc.getIddesglosecamara());
    List<Tmct0movimientoecc> movsMap;
    Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosMismaIc = new HashMap<InformacionCompensacionDTO, List<Tmct0movimientoecc>>();
    // TODO NECESITO QUERY DE WRAPPERS PARA RECUPERAR LOS DISTINTOS
    // INFORMACIONES DE ASIGNACION
    List<Tmct0movimientoecc> movs = movBo.findByIdDesgloseCamara(dc.getIddesglosecamara());
    InformacionCompensacionDTO icMovimientoDc;
    for (Tmct0movimientoecc movFor : movs) {
      LOG.trace("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] procesando mov: {}", movFor);
      LOG.trace("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] cdestadomov == {}",
          movFor.getCdestadomov());
      if (movFor.getCdestadomov() != null
          && movFor.getCdestadomov().trim().equals(EstadosEccMovimiento.FINALIZADO.text)) {

        if (StringUtils.isBlank(movFor.getMiembroCompensadorDestino())
            && StringUtils.isBlank(movFor.getCuentaCompensacionDestino())) {
          LOG.warn("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] no tiene incofmacion de compensacion.");
          icMovimientoDc = new InformacionCompensacionDTO("", "", "", "");
        }
        else if (StringUtils.isBlank(movFor.getMiembroCompensadorDestino())) {
          icMovimientoDc = new InformacionCompensacionDTO(CodigoMiembroCompensadorSV.SV.text,
              movFor.getCuentaCompensacionDestino(), CodigoMiembroCompensadorSV.SV.text,
              movFor.getCodigoReferenciaAsignacion());
        }
        else {
          icMovimientoDc = new InformacionCompensacionDTO(movFor.getMiembroCompensadorDestino(),
              movFor.getCuentaCompensacionDestino(), movFor.getMiembroCompensadorDestino(),
              movFor.getCodigoReferenciaAsignacion());
        }
        LOG.trace(
            "[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] informacion compensacion mov: {}",
            icMovimientoDc);
        movsMap = movimientosMismaIc.get(icMovimientoDc);
        if (movsMap == null) {
          movsMap = new ArrayList<Tmct0movimientoecc>();
          movimientosMismaIc.put(icMovimientoDc, movsMap);
        }
        movsMap.add(movFor);

        LOG.trace("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] movs by ic {} - {}",
            movsMap.size(), icMovimientoDc);
      }// mov estado finalizado
    }
    LOG.trace(
        "[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] dc tiene {} informacion de compensacion distintas.",
        movimientosMismaIc.size());
    LOG.debug("[CuadreEccSibbacBo :: agruparMovimientosByInformacionCompensacion] fin.");
    return movimientosMismaIc;
  }

  /**
   * @param dc
   */
  public void escalarInformacionCompensacionFromMovimientos(Tmct0desglosecamara dc, String auditUser) {
    LOG.debug("[CuadreEccSibbacBo :: escalarInformacionCompensacionFromMovimientos] inicio.");
    Tmct0infocompensacion ic;
    Tmct0movimientoecc mov;
    Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosMismaIc = this
        .agruparMovimientosByInformacionCompensacion(dc);

    if (movimientosMismaIc.size() == 0) {
      LOG.debug("[CuadreEccSibbacBo :: escalarInformacionCompensacionFromMovimientos] NO tiene movimientos.");
    }
    else if (movimientosMismaIc.size() == 1) {
      LOG.debug("[CuadreEccSibbacBo :: escalarInformacionCompensacionFromMovimientos] todos los movs del dc tienen la misma ic.");
      mov = movimientosMismaIc.values().iterator().next().get(0);
      ic = icDao.findByIddesgloseCamara(dc.getIddesglosecamara());
      if (ic == null) {
        ic = new Tmct0infocompensacion();
      }
      ic.setCdctacompensacion(mov.getCuentaCompensacionDestino());
      ic.setCdmiembrocompensador(mov.getMiembroCompensadorDestino());
      ic.setCdrefasignacion(mov.getCodigoReferenciaAsignacion());
      ic.setAuditFechaCambio(new Date());
      ic.setAuditUser(auditUser);
      ic = icDao.save(ic);
      dc.setTmct0infocompensacion(ic);
      if (mov.getMiembroCompensadorDestino() != null
          && mov.getMiembroCompensadorDestino().trim().equals(CodigoMiembroCompensadorSV.SV.text)
          || StringUtils.isBlank(mov.getMiembroCompensadorDestino())
          && StringUtils.isNotBlank(mov.getCuentaCompensacionDestino())) {
        dc.getTmct0alc().setCdentdep(CodigoMiembroCompensadorSV.SV.liquidador);
        dc.getTmct0alc().setCdentliq(CodigoMiembroCompensadorSV.SV.liquidador);
        dc.getTmct0alc().setFhaudit(new Date());
        dc.getTmct0alc().setCdusuaud(auditUser);
        alcBo.save(dc.getTmct0alc());
      }
    }
    else {
      LOG.debug("[CuadreEccSibbacBo :: escalarInformacionCompensacionFromMovimientos] NO todos los movs del dc tienen la misma ic.");
    }
    movimientosMismaIc.clear();
    LOG.debug("[CuadreEccSibbacBo :: escalarInformacionCompensacionFromMovimientos] fin.");
  }

  /**
   * @param data
   * @param dc
   */
  public void escalarEstadoFromMovimientos(Tmct0desglosecamara dc, String auditUser) {
    LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] inicio.");
    BigDecimal titulosMovFinalizados = movBo.getSumNutitulosByIddesglosecamaraAndCdestadomov(dc.getIddesglosecamara(),
        EstadosEccMovimiento.FINALIZADO.text);
    LOG.trace("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] titulos movimientos activos: {} titulos dc: {}",
        titulosMovFinalizados, dc.getNutitulos());
    if (dc.getNutitulos() != null && titulosMovFinalizados != null
        && dc.getNutitulos().compareTo(titulosMovFinalizados) == 0) {
      Map<InformacionCompensacionDTO, List<Tmct0movimientoecc>> movimientosMismaIc = this
          .agruparMovimientosByInformacionCompensacion(dc);
      if (movimientosMismaIc.size() == 0) {
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
          LOG.debug(
              "[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR por estar antes PDTE_LIQUIDAR sin movs: {}",
              dc.getTmct0estadoByCdestadoasig().getIdestado());
          this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId(),
              auditUser);
        }
      }
      else if (movimientosMismaIc.size() == 1) {
        InformacionCompensacionDTO ic = movimientosMismaIc.keySet().iterator().next();
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.IMPUTADA_PROPIA.getId()
            || dc.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.IMPUTADA_S3.getId()) {
          if (ic.getMiembroPropietarioCuentaCompensacion() != null
              && !CodigoMiembroCompensadorSV.SV.text.equals(ic.getMiembroPropietarioCuentaCompensacion().trim())) {
            LOG.debug(
                "[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR por estar antes imputado: {}",
                dc.getTmct0estadoByCdestadoasig().getIdestado());
            dc.setImputacionpropia('N');
            dc.setEnvios3('N');
            dc.setFhenvios3(null);
            this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
                auditUser);

          }
        }
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() <= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
          // si el miembro es la sv y tiene cuenta
          if (StringUtils.isNotBlank(ic.getMiembroPropietarioCuentaCompensacion())
              && StringUtils.isNotBlank(ic.getCuentaCompensacion())
              && ic.getMiembroPropietarioCuentaCompensacion().trim().equals(CodigoMiembroCompensadorSV.SV.text)) {
            LOG.trace("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] buscando cuenta de compensacion en tabla maestra...");
            Tmct0CuentasDeCompensacion cuenta = cuentasDeCompensacionBO.findByCdCodigo(ic.getCuentaCompensacion());

            if (cuenta != null) {
              LOG.trace("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] buscando el tipo de la cuenta de compensacion en tabla maestra...");
              Tmct0TipoCuentaDeCompensacion tipoCuenta = cuenta.getTmct0TipoCuentaDeCompensacion();
              if (tipoCuenta != null) {
                tipoCuenta = tiposCuentaCompensacionBo.findById(tipoCuenta.getIdTipoCuenta());
              }

              if (tipoCuenta != null && "CD".equals(tipoCuenta.getCdCodigo())) {
                LOG.trace("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] tipo cuenta == {}",
                    tipoCuenta.getCdCodigo());
                LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.IMPUTADA_PROPIA");
                dc.setImputacionpropia('S');
                dc.setEnvios3('N');
                dc.setFhenvios3(null);
                this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.IMPUTADA_PROPIA.getId(),
                    auditUser);
              }
              else {
                if (dc.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
                    .getId()) {
                  LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR");
                  this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
                      auditUser);
                }
              }
            }
            else {
              if (dc.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR
                  .getId()) {
                LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR");
                this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
                    auditUser);
              }
            }
          }
          else {
            if (dc.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
              LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR");
              this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
                  auditUser);
            }
          }
        }

      }
      else {
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() >= EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
          LOG.debug(
              "[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR por estar antes PDTE_LIQUIDAR con mas de 1 movs: {}",
              dc.getTmct0estadoByCdestadoasig().getIdestado());
          this.ponerEstadoAsignacionDesgloseCamara(dc, EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId(),
              auditUser);
        }
      }

    }
    LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] fin.");
  }

  /**
   * @param dc
   */
  private void ponerEstadoAsignacionDesgloseCamara(Tmct0desglosecamara dc, int estadoAsignacion, String auditUser) {
    LOG.debug("[CuadreEccSibbacBo :: escalarEstadoFromMovimientos] cambiando estado a {}", estadoAsignacion);
    dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
    dc.getTmct0estadoByCdestadoasig().setIdestado(estadoAsignacion);
    dc.setAuditFechaCambio(new Date());
    dc.setAuditUser(auditUser);
    dcBo.save(dc);
    dc.getTmct0alc().setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    dc.getTmct0alc().setFhaudit(new Date());
    dc.getTmct0alc().setCdusuaud(auditUser);
    alcBo.save(dc.getTmct0alc());
    Tmct0alo alo = aloBo.findById(new Tmct0aloId(dc.getTmct0alc().getId().getNucnfclt(), dc.getTmct0alc().getId()
        .getNbooking(), dc.getTmct0alc().getId().getNuorden()));
    alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
    alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    alo.setFhaudit(new Date());
    alo.setCdusuaud(auditUser);
    aloBo.save(alo);
    alo.getTmct0bok().setTmct0estadoByCdestadoasig(new Tmct0estado());
    alo.getTmct0bok().getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());

    alo.getTmct0bok().setFhaudit(new Date());
    alo.getTmct0bok().setCdusuaud(auditUser);
    bokBo.save(dc.getTmct0alc().getTmct0alo().getTmct0bok());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void arreglarDesglosesCuadreEcc(List<CuadreEccDbReaderRecordBeanDTO> beans, int pageSize)
      throws SIBBACBusinessException {
    Collection<Tmct0bokId> boksRevisadasEjecuciones = new HashSet<Tmct0bokId>();
    Tmct0bokId bokId;
    List<Tmct0alc> alcs;

    boolean error;
    boolean sinInstrucciones;
    String msg;
    Tmct0alo alo;

    CanonesConfigDTO canonesConfig = reglasCanonesBo.getCanonesConfig();

    for (CuadreEccDbReaderRecordBeanDTO cuadreEccDbReaderRecordBeanDTO : beans) {
      error = false;
      sinInstrucciones = false;
      bokId = new Tmct0bokId(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(),
          cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden());

      try {
        alo = aloBo.findById(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());
        if (boksRevisadasEjecuciones.add(bokId)) {
          if (!aloBo.isTitulosAlosAltaOk(bokId)) {
            msg = "CUADRE_ECC-ARREGLAR_DESGLOSES-INCIDENCIA-Titulos alo's alta distinto de booking";
            LOG.warn("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] " + msg + " {}", bokId);
            tmct0logBo.insertarRegistro(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden(),
                cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(), msg, "", "");
          }
          else {
            LOG.debug("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] los titulos de los alos estan bien {}", bokId);
          }

          LOG.debug("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] revisando duplicados en ejecuciones {}", bokId);

          ejeBo.agruparEjecucionesArreglarDesgloses(bokId, pageSize);

        }
        if (!alcBo.isTitulosAlcsOk(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId())) {
          msg = "CUADRE_ECC-ARREGLAR_DESGLOSES-INCIDENCIA-Titulos alc's alta distinto de alo";
          LOG.warn("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] " + msg + " {}", bokId);
          tmct0logBo.insertarRegistro(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden(),
              cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(), cuadreEccDbReaderRecordBeanDTO
                  .getTmct0aloId().getNucnfclt(), msg, "", "");
          BigDecimal nutitliq = alcBo.getSumNutitliqByTmct0aloId(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());
          if (nutitliq == null || nutitliq.compareTo(BigDecimal.ZERO) == 0) {
            // alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
            // alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId());
            // alo.setFhaudit(new Date());
            // alo = aloBo.save(alo);
            msg = "CUADRE_ECC-ARREGLAR_DESGLOSES-Marcado alo desglose antomatico";
            LOG.warn("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] " + msg + " {}", bokId);
            tmct0logBo.insertarRegistro(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden(),
                cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(), cuadreEccDbReaderRecordBeanDTO
                    .getTmct0aloId().getNucnfclt(), msg, "", "");

            SettlementData settlementData = settlementDataBo.getSettlementDataNacional(alo);
            if (settlementData.isDataIL()) {
              this.desglosarAutomatico(canonesConfig, alo, settlementData);
            }
            else {
              sinInstrucciones = true;
              msg = "CUADRE_ECC-ARREGLAR_DESGLOSES-INCIDENCIA-No hay instrucciones de liquidacion para el alias: {0} fetrade: {1}";
              msg = MessageFormat.format(msg, alo.getCdalias(), alo.getFeoperac());
              LOG.warn("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] " + msg + " {}", bokId);
              tmct0logBo.insertarRegistro(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden(),
                  cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(), cuadreEccDbReaderRecordBeanDTO
                      .getTmct0aloId().getNucnfclt(), msg, "", "");
              ejealoBo.recalcularTitulosPendientesDesglose(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());
            }
            LOG.debug("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] recuperada {}", settlementData);

          }
          else {
            alcBo.arreglarDesglosesCuadreEcc(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());
          }
        }
        else {
          boolean titulosDcError = false;

          alcs = alcBo.findByAlloIdActivos(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());

          for (Tmct0alc tmct0alc : alcs) {
            if (dcBo.isTitulosDCOk(tmct0alc)) {
              LOG.debug("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] titulos dc Ok {}", tmct0alc.getId());
            }
            else {
              LOG.debug("[CuadreEccSibbacBo :: arreglarDesglsoesCuadreEcc] titulos dc ERROR {}", tmct0alc.getId());
              titulosDcError = true;
            }
          }

          if (titulosDcError) {
            alcBo.arreglarDesglosesCuadreEcc(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());
          }
          else {
            ejealoBo.recalcularTitulosPendientesDesglose(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId());
          }

        }

      }
      catch (SIBBACBusinessException e) {
        error = true;
        tmct0logBo.insertarRegistroNewTransaction(cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden(),
            cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(), "CUADRE_ECC-" + e.getMessage(), "", "");

        throw e;
      }
      catch (Exception e) {
        error = true;
        throw new SIBBACBusinessException(e);
      }
      finally {
        int estadoTerminacion = EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC
            .getId();
        if (sinInstrucciones) {
          estadoTerminacion = EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.SIN_INSTRUCCIONES.getId();
        }
        if (error) {
          cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflctNewTransaction(
              cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNuorden(), cuadreEccDbReaderRecordBeanDTO
                  .getTmct0aloId().getNbooking(), cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNucnfclt(),
              estadoTerminacion);
        }
        else {
          cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(cuadreEccDbReaderRecordBeanDTO
              .getTmct0aloId().getNuorden(), cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNbooking(),
              cuadreEccDbReaderRecordBeanDTO.getTmct0aloId().getNucnfclt(), estadoTerminacion);
        }

      }

    } // fin for
    canonesConfig.close();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void desglosarAutomatico(CanonesConfigDTO canonesConfig, Tmct0alo alo, SettlementData settlementData)
      throws SIBBACBusinessException, EconomicDataException {
    LOG.debug("[CuadreEccSibbacBo :: desglosarAutomatico] inicio");
    List<Tmct0ejecucionalocation> ejealos = ejealoBo.findAllByAlloId(alo.getId());
    List<Tmct0eje> ejes = ejeBo.findAllByNbookingAndNuordenAndNucnfclt(alo.getId().getNbooking(), alo.getId()
        .getNuorden(), alo.getId().getNucnfclt());
    Tmct0ord ord = ordBo.findById(alo.getId().getNuorden());
    Tmct0bok bok = bokBo.findById(new Tmct0bokId(alo.getId().getNbooking(), alo.getId().getNuorden()));
    Tmct0alc alc = desgloseAlcHelper.crearDesglosesAlc(ord, bok, alo, ejealos, ejes, settlementData);
    alo.getTmct0alcs().add(alc);

    Tcli0comDTO com = new Tcli0comDTO("", (short) 0, BigDecimal.ZERO, BigDecimal.ZERO);

    CalculosEconomicosNacionalBo calculos = ctx.getBean(CalculosEconomicosNacionalBo.class, alo, alo.getTmct0alcs(),
        canonesConfig, com);

    calculos.calcularDatosEconomicos();

    alcBo.persistirAlcs(alo, EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId(),
        EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());

    alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
    alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    alo.setFhaudit(new Date());
    alo = aloBo.save(alo);
    bok.setTmct0estadoByCdestadoasig(new Tmct0estado());
    bok.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    bok.setFhaudit(new Date());
    bokBo.save(bok);
    LOG.debug("[CuadreEccSibbacBo :: desglosarAutomatico] fin");
  }

} // CuadreEccSibbacBo
