package sibbac.business.comunicaciones.pti.mq.bo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.TptiInBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessagesBo;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageFactory;

@Service
public class JmsMessagePtiToDataBaseBo extends WrapperExecutor {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(JmsMessagePtiToDataBaseBo.class);

  private static final Logger LOG_PTI_IN = LoggerFactory.getLogger("PTI_IN");

  private static final boolean MULTITHREAD = true;

  private static final boolean IS_SCRIPT_DIVIDEND = true;

  @Value("${jms.pti.qmanager.name}")
  private String ptiJndiName;

  @Value("${jms.pti.qmanager.host}")
  private String ptiMqHost;

  @Value("${jms.pti.qmanager.port}")
  private int ptiMqPort;

  @Value("${jms.pti.qmanager.channel}")
  private String ptiMqChannel;

  @Value("${jms.pti.qmanager.qmgrname}")
  private String ptiMqManagerName;

  @Value("${jms.pti.queue.online.input.queue}")
  private String onlineIn;

  @Value("${jms.pti.queue.batch.input.queue}")
  private String batchIn;

  @Value("${jms.pti.queue.sd.input.queue}")
  private String batchInSd;

  @Value("${sibbac.comunicaciones.pti.path:/sbrmv/ficheros/pti/in}")
  private String workPath;

  @Value("${sibbac.comunicaciones.pti.path.tratados:/sbrmv/ficheros/pti/in/tratados}")
  private String tratadosPath;

  @Value("${sibbac.comunicaciones.pti.online.file.pattern:{*online*.log,*online*.gz,*online*.zip}}")
  private String onlineFilePattern;

  @Value("${sibbac.comunicaciones.pti.batch.file.pattern:{*batch*.log,*batch*.gz,*batch*.zip}}")
  private String batchFilePattern;

  @Value("${sibbac.comunicaciones.pti.sd.file.pattern:{*script*dividend*.log,*script*dividend*.gz,*script*dividend*.zip}}")
  private String sdFilePattern;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsMessageBo;

  @Autowired
  private TptiInBo tptiInBo;

  @Autowired
  private PtiCommomns ptiCommons;

  @Autowired
  private Tmct0cfgBo cfgBo;

  public JmsMessagePtiToDataBaseBo() {
  }

  public void executeTask(boolean sd) throws Exception {
    LOG.debug("[executeTask] inicio");
    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }
    Tmct0cfg cfg = cfgBo.getPtiMessageVersion();
    if (cfg == null) {
      throw new SIBBACBusinessException("INCIDENCIA-No configurada las fechas de las versiones de la mensajeria pti");
    }
    String cfgVersion = cfg.getKeyvalue();

    if (!sd) {
      if (StringUtils.isNotBlank(onlineIn)) {
        this.loadFromMq(onlineIn, !IS_SCRIPT_DIVIDEND, !MULTITHREAD, cfgVersion);
      }
      if (StringUtils.isNotBlank(batchIn)) {
        this.loadFromMq(batchIn, !IS_SCRIPT_DIVIDEND, MULTITHREAD, cfgVersion);
      }
    }
    else {
      if (StringUtils.isNotBlank(batchInSd)) {
        this.loadFromMq(batchInSd, IS_SCRIPT_DIVIDEND, MULTITHREAD, cfgVersion);
      }
    }

    if (!sd) {
      if (StringUtils.isNotBlank(onlineFilePattern)) {
        this.loadFromFs(onlineFilePattern, !IS_SCRIPT_DIVIDEND, !MULTITHREAD, cfgVersion);
      }

      if (StringUtils.isNotBlank(batchFilePattern)) {
        this.loadFromFs(batchFilePattern, !IS_SCRIPT_DIVIDEND, MULTITHREAD, cfgVersion);
      }
    }
    else {
      if (StringUtils.isNotBlank(sdFilePattern)) {
        this.loadFromFs(sdFilePattern, IS_SCRIPT_DIVIDEND, MULTITHREAD, cfgVersion);
      }
    }
    LOG.debug("[executeTask] fin");
  }

  @SuppressWarnings("rawtypes")
  private void loadFromFs(String filePatterh, boolean isScriptDividend, boolean threaded, String cfgVersion)
      throws IOException, SIBBACBusinessException {
    LOG.debug("[loadFromFs] workPath: {} filePattern: {}", workPath.toString(), filePatterh);
    ZipEntry ze;
    String originalName;
    String fileBaseName;
    String fileExtension;
    Path procesandoFilePath;
    Path tratadoFilePath;

    Path workPathPath = Paths.get(workPath);
    Path tratadosPathPath = Paths.get(tratadosPath);

    if (Files.notExists(workPathPath)) {
      Files.createDirectories(workPathPath);
    }

    if (Files.notExists(tratadosPathPath)) {
      Files.createDirectories(tratadosPathPath);
    }

    try (DirectoryStream<Path> dir = Files.newDirectoryStream(workPathPath, filePatterh);) {
      ExecutorService executor = this.getNewExecutor();
      List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
      try {
        for (Path path : dir) {

          LOG.debug("[loadFromFs] tratando fichero: {}", path.toString());
          originalName = path.getFileName().toString();
          fileBaseName = FilenameUtils.getBaseName(path.toString());
          fileExtension = FilenameUtils.getExtension(path.toString());
          procesandoFilePath = Paths.get(workPath, String.format("%s_%s.%s", fileBaseName,
              FormatDataUtils.convertDateToConcurrentFileName(new Date()), "procesando"));

          tratadoFilePath = Paths.get(tratadosPath, String.format("%s_%s.%s", fileBaseName,
              FormatDataUtils.convertDateToConcurrentFileName(new Date()), fileExtension));
          LOG.debug("[loadFromFs] moviendo fichero: {} a {}", path.toString(), procesandoFilePath.toString());
          path = Files.move(path, procesandoFilePath, StandardCopyOption.ATOMIC_MOVE);

          if (originalName.toUpperCase().endsWith(".ZIP")) {
            try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(path, StandardOpenOption.READ));
                BufferedReader br = new BufferedReader(new InputStreamReader(zis));) {
              while ((ze = zis.getNextEntry()) != null) {
                LOG.debug("[loadFromFs] procesando fichero del zip: {}", ze.getName());
                this.procesarLineas(executor, listFutures, br, isScriptDividend, threaded, cfgVersion);
                zis.closeEntry();
              }
            }
            LOG.debug("[loadFromFs] moviendo fichero: {} a {}", path.toString(), tratadoFilePath.toString());
            Files.move(path, tratadoFilePath, StandardCopyOption.ATOMIC_MOVE);
          }
          else if (originalName.toUpperCase().endsWith(".GZ")) {
            try (GZIPInputStream gis = new GZIPInputStream(Files.newInputStream(path, StandardOpenOption.READ));
                BufferedReader br = new BufferedReader(new InputStreamReader(gis));) {
              this.procesarLineas(executor, listFutures, br, isScriptDividend, threaded, cfgVersion);
            }
            LOG.debug("[loadFromFs] moviendo fichero: {} a {}", path.toString(), tratadoFilePath.toString());
            Files.move(path, tratadoFilePath, StandardCopyOption.ATOMIC_MOVE);
          }
          else {
            try (InputStream gis = Files.newInputStream(path, StandardOpenOption.READ);
                BufferedReader br = new BufferedReader(new InputStreamReader(gis));) {
              this.procesarLineas(executor, listFutures, br, isScriptDividend, threaded, cfgVersion);
            }
            Path tmpPath = Paths.get(tratadosPath, String.format("%s_%s.%s.%s", fileBaseName,
                FormatDataUtils.convertDateToConcurrentFileName(new Date()), fileExtension, "gz"));
            LOG.debug("[loadFromFs] moviendo fichero: {} a {}", path.toString(), tmpPath.toString());
            try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);
                GZIPOutputStream gos = new GZIPOutputStream(Files.newOutputStream(tmpPath,
                    StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE));) {
              IOUtils.copyLarge(is, gos);
            }
            Files.deleteIfExists(path);
          }

        }// fin fichero
      }
      catch (Exception e) {
        LOG.trace(e.getMessage(), e);
        if (executor != null) {
          closeExecutorNow(executor);

        }
      }
      finally {
        if (executor != null) {
          closeExecutor(executor);
        }
      }

      for (Future future : listFutures) {
        try {
          future.get(0, TimeUnit.SECONDS);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
          future.cancel(true);
          throw new SIBBACBusinessException(e);
        }
        catch (Exception e) {
          future.cancel(true);
          throw new SIBBACBusinessException(e);
        }
      }
    }
  }

  private void procesarLineas(ExecutorService executor, List<Future<Void>> listFutures, BufferedReader br,
      boolean isScriptDividend, boolean multithread, String cfgVersion) throws IOException {
    String msg;
    int groupSeq = 1;
    String groupId = br.hashCode() + "";
    while ((msg = br.readLine()) != null) {
      if (msg.contains(" - ")) {
        msg = msg.substring(msg.indexOf(" - ") + " - ".length());
      }
      LOG_PTI_IN.info(msg);
      if (multithread) {
        this.submitCallable(executor, this.getHiloConsumidorFs(msg, groupId, groupSeq++, isScriptDividend, cfgVersion));

      }
      else {
        tptiInBo.savePtiMessageNewTransaction(msg, groupId, groupId, groupSeq, false, System.currentTimeMillis(),
            "SIBBAC20", isScriptDividend, cfgVersion);
      }
    }
  }

  private void loadFromMq(String queueName, boolean isScriptDividend, boolean threaded, String cfgVersion)
      throws JMSException, SIBBACBusinessException {

    LOG.debug("[loadFromMq] inicio");

    boolean sessionTransacted = !threaded;
    ConnectionFactory cf = null;
    try {
      cf = jmsMessageBo.getConnectionFactoryFromContext(ptiJndiName);
    }
    catch (Exception e) {
      cf = jmsMessageBo.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
    }

    List<Future<Void>> listFurutres = new ArrayList<Future<Void>>();
    ExecutorService executor = null;
    try (Connection connection = jmsMessageBo.getNewConnection(cf);) {
      connection.start();
      try (Session session = jmsMessageBo.getNewSession(connection, sessionTransacted);
          MessageConsumer messageConsumer = jmsMessageBo.getNewConsumer(session, queueName)) {

        try {
          int messageCount = jmsMessageBo.countQueueMessages(session, queueName);
          if (messageCount > 0) {
            executor = getNewExecutor();

            LOG.trace("[loadFromMq] Count Messages: {} in queue: {}", messageCount, queueName);
            if (threaded) {
              this.ejecutarHilosConsumidores(executor, listFurutres, cf, queueName, getTransactionSize(),
                  isScriptDividend, cfgVersion, messageCount);
            }
            else {
              this.receiveAndProcessMessage(session, messageConsumer, messageCount, isScriptDividend, cfgVersion);
            }
            LOG.debug("[loadFromMq] se han consumido los mensajes {} que habia originalmente en la cola: {}",
                messageCount, queueName);
          }
          else {
            LOG.debug("[loadFromMq]no hay mensajes en la cola: {}", queueName);
          }

        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          closeExecutorNow(executor);
          if (session != null) {
            session.rollback();
          }
          throw new SIBBACBusinessException(e);
        }
        finally {
          if (executor != null)
            closeExecutor(executor);
        }
      }
    }

    for (Future<Void> future : listFurutres) {
      try {
        future.get(0, TimeUnit.SECONDS);
      }
      catch (InterruptedException | ExecutionException | TimeoutException e) {
        future.cancel(true);
        throw new SIBBACBusinessException(e);
      }
      catch (Exception e) {
        future.cancel(true);
        throw new SIBBACBusinessException(e);
      }
    }
    LOG.debug("[loadFromMq] fin");
  }

  private Callable<Void> getNewThreadToProcessMessage(final ConnectionFactory connectionFactory,
      final String queueName, final int consumeMessagesCount, final boolean isScriptDividend, final String cfgVersion) {
    Callable<Void> p = new Callable<Void>() {

      @Override
      public Void call() throws SIBBACBusinessException {
        try (Connection connection = jmsMessageBo.getNewConnection(connectionFactory);) {
          connection.start();
          try (Session session = jmsMessageBo.getNewSession(connection, true);
              MessageConsumer messageConsumer = jmsMessageBo.getNewConsumer(session, queueName);) {
            try {
              receiveAndProcessMessage(session, messageConsumer, consumeMessagesCount, isScriptDividend, cfgVersion);
              LOG.debug("[loadFromMq] No hay mensajes en la cola: {}", queueName);
            }
            catch (Exception e) {
              if (session != null) {
                session.rollback();
              }
              throw e;
            }
          }
        }
        catch (Exception e1) {

          throw new SIBBACBusinessException(e1.getMessage(), e1);
        }
        finally {
        }
        return null;
      }
    };

    return p;
  }

  @SuppressWarnings("unchecked")
  private void ejecutarHilosConsumidores(ExecutorService executor, List<Future<Void>> listFutures,
      ConnectionFactory connectionFactory, String queueName, int transactionSize, boolean isScriptDividend,
      String cfgVersion, int hilos) throws SIBBACBusinessException {
    for (int i = 0; i < hilos; i++) {
      listFutures.add(this.submitCallable(executor, this.getNewThreadToProcessMessage(connectionFactory, queueName,
          transactionSize, isScriptDividend, cfgVersion)));

    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void receiveAndProcessMessage(Session session, MessageConsumer messageConsumer, int messageCount,
      boolean isScriptDividend, String cfgVersion) throws JMSException, SIBBACBusinessException, IOException {
    String groupId;
    Integer groupSeq;
    boolean isLast;
    for (int i = 0; i < messageCount; i++) {
      Message msg = jmsMessageBo.receiveMessage(messageConsumer, -1);
      if (msg != null) {
        LOG.trace("[receiveAndProcessMessage] Receive Message: {}", msg);
        String msgSt = jmsMessageBo.getText(msg);
        LOG_PTI_IN.info(msgSt);
        groupId = jmsMessageBo.getGroupId(msg);
        groupSeq = jmsMessageBo.getGroupSeq(msg);
        isLast = jmsMessageBo.getIsLast(msg);
        tptiInBo.savePtiMessageNewTransaction(msgSt, msg.getJMSMessageID(), groupId, groupSeq, isLast,
            msg.getJMSTimestamp(), "SIBBAC20", isScriptDividend, cfgVersion);
        LOG.trace("[receiveAndProcessMessage] Acknowledge Message: {}", msg);
        msg.acknowledge();
        session.commit();
      }
      else {
        break;
      }
    }
  }

  private Callable<Void> getHiloConsumidorFs(final String msgSt, final String groupId, final Integer groupSeq,
      final boolean isScriptDividend, final String cfgVersion) {
    Callable<Void> p = new Callable<Void>() {

      @Override
      public Void call() throws SIBBACBusinessException {
        try {
          tptiInBo.savePtiMessageNewTransaction(msgSt, msgSt.hashCode() + "", groupId, groupSeq, false,
              System.currentTimeMillis(), "SIBBAC20", isScriptDividend, cfgVersion);
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(e);
        }
        finally {

        }
        return null;
      }
    };
    return p;
  }

  @Override
  public int getThreadSize() {
    return 25;
  }

  @Override
  public int getTransactionSize() {
    return 1;
  }

}
