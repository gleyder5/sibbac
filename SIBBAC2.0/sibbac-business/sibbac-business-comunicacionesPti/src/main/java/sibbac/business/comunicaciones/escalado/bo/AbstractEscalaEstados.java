/**
 * 
 */
package sibbac.business.comunicaciones.escalado.bo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.escalado.dto.EscalaEstadosConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0aloDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0bokDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0desgloseCamaraDaoImpl;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

/**
 * @author XIS16630
 * @version 2 @author xIS16630-Jonatan San Andres Gil
 */
public abstract class AbstractEscalaEstados extends WrapperExecutor {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(AbstractEscalaEstados.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  private Tmct0ordBo tmct0ordBo;

  @Autowired
  private Tmct0bokDaoImp tmct0bokDaoImp;

  @Autowired
  private Tmct0aloDaoImp tmct0aloDaoImp;

  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private Tmct0desgloseCamaraDaoImpl tmct0desgloseCamaraDaoImpl;

  @Autowired
  private Tmct0estadoBo estadoBo;

  @Autowired
  private Tmct0cfgBo configBo;

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoIncidencias
   * ()
   */
  // @Override
  public abstract Integer getEstadoIncidencias();

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoRechazado()
   */
  // @Override
  public abstract Integer getEstadoRechazado();

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoEnCurso()
   */
  // @Override
  public abstract Integer getEstadoEnCurso();

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoTratandose()
   */
  // @Override
  public abstract Integer getEstadoTratandose();

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getNombreCampoEstado
   * ()
   */
  // @Override
  public abstract String getNombreCampoEstado();

  @Transactional(readOnly = true)
  public void escalarEstados(boolean isScriptDividend) throws SIBBACBusinessException {
    long size;
    try {
      LOG.trace("[escalarEstados] buscando bookings que escalar el campo {} ...", getNombreCampoEstado());
      List<Object[]> listBookings = this.getBookingsEscalar(isScriptDividend);
      LOG.trace("[escalarEstados] encontrados {} bookings que escalar el campo {}", listBookings.size(),
          getNombreCampoEstado());
      if (CollectionUtils.isNotEmpty(listBookings)) {
        EscalaEstadosConfigDTO config = getConfig();
        int tam = calculateTransactionSizeBetweenThreads(listBookings.size());
        int pos = 0;
        int posEnd = 0;
        List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        if (isScriptDividend) {
          tfb.setNameFormat(String.format("Thread-Escalado-SD-%s-%s", getNombreCampoEstado(), "%d"));
        }
        else {
          tfb.setNameFormat(String.format("Thread-Escalado-%s-%s", getNombreCampoEstado(), "%d"));
        }
        ExecutorService executor = Executors.newFixedThreadPool(getThreadSize(), tfb.build());
        try {
          while (pos < listBookings.size()) {
            posEnd = Math.min(posEnd + tam, listBookings.size());
            listFutures.add(executor.submit(this.getCallableProcessEscalaEstado(listBookings.subList(pos, posEnd),
                config)));
            pos = posEnd;

            // if (pos % tam * 10 == 0) {
            // LOG.trace("[escalarEstados] escalando lista i: {} de {}", pos,
            // listBookings.size());
            // }
            size = ((ThreadPoolExecutor) executor).getTaskCount()
                - ((ThreadPoolExecutor) executor).getCompletedTaskCount();
            if (size > 10000) {
              Thread.sleep(500);
            }
          }
        }
        catch (Exception e) {
          if (executor != null)
            closeExecutorNow(executor);
          throw new SIBBACBusinessException(String.format("Incidencia lanzando los procesos de escalado %s",
              e.getMessage()), e);
        }
        finally {
          if (executor != null)
            closeExecutor(executor);
        }

        for (Future<Void> future : listFutures) {
          future.get(0, TimeUnit.SECONDS);
        }
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
  }

  @Transactional
  private EscalaEstadosConfigDTO getConfig() throws SIBBACBusinessException {

    String sqlAllocation = getSelectAlosQuery();
    String sqlAllocationToClearer = getSelectAlcsQuery();
    String sqlDesgloseCamara = getSelectDcsQuery();
    String sqlAllocationToClearerGrouping = getSelectAlcsGroupQuery();
    String sqlAllocationGrouping = getSelectAlosGroupQuery();
    Long tiempoMaxProcesoBok = configBo.getMinutosEscaladoBokNoActivo();
    if (tiempoMaxProcesoBok == null) {
      throw new SIBBACBusinessException("Tiempo maximo en curso para booking no configurado.");
    }
    Long tiempoMaxProcesoAlo = configBo.getMinutosEscaladoAloNoActivo();
    if (tiempoMaxProcesoAlo == null) {
      throw new SIBBACBusinessException("Tiempo maximo en curso para alo no configurado.");
    }
    Long tiempoMaxProcesoAlc = configBo.getMinutosEscaladoAlcNoActivo();
    if (tiempoMaxProcesoAlc == null) {
      throw new SIBBACBusinessException("Tiempo maximo en curso para alc no configurado.");
    }
    Integer estadoIncidencias = getEstadoIncidencias();
    Integer estadoRechazado = getEstadoRechazado();
    Integer estadoEnCurso = getEstadoEnCurso();
    Integer estadoTratandose = getEstadoTratandose();

    String nombreCampoEstado = getNombreCampoEstado();

    EscalaEstadosConfigDTO config = new EscalaEstadosConfigDTO();

    config.setEstadoEnCurso(estadoEnCurso);
    config.setEstadoIncidencias(estadoIncidencias);
    config.setEstadoRechazado(estadoRechazado);
    config.setEstadoTratandose(estadoTratandose);
    config.setNombreCampoEstado(nombreCampoEstado);
    config.setSqlAllocation(sqlAllocation);
    config.setSqlAllocationGrouping(sqlAllocationGrouping);
    config.setSqlAllocationToClearer(sqlAllocationToClearer);
    config.setSqlAllocationToClearerGrouping(sqlAllocationToClearerGrouping);
    config.setSqlDesgloseCamara(sqlDesgloseCamara);
    config.setTiempoMaxProcesoAlc(tiempoMaxProcesoAlc);
    config.setTiempoMaxProcesoAlo(tiempoMaxProcesoAlo);
    config.setTiempoMaxProcesoBok(tiempoMaxProcesoBok);
    config.setEstadosEquivalentes(estadoBo.getMapEstadosEquivalentes());

    return config;
  }

  private Callable<Void> getCallableProcessEscalaEstado(final List<Object[]> listBookings,
      final EscalaEstadosConfigDTO config) {
    Callable<Void> c = new Callable<Void>() {

      @Override
      public Void call() throws Exception {
        escalaEstadosListBookings(listBookings, config);
        return null;
      }
    };
    return c;
  }

  private List<Object[]> getBookingsEscalar(boolean sd) {
    Map<String, Object> params = new HashMap<String, Object>();
    params.clear();
    params.put("pcdestado1", getEstadoEnCurso());
    params.put("pcdestado2", getEstadoTratandose());
    params.put("pnotcdestado1", getEstadoRechazado());
    params.put("pnotcdestadoasig", ASIGNACIONES.RECHAZADA.getId());

    params.put("isscrdiv", sd ? 'S' : 'N');

    return tmct0bokDaoImp.prepareAndExecuteNativeQuery(getSelectBookingsQuery(), params);
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * 
   * @param listbooking
   * @param tiempoMaxProcesoBooking
   * @param tiempoMaxProcesoAlo
   * @param tiempoMaxProcesoAlc
   * @return
   * @throws Exception
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void escalaEstadosListBookings(List<Object[]> listbooking, EscalaEstadosConfigDTO config)
      throws SIBBACBusinessException {
    String nuorden;
    String nbooking;
    String nucnfclt;
    BigDecimal nucnfliq;
    Timestamp fhaudit;

    try {
      Map<String, Object> params = new HashMap<String, Object>();

      Long count = 0L;
      List<Object[]> tmct0aloList;
      List<Object[]> tmct0aloList1;
      List<Object[]> tmct0alcList;
      List<Object[]> tmct0alcList1;
      List<Object[]> tmct0desglosecamaraList;

      for (Object[] pkbok : listbooking) {
        nuorden = (String) pkbok[0];
        nbooking = (String) pkbok[1];
        count++;
        LOG.trace("Escalando booking EN CURSO ## nuorden: '{}' ## nbooking: '{}' ", nuorden, nbooking);
        try {

          // Se buscan los alo's no rechazados del booking
          params.clear();
          params.put("pcdestado1", getEstadoEnCurso());
          params.put("pcdestado2", getEstadoTratandose());
          params.put("nuorden", nuorden);
          params.put("nbooking", nbooking);
          params.put("pnotcdestado1", getEstadoRechazado());
          params.put("pnotcdestadoasig", ASIGNACIONES.RECHAZADA.getId());
          tmct0aloList = tmct0aloDaoImp.prepareAndExecuteNativeQuery(config.getSqlAllocation(), params);

          if (tmct0aloList != null && !tmct0aloList.isEmpty()) {
            for (Object[] tmct0alo : tmct0aloList) {
              nucnfclt = (String) tmct0alo[2];
              LOG.trace("Encontrados alos EN CURSO ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}'", nuorden,
                  nbooking, nucnfclt);

              // Se buscan los alc's no rechazados de un alo
              params.clear();
              params.put("pcdestado1", getEstadoEnCurso());
              params.put("nuorden", nuorden);
              params.put("nbooking", nbooking);
              params.put("nucnfclt", nucnfclt);
              params.put("pnotcdestado1", getEstadoRechazado());
              params.put("pnotcdestadoasig", ASIGNACIONES.RECHAZADA.getId());
              tmct0alcList = tmct0alcDaoImp.prepareAndExecuteNativeQuery(config.getSqlAllocationToClearer(), params);

              if (tmct0alcList != null && !tmct0alcList.isEmpty()) {
                for (Object[] tmct0alc : tmct0alcList) {
                  nucnfliq = (BigDecimal) tmct0alc[3];
                  LOG.trace(
                      "Encontrados alcs EN CURSO ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}'",
                      nuorden, nbooking, nucnfclt, nucnfliq);

                  // Se buscan los dc's de un alc
                  params.clear();
                  params.put("nuorden", nuorden);
                  params.put("nbooking", nbooking);
                  params.put("nucnfclt", nucnfclt);
                  params.put("nucnfliq", nucnfliq);
                  params.put("pnotcdestado1", getEstadoRechazado());
                  params.put("pnotcdestadoasig", ASIGNACIONES.RECHAZADA.getId());
                  tmct0desglosecamaraList = tmct0desgloseCamaraDaoImpl.prepareAndExecuteNativeQuery(
                      config.getSqlDesgloseCamara(), params);

                  fhaudit = (Timestamp) tmct0alc[4];
                  escalarDcAlc(nuorden, nbooking, nucnfclt, nucnfliq, fhaudit, tmct0desglosecamaraList, config);
                } // for (Object[] tmct0alc : tmct0alcList)

                tmct0alcList.clear();
              } // if (tmct0alcList != null && !tmct0alcList.isEmpty())

              params.clear();
              params.put("nuorden", nuorden);
              params.put("nbooking", nbooking);
              params.put("nucnfclt", nucnfclt);
              params.put("pnotcdestado1", getEstadoRechazado());
              params.put("pnotcdestadoasig", ASIGNACIONES.RECHAZADA.getId());
              tmct0alcList1 = tmct0alcDaoImp.prepareAndExecuteNativeQuery(config.getSqlAllocationToClearerGrouping(),
                  params);

              if (tmct0alcList1 != null && !tmct0alcList1.isEmpty()) {
                fhaudit = (Timestamp) tmct0alo[3];
                escalarAlcAlo(nuorden, nbooking, nucnfclt, fhaudit, tmct0alcList1, config);
                tmct0alcList1.clear();
              } // if

            } // for (Object[] tmct0alo : tmct0aloList)

            tmct0aloList.clear();
          } // if (tmct0aloList != null && !tmct0aloList.isEmpty())

          params.clear();
          params.put("nuorden", nuorden);
          params.put("nbooking", nbooking);
          params.put("pnotcdestado1", getEstadoRechazado());
          params.put("pnotcdestadoasig", ASIGNACIONES.RECHAZADA.getId());
          tmct0aloList1 = tmct0aloDaoImp.prepareAndExecuteNativeQuery(config.getSqlAllocationGrouping(), params);

          if (tmct0aloList1 != null && !tmct0aloList1.isEmpty()) {
            fhaudit = (Timestamp) pkbok[2];
            escalarAloBok(nuorden, nbooking, fhaudit, tmct0aloList1, config);
            tmct0aloList1.clear();
          }else{
            tmct0aloList1 = new ArrayList<Object[]>();
            tmct0aloList1.add(new Object[]{nuorden, nbooking, getEstadoRechazado()});
            fhaudit = (Timestamp) pkbok[2];
            escalarAloBok(nuorden, nbooking, fhaudit,  tmct0aloList1, config);
            tmct0aloList1.clear();
          }
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(String.format("Incidencia escalando booking: %s-%s campo %s %s", nuorden,
              nbooking, getNombreCampoEstado(), e.getMessage()), e);
        }
      } // for (Object[] pkbok : listbooking)

      // listbooking.clear();
    }
    catch (SIBBACBusinessException e) {
      LOG.warn("Haciendo rollback ...");

      throw e;
    }
    catch (Exception e) {
      LOG.warn("Haciendo rollback ...");

      throw new SIBBACBusinessException(String.format("Incidencia con grupo de bookings %s  escalando estado %s %s",
          listbooking, getNombreCampoEstado(), e.getMessage()), e);
    } // catch
  } // process

  /**
   * 
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   * @param fhaudit
   * @param listdeslosecamara
   * @param tiempoMaxProcesoAlc
   * @param estadoIncidencias
   * @param estadoRechazado
   * @param estadoEnCurso
   * @param estadoTratandose
   * @param nombreCampoEstado
   * @param em
   */
  @Transactional
  private void escalarDcAlc(String nuorden, String nbooking, String nucnfclt, BigDecimal nucnfliq, Timestamp fhaudit,
      List<Object[]> listdeslosecamara, EscalaEstadosConfigDTO config) {
    Integer status = null;
    if (CollectionUtils.isNotEmpty(listdeslosecamara)) {
      Collection<Integer> estados = new HashSet<Integer>();
      for (Object[] objects : listdeslosecamara) {
        estados.add((Integer) objects[4]);
      }
      estados.clear();
      status = getEstadoEquivalente(config.getEstadosEquivalentes(), estados);
      if (listdeslosecamara.size() == 1 || status != null) {
        LOG.trace(
            "Escalando estado a tmct0alc ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}' a estado: '{}'. Solo hay un desglose camara.",
            nuorden, nbooking, nucnfclt, nucnfliq, getNombreCampoEstado());
        if (status == null) {
          status = (Integer) listdeslosecamara.get(0)[4];
        }
        if (status != getEstadoEnCurso() && status != getEstadoTratandose()) {
          LOG.trace(
              "Escalando estado a tmct0alc ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}' a estado: '{}'",
              nuorden, nbooking, nucnfclt, nucnfliq, status);
          actualizarEstadoAlc(nuorden, nbooking, nucnfclt, nucnfliq, status);
        }
        else {
          if ((new java.sql.Timestamp(System.currentTimeMillis()).getTime() - fhaudit.getTime()) > config
              .getTiempoMaxProcesoAlc()) {
            LOG.trace(
                "Escalando estado INCIDENCIAS a tmct0alc. Demasiado tiempo en estado EN CURSO ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}' a estado: '{}'",
                nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
            actualizarEstadoDcsIncidencias(nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
            actualizarEstadoAlc(nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
          } // if
        } // else
      }
      else {
        LOG.trace(
            "Escalando estado a tmct0alc ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}' a estado: '{}'. Hay varios desglose camara.",
            nuorden, nbooking, nucnfclt, nucnfliq, getNombreCampoEstado());
        Boolean bincidenciaalo = false;
        for (Object[] pkdesglosecamara : listdeslosecamara) {
          status = (Integer) pkdesglosecamara[4];
          if (status == getEstadoEnCurso() || status == getEstadoTratandose()
              || status == config.getEstadoIncidencias()) {
            bincidenciaalo = true;
            break;
          } // if
        } // for (Object[] pkdesglosecamara : listdeslosecamara)
        if (!bincidenciaalo) {
          if ((new java.sql.Timestamp(System.currentTimeMillis()).getTime() - fhaudit.getTime()) > config
              .getTiempoMaxProcesoAlc()) {
            LOG.trace(
                "Escalando estado INCIDENCIAS a tmct0alc. Demasiado tiempo en estado EN CURSO ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}' a estado: '{}'",
                nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
            actualizarEstadoDcsIncidencias(nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
            actualizarEstadoAlc(nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
          } // if
        } // if (!bincidenciaalo)
      } // else
    }
    else {
      // El alc no tiene dc's
      LOG.trace(
          "Escalando estado a tmct0alc ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' ## nucnfliq: '{}' a estado: '{}'.",
          nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
      actualizarEstadoAlc(nuorden, nbooking, nucnfclt, nucnfliq, config.getEstadoIncidencias());
    } // else
  } // escalarDcAlc

  /**
   * 
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param fhaudit
   * @param listalcs
   * @param tiempoMaxProcesoAlo
   * @param estadoIncidencias
   * @param estadoRechazado
   * @param estadoEnCurso
   * @param estadoTratandose
   * @param nombreCampoEstado
   * @param em
   */
  @Transactional
  private void escalarAlcAlo(String nuorden, String nbooking, String nucnfclt, Timestamp fhaudit,
      List<Object[]> listalcs, EscalaEstadosConfigDTO config) {
    Integer status = null;
    if (listalcs != null && !listalcs.isEmpty()) {
      Collection<Integer> estados = new HashSet<Integer>();
      for (Object[] objects : listalcs) {
        estados.add((Integer) objects[3]);
      }
      estados.clear();
      status = getEstadoEquivalente(config.getEstadosEquivalentes(), estados);
      if (listalcs.size() == 1 || status != null) {
        LOG.trace(
            "Escalando estado a tmct0alo ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' a estado: '{}'. Solo hay un ALC.",
            nuorden, nbooking, nucnfclt, getNombreCampoEstado());
        if (status == null) {
          status = (Integer) listalcs.get(0)[3];
        }

        if (status != getEstadoEnCurso() && status != getEstadoTratandose()) {
          LOG.trace("Escalando estado a tmct0alo ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' a estado: '{}'",
              nuorden, nbooking, nucnfclt, status);
          actualizarEstadoAlo(nuorden, nbooking, nucnfclt, status);
        }
        else {
          if ((new java.sql.Timestamp(System.currentTimeMillis()).getTime() - fhaudit.getTime()) > config
              .getTiempoMaxProcesoAlo()) {
            LOG.trace(
                "Escalando estado INCIDENCIAS a tmct0alo. Demasiado tiempo en estado EN CURSO ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' a estado: '{}'",
                nuorden, nbooking, nucnfclt, getEstadoIncidencias());
            actualizarEstadoAlcsIncidencias(nuorden, nbooking, nucnfclt, getEstadoIncidencias());
            actualizarEstadoAlo(nuorden, nbooking, nucnfclt, getEstadoIncidencias());
          } // if
        } // else
      }
      else {
        LOG.trace(
            "Escalando estado a tmct0alo ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' a estado: '{}'. Hay varios ALC.",
            nuorden, nbooking, nucnfclt, getNombreCampoEstado());
        boolean bincidenciaalo = false;
        for (Object[] pkalc2 : listalcs) {
          status = (Integer) pkalc2[3];
          if (status == getEstadoEnCurso() || status == getEstadoTratandose() || status == getEstadoIncidencias()) {
            bincidenciaalo = true;
            break;
          } // if
        } // for (Object[] pkalc2 : listalcs)
        if (!bincidenciaalo) {
          if ((new java.sql.Timestamp(System.currentTimeMillis()).getTime() - fhaudit.getTime()) > config
              .getTiempoMaxProcesoAlo()) {
            LOG.trace(
                "Escalando estado INCIDENCIAS a tmct0alo. Demasiado tiempo en estado EN CURSO ## nuorden: '{}' ## nbooking: '{}' ## nucnfclt: '{}' a estado: '{}'",
                nuorden, nbooking, nucnfclt, getEstadoIncidencias());
            actualizarEstadoAlcsIncidencias(nuorden, nbooking, nucnfclt, getEstadoIncidencias());
            actualizarEstadoAlo(nuorden, nbooking, nucnfclt, getEstadoIncidencias());
          } // if
        } // if (!bincidenciaalo)
      } // else
    } // if (listalcs != null && !listalcs.isEmpty())
  } // escalarAlcAlo

  /**
   * 
   * @param nuorden
   * @param nbooking
   * @param fhaudit
   * @param listalos
   * @param tiempoMaxProcesoBok
   * @param estadoIncidencias
   * @param estadoRechazado
   * @param estadoEnCurso
   * @param estadoTratandose
   * @param nombreCampoEstado
   * @param em
   */
  @Transactional
  private void escalarAloBok(String nuorden, String nbooking, Timestamp fhaudit, List<Object[]> listalos,
      EscalaEstadosConfigDTO config) {
    Integer status = null;
    if (listalos != null && !listalos.isEmpty()) {
      Collection<Integer> estados = new HashSet<Integer>();
      for (Object[] objects : listalos) {
        estados.add((Integer) objects[2]);
      }
      status = getEstadoEquivalente(config.getEstadosEquivalentes(), estados);
      estados.clear();
      if (listalos.size() == 1 || status != null) {
        LOG.trace("Escalando estado a tmct0bok ## nuorden: '{}' ## nbooking: '{}' a estado: '{}'. Solo hay un ALO.",
            nuorden, nbooking, getNombreCampoEstado());
        if (status == null) {
          status = ((Integer) listalos.get(0)[2]).intValue();
        }

        if (status != getEstadoEnCurso() && status != getEstadoTratandose()) {
          LOG.trace("Escalando estado a tmct0bok ## nuorden: '{}' ## nbooking: '{}' a estado: '{}'", nuorden, nbooking,
              status);
          actualizarEstadoBok(nuorden, nbooking, status);
        }
        else {
          if ((new java.sql.Timestamp(System.currentTimeMillis()).getTime() - fhaudit.getTime()) > config
              .getTiempoMaxProcesoBok()) {
            LOG.trace(
                "Escalando estado INCIDENCIAS a tmct0bok. Demasiado tiempo en estado EN CURSO ## nuorden: '{}' ## nbooking: '{}' a estado: '{}'",
                nuorden, nbooking, getEstadoIncidencias());
            actualizarEstadoAlosIncidencias(nuorden, nbooking, config.getEstadoIncidencias());
            actualizarEstadoBok(nuorden, nbooking, config.getEstadoIncidencias());
          } // if
        } // else
      }
      else {
        LOG.trace("Escalando estado a tmct0bok ## nuorden: '{}' ## nbooking: '{}' a estado: '{}'. Hay varios ALO.",
            nuorden, nbooking, getNombreCampoEstado());
        boolean bincidenciabok = false;
        for (Object[] pkalo1 : listalos) {
          status = (Integer) pkalo1[2];
          if (status == getEstadoEnCurso() || status == getEstadoTratandose() || status == getEstadoIncidencias()) {
            bincidenciabok = true;
            break;
          } // if
        } // for (Object[] pkalo1 : listalos)
        if (!bincidenciabok) {
          if ((new java.sql.Timestamp(System.currentTimeMillis()).getTime() - fhaudit.getTime()) > config
              .getTiempoMaxProcesoBok()) {
            LOG.trace(
                "Escalando estado INCIDENCIAS a tmct0bok. Demasiado tiempo en estado EN CURSO ## nuorden: '{}' ## nbooking: '{}' a estado: '{}'",
                nuorden, nbooking, getEstadoIncidencias());
            actualizarEstadoAlosIncidencias(nuorden, nbooking, config.getEstadoIncidencias());
            actualizarEstadoBok(nuorden, nbooking, config.getEstadoIncidencias());
          } // if
        } // if (!bincidenciabok)
      } // else
    } // if (listalos != null && !listalos.isEmpty())
  } // escalarAloBok

  /**
   * 
   * @param nuOrden
   * @param nBooking
   * @param nucnfclt
   * @param nucnfliq
   * @param nombreCampoEstado
   * @param valorUpdateCampoEstado
   * @param EntityManager em
   */
  @Transactional
  private void actualizarEstadoAlc(String nuOrden, String nBooking, String nucnfclt, BigDecimal nucnfliq,
      Integer valorUpdateCampoEstado) {

    StringBuilder sql = new StringBuilder("UPDATE TMCT0ALC ");
    sql.append(" SET ").append(getNombreCampoEstado().toUpperCase()).append(" =:pcdestado ");
    sql.append(", fhaudit = :pfhaudit ");
    sql.append(" WHERE NUORDEN =:nuOrden AND NBOOKING =:nBooking ");
    sql.append(" AND NUCNFCLT =:nucnfclt AND NUCNFLIQ = :nucnfliq");
    Map<String, Object> params = new HashMap<String, Object>();

    params.put("pcdestado", valorUpdateCampoEstado);
    params.put("nuOrden", nuOrden);
    params.put("nBooking", nBooking);
    params.put("nucnfclt", nucnfclt);
    params.put("nucnfliq", nucnfliq);
    params.put("pfhaudit", new Timestamp(System.currentTimeMillis()));

    tmct0alcDaoImp.prepareAndExecuteNativeUpdate(sql.toString(), params);
  } // actualizarEstadoAlc

  /**
   * 
   * @param nuOrden
   * @param nBooking
   * @param nucnfclt
   * @param nombreCampoEstado
   * @param valorUpdateCampoEstado
   * @param EntityManager em
   */
  @Transactional
  private void actualizarEstadoAlo(String nuOrden, String nBooking, String nucnfclt, Integer valorUpdateCampoEstado) {
    StringBuilder sql = new StringBuilder("UPDATE TMCT0ALO ");
    sql.append(" SET ").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestado, FHAUDIT = :pfhaudit ");
    sql.append(" WHERE  NUORDEN = :nuOrden AND NBOOKING = :nBooking ");
    sql.append(" AND NUCNFCLT = :nucnfclt ");
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("pcdestado", valorUpdateCampoEstado);
    params.put("nuOrden", nuOrden);
    params.put("nBooking", nBooking);
    params.put("nucnfclt", nucnfclt);
    params.put("pfhaudit", new Timestamp(System.currentTimeMillis()));

    tmct0aloDaoImp.prepareAndExecuteNativeUpdate(sql.toString(), params);
  } // actualizarEstadoAlo

  /**
   * 
   * @param nuOrden
   * @param nBooking
   * @param nombreCampoEstado
   * @param valorUpdateCampoEstado
   * @param EntityManager em
   */
  @Transactional
  private void actualizarEstadoBok(String nuOrden, String nBooking, int valorUpdateCampoEstado) {
    StringBuilder sql = new StringBuilder("UPDATE TMCT0BOK ");
    sql.append(" SET ").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestado, FHAUDIT = :pfhaudit ");
    sql.append(" WHERE NUORDEN = :nuOrden AND NBOOKING = :nBooking ");
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("pcdestado", valorUpdateCampoEstado);
    params.put("nuOrden", nuOrden);
    params.put("nBooking", nBooking);
    params.put("pfhaudit", new Timestamp(System.currentTimeMillis()));
    tmct0bokDaoImp.prepareAndExecuteNativeUpdate(sql.toString(), params);
  } // actualizarEstadoBok

  /**
   * 
   * @param nuOrden
   * @param nBooking
   * @param nucnfclt
   * @param nucnfliq
   * @param nombreCampoEstado
   * @param valorUpdateCampoEstado
   * @param notEstadoRechazado
   * @param estadoEnCurso
   * @param estadoTratandose
   * @param estadoIncidencias
   * @param EntityManager em
   */
  @Transactional
  private void actualizarEstadoDcsIncidencias(String nuOrden, String nBooking, String nucnfclt, BigDecimal nucnfliq,
      Integer valorUpdateCampoEstado) {
    StringBuilder sql = new StringBuilder("UPDATE TMCT0DESGLOSECAMARA ");
    sql.append(" SET ").append(getNombreCampoEstado().toUpperCase())
        .append(" = :pcdestado, AUDIT_FECHA_CAMBIO = :pfhaudit ");
    sql.append(" WHERE  NUORDEN = :nuOrden AND NBOOKING = :nBooking ");
    sql.append(" AND NUCNFCLT =:pnucnfclt AND NUCNFLIQ = :pnucnfliq ");
    sql.append(" AND ").append(getNombreCampoEstado().toUpperCase()).append(" <> :pnotEstadoRechazado ");
    sql.append(" AND (").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoEnCurso OR ")
        .append(getNombreCampoEstado().toUpperCase()).append(" =:pcdestadoTratandose OR ")
        .append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoIncidencias) ");
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("pcdestado", valorUpdateCampoEstado);
    params.put("pcdestadoEnCurso", getEstadoEnCurso());
    params.put("pcdestadoTratandose", getEstadoTratandose());
    params.put("pcdestadoIncidencias", getEstadoIncidencias());
    params.put("pnotEstadoRechazado", valorUpdateCampoEstado);
    params.put("nuOrden", nuOrden);
    params.put("nBooking", nBooking);
    params.put("pnucnfclt", nucnfclt);
    params.put("pnucnfliq", nucnfliq);
    params.put("pfhaudit", new Timestamp(System.currentTimeMillis()));

    tmct0desgloseCamaraDaoImpl.prepareAndExecuteNativeUpdate(sql.toString(), params);
  } // actualizarEstadoDcsIncidencias

  /**
   * 
   * @param nuOrden
   * @param nBooking
   * @param nucnfclt
   * @param nombreCampoEstado
   * @param valorUpdateCampoEstado
   * @param notEstadoRechazado
   * @param estadoEnCurso
   * @param estadoTratandose
   * @param estadoIncidencias
   * @param EntityManager em
   */
  @Transactional
  private void actualizarEstadoAlcsIncidencias(String nuOrden, String nBooking, String nucnfclt,
      Integer valorUpdateCampoEstado) {
    StringBuilder sql = new StringBuilder("UPDATE TMCT0ALC ");
    sql.append(" SET ").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestado, FHAUDIT = :pfhaudit ");
    sql.append(" WHERE  NUORDEN = :nuOrden AND NBOOKING = :nBooking ");
    sql.append(" AND NUCNFCLT = :pnucnfclt ");
    sql.append(" AND ").append(getNombreCampoEstado().toUpperCase()).append(" <> :pnotEstadoRechazado ");
    sql.append(" AND (").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoEnCurso OR ")
        .append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoTratandose OR ")
        .append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoIncidencias) ");
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("pcdestado", valorUpdateCampoEstado);
    params.put("pcdestadoEnCurso", getEstadoEnCurso());
    params.put("pcdestadoTratandose", getEstadoTratandose());
    params.put("pcdestadoIncidencias", getEstadoIncidencias());
    params.put("pnotEstadoRechazado", valorUpdateCampoEstado);
    params.put("nuOrden", nuOrden);
    params.put("nBooking", nBooking);
    params.put("pnucnfclt", nucnfclt);
    params.put("pfhaudit", new Timestamp(System.currentTimeMillis()));

    tmct0alcDaoImp.prepareAndExecuteNativeUpdate(sql.toString(), params);
  } // actualizarEstadoAlcsIncidencias

  /**
   * 
   * @param nuOrden
   * @param nBooking
   * @param nombreCampoEstado
   * @param valorUpdateCampoEstado
   * @param notEstadoRechazado
   * @param estadoEnCurso
   * @param estadoTratandose
   * @param estadoIncidencias
   * @param EntityManager em
   */
  @Transactional
  private void actualizarEstadoAlosIncidencias(String nuOrden, String nBooking, Integer valorUpdateCampoEstado) {
    StringBuilder sql = new StringBuilder("UPDATE TMCT0ALO ");
    sql.append(" SET ").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestado, FHAUDIT = :pfhaudit ");
    sql.append(" WHERE  NUORDEN = :nuOrden AND NBOOKING = :nBooking ");
    sql.append(" AND ").append(getNombreCampoEstado().toUpperCase()).append(" <> :pnotEstadoRechazado ");
    sql.append(" AND (").append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoEnCurso OR ")
        .append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoTratandose OR ")
        .append(getNombreCampoEstado().toUpperCase()).append(" = :pcdestadoIncidencias) ");
    Map<String, Object> params = new HashMap<String, Object>();
    params.put("pcdestado", valorUpdateCampoEstado);
    params.put("pcdestadoEnCurso", getEstadoEnCurso());
    params.put("pcdestadoTratandose", getEstadoTratandose());
    params.put("pcdestadoIncidencias", getEstadoIncidencias());
    params.put("pnotEstadoRechazado", valorUpdateCampoEstado);
    params.put("nuOrden", nuOrden);
    params.put("nBooking", nBooking);
    params.put("pfhaudit", new Timestamp(System.currentTimeMillis()));

    tmct0aloDaoImp.prepareAndExecuteNativeUpdate(sql.toString(), params);
  } // actualizarEstadoAlosIncidencias

  private Integer getEstadoEquivalente(Map<String, Integer> cachedValues, Collection<Integer> estados) {
    LOG.trace("getEstadoEquivalente() Estados = {}", estados);
    if (estados != null) {
      Collection<Integer> estados2 = new HashSet<Integer>(estados);
      Collection<Integer> estadosResultado = new HashSet<Integer>();
      Integer estEquivalente;
      for (Integer est1 : estados) {
        for (Integer est2 : estados2) {
          estEquivalente = estadoBo.getEstadoEquivalente(cachedValues, est1, est2);
          if (estEquivalente != null) {
            estadosResultado.add(estEquivalente);
          }
        }
      }
      estados2.clear();
      if (estadosResultado.size() == 1) {
        Integer res = estadosResultado.iterator().next();
        LOG.trace("getEstadoEquivalente() Estados = {} Estado Equivalente = {}", estados, res);
        estados.clear();
        estadosResultado.clear();
        return res;
      }
      else if (estadosResultado.size() > 1 && estadosResultado.size() < estados.size()) {
        LOG.trace("getEstadoEquivalente() Llamada Recursiva.");
        Integer res = getEstadoEquivalente(cachedValues, estadosResultado);
        estados.clear();
        estadosResultado.clear();
        return res;
      }
    }
    LOG.trace("getEstadoEquivalente() Estados = {} No hay estado equivalente.", estados);
    return null;
  }

  private String getSelectBookingsQuery() {
    StringBuilder sb = new StringBuilder("SELECT B.NUORDEN, B.NBOOKING, B.FHAUDIT FROM TMCT0BOK B ");
    sb.append(" INNER JOIN TMCT0ORD O ON B.NUORDEN = O.NUORDEN  WHERE (B.{0} = :pcdestado1 OR B.{0} = :pcdestado2 ) ")
        .append(" AND B.{0} <> :pnotcdestado1 AND B.cdestadoasig > :pnotcdestadoasig AND O.isscrdiv = :isscrdiv");
    String sqlBooking = MessageFormat.format(sb.toString().trim(), getNombreCampoEstado());
    sqlBooking = String.format("%s AND B.CDUSUBLO = '' ", sqlBooking);
    return sqlBooking;
  }

  private String getSelectAlosQuery() {
    StringBuilder sb = new StringBuilder("SELECT NUORDEN, NBOOKING, NUCNFCLT, FHAUDIT FROM TMCT0ALO ");
    sb.append(" WHERE {0} IN (:pcdestado1, :pcdestado2) AND NUORDEN = :nuorden AND NBOOKING = :nbooking         ")
        .append(" AND {0} <> :pnotcdestado1 AND cdestadoasig <> :pnotcdestadoasig ");
    String sqlAllocation = MessageFormat.format(sb.toString().trim(), getNombreCampoEstado());
    return sqlAllocation;
  }

  private String getSelectAlcsQuery() {
    StringBuilder sb = new StringBuilder("SELECT NUORDEN, NBOOKING, NUCNFCLT, NUCNFLIQ, FHAUDIT FROM TMCT0ALC ");
    sb.append(" WHERE {0} = :pcdestado1 AND NUORDEN = :nuorden AND NBOOKING = :nbooking                          ")
        .append(" AND NUCNFCLT = :nucnfclt AND {0} <> :pnotcdestado1 AND cdestadoasig <> :pnotcdestadoasig ");
    String sqlAllocationToClearer = MessageFormat.format(sb.toString().trim(), getNombreCampoEstado());
    return sqlAllocationToClearer;
  }

  private String getSelectDcsQuery() {
    StringBuilder sb = new StringBuilder("SELECT NUORDEN, NBOOKING, NUCNFCLT, NUCNFLIQ, {0} FROM TMCT0DESGLOSECAMARA ");
    sb.append(" WHERE  NUORDEN = :nuorden AND NBOOKING = :nbooking AND NUCNFCLT = :nucnfclt ")
        .append(" AND NUCNFLIQ = :nucnfliq AND {0} <> :pnotcdestado1 AND cdestadoasig <> :pnotcdestadoasig")
        .append(" GROUP BY NUORDEN, NBOOKING, NUCNFCLT, NUCNFLIQ, {0}");
    String sqlDesgloseCamara = MessageFormat.format(sb.toString().trim(), getNombreCampoEstado());
    return sqlDesgloseCamara;
  }

  private String getSelectAlcsGroupQuery() {
    StringBuilder sb = new StringBuilder(
        "SELECT NUORDEN, NBOOKING, NUCNFCLT, {0} FROM TMCT0ALC WHERE NUORDEN = :nuorden ");
    sb.append(" AND NBOOKING = :nbooking AND NUCNFCLT = :nucnfclt AND {0} <> :pnotcdestado1                   ")
        .append(" AND cdestadoasig <> :pnotcdestadoasig GROUP BY NUORDEN, NBOOKING, NUCNFCLT, {0} ");

    String sqlAllocationToClearerGrouping = MessageFormat.format(sb.toString().trim(), getNombreCampoEstado());
    return sqlAllocationToClearerGrouping;
  }

  private String getSelectAlosGroupQuery() {
    StringBuilder sb = new StringBuilder("SELECT NUORDEN, NBOOKING, {0} FROM TMCT0ALO ");
    sb.append(" WHERE NUORDEN = :nuorden AND NBOOKING = :nbooking AND {0} <> :pnotcdestado1                          ")
        .append(" AND cdestadoasig <> :pnotcdestadoasig GROUP BY NUORDEN, NBOOKING, {0} ");
    String sqlAllocationGrouping = MessageFormat.format(sb.toString().trim(), getNombreCampoEstado());
    return sqlAllocationGrouping;
  }

} // EscalaEstados
