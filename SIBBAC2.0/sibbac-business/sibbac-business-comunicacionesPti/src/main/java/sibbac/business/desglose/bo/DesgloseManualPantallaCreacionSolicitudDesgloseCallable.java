package sibbac.business.desglose.bo;

import java.util.concurrent.Callable;

import sibbac.business.desglose.commons.AlgoritmoDesgloseManual;
import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.fase0.database.model.Tmct0aloId;

class DesgloseManualPantallaCreacionSolicitudDesgloseCallable implements Callable<SolicitudDesgloseManual> {

  private Tmct0aloId aloId;
  private DesgloseMultithread desglose;
  private AlgoritmoDesgloseManual algoritmo;
  private DesgloseConfigDTO config;

  DesgloseManualPantallaCreacionSolicitudDesgloseCallable(Tmct0aloId aloId, AlgoritmoDesgloseManual algoritmo,
      DesgloseMultithread desglose, DesgloseConfigDTO config) {
    this.desglose = desglose;
    this.aloId = aloId;
    this.algoritmo = algoritmo;
    this.config = config;
  }

  @Override
  public SolicitudDesgloseManual call() throws Exception {
    return desglose.crearSolicitudDesgloseManual(aloId, algoritmo, config);
  }

}
