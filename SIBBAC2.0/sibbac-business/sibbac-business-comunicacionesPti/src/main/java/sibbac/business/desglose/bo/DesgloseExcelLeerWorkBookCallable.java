package sibbac.business.desglose.bo;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.poi.ss.usermodel.Sheet;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

class DesgloseExcelLeerWorkBookCallable implements Callable<CallableResultDTO> {

  private List<Integer> listPosAlos;
  private int posicionAllocationEnExcel;
  private Sheet sheet;
  private List<SolicitudDesgloseManual> listSolicitudDesgloseManual;
  private DesgloseConfigDTO config;
  private DesgloseExcelBo desgloseExcelBo;

  DesgloseExcelLeerWorkBookCallable(List<Integer> listPosAlos, int posicionAllocationEnExcel, Sheet sheet,
      List<SolicitudDesgloseManual> listSolicitudDesgloseManual, DesgloseConfigDTO config,
      DesgloseExcelBo desgloseExcelBo) {
    this.listPosAlos = listPosAlos;
    this.posicionAllocationEnExcel = posicionAllocationEnExcel;
    this.sheet = sheet;
    this.listSolicitudDesgloseManual = listSolicitudDesgloseManual;
    this.config = config;
    this.desgloseExcelBo = desgloseExcelBo;

  }

  @Override
  public CallableResultDTO call() throws SIBBACBusinessException {
    return this.desgloseExcelBo.leerDatosAllo(listPosAlos, posicionAllocationEnExcel, sheet,
        listSolicitudDesgloseManual, config);
  }

}
