package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBroker;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.StatusXML_TR;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

@Service
public class XmlTmct0ctg {

  @Autowired
  private XmlOrdenHelper xmlOrdenHelper;

  @Autowired
  private SettlementDataBo settlementDataBo;

  public XmlTmct0ctg() {
    super();
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void accumulateNew(Tmct0gal tmct0gal, Tmct0alo tmct0alo, BigDecimal imcbmerc, Tmct0ctg tmct0ctg)
      throws XmlOrdenExceptionReception {

    tmct0ctg.getId().setNbooking(tmct0gal.getId().getNbooking());
    /*
     * campos que acumulan en el tmct0cto
     */
    tmct0ctg.setImcammed(tmct0alo.getImcammed().setScale(6, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcamnet(tmct0alo.getImcamnet().setScale(6, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImtotbru(tmct0gal.getImbrmerc().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImtotnet(tmct0gal.getImtotnet().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setEutotbru(tmct0gal.getImbrmreu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setEutotnet(tmct0gal.getEutotnet().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImnetbrk(new BigDecimal(0));
    tmct0ctg.setEunetbrk(new BigDecimal(0));
    tmct0ctg.setImcomisn(tmct0gal.getImcomisn().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcomieu(tmct0gal.getImcomieu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcomdev(tmct0gal.getImcomdev().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setEucomdev(tmct0gal.getEucomdev().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImfibrdv(tmct0gal.getImfibrdv().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImfibreu(tmct0gal.getImfibreu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImsvb(tmct0gal.getImsvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImsvbeu(tmct0gal.getImsvbeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgastos(tmct0gal.getImgastosb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgatdvs(tmct0gal.getImgatdvsb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImimpdvs(tmct0gal.getImimpdvsb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImimpeur(tmct0gal.getImimpeurb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcconeu(tmct0gal.getImcconeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcliqeu(tmct0gal.getImcliqeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImdereeu(tmct0gal.getImdereeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setNutitagr(tmct0gal.getNutitagr().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0ctg.setNutitpnr(tmct0gal.getNutitagr().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImntcleu(new BigDecimal(0));
    tmct0ctg.setImnetocl(new BigDecimal(0));
    // Cambiar por el medio de grupos de ejecución.

    // tmct0ctg.setImcbmerc(imcbmerc);
    // tmct0ctg.setImcbmerc(tmct0gal.getTmct0gre().getTmct0bok().getImcbmerc());
    tmct0ctg.setImtpcamb(tmct0gal.getTmct0gre().getTmct0bok().getImtpcamb().setScale(8, BigDecimal.ROUND_HALF_UP));
    // tmct0ctg.setImcbmerc(JdoAccumulators.getMarketAveragePrice(tmct0gal
    // .getTmct0gre().getTmct0bok().getTmct0ord()
    // .getNuorden(), tmct0gal.getTmct0gre().getTmct0bok()
    // .getNbooking(), tmct0gal.getTmct0gre().getCdbroker(),
    // tmct0gal.getTmct0gre().getCdmercad(), hpm));

    tmct0ctg.setNbvalors(tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getNbvalors());
    tmct0ctg.setPodevolu(tmct0gal.getTmct0gre().getTmct0bok().getPodevolu().setScale(4, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setPccombrk(tmct0gal.getPccombrk());
    tmct0ctg.setCdstcexc("");
    tmct0ctg.setImmarpri(new BigDecimal(0));
    tmct0ctg.setCdenvcor('N');
    tmct0ctg.setCddatliq('S');
    tmct0ctg.setCdbloque('N');
    tmct0ctg.setRfparten("");
    tmct0ctg.setPkbroker1(0);
    tmct0ctg.setPkentity("");
    tmct0ctg.setCdalias(tmct0gal.getTmct0gre().getTmct0bok().getCdalias());

    /*
     * Calculo gross market en divisa y eur numero de titulos por precio medio.
     */
    // TODO: Cambiar el calculo del precio medio. Acumular el gros
    // (imbrmerc) y dividirle entre el numero de titulos acumulado
    // (nuntitagr) en cada carga.
    // tmct0ctg.setImbrmerc(tmct0ctg.getNutitagr().multiply(
    // tmct0ctg.getImcbmerc()).setScale(
    // Tools_currency
    // .getCurrencyPrecision(tmct0alo.getCdmoniso(), hpm),
    // RoundingMode.HALF_UP));
    // tmct0ctg.setImbrmreu(tmct0ctg.getImbrmerc().multiply(
    // tmct0ctg.getImtpcamb()).setScale(
    // Tools_currency.getEURCurrencyPrecision(hpm),
    // RoundingMode.HALF_UP));
    if (tmct0gal.getImbrmercb().equals(BigDecimal.ZERO)) {
      tmct0ctg.setImbrmerc(tmct0gal.getImbrmerc().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0ctg.setImbrmreu(tmct0gal.getImbrmreu().setScale(8, BigDecimal.ROUND_HALF_UP));
    }
    else {
      tmct0ctg.setImbrmerc(tmct0gal.getImbrmercb().setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0ctg.setImbrmreu(tmct0gal.getImbrmreub().setScale(8, BigDecimal.ROUND_HALF_UP));
    }
    tmct0ctg.setImcbmerc(tmct0ctg.getImbrmerc().divide(tmct0ctg.getNutitagr(), 8, RoundingMode.HALF_UP));

    tmct0ctg.setCdtpoper(tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdtpoper());
    tmct0ctg.setCdmoniso(tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdmoniso());

    // Gastos
    tmct0ctg.setImgasliqdv(tmct0gal.getImgasliqdvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgasliqeu(tmct0gal.getImgasliqeub().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0ctg.setImgasgesdv(tmct0gal.getImgasgesdvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgasgeseu(tmct0gal.getImgasgeseub().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0ctg.setImgastotdv(tmct0gal.getImgastotdvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgastoteu(tmct0gal.getImgastoteub().setScale(8, BigDecimal.ROUND_HALF_UP));

    // indicador de gastos
    tmct0ctg.setCdindgas(tmct0gal.getCdindgasb());

    /*
     * Calculo del Net Center Consideration
     */
    // if ("C"
    // .equalsIgnoreCase(tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdtpoper()
    // .trim())) {
    // tmct0ctg
    // .setImntbreu(tmct0ctg.getImbrmreu().add(tmct0ctg.getImfibreu()).add(tmct0ctg.getImgastos()).add(
    // tmct0ctg.getImimpeur()).setScale(Tools_currency.getEURCurrencyPrecision(hpm),
    // RoundingMode.HALF_UP));
    // } else {
    // tmct0ctg.setImntbreu(tmct0ctg.getImbrmreu().subtract(tmct0ctg.getImfibreu()).subtract(
    // tmct0ctg.getImgastos()).subtract(tmct0ctg.getImimpeur()).setScale(
    // Tools_currency.getEURCurrencyPrecision(hpm), RoundingMode.HALF_UP));
    // }
    /*
     * Carga descrali Tabla Fmeg0ali clave primaria cdalias. PARA EXTRANJERO
     */
    // if
    // (Tools_operation.isForeingMarket(tmct0alo.getTmct0bok().getTmct0ord().getCdclsmdo()))
    // {

    tmct0ctg.setDescrali(tmct0alo.getTmct0bok().getDescrali());
    // } else {
    // /*
    // * Carga descrali. PARA NACIONAL se carga de Booking
    // */
    // tmct0ctg.setDescrali(tmct0gal.getTmct0gre().getTmct0bok().getDescrali().trim());
    // }
    tmct0ctg.setCdisin(tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdisin());

    tmct0ctg.setFetrade(tmct0gal.getTmct0gre().getTmct0bok().getFeejecuc());
    tmct0ctg.setFeconsol(tmct0gal.getTmct0gre().getTmct0bok().getFeconsol());
    tmct0ctg.setFealta(tmct0gal.getTmct0gre().getTmct0bok().getFealta());
    tmct0ctg.setHoalta(tmct0gal.getTmct0gre().getTmct0bok().getHoalta());
    tmct0ctg.setFecontra(tmct0gal.getTmct0gre().getTmct0bok().getFecontra());
    tmct0ctg.setFeejecuc(tmct0gal.getTmct0gre().getTmct0bok().getFeejecuc());
    tmct0ctg.setHoejecuc(tmct0gal.getTmct0gre().getTmct0bok().getHoejecuc());
    tmct0ctg.setFevalide(tmct0gal.getTmct0gre().getTmct0bok().getFevalide());
    tmct0ctg.setFevalor(tmct0gal.getTmct0gre().getTmct0bok().getFevalor());
    tmct0ctg.setFevalorr(tmct0gal.getTmct0gre().getTmct0bok().getFevalorr());

    /*
     * Campos del broker
     */

    // Si es MTF el broker que se pasa para obtener las instrucciones de
    // liquidaci�n ser� el central counterparty
    // asociado
    String cdbroker = tmct0gal.getTmct0gre().getCdbroker();
    BigDecimal fhconsol;
    try {
      fhconsol = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(tmct0ctg.getFeconsol()));
    }
    catch (SIBBACBusinessException e) {
      throw new XmlOrdenExceptionReception(tmct0ctg.getId().getNuorden(), tmct0ctg.getId().getNbooking(), "", "",
          e.getMessage(), e);
    }
    SettlementChainBroker settlementChainBroker = settlementDataBo.getSettlementChainBrokerInternacional(cdbroker,
        tmct0gal.getTmct0gre().getCdmercad(), tmct0alo.getTpsetcle(), fhconsol);

    // local custodian
    tmct0ctg.setBcdloccus(settlementChainBroker.getBcdloccus());
    tmct0ctg.setBlcusbic(settlementChainBroker.getBlcusbic());
    tmct0ctg.setBacloccus(settlementChainBroker.getBacloccus());
    tmct0ctg.setBidloccus(settlementChainBroker.getBidloccus());

    // global custodian
    tmct0ctg.setBcdglocus(settlementChainBroker.getBcdglocus());
    tmct0ctg.setBgcusbic(settlementChainBroker.getBgcusbic());
    tmct0ctg.setBacglocus(settlementChainBroker.getBacglocus());
    tmct0ctg.setBidglocus(settlementChainBroker.getBidglocus());

    // beneficiario
    tmct0ctg.setBcdbenefi(settlementChainBroker.getBcdbenefi());
    tmct0ctg.setBcdbenbic(settlementChainBroker.getBcdbenbic());
    tmct0ctg.setBacbenefi(settlementChainBroker.getBacbenefi());
    tmct0ctg.setBidbenefi(settlementChainBroker.getBidbenefi());

    tmct0ctg.setCdconcil(' ');
    tmct0ctg.setIndatfis(tmct0alo.getTmct0bok().getIndatfis());
    tmct0ctg.setErdatfis(tmct0alo.getTmct0bok().getErdatfis());
    tmct0ctg.setCdusublo("");
    tmct0ctg.setDsobserv("");
    tmct0ctg.setCdauxili("");
    tmct0ctg.setInbokmul('N');
    tmct0ctg.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0ctg.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);

    // tmct0ctg.setFk_tmct0sta_11(StatusMulticenter.getStateTableJDO(StatusMulticenter.TMCT0CTG,
    // StatusMulticenter.STATE_INPUT_SYSTEM, hpm));

    Tmct0sta tmct0sta = new Tmct0sta(new Tmct0staId(TypeCdestado.NEW_REGISTER.getValue(),
        TypeCdtipest.TMCT0CTG.getValue()));

    tmct0ctg.setTmct0sta(tmct0sta);

    // Carga id Tr
    tmct0ctg.setIdtr("");
    tmct0ctg.setStatr(StatusXML_TR.XML_NO_SEND.getStatus());

    tmct0ctg.setImnetobr(xmlOrdenHelper.calculateGrossCenter(tmct0ctg));
    tmct0ctg.setImntbreu(xmlOrdenHelper.calculateGrossCenterEur(tmct0ctg));
    tmct0ctg.setCdmtfnet(' ');
    tmct0ctg.setNumerord("");

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void accumulateAdd(Tmct0gal tmct0gal, Tmct0alo tmct0alo, Tmct0ctg tmct0ctg) throws XmlOrdenExceptionReception {

    tmct0ctg.setImtotbru(tmct0ctg.getImbrmerc().add(tmct0gal.getImbrmerc()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImtotnet(tmct0ctg.getImtotnet().add(tmct0gal.getImtotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setEutotbru(tmct0ctg.getImbrmreu().add(tmct0gal.getImbrmreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setEutotnet(tmct0ctg.getEutotnet().add(tmct0gal.getEutotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImnetbrk(new BigDecimal(0));
    tmct0ctg.setEunetbrk(new BigDecimal(0));
    tmct0ctg.setImcomisn(tmct0ctg.getImcomisn().add(tmct0gal.getImcomisn()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcomieu(tmct0ctg.getImcomieu().add(tmct0gal.getImcomieu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcomdev(tmct0ctg.getImcomdev().add(tmct0gal.getImcomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setEucomdev(tmct0ctg.getEucomdev().add(tmct0gal.getEucomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImfibrdv(tmct0ctg.getImfibrdv().add(tmct0gal.getImfibrdv()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImfibreu(tmct0ctg.getImfibreu().add(tmct0gal.getImfibreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImsvb(tmct0ctg.getImsvb().add(tmct0gal.getImsvb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImsvbeu(tmct0ctg.getImsvbeu().add(tmct0gal.getImsvbeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgastos(tmct0ctg.getImgastos().add(tmct0gal.getImgastosb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgatdvs(tmct0ctg.getImgatdvs().add(tmct0gal.getImgatdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImimpdvs(tmct0ctg.getImimpdvs().add(tmct0gal.getImimpdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImimpeur(tmct0ctg.getImimpeur().add(tmct0gal.getImimpeurb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcconeu(tmct0ctg.getImcconeu().add(tmct0gal.getImcconeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImcliqeu(tmct0ctg.getImcliqeu().add(tmct0gal.getImcliqeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImdereeu(tmct0ctg.getImdereeu().add(tmct0gal.getImdereeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setNutitagr(tmct0ctg.getNutitagr().add(tmct0gal.getNutitagr()).setScale(0, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setNutitpnr(tmct0ctg.getNutitagr().setScale(0, BigDecimal.ROUND_HALF_UP));

    /*
     * Calculo gross market en divisa y eur numero de titulos por precio medio.
     */
    // tmct0ctg.setImbrmerc(tmct0ctg.getNutitagr().multiply(
    // tmct0ctg.getImcbmerc()).setScale(
    // Tools_currency
    // .getCurrencyPrecision(tmct0alo.getCdmoniso(), hpm),
    // RoundingMode.HALF_UP));
    // tmct0ctg.setImbrmreu(tmct0ctg.getImbrmerc().multiply(
    // tmct0ctg.getImtpcamb()).setScale(
    // Tools_currency.getEURCurrencyPrecision(hpm),
    // RoundingMode.HALF_UP));
    // CAmbio para calcular el precio medio en base al gross / titulos
    if (tmct0gal.getImbrmercb().equals(BigDecimal.ZERO)) {
      tmct0ctg.setImbrmerc(tmct0ctg.getImbrmerc().add(tmct0gal.getImbrmerc()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0ctg.setImbrmreu(tmct0ctg.getImbrmreu().add(tmct0gal.getImbrmreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    }
    else {
      tmct0ctg.setImbrmerc(tmct0ctg.getImbrmerc().add(tmct0gal.getImbrmercb()).setScale(8, BigDecimal.ROUND_HALF_UP));
      tmct0ctg.setImbrmreu(tmct0ctg.getImbrmreu().add(tmct0gal.getImbrmreub()).setScale(8, BigDecimal.ROUND_HALF_UP));
    }

    tmct0ctg.setImcbmerc(tmct0ctg.getImbrmerc().divide(tmct0ctg.getNutitagr(), 8, RoundingMode.HALF_UP));

    /*
     * Calculo del Net Center Consideration
     */

    tmct0ctg.setFhaudit(new Timestamp(System.currentTimeMillis()));

    tmct0ctg.setNumerord("");

    tmct0ctg.setImgasliqdv(tmct0ctg.getImgasliqdv().add(tmct0gal.getImgasliqdvb())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgasliqeu(tmct0ctg.getImgasliqeu().add(tmct0gal.getImgasliqeub())
        .setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0ctg.setImgasgesdv(tmct0ctg.getImgasgesdv().add(tmct0gal.getImgasgesdvb())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgasgeseu(tmct0ctg.getImgasgeseu().add(tmct0gal.getImgasgeseub())
        .setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0ctg.setImgastotdv(tmct0ctg.getImgastotdv().add(tmct0gal.getImgastotdvb())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImgastoteu(tmct0ctg.getImgastoteu().add(tmct0gal.getImgastoteub())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0ctg.setImntbreu(xmlOrdenHelper.calculateGrossCenterEur(tmct0ctg));
    tmct0ctg.setImnetobr(xmlOrdenHelper.calculateGrossCenter(tmct0ctg));
  }

}
