package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpClearedGrossTradesSpainFields implements WrapperFileReaderFieldEnumInterface {
  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  SUBACCOUNT_NUMBER("subaccountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  OPPOSITE_PARTY_CODE("oppositePartyCode", 6, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  EXCHANGE_CODE_TRADE("exchangeCodeTrade", 4, 0, WrapperFileReaderFieldType.STRING),
  SYMBOL("symbol", 6, 0, WrapperFileReaderFieldType.STRING),
  TYPE("type", 1, 0, WrapperFileReaderFieldType.CHAR),
  EXPIRATION_DATE("expirationDate", 8, 0, WrapperFileReaderFieldType.DATE),
  EXERCISE_PRICE("exercisePrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  EXTERNAL_MEMBER("externalMember", 10, 0, WrapperFileReaderFieldType.STRING),
  EXTERNAL_ACCOUNT("externalAccount", 15, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  MOVEMENT_CODE("movementCode", 2, 0, WrapperFileReaderFieldType.STRING),
  BUY_SELL_CODE("buySellCode", 1, 0, WrapperFileReaderFieldType.CHAR),
  QUANTITY_LONG_SIGN("quantityLongSign", 1, 0, WrapperFileReaderFieldType.CHAR),
  PROCESSED_QUANTITY_LONG("processedQuantityLong", 12, 2, WrapperFileReaderFieldType.NUMERIC),
  QUANTITY_SHORT_SIGN("quantityShortSign", 1, 0, WrapperFileReaderFieldType.CHAR),
  PROCESSED_QUANTITY_SHORT("processedQuantityShort", 12, 2, WrapperFileReaderFieldType.NUMERIC),
  CLEARING_FEE("clearingFee", 12, 4, WrapperFileReaderFieldType.NUMERIC),
  CLEARING_FEE_DC("clearingFeeDc", 1, 0, WrapperFileReaderFieldType.CHAR),
  CLEARING_FEE_CUR_CODE("clearingFeeCurCode", 3, 0, WrapperFileReaderFieldType.STRING),
  COUNTER_VALUE("counterValue", 18, 4, WrapperFileReaderFieldType.NUMERIC),
  COUNTER_VALUE_DC("counterValueDc", 1, 0, WrapperFileReaderFieldType.CHAR),
  COUNTER_VALUE_CUR_CODE("counterValueCurCode", 3, 0, WrapperFileReaderFieldType.STRING),
  COUPON_INTEREST("couponInterest", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  COUPON_INTEREST_DC("couponInterestDc", 1, 0, WrapperFileReaderFieldType.CHAR),
  EFFECTIVE_VALUE("effectiveValue", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  EFFECTIVE_VALUE_DC("effectiveValueDc", 1, 0, WrapperFileReaderFieldType.CHAR),
  TRANSACTION_PRICE("transactionPrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  TRANSACTION_DATE("transactionDate", 8, 0, WrapperFileReaderFieldType.DATE),
  SETTLEMENT_DATE("settlementDate", 8, 0, WrapperFileReaderFieldType.DATE),
  UNSETTLED_REFERENCE("unsettledReference", 9, 0, WrapperFileReaderFieldType.STRING),
  EXTERNAL_TRANSACTION_ID_EXCHANGE("externalTransactionIdExchange", 20, 0, WrapperFileReaderFieldType.STRING),
  SETTLEMENT_INSTRUCTION_REFERENCE("settlementInstructionReference", 9, 0, WrapperFileReaderFieldType.STRING),
  ORDER_NUMBER("orderNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ISIN_CODE("isinCode", 12, 0, WrapperFileReaderFieldType.STRING),
  TRADER_INITIALS("traderInitials", 6, 0, WrapperFileReaderFieldType.STRING),
  ULV_TRADING_UNIT("ulvTradingUnit", 11, 4, WrapperFileReaderFieldType.NUMERIC),
  TRANSACTION_ORIGIN("transactionOrigin", 4, 0, WrapperFileReaderFieldType.STRING),
  EXEC_TRADING_ID("execTradingId", 6, 0, WrapperFileReaderFieldType.STRING),
  DEPOT_ID("depotId", 6, 0, WrapperFileReaderFieldType.STRING),
  SAFE_KEEPING_ID("safeKeepingId", 2, 0, WrapperFileReaderFieldType.STRING),
  COMMENT("comment", 21, 0, WrapperFileReaderFieldType.STRING),
  TIMESTAMP("timestamp", 6, 0, WrapperFileReaderFieldType.STRING),
  TRANSACTION_TYPE_CODE("transactionTypeCode", 3, 0, WrapperFileReaderFieldType.STRING),
  EXTERNAL_POSITION_ACCOUNT_ID("externalPositionAccountId", 30, 0, WrapperFileReaderFieldType.STRING),
  CLEARING_ACCOUNT("clearingAccount", 8, 0, WrapperFileReaderFieldType.STRING),
  SPANISH_CSD_ACCOUNT_TYPE("spanishCsdAccountType", 1, 0, WrapperFileReaderFieldType.CHAR),
  OWNER_REFERENCE("ownerReference", 20, 0, WrapperFileReaderFieldType.STRING),
  HOLD_OR_RELEASE_STATUS("holdOrReleaseStatus", 1, 0, WrapperFileReaderFieldType.CHAR),
  FILLER("filler", 66, 0, WrapperFileReaderFieldType.STRING);

  private final String name;
  private final int length;
  private final int scale;
  private final WrapperFileReaderFieldType fieldType;
  
  private EuroCcpClearedGrossTradesSpainFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
