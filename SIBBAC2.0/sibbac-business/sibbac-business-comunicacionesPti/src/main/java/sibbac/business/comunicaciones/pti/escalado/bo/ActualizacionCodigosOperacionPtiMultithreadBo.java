package sibbac.business.comunicaciones.pti.escalado.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.dto.ActualizacionCodigosOperacionEccDTO;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class ActualizacionCodigosOperacionPtiMultithreadBo {

  private static final Logger LOG = LoggerFactory.getLogger(ActualizacionCodigosOperacionPtiMultithreadBo.class);

  @Value("${sibbac.nacional.actualizacion.codigos.op.threadpool.size:10}")
  private int threadSize;

  @Value("${sibbac.nacional.actualizacion.codigos.op.estados.revisar:28}")
  private String estadosRevisar;

  @Autowired
  private ActualizacionCodigosOperacionPtiWithUserLockBo actualizacionCodigosOperacionBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0estadoBo estadoBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  public ActualizacionCodigosOperacionPtiMultithreadBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED, readOnly = true)
  public void actualizarCodigosOperacionPtiMultithread(boolean isScripDividend, String auditUser)
      throws SIBBACBusinessException {

    LOG.trace("[actualizarCodigosOperacionPtiMultithread] inicio");
    Integer estado;
    Tmct0estado estadoHb;

    if (estadosRevisar != null && !estadosRevisar.trim().isEmpty()) {
      String estados[] = estadosRevisar.split(",");
      if (estados != null && estados.length > 0) {
        for (String estadoSt : estados) {
          if (estadoSt != null && !estadoSt.trim().isEmpty()) {
            try {
              estado = Integer.parseInt(estadoSt.trim());
            }
            catch (NumberFormatException e1) {
              LOG.warn(e1.getMessage(), e1);
              continue;
            }
            estadoHb = estadoBo.findById(estado);
            if (estadoHb == null) {
              LOG.warn(
                  "[actualizarCodigosOperacionPtiMultithread]INCIDENCIA-Estado: {} no encontrado en el maestro de estados.",
                  estado);
            }
            else {
              LOG.info("[actualizarCodigosOperacionPtiMultithread] Actualizando codigos de op para estado: {}-{}",
                  estadoHb.getIdestado(), estadoHb.getNombre());
              try {
                this.actualizarCodigosOperacionPtiMultithread(estado, isScripDividend, auditUser);
              }
              catch (Exception e) {
                throw new SIBBACBusinessException(String.format(
                    "INCIDENCIA-Actualizando codigos de op para estado: %s-%s. %s", estadoHb.getIdestado(),
                    estadoHb.getNombre(), e.getMessage()), e);
              }
            }
          }
        }

      }
    }

    LOG.trace("[actualizarCodigosOperacionPtiMultithread] fin");

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED, readOnly = true)
  public void actualizarCodigosOperacionPtiMultithread(int estadoAsignacion, boolean isScriptDividend, String auditUser)
      throws SIBBACBusinessException {
    LOG.trace("[actualizarCodigosOperacionPtiMultithread] inicio");

    char isSd = 'N';
    if (isScriptDividend) {
      isSd = 'S';
    }

    List<ActualizacionCodigosOperacionEccDTO> listFechas = dcBo
        .findAllDatesCdisinCdsentidoByForActualizacionCodigosOperacionEcc(estadoAsignacion, isSd);
    if (CollectionUtils.isNotEmpty(listFechas)) {

      this.leerDatos(listFechas, isScriptDividend);

      this.procesarDatos(listFechas, isScriptDividend, auditUser);

    }

    LOG.trace("[actualizarCodigosOperacionPtiMultithread] fin");
  }

  private void leerDatos(List<ActualizacionCodigosOperacionEccDTO> listFechas, boolean isScriptDividend)
      throws SIBBACBusinessException {

    LOG.trace("[leerDatos] inicio");

    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    if (isScriptDividend) {
      tfb.setNameFormat("Thread-LeerDatos-SD-ActualizarCodigosOperacion-%d");
    }
    else {
      tfb.setNameFormat("Thread-LeerDatos-ActualizarCodigosOperacion-%d");
    }
    // leer datos
    ExecutorService executor = Executors.newFixedThreadPool(threadSize, tfb.build());

    try {
      for (final ActualizacionCodigosOperacionEccDTO fechas : listFechas) {
        Runnable p = new Runnable() {

          @Override
          public void run() {
            dcBo.findAllBookingIdByDesglosesSinTitulosDiponiblesAnotacion(fechas, fechas.getEstadoAsignacion());

          }
        };
        executor.execute(p);
        // meter aqui el hilo

      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
      executor.shutdownNow();
      throw new SIBBACBusinessException("Incidencia lanzando procesos para recuperar los datos a procesar.", e);
    }
    finally {
      if (!executor.isShutdown()) {
        executor.shutdown();
      }
      try {
        executor.awaitTermination((long) 10 * listFechas.size(), TimeUnit.SECONDS);
      }
      catch (InterruptedException e) {
        throw new SIBBACBusinessException("Incidencia esperando a que los procesos termienen.", e);
      }
    }
    LOG.trace("[leerDatos] final");
  }

  private void procesarDatos(List<ActualizacionCodigosOperacionEccDTO> listFechas, boolean isScriptDividend,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[procesarDatos] inicio");
    // procesar
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    if (isScriptDividend) {
      tfb.setNameFormat("Thread-ProcesarDatos-SD-ActualizarCodigosOperacion-%d");
    }
    else {
      tfb.setNameFormat("Thread-ProcesarDatos-ActualizarCodigosOperacion-%d");
    }
    // leer datos
    ExecutorService executor = Executors.newFixedThreadPool(threadSize, tfb.build());
    List<Future<CallableResultDTO>> listFutures = new ArrayList<Future<CallableResultDTO>>();
    try {
      for (ActualizacionCodigosOperacionEccDTO fechas : listFechas) {
        if (CollectionUtils.isNotEmpty(fechas.getBookings())) {
          listFutures.add(executor.submit(getNewCallableActualizacionCodigosOperacion(fechas, auditUser)));

        }
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
      executor.shutdownNow();
      throw new SIBBACBusinessException("Incidencia lanzando procesos para actualizar codigos de operacion.");
    }
    finally {
      if (!executor.isShutdown()) {
        executor.shutdown();
      }
      try {
        executor.awaitTermination(1 * listFechas.size(), TimeUnit.MINUTES);
      }
      catch (InterruptedException e) {
        throw new SIBBACBusinessException("Incidencia esperando a que los procesos termienen.");
      }
    }

    try {
      for (Future<CallableResultDTO> future : listFutures) {
        try {
          future.get(1, TimeUnit.SECONDS);
        }
        catch (TimeoutException e) {
          LOG.warn(e.getMessage(), e);
          future.cancel(true);
        }
        catch (InterruptedException | ExecutionException e) {
          LOG.warn(e.getMessage(), e);
          future.cancel(true);
        }
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }
    finally {
      listFutures.clear();
    }
    LOG.trace("[procesarDatos] final");
  }

  private Callable<CallableResultDTO> getNewCallableActualizacionCodigosOperacion(
      final ActualizacionCodigosOperacionEccDTO datos, final String auditUser) {
    Callable<CallableResultDTO> c = new Callable<CallableResultDTO>() {

      @Override
      public CallableResultDTO call() throws Exception {
        return actualizacionCodigosOperacionBo.actualizarCodigosOperacionOrdenesWituserLock(datos.getBookings(),
            auditUser);

      }
    };
    return c;
  }
}
