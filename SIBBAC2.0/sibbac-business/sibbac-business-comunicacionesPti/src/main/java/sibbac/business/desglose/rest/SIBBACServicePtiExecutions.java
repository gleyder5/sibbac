package sibbac.business.desglose.rest;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.comunicaciones.rest.SIBBACServiceTraspasosEcc;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.operaciones.query.PtiExecutionPageQuery;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.StreamResult;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.FilterInfo;
import sibbac.database.bo.QueryBo;

@RestController
@RequestMapping("/operaciones/ptiexecutions")
public class SIBBACServicePtiExecutions {

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceTraspasosEcc.class);

  private static final String JSON_UTF8 = "application/json;charset=UTF-8";

  @Autowired
  private Tmct0ValoresBo valoresBo;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private QueryBo queryBo;

  @RequestMapping(value = "/filters", produces = JSON_UTF8, method = RequestMethod.GET)
  public List<FilterInfo> getFilters() throws Exception {
    final AbstractDynamicPageQuery query = new PtiExecutionPageQuery();
    return query.getFiltersInfo();
  }

  @RequestMapping(value = "/columns", produces = JSON_UTF8, method = RequestMethod.GET)
  public List<DynamicColumn> getColumns() throws Exception {
    final AbstractDynamicPageQuery query = new PtiExecutionPageQuery();
    return query.getColumns();
  }

  @RequestMapping(method = RequestMethod.GET)
  public void executeQuery(HttpServletRequest request, HttpServletResponse servletResponse) throws Exception {
    final AbstractDynamicPageQuery adpq = new PtiExecutionPageQuery();
    final StreamResult streamResult;

    LOG.debug("[executeQuery] Inicio...");
    if (request.getParameterMap().get("nutitpendcase") != null
        && request.getParameterMap().get("nutitpendcase")[0] != null
        && !request.getParameterMap().get("nutitpendcase")[0].trim().isEmpty()) {
      ((PtiExecutionPageQuery) adpq).setFiltroRecibidosPdteRecibir(request.getParameterMap().get("nutitpendcase")[0]
          .trim().charAt(0));
    }
    adpq.fillDynamicQuery(request.getParameterMap());
    adpq.setIsoFormat(true);
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(adpq, FormatStyle.JSON_OBJECTS, streamResult);
    LOG.debug("[executeQuery] Fin");
  }

  private String getSingleFilter(Map<String, String[]> params) {
    final String[] values;

    if (params.containsKey("singleFilter")) {
      values = params.get("singleFilter");
      if (values.length > 0) {
        return values[0].toUpperCase();
      }
    }
    return null;
  }

  @RequestMapping(value = "/excel", method = RequestMethod.GET)
  public void executeExcelQuery(HttpServletRequest request, HttpServletResponse servletResponse) throws Exception {
    final StreamResult streamResult;
    final AbstractDynamicPageQuery query = new PtiExecutionPageQuery();
    if (request.getParameterMap().get("nutitpendcase") != null
        && request.getParameterMap().get("nutitpendcase")[0] != null
        && !request.getParameterMap().get("nutitpendcase")[0].trim().isEmpty()) {
      ((PtiExecutionPageQuery) query).setFiltroRecibidosPdteRecibir(request.getParameterMap().get("nutitpendcase")[0]
          .trim().charAt(0));
    }
    query.fillDynamicQuery(request.getParameterMap());
    query.setSingleFilter(getSingleFilter(request.getParameterMap()));
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(query, FormatStyle.EXCEL, streamResult);
  }

  @RequestMapping(value = "/csv", method = RequestMethod.GET)
  public void executeCSVQuery(HttpServletRequest request, HttpServletResponse servletResponse) throws Exception {
    final StreamResult streamResult;
    final AbstractDynamicPageQuery query = new PtiExecutionPageQuery();
    if (request.getParameterMap().get("nutitpendcase") != null
        && request.getParameterMap().get("nutitpendcase")[0] != null
        && !request.getParameterMap().get("nutitpendcase")[0].trim().isEmpty()) {
      ((PtiExecutionPageQuery) query).setFiltroRecibidosPdteRecibir(request.getParameterMap().get("nutitpendcase")[0]
          .trim().charAt(0));
    }
    query.fillDynamicQuery(request.getParameterMap());
    query.setSingleFilter(getSingleFilter(request.getParameterMap()));
    streamResult = new HttpStreamResultHelper(servletResponse);
    queryBo.executeQuery(query, FormatStyle.CSV, streamResult);
  }

  @RequestMapping(value = "/autocompleter/isin", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<LabelValueObject> isinList() {
    return valoresBo.labelValueList();
  }

  @RequestMapping(value = "/autocompleter/alias", method = RequestMethod.GET, produces = JSON_UTF8)
  public List<LabelValueObject> aliasList() {
    return aliBo.labelValueList();
  }

}
