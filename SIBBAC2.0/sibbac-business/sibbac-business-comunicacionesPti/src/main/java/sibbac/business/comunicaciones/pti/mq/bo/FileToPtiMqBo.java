package sibbac.business.comunicaciones.pti.mq.bo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.jms.JmsMessagesPtiBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Service
public class FileToPtiMqBo {

  private static final Logger LOG = LoggerFactory.getLogger(FileToPtiMqBo.class);
  private static final Logger LOG_PTI_OUT = LoggerFactory.getLogger("PTI_OUT");

  @Autowired
  @Qualifier(value = "JmsMessagesPtiBo")
  private JmsMessagesPtiBo jmsMessagesBo;

  @Value(value = "${jms.pti.qmanager.name}")
  private String ptiJndiName;

  @Value("${jms.pti.qmanager.host}")
  private String ptiMqHost;

  @Value("${jms.pti.qmanager.port}")
  private int ptiMqPort;

  @Value(" ${jms.pti.qmanager.channel}")
  private String ptiMqChannel;

  @Value("${jms.pti.qmanager.qmgrname}")
  private String ptiMqManagerName;

  @Value("${jms.pti.queue.online.output.queue}")
  private String queueOnlineOut;

  @Value("${jms.pti.queue.batch.output.queue}")
  private String queueBatchOut;

  @Value("${jms.pti.queue.sd.output.queue}")
  private String queueSdBatchOut;

  // @Qualifier(value = "ptiQueueOnlineIn")
  // @Autowired(required = false)
  // private Queue queueOnlineIn;
  // @Qualifier(value = "ptiQueueBatchIn")
  // @Autowired(required = false)
  // private Queue queueBatchIn;

  public FileToPtiMqBo() {
  }

  /**
   * @throws JMSException
   * @see
   * <a>https://www.ibm.com/support/knowledgecenter/SSFKSJ_7.5.0/com.ibm.mq.
   * dev.doc/q032070_.htm</a>
   * @see
   * <a>https://www.ibm.com/support/knowledgecenter/SSFKSJ_7.5.0/com.ibm.mq.
   * dev.doc/q031990_.htm</a>
   * https://www.ibm.com/support/knowledgecenter/SSEQTP_7
   * .0.0/com.ibm.websphere.nd.doc/info/ae/ae/rjc0007_.html
   * 
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Path enviarFicheros(Path file, final boolean batchMode, boolean scriptDividendMode)
      throws SIBBACBusinessException, JMSException {
    LOG.trace("[FilesystemToPtiMqBo :: enviarFicheros] inicio.");
    String logToken = " - ";
    String destination = null;
    ConnectionFactory cf = null;
    try {
      cf = jmsMessagesBo.getConnectionFactoryFromContext(ptiJndiName);
    }
    catch (Exception e) {
      LOG.trace(e.getMessage(), e);
      cf = jmsMessagesBo.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
    }

    if (cf == null) {
      throw new SIBBACBusinessException("No hemos podido conectar con el gestor Mq.");
    }
    JmsTemplate jmsTemplate = jmsMessagesBo.getNewJmsTemplate(cf, true);

    if (scriptDividendMode) {
      destination = queueSdBatchOut;
    }
    else if (batchMode) {
      destination = queueBatchOut;
    }
    else {
      destination = queueOnlineOut;
    }
    if (file != null && Files.exists(file)) {
      String originalname = file.toFile().getName();
      boolean gz = originalname.toUpperCase().endsWith(".GZ");
      Path renombrado = Paths.get(file.getParent().toString(), originalname.substring(0, originalname.indexOf("."))
          + ".procesando");
      InputStream is = null;
      try {
        LOG.info("[FilesystemToPtiMqBo :: enviarFicheros] moviendo fichero de {} a {}...", file.toString(),
            renombrado.toString());
        LOG.info("[FilesystemToPtiMqBo :: enviarFicheros] Los envios se van a hacer por la cola: {}", destination);
        file = Files.move(file, renombrado, StandardCopyOption.ATOMIC_MOVE);
        byte[] groupId = ("" + System.currentTimeMillis() + file.hashCode() + this.hashCode()).getBytes();

        if (gz) {
          is = new GZIPInputStream(new FileInputStream(file.toFile()));
        }
        else {
          is = new FileInputStream(file.toFile());
        }
        LineIterator iterator = IOUtils.lineIterator(is, StandardCharsets.ISO_8859_1);
        String appIddentityData = null;
        int i = 0;
        String txtFile;
        boolean hasNext;
        while (iterator.hasNext()) {
          ++i;
          txtFile = iterator.next();

          if (txtFile.contains(logToken)) {
            txtFile = txtFile.substring(txtFile.indexOf(logToken) + logToken.length());
          }
          hasNext = iterator.hasNext();

          if (appIddentityData == null) {
            appIddentityData = jmsMessagesBo.getAppIdentityData(txtFile, batchMode);

          }
          if (i == 1) {
            LOG.info("[FilesystemToPtiMqBo :: enviarFicheros] Enviando msg primer msg del lote msg {}", txtFile);
          }
          else if (i % 1000 == 0) {
            LOG.info("[FilesystemToPtiMqBo :: enviarFicheros] Enviando msg index: {} msg {}", i, txtFile);
          }
          if (!hasNext) {
            LOG.info("[FilesystemToPtiMqBo :: enviarFicheros] Enviando msg ultimo msg del lote msg {}", txtFile);
          }
          jmsMessagesBo.sendPtiJmsTextMessage(jmsTemplate, destination, batchMode, groupId, i, hasNext,
              appIddentityData, txtFile);

        }// fin while
        iterator.close();

        // } catch (JMSException e) {
        // Path originalPath = Paths.get(file.getParent().toString(),
        // originalname);
        // try {
        // file = Files.move(file, originalPath,
        // StandardCopyOption.ATOMIC_MOVE);
        // } catch (IOException e1) {
        // throw new SIBBACBusinessException("Incidencia moviendo el fichero : "
        // + file.toString()
        // + " al fichero original : " + originalPath.toString() + " "
        // + e1.getMessage(), e);
        // }
        // throw new SIBBACBusinessException(e);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e);
      }
      finally {
        if (is != null) {
          try {
            is.close();
          }
          catch (IOException e) {

          }
        }
      }

    }

    LOG.trace("[FilesystemToPtiMqBo :: enviarFicheros] fin.");
    return file;
  }

  public void escribirPtiOut(Path file, String originalname) throws FileNotFoundException, IOException {
    LOG.trace("[FilesystemToPtiMqBo :: escribirPtiOut] incio leer fichero {}.", originalname);
    String logToken = " - ";
    InputStream is = null;
    boolean gz = originalname.toUpperCase().endsWith(".GZ");
    if (gz) {
      is = new GZIPInputStream(new FileInputStream(file.toFile()));
    }
    else {
      is = new FileInputStream(file.toFile());
    }
    LineIterator iterator = IOUtils.lineIterator(is, StandardCharsets.ISO_8859_1);

    String msg;
    while (iterator.hasNext()) {
      msg = iterator.next();
      if (msg.contains(logToken)) {
        msg = msg.substring(msg.indexOf(logToken) + logToken.length());
      }
      LOG_PTI_OUT.info(msg);
    }

    iterator.close();
    is.close();
    LOG.trace("[FilesystemToPtiMqBo :: escribirPtiOut] fin.");
  }

  public void moveToTratados(Path directorioDestino, Path file, String originalName, boolean gz) throws IOException {
    String name = originalName + "_" + FormatDataUtils.convertDateToConcurrentFileName(new Date()) + (gz ? ".GZ" : "");
    if (gz) {
      gz = !originalName.toUpperCase().endsWith(".GZ");
      if (!gz) {
        name = originalName.substring(0, originalName.toUpperCase().indexOf(".GZ")) + "_"
            + FormatDataUtils.convertDateToConcurrentFileName(new Date()) + ".GZ";
      }
    }

    Path destino = Paths.get(directorioDestino.toString(), name);

    LOG.info("[FilesystemToPtiMqBo :: moveToTratados] Se va a mover el fichero {} a {}", file.toString(),
        destino.toString());

    if (gz) {
      OutputStream os = Files.newOutputStream(destino, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
      GZIPOutputStream gzos = new GZIPOutputStream(os);
      Files.copy(file, gzos);
      gzos.close();
      os.close();
      Files.deleteIfExists(file);
    }
    else {
      Files.move(file, destino, StandardCopyOption.ATOMIC_MOVE);
    }
  }

}
