package sibbac.business.comunicaciones.euroccp.realigment.mapper;

import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.realigment.dto.EuroCcpExecutionRealigmentDTO;
import sibbac.business.comunicaciones.euroccp.realigment.enums.EuroCcpExecutionRealigmentFields;
import sibbac.business.comunicaciones.euroccp.titulares.mapper.EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpExecutionRealigmentFieldSetMapper extends
    WrapperAbstractFieldSetMapper<EuroCcpCommonSendRegisterDTO> {

  public EuroCcpExecutionRealigmentFieldSetMapper() {
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpExecutionRealigmentFields.values();
  }

  @Override
  public EuroCcpExecutionRealigmentDTO getNewInstance() {
    return new EuroCcpExecutionRealigmentDTO();
  }

  @Override
  public LineAggregator<EuroCcpCommonSendRegisterDTO> getLineAggregator() {
    FormatterLineAggregator<EuroCcpCommonSendRegisterDTO> lineAggregator = new EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator<EuroCcpCommonSendRegisterDTO>(
        this);

    BeanWrapperFieldExtractor<EuroCcpCommonSendRegisterDTO> fieldExtractor = new BeanWrapperFieldExtractor<EuroCcpCommonSendRegisterDTO>();
    fieldExtractor.setNames(getFieldNames());
    lineAggregator.setFormat(getFormat());
    lineAggregator.setFieldExtractor(fieldExtractor);
    return lineAggregator;
  }

}
