package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.IndicadorCapacidad;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.ce.CE;
import sibbac.pti.messages.ce.CEControlBlock;
import sibbac.pti.messages.ce.R00;

@Service("ptiMessageProcessCeBo")
public class PtiMessageProcessCeBo extends PtiMessageProcess<CE> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessCeBo.class);

  @Autowired
  private Tmct0operacioncdeccBo opEccBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0logBo logBo;

  public PtiMessageProcessCeBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, CE ce, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    int pageTam = 100;
    R00 r00;
    String cdisin;
    Date fecontratacion;
    String nuexemkt;
    String miembroMercado;
    Character cdsentido;
    String cdopracionmkt;
    String segmentoMercado;
    List<Tmct0operacioncdecc> ops;
    StringBuilder sb;
    List<PTIMessageRecord> r00s = ce.getAll(PTIMessageRecordType.R00);
    CEControlBlock controlBlock = (CEControlBlock) ce.getControlBlock();
    String codigoError = controlBlock.getCodigoDeError();
    String descripcionError = controlBlock.getTextoDeError();
    boolean error = !"000".equalsIgnoreCase(codigoError) && !"".equals(codigoError.trim());
    if (CollectionUtils.isNotEmpty(r00s)) {

      for (PTIMessageRecord record : r00s) {
        r00 = (R00) record;
        cdisin = r00.getCodigoIsin();
        fecontratacion = new Date(r00.getFechaContratacion().getTime());
        nuexemkt = r00.getNumeroEjecucion();
        miembroMercado = r00.getMiembroMercado();
        cdsentido = cache.getSentidoPtiSentidoSibbac(r00.getIndicadorCompraVenta());
        cdopracionmkt = r00.getTipoOperacion();
        segmentoMercado = r00.getSegmentoMercado();
        sb = new StringBuilder("Datos recepcion CE");
        sb.append(" Miembro:").append(miembroMercado);
        sb.append("-CdOperacionMkt:").append(cdopracionmkt);
        sb.append("-Segmento Mercado:").append(segmentoMercado);
        sb.append("-Sentido:").append(cdsentido);
        sb.append("-ISIN:").append(cdisin);
        sb.append("-N.ExeMkt:").append(nuexemkt);
        sb.append("-F.Contratacion:").append(fecontratacion);
        LOG.info(sb.toString());

        ops = opEccBo.findOpEccForCe(cdopracionmkt, cdisin, nuexemkt, fecontratacion, segmentoMercado, miembroMercado);
        if (CollectionUtils.isNotEmpty(ops)) {
          if (ops.size() > 1) {
            LOG.warn("MAS DE UNA OP ENCONTRADA...");
          }
          for (Tmct0operacioncdecc op : ops) {
            if (op.getCeenviado() != null && op.getCeenviado() == 'S') {
              if (!error && !r00.getIndicadorCapacidadTReporting().trim().isEmpty()) {
                op.setCdindcapacidad(r00.getIndicadorCapacidadTReporting().charAt(0));
              }
              this.processOp(op, error, descripcionError, pageTam);
            }
            else {
              LOG.warn("La op ecc id: {} - {} - {} no tiene la marca de ce enviado.", op.getIdoperacioncdecc(),
                  op.getCdoperacionecc(), op.getFcontratacion());
            }
          }
        }
        else {
          LOG.debug("Operacion Ecc NO encontrada.");
        }
      }
    }
    else {
      LOG.warn("CE NO TIENE R00'S");
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void processOp(Tmct0operacioncdecc op, boolean error, String descripcionError, int pageTam)
      throws SIBBACBusinessException {
    List<Tmct0desgloseclitit> desgloses;
    LOG.debug("Operacion Ecc encontrada.");

    StringBuilder sb = new StringBuilder("Recepcion CE Ind.Capacidad Op.ECC:");
    sb.append(op.getCdoperacionecc());
    sb.append("-Nuexemkt:").append(op.getNuexemkt());
    sb.append(" a ").append(op.getCdindcapacidad());
    if (IndicadorCapacidad.PROPIO.equals(op.getCdindcapacidad())) {
      sb.append("-PROPIO");
    }
    else {
      sb.append("-AJENO");
    }
    if (error) {
      sb.append(" rechazado.Error:").append(descripcionError);
    }
    else {
      sb.append(" aceptado.");
    }
    LOG.debug(sb.toString());
    Pageable page = new PageRequest(0, pageTam);
    do {
      desgloses = dctBo.findAllByDatosMercado(op.getCdisin(), op.getFcontratacion(), op.getCdsentido(),
          op.getCdcamara(), op.getCdsegmento(), op.getCdoperacionmkt(), op.getCdmiembromkt(), op.getNuordenmkt(),
          op.getNuexemkt(), page);
      this.procesarDesgloses(desgloses, sb);
      page = page.next();
    }
    while (desgloses.size() == page.getPageSize());

    if (error) {
      op.setCeenviado('N');
    }
    else {
      op.setCeenviado('S');
    }
    op.setAuditFechaCambio(new Date());
    op.setAuditUser("SIBBAC20");
    opEccBo.save(op);
  }

  @Transactional
  private void procesarDesgloses(final List<Tmct0desgloseclitit> desgloses, StringBuilder sb)
      throws SIBBACBusinessException {
    for (Tmct0desgloseclitit desglose : desgloses) {
      logBo.insertarRegistro(desglose.getTmct0desglosecamara().getTmct0alc(), new Date(), new Date(), "", desglose
          .getTmct0desglosecamara().getTmct0estadoByCdestadoasig(), sb.toString());

    }
  }

}