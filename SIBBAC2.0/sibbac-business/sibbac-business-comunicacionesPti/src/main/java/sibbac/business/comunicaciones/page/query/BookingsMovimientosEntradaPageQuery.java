package sibbac.business.comunicaciones.page.query;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

@Component("BookingsMovimientosEntradaPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BookingsMovimientosEntradaPageQuery extends AbstractDynamicPageQuery {

  private static final String SELECT = "SELECT "
      + "DC.NBOOKING, DC.NUCNFCLT, DC.NUCNFLIQ, A.REF_CLIENTE, EST.NOMBRE ESTADO, B.CDALIAS ALIAS, B.DESCRALI DESC_ALIAS, "
      + "B.CDTPOPER SENTIDO, B.CDISIN ISIN, B.NBVALORS DESC_ISIN, DCT.IMTITULOS TITULOS, DCT.IMPRECIO PRECIO, DCT.IMEFECTIVO EFECTIVO";

  private static final String FROM = "FROM TMCT0MOVIMIENTOECC_FIDESSA M "
      + "INNER JOIN TMCT0DESGLOSECLITIT DCT ON M.NUOPERACIONPREV=DCT.CDOPERACIONECC "
      + "INNER JOIN TMCT0DESGLOSECAMARA DC ON DCT.IDDESGLOSECAMARA=DC.IDDESGLOSECAMARA "
      + "INNER JOIN TMCT0ALO A ON DC.NUORDEN=A.NUORDEN AND DC.NBOOKING=A.NBOOKING AND DC.NUCNFCLT=A.NUCNFCLT "
      + "INNER JOIN TMCT0BOK B ON A.NUORDEN=B.NUORDEN AND A.NBOOKING=B.NBOOKING "
      + "INNER JOIN TMCT0ESTADO EST ON DC.CDESTADOASIG=EST.IDESTADO";

  private static final String WHERE = "B.CDESTADOASIG > {0} AND DC.CDESTADOASIG > {0} AND A.CDESTADOASIG > {0} ";

  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("N. Booking", "NBOOKING"),
      new DynamicColumn("N. Desg. Cliente", "NUCNFCLT"), new DynamicColumn("N. Desg. Liquidador", "NUCNFLIQ"),
      new DynamicColumn("Ref. Cliente", "REF_CLIENTE"), new DynamicColumn("Estado", "ESTADO"),
      new DynamicColumn("Cod. Alias", "ALIAS"), new DynamicColumn("Desc. Alias", "DESC_ALIAS"),
      new DynamicColumn("Sentido", "SENTIDO"), new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Desc. Isin", "DESC_ISIN"),
      new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Precio", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Efectivo", "EFECTIVO", DynamicColumn.ColumnType.N4) };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter("CDREFMOVIMIENTOECC", "Movimiento"));
    return res;
  }

  public BookingsMovimientosEntradaPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {
    return MessageFormat.format(WHERE, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return null;
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return null;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
  }

}
