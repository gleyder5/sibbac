package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;


public class DatosAsignacionIntermediarioFinancieroDTO implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = -160245012669322909L;
  /**
   * Cuenta interna de compensacion
   * */
  private String cuentaCompensacion = null;
  private Long idCuentaCompensacion = null;
  /**
   * Miembro compensador externo
   * */
  private String miembroCompensador = null;
  private Long idMiembroCompensador = null;
  /**
   * Referencia pactada del miembro compensador externo
   * */
  private String referenciaAsignacionMiembroCompensador = null;

  public String getCuentaCompensacion() {
    return cuentaCompensacion;
  }

  public void setCuentaCompensacion(String cuentaCompensacion) {
    this.cuentaCompensacion = cuentaCompensacion;
  }

  public String getMiembroCompensador() {
    return miembroCompensador;
  }

  public void setMiembroCompensador(String miembroCompensador) {
    this.miembroCompensador = miembroCompensador;
  }

  public String getReferenciaAsignacionMiembroCompensador() {
    return referenciaAsignacionMiembroCompensador;
  }

  public void setReferenciaAsignacionMiembroCompensador(String referenciaAsignacionMiembroCompensador) {
    this.referenciaAsignacionMiembroCompensador = referenciaAsignacionMiembroCompensador;
  }

  public Long getIdCuentaCompensacion() {
    return idCuentaCompensacion;
  }

  public void setIdCuentaCompensacion(Long idCuentaCompensacion) {
    this.idCuentaCompensacion = idCuentaCompensacion;
  }

  public Long getIdMiembroCompensador() {
    return idMiembroCompensador;
  }

  public void setIdMiembroCompensador(Long idMiembroCompensador) {
    this.idMiembroCompensador = idMiembroCompensador;
  }

  @Override
  public String toString() {
    return "DatosAsignacionDTO [cuentaCompensacion=" + cuentaCompensacion + ", idCuentaCompensacion="
           + idCuentaCompensacion + ", miembroCompensador=" + miembroCompensador + ", idMiembroCompensador="
           + idMiembroCompensador + ", referenciaAsignacionMiembroCompensador="
           + referenciaAsignacionMiembroCompensador + "]";
  }

}
