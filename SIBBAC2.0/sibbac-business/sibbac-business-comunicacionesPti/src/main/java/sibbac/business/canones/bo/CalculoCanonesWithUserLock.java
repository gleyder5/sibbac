package sibbac.business.canones.bo;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

@Service
public class CalculoCanonesWithUserLock {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CalculoCanonesWithUserLock.class);

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private CalculoCanonesWriteLog calculoCanonWriteLog;

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void bajaCanonCalculadoDesglosesBaja(Tmct0bokId id, String auditUser) throws SIBBACBusinessException {
    boolean imLock = false;

    try {
      bokBo.bloquearBooking(id, auditUser);
      imLock = true;
      List<Tmct0alcId> listAlcs = alcBo.findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndBaja(id, 'S');
      if (CollectionUtils.isNotEmpty(listAlcs)) {
        for (Tmct0alcId alcId : listAlcs) {
          calculoCanonWriteLog.bajaCanonCalculadoDesglosesBaja(alcId, auditUser);
        }
      }
    }
    catch (LockUserException e) {
      LOG.warn(e.getMessage(), e);
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(id, auditUser);
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException("No se ha podido desbloquar el booking");
        }
      }
    }
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void calcularCanon(Tmct0bokId id, CanonesConfigDTO reglasCanones, String auditUser)
      throws SIBBACBusinessException {
    boolean imLock = false;

    try {
      bokBo.bloquearBooking(id, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), auditUser);
      imLock = true;
      LOG.trace("[calcularCanon] buscando alc's del booking {} con el canon sin calcular...", id);
      List<Tmct0alcId> listAlcs = alcBo.findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndConReferenciaTitular(id, 'N');

      LOG.trace("[calcularCanon] encontrados {} alc's del booking {} con el canon sin calcular...", listAlcs.size(), id);
      if (CollectionUtils.isNotEmpty(listAlcs)) {
        for (Tmct0alcId alcId : listAlcs) {
          calculoCanonWriteLog.calcularCanon(alcId, reglasCanones, auditUser);
        }
      }
    }
    catch (LockUserException e) {
      LOG.warn(e.getMessage(), e);
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(id, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), auditUser);
        }
        catch (LockUserException e) {
          throw new SIBBACBusinessException("No se ha podido desbloquar el booking");
        }
      }
    }
  }

}
