package sibbac.business.desglose.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.escalado.bo.ActualizacionCodigosOperacionPtiBo;
import sibbac.business.desglose.commons.AlgoritmoDesgloseManual;
import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.DesgloseManualDTO;
import sibbac.business.desglose.commons.DesgloseManualEjecucionDTO;
import sibbac.business.desglose.commons.DesgloseManualGrupoEjecucionDTO;
import sibbac.business.desglose.commons.SettlementChainDataException;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.desglose.commons.TipoDesgloseManual;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.titulares.bo.CreacionTablasTitularesBo;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0grupoejecucionBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0alcHelper;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;

@Service
public class DesgloseBo {
  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(DesgloseBo.class);

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0grupoejecucionBo grupoEjecucionBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private CreacionTablasTitularesBo tablasTitularesBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private DesgloseTmct0alcHelper desgloseAlcHelper;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private ActualizacionCodigosOperacionPtiBo actualizacionCodigosBo;

  @Autowired
  private ApplicationContext ctx;

  public DesgloseBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void desgloseNacionalAutomatico(Tmct0aloId id, DesgloseConfigDTO config) throws SIBBACBusinessException,
      SettlementChainDataException {
    LOG.info("[desglosarNacional] alo: {} inicio.", id);
    String msg;
    SettlementData settlementData;
    Tmct0alo alo = aloBo.findById(id);
    if (alo == null) {
      throw new SIBBACBusinessException("Alo no encontrado.");
    }
    // TODO QUIZA NO SEA NECESARIO REVISAR LOS ESTADOS
    else if (alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSAR.getId()
        && alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSE_MANUAL
            .getId()
        && alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.SIN_INSTRUCCIONES.getId()) {
      throw new SIBBACBusinessException(MessageFormat.format("Estado del alo {0} no permite el desglose.", alo
          .getTmct0estadoByCdestadoasig().getNombre()));
    }
    else {
      settlementData = config.getSettlementDataNacional(alo);
      boolean isRouting = ordBo.isRouting(alo.getTmct0bok().getTmct0ord());
      boolean isScriptDividend = ordBo.isScriptDividend(alo.getTmct0bok().getTmct0ord());

      this.checkSettlmentData(isRouting, isScriptDividend, settlementData);

      msg = String.format("Las instrucciones de liquidacion para alias: '%s' estan en '%s'-'%s'",
          StringUtils.trim(alo.getCdalias()), settlementData.getTmct0ilq().getDesglose(),
          this.getDescripcionDesgloseIlq(settlementData.getTmct0ilq().getDesglose()));
      LOG.debug("[desglosarNacional] {}", msg);
      logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
          .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());

      if (settlementData.getTmct0ilq().getDesglose() == null || settlementData.getTmct0ilq().getDesglose() == 'M') {
        msg = String.format("Cambiado estado alo a '%s'-DESGLOSE_MANUAL",
            EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSE_MANUAL.getId());
        LOG.debug("[desglosarNacional] {}", msg);
        logBo.insertarRegistro(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig().getNombre(), msg,
            config.getAuditUser());
        alo.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSE_MANUAL.getId()));
        alo.setFhaudit(new Date());
        alo.setCdusuaud(config.getAuditUser());
      }
      else {
        try {
          this.desgloseNacionalAutomatico(alo, settlementData, config);
        }
        catch (SIBBACBusinessException e) {
          msg = String.format("INCIDENCIA_DESGLOSE- %s", e.getMessage());
          throw new SIBBACBusinessException(msg, e);
        }
      }

    }

    LOG.info("[desglosarNacional] alo: {} final.", id);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void desgloseNacionalManual(Tmct0bokId bookId, List<SolicitudDesgloseManual> solicitudesDesglose,
      DesgloseConfigDTO config) throws SIBBACBusinessException, SettlementChainDataException {
    for (SolicitudDesgloseManual solicitudDesgloseManual : solicitudesDesglose) {
      this.desgloseNacionalManual(solicitudDesgloseManual, config);
    }
  }

  private void setFlagsIlqDesgloseManual(Tmct0alo alo, List<DesgloseManualDTO> altas) {
    SettlementData ilq;
    boolean isRouting = ordBo.isRouting(alo.getTmct0bok().getTmct0ord());
    boolean isScriptDividend = ordBo.isScriptDividend(alo.getTmct0bok().getTmct0ord());
    for (DesgloseManualDTO alta : altas) {
      ilq = alta.getSettlementData();
      if (ilq != null) {
        if (ilq.getListHolder() != null && !ilq.getListHolder().isEmpty()) {
          ilq.setDataTFI(true);
        }
        else {
          ilq.setDataTFI(false);
        }

        if (ilq.getClearer() != null && !ilq.getClearer().getCdcleare().trim().isEmpty()) {
          ilq.setDataClearer(true);
        }
        else {
          ilq.setDataClearer(false);
        }

        if (ilq.getCustodian() != null && !ilq.getCustodian().getCdcleare().trim().isEmpty()) {
          ilq.setDataCustodian(true);
        }
        else {
          ilq.setDataCustodian(false);
        }

        if (ilq.isClearerMultiple()) {
          ilq.setDataIL(false);
        }
        else {
          if (!ilq.isDataClearer() && !ilq.isDataCustodian() && !isRouting && !isScriptDividend && !ilq.isDataTFI()) {
            ilq.setDataIL(false);
          }
          else {
            ilq.setDataIL(true);
          }
        }

      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void desgloseNacionalManual(SolicitudDesgloseManual solicitudDesglose, DesgloseConfigDTO config)
      throws SIBBACBusinessException, SettlementChainDataException {

    Tmct0aloId id = solicitudDesglose.getTmct0aloId();
    List<Tmct0alcId> bajas = solicitudDesglose.getBajas();
    List<DesgloseManualDTO> altas = solicitudDesglose.getAltas();

    List<DesgloseManualEjecucionDTO> listTitulosDisponibles = new ArrayList<DesgloseManualEjecucionDTO>();
    LOG.debug("[desgloseNacionalManual] inicio alo: {} TipoDesglose: {} user: {}", id, config.getTipoDesgloseManual(),
        config.getAuditUser());

    if (config.getTipoDesgloseManual() == null) {
      throw new SIBBACBusinessException("INCIDENCIA-El tipo de desglose manual debe informarse MANUAL/EXCEL");
    }

    if (CollectionUtils.isEmpty(altas)) {
      throw new SIBBACBusinessException("INCIDENCIA-La lista de desgloses de alta debe contener datos.");
    }

    boolean repartoPantalla = this.validarRepartoPantallaDesgloseManual(altas);

    this.bajaAlcs(id, bajas, config, listTitulosDisponibles);

    Tmct0alo alo = aloBo.findById(id);

    this.setFlagsIlqDesgloseManual(alo, altas);

    this.validarEstadoAloDesgloseManual(alo);

    if (CollectionUtils.isEmpty(bajas) && CollectionUtils.isEmpty(listTitulosDisponibles)) {
      listTitulosDisponibles.addAll(this.getTitulosDisponiblesFromGruposEjecucion(alo, config));
    }

    if (repartoPantalla) {
      this.mapearRepartoAndPersist(alo, solicitudDesglose, config);
    }
    else {

      if (solicitudDesglose.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.FEWEST) {
        this.desgloseAlgoritmoFewest(alo, solicitudDesglose, listTitulosDisponibles, config);
      }
      else if (solicitudDesglose.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.PROPORTIONAL) {
        this.desgloseAlgoritmoProportional(alo, solicitudDesglose, listTitulosDisponibles, config);
      }
      else if (solicitudDesglose.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.CLEARING_INFORMATION) {
        this.desgloseAlgoritmoAgrupandoPorClearingInformation(alo, solicitudDesglose, listTitulosDisponibles, config);
      }
      else if (solicitudDesglose.getAlgoritmoDesgloseManual() == AlgoritmoDesgloseManual.PRICE) {
        this.desgloseAlgoritmoAgrupandoPorPrecio(alo, solicitudDesglose, listTitulosDisponibles, config);
      }
      else {
        throw new SIBBACBusinessException(MessageFormat.format("Algoritmo de desglose no reconocido {0}",
            solicitudDesglose.getAlgoritmoDesgloseManual()));

      }
    }
    LOG.debug("[desgloseNacionalManual] fin alo: {} user: {}", id, config.getAuditUser());
  }

  private void validarEstadoAloDesgloseManual(Tmct0alo alo) throws SIBBACBusinessException {
    if (alo == null) {
      throw new SIBBACBusinessException("INCIDENCIA-Alo no encontrado");
    }
    else if (alo.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSAR.getId()
        || alo.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId()
        || alo.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()
        || alo.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId()
        || alo.getTmct0estadoByCdestadoasig().getIdestado() == EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR_SIN_TITULAR
            .getId()) {
      throw new SIBBACBusinessException(
          String
              .format(
                  "INCIDENCIA-Alo en estado '%s' no permite el desglose.En estos estados otros procesos podrian estar trabajando.",
                  StringUtils.trim(alo.getTmct0estadoByCdestadoasig().getNombre())));
    }
  }

  private void bajaAlcs(Tmct0aloId id, List<Tmct0alcId> bajas, DesgloseConfigDTO config,
      List<DesgloseManualEjecucionDTO> listTitulosDisponibles) throws SIBBACBusinessException {
    // BAJA DESGLOSES
    if (CollectionUtils.isNotEmpty(bajas)) {
      listTitulosDisponibles.addAll(this.bajaAlcs(bajas, config));
    }
    else {
      bajas.addAll(alcBo.findAllTmct0alcIdByTmct0aloIdAndDeAlta(id));
      if (CollectionUtils.isNotEmpty(bajas)) {
        listTitulosDisponibles.addAll(this.bajaAlcs(bajas, config));
      }
    }
  }

  private boolean validarRepartoPantallaDesgloseManual(List<DesgloseManualDTO> altas) throws SIBBACBusinessException {
    boolean conReparto = false;
    boolean sinReparto = false;

    for (DesgloseManualDTO desgloseManualDTO : altas) {
      if (CollectionUtils.isNotEmpty(desgloseManualDTO.getRepartoEjecuciones())) {
        conReparto = true;
      }
      else {
        sinReparto = true;
      }
    }

    if (conReparto && sinReparto) {
      throw new SIBBACBusinessException("INCIDENCIA-Todo debe venir con el reparto hecho o sin hacer, pero no a la vez");
    }
    return conReparto;
  }

  private void mapearRepartoAndPersist(Tmct0alo alo, SolicitudDesgloseManual solicitudDesglose, DesgloseConfigDTO config)
      throws SIBBACBusinessException, SettlementChainDataException {
    LOG.trace("[desgloseManualRepartoHecho] inicio TipoDesglose: {} User: {}", config.getTipoDesgloseManual(),
        config.getAuditUser());
    String msg;
    SettlementData settlementData;

    Tmct0alc alc;
    Tmct0desglosecamara dc;
    Tmct0desgloseclitit dct;
    Tmct0movimientoecc mov;
    BigDecimal titulosAlc;
    List<Tmct0movimientoecc> listMovs;
    Map<String, Tmct0desglosecamara> mapDesglosesCamaraByCamara;

    List<DesgloseManualDTO> altas = solicitudDesglose.getAltas();
    Character corretajePtiAlias = config.getCorretajePtiAlias(alo.getCdalias());
    Character corretajePtiManual = config.getCorretajePtiAlias(alo.getCdalias());
    Character corretajePtiCompensador;

    boolean isRouting = ordBo.isRouting(alo.getTmct0bok().getTmct0ord());
    boolean isScriptDividend = ordBo.isScriptDividend(alo.getTmct0bok().getTmct0ord());

    Short nucnfliq = this.getMaxNucnfliqAdd1(alo.getId());
    LOG.debug("[mapearRepartoAndPersist] secuencia para alc: {} TipoDesglose: {} User: {}", nucnfliq,
        config.getTipoDesgloseManual(), config.getAuditUser());

    if (CollectionUtils.isNotEmpty(altas)) {

      if (config.getTipoDesgloseManual() == TipoDesgloseManual.EXCEL) {
        msg = "Peticion Desglose Excel ";
      }
      else {
        msg = "Peticion Desglose Manual ";
      }

      msg = String.format("%s Algoritmo: '%s' ", msg, solicitudDesglose.getAlgoritmoDesgloseManual());
      LOG.debug("[mapearRepartoAndPersist] {}", msg);
      logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
          .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());

      for (DesgloseManualDTO desgloseManualDTO : altas) {

        if (CollectionUtils.isNotEmpty(desgloseManualDTO.getRepartoEjecuciones())) {

          settlementData = desgloseManualDTO.getSettlementData();

          corretajePtiManual = desgloseManualDTO.getTipoEnvioComision();
          corretajePtiCompensador = null;
          if (corretajePtiManual == null || corretajePtiManual == ' ') {
            corretajePtiManual = corretajePtiAlias;
          }

          this.checkSettlmentData(isRouting, isScriptDividend, settlementData);

          alc = this.crearNuevoAlcManual(alo, desgloseManualDTO, nucnfliq, config.getAuditUser());

          titulosAlc = BigDecimal.ZERO;
          mapDesglosesCamaraByCamara = new HashMap<String, Tmct0desglosecamara>();

          for (DesgloseManualEjecucionDTO reparto : desgloseManualDTO.getRepartoEjecuciones()) {
            titulosAlc = titulosAlc.add(reparto.getImtitulos());
            dc = mapDesglosesCamaraByCamara.get(reparto.getCdcamara());
            if (dc == null) {
              dc = this.crearNuevoDesgloseCamaraManual(alo.getTmct0bok().getTmct0ord(), alo.getTmct0bok(), alo,
                  reparto, config);
              dc.setTmct0alc(alc);
              this.asignarInformacionAsignacionDesgloseManual(alo.getTmct0bok().getTmct0ord(), alo, dc,
                  desgloseManualDTO, config);
              if (StringUtils.isNotBlank(dc.getTmct0infocompensacion().getCdmiembrocompensador())) {
                corretajePtiCompensador = config.getCorretajePtiCompensador(dc.getTmct0infocompensacion()
                    .getCdmiembrocompensador());
              }

              alcBo.setTipoEnvioCorretaje(alc, isRouting, isScriptDividend, dc.getTmct0infocompensacion()
                  .getCdctacompensacion(), dc.getTmct0infocompensacion().getCdmiembrocompensador(), corretajePtiManual,
                  corretajePtiCompensador);

              mapDesglosesCamaraByCamara.put(reparto.getCdcamara(), dc);
            }
            else {
              dc.setNutitulos(dc.getNutitulos().add(reparto.getImtitulos()));
            }
            dct = reparto.getNewDesglose();
            dct.setMtf(config.isCdbrokerMtf(dct.getCdPlataformaNegociacion()));
            dct.setImefectivo(dct.getImtitulos().multiply(dct.getImprecio()).setScale(2, RoundingMode.HALF_UP));

            dct.setAuditFechaCambio(new Date());
            dct.setAuditUser(config.getAuditUser());

            dct.setTmct0desglosecamara(dc);
            dc.getTmct0desgloseclitits().add(dct);
            mov = movBo.crearMovimientosOrigenDestinoIgual(dc, dct, dc.getTmct0infocompensacion(),
                config.getAuditUser());
            if (mov != null) {
              listMovs = new ArrayList<Tmct0movimientoecc>();
              listMovs.add(mov);
              movBo.escribirTmct0logForMovimientos(alc.getId(), listMovs, msg, msg, config.getAuditUser());
              listMovs.clear();
            }

          }
          alc.getTmct0desglosecamaras().addAll(mapDesglosesCamaraByCamara.values());
          alc.setNutitliq(titulosAlc);
          if (alc.getNutitliq().compareTo(BigDecimal.ZERO) <= 0) {
            throw new SIBBACBusinessException("INCIDENCIA_DESGLOSE-Creado desglose con 0 titulos");
          }

          nucnfliq = (short) (nucnfliq + (short) 1);

          alo.getTmct0alcs().add(alc);
          desgloseManualDTO.setTmct0alcCreado(alc);
        }
      }
    }

    this.postCreateAlcsDesgloseManual(alo, solicitudDesglose, config);
    LOG.trace("[mapearRepartoAndPersist] inicio");
  }

  private void desgloseAlgoritmoFewest(Tmct0alo alo, SolicitudDesgloseManual solicitudDesglose,
      List<DesgloseManualEjecucionDTO> listTitulosDisponibles, DesgloseConfigDTO config)
      throws SIBBACBusinessException, SettlementChainDataException {
    BigDecimal titulosPendientesDesglose;
    BigDecimal titulosGrupo;
    this.removeEmpty(listTitulosDisponibles);
    List<DesgloseManualGrupoEjecucionDTO> listGruposEjecucion = getGruposEjecucion(listTitulosDisponibles);
    sortFewestGrupos(listGruposEjecucion);

    this.sortFewestDesgloses(solicitudDesglose.getAltas());

    for (DesgloseManualDTO desgloseManualDTO : solicitudDesglose.getAltas()) {
      if (CollectionUtils.isEmpty(desgloseManualDTO.getRepartoEjecuciones())) {
        if (desgloseManualDTO.getTitulos() == null || desgloseManualDTO.getTitulos().compareTo(BigDecimal.ZERO) == 0) {
          throw new SIBBACBusinessException("INCIDENCIA-Algoritmo FEWEST necesita los titulos.");
        }
        titulosPendientesDesglose = desgloseManualDTO.getTitulos().setScale(2, RoundingMode.HALF_UP);
        if (titulosPendientesDesglose.compareTo(BigDecimal.ZERO) > 0) {
          for (DesgloseManualGrupoEjecucionDTO grupo : listGruposEjecucion) {
            titulosGrupo = grupo.getTitulos();
            if (titulosPendientesDesglose.compareTo(titulosGrupo) > 0) {
              desgloseManualDTO.getRepartoEjecuciones().addAll(grupo.remove(titulosGrupo));
              titulosPendientesDesglose = titulosPendientesDesglose.subtract(titulosGrupo);
            }
            else {
              desgloseManualDTO.getRepartoEjecuciones().addAll(grupo.remove(titulosPendientesDesglose));
              titulosPendientesDesglose = BigDecimal.ZERO;
              break;
            }
          }
          this.removeEmptyGrupos(listGruposEjecucion);
          this.sortFewestGrupos(listGruposEjecucion);
        }
        if (!desgloseManualDTO.isTerminado()) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA-No hay titulos suficientes para el desglose que ocupa la posicion %s dentro del alo.",
              solicitudDesglose.getAltas().indexOf(desgloseManualDTO)));
        }
      }
    }

    this.mapearRepartoAndPersist(alo, solicitudDesglose, config);
  }

  private List<DesgloseManualGrupoEjecucionDTO> getGruposEjecucion(
      List<DesgloseManualEjecucionDTO> listTitulosDisponibles) {

    DesgloseManualGrupoEjecucionDTO grupo;
    Map<String, DesgloseManualGrupoEjecucionDTO> mapGrupos = new HashMap<String, DesgloseManualGrupoEjecucionDTO>();

    sortFewest(listTitulosDisponibles);

    for (DesgloseManualEjecucionDTO desgloseManualEjecucionDTO : listTitulosDisponibles) {

      grupo = mapGrupos.get(desgloseManualEjecucionDTO.getImprecio().setScale(8, RoundingMode.HALF_UP).toString());
      if (grupo == null) {
        grupo = new DesgloseManualGrupoEjecucionDTO(desgloseManualEjecucionDTO.getImprecio().setScale(8,
            RoundingMode.HALF_UP));
        mapGrupos.put(desgloseManualEjecucionDTO.getImprecio().setScale(8, RoundingMode.HALF_UP).toString(), grupo);
      }

      grupo.add(desgloseManualEjecucionDTO);
      LOG.debug("[getGruposEjecucion] Grupo Precio: {} titulos: {}, ejecucion titulos: {}", grupo.getPrecio(),
          grupo.getTitulos(), desgloseManualEjecucionDTO.getImtitulos());
    }
    return new ArrayList<DesgloseManualGrupoEjecucionDTO>(mapGrupos.values());
  }

  private void sortFewest(List<DesgloseManualEjecucionDTO> listTitulosDisponibles) {
    Collections.sort(listTitulosDisponibles, new Comparator<DesgloseManualEjecucionDTO>() {

      @Override
      public int compare(DesgloseManualEjecucionDTO arg0, DesgloseManualEjecucionDTO arg1) {
        int titulos = arg0.getImtitulos().compareTo(arg1.getImtitulos()) * -1;
        if (titulos != 0) {
          return arg0.getImprecio().compareTo(arg1.getImprecio());
        }
        else {
          return titulos;
        }
      }
    });

  }

  private void sortFewestDesgloses(List<DesgloseManualDTO> listDesgloses) {
    Collections.sort(listDesgloses, new Comparator<DesgloseManualDTO>() {
      @Override
      public int compare(DesgloseManualDTO arg0, DesgloseManualDTO arg1) {
        return arg0.getTitulos().compareTo(arg1.getTitulos()) * -1;
      }
    });

  }

  private void sortFewestGrupos(List<DesgloseManualGrupoEjecucionDTO> listTitulosDisponibles) {
    Collections.sort(listTitulosDisponibles, new Comparator<DesgloseManualGrupoEjecucionDTO>() {
      @Override
      public int compare(DesgloseManualGrupoEjecucionDTO arg0, DesgloseManualGrupoEjecucionDTO arg1) {

        int titulos = arg0.getTitulos().compareTo(arg1.getTitulos()) * -1;
        if (titulos != 0) {
          return titulos;
        }
        else {
          int precio = arg0.getPrecio().compareTo(arg1.getPrecio());
          return precio;
        }
      }
    });

  }

  private void sortPrecioGrupos(List<DesgloseManualGrupoEjecucionDTO> listTitulosDisponibles) {
    Collections.sort(listTitulosDisponibles, new Comparator<DesgloseManualGrupoEjecucionDTO>() {
      @Override
      public int compare(DesgloseManualGrupoEjecucionDTO arg0, DesgloseManualGrupoEjecucionDTO arg1) {
        int precio = arg0.getPrecio().compareTo(arg1.getPrecio());
        if (precio != 0) {
          return arg0.getTitulos().compareTo(arg1.getTitulos()) * -1;
        }
        else {
          return precio;
        }
      }
    });

  }

  private void removeEmptyGrupos(List<DesgloseManualGrupoEjecucionDTO> listTitulosDisponibles) {
    List<DesgloseManualGrupoEjecucionDTO> toRemove = new ArrayList<DesgloseManualGrupoEjecucionDTO>();
    for (DesgloseManualGrupoEjecucionDTO grupo : listTitulosDisponibles) {
      if (grupo.isEmpty()) {
        toRemove.add(grupo);
      }
    }
    listTitulosDisponibles.removeAll(toRemove);
    toRemove.clear();
  }

  private void removeEmpty(List<DesgloseManualEjecucionDTO> listTitulosDisponibles) {
    List<DesgloseManualEjecucionDTO> toRemove = new ArrayList<DesgloseManualEjecucionDTO>();
    for (DesgloseManualEjecucionDTO desgloseManualEjecucionDTO : listTitulosDisponibles) {
      if (desgloseManualEjecucionDTO.getImtitulos().compareTo(BigDecimal.ZERO) <= 0) {
        toRemove.add(desgloseManualEjecucionDTO);
      }
    }
    listTitulosDisponibles.removeAll(toRemove);
    toRemove.clear();
  }

  private void desgloseAlgoritmoProportional(Tmct0alo alo, SolicitudDesgloseManual solicitudDesgloseManual,
      List<DesgloseManualEjecucionDTO> listTitulosDisponibles, DesgloseConfigDTO config)
      throws SIBBACBusinessException, SettlementChainDataException {
    LOG.debug("[desgloseAlgoritmoProportional] inicio. TipoDesglose: {} user: {}", config.getTipoDesgloseManual(),
        config.getAuditUser());
    boolean asignadoTitulo;
    BigDecimal titulosGrupo;
    BigDecimal titulosReparto;
    BigDecimal porcentajeReparto;
    DesgloseManualEjecucionDTO ejecucion;
    BigDecimal titulosPendientesDesglose;
    BigDecimal titulosAntesRepartoOneToOne;
    List<DesgloseManualEjecucionDTO> repartoGrupo;

    List<DesgloseManualDTO> altas = solicitudDesgloseManual.getAltas();
    this.removeEmpty(listTitulosDisponibles);
    List<DesgloseManualGrupoEjecucionDTO> listGruposEjecucion = getGruposEjecucion(listTitulosDisponibles);
    sortPrecioGrupos(listGruposEjecucion);

    Map<DesgloseManualDTO, Map<DesgloseManualGrupoEjecucionDTO, BigDecimal>> titulosPorcentaje = new HashMap<DesgloseManualDTO, Map<DesgloseManualGrupoEjecucionDTO, BigDecimal>>();

    // 0.-Calculamos los porcentajes
    for (DesgloseManualDTO desgloseManualDTO : altas) {
      if (CollectionUtils.isEmpty(desgloseManualDTO.getRepartoEjecuciones())) {
        LOG.debug("[desgloseAlgoritmoProportional] Desglose: {} titulos: {} ", altas.indexOf(desgloseManualDTO),
            desgloseManualDTO.getTitulos());
        porcentajeReparto = desgloseManualDTO.getTitulos().divide(alo.getNutitcli(), 16, RoundingMode.HALF_DOWN);
        titulosPendientesDesglose = desgloseManualDTO.getTitulos();
        for (DesgloseManualGrupoEjecucionDTO grupo : listGruposEjecucion) {
          // titulosPendientesDesglose =
          // desgloseManualDTO.getTitulos().setScale(2, RoundingMode.HALF_UP);
          titulosGrupo = grupo.getTitulos();
          titulosReparto = titulosGrupo.multiply(porcentajeReparto).setScale(0, RoundingMode.HALF_DOWN);
          if (titulosPorcentaje.get(desgloseManualDTO) == null) {
            titulosPorcentaje.put(desgloseManualDTO, new HashMap<DesgloseManualGrupoEjecucionDTO, BigDecimal>());
          }

          if (titulosReparto.compareTo(BigDecimal.ZERO) <= 0) {
            titulosReparto = BigDecimal.ONE;
          }

          if (titulosReparto.compareTo(titulosPendientesDesglose) > 0) {
            titulosReparto = titulosPendientesDesglose;
          }
          else {
            titulosPendientesDesglose = titulosPendientesDesglose.subtract(titulosReparto);
          }
          String cdholder = "";
          if (desgloseManualDTO.getSettlementData().getHolder() != null) {
            cdholder = StringUtils.trim(desgloseManualDTO.getSettlementData().getHolder().getCdholder());
          }
          LOG.debug(
              "[desgloseAlgoritmoProportional] Desglose: {} Titulos: {} Holder: {} porcentaje: {} Le tocan: {} titulos del grupo titulos: {} precio: {}",
              altas.indexOf(desgloseManualDTO), desgloseManualDTO.getTitulos(), cdholder, porcentajeReparto,
              titulosReparto, grupo.getTitulos(), grupo.getPrecio());

          if (titulosReparto.compareTo(BigDecimal.ZERO) > 0) {
            titulosPorcentaje.get(desgloseManualDTO).put(grupo, titulosReparto);
          }

        }
      }
    }

    // 1.- Repartimos por porcentaje
    for (DesgloseManualDTO desgloseManualDTO : altas) {
      titulosPendientesDesglose = desgloseManualDTO.getTitulos().setScale(2, RoundingMode.HALF_UP);
      if (titulosPendientesDesglose.compareTo(BigDecimal.ZERO) > 0) {

        for (DesgloseManualGrupoEjecucionDTO grupo : listGruposEjecucion) {
          if (!grupo.isEmpty()) {
            if (titulosPorcentaje.get(desgloseManualDTO) != null
                && titulosPorcentaje.get(desgloseManualDTO).get(grupo) != null) {
              titulosReparto = titulosPorcentaje.get(desgloseManualDTO).get(grupo);
              repartoGrupo = grupo.remove(titulosReparto);
              desgloseManualDTO.getRepartoEjecuciones().addAll(repartoGrupo);
              LOG.debug(
                  "[desgloseAlgoritmoProportional] Asignando Ejecuciones Desglose: {} Titulos: {}  Le tocan: {} titulos del grupo titulos: {} precio: {}, le tocan: {} ejecuciones",
                  altas.indexOf(desgloseManualDTO), desgloseManualDTO.getTitulos(), titulosReparto, grupo.getTitulos(),
                  grupo.getPrecio(), repartoGrupo.size());
            }
          }

        }
        LOG.debug(
            "[desgloseAlgoritmoProportional] Asignando Ejecuciones Desglose: {} Titulos: {}  Se le han asignado: {} titulos por el porcentaje, le tocan: {} ejecuciones",
            altas.indexOf(desgloseManualDTO), desgloseManualDTO.getTitulos(), desgloseManualDTO.getTitulosRepartidos(),
            desgloseManualDTO.getRepartoEjecuciones().size());

      }
    }

    titulosPorcentaje.clear();

    // 1.- Repartimos de 1 en 1 hasta completar los titulos

    for (DesgloseManualDTO desgloseManualDTO : altas) {

      while (!desgloseManualDTO.isTerminado()) {
        this.removeEmptyGrupos(listGruposEjecucion);
        if (CollectionUtils.isEmpty(listGruposEjecucion)) {
          throw new SIBBACBusinessException(String.format(
              "INCIDENCIA-No hay grupos de ejecucion disponibles para el desglose posicion %s dentro del alo.",
              altas.indexOf(desgloseManualDTO)));
        }
        else {
          titulosAntesRepartoOneToOne = desgloseManualDTO.getTitulosRepartidos();

          for (DesgloseManualGrupoEjecucionDTO grupo : listGruposEjecucion) {

            ejecucion = grupo.removeOne();
            if (ejecucion != null) {
              asignadoTitulo = false;
              for (DesgloseManualEjecucionDTO ejef : desgloseManualDTO.getRepartoEjecuciones()) {
                if (ejecucion.getNuejecuc().trim().equals(ejef.getNuejecuc().trim())
                    && (StringUtils.isBlank(ejecucion.getCdoperacionecc())
                        && StringUtils.isBlank(ejef.getCdoperacionecc()) || ejecucion.getCdoperacionecc() != null
                        && ejef.getCdoperacionecc() != null
                        && ejecucion.getCdoperacionecc().trim().equals(ejef.getCdoperacionecc().trim()))) {
                  ejef.setImtitulos(ejef.getImtitulos().add(ejecucion.getImtitulos()));
                  asignadoTitulo = true;
                  break;
                }
              }
              if (!asignadoTitulo) {
                desgloseManualDTO.getRepartoEjecuciones().add(ejecucion);

              }
            }

            if (desgloseManualDTO.isTerminado()) {
              break;
            }
          }

          if (!desgloseManualDTO.isTerminado()
              && titulosAntesRepartoOneToOne.compareTo(desgloseManualDTO.getTitulosRepartidos()) == 0) {
            throw new SIBBACBusinessException(String.format(
                "INCIDENCIA-No hay titulos disponibles para el desglose posicion %s dentro del alo.",
                altas.indexOf(desgloseManualDTO)));
          }
        }
      }
    }

    this.mapearRepartoAndPersist(alo, solicitudDesgloseManual, config);
    LOG.debug("[desgloseAlgoritmoProportional] fin. TipoDesglose: {} user: {}", config.getTipoDesgloseManual(),
        config.getAuditUser());
  }

  private void desgloseAlgoritmoAgrupandoPorClearingInformation(Tmct0alo alo,
      SolicitudDesgloseManual solicitudDesgloseManual, List<DesgloseManualEjecucionDTO> listTitulosDisponibles,
      DesgloseConfigDTO config) throws SIBBACBusinessException, SettlementChainDataException {

    String key;

    List<DesgloseManualEjecucionDTO> listEjecucionesAgrupadas;
    List<DesgloseManualGrupoEjecucionDTO> listGruposEjecucion;
    // List<DesgloseManualDTO> listAltas;

    List<DesgloseManualDTO> listAltas = solicitudDesgloseManual.getAltas();
    Map<String, List<DesgloseManualEjecucionDTO>> mapDesglosesAgrupadosClearing = new HashMap<String, List<DesgloseManualEjecucionDTO>>();

    Map<String, List<DesgloseManualGrupoEjecucionDTO>> mapGruposEjecucionAgrupadosClearing = new HashMap<String, List<DesgloseManualGrupoEjecucionDTO>>();

    for (DesgloseManualEjecucionDTO desglose : listTitulosDisponibles) {
      key = String.format("%s_%s_%s", StringUtils.trim(desglose.getCuentaCompensacionDestino()),
          StringUtils.trim(desglose.getMiembroCompensadorDestino()),
          StringUtils.trim(desglose.getCodigoReferenciaAsignacion()));

      if (mapDesglosesAgrupadosClearing.get(key) == null) {
        mapDesglosesAgrupadosClearing.put(key, new ArrayList<DesgloseManualEjecucionDTO>());
      }
      mapDesglosesAgrupadosClearing.get(key).add(desglose);
    }

    if (listAltas.size() == 1) {
      listAltas = getAltasFromMapAgrupado(listAltas.get(0), mapDesglosesAgrupadosClearing);
      solicitudDesgloseManual.getAltas().clear();
      solicitudDesgloseManual.getAltas().addAll(listAltas);
      if (CollectionUtils.isNotEmpty(listAltas)) {
        this.mapearRepartoAndPersist(alo, solicitudDesgloseManual, config);
      }
    }
    else if (listAltas.size() > 1) {
      if (listAltas.size() < mapDesglosesAgrupadosClearing.size()) {
        throw new SIBBACBusinessException(
            "INCIDENCIA-La cantidad de desgloses por Clearing Information debe ser mayor o igual al de agrupaciones por Clearing Information");
      }
      else {

        for (String keyMap : mapDesglosesAgrupadosClearing.keySet()) {
          listEjecucionesAgrupadas = mapDesglosesAgrupadosClearing.get(keyMap);
          listGruposEjecucion = this.getGruposEjecucion(listEjecucionesAgrupadas);
          if (mapGruposEjecucionAgrupadosClearing.get(keyMap) == null) {
            mapGruposEjecucionAgrupadosClearing.put(keyMap, new ArrayList<DesgloseManualGrupoEjecucionDTO>());
          }
          mapGruposEjecucionAgrupadosClearing.get(keyMap).addAll(listGruposEjecucion);
        }

        for (DesgloseManualDTO alta : listAltas) {
          if (StringUtils.isBlank(alta.getClearingAccountCodeFrom())
              && StringUtils.isBlank(alta.getClearingMemberCodeFrom())
              && StringUtils.isBlank(alta.getGiveUpReferenceFrom())) {
            throw new SIBBACBusinessException(
                "INCIDENCIA-La Clearing Information debe ser informada para un desglose agrupado por esta con mas de un desglose.");
          }
          key = String.format("%s_%s_%s", StringUtils.trim(alta.getClearingAccountCodeFrom()),
              StringUtils.trim(alta.getClearingMemberCodeFrom()), StringUtils.trim(alta.getGiveUpReferenceFrom()));

          listGruposEjecucion = mapGruposEjecucionAgrupadosClearing.get(key);

          if (CollectionUtils.isEmpty(listGruposEjecucion)) {
            throw new SIBBACBusinessException(
                String
                    .format(
                        "INCIDENCIA-No encontrados titulos para Clearing Information Cta: '%s' Miembro: '%s' Ref.Give-up: '%s'.",
                        StringUtils.trim(alta.getClearingAccountCodeFrom()),
                        StringUtils.trim(alta.getClearingMemberCodeFrom()),
                        StringUtils.trim(alta.getGiveUpReferenceFrom())));
          }

          this.removeEmptyGrupos(listGruposEjecucion);

          this.sortFewestGrupos(listGruposEjecucion);
          // CON TITULOS PASADOS POR USUARIO
          if (alta.getTitulos() != null && alta.getTitulos().compareTo(BigDecimal.ZERO) > 0) {
            for (DesgloseManualGrupoEjecucionDTO grupo : listGruposEjecucion) {
              alta.getRepartoEjecuciones().addAll(grupo.remove(alta.getTitulos()));

              this.removeEmptyGrupos(listGruposEjecucion);
              this.sortFewestGrupos(listGruposEjecucion);
              if (alta.isTerminado()) {
                break;
              }
            }

          }
          else {
            alta.getRepartoEjecuciones().addAll(mapDesglosesAgrupadosClearing.get(key));
            alta.setTitulos(alta.getTitulosRepartidos());
          }
          if (!alta.isTerminado()) {
            throw new SIBBACBusinessException(
                String
                    .format(
                        "INCIDENCIA-No encontrados titulos suficientes para Clearing Information Cta: '%s' Miembro: '%s' Ref.Give-up: '%s'.",
                        StringUtils.trim(alta.getClearingAccountCodeFrom()),
                        StringUtils.trim(alta.getClearingMemberCodeFrom()),
                        StringUtils.trim(alta.getGiveUpReferenceFrom())));
          }
        }
        this.mapearRepartoAndPersist(alo, solicitudDesgloseManual, config);
      }
    }
  }

  private List<DesgloseManualDTO> getAltasFromMapAgrupado(DesgloseManualDTO alta,
      Map<String, List<DesgloseManualEjecucionDTO>> mapDesglosesAgrupados) {
    BigDecimal titulos;
    DesgloseManualDTO altaAgrupado;
    List<DesgloseManualEjecucionDTO> listAgrupados;
    List<DesgloseManualDTO> listAltas = new ArrayList<DesgloseManualDTO>();
    for (String keyMap : mapDesglosesAgrupados.keySet()) {
      listAgrupados = mapDesglosesAgrupados.get(keyMap);
      if (CollectionUtils.isNotEmpty(listAgrupados)) {
        titulos = BigDecimal.ZERO;
        for (DesgloseManualEjecucionDTO desgloseEjecucion : listAgrupados) {
          titulos = titulos.add(desgloseEjecucion.getImtitulos());
        }
        altaAgrupado = new DesgloseManualDTO(alta);
        altaAgrupado.getRepartoEjecuciones().clear();
        altaAgrupado.getRepartoEjecuciones().addAll(listAgrupados);
        altaAgrupado.setTitulos(titulos);
        listAltas.add(altaAgrupado);
      }
    }
    return listAltas;
  }

  private void desgloseAlgoritmoAgrupandoPorPrecio(Tmct0alo alo, SolicitudDesgloseManual solicitudDesgloseManual,
      List<DesgloseManualEjecucionDTO> listTitulosDisponibles, DesgloseConfigDTO config)
      throws SIBBACBusinessException, SettlementChainDataException {

    String key;

    List<DesgloseManualEjecucionDTO> listEjecucionesAgrupadas;
    List<DesgloseManualGrupoEjecucionDTO> listGruposEjecucion;

    List<DesgloseManualDTO> listAltas = solicitudDesgloseManual.getAltas();
    // List<DesgloseManualDTO> listAltas;
    Map<String, List<DesgloseManualEjecucionDTO>> mapDesglosesAgrupadosClearing = new HashMap<String, List<DesgloseManualEjecucionDTO>>();

    Map<String, List<DesgloseManualGrupoEjecucionDTO>> mapGruposEjecucionAgrupadosClearing = new HashMap<String, List<DesgloseManualGrupoEjecucionDTO>>();

    for (DesgloseManualEjecucionDTO desglose : listTitulosDisponibles) {
      key = desglose.getImprecio().setScale(8, RoundingMode.HALF_UP).toString();

      if (mapDesglosesAgrupadosClearing.get(key) == null) {
        mapDesglosesAgrupadosClearing.put(key, new ArrayList<DesgloseManualEjecucionDTO>());
      }
      mapDesglosesAgrupadosClearing.get(key).add(desglose);
    }

    if (listAltas.size() == 1) {
      listAltas = getAltasFromMapAgrupado(listAltas.get(0), mapDesglosesAgrupadosClearing);
      if (CollectionUtils.isNotEmpty(listAltas)) {
        solicitudDesgloseManual.getAltas().clear();
        solicitudDesgloseManual.getAltas().addAll(listAltas);
        this.mapearRepartoAndPersist(alo, solicitudDesgloseManual, config);
      }
    }
    else if (listAltas.size() > 1) {
      if (listAltas.size() < mapDesglosesAgrupadosClearing.size()) {
        throw new SIBBACBusinessException(
            "INCIDENCIA-La cantidad de desgloses por Clearing Information debe ser mayor o igual al de agrupaciones por Clearing Information");
      }
      else {

        for (String keyMap : mapDesglosesAgrupadosClearing.keySet()) {
          listEjecucionesAgrupadas = mapDesglosesAgrupadosClearing.get(keyMap);
          listGruposEjecucion = this.getGruposEjecucion(listEjecucionesAgrupadas);
          if (mapGruposEjecucionAgrupadosClearing.get(keyMap) == null) {
            mapGruposEjecucionAgrupadosClearing.put(keyMap, new ArrayList<DesgloseManualGrupoEjecucionDTO>());
          }
          mapGruposEjecucionAgrupadosClearing.get(keyMap).addAll(listGruposEjecucion);
        }

        for (DesgloseManualDTO alta : listAltas) {
          if (alta.getPrecioFrom() == null || alta.getPrecioFrom().compareTo(BigDecimal.ZERO) == 0) {
            throw new SIBBACBusinessException(
                "INCIDENCIA-El precio debe informarse en un desglose por precio con mas de un desglose.");
          }
          key = alta.getPrecioFrom().setScale(8, RoundingMode.HALF_UP).toString();

          listGruposEjecucion = mapGruposEjecucionAgrupadosClearing.get(key);

          if (CollectionUtils.isEmpty(listGruposEjecucion)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA-No hay titulos para el precio '%s'",
                alta.getPrecioFrom()));
          }

          this.removeEmptyGrupos(listGruposEjecucion);

          this.sortFewestGrupos(listGruposEjecucion);
          // CON TITULOS PASADOS POR USUARIO
          if (alta.getTitulos() != null && alta.getTitulos().compareTo(BigDecimal.ZERO) > 0) {
            for (DesgloseManualGrupoEjecucionDTO grupo : listGruposEjecucion) {
              alta.getRepartoEjecuciones().addAll(grupo.remove(alta.getTitulos()));

              this.removeEmptyGrupos(listGruposEjecucion);
              this.sortFewestGrupos(listGruposEjecucion);
              if (alta.isTerminado()) {
                break;
              }
            }
          }
          else {
            alta.getRepartoEjecuciones().addAll(mapDesglosesAgrupadosClearing.get(key));
            alta.setTitulos(alta.getTitulosRepartidos());
          }
          if (!alta.isTerminado()) {
            throw new SIBBACBusinessException(String.format(
                "INCIDENCIA-No hay titulos suficientes para el precio '%s'", alta.getPrecioFrom()));
          }
        }

        this.mapearRepartoAndPersist(alo, solicitudDesgloseManual, config);
      }
    }
  }

  @SuppressWarnings("unused")
  private void validarEstadosDesgloseManual(Tmct0alo alo, DesgloseConfigDTO config) throws SIBBACBusinessException {
    List<Tmct0alc> listAlcs = alo.getTmct0alcs();
    for (Tmct0alc alc : listAlcs) {
      this.validarEstadosDesgloseManual(alc, config);
    }
  }

  private void validarEstadosDesgloseManual(Tmct0alc alc, DesgloseConfigDTO config) throws SIBBACBusinessException {
    List<Tmct0desglosecamara> listDcs;
    if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
      listDcs = alc.getTmct0desglosecamaras();
      for (Tmct0desglosecamara dc : listDcs) {
        if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          this.validacionEstadosEnvio(dc.getTmct0estadoByCdestadoasig(), alc.getEstadoentrec(),
              dc.getTmct0estadoByCdestadoentrec(), config);
        }
      }

    }
  }

  private void validacionEstadosEnvio(Tmct0estado estadoAsignacion, Tmct0estado estadoS3Cliente,
      Tmct0estado estadoS3Mercado, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[validacionEstadosEnvio] ejecutando validaciones de coherecia de desglose...");
    String msg;

    if (estadoAsignacion == null) {
      msg = "INCIDENCIA-El estado asignacion es obligatorio, no se puede desglosar.";
      LOG.warn("[validacionEstadosEnvio] {}", msg);
      throw new SIBBACBusinessException(msg);
    }

    if (config.getCdestadoasigGtLockUserActivity() != null
        && estadoAsignacion.getIdestado() > config.getCdestadoasigGtLockUserActivity()
        || CollectionUtils.isNotEmpty(config.getListCdestadoasigLockUserActivity())
        && config.getListCdestadoasigLockUserActivity().contains(estadoAsignacion.getIdestado())) {
      msg = String
          .format("INCIDENCIA-El desglose en estado '%s' no permite el desglose.", estadoAsignacion.getNombre());
      LOG.warn("[validacionEstadosEnvio] {}", msg);
      throw new SIBBACBusinessException(msg);
    }
    if (estadoS3Cliente != null) {
      if (config.getCdestadoentrefGtLockUserActivity() != null
          && estadoS3Cliente.getIdestado() > config.getCdestadoentrefGtLockUserActivity()) {
        msg = String.format("INCIDENCIA-Pata cliente en estado '%s' no permite el desglose.",
            estadoS3Cliente.getNombre());
        LOG.warn("[validacionEstadosEnvio] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }

    if (estadoS3Mercado != null) {
      if (config.getCdestadoentrefGtLockUserActivity() != null
          && estadoS3Mercado.getIdestado() > config.getCdestadoentrefGtLockUserActivity()) {
        msg = String.format("INCIDENCIA-Pata mercado en estado '%s' no permite el desglose.",
            estadoS3Mercado.getNombre());
        LOG.warn("[validacionEstadosEnvio] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }

  }

  private List<DesgloseManualEjecucionDTO> getTitulosDisponiblesFromGruposEjecucion(Tmct0alo alo,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[getTitulosDisponiblesFromGruposEjecucion] inicio.");

    List<Tmct0ejecucionalocation> listEjeAlos;
    Tmct0eje eje;
    Tmct0caseoperacioneje opCase;
    Tmct0operacioncdecc op;
    Tmct0CuentasDeCompensacion cuenta;
    DesgloseManualEjecucionDTO desglose;

    List<DesgloseManualEjecucionDTO> res = new ArrayList<DesgloseManualEjecucionDTO>();
    List<Tmct0grupoejecucion> listGrupos = grupoEjecucionBo.findAllByTmct0aloId(alo.getId());
    // List<Tmct0grupoejecucion> listGrupos = alo.getTmct0grupoejecucions();

    BigDecimal titulos = BigDecimal.ZERO;
    for (Tmct0grupoejecucion gr : listGrupos) {
      listEjeAlos = gr.getTmct0ejecucionalocations();
      for (Tmct0ejecucionalocation ealo : listEjeAlos) {
        titulos = titulos.add(ealo.getNutitulos());
        opCase = null;
        op = null;
        cuenta = null;
        eje = ealo.getTmct0eje();

        if (eje.getTmct0caseoperacioneje() != null) {
          opCase = eje.getTmct0caseoperacioneje();
          if (opCase.getTmct0operacioncdecc() != null) {
            op = opCase.getTmct0operacioncdecc();
          }
          if (op.getTmct0CuentasDeCompensacion() != null) {
            cuenta = op.getTmct0CuentasDeCompensacion();
          }
        }

        desglose = new DesgloseManualEjecucionDTO(gr, ealo, eje, opCase, op, cuenta);
        ealo.setNutitulosPendientesDesglose(BigDecimal.ZERO);
        ealo.setAuditFechaCambio(new Date());
        ealo.setAuditUser(config.getAuditUser());
        res.add(desglose);
        LOG.debug("[getTitulosDisponiblesFromGruposEjecucion] recuperado ejealo precio: {} titulos: {} para desglosar",
            eje.getImcbmerc(), ealo.getNutitulos());
      }
    }
    LOG.trace("[getTitulosDisponiblesFromGruposEjecucion] final titulos: {}.", titulos);
    return res;
  }

  private List<DesgloseManualEjecucionDTO> bajaAlcs(List<Tmct0alcId> bajas, DesgloseConfigDTO config)
      throws SIBBACBusinessException {

    String auditUser = config.getAuditUser();
    LOG.debug("[bajaAlcs] inicio user: {}", auditUser);
    Tmct0alc alc;
    List<Tmct0movimientoecc> listMovs;
    List<Tmct0desglosecamara> listDcs;
    List<Tmct0desgloseclitit> listDcts;
    Map<String, DesgloseManualEjecucionDTO> mapTitulosDisponiblesBy = new HashMap<String, DesgloseManualEjecucionDTO>();

    DesgloseManualEjecucionDTO titulosDisponibles;

    String key;

    for (Tmct0alcId alcId : bajas) {
      LOG.debug("[bajaAlcs] leyendo alc para dar de baja alc: {} user: {}", alcId, auditUser);
      alc = alcBo.findById(alcId);
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        this.validarEstadosDesgloseManual(alc, config);
        LOG.debug("[bajaAlcs] dando de baja alc: {} titulos: {} user: {}", alcId, alc.getNutitliq(), auditUser);

        listDcs = alc.getTmct0desglosecamaras();
        for (Tmct0desglosecamara dc : listDcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            LOG.debug("[bajaAlcs] baja dc: {} user: {} camara: {}", dc.getIddesglosecamara(), auditUser, dc
                .getTmct0CamaraCompensacion().getCdCodigo());

            listDcts = dc.getTmct0desgloseclitits();
            for (Tmct0desgloseclitit dct : listDcts) {
              LOG.debug("[bajaAlcs] dando de baja desglose: {} cd.op: {} titulos: {} user: {}", dct.getNudesglose(),
                  dct.getCdoperacionecc(), dct.getImtitulos(), auditUser);
              key = String.format("%s_%s", StringUtils.trim(dct.getNuejecuc()),
                  StringUtils.trim(dct.getCdoperacionecc()));
              titulosDisponibles = mapTitulosDisponiblesBy.get(key);
              if (titulosDisponibles == null) {
                titulosDisponibles = new DesgloseManualEjecucionDTO(dct);

                titulosDisponibles.setFevalor(dc.getFeliquidacion());
                mapTitulosDisponiblesBy.put(key, titulosDisponibles);

              }
              else {
                titulosDisponibles.setImtitulos(dct.getImtitulos().add(titulosDisponibles.getImtitulos()));
                titulosDisponibles.setImefectivo(dct.getImefectivo().add(titulosDisponibles.getImefectivo()));
                titulosDisponibles.setImcanoncompdv(dct.getImcanoncompdv().add(titulosDisponibles.getImcanoncompdv()));
                titulosDisponibles.setImcanoncompeu(dct.getImcanoncompeu().add(titulosDisponibles.getImcanoncompeu()));
                titulosDisponibles.setImcanoncontrdv(dct.getImcanoncontrdv()
                    .add(titulosDisponibles.getImcanoncontrdv()));
                titulosDisponibles.setImcanoncontreu(dct.getImcanoncontreu()
                    .add(titulosDisponibles.getImcanoncontreu()));
                titulosDisponibles.setImcanonliqdv(dct.getImcanonliqdv().add(titulosDisponibles.getImcanonliqdv()));
                titulosDisponibles.setImcanonliqeu(dct.getImcanonliqeu().add(titulosDisponibles.getImcanonliqeu()));

                LOG.debug(
                    "[bajaAlcs] nudesglose: {} acumulando a cd.op: {} nuejecuc: {} titulos: {} tit. acumulados: {} user: {}",
                    dct.getNudesglose(), dct.getCdoperacionecc(), dct.getNuejecuc(), dct.getImtitulos(),
                    titulosDisponibles.getImtitulos(), auditUser);
              }

              LOG.debug("[bajaAlcs] Acumulando titulos por codigo operacion: {} nuejecuc: {} precio: {} titulos: {}",
                  titulosDisponibles.getCdoperacionecc(), titulosDisponibles.getNuejecuc(),
                  titulosDisponibles.getImprecio(), titulosDisponibles.getImtitulos());
            }
            LOG.debug("[bajaAlcs] buscando los movimientos a dar de baja user: {}", auditUser);
            listMovs = dc.getTmct0movimientos();
            for (Tmct0movimientoecc mov : listMovs) {
              if (mov.getCdestadomov() != null && !mov.getCdestadomov().trim().equals("21")) {
                mov.setCdestadomov("21");
                mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
                mov.setAuditFechaCambio(new Date());
                mov.setAuditUser(auditUser);
              }
            }

            dc.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()));
            dc.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId()));
            dc.setAuditUser(auditUser);
            dc.setAuditFechaCambio(new Date());
          }
        }

        alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
        alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId()));
        alc.setCdusuaud(auditUser);
        alc.setFhaudit(new Date());
      }
    }
    List<DesgloseManualEjecucionDTO> listTitulosDisponibles = new ArrayList<DesgloseManualEjecucionDTO>(
        mapTitulosDisponiblesBy.values());

    sortFewest(listTitulosDisponibles);
    mapTitulosDisponiblesBy.clear();
    LOG.debug("[bajaAlcs] fin user: {}", auditUser);
    return listTitulosDisponibles;
  }

  private void desgloseNacionalAutomatico(Tmct0alo alo, SettlementData settlementData, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    LOG.trace("[desgloseNacionalAutomatico] inicio alo: {}", alo.getId());
    List<Tmct0ejecucionalocation> listEjealos = new ArrayList<Tmct0ejecucionalocation>();
    List<Tmct0grupoejecucion> listGrupos = alo.getTmct0grupoejecucions();
    for (Tmct0grupoejecucion gr : listGrupos) {
      listEjealos.addAll(gr.getTmct0ejecucionalocations());
    }

    Tmct0alc alc = crearDesglosesAlcAutomatico(alo, listEjealos, settlementData, config);

    if (alc.getNutitliq().compareTo(BigDecimal.ZERO) <= 0) {
      throw new SIBBACBusinessException("Se ha creado un alc con 0 titulos.");
    }
    alo.getTmct0alcs().add(alc);

    this.postCreateAlcsDesgloseAutomatico(alo, alc, settlementData, config);

    LOG.trace("[desgloseNacionalAutomatico] fin alo: {}", alo.getId());
  }

  private void postCreateAlcsDesgloseAutomatico(Tmct0alo alo, Tmct0alc alc, SettlementData settlementData,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[postCreateAlcsDesgloseAutomatico] inicio");

    LOG.debug("[postCreateAlcsDesgloseAutomatico] vamos a repartir los canones de MTF...");
    this.repartirCanonesMTF(alo, config);

    LOG.debug("[postCreateAlcsDesgloseAutomatico] buscando en tabla tcli0com comisiones DWH...");
    Tcli0comDTO tcli0com = getTcli0com(alo, config);

    LOG.debug("[postCreateAlcsDesgloseAutomatico] revisando informacion de asignacion...");

    this.checkTitulosDesglosados(alo);

    boolean asigOrden = this.isAsignacionOrdenGiveUp(alc) || this.isAsignacionOrdenInterna(alc)
        && this.existsMovimientos(alc);

    if (asigOrden) {
      LOG.debug("[postCreateAlcsDesgloseAutomatico] detectado modelo de negocio: {}...", alo.getTmct0bok()
          .getTmct0ord().getCdmodnego());
      this.ponerEstadoUpToAllo(alc.getTmct0desglosecamaras(),
          EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId(), config.getAuditUser());
    }
    else {
      if (settlementData.getTmct0ilq().getDesglose() == 'D') {
        LOG.debug("[postCreateAlcsDesgloseAutomatico] ilq's en DEFINITIVO...");
        this.revisarEstadoEnvioAsignacionDesglosesVsInfoCompensacion(alo, config);

      }
      else {
        LOG.debug("[postCreateAlcsDesgloseAutomatico] ilq's en PROVISIONAL...");
        this.ponerEstadoUpToAllo(alc.getTmct0desglosecamaras(),
            EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId(), config.getAuditUser());
      }
    }

    if (!settlementData.getListHolder().isEmpty()) {
      LOG.debug("[postCreateAlcsDesgloseAutomatico] grabando titulares nacional...");
      afiBo.grabarAfisSinErroresNacional(alc, settlementData.getListHolder(), config.getAuditUser());
    }

    tablasTitularesBo.crearTablasTitularesDesgloseByHolders(alc, settlementData.getListHolder(), config);

    alcBo.setProvisionalNudesgloses(alc);

    List<Tmct0alc> listAlcs = new ArrayList<Tmct0alc>();
    listAlcs.add(alc);

    LOG.debug("[postCreateAlcsDesgloseAutomatico] calculos economicos alo: {}...", alo.getId());
    CalculosEconomicosNacionalBo calculosNacionalBo = ctx.getBean(CalculosEconomicosNacionalBo.class, alo, listAlcs,
        config.getCanonesConfig(), tcli0com);
    try {
      calculosNacionalBo.calcularDatosEconomicos();
    }
    catch (EconomicDataException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }

    alcBo.removeProvisionalNudesgloses(alc);

    alcBo.persistirAlcsForDesgloseAutomaticoAndManual(alo);

    LOG.trace("[postCreateAlcsDesgloseAutomatico] fin");
  }

  private void postCreateAlcsDesgloseManual(Tmct0alo alo, SolicitudDesgloseManual solicitudDesgloseManual,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    Tmct0alc alc;
    String msg;
    LOG.trace("[postCreateAlcsDesgloseManual] inicio");
    LOG.debug("[postCreateAlcsDesgloseManual] vamos a repartir los canones de MTF...");

    List<DesgloseManualDTO> listDesglosesManuales = solicitudDesgloseManual.getAltas();
    this.repartirCanonesMTF(alo, config);

    LOG.debug("[postCreateAlcsDesgloseManual] buscando en tabla tcli0com comisiones DWH...");
    Tcli0comDTO tcli0com = getTcli0com(alo, config);

    LOG.debug("[postCreateAlcsDesgloseManual] revisando informacion de asignacion...");
    List<Tmct0alc> listAlcs = new ArrayList<Tmct0alc>();
    for (DesgloseManualDTO desgloseManualDTO : listDesglosesManuales) {
      alc = desgloseManualDTO.getTmct0alcCreado();

      msg = String.format("Alc: '%s' - Instrucciones Liquidacion Origen: '%s' en estado  '%s'-'%s'", alc.getId()
          .getNucnfliq(), config.getTipoDesgloseManual(), desgloseManualDTO.getSettlementData().getTmct0ilq()
          .getDesglose(), this.getDescripcionDesgloseIlq(desgloseManualDTO.getSettlementData().getTmct0ilq()
          .getDesglose()));
      LOG.debug("[desglosarNacional] {}", msg);
      logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
          .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());

      if (desgloseManualDTO.getSettlementData().getTmct0ilq().getDesglose() == 'D') {
        this.revisarEstadoEnvioAsignacionDesglosesVsInfoCompensacion(alo, config);
      }
      else {
        this.ponerEstadoUpToAllo(alc.getTmct0desglosecamaras(),
            EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId(), config.getAuditUser());
      }

      if (!desgloseManualDTO.getSettlementData().getListHolder().isEmpty()) {
        LOG.debug("[postCreateAlcsDesgloseManual] grabando titulares nacional...");
        afiBo.grabarAfisSinErroresNacional(alc, desgloseManualDTO.getSettlementData().getListHolder(),
            config.getAuditUser());
      }

      tablasTitularesBo.crearTablasTitularesDesgloseByHolders(alc, desgloseManualDTO.getSettlementData()
          .getListHolder(), config);

      listAlcs.add(alc);
    }

    this.checkTitulosDesglosados(alo);

    for (Tmct0alc alcf : listAlcs) {
      alcBo.setProvisionalNudesgloses(alcf);
    }

    LOG.debug("[postCreateAlcsDesgloseManual] calculos economicos alo: {}...", alo.getId());
    CalculosEconomicosNacionalBo calculosNacionalBo = ctx.getBean(CalculosEconomicosNacionalBo.class, alo, listAlcs,
        config.getCanonesConfig(), tcli0com);
    try {
      calculosNacionalBo.calcularDatosEconomicos();
    }
    catch (EconomicDataException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }

    for (Tmct0alc alcf : listAlcs) {
      alcBo.removeProvisionalNudesgloses(alcf);
    }

    alcBo.persistirAlcsForDesgloseAutomaticoAndManual(alo);

    LOG.trace("[postCreateAlcs] fin");
  }

  private void checkTitulosDesglosados(Tmct0alo alo) throws SIBBACBusinessException {
    BigDecimal titulosDesgloses = BigDecimal.ZERO;
    List<Tmct0alc> listAlcs = alo.getTmct0alcs();
    for (Tmct0alc alc : listAlcs) {
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        titulosDesgloses = titulosDesgloses.add(alc.getNutitliq());
      }
    }
    if (titulosDesgloses.compareTo(alo.getNutitcli()) != 0) {
      throw new SIBBACBusinessException(MessageFormat.format(
          "INCIDENCIA-Titulos alo: {0} titulos desgloses: {1}. Se debe desglosar por completo", alo.getNutitcli(),
          titulosDesgloses));
    }
  }

  private Tcli0comDTO getTcli0com(Tmct0alo alo, DesgloseConfigDTO config) {
    String cdproductNacional = "008";

    LOG.debug("[desgloseNacionalAutomatico] buscando en tabla tcli0com comisiones DWH...");
    Tcli0comDTO tcli0com = null;
    try {
      int cdaliasShort = Integer.parseInt(StringUtils.trim(alo.getTmct0bok().getCdalias()));
      tcli0com = config.getTcli0com(cdproductNacional, cdaliasShort);

    }
    catch (Exception e1) {
      LOG.warn("[desgloseNacionalAutomatico]Inciencia recuperando tcli0com, {}", alo.getTmct0bok().getCdalias(),
          e1.getMessage(), e1);
    }

    if (tcli0com == null) {
      String msg = "No encontrado registro tcli0com 'comision cuenta' para DWH";
      LOG.warn("[desgloseNacionalAutomatico] {} - {} - {}", alo.getId(), alo.getCdalias(), msg);
      logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
          .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
      tcli0com = new Tcli0comDTO("", 0, BigDecimal.ZERO, BigDecimal.ZERO);
    }

    return tcli0com;
  }

  private boolean tieneAloMovimientosAceptados(Tmct0alo alo) {

    BigDecimal sumTitulos = BigDecimal.ZERO;
    List<Tmct0movimientoecc> listMov = movBo.findByTmct0aloId(alo.getId());
    if (listMov != null)
      for (Tmct0movimientoecc tmct0movimientoecc : listMov) {
        if (tmct0movimientoecc.getCdestadomov() != null
            && !tmct0movimientoecc.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO_INT.text)
            && !tmct0movimientoecc.getCdestadomov().trim().equals(EstadosEccMovimiento.RECHAZADO_ECC.text)
            && !tmct0movimientoecc.getCdestadomov().trim().equals(EstadosEccMovimiento.CANCELADO.text)) {
          sumTitulos = sumTitulos.add(tmct0movimientoecc.getImtitulos());
        }
      }
    return alo.getNutitcli().compareTo(sumTitulos) == 0;
  }

  private void revisarEstadoEnvioAsignacionDesglosesVsInfoCompensacion(Tmct0alo alo, DesgloseConfigDTO config) {
    LOG.trace("[revisarEstadoEnvioAsignacionDesglosesVsInfoCompensacion] inicio");
    Tmct0infocompensacion ic;
    List<Tmct0alc> listAlcs = alo.getTmct0alcs();

    List<Tmct0desglosecamara> listDcs;
    List<Tmct0desgloseclitit> listDcts;
    Integer estado;
    String indPosicionSt;
    boolean hasMovimientosDisiciacion = this.tieneAloMovimientosAceptados(alo);
    for (Tmct0alc alc : listAlcs) {
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        listDcs = alc.getTmct0desglosecamaras();
        for (Tmct0desglosecamara dc : listDcs) {

          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {

            if (hasMovimientosDisiciacion) {
              estado = EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId();
            }
            else {
              estado = null;
              ic = dc.getTmct0infocompensacion();
              listDcts = dc.getTmct0desgloseclitits();
              if (alo.getTmct0bok().getCasepti() == 'S') {
                dctBo.recuperarAnotaciones(alc.getId(), listDcts, config.getAuditUser());
              }
              for (Tmct0desgloseclitit dct : listDcts) {
                if (config.isCdbrokerMtf(dct.getCdPlataformaNegociacion())
                    && !StringUtils.equals(dct.getCuentaCompensacionDestino(), ic.getCdctacompensacion())) {
                  estado = EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId();
                  break;
                }
                else {
                  if (StringUtils.isNotBlank(dct.getCdoperacionecc()) && dct.getCdoperacionecc().trim().length() == 16) {
                    indPosicionSt = StringUtils.substring(dct.getCdoperacionecc(), 15, 16);
                    if (StringUtils.isNotBlank(indPosicionSt)) {
                      char indPosicion = indPosicionSt.charAt(0);
                      // OPEN
                      if (indPosicion == 'O'
                          && !StringUtils.equals(dct.getCuentaCompensacionDestino(), ic.getCdctacompensacion())) {
                        estado = EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId();
                        break;
                      }
                    }
                  }
                }
              } // fin for dcts

              if (estado == null) {
                for (Tmct0desgloseclitit dct : listDcts) {
                  if (config.isCdbrokerMtf(dct.getCdPlataformaNegociacion())
                      && !StringUtils.equals(dct.getCuentaCompensacionDestino(), ic.getCdctacompensacion())) {
                    estado = EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId();
                    break;
                  }
                  else {
                    if (StringUtils.isNotBlank(dct.getCdoperacionecc())
                        && dct.getCdoperacionecc().trim().length() == 16) {
                      indPosicionSt = StringUtils.substring(dct.getCdoperacionecc(), 15, 16);
                      if (StringUtils.isNotBlank(indPosicionSt)) {
                        char indPosicion = indPosicionSt.charAt(0);
                        // OPEN
                        if (indPosicion == 'O'
                            && !StringUtils.equals(dct.getCuentaCompensacionDestino(), ic.getCdctacompensacion())) {

                        }
                        else {
                          estado = EstadosEnumerados.ASIGNACIONES.PDTE_REVISAR_USUARIO.getId();
                          break;
                        }
                      }

                    }
                  }
                } // fin for dcts
              }
            }
            if (estado == null) {
              estado = EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId();
            }
            dcBo.ponerEstadoAsignacionUpToAllo(dc, estado, config.getAuditUser());
          }// if
        } // fin for dc
      } // if
    }// fin for alc
    LOG.trace("[revisarEstadoEnvioAsignacionDesglosesVsInfoCompensacion] fin");
  }

  private void repartirCanonesMTF(Tmct0alo alo, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[repartirCanonesMTF] inicio alo: {}", alo.getId());
    List<Tmct0alc> listAlcs = alo.getTmct0alcs();
    List<Tmct0desglosecamara> listDcs;
    List<Tmct0desgloseclitit> listDcts;
    Tmct0ejeId ejeId;
    Tmct0eje eje;
    String msg;
    List<Tmct0eje> listEjes = alo.getTmct0bok().getTmct0ejes();
    for (Tmct0alc alc : listAlcs) {
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        listDcs = alc.getTmct0desglosecamaras();
        for (Tmct0desglosecamara dc : listDcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            listDcts = dc.getTmct0desgloseclitits();
            for (Tmct0desgloseclitit dct : listDcts) {
              if (config.isCdbrokerMtf(dct.getCdPlataformaNegociacion())) {
                ejeId = new Tmct0ejeId(alo.getId().getNuorden(), alo.getId().getNbooking(), dct.getNuejecuc());
                eje = this.findTmct0eje(listEjes, ejeId);
                if (eje == null) {
                  msg = String.format("Ejecucion no encontrada desglose MTF [%s-%s-%s]", alo.getId().getNuorden(), alo
                      .getId().getNbooking(), dct.getNuejecuc());
                  throw new SIBBACBusinessException(msg);
                }
                this.repartirCanonesMTF(eje, dct);
              }
            }
          }
        }
      }
    }
    LOG.debug("[repartirCanonesMTF] fin alo: {}", alo.getId());
  }

  private void repartirCanonesMTF(Tmct0eje eje, Tmct0desgloseclitit dct) {
    LOG.trace("[repartirCanonesMTF] inicio eje: {} en desglose: {}", eje.getId(), dct.getNudesglose());
    BigDecimal porcentaje = dct.getImtitulos().divide(eje.getNutiteje(), 16, RoundingMode.HALF_UP);

    LOG.trace("[repartirCanonesMTF] reparto porcentaje == {}", porcentaje);
    if (eje.getImCanonContrDv() != null) {
      dct.setImcanoncontrdv(eje.getImCanonContrDv().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP));
    }
    if (eje.getImCanonContrEu() != null) {
      dct.setImcanoncontreu(eje.getImCanonContrEu().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP));
    }

    if (eje.getImCanonCompDv() != null) {
      dct.setImcanoncompdv(eje.getImCanonCompDv().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP));
    }

    if (eje.getImCanonCompEu() != null) {
      dct.setImcanoncompeu(eje.getImCanonCompEu().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP));
    }

    if (eje.getImCanonLiqDv() != null) {
      dct.setImcanonliqdv(eje.getImCanonLiqDv().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP));
    }

    if (eje.getImCanonLiqEu() != null) {
      dct.setImcanonliqeu(eje.getImCanonLiqEu().multiply(porcentaje).setScale(2, RoundingMode.HALF_UP));
    }
    LOG.trace("[repartirCanonesMTF] inicio eje: {} en desglose: {}", eje.getId(), dct.getNudesglose());
  }

  private Tmct0eje findTmct0eje(List<Tmct0eje> listEjes, Tmct0ejeId id) {
    Tmct0eje eje = null;
    for (Tmct0eje tmct0eje : listEjes) {
      if (tmct0eje.getId().equals(id)) {
        eje = tmct0eje;
        break;
      }
    }
    return eje;
  }

  private void ponerEstadoUpToAllo(List<Tmct0desglosecamara> listDcs, int estado, String auditUser) {
    for (Tmct0desglosecamara dc : listDcs) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        dcBo.ponerEstadoAsignacionUpToAllo(dc, estado, auditUser);
      }
    }
  }

  private Tmct0alc crearDesglosesAlcAutomatico(Tmct0alo alo, List<Tmct0ejecucionalocation> ejealos,
      SettlementData settlementData, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[crearDesglosesAlcAutomatico] inicio");
    Tmct0eje ejeFor;
    Tmct0desglosecamara dc;
    Tmct0infocompensacion ic;
    Tmct0desgloseclitit dt;
    Tmct0movimientoecc mov;
    Tmct0caseoperacioneje caseOpEje;
    Tmct0operacioncdecc opCdEcc;
    List<Tmct0movimientoecc> listMovs;
    String msg = "Creado Movimiento, destino parametrizacion igual que origen";

    Tmct0alc alc = new Tmct0alc();

    Character corretajePtiAlias = config.getCorretajePtiAlias(alo.getCdalias());
    Character corretajePtiCompensador;
    LOG.debug("[crearDesglosesAlcAutomatico] buscando nueva secuencia para alc...");
    Short nucnfliq = this.getMaxNucnfliqAdd1(alo.getId());
    LOG.debug("[crearDesglosesAlcAutomatico] secuencia para alc: {}", nucnfliq);
    alc.setId(new Tmct0alcId(alo.getId(), nucnfliq));
    Tmct0bok bok = alo.getTmct0bok();
    Tmct0ord ord = bok.getTmct0ord();
    List<Tmct0eje> ejes = bok.getTmct0ejes();
    alc.setTmct0alo(alo);
    LOG.debug("[crearDesglosesAlcAutomatico] inicializando nuevo alc...");
    desgloseAlcHelper.inizializeAlc(ord, bok, alo, ejes, alc, settlementData);
    ic = new Tmct0infocompensacion();
    ic.setCdctacompensacion("");
    ic.setCdmiembrocompensador("");
    ic.setCdrefasignacion("");
    ic.setCdnmotecnico("");
    ic.setAuditFechaCambio(new Date());
    ic.setAuditUser(config.getAuditUser());
    icDao.save(ic);
    alc.setIdinfocomp(ic.getIdinfocomp());
    alc.setFhaudit(new Date());
    alc.setCdusuaud(config.getAuditUser());
    Map<String, Tmct0desglosecamara> desglosesCamara = new HashMap<String, Tmct0desglosecamara>();

    LOG.debug("[crearDesglosesAlcAutomatico] calculando si es ROUTING...");
    boolean isRouting = ordBo.isRouting(ord);
    boolean isScriptDividend = ordBo.isScriptDividend(ord);
    LOG.debug("[crearDesglosesAlcAutomatico] ROUTING == {}", isRouting);
    alc.setNutitliq(BigDecimal.ZERO);
    for (Tmct0ejecucionalocation ejeAloFor : ejealos) {

      LOG.debug("[crearDesglosesAlcAutomatico] creando desglose para ejecucionalocation id: {}",
          ejeAloFor.getIdejecucionalocation());
      ejeFor = ejeAloFor.getTmct0eje();

      caseOpEje = ejeFor.getTmct0caseoperacioneje();
      opCdEcc = null;
      if (caseOpEje != null) {
        LOG.debug("[crearDesglosesAlcAutomatico] encontrado case id: {}", caseOpEje.getIdcase());
        opCdEcc = caseOpEje.getTmct0operacioncdecc();
      }
      LOG.debug("[crearDesglosesAlcAutomatico] buscando desglose ya creado para camara: {}",
          ejeFor.getClearingplatform());
      dc = desglosesCamara.get(ejeFor.getClearingplatform());
      if (dc == null) {
        LOG.debug("[crearDesglosesAlcAutomatico] Crando nuevo desglose camara...");
        dc = this.crearNuevoDesgloseCamara(ord, bok, alo, alc, ejeAloFor, ejeFor, config);
        dc.setTmct0alc(alc);
        ic = dc.getTmct0infocompensacion();
        if (StringUtils.isNotBlank(ic.getCdmiembrocompensador())) {
          corretajePtiCompensador = config.getCorretajePtiCompensador(ic.getCdmiembrocompensador());
        }
        else {
          corretajePtiCompensador = ' ';
        }
        LOG.debug("[crearDesglosesAlcAutomatico] Calculando el tipo envio de corretaje...");
        alcBo.setTipoEnvioCorretaje(alc, isRouting, isScriptDividend, ic.getCdctacompensacion(),
            ic.getCdmiembrocompensador(), corretajePtiAlias, corretajePtiCompensador);
        desglosesCamara.put(ejeFor.getClearingplatform(), dc);
        alc.getTmct0desglosecamaras().add(dc);
      }
      else {
        LOG.debug("[crearDesglosesAlcAutomatico] Acumulando a desglose camara ya creado...");
        dc.setNutitulos(dc.getNutitulos().add(ejeAloFor.getNutitulosPendientesDesglose()));
      }

      LOG.debug("[crearDesglosesAlcAutomatico] Creando nuevo desgloseclitit...");
      dt = new Tmct0desgloseclitit();
      dt.setTmct0desglosecamara(dc);

      desgloseAlcHelper.inizializeDesgloseClitit(dt, ejeAloFor.getTmct0grupoejecucion(), ejeAloFor, ejeFor, caseOpEje,
          opCdEcc);
      dt.setMtf(config.isCdbrokerMtf(dt.getCdPlataformaNegociacion()));
      dc.getTmct0desgloseclitits().add(dt);

      ejeAloFor.setNutitulosPendientesDesglose(BigDecimal.ZERO);
      alc.setNutitliq(alc.getNutitliq().add(dt.getImtitulos()));
      if (dc.getAsigorden() == 'S') {
        dt.setCuentaCompensacionDestino(dc.getTmct0infocompensacion().getCdctacompensacion());
        dt.setMiembroCompensadorDestino(dc.getTmct0infocompensacion().getCdmiembrocompensador());
        dt.setCodigoReferenciaAsignacion(dc.getTmct0infocompensacion().getCdrefasignacion());
      }

      if (alo.getTmct0bok().getCasepti() == 'S' && this.isAsignacionOrdenInterna(dc)) {
        mov = movBo.crearMovimientosOrigenDestinoIgual(dc, dt, ic, config.getAuditUser());
        if (mov != null) {
          listMovs = new ArrayList<Tmct0movimientoecc>();
          listMovs.add(mov);
          movBo.escribirTmct0logForMovimientos(alc.getId(), listMovs, msg, msg, config.getAuditUser());
          listMovs.clear();
        }
      }
    }
    LOG.trace("[crearDesglosesAlcAutomatico] fin");
    return alc;
  }

  private boolean existsMovimientos(Tmct0alc alc) {
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara tmct0desglosecamara : listDcs) {
      if (tmct0desglosecamara.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
          .getId() && !tmct0desglosecamara.getTmct0movimientos().isEmpty()) {
        return true;
      }
    }
    return false;
  }

  private boolean isAsignacionOrdenInterna(Tmct0alc alc) {
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara tmct0desglosecamara : listDcs) {
      if (tmct0desglosecamara.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
          .getId() && this.isAsignacionOrdenInterna(tmct0desglosecamara)) {
        return true;
      }
    }
    return false;
  }

  private boolean isAsignacionOrdenGiveUp(Tmct0alc alc) {
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara tmct0desglosecamara : listDcs) {
      if (tmct0desglosecamara.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
          .getId() && this.isAsignacionOrdenGiveUp(tmct0desglosecamara)) {
        return true;
      }
    }
    return false;
  }

  private boolean isAsignacionOrdenGiveUp(Tmct0desglosecamara dc) {
    boolean res = dc.getAsigorden() == 'S' && dc.getTmct0infocompensacion().getCdmiembrocompensador() != null
        && !dc.getTmct0infocompensacion().getCdmiembrocompensador().trim().isEmpty()
        && dc.getTmct0infocompensacion().getCdrefasignacion() != null
        && !dc.getTmct0infocompensacion().getCdrefasignacion().trim().isEmpty();

    return res;
  }

  private boolean isAsignacionOrdenInterna(Tmct0desglosecamara dc) {
    boolean res = dc.getAsigorden() == 'S' && dc.getTmct0infocompensacion().getCdctacompensacion() != null
        && !dc.getTmct0infocompensacion().getCdctacompensacion().trim().isEmpty();

    return res;
  }

  private Tmct0alc crearNuevoAlc(Tmct0alo alo, short nucnfliq, SettlementData settlementData, String auditUser) {
    Tmct0alc alc = new Tmct0alc();

    LOG.debug("[crearDesglosesAlcAutomatico] buscando nueva secuencia para alc...");
    LOG.debug("[crearDesglosesAlcAutomatico] secuencia para alc: {}", nucnfliq);
    alc.setId(new Tmct0alcId(alo.getId(), nucnfliq));
    Tmct0bok bok = alo.getTmct0bok();
    Tmct0ord ord = bok.getTmct0ord();
    List<Tmct0eje> ejes = bok.getTmct0ejes();
    alc.setTmct0alo(alo);
    LOG.debug("[crearDesglosesAlcAutomatico] inicializando nuevo alc...");
    desgloseAlcHelper.inizializeAlc(ord, bok, alo, ejes, alc, settlementData);
    Tmct0infocompensacion ic = new Tmct0infocompensacion();
    ic.setCdctacompensacion("");
    ic.setCdmiembrocompensador("");
    ic.setCdrefasignacion("");
    ic.setCdnmotecnico("");
    ic.setAuditFechaCambio(new Date());
    ic.setAuditUser(auditUser);
    icDao.save(ic);
    alc.setIdinfocomp(ic.getIdinfocomp());
    alc.setFhaudit(new Date());
    alc.setCdusuaud(auditUser);
    alc.setTmct0infocompensacion(ic);
    return alc;
  }

  private Tmct0alc crearNuevoAlcManual(Tmct0alo alo, DesgloseManualDTO desglose, short nucnfliq, String auditUser) {
    Tmct0alc alc = this.crearNuevoAlc(alo, nucnfliq, desglose.getSettlementData(), auditUser);
    Tmct0infocompensacion ic = alc.getTmct0infocompensacion();
    if (desglose.getClearingRuleTo() != null) {
      ic.setTmct0Regla(new Tmct0Regla());
      ic.getTmct0Regla().setIdRegla(desglose.getClearingRuleTo());
    }

    if (desglose.getClearingAccountCodeTo() != null) {
      ic.setCdctacompensacion(desglose.getClearingAccountCodeTo());
    }
    if (desglose.getClearingMemberCodeTo() != null) {
      ic.setCdmiembrocompensador(desglose.getClearingMemberCodeTo());
    }
    if (desglose.getGiveUpReferenceTo() != null) {
      ic.setCdrefasignacion(desglose.getGiveUpReferenceTo());
    }

    if (desglose.getBankComission() != null) {
      alc.setImcombco(desglose.getBankComission());
    }
    if (desglose.getBankComissionPercent() != null) {
      alc.setPccombco(desglose.getBankComissionPercent());
    }

    if (desglose.getPayerComission() != null) {
      alc.setImcombrk(desglose.getPayerComission());
    }
    if (desglose.getPayerComissionPercent() != null) {
      alc.setPccombrk(desglose.getPayerComissionPercent());
    }

    if (desglose.getBankReference() != null) {
      alc.setCdrefban(desglose.getBankReference());
    }
    if (desglose.getAccountAtClearer() != null) {
      alc.setAcctatcle(desglose.getAccountAtClearer());
    }
    return alc;
  }

  private Tmct0desglosecamara crearNuevoDesgloseCamara(Tmct0ord ord, Tmct0bok bok, Tmct0alo alo, Tmct0alc alc,
      Tmct0ejecucionalocation ejealo, Tmct0eje ejeFor, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[crearNuevoDesgloseCamara] inicio");
    Tmct0desglosecamara dc = new Tmct0desglosecamara();
    desgloseAlcHelper.inizializeDesgloseCamara(bok, ejealo, ejeFor, dc);
    dc.setTmct0alc(alc);
    this.asignarInformacionAsignacion(ord, alo, dc, ejeFor, config);
    dc.setAuditUser(config.getAuditUser());
    dc.setAuditFechaCambio(new Date());
    LOG.trace("[crearNuevoDesgloseCamara] fin");
    return dc;

  }

  private Tmct0desglosecamara crearNuevoDesgloseCamaraManual(Tmct0ord ord, Tmct0bok bok, Tmct0alo alo,
      DesgloseManualEjecucionDTO desglose, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[crearNuevoDesgloseCamaraManual] inicio");
    Tmct0desglosecamara dc = new Tmct0desglosecamara();

    dc.setAsigorden('N');
    dc.setAuditFechaCambio(new Date());
    dc.setAuditUser(config.getAuditUser());
    dc.setCasepti(bok.getCasepti());
    dc.setEnvios3('N');
    dc.setFecontratacion(desglose.getFeejecuc());
    dc.setFeliquidacion(desglose.getFevalor());
    dc.setImputacionpropia('N');
    dc.setIsin(desglose.getCdisin());
    dc.setNutitulos(desglose.getImtitulos());
    // ejealo.setNutitulosPendientesDesglose(BigDecimal.ZERO);
    dc.setSentido(desglose.getCdsentido());
    Tmct0CamaraCompensacion camara = camaraCompensacionBo.findByCdCodigo(StringUtils.trim(desglose.getCdcamara()));
    if (camara == null) {
      throw new SIBBACBusinessException(String.format("No encontrada la camara '%s'", desglose.getCdcamara()));
    }
    dc.setTmct0CamaraCompensacion(camara);
    dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
    dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId());
    dc.setTmct0estadoByCdestadocont(new Tmct0estado());
    dc.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
    dc.setTmct0estadoByCdestadoentrec(new Tmct0estado());
    dc.getTmct0estadoByCdestadoentrec().setIdestado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
    dc.setTmct0estadoByCdestadotit(new Tmct0estado());
    dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());
    LOG.trace("[crearNuevoDesgloseCamaraManual] fin");
    return dc;
  }

  private void asignarInformacionAsignacion(Tmct0ord ord, Tmct0alo alo, Tmct0desglosecamara dc, Tmct0eje ejeFor,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[asignarInformacionAsignacion] inicio");

    String msg;
    Tmct0parametrizacionDTO parametrizacion;
    Tmct0infocompensacion ic = new Tmct0infocompensacion();

    if (dc.getTmct0CamaraCompensacion() == null || StringUtils.isBlank(dc.getTmct0CamaraCompensacion().getCdCodigo())) {
      throw new SIBBACBusinessException("Camara de compensacion no informada");
    }

    if (dc.getAsigorden() == 'S') {
      msg = String
          .format(
              "Alc: '%s' - Asignado desde la orden alias: '%s' camara: '%s' mod.negocio: '%s' cta: '%s' miembro: '%s' ref.give-up: '%s'",
              dc.getTmct0alc().getId().getNucnfliq(), StringUtils.trim(alo.getCdalias()),
              StringUtils.trim(ejeFor.getClearingplatform()), StringUtils.trim(ord.getCdmodnego()),
              StringUtils.trim(ejeFor.getCtacomp()), StringUtils.trim(ejeFor.getMiembrocomp()),
              StringUtils.trim(ejeFor.getRefasignacion()));
      LOG.debug("[crearNuevoDesgloseCamara] {}", msg);
      logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
          .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
      ic.setCdctacompensacion(ejeFor.getCtacomp());
      ic.setCdmiembrocompensador(ejeFor.getMiembrocomp());
      ic.setCdrefasignacion(ejeFor.getRefasignacion());
    }
    else {
      parametrizacion = config.getParametrizacionAsignacion(alo, ejeFor.getClearingplatform());
      if (parametrizacion != null) {
        this.asignarParametrizacion(alo, dc, parametrizacion, ic, config);
      }
      else {
        msg = String.format("Alc: '%s' - No encontrada parametrizacion para el alias '%s' camara: '%s'", dc
            .getTmct0alc().getId().getNucnfliq(), StringUtils.trim(alo.getCdalias()),
            StringUtils.trim(ejeFor.getClearingplatform()));
        LOG.debug("[crearNuevoDesgloseCamara] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }

    ic.setAuditUser(config.getAuditUser());
    ic.setAuditFechaCambio(new Date());
    dc.setTmct0infocompensacion(ic);

    LOG.trace("[asignarInformacionAsignacion] fin");
  }

  private void asignarInformacionAsignacionDesgloseManual(Tmct0ord ord, Tmct0alo alo, Tmct0desglosecamara dc,
      DesgloseManualDTO desglose, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[asignarInformacionAsignacionDesgloseManual] inicio");
    String msg;
    Tmct0infocompensacion ic = new Tmct0infocompensacion();
    ic.setAuditFechaCambio(new Date());
    ic.setAuditUser(config.getAuditUser());
    Tmct0parametrizacionDTO parametrizacion = null;
    if (desglose.getClearingRuleTo() != null) {
      LOG.trace("[asignarInformacionAsignacionDesgloseManual] usando regla {}-{} TipoDesglose: {} User: {}",
          desglose.getClearingRuleTo(), desglose.getClearingRuleDescriptionTo(), config.getTipoDesgloseManual(),
          config.getAuditUser());

      parametrizacion = config.getParametrizacionAsignacion(alo, desglose.getClearingRuleTo(), dc
          .getTmct0CamaraCompensacion().getCdCodigo());

      if (parametrizacion != null) {
        if (config.getTipoDesgloseManual() == TipoDesgloseManual.EXCEL) {
          parametrizacion.setExcel(true);
          msg = String.format("Alc: '%s' Utilizando EXCEL Clearing Rule '%s'-'%s' a peticion del usuario", dc
              .getTmct0alc().getId().getNucnfliq(), desglose.getClearingRuleTo(),
              desglose.getClearingRuleDescriptionTo());

        }
        else {
          parametrizacion.setManual(true);
          msg = String.format("Alc: '%s' Utilizando MANUAL Clearing Rule '%s'-'%s' a peticion del usuario", dc
              .getTmct0alc().getId().getNucnfliq(), desglose.getClearingRuleTo(),
              desglose.getClearingRuleDescriptionTo());
        }
        LOG.debug("[asignarInformacionAsignacionDesgloseManual] {}", msg);
        logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
            .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
        this.asignarParametrizacion(alo, dc, parametrizacion, ic, config);
      }
      else {
        msg = String.format("Alc: '%s' No encontrada parametrizacion la regla '%s'-'%s' camara: '%s'", dc.getTmct0alc()
            .getId().getNucnfliq(), desglose.getClearingRuleTo(),
            StringUtils.substring(StringUtils.trim(desglose.getClearingRuleDescriptionTo()), 0, 20),
            StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()));
        LOG.debug("[asignarInformacionAsignacionDesgloseManual] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }
    else if (StringUtils.isNotBlank(desglose.getClearingAccountCodeTo())
        || StringUtils.isNotBlank(desglose.getClearingMemberCodeTo())
        && StringUtils.isNotBlank(desglose.getGiveUpReferenceTo())) {
      LOG.trace(
          "[asignarInformacionAsignacionDesgloseManual] usando parametrizacion Manual cta: {} miembro: {}-{} ref.give-up: {}",
          desglose.getClearingAccountTo(), desglose.getClearingMemberCodeTo(),
          desglose.getClearingMemberDescriptionTo(), desglose.getGiveUpReferenceTo());

      parametrizacion = new Tmct0parametrizacionDTO();

      if (config.getTipoDesgloseManual() == TipoDesgloseManual.EXCEL) {
        parametrizacion.setExcel(true);
        msg = String.format("Alc: '%s' Desglose EXCEL Cta: '%s' Miembro: '%s'-'%s' Ref.Give-up: '%s'", dc.getTmct0alc()
            .getId().getNucnfliq(), desglose.getClearingAccountCodeTo(), desglose.getClearingMemberCodeTo(),
            desglose.getClearingMemberDescriptionTo(), desglose.getGiveUpReferenceTo());

      }
      else {
        parametrizacion.setManual(true);
        msg = String.format("Alc: '%s' Desglose MANUAL Cta: '%s' Miembro: '%s'-'%s' Ref.Give-up: '%s'", dc
            .getTmct0alc().getId().getNucnfliq(), desglose.getClearingAccountCodeTo(),
            desglose.getClearingMemberCodeTo(), desglose.getClearingMemberDescriptionTo(),
            desglose.getGiveUpReferenceTo());
      }
      LOG.debug("[asignarInformacionAsignacionDesgloseManual] {}", msg);
      logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg, alo
          .getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());

      parametrizacion.setCd_codigoCamaraComp(dc.getTmct0CamaraCompensacion().getCdCodigo());
      parametrizacion.setCd_codigoCuentaCompensacion(desglose.getClearingAccountCodeTo());
      parametrizacion.setCdCodigoCuentaCompensacion(desglose.getClearingAccountCodeTo());
      parametrizacion.setNbNombreCompensador(desglose.getClearingMemberCodeTo());
      parametrizacion.setReferenciaAsignacion(desglose.getGiveUpReferenceTo());
      this.asignarParametrizacion(alo, dc, parametrizacion, ic, config);
    }
    else {
      parametrizacion = config.getParametrizacionAsignacion(alo, dc.getTmct0CamaraCompensacion().getCdCodigo());
      if (parametrizacion != null) {
        this.asignarParametrizacion(alo, dc, parametrizacion, ic, config);
      }
      else {
        msg = String.format("Alc: '%s' No encontrada parametrizacion para alias: '%s' camara: '%s'", dc.getTmct0alc()
            .getId().getNucnfliq(), StringUtils.trim(alo.getCdalias()),
            StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()));
        LOG.debug("[asignarInformacionAsignacionDesgloseManual] {}", msg);
        throw new SIBBACBusinessException(msg);
      }
    }

    dc.setTmct0infocompensacion(ic);
    LOG.trace("[asignarInformacionAsignacionDesgloseManual] fin");
  }

  private void validarParametrizacion(Tmct0alo alo, Tmct0desglosecamara dc, Tmct0parametrizacionDTO parametrizacion,
      Tmct0infocompensacion ic, DesgloseConfigDTO config) throws SIBBACBusinessException {
    String msg;
    String tipoPatametrizacion = "";

    if (parametrizacion != null) {

      if (parametrizacion.isManual()) {
        tipoPatametrizacion = "MANUAL";
      }
      else if (parametrizacion.isExcel()) {
        tipoPatametrizacion = "EXCEL";
      }

      if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(config.getCamaraEuroccp())) {
        if (StringUtils.isBlank(parametrizacion.getCdCodigoCuentaCompensacion())) {
          if (parametrizacion.getIdRegla() == null) {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' cta: '%s'. Para EuroCcp la cuenta es obligatoria.",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()));
          }
          else {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' regla: '%s'-'%s' cta: '%s'. Para EuroCcp la cuenta es obligatoria.",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()), parametrizacion.getIdRegla(),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getDescripcionRegla()), 0, 20),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()));
          }
          throw new SIBBACBusinessException(msg);
        }
        else if (StringUtils.isNotBlank(parametrizacion.getNbNombreCompensador())
            || StringUtils.isNotBlank(parametrizacion.getReferenciaAsignacion())) {

          if (parametrizacion.getIdRegla() == null) {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' miembro: '%s'-'%s' ref.give-up: '%s'. El miembro y ref.give-up no deben estar informados",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()),
                    StringUtils.trim(parametrizacion.getNbNombreCompensador()),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getNb_descripcionCompensador()), 0, 20),
                    StringUtils.trim(parametrizacion.getReferenciaAsignacion()));
          }
          else {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' regla: '%s'-'%s' miembro: '%s'-'%s' ref.give-up: '%s'. El miembro y ref.give-up no deben estar informados",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()), parametrizacion.getIdRegla(),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getDescripcionRegla()), 0, 20),
                    StringUtils.trim(parametrizacion.getNbNombreCompensador()),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getNb_descripcionCompensador()), 0, 20),
                    StringUtils.trim(parametrizacion.getReferenciaAsignacion()));
          }
          throw new SIBBACBusinessException(msg);

        }
        else if (!config.hasEuroCppConfig(parametrizacion.getCdCodigoCuentaCompensacion())) {
          if (parametrizacion.getIdRegla() == null) {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' cta: '%s'. La Cta no esta configurada para Euroccp.",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()));
          }
          else {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' regla: '%s'-'%s' cta: '%s'. La Cta no esta configurada para Euroccp.",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()), parametrizacion.getIdRegla(),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getDescripcionRegla()), 0, 20),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()));
          }
          throw new SIBBACBusinessException(msg);
        }
        else {
          if (parametrizacion != null && parametrizacion.isOk()) {
            if (parametrizacion.getIdRegla() == null) {
              msg = String.format("Alc: '%s' - Parametrizacion %s alias: '%s' camara: '%s' cta: '%s' ", dc
                  .getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                  StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()),
                  StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()));
            }
            else {
              msg = String.format(
                  "Alc: '%s' - Parametrizacion %s alias: '%s' camara: '%s' regla: '%s'-'%s' cta: '%s' ", dc
                      .getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                  StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()), parametrizacion.getIdRegla(),
                  StringUtils.substring(StringUtils.trim(parametrizacion.getDescripcionRegla()), 0, 20),
                  StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()));
            }
            LOG.debug("[crearNuevoDesgloseCamara] {}", msg);
            logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg,
                alo.getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
          }
        }
      }
      else {
        if (parametrizacion.isOk()) {
          if (parametrizacion.getIdRegla() == null) {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s alias: '%s' camara: '%s' cta: '%s' miembro: '%s'-'%s' ref.give-up: '%s'",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()),
                    StringUtils.trim(parametrizacion.getNbNombreCompensador()),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getNb_descripcionCompensador()), 0, 20),
                    StringUtils.trim(parametrizacion.getReferenciaAsignacion()));
          }
          else {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s alias: '%s' camara: '%s' regla: '%s'-'%s' cta: '%s' miembro: '%s'-'%s' ref.give-up: '%s'",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()), parametrizacion.getIdRegla(),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getDescripcionRegla()), 0, 20),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()),
                    StringUtils.trim(parametrizacion.getNbNombreCompensador()),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getNb_descripcionCompensador()), 0, 20),
                    StringUtils.trim(parametrizacion.getReferenciaAsignacion()));
          }
          LOG.debug("[crearNuevoDesgloseCamara] {}", msg);
          logBo.insertarRegistro(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), msg,
              alo.getTmct0estadoByCdestadoasig().getNombre(), config.getAuditUser());
        }
        else {
          if (parametrizacion.getIdRegla() == null) {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' cta: '%s' miembro: '%s'-'%s' ref.give-up: '%s'",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()),
                    StringUtils.trim(parametrizacion.getNbNombreCompensador()),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getNb_descripcionCompensador()), 0, 20),
                    StringUtils.trim(parametrizacion.getReferenciaAsignacion()));
          }
          else {
            msg = String
                .format(
                    "Alc: '%s' - Parametrizacion %s INCORRECTA alias: '%s' camara: '%s' regla: '%s'-'%s' cta: '%s' miembro: '%s'-'%s' ref.give-up: '%s'",
                    dc.getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
                    StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()), parametrizacion.getIdRegla(),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getDescripcionRegla()), 0, 20),
                    StringUtils.trim(parametrizacion.getCdCodigoCuentaCompensacion()),
                    StringUtils.trim(parametrizacion.getNbNombreCompensador()),
                    StringUtils.substring(StringUtils.trim(parametrizacion.getNb_descripcionCompensador()), 0, 20),
                    StringUtils.trim(parametrizacion.getReferenciaAsignacion()));
          }
          throw new SIBBACBusinessException(msg);
        }
      }
    }
    else {
      msg = String.format("Alc: '%s' - Parametrizacion %s INCIDENCIA alias: '%s' camara: '%s' SIN PARAMETRIZACION", dc
          .getTmct0alc().getId().getNucnfliq(), tipoPatametrizacion, StringUtils.trim(alo.getCdalias()),
          StringUtils.trim(dc.getTmct0CamaraCompensacion().getCdCodigo()));
      throw new SIBBACBusinessException(msg);
    }
  }

  private void asignarParametrizacion(Tmct0alo alo, Tmct0desglosecamara dc, Tmct0parametrizacionDTO parametrizacion,
      Tmct0infocompensacion ic, DesgloseConfigDTO config) throws SIBBACBusinessException {
    LOG.trace("[asignarParametrizacion] inicio");

    this.validarParametrizacion(alo, dc, parametrizacion, ic, config);
    if (parametrizacion.getIdRegla() != null) {
      ic.setTmct0Regla(new Tmct0Regla());
      ic.getTmct0Regla().setIdRegla(parametrizacion.getIdRegla());
    }
    ic.setCdctacompensacion(parametrizacion.getCdCodigoCuentaCompensacion());
    ic.setCdmiembrocompensador(parametrizacion.getNbNombreCompensador());
    ic.setCdrefasignacion(parametrizacion.getReferenciaAsignacion());
    LOG.trace("[asignarParametrizacion] fin");
  }

  private boolean checkSettlmentData(boolean isRouting, boolean isScriptDividend, SettlementData s)
      throws SettlementChainDataException {

    if (s.isClearerMultiple()) {
      throw new SettlementChainDataException(String.format(
          "SETTLEMENT_DATA_ERROR-Alias: '%s' SubCta: '%s' Multiples clearers",
          StringUtils.trim(s.getTmct0ilq().getCdaliass()), StringUtils.trim(s.getTmct0ilq().getCdsubcta())));
    }
    else if (!s.isDataIL()) {
      throw new SettlementChainDataException(String.format(
          "SETTLEMENT_DATA_ERROR-Alias: '%s' SubCta: '%s' No hay instrucciones de liquidacion",
          StringUtils.trim(s.getTmct0ilq().getCdaliass()), StringUtils.trim(s.getTmct0ilq().getCdsubcta())));
    }
    else if (!s.isDataClearer() && !s.isDataCustodian()) {
      throw new SettlementChainDataException(String.format(
          "SETTLEMENT_DATA_ERROR-Alias: '%s' SubCta: '%s' No hay clearer/custodian",
          StringUtils.trim(s.getTmct0ilq().getCdaliass()), StringUtils.trim(s.getTmct0ilq().getCdsubcta())));
    }
    else if (!isRouting && !isScriptDividend && !s.isDataTFI()) {
      throw new SettlementChainDataException(String.format(
          "SETTLEMENT_DATA_ERROR-Alias: '%s' SubCta: '%s' No tiene holders.",
          StringUtils.trim(s.getTmct0ilq().getCdaliass()), StringUtils.trim(s.getTmct0ilq().getCdsubcta())));
    }

    return true;
  }

  private String getDescripcionDesgloseIlq(Character desglose) {
    switch (desglose) {
      case 'M':
        return "MANUAL";
      case 'D':
        return "DEFINITIVO";
      case 'P':
        return "PROVISIONAL";
      default:
        return "UNDEFINED";
    }
  }

  private short getMaxNucnfliqAdd1(Tmct0aloId id) {
    Short nucnfliq = alcBo.getMaxNucnfliqByTmct0aloId(id);
    if (nucnfliq == null) {
      nucnfliq = (short) 1;
    }
    else {
      nucnfliq = new Short(String.format("%s", nucnfliq.intValue() + 1));

    }
    return nucnfliq;
  }

}
