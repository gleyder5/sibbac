package sibbac.business.comunicaciones.pti.mo.bo;

import java.util.Comparator;

import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;

public class Tmct0MovimientooperacionnuevaImefectivoDescComparator implements Comparator<Tmct0movimientooperacionnuevaecc> {

  @Override
  public int compare(Tmct0movimientooperacionnuevaecc arg0, Tmct0movimientooperacionnuevaecc arg1) {
    return arg0.getImefectivo().compareTo(arg1.getImefectivo()) * -1;
  }

}
