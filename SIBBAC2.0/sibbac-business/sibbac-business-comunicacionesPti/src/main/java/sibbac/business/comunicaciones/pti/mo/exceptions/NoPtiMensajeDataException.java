/**
 * 
 */
package sibbac.business.comunicaciones.pti.mo.exceptions;

import sibbac.common.SIBBACBusinessException;

/**
 * @author xIS16630
 *
 */
public class NoPtiMensajeDataException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = 2583492220886420818L;

  /**
   * 
   */
  public NoPtiMensajeDataException() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public NoPtiMensajeDataException(String arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public NoPtiMensajeDataException(Throwable arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public NoPtiMensajeDataException(String arg0, Throwable arg1) {
    super(arg0, arg1);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   * @param arg2
   * @param arg3
   */
  public NoPtiMensajeDataException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
    // TODO Auto-generated constructor stub
  }

}
