package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccLogDao;
import sibbac.business.comunicaciones.pti.cuadreecc.dao.CuadreEccTitularDao;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccTitular;
import sibbac.database.bo.AbstractBo;

@Service
public class CuadreEccTitularBo extends AbstractBo<CuadreEccTitular, Long, CuadreEccTitularDao> {

  @Autowired
  private CuadreEccLogDao cuadreEccLogDao;

  public Integer deleteByFechaejecucionAndNumerooperacionAndSentido(Date fechaEjecucion,
                                                                    String numerooperacion1,
                                                                    Character sentido) {
    return dao.deleteByFechaejecucionAndNumerooperacionAndSentido(fechaEjecucion, numerooperacion1, sentido);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Integer deleteByIdCuadreEccReferencia(long idReferencia) {

    List<Long> ids = dao.findIdByCuadreEccReferenciaId(idReferencia);
    if (CollectionUtils.isNotEmpty(ids)) {
      for (Long id : ids) {
        cuadreEccLogDao.deleteByIdCuadreEccTitular(id);
      }
    }
    return dao.deleteByIdCuadreEccReferencia(idReferencia);
  }

  public List<CuadreEccTitular> findByEccMessageInTiId(Long idTi) {
    return dao.findByCuadreEccReferenciaId(idTi);
  }

}
