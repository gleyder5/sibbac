package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.fs.FS;
import sibbac.pti.messages.fs.FSCommonDataBlock;

@Service("ptiMessageProcessFsBo")
public class PtiMessageProcessFsBo extends PtiMessageProcess<FS> {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessFsBo.class);

  @Autowired
  private Tmct0menBo menBo;

  public PtiMessageProcessFsBo() {
  }

  @Override
  @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = { Exception.class })
  public void process(PTIMessageVersion version, FS fs, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {
    FSCommonDataBlock cdb = (FSCommonDataBlock) fs.getCommonDataBlock();
    Date fechaSession = cdb.getFechaDeSesion(); // ASCII(8)
    Date horaFinSession = cdb.getHoraFinDeSesionOnline(); // ASCII(9)
    LOG.info(": Recibida la hora de fin de sesion: [{}/{}]", fechaSession, horaFinSession);

    // Nuevos valores.

    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    BigDecimal fechaSessionFs = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
        .convertDateToString(fechaSession));
    if (now.compareTo(fechaSessionFs) > 0) {
      String msg = MessageFormat.format("Ha llegado un mensaje FS de una sesion pasada: {0}", fs);
      throw new SIBBACBusinessException(msg);
    }

    Tmct0men men = menBo.findByCdmensa(StringUtils.leftPad(String.format("%s", TIPO_APUNTE.PTI_FIN_DIA.getTipo()), 3,
        "0"));
    if (men != null) {

      // La nueva fecha y hora.
      men.setHrmensa(horaFinSession);
      men.setFemensa(fechaSession);

      // El campo de "user open".
      men.setCdUserSessionOpen(null);

      men.setCdGeneralPtiSessionStatus('C');

      LOG.info(": La sesion finaliza a las : {}", horaFinSession);

      menBo.save(men);
    }
    else {
      throw new SIBBACBusinessException(String.format(
          "INCIDENCIA-No encontrado tmct0men:'%s' para cerrar la sesion a las : '%s' '%s'", "210", fechaSession,
          horaFinSession));
    }

    // End.

  }
}
