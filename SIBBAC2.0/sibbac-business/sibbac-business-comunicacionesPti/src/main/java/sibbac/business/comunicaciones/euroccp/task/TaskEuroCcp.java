package sibbac.business.comunicaciones.euroccp.task;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.comunicaciones.euroccp.ciffile.bo.EuroCcpCifFileReader;
import sibbac.business.comunicaciones.euroccp.ciffile.bo.EuroCcpProcessedUnsettledMovementDbReader;
import sibbac.business.comunicaciones.euroccp.realigment.bo.EuroCcpExecutionRealigmentReceptionFilesBo;
import sibbac.business.comunicaciones.euroccp.realigment.bo.EuroCcpExecutionRealigmentSendFilesBo;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpFileSentBo;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReferenceStaticDataDbReader;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionCorrectionsDbReader;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpOwnershipReportingGrossExecutionsSendDbReader;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpTitularesFileReader;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_EURO_CCP, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 1,12,14,16,27,29,31,42,44,46,57,59 7-22 ? * MON-FRI")
public class TaskEuroCcp extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEuroCcp.class);

  @Value("${sibbac.euroccp.in.path}")
  private String workPathSt;

  @Value("${sibbac.euroccp.in.path.tratados}")
  private String tratadosFolder;

  @Value("${sibbac.euroccp.cif.file.in.pattern}")
  private String cifFileInPattern;

  @Value("${sibbac.euroccp.cif.file.process.pattern}")
  private String cifFileProcessPattern;

  @Value("${sibbac.euroccp.cif.file.charset}")
  private Charset cifFileCharset;

  @Value("${sibbac.euroccp.titulares.ors.file.send.first.sd.date.init.hour}")
  private String initOrsSendHourFirstDate;

  @Value("${sibbac.euroccp.titulares.ors.file.send.last.sd.date.end.hour}")
  private String endOrsSendHourLastDate;

  @Value("${sibbac.euroccp.titulares.org.file.send.first.sd.date.init.hour}")
  private String initOrgSendHourFirstDate;

  @Value("${sibbac.euroccp.titulares.org.file.send.last.sd.date.end.hour}")
  private String endOrgSendHourLastDate;

  @Value("${sibbac.euroccp.titulares.crg.file.send.first.sd.date.init.hour}")
  private String initCrgSendHourFirstDate;

  @Value("${sibbac.euroccp.titulares.crg.file.send.last.sd.date.end.hour}")
  private String endCrgSendHourLastDate;

  @Value("${sibbac.euroccp.realigment.erg.file.send.first.sd.date.init.hour}")
  private String initErgSendHourFirstDate;

  @Value("${sibbac.euroccp.realigment.erg.file.send.last.sd.date.end.hour}")
  private String endErgSendHourLastDate;

  @Autowired
  private EuroCcpCifFileReader euroCcpCifFileReader;

  @Qualifier(value = "euroCcpProcessedUnsettledMovementDbReader")
  @Autowired
  private EuroCcpProcessedUnsettledMovementDbReader euroCcpProcessedUnsettledMovementDbReaderBo;

  @Qualifier(value = "euroCcpOwnershipReferenceStaticDataDbReader")
  @Autowired
  private EuroCcpOwnershipReferenceStaticDataDbReader euroCcpGeneracionFicheroExcelTitularesDbReader;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionsSendDbReader")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsSendDbReader euroCcpOwnershipReportingGrossExecutionsDbReader;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionCorrectionsDbReader")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionCorrectionsDbReader euroCcpOwnershipReportingGrossExecutionCorrectionssDbReader;

  @Autowired
  private EuroCcpTitularesFileReader euroCcpTitularesFileReader;

  @Qualifier(value = "euroCcpOwnershipReportingGrossExecutionsReceiveDbReader")
  @Autowired
  private EuroCcpOwnershipReportingGrossExecutionsReceiveDbReader euroCcpOwnershipReportingGrossExecutionsReceiveDbReader;

  @Autowired
  private EuroCcpExecutionRealigmentSendFilesBo enviarFicherosErgBo;

  @Autowired
  private EuroCcpExecutionRealigmentReceptionFilesBo recepcionFicherosErgBo;

  @Autowired
  private EuroCcpFileSentBo euroCcpFileSentBo;

  public TaskEuroCcp() {

  }

  @Override
  public void executeTask() throws Exception {
    LOG.trace("[TaskEuroCcp :: executeTask] inicio");

    String dateFormat = "HHmmss";

    LOG.info("[TaskEuroCcp :: executeTask] procesando ficheros...");
    this.procesarFicherosCif();

    LOG.info("[TaskEuroCcp :: executeTask] cargando op ecc, case y movimientos...");
    euroCcpProcessedUnsettledMovementDbReaderBo.executeTask();

    // quiza tengamos que pasar a un task nuevo para poder configurarlo cada
    // hora minimo
    euroCcpGeneracionFicheroExcelTitularesDbReader.createPathsIfNotExists();

    boolean canExecuteSendOrs = this.canExecuteNow(initOrsSendHourFirstDate, endOrsSendHourLastDate, dateFormat);
    
    if (canExecuteSendOrs) {
      LOG.info("[TaskEuroCcp :: executeTask] generando fichero titulares ORS...");
      euroCcpGeneracionFicheroExcelTitularesDbReader.executeTask();
    }
    
    boolean canExecuteSendOrg = this.canExecuteNow(initOrgSendHourFirstDate, endOrgSendHourLastDate, dateFormat);

    if (canExecuteSendOrg) {
      LOG.info("[TaskEuroCcp :: executeTask] generando fichero titulares ORG...");
      euroCcpOwnershipReportingGrossExecutionsDbReader.executeTask();
    }
    
    boolean canExecuteSendCrg = this.canExecuteNow(initCrgSendHourFirstDate, endCrgSendHourLastDate, dateFormat);

    if (canExecuteSendCrg) {
      LOG.info("[TaskEuroCcp :: executeTask] generando fichero titulares CRG...");
      euroCcpOwnershipReportingGrossExecutionCorrectionssDbReader.executeTask();
    }

    LOG.info("[TaskEuroCcp :: executeTask] procesando respuesta a ficheros titulares ORG Y CRG...");
    euroCcpTitularesFileReader.procesarFicherosRespuestaEuroCcp();

    LOG.info("[TaskEuroCcp :: executeTask] procesando registros respuesta titulares...");
    euroCcpOwnershipReportingGrossExecutionsReceiveDbReader.executeTask();
    
    boolean canExecuteSendErg = this.canExecuteNow(initErgSendHourFirstDate, endErgSendHourLastDate, dateFormat);
    
    if (canExecuteSendErg) {
      LOG.info("[TaskEuroCcp :: executeTask] Generando fichero realigment ERG...");
      enviarFicherosErgBo.enviarFicheros();
    }

    LOG.info("[TaskEuroCcp :: executeTask] Procesando respuesta fichero realigment ERG...");
    recepcionFicherosErgBo.procesarFicherosRespuestaEuroCcp();
    LOG.trace("[TaskEuroCcp :: executeTask] fin");
  }

  private void procesarFicherosCif() throws SIBBACBusinessException {
    LOG.info(
        "[TaskEuroCcp :: procesarFicherosCif] config: path: {}, cifFileInPattern: {}, cifFileProcessPattern: {}, cifFileCharset: {}.",
        workPathSt.toString(), cifFileInPattern, cifFileProcessPattern, cifFileCharset);
    Path workPath = Paths.get(workPathSt);
    Path procesadosPath = Paths.get(workPath.toString(), tratadosFolder);

    if (Files.notExists(workPath)) {
      LOG.warn("[TaskEuroCcp :: procesarFicherosCif] no existe la ruta : {}, la creamos", workPath.toString());
      try {
        Files.createDirectories(workPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
    }

    if (Files.notExists(procesadosPath)) {
      LOG.warn("[TaskEuroCcp :: procesarFicherosCif] no existe la ruta : {}, la creamos", procesadosPath.toString());
      try {
        Files.createDirectories(procesadosPath);
      }
      catch (IOException e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
    }

    euroCcpCifFileReader.procesarFicherosCif(workPath, procesadosPath, cifFileInPattern, cifFileCharset, false);

  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_EURO_CCP;
  }

}
