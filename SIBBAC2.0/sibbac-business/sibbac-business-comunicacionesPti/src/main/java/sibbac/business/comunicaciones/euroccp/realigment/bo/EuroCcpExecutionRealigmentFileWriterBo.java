package sibbac.business.comunicaciones.euroccp.realigment.bo;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpFileTypeEnum;
import sibbac.business.comunicaciones.euroccp.realigment.dto.EuroCcpExecutionRealigmentDTO;
import sibbac.business.comunicaciones.euroccp.realigment.mapper.EuroCcpExecutionRealigmentFieldSetMapper;
import sibbac.business.comunicaciones.euroccp.realigment.model.EuroCcpExecutionRealigment;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpAbstractFileGenerationBo;
import sibbac.business.comunicaciones.euroccp.titulares.bo.EuroCcpFileSentBo;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpFileTypeDao;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileType;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Service("euroCcpExecutionRealigmentFileWriterBo")
public class EuroCcpExecutionRealigmentFileWriterBo extends
    EuroCcpAbstractFileGenerationBo<EuroCcpIdDTO, EuroCcpCommonSendRegisterDTO> {

  @Autowired
  private EuroCcpExecutionRealigmentBo executionRealigmentBo;

  @Autowired
  private EuroCcpFileSentBo fileSentBo;

  @Autowired
  private EuroCcpFileTypeDao fileSentTypeDao;

  @Autowired
  private Tmct0logBo logBo;

  public EuroCcpExecutionRealigmentFileWriterBo() {
  }

  @Override
  protected void processBeansAndWriteTmpFile(Path tmpPath, String tmpFileName, Date sentDate, List<EuroCcpIdDTO> beans,
      Map<String, FlatFileItemWriter<EuroCcpCommonSendRegisterDTO>> mapFicherosByClientNumber,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosPrincipal,
      Map<String, EuroCcpFileSent> mapRegistrosFicherosEnviadosTmp, int lastClientNumberPositions, Charset charset)
      throws SIBBACBusinessException {
    if (CollectionUtils.isNotEmpty(beans)) {
      EuroCcpExecutionRealigment er;
      EuroCcpFileSent tmpFileSent;
      EuroCcpFileSent principalFileSent;
      String clientNumber;
      FlatFileItemWriter<EuroCcpCommonSendRegisterDTO> writer;
      Path file;
      List<EuroCcpExecutionRealigmentDTO> beansToWrite = new ArrayList<EuroCcpExecutionRealigmentDTO>();
      for (EuroCcpIdDTO euroCcpIdDTO : beans) {
        er = executionRealigmentBo.findById(euroCcpIdDTO.getId());
        if (er != null) {
          clientNumber = er.getClientNumber();
          principalFileSent = mapRegistrosFicherosEnviadosPrincipal.get(clientNumber);
          if (principalFileSent == null) {
            throw new SIBBACBusinessException(
                MessageFormat
                    .format(
                        "INCIDENCIA- Reg principal file sent no disponible para el envio. ClientNumber: {0} Date: {1} type: {2}",
                        clientNumber, sentDate, getPrincipalFileType()));
          }

          tmpFileSent = mapRegistrosFicherosEnviadosTmp.get(clientNumber);
          writer = mapFicherosByClientNumber.get(clientNumber);
          if (tmpFileSent == null) {
            String fileBaseName = FilenameUtils.getBaseName(tmpFileName);
            String extension = FilenameUtils.getExtension(tmpFileName);

            String tmpName = fileBaseName + clientNumber.substring(clientNumber.length() - lastClientNumberPositions)
                + "_" + euroCcpIdDTO.getId() + "_" + FormatDataUtils.convertDateToConcurrentFileName(new Date()) + "."
                + extension;
            file = Paths.get(tmpPath.toString(), tmpName);
            EuroCcpFileType fileType = fileSentTypeDao.findByCdCodigo(getTmpFileType().name());
            tmpFileSent = fileSentBo.insertTxtTmpFileSent(principalFileSent, fileType, file, sentDate);
            mapRegistrosFicherosEnviadosTmp.put(clientNumber, tmpFileSent);
            writer = getNewWriter(file, charset, new EuroCcpExecutionRealigmentFieldSetMapper());
            mapFicherosByClientNumber.put(clientNumber, writer);
          }
          er.setFileSent(tmpFileSent);

          beansToWrite.add(new EuroCcpExecutionRealigmentDTO(er));
          er.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.FICHERO_GENERADO.getId());
          er.setAuditDate(new Date());

          if (er.getDesglose() != null) {
            logBo.insertarRegistro(
                er.getDesglose().getTmct0desglosecamara().getTmct0alc(),
                new Date(),
                new Date(),
                "",
                er.getDesglose().getTmct0desglosecamara().getTmct0estadoByCdestadoasig(),
                String.format("Realigment execution '%s' from '%s' to '%s' shares '%s'", er.getExecutionReference(),
                    er.getAccountNumberFrom(), er.getAccountNumberTo(), er.getNumberShares()));
          }
          try {
            writer.write(beansToWrite);
          }
          catch (Exception e) {
            throw new SIBBACBusinessException("Incidencia- Escribiendo en fichero", e);
          }
          beansToWrite.clear();
        }
      }
    }
  }

  @Override
  public void updateEstadoProcesado(EuroCcpFileSent principalFile, int estadoProcesado, String auditUser) {

  }

  @Override
  protected EuroCcpFileTypeEnum getPrincipalFileType() {
    return EuroCcpFileTypeEnum.ERG;
  }

  @Override
  protected EuroCcpFileTypeEnum getTmpFileType() {
    return EuroCcpFileTypeEnum.ERG_TMP;
  }

  @Override
  protected boolean getWriteFooterPrincipalFile() {
    return true;
  }

}
