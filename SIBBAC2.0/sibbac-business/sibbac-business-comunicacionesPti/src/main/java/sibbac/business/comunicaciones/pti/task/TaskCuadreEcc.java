package sibbac.business.comunicaciones.pti.task;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccArreglarDesglosesDbReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccEjecucionRevisionTitulosDbReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccFileReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccReferenciaValidacionDbReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccRevisarAndImportarDesglosesDbReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccSepararMovimientosByInformacionCompensacionDbReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccSibbacCargaOperacionesTraspasadasDbReader;
import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccValidacionTitulosReferenciasFicheroEccDbReader;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0cfgId;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.pti.PTIMessageFactory;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

/**
 * @author xIS16630</BR>
 * 
 *         CECCFL = CUADRE ECC FILE LOAD </BR> CECCVTI = CUADRE ECC REVISION TITULARES </BR> CECCVOE = CUADRE ECC
 *         REVISION CODIGOS OPERACION ECC</BR> CECCARD = CUADRE ECC ARREGLAR DESGLOSES</BR> CECCCTR = CUADRE ECC CARGAR
 *         OPERACIONES TRASPASADAS </BR> CECCVRT = CUADRE ECC REVISION REFERENCIAS TITULAR </BR> CECCIMD = CUADRE ECC
 *         IMPORTAR DESGLOSES </BR> CECCSMO = CUADRE ECC SEPARAR MOVIMIENTOS </BR>
 *
 */
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME,
           name = Task.GROUP_COMUNICACIONES_ECC.JOB_CUADRE_ECC,
           jobType = SIBBACJobType.CRON_JOB,
           cronExpression = "0 0/10 7-23 ? * MON-FRI")
public class TaskCuadreEcc extends WrapperTaskConcurrencyPrevent {

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${sibbac.files.pti.root.dir}")
  private String rootDir;

  @Value("${sibbac.files.pti.temporal.dir}")
  private String dirPath;

  @Value("${sibbac.files.pti.processed.dir}")
  private String processedDir;

  // Intermediarios Financieros.
  @Value("${sibbac.files.pti.if.prefix}")
  private String fileNamePrefix;

  @Value("${sibbac.files.pti.if.suffix}")
  private String fileNameSufix;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private PtiCommomns ptiCommons;

  @Qualifier(value = "cuadreEccFileReader")
  @Autowired
  private CuadreEccFileReader cuadreEccFileReader;

  @Qualifier(value = "cuadreEccValidacionTitulosReferenciasFicheroEccDbReader")
  @Autowired
  private CuadreEccValidacionTitulosReferenciasFicheroEccDbReader validacionReferenciasFicheroEccDbReader;

  @Qualifier(value = "cuadreEccEjecucionRevisionTitulosDbReader")
  @Autowired
  private CuadreEccEjecucionRevisionTitulosDbReader cuadreEccEjecucionRevisionTitulosDbReader;

  @Qualifier(value = "cuadreEccArreglarDesglosesDbReader")
  @Autowired
  private CuadreEccArreglarDesglosesDbReader cuadreEccArreglarDesglosesDbReader;

  @Qualifier(value = "cuadreEccSibbacCargaOperacionesTraspasadasDbReader")
  @Autowired
  private CuadreEccSibbacCargaOperacionesTraspasadasDbReader cuadreEccSibbacCargaOperacionesTraspasadasDbReader;

  @Qualifier(value = "cuadreEccReferenciaValidacionDbReader")
  @Autowired
  private CuadreEccReferenciaValidacionDbReader cuadreEccReferenciaValidacionDbReader;

  @Qualifier(value = "cuadreEccRevisarAndImportarDesglosesDbReader")
  @Autowired
  private CuadreEccRevisarAndImportarDesglosesDbReader cuadreEccRevisarAndImportarDesglosesDbReader;

  @Qualifier(value = "cuadreEccSepararMovimientosByInformacionCompensacionDbReader")
  @Autowired
  private CuadreEccSepararMovimientosByInformacionCompensacionDbReader cuadreEccSepararMovimientosByIc;

  public TaskCuadreEcc() {
  }

  @Override
  public void executeTask() throws Exception {
    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }

    Tmct0cfg cfgActivation = cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName, "PTI",
                                                                        "sibbac.cuadre.ecc.activation.subprocess");
    if (cfgActivation == null) {
      cfgActivation = new Tmct0cfg();
      cfgActivation.setId(new Tmct0cfgId(sibbac20ApplicationName, "PTI", "sibbac.cuadre.ecc.activation.subprocess"));
      cfgActivation.setKeyvalue("CECCFL,CECCVTI,CECCVOE,CECCARD,CECCCTR,CECCVRT,CECCIMD,CECCSMO");
      cfgActivation.setCdauxili("");
      cfgActivation = cfgBo.save(cfgActivation);
    }
    String activacionProcess = "";
    if (cfgActivation != null && !StringUtils.isBlank(cfgActivation.getKeyvalue())) {
      activacionProcess = cfgActivation.getKeyvalue();
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCFL")) {
      // CECCFL
      // 1.- Procesar fichero con los titulares
      procesarFichedrosCuadreEccPti();
    }
    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCVTI")) {
      // CECCVTI
      // ESTADO 2000 -> 2002
      validacionReferenciasFicheroEccDbReader.executeTask();
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCVOE")) {
      // CECCVOE
      // ESTADO 2002 -> 2010, 2015, 2016, 2020
      cuadreEccEjecucionRevisionTitulosDbReader.executeTask();
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCARD")) {
      // CECCARD
      // ESTADO 2016 -> 2002
      cuadreEccArreglarDesglosesDbReader.executeTask();
      // cuadreEccEjecucionRevisionTitulosDbReader.executeTask();
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCCTR")) {
      // CECCCTR
      // ESTADO 2017 ->2002
      // 2.- Revisar errores de las operaciones de bbdd
      cuadreEccSibbacCargaOperacionesTraspasadasDbReader.executeTask();
      // cuadreEccEjecucionRevisionTitulosDbReader.executeTask();
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCVRT")) {
      // CECCVRT
      // ESTADO 2020 -> 2030, 2040
      // 3.- Revisar reparto desgloses y referencias
      try {
        cuadreEccReferenciaValidacionDbReader.executeTask();
      } catch (Exception e) {
        LOG.warn(e.getMessage(), e);
      }
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCIMD")) {
      // CECCIMD
      // ESTADO 2040
      // 4.- Procesar informacion en bbdd
      cuadreEccRevisarAndImportarDesglosesDbReader.executeTask();
    }

    if (StringUtils.isBlank(activacionProcess) || activacionProcess.contains("CECCSMO")) {
      // CECCSMO
      // ESTADO 2100 -> 2110, 2105
      // cuadreEccEjecucionRevisionTitulosDbReader.executeTask();
      cuadreEccSepararMovimientosByIc.executeTask();
    }
  }

  private void procesarFichedrosCuadreEccPti() throws Exception {
    Path workPath = Paths.get(rootDir, dirPath);
    Path processedPath = Paths.get(rootDir, processedDir);

    procesarFicherosPath(workPath, processedPath, fileNamePrefix + "*" + fileNameSufix);
    procesarFicherosPath(workPath, processedPath, fileNamePrefix + "*" + fileNameSufix + "*" + ".GZ");
    procesarFicherosPath(workPath, processedPath, fileNamePrefix + "*" + fileNameSufix + "*" + ".gz");

  }

  private void procesarFicherosPath(Path workPath, Path processedPath, String filePattern) throws Exception {
    LOG.trace("[TaskRevisionTitularesPti :: procesarFicherosPath] inicio.");
    LOG.debug("[TaskRevisionTitularesPti :: procesarFicherosPath] procesando ficheros: {} pattern: {}.",
              workPath.toString(), filePattern);
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(workPath, filePattern);) {
      List<Path> listPathsOrdenados = new ArrayList<Path>();
      for (Path path : directoryStream) {
        listPathsOrdenados.add(path);
      }
      Collections.sort(listPathsOrdenados, new Comparator<Path>() {
        @Override
        public int compare(Path arg0, Path arg1) {
          if (arg0.getFileName().compareTo(arg1.getFileName()) != 0) {
            return arg0.getFileName().compareTo(arg1.getFileName());
          } else {
            try {
              return Files.getLastModifiedTime(arg0).compareTo(Files.getLastModifiedTime(arg1));
            } catch (IOException e) {
              return 0;
            }
          }
        }

      });
      for (Path path : listPathsOrdenados) {
        LOG.debug("[TaskRevisionTitularesPti :: procesarFicherosPath] procesando fichero: {}", path.toString());

        cuadreEccFileReader.executeTask(path.toString(), false, StandardCharsets.ISO_8859_1);
        LOG.debug("[TaskRevisionTitularesPti :: procesarFicherosPath] borrando fichero temporal: {}",
                  cuadreEccFileReader.getFile());
        Files.deleteIfExists(cuadreEccFileReader.getFile());
      }
    }
    LOG.trace("[TaskRevisionTitularesPti :: procesarFicherosPath] fin.");
  }

  // private void moverProcesados(Path path, Path originalPath, Path processedPath) throws IOException {
  // boolean gz = originalPath.toString().toUpperCase().endsWith(".GZ");
  // if (!gz) {
  // Path toMove = Paths.get(processedPath.toString(),
  // originalPath.toFile().getName()
  // + FormatDataUtils.convertDateToConcurrentFileName(new Date()) + ".GZ");
  // LOG.debug("[TaskRevisionTitularesPti :: moverProcesados] Moviendo fichero de: {} a {}", originalPath.toString(),
  // toMove.toString());
  //
  // OutputStream gos = new GZIPOutputStream(Files.newOutputStream(toMove, StandardOpenOption.CREATE,
  // StandardOpenOption.WRITE));
  // InputStream is = Files.newInputStream(path, StandardOpenOption.READ);
  //
  // IOUtils.copyLarge(is, gos);
  // gos.close();
  // is.close();
  // Files.deleteIfExists(path);
  //
  // } else {
  // Path toMove = Paths.get(processedPath.toString(),
  // originalPath.toFile().getName()
  // + FormatDataUtils.convertDateToConcurrentFileName(new Date()));
  // LOG.debug("[TaskRevisionTitularesPti :: moverProcesados] Moviendo fichero de: {} a {}", originalPath.toString(),
  // toMove.toString());
  // Files.move(path, toMove, StandardCopyOption.ATOMIC_MOVE);
  // }
  // }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_CUADRE_ECC;
  }

}
