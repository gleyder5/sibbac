package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

@Service
public class XmlTmct0cta {

  protected static final Logger LOG = LoggerFactory.getLogger(XmlTmct0cta.class);

  @Autowired
  private XmlOrdenHelper xmlOrdenHelper;

  public XmlTmct0cta() {
    super();

  }

  public void accumulateNew(Tmct0gal tmct0gal, Tmct0alo tmct0alo, Tmct0cta tmct0cta) throws XmlOrdenExceptionReception {

    tmct0cta.getId().setNucnfclt(tmct0gal.getId().getNucnfclt());

    tmct0cta.setImcammed(tmct0alo.getImcammed().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcamnet(tmct0alo.getImcamnet().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImtotbru(tmct0gal.getImbrmerc().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImtotnet(tmct0gal.getImtotnet().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setEutotbru(tmct0gal.getImbrmreu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setEutotnet(tmct0gal.getEutotnet().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImnetbrk(new BigDecimal(0));
    tmct0cta.setEunetbrk(new BigDecimal(0));
    tmct0cta.setImcomisn(tmct0gal.getImcomisn().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcomieu(tmct0gal.getImcomieu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcomdev(tmct0gal.getImcomdev().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setEucomdev(tmct0gal.getEucomdev().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImfibrdv(tmct0gal.getImfibrdv().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImfibreu(tmct0gal.getImfibreu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImsvb(tmct0gal.getImsvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImsvbeu(tmct0gal.getImsvbeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImbrmerc(tmct0gal.getImbrmercb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImbrmreu(tmct0gal.getImbrmreub().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgastos(tmct0gal.getImgastosb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgatdvs(tmct0gal.getImgatdvsb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImimpdvs(tmct0gal.getImimpdvsb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImimpeur(tmct0gal.getImimpeurb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcconeu(tmct0gal.getImcconeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcliqeu(tmct0gal.getImcliqeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImdereeu(tmct0gal.getImdereeu().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setNutitagr(tmct0gal.getNutitagr().setScale(0, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setPccombrk(tmct0gal.getPccombrk().setScale(6, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setCdmoniso(tmct0alo.getCdmoniso());
    tmct0cta.setCdtpoper(tmct0alo.getCdtpoper());

    tmct0cta.setCdindgas(tmct0gal.getCdindgasb());

    tmct0cta.setEutotbru(tmct0gal.getImbrmreu().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setCdcleare(tmct0alo.getCdcleare());
    tmct0cta.setAcctatcle(tmct0alo.getAcctatcle());
    tmct0cta.setCdbencle(tmct0alo.getCdbencle());
    tmct0cta.setAcbencle(tmct0alo.getAcbencle());
    tmct0cta.setIdbencle(tmct0alo.getIdbencle());
    tmct0cta.setGclecode(tmct0alo.getGclecode());
    tmct0cta.setGcleacct(tmct0alo.getGcleacct());
    tmct0cta.setGcleid(tmct0alo.getGcleid());
    tmct0cta.setLclecode(tmct0alo.getLclecode());
    tmct0cta.setLcleacct(tmct0alo.getLcleacct());
    tmct0cta.setLcleid(tmct0alo.getLcleid());
    tmct0cta.setPccleset(tmct0alo.getPccleset());
    tmct0cta.setSendbecle(tmct0alo.getSendbecle());
    tmct0cta.setTpsetcle(tmct0alo.getTpsetcle());
    tmct0cta.setTysetcle(tmct0alo.getTysetcle());

    tmct0cta.setCdcustod(tmct0alo.getCdcustod());
    tmct0cta.setAcctatcus(tmct0alo.getAcctatcus());
    tmct0cta.setCdbencus(tmct0alo.getCdbencus());
    tmct0cta.setAcbencus(tmct0alo.getAcbencus());
    tmct0cta.setIdbencus(tmct0alo.getIdbencus());
    tmct0cta.setGcuscode(tmct0alo.getGcuscode());
    tmct0cta.setGcusacct(tmct0alo.getGcusacct());
    tmct0cta.setGcusid(tmct0alo.getGcusid());
    tmct0cta.setLcuscode(tmct0alo.getLcuscode());
    tmct0cta.setLcusacct(tmct0alo.getLcusacct());
    tmct0cta.setLcusid(tmct0alo.getLcusid());
    tmct0cta.setPccusset(tmct0alo.getPccusset());
    tmct0cta.setSendbecus(tmct0alo.getSendbecus());
    tmct0cta.setTpsetcus(tmct0alo.getTpsetcus());
    tmct0cta.setTysetcus(tmct0alo.getTysetcus());

    tmct0cta.setNbtitula(tmct0alo.getNbtitulr());
    tmct0cta.setFevalor(tmct0alo.getTmct0bok().getFevalor());
    // FIX:Inicio
    tmct0cta.setFevalorr(tmct0alo.getTmct0bok().getFevalorr());
    // FIX:Fin
    tmct0cta.setFetrade(tmct0alo.getTmct0bok().getFetrade());
    // cambiar fechas
    tmct0cta.setFhsending(new Date(new GregorianCalendar(0001, 00, 01, 00, 00).getTimeInMillis()));
    tmct0cta.setHosending(new Time(new GregorianCalendar(0001, 00, 01, 00, 00).getTimeInMillis()));

    /*
     * marcas
     */
    tmct0cta.setImclecha(new BigDecimal(0));
    tmct0cta.setInforcon(' ');
    tmct0cta.setInsenmod(' ');
    tmct0cta.setInsenfla(' ');
    tmct0cta.setIndatfis(tmct0alo.getIndatfis());
    tmct0cta.setErdatfis(tmct0alo.getErdatfis());
    tmct0cta.setCddatliq(tmct0alo.getCddatliq());
    tmct0cta.setCdbloque(' ');
    tmct0cta.setDsobserv("");
    tmct0cta.setCdusublo("");

    tmct0cta.setNuoprout(tmct0alo.getNuoprout());

    /*
     * porcentajes calculados
     */
    tmct0cta.setPotaxbrk(new BigDecimal(0));

    tmct0cta.setCdentliq("");
    tmct0cta.setCdrefban(tmct0alo.getTmct0bok().getTmct0ord().getNureford());

    /*
     * xas
     */
    tmct0cta.setCdstcexc("");

    tmct0cta.setImcamalo(new BigDecimal(0));
    tmct0cta.setImmarpri(new BigDecimal(0));

    /*
     * campos que recogemos de la orden
     */
    tmct0cta.setCdisin(tmct0alo.getTmct0bok().getTmct0ord().getCdisin());
    tmct0cta.setNbvalors(tmct0alo.getTmct0bok().getTmct0ord().getNbvalors());

    // setImtotbru(tmct0alo.getImtotbru());
    // setEutotbru(tmct0alo.getEutotbru());
    // setImnetbrk(tmct0alo.getImnetbrk());
    // setEunetbrk(tmct0alo.getEunetbrk());
    // setImfibrdv(tmct0alo.getImfibrdv());
    // setImfibreu(tmct0alo.getImfibreu());
    // setImnetobr(tmct0alo.getImnetbrk());

    tmct0cta.setCdalias(tmct0alo.getCdalias());
    tmct0cta.setDescrali(tmct0alo.getDescrali());

    /*
     * campos pk
     */
    tmct0cta.setPkbroker1(0);
    tmct0cta.setPkentity("");

    tmct0cta.setNupareje(tmct0alo.getNupareje());

    /*
     * porcentajes calculados
     */
    try {
      tmct0cta.setPodevolu(tmct0cta.getImcomdev().divide(tmct0cta.getImbrmerc()).multiply(new BigDecimal(100))
          .setScale(6, RoundingMode.HALF_UP));
    }
    catch (Exception e) {
      LOG.trace(e.getMessage(), e);
      tmct0cta.setPodevolu(BigDecimal.ZERO);
    }

    tmct0cta.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0cta.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);

    if ('E' == tmct0cta.getIndatfis()) {
      tmct0cta.getTmct0ctg().setIndatfis('E');
      tmct0cta.getTmct0ctg().setErdatfis("ERROR EN DATOS FISCALES");
    }
    else if ('N' == tmct0cta.getIndatfis()) {
      tmct0cta.getTmct0ctg().setIndatfis('N');
      tmct0cta.getTmct0ctg().setErdatfis("NO EXISTEN DATOS FISCALES");
    }

    if ('N' == tmct0cta.getCddatliq()) {
      tmct0cta.getTmct0ctg().setCddatliq('N');
    }

    tmct0cta.setImgasliqdv(tmct0gal.getImgasliqdvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgasliqeu(tmct0gal.getImgasliqeub().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setImgasgesdv(tmct0gal.getImgasgesdvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgasgeseu(tmct0gal.getImgasgeseub().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setImgastotdv(tmct0gal.getImgastotdvb().setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgastoteu(tmct0gal.getImgastoteub().setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setImnetobr(xmlOrdenHelper.calculateGrossCenter(tmct0cta));

    tmct0cta.setTmct0sta(new Tmct0sta(new Tmct0staId(TypeCdestado.NEW_REGISTER.getValue(), TypeCdtipest.TMCT0CTA
        .getValue())));

  }

  public void accumulateAdd(Tmct0gal tmct0gal, Tmct0alo tmct0alo, Tmct0cta tmct0cta) throws XmlOrdenExceptionReception {

    tmct0cta.setImtotbru(tmct0cta.getImtotbru().add(tmct0gal.getImbrmerc()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImtotnet(tmct0cta.getImtotnet().add(tmct0gal.getImtotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setEutotbru(tmct0cta.getEutotbru().add(tmct0gal.getImbrmreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setEutotnet(tmct0cta.getEutotnet().add(tmct0gal.getEutotnet()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImnetbrk(new BigDecimal(0));
    tmct0cta.setEunetbrk(new BigDecimal(0));
    tmct0cta.setImcomisn(tmct0cta.getImcomisn().add(tmct0gal.getImcomisn()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcomieu(tmct0cta.getImcomieu().add(tmct0gal.getImcomieu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcomdev(tmct0cta.getImcomdev().add(tmct0gal.getImcomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setEucomdev(tmct0cta.getEucomdev().add(tmct0gal.getEucomdev()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImfibrdv(tmct0cta.getImfibrdv().add(tmct0gal.getImfibrdv()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImfibreu(tmct0cta.getImfibreu().add(tmct0gal.getImfibreu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImsvb(tmct0cta.getImsvb().add(tmct0gal.getImsvb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImsvbeu(tmct0cta.getImsvbeu().add(tmct0gal.getImsvbeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImbrmerc(tmct0cta.getImbrmerc().add(tmct0gal.getImbrmercb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImbrmreu(tmct0cta.getImbrmreu().add(tmct0gal.getImbrmreub()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgastos(tmct0cta.getImgastos().add(tmct0gal.getImgastosb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgatdvs(tmct0cta.getImgatdvs().add(tmct0gal.getImgatdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImimpdvs(tmct0cta.getImimpdvs().add(tmct0gal.getImimpdvsb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImimpeur(tmct0cta.getImimpeur().add(tmct0gal.getImimpeurb()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcconeu(tmct0cta.getImcconeu().add(tmct0gal.getImcconeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImcliqeu(tmct0cta.getImcliqeu().add(tmct0gal.getImcliqeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImdereeu(tmct0cta.getImdereeu().add(tmct0gal.getImdereeu()).setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setNutitagr(tmct0cta.getNutitagr().add(tmct0gal.getNutitagr()).setScale(0, BigDecimal.ROUND_HALF_UP));

    try {
      tmct0cta.setPodevolu(tmct0cta.getImcomdev().divide(tmct0cta.getImbrmerc()).multiply(new BigDecimal(100))
          .setScale(6, RoundingMode.HALF_UP));
    }
    catch (Exception e) {
      LOG.trace(e.getMessage(), e);
      tmct0cta.setPodevolu(BigDecimal.ZERO);
    }

    tmct0cta.setFhaudit(new Timestamp(System.currentTimeMillis()));

    tmct0cta.setImgasliqdv(tmct0cta.getImgasliqdv().add(tmct0gal.getImgasliqdvb())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgasliqeu(tmct0cta.getImgasliqeu().add(tmct0gal.getImgasliqeub())
        .setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setImgasgesdv(tmct0cta.getImgasgesdv().add(tmct0gal.getImgasgesdvb())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgasgeseu(tmct0cta.getImgasgeseu().add(tmct0gal.getImgasgeseub())
        .setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setImgastotdv(tmct0cta.getImgastotdv().add(tmct0gal.getImgastotdvb())
        .setScale(8, BigDecimal.ROUND_HALF_UP));
    tmct0cta.setImgastoteu(tmct0cta.getImgastoteu().add(tmct0gal.getImgastoteub())
        .setScale(8, BigDecimal.ROUND_HALF_UP));

    tmct0cta.setImnetobr(xmlOrdenHelper.calculateGrossCenter(tmct0cta));

  }

}
