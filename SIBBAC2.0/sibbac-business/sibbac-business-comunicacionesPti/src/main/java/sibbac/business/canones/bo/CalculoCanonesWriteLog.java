package sibbac.business.canones.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Service
public class CalculoCanonesWriteLog {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CalculoCanonesWriteLog.class);

  @Autowired
  private CalculoCanonesNacionalBo calculoCanonesNacionalBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0logBo logBo;

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void bajaCanonCalculadoDesglosesBaja(Tmct0alcId alcId, String auditUser) {
    try {
      calculoCanonesNacionalBo.bajaCanonCalculadoDesglosesBajaNewTransaction(alcId, auditUser);
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      String msg = String.format("Alc: '%s' No se ha podido dar de baja el canon agrupado. Err: '%s'",
          alcId.getNucnfliq(), e.getMessage());

      LOG.warn("{} {}", alcId, msg);
      logBo.insertarRegistroNewTransaction(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "",
          auditUser);

    }
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void calcularCanon(Tmct0alcId alcId, CanonesConfigDTO reglasCanones, String auditUser) {
    try {
      calculoCanonesNacionalBo.calcularCanonForBatchProcessNewTransaction(alcId, reglasCanones, auditUser);
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      String msg = String.format("Alc: '%s' No se han podido calcular el canon agrupado. Err: '%s'",
          alcId.getNucnfliq(), e.getMessage());

      LOG.warn("{} {}", alcId, msg);
      logBo.insertarRegistroNewTransaction(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "",
          auditUser);

      alcBo.updateFlagCanonCalculadoByAlcIdNewTransaction(alcId, 'E', auditUser);

    }
  }
}
