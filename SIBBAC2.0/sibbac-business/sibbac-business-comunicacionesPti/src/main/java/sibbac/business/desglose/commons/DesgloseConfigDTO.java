package sibbac.business.desglose.commons;

import java.io.Closeable;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.canones.bo.ReglasCanonesBo;
import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.titulares.bo.TitularesConfigDTO;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.bo.Tcli0comBo;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0ReglaBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0parametrizacionBo;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClearerDTO;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(value = "prototype")
public class DesgloseConfigDTO extends TitularesConfigDTO implements Closeable, Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1177896970088456426L;
  @Autowired
  private ReglasCanonesBo reglasCanonesBo;
  @Autowired
  private Tcli0comBo tclicomBo;
  @Autowired
  private Tmct0ValoresBo valoresBo;
  @Autowired
  private Tmct0aliBo aliasBo;
  @Autowired
  private Tmct0ReglaBo reglaBo;
  @Autowired
  private Tmct0actBo subcuentaBo;
  @Autowired
  private SettlementDataBo settlementDataBo;
  @Autowired
  private Tmct0tfiBo holdersBo;
  @Autowired
  private Tmct0parametrizacionBo parametrizacionAsignacionBo;
  @Autowired
  private Tmct0CompensadorBo compensadorBo;
  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentaCompensacionBo;
  @Autowired
  private Tmct0brkBo brokerBo;
  @Autowired
  private Tmct0cfgBo configuracionBo;

  @Autowired
  private FestivoBo festivoBo;

  @Value("${sibbac.nacional.segmentos.sin.ecc}")
  private String segmentosMercadoSinEcc;

  private final Map<String, Tcli0comDTO> tclicomCached;
  private CanonesConfigDTO canonesConfig;
  private final Map<String, Boolean> mapIsBrokerMtf;
  private final Map<String, SettlementData> cachedSettlements;
  private final Map<String, Tmct0parametrizacionDTO> cachedParametrizacionAsignacion;
  private final Map<String, Tmct0parametrizacionDTO> cachedParametrizacionAsignacionRegla;
  private final Map<String, Character> mapCorretajePtiAlias;
  private final Map<String, Character> mapCorretajePtiCompensadores;
  private final Map<String, Boolean> mapCtaCompensacionConfigEuroCpp;
  private final Map<String, Tmct0Compensador> mapCompensadores;
  private final Map<String, Tmct0CuentasDeCompensacion> mapCuentasCompensacion;
  private final Map<String, Tmct0Regla> mapReglaAsignacion;
  private final Map<String, List<Holder>> mapHolders;

  private final Map<String, List<SettlementDataClearerDTO>> mapClearers;

  private final Map<String, Tmct0Regla> mapReglaAliass;

  private final List<String> listSegmentosSinEcc;

  private Integer cdestadoentrefGtLockUserActivity;
  private List<Integer> listCdestadoasigLockUserActivity;
  private Integer cdestadoasigGtLockUserActivity;

  private TipoDesgloseManual tipoDesgloseManual;

  public DesgloseConfigDTO() {
    this.cachedSettlements = new HashMap<String, SettlementData>();
    this.cachedParametrizacionAsignacion = new HashMap<String, Tmct0parametrizacionDTO>();
    this.cachedParametrizacionAsignacionRegla = new HashMap<String, Tmct0parametrizacionDTO>();
    this.mapIsBrokerMtf = new HashMap<String, Boolean>();
    this.mapCorretajePtiAlias = new HashMap<String, Character>();
    this.mapCorretajePtiCompensadores = new HashMap<String, Character>();
    this.mapCtaCompensacionConfigEuroCpp = new HashMap<String, Boolean>();
    this.listSegmentosSinEcc = new ArrayList<String>();
    this.mapReglaAliass = new HashMap<String, Tmct0Regla>();
    this.mapClearers = new HashMap<String, List<SettlementDataClearerDTO>>();
    this.mapCompensadores = new HashMap<String, Tmct0Compensador>();
    this.mapCuentasCompensacion = new HashMap<String, Tmct0CuentasDeCompensacion>();
    this.mapReglaAsignacion = new HashMap<String, Tmct0Regla>();
    this.mapHolders = new HashMap<String, List<Holder>>();
    this.tclicomCached = new HashMap<String, Tcli0comDTO>();
    this.auditUser = "SIBBAC20";
  }

  @PostConstruct
  public void init() throws SIBBACBusinessException {
    this.canonesConfig = reglasCanonesBo.getCanonesConfig();
    this.listSegmentosSinEcc.addAll(Arrays.asList(segmentosMercadoSinEcc.split(",")));
    this.camaraNacional = configuracionBo.getIdPti();
    this.camaraEuroccp = configuracionBo.getIdEuroCcp();
    this.cdestadoasigGtLockUserActivity = configuracionBo.getCdestadoasigGtLockUserActivity();
    this.cdestadoentrefGtLockUserActivity = configuracionBo.getCdestadoentrecGtLockUserActivity();
    this.listCdestadoasigLockUserActivity = configuracionBo.getListCdestadoasigLockUserActivity();

    this.holidays = festivoBo.findAllFestivosNacionales();
    this.workDaysOfWeek = configuracionBo.getWorkDaysOfWeek();
    this.limiteDiasEnvioTitularesEuroccp = configuracionBo.getLimiteDiasEnvioTitularesEuroccp();
    this.limiteDiasEnvioTitularesNacional = configuracionBo.getLimiteDiasEnvioTitularesPti();
    this.auditUser = "SIBBAC20";
  }

  public Tcli0comDTO getTcli0com(String cdproduc, int cdaliass) {
    return tclicomBo.findTcli0comByCdproducAndNuctaproCached(tclicomCached, cdproduc, cdaliass);
  }

  public SettlementData getSettlementDataNacional(Tmct0alo alo) throws SIBBACBusinessException {
    return settlementDataBo.getSettlmentDataNacional(cachedSettlements, alo);
  }

  public Tmct0parametrizacionDTO getParametrizacionAsignacion(Tmct0alo alo, String camaraCompensacion) {
    return parametrizacionAsignacionBo.getParametrizacion(cachedParametrizacionAsignacion, alo, camaraCompensacion);
  }

  public Tmct0parametrizacionDTO getParametrizacionAsignacion(Tmct0alo alo, BigInteger idRegla,
      String camaraCompensacion) {
    return parametrizacionAsignacionBo.getParametrizacion(cachedParametrizacionAsignacionRegla, alo, idRegla,
        camaraCompensacion);
  }

  public boolean isCdbrokerMtf(String cdbroker) {
    return brokerBo.isCdbrokerMtf(mapIsBrokerMtf, cdbroker);
  }

  public boolean isSinEcc(String marketSegment) {
    return listSegmentosSinEcc.contains(StringUtils.trim(marketSegment));
  }

  public Character getCorretajePtiAlias(String cdalias) throws SIBBACBusinessException {
    return aliasBo.getCorretajePtiChached(mapCorretajePtiAlias, cdalias);
  }

  public Character getCorretajePtiCompensador(String compensador) throws SIBBACBusinessException {
    return compensadorBo.getCorretajePtiChached(mapCorretajePtiCompensadores, compensador);
  }

  public Tmct0Regla getRegla(Tmct0alo alo) throws SIBBACBusinessException {

    return reglaBo.getReglaCached(mapReglaAliass, alo);

  }

  public List<SettlementDataClearerDTO> getClearerNacional(Tmct0alo alo, String clearer) {
    return settlementDataBo.getClearerNacional(mapClearers, alo, clearer);
  }

  public boolean hasEuroCppConfig(String cta) {
    return cuentaCompensacionBo.hasEuroCppConfig(mapCtaCompensacionConfigEuroCpp, cta);
  }

  public Tmct0Compensador getCompensador(String nombre) {
    return compensadorBo.findByNbnombreCached(mapCompensadores, nombre);
  }

  public Tmct0CuentasDeCompensacion getCuentaCompensacion(String codigo) {
    return cuentaCompensacionBo.findByCdodigoCached(mapCuentasCompensacion, codigo);
  }

  public TipoDesgloseManual getTipoDesgloseManual() {
    return tipoDesgloseManual;
  }

  public void setTipoDesgloseManual(TipoDesgloseManual tipoDesgloseManual) {
    this.tipoDesgloseManual = tipoDesgloseManual;
  }

  public Integer getCdestadoentrefGtLockUserActivity() {
    return cdestadoentrefGtLockUserActivity;
  }

  public List<Integer> getListCdestadoasigLockUserActivity() {
    return listCdestadoasigLockUserActivity;
  }

  public Integer getCdestadoasigGtLockUserActivity() {
    return cdestadoasigGtLockUserActivity;
  }

  public List<Holder> findAllHoldersByIndentificacion(String identificacion, BigDecimal fetrade) {
    return holdersBo.findAllHoldersByCdholderCached(mapHolders, identificacion, fetrade);
  }

  public List<Holder> findAllHoldersByRazonSocialAndJuridico(String razonSozial, BigDecimal fetrade) {
    return holdersBo.findAllHoldersByNbclient1AndJuridicoCached(mapHolders, razonSozial, fetrade);
  }

  public CanonesConfigDTO getCanonesConfig() {
    return canonesConfig;
  }

  @Override
  public void close() {
    tclicomCached.clear();
    cachedSettlements.clear();
    cachedParametrizacionAsignacion.clear();
    mapIsBrokerMtf.clear();
    mapCorretajePtiAlias.clear();
    mapCorretajePtiCompensadores.clear();
    mapCtaCompensacionConfigEuroCpp.clear();
    cachedParametrizacionAsignacionRegla.clear();
    mapReglaAliass.clear();
    mapClearers.clear();
    mapCompensadores.clear();
    mapCuentasCompensacion.clear();
    mapReglaAsignacion.clear();
    mapHolders.clear();
    canonesConfig.close();
    super.close();
  }
}
