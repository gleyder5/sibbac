package sibbac.business.comunicaciones.page.query;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.page.query.dto.DesgloseclitTraspasosDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.SimpleDynamicFilter.SimpleDynamicFilterComparatorType;
import sibbac.database.StringDynamicFilter;

@Component("Tmct0alcByInformacionCompensacionTraspasosPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Tmct0alcByInformacionCompensacionTraspasosPageQuery extends AbstractDynamicPageQuery {

  private static final String NBOOKING = "B.NBOOKING";
  private static final String REF_CLIENTE = "A.REF_CLIENTE";
  private static final String FECHA_CONTRATACION = "B.FETRADE";
  private static final String FECHA_LIQUIDACION = "B.FEVALOR";
  private static final String ALIAS = "B.CDALIAS";
  private static final String ISIN = "B.CDISIN";
  private static final String COMPRA_VENTA = "B.CDTPOPER";

  private static final DynamicColumn[] COLUMNS = {
      new DynamicColumn("F. Contratación", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("F. Liquidación", "FEVALOR", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Sentido", "SENTIDO"), 
      new DynamicColumn("Estado", "ESTADO"),
      new DynamicColumn("Alias", "ALIAS"),
      new DynamicColumn("Desc. Alias", "DESC_ALIAS"),
      new DynamicColumn("Isin", "ISIN"),
      new DynamicColumn("Desc. Isin", "DESC_ISIN"), 
      new DynamicColumn("Camara", "CAMARA"),
      new DynamicColumn("Cuenta", "CUENTA_COMPENSACION_DESTINO"),
      new DynamicColumn("Compensador", "MIEMBRO_COMPENSADOR_DESTINO"), 
      new DynamicColumn("Desc. Compensador", "DESC_MIEMBRO_COMPENSADOR_DESTINO"),
      new DynamicColumn("Ref. Give-Up", "CD_REF_ASIGNACION"),
      new DynamicColumn("Precio", "PRECIO", DynamicColumn.ColumnType.N6),
      new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Efectivo", "EFECTIVO", DynamicColumn.ColumnType.N4),
      new DynamicColumn("N. Orden", "NUORDEN"), 
      new DynamicColumn("N. Booking", "NBOOKING"),
      new DynamicColumn("N. Desg. Cliente", "NUCNFCLT"), 
      new DynamicColumn("N. Desg. Liquidador", "NUCNFLIQ")
    };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new DateDynamicFilter(FECHA_CONTRATACION, "F. Contratación"));
    res.add(new DateDynamicFilter(FECHA_LIQUIDACION, "F. Liquidación"));
    res.add(new StringDynamicFilter(COMPRA_VENTA, "Sentido"));
    res.add(new StringDynamicFilter(REF_CLIENTE, "Referencia cliente"));
    res.add(new StringDynamicFilter(NBOOKING, "Número booking"));
    res.add(new StringDynamicFilter(ALIAS, "Alias"));
    res.add(new StringDynamicFilter(ISIN, "ISIN"));
    return res;
  };

  public Tmct0alcByInformacionCompensacionTraspasosPageQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  @Override
  public String getSelect() {
    StringBuilder sb = new StringBuilder()
      .append("SELECT DC.FECONTRATACION FETRADE, DC.FELIQUIDACION FEVALOR, B.CDTPOPER SENTIDO,  E.NOMBRE AS ESTADO, B.CDALIAS ALIAS, B.DESCRALI DESC_ALIAS, B.CDISIN ISIN, B.NBVALORS DESC_ISIN,  CC.CD_CODIGO CAMARA, ")
      .append("DCT.CUENTA_COMPENSACION_DESTINO, DCT.MIEMBRO_COMPENSADOR_DESTINO, COALESCE(MC.NB_DESCRIPCION,'') AS DESC_MIEMBRO_COMPENSADOR_DESTINO, ")
      .append("DCT.CD_REF_ASIGNACION, AVG(DCT.IMPRECIO) AS PRECIO, SUM(DCT.IMTITULOS) AS TITULOS, SUM(DCT.IMEFECTIVO) AS EFECTIVO, ")
      .append("C.NUORDEN, C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ ");
    return sb.toString();
  }

  @Override
  public String getFrom() {
    StringBuilder sb = new StringBuilder()
        .append("FROM  TMCT0BOK B INNER JOIN TMCT0ALO A ON B.NUORDEN = A.NUORDEN AND B.NBOOKING = A.NBOOKING ")
        .append(" INNER JOIN  TMCT0ALC C ON A.NUORDEN = C.NUORDEN AND A.NBOOKING = C.NBOOKING AND A.NUCNFCLT = C.NUCNFCLT ")
        .append(" INNER JOIN TMCT0DESGLOSECAMARA DC ON C.NUORDEN = DC.NUORDEN AND C.NBOOKING = DC.NBOOKING AND C.NUCNFCLT=DC.NUCNFCLT AND C.NUCNFLIQ = DC.NUCNFLIQ ")
        .append(" INNER JOIN TMCT0_CAMARA_COMPENSACION CC ON  DC.IDCAMARA = CC.ID_CAMARA_COMPENSACION ")
        .append(" INNER JOIN TMCT0DESGLOSECLITIT DCT ON DC.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA ")
        .append(" INNER JOIN TMCT0ESTADO E ON B.CDESTADOASIG = E.IDESTADO ")
        .append(" LEFT OUTER JOIN TMCT0_COMPENSADOR MC ON DCT.MIEMBRO_COMPENSADOR_DESTINO =  MC.NB_NOMBRE ");
    return sb.toString();
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {
    String casePti = "'S'";
    String ctaCompensacion = "''";
    StringBuilder sb = new StringBuilder(" B.CDESTADOASIG > {0} AND B.CASEPTI = {1} AND DC.CDESTADOASIG > {0} ");
    sb.append(" AND B.CDESTADOASIG > {0} AND B.CASEPTI = {1} AND DC.CDESTADOASIG > {0} AND DC.CDESTADOASIG <> {2} ")
        .append(" AND DC.CDESTADOASIG <> {3} AND DC.CDESTADOASIG <> {4} AND DC.CDESTADOASIG <> {5} ")
        .append(" AND DC.CDESTADOASIG <> {6} AND DC.CDESTADOASIG <> {7} AND DCT.CUENTA_COMPENSACION_DESTINO <> {8} ");
    String postWhere = MessageFormat.format(sb.toString(), EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(), casePti,
        EstadosEnumerados.ASIGNACIONES.PDTE_ACEPTAR_COMPENSADOR.getId(),
        EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId(),
        EstadosEnumerados.ASIGNACIONES.ENVIANDOSE.getId(), EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_S3.getId(),
        EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO.getId(),
        EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_ANULACION.getId(), ctaCompensacion);
    return postWhere;

  }

  @Override
  public String getGroup() {
    StringBuilder sb = new StringBuilder(
        " GROUP BY C.NUORDEN, C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ, DC.FECONTRATACION, DC.FELIQUIDACION, B.CDTPOPER,  B.CDISIN, B.NBVALORS,  CC.CD_CODIGO, DCT.CUENTA_COMPENSACION_DESTINO, ");
    sb.append("DCT.MIEMBRO_COMPENSADOR_DESTINO, COALESCE(MC.NB_DESCRIPCION,''), DCT.CD_REF_ASIGNACION, B.CDALIAS, B.DESCRALI, E.IDESTADO, E.NOMBRE ");
    return sb.toString();
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @SuppressWarnings("unchecked")
  public void setNbookingFilter(String nbooking) {
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      if (filter.getField().equals(NBOOKING)) {
        filter.setComparator(SimpleDynamicFilterComparatorType.EQ);
        ((SimpleDynamicFilter<String>) filter).getValues().add(nbooking);
        break;
      }
    }
  }

  public void clearNbookingFilter() {
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      if (filter.getField().equals(NBOOKING)) {
        filter.setComparator(null);
        filter.getValues().clear();
        break;
      }
    }
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  protected DesgloseclitTraspasosDTO loadFromArray(Object[] o) {
    Date fechaContratacion = (Date) o[0];
    Date fechaLiquidacion = (Date) o[1];
    String cdoperacionecc = null;
    String nuoperacionprev = null;
    String nuoperacioninic = null;
    String ordenMercado = null;
    String ejecucionMercado = null;
    Character sentido = (Character) o[2];
    String cdisin = (String) o[3];
    String descripcionIsin = (String) o[4];
    String camaraCompensacion = (String) o[5];
    String cuenta = (String) o[6];
    String miembroCompensador = (String) o[7];
    String nombreMiembroCompensador = (String) o[8];
    String referenciaAsignacionExterna = (String) o[9];
    BigDecimal titulos = (BigDecimal) o[10];
    BigDecimal precio = null;
    BigDecimal efectivo = (BigDecimal) o[11];
    String nuorden = (String) o[12];
    String nbooking = (String) o[13];
    String nucnfclt = (String) o[14];
    Short nucnfliq = (Short) o[15];
    Long iddesglosecamara = null;
    Long nudesglose = null;

    DesgloseclitTraspasosDTO dto = new DesgloseclitTraspasosDTO(fechaContratacion, fechaLiquidacion, cdoperacionecc,
        nuoperacionprev, nuoperacioninic, ordenMercado, ejecucionMercado, sentido, cdisin, descripcionIsin,
        camaraCompensacion, cuenta, miembroCompensador, nombreMiembroCompensador, referenciaAsignacionExterna, titulos,
        precio, efectivo, nuorden, nbooking, nucnfclt, nucnfliq, iddesglosecamara, nudesglose);

    return dto;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    boolean lanzarEx = true;
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      switch (filter.getField()) {
        case NBOOKING:
        case REF_CLIENTE:
        case FECHA_CONTRATACION:
        case FECHA_LIQUIDACION:

          if (!filter.getValues().isEmpty()) {
            lanzarEx = false;
          }
          break;

        default:
          break;
      }
      if (!lanzarEx) {
        break;
      }
    }
    if (lanzarEx) {
      throw new SIBBACBusinessException("Mandatory filter not set.");
    }

  }

}
