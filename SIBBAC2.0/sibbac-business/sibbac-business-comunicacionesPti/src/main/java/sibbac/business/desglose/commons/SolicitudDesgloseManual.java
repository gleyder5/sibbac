package sibbac.business.desglose.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0alcId;

public class SolicitudDesgloseManual implements Serializable {

  /**
   * Numero de serie generado
   */
  private static final long serialVersionUID = -5834015334007522402L;

  /**
   * id del allo que se va a desglosar
   * **/
  private Tmct0aloId tmct0aloId;

  private Character sentido;

  // ROUTING
  private String refCliente;

  private String isin;
  private String descripcionIsin;

  private String alias;
  private String descripcionAlias;

  private String origenOrden;
  private String tipoOrden;

  private Date fechaLiquidacion;
  private Date fechaContratacion;

  private String mercado;
  private String moneda;
  private String canal;

  private BigDecimal titulosOrdenados;
  private BigDecimal titulosEjecutados;
  private BigDecimal titulosAlo;

  /**
   * Tipo de reparto de titulos que se va a realizar
   * */
  private AlgoritmoDesgloseManual algoritmoDesgloseManual;

  /**
   * Position break down, si la ilq es P-PROVISIONAL/D-DEFINITIVA/M-MANUAL al
   * tratarse de un desglose manual M no aplica
   * */
  private Character positionBreakDown;

  /**
   * Desglose manual solicitado
   * 
   */
  private List<DesgloseManualDTO> altas = new ArrayList<DesgloseManualDTO>();

  /**
   * Lista de los identificadores de alc que se darán de baja.
   */
  private List<Tmct0alcId> bajas = new ArrayList<Tmct0alcId>();

  public List<DesgloseManualDTO> getAltas() {
    return altas;
  }

  public List<Tmct0alcId> getBajas() {
    return bajas;
  }

  public void setAltas(List<DesgloseManualDTO> altas) {
    this.altas = altas;
  }

  public void setBajas(List<Tmct0alcId> bajas) {
    this.bajas = bajas;
  }

  public Tmct0aloId getTmct0aloId() {
    return tmct0aloId;
  }

  public AlgoritmoDesgloseManual getAlgoritmoDesgloseManual() {
    return algoritmoDesgloseManual;
  }

  public void setTmct0aloId(Tmct0aloId tmct0aloId) {
    this.tmct0aloId = tmct0aloId;
  }

  public void setAlgoritmoDesgloseManual(AlgoritmoDesgloseManual algoritmoDesgloseManual) {
    this.algoritmoDesgloseManual = algoritmoDesgloseManual;
  }

  public Character getPositionBreakDown() {
    return positionBreakDown;
  }

  public void setPositionBreakDown(Character positionBreakDown) {
    this.positionBreakDown = positionBreakDown;
  }

  public String getRefCliente() {
    return refCliente;
  }

  public String getIsin() {
    return isin;
  }

  public String getDescripcionIsin() {
    return descripcionIsin;
  }

  public String getAlias() {
    return alias;
  }

  public String getDescripcionAlias() {
    return descripcionAlias;
  }

  public String getOrigenOrden() {
    return origenOrden;
  }

  public String getTipoOrden() {
    return tipoOrden;
  }

  public void setRefCliente(String refCliente) {
    this.refCliente = refCliente;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setDescripcionIsin(String descripcionIsin) {
    this.descripcionIsin = descripcionIsin;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public void setDescripcionAlias(String descripcionAlias) {
    this.descripcionAlias = descripcionAlias;
  }

  public void setOrigenOrden(String origenOrden) {
    this.origenOrden = origenOrden;
  }

  public void setTipoOrden(String tipoOrden) {
    this.tipoOrden = tipoOrden;
  }

  public Character getSentido() {
    return sentido;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public Date getFechaContratacion() {
    return fechaContratacion;
  }

  public String getMercado() {
    return mercado;
  }

  public String getMoneda() {
    return moneda;
  }

  public String getCanal() {
    return canal;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  public void setFechaContratacion(Date fechaContratacion) {
    this.fechaContratacion = fechaContratacion;
  }

  public void setMercado(String mercado) {
    this.mercado = mercado;
  }

  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }

  public void setCanal(String canal) {
    this.canal = canal;
  }

  public BigDecimal getTitulosOrdenados() {
    return titulosOrdenados;
  }

  public BigDecimal getTitulosEjecutados() {
    return titulosEjecutados;
  }

  public BigDecimal getTitulosAlo() {
    return titulosAlo;
  }

  public void setTitulosOrdenados(BigDecimal titulosOrdenados) {
    this.titulosOrdenados = titulosOrdenados;
  }

  public void setTitulosEjecutados(BigDecimal titulosEjecutados) {
    this.titulosEjecutados = titulosEjecutados;
  }

  public void setTitulosAlo(BigDecimal titulosAlo) {
    this.titulosAlo = titulosAlo;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((algoritmoDesgloseManual == null) ? 0 : algoritmoDesgloseManual.hashCode());
    result = prime * result + ((alias == null) ? 0 : alias.hashCode());
    result = prime * result + ((altas == null) ? 0 : altas.hashCode());
    result = prime * result + ((bajas == null) ? 0 : bajas.hashCode());
    result = prime * result + ((canal == null) ? 0 : canal.hashCode());
    result = prime * result + ((descripcionAlias == null) ? 0 : descripcionAlias.hashCode());
    result = prime * result + ((descripcionIsin == null) ? 0 : descripcionIsin.hashCode());
    result = prime * result + ((fechaContratacion == null) ? 0 : fechaContratacion.hashCode());
    result = prime * result + ((fechaLiquidacion == null) ? 0 : fechaLiquidacion.hashCode());
    result = prime * result + ((isin == null) ? 0 : isin.hashCode());
    result = prime * result + ((mercado == null) ? 0 : mercado.hashCode());
    result = prime * result + ((moneda == null) ? 0 : moneda.hashCode());
    result = prime * result + ((origenOrden == null) ? 0 : origenOrden.hashCode());
    result = prime * result + ((positionBreakDown == null) ? 0 : positionBreakDown.hashCode());
    result = prime * result + ((refCliente == null) ? 0 : refCliente.hashCode());
    result = prime * result + ((sentido == null) ? 0 : sentido.hashCode());
    result = prime * result + ((tipoOrden == null) ? 0 : tipoOrden.hashCode());
    result = prime * result + ((titulosAlo == null) ? 0 : titulosAlo.hashCode());
    result = prime * result + ((titulosEjecutados == null) ? 0 : titulosEjecutados.hashCode());
    result = prime * result + ((titulosOrdenados == null) ? 0 : titulosOrdenados.hashCode());
    result = prime * result + ((tmct0aloId == null) ? 0 : tmct0aloId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SolicitudDesgloseManual other = (SolicitudDesgloseManual) obj;
    if (algoritmoDesgloseManual != other.algoritmoDesgloseManual)
      return false;
    if (alias == null) {
      if (other.alias != null)
        return false;
    }
    else if (!alias.equals(other.alias))
      return false;
    if (altas == null) {
      if (other.altas != null)
        return false;
    }
    else if (!altas.equals(other.altas))
      return false;
    if (bajas == null) {
      if (other.bajas != null)
        return false;
    }
    else if (!bajas.equals(other.bajas))
      return false;
    if (canal == null) {
      if (other.canal != null)
        return false;
    }
    else if (!canal.equals(other.canal))
      return false;
    if (descripcionAlias == null) {
      if (other.descripcionAlias != null)
        return false;
    }
    else if (!descripcionAlias.equals(other.descripcionAlias))
      return false;
    if (descripcionIsin == null) {
      if (other.descripcionIsin != null)
        return false;
    }
    else if (!descripcionIsin.equals(other.descripcionIsin))
      return false;
    if (fechaContratacion == null) {
      if (other.fechaContratacion != null)
        return false;
    }
    else if (!fechaContratacion.equals(other.fechaContratacion))
      return false;
    if (fechaLiquidacion == null) {
      if (other.fechaLiquidacion != null)
        return false;
    }
    else if (!fechaLiquidacion.equals(other.fechaLiquidacion))
      return false;
    if (isin == null) {
      if (other.isin != null)
        return false;
    }
    else if (!isin.equals(other.isin))
      return false;
    if (mercado == null) {
      if (other.mercado != null)
        return false;
    }
    else if (!mercado.equals(other.mercado))
      return false;
    if (moneda == null) {
      if (other.moneda != null)
        return false;
    }
    else if (!moneda.equals(other.moneda))
      return false;
    if (origenOrden == null) {
      if (other.origenOrden != null)
        return false;
    }
    else if (!origenOrden.equals(other.origenOrden))
      return false;
    if (positionBreakDown == null) {
      if (other.positionBreakDown != null)
        return false;
    }
    else if (!positionBreakDown.equals(other.positionBreakDown))
      return false;
    if (refCliente == null) {
      if (other.refCliente != null)
        return false;
    }
    else if (!refCliente.equals(other.refCliente))
      return false;
    if (sentido == null) {
      if (other.sentido != null)
        return false;
    }
    else if (!sentido.equals(other.sentido))
      return false;
    if (tipoOrden == null) {
      if (other.tipoOrden != null)
        return false;
    }
    else if (!tipoOrden.equals(other.tipoOrden))
      return false;
    if (titulosAlo == null) {
      if (other.titulosAlo != null)
        return false;
    }
    else if (!titulosAlo.equals(other.titulosAlo))
      return false;
    if (titulosEjecutados == null) {
      if (other.titulosEjecutados != null)
        return false;
    }
    else if (!titulosEjecutados.equals(other.titulosEjecutados))
      return false;
    if (titulosOrdenados == null) {
      if (other.titulosOrdenados != null)
        return false;
    }
    else if (!titulosOrdenados.equals(other.titulosOrdenados))
      return false;
    if (tmct0aloId == null) {
      if (other.tmct0aloId != null)
        return false;
    }
    else if (!tmct0aloId.equals(other.tmct0aloId))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Solicitud desglose manual aloId: [%s], bajas: [%s], altas: [%s], algoritmo: [%s]",
        tmct0aloId, altas.toString(), bajas.toString(), algoritmoDesgloseManual);
  }

}
