package sibbac.business.comunicaciones.euroccp.common;


public class EuroCcpCommonSendRegisterDTO {

  public final EuroCcpCommonSendRegisterTypeEnum type;

  public EuroCcpCommonSendRegisterDTO(EuroCcpCommonSendRegisterTypeEnum type) {
    this.type = type;
  }

}
