package sibbac.business.comunicaciones.pti.ce.bo;

import java.util.concurrent.Callable;

import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.pti.PTIMessageVersion;

@Component
@Scope(value = "prototype")
public class EnvioCeCallable implements Callable<Void> {

  private ConnectionFactory cf;
  private String destination, cdoperacionecc;
  private PTIMessageVersion version;
  private Tmct0xasAndTmct0cfgConfig cache;

  @Autowired
  private EnvioCeBo envioCe;

  public EnvioCeCallable(ConnectionFactory cf, String destination, String cdoperacionecc, PTIMessageVersion version,
      Tmct0xasAndTmct0cfgConfig cache) {
    this.cf = cf;
    this.destination = destination;

    this.cdoperacionecc = cdoperacionecc;
    this.version = version;
    this.cache = cache;
  }

  @Override
  public Void call() throws Exception {
    envioCe.enviarCe(cf, destination, cdoperacionecc, version, cache);
    return null;
  }

}
