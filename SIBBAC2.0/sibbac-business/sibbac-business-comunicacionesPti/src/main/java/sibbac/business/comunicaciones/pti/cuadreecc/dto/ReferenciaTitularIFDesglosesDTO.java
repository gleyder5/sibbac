package sibbac.business.comunicaciones.pti.cuadreecc.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.model.Tmct0alc;

public class ReferenciaTitularIFDesglosesDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8222453375801440644L;

  private final ReferenciaTitularIFDTO referenciaTitular;

  private BigDecimal titulos = BigDecimal.ZERO;
  private List<DesgloseIFDTO> desgloses = new ArrayList<DesgloseIFDTO>();

  private Long idEccMessgeInTi;

  private String nuorden, nbooking, nucnflt;
  private short nucnfliq;

  
  private Tmct0alc desgloseRelacionadoAlc;

  private DesgloseRecordDTO datosDesgloseIF;

  public ReferenciaTitularIFDesglosesDTO(ReferenciaTitularIFDTO referenciaTitular) {
    super();
    this.referenciaTitular = referenciaTitular;
  }

  public ReferenciaTitularIFDTO getReferenciaTitular() {
    return referenciaTitular;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public List<DesgloseIFDTO> getDesgloses() {
    return desgloses;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setDesgloses(List<DesgloseIFDTO> desgloses) {
    this.desgloses = desgloses;
  }

  public Long getIdEccMessgeInTi() {
    return idEccMessgeInTi;
  }

  public void setIdEccMessgeInTi(Long idEccMessgeInTi) {
    this.idEccMessgeInTi = idEccMessgeInTi;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNucnflt() {
    return nucnflt;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNucnflt(String nucnflt) {
    this.nucnflt = nucnflt;
  }

  public short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public Tmct0alc getDesgloseRelacionado() {
    return desgloseRelacionadoAlc;
  }

  public void setDesgloseRelacionado(Tmct0alc desgloseRelacionado) {
    this.desgloseRelacionadoAlc = desgloseRelacionado;
  }

  public DesgloseRecordDTO getDatosDesgloseIF() {
    return datosDesgloseIF;
  }

  public void setDatosDesgloseIF(DesgloseRecordDTO datosDesgloseIF) {
    this.datosDesgloseIF = datosDesgloseIF;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((desgloses == null) ? 0 : desgloses.hashCode());
    result = prime * result + ((referenciaTitular == null) ? 0 : referenciaTitular.hashCode());
    result = prime * result + ((titulos == null) ? 0 : titulos.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ReferenciaTitularIFDesglosesDTO other = (ReferenciaTitularIFDesglosesDTO) obj;
    if (desgloses == null) {
      if (other.desgloses != null)
        return false;
    } else if (!desgloses.equals(other.desgloses))
      return false;
    if (referenciaTitular == null) {
      if (other.referenciaTitular != null)
        return false;
    } else if (!referenciaTitular.equals(other.referenciaTitular))
      return false;
    if (titulos == null) {
      if (other.titulos != null)
        return false;
    } else if (titulos.compareTo(other.titulos) != 0)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "ReferenciaTitularIFDesglosesDTO [referenciaTitular=" + referenciaTitular + ", titulos=" + titulos
           + ", desgloses=" + desgloses + "]";
  }

}
