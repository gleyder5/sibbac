package sibbac.business.comunicaciones.euroccp.titulares.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReferenceStaticData;

@Component
public interface EuroCcpOwnershipReferenceStaticDataDao extends
    JpaRepository<EuroCcpOwnershipReferenceStaticData, Long> {

  @Query("SELECT MIN(s.firstSentDate) FROM EuroCcpOwnershipReferenceStaticData s WHERE s.ownerReference = :pownerReference AND s.estadoProcesado >= :pestadoProcesadoGte")
  Date minFirstSentDateByOwnerReferenceAndEstadoProcesadoGte(@Param("pownerReference") String ownerReference,
      @Param("pestadoProcesadoGte") int estadoProcesadoGte);

  @Query("SELECT s FROM EuroCcpOwnershipReferenceStaticData s WHERE s.ownerReference = :pownerReference AND s.estadoProcesado >= :pestadoProcesadoGte")
  List<EuroCcpOwnershipReferenceStaticData> findByOwnerReferenceAndEstadoProcesadoGte(
      @Param("pownerReference") String ownerReference, @Param("pestadoProcesadoGte") int estadoProcesadoGte);

  @Query("SELECT s FROM EuroCcpOwnershipReferenceStaticData s WHERE s.ownerReference = :pownerReference AND s.estadoProcesado = :pestadoProcesadoEq")
  List<EuroCcpOwnershipReferenceStaticData> findByOwnerReferenceAndEstadoProcesadoEq(
      @Param("pownerReference") String ownerReference, @Param("pestadoProcesadoEq") int estadoProcesadoEq);

  @Query("SELECT s FROM EuroCcpOwnershipReferenceStaticData s WHERE s.ownerReference = :pownerReference AND s.identificationCode = :identificationCode AND s.estadoProcesado >= :pestadoProcesadoGte")
  EuroCcpOwnershipReferenceStaticData findByOwnerReferenceAndIdentificationCodeAndEstadoProcesadoGte(
      @Param("pownerReference") String ownerReference, @Param("identificationCode") String identificationCode,
      @Param("pestadoProcesadoGte") int estadoProcesadoGte);

}
