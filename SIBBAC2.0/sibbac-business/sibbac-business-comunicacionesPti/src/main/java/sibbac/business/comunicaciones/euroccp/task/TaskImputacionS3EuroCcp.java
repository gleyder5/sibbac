package sibbac.business.comunicaciones.euroccp.task;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.comunicaciones.imputacions3.bo.IManualUserImputacionS3EuroCcp;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_IMPUTACION_S3_EUROCCP, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0/5 10-14,25-29,40-44,55-59 11-17 ? * MON-FRI")
public class TaskImputacionS3EuroCcp extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private IManualUserImputacionS3EuroCcp manualUserImputacionS3EuroCcp;

  @Override
  public TIPO determinarTipoApunte() {
    return manualUserImputacionS3EuroCcp.determinarTipoApunte();
  }

  @Override
  public void executeTask() throws Exception {
    manualUserImputacionS3EuroCcp.executeTask();
  }

}
