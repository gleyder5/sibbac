package sibbac.business.comunicaciones.pti.cuadreecc.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccEjecucion;

public interface CuadreEccEjecucionDao extends JpaRepository<CuadreEccEjecucion, Long> {

  CuadreEccEjecucion findByNumOpAndIdEcc(String numOp, String idEcc);

  List<CuadreEccEjecucion> findByNumOpInicialAndIdEcc(String numOpInicial, String idEcc);

  CuadreEccEjecucion findByIdEccAndNumOpDcvAndFechaNegAndSentido(String idEcc,
                                                                 String numOpDcv,
                                                                 Date fechaNeg,
                                                                 Character sentido);

  Integer countByEstadoProcesado(int procesado);

  Integer deleteByNumOp(String numOp);

  CuadreEccEjecucion findByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNeg(Date FechaNeg,
                                                                                                                                                                String idEcc,
                                                                                                                                                                String isin,
                                                                                                                                                                String miembroMercadoNeg,
                                                                                                                                                                Character sentido,
                                                                                                                                                                String segmentoNeg,
                                                                                                                                                                String codOpMercado,
                                                                                                                                                                String numOrdenMercado,
                                                                                                                                                                String numEjeMercado,
                                                                                                                                                                String plataformaNeg);

  CuadreEccEjecucion findByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNegAndNumOpDcv(Date FechaNeg,
                                                                                                                                                                           String idEcc,
                                                                                                                                                                           String isin,
                                                                                                                                                                           String miembroMercadoNeg,
                                                                                                                                                                           Character sentido,
                                                                                                                                                                           String segmentoNeg,
                                                                                                                                                                           String codOpMercado,
                                                                                                                                                                           String numOrdenMercado,
                                                                                                                                                                           String numEjeMercado,
                                                                                                                                                                           String plataformaNeg,
                                                                                                                                                                           String numOpDcv);

  Integer deleteByFechaNegAndIdEccAndIsinAndMiembroMercadoNegAndSentidoAndSegmentoNegAndCodOpMercadoAndNumOrdenMercadoAndNumEjeMercadoAndPlataformaNeg(Date FechaNeg,
                                                                                                                                                       String idEcc,
                                                                                                                                                       String isin,
                                                                                                                                                       String miembroMercadoNeg,
                                                                                                                                                       Character sentido,
                                                                                                                                                       String segmentoNeg,
                                                                                                                                                       String codOpMercado,
                                                                                                                                                       String numOrdenMercado,
                                                                                                                                                       String numEjeMercado,
                                                                                                                                                       String plataformaNeg);

  @Query("SELECT DISTINCT e.fechaLiq FROM CuadreEccEjecucion e WHERE e.estadoProcesado= :pEstadoProcesado ORDER BY e.fechaLiq ASC ")
  List<Date> findDistinctFechaLiqByEstadoProcesado(@Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT new sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO( e.id) FROM CuadreEccEjecucion e WHERE e.estadoProcesado= :pEstadoProcesado AND e.fechaLiq = :pFechaLiqLte")
  List<CuadreEccIdDTO> findIdByEstadoProcesadoAndFechaLiq(@Param("pEstadoProcesado") int estadoProcesado,
                                                          @Param("pFechaLiqLte") Date fechaLiqLte);

  @Query("SELECT new sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO( e.id) FROM CuadreEccEjecucion e WHERE e.estadoProcesado= :pEstadoProcesado AND e.fechaLiq = :pFechaLiqLte")
  List<CuadreEccIdDTO> findIdByEstadoProcesadoAndFechaLiq(@Param("pEstadoProcesado") int estadoProcesado,
                                                          @Param("pFechaLiqLte") Date fechaLiqLte,
                                                          Pageable page);

  @Query("SELECT new sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO( e.numOpInicial, e.fechaNeg, e.isin, e.sentido, e.idEcc, e.segmentoNeg, e.codOpMercado, "
         + " e.miembroMercadoNeg, e.numOrdenMercado, e.numEjeMercado, e.numOpDcv ) "
         + " FROM CuadreEccEjecucion e WHERE e.estadoProcesado= :pEstadoProcesado AND e.fechaLiq = :pFechaLiqLte "
         + " GROUP BY e.numOpInicial, e.fechaNeg, e.isin, e.sentido, e.idEcc, e.segmentoNeg, e.codOpMercado, "
         + " e.miembroMercadoNeg, e.numOrdenMercado, e.numEjeMercado, e.numOpDcv"
         + " ORDER BY e.numOpInicial, e.fechaNeg, e.isin, e.sentido, e.idEcc, e.segmentoNeg, e.codOpMercado, "
         + " e.miembroMercadoNeg, e.numOrdenMercado, e.numEjeMercado, e.numOpDcv")
  List<CuadreEccIdDTO> findIdentificacionOperacionCuadreEccDTOByEstadoProcesadoAndFechaLiq(@Param("pEstadoProcesado") int estadoProcesado,
                                                                                           @Param("pFechaLiqLte") Date fechaLiq,
                                                                                           Pageable page);

  @Modifying
  @Query("UPDATE CuadreEccEjecucion e set e.estadoProcesado = :pEstadoProcesado, e.auditDate = :audit, e.auditUser = :audituser "
         + " WHERE e.id = :id and e.estadoProcesado <> :pEstadoProcesado ")
  Integer updateEstadoProcesadoById(@Param("pEstadoProcesado") int estadoProcesado,
                                    @Param("audit") Date audit,
                                    @Param("audituser") String audituser,
                                    @Param("id") long id);
}
