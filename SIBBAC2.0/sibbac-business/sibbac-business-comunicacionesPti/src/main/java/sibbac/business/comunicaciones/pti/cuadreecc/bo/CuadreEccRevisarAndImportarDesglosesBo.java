package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.bo.ReglasCanonesBo;
import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.comunicaciones.pti.commons.EnumeradosComunicacionesPti.TipoCuentaIberclear;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.DesgloseIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.IdentificacionOperacionDatosMercadoDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.ReferenciaTitularIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.ReferenciaTitularIFDesglosesDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.AltaTitularRevisionTitularesException;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.CambiarReferenciaTitularRevisionTitularesException;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.CargaDatosRevisionDesglosesIfException;
import sibbac.business.comunicaciones.pti.cuadreecc.exception.InactivacionTitularesRevisionTitularesException;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccSibbac;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.bo.Tmct0grupoejecucionBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.bo.Tmct0referenciaTitularBo;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.data.Tmct0AloDataOperacionesEspecialesPartenon;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Service(value = "cuadreEccRevisarAndImportarDesglosesBo")
public class CuadreEccRevisarAndImportarDesglosesBo {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccRevisarAndImportarDesglosesBo.class);

  @Qualifier(value = "cuadreEccRevisarAndImportarDesglosesSubListBo")
  @Autowired
  private CuadreEccRevisarAndImportarDesglosesSubListBo revisarDesglosesSubList;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0grupoejecucionBo gruBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejealoBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0referenciaTitularBo refTitBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  @Autowired
  private CuadreEccReferenciaBo cuadreEccReferenciaBo;

  @Autowired
  private CuadreEccTitularBo cuadreEccTitularBo;

  @Autowired
  private Tmct0operacioncdeccBo opEccBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private ReglasCanonesBo reglasCanonesBo;

  @Autowired
  private Tmct0xasBo conversionesBo;

  public CuadreEccRevisarAndImportarDesglosesBo() {
  }

  /**
   * Vaya mierda de codigo me esta saliendo
   * 
   * @throws SIBBACBusinessException
   * 
   * */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void cuadreEccRevisarAndImportarDesglosesCuadreEcc(List<CuadreEccDbReaderRecordBeanDTO> beans, int pageSize)
      throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] inicio.");

    // ESTE PROCESO ES UNA PUTA LOCURA, TENGO QUE REHACERLO ENTERO.
    // HAY QUE GRABAR EN LA TABLA ELEMENTO IF EL NUMERO DE TITULOS DEL DESGLOSE
    // QUE HAY EN EL ALO, HAY QUE GRABAR EL
    // NUMERO DE TITULOS QUE VIENE EN EL TI
    // SI ES DIFERENTE ENTONCES ES CUANDO HAY QUE ACTUAR, ADEMAS TAMBIEN TENEMOS
    // COPIA

    // NOTA CUIDADO CON LAS OPERACIONES QUE TIENEN MILLONES DE DESGLOSES PENSAR
    // COMO RESOLVER ESTE PROBLEMA
    Pageable page = new PageRequest(0, pageSize);

    Tmct0alo alo;
    List<CuadreEccSibbac> desglosesAllo;
    List<CuadreEccReferencia> desglosesEcc;
    List<CuadreEccReferencia> desglosesEccAux;
    CuadreEccReferencia cuadreEccReferencia;

    List<DesgloseIFDTO> bajasRealiadas;
    List<DesgloseIFDTO> altasRealiadas;

    List<DesgloseIFDTO> desglosesAloAlta;
    List<DesgloseIFDTO> desglosesEccAlta;
    List<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesEccAlta;
    List<ReferenciaTitularIFDTO> referenciasEccAlta;
    List<ReferenciaTitularIFDTO> referenciasAloAlta;
    List<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesAloAlta;

    List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesAlo;
    Tmct0CamaraCompensacion camaraCompensacionHibernate;

    Date fechaCuadre = new Date();

    Map<String, Tmct0CamaraCompensacion> mapCamarasByCdcodigo = new HashMap<String, Tmct0CamaraCompensacion>();
    CanonesConfigDTO listReglasCanones = reglasCanonesBo.getCanonesConfig();
    try {

      for (CuadreEccDbReaderRecordBeanDTO bean : beans) {
        try {
          LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] procesando {}.", bean);

          bajasRealiadas = new ArrayList<DesgloseIFDTO>();
          altasRealiadas = new ArrayList<DesgloseIFDTO>();

          desglosesEccAlta = new ArrayList<DesgloseIFDTO>();
          desglosesAloAlta = new ArrayList<DesgloseIFDTO>();
          referenciasEccAlta = new ArrayList<ReferenciaTitularIFDTO>();
          referenciasDesglosesEccAlta = new ArrayList<ReferenciaTitularIFDesglosesDTO>();

          referenciasAloAlta = new ArrayList<ReferenciaTitularIFDTO>();
          referenciasDesglosesAloAlta = new ArrayList<ReferenciaTitularIFDesglosesDTO>();

          try {
            alo = aloBo.findById(bean.getTmct0aloId());

            if (alo == null) {
              LOG.warn("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] No encontrado alo: {}", bean);
              logBo.insertarRegistro(bean.getTmct0aloId().getNuorden(), bean.getTmct0aloId().getNbooking(),
                  "CUADRE_ECC-IMPORTAR_DESGLOSES-Alo no encontrado: " + bean.getTmct0aloId().getNucnfclt(), "", "IF");
              continue;
            }

            LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] buscando eleifs...");
            // buscamos los desgloses allo
            desglosesAllo = cuadreEccSibbacBo.findAlos(bean.getTmct0aloId().getNuorden(), bean.getTmct0aloId()
                .getNbooking(), bean.getTmct0aloId().getNucnfclt());

            // TODO REVISAR CUAL ES MAS OPTIMO
            // desglosesEcc =
            // eccMsgTiBo.findByNuordenAndNbookingAndNucnfcltJoinWithReferenciaTitular(alo.getId().getNuorden(),
            // alo.getId().getNbooking(),
            // alo.getId().getNucnfclt());
            desglosesEcc = new ArrayList<CuadreEccReferencia>();
            boolean cuadreEccEstadoIncorrecto = false;
            // buscamos la relacion entre eleif y msg ti
            for (CuadreEccSibbac cuadreEccSibbac : desglosesAllo) {
              if (cuadreEccSibbac.getEstadoProcesado() < EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_IMPORTAR_DESGLOSES_ECC
                  .getId()) {
                cuadreEccEstadoIncorrecto = true;
                break;
              }
              cuadreEccReferencia = findCuadreEccReferenciaByCuadreEccSibbac(cuadreEccSibbac);

              if (cuadreEccReferencia != null) {

                if (cuadreEccSibbac.getNumTitulosDesSibbac().compareTo(cuadreEccReferencia.getNumValoresImpNominal()) > 0) {
                  LOG.warn("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Uno de los desgloses del alo no esta en EccMsgTI");
                  desglosesEcc.clear();
                  break;
                }
                desglosesEcc.add(cuadreEccReferencia);
              }
              else {
                desglosesEcc.clear();
                LOG.warn("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Uno de los desgloses del alo no esta en EccMsgTI");
                break;
              }
            }// fin for eleIF

            if (cuadreEccEstadoIncorrecto) {
              logBo.insertarRegistro(bean.getTmct0aloId().getNuorden(), bean.getTmct0aloId().getNbooking(),
                  "CUADRE_ECC-IMPORTAR_DESGLOSES-INCIDENCIA-Desglose sibbac estado no correcto.", "", "IF");
              cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(alo.getId().getNuorden(), alo
                  .getId().getNbooking(), alo.getId().getNucnfclt(),
                  EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_REPARTO_DESGLOSES_USUARIO.getId());
              continue;
            }

            LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] encontrados {} eleifs.",
                desglosesAllo.size());

            if (CollectionUtils.isEmpty(desglosesEcc) || desglosesEcc.size() != desglosesAllo.size()) {
              desglosesEcc = new ArrayList<CuadreEccReferencia>();
              do {
                desglosesEccAux = cuadreEccReferenciaBo.findByNuordenAndNbookingAndNucnfcltAndCdestadotitLt(alo.getId()
                    .getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(),
                    EstadosEnumerados.TITULARIDADES.CIERRE_IF.getId(), page);
                if (CollectionUtils.isNotEmpty(desglosesEccAux)) {
                  page = page.next();
                  desglosesEcc.addAll(desglosesEccAux);
                }
              }
              while (desglosesEccAux.size() == page.getPageSize());
            }

            if (CollectionUtils.isEmpty(desglosesEcc)) {
              LOG.warn("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] encontrados {} eleifs.",
                  desglosesEcc.size());
              cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(alo.getId().getNuorden(), alo
                  .getId().getNbooking(), alo.getId().getNucnfclt(),
                  EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.PDTE_REVISAR_TITULOS_OPERACIONES_ECC.getId());
              logBo
                  .insertarRegistroNewTransaction(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig(),
                      "CUADRE_ECC-IMPORTAR_DESGLOSES-Revision Titulares desgloses IF. INDIENCIA. No encontrados Desgloses.");
              continue;
            }

            LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] encontrados {} msg ti ecc in...",
                desglosesEcc.size());

            this.organizarInformacionRevisarDesgloses(desglosesAllo, desglosesEcc, desglosesAloAlta, desglosesEccAlta,
                referenciasAloAlta, referenciasEccAlta, referenciasDesglosesAloAlta, referenciasDesglosesEccAlta);
          }
          catch (Exception e1) {
            throw new CargaDatosRevisionDesglosesIfException(
                "CUADRE_ECC-IMPORTAR_DESGLOSES-INCIDENCIA Recuperando datos para el proceso de revision de desglsoes "
                    + bean, e1);
          }

          // TODO REVISAR QUE LOS TITULOOS DE LA TABLA IF SEAN LOS MISMOS QUE EN
          // ALO SI NO INCIDENCIA

          // TODO IGUAL HAY QUE HACER ALGO SI UN ALC ESTA EN DOS CUENTAS
          // DISTINTAS

          // desgloses son los mismos
          if (referenciasDesglosesAloAlta.size() == referenciasDesglosesEccAlta.size()
              && CollectionUtils.containsAll(referenciasDesglosesAloAlta, referenciasDesglosesEccAlta)) {
            LOG.debug(
                "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Las los desgloses por referencia son correctos {}",
                alo.getId());
            logBo.insertarRegistroNewTransaction(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig(),
                "CUADRE_ECC-IMPORTAR_DESGLOSES--Revision Titulares desgloses IF. Desgloses CORRECTOS.");
            revisarTitularesAlcsFromAlo(alo, referenciasDesglosesAloAlta,
                new HashMap<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO>());
            actualizarDesglosesAloAndDesglosesPti(alo.getId(), desglosesAllo, desglosesEcc, fechaCuadre);

            cuadreEccEjecucionBo
                .updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(
                    alo.getId().getNuorden(),
                    alo.getId().getNbooking(),
                    alo.getId().getNucnfclt(),
                    EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.TITULARES_DESGLOSES_OK_PDTE_REVISAR_INFORMACION_ASIGNACION_MOVIMIENTOS
                        .getId());

          }
          else {

            if (alo.getTmct0estadoByCdestadocont().getIdestado() > EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_COBRO_BME
                .getId()) {
              LOG.warn(
                  "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] {} con estado contabilidad no procesable: {}",
                  alo.getId(), alo.getTmct0estadoByCdestadocont().getNombre());

              cuadreEccEjecucionBo.updateEstadoProcesadoByNuordenAndNbookingAndNucnflct(alo.getId().getNuorden(), alo
                  .getId().getNbooking(), alo.getId().getNucnfclt(),
                  EstadosEnumerados.ESTADO_PROCESADO_CUADRE_ECC.ERROR_DESGLOSE_COBRADO.getId());

              logBo.insertarRegistroNewTransaction(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig(),
                  "CUADRE_ECC-IMPORTAR_DESGLOSES-INCIDENCIA-COBRADO. Desgloses NO CORRECTOS.");

              continue;
            }
            else {
              logBo.insertarRegistroNewTransaction(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig(),
                  "CUADRE_ECC-IMPORTAR_DESGLOSES-INCIDENCIA-Desgloses NO CORRECTOS.");
            }

            Collection<ReferenciaTitularIFDesglosesDTO> interseccion = interseccion(alo.getNutitcli(),
                referenciasDesglosesAloAlta, referenciasDesglosesEccAlta);

            Map<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO> equivalentes = recuperarReferenciasEquivalentes(
                alo, interseccion, referenciasDesglosesAloAlta, referenciasDesglosesEccAlta);

            referenciasDesglosesAloAlta.removeAll(interseccion);
            referenciasDesglosesEccAlta.removeAll(interseccion);

            if (CollectionUtils.isNotEmpty(interseccion) && !equivalentes.isEmpty()) {
              LOG.debug(
                  "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Hay referencias que no cambian {}  y referencias que si {} en {}",
                  interseccion.size(), equivalentes.size(), alo.getId());

            }
            // } else {

            Collection<ReferenciaTitularIFDesglosesDTO> altas = new HashSet<ReferenciaTitularIFDesglosesDTO>(
                referenciasDesglosesEccAlta);
            altas.removeAll(interseccion);

            Tmct0AloDataOperacionesEspecialesPartenon aloData = new Tmct0AloDataOperacionesEspecialesPartenon(alo, true);

            // recuperarmos los grupos de ejecucion del alo
            // TODO NO SE SI PODREMOS HACER LA BUSQUEDA ASI, YA QUE CADA ORDEN
            // PODRA TENER BIEN CARGADOS O NO LOS
            // CODIGOS
            // DE OPERACION ECC, ASI EN EL TI NOS PUEDE ESTAR LLEGANDO UN CODIGO
            // ECC QUE NO TENGAMOS EN NINGUNA ORDEN,
            // POR
            // ESO HAY QUE BUSCAR EN MOV_FIDESA Y EN ANOTACIONES
            boolean sinEcc = false;

            try {
              if (altas.iterator().next().getDesgloses().get(0).getNumeroOperacion().startsWith("IBO")) {
                ejecucionesAlo = ejealoBo.findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOSinECCByAlloId(alo
                    .getId());
                sinEcc = true;
              }
              else {
                ejecucionesAlo = ejealoBo.findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOConECCByAlloId(alo
                    .getId());
              }
              if (CollectionUtils.isEmpty(ejecucionesAlo)) {
                ejecucionesAlo = ejealoBo.findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOSinECCByAlloId(alo
                    .getId());
              }

              for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO ealo : ejecucionesAlo) {
                camaraCompensacionHibernate = camaraCompensacionBo.findByCdCodigo(mapCamarasByCdcodigo,
                    ealo.getCdcamara());
                if (camaraCompensacionHibernate != null) {
                  ealo.setIdCamara(camaraCompensacionHibernate.getIdCamaraCompensacion());
                }
              }
            }
            catch (Exception e1) {
              throw new CargaDatosRevisionDesglosesIfException(
                  "CUADRE_ECC-IMPORTAR_DESGLOSES-INCIDENCIA Recuperando los grupos de ejecucion para el alo.id: "
                      + alo.getId(), e1);
            }

            // construimos la lista de ejealos que necesitamos para desglosar
            Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesDatosMercado = getMapEjecucionesMercado(
                alo, ejecucionesAlo, sinEcc);

            // alta por allo
            if (altas.size() + interseccion.size() >= 1000) {
              LOG.debug(
                  "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Las altas se van a crear por ALO.",
                  alo.getId());

              revisarDesglosesSubList.sustituirAloOriginalMultiplesAloNewTransaction(aloData, ejecucionesDatosMercado,
                  altas, listReglasCanones);

            }
            else {
              LOG.debug(
                  "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Las altas se van a crear por ALC.",
                  alo.getId());

              revisarDesglosesPorAlc(alo, interseccion, equivalentes, bajasRealiadas);
              // TODO DESPUES DE DAR LAS BAJAS HAY QUE PONER LOS CALCULOS
              // ECONOMICOS DEL ALO DE LO QUE SE HA DADO DE
              // BAJA
              if (CollectionUtils.isNotEmpty(altas)) {

                revisarDesglosesSubList.crearDesglosesAlc(aloData, altas, ejecucionesDatosMercado, listReglasCanones,
                    bajasRealiadas, altasRealiadas);

                // TODO HACER ALTAS DE ALCS
                LOG.debug(
                    "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Vamos a hacer las altas de los alcs que hemos dado de baja...",
                    alo.getId());
              }
              actualizarDesglosesAloAndDesglosesPti(alo.getId(), desglosesAllo, desglosesEcc, fechaCuadre);
            }// fin else not contains all

          }

        }
        catch (Exception e) {
          throw new SIBBACBusinessException("INCIDENCIA en bean: " + bean, e);
        }
      }// fin for beans
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e);
    }
    finally {
      listReglasCanones.close();
      mapCamarasByCdcodigo.clear();
    }

    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] fin.");

  }

  /**
   * @param cuadreEccSibbac
   * @return
   */
  private CuadreEccReferencia findCuadreEccReferenciaByCuadreEccSibbac(CuadreEccSibbac cuadreEccSibbac) {
    CuadreEccReferencia cuadreEccReferencia = cuadreEccReferenciaBo
        .findByfechaEjeAndNumOpAndSentidoAndRefTitularAndNumValoresImpNominalPendienteDesglose(
            cuadreEccSibbac.getFeoperac(), cuadreEccSibbac.getNumOp(), cuadreEccSibbac.getSentido(),
            cuadreEccSibbac.getRefTitular(), cuadreEccSibbac.getNumTitulosDesSibbac());
    if (cuadreEccReferencia == null) {
      cuadreEccReferencia = cuadreEccReferenciaBo
          .findByfechaEjeAndNumOpAndSentidoAndNumValoresImpNominalPendienteDesglose(cuadreEccSibbac.getFeoperac(),
              cuadreEccSibbac.getNumOp(), cuadreEccSibbac.getSentido(), cuadreEccSibbac.getNumTitulosDesSibbac());
    }

    if (cuadreEccReferencia == null) {
      cuadreEccReferencia = cuadreEccReferenciaBo.findByFechaejecucionAndNumerooperacionAndSentidoAndReferenciatitular(
          cuadreEccSibbac.getFeoperac(), cuadreEccSibbac.getNumOp(), cuadreEccSibbac.getSentido(),
          cuadreEccSibbac.getRefTitular());
    }
    if (cuadreEccReferencia == null) {
      // buscar por infomercado con la reftit y los titulos

    }

    return cuadreEccReferencia;
  }

  /**
   * @param alo
   * @param interseccion
   * @param equivalentes
   * @param bajasRealiadas
   * @throws InactivacionTitularesRevisionTitularesException
   * @throws AltaTitularRevisionTitularesException
   * @throws CambiarReferenciaTitularRevisionTitularesException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void revisarDesglosesPorAlc(Tmct0alo alo, Collection<ReferenciaTitularIFDesglosesDTO> interseccion,
      Map<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO> equivalentes,
      List<DesgloseIFDTO> bajasRealiadas) throws AltaTitularRevisionTitularesException,
      InactivacionTitularesRevisionTitularesException, CambiarReferenciaTitularRevisionTitularesException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesPorAlc] inicio.");
    List<Tmct0alc> alcs = alcBo.findByAlloIdActivos(alo.getId());
    ReferenciaTitularIFDTO referenciaIF;
    ReferenciaTitularIFDesglosesDTO referenciaConDesglosesIF;
    DesgloseIFDTO desgloseIF;

    List<Tmct0desgloseclitit> dcts;

    Tmct0operacioncdecc opEcc;
    Tmct0anotacionecc an;

    String camaraDesglose;

    for (Tmct0alc alc : alcs) {

      Tmct0referenciatitular refTit = alcBo.findTmct0referenciatitular(alc.getId());
      if (refTit != null) {

        referenciaIF = new ReferenciaTitularIFDTO(refTit.getCdreftitularpti(), refTit.getReferenciaAdicional());
      }
      else {
        if (StringUtils.isNotBlank(alc.getCdrefban())) {
          referenciaIF = new ReferenciaTitularIFDTO("", alc.getCdrefban().trim());
        }
        else {
          referenciaIF = new ReferenciaTitularIFDTO("", "");
        }
      }

      dcts = dctBo.findByTmct0alcId(alc.getId());

      referenciaConDesglosesIF = new ReferenciaTitularIFDesglosesDTO(referenciaIF);
      referenciaConDesglosesIF.setNucnfliq(alc.getId().getNucnfliq());
      referenciaConDesglosesIF.setTitulos(alc.getNutitliq());
      String numeroOperacion;
      for (Tmct0desgloseclitit dct : dcts) {
        if (StringUtils.isNotBlank(dct.getCdoperacionecc())) {

          an = anBo.findOperacionTitulosDisponiblesGte(dct.getCdoperacionecc(), dct.getImtitulos());
          if (an == null) {
            numeroOperacion = dct.getCdoperacionecc();
          }
          else {
            numeroOperacion = an.getCdoperacionecc();

          }

        }
        else if (StringUtils.isNotBlank(dct.getNumeroOperacionDCV())) {
          numeroOperacion = dct.getNumeroOperacionDCV();

        }
        else {
          opEcc = opEccBo.findTmct0operacioncdeccByInformacionMercado(dct.getNuordenmkt(), dct.getNurefexemkt(),
              dct.getFeejecuc(), dct.getTpopebol(), dct.getCdisin(), dct.getCdsegmento(), dct.getCdsentido(),
              dct.getCdmiembromkt());
          if (opEcc != null) {
            an = anBo.findOperacionTitulosDisponiblesGte(opEcc.getCdoperacionecc(), dct.getImtitulos());
            if (an != null) {
              dct.setCdoperacionecc(an.getCdoperacionecc());
              dct.setNuoperacionprev(an.getNuoperacionprev());
              dct.setNuoperacioninic(an.getNuoperacioninic());
              dctBo.save(dct);
            }
            else {
              dct.setCdoperacionecc(opEcc.getCdoperacionecc());
              dct.setNuoperacionprev(opEcc.getCdoperacionecc());
              dct.setNuoperacioninic(opEcc.getCdoperacionecc());
              dctBo.save(dct);
            }
          }
          numeroOperacion = dct.getCdoperacionecc();

        }// fin else
        camaraDesglose = dct.getCdcamara();
        desgloseIF = new DesgloseIFDTO(numeroOperacion, referenciaIF, dct.getImtitulos(),
            new IdentificacionOperacionDatosMercadoDTO(dct.getFeejecuc(), dct.getCdisin(), dct.getCdsentido(),
                camaraDesglose, dct.getCdsegmento(), dct.getTpopebol(), dct.getCdmiembromkt(), dct.getNuordenmkt(),
                dct.getNurefexemkt()));
        referenciaConDesglosesIF.getDesgloses().add(desgloseIF);

      }// fin for dcts

      boolean encontradoDesglose = false;
      boolean isDesgloseEquivalente = false;
      ReferenciaTitularIFDesglosesDTO desgloseEquivalente = equivalentes.get(referenciaConDesglosesIF);
      if (desgloseEquivalente == null) {
        for (ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO : interseccion) {
          if (referenciaTitularIFDesglosesDTO.getReferenciaTitular().equals(referenciaIF)
              && alc.getNutitliq().compareTo(referenciaTitularIFDesglosesDTO.getTitulos()) == 0) {
            encontradoDesglose = true;
          }
        }

        for (ReferenciaTitularIFDesglosesDTO referenciaTitularIFDesglosesDTO : equivalentes.keySet()) {
          if (referenciaTitularIFDesglosesDTO.getReferenciaTitular().equals(referenciaIF)
              && alc.getNutitliq().compareTo(referenciaTitularIFDesglosesDTO.getTitulos()) == 0) {
            LOG.debug(
                "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Esta en equivalentes solo debemos cambiar la referencia de titular.",
                alo.getId());
            encontradoDesglose = true;
            isDesgloseEquivalente = true;
            desgloseEquivalente = equivalentes.get(referenciaTitularIFDesglosesDTO);
          }
        }
      }
      else {
        encontradoDesglose = true;
        isDesgloseEquivalente = true;
      }
      if (!encontradoDesglose) {
        revisarDesglosesSubList.bajaAlc(alc, bajasRealiadas);

      }
      else if (isDesgloseEquivalente) {
        // CAMBIAR REFERENCIA DE TITULAR Y REVISAR LOS TITULARES
        revisarDesglosesSubList.cambiarReferenciaTitular(alc, desgloseEquivalente.getReferenciaTitular());
        revisarDesglosesSubList.revisarTitulares(alc, desgloseEquivalente);

      }

    }// for alcs para baja
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesPorAlc] fin.");
  }

  /**
   * @param alo
   * @param ejecucionesAlo
   * @param sinEcc
   * @return
   */
  private Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> getMapEjecucionesMercado(
      Tmct0alo alo, List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesAlo, boolean sinEcc) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: getMapEjecucionesMercado] inicio.");
    Map<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesDatosMercado = new HashMap<IdentificacionOperacionDatosMercadoDTO, EjecucionAlocationProcesarDesglosesOpEspecialesDTO>();
    IdentificacionOperacionDatosMercadoDTO datosMercado;
    String camaraDesglose;
    for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejeAlo : ejecucionesAlo) {
      LOG.trace("[CuadreEccRevisarAndImportarDesglosesBo :: getMapEjecucionesMercado] {}.", ejeAlo);
      camaraDesglose = ejeAlo.getCdcamara().trim();
      // camaraDesglose =
      // conversionesBo.getCamaraCompensacionClearingPlatform(ejeAlo.getCdcamara());
      if (!camaraDesglose.equals(ejeAlo.getCdcamara().trim())) {
        ejeAlo.setCdcamara(camaraDesglose);
        LOG.trace("[CuadreEccRevisarAndImportarDesglosesBo :: getMapEjecucionesMercado] convertimos la camara {}.",
            ejeAlo);
      }

      if (sinEcc) {
        datosMercado = new IdentificacionOperacionDatosMercadoDTO(ejeAlo.getFeejecuc(), alo.getCdisin().trim(),
            alo.getCdtpoper(), ejeAlo.getCdcamara().trim(), ejeAlo.getCdsegmento().trim(), ejeAlo.getTpopebol().trim(),
            ejeAlo.getCdmiembromkt().trim(), ejeAlo.getNuordenmkt().trim(), ejeAlo.getNuexemkt().trim());
      }
      else {
        datosMercado = new IdentificacionOperacionDatosMercadoDTO(ejeAlo.getFeejecuc(), alo.getCdisin().trim(),
            alo.getCdtpoper(), ejeAlo.getCdcamara().trim(), ejeAlo.getCdsegmento().trim(), ejeAlo.getTpopebol().trim(),
            ejeAlo.getCdmiembromkt().trim(), ejeAlo.getNuordenmkt().trim(), ejeAlo.getNuexemkt().trim());
      }

      ejecucionesDatosMercado.put(datosMercado, ejeAlo);

    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: getMapEjecucionesMercado] fin.");
    return ejecucionesDatosMercado;
  }

  /**
   * Realiza la interseccion de los desgloses que tiene el alo y los que nos
   * manda la ecc, estos son los que no se van a tocar porque son identicos
   * 
   * @param nutitAlo
   * @param referenciasDesglosesAloAlta
   * @param referenciasDesglosesEccAlta
   * @return
   */
  private Collection<ReferenciaTitularIFDesglosesDTO> interseccion(BigDecimal nutitAlo,
      Collection<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesAloAlta,
      Collection<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesEccAlta) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: interseccion] inicio.");
    BigDecimal titulosEcc = BigDecimal.ZERO;
    Collection<ReferenciaTitularIFDesglosesDTO> interseccion = CollectionUtils.intersection(
        referenciasDesglosesAloAlta, referenciasDesglosesEccAlta);
    if (CollectionUtils.isEmpty(interseccion)) {
      for (ReferenciaTitularIFDesglosesDTO aloAlta : referenciasDesglosesAloAlta) {
        for (ReferenciaTitularIFDesglosesDTO eccAlta : referenciasDesglosesEccAlta) {
          if (aloAlta.equals(eccAlta)) {
            interseccion.add(aloAlta);
          }
        }
      }
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: interseccion] calculando titulos ecc...");
    for (ReferenciaTitularIFDesglosesDTO eccAlta : referenciasDesglosesEccAlta) {
      titulosEcc = titulosEcc.add(eccAlta.getTitulos());
    }

    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: interseccion] calculando titulos ecc: {} titulos alo: {}.",
        titulosEcc, nutitAlo);
    if (titulosEcc.compareTo(nutitAlo) > 0) {
      LOG.warn(
          "[CuadreEccRevisarAndImportarDesglosesBo :: interseccion] Los titulos Ecc: {} encontrados es mayor a los del allo: {}",
          titulosEcc, nutitAlo);
      for (ReferenciaTitularIFDesglosesDTO aloAlta : referenciasDesglosesAloAlta) {
        for (ReferenciaTitularIFDesglosesDTO eccAlta : referenciasDesglosesEccAlta) {
          if (aloAlta.getReferenciaTitular().equals(eccAlta.getReferenciaTitular())
              && aloAlta.getTitulos().compareTo(eccAlta.getTitulos()) < 0) {
            Collection<DesgloseIFDTO> inserseccionDesgloses = CollectionUtils.intersection(aloAlta.getDesgloses(),
                eccAlta.getDesgloses());

            Collection<DesgloseIFDTO> noInterseccionDesgloses = new HashSet<DesgloseIFDTO>(eccAlta.getDesgloses());
            noInterseccionDesgloses.removeAll(inserseccionDesgloses);
            for (DesgloseIFDTO desgloseNoInterseccionIFDTO : noInterseccionDesgloses) {
              for (DesgloseIFDTO desgloseAloIFDTO : aloAlta.getDesgloses()) {
                if (desgloseNoInterseccionIFDTO.getDatosMercado().equals(desgloseAloIFDTO.getDatosMercado())
                    && desgloseNoInterseccionIFDTO.getNumeroTitulos().compareTo(desgloseAloIFDTO.getNumeroTitulos()) > 0) {
                  inserseccionDesgloses
                      .add(new DesgloseIFDTO(desgloseAloIFDTO.getNumeroOperacion(), desgloseAloIFDTO
                          .getReferenciaTitular(), desgloseAloIFDTO.getNumeroTitulos(), desgloseAloIFDTO
                          .getDatosMercado()));
                }
              }
            }

            if (CollectionUtils.isNotEmpty(inserseccionDesgloses)) {

              interseccion.add(aloAlta);

              ReferenciaTitularIFDesglosesDTO referenciaConDesglosesIF = new ReferenciaTitularIFDesglosesDTO(
                  eccAlta.getReferenciaTitular());
              referenciaConDesglosesIF.getDesgloses().addAll(inserseccionDesgloses);
              for (DesgloseIFDTO desgloseIFDTO : inserseccionDesgloses) {
                referenciaConDesglosesIF.setTitulos(referenciaConDesglosesIF.getTitulos().add(
                    desgloseIFDTO.getNumeroTitulos()));
              }
              if (referenciasDesglosesAloAlta.contains(referenciaConDesglosesIF)) {
                interseccion.add(referenciaConDesglosesIF);
                titulosEcc = titulosEcc.subtract(eccAlta.getTitulos().subtract(referenciaConDesglosesIF.getTitulos()));
              }
            }

          }
        }
      }
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: interseccion] fin.");
    return interseccion;
  }

  /**
   * @param alo
   * @param desglosesIguales
   * @param equivalentes
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void revisarTitularesAlcsFromAlo(Tmct0alo alo, Collection<ReferenciaTitularIFDesglosesDTO> desglosesIguales,
      Map<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO> equivalentes)
      throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarTitularesAlcsFromAlo] inicio.");
    List<Tmct0alc> alcs = alcBo.findByAlloIdActivos(alo.getId());
    List<Tmct0desgloseclitit> dcts;
    Tmct0referenciatitular refTit;
    ReferenciaTitularIFDesglosesDTO desgloseAlc = null;
    ReferenciaTitularIFDesglosesDTO desgloseAlcEquivalente;
    ReferenciaTitularIFDTO referenciaTitularAlc;
    DesgloseIFDTO desgloseOperacionAlc;
    String numeroOperacion;
    Tmct0anotacionecc an;

    if (CollectionUtils.isNotEmpty(alcs)) {
      for (Tmct0alc alc : alcs) {
        LOG.trace(
            "[CuadreEccRevisarAndImportarDesglosesBo :: revisarTitularesAlcsFromAlo] buscando dcts de alcId: {} ...",
            alc.getId());
        try {
          dcts = dctBo.findByTmct0alcId(alc.getId());
          refTit = alcBo.findTmct0referenciatitular(alc.getId());
          if (refTit == null) {
            referenciaTitularAlc = new ReferenciaTitularIFDTO(alc.getRftitaud().trim(), alc.getCdrefban().trim());
          }
          else {
            referenciaTitularAlc = new ReferenciaTitularIFDTO(refTit.getCdreftitularpti(),
                refTit.getReferenciaAdicional());
          }
          LOG.trace(
              "[CuadreEccRevisarAndImportarDesglosesBo :: revisarTitularesAlcsFromAlo] Referencia Titular: {} ...",
              referenciaTitularAlc);
          desgloseAlc = new ReferenciaTitularIFDesglosesDTO(referenciaTitularAlc);
          desgloseAlc.setTitulos(alc.getNutitliq());
          desgloseAlc.setNucnfliq(alc.getId().getNucnfliq());
          for (Tmct0desgloseclitit dct : dcts) {
            // TODO FALTA QUERY PARA OPERACIONES SIN ECC
            String camara = "";
            if (StringUtils.isNotBlank(dct.getCdoperacionecc())) {
              an = anBo.findOperacionTitulosDisponiblesGte(dct.getCdoperacionecc(), dct.getImtitulos());

              if (an != null) {
                numeroOperacion = an.getCdoperacionecc().trim();
                camara = an.getCdcamara();
              }
              else {
                numeroOperacion = dct.getCdoperacionecc().trim();
                camara = dct.getCdcamara();
              }
            }
            else {
              numeroOperacion = dct.getNumeroOperacionDCV().trim();
              camara = "";
            }

            desgloseOperacionAlc = new DesgloseIFDTO(numeroOperacion, referenciaTitularAlc, dct.getImtitulos(),
                new IdentificacionOperacionDatosMercadoDTO(dct.getFeejecuc(), dct.getCdisin().trim(),
                    dct.getCdsentido(), camara.trim(), dct.getCdsegmento().trim(), dct.getTpopebol().trim(), dct
                        .getCdmiembromkt().trim(), dct.getNuordenmkt().trim(), dct.getNurefexemkt().trim()));
            desgloseOperacionAlc.setDesgloseRelacionado(dct);
            desgloseAlc.getDesgloses().add(desgloseOperacionAlc);
          }// fin dct
        }
        catch (Exception e) {
          throw new CargaDatosRevisionDesglosesIfException("INCIDENCIA Cargando datos de desglose para alc.id: "
              + alc.getId(), e);
        }

        desgloseAlc.setDesgloseRelacionado(alc);

        if (desglosesIguales.contains(desgloseAlc)) {
          revisarDesglosesSubList.revisarTitulares(alc, desgloseAlc);
        }
        else {
          desgloseAlcEquivalente = equivalentes.get(desgloseAlc);
          if (desgloseAlcEquivalente != null) {
            logBo.insertarRegistro(alc, new Date(), new Date(), "", alc.getEstadotit(),
                "IF-Cambiando Ref.Tit: '" + alc.getRftitaud() + "' a '"
                    + desgloseAlcEquivalente.getReferenciaTitular().getReferenciaTitular() + "' ");
            revisarDesglosesSubList.cambiarReferenciaTitular(alc, desgloseAlcEquivalente.getReferenciaTitular());
            revisarDesglosesSubList.revisarTitulares(alc, desgloseAlcEquivalente);

          }
        }

      }// fin alc
    }
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: revisarTitularesAlcsFromAlo] fin.");
  }

  /**
   * Recupera todos aquellos desgloses que no tienen la misma referencia de
   * titular pero si contienen los mismos codigos de operacion y mismo numero de
   * titulos
   * 
   * @param alo
   * @param interseccion
   * @param referenciasDesglosesAloAlta
   * @param referenciasDesglosesEccAlta
   * @return
   */
  private Map<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO> recuperarReferenciasEquivalentes(
      Tmct0alo alo, Collection<ReferenciaTitularIFDesglosesDTO> interseccion,
      Collection<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesAloAlta,
      Collection<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesEccAlta) {
    // las referencias son iguales pero nos cambian los desgloses
    Map<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO> equivalentes = new HashMap<ReferenciaTitularIFDesglosesDTO, ReferenciaTitularIFDesglosesDTO>();

    for (ReferenciaTitularIFDesglosesDTO referenciaDesgloseAlo : referenciasDesglosesAloAlta) {
      for (ReferenciaTitularIFDesglosesDTO referenciaDesgloseEcc : referenciasDesglosesEccAlta) {
        if (!interseccion.contains(referenciaDesgloseAlo)
            && !referenciaDesgloseAlo.getReferenciaTitular().equals(referenciaDesgloseEcc.getReferenciaTitular())
            && referenciaDesgloseAlo.getTitulos().compareTo(referenciaDesgloseEcc.getTitulos()) == 0
            && referenciaDesgloseAlo.getDesgloses().equals(referenciaDesgloseEcc.getDesgloses())) {
          LOG.debug(
              "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] Encontrada refrencia distinta old: {} new: {} con mismos desgloses y titulos {}",
              referenciaDesgloseAlo.getReferenciaTitular(), referenciaDesgloseEcc.getReferenciaTitular(), alo.getId());

          equivalentes.put(referenciaDesgloseAlo, referenciaDesgloseEcc);
        }
        else {
          LOG.debug(
              "[CuadreEccRevisarAndImportarDesglosesBo :: revisarDesglosesIf] No es posible encontrar coincidencia {}",
              alo.getId());
        }
      }
    }// fin primer for
    return equivalentes;

  }

  // private void contieneTodasReferenciasTitular(List<ReferenciaTitularIFDTO>
  // referenciasEccAlta,
  // List<ReferenciaTitularIFDTO> referenciasAloAlta) {
  //
  //
  // }

  /**
   * Construye las collectiones de referencias con titulos y desgloses que
   * tienen por cada lado
   * 
   * @param desglosesAllo
   * @param desglosesEcc
   * @param desglosesAloAlta
   * @param desglosesEccAlta
   * @param referenciasAloAlta
   * @param referenciasEccAlta
   * @param referenciasDesglosesAloAlta
   * @param referenciasDesglosesEccAlta
   */
  private void organizarInformacionRevisarDesgloses(List<CuadreEccSibbac> desglosesAllo,
      List<CuadreEccReferencia> desglosesEcc, List<DesgloseIFDTO> desglosesAloAlta,
      List<DesgloseIFDTO> desglosesEccAlta, Collection<ReferenciaTitularIFDTO> referenciasAloAlta,
      Collection<ReferenciaTitularIFDTO> referenciasEccAlta,
      Collection<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesAloAlta,
      Collection<ReferenciaTitularIFDesglosesDTO> referenciasDesglosesEccAlta) {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: organizarInformacionRevisarDesgloses] inicio.");
    ReferenciaTitularIFDTO referenciaIF;
    DesgloseIFDTO desgloseIF;
    ReferenciaTitularIFDesglosesDTO referenciaConDesglosesIF;
    Map<ReferenciaTitularIFDTO, ReferenciaTitularIFDesglosesDTO> referenciasDesglosesAloAltaMap = new HashMap<ReferenciaTitularIFDTO, ReferenciaTitularIFDesglosesDTO>();
    Map<ReferenciaTitularIFDTO, ReferenciaTitularIFDesglosesDTO> referenciasDesglosesEccAltaMap = new HashMap<ReferenciaTitularIFDTO, ReferenciaTitularIFDesglosesDTO>();
    for (CuadreEccSibbac cuadreEccSibbac : desglosesAllo) {
      referenciaIF = new ReferenciaTitularIFDTO(cuadreEccSibbac.getRefTitular(), cuadreEccSibbac.getRefAdicional());
      desgloseIF = new DesgloseIFDTO(cuadreEccSibbac.getNumOp(), referenciaIF,
          cuadreEccSibbac.getNumTitulosDesSibbac(), new IdentificacionOperacionDatosMercadoDTO(
              cuadreEccSibbac.getFeoperac(), cuadreEccSibbac.getCdisin(), cuadreEccSibbac.getSentido(),
              cuadreEccSibbac.getCdcamara(), cuadreEccSibbac.getCdsegmento(), cuadreEccSibbac.getCdoperacionmkt(),
              cuadreEccSibbac.getCdmiembromkt(), cuadreEccSibbac.getNuordenmkt(), cuadreEccSibbac.getNuexemkt()));
      desgloseIF.setCuadreEccSibbac(cuadreEccSibbac);

      desgloseIF.setIddesglosecamara(cuadreEccSibbac.getIddesglosecamara());
      desgloseIF.setNudesglose(cuadreEccSibbac.getNudesglose());
      desgloseIF.setIdejecucionalocation(cuadreEccSibbac.getIdejecucionalocation());
      desgloseIF.setNuejecuc(cuadreEccSibbac.getNuejecuc());
      referenciaConDesglosesIF = referenciasDesglosesAloAltaMap.get(referenciaIF);
      if (referenciaConDesglosesIF == null) {
        referenciaConDesglosesIF = new ReferenciaTitularIFDesglosesDTO(referenciaIF);
        referenciasDesglosesAloAltaMap.put(referenciaIF, referenciaConDesglosesIF);
      }
      desglosesAloAlta.add(desgloseIF);
      referenciasAloAlta.add(referenciaIF);

      referenciaConDesglosesIF.getDesgloses().add(desgloseIF);
      referenciaConDesglosesIF.setTitulos(referenciaConDesglosesIF.getTitulos().add(desgloseIF.getNumeroTitulos()));
      // referenciaConDesglosesIF.setIdEccMessgeInTi(eleIf.getId_ecc_msg_in_ti());

      referenciaConDesglosesIF.setNuorden(cuadreEccSibbac.getNuorden());
      referenciaConDesglosesIF.setNbooking(cuadreEccSibbac.getNbooking());
      referenciaConDesglosesIF.setNucnflt(cuadreEccSibbac.getNucnfclt());
      referenciaConDesglosesIF.setNucnfliq(cuadreEccSibbac.getNucnfliq());

    }// fin for
    referenciasDesglosesAloAlta.addAll(referenciasDesglosesAloAltaMap.values());
    referenciasDesglosesAloAltaMap.clear();

    for (CuadreEccReferencia referenciaEcc : desglosesEcc) {
      if (!"B".equals(referenciaEcc.getIndRectificacion())) {
        referenciaIF = new ReferenciaTitularIFDTO(referenciaEcc.getRefTitular(), referenciaEcc.getRefAdicional());
        desgloseIF = new DesgloseIFDTO(referenciaEcc.getNumOp(), referenciaIF, referenciaEcc.getNumValoresImpNominal(),
            new IdentificacionOperacionDatosMercadoDTO(referenciaEcc.getFechaEje(), referenciaEcc.getCdisin(),
                referenciaEcc.getSentido(), referenciaEcc.getIdEcc(), referenciaEcc.getCdsegmento(),
                referenciaEcc.getCdoperacionmkt(), referenciaEcc.getCdmiembromkt(), referenciaEcc.getNuordenmkt(),
                referenciaEcc.getNuexemkt()));
        desgloseIF.setCuadreEccReferencia(referenciaEcc);

        desglosesEccAlta.add(desgloseIF);
        referenciasEccAlta.add(referenciaIF);

        referenciaConDesglosesIF = referenciasDesglosesEccAltaMap.get(referenciaIF);
        if (referenciaConDesglosesIF == null) {
          referenciaConDesglosesIF = new ReferenciaTitularIFDesglosesDTO(referenciaIF);
          referenciasDesglosesEccAltaMap.put(referenciaIF, referenciaConDesglosesIF);
        }
        // desglosesEccAlta.add(desgloseIF);
        // referenciasEccAlta.add(referenciaIF);
        desgloseIF.setCanonContratacionTeorico(referenciaEcc.getCanonTeorico());
        desgloseIF.setCanonContratacionAplicado(referenciaEcc.getCanonAplicado());
        desgloseIF.setBrutoMercado(referenciaEcc.getImpEfectivo());
        referenciaConDesglosesIF.getDesgloses().add(desgloseIF);
        referenciaConDesglosesIF.setTitulos(referenciaConDesglosesIF.getTitulos().add(desgloseIF.getNumeroTitulos()));
        referenciaConDesglosesIF.setIdEccMessgeInTi(referenciaEcc.getId());
      }
    }

    referenciasDesglosesEccAlta.addAll(referenciasDesglosesEccAltaMap.values());
    referenciasDesglosesEccAltaMap.clear();
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: organizarInformacionRevisarDesgloses] fin.");
  }

  /**
   * @param aloId
   * @param desglosesAllo
   * @param desglosesEcc
   * @param fechaCuadre
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void actualizarDesglosesAloAndDesglosesPti(Tmct0aloId aloId, List<CuadreEccSibbac> desglosesAllo,
      List<CuadreEccReferencia> desglosesEcc, Date fechaCuadre) throws SIBBACBusinessException {
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: actualizarDesglosesAloAndDesglosesPti] inicio.");

    Collection<Integer> estadosTitularidades = new HashSet<Integer>();
    Integer cdestadoTitSettear = null;
    BigDecimal fechaCuadreBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
        .convertDateToString(fechaCuadre));

    BigDecimal fevalorCierre;
    boolean isCierreCuentaTerceros;
    boolean isCierreCuentaNoTerceros;
    for (CuadreEccSibbac cuadreEccSibbac : desglosesAllo) {
      fevalorCierre = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(cuadreEccSibbac
          .getFevalor()));
      isCierreCuentaNoTerceros = fevalorCierre.compareTo(fechaCuadreBd) <= 0;
      // TODO RECUPERAR LOS FESTIVOS Y FINES DE SEMANA Y LOS DIAS NECESARIOS
      // DESDE FTL PARA QUE UNA ITULARIDAD SEA
      // DEFINITIVA
      fevalorCierre = fevalorCierre.add(new BigDecimal(6));

      isCierreCuentaTerceros = fevalorCierre.compareTo(fechaCuadreBd) <= 0;
      cdestadoTitSettear = cuadreEccSibbac.getEstado();

      if (!cuadreEccSibbac.getTipoCuentaLiquidacionIberclear().equals(TipoCuentaIberclear.CUENTA_TERCEROS.name())
          && !cuadreEccSibbac.getTipoCuentaLiquidacionIberclear().equals(
              TipoCuentaIberclear.CUENTA_INDIVIDUAL_IF.name())) {
        if (isCierreCuentaNoTerceros) {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId();
        }
      }
      else if (cuadreEccSibbac.getTipoCuentaLiquidacionIberclear().equals(
          TipoCuentaIberclear.CUENTA_INDIVIDUAL_IF.name())) {
        if (isCierreCuentaTerceros) {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.CIERRE_IF.getId();
        }
        else {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.PROVISIONAL_IF.getId();
        }
      }
      else if (cuadreEccSibbac.getTipoCuentaLiquidacionIberclear().equals(TipoCuentaIberclear.CUENTA_TERCEROS.name())) {
        if (isCierreCuentaTerceros) {
          cdestadoTitSettear = EstadosEnumerados.TITULARIDADES.TITULARIDAD_DEFINITIVA.getId();
        }
      }

      if (cuadreEccSibbac.getEstado() != cdestadoTitSettear) {
        cuadreEccSibbac.setEstado(cdestadoTitSettear);
        cuadreEccSibbac.setAuditDate(new Date());
        cuadreEccSibbac.setAuditUser("IF");
      }
      estadosTitularidades.add(cdestadoTitSettear);
    }

    if (estadosTitularidades.size() == 1) {
      // LOG.warn("[CuadreEccRevisarAndImportarDesglosesBo :: actualizarDesglosesAloAndDesglosesPti] COMENTADO CAMBIO DE ESTADOS DE LOS ALCS");
      LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: actualizarDesglosesAloAndDesglosesPti] cambiando estados de la orden");
      dcBo.updateCdestadotitByTmct0aloId(aloId, cdestadoTitSettear);
      alcBo.updateCdestadotitByTmct0aloId(aloId, cdestadoTitSettear);
      aloBo.updateCdestadotitByTmct0aloId(aloId, cdestadoTitSettear);

    }
    else {
      // TODO INCIDENCIA DISTINTOS TIPOS DE CUENTA
    }

    cuadreEccSibbacBo.save(desglosesAllo);

    for (CuadreEccReferencia ti : desglosesEcc) {
      ti.setFechaCuadre(fechaCuadre);
      ti.setAuditDate(new Date());
      ti.setAuditUser("IF");
    }
    cuadreEccReferenciaBo.save(desglosesEcc);
    LOG.debug("[CuadreEccRevisarAndImportarDesglosesBo :: actualizarDesglosesAloAndDesglosesPti] fin.");
  }

}
