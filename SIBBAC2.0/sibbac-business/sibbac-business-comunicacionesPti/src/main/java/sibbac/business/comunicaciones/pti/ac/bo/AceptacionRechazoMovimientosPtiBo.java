package sibbac.business.comunicaciones.pti.ac.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.NamingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.ac.dao.PtiEnvioAcDao;
import sibbac.business.comunicaciones.pti.ac.dto.AceptacionRechazoMovimientosDTO;
import sibbac.business.comunicaciones.pti.ac.model.PtiEnvioAc;
import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.commons.enums.EstadosEccMovimiento;
import sibbac.business.comunicaciones.pti.commons.enums.TipoMovimiento;
import sibbac.business.comunicaciones.pti.jms.JmsMessagesPtiBo;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.ReferenciasActuacion;
import sibbac.pti.PTIException;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.ac.AC;
import sibbac.pti.messages.ac.R00;

@Service
public class AceptacionRechazoMovimientosPtiBo extends WrapperExecutor {

  protected static final Logger LOG = LoggerFactory.getLogger(AceptacionRechazoMovimientosPtiBo.class);

  private static final Logger LOG_PTI_OUT = LoggerFactory.getLogger("PTI_OUT");

  @Value("${jms.pti.qmanager.name}")
  private String ptiJndiName;

  @Value("${jms.pti.qmanager.host}")
  private String ptiMqHost;

  @Value("${jms.pti.qmanager.port}")
  private int ptiMqPort;

  @Value("${jms.pti.qmanager.channel}")
  private String ptiMqChannel;

  @Value("${jms.pti.qmanager.qmgrname}")
  private String ptiMqManagerName;

  @Value("${jms.pti.queue.online.output.queue}")
  private String onlineOut;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0xasBo conversionesBo;

  
  @Autowired
  @Qualifier(value = "JmsMessagesPtiBo")
  private JmsMessagesPtiBo jmsSendMessage;

  @Autowired
  private PtiEnvioAcDao acDao;

  @Autowired
  private PtiCommomns ptiCommons;

  public AceptacionRechazoMovimientosPtiBo() {
  }

  @Override
  public int getThreadSize() {
    return 25;
  }

  @Override
  public int getTransactionSize() {
    return 100;
  }

  // TODO QUITAR LA POSIBILIDAD DE EJCUTAR CON UNA LISTA, ENVIAR UN MENSAJE AC
  // EN UNA TRANSACCION Y ASI LANZAR EXCEPCION PARA HACER ROLLBACK DE LA
  // TRANSACCION

  /**
   * @param connectionFactory
   * @param actuacion
   * @param ctaDestino
   * @param listCdrefmov lista de cdrefmovimientoecc
   * @param cfgVersion
   * @param auditUser
   * @return
   * @throws JMSException
   * @throws SIBBACBusinessException
   */
  @Transactional
  private List<String> enviarMensajesAC(ConnectionFactory connectionFactory, String actuacion, String ctaDestino,
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, PTIMessageVersion version, String auditUser)
      throws JMSException, SIBBACBusinessException {
    List<String> msgs = new ArrayList<String>();
    List<String> msgsOk = new ArrayList<String>();
    List<PtiEnvioAc> flagsEnvioAc = new ArrayList<PtiEnvioAc>();

    AC ac;
    R00 r00;
    List<Tmct0movimientoeccFidessa> litsMovf;
    Tmct0movimientoeccFidessa movf;
    String sentido;
    BigDecimal titulos;
    PtiEnvioAc envioAc;
    String referenciaAc;
    List<PtiEnvioAc> enviosAnteriores;
    String msgLog;
    Collection<Object> idsLog;
    String msg;
    if (CollectionUtils.isNotEmpty(listCdrefmov)) {
      try {
        if (PTIMessageFactory.getCredentials() == null)
          ptiCommons.initPTICredentials();
      }
      catch (PTIException e) {
        throw new SIBBACBusinessException("No se ha podido configurar la conexion con pti.", e);
      }
      List<String> cdestadosMovsActivos = Arrays.asList(new String[] { EstadosEccMovimiento.ENVIANDOSE_INT.text,
          EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text });
      LOG.trace("[enviarMensajesAC] conectando a gestor MQ usando ConnectionFactory: {}...", connectionFactory);
      try (Connection connection = jmsSendMessage.getNewConnection(connectionFactory);) {
        connection.start();
        LOG.trace("[enviarMensajesAC] creando session con gestor MQ...");
        try (Session session = jmsSendMessage.getNewSession(connection, true)) {
          try {
            List<PTIMessage> ptiMessages = new ArrayList<PTIMessage>();
            LOG.trace("[enviarMensajesAC] eliminando cdrefmovecc duplicados...");
            Collection<AceptacionRechazoMovimientosDTO> listCdrefmovimientoEccSinDuplicados = new HashSet<AceptacionRechazoMovimientosDTO>();
            listCdrefmovimientoEccSinDuplicados.addAll(listCdrefmov);
            idsLog = new HashSet<Object>();
            for (AceptacionRechazoMovimientosDTO cdrefmov : listCdrefmovimientoEccSinDuplicados) {
              LOG.debug("[enviarMensajesAC] procesando cdrefmovecc: {}", cdrefmov);

              LOG.trace("[enviarMensajesAC] buscando registros movimiento fidessa...");
              litsMovf = movfBo.findAllByCdrefmovimientoeccAndFliquidacion(cdrefmov.getCdrefmovimientoecc(),
                  cdrefmov.getFliquidacion());

              if (CollectionUtils.isNotEmpty(litsMovf)) {
                titulos = BigDecimal.ZERO;
                LOG.trace("[enviarMensajesAC] acumulando titulos de movimiento fidessa...");
                for (Tmct0movimientoeccFidessa movfFor : litsMovf) {
                  titulos = titulos.add(movfFor.getImtitulos());
                }
                LOG.trace("[enviarMensajesAC] recuperando cualquiera de los movimientos fidessa para usarlo como marco del mensaje AC...");
                movf = litsMovf.get(0);
                try {
                  LOG.trace("[enviarMensajesAC] revisando validez envio AC para el movimiento...");
                  this.checkActuacionTipoMovimientoEstado(movf, actuacion);

                  LOG.trace("[enviarMensajesAC] buscando envios anteriores de AC para el movimiento...");
                  enviosAnteriores = acDao.findAllByCdrefmovimientoeccAndCdrefnotificacionAndFliquidacion(
                      movf.getCdrefmovimientoecc(), movf.getCdrefnotificacion(), movf.getFliquidacion());
                  boolean enviarAc = true;
                  if (CollectionUtils.isNotEmpty(enviosAnteriores)) {
                    for (PtiEnvioAc ptiEnvioAc : enviosAnteriores) {
                      if (StringUtils.isBlank(ptiEnvioAc.getCodigoError())
                          || ptiEnvioAc.getEstadoProcesado() > ESTADO_PROCESADO_EURO_CCP.REGISTRO_CANCELADO.getId()) {
                        enviarAc = false;
                        break;
                      }
                    }
                  }

                  if (!enviarAc) {
                    msg = String.format("No se puede realizar el envio: '%s' actuacion '%s' ya se hizo.", cdrefmov,
                        actuacion);
                    LOG.warn("[enviarMensajesAC] {}", msg);
                    msgs.add(msg);
                  }
                  else {
                    LOG.trace("[enviarMensajesAC] pidiendo nueva sequencia para AC...");
                    referenciaAc = getNewAcSequence();
                    envioAc = crearPtiEnvioAc(movf, referenciaAc, titulos, actuacion, ctaDestino, auditUser);
                    msgLog = String
                        .format(
                            "Enviado AC ref.ac: '%s' ref.mov: '%s' ref.mov.ecc: '%s' ref.notificacion: '%s' actuacion: '%s' ",
                            envioAc.getCdrefmovimiento(), movf.getCdrefmovimiento(), envioAc.getCdrefmovimientoecc(),
                            envioAc.getCdrefnotificacion(), envioAc.getActuacion());
                    LOG.trace("[enviarMensajesAC] creando mensaje pti AC...");
                    ac = (AC) PTIMessageFactory.getInstance(version, PTIMessageType.AC);
                    LOG.trace("[enviarMensajesAC] creando R00...");
                    r00 = (R00) ac.getRecordInstance(PTIMessageRecordType.R00);
                    r00.setReferenciaDeMovimiento(envioAc.getCdrefmovimiento());
                    r00.setReferenciaDeMovimientoECC(envioAc.getCdrefmovimientoecc());
                    r00.setReferenciaNotificacionPrevia(envioAc.getCdrefnotificacion());
                    r00.setFechaTeoricaLiquidacion(envioAc.getFliquidacion());
                    sentido = conversionesBo.getSentidoSibbacSentidoPti(movf.getSentido() + "");
                    r00.setSentido(sentido);
                    r00.setNumeroDeValoresImporteNominal(titulos);
                    r00.setActuacion(actuacion);

                    if (actuacion.equals(ReferenciasActuacion.ACEPTADA)) {
                      r00.setCuentaCompensacionDestino(ctaDestino);
                      msgLog = String.format("%s cta destino: '%s'", msgLog, ctaDestino);
                    }

                    LOG.trace("[enviarMensajesAC] aniadiendo R00 a AC...");
                    ac.addRecord(PTIMessageRecordType.R00, r00);

                    LOG.trace("[enviarMensajesAC] aniadiendo AC a la lista para el envio...");
                    ptiMessages.add(ac);
                    flagsEnvioAc.add(envioAc);
                    this.escribirLogToMovimientosEcc(cdrefmov, actuacion, cdestadosMovsActivos, idsLog, msgLog,
                        auditUser);
                    msg = String.format("Enviado mensaje para movimiento: '%s'.", cdrefmov.getCdrefmovimientoecc());
                    msgsOk.add(msg);

                  }
                }
                catch (SIBBACBusinessException e) {
                  msg = e.getMessage();
                  LOG.warn("[enviarMensajesAC] {}", msg);
                  LOG.trace(e.getMessage(), e);
                  msgs.add(msg);
                }
              }
              else {
                msg = String.format("Movimiento no encontrado: '%s'", cdrefmov);
                LOG.warn("[enviarMensajesAC] {}", msg);
                msgs.add(msg);
              }
            }
            if (CollectionUtils.isNotEmpty(ptiMessages)) {
              LOG.trace("[enviarMensajesAC] persistiendo AC's...");
              acDao.save(flagsEnvioAc);
              LOG.trace("[enviarMensajesAC] enviando por MQ AC's...");
              acDao.flush();
              jmsSendMessage.sendPtiMessageOnline(session, onlineOut, ptiMessages);
              LOG.trace("[enviarMensajesAC] commit MQ...");
              session.commit();

              LOG.trace("[enviarMensajesAC] escribiendo PTI_OUT...");
              for (PTIMessage ptiMessage : ptiMessages) {
                LOG_PTI_OUT.info(ptiMessage.toPTIFormat());
              }
              msgs.addAll(msgsOk);
              msgsOk.clear();
              for (String msgFor : msgs) {
                LOG.debug("[enviarMensajesAC] {}", msgFor);
              }
            }
          }
          catch (PTIMessageException e) {
            LOG.warn(e.getMessage(), e);
            session.rollback();
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
          catch (JMSException e) {
            LOG.warn(e.getMessage(), e);
            session.rollback();
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
          catch (Exception e) {
            LOG.warn(e.getMessage(), e);
            session.rollback();
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
          finally {

          }
        }
      }
    }
    return msgs;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void escribirLogToMovimientosEcc(AceptacionRechazoMovimientosDTO cdrefmov, String actuacion,
      List<String> cdestadosMovsActivos, Collection<Object> idsLog, String msgLog, String usuarioAuditoria)
      throws SIBBACBusinessException {
    LOG.trace("[escribirLogToMovimientosEcc] inicio");
    LOG.trace("[escribirLogToMovimientosEcc] buscando movimientosecc con refmovecc: {} ...", cdrefmov);
    List<Tmct0movimientoecc> movs = movBo.findAllByCdrefmovimientoeccAndFeliquidacionAndDesgloseActivo(
        cdrefmov.getCdrefmovimientoecc(), cdrefmov.getFliquidacion());
    movs.addAll(movBo.findAllByCdrefmovimientoeccAndFeliquidacionAndAloActivo(cdrefmov.getCdrefmovimientoecc(),
        cdrefmov.getFliquidacion()));
    if (CollectionUtils.isNotEmpty(movs)) {
      for (Tmct0movimientoecc mov : movs) {
        if (mov.getCdestadomov() != null && cdestadosMovsActivos.contains(mov.getCdestadomov().trim())) {
          if (mov.getTmct0alo() != null
              && mov.getTmct0alo().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                  .getId()) {
            if (idsLog.add(mov.getTmct0alo().getId())) {

              LOG.trace("[escribirLogToMovimientosEcc] escribiendo log para id: {}-{}...", mov.getTmct0alo().getId(),
                  msgLog);
              logBo.insertarRegistro(mov.getTmct0alo().getId().getNuorden(), mov.getTmct0alo().getId().getNbooking(),
                  mov.getTmct0alo().getId().getNucnfclt(), msgLog, mov.getTmct0alo().getTmct0estadoByCdestadoasig()
                      .getNombre(), usuarioAuditoria);
              if (actuacion.equals(ReferenciasActuacion.CANCELADO)) {
                aloBo.ponerEstadoAsignacion(mov.getTmct0alo(),
                    EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_ANULACION.getId(), usuarioAuditoria);
              }
            }
          }
          else if (mov.getTmct0desglosecamara() != null
              && mov.getTmct0desglosecamara().getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                  .getId()) {
            if (idsLog.add(mov.getTmct0desglosecamara().getTmct0alc().getId())) {
              LOG.trace("[escribirLogToMovimientosEcc] escribiendo log para id: {}-{}...", mov.getTmct0desglosecamara()
                  .getTmct0alc().getId(), msgLog);
              logBo.insertarRegistro(mov.getTmct0desglosecamara().getTmct0alc().getId().getNuorden(), mov
                  .getTmct0desglosecamara().getTmct0alc().getId().getNbooking(), mov.getTmct0desglosecamara()
                  .getTmct0alc().getId().getNucnfclt(), msgLog, mov.getTmct0desglosecamara()
                  .getTmct0estadoByCdestadoasig().getNombre(), usuarioAuditoria);

              if (actuacion.equals(ReferenciasActuacion.CANCELADO)) {
                dcBo.ponerEstadoAsignacion(mov.getTmct0desglosecamara(),
                    EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_ANULACION.getId(), usuarioAuditoria);
                dcBo.save(mov.getTmct0desglosecamara());
              }
            }
          }
        }
      }
    }
    LOG.trace("[escribirLogToMovimientosEcc] fin");
  }

  @Transactional
  private PtiEnvioAc crearPtiEnvioAc(Tmct0movimientoeccFidessa movf, String referenciaAc, BigDecimal titulos,
      String actuacion, String ctaDestino, String auditUser) {
    LOG.trace("[crearPtiEnvioAc] inicio");
    PtiEnvioAc ac = new PtiEnvioAc();
    ac.setActuacion(actuacion.charAt(0));
    ac.setMovimientoFidessa(movf);
    ac.setCdrefmovimiento(referenciaAc);
    ac.setEstadoProcesado(ESTADO_PROCESADO_EURO_CCP.PROCESADO_ENVIADO.getId());
    ac.setCdrefmovimientoecc(movf.getCdrefmovimientoecc());
    ac.setCdrefnotificacion(movf.getCdrefnotificacion());
    ac.setFliquidacion(movf.getFliquidacion());
    ac.setTitulos(titulos);
    ac.setCuentaCompensacionDestino(ctaDestino);
    ac.setAuditUser(auditUser);

    ac.setAuditDate(new Date());
    LOG.trace("[crearPtiEnvioAc] fin");
    return ac;

  }

  @Transactional
  private String getNewAcSequence() {
    BigDecimal seq = acDao.getNewACPTISequence();
    LOG.trace("[getNewAcSequence] new sequence: {}", seq);
    return String.format("AC%s", Long.toString(seq.longValue(), Character.MAX_RADIX));
  }

  private void checkActuacionTipoMovimientoEstado(Tmct0movimientoeccFidessa movf, String actuacion)
      throws SIBBACBusinessException {
    LOG.trace("[checkActuacionTipoMovimientoEstado] inicio");
    String msg;
    switch (actuacion) {
      case ReferenciasActuacion.CANCELADO:
        if (movf.getCdtipomov().equals(TipoMovimiento.GIVE_UP.text)
            && !movf.getCdestadomov().trim().equals(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text)) {
          msg = String.format("El movimiento '%s' tipo: '%s' no permite la accion: '%s', esta en estado '%s'.",
              movf.getCdrefmovimientoecc(), movf.getCdtipomov(), actuacion, movf.getCdestadomov());
          LOG.warn(msg);
          throw new SIBBACBusinessException(msg);
        }
        break;
      case ReferenciasActuacion.ACEPTADA:
      case ReferenciasActuacion.RECHAZADA:
        if (movf.getCdtipomov().equals(TipoMovimiento.TAKE_UP.text)
            && !movf.getCdestadomov().trim().equals(EstadosEccMovimiento.PDTE_ACEPTAR_COMPENSADOR.text)) {
          msg = String.format("El movimiento '%s' tipo: '%s' no permite la accion: '%s', esta en estado '%s'.",
              movf.getCdrefmovimientoecc(), movf.getCdtipomov(), actuacion, movf.getCdestadomov());
          LOG.warn(msg);
          throw new SIBBACBusinessException(msg);
        }
        break;

      default:
        msg = String.format("El movimiento '%s' tipo: '%s' no permite la accion: '%s', esta en estado '%s'.",
            movf.getCdrefmovimientoecc(), movf.getCdtipomov(), actuacion, movf.getCdestadomov());
        LOG.warn(msg);
        throw new SIBBACBusinessException(msg);
    }
    LOG.trace("[checkActuacionTipoMovimientoEstado] fin");
  }

  /**
   * Envia el rechazo de los movimientos que estan listos para entrar
   * @param listCdrefmov
   * @param version
   * @param auditUser este dato sale de {@link Tmct0cfgBo.getPtiMessageVersion}
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public List<String> enviarRechazoMovimientosEntradaByCdrefmovimientoecc(
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, String auditUser) throws SIBBACBusinessException {
    ConnectionFactory cf = null;
    PTIMessageVersion version = cfgBo.getPtiMessageVersionFromTmct0cfg(new Date());
    try {
      try {
        cf = jmsSendMessage.getConnectionFactoryFromContext(ptiJndiName);
      }
      catch (NamingException e) {
        LOG.trace(e.getMessage(), e);
        cf = jmsSendMessage.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
      }
      return enviarMensajesAC(cf, ReferenciasActuacion.RECHAZADA, null, listCdrefmov, version, auditUser);
    }
    catch (JMSException e) {
      throw new SIBBACBusinessException("INCIDENCIA-Conectando con el sistema remoto.", e);
    }

  }

  /**
   * Envia la aceptacion del movimiento que esta pendiente de entrada, a la
   * cuenta de compensacion que se pasa por parametro
   * 
   * @param listCdrefmov lista de cdrefmovimientoecc
   * @param ctaDestino cuenta de compensacion destino del movimiento
   * @param version este dato sale de {@link Tmct0cfgBo.getPtiMessageVersion}
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public List<String> enviarAceptacionMovimientosEntradaByCdrefmovimientoecc(
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, String ctaDestino, String auditUser)
      throws SIBBACBusinessException {
    ConnectionFactory cf = null;
    PTIMessageVersion version = cfgBo.getPtiMessageVersionFromTmct0cfg(new Date());
    try {

      try {
        cf = jmsSendMessage.getConnectionFactoryFromContext(ptiJndiName);
      }
      catch (NamingException e) {
        LOG.trace(e.getMessage(), e);
        cf = jmsSendMessage.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
      }
      return enviarMensajesAC(cf, ReferenciasActuacion.ACEPTADA, ctaDestino, listCdrefmov, version, auditUser);
    }
    catch (JMSException e) {
      throw new SIBBACBusinessException("INCIDENCIA-Conectando con el sistema remoto.", e);
    }
  }

  /**
   * @param listCdrefmov lista de cdrefmovimientoecc para los que se van a
   * enviar las peticiones de cancelacion
   * @param cfgVersion este dato sale de {@link Tmct0cfgBo.getPtiMessageVersion}
   * @param auditUser
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public List<String> enviarCancelacionMovimientosSalidaByCdrefmovimientoecc(
      List<AceptacionRechazoMovimientosDTO> listCdrefmov, String auditUser) throws SIBBACBusinessException {
    ConnectionFactory cf = null;
    PTIMessageVersion version = cfgBo.getPtiMessageVersionFromTmct0cfg(new Date());
    try {
      try {
        cf = jmsSendMessage.getConnectionFactoryFromContext(ptiJndiName);
      }
      catch (NamingException e) {
        LOG.trace(e.getMessage(), e);
        cf = jmsSendMessage.getNewJmsConnectionFactory(ptiMqHost, ptiMqPort, ptiMqChannel, ptiMqManagerName, "", "");
      }
      return enviarMensajesAC(cf, ReferenciasActuacion.CANCELADO, null, listCdrefmov, version, auditUser);
    }
    catch (JMSException e) {
      throw new SIBBACBusinessException("INCIDENCIA-Conectando con el sistema remoto.", e);
    }

  }

}
