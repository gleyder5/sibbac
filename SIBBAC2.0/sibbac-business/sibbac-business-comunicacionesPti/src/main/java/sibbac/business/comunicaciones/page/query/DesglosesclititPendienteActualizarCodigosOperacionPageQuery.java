package sibbac.business.comunicaciones.page.query;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.DynamicColumn;

@Component("DesglosesclititPendienteActualizarCodigosOperacionPageQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DesglosesclititPendienteActualizarCodigosOperacionPageQuery extends DesglosesclititTraspasosPageQuery {

  private static final Logger LOG = LoggerFactory
      .getLogger(DesglosesclititPendienteActualizarCodigosOperacionPageQuery.class);

  @Value("${hora.inicio.traspaso.fuera.hora:171500}")
  private String horaInicioTraspasoFueraHora = null;

  private static final DynamicColumn[] COLUMNS = { new DynamicColumn("N. Orden", "NUORDEN"),
      new DynamicColumn("Número booking", "NBOOKING"), new DynamicColumn("Referencia cliente", "REF_CLIENTE"),
      new DynamicColumn("Alias", "ALIAS"), new DynamicColumn("Desc. Alias", "DESC_ALIAS"),
      new DynamicColumn("Isin", "ISIN"), new DynamicColumn("Desc. Isin", "DESC_ISIN"),
      new DynamicColumn("F. Contratación", "FETRADE", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("F. Liquidación", "FEVALOR", DynamicColumn.ColumnType.DATE),
      new DynamicColumn("Sentido", "SENTIDO"), new DynamicColumn("Títulos", "TITULOS", DynamicColumn.ColumnType.INT),
      new DynamicColumn("Efectivo", "EFECTIVO", DynamicColumn.ColumnType.N4),
      new DynamicColumn("Cuenta", "CUENTA_COMPENSACION_DESTINO"),
      new DynamicColumn("Compensador", "MIEMBRO_COMPENSADOR_DESTINO"), new DynamicColumn("Desc. Compensador", "DESC_MIEMBRO_COMPENSADOR_DESTINO"),
      new DynamicColumn("Ref. Give-Up", "CD_REF_ASIGNACION"),
      new DynamicColumn("Op. ECC Desglose", "CDOPERACIONECC_DESGLOSE"),
      new DynamicColumn("Op. ECC Anotacion", "CDOPERACIONECC_ANOTACION"),
      new DynamicColumn("Op. ECC. An Prev.", "NUOPERACIONPREV_ANOTACION"),
      new DynamicColumn("Op. ECC. Inicial", "NUOPERACIONINIC") };

  public DesglosesclititPendienteActualizarCodigosOperacionPageQuery() {
    super(Arrays.asList(COLUMNS));

  }

  @Override
  public String getSelect() {

    StringBuilder sb = new StringBuilder(
        "SELECT DISTINCT B.NUORDEN, B.NBOOKING, COALESCE(A.REF_CLIENTE, '') REF_CLIENTE, B.CDALIAS ALIAS, B.DESCRALI DESC_ALIAS, ");
    sb.append(
        "B.CDISIN ISIN, B.NBVALORS DESC_ISIN, B.FETRADE, B.FEVALOR, B.CDTPOPER SENTIDO, AN.IMTITULOSDISPONIBLES TITULOS, AN.IMEFECTIVODISPONIBLE EFECTIVO,")
        .append(" CC.CD_CODIGO CUENTA_COMPENSACION_DESTINO, MC.NB_NOMBRE MIEMBRO_COMPENSADOR_DESTINO, MC.NB_DESCRIPCION DESC_MIEMBRO_COMPENSADOR_DESTINO, AN.CDREFASIGEXT CD_REF_ASIGNACION, ")
        .append(
            " DCT.CDOPERACIONECC CDOPERACIONECC_DESGLOSE, AN.CDOPERACIONECC CDOPERACIONECC_ANOTACION, AN.NUOPERACIONPREV NUOPERACIONPREV_ANOTACION, AN.NUOPERACIONINIC ");
    return sb.toString();
  }

  @Override
  public String getFrom() {
    StringBuilder sb = new StringBuilder(" FROM TMCT0ANOTACIONECC AN ");
    sb.append(" INNER JOIN TMCT0_CUENTAS_DE_COMPENSACION CC ON AN.IDCUENTACOMP=CC.ID_CUENTA_COMPENSACION ")
        .append(" INNER JOIN TMCT0_COMPENSADOR MC ON AN.ID_COMPENSADOR=MC.ID_COMPENSADOR ")
        .append(
            " INNER JOIN TMCT0DESGLOSECLITIT DCT ON AN.NUOPERACIONINIC=DCT.NUOPERACIONINIC AND AN.CDSENTIDO=DCT.CDSENTIDO AND DCT.CDOPERACIONECC <> AN.CDOPERACIONECC ")
        .append(" INNER JOIN TMCT0DESGLOSECAMARA DC ON DCT.IDDESGLOSECAMARA=DC.IDDESGLOSECAMARA ")
        .append(
            " INNER JOIN TMCT0ALO A ON DC.NUORDEN=A.NUORDEN AND DC.NBOOKING=A.NBOOKING AND DC.NUCNFCLT = A.NUCNFCLT ")
        .append(" INNER JOIN TMCT0BOK B ON A.NUORDEN=B.NUORDEN AND A.NBOOKING=B.NBOOKING ");
    return sb.toString();
  }

  @Override
  public String getWhere() {
    return "";
  }

  @Override
  public String getPostWhereConditions() {
    String casePti = "'S'";
    String cdindposicion = "'C'"; // C-CIERRE-O-OPEN
    int hora = 0;
    int minuto = 0;

    if (horaInicioTraspasoFueraHora != null) {
      Calendar cal = Calendar.getInstance();
      try {
        cal.setTime(FormatDataUtils.convertStringToTime(horaInicioTraspasoFueraHora));
        hora = cal.get(Calendar.HOUR_OF_DAY);
        minuto = cal.get(Calendar.MINUTE);
      }
      catch (ParseException e) {
        LOG.warn(e.getMessage(), e);
      }

    }

    StringBuilder sb = new StringBuilder(" B.CDESTADOASIG > {0} AND B.CASEPTI = {1}  ");
    sb.append(" AND DC.CDESTADOASIG > {0} AND DC.CDESTADOASIG <> {2} AND A.CDESTADOASIG > {0} ")
        .append(
            " AND AN.CDINDPOSICION <> {3} AND HOUR(AN.AUDIT_FECHA_CAMBIO) >= {4} AND MINUTE(AN.AUDIT_FECHA_CAMBIO) > {5} ")
        .append(" AND DATE(AN.AUDIT_FECHA_CAMBIO) > AN.FCONTRATACION ");

    String postWhere = MessageFormat.format(sb.toString(), EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(), casePti,
        EstadosEnumerados.ASIGNACIONES.PDTE_ACTUALIZAR_CODIGOS_OPERACION.getId(), cdindposicion, hora, minuto);
    return postWhere;
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {
    super.checkLocalMandatoryFiltersSet();
    if (horaInicioTraspasoFueraHora == null || horaInicioTraspasoFueraHora.trim().isEmpty()) {
      throw new SIBBACBusinessException("La hora inicial de revision de anotaciones en cuenta no esta configurada.");
    }

  }

}
