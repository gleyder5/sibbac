package sibbac.business.comunicaciones.euroccp.titulares.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.database.DBConstants.EURO_CCP;
import sibbac.database.model.ATable;

@Table(name = EURO_CCP.FILE_SENT)
@Entity
public class EuroCcpFileSent extends ATable<EuroCcpFileSent> {

  /**
   * 
   */
  private static final long serialVersionUID = -5897975641237870508L;

  @Column(name = "ESTADO_PROCESADO", nullable = false)
  private int estadoProcesado;
  
  @Column(name = "CLIENT_NUMBER", length = 10, nullable = false)
  private String clientNumber;

  @Temporal(TemporalType.DATE)
  @Column(name = "SENT_DATE")
  private Date sentDate;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_FILE_TYPE", nullable = false)
  private EuroCcpFileType fileType;

  @Column(name = "SEQUENCE_NUMBER")
  private Integer sequenceNumber;

  @Column(name = "FILE_NAME", length = 255, nullable = true)
  private String fileName;
  
  @Column(name = "FILE_BASE_NAME", length = 255, nullable = true)
  private String fileBaseName;

  @Column(name = "FILE_PATH", length = 2047, nullable = true)
  private String filePath;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_PARENT_FILE", nullable = true)
  private EuroCcpFileSent parentFile;

  public EuroCcpFileSent() {
  }

  public Date getSentDate() {
    return sentDate;
  }

  public EuroCcpFileType getFileType() {
    return fileType;
  }

  public Integer getSequenceNumber() {
    return sequenceNumber;
  }

  public String getFileName() {
    return fileName;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setSentDate(Date sentDate) {
    this.sentDate = sentDate;
  }

  public void setFileType(EuroCcpFileType fileType) {
    this.fileType = fileType;
  }

  public void setSequenceNumber(Integer sequenceNumber) {
    this.sequenceNumber = sequenceNumber;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public EuroCcpFileSent getParentFile() {
    return parentFile;
  }

  public void setParentFile(EuroCcpFileSent parentFile) {
    this.parentFile = parentFile;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public String getFileBaseName() {
    return fileBaseName;
  }

  public void setFileBaseName(String fileBaseName) {
    this.fileBaseName = fileBaseName;
  }

}
