package sibbac.business.desglose.bo;

import java.util.List;
import java.util.concurrent.Callable;

import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

class DesgloseManualTmct0bokCallable implements Callable<CallableResultDTO> {

  private DesgloseWithUserLock desgloseAutomatico;
  private DesgloseConfigDTO config;
  private Tmct0bokId bookId;
  private List<SolicitudDesgloseManual> listSolicitudDesgloseManual;

  DesgloseManualTmct0bokCallable(DesgloseWithUserLock desgloseAutomatico, DesgloseConfigDTO config, Tmct0bokId bookId,
      List<SolicitudDesgloseManual> listSolicitudDesgloseManual) {
    this.config = config;
    this.desgloseAutomatico = desgloseAutomatico;
    this.bookId = bookId;
    this.listSolicitudDesgloseManual = listSolicitudDesgloseManual;
  }

  @Override
  public CallableResultDTO call() throws SIBBACBusinessException {
    return desgloseAutomatico.desgloseManual(bookId, listSolicitudDesgloseManual, config);
  }

  Tmct0bokId getBookId() {
    return bookId;
  }

  void setBookId(Tmct0bokId bookId) {
    this.bookId = bookId;
  }

  List<SolicitudDesgloseManual> getListSolicitudDesgloseManual() {
    return listSolicitudDesgloseManual;
  }

  void setListSolicitudDesgloseManual(List<SolicitudDesgloseManual> listSolicitudDesgloseManual) {
    this.listSolicitudDesgloseManual = listSolicitudDesgloseManual;
  }

}
