package sibbac.business.comunicaciones.euroccp.ciffile.runnable;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpAggregatesNetSettlementsDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpCashPositionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpProcessedMoneyMovementDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpProcessedSettledMovementDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpProcessedUnsettledMovementDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpSettledPositionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpSettlementInstructionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpTrailerDao;
import sibbac.business.comunicaciones.euroccp.ciffile.dao.EuroCcpUnsettledPositionDao;
import sibbac.business.comunicaciones.euroccp.ciffile.enums.EuroCcpMessageType;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpAggregatesNetSettlements;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpCashPosition;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpLineaFicheroRecordBean;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedMoneyMovement;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedSettledMovement;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettledPosition;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettlementInstuction;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpTrailer;
import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpUnsettledPosition;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "euroCcpCifFileReaderRunnable")
public class EuroCcpCifFileReaderRunnable extends IRunnableBean<EuroCcpLineaFicheroRecordBean> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpCifFileReaderRunnable.class);

  @Autowired
  private EuroCcpProcessedUnsettledMovementDao euroCcpProcessedUntsettledMovementDao;

  @Autowired
  private EuroCcpProcessedSettledMovementDao euroCcpProcessedSettledMovementDao;

  @Autowired
  private EuroCcpAggregatesNetSettlementsDao euroCcpAggregatesNetSettlementsDao;
  
  @Autowired
  private EuroCcpUnsettledPositionDao euroCcpUnsettledPositionDao;

  @Autowired
  private EuroCcpSettlementInstructionDao euroCcpSettlementInstructionDao;

  @Autowired
  private EuroCcpSettledPositionDao euroCcpSettledPositionDao;

  @Autowired
  private EuroCcpProcessedMoneyMovementDao euroCcpProcessedMoneyMovementDao;

  @Autowired
  private EuroCcpCashPositionDao euroCcpCashPositionDao;

  @Autowired
  private EuroCcpTrailerDao euroCcpTrailerDao;

  public EuroCcpCifFileReaderRunnable() {
    super();
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<EuroCcpLineaFicheroRecordBean> beans, int order) throws SIBBACBusinessException {
    try {
      for (EuroCcpLineaFicheroRecordBean linea : beans) {
        LOG.debug("[EuroCcpCifFileReaderRunnable :: run] bean.tipoRegistro == {}", linea.getTipoRegistro());
        LOG.debug("[EuroCcpCifFileReaderRunnable :: run] bean.releaseCode == {}", linea.getReleaseCode());
        LOG.debug("[EuroCcpCifFileReaderRunnable :: run] bean.processingDate == {}", linea.getProcessingDate());
        LOG.debug("[EuroCcpCifFileReaderRunnable :: run] bean.claringSiteCode == {}", linea.getClearingSiteCode());
        linea.setAuditDate(new Date());
        linea.setAuditUser("MIDDLE");
        linea.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.PDTE_PROCESAR.getId());
        if (EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_409.text.equals(linea.getTipoRegistro())
            || EuroCcpMessageType.TYPE_PROCESSED_UNSETTLED_MOVEMENT_410.text.equals(linea.getTipoRegistro())) {

          euroCcpProcessedUntsettledMovementDao.save((EuroCcpProcessedUnsettledMovement) linea);
        } else if (EuroCcpMessageType.TYPE_PROCESSED_SETTLED_MOVEMENT_411.text.equals(linea.getTipoRegistro())) {
          euroCcpProcessedSettledMovementDao.save((EuroCcpProcessedSettledMovement) linea);
        } else if (EuroCcpMessageType.TYPE_AGGREGATES_NET_SETTLEMENTS_415.text.equals(linea.getTipoRegistro())) {
          euroCcpAggregatesNetSettlementsDao.save((EuroCcpAggregatesNetSettlements) linea);
        } else if (EuroCcpMessageType.TYPE_UNSETTLED_POSITION_420.text.equals(linea.getTipoRegistro())) {
          euroCcpUnsettledPositionDao.save((EuroCcpUnsettledPosition) linea);
        } else if (EuroCcpMessageType.TYPE_SETTLED_POSITION_421.text.equals(linea.getTipoRegistro())) {
          euroCcpSettledPositionDao.save((EuroCcpSettledPosition) linea);
        } else if (EuroCcpMessageType.TYPE_SETTLEMENT_INSTRUCTION_450.text.equals(linea.getTipoRegistro())) {
          euroCcpSettlementInstructionDao.save((EuroCcpSettlementInstuction) linea);
        } else if (EuroCcpMessageType.TYPE_PROCESSED_MONEY_MOVEMENT_600.text.equals(linea.getTipoRegistro())) {
          euroCcpProcessedMoneyMovementDao.save((EuroCcpProcessedMoneyMovement) linea);
        } else if (EuroCcpMessageType.TYPE_CASH_POSITION_610.text.equals(linea.getTipoRegistro())) {
          euroCcpCashPositionDao.save((EuroCcpCashPosition) linea);
        } else if (EuroCcpMessageType.TYPE_TRAILER_910.text.equals(linea.getTipoRegistro())) {
          euroCcpTrailerDao.save((EuroCcpTrailer) linea);
        } else {
          LOG.warn("[EuroCcpCifFileReaderRunnable :: run] bean.tipoRegistro == {} no contemplado", linea.getTipoRegistro());
        }

      }

    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException(e);
    } finally {
      getObserver().sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<EuroCcpLineaFicheroRecordBean> clone() {
    return this;
  }

}
