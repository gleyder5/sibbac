package sibbac.business.comunicaciones.euroccp.ciffile.enums;

public enum EuroCcpDebitCreditIndicator {

  DEBIT("D"),
  CREDIT("C");
  public String text;

  private EuroCcpDebitCreditIndicator(String text) {
    this.text = text;
  }
}
