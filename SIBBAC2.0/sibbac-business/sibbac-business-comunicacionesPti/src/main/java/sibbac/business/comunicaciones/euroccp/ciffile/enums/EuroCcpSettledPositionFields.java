package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpSettledPositionFields implements WrapperFileReaderFieldEnumInterface {

  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  SUBACCOUNT_NUMBER("subaccountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  SYMBOL("symbol", 6, 0, WrapperFileReaderFieldType.STRING),
  OPTION_TYPE("optionType", 1, 0, WrapperFileReaderFieldType.CHAR),
  EXPIRATION_DATE("expirationDate", 8, 0, WrapperFileReaderFieldType.DATE),
  EXERCISE_PRICE("exercisePrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  DEPOT_ID("depotId", 6, 0, WrapperFileReaderFieldType.STRING),
  SAFE_KEEPING_ID("safeKeepingId", 2, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  CUPON_INTEREST("cuponInterest", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  CUPON_INTEREST_DC("cuponInterestDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  PROCESSED_QUANTITY_LONG("processedQuantityLong", 13, 2, WrapperFileReaderFieldType.NUMERIC),
  PROCESSED_QUANTITY_SHORT("processedQuantityShort", 13, 2, WrapperFileReaderFieldType.NUMERIC),
  MARK_TO_MARKET_VALUE("markMarketValue", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  MARK_TO_MARKET_VALUE_DC("markMarketValueDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  VALUATION_PRICE("valuationPrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  ISIN_CODE("isinCode", 12, 0, WrapperFileReaderFieldType.STRING),
  FILLER("filler", 325, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpSettledPositionFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
