package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.wrappers.database.model.Impuesto;
import sibbac.business.wrappers.database.model.Tmct0gae;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0galId;
import sibbac.business.wrappers.database.model.Tmct0gre;
import sibbac.common.utils.ImpuestoEnum;
import sibbac.common.utils.SibbacEnums;

import com.sv.sibbac.multicenter.AllocationID;
import com.sv.sibbac.multicenter.ExecutionGroupType;
import com.sv.sibbac.multicenter.ExecutionID;

public class XmlTmct0gal {

  private Tmct0gal tmct0gal;

  public XmlTmct0gal(Tmct0gre tmct0gre) {
    super();
    tmct0gal = new Tmct0gal();
    tmct0gal.setTmct0gre(tmct0gre);
    tmct0gal.setId(new Tmct0galId(tmct0gre.getId().getNuorden(), tmct0gre.getId().getNbooking(), tmct0gre.getId()
        .getNuagreje(), null));
  }

  /**
   * @param executionsGroup
   * @param allocationID
   * @param mAllocation
   * @throws XmlOrdenExceptionReception
   */
  @SuppressWarnings("unchecked")
  public void xml_mapping(ExecutionGroupType executionsGroup, AllocationID allocationID, Map<String, Object> mAllocation)
      throws XmlOrdenExceptionReception {
    tmct0gal.getId().setNucnfclt(allocationID.getNucnfclt());
    BigDecimal impuestoEur;
    BigDecimal impuestoDivisa;
    Impuesto impuesto = null;
    /*
     * EconomicDataType
     */
    XmlTmct0gae xmlTmct0gae;
    XmlImpuesto xmlImpuesto;
    List<Impuesto> impuestos = new ArrayList<Impuesto>();
    int nutitagr = 0;
    List<Tmct0gae> tmct0gaes = new ArrayList<Tmct0gae>();

    Map<String, Tmct0gae> mGae = new HashMap<String, Tmct0gae>();

    for (ExecutionID executionId : allocationID.getExecutions()) {
      nutitagr += executionId.getNumtitasig();
      if (mGae.get(executionId.getNuejecuc()) != null) {
        xmlTmct0gae = new XmlTmct0gae(tmct0gal, mGae.get(executionId.getNuejecuc()));
      }
      else {
        xmlTmct0gae = new XmlTmct0gae(tmct0gal);
      }

      xmlTmct0gae.xml_mapping(executionId);
      mGae.put(executionId.getNuejecuc(), xmlTmct0gae.getJDOObject());
      tmct0gaes.add(xmlTmct0gae.getJDOObject());
    }
    mGae.clear();
    tmct0gal.getTmct0gaes().addAll(tmct0gaes);

    // Suma de los títulos
    tmct0gal.setNutitagr(new BigDecimal(nutitagr).setScale(5, RoundingMode.HALF_UP));
    tmct0gal.setImtpcamb((BigDecimal) mAllocation.get("imtpcamb"));
    tmct0gal.setImcambio(tmct0gal.getTmct0gre().getPrecioBrutoMkt());
    if (executionsGroup.getFhejecuc() != null) {
      tmct0gal.setFeejecuc(new Date(executionsGroup.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    }

    tmct0gal.setImcliqeu(new BigDecimal(0));
    tmct0gal.setImcconeu(new BigDecimal(0));
    tmct0gal.setImdereeu(new BigDecimal(0));

    // nuevos
    tmct0gal.setImgastos(BigDecimal.ZERO);
    tmct0gal.setImgatdvs(BigDecimal.ZERO);
    tmct0gal.setImgastosb(BigDecimal.ZERO);
    tmct0gal.setImgatdvsb(BigDecimal.ZERO);

    tmct0gal.setImimpdvs(BigDecimal.ZERO);
    tmct0gal.setImimpeur(BigDecimal.ZERO);
    tmct0gal.setImimpdvsb(BigDecimal.ZERO);
    tmct0gal.setImimpeurb(BigDecimal.ZERO);

    tmct0gal.setImgastotdv(BigDecimal.ZERO);
    tmct0gal.setImgastoteu(BigDecimal.ZERO);
    tmct0gal.setImgastotdvb(BigDecimal.ZERO);
    tmct0gal.setImgastoteub(BigDecimal.ZERO);

    tmct0gal.setCdreside(' ');
    tmct0gal.setCdindgas(' ');
    tmct0gal.setCdindimp(' ');
    tmct0gal.setCdresideb(' ');
    tmct0gal.setCdindgasb(' ');
    tmct0gal.setCdindimpb(' ');

    tmct0gal.setPccombrk(BigDecimal.ZERO);

    tmct0gal.setCdmoniso((String) mAllocation.get("cdmoniso"));
    tmct0gal.setCanonContrPagoClte((Integer) mAllocation.get("canonContrPagoClte") == 1 ? true : false);
    tmct0gal.setCanonContrPagoMkt((Integer) mAllocation.get("canonContrPagoMkt") == 1 ? true : false);
    tmct0gal.setCanonCompPagoClte((Integer) mAllocation.get("canonCompPagoClte") == 1 ? true : false);
    tmct0gal.setCanonCompPagoMkt((Integer) mAllocation.get("canonCompPagoMkt") == 1 ? true : false);
    tmct0gal.setCanonLiqPagoClte((Integer) mAllocation.get("canonLiqPagoClte") == 1 ? true : false);
    tmct0gal.setCanonLiqPagoMkt((Integer) mAllocation.get("canonLiqPagoMkt") == 1 ? true : false);
    tmct0gal.setXtdcm(' ');
    tmct0gal.setXtderech(' ');
    tmct0gal.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0gal.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);

    substratValue(mAllocation, "nutitcli_sin_repartir", tmct0gal.getNutitagr());
    BigDecimal nutitAgrRest = getValue(mAllocation, "nutitcli_sin_repartir");
    if (nutitAgrRest.compareTo(BigDecimal.ZERO) == 0) {
      tmct0gal.setImsvb(getRestValue(mAllocation, "imsvb"));
      tmct0gal.setImsvbeu(getRestValue(mAllocation, "imsvbeu"));
      tmct0gal.setImfibrdv(getRestValue(mAllocation, "imfibrdv"));
      tmct0gal.setImfibreu(getRestValue(mAllocation, "imfibreu"));
      tmct0gal.setImtotnet(getRestValue(mAllocation, "imtotnet"));
      tmct0gal.setEutotnet(getRestValue(mAllocation, "eutotnet"));
      tmct0gal.setImcomdev(getRestValue(mAllocation, "imcomdev"));
      tmct0gal.setEucomdev(getRestValue(mAllocation, "eucomdev"));
      tmct0gal.setImcomisn(getRestValue(mAllocation, "imcomisn"));
      tmct0gal.setImcomieu(getRestValue(mAllocation, "imcomieu"));

      // mapeo
      tmct0gal.setImbrmerc(getRestValue(mAllocation, "imtotbru"));
      tmct0gal.setImbrmreu(getRestValue(mAllocation, "eutotbru"));
      tmct0gal.setImbrmercb(getRestValue(mAllocation, "imbrmerc"));
      tmct0gal.setImbrmreub(getRestValue(mAllocation, "imbrmreu"));

      tmct0gal.setImgasgeseu(getRestValue(mAllocation, "imgasgeseu"));
      tmct0gal.setImgasgesdv(getRestValue(mAllocation, "imgasgesdv"));
      tmct0gal.setImgasgeseub(getRestValue(mAllocation, "imgasgeseu"));
      tmct0gal.setImgasgesdvb(getRestValue(mAllocation, "imgasgesdv"));

      tmct0gal.setImgasliqeu(getRestValue(mAllocation, "imgasliqeu"));
      tmct0gal.setImgasliqdv(getRestValue(mAllocation, "imgasliqdv"));
      tmct0gal.setImgasliqeub(getRestValue(mAllocation, "imgasliqeu"));
      tmct0gal.setImgasliqdvb(getRestValue(mAllocation, "imgasliqdv"));

      // broker

      // fin mapeo campos nuevos

      tmct0gal.setImCanonCompDv(getRestValue(mAllocation, "imCanonCompDv"));
      tmct0gal.setImCanonCompEu(getRestValue(mAllocation, "imCanonCompEu"));
      
   

      for (Entry<String, Object> entry : ((Map<String, Object>) mAllocation.get("impuesto")).entrySet()) {

        if ('E' == tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdclsmdo())

        {
          // impuestoEur = getRestValue((Map<String, Object>) entry.getValue(),
          // "importeEu");
          // impuestoDivisa = getRestValue((Map<String, Object>)
          // entry.getValue(), "importeDv");

          
          // for (Impuesto impuestoFor : tmct0gal.getImpuestos()) {
          // if (entry.getKey().equals(impuesto.getId().getNombre())) {
          // impuesto = impuestoFor;
          // break;
          //
          // }
          // }
          // if (impuesto != null) {
          // impuesto.setImpDv(impuesto.getImpDv().add(impuestoDivisa));
          // impuesto.setImpEu(impuesto.getImpEu().add(impuestoEur));
          // }
          // else {
          // xmlImpuesto = new XmlImpuesto(tmct0gal);
          // xmlImpuesto.xml_mapping(entry);
          // xmlImpuesto.getJDOObject().setPagoClte(
          // (boolean) ((Map<String, Object>) entry.getValue()).get("pagoClte")
          // ?
          // 1 : 0);
          // xmlImpuesto.getJDOObject().setPagoMkt(
          // (boolean) ((Map<String, Object>) entry.getValue()).get("pagoMkt") ?
          // 1
          // : 0);
          // xmlImpuesto.getJDOObject().setImpDv(impuestoDivisa);
          // xmlImpuesto.getJDOObject().setImpEu(impuestoEur);
          //
          // impuestos.add(xmlImpuesto.getJDOObject());
          //
          // tmct0gal.getImpuestos().addAll(impuestos);
          // }
          
          impuestoEur = getRestValue((Map<String, Object>) entry.getValue(), "importeEu");
          impuestoDivisa = getRestValue((Map<String, Object>) entry.getValue(), "importeDv");

          if (entry.getKey().equals(ImpuestoEnum.MARKET_TAX.name())) {
            {
              //final BigDecimal imimpeur = getRestValue((Map<String, Object>) entry.getValue(), "imimpeur");
              //final BigDecimal imimpdvs = getRestValue((Map<String, Object>) entry.getValue(), "imimpdvs");

              tmct0gal.setImimpeur(impuestoEur);
              tmct0gal.setImimpeurb(impuestoEur);

              tmct0gal.setImimpdvs(impuestoDivisa);
              tmct0gal.setImimpdvsb(impuestoDivisa);

            }

          }

          if (entry.getKey().equals(ImpuestoEnum.CENTER_MKT_CHARGE.name())) {

            //final BigDecimal imgatdvsb = getRestValue((Map<String, Object>) entry.getValue(), "imgatdvsb");
            //final BigDecimal imgastosb = getRestValue((Map<String, Object>) entry.getValue(), "imgastosb");

            tmct0gal.setImgatdvsb(impuestoDivisa);
            tmct0gal.setImgastosb(impuestoEur);

          }

          if (entry.getKey().equals(ImpuestoEnum.FOREIGN_MC.name())) {
            
            

            //if ((boolean) ((Map<String, Object>) entry.getValue()).get("pagoClte")) 
            {
              tmct0gal.setImgatdvs(impuestoDivisa);
              tmct0gal.setImgastos(impuestoEur);
            }

            // Si va activado
            if ((boolean) ((Map<String, Object>) entry.getValue()).get("pagoMkt")) {
              tmct0gal.setCdindgasb(SibbacEnums.SI_NO.SI.getValue());
            }
            else {
              tmct0gal.setCdindgasb(SibbacEnums.SI_NO.NO.getValue());

            }

            // if ((boolean) ((Map<String, Object>)
            // entry.getValue()).get("pagoMkt")) {
            // tmct0gal.setImgatdvsb(impuestoDivisa);
            // tmct0gal.setImgastosb(impuestoEur);
            // }

            // Solo se informa al ftt a nivel de broker si la diferencia entre
            // bruto y broker es > que la comisión broker

            // if (BigDecimal.ZERO.compareTo(impuestoDivisa) != 0) {
            // final Character tipoOperacion =
            // tmct0gal.getTmct0gre().getTmct0bok().getCdtpoper();
            //
            // imbrmercb
            // final BigDecimal brutoDivisa = tmct0gal.getImbrmercb()
            // final BigDecimal netoDivisa = tmct0bok.getImnetocl();
            // final BigDecimal comisionClienteDivisa = tmct0bok.getImfpacdv();
            // final BigDecimal fttDivisa = acu_dv;
            // BigDecimal marketTaxDivisa = BigDecimal.ZERO;
            //
            // BigDecimal diffDivisa = (tipoOperacion == 'V') ?
            // brutoDivisa.subtract(netoDivisa) : netoDivisa
            // .subtract(brutoDivisa);
            // diffDivisa =
            // diffDivisa.subtract(comisionClienteDivisa).subtract(fttDivisa);
            //
            // if (new BigDecimal(0.1).compareTo(diffDivisa) <= 0) {
            // marketTaxDivisa = diffDivisa;
            // tmct0bok.setImimpdvs(marketTaxDivisa.add(tmct0bok.getImimpdvs()));
            //
            // final BigDecimal imtpcamb = tmct0bok.getImtpcamb();
            // final BigDecimal marketTaxEuro =
            // marketTaxDivisa.multiply(imtpcamb);
            //
            // tmct0bok.setImimpeur(marketTaxEuro.add(tmct0bok.getImimpeur()));
            //
            // }
            //
            //
            //

          }
        }
      }

    }
    else {
      BigDecimal brutoMercadoAllo = getValue(mAllocation, "brutoMercadoAlloAcumulado");
      BigDecimal brutoMercadoGrupo = tmct0gal.getNutitagr().multiply(tmct0gal.getTmct0gre().getPrecioBrutoMkt())
          .setScale(2, RoundingMode.HALF_UP);

      BigDecimal proporcion = brutoMercadoGrupo.divide(brutoMercadoAllo, 8, RoundingMode.HALF_UP);
      tmct0gal.setImsvb(getProportionalValueAndSubstratRest(mAllocation, "imsvb", proporcion));
      tmct0gal.setImsvbeu(getProportionalValueAndSubstratRest(mAllocation, "imsvbeu", proporcion));
      tmct0gal.setImfibrdv(getProportionalValueAndSubstratRest(mAllocation, "imfibrdv", proporcion));
      tmct0gal.setImfibreu(getProportionalValueAndSubstratRest(mAllocation, "imfibreu", proporcion));
      tmct0gal.setImtotnet(getProportionalValueAndSubstratRest(mAllocation, "imtotnet", proporcion));
      tmct0gal.setEutotnet(getProportionalValueAndSubstratRest(mAllocation, "eutotnet", proporcion));
      tmct0gal.setImcomdev(getProportionalValueAndSubstratRest(mAllocation, "imcomdev", proporcion));
      tmct0gal.setEucomdev(getProportionalValueAndSubstratRest(mAllocation, "eucomdev", proporcion));
      tmct0gal.setImcomisn(getProportionalValueAndSubstratRest(mAllocation, "imcomisn", proporcion));
      tmct0gal.setImcomieu(getProportionalValueAndSubstratRest(mAllocation, "imcomieu", proporcion));

      // mapeo
      tmct0gal.setImbrmerc(getProportionalValueAndSubstratRest(mAllocation, "imtotbru", proporcion));
      tmct0gal.setImbrmreu(getProportionalValueAndSubstratRest(mAllocation, "eutotbru", proporcion));
      tmct0gal.setImbrmercb(getProportionalValueAndSubstratRest(mAllocation, "imbrmerc", proporcion));
      tmct0gal.setImbrmreub(getProportionalValueAndSubstratRest(mAllocation, "imbrmreu", proporcion));

      tmct0gal.setImgasgeseu(getProportionalValueAndSubstratRest(mAllocation, "imgasgeseu", proporcion));
      tmct0gal.setImgasgesdv(getProportionalValueAndSubstratRest(mAllocation, "imgasgesdv", proporcion));
      tmct0gal.setImgasgeseub(getProportionalValueAndSubstratRest(mAllocation, "imgasgeseu", proporcion));
      tmct0gal.setImgasgesdvb(getProportionalValueAndSubstratRest(mAllocation, "imgasgesdv", proporcion));

      tmct0gal.setImgasliqeu(getProportionalValueAndSubstratRest(mAllocation, "imgasliqeu", proporcion));
      tmct0gal.setImgasliqdv(getProportionalValueAndSubstratRest(mAllocation, "imgasliqdv", proporcion));
      tmct0gal.setImgasliqeub(getProportionalValueAndSubstratRest(mAllocation, "imgasliqeu", proporcion));
      tmct0gal.setImgasliqdvb(getProportionalValueAndSubstratRest(mAllocation, "imgasliqdv", proporcion));

      // broker

      // fin mapeo campos nuevos

      tmct0gal.setImCanonCompDv(getProportionalValueAndSubstratRest(mAllocation, "imCanonCompDv", proporcion));
      tmct0gal.setImCanonCompEu(getProportionalValueAndSubstratRest(mAllocation, "imCanonCompEu", proporcion));

      for (Entry<String, Object> entry : ((Map<String, Object>) mAllocation.get("impuesto")).entrySet()) {

        if ('E' == tmct0gal.getTmct0gre().getTmct0bok().getTmct0ord().getCdclsmdo())

        {
          

          // for (Impuesto impuestoFor : tmct0gal.getImpuestos()) {
          // if (entry.getKey().equals(impuesto.getId().getNombre())) {
          // impuesto = impuestoFor;
          // break;
          //
          // }
          // }
          // if (impuesto != null) {
          // impuesto.setImpDv(impuesto.getImpDv().add(impuestoDivisa));
          // impuesto.setImpEu(impuesto.getImpEu().add(impuestoEur));
          // }
          // else {
          // xmlImpuesto = new XmlImpuesto(tmct0gal);
          // xmlImpuesto.xml_mapping(entry);
          // xmlImpuesto.getJDOObject().setPagoClte(
          // (boolean) ((Map<String, Object>) entry.getValue()).get("pagoClte")
          // ?
          // 1 : 0);
          // xmlImpuesto.getJDOObject().setPagoMkt(
          // (boolean) ((Map<String, Object>) entry.getValue()).get("pagoMkt") ?
          // 1
          // : 0);
          // xmlImpuesto.getJDOObject().setImpDv(impuestoDivisa);
          // xmlImpuesto.getJDOObject().setImpEu(impuestoEur);
          //
          // impuestos.add(xmlImpuesto.getJDOObject());
          //
          // // tmct0gal.getImpuestos().addAll(impuestos);
          // }
          
          impuestoEur = getProportionalValueAndSubstratRest((Map<String, Object>) entry.getValue(), "importeEu", proporcion);
          impuestoDivisa = getProportionalValueAndSubstratRest((Map<String, Object>) entry.getValue(), "importeDv", proporcion);

          if (entry.getKey().equals(ImpuestoEnum.MARKET_TAX.name())) {
            
              //final BigDecimal imimpeur = getProportionalValueAndSubstratRest((Map<String, Object>) entry.getValue(), "imimpeur", proporcion);
              //final BigDecimal imimpdvs = getProportionalValueAndSubstratRest((Map<String, Object>) entry.getValue(), "imimpdvs", proporcion);
              
             

              tmct0gal.setImimpeur(impuestoEur);
              tmct0gal.setImimpeurb(impuestoEur);

              tmct0gal.setImimpdvs(impuestoDivisa);
              tmct0gal.setImimpdvsb(impuestoDivisa);

            

          }

          if (entry.getKey().equals(ImpuestoEnum.CENTER_MKT_CHARGE.name())) {

            //final BigDecimal imgatdvsb = getProportionalValueAndSubstratRest((Map<String, Object>) entry.getValue(), "imgatdvsb", proporcion);
            //final BigDecimal imgastosb = getProportionalValueAndSubstratRest((Map<String, Object>) entry.getValue(), "imgastosb", proporcion);
            
            

            tmct0gal.setImgatdvsb(impuestoDivisa);
            tmct0gal.setImgastosb(impuestoEur);

          }

          if (entry.getKey().equals(ImpuestoEnum.FOREIGN_MC.name())) {       
            
              
            
            if ((boolean) ((Map<String, Object>) entry.getValue()).get("pagoClte")) {
              tmct0gal.setImgatdvs(impuestoDivisa);
              tmct0gal.setImgastos(impuestoEur);
            }

            // Si va activado
            if ((boolean) ((Map<String, Object>) entry.getValue()).get("pagoMkt")) {
              tmct0gal.setCdindgasb(SibbacEnums.SI_NO.SI.getValue());
            }
            else {
              tmct0gal.setCdindgasb(SibbacEnums.SI_NO.NO.getValue());

            }

            // if ((boolean) ((Map<String, Object>)
            // entry.getValue()).get("pagoMkt")) {
            // tmct0gal.setImgatdvsb(impuestoDivisa);
            // tmct0gal.setImgastosb(impuestoEur);
            // }

          }
        }
      }

    }

    if (tmct0gal.getImtotnet().compareTo(BigDecimal.ZERO) != 0) {
      tmct0gal.setPccomsvb(tmct0gal.getImsvb().divide(tmct0gal.getImtotnet(), 6, RoundingMode.HALF_UP));
    }
    else {
      tmct0gal.setPccomsvb(BigDecimal.ZERO);
    }
  }

  private BigDecimal getProportionalValueAndSubstratRest(Map<String, Object> mAllocation, String key,
      BigDecimal proporcion) {
    BigDecimal value = ((BigDecimal) mAllocation.get(key)).multiply(proporcion).setScale(2, RoundingMode.HALF_UP);
    key += "_rest";
    mAllocation.put(key, ((BigDecimal) mAllocation.get(key)).subtract(value));
    return value;
  }

  private void substratValue(Map<String, Object> mAllocation, String key, BigDecimal value) {
    mAllocation.put(key, ((BigDecimal) mAllocation.get(key)).subtract(value));
    key += "_rest";
    mAllocation.put(key, ((BigDecimal) mAllocation.get(key)).subtract(value));
  }

  private BigDecimal getRestValue(Map<String, Object> mAllocation, String key) {
    key += "_rest";
    BigDecimal res = (BigDecimal) mAllocation.get(key);
    mAllocation.put(key, BigDecimal.ZERO);
    return res;
  }

  private BigDecimal getValue(Map<String, Object> mAllocation, String key) {
    return (BigDecimal) mAllocation.get(key);
  }

  public Tmct0gal getJDOObject() {
    return tmct0gal;
  }

}
