package sibbac.business.comunicaciones.pti.msgprocess.bo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.common.Tmct0xasAndTmct0cfgConfig;
import sibbac.business.fase0.database.bo.TptiInBo;
import sibbac.business.fase0.database.dto.TptiInDTO;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class PtiMessageProcessBo extends WrapperExecutor {

  private static final Logger LOG = LoggerFactory.getLogger(PtiMessageProcessSelectorBo.class);

  @Autowired
  private TptiInBo inBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private PtiMessageProcessSelectorBo messageProcessSelectorBo;

  public PtiMessageProcessBo() {
  }

  public void executeTask(boolean sd) throws Exception {
    LOG.debug("[executeTask] inicio");

    try (Tmct0xasAndTmct0cfgConfig cache = ctx.getBean(Tmct0xasAndTmct0cfgConfig.class)) {
      String versionCfg = cache.getPtiMessageVersion();
      if (StringUtils.isBlank(versionCfg)) {
        throw new SIBBACBusinessException("INCIDENCIA-Version mensajeria pti no configurada.");
      }

      String idCamara = cache.getIdPti();

      if (StringUtils.isBlank(idCamara)) {
        throw new SIBBACBusinessException("INCIDENCIA-Id Camara no configurada.");
      }
      this.procesarMensajes(sd, cache);
    }
    LOG.debug("[executeTask] Borrando PTI Messages procesados OK...");
    this.deleteProcessedWithOutError(sd);
    LOG.debug("[executeTask] fin");
  }

  private void procesarMensajes(boolean sd, Tmct0xasAndTmct0cfgConfig cache) throws SIBBACBusinessException {

    LOG.debug("[executeTask] Buscamos Msgs que procesar con orden...");
    List<TptiInDTO> listProcessSinOrden = getListProcessSinOrden(sd, cache);

    LOG.debug("[executeTask] Buscamos Msgs que procesar con orden...");
    List<TptiInDTO> listProcessConOrden = getListProcessConOrden(sd, cache);

    LOG.debug("[executeTask] Procesando PTI Messages en orden...");
    this.processPtiMessages(listProcessConOrden, sd, cache);

    LOG.debug("[executeTask] Procesando PTI Messages SIN orden...");
    this.processPtiMessages(listProcessSinOrden, sd, cache);
  }

  private List<TptiInDTO> getListProcessSinOrden(final boolean sd, Tmct0xasAndTmct0cfgConfig cache) {

    final List<TptiInDTO> res = Collections.synchronizedList(new ArrayList<TptiInDTO>());
    final Character sctiptDividend;
    if (sd) {
      sctiptDividend = 'S';
    }
    else {
      sctiptDividend = 'N';
    }

    List<String> msgInOrdenTypes = cache.getPtiMessageTypesInOrden();
    List<TptiInDTO> fechasTipos = inBo
        .findAllDistinctMessageTypeAndMessageDateByNotInMessageTypesAndNotProcessedAndScriptDividend(msgInOrdenTypes,
            sctiptDividend);
    if (CollectionUtils.isNotEmpty(fechasTipos)) {
      ExecutorService executor = getNewExecutor();

      try {
        for (final TptiInDTO tptiInDTO : fechasTipos) {
          Runnable p = new Runnable() {

            @Override
            public void run() {
              TptiInDTO inRes;
              try {

                List<Long> ids = inBo.findAllIdsByMessageDateAndMessageTypeNotProcessedAndScriptDividend(
                    tptiInDTO.getMessageDate(), tptiInDTO.getMessageType(), sctiptDividend);
                LOG.debug("[processPtiMessgesSinOrden] se han encontrado {} ids de fecha {}-{} que procesar.",
                    ids.size(), tptiInDTO.getMessageDate(), tptiInDTO.getMessageType());
                if (CollectionUtils.isNotEmpty(ids)) {
                  inRes = new TptiInDTO(tptiInDTO.getMessageType(), tptiInDTO.getMessageDate());
                  inRes.setIdsToProcess(ids);
                  res.add(inRes);
                }
              }
              catch (Exception e) {
                LOG.warn(e.getMessage(), e);
              }

            }
          };
          executor.execute(p);
        }
      }
      catch (Exception e) {
        if (executor != null)
          closeExecutorNow(executor);
        LOG.debug(e.getMessage(), e);
      }
      finally {
        if (executor != null)
          closeExecutor(executor);
      }
      Collections.sort(res, new Comparator<TptiInDTO>() {

        @Override
        public int compare(TptiInDTO arg0, TptiInDTO arg1) {
          int compDate = arg0.getMessageDate().compareTo(arg1.getMessageDate());

          if (compDate == 0) {
            return arg0.getMessageType().compareTo(arg1.getMessageType());
          }
          else {
            return compDate;
          }
        }
      });
    }

    return res;
  }

  private List<TptiInDTO> getListProcessConOrden(final boolean sd, final Tmct0xasAndTmct0cfgConfig cache) {
    List<String> groupDatas;
    final List<TptiInDTO> res = Collections.synchronizedList(new ArrayList<TptiInDTO>());
    final Character scriptdividend;
    if (sd) {
      scriptdividend = 'S';
    }
    else {
      scriptdividend = 'N';
    }

    List<String> msgInOrdenTypes = cache.getPtiMessageTypesInOrden();
    List<TptiInDTO> fechasTipos = inBo
        .findAllDistinctMessageTypeAndMessageDateByMessageTypesAndNotProcessedAndScriptDividend(msgInOrdenTypes,
            scriptdividend);

    if (CollectionUtils.isNotEmpty(fechasTipos)) {
      ExecutorService executor = getNewExecutor();
      try {

        for (final TptiInDTO tptiInDTO : fechasTipos) {
          groupDatas = inBo.findAllGroupDataByMessageDateAndMessageTypeNotProcessed(tptiInDTO.getMessageDate(),
              tptiInDTO.getMessageType());
          for (final String groupData : groupDatas) {
            Runnable p = new Runnable() {
              @Override
              public void run() {

                TptiInDTO inRes;
                try {
                  List<Long> ids = inBo
                      .findAllIdsByGroupDataAndMessageDateAndMessageTypeAndNotProcessedAndScriptDividend(groupData,
                          tptiInDTO.getMessageDate(), tptiInDTO.getMessageType(), scriptdividend);
                  LOG.debug("[processPtiMessgesSinOrden] se han encontrado {} ids de fecha {}-{} que procesar.",
                      ids.size(), tptiInDTO.getMessageDate(), tptiInDTO.getMessageType());
                  if (CollectionUtils.isNotEmpty(ids)) {
                    inRes = new TptiInDTO(tptiInDTO.getMessageType(), tptiInDTO.getMessageDate());
                    inRes.setGroupData(groupData);
                    inRes.setIdsToProcess(ids);
                    res.add(inRes);
                  }
                }
                catch (Exception e) {
                  LOG.warn(e.getMessage(), e);
                }

              }
            };
            executor.execute(p);

          }
        }
      }
      catch (Exception e) {
        if (executor != null)
          closeExecutorNow(executor);
        LOG.debug(e.getMessage(), e);
      }
      finally {
        if (executor != null)
          closeExecutor(executor);
      }
      Collections.sort(res, new Comparator<TptiInDTO>() {

        @Override
        public int compare(TptiInDTO arg0, TptiInDTO arg1) {
          int compDate = arg0.getMessageDate().compareTo(arg1.getMessageDate());

          if (compDate == 0) {
            int compType = arg0.getMessageType().compareTo(arg1.getMessageType());
            if (compType == 0) {
              return arg0.getGroupData().compareTo(arg1.getGroupData());
            }
            else {
              return compType;
            }
          }
          else {
            return compDate;
          }
        }
      });
    }

    return res;
  }

  @SuppressWarnings("unchecked")
  private void processPtiMessages(List<TptiInDTO> toProcess, boolean isSd, Tmct0xasAndTmct0cfgConfig cache)
      throws SIBBACBusinessException {
    LOG.debug("[processPtiMessages] inicio");

    List<Long> ids;
    if (CollectionUtils.isNotEmpty(toProcess)) {
      ExecutorService executor = null;
      List<Future<Void>> futures = new ArrayList<Future<Void>>();
      try {
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        if (isSd) {
          tfb.setNameFormat("Thread-ProcesamientoMsgPti-SD-%d");
        }
        else {
          tfb.setNameFormat("Thread-ProcesamientoMsgPti-%d");
        }
        executor = Executors.newFixedThreadPool(getThreadSize(), tfb.build());
        for (TptiInDTO tp : toProcess) {
          ids = tp.getIdsToProcess();
          LOG.debug("[processPtiMessages] {} ids: {}", tp, ids.size());
          if (CollectionUtils.isNotEmpty(ids)) {
            if (StringUtils.isNotBlank(tp.getGroupData())) {
              LOG.debug("[processPtiMessages] Group Data: {}.", tp.getGroupData());
              futures.add(submitCallable(executor, getNewRunnableProcessPtiMessages(ids, cache)));

            }
            else {
              LOG.debug("[processPtiMessages] SIN Group Data.");
              int pos = 0;
              int posEnd = 0;
              int tam = calculateTransactionSizeBetweenThreads(ids.size());
              LOG.debug("[processPtiMessages] tam to thread: {}", tam);
              while (pos < ids.size()) {
                posEnd += tam;
                if (posEnd > ids.size()) {
                  posEnd = ids.size();
                }
                futures
                    .add(submitCallable(executor, getNewRunnableProcessPtiMessages(ids.subList(pos, posEnd), cache)));

                pos = posEnd;

              }
            }
          }
        }

      }
      catch (Exception e) {
        if (executor != null) {
          closeExecutorNow(executor);
        }
        throw new SIBBACBusinessException(e);
      }
      finally {
        if (executor != null) {
          closeExecutor(executor);
        }
      }
    }
    LOG.debug("[processPtiMessages] fin");

  }

  private void deleteProcessedWithOutError(boolean sd) throws SIBBACBusinessException {
    LOG.debug("[deleteProcessedWithOutError] inicio");

    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DAY_OF_YEAR, -1);
    cal.add(Calendar.MONTH, -2);

    Character scriptdividend = 'N';

    if (sd) {
      scriptdividend = 'S';
    }

    List<Long> ids = inBo.findAllIdsByProcessedAndWithoutError(scriptdividend);

    ids.addAll(inBo.findAllIdsByProcessedAndWithErrorAndSctiptDividend(cal.getTime(), scriptdividend));
    LOG.debug("[deleteProcessedWithOutError] Vamos a borrar {} ids.", ids.size());
    if (CollectionUtils.isNotEmpty(ids)) {
      ExecutorService executor = getNewExecutor();
      try {
        int pos = 0;
        int posEnd = 0;

        int tam = calculateTransactionSizeBetweenThreads(ids.size());
        while (pos < ids.size()) {
          posEnd += tam;
          if (posEnd > ids.size()) {
            posEnd = ids.size();
          }
          submitCallable(executor, getNewRunnableProcessDeletePtiMessage(ids.subList(pos, posEnd)));
          pos = posEnd;

        }
      }
      catch (Exception e) {
        if (executor != null)
          closeExecutorNow(executor);
        throw new SIBBACBusinessException(e);
      }
      finally {
        if (executor != null)
          closeExecutor(executor);
      }
    }
    LOG.debug("[deleteProcessedWithOutError] fin");
  }

  private Callable<Void> getNewRunnableProcessPtiMessages(final List<Long> ids, final Tmct0xasAndTmct0cfgConfig cache) {
    Callable<Void> p = new Callable<Void>() {

      @Override
      public Void call() throws SIBBACBusinessException {
        for (Long id : ids) {
          try {
            messageProcessSelectorBo.parseAndProcessSelectorPtiMessage(id, cache);
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA-Procesando msg id: %s", id), e);
          }
        }

        return null;
      }
    };
    return p;
  }

  private Callable<Void> getNewRunnableProcessDeletePtiMessage(final List<Long> ids) {
    Callable<Void> p = new Callable<Void>() {

      @Override
      public Void call() {
        try {
          inBo.deletePtiMesages(ids);
        }
        catch (Exception e) {
          LOG.warn("INCIDENCIA- borrando ids: {}- {}", ids, e.getMessage(), e);
        }
        finally {
        }
        return null;
      }
    };
    return p;
  }

  // private void cargarMovimientosEnOrdenes() throws SIBBACBusinessException {
  // LOG.debug("[cargarMovimientosEnOrdenes] inicio.");
  // ExecutorService myExecutor = Executors.newFixedThreadPool(getThreadSize());
  // try {
  //
  // }
  // catch (Exception e) {
  // myExecutor.shutdownNow();
  // try {
  // myExecutor.awaitTermination(10, TimeUnit.MINUTES);
  // }
  // catch (InterruptedException ex) {
  // LOG.debug(ex.getMessage(), ex);
  // }
  // throw new SIBBACBusinessException(e);
  // }
  // finally {
  // if (myExecutor != null) {
  // if (!myExecutor.isShutdown()) {
  // myExecutor.shutdown();
  // }
  // try {
  // myExecutor.awaitTermination(12, TimeUnit.HOURS);
  // }
  // catch (InterruptedException e) {
  // LOG.debug(e.getMessage(), e);
  // }
  // }
  // }
  // LOG.debug("[cargarMovimientosEnOrdenes] fin.");
  // }

  @Override
  public int getThreadSize() {
    return 25;
  }

  @Override
  public int getTransactionSize() {
    return 100;
  }
}
