package sibbac.business.comunicaciones.pti.escalado.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.titulares.bo.TitularesConfigDTO;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;

@Component
@Scope(value = "prototype")
public class EscaladoEstadoMovimientosByBookingCallable implements Callable<CallableResultDTO> {

  private static final Logger LOG = LoggerFactory.getLogger(EscaladoEstadoMovimientosByBookingCallable.class);

  private List<Tmct0bokId> listBookId;
  private TitularesConfigDTO config;

  public EscaladoEstadoMovimientosByBookingCallable(List<Tmct0bokId> listBookId,
      TitularesConfigDTO config) {
    this.listBookId = new ArrayList<Tmct0bokId>();
    if (listBookId != null)
      this.listBookId.addAll(listBookId);
    this.config = config;
  }

  @Autowired
  private EscaladoEstadoMovimientosBo escaladoBo;

  @Override
  public CallableResultDTO call() throws Exception {
    CallableResultDTO res = new CallableResultDTO();
    LOG.trace("[run] inicio");
    escaladoBo.escalarBookings(listBookId, config);
    LOG.trace("[run] fin");
    return res;
  }

}
