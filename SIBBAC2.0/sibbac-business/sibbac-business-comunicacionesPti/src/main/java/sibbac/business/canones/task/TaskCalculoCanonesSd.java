package sibbac.business.canones.task;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.canones.bo.CalculoCanonesMultithread;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_COMUNICACIONES_ECC.NAME, name = Task.GROUP_COMUNICACIONES_ECC.JOB_CALCULO_CANONES_SD, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0/10 7-23 ? * MON-FRI")
public class TaskCalculoCanonesSd extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private CalculoCanonesMultithread calculoCanones;

  @Override
  public void executeTask() throws Exception {
    calculoCanones.bajaCanonCalculadoDesglosesBaja(true, "SIBBAC20");
    calculoCanones.calcularCanon(true, "SIBBAC20");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_CALCULO_CANONES_SD;
  }

}
