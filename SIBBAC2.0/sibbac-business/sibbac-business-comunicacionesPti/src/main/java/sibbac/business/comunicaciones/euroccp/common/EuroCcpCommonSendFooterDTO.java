package sibbac.business.comunicaciones.euroccp.common;

import java.util.Date;

public class EuroCcpCommonSendFooterDTO extends EuroCcpCommonSendRegisterDTO {

  private String clientNumber;
  private Date sendDate;
  private Integer recordCount;
  private String filler;

  public EuroCcpCommonSendFooterDTO() {
    super(EuroCcpCommonSendRegisterTypeEnum.FOOTER);
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public Date getSendDate() {
    return sendDate;
  }

  public Integer getRecordCount() {
    return recordCount;
  }

  public String getFiller() {
    return filler;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public void setSendDate(Date sendDate) {

    this.sendDate = sendDate;
  }

  public void setRecordCount(Integer recordCount) {
    this.recordCount = recordCount;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

  @Override
  public String toString() {
    return String.format("EuroCcpCommonSendFooterDTO [clientNumber=%s, sendDate=%s, recordCount=%s, filler=%s]",
        clientNumber, sendDate, recordCount, filler);
  }

}
