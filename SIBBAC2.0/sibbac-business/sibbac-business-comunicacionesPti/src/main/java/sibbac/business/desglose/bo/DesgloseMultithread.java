package sibbac.business.desglose.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.desglose.commons.AlgoritmoDesgloseManual;
import sibbac.business.desglose.commons.DesgloseConfigDTO;
import sibbac.business.desglose.commons.DesgloseManualDTO;
import sibbac.business.desglose.commons.SolicitudDesgloseManual;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0ReglaBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class DesgloseMultithread {

  @Value("${sibbac.nacional.desglose.automatico.tmct0bok.threadpool.size:10}")
  private int threadSizeAutomatico;

  @Value("${sibbac.nacional.desglose.manual.tmct0bok.threadpool.size:10}")
  private int threadSizeManual;

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(DesgloseMultithread.class);

  @Autowired
  private DesgloseWithUserLock desgloseWithUserLock;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejealoBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0ReglaBo reglaAsignacionBo;

  @Autowired
  private ApplicationContext ctx;

  public DesgloseMultithread() {
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void desgloseAutomatico(boolean sd) throws SIBBACBusinessException {
    String msg;
    Tmct0bokId bokId;
    DesgloseAutomaticoTmct0bokCallable desgloseAutomaticoCallable;

    char isscrdiv = sd ? 'S' : 'N';

    List<Tmct0bokId> listBookings = aloBo.findAllBookingIdContainsTmct0aloByCdestadoasigAndIssrcdiv(
        EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSAR.getId(), isscrdiv);

    if (CollectionUtils.isNotEmpty(listBookings)) {
      try (DesgloseConfigDTO config = ctx.getBean(DesgloseConfigDTO.class);) {
        config.setAuditUser("SIBBAC20");
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        if (sd) {
          tfb.setNameFormat("Thread-DesgloseAutomatico-SD-%d");
        }
        else {
          tfb.setNameFormat("Thread-DesgloseAutomatico-%d");
        }

        ExecutorService executor = Executors.newFixedThreadPool(threadSizeAutomatico, tfb.build());
        Future<Void> future;
        List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
        Map<Future<Void>, DesgloseAutomaticoTmct0bokCallable> mapCallables = new HashMap<Future<Void>, DesgloseAutomaticoTmct0bokCallable>();
        try {
          for (Tmct0bokId tmct0bokId : listBookings) {
            LOG.debug("[desgloseWithUserLock] encolando proceso para desglosar booking: {}", tmct0bokId);
            try {
              desgloseAutomaticoCallable = new DesgloseAutomaticoTmct0bokCallable(desgloseWithUserLock, config,
                  tmct0bokId);
              future = executor.submit(desgloseAutomaticoCallable);
              listFutures.add(future);
              mapCallables.put(future, desgloseAutomaticoCallable);
            }
            catch (Exception e) {
              msg = String.format("Incidencia lanzando hilo para booking %s %s", tmct0bokId, e.getMessage());
              throw new SIBBACBusinessException(msg, e);
            }
          }

          executor.shutdown();
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          executor.shutdownNow();

        }
        finally {

          try {
            // esperamos a los hilos 10 minutos por hilo + 30 minutos de minimo
            executor.awaitTermination(10 * listBookings.size() + 30, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }

          for (Future<Void> future2 : listFutures) {
            desgloseAutomaticoCallable = mapCallables.get(future2);
            bokId = null;
            if (desgloseAutomaticoCallable != null) {
              bokId = desgloseAutomaticoCallable.getBookId();
              LOG.trace("[desgloseWithUserLock] revisando resultado hilo booking: {}", bokId);
            }
            try {
              future2.get(2, TimeUnit.MINUTES);
            }
            catch (InterruptedException | ExecutionException e) {
              LOG.warn("[desgloseWithUserLock] incidencia con booking {} {}", bokId, e.getMessage(), e);
            }
            catch (TimeoutException e) {
              if (!future2.isDone() && future2.cancel(true)) {
                LOG.warn("[desgloseWithUserLock] cancelando el future: {} booking: {}", future2, bokId);
              }
              else {
                LOG.warn("[desgloseWithUserLock] incidencia con booking {} {}", bokId, e.getMessage(), e);
              }
            }
            catch (Exception e) {
              LOG.warn("[desgloseWithUserLock] incidencia con booking {} {}", bokId, e.getMessage(), e);
              // throw new SIBBACBusinessException(e.getMessage(), e);
            }
          }

          if (!executor.isTerminated()) {
            executor.shutdownNow();
            try {
              executor.awaitTermination(1, TimeUnit.MINUTES);
            }
            catch (InterruptedException e) {
              LOG.warn(e.getMessage(), e);
            }
          }
          mapCallables.clear();
          listBookings.clear();
          listFutures.clear();
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
      }
    }

  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public CallableResultDTO desgloseManual(List<SolicitudDesgloseManual> listSolicitudDesgloseManual,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    CallableResultDTO res = new CallableResultDTO();
    String msg;
    Tmct0bokId bokId;
    DesgloseManualTmct0bokCallable desgloseAutomaticoCallable;
    List<SolicitudDesgloseManual> listSolicitudes;
    List<Tmct0bokId> listBookings = new ArrayList<Tmct0bokId>();
    Map<Tmct0bokId, List<SolicitudDesgloseManual>> mapSolicitudesAgrupadas = new HashMap<Tmct0bokId, List<SolicitudDesgloseManual>>();

    for (SolicitudDesgloseManual solicitudDesgloseManual : listSolicitudDesgloseManual) {
      bokId = new Tmct0bokId(solicitudDesgloseManual.getTmct0aloId().getNbooking(), solicitudDesgloseManual
          .getTmct0aloId().getNuorden());
      if (!listBookings.contains(bokId)) {
        listBookings.add(bokId);
      }

      if (mapSolicitudesAgrupadas.get(bokId) == null) {
        mapSolicitudesAgrupadas.put(bokId, new ArrayList<SolicitudDesgloseManual>());
      }
      mapSolicitudesAgrupadas.get(bokId).add(solicitudDesgloseManual);
    }

    if (CollectionUtils.isNotEmpty(listBookings)) {
      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat(String.format("Thread-DesgloseManual-%s-%s-%s", config.getTipoDesgloseManual(),
          config.getAuditUser(), "%d"));
      ExecutorService executor = Executors.newFixedThreadPool(threadSizeManual, tfb.build());
      Future<CallableResultDTO> future;
      List<Future<CallableResultDTO>> listFutures = new ArrayList<Future<CallableResultDTO>>();
      Map<Future<CallableResultDTO>, DesgloseManualTmct0bokCallable> mapCallables = new HashMap<Future<CallableResultDTO>, DesgloseManualTmct0bokCallable>();
      try {
        for (Tmct0bokId tmct0bokId : listBookings) {
          LOG.debug("[desgloseManual] encolando proceso para desglosar booking: {}", tmct0bokId);
          listSolicitudes = mapSolicitudesAgrupadas.get(tmct0bokId);
          if (CollectionUtils.isNotEmpty(listSolicitudes)) {
            try {
              desgloseAutomaticoCallable = new DesgloseManualTmct0bokCallable(desgloseWithUserLock, config, tmct0bokId,
                  listSolicitudes);

              future = executor.submit(desgloseAutomaticoCallable);
              listFutures.add(future);
              mapCallables.put(future, desgloseAutomaticoCallable);
            }
            catch (Exception e) {
              msg = String.format("Incidencia lanzando hilo para booking %s %s", tmct0bokId, e.getMessage());
              throw new SIBBACBusinessException(msg, e);
            }
          }
        }

        executor.shutdown();
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
        res.addErrorMessage("INCIDENCIA Problema ejecutando hilos para realizar los desgloses.");
      }
      finally {

        try {
          // esperamos a los hilos 10 minutos por hilo + 30 minutos de minimo
          executor.awaitTermination(10 * listBookings.size() + 30, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }

        for (Future<CallableResultDTO> future2 : listFutures) {
          desgloseAutomaticoCallable = mapCallables.get(future2);

          if (desgloseAutomaticoCallable != null) {
            bokId = desgloseAutomaticoCallable.getBookId();
            LOG.trace("[desgloseManual] revisando resultado hilo booking: {}", bokId);
            try {
              res.append(future2.get(2, TimeUnit.MINUTES));
            }
            catch (InterruptedException | ExecutionException e) {
              LOG.warn("[desgloseManual] incidencia con booking {} {}", bokId, e.getMessage(), e);
              res.addErrorMessage(String.format(
                  "[%s-%s]INCIDENCIA Se han parado los hilos sin terminar para el booking..",
                  bokId.getNuorden().trim(), bokId.getNbooking().trim()));
            }
            catch (TimeoutException e) {
              if (!future2.isDone() && future2.cancel(true)) {
                LOG.warn("[desgloseManual] cancelando el future: {} booking: {}", future2, bokId);
              }
              else {
                LOG.warn("[desgloseManual] INCIDENCIA con booking {} {}", bokId, e.getMessage(), e);
              }
              res.addErrorMessage(String.format("[%s-%s]Se han producido un timeout para el booking..", bokId
                  .getNuorden().trim(), bokId.getNbooking().trim()));
            }
            catch (Exception e) {
              LOG.warn("[desgloseManual] incidencia con booking {} {}", bokId, e.getMessage(), e);
              res.addErrorMessage(String.format("[%s-%s]INCIDENCIA inesperada con el booking. %s", bokId.getNuorden()
                  .trim(), bokId.getNbooking().trim(), e.getMessage()));

            }
          }

        }

        if (!executor.isTerminated()) {
          executor.shutdownNow();
          try {
            executor.awaitTermination(1, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
        mapCallables.clear();
        listBookings.clear();
        listFutures.clear();
        mapSolicitudesAgrupadas.clear();
        listBookings.clear();
      }

    }
    return res;
  }

  @Transactional
  public List<SolicitudDesgloseManual> crearSolicitudesDesglose(AlgoritmoDesgloseManual algoritmo,
      List<Tmct0aloId> listAlosSeleccionados, int threadSize, String auditUser) throws SIBBACBusinessException {
    List<SolicitudDesgloseManual> res = new ArrayList<SolicitudDesgloseManual>();
    if (!listAlosSeleccionados.isEmpty()) {
      Future<SolicitudDesgloseManual> future;
      DesgloseManualPantallaCreacionSolicitudDesgloseCallable callable;
      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      List<Future<SolicitudDesgloseManual>> listFutures = new ArrayList<Future<SolicitudDesgloseManual>>();

      tfb.setNameFormat(String.format("Thread-CrearSolicitudesDesglose-%s-%s", auditUser, "%d"));
      ExecutorService executor = Executors.newFixedThreadPool(threadSize, tfb.build());
      try {
        try (DesgloseConfigDTO config = ctx.getBean(DesgloseConfigDTO.class)) {
          for (Tmct0aloId tmct0aloId : listAlosSeleccionados) {
            callable = new DesgloseManualPantallaCreacionSolicitudDesgloseCallable(tmct0aloId, algoritmo, this, config);
            future = executor.submit(callable);
            listFutures.add(future);
          }
        }

        executor.shutdown();

        // esperamos 30 segundos por cada hilo mas 5 minutos mas
        executor.awaitTermination(listFutures.size() * 30 + 5 * 60, TimeUnit.SECONDS);

        for (Future<SolicitudDesgloseManual> future2 : listFutures) {
          res.add(future2.get(10, TimeUnit.SECONDS));
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
        throw new SIBBACBusinessException(String.format("INCIDENCIA-No se ha podido recuperar los desgloses por: %s",
            e.getMessage()), e);
      }
      finally {
        listFutures.clear();
      }

    }
    return res;
  }

  public SolicitudDesgloseManual crearSolicitudDesgloseManual(Tmct0aloId tmct0aloId, AlgoritmoDesgloseManual algoritmo,
      DesgloseConfigDTO config) throws SIBBACBusinessException {
    Tmct0alo alo = aloBo.findById(tmct0aloId);
    SettlementData sd = config.getSettlementDataNacional(alo);

    SolicitudDesgloseManual solicitud = new SolicitudDesgloseManual();
    solicitud.setTmct0aloId(tmct0aloId);
    solicitud.setSentido(alo.getCdtpoper());
    solicitud.setTitulosAlo(alo.getNutitcli());
    solicitud.setTitulosOrdenados(alo.getTmct0bok().getNutitord());
    solicitud.setTitulosEjecutados(alo.getTmct0bok().getNutiteje());
    solicitud.setPositionBreakDown('P');
    solicitud.setAlgoritmoDesgloseManual(algoritmo);
    solicitud.setAlias(alo.getCdalias());
    solicitud.setDescripcionAlias(alo.getDescrali());
    solicitud.setIsin(alo.getCdisin());
    solicitud.setDescripcionIsin(alo.getNbvalors());
    solicitud.setRefCliente(alo.getRefCliente());
    solicitud.setOrigenOrden(alo.getTmct0bok().getTmct0ord().getCdorigen());
    solicitud.setTipoOrden(alo.getTmct0bok().getTmct0ord().getCdtipord());
    solicitud.setCanal(alo.getTmct0bok().getTmct0ord().getCdcanal());
    solicitud.setFechaContratacion(alo.getFeoperac());
    solicitud.setFechaLiquidacion(alo.getFevalorr() == null ? alo.getFevalor() : alo.getFevalorr());
    solicitud.setMercado(alo.getTmct0bok().getTmct0ord().getCdmercad());
    solicitud.setMoneda(alo.getCdmoniso());
    solicitud.setAltas(new ArrayList<DesgloseManualDTO>());

    if (algoritmo == AlgoritmoDesgloseManual.FEWEST || algoritmo == AlgoritmoDesgloseManual.PROPORTIONAL
        || algoritmo == AlgoritmoDesgloseManual.MANUAL) {
      solicitud.getAltas().add(this.getNewDesgloseManual(alo, sd, config));
    }
    else if (algoritmo == AlgoritmoDesgloseManual.PRICE) {
      this.crearAltasByPrice(alo, sd, solicitud, config);
    }
    else if (algoritmo == AlgoritmoDesgloseManual.CLEARING_INFORMATION) {
      this.crearAltasByClearingInformation(alo, sd, solicitud, config);
    }
    return solicitud;
  }

  private void crearAltasByPrice(Tmct0alo alo, SettlementData sd, SolicitudDesgloseManual solicitud,
      DesgloseConfigDTO config) {
    List<TitulosDesgloseManualAgrupadosDTO> precios = dctBo.sumImtitulosGroupByImprecioByAloId(solicitud
        .getTmct0aloId());
    if (!precios.isEmpty()) {
      solicitud.getAltas().addAll(this.crearDesglosesManualesAgrupacion(alo, sd, precios, config));
    }
    else {
      precios = ejealoBo.sumImtitulosGroupByImprecioByAloId(solicitud.getTmct0aloId());
      if (!precios.isEmpty()) {
        solicitud.getAltas().addAll(this.crearDesglosesManualesAgrupacion(alo, sd, precios, config));
      }

    }
  }

  private List<DesgloseManualDTO> crearDesglosesManualesAgrupacion(Tmct0alo alo, SettlementData sd,
      List<TitulosDesgloseManualAgrupadosDTO> listTitulos, DesgloseConfigDTO config) {
    DesgloseManualDTO desglose;
    List<DesgloseManualDTO> altas = new ArrayList<DesgloseManualDTO>();
    for (TitulosDesgloseManualAgrupadosDTO titulos : listTitulos) {
      desglose = new DesgloseManualDTO();
      desglose.setTitulos(titulos.getTitulos());
      desglose.setPrecioFrom(titulos.getPrecio());
      desglose.setAccountAtClearer(alcBo.getAcctatcle(alo, sd));
      desglose.setBankReference(alcBo.getCdrefban(alo, sd));
      desglose.setClearingAccountCodeFrom(titulos.getCuentaCompensacionDestino());
      desglose.setClearingMemberCodeFrom(titulos.getMiembroCompensadorDestino());
      desglose.setGiveUpReferenceFrom(titulos.getCodigoReferenciaAsignacion());
      desglose.setSettlementData(sd);

      altas.add(desglose);
    }
    return altas;
  }

  private void crearAltasByClearingInformation(Tmct0alo alo, SettlementData sd, SolicitudDesgloseManual solicitud,
      DesgloseConfigDTO config) {
    List<TitulosDesgloseManualAgrupadosDTO> listClearingInformation = dctBo
        .sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloId(solicitud
            .getTmct0aloId());
    if (!listClearingInformation.isEmpty()) {
      solicitud.getAltas().addAll(this.crearDesglosesManualesAgrupacion(alo, sd, listClearingInformation, config));
    }
    else {
      listClearingInformation = ejealoBo
          .sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndCase(solicitud
              .getTmct0aloId());

      listClearingInformation
          .addAll(ejealoBo
              .sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndNotCase(solicitud
                  .getTmct0aloId()));
      if (!listClearingInformation.isEmpty()) {
        solicitud.getAltas().addAll(this.crearDesglosesManualesAgrupacion(alo, sd, listClearingInformation, config));
      }

    }
  }

  @Transactional
  public DesgloseManualDTO getNewDesgloseManual(Tmct0alo alo, SettlementData sd, DesgloseConfigDTO config)
      throws SIBBACBusinessException {
    DesgloseManualDTO desglose = new DesgloseManualDTO();
    desglose.setTitulos(alo.getNutitcli());
    desglose.setSettlementData(sd);
    if (sd.isDataClearer()) {
      desglose.setBankComissionPercent(sd.getClearer().getPccomisi());
    }
    else if (sd.isDataCustodian()) {
      desglose.setBankComissionPercent(sd.getClearer().getPccomisi());
    }

    desglose.setBankComissionPercent(sd.getTmct0ilq().getPccombco());
    desglose.setPayerComissionPercent(sd.getTmct0ilq().getPccombrk());
    if (config != null) {
      Tmct0Regla regla = config.getRegla(alo);
      if (regla != null) {
        desglose.setClearingRuleTo(regla.getIdRegla());
        desglose.setClearingRuleDescriptionTo(regla.getNbDescripcion());
      }
    }
    else {
      Tmct0Regla regla = reglaAsignacionBo.getRegla(alo);
      if (regla != null) {
        desglose.setClearingRuleTo(regla.getIdRegla());
        desglose.setClearingRuleDescriptionTo(regla.getNbDescripcion());
      }
    }

    desglose.setAccountAtClearer(alcBo.getAcctatcle(alo, sd));
    desglose.setBankReference(alcBo.getCdrefban(alo, sd));
    return desglose;
  }
}
