package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccSibbacBo;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccArreglarDesglosesRunnableBean")
public class CuadreEccArreglarDesglosesRunnableBean extends IRunnableBean<CuadreEccDbReaderRecordBeanDTO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CuadreEccArreglarDesglosesRunnableBean.class);

  @Value("${sibbac.cuadreecc.arreglar.desgloses.page.size:10000}")
  private int revisionTitulosCrearDesglosesPageSize;

  @Autowired
  private CuadreEccSibbacBo cuadreEccSibbacBo;

  public CuadreEccArreglarDesglosesRunnableBean() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<CuadreEccDbReaderRecordBeanDTO> beans, int order) throws SIBBACBusinessException {
    try {
      LOG.debug("[CuadreEccArreglarDesglosesRunnableBean :: run] inicio");
      cuadreEccSibbacBo.arreglarDesglosesCuadreEcc(beans, revisionTitulosCrearDesglosesPageSize);
      LOG.debug("[CuadreEccArreglarDesglosesRunnableBean :: run] fin");
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException(e);
    } finally {
      getObserver().sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<CuadreEccDbReaderRecordBeanDTO> clone() {
    return this;
  }

}
