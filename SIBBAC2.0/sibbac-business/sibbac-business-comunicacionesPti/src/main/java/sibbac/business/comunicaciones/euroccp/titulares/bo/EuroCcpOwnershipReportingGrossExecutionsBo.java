package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.common.EuroCcpErrorCodeEnum;
import sibbac.business.comunicaciones.euroccp.titulares.dao.EuroCcpOwnershipReportingGrossExecutionsDao;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReportingGrossExecutionsDTO;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpErrorCode;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReportingGrossExecutions;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class EuroCcpOwnershipReportingGrossExecutionsBo extends
    AbstractBo<EuroCcpOwnershipReportingGrossExecutions, Long, EuroCcpOwnershipReportingGrossExecutionsDao> {

  /**
   * A common logger.
   */
  private static final Logger LOG = LoggerFactory.getLogger(EuroCcpFileSentBo.class);

  @Autowired
  private EuroCcpFileSentBo euroCcpFileSentBo;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao envioRtDao;

  @Autowired
  private Tmct0enviotitularidadDao envioTiDao;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private EuroCcpOwnershipReferenceStaticDataBo euroCcpOwnershipReferenceStaticDataBo;

  public EuroCcpOwnershipReportingGrossExecutionsBo() {
  }

  /************************************************************* QUERYS *****************************************************************/

  public List<EuroCcpIdDTO> findAllIdDesglosecamaraBySinceDateAndEstadoProcesado(int estadoProcesado) {
    List<EuroCcpIdDTO> ids = new ArrayList<EuroCcpIdDTO>();

    List<Date> dates = dao.findAllDateByEstadoProcesado(estadoProcesado);
    for (Date date : dates) {
      ids.addAll(dao.findAllIdDesglosecamaraByTradeDateAndEstadoProcesado(date, estadoProcesado,
          EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId()));
    }
    return ids;
  }

  /************************************************************* QUERYS *****************************************************************/

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void procesarRecepcionTitulares(EuroCcpFileSent principalFile, List<EuroCcpCommonSendRegisterDTO> beans)
      throws SIBBACBusinessException {

    LOG.debug("[procesarRecepcionTitulares] inicio.");
    String msg;
    Tmct0alcId alcId;
    String accountNumber;
    String auditUser = "SIBBAC20";
    EuroCcpOwnershipReportingGrossExecutions ddbbReg;
    EuroCcpOwnershipReportingGrossExecutionsDTO bean;

    // String accountNumber;
    if (CollectionUtils.isNotEmpty(beans)) {

      for (EuroCcpCommonSendRegisterDTO commonBean : beans) {
        bean = (EuroCcpOwnershipReportingGrossExecutionsDTO) commonBean;
        LOG.debug("[procesarRecepcionTitulares] procesando respuesta : {}", bean);
        accountNumber = StringUtils.leftPad(bean.getAccountNumber(), 10, '0');
        LOG.debug("[procesarRecepcionTitulares] accountNumber: {} find accountNumber: {}", bean.getAccountNumber(),
            accountNumber);
        ddbbReg = Objects.requireNonNull(dao.findToReceiveTitularesProcess(bean.getTradeDate(),
            bean.getExecutionReference(), bean.getMic(), accountNumber, bean.getOwnerReferenceFrom(),
            bean.getOwnerReferenceTo(), bean.getNumberShares(), principalFile.getId()), MessageFormat.format(
            "INCIDENCIA-Registro no encontrado: {0}", bean));

        alcId = Objects.requireNonNull(ddbbReg.getDesgloseCamaraEnvioRt().getDesgloseCamara().getTmct0alc().getId(),
            MessageFormat.format("INCIDENCIA- Alc no encontrado {0}", bean));
        if (StringUtils.isNotBlank(bean.getErrorCode())
            && !EuroCcpErrorCodeEnum.NO_ERROR.text.equals(bean.getErrorCode())) {
          LOG.warn("[procesarRecepcionTitulares] errorCode: {} errorMessage: {}", bean.getErrorCode(),
              bean.getErrorMessage());
          ddbbReg.setErrorCode(new EuroCcpErrorCode(bean.getErrorCode()));
          msg = MessageFormat
              .format(
                  "EuroCCp-Recepcion-ERR-type:{0} from: {1} to: {2} Mic: {3} exec.Ref:{4} client:{5} cta:{6} titles:{7} err: {8}-{9}.",
                  principalFile.getFileType().getCdCodigo(), bean.getOwnerReferenceFrom(), bean.getOwnerReferenceTo(),
                  bean.getMic(), bean.getExecutionReference(), principalFile.getClientNumber(),
                  bean.getAccountNumber(), bean.getNumberShares(), bean.getErrorCode(), bean.getErrorMessage());
          LOG.warn("[procesarRecepcionTitulares] {}", msg);
          logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
          msg = MessageFormat.format("EuroCCp-Recepcion-ERR-Titular con error.exec.Ref:{0} {1}-{2}",
              bean.getExecutionReference(), bean.getErrorCode(), bean.getErrorMessage());
          LOG.debug("[procesarRecepcionTitulares] {}", msg);
          logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
        }
        else {
          ddbbReg.setErrorCode(new EuroCcpErrorCode(EuroCcpErrorCodeEnum.NO_ERROR.text));
          msg = MessageFormat.format(
              "EuroCCp-Recepcion-type:{0} from: {1} to: {2} Mic: {3} exec.Ref:{4} client:{5} cta:{6} titles:{7}.",
              principalFile.getFileType().getCdCodigo(), bean.getOwnerReferenceFrom(), bean.getOwnerReferenceTo(),
              bean.getMic(), bean.getExecutionReference(), principalFile.getClientNumber(), bean.getAccountNumber(),
              bean.getNumberShares());
          LOG.debug("[procesarRecepcionTitulares] {}", msg);
          logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
        }
        ddbbReg.setProcessingStatus(bean.getProcessingStatus());
        ddbbReg.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.RECIBIENDO.getId());
        ddbbReg.setAuditDate(new Date());
        ddbbReg.setAuditUser(auditUser);
        ddbbReg = save(ddbbReg);
      }
    }
    LOG.debug("[procesarRecepcionTitulares] final.");

  }

  /**
   * 
   * TODO falta por cambiar el estado de los registros ors y que este cambio sea
   * sycronized
   * 
   * @param beans
   * @throws SIBBACBusinessException
   * @throws NullPointerException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void revisarEstadosTitulares(List<EuroCcpIdDTO> beans) throws SIBBACBusinessException {
    String msg;
    Tmct0alcId alcId;
    Tmct0DesgloseCamaraEnvioRt rt;
    String auditUser = "SIBBAC20";
    List<EuroCcpOwnershipReportingGrossExecutions> orgs;

    for (EuroCcpIdDTO bean : beans) {
      LOG.trace("[revisarEstadosTitulares] {}.", bean);

      LOG.trace("[revisarEstadosTitulares] buscando orgs...");
      orgs = dao.findAllByIdDesglosecamraAndEstadoProcesado(bean.getId(),
          EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.RECIBIENDO.getId());

      if (CollectionUtils.isNotEmpty(orgs)) {

        for (EuroCcpOwnershipReportingGrossExecutions org : orgs) {
          rt = Objects.requireNonNull(org.getDesgloseCamaraEnvioRt(),
              MessageFormat.format("INCIDENCIA- Tabla rt no encontrada:{0}-{1}.", bean, org));

          alcId = Objects.requireNonNull(rt.getDesgloseCamara().getTmct0alc().getId(),
              MessageFormat.format("INCIDENCIA- Tabla alc no encontrada:{0}.", rt));
          try {
            if (org.getErrorCode() != null && org.getErrorCode().getErrorCode() != null
                && !org.getErrorCode().getErrorCode().equals(EuroCcpErrorCodeEnum.NO_ERROR.text)) {
              LOG.warn("[revisarEstadosTitulares] Encontrado org con error:{}.", org.getErrorCode());
              rt.setEstado('R');
              rt.setAudit_fecha_cambio(new Date());
              rt.setAudit_user(auditUser);
              rt = envioRtDao.save(rt);

              msg = MessageFormat
                  .format(
                      "EuroCcp-RevEstadoOrgs-ORG-CRG-RT-ERR-Reception Ownership Reporting of Gross executions Err:{4} Ref.Tit:{0}, Mic:{1}, Crt.Tit:{2}, Cli.Num:{3} rejected.",
                      rt.getReferenciaTitular(), rt.getCdPlataformaNegociacion(), rt.getCriterioComunicacionTitular(),
                      rt.getCdmiembromkt(), org.getErrorCode().getErrorMessage());
              LOG.warn("[revisarEstadosTitulares] - {}", msg);
              logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

              if (org.getErrorCode() != null && org.getErrorCode().getErrorCode() != null
                  && org.getErrorCode().getErrorCode().trim().equals(EuroCcpErrorCodeEnum.UNKNOWN_OWNER_REFERENCE.text)) {
                LOG.warn("[procesarRecepcionTitulares] el fichero ORS no se ha enviado o contenia errores...");
                if (rt.getEnvioTitularidad().getEstado() != 'R') {
                  euroCcpOwnershipReferenceStaticDataBo.rechazarDatosEstaticosTiOrsNewTransaction(alcId,
                      rt.getFecontratacion(), rt.getCdmiembromkt(), rt.getDesgloseCamara().getTmct0CamaraCompensacion()
                          .getCdCodigo(), org.getOwnerReferenceFrom(), org.getOwnerReferenceTo(), auditUser);
                }

              }
            }
            else {

              if (rt.getEstado() == 'E') {
                rt.setEstado('A');
                rt.setAudit_fecha_cambio(new Date());
                rt.setAudit_user(auditUser);
                rt = envioRtDao.save(rt);
                msg = MessageFormat
                    .format(
                        "EuroCcp-RevEstadoOrgs-ORG-CRG-RT-Reception Ownership Reporting of Gross executions Err:{4} Ref.Tit:{0}, Mic:{1}, Crt.Tit:{2}, Cli.Num:{3} accepted.",
                        rt.getReferenciaTitular(), rt.getCdPlataformaNegociacion(),
                        rt.getCriterioComunicacionTitular(), rt.getCdmiembromkt(), org.getErrorCode().getErrorMessage());
                LOG.debug("[revisarEstadosTitulares] - {}", msg);
                logBo
                    .insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
              }
              else {
                msg = MessageFormat
                    .format(
                        "EuroCcp-RevEstadoOrgs-ORG-CRG-RT-ERR-Reception Ownership Reporting of Gross executions Err:Estado Invalido RT Ref.Tit:{0}, Mic:{1}, Crt.Tit:{2}, Cli.Num:{3} accepted.",
                        rt.getReferenciaTitular(), rt.getCdPlataformaNegociacion(), rt.getCriterioComunicacionTitular());
                LOG.debug("[revisarEstadosTitulares] - {}", msg);
                logBo
                    .insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

              }

              if (rt.getEnvioTitularidad().getEstado() != 'A') {
                euroCcpOwnershipReferenceStaticDataBo.aceptaDatosEstaticosTiOrsNewTransaction(alcId,
                    rt.getFecontratacion(), rt.getCdmiembromkt(), rt.getDesgloseCamara().getTmct0CamaraCompensacion()
                        .getCdCodigo(), org.getOwnerReferenceFrom(), org.getOwnerReferenceTo(), auditUser);
              }
            }
            org.setEstadoProcesado(EstadosEnumerados.ESTADO_PROCESADO_EURO_CCP.FICHERO_PROCESADO.getId());
            org.setAuditDate(new Date());
            org.setAuditUser(auditUser);
            org = save(org);
          }
          catch (Exception e) {
            logBo.insertarRegistroNewTransaction(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
                e.getMessage(), "", auditUser);
            throw new SIBBACBusinessException(MessageFormat.format("INCIDENCIA-Procesando alc: {0}", alcId), e);
          }
        }// fin for

      }
    }// fin for beans
  }

}
