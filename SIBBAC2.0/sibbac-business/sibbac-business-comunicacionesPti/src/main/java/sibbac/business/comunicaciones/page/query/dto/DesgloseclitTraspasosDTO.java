package sibbac.business.comunicaciones.page.query.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DesgloseclitTraspasosDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -5882084697540897380L;
  private Date fechaContratacion, fechaLiquidacion;
  private String cdoperacionecc, nuoperacionprev, nuoperacioninic, ordenMercado, ejecucionMercado;
  private char sentido;
  private String cdisin, descripcionIsin;
  private String camaraCompensacion, cuenta, miembroCompensador, nombreMiembroCompensador, referenciaAsignacionExterna;
  private BigDecimal titulos, precio, efectivo;
  private String nuorden, nbooking, nucnfclt;
  private Short nucnfliq;
  private Long iddesglosecamara, nudesglose;

  public DesgloseclitTraspasosDTO() {
  }

  public DesgloseclitTraspasosDTO(Date fechaContratacion, Date fechaLiquidacion, String cdoperacionecc,
      String nuoperacionprev, String nuoperacioninic, String ordenMercado, String ejecucionMercado, char sentido,
      String cdisin, String descripcionIsin, String camaraCompensacion, String cuenta, String miembroCompensador,
      String nombreMiembroCompensador, String referenciaAsignacionExterna, BigDecimal titulos, BigDecimal precio,
      BigDecimal efectivo, String nuorden, String nbooking, String nucnfclt, Short nucnfliq, Long iddesglosecamara,
      Long nudesglose) {
    super();
    this.fechaContratacion = fechaContratacion;
    this.fechaLiquidacion = fechaLiquidacion;
    this.cdoperacionecc = cdoperacionecc;
    this.nuoperacionprev = nuoperacionprev;
    this.nuoperacioninic = nuoperacioninic;
    this.ordenMercado = ordenMercado;
    this.ejecucionMercado = ejecucionMercado;
    this.sentido = sentido;
    this.cdisin = cdisin;
    this.descripcionIsin = descripcionIsin;
    this.camaraCompensacion = camaraCompensacion;
    this.cuenta = cuenta;
    this.miembroCompensador = miembroCompensador;
    this.nombreMiembroCompensador = nombreMiembroCompensador;
    this.referenciaAsignacionExterna = referenciaAsignacionExterna;
    this.titulos = titulos;
    this.precio = precio;
    this.efectivo = efectivo;
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.iddesglosecamara = iddesglosecamara;
    this.nudesglose = nudesglose;
  }

  public Date getFechaContratacion() {
    return fechaContratacion;
  }

  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public String getNuoperacionprev() {
    return nuoperacionprev;
  }

  public String getNuoperacioninic() {
    return nuoperacioninic;
  }

  public String getOrdenMercado() {
    return ordenMercado;
  }

  public String getEjecucionMercado() {
    return ejecucionMercado;
  }

  public char getSentido() {
    return sentido;
  }

  public String getCdisin() {
    return cdisin;
  }

  public String getDescripcionIsin() {
    return descripcionIsin;
  }

  public String getCamaraCompensacion() {
    return camaraCompensacion;
  }

  public String getCuenta() {
    return cuenta;
  }

  public String getMiembroCompensador() {
    return miembroCompensador;
  }

  public String getNombreMiembroCompensador() {
    return nombreMiembroCompensador;
  }

  public String getReferenciaAsignacionExterna() {
    return referenciaAsignacionExterna;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public BigDecimal getEfectivo() {
    return efectivo;
  }

  public void setFechaContratacion(Date fechaContratacion) {
    this.fechaContratacion = fechaContratacion;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public void setNuoperacionprev(String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  public void setNuoperacioninic(String nuoperacioninic) {
    this.nuoperacioninic = nuoperacioninic;
  }

  public void setOrdenMercado(String ordenMercado) {
    this.ordenMercado = ordenMercado;
  }

  public void setEjecucionMercado(String ejecucionMercado) {
    this.ejecucionMercado = ejecucionMercado;
  }

  public void setSentido(char sentido) {
    this.sentido = sentido;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public void setDescripcionIsin(String descripcionIsin) {
    this.descripcionIsin = descripcionIsin;
  }

  public void setCamaraCompensacion(String camaraCompensacion) {
    this.camaraCompensacion = camaraCompensacion;
  }

  public void setCuenta(String cuenta) {
    this.cuenta = cuenta;
  }

  public void setMiembroCompensador(String miembroCompensador) {
    this.miembroCompensador = miembroCompensador;
  }

  public void setNombreMiembroCompensador(String nombreMiembroCompensador) {
    this.nombreMiembroCompensador = nombreMiembroCompensador;
  }

  public void setReferenciaAsignacionExterna(String referenciaAsignacionExterna) {
    this.referenciaAsignacionExterna = referenciaAsignacionExterna;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public Short getNucnfliq() {
    return nucnfliq;
  }

  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  public Long getNudesglose() {
    return nudesglose;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

}
