package sibbac.business.comunicaciones.euroccp.titulares.mapper;

import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpCommonSendRegisterDTO;
import sibbac.business.comunicaciones.euroccp.titulares.dto.EuroCcpOwnershipReportingGrossExecutionsDTO;
import sibbac.business.comunicaciones.euroccp.titulares.enums.EuroCcpOwnershipReportingGrossExecutionsFields;
import sibbac.business.wrappers.tasks.thread.WrapperAbstractFieldSetMapper;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;

public class EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper extends
    WrapperAbstractFieldSetMapper<EuroCcpCommonSendRegisterDTO> {

  public EuroCcpOwnershipReportingGrossExecutionsFieldSetMapper() {
  }

  @Override
  public WrapperFileReaderFieldEnumInterface[] getFields() {
    return EuroCcpOwnershipReportingGrossExecutionsFields.values();
  }

  @Override
  public EuroCcpCommonSendRegisterDTO getNewInstance() {
    return new EuroCcpOwnershipReportingGrossExecutionsDTO();
  }

  @Override
  public LineAggregator<EuroCcpCommonSendRegisterDTO> getLineAggregator() {
    FormatterLineAggregator<EuroCcpCommonSendRegisterDTO> lineAggregator = new EuroCcpOwnershipReportingGrossExecutionsSendLineAggregator<EuroCcpCommonSendRegisterDTO>(
        this);

    BeanWrapperFieldExtractor<EuroCcpCommonSendRegisterDTO> fieldExtractor = new BeanWrapperFieldExtractor<EuroCcpCommonSendRegisterDTO>();
    fieldExtractor.setNames(getFieldNames());
    lineAggregator.setFormat(getFormat());
    lineAggregator.setFieldExtractor(fieldExtractor);
    return lineAggregator;
  }

}
