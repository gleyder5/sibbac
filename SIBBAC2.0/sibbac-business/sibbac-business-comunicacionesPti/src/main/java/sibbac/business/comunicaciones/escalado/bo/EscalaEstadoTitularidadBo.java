/**
 * 
 */
package sibbac.business.comunicaciones.escalado.bo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TITULARIDADES;

/**
 * Escala el estado de asignación desde los desgloses cámara hasta su booking si
 * cumple las condiciones.
 * 
 * @author XIS16630
 * @see EscalaEstados
 */
@Service
public class EscalaEstadoTitularidadBo extends AbstractEscalaEstados {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public EscalaEstadoTitularidadBo() {
  }

  /** Nombre del campo de base de datos que almacena el estado de titularidad. */
  private static final String NOMBRE_CAMPO_ESTADO_DB = "CDESTADOTIT";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${sibbac20.script.dividen}")
  private Boolean sibbac20ScriptDividen;

  @Value("${daemons.escalado.transaction.size}")
  private Integer transactionSize;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoIncidencias
   * ()
   */
  // @Override
  public Integer getEstadoIncidencias() {
    return ASIGNACIONES.INCIDENCIAS.getId();
  } // getEstadoIncidencias

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoRechazado()
   */
  // @Override
  public Integer getEstadoRechazado() {
    return TITULARIDADES.RECHAZADA.getId();
  } // getEstadoRechazado

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoEnCurso()
   */
  // @Override
  public Integer getEstadoEnCurso() {
    return TITULARIDADES.EN_CURSO.getId();
  } // getEstadoEnCurso

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getEstadoTratandose()
   */
  // @Override
  public Integer getEstadoTratandose() {
    return TITULARIDADES.TRATANDOSE.getId();
  } // getEstadoTratandose

  /*
   * @see
   * sibbac.business.daemons.escalado.common.EscalaEstados#getNombreCampoEstado
   * ()
   */
  // @Override
  public String getNombreCampoEstado() {
    return NOMBRE_CAMPO_ESTADO_DB;
  } // getNombreCampoEstado

  @Override
  public int getThreadSize() {
    return 25;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

} // EscalaEstadoTitularidadBo
