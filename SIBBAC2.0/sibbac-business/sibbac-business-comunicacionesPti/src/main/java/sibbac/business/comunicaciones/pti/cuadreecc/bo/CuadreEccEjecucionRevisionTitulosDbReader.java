package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccEjecucionRevisionTitulosRunnableBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccEjecucionRevisionTitulosDbReader")
public class CuadreEccEjecucionRevisionTitulosDbReader extends WrapperMultiThreadedDbReader<CuadreEccIdDTO> {

  protected static final Logger LOG = LoggerFactory.getLogger(CuadreEccEjecucionRevisionTitulosDbReader.class);

  @Value("${sibbac.cuadreecc.max.reg.process.in.execution:100000}")
  private int maxRegToProcess;

  @Value("${sibbac.cuadreecc.revision.titulos.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadreecc.revision.titulos.thread.pool.size:25}")
  private int threadSize;

  @Autowired
  private CuadreEccEjecucionBo cuadreEccEjecucionBo;

  @Qualifier(value = "cuadreEccEjecucionRevisionTitulosRunnableBean")
  @Autowired
  private CuadreEccEjecucionRevisionTitulosRunnableBean cuadreEccRevTitulosRunnBean;

  public CuadreEccEjecucionRevisionTitulosDbReader() {
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<CuadreEccIdDTO> getBeanToProcessBlock() {
    return cuadreEccRevTitulosRunnBean;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.info("[CuadreEccEjecucionRevisionTitulosDbReader :: preExecuteTask] inicio");
    LOG.info("[CuadreEccEjecucionRevisionTitulosDbReader :: preExecuteTask] buscando ejecuciones...");
    List<CuadreEccIdDTO> listBean = cuadreEccEjecucionBo
        .findIdentificacionOperacionCuadreEccDTOForRevisionTitulos(maxRegToProcess);
    LOG.info("[CuadreEccEjecucionRevisionTitulosDbReader :: preExecuteTask] encontrados {} ejecuciones que revisar.",
        listBean.size());
    beanIterator = listBean.iterator();
    LOG.info("[CuadreEccEjecucionRevisionTitulosDbReader :: preExecuteTask] fin");
  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
