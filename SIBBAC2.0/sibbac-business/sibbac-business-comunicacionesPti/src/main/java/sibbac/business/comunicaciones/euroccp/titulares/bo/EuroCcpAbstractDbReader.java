package sibbac.business.comunicaciones.euroccp.titulares.bo;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.comunicaciones.euroccp.common.EuroCcpRangoFechasHorasInicioProcesosDTO;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;

public abstract class EuroCcpAbstractDbReader<T extends RecordBean> extends WrapperMultiThreadedDbReader<T> {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(EuroCcpAbstractDbReader.class);

  public EuroCcpAbstractDbReader() {
  }

  protected EuroCcpRangoFechasHorasInicioProcesosDTO getConfigHorasCorteEnvioTitulares() throws SIBBACBusinessException {
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(getTimeZone());

    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(cal.getTime(),
                                                                                                   FormatDataUtils.TIME_FORMAT));
    EuroCcpRangoFechasHorasInicioProcesosDTO fechaHoraCorteProcesos = new EuroCcpRangoFechasHorasInicioProcesosDTO();

    fechaHoraCorteProcesos.setInitSendHourFirstDate(FormatDataUtils.convertStringToDate(getInitSendHourFirstDate(),
                                                                                        FormatDataUtils.TIME_FORMAT));
    fechaHoraCorteProcesos.setdDaysFirstSendDate(getdDaysFirstSendDate());
    fechaHoraCorteProcesos.setEndSendHourLastDate(FormatDataUtils.convertStringToDate(getEndSendHourLastDate(),
                                                                                      FormatDataUtils.TIME_FORMAT));
    fechaHoraCorteProcesos.setdDaysLastDate(getdDaysLastDate());

    // hay que acercarse al cero
    if (now.compareTo(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(fechaHoraCorteProcesos.getInitSendHourFirstDate(),
                                                                                                    FormatDataUtils.TIME_FORMAT))) < 0) {
      fechaHoraCorteProcesos.setdDaysFirstSendDate(fechaHoraCorteProcesos.getdDaysFirstSendDate() + 1);
    }

    // hay que alejarse del cero
    if (now.compareTo(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(fechaHoraCorteProcesos.getEndSendHourLastDate(),
                                                                                                    FormatDataUtils.TIME_FORMAT))) > 0) {
      fechaHoraCorteProcesos.setdDaysLastDate(fechaHoraCorteProcesos.getdDaysLastDate() - 1);

    }
    LOG.info("[EuroCcpAbstractDbReader :: getConfigHorasCorteEnvioTitulares] config: {}", fechaHoraCorteProcesos);
    return fechaHoraCorteProcesos;
  }

  protected Date getFechaLiquidacion(int moveDays, List<Integer> workDaysOfWeek, List<Date> holidays) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(getTimeZone());
    int myMoveDays = moveDays * -1;
    if (myMoveDays < 0) {
      cal = DateHelper.getPreviousWorkDate(cal, myMoveDays * -1, workDaysOfWeek, holidays);
    } else {
      cal = DateHelper.getNextWorkDate(cal, myMoveDays, workDaysOfWeek, holidays);
    }
    Date lastDate = cal.getTime();
    return lastDate;
  }

  public abstract int getdDaysFirstSendDate();

  public abstract String getInitSendHourFirstDate();

  public abstract int getdDaysLastDate();

  public abstract String getEndSendHourLastDate();

  public abstract TimeZone getTimeZone();

}
