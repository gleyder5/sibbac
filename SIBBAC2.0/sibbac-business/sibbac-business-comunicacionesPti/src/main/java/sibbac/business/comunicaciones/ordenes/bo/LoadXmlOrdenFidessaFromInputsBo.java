package sibbac.business.comunicaciones.ordenes.bo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.common.RunnableProcess;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessagesBo;
import sibbac.common.utils.FormatDataUtils;

@Service
public class LoadXmlOrdenFidessaFromInputsBo extends WrapperExecutor {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(LoadXmlOrdenFidessaFromInputsBo.class);

  private static final String INICIO_XML = "<multicenter:Orders";
  private static final String FINAL_XML = "</multicenter:Orders>";
  private static final String CABECERA_XML = "<?xml version=\"1.0\" encoding=\"%s\" standalone=\"yes\"?>";

  @Value("${sibbac.fidessa.xml.in:/sbrmv/ficheros/fidessa/xml/in}")
  private String pathXmlInSt;
  @Value("${sibbac.fidessa.xml.in.tratados:/sbrmv/ficheros/fidessa/xml/in/tratados}")
  private String pathXmlInTratadosSt;

  @Value("${sibbac.fidessa.xml.in.errores:/sbrmv/ficheros/fidessa/xml/in/errores}")
  private String pathXmlInErrSt;

  @Value("${sibbac.fidessa.xml.in.tmp:/sbrmv/ficheros/fidessa/xml/in/tmp}")
  private String pathXmlInTmpSt;

  @Value("${sibbac.fidessa.xml.in.charset:UTF8}")
  private Charset xmlCharset;

  @Value("${sibbac.fidessa.xml.in.transaction.size:1}")
  private Integer transactionSize;

  @Value("${sibbac.fidessa.xml.in.threadpool.size:25}")
  private Integer threadPoolSize;

  @Value("${sibbac.fidessa.xml.in.read.mq.timeout:250}")
  private Integer readMqTimeOut;

  @Value("${sibbac.fidessa.log.charset:UTF8}")
  private Charset logCharset;

  @Value("${sibbac.fidessa.xml.in.pattern:{*.xml.gz,*.xml}}")
  private String xmlInFilePattern;

  @Value("${sibbac.fidessa.xml.log.in.pattern:{*.log.zip,*.log,*.log.gz}}")
  private String xmlLogFilePattern;

  @Value("${sibbac.fidessa.zip.in.pattern:*.xmls.zip}")
  private String xmlZipMultipleFilePattern;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsMessagesBo;

  @Value("${jms.xml.fidessa.qmanager.name}")
  private String fidessaXmlJndiName;

  @Value("${jms.xml.fidessa.in.queue}")
  private String fidessaXmlIn;

  @Value("${jms.xml.fidessa.in.sd.queue}")
  private String fidessaXmlInSd;

  @Value("${jms.xml.fidessa.out.queue}")
  private String fidessaXmlOut;

  @Value("${jms.xml.fidessa.qmanager.host}")
  private String fidessaMqHost;

  @Value("${jms.xml.fidessa.qmanager.port}")
  private int fidessaMqPort;

  @Value("${jms.xml.fidessa.qmanager.channel}")
  private String fidessaMqChannel;

  @Value("${jms.xml.fidessa.qmanager.queuemanager}")
  private String fidessaMqManagerName;

  @Autowired
  private Tmct0cfgBo configBo;

  @Autowired
  private LoadXmlOrdenFidessaFromInputsSubListBo loadXmlSubList;

  public LoadXmlOrdenFidessaFromInputsBo() {
  }

  /**
   * @throws Exception
   */
  public void executeTask() throws Exception {
    setObserver(new ObserverProcess());
    Calendar now = Calendar.getInstance();
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: executeTask] inicio.");
    if (StringUtils.isNotBlank(fidessaXmlOut)) {
      Path pathXmlIn = Paths.get(pathXmlInSt);
      Path pathXmlInTratados = Paths.get(pathXmlInTratadosSt);
      Path pathXmlInErrores = Paths.get(pathXmlInErrSt);
      Path pathXmlInTmp = Paths.get(pathXmlInTmpSt);

      if (Files.notExists(pathXmlIn)) {
        Files.createDirectories(pathXmlIn);
      }
      pathXmlInTratados = getPathCarpetasAnioMesDia(pathXmlInTratados);
      if (Files.notExists(pathXmlInTratados)) {
        Files.createDirectories(pathXmlInTratados);
      }

      pathXmlInErrores = getPathCarpetasAnioMesDia(pathXmlInErrores);
      if (Files.notExists(pathXmlInErrores)) {
        Files.createDirectories(pathXmlInErrores);
      }

      if (Files.notExists(pathXmlInTmp)) {
        Files.createDirectories(pathXmlInTmp);
      }
      boolean backupXmlsFs = now.get(Calendar.HOUR_OF_DAY) >= 19;

      if (backupXmlsFs) {
        LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: executeTask] backup ficheros.");
        this.comprimirDiasAnteriores(Paths.get(pathXmlInTratadosSt), pathXmlInTratados, false);
        this.comprimirDiasAnteriores(Paths.get(pathXmlInErrSt), pathXmlInErrores, false);
        LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: executeTask] fin backup ficheros.");

      }

      this.parsearFicherosLogsAndComprimidos(pathXmlIn, pathXmlInTratados, pathXmlInErrores, pathXmlInTmp);

      boolean isLoadInternacional = configBo.isLoadXmlInternacional();
      String aliasScriptDividend = configBo.getAliasScriptDividend();
      String tpopbolSinEcc = configBo.getTpopebolsSinEcc();

      CachedValuesLoadXmlOrdenDTO cachedConfig = new CachedValuesLoadXmlOrdenDTO(false, isLoadInternacional,
          aliasScriptDividend, tpopbolSinEcc);
      cachedConfig.setScriptDividendInstance(false);
      try {
        this.procesarXmlsFromFs(pathXmlIn, pathXmlInTratados, pathXmlInErrores, cachedConfig);

        if (StringUtils.isNotBlank(fidessaXmlIn)) {
          this.procesarXmlsFromMq(fidessaXmlIn, cachedConfig);
        }
        if (StringUtils.isNotBlank(fidessaXmlInSd)) {
          cachedConfig.setScriptDividendInstance(true);
          this.procesarXmlsFromMq(fidessaXmlInSd, cachedConfig);
        }
      }
      catch (Exception e) {
        throw e;
      }
      finally {
        cachedConfig.clearCache();
      }
    }
    else {
      LOG.warn("[LoadXmlOrdenFidessaFromInputsBo :: executeTask] No esta configurada la cola de rechazos a fidessa, no leemos.");
    }
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: executeTask] fin.");
  }

  private void comprimirDiasAnteriores(Path path, Path currentPath, boolean isFile) throws IOException {
    if (!path.toString().equals(currentPath.toString())) {
      if (!isFile) {
        int countFolders = 0;
        Collection<Path> toCompress = new HashSet<Path>();
        try (DirectoryStream<Path> dirs = Files.newDirectoryStream(path)) {
          for (Path path2 : dirs) {
            if (Files.isDirectory(path2)) {
              countFolders++;
              comprimirDiasAnteriores(path2, currentPath, false);
            }
            else {
              toCompress.add(path2.getParent());
            }
          }
        }
        if (countFolders == 0) {
          for (Path path2 : toCompress) {
            comprimirDiasAnteriores(path2, currentPath, true);
          }
        }
        if (CollectionUtils.isEmpty(toCompress) && countFolders == 0) {
          Files.deleteIfExists(path);
        }
        toCompress.clear();
      }
      else {
        boolean containsSubFolders = false;

        boolean added = false;

        Date modifiedDate = new Date(Files.getLastModifiedTime(path).toMillis());
        Path output = Paths.get(
            path.getParent().toString(),
            String.format("%s_modify_%s_zippded_%s.zip", path.getFileName().toString(),
                FormatDataUtils.convertDateToString(modifiedDate), FormatDataUtils.convertDateToString(new Date())));

        LOG.debug("[comprimirDiasAnteriores] abriendo fichero: {}", output.toString());
        try (ZipOutputStream zos = new ZipOutputStream(Files.newOutputStream(output, StandardOpenOption.CREATE_NEW,
            StandardOpenOption.WRITE))) {
          try (DirectoryStream<Path> dir = Files.newDirectoryStream(path)) {
            for (Path file : dir) {
              if (!Files.isDirectory(file)) {
                ZipEntry ze = new ZipEntry(file.getFileName().toString());
                zos.putNextEntry(ze);
                LOG.debug("[comprimirDiasAnteriores] aniadiendo fichero: {}", file.toString());
                try (InputStream is = Files.newInputStream(file, StandardOpenOption.READ)) {
                  IOUtils.copy(is, zos);
                  added = true;
                }
                zos.closeEntry();
                Files.deleteIfExists(file);
              }// fin zip
              else {
                containsSubFolders = true;
              }

            }
          }
        }// fin zip ouput

        if (!added) {
          Files.deleteIfExists(output);
        }
        if (!containsSubFolders) {
          Files.deleteIfExists(path);
        }
      }
    }
  }

  /**
   * @param path
   * @return
   */
  private Path getPathCarpetasAnioMesDia(Path path) {
    Calendar fechaActual = Calendar.getInstance();
    int anio = fechaActual.get(Calendar.YEAR);
    int mes = fechaActual.get(Calendar.MONTH) + 1;
    int dia = fechaActual.get(Calendar.DAY_OF_MONTH);
    Path pathRes = Paths.get(path.toString(), String.format("%04d", anio), String.format("%02d", mes),
        String.format("%02d", dia));

    return pathRes;
  }

  /**
   * @param pathXmlIn
   * @param pathTratados
   * @param pathErr
   * @param tmpPath
   * @throws IOException
   */
  private void parsearFicherosLogsAndComprimidos(final Path pathXmlIn, final Path pathTratados, final Path pathErr,
      final Path tmpPath) throws IOException {
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] inicio.");

    this.extraerXmlZip(pathXmlIn, pathTratados, pathErr, tmpPath);

    this.extraerXmlFromLogComprimido(pathXmlIn, pathTratados, pathErr, tmpPath);

    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] fin.");
  }

  /**
   * @param pathXmlIn
   * @param pathTratados
   * @param pathErr
   * @param tmpPath
   * @throws IOException
   */
  private void extraerXmlZip(final Path pathXmlIn, final Path pathTratados, final Path pathErr, final Path tmpPath)
      throws IOException {

    List<Path> descomprimidos;
    String nombreOriginal;
    ZipEntry ze;
    Path output;
    byte[] buffer = new byte[1024];
    // *.log.zip,*.log,*.log.gz
    try (DirectoryStream<Path> dir = Files.newDirectoryStream(pathXmlIn, xmlZipMultipleFilePattern)) {
      for (Path zipMultipleXmls : dir) {
        LOG.debug(
            "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] encontrado fichero zip multiple: {}",
            zipMultipleXmls.toString());

        if (zipMultipleXmls.getFileName().toString().toUpperCase().endsWith(".ZIP")) {
          nombreOriginal = zipMultipleXmls.getFileName().toString();
          output = Paths.get(pathXmlIn.toString(),
              String.format("%s%s", FilenameUtils.getBaseName(nombreOriginal), ".procesando"));
          Files.move(zipMultipleXmls, output, StandardCopyOption.ATOMIC_MOVE);
          zipMultipleXmls = output;
          descomprimidos = new ArrayList<Path>();
          try {

            try (ZipInputStream zis = new ZipInputStream(
                Files.newInputStream(zipMultipleXmls, StandardOpenOption.READ), logCharset)) {
              while ((ze = zis.getNextEntry()) != null) {
                try {
                  LOG.debug(
                      "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] descomprimiendo zip {} fichero {}",
                      zipMultipleXmls.toAbsolutePath(), ze.getName());
                  if (ze.getName().toUpperCase().endsWith(".ZIP")) {
                    throw new SIBBACBusinessException(String.format(
                        "INCIDENCIA- no se permite cargar un fichero zip con zips dentro: %s-%s",
                        zipMultipleXmls.toString(), ze.getName()));
                  }
                  else if (ze.getName().toUpperCase().endsWith(".GZ")) {
                    output = Paths.get(pathXmlIn.toString(), ze.getName());
                    LOG.debug(
                        "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] creando fichero out: {}",
                        output.toString());
                    try (OutputStream os = Files.newOutputStream(output, StandardOpenOption.CREATE_NEW,
                        StandardOpenOption.WRITE);) {
                      IOUtils.copyLarge(zis, os, buffer);

                    }
                  }
                  else {
                    output = Paths.get(pathXmlIn.toString(), String.format("%s%s", ze.getName(), ".gz"));
                    LOG.debug(
                        "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] creando fichero out: {}",
                        output.toString());
                    try (OutputStream os = Files.newOutputStream(output, StandardOpenOption.CREATE_NEW,
                        StandardOpenOption.WRITE); GZIPOutputStream gos = new GZIPOutputStream(os);) {
                      IOUtils.copyLarge(zis, gos, buffer);

                    }
                  }

                  descomprimidos.add(output);
                }
                catch (Exception e) {
                  throw e;
                }
                finally {
                  zis.closeEntry();
                }
              }

            }
            output = Paths
                .get(
                    pathTratados.toString(),
                    String.format("%s_%s%s%s", nombreOriginal,
                        FormatDataUtils.convertDateToConcurrentFileName(new Date())), ".",
                    FilenameUtils.getExtension(nombreOriginal));
            LOG.debug(
                "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] moviendo fichero de {} a {}",
                zipMultipleXmls.toString(), output.toString());
            Files.move(zipMultipleXmls, output, StandardCopyOption.ATOMIC_MOVE);
          }
          catch (Exception e) {
            LOG.warn(e.getMessage(), e);
            output = Paths.get(pathErr.toString(),
                String.format("%s_%s", nombreOriginal, FormatDataUtils.convertDateToConcurrentFileName(new Date())));
            LOG.debug(
                "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] moviendo fichero de {} a {}",
                zipMultipleXmls.toString(), output.toString());
            Files.move(zipMultipleXmls, output, StandardCopyOption.ATOMIC_MOVE);
            if (CollectionUtils.isNotEmpty(descomprimidos)) {
              for (Path err : descomprimidos) {
                LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos]borrando fichero: {}",
                    err.toString());
                Files.deleteIfExists(err);

              }
            }
          }

        }
        else {
          LOG.warn("INCIDENCIA FICHERO: {} no es un zip.", zipMultipleXmls);
        }
      }
    }// fin procesar ficheros zip multiples
  }

  private void extraerXmlFromLogComprimido(final Path pathXmlIn, final Path pathTratados, final Path pathErr,
      final Path tmpPath) throws IOException {
    String nombreOriginal;
    Path output;
    ZipEntry ze;
    try (DirectoryStream<Path> dir = Files.newDirectoryStream(pathXmlIn, xmlLogFilePattern)) {
      byte[] buffer = new byte[1024];
      for (Path log : dir) {
        nombreOriginal = log.getFileName().toString();
        try {
          LOG.debug(
              "[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] encontrado fichero log : {}",
              log.toString());

          output = Paths.get(pathXmlIn.toString(),
              String.format("%s%s", FilenameUtils.getBaseName(nombreOriginal), ".procesando"));
          Files.move(log, output, StandardCopyOption.ATOMIC_MOVE);
          log = output;
          if (nombreOriginal.toUpperCase().endsWith(".ZIP")) {
            try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(log, StandardOpenOption.READ), logCharset);
                InputStreamReader isr = new InputStreamReader(zis, logCharset);
                BufferedReader br = new BufferedReader(isr);) {
              while ((ze = zis.getNextEntry()) != null) {
                LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] zipentry: {}",
                    ze.toString());

                try {
                  this.extraerXmlsLog(nombreOriginal, br, pathXmlIn, tmpPath);
                }
                catch (Exception e) {
                  throw e;
                }
                finally {
                  zis.closeEntry();
                }
              }
            }
            output = Paths.get(pathTratados.toString(), String.format("%s_%s%s", nombreOriginal,
                FormatDataUtils.convertDateToConcurrentFileName(new Date()), ".zip"));
            Files.move(log, output, StandardCopyOption.ATOMIC_MOVE);
          }
          else if (nombreOriginal.toUpperCase().endsWith(".GZ")) {
            try (GZIPInputStream gis = new GZIPInputStream(Files.newInputStream(log, StandardOpenOption.READ));
                InputStreamReader isr = new InputStreamReader(gis, logCharset);
                BufferedReader br = new BufferedReader(isr);) {
              this.extraerXmlsLog(nombreOriginal, br, pathXmlIn, tmpPath);
            }
            output = Paths.get(pathTratados.toString(), String.format("%s_%s%s", nombreOriginal,
                FormatDataUtils.convertDateToConcurrentFileName(new Date()), ".gz"));
            Files.move(log, output, StandardCopyOption.ATOMIC_MOVE);
          }
          else {
            try (InputStream gis = Files.newInputStream(log, StandardOpenOption.READ);
                InputStreamReader isr = new InputStreamReader(gis, xmlCharset);
                BufferedReader br = new BufferedReader(isr);) {
              this.extraerXmlsLog(nombreOriginal, br, pathXmlIn, tmpPath);
            }

            output = Paths.get(pathTratados.toString(), String.format("%s_%s%s", nombreOriginal,
                FormatDataUtils.convertDateToConcurrentFileName(new Date()), ".gz"));
            try (InputStream is = Files.newInputStream(log, StandardOpenOption.READ);
                OutputStream os = Files.newOutputStream(output, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                GZIPOutputStream gos = new GZIPOutputStream(os);) {
              IOUtils.copyLarge(is, gos, buffer);
            }
            Files.delete(log);
          }

        }
        catch (Exception e) {
          output = Paths.get(pathErr.toString(),
              String.format("%s_%s", nombreOriginal, FormatDataUtils.convertDateToConcurrentFileName(new Date())));
          Files.move(log, output, StandardCopyOption.ATOMIC_MOVE);
          throw e;

        }
      }// fin for
    }
  }

  private void extraerXmlsLog(String nombreOriginal, BufferedReader br, Path pathXmlIn, Path tmpPath)
      throws IOException {

    String linea;
    int countOutput = 1;
    Path output = null;
    String fechaProcesamiento = FormatDataUtils.convertDateToConcurrentFileName(new Date());
    List<Path> creados = new ArrayList<Path>();

    try {
      while ((linea = br.readLine()) != null) {
        if (linea.contains(INICIO_XML)) {
          output = Paths.get(tmpPath.toString(), String.format("%s_%s_%s%s", FilenameUtils.getBaseName(nombreOriginal),
              fechaProcesamiento, countOutput++, ".gz"));
          LOG.debug("[extraerXmlsLog] creando fichero: {}", output.toString());

          this.extaerXmlFromLog(br, linea, output);
          if (Files.exists(output)) {
            creados.add(output);
          }
        }

      }
    }
    catch (Exception e) {
      for (Path path : creados) {
        Files.deleteIfExists(path);
      }
      throw e;
    }
    finally {

    }

    for (Path path : creados) {
      output = Paths.get(pathXmlIn.toString(),
          String.format("%s%s", FilenameUtils.getBaseName(path.getFileName().toString()), ".xml.gz"));
      LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: parsearFicherosLogsAndComprimidos] moviendo fichero de: {} a {} ",
          path.toString(), output.toString());
      Files.move(path, output, StandardCopyOption.ATOMIC_MOVE);
    }
  }

  private void extaerXmlFromLog(BufferedReader br, String firsLine, Path xml) throws IOException {
    if (StringUtils.isNotBlank(firsLine)) {
      boolean xmlCompleto = false;
      boolean isInicioXml = false;
      String linea = null;
      try (OutputStream os = Files.newOutputStream(xml, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
          GZIPOutputStream gos = new GZIPOutputStream(os);
          BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(gos, xmlCharset));) {
        bw.write(String.format(CABECERA_XML, xmlCharset));
        bw.newLine();
        bw.write(firsLine);
        bw.newLine();
        while ((linea = br.readLine()) != null) {
          isInicioXml = linea.contains(INICIO_XML);
          if (isInicioXml) {
            LOG.warn("[extaerXmlFromLog]INCIDENCIA-Encontrado inicio de xml sin el final del anterior. SE DESCARTA.");
            break;
          }
          else {
            bw.write(linea);
            bw.newLine();
            xmlCompleto = linea.contains(FINAL_XML);
            if (xmlCompleto) {
              break;
            }
          }

        }
      }

      if (!xmlCompleto) {
        Files.deleteIfExists(xml);
        if (isInicioXml) {
          extaerXmlFromLog(br, linea, xml);
        }
      }

    }
  }

  private void procesarXmlsFromFs(final Path pathXmlIn, final Path pathXmlInTratados, final Path pathXmlInErrores,
      final CachedValuesLoadXmlOrdenDTO cachedConfig) throws SIBBACBusinessException {
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromFs] inicio.");
    try {
      ConnectionFactory cf = jmsMessagesBo.getNewJmsConnectionFactory(fidessaMqHost, fidessaMqPort, fidessaMqChannel,
          fidessaMqManagerName, "", "");
      try (DirectoryStream<Path> dir = Files.newDirectoryStream(pathXmlIn, xmlInFilePattern)) {
        List<Path> subListPath = new ArrayList<Path>();
        try {
          for (Path path : dir) {
            LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromFs] encontrado fichero xml: {}",
                path.toString());
            if (subListPath.size() >= getTransactionSize()) {
              List<Path> subListToThread = new ArrayList<Path>(subListPath);
              LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromFs] lanzando hilo con {} paths.",
                  subListToThread.size());
              executeProcessInThread(getRunnableProcessLecturaFs(subListToThread, pathXmlIn, pathXmlInTratados,
                  pathXmlInErrores, cf, fidessaXmlOut, cachedConfig));
              if (getObserver().getException() != null) {
                throw getObserver().getException();
              }
              subListPath = new ArrayList<Path>();
            }
            else {
              subListPath.add(path);
            }

          }// fin for

          if (CollectionUtils.isNotEmpty(subListPath)) {
            List<Path> subListToThread = new ArrayList<Path>(subListPath);
            LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromFs] lanzando hilo con {} paths.",
                subListToThread.size());
            executeProcessInThread(getRunnableProcessLecturaFs(subListPath, pathXmlIn, pathXmlInTratados,
                pathXmlInErrores, cf, fidessaXmlOut, cachedConfig));
            if (getObserver().getException() != null) {
              throw getObserver().getException();
            }
          }
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
          closeExecutorNow();
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
        finally {
          closeExecutor();
        }

      }

    }
    catch (IOException | JMSException e) {
      LOG.debug(e.getMessage(), e);
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromFs] fin.");
  }

  private RunnableProcess<RecordBean> getRunnableProcessLecturaFs(final List<Path> subListToThread,
      final Path pathXmlIn, final Path pathXmlInTratados, final Path pathXmlInErrores,
      final ConnectionFactory cfReject, final String rejectQueue, final CachedValuesLoadXmlOrdenDTO cachedConfig) {
    RunnableProcess<RecordBean> process = new RunnableProcess<RecordBean>(getObserver()) {
      public void run() {
        boolean ok;
        String nombreOriginal;
        String baseName;
        String extension;
        Path pathDestino;
        try {
          try (Connection connection = jmsMessagesBo.getNewConnection(cfReject);) {
            connection.start();
            try (Session session = jmsMessagesBo.getNewSession(connection, true);
                MessageProducer messageProducer = jmsMessagesBo.getNewProducer(session, rejectQueue);) {

              for (Path path2 : subListToThread) {
                try {
                  LOG.debug("[TaskLoadXmlFidessaFromMQ :: procesarXmlsFromFs] {}", path2.toString());

                  nombreOriginal = path2.getFileName().toString();
                  baseName = FilenameUtils.getBaseName(nombreOriginal);
                  extension = FilenameUtils.getExtension(nombreOriginal);
                  pathDestino = Paths.get(pathXmlIn.toString(),
                      String.format("%s%s", FilenameUtils.getBaseName(nombreOriginal), ".procesando"));
                  path2 = Files.move(path2, pathDestino, StandardCopyOption.ATOMIC_MOVE);
                  try (InputStream is = Files.newInputStream(path2, StandardOpenOption.READ);) {
                    try {
                      if (nombreOriginal.toUpperCase().endsWith(".GZ")) {
                        ok = loadXmlSubList.processGz(session, messageProducer, is, cachedConfig);
                      }
                      else {
                        ok = loadXmlSubList.processIs(session, messageProducer, is, cachedConfig);
                      }
                      session.commit();
                    }
                    catch (Exception e) {
                      LOG.warn(e.getMessage(), e);
                      ok = false;
                      session.rollback();
                    }
                  }
                  if (ok) {
                    pathDestino = Paths.get(
                        pathXmlInTratados.toString(),
                        String.format("%s_%s%s%s", baseName,
                            FormatDataUtils.convertDateToConcurrentFileName(new Date()), ".", extension));
                    Files.move(path2, pathDestino, StandardCopyOption.ATOMIC_MOVE);
                  }
                  else {
                    pathDestino = Paths.get(
                        pathXmlInErrores.toString(),
                        String.format("%s_%s%s%s", baseName,
                            FormatDataUtils.convertDateToConcurrentFileName(new Date()), ".", extension));
                    Files.move(path2, pathDestino, StandardCopyOption.ATOMIC_MOVE);
                  }
                  LOG.debug("[TaskLoadXmlFidessaFromMQ :: procesarXmlsFromFs] moviendo fichero a destino {}",
                      pathDestino.toString());
                }
                catch (Exception e) {
                  throw e;
                }
              }
            }
          }// fin for
        }
        catch (Exception e) {
          getObserver().setException(e);
          LOG.warn(e.getMessage(), e);
        }
        finally {
          getObserver().sutDownThread(this);
        }
      };
    };
    return process;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void procesarXmlsFromMq(final String queueIn, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws SIBBACBusinessException {
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromMq] inicio. {}", queueIn);
    Integer countMsgEnCola;
    try {
      ConnectionFactory cf = null;
      try {
        cf = jmsMessagesBo.getConnectionFactoryFromContext(fidessaXmlJndiName);
      }
      catch (Exception e1) {
        LOG.warn(e1.getMessage());
        LOG.trace(e1.getMessage(), e1);
        cf = jmsMessagesBo.getNewJmsConnectionFactory(fidessaMqHost, fidessaMqPort, fidessaMqChannel,
            fidessaMqManagerName, "", "");
      }

      try (Connection connection = jmsMessagesBo.getNewConnection(cf);) {
        connection.start();
        try (Session session = jmsMessagesBo.getNewSession(connection, false);) {
          try {
            countMsgEnCola = jmsMessagesBo.countQueueMessages(session, queueIn);
            if (countMsgEnCola > 0) {
              LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromMq] la cola tiene {} msgs sin consumir.",
                  countMsgEnCola);
              for (int i = 0; i < countMsgEnCola; i++) {
                executeProcessInThread(this.getHiloConsumidorMq(cf, queueIn, fidessaXmlOut, cachedConfig));
                LOG.debug(
                    "[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromMq] lanzando hilo para consumir {} xmls.",
                    getTransactionSize());
                if (getObserver().getException() != null) {
                  throw getObserver().getException();
                }
              }

              LOG.debug(
                  "[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromMq] Se han conumido los mensajes {} que habia en la cola {}.",
                  countMsgEnCola, queueIn);
            }
            else {
              LOG.debug(
                  "[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromMq] No hay mensajes que consumir en la cola {}.",
                  queueIn);
            }

          }
          catch (Exception e) {
            LOG.warn(e.getMessage(), e);
            closeExecutorNow();
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
          finally {
            closeExecutor();
          }
        }
      }

    }
    catch (JMSException e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    LOG.debug("[LoadXmlOrdenFidessaFromInputsBo :: procesarXmlsFromMq] fin. {}", queueIn);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private RunnableProcess<RecordBean> getHiloConsumidorMq(final ConnectionFactory connectionFactory,
      final String consumeQueueName, final String rejectQueue, final CachedValuesLoadXmlOrdenDTO cachedConfig) {

    RunnableProcess<RecordBean> process = new RunnableProcess<RecordBean>(getObserver()) {

      @Override
      @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
      public void run() {

        try {
          try (Connection connection = jmsMessagesBo.getNewConnection(connectionFactory);) {
            connection.start();
            try (Session session = jmsMessagesBo.getNewSession(connection, true);
                MessageConsumer messageConsumer = jmsMessagesBo.getNewConsumer(session, consumeQueueName);
                MessageProducer messageProducer = jmsMessagesBo.getNewProducer(session, rejectQueue);) {
              try {

                loadXmlSubList.consumesFromMq(session, messageConsumer, messageProducer, getTransactionSize(),
                    readMqTimeOut, cachedConfig);
              }
              catch (Exception e) {
                if (session != null) {
                  session.rollback();
                }
                throw e;
              }
            }

          }
        }
        catch (Exception e) {
          getObserver().setException(e);
          LOG.warn(e.getMessage(), e);
        }
        finally {
          getObserver().sutDownThread(this);
        }

      }
    };
    return process;
  }

  @Override
  public int getThreadSize() {
    return threadPoolSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

}
