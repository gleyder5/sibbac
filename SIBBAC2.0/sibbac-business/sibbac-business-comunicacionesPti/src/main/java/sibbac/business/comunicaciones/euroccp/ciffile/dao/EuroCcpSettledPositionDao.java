package sibbac.business.comunicaciones.euroccp.ciffile.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpSettledPosition;

@Component
public interface EuroCcpSettledPositionDao extends JpaRepository<EuroCcpSettledPosition, Long> {

}
