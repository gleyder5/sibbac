package sibbac.business.comunicaciones.imputacions3.bo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jms.JMSException;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.ac.bo.AceptacionRechazoMovimientosPtiBo;
import sibbac.business.comunicaciones.pti.ac.dto.AceptacionRechazoMovimientosDTO;
import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.comunicaciones.pti.mo.exceptions.NoPtiMensajeDataException;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0anotacioneccBo;
import sibbac.business.wrappers.database.bo.Tmct0movimientoeccFidessaBo;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.tasks.WrapperUserTaskConcurrencyPrevent;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageVersion;

@Service
public class ManualImputacionS3Pti extends WrapperUserTaskConcurrencyPrevent implements IManualImputacionS3Pti {

  @Value("${sibbac.nacional.defaults.cta.compensacion}")
  private String ctasDefault;

  @Value("${sibbac.nacional.segmentos.mab}")
  private String segmentosMab;

  @Autowired
  private PtiCommomns ptiCommons;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private AceptacionRechazoMovimientosPtiBo aceptacionRechazoMovsBo;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo cuentasCompensacionBo;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private FestivoBo festivoBo;

  @Autowired
  private ImputacionS3Multithread imputacionS3Multithread;

  @Autowired
  private ImputacionS3Bo imputacionS3Bo;

  @Override
  public void executeTask() throws Exception {
    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }

    List<Integer> activationDaysCfg = tmct0cfgBo.findDiasActivacionProcesos();

    Calendar now = Calendar.getInstance();

    if (!activationDaysCfg.contains(now.get(Calendar.DAY_OF_WEEK))) {
      LOG.warn("[ManualImputacionS3Pti :: executeTask] No es un dia de activacion del proceso.");
      return;
    }

    this.enviarInputacionesS3(false, "SIBBAC20");

  }

  @Override
  public CallableResultDTO executeTaskManualUser(String auditUser) throws Exception {
    return this.enviarInputacionesS3(true, auditUser);
  }

  private CallableResultDTO enviarInputacionesS3(boolean isEjecucionManualUsuario, String auditUser)
      throws SIBBACBusinessException, PTIMessageException, JMSException, ParseException {
    BigDecimal nowHora;

    BigDecimal horaIniImputaciones;
    BigDecimal horaEndImputaciones;
    DateFormat df = new SimpleDateFormat("HHmmss");

    Calendar now = Calendar.getInstance();
    CallableResultDTO res = new CallableResultDTO();

    try {
      String camara = tmct0cfgBo.getIdPti();
      List<Date> holydays = festivoBo.findAllFestivosNacionales();

      List<String> ctasDef = Arrays.asList(ctasDefault.split(","));
      List<Integer> workDaysOfWeek = tmct0cfgBo.getWorkDaysOfWeek();
      List<String> segmentosMabDef = Arrays.asList(segmentosMab.split(","));
      Date horaEndImputacionesDate = df.parse(tmct0cfgBo.findHoraFinalImputacionesS3Pti());
      Date horaIniImputacionesDate = df.parse(tmct0cfgBo.findHoraInicioImputacionesS3Pti());
      PTIMessageVersion version = tmct0cfgBo.getPtiMessageVersionFromTmct0cfg(new Date());

      Date sinceDateS3 = DateHelper.getPreviousWorkDate(now.getTime(), 1, workDaysOfWeek, holydays);

      String cuentaCompensacionS3 = cuentasCompensacionBo.findCuentaCompensacionS3();

      LOG.info(
          "[enviarInputacionesS3] config: horaInicio: {} horaFin: {} ctas. Origen: {} fecha: {} cta. Destino: {} version: {}",
          horaIniImputacionesDate, horaEndImputacionesDate, ctasDef, sinceDateS3, cuentaCompensacionS3, version);

      nowHora = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(now.getTime(),
          FormatDataUtils.TIME_FORMAT));

      if (cuentaCompensacionS3 == null || cuentaCompensacionS3.trim().isEmpty()) {
        throw new SIBBACBusinessException("No hay cuenta de compensacion configurada para la imputacion S3.");
      }

      if (DateHelper.isHoliday(now, holydays)) {
        throw new SIBBACBusinessException("No de puede ejecutar la imputacion S3 un dia festivo.");
      }
      else if (DateHelper.isWeekend(now.getTime(), workDaysOfWeek)) {
        throw new SIBBACBusinessException("No de puede ejecutar la imputacion S3 en fin de semana.");
      }
      else if (isEjecucionManualUsuario) {
        if (!menBo.isSessionOpened(false)) {
          LOG.warn("[enviarImputacionS3] LA SESION PTI ESTA CERRADA.");
          throw new SIBBACBusinessException("La sesion con PTI esta cerrada.");
        }
        this.mandarImputacionesS3NoMab(sinceDateS3, ctasDef, segmentosMabDef, camara, cuentaCompensacionS3, version,
            auditUser, res);
      }
      else {
        if (menBo.isSessionOpened(false)) {

          imputacionS3Bo.validarHoraInicioFinBotonazo(horaIniImputacionesDate, horaEndImputacionesDate);

          horaIniImputaciones = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(
              horaIniImputacionesDate, FormatDataUtils.TIME_FORMAT));

          horaEndImputaciones = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(
              horaEndImputacionesDate, FormatDataUtils.TIME_FORMAT));

          if (nowHora.compareTo(horaIniImputaciones) >= 0 && nowHora.compareTo(horaEndImputaciones) <= 0) {
            this.mandarImputacionesS3NoMab(sinceDateS3, ctasDef, segmentosMabDef, camara, cuentaCompensacionS3,
                version, auditUser, res);

          }
          else {
            LOG.info("[enviarImputacionS3] No es la hora de ejecucion del botonazo PTI para operaciones NO MAB.");
          }

        }
        else {
          LOG.warn("[enviarImputacionS3] LA SESION PTI ESTA CERRADA.");
        }
      }

    }
    catch (Exception e) {
      if (isEjecucionManualUsuario) {
        LOG.warn(e.getMessage());
        LOG.trace(e.getMessage(), e);
        res.addErrorMessage(e.getMessage());
      }
      else {
        throw e;
      }
    }
    return res;
  }

  private void mandarImputacionesS3NoMab(Date sinceDateS3, List<String> ctasDef, List<String> segmentosMabDef,
      String camara, String cuentaCompensacionS3, PTIMessageVersion version, String auditUser, CallableResultDTO res)
      throws NoPtiMensajeDataException, PTIMessageException, JMSException, SIBBACBusinessException {
    this.mandarCancelacionMovimientos(sinceDateS3, segmentosMabDef);
    LOG.info("[mandarImputacionesS3NoMab] Buscando operaciones ecc con titulos disponibles para enviar a S3...");

    List<String> cdoperacioneccs = anBo.findAllCdoperacioneccTitulosDisponiblesForEnvioS3AndNotInSegmentosAndCamara(
        sinceDateS3, ctasDef, segmentosMabDef, camara);

    LOG.info("[mandarImputacionesS3NoMab] Encontradas {} operaciones con titulos disponibles de {}",
        cdoperacioneccs.size(), sinceDateS3);
    if (CollectionUtils.isNotEmpty(cdoperacioneccs)) {

      String msg = imputacionS3Multithread.crearImputacionesS3(cdoperacioneccs, cuentaCompensacionS3, version,
          auditUser);
      msg = String.format("%s que no son de mab.", msg);
      res.addMessage(msg);
    }
    else {
      res.addMessage("No hay operaciones pti no mab que imputar.");
    }
  }

  private void mandarCancelacionMovimientos(Date date, List<String> segmentosMabDef) throws SIBBACBusinessException {
    List<Tmct0movimientoeccFidessa> listMovf = movfBo.findAllMovimientosGiveUpPdteAceptarCompensador(date);
    AceptacionRechazoMovimientosDTO cdrefmov;
    if (CollectionUtils.isNotEmpty(listMovf)) {
      List<AceptacionRechazoMovimientosDTO> listCdrefmov = new ArrayList<AceptacionRechazoMovimientosDTO>();
      for (Tmct0movimientoeccFidessa movf : listMovf) {
        if (!segmentosMabDef.contains(movf.getSegmento().trim())) {
          cdrefmov = new AceptacionRechazoMovimientosDTO(movf.getCdrefmovimientoecc(), movf.getFliquidacion());
          if (!listCdrefmov.contains(cdrefmov)) {
            listCdrefmov.add(cdrefmov);
          }
        }
      }
      if (CollectionUtils.isNotEmpty(listCdrefmov)) {
        for (int i = 0; i < listCdrefmov.size(); i++) {
          try {
            aceptacionRechazoMovsBo.enviarCancelacionMovimientosSalidaByCdrefmovimientoecc(
                listCdrefmov.subList(i, i + 1), "SIBBAC20");
          }
          catch (Exception e) {
            LOG.warn(e.getMessage(), e);
          }
        }
      }
    }

  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_IMPUTACION_S3_PTI;
  }

}
