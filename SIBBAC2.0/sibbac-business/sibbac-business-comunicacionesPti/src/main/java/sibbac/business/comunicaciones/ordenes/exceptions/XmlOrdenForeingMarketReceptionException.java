/**
 * 
 */
package sibbac.business.comunicaciones.ordenes.exceptions;

/**
 * @author xIS16630
 *
 */
public class XmlOrdenForeingMarketReceptionException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -4658922532815699237L;

  /**
   * 
   */
  public XmlOrdenForeingMarketReceptionException() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   */
  public XmlOrdenForeingMarketReceptionException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param cause
   */
  public XmlOrdenForeingMarketReceptionException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param message
   * @param cause
   */
  public XmlOrdenForeingMarketReceptionException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

}
