package sibbac.business.comunicaciones.euroccp.titulares.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.database.DBConstants.EURO_CCP;

@Table(name = EURO_CCP.OWNERSHIP_REPORTING_GROSS_EXECUTION)
@Entity
// public class EuroCcpOwnershipReportingGrossExecutionsDTO extends ATable<EuroCcpOwnershipReportingGrossExecutionsDTO>
// {
public class EuroCcpOwnershipReportingGrossExecutions extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = -5599354852621418791L;

  @Column(name = "ESTADO_PROCESADO", nullable = false)
  private int estadoProcesado;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRADE_DATE", nullable = false)
  private Date tradeDate;

  @Column(name = "EXECUTION_REFERENCE", length = 20, nullable = false)
  private String executionReference;

  @Column(name = "EXCHANGE_CODE_TRADE", length = 4, nullable = false)
  private String mic;

  @Column(name = "ACCOUNT_NUMBER", length = 10, nullable = false)
  private String accountNumber;

  @Column(name = "OWNER_REFERENCE_FROM", length = 20, nullable = true)
  private String ownerReferenceFrom = "";

  @Column(name = "OWNER_REFERENCE_TO", length = 20, nullable = false)
  private String ownerReferenceTo;

  @Column(name = "NUMBER_SHARES", length = 10, scale = 0, nullable = false)
  private BigDecimal numberShares;

  @Column(name = "NUMBER_SHARES_AVAILABLE", length = 10, scale = 0, nullable = false)
  private BigDecimal numberSharesAvailable;

  @Column(name = "PROCESSING_STATUS", length = 1, nullable = true)
  private Character processingStatus;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ERROR_CODE", nullable = true)
  private EuroCcpErrorCode errorCode;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDDESGLOSECAMARAENVIORT", nullable = false)
  private Tmct0DesgloseCamaraEnvioRt desgloseCamaraEnvioRt;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_FILE_SENT", nullable = false)
  private EuroCcpFileSent fileSent;

  @ManyToMany(mappedBy = "parentReference",
              fetch = FetchType.LAZY,
              cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
              })
  private List<EuroCcpOwnershipReportingGrossExecutionsParentReference> parentReferences = new ArrayList<EuroCcpOwnershipReportingGrossExecutionsParentReference>();

  @Transient
  private String filler;

  public EuroCcpOwnershipReportingGrossExecutions() {
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public Date getTradeDate() {
    return tradeDate;
  }

  public String getExecutionReference() {
    return executionReference;
  }

  public String getMic() {
    return mic;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public String getOwnerReferenceFrom() {
    return ownerReferenceFrom;
  }

  public String getOwnerReferenceTo() {
    return ownerReferenceTo;
  }

  public BigDecimal getNumberShares() {
    return numberShares;
  }

  public Character getProcessingStatus() {
    return processingStatus;
  }

  public EuroCcpErrorCode getErrorCode() {
    return errorCode;
  }

  public Tmct0DesgloseCamaraEnvioRt getDesgloseCamaraEnvioRt() {
    return desgloseCamaraEnvioRt;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setTradeDate(Date tradeDate) {
    this.tradeDate = tradeDate;
  }

  public void setExecutionReference(String executionReference) {
    this.executionReference = executionReference;
  }

  public void setMic(String mic) {
    this.mic = mic;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public void setOwnerReferenceFrom(String ownerReferenceFrom) {
    this.ownerReferenceFrom = ownerReferenceFrom;
  }

  public void setOwnerReferenceTo(String ownerReferenceTo) {
    this.ownerReferenceTo = ownerReferenceTo;
  }

  public void setNumberShares(BigDecimal numberShares) {
    this.numberShares = numberShares;
  }

  public void setProcessingStatus(Character processingStatus) {
    this.processingStatus = processingStatus;
  }

  public void setErrorCode(EuroCcpErrorCode errorCode) {
    this.errorCode = errorCode;
  }

  public EuroCcpFileSent getFileSent() {
    return fileSent;
  }

  public void setFileSent(EuroCcpFileSent fileSent) {
    this.fileSent = fileSent;
  }

  public void setDesgloseCamaraEnvioRt(Tmct0DesgloseCamaraEnvioRt desgloseCamaraEnvioRt) {
    this.desgloseCamaraEnvioRt = desgloseCamaraEnvioRt;
  }

  public List<EuroCcpOwnershipReportingGrossExecutionsParentReference> getParentReferences() {
    return parentReferences;
  }

  public void setParentReferences(List<EuroCcpOwnershipReportingGrossExecutionsParentReference> parentReferences) {
    this.parentReferences = parentReferences;
  }

  public BigDecimal getNumberSharesAvailable() {
    return numberSharesAvailable;
  }

  public void setNumberSharesAvailable(BigDecimal numberSharesAvailable) {
    this.numberSharesAvailable = numberSharesAvailable;
  }

  public String getFiller() {
    return filler;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

  @Override
  public String toString() {
    return "EuroCcpOwnershipReportingGrossExecutionsDTO [tradeDate=" + tradeDate + ", executionReference="
           + executionReference + ", mic=" + mic + ", accountNumber=" + accountNumber + ", ownerReferenceFrom="
           + ownerReferenceFrom + ", ownerReferenceTo=" + ownerReferenceTo + ", numberShares=" + numberShares
           + ", processingStatus=" + processingStatus + ", errorCode=" + errorCode + "]";
  }

}
