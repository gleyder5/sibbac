package sibbac.business.comunicaciones.pti.escalado.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class ActualizacionCodigosOperacionPtiWithUserLockBo {
  private static final Logger LOG = LoggerFactory.getLogger(ActualizacionCodigosOperacionPtiWithUserLockBo.class);

  @Autowired
  private ActualizacionCodigosOperacionPtiBo actualizacionCodigosOperacionBo;

  @Autowired
  private Tmct0bokBo bokBo;

  public ActualizacionCodigosOperacionPtiWithUserLockBo() {
  }

  @Transactional(propagation = Propagation.NOT_SUPPORTED, isolation = Isolation.READ_UNCOMMITTED)
  public CallableResultDTO actualizarCodigosOperacionOrdenesWituserLock(List<Tmct0bokId> listBookingsId,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[actualizarCodigosOperacionOrdenesWituserLock] inicio");

    CallableResultDTO res = new CallableResultDTO();
    boolean imLock = false;
    for (Tmct0bokId bookId : listBookingsId) {
      imLock = false;
      try {
        bokBo.bloquearBooking(bookId, auditUser);
        imLock = true;
        res.append(actualizacionCodigosOperacionBo.actualizarCodigosOperacionOrdenes(bookId, auditUser));
      }
      catch (LockUserException e) {
        LOG.warn(e.getMessage(), e);
      }
      catch (SIBBACBusinessException e) {
        throw e;
      }
      finally {
        if (imLock) {
          try {
            bokBo.desBloquearBooking(bookId, auditUser);
          }
          catch (LockUserException e) {
            LOG.warn(e.getMessage(), e);

          }
        }
      }
    }

    LOG.trace("[actualizarCodigosOperacionOrdenesWituserLock] fin");
    return res;

  }

}
