package sibbac.business.comunicaciones.pti.cuadreecc.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.model.CuadreEccReferencia;

@Component
public interface CuadreEccReferenciaDao extends JpaRepository<CuadreEccReferencia, Long> {

  CuadreEccReferencia findById(final Long id);

  @Modifying
  @Query("DELETE FROM CuadreEccReferencia s WHERE s.id = :id")
  Integer deleteById(@Param("id") Long id);

  CuadreEccReferencia findByfechaEjeAndNumOpAndSentidoAndRefTitular(Date fechaEje,
                                                                    String numOp,
                                                                    Character sentido,
                                                                    String refTitular);

  CuadreEccReferencia findByfechaEjeAndNumOpAndSentidoAndRefTitularAndNumValoresImpNominalPendienteDesglose(Date fechaEje,
                                                                                                            String numOp,
                                                                                                            Character sentido,
                                                                                                            String refTitular,
                                                                                                            BigDecimal numValoresImpNominalPendienteDesglose);

  CuadreEccReferencia findByfechaEjeAndNumOpAndSentidoAndNumValoresImpNominalPendienteDesglose(Date fechaEje,
                                                                                               String numOp,
                                                                                               Character sentido,
                                                                                               BigDecimal numValoresImpNominalPendienteDesglose);

  Integer countByfechaEjeAndNumOpAndSentidoAndRefTitular(Date fechaEje,
                                                         String numOp,
                                                         Character sentido,
                                                         String refTitular);

  List<CuadreEccReferencia> findByEstado(int estado);

  @Query("SELECT o FROM CuadreEccReferencia o WHERE o.fechaEje =:fechaejec AND o.numOp=:noper1 order by  refTitular, fechaEje, numOp")
  List<CuadreEccReferencia> findByfechaEjeAndNumOp(@Param("fechaejec") Date fechaEje, @Param("noper1") String numOp1);

  @Query("SELECT o FROM CuadreEccReferencia o WHERE o.fechaEje = :fechaEje "
         + " AND o.numOp in (:nopera) AND o.sentido = :sentido " + " AND o.estado < :cdestadotit "
         + " ORDER BY  refTitular, fechaEje, numOp")
  List<CuadreEccReferencia> findByNumOpesAndFeoperacAndSentidoAndCdestadotit(@Param("fechaEje") Date fechaEje,
                                                                             @Param("nopera") List<String> nopera,
                                                                             @Param("sentido") Character sentido,
                                                                             @Param("cdestadotit") Integer cdestadotit);

  @Query("SELECT DISTINCT o FROM CuadreEccReferencia o, CuadreEccSibbac e WHERE o.cdisin = e.cdisin "
         + " AND o.fechaEje = e.feoperac AND o.sentido = e.sentido AND o.idEcc = e.cdcamara "
         + " AND o.cdsegmento = e.cdsegmento AND o.cdoperacionmkt = e.cdoperacionmkt AND o.cdmiembromkt = e.cdmiembromkt "
         + " AND o.nuordenmkt = e.nuordenmkt AND o.nuexemkt = e.nuexemkt AND o.numOp = e.numOp "
         + " AND e.nuorden = :nuorden AND e.nbooking = :nbooking AND e.nucnfclt = :nucnfclt AND e.estado < :cdestadotit "
         + " ORDER BY  o.refTitular, o.fechaEje, o.numOp")
  List<CuadreEccReferencia> findByNuordenAndNbookingAndNucnfcltAndCdestadotitLt(@Param("nuorden") String nuorden,
                                                                                @Param("nbooking") String nbooking,
                                                                                @Param("nucnfclt") String nucnfclt,
                                                                                @Param("cdestadotit") Integer cdestadotit,
                                                                                Pageable page);

  @Query("SELECT DISTINCT o FROM CuadreEccReferencia o, CuadreEccSibbac e WHERE o.cdisin = e.cdisin "
         + " AND o.fechaEje = e.feoperac AND o.sentido = e.sentido AND o.idEcc = e.cdcamara "
         + " AND o.cdsegmento = e.cdsegmento AND o.cdoperacionmkt = e.cdoperacionmkt AND o.cdmiembromkt = e.cdmiembromkt "
         + " AND o.nuordenmkt = e.nuordenmkt AND o.nuexemkt = e.nuexemkt AND o.numOp = e.numOp AND o.refTitular = e.refTitular "
         + " AND e.nuorden = :nuorden AND e.nbooking = :nbooking AND e.nucnfclt = :nucnfclt  "
         + " ORDER BY  o.refTitular, o.fechaEje, o.numOp")
  List<CuadreEccReferencia> findByNuordenAndNbookingAndNucnfcltJoinWithReferenciaTitular(@Param("nuorden") String nuorden,
                                                                                         @Param("nbooking") String nbooking,
                                                                                         @Param("nucnfclt") String nucnfclt);

  @Modifying
  @Query("DELETE FROM CuadreEccReferencia o WHERE o.fechaEje = :fechaEje AND o.numOp = :noper1 "
         + " AND o.sentido = :sentido ")
  Integer deleteByFechaEjeAndNumOpAndSentido(@Param("fechaEje") Date fechaEje,
                                             @Param("noper1") String numOp1,
                                             @Param("sentido") Character sentido);

  @Query("SELECT o.id FROM CuadreEccReferencia o WHERE o.fechaEje = :fechaEje AND o.numOp = :noper1 "
         + " AND o.sentido = :sentido ")
  List<Long> findAllIdsByFechaEjeAndNumOpAndSentido(@Param("fechaEje") Date fechaEje,
                                                    @Param("noper1") String numOp1,
                                                    @Param("sentido") Character sentido);

  @Query("SELECT o.id FROM CuadreEccReferencia o WHERE o.idCuadreEccEjecucion =  :idCuadreEccEjecucion ")
  List<Long> findAllIdsByIdCuadreEccEjecucion(@Param("idCuadreEccEjecucion") long idCuadreEccEjecucion);

  @Query("SELECT sum(o.numValoresImpNominal) FROM CuadreEccReferencia o WHERE o.idCuadreEccEjecucion =  :idCuadreEccEjecucion AND o.indRectificacion <> 'B' ")
  BigDecimal getSumNumvaloresImpNominalActivoByIdCuadreEccEjecucion(@Param("idCuadreEccEjecucion") long idCuadreEccEjecucion);

  @Query("SELECT sum(o.impEfectivo) FROM CuadreEccReferencia o WHERE o.idCuadreEccEjecucion =  :idCuadreEccEjecucion AND o.indRectificacion <> 'B' ")
  BigDecimal getSumImpEfectivoActivoByIdCuadreEccEjecucion(@Param("idCuadreEccEjecucion") long idCuadreEccEjecucion);

  @Query("SELECT sum(o.numValoresImpNominal) FROM CuadreEccReferencia o WHERE o.idCuadreEccEjecucion =  :idCuadreEccEjecucion AND o.indRectificacion <> 'B' "
         + " AND o.id <> :id ")
  BigDecimal getSumNumvaloresImpNominalActivoByIdCuadreEccEjecucionAndDistinctId(@Param("idCuadreEccEjecucion") long idCuadreEccEjecucion,
                                                                                 @Param("id") long distinctId);

  @Query("SELECT min(o.id) FROM CuadreEccReferencia o WHERE o.idCuadreEccEjecucion =  :idCuadreEccEjecucion AND o.indRectificacion <> 'B' ")
  Long getMinIdActivoByIdCuadreEccEjecucion(@Param("idCuadreEccEjecucion") long idCuadreEccEjecucion);

  @Query("SELECT o FROM CuadreEccReferencia o WHERE o.fechaEje = :fechaEje AND o.numOp = :noper1 "
         + " AND o.sentido = :sentido ")
  List<CuadreEccReferencia> findAllByFechaEjeAndNumOpAndSentido(@Param("fechaEje") Date fechaEje,
                                                                @Param("noper1") String numOp1,
                                                                @Param("sentido") Character sentido);

  @Modifying
  @Query("UPDATE CuadreEccReferencia o SET o.estadoProcesado = :pEstadoProcesado, o.auditDate = :pAuditDate"
         + " WHERE o.idCuadreEccEjecucion = :pIdCuadreEccEjecucion ")
  Integer updateEstadoProcesadoByIdCuadreEccEjecucion(@Param("pEstadoProcesado") Integer estadoProcesado,
                                                      @Param("pAuditDate") Date auditDate,
                                                      @Param("pIdCuadreEccEjecucion") Long idCuadreEccEjecucion);

  @Modifying
  @Query("UPDATE CuadreEccReferencia o SET o.estadoProcesado = :pEstadoProcesado, o.auditDate = :pAuditDate"
         + " WHERE o.idCuadreEccEjecucion = :pIdCuadreEccEjecucion AND o.estadoProcesado >= :pEstadoProcesadoGte")
  Integer updateEstadoProcesadoByIdCuadreEccEjecucionAndEstadoProcesadoGte(@Param("pEstadoProcesado") Integer estadoProcesado,
                                                                           @Param("pAuditDate") Date auditDate,
                                                                           @Param("pIdCuadreEccEjecucion") Long idCuadreEccEjecucion,
                                                                           @Param("pEstadoProcesadoGte") Integer estadoProcesadoGte);

  @Query("SELECT DISTINCT o.fechaLiq FROM CuadreEccReferencia o WHERE o.estadoProcesado = :estadoProcesado ")
  List<Date> findAllDistinctFechaLiqByEstadoProcesado(@Param("estadoProcesado") Integer estadoProcesado);

  @Query("SELECT new sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccIdDTO( o.id ) "
         + " FROM CuadreEccReferencia o WHERE o.estadoProcesado = :estadoProcesado AND o.fechaLiq = :fechaLiq")
  List<CuadreEccIdDTO> findAllCuadreEccIdDTOByEstadoProcesadoAndFechaLiq(@Param("estadoProcesado") Integer estadoProcesado,
                                                                         @Param("fechaLiq") Date fechaLiq,
                                                                         Pageable page);

} // CuadreEccReferenciaDao
