package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpTrailerFields implements WrapperFileReaderFieldEnumInterface {

  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  HOLDING_NUMBER("holdingNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  REPORT_DATE("reportDate", 8, 0, WrapperFileReaderFieldType.DATE),
  TOTAL_NUMBER_RECORDS("totalNumberRecords", 8, 0, WrapperFileReaderFieldType.NUMERIC),
  EURO_CCP_BIC_CODE("euroCcpBicCode", 11, 0, WrapperFileReaderFieldType.STRING),
  DELTA_FILE_SEQUENCE_NUMBER("deltaFileSequenceNumber", 2, 0, WrapperFileReaderFieldType.NUMERIC),

  FILLER("filler", 438, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpTrailerFields(String name, int length, int scale, WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
