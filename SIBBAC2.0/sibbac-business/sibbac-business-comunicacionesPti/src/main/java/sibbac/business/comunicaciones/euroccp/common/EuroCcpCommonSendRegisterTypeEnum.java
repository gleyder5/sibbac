package sibbac.business.comunicaciones.euroccp.common;

public enum EuroCcpCommonSendRegisterTypeEnum {

  FOOTER, RECORD;
}
