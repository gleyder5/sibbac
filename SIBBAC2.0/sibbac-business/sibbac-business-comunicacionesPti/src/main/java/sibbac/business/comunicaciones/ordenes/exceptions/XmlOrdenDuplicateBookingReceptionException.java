/**
 * 
 */
package sibbac.business.comunicaciones.ordenes.exceptions;

/**
 * @author xIS16630
 *
 */
public class XmlOrdenDuplicateBookingReceptionException extends XmlOrdenExceptionReception {

  /**
   * 
   */
  private static final long serialVersionUID = -3672859994524705178L;

  public XmlOrdenDuplicateBookingReceptionException(String nuorden, String nbooking, String nucnfclt, String dsobserv) {
    super(dsobserv);
    setNbooking(nbooking);
    setNucnfclt(nucnfclt);
    setNuorden(nuorden);
    setDsobserv(dsobserv);
  }

  public XmlOrdenDuplicateBookingReceptionException(String nuorden,
                                               String nbooking,
                                               String nucnfclt,
                                               String dsobserv,
                                               Exception e) {
    super(dsobserv, e);
    setNbooking(nbooking);
    setNucnfclt(nucnfclt);
    setNuorden(nuorden);
    setDsobserv(dsobserv);
  }

}
