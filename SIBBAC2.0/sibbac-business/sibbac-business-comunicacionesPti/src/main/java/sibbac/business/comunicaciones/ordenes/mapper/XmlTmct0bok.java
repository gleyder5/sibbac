package sibbac.business.comunicaciones.ordenes.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.ordenes.bo.LoadXmlOrdenFidessaInputStreamBo;
import sibbac.business.comunicaciones.ordenes.dto.CachedValuesLoadXmlOrdenDTO;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenExceptionReception;
import sibbac.business.comunicaciones.ordenes.exceptions.XmlOrdenInternalReceptionException;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.bo.Tmct0grupoejecucionBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.dao.Tmct0aloDynamicValuesDao;
import sibbac.business.wrappers.database.dao.Tmct0bokDynamicValuesDao;
import sibbac.business.wrappers.database.dao.Tmct0ejeDynamicValuesDao;
import sibbac.business.wrappers.database.dao.Tmct0ejeFeeSchemaDao;
import sibbac.business.wrappers.database.dao.Tmct0gaeDao;
import sibbac.business.wrappers.database.dao.Tmct0galDao;
import sibbac.business.wrappers.database.dao.Tmct0greDao;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0aloDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0bokDynamicValuesId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchema;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0gae;
import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0gre;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.economic.RepartoDatosEconomicosHelper;
import sibbac.common.utils.ImpuestoEnum;
import sibbac.common.utils.SibbacEnums;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

import com.sv.sibbac.multicenter.Allocation;
import com.sv.sibbac.multicenter.AllocationEconomicDataType;
import com.sv.sibbac.multicenter.AllocationID;
import com.sv.sibbac.multicenter.AllocationNet;
import com.sv.sibbac.multicenter.BookingDatesType;
import com.sv.sibbac.multicenter.BookingEconomicDataType;
import com.sv.sibbac.multicenter.BookingType;
import com.sv.sibbac.multicenter.Cargos;
import com.sv.sibbac.multicenter.Commission;
import com.sv.sibbac.multicenter.DynamicValue;
import com.sv.sibbac.multicenter.ExecutionGroupType;
import com.sv.sibbac.multicenter.ExecutionID;
import com.sv.sibbac.multicenter.ExecutionType;
import com.sv.sibbac.multicenter.Impuesto;
import com.sv.sibbac.multicenter.MarketCharges;
import com.sv.sibbac.multicenter.NetConsideration;
import com.sv.sibbac.multicenter.PriceType;
import com.sv.sibbac.multicenter.VolumeType;

/**
 * @author xIS16630
 *
 */
@Service
public class XmlTmct0bok {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(XmlTmct0bok.class);

  private static final String[] NOMBRES_CALCULOS_REPARTIR_EJE_TO_GRUPOS = { "imCanonCompDv", "imCanonCompEu",
      "imCanonContrDv", "imCanonContrEu", "imCanonLiqDv", "imCanonLiqEu" };

  @Autowired
  private XmlTmct0log xmlTmct0log;

  @Autowired
  private XmlTmct0alo xmlTmct0alo;

  @Autowired
  private XmlTmct0eje xmlTmct0eje;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private Tmct0brkBo brkBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0ejeBo ejeBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0grupoejecucionBo grBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejeAloBo;

  @Autowired
  private Tmct0greDao greDao;

  @Autowired
  private Tmct0galDao galDao;

  @Autowired
  private Tmct0gaeDao gaeDao;

  @Autowired
  private Tmct0bokDynamicValuesDao tmct0bokDynamicValuesDao;

  @Autowired
  private Tmct0aloDynamicValuesDao tmct0aloDynamicValuesDao;

  @Autowired
  private Tmct0ejeDynamicValuesDao tmct0ejeDynamicValuesDao;

  @Autowired
  private Tmct0ejeFeeSchemaDao tmct0ejeFeeSchemaDao;

  /**
   * @param tmct0ord
   */
  public XmlTmct0bok() {
    super();

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tmct0bok xml_mapping(Tmct0ord tmct0ord, BookingType booking, CachedValuesLoadXmlOrdenDTO cachedConfig)
      throws NullPointerException, XmlOrdenExceptionReception, XmlOrdenInternalReceptionException {

    List<Tmct0eje> listEjes = new ArrayList<Tmct0eje>();
    List<Tmct0alo> listAlos = new ArrayList<Tmct0alo>();
    List<Tmct0gre> listGres = new ArrayList<Tmct0gre>();

    boolean isOrdenOperativaSinECC = false;
    Tmct0eje tmct0eje;
    BigDecimal nutitejeExecutions = BigDecimal.ZERO;
    boolean casepti = true;

    BigDecimal titulosMtf = BigDecimal.ZERO;

    String internationlMultibookingKey;

    Tmct0bok tmct0bok = new Tmct0bok();
    tmct0bok.setId(new Tmct0bokId(booking.getNbooking(), tmct0ord.getNuorden()));

    tmct0bok.setTmct0ord(tmct0ord);
    /*
     * multicenter:BookingType
     */
    Map<String, String> mapDifferentsExecutions = new HashMap<String, String>();
    Map<String, Map<String, Object>> mBooking = new HashMap<String, Map<String, Object>>();
    Map<String, Tmct0eje> mExecution = new HashMap<String, Tmct0eje>();
    String nbooking = booking.getNbooking();

    // Tmct0gal tmct0gal;
    tmct0bok.getId().setNbooking(nbooking);
    tmct0bok.setDsobserv(booking.getDsobserv());
    tmct0bok.setCdusuari(booking.getCdusuari());
    tmct0bok.setTipexp(booking.getTipexp().charAt(0));
    tmct0bok.setCdisin(tmct0bok.getCdisin());
    tmct0bok.setCdtpoper(tmct0bok.getCdtpoper());

    if ("C".equals(booking.getTipexp())) {
      LOG.trace(" is MAB");
      xmlTmct0log.xml_mapping(tmct0ord, nbooking, "Order type is C-Compensador or MAB");
    }
    else if ("D".equals(booking.getTipexp())) {
      LOG.trace(" is DIRECTIVOS");
      xmlTmct0log.xml_mapping(tmct0ord, nbooking, "Order type is D-Directivos");
    }
    else if ("S".equals(booking.getTipexp())) {
      LOG.trace(" is SEND");
      xmlTmct0log.xml_mapping(tmct0ord, nbooking, "Order type is S-Send");
    }
    else {
      xmlTmct0log.xml_mapping(tmct0ord, nbooking, "Order type is N-Normal");
    }
    /*
     * multicenter:BookingEconomicDataType
     */
    BookingEconomicDataType economicData = booking.getEconomicDataBooking();
    tmct0bok.setImtpcamb(new BigDecimal(economicData.getImtpcamb()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setCdmoniso(economicData.getCdmoniso());

    /*
     * SpanishMarket or ForeingMarket
     */
    Cargos cargo = economicData.getMarketCharges().getCargos();
    tmct0bok.setXtdcm(' ');
    tmct0bok.setImcconeu(BigDecimal.ZERO);
    tmct0bok.setCdindimp(' ');
    if (cargo == null) {
      tmct0bok.setImdereeu(BigDecimal.ZERO);
      tmct0bok.setImbrmerc(BigDecimal.ZERO);
      tmct0bok.setImbrmreu(BigDecimal.ZERO);
      tmct0bok.setImgasliqdv(BigDecimal.ZERO);
      tmct0bok.setImgasliqeu(BigDecimal.ZERO);
      tmct0bok.setImgasgesdv(BigDecimal.ZERO);
      tmct0bok.setImgasgeseu(BigDecimal.ZERO);
      tmct0bok.setImcanoncompdv(BigDecimal.ZERO);
      tmct0bok.setImcanoncompeu(BigDecimal.ZERO);
      tmct0bok.setCanoncontrpagoclte(0);
      tmct0bok.setCanoncontrpagomkt(0);
      tmct0bok.setCanonliqpagoclte(0);
      tmct0bok.setCanonliqpagocmkt(0);
      tmct0bok.setCanoncomppagoclte(0);
      tmct0bok.setCanoncomppagomkt(0);
    }
    else {
      tmct0bok.setImdereeu(new BigDecimal(cargo.getImCanonContrEu()).setScale(8, RoundingMode.HALF_UP));

      // Si es operación de extranjero y renta fija, el gross cliente es el que
      // nos informan en el xml.
      // En caso contrario se recalcula

      if (tmct0ord.getCdclsmdo() == 'E' && "RF".equals(tmct0ord.getCdtpmerc())) {
        tmct0bok.setImbrmerc(new BigDecimal(cargo.getImBrutoMktDv()).setScale(8, RoundingMode.HALF_UP));
        tmct0bok.setImbrmreu(new BigDecimal(cargo.getImBrutoMktEu()).setScale(8, RoundingMode.HALF_UP));

      }
      else {
        tmct0bok.setImbrmerc(new BigDecimal(economicData.getPrice().getPrecioBrutoClte()
            * economicData.getVolume().getNutiteje()).setScale(8, RoundingMode.HALF_UP));
        tmct0bok.setImbrmreu(new BigDecimal(economicData.getPrice().getPrecioBrutoClte()
            * economicData.getVolume().getNutiteje() * economicData.getImtpcamb()).setScale(8, RoundingMode.HALF_UP));
      }

      tmct0bok.setImgasliqdv(new BigDecimal(cargo.getImCanonLiqDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0bok.setImgasliqeu(new BigDecimal(cargo.getImCanonLiqEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0bok.setImgasgesdv(new BigDecimal(cargo.getImCanonContrDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0bok.setImgasgeseu(new BigDecimal(cargo.getImCanonContrEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0bok.setImcanoncompdv(new BigDecimal(cargo.getImCanonCompDv()).setScale(8, RoundingMode.HALF_UP));
      tmct0bok.setImcanoncompeu(new BigDecimal(cargo.getImCanonCompEu()).setScale(8, RoundingMode.HALF_UP));
      tmct0bok.setCanoncontrpagoclte(cargo.isCanonContrPagoClte() ? 1 : 0);
      tmct0bok.setCanoncontrpagomkt(cargo.isCanonContrPagoMkt() ? 1 : 0);
      tmct0bok.setCanonliqpagoclte(cargo.isCanonLiqPagoClte() ? 1 : 0);
      tmct0bok.setCanonliqpagocmkt(cargo.isCanonLiqPagoMkt() ? 1 : 0);
      tmct0bok.setCanoncomppagoclte(cargo.isCanonCompPagoClte() ? 1 : 0);
      tmct0bok.setCanoncomppagomkt(cargo.isCanonCompPagoMkt() ? 1 : 0);
    }

    /*
     * multicenter:volume
     */
    VolumeType volume = economicData.getVolume();
    tmct0bok.setNutitord(new BigDecimal(volume.getNutitord()).setScale(5, RoundingMode.HALF_UP));
    tmct0bok.setNutiteje(new BigDecimal(volume.getNutiteje()).setScale(5, RoundingMode.HALF_UP));

    /*
     * multicenter:price
     */
    PriceType price = economicData.getPrice();
    tmct0bok.setImlimite(new BigDecimal(price.getPrecioLimiteClte()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImcbmerc(new BigDecimal(price.getPrecioBrutoMkt()).setScale(8, RoundingMode.HALF_UP));

    /*
     * multicenter:dates
     */
    // FIXME PROBLEMA CON LAS FECHAS QUE LLEGAN
    BookingDatesType dates = economicData.getDates();
    tmct0bok.setFeconsol(new Date(dates.getFebooking().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setFealta(new Date(dates.getFhalta().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setHoalta(new Time(dates.getFhalta().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setFecontra(new Date(dates.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setFetrade(new Date(dates.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setFeejecuc(new Date(dates.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setHoejecuc(new Time(dates.getFhejecuc().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setFevalide(new Date(dates.getFevalide().toGregorianCalendar().getTimeInMillis()));
    tmct0bok.setFevalor(new Date(dates.getFevalor().toGregorianCalendar().getTimeInMillis()));

    /*
     * multicenter:Net
     */
    NetConsideration net = economicData.getNet();
    tmct0bok.setImcbntcl(new BigDecimal(net.getPrecioNetoClte()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImnetobr(new BigDecimal(net.getImNetoMktDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImnetocl(new BigDecimal(net.getImNetoClteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImntbreu(new BigDecimal(net.getImNetoMktEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImntcleu(new BigDecimal(net.getImNetoClteEu()).setScale(8, RoundingMode.HALF_UP));

    /*
     * multicenter:commisions
     */
    Commission comisions = economicData.getCommisions();
    tmct0bok.setPodevolu(new BigDecimal(comisions.getPcComOrdenante()).setScale(6, RoundingMode.HALF_UP));
    tmct0bok.setPofpacli(new BigDecimal(comisions.getPcComClte()).setScale(6, RoundingMode.HALF_UP));
    tmct0bok.setImdevcdv(new BigDecimal(comisions.getImComOrdenanteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImdevcli(new BigDecimal(comisions.getImComOrdenanteEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImfibrdv(new BigDecimal(comisions.getImComBrkDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImfibreu(new BigDecimal(comisions.getImComBrkEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImsvb(new BigDecimal(comisions.getImCorretajeSVDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImsvbeu(new BigDecimal(comisions.getImCorretajeSVEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImfpacdv(new BigDecimal(comisions.getImComClteDv()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImbensvb(new BigDecimal(comisions.getImCorretajeSVFinalEu()).setScale(8, RoundingMode.HALF_UP));
    tmct0bok.setImcorretajesvfinaldv(new BigDecimal(comisions.getImCorretajeSVFinalDv()).setScale(8,
        RoundingMode.HALF_UP));
    tmct0bok.setImcomclteeu(new BigDecimal(comisions.getImComClteEu()).setScale(8, RoundingMode.HALF_UP));

    BigDecimal acu_eu = BigDecimal.ZERO;
    BigDecimal acu_dv = BigDecimal.ZERO;
    for (Impuesto impuesto : economicData.getMarketCharges().getImpuesto()) {
      acu_eu = acu_eu.add(new BigDecimal(impuesto.getImporteEu()).setScale(8, RoundingMode.HALF_UP));
      acu_dv = acu_dv.add(new BigDecimal(impuesto.getImporteDv()).setScale(8, RoundingMode.HALF_UP));
    }
    tmct0bok.setImimpeur(acu_eu);
    tmct0bok.setImimpdvs(acu_dv);

    /*
     * cdalias campo cdcuenta de ORD
     */
    tmct0bok.setCdalias(tmct0bok.getTmct0ord().getCdcuenta().trim());
    /*
     * Carga descrali Tabla Fmeg0ali clave primaria cdalias. PARA EXTRANJERO
     */
    String descrali = cachedConfig.getDescrali(aliBo, tmct0bok.getTmct0ord().getCdcuenta());

    if (StringUtils.isBlank(tmct0bok.getTmct0ord().getCdcuenta()) || StringUtils.isBlank(descrali)
        && aliBo.findByFirstAliasOrder(tmct0bok.getTmct0ord().getCdcuenta()) == null) {
      throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), tmct0bok.getId().getNbooking(), "", "",
          String.format("No encontrado alias dado de alta: '%s'", tmct0bok.getTmct0ord().getCdcuenta()));
    }
    tmct0bok.setDescrali(descrali.trim());

    Character retailSpecialOp = cachedConfig.getRetailSpecialOp(aliBo, tmct0bok.getTmct0ord().getCdcuenta());
    Character disociar = cachedConfig.getDisociar(aliBo, tmct0bok.getTmct0ord().getCdcuenta());
    /*
     * carga de cdproduct y nuctapro
     */
    if ('N' == tmct0bok.getTmct0ord().getCdclsmdo()) {
      tmct0bok.setCdproduc(StringUtils.substring(tmct0bok.getCdalias(), 0, 3));
      String nuctapro = StringUtils.substring(tmct0bok.getCdalias(), 3, 8);
      if (StringUtils.isNotBlank(nuctapro) && StringUtils.isNumeric(nuctapro)) {
        tmct0bok.setNuctapro(new Integer(nuctapro));
      }
      else {
        tmct0bok.setNuctapro(0);
      }

    }
    else {
      tmct0bok.setCdproduc("");
      tmct0bok.setNuctapro(0);
    }

    tmct0bok.setCdbloque('N');
    tmct0bok.setFhaudit(new Timestamp(System.currentTimeMillis()));
    tmct0bok.setCdusuaud(LoadXmlOrdenFidessaInputStreamBo.CDUSUAUD);

    tmct0bok.setCdisin(tmct0bok.getTmct0ord().getCdisin());
    tmct0bok.setNbvalors(tmct0bok.getTmct0ord().getNbvalors());
    tmct0bok.setCdtpoper(tmct0bok.getTmct0ord().getCdtpoper());

    tmct0bok.setCddatliq(SibbacEnums.SI_NO.SI.getValue());
    tmct0bok.setInbokmul(SibbacEnums.SI_NO.NO.getValue());
    tmct0bok.setIndatfis(' ');
    tmct0bok.setErdatfis("");

    tmct0bok.setCdusublo("");
    tmct0bok.setCdauxili("");

    tmct0bok.setNuagrpkb(0);

    if ('E' != tmct0bok.getTmct0ord().getCdclsmdo()) {
      LOG.trace(" is NATIONAL");
      ExecutionType execution = null;
      if (CollectionUtils.isNotEmpty(booking.getExecutions())) {
        execution = booking.getExecutions().get(0);
      }

      if (execution != null && !execution.isAsignadoComp() && disociar != null && 'S' == disociar) {
        LOG.trace("Aliass Disociacion Sin Titular EstadosAsignacion.PDTE_ENVIAR_SIN_TITULAR");
        xmlTmct0log.xml_mapping(tmct0ord, tmct0bok.getId().getNbooking(), "Aliass Disociacion Sin Titular");
        tmct0bok.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR_SIN_TITULAR
            .getId()));
      }
      else if (retailSpecialOp != null && 'S' == retailSpecialOp) {
        // tmct0bok.getTmct0ord().setIsscrdiv('S');
        LOG.trace("Aliass Operativa Especial EstadosAsignacion.PDTE_OPERATIVA_ESPECIAL");
        xmlTmct0log.xml_mapping(tmct0ord, tmct0bok.getId().getNbooking(), "Aliass Operativa Especial");
        tmct0bok.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL
            .getId()));
      }
      else {
        LOG.trace("Aliass normal EstadosAsignacion.PDTE_DESGLOSAR");
        tmct0bok.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.PDTE_DESGLOSAR.getId()));
      }
      tmct0bok.setCdestadotit(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());
      tmct0bok.setTmct0estadoByCdestadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));

      tmct0bok.setTmct0sta(new Tmct0sta(new Tmct0staId("800", TypeCdtipest.ASIGNACION.getValue())));
    }
    else {
      LOG.trace(" is FOREIGN");
      tmct0bok.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()));
      tmct0bok.setCdestadotit(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
      tmct0bok.setTmct0estadoByCdestadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.RECHAZADA.getId()));
      if (ordBo.isRouting(tmct0ord)) {
        tmct0bok.setTmct0sta(new Tmct0sta(new Tmct0staId(TypeCdestado.FOREING_WAIT_RESPONSE_CLEARER.getValue(),
            TypeCdtipest.TMCT0BOK.getValue())));
      }
      else {
        tmct0bok.setTmct0sta(new Tmct0sta(new Tmct0staId(TypeCdestado.FOREING_PENDIENTE_RECONCILIAR.getValue(),
            TypeCdtipest.TMCT0BOK.getValue())));
      }

    }

    // 2015-08-18
    // Se añade indicador de gastos
    tmct0bok.setCdindgas(SibbacEnums.SI_NO.NO.getValue());
    tmct0bok.setCdrefban("");

    /*
     * carga en tmct0eje de la lista de elementos ExecutionType
     */
    for (ExecutionType execution : booking.getExecutions()) {
      nutitejeExecutions = nutitejeExecutions.add(new BigDecimal(execution.getNutiteje()));
      if ((tmct0eje = mExecution.get(execution.getNuejecuc())) == null) {

        tmct0eje = new Tmct0eje();
        tmct0eje.setTmct0bok(tmct0bok);
        tmct0eje.setId(new Tmct0ejeId(tmct0bok.getId().getNuorden(), tmct0bok.getId().getNbooking(), execution
            .getNuejecuc()));
        listEjes.add(tmct0eje);

      }

      xmlTmct0eje.xml_mapping(tmct0bok, tmct0eje, execution, cachedConfig);
      mExecution.put(execution.getNuejecuc(), tmct0eje);
      if (!isOrdenOperativaSinECC && StringUtils.isNotBlank(execution.getTpopebol())) {
        isOrdenOperativaSinECC = cachedConfig.isTpopebolSinEcc(execution.getTpopebol().trim());
      }

      internationlMultibookingKey = String.format("%s%s%s%s%s", tmct0eje.getFeejecuc(), tmct0eje.getCdmercad().trim(),
          tmct0eje.getCdbroker().trim(), tmct0eje.getTpopebol().trim(),
          tmct0eje.getImcbmerc().setScale(8, BigDecimal.ROUND_HALF_UP));

      mapDifferentsExecutions.put(internationlMultibookingKey, internationlMultibookingKey);

      // revisar la fecha de contratacion

    }// fin for
    for (Tmct0eje tmct0ejeFor : listEjes) {
      if (tmct0ejeFor.getCasepti() != null && !SibbacEnums.SI_NO.SI.getValue().equals(tmct0ejeFor.getCasepti())) {
        casepti = false;
      }

      if (cachedConfig.isMtf(brkBo, tmct0ejeFor.getCdbroker(), tmct0ejeFor.getExecutionTradingVenue())) {
        titulosMtf = titulosMtf.add(tmct0ejeFor.getNutiteje());
      }
    }
    tmct0bok.getTmct0ejes().addAll(listEjes);

    tmct0bok.setCuadreau(SibbacEnums.SI_NO.SI.getValue());
    tmct0bok.setCasepti(SibbacEnums.SI_NO.SI.getValue());
    if ('N' == tmct0bok.getTmct0ord().getCdclsmdo()) {
      if (casepti) {
        tmct0bok.setCasepti(SibbacEnums.SI_NO.SI.getValue());
      }
      else {
        tmct0bok.setCasepti(SibbacEnums.SI_NO.NO.getValue());
      }

    }

    this.mappearAllos(tmct0ord, tmct0bok, booking, mBooking, mExecution, listAlos, cachedConfig);
    this.repartirCanonesEjecuciones(listAlos, mExecution);
    if (!listAlos.isEmpty()) {
      for (Tmct0alo alo : listAlos) {
        if (alo.getCdindgas() == 'S') {
          tmct0bok.setCdindgas(alo.getCdindgas());
          break;
        }
      }
    }

    tmct0bok.getTmct0alos().addAll(listAlos);

    // tmct0bok.setFk_tmct0bok_1( collection1 );
    tmct0bok.setIsnacmul(listAlos.size() > 1 || mapDifferentsExecutions.keySet().size() > 1 ? SibbacEnums.SI_NO.SI
        .getValue() : SibbacEnums.SI_NO.NO.getValue());

    this.mappearGrupos(tmct0bok, booking, mBooking, mExecution, listGres);

    tmct0bok.getTmct0gres().addAll(listGres);

    if (titulosMtf.compareTo(BigDecimal.ZERO) > 0 && titulosMtf.compareTo(tmct0bok.getNutiteje()) < 0) {

      xmlTmct0log.xml_mapping(
          tmct0ord,
          tmct0bok.getId().getNbooking(),
          String.format("Booking parte de las ejecuciones MTF, titulos MTF %s de %s",
              titulosMtf.setScale(2, RoundingMode.HALF_UP), tmct0bok.getNutiteje().setScale(2, RoundingMode.HALF_UP)));
    }
    else if (titulosMtf.compareTo(tmct0bok.getNutiteje()) == 0) {
      xmlTmct0log.xml_mapping(tmct0ord, tmct0bok.getId().getNbooking(), "Booking con TODAS las ejecuciones MTF");
    }

    if (isOrdenOperativaSinECC) {
      xmlTmct0log.xml_mapping(tmct0ord, tmct0bok.getId().getNbooking(), "Orden operativa SIN ECC");
    }
    else if (!casepti) {
      xmlTmct0log.xml_mapping(tmct0ord, tmct0bok.getId().getNbooking(), "Booking pendiente de case");
    }

    mExecution.clear();
    mapDifferentsExecutions.clear();
    mBooking.clear();

    tmct0bok.setImgastotdv(BigDecimal.ZERO);
    tmct0bok.setImgastoteu(BigDecimal.ZERO);
    this.mappDynamicValues(booking, tmct0bok);

    this.persistsAllCascade(tmct0bok);
    tmct0ord.getTmct0boks().add(tmct0bok);

    return tmct0bok;
  }

  private void repartirCanonesEjecuciones(List<Tmct0alo> listAlos, Map<String, Tmct0eje> mExecution)
      throws XmlOrdenExceptionReception {
    List<Object> toReparto;
    List<Tmct0grupoejecucion> listGrupos;
    List<Tmct0ejecucionalocation> listEjeAlos;
    Map<String, List<Tmct0ejecucionalocation>> mapEjecucionAlocation = new HashMap<String, List<Tmct0ejecucionalocation>>();

    for (Tmct0alo tmct0alo : listAlos) {
      listGrupos = tmct0alo.getTmct0grupoejecucions();
      for (Tmct0grupoejecucion gr : listGrupos) {
        listEjeAlos = gr.getTmct0ejecucionalocations();
        for (Tmct0ejecucionalocation ea : listEjeAlos) {
          if (mapEjecucionAlocation.get(ea.getTmct0eje().getId().getNuejecuc().trim()) == null) {
            mapEjecucionAlocation.put(ea.getTmct0eje().getId().getNuejecuc().trim(),
                new ArrayList<Tmct0ejecucionalocation>());
          }
          mapEjecucionAlocation.get(ea.getTmct0eje().getId().getNuejecuc().trim()).add(ea);
        }
      }

    }
    RepartoDatosEconomicosHelper reparto = new RepartoDatosEconomicosHelper();

    List<String> listNombres = Arrays.asList(NOMBRES_CALCULOS_REPARTIR_EJE_TO_GRUPOS);

    for (Tmct0eje eje : mExecution.values()) {
      listEjeAlos = mapEjecucionAlocation.get(eje.getId().getNuejecuc().trim());
      if (listEjeAlos != null) {
        toReparto = new ArrayList<Object>(listEjeAlos);
        try {
          reparto.repartirBigDecimals(eje, toReparto, "nutiteje", "nutitulos", listNombres, listNombres, 2,
              RoundingMode.HALF_DOWN);
        }
        catch (SIBBACBusinessException e) {
          throw new XmlOrdenExceptionReception(e.getMessage(), e);
        }
        finally {
          toReparto.clear();
        }
      }

    }

    mapEjecucionAlocation.clear();

  }

  private void mappearGrupos(Tmct0bok tmct0bok, BookingType booking, Map<String, Map<String, Object>> mBooking,
      Map<String, Tmct0eje> mExecution, List<Tmct0gre> listGres) throws XmlOrdenExceptionReception {

    String nbooking = booking.getNbooking();
    /*
     * carga en tmct0gre de la lista de elementos ExecutionGroupType
     */
    for (ExecutionGroupType executionsGroup : booking.getExecutionGroups()) {
      XmlTmct0gre xmlTmct0gre = new XmlTmct0gre(tmct0bok);
      xmlTmct0gre.xml_mapping(nbooking, executionsGroup, mBooking, mExecution);
      listGres.add(xmlTmct0gre.getJDOObject());
    }
  }

  private void mappearAllos(Tmct0ord tmct0ord, Tmct0bok tmct0bok, BookingType booking,
      Map<String, Map<String, Object>> mBooking, Map<String, Tmct0eje> mExecution, List<Tmct0alo> listAlos,
      CachedValuesLoadXmlOrdenDTO cachedConfig) throws XmlOrdenExceptionReception, XmlOrdenInternalReceptionException {

    Map<String, Object> mAllocation;
    Map<String, Object> mImpuestos;
    Map<String, Object> mImpuesto;

    AllocationEconomicDataType economicDataType;
    Commission commission;
    AllocationNet allNet;
    Cargos cargos;
    MarketCharges marketCharges;
    String nucnfclt;
    String nbooking = booking.getNbooking();
    String nombre;

    /*
     * carga en tmct0alo de la lista de elementos Allocation
     */
    Integer nupareje = aloBo.getMaxNuparejeByNuoprout(tmct0ord.getNuoprout());
    if (nupareje == null) {
      nupareje = 0;
    }
    BigDecimal acu_foreign_mc_eu = BigDecimal.ZERO;
    BigDecimal acu_foreign_mc_dv = BigDecimal.ZERO;

    BigDecimal acu_market_tax_eu = BigDecimal.ZERO;
    BigDecimal acu_market_tax_dv = BigDecimal.ZERO;

    for (Allocation allocation : booking.getAllocations()) {
      nupareje++;
      Tmct0alo tmct0alo = new Tmct0alo();
      tmct0alo.setTmct0bok(tmct0bok);
      tmct0alo.setId(new Tmct0aloId(allocation.getNucnfclt(), tmct0bok.getId().getNbooking(), tmct0bok.getId()
          .getNuorden()));

      xmlTmct0alo.xml_mapping(booking, allocation, tmct0alo, mExecution, cachedConfig);
      tmct0alo.setNupareje(nupareje.shortValue());
      listAlos.add(tmct0alo);
      /*
       * Preparamos los datos que necesitaremos para repartir los datos
       * económicos de los Allocations entre los ExecutionGroups/Allocation
       */
      if (mBooking.containsKey(nucnfclt = allocation.getNucnfclt())) {
        // msgValidacion += " Está duplicado el Booking/Allocation " + nbooking
        // + "/" + nucnfclt;
        // continue;
        throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), nbooking, "", "", String.format(
            "Esta duplicado el Booking/Allocation '%s'/'%s'.", nbooking, nucnfclt));
      }
      mBooking.put(nucnfclt, mAllocation = new HashMap<String, Object>());
      mAllocation.put("impuesto", mImpuestos = new HashMap<String, Object>());
      mAllocation.put("cdmoniso", (economicDataType = allocation.getEconomicDataAlloc()).getCdmoniso());

      setValue(mAllocation, "nutitcli", allocation.getNutitcli());
      setRestValue(mAllocation, "nutitcli", allocation.getNutitcli());
      setValue(mAllocation, "nutitcli_sin_repartir", allocation.getNutitcli());
      setRestValue(mAllocation, "nutitcli_sin_repartir", allocation.getNutitcli());
      setValue(mAllocation, "imtpcamb", economicDataType.getImtpcamb());
      setRestValue(mAllocation, "imtpcamb", economicDataType.getImtpcamb());
      setRestValue(mAllocation, "imsvb", (commission = economicDataType.getCommision()).getImCorretajeSVDv());
      setRestValue(mAllocation, "imsvbeu", commission.getImCorretajeSVEu());
      setRestValue(mAllocation, "imfibrdv", commission.getImComBrkDv());
      setRestValue(mAllocation, "imfibreu", commission.getImComBrkEu());
      setRestValue(mAllocation, "imcomdev", commission.getImComOrdenanteDv());
      setRestValue(mAllocation, "eucomdev", commission.getImComOrdenanteEu());
      setRestValue(mAllocation, "imcomisn", commission.getImComClteDv());
      setRestValue(mAllocation, "imcomieu", commission.getImComClteEu());
      setRestValue(mAllocation, "imtotnet", (allNet = economicDataType.getNet()).getImNetoClteDv());
      setRestValue(mAllocation, "eutotnet", allNet.getImNetoClteEu());

      // 2015-08-18
      // Se añaden campos imtotbru y eutotbru
      setRestValue(mAllocation, "imtotbru", allNet.getImBrutoClteDv());
      setRestValue(mAllocation, "eutotbru", allNet.getImBrutoClteEu());

      setRestValue(mAllocation, "imbrmerc",
          (cargos = (marketCharges = economicDataType.getMarketCharges()).getCargos()).getImBrutoMktDv());
      setRestValue(mAllocation, "imbrmreu", cargos.getImBrutoMktEu());
      setRestValue(mAllocation, "imgasgeseu", cargos.getImCanonContrEu());
      setRestValue(mAllocation, "imgasgesdv", cargos.getImCanonContrDv());
      setRestValue(mAllocation, "imgasliqeu", cargos.getImCanonLiqEu());
      setRestValue(mAllocation, "imgasliqdv", cargos.getImCanonLiqDv());

      // Crear estos campos a nivel de gal
      setRestValue(mAllocation, "imCanonCompDv", cargos.getImCanonCompEu());
      setRestValue(mAllocation, "imCanonCompEu", cargos.getImCanonCompDv() == null ? 0 : cargos.getImCanonCompDv());
      mAllocation.put("canonContrPagoClte", cargos.isCanonContrPagoClte() ? 1 : 0);
      mAllocation.put("canonContrPagoMkt", cargos.isCanonContrPagoMkt() ? 1 : 0);
      mAllocation.put("canonCompPagoClte", cargos.isCanonCompPagoClte() ? 1 : 0);
      mAllocation.put("canonCompPagoMkt", cargos.isCanonCompPagoMkt() ? 1 : 0);
      mAllocation.put("canonLiqPagoClte", cargos.isCanonLiqPagoClte() ? 1 : 0);
      mAllocation.put("canonLiqPagoMkt", cargos.isCanonLiqPagoMkt() ? 1 : 0);
      for (Impuesto impuesto : marketCharges.getImpuesto()) {
        if (mImpuestos.containsKey(nombre = impuesto.getNombre())) {
          // msgValidacion += " Está duplicado el Booking/Allocation/Impuesto "
          // + nbooking + "/" + nucnfclt + "/" +
          // nombre;
          // continue;
          throw new XmlOrdenExceptionReception(tmct0ord.getNuorden(), nbooking, "", "", String.format(
              "Esta duplicado el Booking/Allocation/Impuesto '%s'/'%s'/'%s'", nbooking, nucnfclt, nombre));
        }
        mImpuestos.put(nombre, mImpuesto = new HashMap<String, Object>());

        mImpuesto.put("pagoClte", impuesto.isPagoClte());
        mImpuesto.put("pagoMkt", impuesto.isPagoMkt());

        setRestValue(mImpuesto, "importeEu", impuesto.getImporteEu());
        setRestValue(mImpuesto, "importeDv", impuesto.getImporteDv());

        // acu_eu = acu_eu.add(new BigDecimal(impuesto.getImporteEu()));
        // acu_dv = acu_dv.add(new BigDecimal(impuesto.getImporteDv()));

        // xmlTmct0log.xml_mapping(tmct0ord, nbooking,
        // String.format("Received tax '%s' : %s %s from fidessa.",impuesto.getNombre(),
        // impuesto.getImporteDv(), tmct0alo.getCdmoniso()));
        if (impuesto.getNombre().equals(ImpuestoEnum.FOREIGN_MC.name())) {

          tmct0alo.setImgatdvs(new BigDecimal(impuesto.getImporteDv()));
          tmct0alo.setImgastos(new BigDecimal(impuesto.getImporteEu()));
          acu_foreign_mc_eu = acu_foreign_mc_eu.add(new BigDecimal(impuesto.getImporteEu()).setScale(8,
              RoundingMode.HALF_UP));
          acu_foreign_mc_dv = acu_foreign_mc_dv.add(new BigDecimal(impuesto.getImporteDv()).setScale(8,
              RoundingMode.HALF_UP));

          // Reajuste para calcular Market Tax, solo para ordenes de extranjero
          // y que tengan Ftt informado
          // if ('E' == tmct0bok.getTmct0ord().getCdclsmdo() &&
          // BigDecimal.ZERO.compareTo(acu_dv) != 0) {
          //
          // final Character tipoOperacion = tmct0alo.getCdtpoper();
          //
          // final BigDecimal brutoDivisa = tmct0alo.getImtotbru();
          // final BigDecimal netoDivisa = tmct0alo.getImtotnet();
          // final BigDecimal comisionClienteDivisa = tmct0alo.getImcomisn();
          // final BigDecimal fttDivisa = new
          // BigDecimal(impuesto.getImporteDv());
          // BigDecimal marketTaxDivisa = BigDecimal.ZERO;
          //
          // BigDecimal diffDivisa = (tipoOperacion == 'V') ?
          // brutoDivisa.subtract(netoDivisa) : netoDivisa
          // .subtract(brutoDivisa);
          // diffDivisa =
          // diffDivisa.subtract(comisionClienteDivisa).subtract(fttDivisa);
          //
          // if (new BigDecimal(0.1).compareTo(diffDivisa) <= 0) {
          // marketTaxDivisa = diffDivisa;
          //
          // tmct0alo.setImimpdvs(marketTaxDivisa.add(tmct0alo.getImimpdvs()));
          //
          // }
          // setRestValue(mAllocation, "imimpdvs",
          // marketTaxDivisa.doubleValue());
          //
          // final BigDecimal brutoEuro = tmct0alo.getEutotbru();
          // final BigDecimal netoEuro = tmct0alo.getEutotnet();
          // final BigDecimal comisionClienteEuro = tmct0alo.getImcomieu();
          // final BigDecimal fttEuro = new BigDecimal(impuesto.getImporteEu());
          // BigDecimal marketTaxEuro = BigDecimal.ZERO;
          //
          // BigDecimal diffEuro = (tipoOperacion == 'V') ?
          // brutoEuro.subtract(netoEuro) : netoEuro.subtract(brutoEuro);
          // diffEuro =
          // diffEuro.subtract(comisionClienteEuro).subtract(fttEuro);
          //
          // if (new BigDecimal(0.1).compareTo(diffEuro) <= 0) {
          // marketTaxEuro = diffEuro;
          // tmct0alo.setImimpeur(marketTaxEuro.add(tmct0alo.getImimpeur()));
          //
          //
          // }
          // setRestValue(mAllocation, "imimpeur", marketTaxEuro.doubleValue());
          //
          // }

        }

        if (impuesto.getNombre().equals(ImpuestoEnum.MARKET_TAX.name())) {

          acu_market_tax_eu = acu_market_tax_eu.add(new BigDecimal(impuesto.getImporteEu()).setScale(8,
              RoundingMode.HALF_UP));
          acu_market_tax_dv = acu_market_tax_dv.add(new BigDecimal(impuesto.getImporteDv()).setScale(8,
              RoundingMode.HALF_UP));

          tmct0alo.setImimpdvs(new BigDecimal(impuesto.getImporteDv()));
          tmct0alo.setImimpeur(new BigDecimal(impuesto.getImporteEu()));

          // setRestValue(mImpuesto, "imimpdvs", impuesto.getImporteDv());
          // setRestValue(mImpuesto, "imimpeur", impuesto.getImporteEu());
        }

        if (impuesto.getNombre().equals(ImpuestoEnum.CENTER_MKT_CHARGE.name())) {

          // setRestValue(mImpuesto, "imgatdvsb", impuesto.getImporteDv());
          // setRestValue(mImpuesto, "imgastosb", impuesto.getImporteEu());

        }

      }

    }// fin for allocations

    // no se tienen en cuenta el ftt para modificar imimpeur y imimpdvs

    // if (tmct0bok.getImimpeur().compareTo(acu_eu) != 0) {
    // LOG.warn("Importe impuesto EURO bok: {} != alo: {}",
    // tmct0bok.getImimpeur(), acu_eu);
    // tmct0bok.setImimpeur(acu_eu);
    // }
    // if (tmct0bok.getImimpdvs().compareTo(acu_dv) != 0) {
    // LOG.warn("Importe impuesto DIVISA bok: {} != alo: {}",
    // tmct0bok.getImimpdvs(), acu_dv);
    // tmct0bok.setImimpdvs(acu_dv);
    // }

    // Se asigna market_tax y foreign_mc al booking

    if ('E' == tmct0bok.getTmct0ord().getCdclsmdo() && BigDecimal.ZERO.compareTo(acu_market_tax_dv) != 0) {
      tmct0bok.setImimpdvs(acu_market_tax_dv.add(tmct0bok.getImimpdvs()));
      tmct0bok.setImimpeur(acu_market_tax_eu.add(tmct0bok.getImimpeur()));

    }

    if ('E' == tmct0bok.getTmct0ord().getCdclsmdo() && BigDecimal.ZERO.compareTo(acu_foreign_mc_eu) != 0) {
      // Se indica ftt a nivel de booking
      tmct0bok.setImgatdvs(acu_foreign_mc_dv);
      tmct0bok.setImgastos(acu_foreign_mc_eu);
    }

    // Se recalcula marketTax, marketCharge solo si ftt y extranjero
    // if ('E' == tmct0bok.getTmct0ord().getCdclsmdo() &&
    // BigDecimal.ZERO.compareTo(acu_dv) != 0) {
    //
    // // Se indica ftt a nivel de booking
    // tmct0bok.setImgatdvs(acu_dv);
    // tmct0bok.setImgastos(acu_eu);
    //
    // final Character tipoOperacion = tmct0bok.getCdtpoper();
    //
    // final BigDecimal brutoDivisa = tmct0bok.getImbrmerc();
    // final BigDecimal netoDivisa = tmct0bok.getImnetocl();
    // final BigDecimal comisionClienteDivisa = tmct0bok.getImfpacdv();
    // final BigDecimal fttDivisa = acu_dv;
    // BigDecimal marketTaxDivisa = BigDecimal.ZERO;
    //
    // BigDecimal diffDivisa = (tipoOperacion == 'V') ?
    // brutoDivisa.subtract(netoDivisa) : netoDivisa
    // .subtract(brutoDivisa);
    // diffDivisa =
    // diffDivisa.subtract(comisionClienteDivisa).subtract(fttDivisa);
    //
    // if (new BigDecimal(0.1).compareTo(diffDivisa) <= 0) {
    // marketTaxDivisa = diffDivisa;
    // tmct0bok.setImimpdvs(marketTaxDivisa.add(tmct0bok.getImimpdvs()));
    //
    // final BigDecimal imtpcamb = tmct0bok.getImtpcamb();
    // final BigDecimal marketTaxEuro = marketTaxDivisa.multiply(imtpcamb);
    //
    // tmct0bok.setImimpeur(marketTaxEuro.add(tmct0bok.getImimpeur()));
    //
    // }
    //
    // /*
    // * final BigDecimal brutoEuro = tmct0bok.getImbrmreu(); final BigDecimal
    // * netoEuro = tmct0bok.getImntcleu(); // final BigDecimal
    // * comisionClienteEuro = tmct0bok.getimf final BigDecimal fttEuro =
    // * acu_eu; BigDecimal marketTaxEuro = BigDecimal.ZERO;
    // *
    // * BigDecimal diffEuro = (tipoOperacion == 'V') ?
    // * brutoEuro.subtract(netoEuro) : netoEuro.subtract(brutoEuro); diffEuro =
    // * diffEuro.subtract(comisionClienteEuro).subtract(fttEuro);
    // *
    // * if (new BigDecimal(0.1).compareTo(diffEuro) <= 0) { marketTaxEuro =
    // * diffEuro;
    // * tmct0bok.setImimpeur(marketTaxEuro.add(tmct0bok.getImimpeur()));
    // *
    // * }
    // */
    //
    // }

    // calcular bruto
    this.calcularBrutoMercadoAlloAcumulado(booking, mBooking);
  }

  private void calcularBrutoMercadoAlloAcumulado(BookingType booking, Map<String, Map<String, Object>> mBooking) {
    Map<String, Object> mAllocation;
    BigDecimal brutoMercadoAcumulado;
    // calcular bruto
    for (ExecutionGroupType executionsGroup : booking.getExecutionGroups()) {

      for (AllocationID allocationId : executionsGroup.getAllocation()) {
        double nutitagr = 0;
        for (ExecutionID executionId : allocationId.getExecutions()) {
          nutitagr += executionId.getNumtitasig();
        }
        mAllocation = mBooking.get(allocationId.getNucnfclt());

        if ((brutoMercadoAcumulado = (BigDecimal) mAllocation.get("brutoMercadoAlloAcumulado")) == null) {
          brutoMercadoAcumulado = BigDecimal.ZERO;
        }
        brutoMercadoAcumulado = brutoMercadoAcumulado.add(BigDecimal.valueOf(nutitagr)
            .multiply(BigDecimal.valueOf(executionsGroup.getPrecioBrutoMkt())).setScale(2, RoundingMode.HALF_UP));
        mAllocation.put("brutoMercadoAlloAcumulado", brutoMercadoAcumulado);
      }
    }
  }

  private void persistsAllCascade(Tmct0bok tmct0bok) {

    List<Tmct0gal> gals;
    List<Tmct0gae> gaes;
    Tmct0ejeFeeSchema feeSchema;
    List<Tmct0grupoejecucion> grs;
    List<Tmct0ejecucionalocation> ejealos;
    List<Tmct0aloDynamicValues> aloDynamicValues;
    List<Tmct0ejeDynamicValues> ejeDynamicValues;
    List<Tmct0eje> ejes = new ArrayList<Tmct0eje>(tmct0bok.getTmct0ejes());
    List<Tmct0alo> alos = new ArrayList<Tmct0alo>(tmct0bok.getTmct0alos());
    List<Tmct0gre> gres = new ArrayList<Tmct0gre>(tmct0bok.getTmct0gres());
    List<Tmct0bokDynamicValues> bokDynamicValues = new ArrayList<Tmct0bokDynamicValues>(tmct0bok.getDynamicValues());

    tmct0bok.getTmct0ejes().clear();
    tmct0bok.getTmct0alos().clear();
    tmct0bok.getTmct0gres().clear();
    tmct0bok.getDynamicValues().clear();

    tmct0bok = bokBo.save(tmct0bok);
    bokDynamicValues = tmct0bokDynamicValuesDao.save(bokDynamicValues);
    tmct0bok.getDynamicValues().addAll(bokDynamicValues);
    bokDynamicValues.clear();

    for (Tmct0eje eje : ejes) {
      feeSchema = eje.getTmct0ejeFeeSchema();

      eje.setTmct0ejeFeeSchema(null);
      ejeDynamicValues = new ArrayList<Tmct0ejeDynamicValues>(eje.getDynamicValues());
      eje.getDynamicValues().clear();
      eje = ejeBo.save(eje);
      ejeDynamicValues = tmct0ejeDynamicValuesDao.save(ejeDynamicValues);
      eje.getDynamicValues().addAll(ejeDynamicValues);
      ejeDynamicValues.clear();
      if (feeSchema != null) {
        feeSchema.setTmct0eje(eje);
        feeSchema = tmct0ejeFeeSchemaDao.save(feeSchema);
        eje.setTmct0ejeFeeSchema(feeSchema);
      }
    }
    tmct0bok.getTmct0ejes().addAll(ejes);
    ejes.clear();

    for (Tmct0alo tmct0alo : alos) {
      grs = new ArrayList<Tmct0grupoejecucion>(tmct0alo.getTmct0grupoejecucions());
      aloDynamicValues = new ArrayList<Tmct0aloDynamicValues>(tmct0alo.getDynamicValues());
      tmct0alo.getDynamicValues().clear();
      tmct0alo.getTmct0grupoejecucions().clear();
      tmct0alo = aloBo.save(tmct0alo);
      aloDynamicValues = tmct0aloDynamicValuesDao.save(aloDynamicValues);
      tmct0alo.getDynamicValues().addAll(aloDynamicValues);
      aloDynamicValues.clear();
      for (Tmct0grupoejecucion gr : grs) {
        ejealos = new ArrayList<Tmct0ejecucionalocation>(gr.getTmct0ejecucionalocations());
        gr.getTmct0ejecucionalocations().clear();

        gr = grBo.save(gr);
        ejealos = ejeAloBo.save(ejealos);
        gr.getTmct0ejecucionalocations().addAll(ejealos);
        ejealos.clear();

      }
      tmct0alo.getTmct0grupoejecucions().addAll(grs);
      grs.clear();
    }

    for (Tmct0gre tmct0gre : gres) {
      gals = new ArrayList<Tmct0gal>(tmct0gre.getTmct0gals());

      tmct0gre.getTmct0gals().clear();
      tmct0gre = greDao.save(tmct0gre);
      for (Tmct0gal tmct0gal : gals) {
        gaes = new ArrayList<Tmct0gae>(tmct0gal.getTmct0gaes());
        tmct0gal.getTmct0gaes().clear();
        galDao.save(tmct0gal);
        gaes = gaeDao.save(gaes);
        tmct0gal.getTmct0gaes().addAll(gaes);
      }
      tmct0gre.getTmct0gals().addAll(gals);
    }
    tmct0bok.getTmct0gres().addAll(gres);

  }

  private BigDecimal setValue(Map<String, Object> mAllocation, String key, double value) {
    BigDecimal bD = BigDecimal.valueOf(value).setScale(8, RoundingMode.HALF_UP);
    mAllocation.put(key, bD);
    return bD;
  }

  private void setRestValue(Map<String, Object> map, String key, double value) {
    map.put(key + "_rest", setValue(map, key, value));
  }

  private void mappDynamicValues(BookingType booking, Tmct0bok tmct0bok) {
    Tmct0bokDynamicValues dynamic;
    Tmct0bokDynamicValuesId id;
    if (booking.getDynamicValues() != null && booking.getDynamicValues().getDynamicValues() != null) {
      for (DynamicValue value : booking.getDynamicValues().getDynamicValues()) {
        id = new Tmct0bokDynamicValuesId(tmct0bok.getId().getNuorden(), tmct0bok.getId().getNbooking(), value.getName());
        dynamic = new Tmct0bokDynamicValues();
        dynamic.setId(id);
        LOG.trace("[mappDynamicValues] recepcion dynamic value id: {} valor:{}-{}", id, value.getValue(),
            value.getValueSt());
        if (value.getValue() != null) {
          dynamic.setValor(BigDecimal.valueOf(value.getValue()));
        }
        dynamic.setValorSt(value.getValueSt());
        dynamic.setAuditDate(new java.util.Date());
        dynamic.setAuditUser("SIBBAC20");
        tmct0bok.getDynamicValues().add(dynamic);
      }
    }
  }
}
