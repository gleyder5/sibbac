package sibbac.business.comunicaciones.euroccp.titulares.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpOwnershipReportingGrossExecutions;

@Component
public interface EuroCcpOwnershipReportingGrossExecutionsDao extends
                                                            JpaRepository<EuroCcpOwnershipReportingGrossExecutions, Long> {

  @Query("SELECT DISTINCT e.tradeDate FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.estadoProcesado = :pEstadoProcesado")
  List<Date> findAllDateByEstadoProcesado(@Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT DISTINCT e.tradeDate FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.tradeDate >= :pTradeDateGte AND e.estadoProcesado = :pEstadoProcesado")
  List<Date> findAllDateByEstadoProcesado(@Param("pTradeDateGte") Date tradeDateGte,
                                          @Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT DISTINCT new sibbac.business.comunicaciones.euroccp.ciffile.dto.EuroCcpIdDTO(e.desgloseCamaraEnvioRt.desgloseCamara.iddesglosecamara) "
         + " FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.tradeDate = :pTradeDate AND e.estadoProcesado = :pEstadoProcesado "
         + " AND e.desgloseCamaraEnvioRt.desgloseCamara.tmct0estadoByCdestadoasig.idestado >= :pcdestadoasigGte ")
  List<EuroCcpIdDTO> findAllIdDesglosecamaraByTradeDateAndEstadoProcesado(@Param("pTradeDate") Date tradeDate,
                                                                          @Param("pEstadoProcesado") int estadoProcesado,
                                                                          @Param("pcdestadoasigGte") int cdestadoasigGte);
  


  @Query("SELECT e FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.desgloseCamaraEnvioRt.desgloseCamara.iddesglosecamara = :pIddesgloseCamra "
         + " AND e.estadoProcesado = :pEstadoProcesado")
  List<EuroCcpOwnershipReportingGrossExecutions> findAllByIdDesglosecamraAndEstadoProcesado(@Param("pIddesgloseCamra") long iddesglosecamara,
                                                                                            @Param("pEstadoProcesado") int estadoProcesado);

  @Query("SELECT e FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.tradeDate = :pTradeDate "
         + " AND e.executionReference = :pExecutionReference AND e.mic = :pMic AND e.accountNumber = :pAccountNumber "
         + " AND e.fileSent.parentFile.estadoProcesado >= :pEstadoProcesadoGte  AND e.numberSharesAvailable > :pNumberSharesAvailable "
         + " ORDER BY e.numberSharesAvailable DESC, e.id ASC ")
  List<EuroCcpOwnershipReportingGrossExecutions> findAllByTradeDateAndExecutionReferenceAndMicAndAccountNumberAndEstadoProcesadoGtAndNumberSharesAvailableGte(@Param("pTradeDate") Date tradeDate,
                                                                                                                                                              @Param("pExecutionReference") String executionReference,
                                                                                                                                                              @Param("pMic") String mic,
                                                                                                                                                              @Param("pAccountNumber") String accountNumber,
                                                                                                                                                              @Param("pEstadoProcesadoGte") int estadoProcesadoe,
                                                                                                                                                              @Param("pNumberSharesAvailable") BigDecimal numberSharesAvailable);

  @Query("SELECT e FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.tradeDate = :pTradeDate "
         + " AND e.executionReference = :pExecutionReference AND e.mic = :pMic AND e.accountNumber = :pAccountNumber "
         + " AND e.fileSent.parentFile.id = :idPrincipalFileSent "
         + " AND e.ownerReferenceFrom = :pownerReferenceFrom AND e.ownerReferenceTo = :pownerReferenceTo "
         + " AND e.numberShares = :pNumberShares  ")
  EuroCcpOwnershipReportingGrossExecutions findToReceiveTitularesProcess(@Param("pTradeDate") Date tradeDate,
                                                                         @Param("pExecutionReference") String executionReference,
                                                                         @Param("pMic") String mic,
                                                                         @Param("pAccountNumber") String accountNumber,
                                                                         @Param("pownerReferenceFrom") String ownerReferenceFrom,
                                                                         @Param("pownerReferenceTo") String ownerReferenceTo,
                                                                         @Param("pNumberShares") BigDecimal numberShares,
                                                                         @Param("idPrincipalFileSent") Long idPrincipalFileSent);

  @Query("SELECT e.id FROM EuroCcpOwnershipReportingGrossExecutions e WHERE e.fileSent.id = :idSentFile ")
  List<Long> findAllIdsByIdSentFile(@Param("idSentFile") long idSentFile);

  @Modifying
  @Query("UPDATE EuroCcpOwnershipReportingGrossExecutions e SET e.estadoProcesado = :pEstadoProcesado, e.auditDate = :pauditDate, e.auditUser = :pauditUser "
         + " WHERE e.id = :pid ")
  int updateEstadoProcesadoById(@Param("pEstadoProcesado") int estadoProcesado,
                                @Param("pauditDate") Date auditDate,
                                @Param("pauditUser") String auditUser,
                                @Param("pid") long id);

  @Modifying
  @Query("UPDATE EuroCcpOwnershipReportingGrossExecutions e SET e.estadoProcesado = :pEstadoProcesado, e.auditDate = :pauditDate, e.auditUser = :pauditUser "
         + " WHERE e.id = :idSentFileLine ")
  int updateEstadoProcesadoByIdFileSentLine(@Param("pEstadoProcesado") int estadoProcesado,
                                            @Param("pauditDate") Date auditDate,
                                            @Param("pauditUser") String auditUser,
                                            @Param("idSentFileLine") long idSentFileLine);

  @Modifying
  @Query("UPDATE EuroCcpOwnershipReportingGrossExecutions e SET e.estadoProcesado = :pEstadoProcesado, e.auditDate = :pauditDate, e.auditUser = :pauditUser "
         + " WHERE e.fileSent.id = :idSentFile ")
  int updateEstadoProcesadoByIdFileSent(@Param("pEstadoProcesado") int estadoProcesado,
                                        @Param("pauditDate") Date auditDate,
                                        @Param("pauditUser") String auditUser,
                                        @Param("idSentFile") long idSentFile);

  @Modifying
  @Query("UPDATE EuroCcpOwnershipReportingGrossExecutions e SET e.estadoProcesado = :pEstadoProcesado, e.auditDate = :pauditDate, e.auditUser = :pauditUser "
         + " WHERE e.fileSent.parentFile.id = :idSentFile ")
  int updateEstadoProcesadoByIdParentFileSent(@Param("pEstadoProcesado") int estadoProcesado,
                                              @Param("pauditDate") Date auditDate,
                                              @Param("pauditUser") String auditUser,
                                              @Param("idSentFile") long idSentFile);

}
