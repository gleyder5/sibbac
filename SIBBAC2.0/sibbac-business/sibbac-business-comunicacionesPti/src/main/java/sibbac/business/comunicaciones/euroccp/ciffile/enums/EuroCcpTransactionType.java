package sibbac.business.comunicaciones.euroccp.ciffile.enums;

public enum EuroCcpTransactionType {

  STANDARD("STD"),
  INTERNAL_MATCHED_TRADE("IMT");
  public String text;

  private EuroCcpTransactionType(String text) {
    this.text = text;
  }
}
