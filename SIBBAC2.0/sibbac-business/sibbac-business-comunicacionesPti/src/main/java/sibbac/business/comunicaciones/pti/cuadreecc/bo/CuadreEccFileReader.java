package sibbac.business.comunicaciones.pti.cuadreecc.bo;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.comunicaciones.pti.commons.PtiMessageFileReader;
import sibbac.business.comunicaciones.pti.commons.PtiMessageFileReaderLineMapper;
import sibbac.business.comunicaciones.pti.commons.dto.PtiMessageFileReaderDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.ControlRevisionTitularesIFDTO;
import sibbac.business.comunicaciones.pti.cuadreecc.runnable.CuadreEccFileReaderRunnableBean;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.an.AN;

@Service(value = "cuadreEccFileReader")
public class CuadreEccFileReader extends PtiMessageFileReader {

  private static final Logger LOG = LoggerFactory.getLogger(CuadreEccFileReader.class);

  @Value("${sibbac.cuadreecc.file.reader.transaction.size:100}")
  private int transactionSize;

  @Value("${sibbac.cuadreecc.file.reader.thread.pool.size:25}")
  private int threadSize;

  @Value("${sibbac.cuadreecc.file.reader.max.beans.in.transaction:150}")
  private int maxRegInTransaction;

  @Qualifier(value = "cuadreEccFileReaderRunnableBean")
  @Autowired
  private CuadreEccFileReaderRunnableBean cuadreEccRunnableBean;

  @Autowired
  public CuadreEccFileReader(PtiMessageFileReaderLineMapper ptiLineMapper) {
    super(ptiLineMapper);
    setObserver(new ControlRevisionTitularesIFDTO());
  }

  protected boolean isDominantDataBean(PtiMessageFileReaderDTO bean) {
    if (bean.getMsg().getTipo() == PTIMessageType.AN) {
      return true;
    }
    else {
      return false;
    }

  }

  @Override
  @Transactional
  public void executeTask() throws Exception {
    LOG.trace("[CuadreEccFileReader :: executeTask] inicio");
    setObserver(new ControlRevisionTitularesIFDTO());
    if (Files.exists(this.file)) {
      if (checkTestigo(this.hasTestigo)) {
        String originalFileName = this.file.toString();
        String procesandoFileName = this.file.toString().substring(0, this.file.toString().indexOf("."))
            + ".procesando";
        Path procesandoFile = Paths.get(procesandoFileName);
        if (!Files.exists(procesandoFile)) {
          this.file = Files.move(this.file, procesandoFile, StandardCopyOption.ATOMIC_MOVE);
        }
        else {
          throw new SIBBACBusinessException("Encontrado fichero : " + procesandoFileName + " ya en el filesystem.");
        }
        openFile(originalFileName);

        try {
          executeTask2();
          if (this.hasTestigo) {
            Files.deleteIfExists(testigo);
          }
        }
        catch (Exception e) {
          throw e;
        }
        finally {
          closeFile();
        }
      }
      else {
        LOG.warn("[CuadreEccFileReader :: executeTask] Testigo no encontrado : {}", this.testigo.toString());
      }
    }
    else {
      LOG.info("[CuadreEccFileReader :: executeTask] Fichero no encontrado : {}", this.file.toString());
    }
    LOG.trace("[CuadreEccFileReader :: executeTask] fin");
  }

  @SuppressWarnings("unchecked")
  private void executeTask2() throws SIBBACBusinessException {
    getObserver().setException(null);
    preExecuteTask();

    LOG.trace("[CuadreEccFileReader :: executeTask] Init ");
    ObserverProcess myObserver;
    try {
      List<PtiMessageFileReaderDTO> processBeanListTransaction = new ArrayList<PtiMessageFileReaderDTO>();
      List<PtiMessageFileReaderDTO> dominantBeanList = new ArrayList<PtiMessageFileReaderDTO>();
      List<PtiMessageFileReaderDTO> slaveBeanList = new ArrayList<PtiMessageFileReaderDTO>();

      PtiMessageFileReaderDTO dataBean;
      PtiMessageFileReaderDTO previousDataBean = null;

      int countLines = 0;
      int countBlocks = 0;
      int countBlocksMaxRegIntransaction = 0;

      IRunnableBean<?> processBean = getBeanToProcessBlock();
      processBean.preRun(getObserver());

      while (hasNextObjectToProcess()) {
        countLines++;
        dataBean = getNextObjectToProcess(countLines);
        if (dataBean != null) {

          if (!isInBlock(previousDataBean, dataBean)) {// llega un an en el data
                                                       // bean.
            countBlocks++;
            countBlocksMaxRegIntransaction = 0;
            processBeanListTransaction.addAll(dominantBeanList);
            processBeanListTransaction.addAll(slaveBeanList);
            dominantBeanList.clear();
            slaveBeanList.clear();

            dominantBeanList.add(dataBean);
            if (countBlocks % getTransactionSize() == 0) {

              if (CollectionUtils.isNotEmpty(processBeanListTransaction)) {
                ejecutarBloquesTransaccionales(processBean, processBeanListTransaction, countLines, countBlocks);
                processBeanListTransaction = new ArrayList<PtiMessageFileReaderDTO>();

              }
            }

          }// fin no esta en bloquee
          else {// llega un TI
            slaveBeanList.add(dataBean);
            if (slaveBeanList.size() > maxRegInTransaction) {
              countBlocksMaxRegIntransaction++;
              LOG.debug("[CuadreEccFileReader :: executeTask] demasiados beans para la transaccion debemos separarlos");
              processBeanListTransaction.addAll(dominantBeanList);
              processBeanListTransaction.addAll(slaveBeanList);
              slaveBeanList.clear();

              myObserver = getObserver();

              if (countBlocksMaxRegIntransaction == 1) {
                myObserver = new ControlRevisionTitularesIFDTO();
                ((ControlRevisionTitularesIFDTO) myObserver)
                    .setIdentificaciones(((ControlRevisionTitularesIFDTO) getObserver()).getIdentificaciones());
              }

              ejecutarBloquesTransaccionales(myObserver, processBean, processBeanListTransaction, countLines,
                  countBlocks);
              processBeanListTransaction = new ArrayList<PtiMessageFileReaderDTO>();

              if (countBlocksMaxRegIntransaction == 1) {
                closeExecutor();

              }// si es el primer hilo del lote
            }
          }

        }
        previousDataBean = dataBean;
      }// fin while

      if (CollectionUtils.isNotEmpty(dominantBeanList) && CollectionUtils.isNotEmpty(slaveBeanList)) {
        processBeanListTransaction.addAll(dominantBeanList);
        processBeanListTransaction.addAll(slaveBeanList);
        dominantBeanList.clear();
        slaveBeanList.clear();
      }

      if (CollectionUtils.isNotEmpty(processBeanListTransaction)) {
        LOG.trace("[CuadreEccFileReader :: executeTask] countLines :: {} ## diferent blocks :: {}", countLines,
            countBlocks);

        ejecutarBloquesTransaccionales(processBean, processBeanListTransaction, countLines, countBlocks);

        processBeanListTransaction = new ArrayList<PtiMessageFileReaderDTO>();
      }

      processBean.postRun(getObserver());

    }
    catch (Exception e) {
      closeExecutorNow();
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
    finally {

      closeExecutor();
      postExecuteTask();
    }
    LOG.trace("[CuadreEccFileReader :: executeTask] End ");
  }

  // NO PUEDO UTILIZARLO YA QUE EL PROCESO DE CARGA DE FICHERO CUANDO DETECTA UN
  // AN BORRA ESE CODIGO DE OPERACION EN LAS
  // TABLAS
  // @SuppressWarnings({ "unchecked", "rawtypes"
  // })
  // @Override
  // public List<List<PtiMessageFileReaderDTO>> crearBloquesTransaccionales(List
  // dataBeanList) {
  // LOG.debug("[RevisionTitularesPtiFileReader :: crearBloquesTransaccionales] inicio");
  // List<List<PtiMessageFileReaderDTO>> res = new
  // ArrayList<List<PtiMessageFileReaderDTO>>();
  // if (CollectionUtils.isNotEmpty(dataBeanList)) {
  // if (dataBeanList.size() > maxRegInTransaction) {
  //
  // List<PtiMessageFileReaderDTO> bloquesTransaccionales = new
  // ArrayList<PtiMessageFileReaderDTO>();
  // // List<PtiMessageFileReaderDTO> bloquesTransaccionalesDefinitivos = new
  // ArrayList<PtiMessageFileReaderDTO>();
  // List<List<PtiMessageFileReaderDTO>> bloquesAgrupados = new
  // ArrayList<List<PtiMessageFileReaderDTO>>();
  // PtiMessageFileReaderDTO bean = null;
  // PtiMessageFileReaderDTO prevBean = null;
  //
  // for (Object obj : dataBeanList) {
  // bean = (PtiMessageFileReaderDTO) obj;
  // if (!isInBlock(prevBean, bean)) {
  // bloquesAgrupados.add(bloquesTransaccionales);
  // bloquesTransaccionales = new ArrayList<PtiMessageFileReaderDTO>();
  //
  // }
  // bloquesTransaccionales.add(bean);
  // prevBean = bean;
  // }// fin for
  //
  // for (List<PtiMessageFileReaderDTO> bloquePreTransaccional :
  // bloquesAgrupados) {
  //
  // if (bloquePreTransaccional.size() > maxRegInTransaction) {
  // PtiMessageFileReaderDTO an = bloquePreTransaccional.get(0);
  // int posIni = 1;
  // int posEnd = posIni;
  // int size = bloquePreTransaccional.size();
  // while (posEnd < size) {
  // posEnd += transactionSize;
  // if (posEnd > size) {
  // posEnd = size;
  // }
  // bloquesTransaccionales = new ArrayList<PtiMessageFileReaderDTO>();
  // bloquesTransaccionales.add(an);
  // bloquesTransaccionales.addAll(bloquePreTransaccional.subList(posIni,
  // posEnd));
  // LOG.debug("[RevisionTitularesPtiFileReader :: crearBloquesTransaccionales] Aniadido bloque de {} beans",
  // bloquesTransaccionales.size());
  // res.add(bloquesTransaccionales);
  // posIni = posEnd;
  // }
  //
  // } else {
  // res.add(bloquePreTransaccional);
  // LOG.debug("[RevisionTitularesPtiFileReader :: crearBloquesTransaccionales] Aniadido bloque de {} beans",
  // bloquesTransaccionales.size());
  // }
  // }
  // dataBeanList.clear();
  // } else {
  // LOG.debug("[RevisionTitularesPtiFileReader :: crearBloquesTransaccionales] Aniadido bloque de {} beans",
  // dataBeanList.size());
  // res.add(dataBeanList);
  // }
  // }
  // LOG.debug("[RevisionTitularesPtiFileReader :: crearBloquesTransaccionales] fin");
  // return res;
  // }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    LOG.trace("[RevisionTitularesPtiFileReader :: isInBlock] prev: {}, current: {}", previousBean, bean);

    if (previousBean != null && bean != null) {
      try {
        if (((PtiMessageFileReaderDTO) bean).getMsg() instanceof AN) {
          LOG.trace("[RevisionTitularesPtiFileReader :: isInBlock] Es un AN es un nuevo bloque.");
          return false;
        }
        else {
          return true;
        }
        // else if (((PtiMessageFileReaderDTO) previousBean).getMsg() instanceof
        // TI
        // && ((PtiMessageFileReaderDTO) bean).getMsg() instanceof TI) {
        // LOG.trace("[RevisionTitularesPtiFileReader :: isInBlock] Es un TI vamos a revisar si es la misa op.");
        // List<PTIMessageRecord> r03sPrevious = ((PtiMessageFileReaderDTO)
        // previousBean).getMsg()
        // .getAll(PTIMessageRecordType.R03);
        // List<PTIMessageRecord> r03s = ((PtiMessageFileReaderDTO)
        // bean).getMsg().getAll(PTIMessageRecordType.R03);
        // if (CollectionUtils.isNotEmpty(r03sPrevious) &&
        // CollectionUtils.isNotEmpty(r03s)) {
        // R03 r03Previous = (R03) r03sPrevious.get(0);
        // R03 r03 = (R03) r03s.get(0);
        // LOG.trace("[RevisionTitularesPtiFileReader :: isInBlock] op bean prev: {} op bean curr: {}",
        // r03Previous.getNumeroOperacion(), r03.getNumeroOperacion());
        //
        // return
        // r03.getNumeroOperacion().equals(r03Previous.getNumeroOperacion());
        // }
        // }
      }
      catch (Exception e) {
        LOG.warn("Inciencia", e);
      }
    }

    return false;
  }

  @Override
  public IRunnableBean<?> getBeanToProcessBlock() {
    return cuadreEccRunnableBean;
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {

  }

  @Override
  public void postExecuteTask() throws SIBBACBusinessException {

  }

}
