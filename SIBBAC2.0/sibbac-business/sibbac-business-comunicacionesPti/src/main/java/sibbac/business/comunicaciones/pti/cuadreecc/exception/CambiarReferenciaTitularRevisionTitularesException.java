package sibbac.business.comunicaciones.pti.cuadreecc.exception;

import sibbac.common.SIBBACBusinessException;

public class CambiarReferenciaTitularRevisionTitularesException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = -4353432223881137114L;

  public CambiarReferenciaTitularRevisionTitularesException() {
    // TODO Auto-generated constructor stub
  }

  public CambiarReferenciaTitularRevisionTitularesException(String message,
                                                            Throwable cause,
                                                            boolean enableSuppression,
                                                            boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    // TODO Auto-generated constructor stub
  }

  public CambiarReferenciaTitularRevisionTitularesException(String message, Throwable cause) {
    super(message, cause);
    // TODO Auto-generated constructor stub
  }

  public CambiarReferenciaTitularRevisionTitularesException(String message) {
    super(message);
    // TODO Auto-generated constructor stub
  }

  public CambiarReferenciaTitularRevisionTitularesException(Throwable cause) {
    super(cause);
    // TODO Auto-generated constructor stub
  }

}
