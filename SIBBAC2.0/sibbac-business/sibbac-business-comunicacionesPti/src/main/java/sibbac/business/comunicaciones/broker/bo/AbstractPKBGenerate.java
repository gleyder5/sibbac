package sibbac.business.comunicaciones.broker.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dao.Tmct0pkbDao;
import sibbac.business.wrappers.database.dao.Tmct0pkcDao;
import sibbac.business.wrappers.database.dao.Tmct0pkdDao;
import sibbac.business.wrappers.database.dao.Tmct0pkeDao;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBicName;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBroker;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0pkb;
import sibbac.business.wrappers.database.model.Tmct0pkc;
import sibbac.business.wrappers.database.model.Tmct0pkcId;
import sibbac.business.wrappers.database.model.Tmct0pkd;
import sibbac.business.wrappers.database.model.Tmct0pkdId;
import sibbac.business.wrappers.database.model.Tmct0pke;
import sibbac.business.wrappers.database.model.Tmct0pkeId;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

public abstract class AbstractPKBGenerate implements Serializable {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractPKBGenerate.class);

  /**
	 * 
	 */
  private static final long serialVersionUID = -1457702558531703363L;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private Tmct0pkbDao pkbDao;

  @Autowired
  private Tmct0pkcDao pkcDao;

  @Autowired
  private Tmct0pkdDao pkdDao;

  @Autowired
  private Tmct0pkeDao pkeDao;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0xasBo xasBo;

  /**
   * 
   * @param hpm
   * @param app
   */
  public AbstractPKBGenerate() {
  }

  /**
   * generateTmct0pkb Genera a partir un una lista de objetos Tmct0ctd un objeto
   * Tmct0pkb con toda su estructura
   * 
   * @param listCtdsProceso
   * @param nuagrbrk
   * @param nuagrpkb
   * @return @
   * @throws SIBBACBusinessException
   */
  protected Tmct0pkb generateTmct0pkb(List<Tmct0ctd> listCtdsProceso, List<Tmct0ctd> listTodosCtds, Integer nuagrbrk,
      Integer nuagrpkb, boolean group, String cdusuaud) throws SIBBACBusinessException {

    BigDecimal fetradeBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils
        .convertDateToString(listCtdsProceso.get(0).getTmct0cta().getTmct0ctg().getFetrade()));

    LOG.debug("[generateTmct0pkb] fetrade: {}", fetradeBd);
    Tmct0pkb tmct0pkb = new Tmct0pkb();

    tmct0pkb.setPkbroker(getPkbroker());
    tmct0pkb.setNuagrpkb(nuagrpkb);
    tmct0pkb.setCdmercad(listCtdsProceso.get(0).getId().getCdmercad());
    tmct0pkb.setCdbroker(listCtdsProceso.get(0).getId().getCdbroker());
    tmct0pkb.setCdtpoper(listCtdsProceso.get(0).getTmct0cta().getTmct0ctg().getCdtpoper());
    tmct0pkb.setCdisin(listCtdsProceso.get(0).getTmct0cta().getTmct0ctg().getCdisin());
    tmct0pkb.setNbvalors(listCtdsProceso.get(0).getTmct0cta().getTmct0ctg().getNbvalors());
    tmct0pkb.setCdstcexc(listCtdsProceso.get(0).getCdstcexc());
    tmct0pkb.setCdmoniso(listCtdsProceso.get(0).getCdmoniso());
    tmct0pkb.setFetrade(listCtdsProceso.get(0).getTmct0cta().getTmct0ctg().getFetrade());
    tmct0pkb.setFevalor(listCtdsProceso.get(0).getFevalor());
    tmct0pkb.setInforcon(listCtdsProceso.get(0).getTmct0cta().getInforcon());
    tmct0pkb.setNuagrbrk(nuagrbrk);
    tmct0pkb.setCdcleare(listCtdsProceso.get(0).getCdcleare().trim());
    tmct0pkb.setAcctatcle(listCtdsProceso.get(0).getAcctatcle().trim());
    tmct0pkb.setDscleare("");
    tmct0pkb.setTpsetcle(listCtdsProceso.get(0).getTpsetcle().trim());
    LOG.debug("[generateTmct0pkb] buscando clearer SettlementChainBicName : {}", listCtdsProceso.get(0).getLclecode());
    SettlementChainBicName settlementChainClient = settlementDataBo.getSettlementChainBicName(listCtdsProceso.get(0)
        .getLclecode(), fetradeBd.intValue());
    // local custodian
    tmct0pkb.setLcusbic(settlementChainClient.getBic().trim());
    tmct0pkb.setCdloccus(settlementChainClient.getName().trim());
    tmct0pkb.setAcloccus(listCtdsProceso.get(0).getLcleacct().trim());
    tmct0pkb.setIdloccus(listCtdsProceso.get(0).getLcleid().trim());
    // global custodian
    LOG.debug("[generateTmct0pkb] buscando custodio SettlementChainBicName : {}", listCtdsProceso.get(0).getGclecode());
    settlementChainClient = settlementDataBo.getSettlementChainBicName(listCtdsProceso.get(0).getGclecode(),
        fetradeBd.intValue());
    tmct0pkb.setGcusbic(settlementChainClient.getBic().trim());
    tmct0pkb.setCdglocus(settlementChainClient.getName().trim());
    tmct0pkb.setAcglocus(listCtdsProceso.get(0).getGcleacct().trim());
    tmct0pkb.setIdglocus(listCtdsProceso.get(0).getGcleid().trim());
    // beneficiario custodian
    LOG.debug("[generateTmct0pkb] buscando beneficiario SettlementChainBicName : {}", listCtdsProceso.get(0)
        .getCdbencle());
    settlementChainClient = settlementDataBo.getSettlementChainBicName(listCtdsProceso.get(0).getCdbencle(),
        fetradeBd.intValue());
    tmct0pkb.setCdbenbic(settlementChainClient.getBic().trim());
    // 2015-08-20
    // Se añade
    String cdbenefi = settlementChainClient.getName();
    if (cdbenefi == null)
      cdbenefi = "";
    else
      cdbenefi = cdbenefi.trim();
    tmct0pkb.setCdbenefi(cdbenefi);
    tmct0pkb.setAcbenefi(listCtdsProceso.get(0).getAcbencle().trim());
    tmct0pkb.setIdbenefi(listCtdsProceso.get(0).getIdbencle().trim());

    SettlementChainBroker settlementChainBroker = settlementDataBo.getSettlementChainBrokerInternacional(
        listCtdsProceso.get(0).getId().getCdbroker(), listCtdsProceso.get(0).getId().getCdmercad(), listCtdsProceso
            .get(0).getTpsetcle(), fetradeBd);

    tmct0pkb.setCdauxili(settlementChainBroker.getBtpsettle());
    // local custodian
    tmct0pkb.setBcdloccus(settlementChainBroker.getBcdloccus());
    tmct0pkb.setBlcusbic(settlementChainBroker.getBlcusbic());
    tmct0pkb.setBacloccus(settlementChainBroker.getBacloccus());
    tmct0pkb.setBidloccus(settlementChainBroker.getBidloccus());
    // global custodian
    tmct0pkb.setBcdglocus(settlementChainBroker.getBcdglocus());
    tmct0pkb.setBgcusbic(settlementChainBroker.getBgcusbic());
    tmct0pkb.setBacglocus(settlementChainBroker.getBacglocus());
    tmct0pkb.setBidglocus(settlementChainBroker.getBidglocus());
    // beneficiario
    tmct0pkb.setBcdbenefi(settlementChainBroker.getBcdbenefi());
    tmct0pkb.setBcdbenbic(settlementChainBroker.getBcdbenbic());
    tmct0pkb.setBacbenefi(settlementChainBroker.getBacbenefi());
    tmct0pkb.setBidbenefi(settlementChainBroker.getBidbenefi());

    // place of settlement
    tmct0pkb.setPccleset(settlementChainBroker.getCdbicmar());

    tmct0pkb.setInsenmod(' ');
    tmct0pkb.setFhsending(new Date(new GregorianCalendar(0001, 00, 01).getTimeInMillis()));
    tmct0pkb.setHosending(new Date(new GregorianCalendar(0001, 00, 01, 00, 00, 00).getTimeInMillis()));
    tmct0pkb.setInsenfla('N');

    tmct0pkb.setFhsenliq(new Date(new GregorianCalendar(0001, 00, 01).getTimeInMillis()));
    tmct0pkb.setHosenliq(new Date(new GregorianCalendar(0001, 00, 01, 00, 00, 00).getTimeInMillis()));
    tmct0pkb.setInsenliq('N');

    tmct0pkb.setPkentity("");

    tmct0pkb.setCdusuaud(cdusuaud);
    tmct0pkb.setFhaudit(new Date());

    tmct0pkb.setNuquant(BigDecimal.ZERO);
    tmct0pkb.setNuquaalo(BigDecimal.ZERO);
    tmct0pkb.setImntobrk(BigDecimal.ZERO);
    tmct0pkb.setImbrubrk(BigDecimal.ZERO);
    tmct0pkb.setImtotbru(BigDecimal.ZERO);
    tmct0pkb.setImtotnet(BigDecimal.ZERO);
    tmct0pkb.setImcombrk(BigDecimal.ZERO);
    tmct0pkb.setImmartax(BigDecimal.ZERO);
    tmct0pkb.setImmarcha(BigDecimal.ZERO);
    tmct0pkb.setImcamalo(BigDecimal.ZERO);
    tmct0pkb.setImsvb(BigDecimal.ZERO);

    /*
     * campos acumulados
     */
    for (Tmct0ctd aux : listCtdsProceso) {
      tmct0pkb.setNuquant(tmct0pkb.getNuquant().add(aux.getNutitagr()));
      tmct0pkb.setNuquaalo(tmct0pkb.getNuquaalo().add(aux.getNutitagr()));
      tmct0pkb.setImntobrk(tmct0pkb.getImntobrk().add(aux.getImnetobr()));
      tmct0pkb.setImbrubrk(tmct0pkb.getImbrubrk().add(aux.getImbrmerc()));
      tmct0pkb.setImtotbru(tmct0pkb.getImtotbru().add(aux.getImtotbru()));
      tmct0pkb.setImtotnet(tmct0pkb.getImtotnet().add(aux.getImtotnet()));
      tmct0pkb.setImcombrk(tmct0pkb.getImcombrk().add(aux.getImfibrdv()));
      tmct0pkb.setImmartax(tmct0pkb.getImmartax().add(aux.getImimpdvs()));
      tmct0pkb.setImmarcha(tmct0pkb.getImmarcha().add(aux.getImgatdvs()));
      tmct0pkb.setImcamalo(tmct0pkb.getImcamalo().add(aux.getImcamalo()));
      tmct0pkb.setImsvb(tmct0pkb.getImsvb().add(aux.getImsvb()));
      if (group) {
        tmct0pkb.setImbrapre(tmct0pkb.getImbrubrk().divide(tmct0pkb.getNuquaalo(), 4, RoundingMode.HALF_UP));
      }
      else {
        tmct0pkb.setImbrapre(aux.getTmct0cta().getTmct0ctg().getImcbmerc());
      }
    }

    tmct0pkb.setTmct0sta(new Tmct0sta(new Tmct0staId(TypeCdestado.NEW_REGISTER.getValue(), TypeCdtipest.TMCT0PKB
        .getValue())));
    tmct0pkb = pkbDao.save(tmct0pkb);

    /*
     * generar tmct0pkc y tmct0pkd
     */
    List<Tmct0pkc> collectionTmct0pkc = new ArrayList<>();

    for (Tmct0ctd aux : listCtdsProceso) {

      Tmct0pkc tmct0pkc = null;
      if (!collectionTmct0pkc.isEmpty()) {
        Iterator<Tmct0pkc> it = collectionTmct0pkc.iterator();
        while (it.hasNext() && null == tmct0pkc) {
          Tmct0pkc itTmct0pkc = (Tmct0pkc) it.next();
          if (aux.getTmct0cta().getId().getNbooking().trim().equals(itTmct0pkc.getId().getNbooking().trim())) {
            tmct0pkc = itTmct0pkc;
          }
        }
      }

      if (null == tmct0pkc) {
        tmct0pkc = new Tmct0pkc();
        tmct0pkc.setId(new Tmct0pkcId());
        tmct0pkc.setTmct0pkb(tmct0pkb);
        tmct0pkc.getId().setPkbroker(tmct0pkb.getPkbroker());
        tmct0pkc.getId().setNuorden(aux.getTmct0cta().getId().getNuorden());
        tmct0pkc.getId().setNbooking(aux.getTmct0cta().getId().getNbooking());
        tmct0pkc.getId().setCdbroker(aux.getTmct0cta().getId().getCdbroker());
        tmct0pkc.getId().setCdmercad(aux.getTmct0cta().getId().getCdmercad());
        tmct0pkc.setDscript("");
        tmct0pkc.setCdusuaud(tmct0pkb.getCdusuaud());
        tmct0pkc.setFhaudit(tmct0pkb.getFhaudit());
        tmct0pkc = pkcDao.save(tmct0pkc);
      }

      // recuperar de bbdd
      List<Tmct0pkd> collectionTmct0pkd = tmct0pkc.getTmct0pkds();
      Tmct0pkd tmct0pkd = null;
      
      if (!collectionTmct0pkd.isEmpty()) {
        Iterator<Tmct0pkd> itTmct0pkd = collectionTmct0pkd.iterator();
        while (itTmct0pkd.hasNext() && null == tmct0pkd) {
          Tmct0pkd it2 = (Tmct0pkd) itTmct0pkd.next();
          if (aux.getTmct0cta().getId().getNucnfclt().trim().equals(it2.getId().getNucnfclt().trim())) {
            tmct0pkd = it2;
          }
        }
      }

      if (null == tmct0pkd) {
        tmct0pkd = new Tmct0pkd();
        tmct0pkd.setId(new Tmct0pkdId());
        tmct0pkd.getId().setPkbroker(tmct0pkb.getPkbroker());
        tmct0pkd.getId().setNuorden(aux.getTmct0cta().getId().getNuorden());
        tmct0pkd.getId().setNbooking(aux.getTmct0cta().getId().getNbooking());
        tmct0pkd.getId().setCdbroker(aux.getTmct0cta().getId().getCdbroker());
        tmct0pkd.getId().setCdmercad(aux.getTmct0cta().getId().getCdmercad());
        tmct0pkd.getId().setNucnfclt(aux.getTmct0cta().getId().getNucnfclt());
        tmct0pkd.setTmct0pkc(tmct0pkc);
        tmct0pkd.setPkentity("");

        tmct0pkd.setDscript("");
        tmct0pkd.setCdusuaud(tmct0pkb.getCdusuaud());
        tmct0pkd.setFhaudit(tmct0pkb.getFhaudit());
        tmct0pkd = pkdDao.save(tmct0pkd);
        collectionTmct0pkd.add(tmct0pkd);
      }

      List<Tmct0pke> collectionTmct0pke = tmct0pkd.getTmct0pkes();

      Tmct0pke tmct0pke = new Tmct0pke();
      tmct0pke.setId(new Tmct0pkeId());
      tmct0pke.getId().setPkbroker(tmct0pkb.getPkbroker());
      tmct0pke.getId().setNuorden(aux.getTmct0cta().getId().getNuorden());
      tmct0pke.getId().setNbooking(aux.getTmct0cta().getId().getNbooking());
      tmct0pke.getId().setCdbroker(aux.getTmct0cta().getId().getCdbroker());
      tmct0pke.getId().setCdmercad(aux.getTmct0cta().getId().getCdmercad());
      tmct0pke.getId().setNucnfclt(aux.getTmct0cta().getId().getNucnfclt());
      tmct0pke.getId().setNualose1(aux.getId().getNualosec());
      tmct0pke.setTmct0pkd(tmct0pkd);

      tmct0pke.setDscript("");
      tmct0pke.setLinmsd(BigDecimal.ZERO);
      tmct0pke.setCdusuaud(tmct0pkb.getCdusuaud());
      tmct0pke.setFhaudit(tmct0pkb.getFhaudit());
      tmct0pke = pkeDao.save(tmct0pke);
      collectionTmct0pke.add(tmct0pke);
      tmct0pkd.setTmct0pkes(collectionTmct0pke);

      tmct0pkc.setTmct0pkds(collectionTmct0pkd);
      collectionTmct0pkc.add(tmct0pkc);
    }

    tmct0pkb.setTmct0pkcs(collectionTmct0pkc);

    return tmct0pkb;
  }

  /**
   * 
   * @return
   */
  protected final Integer getNuagrbrk() {
    return pkbDao.getNewNuagrbrk();
  }

  /**
   * 
   * @return
   */
  protected final Integer getPkbroker() {
    return pkbDao.getNewPkbroker();
  }

  /**
   * 
   * @param listaCtdsProceso
   * @return @
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  protected final Integer getNuagrpkb(List<Tmct0ctd> listaCtdsProceso, List<Tmct0ctd> listaTodosCtds) {

    Integer nuagrpkb = null;
    Collection<Tmct0bok> collection = this.getRelationCollection(listaCtdsProceso);
    for (Tmct0bok tmct0bok : collection) {
      if (null != tmct0bok.getNuagrpkb() && tmct0bok.getNuagrpkb() != 0) {
        if (nuagrpkb == null) {
          nuagrpkb = tmct0bok.getNuagrpkb();
          // Si hay más de un nuagrpkb, añade a la lista de ctd
          // los
          // Que contengan los pkb con ese nuagrpkb y al pkb le
          // asigna el nuagrpkb
        }
        else if (nuagrpkb.compareTo(tmct0bok.getNuagrpkb()) != 0) {
          listaTodosCtds.addAll(pkbDao.findAllTmct0ctdByNuagrpkb(tmct0bok.getNuagrpkb()));
        }
      }
    }

    if (null == nuagrpkb) {
      nuagrpkb = pkbDao.getNewNuagrpkb();
    }
    return nuagrpkb;

  }

  protected Collection<Tmct0bok> getRelationCollection(List<Tmct0ctd> ctds) {

    Collection<Tmct0bok> boks = new HashSet<Tmct0bok>();
    Collection<Tmct0bokId> boksId = new HashSet<Tmct0bokId>();
    Tmct0bokId bokId;
    for (Tmct0ctd tmct0ctd : ctds) {
      bokId = new Tmct0bokId(tmct0ctd.getId().getNbooking(), tmct0ctd.getId().getNuorden());
      if (boksId.add(bokId)) {
        boks.add(bokBo.findById(bokId));
      }
    }
    return boks;
  }

  /**
   * @
   * 
   */
  protected abstract List<Tmct0pkb> runProcess(List<Tmct0ctd> ctds) throws SIBBACBusinessException;

  protected boolean chkStatus(Collection<Tmct0ctd> listTmct0ctd, TypeCdestado typeCdestado) {

    for (Tmct0ctd tmct0ctd : listTmct0ctd) {
      if (!tmct0ctd.getTmct0sta().getId().getCdestado().trim().equals(typeCdestado.getValue())) {
        return false;
      }
    }

    return true;
  }

  /**
   * Ver si Mercado/Broker para desglose PKB's por Propio / Terceros </br>
   * 
   * select inforas4 from bsnbpsql.tmct0xas where nbcamxml='GENPKB' and
   * inforxMl=CDMERCAD-CDBROKER and nbcamas4='DESGLOSE';
   * 
   * @param cdmercad mercado a verificar
   * @param cdbroker broker a verificar
   * @return false - No Desglose para PKB's "N"/blancos/no existe true - Si se
   * Desglosa Pkb's "S"
   */
  public boolean pkbDesglosado(String cdmercado, String cdbroker) {
    // Comprobar Mercado-Broker para proceso
    boolean datos = false;

    String clave = cdmercado.trim() + "-" + cdbroker.trim();

    String inforas4 = xasBo.getGenpkbDesglose(clave);
    if ("S".equalsIgnoreCase(inforas4.trim())) {
      datos = true;
    }
    return datos;
  }
}
