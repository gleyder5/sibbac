package sibbac.business.comunicaciones.pti.ac.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.database.model.ATable;

@Table(name = "TMCT0_PTI_ENVIO_AC")
@Entity
public class PtiEnvioAc extends ATable<PtiEnvioAc> {

  /**
   * 
   */
  private static final long serialVersionUID = 3730915399457931222L;

  @Column(name = "CDREFMOVIMIENTO", length = 20, nullable = false, unique=true)
  private String cdrefmovimiento;

  @Column(name = "ACTUACION", length = 1, nullable = false)
  private char actuacion;

  @Column(name = "ESTADO_PROCESADO", nullable = false)
  private int estadoProcesado;

  @Column(name = "CDREFMOVIMIENTOECC", length = 20, nullable = false)
  private String cdrefmovimientoecc;

  @Column(name = "CDREFNOTIFICACION", length = 20, nullable = false)
  private String cdrefnotificacion;

  @Temporal(TemporalType.DATE)
  @Column(name = "FLIQUIDACION")
  private Date fliquidacion;

  @Column(name = "TITULOS", length = 18, scale = 6)
  private BigDecimal titulos;

  @Column(name = "CUENTA_COMPENSACION_DESTINO", length = 3, nullable = true)
  private String cuentaCompensacionDestino;

  @Column(name = "CODIGO_ERROR")
  private String codigoError;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_MOVIMIENTO_FIDESSA", nullable = true)
  private Tmct0movimientoeccFidessa movimientoFidessa;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDMOVIMIENTOECC", nullable = true)
  private Tmct0movimientoecc movimientoecc;

  public PtiEnvioAc() {
  }

  public String getCdrefmovimiento() {
    return cdrefmovimiento;
  }

  public char getActuacion() {
    return actuacion;
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public String getCdrefmovimientoecc() {
    return cdrefmovimientoecc;
  }

  public String getCdrefnotificacion() {
    return cdrefnotificacion;
  }

  public Date getFliquidacion() {
    return fliquidacion;
  }

  public String getCodigoError() {
    return codigoError;
  }

  public Tmct0movimientoeccFidessa getMovimientoFidessa() {
    return movimientoFidessa;
  }

  public Tmct0movimientoecc getMovimientoecc() {
    return movimientoecc;
  }

  public void setCdrefmovimiento(String cdrefmovimiento) {
    this.cdrefmovimiento = cdrefmovimiento;
  }

  public void setActuacion(char actuacion) {
    this.actuacion = actuacion;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setCdrefmovimientoecc(String cdrefmovimientoecc) {
    this.cdrefmovimientoecc = cdrefmovimientoecc;
  }

  public void setCdrefnotificacion(String cdrefnotificacion) {
    this.cdrefnotificacion = cdrefnotificacion;
  }

  public void setFliquidacion(Date fliquidacion) {
    this.fliquidacion = fliquidacion;
  }

  public void setCodigoError(String codigoError) {
    this.codigoError = codigoError;
  }

  public void setMovimientoFidessa(Tmct0movimientoeccFidessa movimientoFidessa) {
    this.movimientoFidessa = movimientoFidessa;
  }

  public void setMovimientoecc(Tmct0movimientoecc movimientoecc) {
    this.movimientoecc = movimientoecc;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

}
