package sibbac.business.comunicaciones.ordenes.dto;

import java.io.Serializable;

/**
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class Asignacion implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 692968369569914046L;

  /**
	 * 
	 */
  private transient boolean internal;

  /**
	 * 
	 */
  private Long idCuentaCompensacion;

  /**
	 * 
	 */
  private String codigoCuentaCompensacion;

  /**
	 * 
	 */
  private Long idMiembroCompensador;

  /**
	 * 
	 */
  private String nombreCompensador;

  /**
	 * 
	 */
  private String referenciaExterna;

  private String referenciaAsignacion;

  /**
	 * 
	 */
  private String referenciaCompensacion;

  /**
	 * 
	 */
  private String nemotecnico;

  private Long idNemotecnico;

  /**
   * El ID de la regla matcheada.
   */
  private Long idRegla;

  /**
   * El ID de la regla matcheada.
   */
  private String descripcionRegla;

  /**
   * El ID de la parametrización de regla utilizada.
   */
  private Long idParametrizacion;

  private String camaraCompensacion;

  private String segmento;

  private boolean imputacionS3 = false;
  private boolean imputacionPropia = false;

  /**
	 * 
	 */
  public Asignacion() {

  }

  public boolean isImputacionS3() {
    return imputacionS3;
  }

  public void setImputacionS3(boolean imputacionS3) {
    this.imputacionS3 = imputacionS3;
  }

  public boolean isImputacionPropia() {
    return imputacionPropia;
  }

  public void setImputacionPropia(boolean imputacionPropia) {
    this.imputacionPropia = imputacionPropia;
  }

  /**
   * @return
   */
  public boolean isInternal() {
    return this.internal;
  }

  /**
   * @param internal
   */
  public void setInternal(final boolean internal) {
    this.internal = internal;
  }

  /**
   * @return
   */
  public Long getIdCuentaCompensacion() {
    return this.idCuentaCompensacion;
  }

  /**
   * @param cuentaCompensacion
   */
  public void setIdCuentaCompensacion(final Long _idCuentaCompensacion) {
    this.idCuentaCompensacion = _idCuentaCompensacion;
  }

  /**
   * @return
   */
  public String getCodigoCuentaCompensacion() {
    return this.codigoCuentaCompensacion;
  }

  /**
   * @param cuentaCompensacion
   */
  public void setCodigoCuentaCompensacion(final String _codigoCuentaCompensacion) {
    this.codigoCuentaCompensacion = _codigoCuentaCompensacion;
  }

  /**
   * @return
   */
  public Long getIdMiembroCompensador() {
    return this.idMiembroCompensador;
  }

  /**
   * @param idMiembroCompensador
   */
  public void setIdMiembroCompensador(final Long idMiembroCompensador) {
    this.idMiembroCompensador = idMiembroCompensador;
  }

  /**
   * @return
   */
  public String getNombreCompensador() {
    return this.nombreCompensador;
  }

  /**
   * @param referenciaCompensacion
   */
  public void setNombreCompensador(final String _nombreCompensador) {
    this.nombreCompensador = _nombreCompensador;
  }

  /**
   * @return
   */
  public String getReferenciaCompensacion() {
    return this.referenciaCompensacion;
  }

  /**
   * @param referenciaCompensacion
   */
  public void setReferenciaCompensacion(final String referenciaCompensacion) {
    this.referenciaCompensacion = referenciaCompensacion;
  }

  /**
   * @return
   */
  public String getReferenciaExterna() {
    return this.referenciaExterna;
  }

  /**
   * @param _referenciaExterna
   */
  public void setReferenciaExterna(final String _referenciaExterna) {
    this.referenciaExterna = _referenciaExterna;
  }

  public String getReferenciaAsignacion() {
    return referenciaAsignacion;
  }

  public void setReferenciaAsignacion(String referenciaAsignacion) {
    this.referenciaAsignacion = referenciaAsignacion;
  }

  /**
   * @return
   */
  public String getNemotecnico() {
    return this.nemotecnico;
  }

  /**
   * @param nemotecnico
   */
  public void setNemotecnico(final String nemotecnico) {
    this.nemotecnico = nemotecnico;
  }

  public Long getIdNemotecnico() {
    return idNemotecnico;
  }

  public void setIdNemotecnico(Long idNemotecnico) {
    this.idNemotecnico = idNemotecnico;
  }

  /**
   * @return the idRegla
   */
  public Long getIdRegla() {
    return idRegla;
  }

  /**
   * @param idRegla the idRegla to set
   */
  public void setIdRegla(Long idRegla) {
    this.idRegla = idRegla;
  }

  public String getDescripcionRegla() {
    return descripcionRegla;
  }

  public void setDescripcionRegla(String descripcionRegla) {
    this.descripcionRegla = descripcionRegla;
  }

  /**
   * @return the idParametrizacion
   */
  public Long getIdParametrizacion() {
    return idParametrizacion;
  }

  /**
   * @param idParametrizacion the idParametrizacion to set
   */
  public void setIdParametrizacion(Long idParametrizacion) {
    this.idParametrizacion = idParametrizacion;
  }

  public String getCamaraCompensacion() {
    return camaraCompensacion;
  }

  public void setCamaraCompensacion(String camaraCompensacion) {
    this.camaraCompensacion = camaraCompensacion;
  }

  public String getSegmento() {
    return segmento;
  }

  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  @Override
  public String toString() {
    return "Interna: [" + ((this.internal ? "SI" : "NO")) + "] [cc==" + this.idCuentaCompensacion + "] [miembro=="
        + this.idMiembroCompensador + "] [ref==" + this.referenciaCompensacion + "] [nemo==" + this.nemotecnico
        + "] [idRegla==" + this.idRegla + "] [idParametrizacion==" + this.idParametrizacion + "]";
  }

  public boolean isOk() {
    if (referenciaAsignacion != null && !referenciaAsignacion.trim().isEmpty() && nombreCompensador != null
        && !nombreCompensador.trim().isEmpty()
        && (codigoCuentaCompensacion == null || codigoCuentaCompensacion.trim().isEmpty())) {
      return true;
    }
    else if (codigoCuentaCompensacion != null && !codigoCuentaCompensacion.trim().isEmpty()
        && (referenciaAsignacion == null || referenciaAsignacion.trim().isEmpty())) {
      return true;
    }
    else {
      return false;
    }

  }

}
