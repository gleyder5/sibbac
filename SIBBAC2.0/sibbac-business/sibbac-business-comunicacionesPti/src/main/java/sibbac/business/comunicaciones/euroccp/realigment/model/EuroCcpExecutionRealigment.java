package sibbac.business.comunicaciones.euroccp.realigment.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import sibbac.business.comunicaciones.euroccp.ciffile.model.EuroCcpProcessedUnsettledMovement;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpErrorCode;
import sibbac.business.comunicaciones.euroccp.titulares.model.EuroCcpFileSent;
import sibbac.business.wrappers.database.model.MovimientoEccS3;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.database.DBConstants.EURO_CCP;
import sibbac.database.model.ATable;

@Table(name = EURO_CCP.EXECUTION_REALIGMENT)
@Entity
public class EuroCcpExecutionRealigment extends ATable<EuroCcpExecutionRealigment> {

  /**
   * 
   */
  private static final long serialVersionUID = 1245983863524074126L;

  @Column(name = "ESTADO_PROCESADO", nullable = false)
  private int estadoProcesado;

  @Column(name = "CLIENT_NUMBER", nullable = false)
  private String clientNumber;

  @Temporal(TemporalType.DATE)
  @Column(name = "TRADE_DATE", nullable = false)
  private Date tradeDate;

  @Column(name = "EXECUTION_REFERENCE", length = 20, nullable = false)
  private String executionReference;

  @Column(name = "EXCHANGE_CODE_TRADE", length = 4, nullable = false)
  private String mic;

  @Column(name = "ACCOUNT_NUMBER_FROM", length = 10, nullable = false)
  private String accountNumberFrom;

  @Column(name = "ACCOUNT_NUMBER_TO", length = 10, nullable = false)
  private String accountNumberTo;

  @Column(name = "NUMBER_SHARES", length = 10, scale = 0, nullable = false)
  private BigDecimal numberShares;

  @Column(name = "PROCESSING_STATUS", length = 1, nullable = true)
  private Character processingStatus;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ERROR_CODE", nullable = true)
  private EuroCcpErrorCode errorCode;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_FILE_SENT", nullable = true)
  private EuroCcpFileSent fileSent;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDANOTACIONECC", nullable = true)
  private Tmct0anotacionecc anotacion;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "NUDESGLOSE", nullable = true)
  private Tmct0desgloseclitit desglose;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_UNSETTLED_MOVEMENT", nullable = false)
  private EuroCcpProcessedUnsettledMovement unsettledMovement;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "IDMOVIMIENTO", nullable = true)
  private Tmct0movimientoecc movimientoEccOrden;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "ID_MOVIMIENTO_S3", nullable = true)
  private MovimientoEccS3 movimientoEccS3;

  public EuroCcpExecutionRealigment() {
  }

  public int getEstadoProcesado() {
    return estadoProcesado;
  }

  public String getClientNumber() {
    return clientNumber;
  }

  public void setClientNumber(String clientNumber) {
    this.clientNumber = clientNumber;
  }

  public Date getTradeDate() {
    return tradeDate;
  }

  public String getExecutionReference() {
    return executionReference;
  }

  public String getMic() {
    return mic;
  }

  public String getAccountNumberFrom() {
    return accountNumberFrom;
  }

  public String getAccountNumberTo() {
    return accountNumberTo;
  }

  public BigDecimal getNumberShares() {
    return numberShares;
  }

  public Character getProcessingStatus() {
    return processingStatus;
  }

  public EuroCcpErrorCode getErrorCode() {
    return errorCode;
  }

  public EuroCcpFileSent getFileSent() {
    return fileSent;
  }

  public void setEstadoProcesado(int estadoProcesado) {
    this.estadoProcesado = estadoProcesado;
  }

  public void setTradeDate(Date tradeDate) {
    this.tradeDate = tradeDate;
  }

  public void setExecutionReference(String executionReference) {
    this.executionReference = executionReference;
  }

  public void setMic(String mic) {
    this.mic = mic;
  }

  public void setAccountNumberFrom(String accountNumberFrom) {
    this.accountNumberFrom = accountNumberFrom;
  }

  public void setAccountNumberTo(String accountNumberTo) {
    this.accountNumberTo = accountNumberTo;
  }

  public void setNumberShares(BigDecimal numberShares) {
    this.numberShares = numberShares;
  }

  public void setProcessingStatus(Character processingStatus) {
    this.processingStatus = processingStatus;
  }

  public void setErrorCode(EuroCcpErrorCode errorCode) {
    this.errorCode = errorCode;
  }

  public void setFileSent(EuroCcpFileSent fileSent) {
    this.fileSent = fileSent;
  }

  public Tmct0anotacionecc getAnotacion() {
    return anotacion;
  }

  public Tmct0desgloseclitit getDesglose() {
    return desglose;
  }

  public void setAnotacion(Tmct0anotacionecc anotacion) {
    this.anotacion = anotacion;
  }

  public void setDesglose(Tmct0desgloseclitit desglose) {
    this.desglose = desglose;
  }

  public EuroCcpProcessedUnsettledMovement getUnsettledMovement() {
    return unsettledMovement;
  }

  public void setUnsettledMovement(EuroCcpProcessedUnsettledMovement unsettledMovement) {
    this.unsettledMovement = unsettledMovement;
  }

  public Tmct0movimientoecc getMovimientoEccOrden() {
    return movimientoEccOrden;
  }

  public void setMovimientoEccOrden(Tmct0movimientoecc movimientoEccOrden) {
    this.movimientoEccOrden = movimientoEccOrden;
  }

  public MovimientoEccS3 getMovimientoEccS3() {
    return movimientoEccS3;
  }

  public void setMovimientoEccS3(MovimientoEccS3 movimientoEccS3) {
    this.movimientoEccS3 = movimientoEccS3;
  }

  @Override
  public String toString() {
    return "EuroCcpExecutionRealigment [clientNumber=" + clientNumber + ", tradeDate=" + tradeDate
        + ", executionReference=" + executionReference + ", mic=" + mic + ", accountNumberFrom=" + accountNumberFrom
        + ", accountNumberTo=" + accountNumberTo + ", numberShares=" + numberShares + ", errorCode=" + errorCode
        + ", id=" + id + "]";
  }

}
