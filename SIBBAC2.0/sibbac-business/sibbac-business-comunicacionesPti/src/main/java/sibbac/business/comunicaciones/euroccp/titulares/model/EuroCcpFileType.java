package sibbac.business.comunicaciones.euroccp.titulares.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.database.DBConstants.EURO_CCP;
import sibbac.database.model.ATable;

@Table(name = EURO_CCP.FILE_TYPES)
@Entity
public class EuroCcpFileType extends ATable<EuroCcpFileType> {

  /**
   * 
   */
  private static final long serialVersionUID = 2809484393251362995L;

  @Column(name = "CD_CODIGO", length = 255)
  private String cdCodigo;

  @Column(name = "DESCRIPTION", length = 255)
  private String description;

  public EuroCcpFileType() {
  }

  public String getCdCodigo() {
    return cdCodigo;
  }

  public String getDescription() {
    return description;
  }

  public void setCdCodigo(String cdCodigo) {
    this.cdCodigo = cdCodigo;
  }

  public void setDescription(String description) {
    this.description = description;
  }

}
