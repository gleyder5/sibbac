package sibbac.business.comunicaciones.broker.bo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0ctdDao;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0pkb;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;

@Service(value = "PKBGenerateRouting")
public class PKBGenerateRouting extends AbstractPKBGenerate {

  private static final Logger LOG = LoggerFactory.getLogger(PKBGenerateRouting.class);

  /**
	 * 
	 */
  private static final long serialVersionUID = 6910864029323115366L;
  public static final String PROCESS_NAME = "PKBGenerateRouting";

  @Autowired
  private Tmct0ctdDao ctdDao;

  public PKBGenerateRouting() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public List<Tmct0pkb> createPkbs(Tmct0bok tmct0bok) throws SIBBACBusinessException {
    LOG.debug("[createPkbs] buscando ctds {}...", tmct0bok.getId());
    List<Tmct0ctd> ctds = ctdDao.findAllByNuodenAndNbooking(tmct0bok.getId().getNuorden(), tmct0bok.getId()
        .getNbooking());
    return runProcess(ctds);
  }

  /**
   * execute Criterio NO agrupacion nuorden, cdbroker, cdmercad, nbooking,
   * cdcleare, tpsetcle
   * 
   * Criterio SI agrupacion fetrade, cdbroker, cdisin, cdtpoper, cdcleare,
   * tpsetcle
   * @return int the status code
   * @throws SIBBACBusinessException
   * @throws PoolPersistanceManagerException
   * @throws Exception
   */
  protected List<Tmct0pkb> runProcess(List<Tmct0ctd> ctds) throws SIBBACBusinessException {
    List<Tmct0pkb> pkbs = new ArrayList<Tmct0pkb>();
    List<Tmct0ctd> listTmct0ctd = new ArrayList<Tmct0ctd>(ctds);
    while (listTmct0ctd.size() > 0) {

      List<Tmct0ctd> auxList = new ArrayList<Tmct0ctd>();

      Tmct0ctd tmct0ctd = listTmct0ctd.iterator().next();
      listTmct0ctd.remove(tmct0ctd);
      auxList.add(tmct0ctd);

      for (Tmct0ctd aux : listTmct0ctd) {

        if (tmct0ctd.getId().getNuorden().trim().equals(aux.getId().getNuorden().trim())
            && tmct0ctd.getId().getCdbroker().trim().equals(aux.getId().getCdbroker().trim())
            && tmct0ctd.getId().getCdmercad().trim().equals(aux.getId().getCdmercad().trim())
            && tmct0ctd.getId().getNbooking().trim().equals(aux.getId().getNbooking().trim())
            && tmct0ctd.getCdcleare().trim().equals(aux.getCdcleare().trim())
            && tmct0ctd.getTpsetcle().trim().equals(aux.getTpsetcle().trim())) {

          auxList.add(aux);
        }
      }

      listTmct0ctd.removeAll(auxList);
      LOG.debug("[createPkbs] creando pkb de ctds {}...", auxList);
      Tmct0pkb tmct0pkb = generateTmct0pkb(auxList, listTmct0ctd, getNuagrbrk(), getNuagrpkb(auxList, listTmct0ctd),
          true, "SIBBAC20");
      LOG.debug("[createPkbs] copiando nuagrpkb {}...", tmct0pkb.getNuagrpkb());
      for (Tmct0bok auxTmct0bok : getRelationCollection(auxList)) {
        auxTmct0bok.setNuagrpkb(tmct0pkb.getNuagrpkb());
        for (Tmct0alo tmct0alo : (List<Tmct0alo>) auxTmct0bok.getTmct0alos()) {
          if (!tmct0alo.getTmct0sta().getId().getCdestado().trim().equals("002")
              && !tmct0alo.getTmct0sta().getId().getCdestado().trim().equals("003"))
            tmct0alo.setNuagrpkb(tmct0pkb.getNuagrpkb());
        }
      }

      for (Tmct0ctd auxTmct0ctd : auxList) {
        auxTmct0ctd.setPkbroker1(tmct0pkb.getPkbroker());
      }

      tmct0pkb.setTmct0sta(new Tmct0sta(new Tmct0staId(TypeCdestado.FOREING_WAIT_RESPONSE_CLEARER.getValue(),
          TypeCdtipest.TMCT0PKB.getValue())));

      pkbs.add(tmct0pkb);

    }
    return pkbs;

  }
}
