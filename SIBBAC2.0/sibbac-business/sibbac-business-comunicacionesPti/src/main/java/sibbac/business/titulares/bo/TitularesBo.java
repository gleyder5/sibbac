package sibbac.business.titulares.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.common.SIBBACBusinessException;

@Service
public class TitularesBo {

  @Autowired
  private Tmct0cfgBo configBo;
  @Autowired
  private FestivoBo festivoBo;

  public TitularesConfigDTO getConfig(String auditUser) throws SIBBACBusinessException {
    TitularesConfigDTO config = new TitularesConfigDTO();
    config.setAuditUser(auditUser);
    config.setCamaraEuroccp(configBo.getIdEuroCcp());
    config.setCamaraNacional(configBo.getIdPti());
    config.setHolidays(festivoBo.findAllFestivosNacionales());
    config.setWorkDaysOfWeek(configBo.getWorkDaysOfWeek());
    config.setLimiteDiasEnvioTitularesNacional(configBo.getLimiteDiasEnvioTitularesPti());
    config.setLimiteDiasEnvioTitularesEuroccp(configBo.getLimiteDiasEnvioTitularesEuroccp());
    return config;
  }

}
