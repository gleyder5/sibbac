package sibbac.business.comunicaciones.pti.cuadreecc.runnable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.comunicaciones.pti.cuadreecc.bo.CuadreEccSibbacCargaOperacionesTraspasadasBo;
import sibbac.business.comunicaciones.pti.cuadreecc.dto.CuadreEccDbReaderRecordBeanDTO;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "cuadreEccSibbacCargaOperacionesTraspasadasRunnableBean")
public class CuadreEccSibbacCargaOperacionesTraspasadasRunnableBean extends
                                                                   IRunnableBean<CuadreEccDbReaderRecordBeanDTO> {

  @Autowired
  private CuadreEccSibbacCargaOperacionesTraspasadasBo cuadreEccSibbacCargaOperacionesTraspasadasBo;

  public CuadreEccSibbacCargaOperacionesTraspasadasRunnableBean() {
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<CuadreEccDbReaderRecordBeanDTO> beans, int order) throws SIBBACBusinessException {
    try {
      cuadreEccSibbacCargaOperacionesTraspasadasBo.cargarCodigosOperacionEccFromCuadreEccEjecucionNewTransacction(beans);
    } catch (EconomicDataException e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException("INCIDENCIA bean order: " + order + " beans: " + beans, e);
    } catch (Exception e) {
      getObserver().addErrorProcess(e);
      getObserver().setException(e);
      throw new SIBBACBusinessException("INCIDENCIA bean order: " + order + " beans: " + beans, e);
    } finally {
      getObserver().sutDownThread(this);
    }
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<CuadreEccDbReaderRecordBeanDTO> clone() {
    return this;
  }

}
