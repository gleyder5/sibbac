package sibbac.business.comunicaciones.pti.ac.dto;

import java.io.Serializable;
import java.util.Date;

public class AceptacionRechazoMovimientosDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3217556906484517573L;
  private final String cdrefmovimientoecc;
  private final Date fliquidacion;

  public AceptacionRechazoMovimientosDTO(String cdrefmovimientoecc, Date fliquidacion) {
    super();
    this.cdrefmovimientoecc = cdrefmovimientoecc;
    this.fliquidacion = fliquidacion;
  }

  public String getCdrefmovimientoecc() {
    return cdrefmovimientoecc;
  }

  public Date getFliquidacion() {
    return fliquidacion;
  }
  
  
  

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdrefmovimientoecc == null) ? 0 : cdrefmovimientoecc.hashCode());
    result = prime * result + ((fliquidacion == null) ? 0 : fliquidacion.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AceptacionRechazoMovimientosDTO other = (AceptacionRechazoMovimientosDTO) obj;
    if (cdrefmovimientoecc == null) {
      if (other.cdrefmovimientoecc != null)
        return false;
    }
    else if (!cdrefmovimientoecc.equals(other.cdrefmovimientoecc))
      return false;
    if (fliquidacion == null) {
      if (other.fliquidacion != null)
        return false;
    }
    else if (!fliquidacion.equals(other.fliquidacion))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "AceptacionRechazoMovimientosDTO [cdrefmovimientoecc=" + cdrefmovimientoecc + ", fliquidacion="
        + fliquidacion + "]";
  }
  
  
  

}
