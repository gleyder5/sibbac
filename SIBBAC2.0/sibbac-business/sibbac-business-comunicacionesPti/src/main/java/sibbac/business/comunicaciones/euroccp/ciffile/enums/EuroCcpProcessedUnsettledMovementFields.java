package sibbac.business.comunicaciones.euroccp.ciffile.enums;

import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldEnumInterface;
import sibbac.business.wrappers.tasks.thread.WrapperFileReaderFieldType;

public enum EuroCcpProcessedUnsettledMovementFields implements WrapperFileReaderFieldEnumInterface {
  
  REGISTER_TYPE("tipoRegistro", 3, 0, WrapperFileReaderFieldType.STRING),
  RELEASE_CODE("releaseCode", 3, 0, WrapperFileReaderFieldType.STRING),
  PROCESSING_DATE("processingDate", 8, 0, WrapperFileReaderFieldType.DATE),
  CLEARING_SITE_CODE("clearingSiteCode", 5, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_TYPE("accountType", 5, 0, WrapperFileReaderFieldType.STRING),
  CLIENT_NUMBER("clientNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ACCOUNT_NUMBER("accountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  SUBACCOUNT_NUMBER("subaccountNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  OPPOSITE_PARTY_CODE("oppositePartyCode", 6, 0, WrapperFileReaderFieldType.STRING),
  PRODUCT_GROUP_CODE("productGroupCode", 2, 0, WrapperFileReaderFieldType.STRING),
  EXCHANGE_CODE_TRADE("exchangeCodeTrade", 4, 0, WrapperFileReaderFieldType.STRING),
  SYMBOL("symbol", 6, 0, WrapperFileReaderFieldType.STRING),
  TYPE("typeField", 1, 0, WrapperFileReaderFieldType.CHAR),
  EXPIRATION_DATE("expirationDate", 8, 0, WrapperFileReaderFieldType.DATE),
  EXERCISE_PRICE("exercisePrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  EXTERNAL_MEMBER("externalMember", 10, 0, WrapperFileReaderFieldType.STRING),
  EXTERNAL_ACCOUNT("externalAccount", 15, 0, WrapperFileReaderFieldType.STRING),
  CURRENCY_CODE("currencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  MOVEMENT_CODE("movementCode", 2, 0, WrapperFileReaderFieldType.STRING),
  BUY_SELL_CODE("buySellCode", 1, 0, WrapperFileReaderFieldType.CHAR),
  PROCESSED_QUANTITY_LONG("processedQuantityLong", 13, 2, WrapperFileReaderFieldType.NUMERIC),
  PROCESSED_QUANTITY_SHORT("processedQuantityShort", 13, 2, WrapperFileReaderFieldType.NUMERIC),
  CLEARING_FEE("clearingFee", 12, 4, WrapperFileReaderFieldType.NUMERIC),
  CLEARING_FEE_DC("clearingFeeDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  CLEARING_FEE_CURRENCY_CODE("clearingFeeCurrencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  COUNTER_VALUE("counterValue", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  COUNTER_VALUE_DC("counterValueDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  COUNTER_VALUE_CURRENCY_CODE("counterValueCurrencyCode", 3, 0, WrapperFileReaderFieldType.STRING),
  CUPON_INTEREST("cuponInterest", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  CUPON_INTEREST_DC("cuponInterestDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  EFFECTIVE_VALUE("effectiveValue", 18, 2, WrapperFileReaderFieldType.NUMERIC),
  EFFECTIVE_VALUE_DC("effectiveValueDC", 1, 0, WrapperFileReaderFieldType.CHAR),
  TRANSACTION_PRICE("transactionPrice", 15, 7, WrapperFileReaderFieldType.NUMERIC),
  TRANSACTION_DATE("transactionDate", 8, 0, WrapperFileReaderFieldType.DATE),
  SETTLEMENT_DATE("settlementDate", 8, 0, WrapperFileReaderFieldType.DATE),
  UNSETTLED_REFERENCE("unsettledReference", 9, 0, WrapperFileReaderFieldType.STRING),
  EXTERNAL_TRANSACTION_ID_EXCHANGE("externalTransactionIdExchange", 20, 0, WrapperFileReaderFieldType.STRING),
  SETTLEMENT_INSTRUCTION_REFERENCE("settlementInstructionReference", 9, 0, WrapperFileReaderFieldType.STRING),
  ORDER_NUMBER("orderNumber", 10, 0, WrapperFileReaderFieldType.STRING),
  ISIN_CODE("isinCode", 12, 0, WrapperFileReaderFieldType.STRING),
  TRADER_INITIALS("traderInitials", 6, 0, WrapperFileReaderFieldType.STRING),
  ULV_TRADING_UNIT("ulvTradingUnit", 11, 4, WrapperFileReaderFieldType.NUMERIC),
  TRANSACTION_ORIGIN("transactionOrigin", 4, 0, WrapperFileReaderFieldType.STRING),
  EXECUTION_TRADING_ID("executionTradingId", 6, 0, WrapperFileReaderFieldType.STRING),
  DEPOT_ID("depotId", 6, 0, WrapperFileReaderFieldType.STRING),
  SAFE_KEEPING_ID("safeKeepingId", 2, 0, WrapperFileReaderFieldType.STRING),
  COMMENT("commentField", 21, 0, WrapperFileReaderFieldType.STRING),
  TIMESTAMP_FIELD("timestampField", 6, 0, WrapperFileReaderFieldType.TIME),
  TRANSACTION_TYPE_CODE("transactionTypeCode", 3, 0, WrapperFileReaderFieldType.STRING),
  EXTERNAL_POSITION_ACCOUNT_ID("externalPositionAccountId", 30, 0, WrapperFileReaderFieldType.STRING),
  DUAL_LISTED_INDICATOR("dualListedIndicator", 1, 0, WrapperFileReaderFieldType.CHAR),
  FILLER("filler", 96, 0, WrapperFileReaderFieldType.STRING);

  private String name;
  private int length;
  private int scale;
  private WrapperFileReaderFieldType fieldType;

  private EuroCcpProcessedUnsettledMovementFields(String name,
                                                  int length,
                                                  int scale,
                                                  WrapperFileReaderFieldType fieldType) {
    this.name = name;
    this.length = length;
    this.scale = scale;
    this.fieldType = fieldType;

  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getLength() {
    return length;
  }

  @Override
  public int getScale() {
    return scale;
  }

  @Override
  public WrapperFileReaderFieldType getFielType() {
    return fieldType;
  }

}
