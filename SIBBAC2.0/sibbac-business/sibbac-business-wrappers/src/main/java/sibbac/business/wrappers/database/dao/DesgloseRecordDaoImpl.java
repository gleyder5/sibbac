package sibbac.business.wrappers.database.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.common.DTOUtils;
import sibbac.business.wrappers.database.dto.DesgloseDTO;

@Repository
public class DesgloseRecordDaoImpl {
	
	private static final Logger LOG = LoggerFactory.getLogger(DesgloseRecordDaoImpl.class);

	@PersistenceContext
	private EntityManager	em;
	
	/**
	 * 
	 * @param fechaDesde
	 * @param fechaHasta
	 * @param isin
	 * 
	 * @return
	 */
	/*public Integer countFindAllDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin ) {
		List<DesgloseDTO> resultList = this.findDTOForService(fechaDesde, fechaHasta, isin);
		Integer count = 0;
		if (resultList != null)
		{
			count = resultList.size();
		}
		return count;
	}*/
	
	public Integer countFindAllDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin ) {
		final StringBuilder select = new StringBuilder(
				"SELECT COUNT( DISTINCT desgloses.NUMORDEN) " );

		final StringBuilder from = new StringBuilder( " from TMCT0_DESGLOSE_RECORD_BEAN desgloses, TMCT0ORD ord " );
		final StringBuilder where = new StringBuilder( " " );
		boolean whereEmpty = false;

		final String fechaDesdeParamName = "feejeDesde";
		final String fechaHastaParamName = "feejeHasta";
		where.append(" where desgloses.NUMORDEN = ord.NUORDEN ");
		


		if ( fechaDesde != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "fejec >=:" + fechaDesdeParamName );
		}

		if ( fechaHasta != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "fejec <=:" + fechaHastaParamName );
		}
	

	

		final String isinParamName = "isin";
		if ( isin != null && isin.size() > 0 ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
		    if(isin.get(0).indexOf("#") != -1 ){
		    	isin.set(0, isin.get(0).replace("#",""));	
		    	where.append( "codvaiso NOT IN :" + isinParamName );
		    }else{
		    	where.append( "codvaiso IN :" + isinParamName );
		    }
		}

		final Query query = em.createNativeQuery( select.append( from.append( where ) )
				.toString() );
	
		setParametersWithVectors( query, fechaDesde, fechaHasta, isin, fechaDesdeParamName, fechaHastaParamName, isinParamName, null, null, null, null);

		final List< Object > resultList = query.getResultList();

		return (Integer)resultList.get(0);
	}
	
	public Integer countFindHuerfanosAllDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin ) {
		final StringBuilder select = new StringBuilder(
				"SELECT COUNT( DISTINCT desgloses.NUMORDEN) " );

		final StringBuilder from = new StringBuilder( " from TMCT0_DESGLOSE_RECORD_BEAN desgloses " );
		final StringBuilder where = new StringBuilder( " " );
		boolean whereEmpty = false;

		final String fechaDesdeParamName = "feejeDesde";
		final String fechaHastaParamName = "feejeHasta";
		where.append(" where not exists (select * from TMCT0ORD ord where ord.NUORDEN = desgloses.NUMORDEN) ");
		


		if ( fechaDesde != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "fejec >=:" + fechaDesdeParamName );
		}

		if ( fechaHasta != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "fejec <=:" + fechaHastaParamName );
		}
	

	

		final String isinParamName = "isin";
		if ( isin != null && isin.size() > 0 ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
		    if(isin.get(0).indexOf("#") != -1 ){
		    	isin.set(0, isin.get(0).replace("#",""));	
		    	where.append( "codvaiso NOT IN :" + isinParamName );
		    }else{
		    	where.append( "codvaiso IN :" + isinParamName );
		    }
		}

		final Query query = em.createNativeQuery( select.append( from.append( where ) )
				.toString() );
	
		setParametersWithVectors( query, fechaDesde, fechaHasta, isin, fechaDesdeParamName, fechaHastaParamName, isinParamName, null, null, null, null);

		final List< Object > resultList = query.getResultList();

		return (Integer)resultList.get(0);
	}
	
	private void setParametersWithVectors( Query query, Date fechaDesde, Date fechaHasta, List< String > isin, String fechaDesdeParamName,
			String fechaHastaParamName, String isinParamName, Integer firstRow,
			String firstRowParamName, Integer lastRow, String lastRowParamName ) {

		if ( firstRow != null ) {
			query.setParameter( firstRowParamName, firstRow );
		}
		if ( lastRow != null ) {
			query.setParameter( lastRowParamName, lastRow );
		}

		if ( fechaDesde != null ) {
			query.setParameter( fechaDesdeParamName, fechaDesde, TemporalType.DATE );
		}
		if ( fechaHasta != null ) {
			query.setParameter( fechaHastaParamName, fechaHasta, TemporalType.DATE );
		}
		
		if ( isin != null && isin.size() > 0 ) {
			query.setParameter( isinParamName, isin );
		}
		
	}
	
	private List< DesgloseDTO > resultadosConsulta (Query query)
	{
		List<Object> resultList = null;
		try{
		    resultList = query.getResultList();
		}catch(Exception ex){
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}

		final List< DesgloseDTO > desglosesList = new ArrayList< DesgloseDTO >();
		String numOrden = null;
		DesgloseDTO orden = new DesgloseDTO();
		if ( CollectionUtils.isNotEmpty( resultList ) ) {
			for ( Object obj : resultList ) {
				DesgloseDTO desglose = DTOUtils.convertObjectToDesgloseDTO( ( Object[] ) obj );
				if (desglose.getNumOrden().equals(numOrden))
				{
					orden.setTitulos(orden.getTitulos().add(desglose.getTitulos()));
					orden.setNominal(orden.getNominal().add(desglose.getNominal()));
					boolean bOk = orden.getValidado() != 0;
					if (!bOk)
					{
						desglose.setBOkFechaEjecucion(orden.getFejecucion().equals(desglose.getFejecucion()));
						desglose.setBOkBolsa(orden.getBolsa().equals(desglose.getBolsa()));
						desglose.setBOkTipoOperacion(orden.getTipoOperacion().equals(desglose.getTipoOperacion()));
						desglose.setBOkIsin(orden.getIsin().equals(desglose.getIsin()));
						desglose.setBOkTipoCambio(orden.getTipoCambio().equals(desglose.getTipoCambio()));
						desglose.setBOkTipoSaldo(orden.getTipoSaldo().equals(desglose.getTipoSaldo()));
						orden.setBOkFechaEjecucion(desglose.getBOkFechaEjecucion() && orden.getBOkFechaEjecucion());
						orden.setBOkBolsa(desglose.getBOkBolsa() && orden.getBOkBolsa());
						orden.setBOkTipoOperacion(desglose.getBOkTipoOperacion() && orden.getBOkTipoOperacion());
						orden.setBOkIsin(desglose.getBOkIsin() && orden.getBOkIsin());
						orden.setBOkTipoCambio(desglose.getBOkTipoCambio() && orden.getBOkTipoCambio());
						orden.setBOkTipoSaldo(desglose.getBOkTipoSaldo() && orden.getBOkTipoSaldo());						
					}
					orden.getListaDesgloses().add(desglose);
				}
				else
				{
					if (numOrden != null)
					{
						boolean bOk = orden.getValidado() != 0;
						if (!bOk)
						{
							if (orden.getBOkFechaEjecucion() && orden.getBOkBolsa() && orden.getBOkTipoOperacion() && orden.getBOkIsin() && orden.getBOkTipoCambio()
								&& orden.getBOkTipoSaldo() && (orden.getTitulos().compareTo(orden.getTitulosSV()) == 0))
							{
								orden.setValidado(new Integer(1));
							}
						}
						desglosesList.add( orden );
						orden = desglose;
					}
					orden = desglose;
					List<DesgloseDTO> listaDesgloses = new ArrayList<DesgloseDTO> ();
					listaDesgloses.add(new DesgloseDTO(desglose));
					orden.setListaDesgloses(listaDesgloses);
					numOrden = orden.getNumOrden();
				}
				
			}
		}
		
		boolean bOk = orden.getValidado() != 0;
		if (!bOk)
		{
			if (orden.getBOkFechaEjecucion() && orden.getBOkBolsa() && orden.getBOkTipoOperacion() && orden.getBOkIsin() && orden.getBOkTipoCambio()
				&& orden.getBOkTipoSaldo() && (orden.getTitulos().compareTo(orden.getTitulosSV()) == 0))
			{
				orden.setValidado(new Integer(1));
			}
		}
		desglosesList.add( orden );

		return desglosesList;
	}
	
	private Query consultaDesgloses (Date fechaDesde, Date fechaHasta, List< String > isin)
	{
		
		final StringBuilder select = new StringBuilder(
				
				"select desgloses.ID, desgloses.AUDIT_DATE, desgloses.TIPOREG, desgloses.NIF_BIC, desgloses.CBOEJECU, desgloses.CENTCTVA, "
						+ "desgloses.CODEMPRL, desgloses.CODORGEJ, desgloses.OBCSAVB, desgloses.CODVAISO, desgloses.DENOMENA, desgloses.DPTECTVA, "
						+ "desgloses.EMPRCTVA, desgloses.FEJEC, desgloses.NOMINEJ, desgloses.NUMORDEN, desgloses.NUREFORD, "
						+ "desgloses.ORDCOM, desgloses.PROCESADO, desgloses.REFMOD, desgloses.SEGCLI, desgloses.TIPCAMBI, desgloses.TIPOPERAC, "
						+ "desgloses.TIPSALDO, desgloses.ACCEJEC, T.INFORXML, ord.NUTITORD, " );
		
		select.append( "ROW_NUMBER() OVER (ORDER BY desgloses.id DESC) AS row_number  " );

		final StringBuilder from = new StringBuilder( " from TMCT0_DESGLOSE_RECORD_BEAN desgloses, TMCT0ORD ord, TMCT0XAS T, TMCT0XAS SUB " );
		final StringBuilder where = new StringBuilder( " " );
		boolean whereEmpty = false;

		final String fechaDesdeParamName = "feejeDesde";
		final String fechaHastaParamName = "feejeHasta";
		where.append(" where desgloses.NUMORDEN = ord.NUORDEN");
		where.append(" and T.NBCAMAS4 = 'CDBOLSAS' AND T.INFORAS4 = SUB.INFORXML AND SUB.NBCAMAS4='CODORGEJ' AND T.NUVERSION = '1'");
		where.append(" and SUB.INFORAS4 = desgloses.CODORGEJ ");
			if ( fechaDesde != null ) {
				if ( !whereEmpty ) {
					where.append( " and " );
				} else {
					where.append( " where " );
					whereEmpty = false;
				}
				where.append( "fejec >=:" + fechaDesdeParamName );
			}

			if ( fechaHasta != null ) {
				if ( !whereEmpty ) {
					where.append( " and " );
				} else {
					where.append( " where " );
					whereEmpty = false;
				}
				where.append( "fejec <=:" + fechaHastaParamName );
			}
		

		

		final String isinParamName = "isin";
		if ( isin != null && isin.size() > 0 ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
		    if(isin.get(0).indexOf("#") != -1 ){
		    	isin.set(0, isin.get(0).replace("#",""));	
		    	where.append( "codvaiso NOT IN :" + isinParamName );
		    }else{
		    	where.append( "codvaiso IN :" + isinParamName );
		    }
		}

		final Query query = em.createNativeQuery( select.append( from.append( where ) )
				.toString() );

		setParametersWithVectors( query, fechaDesde, fechaHasta, isin, fechaDesdeParamName, fechaHastaParamName, isinParamName, null, null, null, null);
		
		return query;
	}
	
	public List< DesgloseDTO > findAllDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin,  Integer startPosition, Integer maxResult ) {
		
		
		Query query = consultaDesgloses (fechaDesde,fechaHasta, isin);
		
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		
		return this.resultadosConsulta(query);
		
	}//public List< DesgloseDTO > findAllDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin,  Integer startPosition, Integer maxResult ) {
	
	public List< DesgloseDTO > findDTOForService( Date fechaDesde, Date fechaHasta, List< String > isin ) {
				
		Query query = consultaDesgloses (fechaDesde,fechaHasta, isin);		
		return this.resultadosConsulta(query);
		
	}//public List< DesgloseDTO > findDTOForService( Date fechaDesde, Date fechaHasta, List< String > isin) {
	
	private Query consultaDesglosesHuerfanos (Date fechaDesde, Date fechaHasta, List< String > isin)
	{
		
		final StringBuilder select = new StringBuilder(
				
				"select desgloses.ID, desgloses.AUDIT_DATE, desgloses.TIPOREG, desgloses.NIF_BIC, desgloses.CBOEJECU, desgloses.CENTCTVA, "
						+ "desgloses.CODEMPRL, desgloses.CODORGEJ, desgloses.OBCSAVB, desgloses.CODVAISO, desgloses.DENOMENA, desgloses.DPTECTVA, "
						+ "desgloses.EMPRCTVA, desgloses.FEJEC, desgloses.NOMINEJ, desgloses.NUMORDEN, desgloses.NUREFORD, "
						+ "desgloses.ORDCOM, desgloses.PROCESADO, desgloses.REFMOD, desgloses.SEGCLI, desgloses.TIPCAMBI, desgloses.TIPOPERAC, "
						+ "desgloses.TIPSALDO, desgloses.ACCEJEC, T.INFORXML, " );
		
		select.append( "ROW_NUMBER() OVER (ORDER BY desgloses.id DESC) AS row_number  " );

		final StringBuilder from = new StringBuilder( " from TMCT0_DESGLOSE_RECORD_BEAN desgloses, TMCT0XAS T, TMCT0XAS SUB " );
		final StringBuilder where = new StringBuilder( " " );
		boolean whereEmpty = false;

		final String fechaDesdeParamName = "feejeDesde";
		final String fechaHastaParamName = "feejeHasta";
		where.append(" where not exists (select * from TMCT0ORD ord where ord.NUORDEN = desgloses.NUMORDEN) ");
		where.append(" and T.NBCAMAS4 = 'CDBOLSAS' AND T.INFORAS4 = SUB.INFORXML AND SUB.NBCAMAS4='CODORGEJ' AND T.NUVERSION = '1'");
		where.append(" and SUB.INFORAS4 = desgloses.CODORGEJ ");
		
		if ( fechaDesde != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "fejec >=:" + fechaDesdeParamName );
		}

		if ( fechaHasta != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
				where.append( "fejec <=:" + fechaHastaParamName );
		}
		

		

		final String isinParamName = "isin";
		if ( isin != null && isin.size() > 0 ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
		    if(isin.get(0).indexOf("#") != -1 ){
		    	isin.set(0, isin.get(0).replace("#",""));
		    	where.append( "codvaiso NOT IN :" + isinParamName );
		    }else{
		    	where.append( "codvaiso IN :" + isinParamName );
		    }
		}

		final Query query = em.createNativeQuery( select.append( from.append( where ) )
				.toString() );

		setParametersWithVectors( query, fechaDesde, fechaHasta, isin, fechaDesdeParamName, fechaHastaParamName, isinParamName, null, null, null, null);
		
		return query;
	}
	
	private List< DesgloseDTO > resultadosConsultaHuerfano (Query query)
	{
		List<Object> resultList = null;
		try{
		    resultList = query.getResultList();
		}catch(Exception ex){
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}

		final List< DesgloseDTO > desglosesList = new ArrayList< DesgloseDTO >();
		String numOrden = null;
		DesgloseDTO orden = new DesgloseDTO();
		if ( CollectionUtils.isNotEmpty( resultList ) ) {
			for ( Object obj : resultList ) {
				DesgloseDTO desglose = DTOUtils.convertObjectToDesgloseHuefanoDTO( ( Object[] ) obj );
				if (desglose.getNumOrden().equals(numOrden))
				{
					orden.setTitulos(orden.getTitulos().add(desglose.getTitulos()));
					orden.setNominal(orden.getNominal().add(desglose.getNominal()));
					boolean bOk = orden.getValidado() != 0;
					if (!bOk)
					{
						desglose.setBOkFechaEjecucion(orden.getFejecucion().equals(desglose.getFejecucion()));
						desglose.setBOkBolsa(orden.getBolsa().equals(desglose.getBolsa()));
						desglose.setBOkTipoOperacion(orden.getTipoOperacion().equals(desglose.getTipoOperacion()));
						desglose.setBOkIsin(orden.getIsin().equals(desglose.getIsin()));
						desglose.setBOkTipoCambio(orden.getTipoCambio().equals(desglose.getTipoCambio()));
						desglose.setBOkTipoSaldo(orden.getTipoSaldo().equals(desglose.getTipoSaldo()));
						orden.setBOkFechaEjecucion(desglose.getBOkFechaEjecucion() && orden.getBOkFechaEjecucion());
						orden.setBOkBolsa(desglose.getBOkBolsa() && orden.getBOkBolsa());
						orden.setBOkTipoOperacion(desglose.getBOkTipoOperacion() && orden.getBOkTipoOperacion());
						orden.setBOkIsin(desglose.getBOkIsin() && orden.getBOkIsin());
						orden.setBOkTipoCambio(desglose.getBOkTipoCambio() && orden.getBOkTipoCambio());
						orden.setBOkTipoSaldo(desglose.getBOkTipoSaldo() && orden.getBOkTipoSaldo());						
					}
					orden.getListaDesgloses().add(desglose);
				}
				else
				{
					if (numOrden != null)
					{
						boolean bOk = orden.getValidado() != 0;
						if (!bOk)
						{
							if (orden.getBOkFechaEjecucion() && orden.getBOkBolsa() && orden.getBOkTipoOperacion() && orden.getBOkIsin() && orden.getBOkTipoCambio()
								&& orden.getBOkTipoSaldo())
							{
								orden.setValidado(new Integer(1));
							}
						}
						desglosesList.add( orden );
						
					}
					orden = desglose;
					List<DesgloseDTO> listaDesgloses = new ArrayList<DesgloseDTO> ();
					listaDesgloses.add(new DesgloseDTO(desglose));
					orden.setListaDesgloses(listaDesgloses);
					numOrden = orden.getNumOrden();
				}
				
			}
		}
		
		boolean bOk = orden.getValidado() != 0;
		if (!bOk)
		{
			if (orden.getBOkFechaEjecucion() && orden.getBOkBolsa() && orden.getBOkTipoOperacion() && orden.getBOkIsin() && orden.getBOkTipoCambio()
				&& orden.getBOkTipoSaldo())
			{
				orden.setValidado(new Integer(1));
			}
		}
		desglosesList.add( orden );
		return desglosesList;
	}
	
	public List< DesgloseDTO > findHuerfanoDTOForService( Date fechaDesde, Date fechaHasta, List< String > isin ) {
		
		Query query = consultaDesglosesHuerfanos (fechaDesde,fechaHasta, isin);		
		return this.resultadosConsultaHuerfano(query);
		
	}//public List< DesgloseDTO > findHuerfanoDTOForService( Date fechaDesde, Date fechaHasta, List< String > isin,  Integer startPosition, Integer maxResult ) {
	
	public List< DesgloseDTO > findAllHuerfanoDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin,  Integer startPosition, Integer maxResult ) {
		
		
		Query query = consultaDesglosesHuerfanos (fechaDesde,fechaHasta, isin);
		
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		
		return this.resultadosConsultaHuerfano(query);
		
	}//public List< DesgloseDTO > findAllHuerfanosDTOPageable( Date fechaDesde, Date fechaHasta, List< String > isin,  Integer startPosition, Integer maxResult ) {
	
}
