package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;

import sibbac.business.fase0.database.model.Tmct0dir;
import sibbac.common.helper.CloneObjectHelper;

public class SettlementDataClientDirectionsDTO implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = -325360424581989487L;

  public SettlementDataClientDirectionsDTO() {
    // TODO Auto-generated constructor stub
  }

  public SettlementDataClientDirectionsDTO(Tmct0dir dir) {
    this();
    CloneObjectHelper.copyBasicFields(dir, this, null);
  }

  /**
   * Table Field CDBROCLI. CLIENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdbrocli;

  /**
   * Table Field CDALIASS. CUENTA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdaliass;
  /**
   * Table Field CDSUBCTA. SUBCUENTA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdsubcta;
  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal numsec;
  /**
   * Table Field NAMEID. CONTACT NAME Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nameid;
  /**
   * Table Field EMPLOY. EMPLOYMENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String employ;
  /**
   * Table Field TPADDRES. ADDRESS TYPE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tpaddres;
  /**
   * Table Field NBSTREET. STREET Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nbstreet;
  /**
   * Table Field NUSTREET. NUMBER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nustreet;
  /**
   * Table Field NBLOCAT. LOCATION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nblocat;
  /**
   * Table Field NBPROVIN. PROVINCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nbprovin;
  /**
   * Table Field CDZIPCOD. POSTAL CODE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdzipcod;
  /**
   * Table Field CDCOUNTR. COUNTRY CODE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdcountr;
  /**
   * Table Field NUPHONEE. PHONE NUMBER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nuphonee;
  /**
   * Table Field EMAILADD. EMAIL ADDRESS Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String emailadd;
  /**
   * Table Field MCONFIRM. CONFIRM METHOD -FIDESSA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String mconfirm;
  /**
   * Table Field NCONFIRM. CONFIRM LEVEL -FIDESSA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nconfirm;
  /**
   * Table Field IDOASYS. ID ELECTRONIC CONFIRMATION (OASYS-FIDESSA) Documentación columna <!-- begin-user-doc --> <!--
   * end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String idoasys;
  /**
   * Table Field CDUSUAUD. AUDIT USER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. AUDIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.sql.Timestamp fhaudit;
  /**
   * Table Field FHINICIO. INIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhinicio;
  /**
   * Table Field FHFINAL. FINAL DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhfinal;

  /**
   * @return the cdbrocli
   */
  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  /**
   * @param cdbrocli the cdbrocli to set
   */
  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  /**
   * @return the cdaliass
   */
  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  /**
   * @param cdaliass the cdaliass to set
   */
  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  /**
   * @return the cdsubcta
   */
  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  /**
   * @param cdsubcta the cdsubcta to set
   */
  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  /**
   * @return the numsec
   */
  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  /**
   * @param numsec the numsec to set
   */
  public void setNumsec(java.math.BigDecimal numsec) {
    this.numsec = numsec;
  }

  /**
   * @return the nameid
   */
  public java.lang.String getNameid() {
    return nameid;
  }

  /**
   * @param nameid the nameid to set
   */
  public void setNameid(java.lang.String nameid) {
    this.nameid = nameid;
  }

  /**
   * @return the employ
   */
  public java.lang.String getEmploy() {
    return employ;
  }

  /**
   * @param employ the employ to set
   */
  public void setEmploy(java.lang.String employ) {
    this.employ = employ;
  }

  /**
   * @return the tpaddres
   */
  public java.lang.String getTpaddres() {
    return tpaddres;
  }

  /**
   * @param tpaddres the tpaddres to set
   */
  public void setTpaddres(java.lang.String tpaddres) {
    this.tpaddres = tpaddres;
  }

  /**
   * @return the nbstreet
   */
  public java.lang.String getNbstreet() {
    return nbstreet;
  }

  /**
   * @param nbstreet the nbstreet to set
   */
  public void setNbstreet(java.lang.String nbstreet) {
    this.nbstreet = nbstreet;
  }

  /**
   * @return the nustreet
   */
  public java.lang.String getNustreet() {
    return nustreet;
  }

  /**
   * @param nustreet the nustreet to set
   */
  public void setNustreet(java.lang.String nustreet) {
    this.nustreet = nustreet;
  }

  /**
   * @return the nblocat
   */
  public java.lang.String getNblocat() {
    return nblocat;
  }

  /**
   * @param nblocat the nblocat to set
   */
  public void setNblocat(java.lang.String nblocat) {
    this.nblocat = nblocat;
  }

  /**
   * @return the nbprovin
   */
  public java.lang.String getNbprovin() {
    return nbprovin;
  }

  /**
   * @param nbprovin the nbprovin to set
   */
  public void setNbprovin(java.lang.String nbprovin) {
    this.nbprovin = nbprovin;
  }

  /**
   * @return the cdzipcod
   */
  public java.lang.String getCdzipcod() {
    return cdzipcod;
  }

  /**
   * @param cdzipcod the cdzipcod to set
   */
  public void setCdzipcod(java.lang.String cdzipcod) {
    this.cdzipcod = cdzipcod;
  }

  /**
   * @return the cdcountr
   */
  public java.lang.String getCdcountr() {
    return cdcountr;
  }

  /**
   * @param cdcountr the cdcountr to set
   */
  public void setCdcountr(java.lang.String cdcountr) {
    this.cdcountr = cdcountr;
  }

  /**
   * @return the nuphonee
   */
  public java.lang.String getNuphonee() {
    return nuphonee;
  }

  /**
   * @param nuphonee the nuphonee to set
   */
  public void setNuphonee(java.lang.String nuphonee) {
    this.nuphonee = nuphonee;
  }

  /**
   * @return the emailadd
   */
  public java.lang.String getEmailadd() {
    return emailadd;
  }

  /**
   * @param emailadd the emailadd to set
   */
  public void setEmailadd(java.lang.String emailadd) {
    this.emailadd = emailadd;
  }

  /**
   * @return the mconfirm
   */
  public java.lang.String getMconfirm() {
    return mconfirm;
  }

  /**
   * @param mconfirm the mconfirm to set
   */
  public void setMconfirm(java.lang.String mconfirm) {
    this.mconfirm = mconfirm;
  }

  /**
   * @return the nconfirm
   */
  public java.lang.String getNconfirm() {
    return nconfirm;
  }

  /**
   * @param nconfirm the nconfirm to set
   */
  public void setNconfirm(java.lang.String nconfirm) {
    this.nconfirm = nconfirm;
  }

  /**
   * @return the idoasys
   */
  public java.lang.String getIdoasys() {
    return idoasys;
  }

  /**
   * @param idoasys the idoasys to set
   */
  public void setIdoasys(java.lang.String idoasys) {
    this.idoasys = idoasys;
  }

  /**
   * @return the cdusuaud
   */
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * @param cdusuaud the cdusuaud to set
   */
  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  /**
   * @return the fhaudit
   */
  public java.sql.Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * @param fhaudit the fhaudit to set
   */
  public void setFhaudit(java.sql.Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * @return the fhinicio
   */
  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }

  /**
   * @param fhinicio the fhinicio to set
   */
  public void setFhinicio(java.math.BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  /**
   * @return the fhfinal
   */
  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }

  /**
   * @param fhfinal the fhfinal to set
   */
  public void setFhfinal(java.math.BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SettlementDataClientDirectionsDTO [cdbrocli=" + cdbrocli + ", cdaliass=" + cdaliass + ", cdsubcta="
           + cdsubcta + ", numsec=" + numsec + ", nameid=" + nameid + ", employ=" + employ + ", tpaddres=" + tpaddres
           + ", nbstreet=" + nbstreet + ", nustreet=" + nustreet + ", nblocat=" + nblocat + ", nbprovin=" + nbprovin
           + ", cdzipcod=" + cdzipcod + ", cdcountr=" + cdcountr + ", nuphonee=" + nuphonee + ", emailadd=" + emailadd
           + ", mconfirm=" + mconfirm + ", nconfirm=" + nconfirm + ", idoasys=" + idoasys + ", cdusuaud=" + cdusuaud
           + ", fhaudit=" + fhaudit + ", fhinicio=" + fhinicio + ", fhfinal=" + fhfinal + "]";
  }

}
