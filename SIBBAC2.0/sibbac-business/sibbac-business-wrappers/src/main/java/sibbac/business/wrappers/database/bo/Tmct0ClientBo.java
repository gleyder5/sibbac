package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0ClientDao;
import sibbac.business.wrappers.database.dao.Tmct0cliHistoricalDao;
import sibbac.business.wrappers.database.dao.Tmct0dbsDaoImp;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cliHistorical;
import sibbac.business.wrappers.database.model.Tmct0dbs;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ClientBo extends AbstractBo<Tmct0cli, Long, Tmct0ClientDao> {

  @Autowired
  Tmct0cliHistoricalDao tmct0cliHistoricalDao;

  @Autowired
  Tmct0dbsDaoImp tmct0dbsDaoImp;

  @Transactional
  public void saveClient(Tmct0cli tmct0client, List<Tmct0cliHistorical> historicosCliente) {

    try {

      boolean isAlta = true;
      if (tmct0client.getIdClient() != null) {
        isAlta = false;
      }

      tmct0client.getAddress().setCliente(tmct0client);

      if (CollectionUtils.isNotEmpty(tmct0client.getMifid())) {
        for (Tmct0CliMifid mifid : tmct0client.getMifid()) {
          mifid.setCliente(tmct0client);
        }
      }

      if (CollectionUtils.isNotEmpty(tmct0client.getContactos())) {
        for (Contacto contacto : tmct0client.getContactos()) {
          // FIXME campos insertados porque en produccion los campos no admiten nulos
          // contacto.setAlias(new Alias(null,null));
          // contacto.getAlias().setId(1L);
          contacto.setCliente(tmct0client);
          contacto.setDescripcion("");
          if (contacto.getAddress().equals(contacto.getCliente().getAddress())) {
            contacto.setAddress(contacto.getCliente().getAddress());
          }
        }
      }

      if (!isAlta && CollectionUtils.isNotEmpty(historicosCliente)) {
        for (Tmct0cliHistorical histcli : historicosCliente) {
          histcli.setIdClient(tmct0client.getIdClient());
          this.tmct0cliHistoricalDao.saveAndFlush(histcli);
        }
      }

      // Persistencia en registro de actividad.
      Tmct0dbs tmct0dbs = new Tmct0dbs();
      tmct0dbs.setNtabla("TMCT0CLI");
      tmct0dbs.setAuxiliar(tmct0client.getCdbrocli());
      if (isAlta) {
        tmct0dbs.setTipo(new Character('A'));
      } else {
        tmct0dbs.setTipo(new Character('M'));
      }
      this.tmct0dbsDaoImp.populateAndPersistAndFlush(tmct0dbs);

      this.saveAndFlush(tmct0client);

      // En el caso del alta ya generada se setea campo CDOTHEID
      if (tmct0client.getIdClient() != null) {
        String ceros = "";
        StringBuffer sb = new StringBuffer();
        sb.append(String.valueOf(tmct0client.getIdClient()));
        int sizeIdClient = sb.length();
        if (sizeIdClient < 6) {
          int resto = 6 - sizeIdClient;
          for (int x = 0; x < resto; x++) {
            ceros = ceros + "0";
          }
        }
        tmct0client.setCdotheid("99" + ceros + sb.toString());
        this.saveAndFlush(tmct0client);
      }

    } catch (Exception e) {
      LOG.error(e.getMessage());
    }
  }

  @Transactional
  public void saveDeleteClient(Tmct0cli tmct0client) {

    this.saveAndFlush(tmct0client);

    // Persistencia en registro de actividad.
    Tmct0dbs tmct0dbs = new Tmct0dbs();
    tmct0dbs.setNtabla("TMCT0CLI");
    tmct0dbs.setAuxiliar(tmct0client.getCdbrocli());
    tmct0dbs.setTipo(new Character('B'));
    this.tmct0dbsDaoImp.populateAndPersistAndFlush(tmct0dbs);
  }

}
