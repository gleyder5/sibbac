package sibbac.business.wrappers.database.bo;


import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0TipoErDao;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0TipoErBo extends AbstractBo< Tmct0TipoEr, Integer, Tmct0TipoErDao > {

	public List< Tmct0TipoEr > findAll() {
		return dao.findAll();
	}
	
	public List< Tmct0TipoEr >  findByNombre( final String nombre ){
		return dao.findAllByNombre( nombre );
	}


	@Transactional
	public Tmct0TipoEr insertTipoEr( String nombre ) {
		Tmct0TipoEr tipoEr = new Tmct0TipoEr();
		tipoEr.setNombre( nombre );
		tipoEr = dao.save( tipoEr );
		return tipoEr;
	}

	@Transactional
	public boolean deleteById( Integer idTipoEr ) {
		if ( idTipoEr != null ) {
			try {
				dao.delete( idTipoEr );
				return true;
			} catch ( EmptyResultDataAccessException e ) {
				LOG.trace( " Error borrando tipoER con id" + idTipoEr );
			}
		}
		return false;
	}

	@Transactional
	public boolean udpateById( Integer idTipoEr, String nombre ) {
		// Comprobamos si existe el tipo er y lo traemos
		Tmct0TipoEr tipoEr = this.findById( idTipoEr );
		if ( tipoEr != null ) {
			tipoEr.setNombre( nombre );
			return true;
		}
		return false;
	}
}
