package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.dao.Tmct0aloDao;
import sibbac.business.wrappers.database.dao.Tmct0aloDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0ctdDao;
import sibbac.business.wrappers.database.dao.specifications.Tmct0AloSpecifications;
import sibbac.business.wrappers.database.data.AloDesgloseData;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.data.Tmct0AloDataOperacionesEspecialesPartenon;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.dto.TransaccionesDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0aloBo extends AbstractBo<Tmct0alo, Tmct0aloId, Tmct0aloDao> {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0aloBo.class);

  @Autowired
  private Tmct0aloDaoImp daoImpl;

  @Autowired
  private Tmct0bokBo tmct0BokBo;

  @Autowired
  private Tmct0ctgBo ctgBo;

  @Autowired
  private Tmct0ctaBo ctaBo;

  @Autowired
  private Tmct0ctdDao ctdDao;

  @Autowired
  private Tmct0brkBo brkBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0estadoBo estadoBo;

  public List<Tmct0alo> findAllByRefbanif(String refbanif) {
    return dao.findAllByRefbanif(refbanif);
  }

  public List<Tmct0alo> findAllByNuorden(String nuorden) {
    return dao.findAllByNuorden(nuorden);
  }

  public Integer countByNuordenActivosInternacional(String nuorden) {
    return dao.countByNuordenActivosInternacional(nuorden);
  }

  public List<Tmct0alo> findAllByNuordenAndNbooking(String nuorden, String nbooking) {
    return dao.findAllByNuordenAndNbooking(nuorden, nbooking);
  }

  public List<Tmct0aloId> findAllTmct0aloIdByNuordenAndNbooking(String nuorden, String nbooking) {
    return dao.findAllTmct0aloIdByNuordenAndNbooking(nuorden, nbooking);
  }

  public List<Tmct0aloId> findAllTmct0aloIdByNuordenAndNbookingAndCdestadoasig(String nuorden, String nbooking,
      int cdestadoasig) {
    return dao.findAllTmct0aloIdByNuordenAndNbookingAndCdestadoasig(nuorden, nbooking, cdestadoasig);
  }

  public List<Tmct0alo> findAllByNuordenAndNbookingActivosInternacional(String nuorden, String nbooking) {
    return dao.findAllByNuordenAndNbookingActivosInternacional(nuorden, nbooking);
  }

  public Integer getMaxNuparejeByNuoprout(Integer nuoprout) {
    return dao.getMaxNuparejeByNuoprout(nuoprout);
  }

  public List<Tmct0alo> findAllByNbooking(String nbooking) {
    return dao.findAllByNbooking(nbooking);
  }

  public List<Tmct0alo> findAllByNucnfclt(String nucnfclt) {
    return dao.findAllByNucnfclt(nucnfclt);
  }

  public List<Tmct0alo> findAllByRefCliente(String refCliente) {
    return dao.findAllByRefCliente(refCliente);
  }

  public void updateContabilizadoByTipoMovimiento(String tipoMovimiento, boolean contabilizado) {
    dao.updateContabilizadoByTipoMovimiento(tipoMovimiento, contabilizado);
  }

  /**
   * 
   * @param numeroOperacion
   * @param numeroEjecucionMercado
   * @param numeroOrdenMercado
   * @return
   */
  public List<Object[]> findByCdaliasHolder(String numeroOperacion, String numeroEjecucionMercado,
      String numeroOrdenMercado) {
    return dao.findByCdaliasHolder(numeroOperacion, numeroEjecucionMercado, numeroOrdenMercado);
  }

  /**
   * Procesa la búsqueda de Transacciones generica
   * 
   * @param annoFeTrade
   * @param mesFeTrade
   * @param diaFeTrade
   * @param mercado Clave del mercado
   * @return
   */
  public List<TransaccionesDTO> findByFeTradeAndCdmercadAndImgatdsv(Date fcontratacionDesde, Date fcontratacionA,
      String mercado) {
    return daoImpl.findByFeTradeAndCdmercadAndImgatdsv(fcontratacionDesde, fcontratacionA, mercado);
  }

  /**
   * 
   * @param annoFeTrade
   * @param mesFeTrade
   * @param diaFeTrade
   * @param mercado
   * @return
   */
  public List<TransaccionesDTO> findByFeTradeAndCdmercadAndImgatdsvSinOpen(Date fcontratacionDesde,
      Date fcontratacionA, String mercado) {
    return daoImpl.findByFeTradeAndCdmercadAndImgatdsvSinOpen(fcontratacionDesde, fcontratacionA, mercado);
  }

  /**
   * 
   * @param annoFeTrade
   * @param mesFeTrade
   * @param diaFeTrade
   * @param mercado
   * @return
   */
  public List<TransaccionesDTO> findByFeTradeAndCdmercadAndImgatdsvOpen(Date fcontratacionDesde, Date fcontratacionA,
      String mercado) {
    return daoImpl.findByFeTradeAndCdmercadAndImgatdsvOpen(fcontratacionDesde, fcontratacionA, mercado);
  }

  public List<Tmct0alo> findByNuordenAndRfparten(String nuordenDelFichero, String nureford) {
    return dao.findByNuordenAndRfparten(nuordenDelFichero, nureford);
  }

  public List<Tmct0AloData> findDataByNuordenAndRfparten(String nuordenDelFichero, String nureford) {
    return dao.findDataByNuordenAndRfparten(nuordenDelFichero, nureford);
  }

  public List<ControlTitularesDTO> findControlTitularesByAlloId(Tmct0aloId aloId) {
    return dao.findControlTitularesByAlloId(aloId);
  }

  public List<ControlTitularesDTO> findControlTitularesByNuorden(String nuorden) throws PersistenceException {
    return dao.findControlTitularesByNuorden(nuorden);
  } // findDataByNuorden

  public List<ControlTitularesDTO> findControlTitularesByRefCliente(String refCliente) {
    return dao.findControlTitularesByRefCliente(refCliente);
  }

  public List<ControlTitularesDTO> findControlTitularesByRefbanif(String refbanif) {
    return dao.findControlTitularesByRefbanif(refbanif);
  }

  public List<ControlTitularesDTO> findControlTitularesByNuordenAndRefbanif(String nuorden, String refbanif) {
    return dao.findControlTitularesByNuordenAndRefbanif(nuorden, refbanif);
  }

  public List<ControlTitularesDTO> findControlTitularesByFeoperacAndRefbanif(Date feoperac, String refbanif) {
    return dao.findControlTitularesByFeoperacAndRefbanif(feoperac, refbanif);
  }

  public List<ControlTitularesDTO> findControlTitularesByNureford(String nureford) {
    return dao.findControlTitularesByNureford(nureford);
  }

  public List<ControlTitularesDTO> findControlTitularesByNuordenAndCdestadotitLt(String nuorden, Integer cdestadotitLt)
      throws PersistenceException {

    return dao.findControlTitularesByNuordenAndCdestadotitLt(nuorden, cdestadotitLt);

  } // findDataByNuorden

  public List<ControlTitularesDTO> findControlTitularesByNucnfclt(String nucnfclt) {
    return dao.findControlTitularesByNucnfclt(nucnfclt);
  }

  @Transactional
  public List<Tmct0AloData> findDataByNuordenTransactional(String nuorden) throws PersistenceException {
    List<Tmct0AloData> resultList = dao.findDataByNuordenTransactional(nuorden);
    return resultList;
  }

  public Tmct0AloData findDataById(Tmct0aloId id) {
    return dao.findDataById(id);
  }

  public Tmct0alo findByNuordenIgualTitulosBooking(String nuorden) {
    return dao.findByNuordenIgualTitulosBooking(nuorden);
  }

  public Tmct0alo findByNuordenAndCdestadoasig(String nuorden, int cdestadoasig) {
    return dao.findByNuordenAndCdestadoasig(nuorden, cdestadoasig);
  }

  public List<Tmct0alo> findAlosByIdViaSpecifications(final String nuorden, final String nbooking, final String nucnfclt) {
    final List<Tmct0alo> alos = dao.findAll(Tmct0AloSpecifications.listadoFriltrado(nuorden, nbooking, nucnfclt));
    return alos;
  }

  public List<Tmct0alo> findAlosByEstadotit(Integer cdestadotit) {
    return dao.findAlosByEstadotit(cdestadotit);
  }

  public List<Tmct0aloId> findAlosIdByEstadotitPdteIfAndfevalorLte(Date fevalor) {
    return dao.findAlosIdByEstadotitAndfevalorLte(EstadosEnumerados.TITULARIDADES.PDTE_IF.getId(), fevalor);
  }

  public Tmct0alo getAlo(String nuorden, String nbooking, String nucnfclt) {
    return dao.getAlo(nuorden, nbooking, nucnfclt);
  }

  @Transactional
  public Integer updateCdestadoasigByNumordenAndCdestadoasig(List<String> ordenes, int cdestadoasigFrom,
      int cdestadoasigTo) {

    return dao.updateCdestadoasigByNumordenAndCdestadoasig(ordenes, cdestadoasigFrom, cdestadoasigTo, new Date());
  }

  public Integer updateCdestadoasigByTmct0bokIdAndCdestadoasig(Tmct0bokId bokId, int cdestadoasigFrom,
      int cdestadoasigTo) {
    return dao.updateCdestadoasigByTmct0bokIdAndCdestadoasig(bokId, cdestadoasigFrom, cdestadoasigTo, new Date());
  }

  public Integer updateCdestadoasigByTmct0aloId(Tmct0aloId aloId, int cdestadoasigTo) {
    return dao.updateCdestadoasigByTmct0aloId(aloId, cdestadoasigTo, new Date());
  }

  public Integer updateCdestadotitByTmct0bokId(Tmct0bokId bokId, int cdestadotitTo, String cdusuaud) {
    return dao.updateCdestadotitByTmct0bokId(bokId, cdestadotitTo, new Date(), cdusuaud);
  }

  public Integer updateCdestadotitByTmct0aloId(Tmct0aloId alcId, int cdestadoatit) {
    return dao.updateCdestadotitByTmct0aloId(alcId, cdestadoatit, new Date());
  }

  public List<Tmct0bokId> findAllBookingIdContainsTmct0aloByCdestadoasig(int cdestadoasig) {
    return dao.findAllBookingIdContainsTmct0aloByCdestadoasig(cdestadoasig);
  }

  public List<Tmct0bokId> findAllBookingIdContainsTmct0aloByCdestadoasigAndIssrcdiv(int cdestadoasig, char isscrdiv) {
    return dao.findAllBookingIdContainsTmct0aloByCdestadoasigAndIssrcdiv(cdestadoasig, isscrdiv);
  }

  public BigDecimal sumNutitcliActivosByTmct0bokId(Tmct0bokId bookingId) {
    return dao.sumNutitcliByTmct0bokId(bookingId, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
  }

  public BigDecimal sumNutitcliByTmct0bokIdAndCdestadoasig(Tmct0bokId bookingId, int cdestadoasigEq) {
    return dao.sumNutitcliByTmct0bokIdAndCdestadoasig(bookingId, cdestadoasigEq);
  }

  public List<Tmct0alo> findTmct0aloByTmct0bokIdAndCdestadoasig(Tmct0bokId bookingId, int cdestadoasig) {
    return dao.findTmct0aloByTmct0bokIdAndCdestadoasig(bookingId, cdestadoasig);
  }

  public List<Tmct0aloId> findAllTmct0aloIdPdteEnviarMovimientoAsignacionSinTitular(Date sinceDate) {
    List<Tmct0aloId> res = new ArrayList<Tmct0aloId>();

    List<Object[]> l = dao.findAllTmct0aloIdByFeeoperacGtAndCdestadoAsigAndCasepti(sinceDate,
        EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR_SIN_TITULAR.getId(), 'S');

    for (Object[] s : l) {
      res.add(new Tmct0aloId((String) s[2], (String) s[1], (String) s[0]));
    }
    return res;
  }

  /**
   * Recupera el alo principal que se va a repartir en tantos alos como
   * operaciones vengan en el fichero 40.
   * 
   * @param listAloDataOperacionesEspecialesPartenon
   * @param dto
   * @return
   */
  public void recoverEconomicDataToDesgloseDto(
      Tmct0AloDataOperacionesEspecialesPartenon aloDataOperacionesEspecialesPartenon, DesgloseRecordDTO dto) {
    aloDataOperacionesEspecialesPartenon.setNumTitulos(aloDataOperacionesEspecialesPartenon.getNumTitulos().subtract(
        dto.getTitulosEjecutados()));
    if (aloDataOperacionesEspecialesPartenon.getNumTitulos().compareTo(BigDecimal.ZERO) == 0) {
      // Indica que es el último desglose y que por tanto se
      // va la diferencia en la comision para que no haya
      // descuadre.
      aloDataOperacionesEspecialesPartenon.setImcomdev(aloDataOperacionesEspecialesPartenon.getImcomdevOriginal()
          .subtract(aloDataOperacionesEspecialesPartenon.getImcomdevAcumulado()));
      aloDataOperacionesEspecialesPartenon.setImcomisn(aloDataOperacionesEspecialesPartenon.getImcomisnOriginal()
          .subtract(aloDataOperacionesEspecialesPartenon.getImcomisnAcumulado()));

      aloDataOperacionesEspecialesPartenon.setImcamonContrEu(aloDataOperacionesEspecialesPartenon
          .getImcamonContrEuOriginal().subtract(aloDataOperacionesEspecialesPartenon.getImcamonContrEuAcumulado()));

      aloDataOperacionesEspecialesPartenon.setImcomdevAcumulado(aloDataOperacionesEspecialesPartenon
          .getImcomdevAcumulado().add(aloDataOperacionesEspecialesPartenon.getImcomdev()));
      aloDataOperacionesEspecialesPartenon.setImcomisnAcumulado(aloDataOperacionesEspecialesPartenon
          .getImcomisnAcumulado().add(aloDataOperacionesEspecialesPartenon.getImcomisn()));

      dto.setCanCont(new BigDecimal(aloDataOperacionesEspecialesPartenon.getImcamonContrEu().toString()));
      dto.setCorretajes(new BigDecimal(aloDataOperacionesEspecialesPartenon.getImcomisn().toString()));
      dto.setPayerComission(new BigDecimal(aloDataOperacionesEspecialesPartenon.getImcomdev().toString()));
      dto.setLastDesglose(true);
    }
    else {
      BigDecimal numTitulosOriginal = aloDataOperacionesEspecialesPartenon.getNumTitulosOriginal();
      if (numTitulosOriginal != null && numTitulosOriginal.compareTo(BigDecimal.ZERO) > 0) {
        aloDataOperacionesEspecialesPartenon.setImcamonContrEu(FormatDataUtils.roundedScale(
            dto.getTitulosEjecutados().multiply(aloDataOperacionesEspecialesPartenon.getImcamonContrEuOriginal())
                .divide(aloDataOperacionesEspecialesPartenon.getNumTitulosOriginal(), 2, RoundingMode.HALF_DOWN), 2));

        aloDataOperacionesEspecialesPartenon.setImcamonContrEuAcumulado(aloDataOperacionesEspecialesPartenon
            .getImcamonContrEuAcumulado().add(aloDataOperacionesEspecialesPartenon.getImcamonContrEu()));

        aloDataOperacionesEspecialesPartenon.setImcomdev(FormatDataUtils.roundedScale(
            dto.getTitulosEjecutados().multiply(aloDataOperacionesEspecialesPartenon.getImcomdevOriginal())
                .divide(aloDataOperacionesEspecialesPartenon.getNumTitulosOriginal(), 2, RoundingMode.HALF_DOWN), 2));
        aloDataOperacionesEspecialesPartenon.setImcomdevAcumulado(aloDataOperacionesEspecialesPartenon
            .getImcomdevAcumulado().add(aloDataOperacionesEspecialesPartenon.getImcomdev()));

        aloDataOperacionesEspecialesPartenon.setImcomisn(FormatDataUtils.roundedScale(
            dto.getTitulosEjecutados().multiply(aloDataOperacionesEspecialesPartenon.getImcomisnOriginal())
                .divide(aloDataOperacionesEspecialesPartenon.getNumTitulosOriginal(), 2, RoundingMode.HALF_DOWN), 2));

        aloDataOperacionesEspecialesPartenon.setImcomisnAcumulado(aloDataOperacionesEspecialesPartenon
            .getImcomisnAcumulado().add(aloDataOperacionesEspecialesPartenon.getImcomisn()));

        dto.setCanCont(new BigDecimal(aloDataOperacionesEspecialesPartenon.getImcamonContrEu().toString()));
        dto.setCorretajes(new BigDecimal(aloDataOperacionesEspecialesPartenon.getImcomisn().toString()));
        dto.setPayerComission(new BigDecimal(aloDataOperacionesEspecialesPartenon.getImcomdev().toString()));
        dto.setLastDesglose(false);
      }
    }
    dto.setAloDataOperacionesEspecialesPartenon(aloDataOperacionesEspecialesPartenon);
  }

  public void recoverTitlesFromExecutions(
      Tmct0AloDataOperacionesEspecialesPartenon aloDataOperacionesEspecialesPartenon, DesgloseRecordDTO dto) {
    List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> resultado = null;
    if (aloDataOperacionesEspecialesPartenon != null) {
      List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listaEjecucionAlocationData = aloDataOperacionesEspecialesPartenon
          .getListEjecucionAlocacionData();
      resultado = new LinkedList<EjecucionAlocationProcesarDesglosesOpEspecialesDTO>();
      if (CollectionUtils.isNotEmpty(listaEjecucionAlocationData)) {
        BigDecimal numTitulosDesglose = dto.getTitulosEjecutados();
        Iterator<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> iterador = listaEjecucionAlocationData.iterator();
        List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> toRemove = new ArrayList<EjecucionAlocationProcesarDesglosesOpEspecialesDTO>();
        while (iterador.hasNext() && (numTitulosDesglose.compareTo(BigDecimal.ZERO) != 0)) {

          EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionalocationData = iterador.next();
          if (StringHelper.cdBrokerToCode(ejecucionalocationData.getCdbroker().trim()).equals(
              dto.getCodigoBolsaEjecucion())) {

            BigDecimal nutitulosEjecucion = ejecucionalocationData.getNutitulos();
            if (BigDecimal.ZERO.compareTo(nutitulosEjecucion) < 0) {

              EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionalocationDataAux = new EjecucionAlocationProcesarDesglosesOpEspecialesDTO(
                  ejecucionalocationData);
              if (nutitulosEjecucion.compareTo(numTitulosDesglose) <= 0) {
                ejecucionalocationDataAux.setNutitulos(nutitulosEjecucion);
                numTitulosDesglose = numTitulosDesglose.subtract(nutitulosEjecucion);
                ejecucionalocationData.setNutitulos(BigDecimal.ZERO);
                toRemove.add(ejecucionalocationData);
              }
              else {
                ejecucionalocationDataAux.setNutitulos(numTitulosDesglose);
                nutitulosEjecucion = nutitulosEjecucion.subtract(numTitulosDesglose);
                numTitulosDesglose = BigDecimal.ZERO;
                ejecucionalocationData.setNutitulos(nutitulosEjecucion);
              }// else hay mas titulos en la ealo que en el desglose de la
               // operacion especial partenon

              resultado.add(ejecucionalocationDataAux);
            }
          }
        }// fin while
        if (CollectionUtils.isNotEmpty(toRemove)) {
          listaEjecucionAlocationData.removeAll(toRemove);
        }
        dto.setEjecucionesUtilizadas(resultado);

      }
    }
    if (CollectionUtils.isEmpty(resultado)) {
      LOG.warn("No hay ejecuciones para este desglose");
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void revisarEstadosTratandoseOperacionesEspecialesNewTransaction(Tmct0bokId tmct0bokId) {
    LOG.trace("[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction] inicio.");
    BigDecimal nutitcli;
    BigDecimal nutiteje;

    BigDecimal nutitcliPdteOpEsp;

    int countUpdates;
    LOG.debug(
        "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  revisando estados de bookingId {}",
        tmct0bokId);
    nutiteje = tmct0BokBo.findNutitejeById(tmct0bokId);
    if (nutiteje != null) {
      nutitcli = sumNutitcliActivosByTmct0bokId(tmct0bokId);

      if (nutitcli != null && nutitcli.compareTo(nutiteje.multiply(new BigDecimal(2))) == 0) {
        LOG.warn(
            "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  El ultimo hilo se ha dejado el alo original activo {}, tit.book: {} tit.alo: {}",
            tmct0bokId, nutiteje, nutitcli);

        nutitcliPdteOpEsp = sumNutitcliByTmct0bokIdAndCdestadoasig(tmct0bokId,
            EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL.getId());
        if (nutitcliPdteOpEsp != null && nutitcliPdteOpEsp.compareTo(nutiteje) == 0) {
          LOG.warn(
              "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  Dando de baja el alo original... {}",
              tmct0bokId);
          countUpdates = updateCdestadoasigByTmct0bokIdAndCdestadoasig(tmct0bokId,
              EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL.getId(),
              EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());

          if (countUpdates > 0) {
            nutitcli = sumNutitcliActivosByTmct0bokId(tmct0bokId);
            LOG.warn(
                "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  Despues de dar baja alo original {}, tit.book: {} tit.alo: {}",
                tmct0bokId, nutiteje, nutitcli);
          }
        }
      }

      if (nutitcli != null && nutitcli.compareTo(nutiteje) == 0) {
        LOG.debug(
            "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  los allos activos coinciden con bookingId {}",
            tmct0bokId);
        LOG.trace("[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  buscando allos en ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE.");
        countUpdates = updateCdestadoasigByTmct0bokIdAndCdestadoasig(tmct0bokId,
            EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE.getId(),
            EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
        countUpdates += tmct0BokBo.updateCdestadoasigByTmct0bokId(tmct0bokId,
            EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());

        LOG.debug(
            "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  Se han actualizado {} registros.",
            countUpdates);
      }// TITULOS IGUALES
      else {
        LOG.warn(
            "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction]  los allos activos nutiticli {} NO coinciden con bookingId {} nutitieje {}",
            tmct0bokId, nutitcli, nutiteje);
      }
    }
    else {
      LOG.warn(
          "[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction] INCIDENCIA -Recuperando los titulos del booking. {}",
          tmct0bokId);
    }

    LOG.trace("[Tmct0aloBo :: revisarEstadosTratandoseOperacionesEspecialesNewTransaction] fin.");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public boolean isTitulosAlosAltaOk(Tmct0bokId bokId) {
    BigDecimal nutitcliActivos = sumNutitcliActivosByTmct0bokId(bokId);
    BigDecimal nutitejeBooking = tmct0BokBo.findNutitejeById(bokId);
    return nutitcliActivos != null && nutitejeBooking != null && nutitcliActivos.compareTo(nutitejeBooking) == 0;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void updateCentersInternacionalTmct0alo(Tmct0alo tmct0alo, TypeCdestado cdestado, String cdusuaud) {
    boolean isMtf;
    List<Tmct0cta> ctas;
    List<Tmct0ctd> ctds;
    List<Tmct0ctg> ctgs = ctgBo.findAllByNuodenAndNbooking(tmct0alo.getId().getNuorden(), tmct0alo.getId()
        .getNbooking());
    if (CollectionUtils.isNotEmpty(ctgs)) {
      Map<String, Boolean> cachedIsMtf = new HashMap<String, Boolean>();
      TypeCdestado cdestadoCentros;
      for (Tmct0ctg tmct0ctg : ctgs) {
        cdestadoCentros = cdestado;
        if ((cdestado.getValue().equals(TypeCdestado.FOREING_PENDIENTE_RECONCILIAR.getValue()))
            || (cdestado.getValue().equals(TypeCdestado.MTF_NETEO_PENDING.getValue()))) {
          isMtf = brkBo.isCdbrokerMtf(cachedIsMtf, tmct0ctg.getId().getCdbroker());
          // Si MTF y 010 pasar a 015
          if (isMtf && cdestado.getValue().equals(TypeCdestado.FOREING_PENDIENTE_RECONCILIAR.getValue())) {
            cdestadoCentros = TypeCdestado.MTF_NETEO_PENDING;
          }
          // Si NO MTF y 015 pasar a 010
          if (!isMtf && cdestado.getValue().equals(TypeCdestado.MTF_NETEO_PENDING.getValue())) {
            cdestadoCentros = TypeCdestado.FOREING_PENDIENTE_RECONCILIAR;
          }
        }

        ctas = ctaBo.findAllByNuordenAndNbookingAndCdbrokerAndCdmercadAndNucnfclt(tmct0alo.getId().getNuorden(),
            tmct0alo.getId().getNbooking(), tmct0ctg.getId().getCdbroker(), tmct0ctg.getId().getCdmercad(), tmct0alo
                .getId().getNucnfclt());
        if (CollectionUtils.isNotEmpty(ctas)) {
          for (Tmct0cta tmct0cta : ctas) {
            tmct0cta.setTmct0sta(new Tmct0sta(new Tmct0staId(cdestadoCentros.getValue(), TypeCdtipest.TMCT0CTA
                .getValue())));
            tmct0cta.setFhaudit(new Date());
            tmct0cta.setCdusuaud(cdusuaud);
            ctds = tmct0cta.getTmct0ctds();
            for (Tmct0ctd tmct0ctd : ctds) {
              tmct0ctd.setTmct0sta(new Tmct0sta(new Tmct0staId(cdestadoCentros.getValue(), TypeCdtipest.TMCT0CTD
                  .getValue())));
              tmct0ctd.setFhaudit(new Date());
              tmct0ctd.setCdusuaud(cdusuaud);

            }
            ctdDao.save(ctds);
          }
          ctaBo.save(ctas);
        }

        if (ctaBo.isCdestadoInternacionalComplete(tmct0ctg)) {
          tmct0ctg.setTmct0sta(new Tmct0sta(new Tmct0staId(cdestado.getValue(), TypeCdtipest.TMCT0CTG.getValue())));
        }
        else {
          tmct0ctg.setTmct0sta(new Tmct0sta(new Tmct0staId(cdestado.getValueInProcess(), TypeCdtipest.TMCT0CTG
              .getValue())));
        }
        tmct0ctg.setFhaudit(new Date());
        tmct0ctg.setCdusuaud(cdusuaud);
        // if is complete ctg
      }
      ctgBo.save(ctgs);
    }
  }

  @Transactional
  public void ponerEstadoAsignacion(Tmct0alo alo, int estadoAsignacion, String auditUser) {
    Tmct0bok bok = alo.getTmct0bok();

    if (alo.getTmct0estadoByCdestadoasig().getIdestado() != estadoAsignacion) {
      alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
      alo.getTmct0estadoByCdestadoasig().setIdestado(estadoAsignacion);
      alo.setFhaudit(new Date());
      alo.setCdusuaud(auditUser);
    }
    if (bok.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      bok.setTmct0estadoByCdestadoasig(new Tmct0estado());
      bok.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      bok.setFhaudit(new Date());
      bok.setCdusuaud(auditUser);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void ponerEstadoAsignacionPorExcepcion(Tmct0aloId id, int estado, String msg, String auditUser)
      throws SIBBACBusinessException {
    Tmct0alo alo = findById(id);
    Tmct0estado estadoHb = estadoBo.findById(estado);
    LOG.warn("[ponerEstadoPorExcepcion] {}-{}", id, msg);
    logBo.insertarRegistro(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig().getNombre(), msg,
        auditUser);
    msg = String.format("Cambiado estado alo a '%s'-'%s'", estado, estadoHb.getNombre());
    logBo.insertarRegistro(alo, new Date(), new Date(), "", alo.getTmct0estadoByCdestadoasig().getNombre(), msg,
        auditUser);
    alo.setTmct0estadoByCdestadoasig(estadoHb);
    alo.setFhaudit(new Date());
    alo.setCdusuaud(auditUser);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public AloDesgloseData findAloDesgloseData(Tmct0aloId id) {
    return dao.findAloDesgloseData(id);
  }

} // Tmct0aloBo

