package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0referenciatitular;

@Repository
public interface Tmct0referenciatitularDao extends JpaRepository<Tmct0referenciatitular, Long> {

  @Query(value = "SELECT NEXTVAL FOR REFERENCIA_TITULAR_SEQ FROM SYSIBM.SYSDUMMY1 ", nativeQuery = true)
  Long getNextValForReferenciaTitular();

  Tmct0referenciatitular findByCdreftitularptiAndReferenciaAdicional(String cdreftitularpti, String referenciaAdicional);

}
