package sibbac.business.wrappers.common.fileReader;


import java.util.Map;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.LineTokenizer;

import sibbac.business.wrappers.database.model.RecordBean;


public abstract class SibbacFixedLengthFileReader< T extends RecordBean > extends SibbacFileReader< T > {

	public SibbacFixedLengthFileReader( String fileName ) {
		super( fileName );
		final PatternMatchingCompositeLineMapper< T > compositeLineMapper = new PatternMatchingCompositeLineMapper< T >();
		compositeLineMapper.setTokenizers( initializeLineTokenizers() );
		compositeLineMapper.setFieldSetMappers( initializeFieldSetMappers() );
		this.lineMapper = compositeLineMapper;

	}

	public SibbacFixedLengthFileReader( ) {
		super(  );
		final PatternMatchingCompositeLineMapper< T > compositeLineMapper = new PatternMatchingCompositeLineMapper< T >();
		compositeLineMapper.setTokenizers( initializeLineTokenizers() );
		compositeLineMapper.setFieldSetMappers( initializeFieldSetMappers() );
		this.lineMapper = compositeLineMapper;

	}


	protected abstract Map< String, LineTokenizer > initializeLineTokenizers();

	protected abstract Map< String, FieldSetMapper< T > > initializeFieldSetMappers();

}
