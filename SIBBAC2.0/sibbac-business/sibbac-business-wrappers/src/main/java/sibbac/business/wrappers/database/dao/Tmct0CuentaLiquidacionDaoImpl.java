package sibbac.business.wrappers.database.dao;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.CuentaLiquidacionData;

@Repository
public class Tmct0CuentaLiquidacionDaoImpl {

	private static final String	TAG	= Tmct0CuentaLiquidacionDaoImpl.class.getName();
	private static final Logger	LOG	= LoggerFactory.getLogger( TAG );
	@PersistenceContext
	private EntityManager		em;

	@SuppressWarnings( "unchecked" )
	public List< CuentaLiquidacionData > findAllFiltered( Map< String, Serializable > filters ) {
		List< CuentaLiquidacionData > resultList = new LinkedList< CuentaLiquidacionData >();
		LOG.debug( TAG + ": Entrando en findAllFiltered" );
		String primarySelect = "SELECT new sibbac.business.wrappers.database.data.CuentaLiquidacionData( c ) ";
		StringBuilder from = new StringBuilder( "FROM Tmct0CuentaLiquidacion c " );
		String fromRelMkt = ", RelacionCuentaMercado rel ";
		String whereInRelMkt = "rel.relacionCuentaMercadoPK.idCtaLiquidacion.id = c.id AND rel.relacionCuentaMercadoPK.idMercado.id in:idMercado ";
		String fromCtaCmp = ", Tmct0CuentasDeCompensacion cc ";
		String whereCmp = "cc.tmct0CuentaLiquidacion.id = c.id";
		String whereInCtaCmp = " AND cc.idCuentaCompensacion in:idsCuentaCompensacion ";
		String whereTipoCuentaLiq = " AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo = :cdCodigo ";
		StringBuilder select = new StringBuilder( primarySelect );
		//
		StringBuilder querySb = new StringBuilder();
		StringBuilder where = costructCriteria( filters, from, fromRelMkt, whereInRelMkt, fromCtaCmp, whereCmp,whereInCtaCmp, whereTipoCuentaLiq );
		Query query = null;
		try {
			querySb.append( select ).append( from ).append( where );
			query = em.createQuery( querySb.toString() );
			String nameParam = "";
			for ( Map.Entry< String, Serializable > entry : filters.entrySet() ) {
				if ( entry.getValue() != null ) {
					if ( entry.getKey().indexOf( ".id" ) > 0 ) {
						nameParam = entry.getKey().replace( ".", "_" );
						query.setParameter( nameParam, entry.getValue() );
					} else {
						query.setParameter( entry.getKey(), entry.getValue() );
					}
				}
			}
			resultList.addAll( query.getResultList() );
		} catch ( PersistenceException | IllegalArgumentException ex ) {
			LOG.error( ex.getMessage(), ex );
		}
		return resultList;
	}

	private StringBuilder costructCriteria( Map< String, Serializable > filters, StringBuilder from, String fromRelMkt,
			String whereInRelMkt, String fromCtaCmp, String whereCmp,String whereInCtaCmp, String  whereTipoCuentaLiq) {
		String key;
		String criteria;
		StringBuilder where = new StringBuilder();
		boolean addedWhere = false;
		boolean bFromCtaCmp = false;
		for ( Map.Entry< String, Serializable > entry : filters.entrySet() ) {
			if ( entry.getValue() != null ) {
				if ( !addedWhere ) {
					addedWhere = true;
					where.append( "WHERE " );
				} else {
					where.append( "AND " );
				}
				if ( entry.getKey().equals( "idMercado" ) ) {
					from.append( fromRelMkt );
					where.append( whereInRelMkt );
				} else if ( entry.getKey().equals( "idsCuentaCompensacion" ) ) {
					if (!bFromCtaCmp)
					{
						from.append( fromCtaCmp );
						bFromCtaCmp = true;
						whereInCtaCmp=whereCmp +whereInCtaCmp;
					}//if (!bFromCtaCmp)
					
					where.append( whereInCtaCmp );
			    } else if ( entry.getKey().equals( "cdCodigo" ) ) {
			    	if (!bFromCtaCmp)
					{
						from.append( fromCtaCmp );
						bFromCtaCmp = true;
						whereTipoCuentaLiq=whereCmp +whereTipoCuentaLiq;
					}//if (!bFromCtaCmp)
					
					where.append( whereTipoCuentaLiq );
				}
				else {
					key = entry.getKey();
					// si es un campo de otra tabla, se prepara el nombre del
					// parametro para que no contenga el . cambiando el punto
					// por _
					criteria = "c." + key + " =:" + ( ( key.indexOf( "." ) != -1 ) ? key.replace( ".", "_" ) + " " : key + " " );
					where.append( criteria );
				}
			}
		}
		return where;
	}
}
