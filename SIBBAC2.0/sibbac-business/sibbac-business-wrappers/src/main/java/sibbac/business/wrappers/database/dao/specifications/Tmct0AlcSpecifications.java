package sibbac.business.wrappers.database.dao.specifications;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;


public class Tmct0AlcSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( Tmct0AlcSpecifications.class );

	public static Specification< Tmct0alc > nuorden( final String nuorden ) {
		return new Specification< Tmct0alc >() {

			public Predicate toPredicate( Root< Tmct0alc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nuordenPredicate = null;
				LOG.trace( "[Tmct0alcSpecifications::nuorden] [nuorden=={}]", nuorden );
				if ( nuorden != null ) {
					nuordenPredicate = builder.equal( root.< Tmct0alcId > get( "id" ).<String> get("nuorden"), nuorden );
				}
				return nuordenPredicate;
			}
		};
	}

	public static Specification< Tmct0alc > nbooking( final String nbooking ) {
		return new Specification< Tmct0alc >() {

			public Predicate toPredicate( Root< Tmct0alc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbookingPredicate = null;
				LOG.trace( "[Tmct0alcSpecifications::nbooking] [nbooking=={}]", nbooking );
				if ( nbooking != null ) {
					nbookingPredicate = builder.equal( root.< Tmct0alcId > get( "id" ).<String> get("nbooking"), nbooking );
				}
				return nbookingPredicate;
			}
		};
	}

	public static Specification< Tmct0alc > nucnfliq( final Short nucnfliq ) {
		return new Specification< Tmct0alc >() {

			public Predicate toPredicate( Root< Tmct0alc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nucnfliqPredicate = null;
				LOG.trace( "[Tmct0alcSpecifications::nucnfliq] [nucnfliq=={}]", nucnfliq );
				if ( nucnfliq != null ) {
					nucnfliqPredicate = builder.equal( root.< Tmct0alcId > get( "id" ).<Short> get("nucnfliq"), nucnfliq );
				}
				return nucnfliqPredicate;
			}
		};
	}
	
	public static Specification< Tmct0alc > nucnfclt( final String nucnfclt ) {
		return new Specification< Tmct0alc >() {

			public Predicate toPredicate( Root< Tmct0alc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nucnfcltPredicate = null;
				LOG.trace( "[Tmct0alcSpecifications::nucnfclt] [nucnfclt=={}]", nucnfclt );
				if ( nucnfclt != null ) {
					nucnfcltPredicate = builder.equal( root.< Tmct0alcId > get( "id" ).<String> get("nucnfclt"), nucnfclt );
				}
				return nucnfcltPredicate;
			}
		};
	}
	
	public static Specification< Tmct0alc > listadoFriltrado( final String nuorden, final String nbooking, final Short nucnfliq,
			final String nucnfclt ) {
		return new Specification< Tmct0alc >() {

			public Predicate toPredicate( Root< Tmct0alc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0alc > specs = null;

				if ( nuorden != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nuorden( nuorden ) );
					} else {
						specs = specs.and( Specifications.where( nuorden( nuorden ) ) );
					}

				}

				if ( nbooking != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbooking( nbooking ) );
					} else {
						specs = specs.and( Specifications.where( nbooking( nbooking ) ) );
					}

				}

				if ( nucnfliq != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nucnfliq( nucnfliq ) );
					} else {
						specs = specs.and( Specifications.where( nucnfliq( nucnfliq ) ) );
					}

				}

				if ( nucnfclt != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nucnfclt( nucnfclt ) );
					} else {
						specs = specs.and( Specifications.where( nucnfclt( nucnfclt ) ) );
					}

				}
				
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

}
