package sibbac.business.wrappers.tasks.thread.exception;

import java.nio.file.Path;

import org.springframework.batch.item.ItemWriter;

import sibbac.common.SIBBACBusinessException;

public class ItemWriterMultithreadException extends SIBBACBusinessException {

  /**
   * 
   */
  private static final long serialVersionUID = -2594279621739758393L;

  private ItemWriter<?> writer;
  private Path pathFile;

  public ItemWriterMultithreadException() {
  }

  public ItemWriterMultithreadException(String message,
                                        Throwable cause,
                                        boolean enableSuppression,
                                        boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public ItemWriterMultithreadException(String message, Throwable cause) {
    super(message, cause);
  }

  public ItemWriterMultithreadException(String message) {
    super(message);
  }

  public ItemWriterMultithreadException(Throwable cause) {
    super(cause);
  }

  public ItemWriter<?> getWriter() {
    return writer;
  }

  public Path getPathFile() {
    return pathFile;
  }

  public void setWriter(ItemWriter<?> writer) {
    this.writer = writer;
  }

  public void setPathFile(Path pathFile) {
    this.pathFile = pathFile;
  }

}
