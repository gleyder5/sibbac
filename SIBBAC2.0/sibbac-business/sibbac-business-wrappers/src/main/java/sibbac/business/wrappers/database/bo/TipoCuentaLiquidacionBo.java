package sibbac.business.wrappers.database.bo;


import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.TipoCuentaLiquidacionDao;
import sibbac.business.wrappers.database.data.TipoCuentaLiquidacionData;
import sibbac.business.wrappers.database.model.TipoCuentaLiquidacion;
import sibbac.database.bo.AbstractBo;


@Service
public class TipoCuentaLiquidacionBo extends AbstractBo< TipoCuentaLiquidacion, Long, TipoCuentaLiquidacionDao > {

	public List< TipoCuentaLiquidacionData > findAllData() {
		List< TipoCuentaLiquidacionData > resultList = new LinkedList< TipoCuentaLiquidacionData >();
		resultList.addAll( dao.findAllData() );
		return resultList;
	}
}
