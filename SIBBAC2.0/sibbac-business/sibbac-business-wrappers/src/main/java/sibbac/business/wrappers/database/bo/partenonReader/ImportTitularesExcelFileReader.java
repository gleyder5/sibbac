package sibbac.business.wrappers.database.bo.partenonReader;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.bo.partenonReader.ImportTitularesExcelFieldSetMapper.ImportExcelFields;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;

@Component(value = "importTitularesExcelFileReader")
public class ImportTitularesExcelFileReader extends PartenonFileReader {

  protected static final Logger LOG = LoggerFactory.getLogger(PartenonFileReader.class);

  // }

  protected Map<String, LineTokenizer> initializeLineTokenizers() {
    LOG.trace("[ImportTitularesExcelFileReader :: initializeLineTokenizers] Init");
    final Map<String, LineTokenizer> tokenizers = new HashMap<String, LineTokenizer>();
    final DelimitedLineTokenizer partenonLineTokenizer = new DelimitedLineTokenizer();
    partenonLineTokenizer.setNames(ImportExcelFields.getFieldNames());
    partenonLineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_TAB);
    partenonLineTokenizer.setStrict(false);
    tokenizers.put("*", partenonLineTokenizer);
    LOG.trace("[ImportTitularesExcelFileReader :: initializeLineTokenizers] Fin");
    return tokenizers;

  }

  protected Map<String, FieldSetMapper<OperacionEspecialRecordBean>> initializeFieldSetMappers() {
    LOG.trace("[ImportTitularesExcelFileReader :: initializeFieldSetMappers] Init");
    final Map<String, FieldSetMapper<OperacionEspecialRecordBean>> fieldSetMappers = new HashMap<String, FieldSetMapper<OperacionEspecialRecordBean>>();
    fieldSetMappers.put("*", new ImportTitularesExcelFieldSetMapper<PartenonRecordBean>());
    LOG.trace("[ImportTitularesExcelFileReader :: initializeFieldSetMappers] Fin");
    return fieldSetMappers;

  }

}
