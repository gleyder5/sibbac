package sibbac.business.wrappers.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.data.InformeOpercacionData;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.export.ExportBuilder;
import sibbac.common.export.ExportLines;
import sibbac.common.export.LinesSet;

@Component
public class InformeOperacionesExcelBuilder extends ExportBuilder {
    private static final Logger LOG = LoggerFactory
	    .getLogger(InformeOperacionesExcelBuilder.class);

    public InformeOperacionesExcelBuilder() {
    }

    /***************** METODOS ********************/

    public ByteArrayOutputStream generaInformeOperaciones(
	    final String nombreHojaExcel,
	    final List<InformeOpercacionData> lineas,
	    final Map<String, String> lineasCabeceras)
	    throws SIBBACBusinessException {

	// Check data...
	if (lineas == null || lineas.isEmpty()) {
	    throw new SIBBACBusinessException("No data received!");
	}

	ByteArrayOutputStream os = null;
	HSSFWorkbook workbook = new HSSFWorkbook();

	// La hoja
	String name = (nombreHojaExcel != null && nombreHojaExcel.length() > 0) ? nombreHojaExcel
		: "Informe de Operaciones";
	final HSSFSheet sheet = workbook.createSheet(name);
	
	// Headers...
	int rowNum = escribirCabeceraInformeOperaciones(lineasCabeceras, sheet);

	escribirLineasInformeOperaciones(lineas, rowNum, sheet, workbook);
	/*try {
	    FileOutputStream fos =new FileOutputStream(new File("/Users/fjarquellada/Desktop/informe.xls"));
	    workbook.write(fos);
	} catch (IOException ex) {
	    LOG.error(ex.getMessage(), ex);
	}*/
	// Export...
	os = this.getBaos(workbook);
	if (workbook != null) {
	    try {
		workbook.close();
	    } catch (IOException e) {
		// Nothing
	    }
	}
	return os;
    }

    private int escribirCabeceraInformeOperaciones(
	    final Map<String, String> lineasCabeceras, final HSSFSheet sheet) {

	int rowNum = 0;
	int cellNum = 0;
	final Row fila = sheet.createRow(rowNum++);
	if (MapUtils.isNotEmpty(lineasCabeceras)) {
	    String header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_FECHA_OPERACION");
	    Cell celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras.get("INF_OPERACIONES_XLS_FECHA_VALOR");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_NUESTRA_REFERENCIA");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras.get("INF_OPERACIONES_XLS_COMPRA_VENTA");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras.get("INF_OPERACIONES_XLS_ISIN");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras.get("INF_OPERACIONES_XLS_NOMBRE_VALOR");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_MERCADO_CENTRO_CONTRATACION");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_TOTAL_TITULOS_EJEC_POR_ORDEN");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_TITULAR_SUBCUENTA");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_TOTAL_PRECIO_MEDIO_NETO_DE_LA_ORDEN");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_CORRETAJE_TOTAL_ORDEN");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_ESTADO_OPERACION");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);

	    header = lineasCabeceras
		    .get("INF_OPERACIONES_XLS_DATO_DISCREPANTE");
	    celda = fila.createCell(cellNum++);
	    celda.setCellValue(header);
	}
	return rowNum;
    }

    private void escribirLineasInformeOperaciones(final List<InformeOpercacionData> lineas,
	    int rowNum, final HSSFSheet sheet, HSSFWorkbook workbook) {
	// Lineas...
	CellStyle cellStyle = workbook.createCellStyle();
	CreationHelper createHelper = workbook.getCreationHelper();
	cellStyle.setDataFormat(
	    createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

	for (final InformeOpercacionData linea : lineas) {
	    int cellnum = 0;
	    Row fila = sheet.createRow(rowNum++);
	    //fecha op
	    Cell celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getFechaOperación());
	    celda.setCellStyle(cellStyle);
	    //fecha val
	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getFechaValor());
	    celda.setCellStyle(cellStyle);
	    //
	    celda = fila.createCell(cellnum++);
	    StringBuilder referencia = new StringBuilder(linea.getNuestraReferencia().getNbooking())
	    .append("/")
	    .append(linea.getNuestraReferencia().getNucnfclt())
	    .append("/")
	    .append(linea.getNuestraReferencia().getNucnfliq());
	    celda.setCellValue( referencia.toString() );

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getCompraOVenta());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getIsin());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getNombreDelValor());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getMercado());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getTotalTitulos().doubleValue());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getTitularOSubcuenta());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getPrecioMedio().doubleValue());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getCorretaje().doubleValue());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getEstadoDeLaOperacion());

	    celda = fila.createCell(cellnum++);
	    celda.setCellValue(linea.getDatoDiscrepante());
	}
    }

    /********* METODOS EXTENDIDOS ***********/
    @Override
    public Object generate(String name, Object target,
	    Map<String, ? extends Object> model) throws IOException {

	return this.generate(name, target, model, null);
    }

    @Override
    public Object generate(String name, Object target,
	    Map<String, ? extends Object> model, List<String> dataSets)
	    throws IOException {
	HSSFWorkbook workbook = (HSSFWorkbook) target;
	if (model != null) {
	    List<List<LinesSet>> lineas = ExportLines.getLinesFor(model,
		    dataSets);

	    if (lineas == null) {
		LOG.trace("No data!");
		Sheet sheet = workbook.createSheet(getNameForSheet(name));
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		cell.setCellValue("No se han recibido datos");
	    } else {
		// El "name" es el "servicio.comando" que esperamos,
		// normalmente.
		LOG.trace("Nombre de la hoja excel principal [name=={}]", name);

		// Partimos del LinesSet "root".
		// Primero localizamos el "root" para pintarlo en la primera
		// hoja.
		LinesSet root = null;
		String sheetName = null;
		int n = 0;
		for (List<LinesSet> conjunto : lineas) {
		    root = ExportLines.findRootLineSet(conjunto);
		    if (root != null) {
			n++;
			// A pintar en la hoja.
			sheetName = "Sheet " + n + " " + name;
			this.pintaDatos(workbook,
				workbook.createSheet(sheetName), root, conjunto);
		    }
		}
	    }

	} else {
	    LOG.trace("No data!");
	    Sheet sheet = workbook.createSheet(getNameForSheet(name));
	    Row row = sheet.createRow(0);
	    Cell cell = row.createCell(0);
	    cell.setCellValue("No se han recibido datos");
	}
	return workbook;
    }

    @Override
    public ByteArrayOutputStream getBaos(Object target) {
	if (target == null) {
	    return null;
	}

	HSSFWorkbook workbook = (HSSFWorkbook) target;
	ByteArrayOutputStream baos = new ByteArrayOutputStream(
		INITIAL_BAOS_CAPACITY);
	try {
	    workbook.write(baos);
	    baos.flush();
	    baos.close();
	} catch (Exception e) {
	    LOG.error("> ERROR({}): Errors generating the XLS: {}", e
		    .getClass().getName(), e.getMessage());
	} finally {
	    if (workbook != null) {
		try {
		    workbook.close();
		} catch (IOException e) {
		    // Nothing
		}
	    }
	}
	return baos;
    }

}
