package sibbac.business.wrappers.tasks.thread;

public enum WrapperFileReaderFieldType {

  NUMERIC,
  DATE,
  TIME,
  TIMESTAMP,
  STRING,
  CHAR;

}
