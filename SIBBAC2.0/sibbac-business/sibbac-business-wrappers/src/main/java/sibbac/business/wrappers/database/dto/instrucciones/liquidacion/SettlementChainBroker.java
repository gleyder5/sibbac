package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;

import sibbac.business.fase0.database.model.Tmct0blq;

public class SettlementChainBroker implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 2631681278496149060L;

  private String btpsettle = "";

  // local custodian
  private String bcdloccus = "";
  private String blcusbic = "";
  private String bacloccus = "";
  private String bidloccus = "";

  // global custodian
  private String bcdglocus = "";
  private String bgcusbic = "";
  private String bacglocus = "";
  private String bidglocus = "";

  // beneficiario
  private String bcdbenefi = "";
  private String bcdbenbic = "";
  private String bacbenefi = "";
  private String bidbenefi = "";

  private String cdbicmar = "";
  private String issendbe = "";

  private String cdloccus = "";
  private String cdglocus = "";
  private String cdbenefi = "";

  public SettlementChainBroker(SettlementDataBroker sdb) {
    if (sdb.isDataBroker()) {

      final Tmct0blq tmct0blq = sdb.getTmct0blq();

      btpsettle = tmct0blq.getId().getTpsettle();

      //
      // cdbicmar = fmeg0chaSdo.getCdbicmar().trim();
      cdbicmar = tmct0blq.getCdbicmar();

      // issendbe = fmeg0chaSdo.getIssendbe().trim();
      issendbe = new String("1");

      //
      // cdloccus = fmeg0chaSdo.getCdloccus();
      cdloccus = tmct0blq.getBcdloccus();
      cdglocus = tmct0blq.getBcdglocus();
      cdbenefi = tmct0blq.getBcdbenefi();

      bcdloccus = tmct0blq.getBcdloccus();
      // blcusbic = fmeg0entSdo.getCdbiccid();
      blcusbic = tmct0blq.getBlcusbic();
      // bacloccus = fmeg0chaSdo.getAcloccus();
      bacloccus = tmct0blq.getBacloccus();
      // bidloccus = fmeg0chaSdo.getIdloccus();
      bidloccus = tmct0blq.getBidloccus();

      bcdbenefi = tmct0blq.getBcdbenefi();
      bcdbenbic = tmct0blq.getBcdbenbic();
      bacbenefi = tmct0blq.getBacbenefi();
      bidbenefi = tmct0blq.getBidbenefi();
    }
  }

  public String getBtpsettle() {
    return btpsettle.trim();
  }

  public String getBcdloccus() {
    return bcdloccus.trim();
  }

  public String getBlcusbic() {
    return blcusbic.trim();
  }

  public String getBacloccus() {
    return bacloccus.trim();
  }

  public String getBidloccus() {
    return bidloccus.trim();
  }

  public String getBcdglocus() {
    return bcdglocus.trim();
  }

  public String getBgcusbic() {
    return bgcusbic.trim();
  }

  public String getBacglocus() {
    return bacglocus.trim();
  }

  public String getBidglocus() {
    return bidglocus.trim();
  }

  public String getBcdbenefi() {
    return bcdbenefi.trim();
  }

  public String getBcdbenbic() {
    return bcdbenbic.trim();
  }

  public String getBacbenefi() {
    return bacbenefi.trim();
  }

  public String getBidbenefi() {
    return bidbenefi.trim();
  }

  public String getCdbicmar() {
    return cdbicmar.trim();
  }

  public String getIssendbe() {
    return issendbe.trim();
  }

  public String getCdloccus() {
    return cdloccus.trim();
  }

  public String getCdglocus() {
    return cdglocus.trim();
  }

  public String getCdbenefi() {
    return cdbenefi.trim();
  }

}
