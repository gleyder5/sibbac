package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0caseoperacionejeDao;
import sibbac.business.wrappers.database.data.Tmct0caseoperacionejeData;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0caseoperacionejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0caseoperacionejeBo extends AbstractBo<Tmct0caseoperacioneje, Long, Tmct0caseoperacionejeDao> {

	/**
	 * operaciones por isin y sentido
	 * 
	 * @param cdoperacionecc
	 * @param isin
	 * @param sentido
	 * @return noCasado
	 */
	public Tmct0caseoperacioneje findNoCasados(String cdoperacionecc, String isin, Character sentido, String camara) {
		Tmct0caseoperacioneje noCasado = dao.findByFiltro(cdoperacionecc, isin, sentido, camara);
		return noCasado;
	}

	/**
	 * operaciones por isin y sentido usando ins
	 * 
	 * @param cdoperacionecc
	 * @param isin
	 * @param sentido
	 * @return noCasado
	 */
	@Transactional
	public List<Tmct0caseoperacioneje> findOperacionesWithIns(List<String> cdoperacionecc, List<String> isin) {
		List<Tmct0caseoperacioneje> noCasados = dao.findOperacionesWithIns(cdoperacionecc, isin);
		return noCasados;
	}

	public List<Tmct0caseoperacionejeData> findOperacionesDataWithIns(List<String> cdoperacionecc, List<String> isin) {
		List<Tmct0caseoperacionejeData> noCasados = dao.findOperacionesDataWithIns(cdoperacionecc, isin);
		return noCasados;
	}

	/**
	 * Metodo para obtener el idcase y los datos de mercado dada una ejecucion
	 * 
	 * @param nuordenmkt
	 * @param nuexemkt
	 * @param fcontratacion
	 * @param cdoperacionmkt
	 * @param cdisin
	 * @param cdsegmento
	 * @param cdsentido
	 * @param cdmiembromkt
	 * @return
	 */
	public Tmct0caseoperacionejeInformacionMercadoCaseDTO findCaseoperacionEjeInformacionMercado(String nuordenmkt, String nuexemkt,
			Date fcontratacion, String cdoperacionmkt, String cdisin, String cdsegmento, Character cdsentido, String cdmiembromkt) {
		return this.dao.findCaseoperacionEjeInformacionMercado(nuordenmkt, nuexemkt, fcontratacion, cdoperacionmkt, cdisin, cdsegmento,
				cdsentido, cdmiembromkt);
	}

	/**
	 * Actualiza el numero de titulos pendientes de case
	 * 
	 * @param idcase
	 * @param nutitPendCase
	 * @param fAudit
	 */
	public Integer updateTitulosPendientesCase(long idcase, BigDecimal nutitPendCase, Date fAudit) {
		return dao.updateTitulosPendientesCase(idcase, nutitPendCase, fAudit);
	}

}
