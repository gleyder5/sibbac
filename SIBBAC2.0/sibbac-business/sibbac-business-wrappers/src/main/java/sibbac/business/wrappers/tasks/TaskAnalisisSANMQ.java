package sibbac.business.wrappers.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.common.fileWriter.SANMQAnalizer;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

//@SIBBACJob(group = Task.GROUP_COMUNICACIONES.NAME,
//           name = Task.GROUP_COMUNICACIONES.JOB_ENTRADA_SAN_MQ,
//           interval = 1,
//           delay = 0,
//           intervalUnit = IntervalUnit.MINUTE)
@Component(value = "taskAnalisisSANMQ")
public class TaskAnalisisSANMQ extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private SANMQAnalizer analizer;
  @Value("${MQ.partenon.desgloses.in.folder}")
  String folderName;

  @Value("${MQ.partenon.desgloses.in.file}")
  String fileName;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    analizer.analizeFile(fileName, "file://" + folderName);
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_OPERACIONES_ESPECIALES;
  }
}
