package sibbac.business.wrappers.database.dao;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Netting;


@Repository
public interface NettingDao extends JpaRepository< Netting, Long > {

	@Query( "SELECT N FROM Netting N "
			+ "WHERE EXISTS (SELECT A FROM AlcOrdenes A, Tmct0alc alc WHERE A.netting = N "
	        + "AND A.nbooking = alc.id.nbooking "
			+ "AND A.nuorden = alc.id.nuorden "
			+ "AND A.nucnfliq = alc.id.nucnfliq "
			+ "AND A.nucnfclt = alc.id.nucnfclt "
			+ "AND alc.estadoentrec is not null) "
			+ "AND ('N'=:filtroTituloNeteado or N.nutituloNeteado=:nutituloNeteado)  "
			+ "AND ('N'=:filtrofcontratacionDe or fechaContratacion >= :fcontratacionDe) "
			+ "AND ('N'=:filtrofcontratacionA or fechaContratacion <= :fcontratacionA)  "
			+ "AND ('N'=:filtrofliquidacionDe or fechaOperacion >= :fliquidacionDe) "
			+ "AND ('N'=:filtrofliquidacionA or fechaOperacion <= :fliquidacionA) " + "AND ('N'=:filtroAlias or N.cdalias=:alias) "
			+ "AND ('N'=:filtroIsin or N.cdisin=:isin) " )
	List< Netting > findByConsulta( @Param( "nutituloNeteado" ) BigDecimal titulosneteados,
			@Param( "filtroTituloNeteado" ) String filtroTituloNeteado, @Param( "fcontratacionDe" ) Date fcontratacionDe,
			@Param( "filtrofcontratacionDe" ) String filtrofcontratacionDe, @Param( "fcontratacionA" ) Date fcontratacionA,
			@Param( "filtrofcontratacionA" ) String filtrofcontratacionA, @Param( "fliquidacionDe" ) Date fliquidacionDe,
			@Param( "filtrofliquidacionDe" ) String filtrofliquidacionDe, @Param( "fliquidacionA" ) Date fliquidacionA,
			@Param( "filtrofliquidacionA" ) String filtrofliquidacionA, @Param( "alias" ) String alias,
			@Param( "filtroAlias" ) String filtroAlias, @Param( "isin" ) String isin, @Param( "filtroIsin" ) String filtroIsin );
	
	@Query( "SELECT N FROM Netting N "
            + "WHERE EXISTS (SELECT A FROM AlcOrdenes A, Tmct0alc alc WHERE A.netting = N "
            + "AND A.nbooking = alc.id.nbooking "
            + "AND A.nuorden = alc.id.nuorden "
            + "AND A.nucnfliq = alc.id.nucnfliq "
            + "AND A.nucnfclt = alc.id.nucnfclt "
            + "AND alc.estadoentrec is not null) "
			+ "AND ('N'=:filtroTituloNeteado or N.nutituloNeteado=:nutituloNeteado)  "
			+ "AND ('N'=:filtrofcontratacionDe or fechaContratacion >= :fcontratacionDe) "
			+ "AND ('N'=:filtrofcontratacionA or fechaContratacion <= :fcontratacionA)  "
			+ "AND ('N'=:filtrofliquidacionDe or fechaOperacion >= :fliquidacionDe) "
			+ "AND ('N'=:filtrofliquidacionA or fechaOperacion <= :fliquidacionA) " + "AND ('N'=:filtroAlias or N.cdalias<>:alias) "
			+ "AND ('N'=:filtroIsin or N.cdisin<>:isin) " )
	List< Netting > findByConsultaNotAliasNotIsin( @Param( "nutituloNeteado" ) BigDecimal titulosneteados,
			@Param( "filtroTituloNeteado" ) String filtroTituloNeteado, @Param( "fcontratacionDe" ) Date fcontratacionDe,
			@Param( "filtrofcontratacionDe" ) String filtrofcontratacionDe, @Param( "fcontratacionA" ) Date fcontratacionA,
			@Param( "filtrofcontratacionA" ) String filtrofcontratacionA, @Param( "fliquidacionDe" ) Date fliquidacionDe,
			@Param( "filtrofliquidacionDe" ) String filtrofliquidacionDe, @Param( "fliquidacionA" ) Date fliquidacionA,
			@Param( "filtrofliquidacionA" ) String filtrofliquidacionA, @Param( "alias" ) String alias,
			@Param( "filtroAlias" ) String filtroAlias, @Param( "isin" ) String isin, @Param( "filtroIsin" ) String filtroIsin );

	
	@Query( "SELECT N FROM Netting N "
            + "WHERE EXISTS (SELECT A FROM AlcOrdenes A, Tmct0alc alc WHERE A.netting = N "
            + "AND A.nbooking = alc.id.nbooking "
            + "AND A.nuorden = alc.id.nuorden "
            + "AND A.nucnfliq = alc.id.nucnfliq "
            + "AND A.nucnfclt = alc.id.nucnfclt "
            + "AND alc.estadoentrec is not null) "
			+ "AND ('N'=:filtroTituloNeteado or N.nutituloNeteado=:nutituloNeteado)  "
			+ "AND ('N'=:filtrofcontratacionDe or fechaContratacion >= :fcontratacionDe) "
			+ "AND ('N'=:filtrofcontratacionA or fechaContratacion <= :fcontratacionA)  "
			+ "AND ('N'=:filtrofliquidacionDe or fechaOperacion >= :fliquidacionDe) "
			+ "AND ('N'=:filtrofliquidacionA or fechaOperacion <= :fliquidacionA) " + "AND ('N'=:filtroAlias or N.cdalias<>:alias) "
			+ "AND ('N'=:filtroIsin or N.cdisin=:isin) " )
	List< Netting > findByConsultaNotAlias( @Param( "nutituloNeteado" ) BigDecimal titulosneteados,
			@Param( "filtroTituloNeteado" ) String filtroTituloNeteado, @Param( "fcontratacionDe" ) Date fcontratacionDe,
			@Param( "filtrofcontratacionDe" ) String filtrofcontratacionDe, @Param( "fcontratacionA" ) Date fcontratacionA,
			@Param( "filtrofcontratacionA" ) String filtrofcontratacionA, @Param( "fliquidacionDe" ) Date fliquidacionDe,
			@Param( "filtrofliquidacionDe" ) String filtrofliquidacionDe, @Param( "fliquidacionA" ) Date fliquidacionA,
			@Param( "filtrofliquidacionA" ) String filtrofliquidacionA, @Param( "alias" ) String alias,
			@Param( "filtroAlias" ) String filtroAlias, @Param( "isin" ) String isin, @Param( "filtroIsin" ) String filtroIsin );	

	@Query( "SELECT N FROM Netting N "
            + "WHERE EXISTS (SELECT A FROM AlcOrdenes A, Tmct0alc alc WHERE A.netting = N "
            + "AND A.nbooking = alc.id.nbooking "
            + "AND A.nuorden = alc.id.nuorden "
            + "AND A.nucnfliq = alc.id.nucnfliq "
            + "AND A.nucnfclt = alc.id.nucnfclt "
            + "AND alc.estadoentrec is not null) "
			+ "AND ('N'=:filtroTituloNeteado or N.nutituloNeteado=:nutituloNeteado)  "
			+ "AND ('N'=:filtrofcontratacionDe or fechaContratacion >= :fcontratacionDe) "
			+ "AND ('N'=:filtrofcontratacionA or fechaContratacion <= :fcontratacionA)  "
			+ "AND ('N'=:filtrofliquidacionDe or fechaOperacion >= :fliquidacionDe) "
			+ "AND ('N'=:filtrofliquidacionA or fechaOperacion <= :fliquidacionA) " + "AND ('N'=:filtroAlias or N.cdalias=:alias) "
			+ "AND ('N'=:filtroIsin or N.cdisin<>:isin) " )
	List< Netting > findByConsultaNotIsin( @Param( "nutituloNeteado" ) BigDecimal titulosneteados,
			@Param( "filtroTituloNeteado" ) String filtroTituloNeteado, @Param( "fcontratacionDe" ) Date fcontratacionDe,
			@Param( "filtrofcontratacionDe" ) String filtrofcontratacionDe, @Param( "fcontratacionA" ) Date fcontratacionA,
			@Param( "filtrofcontratacionA" ) String filtrofcontratacionA, @Param( "fliquidacionDe" ) Date fliquidacionDe,
			@Param( "filtrofliquidacionDe" ) String filtrofliquidacionDe, @Param( "fliquidacionA" ) Date fliquidacionA,
			@Param( "filtrofliquidacionA" ) String filtrofliquidacionA, @Param( "alias" ) String alias,
			@Param( "filtroAlias" ) String filtroAlias, @Param( "isin" ) String isin, @Param( "filtroIsin" ) String filtroIsin );	
}
