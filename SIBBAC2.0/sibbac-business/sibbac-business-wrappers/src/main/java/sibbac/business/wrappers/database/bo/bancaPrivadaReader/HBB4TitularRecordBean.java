package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;

public class HBB4TitularRecordBean extends HBB4TipoRegistroRecordBean {

  /**
	 * 
	 */
  private static final long serialVersionUID = 3223274589183334345L;

  private BigDecimal numeroTitular;
  private String nombre;
  private String apellido1;
  private String apellido2;
  private String documentoCliente;
  private Character titularidad;
  private String codIfDepositante;
  private String referenciaCentral;
  private String numeroOperacion;
  private Character tipoPersona;
  private Character tipoDocumento;
  private BigDecimal particip;
  private Character IndNac;
  private String nacionalidad;

  public BigDecimal getNumeroTitular() {
    return numeroTitular;
  }

  public String getNombre() {
    return nombre;
  }

  public String getApellido1() {
    return apellido1;
  }

  public String getApellido2() {
    return apellido2;
  }

  public String getDocumentoCliente() {
    return documentoCliente;
  }

  public Character getTitularidad() {
    return titularidad;
  }

  public String getCodIfDepositante() {
    return codIfDepositante;
  }

  public String getReferenciaCentral() {
    return referenciaCentral;
  }

  public String getNumeroOperacion() {
    return numeroOperacion;
  }

  public Character getTipoPersona() {
    return tipoPersona;
  }

  public Character getTipoDocumento() {
    return tipoDocumento;
  }

  public BigDecimal getParticip() {
    return particip;
  }

  public Character getIndNac() {
    return IndNac;
  }

  public void setNumeroTitular(BigDecimal numeroTitular) {
    this.numeroTitular = numeroTitular;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public void setApellido1(String apellido1) {
    this.apellido1 = apellido1;
  }

  public void setApellido2(String apellido2) {
    this.apellido2 = apellido2;
  }

  public void setDocumentoCliente(String documentoCliente) {
    this.documentoCliente = documentoCliente;
  }

  public void setTitularidad(Character titularidad) {
    this.titularidad = titularidad;
  }

  public void setCodIfDepositante(String codIfDepositante) {
    this.codIfDepositante = codIfDepositante;
  }

  public void setReferenciaCentral(String referenciaCentral) {
    this.referenciaCentral = referenciaCentral;
  }

  public void setNumeroOperacion(String numeroOperacion) {
    this.numeroOperacion = numeroOperacion;
  }

  public void setTipoPersona(Character tipoPersona) {
    this.tipoPersona = tipoPersona;
  }

  public void setTipoDocumento(Character tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  public void setIndNac(Character indNac) {
    IndNac = indNac;
  }

  public String getNacionalidad() {
    return nacionalidad;
  }

  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

}
