package sibbac.business.wrappers.database.dao;

import java.util.List;
import java.math.BigInteger;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cli;

@Repository
public interface Tmct0cliDao extends JpaRepository< Tmct0cli, Long > {

	@Query( "SELECT cli FROM Tmct0cli cli WHERE cdbrocli =:cdbrocli and fhfinal = :fhfinal" )
	public Tmct0cli findByCdbrocli( @Param( "cdbrocli" ) String cdbrocli, @Param( "fhfinal" ) int fhfinal );
	
	@Query( "SELECT cli FROM Tmct0cli cli WHERE cdbrocli =:cdbrocli and fhfinal >= :fecha and fhinicio <= :fecha " )
  Tmct0cli findByCdbrocliAndFecha( @Param( "cdbrocli" ) String cdbrocli, @Param( "fecha" ) int fhfinal );

	@Query("SELECT cl FROM Tmct0cli cl order by cl.cdbrocli asc")
	public List<Tmct0cli> findAllOrderByClient();
	
	@Query("SELECT cl FROM Tmct0cli cl where cl.cdbrocli LIKE %:descripCliente% order by cl.cdbrocli asc")
	public List<Tmct0cli> findClientsOrderByClient(@Param( "descripCliente" ) String descripCliente);

	@Query( "SELECT cli FROM Tmct0cli cli WHERE cif = :cif and id <> :id and cdestado<> 'B'" )
	public List<Tmct0cli> findByCifAndNotId(@Param( "cif" ) String cif, @Param( "id" )Long id);
	
	@Query( "SELECT cli FROM Tmct0cli cli WHERE cif = :cif and cdestado<> 'B'" )
	public List<Tmct0cli> findByCif(@Param( "cif" ) String cif);

	@Query( "SELECT cli FROM Tmct0cli cli WHERE cdbrocli = :cdbrocli and id <> :id and cdestado<> 'B'" )
	public List<Tmct0cli> findByCdbrocliAndNotId(@Param( "cdbrocli" ) String cdbrocli, @Param( "id" )Long id);
	
  @Query( "SELECT cli FROM Tmct0cli cli WHERE cdbrocli = :cdbrocli and cdestado<> 'B'" )
  public List<Tmct0cli> findByCdbrocli(@Param( "cdbrocli" ) String cdbrocli);
  
  @Query( "SELECT count(cli) FROM Tmct0cli cli WHERE cdbdp = :cdbdp" )
  public Integer findByCdbdp(@Param( "cdbdp" ) String cdbdp);
	
	/**
	 * Busca los ids de cliente para los cuales la fecha de fin de test de conveniencia no esté actualizada
	 * @param fecha de la última modificación
	 * @param cdbdp identificación de Partenon
	 * @param tppers tipo de persona (física o jurídica)
	 * @return Una lista de ids
	 */
	@Query(value = "SELECT C.IDCLIENT FROM TMCT0CLI C LEFT JOIN TMCT0CLI_MIFID M "
	    + "ON C.IDCLIENT = M.IDCLIENT AND (M.FHCONV IS NULL OR M.FHCONV < :fecha)  "
	    + "WHERE C.CDBDP = :cdbdp AND C.TPFISJUR = :tppers", nativeQuery = true)
	public List<BigInteger> findCliActualizaFechaConveniencia(@Param("fecha") Date fecha, @Param("cdbdp") String cdbdp, 
	    @Param("tppers") String tppers);
	
  /**
   * Busca los ids de cliente para los cuales la fecha de fin de test de idoneidad no esté actualizada
   * @param fecha de la última modificación
   * @param cdbdp identificación de Partenon
   * @param tppers tipo de persona (física o jurídica)
   * @return Una lista de ids
   */
  @Query(value = "SELECT C.IDCLIENT FROM TMCT0CLI C LEFT JOIN TMCT0CLI_MIFID M "
      + "ON C.IDCLIENT = M.IDCLIENT AND (M.FHIDON IS NULL OR M.FHIDON < :fecha) "
      + "WHERE C.CDBDP = :cdbdp AND C.TPFISJUR = :tppers", nativeQuery = true)
  public List<BigInteger> findCliActualizaFechaIdoneidad(@Param("fecha") Date fecha, @Param("cdbdp") String cdbdp, 
      @Param("tppers") String tppers); 
  
}
