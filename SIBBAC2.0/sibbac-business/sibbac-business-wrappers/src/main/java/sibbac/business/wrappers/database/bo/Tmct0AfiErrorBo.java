package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.common.fileReader.ValidationHelper;
import sibbac.business.wrappers.database.dao.Tmct0AfiErrorDao;
import sibbac.business.wrappers.database.dao.Tmct0AfiErrorDaoImpl;
import sibbac.business.wrappers.database.dao.Tmct0aloDao;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.data.Tmct0AfiErrorData;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.model.CodigoError;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0AfiError;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0AfiErrorBo extends AbstractBo<Tmct0AfiError, Long, Tmct0AfiErrorDao> {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0AfiErrorBo.class);

  @Autowired
  private CodigoErrorBo codBo;

  @Autowired
  private Tmct0AfiErrorDaoImpl daoImpl;

  @Autowired
  private Tmct0aloDao tmct0aloDao;

  /**
   * Busca los ID de afiError que devuelva una consulta según los parametros que
   * recibe. Se trae las entidades que corresponden a esos ids y las devuelve en
   * una lista.
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param mercadoNacInt
   * @param sentido
   * @param nudnicif
   * @param codError
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param nbvalors
   * @param tpnactit
   * @param isin
   * @param alias
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param ciudad
   * @param tptiprep
   * @param tpsocied
   * @param particip
   * @param ccv
   * @param nureford
   * @param nacionalidad
   * @return
   */
  public List<Tmct0AfiError> findForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, String nudnicif, String codError, String nbclient, String nbclient1, String nbclient2,
      String nbvalors, Character tpnactit, String isin, String alias, Character tipodocumento, String paisres,
      String tpdomici, String nbdomici, String nudomici, String provincia, String codpostal, String ciudad,
      Character tptiprep, Character tpsocied, BigDecimal particip, String ccv, String nureford, String nacionalidad) {
    List<BigInteger> idList = (List<BigInteger>) daoImpl.findAllIds(fechaDesde, fechaHasta, mercadoNacInt, sentido,
        nudnicif, codError, nbclient, nbclient1, nbclient2, nbvalors, tpnactit, isin, alias, tipodocumento, paisres,
        tpdomici, nbdomici, nudomici, provincia, codpostal, ciudad, tptiprep, tpsocied, particip, ccv, nureford,
        nacionalidad);
    List<Long> idListLong = new ArrayList<Long>();
    for (BigInteger id : idList) {
      idListLong.add(id.longValue());
    }
    return dao.findByIdIn(idListLong);
  }

  public List<Tmct0AfiError> findAllWithNoErrors() {
    return daoImpl.findAllWithNoErrors();
  }

  public List<Tmct0AfiError> findAllWithNurefordAndNoErrors() {
    return daoImpl.findAllWithNurefordAndNoErrors();
  }

  public List<Tmct0AfiError> findAllWithAlcAndNoErrors() {
    return daoImpl.findAllWithAlcAndNoErrors();
  }

  public List<Tmct0AfiError> findAllWithAloAndNoErrors() {
    return daoImpl.findAllWithAloAndNoErrors();
  }

  public List<Tmct0AfiError> findPartnersWithErrorsByNureford(String nureford) {
    return daoImpl.findPartnersWithErrorsByNureford(nureford);
  }

  public List<Tmct0AfiError> findPartnersWithErrorsByAlcId(Tmct0alcId alcId) {
    return daoImpl.findPartnersWithErrorsByAlcId(alcId);
  }

  public List<Tmct0AfiError> findPartnersWithErrorsByAloId(Tmct0aloId aloId) {
    return daoImpl.findPartnersWithErrorsByAloId(aloId);
  }

  public List<Tmct0AfiError> findGroupByAlcId(Tmct0alcId alcId) {
    return daoImpl.findGroupByAlcId(alcId);
  }

  public List<Tmct0AfiError> findGroupByAloId(Tmct0aloId aloId) {
    return daoImpl.findGroupByAloId(aloId);
  }

  // public List<Tmct0AfiError> findPartnersByNureford(String nureford, Long
  // afiErrorId) {
  // return daoImpl.findPartnersByNureford(nureford);
  // }
  //
  public List<Tmct0AfiError> findPartnersByAlcId(Tmct0alcId alcId, Long afiErrorId) {
    return daoImpl.findPartnersByAlcId(alcId, afiErrorId);
  }

  public List<Tmct0AfiError> findPartnersByAloId(Tmct0aloId aloId, Long afiErrorId) {
    return daoImpl.findPartnersByAloId(aloId, afiErrorId);
  }

  public List<TitularesDTO> findDTOForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, String nudnicif, String codError, String nbclient, String nbclient1, String nbclient2,
      String nbvalors, Character tpnactit, String isin, String alias, Character tipodocumento, String paisres,
      String tpdomici, String nbdomici, String nudomici, String provincia, String codpostal, String nureford,
      Short hasErrors) {
    return daoImpl.findAllDTO(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, codError, nbclient, nbclient1,
        nbclient2, nbvalors, tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia,
        codpostal, nureford, hasErrors);
  }

  public List<TitularesDTO> findDTOForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, List<String> nudnicif, String codError, List<String> nbclient, List<String> nbclient1,
      List<String> nbclient2, Character tpnactit, List<String> isin, List<String> alias, Character tipodocumento,
      String paisres, String tpdomici, String nbdomici, String nudomici, String provincia, String codpostal,
      String nureford, String nbooking, Integer firstRow, Integer lastRow, Short hasErrors) {
    return daoImpl.findAllDTO(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, codError, nbclient, nbclient1,
        nbclient2, tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia, codpostal,
        nureford, nbooking, firstRow, lastRow, hasErrors);
  } // findDTOForService

  // Obtiene la lista de errores de un titular
  public List<Map<String, String>> getListErroresTitular(Long afiErrorId) {
    return daoImpl.getListErroresTitular(afiErrorId);
  }

  public List<TitularesDTO> findDTOForServiceWithoutAfi(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, List<String> nudnicif, String codError, List<String> nbclient, List<String> nbclient1,
      List<String> nbclient2, Character tpnactit, List<String> isin, List<String> alias, Character tipodocumento,
      String paisres, String tpdomici, String nbdomici, String nudomici, String provincia, String codpostal,
      String nureford, String nbooking, Integer firstRow, Integer lastRow, Short hasErrors) {
    return daoImpl.findAllDTOWithoutAfi(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, codError, nbclient,
        nbclient1, nbclient2, tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia,
        codpostal, nureford, nbooking, firstRow, lastRow, hasErrors);
  } // findDTOForServiceWithoutAfi

  @Transactional
  public void findAndSetErrors(Tmct0AfiError afiError, List<String> errCodeList,
      Map<String, CodigoErrorData> mapaErrores) {

    Set<CodigoError> errorSet = new HashSet<CodigoError>();
    CodigoError error;
    for (String errCode : errCodeList) {
      if (mapaErrores != null) {
        error = codBo.findById(mapaErrores.get(errCode).getId());

      }
      else {
        error = codBo.findByCodigo(errCode);
      }
      if (error != null) {
        errorSet.add(error);
      }
    }
    afiError.setErrors(errorSet);

  }

  @Transactional
  public void findAndSetErrorsData(Tmct0AfiErrorData afiError, List<String> errCodeList,
      Map<String, CodigoErrorData> mapaErrores) {
    Set<CodigoErrorData> errorSet = new HashSet<CodigoErrorData>();

    for (String errCode : errCodeList) {
      CodigoErrorData error = mapaErrores.get(errCode);
      // error.addTmct0AfiErrorData( afiError );
      errorSet.add(error);
    }
    afiError.setErrors(errorSet);

  }

  public Tmct0AfiError findByIdmensaje(String idmensaje) {
    return this.dao.findByIdmensaje(idmensaje);
  }

  /**
   * Con este metodo se comprueba si el afi que vamos a insertar esta en
   * afiError y se borra en ese caso
   * 
   * @param idMensaje
   */
  @Transactional
  public void deleteAfiErrorIfExists(String idMensaje) {
    Tmct0AfiError afiErrorToDelete = this.findByIdmensaje(idMensaje);
    if (afiErrorToDelete != null) {
      this.delete(afiErrorToDelete);
    }
  }

  @Transactional
  public Integer deleteByNureford(String nureford) {
    return this.dao.deleteByNureford(nureford);
  }

  /**
   * Este metodo recorre los beans para ver si hay algun error, si lo hay se
   * recorre el resto para aniadir el error en el otro titular
   * @param beans
   * @param errorsByBean
   * @param mapaErrores
   */
  public void checkErrorOtroTitular(List<PartenonRecordBean> beans, Map<PartenonRecordBean, List<String>> errorsByBean,
      Map<String, CodigoErrorData> mapaErrores) {
    boolean hasError = false;
    for (PartenonRecordBean partenonRecordBean : beans) {
      if (CollectionUtils.isNotEmpty(errorsByBean.get(partenonRecordBean))) {
        hasError = true;
      }
    }
    if (hasError) {
      for (PartenonRecordBean partenonRecordBean : beans) {
        if (CollectionUtils.isEmpty(errorsByBean.get(partenonRecordBean))) {
          errorsByBean.get(partenonRecordBean).add(ValidationHelper.CodError.ERROROTROTIT.text);
        }
      }
    }
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void saveAfiErrorExtranjero(PartenonRecordBean bean, Map<PartenonRecordBean, List<String>> errorsByBean,
      List<ControlTitularesDTO> alos, Map<String, CodigoErrorData> mapaErrores) {
    this.saveAfiErrorExtranjero(bean, errorsByBean, alos, mapaErrores, true);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void saveAfiErrorExtranjero(PartenonRecordBean bean, Map<PartenonRecordBean, List<String>> errorsByBean,
      List<ControlTitularesDTO> alos, Map<String, CodigoErrorData> mapaErrores, boolean insertErrorOtroTitular) {
    // Si por culpa del error no hay alos lo guardamos solo una vez
    if (alos.size() == 0) {
      Tmct0AfiError afiError = bean.toTmct0afierror();
      this.findAndSetErrors(afiError, errorsByBean.get(bean), mapaErrores);
      if (insertErrorOtroTitular) {
        if (afiError.getErrors().size() == 0) {
          // Si esta aqui y no tiene errores es porque uno de su grupo
          // tiene errores
          afiError.addError(codBo.findByCodigo(ValidationHelper.CodError.ERROROTROTIT.text));
        }
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiError(afiError);
      }
    }

    Integer afisSaved = 0;
    for (ControlTitularesDTO alo : alos) {
      Tmct0AfiError afiError = bean.toTmct0afierror();
      this.findAndSetErrors(afiError, errorsByBean.get(bean), mapaErrores);
      if (insertErrorOtroTitular) {
        if (afiError.getErrors().size() == 0) {
          // Si esta aqui y no tiene errores es porque uno de su grupo
          // tiene errores
          afiError.addError(codBo.findByCodigo(ValidationHelper.CodError.ERROROTROTIT.text));
        }
      }
      afiError.setNucnfliq((short) 0);
      afiError.setTmct0alo(new Tmct0alo());
      afiError.getTmct0alo().setId(alo.getAlloId());
      if (afisSaved > 0) {
        afiError.setIdmensaje(afiError.getIdmensaje().concat(afisSaved.toString()));
      }
      if (afiError.getIdmensaje().length() > 30) {
        afiError.setIdmensaje(afiError.getIdmensaje().substring(afiError.getIdmensaje().length() - 30,
            afiError.getIdmensaje().length() - 1));
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiError(afiError);
      }
      afisSaved++;
    }
  }

  @Transactional
  public void saveAfiErrorExtranjeroOptimizado(PartenonRecordBean bean,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<ControlTitularesDTO> alos,
      Map<String, CodigoErrorData> mapaErrores, boolean permitirSinError) throws Exception {

    // Si por culpa del error no hay alos lo guardamos solo una vez
    if (alos.size() == 0) {
      Tmct0AfiError afiErrorData = bean.toTmct0afierror();
      this.findAndSetErrors(afiErrorData, errorsByBean.get(bean), mapaErrores);

      if (afiErrorData.getErrors().size() == 0 && !permitirSinError) {
        // Si esta aqui y no tiene errores es porque uno de su grupo
        // tiene errores
        afiErrorData.addError(codBo.findByCodigo(ValidationHelper.CodError.ERROROTROTIT.text));
      }
      if (afiErrorData.lengthsAreValid()) {
        this.saveOrUpdateAfiError(afiErrorData);
      }
    }

    Integer afisSaved = 0;
    for (ControlTitularesDTO alo : alos) {
      Tmct0AfiError afiError = bean.toTmct0afierror();
      this.findAndSetErrors(afiError, errorsByBean.get(bean), mapaErrores);
      if (afiError.getErrors().size() == 0 && !permitirSinError) {
        // Si esta aqui y no tiene errores es porque uno de su grupo
        // tiene errores
        afiError.addError(codBo.findByCodigo(ValidationHelper.CodError.ERROROTROTIT.text));
      }
      afiError.setNucnfliq((short) 0);
      afiError.setTmct0alo(new Tmct0alo());
      afiError.getTmct0alo().setId(alo.getAlloId());
      if (afisSaved > 0) {
        afiError.setIdmensaje(afiError.getIdmensaje().concat(afisSaved.toString()));
      }
      if (afiError.getIdmensaje().length() > 30) {
        afiError.setIdmensaje(afiError.getIdmensaje().substring(afiError.getIdmensaje().length() - 30,
            afiError.getIdmensaje().length() - 1));
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiError(afiError);
      }
      afisSaved++;
    }
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void saveAfiError(PartenonRecordBean bean, Map<PartenonRecordBean, List<String>> errorsByBean,
      List<ControlTitularesDTO> alcs, Map<String, CodigoErrorData> mapaErrores) {
    this.saveAfiError(bean, errorsByBean, alcs, mapaErrores, true);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void saveAfiError(PartenonRecordBean bean, Map<PartenonRecordBean, List<String>> errorsByBean,
      List<ControlTitularesDTO> alcs, Map<String, CodigoErrorData> mapaErrores, boolean insertErrorOtroTitular) {
    // Si por culpa del error no hay alos lo guardamos solo una vez

    if (alcs.size() == 0) {
      Tmct0AfiError afiError = bean.toTmct0afierror();

      this.findAndSetErrors(afiError, errorsByBean.get(bean), mapaErrores);
      if (insertErrorOtroTitular) {
        if (afiError.getErrors().size() == 0) {
          // Si esta aqui y no tiene errores es porque uno de su grupo
          // tiene errores
          afiError.addError(codBo.findByCodigo(ValidationHelper.CodError.ERROROTROTIT.text));
        }
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiError(afiError);
      }
    }

    Integer afisSaved = 0;
    for (ControlTitularesDTO alc : alcs) {
      Tmct0AfiError afiError = bean.toTmct0afierror();

      this.findAndSetErrors(afiError, errorsByBean.get(bean), mapaErrores);
      if (insertErrorOtroTitular) {
        if (afiError.getErrors().size() == 0) {
          // Si esta aqui y no tiene errores es porque uno de su grupo
          // tiene errores
          afiError.addError(codBo.findByCodigo(ValidationHelper.CodError.ERROROTROTIT.text));
        }
      }
      afiError.setNucnfliq(alc.getNucnfliq());
      afiError.setTmct0alo(new Tmct0alo());
      afiError.getTmct0alo().setId(alc.getAlloId());
      if (afisSaved > 0) {
        afiError.setIdmensaje(afiError.getIdmensaje().concat(afisSaved.toString()));
      }
      if (afiError.getIdmensaje().length() > 30) {
        afiError.setIdmensaje(afiError.getIdmensaje().substring(afiError.getIdmensaje().length() - 30,
            afiError.getIdmensaje().length() - 1));
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiError(afiError);
      }
      afisSaved++;
    }
  }

  @Transactional
  public void saveAfiErrorOptimizado(PartenonRecordBean bean, Map<PartenonRecordBean, List<String>> errorsByBean,
      List<ControlTitularesDTO> alcs, Map<String, CodigoErrorData> mapaErrores, boolean permitirSinError)
      throws SIBBACBusinessException {
    // Si por culpa del error no hay alos lo guardamos solo una vez
    if (alcs.size() == 0) {
      Tmct0AfiErrorData afiError = bean.toTmct0afierrorData();
      this.findAndSetErrorsData(afiError, errorsByBean.get(bean), mapaErrores);

      if (afiError.getErrors().size() == 0 && !permitirSinError) {
        // Si esta aqui y no tiene errores es porque uno de su grupo
        // tiene errores
        afiError.addError(codBo.findByCodigoData(ValidationHelper.CodError.ERROROTROTIT.text));
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiErrorData(afiError);
      }
    }

    Integer afisSaved = 0;
    for (ControlTitularesDTO alc : alcs) {
      Tmct0AfiErrorData afiError = bean.toTmct0afierrorData();
      this.findAndSetErrorsData(afiError, errorsByBean.get(bean), mapaErrores);
      if (afiError.getErrors().size() == 0 && !permitirSinError) {
        // Si esta aqui y no tiene errores es porque uno de su grupo
        // tiene errores
        afiError.addError(codBo.findByCodigoData(ValidationHelper.CodError.ERROROTROTIT.text));
      }
      afiError.setNucnfliq(alc.getNucnfliq());
      try {
        afiError.setTmc0AloData(Tmct0AloData.entityToData(tmct0aloDao.findOne(alc.getAlloId())));
      }
      catch (RuntimeException e) {
        throw new SIBBACBusinessException("No se ha podido cargar Tmct0AloData.id " + alc.getAlloId(), e);
      }
      if (afisSaved > 0) {
        afiError.setIdmensaje(afiError.getIdmensaje().concat(afisSaved.toString()));
      }
      if (afiError.getIdmensaje().length() > 30) {
        afiError.setIdmensaje(afiError.getIdmensaje().substring(afiError.getIdmensaje().length() - 30,
            afiError.getIdmensaje().length() - 1));
      }
      if (afiError.lengthsAreValid()) {
        this.saveOrUpdateAfiErrorData(afiError);
      }
      afisSaved++;
    }
  }

  public void saveOrUpdateAfiError(Tmct0AfiError afiError) {
    Tmct0AfiError afiErrorToSave = createCopyAfiError(afiError);

    Tmct0AfiError afiErrorToUpdate = this.findByIdmensaje(afiErrorToSave.getIdmensaje());
    if (afiErrorToUpdate != null) {
      this.updateAfiError(afiErrorToSave, afiErrorToUpdate);
      if (afiErrorToUpdate.lengthsAreValid()) {
        afiErrorToUpdate.setAuditDate(new Date());
        this.save(afiErrorToUpdate);
        // this.codBo.save(errores);
      }
    }
    else {
      if (afiErrorToSave.lengthsAreValid()) {
        LOG.trace("[saveOrUpdateAfiError] se va a crear el objeto Tmct0AfiError: " + afiErrorToSave.toString()
            + " en la base de datos....");
        try {
          if (afiErrorToSave.getTmct0alo() != null && afiErrorToSave.getTmct0alo().getId() != null) {
            // FIXME QUITAR LA LECTURA, HASTA AHORA ME HA FALLADO PONIENDO SOLO
            // EL ID
            Tmct0alo alo = tmct0aloDao.findOne(afiErrorToSave.getTmct0alo().getId());
            afiErrorToSave.setTmct0alo(alo);
          }
          afiErrorToSave.setAuditDate(new Date());
          afiErrorToSave = this.save(afiErrorToSave);
          LOG.trace("[saveOrUpdateAfiError] se ha creado el objeto Tmct0AfiError con id: " + afiErrorToSave.getId()
              + " en la base de datos....");
        }
        catch (PersistenceException ex) {
          LOG.error(ex.getMessage() + " cause: " + ex.getCause(), ex);
        }
      }
    }
  }

  public void saveOrUpdateAfiErrorData(Tmct0AfiErrorData afiError) {
    Tmct0AfiError afiErrorToSave = createCopyAfiError(afiError);
    // reattachCodErrorEntities(afiError, afiErrorToSave);
    Tmct0AfiError afiErrorToUpdate = this.findByIdmensaje(afiErrorToSave.getIdmensaje());
    if (afiErrorToUpdate != null) {
      this.updateAfiError(afiErrorToSave, afiErrorToUpdate);
      if (afiErrorToUpdate.lengthsAreValid()) {
        afiErrorToUpdate.setAuditDate(new Date());

        this.save(afiErrorToUpdate);
        // this.codBo.save(afiErrorToUpdate.getErrors());
      }
    }
    else {
      if (afiErrorToSave.lengthsAreValid()) {
        LOG.trace("[saveOrUpdateAfiError] se va a crear el objeto Tmct0AfiError: " + afiErrorToSave.toString()
            + " en la base de datos....");
        try {
          if (afiErrorToSave.getTmct0alo() != null && afiErrorToSave.getTmct0alo().getId() != null) {
            afiErrorToSave.setTmct0alo(tmct0aloDao.findOne(afiErrorToSave.getTmct0alo().getId()));
          }
          else {
            afiErrorToSave.setTmct0alo(null);
          }
          afiErrorToSave.setAuditDate(new Date());

          Collection<CodigoError> errores = afiErrorToSave.getErrors();

          if (CollectionUtils.isNotEmpty(errores)) {
            Set<CodigoError> erroresToSet = new HashSet<CodigoError>();
            CodigoError err;
            for (CodigoError codigoError : errores) {
              err = this.codBo.findByCodigo(codigoError.getCodigo());
              if (err != null) {
                erroresToSet.add(err);
              }
            }// fin for

            afiErrorToSave.setErrors(erroresToSet);
          }
          afiErrorToSave = this.save(afiErrorToSave);
          LOG.trace("[saveOrUpdateAfiError] se ha creado el objeto Tmct0AfiError con id: " + afiErrorToSave.getId()
              + " en la base de datos....");
        }
        catch (PersistenceException ex) {
          LOG.error(ex.getMessage() + " cause: " + ex.getCause(), ex);
        }
      }
    }
  }

  private void updateAfiError(Tmct0AfiError newData, Tmct0AfiError afiErrorToUpdate) {
    afiErrorToUpdate.setTmct0alo(newData.getTmct0alo());
    afiErrorToUpdate.setNureford(newData.getNureford());
    afiErrorToUpdate.setNucnfliq(newData.getNucnfliq());
    afiErrorToUpdate.setTpdomici(newData.getTpdomici());
    afiErrorToUpdate.setNbdomici(newData.getNbdomici());
    afiErrorToUpdate.setNudomici(newData.getNudomici());
    afiErrorToUpdate.setNbciudad(newData.getNbciudad());
    afiErrorToUpdate.setNbprovin(newData.getNbprovin());
    afiErrorToUpdate.setCdpostal(newData.getCdpostal());
    afiErrorToUpdate.setCddepais(newData.getCddepais());
    afiErrorToUpdate.setNudnicif(newData.getNudnicif());
    afiErrorToUpdate.setNbclient(newData.getNbclient());
    afiErrorToUpdate.setNbclient1(newData.getNbclient1());
    afiErrorToUpdate.setNbclient2(newData.getNbclient2());
    afiErrorToUpdate.setTptiprep(newData.getTptiprep());
    afiErrorToUpdate.setCdnactit(newData.getCdnactit());
    afiErrorToUpdate.setTpidenti(newData.getTpidenti());
    afiErrorToUpdate.setTpsocied(newData.getTpsocied());
    afiErrorToUpdate.setTpnactit(newData.getTpnactit());
    afiErrorToUpdate.setRftitaud(newData.getRftitaud());
    afiErrorToUpdate.setCdreferenciatitular(newData.getCdreferenciatitular());
    afiErrorToUpdate.setParticip(newData.getParticip());
    afiErrorToUpdate.setCcv(newData.getCcv());
    afiErrorToUpdate.setIdmensaje(newData.getIdmensaje());
    afiErrorToUpdate.setErrors(newData.getErrors());
  }

  public static Tmct0AfiError createCopyAfiError(Tmct0AfiError afiToCopy) {
    Tmct0AfiError afiCopied = new Tmct0AfiError(afiToCopy.getTmct0alo(), afiToCopy.getNureford(),
        afiToCopy.getNucnfliq(), afiToCopy.getTpdomici(), afiToCopy.getNbdomici(), afiToCopy.getNudomici(),
        afiToCopy.getNbciudad(), afiToCopy.getNbprovin(), afiToCopy.getCdpostal(), afiToCopy.getCddepais(),
        afiToCopy.getNudnicif(), afiToCopy.getNbclient(), afiToCopy.getNbclient1(), afiToCopy.getNbclient2(),
        afiToCopy.getTptiprep(), afiToCopy.getCdnactit(), afiToCopy.getTpidenti(), afiToCopy.getTpsocied(),
        afiToCopy.getTpnactit(), afiToCopy.getRftitaud(), afiToCopy.getCdreferenciatitular(), afiToCopy.getParticip(),
        afiToCopy.getCcv(), afiToCopy.getIdmensaje(), afiToCopy.getErrors());
    return afiCopied;
  }

  public static Tmct0AfiError createCopyAfiError(Tmct0AfiErrorData afiToCopy) {
    Tmct0AfiError result = Tmct0AfiErrorData.dataToEntity(afiToCopy);

    return result;
  }

  @Transactional
  public void guardarBloqueExtranjeroConErrores(List<ControlTitularesDTO> alos,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores) {
    this.guardarBloqueExtranjeroConErrores(alos, errorsByBean, partenonBeanList, mapaErrores, true);
  }

  @Transactional
  public void guardarBloqueExtranjeroConErrores(List<ControlTitularesDTO> alos,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores, boolean insertErrorOtroTitular) {

    for (PartenonRecordBean bean : partenonBeanList) {
      try {
        LOG.trace("[guardarBloqueExtranjeroConErrores] Tiene errores, se guardara en AFI_ERROR "
            + bean.getIdMensajeConcatenado());
        this.saveAfiErrorExtranjero(bean, errorsByBean, alos, mapaErrores);
      }
      catch (RuntimeException e) {
        LOG.error("[guardarBloqueExtranjeroConErrores] Exception saving bean into TMCT0AFIERROR with NUMORDEN: {}",
            bean.getNumOrden());
        throw e;
      }
    }
  }

  public void guardarBloqueExtranjeroConErroresOptimizado(List<ControlTitularesDTO> alos,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores) {
    guardarBloqueExtranjeroConErroresOptimizado(alos, errorsByBean, partenonBeanList, mapaErrores, false);
  }

  @Transactional
  public void guardarBloqueExtranjeroConErroresOptimizado(List<ControlTitularesDTO> alos,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores, boolean permitirSinError) {
    for (PartenonRecordBean bean : partenonBeanList) {

      try {
        LOG.trace("[Tmct0AfiErrorBo :: guardarBloqueExtranjeroConErroresOptimizado] Tiene errores, se guardara en AFI_ERROR "
            + bean.getIdMensajeConcatenado());
        this.saveAfiErrorExtranjeroOptimizado(bean, errorsByBean, alos, mapaErrores, permitirSinError);
        // FIXME Hay que conseguir guardar en el log los extranjeros
        // tmct0logBo.hacerLogGuardadoExtranjero( alos,
        // "Holder data received from the client. Status: KO, at least one holder with errors"
        // );
      }
      catch (Exception e) {
        LOG.error("[Tmct0AfiErrorBo :: guardarBloqueExtranjeroConErroresOptimizado] Exception saving bean into TMCT0AFIERROR with NUMORDEN: "
            + bean.getNumOrden() + " ERROR: " + e.getMessage());
      }
    }

  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void guardarBloqueNacionalConErrores(List<ControlTitularesDTO> alcs,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores) {
    this.guardarBloqueNacionalConErrores(alcs, errorsByBean, partenonBeanList, mapaErrores, true);

  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void guardarBloqueNacionalConErrores(List<ControlTitularesDTO> alcs,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores, boolean insertErrorOtroTitular) {
    for (PartenonRecordBean bean : partenonBeanList) {
      try {
        LOG.trace("[guardarBloqueNacionalConErrores] Tiene errores, se guardara en AFI_ERROR {}",
            bean.getIdMensajeConcatenado());
        saveAfiError(bean, errorsByBean, alcs, mapaErrores, insertErrorOtroTitular);
      }
      catch (RuntimeException e) {
        LOG.error(
            "[guardarBloqueNacionalConErrores] Exception saving bean into TMCT0AFIERROR with NUMORDEN: {} ERROR :{}",
            bean.getNumOrden(), e.getMessage());
        throw e;
      }
    }
  }

  public void guardarBloqueNacionalConErroresOptimizado(List<ControlTitularesDTO> alcs,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores) {
    guardarBloqueNacionalConErroresOptimizado(alcs, errorsByBean, partenonBeanList, mapaErrores, false);
  }

  @Transactional
  public void guardarBloqueNacionalConErroresOptimizado(List<ControlTitularesDTO> alcs,
      Map<PartenonRecordBean, List<String>> errorsByBean, List<PartenonRecordBean> partenonBeanList,
      Map<String, CodigoErrorData> mapaErrores, boolean permitirSinError) {
    for (PartenonRecordBean bean : partenonBeanList) {

      try {
        LOG.trace("[Tmct0AfiErrorBo :: guardarBloqueNacionalConErroresOptimizado] Tiene errores, se guardara en AFI_ERROR "
            + bean.getIdMensajeConcatenado());
        // this.saveAfiError(bean, errorsByBean, alcs);
        this.saveAfiErrorOptimizado(bean, errorsByBean, alcs, mapaErrores, permitirSinError);
        /*
         * Figueroa ha dicho que se comente para mejorar el rendimiento hasta
         * que se mejore
         */
        // tmct0logBo.hacerLogGuardadoNacional( alcs,
        // "Holder data received from the client. Status: KO, at least one holder with errors"
        // );
      }
      catch (Exception e) {
        LOG.error("[Tmct0AfiErrorBo :: guardarBloqueNacionalConErroresOptimizado] Exception saving bean into TMCT0AFIERROR with NUMORDEN: "
            + bean.getNumOrden() + " ERROR: " + e.getMessage());
      }
    }
  }

  public int countFindAllDTOPageable(Date fechaDesde, Date fechaHasta, Character mercadoNacInt, Character sentido,
      List<String> nudnicifVector, String codError, List<String> nbclientVector, List<String> nbclient1Vector,
      List<String> nbclient2Vector, Character tpnactit, List<String> isinVector, List<String> aliasVector,
      Character tipodocumento, String paisres, String tpdomici, String nbdomici, String nudomici, String provincia,
      String codpostal, String nureford, String nbooking, Integer firstRow, Integer lastRow, Short hasErrors) {
    return daoImpl.countFindAllDTOPageable(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicifVector, codError,
        nbclientVector, nbclient1Vector, nbclient2Vector, tpnactit, isinVector, aliasVector, tipodocumento, paisres,
        tpdomici, nbdomici, nudomici, provincia, codpostal, nureford, nbooking, firstRow, lastRow, hasErrors);
  }

  public boolean containsAlcs(Tmct0AfiError afiError) {
    return (afiError.getTmct0alo() != null && afiError.getTmct0alo().getTmct0alcs() != null && afiError.getTmct0alo()
        .getTmct0alcs().size() > 0);
  }

  public Integer countAllDTOWithoutAfi(Date fechaDesde, Date fechaHasta, Character mercadoNacInt, Character sentido,
      List<String> nudnicifList, String codError, List<String> nbclientList, List<String> nbclient1List,
      List<String> nbclient2List, Character tpnactit, List<String> isinList, List<String> aliasList,
      Character tipodocumento, String paisres, String tpdomici, String nbdomici, String nudomici, String provincia,
      String codpostal, String nureford, String nbooking, Object object, Object object2, Short hasErrors) {
    return daoImpl.countAllDTOWithoutAfi(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicifList, codError,
        nbclientList, nbclient1List, nbclient2List, tpnactit, isinList, aliasList, tipodocumento, paisres, tpdomici,
        nbdomici, nudomici, provincia, codpostal, nureford, nbooking, null, null, hasErrors);
  }

  public List<Tmct0AfiError> findByNureford(String nureford) {
    return this.dao.findByNureford(nureford);
  }
}
