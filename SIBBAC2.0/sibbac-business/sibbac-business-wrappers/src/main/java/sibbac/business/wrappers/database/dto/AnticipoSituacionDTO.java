/**
 * 
 */
package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import sibbac.business.wrappers.common.StringHelper;


/**
 * DTO que contiene informacion para la creación de cada
 * linea del fichero anticipo_situacion
 * 
 * @author lucio.vilar
 *
 */
public class AnticipoSituacionDTO implements Serializable {

	/**
	 * serial version
	 */
	private static final long serialVersionUID = 6623420665578429731L;

	private String fechaEjec;
	private String miembro;
	private char sentido;
	private String titulos;
	private String isin;
	private BigDecimal precio;
	private String numEjec;
	private String procedencia;
	private String propuesta;
	private String refAdisional;
	private String refAsignacion;
	/**
	 * @return the fechaEjec
	 */
	public String getFechaEjec() {
		return fechaEjec;
	}
	/**
	 * @param fechaEjec the fechaEjec to set
	 */
	public void setFechaEjec(String fechaEjec) {
		this.fechaEjec = fechaEjec != null ? fechaEjec.trim() : "";
	}
	/**
	 * @return the miembro
	 */
	public String getMiembro() {
		return miembro;
	}
	/**
	 * @param miembro the miembro to set
	 */
	public void setMiembro(String miembro) {
		this.miembro = miembro != null ? miembro.trim() : "";
	}
	/**
	 * @return the sentido
	 */
	public char getSentido() {
		return sentido;
	}
	/**
	 * @param sentido the sentido to set
	 */
	public void setSentido(char sentido) {
		this.sentido = sentido;
	}
	/**
	 * @return the titulos
	 */
	public String getTitulos() {
		return titulos;
	}
	/**
	 * @param titulos the titulos to set
	 */
	public void setTitulos(String titulos) {
		this.titulos = titulos != null ? titulos.trim() : "";
	}
	/**
	 * @return the isin
	 */
	public String getIsin() {
		return isin;
	}
	/**
	 * @param isin the isin to set
	 */
	public void setIsin(String isin) {
		this.isin = isin != null ? isin.trim() : "";
	}
	/**
	 * @return the precio
	 */
	public BigDecimal getPrecio() {
		return precio;
	}
	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(BigDecimal precio) {
	  this.precio = (precio != null) ? precio : BigDecimal.ZERO;
	}
	/**
	 * @return the numEjec
	 */
	public String getNumEjec() {
		return numEjec;
	}
	/**
	 * @param numEjec the numEjec to set
	 */
	public void setNumEjec(String numEjec) {
		this.numEjec = numEjec != null ? numEjec.trim() : "";
	}
	/**
	 * @return the procedencia
	 */
	public String getProcedencia() {
		return procedencia;
	}
	/**
	 * @param procedencia the procedencia to set
	 */
	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia != null ? procedencia.trim() : "";
	}
	/**
	 * @return the propuesta
	 */
	public String getPropuesta() {
		return propuesta;
	}
	/**
	 * @param propuesta the propuesta to set
	 */
	public void setPropuesta(String propuesta) {
		this.propuesta = propuesta != null ? propuesta.trim() : "";
	}
	/**
	 * @return the refAdisional
	 */
	public String getRefAdisional() {
		return refAdisional;
	}
	/**
	 * @param refAdisional the refAdisional to set
	 */
	public void setRefAdisional(String refAdisional) {
		this.refAdisional = refAdisional != null ? refAdisional.trim() : "";
	}
	/**
	 * @return the refAsignacion
	 */
	public String getRefAsignacion() {
		return refAsignacion;
	}
	/**
	 * @param refAsignacion the refAsignacion to set
	 */
	public void setRefAsignacion(String refAsignacion) {
		this.refAsignacion = refAsignacion != null ? refAsignacion.trim() : "" ;
	}
	
	public String getLineaAnticipoSituacion() {
		final StringBuffer linea;
		
		linea = new StringBuffer();
		linea.append(StringHelper.formatearCampo(this.getFechaEjec(), 10, " "));
		linea.append(StringHelper.formatearCampo(this.getMiembro(), 4, " "));
		linea.append(StringHelper.formatearCampo(Character.toString(this.getSentido()), 1, " "));
		linea.append(StringHelper.formatearCampo(this.getTitulos(), 12, "0"));
		linea.append(StringHelper.formatearCampo(this.getIsin(), 12, " "));
		linea.append(StringHelper.formatBigDecimal(this.getPrecio(), 7, 6));
		linea.append(StringHelper.rightPad(StringHelper.formatearCampo(this.getNumEjec(), 9, "0"), 35, " "));
		linea.append(StringHelper.rightPad(this.getProcedencia(), 20, " "));
		linea.append(StringHelper.formatearCampo(this.getPropuesta(), 10, " "));
		linea.append(StringHelper.formatearCampo(this.getRefAdisional(), 20, " "));
		linea.append(StringHelper.rightPad(this.getRefAsignacion(), 18, " "));
		return linea.toString();
	}
	
}
