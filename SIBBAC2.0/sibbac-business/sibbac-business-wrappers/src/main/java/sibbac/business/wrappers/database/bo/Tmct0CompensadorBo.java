package sibbac.business.wrappers.database.bo;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.wrappers.database.dao.Tmct0CompensadorDao;
import sibbac.business.wrappers.database.data.CompensadorData;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0CompensadorBo extends AbstractBo<Tmct0Compensador, Long, Tmct0CompensadorDao> {

  public List<Tmct0Compensador> findAll() {
    return this.dao.findAll(new Sort(Sort.Direction.ASC, "nbDescripcion"));
  }

  public Tmct0Compensador findByTmct0cuentasdecompensacion(Tmct0CuentasDeCompensacion cuentacompensacion) {
    return dao.findByTmct0cuentasdecompensacion(cuentacompensacion);
  }

  public List<CompensadorData> findAllData() {
    return dao.findAllData();
  }

  public Tmct0Compensador findByNbDescripcion(String nbDescripcion) {
    return this.dao.findByNbDescripcion(nbDescripcion);
  }

  public Tmct0Compensador findByNbNombre(String nbNombre) {
    return this.dao.findByNbNombre(nbNombre);
  }

  public List<LabelValueObject> labelValueList() {
    List<LabelValueObject> res = dao.labelValueList();
    res.add(0, new LabelValueObject("", ""));
    return res;
  }

  public Character getCorretajePtiChached(Map<String, Character> chachedCorretajePti, String nbNombre)
      throws SIBBACBusinessException {
    nbNombre = StringUtils.trim(nbNombre);

    if (chachedCorretajePti.get(nbNombre) == null) {
      synchronized (chachedCorretajePti) {
        if (chachedCorretajePti.get(nbNombre) == null) {
          Tmct0Compensador ali = findByNbNombre(nbNombre);
          if (ali != null) {
            try {
              chachedCorretajePti.put(nbNombre, ali.getCorretajePti() != null ? ali.getCorretajePti() : ' ');
            }
            catch (Exception e) {
              chachedCorretajePti.put(nbNombre, ' ');
            }
          }
          else {
            chachedCorretajePti.put(nbNombre, ' ');
            throw new SIBBACBusinessException("INCIDENCIA- Compensador no dado de alta en maestro de compensadores: "
                + nbNombre);

          }
        }
      }
    }
    return chachedCorretajePti.get(nbNombre);
  }

  public Tmct0Compensador findByNbnombreCached(Map<String, Tmct0Compensador> cached, String codigo) {
    codigo = StringUtils.trim(codigo);
    Tmct0Compensador compensador = cached.get(codigo);
    if (compensador == null) {
      synchronized (cached) {
        if ((compensador = cached.get(codigo)) == null) {
          compensador = findByNbNombre(codigo);
          cached.put(codigo, compensador);
        }
      }
    }
    return compensador;
  }

}
