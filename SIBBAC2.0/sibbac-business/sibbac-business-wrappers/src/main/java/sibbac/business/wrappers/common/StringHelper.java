package sibbac.business.wrappers.common;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Contiene métodos de utilidad para el tratamiento de cadenas.
 * 
 * @author XI316153
 */
public class StringHelper {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Salto de linea */
  public static final String SALTO_DE_LINEA = "\n";

  /** Número cero (0) como cadena. */
  public static final String CERO = "0";

  /** Espacio en blanco (" ") como cadena. */
  public static final String ESPACIO_EN_BLANCO = " ";

  /** Caracter '\0'. */
  public static final Character BARRA_CERO = '\0';

  /**
   * <p>
   * The maximum size to which the padding constant(s) can expand.
   * </p>
   */
  private static final int PAD_LIMIT = 8192;

  /**
   * A String for a space character.
   *
   * @since 3.2
   */
  public static final String SPACE = " ";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Rellena con 0 a la izquierda
   * 
   * @param i
   * @param n
   * @return
   */
  public static String padCerosLeft(Integer i, Integer n) {
    return StringUtils.leftPad(String.valueOf(i), n, "0");
  }

  /**
   * Rellena con 0 a la izquierda
   * 
   * @param i
   * @param n
   * @return
   */
  public static String padCerosLeft(String i, Integer n) {
    return StringUtils.leftPad(String.valueOf(i), n, "0");
  }

  public static String padRight(String s, Integer length) throws Exception {
    if (s == null) {
      s = "";
    }
    else {
      s = s.trim();
    }
    if (s.length() > length) {
      return s.substring(0, length);
    }
    return StringHelper.padStringWithSpacesRight(s, length);
  }

  /**
   * Verifica que al menos una posición este llena
   * 
   * @param arreglo
   * @return
   */
  public static boolean isArrayVacio(String[] arreglo) {
    if (arreglo == null)
      return true;
    boolean vacio = true;
    for (String pos : arreglo) {
      if (!StringUtils.isEmpty(pos)) {
        vacio = false;
      }
    }
    return vacio;
  }

  /**
   * Devuelve una cadena con la longitud especificada rellena con ceros.
   * 
   * @param length Tamaño de la cadena a devolver.
   * @return <code>java.lang.String - </code>Cadena generada.
   */
  public static String returnNCeros(int length) {
    StringBuffer outputBuffer = new StringBuffer(length);
    for (int i = 0; i < length; i++) {
      outputBuffer.append(CERO);
    } // for
    return outputBuffer.toString();
  } // returnNCeros

  /**
   * Devuelve una cadena con la longitud especificada rellena con espacios en
   * blanco.
   * 
   * @param length Tamaño de la cadena a devolver.
   * @return <code>java.lang.String - </code>Cadena generada.
   */
  public static String returnNWhiteSpaces(int length) {
    StringBuffer outputBuffer = new StringBuffer(length);
    for (int i = 0; i < length; i++) {
      outputBuffer.append(ESPACIO_EN_BLANCO);
    } // for
    return outputBuffer.toString();
  } // returnNWhiteSpaces

  /**
   * Devuelve una cadena con la longitud especificada rellena con los caracteres
   * '\0'.
   * 
   * @param length Tamaño de la cadena a devolver.
   * @return <code>java.lang.String - </code>Cadena generada.
   */
  public static String returnNBarraCeros(int length) {
    StringBuffer outputBuffer = new StringBuffer(length);
    for (int i = 0; i < length; i++) {
      outputBuffer.append(BARRA_CERO);
    } // for
    return outputBuffer.toString();
  } // returnNBarraCeros

  /**
   * Rellena la cadena especificada con espacios a la derecha hasta llegar a la
   * longitud especificada.
   * 
   * @param finalString Cadena a rellenar.
   * @param length Tamaño máximo de la cadena.
   * @return <code>java.lang.String - </code>Cadena modificada.
   * @throws Exception Si la cadena recibida excede el tamaño máximo
   * especificado.
   */
  public static String padStringWithSpacesRight(String finalString, Integer length) throws Exception {
    finalString = finalString.trim();
    if (finalString.length() == length) {
      return finalString;
    }
    else if (finalString.length() < length) {
      finalString = StringUtils.rightPad(finalString, length, ESPACIO_EN_BLANCO);
      return finalString;
    }
    else {
      throw new Exception(MessageFormat.format("La cadena [{}] es más larga que el espacio asignado de [{}]",
          finalString, length));
    } // else
  } // padStringWithSpacesRight

  public static BigDecimal convertNum(String msg, int length, int scale) {
    BigDecimal number = BigDecimal.ZERO;
    if (scale == 0) {
      number = new BigDecimal(msg.substring(0, length));
    }
    else {
      String integerPart = msg.substring(0, length - scale);
      String fractionalPart = msg.substring(length - scale, length);
      number = new BigDecimal(integerPart + "." + fractionalPart);
    }
    return number.setScale(scale, RoundingMode.HALF_UP);
  }

  public static String formatNum(BigDecimal value, int length, int scale) {
    String textBigDecimal = value.toPlainString();
    return textBigDecimal;
  }

  public static String fillString(String text, int length) {
    String filledString = new String();
    if (text.equals("")) {
      filledString = new String(new char[10]).replace('\0', ' ');
    }

    return filledString;
  }

  public static boolean isWhiteSpaces(String fieldText) {
    if (fieldText.trim().length() > 0) {
      return false;
    }
    return true;
  }

  public static boolean isZeroes(String fieldText) {
    return fieldText.matches("[0]+");
  }

  public static boolean isDistinctFrom(String stringToCompare, String... listOfStrings) {
    boolean isDistinct = true;
    for (String str : listOfStrings) {
      if (str.trim().equals(stringToCompare.trim())) {
        isDistinct = false;
        break;
      }
    }
    return isDistinct;
  }

  public static boolean isIn(String stringToCompare, String... listOfStrings) {
    boolean isIn = false;
    for (String str : listOfStrings) {
      if (stringToCompare.trim().equals(str.trim())) {
        isIn = true;
        break;
      }
    }
    return isIn;
  }

  public static String formatBigDecimal(BigDecimal number, int lengthEntera, int lengthDecimal) {

    if (lengthDecimal > 0) {
      number = number.setScale(lengthDecimal, RoundingMode.FLOOR);
      number = number.movePointRight(lengthDecimal);
    }
    number = number.setScale(0, RoundingMode.FLOOR).abs();
    String salida = StringUtils.leftPad(number.toPlainString(), lengthEntera + lengthDecimal, "0");
    return salida;

  }

  public static String formatBigDecimalWithNSign(BigDecimal number, int lengthEntera, int lengthDecimal) {
    String sign = "";
    if (number == null)
      number = BigDecimal.ZERO;
    if (number.compareTo(BigDecimal.ZERO) == -1) {
      sign = "N";
    }
    String salida = sign + formatBigDecimal(number, lengthEntera - sign.length(), lengthDecimal);
    return salida;
  }

  public static String formatBigDecimalWithSign(BigDecimal number, int lengthEntera, int lengthDecimal) {
    String sign = "+";
    if (number.compareTo(BigDecimal.ZERO) == -1) {
      sign = "-";
    }
    String salida = sign + formatBigDecimal(number, lengthEntera, lengthDecimal);
    return salida;
  }

  public static String cdBrokerToCode(String cdbroker) {
    switch (cdbroker.trim()) {
      case "BMEMADRID":
        return "001";
      case "BMEBARCELONA":
        return "002";
      case "BMEBILBAO":
        return "003";
      case "BMEVALENCIA":
        return "004";
      default:
        return "ERR";
    }
  }

  /**
   * Aplica subString en caso de que la longitud de la cadena de caracteres sea
   * acorde.
   * @param cadena cadena
   * @param inicio inicio
   * @param fin fin
   */
  public static String safeSubString(String cadena, int inicio, int fin) {
    if (StringUtils.isNotBlank(cadena) && cadena.length() > fin) {
      return cadena.substring(inicio, fin);
    }
    else {
      return cadena;
    }
  }

  /**
   * Conversion de String a Character.
   * @param param param
   * @return Character Character
   */
  public static Character getCharFromString(String param) {
    if (StringUtils.isNotEmpty(param)) {
      String paramPayload = StringUtils.remove(param, "'");
      if (StringUtils.isNotEmpty(paramPayload)) {
        return paramPayload.charAt(0);
      }
    }
    return null;
  }

  /**
   * Se quitan corchetes que vienen de una transformacion de array de
   * caracteres.
   * @param caracter caracter
   * @return Character Character
   */
  public static Character suppressBracketsFromCharacter(Character caracter) {
    if (caracter != null) {
      String x = String.valueOf(caracter);
      if (StringUtils.isNotBlank(x) && x.contains("[") && x.contains("]")) {
        return (caracter.toString().replace("[", "").replace("]", "").trim()).charAt(0);
      }
    }
    return caracter;
  }

  /**
   * Devuelve una serie de espacios si el campo viene blanqueado (null o "")
   * @param campo campo
   * @return String String
   */
  public static String populateWithSpacesIfBlank(String campo) {
    if (StringUtils.isNotBlank(campo)) {
      return campo;
    }
    return " ";
  }

  /**
   * Obtiene lista de ID desglose camara.
   * @param listaErroneos listaErroneos
   * @return String String
   */
  public static String getListaStringIdDesgloseCamara(List<Object[]> listaErroneos) {
    StringBuffer sbDesgloseCamara = new StringBuffer();
    if (CollectionUtils.isNotEmpty(listaErroneos)) {
      for (Object[] elem : listaErroneos) {
        if (elem[3] != null) {
          sbDesgloseCamara.append("" + (java.math.BigInteger) elem[3]).append(",");
        }
      }
      if (sbDesgloseCamara.length() > 0) {
        sbDesgloseCamara.substring(0, sbDesgloseCamara.length() - 1);
      }
    }
    return sbDesgloseCamara.toString();
  }

  /**
   * Chequea si una cadena de caracteres tiene formato numerico. Al usarlo que
   * toma como validos formatos dd.dd
   * @param str str
   * @return boolean boolean
   */
  public static boolean isNumericCeldaExcel(String str) {
    return StringUtils.isNotEmpty(str) && str.trim().matches("-?\\d+(\\.\\d+)?");
  }

  /**
   * Devuelve un Short numerico si o si - 0 si no es numerico.
   * @param str str
   * @return Short Short
   */
  public static Short getShortNumeric(String str) {
    if (StringUtils.isNotBlank(str)) {
      if (StringUtils.isNumeric(str)) {
        return new Short(str);
      }
    }
    return new Short("0");
  }

  /**
   * Quita comillas dobles de una cadena de caracteres.
   * @param valor
   * @return String
   */
  public static String quitarComillasDobles(String valor) {
    return StringUtils.isNotBlank(valor) ? valor.replaceAll("\"", "") : valor;
  }

  /**
   * Remueve las comillas del string
   * @param valor
   * @return
   */
  public static String quitarComillas(String valor) {
    return valor.replaceAll("\"", "");
  }

  /**
   * Retorna true/false si el valor incluye comillas
   * @param valor
   * @return
   */
  public static Boolean isValorEncomillado(String valor) {
    return valor.indexOf('"') >= 0;
  }

  /**
   * Chequeo por null para cadena de caracteres.
   * @param cadena cadena
   */
  public static String safeNull(String cadena) {
    if (StringUtils.isNotBlank(cadena)) {
      return cadena;
    }
    else {
      return "";
    }
  }

  /**
   * Suprime los espacios a cada lado del String.
   * @param cadena cadena
   */
  public static String getStringTrimmed(String cadena) {
    if (StringUtils.isNotBlank(cadena)) {
      return cadena.trim();
    }
    else {
      return cadena;
    }
  }

  /**
   * Asignacion de String.
   * @param filterString
   * @return
   */
  public static String processString(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    return processedString;
  }

  /**
   * Devuelve la cantidad de numeros consecutivos en una cadena de caracteres si
   * los hay. Caso contrario devuelve null.
   * @param cadena
   * @param cantidadDigitos
   * @return String
   */
  public static String getNumerosConsecutivosEnString(String cadena, int cantidadDigitos) {
    if (StringUtils.isNotBlank(cadena)) {
      Pattern p = Pattern.compile("(\\d{" + cantidadDigitos + "})");
      Matcher m = p.matcher(cadena);
      if (m.find()) {
        return m.group(0);
      }
    }
    return null;
  }

  public static String formatearCampo(String campo, int longitud, String relleno) {
    String resultado = "";

    if (null != campo) {
      campo = StringUtils.trim(campo);
    }

    if (null == campo) {
      resultado = StringUtils.leftPad(resultado, longitud, relleno);
    }
    else if (campo.length() < longitud) {
      resultado = StringUtils.leftPad(campo, longitud, relleno);
    }
    else if (campo.length() > longitud) {
      resultado = StringUtils.left(campo, longitud);
    }
    else {
      resultado = campo;
    }

    return resultado;
  }

  /**
   * Devuelve true si el String es numerico (incluye decimales)
   * @param cadena
   * @return boolean
   */
  public static boolean isNumerico(String cadena) {
    boolean resultado = true;
    try {
      return Double.parseDouble(cadena) != 0 || Double.parseDouble(cadena) == 0;
    }
    catch (NumberFormatException e) {
      resultado = false;
    }
    return resultado;
  }

  /**
   * Conversion de String a double.
   * @param cantidad
   * @return double
   */
  public static double stringToDouble(String cantidad) {
    double resultado = 0.0;
    int multiplicador = 1;
    String[] arrcantidad = cantidad.split("\\,");
    if (arrcantidad.length > 0) {
      if (cantidad.contains("-")) {
        multiplicador = -1;
      }

      String cantidadEntera = arrcantidad[0];
      String cantidadDecimal = "0";
      // si tiene parte decimal
      if (arrcantidad.length > 1) {
        cantidadDecimal = arrcantidad[1];
      }
      if (cantidadEntera != null) {
        if (!cantidadEntera.equals("")) {
          resultado = Math.abs(Double.parseDouble(cantidadEntera));
        }
      }
      if (cantidadDecimal != null) {
        if (cantidadDecimal.equals("")) {
          cantidadDecimal = "0";
        }
        int contDecimales_ot = cantidadDecimal.length();
        resultado += Math.abs(Double.parseDouble(cantidadDecimal) / Math.pow(10, contDecimales_ot));
      }
    }
    return multiplicador * resultado;
  }

  /**
   * Convierte String a Double suprimiendo "," como cifra de millares.
   *
   * @param cantidad
   * @return double
   */
  public static double stringToDoubleDecimalSupressComma(String cantidad) {
    cantidad = cantidad.replace(",", "");
    return stringToDouble(cantidad);
  }

  /**
   * Obtiene el valor formateado.
   * @param valor
   * @return patron
   */
  public static String getValorFormateado(double valor, String patron) {
    if (StringUtils.isNotBlank(patron)) {
      String patronModif = patron.replace("0", "#").replace(",", "");
      DecimalFormat formatterDecimal = new DecimalFormat(patronModif);
      String valorFormateado = formatterDecimal.format(valor);
      return valorFormateado;
    }
    else {
      return String.valueOf(valor);
    }

  }

  /**
   * <p>
   * Right pad a String with a specified String.
   * </p>
   *
   * <p>
   * The String is padded to the size of {@code size}.
   * </p>
   *
   * <pre>
   * StringUtils.rightPad(null, *, *)      = ""
   * StringUtils.rightPad("", 3, "z")      = "zzz"
   * StringUtils.rightPad("bat", 3, "yz")  = "bat"
   * StringUtils.rightPad("bat", 5, "yz")  = "batyz"
   * StringUtils.rightPad("bat", 8, "yz")  = "batyzyzy"
   * StringUtils.rightPad("bat", 1, "yz")  = "bat"
   * StringUtils.rightPad("bat", -1, "yz") = "bat"
   * StringUtils.rightPad("bat", 5, null)  = "bat  "
   * StringUtils.rightPad("bat", 5, "")    = "bat  "
   * </pre>
   *
   * @param str the String to pad out, may be null
   * @param size the size to pad to
   * @param padStr the String to pad with, null or empty treated as single space
   * @return right padded String or original String if no padding is necessary,
   * {@code null} if null String input
   */
  public static String rightPad(final String str, final int size, String padStr) {
    if (str == null) {
      return StringUtils.rightPad("", size, padStr);
    }
    if (isEmpty(padStr)) {
      padStr = SPACE;
    }
    final int padLen = padStr.length();
    final int strLen = str.length();
    final int pads = size - strLen;
    if (pads <= 0) {
      return str; // returns original String when possible
    }
    if (padLen == 1 && pads <= PAD_LIMIT) {
      return rightPad(str, size, padStr.charAt(0));
    }

    if (pads == padLen) {
      return str.concat(padStr);
    }
    else if (pads < padLen) {
      return str.concat(padStr.substring(0, pads));
    }
    else {
      final char[] padding = new char[pads];
      final char[] padChars = padStr.toCharArray();
      for (int i = 0; i < pads; i++) {
        padding[i] = padChars[i % padLen];
      }
      return str.concat(new String(padding));
    }
  }

  /**
   * <p>
   * Checks if a CharSequence is empty ("") or null.
   * </p>
   *
   * <pre>
   * StringUtils.isEmpty(null)      = true
   * StringUtils.isEmpty("")        = true
   * StringUtils.isEmpty(" ")       = false
   * StringUtils.isEmpty("bob")     = false
   * StringUtils.isEmpty("  bob  ") = false
   * </pre>
   *
   * <p>
   * NOTE: This method changed in Lang version 2.0. It no longer trims the
   * CharSequence. That functionality is available in isBlank().
   * </p>
   *
   * @param cs the CharSequence to check, may be null
   * @return {@code true} if the CharSequence is empty or null
   * @since 3.0 Changed signature from isEmpty(String) to isEmpty(CharSequence)
   */
  public static boolean isEmpty(final CharSequence cs) {
    return cs == null || cs.length() == 0;
  }

  /**
   * <p>
   * Right pad a String with a specified character.
   * </p>
   *
   * <p>
   * The String is padded to the size of {@code size}.
   * </p>
   *
   * <pre>
   * StringUtils.rightPad(null, *, *)     = null
   * StringUtils.rightPad("", 3, 'z')     = "zzz"
   * StringUtils.rightPad("bat", 3, 'z')  = "bat"
   * StringUtils.rightPad("bat", 5, 'z')  = "batzz"
   * StringUtils.rightPad("bat", 1, 'z')  = "bat"
   * StringUtils.rightPad("bat", -1, 'z') = "bat"
   * </pre>
   *
   * @param str the String to pad out, may be null
   * @param size the size to pad to
   * @param padChar the character to pad with
   * @return right padded String or original String if no padding is necessary,
   * {@code null} if null String input
   * @since 2.0
   */
  public static String rightPad(final String str, final int size, final char padChar) {
    if (str == null) {
      return null;
    }
    final int pads = size - str.length();
    if (pads <= 0) {
      return str; // returns original String when possible
    }
    if (pads > PAD_LIMIT) {
      return rightPad(str, size, String.valueOf(padChar));
    }
    return str.concat(repeat(padChar, pads));
  }

  /**
   * <p>
   * Returns padding using the specified delimiter repeated to a given length.
   * </p>
   *
   * <pre>
   * StringUtils.repeat('e', 0)  = ""
   * StringUtils.repeat('e', 3)  = "eee"
   * StringUtils.repeat('e', -2) = ""
   * </pre>
   *
   * <p>
   * Note: this method doesn't not support padding with <a
   * href="http://www.unicode.org/glossary/#supplementary_character">Unicode
   * Supplementary Characters</a> as they require a pair of {@code char}s to be
   * represented. If you are needing to support full I18N of your applications
   * consider using {@link #repeat(String, int)} instead.
   * </p>
   *
   * @param ch character to repeat
   * @param repeat number of times to repeat char, negative treated as zero
   * @return String with repeated character
   * @see #repeat(String, int)
   */
  public static String repeat(final char ch, final int repeat) {
    final char[] buf = new char[repeat];
    for (int i = repeat - 1; i >= 0; i--) {
      buf[i] = ch;
    }
    return new String(buf);
  }

  /**
   * <p>
   * Left pad a String with a specified character.
   * </p>
   *
   * <p>
   * Pad to a size of {@code size}.
   * </p>
   *
   * <pre>
   * StringUtils.leftPad(null, *, *)     = null
   * StringUtils.leftPad("", 3, 'z')     = "zzz"
   * StringUtils.leftPad("bat", 3, 'z')  = "bat"
   * StringUtils.leftPad("bat", 5, 'z')  = "zzbat"
   * StringUtils.leftPad("bat", 1, 'z')  = "bat"
   * StringUtils.leftPad("bat", -1, 'z') = "bat"
   * </pre>
   *
   * @param str the String to pad out, may be null
   * @param size the size to pad to
   * @param padChar the character to pad with
   * @return left padded String or original String if no padding is necessary,
   * {@code null} if null String input
   * @since 2.0
   */
  public static String leftPad(final String str, final int size, final char padChar) {
    if (str == null) {
      return null;
    }
    final int pads = size - str.length();
    if (pads <= 0) {
      return str; // returns original String when possible
    }
    if (pads > PAD_LIMIT) {
      return leftPad(str, size, String.valueOf(padChar));
    }
    return repeat(padChar, pads).concat(str);
  }

  /**
   * <p>
   * Left pad a String with a specified String.
   * </p>
   *
   * <p>
   * Pad to a size of {@code size}.
   * </p>
   *
   * <pre>
   * StringUtils.leftPad(null, *, *)      = null
   * StringUtils.leftPad("", 3, "z")      = "zzz"
   * StringUtils.leftPad("bat", 3, "yz")  = "bat"
   * StringUtils.leftPad("bat", 5, "yz")  = "yzbat"
   * StringUtils.leftPad("bat", 8, "yz")  = "yzyzybat"
   * StringUtils.leftPad("bat", 1, "yz")  = "bat"
   * StringUtils.leftPad("bat", -1, "yz") = "bat"
   * StringUtils.leftPad("bat", 5, null)  = "  bat"
   * StringUtils.leftPad("bat", 5, "")    = "  bat"
   * </pre>
   *
   * @param str the String to pad out, may be null
   * @param size the size to pad to
   * @param padStr the String to pad with, null or empty treated as single space
   * @return left padded String or original String if no padding is necessary,
   * {@code null} if null String input
   */
  public static String leftPad(final String str, final int size, String padStr) {
    if (str == null) {
      return null;
    }
    if (isEmpty(padStr)) {
      padStr = SPACE;
    }
    final int padLen = padStr.length();
    final int strLen = str.length();
    final int pads = size - strLen;
    if (pads <= 0) {
      return str; // returns original String when possible
    }
    if (padLen == 1 && pads <= PAD_LIMIT) {
      return leftPad(str, size, padStr.charAt(0));
    }

    if (pads == padLen) {
      return padStr.concat(str);
    }
    else if (pads < padLen) {
      return padStr.substring(0, pads).concat(str);
    }
    else {
      final char[] padding = new char[pads];
      final char[] padChars = padStr.toCharArray();
      for (int i = 0; i < pads; i++) {
        padding[i] = padChars[i % padLen];
      }
      return new String(padding).concat(str);
    }
  }

  /**
   * Esto se usa en generacion de informes ya que filtra el contenido de las
   * queries realizadas con un tipo Object con valor "null" para datos null.
   * Devuelve true si el String evaluado no es: - "" - null - "null"
   * @param valor
   * @return boolean
   */
  public static boolean isNotBlankAndStringNotNull(String valor) {
    return StringUtils.isNotBlank(valor) && !("null".equals(valor));
  }

} // StringHelper
