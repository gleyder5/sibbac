package sibbac.business.wrappers.database.bo.calculos.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class Tcli0comDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5563371286468994585L;

  private final String cdproduc;
  private final int nuctapro;

  private final BigDecimal pccomsis;
  private final BigDecimal pcbansis;

  public Tcli0comDTO(String cdproduc, int nuctapro, BigDecimal pccomsis, BigDecimal pcbansis) {
    super();
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
    this.pccomsis = pccomsis;
    this.pcbansis = pcbansis;
  }

  public String getCdproduc() {
    return cdproduc;
  }

  public int getNuctapro() {
    return nuctapro;
  }

  public BigDecimal getPccomsis() {
    return pccomsis;
  }

  public BigDecimal getPcbansis() {
    return pcbansis;
  }

 

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdproduc == null) ? 0 : cdproduc.hashCode());
    result = prime * result + nuctapro;
    result = prime * result + ((pcbansis == null) ? 0 : pcbansis.hashCode());
    result = prime * result + ((pccomsis == null) ? 0 : pccomsis.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tcli0comDTO other = (Tcli0comDTO) obj;
    if (cdproduc == null) {
      if (other.cdproduc != null)
        return false;
    }
    else if (!cdproduc.equals(other.cdproduc))
      return false;
    if (nuctapro != other.nuctapro)
      return false;
    if (pcbansis == null) {
      if (other.pcbansis != null)
        return false;
    }
    else if (!pcbansis.equals(other.pcbansis))
      return false;
    if (pccomsis == null) {
      if (other.pccomsis != null)
        return false;
    }
    else if (!pccomsis.equals(other.pccomsis))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Tcli0comDTO [cdproduc=" + cdproduc + ", nuctapro=" + nuctapro + ", pccomsis=" + pccomsis + ", pcbansis="
           + pcbansis + "]";
  }

}

