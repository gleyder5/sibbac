package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.DesgloseDAO;
import sibbac.business.wrappers.database.model.Tmct0Desglose;
import sibbac.database.bo.AbstractBo;
@Service
public class DesgloseBO extends AbstractBo<Tmct0Desglose, Long, DesgloseDAO> {

}
