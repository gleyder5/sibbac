package sibbac.business.wrappers.database.bo;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.GruposImpositivosDao;
import sibbac.business.wrappers.database.model.GruposImpositivos;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;


@Service
public class GruposImpositivosBo extends AbstractBo< GruposImpositivos, Long, GruposImpositivosDao > {

	/**
	 * Recupea el grupo impositivo activo
	 *
	 * @return
	 */
	public GruposImpositivos findGrupoImpositivoActivo() {
		return dao.findByValidoDesdeBeforeAndCancelado( new Date(), GruposImpositivos.GRUPO_IMPOSITIVO_CANCELADO_N );
	}

	/**
	 * Crea un grupo impositivo en la tabla grupos impositivos
	 *
	 * @param params
	 * @return
	 */
	@Transactional
	public Map< String, String > createGrupoImpositivo( Map< String, String > params ) {

		LOG.trace( "Se inicia la creacion de un grupo impositivo" );

		Map< String, String > resultado = new HashMap< String, String >();
		GruposImpositivos gi = new GruposImpositivos();

		gi.setCancelado( params.get( Constantes.PARAM_CANCELADO ) );
		gi.setCategoria( params.get( Constantes.PARAM_CATEGORIA ) );
		gi.setDescripcion( params.get( Constantes.PARAM_DESCRIPCION ) );
		gi.setPorcentaje( Double.parseDouble( params.get( Constantes.PARAM_PORCENTAJE ) ) );
		gi.setValidoDesde( FormatDataUtils.convertStringToDate( params.get( Constantes.PARAM_FHVALIDODESDE ) ) );

		String result = "";

		if ( this.save( gi ) != null ) {
			result = " Se ha creado el grupo impositivo";
			LOG.trace( "Se ha creado el grupo impositivo" );
		} else {
			result = " No se ha creado el grupo impositivo";
			LOG.trace( "No se ha creado el grupo impositivo" );
		}

		resultado.put( "result", result );
		return resultado;

	}

	public Map< String, GruposImpositivos > getGruposImpositivos() {

		LOG.trace( "Se inicia la creacion de la lista de los Grupos Impositivos" );
		Map< String, GruposImpositivos > resultados = new HashMap< String, GruposImpositivos >();

		List< GruposImpositivos > listaGruposImpositivos = ( List< GruposImpositivos > ) this.findAll();

		for ( GruposImpositivos gruposImpositivos : listaGruposImpositivos ) {

			resultados.put( gruposImpositivos.getId().toString(), gruposImpositivos );
		}

		LOG.trace( "Se ha creado la lista de los Grupos Impositivos" );
		return resultados;

	}

	/**
	 * Cambia un objeto grupo impositivo recuperado de la tabla grupo impositivo
	 * modificando los campos que se pasan por parametro (params)
	 *
	 * @param params
	 * @return
	 */
	@Transactional
	public Map< String, String > modificarGruposImpositivos( Map< String, String > params ) {

		LOG.trace( " Se inicia la modificacion de un Grupo Imposisitivo " );
		Map< String, String > resultado = new HashMap< String, String >();

		if ( params == null ) {
			LOG.trace( "[El params de Grupo Impositivo es nulo]" );
		}
		LOG.trace( "[Comienza la búsqueda del id de Grupo Impositivo]" );
		GruposImpositivos gruposImpositivos = this.findById( Long.parseLong( params.get( Constantes.PARAM_ID_GRUPOIMPOSITIVO ) ) );
		LOG.trace( "[Encuentra id de Grupos Impositivos]" );
		gruposImpositivos.setPorcentaje( ( Double.parseDouble( params.get( Constantes.PARAM_PORCENTAJE ) ) ) );
		LOG.trace( "[Encuentra % de Grupos Impositivos]" );
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
		Date fechaGrupoImpositivo = new Date();
		try {
			fechaGrupoImpositivo = dateFormat.parse( params.get( Constantes.PARAM_FHVALIDODESDE ) );
			gruposImpositivos.setValidoDesde( fechaGrupoImpositivo );
			LOG.trace( "[Encuentra Fecha de Grupos Impositivos]" );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}

		// gruposImpositivos.setCategoria(params.get(Constantes.PARAM_CATEGORIA));
		// LOG.trace("[Encuentra Categoria de Grupos Impositivos]");
		// gruposImpositivos.setDescripcion(params
		// .get(Constantes.PARAM_DESCRIPCION_GRUPOSIMPOSITIVOS));
		// LOG.trace("[Encuentra Descripcion de Grupos Impositivos]");
		// gruposImpositivos.setCancelado(params.get(Constantes.PARAM_CANCELADO));
		// LOG.trace("[Encuentra Cancelado de Grupos Impositivos]");

		String result = "";

		if ( this.save( gruposImpositivos ) != null ) {

			result = "Se ha modificado el grupoImpositivo";
			LOG.trace( " Se ha modificado el grupoImpositivo " );

		} else {

			result = "No se modificado el grupoImpositivo";
			LOG.trace( " No se modificado el grupoImpositivo " );

		}

		resultado.put( "result", result );
		return resultado;

	}
}
