package sibbac.business.wrappers.database.filter;

import java.util.Date;
import java.util.List;

import sibbac.database.AbstractQueryFilter;
import sibbac.database.DynamicColumn;

/**
 * Implementacion de un AbstracFilter para la pantalla de Control de Titulares (alias clearingTitulares).
 */
public class TitularesQuery extends AbstractQueryFilter<TitularesFilter> {

    /**
     * Select para las sentencias count
     */
    private static final String QUERY_SELECT_COUNT = "SELECT COUNT(A.NUORDEN), B.FETRADE";
    
    /**
     * Select para el tipo de búsqueda 'TODOS LOS TITULARES'
     */
    private static final String QUERY_SELECT_TODOS_TITULARES_INT = "SELECT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || COALESCE(F.NUCNFLIQ, E.NUCNFLIQ, 0) AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, E.NUREFORD, '') REF_CLIENTE, " +
            "COALESCE(F.CCV, E.CCV, '') CCV, " +
            "A.CDTPOPER, A.CDISIN, A.NUTITCLI, '' AS PRECIO , O.CDCLSMDO, " +
            "COALESCE(F.NBCLIENT, E.NBCLIENT, '') NBCLIENT, " +
            "COALESCE(F.NBCLIENT1, E.NBCLIENT1, '') NBCLIENT1, " +
            "COALESCE(F.NBCLIENT2, E.NBCLIENT2, '') NBCLIENT2, " +
            "COALESCE(F.TPIDENTI, E.TPIDENTI, '') TPIDENTI, " +
            "COALESCE(F.CDHOLDER, E.NUDNICIF, '') NUDNICIF,  " +
            "COALESCE(F.TPSOCIED, E.TPSOCIED, '') TPSOCIED,  " +
            "COALESCE(F.CDDEPAIS, E.CDDEPAIS) CDDEPAIS, " +
            "COALESCE(F.TPNACTIT, E.TPNACTIT) AS NAC,  " +
            "COALESCE(F.NBDOMICI, E.NBDOMICI, '') NBDOMICI, " +
            "COALESCE(F.NBCIUDAD, E.NBCIUDAD, '') NBCIUDAD, " +
            "COALESCE(F.NBPROVIN, E.NBPROVIN, '') NBPROVIN, " +
            "COALESCE(F.CDPOSTAL, E.CDPOSTAL, '') CDPOSTAL, " +
            "COALESCE(F.TPTIPREP, E.TPTIPREP, '') TPTIPREP, " +
            "COALESCE(F.PARTICIP, E.PARTICIP) PARTICIP, " +
            "B.FETRADE, B.CDALIAS, B.DESCRALI,  " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, COALESCE(F.NUCNFLIQ, E.NUCNFLIQ, 0) NUCNFLIQ, " +
            "F.NUSECUEN, " +
            "COALESCE(F.CDNACTIT, E.CDNACTIT, '') CDNACTIT, " +
            "COALESCE(F.TPDOMICI, E.TPDOMICI, '') TPDOMICI, " +
            "COALESCE(F.NUDOMICI, E.NUDOMICI, '') NUDOMICI, " +
            "COALESCE(F.CDINACTI, 'A') CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'TODOS LOS TITULARES'
     */
    private static final String QUERY_SELECT_TODOS_TITULARES_NAC = "SELECT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || COALESCE(F.NUCNFLIQ, E.NUCNFLIQ, 0) AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, E.NUREFORD, '') REF_CLIENTE, " +
            "COALESCE(F.CCV, E.CCV, '') CCV, " +
            "A.CDTPOPER, A.CDISIN, L.NUTITLIQ, '' AS PRECIO , O.CDCLSMDO, " +
            "COALESCE(F.NBCLIENT, E.NBCLIENT, '') NBCLIENT, " +
            "COALESCE(F.NBCLIENT1, E.NBCLIENT1, '') NBCLIENT1, " +
            "COALESCE(F.NBCLIENT2, E.NBCLIENT2, '') NBCLIENT2, " +
            "COALESCE(F.TPIDENTI, E.TPIDENTI, '') TPIDENTI, " +
            "COALESCE(F.CDHOLDER, E.NUDNICIF, '') NUDNICIF,  " +
            "COALESCE(F.TPSOCIED, E.TPSOCIED, '') TPSOCIED,  " +
            "COALESCE(F.CDDEPAIS, E.CDDEPAIS) CDDEPAIS, " +
            "COALESCE(F.TPNACTIT, E.TPNACTIT) AS NAC,  " +
            "COALESCE(F.NBDOMICI, E.NBDOMICI, '') NBDOMICI, " +
            "COALESCE(F.NBCIUDAD, E.NBCIUDAD, '') NBCIUDAD, " +
            "COALESCE(F.NBPROVIN, E.NBPROVIN, '') NBPROVIN, " +
            "COALESCE(F.CDPOSTAL, E.CDPOSTAL, '') CDPOSTAL, " +
            "COALESCE(F.TPTIPREP, E.TPTIPREP, '') TPTIPREP, " +
            "COALESCE(F.PARTICIP, E.PARTICIP) PARTICIP, " +
            "B.FETRADE, B.CDALIAS, B.DESCRALI,  " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, COALESCE(F.NUCNFLIQ, E.NUCNFLIQ, 0) NUCNFLIQ, " +
            "F.NUSECUEN, " +
            "COALESCE(F.CDNACTIT, E.CDNACTIT, '') CDNACTIT, " +
            "COALESCE(F.TPDOMICI, E.TPDOMICI, '') TPDOMICI, " +
            "COALESCE(F.NUDOMICI, E.NUDOMICI, '') NUDOMICI, " +
            "COALESCE(F.CDINACTI, 'A') CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'DESGLOSES SIN TITULAR'
     */
    private static final String QUERY_SELECT_SIN_TITULAR_INT = "SELECT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " + 
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || COALESCE(F.NUCNFLIQ, E.NUCNFLIQ, 0) AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, E.NUREFORD, '') REF_CLIENTE, " +
            "'' AS CCV, " +
            "A.CDTPOPER, A.CDISIN, A.NUTITCLI, '' AS PRECIO , O.CDCLSMDO, " +
            "'' AS NBCLIENT, " +
            "'' AS NBCLIENT1, " +
            "'' AS NBCLIENT2, " +
            "'' AS TPIDENTI, " +
            "'' AS NUDNICIF,  " +
            "'' AS TPSOCIED,  " +
            "'' AS CDDEPAIS, " +
            "'' AS NAC,  " +
            "'' AS NBDOMICI, " +
            "'' AS NBCIUDAD, " +
            "'' AS NBPROVIN, " +
            "'' AS CDPOSTAL, " +
            "'' AS TPTIPREP, " +
            "'' AS PARTICIP, " +
            "B.FETRADE, B.CDALIAS, B.DESCRALI,  " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT,  " +
            "COALESCE(E.NUCNFLIQ, 0) AS NUCNFLIQ, NULL AS NUSECUEN, " +
            "'' AS CDNACTIT, '' AS TPDOMICI, '' AS NUDOMICI, 'A' AS CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'DESGLOSES SIN TITULAR'
     */
    private static final String QUERY_SELECT_SIN_TITULAR_NAC = "SELECT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " + 
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || COALESCE(F.NUCNFLIQ, E.NUCNFLIQ, 0) AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, E.NUREFORD, '') REF_CLIENTE, " +
            "'' AS CCV, " +
            "A.CDTPOPER, A.CDISIN, L.NUTITLIQ, '' AS PRECIO , O.CDCLSMDO, " +
            "'' AS NBCLIENT, " +
            "'' AS NBCLIENT1, " +
            "'' AS NBCLIENT2, " +
            "'' AS TPIDENTI, " +
            "'' AS NUDNICIF,  " +
            "'' AS TPSOCIED,  " +
            "'' AS CDDEPAIS, " +
            "'' AS NAC,  " +
            "'' AS NBDOMICI, " +
            "'' AS NBCIUDAD, " +
            "'' AS NBPROVIN, " +
            "'' AS CDPOSTAL, " +
            "'' AS TPTIPREP, " +
            "'' AS PARTICIP, " +
            "B.FETRADE, B.CDALIAS, B.DESCRALI,  " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT,  " +
            "COALESCE(E.NUCNFLIQ, 0) AS NUCNFLIQ, NULL AS NUSECUEN, " +
            "'' AS CDNACTIT, '' AS TPDOMICI, '' AS NUDOMICI, 'A' AS CDINACTI ";
            
    /**
     * Select para el tipo de búsqueda 'TITULARES SIN ERROR'
     */
    private static final String QUERY_SELECT_SIN_ERROR_INT = "SELECT 'false' AS EDITAR, " +
            "NULL AS ID_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || F.NUCNFLIQ AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, '') REF_CLIENTE, F.CCV, " +
            "A.CDTPOPER, A.CDISIN, A.NUTITCLI, '' AS PRECIO , O.CDCLSMDO, " +
            "F.NBCLIENT, F.NBCLIENT1, F.NBCLIENT2, F.TPIDENTI, F.CDHOLDER, " +
            "F.TPSOCIED, F.CDDEPAIS, " +
            "F.TPNACTIT AS NAC,  " +
            "F.NBDOMICI, F.NBCIUDAD, F.NBPROVIN, F.CDPOSTAL, F.TPTIPREP, " +
            "F.PARTICIP, B.FETRADE, B.CDALIAS, B.DESCRALI, " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, F.NUCNFLIQ, F.NUSECUEN, " +
            "F.CDNACTIT, F.TPDOMICI, F.NUDOMICI, F.CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'TITULARES SIN ERROR'
     */
    private static final String QUERY_SELECT_SIN_ERROR_NAC = "SELECT 'false' AS EDITAR, " +
            "NULL AS ID_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || F.NUCNFLIQ AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, '') REF_CLIENTE, F.CCV, " +
            "A.CDTPOPER, A.CDISIN, L.NUTITLIQ, '' AS PRECIO , O.CDCLSMDO, " +
            "F.NBCLIENT, F.NBCLIENT1, F.NBCLIENT2, F.TPIDENTI, F.CDHOLDER, " +
            "F.TPSOCIED, F.CDDEPAIS, " +
            "F.TPNACTIT AS NAC,  " +
            "F.NBDOMICI, F.NBCIUDAD, F.NBPROVIN, F.CDPOSTAL, F.TPTIPREP, " +
            "F.PARTICIP, B.FETRADE, B.CDALIAS, B.DESCRALI, " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, F.NUCNFLIQ, F.NUSECUEN, " +
            "F.CDNACTIT, F.TPDOMICI, F.NUDOMICI, F.CDINACTI ";

    /**
     * Select para el tipo de búsqueda 'TITULARES CON ERROR'
     */
    private static final String QUERY_SELECT_CON_ERROR_INT = "SELECT DISTINCT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || E.NUCNFLIQ AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, '') REF_CLIENTE, E.CCV, " +
            "A.CDTPOPER, A.CDISIN, A.NUTITCLI, '' AS PRECIO , O.CDCLSMDO, " +
            "E.NBCLIENT, E.NBCLIENT1, E.NBCLIENT2, E.TPIDENTI, COALESCE(E.NUDNICIF, ''), " +
            "E.TPSOCIED, E.CDDEPAIS, " +
            "E.TPNACTIT AS NAC,  " +
            "E.NBDOMICI, E.NBCIUDAD, E.NBPROVIN, E.CDPOSTAL, E.TPTIPREP, " +
            "E.PARTICIP, B.FETRADE, B.CDALIAS, B.DESCRALI, " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, E.NUCNFLIQ, NULL AS NUSECUEN, " +
            "E.CDNACTIT, E.TPDOMICI, E.NUDOMICI, 'A' AS CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'TITULARES CON ERROR'
     */
    private static final String QUERY_SELECT_CON_ERROR_NAC = "SELECT DISTINCT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || E.NUCNFLIQ AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, '') REF_CLIENTE, E.CCV, " +
            "A.CDTPOPER, A.CDISIN, L.NUTITLIQ, '' AS PRECIO , O.CDCLSMDO, " +
            "E.NBCLIENT, E.NBCLIENT1, E.NBCLIENT2, E.TPIDENTI, COALESCE(E.NUDNICIF, ''), " +
            "E.TPSOCIED, E.CDDEPAIS, " +
            "E.TPNACTIT AS NAC,  " +
            "E.NBDOMICI, E.NBCIUDAD, E.NBPROVIN, E.CDPOSTAL, E.TPTIPREP, " +
            "E.PARTICIP, B.FETRADE, B.CDALIAS, B.DESCRALI, " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, E.NUCNFLIQ, NULL AS NUSECUEN, " +
            "E.CDNACTIT, E.TPDOMICI, E.NUDOMICI, 'A' AS CDINACTI ";
    
    
    
    /**
     * Select para el tipo de búsqueda 'TITULARES CON ERROR'
     */
    private static final String QUERY_SELECT_CON_ERROR_EXPORT_INT = "SELECT DISTINCT  " +
            "CD.CODIGO AS CODIGO_ERROR, CD.DESCRIPCION AS DESCRIPCION_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || E.NUCNFLIQ AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, '') REF_CLIENTE, E.CCV, " +
            "A.CDTPOPER, A.CDISIN, A.NUTITCLI, '' AS PRECIO , O.CDCLSMDO, " +
            "E.NBCLIENT, E.NBCLIENT1, E.NBCLIENT2, E.TPIDENTI, COALESCE(E.NUDNICIF, ''), " +
            "E.TPSOCIED, E.CDDEPAIS, " +
            "E.TPNACTIT AS NAC,  " +
            "E.NBDOMICI, E.NBCIUDAD, E.NBPROVIN, E.CDPOSTAL, E.TPTIPREP, " +
            "E.PARTICIP, B.FETRADE, B.CDALIAS, B.DESCRALI, " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, E.NUCNFLIQ, NULL AS NUSECUEN, " +
            "E.CDNACTIT, E.TPDOMICI, E.NUDOMICI, 'A' AS CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'TITULARES CON ERROR'
     */
    private static final String QUERY_SELECT_CON_ERROR_EXPORT_NAC = "SELECT DISTINCT  " +
            "CD.CODIGO AS CODIGO_ERROR, CD.DESCRIPCION AS DESCRIPCION_ERROR, " +
            "A.NBOOKING || '/' || A.NUCNFCLT  ||  '/' || E.NUCNFLIQ AS BOOKING, " +
            "COALESCE(A.REF_CLIENTE, O.NUREFORD, '') REF_CLIENTE, E.CCV, " +
            "A.CDTPOPER, A.CDISIN, L.NUTITLIQ, '' AS PRECIO , O.CDCLSMDO, " +
            "E.NBCLIENT, E.NBCLIENT1, E.NBCLIENT2, E.TPIDENTI, COALESCE(E.NUDNICIF, ''), " +
            "E.TPSOCIED, E.CDDEPAIS, " +
            "E.TPNACTIT AS NAC,  " +
            "E.NBDOMICI, E.NBCIUDAD, E.NBPROVIN, E.CDPOSTAL, E.TPTIPREP, " +
            "E.PARTICIP, B.FETRADE, B.CDALIAS, B.DESCRALI, " +
            "A.NUORDEN, A.NBOOKING, A.NUCNFCLT, E.NUCNFLIQ, NULL AS NUSECUEN, " +
            "E.CDNACTIT, E.TPDOMICI, E.NUDOMICI, 'A' AS CDINACTI ";
    
    /**
     * Select para el tipo de búsqueda 'TITULARES ROUTING SIN ORDEN'
     */
    private static final String QUERY_SELECT_ERROR_SIN_ORDEN = "SELECT DISTINCT 'false' AS EDITAR, " +
            "E.ID AS ID_ERROR, " +
            "COALESCE(NBOOKING, '') || '/' || COALESCE(NUCNFCLT, '')  AS BOOKING, " +
            "COALESCE(NUREFORD, '') AS REF_CLIENTE, E.CCV, " +
            "'' AS CDTPOPER, '' AS CDISIN, '' AS NUTITCLI, '' AS PRECIO , '' AS CDCLSMDO, " +
            "E.NBCLIENT, E.NBCLIENT1, E.NBCLIENT2, E.TPIDENTI, COALESCE(E.NUDNICIF, ''), " +
            "E.TPSOCIED, E.CDDEPAIS, " +
            "E.TPNACTIT AS NAC, " +
            "E.NBDOMICI, E.NBCIUDAD, E.NBPROVIN, E.CDPOSTAL, E.TPTIPREP, " +
            "E.PARTICIP, '' AS FEOPERAC, '' AS CDALIAS, '' AS DESCRALI, "+
            "E.NUORDEN, E.NBOOKING, E.NUCNFCLT, E.NUCNFLIQ, NULL AS NUSECUEN, " +
            "E.CDNACTIT, E.TPDOMICI, E.NUDOMICI, 'A' AS CDINACTI ";

    /**
     * From para todos los tipos de búsqueda excepto 'TITULARES ROUTING SIN ORDEN'. Define los alias
     * <ul>
     *   <li><dfn>A</dfn> TMCT0ALO</li>
     *   <li><dfn>B</dfn> TMCT0BOK</li>
     *   <li><dfn>O</dfn> TMCT0ORD</li>
     * </ul>
     */
    private static final String QUERY_FROM = "FROM TMCT0ALO A " +
            "INNER JOIN TMCT0BOK B ON " +
            "B.NBOOKING = A.NBOOKING AND " +
            "B.NUORDEN = A.NUORDEN " +
            "INNER JOIN TMCT0ORD O ON " +
            "O.NUORDEN = B.NUORDEN ";
    
    /**
     * From para el tipo de búsqueda  'TITULARES ROUTING SIN ORDEN'. Define el alias <dfn>E</dfn> TMCT0_AFI_ERROR
     */
    private static final String QUERY_FROM_ERROR = "FROM TMCT0_AFI_ERROR E ";

    /**
     * Inner join para las consultas de nacional. Define el alias <dfn>L</dfn> TMCT0ALC
     */
    private static final String QUERY_INNER_NAC = "INNER JOIN TMCT0ALC L ON " +
            "L.NUORDEN  = A.NUORDEN AND " +
            "L.NBOOKING = A.NBOOKING AND " +
            "L.NUCNFCLT = A.NUCNFCLT  AND"
            + " L.CDESTADOASIG > 0 " ;
    
    /**
     * Join para traer un afi en mercado nacional
     */
    private static final String QUERY_AFI_JOIN_NAC = "JOIN TMCT0AFI F ON " +
            "F.NUORDEN =  L.NUORDEN AND " +
            "F.NBOOKING = L.NBOOKING AND " +
            "F.NUCNFCLT = L.NUCNFCLT AND " + 
            "F.NUCNFLIQ = L.NUCNFLIQ AND"
            + " F.CDINACTI ='A' ";

    /**
     * Join para traer un afi error en mercado nacional
     */
    private static final String QUERY_ERROR_JOIN_NAC =  "JOIN TMCT0_AFI_ERROR E ON " +
            "E.NUORDEN = L.NUORDEN AND " +
            "E.NBOOKING = L.NBOOKING AND " +
            "E.NUCNFCLT = L.NUCNFCLT AND " + 
            "E.NUCNFLIQ = L.NUCNFLIQ ";
    
    /**
     * Join para traer un afi en mercado internacional
     */
    private static final String QUERY_AFI_JOIN_INT = "JOIN TMCT0AFI F ON " +
            "F.NUORDEN = A.NUORDEN AND " +
            "F.NBOOKING = A.NBOOKING AND " +
            "F.NUCNFCLT = A.NUCNFCLT AND " + 
            "F.NUCNFLIQ = 0 AND "
            + " F.CDINACTI ='A' ";

    /**
     * Join para traer un afi error en mercado internacional
     */
    private static final String QUERY_ERROR_JOIN_INT =  "JOIN TMCT0_AFI_ERROR E ON " +
            "E.NUORDEN = A.NUORDEN AND " +
            "E.NBOOKING = A.NBOOKING AND " +
            "E.NUCNFCLT = A.NUCNFCLT AND " + 
            "E.NUCNFLIQ = 0 ";
    
    private static final String QUERY_ERROR_CODE_JOIN = "LEFT JOIN TMCT0_AFI_ERROR_CODIGO_ERROR C ON " +
            "C.ID_AFI_ERROR = E.ID " +
            "LEFT JOIN TMCT0_CODIGO_ERROR CD " +
            "ON CD.ID = C.ID_CODIGO_ERROR ";
    
    private static final String QUERY_WHERE_END_TODOS_TITULARES = 
            " AND (E.NUORDEN is not null OR F.NUORDEN IS NOT NULL) ";
    
    private static final String QUERY_WHERE_END_SIN_TITULAR =
            " AND E.NUORDEN IS NULL AND F.NUORDEN IS NULL ";

    private static final String QUERY_WHERE_END_NAC = "AND A.CDESTADOASIG >= 65 ";
    
    private static final String QUERY_WHERE_END_INT = "AND A.CDESTADO != '002' ";
    
    private static final String FIELD_FEOPERAC = "B.FETRADE";
    
    private static final String FIELD_AFI_NBCLIENT = "TRIM(F.NBCLIENT)";
    
    private static final String FIELD_ERROR_NBCLIENT = "E.NBCLIENT";
    
    private static final String FIELD_AFI_NBCLIENT1 = "TRIM(F.NBCLIENT1)";
    
    private static final String FIELD_ERROR_NBCLIENT1 = "E.NBCLIENT1";

    private static final String FIELD_AFI_NBCLIENT2 = "TRIM(F.NBCLIENT2)";
    
    private static final String FIELD_ERROR_NBCLIENT2 = "E.NBCLIENT2";
    
    private static final String FIELD_AFI_NUDNICIF = "TRIM(F.CDHOLDER)";
    
    private static final String FIELD_ERROR_NUDNICIF = "E.NUDNICIF";

    private static final String FIELD_MERCADO = "O.CDCLSMDO";
    
    private static final String FIELD_SENTIDO = "A.CDTPOPER";
    
    private static final String FIELD_CDISIN =  "A.CDISIN";
    
    private static final String FIELD_CDALIAS = "B.CDALIAS";
    
    private static final String FIELD_REF_CLIENTE = "A.REF_CLIENTE";
    
    private static final String FIELD_BOOKING = "A.NBOOKING";
    
    private static final String FIELD_ERROR_CODE = "CD.CODIGO";
    
        
  public TitularesQuery(TitularesFilter filter) {
    super(filter, FIELD_FEOPERAC);
  }

  @Override
  public boolean isCountNecessary() {
    final Date fechaDesde;

    fechaDesde = filter.getFechaDesde();
    if (fechaDesde != null && fechaDesde.equals(filter.getFechaHasta())) {
      singleDate(fechaDesde);
      return false;
    }
    return true;
  }

  @Override
  protected boolean buildCountQuery() {
    if (filter.isByPass()) {
      return false;
    }
    switch (filter.getTipoConsulta()) {
      case BUSQUEDA_TODOS_LOS_TITULARES:
        buildCountTodosTitulares();
        return true;
      case BUSQUEDA_TITULARES_SIN_ERROR:
        buildCountSinError();
        return true;
      case BUSQUEDA_TITULARES_CON_ERROR:
        buildCountConError();
        return true;
      case BUSQUEDA_TITULARES_ROUTING_SIN_ORDEN_SV:
        return false;
      case BUSQUEDA_DESGLOSES_SIN_TITULAR:
        buildCountSinTitular();
        return true;
      default:
        return false;
    }
  }

  @Override
  protected void buildSelectQuery() {
    switch (filter.getTipoConsulta()) {
      case BUSQUEDA_TODOS_LOS_TITULARES:
        buildSelectTodosTitulares();
        break;
      case BUSQUEDA_TITULARES_SIN_ERROR:
        buildSelectSinError();
        break;
      case BUSQUEDA_TITULARES_CON_ERROR:
        buildSelectConError();
        break;
      case BUSQUEDA_TITULARES_ROUTING_SIN_ORDEN_SV:
        buildSelectErrorSinOrden();
        break;
      case BUSQUEDA_DESGLOSES_SIN_TITULAR:
        buildSelectSinTitular();
    }
  }

  /**
   * Se sobreescribe el método splitMainData para evitar que el split de fechas
   * sea obligatorio
   */
  @Override
  protected void splitMainDate(Date begin, Date end) {
    if (!filter.isByPass() || begin != null) {
      super.splitMainDate(begin, end);
    }
  }

  private void buildCountTodosTitulares() {
    selectBuilder.append(QUERY_SELECT_COUNT);
    fromBuilder.append(QUERY_FROM);
    if (nac())
      fromBuilder.append(QUERY_INNER_NAC);
    leftJoins();
    splitMainDate(filter.getFechaDesde(), filter.getFechaHasta());
    where();
    whereBuilder.append(QUERY_WHERE_END_TODOS_TITULARES);
    groupByBuilder.append(FIELD_FEOPERAC);
  }

  private void buildCountSinTitular() {
    selectBuilder.append(QUERY_SELECT_COUNT);
    fromBuilder.append(QUERY_FROM);
    if (nac())
      fromBuilder.append(QUERY_INNER_NAC);
    fromBuilder.append("LEFT ").append(nac() ? QUERY_AFI_JOIN_NAC : QUERY_AFI_JOIN_INT);
    fromBuilder.append("LEFT ").append(nac() ? QUERY_ERROR_JOIN_NAC : QUERY_ERROR_JOIN_INT);
    splitMainDate(filter.getFechaDesde(), filter.getFechaHasta());
    where();
    whereBuilder.append(QUERY_WHERE_END_SIN_TITULAR);
    groupByBuilder.append(FIELD_FEOPERAC);
  }

  private void buildCountSinError() {
    selectBuilder.append(QUERY_SELECT_COUNT);
    fromBuilder.append(QUERY_FROM);
    if (nac())
      fromBuilder.append(QUERY_INNER_NAC);
    fromBuilder.append("INNER ").append(nac() ? QUERY_AFI_JOIN_NAC : QUERY_AFI_JOIN_INT);
    onAfi();
    splitMainDate(filter.getFechaDesde(), filter.getFechaHasta());
    where();
    groupByBuilder.append(FIELD_FEOPERAC);
  }

  private void buildCountConError() {
    selectBuilder.append(QUERY_SELECT_COUNT);
    fromBuilder.append(QUERY_FROM);
    if (nac()) {
      fromBuilder.append(QUERY_INNER_NAC);
    }
    fromBuilder.append("INNER ").append(nac() ? QUERY_ERROR_JOIN_NAC : QUERY_ERROR_JOIN_INT);
    onError(fromBuilder);
    fromBuilder.append(QUERY_ERROR_CODE_JOIN);
    equalsIfPresent(FIELD_ERROR_CODE, filter.getErrorCode(), filter.isErrorCodeEq(), fromBuilder);
    equalsOrGreaterIfPresent(FIELD_FEOPERAC, filter.getFechaDesde());
    equalsOrLowerIfPresent(FIELD_FEOPERAC, filter.getFechaHasta());
    where();
    groupByBuilder.append(FIELD_FEOPERAC);
  }

  private void buildSelectTodosTitulares() {
    if (nac()) {
      selectBuilder.append(QUERY_SELECT_TODOS_TITULARES_NAC);
    }
    else {
      selectBuilder.append(QUERY_SELECT_TODOS_TITULARES_INT);
    }

    fromBuilder.append(QUERY_FROM);
    if (nac())
      fromBuilder.append(QUERY_INNER_NAC);
    leftJoins();
    splitMainDateWithFounded();
    where();
    whereBuilder.append(QUERY_WHERE_END_TODOS_TITULARES);
    orderByBuilder.append(FIELD_FEOPERAC).append(" DESC");
  }

  private void buildSelectSinTitular() {
    if (nac()) {
      selectBuilder.append(QUERY_SELECT_SIN_TITULAR_NAC);
    }
    else {
      selectBuilder.append(QUERY_SELECT_SIN_TITULAR_INT);
    }

    fromBuilder.append(QUERY_FROM);
    if (nac()) {
      fromBuilder.append(QUERY_INNER_NAC);
    }
    fromBuilder.append("LEFT ").append(nac() ? QUERY_AFI_JOIN_NAC : QUERY_AFI_JOIN_INT);
    fromBuilder.append("LEFT ").append(nac() ? QUERY_ERROR_JOIN_NAC : QUERY_ERROR_JOIN_INT);
    splitMainDateWithFounded();
    where();
    whereBuilder.append(QUERY_WHERE_END_SIN_TITULAR);
    orderByBuilder.append(FIELD_FEOPERAC).append(" DESC");
  }

  private void buildSelectSinError() {
    if (nac()) {
      selectBuilder.append(QUERY_SELECT_SIN_ERROR_NAC);
    }
    else {
      selectBuilder.append(QUERY_SELECT_SIN_ERROR_INT);
    }

    fromBuilder.append(QUERY_FROM);
    if (nac()) {
      fromBuilder.append(QUERY_INNER_NAC);
    }
    fromBuilder.append("INNER ").append(nac() ? QUERY_AFI_JOIN_NAC : QUERY_AFI_JOIN_INT);
    onAfi();
    splitMainDateWithFounded();
    where();
    orderByBuilder.append(FIELD_FEOPERAC).append(" DESC");
  }

  private void buildSelectConError() {
    if (nac()) {
      selectBuilder.append(filter.isExportation() ? QUERY_SELECT_CON_ERROR_EXPORT_NAC : QUERY_SELECT_CON_ERROR_NAC);
    }
    else {
      selectBuilder.append(filter.isExportation() ? QUERY_SELECT_CON_ERROR_EXPORT_INT : QUERY_SELECT_CON_ERROR_INT);
    }
    fromBuilder.append(QUERY_FROM);
    if (nac())
      fromBuilder.append(QUERY_INNER_NAC);
    fromBuilder.append("INNER ").append(nac() ? QUERY_ERROR_JOIN_NAC : QUERY_ERROR_JOIN_INT);
    onError(fromBuilder);
    fromBuilder.append(QUERY_ERROR_CODE_JOIN);
    equalsIfPresent(FIELD_ERROR_CODE, filter.getErrorCode(), filter.isErrorCodeEq(), fromBuilder);
    splitMainDateWithFounded();
    where();
    if (filter.getErrorCode() != null) {
      whereBuilder.append("AND ").append(FIELD_ERROR_CODE).append(" IS NOT NULL ");
    }
    orderByBuilder.append(FIELD_FEOPERAC).append(" DESC");
  }

  private void buildSelectErrorSinOrden() {
    selectBuilder.append(QUERY_SELECT_ERROR_SIN_ORDEN);
    fromBuilder.append(QUERY_FROM_ERROR).append(QUERY_ERROR_CODE_JOIN);
    equalsIfPresent(FIELD_ERROR_CODE, filter.getErrorCode(), filter.isErrorCodeEq(), fromBuilder);
    whereBuilder.append("AND (E.NUORDEN IS NULL OR E.NUCNFLIQ IS NULL) ");
    equalsIfPresent("NUREFORD", filter.getRefCliente(), filter.isRefClienteEq());
    onError(whereBuilder);
  }

  private void leftJoins() {
    // AFI
    fromBuilder.append("LEFT ").append(nac() ? QUERY_AFI_JOIN_NAC : QUERY_AFI_JOIN_INT);
    onAfi();
    // ERRORS
    fromBuilder.append("LEFT ").append(nac() ? QUERY_ERROR_JOIN_NAC : QUERY_ERROR_JOIN_INT);
    onError(fromBuilder);
  }

  private void onAfi() {
    inList(FIELD_AFI_NBCLIENT, filter.getNbclient(), filter.isNbclientEq(), fromBuilder);
    inList(FIELD_AFI_NBCLIENT1, filter.getNbclient1(), filter.isNbclient1Eq(), fromBuilder);
    inList(FIELD_AFI_NBCLIENT2, filter.getNbclient2(), filter.isNbclient2Eq(), fromBuilder);
    inList(FIELD_AFI_NUDNICIF, filter.getNudnicif(), filter.isNudnicifEq(), fromBuilder);
  }

  private void onError(StringBuilder builder) {
    inList(FIELD_ERROR_NBCLIENT, filter.getNbclient(), filter.isNbclientEq(), builder);
    inList(FIELD_ERROR_NBCLIENT1, filter.getNbclient1(), filter.isNbclient1Eq(), builder);
    inList(FIELD_ERROR_NBCLIENT2, filter.getNbclient2(), filter.isNbclient2Eq(), builder);
    inList(FIELD_ERROR_NUDNICIF, filter.getNudnicif(), filter.isNudnicifEq(), builder);
  }

  private void where() {
    final Character sentido;

    sentido = filter.getSentido();
    if (!nac()) {
      whereBuilder.append("AND (A.CDESTADOASIG IS NULL OR A.CDESTADOASIG = 0 ) ");
    }
    equals(FIELD_MERCADO);
    if (sentido != null) {
      equalsIfPresent(FIELD_SENTIDO, Character.toString(sentido));
    }
    equalsIfPresent(FIELD_REF_CLIENTE, filter.getRefCliente(), filter.isRefClienteEq());
    equalsIfPresent(FIELD_BOOKING, filter.getNbooking(), filter.isNbookingEq());
    inList(FIELD_CDISIN, filter.getIsin(), filter.isIsinEq());
    inList(FIELD_CDALIAS, filter.getAlias(), filter.isAliasEq());
    whereBuilder.append(nac() ? QUERY_WHERE_END_NAC : QUERY_WHERE_END_INT);
  }

  private boolean nac() {
    return filter.getMercadoNacInt() == 'N';
  }

  @Override
  public boolean isIsoFormat() {
    return false;
  }

  @Override
  public String getSingleFilter() {
    return null;
  }

  @Override
  public List<DynamicColumn> getColumns() {
    return null;
  }

}
