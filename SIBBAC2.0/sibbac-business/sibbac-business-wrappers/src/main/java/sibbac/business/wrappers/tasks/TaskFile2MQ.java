package sibbac.business.wrappers.tasks;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.common.fileWriter.MQManager;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_COMUNICACIONES.NAME,
           name = Task.GROUP_COMUNICACIONES.JOB_MQ_WRITER,
           interval = 1,
           delay = 0,
           intervalUnit = IntervalUnit.MINUTE)
public class TaskFile2MQ extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private MQManager writer;

  @Value("${MQ.partenon.desgloses.out.name}")
  String queueConnectionFactoryName;

  @Value("${MQ.partenon.desgloses.out.qmgrname}")
  String outQueueName;

  @Value("${MQ.partenon.desgloses.out.folder}")
  String folderName;

  @Value("${MQ.partenon.desgloses.out.file}")
  String fileName;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    LOG.debug("[TaskFile2MQ :: execute] Init");

    try {
      Path workPath = Paths.get(folderName);
      try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(workPath, "*" + fileName + "*");) {
        if (directoryStream != null) {
          for (Path path : directoryStream) {
            if (Files.exists(path)) {
              LOG.debug("[TaskFile2MQ :: execute] enviando fichero {}", path.toString());
              try {
                writer.writeIntoQueue("jms/" + queueConnectionFactoryName, "jms/" + outQueueName, path.toFile()
                                                                                                      .getName(),
                                      "file://" + folderName);
              } catch (SIBBACBusinessException e) {
                throw new SIBBACBusinessException("Incidencia con el fichero: " + path.toString(), e);
              } catch (Exception e) {
                LOG.warn("[TaskFile2MQ :: execute] ERROR: " + e.getMessage());
                throw new SIBBACBusinessException("Incidencia con el fichero: " + path.toString(), e);

              }
            }
          }
        } else {
          LOG.debug("[TaskFile2MQ :: execute] no encontrados ficheros que enviar por MQ");
        }
      }
    } catch (Exception e) {
      throw new SIBBACBusinessException("Incidencia recuperarndo los ficheros para el envio MQ", e);
    }

    LOG.debug("[TaskFile2MQ :: execute] Fin");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_COMUNICACIONES_MQ_OUT;
  }

}
