package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.common.fileReader.SibbacFileReader;
import sibbac.business.wrappers.common.fileReader.ValidationHelper;
import sibbac.business.wrappers.database.bo.Tmct0AfiErrorBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0paisesBo;
import sibbac.business.wrappers.database.bo.Tmct0provinciasBo;
import sibbac.business.wrappers.database.bo.partenonReader.TitularesExternalValidator;
import sibbac.business.wrappers.database.desglose.bo.CuentaValoresValidateHelper;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean.TipoPersona;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BancaPrivadaFileReader extends SibbacFileReader<HBBNRecordBean> {

  private static final Logger LOG = LoggerFactory.getLogger(BancaPrivadaFileReader.class);

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0AfiErrorBo afiErrorBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private TitularesExternalValidator externalValidator;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0provinciasBo tmct0provinciasBo;

  @Autowired
  private Tmct0paisesBo paisesBo;

  private List<PartenonRecordBean> partenonBeanList;

  private DesgloseRecordBean desgloseRecordBean;

  private Map<String, Tmct0paises> isosPaises;

  private Map<String, Tmct0paises> isosPaisesHacienda;

  private Map<String, List<String>> nombresCiudadPorDistrito;

  private Estado estado;

  private Tmct0ord ord;

  private String nuorden;

  @Autowired
  public BancaPrivadaFileReader(@Value("${sibbac.wrappers.BancaPrivadaFile}") String fileName) {
    super(fileName);
    final FixedLengthTokenizer lineTokenizer;
    final DefaultLineMapper<HBBNRecordBean> lMapper;

    lineTokenizer = new FixedLengthTokenizer();
    lineTokenizer.setNames(BancaPrivadaFieldSetMapper.BancaPrivadaFields.getFieldNames());
    lineTokenizer.setColumns(this.convertLengthsIntoRanges(BancaPrivadaFieldSetMapper.BancaPrivadaFields
        .getBancaPrivadaLengths()));
    lineTokenizer.setStrict(false);
    lMapper = new DefaultLineMapper<>();
    lMapper.setLineTokenizer(lineTokenizer);
    lMapper.setFieldSetMapper(new BancaPrivadaFieldSetMapper());
    lineMapper = lMapper;
    estado = new Inicial();
    LOG.trace("[BancaPrivadaFileReader :: Constructor] FileName: " + fileName);
  }

  @Override
  protected boolean processLine(final Map<String, List<HBBNRecordBean>> group, final HBBNRecordBean bean,
      final int lineNumber) throws SIBBACBusinessException {
    LOG.trace("[processLine] Inicio...");
    try {
      estado = estado.procesaRegistro(bean);
    }
    catch (IllegalStateException se) {
      LOG.error("[processLine] Error de estado al leer fichero: {}", se.getMessage());
      throw new SIBBACBusinessException("Error en lectura fichero Banif: línea fuera de secuencia", se);
    }
    LOG.trace("[processLine] Fin");
    return false;
  }

  @Override
  protected boolean preProcessFile() throws SIBBACBusinessException {
    isosPaises = paisesBo.findMapAllActivosByIsonnum();
    isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();
    nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();
    return super.preProcessFile();
  }

  @Override
  protected boolean postProcessFile(Map<String, List<HBBNRecordBean>> group) throws SIBBACBusinessException {
    return false;
  }

  @Override
  protected void initBlockToProcess() {
  }

  @Override
  protected void finishBlockToProcess() {
  }

  private void fillHbb4Desglose(HBB4DesgloseRecordBean hbb4Bean) {
    LOG.debug("[fillHbb4Desglose] Inicio...");
    partenonBeanList = new ArrayList<>();
    desgloseRecordBean = new DesgloseRecordBean();
    desgloseRecordBean.setNumeroOrden(nuorden);
    desgloseRecordBean.setNumeroReferenciaOrden(hbb4Bean.getRefExterna());
    desgloseRecordBean.setCodEmprl(hbb4Bean.getEntidadLiquidadora());
    desgloseRecordBean.setCodigoValorISO(hbb4Bean.getIsin());
    desgloseRecordBean.setFechaEjecucion(hbb4Bean.getFechaCuadre());
    desgloseRecordBean.setTitulosEjecutados(hbb4Bean.getTitulos());
    desgloseRecordBean.setCambioOperacion(hbb4Bean.getPrecio());
    LOG.debug("[fillHbb4Desglose] Fin, creado objeto desgloseRecordBean.");
  }

  private void fillHbb4Titular(HBB4TitularRecordBean hbb4Bean) {
    final PartenonRecordBean partenonBean;
    final String nombreCompleto;

    LOG.debug("[fillHbb4Titular] Inicio...");
    partenonBean = new PartenonRecordBean();
    partenonBean.setTipoRegistro(hbb4Bean.getTipoRegistro());

    if (TipoPersona.JURIDICA.getTipoPersona() == hbb4Bean.getTipoPersona()) {
      nombreCompleto = String.format("%s %s %s", hbb4Bean.getNombre().trim(), hbb4Bean.getApellido1().trim(), hbb4Bean
          .getApellido2().trim());
      partenonBean.setNomapell(nombreCompleto.trim());
      partenonBean.setNbclient("");
      partenonBean.setNbclient1(nombreCompleto.trim());
      partenonBean.setNbclient2("");
    }
    else {
      partenonBean.setNbclient(hbb4Bean.getNombre().trim());
      partenonBean.setNbclient1(hbb4Bean.getApellido1().trim());
      partenonBean.setNbclient2(hbb4Bean.getApellido2().trim());
      nombreCompleto = String.format("%s * %s * %s", hbb4Bean.getNombre().trim(), hbb4Bean.getApellido1().trim(),
          hbb4Bean.getApellido2().trim());
      partenonBean.setNomapell(nombreCompleto);
    }
    partenonBean.setTipoPersona(hbb4Bean.getTipoPersona());
    partenonBean.setNifbic(hbb4Bean.getDocumentoCliente());
    partenonBean.setTiptitu(hbb4Bean.getTitularidad());
    if (StringUtils.isNotBlank(hbb4Bean.getCodIfDepositante())) {
      String[] split = hbb4Bean.getCodIfDepositante().split(" ");
      if (split.length > 1) {
        String subCcv = split[0];
        if (subCcv.length() == 12) {
          String preCcv = subCcv.substring(0, 4);
          String postCcv = subCcv.substring(5).trim();
          String ccv = String.format("%s%s%s400%s", desgloseRecordBean.getCodEmprl(), preCcv, "**", postCcv);
          if (ccv.trim().length() == 20) {
            partenonBean.setCcv(CuentaValoresValidateHelper.crearCcvBanif(ccv));
          }
        }
      }
    }

    partenonBean.setTipoDocumento(hbb4Bean.getTipoDocumento());
    partenonBean.setIndNac(hbb4Bean.getIndNac());
    partenonBean.setNacionalidad(hbb4Bean.getNacionalidad().trim());

    partenonBean.setParticip(hbb4Bean.getParticip());
    partenonBean.setNumOrden(hbb4Bean.getRefExterna());
    // partenonBean.setNumOrden(hbb4Bean.getReferenciaCentral());
    partenonBean.setFechaContratacion(hbb4Bean.getFechaCuadre());
    partenonBeanList.add(partenonBean);
    partenonBean.setIdSenc(new BigDecimal(partenonBeanList.size()));
    LOG.debug("[fillHbb4Titular] Fin. partenonBeanList con {} elementos", partenonBeanList.size());
  }

  private void fillHbb5(HBB5RecordBean hbb5Bean) {
    final String domicilio;

    LOG.debug("[fillHbb5] Inicio...");
    domicilio = new StringBuilder(hbb5Bean.getClaveCalle()).append(' ').append(hbb5Bean.getDireccion()).toString();
    for (PartenonRecordBean partenonBean : partenonBeanList) {
      partenonBean.setDomicili(domicilio);
      partenonBean.setPoblacion(hbb5Bean.getMunicipio());
      partenonBean.setProvincia(hbb5Bean.getProvincia());
      partenonBean.setDistrito(hbb5Bean.getCodigoPostal().toString());
      if (hbb5Bean.getPais() != null) {
        String paisResidencia = StringUtils.leftPad(hbb5Bean.getPais().setScale(0, RoundingMode.HALF_UP).toString(), 3,
            "0");
        Tmct0paises pais = isosPaises.get(paisResidencia);
        if (pais != null && pais.getCdhacienda() != null && !pais.getCdhacienda().trim().isEmpty()) {
          partenonBean.setPaisResidencia(pais.getCdhacienda().trim());
        }
        else {
          partenonBean.setPaisResidencia(paisResidencia);
        }
      }
      else {
        partenonBean.setPaisResidencia("");
      }
      partenonBean.setNumOrden(hbb5Bean.getRefExterna());
    }
    LOG.debug("[fillHbb5] Fin. Se ha agregado info de domicilio a {} registros partenon", partenonBeanList.size());
  }

  private void cargaOrden(String nuorden) {
    LOG.debug("[cargaOrden] Se retiene el nuorden {}", nuorden);
    this.nuorden = nuorden;
    ord = ordBo.findById(nuorden);
    if (ord == null)
      LOG.debug("[cargaOrden] Numero de orden no encontrado: {}", nuorden);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private void guardaDesglose() {
    final String nureford;
    final Date feoperac;
    final Map<PartenonRecordBean, List<String>> errorsByBean;
    final List<Date> festivosList;
    final List<ControlTitularesDTO> alcs, alos;
    final short cdbloque;
    String erroresNumorden;
    boolean hasErrors, nacional;

    LOG.debug("[guardaDesglose] Inicio...");
    festivosList = cfgBo.findHolidaysBolsa();
    errorsByBean = new HashMap<>();
    // Se decide si el bloque ira a AFI o a AFI_ERROR
    hasErrors = false;
    nacional = true;
    // Se comprueban los errores del ord
    nureford = partenonBeanList.get(0).getNumOrden();
    feoperac = partenonBeanList.get(0).getFechaContratacion();
    alcs = new ArrayList<>();
    alos = new ArrayList<>();
    erroresNumorden = null;
    try {
      LOG.trace("[guardaDesglose] Comprobando si ord es nacional o extranjero: {}", nureford);
      if (StringUtils.isNotBlank(nuorden)) {
        alos.addAll(aloBo.findControlTitularesByNuordenAndRefbanif(nuorden, nureford));
      }
      else if (feoperac != null) {
        alos.addAll(aloBo.findControlTitularesByFeoperacAndRefbanif(feoperac, nureford));
        if (CollectionUtils.isEmpty(alos)) {
          LOG.warn("[guardaDesglose] no encontrado alo's con fecha: {} y refbanif: {}", feoperac, nureford);
          alos.addAll(aloBo.findControlTitularesByRefbanif(nureford));
        }
      }
      if (ord != null && !ord.getCdclsmdo().equals('N')) {
        nacional = false;
        LOG.debug("[guardaDesglose] Comprobando alos para nureford extranjero: {}. Obtenidos {} alos", nureford,
            alos.size());
        if (alos.size() == 0) {
          erroresNumorden = ValidationHelper.CodError.ALOS.text;
          hasErrors = true;
        }
      }
      else {
        LOG.debug("[guardaDesglose] Comprobando alcs para nureford nacional: {}", nureford);
        for (ControlTitularesDTO alo : alos) {
          alcs.addAll(alcBo.findControlTitularesByAlloId(alo.getAlloId()));
        }
        LOG.debug("[guardaDesglose] Obtenidos {} alcs para nureford: {}", alcs.size(), nureford);
        if (alcs.size() == 0) {
          erroresNumorden = ValidationHelper.CodError.NOALC.text;
          hasErrors = true;
        }
      }
    }
    catch (IncorrectResultSizeDataAccessException e) {
      erroresNumorden = ValidationHelper.CodError.ORD.text;
      hasErrors = true;
    }

    externalValidator.validateAll(erroresNumorden, partenonBeanList, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito, errorsByBean);

    // Arbol de decision para guardar el titular
    cdbloque = (nureford != null) ? afiBo.cleanAfisAndErrorsGetCdBloque(nureford) : 0;
    if (hasErrors) {
      if (nacional) {
        afiErrorBo.guardarBloqueNacionalConErrores(alcs, errorsByBean, partenonBeanList, null);
      }
      else {
        afiErrorBo.guardarBloqueExtranjeroConErrores(alos, errorsByBean, partenonBeanList, null);
      }
    }
    else {
      if (nacional) {
        afiBo.guardarBloqueNacionalSinErrores(alcs, cdbloque, partenonBeanList, festivosList, false, "BANIF");
      }
      else {
        afiBo.guardarBloqueExtranjeroSinErrores(alos, cdbloque, partenonBeanList);
      }
    }
    LOG.debug("[guardaDesglose] Fin");
  }

  private abstract class Estado {

    Estado() {
      LOG.debug("Nuevo estado: {}", getClass().getSimpleName());
    }

    abstract Estado procesaRegistro(HBBNRecordBean bean);

  }

  private class Inicial extends Estado {

    @Override
    Estado procesaRegistro(HBBNRecordBean bean) {
      if (!bean.getTipoRegistro().equals("HBB6")
          || ((HBB6RecordBean) bean).getIncicadorInicioFinal() != IndicadorInicioFinalHBB.INICIO)
        throw new IllegalStateException();
      return new EsperandoOrden();
    }

  }

  private class EsperandoOrden extends Estado {

    @Override
    Estado procesaRegistro(HBBNRecordBean bean) {
      final HBB7RecordBean rb;
      final String tipoRegistro;

      tipoRegistro = bean.getTipoRegistro();
      switch (tipoRegistro) {
        case "HBB7":
          rb = (HBB7RecordBean) bean;
          if (rb.getIndicadorInicioFinal() != IndicadorInicioFinalHBB.INICIO)
            throw new IllegalStateException("Se esperaba inicio de orden");
          cargaOrden(rb.getNuorden());
          return new EsperandoDesglose();
        case "HBB6":
          return new Final();
        default:
          throw new IllegalStateException("Se esperaba inicio HBB7 o final HBB6");
      }
    }

  }

  private class EsperandoDesglose extends Estado {

    @Override
    Estado procesaRegistro(HBBNRecordBean bean) {
      final String tipoRegistro;
      final HBB4DesgloseRecordBean rb4;
      final HBB7RecordBean rb7;

      tipoRegistro = bean.getTipoRegistro();
      switch (tipoRegistro) {
        case "HBB4":
          if (!(bean instanceof HBB4DesgloseRecordBean))
            throw new IllegalStateException("Se esperaba registro HBB4 Desglose");
          rb4 = (HBB4DesgloseRecordBean) bean;
          fillHbb4Desglose(rb4);
          return new EsperandoTitular();
        case "HBB7":
          if (!(bean instanceof HBB7RecordBean))
            throw new IllegalStateException("Se esperaba registro HBB7");
          rb7 = (HBB7RecordBean) bean;
          if (rb7.getIndicadorInicioFinal() != IndicadorInicioFinalHBB.FINAL)
            throw new IllegalStateException("Se esperaba fin de orden");
          ord = null;
          nuorden = null;
          return new Final();
        default:
          throw new IllegalStateException();
      }
    }
  }

  private class EsperandoTitular extends Estado {

    @Override
    Estado procesaRegistro(HBBNRecordBean bean) {
      final HBB4TitularRecordBean rb4;
      final HBB5RecordBean rb5;
      final String tipoRegistro;

      tipoRegistro = bean.getTipoRegistro();
      switch (tipoRegistro) {
        case "HBB4":
          if (!(bean instanceof HBB4TitularRecordBean))
            throw new IllegalStateException("Se esperaba registro HBB4 Titular");
          rb4 = (HBB4TitularRecordBean) bean;
          fillHbb4Titular(rb4);
          return this;
        case "HBB5":
          if (!(bean instanceof HBB5RecordBean))
            throw new IllegalStateException("Se esperaba registro HBB5");
          rb5 = (HBB5RecordBean) bean;
          fillHbb5(rb5);
          guardaDesglose();
          return new EsperandoDesglose();
        default:
          throw new IllegalStateException("Se espera registro HBB4 o HBB5");
      }
    }

  }

  private class Final extends Estado {

    @Override
    Estado procesaRegistro(HBBNRecordBean bean) {
      final String idMensaje;
      final HBB7RecordBean rb7;
      final HBB6RecordBean rb6;
      idMensaje = bean.getTipoRegistro();
      switch (idMensaje) {
        case "HBB7":
          if (!(bean instanceof HBB7RecordBean))
            throw new IllegalStateException("Se esperaba registro HBB7");
          rb7 = (HBB7RecordBean) bean;
          if (rb7.getIndicadorInicioFinal() == IndicadorInicioFinalHBB.INICIO) {
            cargaOrden(rb7.getNuorden());
            return new EsperandoDesglose();
          }
          else {
            nuorden = null;
            ord = null;
            return this;
          }
        case "HBB6":
          if (!(bean instanceof HBB6RecordBean))
            throw new IllegalStateException("Se esperaba registro HBB6");
          rb6 = (HBB6RecordBean) bean;
          if (rb6.getIncicadorInicioFinal() == IndicadorInicioFinalHBB.INICIO) {
            throw new IllegalStateException("Se esperaba linea HBB6 final");
          }
          else {
            return new Inicial();
          }
        default:
          throw new IllegalStateException("Se esperaba linea HBB6 final");
      }
    }

  }

}
