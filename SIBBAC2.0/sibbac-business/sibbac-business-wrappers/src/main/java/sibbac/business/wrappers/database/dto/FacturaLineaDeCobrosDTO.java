package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;
import java.util.Date;


public class FacturaLineaDeCobrosDTO {

	// linea de cobro
	private Long		idLineaDeCobro;
	private Long		idCobros;
	private BigDecimal	importe;
	private BigDecimal	importeAplicado;

	// factura
	private Long		idFactura;
	private Long		numeroFactura;
	private Date		fechaFactura;

	public FacturaLineaDeCobrosDTO() {

	}

	public FacturaLineaDeCobrosDTO( Long idLineaDeCobro, Long idCobros, BigDecimal importe, BigDecimal importeAplicado, Long idFactura,
			Long numeroFactura, Date fechaFactura ) {

		this.idLineaDeCobro = idLineaDeCobro;
		this.idCobros = idCobros;
		this.importe = importe;
		this.importeAplicado = importeAplicado;
		this.idFactura = idFactura;
		this.numeroFactura = numeroFactura;
		this.fechaFactura = fechaFactura;
	}

	public Long getIdLineaDeCobro() {
		return idLineaDeCobro;
	}

	public void setIdLineaDeCobro( Long idLineaDeCobro ) {
		this.idLineaDeCobro = idLineaDeCobro;
	}

	public Long getIdCobros() {
		return idCobros;
	}

	public void setIdCobros( Long idCobros ) {
		this.idCobros = idCobros;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	public BigDecimal getImporteAplicado() {
		return importeAplicado;
	}

	public void setImporteAplicado( BigDecimal importeAplicado ) {
		this.importeAplicado = importeAplicado;
	}

	public Long getIdFactura() {
		return idFactura;
	}

	public void setIdFactura( Long idFactura ) {
		this.idFactura = idFactura;
	}

	public Long getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura( Long numeroFactura ) {
		this.numeroFactura = numeroFactura;
	}

	public Date getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura( Date fechaFactura ) {
		this.fechaFactura = fechaFactura;
	}

}
