package sibbac.business.wrappers.database.dao;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;


@Repository
public class Tmct0operacioncdeccDaoImpl {

	@PersistenceContext
	private EntityManager	em;

	@Autowired
	private Tmct0ValoresBo	valoresBo;

	@SuppressWarnings( "unchecked" )
	public List< Object[] > getTitulosCuentaDiariaConFiltro( List< String > operaciones, HashMap< String, Object > filtros ) {

		StringBuilder sb = new StringBuilder( "select sum(o.nutitulos) as titulostotales, o.cdisin, " )
				.append( " o.fcontratacion, o.fliqteorica, o.cdsentido ,  o.imprecio, " )
				.append( " o.cdcamara, o.cdmiembromkt,  min(o.nuordenmkt), min(o.nuexemkt), " )
				.append( " min(o.fordenmkt) from Tmct0operacioncdecc o where o.cdoperacionecc not in :operaciones" );

		if ( filtros.get( "isin" ) != null )
			sb.append( " and o.cdisin = :isin" );

		if ( filtros.get( "camara" ) != null )
			sb.append( " and o.cdcamara = :camara" );

		if ( filtros.get( "sentido" ) != null )
			sb.append( " and o.cdsentido = :sentido" );

		// fecha contratacion
		if ( filtros.get( "fcontratacionDe" ) != null && filtros.get( "fcontratacionA" ) != null ) {
			sb.append( " AND o.fcontratacion BETWEEN :fcontratacionDe AND :fcontratacionA" );
		} else if ( filtros.get( "fcontratacionDe" ) != null ) {
			sb.append( " AND o.fcontratacion >=:fcontratacionDe " );
		} else if ( filtros.get( "fcontratacionA" ) != null ) {
			sb.append( " AND o.fcontratacion <=:fcontratacionA " );
		}
		// fecha liquidacion
		if ( filtros.get( "fliquidacionDe" ) != null && filtros.get( "fliquidacionA" ) != null ) {
			sb.append( " AND o.fliqteorica BETWEEN :fliquidacionDe AND :fliquidacionA" );
		} else if ( filtros.get( "fliquidacionDe" ) != null ) {
			sb.append( " AND o.fliqteorica >=:fliquidacionDe " );
		} else if ( filtros.get( "fliquidacionA" ) != null ) {
			sb.append( " AND o.fliqteorica <=:fliquidacionA " );
		}

		sb.append( " group by o.cdisin,  o.cdsentido , " ).append( " o.cdcamara" );

		javax.persistence.Query query = em.createQuery( sb.toString() );

		query.setParameter( "operaciones", operaciones );
		if ( filtros.get( "isin" ) != null ) {
			String isin = filtros.get( "isin" ).toString();
			query.setParameter( "isin", isin );
		}

		if ( filtros.get( "camara" ) != null ) {
			String camara = filtros.get( "camara" ).toString();
			query.setParameter( "camara", camara );
		}

		if ( filtros.get( "sentido" ) != null ) {
			Character sentido = filtros.get( "sentido" ).toString().charAt( 0 );
			query.setParameter( "sentido", sentido );
		}

		if ( filtros.get( "fcontratacionDe" ) != null ) {
			Date fcontratacionDe = ( Date ) filtros.get( "fcontratacionDe" );
			query.setParameter( "fcontratacionDe", fcontratacionDe );
		}

		if ( filtros.get( "fcontratacionA" ) != null ) {
			Date fcontratacionA = ( Date ) filtros.get( "fcontratacionA" );
			query.setParameter( "fcontratacionA", fcontratacionA );
		}

		if ( filtros.get( "fliquidacionDe" ) != null ) {
			Date fliquidacionDe = ( Date ) filtros.get( "fliquidacionDe" );
			query.setParameter( "fliquidacionDe", fliquidacionDe );
		}

		if ( filtros.get( "fliquidacionA" ) != null ) {
			Date fliquidacionA = ( Date ) filtros.get( "fliquidacionA" );
			query.setParameter( "fliquidacionA", fliquidacionA );
		}

		//
		// if(filtros.get( "bolsa" ) !=null ){
		// String bolsa = filtros.get( "bolsa" ).toString();
		// query.setParameter( "bolsa", bolsa );
		// }
		//
		// if(filtros.get( "precio" ) !=null ){
		// BigDecimal precio = (BigDecimal)filtros.get( "precio" );
		// query.setParameter( "precio", precio );
		// }
		return ( List< Object[] > ) query.getResultList();
	}

	@SuppressWarnings( "unchecked" )
	public List< Object[] > getTitulosCuentaDiariaConFiltrosYCDOperacion( HashMap< String, Serializable > filtros, Date ayer, Date hoy ) {
		StringBuilder sb = new StringBuilder( "select sum(o.nutitulos) as titulostotales, sum(o.imefectivo) as imefectivo, o.cdisin, " )
				.append( " o.fcontratacion, o.fliqteorica, o.cdsentido ,  o.imprecio, " )
				.append( " o.cdcamara, min(o.cdmiembromkt),  min(o.nuordenmkt), min(o.nuexemkt), " )
				.append( " min(o.fordenmkt), o.cdoperacionecc,  o.cdisin " ).append( "from Tmct0operacioncdecc o " )
				.append( "where o.fcontratacion>=:ayer AND o.fcontratacion<=:hoy AND o.imtitulosdisponibles>0 AND o.tmct0CuentasDeCompensacion.cdCodigo='00D' " );
		if ( filtros.get( "cdisin" ) != null )
			sb.append( " and o.cdisin = :isin" );

		if ( filtros.get( "cdcamara" ) != null )
			sb.append( " and o.cdcamara = :camara" );

		if ( filtros.get( "cdsentido" ) != null )
			sb.append( " and o.cdsentido = :sentido" );

		// fecha contratacion
		if ( filtros.get( "fcontratacionDe" ) != null && filtros.get( "fcontratacionA" ) != null ) {
			sb.append( " AND o.fcontratacion BETWEEN :fcontratacionDe AND :fcontratacionA" );
		} else if ( filtros.get( "fcontratacionDe" ) != null ) {
			sb.append( " AND o.fcontratacion >=:fcontratacionDe " );
		} else if ( filtros.get( "fcontratacionA" ) != null ) {
			sb.append( " AND o.fcontratacion <=:fcontratacionA " );
		}
		// fecha liquidacion
		if ( filtros.get( "fliquidacionDe" ) != null && filtros.get( "fliquidacionA" ) != null ) {
			sb.append( " AND o.fliqteorica BETWEEN :fliquidacionDe AND :fliquidacionA" );
		} else if ( filtros.get( "fliquidacionDe" ) != null ) {
			sb.append( " AND o.fliqteorica >=:fliquidacionDe " );
		} else if ( filtros.get( "fliquidacionA" ) != null ) {
			sb.append( " AND o.fliqteorica <=:fliquidacionA " );
		}

		sb.append( " group by o.cdisin,  o.cdsentido , o.cdcamara, o.fcontratacion, o.fliqteorica, o.cdsentido ,  o.imprecio, " ).append(
				" o.cdcamara, o.cdoperacionecc " );

		javax.persistence.Query query = em.createQuery( sb.toString() );

		if ( filtros.get( "cdisin" ) != null ) {
			String isin = filtros.get( "cdisin" ).toString();
			query.setParameter( "isin", isin );
		}

		if ( filtros.get( "cdcamara" ) != null ) {
			String camara = filtros.get( "cdcamara" ).toString();
			query.setParameter( "camara", camara );
		}

		if ( filtros.get( "cdsentido" ) != null ) {
			Character sentido = filtros.get( "cdsentido" ).toString().charAt( 0 );
			query.setParameter( "sentido", sentido );
		}

		if ( filtros.get( "fcontratacionDe" ) != null ) {
			Date fcontratacionDe = ( Date ) filtros.get( "fcontratacionDe" );
			query.setParameter( "fcontratacionDe", fcontratacionDe );
		}

		if ( filtros.get( "fcontratacionA" ) != null ) {
			Date fcontratacionA = ( Date ) filtros.get( "fcontratacionA" );
			query.setParameter( "fcontratacionA", fcontratacionA );
		}

		if ( filtros.get( "fliquidacionDe" ) != null ) {
			Date fliquidacionDe = ( Date ) filtros.get( "fliquidacionDe" );
			query.setParameter( "fliquidacionDe", fliquidacionDe );
		}

		if ( filtros.get( "fliquidacionA" ) != null ) {
			Date fliquidacionA = ( Date ) filtros.get( "fliquidacionA" );
			query.setParameter( "fliquidacionA", fliquidacionA );
		}

		query.setParameter( "ayer", ayer );
		query.setParameter( "hoy", hoy );
		//
		// if(filtros.get( "bolsa" ) !=null ){
		// String bolsa = filtros.get( "bolsa" ).toString();
		// query.setParameter( "bolsa", bolsa );
		// }
		//
		// if(filtros.get( "precio" ) !=null ){
		// BigDecimal precio = (BigDecimal)filtros.get( "precio" );
		// query.setParameter( "precio", precio );
		// }
		List< Object[] > results = ( List< Object[] > ) query.getResultList();
		// //Metemos las descripciones de isin
		// for(Object[] result : results){
		// if(result[result.length-1] != null){
		// List<Tmct0Valores> listaVal = valoresBo.findByCodigoDeValorOrderByFechaEmisionDesc( result[result.length-1].toString() );
		// if(listaVal!=null && listaVal.size()>0){
		// result[result.length-1] = listaVal.get( 0 ).getDescripcion();
		// }
		// }
		// }
		return results;
	}
}
