package sibbac.business.wrappers.database.data;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;

public class MQConnectionData {
    QueueSession queueSession = null;
    QueueConnection queueConnection = null;
    Queue queue = null;
    public QueueSession getQueueSession() {
        return queueSession;
    }
    public void setQueueSession(QueueSession queueSession) {
        this.queueSession = queueSession;
    }
    public QueueConnection getQueueConnection() {
        return queueConnection;
    }
    public void setQueueConnection(QueueConnection queueConnection) {
        this.queueConnection = queueConnection;
    }
    public Queue getQueue() {
        return queue;
    }
    public void setQueue(Queue queue) {
        this.queue = queue;
    }

}
