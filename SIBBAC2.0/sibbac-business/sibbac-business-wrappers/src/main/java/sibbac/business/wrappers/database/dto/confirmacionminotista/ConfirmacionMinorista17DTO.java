/**
 * 
 */
package sibbac.business.wrappers.database.dto.confirmacionminotista;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0bokId;

/**
 * @author xIS16630
 *
 */
public class ConfirmacionMinorista17DTO {

  private Tmct0bokId bookingId;
  private Date fealta;
  private Date hoalta;
  private Date fecontra;
  private Date fevalor;
  private BigDecimal imefeagr;
  private BigDecimal nutitliq;
  private BigDecimal imcbmerc;
  private String cdmoniso;
  private String plataformaNegociacion;
  private String codigoEcc;
  private BigDecimal imcomisn;
  private BigDecimal imcontr;
  private Integer clientPayCanonContr;

  public ConfirmacionMinorista17DTO(Tmct0bokId bookingId,
                                    Date fealta,
                                    Date hoalta,
                                    Date fecontra,
                                    Date fevalor,
                                    BigDecimal imefeagr,
                                    BigDecimal nutitliq,
                                    BigDecimal imcbmerc,
                                    String cdmoniso,
                                    String plataformaNegociacion,
                                    String codigoEcc,
                                    BigDecimal imcomisn,
                                    BigDecimal imcontr,
                                    Integer clientPayCanonContr) {
    super();
    this.bookingId = bookingId;
    this.fealta = fealta;
    this.hoalta = hoalta;
    this.fecontra = fecontra;
    this.fevalor = fevalor;
    this.imefeagr = imefeagr;
    this.nutitliq = nutitliq;
    this.imcbmerc = imcbmerc;
    this.cdmoniso = cdmoniso;
    this.plataformaNegociacion = plataformaNegociacion;
    this.codigoEcc = codigoEcc;
    this.imcomisn = imcomisn;
    this.imcontr = imcontr;
    this.clientPayCanonContr = clientPayCanonContr;
  }

  public Tmct0bokId getBookingId() {
    return bookingId;
  }

  public void setBookingId(Tmct0bokId nbooking) {
    this.bookingId = nbooking;
  }

  public Date getFealta() {
    return fealta;
  }

  public void setFealta(Date fealta) {
    this.fealta = fealta;
  }

  public Date getHoalta() {
    return hoalta;
  }

  public void setHoalta(Date hoalta) {
    this.hoalta = hoalta;
  }

  public Date getFecontra() {
    return fecontra;
  }

  public void setFecontra(Date fecontra) {
    this.fecontra = fecontra;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public BigDecimal getImefeagr() {
    return imefeagr;
  }

  public void setImefeagr(BigDecimal imefeagr) {
    this.imefeagr = imefeagr;
  }

  public BigDecimal getNutitliq() {
    return nutitliq;
  }

  public void setNutitliq(BigDecimal nutitliq) {
    this.nutitliq = nutitliq;
  }

  public BigDecimal getImcbmerc() {
    return imcbmerc;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  public String getCdmoniso() {
    return cdmoniso;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public String getPlataformaNegociacion() {
    return plataformaNegociacion;
  }

  public void setPlataformaNegociacion(String plataformaNegociacion) {
    this.plataformaNegociacion = plataformaNegociacion;
  }

  public String getCodigoEcc() {
    return codigoEcc;
  }

  public void setCodigoEcc(String codigoEcc) {
    this.codigoEcc = codigoEcc;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public BigDecimal getImcontr() {
    return imcontr;
  }

  public void setImcontr(BigDecimal imcontr) {
    this.imcontr = imcontr;
  }

  public Integer getClientPayCanonContr() {
    return clientPayCanonContr;
  }

  public void setClientPayCanonContr(Integer clientPayCanonContr) {
    this.clientPayCanonContr = clientPayCanonContr;
  }

  @Override
  public String toString() {
    return "ConfirmacionMinorista17DTO [nuorden=" + bookingId + ", fealta=" + fealta + ", hoalta=" + hoalta
           + ", fecontra=" + fecontra + ", fevalor=" + fevalor + ", imefeagr=" + imefeagr + ", nutitliq=" + nutitliq
           + ", imcbmerc=" + imcbmerc + ", cdmoniso=" + cdmoniso + ", plataformaNegociacion=" + plataformaNegociacion
           + ", codigoEcc=" + codigoEcc + ", imcomisn=" + imcomisn + ", imcontr=" + imcontr + ", clientPayCanonContr="
           + clientPayCanonContr + "]";
  }

}
