package sibbac.business.wrappers.database.desglose.bo;

import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class CuentaValoresValidateHelper {

  private static Logger LOG = Logger.getLogger(CuentaValoresValidateHelper.class);
  private static final String PREFIX = "";

  /**
   * @param banco
   * @param sucursal
   * @param numeroCuenta
   * @return digito de control de la cuenta
   */
  public static String calcularDC(String banco, String sucursal, String numeroCuenta) {
    LOG.debug(MessageFormat.format(PREFIX
                                       + "[INICIO] calculo DC ## banco : {0} ## sucursal : {1} ## numero cuenta : {2}",
                                   banco, sucursal, numeroCuenta));
    int dc1 = 0;

    dc1 = dc1 + Integer.parseInt(banco.substring(0, 1)) * 4;
    dc1 = dc1 + Integer.parseInt(banco.substring(1, 2)) * 8;
    dc1 = dc1 + Integer.parseInt(banco.substring(2, 3)) * 5;
    dc1 = dc1 + Integer.parseInt(banco.substring(3, 4)) * 10;
    dc1 = dc1 + Integer.parseInt(sucursal.substring(0, 1)) * 9;
    dc1 = dc1 + Integer.parseInt(sucursal.substring(1, 2)) * 7;
    dc1 = dc1 + Integer.parseInt(sucursal.substring(2, 3)) * 3;
    dc1 = dc1 + Integer.parseInt(sucursal.substring(3, 4)) * 6;

    dc1 = dc1 % 11;
    dc1 = 11 - dc1;
    if (dc1 == 10) {
      dc1 = 1;
    } else if (dc1 == 11) {
      dc1 = 0;
    }

    int dc2 = 0;

    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(0, 1)) * 5;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(1, 2)) * 4;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(2, 3)) * 3;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(3, 4)) * 2;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(4, 5)) * 7;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(5, 6)) * 6;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(6, 7)) * 5;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(7, 8)) * 4;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(8, 9)) * 3;
    dc2 = dc2 + Integer.parseInt(numeroCuenta.substring(9, 10)) * 2;

    dc2 = dc2 % 11;
    dc2 = 11 - dc2;
    if (dc2 == 10) {
      dc2 = 0;
    } else if (dc2 == 11) {
      dc2 = 1;
    }

    String dc = String.valueOf(dc1) + String.valueOf(dc2);

    LOG.debug(MessageFormat.format(PREFIX
                                           + "[FIN] calculo DC ## banco : {0} ## sucursal : {1} ## numero cuenta : {2} ## DC : {3}",
                                       banco, sucursal, numeroCuenta, dc));
    return dc;
  }

  public static boolean esCuentaBancaria(String ccv) {

    LOG.debug("Cuenta del Banco " + ccv);

    Pattern cuentaPattern = Pattern.compile("\\d{20}");
    Matcher m = cuentaPattern.matcher(ccv);
    if (m.matches()) {
      LOG.debug("cuenta cumple el patrón (20 dígitos)");

      String banco = ccv.substring(0, 8);
      LOG.debug("Banco (con 00) " + banco);

      int suma = Integer.parseInt(banco.substring(0, 1)) * 4;
      suma = suma + Integer.parseInt(banco.substring(1, 2)) * 8;
      suma = suma + Integer.parseInt(banco.substring(2, 3)) * 5;
      suma = suma + Integer.parseInt(banco.substring(3, 4)) * 10;
      suma = suma + Integer.parseInt(banco.substring(4, 5)) * 9;
      suma = suma + Integer.parseInt(banco.substring(5, 6)) * 7;
      suma = suma + Integer.parseInt(banco.substring(6, 7)) * 3;
      suma = suma + Integer.parseInt(banco.substring(7, 8)) * 6;

      int control = 11 - (suma % 11);
      LOG.debug("control banco después del modulo 11 " + control);
      if (control == 10)
        control = 1;
      else if (control == 11)
        control = 0;

      LOG.debug("control " + control);

      int controlBanco = Integer.parseInt(ccv.substring(8, 9));
      if (controlBanco != control)
        return false;
      LOG.debug("El control del banco está bien");
      String numeroCuenta = ccv.substring(10, 20);
      LOG.debug("cuenta " + numeroCuenta);

      suma = Integer.parseInt(numeroCuenta.substring(0, 1)) * 5;
      suma = suma + Integer.parseInt(numeroCuenta.substring(1, 2)) * 4;
      suma = suma + Integer.parseInt(numeroCuenta.substring(2, 3)) * 3;
      suma = suma + Integer.parseInt(numeroCuenta.substring(3, 4)) * 2;
      suma = suma + Integer.parseInt(numeroCuenta.substring(4, 5)) * 7;
      suma = suma + Integer.parseInt(numeroCuenta.substring(5, 6)) * 6;
      suma = suma + Integer.parseInt(numeroCuenta.substring(6, 7)) * 5;
      suma = suma + Integer.parseInt(numeroCuenta.substring(7, 8)) * 4;
      suma = suma + Integer.parseInt(numeroCuenta.substring(8, 9)) * 3;
      suma = suma + Integer.parseInt(numeroCuenta.substring(9, 10)) * 2;

      control = 11 - (suma % 11);
      LOG.debug("control cuenta después del modulo 11 " + control);
      if (control == 10)
        control = 0;
      else if (control == 11)
        control = 1;

      LOG.debug("control " + control);

      int controlcuenta = Integer.parseInt(ccv.substring(9, 10));
      if (controlcuenta != control)
        return false;
      else
        return true;

    } else
      return false;

  }

  public static String getBanco(String ccv) {
    LOG.debug(PREFIX + " getBanco() ccv : " + ccv);
    return ccv.substring(0, 4);
  }

  public static String getSucursal(String ccv) {
    LOG.debug(PREFIX + " getSucursal() ccv : " + ccv);
    return ccv.substring(4, 8);
  }

  public static String getDigitoControl(String ccv) {
    LOG.debug(PREFIX + " getDigitoControl() ccv : " + ccv);
    return ccv.substring(8, 10);
  }

  public static String getNumeroCuenta(String ccv) {
    LOG.debug(PREFIX + " getNumeroCuenta() ccv : " + ccv);
    return ccv.substring(10, 20);

  }

  public static String getNumeroCuentaRouting(String ccv) {
    LOG.debug(PREFIX + " getNumeroCuentaRouting() ccv : " + ccv);
    return ccv.substring(8, 15);
  }

  public static String crearCCV(String banco, String sucursal, String numeroCuenta) {
    String dc = calcularDC(banco, sucursal, numeroCuenta);
    String ccv = banco + sucursal + dc + numeroCuenta;

    LOG.debug(MessageFormat.format(PREFIX
                                           + "[FIN] calculo CCV ## banco : {0} ## sucursal : {1} ## numero cuenta : {2} ## DC : {3} ## CCV : {4}",
                                       banco, sucursal, numeroCuenta, dc, ccv));
    return ccv;
  }

  public static String crearCcvRoutingSanAndOpen(String ccvRoutingSanOpen) {
    LOG.debug(PREFIX + "[INICIO]crearCcvRoutingSanAndOpen() ## ccvRoutingSanOpen : " + ccvRoutingSanOpen);
    // 007301000000020
    String ccv = ccvRoutingSanOpen;
    if (ccvRoutingSanOpen != null && ccvRoutingSanOpen.trim().length() == 15) {
      String banco = getBanco(ccvRoutingSanOpen);
      String sucursal = getSucursal(ccvRoutingSanOpen);
      String numeroCuenta = "400" + getNumeroCuentaRouting(ccvRoutingSanOpen);
      ccv = crearCCV(banco, sucursal, numeroCuenta);
    }
    if (!esCuentaBancaria(ccv)) {
      LOG.debug(PREFIX + "[FIN]crearCcvRoutingSanAndOpen() ## NO ES CUENTA VALORES CCV : " + ccv);
    }
    LOG.debug(PREFIX + "[FIN]crearCcvRoutingSanAndOpen() ## CCV : " + ccv);
    return ccv;
  }

  public static String crearCcvBanif(String ccv) {
    LOG.debug(PREFIX + "[INICIO]crearCcvBanif() ## ccv : " + ccv);
    if (ccv != null && ccv.trim().length() == 20) {
      String banco = getBanco(ccv);
      String sucursal = getSucursal(ccv);
      String numeroCuenta = getNumeroCuenta(ccv);
      ccv = crearCCV(banco, sucursal, numeroCuenta);
    }
    if (!esCuentaBancaria(ccv)) {
      LOG.debug(PREFIX + "[FIN]crearCcvRoutingSanAndOpen() ## NO ES CUENTA VALORES CCV : " + ccv);
    }
    LOG.debug(PREFIX + "[FIN]crearCcvBanif() ## ccv : " + ccv);
    return ccv;
  }
}
