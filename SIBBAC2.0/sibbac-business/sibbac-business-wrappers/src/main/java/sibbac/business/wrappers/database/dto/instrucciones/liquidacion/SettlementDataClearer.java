package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;




public class SettlementDataClearer {

	// Datos
	private SettlementDataClearerDTO	tmct0cle		= null;

	// Flags
	private boolean						dataClearer		= false;
	private boolean						clearerMultiple	= false;
	private String						errormsg		= "";

	public SettlementDataClearer() {
		tmct0cle = new SettlementDataClearerDTO();
		dataClearer = false;
		clearerMultiple = false;
		initSettlementData();
	}

	public SettlementDataClearerDTO getTmct0cle() {
		return tmct0cle;
	}

	public void setTmct0cle( SettlementDataClearerDTO tmct0cle ) {
		this.tmct0cle = tmct0cle;
	}

	public boolean isDataClearer() {
		return dataClearer;
	}

	public void setDataClearer( boolean dataClearer ) {
		this.dataClearer = dataClearer;
	}

	public boolean isClearerMultiple() {
		return clearerMultiple;
	}

	public void setClearerMultiple( boolean clearerMultiple ) {
		this.clearerMultiple = clearerMultiple;
	}

	private void initSettlementData() {
		tmct0cle.initData();
	}

	/**
	 * @return the errormsg
	 */
	public String getErrormsg() {
		return errormsg;
	}

	/**
	 * @param errormsg the errormsg to set
	 */
	public void setErrormsg( String errormsg ) {
		this.errormsg = errormsg;
	}

}
