package sibbac.business.wrappers.database.bo;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.NettingDao;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.database.bo.AbstractBo;


@Service
public class NettingBo extends AbstractBo< Netting, Long, NettingDao > {

	public List< Netting > findByConsulta( BigDecimal titulosneteados, Date fcontratacionDe, Date fcontratacionA, Date fliquidacionDe,
			Date fliquidacionA, String alias, String isin, Integer tipo ) {

		String filtroAlias = "S";
		if ( alias.equals( "" ) ) {
			filtroAlias = "N";
		}
		String filtroIsin = "S";
		if ( isin.equals( "" ) ) {
			filtroIsin = "N";
		}
		String filtrofcontratacionDe = "S";
		if ( fcontratacionDe == null ) {
			filtrofcontratacionDe = "N";
		}
		String filtrofcontratacionA = "S";
		if ( fcontratacionA == null ) {
			filtrofcontratacionA = "N";
		}
		String filtrofliquidacionDe = "S";
		if ( fliquidacionDe == null ) {
			filtrofliquidacionDe = "N";
		}
		String filtrofliquidacionA = "S";
		if ( fliquidacionA == null ) {
			filtrofliquidacionA = "N";
		}
		String filtroTituloNeteado = "S";
		if ( titulosneteados == null ) {
			filtroTituloNeteado = "N";
		}


		
		if(tipo == 2){
			return dao.findByConsultaNotIsin( titulosneteados, filtroTituloNeteado, fcontratacionDe, filtrofcontratacionDe, fcontratacionA,
					filtrofcontratacionA, fliquidacionDe, filtrofliquidacionDe, fliquidacionA, filtrofliquidacionA, alias, filtroAlias, isin,
					filtroIsin );	
		}
		
		if(tipo == 3){

			return dao.findByConsultaNotAlias( titulosneteados, filtroTituloNeteado, fcontratacionDe, filtrofcontratacionDe, fcontratacionA,
					filtrofcontratacionA, fliquidacionDe, filtrofliquidacionDe, fliquidacionA, filtrofliquidacionA, alias, filtroAlias, isin,
					filtroIsin );		
		}
		
		if(tipo == 4){
			return dao.findByConsultaNotAliasNotIsin( titulosneteados, filtroTituloNeteado, fcontratacionDe, filtrofcontratacionDe, fcontratacionA,
					filtrofcontratacionA, fliquidacionDe, filtrofliquidacionDe, fliquidacionA, filtrofliquidacionA, alias, filtroAlias, isin,
					filtroIsin );
		}


		return dao.findByConsulta( titulosneteados, filtroTituloNeteado, fcontratacionDe, filtrofcontratacionDe, fcontratacionA,
				filtrofcontratacionA, fliquidacionDe, filtrofliquidacionDe, fliquidacionA, filtrofliquidacionA, alias, filtroAlias, isin,
				filtroIsin );
	}

}
