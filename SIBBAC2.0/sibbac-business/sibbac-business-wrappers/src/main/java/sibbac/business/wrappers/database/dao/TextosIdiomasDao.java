package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Idioma;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.business.wrappers.database.model.TextosIdiomas;


@Component
public interface TextosIdiomasDao extends JpaRepository< TextosIdiomas, Long > {

	/**
	 * consulta que realiza una buscada de los textosIdioma a partir de un id de
	 * texto
	 * 
	 * @param textos
	 * @return
	 */
	public List< TextosIdiomas > findAllByTexto_Id( final Long idTexto );

	public List< TextosIdiomas > findByTexto_TipoInAndIdioma_Codigo( List< String > tipos, String idioma );

	public TextosIdiomas findByTexto_TipoAndIdioma( final String tipo, final Idioma idioma );

	public TextosIdiomas findByIdiomaAndTexto( Idioma idioma, Texto texto );

}
