package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaRectificativa;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.common.utils.FormatDataUtils;

public class FacturaDTO {

	private Long idFactura;
	private Integer idEstado;
	private String descEstado;
	private String fechaCreacion;
	private String cdAlias;
	private String nombreAlias;
	private Long idPeriodo;
	private String descPeriodo;
	private String comentarios;
	private String fechaInicio;
	private String fechaFin;
	private BigDecimal baseImponible;
	private BigDecimal importeImpuestos;
	private BigDecimal importeTotal;
	private long numeroFactura;
	private BigDecimal importePendiente;
	private Date fecha;
	private String cdBrocli;

	public FacturaDTO(final Factura factura) {

		this.idFactura = factura.getId();
		final Tmct0estado estado = factura.getEstado();
		this.idEstado = estado.getIdestado();
		this.descEstado = estado.getNombre();
		final Periodos periodo = factura.getPeriodo();
		this.idPeriodo = periodo.getId();
		this.comentarios = factura.getComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFechaCreacion());
		this.baseImponible = factura.getImfinsvb();
		this.importeImpuestos = factura.getImporteImpuestos();
		this.importeTotal = factura.getImporteTotal();
		this.fechaInicio = FormatDataUtils.convertDateToString(factura.getFechaInicio());
		this.fechaFin = FormatDataUtils.convertDateToString(factura.getFechaFin());
		final Alias alias = factura.getAlias();
		this.cdAlias = alias.getCdaliass();
		this.nombreAlias = alias.getDescrali();
		this.numeroFactura = factura.getNumeroFactura();
		this.importePendiente = factura.getImportePendiente();
		this.fecha = factura.getFechaCreacion();
		if (factura.getAlias() != null) {
			this.cdBrocli = factura.getAlias().getCdbrocli();
		}

	}

	public FacturaDTO(final FacturaRectificativa factura) {
		
		this.idFactura = factura.getId();
		final Tmct0estado estado = factura.getEstado();
		this.idEstado = estado.getIdestado();
		this.descEstado = estado.getNombre();
		final Periodos periodo = factura.getPeriodo();
		this.idPeriodo = periodo.getId();
		this.comentarios = factura.getComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString(factura.getFechaCreacion());
		this.baseImponible = factura.getImfinsvb();
		this.importeImpuestos = factura.getImporteImpuestos();
		this.importeTotal = factura.getImporteTotal();
		this.fechaInicio = FormatDataUtils.convertDateToString(factura.getFechaInicio());
		this.fechaFin = FormatDataUtils.convertDateToString(factura.getFechaFin());
		final Alias alias = factura.getAlias();
		this.cdAlias = alias.getCdaliass();
		this.nombreAlias = alias.getDescrali();
		this.numeroFactura = factura.getNumeroFactura();

	}

	/**
	 * @return the idFactura
	 */
	public Long getIdFactura() {
		return idFactura;
	}

	/**
	 * @param idFacturaSugerida
	 *            the idFacturaSugerida to set
	 */
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	/**
	 * @return the idEstado
	 */
	public Integer getIdEstado() {
		return idEstado;
	}

	/**
	 * @param idEstado
	 *            the idEstado to set
	 */
	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	/**
	 * @return the descEstado
	 */
	public String getDescEstado() {
		return descEstado;
	}

	/**
	 * @param descEstado
	 *            the descEstado to set
	 */
	public void setDescEstado(String descEstado) {
		this.descEstado = descEstado;
	}

	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the cdAlias
	 */
	public String getCdAlias() {
		return cdAlias;
	}

	/**
	 * @param cdAlias
	 *            the cdAlias to set
	 */
	public void setCdAlias(String cdAlias) {
		this.cdAlias = cdAlias;
	}

	/**
	 * @return the nombreAlias
	 */
	public String getNombreAlias() {
		return nombreAlias;
	}

	/**
	 * @param nombreAlias
	 *            the nombreAlias to set
	 */
	public void setNombreAlias(String nombreAlias) {
		this.nombreAlias = nombreAlias;
	}

	/**
	 * @return the idPeriodo
	 */
	public Long getIdPeriodo() {
		return idPeriodo;
	}

	/**
	 * @param idPeriodo
	 *            the idPeriodo to set
	 */
	public void setIdPeriodo(Long idPeriodo) {
		this.idPeriodo = idPeriodo;
	}

	/**
	 * @return the descPeriodo
	 */
	public String getDescPeriodo() {
		return descPeriodo;
	}

	/**
	 * @param descPeriodo
	 *            the descPeriodo to set
	 */
	public void setDescPeriodo(String descPeriodo) {
		this.descPeriodo = descPeriodo;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios
	 *            the comentarios to set
	 */
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * @return the fechaInicio
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio
	 *            the fechaInicio to set
	 */
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the fechaFin
	 */
	public String getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin
	 *            the fechaFin to set
	 */
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	/**
	 * @return the importeImpuestos
	 */
	public BigDecimal getImporteImpuestos() {
		return importeImpuestos;
	}

	/**
	 * @param importeImpuestos
	 *            the importeImpuestos to set
	 */
	public void setImporteImpuestos(BigDecimal importeImpuestos) {
		this.importeImpuestos = importeImpuestos;
	}

	/**
	 * @return the baseImponible
	 */
	public BigDecimal getBaseImponible() {
		return baseImponible;
	}

	/**
	 * @param baseImponible
	 *            the baseImponible to set
	 */
	public void setBaseImponible(BigDecimal baseImponible) {
		this.baseImponible = baseImponible;
	}

	/**
	 * @return the importeTotal
	 */
	public BigDecimal getImporteTotal() {
		return importeTotal;
	}

	/**
	 * @param importeTotal
	 *            the importeTotal to set
	 */
	public void setImporteTotal(BigDecimal importeTotal) {
		this.importeTotal = importeTotal;
	}

	public long getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(long numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the importePendiente
	 */
	public BigDecimal getImportePendiente() {
		return importePendiente;
	}

	/**
	 * @param importePendiente
	 *            the importePendiente to set
	 */
	public void setImportePendiente(BigDecimal importePendiente) {
		this.importePendiente = importePendiente;
	}

	public boolean isAceptadaConErrores() {
		return EstadosEnumerados.FACTURA.ACEPTADA_CON_ERRORES.getId().equals(idEstado);
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getCdBrocli() {
		return cdBrocli;
	}

	public void setCdBrocli(String cdBrocli) {
		this.cdBrocli = cdBrocli;
	}

}
