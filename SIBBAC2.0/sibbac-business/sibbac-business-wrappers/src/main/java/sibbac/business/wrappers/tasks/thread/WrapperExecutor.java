package sibbac.business.wrappers.tasks.thread;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.common.SIBBACBusinessException;

public abstract class WrapperExecutor {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(WrapperExecutor.class);

  private ExecutorService executor = null;

  private ObserverProcess observer;

  public WrapperExecutor() {
    super();
    this.setObserver(new ObserverProcess());
  }

  public abstract int getThreadSize();

  public abstract int getTransactionSize();

  @Deprecated
  @Transactional
  public void executeProcessInThread(ObserverProcess observer, Runnable process) {
    LOG.trace("[WrapperMultireaded :: executeProcessInThread] Init ");

    esperarPorThreadPool(observer, 1000);
    LOG.info("[WrapperMultireaded :: executeProcessInThread] Lanzamos el proceso {}.", process);
    getExecutor().execute(process);
    observer.addThread(process);
    LOG.trace("[WrapperMultireaded :: executeProcessInThread] Fin ");
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Future submitCallable(ExecutorService e, Callable c) {
    esperarPorThreadPool(e, 1000);
    return e.submit(c);
  }

  @SuppressWarnings("rawtypes")
  public void recuperarExcepciones(List<Future<?>> listFutures) throws SIBBACBusinessException {
    for (Future future : listFutures) {
      try {
        future.get(0, TimeUnit.SECONDS);
      }
      catch (InterruptedException | ExecutionException | TimeoutException e) {
        future.cancel(true);
        throw new SIBBACBusinessException(e);
      }
      catch (Exception e) {
        future.cancel(true);
        throw new SIBBACBusinessException(e);
      }
    }
  }

  @Deprecated
  @SuppressWarnings({ "rawtypes", "unchecked" })
  public Future submitCallable(Callable c) {
    ExecutorService e = getExecutor();
    esperarPorThreadPool(e, 1000);
    return e.submit(c);
  }

  protected void esperarPorThreadPool(ExecutorService e, int esperarSiHilosGt) {
    if (e instanceof ThreadPoolExecutor) {
      esperarPorThreadPool((ThreadPoolExecutor) e, esperarSiHilosGt);
    }
    else if (e instanceof ForkJoinPool) {
      esperarPorThreadPool((ForkJoinPool) e, esperarSiHilosGt);
    }
  }

  @Deprecated
  protected void esperarPorThreadPool(ObserverProcess observer, int esperarSiHilosGt) {
    if (this.executor != null) {
      if (this.executor instanceof ThreadPoolExecutor) {
        esperarPorThreadPool((ThreadPoolExecutor) this.executor, esperarSiHilosGt);
      }
      else if (this.executor instanceof ForkJoinPool) {
        esperarPorThreadPool((ForkJoinPool) this.executor, esperarSiHilosGt);
      }
      else {
        try {
          while (observer.threadsMonitorized() > esperarSiHilosGt) {
            LOG.info("[esperarPorThreadPool] *** ESPERANDO POR HILOS ACTIVOS {} - {}..... ***",
                observer.threadsMonitorized(), getExecutor());

            this.executor.awaitTermination(500, TimeUnit.MILLISECONDS);

          }
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
          closeExecutorNow(this.executor);
        }
      }
    }
  }

  private void esperarPorThreadPool(ThreadPoolExecutor executor, int esperarSiHilosGt) {
    try {

      while (executor.getQueue().size() > esperarSiHilosGt && !executor.awaitTermination(500, TimeUnit.MILLISECONDS)) {
        LOG.info(
            "[esperarPorThreadPool] *** ESPERANDO POR HILOS ACTIVOS {} - QUEUED TASK {} - COMPLETED {} - {}..... ***",
            executor.getActiveCount(), executor.getQueue().size(), executor.getCompletedTaskCount(), executor);

      }

    }
    catch (InterruptedException e) {
      LOG.warn(e.getMessage(), e);
      closeExecutorNow(executor);
    }

  }

  private void esperarPorThreadPool(ForkJoinPool executor, int esperarSiHilosGt) {
    try {

      while (executor.getQueuedTaskCount() > esperarSiHilosGt && !executor.awaitTermination(500, TimeUnit.MILLISECONDS)) {
        LOG.info("[esperarPorThreadPool] *** ESPERANDO POR HILOS ACTIVOS {} - QUEUE {} COMPLETED {} - {}..... ***",
            executor.getActiveThreadCount(), executor.getQueuedTaskCount(), executor.getQueuedTaskCount(), executor);

      }

    }
    catch (InterruptedException e) {
      LOG.warn(e.getMessage(), e);
      closeExecutorNow(executor);
    }
  }

  @Deprecated
  @Transactional
  protected void executeProcessInThread(Runnable process) {
    executeProcessInThread(this.observer, process);
  }

  @Deprecated
  public ExecutorService getExecutor() {
    if (this.executor == null || this.executor.isShutdown() || this.executor.isTerminated()) {
      LOG.info("[WrapperMultireaded :: getExecutor] Creando Thread Pool size : " + getThreadSize());
      this.executor = this.getNewExecutor();
    }
    return this.executor;
  }

  public ExecutorService getNewExecutor() {
    return Executors.newFixedThreadPool(getThreadSize());
  }

  @Deprecated
  public void setExecutor(ExecutorService executor) {
    this.executor = executor;
  }

  public void closeExecutorNow(ExecutorService executor) {
    LOG.trace("[WrapperMultireaded :: closeExecutorNow] inicio");
    if (executor != null) {
      List<Runnable> listRunables = executor.shutdownNow();
      try {
        while (!executor.awaitTermination(1, TimeUnit.MINUTES)) {
          LOG.info("[esperarPorThreadPool :: closeExecutorNow] *** ESPERANDO POR HILOS ACTIVOS {}- hilos Activos:{}",
              executor, listRunables.size());
        }
      }
      catch (InterruptedException e) {
        LOG.warn("INCIDENCIA", e);
      }
    }

    LOG.trace("[WrapperMultireaded :: closeExecutorNow] fin");
  }

  @Deprecated
  protected void closeExecutorNow() {
    this.closeExecutorNow(this.executor);
  }

  public void closeExecutor(ExecutorService executor) {
    LOG.trace("[WrapperMultireaded :: closeExecutor] inicio");
    if (executor != null) {
      executor.shutdown();
      try {
        while (!executor.awaitTermination(1, TimeUnit.MINUTES)) {
          LOG.info("[esperarPorThreadPool :: closeExecutor] *** ESPERANDO POR HILOS ACTIVOS {}", executor);
        }
      }
      catch (InterruptedException e) {
        LOG.warn("INCIDENCIA", e);
      }
    }

    LOG.trace("[WrapperMultireaded :: closeExecutor] fin");
  }

  @Deprecated
  protected void closeExecutor() {
    if (this.executor != null) {
      this.closeExecutor(this.executor);
      this.executor = null;
    }

  }

  protected int calculateTransactionSizeBetweenThreads(int processListTam) {
    int tamdivideThreads = processListTam / getThreadSize();
    int tam = getTransactionSize();
    if (tamdivideThreads > 0) {
      tam = tamdivideThreads > getTransactionSize() ? getTransactionSize() : tamdivideThreads;
    }
    else {
      tam = 1;
    }

    if (tam < 1) {
      tam = 1;
    }
    return tam;
  }

  protected int calculateTransactionSizeBetweenThreads(long processListTam) {
    long tamdivideThreads = processListTam / getThreadSize();
    long tam = getTransactionSize();
    if (tamdivideThreads > 0) {
      tam = tamdivideThreads > getTransactionSize() ? getTransactionSize() : tamdivideThreads;
    }
    else {
      tam = 1;
    }

    if (tam < 1) {
      tam = 1;
    }
    return new BigDecimal(tam).intValue();
  }

  @Override
  protected void finalize() throws Throwable {
    this.closeExecutorNow();
    super.finalize();
  }

  @Deprecated
  protected ObserverProcess getObserver() {
    return observer;
  }

  @Deprecated
  protected void setObserver(ObserverProcess observer) {
    this.observer = observer;
  }

}
