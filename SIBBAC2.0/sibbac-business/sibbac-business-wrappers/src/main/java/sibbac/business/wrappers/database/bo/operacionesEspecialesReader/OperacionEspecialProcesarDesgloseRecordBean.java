package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;

public class OperacionEspecialProcesarDesgloseRecordBean extends OperacionEspecialRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 8836187079410880831L;

  private String numorden;

  private DesgloseRecordDTO desglose;

  public OperacionEspecialProcesarDesgloseRecordBean() {
    super();
  }

  public OperacionEspecialProcesarDesgloseRecordBean(String numorden) {
    this();
    this.numorden = numorden;
  }

  public String getNumorden() {
    return numorden;
  }

  public void setNumorden(String numorden) {
    this.numorden = numorden;
  }

  public DesgloseRecordDTO getDesglose() {
    return desglose;
  }

  public void setDesglose(DesgloseRecordDTO desglose) {
    this.desglose = desglose;
  }

  @Override
  public String toString() {
    return "OperacionEspecialProcesarDesgloseRecordBean [numorden=" + numorden + ", desglose=" + desglose + "]";
  }
  
  
  
}
