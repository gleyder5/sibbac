package sibbac.business.wrappers.common.threads;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;

/**
 * 
 * Procesa la lista de objetos Tmct0swi en un hilo de ejecucion independiente.
 *
 */
public class ProcesaListaTmct0DesgloseClititRunnable implements Runnable {

  /** Lista de elementos a procesar */
  private List<Object[]> listaTmct0desgloseclitit = null;
  private List<String> listaErroresTotal = null;

  /** Bo con los metodos que realizan las operaciones */
  private Tmct0desgloseclititBo miTmct0desgloseclititBo = null;

  /** Identificador de hilo que se esta creando. */
  private int iNumJob = 0;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaListaTmct0DesgloseClititRunnable.class);
  private static final String NOMBRE_SERVICIO = "ProcesaListaTmct0DesgloseClititRunnable";

  public ProcesaListaTmct0DesgloseClititRunnable(List<Object[]> listaTmct0desgloseclitit, Tmct0desgloseclititBo miTmct0desgloseclititBo, int iNumJob) {
    this.listaTmct0desgloseclitit = listaTmct0desgloseclitit;
    this.miTmct0desgloseclititBo = miTmct0desgloseclititBo;
    this.iNumJob = iNumJob;
  }

  @Override
  public void run() {
    try {
      // Metodo que inserta los registros
    	miTmct0desgloseclititBo.actualizarListaDesgloseClitit(listaTmct0desgloseclitit, iNumJob);
//      // Metodo que elimina el hilo de la lista de hilos
    	miTmct0desgloseclititBo.shutdownRunnable(this, iNumJob, false);

    } catch (Exception e) {

      LOG.error("[" + NOMBRE_SERVICIO + " :: Error en la ejcucion del hilo ]  " + iNumJob);
      LOG.error("[" + NOMBRE_SERVICIO + " :: Exception: ]  " + e.getMessage(), e);
      // Se marca en el proceso principal que se ha producido un error para notificar del mismo al usuario
      miTmct0desgloseclititBo.shutdownRunnable(this, iNumJob, true);

    } finally {
    	miTmct0desgloseclititBo.shutdownRunnable(this, iNumJob, false);
    }
  }

  public List<String> getListaErroresTotal() {
		return listaErroresTotal;
	}

  
}
