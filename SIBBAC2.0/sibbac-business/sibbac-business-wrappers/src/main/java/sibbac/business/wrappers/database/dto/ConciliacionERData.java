package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;
import java.util.Date;


public class ConciliacionERData {

	private Date		settlementDate;
	private String		isin;
	private String		descripcion;
	private String		mercado;
	private String		codigoCuentaLiquidacion;
	private BigDecimal	titulos;
	private BigDecimal	efectivo;
	private BigDecimal	corretaje;

	public ConciliacionERData() {
		super();
	}

	public ConciliacionERData( Date settlementDate, String isin, String descripcion, String mercado, String codigoCuentaLiquidacion,
			BigDecimal titulos, BigDecimal efectivo, BigDecimal corretaje ) {
		super();
		this.settlementDate = settlementDate;
		this.isin = isin;
		this.descripcion = descripcion;
		this.mercado = mercado;
		this.codigoCuentaLiquidacion = codigoCuentaLiquidacion;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = corretaje;
	}

	public ConciliacionERData( Date settlementDate, String isin, String descripcion, String mercado, String codigoCuentaLiquidacion,
			BigDecimal titulos, BigDecimal efectivo, int corretaje ) {
		super();
		this.settlementDate = settlementDate;
		this.isin = isin;
		this.descripcion = descripcion;
		this.mercado = mercado;
		this.codigoCuentaLiquidacion = codigoCuentaLiquidacion;
		this.titulos = titulos;
		this.efectivo = efectivo;
		this.corretaje = new BigDecimal( String.valueOf( corretaje ) );
	}

	public Date getSettlementDate() {
		return this.settlementDate;
	}

	public void setSettlementDate( Date settlementDate ) {
		this.settlementDate = settlementDate;
	}

	public String getIsin() {
		return this.isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public String getMercado() {
		return this.mercado;
	}

	public void setMercado( String mercado ) {
		this.mercado = mercado;
	}

	public String getCodigoCuentaLiquidacion() {
		return this.codigoCuentaLiquidacion;
	}

	public void setCodigoCuentaLiquidacion( String codigoCuentaLiquidacion ) {
		this.codigoCuentaLiquidacion = codigoCuentaLiquidacion;
	}

	public BigDecimal getTitulos() {
		return this.titulos;
	}

	public void setTitulos( BigDecimal titulos ) {
		this.titulos = titulos;
	}

	public BigDecimal getEfectivo() {
		return this.efectivo;
	}

	public void setEfectivo( BigDecimal efectivo ) {
		this.efectivo = efectivo;
	}

	public BigDecimal getCorretaje() {
		return this.corretaje;
	}

	public void setCorretaje( BigDecimal corretaje ) {
		this.corretaje = corretaje;
	}

}
