package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;


public class LineaCobroDTO {

	private Long		idFactura;
	private BigDecimal	importeAplicado;
	private boolean		cierreFactura;

	/**
	 * @return the cierreFactura
	 */
	public boolean isCierreFactura() {
		return cierreFactura;
	}

	public LineaCobroDTO() {
	}

	public LineaCobroDTO( final Long idFactura, final BigDecimal importeAplicado, final Boolean cierreFactura ) {
		super();
		this.idFactura = idFactura;
		this.importeAplicado = importeAplicado;
		this.cierreFactura = cierreFactura;
	}

	/**
	 * @return the idFactura
	 */
	public Long getIdFactura() {
		return idFactura;
	}

	/**
	 * @return the importeAplicado
	 */
	public BigDecimal getImporteAplicado() {
		return importeAplicado;
	}

}
