package sibbac.business.wrappers.database.bo;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.TipoDeDocumentoDao;
import sibbac.business.wrappers.database.model.TipoDeDocumento;
import sibbac.database.bo.AbstractBo;


@Service
public class TipoDeDocumentoBo extends AbstractBo< TipoDeDocumento, Long, TipoDeDocumentoDao > {

	public Map< String, TipoDeDocumento > getListaTipoDeDocumentos() {

		Map< String, TipoDeDocumento > resultados = new HashMap< String, TipoDeDocumento >();
		List< TipoDeDocumento > listaDocumentos = ( List< TipoDeDocumento > ) this.findAll();

		for ( TipoDeDocumento documento : listaDocumentos ) {
			if ( documento.getDescripcion() != null ) {
				resultados.put( documento.getDescripcion(), documento );
			}
		}

		return resultados;
	}

	public Map< String, String > altaTipoDeDocumento( Map< String, String > params ) {

		LOG.trace( "Se inicia la creacion de un tipo de documento" );
		Map< String, String > resultado = new HashMap< String, String >();
		TipoDeDocumento tdd = new TipoDeDocumento();

		tdd.setDescripcion( params.get( Constantes.PARAM_DESCRIPCION ) );

		String result = "";

		if ( this.save( tdd ) != null ) {
			result = " Se ha creado el tipo de documento";
			LOG.trace( "Se ha creado el tipo de documento" );
		} else {
			result = " No se ha creado el tipo de documento";
			LOG.trace( "No se ha creado el tipo de documento" );
		}

		resultado.put( "result", result );
		return resultado;

	}

	/**
	 * @param descripcion
	 * @return
	 */
	public TipoDeDocumento getTipoDeDocumento( final String descripcion ) {
		return dao.findByDescripcion( descripcion );
	}

}
