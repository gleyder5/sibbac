package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0TipoCompensador;


@Repository
public interface Tmct0TipoCompensadorDao extends JpaRepository< Tmct0TipoCompensador, Long > {

}
