package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.database.model.Tmct0sta;

public interface Tmct0menDao extends JpaRepository<Tmct0men, String> {

  Tmct0men findByCdmensa(final String cdmensa);

  Tmct0men findByCdmensaAndTmct0sta(String cdmensa, Tmct0sta estado);

  @Query(value = "select CDESTADO from BSNBPSQL.TMCT0MEN where CDMENSA = ?1", nativeQuery = true)
  Object[] findByCdMensaAndGetEstado(String cdmensa);
}
