package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class TitulosDesgloseManualAgrupadosDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 2336071504387724055L;

  private BigDecimal titulos;

  // AGRUPACION
  private BigDecimal precio;

  private String miembroCompensadorDestino;
  private String cuentaCompensacionDestino;
  private String codigoReferenciaAsignacion;

  public TitulosDesgloseManualAgrupadosDTO(BigDecimal precio, BigDecimal titulos) {
    this.precio = precio;
    this.titulos = titulos;
  }

  public TitulosDesgloseManualAgrupadosDTO(String cuentaCompensacionDestino, String miembroCompensadorDestino,
      String codigoReferenciaAsignacion, BigDecimal titulos) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
    this.miembroCompensadorDestino = miembroCompensadorDestino;
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
    this.titulos = titulos;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public String getMiembroCompensadorDestino() {
    return miembroCompensadorDestino;
  }

  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public String getCodigoReferenciaAsignacion() {
    return codigoReferenciaAsignacion;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setMiembroCompensadorDestino(String miembroCompensadorDestino) {
    this.miembroCompensadorDestino = miembroCompensadorDestino;
  }

  public void setCuentaCompensacionDestino(String cuentaCompensacionDestino) {
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
  }

  public void setCodigoReferenciaAsignacion(String codigoReferenciaAsignacion) {
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
  }
  
  
  
  

}
