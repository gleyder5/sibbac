package sibbac.business.wrappers.database.bo;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0MovimientoManualDao;
import sibbac.business.wrappers.database.dao.Tmct0MovimientoManualDaoImpl;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.model.Tmct0MovimientoManual;
import sibbac.business.wrappers.database.model.Tmct0SaldoInicialCuenta;
import sibbac.database.bo.AbstractBo;


/**
 * Dao Movimiento Manual
 * 
 * @author Cristina
 *
 */
/*
 * Metodo para insertar movimientos manuales
 */
@Service
public class Tmct0MovimientoManualBo extends AbstractBo< Tmct0MovimientoManual, Long, Tmct0MovimientoManualDao > {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0MovimientoManualBo.class);
  
	@Autowired
	private Tmct0MovimientoManualDaoImpl	daoImpl;

	@Autowired
	private Tmct0SaldoInicialCuentaBo		saldoBo;

	public Tmct0MovimientoManual insertMovimientoManual( Tmct0MovimientoManual entidad ) throws PersistenceException,
			DataIntegrityViolationException {
		Tmct0MovimientoManual movimiento = dao.save( entidad );
		return movimiento;
	}

	public List< Tmct0MovimientoManual > findAllNoTratadosACuentaVirtual() {
		return dao.findAllNoTratadosACuentaVirtual();
	}
	public void updateMovAlias( MovimientoCuentaAliasData data ) {
	    Tmct0MovimientoManual movMan = convertMovCtaAliasToMovMan(data);
	    updateMovManual(movMan);
	}
	private Tmct0MovimientoManual convertMovCtaAliasToMovMan(
		MovimientoCuentaAliasData data) {
	    Tmct0MovimientoManual man = new Tmct0MovimientoManual();
	    man.setAlias(data.getCdalias());
	    man.setIsin(data.getIsin());
	    man.setDescripcionCuentaLiquidacion(data.getCdcodigocuentaliq());
	    //man.setFechaContratacion(data.getTradedate());
	    man.setFechaLiquidacion(data.getSettlementDate());
	    man.setTitulos(data.getTitulos());
	    man.setEfectivo(data.getEfectivo());
	    return man;
	}

	public void updateMovManual( Tmct0MovimientoManual movMan ) {
		int count;
		DateFormat formatter = new SimpleDateFormat( "dd/MM/yyyy" );

		BigDecimal titulos = BigDecimal.ZERO;
		if ( movMan.getTitulos() != null ) {
			titulos = movMan.getTitulos();
		}
		BigDecimal efectivo = BigDecimal.ZERO;
		if ( movMan.getEfectivo() != null ) {
			efectivo = movMan.getEfectivo();
		}
		
		Date fechaAumentada = DateUtils.addDays(movMan.getFechaLiquidacion(), 1);
		Tmct0SaldoInicialCuenta saldoAnterior = saldoBo
				.findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc( movMan.getIsin(),
						movMan.getDescripcionCuentaLiquidacion(), movMan.getAlias(), fechaAumentada );
		
		if ( saldoAnterior == null ) {
			// El primer movimiento que existe es ese por lo que hay que crear un saldo inicial ese dia
			saldoAnterior = new Tmct0SaldoInicialCuenta( fechaAumentada, movMan.getIsin(), titulos, efectivo,
					movMan.getAlias(), movMan.getDescripcionCuentaLiquidacion() );
			saldoBo.save( saldoAnterior );
		} else if ( formatter.format( saldoAnterior.getFecha() ).equals( formatter.format( fechaAumentada ) ) ) {
			// Existe un saldo incial del dia en que se ha introducido y hay que modificarlo
			saldoAnterior.setTitulos( saldoAnterior.getTitulos().add( titulos ) );
			saldoAnterior.setImefectivo( saldoAnterior.getImefectivo().add( efectivo ) );
			saldoBo.save( saldoAnterior );
		} else {
			// No existe un saldo inicial del dia (pero si uno anterior) en que se ha introducido y hay que crearlo a partir de este
			Tmct0SaldoInicialCuenta saldoActual = new Tmct0SaldoInicialCuenta( fechaAumentada, saldoAnterior.getIsin(),
					saldoAnterior.getTitulos().add( titulos ), saldoAnterior.getImefectivo().add( efectivo ), saldoAnterior.getCdaliass(),
					saldoAnterior.getCdcodigocuentaliq() );
			saldoBo.save( saldoActual );
		}
		count = dao.updateSaldosFromMovManual( titulos, efectivo, movMan.getIsin(), movMan.getAlias(),
				movMan.getDescripcionCuentaLiquidacion(), fechaAumentada);
		LOG.debug("[updateMovManual] saldos actualizados: {}", count);
	}

	public List< Tmct0MovimientoManual > findAll( Map< String, Serializable > filters ) {
		List< Tmct0MovimientoManual > resultList = daoImpl.findAll( filters );
		return resultList;
	}
}
