package sibbac.business.wrappers.tasks;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.FacturaRectificativaBo;
import sibbac.business.wrappers.database.model.FacturaRectificativa;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_ENVIAR_FACTURA_RECTIFICATICA, interval = 1, intervalUnit = IntervalUnit.HOUR)
public class TaskEnvioFacturaRectificativa extends WrapperTaskConcurrencyPrevent {

    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskFactura.class);

    @Autowired
    protected FacturaRectificativaBo facturaRectificativaBo;
 
    public TaskEnvioFacturaRectificativa() {
    }

    @Override
    public void executeTask() {
	final String prefix = "[TaskEnvioFacturaRectificativa]";
	LOG.debug(prefix + "[Started...]");
	
	
	    try {
		executeTask(prefix);
	    } catch (Exception e) {
		throw e;
	    }
    }

    private void executeTask(final String prefix){
	final List<FacturaRectificativa> facturas = facturaRectificativaBo
		.enviarFacturas(this.jobPk);
	if (CollectionUtils.isNotEmpty(facturas)) {
	    LOG.trace(prefix + "[Enviadas " + facturas.size() + " facturas");
	} else {
	    LOG.debug(prefix + "[No se han enviado facturas");
	}
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_FACTURAS_ENVIO_FACTURA_RECTIFICATIVA;
    }

}
