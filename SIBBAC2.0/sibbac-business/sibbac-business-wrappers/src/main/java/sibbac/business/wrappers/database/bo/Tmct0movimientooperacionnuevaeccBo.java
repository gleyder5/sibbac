package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0movimientooperacionnuevaeccDao;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0movimientooperacionnuevaeccBo extends
    AbstractBo<Tmct0movimientooperacionnuevaecc, Long, Tmct0movimientooperacionnuevaeccDao> {

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  public List<Tmct0movimientooperacionnuevaecc> findBytmct0desglosecamara(Tmct0desglosecamara desglosecamara) {
    return this.dao.findBytmct0desglosecamara(desglosecamara);
  }

  public List<Tmct0movimientooperacionnuevaecc> findBycdoperacionecc(String Ccdoperacionecc) {
    return this.dao.findBycdoperacionecc(Ccdoperacionecc);
  }

  public List<Tmct0movimientooperacionnuevaecc> findByIdMovimiento(Long idMovimiento) {
    return this.dao.findByIdMovimiento(idMovimiento);
  }

  public List<Tmct0movimientooperacionnuevaecc> findByNudesglose(Long nudesglose) {
    return dao.findByNudesglose(nudesglose);
  }

  public List<Tmct0movimientooperacionnuevaecc> findByInCdestadomovAndNudesglose(List<String> cdestadosmov,
      Long nudesglose) {
    return dao.findByInCdestadomovAndNudesglose(cdestadosmov, nudesglose);
  }

  public List<Tmct0movimientooperacionnuevaecc> findByInCdestadomovAndIdejecucionalocation(List<String> cdestadosmov,
      Long idejecucionalocation) {
    return dao.findByInCdestadomovAndIdejecucionalocation(cdestadosmov, idejecucionalocation);
  }

  public BigDecimal sumCorretajeByInCdestadomovAndTmct0alcId(List<String> cdestadosmov, Tmct0alcId alcId) {
    return dao.sumCorretajeByInCdestadomovAndTmct0alcId(cdestadosmov, alcId);
  }

  public int updateIdMovimientoFidessaByCdoperacionecc(long idMovimientoFidessa, String cdoperacionecc, String cdusuaud) {
    return dao.updateIdMovimientoFidessaByCdoperacionecc(idMovimientoFidessa, cdoperacionecc, new Date(), cdusuaud);
  }

  public void inizializeMovimientoOperacioNueva(Tmct0desgloseclitit dt, BigDecimal corretaje,
      Tmct0movimientooperacionnuevaecc movn) {
    movn.setAuditFechaCambio(new Date());
    movn.setAuditUser("OP_ESP");
    movn.setCdmiembromkt(dt.getCdmiembromkt());
    movn.setImefectivo(dt.getImefectivo());
    movn.setImprecio(dt.getImprecio());
    movn.setImtitulos(dt.getImtitulos());
    movn.setNuoperacioninic(dt.getNuoperacioninic());
    movn.setNuoperacionprev(dt.getNuoperacionprev());
    movn.setCdoperacionecc(dt.getCdoperacionecc());
    movn.setNudesglose(dt.getNudesglose());
    movn.setCorretaje(corretaje);
  }

  public void inizializeMovimientoOperacioNueva(Tmct0operacioncdecc op, Tmct0movimientooperacionnuevaecc movn) {
    movn.setAuditFechaCambio(new Date());
    movn.setAuditUser("OP_S3");
    movn.setCdmiembromkt(op.getCdmiembromkt());
    movn.setImefectivo(op.getImefectivodisponible());
    movn.setImprecio(op.getImprecio());
    movn.setImtitulos(op.getImtitulosdisponibles());
    movn.setNuoperacioninic(op.getNuoperacioninic());
    movn.setNuoperacionprev(op.getNuoperacionprev());
    movn.setCdoperacionecc(op.getCdoperacionecc());
  }

  public void inizializeMovimientoOperacioNueva(Tmct0anotacionecc op, Tmct0movimientooperacionnuevaecc movn) {
    movn.setAuditFechaCambio(new Date());
    movn.setAuditUser("OP_S3");
    movn.setCdmiembromkt(op.getCdmiembromkt());
    movn.setImefectivo(op.getImefectivodisponible());
    movn.setImprecio(op.getImprecio());
    movn.setImtitulos(op.getImtitulosdisponibles());
    movn.setNuoperacioninic(op.getNuoperacioninic());
    movn.setNuoperacionprev(op.getNuoperacionprev());
    movn.setCdoperacionecc(op.getCdoperacionecc());
  }

  public void inizializeMovimientoOperacioNueva(Tmct0ejecucionalocation ejealo, Tmct0eje eje, Tmct0operacioncdecc op,
      Tmct0movimientooperacionnuevaecc movn) {
    movn.setAuditFechaCambio(new Date());
    movn.setAuditUser("OP_ESP");
    movn.setCdmiembromkt(op.getCdmiembromkt());
    movn.setCdoperacionecc("");
    movn.setImefectivo(eje.getImcbmerc().multiply(ejealo.getNutitulos()).setScale(2, RoundingMode.HALF_UP));
    movn.setImprecio(eje.getImcbmerc());
    movn.setImtitulos(ejealo.getNutitulos());
    movn.setNuoperacioninic(op.getCdoperacionecc());
    movn.setNuoperacionprev(op.getCdoperacionecc());
    movn.setCorretaje(BigDecimal.ZERO);
    movn.setIdejecucionalocation(ejealo.getIdejecucionalocation());
  }

  // @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  // public void engancharConMovFidessa(Tmct0movimientooperacionnuevaecc movn) {
  // List<Tmct0movimientoeccFidessa> listMovsf =
  // movfBo.findAllByCdoperacionecc(movn.getCdoperacionecc());
  // if (!listMovsf.isEmpty()) {
  // for (Tmct0movimientoeccFidessa movf : listMovsf) {
  // if (movf.getCdestadomov() != null &&
  // !movf.getCdestadomov().trim().equals("21")) {
  // movn.setIdMovimientoFidessa(movf.getIdMovimientoFidessa());
  // movf.setImtitulosPendientesAsignar(movf.getImtitulosPendientesAsignar().subtract(movn.getImtitulos()));
  //
  // if (movf.getImtitulosPendientesAsignar().compareTo(BigDecimal.ZERO) < 0) {
  // movf.setImtitulosPendientesAsignar(BigDecimal.ZERO);
  // }
  // break;
  // }
  // }
  // }
  // }

  // public List<Object[]> findByTmct0desglosecamaraOrderByFields(Long
  // desglosecamara) {
  // return this.dao.findByTmct0desglosecamaraOrderByFields(desglosecamara);
  // }

}
