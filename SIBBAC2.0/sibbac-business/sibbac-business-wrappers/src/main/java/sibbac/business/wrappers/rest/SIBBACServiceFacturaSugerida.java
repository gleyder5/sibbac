package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.FacturaSugeridaBo;
import sibbac.business.wrappers.database.dto.FacturaSugeridaDto;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.FacturaSugerida;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceFacturaSugerida implements SIBBACServiceBean {

  @Autowired
  private FacturaSugeridaBo facturaSugeridaBo;

  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  @Autowired
  private Tmct0estadoBo tmct0estadoBo;

  @Autowired
  private AliasBo aliasBo;

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceFacturaSugerida.class);

  private static final String CMD_MARCAR_FACTURAR = "marcarFacturar";
  private static final String CMD_CREATE_FACTURAS = "crearSugeridas";
  private static final String CMD_DELETE_FACTURAS = "borrarSugeridas";
  private static final String CMD_LISTA_FACTURAS = "getListadoFacturasSugeridas";
  private static final String CMD_LISTA_CREAR_FACTURAS = "getListadoCrearFacturas";

  private static final String FILTER_ID_FACT_SUGERIDA = "idFacturaSugerida";
  private static final String FILTER_ID_ALIAS = "idAlias";
  private static final String FILTER_FECHAHASTA = "fechaHasta";
  private static final String FILTER_ID_ESTADO = "idEstado";
  private static final String RESULT_MARCAR_FACTURAR = "marcarFacturar";
  private static final String RESULT_LISTA_FACTURAS = "listadoFacturas";

  public SIBBACServiceFacturaSugerida() {
  }

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    final String prefix = "[SIBBACServiceFacturaSugerida::process] ";
    LOG.debug(prefix + "Processing a web request...");

    final String command = webRequest.getAction();
    LOG.debug(prefix + "+ Command.: [{}]", command);

    final WebResponse webResponse = new WebResponse();

    switch (command) {
      case CMD_MARCAR_FACTURAR:
        final List<Map<String, String>> parametros = webRequest.getParams();
        if (CollectionUtils.isNotEmpty(parametros)) {
          final List<Long> idsFacturasSugeridas = getIdsFacturasSugeridas(parametros);
          LOG.trace("Recibidas " + idsFacturasSugeridas.size() + " facturas para marcar");
          final List<Long> facturasMarcadas = facturaSugeridaBo.marcarFacturasSugeridasFacturar(idsFacturasSugeridas);
          if (CollectionUtils.isEmpty(facturasMarcadas)) {
            LOG.trace("Se han marcado " + facturasMarcadas.size() + " facturas");
          } else {
            LOG.error("No se han marcado ninguna factura");
          }
          final Map<String, Object> resultadoMarcarFacturas = new HashMap<String, Object>();
          resultadoMarcarFacturas.put(RESULT_MARCAR_FACTURAR, "OK");
          webResponse.setResultados(resultadoMarcarFacturas);
        } else {
          webResponse.setError("No se han recibido facturas para marcar");
        }

        break;
      case CMD_CREATE_FACTURAS:
        this.generarFacturasSugeridas(webRequest, webResponse);
        break;
      case CMD_DELETE_FACTURAS:
        this.deleteFacturasSugeridas(webRequest, webResponse);
        break;
      case CMD_LISTA_FACTURAS:
        this.getListadoFacturasSugeridas(webRequest, webResponse);
        break;
      case CMD_LISTA_CREAR_FACTURAS:
        this.getListadoCrearFacturas(webRequest, webResponse);
        break;
      default:
        webResponse.setError(command + " is not a valid action. Valid actions are: " + this.getAvailableCommands());
        break;
    }
    return webResponse;
  }

  private void generarFacturasSugeridas(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    final String prefix = "[SIBBACServiceFacturaSugerida::generarFacturasSugeridas] ";

    final Map<String, String> filtros = webRequest.getFilters();
    if (MapUtils.isNotEmpty(filtros)) {
      final Long idAlias = getIdAlias(filtros.get(FILTER_ID_ALIAS));
      LOG.trace(prefix + "Solicitada la generacion de facturas sugeridas para el alias: [{}]", idAlias);
      
//      Calendar fechaHasta = null;
//      if (filtros.get(FILTER_FECHAHASTA) != null) {
//        fechaHasta = Calendar.getInstance();
//        fechaHasta.setTime(FormatDataUtils.convertStringToDate(filtros.get(FILTER_FECHAHASTA), FormatDataUtils.DATE_ES_FORMAT_SEPARATOR));
//      } else {
//        fechaHasta = Calendar.getInstance(); // hasta hoy
//      }
//      fechaHasta.set(Calendar.HOUR_OF_DAY, 23);
//      fechaHasta.set(Calendar.MINUTE, 59);
//      fechaHasta.set(Calendar.SECOND, 59);
//      fechaHasta.set(Calendar.MILLISECOND, 999);

      Date fechaHasta = null;;
      if (filtros.get(FILTER_FECHAHASTA) != null && !filtros.get(FILTER_FECHAHASTA).isEmpty()) {
        fechaHasta = FormatDataUtils.convertStringToDate(filtros.get(FILTER_FECHAHASTA), FormatDataUtils.DATE_ES_FORMAT_SEPARATOR);
      } else {
        fechaHasta = Calendar.getInstance().getTime(); // hasta hoy
      }

      // Aqui llamamos al metodo al que llama TaskFacturaSugerida para crear las sugeridas.
      // Buscamos el "alias".
      Alias alias = aliasBo.findById(idAlias);
      String cdAliass = alias.getCdaliass();
      // Creamos una lista de aliases para que el metodo funcione.
      List<Alias> aliases = new ArrayList<Alias>();
      aliases.add(alias);
      // Invocamos al metodo.
      final List<FacturaSugerida> sugeridas = facturaSugeridaBo.generarFacturasSugeridas(aliases, null, fechaHasta);

      if (sugeridas == null || sugeridas.isEmpty()) {
        LOG.debug(prefix + "No se han generado sugerencias de facturas para el alias: [{}]", cdAliass);
        webResponse.setError("No se han generado sugerencias de facturas para el alias: [" + cdAliass + "]!");
      } else {
        int total = sugeridas.size();
        LOG.debug(prefix + "Se han generado {} sugerencias de facturas para el alias: [{}]", total, cdAliass);
        webResponse.setError("Generada(s) " + total + " sugerencia(s) de factura(s)");
      }
    } else {
      LOG.warn(prefix + "¡No se ha recibido ningun identificador de sugerencia de factura para eliminar!");
      webResponse.setError("¡No se ha recibido ningun identificador de sugerencia de factura para eliminar!");
    }
  }

  private void deleteFacturasSugeridas(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    final String prefix = "[SIBBACServiceFacturaSugerida::deleteFacturasSugeridas] ";
    final Map<String, String> filtros = webRequest.getFilters();
    if (MapUtils.isNotEmpty(filtros)) {
      final Long id = getIdAlias(filtros.get(FILTER_ID_FACT_SUGERIDA));
      LOG.trace(prefix + "Solicitado el borrado de la factura sugerida: [{}]", id);
      FacturaSugerida facturaSugerida = facturaSugeridaBo.findById(id);
      if (facturaSugerida == null) {
        LOG.warn(prefix + "¡No se ha encontrado ninguna sugerencia de factura para el identificador: [{}]!", id);
        webResponse.setError("¡No se ha encontrado ninguna sugerencia de factura para el identificador: [" + id + "]!");
      } else {
        LOG.trace(prefix + "Encontrada la factura sugerida: [{}]", facturaSugerida.getId());
        // Le cambiamos el estado a los AlcOrdenes de la factura.
        List<AlcOrdenes> ordenes = facturaSugerida.getAlcOrdenes();
        if (ordenes != null && !ordenes.isEmpty()) {
          long total = ordenes.size();
          LOG.trace(prefix + "Cambiando el estado de las {} \"AlcOrdenes\"...", total);
          final Tmct0estado estadoAlcLibre = tmct0estadoBo.findByIdAlcOrden(EstadosEnumerados.ALC_ORDENES.LIBRE);
          for (AlcOrdenes orden : ordenes) {
            orden.setEstado(estadoAlcLibre);
          }
        }
        // Borramos la factura.
        LOG.trace(prefix + "Eliminando la factura sugerida...", id);
        facturaSugeridaBo.delete(facturaSugerida);
        LOG.debug(prefix + "Sugerencia de factura({}) eliminada.", id);
        webResponse.setError("Sugerencia de factura(" + id + ") eliminada");
      }
    } else {
      LOG.warn(prefix + "¡No se ha recibido ningun identificador de sugerencia de factura para eliminar!");
      webResponse.setError("¡No se ha recibido ningun identificador de sugerencia de factura para eliminar!");
    }
  }

  /**
   * Devuelve la lista de facturas sugeridas que cumplen los requisitos de los filtros.
   * 
   * @param webRequest
   * @param webResponse
   * @throws SIBBACBusinessException
   */
  private void getListadoFacturasSugeridas(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    final Map<String, String> filtros = webRequest.getFilters();
    final Map<String, List<FacturaSugeridaDto>> facturas = new HashMap<String, List<FacturaSugeridaDto>>();
    if (MapUtils.isNotEmpty(filtros)) {
      final Long idAlias = getIdAlias(filtros.get(FILTER_ID_ALIAS));
      final Integer idEstado = getIdEstado(filtros.get(FILTER_ID_ESTADO));
      facturas.put(RESULT_LISTA_FACTURAS, facturaSugeridaBo.getListadoFacturasSugeridas(idAlias, idEstado));
      webResponse.setResultados(facturas);
    } else {
      facturas.put(RESULT_LISTA_FACTURAS, facturaSugeridaBo.getListadoFacturasSugeridas(null, null));
      webResponse.setResultados(facturas);
    }
  }

  /**
   * Obtiene la facturas sugeridas para el alias dado y fecha de creación de la factura menos de la especificada.
   * 
   * @param webRequest
   * @param webResponse
   * @throws SIBBACBusinessException
   */
  private void getListadoCrearFacturas(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    final Map<String, String> filtros = webRequest.getFilters();
    final Map<String, List<FacturaSugeridaDto>> facturas = new HashMap<String, List<FacturaSugeridaDto>>();
    if (MapUtils.isNotEmpty(filtros)) {
      final Long idAlias = getIdAlias(filtros.get(FILTER_ID_ALIAS));

      Calendar fechaHasta = null;
      if (filtros.get(FILTER_FECHAHASTA) != null && !filtros.get(FILTER_FECHAHASTA).isEmpty()) {
        fechaHasta = Calendar.getInstance();
        fechaHasta.setTime(FormatDataUtils.convertStringToDate(filtros.get(FILTER_FECHAHASTA), FormatDataUtils.DATE_ES_FORMAT_SEPARATOR));
      } else {
        fechaHasta = Calendar.getInstance(); // hasta hoy
      }
      fechaHasta.set(Calendar.HOUR_OF_DAY, 23);
      fechaHasta.set(Calendar.MINUTE, 59);
      fechaHasta.set(Calendar.SECOND, 59);
      fechaHasta.set(Calendar.MILLISECOND, 999);

      facturas.put(RESULT_LISTA_FACTURAS, facturaSugeridaBo.getListadoCrearFacturas(idAlias, fechaHasta.getTime()));
      webResponse.setResultados(facturas);
    } else {
      facturas.put(RESULT_LISTA_FACTURAS, facturaSugeridaBo.getListadoCrearFacturas(null, null));
      webResponse.setResultados(facturas);
    }
  } // getListadoCrearFacturas

  /**
   * @param idEstadoStr
   * @return
   * @throws SIBBACBusinessException
   */
  private Integer getIdEstado(final String idEstadoStr) throws SIBBACBusinessException {
    Integer idEstado = null;
    try {
      idEstado = FormatDataUtils.convertStringToInteger(idEstadoStr);
    } catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException("Error leyendo el valor de \"idEstado\": [" + idEstadoStr + "] ");
    }

    return idEstado;
  }

  /**
   * @param idAliasStr
   * @return
   * @throws SIBBACBusinessException
   */
  private Long getIdAlias(final String idAliasStr) throws SIBBACBusinessException {
    Long idAlias = null;
    try {
      if (StringUtils.isNotBlank(idAliasStr)) {
        idAlias = NumberUtils.createLong(idAliasStr);
      }
    } catch (NumberFormatException e) {
      throw new SIBBACBusinessException("Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] ");
    }
    return idAlias;
  }

  /**
   * @param parametros
   * @return
   */
  private List<Long> getIdsFacturasSugeridas(final List<Map<String, String>> parametros) {
    final List<Long> idsFacturasSugeridas = new ArrayList<Long>();
    for (Map<String, String> parametro : parametros) {
      idsFacturasSugeridas.add(NumberUtils.createLong(parametro.get(FILTER_ID_FACT_SUGERIDA)));
    }
    return idsFacturasSugeridas;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_MARCAR_FACTURAR);
    commands.add(CMD_LISTA_FACTURAS);
    commands.add(CMD_LISTA_CREAR_FACTURAS);
    return commands;
  }

  @Override
  public List<String> getFields() {

    return null;
  }

}
