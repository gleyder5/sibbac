package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;

@Repository
public interface Tmct0bokDao extends JpaRepository<Tmct0bok, Tmct0bokId> {

  @Query("SELECT bok.casepti FROM Tmct0bok bok WHERE bok.id = :bookingId")
  Character findCasepti(@Param("bookingId") Tmct0bokId bookingId);

  @Query("SELECT bok FROM Tmct0bok bok WHERE bok.tmct0ord.cdcuenta IN :cdcuenta  "
      + "AND bok.fecontra = :fecontra AND bok.tmct0estadoByCdestadoasig.idestado > 0 AND bok.casepti='S'")
  public List<Tmct0bok> findAllByFecontraAndInCdcuenta(@Param("cdcuenta") List<String> cdcuenta,
      @Param("fecontra") Date fecontra);

  @Query("SELECT bok FROM Tmct0bok bok WHERE bok.id.nbooking = :nbooking")
  public List<Tmct0bok> findByNbooking(@Param("nbooking") String nbooking);

  /**
   * 
   * Actualiza el booking a casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   */
  @Modifying
  @Query(value = "UPDATE TMCT0BOK B SET B.CASEPTI='S', B.FHAUDIT=:fhaudit WHERE B.NUORDEN=:nuorden AND B.NBOOKING=:nbooking AND B.CDESTADOASIG > 0 ", nativeQuery = true)
  public Integer updateFlagCasadoPti(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("fhaudit") Date fhaudit);

  /**
   * 
   * Actualiza el booking a casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   */
  @Modifying
  @Query("UPDATE Tmct0bok b SET b.cdindimp='S', b.fhaudit=:fhaudit, b.cdusuaud = :usuario "
      + " WHERE b.id IN :bookings AND (b.cdindimp <> 'S' OR b.cdindimp is null) and b.tmct0estadoByCdestadoasig.idestado > :estado ")
  public Integer updateEnviadoConfirmacionMinorista1617(@Param("bookings") List<Tmct0bokId> bookings,
      @Param("estado") Integer estadoasignacion, @Param("fhaudit") Date fhaudit,
      @Param("usuario") String usuarioAuditoria);

  /**
   * 
   * Actualiza el booking a casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   */
  @Modifying
  @Query("UPDATE Tmct0bok b SET b.cdindimp='S', b.fhaudit=:fhaudit, b.cdusuaud = :usuario "
      + " WHERE b.id = :pid AND (b.cdindimp <> 'S' OR b.cdindimp is null) and b.tmct0estadoByCdestadoasig.idestado > :estado ")
  public Integer updateEnviadoConfirmacionMinorista1617(@Param("pid") Tmct0bokId bookings,
      @Param("estado") Integer estadoasignacion, @Param("fhaudit") Date fhaudit,
      @Param("usuario") String usuarioAuditoria);

  @Modifying
  @Query("update Tmct0bok bok set bok.tmct0estadoByCdestadoasig.idestado = :cdestadoasigTo, bok.fhaudit = :pfhaudit "
      + " where bok.id = :bookingId and bok.tmct0estadoByCdestadoasig.idestado = :cdestadoasigFrom ")
  public Integer updateCdestadoasigByIdAndCdestadoasig(@Param("bookingId") Tmct0bokId bokId,
      @Param("cdestadoasigFrom") int cdestadoasigFrom, @Param("cdestadoasigTo") int cdestadoasigTo,
      @Param("pfhaudit") Date fhaudit);

  @Modifying
  @Query("update Tmct0bok o set o.tmct0estadoByCdestadoasig.idestado = :cdestadoasigTo, o.fhaudit = :pfhaudit "
      + " where o.id = :bokId  ")
  Integer updateCdestadoasigByTmct0bokId(@Param("bokId") Tmct0bokId bokId, @Param("cdestadoasigTo") int cdestadoasigTo,
      @Param("pfhaudit") Date fhaudit);

  @Modifying
  @Query("update Tmct0bok o set o.cdestadotit = :cdestadotitTo, o.fhaudit = :pfhaudit, o.cdusuaud = :cdusuaud where o.id = :bokId  ")
  Integer updateCdestadotitByTmct0bokId(@Param("bokId") Tmct0bokId bokId, @Param("cdestadotitTo") int cdestadotitTo,
      @Param("pfhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);

  @Query("SELECT bok.nutiteje FROM Tmct0bok bok WHERE bok.id = :bokId")
  BigDecimal findNutitejeById(@Param("bokId") Tmct0bokId bokId);

  @Query("SELECT b.id FROM Tmct0ord o, Tmct0bok b, Tmct0alc c "
      + " WHERE o.nuorden = b.id.nuorden and b.fetrade >= :sincedate and b.tmct0estadoByCdestadoasig.idestado >= :estado "
      + " and b.casepti = 'S' AND o.nureford = '' and b.cdalias not in (:aliass) and b.cdindimp = '' "
      + " and c.id.nuorden=b.id.nuorden and c.id.nbooking=b.id.nbooking and c.cdestadoasig >= :estado and c.nutitliq = b.nutiteje ")
  List<Tmct0bokId> findAllConfirmacionMinorista(@Param("aliass") List<String> notInAliass,
      @Param("estado") Integer estado, @Param("sincedate") Date sincedate);

  /**
   * Regresa una lista de los números de booking cuya sumatoria de titulos por
   * enviar a S3 sea diferente a los titulos ejecutados para un número de orden
   * dado
   * @param nuorden El número de orden
   * @param asigPdteLiquidar El estado que representa a Pendiente de liquidar
   * @param entrecPdteFichero El estado que representa a Pendiente Fichero
   * @param entrecRechazadoFichero El estado que representa a Fichero rechazado
   * @param incidencia El estado que representa incidencia
   * @return Una lista de arrays de objetos. El array de objetos es de longitud
   * 3, cuyo primer elemento es el número de booking, el segundo el número de de
   * titulos ejecutados y el tercero el número de titulos del desglose cámara.
   * Solo se traen resultados cuando el segundo y tercer valores son diferentes.
   */
  @Query(value = "SELECT bok.NBOOKING, bok.NUTITEJE, SUM(dc.NUTITULOS) AS NUTITULOS " + "FROM BSNBPSQL.TMCT0BOK bok "
      + "INNER JOIN BSNBPSQL.TMCT0ALC alc ON " + " alc.NUORDEN = bok.NUORDEN AND "
      + " alc.NBOOKING = bok.NBOOKING AND " + " alc.CDESTADOASIG >= :asigPdteLiquidar "
      + "INNER JOIN BSNBPSQL.TMCT0DESGLOSECAMARA dc ON " + " dc.NUORDEN = alc.NUORDEN AND "
      + " dc.NBOOKING = alc.NBOOKING AND " + " dc.NUCNFCLT = alc.NUCNFCLT AND " + " dc.NUCNFLIQ = alc.NUCNFLIQ AND "
      + " dc.CDESTADOASIG >= :asigPdteLiquidar AND " + " (dc.CDESTADOENTREC = :entrecPdteFichero "
      + "  OR dc.CDESTADOENTREC = :entrecRechazadoFichero " + "  OR dc.CDESTADOENTREC = :incidencia) "
      + "WHERE bok.NUORDEN =  :nuorden " + "GROUP BY bok.NBOOKING, bok.NUTITEJE "
      + "HAVING bok.NUTITEJE != SUM(dc.NUTITULOS) ", nativeQuery = true)
  List<Object[]> booksIncompletos(@Param("nuorden") String nuorden, @Param("asigPdteLiquidar") int asigPdteLiquidar,
      @Param("entrecPdteFichero") int entrecPdteFichero, @Param("entrecRechazadoFichero") int entrecRechazadoFichero,
      @Param("incidencia") int incidencia);

  @Query("select b.cdusublo from Tmct0bok b where b.id = :bookingId ")
  String findCdusubloByBookingId(@Param("bookingId") Tmct0bokId bookId);

  @Query("select b.fhaudit from Tmct0bok b where b.id = :bookingId ")
  Date findFhauditByBookingId(@Param("bookingId") Tmct0bokId bookId);

  @Modifying
  @Query("UPDATE Tmct0bok b set b.cdusublo = :auditUser, b.cdusuaud = :auditUser, b.fhaudit = :auditDate where b.id = :bookingId and b.cdusublo ='' ")
  int bloquearBooking(@Param("bookingId") Tmct0bokId bookId, @Param("auditUser") String auditUser,
      @Param("auditDate") Date auditDate);

  @Modifying
  @Query("UPDATE Tmct0bok b set b.cdusublo = '', b.cdusuaud = :auditUser, b.fhaudit = :auditDate where b.id = :bookingId and b.cdusublo = :auditUser ")
  int desBloquearBooking(@Param("bookingId") Tmct0bokId bookId, @Param("auditUser") String auditUser,
      @Param("auditDate") Date auditDate);

}
