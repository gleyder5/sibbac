package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

import sibbac.business.fase0.database.model.Tmct0mfd;
import sibbac.common.helper.CloneObjectHelper;

public class SettlementDataClientMfdDTO implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 7562866770721660068L;

  public SettlementDataClientMfdDTO() {
    initData();
  }

  public SettlementDataClientMfdDTO(Tmct0mfd mfd) {
    this();
    CloneObjectHelper.copyBasicFields(mfd, this, null);
  }

  /**
   * Table Field CDBROCLI. CLIENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdbrocli;

  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal numsec;
  /**
   * Table Field CATEGORY. CATEGORY Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String category;
  /**
   * Table Field CDUSUAUD. AUDIT USER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. AUDIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.sql.Timestamp fhaudit;
  /**
   * Table Field FHINICIO. INIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhinicio;
  /**
   * Table Field FHFINAL. FINAL DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhfinal;

  /**
   * @return the cdbrocli
   */
  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  /**
   * @param cdbrocli the cdbrocli to set
   */
  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  /**
   * @return the numsec
   */
  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  /**
   * @param numsec the numsec to set
   */
  public void setNumsec(java.math.BigDecimal numsec) {
    this.numsec = numsec;
  }

  /**
   * @return the category
   */
  public java.lang.String getCategory() {
    return category;
  }

  /**
   * @param category the category to set
   */
  public void setCategory(java.lang.String category) {
    this.category = category;
  }

  /**
   * @return the cdusuaud
   */
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * @param cdusuaud the cdusuaud to set
   */
  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  /**
   * @return the fhaudit
   */
  public java.sql.Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * @param fhaudit the fhaudit to set
   */
  public void setFhaudit(java.sql.Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * @return the fhinicio
   */
  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }

  /**
   * @param fhinicio the fhinicio to set
   */
  public void setFhinicio(java.math.BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  /**
   * @return the fhfinal
   */
  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }

  /**
   * @param fhfinal the fhfinal to set
   */
  public void setFhfinal(java.math.BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public void initData() {
    setCdbrocli("");
    setNumsec(BigDecimal.ZERO);
    setCategory("");
    setCdusuaud("");
    setFhaudit(null);
    setFhinicio(BigDecimal.ZERO);
    setFhfinal(BigDecimal.ZERO);
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SettlementDataClientMfdDTO [cdbrocli=" + cdbrocli + ", numsec=" + numsec + ", category=" + category
           + ", cdusuaud=" + cdusuaud + ", fhaudit=" + fhaudit + ", fhinicio=" + fhinicio + ", fhfinal=" + fhfinal
           + "]";
  }

}
