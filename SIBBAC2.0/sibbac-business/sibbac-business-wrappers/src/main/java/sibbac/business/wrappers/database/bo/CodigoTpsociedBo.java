package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.CodigoTpsociedDao;
import sibbac.business.wrappers.database.model.CodigoTpsocied;
import sibbac.database.bo.AbstractBo;

@Service
public class CodigoTpsociedBo extends
		AbstractBo<CodigoTpsocied, String, CodigoTpsociedDao> {

	public CodigoTpsocied findByCodigo(String codigo) {
		CodigoTpsocied tpsocied = null;
		try {
			tpsocied = dao.findByCodigo(codigo);
		} catch (Exception e) {
			tpsocied = null;
		}

		return tpsocied;
	}
}
