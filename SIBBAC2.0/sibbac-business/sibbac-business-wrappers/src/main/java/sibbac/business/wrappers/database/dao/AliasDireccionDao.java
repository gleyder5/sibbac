package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasDireccion;


@Component
public interface AliasDireccionDao extends JpaRepository< AliasDireccion, Long > {

	public List< AliasDireccion > findAllByAlias( Alias alias );

}
