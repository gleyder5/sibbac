package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.dto.AliasDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceAlias implements SIBBACServiceBean {

	/** The aliass bo. */
	@Autowired
	private AliasBo				aliasBo;

	public static final String	CMD_GET_ALIAS						= "getListaAlias";
	public static final String	CMD_GET_ALIAS_FACTURACION			= "getListaAliasFacturacion";
	public static final String	CMD_GET_ALIAS_FACTURACION_ALIAS		= "getListaAliasFacturacionAlias";
	public static final String	CMD_GET_ALIAS_FACTURACION_SUBCUENTA	= "getListaAliasFacturacionSubcuenta";
	public static final String	CMD_UPD_ALIAS						= "modificarAlias";
	public static final String	CMD_GET_ALIAS_SIN_FACTURACION		= "getListaAliasSinFacturacion";
	public static final String	CMD_GET_ALIAS_CLIENTE				= "listaAliasCliente";
	

	private static final String	FILTER_ID_ALIAS						= "idAlias";
	private static final String	FILTER_ID_SUBCUENTA					= "idSubcuenta";
	private static final String	FILTER_ID_ALIAS_FACTURACION			= "idAliasFacturacion";
	private static final String	FILTER_ID_IDIOMA					= "idIdioma";
	private static final String	FILTER_CLIENTES						= "cliente";

	private static final String	RESULTADO_LISTA_ALIAS				= "listaAlias";
	private static final String	RESULTADO							= "resultado";
	private static final String	RESULTADO_SUCCESS					= "SUCCESS";
	/** The Constant LOG. */
	private static final Logger	LOG									= LoggerFactory.getLogger( SIBBACServiceAlias.class );

	public SIBBACServiceAlias() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		LOG.debug( "[SIBBACServiceAlias::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// obtiene comandos(paths)
		final String action = webRequest.getAction();
		LOG.debug( "[SIBBACServiceAlias::process] " + "+ Command.: [{}]", action );

		switch ( action ) {
			case SIBBACServiceAlias.CMD_GET_ALIAS:
				this.getAlias( webRequest, webResponse );
				break;
			case SIBBACServiceAlias.CMD_GET_ALIAS_FACTURACION:
				this.getAliasFacturacion( webRequest, webResponse );
				break;
			case SIBBACServiceAlias.CMD_GET_ALIAS_FACTURACION_ALIAS:
				this.getAliasFacturacionAlias( webRequest, webResponse );
				break;
			case SIBBACServiceAlias.CMD_GET_ALIAS_FACTURACION_SUBCUENTA:
				this.getAliasFacturacionSubcuenta( webRequest, webResponse );
				break;
			case SIBBACServiceAlias.CMD_UPD_ALIAS:
				this.updateAlias( webRequest, webResponse );
				break;
			case SIBBACServiceAlias.CMD_GET_ALIAS_SIN_FACTURACION:
				this.getAliasSinFacturacion( webRequest, webResponse );
				break;
			case SIBBACServiceAlias.CMD_GET_ALIAS_CLIENTE:
				this.getAliasCliente( webRequest, webResponse );
				break;
			default:
				webResponse.setError( "No recibe ningun comando" );
				LOG.error( "No le llega ningun comando" );
				break;
		}

		return webResponse;
	}

	private void getAlias( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, List< AliasDTO >> resultados = new HashMap< String, List< AliasDTO >>();
		resultados.put( RESULTADO_LISTA_ALIAS, aliasBo.getListaAlias() );
		webResponse.setResultados( resultados );
	}
	
	private void getAliasSinFacturacion( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, List< AliasDTO >> resultados = new HashMap< String, List< AliasDTO >>();
		resultados.put( RESULTADO_LISTA_ALIAS, aliasBo.getListaAliasSinFacturacion() );
		webResponse.setResultados( resultados );
	}
	
	

  private void getAliasFacturacion(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    final Map<String, List<AliasDTO>> resultados = new HashMap<String, List<AliasDTO>>();
    resultados.put(RESULTADO_LISTA_ALIAS, aliasBo.getListaAliasFacturacion());
    webResponse.setResultados(resultados);
  }

	private void updateAlias( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idAlias = getIdAlias( filtros.get( FILTER_ID_ALIAS ) );
		final Long idAliasFacturacion = getIdAlias( filtros.get( FILTER_ID_ALIAS_FACTURACION ) );
		final Long idIdioma = getIdAlias( filtros.get( FILTER_ID_IDIOMA ) );
		aliasBo.updateAlias( idAlias, idAliasFacturacion, idIdioma );
		final Map< String, String > resultados = new HashMap< String, String >();
		resultados.put( RESULTADO, RESULTADO_SUCCESS );
		webResponse.setResultados( resultados );
	}

	private void getAliasFacturacionAlias( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idAlias = getIdAlias( filtros.get( FILTER_ID_ALIAS ) );
		final Map< String, List< AliasDTO >> resultados = new HashMap< String, List< AliasDTO >>();
		resultados.put( RESULTADO_LISTA_ALIAS, aliasBo.getAliasFacturacionAlias( idAlias ) );
		webResponse.setResultados( resultados );
	}

	private void getAliasFacturacionSubcuenta( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idSubcuenta = getIdSubcuenta( filtros.get( FILTER_ID_SUBCUENTA ) );
		final Map< String, List< AliasDTO >> resultados = new HashMap< String, List< AliasDTO >>();
		resultados.put( RESULTADO_LISTA_ALIAS, aliasBo.getAliasFacturacionSubcuenta( idSubcuenta ) );
		webResponse.setResultados( resultados );
	}

	/**
	 * @param idAliasStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAlias( final String idAliasStr ) throws SIBBACBusinessException {
		Long idAlias = null;
		try {
			if ( StringUtils.isNotBlank( idAliasStr ) ) {
				idAlias = NumberUtils.createLong( idAliasStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] " );
		}
		return idAlias;
	}

	/**
	 * @param idSubcuentaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdSubcuenta( final String idSubcuentaStr ) throws SIBBACBusinessException {
		Long idSubcuenta = null;
		try {
			if ( StringUtils.isNotBlank( idSubcuentaStr ) ) {
				idSubcuenta = NumberUtils.createLong( idSubcuentaStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idSubcuenta\": [" + idSubcuentaStr + "] " );
		}
		return idSubcuenta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		final List< String > commands = new ArrayList< String >();
		commands.add( SIBBACServiceAlias.CMD_GET_ALIAS );
		commands.add( SIBBACServiceAlias.CMD_GET_ALIAS_FACTURACION );
		commands.add( SIBBACServiceAlias.CMD_GET_ALIAS_FACTURACION_ALIAS );
		commands.add( SIBBACServiceAlias.CMD_UPD_ALIAS );
		commands.add( SIBBACServiceAlias.CMD_GET_ALIAS_SIN_FACTURACION );
		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	}
	
	@Transactional
	private void getAliasCliente( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final String cliente =  filtros.get( FILTER_CLIENTES );
		String[] listaCli = cliente.split(";");
		
		final Map< String, List< AliasDTO >> resultados = new HashMap< String, List< AliasDTO >>();
		resultados.put( RESULTADO_LISTA_ALIAS, aliasBo.findAliasCliente(listaCli) );
		webResponse.setResultados( resultados );
	}

}
