package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.AliasDireccionBo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceAliasDirecciones implements SIBBACServiceBean {

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServiceAliasDirecciones.class );

	@Autowired
	private AliasDireccionBo	aliasDireccionBo;

	public SIBBACServiceAliasDirecciones() {
		LOG.trace( "[SIBBACServiceAliasDirecciones::<constructor>] Initiating SIBBAC Service for \"AliasDirecciones\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {

		LOG.trace( "[SIBBACServiceAliasDirecciones::process] Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.trace( "[SIBBACServiceAliasDirecciones::process] Command.: [{}]", webRequest.getAction() );

		// selecciona la accion
		switch ( webRequest.getAction() ) {
			case Constantes.CMD_CREATE_ALIASDIRECCIONES:

				// comprueba que los parametros han sido enviados
				if ( params == null ) {
					webResponse.setError( "Por favor indique los filtros de búsqueda." );
					return webResponse;
				}

				// Hace el set en el webresponse del resultado de introducir en tabla aliasDirecciones, un nuevo aliasDirecciones
				webResponse.setResultados( aliasDireccionBo.createAliasDirecciones( params ) );
				LOG.trace( "Rellenado el WebResponse con el alta aliasDirecciones" );

				break;

			case Constantes.CMD_MODIFICACION_ALIASDIRECCIONES:

				// comprueba que los parametros han sido enviados
				if ( params == null ) {
					webResponse.setError( "Por favor indique los filtros de búsqueda." );
					return webResponse;
				}

				// HAce el set en el webresponse del resultado de modificar un aliasDirecciones en la tabla aliasDirecciones.

				webResponse.setResultados( aliasDireccionBo.modificacionAliasDirecciones( params ) );
				LOG.trace( "Rellenado el WebResponse con la modificacion del aliasDirecciones" );

				break;

			case Constantes.CMD_GETLISTA_ALIASDIRECCIONES:

				// Hace el set de la devolucion de todos los aliasDirecciones de la tabla aliasDirecciones
				webResponse.setResultados( aliasDireccionBo.getListaAliasDirecciones( params ) );
				LOG.trace( "Rellenado el WebResponse con la lista de aliasDirecciones" );

				break;

			default:

				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );

				break;

		}

		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_CREATE_ALIASDIRECCIONES );
		commands.add( Constantes.CMD_GETLISTA_ALIASDIRECCIONES );
		commands.add( Constantes.CMD_MODIFICACION_ALIASDIRECCIONES );

		return commands;
	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		fields.add( "id" );
		fields.add( "aliasId" );
		fields.add( "calle" );
		fields.add( "numero" );
		fields.add( "bloque" );
		fields.add( "poblacion" );
		fields.add( "codigoPostal" );
		fields.add( "provincia" );
		fields.add( "pais" );

		return fields;
	}
}
