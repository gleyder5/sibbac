package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

public interface HolderTipoOrigenDatosType {

  public final char EXCEL = 'E';
  public final char MANUAL = 'M';
  public final char ROUTING = 'R';
  public final char INSTRUCCIONES = 'I';

  public final String EXCEL_DESCRIPCION = "EXCEL";
  public final String MANUAL_DESCRIPCION = "MANUAL";
  public final String ROUTING_DESCRIPCION = "ROUTING";
  public final String INSTRUCCIONES_DESCRIPCION = "FISCAL";

}
