package sibbac.business.wrappers.rest;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.AliasRecordatoriosBo;
import sibbac.business.wrappers.database.dto.AliasRecordatorioDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceAliasRecordatorio implements SIBBACServiceBean {

	private static final Logger		LOG							= LoggerFactory.getLogger( SIBBACServiceAliasRecordatorio.class );

	@Autowired
	private AliasRecordatoriosBo	aliasRecordatoriosBo;

	private static final String		CMD_GETLISTA_RECORDATORIOS	= "getAliasRecordatorio";
	private static final String		CMD_MODIFICACION_ALIAS		= "modificacionAliasRecordatorio";

	private static final String		ID_ALIAS_RECORDATORIO		= "idAliasRecordatorio";
	private static final String		ID_ALIAS					= "idAlias";
	private static final String		IMPORTE_MINIMO				= "importeMinimo";
	private static final String		FRECUENCIA					= "frecuencia";
	private static final String		RESULTADO					= "resultado";

	public SIBBACServiceAliasRecordatorio() {
		LOG.trace( "[SIBBACServiceAliasRecordatorio::<constructor>] Initiating SIBBAC Service for \"AliasRecordatorio\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {

		LOG.trace( "[SIBBACServiceAliasRecordatorio::process] Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();
		// obtiene comandos(paths)
		LOG.trace( "[SIBBACServiceAliasRecordatorio::process] Command.: [{}]", webRequest.getAction() );

		// selecciona la accion
		switch ( webRequest.getAction() ) {
			case SIBBACServiceAliasRecordatorio.CMD_MODIFICACION_ALIAS:
				this.modificacionAliasRecordatorio( webRequest, webResponse );
				break;

			case SIBBACServiceAliasRecordatorio.CMD_GETLISTA_RECORDATORIOS:
				this.getAliasRecordatorio( webRequest, webResponse );
				break;
			default:
				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;
		}
		return webResponse;
	}

	private void getAliasRecordatorio( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idAlias = getIdAlias( filtros.get( ID_ALIAS ) );
		webResponse.setResultados( aliasRecordatoriosBo.getListaAliasRecordatorio( idAlias ) );
	}

	private void modificacionAliasRecordatorio( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idAliasRecordatorio = getIdAliasRecordatorio( filtros.get( ID_ALIAS_RECORDATORIO ) );
		final Long idAlias = getIdAlias( filtros.get( ID_ALIAS ) );
		final BigDecimal importeMinimo = getImporteMinimo( filtros.get( SIBBACServiceAliasRecordatorio.IMPORTE_MINIMO ) );
		final Integer frecuencia = getFrecuencia( filtros.get( SIBBACServiceAliasRecordatorio.FRECUENCIA ) );
		
		aliasRecordatoriosBo.modificacionAliasRecordatorio( new AliasRecordatorioDTO( idAliasRecordatorio, idAlias, importeMinimo,
				frecuencia ) );
		final Map< String, String > resultados = new HashMap< String, String >();
		resultados.put( RESULTADO, "El recordatorio se ha modificado con éxito" );
		webResponse.setResultados( resultados );
	}

	/**
	 * @param importeMinimoStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private BigDecimal getImporteMinimo( final String importeMinimoStr ) throws SIBBACBusinessException {
		BigDecimal idAliasRecordatorio = null;
		try {
			if ( StringUtils.isNotBlank( importeMinimoStr ) ) {
				idAliasRecordatorio = NumberUtils.createBigDecimal( importeMinimoStr );
			} else {
				throw new SIBBACBusinessException( "El valor de \"importeMinimo\": [" + importeMinimoStr + "] es obligatorio " );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"importeMinimoF\": [" + importeMinimoStr + "] " );
		}
		return idAliasRecordatorio;
	}

	/**
	 * @param frecuenciaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Integer getFrecuencia( final String frecuenciaStr ) throws SIBBACBusinessException {
		Integer idAliasRecordatorio = null;
		try {
			if ( StringUtils.isNotBlank( frecuenciaStr ) ) {
				idAliasRecordatorio = NumberUtils.createInteger( frecuenciaStr );
			} else {
				throw new SIBBACBusinessException( "El valor de \"frecuencia\": [" + frecuenciaStr + "] es obligatorio " );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"frecuenciaF\": [" + frecuenciaStr + "] " );
		}
		return idAliasRecordatorio;
	}

	/**
	 * @param idAliasRecordatorioStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAliasRecordatorio( final String idAliasRecordatorioStr ) throws SIBBACBusinessException {
		Long idAliasRecordatorio = null;
		try {
			if ( StringUtils.isNotBlank( idAliasRecordatorioStr ) ) {
				idAliasRecordatorio = NumberUtils.createLong( idAliasRecordatorioStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAliasRecordatorio\": [" + idAliasRecordatorioStr + "] " );
		}
		return idAliasRecordatorio;
	}

	/**
	 * @param idAliasStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAlias( final String idAliasStr ) throws SIBBACBusinessException {
		Long idAlias = null;
		try {
			if ( StringUtils.isNotBlank( idAliasStr ) ) {
				idAlias = NumberUtils.createLong( idAliasStr );
			} else {
				throw new SIBBACBusinessException( "El valor de \"idAlias\": [" + idAliasStr + "] es obligatorio " );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] " );
		}
		return idAlias;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( SIBBACServiceAliasRecordatorio.CMD_MODIFICACION_ALIAS );
		commands.add( SIBBACServiceAliasRecordatorio.CMD_GETLISTA_RECORDATORIOS );

		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}
}
