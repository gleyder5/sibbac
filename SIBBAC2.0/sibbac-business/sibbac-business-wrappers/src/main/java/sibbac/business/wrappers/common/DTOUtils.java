package sibbac.business.wrappers.common;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.dto.AlcEntregaRecepcionClienteDTO;
import sibbac.business.wrappers.database.dto.DesgloseDTO;
import sibbac.business.wrappers.database.dto.GestionReglasDTO;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.dto.TransaccionesDTO;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0parametrizacion;
import sibbac.business.wrappers.database.model.RecordBean.TipoDocumento;

public class DTOUtils {

  private static final Logger LOG = LoggerFactory.getLogger(DTOUtils.class.getName());

  public static TitularesDTO convertObjectToTitularesDTO(Object[] valuesFromQuery) {

    TitularesDTO tituDTO = new TitularesDTO();

    if (valuesFromQuery[0] instanceof Integer) {
      tituDTO.setId(((Integer) valuesFromQuery[0]).longValue());
    }
    else {
      tituDTO.setId(((BigInteger) valuesFromQuery[0]).longValue());
    }

    tituDTO.setNucnfclt((String) valuesFromQuery[1]);
    tituDTO.setNbooking((String) valuesFromQuery[2]);

    if (valuesFromQuery[3] != null) {
      try {
        tituDTO.setNucnfliq(Short.valueOf(valuesFromQuery[3].toString()));
      }
      catch (NumberFormatException e) {
        // Se ignora
      }
    }

    tituDTO.setNbclient((String) valuesFromQuery[4]);
    tituDTO.setNbclient1((String) valuesFromQuery[5]);
    tituDTO.setNbclient2((String) valuesFromQuery[6]);

    if (valuesFromQuery[7] instanceof String) {
      // tituDTO.setTipdoc(' ');
    }
    else {
      tituDTO.setTipdoc((Character) valuesFromQuery[7]);
    }

    if (valuesFromQuery[8] instanceof String) {
      // tituDTO.setTipopers(' ');
    }
    else {
      tituDTO.setTipopers((Character) valuesFromQuery[8]);
    }

    tituDTO.setNudnicif((String) valuesFromQuery[9]);
    tituDTO.setCcv((String) valuesFromQuery[10]);
    tituDTO.setPaisres((String) valuesFromQuery[11]);

    if (valuesFromQuery[12] instanceof String) {
      // tituDTO.setTipnactit(' ');
    }
    else {
      tituDTO.setTipnactit((Character) valuesFromQuery[12]);
    }

    tituDTO.setTpdomici((String) valuesFromQuery[13]);
    tituDTO.setNbdomici((String) valuesFromQuery[14]);
    tituDTO.setNudomici((String) valuesFromQuery[15]);
    tituDTO.setNureford((String) valuesFromQuery[16]);
    tituDTO.setMercadonacint((Character) valuesFromQuery[17]);
    tituDTO.setCdtpoper((Character) valuesFromQuery[18]);
    tituDTO.setIsin((String) valuesFromQuery[19]);
    tituDTO.setCodpostal((String) valuesFromQuery[20]);
    tituDTO.setPoblacion((String) valuesFromQuery[21]);
    tituDTO.setProvincia((String) valuesFromQuery[22]);

    if (valuesFromQuery[23] instanceof String) {
      // tituDTO.setTiptitu(' ');
    }
    else {
      tituDTO.setTiptitu((Character) valuesFromQuery[23]);
    }

    tituDTO.setNacionalidad((String) valuesFromQuery[24]);

    if (valuesFromQuery[25] instanceof Integer) {
      tituDTO.setParticip(new BigDecimal(String.valueOf(valuesFromQuery[25])));
    }
    else {
      tituDTO.setParticip((BigDecimal) valuesFromQuery[25]);
    }

    tituDTO.setNuorden((String) valuesFromQuery[26]);
    tituDTO.setNutiteje((BigDecimal) valuesFromQuery[27]);
    tituDTO.setAlias((String) valuesFromQuery[28]);
    tituDTO.setNombrealias((String) valuesFromQuery[29]);

    tituDTO.setFechacontratacion((Date) valuesFromQuery[30]);
    return tituDTO;
  }

  public static TitularesDTO convertObjectToTitularesNoIdDTO(Object[] valuesFromQuery) {
    TitularesDTO tituDTO = new TitularesDTO();
    tituDTO.setNucnfclt((String) valuesFromQuery[0]);
    tituDTO.setNbooking((String) valuesFromQuery[1]);

    if (valuesFromQuery[2] != null) {
      // tituDTO.setNucnfliq( ( ( BigDecimal ) valuesFromQuery[ 2 ]
      // ).shortValueExact() );
      tituDTO.setNucnfliq((new BigDecimal(valuesFromQuery[2].toString())).shortValueExact());
    }

    tituDTO.setNbclient((String) valuesFromQuery[3]);
    tituDTO.setNbclient1((String) valuesFromQuery[4]);
    tituDTO.setNbclient2((String) valuesFromQuery[5]);
    tituDTO.setTipdoc((Character) valuesFromQuery[6]);
    tituDTO.setTipopers((Character) valuesFromQuery[7]);
    tituDTO.setNudnicif((String) valuesFromQuery[8]);
    tituDTO.setCcv((String) valuesFromQuery[9]);
    tituDTO.setPaisres((String) valuesFromQuery[10]);
    tituDTO.setTipnactit((Character) valuesFromQuery[11]);

    tituDTO.setTpdomici((String) valuesFromQuery[12]);
    tituDTO.setNbdomici((String) valuesFromQuery[13]);
    tituDTO.setNudomici((String) valuesFromQuery[14]);

    tituDTO.setNureford((String) valuesFromQuery[15]);

    tituDTO.setMercadonacint((Character) valuesFromQuery[16]);
    tituDTO.setCdtpoper((Character) valuesFromQuery[17]);
    tituDTO.setIsin((String) valuesFromQuery[18]);
    tituDTO.setCodpostal((String) valuesFromQuery[19]);
    tituDTO.setPoblacion((String) valuesFromQuery[20]);
    tituDTO.setProvincia((String) valuesFromQuery[21]);
    tituDTO.setTiptitu((Character) valuesFromQuery[22]);
    tituDTO.setNacionalidad((String) valuesFromQuery[23]);

    tituDTO.setParticip((BigDecimal) valuesFromQuery[24]);
    tituDTO.setNuorden((String) valuesFromQuery[25]);
    tituDTO.setNusecuen((BigDecimal) valuesFromQuery[26]);
    tituDTO.setNutiteje((BigDecimal) valuesFromQuery[27]);
    tituDTO.setAlias((String) valuesFromQuery[28]);
    tituDTO.setNombrealias((String) valuesFromQuery[29]);
    if (org.apache.commons.lang.ObjectUtils.equals(tituDTO.getTipdoc(), new Character('B'))) {
      tituDTO.setNudnicif((String) valuesFromQuery[30]);
    }
    tituDTO.setFechacontratacion((Date) valuesFromQuery[31]);
    tituDTO.setDT_RowId((BigInteger) valuesFromQuery[32]);
    return tituDTO;
  }

  public static TransaccionesDTO convertObjectToTransaccionesDTO(Object[] valuesFromQuery) {
    TransaccionesDTO transaDTO = new TransaccionesDTO();
    transaDTO.setNuorden((String) valuesFromQuery[0]);
    transaDTO.setNbooking((String) valuesFromQuery[1]);
    transaDTO.setNucnfclt((String) valuesFromQuery[2]);
    transaDTO.setCdalias((String) valuesFromQuery[3]);
    transaDTO.setFetrade((Date) valuesFromQuery[4]);
    transaDTO.setCdisin((String) valuesFromQuery[5]);
    transaDTO.setNutitcli((BigDecimal) valuesFromQuery[6]);
    transaDTO.setEutotbru((BigDecimal) valuesFromQuery[7]);
    transaDTO.setImgatdvs((BigDecimal) valuesFromQuery[8]);
    transaDTO.setNombrealias((String) valuesFromQuery[9]);
    transaDTO.setFevalor((Date) valuesFromQuery[10]);
    transaDTO.setNbvalors((String) valuesFromQuery[11]);
    transaDTO.setEutotnet((BigDecimal) valuesFromQuery[12]);
    transaDTO.setMercado((String) valuesFromQuery[13]);
    // transaDTO.setNucnfliq( ( String ) valuesFromQuery[ 12 ] );
    return transaDTO;

  }

  public static AlcEntregaRecepcionClienteDTO convertObjectToAlcEntregaRecepcionDTO(Object[] valuesFromQuery) { // Esto
                                                                                                                // en
                                                                                                                // su
                                                                                                                // momento
                                                                                                                // habria
                                                                                                                // que
                                                                                                                // pasarlo
    // a un DTO.

    AlcEntregaRecepcionClienteDTO alcERCDTO = new AlcEntregaRecepcionClienteDTO();

    BigDecimal nucnfliq = (BigDecimal) valuesFromQuery[20];
    String nucnfliqtext = String.valueOf(nucnfliq);

    alcERCDTO.setNombre((String) valuesFromQuery[0]);
    alcERCDTO.setIdestado((Integer) valuesFromQuery[25]);

    alcERCDTO.setNbooking((String) valuesFromQuery[18]);
    alcERCDTO.setNucnfclt((String) valuesFromQuery[19]);
    alcERCDTO.setNucnfliq((Short) Short.parseShort(nucnfliqtext));
    alcERCDTO.setNuorden((String) valuesFromQuery[24]);

    alcERCDTO.setCdtpoper((Character) valuesFromQuery[1]);
    alcERCDTO.setCdalias((String) valuesFromQuery[2]);
    alcERCDTO.setCdisin((String) valuesFromQuery[3]);
    alcERCDTO.setNbvalors((String) valuesFromQuery[4]);
    alcERCDTO.setCdcustod((String) valuesFromQuery[17]);
    alcERCDTO.setDescrali((String) valuesFromQuery[27]);

    alcERCDTO.setNutitliq((BigDecimal) valuesFromQuery[5]);
    alcERCDTO.setImefeagr((BigDecimal) valuesFromQuery[6]);
    alcERCDTO.setImcomisn((BigDecimal) valuesFromQuery[7]);
    alcERCDTO.setImnfiliq((BigDecimal) valuesFromQuery[8]);
    alcERCDTO.setImcanoncompdv((BigDecimal) valuesFromQuery[9]);
    alcERCDTO.setImcanoncompeu((BigDecimal) valuesFromQuery[10]);
    alcERCDTO.setImcanoncontrdv((BigDecimal) valuesFromQuery[11]);
    alcERCDTO.setImcanoncontreu((BigDecimal) valuesFromQuery[12]);
    alcERCDTO.setImcanonliqdv((BigDecimal) valuesFromQuery[13]);
    alcERCDTO.setImcanonliqeu((BigDecimal) valuesFromQuery[14]);
    alcERCDTO.setCdniftit((String) valuesFromQuery[15]);
    alcERCDTO.setNbtitliq((String) valuesFromQuery[16]);
    alcERCDTO.setCdcustod((String) valuesFromQuery[17]);
    alcERCDTO.setCdrefban((String) valuesFromQuery[21]);
    alcERCDTO.setOurpar((String) valuesFromQuery[22]);
    alcERCDTO.setTheirpar((String) valuesFromQuery[23]);
    alcERCDTO.setFeejeliq((Date) valuesFromQuery[26]);

    alcERCDTO.setReferencias3((String) valuesFromQuery[28]);
    alcERCDTO.setPata((String) valuesFromQuery[29]);

    alcERCDTO.setPata("C");

    return alcERCDTO;

  }

  public static DesgloseDTO convertObjectToDesgloseDTO(Object[] valuesFromQuery) {

    DesgloseDTO desglose = DTOUtils.convertObjectToDesgloseHuefanoDTO(valuesFromQuery);
    desglose.setTitulosSV((BigDecimal) valuesFromQuery[26]);
    desglose = convertObjectToDesgloseDTO(valuesFromQuery, desglose);
    return desglose;
  }

  private static DesgloseDTO convertObjectToDesgloseDTO(Object[] valuesFromQuery, DesgloseDTO desglose) {
    if (desglose == null) {
      desglose = new DesgloseDTO();
    }
    Long idTitular = null;
    BigInteger idTitularBigInteger = (BigInteger) valuesFromQuery[27];
    if (idTitularBigInteger != null) {
      idTitular = idTitularBigInteger.longValue();
    }
    desglose.setIdTitular(idTitular); // 31 titulares.id
    // FIXME JONY PANTALLA DESGLOSE PARTENON OP ESPECIAL
    // desglose.setBolsaTitular((String) valuesFromQuery[36]); // 36
    // titulares.BOLSA
    // desglose.setCcvTitular((String) valuesFromQuery[37]); // 37 titulares.ccv
    // desglose.setCodEmprTitular((String) valuesFromQuery[38]); // 38
    // titulares.CODEMPR
    // desglose.setCodBICTitular((String) valuesFromQuery[39]); // 39
    // titulares.CODBIC
    // desglose.setDistritoTitular((String) valuesFromQuery[40]); // 40
    // titulares.DISTRITO
    // desglose.setDomiciliTitular((String) valuesFromQuery[41]); // 41
    // titulares.DOMICILI
    // desglose.setIdSencTitular((BigDecimal) valuesFromQuery[42]); // 42
    // titulares.IDSENC
    // desglose.setIndNacTitular((Character) valuesFromQuery[43]); // 43
    // titulares.INDNAC
    // desglose.setNaclidadTitular((String) valuesFromQuery[44]); // 44
    // titulares.NACLIDAD
    // desglose.setNifBicTitular((String) valuesFromQuery[45]); // 45
    // titulares.NIF_BIC
    // String nomApell = (String) valuesFromQuery[46]; // 46 titulares.NOMAPELL
    // desglose.setNombreTitular("");
    // desglose.setApellido1Titular("");
    // desglose.setApellido2Titular("");
    // desglose.setRazonSocialTitular("");
    // if (nomApell != null && !"".equals(nomApell)) {
    // String[] arrNomApell = nomApell.split("\\*");
    // if (arrNomApell != null && arrNomApell.length > 0) {
    // if (arrNomApell.length > 1) {
    // desglose.setNombreTitular(arrNomApell[0]);
    // desglose.setApellido1Titular(arrNomApell[1]);
    // if (arrNomApell.length > 2) {
    // desglose.setApellido2Titular(arrNomApell[2]);
    // }
    // }// if (arrNomApell.length > 1){
    // else {
    // desglose.setRazonSocialTitular(arrNomApell[0]);
    // }// else
    // }// if (arrNomApell != null && arrNomApell.length > 0){
    // }// if (nomApell != null){
    // desglose.setPaisTitular((String) valuesFromQuery[48]);// 48
    // titulares.PAIS
    // desglose.setPaisReTitular((String) valuesFromQuery[49]);// 49
    // titulares.PAISRE
    // desglose.setParticipTitular((BigDecimal) valuesFromQuery[50]); // 50
    // titulares.PARTICIP
    // desglose.setPoblacioTitular((String) valuesFromQuery[51]); // 51
    // titulares.POBLACIO
    // desglose.setProvinciaTitular((String) valuesFromQuery[52]); // 52
    // titulares.PROVINCIA
    // desglose.setTipoDocTitular((Character) valuesFromQuery[53]); // 53
    // titulares.TIPODOC
    // desglose.setTipperTitular((Character) valuesFromQuery[54]); // 54
    // titulares.TIPPER
    // desglose.setTipTitular((Character) valuesFromQuery[55]); // 55
    // titulares.TIPTITU
    // Long idDesgloseRecordBeanTitular = null;
    // BigInteger idDesgloseRecordBeanTitularInteger = (BigInteger)
    // valuesFromQuery[56];
    // if (idDesgloseRecordBeanTitularInteger != null) {
    // idDesgloseRecordBeanTitular =
    // idDesgloseRecordBeanTitularInteger.longValue();
    // }
    // desglose.setIdDesgloseRecordBeanTitular(idDesgloseRecordBeanTitular); //
    // 56 titulares.ID_DESGLOSE_RECORD_BEAN
    // FIN FIXME
    return desglose;
  }

  public static DesgloseDTO convertObjectToDesgloseHuefanoDTO(Object[] valuesFromQuery) {

    // "select desgloses.ID, desgloses.AUDIT_DATE, desgloses.TIPOREG, desgloses.NIF_BIC, desgloses.CBOEJECU, desgloses.CENTCTVA, "
    // +
    // "desgloses.CODEMPRL, desgloses.CODORGEJ, desgloses.OBCSAVB, desgloses.CODVAISO, desgloses.DENOMENA, desgloses.DPTECTVA, "
    // +
    // "desgloses.EMPRCTVA, desgloses.FEJEC, desgloses.NOMINEJ, desgloses.NUMORDEN, desgloses.NUREFORD, "
    // +
    // "desgloses.ORDCOM, desgloses.PROCESADO, desgloses.REFMOD, desgloses.SEGCLI, desgloses.TIPCAMBI, desgloses.TIPOPERAC, "
    // + "desgloses.TIPSALDO, desgloses.ACCEJEC, T.INFORXML, ord.NUTITORD, " );
    //
    // select.append(
    // "ROW_NUMBER() OVER (ORDER BY desgloses.id DESC) AS row_number  " );

    DesgloseDTO desglose = new DesgloseDTO();
    desglose.setId(((BigInteger) valuesFromQuery[0]).longValue()); // desgloses.ID
    desglose.setTitular((String) valuesFromQuery[3]); // desgloses.NIF_BIC
    desglose.setPrecio((BigDecimal) valuesFromQuery[4]);// desgloses.CBOEJECU
    String centctva = (String) valuesFromQuery[5];// desgloses.CENTCTVA
    desglose.setEntidad((String) valuesFromQuery[6]);// desgloses.CODEMPRL
    desglose.setBolsa((String) valuesFromQuery[7]);// desgloses.CODORGEJ
    desglose.setCodsv((String) valuesFromQuery[8]);// desgloses.OBCSAVB
    desglose.setIsin((String) valuesFromQuery[9]);// desgloses.CODVAISO
    desglose.setDesIsin((String) valuesFromQuery[10]);// desgloses.DENOMENA
    String dptectva = (String) valuesFromQuery[11];// desgloses.DPTECTVA
    String emprctva = (String) valuesFromQuery[12];// desgloses.EMPRCTVA
    desglose.setEmpConVal(emprctva);
    desglose.setNumCtoOficina(centctva);
    desglose.setNumCtrVal(dptectva);
    desglose.setCCV(emprctva + "/" + centctva + "/" + dptectva);
    desglose.setFejecucion((Date) valuesFromQuery[13]);// desgloses.FEJEC
    desglose.setNominal((BigDecimal) valuesFromQuery[14]);// desgloses.NOMINEJ
    String numOrden = (String) valuesFromQuery[15];
    if (numOrden != null) {
      numOrden = numOrden.trim();
    }
    desglose.setNumOrden(numOrden);// desgloses.NUMORDEN
    desglose.setNumReferencia((String) valuesFromQuery[16]);// desgloses.NUREFORD
    Object objOrdCom = valuesFromQuery[17];
    String sOrdCom = null;
    if (objOrdCom != null) {
      sOrdCom = objOrdCom.toString();
    }
    desglose.setOrdCom(sOrdCom);// desgloses.OrdCom
    desglose.setValidado(((Short) valuesFromQuery[18]).intValue());// desgloses.PROCESADO
    String sProcesado = "KO";
    if (desglose.getValidado() != null && 1 < desglose.getValidado()) {
      sProcesado = "OK";
    }// if (desglose.getValidado() == 2)
    desglose.setProcesado(sProcesado);
    desglose.setSegmento(valuesFromQuery[20].toString());// desgloses.SEGCLI
    desglose.setTipoCambio(valuesFromQuery[21].toString().charAt(0));// desgloses.TIPCAMBI
    desglose.setTipoOperacion(valuesFromQuery[22].toString().charAt(0));// desgloses.TIPOPERAC
    desglose.setTipoSaldo(valuesFromQuery[23].toString().charAt(0));// desgloses.TIPSALDO
    desglose.setTitulos((BigDecimal) valuesFromQuery[24]);// desgloses.ACCEJEC
    desglose.setBolsaDescripcion((String) valuesFromQuery[25]);// T.INFORXML
    return desglose;
  }

  public static GestionReglasDTO convertObjectGestionReglassDTO(Object[] valuesFromQuery) {
    GestionReglasDTO regla = new GestionReglasDTO();
    try {

      regla.setId((BigInteger) valuesFromQuery[0]);
      regla.setCd_codigo(valuesFromQuery[1].toString());
      regla.setNb_descripcion(valuesFromQuery[2].toString());
      regla.setFecha_creacion((Date) valuesFromQuery[3]);
      regla.setActivo(valuesFromQuery[4].toString());
      regla.setnParametrizacionesActivas((int) valuesFromQuery[5]);
      regla.setnAliasAsociados((int) valuesFromQuery[6]);
      regla.setnSubcuentasAsociadas((int) valuesFromQuery[7]);
    }
    catch (Exception ex) {
      LOG.error("[convertObjectGestionReglassDTO] Error no esperado", ex);
    }
    return regla;
  }

  public static Tmct0parametrizacion convertObjectParametrizacionTransaccionesDTO(Object valuesFromQuery) {

    Tmct0parametrizacion transaParametri = new Tmct0parametrizacion();

    BigInteger nl = (BigInteger) valuesFromQuery;
    Tmct0Regla tmct0Regla = new Tmct0Regla();

    if (nl != null) {
      tmct0Regla.setIdRegla(nl);
      transaParametri.setTmct0Regla(tmct0Regla);
    }

    // query, idRegla, camaraCompensacion, cuentaCompensacion,
    // miembroCompensador, referenciaAsignacion,
    // nemotecnico, segmento, referenciaCliente, referenciaExterna,
    // capacidad, modeloNegocio

    // transaParametri.getIdRegla( ( ( BigInteger ) valuesFromQuery[ 0 ]
    // ).longValue() );

    // transaReglaDTO.setIdRegla( ( ( BigInteger ) valuesFromQuery[ 0 ]
    // ).longValue() );
    // transaReglaDTO.setCdCodigo((String)valuesFromQuery[1]);
    // transaReglaDTO.setNbDescripcion((String)valuesFromQuery[2]);
    // transaReglaDTO.setFechaCreacion((Date)valuesFromQuery[3]);

    return transaParametri;
  }

  public static Tmct0Regla convertObjectReglaTransaccionesDTO(Object[] valuesFromQuery) {

    Tmct0Regla transaReglaDTO = new Tmct0Regla();
    transaReglaDTO.setIdRegla(((BigInteger) valuesFromQuery[0]));
    transaReglaDTO.setCdCodigo((String) valuesFromQuery[1]);
    transaReglaDTO.setNbDescripcion((String) valuesFromQuery[2]);
    transaReglaDTO.setFechaCreacion((Date) valuesFromQuery[3]);
    return transaReglaDTO;
  }

  public static Tmct0parametrizacionDTO convertObjectToParametrizacionesDTO(Tmct0parametrizacion valuesFromQuery) {
    Tmct0parametrizacionDTO parametro = new Tmct0parametrizacionDTO();
    parametro.setIdParametrizacion(valuesFromQuery.getIdParametrizacion());

    if (valuesFromQuery.getDescripcion() != null && valuesFromQuery.getDescripcion() != "") {
      parametro.setDescripcionParametri(valuesFromQuery.getDescripcion());
    }

    // camara compensación
    if (valuesFromQuery.getTmct0CamaraCompensacion() != null
        && valuesFromQuery.getTmct0CamaraCompensacion().getCdCodigo() != ""
        && valuesFromQuery.getTmct0CamaraCompensacion().getCdCodigo() != null) {
      parametro.setCd_codigoCamaraComp(valuesFromQuery.getTmct0CamaraCompensacion().getCdCodigo());
    }

    if (valuesFromQuery.getTmct0CamaraCompensacion() != null
        && valuesFromQuery.getTmct0CamaraCompensacion().getNbDescripcion() != ""
        && valuesFromQuery.getTmct0CamaraCompensacion().getNbDescripcion() != null) {
      parametro.setNb_descripcionCamaraComp(valuesFromQuery.getTmct0CamaraCompensacion().getNbDescripcion());
    }

    if (valuesFromQuery.getTmct0CamaraCompensacion() != null
        && valuesFromQuery.getTmct0CamaraCompensacion().getIdCamaraCompensacion() != 0) {
      parametro.setIdCamaraComp(valuesFromQuery.getTmct0CamaraCompensacion().getIdCamaraCompensacion());
    }

    // cuenta de compensación
    if (valuesFromQuery.getTmct0CuentasDeCompensacion() != null
        && valuesFromQuery.getTmct0CuentasDeCompensacion().getCdCodigo() != ""
        && valuesFromQuery.getTmct0CuentasDeCompensacion().getCdCodigo() != null) {
      parametro.setCd_codigoCuentaCompensacion(valuesFromQuery.getTmct0CuentasDeCompensacion().getCdCodigo());
    }

    if (valuesFromQuery.getTmct0CuentasDeCompensacion() != null
        && valuesFromQuery.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion() != 0) {
      parametro.setIdCuentaCompensacion(valuesFromQuery.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion());
    }

    // miembro compensador
    if (valuesFromQuery.getTmct0Compensador() != null && valuesFromQuery.getTmct0Compensador().getNbDescripcion() != ""
        && valuesFromQuery.getTmct0Compensador().getNbDescripcion() != null) {
      parametro.setNb_descripcionCompensador(valuesFromQuery.getTmct0Compensador().getNbDescripcion());
    }

    if (valuesFromQuery.getTmct0Compensador() != null && valuesFromQuery.getTmct0Compensador().getIdCompensador() != 0) {
      parametro.setIdCompensador(valuesFromQuery.getTmct0Compensador().getIdCompensador());
    }

    if (valuesFromQuery.getCdReferenciaAsignacion() != null && valuesFromQuery.getCdReferenciaAsignacion() != "") {
      parametro.setReferenciaAsignacion(valuesFromQuery.getCdReferenciaAsignacion());
    }

    // nemotécnico
    if (valuesFromQuery.getTmct0Nemotecnicos() != null && valuesFromQuery.getTmct0Nemotecnicos().getNbnombre() != ""
        && valuesFromQuery.getTmct0Nemotecnicos().getNbnombre() != null) {
      parametro.setNb_nombreNmotecnico(valuesFromQuery.getTmct0Nemotecnicos().getNbnombre());
    }

    if (valuesFromQuery.getTmct0Nemotecnicos() != null
        && valuesFromQuery.getTmct0Nemotecnicos().getId_nemotecnico() != 0) {
      parametro.setIdNemotecnico(valuesFromQuery.getTmct0Nemotecnicos().getId_nemotecnico());
    }

    // segmento
    if (valuesFromQuery.getTmct0Segmento() != null && valuesFromQuery.getTmct0Segmento().getCd_codigo() != ""
        && valuesFromQuery.getTmct0Segmento().getCd_codigo() != null) {
      parametro.setCd_codigoSegmento(valuesFromQuery.getTmct0Segmento().getCd_codigo());
    }

    if (valuesFromQuery.getTmct0Segmento() != null && valuesFromQuery.getTmct0Segmento().getNbDescripcion() != ""
        && valuesFromQuery.getTmct0Segmento().getNbDescripcion() != null) {
      parametro.setNb_descripcionSegmento(valuesFromQuery.getTmct0Segmento().getNbDescripcion());
    }

    if (valuesFromQuery.getTmct0Segmento() != null && valuesFromQuery.getTmct0Segmento().getId_segmento() != 0) {
      parametro.setIdSegmento(valuesFromQuery.getTmct0Segmento().getId_segmento());
    }

    if (valuesFromQuery.getReferenciaCliente() != null && valuesFromQuery.getReferenciaCliente() != "") {
      parametro.setReferenciaCliente(valuesFromQuery.getReferenciaCliente());
    }

    if (valuesFromQuery.getReferenciaExterna() != null && valuesFromQuery.getReferenciaExterna() != "") {
      parametro.setReferenciaExterna(valuesFromQuery.getReferenciaExterna());
    }

    // capacidad
    if (valuesFromQuery.getTmct0IndicadorCapacidad() != null
        && valuesFromQuery.getTmct0IndicadorCapacidad().getCd_codigo() != ""
        && valuesFromQuery.getTmct0IndicadorCapacidad().getCd_codigo() != null) {
      parametro.setCd_codigoIndicCapacidad(valuesFromQuery.getTmct0IndicadorCapacidad().getCd_codigo());
    }

    if (valuesFromQuery.getTmct0IndicadorCapacidad() != null
        && valuesFromQuery.getTmct0IndicadorCapacidad().getDescripcion() != ""
        && valuesFromQuery.getTmct0IndicadorCapacidad().getDescripcion() != null) {
      parametro.setNb_descripcionIndicCapacidad(valuesFromQuery.getTmct0IndicadorCapacidad().getDescripcion());
    }

    if (valuesFromQuery.getTmct0IndicadorCapacidad() != null
        && valuesFromQuery.getTmct0IndicadorCapacidad().getId_indicador_capacidad() != 0) {
      parametro.setIdCapacidad(valuesFromQuery.getTmct0IndicadorCapacidad().getId_indicador_capacidad());
    }

    // modelo de negocio
    if (valuesFromQuery.getTmct0ModeloDeNegocio() != null
        && valuesFromQuery.getTmct0ModeloDeNegocio().getCd_codigo() != ""
        && valuesFromQuery.getTmct0ModeloDeNegocio().getCd_codigo() != null) {
      parametro.setCd_codigoModeloNegocio(valuesFromQuery.getTmct0ModeloDeNegocio().getCd_codigo());
    }

    if (valuesFromQuery.getTmct0ModeloDeNegocio() != null
        && valuesFromQuery.getTmct0ModeloDeNegocio().getNbDescripcion() != ""
        && valuesFromQuery.getTmct0ModeloDeNegocio().getNbDescripcion() != null) {
      parametro.setNb_descripcionModeloNegocio(valuesFromQuery.getTmct0ModeloDeNegocio().getNbDescripcion());
    }

    if (valuesFromQuery.getTmct0ModeloDeNegocio() != null
        && valuesFromQuery.getTmct0ModeloDeNegocio().getId_modelo_de_negocio() != 0) {
      parametro.setIdModeloNegocio(valuesFromQuery.getTmct0ModeloDeNegocio().getId_modelo_de_negocio());
    }

    if (valuesFromQuery.getFecha_fin() != null) {
      parametro.setFecha_fin(valuesFromQuery.getFecha_fin());
    }

    return parametro;
  }

}
