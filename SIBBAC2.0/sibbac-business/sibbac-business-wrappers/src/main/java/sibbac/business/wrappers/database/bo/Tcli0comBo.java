package sibbac.business.wrappers.database.bo;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.dao.Tcli0comDao;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnbpsql.tcli.model.Tcli0com;
import sibbac.database.bsnbpsql.tcli.model.Tcli0comId;

@Service
public class Tcli0comBo extends AbstractBo<Tcli0com, Tcli0comId, Tcli0comDao> {

  @Autowired
  private Tmct0dbsBo dbsBo;

  public Tcli0comBo() {
  }

  public Tcli0comDTO findTcli0comByCdproducAndNuctapro(String cdproduc, int cdaliass) {
    return dao.findAllTcli0comByCdproducAndNuctapro(cdproduc, cdaliass);
  }

  public Tcli0comDTO findTcli0comByCdproducAndNuctaproCached(Map<String, Tcli0comDTO> cachedValues, String cdproduc,
      int cdaliass) {
    cdproduc = StringUtils.trim(cdproduc);
    String key = getKeyMap(cdproduc, cdaliass);
    Tcli0comDTO value = cachedValues.get(key);
    if (value == null) {
      synchronized (cachedValues) {
        value = cachedValues.get(key);
        if (value == null) {
          value = findTcli0comByCdproducAndNuctapro(cdproduc, cdaliass);
          cachedValues.put(key, value);
        }
      }

    }
    return value;
  }

  private String getKeyMap(String cdproduc, int cdaliass) {
    return String.format("%s_%s", cdproduc, cdaliass);
  }
}
