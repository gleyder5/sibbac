package sibbac.business.wrappers.common;

/**
 * DTO para cargar las listas en la vista con angular
 * 
 * @author mario.flores
 *
 */
public class SelectDTO {

  /**
   * key - Clave del elemento
   */
  private String key;
  /**
   * Value - Valor del elemento
   */
  private String value;

  /**
   * description - Descripcion adicional
   */
  private String description;

  public SelectDTO(String key, String value) {

    this.key = key;
    this.value = value;
  }

  public SelectDTO(String key, String value, String description) {
    this.key = key;
    this.value = value;
    this.description = description;
  }

  public SelectDTO(Long id, String descripcion) {
	this.key = String.valueOf(id);
	this.value = descripcion;
	this.description = descripcion;
  }

public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((key == null) ? 0 : key.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SelectDTO other = (SelectDTO) obj;
    if (description == null) {
      if (other.description != null)
        return false;
    } else if (!description.equals(other.description))
      return false;
    if (key == null) {
      if (other.key != null)
        return false;
    } else if (!key.equals(other.key))
      return false;
    if (value == null) {
      if (other.value != null)
        return false;
    } else if (!value.equals(other.value))
      return false;
    return true;
  }

}
