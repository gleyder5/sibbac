package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tgrl.model.Tgrl0dir;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0dirId;

@Repository
public interface Tgrl0dirDao extends JpaRepository<Tgrl0dir, Tgrl0dirId> {

  @Query("select max(n.id.nusecnbr) from Tgrl0dir n where n.id.nuoprout = :nuoprout and n.id.nupareje = :nupareje")
  Short maxNusecnbrByNuoproutAndNupareje(@Param("nuoprout") int nuoprout, @Param("nupareje") short nupareje);

  @Query("select n from Tgrl0dir n where n.id.nuoprout = :nuoprout and n.id.nupareje = :nupareje and n.fhbajano = 0")
  List<Tgrl0dir> findAllByNuoproutAndNuparejeAndActivo(@Param("nuoprout") int nuoprout,
      @Param("nupareje") short nupareje);

}
