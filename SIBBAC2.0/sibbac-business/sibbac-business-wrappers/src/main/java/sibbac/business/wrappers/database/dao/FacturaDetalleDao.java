package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.FacturaDetalle;


@Component
public interface FacturaDetalleDao extends JpaRepository< FacturaDetalle, Long > {

	@Query( "SELECT FD FROM FacturaDetalle FD WHERE FD.facturaSugerida.id =:ID_FACTURASUGERIDA" )
	public FacturaDetalle findFacturaDetalleByID_FACTURASUGERIDA( @Param( "ID_FACTURASUGERIDA" ) Long facturaSugeridaId );

}
