package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0IndicadorDeCapacidad;
import sibbac.business.wrappers.database.model.Tmct0ModeloDeNegocio;


@Repository
public interface Tmct0ModeloDeNegocioDao extends JpaRepository<Tmct0ModeloDeNegocio, Long>{
	
	@Query("SELECT modelo FROM Tmct0ModeloDeNegocio modelo WHERE modelo.nbDescripcion=:nbDescripcion ")
	public Tmct0ModeloDeNegocio findByNbDescripcion(@Param("nbDescripcion") String nbDescripcion );

}
