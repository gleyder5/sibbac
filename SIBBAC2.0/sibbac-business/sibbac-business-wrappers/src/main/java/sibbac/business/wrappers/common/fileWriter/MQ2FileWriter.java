package sibbac.business.wrappers.common.fileWriter;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;


//@Component
public class MQ2FileWriter {

	protected static final Logger	LOG	= LoggerFactory.getLogger( MQ2FileWriter.class );
	private Boolean					isForServer	= true;

	public void writeIntoFile( String queueConnectionFactoryName, String queueName, String fileName, String filePath )
			throws SIBBACBusinessException {
		LOG.error( "[MQ2FileWriter :: writeIntoFile] Init (QueueConnectionFactory=" + queueConnectionFactoryName + ", QueueName="
				+ queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")" );
		String urlString = filePath + "/" + fileName;
		URL url;
		try {
			url = new URL(urlString);
			File file = new File(url.getPath());
			file.getParentFile().mkdirs();
			try {
				if ( !file.exists() ) {
					file.createNewFile();
				}
			} catch ( IOException e1 ) {
				LOG.error( "[MQ2FileWriter :: writeIntoFile] Error abriendo fichero " + filePath + "/" + fileName );
			}

			PrintStream printStream = null;
			try {
				printStream = new PrintStream( new FileOutputStream( file, true ) );
			} catch ( IOException e1 ) {
				LOG.error( "[MQ2FileWriter :: writeIntoFile] Error leyendo fichero " + e1.getMessage() );
			}
			QueueConnectionFactory queueConnectionFactory = null;
			QueueConnection queueConnection = null;
			Queue queue = null;
			QueueSession queueSession = null;
			QueueReceiver messageConsummer = null;
			Context ctx;
			try {
				String initialContext = "java:comp/env";
				if ( isForServer ) {
					LOG.debug( "[MQ2FileWriter :: writeIntoFile] Mirando el contexto inicial sin usar: " + initialContext );
					ctx = ( Context ) new InitialContext();
				} else {
					LOG.debug( "[MQ2FileWriter :: writeIntoFile] Mirando el contexto inicial usando: " + initialContext );
					ctx = ( Context ) new InitialContext().lookup( initialContext );
				}

				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Obteniendo recurso JNDI QueueConnectionFactory=" + queueConnectionFactoryName );
				queueConnectionFactory = ( QueueConnectionFactory ) ctx.lookup( queueConnectionFactoryName );
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Obteniendo recurso JNDI Queue=" + queueName );
				queue = ( Queue ) ctx.lookup( queueName );
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Se va a crear la conexión" );
				queueConnection = ( QueueConnection ) queueConnectionFactory.createQueueConnection();
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Conexión creada, se va a crear la sesión" );
				queueSession = ( QueueSession ) queueConnection.createQueueSession( true, Session.AUTO_ACKNOWLEDGE );
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Sesión creada, se va a crear el consumidor" );
				messageConsummer = ( QueueReceiver ) queueSession.createReceiver( queue );
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Consumidor creado, se le da comienzo a la conexión" );
				queueConnection.start();
				Integer receivedLines = receiveLines( queueSession, messageConsummer, printStream );
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Recibidas " + receivedLines + " y copiadas al fichero" );
			} catch ( Exception e ) {
				LOG.error( "[MQ2FileWriter :: writeIntoFile] Error escribiendo en fichero " + e.getMessage() );
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] -> ERROR: ", e );
			} finally {
				try {
					if ( messageConsummer != null ) {
						messageConsummer.close();
					}
				} catch ( Exception e ) {
					LOG.error( "[MQ2FileWriter :: writeIntoFile] Error cerrando el productor: " + e.getMessage() );
				}
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Productor cerrado, se va a cerrar la sesión" );

				try {
					if ( queueSession != null ) {
						queueSession.close();
					}
				} catch ( Exception e ) {
					LOG.error( "[MQ2FileWriter :: writeIntoFile] Error cerrando la sesión: " + e.getMessage() );
				}
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Sesión cerrada, se va a cerrar la conexión" );
				try {
					if ( queueConnection != null ) {
						queueConnection.close();
						LOG.debug( "[MQ2FileWriter :: writeIntoFile] Conexión cerrada" );
					}
				} catch ( Exception e ) {
					LOG.error( "[MQ2FileWriter :: writeIntoFile] Error cerrando la conexión: " + e.getMessage() );
				}
				try {
					if ( printStream != null ) {
						printStream.close();
						LOG.debug( "[MQ2FileWriter :: writeIntoFile] Stream de escritura en el fichero cerrado" );
					}
				} catch ( Exception e ) {
					LOG.error( "[MQ2FileWriter :: writeIntoFile] Error cerrando el Stream de escritura en el fichero: " + e.getMessage() );
				}
			}

		} catch ( MalformedURLException e2 ) {
			LOG.error( "[MQ2FileWriter :: writeIntoFile] Error en la creacion del fichero MalformedURLException: " + e2.getMessage() );
		}
	}

	private Integer receiveLines( QueueSession queueSession, QueueReceiver messageConsummer, PrintStream printStream ) {
		Integer receivedLines = 0;
		TextMessage received = null;
		try {

			while ( true ) {
				received = ( TextMessage ) messageConsummer.receiveNoWait();
				if ( received == null ) {
					break;
				}
				String text = received.getText();
				LOG.debug( "[MQ2FileWriter :: writeIntoFile] Se ha recibido: " + text );
				printStream.println( text );
				queueSession.commit();
				receivedLines++;
			}
		} catch ( Exception e ) {
			LOG.error( "[MQ2FileWriter :: writeIntoFile] Error leyendo línea" + receivedLines );
			return receivedLines;
		}

		return receivedLines;
	}

}
