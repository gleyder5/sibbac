package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tgrl.model.Tgrl0nom;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0nomId;

@Repository
public interface Tgrl0nomDao extends JpaRepository<Tgrl0nom, Tgrl0nomId> {

}
