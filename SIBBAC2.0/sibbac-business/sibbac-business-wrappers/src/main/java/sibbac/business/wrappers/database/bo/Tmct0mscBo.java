package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.model.Tmct0mscId;
import sibbac.business.wrappers.database.dao.Tmct0mscDao;
import sibbac.business.wrappers.database.model.Tmct0msc;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0mscBo extends AbstractBo< Tmct0msc, Tmct0mscId, Tmct0mscDao > {

	public Tmct0msc save( Tmct0msc _tmct0msc ) {
		return dao.save( _tmct0msc );
	}

}
