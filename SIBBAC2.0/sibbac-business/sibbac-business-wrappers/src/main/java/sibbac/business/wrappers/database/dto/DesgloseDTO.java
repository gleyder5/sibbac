package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class DesgloseDTO {

    private Long id;
    private Integer validado;
    private String numOrden;
    private String numReferencia;
    private List<DesgloseDTO> listaDesgloses;
    private List<DesgloseDTO> listaTitulares;
    private String codsv;
    private Date fejecucion;
    private Character tipoOperacion;// 2 = V, C = 1
    private BigDecimal titulos;
    private BigDecimal titulosSV;
    private BigDecimal nominal;
    private String isin;
    private String desIsin;
    private String bolsa;
    private Character tipoSaldo;
    private String entidad;
    private Character tipoCambio;
    private String CCV;
    private String procesado;
    private Boolean bOkFechaEjecucion;
    private Boolean bOkBolsa;
    private Boolean bOkTipoOperacion;
    private Boolean bOkIsin;
    private Boolean bOkTipoCambio;
    private Boolean bOkTipoSaldo;
    private BigDecimal precio;
    private String titular;
    private String empConVal;
    private String numCtoOficina;
    private String numCtrVal;
    private String ordCom;
    private String segmento;
    private String bolsaDescripcion;
    // titulares.
    private Long idTitular;
    private String bolsaTitular;
    private String ccvTitular;
    private String codEmprTitular;
    private String codBICTitular;
    private String distritoTitular;
    private String domiciliTitular;
    private BigDecimal idSencTitular;
    private Character indNacTitular;
    private String naclidadTitular;
    private String nifBicTitular;
    private String nombreTitular;
    private String apellido1Titular;
    private String apellido2Titular;
    private String razonSocialTitular;
    private String paisTitular;
    private String paisReTitular;
    private BigDecimal participTitular;
    private String poblacioTitular;
    private String provinciaTitular;
    private Character tipoDocTitular;
    private Character tipperTitular;
    private Character tipTitular;
    private Long idDesgloseRecordBeanTitular;

    public DesgloseDTO getTitularDto(DesgloseDTO desglose) {
	this.numOrden = desglose.numOrden;
	this.idTitular = desglose.idTitular;
	this.bolsaTitular = desglose.bolsaTitular;
	this.ccvTitular = desglose.ccvTitular;
	this.codEmprTitular = desglose.codEmprTitular;
	this.codBICTitular = desglose.codBICTitular;
	this.distritoTitular = desglose.distritoTitular;
	this.domiciliTitular = desglose.domiciliTitular;
	this.idSencTitular = desglose.idSencTitular;
	this.indNacTitular = desglose.indNacTitular;
	this.naclidadTitular = desglose.naclidadTitular;
	this.nifBicTitular = desglose.nifBicTitular;
	this.nombreTitular = desglose.nombreTitular;
	this.apellido1Titular = desglose.apellido1Titular;
	this.apellido2Titular = desglose.apellido2Titular;
	this.razonSocialTitular = desglose.razonSocialTitular;
	this.paisTitular = desglose.paisTitular;
	this.paisReTitular = desglose.paisReTitular;
	this.participTitular = desglose.participTitular;
	this.poblacioTitular = desglose.poblacioTitular;
	this.provinciaTitular = desglose.provinciaTitular;
	this.tipoDocTitular = desglose.tipoDocTitular;
	this.tipperTitular = desglose.tipperTitular;
	this.tipTitular = desglose.tipTitular;
	this.idDesgloseRecordBeanTitular = desglose.idDesgloseRecordBeanTitular;
	return this;
    }

    public DesgloseDTO() {
	titulos = BigDecimal.ZERO;
	titulosSV = BigDecimal.ZERO;
	nominal = BigDecimal.ZERO;
	bOkFechaEjecucion = true;
	bOkBolsa = true;
	bOkTipoOperacion = true;
	bOkIsin = true;
	bOkTipoCambio = true;
	bOkTipoSaldo = true;
	procesado = "KO";
    }

    public DesgloseDTO(DesgloseDTO desglose) {
	this();
	this.setId(desglose.getId());
	this.setTitular(desglose.getTitular());
	this.setPrecio(desglose.getPrecio());
	this.setEntidad(desglose.getEntidad());
	this.setBolsa(desglose.getBolsa());
	this.setCodsv(desglose.getCodsv());
	this.setIsin(desglose.getIsin());
	this.setDesIsin(desglose.getDesIsin());
	this.setCCV(desglose.getCCV());
	this.setFejecucion(desglose.getFejecucion());
	this.setNominal(desglose.getNominal());
	this.setNumOrden(desglose.getNumOrden());
	this.setNumReferencia(desglose.getNumReferencia());
	this.setValidado(desglose.getValidado());
	this.setProcesado(desglose.getProcesado());
	this.setTipoCambio(desglose.getTipoCambio());
	this.setTipoOperacion(desglose.getTipoOperacion());
	this.setTipoSaldo(desglose.getTipoSaldo());
	this.setTitulos(desglose.getTitulos());
	this.setTitulosSV(desglose.getTitulosSV());
	this.setBolsaDescripcion(desglose.getBolsaDescripcion());
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getValidado() {
	return validado;
    }

    public void setValidado(Integer validado) {
	this.validado = validado;
    }

    public String getNumOrden() {
	return numOrden;
    }

    public void setNumOrden(String numOrden) {
	this.numOrden = numOrden;
    }

    public String getNumReferencia() {
	return numReferencia;
    }

    public void setNumReferencia(String numReferencia) {
	this.numReferencia = numReferencia;
    }

    public List<DesgloseDTO> getListaDesgloses() {
	return listaDesgloses;
    }

    public void setListaDesgloses(List<DesgloseDTO> listaDesgloses) {
	this.listaDesgloses = listaDesgloses;
    }

    public List<DesgloseDTO> getListaTitulares() {
	return listaTitulares;
    }

    public void setListaTitulares(List<DesgloseDTO> listaTitulares) {
	this.listaTitulares = listaTitulares;
    }

    public String getCodsv() {
	return codsv;
    }

    public void setCodsv(String codsv) {
	this.codsv = codsv;
    }

    public Date getFejecucion() {
	return fejecucion;
    }

    public void setFejecucion(Date fejecucion) {
	this.fejecucion = fejecucion;
    }

    public Character getTipoOperacion() {
	if (tipoOperacion != '2') {
	    tipoOperacion = '1';
	}
	return tipoOperacion;
    }

    public void setTipoOperacion(Character tipoOperacion) {
	this.tipoOperacion = tipoOperacion;
    }

    public BigDecimal getTitulos() {
	return titulos;
    }

    public void setTitulos(BigDecimal titulos) {
	this.titulos = titulos;
    }

    public BigDecimal getTitulosSV() {
	return titulosSV;
    }

    public void setTitulosSV(BigDecimal titulosSV) {
	this.titulosSV = titulosSV;
    }

    public BigDecimal getNominal() {
	return nominal;
    }

    public void setNominal(BigDecimal nominal) {
	this.nominal = nominal;
    }

    public String getIsin() {
	return isin;
    }

    public void setIsin(String isin) {
	this.isin = isin;
    }

    public String getDesIsin() {
	return desIsin;
    }

    public void setDesIsin(String desIsin) {
	this.desIsin = desIsin;
    }

    public String getBolsa() {
	return bolsa;
    }

    public void setBolsa(String bolsa) {
	this.bolsa = bolsa;
    }

    public Character getTipoSaldo() {
	return tipoSaldo;
    }

    public void setTipoSaldo(Character tipoSaldo) {
	this.tipoSaldo = tipoSaldo;
    }

    public String getEntidad() {
	return entidad;
    }

    public void setEntidad(String entidad) {
	this.entidad = entidad;
    }

    public Character getTipoCambio() {
	if (tipoCambio != '2') {
	    tipoCambio = '1';
	}
	return tipoCambio;
    }

    public void setTipoCambio(Character tipoCambio) {
	this.tipoCambio = tipoCambio;
    }

    public String getCCV() {
	return CCV;
    }

    public void setCCV(String cCV) {
	CCV = cCV;
    }

    public String getProcesado() {
	return procesado;
    }

    public void setProcesado(String procesado) {
	this.procesado = procesado;
    }

    public Boolean getBOkFechaEjecucion() {
	return bOkFechaEjecucion;
    }

    public void setBOkFechaEjecucion(Boolean bOkFechaEjecucion) {
	this.bOkFechaEjecucion = bOkFechaEjecucion;
    }

    public Boolean getBOkBolsa() {
	return bOkBolsa;
    }

    public void setBOkBolsa(Boolean bOkBolsa) {
	this.bOkBolsa = bOkBolsa;
    }

    public Boolean getBOkTipoOperacion() {
	return bOkTipoOperacion;
    }

    public void setBOkTipoOperacion(Boolean bOkTipoOperacion) {
	this.bOkTipoOperacion = bOkTipoOperacion;
    }

    public Boolean getBOkIsin() {
	return bOkIsin;
    }

    public void setBOkIsin(Boolean bOkIsin) {
	this.bOkIsin = bOkIsin;
    }

    public Boolean getBOkTipoCambio() {
	return bOkTipoCambio;
    }

    public void setBOkTipoCambio(Boolean bOkTipoCambio) {
	this.bOkTipoCambio = bOkTipoCambio;
    }

    public Boolean getBOkTipoSaldo() {
	return bOkTipoSaldo;
    }

    public void setBOkTipoSaldo(Boolean bOkTipoSaldo) {
	this.bOkTipoSaldo = bOkTipoSaldo;
    }

    public BigDecimal getPrecio() {
	return this.precio;
    }

    public void setPrecio(BigDecimal precio) {
	this.precio = precio;
    }

    public String getTitular() {
	return titular;
    }

    public void setTitular(String titular) {
	this.titular = titular;
    }

    public String getEmpConVal() {
	return empConVal;
    }

    public void setEmpConVal(String empConVal) {
	this.empConVal = empConVal;
    }

    public String getNumCtoOficina() {
	return numCtoOficina;
    }

    public void setNumCtoOficina(String numCtoOficina) {
	this.numCtoOficina = numCtoOficina;
    }

    public String getNumCtrVal() {
	return numCtrVal;
    }

    public void setNumCtrVal(String numCtrVal) {
	this.numCtrVal = numCtrVal;
    }

    public String getOrdCom() {
	return ordCom;
    }

    public void setOrdCom(String ordCom) {
	this.ordCom = ordCom;
    }

    public String getSegmento() {
	return segmento;
    }

    public void setSegmento(String segmento) {
	this.segmento = segmento;
    }

    public String getBolsaDescripcion() {
	return bolsaDescripcion;
    }

    public void setBolsaDescripcion(String bolsaDescripcion) {
	this.bolsaDescripcion = bolsaDescripcion;
    }

    public HashMap<String, Object> getHashMapExport(
	    HashMap<String, Object> result) {

	if (result == null) {
	    result = new HashMap<String, Object>();
	}
	result.put("numOrden", this.numOrden);
	List<DesgloseDTO> listaDesgloses = this.getListaDesgloses();
	if (listaDesgloses != null) {
	    Iterator<DesgloseDTO> iterator = listaDesgloses.iterator();
	    List<HashMap<String, Object>> listaHashMapDesgloses = new ArrayList<HashMap<String, Object>>();
	    while (iterator.hasNext()) {
		DesgloseDTO desglose = iterator.next();
		listaHashMapDesgloses.add(desglose.getHashMapExport(result));
	    }
	    result.put("listaDesgloses", listaHashMapDesgloses);
	}
	result.put("codsv", this.codsv);
	Date dejecucion = this.fejecucion;
	int iDia = dejecucion.getDate();
	int iMes = dejecucion.getMonth() + 1;
	int year = dejecucion.getYear();
	String sDia = "" + iDia;
	if (iDia < 10) {
	    sDia = "0" + sDia;
	}
	String sMes = "" + iMes;
	if (iMes < 10) {
	    sMes = "0" + iMes;
	}
	result.put("fejecucion", sDia + "/" + sMes + "/" + year);
	String sTipoOperacion = "Compra";
	if (this.getTipoOperacion().equals('2')) {
	    sTipoOperacion = "Venta";
	}
	result.put("tipoOperacion", sTipoOperacion);
	result.put("bOkTipoOperacion", this.getBOkTipoOperacion());
	result.put("titulos", this.getTitulos().floatValue());
	result.put("titulosSV", this.getTitulosSV().floatValue());
	result.put("nominal", this.getNominal().floatValue());
	result.put("precio", this.getPrecio().floatValue());
	result.put("isin", this.getIsin());
	result.put("bOkIsin", this.getBOkIsin());
	result.put("desIsin", this.getDesIsin());
	result.put("bolsa", this.getBolsaDescripcion());
	result.put("bOkBolsa", this.getBOkBolsa());
	String tipoSaldo = "Propio";
	if (this.getTipoSaldo().equals('T')) {
	    tipoSaldo = "Terceros";
	}
	result.put("tipoSaldo", tipoSaldo);
	result.put("bOkTipoSaldo", this.getBOkTipoSaldo());
	result.put("entidad", this.getEntidad());
	String tipoCambio = "Porcentaje";
	if (this.getTipoCambio().equals('2')) {
	    tipoCambio = "Efectivo";
	}
	result.put("tipoCambio", tipoCambio);
	result.put("bOkTipoCambio", this.getBOkTipoCambio());
	result.put("numReferencia", this.getNumReferencia());
	result.put("titular", this.getTitular());
	result.put("ccv", this.getCCV());
	return result;
    }

    public Boolean getbOkFechaEjecucion() {
	return bOkFechaEjecucion;
    }

    public void setbOkFechaEjecucion(Boolean bOkFechaEjecucion) {
	this.bOkFechaEjecucion = bOkFechaEjecucion;
    }

    public Boolean getbOkBolsa() {
	return bOkBolsa;
    }

    public void setbOkBolsa(Boolean bOkBolsa) {
	this.bOkBolsa = bOkBolsa;
    }

    public Boolean getbOkTipoOperacion() {
	return bOkTipoOperacion;
    }

    public void setbOkTipoOperacion(Boolean bOkTipoOperacion) {
	this.bOkTipoOperacion = bOkTipoOperacion;
    }

    public Boolean getbOkIsin() {
	return bOkIsin;
    }

    public void setbOkIsin(Boolean bOkIsin) {
	this.bOkIsin = bOkIsin;
    }

    public Boolean getbOkTipoCambio() {
	return bOkTipoCambio;
    }

    public void setbOkTipoCambio(Boolean bOkTipoCambio) {
	this.bOkTipoCambio = bOkTipoCambio;
    }

    public Boolean getbOkTipoSaldo() {
	return bOkTipoSaldo;
    }

    public void setbOkTipoSaldo(Boolean bOkTipoSaldo) {
	this.bOkTipoSaldo = bOkTipoSaldo;
    }

    public Long getIdTitular() {
	return idTitular;
    }

    public void setIdTitular(Long idTitular) {
	this.idTitular = idTitular;
    }

    public String getBolsaTitular() {
	return bolsaTitular;
    }

    public void setBolsaTitular(String bolsaTitular) {
	this.bolsaTitular = bolsaTitular;
    }

    public String getCcvTitular() {
	return ccvTitular;
    }

    public void setCcvTitular(String ccvTitular) {
	this.ccvTitular = ccvTitular;
    }

    public String getCodEmprTitular() {
	return codEmprTitular;
    }

    public void setCodEmprTitular(String codEmprTitular) {
	this.codEmprTitular = codEmprTitular;
    }

    public String getCodBICTitular() {
	return codBICTitular;
    }

    public void setCodBICTitular(String codBICTitular) {
	this.codBICTitular = codBICTitular;
    }

    public String getDistritoTitular() {
	return distritoTitular;
    }

    public void setDistritoTitular(String distritoTitular) {
	this.distritoTitular = distritoTitular;
    }

    public String getDomiciliTitular() {
	return domiciliTitular;
    }

    public void setDomiciliTitular(String domiciliTitular) {
	this.domiciliTitular = domiciliTitular;
    }

    public BigDecimal getIdSencTitular() {
	return idSencTitular;
    }

    public void setIdSencTitular(BigDecimal idSencTitular) {
	this.idSencTitular = idSencTitular;
    }

    public Character getIndNacTitular() {
	return indNacTitular;
    }

    public void setIndNacTitular(Character indNacTitular) {
	this.indNacTitular = indNacTitular;
    }

    public String getNaclidadTitular() {
	return naclidadTitular;
    }

    public void setNaclidadTitular(String naclidadTitular) {
	this.naclidadTitular = naclidadTitular;
    }

    public String getNifBicTitular() {
	return nifBicTitular;
    }

    public void setNifBicTitular(String nifBicTitular) {
	this.nifBicTitular = nifBicTitular;
    }

    public String getNombreTitular() {
	return nombreTitular;
    }

    public void setNombreTitular(String nombreTitular) {
	this.nombreTitular = nombreTitular;
    }

    public String getApellido1Titular() {
	return apellido1Titular;
    }

    public void setApellido1Titular(String apellido1Titular) {
	this.apellido1Titular = apellido1Titular;
    }

    public String getApellido2Titular() {
	return apellido2Titular;
    }

    public void setApellido2Titular(String apellido2Titular) {
	this.apellido2Titular = apellido2Titular;
    }

    public String getRazonSocialTitular() {
	return razonSocialTitular;
    }

    public void setRazonSocialTitular(String razonSocialTitular) {
	this.razonSocialTitular = razonSocialTitular;
    }

    public String getPaisTitular() {
	return paisTitular;
    }

    public void setPaisTitular(String paisTitular) {
	this.paisTitular = paisTitular;
    }

    public String getPaisReTitular() {
	return paisReTitular;
    }

    public void setPaisReTitular(String paisReTitular) {
	this.paisReTitular = paisReTitular;
    }

    public BigDecimal getParticipTitular() {
	return participTitular;
    }

    public void setParticipTitular(BigDecimal participTitular) {
	this.participTitular = participTitular;
    }

    public String getPoblacioTitular() {
	return poblacioTitular;
    }

    public void setPoblacioTitular(String poblacioTitular) {
	this.poblacioTitular = poblacioTitular;
    }

    public String getProvinciaTitular() {
	return provinciaTitular;
    }

    public void setProvinciaTitular(String provinciaTitular) {
	this.provinciaTitular = provinciaTitular;
    }

    public Character getTipoDocTitular() {
	return tipoDocTitular;
    }

    public void setTipoDocTitular(Character tipoDocTitular) {
	this.tipoDocTitular = tipoDocTitular;
    }

    public Character getTipperTitular() {
	return tipperTitular;
    }

    public void setTipperTitular(Character tipperTitular) {
	this.tipperTitular = tipperTitular;
    }

    public Character getTipTitular() {
	return tipTitular;
    }

    public void setTipTitular(Character tipTitular) {
	this.tipTitular = tipTitular;
    }

    public Long getIdDesgloseRecordBeanTitular() {
	return idDesgloseRecordBeanTitular;
    }

    public void setIdDesgloseRecordBeanTitular(Long idDesgloseRecordBeanTitular) {
	this.idDesgloseRecordBeanTitular = idDesgloseRecordBeanTitular;
    }

}
