package sibbac.business.wrappers.database.dto;


public class ContactoDTO {

	private Long		id;
	private String		cdAlias;
	private String		descripcion;
	private String		telefono1;
	private String		telefono2;
	private String		movil;
	private String		fax;
	private String		email;
	private String		comentarios;
	private String		posicionCargo;
	private Character	activo;
	private String		nombre;
	private String		apellido1;
	private String		apellido2;
	private Boolean		defecto;
	private Boolean		gestor;
	private String cdEtrali;

	public ContactoDTO( Long id, String cdAlias, String descripcion, String telefono1, String telefono2, String movil, String fax,
			String email, String comentarios, String posicionCargo, Character activo, String nombre, String apellido1, String apellido2,
			Boolean defecto, boolean gestor ) {
		super();
		this.id = id;
		this.cdAlias = cdAlias;
		this.descripcion = descripcion;
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.movil = movil;
		this.fax = fax;
		this.email = email;
		this.comentarios = comentarios;
		this.posicionCargo = posicionCargo;
		this.activo = activo;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.defecto = defecto;
		this.gestor = gestor;
	}

	public ContactoDTO() {

	}
	
	

	public String getCdEtrali() {
    return cdEtrali;
  }

  public void setCdEtrali(String cdEtrali) {
    this.cdEtrali = cdEtrali;
  }

  public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public String getCdAlias() {
		return cdAlias;
	}

	public void setCdAlias( String cdAlias ) {
		this.cdAlias = cdAlias;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1( String telefono1 ) {
		this.telefono1 = telefono1;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2( String telefono2 ) {
		this.telefono2 = telefono2;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil( String movil ) {
		this.movil = movil;
	}

	public String getFax() {
		return fax;
	}

	public void setFax( String fax ) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios( String comentarios ) {
		this.comentarios = comentarios;
	}

	public String getPosicionCargo() {
		return posicionCargo;
	}

	public void setPosicionCargo( String posicionCargo ) {
		this.posicionCargo = posicionCargo;
	}

	public Character getActivo() {
		return activo;
	}

	public void setActivo( Character activo ) {
		this.activo = activo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1( String apellido1 ) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2( String apellido2 ) {
		this.apellido2 = apellido2;
	}

	public Boolean getDefecto() {
		return defecto;
	}

	public void setDefecto( Boolean defecto ) {
		this.defecto = defecto;
	}

	public Boolean getGestor() {
		return gestor;
	}

	public void setGestor( Boolean gestor ) {
		this.gestor = gestor;
	}

}
