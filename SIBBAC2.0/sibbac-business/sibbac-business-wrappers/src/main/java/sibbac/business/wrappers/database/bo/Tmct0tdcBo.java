package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.dao.Tmct0tdcDao;
import sibbac.business.wrappers.database.model.Tmct0tdc;
import sibbac.business.wrappers.database.model.Tmct0tdcId;
import sibbac.database.bo.AbstractBo;

@Deprecated
@Component
public class Tmct0tdcBo extends AbstractBo<Tmct0tdc, Tmct0tdcId, Tmct0tdcDao> {

  public Map<String, List<Tmct0tdc>> getTablaCanonesPorBolsa() {
    Map<String, List<Tmct0tdc>> tablaCanones = new HashMap<String, List<Tmct0tdc>>();
    LOG.debug("[ProcesarDesglosesDbReader :: preExecuteTask] recuperando la tabla de canones...");
    List<Tmct0tdc> listaCanones = dao.findAllByNuversionAndFecha(1, new Date());
    if (CollectionUtils.isEmpty(listaCanones)) {
      LOG.warn("[ProcesarDesglosesDbReader :: preExecuteTask] Incidencia no encontrada la tabla de canones, el proceso no puede continuar");
      return null;
    }
    else {
      for (Tmct0tdc tmct0tdc : listaCanones) {
        if (tablaCanones.get(tmct0tdc.getId().getCddbolsa().trim()) == null) {
          tablaCanones.put(tmct0tdc.getId().getCddbolsa().trim(), new ArrayList<Tmct0tdc>());
        }
        tablaCanones.get(tmct0tdc.getId().getCddbolsa().trim()).add(tmct0tdc);
      }
    }

    return tablaCanones;
  }

  public Tmct0tdc getCanonAplicar(Map<String, List<Tmct0tdc>> cached, String bolsa, BigDecimal efectivo) {
    bolsa = StringUtils.trim(bolsa);
    List<Tmct0tdc> canones = cached.get(bolsa);
    Tmct0tdc canon = null;
    if (CollectionUtils.isNotEmpty(canones)) {
      canon = canones.get(0);

      for (Tmct0tdc tmct0tdc : canones) {
        if (efectivo.compareTo(tmct0tdc.getId().getImmaxefe()) < 0) {
          canon = tmct0tdc;
          break;
        }

      }
    }
    return canon;
  }

}
