package sibbac.business.canones.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.canones.common.CanonCalculadoEfectivoDTO;
import sibbac.business.canones.commons.CriterioAgrupacionCanonDTO;
import sibbac.business.canones.commons.ReglasCanonesDTO;
import sibbac.business.canones.commons.TipoCalculo;
import sibbac.business.canones.dao.CanonAgrupadoReglaDao;
import sibbac.business.canones.dao.CanonAgrupadoReglaDaoImpl;
import sibbac.business.canones.model.CanonAgrupadoRegla;
import sibbac.database.bo.AbstractBo;

@Service
public class CanonAgrupadoReglaBo extends AbstractBo<CanonAgrupadoRegla, Long, CanonAgrupadoReglaDao> {

  @Autowired
  private CanonAgrupadoReglaDaoImpl daoImpl;

  public CanonAgrupadoRegla findByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutaAnula, char ordenTodoNada,
      char ordenEjecutadaSubasta) {
    return daoImpl.findByAllData(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta);

  }

  public CanonCalculadoEfectivoDTO sumCanonAndEfectivoByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutadaSubasta) {
    return daoImpl.sumCanonAndEfectivoByAllData(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta);
  }

  public BigDecimal sumEfectivoByAllData(String tipoCanon, String tipoCalculo, Date fecha,
      String idMercadoContratacion, String titular, String isin, String tipoOperacionBolsa, Character sentido,
      BigDecimal precio, String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutaAnula, char ordenTodoNada,
      char ordenEjecutadaSubasta) {
    return daoImpl.sumEfectivoByAllData(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta);
  }

  public BigDecimal sumCanonByAllData(String tipoCanon, String tipoCalculo, Date fecha, String idMercadoContratacion,
      String titular, String isin, String tipoOperacionBolsa, Character sentido, BigDecimal precio,
      String miembroMercado, String ordenMercado, String ejecucionMercado, String tipoSubasta,
      String idSegmentoMercadoContratacion, char ordenPuntoMedio, char ordenOculta, char ordenBloqueCombinado,
      char ordenVolumenOculto, char restriccionOrden, char ordenEjecutaAnula, char ordenTodoNada,
      char ordenEjecutadaSubasta) {
    return daoImpl.sumCanonByAllData(tipoCanon, tipoCalculo, fecha, idMercadoContratacion, titular, isin,
        tipoOperacionBolsa, sentido, precio, miembroMercado, ordenMercado, ejecucionMercado, tipoSubasta,
        idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado, ordenVolumenOculto,
        restriccionOrden, ordenEjecutadaSubasta);
  }

  public CanonCalculadoEfectivoDTO sumCanonAndEfectivoByAllDataByAgrupacionAndReglaFromCalculos(
      CriterioAgrupacionCanonDTO agrupacion, ReglasCanonesDTO reglaAplicar) {
    String idSegmentoMercadoContratacion = null;
    char ordenPuntoMedio = 'N';
    char ordenOculta = 'N';
    char ordenBloqueCombinado = 'N';
    char ordenVolumenOculto = 'N';
    char restriccionOrden = 'N';
    char ordenEjecutadaSubasta = 'N';

    if (reglaAplicar.isOrdenPuntoMedio()) {
      ordenPuntoMedio = 'S';
    }
    if (reglaAplicar.isOrdenOculta()) {
      ordenOculta = 'S';
    }
    if (reglaAplicar.isOrdenBloqueCombinado()) {
      ordenBloqueCombinado = 'S';
    }
    if (reglaAplicar.isOrdenVolumenOculto()) {
      ordenVolumenOculto = 'S';
    }
    if (reglaAplicar.isRestriccionOrden()) {
      restriccionOrden = 'S';
    }
    if (reglaAplicar.isEjecucionSubasta()) {
      ordenEjecutadaSubasta = 'S';
    }

    return daoImpl.sumCanonAndEfectivoByAllData(reglaAplicar.getTipoCanon().name(), TipoCalculo.CALCULO.name(),
        agrupacion.getFecha(), agrupacion.getMercadoContratacion(), agrupacion.getTitularFinal(), agrupacion.getIsin(),
        agrupacion.getTipoOperacionBolsa(), agrupacion.getSentido(), agrupacion.getPrecio(),
        agrupacion.getMiembroMercado(), agrupacion.getOrdenMercado(), agrupacion.getEjecucionMercado(),
        agrupacion.getTipoSubasta(), idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado,
        ordenVolumenOculto, restriccionOrden, ordenEjecutadaSubasta);

  }

  public CanonAgrupadoRegla findCanonCalculadoByAgrupacionAndRegla(CriterioAgrupacionCanonDTO agrupacion,
      ReglasCanonesDTO reglaAplicar) {
    String idSegmentoMercadoContratacion = null;
    char ordenPuntoMedio = 'N';
    char ordenOculta = 'N';
    char ordenBloqueCombinado = 'N';
    char ordenVolumenOculto = 'N';
    char restriccionOrden = 'N';
    char ordenEjecutadaSubasta = 'N';

    if (reglaAplicar.isOrdenPuntoMedio()) {
      ordenPuntoMedio = 'S';
    }
    if (reglaAplicar.isOrdenOculta()) {
      ordenOculta = 'S';
    }
    if (reglaAplicar.isOrdenBloqueCombinado()) {
      ordenBloqueCombinado = 'S';
    }
    if (reglaAplicar.isOrdenVolumenOculto()) {
      ordenVolumenOculto = 'S';
    }
    if (reglaAplicar.isRestriccionOrden()) {
      restriccionOrden = 'S';
    }
    if (reglaAplicar.isEjecucionSubasta()) {
      ordenEjecutadaSubasta = 'S';
    }

    return daoImpl.findByAllData(reglaAplicar.getTipoCanon().name(), reglaAplicar.getTipoCalculo().name(),
        agrupacion.getFecha(), agrupacion.getMercadoContratacion(), agrupacion.getTitularFinal(), agrupacion.getIsin(),
        agrupacion.getTipoOperacionBolsa(), agrupacion.getSentido(), agrupacion.getPrecio(),
        agrupacion.getMiembroMercado(), agrupacion.getOrdenMercado(), agrupacion.getEjecucionMercado(),
        agrupacion.getTipoSubasta(), idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado,
        ordenVolumenOculto, restriccionOrden, ordenEjecutadaSubasta);

  }

  public List<CanonAgrupadoRegla> findAllCanonCalculadoByAgrupacionAndRegla(CriterioAgrupacionCanonDTO agrupacion,
      ReglasCanonesDTO reglaAplicar) {
    String idSegmentoMercadoContratacion = null;
    char ordenPuntoMedio = 'N';
    char ordenOculta = 'N';
    char ordenBloqueCombinado = 'N';
    char ordenVolumenOculto = 'N';
    char restriccionOrden = 'N';
    char ordenEjecutadaSubasta = 'N';

    if (reglaAplicar.isOrdenPuntoMedio()) {
      ordenPuntoMedio = 'S';
    }
    if (reglaAplicar.isOrdenOculta()) {
      ordenOculta = 'S';
    }
    if (reglaAplicar.isOrdenBloqueCombinado()) {
      ordenBloqueCombinado = 'S';
    }
    if (reglaAplicar.isOrdenVolumenOculto()) {
      ordenVolumenOculto = 'S';
    }
    if (reglaAplicar.isRestriccionOrden()) {
      restriccionOrden = 'S';
    }
    if (reglaAplicar.isEjecucionSubasta()) {
      ordenEjecutadaSubasta = 'S';
    }
    return daoImpl.findAllByAllData(reglaAplicar.getTipoCanon().name(), reglaAplicar.getTipoCalculo().name(),
        agrupacion.getFecha(), agrupacion.getMercadoContratacion(), agrupacion.getTitularFinal(), agrupacion.getIsin(),
        agrupacion.getTipoOperacionBolsa(), agrupacion.getSentido(), agrupacion.getPrecio(),
        agrupacion.getEjecucionMercado(), agrupacion.getOrdenMercado(), agrupacion.getEjecucionMercado(),
        agrupacion.getTipoSubasta(), idSegmentoMercadoContratacion, ordenPuntoMedio, ordenOculta, ordenBloqueCombinado,
        ordenVolumenOculto, restriccionOrden, ordenEjecutadaSubasta);

  }

  public CanonAgrupadoRegla crearCanonAgrupadoRegla(CriterioAgrupacionCanonDTO agrupacion, ReglasCanonesDTO reglaAplicar) {
    CanonAgrupadoRegla res = new CanonAgrupadoRegla();
    String idSegmentoMercadoContratacion = null;

    if (reglaAplicar.isOrdenPuntoMedio()) {
      res.setOrdenPuntoMedio('S');
    }
    if (reglaAplicar.isOrdenOculta()) {
      res.setOrdenOculta('S');
    }
    if (reglaAplicar.isOrdenBloqueCombinado()) {
      res.setOrdenBloqueCombinado('S');
    }
    if (reglaAplicar.isOrdenVolumenOculto()) {
      res.setOrdenVolumenOculto('S');
    }
    if (reglaAplicar.isRestriccionOrden()) {
      res.setRestriccionOrden('S');
    }
    if (reglaAplicar.isEjecucionSubasta()) {
      res.setOrdenEjecutadaSubasta('S');
    }

    res.setFechaContratacion(agrupacion.getFecha());
    res.setOrdenMercado(agrupacion.getOrdenMercado());
    res.setEjecucionMercado(agrupacion.getEjecucionMercado());
    res.setIsin(agrupacion.getIsin());
    res.setMercadoContratacion(agrupacion.getMercadoContratacion());
    res.setPrecio(agrupacion.getPrecio());
    res.setSegmentoMercadoContratacion(idSegmentoMercadoContratacion);
    res.setSentido(agrupacion.getSentido());
    res.setTipoCalculo(reglaAplicar.getTipoCalculo().name());
    res.setTipoCanon(reglaAplicar.getTipoCanon().name());
    res.setTipoOperacionBolsa(agrupacion.getTipoOperacionBolsa());
    res.setTipoSubasta(agrupacion.getTipoSubasta());
    res.setTitular(agrupacion.getTitularFinal());
    res.setMiembroMercado(agrupacion.getMiembroMercado());
    res.setFhaudit(new Date());
    res.setImporteCanon(BigDecimal.ZERO);
    res.setImporteEfectivo(BigDecimal.ZERO);
    if (reglaAplicar.getIdReglaCanonParametrizacion() != null) {
      res.setParametrizacion(reglaAplicar.getIdReglaCanonParametrizacion().longValue());
    }
    else {
      res.setParametrizacion(0L);
    }
    return res;
  }

}
