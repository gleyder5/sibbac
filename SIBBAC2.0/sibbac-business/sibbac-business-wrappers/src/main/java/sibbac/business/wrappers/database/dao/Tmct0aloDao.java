package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.data.AloDesgloseData;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bokId;

@Repository
public interface Tmct0aloDao extends JpaRepository<Tmct0alo, Tmct0aloId>, JpaSpecificationExecutor<Tmct0alo> {

  List<Tmct0alo> findAllByRefbanif(String refbanif);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden ")
  List<Tmct0alo> findAllByNuorden(@Param("nuorden") String nuorden);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nbooking=:nbooking ")
  List<Tmct0alo> findAllByNbooking(@Param("nbooking") String nbooking);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nucnfclt=:nucnfclt ")
  List<Tmct0alo> findAllByNucnfclt(@Param("nucnfclt") String nucnfclt);

  @Query(value = "SELECT count(*) FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND alo.tmct0sta.id.cdestado not in ('002', '003') ")
  Integer countByNuordenActivosInternacional(@Param("nuorden") String nuorden);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND ALO.id.nbooking =:nbooking")
  List<Tmct0alo> findAllByNuordenAndNbooking(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking);

  @Query(value = "SELECT ALO.id FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND ALO.id.nbooking =:nbooking AND ALO.tmct0estadoByCdestadoasig.idestado = :cdEstadoAsig ")
  List<Tmct0aloId> findAllTmct0aloIdByNuordenAndNbookingAndCdestadoasig(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("cdEstadoAsig") int cdEstadoAsig);

  @Query(value = "SELECT ALO.id FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND ALO.id.nbooking =:nbooking AND ALO.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0aloId> findAllTmct0aloIdByNuordenAndNbooking(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND ALO.id.nbooking =:nbooking AND alo.tmct0sta.id.cdestado not in ('002', '003') ")
  List<Tmct0alo> findAllByNuordenAndNbookingActivosInternacional(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking);

  List<Tmct0alo> findAllByRefCliente(String refCliente);

  // @Query("SELECT * FROM BSNBPSQL.TMCT0ALC A, BSNBPSQL.TMCT0BOK B WHERE
  // A.NUORDEN=B.NUORDEN AND A.NBOOKING=B.NBOOKING AND year(FEEJELIQ)=2015 AND
  // CDESTLIQ='A' AND A.CDESTADOASIG IN (828)")
  // List<Tmct0alo> findByDevengoContabilizado(final String tipoMovimiento,
  // final boolean contabilizado);
  //
  @Query("UPDATE Tmct0alo SET contabilizado = :contabilizado WHERE tipoMovimiento = :tipoMovimiento")
  public void updateContabilizadoByTipoMovimiento(@Param("tipoMovimiento") String tipoMovimiento,
      @Param("contabilizado") boolean contabilizado);

  @Query(value = "SELECT a.nutitcli FROM Tmct0alo a WHERE a.id = :id")
  BigDecimal getNutitcliByTmct0aloId(@Param("id") Tmct0aloId id);

  @Query(value = "SELECT max(a.nupareje) FROM Tmct0alo a WHERE a.nuoprout = :pnuoprout")
  Integer getMaxNuparejeByNuoprout(@Param("pnuoprout") Integer nuoprout);

  /**
   * Obtiene todos los 'objetos' <code>Afectacion</code> que cumplen las
   * condiciones.
   * 
   * @param isin Isin a buscar.
   * @param tpoper Sentido de la operación.
   * @param feLiquidacion Fecha de liquidación.
   * @param cuentaClearing Tipo de cuenta clearing en la que liquida la
   * operación.
   * @return <code>java.util.List<Object[]> - </code>Lista obtenida.
   * @author XI316153
   */
  @Query(value = "SELECT ALO.CDALIAS, ALO.NUORDEN, ALO.NBOOKING, ALO.NUCNFCLT, ALC.NUCNFLIQ, ALC.IDDESGLOSECAMARA, ALC.NUDESGLOSE, ALC.IMTITULOS, ALC.IMPRECIO, ALC.NUTITFALLIDOS, ALC.FEEJECUC, ALC.HOEJECUC FROM TMCT0ALO ALO, ( SELECT ALC.NUORDEN, ALC.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ, DC.IDDESGLOSECAMARA, DC.NUDESGLOSE, DC.IMTITULOS, DC.IMPRECIO, DC.NUTITFALLIDOS, DC.FEEJECUC, DC.HOEJECUC, DC.TSEJECUC FROM TMCT0ALC ALC, ( SELECT DC.NUORDEN, DC.NBOOKING, DC.NUCNFCLT, DC.NUCNFLIQ, DC.IDDESGLOSECAMARA, DCT.NUDESGLOSE, DCT.IMTITULOS, DCT.IMPRECIO, DCT.NUTITFALLIDOS, DCT.FEEJECUC, DCT.HOEJECUC, DCT.TSEJECUC FROM TMCT0DESGLOSECAMARA DC, ( SELECT IDDESGLOSECAMARA, NUDESGLOSE, IMTITULOS, IMPRECIO, NUTITFALLIDOS, FEEJECUC, HOEJECUC, TIMESTAMP(FEEJECUC, HOEJECUC) AS TSEJECUC FROM TMCT0DESGLOSECLITIT DCT WHERE NUDESGLOSE NOT IN ( :nudesgloses_afectados ) AND CDISIN = :isin AND CDSENTIDO = :tpoper AND FEEJECUC >= :feEjecucion AND IMTITULOS > NUTITFALLIDOS ) DCT WHERE DC.CDESTADOASIG = :cdEstadoAsig AND DC.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA AND IDINFOCOMPENSACION IN ( SELECT IDINFOCOMP FROM TMCT0INFOCOMPENSACION WHERE CDCTACOMPENSACION IN ( SELECT CD_CODIGO FROM TMCT0_CUENTAS_DE_COMPENSACION WHERE CUENTA_CLEARING = :cuentaClearing ) ) ) DC WHERE ALC.NUORDEN = DC.NUORDEN AND ALC.NBOOKING = DC.NBOOKING AND ALC.NUCNFCLT = DC.NUCNFCLT AND ALC.NUCNFLIQ = DC.NUCNFLIQ ) ALC WHERE ALO.NUORDEN = ALC.NUORDEN AND ALO.NBOOKING = ALC.NBOOKING AND ALO.NUCNFCLT = ALC.NUCNFCLT ORDER BY ALC.IMPRECIO ASC, ALC.IMTITULOS DESC, ALC.TSEJECUC ASC", nativeQuery = true)
  public List<Object[]> findByIsinAndTpoperAndFeliquidacionAndCuentaClearingAndNudesgloses(@Param("isin") String isin,
      @Param("tpoper") Character tpoper, @Param("feEjecucion") Date feEjecucion,
      @Param("cdEstadoAsig") Integer cdEstadoAsig, @Param("cuentaClearing") Character cuentaClearing,
      @Param("nudesgloses_afectados") Collection<Long> nudesgloses_afectados);

  @Query(value = "SELECT ALO.CDALIAS, ALO.NUORDEN, ALO.NBOOKING, ALO.NUCNFCLT, ALC.NUCNFLIQ, ALC.IDDESGLOSECAMARA, ALC.NUDESGLOSE, ALC.IMTITULOS, ALC.IMPRECIO, ALC.NUTITFALLIDOS, ALC.FEEJECUC, ALC.HOEJECUC FROM TMCT0ALO ALO, ( SELECT ALC.NUORDEN, ALC.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ, DC.IDDESGLOSECAMARA, DC.NUDESGLOSE, DC.IMTITULOS, DC.IMPRECIO, DC.NUTITFALLIDOS, DC.FEEJECUC, DC.HOEJECUC, DC.TSEJECUC FROM TMCT0ALC ALC, ( SELECT DC.NUORDEN, DC.NBOOKING, DC.NUCNFCLT, DC.NUCNFLIQ, DC.IDDESGLOSECAMARA, DCT.NUDESGLOSE, DCT.IMTITULOS, DCT.IMPRECIO, DCT.NUTITFALLIDOS, DCT.FEEJECUC, DCT.HOEJECUC, DCT.TSEJECUC FROM TMCT0DESGLOSECAMARA DC, ( SELECT IDDESGLOSECAMARA, NUDESGLOSE, IMTITULOS, IMPRECIO, NUTITFALLIDOS, FEEJECUC, HOEJECUC, TIMESTAMP(FEEJECUC, HOEJECUC) AS TSEJECUC FROM TMCT0DESGLOSECLITIT DCT WHERE CDISIN = :isin AND CDSENTIDO = :tpoper AND FEEJECUC >= :feEjecucion AND IMTITULOS > NUTITFALLIDOS ) DCT WHERE DC.CDESTADOASIG = :cdEstadoAsig AND DC.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA AND IDINFOCOMPENSACION IN ( SELECT IDINFOCOMP FROM TMCT0INFOCOMPENSACION WHERE CDCTACOMPENSACION IN ( SELECT CD_CODIGO FROM TMCT0_CUENTAS_DE_COMPENSACION WHERE CUENTA_CLEARING = :cuentaClearing ) ) ) DC WHERE ALC.NUORDEN = DC.NUORDEN AND ALC.NBOOKING = DC.NBOOKING AND ALC.NUCNFCLT = DC.NUCNFCLT AND ALC.NUCNFLIQ = DC.NUCNFLIQ ) ALC WHERE ALO.NUORDEN = ALC.NUORDEN AND ALO.NBOOKING = ALC.NBOOKING AND ALO.NUCNFCLT = ALC.NUCNFCLT ORDER BY ALC.IMPRECIO ASC, ALC.IMTITULOS DESC, ALC.TSEJECUC ASC", nativeQuery = true)
  public List<Object[]> findByIsinAndTpoperAndFeliquidacionAndCuentaClearing(@Param("isin") String isin,
      @Param("tpoper") Character tpoper, @Param("feEjecucion") Date feEjecucion,
      @Param("cdEstadoAsig") Integer cdEstadoAsig, @Param("cuentaClearing") Character cuentaClearing);

  /**
   * Obtiene el sumatorio de titulos comprados y el de vendidos para la fecha de
   * ejecución y lista de alias especificados.
   * 
   * @param tpoper Sentido de la operación.
   * @param feEjecucion Fecha de ejecución.
   * @param listaAlias Colección de alias.
   * @return <code>java.util.List<Object[]> - </code>Lista obtenida.
   * @author XI316153
   */
  @Query(value = "SELECT ALO.CDALIAS, ALO.CDTPOPER, SUM(ALC.IMTITULOS), ALC.FEEJECUC FROM TMCT0ALO ALO, ( SELECT ALC.NUORDEN, ALC.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ, DC.IMTITULOS, DC.FEEJECUC FROM TMCT0ALC ALC, ( SELECT DC.NUORDEN, DC.NBOOKING, DC.NUCNFCLT, DC.NUCNFLIQ, DCT.IMTITULOS, DCT.FEEJECUC FROM TMCT0DESGLOSECAMARA DC, ( SELECT IDDESGLOSECAMARA, IMTITULOS - NUTITFALLIDOS AS IMTITULOS, FEEJECUC FROM TMCT0DESGLOSECLITIT DCT WHERE CDISIN = :isin AND FEEJECUC >= :feEjecucion ) DCT WHERE DC.CDESTADOASIG = :cdEstadoAsig AND DC.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA AND IDINFOCOMPENSACION IN ( SELECT IDINFOCOMP FROM TMCT0INFOCOMPENSACION WHERE CDCTACOMPENSACION IN ( SELECT CD_CODIGO FROM TMCT0_CUENTAS_DE_COMPENSACION WHERE CUENTA_CLEARING = :cuentaClearing ) ) ) DC WHERE ALC.NUORDEN = DC.NUORDEN AND ALC.NBOOKING = DC.NBOOKING AND ALC.NUCNFCLT = DC.NUCNFCLT AND ALC.NUCNFLIQ = DC.NUCNFLIQ ) ALC WHERE ALO.NUORDEN = ALC.NUORDEN AND ALO.NBOOKING = ALC.NBOOKING AND ALO.NUCNFCLT = ALC.NUCNFCLT AND ALO.CDALIAS IN ( :listaAlias ) GROUP BY ALO.CDALIAS, ALO.CDTPOPER, ALC.FEEJECUC ORDER BY ALO.CDALIAS, ALC.FEEJECUC ASC, ALO.CDTPOPER ASC, SUM(ALC.IMTITULOS) ASC", nativeQuery = true)
  public List<Object[]> findByCdaliasAndTpoperAndFeejecucAndSaldos(@Param("isin") String isin,
      @Param("feEjecucion") Date feEjecucion, @Param("cdEstadoAsig") Integer cdEstadoAsig,
      @Param("cuentaClearing") Character cuentaClearing, @Param("listaAlias") Collection<String> listaAlias);

  /**
   * Obtiene los campos de alias, holder, descripcion del ISIN y alias del ALO a
   * partir de las operaciones de desgloseclitit
   * 
   * @param numeroOperacion CDOPERACION de DESGLOSECLITIT
   * @param numeroEjecucionMercado NUREFEXEMKT de DESGLOSECLITIT
   * @param numeroOrdenMercado NUORDENMKT de DESGLOSECLITIT
   * @return <code>java.util.List<Object[]> - </code> Lista de campos de los
   * ALOs
   */
  @Query(
  // La otra estaba dando un error: "DB2 SQL Error: SQLCODE=-811,
  // SQLSTATE=21000"
  // sobre que un INNER SELECT mas de un registro.
  value = "SELECT CDALIAS, CDHOLDER, DESCRALI, NBVALORS, NBTITULR, NUORDEN"
      + " FROM  TMCT0ALO"
      + " WHERE NUCNFCLT =("
      + "     SELECT DISTINCT(NUCNFCLT) FROM TMCT0ALC WHERE NUCNFCLT = ("
      + "         SELECT DISTINCT(NUCNFCLT) FROM TMCT0DESGLOSECAMARA WHERE IDDESGLOSECAMARA = ("
      + "             SELECT IDDESGLOSECAMARA FROM TMCT0DESGLOSECLITIT WHERE CDOPERACIONECC = :numeroOperacion AND NUREFEXEMKT = :numeroEjecucionMercado AND NUORDENMKT = :numeroOrdenMercado"
      + "         )" + "     )" + " )",
  /*
   * value =
   * "SELECT CDALIAS, CDHOLDER, DESCRALI, NBVALORS, NBTITULR FROM TMCT0ALO WHERE NUCNFCLT = ("
   * + " SELECT NUCNFCLT FROM TMCT0ALC WHERE NUCNFCLT = (" +
   * " SELECT NUCNFCLT FROM TMCT0DESGLOSECAMARA WHERE IDDESGLOSECAMARA = (" +
   * " SELECT IDDESGLOSECAMARA FROM TMCT0DESGLOSECLITIT WHERE CDOPERACION = :numeroOperacion AND NUREFEXEMKT = :numeroEjecucionMercado AND NUORDENMKT = :numeroOrdenMercado )))"
   * ,
   */
  nativeQuery = true)
  public List<Object[]> findByCdaliasHolder(@Param("numeroOperacion") String numeroOperacion,
      @Param("numeroEjecucionMercado") String numeroEjecucionMercado,
      @Param("numeroOrdenMercado") String numeroOrdenMercado);

  @Query(value = "SELECT CDALIAS, CDHOLDER, DESCRALI, NBVALORS, NBHOLDER FROM TMCT0ALO WHERE NUCNFCLT = :idAlo", nativeQuery = true)
  public List<Object[]> findByNuncnfclt(@Param("idAlo") String idAlo);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND ALO.rfparten =:rfparten")
  public List<Tmct0alo> findByNuordenAndRfparten(@Param("nuorden") String nuordenDelFichero,
      @Param("rfparten") String nureford);

  @Query(value = "SELECT new sibbac.business.wrappers.database.data.Tmct0AloData( ALO, ALO.tmct0alcs, ALO.tmct0estadoByCdestadoasig, ALO.tmct0estadoByCdestadocont ) FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden AND ALO.rfparten =:rfparten")
  public List<Tmct0AloData> findDataByNuordenAndRfparten(@Param("nuorden") String nuordenDelFichero,
      @Param("rfparten") String nureford);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden")
  List<Tmct0alo> findByNuorden(@Param("nuorden") String nuorden);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.id = :alloId ")
  List<ControlTitularesDTO> findControlTitularesByAlloId(@Param("alloId") Tmct0aloId aloId);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.id.nuorden=:nuorden ")
  List<ControlTitularesDTO> findControlTitularesByNuorden(@Param("nuorden") String nuorden);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.refCliente=:prefCliente ")
  List<ControlTitularesDTO> findControlTitularesByRefCliente(@Param("prefCliente") String refCliente);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.id.nuorden = :nuorden AND a.refbanif = :refbanif")
  List<ControlTitularesDTO> findControlTitularesByNuordenAndRefbanif(@Param("nuorden") String nuorden,
      @Param("refbanif") String refbanif);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND a.feoperac = :feoperac AND a.refbanif = :refbanif")
  List<ControlTitularesDTO> findControlTitularesByFeoperacAndRefbanif(@Param("feoperac") Date feoperac,
      @Param("refbanif") String refbanif);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.refbanif = :refbanif")
  List<ControlTitularesDTO> findControlTitularesByRefbanif(@Param("refbanif") String refbanif);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  o.nureford=:nureford")
  List<ControlTitularesDTO> findControlTitularesByNureford(@Param("nureford") String nureford);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.id.nuorden = :nuorden AND a.cdestadotit < :estadotit "
      + " AND a.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<ControlTitularesDTO> findControlTitularesByNuordenAndCdestadotitLt(@Param("nuorden") String nuorden,
      @Param("estadotit") Integer cdestadotitLt);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO WHERE ALO.id.nucnfclt=:nucnfclt")
  List<Tmct0alo> findByNucnfclt(@Param("nucnfclt") String nucnfclt);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id, a.fevalor, a.reftitular, a.cdestadotit, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alo a, Tmct0ord o WHERE o.nuorden = a.id.nuorden AND  a.id.nucnfclt=:nucnfclt AND a.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<ControlTitularesDTO> findControlTitularesByNucnfclt(@Param("nucnfclt") String nucnfclt);

  @Query(value = "SELECT new sibbac.business.wrappers.database.data.Tmct0AloData(ALO) FROM Tmct0alo ALO WHERE ALO.id.nuorden=:nuorden")
  List<Tmct0AloData> findDataByNuordenTransactional(@Param("nuorden") String nuorden);

  @Query(value = "SELECT new sibbac.business.wrappers.database.data.Tmct0AloData(ALO) FROM Tmct0alo ALO WHERE ALO.id = :alloId ")
  Tmct0AloData findDataById(@Param("alloId") Tmct0aloId alloId);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO, Tmct0bok BOK "
      + " WHERE ALO.id.nuorden = BOK.id.nuorden and ALO.id.nbooking = BOK.id.nbooking "
      + " and ALO.id.nuorden = :nuorden and BOK.nutiteje = ALO.nutitcli and ALO.tmct0estadoByCdestadoasig.idestado > 0 ")
  Tmct0alo findByNuordenIgualTitulosBooking(@Param("nuorden") String nuorden);

  @Query(value = "SELECT ALO FROM Tmct0alo ALO "
      + " WHERE ALO.id.nuorden = :nuorden and ALO.tmct0estadoByCdestadoasig.idestado = :pcdestadoasig ")
  Tmct0alo findByNuordenAndCdestadoasig(@Param("nuorden") String nuorden, @Param("pcdestadoasig") int cdestadoasig);

  // public Set<ElementosIFinanciero>
  // findAllGroupByNuordenAndNbookingAndNucnfclt();

  @Query("SELECT o FROM Tmct0alo o WHERE o.cdestadotit =:cdestadotit")
  List<Tmct0alo> findAlosByEstadotit(@Param("cdestadotit") Integer cdestadotit);

  // public Set<ElementosIFinanciero>
  // findAllGroupByNuordenAndNbookingAndNucnfclt();

  @Query("SELECT o.id FROM Tmct0alo o WHERE o.cdestadotit =:cdestadotit AND o.fevalor <=:fevalor")
  List<Tmct0aloId> findAlosIdByEstadotitAndfevalorLte(@Param("cdestadotit") Integer cdestadotit,
      @Param("fevalor") Date fevalor);

  @Query("SELECT o FROM Tmct0alo o WHERE o.id.nuorden =:nuorden AND o.id.nbooking=:nbooking AND o.id.nucnfclt=:nucnfclt")
  Tmct0alo getAlo(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt);

  @Modifying
  @Query("update Tmct0alo o set o.tmct0estadoByCdestadoasig.idestado = :cdestadoasigTo, o.fhaudit = :pfhaudit "
      + " where o.id.nuorden in :pordenes and o.tmct0estadoByCdestadoasig.idestado = :cdestadoasigFrom ")
  Integer updateCdestadoasigByNumordenAndCdestadoasig(@Param("pordenes") List<String> ordenes,
      @Param("cdestadoasigFrom") int cdestadoasigFrom, @Param("cdestadoasigTo") int cdestadoasigTo,
      @Param("pfhaudit") Date fhaudit);

  @Modifying
  @Query("update Tmct0alo o set o.tmct0estadoByCdestadoasig.idestado = :cdestadoasigTo, o.fhaudit = :pfhaudit "
      + " where o.tmct0bok.id = :bookingId and o.tmct0estadoByCdestadoasig.idestado = :cdestadoasigFrom ")
  Integer updateCdestadoasigByTmct0bokIdAndCdestadoasig(@Param("bookingId") Tmct0bokId bokId,
      @Param("cdestadoasigFrom") int cdestadoasigFrom, @Param("cdestadoasigTo") int cdestadoasigTo,
      @Param("pfhaudit") Date fhaudit);

  @Modifying
  @Query("update Tmct0alo o set o.tmct0estadoByCdestadoasig.idestado = :cdestadoasigTo, o.fhaudit = :pfhaudit "
      + " where o.id = :aloId  ")
  Integer updateCdestadoasigByTmct0aloId(@Param("aloId") Tmct0aloId aloId, @Param("cdestadoasigTo") int cdestadoasigTo,
      @Param("pfhaudit") Date fhaudit);

  @Modifying
  @Query("update Tmct0alo o set o.cdestadotit = :cdestadotitTo, o.fhaudit = :pfhaudit, o.cdusuaud = :cdusuaud where o.tmct0bok.id = :bokId "
      + " and o.tmct0estadoByCdestadoasig.idestado > 0 ")
  Integer updateCdestadotitByTmct0bokId(@Param("bokId") Tmct0bokId bokId, @Param("cdestadotitTo") int cdestadotitTo,
      @Param("pfhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);

  @Modifying
  @Query("UPDATE Tmct0alo a SET a.cdestadotit = :pcdestadotit, a.fhaudit = :fhaudit "
      + " WHERE a.id = :aloId AND a.tmct0estadoByCdestadoasig.idestado > 0")
  Integer updateCdestadotitByTmct0aloId(@Param("aloId") Tmct0aloId aloId, @Param("pcdestadotit") int cdestadoatit,
      @Param("fhaudit") Date fhaudit);

  @Query("SELECT distinct a.tmct0bok.id FROM Tmct0alo a WHERE a.tmct0estadoByCdestadoasig.idestado = :pcdestadoasig")
  List<Tmct0bokId> findAllBookingIdContainsTmct0aloByCdestadoasig(@Param("pcdestadoasig") int cdestadoasig);

  @Query("SELECT distinct a.tmct0bok.id FROM Tmct0alo a WHERE a.tmct0estadoByCdestadoasig.idestado = :pcdestadoasig"
      + " and a.tmct0bok.tmct0ord.isscrdiv = :isscrdiv ")
  List<Tmct0bokId> findAllBookingIdContainsTmct0aloByCdestadoasigAndIssrcdiv(@Param("pcdestadoasig") int cdestadoasig,
      @Param("isscrdiv") char isscrdiv);

  @Query("SELECT a FROM Tmct0alo a WHERE a.tmct0bok.id=:bookingId and a.tmct0estadoByCdestadoasig.idestado = :pcdestadoasig")
  List<Tmct0alo> findTmct0aloByTmct0bokIdAndCdestadoasig(@Param("bookingId") Tmct0bokId bookingId,
      @Param("pcdestadoasig") int cdestadoasig);

  @Query("SELECT sum(a.nutitcli) FROM Tmct0alo a WHERE a.tmct0bok.id=:bookingId and a.tmct0estadoByCdestadoasig.idestado > :pcdestadoasig")
  BigDecimal sumNutitcliByTmct0bokId(@Param("bookingId") Tmct0bokId bookingId,
      @Param("pcdestadoasig") int cdestadoasigGt);

  @Query("SELECT sum(a.nutitcli) FROM Tmct0alo a WHERE a.tmct0bok.id=:bookingId and a.tmct0estadoByCdestadoasig.idestado = :pcdestadoasig")
  BigDecimal sumNutitcliByTmct0bokIdAndCdestadoasig(@Param("bookingId") Tmct0bokId bookingId,
      @Param("pcdestadoasig") int cdestadoasigEq);

  @Query(value = "SELECT C.NUORDEN, C.NBOOKING, C.NUCNFCLT "
      + " FROM (SELECT B.NUORDEN, B.NBOOKING FROM TMCT0BOK B WHERE B.FEVALOR > :feliquidacionGt AND B.CDESTADOASIG > 0 AND B.CASEPTI = :casepti ) B "
      + " INNER JOIN (SELECT C.NUORDEN, C.NBOOKING, C.NUCNFCLT FROM TMCT0ALO C WHERE C.FEVALOR > :feliquidacionGt AND C.CDESTADOASIG = :pcdestadoasig ) "
      + " C ON B.NUORDEN=C.NUORDEN AND B.NBOOKING=C.NBOOKING ", nativeQuery = true)
  List<Object[]> findAllTmct0aloIdByFeeoperacGtAndCdestadoAsigAndCasepti(@Param("feliquidacionGt") Date sinceGt,
      @Param("pcdestadoasig") int cdestadoasig, @Param("casepti") char casepti);

  @Query("SELECT new sibbac.business.wrappers.database.data.AloDesgloseData(alo) "
      + "FROM Tmct0alo alo WHERE alo.id = :aloId")
  AloDesgloseData findAloDesgloseData(@Param("aloId") Tmct0aloId id);

} // Tmct0aloDao
