package sibbac.business.wrappers.database.dto;

import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0Nemotecnicos;

public class Tmct0NemotecnicosDTO {
	
	private Long id_nemotecnico;
	private String nbnombre;
	private String nb_nemotecnico;
	private Long id_compensador;
	private Date audit_fecha_cambio;
	private String audit_user;
	private String cd_referencia_asignacion;
	private String nbDescripcion;
	
	
	public Tmct0NemotecnicosDTO() {	}
	

	public Tmct0NemotecnicosDTO(Tmct0Nemotecnicos Tmct0Nemotecnicos){
		id_nemotecnico =Tmct0Nemotecnicos.getId_nemotecnico();
		nbnombre = Tmct0Nemotecnicos.getNbnombre();
		nb_nemotecnico = Tmct0Nemotecnicos.getNbNemotecnico();
		id_compensador = Tmct0Nemotecnicos.getId_compensador();
		audit_fecha_cambio= Tmct0Nemotecnicos.getAudit_fecha_cambio();
		audit_user  = Tmct0Nemotecnicos.getAudit_user();
		cd_referencia_asignacion =Tmct0Nemotecnicos.getCd_referencia_asignacion();
		nbDescripcion = Tmct0Nemotecnicos.getNbDescripcion();
	}
	
	public Long getId_nemotecnico() {
		return id_nemotecnico;
	}


	public void setId_nemotecnico(Long id_nemotecnico) {
		this.id_nemotecnico = id_nemotecnico;
	}


	public String getNbnombre() {
		return nbnombre;
	}


	public void setNbnombre(String nbnombre) {
		this.nbnombre = nbnombre;
	}


	public String getNb_nemotecnico() {
		return nb_nemotecnico;
	}


	public void setNb_nemotecnico(String nb_nemotecnico) {
		this.nb_nemotecnico = nb_nemotecnico;
	}


	public Long getId_compensador() {
		return id_compensador;
	}


	public void setId_compensador(Long id_compensador) {
		this.id_compensador = id_compensador;
	}


	public Date getAudit_fecha_cambio() {
		return audit_fecha_cambio;
	}


	public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
		this.audit_fecha_cambio = audit_fecha_cambio;
	}


	public String getAudit_user() {
		return audit_user;
	}


	public void setAudit_user(String audit_user) {
		this.audit_user = audit_user;
	}


	public String getCd_referencia_asignacion() {
		return cd_referencia_asignacion;
	}


	public void setCd_referencia_asignacion(String cd_referencia_asignacion) {
		this.cd_referencia_asignacion = cd_referencia_asignacion;
	}


	public String getNbDescripcion() {
		return nbDescripcion;
	}


	public void setNbDescripcion(String nb_descripcion) {
		this.nbDescripcion = nb_descripcion;
	}
	
	
	
	
	

}
