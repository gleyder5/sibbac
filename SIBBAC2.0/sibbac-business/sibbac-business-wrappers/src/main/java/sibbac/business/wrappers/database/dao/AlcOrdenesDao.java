package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Netting;

@Repository
public interface AlcOrdenesDao extends JpaRepository<AlcOrdenes, Long> {

  @Query("SELECT alcOrden FROM AlcOrdenes alcOrden WHERE alcOrden.nuorden = :nuorden "
      + "AND alcOrden.nbooking = :nbooking " + "AND alcOrden.nucnfclt = :nucnfclt "
      + "AND alcOrden.nucnfliq = :nucnfliq "
      + "AND (alcOrden.estadoInstruccion is null or alcOrden.estadoInstruccion = 'A') ")
  public AlcOrdenes findByTmct0alc(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") short nucnfliq);

  @Query("SELECT o FROM AlcOrdenes o WHERE o.nbooking = '0' " + "AND o.nuorden = ?1 " + "AND o.nucnfliq = 0 "
      + "AND o.nucnfclt = '0' " + "AND o.estadoInstruccion = 'A' ")
  public List<AlcOrdenes> findAllByNuordenAndZeroes(String nuorden);

  @Query("SELECT o FROM AlcOrdenes o WHERE o.nbooking = '0' AND o.nuorden = :nuorden AND o.nucnfliq = 0 "
      + "AND o.nucnfclt = '0' AND o.estadoInstruccion = 'A' ")
  public List<AlcOrdenes> findPatasMercadoPorOrden(@Param("nuorden") String nuorden);

  @Query(value = "SELECT alcOrden.ID FROM TMCT0_ALC_ORDEN alcOrden WHERE alcOrden.nuorden = :nuOrden "
      + "AND alcOrden.nbooking = '0' AND alcOrden.nucnfliq = 0 AND alcOrden.nucnfclt = '0' ORDER BY ID DESC", nativeQuery = true)
  public List<BigInteger> findByNuOrden(@Param("nuOrden") String nuOrden);

  @Query(value = "SELECT count(alcOrden.ID) FROM TMCT0_ALC_ORDEN alcOrden WHERE alcOrden.nuorden = :nuOrden "
      + "AND alcOrden.nbooking = '0' AND alcOrden.nucnfliq = 0 AND alcOrden.nucnfclt = '0' AND "
      + "alcOrden.id NOT IN (:idsOrdenes)", nativeQuery = true)
  public Integer countByNuOrden(@Param("nuOrden") String nuOrden, @Param("idsOrdenes") List<Long> idsOrdenes);

  public List<AlcOrdenes> findByNetting(Netting netting);

  public AlcOrdenes findByReferenciaS3(String referencia_s3);

  @Query("SELECT alcOrden FROM AlcOrdenes alcOrden WHERE alcOrden.netting.id = :idNetting")
  public List<AlcOrdenes> findByIdNetting(@Param("idNetting") Long idNetting);

  @Query(value = "SELECT orden.ID, orden.TITULOS_ASIGNADOS, orden.TITULOS_ENVIADOS FROM TMCT0_ALC_ORDEN orden "
      + "INNER JOIN TMCT0MOVIMIENTOECC ecc ON orden.CUENTA_COMPENSACION_DESTINO = ecc.CUENTA_COMPENSACION_DESTINO "
      + "AND orden.NUORDEN = ecc.NUORDEN WHERE orden.NUORDEN = :nuOrden AND orden.NBOOKING = '0' AND orden.NUCNFCLT = '0' "
      + "AND orden.NUCNFLIQ = 0 AND ecc.IDDESGLOSECAMARA = :idDesgloseCamara", nativeQuery = true)
  public List<Object[]> findByNuOrden(@Param("nuOrden") String nuOrden,
      @Param("idDesgloseCamara") Integer idDesgloseCamara);

  @Query(value = "SELECT CD_CODIGO FROM TMCT0_CUENTAS_DE_COMPENSACION WHERE CUENTA_CLEARING='S' ORDER BY cd_codigo", nativeQuery = true)
  public List<String> findDistinctCuentaCompensacionDestino();

  @Modifying
  @Query("UPDATE AlcOrdenes alcOrden SET alcOrden.estadoInstruccion = :estado, alcOrden.auditDate = :auditDate, "
      + "alcOrden.auditUser = :auditUser, alcOrden.titulosAsignados = :titulosAsignados WHERE alcOrden.id = :idAlcOrden")
  public void update(@Param("estado") Character estado, @Param("auditDate") Date auditDate,
      @Param("auditUser") String auditUser, @Param("titulosAsignados") BigDecimal titulosAsignados,
      @Param("idAlcOrden") Long id);

  @Modifying
  @Query("UPDATE AlcOrdenes alcOrden SET alcOrden.titulosEnviados = :titulosEnviados WHERE alcOrden.id = :idAlcOrden")
  public void update(@Param("titulosEnviados") BigDecimal titulosEnviados, @Param("idAlcOrden") Long id);

  @Modifying
  @Query(value = "INSERT INTO TMCT0_ALC_ORDEN (AUDIT_DATE, AUDIT_USER, ID_ESTADO, IDNETTING, "
      + "NBOOKING, NUCNFCLT, NUCNFLIQ, NUORDEN, CUENTA_COMPENSACION_DESTINO, TITULOS_ENVIADOS, TITULOS_ASIGNADOS, ESTADO) "
      + "VALUES (CURRENT DATE, 'SIBBAC', :idEstado, NULL, '0', '0', 0, :nuOrden, :cuentaCompensacionDestino, :titEnviados, "
      + ":titAsignados, 'A')", nativeQuery = true)
  public void insert(@Param("idEstado") Integer idEstado, @Param("nuOrden") String nuOrden,
      @Param("cuentaCompensacionDestino") String cuentaCompensacionDestino,
      @Param("titEnviados") BigDecimal titEnviados, @Param("titAsignados") BigDecimal titAsignados);

  @Modifying
  @Query("UPDATE AlcOrdenes alcOrden SET alcOrden.referenciaS3 = :referenciaS3 WHERE alcOrden.id = :idAlcOrden")
  public void update(@Param("referenciaS3") String referenciaS3, @Param("idAlcOrden") Long id);

}