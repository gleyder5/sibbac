package sibbac.business.wrappers.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.S3ErroresDao;
import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.database.bo.AbstractBo;


@Service
public class S3ErroresBo extends AbstractBo< S3Errores, Long, S3ErroresDao > {

	public List< S3Errores > findAll() {
		return dao.findAll();
	}

	public List< S3Errores > findByIdLiquidacion( Long idLiquidacion ) {
		return dao.findByIdLiquidacion( idLiquidacion );
	}

}
