package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sibbac.business.wrappers.database.model.Tmct0cli;

@Repository
public interface Tmct0ClientDao extends JpaRepository<Tmct0cli, Long> {
	
	  @Query("SELECT cl FROM Tmct0cli cl order by cl.cdbrocli asc")
	  public List<Tmct0cli> findClientsOrderByClient(String descripCliente);
 
}
