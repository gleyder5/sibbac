package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.S3Errores;


@Repository
public interface S3ErroresDao extends JpaRepository< S3Errores, Long > {

	@Query( "SELECT E FROM S3Errores E WHERE E.s3liquidacion.id=:idliquidacion " )
	List< S3Errores > findByIdLiquidacion( @Param( "idliquidacion" ) Long idLiquidacion );

}
