package sibbac.business.wrappers.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tval0ps1Bo;
import sibbac.business.fase0.database.bo.Tval0ps2Bo;
import sibbac.business.fase0.database.bo.Tval0vpbBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.common.Results;
import sibbac.business.wrappers.database.bo.CodigoErrorBo;
import sibbac.business.wrappers.database.bo.CodigoTpidentiBo;
import sibbac.business.wrappers.database.bo.CodigoTpnactitBo;
import sibbac.business.wrappers.database.bo.CodigoTpsociedBo;
import sibbac.business.wrappers.database.bo.CodigoTptiprepBo;
import sibbac.business.wrappers.database.bo.Tmct0AfiErrorBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo.ModifiedFields;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0paisesBo;
import sibbac.business.wrappers.database.bo.Tmct0provinciasBo;
import sibbac.business.wrappers.database.bo.partenonReader.ImportTitularesExcelFileReader;
import sibbac.business.wrappers.database.bo.partenonReader.TitularesExternalValidator;
import sibbac.business.wrappers.database.dao.Tmct0gaeDao;
import sibbac.business.wrappers.database.dto.CodigoErrorDTO;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.dto.Tmct0ejeDTO;
import sibbac.business.wrappers.database.model.CodigoError;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0AfiError;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0afiId;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.rest.filter.TitularesFilterImpl;
import sibbac.common.FormatStyle;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.DBConstants;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACDataTableServiceBean;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.session.UserSession;

/**
 * Servicio que ofrece las operaciones disponibles sobre los titulares.
 * 
 * @author XI316153
 * @see SIBBACServiceBean
 */
@SIBBACService
public class SIBBACServiceTitulares implements SIBBACServiceBean, SIBBACDataTableServiceBean {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceTitulares.class);

  // Constantes con las operaciones disponibles
  private static final String CMD_ERROR_LIST = "getErrorsList";
  private static final String CMD_GET_TPIDENTI_LIST = "getTpidentiList";
  private static final String CMD_GET_TPNACTIT_LIST = "getTpnactitList";
  private static final String CMD_GET_TPSOCIED_LIST = "getTpsociedList";
  private static final String CMD_GET_TPTIPREP_LIST = "getTptiprepList";
  private static final String CMD_GET_PAISRES_LIST = "getPaisResList";
  private static final String CMD_GET_TP_DOMICI_LIST = "getTpDomiciList";
  private static final String CMD_GET_NACIONALIDAD_LIST = "getNacionalidadList";
  private static final String CMD_GET_LIST_ERRORES_TITULAR = "getListErroresTitular";

  // Constantes con el nombre del parámetro que devuelven las operaciones
  private static final String RESULT_ERROR_LIST = "errorList";
  private static final String RESULTADOS_TP_IDENTI = "resultados_tpidenti";
  private static final String RESULTADOS_TP_NACTIT = "resultados_tpnactit";
  private static final String RESULTADOS_TP_SOCIED = "resultados_tpsocied";
  private static final String RESULTADOS_TP_TIPREP = "resultados_tptiprep";

  // Constantes con el nombre de los parámetros recibidos desde la pantalla
  private static final String FILTER_FECHA_DESDE = "fechaDesde";
  private static final String FILTER_FECHA_HASTA = "fechaHasta";
  private static final String FILTER_NUDNICIF = "nudnicif";
  private static final String FILTER_TIPNAC = "tpnactit";
  private static final String FILTER_REF_CLIENTE = "refCliente";
  private static final String FILTER_NBCLIENT = "nbcliente";
  private static final String FILTER_NBCLIENT1 = "nbcliente1";
  private static final String FILTER_NBCLIENT2 = "nbcliente2";
  private static final String FILTER_PAISRES = "cddepais";
  private static final String FILTER_NB_DOMICILIO = "nbdomici";
  private static final String FILTER_TP_DOMICILIO = "tpdomici";
  private static final String FILTER_NU_DOMICILIO = "nudomici";
  private static final String FILTER_PROVINCIA = "nbprovin";
  private static final String FILTER_COD_POSTAL = "cdpostal";
  private static final String FILTER_TIPDOC = "tpidenti";
  private static final String FILTER_CIUDAD = "ciudad";

  private static final String FILTER_NUORDEN = "nuorden";
  private static final String FILTER_NBOOKING = "nbooking";
  private static final String FILTER_NUCNFCLT = "nucnfclt";
  private static final String FILTER_NUCNFLIQ = "nucnfliq";

  private static final String CMD_NEW_TITULARES = "createTitulares";

  private static final String CMD_UPDATE_TITULARES_LIST = "updateTitularesList";

  private static final String CMD_DELETE_TITULAR = "deleteTitular";
  private static final String CMD_UPDATE_TITULARES_MASSIVE = "updateTitularesMassive";
  private static final String CMD_CHECK_AFI_ERRORS = "checkAfiErrors";

  private static final String CMD_GET_EJECUCIONES_FROM_NUREFORD = "getEjecucionesFromNureford";
  private static final String CMD_GET_EJECUCIONES_FROM_BOOKING = "getEjecucionesFromBooking";

  private static final String CMD_GET_ORD_DATA = "getOrdData";

  private static final String CMD_GET_IMPORT_TITULARES_EXCEL = "importTitularesExcel";
  private static final String CMD_GET_NREGISTROS_QUERY = "getNRegistrosQuery";

  private static final String FILTER_NACIONALIDAD = "cdnactit";

  private static final String FILTER_TPSOCIED = "tpsocied";
  private static final String FILTER_TPTIPREP = "tptiPrep";
  private static final String FILTER_PARTICIP = "particip";
  private static final String FILTER_CCV = "ccv";

  private static final String RESULTADOS_MENSAJE = "mensaje";
  private static final String RESULTADOS_ES_BLOQUEANTE = "esBloqueante";
  private static final String RESULTADOS_CORRECT = "Correct";

  private static final String RESULTADOS_EJECUCIONES = "resultados_ejecuciones";

  private static final String FILTER_SALTA_FILTRO = "saltaFiltro";

  private static final String FILTER_FILE = "file";
  private static final String FILTER_FILENAME = "filename";

  // Filters Old for massive
  private static final String FILTER_NBCLIENT_OLD = "nbclientold";
  private static final String FILTER_NBCLIENT1_OLD = "nbclient1old";
  private static final String FILTER_NBCLIENT2_OLD = "nbclient2old";
  private static final String FILTER_NACIONALIDAD_OLD = "nacionalidadold";
  private static final String FILTER_NUDNICIF_OLD = "nudnicifold";
  private static final String FILTER_TIPNAC_OLD = "tipnacold";
  private static final String FILTER_TIPDOC_OLD = "tipoDocumentoold";
  private static final String FILTER_PAISRES_OLD = "paisResold";
  private static final String FILTER_PROVINCIA_OLD = "provinciaold";
  private static final String FILTER_COD_POSTAL_OLD = "codpostalold";
  private static final String FILTER_TP_DOMICILIO_OLD = "tpdomiciold";
  private static final String FILTER_NB_DOMICILIO_OLD = "nbdomiciold";
  private static final String FILTER_NU_DOMICILIO_OLD = "nudomiciold";
  private static final String FILTER_CIUDAD_OLD = "ciudadold";
  private static final String FILTER_TPTIPREP_OLD = "tptiPrepold";
  private static final String FILTER_PARTICIP_OLD = "participold";
  private static final String FILTER_CCV_OLD = "ccvold";
  private static final String FILTER_NUREFORD_OLD = "nurefordold";
  private static final String FILTER_TPSOCIED_OLD = "tpsociedold";

  private static final String RESULT_NREGISTROS_QUERY = "nRegistrosQuery";

  private static final int TAMANIO_EXECUTOR = 25;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${sibbac.wrappers.routingTitulares.nRegistrosEnLaQuery}")
  private Integer step;

  @Autowired
  private CodigoErrorBo codBo;

  @Autowired
  private Tmct0AfiErrorBo afiErrorBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0gaeDao gaeDao;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private CodigoTpidentiBo tpidentiBo;

  @Autowired
  private CodigoTpnactitBo tpnactitBo;

  @Autowired
  private CodigoTpsociedBo tpsociedBo;

  @Autowired
  private CodigoTptiprepBo tptiprepBo;

  @Autowired
  private Tval0vpbBo tpdomiciBo;

  @Autowired
  private Tval0ps1Bo paisBo;

  @Autowired
  private Tval0ps2Bo paisNacBo;

  @Autowired
  private Tmct0paisesBo paisesBo;

  @Autowired
  private HttpSession session;

  /** Objeto para el acceso a la tabla <code>TMCT0PROVINCIAS</code>. */
  @Autowired
  protected Tmct0provinciasBo tmct0provinciasBo;

  @Autowired
  protected Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private TitularesExternalValidator externalValidator;

  @Qualifier(value = "importTitularesExcelFileReader")
  @Autowired
  private ImportTitularesExcelFileReader reader;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0aloBo tmct0aloBo;

  @Autowired
  private Tmct0menBo menBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see
   * sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.WebRequest
   * )
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    final String prefix = "[SIBBACServiceTitulares::process] ";
    LOG.debug(prefix + "Processing a web request...");

    final String command = webRequest.getAction();
    LOG.debug(prefix + "+ Command.: [{}]", command);

    WebResponse webResponse = new WebResponse();

    switch (command) {
      case CMD_ERROR_LIST:
        getErrorList(webRequest, webResponse);
        break;
      case CMD_GET_TPIDENTI_LIST:
        getTpidentiList(webRequest, webResponse);
        break;
      case CMD_GET_TPNACTIT_LIST:
        getTpnactitList(webRequest, webResponse);
        break;
      case CMD_GET_TPSOCIED_LIST:
        getTpsociedList(webRequest, webResponse);
        break;
      case CMD_GET_TPTIPREP_LIST:
        getTptiprepList(webRequest, webResponse);
        break;
      case CMD_GET_PAISRES_LIST:
        getPaisResList(webRequest, webResponse);
        break;
      case CMD_GET_TP_DOMICI_LIST:
        getTpDomiciList(webRequest, webResponse);
        break;
      case CMD_GET_NACIONALIDAD_LIST:
        getNacionalidadList(webRequest, webResponse);
        break;
      case CMD_NEW_TITULARES:
        createTitulares(webRequest, webResponse);
        break;
      case CMD_GET_NREGISTROS_QUERY:
        getNRegistrosQuery(webRequest, webResponse);
        break;
      case CMD_UPDATE_TITULARES_LIST:
        updateTitularesList(webRequest, webResponse);
        break;
      case CMD_DELETE_TITULAR:
        deleteTitular(webRequest, webResponse);
        break;
      case CMD_UPDATE_TITULARES_MASSIVE:
        if (updateTitularesMassiveCheckFilters(webRequest, webResponse)) {
          updateTitularesMassive(webRequest, webResponse);
        }
        break;
      case CMD_CHECK_AFI_ERRORS:
        checkAfiErrors(webRequest, webResponse);
        break;
      case CMD_GET_EJECUCIONES_FROM_NUREFORD:
        getEjecucionesFromNuorden(webRequest, webResponse);
        break;
      case CMD_GET_EJECUCIONES_FROM_BOOKING:
        getEjecucionesBooking(webRequest, webResponse);
        break;
      case CMD_GET_ORD_DATA:
        getOrdData(webRequest, webResponse);
        break;
      case CMD_GET_IMPORT_TITULARES_EXCEL:
        importTitularesExcel(webRequest, webResponse);
        break;

      case CMD_GET_LIST_ERRORES_TITULAR:
        getListErroresTitular(webRequest, webResponse);
        break;
      default:
        webResponse.setError(command + " is not a valid action. Valid actions are: " + getAvailableCommands());
        break;
    } // switch

    return webResponse;
  } // process

  /*
   * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
   */
  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_ERROR_LIST);
    commands.add(CMD_GET_TPIDENTI_LIST);
    commands.add(CMD_GET_TPNACTIT_LIST);
    commands.add(CMD_GET_TPSOCIED_LIST);
    commands.add(CMD_GET_TPTIPREP_LIST);
    commands.add(CMD_GET_PAISRES_LIST);
    commands.add(CMD_GET_TP_DOMICI_LIST);
    commands.add(CMD_GET_NACIONALIDAD_LIST);
    commands.add(CMD_NEW_TITULARES);
    commands.add(CMD_UPDATE_TITULARES_LIST);
    commands.add(CMD_UPDATE_TITULARES_MASSIVE);
    commands.add(CMD_CHECK_AFI_ERRORS);
    commands.add(CMD_GET_EJECUCIONES_FROM_NUREFORD);
    commands.add(CMD_GET_ORD_DATA);
    return commands;
  } // getAvailableCommands

  @Override
  public void queryDataTable(Map<String, String[]> params, StreamResult streamResult) {
    final TitularesFilterImpl filter;

    filter = new TitularesFilterImpl();
    filter.parseParameters(params);
    afiBo.findFiltered(filter, FormatStyle.JSON_ARRAY, streamResult);
  }

  @Override
  public void queryExport(Map<String, String[]> params, FormatStyle formatStyle, StreamResult streamResult) {
    final TitularesFilterImpl filter;

    filter = new TitularesFilterImpl();
    filter.parseParameters(params);
    filter.setExportation(true);
    afiBo.findFiltered(filter, formatStyle, streamResult);
  }

  @Override
  @Deprecated
  public List<String> getFields() {
    return null;
  }

  /**
   * Devuelve la lista de códigos de error posibles.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getErrorList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getErrorList] Inicio ...");

    final Map<String, List<CodigoErrorDTO>> resultados = new HashMap<String, List<CodigoErrorDTO>>();
    List<CodigoErrorDTO> listaErrores = new ArrayList<CodigoErrorDTO>();

    for (CodigoError codError : codBo.findAllByGrupoOrderByCodigoAsc("Routing Titulares")) {
      listaErrores.add(new CodigoErrorDTO(codError));
    } // for

    resultados.put(RESULT_ERROR_LIST, listaErrores);
    webResponse.setResultados(resultados);

    LOG.debug("[getErrorList] Fin ...");
  } // getErrorList

  /**
   * Devuelve la lista de tipos de identidad.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getTpidentiList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getTpidentiList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap(RESULTADOS_TP_IDENTI, tpidentiBo.findAll()));
    LOG.trace("[getTpidentiList] Fin ...");
  } // getTpidentiList

  /**
   * Devuelve la lista de tipos de nacionalidad.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getTpnactitList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getTpnactitList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap(RESULTADOS_TP_NACTIT, tpnactitBo.findAll()));
    LOG.trace("[getTpnactitList] Fin ...");
  } // getTpnactitList

  /**
   * Devuelve la loista de tipos de sociedad.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getTpsociedList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getTpsociedList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap(RESULTADOS_TP_SOCIED, tpsociedBo.findAll()));
    LOG.trace("[getTpsociedList] Fin ...");
  } // getTpsociedList

  /**
   * Devuelve la lista de tipos de representantes.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getTptiprepList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getTptiprepList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap(RESULTADOS_TP_TIPREP, tptiprepBo.findAll()));
    LOG.trace("[getTptiprepList] Fin ...");
  } // getTptiprepList

  /**
   * Devuelve la lista de paises de residencia.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getPaisResList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getPaisResList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap("resultados_paisres", paisBo.findAllActiveOrderByCdiso2po()));
    LOG.trace("[getPaisResList] Fin ...");
  } // getPaisResList

  /**
   * Devuelve la lista de tipos de domicilio.
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getTpDomiciList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getTpDomiciList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap("resultados_tpdomici", tpdomiciBo.findAll()));
    LOG.trace("[getTpDomiciList] Fin ...");
  } // getTpDomiciList

  /**
   * Devuelve la lista de nacionalidades
   * 
   * @param webRequest Petición web.
   * @param webResponse Respuesta web.
   */
  private void getNacionalidadList(WebRequest webRequest, WebResponse webResponse) {
    LOG.trace("[getNacionalidadList] Inicio ...");
    webResponse.setResultados(Collections.singletonMap("resultados_nacionalidad",
        paisNacBo.findAllActiveOrderByCdiso2po()));
    LOG.trace("[getNacionalidadList] Fin ...");
  } // getNacionalidadList

  /**
   * Metodo que recibe los datos para crear titulares nuevos y, segun
   * corresponda los guarda en afi o afiError
   * 
   * @param webRequest
   * @param webResponse
   */
  private void createTitulares(WebRequest webRequest, WebResponse webResponse) {
    final ControlTitularesParams ctParams;

    LOG.debug("[createTitulares] Inicio");
    ctParams = new ControlTitularesParams();
    ctParams.cargaParametros(webRequest.getParams());
    webResponse.setResultados(afiBo.createTitulares(ctParams));
  }// createTitulares

  private void deleteTitular(WebRequest webRequest, WebResponse webResponse) {
    final Map<String, Object> resultados;
    final ControlTitularesParams ctParams;
    String mensaje = "";
    String msgFor;
    LOG.debug("[deleteTitular] Init");
    ctParams = new ControlTitularesParams();
    ctParams.cargaParametros(webRequest.getParams());
    Tmct0AfiError afiError;
    PartenonRecordBean bean;
    Results nAfiProcessed = new Results();
    Results nAfiFixed = new Results();
    List<Tmct0AfiError> listAfiError = new ArrayList<Tmct0AfiError>();
    List<Tmct0afiId> listAfiId;
    List<Tmct0afi> afis;
    List<Tmct0afi> afisToDelete;

    Map<String, Tmct0paises> isosPaises = paisesBo.findMapAllActivosByIsonnum();

    Map<String, Tmct0paises> isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();

    Map<String, List<String>> nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();
    Map<String, List<Tmct0AfiError>> afiErrorByNureford = new HashMap<String, List<Tmct0AfiError>>();
    Map<Tmct0alcId, List<Tmct0AfiError>> afiErrorByAlcId = new HashMap<Tmct0alcId, List<Tmct0AfiError>>();
    Map<Tmct0alcId, List<Tmct0afi>> afiByAlcId = new HashMap<Tmct0alcId, List<Tmct0afi>>();
    final List<Date> listaFestivos = cfgBo.findHolidaysBolsa();
    if (!ctParams.getListAfiErrorIds().isEmpty()) {

      for (long iderror : ctParams.getListAfiErrorIds()) {
        LOG.debug("[deleteTitular] Se solicita la eliminacion del afiError con Id {}", iderror);
        afiError = afiErrorBo.findById(iderror);
        if (afiError != null) {
          listAfiError.add(afiError);
        }
      }
      this.organizarAfiErroByAlcIdAndNureford(listAfiError, afiErrorByAlcId, afiErrorByNureford);
      
        for (Tmct0alcId alcIdFor : afiErrorByAlcId.keySet()) {
          afiErrorBo.delete(afiErrorByAlcId.get(alcIdFor));
          nAfiProcessed.addnAfiError(afiErrorByAlcId.get(alcIdFor).size());
          if (afiBo.validacionesBloque(
              new Tmct0aloId(alcIdFor.getNucnfclt(), alcIdFor.getNbooking(), alcIdFor.getNuorden()),
              alcIdFor.getNucnfliq(), isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
            nAfiFixed.addnAfiError(afiErrorByAlcId.get(alcIdFor).size());
          }
        }

      for (String nurefordFor : afiErrorByNureford.keySet()) {
        afiErrorBo.delete(afiErrorByNureford.get(nurefordFor));
        nAfiProcessed.addnAfiError(afiErrorByNureford.get(nurefordFor).size());
        if (afiBo.validacionesBloque(nurefordFor, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
          nAfiFixed.addnAfiError(afiErrorByNureford.get(nurefordFor).size());
        }
      }  

      listAfiError.clear();
      afiErrorByAlcId.clear();
      afiErrorByNureford.clear();
    }

    if (!ctParams.getListAfiIds().isEmpty()) {
      Map<Tmct0alcId, List<Tmct0afiId>> mapAfiByAlc = afiBo.getMapAfiIdGroupByAlc(ctParams.getListAfiIds());
      List<Tmct0alcId> listAlcQuitarMap = new ArrayList<Tmct0alcId>();

      for (Tmct0alcId alcId : mapAfiByAlc.keySet()) {

        afiErrorBo.delete(afiErrorBo.findGroupByAlcId(alcId));
        Tmct0alc alc = alcBo.findById(alcId);
        msgFor = "";
        if (alc != null) {
          msgFor = afiBo.comprobacionesALC(alc, listaFestivos);
        }
        if (StringUtils.isBlank(msgFor)) {
          listAfiId = mapAfiByAlc.get(alcId);
          afis = afiBo.findActivosByAlcData(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
              alcId.getNucnfliq());
          nAfiProcessed.addNAfiSynchronized(afis.size());
          afisToDelete = new ArrayList<Tmct0afi>();
          for (Tmct0afi afi : afis) {
            if (listAfiId.contains(afi.getId())) {
              afisToDelete.add(afi);
            }
          }
          if (!afisToDelete.isEmpty()) {
            afis.removeAll(afisToDelete);
          }
          afiByAlcId.put(alcId, afis);
        }
        else {
          nAfiProcessed.addMsg(String.format("%s Orden:'%s'", msgFor, alcId.getNuorden()));
          afiByAlcId.get(alcId).clear();
          listAlcQuitarMap.add(alcId);
        }
      }

      mapAfiByAlc.clear();
      for (Tmct0alcId alcId : listAlcQuitarMap) {
        afiByAlcId.remove(alcId);
      }

      listAlcQuitarMap.clear();

      for (Tmct0alcId alcIdFor : afiByAlcId.keySet()) {
        if (CollectionUtils.isNotEmpty(afiByAlcId.get(alcIdFor))) {
          nAfiProcessed.addNAfiErrorSynchronized(afiByAlcId.get(alcIdFor).size());
          for (Tmct0afi afiFor : afiByAlcId.get(alcIdFor)) {
            bean = new PartenonRecordBean(afiFor);

            afiErrorBo.save(bean.toTmct0afierror(afiBo.getNewTmct0afiSequence() + ""));

          }
          if (afiBo.validacionesBloque(
              new Tmct0aloId(alcIdFor.getNucnfclt(), alcIdFor.getNbooking(), alcIdFor.getNuorden()),
              alcIdFor.getNucnfliq(), isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
            nAfiFixed.addnAfi(afiByAlcId.get(alcIdFor).size());
          }
        }
        else {
          mensaje = "No se puede dejar una orden sin titulares";
        }

      }
      afiByAlcId.clear();
    }
    if (CollectionUtils.isEmpty(ctParams.getListAfiIds()) && CollectionUtils.isEmpty(ctParams.getListAfiErrorIds())) {
      LOG.warn("[deleteTitular] Se ha solicitado eliminacion de titular, pero no se ha enviado correctamente el Id");
      mensaje = Tmct0afiBo.MSJ_BLOQUEANTE_5;
    }

    afiErrorByAlcId.clear();
    afiErrorByNureford.clear();

    resultados = new HashMap<>();

    resultados.put("nAfiErrorProcessed", nAfiProcessed.getnAfiError());
    resultados.put("nAfiProcessed", nAfiProcessed.getnAfi());
    resultados.put("nAfiErrorNotProcessed", 0);
    resultados.put("nAfiNotProcessed", 0);

    resultados.put("afiCreated", 0);
    resultados.put("afiErrorCreated", nAfiProcessed.getnAfiError());
    resultados.put(RESULTADOS_MENSAJE, mensaje);
    resultados.put(RESULTADOS_ES_BLOQUEANTE, true);
    webResponse.setResultados(resultados);
    LOG.debug("[deleteTitular] Fin");
  }

  private void organizarAfiErroByAlcIdAndNureford(List<Tmct0AfiError> afierros,
      Map<Tmct0alcId, List<Tmct0AfiError>> afiErrorByAlcId, Map<String, List<Tmct0AfiError>> afiErrorByNureford) {

    Tmct0aloId aloId;
    Tmct0alcId alcId;
    Short nucnfliq;
    String nureford;

    for (Tmct0AfiError afiError : afierros) {
      if (afiError.getTmct0alo() != null) {
        aloId = afiError.getTmct0alo().getId();
        nucnfliq = afiError.getNucnfliq();
        if (nucnfliq == null) {
          nucnfliq = (short) 0;
        }
        alcId = new Tmct0alcId(aloId, nucnfliq);
        if (afiErrorByAlcId.get(alcId) == null) {
          afiErrorByAlcId.put(alcId, new ArrayList<Tmct0AfiError>());
        }
        afiErrorByAlcId.get(alcId).add(afiError);

      }
      else {
        nureford = afiError.getNureford();
        if (afiErrorByNureford.get(nureford) == null) {
          afiErrorByNureford.put(nureford, new ArrayList<Tmct0AfiError>());
        }
        afiErrorByNureford.get(nureford).add(afiError);
      }
    }
  }

  private void importTitularesExcel(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[importTitularesExcel] Init");
    Map<String, String> filters = webRequest.getFilters();
    final String excelFile = processString(filters.get(FILTER_FILE));
    final String excelFileName = processString(filters.get(FILTER_FILENAME));
    byte[] data = Base64.decodeBase64(excelFile);
    File folderDestino = new File("/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/");
    folderDestino.mkdirs();

    try (OutputStream stream = new FileOutputStream("/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/" + excelFileName)) {
      stream.write(data);
      LOG.debug("[importTitularesExcel] Se guarda el fichero " + "/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/"
          + excelFileName + " por el usuario: <>");

    }
    catch (FileNotFoundException e) {
      LOG.debug("[importTitularesExcel] Falla la subida del fichero "
          + "/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/" + excelFileName + " por el usuario: <>");
    }
    catch (IOException e) {
      LOG.debug("[importTitularesExcel] Falla la subida del fichero "
          + "/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/" + excelFileName + " por el usuario: <>");
    }
    boolean wellProcessed = false;
    try {

      reader.executeTask("/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/" + excelFileName, false,
          StandardCharsets.UTF_16LE);
      wellProcessed = true;
    }
    catch (Exception e) {
      LOG.debug("[importTitularesExcel] Falla el procesamiento del fichero "
          + "/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/" + excelFileName + " por el usuario: <>");
      LOG.warn(excelFileName);
    }
    final Map<String, Boolean> resultados = new HashMap<String, Boolean>();
    resultados.put("wellProcessed", wellProcessed);
    webResponse.setResultados(resultados);
    if (wellProcessed) {
      LOG.debug("[importTitularesExcel] Bien procesado el fichero "
          + "/sbrmv/ficheros/partenon/in/fiscaldata/uploaded/" + excelFileName + " por el usuario: <>");
    }
    LOG.debug("[importTitularesExcel] Fin");
  }

  /**
   * Este metodo intenta devolver los datos nbooking, nuorden, nucnfclt y
   * nucnfliq para el proceso de creacion de titulares.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getOrdData(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[getOrdData] Init");
    Map<String, String> filters = webRequest.getFilters();

    String nuordenRecibido = processString(filters.get(FILTER_NUORDEN));
    String nbookingRecibido = processString(filters.get(FILTER_NBOOKING));
    String nucnfcltRecibido = processString(filters.get(FILTER_NUCNFCLT));
    Short nucnfliqRecibido;
    if (StringUtils.isNotEmpty(filters.get(FILTER_NUCNFLIQ))) {
      try {
        nucnfliqRecibido = Short.parseShort((filters.get(FILTER_NUCNFLIQ)));
      }
      catch (NumberFormatException nfex) {
        LOG.debug("[getOrdData] nucnfliqRecibido con formato incorrecto: {}", filters.get(FILTER_NUCNFLIQ));
        nucnfliqRecibido = null;
      }
    }
    else {
      nucnfliqRecibido = null;
    }
    String nuordendevuelto = "";
    String nbookingdevuelto = "";
    String nucnfcltdevuelto = "";
    Short nucnfliqdevuelto = null;
    boolean isNuordenCorrect = false;
    boolean isNbookingCorrect = false;
    boolean isNucnfcltCorrect = false;
    boolean isNucnfliqCorrect = false;
    boolean isEverythingCorrect = false;
    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
    List<Tmct0alo> alos = new ArrayList<Tmct0alo>();

    Tmct0alc alcfinal = null;

    try {
      alcs = alcBo.findAlcsByIdViaSpecifications(nuordenRecibido, nbookingRecibido, nucnfliqRecibido, nucnfcltRecibido);
    }
    catch (Exception e) {
      LOG.debug("[getOrdData] Error obteniendo resultados de la tabla TMCT0ALC");
    }

    try {
      if (nucnfliqRecibido == null || nucnfliqRecibido == 0) {
        alos = tmct0aloBo.findAlosByIdViaSpecifications(nuordenRecibido, nbookingRecibido, nucnfcltRecibido);
      }
    }
    catch (Exception e) {
      LOG.debug("[getOrdData] Error obteniendo resultados de la tabla TMCT0ALC");
    }

    if (alcs.size() > 0) {
      nuordendevuelto = alcs.get(0).getId().getNuorden().trim();
      nbookingdevuelto = alcs.get(0).getId().getNbooking().trim();
      nucnfcltdevuelto = alcs.get(0).getId().getNucnfclt().trim();
      nucnfliqdevuelto = alcs.get(0).getId().getNucnfliq();
      isNuordenCorrect = true;
      isNbookingCorrect = true;
      isNucnfcltCorrect = true;
      isNucnfliqCorrect = true;

      for (Tmct0alc alc : alcs) {
        if (!nuordendevuelto.equals(alc.getId().getNuorden().trim())) {
          isNuordenCorrect = false;
        }
        if (!nbookingdevuelto.equals(alc.getId().getNbooking().trim())) {
          isNbookingCorrect = false;
        }
        if (!nucnfcltdevuelto.equals(alc.getId().getNucnfclt().trim())) {
          isNucnfcltCorrect = false;
        }
        if (!nucnfliqdevuelto.equals(alc.getId().getNucnfliq())) {
          isNucnfliqCorrect = false;
        }
        if (isNuordenCorrect && isNbookingCorrect && isNucnfcltCorrect && isNucnfliqCorrect) {
          alcfinal = alc;
        }
      }
    }
    else if (alos.size() > 0) {
      nuordendevuelto = alos.get(0).getId().getNuorden().trim();
      nbookingdevuelto = alos.get(0).getId().getNbooking().trim();
      nucnfcltdevuelto = alos.get(0).getId().getNucnfclt().trim();
      nucnfliqdevuelto = 0;
      isNuordenCorrect = true;
      isNbookingCorrect = true;
      isNucnfcltCorrect = true;
      isNucnfliqCorrect = true;

      for (Tmct0alo alo : alos) {
        if (!nuordendevuelto.equals(alo.getId().getNuorden().trim())) {
          isNuordenCorrect = false;
        }
        if (!nbookingdevuelto.equals(alo.getId().getNbooking().trim())) {
          isNbookingCorrect = false;
        }
        if (!nucnfcltdevuelto.equals(alo.getId().getNucnfclt().trim())) {
          isNucnfcltCorrect = false;
        }
      }

    }
    else {

      isNuordenCorrect = false;
      isNbookingCorrect = false;
      isNucnfcltCorrect = false;
      isNucnfliqCorrect = false;
      isEverythingCorrect = false;
    }

    if (isNuordenCorrect && isNbookingCorrect && isNucnfcltCorrect && isNucnfliqCorrect) {
      isEverythingCorrect = true;
    }

    final Map<String, Object> resultados = new HashMap<String, Object>();

    resultados.put(FILTER_NUORDEN, nuordendevuelto);
    resultados.put(FILTER_NBOOKING, nbookingdevuelto);
    resultados.put(FILTER_NUCNFCLT, nucnfcltdevuelto);
    resultados.put(FILTER_NUCNFLIQ, nucnfliqdevuelto);

    resultados.put(FILTER_NUORDEN + RESULTADOS_CORRECT, isNuordenCorrect);
    resultados.put(FILTER_NBOOKING + RESULTADOS_CORRECT, isNbookingCorrect);
    resultados.put(FILTER_NUCNFCLT + RESULTADOS_CORRECT, isNucnfcltCorrect);
    resultados.put(FILTER_NUCNFLIQ + RESULTADOS_CORRECT, isNucnfliqCorrect);

    resultados.put("isCorrect", isEverythingCorrect);
    if (isEverythingCorrect) {
      // Si todo esta correcto comprobamos si se ha de enviar mensaje de
      // error
      if (alcfinal != null && alcfinal.getEstadoentrec() != null
          && alcfinal.getEstadoentrec().getIdestado() > EstadosEnumerados.CLEARING.ACEPTADA_ANULACION.getId()) {
        resultados
            .put(RESULTADOS_MENSAJE,
                "Antes de poder modificar los titulares se debe solicitar rechazo a S3 de la instrucción comunicada. ¿Desea continuar?");
      }
    }
    webResponse.setResultados(resultados);
    LOG.debug("[getOrdData] Fin");

  }

  /**
   * Metodo que devuelve las ejecuciones asociadas a un numero de orden para que
   * se puedan mostrar en la pantalla
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getEjecucionesFromNuorden(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[getEjecucionesFromNuorden] Init");
    Map<String, String> filters = webRequest.getFilters();
    final String nureford = processString(filters.get(FILTER_REF_CLIENTE));

    List<Tmct0eje> ejes = ordBo.findEjesByNureford(nureford);
    final Map<String, List<Tmct0ejeDTO>> resultados = new HashMap<String, List<Tmct0ejeDTO>>();

    List<Tmct0ejeDTO> ejesDTO = new ArrayList<Tmct0ejeDTO>();

    for (Tmct0eje eje : ejes) {
      boolean foundPartner = false;
      for (Tmct0ejeDTO ejeDto : ejesDTO) {
        if (eje.getImcbmerc().compareTo(ejeDto.getImcbmerc()) == 0) {
          ejeDto.addNutiteje(eje.getNutiteje());
          foundPartner = true;
          break;
        }
      }
      if (!foundPartner) {
        ejesDTO.add(new Tmct0ejeDTO(eje));
      }
    }

    resultados.put(RESULTADOS_EJECUCIONES, ejesDTO);
    webResponse.setResultados(resultados);
    LOG.debug("[getEjecucionesFromNuorden] Fin");
  }

  /**
   * Metodo que devuelve las ejecuciones asociadas a un numero de orden para que
   * se puedan mostrar en la pantalla
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getEjecucionesBooking(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[getEjecucionesAlc] Init");
    Tmct0ejeDTO eje;
    Map<String, String> filters = webRequest.getFilters();
    final Map<String, List<Tmct0ejeDTO>> resultados = new HashMap<String, List<Tmct0ejeDTO>>();
    List<Tmct0ejeDTO> ejesDTO = new ArrayList<Tmct0ejeDTO>();

    final String nuorden = processString(filters.get(FILTER_NUORDEN));
    final String nbooking = processString(filters.get(FILTER_NBOOKING));
    final String nucnfclt = processString(filters.get(FILTER_NUCNFCLT));
    final short nucnfliq = Short.parseShort(processString(filters.get(FILTER_NUCNFLIQ)));

    List<Object[]> precios;
    if (nucnfliq > 0) {
      precios = dctBo.sumTitulosByPriceAndAlcId(new Tmct0alcId(nbooking, nuorden, nucnfliq, nucnfclt));
    }
    else {
      precios = gaeDao.sumTitulosByPriceAndAloId(nuorden, nbooking, nucnfclt);
    }

    for (Object[] o : precios) {
      eje = new Tmct0ejeDTO();
      eje.setImcbmerc((BigDecimal) o[0]);
      eje.setNutiteje((BigDecimal) o[1]);
      ejesDTO.add(eje);
    }

    resultados.put(RESULTADOS_EJECUCIONES, ejesDTO);
    webResponse.setResultados(resultados);
    LOG.debug("[getEjecucionesFromNuorden] Fin");
  }

  /**
   * Metodo que devuelve las ejecuciones asociadas a un numero de orden para que
   * se puedan mostrar en la pantalla
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getListErroresTitular(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[getListErroresTitular] Init");
    Map<String, String> filters = webRequest.getFilters();
    final String idtitular = (String) filters.get("id");
    final Map<String, List<Map<String, String>>> resultados = new HashMap<String, List<Map<String, String>>>();
    final long id = Long.parseLong(idtitular);
    List<Map<String, String>> errorList;
    errorList = afiErrorBo.getListErroresTitular(id);
    resultados.put("listaErrores", errorList);
    webResponse.setResultados(resultados);
    LOG.debug("[getListErroresTitular] Fin");
  }

  /**
   * Comprueba los AFI_ERROR que hay sin errores y trae al resto de titulares de
   * esa orden. Si consigue que toda la oren este sin errores los manda a AFI.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void checkAfiErrors(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[checkAfiErrors] Init");
    Results fixedAfis = new Results();

    Map<String, Object> resultados = new HashMap<String, Object>();
    Tmct0men semaforo = null;

    try {
      semaforo = menBo.putEstadoMEN(TIPO_APUNTE.TASK_ROUTING_TITULARES, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);
    }
    catch (SIBBACBusinessException e1) {
      LOG.debug("No he mos podido bloquear el semaroro");
      DecimalFormat df = new DecimalFormat("000");
      semaforo = menBo.findByCdmensa(df.format(TIPO_APUNTE.TASK_ROUTING_TITULARES.getTipo()));
      if (semaforo != null) {
        resultados.put(RESULTADOS_MENSAJE, String.format(
            "Otro usuario/proceso esta tratando titulares, usuario: '%s' hora ejecucion: '%s'",
            StringUtils.trim(semaforo.getCdusuaud()), semaforo.getFhaudit()));
      }
      else {
        resultados.put(RESULTADOS_MENSAJE, "No se ha podido bloquear el proceso.");
      }
      webResponse.setResultados(resultados);
      return;
    }
    try {

      Map<String, Tmct0paises> isosPaises = paisesBo.findMapAllActivosByIsonnum();

      Map<String, Tmct0paises> isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();

      Map<String, List<String>> nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();

      String usuarioAuditoria = (((UserSession) session.getAttribute("UserSession")).getName().length() >= 10) ? ((UserSession) session
          .getAttribute("UserSession")).getName().substring(0, 10)
          : ((UserSession) session.getAttribute("UserSession")).getName();

      // Se coge la lista de festivos para la actualizacion de estados del alc
      List<Date> festivosList = cfgBo.findHolidaysBolsa();

      LOG.debug("[checkAfiErrors] Se van a obtener los que llegaron via nureford");
      List<Tmct0AfiError> afiErrorsNureford = afiErrorBo.findAllWithNurefordAndNoErrors();
      // Por nureford
      LOG.debug("[checkAfiErrors] Se van a comprobar {} de los que llegaron via nureford", afiErrorsNureford.size());
      checkAfiErrorsViaNureford(afiErrorsNureford, fixedAfis, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito,
          festivosList, usuarioAuditoria);

      LOG.debug("[checkAfiErrors] Se van a obtener los que llegaron via alc");
      List<Tmct0AfiError> afiErrorsAlc = afiErrorBo.findAllWithAlcAndNoErrors();
      // Por alc
      LOG.debug("[checkAfiErrors] Se van a comprobar {} de los que llegaron via ALC (nacionales)", afiErrorsAlc.size());
      checkAfiErrorsViaAlc(afiErrorsAlc, fixedAfis, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito,
          festivosList, usuarioAuditoria);

      LOG.debug("[checkAfiErrors] Se van a obtener los que llegaron via alo");
      List<Tmct0AfiError> afiErrorsAlo = afiErrorBo.findAllWithAloAndNoErrors();
      // Por alo
      LOG.debug("[checkAfiErrors] Se van a comprobar {} de los que llegaron via ALO (internacionales)",
          afiErrorsAlo.size());
      checkAfiErrorsViaAlo(afiErrorsAlo, fixedAfis, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito,
          festivosList, usuarioAuditoria);
      menBo.updateFhauditAndFemensaAndHrmensaAndIncrementNuenvio(semaforo, Calendar.getInstance());
      menBo.putEstadoMEN(semaforo, TMCT0MSC.LISTO_GENERAR);
      resultados.put("fixedAfis", fixedAfis.getnAfi());
      webResponse.setResultados(resultados);
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
      menBo.putEstadoMEN(semaforo, TMCT0MSC.EN_ERROR);
    }

    LOG.debug("[checkAfiErrors] Fin");
  }

  /**
   * Método que recibe una lista de afiErrors que se agrupan por nureford e
   * intenta comprobar si pueden pasar a AFI
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void checkAfiErrorsViaNureford(List<Tmct0AfiError> afiErrorsNureford, Results fixedAfis,
      Map<String, Tmct0paises> isosPaises, Map<String, Tmct0paises> isosPaisesHacienda,
      Map<String, List<String>> nombresCiudadPorDistrito, List<Date> festivosList, String usuarioAuditoria) {
    List<PartenonRecordBean> beanList = new ArrayList<PartenonRecordBean>();

    if (afiErrorsNureford.size() > 0) {
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);
      try {
        String lastNureford = null;
        for (Tmct0AfiError afiError : afiErrorsNureford) {
          LOG.debug("[checkAfiErrorsViaNureford] nureford: {}", afiError.getNureford());
          if (lastNureford != null && !lastNureford.equals(afiError.getNureford())) {
            // Tenemos que guardar los anteriores
            LOG.debug("[checkAfiErrorsViaNureford] find parners with incidence by nureford: {}", afiError.getNureford());
            if (CollectionUtils.isNotEmpty(beanList)) {
              this.executeCheckErrorsThreaded(executor, beanList, fixedAfis, isosPaises, isosPaisesHacienda,
                  nombresCiudadPorDistrito, festivosList, usuarioAuditoria);
            }
            beanList = new ArrayList<PartenonRecordBean>();
          }
          beanList.add(new PartenonRecordBean(afiError));
          lastNureford = afiError.getNureford();
        }

        // Ultima iteracion
        if (CollectionUtils.isNotEmpty(beanList)) {
          this.executeCheckErrorsThreaded(executor, beanList, fixedAfis, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, festivosList, usuarioAuditoria);
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
      }
      finally {
        if (!executor.isShutdown()) {
          executor.shutdown();
        }
        try {
          executor.awaitTermination(10, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
        if (!executor.isShutdown()) {
          executor.shutdownNow();
          try {
            executor.awaitTermination(2, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
        LOG.debug("[checkAfiErrorsViaNureford] terminado Executor: {}", executor);
      }
    }
  }

  /**
   * Método que recibe una lista de afiErrors que se agrupan por alc e intenta
   * comprobar si pueden pasar a AFI
   * 
   * @param afiErrorsAlc
   * @param fixedAfis
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void checkAfiErrorsViaAlc(List<Tmct0AfiError> afiErrorsAlc, Results fixedAfis,
      Map<String, Tmct0paises> isosPaises, Map<String, Tmct0paises> isosPaisesHacienda,
      Map<String, List<String>> nombresCiudadPorDistrito, List<Date> festivosList, String usuarioAuditoria) {
    List<PartenonRecordBean> beanList = new ArrayList<PartenonRecordBean>();

    if (afiErrorsAlc.size() > 0) {
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);

      try {
        Tmct0alcId lastAlcId = null;
        for (Tmct0AfiError afiError : afiErrorsAlc) {
          if (lastAlcId != null
              && !lastAlcId.equals(new Tmct0alcId(afiError.getTmct0alo().getId(), afiError.getNucnfliq()))) {
            // Tenemos que guardar los anteriores
            LOG.debug("[checkAfiErrorsViaAlc] find parners with incidence by alcid: {}", lastAlcId);
            if (CollectionUtils.isNotEmpty(beanList)) {
              this.executeCheckErrorsThreaded(executor, beanList, fixedAfis, isosPaises, isosPaisesHacienda,
                  nombresCiudadPorDistrito, festivosList, usuarioAuditoria);
            }

            beanList = new ArrayList<PartenonRecordBean>();
          }
          beanList.add(new PartenonRecordBean(afiError));
          lastAlcId = new Tmct0alcId(afiError.getTmct0alo().getId(), afiError.getNucnfliq());
        }

        // Ultima iteracion
        if (CollectionUtils.isNotEmpty(beanList)) {
          this.executeCheckErrorsThreaded(executor, beanList, fixedAfis, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, festivosList, usuarioAuditoria);
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
      }
      finally {
        if (!executor.isShutdown()) {
          executor.shutdown();
        }
        try {
          executor.awaitTermination(10, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
        if (!executor.isShutdown()) {
          executor.shutdownNow();
          try {
            executor.awaitTermination(2, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
        LOG.debug("[checkAfiErrorsViaAlc] terminado Executor: {}", executor);
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void executeCheckErrorsThreaded(Executor executor, List<PartenonRecordBean> bloqueMismaOrden,
      final Results fixedAfis, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> festivosList, final String usuarioAuditoria) {
    final List<Long> lToThread = new ArrayList<Long>();
    for (PartenonRecordBean bean : bloqueMismaOrden) {
      lToThread.add(bean.getAfiError().getId());

    }
    Runnable process = new Runnable() {

      @Override
      @Transactional(propagation = Propagation.REQUIRES_NEW)
      public void run() {
        try {
          checkSavePreviousBlock(lToThread, fixedAfis, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito,
              festivosList, usuarioAuditoria);
        }
        catch (Exception e) {
          LOG.warn(e.getMessage(), e);
        }
        finally {

        }

      }
    };
    executor.execute(process);

    LOG.debug("[executeCheckErrorsThreaded] lanzado proceso beanList: {} - Executor: {}", lToThread, executor);
  }

  /**
   * Método que recibe una lista de afiErrors que se agrupan por alo e intenta
   * comprobar si pueden pasar a AFI
   * 
   * @param afiErrorsAlc
   * @param fixedAfis
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void checkAfiErrorsViaAlo(List<Tmct0AfiError> afiErrorsAlo, Results fixedAfis,
      Map<String, Tmct0paises> isosPaises, Map<String, Tmct0paises> isosPaisesHacienda,
      Map<String, List<String>> nombresCiudadPorDistrito, List<Date> festivosList, String usuarioAuditoria) {
    List<PartenonRecordBean> beanList = new ArrayList<PartenonRecordBean>();
    if (afiErrorsAlo.size() > 0) {
      Tmct0aloId lastAloId = null;
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);
      try {

        for (Tmct0AfiError afiError : afiErrorsAlo) {
          if (lastAloId != null && !lastAloId.equals(afiError.getTmct0alo().getId())) {
            // Tenemos que guardar los anteriores
            LOG.debug("[checkAfiErrorsViaAlo] find parners with incidence by aloid: {}", afiError.getTmct0alo().getId());
            if (CollectionUtils.isNotEmpty(beanList)) {
              this.executeCheckErrorsThreaded(executor, beanList, fixedAfis, isosPaises, isosPaisesHacienda,
                  nombresCiudadPorDistrito, festivosList, usuarioAuditoria);

            }
            beanList = new ArrayList<PartenonRecordBean>();
          }
          beanList.add(new PartenonRecordBean(afiError));
          lastAloId = afiError.getTmct0alo().getId();
        }

        // Ultima iteracion
        if (CollectionUtils.isNotEmpty(beanList)) {
          this.executeCheckErrorsThreaded(executor, beanList, fixedAfis, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, festivosList, usuarioAuditoria);
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
      }
      finally {
        if (!executor.isShutdown()) {
          executor.shutdown();
        }
        try {
          executor.awaitTermination(10, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
        if (!executor.isShutdown()) {
          executor.shutdownNow();
          try {
            executor.awaitTermination(2, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
        LOG.debug("[checkAfiErrorsViaAlo] terminado Executor: {}", executor);
      }
    }
  }

  /**
   * Guarda un bloque de afiErrors en Afi una vez se ha chequeado que no tienen
   * errores.
   * 
   * @param bloqueMismaOrden
   * @param fixedAfis
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private void checkSavePreviousBlock(List<Long> ids, Results fixedAfis, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito,
      List<Date> festivosList, String usuarioAuditoria) {
    LOG.trace("[SIBBACServiceTitulares :: checkSavePreviousBlock] Init");
    Map<PartenonRecordBean, List<String>> errorsByBean = new HashMap<PartenonRecordBean, List<String>>();

    // Se decide si el bloque ira a AFI o a AFI_ERROR
    boolean hasErrors = false;

    List<PartenonRecordBean> bloqueMismaOrden = new ArrayList<PartenonRecordBean>();

    for (Long id : ids) {
      bloqueMismaOrden.add(new PartenonRecordBean(afiErrorBo.findById(id)));
    }

    // Se comprueban los errores del ord
    String nureford = bloqueMismaOrden.get(0).getNumOrden();
    String erroresNumorden = null;
    List<ControlTitularesDTO> alcs = new ArrayList<ControlTitularesDTO>();
    List<ControlTitularesDTO> alos = new ArrayList<ControlTitularesDTO>();

    String errorProcedencia = afiBo.comprobarErroresProcedencia(bloqueMismaOrden, alcs, alos);

    // Comprobamos los errores de grupo

    hasErrors = externalValidator.validateAll(erroresNumorden, bloqueMismaOrden, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito, errorsByBean);
    hasErrors = hasErrors || StringUtils.isNotBlank(errorProcedencia);

    Short cdbloque = 0;
    boolean borrarAfiErrAntiguos = false;
    if (hasErrors) {
      if (CollectionUtils.isNotEmpty(alcs)) {
        afiErrorBo.guardarBloqueNacionalConErrores(alcs, errorsByBean, bloqueMismaOrden, null);
        borrarAfiErrAntiguos = true;
      }
      else if (CollectionUtils.isNotEmpty(alos)) {
        afiErrorBo.guardarBloqueExtranjeroConErrores(alos, errorsByBean, bloqueMismaOrden, null);
        borrarAfiErrAntiguos = true;
      }
      LOG.trace(
          "[SIBBACServiceTitulares :: checkSavePreviousBlock] Se ha intentado hacer checkedSave de un titular con errores: {} {}",
          errorsByBean, erroresNumorden);
    }
    else {
      if (CollectionUtils.isEmpty(alos) && CollectionUtils.isEmpty(alcs) && nureford != null) {
        cdbloque = afiBo.inactivatePreviousAfis(nureford);
        cdbloque++;
      }

      try {
        if (CollectionUtils.isNotEmpty(alcs)) {
          List<ControlTitularesDTO> alcsFor;
          for (ControlTitularesDTO alc : alcs) {
            alcsFor = new ArrayList<ControlTitularesDTO>();
            alcsFor.add(alc);
            cdbloque = afiBo.updatePreviousAfisViaAlc(alc);
            cdbloque++;
            afiBo.saveAfi(bloqueMismaOrden, alcs, cdbloque, festivosList, false, usuarioAuditoria);
            borrarAfiErrAntiguos = true;
            fixedAfis.addNAfiSynchronized(bloqueMismaOrden.size());
          }

        }
        else {
          if (CollectionUtils.isEmpty(alos)) {
            alos.add(new ControlTitularesDTO(bloqueMismaOrden.get(0).getTmct0alo().getTmct0bok().getTmct0ord()
                .getCdclsmdo(), bloqueMismaOrden.get(0).getTmct0alo().getId(), bloqueMismaOrden.get(0).getTmct0alo()
                .getFevalor(), bloqueMismaOrden.get(0).getTmct0alo().getReftitular(), bloqueMismaOrden.get(0)
                .getTmct0alo().getCdestadotit(), bloqueMismaOrden.get(0).getTmct0alo().getTmct0bok().getTmct0ord()
                .getCdclente(), bloqueMismaOrden.get(0).getTmct0alo().getTmct0bok().getTmct0ord().getIsscrdiv()));
          }
          List<ControlTitularesDTO> alosFor;
          for (ControlTitularesDTO alo : alos) {
            alosFor = new ArrayList<ControlTitularesDTO>();
            alosFor.add(alo);
            cdbloque = afiBo.updatePreviousAfisViaAlo(alo);
            cdbloque++;
            afiBo.saveAfiExtranjera(bloqueMismaOrden, alos, cdbloque);
            borrarAfiErrAntiguos = true;
          }
          fixedAfis.addNAfiSynchronized(bloqueMismaOrden.size());
        }
      }
      catch (Exception e) {
        LOG.warn("[SIBBACServiceTitulares :: checkSavePreviousBlock] Exception saving bean " + e.getMessage());
      }

    }

    if (borrarAfiErrAntiguos) {
      // Se recorren los titulares de la misma orden para guararlos
      for (PartenonRecordBean bean : bloqueMismaOrden) {
        afiErrorBo.delete(bean.getAfiError());
      }
    }

    LOG.trace("[SIBBACServiceTitulares :: checkSavePreviousBlock] Fin");

  }

  /**
   * Actualiza los titulares que recibe de una lista. Ejemplo de llamada y
   * respuesta: /sibbac-html/Documents/RESTServices/wrappers/titulares/
   * updateTitularesList.txt
   * 
   * @param webRequest
   * @param webResponse
   */
  private void updateTitularesList(WebRequest webRequest, WebResponse webResponse) {
    final ControlTitularesParams ctParams;

    LOG.debug("[updateTitularesList] Init");
    ctParams = new ControlTitularesParams();
    ctParams.cargaParametros(webRequest.getParams());
    webResponse.setResultados(afiBo.updateTitularesList(ctParams));
    LOG.debug("[updateTitularesList] Fin");
  } // updateTitularesList

  private void updateTitularesMassive(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[updateTitularesMassive] Init");
    Map<String, String> filters = webRequest.getFilters();
    List<Tmct0AfiError> afiErrors = new ArrayList<Tmct0AfiError>();
    List<Tmct0afi> afis = new ArrayList<Tmct0afi>();
    List<Date> listaFestivos = cfgBo.findHolidaysBolsa();
    String mensaje = "";
    boolean esBloqueante = false;

    String usuarioAuditoria = String.class.cast(Objects.requireNonNull(session.getAttribute("UserSession")));

    Map<String, Tmct0paises> isosPaises = paisesBo.findMapAllActivosByIsonnum();

    Map<String, Tmct0paises> isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();

    Map<String, List<String>> nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();

    boolean saltaFiltro = false;
    try {
      saltaFiltro = Boolean.parseBoolean(filters.get(FILTER_SALTA_FILTRO));
    }
    catch (Exception e) {
      LOG.debug("[updateTitularesMassive] Si no trae saltaFiltro se pone falso por defecto");
    }
    final Map<String, Object> resultados = new HashMap<String, Object>();
    Integer nAfiErrorProcessed = new Integer(0);
    Integer nAfiProcessed = new Integer(0);
    Integer nAfiErrorFixed = new Integer(0);
    Integer nAfiNotProcessed = new Integer(0);

    final Date fechaDesde = convertirStringADate(filters.get(FILTER_FECHA_DESDE));
    final Date fechaHasta = convertirStringADate(filters.get(FILTER_FECHA_HASTA));

    final String nbclientold = processString(filters.get(FILTER_NBCLIENT_OLD));
    final String nbclient1old = processString(filters.get(FILTER_NBCLIENT1_OLD));
    final String nbclient2old = processString(filters.get(FILTER_NBCLIENT2_OLD));
    final String nudnicifold = processString(filters.get(FILTER_NUDNICIF_OLD));
    final Character tpnactitold = processCharacter(filters.get(FILTER_TIPNAC_OLD));
    final Character tipodocumentoold = processCharacter(filters.get(FILTER_TIPDOC_OLD));
    final String paisresold = processString(filters.get(FILTER_PAISRES_OLD));
    final String nbdomiciold = processString(filters.get(FILTER_NB_DOMICILIO_OLD));
    final String tpdomiciold = processString(filters.get(FILTER_TP_DOMICILIO_OLD));
    final String nudomiciold = processString(filters.get(FILTER_NU_DOMICILIO_OLD));
    final String provinciaold = processString(filters.get(FILTER_PROVINCIA_OLD));
    final String codpostalold = processString(filters.get(FILTER_COD_POSTAL_OLD));
    final String nacionalidadold = processString(filters.get(FILTER_NACIONALIDAD_OLD));
    final String ciudadold = processString(filters.get(FILTER_CIUDAD_OLD));
    final Character tptiprepold = processCharacter(filters.get(FILTER_TPTIPREP_OLD));
    final Character tpsociedold = processCharacter(filters.get(FILTER_TPSOCIED_OLD));
    final BigDecimal participold = processBigDecimal(filters.get(FILTER_PARTICIP_OLD));
    final String ccvold = processString(filters.get(FILTER_CCV_OLD));
    final String nurefordold = processString(filters.get(FILTER_NUREFORD_OLD));

    final TitularesDTO titular = new TitularesDTO();
    titular.setNbclient(processString(filters.get(FILTER_NBCLIENT)));
    titular.setNbclient1(processString(filters.get(FILTER_NBCLIENT1)));
    titular.setNbclient2(processString(filters.get(FILTER_NBCLIENT2)));
    titular.setNudnicif(processString(filters.get(FILTER_NUDNICIF)));
    titular.setTipnactit(processCharacter(filters.get(FILTER_TIPNAC)));
    titular.setNacionalidad(processString(filters.get(FILTER_NACIONALIDAD)));
    titular.setTipdoc(processCharacter(filters.get(FILTER_TIPDOC)));
    titular.setPaisres(processString(filters.get(FILTER_PAISRES)));
    titular.setProvincia(processString(filters.get(FILTER_PROVINCIA)));
    titular.setPoblacion(processString(filters.get(FILTER_CIUDAD)));
    titular.setCodpostal(processString(filters.get(FILTER_COD_POSTAL)));
    titular.setTptiprep(processCharacter(filters.get(FILTER_TPTIPREP)));
    titular.setTipopers(processCharacter(filters.get(FILTER_TPSOCIED)));
    titular.setParticip(processBigDecimal(filters.get(FILTER_PARTICIP)));
    titular.setCcv(processString(filters.get(FILTER_CCV)));
    // Domicilio
    titular.setTpdomici(processString(filters.get(FILTER_TP_DOMICILIO)));
    titular.setNbdomici(processString(filters.get(FILTER_NB_DOMICILIO)));
    titular.setNudomici(processString(filters.get(FILTER_NU_DOMICILIO)));
    //
    titular.setRefCliente(processString(filters.get(FILTER_REF_CLIENTE)));

    Set<Tmct0alc> listaAlcsModificados = new HashSet<Tmct0alc>();

    ModifiedFields infoForLog = new ModifiedFields();
    fillInfoForLog(infoForLog, titular);

    try {
      LOG.debug("[updateTitularesMassive] Comenzando consulta a " + DBConstants.WRAPPERS.AFI_ERROR);
      afiErrors = afiErrorBo.findForService(fechaDesde, fechaHasta, null, null, nudnicifold, null, nbclientold,
          nbclient1old, nbclient2old, null, tpnactitold, null, null, tipodocumentoold, paisresold, tpdomiciold,
          nbdomiciold, nudomiciold, provinciaold, codpostalold, ciudadold, tptiprepold, tpsociedold, participold,
          ccvold, nurefordold, nacionalidadold);
      LOG.debug("[updateTitularesMassive] Terminando consulta a " + DBConstants.WRAPPERS.AFI_ERROR + ". Obtenidos "
          + afiErrors.size() + " registros");
    }
    catch (Exception e) {
      LOG.debug("[updateTitularesMassive] Excepción consulta " + DBConstants.WRAPPERS.AFI_ERROR + " : "
          + e.getMessage());
    }

    try {
      for (Tmct0AfiError tmct0afierror : afiErrors) {
        afiBo.modifyAfiError(tmct0afierror, titular, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
        nAfiErrorProcessed++;
        if (afiErrorBo.containsAlcs(tmct0afierror)) {
          listaAlcsModificados.addAll(tmct0afierror.getTmct0alo().getTmct0alcs());
        }
      }
    }
    catch (Exception e) {
      LOG.debug("[updateTitularesMassive] Excepción modificacion " + DBConstants.WRAPPERS.AFI_ERROR + " : "
          + e.getMessage());

    }
    // PARTE AFIS
    List<Tmct0afi> afisAModificar = new ArrayList<Tmct0afi>();
    try {
      LOG.debug("[updateTitularesMassive] Comenzando consulta a TMCT0AFI");
      afis = afiBo.findForService(fechaDesde, fechaHasta, null, null, nudnicifold, null, nbclientold, nbclient1old,
          nbclient2old, null, tpnactitold, null, null, tipodocumentoold, paisresold, tpdomiciold, nbdomiciold,
          nudomiciold, provinciaold, codpostalold, ciudadold, tptiprepold, tpsociedold, participold, ccvold,
          nacionalidadold, nurefordold);
    }
    catch (Exception e) {
      LOG.debug("[updateTitularesMassive] Excepción consulta TMCT0AFI");
    }
    LOG.debug("[updateTitularesMassive] Terminando consulta a TMCT0AFI. Obtenidos " + afis.size() + " registros");
    if (saltaFiltro) {
      afisAModificar = afis;
    }
    else {
      for (Tmct0afi afi : afis) {
        String mensajeInterno = "";

        try {
          List<Tmct0alc> alcs = alcBo.findAlcsByIdViaSpecifications(afi.getId().getNuorden(),
              afi.getId().getNbooking(), afi.getId().getNucnfliq(), afi.getId().getNucnfclt());
          if (alcs != null && alcs.size() > 0) {
            mensajeInterno = afiBo.comprobacionesALC(alcs.get(0), listaFestivos);
            if (StringUtils.isNotEmpty(mensajeInterno)) {
              nAfiNotProcessed++;
              if (Tmct0afiBo.MSJ_BLOQUEANTE_1.equals(mensajeInterno)
                  || Tmct0afiBo.MSJ_BLOQUEANTE_2.equals(mensajeInterno)) {
                esBloqueante = true;
                mensaje = mensajeInterno;
              }
              if (StringUtils.isEmpty(mensaje)) {
                mensaje = mensajeInterno;
              }
            }
            else {
              afisAModificar.add(afi);
            }
          }
          else {
            afisAModificar.add(afi);
          }
        }
        catch (Exception e) {
          LOG.debug("[updateTitularesMassive] Excepción comprobando el alc del TMCT0AFI con nuorden:"
              + afi.getId().getNuorden() + ", nbooking:" + afi.getId().getNbooking() + ", nucnfclt:"
              + afi.getId().getNucnfclt() + ", nucnfliq:" + afi.getId().getNucnfliq());
        }
      }
    }
    try {
      for (Tmct0afi tmct0afi : afisAModificar) {
        // Comentado provisionalmente para no meter errores en BBDD
        String oldCcv = tmct0afi.getCcv();
        boolean fueModificado = modifyAfi(tmct0afi, titular, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
        if (fueModificado) {
          nAfiProcessed++;
          alcBo.updateAlcsViaCcv(tmct0afi, listaAlcsModificados, oldCcv, listaFestivos, usuarioAuditoria);
        }
        else {
          nAfiNotProcessed++;
          PartenonRecordBean bean = new PartenonRecordBean(tmct0afi);

          Tmct0AfiError afiError = bean.toTmct0afierror(afiBo.getNewTmct0afiSequence() + "");
          afiBo.modifyAfiError(afiError, titular, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
          nAfiErrorProcessed++;

          if (afiBo.validacionesBloque(tmct0afi.getTmct0alo().getId(), tmct0afi.getId().getNucnfliq(), isosPaises,
              isosPaisesHacienda, nombresCiudadPorDistrito)) {
            nAfiErrorFixed++;
          }
        }
        if (afiBo.containsAlcs(tmct0afi) && fueModificado) {
          listaAlcsModificados.addAll(tmct0afi.getTmct0alo().getTmct0alcs());
        }
      }
    }
    catch (Exception e) {
      LOG.debug("[updateTitularesMassive] Excepción modificacion TMCT0AFI: " + e.getMessage());

    }
    afiBo.sendInfoToTmct0Log(listaAlcsModificados, infoForLog);
    resultados.put("nAfiErrorProcessed", nAfiErrorProcessed);
    resultados.put("nAfiProcessed", nAfiProcessed);
    resultados.put("nAfiNotProcessed", nAfiNotProcessed);
    resultados.put("nAfiErrorFixed", nAfiErrorFixed);
    resultados.put(RESULTADOS_ES_BLOQUEANTE, esBloqueante);
    resultados.put(RESULTADOS_MENSAJE, mensaje);
    webResponse.setResultados(resultados);
    LOG.debug("[updateTitularesMassive] Fin");
  }

  /**
   * Este metodo se ocupa de buscar cuantos campos se han modificado y cual es
   * el primero de ellos. Se usa para mostrar esos datos en TMCT0LOG en el caso
   * de la actualizacion masiva.
   * 
   * @param infoForLog
   * @param titular
   */
  private void fillInfoForLog(ModifiedFields infoForLog, TitularesDTO titular) {

    if (titular.getNudnicif() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NUDNICIF;
      }
    }
    if (titular.getNbclient() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NBCLIENT;
      }
    }
    if (titular.getNbclient1() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NBCLIENT1;
      }
    }
    if (titular.getNbclient2() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NBCLIENT2;
      }
    }
    if (titular.getNacionalidad() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NACIONALIDAD;
      }
    }
    if (titular.getTipnactit() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_TIPNAC;
      }
    }
    if (titular.getTipdoc() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_TIPDOC;
      }
    }
    if (titular.getPaisres() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_PAISRES;
      }
    }
    if (titular.getTpdomici() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_TP_DOMICILIO;
      }
    }
    if (titular.getNbdomici() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NB_DOMICILIO;

      }
    }
    if (titular.getNudomici() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_NU_DOMICILIO;
      }
    }
    if (titular.getProvincia() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_PROVINCIA;
      }
    }
    if (titular.getCodpostal() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_COD_POSTAL;
      }
    }
    if (titular.getPoblacion() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_CIUDAD;
      }
    }
    if (titular.getTptiprep() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_TPTIPREP;
      }
    }
    if (titular.getTipopers() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_TPSOCIED;
      }
    }
    if (titular.getParticip() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_PARTICIP;
      }
    }
    if (titular.getCcv() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_CCV;
      }
    }
    if (titular.getRefCliente() != null) {
      infoForLog.modifiedFields++;
      if (infoForLog.modifiedFields == 1) {
        infoForLog.firstName = FILTER_REF_CLIENTE;
      }
    }

  }

  /**
   * Recibe un tmct0afi y los distintos campos que pueden ser modificado. Estos
   * campos pueden venir a null desde los filtros o con un valor. Si tienen
   * valor se setean en el tmct0afi y se guardan en caso de que no tengan
   * errores.
   * 
   * @param tmct0afi
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param nacionalidad
   * @param tpnactit
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param ciudad
   * @param tptiprep
   * @param tpsocied
   * @param particip
   * @param ccv
   * @param nureford
   */
  @Transactional
  private boolean modifyAfi(Tmct0afi tmct0afi, TitularesDTO titular, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito) {

    Map<PartenonRecordBean, List<String>> errorsByBean = new HashMap<PartenonRecordBean, List<String>>();
    // Obtenemos el bean
    PartenonRecordBean bean = new PartenonRecordBean(tmct0afi);
    List<PartenonRecordBean> bloqueMismaOrden = new ArrayList<PartenonRecordBean>();
    bloqueMismaOrden.add(bean);
    afiBo.modifyBean(bean, titular);

    // Validamos el bean para tener los errores
    List<String> errCodes = externalValidator.validate(bean);
    List<String> errExternals = externalValidator.validate(bean, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito);
    for (String err : errExternals) {
      if (!errCodes.contains(err)) {
        errCodes.add(err);
      }
    }
    errExternals = externalValidator.validatePaisesProvincias(bean, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito);
    for (String err : errExternals) {
      if (!errCodes.contains(err)) {
        errCodes.add(err);
      }
    }

    List<ControlTitularesDTO> alcs = new ArrayList<ControlTitularesDTO>();
    List<ControlTitularesDTO> alos = new ArrayList<ControlTitularesDTO>();
    // Se comprueban los errores del ord
    String erroresNumorden = afiBo.comprobarErroresProcedencia(bloqueMismaOrden, alcs, alos);
    List<Tmct0afi> others = afiBo.findActivosByAlcData(tmct0afi.getId().getNuorden(), tmct0afi.getId().getNbooking(),
        tmct0afi.getId().getNucnfclt(), tmct0afi.getId().getNucnfliq());
    for (Tmct0afi tmct0afi2 : others) {
      PartenonRecordBean bean2 = new PartenonRecordBean(tmct0afi2);
      if (!bloqueMismaOrden.contains(bean2)) {
        bloqueMismaOrden.add(bean2);
      }
      if (errorsByBean.get(bean2) == null) {
        errorsByBean.put(bean2, new ArrayList<String>());
      }
    }
    boolean hasErrorBloque = externalValidator.validateGroup(bloqueMismaOrden, errorsByBean);

    boolean fueActualizado = false;

    if (errCodes.size() > 0 || erroresNumorden != null || hasErrorBloque) {
      // No se puede guardar un afi con errores
      LOG.debug("[modifyAfi] No se guarda el afi porque tiene errores. " + "nuorden" + ": "
          + tmct0afi.getId().getNuorden() + ", " + "nbooking" + ": " + tmct0afi.getId().getNbooking() + ", "
          + "nucnfclt" + ": " + tmct0afi.getId().getNucnfclt() + ", " + "nucnfliq" + ": "
          + tmct0afi.getId().getNucnfliq() + ", " + "nusecuen" + ":" + tmct0afi.getId().getNusecuen() + " ERRORES: "
          + errCodes.toString());

    }
    else {
      // Actualizamos el afi
      bean.updateTmct0afi(tmct0afi);
      afiBo.save(tmct0afi);
      fueActualizado = true;
    }
    return fueActualizado;
  }

  /**
   * Devuelve el numero de registros para la query de getTitularesList
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getNRegistrosQuery(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[getNRegistrosQuery] Init");
    final Map<String, Integer> resultados = new HashMap<String, Integer>();
    resultados.put(RESULT_NREGISTROS_QUERY, step);
    webResponse.setResultados(resultados);
    LOG.debug("[getNRegistrosQuery] Fin");
  }

  /**
   * @param filtros
   * @return
   */
  private Date convertirStringADate(final String fechaString) {
    Date fecha = null;
    if (StringUtils.isNotEmpty(fechaString)) {
      fecha = FormatDataUtils.convertStringToDate(fechaString);
    }
    return fecha;
  }

  /**
   * @param filtros
   * @return
   */
  private String processString(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    return processedString;
  }

  private Character processCharacter(String filterString) {
    Character processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString.trim().charAt(0);
    }
    return processedString;
  }

  private BigDecimal processBigDecimal(String filterString) {
    BigDecimal processedBigDecimal = null;
    if (StringUtils.isNotEmpty(filterString)) {
      if (filterString.trim().equals("<null>")) {
        processedBigDecimal = new BigDecimal("-1");
      }
      else {
        processedBigDecimal = new BigDecimal(filterString);
      }
    }
    return processedBigDecimal;
  }

  // Chequeo parametros
  private boolean updateTitularesMassiveCheckFilters(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[updateTitularesMassiveCheckFilters] Init");

    Map<String, String> filtros = webRequest.getFilters();
    String errorMsg = new String();
    boolean filtersRight = true;

    final Character tpnactitold = processCharacter(filtros.get(FILTER_TIPNAC_OLD));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TIPNAC_OLD))) {
      if (tpnactitold == null) {
        errorMsg = errorMsg + "The '" + FILTER_TIPNAC_OLD + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (!tpnactitold.equals('-') && tpnactitBo.findByCodigo(tpnactitold.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TIPNAC_OLD + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tpnactit = processCharacter(filtros.get(FILTER_TIPNAC));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TIPNAC))) {
      if (tpnactit == null) {
        errorMsg = errorMsg + "The '" + FILTER_TIPNAC + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (tpnactitBo.findByCodigo(tpnactit.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TIPNAC + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tpidentiold = processCharacter(filtros.get(FILTER_TIPDOC_OLD));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TIPDOC_OLD))) {
      if (tpidentiold == null) {
        errorMsg = errorMsg + "The '" + FILTER_TIPDOC_OLD + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (!tpidentiold.equals('-') && tpidentiBo.findByCodigo(tpidentiold.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TIPDOC_OLD + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tpidenti = processCharacter(filtros.get(FILTER_TIPDOC));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TIPDOC))) {
      if (tpidenti == null) {
        errorMsg = errorMsg + "The '" + FILTER_TIPDOC + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (tpidentiBo.findByCodigo(tpidenti.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TIPDOC + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tpsociedold = processCharacter(filtros.get(FILTER_TPSOCIED_OLD));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TPSOCIED_OLD))) {
      if (tpsociedold == null) {
        errorMsg = errorMsg + "The '" + FILTER_TPSOCIED_OLD + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (!tpsociedold.equals('-') && tpsociedBo.findByCodigo(tpsociedold.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TPSOCIED_OLD + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tpsocied = processCharacter(filtros.get(FILTER_TPSOCIED));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TPSOCIED))) {
      if (tpsocied == null) {
        errorMsg = errorMsg + "The '" + FILTER_TPSOCIED + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (tpsociedBo.findByCodigo(tpsocied.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TPSOCIED + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tptiprepold = processCharacter(filtros.get(FILTER_TPTIPREP_OLD));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TPTIPREP_OLD))) {
      if (tptiprepold == null) {
        errorMsg = errorMsg + "The '" + FILTER_TPTIPREP_OLD + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (!tptiprepold.equals('-') && tptiprepBo.findByCodigo(tptiprepold.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TPTIPREP_OLD + "' is not valid. ";
          filtersRight = false;
        }
      }
    }

    final Character tptiprep = processCharacter(filtros.get(FILTER_TPSOCIED));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_TPSOCIED))) {
      if (tptiprep == null) {
        errorMsg = errorMsg + "The '" + FILTER_TPSOCIED + "' field must be one character. ";
        filtersRight = false;
      }
      else {
        if (tptiprepBo.findByCodigo(tptiprep.toString()) == null) {
          errorMsg = errorMsg + "The '" + FILTER_TPSOCIED + "' is not valid. ";
          filtersRight = false;
        }
      }
    }
    final Date fechaDesde = convertirStringADate(filtros.get(FILTER_FECHA_DESDE));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_FECHA_DESDE))) {
      if (fechaDesde == null) {
        errorMsg = errorMsg + "The '" + FILTER_FECHA_DESDE + "' field must have format: YYYYMMDD. ";
        filtersRight = false;
      }
    }

    final Date fechaHasta = convertirStringADate(filtros.get(FILTER_FECHA_HASTA));
    if (StringUtils.isNotEmpty(filtros.get(FILTER_FECHA_HASTA))) {
      if (fechaHasta == null) {
        errorMsg = errorMsg + "The '" + FILTER_FECHA_HASTA + "' field must have format: YYYYMMDD. ";
        filtersRight = false;
      }
    }

    if ((fechaDesde != null && fechaHasta == null) || (fechaDesde == null && fechaHasta != null)) {
      errorMsg = errorMsg + "Both " + FILTER_FECHA_DESDE + " and " + FILTER_FECHA_HASTA + " must be defined . ";
      filtersRight = false;
    }

    if (fechaDesde != null && fechaHasta != null) {
      long diff = fechaHasta.getTime() - fechaDesde.getTime();
      long daysDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
      if (daysDiff < 0) {
        errorMsg = errorMsg + "The '" + FILTER_FECHA_DESDE + "' must be smaller than " + FILTER_FECHA_HASTA;
        filtersRight = false;
      }
      else if (daysDiff > 366) {
        errorMsg = errorMsg + " Difference between dates has to be 1 year or smaller. ";
        filtersRight = false;
      }
    }

    String nbclient = processString(filtros.get(FILTER_NBCLIENT));
    if (StringUtils.isNotEmpty(nbclient)) {
      int maxLength = 30;
      if (nbclient.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NBCLIENT + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbclientold = processString(filtros.get(FILTER_NBCLIENT_OLD));
    if (StringUtils.isNotEmpty(nbclientold)) {
      int maxLength = 30;
      if (nbclientold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NBCLIENT_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbclient1 = processString(filtros.get(FILTER_NBCLIENT1));
    if (StringUtils.isNotEmpty(nbclient1)) {
      int maxLength = 50;
      if (nbclient1.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NBCLIENT1 + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbclient1old = processString(filtros.get(FILTER_NBCLIENT1_OLD));

    if (StringUtils.isNotEmpty(nbclient1old)) {
      int maxLength = 50;
      if (nbclient1old.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NBCLIENT1_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbclient2 = processString(filtros.get(FILTER_NBCLIENT2));
    if (StringUtils.isNotEmpty(nbclient2)) {
      int maxLength = 30;
      if (nbclient2.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NBCLIENT2 + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbclient2old = processString(filtros.get(FILTER_NBCLIENT2_OLD));

    if (StringUtils.isNotEmpty(nbclient2old)) {
      int maxLength = 30;
      if (nbclient2old.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NBCLIENT2_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nudnicif = processString(filtros.get(FILTER_NUDNICIF));

    if (StringUtils.isNotEmpty(nudnicif)) {
      int maxLength = 11;
      if (nudnicif.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NUDNICIF + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nudnicifold = processString(filtros.get(FILTER_NUDNICIF_OLD));

    if (StringUtils.isNotEmpty(nudnicifold)) {
      int maxLength = 11;
      if (nudnicifold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NUDNICIF_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String paisres = processString(filtros.get(FILTER_PAISRES));

    if (StringUtils.isNotEmpty(paisres)) {
      int maxLength = 3;
      if (paisres.trim().length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PAISRES + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nacionalidad = processString(filtros.get(FILTER_NACIONALIDAD));
    if (StringUtils.isNotEmpty(nacionalidad)) {
      int maxLength = 3;
      if (nacionalidad.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NACIONALIDAD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nacionalidadold = processString(filtros.get(FILTER_NACIONALIDAD_OLD));
    if (StringUtils.isNotEmpty(nacionalidadold)) {
      int maxLength = 3;
      if (nacionalidadold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NACIONALIDAD_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String paisresold = processString(filtros.get(FILTER_PAISRES_OLD));

    if (StringUtils.isNotEmpty(paisresold) && !paisresold.equals("<null>")) {
      int maxLength = 3;
      if (paisresold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PAISRES_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String provincia = processString(filtros.get(FILTER_PROVINCIA));

    if (StringUtils.isNotEmpty(provincia)) {
      int maxLength = 25;
      if (provincia.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PROVINCIA + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String provinciaold = processString(filtros.get(FILTER_PROVINCIA_OLD));

    if (StringUtils.isNotEmpty(provinciaold)) {
      int maxLength = 25;
      if (provinciaold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_PROVINCIA_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codpostal = processString(filtros.get(FILTER_COD_POSTAL));

    if (StringUtils.isNotEmpty(codpostal)) {
      int maxLength = 5;
      if (codpostal.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_COD_POSTAL + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String codpostalold = processString(filtros.get(FILTER_COD_POSTAL_OLD));

    if (StringUtils.isNotEmpty(codpostalold) && !codpostalold.equals("<null>")) {
      int maxLength = 5;
      if (codpostalold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_COD_POSTAL_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tpdomicilio = processString(filtros.get(FILTER_TP_DOMICILIO));

    if (StringUtils.isNotEmpty(tpdomicilio)) {
      int maxLength = 2;
      if (tpdomicilio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TP_DOMICILIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String tpdomicilioold = processString(filtros.get(FILTER_TP_DOMICILIO_OLD));

    if (StringUtils.isNotEmpty(tpdomicilioold) && !tpdomicilioold.equals("<null>")) {
      int maxLength = 2;
      if (tpdomicilioold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_TP_DOMICILIO_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbdomicilio = processString(filtros.get(FILTER_NB_DOMICILIO));

    if (StringUtils.isNotEmpty(nbdomicilio)) {
      int maxLength = 40;
      if (nbdomicilio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NB_DOMICILIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nbdomicilioold = processString(filtros.get(FILTER_NB_DOMICILIO_OLD));

    if (StringUtils.isNotEmpty(nbdomicilioold)) {
      int maxLength = 40;
      if (nbdomicilioold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NB_DOMICILIO_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nudomicilio = processString(filtros.get(FILTER_NU_DOMICILIO));

    if (StringUtils.isNotEmpty(nudomicilio)) {
      int maxLength = 4;
      if (nudomicilio.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NU_DOMICILIO + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nudomicilioold = processString(filtros.get(FILTER_NU_DOMICILIO_OLD));

    if (StringUtils.isNotEmpty(nudomicilioold) && !nudomicilioold.equals("<null>")) {
      int maxLength = 4;
      if (nudomicilioold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NU_DOMICILIO_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String ciudad = processString(filtros.get(FILTER_CIUDAD));

    if (StringUtils.isNotEmpty(ciudad)) {
      int maxLength = 40;
      if (ciudad.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CIUDAD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String ciudadold = processString(filtros.get(FILTER_CIUDAD_OLD));

    if (StringUtils.isNotEmpty(ciudadold)) {
      int maxLength = 40;
      if (ciudadold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_CIUDAD_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nureford = processString(filtros.get(FILTER_REF_CLIENTE));

    if (StringUtils.isNotEmpty(nureford)) {
      int maxLength = 32;
      if (nureford.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_REF_CLIENTE + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    String nurefordold = processString(filtros.get(FILTER_NUREFORD_OLD));

    if (StringUtils.isNotEmpty(nurefordold)) {
      int maxLength = 32;
      if (nurefordold.length() > maxLength) {
        errorMsg = errorMsg + "The '" + FILTER_NUREFORD_OLD + "' field is longer than " + maxLength + ". ";
        filtersRight = false;
      }
    }

    if (filtersRight == false) {
      webResponse.setError(errorMsg);
      LOG.debug("[updateTitularesMassiveCheckFilters] Error: " + errorMsg);
    }
    LOG.debug("[updateTitularesMassiveCheckFilters] Fin");

    return filtersRight;

  }

} // SIBBACServiceTitulares
