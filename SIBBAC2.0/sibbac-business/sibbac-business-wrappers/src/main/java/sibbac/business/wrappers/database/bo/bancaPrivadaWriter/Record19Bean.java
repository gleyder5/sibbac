package sibbac.business.wrappers.database.bo.bancaPrivadaWriter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ord;

public class Record19Bean extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 7637357372450972183L;
  // Tam 2
  private Integer tipoRegistroR19;
  // Tam 16
  private String numrefer;
  // Tam 8
  private String fhlote;
  // Tam 6
  private String hlote;
  // Tam 8
  private String feccontr;
  // Tam 8
  private String fliqui;
  // Tam 15
  private String efecneto;
  // Tam 11
  private String numtitej;
  // Tam 15
  private String nomijec;
  // Tam 12
  private String cbomedio;
  // Tam 4
  private String broker;
  // Tam 3
  private String divisa;
  // Tam 4
  private String cencont;
  // Tam 11
  private String cdorecc;
  // Tam 11 (9,2) BIGDECIMAL
  private String comibroker;
  // Tam 11 (9,2) BIGDECIMAL
  private String comcontr;
  // Tam 11 (9,2) BIGDECIMAL
  private String corretajes;
  // Tam 11 (9,2) BIGDECIMAL
  private String cancont;
  // Tam 1
  private String abcancom;
  // Tam 11 (9,2) BIGDECIMAL
  private String cancomp;
  // Tam 11 (9,2) BIGDECIMAL
  private String canliq;
  // Tam 20
  private String reftit;
  // Tam 6
  private String coderr;
  // Tam 35
  private String descerr;
  // Tam 11
  private String filler;

  public Record19Bean(Date fechaLote,
                      Tmct0ord ord,
                      Tmct0alo alo,
                      Tmct0eje eje,
                      BigDecimal imtotbru,
                      BigDecimal nutitcli,
                      BigDecimal imcomsim,
                      BigDecimal imcanoncontreu,
                      BigDecimal imcanoncompensacion,
                      BigDecimal imcanonliquidacion,
                      String reftit,
                      BigDecimal cambioMedio) {
    
    BigDecimal zero = new BigDecimal("0.0");
    this.tipoRegistroR19 = 19;
    setTipoRegistro(this.tipoRegistroR19 + "");
    this.numrefer = StringUtils.trim(ord.getNuorden());
    DateFormat df = new SimpleDateFormat("yyyyMMdd");
    this.fhlote = df.format(fechaLote);
    DateFormat hf = new SimpleDateFormat("HHmmss");
    this.hlote = hf.format(fechaLote);
    this.feccontr = df.format(alo.getFeoperac());
    this.fliqui = df.format(alo.getFevalor());
    this.efecneto = StringHelper.formatBigDecimalWithSign(imtotbru.setScale(2, BigDecimal.ROUND_HALF_UP), 13, 2);
    this.numtitej = StringHelper.formatBigDecimal(nutitcli, 11, 0);
    this.nomijec = "000000000000000";
    this.cbomedio = StringHelper.formatBigDecimal(cambioMedio.setScale(5, BigDecimal.ROUND_HALF_UP), 7, 5);
    this.broker = "0000";
    this.divisa = "EUR";
    this.cencont = eje.getExecutionTradingVenue();
    this.cdorecc = "IBRCESMMCDE";
    this.comibroker = "+00000000000";
    this.comcontr = "+00000000000";
    this.corretajes = StringHelper.formatBigDecimalWithSign(imcomsim.setScale(2, BigDecimal.ROUND_HALF_UP), 9, 2);

    if (alo.getCanoncontrpagoclte() == 1) {
      this.cancont = StringHelper.formatBigDecimalWithSign(imcanoncontreu.setScale(2, BigDecimal.ROUND_HALF_UP), 9, 2);
    } else {
      this.cancont = StringHelper.formatBigDecimalWithSign(zero.setScale(2, BigDecimal.ROUND_HALF_UP), 9, 2);
    } 
    
    if (alo.getCanoncomppagoclte() == 1) {
      this.abcancom = "S";
    } else if (alo.getCanoncomppagoclte() == 0) {
      this.abcancom = "N";
    } else {
      this.abcancom = " ";
    }
    
    this.cancomp = StringHelper.formatBigDecimalWithSign(imcanoncompensacion.setScale(2, BigDecimal.ROUND_HALF_UP), 9, 2);
    this.canliq = StringHelper.formatBigDecimalWithSign(imcanonliquidacion.setScale(2, BigDecimal.ROUND_HALF_UP), 9, 2);
    this.reftit = reftit;
    this.coderr = "      ";
    this.descerr = "                                   ";
    this.filler = "                ";
  }

  public Integer getTipoRegistroR19() {
    return tipoRegistroR19;
  }

  public void setTipoRegistroR19(Integer tipoRegistroR19) {
    this.tipoRegistroR19 = tipoRegistroR19;
  }

  public String getNumrefer() {
    return numrefer;
  }

  public void setNumrefer(String numrefer) {
    this.numrefer = numrefer;
  }

  public String getFhlote() {
    return fhlote;
  }

  public void setFhlote(String fhlote) {
    this.fhlote = fhlote;
  }

  public String getHlote() {
    return hlote;
  }

  public void setHlote(String hlote) {
    this.hlote = hlote;
  }

  public String getFeccontr() {
    return feccontr;
  }

  public void setFeccontr(String feccontr) {
    this.feccontr = feccontr;
  }

  public String getFliqui() {
    return fliqui;
  }

  public void setFliqui(String fliqui) {
    this.fliqui = fliqui;
  }

  public String getEfecneto() {
    return efecneto;
  }

  public void setEfecneto(String efecneto) {
    this.efecneto = efecneto;
  }

  public String getNumtitej() {
    return numtitej;
  }

  public void setNumtitej(String numtitej) {
    this.numtitej = numtitej;
  }

  public String getNomijec() {
    return nomijec;
  }

  public void setNomijec(String nomijec) {
    this.nomijec = nomijec;
  }

  public String getCbomedio() {
    return cbomedio;
  }

  public void setCbomedio(String cbomedio) {
    this.cbomedio = cbomedio;
  }

  public String getBroker() {
    return broker;
  }

  public void setBroker(String broker) {
    this.broker = broker;
  }

  public String getDivisa() {
    return divisa;
  }

  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  public String getCencont() {
    return cencont;
  }

  public void setCencont(String cencont) {
    this.cencont = cencont;
  }

  public String getCdorecc() {
    return cdorecc;
  }

  public void setCdorecc(String cdorecc) {
    this.cdorecc = cdorecc;
  }

  public String getComibroker() {
    return comibroker;
  }

  public void setComibroker(String comibroker) {
    this.comibroker = comibroker;
  }

  public String getComcontr() {
    return comcontr;
  }

  public void setComcontr(String comcontr) {
    this.comcontr = comcontr;
  }

  public String getCorretajes() {
    return corretajes;
  }

  public void setCorretajes(String corretajes) {
    this.corretajes = corretajes;
  }

  public String getCancont() {
    return cancont;
  }

  public void setCancont(String cancont) {
    this.cancont = cancont;
  }

  public String getAbcancom() {
    return abcancom;
  }

  public void setAbcancom(String abcancom) {
    this.abcancom = abcancom;
  }

  public String getCancomp() {
    return cancomp;
  }

  public void setCancomp(String cancomp) {
    this.cancomp = cancomp;
  }

  public String getCanliq() {
    return canliq;
  }

  public void setCanliq(String canliq) {
    this.canliq = canliq;
  }

  public String getReftit() {
    return reftit;
  }

  public void setReftit(String reftit) {
    this.reftit = reftit;
  }

  public String getCoderr() {
    return coderr;
  }

  public void setCoderr(String coderr) {
    this.coderr = coderr;
  }

  public String getDescerr() {
    return descerr;
  }

  public void setDescerr(String descerr) {
    this.descerr = descerr;
  }

  public String getFiller() {
    return filler;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

}
