package sibbac.business.wrappers.database.dto;

import sibbac.business.wrappers.database.model.CodigoError;

public class CodigoErrorDTO {

	private String codigo;
	private String descripcion;

	public CodigoErrorDTO(final CodigoError error) {
		this.codigo = error.getCodigo();
		this.descripcion = error.getDescripcion();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
