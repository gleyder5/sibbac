package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.fase0.database.dto.LabelValueObject;

@Repository
public interface Tmct0CamaraCompensacionDao extends JpaRepository<Tmct0CamaraCompensacion, Long> {

  Tmct0CamaraCompensacion findByCdCodigo(String cdCodigo);

  @Query("SELECT camara FROM Tmct0CamaraCompensacion camara WHERE camara.nbDescripcion=:nbDescripcion ")
  public Tmct0CamaraCompensacion findByNbDescripcion(@Param("nbDescripcion") String nbDescripcion);
  
  @Query(value = "SELECT camcom.CD_CODIGO " +
          "FROM TMCT0_CAMARA_COMPENSACION camcom " + 
          "INNER JOIN TMCT0_CUENTAS_DE_COMPENSACION cuen ON " +
          "camcom.ID_CAMARA_COMPENSACION = cuen.ID_CUENTA_COMPENSACION " +
          "WHERE cuen.CD_CODIGO = :cdcodigo", nativeQuery = true)
  public String findCodigoCamaraCompensacion(@Param("cdcodigo") String cdcodigo);
  
  @Query("SELECT new sibbac.business.fase0.database.dto.LabelValueObject(c.nbDescripcion, c.cdCodigo) "
      + "FROM Tmct0CamaraCompensacion c")
  public List<LabelValueObject> labelValues();

}
