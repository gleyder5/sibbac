package sibbac.business.wrappers.database.dao.specifications;


import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaSugerida;


public class FacturaSpecificacions {

	protected static final Logger	LOG	= LoggerFactory.getLogger( FacturaSpecificacions.class );

	public static Specification< Factura > numeroFactura( final Long numeroFactura ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate numeroFacturaPredicate = null;
				LOG.trace( "[FacturaSpecificacions::numeroFactura] [numeroFactura=={}]", numeroFactura );
				if ( numeroFactura != null ) {
					numeroFacturaPredicate = builder.equal( root.< Long > get( "numeroFactura" ), numeroFactura );
				}
				return numeroFacturaPredicate;
			}
		};
	}

	public static Specification< Factura > alias( final Long aliasId ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate aliasPredicate = null;
				LOG.trace( "[FacturaSpecificacions::alias] [aliasId=={}]", aliasId );
				if ( aliasId != null ) {
					aliasPredicate = builder.equal( root.< FacturaSugerida > get( "facturaSugerida" ).< Alias > get( "alias" )
							.< Long > get( "id" ), aliasId );
				}
				return aliasPredicate;
			}
		};
	}

	public static Specification< Factura > fechaDesde( final Date fecha ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[FacturaSpecificacions::fechaFacturaPosterior] [fecha=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fechaFactura" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Factura > fechaHasta( final Date fecha ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[FacturaSpecificacions::fechaFacturaAnterior] [fecha=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fechaFactura" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Factura > enviadas( final Boolean enviadas ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate enviadasPredicate = null;
				LOG.trace( "[FacturaSpecificacions::enviados] [enviados=={}]", enviadas );
				if ( enviadas != null ) {
					enviadasPredicate = builder.equal( root.< Boolean > get( "enviado" ), enviadas );
				}
				return enviadasPredicate;
			}
		};
	}

	public static Specification< Factura > estados( final List< Integer > estados ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate estadosPredicate = null;
				LOG.trace( "[FacturaSpecificacions::estados] [estados=={}]", estados );
				if ( CollectionUtils.isNotEmpty( estados ) ) {
					final Path< Tmct0estado > estado = root.< Tmct0estado > get( "estado" );
					estadosPredicate = estado.in( estados );
				}
				return estadosPredicate;
			}
		};
	}

	public static Specification< Factura > listadoFriltrado( final Long aliasId, final Date fechaDesde, final Date fechaHasta,
			final List< Integer > estados, final Boolean enviadas ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Factura > specs = null;
				if ( aliasId != null ) {
					specs = Specifications.where( alias( aliasId ) );
				}
				if ( fechaDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaDesde( fechaDesde ) );
					} else {
						specs = specs.and( fechaDesde( fechaDesde ) );
					}
				}
				if ( fechaHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaHasta( fechaHasta ) );
					} else {
						specs = specs.and( fechaHasta( fechaHasta ) );
					}

				}
				if ( CollectionUtils.isNotEmpty( estados ) ) {
					if ( specs == null ) {
						specs = Specifications.where( estados( estados ) );
					} else {
						specs = specs.and( estados( estados ) );
					}

				}
				if (enviadas != null) {
					if ( specs == null ) {
						specs = Specifications.where( enviadas( enviadas ) );
					} else {
						specs = specs.and( enviadas( enviadas ) );
					}
				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

	public static Specification< Factura > listadoNumeroFactura( final Long numeroFactura ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Factura > specs = null;
				if ( numeroFactura != null ) {
					specs = Specifications.where( numeroFactura( numeroFactura ) );
				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

	public static Specification< Factura > listadoNumeroFacturaEstado( final Long numeroFactura, final List< Integer > estados ) {
		return new Specification< Factura >() {

			public Predicate toPredicate( Root< Factura > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Factura > specs = null;
				if ( numeroFactura != null ) {
					specs = Specifications.where( numeroFactura( numeroFactura ) );
				}
				if ( CollectionUtils.isNotEmpty( estados ) ) {
					if ( specs == null ) {
						specs = Specifications.where( estados( estados ) );
					} else {
						specs = specs.and( estados( estados ) );
					}

				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

}
