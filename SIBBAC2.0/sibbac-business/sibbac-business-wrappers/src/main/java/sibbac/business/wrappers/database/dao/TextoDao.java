package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.model.Texto;


@Component
public interface TextoDao extends JpaRepository< Texto, Long > {
	
	Texto findByTipo( String tipo );
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Texto t SET t.descripcion = :descrip WHERE t.tipo = :tipo")
	public Integer updateDescripByTipo(@Param("descrip") String descrip, @Param("tipo") String tipo);

}
