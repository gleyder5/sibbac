package sibbac.business.wrappers.tasks;

import java.util.List;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.CobrosBo;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_ENVIAR_MAIL_COBROS, interval = 1, intervalUnit = IntervalUnit.HOUR)
public class TaskEnviarMailCobros extends WrapperTaskConcurrencyPrevent {

    @Autowired
    CobrosBo cobroBo;

    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskEnviarMailCobros.class);

    @Override
    @Transactional
    public void executeTask() {
	final String prefix = "[TaskEnviarMailCobros]";
	LOG.debug(prefix + "[Started...]");
	try {
	    List<String> messages = cobroBo.sendMailCobros(this.jobPk);
	    if (messages != null && !messages.isEmpty()) {
		for (String message : messages) {
		    this.blog(message);
		}
	    }
	} catch (Exception ex) {

	    throw ex;
	}
	LOG.debug(prefix + "[Finished...]");
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_COBROS_ENVIO_MAIL;
    }
}
