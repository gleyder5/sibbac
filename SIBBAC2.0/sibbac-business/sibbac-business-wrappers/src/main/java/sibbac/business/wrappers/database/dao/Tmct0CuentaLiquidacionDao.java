package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;

/**
 * The Interface Tmct0CuentaLiquidacionDao.
 */
@Repository
public interface Tmct0CuentaLiquidacionDao extends JpaRepository< Tmct0CuentaLiquidacion, Long > {

	/**
	 * Find by id cta comp.
	 *
	 * @param idCuentaComp the id cuenta comp
	 * @return the tmct0 cuenta liquidacion
	 */
	@Query( "SELECT cl FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cc.idCuentaCompensacion = :idCuentaComp" )
			Tmct0CuentaLiquidacion findByIdCtaComp( @Param( "idCuentaComp" ) Long idCuentaComp );

	/**
	 * Find all clearing propias.
	 *
	 * @return the list
	 */
	@Query( value = "select r from Tmct0CuentaLiquidacion r where r.esCuentaPropia=true" )
	List< Tmct0CuentaLiquidacion > findAllClearingPropias();

	/**
	 * Find all clearing no propias.
	 *
	 * @return the list
	 */
	@Query( "SELECT DISTINCT cc FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cl.esCuentaPropia=false ORDER BY cl.cdCodigo" )
	List< Tmct0CuentasDeCompensacion > findAllClearingNoPropias();

	/**
	 * Obtiene las cuentas de liquidacion que esten relacionadas con cuentas de
	 * compensacion clearing.
	 *
	 * @return the list
	 */
	@Query( "SELECT DISTINCT cl FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cc.cuentaClearing = 'S' ORDER BY cl.cdCodigo" )
	List< Tmct0CuentaLiquidacion > findAllByCuentaDeCompensacionIsClearing();

	@Query( "SELECT DISTINCT cl FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cc.cuentaClearing = 'S' AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo!='CI' AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo!='CP' ORDER BY cl.cdCodigo" )
	List< Tmct0CuentaLiquidacion > findAllByCuentaDeCompensacionIsClearingNoPropiaNoIndividual();

	@Query( "SELECT DISTINCT cl FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cc.tmct0TipoCuentaDeCompensacion.cdCodigo='CI' OR cc.tmct0TipoCuentaDeCompensacion.cdCodigo='CP' ORDER BY cl.cdCodigo" )
	List< Tmct0CuentaLiquidacion > findAllByCuentaDeCompensacionIsPropiaOrIndividual();

	/**
	 * Find by cd codigo.
	 *
	 * @param cdCodigo the cd codigo
	 * @return the tmct0 cuentas de compensacion
	 */
	@Query( "SELECT DISTINCT cc FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cl.cdCodigo = :cdCodigo" )
	List<Tmct0CuentasDeCompensacion> findByCdCodigo( @Param( "cdCodigo" ) String cdCodigo );

	/**
	 * Find by cuenta norma43.
	 *
	 * @param cuentaNorma43 the cuenta norma43
	 * @return the list
	 */
	@Query( "SELECT DISTINCT cc FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cl.cuentaNorma43 = :cuentaNorma43" )
	List< Tmct0CuentasDeCompensacion > findByCuentaNorma43( @Param( "cuentaNorma43" ) Long cuentaNorma43 );

	@Query("SELECT c FROM Tmct0CuentaLiquidacion c WHERE c.cuentaIberclear = :cuentaIberclear")
	List<Tmct0CuentaLiquidacion> findByCuentaIberclear( @Param( "cuentaIberclear" ) String cuentaIberclear );
	
	@Query("SELECT l FROM Tmct0CuentasDeCompensacion c JOIN c.tmct0CuentaLiquidacion l "
			+ "WHERE c.numeroCuentaSubcustodio = :numeroCuentaSubcustodio")
	List<Tmct0CuentaLiquidacion> findByNumeroCuentaSubcustodioCuentaDeCompensacion(
			@Param("numeroCuentaSubcustodio") String numeroCuentaSubcustodio);
	

}

