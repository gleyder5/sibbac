/**
 * 
 */
package sibbac.business.wrappers.database.bo.bancaprivadaexception;

/**
 * @author xIS16630
 *
 */
public class BancaPrivadaWriterAliasNotFoundException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 4036517311069598288L;

  /**
   * 
   */
  public BancaPrivadaWriterAliasNotFoundException() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public BancaPrivadaWriterAliasNotFoundException(String arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public BancaPrivadaWriterAliasNotFoundException(Throwable arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public BancaPrivadaWriterAliasNotFoundException(String arg0, Throwable arg1) {
    super(arg0, arg1);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   * @param arg2
   * @param arg3
   */
  public BancaPrivadaWriterAliasNotFoundException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
    // TODO Auto-generated constructor stub
  }

}
