package sibbac.business.wrappers.database.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.data.TuplaIsinAlias;
import sibbac.business.wrappers.database.model.Tmct0SaldoInicialCuenta;

public interface Tmct0SaldoInicialCuentaDao extends JpaRepository< Tmct0SaldoInicialCuenta, Long > {

	public List< Tmct0SaldoInicialCuenta > findAllByFecha( Date date );

	@Query( "SELECT saldo  FROM Tmct0SaldoInicialCuenta saldo "
			+ "WHERE saldo.isin = :isin AND saldo.cdaliass = :cdaliass AND saldo.cdcodigocuentaliq = :cdcodigocuentaliq" )
	public Tmct0SaldoInicialCuenta findOneByIsinAndAliasAndCuentaLiquidacion( @Param( "isin" ) String isin,
			@Param( "cdaliass" ) String cdaliass, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	public Tmct0SaldoInicialCuenta findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc( String isin,
			String cdcodigocuentaliq, String cdaliass, Date fecha );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha" )
	public List< TuplaIsinAlias > getTuplas( @Param( "fecha" ) Date fecha );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin" )
	public List< TuplaIsinAlias > getTuplasByIsin( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin" )
	public List< TuplaIsinAlias > getTuplasByNotIsin( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdaliass=:alias" )
	public List< TuplaIsinAlias > getTuplasByAlias( @Param( "fecha" ) Date fecha, @Param( "alias" ) String alias );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdaliass<>:alias" )
	public List< TuplaIsinAlias > getTuplasByNotAlias( @Param( "fecha" ) Date fecha, @Param( "alias" ) String alias );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdaliass=:alias  AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public	List< TuplaIsinAlias > getTuplasByAliasAndCuenta( @Param( "fecha" ) Date fecha, @Param( "alias" ) String alias,
					@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdaliass<>:alias  AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public	List< TuplaIsinAlias > getTuplasByNotAliasAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "alias" ) String alias,
					@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );


	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdaliass=:alias  AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public	List< TuplaIsinAlias > getTuplasByAliasAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "alias" ) String alias,
					@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );


	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdaliass<>:alias  AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public	List< TuplaIsinAlias > getTuplasByNotAliasAndCuenta( @Param( "fecha" ) Date fecha, @Param( "alias" ) String alias,
					@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );
	
	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdaliass=:alias" )
	public List< TuplaIsinAlias > getTuplasByIsinAndAlias( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdaliass<>:alias" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndNotAlias( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdaliass<>:alias" )
	public List< TuplaIsinAlias > getTuplasByIsinAndNotAlias( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdaliass=:alias" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndAlias( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias );
	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdaliass=:alias "
			+ " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByIsinAndAliasAndCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );
	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdaliass<>:alias "
			+ " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndNotAliasAndCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdaliass<>:alias "
			+ " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByIsinAndNotAliasAndCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdaliass=:alias "
			+ " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndAliasAndCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdaliass=:alias "
			+ " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByIsinAndAliasAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );
	
	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdaliass<>:alias "
			+ " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndNotAliasAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );
	
	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdaliass<>:alias "
			+ " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByIsinAndNotAliasAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );
	
	
	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdaliass=:alias "
			+ " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndAliasAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "alias" ) String alias, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByIsinAndCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );


	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin=:isin AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByIsinAndNotCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );


	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.isin<>:isin AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotIsinAndCuenta( @Param( "fecha" ) Date fecha, @Param( "isin" ) String isin,
			@Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );


	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdcodigocuentaliq=:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByCuenta( @Param( "fecha" ) Date fecha, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );

	@Query( "SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
			+ "FROM Tmct0SaldoInicialCuenta mov WHERE mov.fecha<=:fecha AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq" )
	public List< TuplaIsinAlias > getTuplasByNotCuenta( @Param( "fecha" ) Date fecha, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq );
}
