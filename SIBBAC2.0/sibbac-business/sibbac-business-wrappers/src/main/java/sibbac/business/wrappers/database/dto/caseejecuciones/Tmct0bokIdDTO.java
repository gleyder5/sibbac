package sibbac.business.wrappers.database.dto.caseejecuciones;

public class Tmct0bokIdDTO {

  private final String nuorden;
  private final String nbooking;

  public Tmct0bokIdDTO(String nuorden, String nbooking) {
    super();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
  }

  public String getNuorden() {
    return nuorden;
  }

  public String getNbooking() {
    return nbooking;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((nbooking == null) ? 0 : nbooking.hashCode());
    result = prime * result + ((nuorden == null) ? 0 : nuorden.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0bokIdDTO other = (Tmct0bokIdDTO) obj;
    if (nbooking == null) {
      if (other.nbooking != null)
        return false;
    } else if (!nbooking.equals(other.nbooking))
      return false;
    if (nuorden == null) {
      if (other.nuorden != null)
        return false;
    } else if (!nuorden.equals(other.nuorden))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Tmct0bokIdDTO [nuorden=" + nuorden + ", nbooking=" + nbooking + "]";
  }

}
