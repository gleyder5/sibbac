package sibbac.business.wrappers.database.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.data.AliasFacturableData;
import sibbac.business.wrappers.database.dto.AliasDTO;
import sibbac.business.wrappers.database.model.Alias;

@Component
public interface AliasDao extends JpaRepository<Alias, Long> {

  @Query("select a from Alias a where a.tmct0ali.id.cdaliass = :cdaliass")
  public Alias findByCdaliass(@Param("cdaliass") String cdaliass);

  public List<Alias> findAllBytmct0ali_Id_Cdbrocli(final String cdbrocli);

  @Query("SELECT new sibbac.business.wrappers.database.data.AliasFacturableData (alias.aliasFacturacion, alias.tmct0ali.id.cdaliass) FROM Alias alias where alias.aliasFacturacion.tmct0ali.id.cdaliass in (:cdaliasAProcesar)")
  public List<AliasFacturableData> findDistinctAliasFacturacion(
      @Param("cdaliasAProcesar") final Set<String> cdaliasAProcesar);

  @Query("select a from Alias a where a.id = :id")
  public Alias findById(@Param("id") Integer id);

  @Query(value = "SELECT aali.id, aali.ID_ALIAS_FACTURACION, aali.ID_IDIOMA, aali.cdaliass, ali.DESCRALI "
      + "FROM bsnbpsql.tmct0cli cli " + "  INNER JOIN bsnbpsql.tmct0ali ali " + "  ON cli.cdbrocli = ali.cdbrocli "
      + "  INNER JOIN bsnbpsql.tmct0_alias aali " + "  ON ali.cdaliass= aali.CDALIASS "
      + "  WHERE cli.CDESTADO = 'A' AND to_char(CURRENT_DATE, 'YYYYMMDD') < ali.fhfinal AND to_char(CURRENT_DATE, 'YYYYMMDD') > ali.fhinicio", nativeQuery = true)
  public List<Object[]> findAllAliasNative();

  @Query("select new sibbac.business.wrappers.database.dto.AliasDTO(a.id,  a.idioma, a.tmct0ali.id.cdaliass, a.tmct0ali.descrali) from Alias a  ")
  public List<AliasDTO> findAllAliasSinFacturacion();

  @Query("select new sibbac.business.wrappers.database.dto.AliasDTO(a.id, a.aliasFacturacion, a.idioma, a.tmct0ali.id.cdaliass, a.tmct0ali.descrali) "
      + "FROM Alias a")
  public List<AliasDTO> findAllAliasFacturacion();

  @Query("select new sibbac.business.wrappers.database.dto.AliasDTO(a.id, a.aliasFacturacion, a.idioma, a.tmct0ali.id.cdaliass, a.tmct0ali.descrali) from Alias a where a.tmct0ali.id.cdbrocli in (:cliente)")
  public List<AliasDTO> findAliasCliente(@Param("cliente") String[] cliente);

  @Query("select a.descrali from Tmct0ali a where a.id.cdaliass = :cdAliass")
  public String findDescraliByCdaliass(@Param("cdAliass") String cdAliass);

}
