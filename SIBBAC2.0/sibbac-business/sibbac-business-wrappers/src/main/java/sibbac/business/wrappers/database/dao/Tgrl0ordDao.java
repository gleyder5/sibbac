package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ord;

@Repository
public interface Tgrl0ordDao extends JpaRepository<Tgrl0ord, Integer> {

  @Query(nativeQuery = true, value = "SELECT C.TPORIGEN FROM BSNVASQL.TVAL0COR C WHERE C.CDORIGEN = :cdorigen ")
  public List<String> findAllTporigenTval0corByCdorigen(@Param("cdorigen") String cdorigen);

}
