package sibbac.business.wrappers.database.dto;

import java.math.BigInteger;

/**
 *	DTO para las lista de contactos de envio por mail.
 *  Se utiliza en task batch de Generacion de informes	 
 */
public class InfoMailContactosDTO {
	
	private String informesSubjectAndBody;
	private BigInteger aliasContacto;
	private String emailContacto;
	private BigInteger idContacto;
	private Character activoContacto;
	private Short activoServicio;
	private String jobNameServicio;
	private String idAlias;
	private String nbDescripcionIdioma;
	
	/**
	 *	Constructor-no-args. 
	 */
	public InfoMailContactosDTO() {
		super();
	}
	
	/**
	 *	Constructor-args. 
	 */
	public InfoMailContactosDTO(String informesSubjectAndBody,
			BigInteger aliasContacto, String emailContacto, Character activoContacto, BigInteger idContacto,
			Short activoServicio, String jobNameServicio, String idAlias, String nbDescripcionIdioma) {
		super();
		this.informesSubjectAndBody = informesSubjectAndBody;
		this.aliasContacto = aliasContacto;
		this.emailContacto = emailContacto;
		this.activoContacto = activoContacto;
		this.idContacto = idContacto;
		this.activoServicio = activoServicio;
		this.jobNameServicio = jobNameServicio;
		this.idAlias = idAlias;
		this.nbDescripcionIdioma = nbDescripcionIdioma;
	}

	public String getInformesSubjectAndBody() {
		return this.informesSubjectAndBody;
	}
	
	public void setInformesSubjectAndBody(String informesSubjectAndBody) {
		this.informesSubjectAndBody = informesSubjectAndBody;
	}
	
	public BigInteger getAliasContacto() {
		return this.aliasContacto;
	}
	
	public void setAliasContacto(BigInteger aliasContacto) {
		this.aliasContacto = aliasContacto;
	}
	
	public String getEmailContacto() {
		return this.emailContacto;
	}
	
	public void setEmailContacto(String emailContacto) {
		this.emailContacto = emailContacto;
	}
	
	public Character getActivoContacto() {
		return this.activoContacto;
	}
	
	public void setActivoContacto(Character activoContacto) {
		this.activoContacto = activoContacto;
	}
	
	public Short getActivoServicio() {
		return this.activoServicio;
	}

	public void setActivoServicio(Short activoServicio) {
		this.activoServicio = activoServicio;
	}

	public String getJobNameServicio() {
		return this.jobNameServicio;
	}
	
	public void setJobNameServicio(String jobNameServicio) {
		this.jobNameServicio = jobNameServicio;
	}
	
	public String getIdAlias() {
		return this.idAlias;
	}
	
	public void setIdAlias(String idAlias) {
		this.idAlias = idAlias;
	}
	
	public String getNbDescripcionIdioma() {
		return this.nbDescripcionIdioma;
	}
	
	public void setNbDescripcionIdioma(String nbDescripcionIdioma) {
		this.nbDescripcionIdioma = nbDescripcionIdioma;
	}

	public BigInteger getIdContacto() {
		return this.idContacto;
	}

	public void setIdContacto(BigInteger idContacto) {
		this.idContacto = idContacto;
	}

}