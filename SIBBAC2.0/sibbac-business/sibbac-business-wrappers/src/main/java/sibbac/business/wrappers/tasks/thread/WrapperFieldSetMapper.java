package sibbac.business.wrappers.tasks.thread;

import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.item.file.transform.Range;

public interface WrapperFieldSetMapper<T> {

  public WrapperFileReaderFieldEnumInterface[] getFields();

  public T getNewInstance();

  public String[] getFieldNames();

  public int[] getLengths();

  public String getFormat();

  /**
   * 
   * @param fieldLenghts
   * @return
   */
  public Range[] getColumnsRanges();
  
  public LineAggregator<T> getLineAggregator();
}
