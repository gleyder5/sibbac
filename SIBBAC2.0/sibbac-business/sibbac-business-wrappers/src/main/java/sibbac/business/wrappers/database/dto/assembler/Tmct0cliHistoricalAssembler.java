package sibbac.business.wrappers.database.dto.assembler;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.dao.ContactoDao;
import sibbac.business.wrappers.database.dao.Tmct0CliMifidDao;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cliHistorical;
import sibbac.business.wrappers.database.model.Tmct0paises;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla
 * TMCT0CLI_HISTORICAL
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0cliHistoricalAssembler {

  @Autowired
  Tmct0CliMifidDao tmct0CliMifidDao;

  @Autowired
  private Tmct0paisesDao tmct0paisesDao;

  @Autowired
  ContactoDao contactoDao;

  /**
   * Se obtiene lista de cambios de historicos.
   * @param clienteModificado clienteModificado
   * @return List<Tmct0cliHistorical> List<Tmct0cliHistorical>
   */
  public List<Tmct0cliHistorical> getCambiosHistoricoCliente(Tmct0cli clienteOriginal, Map<String, Object> paramsObject,
      Tmct0cli clienteParaFechaRev) {

    Map<?, ?> datosCliente = (Map<?, ?>) paramsObject.get("datosCliente");
    String userName = (String) datosCliente.get("userName");
    List<Tmct0cliHistorical> listaHistoricos = new ArrayList<Tmct0cliHistorical>();

    String fechaRevBD = "";
    String fechaRevModificada = "";

    // descli
    if (!(StringUtils.trim((String) datosCliente.get("descli")))
        .equals(StringUtils.trim(clienteOriginal.getDescli()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Descripción", clienteOriginal.getDescli(),
          (String) datosCliente.get("descli"), userName));
    }

    // tpidenti
    String tpidenti = (String) datosCliente.get("tpidenti");
    if (!(StringHelper.getCharFromString(tpidenti)).equals(clienteOriginal.getTpidenti())) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Tipo de Documento",
          String.valueOf(clienteOriginal.getTpidenti()), (String) datosCliente.get("tpidenti"), userName));
    }

    // cif
    if (!(StringUtils.trim((String) datosCliente.get("cif"))).equals(StringUtils.trim(clienteOriginal.getCif()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Nro documento", clienteOriginal.getCif(),
          (String) datosCliente.get("cif"), userName));
    }

    // cdbdp
    if (datosCliente.get("cdbdp") != null)
      if (!(StringUtils.trim((String) datosCliente.get("cdbdp")))
          .equals(StringUtils.trim(clienteOriginal.getCdbdp()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Código de Persona BDP",
            clienteOriginal.getCdbdp(), (String) datosCliente.get("cdbdp"), userName));
      }

    // tpreside
    if (!(StringUtils.trim((String) datosCliente.get("tpreside")))
        .equals(StringUtils.trim(clienteOriginal.getTpreside()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Tipo de Residencia",
          clienteOriginal.getTpreside(), (String) datosCliente.get("tpreside"), userName));
    }

    // tpclienn
    String tpclienn = (String) datosCliente.get("tpclienn");
    if (!(StringHelper.getCharFromString(tpclienn)).equals(clienteOriginal.getTpclienn())) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Tipo de Cliente",
          String.valueOf(clienteOriginal.getTpclienn()), (String) datosCliente.get("tpclienn"), userName));
    }

    // cdkgr
    if (!(StringUtils.trim((String) datosCliente.get("cdkgr"))).equals(StringUtils.trim(clienteOriginal.getCdkgr()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Código KGR", clienteOriginal.getCdkgr(),
          (String) datosCliente.get("cdkgr"), userName));
    }

    // cdcat
    if (!(StringUtils.trim((String) datosCliente.get("cdcat"))).equals(StringUtils.trim(clienteOriginal.getCdcat()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Categorización", clienteOriginal.getCdcat(),
          (String) datosCliente.get("cdcat"), userName));
    }

    // cdbrocli
    if (!(StringUtils.trim((String) datosCliente.get("cdbrocli")))
        .equals(StringUtils.trim(clienteOriginal.getCdbrocli()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Cliente", clienteOriginal.getCdbrocli(),
          (String) datosCliente.get("cdbrocli"), userName));
    }

    // tpfisjur
    String tpfisjur = (String) datosCliente.get("tpfisjur");
    if (!(StringHelper.getCharFromString(tpfisjur)).equals(clienteOriginal.getTpfisjur())) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Naturaleza de Cliente",
          String.valueOf(clienteOriginal.getTpfisjur()), String.valueOf(StringHelper.getCharFromString(tpfisjur)),
          userName));
    }

    // tpNacTit
    String tpNacTit = (String) datosCliente.get("tpNacTit");
    if (!StringHelper.getCharFromString(tpNacTit).equals(clienteOriginal.getTpNacTit())) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Tipo de Nacionalidad",
          String.valueOf(clienteOriginal.getTpNacTit()), String.valueOf(StringHelper.getCharFromString(tpNacTit)),
          userName));
    }

    // cdNacTit
    if (!(StringUtils.trim((String) datosCliente.get("cdNacTit")))
        .equals(StringUtils.trim(clienteOriginal.getCdNacTit()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Cód. del país de Nac.",
          clienteOriginal.getCdNacTit(), (String) datosCliente.get("cdNacTit"), userName));
    }

    // motiPrev
    if (datosCliente.get("motiPrev") != null && !(StringUtils.trim((String) datosCliente.get("motiPrev")))
        .equals(StringUtils.trim(clienteOriginal.getMotiPrev()))) {
      clienteParaFechaRev.setFhRevPrev(new Date());
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Motivo PBC", clienteOriginal.getMotiPrev(),
          (String) datosCliente.get("motiPrev"), userName));
    }

    // Riesgo PBC
    if (datosCliente.get("riesgoPbc") != null && !(StringUtils.trim((String) datosCliente.get("riesgoPbc")))
        .equals(StringUtils.trim(clienteOriginal.getRiesgoPbc()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Riesgo PBC", clienteOriginal.getRiesgoPbc(),
          (String) datosCliente.get("riesgoPbc"), userName));
    }

    // cddeprev
    String cddeprev = (String) datosCliente.get("cddeprev");
    if (!StringHelper.getCharFromString(cddeprev).equals(clienteOriginal.getCddeprev())) {
      clienteParaFechaRev.setFhRevPrev(new Date());
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Control PBC",
          String.valueOf(clienteOriginal.getCddeprev()), (String) datosCliente.get("cddeprev"), userName));
    }

    // tpManCat
    if (!(StringUtils.trim((String) datosCliente.get("tpManCat")))
        .equals(StringUtils.trim(clienteOriginal.getTpManCat()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Ambito Corporativo",
          clienteOriginal.getTpManCat(), (String) datosCliente.get("tpManCat"), userName));
    }

    // ctFiscal
    if (!(StringUtils.trim((String) datosCliente.get("ctFiscal")))
        .equals(StringUtils.trim(clienteOriginal.getCtFiscal()))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Categoría Fiscal", clienteOriginal.getCtFiscal(),
          (String) datosCliente.get("ctFiscal"), userName));
    }

    // Popular historico Address
    this.populateCambiosHistoricoAddress(clienteOriginal, datosCliente, listaHistoricos, userName);

    // Popular historico MIFID
    this.populateCambiosHistoricoMifid(datosCliente, listaHistoricos, userName);

    // Popular historico Contactos
    this.populateCambiosHistoricoContactos(datosCliente, listaHistoricos, userName);

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    if (null != clienteParaFechaRev.getFhRevPrev()) {
      fechaRevModificada = dateFormat.format(clienteParaFechaRev.getFhRevPrev());
    }
    if (null != clienteOriginal.getFhRevPrev()) {
      fechaRevBD = dateFormat.format(clienteOriginal.getFhRevPrev());
    }

    // fhRevPrev
    if (!(StringUtils.trim(fechaRevModificada).equals(StringUtils.trim(fechaRevBD)))) {
      listaHistoricos.add(this.getEntidadHistoricoPopulado("Cliente", "Fecha Revisión Prevención", fechaRevBD,
          fechaRevModificada, userName));
    }

    // private String cdPais; ?
    return listaHistoricos;

  }

  private Tmct0cliHistorical getEntidadHistoricoPopulado(String tpData, String data, String valueprev, String valuenew,
      String userName) {
    if (valueprev == null)
      valueprev = "";
    Tmct0cliHistorical historico = new Tmct0cliHistorical(tpData, data, valueprev, valuenew, new Date(), userName,
        null);
    return historico;
  }

  /**
   * Se popula lista de historico de cliente para direccion
   * @param clienteOriginal clienteOriginal
   * @param datosCliente datosCliente
   * @param listaHistoricos listaHistoricos
   * @param userName userName
   */
  private void populateCambiosHistoricoAddress(Tmct0cli clienteOriginal, Map<?, ?> datosCliente,
      List<Tmct0cliHistorical> listaHistoricos, String userName) {
    Map<?, ?> datosAddress = (Map<?, ?>) datosCliente.get("address");
    if (clienteOriginal.getAddress() != null) {
      // cdDePais
      if (!(StringUtils.trim((String) datosAddress.get("cdDePais")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getCdDePais()))) {

        Tmct0paises paisO = tmct0paisesDao.findByCdHaciendaActivo(clienteOriginal.getAddress().getCdDePais());
        Tmct0paises paisN = tmct0paisesDao.findByCdHaciendaActivo((String) datosAddress.get("cdDePais"));

        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "País de residencia",
            clienteOriginal.getAddress().getCdDePais() + " - " + paisO.getNbpais(),
            (String) datosAddress.get("cdDePais") + " - " + paisN.getNbpais(), userName));

      }

      // tpDomici
      if (!(StringUtils.trim((String) datosAddress.get("tpDomici")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getTpDomici()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "Tipo de dirección",
            clienteOriginal.getAddress().getTpDomici(), (String) datosAddress.get("tpDomici"), userName));
      }

      // nbDomici
      if (!(StringUtils.trim((String) datosAddress.get("nbDomici")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getNbDomici()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "Domicilio",
            clienteOriginal.getAddress().getNbDomici(), (String) datosAddress.get("nbDomici"), userName));
      }

      // nuDomici
      if (!(StringUtils.trim((String) datosAddress.get("nuDomici")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getNuDomici()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "Nro Domicilio",
            clienteOriginal.getAddress().getNuDomici(), (String) datosAddress.get("nuDomici"), userName));
      }

      // nbCiudad
      if (!(StringUtils.trim((String) datosAddress.get("nbCiudad")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getNbCiudad()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "Ciudad",
            clienteOriginal.getAddress().getNbCiudad(), (String) datosAddress.get("nbCiudad"), userName));
      }

      // nbProvin
      if (!(StringUtils.trim((String) datosAddress.get("nbProvin")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getNbProvin()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "Provincia",
            clienteOriginal.getAddress().getNbProvin(), (String) datosAddress.get("nbProvin"), userName));
      }

      // cdPostal
      if (!(StringUtils.trim((String) datosAddress.get("cdPostal")))
          .equals(StringUtils.trim(clienteOriginal.getAddress().getCdPostal()))) {
        listaHistoricos.add(this.getEntidadHistoricoPopulado("Direccion", "Cód. postal",
            clienteOriginal.getAddress().getCdPostal(), (String) datosAddress.get("cdPostal"), userName));
      }
    }
  }

  /**
   * Se popula lista de historico de cliente para mifid
   * @param datosCliente datosCliente
   * @param listaHistoricos listaHistoricos
   * @param userName userName
   */
  private void populateCambiosHistoricoMifid(Map<?, ?> datosCliente, List<Tmct0cliHistorical> listaHistoricos,
      String userName) {

    List<?> mifids = (List<?>) datosCliente.get("mifid");
    if (CollectionUtils.isNotEmpty(mifids)) {
      for (Object mifid : mifids) {
        Map<?, ?> datosMifid = (Map<?, ?>) mifid;
        Tmct0CliMifid mifidOriginal = null;
        if (datosMifid.get("id") != null) {
          BigInteger lidMifid = new BigInteger(datosMifid.get("id").toString());
          if (!lidMifid.equals(BigInteger.ZERO)) {
            mifidOriginal = this.tmct0CliMifidDao.findOne(lidMifid);
          }
        }

        if (mifidOriginal != null) {
          if (mifidOriginal.getCategory() != null && !(StringUtils.trim((String) datosMifid.get("category")))
              .equals(StringUtils.trim(mifidOriginal.getCategory()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("MIFID", "Categoría", mifidOriginal.getCategory(),
                (String) datosMifid.get("category"), userName));
          }

          if (mifidOriginal.gettConven() != null && !(StringUtils.trim((String) datosMifid.get("tConven")))
              .equals(StringUtils.trim(mifidOriginal.gettConven()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("MIFID", "Test de conveniencia",
                mifidOriginal.gettConven(), (String) datosMifid.get("tConven"), userName));
          }

          if (mifidOriginal.gettIdoneid() != null && !(StringUtils.trim((String) datosMifid.get("tIdoneid")))
              .equals(StringUtils.trim(mifidOriginal.gettIdoneid()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("MIFID", "Test de idoneidad",
                mifidOriginal.gettIdoneid(), (String) datosMifid.get("tIdoneid"), userName));
          }

          if (!(StringUtils.trim((String) datosMifid.get("fhConv")))
              .equals(StringUtils.trim(mifidOriginal.getFhConvFormateada()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("MIFID", "Fin vigencia de conveniencia",
                mifidOriginal.getFhConvFormateada(), (String) datosMifid.get("fhConv"), userName));
          }

          if (!(StringUtils.trim((String) datosMifid.get("fhIdon")))
              .equals(StringUtils.trim(mifidOriginal.getFhIdonFormateada()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("MIFID", "Fin vigencia de idoneidad",
                mifidOriginal.getFhIdonFormateada(), (String) datosMifid.get("fhIdon"), userName));
          }
        }
      }
    }
  }

  /**
   * Se popula lista de historico de cliente para contactos
   * @param datosCliente datosCliente
   * @param listaHistoricos listaHistoricos
   * @param userName userName
   */
  private void populateCambiosHistoricoContactos(Map<?, ?> datosCliente, List<Tmct0cliHistorical> listaHistoricos,
      String userName) {

    List<?> contactos = (List<?>) datosCliente.get("contactos");

    if (CollectionUtils.isNotEmpty(contactos)) {
      for (Object contacto : contactos) {
        Map<?, ?> datosContacto = (Map<?, ?>) contacto;
        Contacto contactoOriginal = null;
        if (datosContacto.get("id") != null) {
          Long lidContacto = Long.valueOf(datosContacto.get("id").toString());
          if (lidContacto != 0) {
            contactoOriginal = this.contactoDao.findOne(lidContacto);
          }
        }

        if (contactoOriginal != null) {

          if (contactoOriginal.getNombre() != null && !(StringUtils.trim((String) datosContacto.get("nombre")))
              .equals(StringUtils.trim(contactoOriginal.getNombre()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Nombre", contactoOriginal.getNombre(),
                (String) datosContacto.get("nombre"), userName));
          }

          if (contactoOriginal.getApellido1() != null && !(StringUtils.trim((String) datosContacto.get("apellido1")))
              .equals(StringUtils.trim(contactoOriginal.getApellido1()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Primer Apellido",
                contactoOriginal.getApellido1(), (String) datosContacto.get("apellido1"), userName));
          }

          if (contactoOriginal.getApellido2() != null && !(StringUtils.trim((String) datosContacto.get("apellido2")))
              .equals(StringUtils.trim(contactoOriginal.getApellido2()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Segundo Apellido",
                contactoOriginal.getApellido2(), (String) datosContacto.get("apellido2"), userName));
          }

          if (contactoOriginal.getTelefono1() != null && !(StringUtils.trim((String) datosContacto.get("telefono1")))
              .equals(StringUtils.trim(contactoOriginal.getTelefono1()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Teléfono",
                contactoOriginal.getTelefono1(), (String) datosContacto.get("telefono1"), userName));
          }

          if (contactoOriginal.getEmail() != null && !(StringUtils.trim((String) datosContacto.get("email")))
              .equals(StringUtils.trim(contactoOriginal.getEmail()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Email", contactoOriginal.getEmail(),
                (String) datosContacto.get("email"), userName));
          }

          if (contactoOriginal.getFax() != null && !(StringUtils.trim((String) datosContacto.get("fax")))
              .equals(StringUtils.trim(contactoOriginal.getFax()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Fax", contactoOriginal.getFax(),
                (String) datosContacto.get("fax"), userName));
          }

          if (contactoOriginal.getPosicionCargo() != null
              && !(StringUtils.trim((String) datosContacto.get("posicionCargo")))
                  .equals(StringUtils.trim(contactoOriginal.getPosicionCargo()))) {
            listaHistoricos.add(this.getEntidadHistoricoPopulado("Contacto", "Posición",
                contactoOriginal.getPosicionCargo(), (String) datosContacto.get("posicionCargo"), userName));
          }
        }
      }
    }
  }

}