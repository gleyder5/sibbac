package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Festivo;

@Component
public interface FestivoDao extends JpaRepository<Festivo, Integer> {

  Festivo findById(final Integer id);

  Festivo findByFecha(@Temporal(TemporalType.DATE) Date fecha);

  Long countByFecha(@Temporal(TemporalType.DATE) Date fecha);

  Festivo findByDescripcion(final String descripcion);

  @Query("SELECT COUNT(*) FROM Festivo f WHERE f.fecha >= :fecha AND f.cdmercad = :cdmercad")
  int countByFechaGteAndCdmercad(final @Param("fecha") Date fecha, final @Param("cdmercad") String cdmercad);

  @Query("SELECT f.fecha FROM Festivo f WHERE f.cdmercad = :cdmercad ORDER BY f.fecha asc")
  List<Date> findAllFechaByCdmercad(final @Param("cdmercad") String cdmercad);

}
