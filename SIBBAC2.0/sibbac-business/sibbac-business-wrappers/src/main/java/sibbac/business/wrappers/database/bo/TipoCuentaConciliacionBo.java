package sibbac.business.wrappers.database.bo;


import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.TipoCuentaConciliacionDao;
import sibbac.business.wrappers.database.data.TipoCuentaConciliacionData;
import sibbac.business.wrappers.database.model.TipoCuentaConciliacion;
import sibbac.database.bo.AbstractBo;


@Service
public class TipoCuentaConciliacionBo extends AbstractBo< TipoCuentaConciliacion, Long, TipoCuentaConciliacionDao > {

	public List< TipoCuentaConciliacionData > findAllData() {
		List< TipoCuentaConciliacionData > resultList = new LinkedList< TipoCuentaConciliacionData >();
		resultList.addAll( dao.findAllData() );
		return resultList;
	}
}
