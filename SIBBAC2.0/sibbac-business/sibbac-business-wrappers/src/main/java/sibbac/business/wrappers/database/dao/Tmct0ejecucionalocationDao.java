package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.data.Tmct0ejecucionalocationData;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;

@Repository
public interface Tmct0ejecucionalocationDao extends JpaRepository<Tmct0ejecucionalocation, Long> {

  // @Query(
  // "Select ealo from Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id.nuorden=:nuorden"
  // )
  @Query(value = "SELECT new sibbac.business.wrappers.database.data.Tmct0ejecucionalocationData(ealo) FROM Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id.nuorden=:nuorden")
  List<Tmct0ejecucionalocationData> findAllByNuorden(@Param("nuorden") String nuorden);

  @Query(value = "SELECT new sibbac.business.wrappers.database.model.Tmct0ejecucionalocation(ealo,ealo.tmct0eje, ealo.tmct0grupoejecucion) FROM Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id.nuorden=:nuorden")
  List<Tmct0ejecucionalocation> findAllByNuordenTransactional(@Param("nuorden") String nuorden);

  @Query(value = "SELECT new sibbac.business.wrappers.database.data.Tmct0ejecucionalocationData(ealo) FROM Tmct0ejecucionalocation ealo "
      + " WHERE ealo.tmct0grupoejecucion.tmct0alo.id = :alloId ")
  List<Tmct0ejecucionalocationData> findAllDataByAlloId(@Param("alloId") Tmct0aloId id);

  @Query(value = "SELECT ealo FROM Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id = :alloId "
      + " AND ealo.tmct0grupoejecucion.cdcamara = :camaras ")
  List<Tmct0ejecucionalocation> findAllByAlloIdAndCamarasCompensacion(@Param("alloId") Tmct0aloId id,
      @Param("camaras") String camarasCompensacion);

  @Query(value = "SELECT ealo FROM Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id = :alloId "
      + " AND ealo.tmct0grupoejecucion.cdcamara IN :camaras ")
  List<Tmct0ejecucionalocation> findAllByAlloIdAndInCamarasCompensacion(@Param("alloId") Tmct0aloId id,
      @Param("camaras") List<String> camarasCompensacion);

  @Query(value = "SELECT ealo FROM Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id = :alloId ")
  List<Tmct0ejecucionalocation> findAllByAlloId(@Param("alloId") Tmct0aloId id);

  @Query(value = "SELECT ealo FROM Tmct0ejecucionalocation ealo WHERE ealo.tmct0grupoejecucion.tmct0alo.id.nuorden = :nuorden "
      + " and ealo.tmct0grupoejecucion.tmct0alo.id.nbooking = :nbooking ")
  List<Tmct0ejecucionalocation> findAllByNuordenAndNbooking(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking);

  // //DATOS EJECUCION
  // String nuordenmkt,
  // String nuexemkt,
  // String cdmiembromkt,
  // Character cdindcotizacion,
  // String cdsegmento,
  // String cdPlataformaNegociacion,
  // Date hoejecuc,
  // String tpopebol,
  //
  // // DATOS OPERACIONECC
  // Date feordenmkt,
  // Date hoordenmkt,
  // String nuoperacionprev,
  //
  // // DATOS MOVIMIENTO
  // String cdoperacionecc,
  // String miembroDestino,
  // String cdrefasignacion,
  // String cuentaCompensacion,
  // String cdestadomov,
  // String cdrefmovimiento,
  // String cdrefmovimientoecc,
  // String cdrefnotificacion,
  // String cdrefnotificacionprev,
  // String cdtipomov
  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO(ealo.idejecucionalocation, gru.idgrupoeje, "
      + " gru.fevalor, eje.imcbmerc, gru.cdtipoop, gru.cdsettmkt, gru.cdcamara, gru.asigcomp, gru.tradingvenue, gru.nuagreje, gru.feejecuc, alo.id, eje.id, "
      + " eje.cdbroker, ealo.nutitulos, op.nuordenmkt, op.nuexemkt, op.cdmiembromkt, op.cdindcotizacion, op.cdsegmento, cas.cdPlataformaNegociacion, eje.hoejecuc, eje.tpopebol, "
      + " cas.feordenmkt, cas.hoordenmkt, mf.nuoperacionprev, mf.cdoperacionecc,  mf.miembroCompensadorDestino, mf.cdrefasignacion, mf.cuentaCompensacionDestino, mf.cdestadomov,"
      + " mf.cdrefmovimiento, mf.cdrefmovimientoecc, mf.cdrefnotificacion, mf.cdrefnotificacionprev, mf.cdtipomov, mf.idMovimientoFidessa) "
      + " FROM Tmct0ejecucionalocation ealo, Tmct0grupoejecucion gru, Tmct0alo alo, Tmct0eje eje, "
      + " Tmct0caseoperacioneje cas, Tmct0operacioncdecc op, Tmct0movimientoeccFidessa mf "
      + " WHERE alo.id = :alloId "
      + " AND ealo.tmct0grupoejecucion.idgrupoeje = gru.idgrupoeje "
      + " AND gru.tmct0alo.id = alo.id "
      + " AND ealo.tmct0eje.id=eje.id "
      + " AND  eje.tmct0caseoperacioneje.idcase = cas.idcase "
      + " AND cas.tmct0operacioncdecc.idoperacioncdecc = op.idoperacioncdecc "
      + " AND op.cdoperacionecc = mf.nuoperacionprev "
      + " ORDER BY ealo.nutitulos ASC, eje.imcbmerc ASC, eje.nuopemer ASC, eje.nurefere ASC, eje.id.nuejecuc ASC ")
  List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOConECCByAlloId(
      @Param("alloId") Tmct0aloId id);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO(ealo.idejecucionalocation, gru.idgrupoeje, "
      + " gru.fevalor, eje.imcbmerc, gru.cdtipoop, gru.cdsettmkt, gru.cdcamara, gru.asigcomp, gru.tradingvenue, gru.nuagreje, gru.feejecuc, alo.id, eje.id, "
      + " eje.cdbroker, ealo.nutitulos, eje.nuopemer, eje.nurefere, eje.cdMiembroMkt, eje.indCotizacion, eje.segmenttradingvenue, eje.clearingplatform, eje.hoejecuc, eje.tpopebol, "
      + " eje.feordenmkt, eje.hoordenmkt) "
      + " FROM Tmct0ejecucionalocation ealo, Tmct0grupoejecucion gru, Tmct0alo alo, Tmct0eje eje "
      + " WHERE alo.id = :alloId "
      + " AND ealo.tmct0grupoejecucion.idgrupoeje = gru.idgrupoeje "
      + " AND gru.tmct0alo.id = alo.id "
      + " AND ealo.tmct0eje.id=eje.id "
      + " ORDER BY ealo.nutitulos ASC, eje.imcbmerc ASC, eje.nuopemer ASC, eje.nurefere ASC, eje.id.nuejecuc ASC ")
  List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOSinECCByAlloId(
      @Param("alloId") Tmct0aloId id);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO(eje.id, sum(ealo.nutitulos)) "
      + " FROM Tmct0ejecucionalocation ealo, Tmct0grupoejecucion gru, Tmct0alo alo, Tmct0eje eje "
      + " WHERE ealo.tmct0grupoejecucion.idgrupoeje = gru.idgrupoeje "
      + " AND gru.tmct0alo.id = alo.id "
      + " AND ealo.tmct0eje.id = eje.id "
      + " AND alo.tmct0bok.id = :bookId "
      + " AND alo.tmct0estadoByCdestadoasig.idestado = :cdestadoasig GROUP BY eje.id ")
  List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> findAllTitulosDesglososadosEjeAloOperacionEspecialByBookId(
      @Param("bookId") Tmct0bokId id, @Param("cdestadoasig") int cdestadoasig);

  @Query("SELECT ealo FROM Tmct0ejecucionalocation ealo "
      + "WHERE ealo.tmct0eje.cdisin = :cdisin AND ealo.tmct0eje.feejecuc = :feejecuc AND ealo.tmct0eje.cdtpoper = :sentido "
      + " AND ealo.tmct0eje.clearingplatform = :camara AND ealo.tmct0eje.segmenttradingvenue = :segmento "
      + " AND ealo.tmct0eje.tpopebol = :cdoperacionmkt AND ealo.tmct0eje.cdMiembroMkt = :cdmiembromkt "
      + " AND ealo.tmct0eje.nuopemer = :nuordenmkt AND ealo.tmct0eje.nurefere = :nuexemkt "
      + " AND ealo.tmct0grupoejecucion.tmct0alo.tmct0estadoByCdestadoasig.idestado > 0")
  List<Tmct0ejecucionalocation> findAllByDatosMercado(@Param("cdisin") String cdisin, @Param("feejecuc") Date feejecuc,
      @Param("sentido") char sentido, @Param("camara") String camara, @Param("segmento") String segmento,
      @Param("cdoperacionmkt") String cdoperacionmkt, @Param("cdmiembromkt") String cdmiembromkt,
      @Param("nuordenmkt") String nuordenmkt, @Param("nuexemkt") String nuexemkt, Pageable page);
  
  @Query("SELECT new sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO(d.tmct0eje.imcbmerc, sum(d.nutitulos)) "
      + " from Tmct0ejecucionalocation d where d.tmct0grupoejecucion.tmct0alo.id = :aloId "
      + " GROUP BY d.tmct0eje.imcbmerc ")
  List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByImprecioByAloId(@Param("aloId") Tmct0aloId aloId);
  
  @Query("SELECT new sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO('', '', '', sum(d.nutitulos)) "
      + " from Tmct0ejecucionalocation d where d.tmct0grupoejecucion.tmct0alo.id = :aloId "
      + " and d.tmct0eje.casepti <> 'S' ")
  List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndNotCase(
      @Param("aloId") Tmct0aloId aloId);

  @Query("SELECT new sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO(d.tmct0eje.tmct0caseoperacioneje.tmct0operacioncdecc.tmct0CuentasDeCompensacion.cdCodigo, "
      + " '', '', sum(d.nutitulos)) from Tmct0ejecucionalocation d where d.tmct0grupoejecucion.tmct0alo.id = :aloId "
      + " and d.tmct0eje.casepti = 'S' "
      + " GROUP BY d.tmct0eje.tmct0caseoperacioneje.tmct0operacioncdecc.tmct0CuentasDeCompensacion.cdCodigo")
  List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndCase(
      @Param("aloId") Tmct0aloId aloId);

}
