package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

import sibbac.business.fase0.database.model.Tmct0cleInternacional;
import sibbac.business.fase0.database.model.Tmct0cleNacional;
import sibbac.common.helper.CloneObjectHelper;

public class SettlementDataClearerDTO implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -7906119426151118732L;

  public SettlementDataClearerDTO() {
    initData();
  }

  public SettlementDataClearerDTO(SettlementDataClearerDTO tmct0cle) {
    this();
    CloneObjectHelper.copyBasicFields(tmct0cle, this, null);
  }

  public SettlementDataClearerDTO(Tmct0cleInternacional tmct0cle) {
    this();
    CloneObjectHelper.copyBasicFields(tmct0cle, this, null);
    cdcleare = tmct0cle.getId().getCdcleare();
    cdmercad = tmct0cle.getId().getCdmercad();
    centro = tmct0cle.getId().getCentro();
  }

  /**
   * @param tmct0cle
   */
  public SettlementDataClearerDTO(Tmct0cleNacional tmct0cle) {
    this();
    CloneObjectHelper.copyBasicFields(tmct0cle, this, null);
    cdcleare = tmct0cle.getId().getCdcleare();
    cdmercad = tmct0cle.getId().getCdmercad();
    centro = tmct0cle.getId().getCentro();
  }

  /**
   * Table Field ACBENEFI. BENEFICIARIO ACCOUNT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String acbenefi;

  /**
   * Table Field ACCTATCLE. ACCOUNT TO CLEARER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String acctatcle;

  /**
   * Table Field ACGLOBAL. GLOBAL ACCOUNT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String acglobal;

  /**
   * Table Field ACLOCAL. LOCAL ACCCOUNT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String aclocal;

  /**
   * Table Field ATENCION. A LA ATENCION DE Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String atencion;
  /**
   * Table Field BICBENEFI. BIC BENEFICIARIO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String bicbenefi;
  /**
   * Table Field BICCLE. BIC ENTIDAD LIQUIDADORA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String biccle;
  /**
   * Table Field BICGLOBAL. BIC GLOBAL Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String bicglobal;
  /**
   * Table Field BICLOCAL. BIC LOCAL Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String biclocal;
  /**
   * Table Field CDAGENT. AGENT (S/N) Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String cdagent;
  protected String cdaliass;
  /**
   * Table Field CDBENEFI. BENEFICIARIO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String cdbenefi;
  /**
   * Table Field CDBICMAR. PLACE OF SETTLEMENT Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String cdbicmar;

  /**
   * Table Field CDCLEARE. ENTIDAD LIQUIDADORA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdcleare;

  /** NUEVAS COLUMNAS LIQUIDACION EXTRANJERO **/

  /**
   * Table Field CDGLOBAL. GLOBAL Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String cdglobal;
  /**
   * Table Field CDLOCAL. LOCAL Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String cdlocal;
  /**
   * Table Field CDMEGARA. ENTIDAD LIQUIDADORA MEGARA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdmegara;
  /**
   * Table Field CDMERCAD. MERCADO APLICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdmercad;
  /**
   * Table Field CDORDENA. ORDENANTE ENTIDAD LIQUIDADORA Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal cdordena;
  protected String cdsubcta;
  /**
   * Table Field CENTRO. CENTRO APLICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String centro;
  /**
   * Table Field DSBENEFI. NAME BENEFICIARIO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String dsbenefi;
  /**
   * Table Field DSCLEARE. DESCRIPCION ENTIDAD LIQUIDADORA Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String dscleare;
  /**
   * Table Field DSGLOBAL. NAME GLOBAL Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String dsglobal;
  /**
   * Table Field DSLOCAL. NAME LOCAL Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String dslocal;
  /**
   * Table Field EMAIL. DIRECCION EMAIL Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String email;
  /**
   * Table Field FHSENDFI. DATE SEND FIDESSA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.math.BigDecimal fhsendfi;
  /**
   * Table Field FIDESSA. FIDESSA STATUS I/D/U Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String fidessa;
  /**
   * Table Field IDBENEFI. BENEFICIARIO ID Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String idbenefi;
  /**
   * Table Field IDGLOBAL. GLOBAL ID Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String idglobal;
  /**
   * Table Field IDLOCAL. LOCAL ID Documentación columna <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String idlocal;
  protected Long idRegla;
  /**
   * Table Field ISSENDBE. SEND TO BENEFICIARY Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String issendbe;
  /**
   * Table Field NFAX. NUMERO DE FAX Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String nfax;
  /**
   * Table Field PCCOMISI. % COMISION ENTIDAD LIQUIDADORA Documentación columna
   * <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal pccomisi;

  /**
   * Table Field TPSETTLE. TIPO DE LIQUIDACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   *
   * @generated
   **/
  protected java.lang.String tpsettle;

  /**
   * @return the acbenefi
   */
  public java.lang.String getAcbenefi() {
    return acbenefi;
  }

  /**
   * @param acbenefi the acbenefi to set
   */
  public void setAcbenefi(java.lang.String acbenefi) {
    this.acbenefi = acbenefi;
  }

  /**
   * @return the acctatcle
   */
  public java.lang.String getAcctatcle() {
    return acctatcle;
  }

  /**
   * @param acctatcle the acctatcle to set
   */
  public void setAcctatcle(java.lang.String acctatcle) {
    this.acctatcle = acctatcle;
  }

  /**
   * @return the acglobal
   */
  public java.lang.String getAcglobal() {
    return acglobal;
  }

  /**
   * @param acglobal the acglobal to set
   */
  public void setAcglobal(java.lang.String acglobal) {
    this.acglobal = acglobal;
  }

  /**
   * @return the aclocal
   */
  public java.lang.String getAclocal() {
    return aclocal;
  }

  /**
   * @param aclocal the aclocal to set
   */
  public void setAclocal(java.lang.String aclocal) {
    this.aclocal = aclocal;
  }

  /**
   * @return the atencion
   */
  public java.lang.String getAtencion() {
    return atencion;
  }

  /**
   * @param atencion the atencion to set
   */
  public void setAtencion(java.lang.String atencion) {
    this.atencion = atencion;
  }

  /**
   * @return the bicbenefi
   */
  public java.lang.String getBicbenefi() {
    return bicbenefi;
  }

  /**
   * @param bicbenefi the bicbenefi to set
   */
  public void setBicbenefi(java.lang.String bicbenefi) {
    this.bicbenefi = bicbenefi;
  }

  /**
   * @return the biccle
   */
  public java.lang.String getBiccle() {
    return biccle;
  }

  /**
   * @param biccle the biccle to set
   */
  public void setBiccle(java.lang.String biccle) {
    this.biccle = biccle;
  }

  /**
   * @return the bicglobal
   */
  public java.lang.String getBicglobal() {
    return bicglobal;
  }

  /**
   * @param bicglobal the bicglobal to set
   */
  public void setBicglobal(java.lang.String bicglobal) {
    this.bicglobal = bicglobal;
  }

  /**
   * @return the biclocal
   */
  public java.lang.String getBiclocal() {
    return biclocal;
  }

  /**
   * @param biclocal the biclocal to set
   */
  public void setBiclocal(java.lang.String biclocal) {
    this.biclocal = biclocal;
  }

  /**
   * @return the cdagent
   */
  public java.lang.String getCdagent() {
    return cdagent;
  }

  /**
   * @param cdagent the cdagent to set
   */
  public void setCdagent(java.lang.String cdagent) {
    this.cdagent = cdagent;
  }

  /**
   * @return the cdaliass
   */
  public String getCdaliass() {
    return cdaliass;
  }

  /**
   * @param cdaliass the cdaliass to set
   */
  public void setCdaliass(String cdaliass) {
    this.cdaliass = cdaliass;
  }

  /**
   * @return the cdbenefi
   */
  public java.lang.String getCdbenefi() {
    return cdbenefi;
  }

  /**
   * @param cdbenefi the cdbenefi to set
   */
  public void setCdbenefi(java.lang.String cdbenefi) {
    this.cdbenefi = cdbenefi;
  }

  /**
   * @return the cdbicmar
   */
  public java.lang.String getCdbicmar() {
    return cdbicmar;
  }

  /**
   * @param cdbicmar the cdbicmar to set
   */
  public void setCdbicmar(java.lang.String cdbicmar) {
    this.cdbicmar = cdbicmar;
  }

  /**
   * @return the cdcleare
   */
  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  /**
   * @param cdcleare the cdcleare to set
   */
  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  /**
   * @return the cdglobal
   */
  public java.lang.String getCdglobal() {
    return cdglobal;
  }

  /**
   * @param cdglobal the cdglobal to set
   */
  public void setCdglobal(java.lang.String cdglobal) {
    this.cdglobal = cdglobal;
  }

  /**
   * @return the cdlocal
   */
  public java.lang.String getCdlocal() {
    return cdlocal;
  }

  /**
   * @param cdlocal the cdlocal to set
   */
  public void setCdlocal(java.lang.String cdlocal) {
    this.cdlocal = cdlocal;
  }

  /**
   * @return the cdmegara
   */
  public java.lang.String getCdmegara() {
    return cdmegara;
  }

  /**
   * @param cdmegara the cdmegara to set
   */
  public void setCdmegara(java.lang.String cdmegara) {
    this.cdmegara = cdmegara;
  }

  /**
   * @return the cdmercad
   */
  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  /**
   * @param cdmercad the cdmercad to set
   */
  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }

  /**
   * @return the cdordena
   */
  public java.math.BigDecimal getCdordena() {
    return cdordena;
  }

  /**
   * @param cdordena the cdordena to set
   */
  public void setCdordena(java.math.BigDecimal cdordena) {
    this.cdordena = cdordena;
  }

  /**
   * @return the cdsubcta
   */
  public String getCdsubcta() {
    return cdsubcta;
  }

  /**
   * @param cdsubcta the cdsubcta to set
   */
  public void setCdsubcta(String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  /**
   * @return the centro
   */
  public java.lang.String getCentro() {
    return centro;
  }

  /**
   * @param centro the centro to set
   */
  public void setCentro(java.lang.String centro) {
    this.centro = centro;
  }

  /**
   * @return the dsbenefi
   */
  public java.lang.String getDsbenefi() {
    return dsbenefi;
  }

  /**
   * @param dsbenefi the dsbenefi to set
   */
  public void setDsbenefi(java.lang.String dsbenefi) {
    this.dsbenefi = dsbenefi;
  }

  /**
   * @return the dscleare
   */
  public java.lang.String getDscleare() {
    return dscleare;
  }

  /**
   * @param dscleare the dscleare to set
   */
  public void setDscleare(java.lang.String dscleare) {
    this.dscleare = dscleare;
  }

  /**
   * @return the dsglobal
   */
  public java.lang.String getDsglobal() {
    return dsglobal;
  }

  /**
   * @param dsglobal the dsglobal to set
   */
  public void setDsglobal(java.lang.String dsglobal) {
    this.dsglobal = dsglobal;
  }

  /**
   * @return the dslocal
   */
  public java.lang.String getDslocal() {
    return dslocal;
  }

  /**
   * @param dslocal the dslocal to set
   */
  public void setDslocal(java.lang.String dslocal) {
    this.dslocal = dslocal;
  }

  /**
   * @return the email
   */
  public java.lang.String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(java.lang.String email) {
    this.email = email;
  }

  /**
   * @return the fhsendfi
   */
  public java.math.BigDecimal getFhsendfi() {
    return fhsendfi;
  }

  /**
   * @param fhsendfi the fhsendfi to set
   */
  public void setFhsendfi(java.math.BigDecimal fhsendfi) {
    this.fhsendfi = fhsendfi;
  }

  /**
   * @return the fidessa
   */
  public java.lang.String getFidessa() {
    return fidessa;
  }

  /**
   * @param fidessa the fidessa to set
   */
  public void setFidessa(java.lang.String fidessa) {
    this.fidessa = fidessa;
  }

  /**
   * @return the idbenefi
   */
  public java.lang.String getIdbenefi() {
    return idbenefi;
  }

  /**
   * @param idbenefi the idbenefi to set
   */
  public void setIdbenefi(java.lang.String idbenefi) {
    this.idbenefi = idbenefi;
  }

  /**
   * @return the idglobal
   */
  public java.lang.String getIdglobal() {
    return idglobal;
  }

  /**
   * @param idglobal the idglobal to set
   */
  public void setIdglobal(java.lang.String idglobal) {
    this.idglobal = idglobal;
  }

  /**
   * @return the idlocal
   */
  public java.lang.String getIdlocal() {
    return idlocal;
  }

  /**
   * @param idlocal the idlocal to set
   */
  public void setIdlocal(java.lang.String idlocal) {
    this.idlocal = idlocal;
  }

  /**
   * @return the idRegla
   */
  public Long getIdRegla() {
    return idRegla;
  }

  /**
   * @param idRegla the idRegla to set
   */
  public void setIdRegla(Long idRegla) {
    this.idRegla = idRegla;
  }

  /**
   * @return the issendbe
   */
  public java.lang.String getIssendbe() {
    return issendbe;
  }

  /**
   * @param issendbe the issendbe to set
   */
  public void setIssendbe(java.lang.String issendbe) {
    this.issendbe = issendbe;
  }

  /**
   * @return the nfax
   */
  public java.lang.String getNfax() {
    return nfax;
  }

  /**
   * @param nfax the nfax to set
   */
  public void setNfax(java.lang.String nfax) {
    this.nfax = nfax;
  }

  /**
   * @return the pccomisi
   */
  public java.math.BigDecimal getPccomisi() {
    return pccomisi;
  }

  /**
   * @param pccomisi the pccomisi to set
   */
  public void setPccomisi(java.math.BigDecimal pccomisi) {
    this.pccomisi = pccomisi;
  }

  /**
   * @return the tpsettle
   */
  public java.lang.String getTpsettle() {
    return tpsettle;
  }

  /**
   * @param tpsettle the tpsettle to set
   */
  public void setTpsettle(java.lang.String tpsettle) {
    this.tpsettle = tpsettle;
  }

  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public void initData() {
    setAcctatcle("");
    setAtencion("");
    setBiccle("");
    setCdcleare("");
    setCdmegara("");
    setCdmercad("");
    setCdordena(BigDecimal.ZERO);
    // setCdusuaud("");
    setCentro("");
    setDscleare("");
    setEmail("");
    // clearer.setFhaudit(BigDecimal.ZERO);
    // setFhfinal(BigDecimal.ZERO);
    // setFhinicio(BigDecimal.ZERO);
    setNfax("");
    // setNumsec(BigDecimal.ZERO);
    setPccomisi(BigDecimal.ZERO);
    setTpsettle("");
    setCdbicmar("");
    setIssendbe("");
    setCdlocal("");
    setAclocal("");
    setIdlocal("");
    setDslocal("");
    setBiclocal("");
    setCdglobal("");
    setAcglobal("");
    setIdglobal("");
    setDsglobal("");
    setBicglobal("");
    setCdbenefi("");
    setAcbenefi("");
    setIdbenefi("");
    setDsbenefi("");
    setBicbenefi("");
    setCdagent("");
    setFidessa("");
    setFhsendfi(BigDecimal.ZERO);
  }
  
  

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((acbenefi == null) ? 0 : acbenefi.hashCode());
    result = prime * result + ((acctatcle == null) ? 0 : acctatcle.hashCode());
    result = prime * result + ((acglobal == null) ? 0 : acglobal.hashCode());
    result = prime * result + ((aclocal == null) ? 0 : aclocal.hashCode());
    result = prime * result + ((atencion == null) ? 0 : atencion.hashCode());
    result = prime * result + ((bicbenefi == null) ? 0 : bicbenefi.hashCode());
    result = prime * result + ((biccle == null) ? 0 : biccle.hashCode());
    result = prime * result + ((bicglobal == null) ? 0 : bicglobal.hashCode());
    result = prime * result + ((biclocal == null) ? 0 : biclocal.hashCode());
    result = prime * result + ((cdagent == null) ? 0 : cdagent.hashCode());
    result = prime * result + ((cdaliass == null) ? 0 : cdaliass.hashCode());
    result = prime * result + ((cdbenefi == null) ? 0 : cdbenefi.hashCode());
    result = prime * result + ((cdbicmar == null) ? 0 : cdbicmar.hashCode());
    result = prime * result + ((cdcleare == null) ? 0 : cdcleare.hashCode());
    result = prime * result + ((cdglobal == null) ? 0 : cdglobal.hashCode());
    result = prime * result + ((cdlocal == null) ? 0 : cdlocal.hashCode());
    result = prime * result + ((cdmegara == null) ? 0 : cdmegara.hashCode());
    result = prime * result + ((cdmercad == null) ? 0 : cdmercad.hashCode());
    result = prime * result + ((cdordena == null) ? 0 : cdordena.hashCode());
    result = prime * result + ((cdsubcta == null) ? 0 : cdsubcta.hashCode());
    result = prime * result + ((centro == null) ? 0 : centro.hashCode());
    result = prime * result + ((dsbenefi == null) ? 0 : dsbenefi.hashCode());
    result = prime * result + ((dscleare == null) ? 0 : dscleare.hashCode());
    result = prime * result + ((dsglobal == null) ? 0 : dsglobal.hashCode());
    result = prime * result + ((dslocal == null) ? 0 : dslocal.hashCode());
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((fhsendfi == null) ? 0 : fhsendfi.hashCode());
    result = prime * result + ((fidessa == null) ? 0 : fidessa.hashCode());
    result = prime * result + ((idRegla == null) ? 0 : idRegla.hashCode());
    result = prime * result + ((idbenefi == null) ? 0 : idbenefi.hashCode());
    result = prime * result + ((idglobal == null) ? 0 : idglobal.hashCode());
    result = prime * result + ((idlocal == null) ? 0 : idlocal.hashCode());
    result = prime * result + ((issendbe == null) ? 0 : issendbe.hashCode());
    result = prime * result + ((nfax == null) ? 0 : nfax.hashCode());
    result = prime * result + ((pccomisi == null) ? 0 : pccomisi.hashCode());
    result = prime * result + ((tpsettle == null) ? 0 : tpsettle.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SettlementDataClearerDTO other = (SettlementDataClearerDTO) obj;
    if (acbenefi == null) {
      if (other.acbenefi != null)
        return false;
    }
    else if (!acbenefi.equals(other.acbenefi))
      return false;
    if (acctatcle == null) {
      if (other.acctatcle != null)
        return false;
    }
    else if (!acctatcle.equals(other.acctatcle))
      return false;
    if (acglobal == null) {
      if (other.acglobal != null)
        return false;
    }
    else if (!acglobal.equals(other.acglobal))
      return false;
    if (aclocal == null) {
      if (other.aclocal != null)
        return false;
    }
    else if (!aclocal.equals(other.aclocal))
      return false;
    if (atencion == null) {
      if (other.atencion != null)
        return false;
    }
    else if (!atencion.equals(other.atencion))
      return false;
    if (bicbenefi == null) {
      if (other.bicbenefi != null)
        return false;
    }
    else if (!bicbenefi.equals(other.bicbenefi))
      return false;
    if (biccle == null) {
      if (other.biccle != null)
        return false;
    }
    else if (!biccle.equals(other.biccle))
      return false;
    if (bicglobal == null) {
      if (other.bicglobal != null)
        return false;
    }
    else if (!bicglobal.equals(other.bicglobal))
      return false;
    if (biclocal == null) {
      if (other.biclocal != null)
        return false;
    }
    else if (!biclocal.equals(other.biclocal))
      return false;
    if (cdagent == null) {
      if (other.cdagent != null)
        return false;
    }
    else if (!cdagent.equals(other.cdagent))
      return false;
    if (cdaliass == null) {
      if (other.cdaliass != null)
        return false;
    }
    else if (!cdaliass.equals(other.cdaliass))
      return false;
    if (cdbenefi == null) {
      if (other.cdbenefi != null)
        return false;
    }
    else if (!cdbenefi.equals(other.cdbenefi))
      return false;
    if (cdbicmar == null) {
      if (other.cdbicmar != null)
        return false;
    }
    else if (!cdbicmar.equals(other.cdbicmar))
      return false;
    if (cdcleare == null) {
      if (other.cdcleare != null)
        return false;
    }
    else if (!cdcleare.equals(other.cdcleare))
      return false;
    if (cdglobal == null) {
      if (other.cdglobal != null)
        return false;
    }
    else if (!cdglobal.equals(other.cdglobal))
      return false;
    if (cdlocal == null) {
      if (other.cdlocal != null)
        return false;
    }
    else if (!cdlocal.equals(other.cdlocal))
      return false;
    if (cdmegara == null) {
      if (other.cdmegara != null)
        return false;
    }
    else if (!cdmegara.equals(other.cdmegara))
      return false;
    if (cdmercad == null) {
      if (other.cdmercad != null)
        return false;
    }
    else if (!cdmercad.equals(other.cdmercad))
      return false;
    if (cdordena == null) {
      if (other.cdordena != null)
        return false;
    }
    else if (!cdordena.equals(other.cdordena))
      return false;
    if (cdsubcta == null) {
      if (other.cdsubcta != null)
        return false;
    }
    else if (!cdsubcta.equals(other.cdsubcta))
      return false;
    if (centro == null) {
      if (other.centro != null)
        return false;
    }
    else if (!centro.equals(other.centro))
      return false;
    if (dsbenefi == null) {
      if (other.dsbenefi != null)
        return false;
    }
    else if (!dsbenefi.equals(other.dsbenefi))
      return false;
    if (dscleare == null) {
      if (other.dscleare != null)
        return false;
    }
    else if (!dscleare.equals(other.dscleare))
      return false;
    if (dsglobal == null) {
      if (other.dsglobal != null)
        return false;
    }
    else if (!dsglobal.equals(other.dsglobal))
      return false;
    if (dslocal == null) {
      if (other.dslocal != null)
        return false;
    }
    else if (!dslocal.equals(other.dslocal))
      return false;
    if (email == null) {
      if (other.email != null)
        return false;
    }
    else if (!email.equals(other.email))
      return false;
    if (fhsendfi == null) {
      if (other.fhsendfi != null)
        return false;
    }
    else if (!fhsendfi.equals(other.fhsendfi))
      return false;
    if (fidessa == null) {
      if (other.fidessa != null)
        return false;
    }
    else if (!fidessa.equals(other.fidessa))
      return false;
    if (idRegla == null) {
      if (other.idRegla != null)
        return false;
    }
    else if (!idRegla.equals(other.idRegla))
      return false;
    if (idbenefi == null) {
      if (other.idbenefi != null)
        return false;
    }
    else if (!idbenefi.equals(other.idbenefi))
      return false;
    if (idglobal == null) {
      if (other.idglobal != null)
        return false;
    }
    else if (!idglobal.equals(other.idglobal))
      return false;
    if (idlocal == null) {
      if (other.idlocal != null)
        return false;
    }
    else if (!idlocal.equals(other.idlocal))
      return false;
    if (issendbe == null) {
      if (other.issendbe != null)
        return false;
    }
    else if (!issendbe.equals(other.issendbe))
      return false;
    if (nfax == null) {
      if (other.nfax != null)
        return false;
    }
    else if (!nfax.equals(other.nfax))
      return false;
    if (pccomisi == null) {
      if (other.pccomisi != null)
        return false;
    }
    else if (!pccomisi.equals(other.pccomisi))
      return false;
    if (tpsettle == null) {
      if (other.tpsettle != null)
        return false;
    }
    else if (!tpsettle.equals(other.tpsettle))
      return false;
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SettlementDataClearerDTO [acbenefi=" + acbenefi + ", acctatcle=" + acctatcle + ", acglobal=" + acglobal
        + ", aclocal=" + aclocal + ", atencion=" + atencion + ", bicbenefi=" + bicbenefi + ", biccle=" + biccle
        + ", bicglobal=" + bicglobal + ", biclocal=" + biclocal + ", cdagent=" + cdagent + ", cdaliass=" + cdaliass
        + ", cdbenefi=" + cdbenefi + ", cdbicmar=" + cdbicmar + ", cdcleare=" + cdcleare + ", cdglobal=" + cdglobal
        + ", cdlocal=" + cdlocal + ", cdmegara=" + cdmegara + ", cdmercad=" + cdmercad + ", cdordena=" + cdordena
        + ", cdsubcta=" + cdsubcta + ", centro=" + centro + ", dsbenefi=" + dsbenefi + ", dscleare=" + dscleare
        + ", dsglobal=" + dsglobal + ", dslocal=" + dslocal + ", email=" + email + ", fhsendfi=" + fhsendfi
        + ", fidessa=" + fidessa + ", idbenefi=" + idbenefi + ", idglobal=" + idglobal + ", idlocal=" + idlocal
        + ", idRegla=" + idRegla + ", issendbe=" + issendbe + ", nfax=" + nfax + ", pccomisi=" + pccomisi
        + ", tpsettle=" + tpsettle + "]";
  }

}
