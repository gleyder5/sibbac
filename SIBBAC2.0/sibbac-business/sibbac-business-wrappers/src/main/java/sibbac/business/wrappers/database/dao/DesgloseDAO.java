package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sibbac.business.wrappers.database.model.Tmct0Desglose;

public interface DesgloseDAO extends JpaRepository<Tmct0Desglose, Long>{

}
