package sibbac.business.wrappers.database.bo;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.MovimientoEccS3Dao;
import sibbac.business.wrappers.database.model.MovimientoEccS3;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.database.bo.AbstractBo;

@Service
public class MovimientoEccS3Bo extends AbstractBo<MovimientoEccS3, Long, MovimientoEccS3Dao> {

  public List<MovimientoEccS3> findByCdrefmovimiento(String cdrefmovimiento) {
    return dao.findAllByCdrefmovimiento(cdrefmovimiento);
  }

  public MovimientoEccS3 findByCdrefmovimientoAndNuoperacionprev(String cdrefmovimiento, String nuoperacionprev) {
    return dao.findByCdrefmovimientoAndNuoperacionprev(cdrefmovimiento, nuoperacionprev);
  }

  public void inizializeMovimientoEccS3(Tmct0anotacionecc op, Tmct0movimientoecc mov,
      Tmct0movimientooperacionnuevaecc movn, MovimientoEccS3 ms3) {
    ms3.setAudit_fecha_cambio(new Date());
    ms3.setAudit_user("SIBBAC20");
    ms3.setCdestadomov("20");
    ms3.setCdindcotizacion(op.getCdindcotizacion());
    ms3.setCdisin(op.getCdisin());
    ms3.setCdoperacionecc(null);
    ms3.setCdrefasignacion(null);
    ms3.setCdrefinternaasig(null);
    ms3.setCdrefmovimiento(mov.getCdrefmovimiento());
    ms3.setCdrefmovimientoecc(null);
    ms3.setCdrefnotificacion(null);
    ms3.setCdrefnotificacionprev(null);
    ms3.setCdtipomov(null);
    ms3.setCodigoOperacionMercado(op.getCdoperacionmkt());
    ms3.setCodigoReferenciaAsignacion(null);
    ms3.setCuentaCompensacionDestino(mov.getCuentaCompensacionDestino());
    ms3.setFcontratacion(op.getFcontratacion());
    ms3.setFechaOrden(op.getFordenmkt());
    ms3.setFliquidacion(op.getFliqteorica());
    ms3.setFnegociacion(op.getFnegociacion());
    ms3.setHoraOrden(op.getHordenmkt());
    ms3.setImefectivo(movn.getImefectivo());
    ms3.setImtitulos(movn.getImtitulos());
    ms3.setImtitulosPendientesAsignar(movn.getImtitulos());
    ms3.setIndicadorCapacidad(op.getCdindcapacidad());
    ms3.setMiembroCompensadorDestino(null);
    ms3.setMiembroDestino(null);
    ms3.setMiembroMercado(op.getCdmiembromkt());
    ms3.setMiembroOrigen(null);
    ms3.setMnemotecnico(null);
    ms3.setNuexemkt(op.getNuexemkt());
    ms3.setNuoperacionprev(op.getCdoperacionecc());
    ms3.setCdoperacionecc(op.getCdoperacionecc());
    ms3.setNuordenmkt(op.getNuordenmkt());
    ms3.setPlataforma(op.getCdPlataformaNegociacion());
    ms3.setPrecio(op.getImprecio());
    ms3.setSegmento(op.getCdsegmento());
    ms3.setSentido(op.getCdsentido());
    ms3.setUsuarioDestino(null);
    ms3.setUsuarioOrigen(null);
    ms3.setCdcamara(op.getCdcamara());
  }

  public void inizializeMovimientoEccS3(Tmct0anotacionecc op, String referenciaMovimiento,
      String cuentaCompensacionDestino, MovimientoEccS3 ms3) {
    ms3.setAudit_fecha_cambio(new Date());
    ms3.setAudit_user("SIBBAC2");
    ms3.setCdestadomov("20");
    ms3.setCdindcotizacion(op.getCdindcotizacion());
    ms3.setCdisin(op.getCdisin());
    ms3.setCdoperacionecc(null);
    ms3.setCdrefasignacion(null);
    ms3.setCdrefinternaasig(null);
    ms3.setCdrefmovimiento(referenciaMovimiento);
    ms3.setCdrefmovimientoecc(null);
    ms3.setCdrefnotificacion(null);
    ms3.setCdrefnotificacionprev(null);
    ms3.setCdtipomov(null);
    ms3.setCodigoOperacionMercado(op.getCdoperacionmkt());
    ms3.setCodigoReferenciaAsignacion(null);
    ms3.setCuentaCompensacionDestino(cuentaCompensacionDestino);
    ms3.setFcontratacion(op.getFcontratacion());
    ms3.setFechaOrden(op.getFordenmkt());
    ms3.setFliquidacion(op.getFliqteorica());
    ms3.setFnegociacion(op.getFnegociacion());
    ms3.setHoraOrden(op.getHordenmkt());
    ms3.setImefectivo(op.getImefectivodisponible());
    ms3.setImtitulos(op.getImtitulosdisponibles());
    ms3.setImtitulosPendientesAsignar(op.getImtitulosdisponibles());
    ms3.setIndicadorCapacidad(op.getCdindcapacidad());
    ms3.setMiembroCompensadorDestino(null);
    ms3.setMiembroDestino(null);
    ms3.setMiembroMercado(op.getCdmiembromkt());
    ms3.setMiembroOrigen(null);
    ms3.setMnemotecnico(null);
    ms3.setNuexemkt(op.getNuexemkt());
    ms3.setNuoperacionprev(op.getCdoperacionecc());
    ms3.setCdoperacionecc(op.getCdoperacionecc());
    ms3.setNuordenmkt(op.getNuordenmkt());
    ms3.setPlataforma(op.getCdPlataformaNegociacion());
    ms3.setPrecio(op.getImprecio());
    ms3.setSegmento(op.getCdsegmento());
    ms3.setSentido(op.getCdsentido());
    ms3.setUsuarioDestino(null);
    ms3.setUsuarioOrigen(null);
    ms3.setCdcamara(op.getCdcamara());
  }

}