package sibbac.business.canones.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.canones.commons.CondicionEspecial;
import sibbac.business.canones.commons.CriterioAgrupacion;
import sibbac.business.canones.commons.ReglasCanonesDTO;
import sibbac.business.canones.commons.SimpleComparatorCanonCriteria;
import sibbac.business.canones.commons.TipoCalculo;
import sibbac.business.canones.commons.TipoCanon;
import sibbac.business.canones.dao.ReglasCanonesDao;
import sibbac.business.canones.model.Lista;
import sibbac.business.canones.model.ListaValores;
import sibbac.business.canones.model.MercadoContratacion;
import sibbac.business.canones.model.ReglasCanones;
import sibbac.business.canones.model.ReglasCanonesCondicionesEspeciales;
import sibbac.business.canones.model.ReglasCanonesCriterioAgrupacion;
import sibbac.business.canones.model.ReglasCanonesParametrizaciones;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class ReglasCanonesBo extends AbstractBo<ReglasCanones, BigInteger, ReglasCanonesDao> {

  @Autowired
  private MercadoContratacionBo mercadoContratacionBo;

  @Transactional
  public CanonesConfigDTO getCanonesConfig() throws SIBBACBusinessException {
    CanonesConfigDTO config = new CanonesConfigDTO();
    config.setListReglasCanon(this.findAllReglasActivas());
    config.setMapMercadosContratacion(mercadoContratacionBo.findAllMercadoContratacionDTO());
    return config;
  }

  @Transactional
  private List<ReglasCanonesDTO> findAllReglasActivas() throws SIBBACBusinessException {
    ReglasCanonesDTO dto;
    TipoCanon tipoCanon;
    TipoCalculo tipoCalculo;
    MercadoContratacion mercadoRegla;
    List<MercadoContratacion> listMercados;
    List<ReglasCanonesParametrizaciones> listParametrizaciones;
    List<ReglasCanonesDTO> res = new ArrayList<ReglasCanonesDTO>();
    List<ReglasCanones> listReglas = dao.findAllReglasCanonesActivas();
    for (ReglasCanones regla : listReglas) {

      mercadoRegla = regla.getMercadoContratacion();
      listMercados = mercadoContratacionBo.findAllByMercadoAgrupacion(mercadoRegla.getId());
      if (listMercados.isEmpty()) {
        listMercados.add(mercadoRegla);
      }
      listParametrizaciones = regla.getParametrizaciones();
      if (regla.getParametrizaciones() != null) {
        for (ReglasCanonesParametrizaciones param : listParametrizaciones) {
          if (param.isActivo()) {
            for (MercadoContratacion mercado : listMercados) {
              dto = new ReglasCanonesDTO();
              dto.setIdReglaCanon(regla.getId());
              dto.setIdReglaCanonParametrizacion(param.getId());
              dto.setNombreRegla(regla.getDescripcion());
              tipoCalculo = TipoCalculo.valueOf(regla.getTipoCalculo().getId());
              if (tipoCalculo == null) {
                throw new SIBBACBusinessException(String.format(
                    "No se pueden recuperar las reglas de canones. ERR: Tipo Calculo '%s' no encontrado ", regla
                        .getTipoCalculo().getId()));
              }
              dto.setTipoCalculo(tipoCalculo);

              tipoCanon = TipoCanon.valueOf(regla.getTipoCanon().getId());
              if (tipoCanon == null) {
                throw new SIBBACBusinessException(String.format(
                    "No se pueden recuperar las reglas de canones. ERR: Tipo Canon '%s' no encontrado ", regla
                        .getTipoCanon().getId()));
              }
              dto.setTipoCanon(tipoCanon);

              dto.setMercadoContratacion(mercado.getId());
              dto.setBonificacionComparator(param.getBonificacionComparator());
              dto.setImporteCanonMinimoBonificacion(param.getImporteCanonMinimoBonificacion());
              dto.setImporteCanonMaximoBonificacion(param.getImporteCanonMaximoBonificacion());

              dto.setEfectivoDesde(param.getImporteEfectivoDesde());
              dto.setEfectivoHasta(param.getImporteEfectivoHasta());

              dto.setFechaDesde(regla.getFechaDesde());
              dto.setFechaHasta(regla.getFechaHasta());
              dto.setImporteFijo(param.getImporteCanonFijo());
              dto.setImporteMaximoFijo(param.getImporteCanonMaximo());
              dto.setImporteMinimoFijo(param.getImporteCanonMinimo());
              dto.setPuntoBasicoEfectivo(param.getPuntoBasicoEfectivo());

              if (regla.getAliasComparator() != null && regla.getAliasLista() != null) {
                dto.setListaAliasComparator(SimpleComparatorCanonCriteria.valueOf(regla.getAliasComparator().getId()));
                dto.setListaAlias(this.convert(regla.getAliasLista()));
              }

              if (regla.getIsinComparator() != null && regla.getIsinLista() != null) {
                dto.setListaIsinComparator(SimpleComparatorCanonCriteria.valueOf(regla.getIsinComparator().getId()));
                dto.setListaIsin(this.convert(regla.getIsinLista()));
              }

              if (param.getSegmentoMercadoContratacionComparator() != null
                  && param.getSegmentoMercaContratacionLista() != null) {
                dto.setListaSegmentoMercadoComparator(SimpleComparatorCanonCriteria.valueOf(param
                    .getSegmentoMercadoContratacionComparator().getId()));
                dto.setListSegmentoMercadoContratacion(this.convert(param.getSegmentoMercaContratacionLista()));
              }

              if (param.getTipoOperacionBolsaComparator() != null && param.getTipoOperacionBolsaLista() != null) {
                dto.setListaTipoOperacionBolsaComparator(SimpleComparatorCanonCriteria.valueOf(param
                    .getTipoOperacionBolsaComparator().getId()));
                dto.setListaTipoOperacionBolsa(this.convert(param.getTipoOperacionBolsaLista()));
              }
              this.addCondicionesEspeciales(param, dto);
              this.addCriteriosAgrupacion(param, dto);
              res.add(dto);
            }
          }
        }
      }

    }
    return res;
  }

  private void addCondicionesEspeciales(ReglasCanonesParametrizaciones param, ReglasCanonesDTO dto)
      throws SIBBACBusinessException {
    CondicionEspecial condicion;
    if (param.getCondicionesEspeciales() != null) {
      for (ReglasCanonesCondicionesEspeciales condicionEspecial : param.getCondicionesEspeciales()) {
        try {
          condicion = CondicionEspecial.valueOf(condicionEspecial.getId());
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(String.format(
              "No se pueden recuperar las reglas de canones. ERR: Condicion Especial '%s' no encontrado ",
              condicionEspecial.getId()), e);
        }

        switch (condicion) {
          case ORDEN_PUNTO_MEDIO:
            dto.setOrdenPuntoMedio(true);
            break;
          case ORDEN_OCULTA:
            dto.setOrdenOculta(true);
            break;
          case ORDEN_BLOQUE_COMBINADO:
            dto.setOrdenBloqueCombinado(true);
            break;
          case ORDEN_VOLUMEN_OCULTO:
            dto.setOrdenVolumenOculto(true);
            break;
          case RESTRICCION_ORDEN:
            dto.setRestriccionOrden(true);
            break;
          case EJECUCION_SUBASTA:
            dto.setEjecucionSubasta(true);
            break;
          case ORDEN_CESTA:
            dto.setOrdenCesta(true);
            break;
        }

      }
    }
  }

  private void addCriteriosAgrupacion(ReglasCanonesParametrizaciones param, ReglasCanonesDTO dto)
      throws SIBBACBusinessException {
    CriterioAgrupacion criterio;
    if (param.getAgrupaciones() != null)
      for (ReglasCanonesCriterioAgrupacion agr : param.getAgrupaciones()) {
        try {
          criterio = CriterioAgrupacion.valueOf(agr.getId());
        }
        catch (Exception e) {
          throw new SIBBACBusinessException(
              String.format(
                  "No se pueden recuperar las reglas de canones. ERR: Criterio Agrupacion '%s' no encontrado ",
                  agr.getId()), e);
        }

        switch (criterio) {
          case EJECUCION_MERCADO:
            dto.setCriterioAgrupacionEjecucionMercado(true);
            break;
          case FECHA_CONTRATACION:
            dto.setCriterioAgrupacionFecha(true);
            break;
          case ISIN:
            dto.setCriterioAgrupacionIsin(true);
            break;
          case MERCADO_CONTRATACION:
            dto.setCriterioAgrupacionMercadoContratacion(true);
            break;
          case MIEMBRO_MERCADO:
            dto.setCriterioAgrupacionMiembroMercado(true);
            break;
          case ORDEN_MERCADO:
            dto.setCriterioAgrupacionOrdenMercado(true);
            break;
          case PRECIO:
            dto.setCriterioAgrupacionPrecio(true);
            break;
          case SENTIDO:
            dto.setCriterioAgrupacionSentido(true);
            break;
          case TIPO_OPERACION_BOLSA:
            dto.setCriterioAgrupacionTipoOperacionBolsa(true);
            break;
          case TIPO_SUBASTA:
            dto.setCriterioAgrupacionTipoSubasta(true);
            break;
          case TITULAR:
            dto.setCriterioAgrupacionTitularFinal(true);
            break;
          default:
            break;
        }
      }
  }

  private List<String> convert(Lista lista) {
    List<String> res = null;
    if (lista.getValores() != null && !lista.getValores().isEmpty()) {
      res = new ArrayList<String>();
      for (ListaValores valor : lista.getValores()) {
        res.add(valor.getCodigo());
      }
    }
    return res;
  }

}
