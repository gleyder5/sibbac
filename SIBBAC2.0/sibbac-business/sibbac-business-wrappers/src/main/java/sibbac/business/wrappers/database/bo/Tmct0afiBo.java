package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.persistence.PersistenceException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.common.Results;
import sibbac.business.wrappers.common.fileReader.ValidationHelper;
import sibbac.business.wrappers.database.bo.partenonReader.TitularesExternalValidator;
import sibbac.business.wrappers.database.dao.Tmct0afiDao;
import sibbac.business.wrappers.database.dao.Tmct0afiDaoImpl;
import sibbac.business.wrappers.database.dao.specifications.Tmct0AfiSpecifications;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.data.Tmct0AlcData;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.HolderTipoOrigenDatosType;
import sibbac.business.wrappers.database.filter.TitularesFilter;
import sibbac.business.wrappers.database.filter.TitularesQuery;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0AfiError;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0afiId;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.rest.FuenteTitulares;
import sibbac.common.FormatStyle;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.database.DynamicColumn;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bo.QueryBo;

@Service
public class Tmct0afiBo extends AbstractBo<Tmct0afi, Tmct0afiId, Tmct0afiDao> {

  public static final String MSJ_BLOQUEANTE_1 = "El estado del desglose no permite la modificación, inténtelo más tarde";

  public static final String MSJ_BLOQUEANTE_2 = "El estado del titular no permite la modificación, inténtelo más tarde";
  
  public static final String MSJ_NO_BLOQUEANTE_3 = "No se debe modificar un titular que está enviado a S3, debe solicitar antes la anulación a S3.";
  
  public static final String MSJ_NO_BLOQUEANTE_4 = "El titular de este desglose ya está comunicado a BME/CNMV y la fecha está fuera de plazo para una nueva comunicación.";
  
  public static final String MSJ_BLOQUEANTE_5 = "Error borrando titular";

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0afiBo.class);

  private static final String RESULTADOS_MENSAJE = "mensaje";

  private static final String RESULTADOS_ES_BLOQUEANTE = "esBloqueante";

  private static final int TAMANIO_EXECUTOR = 25;

  private final List<DynamicColumn> columns;

  @Autowired
  private Tmct0afiDaoImpl daoImpl;

  @Autowired
  private Tmct0AfiErrorBo afiErrorBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private QueryBo queryBo;

  @Autowired
  private Tgrl0dirBo tgrl0dirBo;

  @Autowired
  private Tmct0paisesBo paisesBo;

  @Autowired
  private CodigoErrorBo codBo;

  /** Objeto para el acceso a la tabla <code>TMCT0PROVINCIAS</code>. */
  @Autowired
  protected Tmct0provinciasBo tmct0provinciasBo;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0aloBo tmct0aloBo;

  @Autowired
  private TitularesExternalValidator externalValidator;

  public Tmct0afiBo() {
    columns = new ArrayList<>();
    columns.add(new DynamicColumn("Ref. Cliente", "REF_CLIENTE"));
    columns.add(new DynamicColumn("Sentido", "CDTPOPER"));
    columns.add(new DynamicColumn("Isin", "CDISIN"));
    columns.add(new DynamicColumn("Títulos", "NUTITCLI"));
  }

  public BigDecimal getNewTmct0afiSequence() {
    return dao.getNewTmct0afiSequence();
  }

  public List<Tmct0afi> findForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt, Character sentido,
      String nudnicif, String nbclient, String nbclient1, String nbclient2, String nbvalors, Character tpnactit,
      String isin, String alias) {
    return dao.findAll(Tmct0AfiSpecifications.listadoFriltrado(fechaDesde, fechaHasta, mercadoNacInt, sentido,
        nudnicif, nbclient, nbclient1, nbclient2, nbvalors, tpnactit, isin, alias));
  }

  public List<TitularesDTO> findDTOForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, String nudnicif, String nbclient, String nbclient1, String nbclient2, String nbvalors,
      Character tpnactit, String isin, String alias, Character tipodocumento, String paisres, String tpdomici,
      String nbdomici, String nudomici, String provincia, String codpostal, String nureford) {
    return daoImpl.findAllDTO(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, nbclient, nbclient1, nbclient2,
        nbvalors, tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia, codpostal,
        nureford);
  }

  public List<Tmct0afi> findByAlcData(String nuorden, String nbooking, String nucnfclt, Short nucnfliq) {
    return dao.findByAlcData(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  public List<String> findAllCdholderByAlcAndActivos(String nuorden, String nbooking, String nucnfclt, Short nucnfliq) {
    return dao.findAllCdholderByAlcAndActivos(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  public List<Tmct0afi> findActivosByAlcData(String nuorden, String nbooking, String nucnfclt, Short nucnfliq) {
    return dao.findActivosByAlcData(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param mercadoNacInt
   * @param sentido
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param tpnactit
   * @param isin
   * @param alias
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param nureford
   * @param nbooking
   * @param firstRow
   * @param lastRow
   * @return
   */
  public List<TitularesDTO> findDTOForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, List<String> nudnicif, List<String> nbclient, List<String> nbclient1, List<String> nbclient2,
      Character tpnactit, List<String> isin, List<String> alias, Character tipodocumento, String paisres,
      String tpdomici, String nbdomici, String nudomici, String provincia, String codpostal, String nureford,
      String nbooking, Integer firstRow, Integer lastRow) {
    return daoImpl.findAllDTO(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, nbclient, nbclient1, nbclient2,
        tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia, codpostal, nureford,
        nbooking, firstRow, lastRow);
  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param mercadoNacInt
   * @param sentido
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param tpnactit
   * @param isin
   * @param alias
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param nureford
   * @param nbooking
   * @return
   */
  public Integer countFindAllDTOPageable(Date fechaDesde, Date fechaHasta, Character mercadoNacInt, Character sentido,
      List<String> nudnicif, List<String> nbclient, List<String> nbclient1, List<String> nbclient2, Character tpnactit,
      List<String> isin, List<String> alias, Character tipodocumento, String paisres, String tpdomici, String nbdomici,
      String nudomici, String provincia, String codpostal, String nureford, String nbooking) {
    return daoImpl.countFindAllDTOPageable(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, nbclient,
        nbclient1, nbclient2, tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia,
        codpostal, nureford, nbooking);

  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param mercadoNacInt
   * @param sentido
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param tpnactit
   * @param isin
   * @param alias
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param nureford
   * @param nbooking
   * @param startPosition
   * @param maxResult
   * @return
   */
  public List<TitularesDTO> findAllDTOPageable(Date fechaDesde, Date fechaHasta, Character mercadoNacInt,
      Character sentido, List<String> nudnicif, List<String> nbclient, List<String> nbclient1, List<String> nbclient2,
      Character tpnactit, List<String> isin, List<String> alias, Character tipodocumento, String paisres,
      String tpdomici, String nbdomici, String nudomici, String provincia, String codpostal, String nureford,
      String nbooking, Integer startPosition, Integer maxResult) {
    return daoImpl.findAllDTOPageable(fechaDesde, fechaHasta, mercadoNacInt, sentido, nudnicif, nbclient, nbclient1,
        nbclient2, tpnactit, isin, alias, tipodocumento, paisres, tpdomici, nbdomici, nudomici, provincia, codpostal,
        nureford, nbooking, startPosition, maxResult);
  }

  public List<Tmct0afi> findByNureford(String nureford) {

    // return dao.findAll(Tmct0AfiSpecifications.listadoNureford(nureford));
    return dao.findAllTmct0afiByRefClienteAndActivo(nureford);
  }

  public List<Tmct0afi> findActivosByNuorden(String nuorden) {
    return dao.findActivosByNuorden(nuorden);

  }

  public List<Tmct0afi> findActivosByAlloId(Tmct0aloId alloId) {
    return dao.findActivosByAlloId(alloId);

  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void saveAfi(List<PartenonRecordBean> listBeans, final List<ControlTitularesDTO> alcs, short cdbloque,
      final List<Date> holidays, final boolean isOperacionEspecial, String cdusuaud) {
    LOG.trace("[saveAfi] Init");
    final String preffix = "saveAfi";
    Tmct0alcId alcId;
    Tmct0alc alcHb;
    String ccv;
    Collection<Tmct0alcId> listAlcid = new ArrayList<Tmct0alcId>();
    List<String> identificadores;
    String msgLog = "Actualizados titulares de la orden";
    int i = 0;
    for (PartenonRecordBean bean : listBeans) {
      if (i++ > 0) {
        msgLog = String.format("%s,'%s'-'%s'", msgLog, bean.getTiptitu(), StringUtils.trim(bean.getNifbic()));
      }
      else {
        msgLog = String.format("%s '%s'-'%s'", msgLog, bean.getTiptitu(), StringUtils.trim(bean.getNifbic()));
      }

    }
    for (ControlTitularesDTO alc : alcs) {
      LOG.trace("[saveAfi] {}-{} {}", alc.getAlloId(), alc.getNucnfliq(), msgLog);
      ccv = listBeans.get(0).getCcv();
      alcId = new Tmct0alcId(alc.getAlloId(), alc.getNucnfliq());
      identificadores = new ArrayList<String>();
      for (PartenonRecordBean bean : listBeans) {
        identificadores.add(bean.getNifbic().trim());
        saveOneAfi(bean, cdbloque, preffix, alcId);
      }
      if (!isOperacionEspecial) {
        if (listAlcid.add(alcId)) {
          alcHb = alcBo.findById(alcId);
          alcBo.updateAlcModificacionTitulares(alcHb, ccv, identificadores, holidays, cdusuaud);
          logBo.insertarRegistro(alc.getAlloId().getNuorden(), alc.getAlloId().getNbooking(), alc.getAlloId()
              .getNucnfclt(), msgLog, alcHb.getEstadotit().getNombre(), cdusuaud);
        }
      }

    }
    listAlcid.clear();
    LOG.trace("[saveAfi] Fin");
  }

  private void saveOneAfi(PartenonRecordBean bean, short cdbloque, final String preffix, Tmct0alcId alcId) {
    final Tmct0afi afiToSave;
    final Tmct0afiId afiId;
    // final Tmct0alo alo;
    final Tmct0aloId aloId;
    // String message;

    try {
      aloId = new Tmct0aloId(alcId.getNucnfclt(), alcId.getNbooking(), alcId.getNuorden());
      afiToSave = bean.toTmct0afi(cdbloque);
      afiId = new Tmct0afiId(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), alcId.getNucnfliq(),
          getNewTmct0afiSequence());
      afiToSave.setId(afiId);
      afiToSave.setTmct0alo(new Tmct0alo());
      afiToSave.getTmct0alo().setId(aloId);
      LOG.trace("[Tmct0afiBo :: saveAfi] Se va a guardar para el alc con nuorden: " + alcId.getNuorden()
          + ", nbooking: " + alcId.getNbooking() + ", nucnfclt: " + alcId.getNucnfclt() + ", nucnfliq: "
          + alcId.getNucnfliq());
      LOG.trace("[Tmct0afiBo :: saveAfi] Antes de guardar AFI TPNACTIT = " + afiToSave.getTpnactit() + ", RFTITAUD = "
          + afiToSave.getRftitaud() + ", CDDDEBIC= " + afiToSave.getCdddebic() + ", CDHOLDER= "
          + afiToSave.getCdholder());
      // Cambiamos el "M" por "T" en titular antes de que vaya a afi
      if (afiToSave.getTptiprep().equals('M')) {
        afiToSave.setTptiprep('T');
      }
      this.save(afiToSave);
      LOG.trace("[Tmct0afiBo :: saveAfi] Despues de guardar AFI TPNACTIT = " + afiToSave.getTpnactit()
          + ", RFTITAUD = " + afiToSave.getRftitaud() + ", CDDDEBIC= " + afiToSave.getCdddebic() + ", CDHOLDER= "
          + afiToSave.getCdholder());
    }
    catch (PersistenceException ex) {
      LOG.error("[saveOneAfi] Error de persistencia: {} | Origen '{}'", ex.getMessage(), preffix);
      throw ex;
    }
    catch (RuntimeException ex) {
      LOG.error("[saveOneAfi] Error inesperado: {} | Origne {}", ex.getMessage(), preffix);
      throw ex;
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void saveAfiExtranjera(List<PartenonRecordBean> listBeans, List<ControlTitularesDTO> alos, Short cdbloque) {

    LOG.trace("[Tmct0afiBo :: saveAfiExtranjera] Init");

    String msgLog = "Actualizados titulares de la orden";
    int i = 0;
    for (PartenonRecordBean bean : listBeans) {
      if (i++ > 0) {
        msgLog = String.format("%s,'%s'-'%s'", msgLog, bean.getTiptitu(), StringUtils.trim(bean.getNifbic()));
      }
      else {
        msgLog = String.format("%s '%s'-'%s'", msgLog, bean.getTiptitu(), StringUtils.trim(bean.getNifbic()));
      }

    }

    for (ControlTitularesDTO alo : alos) {
      for (PartenonRecordBean bean : listBeans) {

        Tmct0afi afiToSave = bean.toTmct0afi(cdbloque);
        afiToSave.setId(new Tmct0afiId(alo.getAlloId().getNuorden(), alo.getAlloId().getNbooking(), alo.getAlloId()
            .getNucnfclt(), (short) 0, getNewTmct0afiSequence()));
        afiToSave.setTmct0alo(new Tmct0alo());
        afiToSave.getTmct0alo().setId(alo.getAlloId());
        // Tmct0afi afiToSave = createCopyAfi( afi );
        LOG.debug("[saveAfiExtranjera] Se va a guardar para el alc con alo Id {}, ", alo.getAlloId());
        LOG.trace("[saveAfiExtranjera] Antes de guardar AFI TPNACTIT = {}, CDDDEBIC= {}, RFTITAUD = {}, CDHOLDER = {}",
            afiToSave.getTpnactit(), afiToSave.getRftitaud(), afiToSave.getCdddebic(), afiToSave.getCdholder());
        // Cambiamos el "M" por "T" en titular antes de que vaya a afi
        if (afiToSave.getTptiprep().equals('M')) {
          afiToSave.setTptiprep('T');
        }
        this.save(afiToSave);
        LOG.trace(
            "[saveAfiExtranjera] Despues de guardar AFI TPNACTIT = {}, CDDDEBIC= {}, RFTITAUD = {}, CDHOLDER = {}",
            afiToSave.getTpnactit(), afiToSave.getRftitaud(), afiToSave.getCdddebic(), afiToSave.getCdholder());
      }
      logBo.insertarRegistro(alo.getAlloId().getNuorden(), alo.getAlloId().getNbooking(),
          alo.getAlloId().getNucnfclt(), msgLog, "", "PARTENON");
    }
    LOG.debug("[saveAfiExtranjera] Fin");
  }

  public static Tmct0afi createCopyAfi(Tmct0afi afiToCopy) {
    Tmct0afi afiCopied = new Tmct0afi(afiToCopy.getId(), afiToCopy.getTmct0alo(), afiToCopy.getTpdomici(),
        afiToCopy.getNbdomici(), afiToCopy.getNudomici(), afiToCopy.getNbciudad(), afiToCopy.getNbprovin(),
        afiToCopy.getCdpostal(), afiToCopy.getCddepais(), afiToCopy.getNudnicif(), afiToCopy.getNbclient(),
        afiToCopy.getNbclient1(), afiToCopy.getNbclient2(), afiToCopy.getTptiprep(), afiToCopy.getCdnactit(),
        afiToCopy.getFhdenvio(), afiToCopy.getIdproced(), afiToCopy.getCdinacti(), afiToCopy.getFhinacti(),
        afiToCopy.getCdusuina(), afiToCopy.getCdusuaud(), afiToCopy.getFhaudit(), afiToCopy.getNuoprout(),
        afiToCopy.getNupareje(), afiToCopy.getNusecnbr(), afiToCopy.getCdenvaud(), afiToCopy.getTpfisjur(),
        afiToCopy.getCdbloque(), afiToCopy.getCdddebic(), afiToCopy.getTpidenti(), afiToCopy.getTpsocied(),
        afiToCopy.getTpnactit(), afiToCopy.getRftitaud(), afiToCopy.getCdholder(), afiToCopy.getCdreferenciatitular(),
        afiToCopy.getEnviadopti(), afiToCopy.getFechaenvioti(), afiToCopy.getParticip(), afiToCopy.getCdestado(),
        afiToCopy.getCcv());
    return afiCopied;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Short inactivatePreviousAfis(String numorden) {
    Short cdbloque = 0;
    // Calculamos los valores para guardar en afi
    List<Tmct0afi> listaAfis = new ArrayList<Tmct0afi>();
    try {
      listaAfis = this.findByNureford(numorden);
    }
    catch (Exception e) {
      LOG.trace("[Tmct0afiBo - updatePreviousAfis]: WARNING Problema obteniendo afis por nureford: " + numorden
          + e.getMessage());
    }
    if (listaAfis != null && listaAfis.size() != 0) {
      for (Tmct0afi afi : listaAfis) {
        if (cdbloque < afi.getCdbloque()) {
          cdbloque = afi.getCdbloque();
        }
      }
      // Ponemos inactivo el anterior
      for (Tmct0afi afi : listaAfis) {
        if (Short.compare(cdbloque, afi.getCdbloque()) == 0) {
          afi.setCdinacti('I');
          afi.setCdusuina("ROUTING");
          afi.setFhinacti(new Date());
          try {
            this.save(afi);
          }
          catch (Exception e) {
            LOG.trace("[Tmct0afiBo - updatePreviousAfis]: WARNING Problema inactivando afi" + afi.getId()
                + e.getMessage());
          }
        }
      }
    }
    return cdbloque;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Short updatePreviousAfisViaAlc(ControlTitularesDTO data) {
    Short cdbloque = 0;
    // Calculamos los valores para guardar en afi
    List<Tmct0afi> listaAfis = new ArrayList<Tmct0afi>();
    try {
      listaAfis = this.findByAlcData(data.getAlloId().getNuorden(), data.getAlloId().getNbooking(), data.getAlloId()
          .getNucnfclt(), data.getNucnfliq());
    }
    catch (Exception e) {
      LOG.trace("[Tmct0afiBo - updatePreviousAfisViaAlc]: WARNING Problema obteniendo afis por alc" + data.getAlloId()
          + e.getMessage());
    }
    if (listaAfis != null && listaAfis.size() != 0) {
      for (Tmct0afi afi : listaAfis) {
        if (cdbloque < afi.getCdbloque()) {
          cdbloque = afi.getCdbloque();
        }
      }
      // Ponemos inactivo el anterior
      for (Tmct0afi afi : listaAfis) {
        if (Short.compare(cdbloque, afi.getCdbloque()) == 0) {
          afi.setCdinacti('I');
          afi.setCdusuina("ROUTING");
          afi.setFhinacti(new Date());
          try {
            this.save(afi);
          }
          catch (Exception e) {
            LOG.trace("[Tmct0afiBo - updatePreviousAfisViaAlc]: WARNING Problema inactivando afi" + afi.getId()
                + e.getMessage());
          }
        }
      }
    }
    return cdbloque;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Short updatePreviousAfisViaAlo(ControlTitularesDTO data) {
    Short cdbloque = 0;
    // Calculamos los valores para guardar en afi
    List<Tmct0afi> listaAfis = new ArrayList<Tmct0afi>();
    try {
      listaAfis = this.findByAlcData(data.getAlloId().getNuorden(), data.getAlloId().getNbooking(), data.getAlloId()
          .getNucnfclt(), (short) 0);
    }
    catch (Exception e) {
      LOG.trace("[Tmct0afiBo - updatePreviousAfisViaAlo]: WARNING Problema obteniendo afis por alo" + data.getAlloId()
          + e.getMessage());
    }
    if (listaAfis != null && listaAfis.size() != 0) {
      for (Tmct0afi afi : listaAfis) {
        if (cdbloque < afi.getCdbloque()) {
          cdbloque = afi.getCdbloque();
        }
      }
      // Ponemos inactivo el anterior
      for (Tmct0afi afi : listaAfis) {
        if (Short.compare(cdbloque, afi.getCdbloque()) == 0) {
          afi.setCdinacti('I');
          afi.setCdusuina("ROUTING");
          afi.setFhinacti(new Date());
          try {
            this.save(afi);
          }
          catch (Exception e) {
            LOG.trace("[Tmct0afiBo - updatePreviousAfisViaAlo]: WARNING Problema inactivando afi" + afi.getId()
                + e.getMessage());
          }

        }
      }
    }
    return cdbloque;
  }

  public Short updatePreviousAfisOptimizada(String numorden) {
    Short cdbloque = this.obtenerCdBloqueMasAltoPorNuorden(numorden);
    // Calculamos los valores para guardar en afi
    List<Tmct0afi> listaAfis = this.findActivosByNuorden(numorden);
    if (listaAfis != null && listaAfis.size() != 0) {
      // Ponemos inactivo el anterior
      for (Tmct0afi afi : listaAfis) {
        if (Short.compare(cdbloque, afi.getCdbloque()) == 0) {
          afi.setCdinacti('I');
          afi.setCdusuina("ROUTING");
          afi.setFhinacti(new Date());
          this.save(afi);
        }
      }
    }
    return cdbloque;
  }

  public Short updatePreviousAfisOptimizadaByAlloId(Tmct0aloId alloId, String auditUser) {
    Short cdbloque = this.obtenerCdBloqueMasAltoByAlloId(alloId);
    // Calculamos los valores para guardar en afi
    List<Tmct0afi> listaAfis = this.findActivosByAlloId(alloId);
    if (listaAfis != null && listaAfis.size() != 0) {
      // Ponemos inactivo el anterior
      for (Tmct0afi afi : listaAfis) {
        if (Short.compare(cdbloque, afi.getCdbloque()) == 0) {
          afi.setCdinacti('I');
          afi.setCdusuina(auditUser);
          afi.setFhinacti(new Date());
          this.save(afi);
        }
      }
    }
    return cdbloque;
  }

  public Short updatePreviousAfisOptimizadaByAlloIdAndNucnfliq(Tmct0aloId alloId, short nucnfliq, String auditUser) {
    Short cdbloque = this.obtenerCdBloqueMasAltoByAlloId(alloId, nucnfliq);
    // Calculamos los valores para guardar en afi
    List<Tmct0afi> listaAfis = this.findActivosByAlcData(alloId.getNuorden(), alloId.getNbooking(),
        alloId.getNucnfclt(), nucnfliq);
    if (listaAfis != null && listaAfis.size() != 0) {
      // Ponemos inactivo el anterior
      for (Tmct0afi afi : listaAfis) {
        if (Short.compare(cdbloque, afi.getCdbloque()) == 0) {
          afi.setCdinacti('I');
          afi.setCdusuina(auditUser);
          afi.setFhinacti(new Date());
          this.save(afi);
        }
      }
    }
    return cdbloque;
  }

  private Short obtenerCdBloqueMasAltoPorNuorden(String nuorden) {
    Short cdbloque = 0;
    try {
      Short candidatoCdbloque = ((BigDecimal) (this.dao.obtenerCdBloqueMasAltoPorNuorden(nuorden).get(0)))
          .shortValueExact();
      if (candidatoCdbloque != null) {
        cdbloque = candidatoCdbloque;
      }
    }
    catch (Exception e) {
      LOG.trace("No se encontró cdbloque para nuorden: " + nuorden + e.getMessage());
    }
    return cdbloque;
  }

  private Short obtenerCdBloqueMasAltoByAlloId(Tmct0aloId alloId) {
    Short cdbloque = 0;
    try {
      Short candidatoCdbloque = ((Short) (this.dao.obtenerCdBloqueMasAltoByAlloId(alloId).get(0)));
      if (candidatoCdbloque != null) {
        cdbloque = candidatoCdbloque;
      }
    }
    catch (Exception e) {
      LOG.trace("No se encontró cdbloque para nucnfclt: " + alloId + e.getMessage());
    }
    return cdbloque;
  }

  private Short obtenerCdBloqueMasAltoByAlloId(Tmct0aloId alloId, short nucnfliq) {
    Short cdbloque = 0;
    try {
      Short candidatoCdbloque = ((Short) (this.dao.obtenerCdBloqueMasAltoByAlloIdAndNucnfliq(alloId, nucnfliq).get(0)));
      if (candidatoCdbloque != null) {
        cdbloque = candidatoCdbloque;
      }
    }
    catch (Exception e) {
      LOG.trace("No se encontró cdbloque para nucnfclt: " + alloId + e.getMessage());
    }
    return cdbloque;
  }

  public List<Tmct0afi> findForService(Date fechaDesde, Date fechaHasta, Character mercadoNacInt, Character sentido,
      String nudnicif, String codError, String nbclient, String nbclient1, String nbclient2, String nbvalors,
      Character tpnactit, String isin, String alias, Character tipodocumento, String paisres, String tpdomici,
      String nbdomici, String nudomici, String provincia, String codpostal, String ciudad, Character tptiprep,
      Character tpsocied, BigDecimal particip, String ccv, String nacionalidad, String nureford) {
    List<Tmct0afiId> idList = (List<Tmct0afiId>) daoImpl.findAllIds(fechaDesde, fechaHasta, mercadoNacInt, sentido,
        nudnicif, nbclient, nbclient1, nbclient2, nbvalors, tpnactit, isin, alias, tipodocumento, paisres, tpdomici,
        nbdomici, nudomici, provincia, codpostal, ciudad, tptiprep, tpsocied, particip, ccv, nacionalidad, nureford);
    return dao.findByIdIn(idList);
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void guardarBloqueNacionalSinErrores(List<ControlTitularesDTO> alcs, short cdbloque,
      List<PartenonRecordBean> partenonBeanList, List<Date> holidays, boolean isOperacionEspecial, String cdusuaud) {
    LOG.trace("[guardarBloqueNacionalSinErrores] Inicio. Se procesaran {} alcs", alcs.size());
    saveAfi(partenonBeanList, alcs, cdbloque, holidays, isOperacionEspecial, cdusuaud);
    LOG.trace("[guardarBloqueNacionalSinErrores] Fin");
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void grabarAfisSinErroresInternacional(Tmct0alo alo, List<Holder> listHolders, String auditUser)
      throws SIBBACBusinessException {
    Tmct0afi afi;
    Tmct0afiId id;
    if (CollectionUtils.isNotEmpty(listHolders)) {

      short cdbloque;
      try {
        cdbloque = this.obtenerCdbloqueActivoOptimizadaByAlloId(alo.getId(), alo.getRefCliente(), auditUser);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- dando de baja tmct0afis antiguos, alo: '%s' %s",
            alo.getId(), e.getMessage()), e);
      }

      short nusecnbr;
      try {
        nusecnbr = tgrl0dirBo.grabarTitulares(alo, listHolders);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA- mapeando tablas de titulares de AS400, alo: '%s' %s", alo.getId(), e.getMessage()), e);
      }
      String msg;
      try {
        msg = "Add holders operativa internacional ";
        for (Holder holder : listHolders) {
          LOG.trace("[grabarAfisSinErroresInternacional] nuoprout: {} nupareje: {} cargando holder {}...",
              alo.getNuoprout(), alo.getNupareje(), holder.getCdholder());
          LOG.trace("[grabarAfisSinErroresInternacional] holder {}", holder);
          afi = new Tmct0afi();
          id = new Tmct0afiId(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(),
              (short) 0, getNewTmct0afiSequence());
          afi.setId(id);
          afi.setTmct0alo(alo);
          afi.setCdbloque(cdbloque);
          this.loadData(holder, afi);
          afi.setFhaudit(new Date());
          afi.setCdusuaud(auditUser);
          afi.setNuoprout(alo.getNuoprout());
          afi.setNupareje(alo.getNupareje());
          afi.setNusecnbr(nusecnbr);
          save(afi);
          msg = String.format("%s '%s'-'%s'", msg, holder.getTptiprep(), StringUtils.trim(holder.getCdholder()));
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- insertando titulares, alo: '%s' %s", alo.getId(),
            e.getMessage()), e);
      }
      logBo.insertarRegistro(alo, new Date(), new Date(), "", alo.getTmct0sta().getDsestado(), msg, auditUser);
    }
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void grabarAfisSinErroresNacional(Tmct0alc alc, List<Holder> listHolders, String auditUser)
      throws SIBBACBusinessException {
    Tmct0afi afi;
    Tmct0afiId id;
    if (CollectionUtils.isNotEmpty(listHolders)) {

      Tmct0alo alo = alc.getTmct0alo();
      short cdbloque = 1;

      short nusecnbr = 0;

      String msg;
      String idproced = null;
      try {

        msg = String.format("Alta holders Nacional. Tit. Principal: '%s', identificaciones: ", alc.getNbtitliq().trim());
        List<Tmct0afi> listAfis = new ArrayList<Tmct0afi>();

        boolean calcularPorcentajes = false;
        for (Holder holder : listHolders) {
          LOG.trace("[grabarAfisSinErroresNacional] alc: {} cargando holder {}...", alc.getId(), holder.getCdholder());
          LOG.trace("[grabarAfisSinErroresNacional] holder {}", holder);
          afi = new Tmct0afi();
          id = new Tmct0afiId(alo.getId().getNuorden(), alo.getId().getNbooking(), alo.getId().getNucnfclt(), alc
              .getId().getNucnfliq(), getNewTmct0afiSequence());
          afi.setId(id);
          afi.setTmct0alo(alo);
          afi.setCdbloque(cdbloque);
          this.loadData(holder, afi);
          idproced = afi.getIdproced();
          afi.setFhaudit(new Date());
          afi.setCdusuaud(auditUser);
          afi.setNuoprout(alo.getNuoprout());
          afi.setNupareje(alo.getNupareje());
          afi.setNusecnbr(nusecnbr);
          listAfis.add(afi);
          if ((holder.getParticip() == null || holder.getParticip().compareTo(BigDecimal.ZERO) == 0)
              && holder.getTptiprep() != 'R') {
            calcularPorcentajes = true;
          }
          msg = String.format("%s '%s'-'%s'", msg, holder.getTptiprep(), StringUtils.trim(holder.getCdholder()));
        }
        if (!listAfis.isEmpty()) {
          if (calcularPorcentajes) {
            this.calcularPorcentajesParticipacion(listAfis);
          }
          this.save(listAfis);
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- insertando titulares, alc: '%s' %s", alc.getId(),
            e.getMessage()), e);
      }
      if (idproced != null) {
        msg = String.format("Alc: '%s' - Origen Datos: '%s'. %s", alc.getId().getNucnfliq(), idproced, msg);
      }
      logBo.insertarRegistro(alo, new Date(), new Date(), "", "", msg, auditUser);
    }
  }

  private void calcularPorcentajesParticipacion(List<Tmct0afi> listAfis) {
    int countTitulares = 0;
    int countUsufuctuarios = 0;
    for (Tmct0afi afi : listAfis) {
      if (afi.getTptiprep() == 'R') {
        afi.setParticip(null);
      }
      else if (afi.getTptiprep() == 'U') {
        countUsufuctuarios++;
      }
      else {
        countTitulares++;
      }
    }

    BigDecimal porcentajeUsufructo = new BigDecimal("100");
    BigDecimal porcentajeTitular = new BigDecimal("100");

    if (countTitulares > 0) {
      porcentajeTitular = porcentajeTitular.divide(new BigDecimal(countTitulares), 2, RoundingMode.HALF_DOWN);
    }
    if (countUsufuctuarios > 0) {
      porcentajeUsufructo = porcentajeUsufructo.divide(new BigDecimal(countUsufuctuarios), 2, RoundingMode.HALF_DOWN);
    }

    for (Tmct0afi afi : listAfis) {
      if (afi.getTptiprep() == 'U') {
        afi.setParticip(porcentajeUsufructo);
      }
      else if (afi.getTptiprep() != 'R') {
        afi.setParticip(porcentajeTitular);
      }
    }
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void guardarBloqueExtranjeroSinErrores(List<ControlTitularesDTO> alos, Short cdbloque,
      List<PartenonRecordBean> partenonBeanList) {
    this.saveAfiExtranjera(partenonBeanList, alos, cdbloque);
  }

  /**
   * Solicita la desactivación de los Afis previos, la eliminación de los
   * AfiError existentes y devuelve un nuevo número de bloque
   * @param nureford Nureford
   * @return Un entero corto que es el maximo número de bloque más uno.
   */
  @Transactional(propagation = Propagation.REQUIRED)
  public short cleanAfisAndErrorsGetCdBloque(String nureford) {
    short cdbloque;

    LOG.trace("[cleanAfisAndErrorsGetCdBloque] Se van a inactivar los AFI anteriores para nureford " + nureford);
    // Se inactivan los AFI de esa orden
    cdbloque = inactivatePreviousAfis(nureford);
    cdbloque++;
    LOG.trace("[cleanAfisAndErrorsGetCdBloque] Obtenido cdbloque: {}", cdbloque);
    // Se borran los AFI_ERROR de esa orden
    LOG.trace("[cleanAfisAndErrorsGetCdBloque] Se van borrar los AFI_ERROR para nureford {} ", nureford);
    afiErrorBo.deleteByNureford(nureford);
    return cdbloque;
  }

  public Short obtenerCdbloqueActivoOptimizada(String numorden, String nureford) {
    LOG.trace("[Tmct0afiBo :: savePreviousBlock] Se van a inactivar los AFI anteriores para nureford " + nureford);
    // Se inactivan los AFI de esa orden
    Short cdbloque = 0;
    cdbloque = this.updatePreviousAfisOptimizada(numorden);
    cdbloque++;
    LOG.trace("[Tmct0afiBo :: obtenerCdbloqueActivo] Obtenido cdbloque: " + cdbloque);

    return cdbloque;
  }

  public Short obtenerCdbloqueActivoOptimizadaByAlloId(Tmct0aloId tmct0aloId, String nureford, String auditUser) {
    LOG.trace("[Tmct0afiBo :: savePreviousBlock] Se van a inactivar los AFI anteriores para nureford {}", nureford);
    // Se inactivan los AFI de esa orden
    Short cdbloque = 0;
    cdbloque = this.updatePreviousAfisOptimizadaByAlloId(tmct0aloId, auditUser);
    cdbloque++;
    LOG.trace("[Tmct0afiBo :: obtenerCdbloqueActivo] Obtenido cdbloque: {}", cdbloque);
    return cdbloque;
  }

  public Short obtenerCdbloqueActivoOptimizadaByAlloIdAndNucnfliq(Tmct0aloId tmct0aloId, short nucnfliq,
      String auditUser) {
    // Se inactivan los AFI de esa orden
    Short cdbloque = 0;
    cdbloque = this.updatePreviousAfisOptimizadaByAlloIdAndNucnfliq(tmct0aloId, nucnfliq, auditUser);
    cdbloque++;
    LOG.trace("[Tmct0afiBo :: obtenerCdbloqueActivo] Obtenido cdbloque: {}", cdbloque);
    return cdbloque;
  }

  public boolean isOrdenNacional(Tmct0afi afi) {
    boolean isNacional = true;
    Character extranjero = new Character('E');
    try {
      if (extranjero.equals(afi.getTmct0alo().getTmct0bok().getTmct0ord().getCdclsmdo())) {
        isNacional = false;
      }
    }
    catch (Exception e) {
      LOG.trace("[Tmct0afiBo :: isOrdenNacional] WARNING: {}", e.getMessage(), e);
    }
    return isNacional;
  }

  public boolean containsAlcs(Tmct0afi tmct0afi) {
    return (tmct0afi.getTmct0alo() != null && tmct0afi.getTmct0alo().getTmct0alcs() != null && tmct0afi.getTmct0alo()
        .getTmct0alcs().size() > 0);
  }

  public List<Tmct0afi> getAfis(String nuorden, String nbooking, String nucnfclt, String cdreferenciatitular) {
    return dao.getAfis(nuorden, nbooking, nucnfclt, cdreferenciatitular);
  }

  public List<Tmct0afi> getAfis2(String nuorden, String nbooking, String nucnfclt) {
    return dao.getAfis2(nuorden, nbooking, nucnfclt);
  }

  public void findFiltered(TitularesFilter filter, FormatStyle formatStyle, StreamResult streamResult) {
    final TitularesQuery query;

    query = new TitularesQuery(filter);
    queryBo.executeQuery(query, formatStyle, streamResult);
  }
  
  @Transactional
  public Map<String, Object> createTitulares(final FuenteTitulares ctParams) {
    final Map<String, Object> resultados = new HashMap<>();
    final List<PartenonRecordBean> beanList;
    final List<TitularesDTO> titularesDtoList;
    final Results nAfisCreados, nAfisFixed;
    String mensaje = "";

    LOG.debug("[createTitulares] Inicio");
    nAfisCreados = new Results();
    nAfisFixed = new Results();
    beanList = ctParams.listTitulares();
    titularesDtoList = ctParams.listTitularesDto();
    final Map<String, Tmct0paises> isosPaises = paisesBo.findMapAllActivosByIsonnum();
    final Map<String, Tmct0paises> isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();
    final Map<String, List<String>> nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();
    final List<Date> listaFestivos = cfgBo.findHolidaysBolsa();

    if (CollectionUtils.isNotEmpty(ctParams.getListAfiErrorIds())) {
      this.crearTitularesByAfiError(ctParams.getListAfiErrorIds(), beanList, titularesDtoList, isosPaises,
          isosPaisesHacienda, nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed);
    }

    if (CollectionUtils.isNotEmpty(ctParams.getListAfiIds())) {

      this.crearTitularesByAfiId(ctParams.getListAfiIds(), beanList, titularesDtoList, isosPaises, isosPaisesHacienda,
          nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed);
    }

    resultados.put("afiErrorCreated", nAfisCreados.getnAfiError());
    resultados.put("afiErrorFixed", nAfisFixed.getnAfiError());

    for (String msg : nAfisCreados.getMsgs()) {
      if (StringUtils.isNotBlank(msg)) {
        mensaje = String.format("%s %s</br>", mensaje, msg);
      }
    }

    for (String msg : nAfisFixed.getMsgs()) {
      if (StringUtils.isNotBlank(msg)) {
        mensaje = String.format("%s %s</br>", mensaje, msg);
      }
    }

    resultados.put(RESULTADOS_MENSAJE, mensaje);
    resultados.put(RESULTADOS_ES_BLOQUEANTE, nAfisCreados.isBloqueante() || nAfisFixed.isBloqueante());
    LOG.debug("[createTitulares] Fin");
    return resultados;
  }// createTitulares


  @Transactional
  public Map<String, Object> updateTitularesList(final FuenteTitulares ctParams) {
    final TitularesDTO titular;
    List<Tmct0AfiError> listAfiError;
    String mensaje = "";
    final Map<String, Object> resultados = new HashMap<>();
    final Results afiNotProcessed = new Results();
    final Results afiProcessed = new Results();
    final Results afiErrorNotProcessed = new Results();
    final Results afiErrorProcessed = new Results();
    final Results afiErrorFixed = new Results();
    Map<Tmct0alcId, List<Tmct0afiId>> mapAfiIdByAlcId = new HashMap<Tmct0alcId, List<Tmct0afiId>>();
    final List<Date> listaFestivos = cfgBo.findHolidaysBolsa();
    final Map<String, Tmct0paises> isosPaises = paisesBo.findMapAllActivosByIsonnum();
    final Map<String, Tmct0paises> isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();
    final Map<String, List<String>> nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();
    final Map<Object, List<Tmct0AfiError>> mapAfiErrorByAlcIdOrNureford = new HashMap<Object, List<Tmct0AfiError>>();
    Set<Tmct0alc> listaAlcsModificados = new HashSet<Tmct0alc>();
  
    LOG.debug("[updateTitularesList] Init");
    titular = ctParams.getTitular();
    if (CollectionUtils.isNotEmpty(ctParams.getListAfiErrorIds())) {
      listAfiError = new ArrayList<Tmct0AfiError>();
      for (long id : ctParams.getListAfiErrorIds()) {
        listAfiError.add(afiErrorBo.findById(id));
      }
  
      mapAfiErrorByAlcIdOrNureford.putAll(getMapAfiIdGroupByTmct0alcIdOrNureford(listAfiError));
      listAfiError.clear();
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);
      try {
        for (Object key : mapAfiErrorByAlcIdOrNureford.keySet()) {
          List<Long> ids = new ArrayList<Long>();
          for (Tmct0AfiError af : mapAfiErrorByAlcIdOrNureford.get(key)) {
            ids.add(af.getId());
          }
          if (CollectionUtils.isNotEmpty(ids)) {
            executor.execute(getRunnableUpdateTitularesByIdError(ids, titular, isosPaises, isosPaisesHacienda,
                nombresCiudadPorDistrito, listaFestivos, afiProcessed, afiNotProcessed, afiErrorProcessed,
                afiErrorNotProcessed, afiErrorFixed));
            LOG.debug("[updateTitularesList]  procesando {}", ids);
          }
  
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
        try {
          executor.awaitTermination(2, TimeUnit.MINUTES);
        }
        catch (InterruptedException ex) {
          LOG.warn(ex.getMessage(), ex);
        }
      }
      finally {
        if (!executor.isShutdown() && !executor.isTerminated()) {
          executor.shutdown();
          try {
            executor.awaitTermination(20, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
      }
    }
    mapAfiIdByAlcId.putAll(getMapAfiIdGroupByAlc(ctParams.getListAfiIds()));
  
    if (!mapAfiIdByAlcId.isEmpty()) {
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);
      try {
        for (Tmct0alcId alcIdFor : mapAfiIdByAlcId.keySet()) {
          if (CollectionUtils.isNotEmpty(mapAfiIdByAlcId.get(alcIdFor))) {
            executor.execute(getRunnableUpdateTitularesByAlcId(alcIdFor, mapAfiIdByAlcId.get(alcIdFor), titular,
                isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito, listaFestivos, afiProcessed, afiNotProcessed,
                afiErrorProcessed, afiErrorFixed));
          }
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
        try {
          executor.awaitTermination(2, TimeUnit.MINUTES);
        }
        catch (InterruptedException ex) {
          LOG.warn(ex.getMessage(), ex);
        }
      }
      finally {
        if (!executor.isShutdown() && !executor.isTerminated()) {
          executor.shutdown();
          try {
            executor.awaitTermination(20, TimeUnit.MINUTES);
          }
          catch (InterruptedException e) {
            LOG.warn(e.getMessage(), e);
          }
        }
      }
    }
    sendInfoToTmct0Log(listaAlcsModificados, null);
    resultados.put("nAfiErrorProcessed", afiErrorProcessed.getnAfiError());
    resultados.put("nAfiProcessed", afiProcessed.getnAfi());
    resultados.put("nAfiErrorFixed", afiErrorFixed.getnAfiError());
    resultados.put("nAfiErrorNotProcessed", afiErrorNotProcessed.getnAfiError());
    resultados.put("nAfiNotProcessed", afiNotProcessed.getnAfi());
  
    resultados.put(RESULTADOS_ES_BLOQUEANTE, afiNotProcessed.isBloqueante() || afiProcessed.isBloqueante()
        || afiErrorNotProcessed.isBloqueante() || afiErrorProcessed.isBloqueante() || afiErrorFixed.isBloqueante());
  
    for (String msg : afiNotProcessed.msgs) {
      mensaje = String.format("%s %s.</br>", mensaje, msg);
    }
  
    for (String msg : afiProcessed.msgs) {
      mensaje = String.format("%s %s.</br>", mensaje, msg);
    }
    for (String msg : afiErrorNotProcessed.msgs) {
      mensaje = String.format("%s %s.</br>", mensaje, msg);
    }
    for (String msg : afiErrorProcessed.msgs) {
      mensaje = String.format("%s %s.</br>", mensaje, msg);
    }
  
    for (String msg : afiErrorFixed.msgs) {
      mensaje = String.format("%s %s.</br>", mensaje, msg);
    }
  
    resultados.put(RESULTADOS_MENSAJE, mensaje);
    LOG.debug("[updateTitularesList] Fin");
    return resultados;
  } // updateTitularesList
  
  /**
   * Genera datos Fiscales nuevos(Tmct0afi)
   * 
   * @param tmct0alc desglose a cargar datos fiscales
   * @param holder holder
   * @param hpm persistent
   */
  private void loadData(Holder holder, Tmct0afi tmct0afi) {
  
    // Datos
    tmct0afi.setCddepais(holder.getCddepais().trim());
    tmct0afi.setCdenvaud(' ');
    tmct0afi.setCdinacti('A');
    tmct0afi.setCdnactit(holder.getCdnactit().trim());
    tmct0afi.setCdpostal(holder.getCdpostal().trim());
    tmct0afi.setCdusuina("");
    tmct0afi.setFhdenvio(null);
    tmct0afi.setFhinacti(null);
    if (holder.getTpproced() != null) {
      switch (holder.getTpproced()) {
        case HolderTipoOrigenDatosType.MANUAL:
          tmct0afi.setIdproced(HolderTipoOrigenDatosType.MANUAL_DESCRIPCION);
          break;
        case HolderTipoOrigenDatosType.ROUTING:
          tmct0afi.setIdproced(HolderTipoOrigenDatosType.ROUTING_DESCRIPCION);
          break;
        case HolderTipoOrigenDatosType.EXCEL:
          tmct0afi.setIdproced(HolderTipoOrigenDatosType.EXCEL_DESCRIPCION);
          break;
        case HolderTipoOrigenDatosType.INSTRUCCIONES:
        default:
          tmct0afi.setIdproced(HolderTipoOrigenDatosType.INSTRUCCIONES_DESCRIPCION);
          break;
      }
    }
    else {
      tmct0afi.setIdproced(HolderTipoOrigenDatosType.INSTRUCCIONES_DESCRIPCION);
    }
  
    tmct0afi.setNbciudad(holder.getNbciudad().trim());
    tmct0afi.setNbclient(holder.getNbclient().trim());
    tmct0afi.setNbclient1(holder.getNbclien1().trim());
    tmct0afi.setNbclient2(holder.getNbclien2().trim());
    tmct0afi.setNbdomici(holder.getNbdomici().trim());
    tmct0afi.setNbprovin(holder.getNbprovin().trim());
    tmct0afi.setNudnicif(holder.getNudnicif().trim());
    tmct0afi.setNudomici(holder.getNudomici().trim());
    tmct0afi.setTpdomici(holder.getTpdomici().trim());
    tmct0afi.setTpfisjur(holder.getTpfisjur());
    tmct0afi.setTptiprep(holder.getTptiprep());
  
    tmct0afi.setTpidenti(holder.getTpidenti());
    tmct0afi.setCdddebic(holder.getCdddebic());
    tmct0afi.setTpsocied(holder.getTpsocied());
    tmct0afi.setTpnactit(holder.getTpnactit());
    tmct0afi.setCdholder(holder.getCdholder());
  
    tmct0afi.setRftitaud("");
    tmct0afi.setCdreferenciatitular("");
    tmct0afi.setFechaNacimiento(holder.getFechaNacimiento());
  
    tmct0afi.setParticip(holder.getParticip());
  
  }

  @Transactional
  private void crearTitularesByAfiError(final List<Long> afiErrorIds, final List<PartenonRecordBean> beanList,
      final List<TitularesDTO> titularesDtoList, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results nAfisCreados, final Results nAfisFixed) {
    List<Long> ids;
    List<Tmct0AfiError> listAfiError = new ArrayList<Tmct0AfiError>();
    for (long id : afiErrorIds) {
      listAfiError.add(afiErrorBo.findById(id));
    }
    Map<Object, List<Tmct0AfiError>> mapAfiErrorByNureford = getMapAfiIdGroupByTmct0alcIdOrNureford(listAfiError);
    listAfiError.clear();
    if (!mapAfiErrorByNureford.isEmpty()) {
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);
      try {
        for (final Object key : mapAfiErrorByNureford.keySet()) {
          listAfiError = mapAfiErrorByNureford.get(key);
          ids = new ArrayList<Long>();
          for (Tmct0AfiError tmct0AfiError : listAfiError) {
            ids.add(tmct0AfiError.getId());
          }
          if (CollectionUtils.isNotEmpty(ids)) {
            executor.execute(this.getRunnableCrearTitularesAfiErrorId(ids, beanList, titularesDtoList, isosPaises,
                isosPaisesHacienda, nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed));
          }
        }
      }
      catch (Exception e) {
        LOG.debug(e.getMessage(), e);
        executor.shutdownNow();
      }
      finally {
        if (!executor.isShutdown()) {
          executor.shutdown();
        }
        try {
          executor.awaitTermination(20, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
      }
    }

  }
  
  /**
   * @param afisIdAniadirTitular
   * @param beanList
   * @param titularesDtoList
   * @param isosPaises
   * @param isosPaisesHacienda
   * @param nombresCiudadPorDistrito
   * @param listaFestivos
   * @param nAfisCreados
   * @param nAfisFixed
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void crearTitularesByAfiId(final List<Tmct0afiId> afisIdAniadirTitular,
      final List<PartenonRecordBean> beanList, final List<TitularesDTO> titularesDtoList,
      final Map<String, Tmct0paises> isosPaises, final Map<String, Tmct0paises> isosPaisesHacienda,
      final Map<String, List<String>> nombresCiudadPorDistrito, final List<Date> listaFestivos,
      final Results nAfisCreados, final Results nAfisFixed) {
    final Map<Tmct0alcId, List<Tmct0afiId>> mapAfisByAlc = getMapAfiIdGroupByAlc(afisIdAniadirTitular);
    if (!mapAfisByAlc.isEmpty()) {
      ExecutorService executor = Executors.newFixedThreadPool(TAMANIO_EXECUTOR);
      try {
        for (final Tmct0alcId alcId : mapAfisByAlc.keySet()) {
          executor.execute(this.getRunnableCrearTitularesByAlcId(alcId, beanList, titularesDtoList, isosPaises,
              isosPaisesHacienda, nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed));
        }
      }
      catch (Exception e) {
        LOG.debug(e.getMessage(), e);
        executor.shutdownNow();
      }
      finally {
        if (!executor.isShutdown()) {
          executor.shutdown();
        }
        try {
          executor.awaitTermination(20, TimeUnit.MINUTES);
        }
        catch (InterruptedException e) {
          LOG.warn(e.getMessage(), e);
        }
      }
    }
  }

  @Transactional
  private Map<Object, List<Tmct0AfiError>> getMapAfiIdGroupByTmct0alcIdOrNureford(List<Tmct0AfiError> listAfiId) {
    Object key;
    Map<Object, List<Tmct0AfiError>> mapAfiIdByAlcId = new HashMap<Object, List<Tmct0AfiError>>();
    for (Tmct0AfiError afiId : listAfiId) {
      if (afiId.getTmct0alo() != null) {
        key = new Tmct0alcId(afiId.getTmct0alo().getId(), afiId.getNucnfliq());
      }
      else if (StringUtils.isNotBlank(afiId.getNureford())) {
        key = afiId.getNureford();
      }
      else {
        key = afiId.getId();
      }
      LOG.debug("[updateTitularesList]  procesando {}", key);
      if (mapAfiIdByAlcId.get(key) == null) {
        mapAfiIdByAlcId.put(key, new ArrayList<Tmct0AfiError>());
      }
      mapAfiIdByAlcId.get(key).add(afiId);
    }
    return mapAfiIdByAlcId;
  }

  private Runnable getRunnableUpdateTitularesByIdError(final List<Long> ids, final TitularesDTO titular,
      final Map<String, Tmct0paises> isosPaises, final Map<String, Tmct0paises> isosPaisesHacienda,
      final Map<String, List<String>> nombresCiudadPorDistrito, final List<Date> listaFestivos,
      final Results afiProcessed, final Results afiNotProcessed, final Results afiErrorProcessed,
      final Results afiErrorNotProcessed, final Results afiErrorFixed) {
    return new Runnable() {
      @Override
      public void run() {
        try {
          updateTitularesByIdErrorNewTransaction(ids, titular, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, listaFestivos, afiProcessed, afiNotProcessed, afiErrorProcessed,
              afiErrorNotProcessed, afiErrorFixed);
        }
        catch(SIBBACBusinessException e) {
          LOG.error("[getRunnableUpdateTitularesByIdError] Error de lógica de negocio al actualizar titular por IdError",
              e);
        }
        catch(RuntimeException rex) {
          LOG.error("[getRunnableUpdateTitularesByIdError] Error no controlado al actualizar titular por IdError", rex);
        }
      }
    };
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private void updateTitularesByIdErrorNewTransaction(final List<Long> ids, final TitularesDTO titular,
      final Map<String, Tmct0paises> isosPaises, final Map<String, Tmct0paises> isosPaisesHacienda,
      final Map<String, List<String>> nombresCiudadPorDistrito, final List<Date> listaFestivos,
      final Results afiProcessed, final Results afiNotProcessed, final Results afiErrorProcessed,
      final Results afiErrorNotProcessed, final Results afiErrorFixed) throws SIBBACBusinessException {
    try {
      Tmct0AfiError afiError = null;
      for (long id : ids) {
        afiError = afiErrorBo.findById(id);
        PartenonRecordBean bean = new PartenonRecordBean(afiError);
        afiErrorBo.delete(afiError);
        afiError = bean.toTmct0afierror(getNewTmct0afiSequence() + "");
        bean.setAfiError(afiError);
        modifyAfiError(afiError, titular, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
        afiErrorProcessed.addNAfiErrorSynchronized();

      }
      if (afiError != null) {
        if (afiError.getTmct0alo() != null
            && validacionesBloque(afiError.getTmct0alo().getId(), afiError.getNucnfliq(), isosPaises,
                isosPaisesHacienda, nombresCiudadPorDistrito)) {
          afiErrorFixed.addNAfiErrorSynchronized(ids.size());
        }
        else if (StringUtils.isNotBlank(afiError.getNureford())
            && validacionesBloque(afiError.getNureford(), isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
          afiErrorFixed.addNAfiErrorSynchronized(ids.size());
        }
      }
    }
    catch (Exception e) {
      LOG.warn(MessageFormat.format("[updateTitularesList] Excepción actualizar TMCT0_AFI_ERROR id: {0} | Message {1}",
          ids, e.getMessage()), e);
      afiErrorNotProcessed.addNAfiErrorSynchronized();
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
  }

  @Transactional
  public Map<Tmct0alcId, List<Tmct0afiId>> getMapAfiIdGroupByAlc(List<Tmct0afiId> listAfiId) {
    Tmct0alcId alcId;
    Map<Tmct0alcId, List<Tmct0afiId>> mapAfiIdByAlcId = new HashMap<Tmct0alcId, List<Tmct0afiId>>();
    for (Tmct0afiId afiId : listAfiId) {
      alcId = new Tmct0alcId(afiId.getNbooking(), afiId.getNuorden(), afiId.getNucnfliq(), afiId.getNucnfclt());
      LOG.debug("[updateTitularesList]  procesando {}", afiId);
      if (mapAfiIdByAlcId.get(alcId) == null) {
        mapAfiIdByAlcId.put(alcId, new ArrayList<Tmct0afiId>());
      }
      mapAfiIdByAlcId.get(alcId).add(afiId);
    }
    return mapAfiIdByAlcId;
  }

  private Runnable getRunnableUpdateTitularesByAlcId(final Tmct0alcId alcIdFor, final List<Tmct0afiId> listAfiId,
      final TitularesDTO titular, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results afiProcessed, final Results afiNotProcessed,
      final Results afiErrorProcessed, final Results afiErrorFixed) {
    return new Runnable() {
      @Override
      public void run() {
        try {
          updateTitularesByAlcIdNewTransaction(alcIdFor, listAfiId, titular, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, listaFestivos, afiProcessed, afiNotProcessed, afiErrorProcessed, afiErrorFixed);
        }
        catch(SIBBACBusinessException e) {
          LOG.error("[getRunnableUpdateTitularesByAlcId] Error de lógica de negocio al actualizar titular por IdError",
              e);
        }
        catch(RuntimeException rex) {
          LOG.error("[getRunnableUpdateTitularesByAlcId] Error no controlado al actualizar titular por IdError", rex);
        }
      }
    };
  }

  /**
   * Este metodo recorre las listas de Alcs modificados y manda la informacion
   * contenida en infoForLog a TMCT0LOG.
   * 
   * @param listaAlcsModificadosError
   * @param listaAlcsModificados
   * @param infoForLog : viene a null en el caso de la actualizacion manual y
   * relleno en el caso de la masiva
   */
  @Transactional
  public void sendInfoToTmct0Log(Set<Tmct0alc> listaAlcsModificados, ModifiedFields infoForLog) {
    LOG.debug("[sendInfoToTmct0Log] Init");

    // Primero generamos el String que se va a guardar. Depende de si se ha
    // modificado uno o mas de uno
    String desc = "";

    if (infoForLog != null) {
      if (infoForLog.modifiedFields == 1) {
        desc = "Holder data updated by mass update. Field: " + infoForLog.firstName;
      }
      else {
        desc = "Holder data updated by mass update. More than one field (Field1: " + infoForLog.firstName + ")";
      }
    }
    else {
      desc = "Holder data manually updated";
    }
    List<Tmct0alc> alcsSet = new LinkedList<Tmct0alc>(listaAlcsModificados);
    List<Tmct0AlcData> dataList = Tmct0AlcData.entityListToDataList(alcsSet);
    for (Tmct0AlcData alc : dataList) {
      try {
        logBo.insertarRegistro(alc, null, null, null, alc.getEstadotit(), desc, "SIBBAC");

      }
      catch (Exception e) {
        LOG.debug("[sendInfoToTmct0Log] Exception saving into log " + e.getMessage());
      }
    }
    LOG.debug("[sendInfoToTmct0Log] Fin");
  }

  private Runnable getRunnableCrearTitularesAfiErrorId(final List<Long> ids, final List<PartenonRecordBean> beanList,
      final List<TitularesDTO> titularesDtoList, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results nAfisCreados, final Results nAfisFixed) {
    Runnable p = new Runnable() {

      @Override
      public void run() {
        try {
          crearTitularesByAfiErrorNewTransaction(ids, beanList, titularesDtoList, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed);
        }
        catch (Exception e) {

        }
      }
    };
    return p;
  }

  private Runnable getRunnableCrearTitularesByAlcId(final Tmct0alcId alcId, final List<PartenonRecordBean> beanList,
      final List<TitularesDTO> titularesDtoList, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results nAfisCreados, final Results nAfisFixed) {
    Runnable p = new Runnable() {

      @Override
      public void run() {
        try {
          crearTitularesByAlcIdNewTransaction(alcId, beanList, titularesDtoList, isosPaises, isosPaisesHacienda,
              nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed);
        }
        catch (Exception e) {

        }
      }
    };
    return p;
  }

  /**
   * Recibe un tmct0afierror y los distintos campos que pueden ser modificado.
   * Estos campos pueden venir a null desde los filtros o con un valor. Si
   * tienen valor se setean en el tmct0afierror y se guardan.
   * 
   * @param tmct0afierror
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param nacionalidad
   * @param tpnactit
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param ciudad
   * @param tptiprep
   * @param tpsocied
   * @param particip
   * @param ccv
   * @param nureford
   */
  @Transactional
  public void modifyAfiError(Tmct0AfiError tmct0afierror, TitularesDTO titular, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito) {
    // Obtenemos el bean
    PartenonRecordBean bean = new PartenonRecordBean(tmct0afierror);
    // Realizamos cambios en el bean
    modifyBean(bean, titular);

    // Validamos el bean para tener los errores
    List<String> errCodes = validateBean(bean, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);

    bean.updateTmct0afierror(bean.getAfiError());
    afiErrorBo.save(bean.getAfiError());
    afiErrorBo.findAndSetErrors(bean.getAfiError(), errCodes, null);
    // Actualizamos el afiError
    if (CollectionUtils.isNotEmpty(bean.getAfiError().getErrors())) {
      afiErrorBo.save(bean.getAfiError());
    }

  }

  @Transactional
  public boolean validacionesBloque(Tmct0aloId idAlo, Short nucnfliq, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito) {
    List<Tmct0AfiError> bloqueTitularesConError = new ArrayList<Tmct0AfiError>();
    if (nucnfliq != null) {
      bloqueTitularesConError.addAll(afiErrorBo.findGroupByAlcId(new Tmct0alcId(idAlo, nucnfliq)));
    }
    else {

      bloqueTitularesConError.addAll(afiErrorBo.findGroupByAloId(idAlo));

    }
    if (CollectionUtils.isNotEmpty(bloqueTitularesConError)) {
      return validacionesBloque(bloqueTitularesConError, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
    }
    else {
      return true;
    }
  }

  @Transactional
  public boolean validacionesBloque(String nureford, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito) {
    List<Tmct0AfiError> bloqueTitularesConError = new ArrayList<Tmct0AfiError>();
    bloqueTitularesConError.addAll(afiErrorBo.findByNureford(nureford));
    return validacionesBloque(bloqueTitularesConError, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
  }
  
  @Transactional
  private boolean validacionesBloque(List<Tmct0AfiError> bloqueTitularesConError, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito) {

    boolean hasError = true;
    Map<String, CodigoErrorData> mapaErrores = new HashMap<String, CodigoErrorData>();
    List<CodigoErrorData> listaErrores = codBo.findAllByGrupoOrderByCodigoDataAsc("Routing Titulares");
    if (listaErrores != null) {
      for (CodigoErrorData error : listaErrores) {
        mapaErrores.put(error.getCodigo(), error);
      }
    }

    List<PartenonRecordBean> bloqueMismaOrden = new ArrayList<PartenonRecordBean>();
    Map<PartenonRecordBean, List<String>> errorsByBean = new HashMap<PartenonRecordBean, List<String>>();

    List<ControlTitularesDTO> alcs = new ArrayList<ControlTitularesDTO>();
    List<ControlTitularesDTO> alos = new ArrayList<ControlTitularesDTO>();

    for (Tmct0AfiError afiError : bloqueTitularesConError) {
      bloqueMismaOrden.add(new PartenonRecordBean(afiError));
    }

    String errorOrden = comprobarErroresProcedencia(bloqueMismaOrden, alcs, alos);

    hasError = externalValidator.validateAll(errorOrden, bloqueMismaOrden, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito, errorsByBean);

    for (PartenonRecordBean bean : bloqueMismaOrden) {
      if (bean.getAfiError() != null) {
        afiErrorBo.findAndSetErrors(bean.getAfiError(), errorsByBean.get(bean), mapaErrores);
        afiErrorBo.save(bean.getAfiError());
      }
    }

    return !hasError;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private void updateTitularesByAlcIdNewTransaction(final Tmct0alcId alcIdFor, final List<Tmct0afiId> listAfiId,
      final TitularesDTO titular, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results afiProcessed, final Results afiNotProcessed,
      final Results afiErrorProcessed, final Results afiErrorFixed) throws SIBBACBusinessException {
    int aniadidos;
    Tmct0aloId aloId = new Tmct0aloId(alcIdFor.getNucnfclt(), alcIdFor.getNbooking(), alcIdFor.getNuorden());
    try {
      // inicio

      Tmct0alc tmct0alc = alcBo.findById(alcIdFor);
      String mensajeInterno = "";
      if (tmct0alc != null) {
        mensajeInterno = comprobacionesALC(tmct0alc, listaFestivos);
      }
      LOG.debug("[updateTitularesList] msg interno: {}", mensajeInterno);
      if (StringUtils.isNotEmpty(mensajeInterno)) {
        afiNotProcessed.addNAfiSynchronized();
        afiProcessed.setBloqueante(true);
        afiProcessed.addMsg(String.format("%s nuorden '%s'.", mensajeInterno, alcIdFor.getNuorden()));
      }
      else {
        if (CollectionUtils.isNotEmpty(listAfiId)) {
          LOG.debug("[updateTitularesList] borrando titulares en afi error con alcId: {}", alcIdFor);
          afiErrorBo.delete(afiErrorBo.findGroupByAlcId(alcIdFor));

          if (CollectionUtils.isNotEmpty(listAfiId)) {
            for (Tmct0afiId afiIdFor : listAfiId) {
              LOG.debug("[updateTitularesList] actualizando afi id: {}", afiIdFor);
              Tmct0afi tmct0afi = findById(afiIdFor);
              PartenonRecordBean bean = new PartenonRecordBean(tmct0afi);
              Tmct0AfiError afiError = bean.toTmct0afierror(getNewTmct0afiSequence() + "");
              modifyAfiError(afiError, titular, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
              afiErrorProcessed.addNAfiErrorSynchronized();
            }
            aniadidos = this.aniadirAfisToAfiError(aloId, alcIdFor.getNucnfliq(), listAfiId, isosPaises,
                isosPaisesHacienda, nombresCiudadPorDistrito);
            afiErrorProcessed.addNAfiErrorSynchronized(aniadidos);

            if (validacionesBloque(
                new Tmct0aloId(alcIdFor.getNucnfclt(), alcIdFor.getNbooking(), alcIdFor.getNuorden()),
                alcIdFor.getNucnfliq(), isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
              afiErrorFixed.addNAfiErrorSynchronized(listAfiId.size() + aniadidos);
            }
          }
        }
      }
    }
    catch (Exception e) {
      LOG.warn(
          MessageFormat.format("[updateTitularesList] Excepción consulta TMCT0ALC Id {0} | Message {1}", alcIdFor,
              e.getMessage()), e);
      afiErrorProcessed.addNAfiErrorSynchronized(listAfiId.size());
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private void crearTitularesByAfiErrorNewTransaction(final List<Long> ids, final List<PartenonRecordBean> beanList,
      final List<TitularesDTO> titularesDtoList, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results nAfisCreados, final Results nAfisFixed)
      throws SIBBACBusinessException {
    String nureford = null;
    Tmct0AfiError afiError = null;
    try {
      afiError = afiErrorBo.findById(ids.get(0));
      nureford = afiError.getNureford();
      int i = 0;
      for (PartenonRecordBean bean : beanList) {
        Tmct0AfiError afiErrorNuevo = bean.toTmct0afierror(getNewTmct0afiSequence() + "");
        afiErrorNuevo.setTmct0alo(afiError.getTmct0alo());
        afiErrorNuevo.setNucnfliq(afiError.getNucnfliq());
        afiErrorNuevo.setNureford(nureford);
        modifyAfiError(afiErrorNuevo, titularesDtoList.get(i++), isosPaises, isosPaisesHacienda,
            nombresCiudadPorDistrito);
        nAfisCreados.addNAfiErrorSynchronized();
      }
      if (afiError.getTmct0alo() != null) {
        if (validacionesBloque(afiError.getTmct0alo().getId(), afiError.getNucnfliq(), isosPaises, isosPaisesHacienda,
            nombresCiudadPorDistrito)) {
          nAfisFixed.addNAfiErrorSynchronized(titularesDtoList.size());
        }
      }
      else if (nureford != null
          && validacionesBloque(nureford, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
        nAfisFixed.addNAfiErrorSynchronized(titularesDtoList.size());
      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
      nAfisFixed.addMsg(String.format("INCIDENCIA- Aniadiendo titulares a nureford '%s'.", nureford));
      throw new SIBBACBusinessException(e);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private void crearTitularesByAlcIdNewTransaction(final Tmct0alcId alcId, final List<PartenonRecordBean> beanList,
      final List<TitularesDTO> titularesDtoList, final Map<String, Tmct0paises> isosPaises,
      final Map<String, Tmct0paises> isosPaisesHacienda, final Map<String, List<String>> nombresCiudadPorDistrito,
      final List<Date> listaFestivos, final Results nAfisCreados, final Results nAfisFixed)
      throws SIBBACBusinessException {
    String mensaje = "";
    try {
      Tmct0aloId aloId = new Tmct0aloId(alcId.getNucnfclt(), alcId.getNbooking(), alcId.getNuorden());

      List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
      Tmct0ord ord = ordBo.findById(alcId.getNuorden());

      if (ord == null) {
        nAfisFixed.addMsg(String.format("Orden no encontrada '%s'.", alcId.getNuorden()));
      }
      else if (ord.getCdclsmdo() == 'N') {
        if (alcId.getNucnfliq() > 0) {
          Tmct0alc alc = alcBo.findById(alcId);
          if (alc != null && alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            alcs.add(alc);
          }
        }
        else {
          alcs.addAll(alcBo.findByAlloIdActivos(aloId));
        }

        if (CollectionUtils.isEmpty(alcs)) {
          nAfisFixed.addMsg(String.format("Orden Nacional no desglosada '%s'.", alcId.getNuorden()));
        }
        else {
          for (Tmct0alc tmct0alc : alcs) {
            mensaje = aniadirTitularesAndValidar(beanList, titularesDtoList, aloId, tmct0alc.getNucnfliq(), isosPaises,
                isosPaisesHacienda, nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed);

            if (StringUtils.isNotBlank(mensaje)) {
              nAfisFixed.addMsg(String.format("Orden '%s' %s", alcId.getNuorden(), mensaje));
              break;
            }

          }
        }
      }
      else if (ord.getCdclsmdo() == 'E') {
        mensaje = aniadirTitularesAndValidar(beanList, titularesDtoList, aloId, (short) 0, isosPaises,
            isosPaisesHacienda, nombresCiudadPorDistrito, listaFestivos, nAfisCreados, nAfisFixed);
        if (StringUtils.isNotBlank(mensaje)) {
          nAfisFixed.addMsg(String.format("Orden '%s' %s", alcId.getNuorden(), mensaje));
        }
      }
      else {
        nAfisFixed.addMsg(String.format("Tipo orden Nacional/Extranjero no coincide con los datos '%s'.",
            alcId.getNuorden()));

      }
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
      nAfisFixed.addMsg(String.format("INCIDENCIA-Aniandiendo titular a orden '%s'", alcId.getNuorden()));
      throw new SIBBACBusinessException(e);

    }
  }

  @Transactional
  public void modifyBean(PartenonRecordBean bean, TitularesDTO titular) {

    if (titular.getNbclient() != null) {
      bean.setNbclient(titular.getNbclient().trim());
    }

    if (titular.getNbclient1() != null) {
      bean.setNbclient1(titular.getNbclient1().trim());
    }

    if (titular.getNbclient2() != null) {
      bean.setNbclient2(titular.getNbclient2().trim());
    }

    if (titular.getNacionalidad() != null) {
      bean.setNacionalidad(titular.getNacionalidad().trim());
    }
    if (titular.getTipnactit() != null) {
      bean.setIndNac(titular.getTipnactit());
    }
    if (titular.getTipdoc() != null) {
      bean.setTipoDocumento(titular.getTipdoc());
    }
    if (titular.getPaisres() != null) {
      bean.setPaisResidencia(titular.getPaisres().trim());
    }

    if (titular.getTpdomici() != null) {
      bean.setTpdomici(titular.getTpdomici().trim());
    }
    if (titular.getNbdomici() != null) {
      bean.setNbdomici(titular.getNbdomici().trim());
    }
    if (titular.getNudomici() != null) {
      if (" ".equals(titular.getNudomici())) {
        bean.setNudomici(" ");
      }
      else {
        bean.setNudomici(titular.getNudomici().trim());
      }
    }
    if (titular.getProvincia() != null) {
      bean.setProvincia(titular.getProvincia().trim());
    }
    if (titular.getCodpostal() != null) {
      bean.setDistrito(titular.getCodpostal().trim());
    }
    if (titular.getPoblacion() != null) {
      bean.setPoblacion(titular.getPoblacion().trim());
    }
    if (titular.getTptiprep() != null) {
      bean.setTiptitu(titular.getTptiprep());
    }
    if (titular.getTipopers() != null) {
      bean.setTipoPersona(titular.getTipopers());
    }
    if (titular.getParticip() != null) {
      bean.setParticip(titular.getParticip());
    }
    if (titular.getCcv() != null) {
      bean.setCcv(titular.getCcv());
    }
    if (titular.getNudnicif() != null) {
      bean.setNifbic(titular.getNudnicif().trim());
    }

    // Se decidio que no se podria editar el nureford
    // if ( StringUtils.isNotEmpty( nureford ) ) {
    // bean.setNumOrden( nureford.trim() );
    // }
  }

  @Transactional
  public List<String> validateBean(PartenonRecordBean bean, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito) {
    // Validamos el bean para tener los errores
    List<String> errCodes = externalValidator.validate(bean);
    List<String> externalErr = externalValidator.validate(bean, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito);
    for (String err : externalErr) {
      if (!errCodes.contains(err)) {
        errCodes.add(err);
      }
    }

    externalErr = externalValidator.validatePaisesProvincias(bean, isosPaises, isosPaisesHacienda,
        nombresCiudadPorDistrito);
    for (String err : externalErr) {
      if (!errCodes.contains(err)) {
        errCodes.add(err);
      }
    }

    String erroresNumorden = null;
    List<Tmct0alc> alcs = new ArrayList<Tmct0alc>();
    List<Tmct0alo> alos = new ArrayList<Tmct0alo>();

    try {
      Tmct0ord ord = null;
      if (bean.getTmct0alo() != null) {
        ord = ordBo.findById(bean.getTmct0alo().getId().getNuorden());
        alos.add(tmct0aloBo.findById(bean.getTmct0alo().getId()));
      }
      else {
        alos.addAll(tmct0aloBo.findAllByRefCliente(bean.getNumOrden()));
        if (CollectionUtils.isNotEmpty(alos)) {
          ord = alos.get(0).getTmct0bok().getTmct0ord();
        }
        else {
          ord = ordBo.findFirstByNurefordOrderByNuorden(bean.getNumOrden());
        }
      }

      if (ord == null) {
        erroresNumorden = ValidationHelper.CodError.ORD.text;
      }
      else {
        if (!ord.getCdclsmdo().equals('N')) {
          if (alos.size() == 0) {
            erroresNumorden = ValidationHelper.CodError.ALOS.text;
          }
        }
        else {
          for (Tmct0alo alo : alos) {
            if (alo.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              alcs.addAll(alcBo.findByAlloIdActivos(alo.getId()));
            }
          }

          if (alcs.size() == 0) {
            erroresNumorden = ValidationHelper.CodError.NOALC.text;
          }

        }
      }
    }
    catch (IncorrectResultSizeDataAccessException e) {
      erroresNumorden = ValidationHelper.CodError.ORD.text;
    }
    if (erroresNumorden != null) {
      errCodes.add(erroresNumorden);
    }
    return errCodes;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public String comprobarErroresProcedencia(List<PartenonRecordBean> bloqueMismaOrden, List<ControlTitularesDTO> alcs,
      List<ControlTitularesDTO> alos) {
    
    if(bloqueMismaOrden!=null && !bloqueMismaOrden.isEmpty()){
      String nureford = bloqueMismaOrden.get(0).getNumOrden();
      String erroresNumorden = null;

      // Primero comprobamos si no hace falta nureford porque ya tiene los
      // datos de alo o alc
      if (bloqueMismaOrden.get(0).getTmct0alo() != null && bloqueMismaOrden.get(0).getNucnfliq() != null) {
        if (bloqueMismaOrden.get(0).getNucnfliq() != 0) {
          Tmct0aloId aloid = bloqueMismaOrden.get(0).getTmct0alo().getId();
          ControlTitularesDTO alcData = alcBo.findControlTitularesByAlcId(new Tmct0alcId(aloid.getNbooking(), aloid
              .getNuorden(), bloqueMismaOrden.get(0).getNucnfliq(), aloid.getNucnfclt()));
          if (alcData != null) {
            alcs.add(alcData);
            nureford = null;
          }
        }
        else {
          Tmct0alo alo = tmct0aloBo.findById(bloqueMismaOrden.get(0).getTmct0alo().getId());
          Tmct0ord ord = ordBo.findById(alo.getId().getNuorden());
          alos.add(new ControlTitularesDTO(ord.getCdclsmdo(), alo.getId(), alo.getFevalor(), alo.getReftitular(), alo
              .getCdestadotit(), ord.getCdclente(), ord.getIsscrdiv()));
          nureford = null;
        }
      }
      else {
        try {
          LOG.trace("[PartenonFileReader :: savePreviousBlock] Comprobando si ord es nacional o extranjero: {}", nureford);

          Tmct0ord ord;
          alos.addAll(tmct0aloBo.findControlTitularesByRefCliente(nureford));
          if (CollectionUtils.isNotEmpty(alos)) {
            ord = ordBo.findById(alos.get(0).getAlloId().getNuorden());
          }
          else {
            ord = ordBo.findFirstByNurefordOrderByNuorden(nureford);
          }
          if (ord == null) {
            erroresNumorden = ValidationHelper.CodError.ORD.text;
          }
          else {
            if (ord.getCdclsmdo() != 'N') {
              LOG.trace("[PartenonFileReader :: savePreviousBlock] Comprobando alos para nureford extranjero: {}",
                  nureford);
              if (CollectionUtils.isEmpty(alos)) {
                alos.addAll(tmct0aloBo.findControlTitularesByNuorden(ord.getNuorden()));
              }
              LOG.trace("[PartenonFileReader :: savePreviousBlock] Obtenidos {} alos para nureford: {}", alos.size(),
                  nureford);

              if (CollectionUtils.isEmpty(alos)) {
                erroresNumorden = ValidationHelper.CodError.ALOS.text;
              }
            }
            else {
              LOG.trace("[PartenonFileReader :: savePreviousBlock] Comprobando alcs para nureford nacional: {}", nureford);
              if (CollectionUtils.isEmpty(alos)) {
                alcs.addAll(alcBo.findControlTitularesByNuordenAndCdestadotitLt(ord.getNuorden(),
                    EstadosEnumerados.TITULARIDADES.CIERRE_IF.getId()));
              }
              else {
                for (ControlTitularesDTO controlTitularesDTO : alos) {
                  alcs.addAll(alcBo.findControlTitularesByAlloId(controlTitularesDTO.getAlloId()));
                }

              }
              LOG.trace("[PartenonFileReader :: savePreviousBlock] Obtenidos {} alcs para nureford: {}", alcs.size(),
                  nureford);

              if (CollectionUtils.isEmpty(alcs)) {
                erroresNumorden = ValidationHelper.CodError.NOALC.text;
              }
            }
          }
        }
        catch (IncorrectResultSizeDataAccessException e) {
          erroresNumorden = ValidationHelper.CodError.ORD.text;
        }
      }
      return erroresNumorden;  
    }
    else{
      return null;
    }
    
  }

  @Transactional
  private int aniadirAfisToAfiError(Tmct0aloId aloId, Short nucnfliq, List<Tmct0afiId> listAfiIdModificados,
      Map<String, Tmct0paises> isosPaises, Map<String, Tmct0paises> isosPaisesHacienda,
      Map<String, List<String>> nombresCiudadPorDistrito) {
    int aniadidos = 0;
    List<Tmct0afi> afis;
    List<Tmct0AfiError> afiErr;
    boolean encontrado;
    List<String> errorCodes;
    if (nucnfliq == null || nucnfliq == 0) {
      LOG.debug("[aniadirAfisToAfiError] buscando afis y afi error sin nucnfliq");
      afis = findActivosByAlloId(aloId);
      afiErr = afiErrorBo.findGroupByAloId(aloId);
    }
    else {
      LOG.debug("[aniadirAfisToAfiError] buscando afis y afi error con nucnfliq");
      afis = findActivosByAlcData(aloId.getNuorden(), aloId.getNbooking(), aloId.getNucnfclt(), nucnfliq);
      afiErr = afiErrorBo.findGroupByAlcId(new Tmct0alcId(aloId, nucnfliq));
    }

    for (Tmct0afi tmct0afi : afis) {
      if (CollectionUtils.isEmpty(listAfiIdModificados) || !listAfiIdModificados.contains(tmct0afi.getId())) {
        encontrado = false;
        for (Tmct0AfiError tmct0AfiError : afiErr) {

          if (tmct0AfiError.getNudnicif() != null && tmct0AfiError.getTptiprep() != null
              && tmct0afi.getCdholder().trim().equals(tmct0AfiError.getNudnicif().trim())
              && tmct0AfiError.getTptiprep() == tmct0afi.getTptiprep()) {
            encontrado = true;
            break;
          }
        }
        if (!encontrado) {
          PartenonRecordBean beanNew = new PartenonRecordBean(tmct0afi);
          LOG.debug("[aniadirAfisToAfiError] Aniadimos titular to afierror from afi: {}", beanNew);
          Tmct0AfiError afiError = beanNew.toTmct0afierror(getNewTmct0afiSequence() + "");
          errorCodes = validateBean(beanNew, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
          afiErrorBo.findAndSetErrors(afiError, errorCodes, null);
          afiError.setTmct0alo(new Tmct0alo());
          afiError.getTmct0alo().setId(aloId);
          afiError.setNucnfliq(nucnfliq);
          afiErrorBo.save(afiError);
          aniadidos++;
        }
      }
    }

    return aniadidos;
  }

  @Transactional
  private String aniadirTitularesAndValidar(List<PartenonRecordBean> beanList, List<TitularesDTO> titularesDtoList,
      Tmct0aloId aloId, Short nucnfliq, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaisesHacienda, Map<String, List<String>> nombresCiudadPorDistrito,
      List<Date> listaFestivos, Results nAfisCreados, Results nAfisFixed) {
    String message = "";
    int i = 0;
    Tmct0AfiError afiError;
    if (nucnfliq != null && nucnfliq > 0) {
      message = comprobacionesALC(alcBo.findById(new Tmct0alcId(aloId, nucnfliq)), listaFestivos);
    }

    if (StringUtils.isBlank(message)) {
      afiErrorBo.delete(afiErrorBo.findGroupByAlcId(new Tmct0alcId(aloId, nucnfliq)));
      for (PartenonRecordBean bean : beanList) {
        afiError = bean.toTmct0afierror(getNewTmct0afiSequence() + "");
        afiError.setTmct0alo(new Tmct0alo());
        afiError.getTmct0alo().setId(aloId);
        afiError.setNucnfliq(nucnfliq);
        modifyAfiError(afiError, titularesDtoList.get(i++), isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito);
        nAfisCreados.addNAfiErrorSynchronized();
      }

      int aniadidos = this.aniadirAfisToAfiError(aloId, nucnfliq, null, isosPaises, isosPaisesHacienda,
          nombresCiudadPorDistrito);
      nAfisCreados.addNAfiErrorSynchronized(aniadidos);
      if (validacionesBloque(aloId, nucnfliq, isosPaises, isosPaisesHacienda, nombresCiudadPorDistrito)) {
        nAfisFixed.addNAfiErrorSynchronized(beanList.size() + aniadidos);
      }
    }
    return message;
  }

  /**
   * Método que comprueba algunos estados del ALC para la creación, borrado o
   * modificación de titulares tocando la CCV
   * 
   * @param alc
   * @param listaFestivos
   * @return
   */
  @Transactional
  public String comprobacionesALC(Tmct0alc alc, List<Date> listaFestivos) {
    if (getEstadosAsignacionNoPermitidos().contains(alc.getCdestadoasig())) {
      return MSJ_BLOQUEANTE_1;
    }

    if (alc.getEstadotit() != null && getEstadosTitularNoPermitidos().contains(alc.getEstadotit().getIdestado())) {
      return MSJ_BLOQUEANTE_2;
    }

    if (alc.getEstadoentrec() != null
        && EstadosEnumerados.CLEARING.PDTE_ENVIAR_ANULACION.getId() < alc.getEstadoentrec().getIdestado()) {
      return MSJ_NO_BLOQUEANTE_3;
    }

    if (alc.getEstadotit() != null
        && alc.getEstadotit().getIdestado() >= EstadosEnumerados.TITULARIDADES.TITULARIDAD_ACEPTADA.getId()
        && new Date().after(alcBo.addNDays(alc.getFeopeliq(), listaFestivos, 7))) {
      return MSJ_NO_BLOQUEANTE_4;
    }

    return "";
  }

  /**
   * Devuelve los estados asignación no permitidos para que se pueda controlar
   * si se realiza una creación, borrado o modificación de titulares tocando la
   * CCV
   * 
   * @return
   */
  private List<Integer> getEstadosAsignacionNoPermitidos() {
    List<Integer> comprobacionAsignacion = new ArrayList<Integer>();
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId());
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_SIN_TITULAR.getId());
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE.getId());
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_S3.getId());
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_TRASPASO.getId());
    comprobacionAsignacion.add(EstadosEnumerados.ASIGNACIONES.ENVIANDOSE_ANULACION.getId());
    return comprobacionAsignacion;
  }

  /**
   * Devuelve los estados titular no permitidos para que se pueda controlar si
   * se realiza una creación, borrado o modificación de titulares tocando la CCV
   * 
   * @return
   */
  private List<Integer> getEstadosTitularNoPermitidos() {
    List<Integer> comprobacionTitulares = new ArrayList<Integer>();
    comprobacionTitulares.add(EstadosEnumerados.TITULARIDADES.TRATANDOSE.getId());
    comprobacionTitulares.add(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
    comprobacionTitulares.add(EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());
    comprobacionTitulares.add(EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId());
    return comprobacionTitulares;
  }

  /**
   * Esta clase muestra informacion sobre cuantos campos se van a modificar en
   * la modificacion masiva y cual es el nombre del primero.
   *
   */
  public static class ModifiedFields {

    public String firstName;
    public int modifiedFields;

    public ModifiedFields() {
      firstName = "";
      modifiedFields = 0;
    }
  }

}
