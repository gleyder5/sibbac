package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.Tmct0caseoperacionejeData;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0caseoperacionejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;

/**
 * @author xIS16630
 *
 */
@Repository
public interface Tmct0caseoperacionejeDao extends JpaRepository<Tmct0caseoperacioneje, Long> {

  /**
   * metodo que filtra por operacion, isin y sentido
   * 
   * @param cdoperacionecc
   * @param isin
   * @param sentido
   * @return Tmct0caseoperacioneje
   */
  @Query("SELECT co "
         + " FROM Tmct0caseoperacioneje co WHERE co.cdoperacionecc = :cdoperacionecc  AND  co.cdisin = :isin  AND co.cdsentido= :sentido AND co.cdcamara=:camara ")
  Tmct0caseoperacioneje findByFiltro(@Param("cdoperacionecc") String cdoperacionecc,
                                     @Param("isin") String isin,
                                     @Param("sentido") char sentido,
                                     @Param("camara") String camara);

  /**
   * metodo que filtra por operacion e isin con un in
   * 
   * @param cdoperacionecc
   * @param isin
   * @param sentido
   * @return Tmct0caseoperacioneje
   */
  @Query("SELECT co FROM Tmct0caseoperacioneje co"
         + " WHERE co.cdoperacionecc IN :cdoperacionecc  AND  co.cdisin IN :isin ")
  List<Tmct0caseoperacioneje> findOperacionesWithIns(@Param("cdoperacionecc") List<String> cdoperacionecc,
                                                     @Param("isin") List<String> isin);

  /**
   * metodo que filtra por operacion e isin con un in
   * 
   * @param cdoperacionecc
   * @param isin
   * @param sentido
   * @return Tmct0caseoperacioneje
   */
  @Query("SELECT new sibbac.business.wrappers.database.data.Tmct0caseoperacionejeData( co.cdoperacionecc, co.nutitpendcase, co.cdsentido, co.cdisin, co.idcase, co.nutitulostotal ) "
         + "FROM Tmct0caseoperacioneje co join co.tmct0operacioncdecc o "
         + "WHERE o.cdoperacionecc IN (:cdoperacionecc)  AND  o.cdisin IN (:isin) ")
  List<Tmct0caseoperacionejeData> findOperacionesDataWithIns(@Param("cdoperacionecc") List<String> cdoperacionecc,
                                                             @Param("isin") List<String> isin);

  /**
   * Metodo para obtener el idcase y los datos de mercado dada una ejecucion
   * 
   * @param nuordenmkt
   * @param nuexemkt
   * @param fcontratacion
   * @param cdoperacionmkt
   * @param cdisin
   * @param cdsegmento
   * @param cdsentido
   * @param cdmiembromkt
   * @return
   */
  @Query("SELECT new sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0caseoperacionejeInformacionMercadoCaseDTO(c.idcase, o.idoperacioncdecc, o.cdoperacionecc, o.cdisin,"
         + " o.cdsentido, o.fcontratacion, o.fordenmkt, o.hordenmkt, o.cdPlataformaNegociacion, o.cdindcotizacion, o.cdmiembromkt, o.cdsegmento, o.cdoperacionmkt, "
         + " o.nuordenmkt, o.nuexemkt, c.nutitulostotal, c.nutitpendcase)  "
         + " from Tmct0caseoperacioneje c, Tmct0operacioncdecc o"
         + " where c.tmct0operacioncdecc.idoperacioncdecc=o.idoperacioncdecc and o.nuordenmkt=:nuordenmkt and o.nuexemkt=:nuexemkt and "
         + "  o.fcontratacion=:fcontratacion and o.cdoperacionmkt=:cdoperacionmkt and o.cdisin=:cdisin and o.cdsegmento=:cdsegmento"
         + " and o.cdsentido=:cdsentido and o.cdmiembromkt=:cdmiembromkt ")
  Tmct0caseoperacionejeInformacionMercadoCaseDTO findCaseoperacionEjeInformacionMercado(@Param("nuordenmkt") String nuordenmkt,
                                                                                        @Param("nuexemkt") String nuexemkt,
                                                                                        @Param("fcontratacion") Date fcontratacion,
                                                                                        @Param("cdoperacionmkt") String cdoperacionmkt,
                                                                                        @Param("cdisin") String cdisin,
                                                                                        @Param("cdsegmento") String cdsegmento,
                                                                                        @Param("cdsentido") Character cdsentido,
                                                                                        @Param("cdmiembromkt") String cdmiembromkt);

  /**
   * Metodo para obtener el idcase y los datos de mercado dada una ejecucion
   * 
   * @param nuordenmkt
   * @param nuexemkt
   * @param fcontratacion
   * @param cdoperacionmkt
   * @param cdisin
   * @param cdsegmento
   * @param cdsentido
   * @param cdmiembromkt
   * @return
   */
  @Query("SELECT c from Tmct0caseoperacioneje c, Tmct0operacioncdecc o"
         + " where c.tmct0operacioncdecc.idoperacioncdecc=o.idoperacioncdecc and o.nuordenmkt=:nuordenmkt and o.nuexemkt=:nuexemkt and "
         + "  o.fcontratacion=:fcontratacion and o.cdoperacionmkt=:cdoperacionmkt and o.cdisin=:cdisin and o.cdsegmento=:cdsegmento"
         + " and o.cdsentido=:cdsentido and o.cdmiembromkt=:cdmiembromkt ")
  Tmct0caseoperacioneje findTmct0caseoperacionejeByInformacionMercado(@Param("nuordenmkt") String nuordenmkt,
                                                                      @Param("nuexemkt") String nuexemkt,
                                                                      @Param("fcontratacion") Date fcontratacion,
                                                                      @Param("cdoperacionmkt") String cdoperacionmkt,
                                                                      @Param("cdisin") String cdisin,
                                                                      @Param("cdsegmento") String cdsegmento,
                                                                      @Param("cdsentido") Character cdsentido,
                                                                      @Param("cdmiembromkt") String cdmiembromkt);

  /**
   * Actualiza el numero de titulos pendientes de case
   * 
   * @param idcase
   * @param nutitPendCase
   * @param fAudit
   */
  @Modifying
  @Query(value = "UPDATE TMCT0CASEOPERACIONEJE C SET C.NUTITPENDCASE = :nuTitPendCase, C.AUDIT_FECHA_CAMBIO = :fAudit WHERE C.IDCASE = :idcase",
         nativeQuery = true)
  public Integer updateTitulosPendientesCase(@Param("idcase") Long idcase,
                                             @Param("nuTitPendCase") BigDecimal nutitPendCase,
                                             @Param("fAudit") Date fAudit);

} // Tmct0caseoperacionejeDao
