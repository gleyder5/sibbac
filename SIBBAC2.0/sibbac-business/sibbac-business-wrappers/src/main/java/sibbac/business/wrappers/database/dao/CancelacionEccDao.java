package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.CancelacionEcc;

@Component
public interface CancelacionEccDao extends JpaRepository<CancelacionEcc, Long> {

  List<CancelacionEcc> findAllByCdrefmovimiento(String cdrefmovimiento);

  List<CancelacionEcc> findAllByCdrefmovimientoecc(String cdrefmovimientoecc);

}
