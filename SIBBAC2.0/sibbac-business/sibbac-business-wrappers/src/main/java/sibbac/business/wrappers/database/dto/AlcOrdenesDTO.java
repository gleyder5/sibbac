package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;


public class AlcOrdenesDTO {

	private Long		id;
	private String		nBooking;
	private String		nOrden;
	private String		nUcnfclt;
	private String		cdrefban;
	private BigDecimal	imnetliq;
	private BigDecimal	imtotnet;
	private BigDecimal	imtotbru;
	private BigDecimal	imcomisn;
	private String		cdordtit;

	public AlcOrdenesDTO() {

	}

	public AlcOrdenesDTO( Long id, String nBooking, String nOrden, String nUcnfclt, String cdrefban, BigDecimal imnetliq,
			BigDecimal imtotnet, BigDecimal imtotbru, BigDecimal imcomisn, String cdordtit ) {

		this.id = id;
		this.nBooking = nBooking;
		this.nOrden = nOrden;
		this.nUcnfclt = nUcnfclt;
		this.cdrefban = cdrefban;
		this.imnetliq = imnetliq;
		this.imtotnet = imtotnet;
		this.imtotbru = imtotbru;
		this.imcomisn = imcomisn;
		this.cdordtit = cdordtit;
	}

	public Long getId() {
		return id;
	}

	public String getnBooking() {
		return nBooking;
	}

	public String getnOrden() {
		return nOrden;
	}

	public String getnUcnfclt() {
		return nUcnfclt;
	}

	public String getCdrefban() {
		return cdrefban;
	}

	public BigDecimal getImnetliq() {
		return imnetliq;
	}

	public BigDecimal getImtotnet() {
		return imtotnet;
	}

	public BigDecimal getImtotbru() {
		return imtotbru;
	}

	public BigDecimal getImcomisn() {
		return imcomisn;
	}

	public String getCdordtit() {
		return cdordtit;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setnBooking( String nBooking ) {
		this.nBooking = nBooking;
	}

	public void setnOrden( String nOrden ) {
		this.nOrden = nOrden;
	}

	public void setnUcnfclt( String nUcnfclt ) {
		this.nUcnfclt = nUcnfclt;
	}

	public void setCdrefban( String cdrefban ) {
		this.cdrefban = cdrefban;
	}

	public void setImnetliq( BigDecimal imnetliq ) {
		this.imnetliq = imnetliq;
	}

	public void setImtotnet( BigDecimal imtotnet ) {
		this.imtotnet = imtotnet;
	}

	public void setImtotbru( BigDecimal imtotbru ) {
		this.imtotbru = imtotbru;
	}

	public void setImcomisn( BigDecimal imcomisn ) {
		this.imcomisn = imcomisn;
	}

	public void setCdordtit( String cdordtit ) {
		this.cdordtit = cdordtit;
	}

}
