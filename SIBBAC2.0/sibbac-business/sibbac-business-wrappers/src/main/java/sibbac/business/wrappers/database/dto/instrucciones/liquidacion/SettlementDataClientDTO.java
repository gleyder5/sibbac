package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.common.helper.CloneObjectHelper;

public class SettlementDataClientDTO implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 7912700476204319864L;

  public SettlementDataClientDTO() {
    initData();
  }

  public SettlementDataClientDTO(Tmct0cli cli) {
    this();
    CloneObjectHelper.copyBasicFields(cli, this, null);
  }

  public SettlementDataClientDTO(SettlementDataClientDTO cli) {
    this();
    CloneObjectHelper.copyBasicFields(cli, this, null);
  }

  /**
   * Table Field CDBROCLI. CLIENT Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdbrocli;

  /**
   * Table Field NUMSEC. SECUENCIA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal numsec;
  /**
   * Table Field DESCLI. CLIENT DESCRIPTION Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String descli;
  /**
   * Table Field TPIDENTI. TYPE IDENTITY Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tpidenti;
  /**
   * Table Field CIF. CIF Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cif;
  /**
   * Table Field CDBDP. CODIGO PERSONA BDP Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdbdp;
  /**
   * Table Field TPRESIDE. Residence Type Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tpreside;
  /**
   * Table Field CDREFBNK. Bank Reference Partenon Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdrefbnk;
  /**
   * Table Field TPCLIENN. Client Nature (I/R) Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tpclienn;
  /**
   * Table Field CDKGR. KGR CODE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdkgr;
  /**
   * Table Field CDCAT. CATEGORY Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdcat;
  /**
   * Table Field FIDESSA. FIDESSA STATUS I/D/U Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String fidessa;
  /**
   * Table Field FHSENDFI. DATE SEND FIDESSA Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhsendfi;
  /**
   * Table Field CDUSUAUD. AUDIT USER Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdusuaud;
  /**
   * Table Field FHAUDIT. AUDIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.sql.Timestamp fhaudit;
  /**
   * Table Field FHINICIO. INIT DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhinicio;
  /**
   * Table Field FHFINAL. FINAL DATE Documentación columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhfinal;

  /**
   * @return the cdbrocli
   */
  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  /**
   * @param cdbrocli the cdbrocli to set
   */
  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  /**
   * @return the numsec
   */
  public java.math.BigDecimal getNumsec() {
    return numsec;
  }

  /**
   * @param numsec the numsec to set
   */
  public void setNumsec(java.math.BigDecimal numsec) {
    this.numsec = numsec;
  }

  /**
   * @return the descli
   */
  public java.lang.String getDescli() {
    return descli;
  }

  /**
   * @param descli the descli to set
   */
  public void setDescli(java.lang.String descli) {
    this.descli = descli;
  }

  /**
   * @return the tpidenti
   */
  public java.lang.String getTpidenti() {
    return tpidenti;
  }

  /**
   * @param tpidenti the tpidenti to set
   */
  public void setTpidenti(java.lang.String tpidenti) {
    this.tpidenti = tpidenti;
  }

  /**
   * @return the cif
   */
  public java.lang.String getCif() {
    return cif;
  }

  /**
   * @param cif the cif to set
   */
  public void setCif(java.lang.String cif) {
    this.cif = cif;
  }

  /**
   * @return the cdbdp
   */
  public java.lang.String getCdbdp() {
    return cdbdp;
  }

  /**
   * @param cdbdp the cdbdp to set
   */
  public void setCdbdp(java.lang.String cdbdp) {
    this.cdbdp = cdbdp;
  }

  /**
   * @return the tpreside
   */
  public java.lang.String getTpreside() {
    return tpreside;
  }

  /**
   * @param tpreside the tpreside to set
   */
  public void setTpreside(java.lang.String tpreside) {
    this.tpreside = tpreside;
  }

  /**
   * @return the cdrefbnk
   */
  public java.lang.String getCdrefbnk() {
    return cdrefbnk;
  }

  /**
   * @param cdrefbnk the cdrefbnk to set
   */
  public void setCdrefbnk(java.lang.String cdrefbnk) {
    this.cdrefbnk = cdrefbnk;
  }

  /**
   * @return the tpclienn
   */
  public java.lang.String getTpclienn() {
    return tpclienn;
  }

  /**
   * @param tpclienn the tpclienn to set
   */
  public void setTpclienn(java.lang.String tpclienn) {
    this.tpclienn = tpclienn;
  }

  /**
   * @return the cdkgr
   */
  public java.lang.String getCdkgr() {
    return cdkgr;
  }

  /**
   * @param cdkgr the cdkgr to set
   */
  public void setCdkgr(java.lang.String cdkgr) {
    this.cdkgr = cdkgr;
  }

  /**
   * @return the cdcat
   */
  public java.lang.String getCdcat() {
    return cdcat;
  }

  /**
   * @param cdcat the cdcat to set
   */
  public void setCdcat(java.lang.String cdcat) {
    this.cdcat = cdcat;
  }

  /**
   * @return the fidessa
   */
  public java.lang.String getFidessa() {
    return fidessa;
  }

  /**
   * @param fidessa the fidessa to set
   */
  public void setFidessa(java.lang.String fidessa) {
    this.fidessa = fidessa;
  }

  /**
   * @return the fhsendfi
   */
  public java.math.BigDecimal getFhsendfi() {
    return fhsendfi;
  }

  /**
   * @param fhsendfi the fhsendfi to set
   */
  public void setFhsendfi(java.math.BigDecimal fhsendfi) {
    this.fhsendfi = fhsendfi;
  }

  /**
   * @return the cdusuaud
   */
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * @param cdusuaud the cdusuaud to set
   */
  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  /**
   * @return the fhaudit
   */
  public java.sql.Timestamp getFhaudit() {
    return fhaudit;
  }

  /**
   * @param fhaudit the fhaudit to set
   */
  public void setFhaudit(java.sql.Timestamp fhaudit) {
    this.fhaudit = fhaudit;
  }

  /**
   * @return the fhinicio
   */
  public java.math.BigDecimal getFhinicio() {
    return fhinicio;
  }

  /**
   * @param fhinicio the fhinicio to set
   */
  public void setFhinicio(java.math.BigDecimal fhinicio) {
    this.fhinicio = fhinicio;
  }

  /**
   * @return the fhfinal
   */
  public java.math.BigDecimal getFhfinal() {
    return fhfinal;
  }

  /**
   * @param fhfinal the fhfinal to set
   */
  public void setFhfinal(java.math.BigDecimal fhfinal) {
    this.fhfinal = fhfinal;
  }

  public void initData() {
    setCdbrocli("");
    setCdrefbnk("");
    setCdusuaud("");
    setDescli("");
    setTpclienn("");
    setTpreside("");
    setCdbdp("");
    setCdcat("");
    setCdkgr("");
    setCif("");
    setTpclienn("");
    setTpidenti("");
    setFidessa("");
    setFhsendfi(BigDecimal.ZERO);
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SettlementDataClientDTO [cdbrocli=" + cdbrocli + ", numsec=" + numsec + ", descli=" + descli
           + ", tpidenti=" + tpidenti + ", cif=" + cif + ", cdbdp=" + cdbdp + ", tpreside=" + tpreside + ", cdrefbnk="
           + cdrefbnk + ", tpclienn=" + tpclienn + ", cdkgr=" + cdkgr + ", cdcat=" + cdcat + ", fidessa=" + fidessa
           + ", fhsendfi=" + fhsendfi + ", cdusuaud=" + cdusuaud + ", fhaudit=" + fhaudit + ", fhinicio=" + fhinicio
           + ", fhfinal=" + fhfinal + "]";
  }

}
