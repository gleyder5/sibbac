package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;

@Repository
public interface FacturaDao extends JpaRepository<Factura, Long>, JpaSpecificationExecutor<Factura> {

  public List<Factura> findByEstado(final Tmct0estado estado);

  public List<Factura> findByEstadoOrEstadoAndFacturaSugerida_Alias(Tmct0estado estadoPendiente, Tmct0estado estadoPteParcial, Alias alias);

  public List<Factura> findAllByOrderByFechaCreacionAsc(final Long aliasId,
                                                        final Date fechaDesde,
                                                        final Date fechaHasta,
                                                        final List<Tmct0estado> estados);

  public List<Factura> findAllByEstadoIn(@Param("estados") final List<Tmct0estado> estados);

  public List<Factura> findByEstadocont(Tmct0estado estadoCont);

  @Query("SELECT f.id FROM Factura f WHERE f.estadocont.id = :estadoCont")
  public List<Long> findIdByEstadocont(@Param("estadoCont") Integer estadoCont);

  public List<Factura> findByEstadoAndEstadocont(Tmct0estado estado, Tmct0estado estadoCont);

  public List<Factura> findFirst5ByEstadocont(Tmct0estado estadoCont);

  @Query("SELECT f FROM Factura f WHERE (f.enviado is null or f.enviado = false)")
  public List<Factura> findNoEnviadas();

  @Query("SELECT f FROM Factura f WHERE f.estado.id not in (:estado) and (f.enviado is null or f.enviado = false) and f.fechaCreacion >= :fechaDesde and f.fechaCreacion <= :fechaHasta")
  public List<Factura> findNoEnviadasByIdEstadoDistinctBetweenFechas(@Param("estado") List<Integer> estado, @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);
  
  @Modifying @Query("UPDATE Factura SET enviado = true WHERE id = :id")
  public int marcaEnviado(@Param("id") long id);
 
}
