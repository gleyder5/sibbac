package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 *	DTO que contiene informacion del bundle sibbac.partenon.properties 
 *	para procesamiento de la query principalmente. 
 */
public class DatosForQueryPartenonDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7214590721593849736L;
	
	private Integer diasAnticipoSituacion;
	private Date diaInicioAnticipoSituacion;
	private Date fechaEjecucion;
	private String cdrefasignacion;
	private String cdmiembrocompensador;
	private Integer cdestadoasig;
	private Integer cdestadotit;
	private String cdPlataformaNegociacion;
	
	/**
	 *	Constructor no-arg. 
	 */
	public DatosForQueryPartenonDTO() {
		super();
	}

	/**
	 * 	Constructor args.
	 * 
	 * 	@param diasAnticipoSituacion
	 * 	@param diaInicioAnticipoSituacion
	 * 	@param fechaEjecucion
	 * 	@param cdrefasignacion
	 * 	@param cdmiembrocompensador
	 *  @param cdestadoasig
	 *  @param cdestadotit
	 *  @param cdPlataformaNegociacion
	 */
	public DatosForQueryPartenonDTO(Integer diasAnticipoSituacion,
			Date diaInicioAnticipoSituacion, Date fechaEjecucion,
			String cdrefasignacion, String cdmiembrocompensador,
			Integer cdestadoasig, Integer cdestadotit,
			String cdPlataformaNegociacion) {
		super();
		this.diasAnticipoSituacion = diasAnticipoSituacion;
		this.diaInicioAnticipoSituacion = diaInicioAnticipoSituacion;
		this.fechaEjecucion = fechaEjecucion;
		this.cdrefasignacion = cdrefasignacion;
		this.cdmiembrocompensador = cdmiembrocompensador;
		this.cdestadoasig = cdestadoasig;
		this.cdestadotit = cdestadotit;
		this.cdPlataformaNegociacion = cdPlataformaNegociacion;
	}

	/**
	 *	@return Integer  
	 */
	public Integer getDiasAnticipoSituacion() {
		return this.diasAnticipoSituacion;
	}

	/**
	 *	@param diasAnticipoSituacion  
	 */
	public void setDiasAnticipoSituacion(Integer diasAnticipoSituacion) {
		this.diasAnticipoSituacion = diasAnticipoSituacion;
	}

	/**
	 *	@return Date  
	 */
	public Date getDiaInicioAnticipoSituacion() {
		return this.diaInicioAnticipoSituacion;
	}

	/**
	 *	@param diaInicioAnticipoSituacion  
	 */
	public void setDiaInicioAnticipoSituacion(Date diaInicioAnticipoSituacion) {
		this.diaInicioAnticipoSituacion = diaInicioAnticipoSituacion;
	}

	/**
	 *	@return Date  
	 */
	public Date getFechaEjecucion() {
		return this.fechaEjecucion;
	}

	/**
	 *	@param fechaEjecucion  
	 */
	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	/**
	 *	@return String  
	 */
	public String getCdrefasignacion() {
		return this.cdrefasignacion;
	}

	/**
	 *	@param cdrefasignacion  
	 */
	public void setCdrefasignacion(String cdrefasignacion) {
		this.cdrefasignacion = cdrefasignacion;
	}

	/**
	 *	@return String  
	 */
	public String getCdmiembrocompensador() {
		return this.cdmiembrocompensador;
	}

	/**
	 *	@param cdmiembrocompensador  
	 */
	public void setCdmiembrocompensador(String cdmiembrocompensador) {
		this.cdmiembrocompensador = cdmiembrocompensador;
	}

	/**
	 *	@return Integer  
	 */
	public Integer getCdestadoasig() {
		return this.cdestadoasig;
	}

	/**
	 *	@param cdestadoasig  
	 */
	public void setCdestadoasig(Integer cdestadoasig) {
		this.cdestadoasig = cdestadoasig;
	}

	/**
	 *	@return Integer  
	 */
	public Integer getCdestadotit() {
		return this.cdestadotit;
	}

	/**
	 *	@param cdestadotit  
	 */
	public void setCdestadotit(Integer cdestadotit) {
		this.cdestadotit = cdestadotit;
	}

	/**
	 *	@return String  
	 */
	public String getCdPlataformaNegociacion() {
		return this.cdPlataformaNegociacion;
	}

	/**
	 *	@param cdPlataformaNegociacion  
	 */
	public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
		this.cdPlataformaNegociacion = cdPlataformaNegociacion;
	}
	
	/**
	 * 	Obtiene lista de caracteres dividiendo por separador ",".
	 * 	@return List<String> 
	 */
	public List<String> getCdrefasignacionList() {
		if (StringUtils.isNotBlank(this.cdrefasignacion)) {
			return new ArrayList<String>(Arrays.asList(StringUtils.split(this.cdrefasignacion, ",")));
		}
		return null;
	}
	
	/**
	 * 	Obtiene lista de caracteres dividiendo por separador ",".
	 * 	@return List<String> 
	 */
	public List<String> getCdmiembrocompensadorList() {
		if (StringUtils.isNotBlank(this.cdmiembrocompensador)) {
			return new ArrayList<String>(Arrays.asList(StringUtils.split(this.cdmiembrocompensador, ",")));
		}
		return null;
	}
	
	/**
	 * 	Obtiene lista de caracteres dividiendo por separador ",".
	 * 	@return List<String> 
	 */
	public List<String> getCdPlataformaNegociacionList() {
		if (StringUtils.isNotBlank(this.cdPlataformaNegociacion)) {
			return new ArrayList<String>(Arrays.asList(StringUtils.split(this.cdPlataformaNegociacion, ",")));
		}
		return null;
	}
	
}