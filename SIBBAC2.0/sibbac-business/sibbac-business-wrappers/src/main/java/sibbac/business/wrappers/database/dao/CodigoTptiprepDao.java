package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.CodigoTptiprep;


@Repository
public interface CodigoTptiprepDao extends JpaRepository< CodigoTptiprep, String > {

	public CodigoTptiprep findByCodigo( String codigo );

}
