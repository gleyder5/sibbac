package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

public enum IndicadorInicioFinalHBB {
  INICIO("I"), FINAL("F");
  private final String text;

  /**
   * @param text
   */
  private IndicadorInicioFinalHBB(final String text) {
    this.text = text;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString() {
    return text;
  }

  public static String[] getFieldNames() {
    IndicadorInicioFinalHBB[] fields = IndicadorInicioFinalHBB.values();
    final String[] namesArray = new String[fields.length];
    for (int i = 0; i < fields.length; i++) {
      namesArray[i] = fields[i].text;
    }
    return namesArray;
  }

  public static IndicadorInicioFinalHBB getIndicador(String letter) {
    IndicadorInicioFinalHBB[] fields = IndicadorInicioFinalHBB.values();
    IndicadorInicioFinalHBB personType = null;
    for (int i = 0; i < fields.length; i++) {
      if (fields[i].text.equals(letter)) {
        personType = fields[i];
        break;
      }
    }
    return personType;
  }
}