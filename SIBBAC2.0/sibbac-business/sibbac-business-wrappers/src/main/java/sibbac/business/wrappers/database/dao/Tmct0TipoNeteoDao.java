package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.TipoNeteoData;
import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;


@Repository
public interface Tmct0TipoNeteoDao extends JpaRepository< Tmct0TipoNeteo, Long > {

	@Query( "SELECT new sibbac.business.wrappers.database.data.TipoNeteoData(tp) FROM Tmct0TipoNeteo tp" )
	List< TipoNeteoData > findAllData();
}
