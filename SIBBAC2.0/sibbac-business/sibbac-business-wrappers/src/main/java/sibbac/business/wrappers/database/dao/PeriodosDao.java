package sibbac.business.wrappers.database.dao;


import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Periodos;


@Component
public interface PeriodosDao extends JpaRepository< Periodos, Long > {

	Long countByFechaInicioAndFechaFinal( Date altaFechaInicio, Date altaFechaFinal );

	public Periodos findByIdentificador( Long identificador );

	/**
	 * 
	 * Recupera el periodo activo
	 * 
	 * @param fechaInicio
	 * @param fechaFinal
	 * @return
	 */
	public Periodos findFirstByFechaInicioBeforeAndFechaFinalAfter( Date fechaInicio, Date fechaFinal );
}
