package sibbac.business.wrappers.tasks;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.AliasSubcuentaBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasSubcuenta;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_WRAPPERS.NAME, name = Task.GROUP_WRAPPERS.JOB_SINCRONIZACION_ALIAS, interval = 6, intervalUnit = IntervalUnit.HOUR)
public class TaskSincronizacionAlias extends WrapperTaskConcurrencyPrevent {

    private static final String CONTINUE_EXECUTING = "continueExecuting";
    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskSincronizacionAlias.class);
    private static final String TAG = TaskSincronizacionAlias.class.getName();
    @Autowired
    private AliasBo aliasBo;

    @Autowired
    private AliasSubcuentaBo aliasSubucuentaBo;

    public TaskSincronizacionAlias() {
    }

    @Override
    public void executeTask() throws Exception {

	final String prefix = "[TaskSincronizacionAlias]";
	LOG.debug(prefix + "[Started...]");
	LOG.trace(prefix + "[Actualizando alias...]");
	continueExecuting();
	LOG.debug(prefix + "[Finished...]");
    }

    private void continueExecuting(){
	final List<Alias> alias = aliasBo.sincronizarAlias();
	if (CollectionUtils.isNotEmpty(alias)) {
	    LOG.trace("{}::{} [Gerenados " + alias.size() + " alias", TAG, CONTINUE_EXECUTING);
	} else {
	    LOG.debug("{}::{} [No se han generado alias", TAG, CONTINUE_EXECUTING);
	}
	LOG.trace("{}::{} [Actualizando aliasSubcuenta...]", TAG, CONTINUE_EXECUTING);
	final List<AliasSubcuenta> aliasSubcuenta = aliasSubucuentaBo
		.sincronizarAliasSubcuenta();
	if (CollectionUtils.isNotEmpty(aliasSubcuenta)) {
	    LOG.trace("{}::{} [Gerenados " + aliasSubcuenta.size()
		    + " aliasSubuenta", TAG, CONTINUE_EXECUTING);
	} else {
	    LOG.debug("{}::{} [No se han generado aliasSubuenta", TAG, CONTINUE_EXECUTING);
	}
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_ALIAS_SINCRONIZACION;
    }

}
