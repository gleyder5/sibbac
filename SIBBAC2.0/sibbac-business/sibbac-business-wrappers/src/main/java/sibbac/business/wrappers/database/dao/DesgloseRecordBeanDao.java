package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.DesgloseRecordEjecucionesDTO;
import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoOperacion;

public interface DesgloseRecordBeanDao extends JpaRepository<DesgloseRecordBean, Long> {

  @Query("select new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.id, t.codEmprl, t.emprCTVA, t.centCTVA, t.dpteCTVA, t.numeroOrden, t.numeroReferenciaOrden, t.nucnfliq, t.fechaProcesamiento,"
      + "t.codigoSV, t.fechaEjecucion, t.codigoBolsaEjecucion, t.codigoValorISO, t.tipoOperacion, t.tipoCambio, t.titulosEjecutados, "
      + "t.nominalEjecutado, t.cambioOperacion, t.descripcionValor, t.bicTitular, t.tipoSaldo, t.referenciaModelo, t.segmento,"
      + "t.ordenComercial, t.porcesado, (select p.ccv from PartenonRecordBean p where p.desgloseRecordBean.id = t.id group by p.ccv )) "
      + "from DesgloseRecordBean t "
      + "where t.porcesado = :procesado "
      + "order by t.numeroOrden asc, t.titulosEjecutados asc")
  List<DesgloseRecordDTO> findAllProcesado(@Param("procesado") Integer procesado);

  @Query("select distinct new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.numeroOrden) "
      + "from DesgloseRecordBean t " + "where t.porcesado = :procesado " + "order by t.numeroOrden asc")
  List<DesgloseRecordDTO> findAllOrdenesProcesadas(@Param("procesado") Integer procesado);

  @Query("select count(*) from DesgloseRecordBean t where t.numeroOrden = :numeroOrden")
  Integer countByNumeroOrden(@Param("numeroOrden") String numeroOrden);

  @Query("select distinct new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.numeroOrden) "
      + "from DesgloseRecordBean t " + "where t.porcesado in :procesado " + "order by t.numeroOrden asc")
  List<DesgloseRecordDTO> findAllOrdenesByInProcesado(@Param("procesado") List<Integer> procesado);

  @Query("select new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.id, t.codEmprl, t.emprCTVA, t.centCTVA, t.dpteCTVA, t.numeroOrden, t.numeroReferenciaOrden, t.nucnfliq, t.fechaProcesamiento,"
      + "t.codigoSV, t.fechaEjecucion, t.codigoBolsaEjecucion, t.codigoValorISO, t.tipoOperacion, t.tipoCambio, t.titulosEjecutados, "
      + "t.nominalEjecutado, t.cambioOperacion, t.descripcionValor, t.bicTitular, t.tipoSaldo, t.referenciaModelo, t.segmento,"
      + "t.ordenComercial, t.porcesado, (select p.ccv from PartenonRecordBean p where p.desgloseRecordBean.id = t.id group by p.ccv )) "
      + "from DesgloseRecordBean t " + "where t.numeroOrden = :numOrden and t.porcesado =  :pprocesado ")
  List<DesgloseRecordDTO> findAllNumOrden(@Param("numOrden") String numOrden, @Param("pprocesado") int procesado);

  @Query("select new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.id, t.numeroOrden, t.numeroReferenciaOrden, t.titulosEjecutados, t.codigoBolsaEjecucion, t.ordenComercial,"
      + " t.codEmprl, t.tipoSaldo) "
      + "from DesgloseRecordBean t "
      + "where t.numeroReferenciaOrden in :nurefords "
      + "order by t.titulosEjecutados asc, t.id asc")
  List<DesgloseRecordDTO> findAllTitulosEjecutadosByListNureford(@Param("nurefords") List<String> nurefords);

  @Query("SELECT new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.id, t.codEmprl, t.emprCTVA, t.centCTVA, t.dpteCTVA, t.numeroOrden,"
      + "  t.numeroReferenciaOrden, t.nucnfliq, t.fechaProcesamiento, t.codigoSV, t.fechaEjecucion, t.codigoBolsaEjecucion, t.codigoValorISO,"
      + "  t.tipoOperacion, t.tipoCambio, t.titulosEjecutados, t.nominalEjecutado, t.cambioOperacion, t.descripcionValor, t.bicTitular,"
      + "  t.tipoSaldo, t.referenciaModelo, t.segmento, t.ordenComercial, t.porcesado, '')"
      + " FROM DesgloseRecordBean t WHERE t.porcesado = :pprocesado " + " ORDER BY t.numeroOrden ASC")
  List<DesgloseRecordDTO> findAllDesgloseRecordDTOByPorcesadoToEnvio1015(@Param("pprocesado") int procesado,
      Pageable page);

  @Query("SELECT new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.id, t.codEmprl, t.emprCTVA, t.centCTVA, t.dpteCTVA, t.numeroOrden,"
      + "  t.numeroReferenciaOrden, t.nucnfliq, t.fechaProcesamiento, t.codigoSV, t.fechaEjecucion, t.codigoBolsaEjecucion, t.codigoValorISO,"
      + "  t.tipoOperacion, t.tipoCambio, t.titulosEjecutados, t.nominalEjecutado, t.cambioOperacion, t.descripcionValor, t.bicTitular,"
      + "  t.tipoSaldo, t.referenciaModelo, t.segmento, t.ordenComercial, t.porcesado, '')"
      + " FROM DesgloseRecordBean t WHERE t.numeroReferenciaOrden in :nurefords " + " ORDER BY t.numeroOrden ASC")
  List<DesgloseRecordDTO> findAllDesgloseRecordDTOByListNurefordToEnvio1015(@Param("nurefords") List<String> nurefords);

  @Query("select new sibbac.business.wrappers.database.dto.DesgloseRecordDTO(t.id, t.codEmprl, t.emprCTVA, t.centCTVA, t.dpteCTVA, t.numeroOrden, t.numeroReferenciaOrden, t.nucnfliq, t.fechaProcesamiento,"
      + "t.codigoSV, t.fechaEjecucion, t.codigoBolsaEjecucion, t.codigoValorISO, t.tipoOperacion, t.tipoCambio, t.titulosEjecutados, "
      + "t.nominalEjecutado, t.cambioOperacion, t.descripcionValor, t.bicTitular, t.tipoSaldo, t.referenciaModelo, t.segmento,"
      + "t.ordenComercial, t.porcesado, (select p.ccv from PartenonRecordBean p where p.desgloseRecordBean.id = t.id group by p.ccv )) "
      + "from DesgloseRecordBean t " + "where t.id = :pid ")
  DesgloseRecordDTO findDesgloseRecordDTOById(@Param("pid") Long id);

  @Query("select new sibbac.business.wrappers.database.dto.DesgloseRecordEjecucionesDTO(dt.tmct0desglosecamara.tmct0alc.id.nucnfclt, dt.nuejecuc, "
      + " sum(dt.imtitulos), dt.nuordenmkt, dt.nurefexemkt, dt.cdPlataformaNegociacion, dt.cddivisa , dt.imprecio, dt.tmct0desglosecamara.feliquidacion, "
      + " dt.feejecuc, dt.hoejecuc, sum(dt.imcomisn), sum(dt.imfinsvb), sum(dt.imcanoncontreu), sum(dt.imcanoncompeu), sum(dt.imcanonliqeu), sum(dt.imefectivo),  "
      + " dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncontrpagoclte, dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncomppagoclte, dt.tmct0desglosecamara.tmct0alc.tmct0alo.canonliqpagoclte"
      + ")  "
      + " from Tmct0desgloseclitit dt where dt.tmct0desglosecamara.tmct0alc.id.nuorden = :pnuorden  "
      + " and dt.tmct0desglosecamara.tmct0alc.id.nucnfclt = :pnucnfclt  "
      + " and dt.tmct0desglosecamara.tmct0alc.cdestadoasig > 0 "
      + " group by dt.tmct0desglosecamara.tmct0alc.id.nucnfclt, dt.nuejecuc, dt.tmct0desglosecamara.tmct0alc.id.nuorden, dt.tmct0desglosecamara.tmct0alc.id.nbooking, "
      + " dt.tmct0desglosecamara.tmct0alc.id.nucnfclt, dt.nuordenmkt, dt.nurefexemkt, dt.cdPlataformaNegociacion, dt.cddivisa, dt.imprecio, dt.tmct0desglosecamara.feliquidacion,"
      + " dt.feejecuc, dt.hoejecuc,"
      + " dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncontrpagoclte, dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncomppagoclte, dt.tmct0desglosecamara.tmct0alc.tmct0alo.canonliqpagoclte ")
  List<DesgloseRecordEjecucionesDTO> findAllDesglosesFor1015ByNuordenAndNucnfclt(@Param("pnuorden") String nuorden,
      @Param("pnucnfclt") String nucnfclt);

  @Query("SELECT new sibbac.business.wrappers.database.dto.DesgloseRecordEjecucionesDTO(dt.tmct0desglosecamara.tmct0alc.id.nucnfclt, dt.nuejecuc,"
      + "  sum(dt.imtitulos), dt.nuordenmkt, dt.nurefexemkt, dt.cdPlataformaNegociacion, dt.cddivisa , dt.imprecio, dt.tmct0desglosecamara.feliquidacion,"
      + "  dt.feejecuc, dt.hoejecuc, sum(dt.imcomisn), sum(dt.imfinsvb), sum(dt.imcanoncontreu), sum(dt.imcanoncompeu), sum(dt.imcanonliqeu),"
      + "  sum(dt.imefectivo), dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncontrpagoclte, dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncomppagoclte,"
      + "  dt.tmct0desglosecamara.tmct0alc.tmct0alo.canonliqpagoclte)"
      + " FROM Tmct0desgloseclitit dt"
      + " WHERE dt.tmct0desglosecamara.tmct0alc.id.nuorden = :pnuorden AND dt.tmct0desglosecamara.tmct0alc.id.nucnfclt in :pnucnfclt"
      + "       AND dt.tmct0desglosecamara.tmct0alc.cdestadoasig > 0"
      + " GROUP BY dt.tmct0desglosecamara.tmct0alc.id.nucnfclt, dt.nuejecuc, dt.tmct0desglosecamara.tmct0alc.id.nuorden,"
      + "          dt.tmct0desglosecamara.tmct0alc.id.nbooking, dt.tmct0desglosecamara.tmct0alc.id.nucnfclt, dt.nuordenmkt, dt.nurefexemkt,"
      + "          dt.cdPlataformaNegociacion, dt.cddivisa, dt.imprecio, dt.tmct0desglosecamara.feliquidacion, dt.feejecuc, dt.hoejecuc,"
      + "          dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncontrpagoclte, dt.tmct0desglosecamara.tmct0alc.tmct0alo.canoncomppagoclte,"
      + "          dt.tmct0desglosecamara.tmct0alc.tmct0alo.canonliqpagoclte")
  List<DesgloseRecordEjecucionesDTO> findAllDesglosesFor1015ByNuordenAndListNucnfclt(@Param("pnuorden") String nuorden,
      @Param("pnucnfclt") List<String> nucnfclt);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :procesado WHERE t.numeroReferenciaOrden in (:refOrd) ")
  Integer updateProcesadoByNurefordAndProcesado(@Param("refOrd") List<String> nurefOrd,
      @Param("procesado") Integer procesado);

  @Query("SELECT t FROM  DesgloseRecordBean t WHERE t.numeroReferenciaOrden in (:refOrd) ORDER BY t.id DESC")
  List<DesgloseRecordBean> findAllByNureford(@Param("refOrd") List<String> nurefOrd);

  @Query("SELECT t FROM  DesgloseRecordBean t, PartenonRecordBean r WHERE t.id=r.desgloseRecordBean.id and r.id = :id ")
  DesgloseRecordBean findByPartenonRecordBeanId(@Param("id") Long partenonRecordBeanId);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :procesado, t.auditDate = :auditdate WHERE t.id in "
      + " (SELECT r.desgloseRecordBean.id FROM PartenonRecordBean r WHERE r.id = :id) ")
  Integer updateProcesadoByPartenonRecordBeanId(@Param("id") Long partenonRecordBeanId,
      @Param("procesado") Integer procesado, @Param("auditdate") Date auditdate);

  @Query("SELECT sum(t.titulosEjecutados) FROM  DesgloseRecordBean t WHERE t.numeroOrden = :pnuorden and  t.porcesado < :pprocesado")
  BigDecimal sumAllTitulosByNumordenAndPorcesadoLt(@Param("pnuorden") String nuorden, @Param("pprocesado") int procesado);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :setprocesado, t.auditDate = :auditdate "
      + " WHERE t.numeroOrden = :pnuorden and  t.porcesado = :pwhereprocesado ")
  Integer updateProcesadoByNumordenAndPorcesado(@Param("pnuorden") String nuorden,
      @Param("pwhereprocesado") int whereprocesado, @Param("setprocesado") int setprocesado,
      @Param("auditdate") Date auditdate);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :setprocesado, t.auditDate = :auditdate "
      + " WHERE t.id in  :ids and  t.porcesado = :pwhereprocesado ")
  Integer updateProcesadoByListId(@Param("ids") List<Long> ids, @Param("pwhereprocesado") int whereprocesado,
      @Param("setprocesado") int setprocesado, @Param("auditdate") Date auditdate);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :setprocesado, t.auditDate = :auditdate "
      + " WHERE t.id = :pid ")
  Integer updateProcesadoById(@Param("pid") Long id, @Param("setprocesado") int setprocesado,
      @Param("auditdate") Date auditdate);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :setprocesado, t.tipoOperacion = :ptipoop, t.auditDate = :auditdate "
      + " WHERE t.numeroOrden = :pnuorden and  t.porcesado = :pwhereprocesado ")
  Integer updateProcesadoAndTipoOperacionByNumordenAndPorcesado(@Param("pnuorden") String nuorden,
      @Param("pwhereprocesado") int whereprocesado, @Param("setprocesado") int setprocesado,
      @Param("ptipoop") TipoOperacion tipoOp, @Param("auditdate") Date auditdate);

  @Modifying
  @Query("UPDATE DesgloseRecordBean t SET t.porcesado = :setprocesado, t.auditDate = :auditdate "
      + " WHERE  t.porcesado = :pwhereprocesado AND ( t.ordenComercial not in :notinordencomercial OR t.ordenComercial is null )")
  Integer updateProcesadoAndByPorcesadoAndNotInOrdenComercial(@Param("pwhereprocesado") int whereprocesado,
      @Param("notinordencomercial") List<String> notInOrdenComercial, @Param("setprocesado") int setprocesado,
      @Param("auditdate") Date auditdate);

  @Query("SELECT p.numeroReferenciaOrden FROM DesgloseRecordBean p WHERE p.porcesado = :pprocesado GROUP BY p.numeroReferenciaOrden ORDER BY p.numeroReferenciaOrden ASC")
  public List<String> findAllNurefordOperacionesEspeciales(@Param("pprocesado") int procesado, Pageable page);

  @Query("SELECT p.numeroReferenciaOrden FROM DesgloseRecordBean p WHERE p.numeroOrden = :pnumorden  AND p.porcesado = :pprocesado "
      + " GROUP BY p.numeroReferenciaOrden ORDER BY p.numeroReferenciaOrden ASC")
  public List<String> findAllNurefordOperacionesEspecialesByProcesadoAndNumorden(
      @Param("pnumorden") String numeroOrden, @Param("pprocesado") int procesado, Pageable page);

}
