package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Tmct0bokId;

public class ActualizacionCodigosOperacionEccDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 4070362223125942353L;
  private final Date fcontratacion;
  private final String cdisin;
  private final char cdsentido;
  private final int estadoAsignacion;

  private final List<Tmct0bokId> bookings;

  public ActualizacionCodigosOperacionEccDTO(Date fcontratacion, String cdisin, char cdsentido, int estadoAsignacion) {
    super();
    this.fcontratacion = fcontratacion;
    this.cdisin = cdisin;
    this.cdsentido = cdsentido;
    this.estadoAsignacion = estadoAsignacion;
    this.bookings = new ArrayList<Tmct0bokId>();
  }

  public Date getFcontratacion() {
    return fcontratacion;
  }

  public String getCdisin() {
    return cdisin;
  }

  public char getCdsentido() {
    return cdsentido;
  }

  public int getEstadoAsignacion() {
    return estadoAsignacion;
  }

  public List<Tmct0bokId> getBookings() {
    return bookings;
  }

  @Override
  public String toString() {
    return String.format("ActualizacionCodigosOperacionEccDTO [fcontratacion=%s, cdisin=%s, cdsentido=%s]",
        fcontratacion, cdisin, cdsentido);
  }

}
