package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0ctaDao;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctaId;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ctaBo extends AbstractBo<Tmct0cta, Tmct0ctaId, Tmct0ctaDao> {

  public List<Tmct0cta> findAllByNuodenAndNbooking(String nuorden, String nbooking) {
    return dao.findAllByNuodenAndNbooking(nuorden, nbooking);
  }

  public List<Tmct0cta> findAllByNuodenAndNbookingAndNucnfclt(String nuorden, String nbooking, String nucnfclt) {
    return dao.findAllByNuodenAndNbookingAndNucnfclt(nuorden, nbooking, nucnfclt);
  }

  public List<Tmct0cta> findAllByNuordenAndNbookingAndCdbrokerAndCdmercadAndNucnfclt(String nuorden, String nbooking,
      String cdbroker, String cdmercad, String nucnfclt) {

    return dao.findAllByNuordenAndNbookingAndCdbrokerAndCdmercadAndNucnfclt(nuorden, nbooking, cdbroker, cdmercad,
        nucnfclt);
  }

  public boolean isCdestadoInternacionalComplete(Tmct0ctg ctg) {
    boolean res = true;
    List<String> estados = dao.findAllDistinctCdestadoInTmct0ctas(ctg.getId().getNuorden(), ctg.getId().getNbooking(),
        ctg.getId().getCdbroker(), ctg.getId().getCdmercad());
    if (CollectionUtils.isNotEmpty(estados)) {
      res = estados.size() == 1;
    }
    return res;
  }

}
