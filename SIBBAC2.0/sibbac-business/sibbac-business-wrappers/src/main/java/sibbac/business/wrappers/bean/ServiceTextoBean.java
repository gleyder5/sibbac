package sibbac.business.wrappers.bean;


import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;


/**
 * The Class ServiceTextoBean.
 */
public class ServiceTextoBean {

	/**
	 * The Enum SIBBACServiceTextoFields.
	 */
	public enum SIBBACServiceTextoFields {

		/** The fecha inicio. */
		DESCRIPCION_FIELD( "descripcion" ),
		/** The fecha hasta. */
		ID_FIELD( "id" );

		/** The field. */
		private String	field;

		/**
		 * Instantiates a new fields financiacion intereses.
		 *
		 * @param field
		 *            the field
		 */
		private SIBBACServiceTextoFields( final String field ) {
			this.field = field;
		}

		/**
		 * Gets the field.
		 *
		 * @return the field
		 */
		public String getField() {
			return field;
		}
	}

	private String	descripcion;

	private Long	id;

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param filters
	 */
	public ServiceTextoBean( Map< String, String > filters ) throws SIBBACBusinessException {
		final String descipcion = StringUtils.trimToEmpty( filters.get( SIBBACServiceTextoFields.DESCRIPCION_FIELD.getField() ) );
		if ( StringUtils.isNotEmpty( descipcion ) ) {
			this.descripcion = descipcion;
		}
		this.id = FormatDataUtils
				.convertStringToLong( StringUtils.trimToEmpty( filters.get( SIBBACServiceTextoFields.ID_FIELD.getField() ) ) );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		final ToStringBuilder tsb = new ToStringBuilder( ToStringStyle.SHORT_PREFIX_STYLE );
		tsb.append( getId() );
		tsb.append( getDescripcion() );
		return tsb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( getId() );
		hcb.append( getDescripcion() );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object object ) {
		final ServiceTextoBean serviceTextoBean = ( ServiceTextoBean ) object;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( getId(), serviceTextoBean.getId() );
		eqb.append( getDescripcion(), serviceTextoBean.getDescripcion() );
		return eqb.isEquals();
	}

}
