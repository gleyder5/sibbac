package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.CodigoTpidenti;


@Repository
public interface CodigoTpidentiDao extends JpaRepository< CodigoTpidenti, String > {

	public CodigoTpidenti findByCodigo( String codigo );

}
