package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;

@Repository
public interface Tmct0operacioncdeccDao extends JpaRepository<Tmct0operacioncdecc, Long>,
    JpaSpecificationExecutor<Tmct0operacioncdecc> {

  @Query("SELECT distinct o.cdoperacionecc from Tmct0operacioncdecc o")
  List<String> getLCdoperacion();

  Tmct0operacioncdecc findByCdoperacionecc(String cdoperacionecc);

  @Query("SELECT o FROM Tmct0operacioncdecc o WHERE  o.cdoperacionecc IN :cdoperacioneccs ")
  List<Tmct0operacioncdecc> findByInCdoperacionecc(@Param("cdoperacioneccs") List<String> cdoperacionecc);

  @Query("SELECT o FROM Tmct0operacioncdecc o, Tmct0caseoperacioneje c WHERE  c.tmct0operacioncdecc.idoperacioncdecc = o.idoperacioncdecc AND c.idcase = :idcase")
  Tmct0operacioncdecc findByIdCase(@Param("idcase") Long idcase);

  @Query("SELECT o.cdoperacionecc FROM Tmct0operacioncdecc o, Tmct0CuentasDeCompensacion c, Tmct0TipoCuentaDeCompensacion tc "
      + "  WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND c.tmct0TipoCuentaDeCompensacion.idTipoCuenta = tc.idTipoCuenta "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND tc.cdCodigo = 'CD' "
      + " AND (o.envioS3 IS NULL OR o.envioS3 = 'N') ")
  List<String> findAllCdoperacioneccEnvioS3(@Param("fecontratacion") Date fecontratacion);

  @Query("SELECT distinct o.cdoperacionecc FROM Tmct0operacioncdecc o, Tmct0desgloseclitit dt where o.cdoperacionecc = dt.nuoperacioninic "
      + " and o.fcontratacion = :fecontratacion and (o.ceenviado <> 'S' or o.ceenviado is null) and o.cdcamara = :camara ")
  List<String> findAllCdoperacioneccForEnvioCe(@Param("fecontratacion") Date fecontratacion,
      @Param("camara") String camara);

  /**
   * Metodo para obtener el idcase y los datos de mercado dada una ejecucion
   * 
   * @param nuordenmkt
   * @param nuexemkt
   * @param fcontratacion
   * @param cdoperacionmkt
   * @param cdisin
   * @param cdsegmento
   * @param cdsentido
   * @param cdmiembromkt
   * @return
   */
  @Query("SELECT o from Tmct0operacioncdecc o"
      + " where o.nuordenmkt=:nuordenmkt and o.nuexemkt=:nuexemkt and "
      + "  o.fcontratacion=:fcontratacion and o.cdoperacionmkt=:cdoperacionmkt and o.cdisin=:cdisin and o.cdsegmento=:cdsegmento"
      + " and o.cdsentido=:cdsentido and o.cdmiembromkt=:cdmiembromkt ")
  Tmct0operacioncdecc findTmct0operacioncdeccByInformacionMercado(@Param("nuordenmkt") String nuordenmkt,
      @Param("nuexemkt") String nuexemkt, @Param("fcontratacion") Date fcontratacion,
      @Param("cdoperacionmkt") String cdoperacionmkt, @Param("cdisin") String cdisin,
      @Param("cdsegmento") String cdsegmento, @Param("cdsentido") Character cdsentido,
      @Param("cdmiembromkt") String cdmiembromkt);

  @Query("select o from Tmct0operacioncdecc o, Tmct0caseoperacioneje c where o.cdoperacionmkt = :cdoperacionmkt and o.cdisin = :cdisin "
      + " and o.nuexemkt = :nuexemkt and o.fcontratacion = :fcontratacion and o.cdsegmento = :cdsegmento and o.cdmiembromkt = :cdmiembromkt "
      + " and o.idoperacioncdecc = c.tmct0operacioncdecc.idoperacioncdecc ")
  List<Tmct0operacioncdecc> findAllByCdoperacionmktAndCdisinAndNuexemktAndFcontratacionAndCdsegmentoAndCdsentidoAndCdmiembromktWithCase(
      @Param("cdoperacionmkt") String cdoperacionmkt, @Param("cdisin") String cdisin,
      @Param("nuexemkt") String nuexemkt, @Param("fcontratacion") Date fcontratacion,
      @Param("cdsegmento") String cdsegmento, @Param("cdmiembromkt") String cdmiembromkt);

}
