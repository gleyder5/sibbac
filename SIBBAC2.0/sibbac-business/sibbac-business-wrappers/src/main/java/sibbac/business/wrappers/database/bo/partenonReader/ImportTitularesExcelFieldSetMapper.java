package sibbac.business.wrappers.database.bo.partenonReader;


import java.util.Objects;

import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.OperacionEspecialFieldSetMapper;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;


public class ImportTitularesExcelFieldSetMapper< T extends PartenonRecordBean > extends
		OperacionEspecialFieldSetMapper< OperacionEspecialRecordBean > {

	public enum ImportExcelFields {
		BOOKING( "booking" ),
		PROPUESTA_CLIENTE( "propuestacliente" ),
		CCV( "ccv" ),
		NBCLIENT( "nbclient" ),
		NBCLIENT1( "nbclient1" ),
		NBCLIENT2( "nbclient2" ),
		TIPDOC( "tipdoc" ),
		DOCUMENTO( "documento" ),
		TIPOPERS( "tipopers" ),
		PAISRES( "paisres" ),
		NACIONALIDAD( "nacionalidad" ),
		DOMICILIO( "domicilio" ),
		POBLACION( "poblacion" ),
		PROVINCIA( "provincia" ),
		CODPOSTAL( "codpostal" ),
		TIPTITU( "tiptitu" ),
		PARTICIPACION( "participacion" ),
		TIPO_NACIONALIDAD( "tipnac" ),
		NUREFORD( "nureford" );

		private final String	text;

		/**
		 * @param text
		 */
		private ImportExcelFields( final String text ) {
			this.text = text;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return text;
		}

		public static String[] getFieldNames() {
			ImportExcelFields[] fields = ImportExcelFields.values();
			final String[] namesArray = new String[ fields.length ];
			for ( int i = 0; i < fields.length; i++ ) {
				namesArray[ i ] = fields[ i ].text;
			}
			return namesArray;
		}

	}

	@Override
	public PartenonRecordBean mapFieldSet( FieldSet fieldSet ) throws BindException {
		PartenonRecordBean partenonBean = new PartenonRecordBean();
		String propuestaCliente = fieldSet.readString( ImportExcelFields.PROPUESTA_CLIENTE.text ).trim();
		try {
			if ( Objects.equals( propuestaCliente, "Referencia" ) ) {
				return null;
			}
			partenonBean.setNumOrden( fieldSet.readString( ImportExcelFields.PROPUESTA_CLIENTE.text ).trim() );
			partenonBean.setCcv( fieldSet.readString( ImportExcelFields.CCV.text ).trim() );
			partenonBean.setNbclient( fieldSet.readString( ImportExcelFields.NBCLIENT.text ).trim() );
			partenonBean.setNbclient1( fieldSet.readString( ImportExcelFields.NBCLIENT1.text ).trim() );
			partenonBean.setNbclient2( fieldSet.readString( ImportExcelFields.NBCLIENT2.text ).trim() );
			partenonBean.setTipoDocumento( fieldSet.readChar( ImportExcelFields.TIPDOC.text ) );
			partenonBean.setNifbic( fieldSet.readString( ImportExcelFields.DOCUMENTO.text ).trim() );
			partenonBean.setTipoPersona( fieldSet.readChar( ImportExcelFields.TIPOPERS.text ) );
			partenonBean.setPaisResidencia( fieldSet.readString( ImportExcelFields.PAISRES.text ).trim() );
			partenonBean.setNacionalidad( fieldSet.readString( ImportExcelFields.NACIONALIDAD.text ).trim() );
			partenonBean.setDomicili( fieldSet.readString( ImportExcelFields.DOMICILIO.text ).trim() );
			partenonBean.setPoblacion( fieldSet.readString( ImportExcelFields.POBLACION.text ).trim() );
			partenonBean.setProvincia( fieldSet.readString( ImportExcelFields.PROVINCIA.text ).trim() );
			partenonBean.setDistrito( fieldSet.readString( ImportExcelFields.CODPOSTAL.text ).trim() );
			partenonBean.setTiptitu( fieldSet.readChar( ImportExcelFields.TIPTITU.text ) );
			partenonBean.setParticip( fieldSet.readBigDecimal( ImportExcelFields.PARTICIPACION.text ) );
			partenonBean.setIndNac( fieldSet.readChar( ImportExcelFields.TIPO_NACIONALIDAD.text ) );
		} catch ( Exception e ) {
			return null;
		}
		return partenonBean;
	}

}
