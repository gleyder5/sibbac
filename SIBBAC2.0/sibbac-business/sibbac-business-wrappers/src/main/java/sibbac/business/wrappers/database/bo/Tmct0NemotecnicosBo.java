package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0NemotecnicosDao;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0ModeloDeNegocio;
import sibbac.business.wrappers.database.model.Tmct0Nemotecnicos;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0NemotecnicosBo extends AbstractBo< Tmct0Nemotecnicos, Long, Tmct0NemotecnicosDao> {

	/** The Constant LOG. */
	private static final Logger	LOG	= LoggerFactory.getLogger( Tmct0NemotecnicosBo.class );
	
	public List< Tmct0Nemotecnicos > findAll() {    
		return this.dao.findAll(new Sort(Sort.Direction.ASC,"nbDescripcion"));
	}
	
	public Tmct0Nemotecnicos findByNombre(String nbNombre){
		return this.dao.findByNbNombre(nbNombre);
	}
}


