package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.dao.Tmct0movimientoeccDao;
import sibbac.business.wrappers.database.dao.Tmct0movimientoeccDaoImp;
import sibbac.business.wrappers.database.dto.DatosAsignacionDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIConstants.MO_CONSTANTS.TIPO_MOVIMIENTO;

@Service
public class Tmct0movimientoeccBo extends AbstractBo<Tmct0movimientoecc, Long, Tmct0movimientoeccDao> {

  @PersistenceContext
  private EntityManager manager;

  @Autowired
  private Tmct0movimientoeccDaoImp daoImpl;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movnBo;

  @Autowired
  private Tmct0movimientoeccFidessaBo movfBo;

  @Autowired
  private Tmct0logBo logBo;

  public List<Tmct0movimientoecc> findAll() {
    return this.dao.findAll();
  }

  public List<Tmct0movimientoecc> findAllByCdrefmovimientoecc(String cdrefmovimientoecc) {
    return dao.findAllByCdrefmovimientoecc(cdrefmovimientoecc);
  }

  public List<Tmct0movimientoecc> findAllByCdrefmovimientoeccAndInCdestadosmov(String cdrefmovimientoecc,
      List<String> cdestadomovActivos) {
    return dao.findAllByCdrefmovimientoeccAndInCdestadosmov(cdrefmovimientoecc, cdestadomovActivos);
  }

  public List<Tmct0movimientoecc> findAllByCdrefmovimiento(String cdrefmovimiento) {
    return dao.findAllByCdrefmovimiento(cdrefmovimiento);
  }

  public List<Tmct0movimientoecc> findAllByCdrefmovimientoeccAndFeliquidacionAndDesgloseActivo(
      String cdrefmovimientoecc, Date feliquidacion) {
    return dao.findAllByCdrefmovimientoeccAndFeliquidacionAndDesgloseActivo(cdrefmovimientoecc, feliquidacion);
  }

  public List<Tmct0movimientoecc> findAllByCdrefmovimientoeccAndFeliquidacionAndAloActivo(String cdrefmovimientoecc,
      Date feliquidacion) {
    return dao.findAllByCdrefmovimientoeccAndFeliquidacionAndAloActivo(cdrefmovimientoecc, feliquidacion);
  }

  /*
   * LUIS, por favor, NO TOQUES NADA Y HABLA CONMIGO. public Object[]
   * findByIsinAndIdEstadoAsigAndIdEstadoContAndCodCuenta( String isin, String
   * sentido, int idEstadoAsig, int idEstadoCont, String codCuenta ) { return
   * dao.findByIsinAndIdEstadoAsigAndIdEstadoContAndCodCuenta( codCuenta, isin,
   * sentido, idEstadoAsig, idEstadoCont ); }
   */

  public List<Tmct0movimientoecc> findByIdDesgloseCamara(Long iddesgcamara) {
    return dao.findByIdDesgloseCamara(iddesgcamara);
  }

  public List<Tmct0movimientoecc> findByIdDesgloseCamaraAndNotCdestadomov(Long iddesglosecamara, String notCdestadomov) {
    return dao.findByIdDesgloseCamaraAndNotCdestadomov(iddesglosecamara, notCdestadomov);
  }

  public List<Tmct0movimientoecc> findByTmct0aloId(Tmct0aloId aloId) {
    return dao.findByTmct0aloId(aloId);
  }

  /*
   * LUIS, por favor, NO TOQUES NADA Y HABLA CONMIGO.
   */
  public List<Tmct0movimientoecc> findAllNoEnviadosCuentaVirtualByCdestadomov(String cdestadomov) {
    return dao.findAllNoEnviadosCuentaVirtualByCdestadomov(cdestadomov);
  }

  public List<Tmct0movimientoecc> findAllNoEnviadosCuentaAliasByCdestadomov(String cdestadomov) {
    return dao.findAllNoEnviadosCuentaAliasByCdestadomov(cdestadomov);
  }

  public List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigByIddesglosecamara(Long iddesglosescamara) {
    return dao.findAllDistinctCtaMiembroRefAsigByIddesglosecamara(iddesglosescamara);
  }

  public boolean hasMovimientoImputadoS3(Long iddesglosescamara) {
    return dao.countMovimientosImputadosS3ByIdesglosecamara(iddesglosescamara) > 0;
  }

  public boolean hasMovimientoImputadoS3(Tmct0alcId alcId) {
    return dao.countMovimientosImputadosS3ByAlcId(alcId) > 0;
  }

  public List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigByAlcId(Tmct0alcId alcId) {
    return dao.findAllDistinctCtaMiembroRefAsigByAlcId(alcId);
  }

  public List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigFromDesglosesCamaraByBookingId(Tmct0bokId bookingId) {
    return dao.findAllDistinctCtaMiembroRefAsigFromDesglosesCamaraByBookingId(bookingId);
  }

  public List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigFromAloByBookingId(Tmct0bokId bookingId) {
    return dao.findAllDistinctCtaMiembroRefAsigFromAloByBookingId(bookingId);
  }

  public List<Tmct0bokId> findAllBookingWithMovimientosRecibiendose(char isScrdiv) {
    List<Tmct0bokId> listBokIdFromDc = dao.findAllBookingIdByMovimientoeccCdestadoasigFromDesgloseCamara(
        EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId(), isScrdiv);

    List<Tmct0bokId> listBokIdFromAlo = dao.findAllBookingIdByMovimientoeccCdestadoasigFromTmct0alo(
        EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId(), isScrdiv);

    Collection<Tmct0bokId> listBokingsSinDuplicados = new HashSet<Tmct0bokId>(listBokIdFromDc);

    listBokingsSinDuplicados.addAll(listBokIdFromAlo);

    listBokIdFromDc = new ArrayList<Tmct0bokId>(listBokingsSinDuplicados);
    listBokingsSinDuplicados.clear();
    listBokIdFromAlo.clear();

    return listBokIdFromDc;
  }

  public List<Long> findAllIddesglosecamaraWithMovimientosRecibiendose(Tmct0bokId tmct0bokId) {
    return dao.findAllIddesglosecamaraByTmct0bokIdAndMovimientoeccCdestadoasig(tmct0bokId,
        EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
  }

  public List<Tmct0aloId> findAllTmct0aloIdWithMovimientosRecibiendose(Tmct0bokId tmct0bokId) {
    return dao.findAllTmct0aloIdByTmct0bokIdAndMovimientoeccCdestadoasig(tmct0bokId,
        EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
  }

  public void inizializeTmct0movimientoEcc(Tmct0desglosecamara dc, Tmct0desgloseclitit dt, Tmct0infocompensacion ic,
      String cdrefmovimiento, String cdrefinternaasig, Tmct0movimientoecc mov) {
    mov.setAuditFechaCambio(new Date());

    mov.setAuditUser("MOV");
    mov.setCdestadomov("20");// enviandose
    mov.setCdindcotizacion(dt.getCdindcotizacion());
    mov.setCdrefinternaasig(cdrefinternaasig);
    mov.setCdrefmovimiento(cdrefmovimiento);
    mov.setCdrefmovimientoecc("");
    mov.setCdrefnotificacion("");
    mov.setCdrefnotificacionprev("");
    if (StringUtils.isNotBlank(ic.getCdctacompensacion())) {
      mov.setCdtipomov(TIPO_MOVIMIENTO.TAKE_UP);// asignacion interna
    }
    else {
      mov.setCdtipomov(TIPO_MOVIMIENTO.GIVE_UP);// asignacion externa
    }

    mov.setCodigoReferenciaAsignacion(ic.getCdrefasignacion() == null ? "" : ic.getCdrefasignacion());
    mov.setCuentaCompensacionDestino(ic.getCdctacompensacion() == null ? "" : ic.getCdctacompensacion());
    mov.setImefectivo(BigDecimal.ZERO);
    mov.setImputacionPropia('N');
    mov.setImtitulos(BigDecimal.ZERO);
    mov.setIsin(dt.getCdisin());
    mov.setMiembroCompensadorDestino(ic.getCdmiembrocompensador() == null ? "" : ic.getCdmiembrocompensador());
    mov.setSentido(dt.getCdsentido());
    mov.setFechaLiquidacion(dc.getFeliquidacion());
  }

  public void inizializeTmct0movimientoEcc(Tmct0desgloseclitit dt, Tmct0movimientoecc mov, String auditUser) {
    mov.setAuditFechaCambio(new Date());

    mov.setAuditUser(auditUser);
    mov.setCdestadomov("20");// enviandose
    mov.setCdindcotizacion(dt.getCdindcotizacion());
    mov.setCdrefinternaasig(dt.getTmct0desglosecamara().getTmct0alc().getId().getNucnfclt());
    mov.setCdrefmovimiento(getNewMOCdrefmovimientoFromMOSequence());
    mov.setCdrefmovimientoecc("");
    mov.setCdrefnotificacion("");
    mov.setCdrefnotificacionprev("");
    mov.setCdtipomov("");

    mov.setCodigoReferenciaAsignacion(dt.getCodigoReferenciaAsignacion() == null ? "" : dt
        .getCodigoReferenciaAsignacion());
    mov.setCuentaCompensacionDestino(dt.getCuentaCompensacionDestino() == null ? "" : dt.getCuentaCompensacionDestino());
    mov.setImefectivo(BigDecimal.ZERO);
    mov.setImputacionPropia('N');
    mov.setImtitulos(BigDecimal.ZERO);
    mov.setIsin(dt.getCdisin());
    mov.setMiembroCompensadorDestino(dt.getMiembroCompensadorDestino() == null ? "" : dt.getMiembroCompensadorDestino());
    mov.setSentido(dt.getCdsentido());
    mov.setFechaLiquidacion(dt.getTmct0desglosecamara().getFeliquidacion());
  }

  public void inizializeTmct0movimientoEcc(Tmct0desgloseclitit dt, Tmct0movimientoeccFidessa movf,
      Tmct0movimientoecc mov, String auditUser) {
    mov.setAuditFechaCambio(new Date());

    mov.setAuditUser(auditUser);
    mov.setCdestadomov(movf.getCdestadomov());// enviandose
    mov.setCdindcotizacion(dt.getCdindcotizacion());
    mov.setCdrefinternaasig(movf.getCdrefinternaasig());
    mov.setCdrefmovimiento(movf.getCdrefmovimiento());
    mov.setCdrefmovimientoecc(movf.getCdrefmovimientoecc());
    mov.setCdrefnotificacion(movf.getCdrefnotificacion());
    mov.setCdrefnotificacionprev(movf.getCdrefnotificacionprev());
    mov.setCdtipomov(movf.getCdtipomov());

    mov.setCodigoReferenciaAsignacion(dt.getCodigoReferenciaAsignacion() == null ? "" : dt
        .getCodigoReferenciaAsignacion());
    mov.setCuentaCompensacionDestino(dt.getCuentaCompensacionDestino() == null ? "" : dt.getCuentaCompensacionDestino());
    mov.setImefectivo(BigDecimal.ZERO);
    mov.setImputacionPropia('N');
    mov.setImtitulos(BigDecimal.ZERO);
    mov.setIsin(dt.getCdisin());
    mov.setMiembroCompensadorDestino(dt.getMiembroCompensadorDestino() == null ? "" : dt.getMiembroCompensadorDestino());
    mov.setSentido(dt.getCdsentido());
    mov.setFechaLiquidacion(dt.getTmct0desglosecamara().getFeliquidacion());
  }

  public void inizializeTmct0movimientoEcc(Tmct0ejecucionalocation ejealo, Tmct0operacioncdecc op,
      Tmct0infocompensacion ic, String cdrefmovimiento, String cdrefinternaasig, Tmct0movimientoecc mov) {
    mov.setAuditFechaCambio(new Date());

    mov.setAuditUser("MOV");
    mov.setCdestadomov("20");// enviandose
    mov.setCdindcotizacion(op.getCdindcotizacion());
    mov.setCdrefinternaasig(cdrefinternaasig);
    mov.setCdrefmovimiento(cdrefmovimiento);
    mov.setCdrefmovimientoecc("");
    mov.setCdrefnotificacion("");
    mov.setCdrefnotificacionprev("");
    if (StringUtils.isNotBlank(ic.getCdctacompensacion())) {
      mov.setCdtipomov("15");// asignacion interna
    }
    else {
      mov.setCdtipomov("16");// asignacion externa
    }

    mov.setCodigoReferenciaAsignacion(ic.getCdrefasignacion() == null ? "" : ic.getCdrefasignacion());
    mov.setCuentaCompensacionDestino(ic.getCdctacompensacion() == null ? "" : ic.getCdctacompensacion());
    mov.setImefectivo(BigDecimal.ZERO);
    mov.setImputacionPropia('N');
    mov.setImtitulos(BigDecimal.ZERO);
    mov.setIsin(op.getCdisin());
    mov.setMiembroCompensadorDestino(ic.getCdmiembrocompensador() == null ? "" : ic.getCdmiembrocompensador());
    mov.setSentido(op.getCdsentido());
    mov.setFechaLiquidacion(op.getFliqteorica());
  }

  public void inizializeTmct0movimientoEccS3(Tmct0anotacionecc op, String cdrefmovimiento, String cdrefinternaasig,
      String cuentaCompensacion, Tmct0movimientoecc mov) {
    mov.setAuditFechaCambio(new Date());

    mov.setAuditUser("MOV");
    mov.setCdestadomov("20");// enviandose
    mov.setCdindcotizacion(op.getCdindcotizacion());
    mov.setCdrefinternaasig(cdrefinternaasig);
    mov.setCdrefmovimiento(cdrefmovimiento);
    mov.setCdrefmovimientoecc("");
    mov.setCdrefnotificacion("");
    mov.setCdrefnotificacionprev("");
    mov.setCdtipomov("15");// asignacion interna
    mov.setCodigoReferenciaAsignacion("");
    mov.setCuentaCompensacionDestino(cuentaCompensacion);
    mov.setImefectivo(BigDecimal.ZERO);
    mov.setImputacionPropia('N');
    mov.setImtitulos(BigDecimal.ZERO);
    mov.setIsin(op.getCdisin());
    mov.setMiembroCompensadorDestino("");
    mov.setSentido(op.getCdsentido());
    mov.setFechaLiquidacion(op.getFliqteorica());

  }

  public String getNewMOCdrefmovimientoFromMOSequence() {
    return getNewMOCdrefmovimiento(new Long(dao.getNewMOPTISequence().toString()));
  }

  public String getNewMOCdrefmovimiento(Long sequence) {
    if (sequence == null) {
      sequence = new Long(dao.getNewMOPTISequence().toString());
    }
    String refMovbase36 = Long.toString(sequence, Character.MAX_RADIX);
    return "MO" + refMovbase36;
  }

  public List<Object[]> findDistinctIdDesglose() {
    return daoImpl.findDistinctIdDesglose();
  }

  public List<Object[]> findMismoIdDesgloseDistintosValoresError() {

    return daoImpl.findMismoIdDesgloseDistintosValoresError();
  }

  public BigDecimal getSumNutitulosByIddesglosecamaraAndInCdestadomov(Long iddesglosecamara, List<String> cdestadomov) {
    return dao.getSumNutitulosByIddesglosecamaraAndInCdestadomov(iddesglosecamara, cdestadomov);
  }

  public BigDecimal getSumNutitulosByIddesglosecamaraAndCdestadomov(Long iddesglosecamara, String cdestadomov) {
    return dao.getSumNutitulosByIddesglosecamaraAndCdestadomov(iddesglosecamara, cdestadomov);
  }

  public BigDecimal getSumNutitulosByTmct0alcIdAndInCdestadomov(Tmct0alcId alcId, List<String> cdestadomov) {
    return dao.getSumNutitulosByTmct0alcIdAndInCdestadomov(alcId, cdestadomov);
  }

  public boolean isMovimientoAlta(Tmct0movimientoecc mov) {
    return mov.getCdestadomov() != null && !mov.getCdestadomov().trim().equals("21")
        && !mov.getCdestadomov().trim().equals("5") && !mov.getCdestadomov().trim().equals("12")
        || mov.getCdestadoasig() != null && mov.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId();
  }

  @Transactional
  public Tmct0movimientoecc crearMovimientosOrigenDestinoIgual(Tmct0desglosecamara dc, Tmct0desgloseclitit dt,
      Tmct0infocompensacion ic, String auditUser) {
    List<Tmct0movimientoeccFidessa> listMovF;
    LOG.debug("[crearMovimientosOrigenDestinoIgual] Calculando si se debe crear los movimientosecc...");
    if (StringUtils.isNotBlank(dt.getCuentaCompensacionDestino())
        && StringUtils.equals(dt.getCuentaCompensacionDestino(), dc.getTmct0infocompensacion().getCdctacompensacion())
        || StringUtils.isNotBlank(dt.getMiembroCompensadorDestino())
        && StringUtils.equals(dt.getMiembroCompensadorDestino(), dc.getTmct0infocompensacion()
            .getCdmiembrocompensador())
        && StringUtils.equals(dt.getCodigoReferenciaAsignacion(), dc.getTmct0infocompensacion().getCdrefasignacion())) {
      LOG.debug("[crearMovimientosOrigenDestinoIgual] Creando movimientoecc y dejandolo en '9'-Pdte.Liquidar...");
      Tmct0movimientoecc mov = new Tmct0movimientoecc();
      listMovF = movfBo.findAllByCdoperacionecc(dt.getCdoperacionecc());
      mov.setTmct0desglosecamara(dc);
      if (!listMovF.isEmpty()) {
        inizializeTmct0movimientoEcc(dt, listMovF.get(0), mov, auditUser);
      }
      else {
        inizializeTmct0movimientoEcc(dt, mov, auditUser);
        mov.setCdrefmovimientoecc(mov.getCdrefmovimiento());
        mov.setCdrefnotificacion(mov.getCdrefmovimiento());
        mov.setCdestadomov("9");
      }

      mov.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECIBIENDO_ASIGNACIONES.getId());
      LOG.debug("[crearMovimientosOrigenDestinoIgual] Creando movimientooperacionnueva...");
      Tmct0movimientooperacionnuevaecc movn = new Tmct0movimientooperacionnuevaecc();
      movn.setTmct0movimientoecc(mov);
      movnBo.inizializeMovimientoOperacioNueva(dt, BigDecimal.ZERO, movn);
      if (!listMovF.isEmpty()) {
        movn.setIdMovimientoFidessa(listMovF.get(0).getIdMovimientoFidessa());
      }
      // if (dt.getCdoperacionecc() != null &&
      // !dt.getCdoperacionecc().trim().isEmpty()) {
      // movnBo.engancharConMovFidessa(movn);
      // }
      movn.setAuditUser(auditUser);
      movn.setTmct0desgloseclitit(dt);
      movn.setAuditUser(auditUser);
      mov.setImefectivo(mov.getImefectivo().add(movn.getImefectivo()));
      mov.setImtitulos(mov.getImtitulos().add(movn.getImtitulos()));
      mov.getTmct0movimientooperacionnuevaeccs().add(movn);
      dc.getTmct0movimientos().add(mov);
      return mov;

    }
    return null;
  }

//  @Transactional
//  public List<Tmct0movimientoecc> crearMovimientosOrigenDestinoIgual(Tmct0desglosecamara dc, String auditUser) {
//    return this.crearMovimientosOrigenDestinoIgual(dc, dc.getTmct0desgloseclitits(), auditUser);
//  }

  @Transactional
  public List<Tmct0movimientoecc> crearMovimientosOrigenDestinoIgual(Tmct0desglosecamara dc,
      List<Tmct0desgloseclitit> listdcts, String auditUser) {
    Tmct0movimientoecc mov;
    List<Tmct0movimientoecc> res = new ArrayList<Tmct0movimientoecc>();
    for (Tmct0desgloseclitit dt : listdcts) {
      mov = this.crearMovimientosOrigenDestinoIgual(dc, dt, dc.getTmct0infocompensacion(), auditUser);
      if (mov != null) {
        res.add(mov);
      }
    }
    return res;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.MANDATORY)
  public List<String> escribirTmct0logForMovimientos(Tmct0alcId alcId,
      List<Tmct0movimientoecc> movimientosEccDefinitivos, String msgOrden, String msgEnvio, String auditUser) {
    String msg;
    String camara;
    String ctaDestino = null;
    String refGiveUp = null;
    String miembroCompensador = null;
    List<String> listMsgs = new ArrayList<String>();
    if (movimientosEccDefinitivos != null && !movimientosEccDefinitivos.isEmpty()) {
      Map<String, BigDecimal> titulosByCamara = new HashMap<String, BigDecimal>();

      ctaDestino = movimientosEccDefinitivos.get(0).getCuentaCompensacionDestino();
      miembroCompensador = movimientosEccDefinitivos.get(0).getMiembroCompensadorDestino();
      refGiveUp = movimientosEccDefinitivos.get(0).getCodigoReferenciaAsignacion();

      for (Tmct0movimientoecc movFor : movimientosEccDefinitivos) {
        if (movFor.getTmct0desglosecamara() != null
            && movFor.getTmct0desglosecamara().getTmct0CamaraCompensacion() != null) {
          camara = movFor.getTmct0desglosecamara().getTmct0CamaraCompensacion().getCdCodigo().trim();
        }
        else {
          camara = "UNKNOWN";
        }

        if (titulosByCamara.get(camara) == null) {
          titulosByCamara.put(camara, BigDecimal.ZERO);
        }
        titulosByCamara.put(camara, titulosByCamara.get(camara).add(movFor.getImtitulos()));
      }
      for (String camaraFor : titulosByCamara.keySet()) {
        if (StringUtils.isBlank(ctaDestino)) {
          msg = String.format("%s de '%s' titulos de la camara: '%s' al miembro: '%s' ref.give-up: '%s'", msgOrden,
              titulosByCamara.get(camaraFor).setScale(2, RoundingMode.HALF_UP), camaraFor, miembroCompensador,
              refGiveUp);
        }
        else {
          msg = String.format("%s de '%s' titulos de la camara: '%s' a la Cta: '%s'", msgOrden,
              titulosByCamara.get(camaraFor).setScale(2, RoundingMode.HALF_UP), camaraFor, ctaDestino);
        }
        LOG.trace("[sendMensajesTraspaso] grabando registro en tmct0log con informacion {}...", msg);
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

        msg = String.format("'%s/%s/%s/%s' %s", alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
            alcId.getNucnfliq(), msg);
        LOG.debug("[escribirTmct0logForMovimientos] {}", msg);
        listMsgs.add(msg);
      }
      titulosByCamara.clear();

      for (Tmct0movimientoecc movFor : movimientosEccDefinitivos) {
        msg = String.format("%s ref.mov: '%s' ref.interna: '%s' titulos: '%s' ops: ", msgEnvio,
            movFor.getCdrefmovimiento(), movFor.getCdrefinternaasig(),
            movFor.getImtitulos().setScale(2, RoundingMode.HALF_UP));
        for (Tmct0movimientooperacionnuevaecc movNFor : movFor.getTmct0movimientooperacionnuevaeccs()) {
          msg = String.format("%s '%s'", msg, movNFor.getCdoperacionecc());
        }
        logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

        msg = String.format("'%s/%s/%s/%s' %s", alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(),
            alcId.getNucnfliq(), msg);
        LOG.debug("[escribirTmct0logForMovimientos] {}", msg);
      }
    }

    return listMsgs;

  }

}
