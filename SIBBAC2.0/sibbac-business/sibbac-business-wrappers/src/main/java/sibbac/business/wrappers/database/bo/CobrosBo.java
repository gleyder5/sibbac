package sibbac.business.wrappers.database.bo;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.dao.CobrosDao;
import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.dao.LineaDeCobroDao;
import sibbac.business.wrappers.database.dao.TextoDao;
import sibbac.business.wrappers.database.dao.specifications.CobrosSpecificacions;
import sibbac.business.wrappers.database.dto.CobroDTO;
import sibbac.business.wrappers.database.dto.LineaCobroDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Cobros;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.business.wrappers.database.model.LineaDeCobro;
import sibbac.business.wrappers.database.model.Monedas;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.business.wrappers.database.model.TextosIdiomas;
import sibbac.business.wrappers.database.model.TipoDeDocumento;
import sibbac.business.wrappers.rest.SIBBACServiceCobros;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.currency.CurrencyConverter;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.JobPK;
import sibbac.webapp.beans.WebRequest;


@Service
public class CobrosBo extends AbstractBo< Cobros, Long, CobrosDao > {

	protected static final Logger			LOG	= LoggerFactory.getLogger( CobrosBo.class );

	@Autowired
	private AliasBo							aliasBo;

	@Autowired
	private FacturaBo						facturaBO;

	@Autowired
	private NumeradoresBo					numeradoresBo;

	@Autowired
	private PeriodosBo						periodosBo;

	@Autowired
	private MonedasBo						monedaBo;

	@Autowired
	private TipoDeDocumentoBo				tipoDeDocumentoBo;

	@Autowired
	private Tmct0estadoBo					estdoBo;

	@Autowired
	private LineaDeCobroBo					lineaDeCobroBo;

	@Autowired
	private LineaDeCobroDao					lineaDeCobroDao;

	@Autowired
	private SendMail						sendMail;

	@Autowired
	private CobrosDao						cobrosDao;

	@Autowired
	private TextoDao						textoDao;

	@Autowired
	private RecordatorioFacturaPendienteBo	recordatorioFacturaPendienteBo;

	@Autowired
	private EtiquetasBo						etiquetasBo;

	@Autowired
	private EtiquetasDao					etiquetasDao;
	@Autowired
	private GestorServicios					gestorServicios;

	/**
	 * Recupera los datos de la tabla Cobros y devuelve una lista de cobrosDTO
	 * 
	 * @param map
	 *
	 * @return
	 * @throws SIBBACBusinessException
	 */
	public List< CobroDTO > getListado( Map< String, String > filters ) throws SIBBACBusinessException {
		// Extractamos la información de filtrado.

		String idAlias = null;
		String fechaDesde = null;
		String fechaHasta = null;
		String numCobro = null;

		if ( filters != null && !filters.isEmpty() ) {
			idAlias = filters.get( SIBBACServiceCobros.FILTER_ID_ALIAS );
			fechaDesde = filters.get( SIBBACServiceCobros.FILTER_FECHA_DESDE );
			fechaHasta = filters.get( SIBBACServiceCobros.FILTER_FECHA_HASTA );
			numCobro = filters.get( SIBBACServiceCobros.FILTER_NUMERO_COBRO );

			// Validamos
			idAlias = ( idAlias != null && !idAlias.isEmpty() ) ? idAlias : null;
			fechaDesde = ( fechaDesde != null && !fechaDesde.isEmpty() ) ? fechaDesde : null;
			fechaHasta = ( fechaHasta != null && !fechaHasta.isEmpty() ) ? fechaHasta : null;
			numCobro = ( numCobro != null && !numCobro.isEmpty() ) ? numCobro : null;
		}

		LOG.trace( " Llamada al metodo getLista del cobrosBO " );
		List< Cobros > entities = ( List< Cobros > ) this.dao.findAll( CobrosSpecificacions.listadoFiltrado(
				FormatDataUtils.convertStringToLong( idAlias ), // por alias
				FormatDataUtils.convertStringToDate( fechaDesde ), // por fecha desde
				FormatDataUtils.convertStringToDate( fechaHasta ), // por fecha hasta
				FormatDataUtils.convertStringToLong( numCobro ) ) ); // por numero de cobro.
		List< CobroDTO > resultList = new LinkedList< CobroDTO >();

		for ( Cobros entity : entities ) {
			resultList.add( new CobroDTO( entity ) );
		}
		return resultList;
	}

	@Transactional
	public Map< String, CobroDTO > getListaCobro( final WebRequest webRequest ) throws SIBBACBusinessException {

		LOG.trace( " Se inicia el metodo getListaCobro " );
		Map< String, CobroDTO > resultados = new HashMap< String, CobroDTO >();
		List< CobroDTO > listaCobros = ( List< CobroDTO > ) this.getListado( webRequest.getFilters() );

		for ( CobroDTO cobroDTO : listaCobros ) {
			resultados.put( cobroDTO.getId().toString(), cobroDTO );
		}

		return resultados;
	}

	/**
	 * @param cobroDto
	 * @return
	 * @throws SIBBACBusinessException
	 */
	@Transactional
	public Cobros insertarCobro( final CobroDTO cobroDto ) throws SIBBACBusinessException {
		final String prefix = "[cobrosBo::insertarCobro] ";
		LOG.trace( prefix + "Se inicia metodo marcarCobros de CobrosBO " );

		final List< LineaCobroDTO > lineasCobrosDto = cobroDto.getLineasCobroDto();
		final Long idAlias = cobroDto.getIdAlias();
		final Alias alias = aliasBo.findById( idAlias );
		if ( alias == null ) {
			LOG.error( prefix + "No se ha encontrado el alias [{}] ", idAlias );
			throw new SIBBACBusinessException( "No se ha encontrado el alias con identificador " + idAlias );
		}
		final Date fechaContabilizacion = cobroDto.getFechaContabilizacion();
		final BigDecimal importeCobrado = cobroDto.getImporteCobrado();

		final Monedas moneda = monedaBo.findByCodigo( Monedas.COD_MONEDA_EUR );
		final TipoDeDocumento tipoDeDocumento = tipoDeDocumentoBo.getTipoDeDocumento( TipoDeDocumento.COBRO );
		final Periodos periodo = periodosBo.getPeriodosActivo();
		final Identificadores identificador = periodo.getIdentificador();
		final Long numerador = numeradoresBo.calcularNumeroSiguiente( tipoDeDocumento, identificador );
		final Tmct0estado estadoCobradoParcialmente = estdoBo.findByIdFactura( EstadosEnumerados.FACTURA.COBRADO_PARCIALMENTE );
		final Tmct0estado estadoCobradoTotalmente = estdoBo.findByIdFactura( EstadosEnumerados.FACTURA.COBRADO_TOTALMENTE );
		Cobros cobro = new Cobros();
		cobro.setAlias( alias );
		cobro.setContabilizado( Boolean.FALSE );
		cobro.setImporte( importeCobrado );
		cobro.setFecha( fechaContabilizacion );
		cobro.setMoneda( moneda );
		cobro.setTipoDeDocumento( tipoDeDocumento );
		cobro.setCobro( numerador );
		if ( CollectionUtils.isNotEmpty( lineasCobrosDto ) ) {
			BigDecimal importeTotalAplicado = BigDecimal.ZERO;
			for ( final LineaCobroDTO lineaCobroDto : lineasCobrosDto ) {
				final Long idFactura = lineaCobroDto.getIdFactura();
				final Factura factura = facturaBO.findById( idFactura );
				if ( factura == null ) {
					LOG.debug( prefix + "No se ha recuperado información de la factura con identificador [{}] ", idFactura );
					throw new SIBBACBusinessException( "No se ha recuperado información de la factura con identificador [" + idFactura
							+ "]" );
				}
				final BigDecimal importeAplicado = lineaCobroDto.getImporteAplicado();

				final BigDecimal importePendiente = factura.getImportePendiente();
				if ( importeAplicado.compareTo( importePendiente ) > 0 ) {
					LOG.debug( prefix + "Se ha aplicado un importe demasiado alto para la factura [{}]", factura.getNumeroFactura() );
					throw new SIBBACBusinessException( "Se ha aplicado un importe demasiado alto para la factura ["
							+ factura.getNumeroFactura() + "]" );
				}
				final LineaDeCobro lineaDeCobro = new LineaDeCobro();
				lineaDeCobro.setImporteAplicado( importeAplicado );
				cobro.addLineaCobro( lineaDeCobro );
				factura.addLineaDeCobro( lineaDeCobro );
				importeTotalAplicado = importeTotalAplicado.add( importeAplicado );
				final BigDecimal importePagado = factura.getImportePagado();
				final BigDecimal importeTotal = factura.getImporteTotal();
				if ( importeTotal.compareTo( importePagado ) == 0 ) {
					factura.setEstado( estadoCobradoTotalmente );
				} else if ( importeTotal.compareTo( importePagado ) > 0 ) {
					boolean cierreFactura = lineaCobroDto.isCierreFactura();
					if ( cierreFactura ) {
						factura.setEstado( estadoCobradoTotalmente );
					} else {
						factura.setEstado( estadoCobradoParcialmente );
					}
				}
			}
			if ( importeTotalAplicado.compareTo( importeCobrado ) > 0 ) {
				LOG.debug( prefix + "Se ha aplicado un importe demasiado alto para el cobro" );
				throw new SIBBACBusinessException( "El importe total aplicado inferior a la suma de los importes aplicados a las facturas" );
			} else {
				final BigDecimal comisiones = importeCobrado.subtract( importeTotalAplicado );
				cobro.setComisiones( comisiones );
			}
			cobro = dao.save( cobro );

		} else {

			LOG.error( prefix + "No se ha detallado la información del cobro" );
			throw new SIBBACBusinessException( "No se ha detallado la información del cobro" );
		}
		return cobro;
	}

	public List< Cobros > findByFechaEnvioMailNull() {
		final String prefix = "[cobrosBo::findByFechaEnvioMailNull] ";
		try {
			return this.dao.findByFechaEnvioMailNull();
		} catch ( Exception e ) {
			LOG.warn( prefix + "ERROR({}): Error recuperando cobros. Hay? [{}]", e.getClass().getName(), e.getMessage() );
			return null;
		}
	}

	/**
	 * @throws SIBBACBusinessException
	 * 
	 */
	@Transactional
	public List< String > sendMailCobros( final JobPK jobPk ) {
		final String prefix = "[cobrosBo::sendMailCobros] ";
		List< String > messages = new ArrayList< String >();
		// Buscamos aquellos cobros que aun no han sido enviados: sin fecha de envio de correo.
		List< Cobros > cobrosPendientes = this.findByFechaEnvioMailNull();

		if ( cobrosPendientes == null || cobrosPendientes.isEmpty() ) {
			messages.add( "No se han encontrado cobros pendientes de enviar." );
		} else {
			for ( Cobros cobro : cobrosPendientes ) {
				try {
					messages.add( this.sendMailDeCobro( jobPk, cobro ) );
				} catch ( SIBBACBusinessException e ) {
					messages.add( "Error enviando el mail al cobro: " + cobro.getId() + ": " + e.getMessage() );
					LOG.error( prefix + "Error enviando el mail al cobro: " + cobro.getId() + ": " + e.getMessage() );
				}
			}
		}
		return messages;
	}

	@Transactional
	public String sendMailDeCobro( final JobPK jobPk, final Cobros cobro ) throws SIBBACBusinessException {
		final String prefix = "[cobrosBo::sendMailDeCobro] ";
		String subject = "";
		String body = "";

		LOG.trace( prefix + "Mappings..." );
		Map< String, String > cuerpoMapping = createMappingCuerpoMail( cobro );

		Alias alias = null;
		Long idAlias = null;

		try {
			alias = cobro.getAlias();
			LOG.trace( prefix + "> GOT Alias..." );
			idAlias = alias.getId();
			LOG.trace( prefix + "> GOT IDAlias [{}]...", idAlias );
		} catch ( Exception e ) {
			throw new SIBBACBusinessException( "No alias asociado al cobro(" + cobro.getId() + ")!: [" + e.getMessage() + "]", e );
		}
		LOG.trace( prefix + "ID Alias: [{}]", idAlias );

		LOG.trace( prefix + "Buscando el subject y el body..." );
		subject = this.getEtiqueta( "B", idAlias );
		body = this.getEtiqueta( "D", idAlias );

		if ( subject == null || subject.isEmpty() ) {
			throw new SIBBACBusinessException( "No subject!" );
		}

		if ( body == null || body.isEmpty() ) {
			throw new SIBBACBusinessException( "No body!" );
		}

		LOG.trace( prefix + "Preparando los textos..." );
		subject = reemplazarKeys( subject, cuerpoMapping );
		body = reemplazarKeys( body, cuerpoMapping );
		LOG.trace( prefix + "Subject: [{}]", subject );
		LOG.trace( prefix + "Body: [{}]", body );

		String literalNumeroFactura = "";
		LOG.trace( prefix + "Rellenando mas informacion..." );
		literalNumeroFactura = this.getEtiqueta( "M", idAlias );
		if ( literalNumeroFactura == null || literalNumeroFactura.isEmpty() ) {
			throw new SIBBACBusinessException( "No etiqueta \"M\"!" );
		}
		literalNumeroFactura = literalNumeroFactura + ": ";
		LOG.trace( prefix + "+ [literalNumeroFactura=={}]", literalNumeroFactura );

		String literalImporteAplicado = "";
		literalImporteAplicado = this.getEtiqueta( "V", idAlias );
		if ( literalImporteAplicado == null || literalImporteAplicado.isEmpty() ) {
			throw new SIBBACBusinessException( "No etiqueta \"V\"!" );
		}
		LOG.trace( prefix + "+ [literalImporteAplicado=={}]", literalImporteAplicado );

		String numFactura = "";
		String impAplicado = "";
		String numFacturaBody = "";
		String impAplicadoBody = "";
		String emailDelContacto = "";
		List< String > destinatarios = null;

		LOG.trace( prefix + "Recuperando las facturas..." );
		for ( final Factura factura : cobro.getFacturas() ) {
			numFactura = factura.getNumeroFactura().toString();
			numFacturaBody += literalNumeroFactura + numFactura + "\n";
			LOG.trace( prefix + "+ [numFactura=={}]", numFactura );
			LOG.trace( prefix + "+ [numFacturaBody=={}]", numFacturaBody );
		}
		LOG.trace( prefix + "Recuperando las lineas de cobro..." );
		for ( LineaDeCobro linea : cobro.getLineasDeCobro() ) {
			impAplicado = CurrencyConverter.convertToEuro( linea.getImporteAplicado() );
			impAplicadoBody += literalImporteAplicado + impAplicado + "\n";
			LOG.trace( prefix + "+ [impAplicado=={}]", impAplicado );
			LOG.trace( prefix + "+ [impAplicadoBody=={}]", impAplicadoBody );
		}

		body += "\n" + "\n" + numFacturaBody + impAplicadoBody + "\n";
		LOG.trace( prefix + "BODY: {}", body );

		LOG.trace( prefix + "Recuperando el contacto por defecto..." );
		Contacto contactoPorDefecto = cobro.getContactoPorDefecto();
		if ( contactoPorDefecto == null ) {
			throw new SIBBACBusinessException( "No hay contacto!" );
		}
		LOG.trace( prefix + "+ [contacto=={}]", contactoPorDefecto );

		emailDelContacto = contactoPorDefecto.getEmail();
		LOG.trace( prefix + "+ [emailDelContacto=={}]", emailDelContacto );
		if ( emailDelContacto == null ) {
			LOG.warn( prefix + "No hay emailDelContacto del contacto!" );
		} else {
			// El mail del contacto puede ser, en realidad, varios separados por ";".
			destinatarios = new ArrayList< String >();
			for ( String email : emailDelContacto.split( ";" ) ) {
				destinatarios.add( email.trim() );
				LOG.trace( prefix + "  [destinatario=={}]", email.trim() );
			}
		}

		final List< Contacto > contactos = gestorServicios.getContactos( alias, jobPk );
		if ( contactos == null || contactos.isEmpty() ) {
			LOG.warn( prefix + "No hay contactos de correo para el alias [{}]!", alias.getCdaliass() );
		} else {
			destinatarios = new ArrayList< String >();
			for ( Contacto c : contactos ) {
				for ( String email : c.getEmail().split( ";" ) ) {
					destinatarios.add( email.trim() );
					LOG.trace( prefix + "  [destinatario=={}]", email.trim() );
				}
				LOG.trace( prefix + "  [destinatario (contacto)=={}]", c.getEmail() );
			}
		}

		LOG.trace( prefix + "Enviando el correo..." );
		try {
			Date ahora = new Date();
			sendMail.sendMail( destinatarios, subject, body );
			LOG.trace( prefix + "Correo enviado." );

			// Seteamos la fecha de envio.
			cobro.setFechaEnvioMail( ahora );
		} catch ( IllegalArgumentException | MessagingException | IOException e ) {
			throw new SIBBACBusinessException( "ERROR(" + e.getClass().getName() + "): [" + e.getMessage() + "]", e );
		}

		return "EMail enviado correctamente a: " + destinatarios;
	}

	public Map< String, String > createMappingCuerpoMail( Cobros cobro ) {
		String key = null;
		String value = null;
		Map< String, String > mapping = new HashMap< String, String >();
		try {
			List< Etiquetas > etiquetas = ( List< Etiquetas > ) etiquetasDao.findAllByTabla( "Cobros" );
			for ( Etiquetas etiqueta : etiquetas ) {
				key = etiqueta.getEtiqueta();
				String campo = etiqueta.getCampo();
				switch ( campo ) {
					case "Cobro":
						value = cobro.getCobro().toString();
						break;
					case "Importe":
						value = CurrencyConverter.convertToEuro( cobro.getImporte() );
						break;
					case "Comisiones":
						value = CurrencyConverter.convertToEuro( cobro.getComisiones() );
						break;
					default:
						value = "<No data>";
				}
				mapping.put( key, value );
			}
		} catch ( Exception e ) {
			// Nothing
		}
		return mapping;
	}

	public String reemplazarKeys( String body, Map< String, String > mapping ) {
		Set< String > keys = mapping.keySet();
		String value = null;
		for ( String key : keys ) {
			value = mapping.get( key );
			body = body.replace( key, value );
		}
		return body;
	}

	public String getEtiqueta( String tipoTexto, Long idAlias ) {
		String prefix = "[CobrosBo::getEtiqueta] ";
		String etiqueta = null;
		LOG.trace( prefix + "Buscando la etiqueta: [{}]", tipoTexto );
		Texto texto = textoDao.findByTipo( tipoTexto );
		if ( texto != null ) {
			TextosIdiomas textosIdiomas = recordatorioFacturaPendienteBo.getTextosidiomas( idAlias, texto );
			if ( textosIdiomas != null ) {
				etiqueta = textosIdiomas.getDescripcion();
			} else {
				LOG.warn( prefix + "ERROR intentando encontrar los textos para la etiqueta: [{}]", tipoTexto );
			}
		} else {
			LOG.warn( prefix + "ERROR intentando encontrar la etiqueta: [{}]", tipoTexto );
		}
		return etiqueta;
	}

}
