/**
 * 
 */
package sibbac.business.wrappers.database.dao;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.model.Tmct0MovimientoManual;


/**
 * Dao de movimiento manual
 * 
 * @author Cristina
 *
 */
public interface Tmct0MovimientoManualDao extends JpaRepository< Tmct0MovimientoManual, Long > {

	/***
	 * Funcion que devuelve los movimientosManual que tienen el flag "procesadoCV" a FALSE o a NULL
	 * 
	 * @return
	 */
	@Query( value = "SELECT o FROM Tmct0MovimientoManual o" + " WHERE o.procesadoCv = '0' OR o.procesadoCv IS NULL " )
	List< Tmct0MovimientoManual > findAllNoTratadosACuentaVirtual();

	@Modifying
	@Query( "UPDATE Tmct0SaldoInicialCuenta saldo "
			+ "SET saldo.titulos = saldo.titulos + :newtitulos, saldo.imefectivo = saldo.imefectivo + :newefectivo "
			+ "WHERE saldo.isin = :isin AND saldo.cdaliass = :cdaliass AND saldo.cdcodigocuentaliq = :cdcodigocuentaliq AND "
			+ "saldo.fecha> :fechahasta" )
	public Integer updateSaldosFromMovManual( @Param( "newtitulos" ) BigDecimal newtitulos, @Param( "newefectivo" ) BigDecimal newefectivo,
			@Param( "isin" ) String isin, @Param( "cdaliass" ) String cdaliass, @Param( "cdcodigocuentaliq" ) String cdcodigocuentaliq,
			@Param( "fechahasta" ) Date fechahasta );
}
