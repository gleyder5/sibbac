package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.AlcOrdenMercadoDao;
import sibbac.business.wrappers.database.dao.AlcOrdenesDao;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dto.AlcOrdenesDTO;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class AlcOrdenesBo extends AbstractBo<AlcOrdenes, Long, AlcOrdenesDao> {
  
  private static final Logger LOG = LoggerFactory.getLogger(AlcOrdenesBo.class);

  @Autowired
  private FacturaDao facturaBo;
  
  @Autowired
  private Tmct0alcBo alcBo;
  
  @Autowired
  private AlcOrdenMercadoDao alcOrdenMercadoDao;

  public AlcOrdenes findByTmct0alc(Tmct0alc tmct0alc) {
    final Tmct0alcId id;
    
    id = tmct0alc.getId();
    return dao.findByTmct0alc(id.getNuorden(), id.getNbooking(), id.getNucnfclt(), id.getNucnfliq());
  }
  
  public List<AlcOrdenes> findByNetting(Netting netting) {
    return dao.findByNetting(netting);
  }

  public Map<String, AlcOrdenesDTO> getListaOrdenesFactura(Map<String, String> params) {
    LOG.trace(" Se inicia el metodo getListaAlcOrdenes ");
    Map<String, AlcOrdenesDTO> resultados = new HashMap<String, AlcOrdenesDTO>();
    Long facturaId = NumberUtils.createLong(params.get(Constantes.ORDENESFACTURA_PARAM_FACTURA_ID));
    List<AlcOrdenesDTO> listaAlcOrdenes = (List<AlcOrdenesDTO>) this.getLista(facturaId);
    for (AlcOrdenesDTO alcOrdenesDTO : listaAlcOrdenes) {
      resultados.put(alcOrdenesDTO.getId().toString(), alcOrdenesDTO);
    }
    LOG.trace("Se ha creado la lista de AlcOrdenes");
    return resultados;
  }

  @Transactional
  public List<AlcOrdenesDTO> getLista(Long facturaId) {
    LOG.trace(" Llamada al metodo getLista del contactoBO ");
    Factura factura = facturaBo.findOne(facturaId);
    List<AlcOrdenes> entities = (List<AlcOrdenes>) factura.getAlcOrdenes();
    List<AlcOrdenesDTO> resultList = new LinkedList<AlcOrdenesDTO>();
    for (AlcOrdenes entity : entities) {
      AlcOrdenesDTO dto = entityAlcOrdenesToDTO(entity);
      resultList.add(dto);
    }
    return resultList;
  }

  public AlcOrdenesDTO entityAlcOrdenesToDTO(AlcOrdenes entity) {
    final AlcOrdenesDTO dto;
    final Tmct0alcId alcId;
    final Tmct0alc alc;
      
    LOG.trace(" Llamada al metodo entityAlcOrdenesToDTO del AlcOrdenesBO ");
    dto = new AlcOrdenesDTO();
    if (entity != null) {
      dto.setId(entity.getId());
      alcId = entity.createAlcIdFromFields();
      alc = alcBo.findById(alcId);
      if(alc != null) {
          dto.setnBooking(alc.getTmct0alo().getId().getNbooking());
          dto.setnOrden(alc.getTmct0alo().getId().getNuorden());
          dto.setnUcnfclt(alc.getTmct0alo().getId().getNucnfclt());
          dto.setCdrefban(alc.getCdrefban());
          dto.setImnetliq(alc.getImnetliq());
          dto.setImtotnet(alc.getImtotnet());
          dto.setImtotbru(alc.getImtotbru());
          dto.setImcomisn(alc.getImfinsvb()); // FIXME Se cambia la imcomisn por la el imfinsvb, aunque no se toca el nombre del atributo ...
          dto.setCdordtit(alc.getCdordtit());
      }
    }
    return dto;
  }

  public AlcOrdenes findByReferenciaS3(String referencia_s3) {
    return dao.findByReferenciaS3(referencia_s3);
  }
  
  public List<BigInteger> findByNuOrden(String nuOrden) {
        return dao.findByNuOrden(nuOrden);
  }
  
  public List<Object[]> findByNuOrden(String nuOrden, Integer idDesgloseCamara) {
      return dao.findByNuOrden(nuOrden, idDesgloseCamara);
  }

  public Integer countByNuOrden(String nuOrden, List<Long> idsOrdenes) {
      return dao.countByNuOrden(nuOrden, idsOrdenes);
  }
  
  public void update(Character estado, Date auditDate, String auditUser, BigDecimal titulosAsignados, Long id) {
      dao.update(estado, auditDate, auditUser, titulosAsignados, id);
  }

  public void update(BigDecimal titulosEnviados, Long id) {
      dao.update(titulosEnviados, id);
  }

  public void update(String referenciaS3, Long id) {
      dao.update(referenciaS3, id);
  }
  
  public void insert(Integer idEstado, String nuOrden, String cuentaCompensacionDestino, BigDecimal titEnviados, BigDecimal titAsignados) {
      dao.insert(idEstado, nuOrden, cuentaCompensacionDestino, titEnviados, titAsignados);
  }
  
  public List<AlcOrdenes> findPatasMercadoPorOrden(String nuorden) {
      return dao.findPatasMercadoPorOrden(nuorden);
  }

  public List<String> findDistinctCuentaCompensacionDestino() {
    return dao.findDistinctCuentaCompensacionDestino();
  }
  
  public String referenciaS3porDesglose(long idDesgloseCamara) throws SIBBACBusinessException {
    final List<String> referenciasS3;
    final String notFoundedMessage;
    
    referenciasS3 = alcOrdenMercadoDao.referenciaS3activa(idDesgloseCamara);
    if(referenciasS3.isEmpty()) {
      notFoundedMessage = MessageFormat.format("Se ha buscado referencia S3 para {0} sin resultaods", idDesgloseCamara);
      LOG.error("[referenciaS3porDesglose] {}", notFoundedMessage);
      throw new SIBBACBusinessException(notFoundedMessage);
    }
    if(referenciasS3.size() > 1) {
      LOG.info("[referenciaS3porDesglose] Se han encontrado {} referencias S3 para {}. Solo se regresa la primera.",
          referenciasS3.size(), idDesgloseCamara);
    }
    return referenciasS3.get(0);
  }
  
}
