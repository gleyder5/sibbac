package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.IdentificadoresBo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceIdentificadores implements SIBBACServiceBean {

	@Autowired
	private IdentificadoresBo	identificadoresBo;

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServiceIdentificadores.class );

	public SIBBACServiceIdentificadores() {
		LOG.trace( "[SIBBACServiceIdentificadores::<constructor>] Initiating SIBBAC Service for \"Identificadores\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {

		LOG.trace( "[SIBBACServiceIdentificadores::process] Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.trace( "[SIBBACServiceIdentificadores::process] Command.: [{}]", webRequest.getAction() );

		// selecciona la accion
		switch ( webRequest.getAction() ) {
			case Constantes.CMD_CREATE_IDENTIFICADORES:

				// comprueba que los parametros han sido enviados
				if ( params == null ) {
					webResponse.setError( "Por favor indique los filtros de búsqueda." );
					return webResponse;
				}

				// Hace el set en el webresponse del resultado de introducir en tabla identificador, un nuevo identificador
				webResponse.setResultados( identificadoresBo.createIdentificadores( params ) );
				LOG.trace( "Rellenado el WebResponse con el alta identificador" );

				break;

			case Constantes.CMD_GET_IDENTIFICADORES:

				// Hace el set de la devolucion de todos los identificador de la tabla identificador
				webResponse.setResultados( identificadoresBo.getListaIdentificadores() );
				LOG.trace( "Rellenado el WebResponse con la lista de identificador" );

				break;

			default:

				webResponse.setError( "No le llega ningun comando correcto" );
				LOG.error( "No le llega ningun comando correcto" );

				break;

		}

		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_CREATE_IDENTIFICADORES );
		commands.add( Constantes.CMD_GET_IDENTIFICADORES );

		return commands;
	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		fields.add( "id" );
		fields.add( "identificadores" );
		return fields;
	}

}
