package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "desgloseProcesorRunnable")
public class DesgloseProcesorRunnable extends IRunnableBean<OperacionEspecialProcesarDesgloseRecordBean> {

  /* Constantes */
  private static final Logger LOG = LoggerFactory.getLogger(DesgloseProcesorRunnable.class);

  /* Propiedades pasadas en el constructor */
  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  public DesgloseProcesorRunnable() {
    super();
  }

  @Override
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public void run(ObserverProcess observer, List<OperacionEspecialProcesarDesgloseRecordBean> beans, int order) throws SIBBACBusinessException {
    LOG.trace("[run] inicio");
    if (CollectionUtils.isNotEmpty(beans)) {
      int internalOrden = order - beans.size();
      if (desgloseRecordBeanBo != null) {
        if (CollectionUtils.isNotEmpty(beans)) {
          LOG.trace("[run] Procesando {} desgloses orden {} ", internalOrden);
          this.desgloseRecordBeanBo.procesarDesgloses(beans);
        }
      }
    }
    LOG.trace("[run] fin");
  }

  @Override
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Override
  public IRunnableBean<OperacionEspecialProcesarDesgloseRecordBean> clone() {
    return this;
  }

}
