package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.wrappers.database.data.TipoCuentaConciliacionData;
import sibbac.business.wrappers.database.model.TipoCuentaConciliacion;


public interface TipoCuentaConciliacionDao extends JpaRepository< TipoCuentaConciliacion, Long > {

	@Query( "SELECT new sibbac.business.wrappers.database.data.TipoCuentaConciliacionData(o) FROM TipoCuentaConciliacion o" )
	List< TipoCuentaConciliacionData > findAllData();
}
