package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Tmct0TiposProducto;

@Component
public interface Tmct0TiposProductoDao extends JpaRepository< Tmct0TiposProducto, Integer >{
  
  Tmct0TiposProducto findByCdCodigo(String cdCodigo);

}
