package sibbac.business.canones.bo;

import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.commons.MercadoContratacionDTO;
import sibbac.business.canones.dao.MercadoContratacionDao;
import sibbac.business.canones.model.MercadoContratacion;
import sibbac.common.FormatStyle;
import sibbac.common.StreamResult;
import sibbac.database.bo.AbstractBo;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MercadoContratacionBo extends AbstractBo<MercadoContratacion, String, MercadoContratacionDao> {

  private static final Logger LOG = getLogger(MercadoContratacionBo.class);

  @Transactional
  public void findById(String idMercadoContratacion, StreamResult streamResult) {
    final MercadoContratacion mc;
    final ObjectMapper objectMapper;
    final String message;

    mc = dao.findOne(idMercadoContratacion);
    if (mc == null) {
      streamResult.sendErrorMessage(format("No existe mercado con clave %s", idMercadoContratacion));
      return;
    }
    objectMapper = new ObjectMapper();
    try (OutputStream os = streamResult.getOutputStream(FormatStyle.JSON_OBJECTS)) {
      objectMapper.writeValue(os, mc);
    }
    catch (IOException ioex) {
      message = format("Al serializar la entidad %s", idMercadoContratacion);
      LOG.error("[findById] {}", message, ioex);
      streamResult.sendErrorMessage(message);
    }
  }

  public List<MercadoContratacion> findAllByMercadoAgrupacion(String mercadoAgrupacion) {
    return dao.findAllByMercadoAgrupacion(mercadoAgrupacion);
  }

  public Map<String, MercadoContratacionDTO> findAllMercadoContratacionDTO() {
    MercadoContratacionDTO dto;
    Map<String, MercadoContratacionDTO> res = new HashMap<String, MercadoContratacionDTO>();
    List<MercadoContratacion> l = dao.findAll();
    for (MercadoContratacion o : l) {
      dto = new MercadoContratacionDTO();
      dto.setId(o.getId().trim());
      dto.setDescripcion(o.getDescripcion());
      dto.setReglas(o.getAdmiteReglasCanon() == 'S');
      res.put(o.getId(), dto);
    }
    return res;

  }
}
