package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0stt;


@Repository
public interface Tmct0sttDao extends JpaRepository< Tmct0stt, String > {

}
