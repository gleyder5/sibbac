package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cliHistorical;

@Repository
public interface Tmct0cliHistoricalDao extends JpaRepository< Tmct0cliHistorical, Long > {
	
	@Query( "SELECT hist FROM Tmct0cliHistorical hist WHERE idClient =:idClient" )
	public List<Tmct0cliHistorical> findByIdClient( @Param( "idClient" ) Long idClient);

}
