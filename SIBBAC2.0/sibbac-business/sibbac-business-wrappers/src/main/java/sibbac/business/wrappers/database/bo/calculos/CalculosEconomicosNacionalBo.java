package sibbac.business.wrappers.database.bo.calculos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.calculos.dto.CalculosEconomicosFlagsDTO;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.common.utils.SibbacEnums.TypeResultProcess;

@Service
@Scope(value = "prototype")
public class CalculosEconomicosNacionalBo implements Serializable {

  protected static final Logger LOG = LoggerFactory.getLogger(CalculosEconomicosNacionalBo.class);

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private ApplicationContext ctx;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTANTES ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 7177122349842287763L;

  /* ~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS ~~~~~~~~~~~~~~~~~~~~ */

  // /** Objeto <code>Tmct0alo</code> sobre el que realizar los cálculos
  // económicos. */
  private Tmct0alo tmct0alo = null;

  /**
   * Objeto <code>Tmct0alc</code> Tmct0alc's con todos los objetos dependientes
   * para realizar los calculos economicos.
   */
  private Map<Tmct0alc, Map<Tmct0desglosecamara, List<Tmct0desgloseclitit>>> tmct0alcs = new HashMap<Tmct0alc, Map<Tmct0desglosecamara, List<Tmct0desgloseclitit>>>();

  /**
   * Lista de objetos <code>Tmct0alc</code> del <code>Tmct0alo</code> que no
   * están rechazados.
   */
  private List<AllocateNacionalTmct0alcEconomicData> activeTmct0alcList = null;

  /**
   * Lista de objetos <code>Tmct0alc</code> del <code>Tmct0alo</code> que se
   * pueden modificar.
   */
  private List<AllocateNacionalTmct0alcEconomicData> canAllocateTmct0alcList = null;

  /**
   * Lista de objetos <code>Tmct0alc</code> del <code>Tmct0alo</code> que no se
   * pueden modificar.
   */
  private List<AllocateNacionalTmct0alcEconomicData> notCanAllocateTmct0alcList = null;

  /**
   * Indica si aun estando el objeto <code>Tmct0alo</code> en un estado no
   * modificable se quieren modificar sus datos.
   */
  private Boolean updateReadOnlyData = false;

  /** Indica si el mercado paga canones de contratación. */
  private Boolean mktPayCanonContr = null;

  /** Indica si el mercado paga canones de compensación. */
  private Boolean mktPayCanonComp = null;

  /** Indica si el mercado paga canones de liquidación. */
  private Boolean mktPayCanonLiq = null;

  /** Indica si el cliente paga canones de contratación. */
  private Boolean cltePayCanonContr = null;

  /** Indica si el cliente paga canones de compensación. */
  private Boolean cltePayCanonComp = null;

  /** Indica si el cliente paga canones de liquidación. */
  private Boolean cltePayCanonLiq = null;

  /** Porcentaje de comision para DWH */
  private Tcli0comDTO tcli0com = null;

  private CanonesConfigDTO canonesConfig;

  private BigDecimal brutoMercadoTotalAllo = null;

  public CalculosEconomicosNacionalBo(Tmct0alo alo, List<Tmct0alc> alcs) {
    this.tmct0alo = alo;
    this.tmct0alcs = new HashMap<Tmct0alc, Map<Tmct0desglosecamara, List<Tmct0desgloseclitit>>>();

    for (Tmct0alc alc : alcs) {
      if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        List<Tmct0desglosecamara> dcs = alc.getTmct0desglosecamaras();
        Map<Tmct0desglosecamara, List<Tmct0desgloseclitit>> map2 = new HashMap<Tmct0desglosecamara, List<Tmct0desgloseclitit>>();
        for (Tmct0desglosecamara dc : dcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            List<Tmct0desgloseclitit> dcts = dc.getTmct0desgloseclitits();
            map2.put(dc, dcts);
          }
        }
        this.tmct0alcs.put(alc, map2);
      }
    }

  }

  public CalculosEconomicosNacionalBo(Tmct0alo alo, List<Tmct0alc> alcs, CanonesConfigDTO canonesConfig) {
    this(alo, alcs);
    this.canonesConfig = canonesConfig;
  }

  public CalculosEconomicosNacionalBo(Tmct0alo alo, List<Tmct0alc> alcs, CanonesConfigDTO canonesConfig,
      Tcli0comDTO clicom) {
    this(alo, alcs, canonesConfig);
    this.tcli0com = clicom;

  }

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /* ~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Dirige el proceso de realización de los cálculos económicos.
   * 
   * @return <code>com.bsch.bsnsvb.sibbac.toolbox.jdo.SibbacEnums.TypeResultProcess - </code>
   * Resultado de la operación. @ Si ocurre algún error al obtener el gestor de
   * acceso a base de datos.
   * @throws EconomicDataException Si ocurre algún error durante los cálculos
   * económicos.
   */
  public TypeResultProcess calcularDatosEconomicos() throws EconomicDataException {
    CalculosEconomicosFlagsDTO flags = new CalculosEconomicosFlagsDTO();
    return calcularDatosEconomicos(flags);
  } // process

  /**
   * Dirige el proceso de realización de los cálculos económicos.
   * 
   * @return <code>com.bsch.bsnsvb.sibbac.toolbox.jdo.SibbacEnums.TypeResultProcess - </code>
   * Resultado de la operación. @ Si ocurre algún error al obtener el gestor de
   * acceso a base de datos.
   * @throws EconomicDataException Si ocurre algún error durante los cálculos
   * económicos.
   */
  public TypeResultProcess calcularDatosEconomicosSinCanones() throws EconomicDataException {
    CalculosEconomicosFlagsDTO flags = new CalculosEconomicosFlagsDTO();
    flags.setCalcularCanones(false);
    return calcularDatosEconomicos(flags);
  } // process

  /**
   * Dirige el proceso de realización de los cálculos económicos.
   * 
   * @return <code>com.bsch.bsnsvb.sibbac.toolbox.jdo.SibbacEnums.TypeResultProcess - </code>
   * Resultado de la operación. @ Si ocurre algún error al obtener el gestor de
   * acceso a base de datos.
   * @throws EconomicDataException Si ocurre algún error durante los cálculos
   * económicos.
   */
  public TypeResultProcess calcularDatosEconomicosSinCanonesAndSinRepartirAloAlc() throws EconomicDataException {
    CalculosEconomicosFlagsDTO flags = new CalculosEconomicosFlagsDTO();
    flags.setCalcularCanones(false);
    flags.setCalcularBrutoMercado(false);
    flags.setRepartirBrutoClienteFidessa(false);
    flags.setRepartirNetoClienteFidessa(false);
    flags.setCalcularComisionCliente(false);
    flags.setRepartirComisionOrdenante(false);
    return calcularDatosEconomicos(flags);
  } // process

  /**
   * Dirige el proceso de realización de los cálculos económicos.
   * 
   * @return <code>com.bsch.bsnsvb.sibbac.toolbox.jdo.SibbacEnums.TypeResultProcess - </code>
   * Resultado de la operación. @ Si ocurre algún error al obtener el gestor de
   * acceso a base de datos.
   * @throws EconomicDataException Si ocurre algún error durante los cálculos
   * económicos.
   */
  public TypeResultProcess calcularComLiqAndProfit() throws EconomicDataException {
    CalculosEconomicosFlagsDTO flags = new CalculosEconomicosFlagsDTO();
    flags.setCalcularCanones(false);
    flags.setCalcularBrutoMercado(false);
    flags.setRepartirBrutoClienteFidessa(false);
    flags.setRepartirNetoClienteFidessa(false);
    flags.setCalcularComisionCliente(false);
    flags.setRepartirComisionOrdenante(false);
    flags.setCalcularImbansis(false);
    flags.setCalcularImcomsis(false);
    flags.setCalcularComisionBanco(false);
    flags.setCalcularComisionDevolucion(false);
    flags.setCalcularNetoCliente(false);
    flags.setCalcularAjustePrecio(false);
    flags.setRepartirImcobrado(false);
    flags.setRepartirImefeMercadoCobrado(false);
    flags.setRepartirImefeClienteCobrado(false);
    flags.setRepartirImcanonCobrado(false);
    flags.setRepartirImcomCobrado(false);
    flags.setRepartirImbrkPagado(false);
    flags.setRepartirImdvoPagado(false);

    return calcularDatosEconomicos(flags);
  } // process

  /**
   * reparte los datos economicos del alc entre sus dct's
   * 
   * @return <code>com.bsch.bsnsvb.sibbac.toolbox.jdo.SibbacEnums.TypeResultProcess - </code>
   * Resultado de la operación. @ Si ocurre algún error al obtener el gestor de
   * acceso a base de datos.
   * @throws EconomicDataException Si ocurre algún error durante los cálculos
   * económicos.
   */
  public TypeResultProcess repartirCalculosAlcEnDct() throws EconomicDataException {
    CalculosEconomicosFlagsDTO flags = new CalculosEconomicosFlagsDTO();
    flags.setRepartirBrutoClienteFidessa(false);
    flags.setRepartirNetoClienteFidessa(false);
    flags.setCalcularComisionCliente(false);
    flags.setRepartirComisionOrdenante(false);

    repartirCalculos(flags);
    return TypeResultProcess.FINISH_OK;
  } // process

  public TypeResultProcess repartirCalculosAlcEnDct(CalculosEconomicosFlagsDTO flags) throws EconomicDataException {
    repartirCalculos(flags);
    return TypeResultProcess.FINISH_OK;
  } // process

  public void copiarCanonContreuToCanoncontrdv() {
    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      tmct0alcEconomicData.copiarCanonContreuToCanoncontrdv();
    }
  }

  public void copiarCanonContrdvToCanoncontreu() {

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      tmct0alcEconomicData.copiarCanonContrdvToCanoncontreu();

    }
  }

  /**
   * reparte los datos economicos del alc entre sus dct's
   * 
   * @return <code>com.bsch.bsnsvb.sibbac.toolbox.jdo.SibbacEnums.TypeResultProcess - </code>
   * Resultado de la operación. @ Si ocurre algún error al obtener el gestor de
   * acceso a base de datos.
   * @throws EconomicDataException Si ocurre algún error durante los cálculos
   * económicos.
   */
  public TypeResultProcess repartirTodosCalculosAlcEnDct() throws EconomicDataException {
    CalculosEconomicosFlagsDTO flags = new CalculosEconomicosFlagsDTO();
    flags.setRepartirBrutoClienteFidessa(true);
    flags.setRepartirNetoClienteFidessa(true);
    flags.setCalcularComisionCliente(true);
    flags.setRepartirComisionOrdenante(true);

    repartirCalculos(flags);
    return TypeResultProcess.FINISH_OK;
  } // process

  /**
   * @param flags
   * @return
   * @throws EconomicDataException
   */
  private TypeResultProcess calcularDatosEconomicos(CalculosEconomicosFlagsDTO flags) throws EconomicDataException {

    long startTime = System.currentTimeMillis();
    LOG.trace("## ALOEData ## process ## Iniciando módulo de cálculos económicos ...");

    LOG.trace("## ALOEData ## process ## {}", flags);

    String nuorden = null;
    String nbooking = null;
    String nucnfclt = null;

    try {
      nuorden = tmct0alo.getId().getNuorden().trim();
      nbooking = tmct0alo.getId().getNbooking().trim();
      nucnfclt = tmct0alo.getId().getNucnfclt().trim();

      LOG.trace("## ALOEData ## process ## Procesando ALO: [nuorden: {}, nbooking: {}, nucnfclt: {}]", nuorden,
          nbooking, nucnfclt);

      LOG.trace("## ALOEData ## process ## Iniciando calculos previos ...");
      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
        AllocateNacionalTmct0alcEconomicData.inicializarNullsTmct0alc(tmct0alcEconomicData.getTmct0alc());
        tmct0alcEconomicData.inizializeNullsDesglosesClitit();
      }
      if (flags.isCalcularBrutoMercado()) {
        calcularBrutoMercado();
      }
      if (flags.isRepartirBrutoClienteFidessa()) {
        repartirBrutoCliente();
      }
      if (flags.isRepartirNetoClienteFidessa()) {
        repartirNetoClienteFidessa();
      }
      if (flags.isCalcularComisionCliente()) {
        calcularComisionCliente();
      }
      if (flags.isRepartirComisionOrdenante()) {
        repartirComisionOrdenante();
      }

      LOG.trace("## ALOEData ## process ## Iniciando calculos economicos");

      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
        LOG.trace("## ALOEData ## process ## Procesando ALC: " + tmct0alcEconomicData.getTmct0alc().getNucnfliq());
        if (flags.isCalcularImcomsis()) {
          tmct0alcEconomicData.calcularImcomsis();
        }
        if (flags.isCalcularImbansis()) {
          tmct0alcEconomicData.calcularImbansis();
        }
        if (flags.isCalcularCanones()) {
          tmct0alcEconomicData.calcularCanones();
        }
        if (flags.isCalcularComisionBanco()) {
          tmct0alcEconomicData.calcularComisionBanco();
        }
        if (flags.isCalcularComisionDevolucion()) {
          tmct0alcEconomicData.calcularRebate();
        }

        if (flags.isCalcularNetoCliente()) {
          if ((getCltePayCanonComp() || getCltePayCanonContr() || getCltePayCanonLiq())) {
            tmct0alcEconomicData.calcularNetoClienteFinal();
          }
          else {
            tmct0alcEconomicData.calcularNetoClienteFinalPrecioNetoMedio();
          } // else
        }
        if (flags.isCalcularAjustePrecio()) {
          tmct0alcEconomicData.calcularPriceAjustment();
        }

        if (flags.isCalcularComisionLiquidacion()) {
          tmct0alcEconomicData.calcularComisionLiquidacion();
        }
        if (flags.isCalcularComisionBeneficio()) {
          tmct0alcEconomicData.calcularProfitCommission();
        }

        if (flags.isRepartirImcobrado()) {
          tmct0alcEconomicData.repartirImcobrado();
        }
        // if (flags.isRepartirConciliado()) {
        // tmct0alcEconomicData.repartirImconciliado();
        // }

        if (flags.isRepartirImefeMercadoCobrado()) {
          tmct0alcEconomicData.repartirImefeMercadoCobrado();
        }

        if (flags.isRepartirImefeClienteCobrado()) {
          tmct0alcEconomicData.repartirImefeClienteCobrado();
        }

        if (flags.isRepartirImcanonCobrado()) {
          tmct0alcEconomicData.repartirImcanonCobrado();
        }
        if (flags.isRepartirImcomCobrado()) {
          tmct0alcEconomicData.repartirImcomCobrado();
        }

        if (flags.isRepartirImbrkPagado()) {
          tmct0alcEconomicData.repartirImbrkPagado();
        }

        if (flags.isRepartirImdvoPagado()) {
          tmct0alcEconomicData.repartirImdvoPagado();
        }

      }
      if (flags.isCalcularNetoCliente()) {
        if (!getCltePayCanonComp() && !getCltePayCanonContr() && !getCltePayCanonLiq()) {
          calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio();
        } // if
      }
      else {
        if (flags.isRepartirComisionAjuste()) {
          repartirComisionAjuste();
        }
      }
      return TypeResultProcess.FINISH_OK;

    }
    catch (Exception e) {
      LOG.error("## ALOEData ## process ## Error: " + e.getMessage());
      LOG.trace("## ALOEData ## process ## Pila de error: ", e);
      throw new EconomicDataException(e.getMessage(), e.fillInStackTrace());
    }
    finally {
      activeTmct0alcList.clear();
      canAllocateTmct0alcList.clear();
      notCanAllocateTmct0alcList.clear();

      Long endTime = System.currentTimeMillis();
      LOG.trace("## ALOEData ## process ## Finalizado módulo de cálculos económicos para [nuorden: "
          + nuorden
          + ", nbooking: "
          + nbooking
          + ", nucnfclt: "
          + nucnfclt
          + "] - ["
          + String.format(
              "%d min, %d sec",
              TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
              TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                  - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally

  }

  public void acumulateFromTmct0alcs() {
    this.acumulateFromTmct0alcs(new CalculosEconomicosFlagsDTO());
  }

  protected void acumulateFromTmct0alcs(CalculosEconomicosFlagsDTO flags) {
    for (AllocateNacionalTmct0alcEconomicData allocateNacionalTmct0alcEconomicData : getActiveTmct0alcList()) {
      allocateNacionalTmct0alcEconomicData.acumulateFromExecutionGroup(flags);
    }
  }

  protected void repartirCalculos(CalculosEconomicosFlagsDTO flags) throws EconomicDataException {

    long startTime = System.currentTimeMillis();
    LOG.trace("## ALOEData ## process ## Iniciando módulo de cálculos económicos ...");

    String nuorden = null;
    String nbooking = null;
    String nucnfclt = null;

    try {
      nuorden = tmct0alo.getId().getNuorden().trim();
      nbooking = tmct0alo.getId().getNbooking().trim();
      nucnfclt = tmct0alo.getId().getNucnfclt().trim();

      LOG.trace("## ALOEData ## process ## Procesando ALO: [nuorden: " + nuorden + ", nbooking: " + nbooking
          + ", nucnfclt: " + nucnfclt + "]");

      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
        AllocateNacionalTmct0alcEconomicData.inicializarNullsTmct0alc(tmct0alcEconomicData.getTmct0alc());
        tmct0alcEconomicData.inizializeNullsDesglosesClitit();
      }

      if (flags.isCalcularBrutoMercado()) {
        calcularBrutoMercado();
      }
      if (flags.isRepartirBrutoClienteFidessa()) {
        repartirBrutoCliente();
      }
      if (flags.isRepartirNetoClienteFidessa()) {
        repartirNetoClienteFidessa();
      }
      if (flags.isCalcularComisionCliente()) {
        repartirComisionCliente();
      }
      if (flags.isRepartirComisionOrdenante()) {
        repartirComisionOrdenante();
      }

      LOG.trace("## ALOEData ## process ## Iniciando calculos economicos");

      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
        if (flags.isRepartirBrutoClienteFidessaDct()) {
          tmct0alcEconomicData.repartirBrutoCliente();
        }
        if (flags.isRepartirNetoClienteFidessaDct()) {
          tmct0alcEconomicData.repartirNetoClienteFidessa();
        }
        if (flags.isCalcularComisionClienteDct()) {
          tmct0alcEconomicData.repartirComisionCliente();
        }
        if (flags.isRepartirComisionOrdenanteDct()) {
          tmct0alcEconomicData.repartirComisionOrdenante();
        }
        if (flags.isCalcularImcomsis()) {
          tmct0alcEconomicData.repartirImbansis();
        }
        if (flags.isCalcularImbansis()) {
          tmct0alcEconomicData.repartirImbansis();
        }
        if (flags.isCalcularCanones()) {
          tmct0alcEconomicData.repartirCanones();
        }
        if (flags.isCalcularComisionBanco()) {
          tmct0alcEconomicData.repartirComisionBanco();
        }
        if (flags.isCalcularComisionDevolucion()) {
          tmct0alcEconomicData.repartirRebate();
        }
        if (flags.isCalcularNetoCliente()) {
          tmct0alcEconomicData.repartirNetoClienteFinal();
        }
        if (flags.isCalcularAjustePrecio()) {
          tmct0alcEconomicData.repartirPriceAjustment();
        }
        if (flags.isCalcularComisionLiquidacion()) {
          tmct0alcEconomicData.repartirComisionLiquidacion();
        }
        if (flags.isCalcularComisionBeneficio()) {
          tmct0alcEconomicData.repartirProfitCommission();
        }

        if (flags.isRepartirComisionAjuste()) {
          tmct0alcEconomicData.repartirAjustmentCommission();
        }

        if (flags.isRepartirImcobrado()) {
          tmct0alcEconomicData.repartirImcobrado();
        }
        // if (flags.isRepartirConciliado()) {
        // tmct0alcEconomicData.repartirImconciliado();
        // }

        if (flags.isRepartirImefeMercadoCobrado()) {
          tmct0alcEconomicData.repartirImefeMercadoCobrado();
        }

        if (flags.isRepartirImefeClienteCobrado()) {
          tmct0alcEconomicData.repartirImefeClienteCobrado();
        }

        if (flags.isRepartirImcanonCobrado()) {
          tmct0alcEconomicData.repartirImcanonCobrado();
        }
        if (flags.isRepartirImcomCobrado()) {
          tmct0alcEconomicData.repartirImcomCobrado();
        }

        if (flags.isRepartirImbrkPagado()) {
          tmct0alcEconomicData.repartirImbrkPagado();
        }

        if (flags.isRepartirImdvoPagado()) {
          tmct0alcEconomicData.repartirImdvoPagado();
        }

      } // for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData :
        // getCanAllocateTmct0alcList())

    }
    catch (Exception e) {
      LOG.error("## ALOEData ## process ## Error: {}", e.getMessage());
      LOG.trace("## ALOEData ## process ## Pila de error: ", e);
      throw new EconomicDataException(e.getMessage(), e.fillInStackTrace());
    }
    finally {
      activeTmct0alcList.clear();
      canAllocateTmct0alcList.clear();
      notCanAllocateTmct0alcList.clear();

      Long endTime = System.currentTimeMillis();
      LOG.trace("## ALOEData ## process ## Finalizado módulo de cálculos económicos para [nuorden: "
          + nuorden
          + ", nbooking: "
          + nbooking
          + ", nucnfclt: "
          + nucnfclt
          + "] - ["
          + String.format(
              "%d min, %d sec",
              TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
              TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                  - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally

  }

  /* ~~~~~~~~~~~~~~~~~~~~ BRUTO MERCADO ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Para cada <code>Tmct0alc</code> del <code>Tmct0alo</code> calcula el
   * importe bruto mercado.
   * <p>
   * Bruto mercado: Sumatorio de títulos X precio de las agrupaciones del
   * Mercado.
   *
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  private void calcularBrutoMercado() {
    LOG.trace("## ALOEData ## calcularBrutoMercado ## Inicio");
    brutoMercadoTotalAllo = BigDecimal.ZERO;

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      tmct0alcEconomicData.calcularBrutoMercado();
      brutoMercadoTotalAllo = brutoMercadoTotalAllo.add(tmct0alcEconomicData.getTmct0alc().getImefeagr());
    } // for
    if (CollectionUtils.isNotEmpty(getNotCanAllocateTmct0alcList())) {
      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getNotCanAllocateTmct0alcList()) {
        brutoMercadoTotalAllo = brutoMercadoTotalAllo.add(tmct0alcEconomicData.getTmct0alc().getImefeagr());
      }
    }
    LOG.trace("## ALOEData ## calcularBrutoMercado ## brutoMercadoTotalAllo == " + brutoMercadoTotalAllo);
    LOG.trace("## ALOEData ## calcularBrutoMercado ## Fin");
  } // calcularBrutoMercado

  /* ~~~~~~~~~~~~~~~~~~~~ BRUTO CLIENTE ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Reparte el importe bruto cliente (IMTOTBRU) del <code>Tmct0alo</code> entre
   * sus <code>Tmct0alc</code>.<br>
   * Establece el campo <code>IMTOTBRU</code> del <code>Tmct0alc</code> con el
   * resultado del reparto.
   * <p>
   * Bruto ciente: Total de títulos ejecutados X precio medio dado al cliente.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  private void repartirBrutoCliente() {
    LOG.trace("## ALOEData ## repartirBrutoCliente ## Inicio");

    BigDecimal acumulado = BigDecimal.ZERO;
    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getNotCanAllocateTmct0alcList()) {
      acumulado = acumulado.add(tmct0alcEconomicData.getTmct0alc().getImtotbru());
    } // for
    LOG.trace("## ALOEData ## repartirBrutoCliente ## acumulando_NotCanAllocate: {}", acumulado);

    LOG.trace("## ALOEData ## repartirBrutoCliente ## tmct0alo.getImtotbru(): {}", tmct0alo.getImtotbru());

    int i = 0;
    BigDecimal imtotbru;
    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      i++;
      if (i != getCanAllocateTmct0alcList().size()) {
        LOG.trace("## ALOEData ## repartirBrutoCliente ## tmct0alcEconomicData.getTmct0alc().getNutitliq(): {}",
            tmct0alcEconomicData.getTmct0alc().getNutitliq());

        imtotbru = tmct0alo.getImtotbru().multiply(tmct0alcEconomicData.getTmct0alc().getNutitliq())
            .divide(tmct0alcEconomicData.getTmct0alc().getTmct0alo().getNutitcli(), 2, BigDecimal.ROUND_HALF_UP);

        tmct0alcEconomicData.getTmct0alc().setImtotbru(imtotbru);
        acumulado = acumulado.add(imtotbru);
      }
      else {
        LOG.trace("## ALOEData ## repartirBrutoCliente ## acumulado_ultimo: {}", acumulado);
        tmct0alcEconomicData.getTmct0alc().setImtotbru(
            tmct0alo.getImtotbru().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALOEData ## repartirBrutoCliente ## tmct0alcEconomicData.getTmct0alc().getImtotbru(): {}",
          tmct0alcEconomicData.getTmct0alc().getImtotbru());

      // Se reparte el bruto cliente del ALC entre sus DESGLOSECLITIT
      tmct0alcEconomicData.repartirBrutoCliente();
    } // for
    LOG.trace("## ALOEData ## repartirBrutoCliente ## Fin");
  } // repartirBrutoCliente

  /* ~~~~~~~~~~~~~~~~~~~~ NETO CLIENTE FIDESSA ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Reparte el importe neto cliente Fidessa (IMTOTNET) del
   * <code>Tmct0alo</code> entre sus <code>Tmct0alc</code>.<br>
   * Establece el campo <code>IMTOTNET</code> del <code>Tmct0alc</code> con el
   * resultado del reparto.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  private void repartirNetoClienteFidessa() {
    LOG.trace("## ALOEData ## repartirNetoClienteFidessa ## Inicio");

    BigDecimal acumulado = BigDecimal.ZERO;

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getNotCanAllocateTmct0alcList()) {
      acumulado = acumulado.add(tmct0alcEconomicData.getTmct0alc().getImtotnet());

    } // for

    LOG.trace("## ALOEData ## repartirNetoClienteFidessa ## acumulando_NotCanAllocate: {}", acumulado);

    LOG.trace("## ALOEData ## repartirNetoClienteFidessa ## tmct0alo.getImtotnet(): {}", tmct0alo.getImtotnet());

    int i = 0;
    BigDecimal imtotnet;

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      i++;
      if (i != getCanAllocateTmct0alcList().size()) {
        LOG.trace("## ALOEData ## repartirNetoClienteFidessa ## tmct0alcEconomicData.getTmct0alc().getImefeagr(): {}",
            tmct0alcEconomicData.getTmct0alc().getImefeagr());

        imtotnet = tmct0alcEconomicData.getTmct0alc().getNutitliq().multiply(tmct0alo.getImtotnet())
            .divide(tmct0alcEconomicData.getTmct0alc().getTmct0alo().getNutitcli(), 2, BigDecimal.ROUND_HALF_UP);
        tmct0alcEconomicData.getTmct0alc().setImtotnet(imtotnet);
        acumulado = acumulado.add(imtotnet);
      }
      else {
        tmct0alcEconomicData.getTmct0alc().setImtotnet(
            tmct0alo.getImtotnet().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALOEData ## repartirNetoClienteFidessa ## tmct0alcEconomicData.getTmct0alc().getImtotnet(): {}",
          tmct0alcEconomicData.getTmct0alc().getImtotnet());
      tmct0alcEconomicData.repartirNetoClienteFidessa();
    } // for

    LOG.trace("## ALOEData ## repartirNetoClienteFidessa ## Fin");
  } // repartirNetoClienteFidessa

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN CLIENTE ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Calcula la comición cliente.
   * <p>
   * Comisión cobrada al cliente.<br>
   * Si es calculado (PCCOMISN): resultante de aplicar un % de comisión al bruto
   * cliente.<br>
   * Si es un importe (IMCOMISN): es informado directamente (automático o
   * manual).
   * 
   * @
   */
  private void calcularComisionCliente() {
    LOG.trace("## ALOEData ## calcularComisionCliente ## Inicio");
    LOG.trace("## ALOEData ## calcularComisionCliente ## tmct0alo.getPccomisn(): {}", tmct0alo.getPccomisn());
    LOG.trace("## ALOEData ## calcularComisionCliente ## tmct0alo.getImcomisn(): {}", tmct0alo.getImcomisn());

    if (BigDecimal.ZERO.compareTo(tmct0alo.getPccomisn()) == 0
        && BigDecimal.ZERO.compareTo(tmct0alo.getImcomisn()) != 0) {
      repartirComisionCliente();
    }
    else if (BigDecimal.ZERO.compareTo(tmct0alo.getPccomisn()) != 0
        && BigDecimal.ZERO.compareTo(tmct0alo.getImcomisn()) == 0) {
      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
        tmct0alcEconomicData.calcularComisionCliente();
      } // for
    }
    else if (BigDecimal.ZERO.compareTo(tmct0alo.getPccomisn()) != 0
        && BigDecimal.ZERO.compareTo(tmct0alo.getImcomisn()) != 0) {
      repartirComisionCliente();
    }
    else {
      repartirComisionCliente();
    } // else
    LOG.trace("## ALOEData ## calcularComisionCliente ## Fin");
  } // calcularComisionCliente

  /**
   * Reparte la comisión cliente (IMCOMISN) del <code>Tmct0alo</code> entre sus
   * <code>Tmct0alc</code> que se puedan modificar.<br>
   * Establece el campo <code>IMCOMISN</code> del <code>Tmct0alc</code> con el
   * resultado del reparto.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  private void repartirComisionCliente() {
    LOG.trace("## ALOEData ## repartirComisionCliente ## Inicio");
    BigDecimal acumulado = BigDecimal.ZERO;

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getNotCanAllocateTmct0alcList()) {
      acumulado = acumulado.add(tmct0alcEconomicData.getTmct0alc().getImcomisn());
    } // for
    LOG.trace("## ALOEData ## repartirComisionCliente ## acumunlado_NotCanAllocate: {}", acumulado);

    LOG.trace("## ALOEData ## repartirComisionCliente ## tmct0alo.getImcomisn(): {}", tmct0alo.getImcomisn());

    int i = 0;
    BigDecimal imcomisn;
    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      i++;
      if (i != getCanAllocateTmct0alcList().size()) {
        LOG.trace("## ALOEData ## repartirComisionCliente ## tmct0alcEconomicData.getTmct0alc().getImtotbru(): {}",
            tmct0alcEconomicData.getTmct0alc().getImtotbru());

        imcomisn = tmct0alcEconomicData.getTmct0alc().getImtotbru().multiply(tmct0alo.getImcomisn())
            .divide(tmct0alcEconomicData.getTmct0alc().getTmct0alo().getImtotbru(), 2, BigDecimal.ROUND_HALF_UP);
        tmct0alcEconomicData.getTmct0alc().setImcomisn(imcomisn);
        acumulado = acumulado.add(imcomisn);
      }
      else {
        tmct0alcEconomicData.getTmct0alc().setImcomisn(
            tmct0alo.getImcomisn().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALOEData ## repartirComisionCliente ## tmct0alcEconomicData.getTmct0alc().getImcomisn(): {}",
          tmct0alcEconomicData.getTmct0alc().getImcomisn());
      tmct0alcEconomicData.repartirComisionCliente();
    } // for
    LOG.trace("## ALOEData ## repartirComisionCliente ## Fin");
  } // repartirComisionCliente

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN DE DEVOLUCIÓN ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Reparte la comisión de devolución (IMCOMDEV) del <code>Tmct0alo</code>
   * entre sus <code>Tmct0alc</code> que se puedan modificar.<br>
   * Establece el campo <code>IMCOMDVO</code> del <code>Tmct0alc</code> con el
   * resultado del reparto.<br>
   * Corresponde a la payer comission / Comision ordenante de la pantalla de
   * desglose. NO es la comisión de devolución.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  private void repartirComisionOrdenante() {
    LOG.trace("## ALOEData ## repartirComisionOrdenante ## Inicio");
    BigDecimal acumulado = BigDecimal.ZERO;

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getNotCanAllocateTmct0alcList()) {
      acumulado = acumulado.add(tmct0alcEconomicData.getTmct0alc().getImcomdvo());

    } // for

    LOG.trace("## ALOEData ## repartirComisionOrdenante ## acumulado_NotCanAllocate: " + acumulado);

    LOG.trace("## ALOEData ## repartirComisionOrdenante ## tmct0alo.getImcomdev(): " + tmct0alo.getImcomdev());
    LOG.trace("## ALOEData ## repartirComisionOrdenante ## tmct0alo.getBrutoMercadoTotalAllo(): "
        + getBrutoMercadoTotalAllo());

    BigDecimal pccomdev = tmct0alo.getImcomdev().multiply(new BigDecimal(100))
        .divide(tmct0alo.getImtotbru(), 4, BigDecimal.ROUND_HALF_UP);

    LOG.trace("## ALOEData ## repartirComisionOrdenante ## pccomdev: " + pccomdev);

    int i = 0;
    BigDecimal imcomdvo;
    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      i++;
      tmct0alcEconomicData.getTmct0alc().setPccomdev(pccomdev);

      if (i != getCanAllocateTmct0alcList().size()) {
        LOG.trace("## ALOEData ## repartirComisionOrdenante ## tmct0alcEconomicData.getTmct0alc().getImefeagr(): "
            + tmct0alcEconomicData.getTmct0alc().getImefeagr());
        LOG.trace("## ALOEData ## repartirComisionOrdenante ## tmct0alo.getBrutoMercadoTotalAllo(): "
            + getBrutoMercadoTotalAllo());

        imcomdvo = tmct0alo.getImcomdev().multiply(tmct0alcEconomicData.getTmct0alc().getImefeagr())
            .divide(getBrutoMercadoTotalAllo(), 2, BigDecimal.ROUND_HALF_UP);

        LOG.trace("## ALOEData ## repartirComisionOrdenante ## imcomdvo: " + imcomdvo);

        tmct0alcEconomicData.getTmct0alc().setImcomdvo(imcomdvo);
        acumulado = acumulado.add(imcomdvo);
      }
      else {
        tmct0alcEconomicData.getTmct0alc().setImcomdvo(
            tmct0alo.getImcomdev().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALOEData ## repartirComisionOrdenante ## tmct0alcEconomicData.getTmct0alc().getImcomdvo(): "
          + tmct0alcEconomicData.getTmct0alc().getImcomdvo());

      tmct0alcEconomicData.repartirComisionOrdenante();
    } // for
    LOG.trace("## ALOEData ## repartirComisionOrdenante ## Fin");
  } // repartirComisionDevolucion

  /*
   * ~~~~~~~~~~~~~~~~~~~~ AJUSTE NETO FINAL CLIENTE NO ASUME GASTOS
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * 
   * @
   */
  private void calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio() {
    LOG.trace("## ALOEData ## calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio ## Inicio");
    BigDecimal imnetliq = BigDecimal.ZERO;

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      imnetliq = imnetliq.add(tmct0alcEconomicData.getTmct0alc().getImnetliq().setScale(2, BigDecimal.ROUND_HALF_UP));

    } // for
    LOG.trace("## ALOEData ## calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio ## imnetliq: " + imnetliq);
    List<AllocateNacionalTmct0alcEconomicData> tmct0alcs = new ArrayList<AllocateNacionalTmct0alcEconomicData>(
        getCanAllocateTmct0alcList());
    if (imnetliq.compareTo(tmct0alo.getImtotnet()) != 0) {

      // Compra
      if ('C' == tmct0alo.getCdtpoper()) {
        if (tmct0alo.getImtotnet().compareTo(imnetliq) > 0) {
          sortAjusteRepartir(tmct0alcs, 1);
        }
        else {
          sortAjusteRepartir(tmct0alcs, -1);
        } // else
      }
      else { // Venta
        if (tmct0alo.getImtotnet().compareTo(imnetliq) > 0) {
          sortAjusteRepartir(tmct0alcs, -1);
        }
        else {
          sortAjusteRepartir(tmct0alcs, 1);
        } // else
      } // else

      BigDecimal ajuste = BigDecimal.ZERO;
      if ('C' == tmct0alo.getCdtpoper()) {
        ajuste = ajuste.add(imnetliq.subtract(tmct0alo.getImtotnet()));
      }
      else {
        ajuste = ajuste.add(tmct0alo.getImtotnet().subtract(imnetliq));
      } // else
      LOG.trace("## ALOEData ## calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio ## ajuste: " + ajuste);

      AllocateNacionalTmct0alcEconomicData tmct0alcNacionalTmct0alcEconomicData = tmct0alcs.get(0);

      if (imnetliq.compareTo(tmct0alcNacionalTmct0alcEconomicData.getTmct0alc().getImnfiliq()) != 0) {
        tmct0alcNacionalTmct0alcEconomicData.ajusteRepartirNetoClienteFinal(tmct0alcNacionalTmct0alcEconomicData
            .getTmct0alc().getImnfiliq().subtract(ajuste));
      } // if
      tmct0alcs.clear();
    }
    // } // if (imnetliq.compareTo(tmct0alo.getImtotnet()) != 0)

    LOG.trace("## ALOEData ## calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio ## Fin");
  }// calcularAjusteNetoClienteFinalPorPrecioNetoClienteMedio

  /* ~~~~~~~~~~~~~~~~~~~~ MÉTODOS DE UTILIDAD ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Devuelve la lista de <code>Tmct0alc</code> del <code>Tmct0alo</code> no
   * rechazados que se pueden modificar ordenada por número de títulos.
   * 
   * @return <code>java.util.List<AllocateNacionalTmct0alcEconomicData> - </code>
   * Lista. @ Si ocurre algún error al obtener el gestor de acceso a base de
   * datos.
   */
  private List<AllocateNacionalTmct0alcEconomicData> getCanAllocateTmct0alcList() {
    if (canAllocateTmct0alcList == null || notCanAllocateTmct0alcList == null) {
      canAllocateTmct0alcList = new ArrayList<AllocateNacionalTmct0alcEconomicData>();
      notCanAllocateTmct0alcList = new ArrayList<AllocateNacionalTmct0alcEconomicData>();

      List<AllocateNacionalTmct0alcEconomicData> tmct0alcs = getActiveTmct0alcList();

      for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : tmct0alcs) {
        if (updateReadOnlyData || tmct0alcEconomicData.isCanAllocateTmct0alc()) {
          if (!canAllocateTmct0alcList.contains(tmct0alcEconomicData)) {
            canAllocateTmct0alcList.add(tmct0alcEconomicData);
          } // if
        }
        else {
          if (!notCanAllocateTmct0alcList.contains(tmct0alcEconomicData)) {
            notCanAllocateTmct0alcList.add(tmct0alcEconomicData);
          } // if
        } // else
      } // for

      sortAjusteRepartir(canAllocateTmct0alcList, -1);
    } // if
    return canAllocateTmct0alcList;
  } // getListCanAllocateTmct0alcs

  /**
   * Devuelve la lista de <code>Tmct0alc</code> del <code>Tmct0alo</code> no
   * rechazados que no se pueden modificar.
   * 
   * @return <code>java.util.List<AllocateNacionalTmct0alcEconomicData> - </code>
   * Lista. @ Si ocurre algún error al obtener el gestor de acceso a base de
   * datos.
   */
  private List<AllocateNacionalTmct0alcEconomicData> getNotCanAllocateTmct0alcList() {
    if (canAllocateTmct0alcList == null || notCanAllocateTmct0alcList == null) {
      getCanAllocateTmct0alcList();
    } // if
    return notCanAllocateTmct0alcList;
  } // getListNotCanAllocateTmct0alcs

  /**
   * Obtiene la lista ordenada por número de títulos de los
   * <code>Tmct0alc</code> del <code>Tmct0alo</code> que no están rechazados.
   * 
   * @return <code>java.util.List<AllocateNacionalTmct0alcEconomicData> - </code>
   * Lista. @ Si ocurre algún error al obtener el gestor de acceso a base de
   * datos.
   */
  private List<AllocateNacionalTmct0alcEconomicData> getActiveTmct0alcList() {
    List<Tmct0desgloseclitit> desglosesCt;
    Collection<Tmct0desglosecamara> desglosesCamara;
    AllocateNacionalTmct0alcEconomicData tmct0alcEData;

    if (activeTmct0alcList == null) {
      activeTmct0alcList = new ArrayList<AllocateNacionalTmct0alcEconomicData>();
      Integer est;
      for (Tmct0alc tmct0alc : this.tmct0alcs.keySet()) {
        est = tmct0alc.getCdestadoasig();
        if (est != null && est > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          desglosesCamara = this.tmct0alcs.get(tmct0alc).keySet();
          desglosesCt = new ArrayList<Tmct0desgloseclitit>();
          for (Tmct0desglosecamara desgloseCamaraNC : desglosesCamara) {
            if (desgloseCamaraNC.getTmct0estadoByCdestadoasig() != null
                && desgloseCamaraNC.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                    .getId()) {
              desglosesCt.addAll(this.tmct0alcs.get(tmct0alc).get(desgloseCamaraNC));
            }
          }

          if (!desglosesCt.isEmpty()) {

            tmct0alcEData = ctx.getBean(AllocateNacionalTmct0alcEconomicData.class, tmct0alc, desglosesCt,
                this.tcli0com, this.canonesConfig);

            activeTmct0alcList.add(tmct0alcEData);
          }
        }
      } // for (Tmct0alc tmct0alc : tmct0alo.getFk_tmct0alo_1()) {

      sortAjusteRepartir(activeTmct0alcList, 1);
    } // if
    return activeTmct0alcList;
  } // getActiveTmct0alcs

  /**
   * Ordena la lista especificada.
   * 
   * @param tmct0alcEconomicDatas Lista a ordenar.
   * @param asc Modo de ordenación.
   * @return <code>java.util.List<AllocateNacionalTmct0alcEconomicData> - </code>
   * Lista ordenada.
   */
  private List<AllocateNacionalTmct0alcEconomicData> sortAjusteRepartir(
      List<AllocateNacionalTmct0alcEconomicData> tmct0alcEconomicDatas, final int asc) {
    Collections.sort(tmct0alcEconomicDatas, new Comparator<AllocateNacionalTmct0alcEconomicData>() {

      @Override
      public int compare(AllocateNacionalTmct0alcEconomicData o1, AllocateNacionalTmct0alcEconomicData o2) {
        if (o1.getTmct0alc().getNutitliq().compareTo(o2.getTmct0alc().getNutitliq()) != 0) {
          return o1.getTmct0alc().getNutitliq().compareTo(o2.getTmct0alc().getNutitliq()) * asc;
        }
        else {
          if (o1.getTmct0alc().getNbtitliq().compareTo(o2.getTmct0alc().getNbtitliq()) != 0) {
            return o1.getTmct0alc().getNbtitliq().compareTo(o2.getTmct0alc().getNbtitliq()) * asc;
          }
          else {
            return o1.getTmct0alc().getNucnfliq().compareTo(o2.getTmct0alc().getNucnfliq()) * asc;
          } // else
        } // else
      } // compare
    }); // sort
    return tmct0alcEconomicDatas;
  } // sortAjusteRepartir

  /**
   * Reparte la comisión cliente (IMCOMISN) del <code>Tmct0alo</code> entre sus
   * <code>Tmct0alc</code> que se puedan modificar.<br>
   * Establece el campo <code>IMCOMISN</code> del <code>Tmct0alc</code> con el
   * resultado del reparto.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  private void repartirComisionAjuste() {
    LOG.trace("## ALOEData ## repartirComisionCliente ## Inicio");

    for (AllocateNacionalTmct0alcEconomicData tmct0alcEconomicData : getCanAllocateTmct0alcList()) {
      tmct0alcEconomicData.repartirAjustmentCommission();
    } // for
    LOG.trace("## ALOEData ## repartirComisionCliente ## Fin");
  } // repartirComisionCliente

  /* ~~~~~~~~~~~~~~~~~~~~ GETTERS ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Indica si el mercado paga canones de contratación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el mercado
   * paga canones de contratación; <code>false</code> en caso contrario.
   */
  public Boolean getMktPayCanonContr() {
    if (mktPayCanonContr == null) {
      mktPayCanonContr = new Integer(1).equals(tmct0alo.getCanoncontrpagomkt()) ? true : false;
    }
    return mktPayCanonContr;
  } // getMktPayCanonContr

  /**
   * Indica si el mercado paga canones de compensación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el mercado
   * paga canones de compensación; <code>false</code> en caso contrario.
   */
  public Boolean getMktPayCanonComp() {
    if (mktPayCanonComp == null) {
      mktPayCanonComp = new Integer(1).equals(tmct0alo.getCanoncomppagomkt()) ? true : false;
    }
    return mktPayCanonComp;
  } // getMktPayCanonComp

  /**
   * Indica si el mercado paga canones de liquidación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el mercado
   * paga canones de liquidación; <code>false</code> en caso contrario.
   */
  public Boolean getMktPayCanonLiq() {
    if (mktPayCanonLiq == null) {
      mktPayCanonLiq = new Integer(1).equals(tmct0alo.getCanonliqpagomkt()) ? true : false;
    }
    return mktPayCanonLiq;
  } // getMktPayCanonLiq

  /**
   * Indica si el cliente paga canones de contratación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el cliente
   * paga canones de contratación; <code>false</code> en caso contrario.
   */
  public Boolean getCltePayCanonContr() {
    if (cltePayCanonContr == null) {
      cltePayCanonContr = new Integer(1).equals(tmct0alo.getCanoncontrpagoclte()) ? true : false;
    }
    return cltePayCanonContr;
  } // getCltePayCanonContr

  /**
   * Indica si el cliente paga canones de compensación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el cliente
   * paga canones de compensación; <code>false</code> en caso contrario.
   */
  public Boolean getCltePayCanonComp() {
    if (cltePayCanonComp == null) {
      cltePayCanonComp = new Integer(1).equals(tmct0alo.getCanoncomppagoclte()) ? true : false;
    }
    return cltePayCanonComp;
  } // getCltePayCanonComp

  /**
   * Indica si el cliente paga canones de liquidación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el cliente
   * paga canones de liquidación; <code>false</code> en caso contrario.
   */
  public Boolean getCltePayCanonLiq() {
    if (cltePayCanonLiq == null) {
      cltePayCanonLiq = new Integer(1).equals(tmct0alo.getCanonliqpagoclte()) ? true : false;
    }
    return cltePayCanonLiq;
  } // getCltePayCanonLiq

  public Tcli0comDTO getTcli0com() {
    return tcli0com;
  }

  private BigDecimal getBrutoMercadoTotalAllo() {
    return getBrutoMercadoTotalAllo(false);
  }

  private BigDecimal getBrutoMercadoTotalAllo(boolean recalcular) {
    if (brutoMercadoTotalAllo == null || recalcular) {
      brutoMercadoTotalAllo = BigDecimal.ZERO;
      if (CollectionUtils.isNotEmpty(getActiveTmct0alcList())) {
        for (AllocateNacionalTmct0alcEconomicData allocate : getActiveTmct0alcList()) {
          brutoMercadoTotalAllo = brutoMercadoTotalAllo.add(allocate.getTmct0alc().getImefeagr());
        }
      }
    }
    return brutoMercadoTotalAllo;
  }

} // AllocateNacionalTmct0aloEconomicData

