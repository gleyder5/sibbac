package sibbac.business.wrappers.common.fileReader;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.common.RunnableProcess;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.common.SIBBACBusinessException;

import com.google.common.io.Files;

/**
 * 
 * @author XI316153
 *
 * @param <T>
 */
public abstract class SibbacFileReader<T extends RecordBean> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(SibbacFileReader.class);

  /* LISTA DE HILOS PARA PROCESAR BLOQUES DE LINEAS */
  private final List<RunnableProcess<T>> hilos = new ArrayList<>();
  /* ejecutor y gestor de hilos */
  private ExecutorService executor = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${sibbac.wrappers.thread.size}")
  private Integer threadSize;

  protected LineMapper<T> lineMapper;
  protected String fileName;
  protected String testigoName;
  protected boolean error = false;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * Constructor I. LLama al constructor de la clase padre.
   */
  public SibbacFileReader() {
    super();
  } // SibbacFileReader

  /**
   * Constructor II. Llama al constructor de la clase padre e inicializa los
   * atributods de la clase.
   * 
   * @param fileName Ruta absoluta al fichero a procesar.
   */
  public SibbacFileReader(final String fileName) {
    super();
    this.fileName = fileName;
  } // SibbacFileReader

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS ABSTRACTOS

  protected abstract void initBlockToProcess();

  protected abstract void finishBlockToProcess();

  protected abstract boolean processLine(final Map<String, List<T>> group, final T bean, final int lineNumber)
      throws SIBBACBusinessException;

  protected abstract boolean postProcessFile(final Map<String, List<T>> group) throws SIBBACBusinessException;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PROTECTED

  /**
   * 
   * @param bean
   * @return
   */
  protected boolean comprobarDistintaOrden(T beanPrev, T bean) {
    return true;
  } // comprobarDistintaOrden

  /**
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  protected boolean preProcessFile() throws SIBBACBusinessException {
    return true;
  } // preProcessFile

  /**
   * 
   * @param fieldLenghts
   * @return
   */
  protected Range[] convertLengthsIntoRanges(final int[] fieldLenghts) {
    Range[] ranges = new Range[fieldLenghts.length];
    int distAux = 1;
    for (int i = 0; i < fieldLenghts.length; i++) {
      if (fieldLenghts[i] > 0) {
        Range auxRange = new Range(distAux, distAux + fieldLenghts[i] - 1);
        distAux += fieldLenghts[i];
        ranges[i] = auxRange;
      }
    }
    return ranges;
  } // convertLengthsIntoRanges

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * 
   * NOT THREADED
   * */
  public boolean readFromSeveralEspecialesFiles(List<File> files, boolean hasTestigo, String encoding)
      throws SIBBACBusinessException {
    LOG.debug("[SibbacFileReader :: readFromSeveralFiles] Init");
    boolean wellProcessed = false;
    boolean canBeProcessed = true;
    preProcessFile();
    Map<String, List<T>> beansGroped;
    for (File fichero : files) {
      beansGroped = new HashMap<String, List<T>>();
      LOG.debug("[SibbacFileReader :: readFromSeveralFiles] Se va a procesar: " + fichero.getName());
      canBeProcessed = canBeProcessed(fichero, hasTestigo);
      if (!fichero.exists()) {
        LOG.debug("[SibbacFileReader :: readFromSeveralFiles] No sera procesado porque no existe: " + fichero.getName()
            + ")");
      }
      else if (canBeProcessed) {
        final LineIterator lineIterator = this.openFile(fichero.getAbsolutePath(), encoding);
        int lineNumber = 0;
        try {
          while (lineIterator.hasNext()) {
            final String line = lineIterator.nextLine();
            if (StringUtils.isNotEmpty(StringUtils.trimToEmpty(line))) {
              T bean = (T) lineMapper.mapLine(line, lineNumber);
              this.setFilename(fichero.getName());
              this.processLine(beansGroped, bean, lineNumber);
            }
            lineNumber++;
            if (lineNumber % 100 == 0) {
              LOG.debug("[SibbacFileReader :: readFromSeveralFiles] Reading Line: " + lineNumber);
            }
          }
          postProcessFile(beansGroped);
          wellProcessed = true;
        }
        catch (Exception e) {
          throw new SIBBACBusinessException("[SibbacFileReader: readFromSeveralFiles] Process error (line "
              + lineNumber + "): ", e);
        }
        finally {
          if (hasTestigo && wellProcessed) {
            closeAndMoveFile(fichero, lineIterator);
          }
          else {
            lineIterator.close();
          }
        }
      }
      LOG.debug("[SibbacFileReader :: readFromSeveralFiles] Fin (FILENAME: " + fileName + ")");
    }
    return wellProcessed;
  } // readFromSeveralEspecialesFiles

  public boolean procesarBeans(ObserverProcess observerProcess, List<T> beans, int numberToCommit) throws Exception {

    List<T> beanList = new ArrayList<T>();
    int countOrdenes = 0;
    int lineNumber = 0;
    boolean wellProcessed = false;
    try {
      preProcessFile();
      T beanPrev = null;
      for (T bean : beans) {
        lineNumber++;

        if (comprobarDistintaOrden(beanPrev, bean)) {

          LOG.info("[SibbacFileReader :: procesarBeans] Saliendo de comprobarDistintaOrden");
          countOrdenes++;
          LOG.info("[SibbacFileReader :: procesarBeans] lineOrden: " + countOrdenes);
          if (countOrdenes % numberToCommit == 0) {
            LOG.info("[SibbacFileReader :: procesarBeans] lineOrden % numberToCommit: " + countOrdenes % numberToCommit);
            if (this.getError()) {
              LOG.info("[SibbacFileReader :: procesarBeans] hay error");
              if (observerProcess.getException() != null) {
                LOG.error("[SibbacFileReader :: procesarBeans] observerProcess tiene excepcion: "
                    + observerProcess.getException().getMessage());
                throw new SIBBACBusinessException("[procesarBeans: readFromFile] Process error : ",
                    observerProcess.getException());
              }
              else {
                LOG.info("[SibbacFileReader :: procesarBeans] observerProcess no tiene excepcion, lanzando excepcion generica ");
                throw new Exception();
              } // else
            } // if (this.getError())

            executeProcessInThread(observerProcess, beanList, lineNumber);

            beanList = new ArrayList<T>();
          } // if (lineOrden % numberToCommit == 0)
        } // if (comprobarDistintaOrden(bean))
        beanList.add(bean);
        beanPrev = bean;
      }

      if (CollectionUtils.isNotEmpty(beanList)) {
        LOG.info("[SibbacFileReader :: procesarBeans] Procesando ultimos");

        executeProcessInThread(observerProcess, beanList, lineNumber);
        beanList = new ArrayList<T>();
      }// if ( lineNumber % numberToCommit != 0 ) {

      if (this.getError()) {
        LOG.info("[SibbacFileReader :: procesarBeans] Hay error");
        if (observerProcess.getException() != null) {
          LOG.error("[SibbacFileReader :: procesarBeans] observerProcess tiene excepcion: "
              + observerProcess.getException().getMessage());
          throw new SIBBACBusinessException("[SibbacFileReader: procesarBeans] Process error : ",
              observerProcess.getException());
        }
        else {
          LOG.info("[SibbacFileReader :: procesarBeans] observerProcess no tiene excepcion, lanzando excepcion generica ");
          throw new Exception();
        } // else
      } // if

      esperarPorThreadPool(0);

      postProcessFile(null);
      wellProcessed = true;
    }
    catch (Exception e) {
      LOG.error("[SibbacFileReader :: procesarBeans] Error final: " + e.getMessage(), e);
      throw new SIBBACBusinessException(
          "[SibbacFileReader: readFromFile] Capturada excepcion generica. Process error (line " + lineNumber + "): ", e);
    }
    finally {
      if (executor != null) {
        executor.shutdown();
      }

    } // finally
    LOG.debug("[SibbacFileReader :: procesarBeans] Fin (FILENAME: " + fileName + ")");
    return wellProcessed;
  }

  /**
   * Threaded
   * */
  public boolean readFromFile(String fileName, boolean chekIfHasTestigo, String encoding, int numberToCommit)
      throws SIBBACBusinessException {
    // this.fileName = fileName;
    LOG.debug("[SibbacFileReader :: readFromFile] Init (FILENAME: " + fileName + ")");
    boolean wellProcessed = false;
    boolean canBeProcessed = canBeProcessed(chekIfHasTestigo);
    List<T> beanList = new ArrayList<T>();

    File fichero = new File(fileName);
    if (!fichero.exists()) {
      LOG.debug("[SibbacFileReader :: readFromFile] No sera procesado porque no existe: " + fileName + ")");
    }
    else if (canBeProcessed) {
      final LineIterator lineIterator = this.openFile(fileName, encoding);
      Integer lineNumber = 0;
      Integer countOrdenes = 0;

      preProcessFile();
      ObserverProcess observerProcess = new ObserverProcess();

      try {
        T beanPrev = null;
        while (lineIterator.hasNext()) {
          final String line = lineIterator.nextLine();
          if (StringUtils.isNotEmpty(StringUtils.trimToEmpty(line))) {
            T bean = null;
            lineNumber++;
            try {
              LOG.info("[SibbacFileReader :: readFromFile] Procesando linea '{}' ", lineNumber);
              bean = (T) lineMapper.mapLine(line, lineNumber);
              LOG.info("[SibbacFileReader :: readFromFile] Linea procesada correctamente");
            }
            catch (Exception e) {
              LOG.error("[SibbacFileReader :: readFromFile] linea erronea en el fichero " + lineNumber);
              throw e;
            }
            if (bean != null) {

              if (comprobarDistintaOrden(beanPrev, bean)) {

                LOG.info("[SibbacFileReader :: readFromFile] Saliendo de comprobarDistintaOrden");
                countOrdenes++;
                LOG.info("[SibbacFileReader :: readFromFile] lineOrden: " + countOrdenes);
                if (countOrdenes % numberToCommit == 0) {
                  LOG.info("[SibbacFileReader :: readFromFile] lineOrden % numberToCommit: " + countOrdenes
                      % numberToCommit);
                  if (this.getError()) {
                    LOG.info("[SibbacFileReader :: readFromFile] hay error");

                    if (observerProcess.getException() != null) {
                      LOG.error("[SibbacFileReader :: readFromFile] observerProcess tiene excepcion: "
                          + observerProcess.getException().getMessage());
                      throw new SIBBACBusinessException("[SibbacFileReader: readFromFile] Process error : ",
                          observerProcess.getException());
                    }
                    else {
                      LOG.info("[SibbacFileReader :: readFromFile] observerProcess no tiene excepcion, lanzando excepcion generica ");

                      throw new Exception();
                    } // else
                  } // if (this.getError())

                  executeProcessInThread(observerProcess, beanList, lineNumber);

                  beanList = new ArrayList<T>();
                } // if (lineOrden % numberToCommit == 0)
              } // if (comprobarDistintaOrden(bean))
              beanList.add(bean);

              beanPrev = bean;

            } // if (bean != null)
          } // if (StringUtils.isNotEmpty(StringUtils.trimToEmpty(line)))
        } // while (lineIterator.hasNext())

        // Para guardar los ultimos
        if (CollectionUtils.isNotEmpty(beanList)) {
          LOG.info("[SibbacFileReader :: readFromFile] Procesando ultimos");

          executeProcessInThread(observerProcess, beanList, lineNumber);
          beanList = new ArrayList<T>();
        }// if ( lineNumber % numberToCommit != 0 ) {

        if (this.getError()) {
          LOG.info("[SibbacFileReader :: readFromFile] Hay error");
          if (observerProcess.getException() != null) {
            LOG.error("[SibbacFileReader :: readFromFile] observerProcess tiene excepcion: "
                + observerProcess.getException().getMessage());
            throw new SIBBACBusinessException("[procesarBeans: readFromFile] Process error : ",
                observerProcess.getException());
          }
          else {
            LOG.info("[SibbacFileReader :: readFromFile] observerProcess no tiene excepcion, lanzando excepcion generica ");
            throw new Exception();
          } // else
        } // if

        esperarPorThreadPool(0);

        postProcessFile(null);

        wellProcessed = true;
      }
      catch (Exception e) {
        LOG.error("[SibbacFileReader :: readFromFile] Error final: " + e.getMessage(), e);
        throw new SIBBACBusinessException(
            "[SibbacFileReader: readFromFile] Capturada excepcion generica. Process error (line " + lineNumber + "): ",
            e);
      }
      finally {

        if (executor != null) {
          executor.shutdown();
          executor = null;
        }
        if (chekIfHasTestigo && wellProcessed) {
          closeAndMoveFile(lineIterator);
        }
        else {
          closeFile(lineIterator);
        }
      } // finally
    } // else
    LOG.debug("[SibbacFileReader :: readFromFile] Fin (FILENAME: " + fileName + ")");
    return wellProcessed;

  }

  /**
   * Threaded
   * */
  public boolean readFromFile(boolean chekIfHasTestigo, String encoding, int numberToCommit)
      throws SIBBACBusinessException {
    return readFromFile(fileName, chekIfHasTestigo, encoding, numberToCommit);
  } // readFromFile

  private void esperarPorThreadPool(int esperarSiHilosGt) {
    while (hilos.size() > esperarSiHilosGt) {
      LOG.info("[esperarPorThreadPool] *** ESPERANDO POR HILOS ACTIVOS {} .....***", hilos.size());
      try {
        Thread.sleep(1000);
      }
      catch (InterruptedException e) {
        LOG.warn(e.getMessage(), e);
      }
    }
  }

  @Transactional
  private void executeProcessInThread(ObserverProcess observerProcess, List<T> beanList, int lineaFichero) {
    LOG.debug("[SibbacFileReader :: executeProcessInThread] Init ");
    if (executor == null || executor.isShutdown()) {
      LOG.info("[SibbacFileReader :: executeProcessInThread] Creando Thread Pool size : " + threadSize);
      executor = Executors.newFixedThreadPool(threadSize);
    }

    int esperarThreads = threadSize;

    if (esperarThreads > 0) {
      esperarThreads = esperarThreads - 1;
    }
    else {
      esperarThreads = 0;
    }
    // Esperamos a que haya un hueco

    LOG.info(
        "[SibbacFileReader :: executeProcessInThread] Esperaremos hasta que haya un maximo de {} threads activos.",
        esperarThreads);
    esperarPorThreadPool(esperarThreads);

    RunnableProcess<T> runnable = new RunnableProcess<T>(this, observerProcess, beanList, lineaFichero);

    executor.execute(runnable);
    hilos.add(runnable);
    LOG.debug("[SibbacFileReader :: executeProcessInThread] Fin ");
  }

  /**
   * Not threaded
   * */
  public boolean readFromFile(boolean hasTestigo, String encoding) throws SIBBACBusinessException {
    LOG.debug("[readFromFile] Init (FILENAME: {})", fileName);
    boolean wellProcessed = false;
    boolean canBeProcessed = canBeProcessed(hasTestigo);

    File fichero = new File(fileName);
    if (!fichero.exists()) {
      LOG.debug("[readFromFile] No sera procesado porque {} no existe", fileName);
    }
    else if (canBeProcessed) {
      final LineIterator lineIterator = this.openFile(fileName, encoding);
      int lineNumber = 0;
      preProcessFile();
      try {
        Map<String, List<T>> beansGroped = new HashMap<String, List<T>>();
        while (lineIterator.hasNext()) {
          final String line = lineIterator.nextLine();
          if (StringUtils.isNotEmpty(StringUtils.trimToEmpty(line))) {
            T bean = (T) lineMapper.mapLine(line, lineNumber);
            this.processLine(beansGroped, bean, lineNumber);
          }
          lineNumber++;
          if (lineNumber % 100 == 0) {
            LOG.debug("[readFromFile] Reading Line: {}", lineNumber);
          }
        }// fin while
        postProcessFile(beansGroped);
        wellProcessed = true;
      }
      catch (Exception e) {
        final String message = MessageFormat.format("Error process in the line {0} of {1}", lineNumber, fileName);
        LOG.error("[readFromFile] {} Message : {}", message, e.getMessage());
        throw new SIBBACBusinessException(message, e);
      }
      finally {
        if (wellProcessed) {
          closeAndMoveFile(fichero, lineIterator);
        }
        else {
          closeFile(lineIterator);
        }
      }
    }
    LOG.debug("[SibbacFileReader :: readFromFile sin transaction size] Fin (FILENAME: " + fileName + ")");
    return wellProcessed;
  }

  // @Transactional(propagation = Propagation.REQUIRES_NEW)
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  private int saveAndCommit(List<T> beanList, int lineNumber) throws SIBBACBusinessException {
    LOG.debug("[SibbacFileReader :: saveAndCommit] Se va a empezar el guardado y commit de la línea: " + lineNumber);
    Map<String, List<T>> group = new HashMap<String, List<T>>();
    for (T bean : beanList) {
      try {
        initBlockToProcess();
        this.processLine(group, bean, lineNumber);
      }
      catch (Exception e) {
        LOG.error(e.getMessage() + " Causa: " + e.getCause(), e);
        throw new SIBBACBusinessException("[SibbacFileReader: readFromFile] Process error (line " + lineNumber + "): ",
            e);
      }
      lineNumber++;
    }
    try {
      finishBlockToProcess();
    }
    catch (Exception e) {
      LOG.error(e.getMessage() + " Causa: " + e.getCause(), e);
      throw new SIBBACBusinessException("[SibbacFileReader: readFromFile] Process error (line " + lineNumber + "): ", e);
    }
    LOG.debug("[SibbacFileReader :: saveAndCommit] Se ha terminado el guardado y se va a hacer el commit hasta la línea: "
        + lineNumber);
    return lineNumber;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public int saveAndCommitRunnable(List<T> beanList, int lineNumber) throws SIBBACBusinessException {
    LOG.debug("[SibbacFileReader :: saveAndCommitRunnable] Se va a empezar el guardado y commit de la línea: "
        + lineNumber);
    try {
      SibbacFileReader<T> clone = extracted();
      SibbacFileReader<T> sibbacFileReader = clone;
      sibbacFileReader.saveAndCommit(beanList, lineNumber);
      // this.saveAndCommit(beanList, lineNumber);
      // sibbacFileReader.postProcessFile();
    }
    catch (Exception e) {
      LOG.error("[SibbacFileReader: saveAndCommitRunnable] Process error (line " + lineNumber + "): ", e);
      LOG.error(e.getMessage() + " Causa: " + e.getCause(), e);
      throw new SIBBACBusinessException("[SibbacFileReader: saveAndCommitRunnable] Process error (line " + lineNumber
          + "): ", e);
    }
    finally {
    }

    LOG.debug("[SibbacFileReader :: saveAndCommit] Se ha terminado el guardado y se va a hacer el commit hasta la línea: "
        + lineNumber);
    return lineNumber + CollectionUtils.size(beanList);
  }

  public void setFilename(String fileName) {
    this.fileName = fileName;
  }

  public void closeAndMoveFile(LineIterator lineIterator, String importedFileName) {
    LOG.debug("[SibbacFileReader :: closeAndMoveFile] Init (FILENAME: " + importedFileName + ")");
    lineIterator.close();
    SimpleDateFormat dateFormat = new SimpleDateFormat(".yyyyMMddHHmmss");
    try {
      File fileOrigen = new File(importedFileName);
      File folderDestino = new File(fileOrigen.getParentFile(), "tratados");
      folderDestino.mkdirs();
      String dateToConcat = dateFormat.format(new Date());
      Files.move(fileOrigen, new File(folderDestino, fileOrigen.getName().concat(dateToConcat)));
      LOG.debug("[SibbacFileReader :: closeAndMoveFile] Fichero movido a carpeta tratados");
    }
    catch (Exception e) {
      LOG.error("[SibbacFileReader :: closeAndMoveFile] Error moviendo fichero procesado (FILENAME: "
          + importedFileName + ")");

    }
    LOG.debug("[SibbacFileReader :: closeAndMoveFile] Fin (FILENAME: " + importedFileName + ")");
  }

  public LineIterator openFile(String pathToFile, String encoding) throws SIBBACBusinessException {
    if (StringUtils.isEmpty(pathToFile)) {
      throw new SIBBACBusinessException("SibbacFileReader: openFile: provided path is null");
    }
    final File file = new File(pathToFile);
    LineIterator lineIterator;
    try {
      lineIterator = FileUtils.lineIterator(file, encoding);
    }
    catch (IOException e) {
      throw new SIBBACBusinessException("SibbacFileReader: readFile: read error", e);
    }
    return lineIterator;
  }

  public void closeFile(LineIterator lineIterator) {
    lineIterator.close();
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  public boolean getError() {
    return error;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  public void setError(boolean error) {
    this.error = error;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * 
   * @return
   * @throws CloneNotSupportedException
   */
  @SuppressWarnings("unchecked")
  private SibbacFileReader<T> extracted() throws CloneNotSupportedException {
    return (SibbacFileReader<T>) clone();
  } // extracted

  /**
   * 
   * @param hasTestigo
   * @return
   */
  private boolean canBeProcessed(boolean hasTestigo) {
    boolean canBeProcessed = true;
    if (hasTestigo) {
      testigoName = FilenameUtils.removeExtension(fileName).concat(".TST");
      try {
        File testigo = new File(testigoName);
        if (!testigo.exists()) {
          // Si no existe el testigo no se podra procesar
          canBeProcessed = false;
          LOG.debug("[SibbacFileReader :: readFromFile] No sera procesado porque no existe testigo: " + testigoName
              + ")");
        }
      }
      catch (Exception e) {
        // Si no existe el testigo no se podra procesar
        canBeProcessed = false;
        LOG.debug("[SibbacFileReader :: readFromFile] No sera procesado porque no existe testigo: " + testigoName + ")");
      }
    }
    return canBeProcessed;
  } // canBeProcessed

  /**
   * 
   * @param fichero
   * @param hasTestigo
   * @return
   */
  private boolean canBeProcessed(File fichero, boolean hasTestigo) {
    boolean canBeProcessed = true;
    if (hasTestigo) {
      testigoName = FilenameUtils.removeExtension(fichero.getAbsolutePath()).concat(".TST");
      try {
        File testigo = new File(testigoName);
        if (!testigo.exists()) {
          // Si no existe el testigo no se podra procesar
          canBeProcessed = false;
          LOG.debug("[SibbacFileReader :: readFromFile] No sera procesado porque no existe testigo: " + testigoName
              + ")");
        }
      }
      catch (Exception e) {
        // Si no existe el testigo no se podra procesar
        canBeProcessed = false;
        LOG.debug("[SibbacFileReader :: readFromFile] No sera procesado porque no existe testigo: " + testigoName + ")");
      }
    }
    return canBeProcessed;
  } // canBeProcessed

  /**
   * 
   * @param lineIterator
   */
  private void closeAndMoveFile(LineIterator lineIterator) {
    LOG.debug("[SibbacFileReader :: closeAndMoveFile] Init (FILENAME: " + fileName + ")");
    lineIterator.close();
    SimpleDateFormat dateFormat = new SimpleDateFormat(".yyyyMMddHHmmss");
    try {
      File fileOrigen = new File(fileName);
      File fileOrigenTestigo = new File(testigoName);
      if (fileOrigenTestigo.exists()) {
        fileOrigenTestigo.delete();
      }
      File folderDestino = new File(fileOrigen.getParentFile(), "tratados");
      folderDestino.mkdirs();
      String dateToConcat = dateFormat.format(new Date());
      Files.move(fileOrigen, new File(folderDestino, fileOrigen.getName().concat(dateToConcat)));
      LOG.debug("[SibbacFileReader :: closeAndMoveFile] Fichero movido a carpeta tratados");
    }
    catch (Exception e) {
      LOG.error("[SibbacFileReader :: closeAndMoveFile] Error moviendo fichero procesado (FILENAME: " + fileName + ")");
    }
    LOG.debug("[SibbacFileReader :: closeAndMoveFile] Fin (FILENAME: " + fileName + ")");
  } // closeAndMoveFile

  /**
   * 
   * @param fichero
   * @param lineIterator
   */
  private void closeAndMoveFile(File fichero, LineIterator lineIterator) {
    LOG.debug("[SibbacFileReader :: closeAndMoveFile] Init (FILENAME: " + fichero.getName() + ")");
    lineIterator.close();
    SimpleDateFormat dateFormat = new SimpleDateFormat(".yyyyMMddHHmmss");
    try {
      File fileOrigenTestigo = new File(fichero.getParent() + File.separator
          + FilenameUtils.getBaseName(fichero.getPath()) + ".TST");
      fileOrigenTestigo.delete();
      File folderDestino = new File(fichero.getParentFile(), "tratados");
      folderDestino.mkdirs();
      String dateToConcat = dateFormat.format(new Date());
      Files.move(fichero, new File(folderDestino, fichero.getName().concat(dateToConcat)));
      LOG.debug("[SibbacFileReader :: closeAndMoveFile] Fichero movido a carpeta tratados");
    }
    catch (Exception e) {
      LOG.error("[SibbacFileReader :: closeAndMoveFile] Error moviendo fichero procesado (FILENAME: "
          + fichero.getName() + ")");
    }
    LOG.debug("[SibbacFileReader :: closeAndMoveFile] Fin (FILENAME: " + fileName + ")");
  } // closeAndMoveFile

  public synchronized void shutdownRunnable(RunnableProcess<T> current) {
    hilos.remove(current);
  }

  public void logError(String message, Throwable th) {
    LOG.warn("[logInciencia] Error ejecutando hilo desde la tarea  clase dónde ocurrió la incidencia: {} causa: ", th
        .getClass().getName(), th.getCause());
  }

} // SibbacFileReader
