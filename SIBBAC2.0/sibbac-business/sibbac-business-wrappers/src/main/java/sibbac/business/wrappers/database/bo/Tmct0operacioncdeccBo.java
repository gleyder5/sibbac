package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0operacioncdeccDao;
import sibbac.business.wrappers.database.dao.Tmct0operacioncdeccDaoImpl;
import sibbac.business.wrappers.database.dao.specifications.OperacioncdeccSpecifications;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0operacioncdeccBo extends AbstractBo<Tmct0operacioncdecc, Long, Tmct0operacioncdeccDao> {

  @Autowired
  private Tmct0operacioncdeccDaoImpl opimpDao;

  public List<String> getLcdoperacionecc() {
    return this.dao.getLCdoperacion();
  }

  public List<Object[]> getTitulosCuentaDiariaConFiltro(List<String> operaciones, HashMap<String, Object> filtros) {
    return opimpDao.getTitulosCuentaDiariaConFiltro(operaciones, filtros);
  }

  public List<Object[]> getTitulosCuentaDiariaConFiltrosYCDOperacion(HashMap<String, Serializable> filtros, Date ayer,
      Date hoy) {
    return opimpDao.getTitulosCuentaDiariaConFiltrosYCDOperacion(filtros, ayer, hoy);
  }

  public Tmct0operacioncdecc findByCdoperacionecc(String cdoperacionecc) {
    return dao.findByCdoperacionecc(cdoperacionecc);
  }

  public List<Tmct0operacioncdecc> findByInCdoperacionecc(List<String> cdoperacionecc) {
    return dao.findByInCdoperacionecc(cdoperacionecc);
  }

  /**
   * @return
   */
  @Transactional
  public List<Tmct0operacioncdecc> findOperacionesForDiaria(final Date fechaContratacionDesde,
      final Date fechaContratacionHasta, final Date fechaLiquidacionDesde, final Date fechaLiquidacionHasta,
      final String camara, final String isin, final String cuenta) {
    final List<Tmct0operacioncdecc> operaciones = dao.findAll(OperacioncdeccSpecifications.listadoFriltrado(
        fechaContratacionDesde, fechaContratacionHasta, fechaLiquidacionDesde, fechaLiquidacionHasta, camara, isin,
        cuenta, true));
    return operaciones;
  }

  public Tmct0operacioncdecc findByIdCase(Long idcase) {
    return dao.findByIdCase(idcase);
  }

  public List<String> findAllCdoperacioneccEnvioS3(Date fecontratacion) {
    return dao.findAllCdoperacioneccEnvioS3(fecontratacion);
  }

  public Tmct0operacioncdecc findTmct0operacioncdeccByInformacionMercado(String nuordenmkt, String nuexemkt,
      Date fcontratacion, String cdoperacionmkt, String cdisin, String cdsegmento, Character cdsentido,
      String cdmiembromkt) {
    return dao.findTmct0operacioncdeccByInformacionMercado(nuordenmkt, nuexemkt, fcontratacion, cdoperacionmkt, cdisin,
        cdsegmento, cdsentido, cdmiembromkt);
  }

  public List<Tmct0operacioncdecc> findOpEccForCe(String cdoperacionmkt, String cdisin, String nuexemkt, Date fcontratacion,
      String cdsegmento, String cdmiembromkt) {
    return dao
        .findAllByCdoperacionmktAndCdisinAndNuexemktAndFcontratacionAndCdsegmentoAndCdsentidoAndCdmiembromktWithCase(
            cdoperacionmkt, cdisin, nuexemkt, fcontratacion, cdsegmento, cdmiembromkt);

  }

}
