/**
 * 
 */
package sibbac.business.wrappers.database.exceptions;

/**
 * @author xIS16630
 *
 */
public class LockUserException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -837985242767033776L;

  /**
   * 
   */
  public LockUserException() {
  }

  /**
   * @param arg0
   */
  public LockUserException(String arg0) {
    super(arg0);
  }

  /**
   * @param arg0
   */
  public LockUserException(Throwable arg0) {
    super(arg0);
  }

  /**
   * @param arg0
   * @param arg1
   */
  public LockUserException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  /**
   * @param arg0
   * @param arg1
   * @param arg2
   * @param arg3
   */
  public LockUserException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

}
