package sibbac.business.wrappers.database.bo.fichero1015;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class EnvioFichero1015 {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(EnvioFichero1015.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Ruta al fichero de configuración del proceso. */
  @Value("${MQ.partenon.desgloses.out.folder}")
  private String path;

  // TODO QUITAR EL VALOR POR DEFECTO DE ESTOS PARAMETROS

  @Value("${sibbac.wrappers.operacion.especial.1015.patenon.ftp.codigos.clearer:}")
  private String pathPartenonCodigosClearer;

  @Value("${sibbac.wrappers.operacion.especial.1015.patenon.ftp.folder:}")
  private String pathPartenonOutFtp;

  @Value("${sibbac.wrappers.operacion.especial.1015.patenon.ftp.file.prefix:}")
  private String partenonFtpFilePrefix;

  @Value("${sibbac.wrappers.operacion.especial.1015.patenon.ftp.file.name:}")
  private String partenonOutFtpFileName;
  @Value("${sibbac.wrappers.operacion.especial.1015.patenon.ftp.file.name.tst:}")
  private String partenonOutFtpFileNameTst;

  @Value("${sibbac.wrappers.operacion.especial.1015.openbank.ftp.codigos.clearer:}")
  private String pathOpenBankCodigosClearer;

  @Value("${sibbac.wrappers.operacion.especial.1015.openbank.ftp.folder:}")
  private String pathOpenBankOutFtp;

  @Value("${sibbac.wrappers.operacion.especial.1015.openbank.ftp.file.prefix:}")
  private String openBankFtpFilePrefix;

  @Value("${sibbac.wrappers.operacion.especial.1015.openbank.ftp.file.name:}")
  private String openBankOutFtpFileName;

  @Value("${sibbac.wrappers.operacion.especial.1015.openbank.ftp.file.name.tst:}")
  private String openBankOutFtpFileNameTst;

  @Value("${MQ.partenon.desgloses.out.file}")
  private String file;

  @Value("${sibbac.wrappers.operacion.especial.1015.file.prefix}")
  private String filePrefix;

  @Autowired
  private EnvioFichero1015SubList enviarFichero1015SubList;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Value("${sibbac.wrappers.thread.size}")
  private Integer threadSize;

  @Value("${sibbac.wrappers.operacion.especial.1015.codigos.comerciales}")
  private String codigosComerciales;

  @Value("${sibbac.wrappers.large.query.page.size}")
  private int largeQueryPageSize;

  @Value("${sibbac.wrappers.operacion.especial.1015.transaction.size}")
  private int transactionSize;

  @Value("${sibbac.wrappers.operacion.especial.1015.max.execution.pages}")
  private int maxExecutionPages;

  @Autowired
  private Tmct0menBo menBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  public int getThreadSize() {
    return threadSize;
  }

  public int getTransactionSize() {
    return transactionSize;
  }

  public void executeTask() throws Exception {
    try {
      Path tmp = Paths.get(path, "tmp");
      if (Files.notExists(tmp)) {
        Files.createDirectories(tmp);
      }
    }
    catch (IOException e) {
      LOG.warn("[EnvioFichero1015 :: preExecuteTask] Incidencia NO se han podido crear los directorios necesarios", e);
      return;
    }

    try {
      Path tmp = Paths.get(pathPartenonOutFtp);
      if (Files.notExists(tmp)) {
        Files.createDirectories(tmp);
      }
    }
    catch (IOException e) {
      LOG.warn("[EnvioFichero1015 :: preExecuteTask] Incidencia NO se han podido crear los directorios necesarios", e);
      return;
    }
    try {
      Path tmp = Paths.get(pathOpenBankOutFtp);
      if (Files.notExists(tmp)) {
        Files.createDirectories(tmp);
      }
    }
    catch (IOException e) {
      LOG.warn("[EnvioFichero1015 :: preExecuteTask] Incidencia NO se han podido crear los directorios necesarios", e);
      return;
    }

    List<String> codigos = Arrays.asList(codigosComerciales.split(","));
    List<DesgloseRecordDTO> desgloses = getListaOperacionesEspecialesPdte1015();

    if (!desgloses.isEmpty()) {

      Map<String, List<DesgloseRecordDTO>> desglosesAgrupadosBanco = this.agruparPorBanco(desgloses);
      desgloses.clear();
      for (String banco : desglosesAgrupadosBanco.keySet()) {
        this.ejecutarByBanco(banco, codigos, desglosesAgrupadosBanco.get(banco));
      }

    }
  };

  private void ejecutarByBanco(String banco, List<String> codigos, List<DesgloseRecordDTO> desgloses)
      throws SIBBACBusinessException {
    LOG.trace("[ejecutarByBanco] inicio");
    if (desgloses != null && !desgloses.isEmpty()) {
      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat(String.format("Thread-EnvioFichero1015-%s-%s", banco, "%d"));
      ExecutorService executor = Executors.newFixedThreadPool(getThreadSize(), tfb.build());
      List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
      try {
        this.lanzarProcesos(executor, banco, codigos, desgloses, listFutures);
        executor.shutdown();
        this.recuperarResultadoProcesos(executor, listFutures);
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
        executor.shutdownNow();
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
      finally {
        listFutures.clear();
        desgloses.clear();
      }

    }
    LOG.trace("[ejecutarByBanco] fin");
  }

  private Map<String, List<DesgloseRecordDTO>> agruparPorBanco(List<DesgloseRecordDTO> desgloses) {
    LOG.trace("[agruparPorBanco] inicio");
    List<String> clearersPartenon = new ArrayList<String>();
    List<String> clearesOpenBank = new ArrayList<String>();

    if (pathPartenonCodigosClearer != null && !pathPartenonCodigosClearer.trim().isEmpty()) {
      clearersPartenon.addAll(Arrays.asList(pathPartenonCodigosClearer.split(",")));
    }

    if (pathOpenBankCodigosClearer != null && !pathOpenBankCodigosClearer.trim().isEmpty()) {
      clearesOpenBank.addAll(Arrays.asList(pathOpenBankCodigosClearer.split(",")));
    }
    Map<String, List<DesgloseRecordDTO>> res = new HashMap<String, List<DesgloseRecordDTO>>();
    res.put(this.partenonFtpFilePrefix, new ArrayList<DesgloseRecordDTO>());
    res.put(this.openBankFtpFilePrefix, new ArrayList<DesgloseRecordDTO>());
    for (DesgloseRecordDTO d : desgloses) {
      if (d.getEmprCTVA() != null && clearersPartenon.contains(d.getEmprCTVA().trim())) {
        res.get(this.partenonFtpFilePrefix).add(d);
      }
      else if (d.getEmprCTVA() != null && clearesOpenBank.contains(d.getEmprCTVA().trim())) {
        res.get(this.openBankFtpFilePrefix).add(d);
      }
      else {
        LOG.warn(
            "[agruparPorBanco] codigo emprctva: {} no encontrado en las listas de clearers partenon y openbank {}-{}. VIAJA POR PARTENON.",
            d.getEmprCTVA(), clearersPartenon, clearesOpenBank);
        res.get(this.partenonFtpFilePrefix).add(d);
      }
    }
    LOG.trace("[agruparPorBanco] fin");
    return res;
  }

  private void lanzarProcesos(ExecutorService executor, String banco, List<String> codigosComerciales,
      List<DesgloseRecordDTO> desgloses, List<Future<Void>> listFutures) {
    int posIni = 0;
    int posEnd = 0;
    Future<Void> future;
    EnvioFichero1015Callable callable;
    if (desgloses != null && !desgloses.isEmpty()) {
      while (posEnd < desgloses.size()) {
        posEnd = Math.min(posEnd + getTransactionSize(), desgloses.size());
        callable = new EnvioFichero1015Callable(banco, codigosComerciales, desgloses.subList(posIni, posEnd),
            enviarFichero1015SubList);
        future = executor.submit(callable);
        listFutures.add(future);
        posIni = posEnd;
      }
    }
  }

  private void recuperarResultadoProcesos(ExecutorService executor, List<Future<Void>> listFutures)
      throws SIBBACBusinessException {
    try {
      executor.awaitTermination((long) listFutures.size() + 5, TimeUnit.MINUTES);
    }
    catch (InterruptedException e) {
      LOG.warn(e.getMessage(), e);
    }

    for (Future<Void> future : listFutures) {
      try {
        future.get(10, TimeUnit.SECONDS);
      }
      catch (InterruptedException | ExecutionException | TimeoutException e) {
        throw new SIBBACBusinessException(e.getMessage(), e);
      }
    }
  }

  private List<DesgloseRecordDTO> getListaOperacionesEspecialesPdte1015() throws SIBBACBusinessException {
    final List<DesgloseRecordDTO> list = new ArrayList<DesgloseRecordDTO>();

    final Pageable page = new PageRequest(0, maxExecutionPages * largeQueryPageSize);
    LOG.debug("[EnvioFichero1015 :: getListaOperacionesEspecialesPdte1015] buscando lista de nurefords en EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR");
    LOG.debug("[EnvioFichero1015 :: getListaOperacionesEspecialesPdte1015] nurefords {}", page);
    List<String> nurefords = desgloseRecordBeanBo
        .findAllNurefordOperacionesEspeciales(
            EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSE_EJECUTADO_CORRECTO_ALLO_CREADO.getId(),
            page);

    LOG.debug("[EnvioFichero1015 :: getListaOperacionesEspecialesPdte1015] encontrados {} nurefors", nurefords.size());
    if (CollectionUtils.isNotEmpty(nurefords)) {
      ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
      tfb.setNameFormat("Thread-LecturaRegistros2040Envio1015-%d");
      ExecutorService executor = Executors.newFixedThreadPool(getThreadSize(), tfb.build());
      List<Future<List<DesgloseRecordDTO>>> listFutures = new ArrayList<Future<List<DesgloseRecordDTO>>>();
      try {
        int posIni = 0;
        int posEnd = 0;
        while (posEnd < nurefords.size()) {
          posEnd += largeQueryPageSize;
          if (posEnd > nurefords.size()) {
            posEnd = nurefords.size();
          }

          LOG.debug("[EnvioFichero1015 :: getListaOperacionesEspecialesPdte1015] pageable posIni : {} posEnd : {}",
              posIni, posEnd);
          final List<String> nurefordSubList = new ArrayList<String>(nurefords.subList(posIni, posEnd));
          LOG.debug("[EnvioFichero1015 :: getListaOperacionesEspecialesPdte1015] Lanzando hilo para leer...");

          Callable<List<DesgloseRecordDTO>> process = new Callable<List<DesgloseRecordDTO>>() {

            @Override
            public List<DesgloseRecordDTO> call() throws Exception {
              LOG.debug("[call] inicio.");
              LOG.debug("[call] buscando desgloses.");
              List<DesgloseRecordDTO> listSub = desgloseRecordBeanBo
                  .findAllDesgloseRecordDTOByListNurefordToEnvio1015(nurefordSubList);
              LOG.debug("[call] encontrados {} desgloses.", listSub.size());
              LOG.debug("[call] fin.");
              return listSub;

            }
          };
          listFutures.add(executor.submit(process));

          LOG.debug("[EnvioFichero1015 :: getListaOperacionesEspecialesPdte1015] hilo lanzado");
          posIni = posEnd;
        }
        executor.shutdown();

        executor.awaitTermination((long) listFutures.size() + 10, TimeUnit.MINUTES);

        for (Future<List<DesgloseRecordDTO>> future : listFutures) {
          list.addAll(future.get(10, TimeUnit.SECONDS));
        }
      }
      catch (Exception e) {
        executor.shutdownNow();
        throw new SIBBACBusinessException(e);
      }
      finally {
        listFutures.clear();
        nurefords.clear();
      }

    }
    LOG.debug("[EnvioFichero1015 :: run] lista total {} desgloses.", list.size());
    return list;
  }

  /**
   * 
   * @throws Exception
   */
  @Transactional
  public void appendOutFiles() throws Exception {
    LOG.trace("[EnvioFichero1015 :: appendOutFiles] inicio");

    Tmct0men semaforo = menBo.findByTipo(TIPO_APUNTE.TASK_COMUNICACIONES_MQ_OUT);
    if (semaforo != null && semaforo.getTmct0sta() != null) {
      LOG.debug("[appendOutFiles] semaforo: {} en estado: {}", semaforo.getCdmensa(), semaforo.getTmct0sta().getId());
      if (semaforo.getTmct0sta().getId().getCdestado().trim().equals(TMCT0MSC.BLOQUEADO_USUARIO.getCdmensa())) {
        Path rootPath = Paths.get(path);
        Path output = Paths.get(pathPartenonOutFtp);
        this.appendFilesIfExists(rootPath, partenonFtpFilePrefix, output, partenonOutFtpFileName,
            partenonOutFtpFileNameTst, false);

        output = Paths.get(pathOpenBankOutFtp);
        this.appendFilesIfExists(rootPath, openBankFtpFilePrefix, output, openBankOutFtpFileName,
            openBankOutFtpFileNameTst, false);
      }
      else {
        LOG.debug("[appendOutFiles] semaforo: {} no esta bloqueado no hacemos append.", semaforo.getCdmensa());
      }
    }
    LOG.trace("[EnvioFichero1015 :: appendOutFiles] fin");
  }

  private void appendFilesIfExists(Path rootPath, String banco, Path output, String outputFileName,
      String tstOutputFileName, boolean putTst) throws IOException {
    LOG.trace("[appendFilesIfExists] inicio");
    boolean hasFiles = false;
    String inputPattern = String.format("*%s*%s*%s*", filePrefix, banco, file);
    LOG.debug("[appendFilesIfExists] inpath: {} pattern: {} outPath: {} outFile: {}, tstFile: {}", rootPath.toString(),
        inputPattern, output.toString(), outputFileName, tstOutputFileName);
    try (DirectoryStream<Path> ficherosInput = Files.newDirectoryStream(rootPath, inputPattern);) {
      hasFiles = ficherosInput.iterator().hasNext();
    }

    if (hasFiles) {
      this.appendFiles(rootPath, inputPattern, output, outputFileName, tstOutputFileName, putTst);
    }
    LOG.trace("[appendFilesIfExists] fin");
  }

  private void appendFiles(Path rootPath, String inputPatter, Path output, String outputFileName,
      String tstOutputFileName, boolean putTst) throws IOException {
    LOG.trace("[appendFiles] inicio");
    ZipEntry zipEntry;
    Path pathTratados = Paths.get(output.toString(), "tratados");
    if (Files.notExists(pathTratados)) {
      Files.createDirectories(pathTratados);
    }
    Path fileOutput = Paths.get(output.toString(), outputFileName);
    Path fileOutputTst = Paths.get(output.toString(), tstOutputFileName);

    String concurrent = FormatDataUtils.convertDateToConcurrentFileName(new Date());
    String tmpName = String.format("%s_%s.TMP", outputFileName, concurrent);

    Path fileOutputTmp = Paths.get(output.toString(), tmpName);

    Path zipOutTratados = Paths.get(pathTratados.toString(),
        String.format("%s_%s.zip", inputPatter.replace("*", ""), concurrent));

    if (Files.exists(fileOutputTst)) {
      LOG.info("[appendFiles] borrando fichero TST: {}...", fileOutputTst.toString());
      Files.delete(fileOutputTst);
    }
    boolean existsOutput = Files.exists(fileOutput);

    if (existsOutput) {
      LOG.info("[appendFiles] el fichero output ya existe, lo renombramos de {} a {}", fileOutput.toString(),
          fileOutputTmp.toString());
      Files.move(fileOutput, fileOutputTmp, StandardCopyOption.ATOMIC_MOVE);
    }
    LOG.info("[appendFiles] buscando ficheros {} en {}...", inputPatter, rootPath.toString());
    try (ZipOutputStream zos = new ZipOutputStream(Files.newOutputStream(zipOutTratados, StandardOpenOption.CREATE_NEW,
        StandardOpenOption.WRITE))) {

      try (DirectoryStream<Path> ficherosInput = Files.newDirectoryStream(rootPath, inputPatter);) {
        LOG.info("[appendFiles] Creando/Append fichero: {}...", fileOutputTmp.toString());
        try (OutputStream os = Files.newOutputStream(fileOutputTmp, (existsOutput ? StandardOpenOption.APPEND
            : StandardOpenOption.CREATE), StandardOpenOption.WRITE);) {
          for (Path fichero1015 : ficherosInput) {
            LOG.info("[appendFiles] Copiando fichero: {} ...", fichero1015.toString());
            try (InputStream is = Files.newInputStream(fichero1015, StandardOpenOption.READ);) {
              IOUtils.copyLarge(is, os);
            }
            os.flush();
            LOG.info("[appendFiles] Borrando fichero: {} ...", fichero1015.toString());
            zipEntry = new ZipEntry(fichero1015.getFileName().toString());
            zipEntry.setTime(Files.getLastModifiedTime(fichero1015).toMillis());
            zos.putNextEntry(zipEntry);
            try (InputStream is = Files.newInputStream(fichero1015, StandardOpenOption.READ);) {
              IOUtils.copyLarge(is, zos);
            }
            zos.flush();
            zos.closeEntry();
            Files.delete(fichero1015);
          }
        }
      }
    }
    LOG.info("[appendFiles] Moviendo el fichero: {} a {}...", fileOutputTmp.toString(), fileOutput.toString());
    Files.move(fileOutputTmp, fileOutput, StandardCopyOption.ATOMIC_MOVE);

    if (putTst && tstOutputFileName != null && !tstOutputFileName.trim().isEmpty()) {
      LOG.info("[appendFiles] Creando fichero TST {} ...", fileOutputTst.toString());
      fileOutputTst = Files.createFile(fileOutputTst);
    }
    LOG.trace("[appendFiles] fin");
  }
} // EnvioFichero1015
