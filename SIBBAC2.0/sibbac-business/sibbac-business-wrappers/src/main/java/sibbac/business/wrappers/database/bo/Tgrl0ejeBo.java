package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.wrappers.database.dao.Tgrl0ejeDao;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0eje;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ejeId;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ord;

@Service
public class Tgrl0ejeBo extends AbstractBo<Tgrl0eje, Tgrl0ejeId, Tgrl0ejeDao> {

  @Autowired
  private Tmct0brkBo brkBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0dbsBo dbsBo;

  public Tgrl0ejeBo() {
  }

  /**
   * GRABAR TGRL0EJE
   * 
   * @param tmct0alo datos allocation cliente
   * @throws Exception
   * @generated
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tgrl0eje loadTgrl0eje(Tgrl0ord tgrl0ord, Tmct0alo tmct0alo) throws Exception {
    boolean isNew = false;
    try {
      Tgrl0ejeId id = new Tgrl0ejeId(tmct0alo.getNuoprout(), tmct0alo.getNupareje());

      Tgrl0eje tgrl0eje = dao.findOne(id);

      if (tgrl0eje == null) {
        tgrl0eje = new Tgrl0eje();
        tgrl0eje.setId(id);
        tgrl0eje.setTgrl0ord(tgrl0ord);
        mapping(tgrl0eje, tmct0alo);
        isNew = true;
        // rTgrl0eje.save(false);
      }
      tgrl0eje.setCdestado('A');

      tgrl0eje = dao.save(tgrl0eje);
      dbsBo.insertRegister(tgrl0eje, isNew);
      return tgrl0eje;
    }
    catch (Exception e) {
      throw e;
    }
  }

  @Transactional
  public void mapping(Tgrl0eje tgrl0eje, Tmct0alo tmct0alo) throws Exception {
    Tgrl0ord tgrl0ord = tgrl0eje.getTgrl0ord();
    /*
     * Recuperamos tmct0ord
     */
    Tmct0ord tmct0ord = tmct0alo.getTmct0bok().getTmct0ord();

    boolean foreignMarket = 'E' == tmct0ord.getCdclsmdo();

    /*
     * ***************** mapeo objetos *****************
     */

    if (foreignMarket) {
      tgrl0eje.setNuopbols("000000000000000");
    }
    else {
      tgrl0eje.setNuopbols("");
    }

    /*
     * CDCLIENT C�digo cliente TGRL0ORD.CDCLIENT
     */
    tgrl0eje.setCdclient(tgrl0ord.getCdclient());

    /*
     * CDENTLIQ Entidad liquidadora Mercado Extranjero a "0000" Mercado Nacional
     * se obtiene de TCMT0CTD.CDENTLIQ
     */
    String cdentliq = "";

    if (cdentliq.equals("")) {
      tgrl0eje.setCdentliq("0000");
    }
    else {
      tgrl0eje.setCdentliq(cdentliq);
    }

    /*
     * CDISINVV Valor ISIN TGRL0ORD.CDISINVV
     */
    tgrl0eje.setCdisinvv(tgrl0ord.getCdisinvv());

    /*
     * NUSERIEV Serie TGRL0ORD.NUSERIEV
     */
    tgrl0eje.setNuseriev(tgrl0ord.getNuseriev());

    /*
     * CDBOLSAS Bolsa Mercado Extranjero: D Mercado Nacional: TBAGRCNF.CDBOLSA
     */
    if (foreignMarket) {
      tgrl0eje.setCdbolsas("D");
      if (brkBo.isCdbrokerMTF(tmct0ord.getCdbroker())) {
        tgrl0eje.setCdbolsas("ME");
      }
    }
    else {
      tgrl0eje.setCdbolsas("");
    }

    /*
     * TPOPERAC Tipo operaci�n TGRL0ORD.TPOPERAC
     */
    tgrl0eje.setTpoperac(tgrl0ord.getTpoperac());

    /*
     * TPMERCAD Tipo mercado TGRL0ORD.TPMERCAD
     */
    tgrl0eje.setTpmercad(tgrl0ord.getTpmercad());

    /*
     * FHEJECUC Fecha ejecuci�n Mercado Extranjero: TMCT0ALO.FEOPERAC (aaaammdd)
     * Mercado Nacional: TBAGRCNF.FEEJELIQ (aaaammdd)
     */
    if (foreignMarket) {
      tgrl0eje.setFhejecuc(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(tmct0alo
          .getFeoperac())));
    }
    else {
      tgrl0eje.setFhejecuc(0);
    }

    /*
     * HOEJECUC Hora ejecuci�n Ceros
     */
    tgrl0eje.setHoejecuc(0);

    /*
     * TPCONTRA Tipo contrataci�n TGRL0ORD.TPCONTRA
     */
    tgrl0eje.setTpcontra(tgrl0ord.getTpcontra());

    /*
     * NUCANEJE Cantidad ejecutada Mercado Extranjero: TMCT0ALO.NUTITCLI Mercado
     * Nacional: TBAGRCNF.NUTITDES
     */
    if (foreignMarket) {

      if ('T' == tmct0ord.getCdtpcamb()) {
        // NUCANEJE
        tgrl0eje.setNucaneje(tmct0alo.getNutitcli().multiply(tmct0ord.getImnomval()).setScale(2, RoundingMode.HALF_UP));
      }
      else {
        // NUCANEJE
        tgrl0eje.setNucaneje(tmct0alo.getNutitcli().setScale(2, RoundingMode.HALF_UP));
      }
    }
    else {
      tgrl0eje.setNucaneje(new BigDecimal(0));
    }

    /*
     * TPCAMBIO Tipo cambio TGRL0ORD.TPCAMBIO
     */
    tgrl0eje.setTpcambio(tgrl0ord.getTpcambio());

    /*
     * IMCAMBIO Importe cambio Mercado Extranjero: TMCT0ALO.IMCAMMED Mercado
     * Nacional: TBAGRCNF.IMCAMBIO
     */
    if (foreignMarket) {
      tgrl0eje.setImcambio(tmct0alo.getImcammed().setScale(4, RoundingMode.HALF_UP));
    }
    else {
      tgrl0eje.setImcambio(new BigDecimal(0));
    }

    /*
     * IMEFECUP Importe efectivo cup�n TBAGRCNF.IMEFECUP
     */
    tgrl0eje.setImefecup(new BigDecimal(0));

    /*
     * IMEFECTI Importe efectivo Mercado Extranjero: TMCT0ALO.EUTOTBRU Mercado
     * Nacional: TBAGRCNF.IMEFEAGR
     */
    if (foreignMarket) {
      tgrl0eje.setImefecti(tmct0alo.getEutotbru().setScale(2, RoundingMode.HALF_UP));
    }
    else {
      tgrl0eje.setImefecti(new BigDecimal(0));
    }

    /*
     * PCCOMISN % Comisi�n TMCT0ALO.PCCOMISN
     */

    // IMPORTANTE, si el porcentaje que aparece en la tmct0ald supera el 999% se
    // escribe 0
    BigDecimal pccomisn = tmct0alo.getPccomisn();

    if (pccomisn.compareTo(new BigDecimal(999.999)) >= 0)
      pccomisn = new BigDecimal(0.0);

    tgrl0eje.setPccomisn(pccomisn.setScale(3, RoundingMode.HALF_UP));

    /*
     * IMBONIFI Bonificaci�n Cero
     */
    tgrl0eje.setImbonifi(new BigDecimal(0));

    /*
     * IMDERLIQ Derechos liquidaci�n Mercado Extranjero: Cero Mercado Nacional:
     * TBAGRCNF.IMLIQAUD
     */
    if (foreignMarket) {
      tgrl0eje.setImderliq(new BigDecimal(0));
    }
    else {
      tgrl0eje.setImderliq(new BigDecimal(0));
    }

    /*
     * IMDERCON Derechos contrataci�n Cero
     */
    tgrl0eje.setImdercon(new BigDecimal(0));

    /*
     * IMCANGES Derechos gesti�n Mercado Extranjero: Cero Mercado Nacional:
     * TBAGRCNF.IMGESAUD
     */
    if (foreignMarket) {
      tgrl0eje.setImcanges(new BigDecimal(0));
    }
    else {
      tgrl0eje.setImcanges(new BigDecimal(0));
    }

    /*
     * IMTIMBRE Timbre Cero
     */
    tgrl0eje.setImtimbre(new BigDecimal(0));

    /*
     * HOCONFIR Hora confirmaci�n. Cero
     */
    tgrl0eje.setHoconfir(0);

    /*
     * CDESTADO Cod. Estado "A" -> Alta "B" -> Baja
     */
    tgrl0eje.setCdestado('A');

    /*
     * CDSISLIV Sistema liquidaci�n TGRL0ORD.CDSISLIV
     */
    tgrl0eje.setCdsisliv(tgrl0ord.getCdsisliv());

    /*
     * CDCLASEV Clase valor TGRL0ORD.CDCLASEV
     */
    tgrl0eje.setCdclasev(tgrl0ord.getCdclasev());

    /*
     * CDGRCOMV Grupo comisi�n Blanco
     */
    tgrl0eje.setCdgrcomv(' ');
    /*
     * NUCANTLI Cantidad liquidada Cero
     */
    tgrl0eje.setNucantli(new BigDecimal(0));

    /*
     * IMEFECLI Efectivo liquidado Cero
     */
    tgrl0eje.setImefecli(new BigDecimal(0));

    /*
     * IMBONILI Bonificaci�n liquidada Cero
     */
    tgrl0eje.setImbonili(new BigDecimal(0));

    /*
     * IMDLIQLI Dere. Liquidaci�n liquidados Cero
     */
    tgrl0eje.setImdliqli(new BigDecimal(0));

    /*
     * IMDCONLI contrataci�n Cero
     */
    tgrl0eje.setImdconli(new BigDecimal(0));

    /*
     * IMDGEBLI gesti�n Cero
     */
    tgrl0eje.setImdgebli(new BigDecimal(0));

    /*
     * IMTIMBLI timbre Cero
     */
    tgrl0eje.setImtimbli(new BigDecimal(0));

    /*
     * FHRECTIF F. movimiento Mercado Extranjero: TMCT0ALO.FHAUDIT (aaaammmdd)
     * Mercado Nacional: TBAGRCNF.FHAUDIT (aaaammdd)
     */
    if (foreignMarket) {
      tgrl0eje.setFhrectif(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(tmct0alo
          .getFhaudit())));
    }
    else {
      tgrl0eje.setFhrectif(0);
    }

    /*
     * FHCONCON F. Entrada contrataci�n en Base de Datos. Fecha sistema
     * (aaaammdd)
     */
    tgrl0eje.setFhconcon(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(new Date())));

    /*
     * FHLIQBOL Mercado Extranjero: TMCT0ALO.FEVALOR (aaaammmdd) Mercado
     * Nacional: TBAGRCNF.FEOPELIQ (aaaammdd)
     */
    if (foreignMarket) {
      tgrl0eje.setFhliqbol(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(tmct0alo
          .getFevalor())));
    }
    else {
      tgrl0eje.setFhliqbol(0);
    }

    /*
     * NUULLIPA N� �ltima liquidaci�n. Cero
     */
    tgrl0eje.setNuullipa((short) 0);

    /*
     * FHCOCLPA F. confirmaci�n cliente papel Cero
     */
    tgrl0eje.setFhcoclpa(0);

    /*
     * CDLIQBOL Cod. Liquidaci�n bolsa Cero
     */
    tgrl0eje.setFhcolibo(0);

    /*
     * CDLIQBOL Cod. Liquidaci�n bolsa "S"
     */
    tgrl0eje.setCdliqbol('S');

    /*
     * CDCOBEFE Cod. Cobro efectivo. Blanco
     */
    tgrl0eje.setCdcobefe("");

    /*
     * CDTITULA Estado titular "S" -> Sin titular asignado "P" -> Titular
     * asignado autom�ticamente pdte env�o AUD "Q" -> Titular asignado
     * manualmente pdte env�o AUD "R" -> Titular asignado manualmente pdte env�o
     * AUD y procedente de una agrupaci�n "E" -> Titular asignado
     * autom�ticamente enviado AUD "F" -> Titular asignado manualmente enviado
     * AUD "G" -> Titular asignado autom�ticamente enviado AUD y procedente de
     * una agrupaci�n "D" -> Titular asignado para Mercado Extranjero "I" ->
     * Titular asignado pero con error datos fiscales
     */
    // tgrl0eje.setCdtitula('S');
    tgrl0eje.setCdtitula('P');
    // TODO VER COMO INSERTA INTERNACIONAL LA TGRL0DIR
    // Collection<Tgrl0dir> tgrl0dirList =
    // JdoGetRelation.getListTgrl0dir(tmct0alo, pm);
    // try {
    // if (!tgrl0dirList.isEmpty()) {
    // tgrl0eje.setCdtitula("P");
    // }
    // }
    // catch (Exception e) {
    // }

    /*
     * CDSITDES Situaci�n desglose Mercado Extranjero:"D" Mercado Nacional:
     * TBAGRCNF.CDSITDES
     */
    if (foreignMarket) {
      tgrl0eje.setCdsitdes('D');
    }
    else {
      tgrl0eje.setCdsitdes(' ');
    }

    /*
     * CDINTERN C�digo interno bolsa Cero
     */
    tgrl0eje.setCdintern("0");

    /*
     * NUPRCLTE N� parcial ejecuci�n cliente. Cero
     */
    tgrl0eje.setNuprclte("000");

    /*
     * NUOPCLTE N� Operaci�n cliente TMCT0ORD.NUREFORD
     */
    tgrl0eje.setNuopclte(tmct0ord.getNureford().substring(0, 16).trim());

    /*
     * NUOPCATS N� operaci�n CATS Cero
     */
    tgrl0eje.setNuopcats(0);

    /*
     * TPEJERCI Tipo ejercicio. TMCT0EJE.TPOPEBOL
     */
    tgrl0eje.setTpejerci("");

    if (foreignMarket) {
      tgrl0eje.setCdproced('D');
    }
    else {
      tgrl0eje.setCdproced(' ');
    }

    /*
     * CDREFBAN Referencia bancaria Mercado Extranjero: Blancos Mercado
     * Nacional: TBCNFLIQ.CDREFBAN
     */
    if (foreignMarket) {
      tgrl0eje.setCdrefban("");
    }
    else {
      tgrl0eje.setCdrefban("");
    }

    /*
     * CDPROTER P=Propios/T=Terceros Mercado Extanjero: T Mercado Nacional:
     * TBCNFLIQ.TPDSALDO
     */
    if (foreignMarket) {
      tgrl0eje.setCdproter('T');
    }
    else {
      tgrl0eje.setCdproter(' ');
    }

    /*
     * CDSITSCL Situaci�n SCL Mercado Extranjero: Blancos Mercado Nacional:
     * TBAGRCNF.CDSITSCL
     */
    if (foreignMarket) {
      tgrl0eje.setCdsitscl(' ');
    }
    else {
      tgrl0eje.setCdsitscl(' ');
    }

    /*
     * TPOPESTD Tipo operaci�n estad�stico Mercado Extranjero una "N" Mercado
     * Nacional depende campo Tipo ejercicio (fila 57). "N" -> cuando Tipo
     * ejercicio sea "CV" o "EO" "E" -> cuando Tipo ejercicio NO sea "CV" o "EO"
     */
    if (foreignMarket) {
      tgrl0eje.setTpopestd("N");
    }
    else {
      if (tgrl0eje.getTpejerci().equals("CV") || tgrl0eje.getTpejerci().equals("EO")) {
        tgrl0eje.setTpopestd("N");
      }
      else {
        tgrl0eje.setTpopestd("E");
      }
    }

    /*
     * CDCOMISN Quien gestiona la comisi�n TBCNFLIQ.CDCOMISN
     */
    tgrl0eje.setCdcomisn(' ');

    /*
     * PCLIMCAM Porcentaje L�mite Cambio TMCT0EJE.PCCAMBIO
     */
    tgrl0eje.setPclimcam(new BigDecimal(0));

    /*
     * PCEFECUP Porcentaje Efecto Cup�n Mercado Extranjero: Ceros Mercado
     * Nacional: TBAGRCNF.PCEFECUP
     */
    if (foreignMarket) {
      tgrl0eje.setPcefecup(new BigDecimal(0));
    }
    else {
      tgrl0eje.setPcefecup(new BigDecimal(0));
    }

    /*
     * CDMODORD C�digo Modo Orden Mercado Extranjero: N Mercado Nacional: "A" ->
     * Aceptada. Cuando es Modo Orden y se ha enviado al AUD y AUD lo ha
     * aceptado "N" -> NO Modo Orden.
     */
    if (foreignMarket) {
      tgrl0eje.setCdmodord('N');
    }
    else {
      tgrl0eje.setCdmodord(' ');
    }

    /*
     * CDGESSCL C�digo Gesti�n SCL Mercado Extranjero: N Mercado Nacional:
     * TBAGRCNF.CDMODORD
     */
    if (foreignMarket) {
      tgrl0eje.setCdgesscl('N');
    }
    else {
      tgrl0eje.setCdgesscl(' ');
    }

    /*
     * CDCLEARE Clearer Mercado Extranjero: TMCT0ALO.CDCLEARE Mercado Nacional:
     * Blancos
     */
    if (foreignMarket) {
      tgrl0eje.setCdcleare(tmct0alo.getCdcleare().trim());
    }
    else {
      tgrl0eje.setCdcleare("");
    }

    /*
     * CDMERCAD Exchange (M.Extranjero). Segment (M.Nacional) TMCT0ORD.CDMERCAD
     */
    tgrl0eje.setCdmercad(tmct0ord.getCdmercad());

    /*
     * CDORDENA Cod. Ordenante Mercado Extranjero a Ceros Mercado Nacional se
     * obtiene de tabla TVAL0ELI. A esta tabla se accede por TGRL0EJE.CDENTLIQ y
     * TGRL0EJE.CDBOLSAS. En caso de no existir se deja a "6666"
     */
    if (foreignMarket) {
      tgrl0eje.setCdordena("0000");
    }
    else {
      // recuperamos objeto tval0eli
      // List<Tval0eli> tval0elilst = JdoGetRelation.getListTval0eli(cdentliq,
      // cdentliq, pm);
      // try {
      // tgrl0eje.setCdordena(tval0elilst.get(0).getCdordena());
      // }
      // catch (Exception e) {
      // tgrl0eje.setCdordena("6666");
      // }
      tgrl0eje.setCdordena("0000");
    }

    /*
     * CDSUBORD Cod. Subordenante Mercado Extranjero a Ceros Mercado Nacional se
     * obtiene de tabla TCLI0CLV. A esta tabla se accede por TGRL0EJE.CDCLIENT y
     * TGRL0EJE.CDBOLSAS. En caso de no existir se deja a "6666"
     */
    if (foreignMarket) {
      tgrl0eje.setCdsubord("0000");
    }
    else {
      // recuperamos objeto tcli0clv
      // Tcli0clvSdo tcli0clv = CorbtsqlFactory.eINSTANCE.createTcli0clv();
      // ((HaleeEDataObject) tcli0clv).setPersistenceManager(pm);
      // tcli0clv.setCdbolsas(getCdbolsas());
      // tcli0clv.setCdproduc(getCdclient().substring(0, 3));
      // tcli0clv.setNuctapro(new BigDecimal(getCdclient().substring(3, 8)));
      // try {
      // ((HaleeEDataObject) tcli0clv).loadFromPKFields();
      // setCdsubord(tcli0clv.getClsubord());
      // } catch (Exception e) {
      tgrl0eje.setCdsubord("6666");
      // }

    }

  }
}
