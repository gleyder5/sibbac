package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0anotacioneccDao;
import sibbac.business.wrappers.database.dao.Tmct0anotacioneccDaoImpl;
import sibbac.business.wrappers.database.dao.specifications.AnotacionEccSpecificacions;
import sibbac.business.wrappers.database.data.AnotacioneccData;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0anotacioneccBo extends AbstractBo<Tmct0anotacionecc, Long, Tmct0anotacioneccDao> {

  @Autowired
  private Tmct0anotacioneccDaoImpl daoImpl;

  public Tmct0anotacionecc findOperacionTitulosDisponiblesGte(String cdoperacionecc, BigDecimal imtitulos) {

    Tmct0anotacionecc an = dao.findByCdoperacionecc(cdoperacionecc);
    if (an != null) {
      if (an.getImtitulosdisponibles().compareTo(imtitulos) >= 0) {
        return an;
      }
      else {
        List<Tmct0anotacionecc> ans = dao.findByNuoperacionprev(cdoperacionecc);
        for (Tmct0anotacionecc tmct0anotacionecc : ans) {
          if (tmct0anotacionecc.getImtitulosdisponibles().compareTo(imtitulos) >= 0) {
            return tmct0anotacionecc;
          }
          else {
            return findOperacionTitulosDisponiblesGte(tmct0anotacionecc.getCdoperacionecc(), imtitulos);
          }
        }
      }
    }
    return null;
  }

  public List<Tmct0anotacionecc> findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(String nuoperacioninic,
      char sentido) {
    return dao.findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(nuoperacioninic, sentido);
  }

  public List<Tmct0anotacionecc> findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(String nuoperacioninic,
      char sentido, BigDecimal nutitulos) {
    return dao.findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(nuoperacioninic, sentido, nutitulos);
  }

  public List<Tmct0anotacionecc> findAllByNuoperacioninicOrderDescendente(String nuoperacioninic, char sentido,
      BigDecimal nutitulos) {
    return dao.findAllByNuoperacioninicOrderDescendente(nuoperacioninic, sentido, nutitulos);
  }

  public Tmct0anotacionecc findByCdoperacionecc(String cdoperacionecc) {
    return dao.findByCdoperacionecc(cdoperacionecc);
  }

  public List<Tmct0anotacionecc> findByNuoperacionprev(String nuoperacionprev) {
    return dao.findByNuoperacionprev(nuoperacionprev);
  }

  public List<Tmct0anotacionecc> findByNuoperacioninicAndSentidoAndNotNovacion(String nuoperacionprev, char sentido) {
    return dao.findByNuoperacioninicAndSentidoAndNotNovacion(nuoperacionprev, sentido);
  }

  public List<Tmct0anotacionecc> findByNuoperacioninic(String nuoperacioninic) {
    return dao.findByNuoperacioninic(nuoperacioninic);
  }

  /**
   * 
   * @param fcontratacion
   * @param isin
   * @param tmct0CuentasDeCompensacion
   * @return
   */
  public List<Tmct0anotacionecc> findByFcontratacionAndCdisinAndTmct0CuentasDeCompensacion(Date fcontratacion,
      String isin, Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion) {
    if (fcontratacion != null && isin != null && !"".equals(isin) && tmct0CuentasDeCompensacion != null) {
      return dao.findByFcontratacionAndCdisinAndTmct0CuentasDeCompensacion(fcontratacion, isin,
          tmct0CuentasDeCompensacion);
    }
    return null;
  }

  /**
   * 
   * @param fcontratacion
   * @param isin
   * @param tmct0CuentasDeCompensacion
   * @param movimientoecc
   * @return
   */
  public Tmct0anotacionecc findByFcontratacionAndCdisinAndTmct0CuentasDeCompensacionAndTmct0movimientoecc(
      Date fcontratacion, String isin, Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion,
      Tmct0movimientoecc movimientoecc) {
    if (fcontratacion != null && isin != null && !"".equals(isin) && tmct0CuentasDeCompensacion != null
        && movimientoecc != null) {
      return dao.findByFcontratacionAndCdisinAndTmct0CuentasDeCompensacionAndTmct0movimientoecc(fcontratacion, isin,
          tmct0CuentasDeCompensacion, movimientoecc);
    }
    return null;
  }

  /**
   * 
   * @param tmct0movimientoecc
   * @return
   */
  public List<Tmct0anotacionecc> findByTmct0movimientoecc(Tmct0movimientoecc tmct0movimientoecc) {
    if (tmct0movimientoecc != null) {
      dao.findByTmct0movimientoecc(tmct0movimientoecc);
    }
    return null;
  }

  public List<Tmct0anotacionecc> findByTipoCuenta(long tipoCuenta) {
    if (tipoCuenta != 0) {
      return dao.findByTipoCuenta(tipoCuenta);
    }
    return null;
  }

  @Transactional
  public List<Object[]> findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentido(long tipoCuenta) {
    List<Object[]> result = null;
    if (tipoCuenta != 0 && !"".equals(tipoCuenta)) {
      result = dao.findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentido(tipoCuenta);
    }
    return result;
  }

  /**
   * 
   * @param tipoCuenta
   * @param filtros
   * @return List<Tmct0anotacionecc>
   */
  @Transactional
  public List<AnotacioneccData> findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentido(long tipoCuentaPropia,
      long tipoCuentaIndividual, Map<String, Serializable> filtros) {
    List<AnotacioneccData> resultList = daoImpl.findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentido(
        tipoCuentaPropia, tipoCuentaIndividual, filtros);
    return resultList;
  }

  @Transactional
  public List<AnotacioneccData> findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentidoWithPagination(
      long tipoCuentaPropia, long idTipoCuentaIndividual, Map<String, Serializable> filtros, int startPosition,
      int maxResult) {
    List<AnotacioneccData> resultList = daoImpl
        .findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentidoWithPagination(tipoCuentaPropia,
            idTipoCuentaIndividual, filtros, startPosition, maxResult);
    return resultList;
  }

  @Transactional
  public List<Object[]> findAnotacionesCuenta00DForDiaria() {
    List<Object[]> resultList = dao.findAnotacionesCuenta00DForDiaria(new Date());
    return resultList;
  }

  /**
   * @return
   */
  @Transactional
  public List<Tmct0anotacionecc> findAnotacionesCuenta00DForDiaria(final Date fechaContratacionDesde,
      final Date fechaContratacionHasta, final Date fechaLiquidacionDesde, final Date fechaLiquidacionHasta,
      final String camara, final String isin) {
    final List<Tmct0anotacionecc> anotaciones = dao.findAll(AnotacionEccSpecificacions.listadoFriltrado(
        fechaContratacionDesde, fechaContratacionHasta, fechaLiquidacionDesde, fechaLiquidacionHasta, camara, isin,
        "00D", true));
    return anotaciones;
  }

  /**
   * @return
   */
  @Transactional
  public List<Tmct0anotacionecc> findAnotacionesCuentaNot00DForDiaria(final Date fechaContratacionDesde,
      final Date fechaContratacionHasta, final Date fechaLiquidacionDesde, final Date fechaLiquidacionHasta,
      final String camara, final String isin) {
    final List<Tmct0anotacionecc> anotaciones = dao.findAll(AnotacionEccSpecificacions.listadoFriltrado(
        fechaContratacionDesde, fechaContratacionHasta, fechaLiquidacionDesde, fechaLiquidacionHasta, camara, isin,
        "00D", false));
    return anotaciones;
  }

  public List<String> findAllCdoperacioneccTitulosDisponiblesForEnvioS3AndCamara(Date date, List<String> ctas,
      String camara) {
    List<String> l1 = dao.findAllCdoperacioneccEnvioS3CuentaDiariaAndCamara(date, camara);
    Collection<String> lSinDuplicados = new HashSet<String>();
    lSinDuplicados.addAll(l1);
    l1.clear();

    if (CollectionUtils.isNotEmpty(ctas)) {
      List<String> l2 = dao.findAllCdoperacioneccEnvioS3ByListCuentasAndCamara(date, ctas, camara);
      lSinDuplicados.addAll(l2);
      l2.clear();
    }

    List<String> res = new ArrayList<String>();
    res.addAll(lSinDuplicados);

    lSinDuplicados.clear();
    return res;
  }

  public List<String> findAllCdoperacioneccTitulosDisponiblesForEnvioS3AndInSegmentosAndCamara(Date date,
      List<String> ctas, List<String> segmentosIn, String camara) {
    List<String> l1 = dao.findAllCdoperacioneccEnvioS3CuentaDiariaAndInSegmentosAndCamara(date, segmentosIn, camara);
    Collection<String> lSinDuplicados = new HashSet<String>(l1);
    l1.clear();

    if (CollectionUtils.isNotEmpty(ctas)) {
      List<String> l2 = dao.findAllCdoperacioneccEnvioS3ByListCuentasAndInSegmentosAndCamara(date, ctas, segmentosIn,
          camara);
      lSinDuplicados.addAll(l2);
      l2.clear();
    }

    List<String> res = new ArrayList<String>(lSinDuplicados);

    lSinDuplicados.clear();
    return res;
  }

  public List<String> findAllCdoperacioneccTitulosDisponiblesForEnvioS3AndNotInSegmentosAndCamara(Date date,
      List<String> ctas, List<String> segmentosNotIn, String camara) {
    List<String> l1 = dao.findAllCdoperacioneccEnvioS3CuentaDiariaAndNotInSegmentosAndCamara(date, segmentosNotIn,
        camara);
    Collection<String> lSinDuplicados = new HashSet<String>(l1);
    l1.clear();

    if (CollectionUtils.isNotEmpty(ctas)) {
      List<String> l2 = dao.findAllCdoperacioneccEnvioS3ByListCuentasAndNotInSegmentosAndCamara(date, ctas,
          segmentosNotIn, camara);
      lSinDuplicados.addAll(l2);
      l2.clear();
    }

    List<String> res = new ArrayList<String>(lSinDuplicados);

    lSinDuplicados.clear();
    return res;
  }
}
