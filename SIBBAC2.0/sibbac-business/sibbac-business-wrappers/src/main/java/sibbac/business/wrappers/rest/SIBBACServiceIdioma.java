package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.IdiomaBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceIdioma implements SIBBACServiceBean {

	private static final Logger	LOG					= LoggerFactory.getLogger( SIBBACServiceIdioma.class );

	@Autowired
	private IdiomaBo			idiomaBo;

	public static final String	FILTER_CODIGO		= "codigo";

	public static final String	FILTER_DESCRIPCION	= "descripcion";

	public static final String	CMD_GET_IDIOMAS		= "getListaIdiomas";

	public SIBBACServiceIdioma() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		LOG.debug( "[SIBBACServiceIdioma::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		final String action = webRequest.getAction();
		LOG.debug( "[SIBBACServiceIdioma::process] " + "+ Command.: [{}]", action );

		switch ( action ) {
			case SIBBACServiceIdioma.CMD_GET_IDIOMAS:
				webResponse.setResultados( idiomaBo.getListaIdiomas() );
				break;
			default:
				webResponse.setError( "No recibe ningun comando" );
				LOG.error( "No le llega ningun comando" );
				break;
		}

		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( SIBBACServiceIdioma.CMD_GET_IDIOMAS );
		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}

}
