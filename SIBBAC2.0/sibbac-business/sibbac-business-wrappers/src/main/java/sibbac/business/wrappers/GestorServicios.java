package sibbac.business.wrappers;


import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.database.DBConstants;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.database.JobPK;


/**
 * Interfaz para comunicar con el Gestor de Servicios.
 * <p/>
 * Para que un "job" se pueda identificar frente al Gestor de Servicios se utiliza la propiedad: "jobPk" de la clase: {@link SIBBACTask}.
 * Esta propiedad es el identificador unico del proceso que permite que el Gestor lo pueda identificar.
 * <p/>
 * Este dato permite acceder a la tabla: {@link DBConstants.TASKS#JOB_INFO} para localizar e identificar el proceso.
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public interface GestorServicios {

	/**
	 * Permite conocer si un servicio dado, a partir del "idServicio", esta activo en este momento.
	 * 
	 * @param jobPk El identificador del proceso.
	 * @return TRUE o FALSE.
	 */
	public List< Alias > isActive( final JobPK jobPk );

	/**
	 * Permite conocer si un servicio dado, a partir del "idServicio", esta activo en un momento dado.
	 * 
	 * @param jobPk El identificador del proceso.
	 * @param time El momento para el cual se desea conocer el estado.
	 * @return TRUE o FALSE.
	 */
	public List< Alias > isActive( final JobPK jobPk, final Date time );

	/**
	 * Permite acceder a los {@link Contacto contactos} de un {@link Alias} para un {@link JobPK servicio} dado en este momento.
	 * 
	 * @param alias El {@link Alias}
	 * @param jobPk El {@link JobPk identificador} del proceso.
	 * @return Una {@link List lista} de {@link Contacto contactos} de destino o NULL si no hay contactos.
	 */
	public List< Contacto > getContactos( final Alias alias, final JobPK jobPk );
	
	/**
	 * Permite acceder a los {@link Contacto contactos} de un {@link Alias} para un {@link JobPK servicio} dado en un momento determinado.
	 * 
	 * @param alias El {@link Alias}
	 * @param jobPk El {@link JobPk identificador} del proceso.
	 * @param time El momento para el cual se desea disponer de la lista de {@link Contacto contactos}.
	 * @return Una {@link List lista} de {@link Contacto contactos} de destino o NULL si no hay contactos.
	 */
	public List< Contacto > getContactos( final Alias alias, final JobPK jobPk, final Date time );

}
