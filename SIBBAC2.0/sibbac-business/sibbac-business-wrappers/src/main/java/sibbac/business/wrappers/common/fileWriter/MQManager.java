package sibbac.business.wrappers.common.fileWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.data.MQConnectionData;
import sibbac.business.wrappers.util.FicherosUtils;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;

@Component
public class MQManager {

  // MFG Parametros envio mail
  @Value("${MQ.partenon.desgloses.in.body}")
  private String mqPartenonBody;

  @Value("${MQ.partenon.desgloses.in.subject}")
  private String mqPartenonSubject;

  @Value("${MQ.partenon.desgloses.in.emails}")
  private String mqPartenonEmails;

  @Value("${sibbac.wrappers.mqmanager.read.timeout:10000}")
  private int readTimeout;

  @Autowired
  private SendMail sendMail;

  protected static final Logger LOG = LoggerFactory.getLogger(MQManager.class);

  private boolean isForServer = true;
  private static final String FILE_TMP = "recibiendo.tmp";
  private static final String FILE_CHARSET = "ISO-8859-1";

  private Context getContext() throws NamingException {
    String initialContext = "java:comp/env";

    Context ctx = null;
    if (isForServer) {
      LOG.trace("[MQManager :: getContext] Mirando el contexto inicial sin usar: " + initialContext);
      ctx = (Context) new InitialContext();
    } else {
      LOG.trace("[MQManager :: getContext] Mirando el contexto inicial usando: " + initialContext);
      ctx = (Context) new InitialContext().lookup(initialContext);
    }
    return ctx;
  }

  private QueueConnection createConnection(String queueConnectionFactoryName, Context ctx) throws NamingException,
                                                                                          JMSException {
    QueueConnectionFactory queueConnectionFactory = null;
    QueueConnection queueConnection = null;
    LOG.trace("[MQManager :: createConnection] Obteniendo recurso JNDI QueueConnectionFactory="
              + queueConnectionFactoryName);
    queueConnectionFactory = (QueueConnectionFactory) ctx.lookup(queueConnectionFactoryName);
    LOG.trace("[MQManager :: createConnection] Se va a crear la conexión");
    queueConnection = (QueueConnection) queueConnectionFactory.createQueueConnection();
    return queueConnection;
  }

  private QueueSession createSession(QueueConnection queueConnection) throws NamingException, JMSException {
    QueueSession queueSession = null;
    LOG.trace("[File2MQWriter :: createSession] Conexión creada, se va a crear la sesión");
    queueSession = (QueueSession) queueConnection.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);
    return queueSession;
  }

  private Queue getQueueFromJNDI(Context ctx, String queueName) throws NamingException {
    LOG.trace("[MQManager :: getQueueFromJNDI] Obteniendo recurso JNDI Queue=" + queueName);
    Queue queue = null;
    queue = (Queue) ctx.lookup(queueName);
    return queue;
  }

  public void writeIntoQueue(String queueConnectionFactoryName, String queueName, String fileName, String filePath) throws SIBBACBusinessException {
    LOG.trace("[MQManager :: writeIntoQueue] Init (QueueConnectionFactory=" + queueConnectionFactoryName
              + ", QueueName=" + queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")");
    File file = null;
    file = getFileForReading(fileName, filePath);

    if (file != null && file.exists() && file.length() > 0) {
      Integer hadError = 0;
      QueueSender queueSender = null;
      MQConnectionData connectionData = null;
      try {
        connectionData = initalizeConnectionData(queueConnectionFactoryName, queueName);
        queueSender = connectionData.getQueueSession().createSender(connectionData.getQueue());
        LOG.trace("[MQManager :: writeIntoQueue] Sender creado, se le da comienzo a la conexión");
        connectionData.getQueueConnection().start();
        LOG.trace("[MQManager :: writeIntoQueue] Conexión comenzada");
        hadError = sendLines(file, connectionData.getQueueSession(), queueSender);
        if (hadError == 0) {
          FicherosUtils.fileToTratados(file, FormatDataUtils.CONCURRENT_FILE_DATETIME_FORMAT);
        } else {
          FicherosUtils.fileToErroneos(file);
        }
      } catch (Exception e) {
        LOG.error("[MQManager :: writeIntoQueue] Error. Líneas mandadas=" + hadError + " - " + e.getMessage());
        throw new SIBBACBusinessException(e);
      } finally {
        closeSender(queueSender);
        LOG.trace("[MQManager :: writeIntoQueue] Productor cerrado, se va a cerrar la sesión");
        closeSession(connectionData.getQueueSession());
        LOG.trace("[MQManager :: writeIntoQueue] Sesión cerrada, se va a cerrar la conexión");
        closeConnection(connectionData.getQueueConnection());
        if (hadError != 0) {
          LOG.trace("[MQManager :: writeIntoQueue] Fin de la conexión - Mandadas " + hadError + " líneas (QueueName="
                    + queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")");
        } else {
          LOG.trace("[MQManager :: writeIntoQueue] Fin de la conexión - Fichero enviado con éxito (QueueName="
                    + queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")");
        }
      }
    }
  }

  private File getFileForReading(String fileName, String filePath) {
    File file = null;
    try {
      file = FicherosUtils.fileToTemp(fileName, filePath, ".mq");
    } catch (IOException e1) {
      LOG.trace("[MQManager :: getFileForReading] No se encuentra el fichero: " + filePath + fileName);
    }
    return file;
  }

  private void closeConnection(QueueConnection queueConnection) {
    try {
      if (queueConnection != null) {
        queueConnection.close();
        LOG.trace("[MQManager :: closeConnection] Conexión cerrada");
      }
    } catch (Exception e) {
      LOG.error("[MQManager :: closeConnection] Error cerrando la conexión: " + e.getMessage());
    }
  }

  private void closeSession(QueueSession queueSession) {
    try {
      if (queueSession != null) {
        queueSession.close();
      }
    } catch (Exception e) {
      LOG.error("[MQManager :: closeSession] Error cerrando la sesión: " + e.getMessage());
    }
  }

  private void closeSender(QueueSender queueSender) {
    try {
      if (queueSender != null) {
        queueSender.close();
      }
    } catch (Exception e) {
      LOG.error("[MQManager :: closeSender] Error cerrando el productor: " + e.getMessage());
    }
  }

  public Integer sendLines(File file, QueueSession queueSession, QueueSender queueSender) throws SIBBACBusinessException {
    Integer sentLines = 0;
    TextMessage textMessage = null;
    Scanner sc = null;
    try {
      sc = new Scanner(file);
    } catch (Exception e) {
      LOG.error("[MQManager :: sendLines] Error abriendo el fichero");
    }
    while (sc.hasNextLine()) {
      String fileLine = sc.nextLine();
      try {
        LOG.trace("[MQManager :: sendLines] Se va a crear el mensaje de texto con: " + fileLine);
        textMessage = (TextMessage) queueSession.createTextMessage();
        textMessage.setText(fileLine);
        queueSender.send(textMessage);
        LOG.trace("[MQManager :: sendLines] Mensaje mandado a cola: " + textMessage.getText());
        queueSession.commit();
      } catch (Exception e) {
        LOG.error("[MQManager :: sendLines] ERROR mandando mensaje de texto. Líneas mandadas hasta ahora=" + sentLines
                  + " - " + e.getMessage());
        sc.close();
        throw new SIBBACBusinessException(e);
      }
      sentLines++;
    }
    sc.close();
    LOG.trace("[MQManager :: sendLines] Fin de la lectura del fichero - Mandadas " + sentLines + " líneas");
    return 0;
  }

  public void writeIntoFile(String queueConnectionFactoryName, String queueName, String fileName, String filePath) throws SIBBACBusinessException {
    LOG.trace("[MQManager :: writeIntoFile] Init (QueueConnectionFactory=" + queueConnectionFactoryName
              + ", QueueName=" + queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")");
    MQConnectionData connectionData = new MQConnectionData();
    QueueReceiver messageReceiver = null;
    try {
      connectionData = initalizeConnectionData(queueConnectionFactoryName, queueName);
      messageReceiver = connectionData.getQueueSession().createReceiver(connectionData.getQueue());
      LOG.trace("[MQManager :: writeIntoFile] Consumidor creado, se le da comienzo a la conexión");
      connectionData.getQueueConnection().start();
      Integer receivedLines = receiveLines(connectionData.getQueueSession(), messageReceiver, fileName, filePath);
      LOG.trace("[MQManager :: writeIntoFile] Recibidas " + receivedLines + " y copiadas al fichero");
    } catch (Exception e) {

      LOG.error("[MQManager :: writeIntoFile] Error escribiendo en fichero " + e.getMessage());
      throw new SIBBACBusinessException(e);
    } finally {
      closeConsummer(messageReceiver);
      LOG.trace("[MQManager :: writeIntoFile] Productor cerrado, se va a cerrar la sesión");
      closeSession(connectionData.getQueueSession());
      LOG.trace("[MQManager :: writeIntoFile] Sesión cerrada, se va a cerrar la conexión");
      closeConnection(connectionData.getQueueConnection());
    }
  }

  private File openFileToBeWritten(String fileName, String filePath) throws MalformedURLException {
    String urlString = filePath + "/" + fileName;
    URL url;
    url = new URL(urlString);
    File file = new File(url.getPath());
    file.getParentFile().mkdirs();
    try {
      if (!file.exists()) {
        file.createNewFile();
      }
    } catch (IOException e1) {
      LOG.error("[MQManager :: openFileToBeWritten] Error abriendo fichero " + filePath + "/" + fileName + ":"
                + e1.getMessage());
    }
    return file;
  }

  private PrintStream initPrintStream(File file) {
    PrintStream printStream = null;
    try {
      printStream = new PrintStream(new FileOutputStream(file, true));
    } catch (IOException e1) {
      LOG.error("[MQManager :: initPrintStream] Error leyendo fichero " + e1.getMessage());
    }
    return printStream;
  }

  private PrintStream initPrintStreamCharset(File file, String sCharSet) {
    PrintStream printStream = null;
    try {
      printStream = new PrintStream(new FileOutputStream(file, true), true, sCharSet);
    } catch (IOException e1) {
      LOG.error("[MQManager :: initPrintStream] Error leyendo fichero " + e1.getMessage());
    }
    return printStream;
  }

  private MQConnectionData initalizeConnectionData(String queueConnectionFactoryName, String queueName) throws NamingException,
                                                                                                       JMSException {
    MQConnectionData connectionData = new MQConnectionData();
    Context ctx = getContext();
    connectionData.setQueueConnection(createConnection(queueConnectionFactoryName, ctx));
    connectionData.setQueueSession(createSession(connectionData.getQueueConnection()));
    connectionData.setQueue(getQueueFromJNDI(ctx, queueName));
    return connectionData;
  }

  private void closeConsummer(QueueReceiver messageConsummer) {
    try {
      if (messageConsummer != null) {
        messageConsummer.close();
      }
    } catch (Exception e) {
      LOG.error("[MQManager :: closeConsummer] Error cerrando el productor: " + e.getMessage());
    }
  }

  private Integer receiveLines(QueueSession queueSession,
                               QueueReceiver messageConsummer,
                               String fileName,
                               String filePath) throws IOException, SIBBACBusinessException {
    Integer receivedLines = 0;
    TextMessage received = null;
    PrintStream printStream = null;
    String text = "";
    File file = null;
    boolean toErroneos = false;

    try {
      while (true) {
        // MFG 24/10/2016 Se reciben con un timeout
        // received = (TextMessage) messageConsummer.receiveNoWait();
        received = (TextMessage) messageConsummer.receive(readTimeout);
        if (received == null) {
          break;
        } else if (printStream == null) {
          file = openFileToBeWritten(FILE_TMP, filePath);
          // MFG 10/10/2016 - Se modifica para que el fichero final, lo genere en ISO
          printStream = initPrintStreamCharset(file, FILE_CHARSET);
        }
        text = received.getText();
        LOG.trace("[MQManager :: writeIntoFile] Se ha recibido: " + text);
        printStream.println(text);
        queueSession.commit();
        receivedLines++;
      }
    } catch (Exception e) {
      // Se envian un correo indicando el error
      try {
        sendMail.sendMail(Arrays.asList(mqPartenonEmails.split(";")), mqPartenonSubject, mqPartenonBody);
      } catch (Exception e2) {
        LOG.error("[MQManager :: writeIntoFile] ERROR enviando el mail de aviso: " + e2.getMessage());
      }

      toErroneos = true;
      LOG.error("[MQManager :: writeIntoFile] ERROR recibiendo alguna línea" + receivedLines + "(" + text + ")" + ": "
                + e.getMessage());
      throw new SIBBACBusinessException(e);
    } finally {
      closePrintStream(printStream);
      // Se mueve a erroneos o se cambia el nombre.
      try {

        if (file != null) {
          if (toErroneos) {
            FicherosUtils.fileToErroneos(file);
          } else {

            // MFG 21/10/2016 Se añade la fecha al nombre.
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            String dateToConcat = dateFormat.format(new Date());

            String sNombreFinal = fileName.replace(".DAT", "_" + dateToConcat + ".DAT");

            FicherosUtils.removeTmpFromName(file, sNombreFinal);
          }
        }

      } catch (Exception e2) {
        // Si se produce un error se envia el mail.
        try {
          sendMail.sendMail(Arrays.asList(mqPartenonEmails.split(";")), mqPartenonSubject, mqPartenonBody);
        } catch (Exception e3) {
          LOG.error("[MQManager :: writeIntoFile] ERROR enviando el mail de aviso: " + e3.getMessage());
        }

      }

    }

    return receivedLines;
  }

  private void closePrintStream(PrintStream printStream) {
    try {
      if (printStream != null) {
        printStream.close();
        LOG.trace("[MQManager :: writeIntoFile] Stream de escritura en el fichero cerrado");
      }
    } catch (Exception e) {
      LOG.error("[MQManager :: writeIntoFile] Error cerrando el Stream de escritura en el fichero: " + e.getMessage());
    }
  }

}
