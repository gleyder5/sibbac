package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Tmct0GruposValores;

@Component
public interface Tmct0GruposValoresDao extends JpaRepository< Tmct0GruposValores, Integer >{
  
  Tmct0GruposValores findByCdCodigo(String cdCodigo);

}
