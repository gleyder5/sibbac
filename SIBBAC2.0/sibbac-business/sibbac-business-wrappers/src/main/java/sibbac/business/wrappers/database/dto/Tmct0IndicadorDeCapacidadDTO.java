package sibbac.business.wrappers.database.dto;

import sibbac.business.wrappers.database.model.Tmct0IndicadorDeCapacidad;

public class Tmct0IndicadorDeCapacidadDTO {
	
	private Long id_indicador_capacidad;
	private Long valor;
	private String cd_codigo;
	private String descripcion;
	
	
	public Tmct0IndicadorDeCapacidadDTO() {}


	public Tmct0IndicadorDeCapacidadDTO(Tmct0IndicadorDeCapacidad tmct0IndicadorDeCapacidad) {
	
		id_indicador_capacidad = tmct0IndicadorDeCapacidad.getId_indicador_capacidad();
		valor = tmct0IndicadorDeCapacidad.getValor();
		cd_codigo = tmct0IndicadorDeCapacidad.getCd_codigo();
		descripcion = tmct0IndicadorDeCapacidad.getDescripcion();
	}


	public Long getId_indicador_capacidad() {
		return id_indicador_capacidad;
	}


	public void setId_indicador_capacidad(Long id_indicador_capacidad) {
		this.id_indicador_capacidad = id_indicador_capacidad;
	}


	public Long getValor() {
		return valor;
	}


	public void setValor(Long valor) {
		this.valor = valor;
	}


	public String getCd_codigo() {
		return cd_codigo;
	}


	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	

}
