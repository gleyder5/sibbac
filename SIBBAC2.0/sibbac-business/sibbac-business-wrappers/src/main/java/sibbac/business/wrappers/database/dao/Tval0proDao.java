package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tval0pro;


@Repository
public interface Tval0proDao extends JpaRepository< Tval0pro, String > {

	@Query( value = "SELECT CDPROVIN FROM BSNVASQL.TVAL0PRO", nativeQuery = true )
	public List< Object > findAllCodigos();

	@Query( value = "SELECT CDPROVIN, NBPROVIN FROM BSNVASQL.TVAL0PRO", nativeQuery = true )
	public List< Object[] > findAllCodigosAndNombres();
	
	public Tval0pro findBycdprovin(String cdprovin);

}
