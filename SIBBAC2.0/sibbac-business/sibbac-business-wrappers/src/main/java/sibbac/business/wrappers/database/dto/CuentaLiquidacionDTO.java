package sibbac.business.wrappers.database.dto;


public class CuentaLiquidacionDTO {

	private long	id;
	private long	entidadRegistro;
	private String	tipoER;
	private long	idMercadoContratacion;
	private String	cdCodigo;
	private long	numCuentaER;
	private boolean	esCuentaPropia;
	private String	tipoNeteoTitulo;
	private String	tipoNeteoEfectivo;
	private long	frecuenciaEnvioExtracto;
	private String	tipoFicheroConciliacion;
	private long	idCuentasDeCompensacion;
	private String	cuentaIberclear;

	public long getId() {
		return id;
	}

	public void setId( long id ) {
		this.id = id;
	}

	public long getEntidadRegistro() {
		return entidadRegistro;
	}

	public void setEntidadRegistro( long entidadRegistro ) {
		this.entidadRegistro = entidadRegistro;
	}

	public String getTipoER() {
		return tipoER;
	}

	public void setTipoER( String tipoER ) {
		this.tipoER = tipoER;
	}

	public long getIdMercadoContratacion() {
		return idMercadoContratacion;
	}

	public void setIdMercadoContratacion( long idMercadoContratacion ) {
		this.idMercadoContratacion = idMercadoContratacion;
	}

	public String getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	public long getNumCuentaER() {
		return numCuentaER;
	}

	public void setNumCuentaER( long numCuentaER ) {
		this.numCuentaER = numCuentaER;
	}

	public boolean isEsCuentaPropia() {
		return esCuentaPropia;
	}

	public void setEsCuentaPropia( boolean esCuentaPropia ) {
		this.esCuentaPropia = esCuentaPropia;
	}

	public String getTipoNeteoTitulo() {
		return tipoNeteoTitulo;
	}

	public void setTipoNeteoTitulo( String tipoNeteoTitulo ) {
		this.tipoNeteoTitulo = tipoNeteoTitulo;
	}

	public String getTipoNeteoEfectivo() {
		return tipoNeteoEfectivo;
	}

	public void setTipoNeteoEfectivo( String tipoNeteoEfectivo ) {
		this.tipoNeteoEfectivo = tipoNeteoEfectivo;
	}

	public long getFrecuenciaEnvioExtracto() {
		return frecuenciaEnvioExtracto;
	}

	public void setFrecuenciaEnvioExtracto( long frecuenciaEnvioExtracto ) {
		this.frecuenciaEnvioExtracto = frecuenciaEnvioExtracto;
	}

	public String getTipoFicheroConciliacion() {
		return tipoFicheroConciliacion;
	}

	public void setTipoFicheroConciliacion( String tipoFicheroConciliacion ) {
		this.tipoFicheroConciliacion = tipoFicheroConciliacion;
	}

	public long getIdCuentasDeCompensacion() {
		return idCuentasDeCompensacion;
	}

	public void setIdCuentasDeCompensacion( long idCuentasDeCompensacion ) {
		this.idCuentasDeCompensacion = idCuentasDeCompensacion;
	}

	public String getCuentaIberclear() {
		return this.cuentaIberclear;
	}

	public void setCuentaIberclear( String cuentaIberclear ) {
		this.cuentaIberclear = cuentaIberclear;
	}

}
