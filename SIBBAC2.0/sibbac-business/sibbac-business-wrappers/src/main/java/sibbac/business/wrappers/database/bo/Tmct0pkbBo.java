package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0pkbDao;
import sibbac.business.wrappers.database.model.Tmct0pkb;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0pkbBo extends AbstractBo<Tmct0pkb, BigDecimal, Tmct0pkbDao> {

}
