package sibbac.business.canones.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.common.CanonCalculadoEfectivoDTO;
import sibbac.business.canones.commons.AgrupacionCanonByAlcDTO;
import sibbac.business.canones.commons.CanonByDesgloseDTO;
import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.canones.commons.CriterioAgrupacionCanonDTO;
import sibbac.business.canones.commons.MercadoContratacionDTO;
import sibbac.business.canones.commons.ReglasCanonesDTO;
import sibbac.business.canones.commons.SimpleComparatorCanonCriteria;
import sibbac.business.canones.commons.TipoCalculo;
import sibbac.business.canones.commons.TipoCanon;
import sibbac.business.canones.model.CanonAgrupadoRegla;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchema;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.economic.RepartoDatosEconomicosHelper;
import sibbac.common.utils.FormatDataUtils;

@Service
public class CalculoCanonesNacionalBo {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(CalculoCanonesNacionalBo.class);

  private static final BigDecimal PUNTO_BASICO = new BigDecimal(10000);

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0bokBo tmct0bokBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private CanonAgrupadoReglaBo canonAgrupadoReglaBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private ApplicationContext ctx;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void bajaCanonCalculadoDesglosesBajaNewTransaction(Tmct0alcId alcId, String auditUser)
      throws SIBBACBusinessException {
    LOG.trace("[bajaCanonCalculadoDesglosesBajaNewTransaction] inicio");
    Tmct0alc alc = alcBo.findById(alcId);
    if (alc.getCanonCalculado() == 'S' && alc.getCdestadoasig() == EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
      alc.setCanonCalculado('N');
      alc.setFhaudit(new Date());
      alc.setCdusuaud(auditUser);
      LOG.trace("[bajaCanonCalculadoDesglosesBajaNewTransaction] baja canon calculado alc: {}", alc.getId());
      if (alc.getReferenciaTitular() != null) {
        alcBo.bajaCanonCalculado(alc, auditUser);
        String msg = String.format("Alc: '%s' dado de baja el canon calculado para el titular: '%s' fecha: '%s'", alc
            .getId().getNucnfliq(), alc.getReferenciaTitular().getCdreftitularpti().trim(), alc.getFeejeliq());
        LOG.warn("[bajaCanonCalculadoDesglosesBajaNewTransaction] {} - {}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            auditUser);
      }
    }
    LOG.trace("[bajaCanonCalculadoDesglosesBajaNewTransaction] fin");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void calcularCanonForBatchProcessNewTransaction(Tmct0alcId alcId, CanonesConfigDTO reglasCanones,
      String auditUser) throws SIBBACBusinessException {
    LOG.trace("[calcularCanon] inicio {}", alcId);
    Tmct0alc alc = alcBo.findById(alcId);
    this.calcularCanonForProcess(alc, reglasCanones, auditUser);
    LOG.trace("[calcularCanon] fin {}", alcId);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRED)
  public void calcularCanonForDesglose(Tmct0alc alc, CanonesConfigDTO reglasCanones, String auditUser)
      throws SIBBACBusinessException {
    String msg;
    LOG.trace("[calcularCanon] inicio");
    List<Tmct0desgloseclitit> listDesgloses = this.findDesgloses(alc);

    if (this.ningunDesgloseUsaReglasCanon(reglasCanones.getMapMercadosContratacion(), alc, listDesgloses)) {
      this.acumularCanones(alc, listDesgloses, auditUser);
      this.marcarAlcNingunDesgloseUsaReglasCanon(alc, auditUser);
    }
    else {
      Tmct0bok bok = alc.getTmct0alo().getTmct0bok();
      List<ReglasCanonesDTO> reglasAplicar = this.findReglasAplicar(bok, reglasCanones.getListReglasCanon());
      if (!reglasAplicar.isEmpty()) {
        LOG.trace("[calcularCanon] buscando dcts de alc {}  ...", alc.getId());
        this.aplicarReglas(reglasCanones.getMapMercadosContratacion(), reglasAplicar, alc, listDesgloses, false,
            reglasCanones.isWriteTmct0log(), auditUser);
        alc.setFhaudit(new Date());
        alc.setCdusuaud(auditUser);
      }
      else {
        msg = String.format("Alc: '%s' No hay reglas de canon que aplicar.", alc.getId().getNucnfliq());
        LOG.warn("[calcularCanon] alc: {} - {} ", alc.getId(), msg);
        LOG.warn("[calcularCanon] alc: {} NO HAY REGLAS DE CANON A APLICAR.", alc.getId());
        throw new SIBBACBusinessException(msg);

      }
    }
    LOG.trace("[calcularCanon] fin");
  }

  private void marcarAlcNingunDesgloseUsaReglasCanon(Tmct0alc alc, String auditUser) {
    String msg = String.format(
        "Alc: '%s' no se han aplicado reglas de canon, ningun desglose es de un mercado que use reglas de canon.", alc
            .getId().getNucnfliq());
    LOG.trace("[aplicarReglas] {} - {}", alc.getId(), msg);
    logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
        auditUser);
    alc.setCanonCalculado('S');
    alc.setFhaudit(new Date());
    alc.setCdusuaud(auditUser);

  }

  private void calcularCanonForProcess(Tmct0alc alc, CanonesConfigDTO reglasCanones, String auditUser)
      throws SIBBACBusinessException {
    String msg;

    LOG.trace("[calcularCanonForProcess] inicio");
    List<Tmct0desgloseclitit> listDesgloses = this.findDesgloses(alc);

    if (this.ningunDesgloseUsaReglasCanon(reglasCanones.getMapMercadosContratacion(), alc, listDesgloses)) {
      this.acumularCanones(alc, listDesgloses, auditUser);
      this.marcarAlcNingunDesgloseUsaReglasCanon(alc, auditUser);
    }
    else {
      Tmct0bok bok = alc.getTmct0alo().getTmct0bok();
      List<ReglasCanonesDTO> reglasAplicar = this.findReglasAplicar(bok, reglasCanones.getListReglasCanon());
      if (!reglasAplicar.isEmpty()) {
        if (alc.getCanonCalculado() != 'S') {
          LOG.trace("[calcularCanon] buscando dcts de alc {}  ...", alc.getId());

          BigDecimal canonAntesAplicarReglas = alc.getImCanonBasico().add(alc.getImCanonEspecial());
          BigDecimal canonBonificadoAntesAplicarReglas = alc.getImcanoncontreu();

          listDesgloses = this.findDesgloses(alc);
          this.aplicarReglas(reglasCanones.getMapMercadosContratacion(), reglasAplicar, alc, listDesgloses, true,
              reglasCanones.isWriteTmct0log(), auditUser);

          this.calcularAjusteDespuesAplicarReglas(alc, canonAntesAplicarReglas, canonBonificadoAntesAplicarReglas,
              auditUser);

          alc.setCanonCalculado('S');
          alc.setFhaudit(new Date());
          alc.setCdusuaud(auditUser);
        }
      }
      else {
        LOG.trace("[calcularCanonForProcess] {} se marca el alc canonCalculado ='E' error canon...", alc.getId());
        alc.setCanonCalculado('E');
        alc.setFhaudit(new Date());
        alc.setCdusuaud(auditUser);
        msg = String.format(
            "Alc: '%s' No hay reglas de canon que aplicar, Se marca el alc para no reprocesar, revise las reglas.", alc
                .getId().getNucnfliq());
        LOG.warn("[calcularCanonForProcess] alc: {} - {} ", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            auditUser);

        LOG.warn("[calcularCanonForProcess] alc: {} NO HAY REGLAS DE CANON A APLICAR.", alc.getId());
      }
    }
    LOG.trace("[calcularCanonForProcess] fin");
  }

  private void calcularAjusteDespuesAplicarReglas(Tmct0alc alc, BigDecimal canonAntesAplicarReglas,
      BigDecimal canonBonificadoAntesAplicarReglas, String auditUser) throws SIBBACBusinessException {
    LOG.trace("[calcularAjusteDespuesAplicarReglas] inicio {}", alc.getId());
    String msg;
    BigDecimal ajuste;
    if (canonAntesAplicarReglas.compareTo(alc.getImCanonBasico().add(alc.getImCanonEspecial())) != 0
        || canonBonificadoAntesAplicarReglas.compareTo(alc.getImcanoncontreu()) != 0) {
      LOG.trace("[calcularAjusteDespuesAplicarReglas] {} hay diferencia en el canon...", alc.getId());
      Tmct0alo alo = alc.getTmct0alo();
      if (alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
        alo.setTmct0estadoByCdestadoasig(new Tmct0estado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()));
        alo.setFhaudit(new Date());
        alo.setCdusuaud(auditUser);
      }

      if (alc.getCdestadoasig() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
        alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
        alc.setFhaudit(new Date());
        alc.setCdusuaud(auditUser);
      }

      List<Tmct0alc> listAlcs = new ArrayList<Tmct0alc>(alo.getTmct0alcs());
      for (int i = 0; i < listAlcs.size(); i++) {
        if (alc.getId().equals(listAlcs.get(i).getId())) {
          listAlcs.remove(i);
          listAlcs.add(i, alc);
          break;
        }
      }

      CalculosEconomicosNacionalBo calculos = ctx.getBean(CalculosEconomicosNacionalBo.class, alc.getTmct0alo(),
          listAlcs);

      if (alo.getNutitcli().compareTo(alc.getNutitliq()) == 0
          && (ordBo.isRouting(alo.getTmct0bok().getTmct0ord()) || ordBo.isScriptDividend(alo.getTmct0bok()
              .getTmct0ord()))) {
        LOG.trace("[calcularAjusteDespuesAplicarReglas] {} ROUTING|SD con un solo desglose...", alc.getId());
        ajuste = alo.getImdereeu().subtract(alc.getImcanoncontreu().add(alc.getImajusvb()));

        LOG.trace("[calcularAjusteDespuesAplicarReglas] {} ajuste: {}...", alc.getId(), ajuste);
        if (ajuste.compareTo(BigDecimal.ZERO) != 0) {
          msg = String
              .format(
                  "Alc: '%s' El NETO de la operacion NO puede modificarse por ser ROUTING o SD, la diferencia de canon se repercute a com. ajuste y beneficio de la sv.",
                  alc.getId().getNucnfliq());
          LOG.trace("[aplicarReglas] {} - {}", alc.getId(), msg);
          logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg,
              "", auditUser);

          this.ajustarContraBeneficioSv(alc, ajuste);
          try {
            LOG.trace("[calcularAjusteDespuesAplicarReglas] {} se realizan los calculos normales...", alc.getId());
            calculos.calcularDatosEconomicosSinCanonesAndSinRepartirAloAlc();
          }
          catch (EconomicDataException e) {
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
        }
      }
      else if (!alcBo.puedoModificarNetoFinal(alc)) {
        LOG.trace("[calcularAjusteDespuesAplicarReglas] {} NO se puede modificar el neto final...", alc.getId());
        msg = String
            .format(
                "Alc: '%s' El NETO de la operacion NO puede modificarse, la diferencia de canon se repercute a com. ajuste y beneficio de la sv.",
                alc.getId().getNucnfliq());
        LOG.trace("[aplicarReglas] {} - {}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            auditUser);

        ajuste = alc.getImcanoncontreu().subtract(canonBonificadoAntesAplicarReglas);

        this.ajustarContraBeneficioSv(alc, ajuste);

        try {
          LOG.trace("[calcularAjusteDespuesAplicarReglas] {} unicamente se reparten los datos economicos...",
              alc.getId());
          calculos.repartirCalculosAlcEnDct();
        }
        catch (EconomicDataException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
      else {
        LOG.trace("[calcularAjusteDespuesAplicarReglas] {} se puede modificar el neto final...", alc.getId());
        msg = String.format("Alc: '%s' El neto de la operacion puede modificarse, se realizan los calculos normales.",
            alc.getId().getNucnfliq());
        LOG.trace("[aplicarReglas] {} - {}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            auditUser);

        try {
          LOG.trace("[calcularAjusteDespuesAplicarReglas] {} se realizan los calculos normales...", alc.getId());
          calculos.calcularDatosEconomicosSinCanonesAndSinRepartirAloAlc();
        }
        catch (EconomicDataException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }

      if (alcBo.isDevengada(alc)) {
        LOG.trace("[calcularAjusteDespuesAplicarReglas] {} esta devengada se pone en PDTE_AJUSTE...", alc.getId());
        alc.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_AJUSTE.getId()));
      }

    }
    LOG.trace("[calcularAjusteDespuesAplicarReglas] fin {}", alc.getId());
  }

  @SuppressWarnings("unused")
  private void ajusteCanonMayorEfectivo(Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses, String cdusuaud)
      throws SIBBACBusinessException {

    LOG.trace("[ajusteCanonMayorEfectivo] {} inicio...", alc.getId());
    String msg;
    RoundingMode roundedMode = RoundingMode.HALF_UP;
    BigDecimal canonBonificado = alc.getImcanoncontreu();

    if (canonBonificado.compareTo(alc.getImefeagr()) >= 0) {
      LOG.trace("[ajusteCanonMayorEfectivo] {} canon {} mayor que efecitivo {}...", alc.getId(), canonBonificado,
          alc.getImefeagr());
      Tmct0alo alo = alc.getTmct0alo();

      List<Tmct0alc> listAlcs = new ArrayList<Tmct0alc>(alo.getTmct0alcs());
      for (int i = 0; i < listAlcs.size(); i++) {
        if (alc.getId().equals(listAlcs.get(i).getId())) {
          listAlcs.add(i, alc);
          break;
        }
      }

      BigDecimal canonMayorEfectivo = alc.getImefeagr().subtract(alc.getImcomisn()).subtract(alc.getImcombco())
          .subtract(new BigDecimal("0.01"));

      msg = String.format("Alc: '%s' el canon bonificado: {} es mayor que el efectivo: {}, ajustamos el canon: {}", alc
          .getId().getNucnfliq(), canonBonificado.setScale(2, roundedMode), alc.getImefeagr().setScale(2, roundedMode),
          canonMayorEfectivo.setScale(2, roundedMode));
      LOG.trace("[aplicarReglas] {} - {}", alc.getId(), msg);
      logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
          cdusuaud);

      if (!alcBo.puedoModificarNetoFinal(alc)) {
        LOG.trace("[ajusteCanonMayorEfectivo] {} NO puedo modificar el neto...", alc.getId());

        BigDecimal ajuste = canonBonificado.subtract(canonMayorEfectivo);
        LOG.trace("[ajusteCanonMayorEfectivo] {} ajuste {}...", alc.getId(), ajuste);

        this.ajustarContraBeneficioSv(alc, ajuste);

        alc.setImcanoncontrdv(alc.getImcanoncontreu().setScale(2, roundedMode));
        alc.setImderges(canonMayorEfectivo.setScale(2, roundedMode));
        alc.setImcanoncontreu(canonMayorEfectivo.setScale(2, roundedMode));

        LOG.trace("[ajusteCanonMayorEfectivo] {} canon real {}, canon cobrado {}...", alc.getId(),
            alc.getImcanoncontreu(), canonMayorEfectivo);

        msg = String.format("Alc: '%s' canon real: '%s' canon cobrado: '%s'", alc.getId().getNucnfclt(), alc
            .getImcanoncontreu().setScale(2, roundedMode), canonMayorEfectivo.setScale(2, roundedMode));
        LOG.trace("[ajusteCanonMayorEfectivo] {} - {}...", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            cdusuaud);
        CalculosEconomicosNacionalBo calculos = ctx.getBean(CalculosEconomicosNacionalBo.class, alc.getTmct0alo(),
            listAlcs);
        try {
          LOG.trace("[ajusteCanonMayorEfectivo] {} unicamente se reparten los datos economicos...", alc.getId());
          calculos.repartirCalculosAlcEnDct();
        }
        catch (EconomicDataException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
      else {
        msg = String
            .format(
                "Alc: '%s' El neto de la operacion puede modificarse, se realizan los calculos normales con el canon ajustado.",
                alc.getId().getNucnfliq());
        LOG.trace("[ajusteCanonMayorEfectivo] {} - {}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            cdusuaud);

        alc.setImcanoncontrdv(alc.getImcanoncontreu().setScale(2, roundedMode));
        alc.setImderges(canonMayorEfectivo.setScale(2, roundedMode));
        alc.setImcanoncontreu(canonMayorEfectivo.setScale(2, roundedMode));

        msg = String.format("Alc: '%s' canon real: '%s' canon cobrado: '%s'", alc.getId().getNucnfclt(), alc
            .getImcanoncontreu().setScale(2, roundedMode), canonMayorEfectivo.setScale(2, roundedMode));
        LOG.trace("[ajusteCanonMayorEfectivo] {} - {}...", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            cdusuaud);

        CalculosEconomicosNacionalBo calculos = ctx.getBean(CalculosEconomicosNacionalBo.class, alc.getTmct0alo(),
            listAlcs);
        try {
          LOG.trace("[ajusteCanonMayorEfectivo] {} se reparten los datos economicos...", alc.getId());
          calculos.repartirCalculosAlcEnDct();
          LOG.trace("[ajusteCanonMayorEfectivo] {} se realizan los calculos economicos completos...", alc.getId());
          calculos.calcularDatosEconomicosSinCanonesAndSinRepartirAloAlc();
        }
        catch (EconomicDataException e) {
          throw new SIBBACBusinessException(e.getMessage(), e);
        }
      }
      if (alcBo.isDevengada(alc)) {
        LOG.trace("[ajusteCanonMayorEfectivo] {} esta devengada se pone en PDTE_AJUSTE...", alc.getId());
        alc.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_AJUSTE.getId()));
      }
    }
    LOG.trace("[ajusteCanonMayorEfectivo] {} fin...", alc.getId());
  }

  private void ajustarContraBeneficioSv(Tmct0alc alc, BigDecimal ajuste) {
    LOG.trace("[aplicarReglas] {} ajuste: {}", alc.getId(), ajuste);

    if (alc.getImajusvb() == null) {
      alc.setImajusvb(BigDecimal.ZERO);
    }
    alc.setImajusvb(alc.getImajusvb().add(ajuste));

    if (alc.getTmct0alo().getCanoncomppagoclte() == 1 || alc.getTmct0alo().getCanoncontrpagoclte() == 1
        || alc.getTmct0alo().getCanonliqpagoclte() == 1) {
      alc.setImfinsvb(alc.getImfinsvb().add(ajuste));
    }

    alc.setImcomsvb(alc.getImcomsvb().subtract(ajuste));
  }

  private List<Tmct0desgloseclitit> findDesgloses(Tmct0alc alc) {
    List<Tmct0desgloseclitit> listDesgloses = new ArrayList<Tmct0desgloseclitit>();
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listDcs) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        listDesgloses.addAll(dc.getTmct0desgloseclitits());
      }
    }
    return listDesgloses;
  }

  private void aplicarReglas(Map<String, MercadoContratacionDTO> mercadosContratacion,
      List<ReglasCanonesDTO> reglasAplicar, Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses, boolean agrupar,
      boolean writeTmct0log, String cdusuaud) throws SIBBACBusinessException {
    String msg;
    RoundingMode roundedMode = RoundingMode.HALF_UP;
    LOG.trace("[aplicarReglas] inicio alc {}  ...", alc.getId());

    BigDecimal canon = alc.getImcanoncontrdv().setScale(2, roundedMode);
    BigDecimal canonEspecial = alc.getImCanonEspecial().setScale(2, roundedMode);
    BigDecimal canonBonificado = alc.getImcanoncontreu().setScale(2, roundedMode);

    dctBo.loadTmct0ejeFeeSchema(listDesgloses);

    Map<ReglasCanonesDTO, BigDecimal> mapAplicacionReglas = new HashMap<ReglasCanonesDTO, BigDecimal>();
    Map<Long, Tmct0desgloseclitit> mapDesglosesByNudesglose = new HashMap<Long, Tmct0desgloseclitit>();
    List<AgrupacionCanonByAlcDTO> reglasAplicadas = new ArrayList<AgrupacionCanonByAlcDTO>();
    List<AgrupacionCanonByAlcDTO> bonificacionesAplicadas = new ArrayList<AgrupacionCanonByAlcDTO>();

    LOG.trace("[aplicarReglas] limpiando canones dct...");

    // LIMPIAR CAMPOS DE CANONES Y AGRUPAMOS POR NUDESGLOSE
    this.limpiarCanonesAndAgruparByNudesglose(mercadosContratacion, alc, listDesgloses, mapDesglosesByNudesglose);

    // APLICAMOS REGLAS DE CALCULO BASICO
    for (ReglasCanonesDTO regla : reglasAplicar) {
      if (regla.getTipoCalculo() == TipoCalculo.CALCULO && regla.getTipoCanon() == TipoCanon.BASICO) {
        reglasAplicadas.addAll(this.aplicarRegla(regla, alc, mapDesglosesByNudesglose, agrupar, cdusuaud));
      }
    }

    // APLICAMOS REGLAS DE CALCULO ESPECIAL
    for (ReglasCanonesDTO regla : reglasAplicar) {
      if (regla.getTipoCalculo() == TipoCalculo.CALCULO && regla.getTipoCanon() == TipoCanon.ESPECIAL) {
        reglasAplicadas.addAll(this.aplicarRegla(regla, alc, mapDesglosesByNudesglose, agrupar, cdusuaud));
      }
    }

    if (reglasAplicadas.size() == 0) {
      msg = String.format("Alc: '%s' No se ha aplicado ninguna regla de canon.", alc.getId().getNucnfliq());
      throw new SIBBACBusinessException(msg);
    }

    // APLICAMOS BONIFICACIONES
    for (ReglasCanonesDTO regla : reglasAplicar) {
      if (regla.getTipoCalculo() == TipoCalculo.BONIFICACION) {
        bonificacionesAplicadas.addAll(this.aplicarRegla(regla, alc, mapDesglosesByNudesglose, agrupar, cdusuaud));
      }
    }

    // ACUMUMLAMOS CANONES DESDE DCT A ALC
    this.acumularCanones(alc, listDesgloses, cdusuaud);
    msg = String
        .format(
            "Alc: '%s' ANTES canon: '%s', canon bonificado: '%s', canon especial: '%s' ## DESPUES canon: '%s', canon bonificado: '%s', canon especial: '%s'",
            alc.getId().getNucnfliq(), canon.setScale(2, roundedMode), canonBonificado.setScale(2, roundedMode),
            canonEspecial.setScale(2, roundedMode), alc.getImcanoncontrdv().setScale(2, roundedMode), alc
                .getImcanoncontreu().setScale(2, roundedMode), alc.getImCanonEspecial().setScale(2, roundedMode));
    LOG.trace("[aplicarReglas] {} - {}", alc.getId(), msg);
    if (writeTmct0log) {
      logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
          cdusuaud);
    }

    // AJUSTAMOS SI EL CANON ES MAYOR AL EFECTIVO
    this.ajustePorCanonMayorEfectivo(alc, listDesgloses, cdusuaud);

    if (agrupar) {
      // RESTAMOS EL AJUSTE A LAS REGLAS APLICADAS Y LAS BONIFICACIONES
      // APLICADAS
      this.ajustarCanonBonificacionAndNetoNegativo(reglasAplicadas, bonificacionesAplicadas, alc, listDesgloses);
      // GRABAMOS/ACTUALIZAMOS LOS REGISTROS DONDE SE GUARDA EL CANON AGRUPADO
      this.grabarReglasAplicadasAndBonificaciones(reglasAplicadas, bonificacionesAplicadas, cdusuaud);
    }
    mapAplicacionReglas.clear();
    mapDesglosesByNudesglose.clear();
    reglasAplicadas.clear();
    bonificacionesAplicadas.clear();

    LOG.trace("[aplicarReglas] fin alc {}  ...", alc.getId());
  }

  private boolean ningunDesgloseUsaReglasCanon(Map<String, MercadoContratacionDTO> mercadosContratacion, Tmct0alc alc,
      List<Tmct0desgloseclitit> listDesgloses) throws SIBBACBusinessException {
    String msg;
    boolean res = true;
    for (Tmct0desgloseclitit dct : listDesgloses) {
      if (dct.getCdPlataformaNegociacion() == null
          || mercadosContratacion.get(dct.getCdPlataformaNegociacion().trim()) == null) {
        msg = String.format("Alc: '%s' mercado contratacion '%s' no configurado en el maestro de mercados.", alc
            .getId().getNucnfliq(), dct.getCdPlataformaNegociacion());
        throw new SIBBACBusinessException(msg);
      }
      else if (mercadosContratacion.get(dct.getCdPlataformaNegociacion().trim()).isReglas()) {
        res = false;
        break;
      }
    }
    return res;
  }

  private void limpiarCanonesAndAgruparByNudesglose(Map<String, MercadoContratacionDTO> mercadosContratacion,
      Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses, Map<Long, Tmct0desgloseclitit> mapDesglosesByNudesglose)
      throws SIBBACBusinessException {
    String msg;
    LOG.trace("[limpiarCanonesAndAgruparByNudesglose] inicio.");
    for (Tmct0desgloseclitit dct : listDesgloses) {
      if (dct.getCdPlataformaNegociacion() == null
          || mercadosContratacion.get(dct.getCdPlataformaNegociacion().trim()) == null) {
        msg = String.format("Alc: '%s' mercado contratacion '%s' no configurado en el maestro de mercados.", alc
            .getId().getNucnfliq(), dct.getCdPlataformaNegociacion());
        throw new SIBBACBusinessException(msg);
      }
      else if (mercadosContratacion.get(dct.getCdPlataformaNegociacion().trim()).isReglas()) {
        dct.setImcanoncontrdv(BigDecimal.ZERO);
        dct.setImcanoncontreu(BigDecimal.ZERO);
        dct.setImCanonEspecial(BigDecimal.ZERO);
        dct.setImCanonBasico(BigDecimal.ZERO);
      }
      mapDesglosesByNudesglose.put(dct.getNudesglose(), dct);

    }
    LOG.trace("[limpiarCanonesAndAgruparByNudesglose] fin.");
  }

  private void acumularCanones(Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses, String cdusuaud) {
    LOG.trace("[acumularCanones] acumulando canones en {}...", alc.getId());
    RoundingMode roundedMode = RoundingMode.HALF_UP;
    alc.setImcanoncontrdv(BigDecimal.ZERO);
    alc.setImcanoncontreu(BigDecimal.ZERO);
    alc.setImCanonEspecial(BigDecimal.ZERO);
    alc.setImCanonBasico(BigDecimal.ZERO);

    for (Tmct0desgloseclitit dct : listDesgloses) {
      dct.setImcanoncontreu(dct.getImcanoncontreu().add(dct.getImCanonEspecial()));
      dct.setImcanoncontrdv(dct.getImcanoncontreu().setScale(2, roundedMode));
      // ACUMULAR ALC
      alc.setImcanoncontreu(alc.getImcanoncontreu().add(dct.getImcanoncontreu()));
      alc.setImcanoncontrdv(alc.getImcanoncontrdv().add(dct.getImcanoncontrdv()));
      alc.setImCanonBasico(alc.getImCanonBasico().add(dct.getImCanonBasico()));
      alc.setImCanonEspecial(alc.getImCanonEspecial().add(dct.getImCanonEspecial()));
      dct.setAuditFechaCambio(new Date());
      dct.setAuditUser(cdusuaud);
    }

    LOG.trace("[aplicarReglas] copiando canones campos antiguos {}...", alc.getId());
    alc.setImderges(alc.getImcanoncontreu().setScale(2, roundedMode));
    alc.setFhaudit(new Date());
    alc.setCdusuaud(cdusuaud);
    LOG.trace("[acumularCanones] fin acumulando canones en {}...", alc.getId());
  }

  private void ajustePorCanonMayorEfectivo(Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses, String cdusuaud)
      throws SIBBACBusinessException {
    RoundingMode roundedMode = RoundingMode.HALF_UP;
    LOG.trace("[ajustePorCanonMayorEfectivo] inicio {}.", alc.getId());
    if (alc.getImefeagr().compareTo(alc.getImcanoncontreu()) <= 0) {
      if (alc.getTmct0alo().getCanoncomppagoclte() == 0 && alc.getTmct0alo().getCanoncontrpagoclte() == 0
          && alc.getTmct0alo().getCanonliqpagoclte() == 0) {
        BigDecimal canonBonificado = alc.getImcanoncontreu().setScale(2, roundedMode);
        BigDecimal canonMayorEfectivo = alc.getImefeagr().subtract(new BigDecimal("0.01"));
        alc.setImcanoncontreu(canonMayorEfectivo.setScale(2, roundedMode));
        alc.setImderges(canonMayorEfectivo.setScale(2, roundedMode));
        RepartoDatosEconomicosHelper reparto = new RepartoDatosEconomicosHelper();
        reparto.repartirBigDecimals(listDesgloses, canonMayorEfectivo.setScale(2, roundedMode), alc.getImefeagr(),
            "imefectivo", "imcanoncontreu", 2, RoundingMode.HALF_DOWN);

        String msg = String.format(
            "Alc: '%s' el canon bonificado: '%s' es mayor que el efectivo: '%s', ajustamos el canon: '%s'", alc.getId()
                .getNucnfliq(), canonBonificado.setScale(2, roundedMode), alc.getImefeagr().setScale(2, roundedMode),
            canonMayorEfectivo.setScale(2, roundedMode));
        LOG.trace("[ajustePorCanonMayorEfectivo] {} - {}", alc.getId(), msg);
        logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), alc.getId().getNucnfclt(), msg, "",
            cdusuaud);
      }
      else {
        LOG.warn(
            "[ajustePorCanonMayorEfectivo] {} el canon: {} es mayor al efectivo {}, pero el cliente no paga los canones, no se ajusta.",
            alc.getId(), alc.getImcanoncontreu(), alc.getImefeagr());
      }
    }
    else {
      LOG.trace("[ajustePorCanonMayorEfectivo] no se necesita ajuste por neto negativo {}.", alc.getId());
    }
    LOG.trace("[ajustePorCanonMayorEfectivo] fin {}.", alc.getId());
  }

  private void grabarReglasAplicadasAndBonificaciones(List<AgrupacionCanonByAlcDTO> reglasAplicadas,
      List<AgrupacionCanonByAlcDTO> bonificacionesAplicadas, String cdusuaud) {
    LOG.trace("[grabarReglasAplicadasAndBonificaciones] inicio.");
    for (AgrupacionCanonByAlcDTO reglaAplicada : reglasAplicadas) {
      if (reglaAplicada.getCanonRepartir() != null && reglaAplicada.getSumatorioEfectivo() != null) {
        CanonAgrupadoRegla canonAgrupado = canonAgrupadoReglaBo.findCanonCalculadoByAgrupacionAndRegla(
            reglaAplicada.getAgrupacion(), reglaAplicada.getReglaCanon());
        if (canonAgrupado == null) {
          canonAgrupado = canonAgrupadoReglaBo.crearCanonAgrupadoRegla(reglaAplicada.getAgrupacion(),
              reglaAplicada.getReglaCanon());
          LOG.trace(
              "[grabarReglasAplicadasAndBonificaciones] no hay registro canon agrupado de operaciones previas {}.",
              canonAgrupado);
        }
        canonAgrupado.setImporteCanon(canonAgrupado.getImporteCanon().add(reglaAplicada.getCanonRepartir()));
        canonAgrupado.setImporteEfectivo(canonAgrupado.getImporteEfectivo().add(reglaAplicada.getSumatorioEfectivo()));
        canonAgrupado.setCdUsuaud(cdusuaud);
        canonAgrupado.setFhaudit(new Date());
        if (reglaAplicada.getReglaCanon().getIdReglaCanonParametrizacion() != null) {
          canonAgrupado.setParametrizacion(reglaAplicada.getReglaCanon().getIdReglaCanonParametrizacion().longValue());
        }
        LOG.trace("[grabarReglasAplicadasAndBonificaciones] save {}.", canonAgrupado);
        canonAgrupadoReglaBo.save(canonAgrupado);
      }
    }
    for (AgrupacionCanonByAlcDTO reglaAplicada : bonificacionesAplicadas) {
      if (reglaAplicada.getCanonRepartir() != null && reglaAplicada.getSumatorioEfectivo() != null) {
        CanonAgrupadoRegla canonAgrupado = canonAgrupadoReglaBo.findCanonCalculadoByAgrupacionAndRegla(
            reglaAplicada.getAgrupacion(), reglaAplicada.getReglaCanon());
        if (canonAgrupado == null) {
          canonAgrupado = canonAgrupadoReglaBo.crearCanonAgrupadoRegla(reglaAplicada.getAgrupacion(),
              reglaAplicada.getReglaCanon());
          LOG.trace(
              "[grabarReglasAplicadasAndBonificaciones] no hay registro canon agrupado de operaciones previas {}.",
              canonAgrupado);
        }
        canonAgrupado.setImporteCanon(canonAgrupado.getImporteCanon().add(reglaAplicada.getCanonRepartir()));
        canonAgrupado.setImporteEfectivo(canonAgrupado.getImporteEfectivo().add(reglaAplicada.getSumatorioEfectivo()));
        canonAgrupado.setCdUsuaud(cdusuaud);
        canonAgrupado.setFhaudit(new Date());
        if (reglaAplicada.getReglaCanon().getIdReglaCanonParametrizacion() != null) {
          canonAgrupado.setParametrizacion(reglaAplicada.getReglaCanon().getIdReglaCanonParametrizacion().longValue());
        }
        LOG.trace("[grabarReglasAplicadasAndBonificaciones] save {}.", canonAgrupado);
        canonAgrupadoReglaBo.save(canonAgrupado);
      }
    }
    LOG.trace("[grabarReglasAplicadasAndBonificaciones] fin.");
  }

  private void ajustarCanonBonificacionAndNetoNegativo(List<AgrupacionCanonByAlcDTO> reglasAplicadas,
      List<AgrupacionCanonByAlcDTO> bonificacionesAplicadas, Tmct0alc alc, List<Tmct0desgloseclitit> listDesgloses) {
    LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] inicio.");
    for (Tmct0desgloseclitit dct : listDesgloses) {
      if (dct.getImcanoncontreu().compareTo(dct.getImCanonBasico().add(dct.getImCanonEspecial())) != 0) {
        BigDecimal ajuste = dct.getImCanonBasico().add(dct.getImCanonEspecial()).subtract(dct.getImcanoncontreu());
        for (AgrupacionCanonByAlcDTO reglaAplicada : reglasAplicadas) {
          if (reglaAplicada.getReglaCanon().getTipoCanon() == TipoCanon.BASICO) {
            if (reglaAplicada.getCanonRepartir() != null) {

              for (CanonByDesgloseDTO desglose : reglaAplicada.getListDesgloses()) {
                if (desglose.getNudesglose() == dct.getNudesglose()) {
                  LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada antes Ajuste: {}", reglaAplicada);
                  if (ajuste.compareTo(desglose.getCanon()) >= 0) {
                    reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(desglose.getCanon()));
                    ajuste = ajuste.subtract(desglose.getCanon());
                    desglose.setCanon(BigDecimal.ZERO);
                  }
                  else {
                    reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(ajuste));
                    desglose.setCanon(desglose.getCanon().subtract(ajuste));
                    ajuste = BigDecimal.ZERO;
                  }
                  LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada despues Ajuste: {}", reglaAplicada);
                  break;
                }
              }
              if (ajuste.compareTo(BigDecimal.ZERO) == 0) {
                break;
              }
            }
          }
        }

        for (AgrupacionCanonByAlcDTO reglaAplicada : reglasAplicadas) {
          if (reglaAplicada.getReglaCanon().getTipoCanon() == TipoCanon.ESPECIAL) {
            if (reglaAplicada.getCanonRepartir() != null) {
              for (CanonByDesgloseDTO desglose : reglaAplicada.getListDesgloses()) {
                if (desglose.getNudesglose() == dct.getNudesglose()) {
                  LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada antes Ajuste: {}", reglaAplicada);
                  if (ajuste.compareTo(desglose.getCanon()) >= 0) {
                    reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(desglose.getCanon()));
                    ajuste = ajuste.subtract(desglose.getCanon());
                    desglose.setCanon(BigDecimal.ZERO);
                  }
                  else {
                    reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(ajuste));
                    desglose.setCanon(desglose.getCanon().subtract(ajuste));
                    ajuste = BigDecimal.ZERO;
                  }
                  LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada despues Ajuste: {}", reglaAplicada);
                  break;
                }
              }
              if (ajuste.compareTo(BigDecimal.ZERO) == 0) {
                break;
              }
            }
          }
        }

        ajuste = alc.getImcanoncontrdv().subtract(alc.getImcanoncontreu());
        if (ajuste.compareTo(BigDecimal.ZERO) != 0) {
          for (AgrupacionCanonByAlcDTO reglaAplicada : bonificacionesAplicadas) {
            if (reglaAplicada.getReglaCanon().getTipoCanon() == TipoCanon.BASICO) {
              if (reglaAplicada.getCanonRepartir() != null) {

                for (CanonByDesgloseDTO desglose : reglaAplicada.getListDesgloses()) {
                  if (desglose.getNudesglose() == dct.getNudesglose()) {
                    LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada antes Ajuste: {}", reglaAplicada);
                    if (ajuste.compareTo(desglose.getCanon()) >= 0) {
                      reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(desglose.getCanon()));
                      ajuste = ajuste.subtract(desglose.getCanon());
                      desglose.setCanon(BigDecimal.ZERO);
                    }
                    else {
                      reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(ajuste));
                      desglose.setCanon(desglose.getCanon().subtract(ajuste));
                      ajuste = BigDecimal.ZERO;
                    }
                    LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada despues Ajuste: {}",
                        reglaAplicada);
                    break;
                  }
                }
                if (ajuste.compareTo(BigDecimal.ZERO) == 0) {
                  break;
                }
              }
            }
          }

          for (AgrupacionCanonByAlcDTO reglaAplicada : bonificacionesAplicadas) {
            if (reglaAplicada.getReglaCanon().getTipoCanon() == TipoCanon.ESPECIAL) {
              if (reglaAplicada.getCanonRepartir() != null) {
                for (CanonByDesgloseDTO desglose : reglaAplicada.getListDesgloses()) {
                  if (desglose.getNudesglose() == dct.getNudesglose()) {
                    LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada antes Ajuste: {}", reglaAplicada);
                    if (ajuste.compareTo(desglose.getCanon()) >= 0) {
                      reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(desglose.getCanon()));
                      ajuste = ajuste.subtract(desglose.getCanon());
                      desglose.setCanon(BigDecimal.ZERO);
                    }
                    else {
                      reglaAplicada.setCanonRepartir(reglaAplicada.getCanonRepartir().subtract(ajuste));
                      desglose.setCanon(desglose.getCanon().subtract(ajuste));
                      ajuste = BigDecimal.ZERO;
                    }
                    LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] reglaAplicada despues Ajuste: {}",
                        reglaAplicada);
                    break;
                  }
                }
                if (ajuste.compareTo(BigDecimal.ZERO) == 0) {
                  break;
                }
              }
            }
          }
        }
      }
    }
    LOG.trace("[ajustarCanonBonificacionAndNetoNegativo] fin.");
  }

  private void agruparByRegla(ReglasCanonesDTO regla, Tmct0alc alc, Collection<Tmct0desgloseclitit> listDesgloses,
      List<AgrupacionCanonByAlcDTO> listAgrByAlc, boolean agrupar) throws SIBBACBusinessException {
    LOG.trace("[agruparByRegla] {} agrupando desgloses: {}...", alc.getId(), regla);
    String titular;
    CanonByDesgloseDTO desglose;
    AgrupacionCanonByAlcDTO agrAlc;
    CriterioAgrupacionCanonDTO key;
    Tmct0ejeFeeSchema feeSchema;
    RoundingMode roundedMode = RoundingMode.HALF_UP;
    Map<CriterioAgrupacionCanonDTO, AgrupacionCanonByAlcDTO> mapAgrByAlc = new HashMap<CriterioAgrupacionCanonDTO, AgrupacionCanonByAlcDTO>();
    for (Tmct0desgloseclitit dct : listDesgloses) {
      key = new CriterioAgrupacionCanonDTO();

      if (regla.getMercadoContratacion() != null
          && !regla.getMercadoContratacion().trim().equals(dct.getCdPlataformaNegociacion().trim())) {
        LOG.trace("[agruparByRegla] {} el mercado contratacion no machea con la regla", alc.getId());
        continue;
      }

      if (regla.getListaTipoOperacionBolsaComparator() != null
          && (regla.getListaTipoOperacionBolsaComparator() == SimpleComparatorCanonCriteria.IN
              && !regla.getListaTipoOperacionBolsa().contains(dct.getTpopebol().trim()) || regla
              .getListaTipoOperacionBolsaComparator() == SimpleComparatorCanonCriteria.NOT_IN
              && regla.getListaTipoOperacionBolsa().contains(dct.getTpopebol().trim()))) {

        LOG.trace("[agruparByRegla] {} el tipo operacion bolsa no machea con la regla.", alc.getId());
        continue;
      }

      if (regla.getListaSegmentoMercadoComparator() != null
          && (regla.getListaSegmentoMercadoComparator() == SimpleComparatorCanonCriteria.IN
              && !regla.getListSegmentoMercadoContratacion().contains(dct.getCdsegmento().trim()) || regla
              .getListaSegmentoMercadoComparator() == SimpleComparatorCanonCriteria.NOT_IN
              && regla.getListSegmentoMercadoContratacion().contains(dct.getCdsegmento().trim()))) {
        LOG.trace("[agruparByRegla] {} el segmento del mercado no coincide con el del desglose.", alc.getId());
        continue;
      }

      feeSchema = dct.getTmct0ejeFeeSchema();

      if (regla.getTipoCanon() == TipoCanon.ESPECIAL) {
        if (feeSchema == null) {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no tiene feeSchema.", alc.getId());
          continue;
        }

        if (regla.isEjecucionSubasta() && feeSchema.getSubastaApertura() != 'Y'
            && feeSchema.getSubastaVolatilidad() != 'Y' && feeSchema.getSubastaCierre() != 'Y') {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no es subasta apertura/volatilidad/cierre.",
              alc.getId());
          continue;
        }
        if (regla.isOrdenBloqueCombinado() && feeSchema.getOrdenBloqueCombinado() != 'Y') {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no es bloque combinado.", alc.getId());
          continue;
        }

        if (regla.isRestriccionOrden() && feeSchema.getRestriccionOrden() != 'Y') {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no es restrinccion orden.", alc.getId());
          continue;
        }

        if (regla.isOrdenOculta() && feeSchema.getOrdenOculta() != 'Y') {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no es orden oculta.", alc.getId());
          continue;
        }
        if (regla.isOrdenPuntoMedio() && feeSchema.getOrdenPuntoMedio() != 'Y') {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no es punto medio.", alc.getId());
          continue;
        }

        if (regla.isOrdenVolumenOculto() && feeSchema.getOrdenVolumenOculto() != 'Y') {
          LOG.trace("[agruparByRegla] {} regla tipo especial, desglose no es orden volumen oculto.", alc.getId());
          continue;
        }
      }

      if (regla.isCriterioAgrupacionFecha()) {
        key.setFecha(dct.getFeejecuc());
      }
      if (regla.isCriterioAgrupacionEjecucionMercado()) {
        key.setEjecucionMercado(dct.getNurefexemkt().trim());
      }
      if (regla.isCriterioAgrupacionIsin()) {
        key.setIsin(dct.getCdisin().trim());
      }
      if (regla.isCriterioAgrupacionMercadoContratacion()) {
        key.setMercadoContratacion(dct.getCdPlataformaNegociacion().trim());
      }

      if (regla.isCriterioAgrupacionOrdenMercado()) {
        key.setOrdenMercado(dct.getNuordenmkt().trim());
      }

      if (regla.isCriterioAgrupacionPrecio()) {
        key.setPrecio(dct.getImprecio());
      }

      if (regla.isCriterioAgrupacionSentido()) {
        key.setSentido(dct.getCdsentido());
      }
      if (regla.isCriterioAgrupacionTipoOperacionBolsa()) {
        key.setTipoOperacionBolsa(dct.getTpopebol().trim());
      }
      if (regla.isCriterioAgrupacionTipoSubasta()) {
        key.setTipoSubasta("");
      }
      if (regla.isCriterioAgrupacionTitularFinal()) {
        if (dct.getTmct0desglosecamara().getTmct0alc().getReferenciaTitular() == null) {
          if (agrupar) {
            throw new SIBBACBusinessException("No se puede hacer el calculo final del canon sin el titular.");
          }
          titular = String.format("%s%s", dct.getTmct0desglosecamara().getTmct0alc().getId().getNucnfclt().trim(), dct
              .getTmct0desglosecamara().getTmct0alc().getId().getNucnfliq());
          key.setTitularFinal(titular);
        }
        else {
          key.setTitularFinal(dct.getTmct0desglosecamara().getTmct0alc().getReferenciaTitular().getCdreftitularpti()
              .trim());
        }
      }

      if (regla.isCriterioAgrupacionMiembroMercado()) {
        key.setMiembroMercado(dct.getCdmiembromkt().trim());
      }

      agrAlc = mapAgrByAlc.get(key);
      if (agrAlc == null) {
        agrAlc = new AgrupacionCanonByAlcDTO(regla, key);
        mapAgrByAlc.put(key, agrAlc);
        listAgrByAlc.add(agrAlc);
      }
      desglose = new CanonByDesgloseDTO();
      desglose.setNudesglose(dct.getNudesglose());
      desglose.setEfectivo(dct.getImefectivo().setScale(2, roundedMode));

      agrAlc.getListDesgloses().add(desglose);
      agrAlc.setSumatorioEfectivo(agrAlc.getSumatorioEfectivo().add(dct.getImefectivo()));

      if (regla.getTipoCanon() == TipoCanon.ESPECIAL) {
        desglose.setCanon(dct.getImCanonEspecial().setScale(2, roundedMode));
        agrAlc.setSumatorioCanon(agrAlc.getSumatorioCanon().add(dct.getImCanonEspecial()));
      }
      else {
        desglose.setCanon(dct.getImCanonBasico().setScale(2, roundedMode));
        agrAlc.setSumatorioCanon(agrAlc.getSumatorioCanon().add(dct.getImCanonBasico()));
      }

      LOG.trace("[agruparByRegla] {} agrupando desgloses: {}, key: {}...", alc.getId(), regla, key);
    }
    mapAgrByAlc.clear();

    LOG.trace("[agruparByRegla] {} fin agrupando desgloses: {}...", alc.getId(), regla);
  }

  private int calcularCanonByRegla(ReglasCanonesDTO regla, Map<Long, Tmct0desgloseclitit> mapDesglosesByNudesglose,
      List<AgrupacionCanonByAlcDTO> listAgrByAlc, boolean agrupar, String cdusuaud) throws SIBBACBusinessException {
    int agrupaciones = 0;

    BigDecimal canonAplicarRegla;
    BigDecimal canonRepartir;
    BigDecimal efectivoAgrupado;
    BigDecimal efectivoAgrupadoAlc;
    CriterioAgrupacionCanonDTO key;
    BigDecimal canonOtrosAlcs;
    CanonAgrupadoRegla canonAgrupadoRegla;
    RoundingMode roundedMode = RoundingMode.HALF_UP;

    if (listAgrByAlc.isEmpty()) {
      LOG.trace("[calcularCanonByRegla] regla no aplica {}", regla);
    }
    else {
      RepartoDatosEconomicosHelper reparto = new RepartoDatosEconomicosHelper();
      for (AgrupacionCanonByAlcDTO agr : listAgrByAlc) {
        key = agr.getAgrupacion();
        canonAplicarRegla = BigDecimal.ZERO;
        canonOtrosAlcs = BigDecimal.ZERO;
        efectivoAgrupado = agr.getSumatorioEfectivo();
        efectivoAgrupadoAlc = agr.getSumatorioEfectivo();

        LOG.trace("[calcularCanonByRegla] calculando canon efectivo: {}, key: {}, regla: {}", efectivoAgrupado, key,
            regla);

        if (agrupar) {
          canonAgrupadoRegla = canonAgrupadoReglaBo.findCanonCalculadoByAgrupacionAndRegla(key, regla);
          if (canonAgrupadoRegla != null) {
            LOG.trace("[calcularCanonByRegla] exite registro previo canon calculado: {}", canonAgrupadoRegla);
            canonOtrosAlcs = canonAgrupadoRegla.getImporteCanon();
            efectivoAgrupado = canonAgrupadoRegla.getImporteEfectivo().add(agr.getSumatorioEfectivo());
          }
        }

        // entra cuando no es una regla de tramos o cuando es una regla de
        // tramos
        // y el efectivo esta entre el tramo
        if (regla.getEfectivoDesde() != null && efectivoAgrupado.compareTo(regla.getEfectivoDesde()) < 0
            || regla.getEfectivoHasta() != null && efectivoAgrupado.compareTo(regla.getEfectivoHasta()) > 0) {
          LOG.trace("[calcularCanonByRegla] el efectivo {} no cumple los tramos {} - {}", efectivoAgrupado,
              regla.getEfectivoDesde(), regla.getEfectivoHasta());
          continue;
        }

        canonAplicarRegla = this.calcularCanonAplicarRegla(regla, efectivoAgrupado, roundedMode);

        LOG.trace("[calcularCanonByRegla] canon aplicar: {} min: {}, max: {}", canonAplicarRegla,
            regla.getImporteMinimoFijo(), regla.getImporteMaximoFijo());

        if (canonAplicarRegla.compareTo(BigDecimal.ZERO) != 0) {

          canonRepartir = canonAplicarRegla.subtract(canonOtrosAlcs);
          agr.setCanonRepartir(canonRepartir.setScale(2, roundedMode));
          reparto.repartirBigDecimals(agr.getListDesgloses(), canonRepartir.setScale(2, roundedMode),
              efectivoAgrupadoAlc, "efectivo", "canon", 2, roundedMode);
          for (CanonByDesgloseDTO canon : agr.getListDesgloses()) {
            if (regla.getTipoCanon() == TipoCanon.BASICO) {
              mapDesglosesByNudesglose.get(canon.getNudesglose()).setImCanonBasico(
                  mapDesglosesByNudesglose.get(canon.getNudesglose()).getImCanonBasico().add(canon.getCanon()));
              mapDesglosesByNudesglose.get(canon.getNudesglose()).setImcanoncontrdv(
                  mapDesglosesByNudesglose.get(canon.getNudesglose()).getImcanoncontrdv().add(canon.getCanon()));
              mapDesglosesByNudesglose.get(canon.getNudesglose()).setImcanoncontreu(
                  mapDesglosesByNudesglose.get(canon.getNudesglose()).getImcanoncontreu().add(canon.getCanon()));
            }
            else {
              mapDesglosesByNudesglose.get(canon.getNudesglose()).setImCanonEspecial(
                  mapDesglosesByNudesglose.get(canon.getNudesglose()).getImCanonEspecial().add(canon.getCanon()));
            }
          }

        }
        agrupaciones++;

      } // fin for agrupaciones
    }
    return agrupaciones;
  }

  private void bonificarCanonByRegla(ReglasCanonesDTO regla, Map<Long, Tmct0desgloseclitit> mapDesglosesByNudesglose,
      List<AgrupacionCanonByAlcDTO> listAgrByAlc, boolean agrupar, String cdusuaud) throws SIBBACBusinessException {

    BigDecimal canonAplicarRegla;
    BigDecimal canonRepartir;
    BigDecimal canonAgrupacion;
    BigDecimal canonAsignadoOtrosAlcs;
    BigDecimal canonAgrupacionTotal;
    BigDecimal efectivoAgrupacionTotal;
    BigDecimal efectivoAgrupadoDctsAlc;
    CriterioAgrupacionCanonDTO key;
    CanonCalculadoEfectivoDTO canonCalculadoAndEfectivo;
    RoundingMode roundedMode = RoundingMode.HALF_UP;

    if (listAgrByAlc.isEmpty()) {
      LOG.trace("[calcularCanonByRegla] regla no aplica {}", regla);
    }
    else {
      RepartoDatosEconomicosHelper reparto = new RepartoDatosEconomicosHelper();
      for (AgrupacionCanonByAlcDTO agr : listAgrByAlc) {
        // RECUPERAMOS DATOS DE ESTE ALC
        key = agr.getAgrupacion();
        canonRepartir = BigDecimal.ZERO;
        canonCalculadoAndEfectivo = null;
        canonAplicarRegla = BigDecimal.ZERO;
        canonAsignadoOtrosAlcs = BigDecimal.ZERO;
        canonAgrupacion = agr.getSumatorioCanon();
        canonAgrupacionTotal = agr.getSumatorioCanon();
        efectivoAgrupacionTotal = agr.getSumatorioEfectivo();
        efectivoAgrupadoDctsAlc = agr.getSumatorioEfectivo();

        LOG.trace("[calcularCanonByRegla] calculando canon efectivo: {}, key: {}, regla: {}", efectivoAgrupacionTotal,
            key, regla);

        if (agrupar) {
          // SI AGRUPAMOS RECUPERAMOS LOS DATOS DE TODOS LOS ALCS
          canonCalculadoAndEfectivo = canonAgrupadoReglaBo
              .sumCanonAndEfectivoByAllDataByAgrupacionAndReglaFromCalculos(key, regla);
          if (canonCalculadoAndEfectivo.getImcanon() != null && canonCalculadoAndEfectivo.getImefectivo() != null) {

            canonAsignadoOtrosAlcs = canonCalculadoAndEfectivo.getImcanon();
            canonAgrupacionTotal = canonAgrupacion.add(canonAsignadoOtrosAlcs);
            efectivoAgrupacionTotal = canonCalculadoAndEfectivo.getImefectivo().add(efectivoAgrupadoDctsAlc);
          }
        }

        // SI ENTRA POR EL CANON MINIMO Y MAXIMO EN LA BONIFICACION

        // entra cuando no es una regla de tramos o cuando es una regla de
        // tramos
        // y el efectivo esta entre el tramo
        if (regla.getEfectivoDesde() != null && efectivoAgrupacionTotal.compareTo(regla.getEfectivoDesde()) < 0
            || regla.getEfectivoHasta() != null && efectivoAgrupacionTotal.compareTo(regla.getEfectivoHasta()) > 0) {
          LOG.trace("[bonificarCanonByRegla] el efectivo {} no cumple los tramos {} - {}", efectivoAgrupacionTotal,
              regla.getEfectivoDesde(), regla.getEfectivoHasta());
          continue;
        }

        if (regla.getImporteCanonMinimoBonificacion() != null
            && canonAgrupacionTotal.compareTo(regla.getImporteCanonMinimoBonificacion()) < 0
            || regla.getImporteCanonMaximoBonificacion() != null
            && canonAgrupacionTotal.compareTo(regla.getImporteCanonMaximoBonificacion()) > 0) {
          LOG.trace("[bonificarCanonByRegla] el canon {} no cumple los tramos {} - {}", canonAgrupacionTotal,
              regla.getImporteCanonMinimoBonificacion(), regla.getImporteCanonMaximoBonificacion());
          continue;
        }

        canonAplicarRegla = this.calcularCanonAplicarRegla(regla, efectivoAgrupacionTotal, roundedMode);

        if (regla.getBonificacionComparator().equals("<")) {
          LOG.trace(
              "[bonificarCanonByRegla] bonificacion comparartor {}-MENOR  canon total: {} canon bonificacion: {}",
              regla.getBonificacionComparator(), canonAgrupacionTotal, canonAplicarRegla);
          if (canonAgrupacionTotal.compareTo(canonAplicarRegla) < 0) {
            LOG.trace("[bonificarCanonByRegla] el canon bonificado es mayor al total, no aplica bonificacion.");
            canonRepartir = BigDecimal.ZERO;
            continue;
          }
          else if (canonAgrupacionTotal.compareTo(canonAplicarRegla) > 0) {
            canonRepartir = canonAplicarRegla.subtract(canonAsignadoOtrosAlcs);
          }
        }
        else if (regla.getBonificacionComparator().equals(">")) {
          LOG.trace(
              "[bonificarCanonByRegla] bonificacion comparartor {}-MAYOR  canon total: {} canon bonificacion: {}",
              regla.getBonificacionComparator(), canonAgrupacionTotal, canonAplicarRegla);
          if (canonAgrupacionTotal.compareTo(canonAplicarRegla) < 0) {
            canonRepartir = canonAplicarRegla.subtract(canonAsignadoOtrosAlcs);
          }
          else if (canonAgrupacionTotal.compareTo(canonAplicarRegla) > 0) {
            LOG.trace("[bonificarCanonByRegla] el canon bonificado es menor al total, no aplica bonificacion.");
            canonRepartir = BigDecimal.ZERO;
            continue;
          }
        }

        agr.setCanonRepartir(canonRepartir.setScale(2, roundedMode));

        reparto.repartirBigDecimals(agr.getListDesgloses(), canonRepartir.setScale(2, roundedMode),
            efectivoAgrupadoDctsAlc, "efectivo", "canon", 2, roundedMode);

        for (CanonByDesgloseDTO canon : agr.getListDesgloses()) {
          mapDesglosesByNudesglose.get(canon.getNudesglose()).setImcanoncontreu(
              canon.getCanon().setScale(2, roundedMode));
          mapDesglosesByNudesglose.get(canon.getNudesglose()).setImcanoncontrdv(
              canon.getCanon().setScale(2, roundedMode));
        }

      } // fin for agrupaciones
    }
  }

  private BigDecimal calcularCanonAplicarRegla(ReglasCanonesDTO regla, BigDecimal efectivoAgrupado,
      RoundingMode roundedMode) {
    BigDecimal canonAplicarReglaPb;
    BigDecimal canonAplicarRegla = BigDecimal.ZERO;
    // añadimos el lineal al canon a aplicar
    if (regla.getImporteFijo() != null && regla.getImporteFijo().compareTo(BigDecimal.ZERO) != 0) {
      LOG.trace("[calcularCanonByRegla] calculando canon efectivo: {}, importe fijo: {}, regla: {}", efectivoAgrupado,
          regla.getImporteFijo(), regla);
      canonAplicarRegla = canonAplicarRegla.add(regla.getImporteFijo());
    }

    // añadimos los pb al canon a a plicar
    if (regla.getPuntoBasicoEfectivo() != null && regla.getPuntoBasicoEfectivo().compareTo(BigDecimal.ZERO) != 0) {
      canonAplicarReglaPb = efectivoAgrupado.multiply(regla.getPuntoBasicoEfectivo()).divide(PUNTO_BASICO, 2,
          roundedMode);
      LOG.trace("[calcularCanonByRegla] aplicando max/min canon efectivo: {}, pb: {}, calculo: {}, regla: {}",
          efectivoAgrupado, regla.getPuntoBasicoEfectivo(), canonAplicarReglaPb, regla);
      canonAplicarRegla = canonAplicarRegla.add(canonAplicarReglaPb);
      LOG.trace("[calcularCanonByRegla] canon efectivo: {}, canon aplicar: {}, regla: {}", efectivoAgrupado,
          regla.getPuntoBasicoEfectivo(), canonAplicarRegla, regla);
    }
    // calculamos minimo por pb
    if (regla.getPuntoBasicoMinimo() != null) {
      canonAplicarReglaPb = efectivoAgrupado.multiply(regla.getPuntoBasicoMinimo())
          .divide(PUNTO_BASICO, 2, roundedMode);
      if (canonAplicarRegla.compareTo(canonAplicarReglaPb) < 0) {
        canonAplicarRegla = canonAplicarReglaPb;
      }
    }

    // calculamos maximo por pb
    if (regla.getPuntoBasicoMaximo() != null) {
      canonAplicarReglaPb = efectivoAgrupado.multiply(regla.getPuntoBasicoMaximo())
          .divide(PUNTO_BASICO, 2, roundedMode);
      if (canonAplicarRegla.compareTo(canonAplicarReglaPb) > 0) {
        canonAplicarRegla = canonAplicarReglaPb;
      }
    }

    // calculamos minimo fijo
    if (regla.getImporteMinimoFijo() != null && canonAplicarRegla.compareTo(regla.getImporteMinimoFijo()) < 0) {
      canonAplicarRegla = regla.getImporteMinimoFijo();
    }
    // calculamos maximo fijo
    if (regla.getImporteMaximoFijo() != null && canonAplicarRegla.compareTo(regla.getImporteMaximoFijo()) > 0) {
      canonAplicarRegla = regla.getImporteMaximoFijo();
    }
    return canonAplicarRegla;
  }

  private List<AgrupacionCanonByAlcDTO> aplicarRegla(ReglasCanonesDTO regla, Tmct0alc alc,
      Map<Long, Tmct0desgloseclitit> mapDesglosesByNudesglose, boolean agrupar, String cdusuaud)
      throws SIBBACBusinessException {

    LOG.trace("[aplicarRegla] {} inicio aplicando regla: {}", alc.getId(), regla);
    List<AgrupacionCanonByAlcDTO> listAgrByAlc = new ArrayList<AgrupacionCanonByAlcDTO>();
    this.agruparByRegla(regla, alc, mapDesglosesByNudesglose.values(), listAgrByAlc, agrupar);
    if (regla.getTipoCalculo() == TipoCalculo.CALCULO) {
      this.calcularCanonByRegla(regla, mapDesglosesByNudesglose, listAgrByAlc, agrupar, cdusuaud);
    }
    else if (regla.getTipoCalculo() == TipoCalculo.BONIFICACION) {
      this.bonificarCanonByRegla(regla, mapDesglosesByNudesglose, listAgrByAlc, agrupar, cdusuaud);
    }

    LOG.trace("[aplicarRegla] {} fin aplicando regla: {}", alc.getId(), regla);
    return listAgrByAlc;
  }

  private List<ReglasCanonesDTO> findReglasAplicar(Tmct0bok bok, List<ReglasCanonesDTO> reglas)
      throws SIBBACBusinessException {
    LOG.trace("[findReglasAplicar] inicio");
    return this.findReglasAplicar(bok.getFetrade(), bok.getCdisin(), bok.getCdalias(), bok.getCdtpoper(), reglas);
  }

  private List<ReglasCanonesDTO> findReglasAplicar(Date fecha, String isin, String alias, char sentido,
      List<ReglasCanonesDTO> reglas) throws SIBBACBusinessException {
    LOG.trace("[findReglasAplicar] inicio fecha: {}, isin: {}, alias: {}, sentido: {}", fecha, isin, alias, sentido);
    List<ReglasCanonesDTO> reglasAplicar = new ArrayList<ReglasCanonesDTO>();
    BigDecimal fechaDesde;
    BigDecimal fechaHasta;
    BigDecimal fechaBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(fecha));

    for (ReglasCanonesDTO regla : reglas) {
      fechaDesde = null;
      fechaHasta = null;
      if (regla.getFechaDesde() != null) {
        fechaDesde = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(regla
            .getFechaDesde()));
      }
      if (regla.getFechaHasta() != null) {
        fechaHasta = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(regla
            .getFechaHasta()));
      }
      // fecha desde inclusive se usa la regla, fecha hasta no inclusive
      if ((fechaDesde != null && fechaDesde.compareTo(fechaBd) > 0)
          || (fechaHasta != null && fechaHasta.compareTo(fechaBd) <= 0)) {
        LOG.trace("[findReglasAplicar] fecha regla {} - {} no valida para fetrade: {}", regla.getFechaDesde(),
            regla.getFechaHasta(), fecha);
        continue;
      }

      if (regla.getListaAliasComparator() != null && regla.getListaAlias() != null) {
        if (regla.getListaAliasComparator() == SimpleComparatorCanonCriteria.IN
            && !regla.getListaAlias().contains(alias)
            || regla.getListaAliasComparator() == SimpleComparatorCanonCriteria.NOT_IN
            && regla.getListaAlias().contains(alias)) {
          LOG.trace("[findReglasAplicar] alias no valido: {} - {} - {}", alias, regla.getListaAliasComparator(),
              regla.getListaAlias());
          continue;
        }
      }

      if (regla.getListaIsinComparator() != null && regla.getListaIsin() != null) {
        if (regla.getListaIsinComparator() == SimpleComparatorCanonCriteria.IN && !regla.getListaIsin().contains(isin)
            || regla.getListaIsinComparator() == SimpleComparatorCanonCriteria.NOT_IN
            && regla.getListaIsin().contains(isin)) {
          LOG.trace("[findReglasAplicar] isin no valido: {} - {} - {}", isin, regla.getListaIsinComparator(),
              regla.getListaIsin());
          continue;
        }
      }

      if (regla.getSentido() != null && regla.getSentido() != sentido) {
        LOG.trace("[findReglasAplicar] sentido no valido: {} - {} ", sentido, regla.getSentido());
        continue;
      }

      reglasAplicar.add(regla);
    }
    LOG.trace("[findReglasAplicar] inicio fecha: {}, isin: {}, alias: {}, sentido: {}, reglas seleccionadas: {}",
        fecha, isin, alias, sentido, reglasAplicar.size());
    return reglasAplicar;
  }
}
