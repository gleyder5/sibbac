package sibbac.business.wrappers.database.bo.partenonReader;

import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component(value = "partenonEspecialesFileReader")
public class PartenonEspecialesFileReader extends PartenonFileReader {

  protected static final Logger LOG = LoggerFactory.getLogger(PartenonEspecialesFileReader.class);

  public PartenonEspecialesFileReader() {
    super();
  }

  public PartenonEspecialesFileReader(String fileName) {
    LOG.debug("[PartenonEspecialesFileReader :: Constructor] FileName: " + fileName);
    file = Paths.get(fileName);
  }

}
