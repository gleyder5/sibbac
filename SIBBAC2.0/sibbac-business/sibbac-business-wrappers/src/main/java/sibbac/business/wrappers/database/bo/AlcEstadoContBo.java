package sibbac.business.wrappers.database.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.AlcEstadoContDao;
import sibbac.business.wrappers.database.model.AlcEstadoCont;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.database.bo.AbstractBo;

@Service
public class AlcEstadoContBo extends AbstractBo<AlcEstadoCont, Long, AlcEstadoContDao> {
	@Autowired private AlcEstadoContDao alcEstadoCont;

	public boolean findByAlcAndTipoEstado(Tmct0alc alc, TIPO_ESTADO tipoestado, Tmct0estado estado) {
		AlcEstadoCont alcEstado = dao.findByAlcAndTipoEstado(alc, tipoestado);
		if (alcEstado!=null && alcEstado.getEstadoCont().equals(estado)) {
			return true;
		}
		return false;
	}

	public boolean findCountByNBookinAndNuordenAndNucnfcltAndNuCnfliqAndTipoestadoAndEstadocont(String nbooking, String nuorden, short nucnfliq, String nucnfclt, int tipoestado, int estadocont){
		Long existentes =  alcEstadoCont.findCountByNBookinAndNuordenAndNucnfcltAndNuCnfliqAndTipoestadoAndEstadocont(nbooking, nuorden, nucnfliq, nucnfclt, tipoestado, estadocont);
		if( existentes != null){
			if( existentes > 0){
				return true;
			}
		}
		return false;
	}
}
