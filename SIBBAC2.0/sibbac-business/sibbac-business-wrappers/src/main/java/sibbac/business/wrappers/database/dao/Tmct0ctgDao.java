package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0ctgId;

@Repository
public interface Tmct0ctgDao extends JpaRepository<Tmct0ctg, Tmct0ctgId> {

  @Query("select g from Tmct0ctg g where g.id.nuorden = :pnuorden ")
  List<Tmct0ctg> findAllByNuoden(@Param("pnuorden") String nuorden);

  @Query("select g from Tmct0ctg g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking")
  List<Tmct0ctg> findAllByNuodenAndNbooking(@Param("pnuorden") String nuorden, @Param("pnbooking") String nbooking);

  

  @Modifying
  @Query("delete from Tmct0ctg g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking")
  int deleteByNuodenAndNbooking(@Param("pnuorden") String nuorden, @Param("pnbooking") String nbooking);

}
