package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.wrappers.database.data.MercadoData;
import sibbac.business.wrappers.database.model.Tmct0Mercado;


public interface MercadoDao extends JpaRepository< Tmct0Mercado, Long > {

	@Query( "SELECT new sibbac.business.wrappers.database.data.MercadoData(o) FROM Tmct0Mercado o" )
	List< MercadoData > findAllData();

	List< Tmct0Mercado > findByIdIn( List< Long > valor );
}
