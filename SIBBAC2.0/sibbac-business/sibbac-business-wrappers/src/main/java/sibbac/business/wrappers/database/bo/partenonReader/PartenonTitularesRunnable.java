package sibbac.business.wrappers.database.bo.partenonReader;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.common.fileReader.ValidationHelper;
import sibbac.business.wrappers.database.bo.CodigoErrorBo;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.Tmct0AfiErrorBo;
import sibbac.business.wrappers.database.bo.Tmct0afiBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0paisesBo;
import sibbac.business.wrappers.database.bo.Tmct0provinciasBo;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.OrdTitularesDTO;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.RecordBean.TipoTitular;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Service(value = "PartenonTitularesRunnable")
public class PartenonTitularesRunnable extends IRunnableBean<OperacionEspecialRecordBean> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(PartenonFileReader.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0PAISES</code>. */
  @Autowired
  protected Tmct0paisesBo tmct0paisesBo;

  /** Objeto para el acceso a la tabla <code>TMCT0PROVINCIAS</code>. */
  @Autowired
  protected Tmct0provinciasBo tmct0provinciasBo;

  @Autowired
  protected Tmct0cfgBo tmct0cfgBo;

  @Autowired
  protected Tmct0ordBo ordBo;

  @Autowired
  protected CodigoErrorBo codBo;

  @Autowired
  protected Tmct0AfiErrorBo afiErrorBo;

  @Autowired
  protected Tmct0afiBo afiBo;

  @Autowired
  protected Tmct0aloBo aloBo;

  @Autowired
  protected Tmct0alcBo alcBo;

  @Autowired
  protected TitularesExternalValidator externalValidator;

  @Autowired
  protected Tmct0logBo logBo;

  @Autowired
  protected DesgloseRecordBeanBo desglosesPartenonRecordBeanBo;

  /**
   * Flag para saber de donde recuperar las ops
   * */
  protected boolean operacionEspecial = false;

  /** Lista de días fectivos. */
  protected List<Date> festivosList;

  /** Mapa de errores. */
  protected Map<String, CodigoErrorData> erroresMap;

  protected Map<String, Tmct0paises> isosPaises;
  protected Map<String, Tmct0paises> isosPaiseshacienda;
  protected Map<String, List<String>> nombresCiudadPorDistrito;

  boolean initPreRun = false;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  public PartenonTitularesRunnable() {
    super();
  }

  public void initDataPreRun(List<Date> festivosList, Map<String, CodigoErrorData> erroresMap,
      Map<String, Tmct0paises> isospaises, Map<String, Tmct0paises> isosPaisesHacienda,
      Map<String, List<String>> nombresCiudadPorDistrito, boolean isOperacionEspecial) {
    this.festivosList = festivosList;
    this.erroresMap = erroresMap;
    this.operacionEspecial = isOperacionEspecial;
    this.isosPaises = isospaises;
    this.isosPaiseshacienda = isosPaisesHacienda;
    this.nombresCiudadPorDistrito = nombresCiudadPorDistrito;

  }

  @Override
  @Transactional
  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {
    if (!initPreRun) {
      if (CollectionUtils.isEmpty(festivosList)) {
        festivosList = tmct0cfgBo.findHolidaysBolsa();
      }

      if (erroresMap == null || erroresMap.isEmpty()) {
        erroresMap = new HashMap<String, CodigoErrorData>();
        List<CodigoErrorData> listaErrores = codBo.findAllByGrupoOrderByCodigoDataAsc("Routing Titulares");
        if (listaErrores != null) {
          for (CodigoErrorData error : listaErrores) {
            erroresMap.put(error.getCodigo(), error);
          } // for
        } // if
      }
      initPreRun = true;
    }

  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = {
      SIBBACBusinessException.class, Exception.class })
  public void run(ObserverProcess observer, List<OperacionEspecialRecordBean> beans, int order)
      throws SIBBACBusinessException {
    int localOrden = order - beans.size();
    Map<String, List<OperacionEspecialRecordBean>> titularesAgrupados = new HashMap<String, List<OperacionEspecialRecordBean>>();
    for (OperacionEspecialRecordBean recordBean : beans) {

      this.processBean(titularesAgrupados, recordBean, localOrden++);
    }

    for (String key : titularesAgrupados.keySet()) {
      savePreviousBlock(titularesAgrupados.get(key));
    }

    titularesAgrupados.clear();
  }

  @Override
  @Transactional
  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see
   * sibbac.business.wrappers.common.fileReader.SibbacFileReader#processLine
   * (sibbac.business.wrappers.common.fileReader .RecordBean, int)
   */
  @Transactional
  public boolean processBean(Map<String, List<OperacionEspecialRecordBean>> titulares,
      OperacionEspecialRecordBean bean, int lineNumber) throws SIBBACBusinessException {
    LOG.trace("[PartenonFileReader :: processLine] Init");

    LOG.trace("[PartenonFileReader :: processLine] Linea : {} ## bean : {}", lineNumber, bean);
    PartenonRecordBean partenonRecordBean = (PartenonRecordBean) bean;
    List<OperacionEspecialRecordBean> bloqueMismaOrden = titulares.get(partenonRecordBean.getNumOrden());
    if (bloqueMismaOrden == null) {
      bloqueMismaOrden = new ArrayList<OperacionEspecialRecordBean>();
    }

    if (CollectionUtils.isEmpty(bloqueMismaOrden)) {
      // Primero de un bloque
      bloqueMismaOrden.add(bean);
    }
    else {
      PartenonRecordBean previousBean = (PartenonRecordBean) bloqueMismaOrden.get(bloqueMismaOrden.size() - 1);
      // Pertenece al mismo bloque
      if (!bean.equals(previousBean)) {
        // Si es un registro nuevo se incluye
        bloqueMismaOrden.add(bean);
      }
      else {
        // Si es un registro partido lo ampliamos
        previousBean.updateData(partenonRecordBean);
      }
    }
    titulares.put(partenonRecordBean.getNumOrden(), bloqueMismaOrden);
    LOG.trace("[PartenonFileReader :: processLine] Fin");
    return true;
  } // processLine

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Una vez se han leido varios titulares que corresponden a la misma orden en
   * este metodo se guardan en AFI_ERROR si tienen errores y en AFI si no los
   * tienen.
   */
  @Transactional
  protected void savePreviousBlock(List<OperacionEspecialRecordBean> bloqueMismaOrde) {
    String auditUser = "SIBBAC20";
    if (CollectionUtils.isNotEmpty(bloqueMismaOrde)) {
      List<PartenonRecordBean> bloqueMismaOrden = new ArrayList<PartenonRecordBean>();

      for (OperacionEspecialRecordBean operacionEspecialRecordBean : bloqueMismaOrde) {
        bloqueMismaOrden.add((PartenonRecordBean) operacionEspecialRecordBean);
      }
      Map<PartenonRecordBean, List<String>> errorsByBean = new HashMap<PartenonRecordBean, List<String>>();
      // Se decide si el bloque ira a AFI o a AFI_ERROR
      boolean hasErrors = false;
      boolean nacional = true;
      PartenonRecordBean partenonRecord = (PartenonRecordBean) bloqueMismaOrden.get(0);
      // Se comprueban los errores del ord
      String nureford = partenonRecord.getNumOrden();
      String erroresNumorden = null;

      Map<ControlTitularesDTO, List<ControlTitularesDTO>> alcsPorAllo = new HashMap<ControlTitularesDTO, List<ControlTitularesDTO>>();
      List<ControlTitularesDTO> alcs = null;
      List<ControlTitularesDTO> alos = new ArrayList<ControlTitularesDTO>();
      OrdTitularesDTO ord = null;
      String nucnfcltOpEspecial = null;
      boolean isOperacionEspecial = isOperacionEspecial();
      try {

        this.asignarParticipacionUsufructuarios(bloqueMismaOrden);

        LOG.trace("[PartenonFileReader :: savePreviousBlock] Comprobando si ord es nacional o extranjero: {}", nureford);

        alos = aloBo.findControlTitularesByRefCliente(nureford);
        if (CollectionUtils.isNotEmpty(alos)) {
          LOG.trace("[PartenonFileReader :: savePreviousBlock] Encontrados Allo's con ID : {}.", nucnfcltOpEspecial);
          ord = new OrdTitularesDTO(alos.get(0).getAlloId().getNuorden(), alos.get(0).getMercado());
          if (!ord.getCdclsmdo().equals('N')) {
            nacional = false;
          }
          else {
            nacional = true;
          }
        }
        else {
          if (isOperacionEspecial) {
            LOG.trace("[PartenonFileReader :: savePreviousBlock] Ex operativa especial.");
            nucnfcltOpEspecial = Tmct0alo.CTE_PARTENON + nureford;
            nacional = true;
            LOG.trace("[PartenonFileReader :: savePreviousBlock] Buscando Allo con ID : {}.", nucnfcltOpEspecial);

            alos = aloBo.findControlTitularesByNucnfclt(nucnfcltOpEspecial);
            if (CollectionUtils.isNotEmpty(alos)) {
              LOG.trace("[PartenonFileReader :: savePreviousBlock] Encontrados Allo's con ID : {}.", nucnfcltOpEspecial);
              ord = new OrdTitularesDTO(alos.get(0).getAlloId().getNuorden(), alos.get(0).getMercado());

            }
            else {
              LOG.warn("[PartenonFileReader :: savePreviousBlock] NO encontrado Allo con ID : {}.", nucnfcltOpEspecial);
            }
          }
          else {
            ord = ordBo.findFirstByNurefordOrderByNuordenNative(nureford);

            if (ord != null) {
              if (!ord.getCdclsmdo().equals('N')) {
                nacional = false;
                try {
                  LOG.trace("[PartenonFileReader :: savePreviousBlock] Comprobando alos para nureford extranjero: "
                      + nureford);
                  LOG.trace("[PartenonFileReader :: savePreviousBlock] aloBo es nulo? " + (aloBo == null ? "si" : "no"));
                  LOG.trace("[PartenonFileReader :: savePreviousBlock] Buscando alos de la orden: " + ord.getNuorden());
                  if (CollectionUtils.isEmpty(alos)) {
                    alos = aloBo.findControlTitularesByNuorden(ord.getNuorden());
                  }
                }
                catch (PersistenceException e) {
                  LOG.trace("[PartenonFileReader :: savePreviousBlock] Excepcion capturada: " + e.getMessage(), e);
                  throw e;
                }
                LOG.trace("[PartenonFileReader :: savePreviousBlock] Obtenidos " + alos.size()
                    + " alos para nureford: " + nureford);

                if (CollectionUtils.isEmpty(alos)) {
                  erroresNumorden = ValidationHelper.CodError.ALOS.text;
                  hasErrors = true;
                }
              }
              else {
                nacional = true;
                if (CollectionUtils.isEmpty(alos)) {
                  LOG.trace("[PartenonFileReader :: savePreviousBlock] Comprobando alos para nureford nacional: "
                      + nureford);
                  alos = aloBo.findControlTitularesByNuordenAndCdestadotitLt(ord.getNuorden(),
                      EstadosEnumerados.TITULARIDADES.TITULARIDAD_ACEPTADA.getId());
                  LOG.trace("[PartenonFileReader :: savePreviousBlock] Obtenidos " + alos.size()
                      + " alos para nureford: " + nureford);
                }
              }
            }
            else {
              // no hay ord
              erroresNumorden = ValidationHelper.CodError.ORD.text;
              hasErrors = true;
            }
          }
        }
        if (CollectionUtils.isEmpty(alos)) {
          if (ord != null) {
            erroresNumorden = ValidationHelper.CodError.ALOS.text;
            hasErrors = true;
          }
        }
        else {
          if (nacional) {
            for (ControlTitularesDTO tmct0alo : alos) {
              if (tmct0alo.getEstadoTitularidad() < EstadosEnumerados.TITULARIDADES.TITULARIDAD_ACEPTADA.getId()) {
                LOG.trace("[PartenonFileReader :: savePreviousBlock] buscando alcs para nureford nacional: " + nureford);
                alcs = alcBo.findControlTitularesByAlloId(tmct0alo.getAlloId());
                LOG.trace("[PartenonFileReader :: savePreviousBlock] encontrados {} alcs para nureford nacional: ",
                    alcs.size(), nureford);

                if (CollectionUtils.isEmpty(alcs)) {
                  erroresNumorden = ValidationHelper.CodError.NOALC.text;
                  hasErrors = true;
                }
                else {
                  alcsPorAllo.put(tmct0alo, alcs);
                }
              }
            }
          }
        }

      }
      catch (IncorrectResultSizeDataAccessException e) {
        erroresNumorden = ValidationHelper.CodError.ORD.text;
        hasErrors = true;
      }

      hasErrors = externalValidator.validateAll(erroresNumorden, bloqueMismaOrden, isosPaises, isosPaiseshacienda,
          nombresCiudadPorDistrito, errorsByBean);

      // borramos todos los afi errors
      if (nureford != null) {
        LOG.trace("[PartenonFileReader :: savePreviousBlock] borrando los afi erros nureford : {}", nureford);
        int afiErroDeleted = this.afiErrorBo.deleteByNureford(nureford);
        LOG.trace("[PartenonFileReader :: savePreviousBlock] borrados {} afi erros nureford : {}", afiErroDeleted,
            nureford);
      }

      Short cdbloque = 0;
      if (hasErrors && CollectionUtils.isEmpty(alos) && CollectionUtils.isEmpty(alcs)) {
        afiErrorBo.guardarBloqueExtranjeroConErroresOptimizado(new ArrayList<ControlTitularesDTO>(), errorsByBean,
            bloqueMismaOrden, erroresMap);
      }
      else {
        List<ControlTitularesDTO> myAlos;
        for (ControlTitularesDTO allo : alos) {
          if (nacional) {
            if (allo.getEstadoTitularidad() < EstadosEnumerados.TITULARIDADES.TITULARIDAD_ACEPTADA.getId()) {
              alcs = alcsPorAllo.get(allo);

              if (hasErrors) {
                if (CollectionUtils.isEmpty(alcs)) {
                  myAlos = new ArrayList<ControlTitularesDTO>();
                  myAlos.add(allo);
                  afiErrorBo.guardarBloqueNacionalConErrores(myAlos, errorsByBean, bloqueMismaOrden, erroresMap);
                }
                else {
                  afiErrorBo.guardarBloqueNacionalConErrores(alcs, errorsByBean, bloqueMismaOrden, erroresMap);
                }
              }
              else {
                if (CollectionUtils.isNotEmpty(alcs)) {
                  List<ControlTitularesDTO> alcsFor;
                  for (ControlTitularesDTO alc : alcs) {
                    if (alc.getEstadoTitularidad() < EstadosEnumerados.TITULARIDADES.TITULARIDAD_ACEPTADA.getId()) {
                      alcsFor = new ArrayList<ControlTitularesDTO>();
                      alcsFor.add(alc);
                      cdbloque = afiBo.updatePreviousAfisViaAlc(alc);
                      cdbloque++;
                      afiBo.guardarBloqueNacionalSinErrores(alcsFor, cdbloque, bloqueMismaOrden, festivosList,
                          isOperacionEspecial(), "PARTENON");
                    }
                    else {
                      LOG.warn("[PartenonFileReader :: savePreviousBlock] alc titulares ya aceptados {}-{}",
                          allo.getAlloId(), alc.getNucnfliq());
                    }
                  }
                }
                else {
                  LOG.warn("[PartenonFileReader :: savePreviousBlock] no tiene alcs para cargar titulares {}",
                      allo.getAlloId());
                }
              }
            }
            else {
              LOG.warn("[PartenonFileReader :: savePreviousBlock] allo titulares ya aceptados {}", allo.getAlloId());
            }
          }
          else {
            myAlos = new ArrayList<ControlTitularesDTO>();
            myAlos.add(allo);
            if (hasErrors) {
              afiErrorBo
                  .guardarBloqueExtranjeroConErroresOptimizado(myAlos, errorsByBean, bloqueMismaOrden, erroresMap);
            }
            else {
              cdbloque = afiBo.obtenerCdbloqueActivoOptimizadaByAlloId(allo.getAlloId(), nureford, auditUser);
              cdbloque++;
              afiErrorBo
                  .guardarBloqueExtranjeroConErroresOptimizado(myAlos, errorsByBean, bloqueMismaOrden, erroresMap);

            }
          }

        }
      }
    }
    LOG.trace("[PartenonFileReader :: savePreviousBlock] Fin");
  }// savePreviousBlock

  private void asignarParticipacionUsufructuarios(List<PartenonRecordBean> bloqueMismaOrden) {
    LOG.trace("[PartenonFileReader :: asignarParticipacionUsufructuarios] Inicio");

    Collection<PartenonRecordBean> usufructuarios = new HashSet<PartenonRecordBean>();
    if (CollectionUtils.isNotEmpty(bloqueMismaOrden)) {
      for (PartenonRecordBean partenonRecordBean : bloqueMismaOrden) {
        if (partenonRecordBean.getTiptitu() == TipoTitular.USUFRUCTUARIO.getTipoTitular()) {
          LOG.trace("[PartenonFileReader :: asignarParticipacionUsufructuarios] Encontrado usufructuario : {}",
              partenonRecordBean);
          usufructuarios.add(partenonRecordBean);
        }
      }
      int countUsufructuarios = usufructuarios.size();
      if (countUsufructuarios > 0) {
        BigDecimal participacion = new BigDecimal("100");
        if (countUsufructuarios > 1) {
          participacion = participacion.divide(new BigDecimal(countUsufructuarios), 2, RoundingMode.DOWN);
        }
        LOG.trace(
            "[PartenonFileReader :: asignarParticipacionUsufructuarios] Hay {} usufructuario y tocan a {}% de usufructo cada uno.",
            countUsufructuarios, participacion);
        for (PartenonRecordBean partenonRecordBean : usufructuarios) {
          partenonRecordBean.setParticip(participacion);
        }
      }
      usufructuarios.clear();
    }
    LOG.trace("[PartenonFileReader :: asignarParticipacionUsufructuarios] final");
  }

  public boolean isOperacionEspecial() {
    return operacionEspecial;
  }

  public void setOperacionEspecial(boolean operacionEspecial) {
    this.operacionEspecial = operacionEspecial;
  }

  @Override
  public IRunnableBean<OperacionEspecialRecordBean> clone() {
    // PartenonTitularesRunnable pt = new PartenonTitularesRunnable();
    //
    //
    // pt.tmct0paisesBo = this.tmct0paisesBo;
    // pt.afiBo = this.afiBo;
    // pt.afiErrorBo = this.afiErrorBo;
    // pt.alcBo = this.alcBo;
    // pt.aloBo = this.aloBo;
    // pt.cdHaciendaList = this.cdHaciendaList;
    // pt.cdIsonumList = this.cdIsonumList;
    // pt.codBo = this.codBo;
    // pt.desglosesPartenonRecordBeanBo = this.desglosesPartenonRecordBeanBo;
    // pt.erroresMap = this.erroresMap;
    // pt.externalValidator = this.externalValidator;
    // pt.festivosList = this.festivosList;
    // pt.logBo = this.logBo;
    // pt.operacionEspecial = this.operacionEspecial;
    // pt.ordBo = this.ordBo;
    // pt.provinciasList = this.provinciasList;
    // return pt;
    return this;
  }

}