package sibbac.business.wrappers.database.dto;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.model.Tmct0Regla;

public class GestionReglasDTO {
	
	private BigInteger		id;
	private String 		cd_codigo;  	
	private String 		nb_descripcion;
	private Date 		fecha_creacion;
	private Date 		audit_fecha;
	private String		audit_usuario;
	private int 		nParametrizacionesActivas;
	private int         nAliasAsociados;
	private int			nSubcuentasAsociadas;
	private Tmct0Regla  tmct0Regla;
	private String      activo;
	private List<SelectDTO> alias;
	
	
	public GestionReglasDTO(){}
	
	public GestionReglasDTO(Tmct0Regla tmct0Regla){
		id = tmct0Regla.getIdRegla();
		cd_codigo = tmct0Regla.getCdCodigo();
		nb_descripcion = tmct0Regla.getNbDescripcion();
		
	}
		

	public GestionReglasDTO(Tmct0Regla tmct0regla, int nParametrizacionesActivas){
		id =tmct0regla.getIdRegla();
		cd_codigo = tmct0regla.getCdCodigo();
		nb_descripcion = tmct0regla.getNbDescripcion();
		fecha_creacion = tmct0regla.getFechaCreacion();
		audit_fecha	   = tmct0regla.getAuditFechaCambio();
		audit_usuario  = tmct0regla.getAuditUser();
		this.nParametrizacionesActivas = nParametrizacionesActivas;
//		this.nAliasAsociados = nAliasAsociados; 
//		this.nSubcuentasAsociadas = nSubcuentasAsociadas;
	}

	public GestionReglasDTO(BigInteger id, String cd_codigo, String nb_descripcion,
	Date fecha_creacion) {
		super();
		this.id = id;
		this.cd_codigo = cd_codigo;
		this.nb_descripcion = nb_descripcion;
		this.fecha_creacion = fecha_creacion;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getCd_codigo() {
		return cd_codigo;
	}

	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}

	public String getNb_descripcion() {
		return nb_descripcion;
	}

	public void setNb_descripcion(String nb_descripcion) {
		this.nb_descripcion = nb_descripcion;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public Date getAudit_fecha() {
		return audit_fecha;
	}

	public void setAudit_fecha(Date audit_fecha) {
		this.audit_fecha = audit_fecha;
	}

	public String getAudit_usuario() {
		return audit_usuario;
	}

	public void setAudit_usuario(String audit_usuario) {
		this.audit_usuario = audit_usuario;
	}

	public int getnParametrizacionesActivas() {
		return nParametrizacionesActivas;
	}

	public void setnParametrizacionesActivas(int nParametrizacionesActivas) {
		this.nParametrizacionesActivas = nParametrizacionesActivas;
	}

	public int getnAliasAsociados() {
		return nAliasAsociados;
	}

	public void setnAliasAsociados(int nAliasAsociados) {
		this.nAliasAsociados = nAliasAsociados;
	}

	public int getnSubcuentasAsociadas() {
		return nSubcuentasAsociadas;
	}

	public void setnSubcuentasAsociadas(int nSubcuentasAsociadas) {
		this.nSubcuentasAsociadas = nSubcuentasAsociadas;
	}

	public Tmct0Regla getTmct0Regla() {
		return tmct0Regla;
	}

	public void setTmct0Regla(Tmct0Regla tmct0Regla) {
		this.tmct0Regla = tmct0Regla;
	}
	
	
	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public List<SelectDTO> getAlias() {
		return alias;
	}

	public void setAlias(List<SelectDTO> alias) {
		this.alias = alias;
	}	
	
}
