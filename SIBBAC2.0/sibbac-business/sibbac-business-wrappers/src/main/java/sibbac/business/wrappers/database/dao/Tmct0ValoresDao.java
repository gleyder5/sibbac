package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.wrappers.database.model.Tmct0GruposValores;
import sibbac.business.wrappers.database.model.Tmct0Valores;

@Repository
public interface Tmct0ValoresDao extends JpaRepository<Tmct0Valores, Long> {

  public List<Tmct0Valores> findByCodigoDeValorOrderByFechaEmisionDesc(String codigoDeValor);

  @Query("SELECT g FROM Tmct0Valores v, Tmct0GruposValores g WHERE v.tmct0GruposValores.id = g.id "
      + " AND v.codigoDeValor = :codigoValor ORDER BY V.codigoDeValor")
  List<Tmct0GruposValores> findGrupoValoresByCodigoDeValorOrderByFechaEmisionDesc(
      @Param("codigoValor") String codigoDeValor);

  @Query("SELECT V.codigoDeValor||'|'||V.descripcion FROM Tmct0Valores V GROUP BY V.codigoDeValor, V.descripcion  ORDER BY V.codigoDeValor  ")
  List<String> findIsin();

  @Query(value = "SELECT T.CD_CODIGO FROM TMCT0_VALORES V INNER JOIN TMCT0_TIPOS_PRODUCTO T ON V.ID_TIPO_PRODUCTO = T.ID "
      + " WHERE V.CODIGO_DE_VALOR = :codigoValor", nativeQuery = true)
  List<String> findTiposProductoByCodigoDeValorOrderByFechaEmisionDesc(@Param("codigoValor") String codigoDeValor);

  @Query("SELECT new sibbac.business.fase0.database.dto.LabelValueObject("
      + "CONCAT(c.codigoDeValor,'-(',c.descripcion,')'), c.codigoDeValor) " + "FROM Tmct0Valores c")
  List<LabelValueObject> labelValueList();

  @Query("SELECT v FROM Tmct0Valores v WHERE v.fechaUltimoDiaNegociacion != '' and v.activo = 'S' ")
  List<Tmct0Valores> findAllActivosConFechaUltimoDiaNegociacion();

}
