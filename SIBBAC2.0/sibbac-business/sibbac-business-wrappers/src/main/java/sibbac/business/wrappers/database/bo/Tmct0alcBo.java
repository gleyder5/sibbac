package sibbac.business.wrappers.database.bo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.bo.CanonSolicitaRecalculoBo;
import sibbac.business.canones.dao.CanonAgrupadoReglaDao;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.dao.Tmct0fisDaoSoloEstaticos;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dao.Tmct0aloDao;
import sibbac.business.wrappers.database.dao.Tmct0bokDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.dao.specifications.Tmct0AlcSpecifications;
import sibbac.business.wrappers.database.data.InformeOpercacionData;
import sibbac.business.wrappers.database.data.Tmct0AlcData;
import sibbac.business.wrappers.database.desglose.bo.CuentaValoresValidateHelper;
import sibbac.business.wrappers.database.dto.AlcEntregaRecepcionClienteDTO;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.EnvioCorretajeTmct0alcDataDTO;
import sibbac.business.wrappers.database.dto.PayLetterDTO;
import sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista17DTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean.TipoPersona;
import sibbac.business.wrappers.util.InformeOperacionesExcelBuilder;
import sibbac.common.ExportType;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TipoRouting;
import sibbac.common.TiposSibbac.TiposComision;
import sibbac.common.TiposSibbac.TiposEnvioComisionPti;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.JobPK;

@Service
public class Tmct0alcBo extends AbstractBo<Tmct0alc, Tmct0alcId, Tmct0alcDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0alcBo.class);

  private static final String TAG = Tmct0alcBo.class.getName();

  private static final String INFORME_OPERACIONES_NAME = "Informe Operaciones";

  @Value("${daemons.confirmacionminotista.clearing.members}")
  private String compensadores1617;
  @Value("${daemons.confirmacionminotista.clearing.member.referencies}")
  private String referencias1617;
  @Value("${daemons.confirmacionminotista.like.cdrefban}")
  private String cdrefbanStart1617;
  @Value("${daemons.confirmacionminotista.not.aliass}")
  private String notInAliass1617;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0estadoDao estadoDao;

  @Autowired
  private Tmct0ejeBo ejeBo;
  @Autowired
  private Tmct0ejecucionalocationBo ejealoBo;
  @Autowired
  private Tmct0afiBo afiBo;
  @Autowired
  private Tmct0alcDaoImp daoImpl;
  @Autowired
  private Tmct0referenciaTitularBo referenciaTitularBo;

  @Autowired
  private InformeOperacionesExcelBuilder excelBuilder;
  @Autowired
  private TextosIdiomasBo textoIdiomaBo;
  @Autowired
  private GestorServicios gestorServicios;
  @Autowired
  private SendMail mailUtil;
  @Autowired
  private EtiquetasDao etiquetasDao;
  @Autowired
  private Tmct0movimientoeccBo movimientoEccBo;

  /** Business object para la tabla <code>Tmct0bok</code>. */
  @Autowired
  private Tmct0bokDao tmct0bokDao;

  /** Business object para la tabla <code>Tmct0alo</code>. */
  @Autowired
  private Tmct0aloDao tmct0aloDao;

  /** Business object para la tabla <code>Tmct0log</code>. */
  @Autowired
  private Tmct0logBo tmct0logBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movimientoOpNuevaBo;

  @Autowired
  private Tmct0desgloseclititBo desgloseClititBo;

  @Autowired
  private Tmct0enviotitularidadDao envioTitularidadDao;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao desgloseCamaraEnvioRtDao;

  @Autowired
  private Tmct0infocompensacionDao infoCompensacionDao;

  @Autowired
  Tmct0alcDaoImp tmct0alcDaoImp;

  @Autowired
  private CanonAgrupadoReglaDao canonAgrupadoReglaDao;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private CanonSolicitaRecalculoBo canonSolicitaRecalculoBo;

  @Autowired
  private Tmct0fisDaoSoloEstaticos fisDao;

  public Short getMaxNucnfliqByTmct0aloId(Tmct0aloId aloId) {
    return dao.getMaxNucnfliqByTmct0aloId(aloId);
  }

  public BigDecimal getSumNutitliqByTmct0aloId(Tmct0aloId aloId) {
    return dao.getSumNutitliqByTmct0aloId(aloId);
  }

  @Transactional
  public List<Tmct0alc> findOrdenesParaContabilizar(int year, String cdestliq, List<Integer> lEstadosAsig,
      Tmct0estado estadocont) {
    return dao.findOrdenesParaContabilizar(year, cdestliq, lEstadosAsig, estadocont);
  }

  public List<Tmct0alc> findByEstadosAsig(List<Integer> lEstadosAsig) {
    return dao.findByEstadosAsig(lEstadosAsig);
  }

  public List<Tmct0alcId> findAllTmct0alcIdByTmct0aloIdAndDeAlta(Tmct0aloId aloId) {
    return dao.findAllTmct0alcIdByTmct0aloIdAndDeAlta(aloId);
  }

  public List<Tmct0alcId> findAllTmct0alcIdByListTmct0aloIdAndDeAlta(List<Tmct0aloId> listAloId) {
    List<Tmct0alcId> listAlcs = new ArrayList<Tmct0alcId>();
    if (listAloId.size() > 500) {
      int pos = 0;
      int posEnd = 0;
      while (pos < listAloId.size()) {
        posEnd = Math.min(posEnd + 500, listAloId.size());
        listAlcs.addAll(dao.findAllTmct0alcIdByListTmct0aloIdAndDeAlta(listAloId.subList(pos, posEnd)));
        pos = posEnd;
      }

    }
    else {
      listAlcs = dao.findAllTmct0alcIdByListTmct0aloIdAndDeAlta(listAloId);

    }
    return listAlcs;
  }

  public void updateContabilizadoByTipoMovimiento(String tipoMovimiento, boolean contabilizado) {
    dao.updateContabilizadoByTipoMovimiento(tipoMovimiento, contabilizado);
  }

  public Tmct0alc findByNucnfliqAndLikeNucnfclt(Short nucnfliq, String nucnfclt) {
    return dao.findByNucnfliqAndLikeNucnfclt(nucnfliq, nucnfclt);
  }

  public Tmct0alc findByNbookingAndNucnfliqAndNucnfclt(String nbooking, Short nucnfliq, String nucnfclt) {
    return dao.findByNbookingAndNucnfliqAndNucnfclt(nbooking, nucnfliq, nucnfclt);
  }

  public List<Tmct0alc> findByAlloId(Tmct0aloId alloId) {
    return dao.findByAlloId(alloId);
  }

  public List<Tmct0alc> findByAlloIdActivos(Tmct0aloId alloId) {
    return dao.findByAlloIdActivos(alloId);
  }

  public List<ControlTitularesDTO> findControlTitularesByNuordenAndCdestadotitLt(String nuorden, Integer cdestadotitLt) {
    return dao.findControlTitularesByNuordenAndCdestadotitLt(nuorden, cdestadotitLt);
  }

  public List<ControlTitularesDTO> findControlTitularesByAlloId(Tmct0aloId alloId) {
    return dao.findControlTitularesByAlloId(alloId.getNuorden(), alloId.getNbooking(), alloId.getNucnfclt());
  }

  public ControlTitularesDTO findControlTitularesByAlcId(Tmct0alcId allcId) {
    return dao.findControlTitularesByAlcId(allcId);
  }

  public List<Tmct0alc> findAllByTmct0aloIdAndCdestadoasig(Tmct0aloId alloId, int cdestadoasig) {
    return dao.findAllByNuordenAndNbookingAndNucnfcltAndCdestadoasig(alloId.getNuorden(), alloId.getNbooking(),
        alloId.getNucnfclt(), cdestadoasig);
  }

  public Tmct0alc findByTmct0desglosecamaras(Tmct0desglosecamara desglosecamara) {
    return dao.findByTmct0desglosecamaras(desglosecamara);
  }

  /**
   * Recupera los ALCS ordenados por Alias.
   * 
   * @return
   */
  public List<Tmct0alc> findTmct0alcByAlias(final Set<String> codigosAlias) {
    return dao.findAlcsByEstadoContable(EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId(),
        codigosAlias);
  }

  /**
   * 
   * @param estado
   * @param codigosAlias
   * @param fechaContratacionHasta
   * @return
   */
  public List<Tmct0alc> findAlcsByEstadoContableToDate(int estado, final Set<String> codigosAlias,
      Date fechaContratacionHasta, Date fechaHasta) {
    return dao.findAlcsByEstadoContableToDate(estado, codigosAlias, fechaContratacionHasta, fechaHasta);
  }

  /**
   * 
   * @param estado
   * @param codigosAlias
   * @return
   */
  public List<Tmct0alc> findAlcsByEstadoContableAndFeejeliq(int estado, final Set<String> codigosAlias, Date fechaHasta) {
    return dao.findAlcsByEstadoContableAndFeejeliq(estado, codigosAlias, fechaHasta);
  }

  public List<Tmct0alc> findAllGroupByCdEstadoentrecAndNoTratadoCliente(List<Integer> estados) {
    return dao.findAllGroupByCdEstadoentrecAndNoTratadoCliente(estados);
  }

  public List<Object[]> findInformeContable(Date fechTradeIni, Date fechTradeFin, Date settlementIni,
      Date settlementFin, List<String> alias, String tipologia, List<String> estadocont,
      List<EstadosEnumerados.ASIGNACIONES> lEstadoAsig, List<EstadosEnumerados.CLEARING> estadoentrec,
      List<Integer> cuenta, String tipoOrden) {
    return dao.findInformeContable(fechTradeIni, fechTradeIni == null ? "N" : "S", fechTradeFin,
        fechTradeFin == null ? "N" : "S", settlementIni, settlementIni == null ? "N" : "S", settlementFin,
        settlementFin == null ? "N" : "S", alias, alias.isEmpty() ? "N" : "S");
  }

  @Transactional
  public List<AlcEntregaRecepcionClienteDTO> findByEntregaRecepcionTodos(Date fcontratacionDe, Date fcontratacionA,
      Date fliquidacionDe, Date fliquidacionA, String estado, String nbooking, String cdniftit, String nbtitliq,
      final String biccle, final BigDecimal importeNetoDesde, final BigDecimal importeNetoHasta,
      final BigDecimal titulosDesde, final BigDecimal titulosHasta, final int estadoAsig, String isin, String alias,
      String referencias3, String cif_svb, String sentidoBusqueda, Integer tipo, String estadoInstruccion, String cuenta)
      throws SIBBACBusinessException {

    List<AlcEntregaRecepcionClienteDTO> lista_alc = daoImpl.findByEntregaRecepcionTodos(fcontratacionDe,
        fcontratacionA, fliquidacionDe, fliquidacionA, estado, nbooking, cdniftit, nbtitliq, biccle, importeNetoDesde,
        importeNetoHasta, titulosDesde, titulosHasta, estadoAsig, isin, alias, referencias3, cif_svb, sentidoBusqueda,
        tipo, estadoInstruccion, cuenta);

    return lista_alc;
  }

  @Transactional
  public List<Tmct0alc> findByNetting(Netting netting) {

    List<Tmct0alc> lista_alc = dao.findByNetting(netting);
    for (Tmct0alc datos_alc : lista_alc) {
      datos_alc.getEstadoentrec().getNombre();
    }
    return lista_alc;
  }

  @Transactional
  public Date getMaxFechaValorS3LiqByAlc(Tmct0alc tmct0alc) {

    Date fechaLiquidacion = dao.getMaxFechaValorS3LiqByAlc(tmct0alc);

    return fechaLiquidacion;
  }

  public List<Tmct0alc> findByEstadoGreaterOrEqualThan(Integer estado, Date fechaMercado) {
    return this.dao.findByEstadoGreaterOrEqualThan(estado, fechaMercado);
  }

  public List<Tmct0alc> findByCdEstadoentrecAndNoTratadoClienteAndEstadoCuentaVirtual(List<Integer> estados) {
    return dao.findAllByCdEstadoentrecAndNoTratadoClienteAndEstadoEnviadoCuentaVirtual(estados);
  }

  public List<Tmct0alc> findAlcsCliente(Date date) {
    return this.dao.findAlcsCliente(date);
  }

  public List<Tmct0alc> findAlcsByIdViaSpecifications(final String nuorden, final String nbooking,
      final Short nucnfliq, final String nucnfclt) {
    final List<Tmct0alc> alcs = dao.findAll(Tmct0AlcSpecifications.listadoFriltrado(nuorden, nbooking, nucnfliq,
        nucnfclt));
    return alcs;
  }

  public void setEstadocont(Tmct0alc alc, Tmct0estado estadoCont) {
    Tmct0estado estadoEnCurso = estadoDao.findOne(CONTABILIDAD.EN_CURSO.getId());
    Tmct0alo alo = alc.getTmct0alo();
    alo.setTmct0estadoByCdestadocont(estadoEnCurso);
    alo.getTmct0bok().setTmct0estadoByCdestadocont(estadoEnCurso);
    alc.setEstadocont(estadoCont);
    for (Tmct0desglosecamara desglose : alc.getTmct0desglosecamaras()) {
      desglose.setTmct0estadoByCdestadocont(estadoCont);
    }
    dao.save(alc);
  }

  /**
   * Estable el estado de asignación del alc y sus desglose cámara al
   * especificado y el de su alo y su booking a en curso.
   * 
   * @param alc ALC.
   * @param estadoAsig Estado de asignación a asignar al alc.
   */
  @Deprecated //Se añade deprecated a fecha 05.11.2018 porque nadie usa este método. Borrar si sigue sin ser necesario
  public void setEstadoasig(Tmct0alc alc, Tmct0estado estadoAsig) {
    Tmct0estado estadoEnCurso = estadoDao.findOne(ASIGNACIONES.EN_CURSO.getId());
    Date ahora = Calendar.getInstance().getTime();
    

    Tmct0alo alo = alc.getTmct0alo();
    alo.setTmct0estadoByCdestadoasig(estadoEnCurso);
    alo.setFhaudit(ahora);

    alo.getTmct0bok().setTmct0estadoByCdestadoasig(estadoEnCurso);
    alo.getTmct0bok().setFhaudit(ahora);

    alc.setCdestadoasig(estadoAsig.getIdestado());
    alc.setFhaudit(ahora);

    for (Tmct0desglosecamara desglose : alc.getTmct0desglosecamaras()) {
      desglose.setTmct0estadoByCdestadoasig(estadoAsig);
      desglose.setAuditFechaCambio(ahora);
    }
    dao.save(alc);
  } // setEstadoasig

  /* CORREO */
  public List<InformeOpercacionData> getInformeOperacionData(String cdalias) {
    return daoImpl.getInformeOperacionData(cdalias);
  }

  /**
   * 
   * @param jobPk
   * @param alias
   * @param duplicado
   * @param taskMessages
   */
  @Transactional
  public boolean enviarCorreo(final JobPK jobPk, List<InformeOpercacionData> lineas, final Alias alias,
      boolean duplicado, final List<String> taskMessages) {
    boolean result = false;
    String prefix = "[Tmct0AlcBo::enviarCorreo] ";
    LOG.debug(prefix + "[JobPK=={}]", jobPk);
    final List<Contacto> contactos = gestorServicios.getContactos(alias, jobPk);
    if (contactos == null || contactos.isEmpty()) {
      LOG.error(prefix + "ERROR: No se puede enviar el correo a este alias [{}] porque carece de contactos!",
          alias.getCdaliass());
    }
    else {
      // InputStream con datos de entrada
      final List<InputStream> inputstream = new ArrayList<InputStream>();
      // Nombre del fichero destino (<Nombre>.<extension>)
      final List<String> nameDest = new ArrayList<String>();
      Date now = new Date();

      // Generamos el excel
      int excels = new BigDecimal(lineas.size()).divide(new BigDecimal(65535), RoundingMode.UP).intValue();
      for (int x = 0; x < excels; x++) {
        if (lineas.size() > 65535) {
          List<InformeOpercacionData> subLineas = new LinkedList<InformeOpercacionData>();
          for (int i = 0; i < 65535; i++) {
            subLineas.add(lineas.get(i));
          }
          final InputStream inputExcel = crearExcel(subLineas, alias.getCodigoIdioma());
          inputstream.add(inputExcel);
          lineas.removeAll(subLineas);
        }
        else {
          final InputStream inputExcel = crearExcel(lineas, alias.getCodigoIdioma());
          inputstream.add(inputExcel);
        }
        String nombreFichero = this.generateFileNameForAttachment(ExportType.EXCEL_INFORME_OPERACIONES,
            INFORME_OPERACIONES_NAME + "_" + (x + 1), now);
        LOG.debug(prefix + " nombre Excel a enviar: " + nombreFichero);
        nameDest.add(nombreFichero);
      }

      final List<String> destinatarios = new ArrayList<String>();

      for (Contacto destinatario : contactos) {
        String[] mail = destinatario.getEmail().split(";");
        for (int i = 0; i < mail.length; i++) {
          destinatarios.add(mail[i].trim());
        }
      }

      // Montamos el mapping con los datos que se van a sustituir en el
      // cuerpo
      // del Mail
      Map<String, String> mapping = createMappingCuerpoMail(alias);

      // Recuperamos el body de la base de datos
      String body = textoIdiomaBo.recuperarTextoIdioma("INF_OPERACIONES_MAIL_BODY", alias.getIdioma());
      body = reemplazarKeys(body, mapping);

      String asunto = textoIdiomaBo.recuperarTextoIdioma("INF_OPERACIONES_MAIL_SUBJECT", alias.getIdioma());
      asunto = reemplazarKeys(asunto, mapping);
      try {
        mailUtil.sendMail(destinatarios, asunto, body, inputstream, nameDest, null);
        result = true;
      }
      catch (MailSendException | IllegalArgumentException | MessagingException | IOException e) {
        LOG.error("Error enviando el correo de facturas", e.getCause());
      }
    }
    return result;
  }

  public InputStream crearExcel(final List<InformeOpercacionData> lineas, String codIdioma) {
    LOG.debug(TAG + "::crearExcel Comienza a generar el Excel lineas: " + lineas.size() + " cod Idioma: " + codIdioma);
    final Map<String, String> lineasCabeceras = textoIdiomaBo.recuperarCabecerasExcelInformeOperaciones(codIdioma);
    LOG.debug(TAG + "::crearExcel cabeceras encontradas: " + lineasCabeceras.size());

    InputStream inputExcel = null;
    try {
      final ByteArrayOutputStream out = excelBuilder.generaInformeOperaciones("InformeOperaciones", lineas,
          lineasCabeceras);
      inputExcel = new ByteArrayInputStream(out.toByteArray());
      LOG.debug(TAG + "::crearExcel ***** CREADO CORRECTAMENTE ******");
    }
    catch (Exception e) {
      LOG.error(TAG + " Error generando un Excel: " + e);
    }

    return inputExcel;
  }

  private String generateFileNameForAttachment(ExportType e, final String name, final Date d) {
    StringBuffer s = new StringBuffer();

    s.append(e.getFileName());
    s.append("_");
    s.append("[" + name + "]");
    s.append("_");
    s.append("[" + FormatDataUtils.convertDateToFileName(d) + "]");
    s.append(".");
    if (e.equals(ExportType.EXCEL_INFORME_OPERACIONES)) {
      s.append("xls");
    }
    else {
      s.append("pdf");
    }

    return s.toString();
  }

  public String reemplazarKeys(String body, Map<String, String> mapping) {
    final Set<String> keys = mapping.keySet();
    for (final String key : keys) {
      body = body.replace(key, mapping.get(key));
    }
    return body;
  }

  public Map<String, String> createMappingCuerpoMail(final Alias alias) {
    String key = null;
    String value = null;
    Map<String, String> mapping = new HashMap<String, String>();
    final List<Etiquetas> etiquetas = (List<Etiquetas>) etiquetasDao.findAllByTabla("Informe Operaciones");
    for (Etiquetas etiqueta : etiquetas) {
      key = etiqueta.getEtiqueta();
      String campo = etiqueta.getCampo();
      switch (campo) {
        case "NBNOMBREALIAS":
          value = alias.getDescrali();
          break;
        default:
          value = "<No data>";
      }
      mapping.put(key, value.trim());
    }
    return mapping;
  }

  public List<Tmct0alc> findByReferencias3(String referencias3) {
    return dao.findByReferencias3(referencias3);
  }

  public List<Tmct0alc> findAllByNuordenAndNbookingAndActivos(String nuorden, String nbooking) {
    return dao.findAllByNuordenAndNbookingAndActivos(nuorden, nbooking);
  }

  public List<Date> findAllFeejeliqBySinReferenciaAndNotCdestadotitAndCdestadotitLt(int notcdestadotit,
      int cdestadotitLt) {
    return dao.findAllFeejeliqBySinReferenciaAndNotCdestadotitAndCdestadotitLt(notcdestadotit, cdestadotitLt);
  }

  public List<Date> findAllFeejeliqByConReferenciaAndInCdestadotitAndCdestadoasigGte(List<Integer> listCdestadotit,
      int cdestadoasigGte) {
    return dao.findAllFeejeliqByConReferenciaAndInCdestadotitAndCdestadoasigGte(listCdestadotit, cdestadoasigGte);
  }

  public List<Tmct0alcId> findAllTmct0alcIdByNuordenAndNbookingAndActivosAndNotCdestadotit(String nuorden,
      String nbooking, int notcdestadotit) {
    return dao.findAllTmct0alcIdByNuordenAndNbookingAndActivosAndNotCdestadotit(nuorden, nbooking, notcdestadotit);
  }

  public List<Tmct0alcId> findAllTmct0alcIdByNuordenAndNbookingAndActivosAndSinReferenciaTitular(String nuorden,
      String nbooking) {
    return dao.findAllTmct0alcIdByNuordenAndNbookingAndActivosAndSinReferenciaTitular(nuorden, nbooking);
  }

  public List<Tmct0bokId> findAllTmct0bokIdByFeejeliqSinReferenciaTitularRouting(Date feejeliq, char isscrdiv,
      int notCdestadotit, Pageable page) {
    return dao.findAllTmct0bokIdByFeejeliqAndSinReferenciaTitularRouting(feejeliq, isscrdiv, notCdestadotit,
        TipoRouting.CDORIGEN, TipoRouting.CDCLENTE, page);
  }

  public List<Tmct0bokId> findAllTmct0bokIdByFeejeliqAndSinReferenciaTitularNoRouting(Date feejeliq, char isscrdiv,
      int notCdestadotit, Pageable page) {
    return dao.findAllTmct0bokIdByFeejeliqAndSinReferenciaTitularNoRouting(feejeliq, isscrdiv, notCdestadotit,
        TipoRouting.CDORIGEN, TipoRouting.CDCLENTE, page);
  }

  public List<Tmct0bokId> findAllTmct0bokIdByFeejeliqInCdestadotitAndCdestadoasigGteConReferenciaTitular(Date feejeliq,
      char isscrdiv, List<Integer> inCdestadotit, int cdestadoasigGte, Pageable page) {
    return dao.findAllTmct0bokIdByFeejeliqInCdestadotitAndCdestadoasigGteConReferenciaTitular(feejeliq, isscrdiv,
        inCdestadotit, cdestadoasigGte, page);

  }

  public List<Tmct0alc> findByNbooking(String nbooking) {
    return dao.findByNbooking(nbooking);
  }

  public List<Tmct0alc> findByNbookingAndCuentaCompensacionDestino(String nbooking, List<String> cdCodigosValidos) {
    return dao.findByNbookingAndCuentaCompensacionDestino(nbooking, cdCodigosValidos);
  }

  public List<Tmct0eje> findEjes(Tmct0alc alc) {
    List<Tmct0desglosecamara> listaDc = alc.getTmct0desglosecamaras();
    List<Tmct0eje> listaEjes = new ArrayList<Tmct0eje>();
    for (Tmct0desglosecamara dc : listaDc) {
      for (Tmct0desgloseclitit dclitit : dc.getTmct0desgloseclitits()) {
        listaEjes.addAll(ejeBo.findAllByNbookingAndNuordenAndNuejecuc(alc.getId().getNbooking(), alc.getId()
            .getNuorden(), dclitit.getNuejecuc()));
      }
    }
    return listaEjes;
  }

  public List<Tmct0desgloseclitit> findDesglosesClitit(Tmct0alc alc) {
    List<Tmct0desglosecamara> listaDc = alc.getTmct0desglosecamaras();
    List<Tmct0desgloseclitit> listaDcClitit = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desglosecamara dc : listaDc) {
      listaDcClitit.addAll(dc.getTmct0desgloseclitits());
    }
    return listaDcClitit;
  }

  /**
   * Cambia el estado de asignación de todos los desgloses camara recibidos cuya
   * cuenta de compensación esté vacía o sea nula al estado
   * ASIGNACIONES.LIQUIDADA_TEORICA.
   *
   * @param alcList Lista de <code>Tmct0alc</code> a cambiar el estado.
   */
  @Transactional
  public void changeAsigStateDMenosTres(Date dMenosTres, Date hoy, Tmct0estado estadoAsigLiquidadaTeorica,
      Tmct0estado estadoAsigEnCurso) {
    LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Inicio ...");

    Boolean cambio = false;
    List<Tmct0alc> alcList = dao.findByNullCompCtaAndFeejeliq(ASIGNACIONES.LIQUIDADA_DEFINITIVA.getId(), dMenosTres);

    if (alcList != null) {
      LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Cambiando estado de " + alcList.size() + " alcs ...");
      for (Tmct0alc tmct0alc : alcList) {
        LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Procesando alc: " + tmct0alc.getId().toString());

        for (Tmct0desglosecamara tmct0desglosecamara : tmct0alc.getTmct0desglosecamaras()) {
          LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Procesando dc: "
              + tmct0desglosecamara.getIddesglosecamara());
          if (tmct0desglosecamara.getTmct0infocompensacion().getCdctacompensacion() == null
              || tmct0desglosecamara.getTmct0infocompensacion().getCdctacompensacion().trim().isEmpty()) {
            LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] CDCTACOMPENSACION null o vacio");
            tmct0desglosecamara.setTmct0estadoByCdestadoasig(estadoAsigLiquidadaTeorica);
            tmct0desglosecamara.setAuditFechaCambio(hoy);
            cambio = true;
          }
          else {
            LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] CDCTACOMPENSACION no vacio: "
                + tmct0desglosecamara.getTmct0infocompensacion().getCdctacompensacion());
          } // else
        } // for (Tmct0desglosecamara tmct0desglosecamara :
          // tmct0alc.getTmct0desglosecamaras())

        if (cambio) {
          tmct0alc.setCdestadoasig(ASIGNACIONES.EN_CURSO.getId());
          tmct0alc.setFhaudit(hoy);
          // Se salvan también los desglosescamara por el
          // Cascade.Persist
          dao.save(tmct0alc);

          Tmct0alo alo = tmct0alc.getTmct0alo();
          alo.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
          alo.setFhaudit(hoy);
          tmct0aloDao.save(alo);

          Tmct0bok bok = alo.getTmct0bok();
          bok.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
          bok.setFhaudit(hoy);
          tmct0bokDao.save(bok);

          try {
            tmct0logBo.insertarRegistro(tmct0alc, hoy, hoy, null, ASIGNACIONES.LIQUIDADA_TEORICA.getId(),
                "LiqStateChange. ASIGNACIONES.LIQUIDADA_TEORICA");
          }
          catch (Exception ex) {
            LOG.warn(
                "[LiqStateChange :: changeAsigStateDMenosTres] Error cambiando estado dMenosTres en Tmct0log ... ",
                ex.getMessage());
          } // catch
        } // if (cambio)

        cambio = false; // Se resetea el estado de la variable
      } // for (Tmct0alc tmct0alc : alcList)
    } // if
    LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Fin ...");
  } // changeAsigStateDMenosTres

  /**
   * Cambia el estado de asignación de todos los ALCS recibidos al estado
   * ASIGNACIONES.LIQUIDADA_DEFINITIVA.
   *
   * @param alcList Lista de <code>Tmct0alc</code> a cambiar el estado.
   */
  @Transactional
  public void changeAsigStateDMenosOcho(Date dMenosOcho, Date hoy, Tmct0estado estadoAsigLiquidadaDefinitiva,
      Tmct0estado estadoAsigEnCurso) {
    LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Inicio ...");

    Boolean cambio = false;
    List<Tmct0alc> alcList = dao.findByEstadosAsigAndFeejeliq(ASIGNACIONES.LIQUIDADA_TEORICA.getId(),
        ASIGNACIONES.LIQUIDADA_DEFINITIVA.getId(), dMenosOcho);

    if (alcList != null) {
      LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Cambiando estado de " + alcList.size() + " alcs ...");
      for (Tmct0alc tmct0alc : alcList) {
        LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Procesando alc: " + tmct0alc.getId().toString());

        for (Tmct0desglosecamara tmct0desglosecamara : tmct0alc.getTmct0desglosecamaras()) {
          LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Procesando dc: "
              + tmct0desglosecamara.getIddesglosecamara());
          tmct0desglosecamara.setTmct0estadoByCdestadoasig(estadoAsigLiquidadaDefinitiva);
          tmct0desglosecamara.setAuditFechaCambio(hoy);
          cambio = true;
        } // for

        if (cambio) {
          tmct0alc.setCdestadoasig(ASIGNACIONES.EN_CURSO.getId());
          tmct0alc.setFhaudit(hoy);
          // Se salvan también los desglosescamara por el
          // Cascade.Persist
          dao.save(tmct0alc);

          Tmct0alo alo = tmct0alc.getTmct0alo();
          alo.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
          alo.setFhaudit(hoy);
          tmct0aloDao.save(alo);

          Tmct0bok bok = alo.getTmct0bok();
          bok.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
          bok.setFhaudit(hoy);
          tmct0bokDao.save(bok);

          try {
            tmct0logBo.insertarRegistro(tmct0alc, hoy, hoy, null, ASIGNACIONES.LIQUIDADA_DEFINITIVA.getId(),
                "LiqStateChange. ASIGNACIONES.LIQUIDADA_DEFINITIVA");
          }
          catch (Exception ex) {
            LOG.warn(
                "[LiqStateChange :: changeAsigStateDMenosOcho] Error cambiando estado dMenosOcho en Tmct0log ... ",
                ex.getMessage());
          } // catch
        } // if (cambio)

        cambio = false; // Se resetea el estado de la variable
      } // for (Tmct0alc tmct0alc : alcList)
    } // if
    LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Fin ...");
  } // changeAsigStateDMenosOcho

  /**
   * Recibe un alc y devuelve una lista de los movimientos ecc asociados a sus
   * desgloses cámaras
   * 
   * @param alc
   * @return
   */
  public List<Tmct0movimientoecc> getMovimientosEcc(Tmct0alc alc) {
    List<Tmct0movimientoecc> listaEccs = new ArrayList<Tmct0movimientoecc>();
    List<Tmct0desglosecamara> listaDesgloses = alc.getTmct0desglosecamaras();

    for (Tmct0desglosecamara dc : listaDesgloses) {
      List<Tmct0movimientoecc> movimientosEcc = movimientoEccBo.findByIdDesgloseCamara(dc.getIddesglosecamara());

      if (movimientosEcc != null && movimientosEcc.size() > 0) {
        listaEccs.addAll(movimientosEcc);
      }
    }
    return listaEccs;
  }

  @Transactional
  public List<String> findOrdenesParaEnvioS3(Date fechaBusqueda) {
    return dao
        .findOrdenesParaEnvioS3(fechaBusqueda, ASIGNACIONES.PDTE_LIQUIDAR.getId(),
            Integer.toString(CLEARING.PDTE_FICHERO.getId()), Integer.toString(CLEARING.RECHAZADO_FICHERO.getId()),
            Integer.toString(CLEARING.PDTE_ENVIAR_ANULACION.getId()),
            Integer.toString(CLEARING.PDTE_ENVIAR_ANULACION_PARCIAL.getId()),
            Integer.toString(CLEARING.INCIDENCIA.getId()));
  }

  @Transactional
  public List<Tmct0alc> findByEnvioFicheroMegaraClienteTactico(Date fechaBusqueda) {
    return dao.findByEnvioFicheroMegaraClienteTactico(fechaBusqueda, ASIGNACIONES.PDTE_LIQUIDAR.getId(),
        Integer.toString(CLEARING.PDTE_FICHERO.getId()), Integer.toString(CLEARING.RECHAZADO_FICHERO.getId()),
        Integer.toString(CLEARING.PDTE_ENVIAR_ANULACION.getId()), Integer.toString(CLEARING.INCIDENCIA.getId()));
  }

  public List<Tmct0alc> findConDesglosesPorEnviar(String nuorden) {
    return dao.findConDesglosesPorEnviar(nuorden);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void updateAlcModificacionTitulares(Tmct0alc alc, String ccvNueva, List<String> indentificaciones,
      List<Date> holidays, String cdusuaud) {
    this.updateAlcModificacionTitulares(alc, null, ccvNueva, indentificaciones, holidays, cdusuaud);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void updateAlcModificacionTitulares(Tmct0alc alc, Tmct0desglosecamara dc, String ccvNueva,
      List<String> listIdentificaciones, List<Date> holidays, String cdusuaud) {

    List<Tmct0desglosecamara> dcs;
    boolean persistAlc = false;
    boolean persistBok = false;

    String msg;

    Tmct0referenciatitular referenciaTitularAntigua;
    Tmct0referenciatitular referenciaTitularNueva = null;
    String referenciaTitularNuevaSt = null;
    String ccvVieja = StringUtils.trim(alc.getCdrefban());
    if (ccvNueva == null) {
      ccvNueva = ccvVieja;
    }
    ccvNueva = StringUtils.trim(ccvNueva);

    boolean modificacionCcv = ccvNueva != null && !StringUtils.equals(ccvVieja, ccvNueva);

    LOG.trace("[updateEstadosAlcModificacionTitulares] alcID: {}", alc.getId());

    boolean isFinModificaciones = alc.getFeopeliq().after(addNDays(new Date(), holidays, 7));
    boolean isRectificacion = new Date().after(alc.getFeopeliq());
    boolean reenvioTi = true;

    if (!isFinModificaciones) {

      if (dc != null) {
        dcs = new ArrayList<Tmct0desglosecamara>();
        dcs.add(dc);
      }
      else {
        // dcs = dcBo.findAllByTmct0alcId(alc.getId());
        dcs = alc.getTmct0desglosecamaras();
      }

      boolean isRouting = isRoutingOrOperacionEspecial(alc);

      LOG.trace("[updateEstadosAlcModificacionTitulares] ccv vieja: {} ccv nueva: {}", ccvVieja, ccvNueva);
      referenciaTitularAntigua = alc.getReferenciaTitular();

      reenvioTi = referenciaTitularAntigua != null && !isRectificacion;
      if (isRectificacion || !isRouting || modificacionCcv) {
        persistAlc = true;
        this.limpiarReferenciaTitularVieja(alc, cdusuaud);
      }

      if (modificacionCcv) {
        persistAlc = true;
        reenvioTi = false;
        persistBok = this.modificarCcv(alc, referenciaTitularAntigua, ccvVieja, ccvNueva, isRouting, isRectificacion,
            cdusuaud);

      }

      if (reenvioTi && !isRouting && !isRectificacion && CollectionUtils.isNotEmpty(listIdentificaciones)) {
        // calcular la nueva referencia de titular desde las indentificaciones
        referenciaTitularNuevaSt = referenciaTitularBo.findReferenciaTitularFromReferenciasTitularFis(ccvNueva,
            listIdentificaciones);
        if (referenciaTitularNuevaSt == null || referenciaTitularAntigua == null
            || !referenciaTitularNuevaSt.trim().equals(referenciaTitularAntigua.getCdreftitularpti().trim())) {
          reenvioTi = false;
        }
      }

      if (reenvioTi) {
        // se reenvia el ti cuando no sea routing ni rectificacion y la
        // referencia del titular no cambie
        // se reenviara el ti para todos los routing
        referenciaTitularNueva = referenciaTitularAntigua;
        this.marcarTiReenvio(alc, referenciaTitularAntigua, cdusuaud);
      }
      else if (StringUtils.isNotBlank(referenciaTitularNuevaSt)) {
        referenciaTitularNueva = referenciaTitularBo.findByCdreftitularptiAndReferenciaAdicional(
            referenciaTitularNuevaSt, ccvNueva);
        if (referenciaTitularNueva != null) {
          this.marcarTiReenvio(alc, referenciaTitularNueva, cdusuaud);
        }
      }

      if (referenciaTitularNueva != null && referenciaTitularAntigua != null
          && referenciaTitularAntigua.getIdreferencia() == referenciaTitularNueva.getIdreferencia()) {
        alc.setReferenciaTitular(referenciaTitularNueva);
        msg = String.format("La referencia de titular '%s' no cambia, se vuelve a asignar al desglose.",
            referenciaTitularNueva.getCdreftitularpti().trim());
        LOG.debug("[updateEstadosAlcModificacionTitulares] {}", msg);
        tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);
      }

      if (referenciaTitularAntigua != null
          && (alc.getReferenciaTitular() == null || alc.getReferenciaTitular() != null
              && referenciaTitularAntigua.getIdreferencia() != alc.getReferenciaTitular().getIdreferencia())) {
        setFlagRecalculateCanonByReferenciaTitular(alc, referenciaTitularAntigua, cdusuaud);
      }

      if (alc.getEstadoentrec().getIdestado() == EstadosEnumerados.CLEARING.ACEPTADA_ANULACION.getId()) {
        persistAlc = true;

        msg = String.format("Marcando pata cliente para el reenvio, tit.bok:'%s' tit:'%s'.", alc.getTmct0alo()
            .getTmct0bok().getNutiteje(), alc.getNutitliq());
        LOG.trace("[updateEstadosAlcModificacionTitulares] {}", msg);
        tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, alc.getEstadoentrec()
            .getNombre(), cdusuaud);
        alc.setEstadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
      }

      for (Tmct0desglosecamara dcFor : dcs) {

        if (dcFor.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          if (dcFor.getTmct0estadoByCdestadoentrec().getIdestado() == EstadosEnumerados.CLEARING.ACEPTADA_ANULACION
              .getId()) {

            msg = String.format("Marcando pata mercado id dc: '%s' para el reenvio, tit.bok.:'%s' tit.dc.:'%s'.",
                dcFor.getIddesglosecamara(), alc.getTmct0alo().getTmct0bok().getNutiteje(), dcFor.getNutitulos());
            LOG.trace("[updateEstadosAlcModificacionTitulares] {}", msg);
            tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, dcFor
                .getTmct0estadoByCdestadoentrec().getNombre(), cdusuaud);
            dcFor.setTmct0estadoByCdestadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
          }

          if (isRectificacion) {
            dcFor.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR_RECTIFICACION
                .getId()));
          }
          else {
            dcFor.setTmct0estadoByCdestadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR_MODIFICACION
                .getId()));
          }
          dcFor.setAuditFechaCambio(new Date());
          dcFor.setAuditUser(cdusuaud);

        }
        dcBo.save(dcFor);
      }
      if (persistAlc || persistBok) {
        alc.setEstadotit(new Tmct0estado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId()));
        alc.setFhaudit(new Date());
        alc.setCdusuaud(cdusuaud);
        save(alc);

        alc.getTmct0alo().setCdestadotit(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
        alc.getTmct0alo().setCdusuaud(cdusuaud);
        alc.getTmct0alo().setFhaudit(new Date());
        tmct0aloDao.save(alc.getTmct0alo());

        alc.getTmct0alo().getTmct0bok().setCdestadotit(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
        alc.getTmct0alo().getTmct0bok().setCdusuaud(cdusuaud);
        alc.getTmct0alo().getTmct0bok().setFhaudit(new Date());
        tmct0bokDao.save(alc.getTmct0alo().getTmct0bok());
      }

    }
    else {
      LOG.warn("[updateEstadosAlcModificacionTitulares] YA NO SE PUEDE MODIFICAR LA TITULARIDAD.");
    }
    if (listIdentificaciones != null && !listIdentificaciones.isEmpty()) {
      if (alc.getCdniftit() == null || alc.getCdniftit().trim().isEmpty()
          || !alc.getCdniftit().trim().equals(listIdentificaciones.get(0).trim())) {
        List<Tmct0afi> listAfis = afiBo.findActivosByAlcData(alc.getId().getNuorden(), alc.getId().getNbooking(), alc
            .getId().getNucnfclt(), alc.getId().getNucnfliq());
        for (Tmct0afi afi : listAfis) {
          if (afi.getCdholder() != null && afi.getCdholder().trim().equals(listIdentificaciones.get(0).trim())) {
            setDataFromAfi(afi, alc);
            break;
          }
        }
      }
    }
  }

  private void setDataFromAfi(Tmct0afi afi, Tmct0alc alc) {
    String nbtitular;
    Tmct0fis fis = null;

    try {
      fis = fisDao.findBycdholder(afi.getCdholder());
    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    alc.setCdniftit(StringUtils.substring(afi.getCdholder().trim(), 0, 11));

    if (afi.getTpfisjur() != null && afi.getTpfisjur() == TipoPersona.FISICA.getTipoPersona()
        && afi.getNbclient() != null && !afi.getNbclient().trim().isEmpty()) {
      nbtitular = String.format("%s * %s * %s", afi.getNbclient().trim(), afi.getNbclient1().trim(), afi.getNbclient2()
          .trim());
    }
    else {
      nbtitular = afi.getNbclient1().trim();
    }
    alc.setNbtitliq(StringUtils.substring(nbtitular, 0, 60));

    if (fis == null) {
      alc.setCdordtit(StringUtils.substring(afi.getCdholder().trim(), 0, 25));
    }
    else {
      if (fis.getCdordtit() != null && !fis.getCdordtit().trim().isEmpty()) {
        alc.setCdordtit(StringUtils.substring(fis.getCdordtit().trim(), 0, 25));
      }
      else {
        alc.setCdordtit(StringUtils.substring(afi.getCdholder().trim(), 0, 25));
      }
    }
  }

  @Transactional
  public void reenviarS3(Tmct0alc alc, Tmct0desglosecamara dc, String cdusuaud) {
    String msg;
    if (alc.getEstadoentrec().getIdestado() == EstadosEnumerados.CLEARING.ACEPTADA_ANULACION.getId()) {

      msg = String.format("Marcando pata cliente para el reenvio, tit.bok:'%s' tit:'%s'.", alc.getTmct0alo()
          .getTmct0bok().getNutiteje(), alc.getNutitliq());
      LOG.trace("[updateEstadosAlcModificacionTitulares] {}", msg);
      tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, alc.getEstadoentrec()
          .getNombre(), cdusuaud);
      alc.setEstadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
      save(alc);
    }

    if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
      if (dc.getTmct0estadoByCdestadoentrec().getIdestado() == EstadosEnumerados.CLEARING.ACEPTADA_ANULACION.getId()) {

        msg = String.format("Marcando pata mercado id dc: '%s' para el reenvio, tit.bok.:'%s' tit.dc.:'%s'.",
            dc.getIddesglosecamara(), alc.getTmct0alo().getTmct0bok().getNutiteje(), dc.getNutitulos());
        LOG.trace("[updateEstadosAlcModificacionTitulares] {}", msg);
        tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, dc
            .getTmct0estadoByCdestadoentrec().getNombre(), cdusuaud);
        dc.setTmct0estadoByCdestadoentrec(new Tmct0estado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId()));
      }

      dc.setAuditFechaCambio(new Date());
      dc.setAuditUser(cdusuaud);

      dcBo.save(dc);
    }

  }

  @Transactional
  private void limpiarReferenciaTitularVieja(Tmct0alc alc, String cdusuaud) {
    LOG.trace("[updateEstadosAlcModificacionTitulares] desvinculando de la ferenencia de titular vieja: {}",
        alc.getCdreftit());

    alc.setCdreftit("");
    alc.setRftitaud("");
    if (alc.getReferenciaTitular() != null) {
      String msg = String.format(
          "Limpiada referencia titular '%s' para que se recalcule forzando que se reenvie referencia y titular", alc
              .getReferenciaTitular().getCdreftitularpti().trim());
      LOG.debug("[updateEstadosAlcModificacionTitulares] {}", msg);
      tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);
      alc.setReferenciaTitular(null);
    }
  }

  @Transactional
  private boolean modificarCcv(Tmct0alc alc, Tmct0referenciatitular referenciaTitularAntigua, String ccvVieja,
      String ccvNueva, boolean isRouting, boolean isRectificacion, String cdusuaud) {
    boolean persistBok = false;
    alc.setCdrefban(ccvNueva);
    String msg = String.format("Modificacion CCV: '%s' a '%s' titulos:'%s'", ccvVieja, ccvNueva, alc.getNutitliq());
    LOG.debug("[updateEstadosAlcModificacionTitulares] {}", msg);
    tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);
    if (StringUtils.isBlank(alc.getTmct0alo().getTmct0bok().getTmct0ord().getNureford())
        && StringUtils.isBlank(alc.getTmct0alo().getRefCliente())) {
      persistBok = true;
      alc.getTmct0alo().getTmct0bok().setCdindimp(' ');
      alc.getTmct0alo().getTmct0bok().setCdauxili("");
      msg = "Marcado 16-17 para reenvio.";
      LOG.debug("[updateEstadosAlcModificacionTitulares] {}", msg);
      tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);

    }
    alc.setCdenvmor("N");
    msg = "Marcado 18-19 para reenvio.";
    LOG.debug("[updateEstadosAlcModificacionTitulares] {}", msg);
    tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);

    return persistBok;
  }

  @Transactional
  private void marcarTiReenvio(Tmct0alc alc, Tmct0referenciatitular referenciaTitular, String cdusuaud) {
    String msg;
    List<Tmct0enviotitularidad> tis = envioTitularidadDao.findAllByReferenciaTitularAndFinicioAndAlta(
        referenciaTitular.getCdreftitularpti(), alc.getFeejeliq());
    for (Tmct0enviotitularidad ti : tis) {
      // no es cancelado
      if (ti.getEstado() != null && ti.getEstado() != 'C') {
        ti.setEstado('P');
        ti.setAuditFechaCambio(new Date());
        ti.setAuditUser(cdusuaud);
        ti.setFenvio(null);
        ti.setFrecepcion(null);
        msg = String.format("Marcado TI para reenvio miembro: '%s' f.contratacion:'%s'.", ti.getCdmiembromkt(),
            ti.getFeinicio());
        LOG.debug("[updateEstadosAlcModificacionTitulares] {}", msg);
        tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);
        envioTitularidadDao.save(ti);

      }
    }
  }

  private boolean isRoutingOrOperacionEspecial(Tmct0alc alc) {

    boolean isRouting = ordBo.isRouting(alc.getTmct0alo().getTmct0bok().getTmct0ord());
    if (!isRouting) {
      LOG.trace("[updateEstadosAlcModificacionTitulares] no es routing, revisamos si es op especial: {}", alc
          .getTmct0alo().getTmct0bok().getCdalias());
      Tmct0ali alias = aliBo.findByFirstAliasOrder(alc.getTmct0alo().getTmct0bok().getCdalias());
      if (alias != null) {
        isRouting = alias.getRetailSpecialOp() != null && 'S' == alias.getRetailSpecialOp();
      }
    }

    if (!isRouting) {
      LOG.trace("[updateEstadosAlcModificacionTitulares] revisando si es una operacion marcada como barrido...");
      isRouting = 'S' == alc.getTmct0alo().getTmct0bok().getTmct0ord().getIsscrdiv();
    }

    return isRouting;
  }

  public Date add5Days(Date fechaInicial, List<Date> holidays) {
    Date fechaFinal = fechaInicial;

    try {
      int daysToAdd = 5;

      while (daysToAdd != 0) {
        fechaFinal = DateUtils.addDays(fechaFinal, 1);
        boolean seSuma = true;

        // Primero comprobamos si es fin de semana
        Calendar c = Calendar.getInstance();
        c.setTime(fechaFinal);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 7 || dayOfWeek == 1) {
          seSuma = false;
        }
        else {
          for (Date holiday : holidays) {
            if (DateUtils.isSameDay(holiday, fechaFinal)) {
              seSuma = false;
              break;
            }
          }
        }
        if (seSuma) {
          daysToAdd--;
        }
      }
    }
    catch (Exception e) {
      LOG.error("[Tmct0alcBo :: add5Days] Error: " + e.getMessage());
    }
    return fechaFinal;
  }

  public Date addNDays(Date fechaInicial, List<Date> holidays, int daysToAdd) {
    Date fechaFinal = fechaInicial;

    try {
      while (daysToAdd != 0) {
        fechaFinal = DateUtils.addDays(fechaFinal, 1);
        boolean seSuma = true;

        // Primero comprobamos si es fin de semana
        Calendar c = Calendar.getInstance();
        c.setTime(fechaFinal);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 7 || dayOfWeek == 1) {
          seSuma = false;
        }
        else {
          for (Date holiday : holidays) {
            if (DateUtils.isSameDay(holiday, fechaFinal)) {
              seSuma = false;
              break;
            }
          }
        }
        if (seSuma) {
          daysToAdd--;
        }
      }
    }
    catch (Exception e) {
      LOG.error("[Tmct0alcBo :: add5Days] Error: " + e.getMessage());
    }
    return fechaFinal;
  }

  public List<Tmct0alc> findByNuorden(String nuorden) {
    return dao.findByNuorden(nuorden);
  }

  @Transactional
  public List<Tmct0AlcData> findDataByNuorden(String nuorden) {
    return dao.findDataByNuorden(nuorden);
  }

  public Tmct0AlcData findAlcDataById(Tmct0alcId tmct0alcId) {
    Tmct0AlcData data = null;
    try {
      Tmct0alc alc = findById(tmct0alcId);
      if (alc != null) {
        data = new Tmct0AlcData(alc);
      }
      else {
        return null;
      }
    }
    catch (Exception e) {
      LOG.debug("[Tmct0alcBo :: findAlcDataById] Problema " + e.getMessage());
    }
    return data;
  }

  public List<Tmct0alc> getAlcs(String nuorden, String nbooking, String nucnfclt, int estado) {
    return dao.getAlcs(nuorden, nbooking, nucnfclt, estado);
  }

  public List<Tmct0alc> getAlcs(String nuorden, String nbooking, String nucnfclt) {
    return dao.getAlcs(nuorden, nbooking, nucnfclt);
  }

  public Tmct0alc getAlc(String nuorden, String nbooking, String nucnfclt, short nucnfliq) {
    return dao.getAlc(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  public void updateAlcsViaCcv(Tmct0afi tmct0afi, Set<Tmct0alc> listaAlcsModificados, String oldCcv,
      List<Date> listaFestivos, String cdusuaud) {
    if (afiBo.containsAlcs(tmct0afi)) {
      listaAlcsModificados.addAll(tmct0afi.getTmct0alo().getTmct0alcs());

      // Vamos a actualizar los alc
      List<String> identificaciones;
      for (Tmct0alc alc : tmct0afi.getTmct0alo().getTmct0alcs()) {
        identificaciones = afiBo.findAllCdholderByAlcAndActivos(alc.getId().getNuorden(), alc.getId().getNbooking(),
            alc.getId().getNucnfclt(), alc.getId().getNucnfliq());
        this.updateAlcModificacionTitulares(alc, tmct0afi.getCcv(), identificaciones, listaFestivos, cdusuaud);
      }
    }
  }

  /**
   * devuelve alcs que tienen el cdenvliq='O' y corretajePti='F'
   * */
  public List<EnvioCorretajeTmct0alcDataDTO> getEnvioCorretajeTmct0alcSinEnviar(Date toFechaContratacion,
      Date toFechaLiquidacion) {
    return dao.getEnvioCorretajeTmct0alcSinEnviar(toFechaContratacion, toFechaLiquidacion,
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(), EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
  }

  public Integer updateEstadoEntregaRecepcion(String nuorden, String nbooking, String nucnfclt, short nucnfliq,
      EstadosEnumerados.CLEARING estado) {
    return dao.updateEstadoEntregaRecepcion(nuorden, nbooking, nucnfclt, nucnfliq, estado.getId(), new Date());
  }

  public List<ConfirmacionMinorista17DTO> findAllConfirmacionMinorista17(List<String> compensadores,
      List<String> referencias, List<String> notInAliass, String cdrefbanStart, Integer estado, Date sincedate) {

    return this.dao.findAllConfirmacionMinorista17(compensadores, referencias, notInAliass, cdrefbanStart, estado, "9",
        sincedate);
  }

  public List<ConfirmacionMinorista17DTO> findAllConfirmacionMinorista17(String nuorden, String nbooking,
      List<String> compensadores, List<String> referencias) {
    return dao.findAllConfirmacionMinorista17(nuorden, nbooking, compensadores, referencias,
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(), "9");
  }

  /**
   * 
   * @param alias
   * @param fechaDesde
   * @param fechaHasta
   * @param tipoEstado
   * @param estadoCont
   * @return
   */
  public List<PayLetterDTO> findAllDataAndDvoByFechaAndAlias(String alias, Date fechaDesde, Date fechaHasta,
      Integer tipoEstado, Integer estadoCont) {
    List<PayLetterDTO> payLetterDTOList;
    PayLetterDTO payLetterDTO;

    List<Object[]> tmct0alcList = dao.findAllDataAndDvoByFechaAndAlias(alias, fechaDesde, fechaHasta, tipoEstado,
        estadoCont);
    payLetterDTOList = new ArrayList<PayLetterDTO>(tmct0alcList.size());

    for (Object[] tmct0alc : tmct0alcList) {
      payLetterDTO = new PayLetterDTO(tmct0alc);
      payLetterDTOList.add(payLetterDTO);
    } // for

    return payLetterDTOList;
  } // findAllDataAndDvoByFechaAndAlias

  /**
   * 
   * @param alias
   * @param fechaDesde
   * @param fechaHasta
   * @param tipoEstado
   * @param estadoCont
   * @return
   */
  public List<PayLetterDTO> findAllDataAndBrkByFechaAndAlias(String alias, Date fechaDesde, Date fechaHasta,
      Integer tipoEstado, Integer estadoCont) {
    List<PayLetterDTO> payLetterDTOList;
    PayLetterDTO payLetterDTO;

    List<Object[]> tmct0alcList = dao.findAllDataAndBrkByFechaAndAlias(alias, fechaDesde, fechaHasta, tipoEstado,
        estadoCont);
    payLetterDTOList = new ArrayList<PayLetterDTO>(tmct0alcList.size());

    for (Object[] tmct0alc : tmct0alcList) {
      payLetterDTO = new PayLetterDTO(tmct0alc);
      payLetterDTOList.add(payLetterDTO);
    } // for

    return payLetterDTOList;
  } // findAllDataAndBrkByFechaAndAlias

  public Integer updateCdestadotitByTmct0aloId(Tmct0aloId alcId, int cdestadoatit) {
    return dao.updateCdestadotitByTmct0aloId(alcId, cdestadoatit, new Date());
  }

  @Transactional
  public Integer updateCdestadotitByTmct0alcIdAndDown(Tmct0alcId alcId, int cdestadoatit) {
    dcBo.updateCdestadotitByTmct0alcId(alcId, cdestadoatit);
    return dao.updateCdestadotitByTmct0alcId(alcId, cdestadoatit, new Date());
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Integer updateCdestadotitByTmct0alcIdAndDownNewTransaction(Tmct0alcId alcId, int cdestadoatit) {
    dcBo.updateCdestadotitByTmct0alcId(alcId, cdestadoatit);
    return dao.updateCdestadotitByTmct0alcId(alcId, cdestadoatit, new Date());
  }

  public Tmct0referenciatitular findTmct0referenciatitular(Tmct0alcId alcId) {
    return dao.findTmct0referenciatitular(alcId);
  }

  public boolean isTitulosAlcsOk(Tmct0aloId aloId) {
    BigDecimal nutitcli = tmct0aloDao.getNutitcliByTmct0aloId(aloId);
    BigDecimal nutitliq = dao.getSumNutitliqByTmct0aloId(aloId);
    return nutitcli != null && nutitliq != null && nutitcli.compareTo(nutitliq) == 0;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void arreglarDesglosesCuadreEcc(Tmct0aloId aloId) {
    LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] inicio {}", aloId);
    List<Tmct0alc> alcsDeAlta = new ArrayList<Tmct0alc>();
    List<Tmct0alc> alcsDeBaja = new ArrayList<Tmct0alc>();
    Tmct0alo alo = tmct0aloDao.findOne(aloId);
    BigDecimal nutitliqAlta = BigDecimal.ZERO;
    BigDecimal nutitliqBaja = BigDecimal.ZERO;
    Tmct0alc alc;
    List<Tmct0desglosecamara> dcs;
    BigDecimal titulosDc;
    String msg;
    // recupero todos los alcs del alo, me da igual si de alta o baja
    LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] buscando todos los alcs {}...", aloId);
    List<Tmct0alc> alcs = findByAlloId(aloId);
    boolean hayAlcDesgloseTotalDeBaja = false;
    LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] separando alcs de alta y baja {}...", aloId);
    for (Tmct0alc tmct0alc : alcs) {
      if (tmct0alc.getNutitliq().compareTo(BigDecimal.ZERO) > 0) {
        if (tmct0alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          alcsDeAlta.add(tmct0alc);
          nutitliqAlta = nutitliqAlta.add(tmct0alc.getNutitliq());
        }
        else {
          alcsDeBaja.add(tmct0alc);
          if (tmct0alc.getNutitliq().compareTo(alo.getNutitcli()) == 0) {
            LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] hay un alc de baja con los titulos del alo. {}",
                tmct0alc.getId());
            hayAlcDesgloseTotalDeBaja = false;
          }
          nutitliqBaja = nutitliqBaja.add(tmct0alc.getNutitliq());
        }
      }
      else {
        LOG.warn("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] hay un alc con los titulos a cero, se ignora. {}",
            tmct0alc.getId());
      }
    }

    LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] ordenamos los alc's con id descendente.");
    Collections.sort(alcsDeAlta, new Comparator<Tmct0alc>() {

      @Override
      public int compare(Tmct0alc arg0, Tmct0alc arg1) {
        return arg0.getNucnfliq().compareTo(arg1.getNucnfliq()) * -1;
      }
    });

    if (hayAlcDesgloseTotalDeBaja && alcsDeBaja.size() > 1) {
      Collections.sort(alcsDeBaja, new Comparator<Tmct0alc>() {

        @Override
        public int compare(Tmct0alc arg0, Tmct0alc arg1) {
          return arg0.getNucnfliq().compareTo(arg1.getNucnfliq()) * -1;
        }
      });

      if (alcsDeAlta.get(0).getNucnfliq().compareTo(alcsDeBaja.get(0).getNucnfliq()) < 0
          && alcsDeBaja.get(0).getNutitliq().compareTo(alo.getNutitcli()) == 0) {
        LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] hemos encontrado un alc mas nuevo de baja con todos los titulos.");
        for (Tmct0alc tmct0alc : alcsDeAlta) {
          tmct0alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
          tmct0alc.setEstadotit(new Tmct0estado());
          tmct0alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
          tmct0alc.setFhaudit(new Date());
          darBajaDesglosesCamara(tmct0alc, dcBo.findAllByTmct0alcId(tmct0alc.getId()));
        }
        alc = alcsDeBaja.get(0);
        alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
        alc.setEstadotit(new Tmct0estado());
        alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
        alc.setFhaudit(new Date());
        dcs = dcBo.findAllByTmct0alcId(alc.getId());
        if (CollectionUtils.isNotEmpty(dcs)) {
          Collections.sort(dcs, new Comparator<Tmct0desglosecamara>() {

            @Override
            public int compare(Tmct0desglosecamara arg0, Tmct0desglosecamara arg1) {
              return arg0.getIddesglosecamara().compareTo(arg1.getIddesglosecamara()) * -1;
            }
          });
          titulosDc = BigDecimal.ZERO;
          List<Tmct0desglosecamara> dcsDarAlta = new ArrayList<Tmct0desglosecamara>();
          for (Tmct0desglosecamara dc : dcs) {
            titulosDc = titulosDc.add(dc.getNutitulos());
            dcsDarAlta.add(dc);
          }// fin for

          if (titulosDc.compareTo(alc.getNutitliq()) == 0) {
            msg = "CUADRE_ECC-ARREGLAR_DESGLOSES-Dando de alta el alc: " + alc.getId().getNucnfliq();
            tmct0logBo.insertarRegistro(aloId.getNuorden(), aloId.getNbooking(), aloId.getNucnfclt(), msg, "", "");
            volverAdarAltaDesglosesCamara(alc, dcsDarAlta);
            dcBo.save(dcsDarAlta);
          }
          else {
            msg = "CUADRE_ECC-ARREGLAR_DESGLOSES-No se ha podido arreglar el desglose, revisar manualmente los dc's";
            tmct0logBo.insertarRegistro(aloId.getNuorden(), aloId.getNbooking(), aloId.getNucnfclt(), msg, "", "");
          }
        }
      }
      else { // el alc de baja no tiene los mismos titulos que el alo

        nutitliqAlta = BigDecimal.ZERO;
        nutitliqBaja = BigDecimal.ZERO;
        alcsDeAlta.clear();
        alcsDeBaja.clear();
        for (Tmct0alc tmct0alc : alcs) {
          if (tmct0alc.getNutitliq().compareTo(BigDecimal.ZERO) <= 0) {
            LOG.warn("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] encontrado alc {} titulos a 0.", tmct0alc.getId());
            titulosDc = dcBo.getSumNutitulosByTmct0alcId(tmct0alc.getId());
            LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] en los dc's hay {} titulos {}.", titulosDc,
                tmct0alc.getId());
            if (titulosDc != null) {
              tmct0alc.setNutitliq(titulosDc);
              tmct0alc = save(tmct0alc);
            }
            else {
              LOG.warn("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] no encontrados dc's para el alc {}.",
                  tmct0alc.getId());
            }
          }
          if (tmct0alc.getNutitliq().compareTo(BigDecimal.ZERO) > 0) {
            if (tmct0alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              alcsDeAlta.add(tmct0alc);
              nutitliqAlta = nutitliqAlta.add(tmct0alc.getNutitliq());
            }
            else {
              if (tmct0alc.getNutitliq().compareTo(alo.getNutitcli()) < 0) {
                alcsDeBaja.add(tmct0alc);
                nutitliqBaja = nutitliqBaja.add(tmct0alc.getNutitliq());
              }
            }
          }
          else {
            LOG.warn("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] hay un alc con los titulos a cero, se ignora. {}",
                tmct0alc.getId());
          }
        }
      }
    }

    if (alo.getNutitcli().compareTo(nutitliqBaja.add(nutitliqAlta)) == 0) {
      LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] la suma de los alcs de alta y de baja da los titulos del alo.");
      for (Tmct0alc tmct0alc : alcsDeAlta) {
        if (tmct0alc.getCdestadoasig() <= EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          tmct0alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
          tmct0alc.setFhaudit(new Date());
          tmct0alc.setEstadotit(new Tmct0estado());
          tmct0alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
          tmct0alc.setFhaudit(new Date());
        }
        volverAdarAltaDesglosesCamara(tmct0alc, dcBo.findAllByTmct0alcId(tmct0alc.getId()));
      }
      alcsDeAlta = save(alcsDeAlta);
    }
    else if (alo.getNutitcli().compareTo(nutitliqBaja.add(nutitliqAlta)) < 0) {
      LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] Tenemos de mas titulos entre los alc's de baja y de alta, debemos elegir cual dar de alta.");

      BigDecimal sumatorioTitulos = new BigDecimal(nutitliqAlta.toString());
      List<Tmct0alc> alcsDarAlta = new ArrayList<Tmct0alc>();
      for (Tmct0alc tmct0alc : alcsDeBaja) {
        sumatorioTitulos = sumatorioTitulos.add(tmct0alc.getNutitliq());
        if (alo.getNutitcli().compareTo(sumatorioTitulos) == 0) {
          alcsDarAlta.add(tmct0alc);
          break;
        }
      }

      if (sumatorioTitulos.compareTo(alo.getNutitcli()) == 0) {
        if (CollectionUtils.isNotEmpty(alcsDarAlta)) {
          for (Tmct0alc tmct0alc : alcsDarAlta) {
            tmct0alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
            tmct0alc.setFhaudit(new Date());
            tmct0alc.setEstadotit(new Tmct0estado());
            tmct0alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
            tmct0alc.setFhaudit(new Date());
            volverAdarAltaDesglosesCamara(tmct0alc, dcBo.findAllByTmct0alcId(tmct0alc.getId()));
          }
        }
      }
    }

    ejealoBo.recalcularTitulosPendientesDesglose(aloId);

    LOG.debug("[Tmct0alcBo :: arreglarDesglosesCuadreEcc] fin {}", aloId);
  }

  /**
   * falta dar de alta los movimientos
   * 
   * @param alc
   * @param dcs
   */
  private void volverAdarAltaDesglosesCamara(Tmct0alc alc, List<Tmct0desglosecamara> dcs) {
    if (CollectionUtils.isNotEmpty(dcs)) {
      List<Tmct0desglosecamara> dcsDarAlta = new ArrayList<Tmct0desglosecamara>();
      BigDecimal titulosDc = BigDecimal.ZERO;
      for (Tmct0desglosecamara dc : dcs) {
        titulosDc = titulosDc.add(dc.getNutitulos());
        dcsDarAlta.add(dc);
      }// fin for
      if (titulosDc.compareTo(alc.getNutitliq()) == 0) {
        for (Tmct0desglosecamara dc : dcsDarAlta) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() <= EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
            dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
            dc.setTmct0estadoByCdestadotit(new Tmct0estado());
            dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.EN_CURSO.getId());
            dc.setAuditFechaCambio(new Date());
          }
        }
        dcBo.save(dcsDarAlta);
      }
    }
  }

  /**
   * falta dar de baja los movimientos
   * 
   * @param alc
   * @param dcs
   */
  private void darBajaDesglosesCamara(Tmct0alc alc, List<Tmct0desglosecamara> dcs) {

    if (CollectionUtils.isNotEmpty(dcs)) {
      List<Tmct0desglosecamara> dcsDarAlta = new ArrayList<Tmct0desglosecamara>();
      BigDecimal titulosDc = BigDecimal.ZERO;
      for (Tmct0desglosecamara dc : dcs) {
        titulosDc = titulosDc.add(dc.getNutitulos());
        dcsDarAlta.add(dc);
      }// fin for
      if (titulosDc.compareTo(alc.getNutitliq()) == 0) {
        for (Tmct0desglosecamara dc : dcsDarAlta) {
          dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
          dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
          dc.setTmct0estadoByCdestadotit(new Tmct0estado());
          dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
          dc.setAuditFechaCambio(new Date());
        }
        dcBo.save(dcsDarAlta);
      }
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void persistirAlcs(Tmct0alo aloCopia, int cdestadoasig, int cdestadotit) {
    this.persistirAlcsLocal(aloCopia, cdestadoasig, cdestadotit, true, true);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void persistirAlcs(Tmct0alo aloCopia) {
    this.persistirAlcsLocal(aloCopia, null, null, false, true);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void persistirAlcsForDesgloseAutomaticoAndManual(Tmct0alo aloCopia) {
    this.persistirAlcsLocal(aloCopia, null, null, false, false);
  }

  private void persistirAlcsLocal(Tmct0alo aloCopia, Integer cdestadoasig, Integer cdestadotit, boolean changeStatus,
      boolean persistReferenciaTitularAndTi) {
    LOG.trace("{}::{}  inicio.", TAG, "persistirAlcs");
    List<Tmct0alc> alcs = aloCopia.getTmct0alcs();
    List<Tmct0desglosecamara> dcs;
    List<Tmct0desgloseclitit> dcts;
    List<Tmct0movimientoecc> movs;
    List<Tmct0movimientooperacionnuevaecc> movsn;
    List<Tmct0enviotitularidad> tis;
    List<Tmct0DesgloseCamaraEnvioRt> rts;
    Tmct0referenciatitular refTit;
    for (Tmct0alc tmct0alc : alcs) {
      if (tmct0alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        if (changeStatus) {
          tmct0alc.setCdestadoasig(cdestadoasig);
          tmct0alc.getEstadotit().setIdestado(cdestadotit);
        }
        dcs = new ArrayList<Tmct0desglosecamara>(tmct0alc.getTmct0desglosecamaras());
        tmct0alc.getTmct0desglosecamaras().clear();
        if (persistReferenciaTitularAndTi) {
          refTit = tmct0alc.getReferenciaTitular();
          if (refTit != null) {
            tis = new ArrayList<Tmct0enviotitularidad>(refTit.getTmct0enviotitularidads());
            refTit.getTmct0enviotitularidads().clear();
            refTit = referenciaTitularBo.save(refTit);
            envioTitularidadDao.save(tis);// los dc rt estan colgando del
                                          // desglose
                                          // camara

            refTit.getTmct0enviotitularidads().addAll(tis);
          }
        }

        tmct0alc = save(tmct0alc);
        tmct0alc.getTmct0desglosecamaras().addAll(dcs);
        for (Tmct0desglosecamara dc : dcs) {
          if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            dcts = new ArrayList<Tmct0desgloseclitit>(dc.getTmct0desgloseclitits());
            dc.getTmct0desgloseclitits().clear();

            rts = new ArrayList<Tmct0DesgloseCamaraEnvioRt>(dc.getTmct0DesgloseCamaraEnvioRt());
            dc.getTmct0DesgloseCamaraEnvioRt().clear();

            movs = new ArrayList<Tmct0movimientoecc>(dc.getTmct0movimientos());
            if (changeStatus) {
              dc.getTmct0estadoByCdestadoasig().setIdestado(cdestadoasig);
            }
            dc.getTmct0movimientos().clear();
            if (dc.getTmct0infocompensacion() != null) {
              infoCompensacionDao.save(dc.getTmct0infocompensacion());
            }
            if (changeStatus) {
              dc.getTmct0estadoByCdestadotit().setIdestado(cdestadotit);
            }
            dc = dcBo.save(dc);

            desgloseClititBo.save(dcts);
            for (Tmct0DesgloseCamaraEnvioRt rt : rts) {
              if (rt.getTmct0desgloseclitit() != null) {
                rt.setNudesglose(rt.getTmct0desgloseclitit().getNudesglose());
              }
            }
            desgloseCamaraEnvioRtDao.save(rts);

            for (Tmct0movimientoecc mov : movs) {
              movsn = new ArrayList<Tmct0movimientooperacionnuevaecc>(mov.getTmct0movimientooperacionnuevaeccs());
              mov.getTmct0movimientooperacionnuevaeccs().clear();
              mov = movimientoEccBo.save(mov);
              for (Tmct0movimientooperacionnuevaecc movn : movsn) {
                if (movn.getTmct0desgloseclitit() != null) {
                  movn.setNudesglose(movn.getTmct0desgloseclitit().getNudesglose());
                }
              }// fin for movn
              movimientoOpNuevaBo.save(movsn);
              mov.getTmct0movimientooperacionnuevaeccs().addAll(movsn);
            }

            dc.getTmct0desgloseclitits().addAll(dcts);
            dc.getTmct0DesgloseCamaraEnvioRt().addAll(rts);
            dc.getTmct0movimientos().addAll(movs);
          }
        }
      }
    }
    LOG.trace("{}::{}  Fin.", TAG, "persistirAlcs");
  }

  /**
   * Setea los campos alc.cdenvliq y alc.corretajepti en funcion de si es
   * routing, cta, miembro y el tipo de envio que acepta el miembro
   * 
   * @param alc
   * @param isRouting
   * @param cta
   * @param compensador
   * @param corretajePtiCompensador
   */
  public void setTipoEnvioCorretaje(Tmct0alc alc, boolean isRouting, boolean isOperacionEspecial, String cta,
      String compensador, Character corretajeAlias, Character corretajePtiCompensador) {

    if (isRouting) {
      alc.setCdenvliq(TiposComision.PARTENON_ROUTING);
      alc.setCorretajePti(' ');
    }
    else if (isOperacionEspecial) {
      alc.setCdenvliq(" ");
      alc.setCorretajePti(' ');
    }
    else {
      alc.setCdenvliq(TiposComision.CORRETAJE_PTI);
    }

    if (StringUtils.isNotBlank(cta)) {
      alc.setCdenvliq(TiposComision.CLEARING);
    }
    else if (StringUtils.isNotBlank(alc.getCdenvliq())
        && !alc.getCdenvliq().trim().equals(TiposComision.PARTENON_ROUTING)) {
      if (StringUtils.isNotBlank(compensador)) {
        if (corretajePtiCompensador == null || TiposEnvioComisionPti.NOT_ALLOWED == corretajePtiCompensador) {
          alc.setCdenvliq(TiposComision.FACTURACION);
        }
        else {
          if (TiposEnvioComisionPti.ALL_METHOD_ALLOWED == corretajePtiCompensador) {
            if (corretajeAlias != null && corretajeAlias != ' ') {
              alc.setCorretajePti(corretajeAlias);
            }
            else {
              alc.setCorretajePti(TiposEnvioComisionPti.MESSAGE_PTI);
            }
          }
          else {
            alc.setCorretajePti(corretajePtiCompensador);
            if (corretajeAlias != null && corretajeAlias != corretajePtiCompensador) {
              LOG.warn("[setTipoEnvioCorretaje] Corretaje compensador: {} corretaje alias: {}",
                  corretajePtiCompensador, corretajeAlias);
            }
          }
        }
      }
    }
  }

  @Transactional
  public void ponerEstadoAsignacion(Tmct0alc alc, int estadoAsignacion, String auditUser) {
    Tmct0alo alo = alc.getTmct0alo();
    Tmct0bok bok = alo.getTmct0bok();
    List<Tmct0desglosecamara> listDcs = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listDcs) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && dc.getTmct0estadoByCdestadoasig().getIdestado() != estadoAsignacion) {
        dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
        dc.getTmct0estadoByCdestadoasig().setIdestado(estadoAsignacion);
        dc.setAuditFechaCambio(new Date());
        dc.setAuditUser(auditUser);
      }
    }
    if (alc.getCdestadoasig() != estadoAsignacion) {
      alc.setCdestadoasig(estadoAsignacion);
      alc.setFhaudit(new Date());
      alc.setCdusuaud(auditUser);
    }
    if (alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
      alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      alo.setFhaudit(new Date());
      alo.setCdusuaud(auditUser);
    }
    if (bok.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      bok.setTmct0estadoByCdestadoasig(new Tmct0estado());
      bok.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      bok.setFhaudit(new Date());
      bok.setCdusuaud(auditUser);
    }
  }

  public String getAcctatcle(Tmct0alo alo, SettlementData settlementData) {
    String acctatcle = "";
    Tmct0bok bok = alo.getTmct0bok();

    // SI ES DIRECTIVOS
    if (bok.getTipexp() != null && 'D' == bok.getTipexp()) {
      final String ccv = alo.getCcv().trim();
      acctatcle = ccv;
    }
    else if (StringUtils.isNotBlank(alo.getReftitular())) {
      acctatcle = alo.getReftitular().trim();
    }
    else if (StringUtils.isNotBlank(alo.getCcv())) {
      acctatcle = alo.getCcv().trim();
    }
    else {
      acctatcle = settlementData.getTmct0ilq().getAcctatcle().trim();
    }
    return acctatcle;
  }

  public String getCdrefban(Tmct0alo alo, SettlementData settlementData) {
    String cdrefban = "";
    Tmct0bok bok = alo.getTmct0bok();
    Tmct0ord ord = bok.getTmct0ord();
    boolean isRouting = ordBo.isRouting(ord);
    boolean isSd = ordBo.isScriptDividend(ord);
    if (isRouting || isSd) {
      // String partenonClearer = null;
      String ccv = "";
      if (StringUtils.isNotBlank(alo.getReftitular())) {
        ccv = alo.getReftitular().trim();
      }
      else if (StringUtils.isNotBlank(ord.getCcv())) {
        ccv = alo.getTmct0bok().getTmct0ord().getCcv().trim();
      }
      if (ccv.length() == 20) {
        cdrefban = CuentaValoresValidateHelper.crearCcvBanif(ccv);
      }
      else if (ccv.length() == 15) {
        cdrefban = CuentaValoresValidateHelper.crearCcvRoutingSanAndOpen(ccv);
      }
      else {
        cdrefban = ccv;
      }
    }
    else {
      if (StringUtils.isNotBlank(settlementData.getTmct0ilq().getCdrefban())) {
        cdrefban = settlementData.getTmct0ilq().getCdrefban().trim();
      }
    }
    return cdrefban;
  }

  /**
   * Marca la bandera de Canon como calculado para un ALC.
   * @param alc al cual se marcará como calculado.
   * @param cdusuaud usuario logeado en el sistema.
   * @return número de registros modificados, o cero si no se hace acualización
   * alguna.
   */
  @Transactional
  public int bajaCanonCalculado(Tmct0alc alc, String cdusuaud) {
    if (alc.getReferenciaTitular() != null) {
      canonAgrupadoReglaDao.updateImportesaCero(alc.getFeejeliq(), alc.getReferenciaTitular().getCdreftitularpti(),
          new Date(), cdusuaud);
      return dao.updateFlagCanonCalculado(alc.getReferenciaTitular().getIdreferencia(), alc.getFeejeliq(), 'N',
          new Date(), cdusuaud);
    }
    LOG.trace(
        "[setCalculatedFlagCanonCalculadoByReferenciaTitular] La referencia del ALC[{}] es nula, no se actualiza", alc
            .getId().toString());
    return 0;
  }

  @Transactional
  public int setFlagRecalculateCanonByReferenciaTitular(Tmct0alc alc, Tmct0referenciatitular refTitular, String cdusuaud) {
    final Date fecha;
    final String titular;
    final int mod;
    final boolean solicitado;

    if (refTitular != null) {
      alc.setCanonCalculado('N');
      alc.setFhaudit(new Date());
      alc.setCdusuaud(cdusuaud);
      fecha = alc.getFeejeliq();
      titular = refTitular.getCdreftitularpti();
      String msg = String.format("Alc: '%s' Solicitud recalculo canon para referencia: '%s'.", alc.getId()
          .getNucnfliq(), titular.trim());
      LOG.debug("[setFlagRecalculateCanonByReferenciaTitular] {} - {}", alc.getId(), msg);
      tmct0logBo.insertarRegistro(alc.getId().getNuorden(), alc.getId().getNbooking(), msg, "", cdusuaud);

      mod = 1;
      // canonAgrupadoReglaDao.updateImportesaCero(fecha, titular, new Date(),
      // cdusuaud);
      solicitado = canonSolicitaRecalculoBo.agregaSolicitud(fecha, refTitular.getIdreferencia(), cdusuaud);
      if (!solicitado) {
        LOG.trace("[setFlagRecalculateCanonByReferenciaTitular] Ya se había solicitado recálculo para ALC [{}]", alc
            .getId().toString());
      }
      return mod;
    }

    LOG.trace(
        "[setFlagRecalculateCanonByReferenciaTitular] La referencia del ALC[{}] es nula, no se solicita recálculo", alc
            .getId().toString());
    return 0;
  }

  public boolean isEnviado1617(Tmct0alc alc) {
    Tmct0infocompensacion ic;
    boolean res = false;
    List<String> listAliasNotSent = new ArrayList<String>(Arrays.asList(this.notInAliass1617.split(",")));
    if (!listAliasNotSent.contains(alc.getTmct0alo().getTmct0bok().getCdalias())) {
      if (!isEnviado1819(alc)) {
        String cdrefban = this.cdrefbanStart1617.replace("%", ".*");
        if (alc.getCdrefban().matches(cdrefban) && alc.getCdrefban().trim().length() == 20) {
          List<String> listCompensadores = new ArrayList<String>(Arrays.asList(this.compensadores1617.split(",")));
          List<String> listRefGiveUp = new ArrayList<String>(Arrays.asList(this.referencias1617.split(",")));

          List<Tmct0desglosecamara> listds = alc.getTmct0desglosecamaras();
          for (Tmct0desglosecamara dc : listds) {
            if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              ic = dc.getTmct0infocompensacion();
              if (ic.getCdmiembrocompensador() != null
                  && listCompensadores.contains(ic.getCdmiembrocompensador().trim()) && ic.getCdrefasignacion() != null
                  && listRefGiveUp.contains(ic.getCdmiembrocompensador().trim())) {
                if (alc.getTmct0alo().getTmct0bok().getCdindimp() == 'S') {
                  res = true;
                  break;
                }
              }
            }
          }
        }
      }
    }
    return res;
  }

  public boolean isEnviado1819(Tmct0alc alc) {
    boolean res = alc.getCdenvmor() != null && alc.getCdenvmor().trim().equals("S");
    return res;
  }

  public boolean isEnviadoPti(Tmct0alc alc) {
    boolean res = false;
    List<Tmct0desglosecamara> listds = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listds) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId()
          && alc.getCdenvliq().trim().equals(TiposComision.CORRETAJE_PTI)
          && alc.getCorretajePti() == TiposEnvioComisionPti.MESSAGE_PTI) {
        res = true;
        break;
      }
    }
    return res;
  }

  public boolean isEnviadoClearing(Tmct0alc alc) {
    boolean res = alc.getEstadoentrec().getIdestado() > EstadosEnumerados.CLEARING.PDTE_ENVIAR_ANULACION_PARCIAL
        .getId();

    List<Tmct0desglosecamara> listds = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listds) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId()
          && dc.getTmct0estadoByCdestadoentrec().getIdestado() > EstadosEnumerados.CLEARING.PDTE_ENVIAR_ANULACION_PARCIAL
              .getId()) {
        res = true;
        break;
      }
    }

    return res;
  }

  public boolean isFacturado(Tmct0alc alc) {
    boolean res = false;
    List<Tmct0desglosecamara> listds = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listds) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && dc.getTmct0estadoByCdestadocont().getIdestado() > EstadosEnumerados.CONTABILIDAD.FACTURADA_PDTE_COBRO
              .getId()) {
        res = true;
        break;
      }
    }
    return res;
  }

  public boolean isCobrado(Tmct0alc alc) {
    boolean res = false;
    List<Tmct0desglosecamara> listds = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listds) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && dc.getTmct0estadoByCdestadocont().getIdestado() > EstadosEnumerados.CONTABILIDAD.PARCIALMENTE_COBRADA
              .getId()) {
        res = true;
        break;
      }
    }
    return res;
  }

  public boolean isDevengada(Tmct0alc alc) {
    boolean res = false;
    List<Tmct0desglosecamara> listds = alc.getTmct0desglosecamaras();
    for (Tmct0desglosecamara dc : listds) {
      if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()
          && dc.getTmct0estadoByCdestadocont().getIdestado() > EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_COBRO
              .getId()) {
        res = true;
        break;
      }
    }
    return res;
  }

  public boolean puedoModificarNetoFinal(Tmct0alc alc) {
    return !ordBo.isRouting(alc.getTmct0alo().getTmct0bok().getTmct0ord())
        && !ordBo.isScriptDividend(alc.getTmct0alo().getTmct0bok().getTmct0ord()) && !isEnviado1617(alc)
        && !isEnviado1819(alc) && !isEnviadoPti(alc) && !isEnviadoClearing(alc) && !isFacturado(alc) && !isCobrado(alc);
  }

  public List<Date> findAllDistinctFetradeByCanonCalculadoAndConReferenciaTitular(char canonCalculado) {
    return dao.findAllDistinctFetradeByCanonCalculadoAndConReferenciaTitular(canonCalculado);
  }

  public List<Object[]> findAllDistinctFetradeCdisinCdtpoperByFeejeliqAndCanonCalculadoAndConReferenciaTitularAndCasado(
      Date feejeliq, char canonCalculado, char isscrdiv) {
    return dao.findAllDistinctFetradeCdisinCdtpoperByFeejeliqAndCanonCalculadoAndConReferenciaTitularAndCasado(
        feejeliq, canonCalculado, isscrdiv);

  }

  public List<Tmct0bokId> findAllTmct0bokIdByCanonCalculadoAndBaja(char canonCalculado, char isscrdiv) {
    return dao.findAllTmct0bokIdByCanonCalculadoAndBaja(canonCalculado, isscrdiv);
  }

  public List<Tmct0alcId> findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndBaja(Tmct0bokId bookId, char canonCalculado) {
    return dao.findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndBaja(bookId, canonCalculado);
  }

  public List<Tmct0bokId> findAllTmct0bokIdByFechaAndCdisinAndSentidoAndCasadoSinCanonCalculadoConReferenciaTitular(
      Date fetrade, String cdisin, char sentido, char canonCalculado, char isscrdiv) {
    Tmct0bokId bokId;
    List<Tmct0bokId> listBookings = new ArrayList<Tmct0bokId>();
    Collection<Tmct0bokId> collectionBookings = new HashSet<Tmct0bokId>();
    List<Tmct0aloId> listAlos = dao
        .findAllTmct0AloIdByFechaAndCdisinAndSentidoAndCasadoSinCanonCalculadoConReferenciaTitular(fetrade, cdisin,
            sentido, canonCalculado, isscrdiv);
    for (Tmct0aloId aloId : listAlos) {
      bokId = new Tmct0bokId(aloId.getNbooking(), aloId.getNuorden());
      if (collectionBookings.add(bokId)) {
        listBookings.add(bokId);
      }
    }
    collectionBookings.clear();
    return listBookings;
  }

  public List<Tmct0alcId> findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndConReferenciaTitular(Tmct0bokId bookId,
      char canonCalculado) {
    return dao.findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndConReferenciaTitular(bookId, canonCalculado);
  }

  @Transactional
  public Integer updateFlagCanonCalculado(long idReferenciaTitular, Date fecontratacion, char calculado, String cdusuaud) {
    return dao.updateFlagCanonCalculado(idReferenciaTitular, fecontratacion, calculado, new Date(), cdusuaud);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Integer updateFlagCanonCalculadoByAlcIdNewTransaction(Tmct0alcId alcId, char calculado, String cdusuaud) {
    return dao.updateFlagCanonCalculadoByAlcId(alcId, calculado, new Date(), cdusuaud);
  }

  public void setProvisionalNudesgloses(Tmct0alc alc) {
    int i = 1;
    for (Tmct0desglosecamara dc : alc.getTmct0desglosecamaras()) {
      for (Tmct0desgloseclitit dt : dc.getTmct0desgloseclitits()) {
        dt.setNudesglose((long) i++);
      }
    }
  }

  public void removeProvisionalNudesgloses(Tmct0alc alc) {
    for (Tmct0desglosecamara dc : alc.getTmct0desglosecamaras()) {
      for (Tmct0desgloseclitit dt : dc.getTmct0desgloseclitits()) {
        dt.setNudesglose(null);
      }
    }
  }
} // Tmct0alcBo
