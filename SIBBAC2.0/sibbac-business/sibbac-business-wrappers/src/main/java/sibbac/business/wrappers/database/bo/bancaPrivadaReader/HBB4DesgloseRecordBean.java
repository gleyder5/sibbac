package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;

public class HBB4DesgloseRecordBean extends HBB4TipoRegistroRecordBean {

  /**
	 * 
	 */
  private static final long serialVersionUID = 3223274589183334345L;

  private String entidadLiquidadora;
  private Character fillerPostEntidadLiquidadora1;
  private Character fillerPostEntidadLiquidadora2;
  private Character fillerPostEntidadLiquidadora3;
  private String isin;
  private BigDecimal titulos;
  private BigDecimal precio;
  private String fillerPostPrecio;
  private String nuorden;

  public String getEntidadLiquidadora() {
    return entidadLiquidadora;
  }

  public Character getFillerPostEntidadLiquidadora1() {
    return fillerPostEntidadLiquidadora1;
  }

  public Character getFillerPostEntidadLiquidadora2() {
    return fillerPostEntidadLiquidadora2;
  }

  public Character getFillerPostEntidadLiquidadora3() {
    return fillerPostEntidadLiquidadora3;
  }

  public String getIsin() {
    return isin;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public void setEntidadLiquidadora(String entidadLiquidadora) {
    this.entidadLiquidadora = entidadLiquidadora;
  }

  public void setFillerPostEntidadLiquidadora1(Character fillerPostEntidadLiquidadora1) {
    this.fillerPostEntidadLiquidadora1 = fillerPostEntidadLiquidadora1;
  }

  public void setFillerPostEntidadLiquidadora2(Character fillerPostEntidadLiquidadora2) {
    this.fillerPostEntidadLiquidadora2 = fillerPostEntidadLiquidadora2;
  }

  public void setFillerPostEntidadLiquidadora3(Character fillerPostEntidadLiquidadora3) {
    this.fillerPostEntidadLiquidadora3 = fillerPostEntidadLiquidadora3;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public String getFillerPostPrecio() {
    return fillerPostPrecio;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setFillerPostPrecio(String fillerPostPrecio) {
    this.fillerPostPrecio = fillerPostPrecio;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

}
