package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.common.utils.FormatDataUtils;

public class HBB5FieldSetMapper implements FieldSetMapper<HBB5RecordBean> {
	public final static String delimiter = ";";

	public enum HBB5Fields {
		REFEXTERNA("refExterna", 16), FECHACUADRE("fechaCuadre",
				8), CLAVECALLE("claveCalle", 2), DIRECCION("direccion", 40), MUNICIPIO(
				"municipio", 40), PROVINCIA("provincia", 30), CODIGOPOSTAL(
				"codigoPostal", 5), NACIONALEXTRANJERO("nacionalExtranjero", 1), PAIS(
				"pais", 3), TIPOENVIO("tipoEnvio", 1), FILLER(
				"filler", 100);

		private final String text;
		private final int length;

		/**
		 * @param text
		 */
		private HBB5Fields(final String text, int length) {
			this.text = text;
			this.length = length;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return text;
		}

		public static String[] getFieldNames() {
			HBB5Fields[] fields = HBB5Fields.values();
			final String[] namesArray = new String[fields.length];
			for (int i = 0; i < fields.length; i++) {
				namesArray[i] = fields[i].text;
			}
			return namesArray;
		}

		public static int[] getHBB5Lengths() {
			HBB5Fields[] fields = HBB5Fields.values();
			final int[] lengthsArray = new int[fields.length];
			for (int i = 0; i < fields.length; i++) {
				lengthsArray[i] = fields[i].length;
			}
			return lengthsArray;
		}
	}

	@Override
	public HBB5RecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
		HBB5RecordBean HBB5bean = new HBB5RecordBean();
		HBB5bean.setRefExterna(fieldSet.readString(HBB5Fields.REFEXTERNA.text));
		HBB5bean.setFechaCuadre(fieldSet.readDate(HBB5Fields.FECHACUADRE.text,FormatDataUtils.DATE_FORMAT));
		HBB5bean.setClaveCalle(fieldSet.readString(HBB5Fields.CLAVECALLE.text));
		HBB5bean.setDireccion(fieldSet.readString(HBB5Fields.DIRECCION.text));
		HBB5bean.setMunicipio(fieldSet.readString(HBB5Fields.MUNICIPIO.text));
		HBB5bean.setProvincia(fieldSet.readString(HBB5Fields.PROVINCIA.text));
		HBB5bean.setCodigoPostal(fieldSet.readBigDecimal(HBB5Fields.CODIGOPOSTAL.text));
		HBB5bean.setNacionalExtranjero(fieldSet.readString(HBB5Fields.NACIONALEXTRANJERO.text));
		HBB5bean.setPais(fieldSet.readBigDecimal(HBB5Fields.PAIS.text));
		HBB5bean.setTipoEnvio(fieldSet.readBigDecimal(HBB5Fields.TIPOENVIO.text));
		HBB5bean.setFiller(fieldSet.readString(HBB5Fields.FILLER.text));
		return HBB5bean;
	}

}