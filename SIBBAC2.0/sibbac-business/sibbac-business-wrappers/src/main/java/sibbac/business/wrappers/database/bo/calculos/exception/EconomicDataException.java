package sibbac.business.wrappers.database.bo.calculos.exception;

/**
 * Se lanza para indicar que ha ocurrido algún error durante los cáculos económicos.
 * 
 * @author XI316153
 * @version 1.0
 * @since SIBBAC2.0
 */
public class EconomicDataException extends Exception {

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTANTES ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 8055792758324777576L;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Constructor I. Genera una nueva excepción con el mensaje especificado.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   */
  public EconomicDataException(String mensaje) {
    super(mensaje);
  } // EconomicDataException

  /**
   * Constructor II. Genera una nueva excepción con el mensaje especificado y la excepción producida.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   * @param causa Causa por la que se ha lanzado la excepción.
   */
  public EconomicDataException(String mensaje, Throwable causa) {
    super(mensaje, causa);
  } // EconomicDataException

} // EconomicDataException