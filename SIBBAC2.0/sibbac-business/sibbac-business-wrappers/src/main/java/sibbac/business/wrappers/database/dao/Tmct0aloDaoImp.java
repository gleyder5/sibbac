package sibbac.business.wrappers.database.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.DTOUtils;
import sibbac.business.wrappers.database.dto.TransaccionesDTO;

@Repository
public class Tmct0aloDaoImp {
    
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0aloDaoImp.class);
    
  private static final String[] queryUpdateCdestadocont = { 
          "update tmct0alo set cdestadocont=", 
          " where nucnfclt='", 
          "' and nbooking='", 
          "' and nuorden='", "'"
  };

  @PersistenceContext
  private EntityManager em;
  
  @Value("${transacciones.informe.open.alias}")
  private String openAlias;

  /**
   * Establece los parametros en la query especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>List<Object[]> - </code>Lista de resultados devueltos por la query.
   * @author XI316153
   */
  @SuppressWarnings("unchecked")
  @Transactional
  public List<Object[]> prepareAndExecuteNativeQuery(String sqlQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.getResultList();
  } // prepareAndExecuteQuery

  /**
   * Establece los parametros en la query de actualización especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>Integer - </code>Número de registros actualizados.
   * @author XI316153
   */
  @Modifying
  @Transactional
  public Integer prepareAndExecuteNativeUpdate(String sqlUpdateQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlUpdateQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.executeUpdate();
  } // prepareAndExecuteQuery

  public int updateCdestadocont(int estadocont, String nucnfclt, String nbooking, String nuorden) {
    StringBuffer sB = new StringBuffer(queryUpdateCdestadocont[0]).append(estadocont)
                                                                  .append(queryUpdateCdestadocont[1]).append(nucnfclt)
                                                                  .append(queryUpdateCdestadocont[2]).append(nbooking)
                                                                  .append(queryUpdateCdestadocont[3]).append(nuorden)
                                                                  .append(queryUpdateCdestadocont[4]);
    Query query = em.createNativeQuery(sB.toString());
    return query.executeUpdate();
  }

  public List<TransaccionesDTO> findByFeTradeAndCdmercadAndImgatdsv(Date fcontratacionDesde,
                                                                    Date fcontratacionA,
                                                                    String mercado) {
    final List<?> resultList;
    final List<TransaccionesDTO> transaccionesList;
    final StringBuilder queryBuilder, whereBuilder;
    final Query query;
    
    queryBuilder = new StringBuilder()
            .append("SELECT DISTINCT A.NUORDEN, A.NBOOKING,A.NUCNFCLT,K.CDALIAS,B.FETRADE,A.CDISIN,")
            .append("A.NUTITCLI AS NUMERO_TITULAR, A.EUTOTBRU AS EFECTIVO, A.IMGATDVS AS FTT, C.DESCRALI, A.FEVALOR, A.NBVALORS, A.EUTOTNET, B.CDMERCAD ")
            .append("FROM BSNBPSQL.TMCT0ALO A, BSNBPSQL.TMCT0BOK K  LEFT JOIN BSNBPSQL.TMCT0ALI C ON K.CDALIAS = C.CDALIASS  AND FHFINAL = 99991231, BSNBPSQL.TMCT0CTD B ");
    whereBuilder = new StringBuilder()
            .append("WHERE A.NUORDEN = B.NUORDEN AND A.NBOOKING=B.NBOOKING AND A.NBOOKING = K.NBOOKING AND ")
            .append("A.IMGATDVS != 0 AND B.FETRADE >= :fcontratacionDesde ")
            .append("AND ( (B.CDMERCAD = 'MIL' AND A.CDALIAS != :openAlias) OR (B.CDMERCAD != 'MIL') ) ");
    if (fcontratacionA != null) {
        whereBuilder.append(" AND B.FETRADE <= :fcontratacionA ");
    }
    if (mercado != null) {
        whereBuilder.append(" AND B.CDMERCAD = :mercado ");
    }
    whereBuilder.append("ORDER BY FETRADE");
    queryBuilder.append(whereBuilder);
    query = em.createNativeQuery(queryBuilder.toString());
    setParameters(query, fcontratacionDesde, fcontratacionA, mercado);
    resultList = query.getResultList();
    transaccionesList = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TransaccionesDTO transaccion = DTOUtils.convertObjectToTransaccionesDTO((Object[]) obj);
        transaccionesList.add(transaccion);
      }
    }
    return transaccionesList;
  }

  public List<TransaccionesDTO> findByFeTradeAndCdmercadAndImgatdsvSinOpen(Date fcontratacionDesde,
                                                                           Date fcontratacionA,
                                                                           String mercado) {

    final StringBuilder select = new StringBuilder(
                                                   "SELECT DISTINCT A.NUORDEN, A.NBOOKING,A.NUCNFCLT,K.CDALIAS,B.FETRADE,A.CDISIN,"
                                                       + "A.NUTITCLI AS NUMERO_TITULAR, A.EUTOTBRU AS EFECTIVO, A.IMGATDVS AS FTT, C.DESCRALI, A.FEVALOR, A.NBVALORS, A.EUTOTNET, B.CDMERCAD ");

    final StringBuilder from = new StringBuilder(
                                                 "FROM BSNBPSQL.TMCT0ALO A, BSNBPSQL.TMCT0BOK K LEFT JOIN BSNBPSQL.TMCT0ALI C ON K.CDALIAS = C.CDALIASS  AND FHFINAL = 99991231, BSNBPSQL.TMCT0CTD B ");
    final StringBuilder where = new StringBuilder(
                                                  "WHERE A.NUORDEN = B.NUORDEN AND A.NBOOKING=B.NBOOKING AND A.NBOOKING = K.NBOOKING AND "
                                                      + "A.IMGATDVS = 0 AND A.CDALIAS != :openAlias "
                                                      + "AND B.FETRADE >= :fcontratacionDesde ");

    if (fcontratacionA != null) {
      where.append(" AND B.FETRADE <= :fcontratacionA ");
    }

    if (mercado != null) {
      where.append(" AND B.CDMERCAD = :mercado ");
    }

    where.append("ORDER BY B.FETRADE");

    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());

    setParameters(query, fcontratacionDesde, fcontratacionA, mercado);

    final List<?> resultList = query.getResultList();
    final List<TransaccionesDTO> TransaccionesList = new ArrayList<TransaccionesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TransaccionesDTO transaccion = DTOUtils.convertObjectToTransaccionesDTO((Object[]) obj);

        TransaccionesList.add(transaccion);
      }
    }

    return TransaccionesList;

  }

  public List<TransaccionesDTO> findByFeTradeAndCdmercadAndImgatdsvOpen(Date fcontratacionDesde,
                                                                        Date fcontratacionA,
                                                                        String mercado) {
    LOG.debug("[findByFeTradeAndCdmercadAndImgatdsvOpen] Inicio...");  
    final StringBuilder queryBuilder = new StringBuilder()
            .append("SELECT DISTINCT A.NUORDEN, A.NBOOKING,A.NUCNFCLT,K.CDALIAS,B.FETRADE,A.CDISIN, ")
            .append("A.NUTITCLI AS NUMERO_TITULAR, A.EUTOTBRU AS EFECTIVO, A.IMGATDVS AS FTT, C.DESCRALI, A.FEVALOR, A.NBVALORS, A.EUTOTNET, B.CDMERCAD ")
            .append("FROM BSNBPSQL.TMCT0ALO A, BSNBPSQL.TMCT0BOK K LEFT JOIN BSNBPSQL.TMCT0ALI C ON K.CDALIAS = C.CDALIASS  AND FHFINAL = 99991231, BSNBPSQL.TMCT0CTD B ")
            .append("WHERE A.NUORDEN = B.NUORDEN AND A.NBOOKING=B.NBOOKING AND A.NBOOKING = K.NBOOKING AND ")
            .append("A.CDALIAS = :openAlias AND B.FETRADE >= :fcontratacionDesde ");
    if (fcontratacionA != null) {
        queryBuilder.append(" AND B.FETRADE <= :fcontratacionA ");
    }
    if (mercado != null) {
        queryBuilder.append(" AND B.CDMERCAD = :mercado ");
    }
    queryBuilder.append("ORDER BY B.FETRADE");
    final String stringQuery = queryBuilder.toString();
    LOG.debug("[findByFeTradeAndCdmercadAndImgatdsvOpen] consulta generada: {}", stringQuery);
    final Query query = em.createNativeQuery(stringQuery);
    setParameters(query, fcontratacionDesde, fcontratacionA, mercado);
    final List<?> resultList = query.getResultList();
    LOG.debug("[findByFeTradeAndCdmercadAndImgatdsvOpen] consulta terminada, {} resultados", 
            resultList.size());
    final List<TransaccionesDTO> TransaccionesList = new ArrayList<TransaccionesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TransaccionesDTO transaccion = DTOUtils.convertObjectToTransaccionesDTO((Object[]) obj);

        TransaccionesList.add(transaccion);
      }
    }
    LOG.debug("[findByFeTradeAndCdmercadAndImgatdsvOpen] Fin.");
    return TransaccionesList;
  }

  private void setParameters(Query query,
                             Date fcontratacionDesde,
                             Date fcontratacionA,
                             String mercado) {

    if (fcontratacionDesde != null) {
      query.setParameter("fcontratacionDesde", fcontratacionDesde);
    }

    if (fcontratacionA != null) {
      query.setParameter("fcontratacionA", fcontratacionA);
    }

    if (mercado != null) {
      query.setParameter("mercado", mercado);
    }
    query.setParameter("openAlias", openAlias);

  }

} // Tmct0aloDaoImpl
