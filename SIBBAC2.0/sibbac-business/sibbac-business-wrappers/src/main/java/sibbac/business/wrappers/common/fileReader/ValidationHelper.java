package sibbac.business.wrappers.common.fileReader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean.TipoPersona;
import sibbac.business.wrappers.database.model.RecordBean.IndicadorNacionalidad;
import sibbac.business.wrappers.database.model.RecordBean.TipoDocumento;
import sibbac.business.wrappers.database.model.RecordBean.TipoTitular;

public class ValidationHelper {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(ValidationHelper.class);

  public enum CodError {
    TITULAR("COD001"), DIRECCION("COD002"), CIUDAD("COD003"), PROVINCIA("COD004"), PAIS("COD005"), CODPOSTAL("COD006"), TIPPERS(
        "COD007"), NOMBRE("COD008"), TIPTITU("COD009"), NACIONALIDAD("COD010"), NIFMENOR("COD011"), MENORSINREPRESENTANTE(
        "COD012"), TITULARIDADNOT("COD013"), NIFMENORREPRESENTANTE("COD014"), CIFNIF("COD015"), TIPODOCUMENTO("COD016"), BIC(
        "COD017"), TIPONAC("COD018"), NOALC("COD019"), ORD("COD020"), ALOS("COD021"), ERROROTROTIT("COD022"), NOMBRE_DOS_APELLIDOS(
        "COD023"), NOMBRE_UN_APELLIDO("COD024"), INFORMAR_RAZON_SOCIAL("COD025"), PERSONA_JURIDICA_CON_NBCLIENT(
        "COD026"), INFORMAR_DOCUMENTO("COD027"), INFORMAR_TIPO_DOCUMENTO("COD028"), INFORMAR_TIPPERS("COD029"), INFORMAR_IND_NAC(
        "COD030"), INFORMAR_NACIONALIDAD("COD031"), INFORMAR_TIPO_TITULAR("COD032"), INFORMAR_COD_POSTAL("COD033"), INFORMAR_PAIS_RES(
        "COD034"), PAIS_RES_INCORRECTO("COD035"), DOMICILIO_DEMASIADO_CORTO("COD036"), FORMATO_NIE("COD037"), NIF_COMIENZO(
        "COD038"), NIF_FORMATO("COD039"), NIE_COMIENZO("COD040"), NIE_FORMATO("COD041"), LONG_NIF_NIE_CIF("COD042"), LONG_BIC(
        "COD043"), LONG_OTROS("COD044"), CIF_COMIENZO("COD045"), FORMATO_CIF("COD046"), CONTROL_CIF("COD047"), BLANCO_BIC(
        "COD048"), BIC_FORMATO_LETRAS("COD049"), BIC_INEXISTENTE("COD050"), BIC_FORMATO_PAISES("COD051"), NIF_BIC_INFORMADOS(
        "COD052"), BIC_FICTICIO("COD053"), INCOHERENCIA_NACIONAL("COD054"), INCOHERENCIA_EXTRANJERO("COD055"), IDENTIFICADOR_INCORRECTO_FIS(
        "COD056"), IDENTIFICADOR_INCORRECTO_JUR("COD057"), NIF_TIPO_M("COD058"), NIF_TIPO_N_W("COD059"), NIF_EXTRANJERO_NO_TIPO_M(
        "COD060"), CIF_EXTRANJERO_NO_TIPO_NW("COD061"), REPRESENTANTE_BIC("COD062"), REPRESENTANTE_PARTICIP("COD063"), COHERENCIA_CP_CODIGO_PROVINCIA(
        "COD064"), COHERENCIA_CP_NOMBRE_PROVINCIA("COD065"), IDENTIFICADORES_DUPLICADOS("COD066"), NO_TITULAR_REAL(
        "COD067"), PORCENTAJE_ERRONEO("COD068"), TITULAR_REPETIDO("COD069"), DIRECCION_CORTA("COD070"), CCV_INCORRECTO(
        "COD071"), ASCII_NOMBRE("COD072"), ASCII_1AP("COD073"), ASCII_2AP("COD074"), ASCII_JUR("COD075"), TIPO_DOCUMENTO_NACIONAL_ERRONEO(
        "COD076"), TITULARES_ACEPTADOS("COD077"), NUDO_PROPIETARIO_SIN_USUFRUCTUARIO("COD078"), USUFRUCTUARIO_SIN_NUDO_PROPIETARIO(
        "COD079"), DISTINTAS_CCV("COD080");

    public final String text;

    /**
     * @param text
     */
    private CodError(final String text) {
      this.text = text;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      CodError[] fields = CodError.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static CodError getCodError(String letter) {
      CodError[] fields = CodError.values();
      CodError indNac = null;
      for (int i = 0; i < fields.length; i++) {
        if (fields[i].text.equals(letter)) {
          indNac = fields[i];
          break;
        }
      }
      return indNac;
    }

  }

  public static boolean validateWhiteSpaces(String fieldValue) {
    if (StringUtils.trimToEmpty(fieldValue).equals("")) {
      return false;
    }
    return true;
  }

  public static boolean validateWhiteSpacesOrZeroes(String fieldName, String fieldValue) {
    if (StringUtils.trimToEmpty(fieldValue).equals("")) {
      return false;
    }
    else if (StringHelper.isZeroes(fieldName)) {
      return false;
    }
    return true;
  }

  // /**
  // *
  // * @param fieldName
  // * @param fieldValue
  // * @param errorSet
  // * @return true if it is valid and false if it is not valid
  // */
  public static boolean validateZero(BigDecimal fieldValue) {
    if (fieldValue == null || fieldValue.compareTo(BigDecimal.ZERO) == 0) {
      return false;
    }
    return true;
  }

  private static boolean hasInvalidAscii(String validAscii, String stringToCheck) {
    for (Character character : stringToCheck.toCharArray()) {
      if (validAscii.indexOf(character) == -1) {
        return true;
      }
    }
    return false;
  }

  private static Character obtenerDigitoControl(String cif) {
    int resultado1 = Integer.parseInt(cif.substring(2, 3)) + Integer.parseInt(cif.substring(4, 5))
        + Integer.parseInt(cif.substring(6, 7));
    int resultado2 = 0;
    int resultado3 = 0;
    Character digitoControl = ' ';
    List<Integer> numerosParaMultiplicar = new ArrayList<Integer>();
    numerosParaMultiplicar.add(Integer.parseInt(cif.substring(1, 2)));
    numerosParaMultiplicar.add(Integer.parseInt(cif.substring(3, 4)));
    numerosParaMultiplicar.add(Integer.parseInt(cif.substring(5, 6)));
    numerosParaMultiplicar.add(Integer.parseInt(cif.substring(7, 8)));

    for (Integer numero : numerosParaMultiplicar) {
      Integer resultado = numero * 2;
      for (Character digito : resultado.toString().toCharArray()) {
        resultado2 += Character.getNumericValue(digito);
      }
    }
    resultado3 = resultado1 + resultado2;
    Integer calculado = 10 - resultado3 % 10;
    if (calculado == 10) {
      calculado = 0;
    }

    if ("ABCDEFGHJUV".indexOf(cif.substring(0, 1)) != -1) {
      digitoControl = calculado.toString().charAt(0);
    }
    else if ("NWPQSR".indexOf(cif.substring(0, 1)) != -1) {
      digitoControl = "JABCDEFGHI".charAt(calculado);
    }
    return digitoControl;
  }

  private static boolean coincideRestoConLetra(Integer numeroDni, String letraFinal) {
    String[] arrayLetras = { "T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V",
        "H", "L", "C", "K", "E" };
    int resto = numeroDni % 23;
    if (arrayLetras[resto].equals(letraFinal)) {
      return true;
    }
    return false;
  }

  public static List<String> validarErroresAscii(String validAscii, PartenonRecordBean partenonRecordBean) {
    List<String> errCodes = new ArrayList<String>();
    if (TipoPersona.FISICA.getTipoPersona().equals(partenonRecordBean.getTipoPersona())) {
      if (hasInvalidAscii(validAscii, partenonRecordBean.getNbclient())) {
        errCodes.add(CodError.ASCII_NOMBRE.text);
      }
      if (hasInvalidAscii(validAscii, partenonRecordBean.getNbclient1())) {
        errCodes.add(CodError.ASCII_1AP.text);
      }
      if (hasInvalidAscii(validAscii, partenonRecordBean.getNbclient2())) {
        errCodes.add(CodError.ASCII_2AP.text);
      }
    }
    else if (TipoPersona.JURIDICA.getTipoPersona().equals(partenonRecordBean.getTipoPersona())
        && (hasInvalidAscii(validAscii, partenonRecordBean.getNbclient1()) || hasInvalidAscii(validAscii,
            partenonRecordBean.getNbclient2()))) {
      errCodes.add(CodError.ASCII_JUR.text);
    }
    return errCodes;
  }

  public static List<String> comprobarMalInformados(PartenonRecordBean bean) {
    List<String> errCodes = new ArrayList<String>();

    comprobarTipoDocumento(bean, errCodes);

    comprobarTipoPersona(bean, errCodes);

    comprobarIndicadorNacionalidad(bean, errCodes);

    comprobarTipoTitular(bean, errCodes);

    comprobarCodigoPostal(bean, errCodes);

    comprobarLongitudDocumentos(bean, errCodes);

    comprobarTipoNif(bean, errCodes);

    comprobarFormatoNif(bean, errCodes);

    comprobarFormatoNie(bean, errCodes);

    comprobarDigitoControlNifNie(bean, errCodes);

    comprobarCif(bean, errCodes);

    comprobarBic(bean, errCodes);

    comprobarCoherenciaIdentificadores(bean, errCodes);

    comprobarNoBicFicticiosIberclear(bean, errCodes);

    comprobarCoherenciaIndNacPaisNac(bean, errCodes);

    comprobarCoherenciaTipoIdentificadorTipoPersona(bean, errCodes);

    comprobarCoherenciaDocumentoTipoPersonaNacionalidad(bean, errCodes);

    comprobarParticularidadesRepresentante(bean, errCodes);

    comprobarLongitudCcv(bean, errCodes);

    comprobarCoherenciaTipoIdentificadorNacional(bean, errCodes);

    return errCodes;
  }

  private static void comprobarParticularidadesRepresentante(PartenonRecordBean bean, List<String> errCodes) {
    // 49- Comprobación de que un representante no tenga como tipo de
    // identificador un BIC.
    if (new Character('R').equals(bean.getTiptitu()) && new Character('B').equals(bean.getTipoDocumento())) {
      errCodes.add(CodError.REPRESENTANTE_BIC.text);
    }

    // 52- Comprobación de no porcentaje de titularidad en representantes.
    if (new Character('R').equals(bean.getTiptitu())
        && (bean.getParticip() != null && bean.getParticip().compareTo(BigDecimal.ZERO) != 0)) {
      errCodes.add(CodError.REPRESENTANTE_PARTICIP.text);
    }
  }

  /**
   * Control adicional a todos los controles que se hacen de titulares, debes
   * controlar que el campo CCV solo puede estar vacío o tener un código
   * numérico de longitud 20, en cualquier otro caso debes dar error por CCV
   * incorrecta
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarLongitudCcv(PartenonRecordBean bean, List<String> errCodes) {
    if (StringUtils.isNotEmpty(bean.getCcv())
        && (!StringUtils.isNumeric(bean.getCcv()) || !esCuentaBancaria(bean.getCcv()))) {
      errCodes.add(CodError.CCV_INCORRECTO.text);
    }
  }

  public static boolean esCuentaBancaria(String ccv) {

    LOG.trace("Cuenta del Banco " + ccv);

    Pattern cuentaPattern = Pattern.compile("\\d{20}");
    Matcher m = cuentaPattern.matcher(ccv);
    if (m.matches()) {
      LOG.trace("cuenta cumple el patrón (20 dígitos)");

      String banco = ccv.substring(0, 8);
      LOG.trace("Banco (con 00) " + banco);

      int suma = Integer.parseInt(banco.substring(0, 1)) * 4;
      suma = suma + Integer.parseInt(banco.substring(1, 2)) * 8;
      suma = suma + Integer.parseInt(banco.substring(2, 3)) * 5;
      suma = suma + Integer.parseInt(banco.substring(3, 4)) * 10;
      suma = suma + Integer.parseInt(banco.substring(4, 5)) * 9;
      suma = suma + Integer.parseInt(banco.substring(5, 6)) * 7;
      suma = suma + Integer.parseInt(banco.substring(6, 7)) * 3;
      suma = suma + Integer.parseInt(banco.substring(7, 8)) * 6;

      int control = 11 - (suma % 11);
      LOG.trace("control banco después del modulo 11 " + control);
      if (control == 10)
        control = 1;
      else if (control == 11)
        control = 0;

      LOG.trace("control " + control);

      int controlBanco = Integer.parseInt(ccv.substring(8, 9));
      if (controlBanco != control)
        return false;
      LOG.trace("El control del banco está bien");
      String numeroCuenta = ccv.substring(10, 20);
      LOG.trace("cuenta " + numeroCuenta);

      suma = Integer.parseInt(numeroCuenta.substring(0, 1)) * 5;
      suma = suma + Integer.parseInt(numeroCuenta.substring(1, 2)) * 4;
      suma = suma + Integer.parseInt(numeroCuenta.substring(2, 3)) * 3;
      suma = suma + Integer.parseInt(numeroCuenta.substring(3, 4)) * 2;
      suma = suma + Integer.parseInt(numeroCuenta.substring(4, 5)) * 7;
      suma = suma + Integer.parseInt(numeroCuenta.substring(5, 6)) * 6;
      suma = suma + Integer.parseInt(numeroCuenta.substring(6, 7)) * 5;
      suma = suma + Integer.parseInt(numeroCuenta.substring(7, 8)) * 4;
      suma = suma + Integer.parseInt(numeroCuenta.substring(8, 9)) * 3;
      suma = suma + Integer.parseInt(numeroCuenta.substring(9, 10)) * 2;

      control = 11 - (suma % 11);
      LOG.trace("control cuenta después del modulo 11 " + control);
      if (control == 10)
        control = 0;
      else if (control == 11)
        control = 1;

      LOG.trace("control " + control);

      int controlcuenta = Integer.parseInt(ccv.substring(9, 10));
      if (controlcuenta != control)
        return false;
      else
        return true;

    }
    else
      return false;

  }

  private static void comprobarCoherenciaDocumentoTipoPersonaNacionalidad(PartenonRecordBean bean, List<String> errCodes) {
    // 45- Comprobación de tipo de NIF correcto para persona física
    // nacional.
    if (bean.getNifbic() != null && bean.getNifbic().length() > 1) {
      if (new Character('N').equals(bean.getIndNac()) && new Character('F').equals(bean.getTipoPersona())
          && "M".equals(bean.getNifbic().substring(0, 1))) {
        errCodes.add(CodError.NIF_TIPO_M.text);
      }

      // 46- Comprobación de tipo de CIF correcto para persona jurídica
      // nacional.
      if (new Character('N').equals(bean.getIndNac()) && new Character('J').equals(bean.getTipoPersona())
          && new Character('C').equals(bean.getTipoDocumento())
          && ("N".equals(bean.getNifbic().substring(0, 1)) || "W".equals(bean.getNifbic().substring(0, 1)))) {
        errCodes.add(CodError.NIF_TIPO_N_W.text);
      }

      // 47- Comprobación de tipo de NIF correcto para persona física
      // extranjera.
      if (new Character('E').equals(bean.getIndNac()) && new Character('F').equals(bean.getTipoPersona())
          && new Character('N').equals(bean.getTipoDocumento()) && !"M".equals(bean.getNifbic().substring(0, 1))) {
        errCodes.add(CodError.NIF_EXTRANJERO_NO_TIPO_M.text);
      }

      // 48- Comprobación de tipo de CIF correcto para persona jurídica
      // extranjera.
      if (new Character('E').equals(bean.getIndNac()) && new Character('J').equals(bean.getTipoPersona())
          && new Character('C').equals(bean.getTipoDocumento())
          && (!"N".equals(bean.getNifbic().substring(0, 1)) && !"W".equals(bean.getNifbic().substring(0, 1)))) {
        errCodes.add(CodError.CIF_EXTRANJERO_NO_TIPO_NW.text);
      }
    }
  }

  /**
   * 43- Comprobación de tipo de identificador correcto para persona física y
   * jurídica.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarCoherenciaTipoIdentificadorTipoPersona(PartenonRecordBean bean, List<String> errCodes) {
    // Para persona física.
    if (bean.getTipoPersona() != null && new Character('F').equals(bean.getTipoPersona())
        && bean.getTipoDocumento() != null
        && (new Character('C').equals(bean.getTipoDocumento()) || new Character('B').equals(bean.getTipoDocumento()))) {
      errCodes.add(CodError.IDENTIFICADOR_INCORRECTO_FIS.text);
    }

    // Para persona jurídica.
    if (bean.getTipoPersona() != null && new Character('J').equals(bean.getTipoPersona())
        && bean.getTipoDocumento() != null
        && (new Character('N').equals(bean.getTipoDocumento()) || new Character('E').equals(bean.getTipoDocumento()))) {
      errCodes.add(CodError.IDENTIFICADOR_INCORRECTO_JUR.text);
    }
  }

  /**
   * 41 y 42- Comprobación de coherencia entre país de nacionalidad e indicador
   * de nacionalidad
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarCoherenciaIndNacPaisNac(PartenonRecordBean bean, List<String> errCodes) {

    // Para nacionales
    if (bean.getNacionalidad() != null && "724".compareTo(bean.getNacionalidad()) == 0
        && !new Character('N').equals(bean.getIndNac())) {
      errCodes.add(CodError.INCOHERENCIA_NACIONAL.text);
    }

    // Para extranjeros.
    if (bean.getNacionalidad() != null && "724".compareTo(bean.getNacionalidad()) != 0
        && !new Character('E').equals(bean.getIndNac())) {
      errCodes.add(CodError.INCOHERENCIA_EXTRANJERO.text);
    }

  }

  /**
   * 43- Comprobación de titular nacional con identificador OTROS
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarCoherenciaTipoIdentificadorNacional(PartenonRecordBean bean, List<String> errCodes) {

    // Para nacionales
    if (bean.getNacionalidad() != null && "724".compareTo(bean.getNacionalidad()) == 0 || bean.getIndNac() != null
        && new Character('N').equals(bean.getIndNac())) {
      if (bean.getTipoDocumento() != null
          && TipoDocumento.OTROS.getTipoDocumento().compareTo(bean.getTipoDocumento()) == 0) {
        errCodes.add(CodError.TIPO_DOCUMENTO_NACIONAL_ERRONEO.text);
      }

    }
  }

  /**
   * 40- Comprobación de no inclusión de códigos BIC ficticios de Iberclear.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarNoBicFicticiosIberclear(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() != null && TipoDocumento.OTROS.getTipoDocumento().equals(bean.getTipoDocumento())
        && bean.getNifbic() != null && bean.getNifbic().trim().length() >= 4
        && bean.getNifbic().trim().substring(0, 3).equals("COD")
        && StringUtils.isNumeric(bean.getNifbic().trim().substring(3, 4))) {
      errCodes.add(CodError.BIC_FICTICIO.text);
    }
  }

  /**
   * 39- Comprobación de coherencia de identificadores.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarCoherenciaIdentificadores(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() != null
        && (TipoDocumento.NIE.getTipoDocumento().equals(bean.getTipoDocumento())
            || TipoDocumento.CIF.getTipoDocumento().equals(bean.getTipoDocumento()) || TipoDocumento.NIF
            .getTipoDocumento().equals(bean.getTipoDocumento())) && StringUtils.isNotBlank(bean.getNifbic())) {
      // errCodes.add(CodError.NIF_BIC_INFORMADOS.text);
    }
  }

  /**
   * Comprobación de tipo de NIF válido en caso de recibir como identificador un
   * NIF.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarTipoNif(PartenonRecordBean bean, List<String> errCodes) {
    if (TipoDocumento.NIF.getTipoDocumento().equals(bean.getTipoDocumento())
        && StringUtils.isNotEmpty(bean.getNifbic()) && !StringUtils.isNumeric(bean.getNifbic().substring(0, 1))
        && !StringHelper.isIn(bean.getNifbic().substring(0, 1), "K", "L", "M")) {
      errCodes.add(CodError.NIF_COMIENZO.text);
    }
  }

  /**
   * Comprueba las longitudes de los distintos documentos ya sean NIF, NIE, CIF,
   * BIC u Otros
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarLongitudDocumentos(PartenonRecordBean bean, List<String> errCodes) {
    comprobarLongitudNifNieCif(bean, errCodes);

    comprobarLongitudBic(bean, errCodes);

    comprobarLongitudOtros(bean, errCodes);
  }

  /**
   * 26- Comprobación de la longitud del identificador en caso de ser de tipo
   * “Otros”.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarLongitudOtros(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() != null && TipoDocumento.OTROS.getTipoDocumento().equals(bean.getTipoDocumento())
        && bean.getNifbic() != null && bean.getNifbic().trim().length() > 40) {
      errCodes.add(CodError.LONG_OTROS.text);
    }
  }

  /**
   * 25- Comprobación de la longitud del identificador en caso de ser BIC.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarLongitudBic(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() != null && TipoDocumento.BIC.getTipoDocumento().equals(bean.getTipoDocumento())
        && bean.getNifbic() != null && bean.getNifbic().trim().length() != 11) {
      errCodes.add(CodError.LONG_BIC.text);
    }
  }

  /**
   * Comprobación de la longitud del identificador en caso de ser NIF, NIE o
   * CIF.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarLongitudNifNieCif(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() != null
        && (TipoDocumento.NIE.getTipoDocumento().equals(bean.getTipoDocumento())
            || TipoDocumento.CIF.getTipoDocumento().equals(bean.getTipoDocumento()) || TipoDocumento.NIF
            .getTipoDocumento().equals(bean.getTipoDocumento())) && bean.getNifbic() != null
        && bean.getNifbic().trim().length() != 9) {
      errCodes.add(CodError.LONG_NIF_NIE_CIF.text);
    }
  }

  /**
   * Comprobación de longitud y formato del dato recibido en el código postal
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarCodigoPostal(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getDistrito() != null
        && (bean.getDistrito().trim().length() != 5 || !StringUtils.isNumeric(bean.getDistrito()))) {
      errCodes.add(CodError.CODPOSTAL.text);
    }
  }

  /**
   * Comprobación de valor correcto en el campo tipo de titular.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarTipoTitular(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTiptitu() != null && StringUtils.isNotEmpty(bean.getTiptitu().toString())
        && TipoTitular.getTipoTitular(bean.getTiptitu()) == null) {
      errCodes.add(CodError.TIPTITU.text);
    }
  }

  /**
   * Comprobación de valor correcto en el campo indicador de nacionalidad.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarIndicadorNacionalidad(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getIndNac() != null && StringUtils.isNotEmpty(bean.getIndNac().toString())
        && IndicadorNacionalidad.getIndicadorNacionalidad(bean.getIndNac()) == null) {
      errCodes.add(CodError.TIPONAC.text);
    }
  }

  /**
   * Comprobación de valor correcto en el campo tipo de persona.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarTipoPersona(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoPersona() != null && StringUtils.isNotEmpty(bean.getTipoPersona().toString())
        && TipoPersona.getTipoPersona(bean.getTipoPersona()) == null) {
      errCodes.add(CodError.TIPPERS.text);
    }
  }

  /**
   * Comprobación de valor correcto en el campo tipo de identificador (tipo de
   * documento).
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarTipoDocumento(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() != null && StringUtils.isNotEmpty(bean.getTipoDocumento().toString())
        && TipoDocumento.getTipoDocumento(bean.getTipoDocumento()) == null) {
      errCodes.add(CodError.TIPODOCUMENTO.text);
    }
  }

  /**
   * Comprobaciones del código BIC
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarBic(PartenonRecordBean bean, List<String> errCodes) {
    // 35- Comprobación del formato de un BIC recibido como identificador
    // del titular (I).
    if (TipoDocumento.BIC.getTipoDocumento().equals(bean.getTipoDocumento()) && bean.getNifbic() != null) {
      Pattern pattern = Pattern.compile("\\s");
      Matcher matcher = pattern.matcher(bean.getNifbic().trim());
      if (matcher.find()) {
        errCodes.add(CodError.BLANCO_BIC.text);
      }
    }

    // 36- Comprobación del formato de un BIC recibido como identificador
    // del titular (II).
    if (TipoDocumento.BIC.getTipoDocumento().equals(bean.getTipoDocumento()) && bean.getNifbic() != null
        && !StringUtils.isAlpha(bean.getNifbic().trim().substring(0, 6))) {
      errCodes.add(CodError.BIC_FORMATO_LETRAS.text);
    }
  }

  /**
   * 32- Comprobación de tipo de CIF válido en caso de recibir como
   * identificador un CIF.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarCif(PartenonRecordBean bean, List<String> errCodes) {
    if (TipoDocumento.CIF.getTipoDocumento().equals(bean.getTipoDocumento()) && bean.getNifbic() != null) {
      if (bean.getNifbic().trim().isEmpty() || "ABCDEFGHJUVNWPQSR".indexOf(bean.getNifbic().substring(0, 1)) == -1) {
        errCodes.add(CodError.CIF_COMIENZO.text);
      }
      else if (bean.getNifbic().trim().length() == 9) {
        // 33- Comprobación del formato de un CIF recibido como
        // identificador del titular.
        if (!StringUtils.isNumeric(bean.getNifbic().substring(1, 8))
            || !StringUtils.isAlphanumeric(bean.getNifbic().substring(8, 9))) {
          errCodes.add(CodError.FORMATO_CIF.text);
        }
        else if (obtenerDigitoControl(bean.getNifbic()) != bean.getNifbic().charAt(8)) {
          // 34- Comprobación del dígito de control de un CIF recibido
          // como identificador del titular.
          errCodes.add(CodError.CONTROL_CIF.text);
        }
      }
    }
  }

  /**
   * Comprobación del dígito de control de un NIF o NIE recibido como
   * identificador del titular
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarDigitoControlNifNie(PartenonRecordBean bean, List<String> errCodes) {
    Integer numeroDni = null;
    if (TipoDocumento.NIF.getTipoDocumento().equals(bean.getTipoDocumento())
        || TipoDocumento.NIE.getTipoDocumento().equals(bean.getTipoDocumento())) {
      if (bean.getNifbic() != null && bean.getNifbic().length() < 9) {
        errCodes.add(CodError.CIFNIF.text);
      }
      else if (bean.getNifbic() != null && StringUtils.isAlpha(bean.getNifbic().substring(8, 9))) {
        String letraFinal = bean.getNifbic().substring(8, 9);
        if (StringUtils.isNumeric(bean.getNifbic().substring(0, 1))
            && StringUtils.isNumeric(bean.getNifbic().substring(0, 8))) {
          numeroDni = Integer.parseInt(bean.getNifbic().substring(0, 8));
        }
        else if (StringUtils.isAlpha(bean.getNifbic().substring(0, 1))
            && StringUtils.isNumeric(bean.getNifbic().substring(1, 8))) {
          String letra = bean.getNifbic().substring(0, 1);
          try {
            if ("Y".equals(letra)) {
              numeroDni = Integer.parseInt("1" + bean.getNifbic().substring(1, 8));
            }
            else if ("Z".equals(letra)) {
              numeroDni = Integer.parseInt("2" + bean.getNifbic().substring(1, 8));
            }
            else {
              numeroDni = Integer.parseInt(bean.getNifbic().substring(1, 8));
            }
          }
          catch (Exception e) {
            errCodes.add(CodError.CIFNIF.text);
          }

        }
        if (numeroDni != null && !coincideRestoConLetra(numeroDni, letraFinal)) {
          errCodes.add(CodError.CIFNIF.text);
        }
      }
    }
  }

  /**
   * Comprobación de tipo de NIE válido en caso de recibir como identificador un
   * NIE.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarFormatoNie(PartenonRecordBean bean, List<String> errCodes) {
    if (TipoDocumento.NIE.getTipoDocumento().equals(bean.getTipoDocumento())
        && StringUtils.isNotEmpty(bean.getNifbic())) {
      if (!StringHelper.isIn(bean.getNifbic().substring(0, 1), "X", "Y", "Z")) {
        errCodes.add(CodError.NIE_COMIENZO.text);
      }
      else if (!StringUtils.isAlpha(bean.getNifbic().substring(0, 1))
          || !StringUtils.isNumeric(bean.getNifbic().substring(1, 8))
          || !StringUtils.isAlpha(bean.getNifbic().substring(8, 9))) {
        // Comprobación del formato de un NIE recibido como
        // identificador del titular.
        errCodes.add(CodError.FORMATO_NIE.text);
      }
    }
  }

  /**
   * Comprobación del formato de un NIF recibido como identificador del titular.
   * 
   * @param bean
   * @param errCodes
   */
  private static void comprobarFormatoNif(PartenonRecordBean bean, List<String> errCodes) {
    if (TipoDocumento.NIF.getTipoDocumento().equals(bean.getTipoDocumento())
        && (StringUtils.isEmpty(bean.getNifbic()) || bean.getNifbic().length() < 9)) {
      errCodes.add(CodError.NIF_FORMATO.text);
    }
    else if (TipoDocumento.NIF.getTipoDocumento().equals(bean.getTipoDocumento())
        && ((StringHelper.isIn(bean.getNifbic().substring(0, 1), "K", "L", "M") && (!StringUtils.isNumeric(bean
            .getNifbic().substring(1, 8)) || !StringUtils.isAlpha(bean.getNifbic().substring(8, 9)))) || (StringUtils
            .isNumeric(bean.getNifbic().substring(0, 1)) && (!StringUtils.isNumeric(bean.getNifbic().substring(0, 8)) || !StringUtils
            .isAlpha(bean.getNifbic().substring(8, 9)))))) {
      errCodes.add(CodError.NIF_FORMATO.text);
    }
  }

  public static List<String> comprobarNoInformados(PartenonRecordBean bean) {
    List<String> errCodes = new ArrayList<String>();

    comprobarNombresNoInformados(bean, errCodes);

    comprobarDocumentoNoInformado(bean, errCodes);

    comprobarIndicadoresNoInformados(bean, errCodes);

    comprobarNacionalidadNoInformada(bean, errCodes);

    comprobarComponentesDireccionNoInformados(bean, errCodes);

    return errCodes;
  }

  private static void comprobarNacionalidadNoInformada(PartenonRecordBean bean, List<String> errCodes) {
    if (StringUtils.isEmpty(bean.getNacionalidad())) {
      errCodes.add(CodError.INFORMAR_NACIONALIDAD.text);
    }
  }

  private static void comprobarDocumentoNoInformado(PartenonRecordBean bean, List<String> errCodes) {
    if ((bean.getNifbic() == null || StringUtils.isEmpty(bean.getNifbic()))) {
      errCodes.add(CodError.INFORMAR_DOCUMENTO.text);
    }
  }

  private static void comprobarIndicadoresNoInformados(PartenonRecordBean bean, List<String> errCodes) {
    if (bean.getTipoDocumento() == null || StringUtils.isEmpty(bean.getTipoDocumento().toString().trim())) {
      errCodes.add(CodError.INFORMAR_TIPO_DOCUMENTO.text);
    }

    if (bean.getTipoPersona() == null || StringUtils.isEmpty(bean.getTipoPersona().toString().trim())) {
      errCodes.add(CodError.INFORMAR_TIPPERS.text);
    }

    if (bean.getIndNac() == null || StringUtils.isEmpty(bean.getIndNac().toString().trim())) {
      errCodes.add(CodError.INFORMAR_IND_NAC.text);
    }

    if (bean.getTiptitu() == null || StringUtils.isEmpty(bean.getTiptitu().toString().trim())) {
      errCodes.add(CodError.INFORMAR_TIPO_TITULAR.text);
    }
  }

  private static void comprobarComponentesDireccionNoInformados(PartenonRecordBean bean, List<String> errCodes) {
    if (StringUtils.isEmpty(bean.getDomicili())) {
      errCodes.add(CodError.DIRECCION.text);
    }
    else if (bean.getDomicili().replaceAll("\\s+", "").length() < 3) {
      errCodes.add(CodError.DIRECCION_CORTA.text);
    }

    if (StringUtils.isEmpty(bean.getPoblacion())) {
      errCodes.add(CodError.CIUDAD.text);
    }

    if ("011".equals(bean.getPaisResidencia()) && StringUtils.isEmpty(bean.getProvincia())) {
      errCodes.add(CodError.PROVINCIA.text);
    }

    if (StringUtils.isEmpty(bean.getDistrito())) {
      errCodes.add(CodError.INFORMAR_COD_POSTAL.text);
    }

    if (StringUtils.isEmpty(bean.getPaisResidencia())) {
      errCodes.add(CodError.INFORMAR_PAIS_RES.text);
    }
  }

  private static void comprobarNombresNoInformados(PartenonRecordBean bean, List<String> errCodes) {
    if (TipoPersona.FISICA.getTipoPersona().equals(bean.getTipoPersona()) && "724".equals(bean.getNacionalidad())) {
      // Para persona fisica nacional es obligatorio informar el nombre y
      // dos apellidos
      if (StringUtils.isEmpty(bean.getNbclient()) || StringUtils.isEmpty(bean.getNbclient1())
          || StringUtils.isEmpty(bean.getNbclient2())) {
        errCodes.add(CodError.NOMBRE_DOS_APELLIDOS.text);
      }
    }

    if (TipoPersona.FISICA.getTipoPersona().equals(bean.getTipoPersona()) && !"724".equals(bean.getNacionalidad())) {
      // Para persona fisica no es obligatorio informar el nombre y un
      // apellido
      if (StringUtils.isEmpty(bean.getNbclient()) || StringUtils.isEmpty(bean.getNbclient1())) {
        errCodes.add(CodError.NOMBRE_UN_APELLIDO.text);
      }
    }

    if (TipoPersona.JURIDICA.getTipoPersona().equals(bean.getTipoPersona())) {
      // Para persona fisica no es obligatorio informar el nombre y un
      // apellido
      if (StringUtils.isEmpty(bean.getNbclient1())) {
        errCodes.add(CodError.INFORMAR_RAZON_SOCIAL.text);
      }
    }

    if (TipoPersona.JURIDICA.getTipoPersona().equals(bean.getTipoPersona())) {
      // Para persona fisica no es obligatorio informar el nombre y un
      // apellido
      if (StringUtils.isNotEmpty(StringUtils.trimToEmpty(bean.getNbclient()))) {
        errCodes.add(CodError.PERSONA_JURIDICA_CON_NBCLIENT.text);
      }
    }
  }
  
  
  

}
