package sibbac.business.wrappers.tasks;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.FacturaRectificativaBo;
import sibbac.business.wrappers.database.model.FacturaRectificativa;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_FACTURA_RECTIFICATICA, interval = 1, intervalUnit = IntervalUnit.MINUTE)
public class TaskFacturaRectificativa extends WrapperTaskConcurrencyPrevent {

    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskFacturaRectificativa.class);

    @Autowired
    protected FacturaRectificativaBo facturaRectificativaBo;

    public TaskFacturaRectificativa() {
    }

    @Override
    public void executeTask() {

	final String prefix = "[TaskFacturaRectificativa] ";
	LOG.trace(prefix + "[Started...]");
	this.executeTask(prefix);
	LOG.trace(prefix + "[Finished...]");
    }

    private void executeTask(final String prefix){
	final List<FacturaRectificativa> facturasRectificativas = facturaRectificativaBo
		.generarFacturasRectificativas();
	if (CollectionUtils.isNotEmpty(facturasRectificativas)) {
	    LOG.debug(prefix + "[Generadas " + facturasRectificativas.size()
		    + " facturas rectificativas");
	} else {
	    LOG.trace(prefix + "[No se han generado facturas rectificativas]");
	}
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_FACTURAS_FACTURA_RECTIFICATIVA;
    }

}
