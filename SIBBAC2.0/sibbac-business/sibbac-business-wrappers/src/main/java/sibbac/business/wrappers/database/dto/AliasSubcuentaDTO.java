package sibbac.business.wrappers.database.dto;


import org.apache.commons.lang3.StringUtils;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasSubcuenta;


public class AliasSubcuentaDTO {

	private Long	id;
	private String	nombre;
	private Long	idAliasFacturacion;
	private String	nombreAliasFacturacion;

	public AliasSubcuentaDTO( final AliasSubcuenta aliasSubcuenta ) {
		this( aliasSubcuenta.getId(), aliasSubcuenta.getAliasFacturacion(), aliasSubcuenta.getCdsubcta(), aliasSubcuenta.getDescrali() );
	}

	public AliasSubcuentaDTO( final Long id, final Alias aliasFacturacion, final String nombre, final String descrali ) {
		this.id = id;
		this.nombre = formarNombreAlias( StringUtils.trimToEmpty( nombre ), StringUtils.trimToEmpty( descrali ) );
		if ( aliasFacturacion != null ) {
			this.idAliasFacturacion = aliasFacturacion.getId();
			this.nombreAliasFacturacion = formarNombreAlias( aliasFacturacion.getCdaliass(), aliasFacturacion.getDescrali() );
		}
	}

	/**
	 * @param alias
	 */
	private String formarNombreAlias( final String cdsubcta, final String descrali ) {
		String nombre = "";
		if ( StringUtils.isNotEmpty( cdsubcta ) ) {
			nombre = StringUtils.trimToEmpty( cdsubcta );
		}
		if ( StringUtils.isNotEmpty( cdsubcta ) && StringUtils.isNotEmpty( descrali ) ) {
			nombre = nombre + " - ";
		}
		if ( StringUtils.isNotEmpty( descrali ) ) {
			nombre = nombre + StringUtils.trimToEmpty( descrali );
		}
		return nombre;
	}

	public Long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	/**
	 * @return the idAliasFacturacion
	 */
	public Long getIdAliasFacturacion() {
		return idAliasFacturacion;
	}

	/**
	 * @param idAliasFacturacion the idAliasFacturacion to set
	 */
	public void setIdAliasFacturacion( Long idAliasFacturacion ) {
		this.idAliasFacturacion = idAliasFacturacion;
	}

	/**
	 * @return the nombreAliasFacturacion
	 */
	public String getNombreAliasFacturacion() {
		return nombreAliasFacturacion;
	}

	/**
	 * @param nombreAliasFacturacion the nombreAliasFacturacion to set
	 */
	public void setNombreAliasFacturacion( String nombreAliasFacturacion ) {
		this.nombreAliasFacturacion = nombreAliasFacturacion;
	}

}
