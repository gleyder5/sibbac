package sibbac.business.wrappers.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

public class FicherosUtils {

  protected static final Logger LOG = LoggerFactory.getLogger(FicherosUtils.class);

  public static File fileToTemp(String fileName, String filePath, String extension) throws IOException {
    LOG.debug("[FicherosUtils :: fileToTemp] Init (FileName=" + fileName + ", FilePath=" + filePath + ")");
    String urlString = filePath + "/" + fileName;
    URL url = new URL(urlString);
    File fileOrigen = new File(url.getPath());
    String baseName = FilenameUtils.getBaseName(urlString);
    File fileDestino = new File(fileOrigen.getParent(), baseName.concat(extension));
    Files.move(fileOrigen, fileDestino);
    try {
      fileOrigen = new File(url.toURI());
    } catch (URISyntaxException e) {
      LOG.debug("[FicherosUtils :: fileToTemp] Error de sintaxis en la URI del fichero: " + e.getMessage());
    }
    LOG.debug("[FicherosUtils :: fileToTemp] Fin (FileName=" + fileName + ", FilePath=" + filePath + ")");
    return fileDestino;
  }

  public static List<File> filesToTemp(List<File> files, String extension) throws IOException {
    LOG.debug("[FicherosUtils :: filesToTemp] Init (extension=" + extension + ")");
    List<File> temporalList = new ArrayList<File>();
    for (File fileOrigen : files) {
      String baseName = FilenameUtils.getBaseName(fileOrigen.getAbsolutePath());
      File fileDestino = new File(fileOrigen.getParent(), baseName.concat(extension));
      Files.move(fileOrigen, fileDestino);
      temporalList.add(fileDestino);
    }
    LOG.debug("[FicherosUtils :: filesToTemp] Fin (extension=" + extension + ")");
    return temporalList;
  }

  public static File removeTmpFromName(File fileTemporal, String fileName) throws IOException {
    File fileDestino = new File(fileTemporal.getParent(), fileName);
    Files.move(fileTemporal, fileDestino);
    return fileDestino;
  }

  public static File fileToTemp(String fileName, String filePath) throws IOException {
    return fileToTemp(fileName, filePath, ".tmp");
  }

  public static void fileToTratados(File file, String dateFormatString) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
    try {
      File folderDestino = new File(file.getParentFile(), "tratados");
      folderDestino.mkdirs();
      String dateToConcat = dateFormat.format(new Date());
      Files.move(file, new File(folderDestino, FilenameUtils.getBaseName(file.getName()).concat(dateToConcat)));
      LOG.debug("[FicherosUtils :: fileToTratados] Fichero movido a carpeta tratados");
    } catch (Exception e) {
      LOG.error("[FicherosUtils :: fileToTratados] Error moviendo fichero a tratados (FILE: " + file.getPath() + ")");
    }
    LOG.debug("[FicherosUtils :: fileToTratados] Fin (FILE: " + file.getPath() + ")");
  }

  public static void copyToTratados(Path file) {
    copyToTratados(file, ".yyyyMMddHHmmssSSS");
  }

  private static void copyToTratados(Path file, String dateFormatString) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
    String fileName = file.toFile().getName();
    try {
      Path folderDestino = Paths.get(file.getParent().toString(), "tratados");
      Path ficheroDestino = Paths.get(folderDestino.toString(),
                                      FilenameUtils.getBaseName(fileName) + dateFormat.format(new Date()));
      if (java.nio.file.Files.notExists(folderDestino)) {
        java.nio.file.Files.createDirectories(folderDestino);
      }

      java.nio.file.Files.copy(file, ficheroDestino, StandardCopyOption.COPY_ATTRIBUTES);

      LOG.debug("[FicherosUtils :: fileToTratados] Fichero movido a carpeta tratados {} a {}", file.toString(),
                ficheroDestino.toString());
    } catch (Exception e) {
      LOG.warn("[FicherosUtils :: fileToTratados] Error moviendo fichero a tratados (FILE: {} )", file.toString(), e);
    }
    LOG.debug("[FicherosUtils :: fileToTratados] Fin (FILE: {})", file.toString());
  }

  public static void fileToTratados(File file) {
    fileToTratados(file, ".yyyyMMddHHmmss");
  }

  public static void filesToTratados(List<File> files) {
    filesToTratados(files, ".yyyyMMddHHmmss");
  }

  private static void filesToTratados(List<File> files, String dateFormatString) {
    for (File file : files) {
      fileToTratados(file, dateFormatString);
    }

  }

  public static void fileToErroneos(File file) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(".yyyyMMddHHmmss");
    try {
      File folderDestino = new File(file.getParentFile(), "erroneos");
      folderDestino.mkdirs();
      String dateToConcat = dateFormat.format(new Date());
      Files.move(file, new File(folderDestino, FilenameUtils.getBaseName(file.getName()).concat(dateToConcat)));
      LOG.debug("[FicherosUtils :: fileToTratados] Fichero movido a carpeta erroneos");
    } catch (Exception e) {
      LOG.error("[FicherosUtils :: fileToTratados] Error moviendo fichero a erroneos (FILE: " + file.getPath() + ")");
    }
    LOG.debug("[FicherosUtils :: fileToTratados] Fin (FILE: " + file.getPath() + ")");
  }

}
