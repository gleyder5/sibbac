package sibbac.business.wrappers.enums;

public interface Enumerados {

	/**
	 * 	Estados posibles para registros de TMCT0MEN.
	 */
	public enum EstadosTmct0Men {

		ESTADO_LISTO_GENERAR("010"),
		ESTADO_EN_EJECUCION("900"),
		ESTADO_EN_ERROR("000");

		private String id;

		private EstadosTmct0Men(String id) {
			this.id = id;
		}

		public String getId() {
			return id;
		}

	}

	/**
	 * The Enum para la lista totalizar
	 */
	public enum EnuIdioma {

		ESPAÑOL(2),
		INGLES(3);

		private Integer key;

		EnuIdioma(Integer key) {
			this.key = key;
		}

		public Integer getKey() {
			return key;
		}

	}

}