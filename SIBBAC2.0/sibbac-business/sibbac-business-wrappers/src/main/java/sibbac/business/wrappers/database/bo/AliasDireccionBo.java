package sibbac.business.wrappers.database.bo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.AliasDireccionDao;
import sibbac.business.wrappers.database.dto.AliasDireccionDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasDireccion;
import sibbac.database.bo.AbstractBo;

@Service
public class AliasDireccionBo extends AbstractBo<AliasDireccion, Long, AliasDireccionDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(AliasDireccionBo.class);

  @Autowired
  private AliasBo aliasBO;

  /**
   * Devuelve una lista de los aliasDirecciones
   *
   * @return
   */
  public List<AliasDireccionDTO> getLista() {

    LOG.trace(" Llamada al metodo getLista del aliasDireccionesBO ");
    List<AliasDireccion> entities = (List<AliasDireccion>) findAll();
    List<AliasDireccionDTO> resultList = new LinkedList<AliasDireccionDTO>();

    for (AliasDireccion entity : entities) {
      AliasDireccionDTO dto = entityAliasDireccionToDTO(entity);
      resultList.add(dto);
    }

    return resultList;

  }

  public AliasDireccionDTO entityAliasDireccionToDTO(AliasDireccion entity) {

    LOG.trace(" Llamada al metodo entityAliasDireccionToDTO del aliasDireccionBO ");
    AliasDireccionDTO dto = new AliasDireccionDTO();

    if (entity != null) {
      dto.setId(entity.getId());
      dto.setCdAlias(entity.getAlias().getCdaliass());
      dto.setCalle(entity.getCalle());
      dto.setNumero(entity.getNumero());
      dto.setBloque(entity.getBloque());
      dto.setPoblacion(entity.getPoblacion());
      dto.setCodigoPostal(entity.getCodigoPostal());
      dto.setProvincia(entity.getProvincia());
      dto.setPais(entity.getPais());
    }

    return dto;

  }

  /**
   * Busca todos los AliasDirecciones por el alias
   *
   * @param alias
   * @return
   */
  public List<AliasDireccion> findAllByAlias(Alias alias) {

    LOG.trace(" LLamada al metodo findAllByAlias del aliasDireccionesBO ");
    return this.dao.findAllByAlias(alias);

  }

  /**
   * Como los metodos save y delete reciben los datos un objeto
   * AliasDirecciones, este metodo sirve para no duplicar codigo, se rellena
   * aqui y se pasa a cada metodo el objeto creado.
   *
   * @param params
   * @return
   */
  public AliasDireccion rellenarObjeto(Map<String, String> params) {

    LOG.trace("Se inicia la creacion del objeto aliasDirecciones");

    AliasDireccion aliasDireccion = new AliasDireccion();

    aliasDireccion.setAlias(this.findAliasById(params));

    aliasDireccion.setCalle(params.get(Constantes.ALIASDIRECCIONES_PARAM_CALLE));
    aliasDireccion.setNumero(params.get(Constantes.ALIASDIRECCIONES_PARAM_NUMERO));
    aliasDireccion.setBloque(params.get(Constantes.ALIASDIRECCIONES_PARAM_BLOQUE));
    aliasDireccion.setPoblacion(params.get(Constantes.ALIASDIRECCIONES_PARAM_POBLACION));
    aliasDireccion.setCodigoPostal(params.get(Constantes.ALIASDIRECCIONES_PARAM_CODIGOPOSTAL));
    aliasDireccion.setProvincia(params.get(Constantes.ALIASDIRECCIONES_PARAM_PROVINCIA));
    aliasDireccion.setPais(params.get(Constantes.ALIASDIRECCIONES_PARAM_PAIS));

    LOG.trace(" Se ha creado el objeto aliasDirecciones ");

    return aliasDireccion;

  }

  /**
   * Cambia un objeto aliasDirecciones recuperado de la tabla aliasDirecciones
   * modificando los campos que se pasan por parametro (params)
   *
   * @param params
   * @return
   */
  @Transactional
  public Map<String, String> modificacionAliasDirecciones(Map<String, String> params) {

    LOG.trace(" Se inicia la modificacion de un aliasDirecciones ");
    Map<String, String> resultado = new HashMap<String, String>();

    if (params == null) {
      return null;
    }

    AliasDireccion aliasDireccion = this.findById(Long.parseLong(params.get(Constantes.ALIASDIRECCIONES_PARAM_ID)));

    // aliasDireccion.setAlias(this.findAliasById(params));
    aliasDireccion.setCalle(params.get(Constantes.ALIASDIRECCIONES_PARAM_CALLE));
    aliasDireccion.setNumero(params.get(Constantes.ALIASDIRECCIONES_PARAM_NUMERO));
    aliasDireccion.setBloque(params.get(Constantes.ALIASDIRECCIONES_PARAM_BLOQUE));
    aliasDireccion.setPoblacion(params.get(Constantes.ALIASDIRECCIONES_PARAM_POBLACION));
    aliasDireccion.setCodigoPostal(params.get(Constantes.ALIASDIRECCIONES_PARAM_CODIGOPOSTAL));
    aliasDireccion.setProvincia(params.get(Constantes.ALIASDIRECCIONES_PARAM_PROVINCIA));
    aliasDireccion.setPais(params.get(Constantes.ALIASDIRECCIONES_PARAM_PAIS));

    String result = "";
    try { 
	   this.save(aliasDireccion);
	   LOG.trace(" Se ha modificado el aliasDirecciones ");
    }catch(Exception ex) {
	   LOG.trace(" No se modificado el aliasDirecciones ex: {}",ex);
    }
    resultado.put("result", result);
    return resultado;
  }

  /**
   * Se da de alta un nuevo aliasDirecciones con los datos pasados por parametro
   * (params) en la tabla AliasDirecciones.
   *
   * @param params
   * @return
   */
  @Transactional
  public Map<String, String> createAliasDirecciones(Map<String, String> params) {

    LOG.trace(" Se inicia el alta de un aliasDirecciones ");
    Map<String, String> resultado = new HashMap<String, String>();
    String result = "";
    AliasDireccion aliasDireccionEntity  =  this.rellenarObjeto(params); 
    try {
      aliasDireccionEntity = this.save(aliasDireccionEntity);
      LOG.trace(" Se ha creado el aliasDirecciones ");
      resultado.put("result", "Dirección registrada correctamente" );
      resultado.put("aliasDireccionId", aliasDireccionEntity.getId().toString());
    }catch(Exception ex){
      LOG.trace(" No se ha podido crear el aliasDirecciones {}",ex);
      resultado.put("result", "false");
    }
    return resultado;
  }

  /**
   * Devuelve una lista de los contactos de la tabla AliasDirecciones
   *
   * @param params
   * @return
   */

  public Map<String, AliasDireccion> getListaAliasDirecciones(Map<String, String> params) {
    AliasDireccion aliasDireccion = new AliasDireccion();
    Alias alias = aliasBO.findByCdaliass(params);
    AliasDireccion aliasDireccionAux = alias.getAliasDireccion();
    Map<String, AliasDireccion> resultados = new HashMap<String, AliasDireccion>();
    if (aliasDireccionAux == null) {
      aliasDireccion.setId((long) 0);
      aliasDireccion.setBloque("");
      aliasDireccion.setCalle("");
      aliasDireccion.setCodigoPostal("");
      aliasDireccion.setNumero("");
      aliasDireccion.setPais("");
      aliasDireccion.setPoblacion("");
      aliasDireccion.setProvincia("");
      resultados.put(alias.getId().toString(), aliasDireccionAux);

      resultados.put(alias.getId().toString(), aliasDireccion);

    }
    else {
      aliasDireccion.setId(alias.getAliasDireccion().getId());
      aliasDireccion.setBloque(alias.getAliasDireccion().getBloque());
      aliasDireccion.setCalle(alias.getAliasDireccion().getCalle());
      aliasDireccion.setCodigoPostal(alias.getAliasDireccion().getCodigoPostal());
      aliasDireccion.setNumero(alias.getAliasDireccion().getNumero());
      aliasDireccion.setPais(alias.getAliasDireccion().getPais());
      aliasDireccion.setPoblacion(alias.getAliasDireccion().getPoblacion());
      aliasDireccion.setProvincia(alias.getAliasDireccion().getProvincia());
      resultados.put(alias.getId().toString(), aliasDireccion);

      LOG.trace("Se inicia la creacion de la lista de los AliasDireccion");

    }

    return resultados;

  }

  /*
   * Este metodo devuelve un objeto Alias en base al id alias que se le pasa y
   * que lleva el objeto AliasDirecciones.
   * 
   * @param params
   * 
   * @return
   */
  public Alias findAliasById(Map<String, String> params) {

    LOG.trace(" Llamada al metodo findAliasById del aliasDireccionesBO ");
    return aliasBO.findById(Long.parseLong(params.get(Constantes.PARAM_ALIAS_ID)));
  }

}
