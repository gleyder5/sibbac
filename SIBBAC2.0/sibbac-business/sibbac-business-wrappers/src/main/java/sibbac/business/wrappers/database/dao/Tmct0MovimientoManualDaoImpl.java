/**
 * 
 */
package sibbac.business.wrappers.database.dao;


import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0MovimientoManual;


/**
 * Implementacion dao cuenta manual
 * 
 * @author Cristina
 *
 */
@Repository
public class Tmct0MovimientoManualDaoImpl {

	private static final String	TAG	= Tmct0MovimientoManualDaoImpl.class.getName();
	private static final Logger	LOG	= LoggerFactory.getLogger( TAG );

	@PersistenceContext
	private EntityManager		em;

	@SuppressWarnings( "unchecked" )
	public List< Tmct0MovimientoManual > findAll( Map< String, Serializable > filters ) {
		List< Tmct0MovimientoManual > resultList = new LinkedList< Tmct0MovimientoManual >();
		StringBuilder select = new StringBuilder( "SELECT m FROM Tmct0MovimientoManual m " );
		String clauseTipoCuenta = "AND (cc.tmct0TipoCuentaDeCompensacion.idTipoCuenta =:idTipoCuentaPropia OR cc.tmct0TipoCuentaDeCompensacion.idTipoCuenta =:idTipoCuentaIndividual) ";
		String fcontratacionBtw = " AND a.fechaContratacion BETWEEN :fcontratacionDe AND :fcontratacionA";
		//
		StringBuilder where = new StringBuilder();
		StringBuilder sbQuery = new StringBuilder();
		boolean addedCuentaCompensacion = false;
		boolean addedTipoCuentaClause = false;
		boolean addedWhereClause = false;
		//
		if ( filters != null ) {
			for ( Map.Entry< String, Serializable > entry : filters.entrySet() ) {
				if ( entry.getValue() != null ) {
					if ( !entry.getKey().equals( "fcontratacionDe" ) && !entry.getKey().equals( "fcontratacionA" )
							&& !entry.getKey().equals( "fliquidacionDe" ) && !entry.getKey().equals( "fliquidacionA" )
							&& !entry.getKey().equals( "idTipoCuentaPropia" ) && !entry.getKey().equals( "idTipoCuentaIndividual" )
							&& !entry.getKey().equals( "cdisin" ) ) {
						where.append( "AND m." ).append( entry.getKey() ).append( "=:" ).append( entry.getKey() + " " );
					} else if ( entry.getKey().equals( "idTipoCuentaPropia" ) || entry.getKey().equals( "idTipoCuentaIndividual" ) ) {
						if ( !addedCuentaCompensacion ) {
							select.append( ", Tmct0CuentasDeCompensacion cc " );
							addedCuentaCompensacion = true;
							if ( !addedWhereClause ) {
								where.append( "WHERE (cc.cdCodigo = m.cuentaLiquidacion OR cc.cdCodigo = m.cuentaLiquidacionEfectivo) " );
								addedWhereClause = true;
							} else {
								where.append( "AND (cc.cdCodigo = m.cuentaLiquidacion OR cc.cdCodigo = m.cuentaLiquidacionEfectivo) " );
							}
						}
						if ( !addedTipoCuentaClause ) {
							where.append( clauseTipoCuenta );
							addedTipoCuentaClause = true;
						}
					} else if ( entry.getKey().equals( "fcontratacionDe" ) ) {
						// si vienen las dos fechas se hace un between
						if ( checkIfKeyExist( "fcontratacionA", filters ) ) {
							where.append( fcontratacionBtw );
						} else {
							where.append( "AND m.fcontratacion >=:fcontratacionDe " );
						}
					} else if ( entry.getKey().equals( "fcontratacionA" ) ) {
						// si vienen las dos fechas se hace un between
						if ( !checkIfKeyExist( "fcontratacionDe", filters ) ) {
							where.append( "AND m.fcontratacion <=:fcontratacionA " );
						}
					} else if ( entry.getKey().equals( "cdisin" ) && !"".equals( entry.getValue() ) ) {
						if ( addedWhereClause ) {
							where.append( "AND m.isin =:cdisin " );
						} else {
							where.append( "WHERE m.isin =:cdisin " );
							addedWhereClause = true;
						}
					}
				}
			}
		}
		sbQuery.append( select ).append( where );
		Query query = null;
		//
		try {
			query = em.createQuery( sbQuery.toString() );
			for ( Map.Entry< String, Serializable > entry : filters.entrySet() ) {
				query.setParameter( entry.getKey(), entry.getValue() );
			}
			resultList = query.getResultList();
		} catch ( PersistenceException | IllegalArgumentException ex ) {
			LOG.error( ex.getMessage(), ex );
		}
		return resultList;
	}

	private boolean checkIfKeyExist( String key, Map< String, Serializable > params ) {
		boolean exist = false;
		if ( params.get( key ) != null ) {
			exist = true;
		}
		return exist;
	}
}
