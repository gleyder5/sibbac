package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.wrappers.database.dao.Tmct0CamaraCompensacionDao;
import sibbac.business.wrappers.database.dto.CamaraCompensacionDTO;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0CamaraCompensacionBo extends AbstractBo<Tmct0CamaraCompensacion, Long, Tmct0CamaraCompensacionDao> {

  public List<Tmct0CamaraCompensacion> findAll() {
    return this.dao.findAll(new Sort(Sort.Direction.ASC, "nbDescripcion"));
  }

  public List<CamaraCompensacionDTO> entitiesListToAnDTOList(List<Tmct0CamaraCompensacion> camaras) {
    List<CamaraCompensacionDTO> list = new ArrayList<CamaraCompensacionDTO>();
    for (Tmct0CamaraCompensacion camara : camaras) {
      list.add(new CamaraCompensacionDTO(camara.getCdCodigo(), camara.getNbDescripcion()));
    }
    return list;
  }

  public Tmct0CamaraCompensacion findByNbDescripcion(String nbDescripcion) {
    return this.dao.findByNbDescripcion(nbDescripcion);
  }

  public Tmct0CamaraCompensacion findByCdCodigo(String cdCodigo) {
    return dao.findByCdCodigo(cdCodigo);
  }
  
  public String findCodigoCamaraCompensacion(String cdcodigo) {
      return dao.findCodigoCamaraCompensacion(cdcodigo);
  }

  public Tmct0CamaraCompensacion findByCdCodigo(Map<String, Tmct0CamaraCompensacion> mapByCdcodigo, String cdCodigo) {
    Tmct0CamaraCompensacion camara = mapByCdcodigo.get(StringUtils.trim(cdCodigo));

    if (camara == null) {
      camara = findByCdCodigo(cdCodigo);
      mapByCdcodigo.put(StringUtils.trim(cdCodigo), camara);
    }
    return camara;
  }
  
  public List<LabelValueObject> labelValues() {
    return dao.labelValues();
  }

}
