package sibbac.business.wrappers.tasks;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.common.CallableResultDTO;

public interface WrapperUserTaskConcurrencyPreventInterface {

  public CallableResultDTO executeManualUser(String auditUser);

  public CallableResultDTO executeTaskManualUser(String auditUser) throws Exception;
  
  public TIPO determinarTipoApunte();
  
  public void executeTask() throws Exception;
}
