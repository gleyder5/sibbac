/**
 * 
 */
package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Arrays;

import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.utils.FormatDataUtils;

/**
 * @author XI316153
 */
public class RechazoApunteDto {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Identificador del alc al que pertenece el apunte contable. */
  private Tmct0alcId tmct0alcId;

  /** Tipo de apunte en cuenta. D = Debe, H = Haber */
  private String apunte;

  /** Concepto del apunte contable. */
  private String concepto;

  /** Descripción del apunte contable. */
  private String descripcion;

  /** Fecha del apunte contable. */
  private Date fecha;

  /** Importe del apunte contable. */
  private BigDecimal importe;

  /** Tipo de operativa. N = Nacional, I = Internacional. */
  private String operativa;

  /** Tipo de comprobante del apunte contable. */
  private Integer tipoComprobante;

  /** Tipo de movimiento que refleja el apunte contable. */
  private Integer tipoMovimiento;

  /** Auxiliar contable. */
  private Long auxiliarContable;

  /** Cuenta contable del apunte. */
  private Long cuentaContable;

  // /** Comisión ordenante del alc. */
  // private BigDecimal imcomdvo;
  //
  // /** Comisión de devolución del alc. */
  // private BigDecimal imcombrk;
  //
  // /** Ajuste de comisiones del alc. */
  // private BigDecimal imajusvb;
  //
  // /** Comisión cliente del alc. */
  // private BigDecimal imcomisn;
  //
  // /** Canon de compensación del alc. */
  // private BigDecimal imcanoncompeu;
  //
  // /** Canon de contratación del alc. */
  // private BigDecimal imcanoncontreu;
  //
  // /** Canon de liquidación del alc. */
  // private BigDecimal imcanonliqeu;
  //
  // /** Indicador del si el cliente paga el canon de contratación del alo. */
  // private Integer canoncontrpagoclte;
  //
  // /** Importe final de comisiones del alc. */
  // private BigDecimal imfinsvb;
  //
  // /** Estado contable del alc. */
  // private Integer cdestadocont;

  /** Importe que el alc aporta al apunte contable. */
  private BigDecimal importeApunteAlc;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Crea un nuevo objeto de la clase a partir del array especificado.
   * 
   * @param rechazo Datos del rechazo a crear.
   */
  public RechazoApunteDto(Object[] rechazoArgs) {
    Object[] rechazo = Arrays.copyOf(rechazoArgs, rechazoArgs.length);
	this.tmct0alcId = new Tmct0alcId(String.valueOf(rechazo[1]).trim(), String.valueOf(rechazo[0]).trim(), Short.valueOf(rechazo[3].toString()),
                String.valueOf(rechazo[2]).trim());
	this.apunte = String.valueOf(rechazo[4]);
	this.concepto = String.valueOf(rechazo[5]);
	this.descripcion = String.valueOf(rechazo[6]);
	this.fecha = FormatDataUtils.convertStringToDate(rechazo[7].toString(), FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
	this.importe = new BigDecimal(rechazo[8].toString());
	this.operativa = String.valueOf(rechazo[9]);
	this.tipoComprobante = Integer.valueOf(rechazo[10].toString());
	this.tipoMovimiento = Integer.valueOf(rechazo[11].toString());
	this.auxiliarContable = rechazo[12] != null ? Long.valueOf(rechazo[12].toString()) : null;
	this.cuentaContable = Long.valueOf(rechazo[13].toString());

    // this.imcomdvo = new BigDecimal(rechazo[14].toString());
    // this.imcombrk = new BigDecimal(rechazo[15].toString());
    // this.imajusvb = new BigDecimal(rechazo[16].toString());
    // this.imcomisn = new BigDecimal(rechazo[17].toString());
    // this.imcanoncompeu = new BigDecimal(rechazo[18].toString());
    // this.imcanoncontreu = new BigDecimal(rechazo[19].toString());
    // this.imcanonliqeu = new BigDecimal(rechazo[20].toString());
    // this.canoncontrpagoclte = Integer.valueOf(rechazo[21].toString());
    // this.imfinsvb = new BigDecimal(rechazo[22].toString());
    // this.cdestadocont = Integer.valueOf(rechazo[23].toString());

    // this.importeApunteAlc = new BigDecimal(rechazo[24].toString());
    this.importeApunteAlc = new BigDecimal(rechazo[14].toString());
  } // RechazoApunteDto

  /**
   * Constructor II. Crea un nuevo objeto de la clase a partir del ALC y el array especificado.
   * 
   * @param nuorden Número de orden.
   * @param nbooking Número de booking.
   * @param nucnfclt Numéro de desglose cliente.
   * @param nucnfliq Número de desglose liquidación.
   * @param rechazo Datos del rechazo a crear.
   */
  public RechazoApunteDto(String nuorden, String nbooking, String nucnfclt, Short nucnfliq, Object[] rechazoArg) {
    Object[] rechazo = Arrays.copyOf(rechazoArg, rechazoArg.length);
    this.tmct0alcId = new Tmct0alcId(nbooking, nuorden, nucnfliq, nucnfclt);
    this.apunte = String.valueOf(rechazo[0]);
    this.concepto = String.valueOf(rechazo[1]);
    this.descripcion = String.valueOf(rechazo[2]);
    this.fecha = FormatDataUtils.convertStringToDate(rechazo[3].toString(), FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    this.importe = new BigDecimal(rechazo[4].toString());
    this.operativa = String.valueOf(rechazo[5]);
    this.tipoComprobante = Integer.valueOf(rechazo[6].toString());
    this.tipoMovimiento = Integer.valueOf(rechazo[7].toString());
    this.auxiliarContable = rechazo[8] != null ? Long.valueOf(rechazo[8].toString()) : null;
    this.cuentaContable = Long.valueOf(rechazo[9].toString());
    this.importeApunteAlc = new BigDecimal(rechazo[10].toString());
  } // RechazoApunteDto

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((apunte == null) ? 0 : apunte.hashCode());
    result = prime * result + ((auxiliarContable == null) ? 0 : auxiliarContable.hashCode());
    result = prime * result + ((concepto == null) ? 0 : concepto.hashCode());
    result = prime * result + ((cuentaContable == null) ? 0 : cuentaContable.hashCode());
    result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
    result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
    result = prime * result + ((importe == null) ? 0 : importe.hashCode());
    result = prime * result + ((importeApunteAlc == null) ? 0 : importeApunteAlc.hashCode());
    result = prime * result + ((operativa == null) ? 0 : operativa.hashCode());
    result = prime * result + ((tipoComprobante == null) ? 0 : tipoComprobante.hashCode());
    result = prime * result + ((tipoMovimiento == null) ? 0 : tipoMovimiento.hashCode());
    result = prime * result + ((tmct0alcId == null) ? 0 : tmct0alcId.hashCode());
    return result;
  } // hashCode

  /*
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RechazoApunteDto other = (RechazoApunteDto) obj;
    if (apunte == null) {
      if (other.apunte != null)
        return false;
    } else if (!apunte.equals(other.apunte))
      return false;
    if (auxiliarContable == null) {
      if (other.auxiliarContable != null)
        return false;
    } else if (!auxiliarContable.equals(other.auxiliarContable))
      return false;
    if (concepto == null) {
      if (other.concepto != null)
        return false;
    } else if (!concepto.equals(other.concepto))
      return false;
    if (cuentaContable == null) {
      if (other.cuentaContable != null)
        return false;
    } else if (!cuentaContable.equals(other.cuentaContable))
      return false;
    if (descripcion == null) {
      if (other.descripcion != null)
        return false;
    } else if (!descripcion.equals(other.descripcion))
      return false;
    if (fecha == null) {
      if (other.fecha != null)
        return false;
    } else if (!fecha.equals(other.fecha))
      return false;
    if (importe == null) {
      if (other.importe != null)
        return false;
    } else if (!importe.equals(other.importe))
      return false;
    if (importeApunteAlc == null) {
      if (other.importeApunteAlc != null)
        return false;
    } else if (!importeApunteAlc.equals(other.importeApunteAlc))
      return false;
    if (operativa == null) {
      if (other.operativa != null)
        return false;
    } else if (!operativa.equals(other.operativa))
      return false;
    if (tipoComprobante == null) {
      if (other.tipoComprobante != null)
        return false;
    } else if (!tipoComprobante.equals(other.tipoComprobante))
      return false;
    if (tipoMovimiento == null) {
      if (other.tipoMovimiento != null)
        return false;
    } else if (!tipoMovimiento.equals(other.tipoMovimiento))
      return false;
    if (tmct0alcId == null) {
      if (other.tmct0alcId != null)
        return false;
    } else if (!tmct0alcId.equals(other.tmct0alcId))
      return false;
    return true;
  } // equals

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "RechazoApunteDto [tmct0alcId=" + tmct0alcId + ", apunte=" + apunte + ", concepto=" + concepto + ", descripcion=" + descripcion
           + ", fecha=" + fecha + ", importe=" + importe + ", operativa=" + operativa + ", tipoComprobante=" + tipoComprobante + ", tipoMovimiento="
           + tipoMovimiento + ", auxiliarContable=" + auxiliarContable + ", cuentaContable=" + cuentaContable + ", importeApunteAlc="
           + importeApunteAlc + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the tmct0alcId
   */
  public Tmct0alcId getTmct0alcId() {
    return tmct0alcId;
  }

  /**
   * @return the apunte
   */
  public String getApunte() {
    return apunte;
  }

  /**
   * @return the concepto
   */
  public String getConcepto() {
    return concepto;
  }

  /**
   * @return the descripcion
   */
  public String getDescripcion() {
    return descripcion;
  }

  /**
   * @return the fecha
   */
  public Date getFecha() {
    return fecha;
  }

  /**
   * @return the importe
   */
  public BigDecimal getImporte() {
    return importe;
  }

  /**
   * @return the operativa
   */
  public String getOperativa() {
    return operativa;
  }

  /**
   * @return the tipoComprobante
   */
  public Integer getTipoComprobante() {
    return tipoComprobante;
  }

  /**
   * @return the tipoMovimiento
   */
  public Integer getTipoMovimiento() {
    return tipoMovimiento;
  }

  /**
   * @return the auxiliarContable
   */
  public Long getAuxiliarContable() {
    return auxiliarContable;
  }

  /**
   * @return the cuentaContable
   */
  public Long getCuentaContable() {
    return cuentaContable;
  }

  // /**
  // * @return the imcomdvo
  // */
  // public BigDecimal getImcomdvo() {
  // return imcomdvo;
  // }
  //
  // /**
  // * @return the imcombrk
  // */
  // public BigDecimal getImcombrk() {
  // return imcombrk;
  // }
  //
  // /**
  // * @return the imajusvb
  // */
  // public BigDecimal getImajusvb() {
  // return imajusvb;
  // }
  //
  // /**
  // * @return the imcomisn
  // */
  // public BigDecimal getImcomisn() {
  // return imcomisn;
  // }
  //
  // /**
  // * @return the imcanoncompeu
  // */
  // public BigDecimal getImcanoncompeu() {
  // return imcanoncompeu;
  // }
  //
  // /**
  // * @return the imcanoncontreu
  // */
  // public BigDecimal getImcanoncontreu() {
  // return imcanoncontreu;
  // }
  //
  // /**
  // * @return the imcanonliqeu
  // */
  // public BigDecimal getImcanonliqeu() {
  // return imcanonliqeu;
  // }
  //
  // /**
  // * @return the canoncontrpagoclte
  // */
  // public Integer getCanoncontrpagoclte() {
  // return canoncontrpagoclte;
  // }
  //
  // /**
  // * @return the imfinsvb
  // */
  // public BigDecimal getImfinsvb() {
  // return imfinsvb;
  // }
  //
  // /**
  // * @return the cdestadocont
  // */
  // public Integer getCdestadocont() {
  // return cdestadocont;
  // }

  /**
   * @return the importeApunteAlc
   */
  public BigDecimal getImporteApunteAlc() {
    return importeApunteAlc;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param tmct0alcId the tmct0alcId to set
   */
  public void setTmct0alcId(Tmct0alcId tmct0alcId) {
    this.tmct0alcId = tmct0alcId;
  }

  /**
   * @param apunte the apunte to set
   */
  public void setApunte(String apunte) {
    this.apunte = apunte;
  }

  /**
   * @param concepto the concepto to set
   */
  public void setConcepto(String concepto) {
    this.concepto = concepto;
  }

  /**
   * @param descripcion the descripcion to set
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  /**
   * @param fecha the fecha to set
   */
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  /**
   * @param importe the importe to set
   */
  public void setImporte(BigDecimal importe) {
    this.importe = importe;
  }

  /**
   * @param operativa the operativa to set
   */
  public void setOperativa(String operativa) {
    this.operativa = operativa;
  }

  /**
   * @param tipoComprobante the tipoComprobante to set
   */
  public void setTipoComprobante(Integer tipoComprobante) {
    this.tipoComprobante = tipoComprobante;
  }

  /**
   * @param tipoMovimiento the tipoMovimiento to set
   */
  public void setTipoMovimiento(Integer tipoMovimiento) {
    this.tipoMovimiento = tipoMovimiento;
  }

  /**
   * @param auxiliarContable the auxiliarContable to set
   */
  public void setAuxiliarContable(Long auxiliarContable) {
    this.auxiliarContable = auxiliarContable;
  }

  /**
   * @param cuentaContable the cuentaContable to set
   */
  public void setCuentaContable(Long cuentaContable) {
    this.cuentaContable = cuentaContable;
  }

  // /**
  // * @param imcomdvo the imcomdvo to set
  // */
  // public void setImcomdvo(BigDecimal imcomdvo) {
  // this.imcomdvo = imcomdvo;
  // }
  //
  // /**
  // * @param imcombrk the imcombrk to set
  // */
  // public void setImcombrk(BigDecimal imcombrk) {
  // this.imcombrk = imcombrk;
  // }
  //
  // /**
  // * @param imajusvb the imajusvb to set
  // */
  // public void setImajusvb(BigDecimal imajusvb) {
  // this.imajusvb = imajusvb;
  // }
  //
  // /**
  // * @param imcomisn the imcomisn to set
  // */
  // public void setImcomisn(BigDecimal imcomisn) {
  // this.imcomisn = imcomisn;
  // }
  //
  // /**
  // * @param imcanoncompeu the imcanoncompeu to set
  // */
  // public void setImcanoncompeu(BigDecimal imcanoncompeu) {
  // this.imcanoncompeu = imcanoncompeu;
  // }
  //
  // /**
  // * @param imcanoncontreu the imcanoncontreu to set
  // */
  // public void setImcanoncontreu(BigDecimal imcanoncontreu) {
  // this.imcanoncontreu = imcanoncontreu;
  // }
  //
  // /**
  // * @param imcanonliqeu the imcanonliqeu to set
  // */
  // public void setImcanonliqeu(BigDecimal imcanonliqeu) {
  // this.imcanonliqeu = imcanonliqeu;
  // }
  //
  // /**
  // * @param canoncontrpagoclte the canoncontrpagoclte to set
  // */
  // public void setCanoncontrpagoclte(Integer canoncontrpagoclte) {
  // this.canoncontrpagoclte = canoncontrpagoclte;
  // }
  //
  // /**
  // * @param imfinsvb the imfinsvb to set
  // */
  // public void setImfinsvb(BigDecimal imfinsvb) {
  // this.imfinsvb = imfinsvb;
  // }
  //
  // /**
  // * @param cdestadocont the cdestadocont to set
  // */
  // public void setCdestadocont(Integer cdestadocont) {
  // this.cdestadocont = cdestadocont;
  // }

  /**
   * @param importeApunteAlc the importeApunteAlc to set
   */
  public void setImporteApunteAlc(BigDecimal importeApunteAlc) {
    this.importeApunteAlc = importeApunteAlc;
  }

} // RechazoApunteDto
