package sibbac.business.wrappers.database.desglose.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.utils.FormatDataUtils;

public class DesgloseTmct0aloHelper {

  /* Constantes */
  private static final Logger LOG = LoggerFactory.getLogger(DesgloseTmct0aloHelper.class);

  public static void initializeAlo(DesgloseRecordDTO dto, Tmct0AloData alo, Tmct0alo aloCopia) {
    LOG.trace("[DesgloseTmct0aloBo :: initializeAloOperacionEspecial]INIT initializeAlo.");
    Tmct0aloId id = aloCopia.getId();
    id = new Tmct0aloId();
    id.setNbooking(alo.getId().getNbooking());
    id.setNuorden(alo.getId().getNuorden());

    aloCopia.setId(id);
    BigDecimal numTitulos = dto.getTitulosEjecutados();
    aloCopia.setNutitcli(numTitulos);
    aloCopia.setNutitpen(numTitulos);
    BigDecimal imcammed = alo.getImcammed();
    BigDecimal imcamnet = alo.getImcamnet();
    BigDecimal imtobru = FormatDataUtils.roundedScale(numTitulos.multiply(imcammed), 2);
    BigDecimal imtonet = FormatDataUtils.roundedScale(numTitulos.multiply(imcamnet), 2);
    aloCopia.setImtotbru(imtobru);
    aloCopia.setImtotnet(imtonet);
    aloCopia.setEutotbru(imtobru);
    aloCopia.setEutotnet(imtonet);
    aloCopia.setImnetbrk(null);
    aloCopia.setEunetbrk(null);
    aloCopia.setImcomisn(dto.getCorretajes());
    aloCopia.setImcomieu(dto.getCorretajes());
    aloCopia.setImcomdev(dto.getPayerComission());
    aloCopia.setEucomdev(dto.getPayerComission());
    aloCopia.setImfibrdv(null);
    aloCopia.setImfibreu(null);
    aloCopia.setImsvb(null);
    aloCopia.setImsvbeu(null);
    aloCopia.setImbrmerc(imtobru);
    aloCopia.setImbrmreu(imtobru);
    aloCopia.setImgastos(null);
    aloCopia.setImgatdvs(null);
    aloCopia.setImimpdvs(null);
    aloCopia.setImimpeur(null);
    aloCopia.setImcconeu(null);
    aloCopia.setImcliqeu(null);
    aloCopia.setImdereeu(null);
    aloCopia.setRfparten(dto.getNumeroOrden());
    aloCopia.setNuoprout(alo.getNuoprout());
    if (dto.getNupareje() == null) {
      aloCopia.setNupareje((short) 0);
    }
    else {
      aloCopia.setNupareje(dto.getNupareje());
    }
    aloCopia.setImliq(null);
    aloCopia.setImeurliq(null);
    aloCopia.setImajliq(null);
    aloCopia.setImajeliq(null);
    aloCopia.setImgasliqdv(null);
    aloCopia.setImgasliqeu(null);
    aloCopia.setImgasgesdv(null);
    aloCopia.setImgasgeseu(null);
    aloCopia.setImgastotdv(null);
    aloCopia.setImgastoteu(null);
    aloCopia.setCdholder(null);
    if (aloCopia.getTmct0estadoByCdestadoasig() == null) {
      aloCopia.setTmct0estadoByCdestadoasig(new Tmct0estado());
    }
    aloCopia.getTmct0estadoByCdestadoasig().setIdestado(
        EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE.getId());
    aloCopia.setCdestadotit(EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());
    aloCopia.setReftitular(dto.getRefTitu());
    aloCopia.setImcorretajesvfinaldv(null);
    aloCopia.setImcomclteeu(null);
    aloCopia.setTmct0estadoByCdestadocont(null);
    if (aloCopia.getTmct0estadoByCdestadocont() == null) {
      aloCopia.setTmct0estadoByCdestadocont(new Tmct0estado());
    }
    aloCopia.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
    aloCopia.setTmct0sta(new Tmct0sta());
    if (alo.getTmct0sta() != null) {
      aloCopia.getTmct0sta().setId(alo.getTmct0sta().getId());
    }
    else {
      aloCopia.getTmct0sta().setId(new Tmct0staId("800", "ASIGNACION"));
    }
    aloCopia.setImcanoncompdv(null);
    aloCopia.setImcanoncompeu(null);
    aloCopia.setImcanoncontrdv(null);
    aloCopia.setImcanoncontreu(dto.getCanCont());
    aloCopia.setImcanonliqdv(null);
    aloCopia.setImcanonliqeu(null);
    aloCopia.setNutitfallidos(null);
    if (StringUtils.isNotBlank(dto.getNumeroReferenciaOrden())) {
      aloCopia.setRefCliente(dto.getNumeroReferenciaOrden());
    }
    else {
      aloCopia.setRefCliente(alo.getRefCliente());
    }
    dto.setFhLote(new Date());
    dto.setHlote(new Date());
    List<Date> listaFhEjec = new ArrayList<Date>();
    List<Date> listaHejec = new ArrayList<Date>();
    List<String> listaCDOrgNeg = new ArrayList<String>();
    List<String> listaNumEjeCu = new ArrayList<String>();
    List<String> listaNEjecMer = new ArrayList<String>();
    dto.setListaFhEjec(listaFhEjec);
    dto.setListaHejec(listaHejec);
    dto.setListaCDOrgNeg(listaCDOrgNeg);
    dto.setListaNumEjeCu(listaNumEjeCu);
    dto.setListaNEjecMer(listaNEjecMer);
    dto.setListaTitulos(new ArrayList<BigDecimal>());

    dto.setFLiqui(alo.getFevalor());
    dto.setDivisa(alo.getCdmoniso());

    Character abcanCom = 'N';
    if (alo.getCanoncomppagoclte() == 1) {
      abcanCom = 'S';
    }

    dto.setCorretajes(dto.getCorretajes());
    dto.setCanCont(dto.getCanCont());
    dto.setAbcanCom(abcanCom);
    dto.setCanComp(BigDecimal.ZERO);

    LOG.trace("[DesgloseTmct0aloBo :: initializeAloOperacionEspecial]INIT initializeAlo fin.");
  }

  public static void initializeAloOperacionEspecial(DesgloseRecordDTO dto, Tmct0AloData alo, Tmct0alo aloCopia) {
    initializeAlo(dto, alo, aloCopia);
    aloCopia.getId().setNucnfclt(String.format("%s%s", Tmct0alo.CTE_PARTENON, dto.getNumeroReferenciaOrden()));
    aloCopia.setRefCliente(dto.getNumeroReferenciaOrden());

    if (aloCopia.getTmct0estadoByCdestadoasig() == null) {
      aloCopia.setTmct0estadoByCdestadoasig(new Tmct0estado());
    }
    aloCopia.getTmct0estadoByCdestadoasig().setIdestado(
        EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE.getId());
    aloCopia.setCdestadotit(EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());

    aloCopia.setTmct0estadoByCdestadocont(null);
    if (aloCopia.getTmct0estadoByCdestadocont() == null) {
      aloCopia.setTmct0estadoByCdestadocont(new Tmct0estado());
    }
    aloCopia.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
    aloCopia.setTmct0sta(new Tmct0sta());
    if (alo.getTmct0sta() != null) {
      aloCopia.getTmct0sta().setId(alo.getTmct0sta().getId());
    }
    else {
      aloCopia.getTmct0sta().setId(new Tmct0staId("800", "ASIGNACION"));
    }

  }

  public static void initializeAloRevisionDesglosesIf(DesgloseRecordDTO dto, int index, Tmct0AloData alo,
      Tmct0alo aloCopia) {

    initializeAlo(dto, alo, aloCopia);

    aloCopia.getId().setNucnfclt(
        String.format("PTI%s%s", FormatDataUtils.convertDateToString(new Date()),
            Long.toString(index, Character.MAX_RADIX)));

    if (aloCopia.getTmct0estadoByCdestadoasig() == null) {
      aloCopia.setTmct0estadoByCdestadoasig(new Tmct0estado());
    }
    aloCopia.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId());
    aloCopia.setCdestadotit(EstadosEnumerados.TITULARIDADES.TRATANDOSE.getId());

    aloCopia.setTmct0estadoByCdestadocont(null);
    if (aloCopia.getTmct0estadoByCdestadocont() == null) {
      aloCopia.setTmct0estadoByCdestadocont(new Tmct0estado());
    }
    aloCopia.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.TRATANDOSE.getId());
    aloCopia.setTmct0sta(new Tmct0sta());
    if (alo.getTmct0sta() != null) {
      aloCopia.getTmct0sta().setId(alo.getTmct0sta().getId());
    }
    else {
      aloCopia.getTmct0sta().setId(new Tmct0staId("800", "ASIGNACION"));
    }

  }

  public static Tmct0alo getNewCopiaTmct0aloDesgloseIf(final DesgloseRecordDTO dto, final int index,
      List<Tmct0grupoejecucion> tmct0grupoejecucions, List<Tmct0ejecucionalocation> ejealos) {
    Tmct0alo aloCopia = Tmct0AloData.dataToEntity(dto.getAloDataOperacionesEspecialesPartenon().getAloData());
    List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listaEjecucionAlocacionData = dto
        .getEjecucionesUtilizadas();

    DesgloseTmct0aloHelper.initializeAloRevisionDesglosesIf(dto, index, dto.getAloDataOperacionesEspecialesPartenon()
        .getAloData(), aloCopia);

    // Inicializa
    HashMap<Long, Tmct0grupoejecucion> hmGrupoejecucion = new HashMap<Long, Tmct0grupoejecucion>();
    for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionAlocacionData : listaEjecucionAlocacionData) {
      Tmct0grupoejecucion grupoEjecucion = hmGrupoejecucion.get(ejecucionAlocacionData.getIdgrupoeje());
      if (grupoEjecucion == null) {
        grupoEjecucion = new Tmct0grupoejecucion();
        DesgloseTmct0grupoejecucionHelper.inicializeTmct0grupoejecucion(grupoEjecucion, aloCopia,
            ejecucionAlocacionData);
        tmct0grupoejecucions.add(grupoEjecucion);
      }

      grupoEjecucion.setNutitulos(grupoEjecucion.getNutitulos().add(ejecucionAlocacionData.getNutitulos()));

      Tmct0ejecucionalocation ejealo = new Tmct0ejecucionalocation();
      DesgloseTmct0ejecucionalocationHelper.inizializeTmct0ejecucionAlocation(ejealo, grupoEjecucion,
          ejecucionAlocacionData.getNutitulos(), ejecucionAlocacionData.getTmct0ejeId());

      ejealo.setNutitulosPendientesDesglose(BigDecimal.ZERO);

      ejealos.add(ejealo);
      grupoEjecucion.getTmct0ejecucionalocations().add(ejealo);
    }
    aloCopia.setTmct0grupoejecucions(tmct0grupoejecucions);
    return aloCopia;
  }

  public static Tmct0alo getNewCopiaTmct0aloOperacionEspecial(final DesgloseRecordDTO dto,
      List<Tmct0grupoejecucion> tmct0grupoejecucions, List<Tmct0ejecucionalocation> ejealos) {
    Tmct0alo aloCopia = Tmct0AloData.dataToEntity(dto.getAloDataOperacionesEspecialesPartenon().getAloData());
    List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listaEjecucionAlocacionData = dto
        .getEjecucionesUtilizadas();

    DesgloseTmct0aloHelper.initializeAloOperacionEspecial(dto, dto.getAloDataOperacionesEspecialesPartenon()
        .getAloData(), aloCopia);

    // Inicializa
    HashMap<Long, Tmct0grupoejecucion> hmGrupoejecucion = new HashMap<Long, Tmct0grupoejecucion>();
    for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionAlocacionData : listaEjecucionAlocacionData) {
      Tmct0grupoejecucion grupoEjecucion = hmGrupoejecucion.get(ejecucionAlocacionData.getIdgrupoeje());
      if (grupoEjecucion == null) {
        grupoEjecucion = new Tmct0grupoejecucion();
        DesgloseTmct0grupoejecucionHelper.inicializeTmct0grupoejecucion(grupoEjecucion, aloCopia,
            ejecucionAlocacionData);
        tmct0grupoejecucions.add(grupoEjecucion);
      }

      grupoEjecucion.setNutitulos(grupoEjecucion.getNutitulos().add(ejecucionAlocacionData.getNutitulos()));

      Tmct0ejecucionalocation ejealo = new Tmct0ejecucionalocation();
      DesgloseTmct0ejecucionalocationHelper.inizializeTmct0ejecucionAlocation(ejealo, grupoEjecucion,
          ejecucionAlocacionData.getNutitulos(), ejecucionAlocacionData.getTmct0ejeId());

      ejealo.setNutitulosPendientesDesglose(BigDecimal.ZERO);

      ejealos.add(ejealo);
      grupoEjecucion.getTmct0ejecucionalocations().add(ejealo);
    }
    aloCopia.setTmct0grupoejecucions(tmct0grupoejecucions);
    return aloCopia;
  }

  /**
   * Dada una lista de nureford separadas por comas y entre comillas, genera su
   * correspondiente 'PARTENONXXXXXXXX'
   * 
   * @param nurefordList
   * @return
   */
  public static String getListPartenonNurefordCta123(String nurefordList) {
    String[] nurefordListCta123 = nurefordList.replace("'", "").trim().split(",");
    String nurefordListCta123St = "";
    int i = 0;
    for (String nureford : nurefordListCta123) {
      i++;
      nurefordListCta123St += "'PARTENON" + nureford + "'";
      if (i == 1) {
        LOG.debug(
            "[DesgloseTmct0aloHelper :: getListPartenonNurefordCta123] Buscando corretaje ordenes cta 123-barrido san.Primer Nureford: {}",
            nurefordListCta123St);
      }
      if (i != nurefordListCta123.length) {
        nurefordListCta123St += ", ";
      }

    }

    return nurefordListCta123St;
  }

}
