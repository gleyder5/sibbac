package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;

@Repository
public interface Tmct0movimientooperacionnuevaeccDao extends JpaRepository<Tmct0movimientooperacionnuevaecc, Long> {

  List<Tmct0movimientooperacionnuevaecc> findBytmct0desglosecamara(Tmct0desglosecamara desglosecamara);

  List<Tmct0movimientooperacionnuevaecc> findBycdoperacionecc(String Ccdoperacionecc);

  @Query("SELECT m FROM Tmct0movimientooperacionnuevaecc m WHERE m.tmct0movimientoecc.idmovimiento=:idMovimiento")
  List<Tmct0movimientooperacionnuevaecc> findByIdMovimiento(@Param("idMovimiento") Long idMovimiento);

  List<Tmct0movimientooperacionnuevaecc> findByNudesglose(Long nudesglose);

  @Query("SELECT m FROM Tmct0movimientooperacionnuevaecc m WHERE m.nudesglose = :pnudesglose AND m.tmct0movimientoecc.cdestadomov  in :pcdestadosmov ")
  List<Tmct0movimientooperacionnuevaecc> findByInCdestadomovAndNudesglose(
      @Param("pcdestadosmov") List<String> cdestadosmov, @Param("pnudesglose") Long nudesglose);

  @Query("SELECT m FROM Tmct0movimientooperacionnuevaecc m WHERE m.idejecucionalocation = :pidejecucionalocation AND m.tmct0movimientoecc.cdestadomov  in :pcdestadosmov ")
  List<Tmct0movimientooperacionnuevaecc> findByInCdestadomovAndIdejecucionalocation(
      @Param("pcdestadosmov") List<String> cdestadosmov, @Param("pidejecucionalocation") Long idejecucionalocation);

  @Query("SELECT sum(mn.corretaje) FROM Tmct0movimientooperacionnuevaecc mn, Tmct0movimientoecc m, Tmct0desglosecamara d, Tmct0alc c "
      + " WHERE mn.tmct0movimientoecc.idmovimiento= m.idmovimiento AND m.tmct0desglosecamara.iddesglosecamara=d.iddesglosecamara "
      + " AND  d.tmct0alc.id = c.id AND c.id = :alcId AND m.cdestadomov  in :pcdestadosmov AND d.tmct0estadoByCdestadoasig.idestado > 0")
  BigDecimal sumCorretajeByInCdestadomovAndTmct0alcId(@Param("pcdestadosmov") List<String> cdestadosmov,
      @Param("alcId") Tmct0alcId alcId);

  @Modifying
  @Query("UPDATE Tmct0movimientooperacionnuevaecc mn SET mn.idMovimientoFidessa = :idMovimientoFidessa, mn.auditFechaCambio = :fhaudit, mn.auditUser = :cdusuaud "
      + " WHERE mn.cdoperacionecc = :cdoperacionecc AND mn.idMovimientoFidessa != :idMovimientoFidessa ")
  int updateIdMovimientoFidessaByCdoperacionecc(@Param("idMovimientoFidessa") long idMovimientoFidessa,
      @Param("cdoperacionecc") String cdoperacionecc, @Param("fhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);

  // @Query(value =
  // "SELECT NUOPERACIONINIC, IMEFECTIVO, IMPRECIO, NUDESGLOSE FROM BSNBPSQL.TMCT0MOVIMIENTOOPERACIONNUEVAECC WHERE IDDESGLOSECAMARA = :desglosecamara GROUP BY NUOPERACIONINIC, IMEFECTIVO, IMPRECIO, NUDESGLOSE",
  // nativeQuery = true)
  // List<Object[]>
  // findByTmct0desglosecamaraOrderByFields(@Param("desglosecamara") Long
  // desglosecamara);

} // Tmct0movimientooperacionnuevaeccDao
