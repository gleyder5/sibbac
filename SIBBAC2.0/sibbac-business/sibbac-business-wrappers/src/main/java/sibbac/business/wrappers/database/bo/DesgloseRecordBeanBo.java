package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.OperacionEspecialProcesarDesgloseRecordBean;
import sibbac.business.wrappers.database.dao.DesgloseRecordBeanDao;
import sibbac.business.wrappers.database.dao.DesgloseRecordDaoImpl;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0bokDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.dao.Tmct0referenciatitularDao;
import sibbac.business.wrappers.database.data.Tmct0AloDataOperacionesEspecialesPartenon;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0alcHelper;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0aloHelper;
import sibbac.business.wrappers.database.dto.DesgloseDTO;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.DesgloseRecordEjecucionesDTO;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoOperacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class DesgloseRecordBeanBo extends AbstractBo<DesgloseRecordBean, Long, DesgloseRecordBeanDao> {

  private static final String PROCESAR_DESGLOSES = "procesarDesgloses";
  protected static final Logger LOG = LoggerFactory.getLogger(DesgloseRecordBeanBo.class);
  private static final String TAG = DesgloseRecordBeanBo.class.getName();
  // private static final String DEFAULT_CDSETMKT = "XMCE";

  @Value("${sibbac.wrappers.operacion.especial.1015.codigos.comerciales:23,24,25,26,28,30,91}")
  private String codigosComerciales;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0bokDao tmct0bokDao;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private DesgloseTmct0alcHelper desgloseAlcHelper;

  @Autowired
  private Tmct0desgloseCamaraBo desgloseCamaraBo;

  @Autowired
  private Tmct0desgloseclititBo desgloseClititBo;

  @Autowired
  private Tmct0infocompensacionDao infoCompensacionDao;

  @Autowired
  private Tmct0movimientoeccBo movimientoEccBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movimientoOpNuevaBo;

  @Autowired
  private Tmct0grupoejecucionBo grupoEjecucionBo;

  @Autowired
  private Tmct0ejecucionalocationBo ealoBo;

  @Autowired
  private DesgloseRecordDaoImpl daoImpl;

  @Autowired
  private DesgloseBO desgloseBo;

  @Autowired
  private PartenonRecordBeanBo partenonRecoredBeanBo;

  @Autowired
  private Tmct0enviotitularidadDao envioTitularidadDao;

  @Autowired
  private Tmct0referenciatitularDao referenciaTitularDao;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao desgloseCamaraEnvioRtDao;

  @Autowired
  private ApplicationContext ctx;

  public DesgloseRecordBean findByPartenonRecordBeanId(Long partenonRecordBeanId) {
    return dao.findByPartenonRecordBeanId(partenonRecordBeanId);
  }

  public Integer updateProcesadoByPartenonRecordBeanId(Long partenonRecordBeanId, Integer procesado) {
    return dao.updateProcesadoByPartenonRecordBeanId(partenonRecordBeanId, procesado, new Date());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public Integer updateProcesadoPdteCargarTitularDesglosesRecordBeanOrdenComercialNoEnvian1015(
      List<String> codigosComerciales) {

    return dao
        .updateProcesadoAndByPorcesadoAndNotInOrdenComercial(
            EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSE_EJECUTADO_CORRECTO_ALLO_CREADO.getId(),
            codigosComerciales,
            EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR.getId(),
            new Date());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void revisarTitulosDesgloses() {
    LOG.info("[revisarTitulosDesgloses] inicio.");

    List<Integer> estados = new ArrayList<Integer>();
    estados.add(EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.INICIO_PDTE_REVISAR_DESGLOSES.getId());
    estados.add(EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_ERRONEOS.getId());
    LOG.info("[revisarTitulosDesgloses] Buscando desglose record bean procesado in {}...", estados);
    List<DesgloseRecordDTO> listaDesgloses = this.dao.findAllOrdenesByInProcesado(estados);

    LOG.info("[revisarTitulosDesgloses] Encontrados {} desgloses.", listaDesgloses.size());
    // Comprobamos que hay desgloses sin procesar
    if (CollectionUtils.isNotEmpty(listaDesgloses)) {
      BigDecimal numeroTitulos = null;
      String sNumeroOrden;
      // recorremos los desgloses recuperados
      for (DesgloseRecordDTO desgloseRecordDto : listaDesgloses) {

        sNumeroOrden = desgloseRecordDto.getNumeroOrden();

        LOG.info("[revisarTitulosDesgloses] Revisando Nuorden: {}.", sNumeroOrden);

        numeroTitulos = dao.sumAllTitulosByNumordenAndPorcesadoLt(sNumeroOrden,
            EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS.getId());

        if (numeroTitulos != null && numeroTitulos.compareTo(BigDecimal.ZERO) > 0) {
          revisarTitulosDesglosesNumordenActual(sNumeroOrden, numeroTitulos);
        }
        else {
          LOG.warn("[revisarTitulosDesgloses] No encontrado titulos para la orden {} en estado {}", sNumeroOrden,
              EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.INICIO_PDTE_REVISAR_DESGLOSES.getId());
        }

      }
    }// if (listaDesgloses != null)
    LOG.info("[revisarTitulosDesgloses] fin.");
  }// public void recepcionDesgloses() {

  @Transactional
  private void revisarTitulosDesglosesNumordenActual(String sNumeroOrdenActual, BigDecimal numeroTitulos) {
    LOG.debug("[revisarTitulosDesglosesNumordenActual] inicio.");
    if (StringUtils.isNotBlank(sNumeroOrdenActual)) {
      LOG.info("[revisarTitulosDesglosesNumordenActual] Revisando desgloses de orden {}.", sNumeroOrdenActual);
      // sNumeroOrdenActual = this.completarNumeroOrden(sNumeroOrdenActual);
      Tmct0ord ord = ordBo.findByNuorden(sNumeroOrdenActual);
      if (ord != null) {
        Integer procesado = EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_ERRONEOS.getId();// revisado
                                                                                                                 // faltan
                                                                                                                 // titulos
        TipoOperacion tipoOperacion = TipoOperacion.COMPRA_CONTADO;
        Character cdtpoper = ord.getCdtpoper();
        if (cdtpoper.compareTo(new Character('V')) == 0) {
          tipoOperacion = TipoOperacion.VENTA_CONTADO;
        }
        if (numeroTitulos.compareTo(ord.getNutiteje()) == 0) {
          procesado = EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS
              .getId();
          LOG.info("[revisarTitulosDesglosesNumordenActual] La Orden {} esta lista para partir en los desgloses.",
              sNumeroOrdenActual);
          LOG.info("[revisarTitulosDesglosesNumordenActual] Los desgloses se van a marcar en procesado == 2-DESGLOSE OK.");
        }
        else {
          LOG.warn("[revisarTitulosDesglosesNumordenActual] Titulos Ord:{} Titulos Desgloses:{} NO COINCIDEN.",
              ord.getNutiteje(), numeroTitulos);
          LOG.warn("[revisarTitulosDesglosesNumordenActual] Los desgloses se van a marcar en procesado == 1-DESGLOSE KO.");
        }

        Integer countActualizados = dao.updateProcesadoAndTipoOperacionByNumordenAndPorcesado(sNumeroOrdenActual,
            EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.INICIO_PDTE_REVISAR_DESGLOSES.getId(), procesado,
            tipoOperacion, new Date());

        if (procesado != EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_ERRONEOS.getId()) {
          countActualizados += dao.updateProcesadoAndTipoOperacionByNumordenAndPorcesado(sNumeroOrdenActual,
              EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_ERRONEOS.getId(), procesado,
              tipoOperacion, new Date());
        }

        if (countActualizados > 0) {
          List<Tmct0bok> listBookings = ord.getTmct0boks();
          if (CollectionUtils.isNotEmpty(listBookings)) {
            String estado = "";
            if (procesado == EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS
                .getId()) {
              estado = "DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS";
            }
            else {
              estado = "DESGLOSES_ERRONEOS_TITULOS_INCORRECTOS";
            }
            String msg = MessageFormat.format(
                "Se han actualizado {0} 40s a estado {1}-{2} Tit. Eje. Ord: {3} Tit.40s: {4}", countActualizados,
                procesado, estado, ord.getNutiteje().setScale(2, RoundingMode.HALF_UP),
                numeroTitulos.setScale(2, RoundingMode.HALF_UP));
            for (Tmct0bok tmct0bok : listBookings) {
              logBo.insertarRegistro(tmct0bok.getId().getNuorden(), tmct0bok.getId().getNbooking(), msg, tmct0bok
                  .getTmct0estadoByCdestadoasig().getNombre(), "SIBBAC20");
            }
          }
        }

        LOG.info("[revisarTitulosDesglosesNumordenActual] se han marcado {} desgloses a estado {} de la orden {}",
            countActualizados, procesado, sNumeroOrdenActual);

      }
      else {
        LOG.warn("[revisarTitulosDesglosesNumordenActual] No encontrada la orden : {}", sNumeroOrdenActual);
      }
    }// if (sNumeroOrdenActual != null)
    LOG.debug("[revisarTitulosDesglosesNumordenActual] fin.");
  }

  // countFindAllDTOPageable metodo que recupera todas las filas.
  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param isin
   * @return
   */
  public Integer countFindAllDTOPageable(Date fechaDesde, Date fechaHasta, List<String> isin) {
    return daoImpl.countFindAllDTOPageable(fechaDesde, fechaHasta, isin);

  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param isin
   * @return
   */
  public Integer countFindHuerfanosAllDTOPageable(Date fechaDesde, Date fechaHasta, List<String> isin) {
    return daoImpl.countFindHuerfanosAllDTOPageable(fechaDesde, fechaHasta, isin);

  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param isin
   * @return
   */
  public List<DesgloseDTO> findDTOForService(Date fechaDesde, Date fechaHasta, List<String> isin) {
    return daoImpl.findDTOForService(fechaDesde, fechaHasta, isin);

  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param isin
   * @param startPosition
   * @param maxResult
   * @return
   */
  public List<DesgloseDTO> findAllDTOPageable(Date fechaDesde, Date fechaHasta, List<String> isin,
      Integer startPosition, Integer maxResult) {
    return daoImpl.findAllDTOPageable(fechaDesde, fechaHasta, isin, startPosition, maxResult);
  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param isin
   * @param startPosition
   * @param maxResult
   * @return
   */
  public List<DesgloseDTO> findAllHuerfanoDTOPageable(Date fechaDesde, Date fechaHasta, List<String> isin,
      Integer startPosition, Integer maxResult) {
    return daoImpl.findAllHuerfanoDTOPageable(fechaDesde, fechaHasta, isin, startPosition, maxResult);
  }

  public List<DesgloseRecordDTO> findByNumOrden(String numOrden, int procesado) {

    List<DesgloseRecordDTO> listaDesglosesRecordDTO = dao.findAllNumOrden(numOrden, procesado);
    return listaDesglosesRecordDTO;
  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param isin
   * @return
   */
  public List<DesgloseDTO> findHuerfanoDTOForService(Date fechaDesde, Date fechaHasta, List<String> isin) {
    return daoImpl.findHuerfanoDTOForService(fechaDesde, fechaHasta, isin);

  }

  public List<DesgloseRecordDTO> findAllProcesado(Integer procesado) {
    return dao.findAllProcesado(procesado);
  }

  public List<DesgloseRecordDTO> findAllOrdenesProcesadas(Integer procesado) {
    return dao.findAllOrdenesProcesadas(procesado);
  }

  public Integer countByNumeroOrden(String numeroOrden) {
    return dao.countByNumeroOrden(numeroOrden);
  }

  public List<DesgloseRecordDTO> findAllTitulosEjecutadosByListNureford(List<String> nurefords) {
    return dao.findAllTitulosEjecutadosByListNureford(nurefords);
  }

  public List<DesgloseRecordDTO> findAllDesgloseRecordDTOByListNurefordToEnvio1015(List<String> nurefords) {
    return dao.findAllDesgloseRecordDTOByListNurefordToEnvio1015(nurefords);
  }

  public DesgloseRecordDTO findDesgloseRecordDTOById(Long id) {
    return dao.findDesgloseRecordDTOById(id);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void procesarDesgloses(List<OperacionEspecialProcesarDesgloseRecordBean> desgloses)
      throws SIBBACBusinessException {
    Map<Tmct0aloId, Tmct0alo> alos = new HashMap<Tmct0aloId, Tmct0alo>();
    // Map<Tmct0ejeId, Tmct0eje> ejes= new HashMap<Tmct0ejeId, Tmct0eje>();
    for (OperacionEspecialProcesarDesgloseRecordBean desgloseRecordDTO : desgloses) {
      this.procesarDesgloseOperacionEspecial(desgloseRecordDTO.getDesglose(), alos);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void procesarDesgloseOperacionEspecial(DesgloseRecordDTO dto, Map<Tmct0aloId, Tmct0alo> alos)
      throws SIBBACBusinessException {
    Tmct0AloDataOperacionesEspecialesPartenon aloDataOperacionesEspecialesPartenon = dto
        .getAloDataOperacionesEspecialesPartenon();
    List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listaEjecucionAlocacionData = dto
        .getEjecucionesUtilizadas();
    LOG.trace("{}::{}  INIT hay desgloses validados.", TAG, PROCESAR_DESGLOSES);
    LOG.trace("{}::{} Procesando: {} .", TAG, PROCESAR_DESGLOSES, dto);
    LOG.trace("{}::{} Procesando: {} .", TAG, PROCESAR_DESGLOSES, aloDataOperacionesEspecialesPartenon);

    LOG.trace("{}::{} Buscando allo...", TAG, PROCESAR_DESGLOSES);

    if (CollectionUtils.isNotEmpty(listaEjecucionAlocacionData)) {
      LOG.trace("{}::{} Allo.id = {}", TAG, PROCESAR_DESGLOSES, aloDataOperacionesEspecialesPartenon.getAloData()
          .getId());

      if (StringUtils.isBlank(dto.getRefTitu())) {
        List<String> ccvs = partenonRecoredBeanBo.findAllCcvByNureford(dto.getNumeroReferenciaOrden());
        if (CollectionUtils.isNotEmpty(ccvs)) {
          dto.setCcv(ccvs.get(0));
          dto.setRefTitu(dto.getCcv());
          dto.setRefAdic(dto.getCcv());
        }
        else {
          LOG.warn("{}::{} no encontrada ccv para el nureford {}", TAG, PROCESAR_DESGLOSES,
              dto.getNumeroReferenciaOrden());
        }
      }

      // Inicializa
      List<Tmct0grupoejecucion> tmct0grupoejecucions = new LinkedList<Tmct0grupoejecucion>();
      List<Tmct0ejecucionalocation> ejealos = new LinkedList<Tmct0ejecucionalocation>();

      Tmct0alo aloCopia = DesgloseTmct0aloHelper.getNewCopiaTmct0aloOperacionEspecial(dto, tmct0grupoejecucions,
          ejealos);
      aloCopia = aloBo.save(aloCopia);

      grupoEjecucionBo.save(tmct0grupoejecucions);
      ejealos = ealoBo.save(ejealos);

      LOG.trace("{}::{} Grabando copia del Allo...", TAG, PROCESAR_DESGLOSES);
      Tmct0alc alc = desgloseAlcHelper.crearDesglosesAlcOperacionEspecial(dto, ejealos, aloCopia);

      aloCopia.getTmct0alcs().add(alc);

      List<Tmct0alc> listAlcs = new ArrayList<Tmct0alc>();
      listAlcs.add(alc);
      alcBo.setProvisionalNudesgloses(alc);

      try {

        CalculosEconomicosNacionalBo calculos = ctx.getBean(CalculosEconomicosNacionalBo.class, aloCopia, listAlcs,
            dto.getCanonesConfig(), dto.getTclicomDto());
        calculos.calcularDatosEconomicos();

      }
      catch (Exception e) {
        throw new SIBBACBusinessException("Incidencia calculos Economicos AlloID : " + aloCopia.getId(), e);
      }

      alcBo.removeProvisionalNudesgloses(alc);

      alcBo.persistirAlcs(aloCopia, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
          EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());

      if (dto.isLastDesglose() && !aloDataOperacionesEspecialesPartenon.getSavedAlo()) {
        LOG.info("{}::{} Se han terminado los titulos del Allo. {}", TAG, PROCESAR_DESGLOSES,
            aloDataOperacionesEspecialesPartenon.getAloData().getId());

        aloDataOperacionesEspecialesPartenon.setSavedAlo(Boolean.TRUE);
        aloBo.updateCdestadoasigByTmct0aloId(aloDataOperacionesEspecialesPartenon.getAloData().getId(),
            EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());

        if (aloDataOperacionesEspecialesPartenon.getAloData().getTmct0bokId() != null) {
          tmct0bokDao.updateCdestadoasigByTmct0bokId(aloDataOperacionesEspecialesPartenon.getAloData().getTmct0bokId(),
              EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), new Date());
        }

      }

      int setProcesado = EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSE_EJECUTADO_CORRECTO_ALLO_CREADO
          .getId();

      if (dto.getOrdenComercial() == null || !codigosComerciales.contains(dto.getOrdenComercial().getOrdenComercial())) {
        setProcesado = EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR
            .getId();
      }

      LOG.trace("{}::{} Actualizando DesgloseRecordBean.id == {} {}-Procesado", TAG, PROCESAR_DESGLOSES, dto.getId(),
          setProcesado);
      Integer countUpdate = dao.updateProcesadoById(dto.getId(), setProcesado, new Date());
      LOG.trace("{}::{} Actualizando DesgloseRecordBean.id == {} {}-Procesado, count: {}", TAG, PROCESAR_DESGLOSES,
          dto.getId(), setProcesado, countUpdate);
    }
    else {
      LOG.warn("{}::{} No se han encontrado ejecuciones que utilizar numorden {} nureford {}. ", TAG,
          PROCESAR_DESGLOSES, dto.getNumeroOrden(), dto.getNumeroReferenciaOrden());
    }

    LOG.trace("{}::{}  FIN hay desgloses validados.", TAG, PROCESAR_DESGLOSES);
  }// public void procesarDesgloses(DesgloseRecordDTO dto,

  // Tmct0AloDataOperacionesEspecialesPartenon
  // aloDataOperacionesEspecialesPartenon, List<Tmct0ejecucionalocationData>
  // listaEjecucionAlocacionData) {

  public Integer updateProcesadoOkTitularEspecialNureford(List<String> nurefOrd) {

    return dao.updateProcesadoByNurefordAndProcesado(nurefOrd, 4);
  }

  public List<DesgloseRecordBean> findAllByNureford(List<String> nurefOrd) {
    return dao.findAllByNureford(nurefOrd);
  }

  public List<DesgloseRecordEjecucionesDTO> findAllDesglosesFor1015ByNuordenAndNucnfclt(String nuorden, String nucnfclt) {
    return dao.findAllDesglosesFor1015ByNuordenAndNucnfclt(nuorden, nucnfclt);
  }

  public Map<String, List<DesgloseRecordEjecucionesDTO>> findAllDesglosesFor1015ByNuordenAndNucnfclts(String nuorden,
      List<String> nucnfclts) {
    LOG.debug("[findAllDesglosesFor1015ByNuordenAndNucnfclts] Inicio ...");
    Map<String, List<DesgloseRecordEjecucionesDTO>> result = new HashMap<String, List<DesgloseRecordEjecucionesDTO>>();

    List<DesgloseRecordEjecucionesDTO> ejecuciones = dao.findAllDesglosesFor1015ByNuordenAndListNucnfclt(nuorden,
        nucnfclts);

    LOG.trace("[findAllDesglosesFor1015ByNuordenAndNucnfclts] Encontradas '{}' ejecuciones", ejecuciones.size());

    List<DesgloseRecordEjecucionesDTO> ejecucionesPorNucnfclt;
    for (DesgloseRecordEjecucionesDTO desgloseRecordEjecucionesDTO : ejecuciones) {
      LOG.trace("[findAllDesglosesFor1015ByNuordenAndNucnfclts] Procesando ", desgloseRecordEjecucionesDTO.toString());
      ejecucionesPorNucnfclt = result.get(desgloseRecordEjecucionesDTO.getNucnfclt().trim());
      if (ejecucionesPorNucnfclt == null) {
        ejecucionesPorNucnfclt = new ArrayList<DesgloseRecordEjecucionesDTO>();
      }
      ejecucionesPorNucnfclt.add(desgloseRecordEjecucionesDTO);
      result.put(desgloseRecordEjecucionesDTO.getNucnfclt().trim(), ejecucionesPorNucnfclt);
    } // for

    ejecuciones.clear();

    LOG.debug("[findAllDesglosesFor1015ByNuordenAndNucnfclts] Fin ...");
    return result;
  } // findAllDesglosesFor1015ByNuordenAndNucnfclts

  public Integer updateProcesadoByListId(List<Long> ids, int whereprocesado, int setprocesado) {
    // int result = 0;
    // int posIni = 0;
    // int posEnd = 0;
    // int incremento = 100;
    // while (posIni < ids.size()) {
    // posEnd += incremento;
    //
    // if (posEnd > ids.size()) {
    // posEnd = ids.size();
    // }
    // result += dao.updateProcesadoByListId(ids.subList(posIni, posEnd),
    // whereprocesado, setprocesado, new Date());
    // posIni = posEnd;
    // }
    // return result;
    return dao.updateProcesadoByListId(ids, whereprocesado, setprocesado, new Date());
  }

  public Integer updateProcesadoById(Long id, int setprocesado) {
    return dao.updateProcesadoById(id, setprocesado, new Date());
  }

  public List<String> findAllNurefordOperacionesEspeciales(int procesado, Pageable page) {
    return dao.findAllNurefordOperacionesEspeciales(procesado, page);
  }

  public List<String> findAllNurefordOperacionesEspecialesByProcesadoAndNumorden(String numorden, int procesado,
      Pageable page) {
    return dao.findAllNurefordOperacionesEspecialesByProcesadoAndNumorden(numorden, procesado, page);
  }

  public interface CriteriosComunicacionTitulares {

    public static final Character NUMERO_OPERACION_ECC = '1';
    public static final Character NUMERO_OPERACION_DCV = '2';
    public static final Character NUMERO_ORDEN_MERCADO = '5';
    public static final Character NUMERO_REFERENCIA_CLIENTE = '6';
    public static final Character EUROCCP_EXECUTION_ID = 'E';
  }
}// public class DesgloseRecordBeanBo extends AbstractBo< DesgloseRecordBean,
// Long, DesgloseRecordBeanDao > {
