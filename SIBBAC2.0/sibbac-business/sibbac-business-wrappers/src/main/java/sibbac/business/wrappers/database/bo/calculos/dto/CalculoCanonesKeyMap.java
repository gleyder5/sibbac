package sibbac.business.wrappers.database.bo.calculos.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CalculoCanonesKeyMap implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID  = 1823702770943471454L;
  private final String    cdmiembromkt;
  private final String    cdsegmento;
  private final String    tpopebol;
  private final BigDecimal  precio;
  private final Date      fecontratacion;

  /**
   * @param fecontratacion
   * @param cdmiembromkt
   * @param cdsegmento
   * @param tpopebol
   * @param precio
   */
  public CalculoCanonesKeyMap( Date fecontratacion, String cdmiembromkt, String cdsegmento, String tpopebol, BigDecimal precio ) {
    super();
    this.fecontratacion = fecontratacion;
    this.cdmiembromkt = cdmiembromkt;
    this.cdsegmento = cdsegmento;
    this.tpopebol = tpopebol;
    this.precio = precio;
  }

  
  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  
  /**
   * @return the cdmiembromkt
   */
  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  
  /**
   * @return the cdsegmento
   */
  public String getCdsegmento() {
    return cdsegmento;
  }

  
  /**
   * @return the tpopebol
   */
  public String getTpopebol() {
    return tpopebol;
  }

  
  /**
   * @return the precio
   */
  public BigDecimal getPrecio() {
    return precio;
  }

  
  /**
   * @return the fecontratacion
   */
  public Date getFecontratacion() {
    return fecontratacion;
  }


  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ( ( cdmiembromkt == null ) ? 0 : cdmiembromkt.hashCode() );
    result = prime * result + ( ( cdsegmento == null ) ? 0 : cdsegmento.hashCode() );
    result = prime * result + ( ( fecontratacion == null ) ? 0 : fecontratacion.hashCode() );
    result = prime * result + ( ( precio == null ) ? 0 : precio.hashCode() );
    result = prime * result + ( ( tpopebol == null ) ? 0 : tpopebol.hashCode() );
    return result;
  }


  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals( Object obj ) {
    if ( this == obj )
      return true;
    if ( obj == null )
      return false;
    if ( getClass() != obj.getClass() )
      return false;
    CalculoCanonesKeyMap other = ( CalculoCanonesKeyMap ) obj;
    if ( cdmiembromkt == null ) {
      if ( other.cdmiembromkt != null )
        return false;
    } else if ( !cdmiembromkt.equals( other.cdmiembromkt ) )
      return false;
    if ( cdsegmento == null ) {
      if ( other.cdsegmento != null )
        return false;
    } else if ( !cdsegmento.equals( other.cdsegmento ) )
      return false;
    if ( fecontratacion == null ) {
      if ( other.fecontratacion != null )
        return false;
    } else if ( !fecontratacion.equals( other.fecontratacion ) )
      return false;
    if ( precio == null ) {
      if ( other.precio != null )
        return false;
    } else if ( !precio.equals( other.precio ) )
      return false;
    if ( tpopebol == null ) {
      if ( other.tpopebol != null )
        return false;
    } else if ( !tpopebol.equals( other.tpopebol ) )
      return false;
    return true;
  }


  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "CalculoCanonesKeyMap [cdmiembromkt=" + cdmiembromkt + ", cdsegmento=" + cdsegmento + ", tpopebol=" + tpopebol + ", precio="
        + precio + ", fecontratacion=" + fecontratacion + "]";
  }

}

