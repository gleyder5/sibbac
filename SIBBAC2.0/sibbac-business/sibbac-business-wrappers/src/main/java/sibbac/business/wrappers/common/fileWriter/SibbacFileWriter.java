package sibbac.business.wrappers.common.fileWriter;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;

import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.common.SIBBACBusinessException;


public abstract class SibbacFileWriter< T extends RecordBean > {
	protected DelimitedLineAggregator< T > delLineAgg;
	protected String				folderName;
	protected static final Logger	LOG	= LoggerFactory.getLogger( SibbacFileWriter.class );

	public SibbacFileWriter(  ) {
		super();
	}
	
	public SibbacFileWriter( final String folderName ) {
		super();
		this.folderName = folderName;
	}
	
	public abstract boolean writeIntoFile() throws SIBBACBusinessException;

	
	

}
