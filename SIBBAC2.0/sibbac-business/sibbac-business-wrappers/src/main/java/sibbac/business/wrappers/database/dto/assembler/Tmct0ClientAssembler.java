package sibbac.business.wrappers.database.dto.assembler;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.dao.Tmct0cliDao;
import sibbac.business.wrappers.database.model.Tmct0cli;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0CLI
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0ClientAssembler {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ClientAssembler.class);

  @Autowired
  Tmct0cliDao tmct0cliDao;

  @Autowired
  Tmct0ContactoAssembler tmct0ContactoAssembler;

  @Autowired
  Tmct0MifidAssembler tmct0MifidAssembler;

  @Autowired
  Tmct0AddressAssembler tmct0AddressAssembler;

  /**
   * Realiza la conversion entre los datos del front y la entity a gestionar en back
   * 
   * @param paramsObjects -> datos del front
   * @return Entity - Entity a gestionar en la parte back
   */
  public Tmct0cli getTmct0ClientEntity(Map<String, Object> paramsObjects) throws ParseException {
    LOG.debug("Inicio de la transformación getTmct0ClientEntity");

    Tmct0cli miEntity = new Tmct0cli();
    boolean isAlta = true;

    // TODO - Falta corroborar estructura
    if (paramsObjects.get("datosCliente") != null) {

      // Se comprueba si se tiene el idClient que determina si es una entidad nueva o ya existente
      Map<?, ?> datosCliente = (Map<?, ?>) paramsObjects.get("datosCliente");
      if (datosCliente.get("idClient") != null) {
        Long lidClient = Long.valueOf(datosCliente.get("idClient").toString());
        if (lidClient != 0) {
          miEntity = this.tmct0cliDao.findOne(lidClient);
          if (miEntity != null) {
            isAlta = false;
          }
        }
      }

      // Datos a popular solo en alta de cliente.
      if (isAlta) {
        this.popularEnAlta(miEntity);
      }

      miEntity.setFhaudit(new Date());

      // TODO - Cliente - CDBROCLI - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("cdbrocli") != null) {
        miEntity.setCdbrocli((String) datosCliente.get("cdbrocli"));
      }

      // numsec - NUMSEC
      miEntity.setNumsec(BigDecimal.ONE);

      // Descripcion - DESCLI
      if (datosCliente.get("descli") != null) {
        miEntity.setDescli((String) datosCliente.get("descli"));
      }

      // TODO - Tipo de documento - TPIDENTI - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("tpidenti") != null) {
        String tpidenti = (String) datosCliente.get("tpidenti");
        if (StringUtils.isNotEmpty(tpidenti)) {
          miEntity.setTpidenti(StringHelper.getCharFromString(tpidenti));
        }
      }

      // Codigo de Persona BDP - CDBDP - Hay 2 campos que almacenan este valor.
      if (datosCliente.get("cdbdp") != null) {
        miEntity.setCdbdp((String) datosCliente.get("cdbdp"));
        miEntity.setCdrefbnk((String) datosCliente.get("cdbdp"));
        if (miEntity.getCdrefbnk() == null) {
          miEntity.setCdrefbnk("");
        }
      } else {
        miEntity.setCdbdp("000000000"); // MFG 28/11/2016 Incidencia recibido por correo. Si no ha insertado datos del
                                        // cdbdp se asigna este valor
        if (miEntity.getCdrefbnk() == null) {
          miEntity.setCdrefbnk("");
        }
      }

      // TODO - Numero de documento - CIF - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("cif") != null) {
        miEntity.setCif((String) datosCliente.get("cif"));
      }

      // TODO - Tipo de residencia - TPRESIDE - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("tpreside") != null) {
        miEntity.setTpreside((String) datosCliente.get("tpreside"));
      }

      // TODO - Tipo de cliente - TPCLIENN - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("tpclienn") != null) {
        String tpclienn = (String) datosCliente.get("tpclienn");
        if (StringUtils.isNotEmpty(tpclienn)) {
          miEntity.setTpclienn(StringHelper.getCharFromString(tpclienn));
        }
      }

      // TODO - Naturaleza de cliente - TPFISJUR - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("tpfisjur") != null) {
        String tpfisjur = (String) datosCliente.get("tpfisjur");
        if (StringUtils.isNotEmpty(tpfisjur)) {
          miEntity.setTpfisjur(StringHelper.getCharFromString(tpfisjur));
        }
      }

      // Estado del cliente - CDESTADO
      if (datosCliente.get("cdestado") != null) {
        String cdestado = (String) datosCliente.get("cdestado");
        if (StringUtils.isNotEmpty(cdestado)) {
          miEntity.setCdestado(StringHelper.getCharFromString(cdestado));
        }
      }

      // Fecha de alta - FHINICIO
      if (datosCliente.get("fhinicio") != null) {
        miEntity.setFhinicio((Integer) datosCliente.get("fhinicio"));
      }

      // MFG 28/11/2016 Incidencia recibido por correo. En un alta la fecha de fin tiene que ser 99991231
      if (isAlta) {
        miEntity.setFhfinal(99991231);
      }

      // Codigo KGR - CDKGR
      miEntity.setCdkgr(StringHelper.populateWithSpacesIfBlank((String) datosCliente.get("cdkgr")));

      // TODO - Tipo de nacionalidad - TPNACTIT - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("tpNacTit") != null) {
        String tpNacTit = (String) datosCliente.get("tpNacTit");
        if (StringUtils.isNotEmpty(tpNacTit)) {
          miEntity.setTpNacTit(StringHelper.getCharFromString(tpNacTit));
        }
      }

      // TODO - Codigo del pais de nacionalidad - CDNACTIT - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("cdNacTit") != null) {
        miEntity.setCdNacTit((String) datosCliente.get("cdNacTit"));
        miEntity.setCdNacTit(miEntity.getCdNacTit().trim());
      }

      // TODO - Categorizacion - CDCAT - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("cdcat") != null) {
        miEntity.setCdcat((String) datosCliente.get("cdcat"));
      }

      // TODO - TPMANCAT - tpManCat - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("tpManCat") != null) {
        miEntity.setTpManCat((String) datosCliente.get("tpManCat"));
      }

      // TODO - CTFISCAL - ctFiscal - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("ctFiscal") != null) {
        miEntity.setCtFiscal((String) datosCliente.get("ctFiscal"));
      }

      // TODO - FHNACIMI - fhNacimi - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("fhNacimi") != null && String.valueOf(datosCliente.get("fhNacimi")).trim() != "") {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        miEntity.setFhNacimi(formatter.parse(String.valueOf(datosCliente.get("fhNacimi"))));
      } else {
        miEntity.setFhNacimi(null);
      }

      // TODO - Control PBC - CDDEPREV - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("cddeprev") != null) {
        String cddeprev = (String) datosCliente.get("cddeprev");
        if (StringUtils.isNotEmpty(cddeprev)) {
          miEntity.setCddeprev(StringHelper.getCharFromString(cddeprev));
        }
      }
      
      if (datosCliente.get("riesgoPbc") != null) {
        String riesgoPbc = (String) datosCliente.get("riesgoPbc");
        if (StringUtils.isNotEmpty(riesgoPbc)) {
          miEntity.setRiesgoPbc(riesgoPbc);
        }
      }    
      
      // fhRevRiesgoPbc
      if (datosCliente.get("fhRevRiesgoPbc") != null && String.valueOf(datosCliente.get("fhRevRiesgoPbc")).trim() != "") {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        miEntity.setFhRevRiesgoPbc(formatter.parse(String.valueOf(datosCliente.get("fhRevRiesgoPbc"))));
      } else {
        miEntity.setFhRevRiesgoPbc(null);
      }   

      // TODO - Motivo PBC - MOTIPREV - Obligatorio - (Corroborar validación back ?)
      if (datosCliente.get("motiPrev") != null) {
        miEntity.setMotiPrev((String) datosCliente.get("motiPrev"));
      }
      
      if (datosCliente.get("riesgoPbc") != null) {
        miEntity.setRiesgoPbc((String) datosCliente.get("riesgoPbc"));
      }

      // Lista de contactos.
      miEntity.setContactos(this.tmct0ContactoAssembler.getTmct0ContactoEntityList(datosCliente));

      // Lista mifid.
      miEntity.setMifid(this.tmct0MifidAssembler.getTmct0MifidEntityList(datosCliente));

      // Address.
      miEntity.setAddress(this.tmct0AddressAssembler.getTmct0AddressEntity(datosCliente));

    }
    LOG.debug("Fin de la transformación getTmct0ClientEntity");
    return miEntity;
  }

  /**
   * Datos a popular en alta.
   *
   * @param miEntity miEntity
   */
  private void popularEnAlta(Tmct0cli miEntity) {

    miEntity.setCdusuaud("SIBBAC");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    String fecha = sdf.format(new Date());

    miEntity.setFhinicio(new Integer(fecha));
    miEntity.setFidessa(' ');
    miEntity.setFhsendfi(0);
    miEntity.setCdotheid("");
    miEntity.setCdestado('A');

  }

}
