package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.CodigoTptiprepDao;
import sibbac.business.wrappers.database.model.CodigoTptiprep;
import sibbac.database.bo.AbstractBo;

@Service
public class CodigoTptiprepBo extends
		AbstractBo<CodigoTptiprep, String, CodigoTptiprepDao> {

	public CodigoTptiprep findByCodigo(String codigo) {
		CodigoTptiprep tptiprep = null;
		try {
			tptiprep = dao.findByCodigo(codigo);
		} catch (Exception e) {
			tptiprep = null;
		}

		return tptiprep;
	}
}
