package sibbac.business.wrappers.tasks;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.partenonReader.PartenonFileReader;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Tarea que procesa los ficheros de titulares de Partenon y de Openbank.
 * 
 * @author XI316153
 * @see WrapperTaskConcurrencyPrevent
 */
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_ROUTING.NAME,
           name = Task.GROUP_ROUTING.JOB_TITULARES,
           interval = 1,
           delay = 0,
           intervalUnit = IntervalUnit.HOUR)
public class TaskRoutingTitulares extends WrapperTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  private final static String TRATADOS_FOLDER = "tratados";

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskRoutingTitulares.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${sibbac.wrappers.routingTitularesOpenFile}")
  private String ficheroTitularesOpenBank;

  @Value("${sibbac.wrappers.routingTitularesFile}")
  private String ficheroTitularesRoutingPartenon;

  /** Reader para los ficheros de titulares de Partenón. */
  @Qualifier(value = "partenonFileReader")
  @Autowired
  private PartenonFileReader reader;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent#executeTask()
   */
  @Override
  public void executeTask() throws Exception {
    LOG.debug("[executeTask] Iniciando la tarea de procesado del fichero de titularidades ...");
    Long startTime = System.currentTimeMillis();
    StringBuffer errorMessage = new StringBuffer();
    if (Files.exists(Paths.get(ficheroTitularesRoutingPartenon))) {
      try {
        LOG.debug("[executeTask] Iniciando el procesado del fichero " + ficheroTitularesRoutingPartenon);
        reader.executeTask(ficheroTitularesRoutingPartenon, true, StandardCharsets.ISO_8859_1);
        pasarTratado(reader.getFile());
      } catch (Exception ex) {
        errorMessage.append("[executeTask] NOMBRES.DAT: " + ex.getMessage());
        errorMessage.append(System.getProperty("line.separator"));
        errorMessage.append(System.getProperty("line.separator"));
      } finally {

        final Long endTime = System.currentTimeMillis();
        LOG.info("[executeTask] Finalizado el procesado del fichero "
                 + ficheroTitularesRoutingPartenon
                 + " ... ["
                 + String.format("%d min, %d sec",
                                 TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                                 TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                     - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
                 + "]");

      } // finally
    }
    if (Files.exists(Paths.get(ficheroTitularesOpenBank))) {
      try {
        startTime = System.currentTimeMillis();
        LOG.debug("[executeTask] Iniciando el procesado del fichero " + ficheroTitularesOpenBank);
        reader.executeTask(ficheroTitularesOpenBank, true, StandardCharsets.ISO_8859_1);
        pasarTratado(reader.getFile());
        // openFileReader.readFromFile(true, StandardCharsets.ISO_8859_1.name(), transactionSize);
      } catch (Exception ex) {
        errorMessage.append("[executeTask] NOMOPEN.DAT: " + ex.getMessage());
      } finally {

        final Long endTime = System.currentTimeMillis();
        LOG.info("[executeTask] Finalizado el procesado del fichero "
                 + ficheroTitularesOpenBank
                 + " ... ["
                 + String.format("%d min, %d sec",
                                 TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                                 TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                     - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
                 + "]");
      } // finally
    }
    if (errorMessage.length() > 0) {
      throw new Exception(errorMessage.toString());
    } // if

    LOG.debug("[executeTask] Iniciando la tarea de procesado del fichero de titularidades ...");
  } // executeTask

  private void pasarTratado(Path file) throws IOException {
    LOG.debug("[TaskRoutingTitulares :: pasarTratado] inicio");
    if (Files.exists(file)) {
      String filename = file.toString().substring(0, file.toString().indexOf(".")) + ".DAT_"
                        + FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.FILE_DATETIME_FORMAT);

      LOG.info("[TaskRoutingTitulares :: pasarTratado] El fichero : {} sera renombrado a : {}", file.toString(),
               filename);
      Path nombreFinal = Paths.get(filename);

      nombreFinal = Paths.get(file.getParent().toString() + "/" + TRATADOS_FOLDER, nombreFinal.toFile().getName());

      Files.move(file, nombreFinal, StandardCopyOption.ATOMIC_MOVE);
    } else {
      LOG.warn("[TaskRoutingTitulares :: pasarTratado] Incidencia fichero no renombrado : {}", file.toString());
    }
    LOG.debug("[TaskRoutingTitulares :: pasarTratado] fin");
  }

  /*
   * @see sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_ROUTING_TITULARES;
  } // determinarTipoApunte

} // TaskRoutingTitulares
