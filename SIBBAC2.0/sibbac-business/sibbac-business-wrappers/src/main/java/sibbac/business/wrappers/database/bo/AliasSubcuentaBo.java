package sibbac.business.wrappers.database.bo;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0actId;
import sibbac.business.wrappers.database.dao.AliasSubcuentaDao;
import sibbac.business.wrappers.database.data.AliasFacturableData;
import sibbac.business.wrappers.database.dto.AliasSubcuentaDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasSubcuenta;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;


@Service
public class AliasSubcuentaBo extends AbstractBo< AliasSubcuenta, Long, AliasSubcuentaDao > {

	@Autowired
	private Tmct0actBo	tmct0actBo;
	@Autowired
	private AliasBo		aliasBo;
	
	private static final String TAG = AliasSubcuentaBo.class.getName();
	/**
	 * @param
	 * @return
	 */
	private Predicate< AliasSubcuenta > encontrarAliasSubcuenta( final Tmct0act tmct0act ) {
		final Predicate< AliasSubcuenta > predicate = new Predicate< AliasSubcuenta >() {

			@Override
			public boolean evaluate( final AliasSubcuenta aliasSubcuenta ) {
				boolean encontrado = false;
				if ( aliasSubcuenta.getTmct0act().equals( tmct0act ) ) {
					encontrado = true;
				}
				return encontrado;
			}
		};
		return predicate;
	}

	/**
	 * @param aliasSubcuentaList
	 * @param tmct0act
	 * @return
	 */
	private AliasSubcuenta encontrarAliasSucuenta( final List< AliasSubcuenta > aliasSubcuentaList, Tmct0act tmct0act ) {
		final Predicate< AliasSubcuenta > predicate = encontrarAliasSubcuenta( tmct0act );
		AliasSubcuenta alias = ( AliasSubcuenta ) CollectionUtils.find( aliasSubcuentaList, predicate );
		return alias;
	}

	/**
	 * 
	 * Crea los Alias que no exisntan en la tabla de wrapers.
	 * 
	 * @return
	 */
	@Transactional( propagation = Propagation.REQUIRES_NEW )
	public List< AliasSubcuenta > sincronizarAliasSubcuenta() {
		final String prefix = "[AliasSubcuentaBo::actualizarAliasSubcuenta] ";
		LOG.trace( prefix + "[Actualizando subcuentas...]" );
		final List< Tmct0act > tmct0acts = tmct0actBo.findAliasSubuentasActivos();
		final List< Tmct0actId > tmct0actIds = tmct0actBo.extractIds( tmct0acts );
		final List< AliasSubcuenta > alisSubucentas = findAll();
		eliminarRepetidos( tmct0acts, tmct0actIds, alisSubucentas );
		final List< AliasSubcuenta > alisSubcuentaTratados = new ArrayList< AliasSubcuenta >();
		
		if ( CollectionUtils.isNotEmpty( tmct0acts ) ) {
		    int aliasSubcuentaNuevos = 0;
		    for ( final Tmct0act tmct0act : tmct0acts ) {
				String cdsubcta = tmct0act.getCdsubcta();
				String cdaliass = tmct0act.getCdaliass();
				AliasSubcuenta aliasSubcuenta = encontrarAliasSucuenta( alisSubucentas, tmct0act );
				if ( aliasSubcuenta == null ) {
					LOG.trace( prefix + "[Subcuenta [{}] nueva]", cdsubcta );
					final Alias aliasFacturacionDefecto = aliasBo.findByCdaliass( cdaliass );
					if ( aliasFacturacionDefecto != null ) {
						aliasSubcuenta = new AliasSubcuenta( aliasFacturacionDefecto, tmct0act );
						aliasSubcuenta = this.save( aliasSubcuenta );
						alisSubcuentaTratados.add( aliasSubcuenta );
						aliasSubcuentaNuevos++;
						LOG.trace( "Crada la subcuenta [{}] con alias de facturación [{}].", cdsubcta, cdaliass );
					} else {
						LOG.warn(
								"Posible inconsistencia de datos. No existe el alias [{}] para la subcuenta [{}]. No se registrará la subcuenta ",
								cdaliass, cdsubcta );
					}
				} else {
					aliasSubcuenta.setTmct0act( tmct0act );
					aliasSubcuenta = this.save( aliasSubcuenta );
					alisSubcuentaTratados.add( aliasSubcuenta );
					LOG.trace( prefix + "[Subcuenta [{}] se actualiza]", cdsubcta );
				}
			}
			LOG.debug("{}::{} Se han generado {} objetos de la tabla TMCT0_ALIASSUBCUENTA nuevos.", TAG, "sincronizarAliasSubcuenta", aliasSubcuentaNuevos);
		}

		return alisSubcuentaTratados;
	}

	/**
	 * @param tmct0acts
	 * @param tmct0actIds
	 * @param alisSubucentas
	 */
	private void eliminarRepetidos( final List< Tmct0act > tmct0acts, final List< Tmct0actId > tmct0actIds,
			final List< AliasSubcuenta > alisSubucentas ) {
		if ( CollectionUtils.isNotEmpty( alisSubucentas ) ) {
			for ( final Iterator< AliasSubcuenta > alisSubcuentaIterator = alisSubucentas.iterator(); alisSubcuentaIterator.hasNext(); ) {
				final AliasSubcuenta aliSubucenta = alisSubcuentaIterator.next();
				final Tmct0act tmct0act = aliSubucenta.getTmct0act();
				final Tmct0actId tmct0actId = tmct0act.getId();
				if ( tmct0actIds.contains( tmct0actId ) ) {
					alisSubcuentaIterator.remove();
					tmct0actIds.remove( tmct0actId );
					tmct0acts.remove( tmct0act );
				}
			}
		}

	}

	/**
	 * Para cada alias de facturación que tienen las subcuentas, recupera una lista de subcuentas asociadas. Devuelve un map con una entrada
	 * para cada cdaliass y su lista de subcuentas asociadas
	 * 
	 * @param cdaliasAProcesar
	 * @return
	 */
	@Transactional
	public List< AliasFacturableData > recuperarAliasFactruablesSubcuentas( final Set< String > cdaliasAProcesar ) {
		final List< AliasFacturableData > aliasFacturacion = dao.findDistinctAliasFacturacion( cdaliasAProcesar );
		return aliasFacturacion;
	}

	@Transactional
	public List< AliasSubcuentaDTO > getListaSubcuentas( final Long idAlias ) {
		return dao.findAllAliasSubcuentas( idAlias );
	}

	@Transactional
	public AliasSubcuenta updateAliasSubcuenta( final Long idAliasSubcuenta, final Long idAliasFacturacion ) throws SIBBACBusinessException {
		AliasSubcuenta aliasSubcuenta = dao.findOne( idAliasSubcuenta );
		if ( aliasSubcuenta == null ) {
			throw new SIBBACBusinessException( "No se ha recuperado informacion para el aliasSubcuenta [" + idAliasSubcuenta + "]" );
		}
		final Alias aliasFacturacion = aliasBo.findById( idAliasFacturacion );
		if ( aliasFacturacion == null ) {
			throw new SIBBACBusinessException( "No se ha recuperado informacion para el alias de facturacion [" + idAliasFacturacion + "]" );
		}
		aliasSubcuenta.setAliasFacturacion( aliasFacturacion );
		aliasSubcuenta = save( aliasSubcuenta );
		return aliasSubcuenta;
	}
}
