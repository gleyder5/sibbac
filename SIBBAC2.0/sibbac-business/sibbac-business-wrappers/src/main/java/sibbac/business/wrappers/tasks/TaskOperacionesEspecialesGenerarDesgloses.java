package sibbac.business.wrappers.tasks;

import java.util.List;

import org.quartz.DateBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.ProcesarDesglosesDbReader;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_WRAPPERS.NAME,
           name = Task.GROUP_WRAPPERS.JOB_OPERACIONES_ESPECIALES_2040_GENERAR_DESGLOSES,
           interval = 1,
           delay = 1,
           intervalUnit = DateBuilder.IntervalUnit.MINUTE)
@DisallowConcurrentExecution
public class TaskOperacionesEspecialesGenerarDesgloses extends WrapperTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskOperacionesEspecialesGenerarDesgloses.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Qualifier(value = "procesarDesglosesDbReader")
  @Autowired
  private ProcesarDesglosesDbReader reader;

  @Autowired
  private Tmct0aloBo aloBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.MINIJOBS_TASK_PROCESAR_DESGLOSE;
  } // determinarTipoApunte

  /*
   * @see sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent#executeTask()
   */
  @Override
  public void executeTask() throws Exception {
    LOG.info("[TaskOperacionesEspecialesGenerarDesgloses :: executeTask] Iniciando la tarea de procesar los desgloses que han sido validados por el usuario ...");

    try {
      LOG.info("[TaskOperacionesEspecialesGenerarDesgloses :: executeTask] Buscando DesglosesRecordBean en estado 2-Pendiente Procesar");
      /* Recupera las ordenes especiales partnenon que estan validadas por el usuario. */
      reader.executeTask();

      /* CREAR FICHERO MQ */
      // SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
      // String fecha = sdf.format(new Date());

      // envioFichero1015.crearFicheroMQ();

      LOG.debug("[TaskOperacionesEspecialesGenerarDesgloses :: executeTask] Fin de la tarea de procesar los desgloses que han sido validados por el usuario ...");
    } catch (Exception ex) {
      LOG.info(ex.getMessage(), ex);
      throw ex;
    } // catch
  } // executeTask

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * @param ordenes
   */
  @Transactional
  private void updateAlosPdteOperativaEspecialAPdteDesglosar(List<String> ordenes) {
    LOG.info("[TaskOperacionesEspecialesGenerarDesgloses :: updateAlosPdteOperativaEspecialAPdteDesglosar] inicio.");
    LOG.info("[TaskOperacionesEspecialesGenerarDesgloses :: updateAlosPdteOperativaEspecialAPdteDesglosar] Ordenes a revisar estados{}.", ordenes);
    Integer result = aloBo.updateCdestadoasigByNumordenAndCdestadoasig(ordenes,
                                                                       EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE.getId(),
                                                                       EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
    LOG.info("[TaskOperacionesEspecialesGenerarDesgloses :: updateAlosPdteOperativaEspecialAPdteDesglosar] Alos actualizados {}.", result);
    LOG.info("[TaskOperacionesEspecialesGenerarDesgloses :: updateAlosPdteOperativaEspecialAPdteDesglosar] fin.");
  } // updateAlosPdteOperativaEspecialAPdteDesglosar

} // TaskOperacionesEspecialesGenerarDesgloses
