package sibbac.business.wrappers.database.filter;

public interface FinalHoldersFilter {
  
  String getName();
  
  boolean isThereName();

}
