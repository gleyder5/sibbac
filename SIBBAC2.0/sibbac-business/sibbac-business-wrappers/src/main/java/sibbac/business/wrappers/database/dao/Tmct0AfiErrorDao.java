package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0AfiError;

@Repository
public interface Tmct0AfiErrorDao extends JpaRepository<Tmct0AfiError, Long> {

  // public List< Tmct0AfiError > findAll( Date fechaDesde, Date fechaHasta, Character mercadoNacInt, Character sentido,
  // String nudnicif,
  // String codError, String nbclient, String nbclient1, String nbclient2, String nbvalors, Character tpnactit, String
  // isin,
  // String alias, Character tipodocumento );

  public Tmct0AfiError findByIdmensaje(String idmensaje);

  public List<Tmct0AfiError> findByIdIn(List<Long> idList);

  public Integer deleteByNureford(String nureford);

  public List<Tmct0AfiError> findByNureford(String nureford);

}
