package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0tipoMovimientoConciliacionDao;
import sibbac.business.wrappers.database.model.Tmct0TipoMovimientoConciliacion;
import sibbac.database.bo.AbstractBo;


/**
 * The Class Tmct0tipoMovimientoConciliacionBo.
 */
@Service
public class Tmct0tipoMovimientoConciliacionBo extends
		AbstractBo< Tmct0TipoMovimientoConciliacion, Long, Tmct0tipoMovimientoConciliacionDao > {

	public Tmct0TipoMovimientoConciliacion findByTipoOperacion( String tipoOperacion ) {
		return this.dao.findByTipoOperacion( tipoOperacion );
	}
}
