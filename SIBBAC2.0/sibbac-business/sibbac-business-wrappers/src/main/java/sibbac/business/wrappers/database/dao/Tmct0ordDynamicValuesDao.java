package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ordDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0ordDynamicValuesId;

@Repository
public interface Tmct0ordDynamicValuesDao extends JpaRepository<Tmct0ordDynamicValues, Tmct0ordDynamicValuesId> {

}
