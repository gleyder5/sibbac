package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.SistemaLiquidacionDao;
import sibbac.business.wrappers.database.model.SistemaLiquidacion;
import sibbac.database.bo.AbstractBo;

@Service
public class SistemaLiquidacionBo extends AbstractBo<SistemaLiquidacion, Long, SistemaLiquidacionDao>{

}
