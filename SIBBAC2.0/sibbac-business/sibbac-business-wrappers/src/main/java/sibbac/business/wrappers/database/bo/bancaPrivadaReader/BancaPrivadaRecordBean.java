package sibbac.business.wrappers.database.bo.bancaPrivadaReader;


import java.math.BigDecimal;
import java.util.List;

import sibbac.business.wrappers.database.model.RecordBean;


public class BancaPrivadaRecordBean extends RecordBean {

  private static final long serialVersionUID = 8471411487123805207L;

  BigDecimal				codOrigen;
	BigDecimal				secuencia;
	HBB6RecordBean			registroInicio;
	List< HBB4TitularRecordBean >	registrosTitulares;
	List< HBB5RecordBean >	registrosDomicilios;
	HBB6RecordBean			registroFin;

	public HBB6RecordBean getInicio() {
		return registroInicio;
	}

	public void setInicio( HBB6RecordBean inicio ) {
		this.registroInicio = inicio;
	}

	public List< HBB4TitularRecordBean > getRegistrosTitulares() {
		return registrosTitulares;
	}

	public void setRegistrosTitulares( List< HBB4TitularRecordBean > registrosTitulares ) {
		this.registrosTitulares = registrosTitulares;
	}

	public void addRegistroTitular( HBB4TitularRecordBean registroTitular ) {
		this.registrosTitulares.add( registroTitular );
	}

	public List< HBB5RecordBean > getRegistrosDomicilios() {
		return registrosDomicilios;
	}

	public void setRegistrosDomicilios( List< HBB5RecordBean > registrosDomicilios ) {
		this.registrosDomicilios = registrosDomicilios;
	}

	public void addRegistroDomicilio( HBB5RecordBean registroDomicilio ) {
		this.registrosDomicilios.add( registroDomicilio );
	}

	public HBB6RecordBean getFin() {
		return registroFin;
	}

	public void setFin( HBB6RecordBean fin ) {
		this.registroFin = fin;
	}

	public BigDecimal getCodOrigen() {
		return codOrigen;
	}

	public void setCodOrigen( BigDecimal codOrigen ) {
		this.codOrigen = codOrigen;
	}

	public BigDecimal getSecuencia() {
		return secuencia;
	}

	public void setSecuencia( BigDecimal secuencia ) {
		this.secuencia = secuencia;
	}

	public void addRegistroTitulares( HBB4TitularRecordBean hbb4 ) {
		registrosTitulares.add( hbb4 );
	}

	public void addRegistroDomicilios( HBB4TitularRecordBean hbb5 ) {
		registrosTitulares.add( hbb5 );
	}
}
