package sibbac.business.wrappers.database.dto;

/**
 * Clase que guarda la información de un ord necesaria en lectura de titulares
 */
public class OrdTitularesDTO {

  private String nuorden = "";
  private Character cdclsmdo = ' ';

  private Character issrcdiv = ' ';

  public OrdTitularesDTO(String nuorden, Character cdclsmdo) {
    super();
    this.nuorden = nuorden;
    this.cdclsmdo = cdclsmdo;
  }

  public OrdTitularesDTO(String nuorden, Character cdclsmdo, Character issrcdiv) {
    super();
    this.nuorden = nuorden;
    this.cdclsmdo = cdclsmdo;
    this.issrcdiv = issrcdiv;
  }

  public Character getIssrcdiv() {
    return issrcdiv;
  }

  public void setIssrcdiv(Character issrcdiv) {
    this.issrcdiv = issrcdiv;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public Character getCdclsmdo() {
    return cdclsmdo;
  }

  public void setCdclsmdo(Character cdclsmdo) {
    this.cdclsmdo = cdclsmdo;
  }

}
