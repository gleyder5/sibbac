package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;
import java.util.Date;

public class HBB4TipoRegistroRecordBean extends HBBNRecordBean {

  /**
	 * 
	 */
  private static final long serialVersionUID = 3223274589183334345L;

  private String refExterna;
  private Date fechaCuadre;
  private BigDecimal tipoRegistroHBB;

  public String getRefExterna() {
    return refExterna;
  }

  public Date getFechaCuadre() {
    return fechaCuadre;
  }

  public BigDecimal getTipoRegistroHBB() {
    return tipoRegistroHBB;
  }

  public void setRefExterna(String refExterna) {
    this.refExterna = refExterna;
  }

  public void setFechaCuadre(Date fechaCuadre) {
    this.fechaCuadre = fechaCuadre;
  }

  public void setTipoRegistroHBB(BigDecimal tipoRegistro) {
    this.tipoRegistroHBB = tipoRegistro;
  }

}
