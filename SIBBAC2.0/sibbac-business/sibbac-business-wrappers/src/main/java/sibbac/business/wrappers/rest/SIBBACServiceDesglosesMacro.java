package sibbac.business.wrappers.rest;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;




import sibbac.business.fase0.database.dto.Tmct0cliDTO;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
@Transactional
public class SIBBACServiceDesglosesMacro implements SIBBACServiceBean {
	
	@Autowired
	private Tmct0cliBo tmct0cliBo = null;

	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceDesglosesMacro.class);
	
	private static final String NOMBRE_SERVICIO = "SIBBACServiceDesglosesMacro";

	private static final String CMD_INFORME_LIST  = "ListaDesglosesMacro"; 
	private static final String CMD_CLIENTES_LIST = "ListaClientes"; 
	
	private static final String FILTRO_FDESDE  =     "fechaDesde";
	private static final String FILTRO_FHASTA  =     "fechaHasta";
	private static final String FILTRO_ALIAS   =     "alias";
	private static final String FILTRO_CLIENTE =     "cliente";
	
	private static final String RETURN_CLIENTE_LIST	 =  "resultado_lista_clientes";
	private static final String RETURN_DESGLOSE_LIST =  "resultado_lista_desgloses";
	
	private static DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");
	
	public SIBBACServiceDesglosesMacro() {

	}

	@Override
	public WebResponse process(WebRequest webRequest) 	throws SIBBACBusinessException{
		final String prefix = "[SIBBACServiceGestionReglas::process] ";
		LOG.debug(prefix + "Processing a web request...");

		final String command = webRequest.getAction();
		LOG.debug(prefix + "+ Command.: [{}]", command);

		final WebResponse webResponse = new WebResponse();

		switch (command) {
		case CMD_INFORME_LIST:
			webResponse.setResultados(this.getListaDesglosesMacro(webRequest, webResponse));
			break;	
		case CMD_CLIENTES_LIST:
			webResponse.setResultados(this.getListaClientes(webRequest, webResponse));
			break;	
		default:
			webResponse.setError(command + " is not a valid action. Valid actions are: " + this.getAvailableCommands());
			break;

		}
		return webResponse;

	}
	
	
	private Map<String, Object> getListaDesglosesMacro(WebRequest webRequest, WebResponse webResponse) {
		  
	    Map<String, Object> map = new HashMap<String, Object>();
		try{

		    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			
		    LOG.debug("[" + NOMBRE_SERVICIO + " :: getReglasPorFiltros] Init");
			Map<String, String> filters = new HashMap<String, String>();
			filters = webRequest.getFilters();
			Date fDesde = null, fHasta = null;
			List<String> aliasVector = new ArrayList<String>();
			List<String> clientesVector = new ArrayList<String>();
		
			if (filters.get(FILTRO_FDESDE) != null) {
				fDesde = convertirStringADate( filters.get(FILTRO_FDESDE));
			}
	
			if (filters.get(FILTRO_FHASTA) != null) {
				fHasta = convertirStringADate( filters.get(FILTRO_FHASTA));
			}
	
			if (filters.get(FILTRO_ALIAS) != null) {
				String[] parts = filters.get(FILTRO_ALIAS).split(";");
				for (String part : parts) {
				    aliasVector.add(part);
				}
			}
			
			if (filters.get(FILTRO_CLIENTE) != null) {
				String[] parts = filters.get(FILTRO_CLIENTE).split(";");
				for (String part : parts) {
				    clientesVector.add(part);
				}
			}
			
			
		    List<Tmct0cliDTO> listaDesglosesMacro = tmct0cliBo.findDesglosesMacro(fDesde, fHasta, aliasVector, clientesVector);
	
		    map.put(RETURN_DESGLOSE_LIST, listaDesglosesMacro);
		    return map;
		    
		} catch (Exception ex){
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());	
		}
		
	    map.put(RETURN_DESGLOSE_LIST, "Error en la generación de la lista");
	    return map;
	    
	}

	private Map<String, Object> getListaClientes(WebRequest webRequest, WebResponse webResponse) {
		  
	    Map<String, Object> resultados = new HashMap<String, Object>();
	    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

	    Map<String, Object> bean = null;

	    List<Tmct0cli> clientes = tmct0cliBo.findAll() ;
	    for (Tmct0cli cliente : clientes) {
	    	bean = new HashMap<String, Object>();
	    	bean.put("id", cliente.getCdbrocli());
	    	bean.put("descripcion", cliente.getDescli());
	    	list.add(bean);
	    }

	    resultados.put(RETURN_CLIENTE_LIST, list);

	    return resultados;
}
    private Date convertirStringADate(final String fechaString) {
	java.util.Date fecha = null;
	if (StringUtils.isNotEmpty(fechaString)) {
	    fecha = FormatDataUtils.convertStringToDate(fechaString);
	}
	return (Date) fecha;
    }
	@Override
	public List<String> getAvailableCommands() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getFields() {
		// TODO Auto-generated method stub
		return null;
	}
}
