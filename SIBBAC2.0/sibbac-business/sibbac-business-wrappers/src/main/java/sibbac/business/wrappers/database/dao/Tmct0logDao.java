package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.model.Tmct0log;
import sibbac.business.wrappers.database.model.Tmct0logId;

@Repository
public interface Tmct0logDao extends JpaRepository<Tmct0log, Tmct0logId> {

	@Query(value = "SELECT NEXT VALUE FOR TMCT0LOG_NUMSEC_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
	public BigDecimal getSequence();

	@Modifying
	@Transactional
	@Query(value = "INSERT INTO TMCT0LOG (NUORDEN, NBOOKING, NUCNFCLT, LGNUMSEC, LGDESCRI, LGDESEST, LGFECHA, LGHORA, FHAUDIT, "
			+ " CDUSUAUD, CDBROKER, CDMERCAD, NUALOSE1, NUVERSION, PKENTITY, PKBROKER1) "
			+ " VALUES (:nuorden, :nbooking, '', NEXT VALUE FOR TMCT0LOG_NUMSEC_SEQ, :msg, :dsestado, :fhaudit, :fhaudit, :fhaudit, "
			+ " :cdusuaud, '', '', 0, 0, '', 0 )", nativeQuery = true)
	void insertarRegistro(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking, @Param("msg") String msg,
			@Param("dsestado") String dsestado, @Param("fhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);
	
	@Modifying
  @Transactional
  @Query(value = "INSERT INTO TMCT0LOG (NUORDEN, NBOOKING, NUCNFCLT, LGNUMSEC, LGDESCRI, LGDESEST, LGFECHA, LGHORA, FHAUDIT, "
      + " CDUSUAUD, CDBROKER, CDMERCAD, NUALOSE1, NUVERSION, PKENTITY, PKBROKER1) "
      + " VALUES (:nuorden, :nbooking, :nucnfclt, NEXT VALUE FOR TMCT0LOG_NUMSEC_SEQ, :msg, :dsestado, :fhaudit, :fhaudit, :fhaudit, "
      + " :cdusuaud, '', '', 0, 0, '', 0 )", nativeQuery = true)
  void insertarRegistro(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("msg") String msg,
      @Param("dsestado") String dsestado, @Param("fhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);

} // Tmct0logDao
