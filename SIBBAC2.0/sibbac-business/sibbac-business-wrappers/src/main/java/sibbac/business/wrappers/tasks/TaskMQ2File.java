package sibbac.business.wrappers.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.common.fileWriter.MQManager;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_COMUNICACIONES.NAME, 
	name = Task.GROUP_COMUNICACIONES.JOB_MQ_READER, 
	interval = 1, delay = 30, 
	intervalUnit = IntervalUnit.MINUTE)
public class TaskMQ2File extends WrapperTaskConcurrencyPrevent {

    @Autowired
    private MQManager writer;
    @Value("${MQ.partenon.desgloses.in.name}")
    String queueConnectionFactoryName;

    @Value("${MQ.partenon.desgloses.in.qmgrname}")
    String inQueueName;

    @Value("${MQ.partenon.desgloses.in.folder}")
    String folderName;

    @Value("${MQ.partenon.desgloses.in.file}")
    String fileName;

    @Override
    public void executeTask() throws SIBBACBusinessException {
	writer.writeIntoFile("jms/" + queueConnectionFactoryName, "jms/"
		+ inQueueName, fileName, "file://" + folderName);
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_COMUNICACIONES_MQ_IN;
    }

}
