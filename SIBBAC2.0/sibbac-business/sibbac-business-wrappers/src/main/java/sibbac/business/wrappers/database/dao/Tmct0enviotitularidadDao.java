package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;

@Repository
public interface Tmct0enviotitularidadDao extends JpaRepository<Tmct0enviotitularidad, Long> {
  
  @Query(value = "SELECT TI.IDENVIOTITULARIDAD, TI.CDMIEMBROMKT "
      + " FROM (SELECT D.IDDESGLOSECAMARA, D.NUORDEN, D.NBOOKING, D.NUCNFCLT, D.NUCNFLIQ FROM TMCT0DESGLOSECAMARA D WHERE D.FECONTRATACION = :pfecontratacion AND D.CDESTADOASIG >= :pestadoAsignGte "
      + " AND D.CDESTADOTIT = :pestadoTitularidad AND D.IDCAMARA = :pcdcamara ) D "
      + " INNER JOIN TMCT0_DESGLOSECAMARA_ENVIO_RT RT ON D.IDDESGLOSECAMARA = RT.IDDESGLOSECAMARA AND RT.ESTADO <> :pNotEstadoEnvioRt  "
      + " INNER JOIN (SELECT TI.IDENVIOTITULARIDAD, TI.CDMIEMBROMKT FROM TMCT0ENVIOTITULARIDAD TI WHERE TI.FEINICIO = :pfecontratacion AND TI.ESTADO = :pestadoTI AND TI.IDCAMARA = :pcdcamara) "
      + " TI ON RT.IDENVIOTITULARIDAD = TI.IDENVIOTITULARIDAD "
      + " INNER JOIN TMCT0AFI A ON D.NUORDEN = A.NUORDEN AND D.NBOOKING = A.NBOOKING AND D.NUCNFCLT = A.NUCNFCLT AND D.NUCNFLIQ = A.NUCNFLIQ AND A.CDINACTI = 'A' "
      + " GROUP BY TI.IDENVIOTITULARIDAD, TI.CDMIEMBROMKT ORDER BY TI.CDMIEMBROMKT ASC, TI.IDENVIOTITULARIDAD ASC ", nativeQuery = true)
  List<Object[]> findAllIdsByCamaraAndFecontratacionAndEstado(@Param("pfecontratacion") Date fecontratacion,
      @Param("pcdcamara") Long cdcamara, @Param("pestadoTI") Character estadoEnvioTitularidad,
      @Param("pNotEstadoEnvioRt") Character notEstadoEnvioRt, @Param("pestadoAsignGte") int estadoAsignacionGte,
      @Param("pestadoTitularidad") int estadoTitularidad);

  @Query(value = "SELECT DISTINCT TI.FEINICIO FROM ( "
      + " SELECT D.IDDESGLOSECAMARA FROM TMCT0DESGLOSECAMARA D WHERE D.FELIQUIDACION BETWEEN :pfeliquidacion1 AND :pfeliquidacion2 "
      + " AND D.CDESTADOASIG >= :pestadoAsignGte AND D.CDESTADOTIT = :pestadoTitularidad AND D.IDCAMARA = :pcdcamara "
      + " ) D INNER JOIN TMCT0_DESGLOSECAMARA_ENVIO_RT RT ON D.IDDESGLOSECAMARA = RT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0ENVIOTITULARIDAD TI ON TI.IDENVIOTITULARIDAD=RT.IDENVIOTITULARIDAD AND TI.ESTADO = :pestado "
      + " ORDER BY TI.FEINICIO ASC ", nativeQuery = true)
  List<Date> findAllDistinctFeinicioByCamaraAndBetweenFeliquidacionAndEstado(
      @Param("pfeliquidacion1") Date pfeliquidacion1, @Param("pfeliquidacion2") Date pfeliquidacion2,
      @Param("pcdcamara") Long cdcamara, @Param("pestado") Character estado,
      @Param("pestadoAsignGte") int estadoAsignacionGte, @Param("pestadoTitularidad") int cdestadotit);

  @Query("select t from Tmct0enviotitularidad t where t.tmct0referenciatitular.cdreftitularpti = :refTit and t.feinicio = :fechaInicio "
      + " and t.cdmiembromkt = :cdmiembro and t.camaraCompensacion.cdCodigo = :cdcamara ")
  Tmct0enviotitularidad findByReferenciaTitularAndFechaInicioAndCdmiembromktAndCdcamara(@Param("refTit") String refTit,
      @Param("fechaInicio") Date fechaInicio, @Param("cdmiembro") String cdmiembro, @Param("cdcamara") String cdcamara);

  @Query("select t from Tmct0enviotitularidad t where t.tmct0referenciatitular.cdreftitularpti = :refTit and t.feinicio = :fechaInicio "
      + " and t.cdmiembromkt = :cdmiembro and t.tmct0referenciatitular.referenciaAdicional = :refAdi and t.camaraCompensacion.cdCodigo = :cdcamara ")
  Tmct0enviotitularidad findByReferenciaTitularAndReferenciaAdicionalAndFechaInicioAndCdmiembromktAndCdcamara(
      @Param("refTit") String refTit, @Param("refAdi") String refAdicional, @Param("fechaInicio") Date fechaInicio,
      @Param("cdmiembro") String cdmiembro, @Param("cdcamara") String cdcamara);

  @Query("select t from Tmct0enviotitularidad t where t.tmct0referenciatitular.cdreftitularpti = :refTit and t.feinicio = :fechaInicio "
      + " and t.estado <> 'C' ")
  List<Tmct0enviotitularidad> findAllByReferenciaTitularAndFinicioAndAlta(@Param("refTit") String refTit,
      @Param("fechaInicio") Date fechaInicio);

}
