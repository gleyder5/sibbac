package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0afiId;

@Repository
public interface Tmct0afiDao extends JpaRepository<Tmct0afi, Tmct0afiId>, JpaSpecificationExecutor<Tmct0afi> {

  @Query(value = "SELECT NEXT VALUE FOR TMCT0AFI_NUMSEC_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  BigDecimal getNewTmct0afiSequence();

  public List<Tmct0afi> findByIdIn(List<Tmct0afiId> idList);

  @Query("SELECT afi.cdholder FROM Tmct0afi afi WHERE afi.id.nuorden=:nuorden AND afi.id.nbooking=:nbooking AND afi.id.nucnfclt=:nucnfclt AND afi.id.nucnfliq=:nucnfliq AND afi.cdinacti = 'A' ")
  public List<String> findAllCdholderByAlcAndActivos(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  @Query("SELECT afi FROM Tmct0afi afi WHERE afi.id.nuorden=:nuorden AND afi.id.nbooking=:nbooking AND afi.id.nucnfclt=:nucnfclt AND afi.id.nucnfliq=:nucnfliq ")
  public List<Tmct0afi> findByAlcData(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  @Query("SELECT afi FROM Tmct0afi afi WHERE afi.id.nuorden=:nuorden AND afi.id.nbooking=:nbooking AND afi.id.nucnfclt=:nucnfclt AND afi.id.nucnfliq=:nucnfliq AND afi.cdinacti = 'A' ")

  public List<Tmct0afi> findActivosByAlcData(@Param("nuorden") String nuorden,
                                             @Param("nbooking") String nbooking,
                                             @Param("nucnfclt") String nucnfclt,
                                             @Param("nucnfliq") Short nucnfliq);

  
  @Query("SELECT afi FROM Tmct0afi afi WHERE afi.id.nuorden=:nuorden AND afi.id.nbooking=:nbooking AND afi.id.nucnfclt=:nucnfclt AND afi.id.nucnfliq=:nucnfliq AND afi.cdinacti = 'A' AND afi.tptiprep NOT IN (:lTiposNoPermitidos) ")
  public List<Tmct0afi> findActivosTmct0afiExcludedTipos(@Param("nuorden") String nuorden,
                                             @Param("nbooking") String nbooking,
                                             @Param("nucnfclt") String nucnfclt,
                                             @Param("nucnfliq") Short nucnfliq,
                                             @Param("lTiposNoPermitidos") List<Character> lTiposNoPermitidos);

  @Query("SELECT afi FROM Tmct0afi afi WHERE afi.id.nuorden=:nuorden AND afi.cdinacti = 'A' ")
  public List<Tmct0afi> findActivosByNuorden(@Param("nuorden") String nuorden);

  @Query("SELECT afi FROM Tmct0afi afi WHERE afi.tmct0alo.id=:alloId AND afi.cdinacti = 'A' ")
  public List<Tmct0afi> findActivosByAlloId(@Param("alloId") Tmct0aloId nucnfclt);

  @Query(value = "SELECT MAX(CDBLOQUE) FROM TMCT0AFI WHERE NUORDEN=:nuorden", nativeQuery = true)
  public List<Object> obtenerCdBloqueMasAltoPorNuorden(@Param("nuorden") String nuorden);

  @Query("SELECT MAX(afi.cdbloque) FROM Tmct0afi afi WHERE afi.tmct0alo.id=:alloId AND afi.cdinacti = 'A' ")
  public List<Object> obtenerCdBloqueMasAltoByAlloId(@Param("alloId") Tmct0aloId alloId);

  @Query("SELECT MAX(afi.cdbloque) FROM Tmct0afi afi WHERE afi.tmct0alo.id=:alloId AND afi.id.nucnfliq = :nucnfliq AND afi.cdinacti = 'A' ")
  public List<Object> obtenerCdBloqueMasAltoByAlloIdAndNucnfliq(@Param("alloId") Tmct0aloId alloId,
      @Param("nucnfliq") short nucnfliq);

  @Query("SELECT o FROM Tmct0afi o WHERE o.id.nuorden =:nuorden AND o.id.nbooking=:nbooking AND o.id.nucnfclt=:nucnfclt AND "
      +
      // " o.id.nucnfliq=:nucnfliq  AND +"
      " o.cdreferenciatitular =:cdreferenciatitular")
  List<Tmct0afi> getAfis(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, // @Param("nucnfliq") short
      // nucnfliq,
      @Param("cdreferenciatitular") String cdreferenciatitular);

  @Query("SELECT o FROM Tmct0afi o WHERE o.id.nuorden =:nuorden AND o.id.nbooking=:nbooking AND o.id.nucnfclt=:nucnfclt")
  List<Tmct0afi> getAfis2(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt);

  @Query("SELECT afi FROM Tmct0afi afi WHERE afi.tmct0alo.refCliente = :refCliente AND afi.cdinacti = 'A' ")
  List<Tmct0afi> findAllTmct0afiByRefClienteAndActivo(@Param("refCliente") String refCliente);

}
