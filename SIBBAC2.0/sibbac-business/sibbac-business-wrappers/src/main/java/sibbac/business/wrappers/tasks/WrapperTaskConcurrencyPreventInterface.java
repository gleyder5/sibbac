package sibbac.business.wrappers.tasks;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;

public interface WrapperTaskConcurrencyPreventInterface {
  
  
  public void execute();
  
  public void executeTask() throws Exception;

  public TIPO determinarTipoApunte();

}
