package sibbac.business.wrappers.rest;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.CobrosBo;
import sibbac.business.wrappers.database.dto.CobroDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class ServiceCobros.
 */
@SIBBACService
public class SIBBACServiceCobros implements SIBBACServiceBean {

	private static final Logger	LOG								= LoggerFactory.getLogger( SIBBACServiceCobros.class );

	public static final String	CMD_INSERTAR_COBRO				= "insertarCobro";
	public static final String	CMD_LISTA_COBRO					= "getListaCobro";

	@Autowired
	private CobrosBo			cobrosBo;

	public static final String	FILTER_ID_ALIAS					= "idAlias";
	public static final String	FILTER_IMPORTE_COBRADO			= "importeCobrado";
	public static final String	PARAM_CIERRE_FACTURA			= "cierre";
	public static final String	FILTER_FECHA_CONTABILIZACION	= "fechaContabilizacion";
	public static final String	FILTER_FECHA_DESDE				= "fechaDesde";
	public static final String	FILTER_FECHA_HASTA				= "fechaHasta";
	public static final String	FILTER_NUMERO_COBRO				= "numCobro";
	public static final String	PARAM_ID_FACTURA				= "idFactura";
	public static final String	PARAM_IMPORTE_APLICADO			= "importeAplicado";

	public static final String	RESULTADO						= "resultado";
	public static final String	RESULTADO_SUCCESS				= "SUCCESS";

	public SIBBACServiceCobros() {
		LOG.trace( "[ServiceCobros::<constructor>] Initiating SIBBAC Service for \"Cobros\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {

		LOG.trace( "[ServiceCobros::process] Processing a web request..." );

		// prepara webResponse
		final WebResponse webResponse = new WebResponse();

		// obtiene comandos(paths)
		LOG.trace( "[ServiceCobros::process] Command.: [{}]", webRequest.getAction() );

		// selecciona la accion
		switch ( webRequest.getAction() ) {
			case CMD_INSERTAR_COBRO:
				this.insertarCobro( webRequest, webResponse );
				break;

			case CMD_LISTA_COBRO:

				webResponse.setResultados( cobrosBo.getListaCobro( webRequest ) );
				// webResponse.setResultados( cobrosBo.getAllRevisions());
				break;

			default:
				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;
		}

		return webResponse;

	}

	/**
	 * @param webRequest
	 * @return
	 */
	private void insertarCobro( final WebRequest webRequest, final WebResponse webResponse ) throws SIBBACBusinessException {

		final Map< String, String > filters = webRequest.getFilters();
		final List< Map< String, String >> params = webRequest.getParams();
		// comprueba que los parametros han sido enviados
		if ( CollectionUtils.isEmpty( params ) || MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los filtros y los parámetros de llamada." );

		}

		final Long idAlias = getIdAlias( filters.get( FILTER_ID_ALIAS ) );
		final Date fechaContabilizacion = convertirStringADate( filters.get( FILTER_FECHA_CONTABILIZACION ) );
		final BigDecimal importeCobrado = getImporteCobrado( filters.get( FILTER_IMPORTE_COBRADO ) );
		final CobroDTO cobroDto = new CobroDTO( idAlias, fechaContabilizacion, importeCobrado );
		for ( final Map< String, String > param : params ) {
			final Long idFactura = getIdFactura( param.get( PARAM_ID_FACTURA ) );
			final BigDecimal importeAplicado = getImporteAplicado( param.get( PARAM_IMPORTE_APLICADO ) );
			final Boolean cierreFactura = getCierreFactura( param.get( PARAM_CIERRE_FACTURA ) );
			cobroDto.addLineaCobro( idFactura, importeAplicado, cierreFactura.booleanValue() );
		}
		final Map< String, String > resultados = new HashMap< String, String >();
		cobrosBo.insertarCobro( cobroDto );
		resultados.put( RESULTADO, RESULTADO_SUCCESS );
		webResponse.setResultados( resultados );
		LOG.trace( " Se rellena el WebResponse con las facturas marcadas " );
	}

	/**
	 * @param filtros
	 * @return
	 */
	private Date convertirStringADate( final String fechaString ) {
		Date fecha = null;
		if ( StringUtils.isNotEmpty( fechaString ) ) {
			fecha = FormatDataUtils.convertStringToDate( fechaString );
		}
		return fecha;
	}

	/**
	 * @param idFacturaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdFactura( final String idFacturaStr ) throws SIBBACBusinessException {
		Long idFactura = null;
		try {
			if ( StringUtils.isNotBlank( idFacturaStr ) ) {
				idFactura = NumberUtils.createLong( idFacturaStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idFactrua\": [" + idFacturaStr + "] " );
		}
		return idFactura;
	}

	/**
	 * @param idAliasStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAlias( final String idAliasStr ) throws SIBBACBusinessException {
		Long idAlias = null;
		try {
			if ( StringUtils.isNotBlank( idAliasStr ) ) {
				idAlias = NumberUtils.createLong( idAliasStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] " );
		}
		return idAlias;
	}

	/**
	 * @param importeCobradoStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private BigDecimal getImporteCobrado( final String importeCobradoStr ) throws SIBBACBusinessException {
		BigDecimal importeCobrado = null;
		try {
			if ( StringUtils.isNotBlank( importeCobradoStr ) ) {
				importeCobrado = FormatDataUtils.convertStringToBigDecimal( importeCobradoStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"importe cobrado\": [" + importeCobradoStr + "] " );
		}
		return importeCobrado;
	}

	/**
	 * @param importeAplicadoStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private BigDecimal getImporteAplicado( final String importeAplicadoStr ) throws SIBBACBusinessException {
		BigDecimal importeCobrado = null;
		try {
			if ( StringUtils.isNotBlank( importeAplicadoStr ) ) {
				importeCobrado = FormatDataUtils.convertStringToBigDecimal( importeAplicadoStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"importe aplicado\": [" + importeAplicadoStr + "] " );
		}
		return importeCobrado;
	}

	/**
	 * @param cierreFactura
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Boolean getCierreFactura( final String cierreFacturaStr ) throws SIBBACBusinessException {
		Boolean cierreFactura = null;
		try {
			if ( StringUtils.isNotBlank( cierreFacturaStr ) ) {
				cierreFactura = FormatDataUtils.convertStringToBoolean( cierreFacturaStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"cierre factura\": [" + cierreFacturaStr + "] " );
		}
		return cierreFactura;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_INSERTAR_COBRO );
		commands.add( CMD_LISTA_COBRO );
		return commands;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		return fields;
	}

}
