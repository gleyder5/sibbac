package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ejeDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0ejeDynamicValuesId;

@Repository
public interface Tmct0ejeDynamicValuesDao extends JpaRepository<Tmct0ejeDynamicValues, Tmct0ejeDynamicValuesId> {

}
