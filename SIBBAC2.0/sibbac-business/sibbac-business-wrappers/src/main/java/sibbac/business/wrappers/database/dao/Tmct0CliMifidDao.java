package sibbac.business.wrappers.database.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CliMifid;

@Repository
public interface Tmct0CliMifidDao extends JpaRepository< Tmct0CliMifid, BigInteger > {

	@Query( "SELECT mifid FROM Tmct0CliMifid mifid WHERE mifid.cliente.idClient = :idClient" )
	public Tmct0CliMifid findByIdCliente( @Param( "idClient" ) Long idClient);
	
}
