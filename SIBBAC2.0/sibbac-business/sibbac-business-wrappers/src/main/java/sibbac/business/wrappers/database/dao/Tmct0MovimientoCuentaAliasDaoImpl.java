package sibbac.business.wrappers.database.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.model.Tmct0Valores;

@SuppressWarnings("unchecked")
public class Tmct0MovimientoCuentaAliasDaoImpl {

  private static final Logger LOG = LoggerFactory.getLogger(MovimientoCuentaAliasDAOImpl.class);
  private static final String OPERACION_COMPRA = "operacionCompra";
  private static final String FLIQUIDACION_A = "fliquidacionA";
  private static final String FLIQUIDACION_DE = "fliquidacionDe";
  private static final String FCONTRATACION_A = "fcontratacionA";
  private static final String FCONTRATACION_DE = "fcontratacionDe";
  private static final String OPERACION_VENTA = "operacionVenta";
  private static final String ISIN = "isin";

  @PersistenceContext
  private EntityManager em;

  @Autowired
  Tmct0ValoresDao valoresDao;

  public List<?> findAllByRangeFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(long idCuentaComp,
      Map<String, Serializable> filtros) {
    String select = "SELECT M " + "FROM MovimientoCuentaAlias M WHERE ";
    String orderBy = "ORDER BY M.isin desc";
    String dateRange = "AND M.fecha BETWEEN :fechaInicio AND :fechaFin";
    StringBuilder sb = new StringBuilder(select);
    // Construimos la query

    boolean flagAnd = false;
    boolean needAlias = false;
    boolean needCuenta = false;
    boolean needIsin = false;

    if (checkIfKeyExist("isin", filtros)) {

      needIsin = true;
      sb.append("M.isin = :" + filtros.get("isin") + " ");
      flagAnd = true;
    }

    if (checkIfKeyExist("aliasDes", filtros)) {

      needAlias = true;
      if (flagAnd) {
        sb.append("AND M.alias = :alias ");

      }
      else {
        sb.append("M.alias = :alias ");
      }
      flagAnd = true;
    }
    if (checkIfKeyExist("cuenta", filtros)) {
      needCuenta = true;

      if (flagAnd) {
        sb.append("AND M.cuentaLiq = :cuenta ");
      }
      else {
        sb.append("M.cuentaLiq = :cuenta ");
      }
    }
    sb.append(dateRange);
    sb.append(orderBy);

    Query query = em.createQuery(sb.toString());

    if (needIsin) {
      String isin = (String) filtros.get("isin");
      query.setParameter("isin", isin);
    }
    if (needAlias) {
      String alias = (String) filtros.get("aliasDes");
      query.setParameter("alias", alias);
    }
    if (needCuenta) {
      String cuenta = (String) filtros.get("cuenta");
      query.setParameter("cuenta", cuenta);
    }

    Date fechaInicio = (Date) filtros.get(FLIQUIDACION_DE);
    Date fechaFin = (Date) filtros.get("fliquidacion");

    query.setParameter("fechaInicio", fechaInicio);
    query.setParameter("fechaFin", fechaFin);
    return query.getResultList();
  }

  public List<?> findAllSumByIsinAndCuentaLiquidacionAndFechaGroupByIsinAndCuentaLiquidacion(
      Map<String, Serializable> filtros) {
    String select = "SELECT  new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData"
        + "(M.isin, M.cdcodigocuentaliq, sum(M.imefectivo), max(M.settlementdate)) "
        + "FROM Tmct0MovimientoCuentaAlias M WHERE ";

    String dateRange = "AND M.tradedate BETWEEN :fromdate AND :ftodate ";
    String dateSearch = "M.tradedate >= :fromdate ";
    String groupBy = "GROUP BY M.isin, M.cdcodigocuentaliq ";
    // String orderBy = "ORDER BY fechaOrden desc ";
    StringBuilder sb = new StringBuilder(select);
    String criteriaCdOperacion = "AND (M.cdoperacion =:operacionCompra OR M.cdoperacion =:operacionVenta) ";
    boolean addedCriteriaOperacion = false;
    // Construimos la query

    boolean flagAnd = false;
    boolean needCuenta = false;
    boolean needIsin = false;
    boolean needToDate = false;

    String isin = (String) filtros.get("isin");
    if (checkIfKeyExist("isin", filtros)) {
      // Añadimos el isin a la consulta
      needIsin = true;
      if (isin.indexOf("#") != -1) {
        isin = isin.replace("#", "");
        sb.append("M.isin <> :isin ");
      }
      else {
        sb.append("M.isin = :isin ");
      }

      flagAnd = true;
    }
    if (checkIfKeyExist("cuenta", filtros)) {

      // NO Añadimos la cuenta a la consulta
      needCuenta = false;

      if (flagAnd) {
        sb.append("AND M.cdcodigocuentaliq IN " + filtros.get("cuenta"));
      }
      else {
        sb.append("M.cdcodigocuentaliq IN " + filtros.get("cuenta"));
      }
      flagAnd = true;
    }
    if (checkIfKeyExist("ftodate", filtros)) {

      // Añadimos la fecha final a la consulta
      needToDate = true;
      sb.append(dateRange);

    }
    else {

      if (flagAnd) {

        // Añadimos la consulta de fechas solo con origen
        sb.append("AND " + dateSearch);
      }
      else {
        sb.append(dateSearch);
      }
    }
    if (checkIfKeyExist(OPERACION_COMPRA, filtros) || checkIfKeyExist(OPERACION_VENTA, filtros)
        && !addedCriteriaOperacion) {
      addedCriteriaOperacion = true;
      sb.append(criteriaCdOperacion);
    }
    sb.append(groupBy);

    String consulta = sb.toString();

    // LANZAMOS LA QUERY
    Query query = em.createQuery(consulta);

    if (needIsin) {

      query.setParameter("isin", isin);
    }
    if (needCuenta) {
      List<String> cuenta = (List<String>) filtros.get("cuenta");
      query.setParameter("cuenta", cuenta);
    }
    if (needToDate) {
      Date toDate = (Date) filtros.get("ftodate");
      java.sql.Date sqlToDate = new java.sql.Date(toDate.getTime());
      query.setParameter("ftodate", sqlToDate);
      LOG.debug("ftodate: " + sqlToDate);
    }
    if (addedCriteriaOperacion) {
      query.setParameter(OPERACION_COMPRA, filtros.get(OPERACION_COMPRA));
      query.setParameter(OPERACION_VENTA, filtros.get(OPERACION_VENTA));
    }

    // Esta fecha la tendremos siempre
    Date fromdate = (Date) filtros.get("fromdate");
    java.sql.Date sqlFromDate = new java.sql.Date(fromdate.getTime());
    query.setParameter("fromdate", sqlFromDate);
    LOG.debug("fromdate: " + sqlFromDate);

    List<MovimientoCuentaAliasData> result = query.getResultList();
    for (MovimientoCuentaAliasData mov : result) {

      if (mov.getIsin() != null) {

        List<Tmct0Valores> listValores = valoresDao.findByCodigoDeValorOrderByFechaEmisionDesc(mov.getIsin());

        if (listValores != null && !listValores.isEmpty()) {

          String descripcion = listValores.get(0).getDescripcion();
          mov.setDescrIsin(descripcion);
        }
      }
    }
    return result;
  }

  public List<MovimientoCuentaAliasData> findAll(Map<String, Serializable> filtros, String[] operaciones) {
    return findAll(filtros, "", operaciones);
  }

  public List<MovimientoCuentaAliasData> findAll(Map<String, Serializable> filtros, String sortBy,
      String[] cdoperaciones) {
    List<MovimientoCuentaAliasData> resultList = new LinkedList<MovimientoCuentaAliasData>();
    String selectDescIsin = "(SELECT v.descripcion FROM Tmct0Valores v WHERE m.isin = v.codigoDeValor) ";
    String selectDescOperacion = "(SELECT DISTINCT tp.descripcion FROM Tmct0TipoMovimientoConciliacion tp WHERE tp.tipoOperacion = m.cdoperacion)";
    String select = "SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(m.cdcodigocuentaliq, m.cdaliass, m.settlementdate, m.isin, "
        + selectDescIsin
        + ", m.mercado, m.cdoperacion, "
        + selectDescOperacion
        + " ,m.signoanotacion, m.tradedate, m.titulos, m.imefectivo, m.corretaje, m.auditUser, m.auditDate, m.sistemaLiquidacion) ";
    String from = "FROM Tmct0MovimientoCuentaAlias m ";
    StringBuilder where = new StringBuilder();
    boolean addedWhere = false;
    String sort = "ORDER BY ";
    String criteriaCdOperacion = "AND (m.cdoperacion =:operacionCompra OR m.cdoperacion =:operacionVenta) ";
    boolean addedCriteriaOperacion = false;
    // Hack: Pedido el 2015/10/01: Ordenar por TradeDate y Signo/Sentido
    boolean ordenarPorFLiq = (filtros.get("fliquidacionDe") != null || filtros.get("fliquidacionDe") != null);
    sort += (sortBy != null && !"".equals(sortBy)) ? sortBy : "m.isin, cdaliass, "
        + ((!ordenarPorFLiq) ? "m.tradedate" : "m.settlementdate") + " ASC , m.signoanotacion";
    // fechas
    String fcontratacionBtw = "m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "m.settlementdate IS NOT NULL AND (m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA) ";
    // Fecha liquidacion
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    BigDecimal lFliquidacion = BigDecimal.ZERO;
    /* Si viene lista de operaciones, se construye el criterio de búsqueda */
    StringBuilder criteriaOperaciones = null;
    if (cdoperaciones != null) {
      criteriaOperaciones = new StringBuilder();
      for (String cdop : cdoperaciones) {
        if (criteriaOperaciones.length() > 0) {
          criteriaOperaciones.append("OR ");
        }
        else {
          criteriaOperaciones.append("(");
        }
        criteriaOperaciones.append("m.cdoperacion = '").append(cdop).append("' ");
      }
      criteriaOperaciones.append(") ");
    }

    if (filtros.get(FLIQUIDACION_DE) != null) {
      Date fLiquidacion = (Date) filtros.get(FLIQUIDACION_DE);
      String strFLiq = sdf.format(fLiquidacion);

      try {
        lFliquidacion = BigDecimal.valueOf(Long.valueOf(strFLiq));
      }
      catch (NumberFormatException ex) {
        LOG.error(ex.getMessage(), ex);
      }
    }
    StringBuilder querySB = new StringBuilder(select).append(from);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null) {
        if (!addedWhere) {
          where.append("WHERE ");
          addedWhere = true;
        }
        else if (!where.toString().endsWith("AND ")) {
          where.append("AND ");
        }
        String param = entry.getKey();
        if (!"".equals(entry.getKey())
            && (!entry.getKey().equals(FCONTRATACION_DE) && !entry.getKey().equals(FCONTRATACION_A)
                && !entry.getKey().equals(FLIQUIDACION_DE) && !entry.getKey().equals(FLIQUIDACION_A)
                && !entry.getKey().equals(OPERACION_VENTA) && !entry.getKey().equals(OPERACION_COMPRA))) {
          if (((String) entry.getValue()).indexOf("#") != -1) {
            where.append("m.").append(entry.getKey()).append("<>:").append(entry.getKey()).append(" ");
          }
          else {
            where.append("m.").append(entry.getKey()).append("=:").append(entry.getKey()).append(" ");
          }
        }
        else if (entry.getKey().equals(FCONTRATACION_DE)) {
          // si vienen las dos fechas se hace un between
          if (checkIfKeyExist(FCONTRATACION_A, filtros)) {
            where.append(fcontratacionBtw);
          }
          else {
            where.append("m.tradedate >=:fcontratacionDe ");
          }
        }
        else if (entry.getKey().equals(FLIQUIDACION_DE)) {
          // si vienen las dos fechas se hace un between
          if (checkIfKeyExist(FLIQUIDACION_A, filtros)) {
            where.append(fliquidacionBtw);
          }
          else {
            where.append("m.settlementdate IS NOT NULL AND m.settlementdate >=:fliquidacionDe ");
          }
        }
        else if ((param.equals(OPERACION_VENTA) || param.equals(OPERACION_COMPRA)) && !addedCriteriaOperacion) {
          addedCriteriaOperacion = true;
          where.append(criteriaCdOperacion);
        }
        else if (param.equals(ISIN)) {
          where.append("m.isin = :isin ");
        }
      }
    }
    if (criteriaOperaciones != null) {
      if (!where.toString().endsWith("AND ")) {
        where.append("AND ");
      }
      where.append(criteriaOperaciones.toString());
    }
    // Si se ha quedado la clausula AND al final, se elimina
    if (where.toString().endsWith("AND ")) {
      where.delete(where.toString().lastIndexOf("AND"), where.toString().length());
    }
    querySB.append(where).append(sort);
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
        if (entry.getKey().equals("fliquidacionA")) {
          query.setParameter(entry.getKey(), DateUtils.addDays((Date) entry.getValue(), 1));
        }
        else {
          if (entry.getKey().equals("cdcodigocuentaliq") && ((String) entry.getValue()).indexOf("#") != -1)
            query.setParameter(entry.getKey(), ((String) entry.getValue()).replace("#", ""));
          else
            query.setParameter(entry.getKey(), entry.getValue());
        }
      }
      resultList = query.getResultList();
    }
    catch (PersistenceException | IllegalArgumentException | NullPointerException ex) {
      LOG.error(ex.getMessage(), ex);
    }
    if (lFliquidacion != BigDecimal.ZERO) {
      addAliasDescription(resultList, lFliquidacion);
    }
    else {
      addAliasDescriptionZero(resultList);
    }
    return resultList;
  }

  private List<MovimientoCuentaAliasData> addAliasDescriptionZero(List<MovimientoCuentaAliasData> resultList) {

    StringBuilder select = new StringBuilder("SELECT a.descrali, a.fhinicio, a.fhfinal FROM Tmct0ali a ");
    StringBuilder where = new StringBuilder("WHERE  CAST(a.fhfinal as integer) = 99991231 AND a.id.cdaliass =:alias ");
    StringBuilder sbQuery = new StringBuilder(select).append(where);
    // si no se ha encontrado ninguna coincidencia no se puede eliminar
    // de la lista
    // boolean encontrado = false;
    for (int i = 0; i < resultList.size(); i++) {
      MovimientoCuentaAliasData first = resultList.get(i);
      String alias = (first.getCdalias() != null) ? first.getCdalias().trim() : "";
      if (alias == null || "".equals(alias)) {
        continue;
      }
      Query query = null;
      Object[] result = null;
      try {
        query = em.createQuery(sbQuery.toString());
        query.setParameter("alias", alias);
        List<Object[]> lista = query.getResultList();
        if (lista != null && lista.size() > 0) {
          result = lista.get(0);
        }
      }
      catch (IllegalArgumentException | NullPointerException | PersistenceException ex) {
        LOG.error(ex.getMessage(), ex);
        continue;
      }
      if (result != null) {
        String descripcion = (String) result[0];
        Integer fhinicio = (Integer) result[1];
        Integer fhfinal = (Integer) result[2];
        first.setDescali(descripcion);
        first.setFhinicio(fhinicio);
        first.setFhfinal(fhfinal);
      }
    }
    return resultList;
  }

  private List<MovimientoCuentaAliasData> addAliasDescription(List<MovimientoCuentaAliasData> resultList,
      BigDecimal fliq) {

    StringBuilder select = new StringBuilder("SELECT a.descrali, a.fhinicio, a.fhfinal FROM Tmct0ali a ");
    StringBuilder where = new StringBuilder("WHERE CAST(a.fhinicio as integer) <= " + fliq
        + " AND CAST(a.fhfinal as integer) >= " + fliq + " AND a.id.cdaliass =:alias ");
    StringBuilder sbQuery = new StringBuilder(select).append(where);
    // si no se ha encontrado ninguna coincidencia no se puede eliminar
    // de la lista
    // boolean encontrado = false;
    for (int i = 0; i < resultList.size(); i++) {
      MovimientoCuentaAliasData first = resultList.get(i);
      String alias = (first.getCdalias() != null) ? first.getCdalias().trim() : "";
      if (alias == null || "".equals(alias)) {
        continue;
      }
      Query query = null;
      Object[] result = null;
      try {
        query = em.createQuery(sbQuery.toString());
        query.setParameter("alias", alias);
        List<Object[]> lista = query.getResultList();
        if (lista != null && lista.size() > 0) {
          result = lista.get(0);
        }
      }
      catch (IllegalArgumentException | NullPointerException | PersistenceException ex) {
        LOG.error(ex.getMessage(), ex);
        continue;
      }
      if (result != null) {
        String descripcion = (String) result[0];
        Integer fhinicio = (Integer) result[1];
        Integer fhfinal = (Integer) result[2];
        first.setDescali(descripcion);
        first.setFhinicio(fhinicio);
        first.setFhfinal(fhfinal);
      }
    }
    return resultList;
  }

  public List<MovimientoCuentaAliasData> findAllMovGroupBySentido(Map<String, Serializable> filtros, String sortBy)
      throws PersistenceException, IllegalArgumentException, NullPointerException {
    List<MovimientoCuentaAliasData> resultList = new LinkedList<MovimientoCuentaAliasData>();
    StringBuilder select = new StringBuilder();
    /*
     * La siguiente select, asegura que se tomen los movimientos hasta la fecha
     * distinguiendo por el alias de quién realizó dichos movimientos, de lo
     * contrario agruparia por isin y codLiq sin tener esto en cuenta la select
     * agrupa los registros por isin y codLiq hasta la fecha, pero con mismo
     * cdaliass. !Atención, la fecha es obligatoria, de lo contrario esta query
     * no dará los resultados coherentes porque no distinguiria los isines
     */
    String selectDescIsin = "(SELECT v.descripcion FROM Tmct0Valores v WHERE m.isin = v.codigoDeValor) ";
    select.append("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData( 0, ")
        .append("m.cdcodigocuentaliq, ").append("m.isin, ").append(selectDescIsin).append(", SUM(m.titulos), ")
        .append("SUM(m.imefectivo)) ");
    String from = "FROM Tmct0MovimientoCuentaAlias m ";
    // Solo nos interesa los movimientos no procesados y que han liquidado
    // (fecha liquidacion distinta de null)
    StringBuilder where = new StringBuilder(
        "WHERE (m.guardadosaldo = false OR m.guardadosaldo IS NULL) AND m.settlementdate IS NOT NULL ");
    String groupBy = "GROUP BY m.isin, m.cdcodigocuentaliq ";
    boolean addedWhere = true;
    // fechas
    String fcontratacionBtw = "m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String fliquidacionBtw = "m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";

    // query string filan
    StringBuilder querySB = new StringBuilder(select).append(from);
    // se construye la query segun los filtros
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null) {
        if (!addedWhere) {
          where.append("WHERE ");
          addedWhere = true;
        }
        else if (!where.toString().endsWith("AND ")) {
          where.append("AND ");
        }
        String keyParam = entry.getKey();
        if (keyParam != null && !"".equals(keyParam)) {
          if (!keyParam.equals(FCONTRATACION_DE) && !keyParam.equals(FCONTRATACION_A)
              && !keyParam.equals(FLIQUIDACION_DE) && !keyParam.equals(FLIQUIDACION_A)
              && !keyParam.equals(OPERACION_VENTA) && !keyParam.equals(OPERACION_COMPRA)) {

            String valor = (String) entry.getValue();
            if (valor.indexOf("#") != -1) {
              entry.setValue(valor.replace("#", ""));
              where.append("m.").append(keyParam).append("<>:").append(keyParam).append(" ");
            }
            else
              where.append("m.").append(keyParam).append("=:").append(keyParam).append(" ");

          }
          else if (keyParam.equals(FCONTRATACION_DE)) {
            // si vienen las dos fechas se hace un between
            if (checkIfKeyExist(FCONTRATACION_A, filtros)) {
              where.append(fcontratacionBtw);
            }
            else {
              where.append("m.tradedate >=:fcontratacionDe ");
            }
          }
          else if (keyParam.equals(FLIQUIDACION_DE)) {
            // si vienen las dos fechas se hace un between
            if (checkIfKeyExist(FLIQUIDACION_A, filtros)) {
              where.append(fliquidacionBtw);
            }
            else {
              // where.append(maxFechaClauseByCdalias);
              where.append("m.settlementdate <=:fliquidacionDe ");
            }
          }
        }
      }
    }

    // Si se ha quedado la clausula AND al final, se elimina
    if (where.toString().endsWith("AND ")) {
      where.delete(where.toString().lastIndexOf("AND"), where.toString().length());
    }
    querySB.append(where).append(groupBy);
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
        query.setParameter(entry.getKey(), entry.getValue());
      }
      resultList = query.getResultList();
    }
    catch (PersistenceException | IllegalArgumentException | NullPointerException ex) {
      LOG.error(ex.getMessage(), ex);
      throw ex;
    }

    return resultList;
  }

  private boolean checkIfKeyExist(String key, Map<String, Serializable> params) {
    boolean exist = false;
    if (params.get(key) != null && !params.get(key).equals("")) {
      exist = true;
    }
    return exist;
  }
}
