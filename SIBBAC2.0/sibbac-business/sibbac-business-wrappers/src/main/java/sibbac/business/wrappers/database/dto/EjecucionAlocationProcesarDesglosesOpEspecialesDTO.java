package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejeId;

public class EjecucionAlocationProcesarDesglosesOpEspecialesDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -3039188566251926100L;

  private Long idejecucionalocation;
  private Long idgrupoeje;
  private Date fevalor;
  private BigDecimal impreciomkt;
  private String cdtipoop;
  private String cdsettmkt;
  private String cdcamara;
  private Long idCamara;
  private Character asigcomp;
  private String tradingvenue;
  private String nuagreje;
  private Date feejecuc;
  private Tmct0aloId tmct0aloId;
  private Tmct0ejeId tmct0ejeId;
  private String cdbroker;
  private BigDecimal nutitulos;

  // DATOS EJECUCION
  private String nuordenmkt;
  private String nuexemkt;
  private String cdmiembromkt;
  private Character cdindcotizacion;
  private String cdsegmento;
  private String cdPlataformaNegociacion;
  private Date hoejecuc;
  private String tpopebol;

  // DATOS OPERACIONECC
  private Date feordenmkt;
  private Date hoordenmkt;
  private String nuoperacionprev;

  // DATOS MOVIMIENTO
  private String cdoperacionecc;
  private String miembroDestino;
  private String corretajePtiMiembroDestino;
  private String cdrefasignacion;
  private String cuentaCompensacion;
  private String cdestadomov;
  private String cdrefmovimiento;
  private String cdrefmovimientoecc;
  private String cdrefnotificacion;
  private String cdrefnotificacionprev;
  private String cdtipomov;
  private Long idMovimientoFidessa;

  private Tmct0desgloseclitit desgloseRelacionado;
  
  private BigDecimal canonContratacionTeorico;
  private BigDecimal canonContratacionAplicado;
  

  public EjecucionAlocationProcesarDesglosesOpEspecialesDTO(Tmct0ejeId tmct0ejeId, BigDecimal nutitulos) {
    super();
    this.tmct0ejeId = tmct0ejeId;
    this.nutitulos = nutitulos;
  }

  public EjecucionAlocationProcesarDesglosesOpEspecialesDTO(Long idejecucionalocation,
                                                            Long idgrupoeje,
                                                            Date fevalor,
                                                            BigDecimal impreciomkt,
                                                            String cdtipoop,
                                                            String cdsettmkt,
                                                            String cdcamara,
                                                            Character asigcomp,
                                                            String tradingvenue,
                                                            String nuagreje,
                                                            Date feejecuc,
                                                            Tmct0aloId tmct0aloId,
                                                            Tmct0ejeId tmct0ejeId,
                                                            String cdbroker,
                                                            BigDecimal nutitulos,
                                                            // DATOS EJECUCION
                                                            String nuordenmkt,
                                                            String nuexemkt,
                                                            String cdmiembromkt,
                                                            Character cdindcotizacion,
                                                            String cdsegmento,
                                                            String cdPlataformaNegociacion,
                                                            Date hoejecuc,
                                                            String tpopebol,

                                                            // DATOS OPERACIONECC
                                                            Date feordenmkt,
                                                            Date hoordenmkt,
                                                            String nuoperacionprev,

                                                            // DATOS MOVIMIENTO
                                                            String cdoperacionecc,
                                                            String miembroDestino,
                                                            String cdrefasignacion,
                                                            String cuentaCompensacion,
                                                            String cdestadomov,
                                                            String cdrefmovimiento,
                                                            String cdrefmovimientoecc,
                                                            String cdrefnotificacion,
                                                            String cdrefnotificacionprev,
                                                            String cdtipomov,
                                                            Long idMovimientoFidessa) {
    super();
    this.idejecucionalocation = idejecucionalocation;
    this.idgrupoeje = idgrupoeje;
    this.fevalor = fevalor;
    this.impreciomkt = impreciomkt;
    this.cdtipoop = cdtipoop;
    this.cdsettmkt = cdsettmkt;
    this.cdcamara = cdcamara;
    this.asigcomp = asigcomp;
    this.tradingvenue = tradingvenue;
    this.nuagreje = nuagreje;
    this.feejecuc = feejecuc;
    this.tmct0aloId = tmct0aloId;
    this.tmct0ejeId = tmct0ejeId;
    this.cdbroker = cdbroker;
    this.nutitulos = nutitulos;

    // DATOS EJECUCION
    this.nuordenmkt = nuordenmkt;
    this.nuexemkt = nuexemkt;
    this.cdmiembromkt = cdmiembromkt;
    this.cdindcotizacion = cdindcotizacion;
    this.cdsegmento = cdsegmento;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.hoejecuc = hoejecuc;
    this.tpopebol = tpopebol;

    // DATOS OPERACIONECC
    this.feordenmkt = feordenmkt;
    this.hoordenmkt = hoordenmkt;
    this.nuoperacionprev = nuoperacionprev;

    // DATOS MOVIMIENTO
    this.cdoperacionecc = cdoperacionecc;
    this.miembroDestino = miembroDestino;
    this.cdrefasignacion = cdrefasignacion;
    this.cuentaCompensacion = cuentaCompensacion;
    this.cdestadomov = cdestadomov;
    this.cdrefmovimiento = cdrefmovimiento;
    this.cdrefmovimientoecc = cdrefmovimientoecc;
    this.cdrefnotificacion = cdrefnotificacion;
    this.cdrefnotificacionprev = cdrefnotificacionprev;
    this.cdtipomov = cdtipomov;
    this.idMovimientoFidessa = idMovimientoFidessa;
  }

  public EjecucionAlocationProcesarDesglosesOpEspecialesDTO(Long idejecucionalocation,
                                                            Long idgrupoeje,
                                                            Date fevalor,
                                                            BigDecimal impreciomkt,
                                                            String cdtipoop,
                                                            String cdsettmkt,
                                                            String cdcamara,
                                                            Character asigcomp,
                                                            String tradingvenue,
                                                            String nuagreje,
                                                            Date feejecuc,
                                                            Tmct0aloId tmct0aloId,
                                                            Tmct0ejeId tmct0ejeId,
                                                            String cdbroker,
                                                            BigDecimal nutitulos,
                                                            // DATOS EJECUCION
                                                            String nuordenmkt,
                                                            String nuexemkt,
                                                            String cdmiembromkt,
                                                            Character cdindcotizacion,
                                                            String cdsegmento,
                                                            String cdPlataformaNegociacion,
                                                            Date hoejecuc,
                                                            String tpopebol,

                                                            // DATOS OPERACIONECC
                                                            Date feordenmkt,
                                                            Date hoordenmkt) {
    super();
    this.idejecucionalocation = idejecucionalocation;
    this.idgrupoeje = idgrupoeje;
    this.fevalor = fevalor;
    this.impreciomkt = impreciomkt;
    this.cdtipoop = cdtipoop;
    this.cdsettmkt = cdsettmkt;
    this.cdcamara = cdcamara;
    this.asigcomp = asigcomp;
    this.tradingvenue = tradingvenue;
    this.nuagreje = nuagreje;
    this.feejecuc = feejecuc;
    this.tmct0aloId = tmct0aloId;
    this.tmct0ejeId = tmct0ejeId;
    this.cdbroker = cdbroker;
    this.nutitulos = nutitulos;

    // DATOS EJECUCION
    this.nuordenmkt = nuordenmkt;
    this.nuexemkt = nuexemkt;
    this.cdmiembromkt = cdmiembromkt;
    this.cdindcotizacion = cdindcotizacion;
    this.cdsegmento = cdsegmento;
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
    this.hoejecuc = hoejecuc;
    this.tpopebol = tpopebol;

    // DATOS OPERACIONECC
    this.feordenmkt = feordenmkt;
    this.hoordenmkt = hoordenmkt;

  }

  public EjecucionAlocationProcesarDesglosesOpEspecialesDTO(EjecucionAlocationProcesarDesglosesOpEspecialesDTO other) {

    this(other.getIdejecucionalocation(), other.getIdgrupoeje(), other.getFevalor(), other.getImpreciomkt(),
         other.getCdtipoop(), other.getCdsettmkt(), other.getCdcamara(), other.getAsigcomp(), other.getTradingvenue(),
         other.getNuagreje(), other.getFeejecuc(), other.getTmct0aloId(), other.getTmct0ejeId(), other.getCdbroker(),
         other.getNutitulos(), other.getNuordenmkt(), other.getNuexemkt(), other.getCdmiembromkt(),
         other.getCdindcotizacion(), other.getCdsegmento(), other.getCdPlataformaNegociacion(), other.getHoejecuc(),
         other.getTpopebol(), other.getFeordenmkt(), other.getHoordenmkt(), other.getNuoperacionprev(),
         other.getCdoperacionecc(), other.getMiembroDestino(), other.getCdrefasignacion(),
         other.getCuentaCompensacion(), other.getCdestadomov(), other.getCdrefmovimiento(),
         other.getCdrefmovimientoecc(), other.getCdrefnotificacion(), other.getCdrefnotificacionprev(),
         other.getCdtipomov(), other.getIdMovimientoFidessa());
  }

  public Long getIdejecucionalocation() {
    return idejecucionalocation;
  }

  public Long getIdgrupoeje() {
    return idgrupoeje;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public BigDecimal getImpreciomkt() {
    return impreciomkt;
  }

  public String getCdtipoop() {
    return cdtipoop;
  }

  public String getCdsettmkt() {
    return cdsettmkt;
  }

  public String getCdcamara() {
    return cdcamara;
  }

  public Character getAsigcomp() {
    return asigcomp;
  }

  public String getTradingvenue() {
    return tradingvenue;
  }

  public String getNuagreje() {
    return nuagreje;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public Tmct0aloId getTmct0aloId() {
    return tmct0aloId;
  }

  public Tmct0ejeId getTmct0ejeId() {
    return tmct0ejeId;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public BigDecimal getNutitulos() {
    return nutitulos;
  }

  public void setIdejecucionalocation(Long idejecucionalocation) {
    this.idejecucionalocation = idejecucionalocation;
  }

  public void setIdgrupoeje(Long idgrupoeje) {
    this.idgrupoeje = idgrupoeje;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setImpreciomkt(BigDecimal impreciomkt) {
    this.impreciomkt = impreciomkt;
  }

  public void setCdtipoop(String cdtipoop) {
    this.cdtipoop = cdtipoop;
  }

  public void setCdsettmkt(String cdsettmkt) {
    this.cdsettmkt = cdsettmkt;
  }

  public void setCdcamara(String cdcamara) {
    this.cdcamara = cdcamara;
  }

  public Long getIdCamara() {
    return idCamara;
  }

  public void setIdCamara(Long idCamara) {
    this.idCamara = idCamara;
  }

  public void setAsigcomp(Character asigcomp) {
    this.asigcomp = asigcomp;
  }

  public void setTradingvenue(String tradingvenue) {
    this.tradingvenue = tradingvenue;
  }

  public void setNuagreje(String nuagreje) {
    this.nuagreje = nuagreje;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public void setTmct0aloId(Tmct0aloId tmct0aloId) {
    this.tmct0aloId = tmct0aloId;
  }

  public void setTmct0ejeId(Tmct0ejeId tmct0ejeId) {
    this.tmct0ejeId = tmct0ejeId;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setNutitulos(BigDecimal nutitulos) {
    this.nutitulos = nutitulos;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public Character getCdindcotizacion() {
    return cdindcotizacion;
  }

  public String getCdsegmento() {
    return cdsegmento;
  }

  public String getCdPlataformaNegociacion() {
    return cdPlataformaNegociacion;
  }

  public Date getHoejecuc() {
    return hoejecuc;
  }

  public Date getFeordenmkt() {
    return feordenmkt;
  }

  public Date getHoordenmkt() {
    return hoordenmkt;
  }

  public String getNuoperacionprev() {
    return nuoperacionprev;
  }

  public String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  public void setCdsegmento(String cdsegmento) {
    this.cdsegmento = cdsegmento;
  }

  public void setCdPlataformaNegociacion(String cdPlataformaNegociacion) {
    this.cdPlataformaNegociacion = cdPlataformaNegociacion;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public void setFeordenmkt(Date feordenmkt) {
    this.feordenmkt = feordenmkt;
  }

  public void setHoordenmkt(Date hoordenmkt) {
    this.hoordenmkt = hoordenmkt;
  }

  public void setNuoperacionprev(String nuoperacionprev) {
    this.nuoperacionprev = nuoperacionprev;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public String getMiembroDestino() {
    return miembroDestino;
  }

  public String getCdrefasignacion() {
    return cdrefasignacion;
  }

  public String getCuentaCompensacion() {
    return cuentaCompensacion;
  }

  public String getCdestadomov() {
    return cdestadomov;
  }

  public String getCdrefmovimiento() {
    return cdrefmovimiento;
  }

  public String getCdrefmovimientoecc() {
    return cdrefmovimientoecc;
  }

  public void setCdindcotizacion(Character cdindcotizacion) {
    this.cdindcotizacion = cdindcotizacion;
  }

  public void setMiembroDestino(String miembroDestino) {
    this.miembroDestino = miembroDestino;
  }

  public String getCorretajePtiMiembroDestino() {
    return corretajePtiMiembroDestino;
  }

  public void setCorretajePtiMiembroDestino(String corretajePtiMiembroDestino) {
    this.corretajePtiMiembroDestino = corretajePtiMiembroDestino;
  }

  public void setCdrefasignacion(String cdrefasignacion) {
    this.cdrefasignacion = cdrefasignacion;
  }

  public void setCuentaCompensacion(String cuentaCompensacion) {
    this.cuentaCompensacion = cuentaCompensacion;
  }

  public void setCdestadomov(String cdestadomov) {
    this.cdestadomov = cdestadomov;
  }

  public void setCdrefmovimiento(String cdrefmovimiento) {
    this.cdrefmovimiento = cdrefmovimiento;
  }

  public void setCdrefmovimientoecc(String cdrefmovimientoecc) {
    this.cdrefmovimientoecc = cdrefmovimientoecc;
  }

  public String getCdrefnotificacion() {
    return cdrefnotificacion;
  }

  public String getCdrefnotificacionprev() {
    return cdrefnotificacionprev;
  }

  public void setCdrefnotificacion(String cdrefnotificacion) {
    this.cdrefnotificacion = cdrefnotificacion;
  }

  public void setCdrefnotificacionprev(String cdrefnotificacionprev) {
    this.cdrefnotificacionprev = cdrefnotificacionprev;
  }

  public String getCdtipomov() {
    return cdtipomov;
  }

  public void setCdtipomov(String cdtipomov) {
    this.cdtipomov = cdtipomov;
  }

  public String getTpopebol() {
    return tpopebol;
  }

  public void setTpopebol(String tpopebol) {
    this.tpopebol = tpopebol;
  }

  public Long getIdMovimientoFidessa() {
    return idMovimientoFidessa;
  }

  public void setIdMovimientoFidessa(Long idMovimientoFidessa) {
    this.idMovimientoFidessa = idMovimientoFidessa;
  }

  public Tmct0desgloseclitit getDesgloseRelacionado() {
    return desgloseRelacionado;
  }

  public void setDesgloseRelacionado(Tmct0desgloseclitit desgloseRelacionado) {
    this.desgloseRelacionado = desgloseRelacionado;
  }

  public BigDecimal getCanonContratacionAplicado() {
    return canonContratacionAplicado;
  }

  public void setCanonContratacionAplicado(BigDecimal canonContratacionAplicado) {
    this.canonContratacionAplicado = canonContratacionAplicado;
  }

  public BigDecimal getCanonContratacionTeorico() {
    return canonContratacionTeorico;
  }

  public void setCanonContratacionTeorico(BigDecimal canonContratacionTeorico) {
    this.canonContratacionTeorico = canonContratacionTeorico;
  }

  @Override
  public String toString() {
    return "EjecucionAlocationProcesarDesglosesOpEspecialesDTO [idejecucionalocation=" + idejecucionalocation
           + ", idgrupoeje=" + idgrupoeje + ", fevalor=" + fevalor + ", impreciomkt=" + impreciomkt + ", cdtipoop="
           + cdtipoop + ", cdsettmkt=" + cdsettmkt + ", cdcamara=" + cdcamara + ", asigcomp=" + asigcomp
           + ", tradingvenue=" + tradingvenue + ", nuagreje=" + nuagreje + ", feejecuc=" + feejecuc + ", tmct0aloId="
           + tmct0aloId + ", tmct0ejeId=" + tmct0ejeId + ", cdbroker=" + cdbroker + ", nutitulos=" + nutitulos + "]";
  }

}
