package sibbac.business.wrappers.tasks;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.model.Tmct0Users;

public abstract class WrapperUserTaskConcurrencyPrevent implements WrapperUserTaskConcurrencyPreventInterface {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(WrapperUserTaskConcurrencyPrevent.class);

  @Value("${environment}")
  protected String environment;

  @Value("${sibbac.process.exception.mail.to}")
  protected String mailTo;

  @Value("${sibbac.process.exception.mail.cc}")
  protected String mailCc;

  @Value("${sibbac.process.exception.mail.subject}")
  protected String mailSubject;

  /**
   * Para cambiar el estado de la tarea.
   */
  @Autowired
  protected Tmct0menBo menBo;

  @Autowired
  protected SendMail mail;

  @Autowired
  protected Tmct0UsersDao usersDao;

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public CallableResultDTO executeManualUser(String auditUser) {
    CallableResultDTO res = new CallableResultDTO();
    // Ejecucion de la tarea en si
    LOG.trace(">>> LAUNCHING TASK EXECUTION - {}.", auditUser);
    // Para asegurarse de que se inicializa el tipo de apunte y el
    // nombre de la clase (para las trazas)
    TIPO tipoApunte = determinarTipoApunte();
    // Hay tareas que controlan si se están ejecutando independientemente,
    // por eso se pone if, si tipoApunte no es nulo se controla desde aquí.
    if (tipoApunte != null) {
      Tmct0men tmct0men = null;
      TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
      TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
      TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
      try {
        LOG.debug("execute ==> Detectando si se está ejecutando la tarea ....");
        tmct0men = menBo.putEstadoMEN(tipoApunte, estadoIni, estadoExe, auditUser);
        try {
          res = this.executeTaskManualUser(auditUser);
          menBo.putEstadoMEN(tmct0men, estadoIni, auditUser);
          LOG.debug("execute Se acabó la tarea y se dejó en estado disponible");
        }
        catch (Exception ex) {
          res.addErrorMessage(String.format("Incidencia en ejecución de la tarea. %s", ex.getMessage()));
          this.sendMailWithExceptionMessage(tmct0men, ex, auditUser);
          menBo.putEstadoMEN(tmct0men, estadoError, auditUser);
          LOG.warn("Intento de ejecución de la tarea. {}", ex.getMessage(), ex);

        }
      }
      catch (SIBBACBusinessException ex) {
        LOG.info(ex.getMessage());
        LOG.trace(ex.getMessage(), ex);
        res.addErrorMessage(String.format("Incidencia en ejecución de la tarea. %s", ex.getMessage()));
      }
      catch (Exception ex) {
        LOG.warn("{}  cause: {}", ex.getMessage(), ex.getCause(), ex);
        res.addErrorMessage(String.format("Incidencia en ejecución de la tarea. %s", ex.getMessage()));
      }
    }
    else {
      try {
        res = this.executeTaskManualUser(auditUser);
      }
      catch (Exception e) {
        LOG.error(" Error ejecutando la tarea. {}", e.getMessage(), e);
        res.addErrorMessage(String.format("Incidencia en ejecución de la tarea. %s", e.getMessage()));
        this.sendMailWithExceptionMessage(null, e, auditUser);
      }
    }
    return res;
  }

  private void sendMailWithExceptionMessage(Tmct0men tmct0men, Exception ex, String auditUser) {
    LOG.trace("[sendMailWithExceptionMessage] inicio user: {}.", auditUser);
    String cdmensa;
    String dsmensa;
    List<String> to;
    List<String> cc;

    String mailSubjectReplaced;
    String mailBodyReplaced;

    Tmct0Users user = null;
    try {

      if (auditUser != null && !auditUser.trim().isEmpty()) {
        user = usersDao.getByUsername(auditUser);
      }
      cdmensa = "UNDEFINED";
      dsmensa = this.getClass().getName();

      if (tmct0men != null) {
        cdmensa = StringUtils.trim(tmct0men.getCdmensa());
        dsmensa = StringUtils.trim(tmct0men.getDsmensa());
      }
      to = new ArrayList<String>();
      cc = new ArrayList<String>();

      LOG.trace("[sendMailWithExceptionMessage] montando el to.");
      if (StringUtils.isNotBlank(mailTo)) {
        to.addAll(Arrays.asList(mailTo.replace(",", ";").split(";")));
      }

      LOG.trace("[sendMailWithExceptionMessage] montando el cc.");
      if (StringUtils.isNotBlank(mailCc)) {
        cc.addAll(Arrays.asList(mailCc.replace(",", ";").split(";")));
      }
      if (CollectionUtils.isEmpty(to) && CollectionUtils.isNotEmpty(cc)) {
        LOG.warn("[sendMailWithExceptionMessage] hay cc sin to.");
        to.addAll(cc);
        cc.clear();
      }

      if (user != null && user.getEmail() != null && !user.getEmail().trim().isEmpty()) {
        to.add(user.getEmail());
      }

      if (CollectionUtils.isNotEmpty(to)) {
        LOG.trace("[sendMailWithExceptionMessage] montando subject del email...");
        mailSubjectReplaced = MessageFormat.format(mailSubject, cdmensa, dsmensa, new Date(), environment);
        mailSubject = MessageFormat.format("{0} - User Manual Execution: {1}.", mailSubject, auditUser);
        LOG.trace("[sendMailWithExceptionMessage] montando body del email con la exception...");
        mailBodyReplaced = String.format("%s\n\n%s", mailSubjectReplaced, ex.toString());
        LOG.trace("[sendMailWithExceptionMessage] enviando email a to: {}  cc: {} \nsubject: {} \nbody:\n {}...", to,
            cc, mailSubjectReplaced, mailBodyReplaced);
        mail.sendMail(to, mailSubjectReplaced, mailBodyReplaced, cc);

      }
      else {
        LOG.warn(
            "[sendMailWithExceptionMessage] No se ha configurado destinatario para enviar el email con la excepcion. user: {}",
            auditUser);
      }
    }
    catch (Exception e) {
      LOG.warn("No se ha podido mandar el correo con el error. {} user: {}", e.getMessage(), auditUser, e);
    }
    LOG.trace("[sendMailWithExceptionMessage] fin user: {}.", auditUser);
  }

}
