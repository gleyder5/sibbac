package sibbac.business.wrappers.tasks.thread;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.common.RunnableProcess;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

public abstract class WrapperMultiThreaded<T extends RecordBean> extends WrapperExecutor {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(WrapperMultiThreaded.class);

  public WrapperMultiThreaded() {
    super();
  }

  @SuppressWarnings({ "unchecked", "rawtypes"
  })
  public RunnableProcess<T> getRunnableProces(ObserverProcess observerProcess,
                                              IRunnableBean<T> beanToProcess,
                                              List objectsToProcess,
                                              int firstObjectOrden) {
    return new RunnableProcess<T>(beanToProcess, observerProcess, objectsToProcess, firstObjectOrden);
  }

  public abstract boolean hasNextObjectToProcess() throws Exception;

  public abstract T getNextObjectToProcess(int lineOrder) throws Exception;

  /**
   * 
   * @param bean
   * @return
   */
  protected abstract boolean isInBlock(T previousBean, T bean);

  public abstract IRunnableBean<T> getBeanToProcessBlock();

  public abstract void preExecuteTask() throws SIBBACBusinessException;

  public abstract void postExecuteTask() throws SIBBACBusinessException;

  /**
   * 
   * Metodo creado para ser sobreescrito si es necesario y poder agrupar los beans al antojo del programador y que cada
   * sublista se ejecute en un hilo si el proceso esta configurado para tener hilos
   * 
   * @param dataBeanList
   * @return
   */
  public List<List<T>> crearBloquesTransaccionales(List<T> dataBeanList) {
    List<List<T>> res = new ArrayList<List<T>>();
    if (CollectionUtils.isNotEmpty(dataBeanList)) {
      res.add(dataBeanList);
    }
    return res;
  }

  @Transactional
  public void executeTask() throws Exception {

    // this.printManagementFactoryInfo();

    getObserver().setException(null);
    preExecuteTask();

    LOG.trace("[WrapperMultireaded :: executeTask] Init ");
    try {
      List<T> dataBeanList = new ArrayList<T>();
      T dataBean;
      T previousDataBean = null;

      int countLines = 0;
      int countBlocks = 0;

      IRunnableBean<T> processBean = getBeanToProcessBlock();
      if (processBean == null) {
        LOG.warn("[WrapperMultireaded :: executeTask] INCIDENCIA runabble bean not configured... NULL");
        throw new SIBBACBusinessException("INCIDENCIA runabble bean not configured...");
      }
      processBean.preRun(getObserver());

      while (hasNextObjectToProcess()) {
        countLines++;
        dataBean = getNextObjectToProcess(countLines);
        if (dataBean != null) {
          if (!isInBlock(previousDataBean, dataBean)) {
            countBlocks++;

            if (countBlocks % getTransactionSize() == 0) {
              if (CollectionUtils.isNotEmpty(dataBeanList)) {
                //
                ejecutarBloquesTransaccionales(processBean, dataBeanList, countLines, countBlocks);
                dataBeanList = new ArrayList<T>();
              }
            }
          }
          previousDataBean = dataBean;
          dataBeanList.add(dataBean);
        }
      }// fin while

      if (CollectionUtils.isNotEmpty(dataBeanList)) {
        LOG.trace("[WrapperMultireaded :: executeTask] countLines :: {} ## diferent blocks :: {}", countLines,
                  countBlocks);

        ejecutarBloquesTransaccionales(processBean, dataBeanList, countLines, countBlocks);

        dataBeanList = new ArrayList<T>();
      }

      if (getObserver().getException() != null) {
        throw getObserver().getException();
      }

      processBean.postRun(getObserver());

    } catch (Exception e) {
      closeExecutorNow();
      throw e;
    } finally {

      closeExecutor();
      postExecuteTask();
    }
    LOG.trace("[WrapperMultireaded :: executeTask] End ");
  }

  protected void ejecutarBloquesTransaccionales(IRunnableBean<T> processBean,
                                                List<T> dataBeanList,
                                                int countLines,
                                                int countBlocks) throws Exception {
    ejecutarBloquesTransaccionales(getObserver(), processBean, dataBeanList, countLines, countBlocks);

  }

  protected void ejecutarBloquesTransaccionales(ObserverProcess observer,
                                                IRunnableBean<T> processBean,
                                                List<T> dataBeanList,
                                                int countLines,
                                                int countBlocks) throws Exception {
    LOG.trace("[WrapperMultireaded :: ejecutarBloquesTransaccionales] countLines :: {} ## diferent blocks :: {}",
              countLines, countBlocks);
    RunnableProcess<T> process;
    List<List<T>> bloques = crearBloquesTransaccionales(dataBeanList);
    for (List<T> subList : bloques) {
      process = (RunnableProcess<T>) getRunnableProces(observer, processBean, subList, countLines);
      LOG.trace("[WrapperMultireaded :: ejecutarBloquesTransaccionales] process :: {} ", process);
      if (getThreadSize() <= 1) {
        process.run();
      } else {
        executeProcessInThread(observer, process);
      }
      if (observer.getException() != null) {
        throw observer.getException();
      }
    }

  }

  /**
   * 
   * @param fieldLenghts
   * @return
   */
  protected Range[] convertLengthsIntoRanges(final int[] fieldLenghts) {
    Range[] ranges = new Range[fieldLenghts.length];
    int distAux = 1;
    for (int i = 0; i < fieldLenghts.length; i++) {
      if (fieldLenghts[i] > 0) {
        Range auxRange = new Range(distAux, distAux + fieldLenghts[i] - 1);
        distAux += fieldLenghts[i];
        ranges[i] = auxRange;
      }
    }
    return ranges;
  } // convertLengthsIntoRanges

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
  }

}
