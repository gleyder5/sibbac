package sibbac.business.wrappers.database.bo;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0referenciatitularDao;
import sibbac.business.wrappers.database.dao.Tmct0referenciatitularDaoImp;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.business.wrappers.database.model.Tmct0referenciatitularFis;
import sibbac.database.bo.AbstractBo;

@Component
public class Tmct0referenciaTitularBo extends AbstractBo<Tmct0referenciatitular, Long, Tmct0referenciatitularDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0referenciaTitularBo.class);

  @Autowired
  private Tmct0referenciatitularDaoImp referenciaTitularDaoImp;

  public Tmct0referenciaTitularBo() {
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public Tmct0referenciatitular crearNuevaRefernciaTitularNewTransaction(String referenciaAdicional, String audituser) {
    Tmct0referenciatitular refTit = new Tmct0referenciatitular();

    String referenciaTitular = this.getNewReferenciaTitular();
    refTit.setCdreftitularpti(referenciaTitular);
    refTit.setReferenciaAdicional(referenciaAdicional);
    refTit.setAuditFechaCambio(new Date());
    refTit.setAuditUser(audituser);
    save(refTit);
    return refTit;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public synchronized Tmct0referenciatitular saveSyncronizedNewTransaction(String referenciaTitular,
      String referenciaAdicional, String audituser) {

    Tmct0referenciatitular refTit = dao.findByCdreftitularptiAndReferenciaAdicional(referenciaTitular,
        referenciaAdicional);
    if (refTit == null) {
      refTit = new Tmct0referenciatitular();
      refTit.setCdreftitularpti(referenciaTitular);
      refTit.setReferenciaAdicional(referenciaAdicional);
      refTit.setAuditFechaCambio(new Date());
      refTit.setAuditUser(audituser);

      save(refTit);
    }

    return refTit;

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public Tmct0referenciatitular crearNuevaReferenciaTitularNewTransaction(String referenciaAdicional,
      List<String> listIdentificaciones, String audituser) {
    Tmct0referenciatitularFis fis;
    Tmct0referenciatitular refTit = new Tmct0referenciatitular();

    String referenciaTitular = this.getNewReferenciaTitular();
    refTit.setCdreftitularpti(referenciaTitular);
    refTit.setReferenciaAdicional(referenciaAdicional);
    refTit.setAuditFechaCambio(new Date());
    refTit.setAuditUser(audituser);

    for (String cdholder : listIdentificaciones) {
      fis = new Tmct0referenciatitularFis();
      fis.setReferenciaTitular(refTit);
      fis.setCdholder(cdholder);
      fis.setAuditFechaCambio(new Date());
      fis.setAuditUser(audituser);
      refTit.getTmct0referenciatitularfis().add(fis);
    }

    save(refTit);

    return refTit;

  }

  public String getNewReferenciaTitular() {
    Long id = dao.getNextValForReferenciaTitular();
    String refTitular = StringUtils.leftPad(id.toString(), 20, '0');
    return refTitular;
  }

  public Tmct0referenciatitular findByCdreftitularptiAndReferenciaAdicional(String cdreftitularpti,
      String referenciaAdicional) {
    return dao.findByCdreftitularptiAndReferenciaAdicional(cdreftitularpti, referenciaAdicional);
  }

  public String findReferenciaTitularFromReferenciasTitularFis(String referenciaAdicional,
      List<String> indentificaciones) {
    return referenciaTitularDaoImp.findReferenciaTitularFromReferenciasTitularFis(referenciaAdicional,
        indentificaciones);
  }

}
