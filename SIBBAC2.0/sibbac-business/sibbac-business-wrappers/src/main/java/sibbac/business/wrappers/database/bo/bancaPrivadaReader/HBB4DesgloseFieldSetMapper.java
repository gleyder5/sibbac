package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.common.utils.FormatDataUtils;

public class HBB4DesgloseFieldSetMapper implements FieldSetMapper<HBB4DesgloseRecordBean> {

  public final static String delimiter = ";";

  public enum HBB4DesgloseFields {
    REFEXTERNA("refExterna", 16),
    FECHACUADRE("fechaCuadre", 8),
    TIPOREGISTRO("tipoRegistro", 1),
    ENTIDAD_LIQUIDADORA("entidadLiquidadora", 4),
    FILLER_POST_ENTIDAD_LIQUIDADORA1("fillerPostEntidadLiquidadora1", 1),
    FILLER_POST_ENTIDAD_LIQUIDADORA2("fillerPostEntidadLiquidadora2", 1),
    FILLER_POST_ENTIDAD_LIQUIDADORA3("fillerPostEntidadLiquidadora3", 1),
    ISIN("isin", 12),
    TITULOS("titulos", 7),
    PRECIO("precio", 11),
    FILLER_POST_PRECIO("fillerPostPrecio", 93),
    NUORDEN("nuorden", 20);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private HBB4DesgloseFields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      HBB4DesgloseFields[] fields = HBB4DesgloseFields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getHBB4Lengths() {
      HBB4DesgloseFields[] fields = HBB4DesgloseFields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public HBB4DesgloseRecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    HBB4DesgloseRecordBean hbb4bean = new HBB4DesgloseRecordBean();
    hbb4bean.setRefExterna(fieldSet.readString(HBB4DesgloseFields.REFEXTERNA.text).trim());
    hbb4bean.setFechaCuadre(fieldSet.readDate(HBB4DesgloseFields.FECHACUADRE.text, FormatDataUtils.DATE_FORMAT));
    hbb4bean.setTipoRegistroHBB(fieldSet.readBigDecimal(HBB4DesgloseFields.TIPOREGISTRO.text));
    hbb4bean.setEntidadLiquidadora(fieldSet.readString(HBB4DesgloseFields.ENTIDAD_LIQUIDADORA.text));
    String filler1 = fieldSet.readString(HBB4DesgloseFields.FILLER_POST_ENTIDAD_LIQUIDADORA1.text);
    String filler2 = fieldSet.readString(HBB4DesgloseFields.FILLER_POST_ENTIDAD_LIQUIDADORA2.text);
    String filler3 = fieldSet.readString(HBB4DesgloseFields.FILLER_POST_ENTIDAD_LIQUIDADORA3.text);
    if (StringUtils.isBlank(filler1)) {
      hbb4bean.setFillerPostEntidadLiquidadora1(' ');
    } else {
      hbb4bean.setFillerPostEntidadLiquidadora1(filler1.charAt(0));
    }
    if (StringUtils.isBlank(filler2)) {
      hbb4bean.setFillerPostEntidadLiquidadora2(' ');
    } else {
      hbb4bean.setFillerPostEntidadLiquidadora2(filler2.charAt(0));
    }
    if (StringUtils.isBlank(filler3)) {
      hbb4bean.setFillerPostEntidadLiquidadora3(' ');
    } else {
      hbb4bean.setFillerPostEntidadLiquidadora3(filler3.charAt(0));
    }

    hbb4bean.setIsin(fieldSet.readString(HBB4DesgloseFields.ISIN.text));
    hbb4bean.setTitulos(fieldSet.readBigDecimal(HBB4DesgloseFields.TITULOS.text));

    hbb4bean.setPrecio(fieldSet.readBigDecimal(HBB4DesgloseFields.PRECIO.text));
    hbb4bean.setPrecio(hbb4bean.getPrecio().movePointLeft(4));
    hbb4bean.setFillerPostPrecio(fieldSet.readString(HBB4DesgloseFields.FILLER_POST_PRECIO.text));
    hbb4bean.setNuorden(fieldSet.readString(HBB4DesgloseFields.NUORDEN.text));
    return hbb4bean;
  }

}
