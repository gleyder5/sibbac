package sibbac.business.wrappers.database.dao;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0dbs;

@Repository
public class Tmct0dbsDaoImp {

  @PersistenceContext
  private EntityManager em;

  public Tmct0dbs persist(Tmct0dbs entity) {
    em.persist(entity);
    em.flush();
    return entity;
  }

  /**
   * Persiste datos estaticos del registro de actividad, inserta y hace flush.
   * @param entity entity
   * @return entity entity
   */
  @Transactional
  public Tmct0dbs populateAndPersistAndFlush(Tmct0dbs tmct0dbs) {
    SimpleDateFormat sdf = new SimpleDateFormat("HHmmssSSS");
    tmct0dbs.setCdestado(new Character('N'));
    tmct0dbs.setCdusuaud("SIBBAC 2.0");
    tmct0dbs.setFecha(new Date());
    tmct0dbs.setFhaudit(new Date());
    tmct0dbs.setNsec1(1);
    tmct0dbs.setNsec2(1);
    tmct0dbs.setNuoprout(new Integer(sdf.format(new Date().getTime())));
    tmct0dbs.setNupareje((short) 1);
    em.persist(tmct0dbs);
    em.flush();
    return tmct0dbs;
  }
}
