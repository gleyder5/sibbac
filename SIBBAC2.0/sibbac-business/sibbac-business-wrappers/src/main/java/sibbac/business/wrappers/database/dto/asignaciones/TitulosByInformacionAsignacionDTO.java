package sibbac.business.wrappers.database.dto.asignaciones;

import java.io.Serializable;
import java.math.BigDecimal;

public class TitulosByInformacionAsignacionDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3858192267130138246L;
  private BigDecimal titulos;
  private String cuentaCompensacion, miembroCompensadorExterno, referenciaAsignacionExterna;

  public TitulosByInformacionAsignacionDTO() {
  }

  public TitulosByInformacionAsignacionDTO(BigDecimal titulos, String cuentaCompensacion,
      String miembroCompensadorExterno, String referenciaAsignacionExterna) {
    super();
    this.titulos = titulos;
    this.cuentaCompensacion = cuentaCompensacion;
    this.miembroCompensadorExterno = miembroCompensadorExterno;
    this.referenciaAsignacionExterna = referenciaAsignacionExterna;
  }

  public BigDecimal getTitulos() {
    return titulos;
  }

  public String getCuentaCompensacion() {
    return cuentaCompensacion;
  }

  public String getMiembroCompensadorExterno() {
    return miembroCompensadorExterno;
  }

  public String getReferenciaAsignacionExterna() {
    return referenciaAsignacionExterna;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setCuentaCompensacion(String cuentaCompensacion) {
    this.cuentaCompensacion = cuentaCompensacion;
  }

  public void setMiembroCompensadorExterno(String miembroCompensadorExterno) {
    this.miembroCompensadorExterno = miembroCompensadorExterno;
  }

  public void setReferenciaAsignacionExterna(String referenciaAsignacionExterna) {
    this.referenciaAsignacionExterna = referenciaAsignacionExterna;
  }

  @Override
  public String toString() {
    return "TitulosByInformacionAsignacionDTO [titulos=" + titulos + ", cuentaCompensacion=" + cuentaCompensacion
        + ", miembroCompensadorExterno=" + miembroCompensadorExterno + ", referenciaAsignacionExterna="
        + referenciaAsignacionExterna + "]";
  }

}
