package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.MonedasDao;
import sibbac.business.wrappers.database.model.Monedas;
import sibbac.database.bo.AbstractBo;


@Service
public class MonedasBo extends AbstractBo< Monedas, String, MonedasDao > {

	public Monedas findByCodigo( String codigo ) {
		return dao.findByCodigo( codigo );
	}
}
