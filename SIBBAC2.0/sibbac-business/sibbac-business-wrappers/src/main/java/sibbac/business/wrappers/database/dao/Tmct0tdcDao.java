package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0tdc;
import sibbac.business.wrappers.database.model.Tmct0tdcId;

@Deprecated
@Repository
public interface Tmct0tdcDao extends JpaRepository<Tmct0tdc, Tmct0tdcId>, JpaSpecificationExecutor<Tmct0tdc> {

  @Query("SELECT t FROM Tmct0tdc t WHERE t.id.nuversion = :pnuversion "
         + " and t.id.fhinival <= :fecha AND t.id.fhfinval > :fecha "
         + " ORDER BY t.id.cddbolsa ASC, t.id.immaxefe ASC ")
  List<Tmct0tdc> findAllByNuversionAndFecha(@Param("pnuversion") int nuversion, @Param("fecha") Date fecha);

}
