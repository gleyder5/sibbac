package sibbac.business.wrappers.database.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0aliId;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.AliasDao;
import sibbac.business.wrappers.database.dao.Tmct0cliDao;
import sibbac.business.wrappers.database.data.AliasFacturableData;
import sibbac.business.wrappers.database.dto.AliasDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasSubcuenta;
import sibbac.business.wrappers.database.model.Idioma;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class AliasBo extends AbstractBo<Alias, Long, AliasDao> {

    private static final String ELIMINAR_REPETIDOS = "eliminarRepetidos";

    private static final String CODIGO_IDIOMA = "ES";

    @Autowired
    private Tmct0aliBo tmct0aliBo;

    @Autowired
    private IdiomaBo idiomaBo;

    @Autowired
    private AliasSubcuentaBo aliasSubucuentaBo;

    @Autowired
    private AliasDao aliasdao;
  
    private static final String TAG = AliasBo.class.getName();

    public Alias findByCdaliass(String cdaliass) {
  return dao.findByCdaliass(cdaliass);
    }
    public Alias findByCdaliass(Map<String,String> params) {
      return dao.findByCdaliass(params.get(Constantes.PARAM_ALIAS_ID));
    }

    /**
     * @param
     * @return
     */
    private Predicate<Alias> encontrarAlias(final Tmct0ali tmct0ali) {
  final Predicate<Alias> predicate = new Predicate<Alias>() {

      @Override
      public boolean evaluate(final Alias alias) {
    boolean encontrado = false;
    if (alias.getTmct0ali().equals(tmct0ali)) {
        encontrado = true;
    }
    return encontrado;
      }
  };
  return predicate;
    }

    /**
     * @param aliasList
     * @param tmct0aliActivo
     * @return
     */
    private Alias encontrarAlias(final List<Alias> aliasList,
      Tmct0ali tmct0aliActivo) {
  final Predicate<Alias> predicate = encontrarAlias(tmct0aliActivo);
  Alias alias = (Alias) CollectionUtils.find(aliasList, predicate);
  return alias;
    }

    /**
     *
     * Crea o actuliza los tmct0ali activos en la tabla Alias de wrapers.
     *
     * @return
     */
    @Transactional
    public List<Alias> sincronizarAlias() {
  final String prefix = "[AliasBo::sincronizarAlias] ";
  LOG.trace(prefix
    + "[Iniciando el proceso de sincronización de alias...]");

  final List<Tmct0ali> tmct0alis = tmct0aliBo.findAlisActivos();
  final List<Tmct0aliId> tmct0aliIds = tmct0aliBo.extractIds(tmct0alis);
  final List<Alias> alis = findAll();
  eliminarRepetidos(tmct0alis, tmct0aliIds, alis);
  final List<Alias> aliasTratados = new ArrayList<Alias>();
  if (CollectionUtils.isNotEmpty(tmct0alis)) {
      final Idioma idioma = idiomaBo.findByCodigo(CODIGO_IDIOMA);
      for (final Tmct0ali tmct0ali : tmct0alis) {
    Alias alias = encontrarAlias(alis, tmct0ali);
    if (alias == null) {
        alias = new Alias(tmct0ali, idioma);
        alias = dao.save(alias);
        alias.setAliasFacturacion(alias);
        aliasTratados.add(alias);
    } else {
        alias.setTmct0ali(tmct0ali);
        alias = dao.save(alias);
        aliasTratados.add(alias);
    }
      }
  }
  LOG.trace(prefix
    + "[Finalizando el proceso de sincronización de alias...]");
  return aliasTratados;
    }

    /**
     * @param tmct0alis
     * @param tmct0aliIds
     * @param alis
     */
    private void eliminarRepetidos(final List<Tmct0ali> tmct0alis,
      final List<Tmct0aliId> tmct0aliIds, final List<Alias> alis) {
  int tmct0aliNulos = 0;
  if (CollectionUtils.isNotEmpty(alis)) {
      LOG.debug(
        "{}::{} Entrando en el método: eliminarRepetidos (Alias) alias: {}",
        TAG, ELIMINAR_REPETIDOS, alis.size());
      for (final Iterator<Alias> alisIterator = alis.iterator(); alisIterator
        .hasNext();) {
    final Alias ali = alisIterator.next();
    final Tmct0ali tmct0ali = ali.getTmct0ali();
    if (tmct0ali == null) {
        LOG.debug(
          "{}::{} El objeto (tmct0ali) {} extraido del objeto (Alias) (cdaliass: {} - cdbrocli: {}), es nulo. ",
          TAG, ELIMINAR_REPETIDOS, ali.getCdaliass(),
          ali.getCdbrocli());
        tmct0aliNulos++;
        continue;
    }
    final Tmct0aliId tmct0aliId = tmct0ali.getId();
    if (tmct0aliIds.contains(tmct0aliId)) {
        alisIterator.remove();
        tmct0aliIds.remove(tmct0aliId);
        tmct0alis.remove(tmct0ali);
    }
      }
  }
  LOG.debug(
    "{}::{} Saliendo del método: eliminarRepetidos con {} objetos tmct0ali nulos.",
    TAG, ELIMINAR_REPETIDOS, tmct0aliNulos);
    }

    /**
     * Para cada alias de facturación que tienen los alias, recupera una lista
     * de alias asociadoss. Devuelve un map con una entrada para cada cdaliass y
     * su lista de alias asociados
     * 
     * @return
     */
    @Transactional
    public List<AliasFacturableData> recuperarAliasFactruablesAlias(
      final Set<String> cdaliasAProcesar) {
  final List<AliasFacturableData> aliasFacturacion = dao
    .findDistinctAliasFacturacion(cdaliasAProcesar);
  return aliasFacturacion;
    }

  // Devuelve una lista de los alias
  @Transactional
  public List<AliasDTO> getListaAlias() {
    List<Object[]> temporalList = dao.findAllAliasNative();
    List<AliasDTO> returnList = convertList(temporalList);
    return returnList;
  }

  private List<AliasDTO> convertList(List<Object[]> clientesAliasActivos) {
    List<AliasDTO> aliases = new ArrayList();

    if ((clientesAliasActivos != null) && (!clientesAliasActivos.isEmpty())) {

      for (Object[] operacion : clientesAliasActivos) {

        BigInteger id = ((BigInteger) operacion[0]);
        BigInteger aliasFacturacionId = ((BigInteger) operacion[1]);
        BigInteger idiomaId = ((BigInteger) operacion[2]);
        String nombre = ((String) operacion[3]);
        String nombreBroCli = ((String) operacion[4]);

        Alias aliasFacturacion = aliasdao.findOne(new Long(aliasFacturacionId.longValue()));

        AliasDTO aliasDTO = new AliasDTO(new Long(id.longValue()), aliasFacturacion, getIdioma(new Long(idiomaId.longValue())), nombre, nombreBroCli);

        aliases.add(aliasDTO);

      }
    }
    return aliases;
  }

  private Idioma getIdioma(Long idiomaId) {

    Idioma ingles = new Idioma();
    ingles.setCodigo("EN");
    ingles.setDescripcion("INGLES");
    Idioma espanol = new Idioma();
    espanol.setCodigo("ES");
    espanol.setDescripcion("ESPAÑOL");
    Idioma aleman = new Idioma();
    aleman.setCodigo("DE");
    aleman.setDescripcion("ALEMAN");

    if (idiomaId == 1) {
      return espanol;
    }
    if (idiomaId == 2) {
      return ingles;
    }
    else {// 3
      return aleman;
    }

  }

    // Devuelve una lista de los alias sin facturacion
    @Transactional
    public List<AliasDTO> getListaAliasSinFacturacion() {
  return dao.findAllAliasSinFacturacion();
    }

  // Devuelve una lista de los alias
  @Transactional
  public List<AliasDTO> getListaAliasFacturacion() {
    return dao.findAllAliasFacturacion();
  }

    @Transactional
    public List<AliasDTO> getAliasFacturacionAlias(final Long idAlias)
      throws SIBBACBusinessException {
  final Alias alias = dao.findOne(idAlias);
  return aliasListToDTO(dao.findAllBytmct0ali_Id_Cdbrocli(alias
    .getCdbrocli()));
    }

    @Transactional
    public List<AliasDTO> getAliasFacturacionSubcuenta(final Long idSubcuenta)
      throws SIBBACBusinessException {
  final AliasSubcuenta aliasSubcuenta = aliasSubucuentaBo
    .findById(idSubcuenta);
  return aliasListToDTO(dao.findAllBytmct0ali_Id_Cdbrocli(aliasSubcuenta
    .getCdbrocli()));
    }

    /**
     * @param aliasList
     * @return
     */
    private List<AliasDTO> aliasListToDTO(final List<Alias> aliasList) {
  final List<AliasDTO> aliasDtoList = new ArrayList<AliasDTO>();
  if (CollectionUtils.isNotEmpty(aliasList)) {
      for (Alias alias : aliasList) {
    aliasDtoList.add(new AliasDTO(alias.getId(), alias
      .getAliasFacturacion(), alias.getIdioma(), alias
      .getCdaliass(), alias.getDescrali()));
      }

  }
  return aliasDtoList;
    }

    @Transactional
    public Alias updateAlias(final Long idAlias, final Long idAliasFacturacion,
      final Long idIdioma) throws SIBBACBusinessException {
  Alias alias = dao.findOne(idAlias);
  if (alias == null) {
      throw new SIBBACBusinessException(
        "No se ha recuperado informacion para el alias [" + idAlias
          + "]");
  }
  final Alias aliasFacturacion = dao.findOne(idAliasFacturacion);
  if (aliasFacturacion == null) {
      throw new SIBBACBusinessException(
        "No se ha recuperado informacion para el alias de facturacion ["
          + idAliasFacturacion + "]");
  }
  final Idioma idioma = idiomaBo.findById(idIdioma);
  if (idioma == null) {
      throw new SIBBACBusinessException(
        "No se ha recuperado informacion para el idioma ["
          + idIdioma + "]");
  }
  alias.setIdioma(idioma);
  alias.setAliasFacturacion(aliasFacturacion);
  alias = save(alias);
  return alias;
    }

    public List<AliasDTO> findAliasCliente(String[] cliente) {
  return aliasdao.findAliasCliente(cliente);
    }
}
