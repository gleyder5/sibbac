package sibbac.business.wrappers.database.dao;


import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Cobros;
import sibbac.business.wrappers.database.model.LineaDeCobro;


@Component
public interface LineaDeCobroDao extends JpaRepository< LineaDeCobro, Long > {

	@Query( "Select lin " + "from LineaDeCobro lin join lin.cobro cob, " + "LineaDeCobro ldc join ldc.factura fac "
			+ "where cob.fecha = :fechaAyer " + "and lin.id = ldc.id" )
	public List< LineaDeCobro > findCobrosYesterday( @Param( "fechaAyer" ) Date fechaAyer );

	// @Query( "SELECT l "
	// + " FROM Factura f, LineaDeCobro l "
	// + " WHERE l.factura = f.id "
	// + " AND l.cobro = :idCobros ")
	// public List<LineaDeCobro> findFacturaLineaDeCobrosByIdCobros(@Param( "idCobros" ) Long idCobros);

	public List< LineaDeCobro > findAllByCobro( Cobros cobro );

	/*
	 * 
	 * select f.ID from bsnbpsql.TMCT0_COBRO_LINEA cl, bsnbpsql.TMCT0_FACTURA f
	 * where f.ID = cl.ID_FACTURA and cl.ID_COBRO = 4
	 * 
	 * public FacturaLineaDeCobrosData(Long idLineaDeCobro, Long idCobros,
	 * BigDecimal importe, BigDecimal importeAplicado, Long idFactura,
	 * Long numeroFactura, Date fechaFactura) {
	 */
}
