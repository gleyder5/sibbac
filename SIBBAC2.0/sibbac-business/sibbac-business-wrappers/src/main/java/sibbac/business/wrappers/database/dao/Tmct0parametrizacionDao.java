package sibbac.business.wrappers.database.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0parametrizacion;


@Repository
public interface Tmct0parametrizacionDao extends JpaRepository< Tmct0parametrizacion, Long >{

	
	@Query( "select param from Tmct0parametrizacion param where param.tmct0Regla.idRegla = :idRegla and param.activo = '1'" )
	public List<Tmct0parametrizacion> findParametrizacionesByIdRegla(@Param( "idRegla" )BigInteger idRegla);
	
//	
//	@Query( "select param from Tmct0parametrizacion findParametrizacionesByIdParametrizacion param where param.idParametrizacion =:idParametrizacion" )
//	public Tmct0parametrizacion findParametrizacionesByIdParametrizacion(@Param("idParametrizacion")Long idParametrizacion);
	
	
	@Query( "select param from Tmct0parametrizacion param where param.idParametrizacion =:idParametrizacion" )
	public Tmct0parametrizacion findParametrizacionesByIdParametrizacion(@Param("idParametrizacion")Long idParametrizacion);
	
	
//	@Query( "select param from Tmct0parametrizacion param where param.idParametrizacion =:idParametrizacion" )
//	public Tmct0parametrizacion findParametrizacionesByIdParametrizacion1(@Param("idParametrizacion")Long idParametrizacion);
//	
//	@Query( "select param from Tmct0parametrizacion param where param.tmct0Regla.idRegla = :idRegla AND param.tmct0parametrizacion.activo =: 1 ")
//	public List<Tmct0parametrizacion> findParametrizacionesByIdReglaActivas(@Param( "idRegla" )Long idRegla, @Param("activo") String activo);

//	@Query( "select param from Tmct0parametrizacion param where param.tmct0Regla.idRegla = :idRegla AND param.Tmct0parametrizacion.activo =: '1' ")
//	public List<Tmct0parametrizacion> findParametrizacionesByIdReglaActivas(@Param( "idRegla" )Long idRegla);
	
//	@Query( "select param from Tmct0parametrizacion param where param.tmct0Regla.idRegla = :idRegla or " )
//	public List<Tmct0parametrizacion> findParametrizacionesList(@Param( "idRegla" )Long idRegla);
	
	@Query( "select param from Tmct0parametrizacion param join param.tmct0Regla regla where regla.idRegla in ( :idReglas ) and param.activo = '1' order by regla.activo DESC, regla.idRegla" )
	public List<Tmct0parametrizacion> findParametrizacionesByManyReglas(@Param("idReglas")List<BigInteger> idReglas);
	

}
