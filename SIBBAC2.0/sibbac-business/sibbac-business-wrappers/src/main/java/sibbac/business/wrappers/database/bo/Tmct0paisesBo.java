package sibbac.business.wrappers.database.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0paisesId;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0paisesBo extends AbstractBo<Tmct0paises, Tmct0paisesId, Tmct0paisesDao> {

  /**
   * Devuelve la lista de códigos de hacienda para los países activos.
   * 
   * @return <code>List<String> - </code>Lista.
   */
  public List<String> findAllCdhaciendaActivos() {
    List<String> resultados = dao.findAllCdhaciendaActivos();
    // for (String resultado : resultados) {
    // resultado = resultado.trim();
    // } // for
    return resultados;
  } // findAllCdhaciendaActivos

  /**
   * Devuelve la lista de códigos ISO para los países activos.
   * 
   * @return <code>List<String> - </code>Lista.
   */
  public List<String> findAllCdisonumActivos() {
    List<String> resultados = dao.findAllCdisonumActivos();
    return resultados;
  } // findAllCdisonumActivos

  public List<Tmct0paises> findAllActivos() {
    return dao.findAllActivos();
  }

  public Map<String, Tmct0paises> findMapAllActivosByIsonnum() {
    Map<String, Tmct0paises> map = new HashMap<String, Tmct0paises>();
    List<Tmct0paises> paises = findAllActivos();
    for (Tmct0paises tmct0paises : paises) {
      map.put(StringUtils.trim(tmct0paises.getId().getCdisonum()), tmct0paises);
    }
    return map;
  }

  public Map<String, Tmct0paises> findMapAllActivosByCdhacienda() {
    Map<String, Tmct0paises> map = new HashMap<String, Tmct0paises>();
    List<Tmct0paises> paises = findAllActivos();
    for (Tmct0paises tmct0paises : paises) {
      map.put(StringUtils.trim(tmct0paises.getCdhacienda()), tmct0paises);
    }
    return map;
  }

} // Tmct0paisesBo
