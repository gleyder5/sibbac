package sibbac.business.wrappers.rest.filter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.database.filter.TitularesFilter;
import sibbac.webapp.AbstractWebFilter;

public class TitularesFilterImpl extends AbstractWebFilter implements TitularesFilter {

  private static final String FILTER_TIPO_CONSULTA = "tipoConsulta";

  private static final String FILTER_FECHA_DESDE = "fechaDesde";

  private static final String FILTER_FECHA_HASTA = "fechaHasta";

  private static final String FILTER_ALIAS = "alias";

  private static final String FILTER_ISIN = "isin";

  private static final String FILTER_NUDNICIF = "nudnicif";

  private static final String FILTER_MERCADO_NAC_INT = "mercadoNacInt";

  private static final String FILTER_SENTIDO = "sentido";

  private static final String FILTER_REF_CLIENTE = "refCliente";

  private static final String FILTER_NBCLIENT = "nbclient";

  private static final String FILTER_NBCLIENT1 = "nbclient1";

  private static final String FILTER_NBCLIENT2 = "nbclient2";

  private static final String FILTER_ERROR_CODE = "errorCode";

  private static final String FILTER_NBOOKING = "nbooking";

  private TipoConsultaEnum tipoConsulta;

  private Date fechaDesde;

  private Date fechaHasta;

  private char mercadoNacInt;

  private Character sentido;

  private List<String> nudnicif;

  private String errorCode;

  private List<String> nbclient;

  private List<String> nbclient1;

  private List<String> nbclient2;

  private List<String> isin;

  private List<String> alias;

  private String refCliente;

  private String nbooking;

  private boolean aliasEq;

  private boolean nbclientEq;

  private boolean nbclient1Eq;

  private boolean nbclient2Eq;

  private boolean nudnicifEq;

  private boolean isinEq;

  private boolean refClienteEq;

  private boolean nbookingEq;

  private boolean errorCodeEq;
  
  private boolean exportation;


  /**
   * Se define durante el parseo de parámetros. Si la búsqueda es por igualdad
   * de número de booking o por igualdad de referencia de cliente, la lógica de
   * la búsqueda se simplifica y no se requiere restringir ni parcelar la
   * consulta por fechas.
   */
  private boolean byPass;

  public TipoConsultaEnum getTipoConsulta() {
    return tipoConsulta;
  }

  public Date getFechaDesde() {
    return fechaDesde;
  }

  public Date getFechaHasta() {
    return fechaHasta;
  }

  public char getMercadoNacInt() {
    return mercadoNacInt;
  }

  public Character getSentido() {
    return sentido;
  }

  public List<String> getNudnicif() {
    return nudnicif;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public List<String> getNbclient() {
    return nbclient;
  }

  public List<String> getNbclient1() {
    return nbclient1;
  }

  public List<String> getNbclient2() {
    return nbclient2;
  }

  public List<String> getIsin() {
    return isin;
  }

  public List<String> getAlias() {
    return alias;
  }

  public String getRefCliente() {
    return refCliente;
  }

  public String getNbooking() {
    return nbooking;
  }

  public boolean isAliasEq() {
    return aliasEq;
  }

  public boolean isNbclientEq() {
    return nbclientEq;
  }

  public boolean isNbclient1Eq() {
    return nbclient1Eq;
  }

  public boolean isNbclient2Eq() {
    return nbclient2Eq;
  }

  public boolean isNudnicifEq() {
    return nudnicifEq;
  }

  public boolean isIsinEq() {
    return isinEq;
  }

  public boolean isRefClienteEq() {
    return refClienteEq;
  }

  public boolean isNbookingEq() {
    return nbookingEq;
  }

  public boolean isErrorCodeEq() {
    return errorCodeEq;
  }
  
  public boolean isByPass() {
    return byPass;
  }
  
  public boolean isExportation() {
    return exportation;
  }
  
  public void setExportation(boolean exportation) {
    this.exportation = exportation;
  }

  @Override
  protected void parseParameters() throws ParseException {
    final int valueTipoConsulta;

    valueTipoConsulta = getMandatoryFirstInteger(FILTER_TIPO_CONSULTA);
    refCliente = getFirstString(FILTER_REF_CLIENTE);
    refClienteEq = getEquality(FILTER_REF_CLIENTE);
    nbooking = getFirstString(FILTER_NBOOKING);
    nbookingEq = getEquality(FILTER_NBOOKING);
    byPass = (nbooking != null && nbookingEq) || (refCliente != null && refClienteEq);
    tipoConsulta = TipoConsultaEnum.values()[valueTipoConsulta];
    if (byPass) {
      fechaDesde = getFirstDate(FILTER_FECHA_DESDE);
      fechaHasta = getFirstDate(FILTER_FECHA_HASTA);
    }
    else {
      switch (tipoConsulta) {
        case BUSQUEDA_TODOS_LOS_TITULARES:
        case BUSQUEDA_TITULARES_SIN_ERROR:
          fechaDesde = getMandatoryFirstDate(FILTER_FECHA_DESDE);
          fechaHasta = getFirstDate(FILTER_FECHA_HASTA);
          break;
        default:
          fechaDesde = getFirstDate(FILTER_FECHA_DESDE);
          fechaHasta = getFirstDate(FILTER_FECHA_HASTA);
      }
    }
    alias = getList(FILTER_ALIAS);
    aliasEq = getEquality(FILTER_ALIAS);
    mercadoNacInt = getFirstChar(FILTER_MERCADO_NAC_INT);
    sentido = getFirstChar(FILTER_SENTIDO);
    if (tipoConsulta != TipoConsultaEnum.BUSQUEDA_DESGLOSES_SIN_TITULAR) {
      nbclient = getList(FILTER_NBCLIENT);
      nbclientEq = getEquality(FILTER_NBCLIENT);
      nbclient1 = getList(FILTER_NBCLIENT1);
      nbclient1Eq = getEquality(FILTER_NBCLIENT1);
      nbclient2 = getList(FILTER_NBCLIENT2);
      nbclient2Eq = getEquality(FILTER_NBCLIENT2);
      nudnicif = getList(FILTER_NUDNICIF);
      nudnicifEq = getEquality(FILTER_NUDNICIF);
    }
    isin = getList(FILTER_ISIN);
    isinEq = getEquality(FILTER_ISIN);
    switch (tipoConsulta) {
      case BUSQUEDA_TITULARES_CON_ERROR:
      case BUSQUEDA_TITULARES_ROUTING_SIN_ORDEN_SV:
        errorCode = getFirstString(FILTER_ERROR_CODE);
        errorCodeEq = getEquality(FILTER_ERROR_CODE);
        break;
      default:
    }
  }


}
