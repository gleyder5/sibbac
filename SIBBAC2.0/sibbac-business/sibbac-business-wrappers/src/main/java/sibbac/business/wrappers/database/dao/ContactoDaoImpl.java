package sibbac.business.wrappers.database.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.ServiciosContactosData;
import sibbac.business.wrappers.database.model.Contacto;

/**
 * The Class ContactoDaoImpl.
 */
@Repository
public class ContactoDaoImpl {

	/** The em. */
	@PersistenceContext
	private EntityManager em;

	/** The Constant TAG. */
	private static final String TAG = ContactoDaoImpl.class.getName();

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(TAG);

	/** The Constant ID_ALIAS. */
	private static final String ID_ALIAS = "idAlias";

	/** The Constant CONFIGURACION_CONTACTO. */
	private static final String CONFIGURACION_CONTACTO = "configuracionContactos";

	/**
	 * Gets the contactoy servicios por alias.
	 *
	 * @param idAlias
	 *            the id alias
	 * @param configuracionAlias
	 *            the configuracion alias
	 * @param configuracionContacto
	 *            the configuracion contacto
	 * @return the contactoy servicios por alias
	 */
	@SuppressWarnings("unchecked")
	public List<ServiciosContactosData> getContactoyServiciosPorAlias(Long idAlias, Boolean configuracionContacto) {
		Query query;
		List<ServiciosContactosData> result;
		String prefix = "[" + TAG + "::getContactoyServiciosPorAlias] ";
		StringBuffer qlString = new StringBuffer("SELECT ");
		qlString.append(
				" new sibbac.business.wrappers.database.data.ServiciosContactosData( servicios.id, servicios.nombreServicio, servicios.descripcionServicio ,contacto.id ,contacto.nombre, contacto.apellido1,  contacto.apellido2, contacto.telefono1, contacto.email  ) ");
		qlString.append(
				" FROM Contacto contacto, Alias alias, ServiciosContactos serviciosContactos, Servicios servicios ");
		qlString.append(" WHERE contacto.alias = alias AND serviciosContactos.contacto = contacto AND");
		qlString.append(" serviciosContactos.servicio = servicios AND alias.id = :idAlias AND");
		qlString.append(" servicios.configuracionContactos = :configuracionContactos");

		try {
			query = em.createQuery(qlString.toString());
			query.setParameter(ID_ALIAS, idAlias);
			query.setParameter(CONFIGURACION_CONTACTO, configuracionContacto);
			result = query.getResultList();
		} catch (PersistenceException ex) {
			result = null;
			LOG.error(prefix + "ERROR(): {}", ex.getClass().getName(), ex.getMessage());

		}
		return result;

	}

	public List<Contacto> findContactosFiltro(Long idCliente, String nombre, String posicion, String telefono,
			String email, String fax, Long idAddress, String nombreNot, String posicionNot, String telefonoNot,
			String emailNot, String faxNot, Long idAddressNot) {

		Query query;
		List<Contacto> result;

		StringBuffer qlString = new StringBuffer("SELECT ");
		qlString.append(" contacto ");
		qlString.append(" FROM Contacto contacto ");
		qlString.append(" WHERE 1=1 AND contacto.activo = 'S'");
		if (idCliente != null) {
			qlString.append(" AND contacto.cliente.idClient = :idCliente ");
		}
		if (nombre != null && !nombre.isEmpty()) {
			qlString.append(" AND upper(contacto.nombre) = :nombre ");
		}
		if (nombreNot != null && !nombreNot.isEmpty()) {
			qlString.append(" AND upper(contacto.nombre) <> :nombreNot ");
		}
		if (posicion != null && !posicion.isEmpty()) {
			qlString.append(" AND upper(contacto.posicionCargo) = :posicion ");
		}
		if (posicionNot != null && !posicionNot.isEmpty()) {
			qlString.append(" AND upper(contacto.posicionCargo) <> :posicionNot ");
		}

		if (telefono != null && !telefono.isEmpty()) {
			qlString.append(" upper(AND contacto.telefono1) = :telefono ");
		}
		if (telefonoNot != null && !telefonoNot.isEmpty()) {
			qlString.append(" upper(AND contacto.telefono1) <> :telefonoNot ");
		}

		if (email != null && !email.isEmpty()) {
			qlString.append(" AND upper(contacto.email) = :email ");
		}
		if (emailNot != null && !emailNot.isEmpty()) {
			qlString.append(" AND upper(contacto.email) <> :emailNot ");
		}

		if (fax != null && !fax.isEmpty()) {
			qlString.append(" AND upper(contacto.fax) = :fax ");
		}
		if (faxNot != null && !faxNot.isEmpty()) {
			qlString.append(" AND upper(contacto.fax) <> :faxNot ");
		}

		if (idAddress != null) {
			qlString.append(" AND contacto.address.idAddress = :idAddress ");
		}
		if (idAddressNot != null) {
			qlString.append(" AND contacto.address.idAddress <> :idAddressNot ");
		}

		try {
			query = em.createQuery(qlString.toString());
			if (idAddress != null) {
				query.setParameter("idAddress", idAddress);
			}
			if (idCliente != null) {
				query.setParameter("idCliente", idCliente);
			}
			if (nombre != null && !nombre.isEmpty()) {
				query.setParameter("nombre", nombre);
			}
			if (posicion != null && !posicion.isEmpty()) {
				query.setParameter("posicion", posicion);
			}
			if (telefono != null && !telefono.isEmpty()) {
				query.setParameter("telefono", telefono);
			}

			if (email != null && !email.isEmpty()) {
				query.setParameter("email", email);
			}

			if (fax != null && !fax.isEmpty()) {
				query.setParameter("fax", fax);
			}

			if (idAddressNot != null) {
				query.setParameter("idAddressNot", idAddressNot);
			}
			if (nombreNot != null && !nombreNot.isEmpty()) {
				query.setParameter("nombreNot", nombreNot);
			}
			if (posicionNot != null && !posicionNot.isEmpty()) {
				query.setParameter("posicionNot", posicionNot);
			}
			if (telefonoNot != null && !telefonoNot.isEmpty()) {
				query.setParameter("telefonoNot", telefonoNot);
			}
			if (emailNot != null && !emailNot.isEmpty()) {
				query.setParameter("emailNot", emailNot);
			}
			if (faxNot != null && !faxNot.isEmpty()) {
				query.setParameter("faxNot", faxNot);
			}

			result = query.getResultList();
		} catch (PersistenceException ex) {
			result = null;
			// LOG.error( prefix + "ERROR(): {}", ex.getClass().getName(),
			// ex.getMessage() );

		}
		return result;

	}

}
