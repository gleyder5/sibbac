package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.data.TuplaIsinAlias;
import sibbac.business.wrappers.database.dto.ConciliacionERData;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;

public interface Tmct0MovimientoCuentaAliasDao extends JpaRepository<Tmct0MovimientoCuentaAlias, Long> {

  @Query("SELECT min(s.tradedate) FROM Tmct0MovimientoCuentaAlias s")
  Date findMinTradedate();

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S" + " WHERE S.settlementdate = :fecha " + " AND S.isin = :isin"
      + " AND S.cdaliass = :alias" + " AND S.cdcodigocuentaliq = :cdcodigocuentaliq" + " ORDER BY S.isin")
  public List<Tmct0MovimientoCuentaAlias> findAllByFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(
      @Param("fecha") Date fecha, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT SUM(S.titulos) FROM  Tmct0MovimientoCuentaAlias S" + " WHERE S.settlementdate = :fecha "
      + " AND S.isin = :isin" + " AND S.cdaliass = :alias" + " AND S.cdcodigocuentaliq = :cdcodigocuentaliq"
      + " ORDER BY S.isin")
  public Long findSumTitulosByFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.tradedate < :fecha "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.settlementdate IS NULL ")
  List<Object[]> findSumTitulosByTradedateLteAndIsinAndAliasAndCodigoCuentaLiquidacionNoLiquidados(
      @Param("fecha") Date fecha, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.tradedate < :fecha "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.settlementdate IS NOT NULL ")
  List<Object[]> findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      @Param("fecha") Date fecha, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.tradedate < :fecha "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.settlementdate IS NOT NULL AND S.cdoperacion in :listCdoperacion  ")
  List<Object[]> findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
      @Param("fecha") Date fecha, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq, @Param("listCdoperacion") String[] listCdoperacion);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.settlementdate < :fecha "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.cdoperacion in :listCdoperacion  ")
  List<Object[]> findSumTitulosBySettlementdateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
      @Param("fecha") Date fecha, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq, @Param("listCdoperacion") String[] listCdoperacion);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.tradedate >= :fechaIni  AND S.tradedate < :fechaFin "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.settlementdate IS NOT NULL AND S.cdoperacion in :listCdoperacion ")
  List<Object[]> findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
      @Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin, @Param("isin") String isin,
      @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq,
      @Param("listCdoperacion") String[] listCdoperacion);
  
  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.settlementdate >= :fechaIni  AND S.settlementdate < :fechaFin "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.cdoperacion in :listCdoperacion ")
  List<Object[]> findSumTitulosBySettlementdateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
      @Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin, @Param("isin") String isin,
      @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq,
      @Param("listCdoperacion") String[] listCdoperacion);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.tradedate >= :fechaIni  AND S.tradedate < :fechaFin "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND S.settlementdate IS NOT NULL")
  List<Object[]> findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      @Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin, @Param("isin") String isin,
      @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.settlementdate >= :fechaIni  AND S.settlementdate < :fechaFin "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq ")
  List<Object[]> findSumTitulosBySettlementdateGteAndSettlementdateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      @Param("fechaIni") Date fechaIni, @Param("fechaFin") Date fechaFin, @Param("isin") String isin,
      @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT SUM(S.titulos), SUM(S.imefectivo) FROM  Tmct0MovimientoCuentaAlias S WHERE S.settlementdate < :fechaFin "
      + "AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq ")
  List<Object[]> findSumTitulosBySettlementdateteLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      @Param("fechaFin") Date fechaFin, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S WHERE S.settlementdate BETWEEN :fechaInicio AND :fechaFin "
      + " AND S.isin = :isin AND S.cdaliass = :alias AND S.cdcodigocuentaliq = :cdcodigocuentaliq "
      + " ORDER BY S.isin")
  public List<Tmct0MovimientoCuentaAlias> findAllByRangeFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(
      @Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin, @Param("isin") String isin,
      @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S WHERE S.isin = :isin AND S.cdaliass = :alias "
      + "AND ((S.cdoperacion!='MAN' AND S.settlementdate >= :anterior AND S.settlementdate < :posterior) OR "
      + "(S.cdoperacion='MAN' AND S.settlementdate >= :anterior AND S.settlementdate < :posterior)) "
      + "AND S.cdcodigocuentaliq = :cdcodigocuentaliq AND (S.guardadosaldo = false or S.guardadosaldo is null) "
      + " ORDER BY S.isin")
  public List<Tmct0MovimientoCuentaAlias> findAllByRangeFechaAndIsinAndAlias(@Param("anterior") Date anterior,
      @Param("posterior") Date posterior, @Param("isin") String isin, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  // Nos vamos a traer los movimientos que no son manuales por el audit date y
  // los que lo son por la fecha
  // de liquidacion (settlement)
  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData"
      + "(mov.cdaliass, mov.isin, mov.cdcodigocuentaliq, sum(mov.titulos), sum(mov.imefectivo)) "
      + "FROM Tmct0MovimientoCuentaAlias mov "
      + "WHERE ((mov.cdoperacion!='MAN' AND mov.settlementdate >= :anterior AND mov.settlementdate < :posterior) OR "
      + "(mov.cdoperacion='MAN' AND mov.settlementdate >= :anterior AND mov.settlementdate < :posterior)) "
      + "AND (mov.guardadosaldo=false OR mov.guardadosaldo IS NULL) "
      + "GROUP BY mov.cdaliass, mov.isin, mov.cdcodigocuentaliq")
  public List<MovimientoCuentaAliasData> findSumByDateGroupedByPack(@Param("anterior") Date anterior,
      @Param("posterior") Date posterior);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData"
      + "(mov.cdaliass, mov.isin, mov.cdcodigocuentaliq, mov.settlementdate, sum(mov.titulos), sum(mov.imefectivo)) "
      + "FROM Tmct0MovimientoCuentaAlias mov " + "WHERE " + "(mov.guardadosaldo=false OR mov.guardadosaldo IS NULL) "
      + "AND mov.settlementdate IS NOT NULL "
      + "GROUP BY mov.cdaliass, mov.isin, mov.cdcodigocuentaliq, mov.settlementdate "
      + "ORDER BY mov.settlementdate ASC")
  public List<MovimientoCuentaAliasData> findMovimientosAutomaticosNoGrabados();

  // Nos vamos a traer los movimientos que no son manuales por el audit date y
  // los que lo son por la fecha
  // de liquidacion (settlement)
  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(mov.cdaliass, mov.isin, mov.cdcodigocuentaliq, sum(mov.titulos), sum(mov.imefectivo)) "
      + "FROM Tmct0MovimientoCuentaAlias mov "
      + "WHERE mov.settlementdate >= :anterior AND mov.settlementdate < :posterior "
      + "AND mov.isin=:isin "
      + "AND mov.cdaliass=:cdaliass "
      + "AND mov.cdcodigocuentaliq=:cdcodigocuentaliq "
      + "GROUP BY mov.cdaliass, mov.isin, mov.cdcodigocuentaliq")
  public MovimientoCuentaAliasData findOneSumByDateGroupedByPack(@Param("anterior") Date anterior,
      @Param("posterior") Date posterior, @Param("isin") String isin, @Param("cdaliass") String cdaliass,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(mov.cdaliass, mov.isin, mov.cdcodigocuentaliq, sum(mov.titulos), sum(mov.imefectivo)) "
      + "FROM Tmct0MovimientoCuentaAlias mov "
      + "WHERE mov.settlementdate BETWEEN :anterior AND :posterior "
      + "AND mov.cdcodigocuentaliq=:cdcodigocuentaliq "
      + "GROUP BY mov.cdaliass, mov.isin, mov.cdcodigocuentaliq "
      + "ORDER BY mov.isin ")
  public List<MovimientoCuentaAliasData> findAllSumByDateAndCodigoCuentaLiqGroupedByIsinCuentaLiq(
      @Param("anterior") Date anterior, @Param("posterior") Date posterior,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(mov.cdaliass, mov.isin, mov.cdcodigocuentaliq, sum(mov.titulos), sum(mov.imefectivo)) "
      + "FROM Tmct0MovimientoCuentaAlias mov "
      + "WHERE mov.settlementdate BETWEEN :anterior AND :posterior "
      + "AND mov.isin=:isin "
      + "AND mov.cdcodigocuentaliq=:cdcodigocuentaliq "
      + "GROUP BY mov.cdaliass, mov.isin, mov.cdcodigocuentaliq " + "ORDER BY mov.isin ")
  public List<MovimientoCuentaAliasData> findAllSumByIsinAndDateAndCodigoCuentaLiqGroupedByIsinCuentaLiq(
      @Param("isin") String isin, @Param("anterior") Date anterior, @Param("posterior") Date posterior,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  public List<Tmct0MovimientoCuentaAlias> findAllBySettlementdate(Date fecha);

  public List<Tmct0MovimientoCuentaAlias> findAllByCdaliass(String cdaliass);

  public List<Tmct0MovimientoCuentaAlias> findAllBySettlementdateGreaterThanEqualAndCdaliassInAndIsinIn(
      Date settlementdate, List<String> cdaliass, List<String> isin);

  public List<Tmct0MovimientoCuentaAlias> findAllBySettlementdateGreaterThanEqualAndCdcodigocuentaliqAndCdaliassInAndIsinIn(
      Date settlementdate, String cdcodigocuentaliq, List<String> cdaliass, List<String> isin);

  public List<Tmct0MovimientoCuentaAlias> findAllByIsinAndCdcodigocuentaliqAndAuditDateBetween(String isin,
      String codigoCuentaLiquidacion, Date fechaAnterior, Date fechaPosterior);

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S" + " WHERE S.settlementdate >= :fechadesde"
      + " AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion ")
  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiq(
      @Param("fechadesde") Date fechadesde, @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S" + " WHERE S.settlementdate >= :fechadesde"
      + " AND S.isin = :isin AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion ")
  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiqAndIsin(
      @Param("fechadesde") Date fechadesde, @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion,
      @Param("isin") String isin);

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S" + " WHERE S.settlementdate BETWEEN :fechadesde AND :fechahasta"
      + "  AND S.isin = :isin AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion")
  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinal(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion, @Param("isin") String isin);

  @Query("SELECT S FROM  Tmct0MovimientoCuentaAlias S" + " WHERE S.settlementdate BETWEEN :fechadesde AND :fechahasta"
      + " AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion ")
  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinal(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(S, v.descripcion) FROM  Tmct0MovimientoCuentaAlias S, Tmct0Valores v "
      + " WHERE S.isin = v.codigoDeValor AND S.settlementdate BETWEEN :fechadesde AND :fechahasta"
      + " AND S.isin = :isin AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion")
  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinal(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion, @Param("isin") String isin);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(S, v.descripcion) FROM  Tmct0MovimientoCuentaAlias S, Tmct0Valores v "
      + " WHERE S.isin = v.codigoDeValor AND S.settlementdate BETWEEN :fechadesde AND :fechahasta"
      + " AND S.isin = :isin AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion " + " AND S.titulos!=0")
  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinalAndTitulosNotZero(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion, @Param("isin") String isin);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(S, v.descripcion) FROM  Tmct0MovimientoCuentaAlias S, Tmct0Valores v "
      + " WHERE S.isin = v.codigoDeValor AND S.settlementdate BETWEEN :fechadesde AND :fechahasta"
      + " AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion ")
  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinal(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);

  @Query("SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(S, v.descripcion) FROM  Tmct0MovimientoCuentaAlias S, Tmct0Valores v "
      + " WHERE S.isin = v.codigoDeValor AND S.settlementdate BETWEEN :fechadesde AND :fechahasta"
      + " AND S.cdcodigocuentaliq = :codigoCuentaLiquidacion AND S.titulos!=0 ")
  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinalAndTitulosNotZero(
      @Param("fechadesde") Date fechadesde, @Param("fechahasta") Date fechahasta,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);

  /**
   * Obtiene la ultima fecha de actualizacion teniendo en cuenta los codigos de
   * operaciones pasados por parametro
   * 
   * @param operaciones
   * @return
   */
  @Query("SELECT MAX(m.auditDate) FROM Tmct0MovimientoCuentaAlias m WHERE m.cdoperacion in (:operaciones)")
  public Date findLastUpdateDateTimeInOperaciones(@Param("operaciones") String... operaciones);

  /**
   * 
   * @param refMovimiento
   * @return
   */
  @Query("SELECT mca FROM Tmct0MovimientoCuentaAlias mca WHERE mca.refmovimiento = :refMovimiento")
  public List<Tmct0MovimientoCuentaAlias> findByRefMovimiento(@Param("refMovimiento") String refMovimiento);

  // Se trae las tuplas que no estan metidas en un saldo inicial
  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.isin=:isin AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByIsinAndCuenta(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.isin<>:isin AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotCuenta(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.isin=:isin AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByIsinAndNotCuenta(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.isin<>:isin AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndCuenta(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdaliass=:alias  AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByAliasAndCuenta(@Param("fecha") Date fecha, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdaliass<>:alias  AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByNotAliasAndNotCuenta(@Param("fecha") Date fecha, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdaliass=:alias  AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByAliasAndNotCuenta(@Param("fecha") Date fecha, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdaliass<>:alias  AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByNotAliasAndCuenta(@Param("fecha") Date fecha, @Param("alias") String alias,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplas(@Param("fecha") Date fecha);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByCuenta(@Param("fecha") Date fecha,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND CONCAT(TRIM(mov.isin), TRIM(mov.cdaliass)) NOT IN (SELECT CONCAT(TRIM(saldo.isin), TRIM(saldo.cdaliass)) "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.fecha<=:fecha)")
  public List<TuplaIsinAlias> getTuplasByNotCuenta(@Param("fecha") Date fecha,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin  AND mov.cdaliass NOT IN (SELECT saldo.cdaliass "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.isin=:isin)")
  public List<TuplaIsinAlias> getTuplasByIsin(@Param("fecha") Date fecha, @Param("isin") String isin);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin  AND mov.cdaliass NOT IN (SELECT saldo.cdaliass "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.isin<>:isin)")
  public List<TuplaIsinAlias> getTuplasByNotIsin(@Param("fecha") Date fecha, @Param("isin") String isin);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdaliass=:alias AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias)")
  public List<TuplaIsinAlias> getTuplasByAlias(@Param("fecha") Date fecha, @Param("alias") String alias);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.cdaliass<>:alias AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias)")
  public List<TuplaIsinAlias> getTuplasByNotAlias(@Param("fecha") Date fecha, @Param("alias") String alias);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdaliass=:alias AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.isin=:isin)")
  public List<TuplaIsinAlias> getTuplasByIsinAndAlias(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("alias") String alias);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdaliass<>:alias AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.isin<>:isin)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAlias(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("alias") String alias);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdaliass<>:alias AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.isin=:isin)")
  public List<TuplaIsinAlias> getTuplasByIsinAndNotAlias(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("alias") String alias);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE  ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdaliass=:alias AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.isin<>:isin)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndAlias(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("alias") String alias);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdaliass=:alias "
      + " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.isin=:isin AND saldo.cdcodigocuentaliq=:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByIsinAndAliasAndCuenta(@Param("fecha") Date fecha, @Param("isin") String isin,
      @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdaliass<>:alias "
      + " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.isin<>:isin AND saldo.cdcodigocuentaliq=:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAliasAndCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdaliass<>:alias "
      + " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.isin=:isin AND saldo.cdcodigocuentaliq=:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByIsinAndNotAliasAndCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdaliass=:alias "
      + " AND mov.cdcodigocuentaliq=:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.isin<>:isin AND saldo.cdcodigocuentaliq=:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndAliasAndCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdaliass=:alias "
      + " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.isin=:isin AND saldo.cdcodigocuentaliq<>:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByIsinAndAliasAndNotCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdaliass<>:alias "
      + " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.isin<>:isin AND saldo.cdcodigocuentaliq<>:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAliasAndNotCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin=:isin AND mov.cdaliass<>:alias "
      + " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass<>:alias AND saldo.isin=:isin AND saldo.cdcodigocuentaliq<>:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByIsinAndNotAliasAndNotCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT DISTINCT new sibbac.business.wrappers.database.data.TuplaIsinAlias(mov.isin, mov.cdaliass) "
      // +
      // "FROM Tmct0MovimientoCuentaAlias mov WHERE ((mov.cdoperacion!='MAN' AND mov.auditDate<=:fecha) OR (mov.cdoperacion='MAN' AND mov.settlementdate<=:fecha)) "
      + "FROM Tmct0MovimientoCuentaAlias mov WHERE (mov.cdoperacion!='MAN' AND mov.settlementdate<=:fecha)  "
      + "AND mov.isin<>:isin AND mov.cdaliass=:alias "
      + " AND mov.cdcodigocuentaliq<>:cdcodigocuentaliq AND mov.isin NOT IN (SELECT saldo.isin "
      + "FROM Tmct0SaldoInicialCuenta saldo WHERE saldo.cdaliass=:alias AND saldo.isin<>:isin AND saldo.cdcodigocuentaliq<>:cdcodigocuentaliq)")
  public List<TuplaIsinAlias> getTuplasByNotIsinAndAliasAndNotCuenta(@Param("fecha") Date fecha,
      @Param("isin") String isin, @Param("alias") String alias, @Param("cdcodigocuentaliq") String cdcodigocuentaliq);

  @Query("SELECT new sibbac.business.wrappers.database.dto.ConciliacionERData(m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdcodigocuentaliq, SUM(m.titulos), SUM(m.imefectivo), SUM(m.corretaje)) "
      + "FROM Tmct0MovimientoCuentaAlias m, Tmct0Valores v "
      // enlaces
      + "WHERE m.isin=v.codigoDeValor "
      // parametros
      + "AND m.settlementdate = :fechadesde "
      + "AND m.cdcodigocuentaliq = :codigoCuentaLiquidacion "
      + "AND m.isin = :isin "
      + "GROUP BY m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdcodigocuentaliq "
      + "ORDER BY m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdcodigocuentaliq")
  public List<ConciliacionERData> getInfoConciliacionSV(@Param("fechadesde") Date fechadesde,
      @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion, @Param("isin") String isin);

  /*
   * SELECT M.SETTLEMENTDATE , M.ISIN , V.DESCRIPCION , M.MERCADO ,
   * M.CD_CODIGO_CUENTA_LIQ , SUM(M.TITULOS) AS "Titulos" , SUM(M.IMEFECTIVO) AS
   * "Efectivo" , SUM(M.CORRETAJE) AS "Corretaje" FROM
   * TMCT0_MOVIMIENTO_CUENTA_ALIAS M , TMCT0_VALORES V WHERE M.ISIN =
   * V.CODIGO_DE_VALOR GROUP BY M.SETTLEMENTDATE, M.ISIN, V.DESCRIPCION,
   * M.MERCADO, M.CD_CODIGO_CUENTA_LIQ ORDER BY M.SETTLEMENTDATE, M.ISIN,
   * V.DESCRIPCION, M.MERCADO, M.CD_CODIGO_CUENTA_LIQ;
   */
  @Query("SELECT new sibbac.business.wrappers.database.dto.ConciliacionERData(m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdcodigocuentaliq, SUM(m.titulos), SUM(m.imefectivo), SUM(m.corretaje)) "
      + "FROM Tmct0MovimientoCuentaAlias m, Tmct0Valores v "
      // enlaces
      + "WHERE m.isin=v.codigoDeValor "
      // parametros
      + "AND m.settlementdate IS NOT NULL "
      + "AND m.settlementdate BETWEEN :fechadesde AND :fechahasta "
      + "AND m.cdcodigocuentaliq = :codigoCuentaLiquidacion "
      + "GROUP BY m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdcodigocuentaliq "
      + "ORDER BY m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdcodigocuentaliq")
  public List<ConciliacionERData> getInfoConciliacionSV(@Param("fechadesde") Date fechadesde,
      @Param("fechahasta") Date fechahasta, @Param("codigoCuentaLiquidacion") String codigoCuentaLiquidacion);

  @Modifying
  @Query("UPDATE Tmct0MovimientoCuentaAlias mov SET mov.guardadosaldo=:guardado "
      + "WHERE mov.settlementdate = :settlementDate AND mov.isin = :isin AND mov.cdaliass = :cdaliass "
      + " AND mov.cdcodigocuentaliq = :cdcodigocuentaliq ")
  public int updateGuardadoSaldo(@Param("cdaliass") String cdaliass, @Param("isin") String isin,
      @Param("cdcodigocuentaliq") String cdcodigocuentaliq, @Param("settlementDate") Date settlementDate,
      @Param("guardado") boolean guardado);

}
