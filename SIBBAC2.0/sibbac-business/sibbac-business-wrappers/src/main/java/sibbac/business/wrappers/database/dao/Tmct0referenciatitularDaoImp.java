package sibbac.business.wrappers.database.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Repository;

@Repository
public class Tmct0referenciatitularDaoImp {

  @PersistenceContext
  private EntityManager em;

  public Tmct0referenciatitularDaoImp() {
  }

  @SuppressWarnings("unchecked")
  public String findReferenciaTitularFromReferenciasTitularFis(String referenciaAdicional,
      List<String> indentificaciones) {
    if (CollectionUtils.isNotEmpty(indentificaciones)) {

      Map<String, Object> params = new HashMap<String, Object>();
      params.put("pnunidentificaciones", indentificaciones.size());
      StringBuilder sql = new StringBuilder("SELECT DISTINCT R.CDREFTITULARPTI FROM TMCT0REFERENCIATITULAR R ");
      sql.append(" WHERE  :pnunidentificaciones = ");
      sql.append(" (SELECT COUNT(*) FROM TMCT0REFERENCIATITULAR_FIS RF WHERE R.IDREFERENCIA = RF.IDREFERENCIA) ");
      if (referenciaAdicional != null) {
        sql.append(" AND R.REFERENCIA_ADICIONAL = :preferenciaAdicional ");
        params.put("preferenciaAdicional", referenciaAdicional);
      }
      int i = 0;
      for (String identi : indentificaciones) {
        String idparam = "pidtitular" + i;
        sql.append(" AND EXISTS (SELECT RF.IDREFERENCIA FROM TMCT0REFERENCIATITULAR_FIS RF WHERE R.IDREFERENCIA = RF.IDREFERENCIA AND RF.IDTITULAR = ");
        sql.append(":").append(idparam).append(" ) ");
        params.put(idparam, identi);
        i++;
      }

      sql.append(" ORDER BY R.CDREFTITULARPTI DESC ");

      Query q = em.createNativeQuery(sql.toString());

      for (String key : params.keySet()) {
        q.setParameter(key, params.get(key));
      }
      List<String> referencias = q.getResultList();
      if (CollectionUtils.isNotEmpty(referencias)) {
        return (String) referencias.get(0);
      }
    }
    return null;
  }
}
