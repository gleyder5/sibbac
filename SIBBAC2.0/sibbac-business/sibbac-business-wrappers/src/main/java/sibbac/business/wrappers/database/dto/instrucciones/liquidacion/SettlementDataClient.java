package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import sibbac.business.fase0.database.model.Tmct0mfd;

public class SettlementDataClient implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 8133923044543239576L;
  // Datos
  private SettlementDataClientDTO tmct0cli = null;
  private List<SettlementDataClientMfdDTO> listTmct0mfd = new ArrayList<SettlementDataClientMfdDTO>();

  // Flags
  private boolean dataClient = false;
  private boolean dataMifid = false;
  private String errormsg = "";

  public SettlementDataClient() {
    tmct0cli = new SettlementDataClientDTO();
    dataClient = false;
    dataMifid = false;
    listTmct0mfd = new ArrayList<SettlementDataClientMfdDTO>();
    initSettlementData();
  }

  public SettlementDataClient(SettlementDataClient toCopy) {
    this();

    try {
      tmct0cli = new SettlementDataClientDTO(toCopy.getTmct0cli());

    } catch (Exception e) {
    }
    dataClient = new Boolean(toCopy.dataClient);
    dataMifid = new Boolean(toCopy.dataMifid);
    listTmct0mfd = new ArrayList<SettlementDataClientMfdDTO>(toCopy.listTmct0mfd);

  }

  public SettlementDataClientDTO getTmct0cli() {
    return tmct0cli;
  }

  public void setTmct0cli(SettlementDataClientDTO tmct0cli) {
    this.tmct0cli = tmct0cli;
  }

  public boolean isDataClient() {
    return dataClient;
  }

  public void setDataClient(boolean dataClient) {
    this.dataClient = dataClient;
  }

  public boolean isDataMifid() {
    return dataMifid;
  }

  public void setDataMifid(boolean dataMifid) {
    this.dataMifid = dataMifid;
  }

  public List<SettlementDataClientMfdDTO> getListTmct0mfd() {
    return listTmct0mfd;
  }

  public void setListTmct0mfd(List<SettlementDataClientMfdDTO> listTmct0mfd) {
    this.listTmct0mfd = listTmct0mfd;
  }

  public void setListTmct0mfd2(List<Tmct0mfd> listTmct0mfd) {
    if (this.listTmct0mfd != null) {
      this.listTmct0mfd.clear();
    } else {
      this.listTmct0mfd = new ArrayList<SettlementDataClientMfdDTO>();
    }
    if (listTmct0mfd != null) {
      SettlementDataClientMfdDTO dto;
      for (Tmct0mfd tmct0mfd : listTmct0mfd) {
        dto = new SettlementDataClientMfdDTO(tmct0mfd);
        this.listTmct0mfd.add(dto);
      }
    }

  }

  private void initSettlementData() {
    tmct0cli.initData();
  }

  /**
   * @return the errormsg
   */
  public String getErrormsg() {
    return errormsg;
  }

  /**
   * @param errormsg the errormsg to set
   */
  public void setErrormsg(String errormsg) {
    this.errormsg = errormsg;
  }

}
