package sibbac.business.wrappers.database.dto;

import java.io.Serializable;

public class DatosAsignacionDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -7293487602972060004L;
  private final String cuentaCompensacionDestino;
  private final String miembroCompensadorDestino;
  private final String codigoReferenciaAsignacion;

  public DatosAsignacionDTO(String cuentaCompensacionDestino, String miembroCompensadorDestino,
      String codigoReferenciaAsignacion) {
    super();
    this.cuentaCompensacionDestino = cuentaCompensacionDestino;
    this.miembroCompensadorDestino = miembroCompensadorDestino;
    this.codigoReferenciaAsignacion = codigoReferenciaAsignacion;
  }

  public String getCuentaCompensacionDestino() {
    return cuentaCompensacionDestino;
  }

  public String getMiembroCompensadorDestino() {
    return miembroCompensadorDestino;
  }

  public String getCodigoReferenciaAsignacion() {
    return codigoReferenciaAsignacion;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codigoReferenciaAsignacion == null) ? 0 : codigoReferenciaAsignacion.hashCode());
    result = prime * result + ((cuentaCompensacionDestino == null) ? 0 : cuentaCompensacionDestino.hashCode());
    result = prime * result + ((miembroCompensadorDestino == null) ? 0 : miembroCompensadorDestino.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DatosAsignacionDTO other = (DatosAsignacionDTO) obj;
    if (codigoReferenciaAsignacion == null) {
      if (other.codigoReferenciaAsignacion != null)
        return false;
    }
    else if (!codigoReferenciaAsignacion.equals(other.codigoReferenciaAsignacion))
      return false;
    if (cuentaCompensacionDestino == null) {
      if (other.cuentaCompensacionDestino != null)
        return false;
    }
    else if (!cuentaCompensacionDestino.equals(other.cuentaCompensacionDestino))
      return false;
    if (miembroCompensadorDestino == null) {
      if (other.miembroCompensadorDestino != null)
        return false;
    }
    else if (!miembroCompensadorDestino.equals(other.miembroCompensadorDestino))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String
        .format(
            "DatosAsignacionDTO [cuentaCompensacionDestino=%s, miembroCompensadorDestino=%s, codigoReferenciaAsignacion=%s]",
            cuentaCompensacionDestino, miembroCompensadorDestino, codigoReferenciaAsignacion);
  }

}
