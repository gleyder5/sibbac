package sibbac.business.wrappers.rest.filter;

import java.util.Map;

import sibbac.business.wrappers.database.filter.FinalHoldersFilter;

public class FinalHoldersFilterImpl implements FinalHoldersFilter {
  
  private final Map<String, String[]> parameterMap;

  public FinalHoldersFilterImpl(Map<String, String[]> parameterMap) {
    this.parameterMap = parameterMap;
  }

  @Override
  public String getName() {
    final String list[];
    
    list = parameterMap.get("name");
    if(list != null && list.length > 0) {
      return list[0];
    }
    return null;
  }

  @Override
  public boolean isThereName() {
    return parameterMap.containsKey("name");
  }
  
  
}
