package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0IndicadorDeCapacidad;

@Repository
public interface Tmct0IndicadorDeCapacidadDao extends JpaRepository<Tmct0IndicadorDeCapacidad, Long>{
	
	@Query("SELECT capacidad FROM Tmct0IndicadorDeCapacidad capacidad WHERE capacidad.descripcion=:descripcion ")
	public Tmct0IndicadorDeCapacidad findByDescripcion(@Param("descripcion") String descripcion );

}
