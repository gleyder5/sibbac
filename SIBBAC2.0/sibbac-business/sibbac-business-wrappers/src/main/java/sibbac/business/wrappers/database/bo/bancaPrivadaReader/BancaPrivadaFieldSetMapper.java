package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.validation.BindException;

import sibbac.business.wrappers.database.bo.bancaPrivadaReader.HBB4TipoRegistroFieldSetMapper.HBB4TipoRegistro;

public class BancaPrivadaFieldSetMapper implements FieldSetMapper<HBBNRecordBean> {
  public final static String delimiter = ";";

  public enum BancaPrivadaFields {
    IDENTIFMSG("identifiMsg", 4),
    CODORIGEN("codOrigen", 4),
    SECUENCIA("secuencia", 5),
    RESTOFLINE("restOfLine", 269);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private BancaPrivadaFields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      BancaPrivadaFields[] fields = BancaPrivadaFields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getBancaPrivadaLengths() {
      BancaPrivadaFields[] fields = BancaPrivadaFields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public HBBNRecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    HBBNRecordBean hbbnBean = new HBBNRecordBean();
    hbbnBean.setTipoRegistro(fieldSet.readString(BancaPrivadaFields.IDENTIFMSG.text));
    String restOfMsg = fieldSet.readString(BancaPrivadaFields.RESTOFLINE.text);
    if (hbbnBean.getTipoRegistro().equals("HBB7")) {
      FixedLengthTokenizer fixedLengthTokenizer = new FixedLengthTokenizer();
      fixedLengthTokenizer.setNames(HBB7FieldSetMapper.HBB7Fields.getFieldNames());
      fixedLengthTokenizer.setColumns(this.convertLengthsIntoRanges(HBB7FieldSetMapper.HBB7Fields.getHBB7Lengths()));
      fixedLengthTokenizer.setStrict(false);
      FieldSet fieldSetHBB7 = fixedLengthTokenizer.tokenize(restOfMsg);
      hbbnBean = new HBB7FieldSetMapper().mapFieldSet(fieldSetHBB7);
    } else if (hbbnBean.getTipoRegistro().equals("HBB6")) {
      // Processing Registro inicio/fin
      FixedLengthTokenizer fixedLengthTokenizer = new FixedLengthTokenizer();
      fixedLengthTokenizer.setNames(HBB6FieldSetMapper.HBB6Fields.getFieldNames());
      fixedLengthTokenizer.setColumns(this.convertLengthsIntoRanges(HBB6FieldSetMapper.HBB6Fields.getHBB6Lengths()));
      fixedLengthTokenizer.setStrict(false);
      FieldSet fieldSetHBB6 = fixedLengthTokenizer.tokenize(restOfMsg);
      hbbnBean = new HBB6FieldSetMapper().mapFieldSet(fieldSetHBB6);

    } else if (hbbnBean.getTipoRegistro().equals("HBB4")) {
      // Processing Registro de titulares
      FixedLengthTokenizer fixedLengthTokenizer = new FixedLengthTokenizer();
      fixedLengthTokenizer.setNames(HBB4TipoRegistroFieldSetMapper.HBB4TipoRegistroFields.getFieldNames());
      fixedLengthTokenizer.setColumns(this.convertLengthsIntoRanges(HBB4TipoRegistroFieldSetMapper.HBB4TipoRegistroFields.getHBB4Lengths()));
      fixedLengthTokenizer.setStrict(false);
      FieldSet fieldSetHbb4TipoRegistro = fixedLengthTokenizer.tokenize(restOfMsg);
      HBB4TipoRegistroRecordBean tipoRegistro = new HBB4TipoRegistroFieldSetMapper().mapFieldSet(fieldSetHbb4TipoRegistro);

      fixedLengthTokenizer = new FixedLengthTokenizer();
      fixedLengthTokenizer.setStrict(false);
      if (tipoRegistro.getTipoRegistroHBB().intValue() == HBB4TipoRegistro.DESGLSE.tipo) {
        fixedLengthTokenizer.setNames(HBB4DesgloseFieldSetMapper.HBB4DesgloseFields.getFieldNames());
        fixedLengthTokenizer.setColumns(this.convertLengthsIntoRanges(HBB4DesgloseFieldSetMapper.HBB4DesgloseFields.getHBB4Lengths()));
        FieldSet fieldSetHBB4 = fixedLengthTokenizer.tokenize(restOfMsg);
        hbbnBean = new HBB4DesgloseFieldSetMapper().mapFieldSet(fieldSetHBB4);
      } else {
        fixedLengthTokenizer.setNames(HBB4TitularFieldSetMapper.HBB4TitularFields.getFieldNames());
        fixedLengthTokenizer.setColumns(this.convertLengthsIntoRanges(HBB4TitularFieldSetMapper.HBB4TitularFields.getHBB4Lengths()));

        FieldSet fieldSetHBB4 = fixedLengthTokenizer.tokenize(restOfMsg);
        hbbnBean = new HBB4TitularFieldSetMapper().mapFieldSet(fieldSetHBB4);
      }

    } else if (hbbnBean.getTipoRegistro().equals("HBB5")) {
      // Processing Registro de domicilios
      FixedLengthTokenizer fixedLengthTokenizer = new FixedLengthTokenizer();
      fixedLengthTokenizer.setNames(HBB5FieldSetMapper.HBB5Fields.getFieldNames());
      fixedLengthTokenizer.setColumns(this.convertLengthsIntoRanges(HBB5FieldSetMapper.HBB5Fields.getHBB5Lengths()));
      fixedLengthTokenizer.setStrict(false);
      FieldSet fieldSetHBB5 = fixedLengthTokenizer.tokenize(restOfMsg);
      hbbnBean = new HBB5FieldSetMapper().mapFieldSet(fieldSetHBB5);

    }

    hbbnBean.setTipoRegistro(fieldSet.readString(BancaPrivadaFields.IDENTIFMSG.text));
    hbbnBean.setCodOrigen(fieldSet.readBigDecimal(BancaPrivadaFields.CODORIGEN.text));
    hbbnBean.setSecuencia(fieldSet.readBigDecimal(BancaPrivadaFields.SECUENCIA.text));

    return hbbnBean;
  }

  protected Range[] convertLengthsIntoRanges(int[] fieldLenghts) {
    Range[] ranges = new Range[fieldLenghts.length];
    int distAux = 1;
    for (int i = 0; i < fieldLenghts.length; i++) {
      if (fieldLenghts[i] > 0) {
        Range auxRange = new Range(distAux, distAux + fieldLenghts[i] - 1);
        distAux += fieldLenghts[i];
        ranges[i] = auxRange;
      }
    }
    return ranges;
  }

}
