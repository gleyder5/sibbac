/**
 * 
 */
package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.common.RunnableProcess;
import sibbac.business.wrappers.database.bo.CodigoErrorBo;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.PartenonRecordBeanBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0paisesBo;
import sibbac.business.wrappers.database.bo.Tmct0provinciasBo;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

/**
 * @author xIS16630
 *
 */
@Component(value = "titularesOperacionesEspecialesDbReader")
public class TitularesOperacionesEspecialesDbReader extends WrapperMultiThreadedDbReader<PartenonRecordBean> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TitularesOperacionesEspecialesDbReader.class);

  /** Numero de operaciones a realizar en cada commit de base de datos. */
  @Value("${sibbac.wrappers.transaction.size}")
  private Integer transactionSize;

  @Value("${sibbac.wrappers.thread.size}")
  private Integer threadSize;

  @Value("${sibbac.wrappers.large.query.page.size}")
  private Integer largeQueryPageSize;

  @Value("${sibbac.wrappers.operacion.especial.titulares.max.execution.pages}")
  private Integer maxExecutionPages;

  @Autowired
  private PartenonRecordBeanBo partenonRecordBeanBo;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Qualifier(value = "titularesOperacionesEspecialesRunnable")
  @Autowired
  private TitularesOperacionesEspecialesRunnable titularesRunnable;

  /** Objeto para el acceso a la tabla <code>TMCT0PAISES</code>. */
  @Autowired
  protected Tmct0paisesBo tmct0paisesBo;

  /** Objeto para el acceso a la tabla <code>TMCT0PROVINCIAS</code>. */
  @Autowired
  protected Tmct0provinciasBo tmct0provinciasBo;

  @Autowired
  protected Tmct0cfgBo tmct0cfgBo;

  @Autowired
  protected Tmct0ordBo ordBo;

  @Autowired
  protected CodigoErrorBo codBo;

  @Autowired
  private Tmct0paisesBo paisesBo;

  /**
   * Flag para saber de donde recuperar las ops
   * */
  protected boolean operacionEspecial = false;

  /** Lista de días fectivos. */
  protected List<Date> festivosList;

  /** Mapa de errores. */
  protected Map<String, CodigoErrorData> erroresMap;

  protected Map<String, Tmct0paises> isosPaises;
  protected Map<String, Tmct0paises> isosPaisesHacienda;
  protected Map<String, List<String>> nombresCiudadPorDistrito;

  /**
   * 
   */
  public TitularesOperacionesEspecialesDbReader() {
    super();
  }

  @Override
  public IRunnableBean<OperacionEspecialRecordBean> getBeanToProcessBlock() {
    titularesRunnable.initDataPreRun(festivosList, erroresMap, isosPaises, isosPaisesHacienda,
                                     nombresCiudadPorDistrito, true);
    return titularesRunnable;
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @SuppressWarnings("unchecked")
  private List<PartenonRecordBean> getListaTitularesEspecialesSinCargar() throws SIBBACBusinessException {
    final List<PartenonRecordBean> list = (List<PartenonRecordBean>) Collections.synchronizedList(new ArrayList<PartenonRecordBean>());

    final Pageable page = new PageRequest(0, maxExecutionPages * largeQueryPageSize);
    LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] buscando lista de nurefords en EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR");
    LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] nurefords {}", page);
    List<String> nurefords = desgloseRecordBeanBo.findAllNurefordOperacionesEspeciales(EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR.getId(),
                                                                                       page);

    LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] encontrados {} nurefors", nurefords.size());
    if (CollectionUtils.isNotEmpty(nurefords)) {
      try {
        int posIni = 0;
        int posEnd = 0;
        while (posEnd < nurefords.size()) {
          posEnd += largeQueryPageSize;
          if (posEnd > nurefords.size()) {
            posEnd = nurefords.size();
          }

          LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] pageable posIni : {} posEnd : {}",
                    posIni, posEnd);
          final List<String> nurefordSubList = new ArrayList<String>(nurefords.subList(posIni, posEnd));
          LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] Lanzando hilo para leer...");
          RunnableProcess<PartenonRecordBean> process = new RunnableProcess<PartenonRecordBean>(getObserver()) {

            @Override
            public void run() {
              LOG.debug("[TitularesOperacionesEspecialesDbReader :: run] inicio.");
              try {
                LOG.debug("[TitularesOperacionesEspecialesDbReader :: run] buscando titulares.");
                LOG.trace("[TitularesOperacionesEspecialesDbReader :: run] buscando titulares. nurefords: {}",
                          nurefordSubList);
                List<PartenonRecordBean> listSub = partenonRecordBeanBo.findAllTitularesOperacionesEspecialesByListNureford(nurefordSubList);
                LOG.debug("[TitularesOperacionesEspecialesDbReader :: run] encontrados {} titulares.", listSub.size());
                list.addAll(listSub);
                LOG.debug("[TitularesOperacionesEspecialesDbReader :: run] lista total despues de añadir {} titulares.",
                          list.size());
              } catch (Exception e) {
                getObserver().addErrorProcess(new SIBBACBusinessException("Incidencia con sublista : " + nurefordSubList, e));
              } finally {
                getObserver().sutDownThread(this);
              }
              LOG.debug("[TitularesOperacionesEspecialesDbReader :: run] fin.");
            }
          };
          executeProcessInThread(process);

          LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] hilo lanzado");
          posIni = posEnd;
        }
      } catch (Exception e) {
        closeExecutorNow();
        throw new SIBBACBusinessException(e);
      } finally {

        closeExecutor();
        nurefords.clear();
      }

    }
    if (getObserver().getException() != null) {
      list.clear();
      throw new SIBBACBusinessException(getObserver().getException());
    }
    LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] ordenando {} nurefords....", list.size());
    Collections.sort(list, new Comparator<PartenonRecordBean>() {

      @Override
      public int compare(PartenonRecordBean arg0, PartenonRecordBean arg1) {
        int compareNumorden = arg0.getNumOrden().compareTo(arg1.getNumOrden());
        if (compareNumorden == 0) {
          int compareIdSec = arg0.getIdSenc().compareTo(arg1.getIdSenc());
          if (compareIdSec == 0) {
            return arg0.getId().compareTo(arg1.getId());
          } else {
            return compareIdSec;
          }
        } else {
          return compareNumorden;
        }
      }
    });
    LOG.debug("[TitularesOperacionesEspecialesDbReader :: getListaProcesar] ordenados {} nurefords", list.size());
    return list;
  }

  @Override
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.debug("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] inicio");
    List<PartenonRecordBean> list = getListaTitularesEspecialesSinCargar();

    if (CollectionUtils.isNotEmpty(list)) {
      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] encontrado beanlist: {}", list);
      for (PartenonRecordBean partenonRecordBean : list) {
        partenonRecordBean.loadNombreAppelidos();
        partenonRecordBean.loadDireccion();
      }
      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] despues de cargar datos: {}", list);
      beanIterator = list.iterator();

      festivosList = tmct0cfgBo.findHolidaysBolsa();
      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] festivos {}", festivosList);

      erroresMap = new HashMap<String, CodigoErrorData>();
      List<CodigoErrorData> listaErrores = codBo.findAllByGrupoOrderByCodigoDataAsc("Routing Titulares");
      if (listaErrores != null) {
        for (CodigoErrorData error : listaErrores) {
          erroresMap.put(error.getCodigo(), error);
        } // for
      } // if

      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] codigos error {}", listaErrores);

      isosPaises = paisesBo.findMapAllActivosByIsonnum();

      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] isos {}", isosPaises);

      isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();

      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] isos hacienda {}", isosPaisesHacienda);
      nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();

      LOG.trace("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] nombres ciudad {}",
                nombresCiudadPorDistrito);
    }
    LOG.debug("[TitularesOperacionesEspecialesDbReader :: preExecuteTask] fin");
  }

  @Override
  public void postExecuteTask() {
    titularesRunnable.initDataPreRun(null, null, null, null, null, false);
  }

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    boolean res = true;
    PartenonRecordBean thisBean = (PartenonRecordBean) bean;
    PartenonRecordBean thisPreviousBean = (PartenonRecordBean) previousBean;
    if (previousBean != null) {
      res = thisPreviousBean.getNumOrden().equals(thisBean.getNumOrden());
    }
    return res;
  }

}
