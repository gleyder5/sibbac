package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.Tmct0EntidadRegistroBo;
import sibbac.business.wrappers.database.bo.Tmct0TipoErBo;
import sibbac.business.wrappers.database.data.EntidadRegistroData;
import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceEntidadRegistro implements SIBBACServiceBean {

	private static final String		TAG								= SIBBACServiceEntidadRegistro.class.getName();
	private static final Logger		LOG								= LoggerFactory.getLogger( TAG );
	// COMMANDS
	private static final String		CMD_GET_ALL_ENTIDAD_REGISTRO	= "getAllEntidadRegistro";
	private static final String		CMD_GET_ALL_TIPO_ER				= "getAllTiposEntidadRegistro";
	private static final String		CMD_SAVE_ENTIDAD_REGISTRO		= "saveEntidadRegistro";
	private static final String		CMD_SAVE_TIPO_ER				= "saveTipoEntidadRegistro";
	private static final String		CMD_UPDATE_ENTIDAD_REGISTRO		= "updateEntidadRegistro";
	private static final String		CMD_UPDATE_TIPO_ER				= "updateTipoEntidadRegistro";
	private static final String		CMD_DELETE_ENTIDAD_REGISTRO		= "deleteEntidadRegistro";
	private static final String		CMD_DELETE_TIPO_ER				= "deleteTipoEntidadRegistro";

	// FILTERS
	private static final String		FILTER_NOMBRE_TIPO_ER			= "nombre";
	private static final String		FILTER_NOMBRE_ER				= "nombre";
	private static final String		FILTER_ID_TIPO_ER				= "idTipoEr";
	private static final String		FILTER_BIC_ER					= "bic";
	private static final String		FILTER_ID_TIPO_ER_ER			= "idTipoEr";
	private static final String		FILTER_ID_ER					= "idEr";

	// RESULTS
	private static final String		RESULT_ENTIDADES_REGISTRO		= "result_entidades_de_registro";
	private static final String		RESULT_TIPOS_ENTIDAD_REGISTRO	= "result_tipos_entidad_registro";
	public static final String		RESULTADO						= "resultado";
	public static final String		RESULTADO_SUCCESS				= "SUCCESS";
	public static final String		RESULTADO_ERROR					= "ERROR";

	// Business Objects
	@Autowired
	private Tmct0EntidadRegistroBo	erBo;
	@Autowired
	private Tmct0TipoErBo			tipoErBo;

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		final String command = webRequest.getAction();
		Map< String, Object > resultMap = new HashMap< String, Object >();
		WebResponse response = new WebResponse();
		switch ( command ) {
			case CMD_GET_ALL_ENTIDAD_REGISTRO:
				Map< String, String > filtersReg = webRequest.getFilters();
				List< EntidadRegistroData > resultList = new ArrayList< EntidadRegistroData >();
				if ( MapUtils.isEmpty( filtersReg ) ) {
					resultList = getAllEntidadRegistro();
				} else {
					resultList = getAllEntidadRegistro( filtersReg );
				}
				resultMap.put( RESULT_ENTIDADES_REGISTRO, resultList );
				response.setResultados( resultMap );
				break;
			case CMD_GET_ALL_TIPO_ER:
				Map< String, String > filters = webRequest.getFilters();
				List< Tmct0TipoEr > tipos = new ArrayList< Tmct0TipoEr >();
				if ( !MapUtils.isEmpty( filters ) && filters.get( FILTER_NOMBRE_TIPO_ER ) != null
						&& !filters.get( FILTER_NOMBRE_TIPO_ER ).equals( "" ) ) {
					tipos = findTiposByNombre( filters.get( FILTER_NOMBRE_TIPO_ER ) );
				} else {
					tipos = getAllTiposEntidadRegistro();
				}
				resultMap.put( RESULT_TIPOS_ENTIDAD_REGISTRO, tipos );
				response.setResultados( resultMap );
				break;
			case CMD_SAVE_TIPO_ER:
				this.insertTipoEr( webRequest, response );
				break;
			case CMD_SAVE_ENTIDAD_REGISTRO:
				this.insertEr( webRequest, response );
				break;
			case CMD_UPDATE_TIPO_ER:
				this.updateTipoEr( webRequest, response );
				break;
			case CMD_UPDATE_ENTIDAD_REGISTRO:
				this.udpateEr( webRequest, response );
				break;
			case CMD_DELETE_TIPO_ER:
				this.deleteTipoEr( webRequest, response );
				break;
			case CMD_DELETE_ENTIDAD_REGISTRO:
				this.deleteEr( webRequest, response );
				break;
		}
		return response;
	}

	private List< EntidadRegistroData > getAllEntidadRegistro( Map< String, String > filters ) throws SIBBACBusinessException {
		List< EntidadRegistroData > entidades = null;
		entidades = erBo.findAllData( turnEmptyStringIntoNull( filters.get( FILTER_NOMBRE_ER ) ),
				turnEmptyStringIntoNull( filters.get( FILTER_BIC_ER ) ), getIdTipoEr( filters.get( FILTER_ID_TIPO_ER_ER ) ) );
		return entidades;
	}

	private List< Tmct0TipoEr > findTiposByNombre( String nombre ) {
		return tipoErBo.findByNombre( nombre );
	}

	private void udpateEr( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		// comprueba que los parametros han sido enviados
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los parámetros de llamada." );
		}
		final Long idEr = getIdEr( filters.get( FILTER_ID_ER ) );
		final String nombre = filters.get( FILTER_NOMBRE_TIPO_ER );
		final String bic = filters.get( FILTER_BIC_ER );
		final Integer idTipoEr = getIdTipoEr( filters.get( FILTER_ID_TIPO_ER_ER ) );
		final Map< String, String > resultados = new HashMap< String, String >();
		boolean updated = erBo.updateById( idEr, nombre, bic, idTipoEr );
		if ( updated ) {
			resultados.put( RESULTADO, RESULTADO_SUCCESS );
			LOG.trace( " Se realiza la actualizacion del ER" );
		} else {
			resultados.put( RESULTADO, RESULTADO_ERROR );
			LOG.trace( " Error actualizando ER" );
		}
		webResponse.setResultados( resultados );
	}

	private void updateTipoEr( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		// comprueba que los parametros han sido enviados
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los parámetros de llamada." );
		}
		final Integer idTipoEr = getIdTipoEr( filters.get( FILTER_ID_TIPO_ER ) );
		final String nombre = filters.get( FILTER_NOMBRE_TIPO_ER );
		final Map< String, String > resultados = new HashMap< String, String >();
		boolean updated = tipoErBo.udpateById( idTipoEr, nombre );
		if ( updated ) {
			resultados.put( RESULTADO, RESULTADO_SUCCESS );
			LOG.trace( " Se realiza la actualizacion del tipoER" );
		} else {
			resultados.put( RESULTADO, RESULTADO_ERROR );
			LOG.trace( " Error actualizando tipoER" );
		}
		webResponse.setResultados( resultados );
	}

	private void deleteEr( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		// comprueba que los parametros han sido enviados
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los parámetros de llamada." );
		}
		final Long idEr = getIdEr( filters.get( FILTER_ID_ER ) );
		final Map< String, String > resultados = new HashMap< String, String >();
		boolean deleted = erBo.deleteById( idEr );
		if ( deleted ) {
			resultados.put( RESULTADO, RESULTADO_SUCCESS );
			LOG.trace( " Se realiza el borrado del ER" );
		} else {
			resultados.put( RESULTADO, RESULTADO_ERROR );
			LOG.trace( " Error borrando ER" );
		}
		webResponse.setResultados( resultados );
	}

	private void deleteTipoEr( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		// comprueba que los parametros han sido enviados
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los parámetros de llamada." );
		}
		final Integer idTipoEr = getIdTipoEr( filters.get( FILTER_ID_TIPO_ER ) );
		final Map< String, String > resultados = new HashMap< String, String >();
		boolean deleted = tipoErBo.deleteById( idTipoEr );
		if ( deleted ) {
			resultados.put( RESULTADO, RESULTADO_SUCCESS );
			LOG.trace( " Se realiza el borrado del tipoER" );
		} else {
			resultados.put( RESULTADO, RESULTADO_ERROR );
			LOG.trace( " Error borrando tipoER" );
		}
		webResponse.setResultados( resultados );
	}

	private void insertTipoEr( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		// comprueba que los parametros han sido enviados
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los parámetros de llamada." );
		}
		// final Integer idTipoEr = getIdTipoEr( filters.get( FILTER_ID_TIPO_ER ) );
		final String nombre = filters.get( FILTER_NOMBRE_TIPO_ER );
		final Map< String, String > resultados = new HashMap< String, String >();
		Tmct0TipoEr inserted = tipoErBo.insertTipoEr( nombre );
		resultados.put( RESULTADO, RESULTADO_SUCCESS );
		webResponse.setResultados( resultados );
		LOG.trace( " Se realiza la insercion del tipoER con id " + inserted.getId() );
	}

	private void insertEr( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		// comprueba que los parametros han sido enviados
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "Por favor indique los parámetros de llamada." );
		}
		final String nombre = filters.get( FILTER_NOMBRE_TIPO_ER );
		final String bic = filters.get( FILTER_BIC_ER );
		final Integer idTipoEr = getIdTipoEr( filters.get( FILTER_ID_TIPO_ER_ER ) );
		final Map< String, String > resultados = new HashMap< String, String >();
		Tmct0EntidadRegistro inserted = erBo.insertEr( nombre, bic, idTipoEr );
		resultados.put( RESULTADO, RESULTADO_SUCCESS );
		webResponse.setResultados( resultados );
		LOG.trace( " Se realiza la insercion del ER con id " + inserted.getId() );
	}

	private List< EntidadRegistroData > getAllEntidadRegistro() {
		List< EntidadRegistroData > entidades = null;
		entidades = erBo.findAllData();
		return entidades;
	}

	private List< Tmct0TipoEr > getAllTiposEntidadRegistro() {
		List< Tmct0TipoEr > resultList = null;
		resultList = tipoErBo.findAll();
		return resultList;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new LinkedList< String >();
		commands.add( CMD_GET_ALL_ENTIDAD_REGISTRO );
		commands.add( CMD_GET_ALL_TIPO_ER );
		return commands;
	}

	@Override
	public List< String > getFields() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param idTipoEr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Integer getIdTipoEr( final String idTipoEr ) throws SIBBACBusinessException {
		Integer idTipoErInt = null;
		try {
			if ( StringUtils.isNotBlank( idTipoEr ) ) {
				idTipoErInt = NumberUtils.createInteger( idTipoEr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idTipoEr\": [" + idTipoEr + "] " );
		}
		return idTipoErInt;
	}

	/**
	 * @param idEr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdEr( final String idEr ) throws SIBBACBusinessException {
		Long idErLong = null;
		try {
			if ( StringUtils.isNotBlank( idEr ) ) {
				idErLong = NumberUtils.createLong( idEr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idTipoEr\": [" + idEr + "] " );
		}
		return idErLong;
	}

	private String turnEmptyStringIntoNull( String string ) {
		String fullOrNullString = null;
		if ( string != null && !string.equals( "" ) ) {
			fullOrNullString = string;
		}
		return fullOrNullString;
	}
}
