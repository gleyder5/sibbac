package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.NumeradoresBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class SIBBACServiceNumeradores.
 */
@SIBBACService
public class SIBBACServiceNumeradores implements SIBBACServiceBean {

	/** The numeradores bo. */
	@Autowired
	private NumeradoresBo		numeradoresBo;

	/** The Constant LOG. */
	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServiceNumeradores.class );

	public SIBBACServiceNumeradores() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.debug( "[SIBBACServiceNumeradores::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.debug( "[SIBBACServiceNumeradores::process] " + "+ Command.: [{}]", webRequest.getAction() );

		switch ( webRequest.getAction() ) {
			case Constantes.CMD_CREATE_NUMERADORES:
				// comprueba que los parametros han sido enviados
				if ( params == null ) {
					webResponse.setError( "Por favor indique los filtros de búsqueda." );
					return webResponse;
				}

				try {
					Map< String, String > resultado = numeradoresBo.createNumeradores( params );
					webResponse.setResultados( resultado );
				} catch ( SIBBACBusinessException e ) {
					webResponse.setError( e.getMessage() );
				}

				break;
			case Constantes.CMD_GET_PARAM_NUMERADORES:
				webResponse.setResultados( numeradoresBo.getListaNumeradores() );
				break;
			default:
				webResponse.setError( "No recibe ningun comando" );
				LOG.error( "No le llega ningun comando" );
				break;
		}

		return webResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_CREATE_NUMERADORES );
		commands.add( Constantes.CMD_GET_PARAM_NUMERADORES );

		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		fields.add( "id" );
		fields.add( "inicio" );
		fields.add( "fin" );
		fields.add( "identificadoresId" );
		fields.add( "siguiente" );
		fields.add( "tipoDeDocumentoId" );
		return fields;
	}

	/*
	 * 
	 * 
	 * Map<String, Boolean> resultado = new HashMap<String, Boolean>();
	 * 
	 * Boolean result = false;
	 * 
	 * if (numeradoresBo.save(numeradores) != null) {
	 * result = true;
	 * }
	 * resultado.put("result", result);
	 * return resultado;
	 * }
	 */

	/*
	 * private Map<String, Object> getNumeradores() {
	 * LOG.trace("Recuperamos todos los Numeradores");
	 * Map<String, Object> map = new HashMap<String, Object>();
	 * List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	 * Map<String, Object> bean;
	 * 
	 * map.put("lista_numeradores", list);
	 * for (Numeradores numeradores : numeradoresBo.findAll()) {
	 * list.add(bean = new HashMap<String, Object>());
	 * bean.put("id", numeradores.getId());
	 * bean.put("inicio", numeradores.getInicio());
	 * bean.put("fin", numeradores.getFin());
	 * bean.put("siguiente", numeradores.getSiguiente());
	 * bean.put("identificador", numeradores.getIdentificador());
	 * bean.put("tipo de documento", numeradores.getTipoDeDocumento());
	 * }
	 * return list;
	 * }
	 */

	/*
	 * public Map<String, Numeradores> getNumeradores() {
	 * 
	 * Map<String, Numeradores> resultados = new HashMap<String, Numeradores>();
	 * LOG.trace("Recuperamos todos los Numeradores");
	 * 
	 * List<Numeradores> listaNumeradores = (List<Numeradores>)
	 * numeradoresBo.findAll();
	 * 
	 * for (Numeradores numeradores : listaNumeradores) {
	 * 
	 * 
	 * resultados.put(numeradores.getId().toString(), numeradores); }
	 * 
	 * return resultados; }
	 */

}
