package sibbac.business.wrappers.database.dto;


public class IdentificadoresDTO {

	private Long	id;
	private Long	indentificadores;

	public IdentificadoresDTO() {

	}

	public IdentificadoresDTO( Long id, Long indentificadores ) {
		super();
		this.id = id;
		this.indentificadores = indentificadores;
	}

	public Long getId() {
		return id;
	}

	public Long getIndentificadores() {
		return indentificadores;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setIndentificadores( Long indentificadores ) {
		this.indentificadores = indentificadores;
	}

}
