package sibbac.business.wrappers.database.bo.partenonReader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.common.fileReader.ValidationHelper;
import sibbac.business.wrappers.database.bo.Tmct0swiBo;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.RecordBean.TipoDocumento;
import sibbac.business.wrappers.database.model.RecordBean.TipoTitular;
import sibbac.business.wrappers.database.model.Tmct0paises;

@Component
public class TitularesExternalValidator {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String VALID_ASCII_CHARS = " &'()*,-.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzªºÀÁÂÄÇÈÉÊËÌÍÎÏÑÒÓÔÖÙÚÛÜàáâäçèéêëìíîïñòóôöùúûü";

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TitularesExternalValidator.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0PROVINCIAS</code>. */

  @Autowired
  private Tmct0swiBo swiBo;

  /** Objeto para el acceso a la tabla <code>TMCT0PAISES</code>. */

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  public boolean validateAll(String erroresNumorden, List<PartenonRecordBean> bloqueMismaOrden,
      Map<String, Tmct0paises> isosPaises, Map<String, Tmct0paises> isosPaiseshacienda,
      Map<String, List<String>> nombresCiudadPorDistrito, Map<PartenonRecordBean, List<String>> errorsByBean) {
    boolean hasErrors = StringUtils.isNotBlank(erroresNumorden);
    // Comprobamos todos los errores
    for (PartenonRecordBean bean : bloqueMismaOrden) {
      // Se comprueban los errores internos
      List<String> errors = validate(bean);

      for (String externalError : validate(bean, isosPaises, isosPaiseshacienda, nombresCiudadPorDistrito)) {
        if (!errors.contains(externalError)) {
          errors.add(externalError);
        }
      }
      // for (String externalError : externalValidator.validate(bean,
      // codPaises, codNacionalidades, codProvincias)) {
      for (String externalError : validatePaisesProvincias(bean, isosPaises, isosPaiseshacienda,
          nombresCiudadPorDistrito)) {
        if (!errors.contains(externalError)) {
          errors.add(externalError);
        }
      }
      if (errors.size() > 0) {
        hasErrors = true;
      }
      if (erroresNumorden != null) {
        errors.add(erroresNumorden);
      }

      errorsByBean.put(bean, errors);
    }

    // Comprobamos los errores de grupo
    boolean hasErrorsGrupo = validateGroup(bloqueMismaOrden, errorsByBean);
    hasErrors = hasErrorsGrupo || hasErrors;

    if (hasErrors && bloqueMismaOrden.size() > 1) {
      List<String> erros = new ArrayList<String>();
      erros.add(ValidationHelper.CodError.ERROROTROTIT.text);
      for (PartenonRecordBean key : errorsByBean.keySet()) {
        if (CollectionUtils.isEmpty(errorsByBean.get(key))) {
          errorsByBean.put(key, erros);
        }
      }
    }
    return hasErrors;
  }

  /**
   * 
   * @param bean
   * @return
   */
  public List<String> validate(PartenonRecordBean bean, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaiseshacienda, Map<String, List<String>> nombresCiudadPorDistrito) {
    List<String> errorCodes = new ArrayList<String>();
    try {
      // Validacion pais nacionalidad
      Tmct0paises paisResidencia = isosPaiseshacienda.get(bean.getPaisResidencia());
      Tmct0paises paisNacionalidad = isosPaises.get(bean.getNacionalidad());

      if (paisResidencia == null) {
        errorCodes.add(ValidationHelper.CodError.PAIS_RES_INCORRECTO.text);
      }
      // Validacion pais residencia
      if (paisNacionalidad == null) {
        errorCodes.add(ValidationHelper.CodError.PAIS.text);
      }

      // 37- Comprobación de si es un código BIC válido de SWIFT
      if (TipoDocumento.BIC.getTipoDocumento().equals(bean.getTipoDocumento()) && bean.getNifbic() != null) {
        if (this.swiBo.findById(bean.getNifbic()) == null) {
          errorCodes.add(ValidationHelper.CodError.BIC_INEXISTENTE.text);
        }
        else {
          // 38- Comprobación de coherencia del código BIC con el país
          // de
          // nacionalidad
          if (paisNacionalidad != null) {
            String iso2po = paisNacionalidad.getId().getCdisoalf2();
            if (!bean.getNifbic().substring(4, 6).equals(iso2po.trim())) {
              errorCodes.add(ValidationHelper.CodError.BIC_FORMATO_PAISES.text);
            }
          }
        }
      }

      // Validacion codigo postal ANTIGUA
      // 50- Comprobación de coherencia de la provincia recibida en el
      // código
      // postal recibido para un domicilio de España.
      if (StringUtils.isNotEmpty(bean.getPaisResidencia())) {
        if (bean.getDistrito() == null
            || (("011".equals(bean.getPaisResidencia()) || "042".equals(bean.getPaisResidencia())) && (StringUtils
                .isNotBlank(bean.getDistrito()) && bean.getDistrito().length() == 5 && CollectionUtils
                  .isEmpty(nombresCiudadPorDistrito.get(bean.getDistrito().trim().substring(0, 2)))))) {
          errorCodes.add(ValidationHelper.CodError.COHERENCIA_CP_CODIGO_PROVINCIA.text);
        }
      }
    }
    catch (Exception e) {
      LOG.error("[validate] Error en validaciones externas:" + e.getMessage(), e);
    }
    return errorCodes;
  } // validate

  /**
   * 
   * @param bean
   * @param codPaises
   * @param codNacionalidades
   * @param codProvincias
   * @return
   */
  // public List<String> validate(PartenonRecordBean bean,
  // List<String> codPaises,
  // List<String> codNacionalidades,
  // Map<String, String> codProvincias) {
  public List<String> validatePaisesProvincias(PartenonRecordBean bean, Map<String, Tmct0paises> isosPaises,
      Map<String, Tmct0paises> isosPaiseshacienda, Map<String, List<String>> nombresCiudadPorDistrito) {
    LOG.trace("[validate] Inicio ...");
    List<String> errorCodes = new ArrayList<String>();
    Tmct0paises paisNacionalidad = isosPaises.get(bean.getNacionalidad());
    Tmct0paises paisResidencia = isosPaiseshacienda.get(bean.getPaisResidencia());
    // Validación país de nacionalidad
    LOG.trace("[validate] Validando pais de residencia ... " + bean.getPaisResidencia());
    try {
      if (paisResidencia == null) {
        errorCodes.add(ValidationHelper.CodError.PAIS_RES_INCORRECTO.text);
      } // if
    }
    catch (Exception e) {
      LOG.error("[validate] Error en validación país nacionalidad:" + e.getMessage(), e);
    } // catch

    // Validacion país de residencia
    LOG.trace("[validate] Validando pais de nacionalidad ... " + bean.getNacionalidad());
    try {
      if (paisNacionalidad == null) {
        errorCodes.add(ValidationHelper.CodError.PAIS.text);
      } // if
    }
    catch (Exception e) {
      LOG.error("[validate] Error en validación país residencia:" + e.getMessage(), e);
    } // catch

    // 37- Comprobación de si es un código BIC válido de SWIFT
    LOG.trace("[validate] Comprobando si es un código BIC valido de SWIFT ...");
    try {
      if (TipoDocumento.BIC.getTipoDocumento().equals(bean.getTipoDocumento()) && bean.getNifbic() != null) {
        if (this.swiBo.findById(bean.getNifbic()) == null) {
          errorCodes.add(ValidationHelper.CodError.BIC_INEXISTENTE.text);
        }
        else {

          String iso2po = null;
          if (paisNacionalidad != null) {
            iso2po = paisNacionalidad.getId().getCdisoalf2();
          }

          // 38- Comprobación de coherencia del código BIC con el país de
          // nacionalidad
          LOG.trace("[validate] Comprobando coherencia del codigo BIC con el pais ...");
          try {
            if (iso2po != null) {

              // String iso2po = paisNacionalidad.getCdiso2po();
              // String iso2po = paisNacionalidad.getId().getCdisoalf2();
              LOG.trace("[validate] iso2po: " + iso2po);

              if (!bean.getNifbic().substring(4, 6).equals(iso2po.trim())) {
                errorCodes.add(ValidationHelper.CodError.BIC_FORMATO_PAISES.text);
              } // if
            } // if
          }
          catch (Exception e) {
            LOG.warn(
                "[validate] Incidencia comprobando la coherencia del codigo BIC con el pais ... " + e.getMessage(), e);
          } // catch
        } // else
      } // if
        // (TipoDocumento.BIC.getTipoDocumento().equals(bean.getTipoDocumento())
        // && bean.getCodigoBic() != null)
    }
    catch (Exception e) {
      LOG.warn("[validate] Incidenciar comprobando si es un codigo BIC válido de SWIFT ... " + e.getMessage(), e);
    } // catch

    // Validacion codigo postal ANTIGUA
    // 50- Comprobación de coherencia de la provincia recibida en el código
    // postal recibido para un domicilio de España
    LOG.trace("[validate]  Comprobando codigo postal ...");
    try {

      if (StringUtils.isNotBlank(bean.getPaisResidencia())) {

        if (bean.getDistrito() == null
            || (("011".equals(bean.getPaisResidencia()) || "042".equals(bean.getPaisResidencia())) && (StringUtils
                .isNotEmpty(bean.getDistrito()) && bean.getDistrito().length() == 5 && !nombresCiudadPorDistrito
                  .containsKey(bean.getDistrito().trim().substring(0, 2))))) {
          errorCodes.add(ValidationHelper.CodError.COHERENCIA_CP_CODIGO_PROVINCIA.text);
        }
        else if (("011".equals(bean.getPaisResidencia()) || "042".equals(bean.getPaisResidencia()))
            && bean.getDistrito() != null
            && bean.getDistrito().length() >= 2
            && bean.getProvincia() != null
            && nombresCiudadPorDistrito.get(bean.getDistrito().trim().substring(0, 2)) != null
            && !nombresCiudadPorDistrito.get(bean.getDistrito().trim().substring(0, 2)).contains(
                bean.getProvincia().trim())) {

          // 51- Comprobación de coherencia entre el código postal y la
          // provincia recibida
          // Antes de poner un error, nos aseguramos de que no esté en
          // "otro nombre"
          // try {
          LOG.trace("[validate]  Comprobando otrosNombres ...");
          errorCodes.add(ValidationHelper.CodError.COHERENCIA_CP_NOMBRE_PROVINCIA.text);
          // // String otrosNombres =
          // provinBo.findById(bean.getDistrito().trim().substring(0,
          // 2)).getNbotronombre();
          //
          // List<String> provincias =
          // nombresCiudadPorDistrito.get(bean.getDistrito().trim().substring(0,
          // 2));
          // if (CollectionUtils.isNotEmpty(provincias)) {
          //
          // boolean coincide = provincias.contains(bean.getProvincia().trim());
          //
          // if (coincide == false) {
          // errorCodes.add(ValidationHelper.CodError.COHERENCIA_CP_NOMBRE_PROVINCIA.text);
          // }
          // } else {
          // errorCodes.add(ValidationHelper.CodError.COHERENCIA_CP_NOMBRE_PROVINCIA.text);
          // }
          //
          // } catch (Exception e) {
          // LOG.error("[validate] Error en Comprobación de coherencia entre el código postal y la provincia recibida:"
          // + e.getMessage(), e);
          // }
        }
        // else if (("011".equals(bean.getPaisResidencia()) ||
        // "042".equals(bean.getPaisResidencia()))) {
        // errorCodes.add(ValidationHelper.CodError.COHERENCIA_CP_NOMBRE_PROVINCIA.text);
        // }
      }
    }
    catch (Exception e) {
      LOG.error("[validate] Error en Comprobaciones de código postal: " + e.getMessage(), e);
    }
    LOG.trace("[validate] Fin");
    return errorCodes;
  }// validate

  /**
   * 
   * @param bloqueMismaOrden
   * @param errorsByBean
   * @return
   */
  public boolean validateGroup(List<PartenonRecordBean> bloqueMismaOrden,
      Map<PartenonRecordBean, List<String>> errorsByBean) {
    LOG.trace("[validateGroup] inicio.");
    List<String> errorCodesParaTodos = new ArrayList<String>();
    boolean hasTitularNormal = false;
    boolean hasErrors = false;
    BigDecimal participacionAcumuladaTitulares = BigDecimal.ZERO;
    BigDecimal participacionAcumuladaUsufructuarios = BigDecimal.ZERO;
    boolean hasUsufructuario = false;
    boolean hasNudoPropietario = false;
    Collection<String> ccvs = new HashSet<String>();
    for (int i = 0; i < bloqueMismaOrden.size(); i++) {
      PartenonRecordBean bean = bloqueMismaOrden.get(i);
      if (bean.getCcv() == null || bean.getCcv().trim().isEmpty()) {
        ccvs.add("");
      }
      else {
        ccvs.add(bean.getCcv());
      }
      if (!hasTitularNormal && !TipoTitular.REPRESENTANTE.getTipoTitular().equals(bean.getTiptitu())
          && !TipoTitular.USUFRUCTUARIO.getTipoTitular().equals(bean.getTiptitu())
          && !TipoTitular.NUDO.getTipoTitular().equals(bean.getTiptitu())) {
        hasTitularNormal = true;
      }

      if (!hasNudoPropietario && TipoTitular.NUDO.getTipoTitular().equals(bean.getTiptitu())) {
        hasNudoPropietario = true;
      }

      if (!TipoTitular.REPRESENTANTE.getTipoTitular().equals(bean.getTiptitu())) {
        if (bean.getParticip() == null) {
          errorsByBean.get(bean).add(ValidationHelper.CodError.PORCENTAJE_ERRONEO.text);
        }
        else if (TipoTitular.USUFRUCTUARIO.getTipoTitular().equals(bean.getTiptitu())) {
          hasUsufructuario = true;
          participacionAcumuladaUsufructuarios = participacionAcumuladaUsufructuarios.add(bean.getParticip());
        }
        else {
          participacionAcumuladaTitulares = participacionAcumuladaTitulares.add(bean.getParticip());
        }

      }

      // 57- Comprobación de identificadores duplicados dentro de los
      // titulares de una misma orden.
      // Si no es el último comprobamos si está repetido más adelante
      if (i + 1 < bloqueMismaOrden.size()) {
        for (int j = i + 1; j < bloqueMismaOrden.size(); j++) {
          PartenonRecordBean beanSiguiente = bloqueMismaOrden.get(j);
          if (!errorsByBean.containsKey(beanSiguiente)
              || !errorsByBean.get(beanSiguiente).contains(ValidationHelper.CodError.IDENTIFICADORES_DUPLICADOS.text)) {
            if (bean.getNifbic() != null && beanSiguiente.getNifbic() != null && bean.getTiptitu() != null
                && beanSiguiente.getTiptitu() != null
                && bean.getNifbic().trim().equals(beanSiguiente.getNifbic().trim())
                && bean.getTiptitu() == beanSiguiente.getTiptitu()) {
              if (nombresDistintos(bean, beanSiguiente)) {
                if (errorsByBean.containsKey(bean)) {
                  errorsByBean.get(bean).add(ValidationHelper.CodError.IDENTIFICADORES_DUPLICADOS.text);
                }
                else {
                  List<String> errores = new ArrayList<String>();
                  errores.add(ValidationHelper.CodError.IDENTIFICADORES_DUPLICADOS.text);
                  errorsByBean.put(bean, errores);
                }
                if (errorsByBean.containsKey(beanSiguiente)) {
                  errorsByBean.get(beanSiguiente).add(ValidationHelper.CodError.IDENTIFICADORES_DUPLICADOS.text);
                }
                else {
                  List<String> errores = new ArrayList<String>();
                  errores.add(ValidationHelper.CodError.IDENTIFICADORES_DUPLICADOS.text);
                  errorsByBean.put(beanSiguiente, errores);
                }
              }
              else if (bean.getTiptitu() != null && beanSiguiente.getTiptitu() != null
                  && bean.getTiptitu().equals(beanSiguiente.getTiptitu())) {
                if (errorsByBean.containsKey(bean)) {
                  errorsByBean.get(bean).add(ValidationHelper.CodError.TITULAR_REPETIDO.text);
                }
                else {
                  List<String> errores = new ArrayList<String>();
                  errores.add(ValidationHelper.CodError.TITULAR_REPETIDO.text);
                  errorsByBean.put(bean, errores);
                }
                if (errorsByBean.containsKey(beanSiguiente)) {
                  errorsByBean.get(beanSiguiente).add(ValidationHelper.CodError.TITULAR_REPETIDO.text);
                }
                else {
                  List<String> errores = new ArrayList<String>();
                  errores.add(ValidationHelper.CodError.TITULAR_REPETIDO.text);
                  errorsByBean.put(beanSiguiente, errores);
                }
              }
            }
          }
        }
      }
    }
    if (ccvs.size() > 1) {
      errorCodesParaTodos.add(ValidationHelper.CodError.DISTINTAS_CCV.text);
    }
    ccvs.clear();
    if (!hasTitularNormal && !hasNudoPropietario) {
      // 58- Comprobación de existencia de algún “titular” entre los
      // titulares recibidos para una misma orden.
      errorCodesParaTodos.add(ValidationHelper.CodError.NO_TITULAR_REAL.text);
    }

    if (participacionAcumuladaTitulares.compareTo(new BigDecimal("99.60")) == -1
        || participacionAcumuladaTitulares.compareTo(new BigDecimal("100")) == 1) {
      // 59- Comprobación del total de porcentajes de titularidad
      // recibidos.
      errorCodesParaTodos.add(ValidationHelper.CodError.PORCENTAJE_ERRONEO.text);
    }

    if (hasNudoPropietario && !hasUsufructuario) {
      errorCodesParaTodos.add(ValidationHelper.CodError.NUDO_PROPIETARIO_SIN_USUFRUCTUARIO.text);
    }
    else if (!hasNudoPropietario && hasUsufructuario) {
      errorCodesParaTodos.add(ValidationHelper.CodError.USUFRUCTUARIO_SIN_NUDO_PROPIETARIO.text);
    }

    if (hasUsufructuario) {
      if (participacionAcumuladaUsufructuarios.compareTo(new BigDecimal("99.60")) == -1
          || participacionAcumuladaUsufructuarios.compareTo(new BigDecimal("100")) == 1) {
        // 59- Comprobación del total de porcentajes de titularidad
        // recibidos.
        errorCodesParaTodos.add(ValidationHelper.CodError.PORCENTAJE_ERRONEO.text);
      }
    }

    // Metemos los errores en el mapeo
    if (errorCodesParaTodos.size() > 0) {
      hasErrors = true;
      for (PartenonRecordBean bean : bloqueMismaOrden) {
        if (errorsByBean.containsKey(bean)) {
          errorsByBean.get(bean).addAll(errorCodesParaTodos);
        }
        else {
          errorsByBean.put(bean, errorCodesParaTodos);
        }
      }
    }
    LOG.trace("[validateGroup] fin == {}.", hasErrors);
    return hasErrors;
  } // validateGroup

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * 
   * @param bean
   * @param beanSiguiente
   * @return
   */
  private boolean nombresDistintos(PartenonRecordBean bean, PartenonRecordBean beanSiguiente) {
    boolean sonDistintos = false;
    if (bean.getNbclient() != null && beanSiguiente.getNbclient() != null) {
      if (!bean.getNbclient().trim().equals(beanSiguiente.getNbclient().trim())) {
        sonDistintos = true;
      }
    }
    if (!sonDistintos && bean.getNbclient1() != null && beanSiguiente.getNbclient1() != null) {
      if (!bean.getNbclient1().trim().equals(beanSiguiente.getNbclient1().trim())) {
        sonDistintos = true;
      }
    }
    if (!sonDistintos && bean.getNbclient2() != null && beanSiguiente.getNbclient2() != null) {
      if (!bean.getNbclient2().trim().equals(beanSiguiente.getNbclient2().trim())) {
        sonDistintos = true;
      }
    }
    return sonDistintos;
  } // nombresDistintos

  public List<String> validate(PartenonRecordBean bean) {
    return validate(bean, VALID_ASCII_CHARS);
  }

  public List<String> validate(PartenonRecordBean bean, String validChars) {
    List<String> errorCodes = new ArrayList<String>();
    boolean domicilioVeniaNull = false;

    bean.sustituirCodigoPostalExtranjero();

    if (bean.getDomicili() == null) {
      domicilioVeniaNull = true;
      // Se une domicilio solamente para las validaciones
      if (bean.getTpdomici() != null && bean.getNbdomici() != null) {
        bean.setDomicili(bean.getTpdomici() + " " + bean.getNbdomici());
      }
      else if (bean.getNbdomici() != null) {
        bean.setDomicili(bean.getNbdomici());
      }
    }

    addError(errorCodes, ValidationHelper.comprobarNoInformados(bean));

    addError(errorCodes, ValidationHelper.comprobarMalInformados(bean));

    // if(StringUtils.isNotBlank(validChars)){
    // addError(errorCodes, ValidationHelper.validarErroresAscii(validChars,
    // this));
    // }

    if (domicilioVeniaNull) {
      bean.setDomicili(null);
    }
    return errorCodes;
  }

  public void addError(List<String> errorList, List<String> errCodes) {
    if (errCodes != null && errCodes.size() > 0) {
      errorList.addAll(errCodes);
    }
  }

} // TitularesExternalValidator
