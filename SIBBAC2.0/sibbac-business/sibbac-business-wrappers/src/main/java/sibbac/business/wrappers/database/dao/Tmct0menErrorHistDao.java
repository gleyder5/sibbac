package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.database.model.Tmct0menErrorHist;
import sibbac.business.wrappers.database.model.Tmct0sta;

import java.util.Date;

@Repository
public interface Tmct0menErrorHistDao extends JpaRepository<Tmct0menErrorHist, String> {

  Tmct0men findByCdmensa(final String cdmensa);

  Tmct0men findByCdmensaAndTmct0sta(String cdmensa, Tmct0sta estado);

  Tmct0menErrorHist findFirstByOrderByIdDesc();

  Tmct0menErrorHist findByFhauditAndCdmensaAndTmct0sta(Date currentDate,Tmct0men cdmensa, Tmct0sta estado);

}
