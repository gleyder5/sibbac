package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.ContactoBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceContacto implements SIBBACServiceBean {

	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceContacto.class);

	@Autowired
	private ContactoBo contactoBO;

	public SIBBACServiceContacto() {
		LOG.trace("[SIBBACServiceContacto::<constructor>] Initiating SIBBAC Service for \"Contacto\".");
	}

	@Override
	public WebResponse process(WebRequest webRequest) {

		LOG.trace("[SIBBACServiceContacto::process] Processing a web request...");

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map<String, String> params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.trace("[SIBBACServiceContacto::process] Command.: [{}]", webRequest.getAction());

		// selecciona la accion
		switch (webRequest.getAction()) {
		case Constantes.CMD_ALTA_CONTACTO:

			// comprueba que los parametros han sido enviados
			if (params == null) {
				webResponse.setError("Por favor indique los filtros de búsqueda.");
				return webResponse;
			}

			// Hace el set en el webresponse del resultado de introducir en
			// tabla contacto, un nuevo contacto
			try {
				webResponse.setResultados(contactoBO.altaContacto(params));
			} catch (Exception e1) {
				webResponse.setError(e1.getMessage() + " " + e1.getLocalizedMessage() + " " + e1.getCause());
				LOG.error(e1.getMessage(), e1);
			}
			LOG.trace("Rellenado el WebResponse con el alta contacto");

			break;

		case Constantes.CMD_BAJA_CONTACTO:

			// comprueba que los parametros han sido enviados
			if (params == null) {
				webResponse.setError("Por favor indique los filtros de búsqueda.");
				return webResponse;
			}

			// Hace el set en el webresponse del resultado de borrar en tabla
			// contacto, un contacto
			try {
				Map<String, String> resultado = contactoBO.bajaContacto(params);
				webResponse.setResultados(resultado);
			} catch (SIBBACBusinessException e) {
				webResponse.setError(e.getMessage());
			}

			LOG.trace("Rellenado el WebResponse con la baja del contacto");

			break;

		case Constantes.CMD_MODIFICACION_CONTACTO:

			// comprueba que los parametros han sido enviados
			if (params == null) {
				webResponse.setError("Por favor indique los filtros de búsqueda.");
				return webResponse;
			}

			// HAce el set en el webresponse del resultado de modificar un
			// contacto en la tabla contacto.
			webResponse.setResultados(contactoBO.modificacionContacto(params));
			LOG.trace("Rellenado el WebResponse con la modificacion del contacto");

			break;

		case Constantes.CMD_CAMBIOSTATUS:

			// comprueba que los parametros han sido enviados
			if (params == null) {
				webResponse.setError("Por favor indique los filtros de búsqueda.");
				return webResponse;
			}

			webResponse.setResultados(contactoBO.cambioStatus(params));
			LOG.trace("Rellenado el WebResponse con el cambio de status del contacto");

			break;

		case Constantes.CMD_GETLISTA_CONTACTOS:

			// Hace el set de la devolucion de todos los contactos de la tabla
			// contacto
			webResponse.setResultados(contactoBO.getListaContactos(params));
			LOG.trace("Rellenado el WebResponse con la lista de contactos");

			break;

		default:

			webResponse.setError("Comando incorrecto");
			LOG.error("No le llega ningun comando correcto");

			break;

		}

		return webResponse;
	}

	@Override
	public List<String> getAvailableCommands() {
		List<String> commands = new ArrayList<String>();
		commands.add(Constantes.CMD_ALTA_CONTACTO);
		commands.add(Constantes.CMD_BAJA_CONTACTO);
		commands.add(Constantes.CMD_MODIFICACION_CONTACTO);
		commands.add(Constantes.CMD_CAMBIOSTATUS);
		commands.add(Constantes.CMD_GETLISTA_CONTACTOS);
		return commands;
	}

	@Override
	public List<String> getFields() {
		List<String> fields = new LinkedList<String>();
		fields.add("id");
		fields.add("aliasId");
		fields.add("descripcion");
		fields.add("telefono1");
		fields.add("telefono2");
		fields.add("movil");
		fields.add("fax");
		fields.add("email");
		fields.add("comentarios");
		fields.add("posicionCargo");
		fields.add("activo");
		fields.add("nombre");
		fields.add("apellido1");
		fields.add("apellido2");
		fields.add("defecto");
		fields.add("lista");
		return fields;
	}
}
