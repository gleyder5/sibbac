package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Clase para agrupar objetos del tipo <code>Alc2FailDto</code> por los campos de la clase.
 * 
 * @version 1.0
 * @author XI316153
 */
public class Alc2FailWithAliasKey implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -1046976859801864528L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Fecha de liquidación. */
  private Date fechaLiquidacion;

  /** Código del isin. */
  private String isinCode;

  /** Cuenta de compensación. */
  private String cuentaCompensacion;

  /** Código del alias. */
  private String aliasCode;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Inicializa los atributos de la clase.
   * 
   * @param Alc2FailDto Objetyo del que extraer los datos a informar.
   */
  public Alc2FailWithAliasKey(Alc2FailDto alc2FailDto) {
    this.fechaLiquidacion = alc2FailDto.getFeopeliq();
    this.aliasCode = alc2FailDto.getCdAlias();
    this.isinCode = alc2FailDto.getCdIsin();
    this.cuentaCompensacion = alc2FailDto.getCdCuentaCompensacion();
  } // Alc2FailWithAliasKey

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Alc2FailWithAliasKey [fechaLiquidacion=" + fechaLiquidacion + ", isinCode=" + isinCode + ", cuentaCompensacion=" + cuentaCompensacion
           + ", aliasCode=" + aliasCode + "]";
  } // toString

  /*
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((aliasCode == null) ? 0 : aliasCode.hashCode());
    result = prime * result + ((cuentaCompensacion == null) ? 0 : cuentaCompensacion.hashCode());
    result = prime * result + ((fechaLiquidacion == null) ? 0 : fechaLiquidacion.hashCode());
    result = prime * result + ((isinCode == null) ? 0 : isinCode.hashCode());
    return result;
  } // hashCode

  /*
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Alc2FailWithAliasKey other = (Alc2FailWithAliasKey) obj;
    if (aliasCode == null) {
      if (other.aliasCode != null)
        return false;
    } else if (!aliasCode.equals(other.aliasCode))
      return false;
    if (cuentaCompensacion == null) {
      if (other.cuentaCompensacion != null)
        return false;
    } else if (!cuentaCompensacion.equals(other.cuentaCompensacion))
      return false;
    if (fechaLiquidacion == null) {
      if (other.fechaLiquidacion != null)
        return false;
    } else if (!fechaLiquidacion.equals(other.fechaLiquidacion))
      return false;
    if (isinCode == null) {
      if (other.isinCode != null)
        return false;
    } else if (!isinCode.equals(other.isinCode))
      return false;
    return true;
  } // equals

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the fechaLiquidacion
   */
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  /**
   * @return the isinCode
   */
  public String getIsinCode() {
    return isinCode;
  }

  /**
   * @return the cuentaCompensacion
   */
  public String getCuentaCompensacion() {
    return cuentaCompensacion;
  }

  /**
   * @return the aliasCode
   */
  public String getAliasCode() {
    return aliasCode;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param fechaLiquidacion the fechaLiquidacion to set
   */
  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  /**
   * @param isinCode the isinCode to set
   */
  public void setIsinCode(String isinCode) {
    this.isinCode = isinCode;
  }

  /**
   * @param cuentaCompensacion the cuentaCompensacion to set
   */
  public void setCuentaCompensacion(String cuentaCompensacion) {
    this.cuentaCompensacion = cuentaCompensacion;
  }

  /**
   * @param aliasCode the aliasCode to set
   */
  public void setAliasCode(String aliasCode) {
    this.aliasCode = aliasCode;
  }

} // Alc2FailDto
