package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.AlcOrdenMercado;

/**
 * Data access object de ALC Orden Mercado.
 * 
 * @author Neoris
 *
 */
@Repository
public interface AlcOrdenMercadoDao extends JpaRepository<AlcOrdenMercado, Long> {
  
  @Query(value = "select ao.REFERENCIAS3 from TMCT0_ALC_ORDEN_MERCADO m " +
      "inner join TMCT0_ALC_ORDEN ao ON m.IDALC_ORDEN = ao.ID and ao.ESTADO = 'A' " +
      "where m.IDDESGLOSECAMARA = :idDesgloseCamara and m.ESTADO = 'A'", nativeQuery = true)
  public List<String> referenciaS3activa(@Param("idDesgloseCamara") long idDesgloseCamara);

}