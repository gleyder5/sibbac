package sibbac.business.wrappers.database.bo.fichero1015;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.FlatFileItemWriter;

import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.common.SIBBACBusinessException;

public class EnvioFichero1015Callable implements Callable<Void> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(EnvioFichero1015Callable.class);

  private EnvioFichero1015SubList enviarFichero1015SubList;

  private List<String> codigosComerciales;

  private List<DesgloseRecordDTO> beans;

  private String banco;

  public EnvioFichero1015Callable(String banco, List<String> codigosComerciales, List<DesgloseRecordDTO> beans,
      EnvioFichero1015SubList enviarFichero1015SubList) {
    this.banco = banco;
    this.codigosComerciales = codigosComerciales;
    this.beans = new ArrayList<DesgloseRecordDTO>(beans);
    this.enviarFichero1015SubList = enviarFichero1015SubList;
  }

  public Void call() throws SIBBACBusinessException {
    LOG.trace("[EnvioFichero1015Callable : run] inicio");
    if (CollectionUtils.isNotEmpty(beans)) {

      long numeroEnvio = beans.get(0).getId();
      Path pathFile = enviarFichero1015SubList.getNewTmpFilePath(this.banco, numeroEnvio);

      FlatFileItemWriter<String> writer = enviarFichero1015SubList.initWriter(pathFile);
      try {
        enviarFichero1015SubList.crearFicheroMQ(writer, beans, codigosComerciales);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(e);
      }
      finally {
        enviarFichero1015SubList.closeAndDeleteIfEmpty(pathFile, writer);
        enviarFichero1015SubList.sendEmailAndMoveSendPath(pathFile, this.banco, numeroEnvio);

      }
    }
    LOG.trace("[EnvioFichero1015Callable : run] fin");
    return null;
  }

}
