package sibbac.business.wrappers.common;

import java.util.ArrayList;
import java.util.List;

public class Results {
  
  private int nAfiError;
  
  private int nAfi;

  private boolean bloqueante;

  public List<String> msgs;

  public Results() {
    nAfiError = 0;
    nAfi = 0;
    bloqueante = false;
    msgs = new ArrayList<String>();
  }

  public synchronized void addNAfiErrorSynchronized() {
    nAfiError++;
  }

  public synchronized void addNAfiSynchronized() {
    nAfi++;
  }

  public synchronized void addNAfiErrorSynchronized(int val) {
    nAfiError += val;
  }

  public synchronized void addNAfiSynchronized(int val) {
    nAfi += val;
  }

  public synchronized void addMsg(String msg) {
    msgs.add(msg);
  }

  public synchronized void setBloqueante(boolean bloqueante) {
    this.bloqueante = bloqueante;
  }

  public List<String> getMsgs() {
    return msgs;
  }

  public int getnAfiError() {
    return nAfiError;
  }

  public int getnAfi() {
    return nAfi;
  }

  public boolean isBloqueante() {
    return bloqueante;
  }

  public void addnAfiError(int size) {
    nAfiError += size;
    
  }

  public void addnAfi(int size) {
    nAfi += size;
    
  }


}
