package sibbac.business.wrappers.database.bo;


import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.RelacionCuentaMercadoDao;
import sibbac.business.wrappers.database.data.RelacionCuentaMercadoData;
import sibbac.business.wrappers.database.model.RelacionCuentaMercado;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.database.bo.AbstractBo;


@Service
public class RelacionCuentaMercadoBo extends AbstractBo< RelacionCuentaMercado, Long, RelacionCuentaMercadoDao > {

	public List< RelacionCuentaMercadoData > findAllData() {
		List< RelacionCuentaMercadoData > resultList = new LinkedList< RelacionCuentaMercadoData >();
		resultList.addAll( dao.findAllData() );
		return resultList;
	}
	@Transactional
	public void deleteByIdCuentaLiquidacion( Tmct0CuentaLiquidacion cl ) {
		dao.deleteByIdCuentaLiquidacion( cl );
	}
}
