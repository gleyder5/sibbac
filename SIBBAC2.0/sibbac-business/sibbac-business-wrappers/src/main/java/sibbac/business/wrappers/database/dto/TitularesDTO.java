package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sibbac.business.wrappers.database.model.CodigoError;
import sibbac.business.wrappers.database.model.Tmct0AfiError;
import sibbac.business.wrappers.database.model.Tmct0afi;

public class TitularesDTO {

  private Long id;
  private String nbooking;
  private String nucnfclt;
  private Short nucnfliq;
  private String nbclient;
  private String nbclient1;
  private String nbclient2;
  private String nureford;
  private String ccv;
  private Character cdtpoper;
  private String isin;
  private Character tipdoc;
  private String nudnicif;
  private Character tipopers;
  private String paisres;
  private Character tipnactit;
  private String tpdomici;
  private String nbdomici;
  private String nudomici;
  private Character mercadonacint;
  private BigDecimal particip;
  private String codpostal;
  private String poblacion;
  private String provincia;
  private String nacionalidad;
  private Character tiptitu;
  private BigDecimal nusecuen;
  private String nuorden;
  private BigDecimal nutiteje;

  private String alias;
  private String nombrealias;
  private Date fechacontratacion;
  private String refCliente;

  private Character tptiprep;

  private boolean isAfiError;
  private List<Map<String, String>> errorList;
  /* PARA EL COMPONENTE DATATABLE DE JQUERY */
  private BigInteger DT_RowId; /*
                                * Set the ID property of the TR node for this
                                * row
                                */
  private String DT_RowClass; /* Add the this class to the TR node for this row */

  public TitularesDTO() {
  }

  public TitularesDTO(Tmct0AfiError afiError) {
    this.id = afiError.getId();
    if (afiError.getTmct0alo() != null) {
      this.nbooking = afiError.getTmct0alo().getId().getNbooking();
      this.nucnfclt = afiError.getTmct0alo().getId().getNucnfclt();

      this.isin = afiError.getTmct0alo().getCdisin();
      this.nacionalidad = afiError.getCdnactit();
      this.ccv = afiError.getCcv();
      this.setNureford(afiError.getNureford());
      this.setRefCliente(afiError.getNureford());
      if (afiError.getTmct0alo().getTmct0bok() != null && afiError.getTmct0alo().getTmct0bok().getTmct0ord() != null) {

        this.setCdtpoper(afiError.getTmct0alo().getTmct0bok().getTmct0ord().getCdtpoper());
        this.setMercadonacint(afiError.getTmct0alo().getTmct0bok().getTmct0ord().getCdclsmdo());
        this.nutiteje = afiError.getTmct0alo().getTmct0bok().getTmct0ord().getNutiteje();
      }

      // TODO No estoy feliz con esto
      this.nucnfliq = afiError.getTmct0alo().getTmct0alcs().get(0).getNucnfliq();
    }
    this.nbclient = afiError.getNbclient();
    this.nbclient1 = afiError.getNbclient1();
    this.nbclient2 = afiError.getNbclient2();

    this.tipdoc = afiError.getTpidenti();
    this.nudnicif = afiError.getNudnicif();
    this.tipopers = afiError.getTpsocied();
    this.paisres = afiError.getCddepais();
    this.tpdomici = afiError.getTpdomici();
    this.nbdomici = afiError.getNbdomici();
    this.nudomici = afiError.getNudomici();
    this.particip = afiError.getParticip();
    this.codpostal = afiError.getCdpostal();
    this.poblacion = afiError.getNbciudad();
    this.provincia = afiError.getNbprovin();
    this.tiptitu = afiError.getTptiprep();

    this.isAfiError = true;
    errorList = new ArrayList<Map<String, String>>();
    for (CodigoError error : afiError.getErrors()) {
      Map<String, String> mapeoErrores = new HashMap<String, String>();
      mapeoErrores.put("codigo", error.getCodigo());
      mapeoErrores.put("descripcion", error.getDescripcion());
      errorList.add(mapeoErrores);
    }
  }

  public TitularesDTO(Tmct0afi afi) {

    if (afi.getTmct0alo() != null) {
      this.nbooking = afi.getTmct0alo().getId().getNbooking();
      this.nucnfclt = afi.getTmct0alo().getId().getNucnfclt();
      this.nuorden = afi.getTmct0alo().getId().getNuorden();
      this.nusecuen = afi.getId().getNusecuen();
      this.ccv = afi.getCcv();
      this.isin = afi.getTmct0alo().getCdisin();
      this.nacionalidad = afi.getCdnactit();
      if (afi.getTmct0alo().getTmct0bok() != null && afi.getTmct0alo().getTmct0bok().getTmct0ord() != null) {
        this.setNureford(afi.getTmct0alo().getTmct0bok().getTmct0ord().getNureford());
        this.setCdtpoper(afi.getTmct0alo().getTmct0bok().getTmct0ord().getCdtpoper());
        this.setMercadonacint(afi.getTmct0alo().getTmct0bok().getTmct0ord().getCdclsmdo());
        this.nutiteje = afi.getTmct0alo().getTmct0bok().getTmct0ord().getNutiteje();

      }

      // TODO No estoy feliz con esto
      this.nucnfliq = afi.getTmct0alo().getTmct0alcs().get(0).getNucnfliq();

    }

    this.tipdoc = afi.getTpidenti();
    this.nudnicif = afi.getNudnicif();
    this.tipopers = afi.getTpsocied();
    this.paisres = afi.getCddepais();

    this.nbclient = afi.getNbclient();
    this.nbclient1 = afi.getNbclient1();
    this.nbclient2 = afi.getNbclient2();
    this.tpdomici = afi.getTpdomici();
    this.nbdomici = afi.getNbdomici();
    this.nudomici = afi.getNudomici();
    this.particip = afi.getParticip();
    this.codpostal = afi.getCdpostal();
    this.poblacion = afi.getNbciudad();
    this.provincia = afi.getNbprovin();
    this.tiptitu = afi.getTptiprep();

    this.isAfiError = false;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public boolean isAfiError() {
    return isAfiError;
  }

  public void setAfiError(boolean isAfiError) {
    this.isAfiError = isAfiError;
  }

  public String getNbooking() {
    return nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public Short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public String getNbclient() {
    return nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public String getNbclient1() {
    return nbclient1;
  }

  public void setNbclient1(String nbclient1) {
    this.nbclient1 = nbclient1;
  }

  public String getNbclient2() {
    return nbclient2;
  }

  public void setNbclient2(String nbclient2) {
    this.nbclient2 = nbclient2;
  }

  public String getNureford() {
    return nureford;
  }

  public void setNureford(String nureford) {
    this.nureford = nureford;
  }

  public String getCcv() {
    return ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public String getIsin() {
    return isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public String getNudnicif() {
    return nudnicif;
  }

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  public Character getTipopers() {
    return tipopers;
  }

  public void setTipopers(Character tipopers) {
    this.tipopers = tipopers;
  }

  public Character getTipdoc() {
    return tipdoc;
  }

  public void setTipdoc(Character tipdoc) {
    this.tipdoc = tipdoc;
  }

  public String getPaisres() {
    return paisres;
  }

  public void setPaisres(String paisres) {
    this.paisres = paisres;
  }

  public Character getTipnactit() {
    return tipnactit;
  }

  public void setTipnactit(Character tipnactit) {
    this.tipnactit = tipnactit;
  }

  public String getTpdomici() {
    return tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  public String getNbdomici() {
    return nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  public String getNudomici() {
    return nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  public Character getMercadonacint() {
    return mercadonacint;
  }

  public void setMercadonacint(Character mercadonacint) {
    this.mercadonacint = mercadonacint;
  }

  public BigDecimal getParticip() {
    return particip;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  public String getCodpostal() {
    return codpostal;
  }

  public void setCodpostal(String codpostal) {
    this.codpostal = codpostal;
  }

  public String getPoblacion() {
    return poblacion;
  }

  public void setPoblacion(String poblacion) {
    this.poblacion = poblacion;
  }

  public String getProvincia() {
    return provincia;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public Character getTiptitu() {
    return tiptitu;
  }

  public void setTiptitu(Character tiptitu) {
    this.tiptitu = tiptitu;
  }

  public String getNacionalidad() {
    return nacionalidad;
  }

  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  public BigDecimal getNusecuen() {
    return nusecuen;
  }

  public void setNusecuen(BigDecimal nusecuen) {
    this.nusecuen = nusecuen;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public List<Map<String, String>> getErrorList() {
    return errorList;
  }

  public void setErrorList(List<Map<String, String>> errorList) {
    this.errorList = errorList;
  }

  public Date getFechacontratacion() {
    return fechacontratacion;
  }

  public void setFechacontratacion(Date fechacontratacion) {
    this.fechacontratacion = fechacontratacion;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getNombrealias() {
    return nombrealias;
  }

  public void setNombrealias(String nombrealias) {
    this.nombrealias = nombrealias;
  }

  public BigDecimal getNutiteje() {
    return nutiteje;
  }

  public void setNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje;
  }

  public Character getTptiprep() {
    return tptiprep;
  }

  public void setTptiprep(Character tptiprep) {
    this.tptiprep = tptiprep;
  }

  public String getRefCliente() {
    return refCliente;
  }

  public void setRefCliente(String refCliente) {
    this.refCliente = refCliente;
  }

  public BigInteger getDT_RowId() {
    return DT_RowId;
  }

  public void setDT_RowId(BigInteger dT_RowId) {
    DT_RowId = dT_RowId;
  }

  public String getDT_RowClass() {
    return DT_RowClass;
  }

  public void setDT_RowClass(String dT_RowClass) {
    DT_RowClass = dT_RowClass;
  }

  public void setErrorListFromEntity(Set<CodigoError> errors) {
    errorList = new ArrayList<Map<String, String>>();
    for (CodigoError error : errors) {
      Map<String, String> mapeoErrores = new HashMap<String, String>();
      mapeoErrores.put("codigo", error.getCodigo());
      mapeoErrores.put("descripcion", error.getDescripcion());
      errorList.add(mapeoErrores);
    }
  }

  public HashMap<String, Object> getHashMapExport() {

    HashMap<String, Object> result = new HashMap<String, Object>();
    result.put("Error", "SIN ERRORES");
    if (this.getErrorList() != null && !this.getErrorList().isEmpty()) {
      result.put("Error", this.getErrorList());

    }// if (titularDTO.getErrorList() != null &&
     // !titularDTO.getErrorList().isEmpty())
    result.put("Booking", this.getNbooking() + "/" + this.getNucnfclt() + "/" + this.getNucnfliq());
    result.put("Ref_Cliente", this.getNureford());
    result.put("CCV", this.getCcv());
    result.put("Sentido", "");
    if (this.getCdtpoper() != null) {
      result.put("Sentido", this.getCdtpoper().toString());
    }
    result.put("Isin", this.getIsin());
    result.put("Titulos", "");
    if (this.getNutiteje() != null) {
      result.put("Titulos", this.getNutiteje().toString());
    }
    result.put("Mercado", "");
    if (this.getMercadonacint() != null) {
      result.put("Mercado", this.getMercadonacint().toString());
    }
    result.put("Nombre", this.getNbclient());
    result.put("PrimerApellido", this.getNbclient1());
    result.put("SegundorApellido", this.getNbclient2());
    result.put("T_Documento", "");
    if (this.getTipdoc() != null) {
      result.put("T_Documento", this.getTipdoc().toString());
    }
    result.put("Documento", this.getNudnicif());
    result.put("T_Persona", "");
    if (this.getTipopers() != null) {
      result.put("T_Persona", this.getTipopers().toString());
    }
    result.put("P_Residencia", this.getPaisres());
    result.put("Nacionalidad", "");
    if (this.getTipnactit() != null) {
      result.put("Nacionalidad", this.getTipnactit().toString());
    }
    String sDomicilio = this.getTpdomici();
    if (sDomicilio == null) {
      sDomicilio = "";
    }
    if (this.getNbdomici() != null) {
      sDomicilio += this.getNbdomici();
    }
    if (this.getNudomici() != null) {
      sDomicilio += this.getNudomici();
    }
    result.put("Domicilio", sDomicilio);
    result.put("Poblacion", this.getPoblacion());
    result.put("Provincia", this.getProvincia());
    result.put("C_Postal", this.getCodpostal());
    result.put("T_Titular", "");
    if (this.getTiptitu() != null) {
      result.put("T_Titular", this.getTiptitu().toString());
    }
    result.put("Participacion_Por", "");
    if (this.getParticip() != null) {
      result.put("Participacion_Por", this.getParticip().toString());
    }
    result.put("F_Contratacion", "");
    if (this.getFechacontratacion() != null) {
      result.put("F_Contratacion", this.getFechacontratacion().toString());
    }
    result.put("Alias", this.getAlias());
    result.put("N_Alias", this.getNombrealias());
    return result;
  }

}
