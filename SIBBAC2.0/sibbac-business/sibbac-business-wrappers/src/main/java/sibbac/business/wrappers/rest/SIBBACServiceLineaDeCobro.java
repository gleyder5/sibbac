package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.LineaDeCobroBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceLineaDeCobro implements SIBBACServiceBean {

	private static final Logger	LOG					= LoggerFactory.getLogger( SIBBACServiceCobros.class );
	@Autowired
	private LineaDeCobroBo		lineaDeCobroBo;

	private static final String	CMD_DATOS_FACTURA	= "getFacturaLineaDeCobros";
	private static final String	PARAM_ID_COBROS		= "idCobros";

	public SIBBACServiceLineaDeCobro() {
		LOG.trace( "[ServiceLineaDeCobro::<constructor>] Initiating SIBBAC Service for \"LineaDeCobro\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {

		LOG.trace( "[ServiceLineaDeCobro::process] Processing a web request..." );
		// prepara webResponse
		WebResponse webResponse = new WebResponse();
		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();
		// obtiene comandos(paths)
		LOG.trace( "[ServiceLineaDeCobro::process] Command.: [{}]", webRequest.getAction() );

		// selecciona la accion
		switch ( webRequest.getAction() ) {
			case CMD_DATOS_FACTURA:
				if ( params != null ) {
					webResponse.setResultados( lineaDeCobroBo.getFacturaLineaDeCobros( Long.valueOf( params.get( PARAM_ID_COBROS ) ) ) );
				}
				break;

			default:
				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;
		}

		return webResponse;

	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_DATOS_FACTURA );

		return commands;

	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		return fields;
	}

}
