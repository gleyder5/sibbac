package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.math.BigDecimal;

import sibbac.business.fase0.database.model.Tmct0blq;
import sibbac.business.fase0.database.model.Tmct0blqId;
import sibbac.business.fase0.database.model.Tmct0brk;
import sibbac.business.fase0.database.model.Tmct0brkId;

public class SettlementDataBroker {

  // Datos
  private Tmct0blq tmct0blq = null;
  private Tmct0brk tmct0brk = null;

  // Flags
  private boolean dataIL = false;
  private boolean dataBroker = false;
  private boolean iLMultiple = false;

  public SettlementDataBroker() {
    tmct0blq = new Tmct0blq();
    tmct0blq.setId(new Tmct0blqId());
    tmct0brk = new Tmct0brk();
    tmct0brk.setId(new Tmct0brkId());
    dataIL = false;
    dataBroker = false;
    iLMultiple = false;
    initSettlementData();
  }

  public Tmct0blq getTmct0blq() {
    return tmct0blq;
  }

  public void setTmct0blq(Tmct0blq tmct0blq) {
    this.tmct0blq = tmct0blq;
  }

  public Tmct0brk getTmct0brk() {
    return tmct0brk;
  }

  public void setTmct0brk(Tmct0brk tmct0brk) {
    this.tmct0brk = tmct0brk;
  }

  public boolean isDataIL() {
    return dataIL;
  }

  public void setDataIL(boolean dataIL) {
    this.dataIL = dataIL;
  }

  public boolean isDataBroker() {
    return dataBroker;
  }

  public void setDataBroker(boolean dataBroker) {
    this.dataBroker = dataBroker;
  }

  public boolean isILMultiple() {
    return iLMultiple;
  }

  public void setILMultiple(boolean iLMultiple) {
    this.iLMultiple = iLMultiple;
  }

  private void initSettlementData() {

    tmct0blq.setCdbicid("");
    tmct0blq.setNameid("");
    tmct0blq.setBacbenefi("");
    tmct0blq.setBacglocus("");
    tmct0blq.setBacloccus("");
    tmct0blq.setBcdbenbic("");
    tmct0blq.setBcdbenefi("");
    tmct0blq.setBcdglocus("");
    tmct0blq.setBcdloccus("");
    tmct0blq.setBgcusbic("");
    tmct0blq.setBidbenefi("");
    tmct0blq.setBidglocus("");
    tmct0blq.setBidloccus("");
    tmct0blq.setBlcusbic("");
    tmct0blq.setCdbicmar("");
    tmct0blq.getId().setCdbroker("");
    tmct0blq.getId().setCdmercad("");
    tmct0blq.getId().setCentro("");
    tmct0blq.setCocenliq("");
    tmct0blq.getId().setNumsec(BigDecimal.ZERO);
    tmct0blq.getId().setTpsettle("");
    tmct0blq.setFhfinal(BigDecimal.ZERO);
    tmct0blq.setFhinicio(BigDecimal.ZERO);

    tmct0brk.getId().setCdbroker("");
    tmct0brk.setCdidenti("");
    tmct0brk.setCdopefic(' ');
    tmct0brk.setCdpatfic("");
    tmct0brk.setCdpwdfic("");
    tmct0brk.setCdusufic("");
    tmct0brk.setCdusufic("");
    tmct0brk.setCocenliq("");
    tmct0brk.setImmargen(BigDecimal.ZERO);
    tmct0brk.setInforcon(' ');
    tmct0brk.setInsenfla(' ');
    tmct0brk.setInsenmod(' ');
    tmct0brk.setNbbroker("");
    tmct0brk.setNameid("");
    tmct0brk.setCdbicid("");
    tmct0brk.setNbbroker("");
    tmct0brk.setFidessa(' ');
    tmct0brk.setFhsendfi(0);

  }

}
