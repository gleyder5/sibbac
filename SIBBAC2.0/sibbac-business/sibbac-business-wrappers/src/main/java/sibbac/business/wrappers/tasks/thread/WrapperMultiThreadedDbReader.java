/**
 * 
 */
package sibbac.business.wrappers.tasks.thread;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.database.HibernateCursorItemReader;

import sibbac.business.wrappers.database.model.RecordBean;

/**
 * @author xIS16630
 *
 */
@SuppressWarnings("rawtypes")
public abstract class WrapperMultiThreadedDbReader<T extends RecordBean> extends WrapperMultiThreaded {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(WrapperMultiThreadedDbReader.class);

  protected Iterator<T> beanIterator;
  
  protected HibernateCursorItemReader<T> reader;

  /**
   * 
   */
  public WrapperMultiThreadedDbReader() {
    super();
  }

  @Override
  public boolean hasNextObjectToProcess() throws Exception {
    boolean hasNext = beanIterator != null && beanIterator.hasNext();
    LOG.trace("[WrapperMultireadedDbReader :: hasNextObjectToProcess] hasNext == {}", hasNext);
    return hasNext;
  }

  @Override
  public T getNextObjectToProcess(int lineNumber) throws Exception {
    T object = null;
    if (beanIterator != null) {
      object = beanIterator.next();

    }
    LOG.trace("[WrapperMultireadedDbReader :: getNextObjectToProcess] lineNumber == {} ## next == {}", lineNumber,
              object);
    return object;
  }

  @Override
  public void executeTask() throws Exception {
    super.executeTask();
  }
}
