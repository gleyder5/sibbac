package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0gal;
import sibbac.business.wrappers.database.model.Tmct0galId;

@Repository
public interface Tmct0galDao extends JpaRepository<Tmct0gal, Tmct0galId> {

  @Query("select g from Tmct0gal g where g.id.nuorden = :nuorden and g.id.nbooking = :nbooking and g.id.nuagreje = :nuagreje ")
  List<Tmct0gal> findAllByNuordenAndNbookingAndNuagraje(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nuagreje") String nuagreje);

}
