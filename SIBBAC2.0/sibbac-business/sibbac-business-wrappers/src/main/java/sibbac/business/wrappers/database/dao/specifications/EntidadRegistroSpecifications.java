package sibbac.business.wrappers.database.dao.specifications;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;


public class EntidadRegistroSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( EntidadRegistroSpecifications.class );

	public static Specification< Tmct0EntidadRegistro > nombre( final String nombre ) {
		return new Specification< Tmct0EntidadRegistro >() {

			public Predicate toPredicate( Root< Tmct0EntidadRegistro > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nombrePredicate = null;
				LOG.trace( "[EntidadRegistroSpecifications::nombre] [nombre=={}]", nombre );
				if ( nombre != null ) {
					nombrePredicate = builder.equal( root.< String > get( "nombre" ), nombre );
				}
				return nombrePredicate;
			}
		};
	}
	
	public static Specification< Tmct0EntidadRegistro > bic( final String bic ) {
		return new Specification< Tmct0EntidadRegistro >() {

			public Predicate toPredicate( Root< Tmct0EntidadRegistro > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate bicPredicate = null;
				LOG.trace( "[EntidadRegistroSpecifications::bic] [bic=={}]", bic );
				if ( bic != null ) {
					bicPredicate = builder.equal( root.< String > get( "bic" ), bic );
				}
				return bicPredicate;
			}
		};
	}

	public static Specification< Tmct0EntidadRegistro > tipoEr( final Tmct0TipoEr tipoer ) {
		return new Specification< Tmct0EntidadRegistro >() {

			public Predicate toPredicate( Root< Tmct0EntidadRegistro > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate tipoerPredicate = null;
				LOG.trace( "[EntidadRegistroSpecifications::tipoer] [tipoer=={}]", tipoer );
				if ( tipoer != null ) {
					tipoerPredicate = builder.equal( root.<Tmct0TipoEr> get("tipoer"), tipoer );
				}
				return tipoerPredicate;
			}
		};
	}

	public static Specification< Tmct0EntidadRegistro > listadoFriltrado( final String nombre, final String bic, final Tmct0TipoEr tipoer ) {
		return new Specification< Tmct0EntidadRegistro >() {

			public Predicate toPredicate( Root< Tmct0EntidadRegistro > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0EntidadRegistro > specs = null;
				if ( nombre != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nombre( nombre ) );
					}

				}
				if ( bic != null ) {
					if ( specs == null ) {
						specs = Specifications.where( bic( bic ) );
					} else {
						specs = specs.and( bic( bic ) );
					}

				}
				if ( tipoer != null ) {
					if ( specs == null ) {
						specs = Specifications.where( tipoEr( tipoer ) );
					} else {
						specs = specs.and( tipoEr( tipoer ) );
					}

				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}
}
