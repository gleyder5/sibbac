package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

public class HBB6FieldSetMapper implements FieldSetMapper<HBB6RecordBean> {
  public final static String delimiter = ";";

  public enum HBB6Fields {
    FECHACUADRE("fechaCuadre", 8), TOTTITULARES("totTitulares", 6), TOTDOMICILIOS("totDomicilios", 6), INDICADOR_INICIO_FINAL(
        "incicadorInicioFinal", 1), TIPOENVIO("tipoEnvio", 1), FILLER("filler", 224);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private HBB6Fields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      HBB6Fields[] fields = HBB6Fields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getHBB6Lengths() {
      HBB6Fields[] fields = HBB6Fields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public HBB6RecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    HBB6RecordBean HBB6bean = new HBB6RecordBean();
    HBB6bean.setFechaCuadre(fieldSet.readDate(HBB6Fields.FECHACUADRE.text, "yyyyMMdd"));
    HBB6bean.setTotTitulares(fieldSet.readBigDecimal(HBB6Fields.TOTTITULARES.text));
    HBB6bean.setTotDomicilios(fieldSet.readBigDecimal(HBB6Fields.TOTDOMICILIOS.text));
    HBB6bean.setIncicadorInicioFinal(IndicadorInicioFinalHBB.getIndicador(fieldSet
        .readString(HBB6Fields.INDICADOR_INICIO_FINAL.text)));
    HBB6bean.setTipoEnvio(HBB6RecordBean.TipoEnvio.getTipoEnvio(fieldSet.readBigDecimal(HBB6Fields.TIPOENVIO.text)));
    return HBB6bean;
  }

}