package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.threads.ProcesaListaTmct0swiRunnable;
import sibbac.business.wrappers.database.dao.Tmct0swiDao;
import sibbac.business.wrappers.database.model.Tmct0swi;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0swiBo extends AbstractBo<Tmct0swi, String, Tmct0swiDao> {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0swiBo.class);
  private static final String NOMBRE_BO = "Tmct0swiBo";
  /**
   * Ejecutor de los hilos.
   */
  private ExecutorService executor;

  /**
   * Lista que almacena los hilos.
   */
  final List<ProcesaListaTmct0swiRunnable> misTthreads = new ArrayList<ProcesaListaTmct0swiRunnable>();

  /**
   * Variable que controla si el proceso ha tenido o no errores.
   */
  private int iNumErrores;
  private String sMensajeError = "Error en el proceso, la tabla de códigos swift puede estar incompleta.  Repita el proceso y si el problema persiste, contacte con los equipos de soporte técnico.";

  /** Valor recuperado del propertis con el numero de objetos que procesa cada hilo */
  @Value("${sibbac.wrappers.cargaFicheroSwi.numRegistrosThread}")
  private Integer numRegistrosThread;

  /** Valor recuperado del propertis con el numero de hilos que se ejecutaran a la vez */
  @Value("${sibbac.wrappers.cargaFicheroSwi.numThreadsPool}")
  private Integer numThreadsPool;

  /**
   * Procedimiento que procesa los datos recogidos del fichero swift. Primero los borra todos los registros y luego crea
   * los hilos para generar la insercion de todos los objetos. El proceso controla que algún hilo ha generado algún
   * error e informa al usuario que el proceso no se ha realizado correctamente.
   * 
   * @param datosNuevos - Lista con todo los objetos recuperados del fichero y que se tienen que insertar
   * @throws SIBBACBusinessException
   */
  @Transactional
  public void recargaDatosTabla(List<Tmct0swi> datosNuevos) throws SIBBACBusinessException {
    try {

      // Inicializaciones
      // Se van a crear un pool de creas indicado en la variable. Son el numero de hilos que se ejecutan a la vez.
      executor = Executors.newFixedThreadPool(numThreadsPool);

      // Variable que controla si el proceso general ha tenido errores
      iNumErrores = 0;

      // Creamos un thread para borrar porque sino lo hacemos asi, primero hace los inserts de los hilos y luego el
      // borrado perdiendo datos
      LOG.debug("[" + NOMBRE_BO + " :: Creamos thread de borrado]  ");
      Thread borrado = new Thread(new Runnable() {
        @Override
        public void run() {
          LOG.debug("[" + NOMBRE_BO + " :: Inicio borrado de todos los elementos]  ");
          borrarRegistros();
          LOG.debug("[" + NOMBRE_BO + " :: Fin borrado de todos los elementos]  ");
        }
      });
      borrado.start();
      try {
        borrado.join(); // Sentencia que hace que se espere hasta que acabe el borrado.
      } catch (InterruptedException e1) {
        if (!borrado.interrupted()) {
          borrado.interrupt();
        }
        throw new SIBBACBusinessException(sMensajeError);
      }

      LOG.debug("[" + NOMBRE_BO + " :: Continuo el tratamiento]  ");

      int i = 0;
      int j = 0;

      int paso = numRegistrosThread; // Cada hilo va a procesar el numero de registros indicado en esta variable
      int iNumHilo = 0;

      LOG.debug("[Tmct0swiBo :: recargaDatosTabla] Inicio insercion a traves de hilos");

      // Se recorre la lista principal y se crean sublistas que se van insertado a traves de jobs.
      while (i < datosNuevos.size()) {
        j = i + paso;
        if (j > datosNuevos.size()) {
          j = datosNuevos.size();
        }
        final List<Tmct0swi> subLista = datosNuevos.subList(i, j > datosNuevos.size() ? datosNuevos.size() : j);
        LOG.debug("[" + NOMBRE_BO + " :: Creo la lista con elementos de   " + i + " hasta " + j);

        // Nueva version
        iNumHilo++;
        ejecutarHilo(subLista, iNumHilo);
        i = j; // Se actualiza el paso
      }

      LOG.debug("[Tmct0swiBo :: recargaDatosTabla] Numero de threads creados - " + misTthreads.size());

      while (misTthreads.size() > 0) {
        LOG.info("******{}::{} procesando hilos, quedan " + misTthreads.size());
        try {
          Thread.sleep(5000); // Espero 5 segundos
        } catch (InterruptedException e) {
          LOG.error("[Tmct0swiBo :: recargaDatosTabla] -  Se ha interrumpido algún thread en el control de cuantos quedan vivos ");
          LOG.error(e.getMessage(), e);
          shutdownAllRunnable();

        }
      }

    } catch (Exception e) {
      LOG.error("[Tmct0swiBo :: recargaDatosTabla] - Excepcion generica proceso");
      LOG.error(e.getMessage(), e);
      // Se eliminan todos los hilos.
      shutdownAllRunnable();
      throw new SIBBACBusinessException(sMensajeError);
    }

    LOG.debug("[Tmct0swiBo :: recargaDatosTabla] Fin Todo");

    if (iNumErrores > 0) {
      throw new SIBBACBusinessException(sMensajeError);
    }

  }

  /**
   * Crea un nuevo hilo y lo incorpora al pool
   * 
   * @param listaProcesar - Lista con los elementos que tiene que procesar el hilo
   * @param iNumHilo -- Numero identificativo del hilo
   */
  private void ejecutarHilo(List<Tmct0swi> listaProcesar, int iNumHilo) {
    ProcesaListaTmct0swiRunnable runnable = new ProcesaListaTmct0swiRunnable(listaProcesar, this, iNumHilo);
    executor.execute(runnable);
    misTthreads.add(runnable);
  }

  /**
   * Elimina todos los registros existentes en la tabla.
   */
  private void borrarRegistros() {
    // Esta sentencia hace que se pierda la transaccionalidad aunque se defina
    this.deleteAll();

  }

  /**
   * Inserta los registros de recibidos por parametro
   * 
   * @param datosInsertar - Lista con los elemntos a insertar en este Job
   * @param iNumJob - Numero identificativo del Job creado
   */
  public void insertaRegistrosLista(List<Tmct0swi> datosInsertar, int iNumJob) {
    LOG.debug("[" + NOMBRE_BO + " ::INICIO DE LA INSERCION DE LOS REGISTROS DEL JOB " + iNumJob
              + " Registros a procesar:  " + datosInsertar.size());
    // Se graban los datos.
    this.save(datosInsertar);
    /*
     * for (Tmct0swi tmct0swi : datosInsertar) { this.save(tmct0swi); }
     */
    LOG.debug("[" + NOMBRE_BO + " ::FIN DE LA INSERCION DE LOS REGISTROS DEL JOB " + iNumJob
              + " Registros PROCESADOS:  " + datosInsertar.size());
  }

  /**
   * Elimina el job recibido y si ha habido errores, marca al proceso principal la situacion que el hilo tenia errores
   * 
   * @param current - Job que se quiere eliminar
   * @param iNumJob -
   * @param tieneErrores
   */
  public void shutdownRunnable(ProcesaListaTmct0swiRunnable current, int iNumJob, boolean tieneErrores) {
    LOG.debug("[" + NOMBRE_BO + " ::SE ELIMINA EL JOB " + iNumJob);

    if (tieneErrores) {
      LOG.debug("[" + NOMBRE_BO + " ::EL JOB TENIA ERRORES: " + iNumJob);
      iNumErrores++;
    }

    misTthreads.remove(current);

  }

  /**
   * Elimina los jobs pendientes del pool.
   */
  public void shutdownAllRunnable() {
    LOG.error("[" + NOMBRE_BO + " ::ERROR NO CONTROLADO- SE BORRAR TODOS LOS JOBS ");
    for (ProcesaListaTmct0swiRunnable miHilo : misTthreads) {
      misTthreads.remove(miHilo);
    }
  }

}
