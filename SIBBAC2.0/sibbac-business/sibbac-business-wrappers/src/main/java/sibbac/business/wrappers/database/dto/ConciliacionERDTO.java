package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;


public class ConciliacionERDTO implements Comparator< ConciliacionERDTO >{

	private Date		settlementDate;
	private String		isin;
	private String		descripcion;
	private String		mercado;
	private String		codigoCuentaLiquidacion;
	private BigDecimal	titulosSV;
	private BigDecimal	efectivoSV;
	private BigDecimal	corretajeSV;
	private BigDecimal	titulosCustodio;
	private BigDecimal	efectivoCustodio;
	private BigDecimal	corretajeCustodio;
	private BigDecimal	titulosCamara;
	private BigDecimal	efectivoCamara;
	

	public ConciliacionERDTO() {
		super();
	}

	public ConciliacionERDTO( Date settlementDate, String isin, String descripcion, String mercado, String codigoCuentaLiquidacion,
			BigDecimal titulosSV, BigDecimal efectivoSV, BigDecimal corretajeSV, BigDecimal titulosCustodio, BigDecimal efectivoCustodio,
			BigDecimal corretajeCustodio ) {
		super();
		this.settlementDate = settlementDate;
		this.isin = isin;
		this.descripcion = descripcion;
		this.mercado = mercado;
		this.codigoCuentaLiquidacion = codigoCuentaLiquidacion;
		this.titulosSV = titulosSV;
		this.efectivoSV = efectivoSV;
		this.corretajeSV = corretajeSV;
		this.titulosCustodio = titulosCustodio;
		this.efectivoCustodio = efectivoCustodio;
		this.corretajeCustodio = corretajeCustodio;
	}

	public Date getSettlementDate() {
		return this.settlementDate;
	}

	public void setSettlementDate( Date settlementDate ) {
		this.settlementDate = settlementDate;
	}

	public String getIsin() {
		return this.isin;
	}

	public void setIsin( String isin ) {
		this.isin = isin;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public String getMercado() {
		return this.mercado;
	}

	public void setMercado( String mercado ) {
		this.mercado = mercado;
	}

	public String getCodigoCuentaLiquidacion() {
		return this.codigoCuentaLiquidacion;
	}

	public void setCodigoCuentaLiquidacion( String codigoCuentaLiquidacion ) {
		this.codigoCuentaLiquidacion = codigoCuentaLiquidacion;
	}

	public BigDecimal getTitulosSV() {
		return this.titulosSV;
	}

	public void setTitulosSV( BigDecimal titulosSV ) {
		this.titulosSV = titulosSV;
	}

	public BigDecimal getEfectivoSV() {
		return this.efectivoSV;
	}

	public void setEfectivoSV( BigDecimal efectivoSV ) {
		this.efectivoSV = efectivoSV;
	}

	public BigDecimal getCorretajeSV() {
		return this.corretajeSV;
	}

	public void setCorretajeSV( BigDecimal corretajeSV ) {
		this.corretajeSV = corretajeSV;
	}

	public BigDecimal getTitulosCustodio() {
		return this.titulosCustodio;
	}

	public void setTitulosCustodio( BigDecimal titulosCustodio ) {
		this.titulosCustodio = titulosCustodio;
	}

	public BigDecimal getEfectivoCustodio() {
		return this.efectivoCustodio;
	}

	public void setEfectivoCustodio( BigDecimal efectivoCustodio ) {
		this.efectivoCustodio = efectivoCustodio;
	}

	public BigDecimal getCorretajeCustodio() {
		return this.corretajeCustodio;
	}

	public void setCorretajeCustodio( BigDecimal corretajeCustodio ) {
		this.corretajeCustodio = corretajeCustodio;
	}
	

	public BigDecimal getTitulosCamara() {
	    return titulosCamara;
	}

	public void setTitulosCamara(BigDecimal titulosCamara) {
	    this.titulosCamara = titulosCamara;
	}

	public BigDecimal getEfectivoCamara() {
	    return efectivoCamara;
	}

	public void setEfectivoCamara(BigDecimal efectivoCamara) {
	    this.efectivoCamara = efectivoCamara;
	}

	@Override
	public int compare( ConciliacionERDTO conciliacionERDTO1 ,ConciliacionERDTO conciliacionERDTO2 ) {
		if (conciliacionERDTO1.getSettlementDate().before(conciliacionERDTO2.getSettlementDate()))
		{	
			return -1;
		}//if (conciliacionERDTO1.getSettlementDate().before(conciliacionERDTO2.getSettlementDate()))
		else if (conciliacionERDTO2.getSettlementDate().before(conciliacionERDTO1.getSettlementDate()))
		{
			return 1;
		}//else if (conciliacionERDTO2.getSettlementDate().before(conciliacionERDTO1.getSettlementDate()))
		else
		{	
			return (conciliacionERDTO1.getIsin().compareTo(conciliacionERDTO2.getIsin()));
		}//else
	}//public int compare( conciliacionERDTO2 conciliacionERDTO ) {

}
