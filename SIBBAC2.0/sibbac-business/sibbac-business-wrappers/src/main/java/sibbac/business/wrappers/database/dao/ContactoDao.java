package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.dto.InfoMailContactosDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;


@Component
public interface ContactoDao extends JpaRepository< Contacto, Long > {

	@Query( "UPDATE Contacto SET activo = :activo WHERE id = :id" )
	public Contacto changeStatusActive( char activo, final Long id );

	public Contacto findById( Long id );
	
	public List< Contacto > findAllByAlias( Alias alias );

	public List< Contacto > findAllByAlias_Id( final Long idAlias );
	
	public List< Contacto > findAllByAlias_Tmct0ali_Id_Cdaliass( final String cdaliass );

	/*
	@Query(value = "SELECT SUBSTR(xmlserialize(xmlagg(xmltext(CONCAT( '||', TXTIDIO.NBDESCRIPCIONES))) as VARCHAR(1024)), 3), "
		+	" CT.ID_ALIAS, CT.NBEMAIL, CT.ACTIVO, SV.JOB_NAME, "
		+	" AL.ID, IDIO.NBDESCRIPCION FROM BSNBPSQL.TMCT0_SERVICIOS_CONTACTOS SC "
		+	" JOIN BSNBPSQL.TMCT0_CONTACTO CT ON SC.CONTACTOID = CT.ID JOIN BSNBPSQL.TMCT0_SERVICIOS SV ON SV.ID = SC.SERVICIOID "
		+	" JOIN BSNBPSQL.TMCT0_ALIAS AL ON AL.ID = CT.ID_ALIAS JOIN BSNBPSQL.TMCT0_IDIOMA IDIO ON AL.ID_IDIOMA = IDIO.ID "
		+	" JOIN BSNBPSQL.TMCT0_TEXTO_IDIOMA TXTIDIO ON IDIO.ID = TXTIDIO.ID_IDIOMA WHERE CT.ID_ALIAS = :aliasParam "
		+	" AND TXTIDIO.ID_TEXTO IN (181, 182) GROUP BY CT.ID_ALIAS, CT.NBEMAIL, "
		+	" CT.ACTIVO, SV.JOB_NAME, AL.ID, IDIO.NBDESCRIPCION", nativeQuery = true)
	*/
	@Query(value = "SELECT SUBSTR(xmlserialize(xmlagg(xmltext(CONCAT( '||', TXTIDIO.NBDESCRIPCIONES))) as VARCHAR(4096)), 3) AS INFORMES_SUBJECT_AND_BODY, "
			+ "CT.ID_ALIAS AS ALIAS_CONTACTO, CT.NBEMAIL EMAIL_CONTACTO, CT.ID, CT.ACTIVO ACTIVO_CONTACTO, SV.ACTIVO ACTIVO_SERVICIO, SV.JOB_NAME JOB_NAME_SERVICIO, "
			+ "AL.CDALIASS, IDIO.NBDESCRIPCION NBDESCRIPCION_IDIOMA FROM BSNBPSQL.TMCT0_SERVICIOS_CONTACTOS SC JOIN BSNBPSQL.TMCT0_CONTACTO CT ON SC.CONTACTOID = CT.ID "
			+ "JOIN BSNBPSQL.TMCT0_SERVICIOS SV ON SV.ID = SC.SERVICIOID JOIN BSNBPSQL.TMCT0_ALIAS AL ON AL.ID = CT.ID_ALIAS JOIN BSNBPSQL.TMCT0ALI ALI ON ALI.CDALIASS = AL.CDALIASS "
			+ "JOIN BSNBPSQL.TMCT0_IDIOMA IDIO ON AL.ID_IDIOMA = IDIO.ID JOIN BSNBPSQL.TMCT0_TEXTO_IDIOMA TXTIDIO ON IDIO.ID = TXTIDIO.ID_IDIOMA WHERE SV.ACTIVO = 1 AND CT.ACTIVO = 'S' AND AL.CDALIASS = :aliasParam "
			+ "AND  TXTIDIO.ID_TEXTO IN (SELECT DISTINCT TXT.ID FROM BSNBPSQL.TMCT0_TEXTO_IDIOMA TXTIDIO JOIN BSNBPSQL.TMCT0_TEXTO TXT ON TXTIDIO.ID_TEXTO = TXT.ID WHERE TXT.TIPO IN ( :infIdBody, :infIdSubject )) "
			+ "AND SV.JOB_NAME = :jobName GROUP BY CT.ID_ALIAS, CT.NBEMAIL, CT.ID, CT.ACTIVO, SV.ACTIVO, SV.JOB_NAME, AL.CDALIASS, IDIO.NBDESCRIPCION", nativeQuery = true)
	public List<Object[]> findDatosMailContactoByAlias(@Param("aliasParam") String aliasParam, @Param("infIdSubject") String infIdSubject, @Param("infIdBody") String infIdBody, @Param("jobName") String jobName);

}
