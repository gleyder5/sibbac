package sibbac.business.wrappers.database.dto;


public class NumeradoresDTO {

	private Long	id;
	private Long	inicio;
	private Long	fin;
	private Long	identificadores;
	private Long	siguiente;
	private String	tipodedocumento;

	public NumeradoresDTO() {
	}

	public NumeradoresDTO( Long id, Long inicio, Long fin, Long identificadores, Long siguiente, String tipodedocumento ) {

		this.id = id;
		this.inicio = inicio;
		this.fin = fin;
		this.identificadores = identificadores;
		this.siguiente = siguiente;
		this.tipodedocumento = tipodedocumento;
	}

	public Long getId() {
		return id;
	}

	public Long getInicio() {
		return inicio;
	}

	public Long getFin() {
		return fin;
	}

	public Long getIdentificadores() {
		return identificadores;
	}

	public Long getSiguiente() {
		return siguiente;
	}

	public String getTipodedocumento() {
		return tipodedocumento;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setInicio( Long inicio ) {
		this.inicio = inicio;
	}

	public void setFin( Long fin ) {
		this.fin = fin;
	}

	public void setIdentificadores( Long identificadores ) {
		this.identificadores = identificadores;
	}

	public void setSiguiente( Long siguiente ) {
		this.siguiente = siguiente;
	}

	public void setTipodedocumento( String tipodedocumento ) {
		this.tipodedocumento = tipodedocumento;
	}

}
