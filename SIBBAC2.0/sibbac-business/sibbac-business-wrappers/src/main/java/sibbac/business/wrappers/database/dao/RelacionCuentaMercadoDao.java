package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.data.RelacionCuentaMercadoData;
import sibbac.business.wrappers.database.model.RelacionCuentaMercado;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;


public interface RelacionCuentaMercadoDao extends JpaRepository< RelacionCuentaMercado, Long > {

	@Query( "SELECT new sibbac.business.wrappers.database.data.RelacionCuentaMercadoData(o) FROM RelacionCuentaMercado o" )
	List< RelacionCuentaMercadoData > findAllData();

	@Modifying
	@Query( "DELETE FROM RelacionCuentaMercado WHERE relacionCuentaMercadoPK.idCtaLiquidacion =:cl" )
	void deleteByIdCuentaLiquidacion( @Param( "cl" ) Tmct0CuentaLiquidacion cl );
}
