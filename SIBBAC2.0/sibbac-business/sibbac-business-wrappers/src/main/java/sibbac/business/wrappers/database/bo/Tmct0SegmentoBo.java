package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0SegmentoDao;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Nemotecnicos;
import sibbac.business.wrappers.database.model.Tmct0Segmento;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SegmentoBo extends AbstractBo< Tmct0Segmento, Integer, Tmct0SegmentoDao >{

	/** The Constant LOG. */
	private static final Logger	LOG	= LoggerFactory.getLogger( Tmct0SegmentoBo.class );
	
	public List< Tmct0Segmento > findAll() {    
		return this.dao.findAll(new Sort(Sort.Direction.ASC,"nbDescripcion"));
	}
	public Tmct0Segmento findByNbDescripcion(String nbDescripcion){
		return this.dao.findByNbDescripcion(nbDescripcion);
	}
	
}
