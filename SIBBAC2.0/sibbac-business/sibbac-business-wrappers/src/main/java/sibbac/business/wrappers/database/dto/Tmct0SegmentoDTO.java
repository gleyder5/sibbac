package sibbac.business.wrappers.database.dto;

import sibbac.business.wrappers.database.model.Tmct0Segmento;

public class Tmct0SegmentoDTO {
	
	private int	id_segmento;	
	private String nbDescripcion;
	private String cd_codigo;
	
	public Tmct0SegmentoDTO() {	}

	public Tmct0SegmentoDTO(Tmct0Segmento tmct0Segmento) {
		
		id_segmento = tmct0Segmento.getId_segmento();
		nbDescripcion = tmct0Segmento.getNbDescripcion();
		cd_codigo = tmct0Segmento.getCd_codigo();
	}

	public int getId_segmento() {
		return id_segmento;
	}

	public void setId_segmento(int id_segmento) {
		this.id_segmento = id_segmento;
	}

	public String getNbDescripcion() {
		return nbDescripcion;
	}

	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}

	public String getCd_codigo() {
		return cd_codigo;
	}

	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}
	
	
	

}
