package sibbac.business.wrappers.database.bo;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.AliasRecordatoriosDao;
import sibbac.business.wrappers.database.dto.AliasRecordatorioDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasRecordatorio;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;


@Service
public class AliasRecordatoriosBo extends AbstractBo< AliasRecordatorio, Long, AliasRecordatoriosDao > {

	@Autowired
	private AliasBo	aliasBO;

	/**
	 * Cambia un objeto aliasRecordatorio recuperado de la tabla
	 * aliasRecordatorio modificando los campos que se pasan por parametro
	 * (params)
	 *
	 * @param params
	 * @return
	 * @throws SIBBACBusinessException
	 */
	@Transactional
	public Map< String, String > modificacionAliasRecordatorio( final AliasRecordatorioDTO dto ) throws SIBBACBusinessException {
		String prefix = "[AliasRecordatoriosBo::modificacionAliasRecordatorio] ";
		final Map< String, String > resultado = new HashMap< String, String >();

		LOG.trace( prefix + "Se inicia la creacion/modificacion de un aliasRecordatorio..." );
		LOG.trace( prefix + "> [getIdAliasRecordatorio=={}]", dto.getIdAliasRecordatorio() );
		LOG.trace( prefix + "> [            getIdAlias=={}]", dto.getIdAlias() );
		LOG.trace( prefix + "> [      getImporteMinimo=={}]", dto.getImporteMinimo() );
		LOG.trace( prefix + "> [         getFrecuencia=={}]", dto.getFrecuencia() );
		final Long id = dto.getIdAliasRecordatorio();
		final Long idAlias = dto.getIdAlias();

		boolean crear = false;
		AliasRecordatorio ar = null;
		if ( id != null ) {
			LOG.trace( prefix + "Buscando el aliasRecordatorio: [{}]", id );
			ar = this.findById( id );
			if ( ar == null ) {
				LOG.trace( prefix + "> NOT aliasRecordatorio: [{}]", id );
				crear = true;
			} else {
				LOG.trace( prefix + "> GOT aliasRecordatorio: [{}]", ar );
				crear = false;
			}
		} else {
			LOG.trace( prefix + "> NOT idAliasRecordatorio: [{}]", id );
			crear = true;
		}

		if ( crear ) {
			LOG.trace( prefix + "Buscando el alias: [{}]", idAlias );
			Alias alias = aliasBO.findById( idAlias );
			LOG.trace( prefix + "> GOT alias: [{}]", alias );
			ar = new AliasRecordatorio();
			ar.setAlias( alias );
		}
		LOG.trace( prefix + "Actualizando campos..." );
		ar.setAuditDate( new java.util.Date() );
		ar.setImporteMinimo( dto.getImporteMinimo() );
		ar.setFrecuencia( dto.getFrecuencia() );

		LOG.trace( prefix + "Salvando el aliasRecordatorio..." );
		AliasRecordatorio ar2 = this.save( ar );
		LOG.trace( prefix + "Salvado el aliasRecordatorio: [{}]", ar2.getId() );

		String result = "";
		if ( !crear ) {
			result = "Se ha modificado el aliasRecordatorio";
			LOG.trace( prefix + "Se ha modificado el aliasRecordatorio" );
		} else {
			result = "Se ha creado el aliasRecordatorio";
			LOG.warn( prefix + "Se ha creado el aliasRecordatorio" );
		}
		resultado.put( "result", result );
		return resultado;
	}

  public AliasRecordatorio findByIdAlias(final Long idAlias) {
    AliasRecordatorio aliasRecordatorio = new AliasRecordatorio();
    try {
      aliasRecordatorio = dao.findByAlias_Id(idAlias);
    }
    catch (NoResultException ex) {
      LOG.debug("", ex);
    }
    if(aliasRecordatorio == null){
      LOG.debug("No AliasRecordatorio for alias: " + idAlias);
      aliasRecordatorio = new AliasRecordatorio();
      //seteamos los valores por defecto para poder continuar con el envío
      aliasRecordatorio.setFrecuencia(30);
      aliasRecordatorio.setImporteMinimo(BigDecimal.ONE);
    }
    return aliasRecordatorio;

  }

	/**
	 * Devuelve una lista de los AliasRecordatorios de la tabla
	 * AliasRecordatorios
	 *
	 * @param params
	 * @return
	 */

	public Map< String, AliasRecordatorioDTO > getListaAliasRecordatorio( final Long idAlias ) {
		LOG.trace( "Se inicia la creacion de la lista de los AliasRecordatorio" );
		Map< String, AliasRecordatorioDTO > resultados = null;
		final AliasRecordatorio aliasRecordatorio = dao.findByAlias_Id( idAlias );
		if ( aliasRecordatorio != null ) {
			final AliasRecordatorioDTO aliasRecordatorioDto = new AliasRecordatorioDTO( aliasRecordatorio );
			resultados = new HashMap< String, AliasRecordatorioDTO >();
			resultados.put( idAlias.toString(), aliasRecordatorioDto );
		}
		return resultados;

	}

}
