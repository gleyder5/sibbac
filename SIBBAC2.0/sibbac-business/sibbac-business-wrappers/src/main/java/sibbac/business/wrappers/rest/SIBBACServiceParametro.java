package sibbac.business.wrappers.rest;


// package sibbac.business.facturacion.rest;
//
// import java.util.ArrayList;
// import java.util.LinkedList;
// import java.util.List;
// import java.util.Map;
//
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
//
// import sibbac.business.facturacion.common.Constantes;
// import sibbac.business.facturacion.database.bo.ParametroBo;
// import sibbac.webapp.annotations.SIBBACService;
// import sibbac.webapp.beans.WebRequest;
// import sibbac.webapp.beans.WebResponse;
// import sibbac.webapp.business.SIBBACServiceBean;
//
//
// @SIBBACService
// public class SIBBACServiceParametro implements SIBBACServiceBean {
//
// private static final Logger LOG = LoggerFactory.getLogger( SIBBACServiceParametro.class );
//
// @Autowired
// private ParametroBo parametroBO;
//
// public SIBBACServiceParametro() {
// LOG.trace("[SIBBACServiceParametro::<constructor>] Initiating SIBBAC Service for \"Parametro\".");
// }
//
// @Override
// public WebResponse process( WebRequest webRequest ) {
//
// LOG.trace("[SIBBACServiceParametro::process] Processing a web request...");
//
// // prepara webResponse
// WebResponse webResponse = new WebResponse();
//
// // extrae parametros del httpRequest
// Map<String, String> params = webRequest.getFilters();
//
// // obtiene comandos(paths)
// LOG.trace("[SIBBACServiceParametro::process] Command.: [{}]", webRequest.getAction());
//
// //selecciona la accion
// switch (webRequest.getAction()) {
// case Constantes.PARAMETRO_CREATE:
//
// // comprueba que los parametros han sido enviados
// if(params == null){
// webResponse.setError("Por favor indique los filtros de búsqueda.");
// return webResponse;
// }
//
// // Hace el set en el webresponse del resultado de introducir en tabla contacto, un nuevo contacto
// webResponse.setResultados(parametroBO.createParametro(params));
// LOG.trace("Rellenado el WebResponse con el alta parametro");
//
// break;
//
//
//
// case Constantes.PARAMETRO_MODIFICACION:
//
// // comprueba que los parametros han sido enviados
// if(params == null){
// webResponse.setError("Por favor indique los filtros de búsqueda.");
// return webResponse;
// }
//
// //HAce el set en el webresponse del resultado de modificar un contacto en la tabla contacto.
// webResponse.setResultados(parametroBO.modificacionParametro(params));
// LOG.trace("Rellenado el WebResponse con la modificacion del parametro");
//
// break;
//
//
//
// case Constantes.PARAMETRO_LISTA:
//
// //Hace el set de la devolucion de todos los contactos de la tabla contacto
// webResponse.setResultados(parametroBO.getListaParametro());
// LOG.trace("Rellenado el WebResponse con la lista de parametros");
//
// break;
//
// default:
//
// webResponse.setError("Comando incorrecto");
// LOG.error("No le llega ningun comando correcto");
//
// break;
//
// }
//
// return webResponse;
// }
//
// @Override
// public List< String > getAvailableCommands() {
// List< String > commands = new ArrayList< String >();
// commands.add( Constantes.PARAMETRO_CREATE );
// commands.add( Constantes.PARAMETRO_MODIFICACION);
// commands.add( Constantes.PARAMETRO_LISTA );
//
// return commands;
// }
//
// @Override
// public List< String > getFields() {
// List< String > fields = new LinkedList< String >();
// fields.add("id");
// fields.add("sociedad");
// fields.add("registroMercantil");
// fields.add("texto1");
// fields.add("texto2");
// fields.add("entidadCuenta");
// fields.add("sucursalCuenta");
// fields.add("digitoControl");
// fields.add("cuenta");
// fields.add("banco");
// fields.add("BIC");
//
// return fields;
// }
// }
