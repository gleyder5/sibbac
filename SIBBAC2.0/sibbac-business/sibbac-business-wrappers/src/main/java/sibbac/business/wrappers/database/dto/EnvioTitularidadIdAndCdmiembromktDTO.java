package sibbac.business.wrappers.database.dto;

import sibbac.business.wrappers.database.model.RecordBean;

public class EnvioTitularidadIdAndCdmiembromktDTO extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 369120819465911011L;
  private String cdmiembromkt;

  public EnvioTitularidadIdAndCdmiembromktDTO(long id, String cdmiembromkt) {
    setId(id);
    this.cdmiembromkt = cdmiembromkt;
  }

  public String getCdmiembromkt() {
    return cdmiembromkt;
  }

  public void setCdmiembromkt(String cdmiembromkt) {
    this.cdmiembromkt = cdmiembromkt;
  }

  @Override
  public String toString() {
    return "EnvioTitularidadIdAndCdmiembromktDTO [cdmiembromkt=" + cdmiembromkt + ", id=" + id + "]";
  }

}
