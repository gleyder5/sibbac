package sibbac.business.wrappers.database.dao.specifications;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0alo;


public class Tmct0AloSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( Tmct0AloSpecifications.class );

	public static Specification< Tmct0alo > nuorden( final String nuorden ) {
		return new Specification< Tmct0alo >() {

			public Predicate toPredicate( Root< Tmct0alo > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nuordenPredicate = null;
				LOG.trace( "[Tmct0aloSpecifications::nuorden] [nuorden=={}]", nuorden );
				if ( nuorden != null ) {
					nuordenPredicate = builder.equal( root.< Tmct0aloId > get( "id" ).<String> get("nuorden"), nuorden );
				}
				return nuordenPredicate;
			}
		};
	}

	public static Specification< Tmct0alo > nbooking( final String nbooking ) {
		return new Specification< Tmct0alo >() {

			public Predicate toPredicate( Root< Tmct0alo > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbookingPredicate = null;
				LOG.trace( "[Tmct0aloSpecifications::nbooking] [nbooking=={}]", nbooking );
				if ( nbooking != null ) {
					nbookingPredicate = builder.equal( root.< Tmct0aloId > get( "id" ).<String> get("nbooking"), nbooking );
				}
				return nbookingPredicate;
			}
		};
	}

	public static Specification< Tmct0alo > nucnfclt( final String nucnfclt ) {
		return new Specification< Tmct0alo >() {

			public Predicate toPredicate( Root< Tmct0alo > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nucnfcltPredicate = null;
				LOG.trace( "[Tmct0aloSpecifications::nucnfclt] [nucnfclt=={}]", nucnfclt );
				if ( nucnfclt != null ) {
					nucnfcltPredicate = builder.equal( root.< Tmct0aloId > get( "id" ).<String> get("nucnfclt"), nucnfclt );
				}
				return nucnfcltPredicate;
			}
		};
	}
	
	public static Specification< Tmct0alo > listadoFriltrado( final String nuorden, final String nbooking, final String nucnfclt ) {
		return new Specification< Tmct0alo >() {

			public Predicate toPredicate( Root< Tmct0alo > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0alo > specs = null;

				if ( nuorden != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nuorden( nuorden ) );
					} else {
						specs = specs.and( Specifications.where( nuorden( nuorden ) ) );
					}

				}

				if ( nbooking != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbooking( nbooking ) );
					} else {
						specs = specs.and( Specifications.where( nbooking( nbooking ) ) );
					}

				}

				if ( nucnfclt != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nucnfclt( nucnfclt ) );
					} else {
						specs = specs.and( Specifications.where( nucnfclt( nucnfclt ) ) );
					}

				}
				
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

}
