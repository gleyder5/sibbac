package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Etiquetas;


@Component
public interface EtiquetasDao extends JpaRepository< Etiquetas, Long > {

	public List< Etiquetas > findAllByTabla( String tabla );
}
