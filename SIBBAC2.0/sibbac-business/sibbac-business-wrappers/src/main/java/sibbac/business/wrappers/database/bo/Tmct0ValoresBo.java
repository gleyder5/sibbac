package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.wrappers.database.dao.Tmct0ValoresDao;
import sibbac.business.wrappers.database.dto.ValoresDTO;
import sibbac.business.wrappers.database.model.Tmct0GruposValores;
import sibbac.business.wrappers.database.model.Tmct0Valores;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ValoresBo extends AbstractBo<Tmct0Valores, Long, Tmct0ValoresDao> {
  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0ValoresBo.class);

  public List<Tmct0Valores> findByCodigoDeValorOrderByFechaEmisionDesc(String codigoDeValor) {

    return this.dao.findByCodigoDeValorOrderByFechaEmisionDesc(codigoDeValor);
  }

  public List<Tmct0GruposValores> findGrupoValoresByCodigoDeValorOrderByFechaEmisionDesc(String codigoDeValor) {
    return this.dao.findGrupoValoresByCodigoDeValorOrderByFechaEmisionDesc(codigoDeValor);
  }

  public List<String> findTiposProductoByCodigoDeValorOrderByFechaEmisionDesc(String codigoDeValor) {
    return this.dao.findTiposProductoByCodigoDeValorOrderByFechaEmisionDesc(codigoDeValor);

  }

  public List<ValoresDTO> entitiesListToAnDTOList(List<Tmct0Valores> result) {
    List<ValoresDTO> list = new ArrayList<ValoresDTO>();
    for (Tmct0Valores valor : result) {
      list.add(new ValoresDTO(valor.getCodigoDeValor(), valor.getDescripcion()));
    }
    return list;
  }

  public List<String> findIsin() {
    return dao.findIsin();
  }

  /**
   * Metodo que devuelve la descripcion de un isin si la encuentra. En caso de
   * que no la encuentre devuelve el isin.
   * 
   * @param isin
   * @return
   */
  public String findDescIsinByIsin(String isin) {
    List<Tmct0Valores> lista = this.dao.findByCodigoDeValorOrderByFechaEmisionDesc(isin);
    if (lista != null && lista.size() > 0) {
      return lista.get(0).getDescripcion();
    }
    else {
      return isin;
    }
  }

  public boolean isIsinDerechos(String cdisin) throws SIBBACBusinessException {
    boolean res = false;
    List<String> tiposProducto = findTiposProductoByCodigoDeValorOrderByFechaEmisionDesc(cdisin);
    if (CollectionUtils.isNotEmpty(tiposProducto)) {
      res = tiposProducto.get(0).trim().equals("D");
    }
    else {
      throw new SIBBACBusinessException("INCIDENCIA- ISIN no encontrado en el maestro de valores: " + cdisin);
    }
    return res;
  }

  public boolean isIsinDerechos(Map<String, Boolean> isinDerechos, String cdisin) throws SIBBACBusinessException {
    cdisin = StringUtils.trim(cdisin);
    if (isinDerechos.get(cdisin) == null) {
      synchronized (isinDerechos) {
        if (isinDerechos.get(cdisin) == null) {
          try {
            isinDerechos.put(cdisin, isIsinDerechos(cdisin));
          }
          catch (Exception e) {
            isinDerechos.put(cdisin, false);
            throw e;
          }

        }
      }
    }

    return isinDerechos.get(cdisin);
  }

  public List<LabelValueObject> labelValueList() {
    return dao.labelValueList();
  }

  @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void inactivarValoresUltimoDiaNegociacion() throws SIBBACBusinessException {
    LOG.info("[inactivarValoresUltimoDiaNegociacion] buscando valores...");
    List<Tmct0Valores> listValores = dao.findAllActivosConFechaUltimoDiaNegociacion();
    LOG.info("[inactivarValoresUltimoDiaNegociacion] encontrados {} valores...", listValores.size());
    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
    if (CollectionUtils.isNotEmpty(listValores)) {
      List<Tmct0Valores> listValoresDadosBaja = new ArrayList<Tmct0Valores>();
      BigDecimal ultimaNegociacionBd;
      for (Tmct0Valores valor : listValores) {
        if (StringUtils.isNotBlank(valor.getFechaUltimoDiaNegociacion())) {
          ultimaNegociacionBd = FormatDataUtils.convertStringToBigDecimal(valor.getFechaUltimoDiaNegociacion());
          if (now.compareTo(ultimaNegociacionBd) > 0) {
            LOG.info(
                "[inactivarValoresUltimoDiaNegociacion] inactivando valor: {}-{} f.emision: {} precio: {} precio cierre: {} precio cierre anterior: {} entidad emisora: {}...",
                valor.getCodigoDeValor(), valor.getDescripcion(), valor.getFechaEmision(), valor.getPrecioEjercicio(),
                valor.getPrecioCierreSesionActual(), valor.getPrecioCierreSesionAnterior(),
                valor.getEntidadEmisoraGestora());
            valor.setActivo("N");
            listValoresDadosBaja.add(valor);
          }
        }
      }

      if (CollectionUtils.isNotEmpty(listValoresDadosBaja)) {
        save(listValoresDadosBaja);
      }

    }
  }

}
