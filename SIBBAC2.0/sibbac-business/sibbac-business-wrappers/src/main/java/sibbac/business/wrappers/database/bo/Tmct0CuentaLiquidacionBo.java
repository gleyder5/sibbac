package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0CuentaLiquidacionDao;
import sibbac.business.wrappers.database.dao.Tmct0CuentaLiquidacionDaoImpl;
import sibbac.business.wrappers.database.data.CuentaLiquidacionData;
import sibbac.business.wrappers.database.dto.CuentaLiquidacionDTO;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.database.bo.AbstractBo;

/**
 * The Class Tmct0CuentaLiquidacionBo.
 */
@Service
public class Tmct0CuentaLiquidacionBo extends AbstractBo< Tmct0CuentaLiquidacion, Long, Tmct0CuentaLiquidacionDao > {

	/** The dao impl. */
	@Autowired
	private Tmct0CuentaLiquidacionDaoImpl	daoImpl;

	@Autowired
	private Tmct0CuentaLiquidacionDao	dao;

	/**
	 * Find by id cta comp.
	 *
	 * @param idCuentaComp the id cuenta comp
	 * @return the tmct0 cuenta liquidacion
	 */
	public Tmct0CuentaLiquidacion findByIdCtaComp( Long idCuentaComp ) {
		return dao.findByIdCtaComp( idCuentaComp );
	}

	/**
	 * Find all clearing or propias.
	 *
	 * @return the list
	 */
	@Transactional
	public List< Tmct0CuentaLiquidacion > findAllClearingOrPropias() {
		List< Tmct0CuentaLiquidacion > result = this.dao.findAllClearingPropias();
		List< Tmct0CuentasDeCompensacion > resultNoPropias = this.dao.findAllClearingNoPropias();
		for ( Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion : resultNoPropias ) {
			if ( tmct0CuentasDeCompensacion.getCuentaClearing() == 'S' ) {
				result.add( tmct0CuentasDeCompensacion.getTmct0CuentaLiquidacion() );
			}
		}
		return result;
	}

	/**
	 * Find all clearing no propias.
	 *
	 * @return the list
	 */
	@Transactional
	public List< Tmct0CuentaLiquidacion > findAllClearingNoPropias() {
		return dao.findAll();
	}

	/**
	 * Find all clearing propias.
	 *
	 * @return the list
	 */
	public List< Tmct0CuentaLiquidacion > findAllClearingPropias() {
		return dao.findAllClearingPropias();
	}

	/**
	 * Obtiene las cuentas de liquidacion que esten relacionadas con cuentas de compensacion clearing.
	 *
	 * @return the list
	 */
	public List< Tmct0CuentaLiquidacion > findAllByCuentaDeCompensacionIsClearing() {
		List< Tmct0CuentaLiquidacion > resultList = new LinkedList< Tmct0CuentaLiquidacion >();
		resultList = dao.findAllByCuentaDeCompensacionIsClearing();
		return resultList;
	}
	
	/**
	 * Obtiene las cuentas de liquidacion que esten relacionadas con cuentas de compensacion 
	 * propias e individuales
	 *
	 * @return the list
	 */
	public List< Tmct0CuentaLiquidacion > findAllByCuentaDeCompensacionIsPropiaOrIndividual() {
		List< Tmct0CuentaLiquidacion > resultList = new LinkedList< Tmct0CuentaLiquidacion >();
		resultList = dao.findAllByCuentaDeCompensacionIsPropiaOrIndividual();
		return resultList;
	}

	/**
	 * Obtiene las cuentas de liquidacion que esten relacionadas con cuentas de compensacion clearing
	 * que no son propias ni individuales
	 *
	 * @return the list
	 */
	public List< Tmct0CuentaLiquidacion > findAllByCuentaDeCompensacionIsClearingNoPropiaNoIndividual() {
		List< Tmct0CuentaLiquidacion > resultList = new LinkedList< Tmct0CuentaLiquidacion >();
		resultList = dao.findAllByCuentaDeCompensacionIsClearingNoPropiaNoIndividual();
		return resultList;
	}
	
	/**
	 * Entity to dto.
	 *
	 * @param entity the entity
	 * @return the cuenta liquidacion dto
	 */
	public CuentaLiquidacionDTO entityToDTO( Tmct0CuentaLiquidacion entity ) {
		CuentaLiquidacionDTO dto = new CuentaLiquidacionDTO();
		if ( entity != null ) {
			dto.setCdCodigo( entity.getCdCodigo() );
			dto.setEntidadRegistro( entity.getEntidadRegistro().getId() );
			dto.setEsCuentaPropia( entity.isEsCuentaPropia() );
			dto.setFrecuenciaEnvioExtracto( entity.getFrecuenciaEnvioExtracto() );
			dto.setId( entity.getId() );
			// dto.setIdCuentasDeCompensacion( entity.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion() );
			dto.setIdMercadoContratacion( entity.getIdMercadoContratacion() );
			dto.setNumCuentaER( entity.getNumCuentaER() );
			dto.setTipoER( entity.getEntidadRegistro().getTipoer().getNombre() );
			dto.setTipoFicheroConciliacion( entity.getTipoFicheroConciliacion() );
			dto.setTipoNeteoTitulo( entity.getTipoNeteoTitulo().getNombre() );
			dto.setCuentaIberclear( entity.getCuentaIberclear() );
		}
		return dto;
	}

	/**
	 * Entities to dto list.
	 *
	 * @param entities the entities
	 * @return the list
	 */
	public List< CuentaLiquidacionDTO > entitiesToDTOList( List< Tmct0CuentaLiquidacion > entities ) {
		List< CuentaLiquidacionDTO > resultList = new LinkedList< CuentaLiquidacionDTO >();
		for ( Tmct0CuentaLiquidacion entity : entities ) {
			CuentaLiquidacionDTO dto = entityToDTO( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	/**
	 * Find by cd_codigo.
	 *
	 * @param cdCodigo the cd codigo
	 * @return the tmct0 cuenta liquidacion
	 */
	public Tmct0CuentasDeCompensacion findByCdCodigo( final String cdCodigo ) {
		final List<Tmct0CuentasDeCompensacion> lista;
		
		lista = dao.findByCdCodigo(cdCodigo);
		return lista.isEmpty() ? null : lista.get(0);
	}

	/**
	 * Find by cuenta norma43.
	 *
	 * @param cuentaNorma43 the cuenta norma43
	 * @return the tmct0 cuenta liquidacion
	 */
	public Tmct0CuentasDeCompensacion findByCuentaNorma43( final Long cuentaNorma43 ) {
		Tmct0CuentasDeCompensacion cta = null;
		List< Tmct0CuentasDeCompensacion > ctas = dao.findByCuentaNorma43( cuentaNorma43 );
		if ( ctas != null && ctas.size() > 0 ) {
			cta = ctas.get( 0 );
		}
		return cta;
	}

	/**
	 * Find all filtered.
	 *
	 * @param filters the filters
	 * @return the list
	 */
	@Transactional
	public List< CuentaLiquidacionData > findAllFiltered( Map< String, Serializable > filters ) {
		List< CuentaLiquidacionData > resultList = daoImpl.findAllFiltered( filters );
		return resultList;
	}
	
	@Transactional
	public Tmct0CuentaLiquidacion findByCuentaIberclear(String cuentaIberclear) {
		final List<Tmct0CuentaLiquidacion> res;
		
		res = dao.findByCuentaIberclear(cuentaIberclear);
		if(res != null && !res.isEmpty()) {
			return res.get(0);
		}
		return null;
	}
	
	@Transactional
	public Tmct0CuentaLiquidacion findByNumeroCuentaSubcustodioCuentaDeCompensacion(String numeroCuentaSubcustodio) {
		final List<Tmct0CuentaLiquidacion> res;
		
		res = dao.findByNumeroCuentaSubcustodioCuentaDeCompensacion(numeroCuentaSubcustodio);
		if(res != null && !res.isEmpty()) {
			return res.get(0);
		}
		return null;
	}

}
