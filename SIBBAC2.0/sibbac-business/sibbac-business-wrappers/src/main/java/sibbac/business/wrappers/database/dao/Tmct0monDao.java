package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0mon;
import sibbac.business.wrappers.database.model.Tmct0mon;

@Repository
public interface Tmct0monDao extends JpaRepository<Tmct0mon, String> {

}
