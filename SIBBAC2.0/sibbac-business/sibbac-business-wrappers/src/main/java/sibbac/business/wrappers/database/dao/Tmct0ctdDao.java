package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0ctdId;

@Repository
public interface Tmct0ctdDao extends JpaRepository<Tmct0ctd, Tmct0ctdId> {

  @Query("select g from Tmct0ctd g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking")
  List<Tmct0ctd> findAllByNuodenAndNbooking(@Param("pnuorden") String nuorden, @Param("pnbooking") String nbooking);

}
