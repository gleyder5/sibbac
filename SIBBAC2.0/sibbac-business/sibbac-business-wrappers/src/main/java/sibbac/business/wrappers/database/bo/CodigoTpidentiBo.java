package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.CodigoTpidentiDao;
import sibbac.business.wrappers.database.model.CodigoTpidenti;
import sibbac.database.bo.AbstractBo;

@Service
public class CodigoTpidentiBo extends
		AbstractBo<CodigoTpidenti, String, CodigoTpidentiDao> {

	public CodigoTpidenti findByCodigo(String codigo) {
		CodigoTpidenti tpidenti = null;
		try {
			tpidenti = dao.findByCodigo(codigo);
		} catch (Exception e) {
			tpidenti = null;
		}

		return tpidenti;
	}
}
