package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.CodigoTpsocied;


@Repository
public interface CodigoTpsociedDao extends JpaRepository< CodigoTpsocied, String > {

	public CodigoTpsocied findByCodigo( String codigo );

}
