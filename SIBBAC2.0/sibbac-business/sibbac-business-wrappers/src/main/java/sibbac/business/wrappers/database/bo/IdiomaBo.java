package sibbac.business.wrappers.database.bo;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.IdiomaDao;
import sibbac.business.wrappers.database.model.Idioma;
import sibbac.database.bo.AbstractBo;


@Service
public class IdiomaBo extends AbstractBo< Idioma, Long, IdiomaDao > {

	public Idioma findByCodigo( String codigo ) {
		return dao.findByCodigo( codigo );
	}

	/**
	 * Recupera todos los grupos impositivos de la tabla GruposImpositivos
	 *
	 * @return
	 */
	public Map< String, Idioma > getListaIdiomas() {

		LOG.trace( "Se inicia la creacion de la lista de los idiomas" );
		final Map< String, Idioma > resultados = new HashMap< String, Idioma >();
		final List< Idioma > idiomas = ( List< Idioma > ) this.findAll();
		if ( CollectionUtils.isNotEmpty( idiomas ) ) {
			for ( final Idioma idioma : idiomas ) {
				resultados.put( idioma.getId().toString(), idioma );
			}
		}

		LOG.trace( "Se ha creado la lista de los idiomas" );
		return resultados;

	}

}
