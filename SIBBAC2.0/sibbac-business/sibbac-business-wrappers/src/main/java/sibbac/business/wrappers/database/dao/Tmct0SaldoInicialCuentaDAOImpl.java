package sibbac.business.wrappers.database.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.SaldoInicialCuentaData;

@Repository
public class Tmct0SaldoInicialCuentaDAOImpl {
    private static final Logger LOG = LoggerFactory
	    .getLogger(Tmct0SaldoInicialCuentaDAOImpl.class);
    @PersistenceContext
    private EntityManager em;

    /**
     * SELECT FECHA, ISIN, CDALIASS, CD_CODIGO_CUENTA_LIQ, SUM(TITULOS) FROM
     * TMCT0_SALDO_INICIAL_CUENTA S WHERE FECHA = (SELECT MAX(FECHA) FROM
     * TMCT0_SALDO_INICIAL_CUENTA T WHERE T.FECHA <= 'fecha_filtro' AND S.ISIN =
     * T.ISIN AND T.CD_CODIGO_CUENTA_LIQ = S.CD_CODIGO_CUENTA_LIQ AND S.CDALIASS = T.CDALIASS
     *  GROUP BY ISIN,
     * CD_CODIGO_CUENTA_LIQ) GROUP BY ISIN, CDALIASS, CD_CODIGO_CUENTA_LIQ,
     * FECHA ORDER BY FECHA DESC;
     *
     * Este método tiene que conseguir lo que la query de arriba, es decir
     * obtener los saldos cuya fecha sea la mayor extraida de la subquery, esta
     * fecha es la mayor de agrupar los saldos por isin y codigo_cuenta_liq, los
     * saldos en la query externa, se agrupan también por cdaliass, porque
     * pueden haber varios saldos con mismo isin, pero distinto alias, por
     * tanto, se coje el de la última fecha más cercana a la pedida, de ahí que
     * en la subquery se agrupe solo por isin y codCtaLiq.
     * 
     * @param filtros
     * @return
     * @throws PersistenceException
     */
    @SuppressWarnings("unchecked")
    public List<SaldoInicialCuentaData> findAllGroupByIsinAndCodCuentaliqAndFLiquidacionOrderByIsinAndFecha(
	    Map<String, Serializable> filtros) throws PersistenceException, IllegalArgumentException {
	List<SaldoInicialCuentaData> resultList = new ArrayList<SaldoInicialCuentaData>();
	String selectDescIsin = "(SELECT v.descripcion FROM Tmct0Valores v WHERE inicial.isin = v.codigoDeValor) ";
	String select = "SELECT new sibbac.business.wrappers.database.data.SaldoInicialCuentaData(inicial.isin, "
		+ selectDescIsin
		+ ", inicial.cdcodigocuentaliq, SUM(inicial.titulos), SUM(inicial.imefectivo) "
		+ ") FROM Tmct0SaldoInicialCuenta inicial";
	String maxFechaClause = " AND inicial.fecha = (select MAX(s.fecha) from Tmct0SaldoInicialCuenta s WHERE s.fecha <=:fliquidacionDe "
		+ "AND s.cdcodigocuentaliq = inicial.cdcodigocuentaliq AND s.isin = inicial.isin AND s.cdaliass = inicial.cdaliass GROUP BY s.isin, s.cdcodigocuentaliq) ";
	String groupBy = " GROUP BY inicial.isin, inicial.cdcodigocuentaliq ";
	String orderBy = "";
	StringBuilder sb = new StringBuilder(select);
	boolean addedWhere = false;
	// se construye la query segun los filtros
	for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
	    if (entry.getValue() != null && !"".equals(entry.getKey())) {

			if (!entry.getKey().equals("fliquidacionDe") && !entry.getKey().equals("fliquidacionA")) {
			    String clause = "";
			    if (addedWhere) {
			    	clause = " AND ";
			    } else {
					clause = " WHERE ";
					addedWhere = true;
			    }
			    sb.append(clause);
			    
			    String valor = (String) entry.getValue();
		    	if(valor.indexOf("#") != -1){
		    		entry.setValue(valor.replace("#",""));
		    		sb.append("inicial.").append(entry.getKey()).append("<>:").append(entry.getKey());
		    	}else	
		    		sb.append("inicial.").append(entry.getKey()).append("=:").append(entry.getKey());

			}
	    }
	}
	sb.append(maxFechaClause).append(groupBy).append(orderBy);

	try {
	    Query query = em.createQuery(sb.toString());
	    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
		query.setParameter(entry.getKey(), entry.getValue());
	    }
	    resultList.addAll(query.getResultList());
	} catch (PersistenceException | IllegalArgumentException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw ex;
	}
	return resultList;
    }

    private boolean checkIfKeyExist(String key, Map<String, Serializable> params) {
	boolean exist = false;
	if (params.get(key) != null && !"".equals(params.get(key))) {
	    exist = true;
	}
	return exist;
    }

}
