package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import sibbac.business.wrappers.database.dao.Tmct0SaldoInicialCuentaDAOImpl;
import sibbac.business.wrappers.database.dao.Tmct0SaldoInicialCuentaDao;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.data.SaldoInicialCuentaData;
import sibbac.business.wrappers.database.data.TuplaIsinAlias;
import sibbac.business.wrappers.database.model.Tmct0SaldoInicialCuenta;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0SaldoInicialCuentaBo extends AbstractBo<Tmct0SaldoInicialCuenta, Long, Tmct0SaldoInicialCuentaDao> {
  
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0SaldoInicialCuentaBo.class);
  
  @Autowired
  Tmct0MovimientoCuentaAliasBo movimientoCuentaAliasBo;
  
  @Autowired
  Tmct0MovimientoManualBo movimientoManualBo;
  
  @Autowired
  Tmct0SaldoInicialCuentaDAOImpl daoImpl;

  public List<Tmct0SaldoInicialCuenta> findAllByFecha(Date date) {
    return dao.findAllByFecha(date);
  }

  public Tmct0SaldoInicialCuenta findOneByIsinAndAliasAndCuentaLiquidacion(String isin, String alias,
      String cuentaLiquidacion) {

    return dao.findOneByIsinAndAliasAndCuentaLiquidacion(isin, alias, cuentaLiquidacion);
  }

  public Tmct0SaldoInicialCuenta findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(
      String isin, String cdcodigocuentaliq, String cdaliass, Date fecha) {
    return this.dao.findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(isin,
        cdcodigocuentaliq, cdaliass, fecha);

  }

  @Transactional
  public void processMovimientoCuentaAlias() {
    final List<MovimientoCuentaAliasData> listaMovimientosNoProcesados;
    int updated;

    LOG.debug("Metodo processMovimientoCuentaAlias .......");
    // Se extraen todos los movimientos automáticos cuyo estado
    // guardadosaldo sea false
    listaMovimientosNoProcesados = movimientoCuentaAliasBo.findMovimientosAutomaticosNoGrabados();
    Objects.requireNonNull(listaMovimientosNoProcesados, "Movimientos no procesados a devuelto Nulo");
    LOG.debug("Se extraen: {}  movimientos con la columna  guardadosaldo = false.....", 
        listaMovimientosNoProcesados.size());
    // se actualizan los saldos teniendo en cuenta que están ordenados por
    // fecha ascendente
    try {
      for (MovimientoCuentaAliasData data : listaMovimientosNoProcesados) {
          movimientoManualBo.updateMovAlias(data);
          LOG.debug("SaldoInicial actualizado para los movimientos {}", data.agrupacion());
          updated = movimientoCuentaAliasBo.updateGuardadoSaldo(data, true);
          LOG.debug("valor de la propiedad guardadosaldo cambiado a true en {} movimientos {}", updated, 
              data.agrupacion());
      }
    }
    catch(RuntimeException rex) {
      LOG.error("Error inesperado al calcular saldos iniciales", rex);
      throw rex;
    }
  }

  public Tmct0SaldoInicialCuenta calculateSaldoInicial(String isin, String alias, String cuentaLiquidacion,
      Date fecha) {

    Date currentDate = new Date();

    // Obtengo el movimiento que tengo que procesar del día
    MovimientoCuentaAliasData movimientoDiario = movimientoCuentaAliasBo.findOneSumByDateGroupedByPack(fecha, isin,
        alias, cuentaLiquidacion);

    Tmct0SaldoInicialCuenta newsic = new Tmct0SaldoInicialCuenta();

    // Obtenemos el saldo inicial cuenta con el pack
    Tmct0SaldoInicialCuenta tsic = this
        .findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(isin, cuentaLiquidacion,
            alias, fecha);
    // Si no existe, habremos de crearla nueva en la tabla
    if (tsic == null) {

      newsic.setCdaliass(alias);
      newsic.setFecha(currentDate);

      if (movimientoDiario != null) {
        newsic.setImefectivo(movimientoDiario.getEfectivo());
        newsic.setTitulos(movimientoDiario.getTitulos());
        newsic.setCdcodigocuentaliq(movimientoDiario.getCdcodigocuentaliq());
      }
      else {
        newsic.setTitulos(new BigDecimal(0));
        newsic.setImefectivo(new BigDecimal(0));
        newsic.setCdcodigocuentaliq("");
      }
      newsic.setIsin(isin);

    }
    // Si existe, creamos uno nuevo con los titulos actualizados
    else {
      if (movimientoDiario != null) {
        newsic.setImefectivo(movimientoDiario.getEfectivo());
        newsic.setTitulos(movimientoDiario.getTitulos());
        newsic.setCdcodigocuentaliq(movimientoDiario.getCdcodigocuentaliq());
      }
      else {
        newsic.setTitulos(tsic.getTitulos());
        newsic.setImefectivo(tsic.getImefectivo());
        newsic.setCdcodigocuentaliq(tsic.getCdcodigocuentaliq());
      }
      newsic.setCdaliass(alias);
      newsic.setFecha(currentDate);
      newsic.setIsin(isin);
    }
    return newsic;
  }

  public Tmct0SaldoInicialCuenta getSaldoInicial(String isin, String cdaliass, String cdcodigocuentaliq, Date fecha) {
    Tmct0SaldoInicialCuenta inicial = findFirstByIsinAndCdcodigocuentaliqAndCdaliassAndFechaLessThanEqualOrderByFechaDesc(
        isin, cdcodigocuentaliq, cdaliass, fecha);
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    Date now = new Date();

    if (dateFormat.format(now).equals(dateFormat.format(fecha))
        && (inicial == null || !dateFormat.format(now).equals(dateFormat.format(inicial.getFecha())))) {
      // Si ha preguntado por el de hoy y no se ha generado ya, generamos
      // uno solamente para mostrarlo
      inicial = calculateSaldoInicial(isin, cdaliass, cdcodigocuentaliq, fecha);
    }

    return inicial;
  }

  public List<TuplaIsinAlias> getTuplas(Date fecha) {
    return this.dao.getTuplas(fecha);
  }

  public List<TuplaIsinAlias> getTuplasByIsin(Date fecha, String isin) {
    return this.dao.getTuplasByIsin(fecha, isin);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsin(Date fecha, String isin) {
    return this.dao.getTuplasByNotIsin(fecha, isin);
  }

  public List<TuplaIsinAlias> getTuplasByAlias(Date fecha, String alias) {
    return this.dao.getTuplasByAlias(fecha, alias);
  }

  public List<TuplaIsinAlias> getTuplasByNotAlias(Date fecha, String alias) {
    return this.dao.getTuplasByNotAlias(fecha, alias);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByIsinAndAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByNotIsinAndNotAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByIsinAndNotAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByNotIsinAndAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndNotAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndNotAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndNotCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByCuenta(Date fecha, String cdcodigocuentaliq) {
    return this.dao.getTuplasByCuenta(fecha, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotCuenta(Date fecha, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotCuenta(fecha, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByAliasAndCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByAliasAndCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotAliasAndNotCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotAliasAndNotCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByAliasAndNotCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByAliasAndNotCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotAliasAndCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotAliasAndCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<SaldoInicialCuentaData> findAllGroupByIsinAndCodCuentaliqAndFLiquidacionOrderByIsinAndFecha(
      Map<String, Serializable> filter) {
    return daoImpl.findAllGroupByIsinAndCodCuentaliqAndFLiquidacionOrderByIsinAndFecha(filter);
  }

}
