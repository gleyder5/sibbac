package sibbac.business.wrappers.database.dao;

import java.text.MessageFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0aloHelper;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Repository
public class AlcEstadoContDaoImp {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  private static final String UPDATE_CDESTADOCONT = "UPDATE tmct0_alc_estadocont SET estadocont = :estadocont"
                                                    + " WHERE nuorden = :nuorden AND nbooking = :nbooking AND nucnfclt = :nucnfclt AND nucnfliq = :nucnfliq"
                                                    + "       AND tipoestado = :tipoestado";

  private static final String INSERT_CDESTADOCONT = "INSERT INTO tmct0_alc_estadocont (estadocont, nbooking, nuorden, nucnfliq, nucnfclt, tipoestado)"
                                                    + " VALUES (:estadocont, :nbooking, :nuorden, :nucnfliq, :nucnfclt, :tipoestado)";

  private static final String MERGE_CDESTADOCONT = "MERGE INTO BSNBPSQL.TMCT0_ALC_ESTADOCONT AS T"
                                                   + "    USING (SELECT alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ, {0} AS TIPOESTADO, {1} AS ESTADOCONT"
                                                   + "           FROM BSNBPSQL.TMCT0ALC alc, BSNBPSQL.TMCT0ORD ord"
                                                   + "           WHERE ord.NUREFORD IN ({2}) AND alc.NUORDEN = ord.NUORDEN) AS S"
                                                   + "    ON T.NUORDEN=S.NUORDEN AND T.NBOOKING=S.NBOOKING AND T.NUCNFCLT=S.NUCNFCLT"
                                                   + "       AND T.NUCNFLIQ=S.NUCNFLIQ AND T.TIPOESTADO=S.TIPOESTADO AND T.ESTADOCONT=S.ESTADOCONT"
                                                   + "    WHEN MATCHED THEN UPDATE SET T.ESTADOCONT={3}"
                                                   + "    WHEN NOT MATCHED THEN INSERT (nuorden, nbooking, nucnfclt, nucnfliq, tipoestado, estadocont)"
                                                   + "                          VALUES (S.NUORDEN, S.NBOOKING, S.NUCNFCLT, S.NUCNFLIQ, S.TIPOESTADO, {4})";

//  private static final String MERGE_CDESTADOCONT_BY_ALC = "MERGE INTO BSNBPSQL.TMCT0_ALC_ESTADOCONT AS T"
//                                                          + "    USING ({0} AS NUORDEN, {1} AS NBOOKING, {2} AS NUCNFCLT, {3} AS NUCNFLIQ, {4} AS TIPOESTADO, {5} AS ESTADOCONT) AS S"
//                                                          + "    ON T.NUORDEN=S.NUORDEN AND T.NBOOKING=S.NBOOKING AND T.NUCNFCLT=S.NUCNFCLT"
//                                                          + "       AND T.NUCNFLIQ=S.NUCNFLIQ AND T.TIPOESTADO=S.TIPOESTADO AND T.ESTADOCONT=S.ESTADOCONT"
//                                                          + "    WHEN MATCHED THEN UPDATE SET T.ESTADOCONT={6}"
//                                                          + "    WHEN NOT MATCHED THEN INSERT (nuorden, nbooking, nucnfclt, nucnfliq, tipoestado, estadocont)"
//                                                          + "                          VALUES (S.NUORDEN, S.NBOOKING, S.NUCNFCLT, S.NUCNFLIQ, S.TIPOESTADO, {7})";
  
  
  private static final String MERGE_CDESTADOCONT_CTA_123 = "MERGE INTO BSNBPSQL.TMCT0_ALC_ESTADOCONT AS T"
      + "    USING (SELECT alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ, {0} AS TIPOESTADO, {1} AS ESTADOCONT"
      + "           FROM BSNBPSQL.TMCT0ALC alc "
      + "           WHERE alc.NUCNFCLT IN ({2}) ) AS S"
      + "    ON T.NUORDEN=S.NUORDEN AND T.NBOOKING=S.NBOOKING AND T.NUCNFCLT=S.NUCNFCLT"
      + "       AND T.NUCNFLIQ=S.NUCNFLIQ AND T.TIPOESTADO=S.TIPOESTADO AND T.ESTADOCONT=S.ESTADOCONT"
      + "    WHEN MATCHED THEN UPDATE SET T.ESTADOCONT={3}"
      + "    WHEN NOT MATCHED THEN INSERT (nuorden, nbooking, nucnfclt, nucnfliq, tipoestado, estadocont)"
      + "                          VALUES (S.NUORDEN, S.NBOOKING, S.NUCNFCLT, S.NUCNFLIQ, S.TIPOESTADO, {4})";



  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @PersistenceContext
  private EntityManager em;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  public int updateCdestadocont(int estadocont, String nbooking, String nuorden, Short nucnfliq, String nucnfclt, int tipoestado) {
    Query query = em.createNativeQuery(UPDATE_CDESTADOCONT);
    putParameters(query, estadocont, nbooking, nuorden, nucnfliq, nucnfclt, tipoestado);
    return query.executeUpdate();
  } // updateCdestadocont

  public int insertCdestadocont(int estadocont, String nbooking, String nuorden, Short nucnfliq, String nucnfclt, int tipoestado) {
    Query query = em.createNativeQuery(INSERT_CDESTADOCONT);
    putParameters(query, estadocont, nbooking, nuorden, nucnfliq, nucnfclt, tipoestado);
    return query.executeUpdate();
  } // updateOrInsertCdestadocont

  public int updateOrInsertCdestadocont(int estadocont, String nbooking, String nuorden, Short nucnfliq, String nucnfclt, int tipoestado) {
    int resultado = updateCdestadocont(estadocont, nbooking, nuorden, nucnfliq, nucnfclt, tipoestado);
    if (resultado == 0) {
      resultado = insertCdestadocont(estadocont, nbooking, nuorden, nucnfliq, nucnfclt, tipoestado);
    }
    return resultado;
  } // updateOrInsertCdestadocont

  /**
   * Actualiza o inserta registros en la tabla tmct0_alc_estadocont.
   * 
   * @param tipoEstado Tipo de estado a buscar.
   * @param estadoContActual Estado contable a buscar.
   * @param estadoContNuevo Estado contable a establecer.
   * @param nurefordList Lista de ordenes a actualizar.
   * @return
   */
  public int mergeCdestadoCont(Integer tipoEstado, Integer estadoContActual, Integer estadoContNuevo, String nurefordList) {
    int countReg= em.createNativeQuery(MessageFormat.format(MERGE_CDESTADOCONT, tipoEstado, estadoContActual, nurefordList, estadoContNuevo, estadoContNuevo))
             .executeUpdate();
    if(countReg == 0){
      String nurefordListCta123= DesgloseTmct0aloHelper.getListPartenonNurefordCta123(nurefordList);
      countReg = em.createNativeQuery(MessageFormat.format(MERGE_CDESTADOCONT_CTA_123, tipoEstado, estadoContActual, nurefordListCta123, estadoContNuevo, estadoContNuevo))
          .executeUpdate();
    }
    return countReg;
  } // mergeCdestadoCont

//  public void mergeCdestadoContByClave(Integer tipoEstado, Integer estadoContActual, Integer estadoContNuevo, List<Tmct0alcId> alcIdList) {
//    for (Tmct0alcId tmct0alcId : alcIdList) {
//      em.createNativeQuery(MessageFormat.format(MERGE_CDESTADOCONT_BY_ALC, tmct0alcId.getNuorden(), tmct0alcId.getNbooking(),
//                                                tmct0alcId.getNucnfclt(), tmct0alcId.getNucnfliq(), tipoEstado, estadoContActual, estadoContNuevo,
//                                                estadoContNuevo)).executeUpdate();
//    }
//  } // mergeCdestadoCont

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  private void putParameters(Query query, int estadocont, String nbooking, String nuorden, Short nucnfliq, String nucnfclt, int tipoestado) {
    query.setParameter("estadocont", estadocont);
    query.setParameter("nbooking", nbooking);
    query.setParameter("nuorden", nuorden);
    query.setParameter("nucnfliq", nucnfliq);
    query.setParameter("nucnfclt", nucnfclt);
    query.setParameter("tipoestado", tipoestado);
  } // putParameters

} // AlcEstadoContDaoImp
