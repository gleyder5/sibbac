package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Tmct0swi;


@Component
public interface Tmct0swiDao extends JpaRepository< Tmct0swi, String > {
	

	Tmct0swi findBySwift( String swift );
	

}
