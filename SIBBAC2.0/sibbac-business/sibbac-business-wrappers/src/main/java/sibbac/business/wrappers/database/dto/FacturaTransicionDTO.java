package sibbac.business.wrappers.database.dto;


public class FacturaTransicionDTO {

	private Long	numeroFactura;
	private String	estado;
	private String	fecha;

	public FacturaTransicionDTO( Long numeroFactura, String estado, String fecha ) {
		super();
		this.numeroFactura = numeroFactura;
		this.estado = estado;
		this.fecha = fecha;
	}

	/**
	 * @return the numeroFactura
	 */
	public Long getNumeroFactura() {
		return numeroFactura;
	}

	/**
	 * @param numeroFactura the numeroFactura to set
	 */
	public void setNumeroFactura( Long numeroFactura ) {
		this.numeroFactura = numeroFactura;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado( String estado ) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha( String fecha ) {
		this.fecha = fecha;
	}

}
