package sibbac.business.wrappers.database.dao.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.CodigoError;
import sibbac.business.wrappers.database.model.Tmct0AfiError;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0ord;


public class Tmct0AfiErrorSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( Tmct0AfiErrorSpecifications.class );

	public static Specification< Tmct0AfiError > feEjeLiqDesde( final Date fecha ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::nudnicif] [feejeliq=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo(
							root.< Tmct0alo > get( "tmct0alo" ).< Tmct0alc > get( "tmct0alcs" ).< Date > get( "feejeliq" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > nudnicif( final String nudnicif ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nudnicifPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::nudnicif] [nudnicif=={}]", nudnicif );
				if ( nudnicif != null ) {
					nudnicifPredicate = builder.equal( root.< String > get( "nudnicif" ), nudnicif );
				}
				return nudnicifPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > sentido( final Character sentido ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate sentidoPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::sentido] [sentido=={}]", sentido );
				if ( sentido != null ) {
					sentidoPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "cdtpoper" ), sentido );
				}
				return sentidoPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > isin( final String isin ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate isinPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::isin] [isin=={}]", isin );
				if ( isin != null ) {
					isinPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "cdisin" ), isin );
				}
				return isinPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > alias( final String alias ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate aliasPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::alias] [alias=={}]", alias );
				if ( alias != null ) {
					aliasPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "cdalias" ), alias );
				}
				return aliasPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > tpnactit( final Character tpnactit ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate sentidoPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::sentido] [sentido=={}]", tpnactit );
				if ( tpnactit != null ) {
					sentidoPredicate = builder.equal( root.< String > get( "tpnactit" ), tpnactit );
				}
				return sentidoPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > mercadoNacInt( final Character nacint ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate mercadoNacIntPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::mercadoNacInt] [mercadoNacInt=={}]", nacint );
				if ( nacint != null ) {
					mercadoNacIntPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< Tmct0bok > get( "tmct0bok" )
							.< Tmct0ord > get( "tmct0ord" ).< Character > get( "cdclsmdo" ), nacint );
				}
				return mercadoNacIntPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > tipodocumento( final Character tipodocumento ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate tipdocPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::tpidenti] [tpidenti=={}]", tipodocumento );
				if ( tipodocumento != null ) {
					tipdocPredicate = builder.equal( root.< Character > get( "tpidenti" ), tipodocumento );
				}
				return tipdocPredicate;
			}
		};
	}
	
	public static Specification< Tmct0AfiError > nbValors( final String nbValors ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nudnicifPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::nbvalors] [nbvalors=={}]", nbValors );
				if ( nbValors != null ) {
					nudnicifPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "nbvalors" ), nbValors );
				}
				return nudnicifPredicate;
			}
		};
	}

	/*
	 * ESTO ES CON UN IN
	 * http://stackoverflow.com/questions/9321916/jpa-criteriabuilder
	 * -how-to-use- in-comparison-operator
	 */
	// public static Specification<Tmct0AfiError> nbclient(
	// final List<String> nbclient) {
	// return new Specification<Tmct0AfiError>() {
	// public Predicate toPredicate(Root<Tmct0AfiError> root,
	// CriteriaQuery<?> query, CriteriaBuilder builder) {
	// Predicate nbclientPredicate = null;
	// LOG.trace(
	// "[Tmct0AfiErrorSpecifications::nbclient] [nbclient=={}]",
	// nbclient);
	// if (nbclient != null && nbclient.size() > 0) {
	// Expression<String> exp = root.get("nbclient");
	// nbclientPredicate = exp.in(nbclient);
	// }
	// return nbclientPredicate;
	// }
	// };
	// }

	public static Specification< Tmct0AfiError > nbclient( final String nbclient ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbclientPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::nbclient] [nbclient=={}]", nbclient );
				if ( nbclient != null ) {
					nbclientPredicate = builder.equal( root.< String > get( "nbclient" ), nbclient );
				}
				return nbclientPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > nbclient1( final String nbclient1 ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbclientPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::nbclient1] [nbclien1t=={}]", nbclient1 );
				if ( nbclient1 != null ) {
					nbclientPredicate = builder.equal( root.< String > get( "nbclient1" ), nbclient1 );
				}
				return nbclientPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > nbclient2( final String nbclient2 ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbclientPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::nbclient2] [nbclient2=={}]", nbclient2 );
				if ( nbclient2 != null ) {
					nbclientPredicate = builder.equal( root.< String > get( "nbclient2" ), nbclient2 );
				}
				return nbclientPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > codError( final String codError ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate codErrorPredicate = null;
				LOG.trace( "[Tmct0AfiErrorSpecifications::errors] [errors=={}]", codError );
				if ( codError != null ) {
					final Subquery< Long > codErrorQuery = query.subquery( Long.class );
					final Root< CodigoError > codigoError = codErrorQuery.from( CodigoError.class );
					final Join< Tmct0AfiError, CodigoError > join = codigoError.join( "tmct0AfiError" );
					codErrorQuery.select( join.< Long > get( "id" ) );
					codErrorQuery.where( builder.equal( codigoError.< Long > get( "codigo" ), codError ) );
					return builder.in( root.get( "id" ) ).value( codErrorQuery );
				}
				return codErrorPredicate;
			}
		};
	}

	public static Specification< Tmct0AfiError > listadoFriltrado( final Date fechaDesde, final Date fechaHasta,
			final Character mercadoNacInt, final Character sentido, final String nudnicif, final String codError, final String nbclient,
			final String nbclient1, final String nbclient2, final String nbvalors, final Character tpnactit, final String isin,
			final String alias, final Character tipodocumento ) {
		return new Specification< Tmct0AfiError >() {

			public Predicate toPredicate( Root< Tmct0AfiError > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0AfiError > specs = null;

				if ( fechaDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( feEjeLiqDesde( fechaDesde ) );
					}
				}

				if ( nudnicif != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nudnicif( nudnicif ) );
					} else {
						specs = specs.and( Specifications.where( nudnicif( nudnicif ) ) );
					}

				}

				if ( codError != null ) {
					if ( specs == null ) {
						specs = Specifications.where( codError( codError ) );
					} else {
						specs = specs.and( Specifications.where( codError( codError ) ) );
					}

				}

				if ( nbclient != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbclient( nbclient ) );
					} else {
						specs = specs.and( Specifications.where( nbclient( nbclient ) ) );
					}

				}

				if ( nbclient1 != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbclient1( nbclient1 ) );
					} else {
						specs = specs.and( Specifications.where( nbclient1( nbclient1 ) ) );
					}

				}

				if ( nbclient2 != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbclient2( nbclient2 ) );
					} else {
						specs = specs.and( Specifications.where( nbclient2( nbclient2 ) ) );
					}

				}
				if ( nbvalors != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbValors( nbvalors ) );
					} else {
						specs = specs.and( Specifications.where( nbValors( nbvalors ) ) );
					}

				}

				if ( sentido != null ) {
					if ( specs == null ) {
						specs = Specifications.where( sentido( sentido ) );
					} else {
						specs = specs.and( Specifications.where( sentido( sentido ) ) );
					}

				}

				if ( tpnactit != null ) {
					if ( specs == null ) {
						specs = Specifications.where( tpnactit( tpnactit ) );
					} else {
						specs = specs.and( Specifications.where( tpnactit( tpnactit ) ) );
					}

				}
				
				if ( tipodocumento != null ) {
					if ( specs == null ) {
						specs = Specifications.where( tipodocumento( tipodocumento ) );
					} else {
						specs = specs.and( Specifications.where( tipodocumento( tipodocumento ) ) );
					}

				}


				if ( mercadoNacInt != null ) {
					if ( specs == null ) {
						specs = Specifications.where( mercadoNacInt( mercadoNacInt ) );
					} else {
						specs = specs.and( Specifications.where( mercadoNacInt( mercadoNacInt ) ) );
					}

				}

				if ( isin != null ) {
					if ( specs == null ) {
						specs = Specifications.where( isin( isin ) );
					} else {
						specs = specs.and( Specifications.where( isin( isin ) ) );
					}

				}

				if ( alias != null ) {
					if ( specs == null ) {
						specs = Specifications.where( alias( alias ) );
					} else {
						specs = specs.and( Specifications.where( alias( alias ) ) );
					}

				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

}
