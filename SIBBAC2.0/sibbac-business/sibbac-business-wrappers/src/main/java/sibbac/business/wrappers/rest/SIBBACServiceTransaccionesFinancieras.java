package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.dto.TransaccionesDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceTransaccionesFinancieras implements SIBBACServiceBean {

	
	@Autowired
	private Tmct0aloBo				aloBo;
	
	
	private static final Logger		LOG									= LoggerFactory.getLogger( SIBBACServiceTransaccionesFinancieras.class );
	private static final String		NOMBRE_SERVICIO						= "SIBBACServiceTransaccionesFinancieras";
	private static final String		CMD_INFORME_LIST					= "getInformeList";
	
	private static final String		FILTER_FECHA_CONTRATCION_DESDE  	= "fcontratacionDe";
	private static final String		FILTER_FECHA_CONTRATCION_A	  		= "fcontratacionA";	
	private static final String		FILTER_TIPO_INFORME					= "tipoInforme";
	private static final String		FILTER_MERCADO						= "mercado";
	
	public SIBBACServiceTransaccionesFinancieras(){}	
	
	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		final String prefix = "[SIBBACServiceTransaccionesFinancieras::process] ";
		LOG.debug( prefix + "Processing a web request..." );

		final String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );

		final WebResponse webResponse = new WebResponse();
		
		switch ( command ) {
		case CMD_INFORME_LIST:
			this.getInformeList( webRequest, webResponse );
			break;		
		default:
			webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
			break;
		}
		return webResponse;
	}
	
	/**
	 * Servicio que devuelve una lista con los operaciones existentes.
	 * 
	 * @param webRequest
	 * @param webResponse
	 */
	private void getInformeList( WebRequest webRequest, WebResponse webResponse ) {
		LOG.debug( "[" + NOMBRE_SERVICIO + " :: getInformeList] Init" );
		Map< String, String > filters = webRequest.getFilters();

		Short tipoInforme 		= convertirStringAShort( filters.get( FILTER_TIPO_INFORME ));
		Date fcontratacionDesde = convertirStringADate( filters.get( FILTER_FECHA_CONTRATCION_DESDE ));
		Date fcontratacionA		= convertirStringADate( filters.get( FILTER_FECHA_CONTRATCION_A ));
		String mercado    		= processString( filters.get( FILTER_MERCADO ));

		List< TransaccionesDTO > transacciones = null;
		
		try {
			switch(tipoInforme){
				case 1:
	
					transacciones = aloBo.findByFeTradeAndCdmercadAndImgatdsv( fcontratacionDesde, fcontratacionA, mercado );
					break;
				case 2:
					transacciones = aloBo.findByFeTradeAndCdmercadAndImgatdsvSinOpen( fcontratacionDesde, fcontratacionA, mercado );
					break;
				case 3:
					transacciones = aloBo.findByFeTradeAndCdmercadAndImgatdsvOpen( fcontratacionDesde, fcontratacionA, mercado );
					break;	
			}
			
		} catch ( Exception e ) {
			LOG.error( "[" + NOMBRE_SERVICIO + 
			        " :: getInformeList] Error obteniendo resultados de la tabla TMCT0ALO, TMCT0CDT" , e);
			webResponse.setError("Error interno del servidor, notifique al administrador");
			return;
		}
		
		if(transacciones != null && transacciones.size() > 0){
			final Map< String, List< TransaccionesDTO >> resultados = new HashMap<>();
			resultados.put( "resultados_transacciones", transacciones );
			webResponse.setResultados( resultados );
		}else{
			webResponse.setError("La claves de busqueda no han encotrado ningun resultado. Cambielas e Intente de nuevo.");
		}
		
		LOG.debug( "[" + NOMBRE_SERVICIO + " :: getTitularesList] Fin " );		
	}
		
	
	private String processString( final String filterString ) {
		String processedString = null;
		if ( StringUtils.isNotEmpty( filterString ) ) {
			processedString = filterString;
		}
		return processedString;
	}	
	
	/**
	 * @param filtros
	 * @return
	 */
	private Short convertirStringAShort( final String fechaString ) {
		Short fecha = null;
		if ( StringUtils.isNotEmpty( fechaString ) ) {
			fecha = Short.parseShort( fechaString );
		}
		return fecha;
	}	
	
	/**
	 * @param filtros
	 * @return
	 */
	private Date convertirStringADate( final String fechaString ) {
		Date fecha = null;
		if ( StringUtils.isNotEmpty( fechaString ) ) {
			fecha = FormatDataUtils.convertStringToDate( fechaString );
		}
		return fecha;
	}	
	
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_INFORME_LIST );

		return commands;
	}

	@Override
	public List< String > getFields() {

		return null;
	}
	
} //SIBBACServiceTransaccionesFinancieras.

