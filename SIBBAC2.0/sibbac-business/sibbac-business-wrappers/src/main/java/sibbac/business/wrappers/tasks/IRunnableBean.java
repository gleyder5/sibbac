package sibbac.business.wrappers.tasks;

import java.util.List;

import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.thread.WrapperExecutor;
import sibbac.common.SIBBACBusinessException;

public abstract class IRunnableBean<T extends RecordBean> extends WrapperExecutor {

  public abstract void preRun(ObserverProcess observer) throws SIBBACBusinessException;

  public abstract void run(ObserverProcess observer, List<T> beans, int order) throws SIBBACBusinessException;

  public abstract void postRun(ObserverProcess observer) throws SIBBACBusinessException;

  public abstract IRunnableBean<T> clone();

  @Override
  public int getThreadSize() {
    return 0;
  }

  @Override
  public int getTransactionSize() {
    return 0;
  }

}
