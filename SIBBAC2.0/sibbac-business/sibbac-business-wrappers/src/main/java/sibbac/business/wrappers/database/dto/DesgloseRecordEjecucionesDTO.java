package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DesgloseRecordEjecucionesDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7819152823336724456L;
  private String nucnfclt;
  private String nuejecuc;
  private BigDecimal nutiteje;
  private String nuordenmkt;
  private String nuexemkt;
  private String plataformaNegociacion;
  private String divisa;
  private BigDecimal precio;
  private Date feliquidacion;
  private Date feejecuc;
  private Date hoejecuc;
  private BigDecimal imcomisn;
  private BigDecimal imfinsvb;
  private BigDecimal canonContr;
  private BigDecimal canonComp;
  private BigDecimal canonLiq;
  private BigDecimal efectivo;
  private Integer clientPayCanonContr;
  private Integer clientPayCanonComp;
  private Integer clientPayCanonLiq;

  public DesgloseRecordEjecucionesDTO(String nucnfclt,
                                      String nuejecuc,
                                      BigDecimal nutiteje,
                                      String nuordenmkt,
                                      String nuexemkt,
                                      String plataformaNegociacion,
                                      String divisa,
                                      BigDecimal precio,
                                      Date feliquidacion,
                                      Date feejecuc,
                                      Date hoejecuc,
                                      BigDecimal imcomisn,
                                      BigDecimal imfinsvb,
                                      BigDecimal canonContr,
                                      BigDecimal canonComp,
                                      BigDecimal canonLiq,
                                      BigDecimal efectivo,
                                      Integer clientPayCanonContr,
                                      Integer clientPayCanonComp,
                                      Integer clientPayCanonLiq) {
    super();
    this.nucnfclt = nucnfclt;
    this.nuejecuc = nuejecuc;
    this.nutiteje = nutiteje;
    this.nuordenmkt = nuordenmkt;
    this.nuexemkt = nuexemkt;
    this.plataformaNegociacion = plataformaNegociacion;
    this.divisa = divisa;
    this.precio = precio;
    this.feliquidacion = feliquidacion;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.imcomisn = imcomisn;
    this.imfinsvb = imfinsvb;
    this.canonContr = canonContr;
    this.canonComp = canonComp;
    this.canonLiq = canonLiq;
    this.efectivo = efectivo;
    this.clientPayCanonContr = clientPayCanonContr;
    this.clientPayCanonComp = clientPayCanonComp;
    this.clientPayCanonLiq = clientPayCanonLiq;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public BigDecimal getNutiteje() {
    return nutiteje;
  }

  public String getNuordenmkt() {
    return nuordenmkt;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  public String getPlataformaNegociacion() {
    return plataformaNegociacion;
  }

  public String getDivisa() {
    return divisa;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public Date getFeliquidacion() {
    return feliquidacion;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public Date getHoejecuc() {
    return hoejecuc;
  }

  public BigDecimal getImfinsvb() {
    return imfinsvb;
  }

  public BigDecimal getCanonContr() {
    return canonContr;
  }

  public BigDecimal getCanonComp() {
    return canonComp;
  }

  public BigDecimal getCanonLiq() {
    return canonLiq;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public void setNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje;
  }

  public void setNuordenmkt(String nuordenmkt) {
    this.nuordenmkt = nuordenmkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  public void setPlataformaNegociacion(String plataformaNegociacion) {
    this.plataformaNegociacion = plataformaNegociacion;
  }

  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setFeliquidacion(Date feliquidacion) {
    this.feliquidacion = feliquidacion;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public void setImfinsvb(BigDecimal imfinsvb) {
    this.imfinsvb = imfinsvb;
  }

  public void setCanonContr(BigDecimal canonContr) {
    this.canonContr = canonContr;
  }

  public void setCanonComp(BigDecimal canonComp) {
    this.canonComp = canonComp;
  }

  public void setCanonLiq(BigDecimal canonLiq) {
    this.canonLiq = canonLiq;
  }

  public BigDecimal getEfectivo() {
    return efectivo;
  }

  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  public Integer getClientPayCanonContr() {
    return clientPayCanonContr;
  }

  public Integer getClientPayCanonComp() {
    return clientPayCanonComp;
  }

  public Integer getClientPayCanonLiq() {
    return clientPayCanonLiq;
  }

  public void setClientPayCanonContr(Integer clientPayCanonContr) {
    this.clientPayCanonContr = clientPayCanonContr;
  }

  public void setClientPayCanonComp(Integer clientPayCanonComp) {
    this.clientPayCanonComp = clientPayCanonComp;
  }

  public void setClientPayCanonLiq(Integer clientPayCanonLiq) {
    this.clientPayCanonLiq = clientPayCanonLiq;
  }

  @Override
  public String toString() {
    return "DesgloseRecordEjecucionesDTO [nucnfclt=" + nucnfclt + ", nuejecuc=" + nuejecuc + ", nutiteje=" + nutiteje
           + ", nuordenmkt=" + nuordenmkt + ", nuexemkt=" + nuexemkt + ", plataformaNegociacion="
           + plataformaNegociacion + ", divisa=" + divisa + ", precio=" + precio + ", feliquidacion=" + feliquidacion
           + ", feejecuc=" + feejecuc + ", hoejecuc=" + hoejecuc + ", imcomisn=" + imcomisn + ", imfinsvb=" + imfinsvb
           + ", canonContr=" + canonContr + ", canonComp=" + canonComp + ", canonLiq=" + canonLiq + ", efectivo="
           + efectivo + ", clientPayCanonContr=" + clientPayCanonContr + ", clientPayCanonComp=" + clientPayCanonComp
           + ", clientPayCanonLiq=" + clientPayCanonLiq + "]";
  }

}
