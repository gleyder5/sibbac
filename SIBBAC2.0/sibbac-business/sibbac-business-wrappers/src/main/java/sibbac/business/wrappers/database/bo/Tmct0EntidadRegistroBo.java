package sibbac.business.wrappers.database.bo;


import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0EntidadRegistroDao;
import sibbac.business.wrappers.database.dao.specifications.EntidadRegistroSpecifications;
import sibbac.business.wrappers.database.data.EntidadRegistroData;
import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;
import sibbac.business.wrappers.database.model.Tmct0TipoEr;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0EntidadRegistroBo extends AbstractBo< Tmct0EntidadRegistro, Long, Tmct0EntidadRegistroDao > {

	@Autowired
	Tmct0TipoErBo	tipoErBo;

	public List< EntidadRegistroData > findAllData() {
		List< EntidadRegistroData > resultList = new LinkedList< EntidadRegistroData >();
		resultList.addAll( dao.findAllData() );
		return resultList;
	}

	public List< EntidadRegistroData > findAllData( final String nombre, final String bic, final Integer idTipoEr) {
		List< Tmct0EntidadRegistro > resultList = new LinkedList< Tmct0EntidadRegistro >();
		List< EntidadRegistroData > dataResultList = new LinkedList< EntidadRegistroData >();
		Tmct0TipoEr tipoer = null;
		if(idTipoEr!=null){
			tipoer = tipoErBo.findById( idTipoEr );
		}
		resultList = dao.findAll(EntidadRegistroSpecifications.listadoFriltrado( nombre, bic, tipoer ));
		for(Tmct0EntidadRegistro reg:resultList){
			dataResultList.add( new EntidadRegistroData( reg ) );
		}
		return dataResultList;
	}

	@Transactional
	public Tmct0EntidadRegistro insertEr( String nombre, String bic, Integer idTipoEr ) {
		// Comprobamos si existe el tipo er y lo traemos
		Tmct0TipoEr tipoEr = tipoErBo.findById( idTipoEr );
		if ( tipoEr != null ) {
			Tmct0EntidadRegistro er = new Tmct0EntidadRegistro( bic, nombre, tipoEr );
			er = dao.save( er );
			return er;
		}
		return null;
	}

	@Transactional
	public boolean deleteById( Long idEr ) {
		if ( idEr != null ) {
			try {
				dao.delete( idEr );
				return true;
			} catch ( EmptyResultDataAccessException e ) {
				LOG.trace( " Error borrando ER con id" + idEr );
			}
		}
		return false;
	}

	@Transactional
	public boolean updateById( Long idEr, String nombre, String bic, Integer idTipoEr ) {
		// Comprobamos si existe el er y lo traemos
		Tmct0EntidadRegistro er = this.findById( idEr );
		if ( er != null ) {
			// Comprobamos si existe el tipo er y lo traemos
			Tmct0TipoEr tipoEr = tipoErBo.findById( idTipoEr );
			if ( tipoEr != null ) {
				er.setBic( bic );
				er.setNombre( nombre );
				er.setTipoer( tipoEr );
				return true;
			}
		}
		return false;
	}
}
