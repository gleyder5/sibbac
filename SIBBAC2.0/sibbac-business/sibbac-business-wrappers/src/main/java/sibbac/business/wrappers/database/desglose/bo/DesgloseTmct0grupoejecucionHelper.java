package sibbac.business.wrappers.database.desglose.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;

public class DesgloseTmct0grupoejecucionHelper {

  public static void inicializeTmct0grupoejecucion(Tmct0grupoejecucion grupoEjecucion,
                                                                  Tmct0alo alo,
                                                                  EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucionAlocacionData) {
    grupoEjecucion.setNuagreje(ejecucionAlocacionData.getNuagreje());
    grupoEjecucion.setImpreciomkt(ejecucionAlocacionData.getImpreciomkt());
    grupoEjecucion.setCdsettmkt(ejecucionAlocacionData.getCdsettmkt());
    grupoEjecucion.setCdcamara(ejecucionAlocacionData.getCdcamara());
    grupoEjecucion.setCdtipoop(ejecucionAlocacionData.getCdtipoop());
    grupoEjecucion.setTradingvenue(ejecucionAlocacionData.getTradingvenue());
    grupoEjecucion.setFeejecuc(ejecucionAlocacionData.getFeejecuc());
    grupoEjecucion.setFevalor(ejecucionAlocacionData.getFevalor());
    grupoEjecucion.setAsigcomp(ejecucionAlocacionData.getAsigcomp());

    grupoEjecucion.setAuditFechaCambio(new Date());

    grupoEjecucion.setNutitulos(BigDecimal.ZERO);
    grupoEjecucion.setTmct0alo(alo);
    grupoEjecucion.setTmct0ejecucionalocations(new ArrayList<Tmct0ejecucionalocation>());
  }

}
