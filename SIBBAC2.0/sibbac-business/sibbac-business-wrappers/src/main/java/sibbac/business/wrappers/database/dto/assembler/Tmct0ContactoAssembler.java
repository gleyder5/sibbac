package sibbac.business.wrappers.database.dto.assembler;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.dao.ContactoDao;
import sibbac.business.wrappers.database.dto.InfoMailContactosDTO;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.common.SIBBACBusinessException;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_CONTACTO
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0ContactoAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0ContactoAssembler.class);
	
	
	@Autowired
	ContactoDao contactoDao;
	
	@Autowired
	Tmct0AddressAssembler addressAssembler;

	/**
	 * Realiza la conversion entre los datos del front y la entity a gestionar en back
	 * 
	 * @param paramsObjects -> datos del front
	 * @return Entity - Entity a gestionar en la parte back
	 */
	public List<Contacto> getTmct0ContactoEntityList(Map<?, ?> datosCliente) throws ParseException {
		LOG.debug("Inicio de la transformación getTmct0ContactoEntity");

		List<Contacto> listContacto = new ArrayList<Contacto>();

		List<?> contactos = (List<?>) datosCliente.get("contactos");

		if (CollectionUtils.isNotEmpty(contactos)) {
			for (Object contacto : contactos) {
				Map<?, ?> datosContacto = (Map<?, ?>) contacto;
				Contacto miEntity = new Contacto();
				if (datosContacto.get("id") != null) {
					Long lidContacto = Long.valueOf(datosContacto.get("id").toString());
					if (lidContacto != 0) {
						miEntity = this.contactoDao.findOne(lidContacto);
					}
				} else {
					// Se asume registro nuevo.
					this.popularCamposContactoEstatico(miEntity);
				}
				for (Map.Entry<?, ?> entry : datosContacto.entrySet()) {
					switch (entry.getKey().toString()) {
					case "descripcion":
						miEntity.setDescripcion((String) entry.getValue());
						break;
					case "telefono1":
						miEntity.setTelefono1((String) entry.getValue());
						break;
					case "telefono2":
						miEntity.setTelefono2((String) entry.getValue());
						break;
					case "movil":
						miEntity.setMovil((String) entry.getValue());
						break;
					case "fax":
						miEntity.setFax((String) entry.getValue());
						break;
					case "email":
						miEntity.setEmail((String) entry.getValue());
						break;
					case "comentarios":
						miEntity.setComentarios((String) entry.getValue());
						break;
					case "posicionCargo":
						miEntity.setPosicionCargo((String) entry.getValue());
						break;
					case "activo":
						miEntity.setActivo(
							StringHelper.getCharFromString((String) entry.getValue()));
						break;
					case "nombre":
						miEntity.setNombre((String) entry.getValue());
						break;	
					case "apellido1":
						miEntity.setApellido1((String) entry.getValue());
						break;
					case "apellido2":
						miEntity.setApellido2((String) entry.getValue());
						break;
					case "defecto":
						miEntity.setDefecto((Boolean) entry.getValue());
						break;	
          case "gestor":
            miEntity.setGestor((Boolean) entry.getValue());
            break;  
          case "cdEtrali":
            miEntity.setCdEtrali((String) entry.getValue());
            break;              
					}
				}
				
				miEntity.setAddress(addressAssembler.getTmct0AddressEntity(datosContacto));
				listContacto.add(miEntity);
			}
		}
		return listContacto;
	}
	
	public Contacto getTmct0ContactoEntity(Map<?, ?> paramsObjects) throws ParseException {
		LOG.debug("Inicio de la transformación getTmct0ClientEntity");

		Contacto miEntity = new Contacto();

		// TODO - Falta corroborar estructura
		if (paramsObjects.get("datosContacto") != null) {

			// Se comprueba si se tiene el idClient que determina si es una entidad nueva o ya existente
			Map<?, ?> datosContacto = (Map<?, ?>) paramsObjects.get("datosContacto");
			if (datosContacto.get("id") != null) {
				Long lidContacto = Long.valueOf(datosContacto.get("id").toString());
				if (lidContacto != 0) {
					miEntity = this.contactoDao.findById(lidContacto);
				} 
			} else {
				// Se asume registro nuevo.
				this.popularCamposContactoEstatico(miEntity);
			}
			
			if (datosContacto.get("apellido1") != null) {
				miEntity.setApellido1(datosContacto.get("apellido1").toString());
			}
			
			if (datosContacto.get("apellido2") != null) {
				miEntity.setApellido2(datosContacto.get("apellido2").toString());
			}
			
			if (datosContacto.get("nombre") != null) {
				miEntity.setNombre(datosContacto.get("nombre").toString());
			}
			
			if (datosContacto.get("posicionCargo") != null) {
				miEntity.setPosicionCargo(datosContacto.get("posicionCargo").toString());
			}
			
			if (datosContacto.get("telefono1") != null) {
				miEntity.setTelefono1(datosContacto.get("telefono1").toString());
			}
			
			if (datosContacto.get("email") != null) {
				miEntity.setEmail(datosContacto.get("email").toString());
			}
			
			if (datosContacto.get("fax") != null) {
				miEntity.setFax(datosContacto.get("fax").toString());
			}
			
			miEntity.setAddress(addressAssembler.getTmct0AddressEntity(datosContacto));		
			
		}
		
		return miEntity;
	}
	
	/**
	 * 	Se populan campos de contacto con ix estatica para 
	 * 	compatibilidad con otros sistemas.
	 * 	@param miEntity
	 */
	private void popularCamposContactoEstatico(Contacto miEntity) {
		miEntity.setVersion(new Long(1));
		miEntity.setActivo(new Character('S'));
		miEntity.setDefecto(false);
		miEntity.setAuditUser("process");
		miEntity.setAuditDate(new Date());
	}
	
	/**
	 *	Obtiene lista con DTOs de mail de contacto.
	 *	@param listaInfoMailContactos
	 *	@return List<InfoMailContactosDTO>
	 *	@throws SIBBACBusinessException 	  
	 */
	public List<InfoMailContactosDTO> getListaInfoMailContactos(
			List<Object[]> listaInfoMailContactos) throws SIBBACBusinessException {
		List<InfoMailContactosDTO> listInfoMailContactosDTO = null;
		if (CollectionUtils.isNotEmpty(listaInfoMailContactos)) {
			listInfoMailContactosDTO = new ArrayList<InfoMailContactosDTO>();
			for (Object[] obj : listaInfoMailContactos) {
				InfoMailContactosDTO infMailCntDTO = new InfoMailContactosDTO();
				infMailCntDTO.setInformesSubjectAndBody((String) (obj[0]));
				infMailCntDTO.setAliasContacto((BigInteger) (obj[1]));
				infMailCntDTO.setEmailContacto((String) (obj[2]));
				infMailCntDTO.setIdContacto((BigInteger) (obj[3]));
				infMailCntDTO.setActivoContacto((Character) (obj[4]));
				infMailCntDTO.setActivoServicio((Short) (obj[5]));
				infMailCntDTO.setJobNameServicio((String) (obj[6]));
				infMailCntDTO.setIdAlias((String) (obj[7]));
				infMailCntDTO.setNbDescripcionIdioma((String) (obj[8]));
				listInfoMailContactosDTO.add(infMailCntDTO);
			}
		}
		return listInfoMailContactosDTO;
	} 
}	
