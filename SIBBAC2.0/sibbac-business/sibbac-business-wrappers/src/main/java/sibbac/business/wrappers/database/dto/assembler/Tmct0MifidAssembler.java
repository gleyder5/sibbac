package sibbac.business.wrappers.database.dto.assembler;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0CliMifidDao;
import sibbac.business.wrappers.database.model.Tmct0CliMifid;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0CLI_MIFID
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0MifidAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0MifidAssembler.class);

	@Autowired
	Tmct0CliMifidDao tmct0CliMifidDao;
	
	/**
	 * Realiza la conversion entre los datos del front y la entity a gestionar en back
	 * 
	 * @param paramsObjects -> datos del front
	 * @return Entity - Entity a gestionar en la parte back
	 */
	public List<Tmct0CliMifid> getTmct0MifidEntityList(Map<?, ?> datosCliente) {
		LOG.debug("Inicio de la transformación getTmct0MifidEntityList");
		List<Tmct0CliMifid> listMifid = new ArrayList<Tmct0CliMifid>();
		List<?> mifids = (List<?>) datosCliente.get("mifid");
		if (CollectionUtils.isNotEmpty(mifids)) {
			for (Object mifid : mifids) {
				Map<?, ?> datosMifid = (Map<?, ?>) mifid;
				Tmct0CliMifid miEntity = new Tmct0CliMifid();
				if (datosMifid.get("id") != null) {
					BigInteger lidMifid =new BigInteger(datosMifid.get("id").toString());
					if (!lidMifid.equals(BigInteger.ZERO)) {
						miEntity = this.tmct0CliMifidDao.findOne(lidMifid);
					}
				}
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
				for (Map.Entry<?, ?> entry : datosMifid.entrySet()) {
					switch (entry.getKey().toString()) {
					case "category":
						miEntity.setCategory((String) entry.getValue());
						break;
					case "tConven":
						miEntity.settConven((String) entry.getValue());
						break;
					case "tIdoneid":
						miEntity.settIdoneid((String) entry.getValue());
						break;
					case "fhConv":
						try {
							
							if(!String.valueOf(entry.getValue()).isEmpty()){
								miEntity.setFhConv(formatter.parse(String.valueOf(entry.getValue())));
							}
														
						} catch (ParseException e) {
							LOG.error("Error: ", e.getClass().getName(), e.getMessage());
						}
						break;
					case "fhIdon":
						try {
							
							if(!String.valueOf(entry.getValue()).isEmpty()){
								miEntity.setFhIdon(formatter.parse(String.valueOf(entry.getValue())));
							}
							
						} catch (ParseException e) {
							LOG.error("Error: ", e.getClass().getName(), e.getMessage());
						}
						break;
					}
				}
				if (!miEntity.isEmpty()) listMifid.add(miEntity);
			}
		}
		return listMifid;
	}
}	
