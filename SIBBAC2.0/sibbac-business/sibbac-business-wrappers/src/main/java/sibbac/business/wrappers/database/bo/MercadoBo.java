package sibbac.business.wrappers.database.bo;


import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.MercadoDao;
import sibbac.business.wrappers.database.data.MercadoData;
import sibbac.business.wrappers.database.model.Tmct0Mercado;
import sibbac.database.bo.AbstractBo;


@Service
public class MercadoBo extends AbstractBo< Tmct0Mercado, Long, MercadoDao > {

	public List< MercadoData > findAllData() {
		List< MercadoData > resultList = new LinkedList< MercadoData >();
		resultList.addAll( dao.findAllData() );
		return resultList;
	}

	public List< Tmct0Mercado > findByIdIn( List< Long > valor ) {

		return dao.findByIdIn( valor );
	}
}
