package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;
import java.util.Date;

public class HBB6RecordBean extends HBBNRecordBean {

  private static final long serialVersionUID = 7520314440766135459L;

  public enum TipoEnvio {
    INICIO(BigDecimal.ZERO), FINAL(BigDecimal.ONE);
    private final BigDecimal value;

    /**
     * @param text
     */
    private TipoEnvio(final BigDecimal value) {
      this.value = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return value.toString();
    }

    public static TipoEnvio getTipoEnvio(BigDecimal external) {
      TipoEnvio[] fields = TipoEnvio.values();
      TipoEnvio personType = null;
      for (int i = 0; i < fields.length; i++) {
        if (fields[i].value.compareTo(external) == 0) {
          personType = fields[i];
          break;
        }
      }
      return personType;
    }
  }

  private Date fechaCuadre;
  private BigDecimal totTitulares;
  private BigDecimal totDomicilios;
  private IndicadorInicioFinalHBB incicadorInicioFinal;
  private TipoEnvio tipoEnvio;

  public Date getFechaCuadre() {
    return fechaCuadre;
  }

  public void setFechaCuadre(Date fechaCuadre) {
    this.fechaCuadre = fechaCuadre;
  }

  public BigDecimal getTotTitulares() {
    return totTitulares;
  }

  public void setTotTitulares(BigDecimal totTitulares) {
    this.totTitulares = totTitulares;
  }

  public BigDecimal getTotDomicilios() {
    return totDomicilios;
  }

  public void setTotDomicilios(BigDecimal totDomicilios) {
    this.totDomicilios = totDomicilios;
  }

  public TipoEnvio getTipoEnvio() {
    return tipoEnvio;
  }

  public void setTipoEnvio(TipoEnvio tipoEnvio) {
    this.tipoEnvio = tipoEnvio;
  }

  public IndicadorInicioFinalHBB getIncicadorInicioFinal() {
    return incicadorInicioFinal;
  }

  public void setIncicadorInicioFinal(IndicadorInicioFinalHBB incicadorInicioFinal) {
    this.incicadorInicioFinal = incicadorInicioFinal;
  }

}
