package sibbac.business.wrappers.database.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.database.bo.AbstractBo;

@Service
public class EtiquetasBo extends AbstractBo<Etiquetas, Long, EtiquetasDao> {

	public Map<String, Etiquetas> getListaEtiquetas() {

		LOG.trace("Se inicia la creacion de la lista de las etiquetas");
		Map<String, Etiquetas> resultados = new HashMap<String, Etiquetas>();

		List<Etiquetas> listaEtiquetas = (List<Etiquetas>) this.findAll();

		for (Etiquetas etiquetas : listaEtiquetas) {

			resultados.put(etiquetas.getId().toString(), etiquetas);
		}

		LOG.trace("Se ha creado la lista de las etiquetas");
		return resultados;

	}
}
