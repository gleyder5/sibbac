package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;

public class SettlementChainBicName implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 6076621092249256523L;
  // bic
  private String bic = "";
  // name
  private String name = "";

  public SettlementChainBicName() {

  }

  public SettlementChainBicName(String bic, String name) {
    this.bic = bic;
    this.name = name;
  }

  public String getBic() {
    return bic;
  }

  public String getName() {
    return name;
  }

}
