package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.data.Tmct0AlcData;
import sibbac.business.wrappers.database.dto.ControlTitularesDTO;
import sibbac.business.wrappers.database.dto.EnvioCorretajeTmct0alcDataDTO;
import sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista17DTO;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;

@Repository
public interface Tmct0alcDao extends JpaRepository<Tmct0alc, Tmct0alcId>, JpaSpecificationExecutor<Tmct0alc> {

  @Query("SELECT max(c.id.nucnfliq) FROM Tmct0alc c WHERE c.tmct0alo.id=:aloId")
  Short getMaxNucnfliqByTmct0aloId(@Param("aloId") Tmct0aloId aloId);

  @Query("SELECT c.id FROM Tmct0alc c WHERE c.tmct0alo.id=:aloId and c.cdestadoasig > 0")
  List<Tmct0alcId> findAllTmct0alcIdByTmct0aloIdAndDeAlta(@Param("aloId") Tmct0aloId aloId);

  @Query("SELECT c.id FROM Tmct0alc c WHERE c.tmct0alo.id in (:listAloId) and c.cdestadoasig > 0")
  List<Tmct0alcId> findAllTmct0alcIdByListTmct0aloIdAndDeAlta(@Param("listAloId") List<Tmct0aloId> listAloId);

  @Query(value = "SELECT alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ,"
      + "     bok.CDTPOPER, bok.CDALIAS, bok.DESCRALI, bok.CDISIN, bok.NBVALORS,"
      + "     alc.FEEJELIQ, alc.FEOPELIQ, alc.NUTITLIQ, alc.IMEFEAGR, alc.IMNFILIQ,"
      + "     cc.CD_CODIGO AS CUENTACOMPENSACION, cl.CD_CODIGO AS CUENTALIQUIDACION, alco.REFERENCIAS3 AS REFERENCIAS3"
      + " FROM BSNBPSQL.TMCT0ALC alc"
      + "      INNER JOIN BSNBPSQL.TMCT0BOK bok ON alc.NUORDEN = bok.NUORDEN AND alc.NBOOKING = bok.NBOOKING"
      + "      INNER JOIN BSNBPSQL.TMCT0ALI ali ON ali.cdaliass = bok.cdalias AND ali.fhfinal = 99991231 AND ali.ind_Neting = :netting"
      + "      INNER JOIN (SELECT dc.NUORDEN, dc.NBOOKING, dc.NUCNFCLT, dc.NUCNFLIQ, m.CUENTA_COMPENSACION_DESTINO"
      + "                  FROM BSNBPSQL.TMCT0MOVIMIENTOECC m, BSNBPSQL.TMCT0DESGLOSECAMARA dc"
      + "                  WHERE m.IDDESGLOSECAMARA=dc.IDDESGLOSECAMARA AND m.CDESTADOMOV=9) mdc"
      + "        ON mdc.NUORDEN = alc.NUORDEN AND mdc.NBOOKING = alc.NBOOKING AND mdc.NUCNFCLT = alc.NUCNFCLT AND mdc.NUCNFLIQ = alc.NUCNFLIQ"
      + "      LEFT JOIN BSNBPSQL.TMCT0_ALC_ORDEN alco ON alco.NUORDEN = alc.NUORDEN AND alco.NBOOKING = alc.NBOOKING AND alco.NUCNFCLT = alc.NUCNFCLT AND alco.NUCNFLIQ = alc.NUCNFLIQ,"
      + "      BSNBPSQL.TMCT0_CUENTAS_DE_COMPENSACION cc"
      + "      INNER JOIN BSNBPSQL.TMCT0_CUENTA_LIQUIDACION cl ON cc.ID_CUENTA_LIQUIDACION=cl.ID"
      + " WHERE alc.CDESTADOASIG >= :estadoAsignacion AND alc.FEOPELIQ = :feopeliq AND alc.CDESTADOENTREC > :estadoEntrecInicial AND alc.CDESTADOENTREC < :estadoEntrecLiq"
      + "       AND mdc.CUENTA_COMPENSACION_DESTINO = cc.CD_CODIGO"
      + "       AND NOT EXISTS (SELECT 1"
      + "                       FROM BSNBPSQL.TMCT0_FALLIDOS_ALC alc2"
      + "                       WHERE alc2.NUORDEN=alc.NUORDEN AND alc2.NBOOKING=alc.NBOOKING AND alc2.NUCNFCLT=alc.NUCNFCLT AND alc2.NUCNFLIQ=alc.NUCNFLIQ)"
      + " UNION"
      + " SELECT alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ,"
      + "        alc.CDTPOPER, alc.CDALIAS, alc.DESCRALI, alc.CDISIN, alc.NBVALORS,"
      + "        alc.FEEJELIQ, alc.FEOPELIQ, SUM(-s3l.NUTIULOLIQUIDADOS) AS TITULOS, SUM(-s3l.IMPORTENETOLIQUIDADO) AS EFECTIVO,"
      + "        alc.IMNFILIQ, alc.CUENTACOMPENSACION, alc.CUENTALIQUIDACION, s3l.IDFICHERO AS REFERENCIAS3"
      + " FROM (SELECT alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ,"
      + "              bok.CDTPOPER, bok.CDALIAS, bok.DESCRALI, bok.CDISIN, bok.NBVALORS,"
      + "              alc.FEEJELIQ, alc.FEOPELIQ, alc.NUTITLIQ, alc.IMEFEAGR, alc.IMNFILIQ,"
      + "              cc.CD_CODIGO AS CUENTACOMPENSACION, cl.CD_CODIGO AS CUENTALIQUIDACION"
      + "       FROM BSNBPSQL.TMCT0ALC alc"
      + "            INNER JOIN BSNBPSQL.TMCT0BOK bok ON alc.NUORDEN=bok.NUORDEN AND alc.NBOOKING=bok.NBOOKING"
      + "            INNER JOIN BSNBPSQL.TMCT0ALI ali ON ali.cdaliass=bok.cdalias AND ali.fhfinal=99991231 AND ali.ind_Neting = :netting"
      + "            INNER JOIN (SELECT dc.NUORDEN, dc.NBOOKING, dc.NUCNFCLT, dc.NUCNFLIQ, m.CUENTA_COMPENSACION_DESTINO"
      + "                        FROM BSNBPSQL.TMCT0MOVIMIENTOECC m, BSNBPSQL.TMCT0DESGLOSECAMARA dc"
      + "                        WHERE m.IDDESGLOSECAMARA=dc.IDDESGLOSECAMARA AND m.CDESTADOMOV=9) mdc"
      + "              ON MDC.NUORDEN=alc.NUORDEN AND MDC.NBOOKING=alc.NBOOKING AND MDC.NUCNFCLT=alc.NUCNFCLT AND MDC.NUCNFLIQ=alc.NUCNFLIQ,"
      + "            BSNBPSQL.TMCT0_CUENTAS_DE_COMPENSACION cc"
      + "            INNER JOIN BSNBPSQL.TMCT0_CUENTA_LIQUIDACION cl ON cc.ID_CUENTA_LIQUIDACION=cl.ID"
      + "       WHERE alc.CDESTADOASIG >= :estadoAsignacion AND alc.FEOPELIQ = :feopeliq AND alc.CDESTADOENTREC = :estadoEntrecLiqParcial"
      + "             AND mdc.CUENTA_COMPENSACION_DESTINO = cc.CD_CODIGO"
      + "       AND NOT EXISTS (SELECT 1"
      + "                       FROM BSNBPSQL.TMCT0_FALLIDOS_ALC alc2"
      + "                       WHERE alc2.NUORDEN=alc.NUORDEN AND alc2.NBOOKING=alc.NBOOKING AND alc2.NUCNFCLT=alc.NUCNFCLT AND alc2.NUCNFLIQ=alc.NUCNFLIQ)"
      + "      ) alc"
      + "      INNER JOIN BSNBPSQL.TMCT0_S3LIQUIDACION s3l ON s3l.NBOOKING=alc.NBOOKING AND s3l.NUCNFCLT=alc.NUCNFCLT AND s3l.NUCNFLIQ=alc.NUCNFLIQ"
      + " WHERE cdestados3='LIQ'" + " GROUP BY alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ,"
      + "          alc.CDTPOPER, alc.CDALIAS, alc.DESCRALI, alc.CDISIN, alc.NBVALORS,"
      + "          alc.FEEJELIQ, alc.FEOPELIQ, alc.IMNFILIQ,"
      + "          alc.CUENTACOMPENSACION, alc.CUENTALIQUIDACION, s3l.IDFICHERO", nativeQuery = true)
  public List<Object[]> findAllAlcByEnviadasSinLiquidar(@Param("estadoAsignacion") Integer estadoAsignacion,
      @Param("feopeliq") Date feopeliq, @Param("estadoEntrecInicial") Integer estadoEntrecInicial,
      @Param("estadoEntrecLiq") Integer estadoEntrecLiq,
      @Param("estadoEntrecLiqParcial") Integer estadoEntrecLiqParcial, @Param("netting") Character netting);

  @Query("SELECT alc FROM Tmct0alc alc "
      + " WHERE alc.feejeliq = ?1 AND alc.cdestadoasig >= ?2 "
      + " AND (CONCAT(alc.estadoentrec.idestado,'') = ?3 OR "
      + "CONCAT(alc.estadoentrec.idestado,'') = ?4 OR "
      + "CONCAT(alc.estadoentrec.idestado,'') = ?5 OR "
      + "CONCAT(alc.estadoentrec.idestado,'') = ?6) "
      + " AND EXISTS (SELECT 1 FROM Tmct0movimientoecc m, Tmct0CuentasDeCompensacion cc "
      + "   WHERE m.tmct0desglosecamara.tmct0alc = alc and m.cuentaCompensacionDestino = cc.cdCodigo and cc.cuentaClearing = 'S' and m.cdestadomov ='9' )")
  List<Tmct0alc> findByEnvioFicheroMegaraClienteTactico(Date feejeliq, int AsigPdteLiquidar, String EntrecPdteFichero,
      String EntrecRechazadoFichero, String EntrecPdteAnulacion, String EntrecIncidencia);

  @Query(value = "SELECT DISTINCT alc.NUORDEN "
      + "FROM TMCT0ALC alc "
      + "INNER JOIN TMCT0DESGLOSECAMARA descam ON "
      + "alc.NUORDEN = descam.NUORDEN AND "
      + "alc.NBOOKING = descam.NBOOKING AND "
      + "alc.NUCNFCLT = descam.NUCNFCLT AND "
      + "alc.NUCNFLIQ = descam.NUCNFLIQ "
      + "INNER JOIN  TMCT0MOVIMIENTOECC mem "
      + "ON descam.IDDESGLOSECAMARA = mem.IDDESGLOSECAMARA AND mem.CDESTADOMOV = '9' "
      + "INNER JOIN TMCT0_CUENTAS_DE_COMPENSACION CC ON mem.CUENTA_COMPENSACION_DESTINO = CC.CD_CODIGO AND CC.CUENTA_CLEARING ='S' "
      + "WHERE alc.feejeliq >= ?1 AND alc.CDESTADOASIG >= ?2 AND descam.CDESTADOASIG >= ?2 AND "
      + "(CONCAT(descam.CDESTADOENTREC, '') = ?3 OR  CONCAT(descam.CDESTADOENTREC,'') = ?4 OR "
      + "CONCAT(descam.CDESTADOENTREC, '') = ?5 OR CONCAT(descam.CDESTADOENTREC,'') = ?6"
      + " OR CONCAT(descam.CDESTADOENTREC,'') = ?7 ) ", nativeQuery = true)
  List<String> findOrdenesParaEnvioS3(Date feejeliq, int asigPdteLiquidar, String entrecPdteFichero,
      String entrecRechazadoFichero, String entrecPdteAnulacion, String entrecPdteAnulacionParcial, String incidencia);

  /*
   * Para el estrategico de operativaNetting
   */
  @Query("SELECT alc FROM Tmct0alc alc, Tmct0bok bok, Tmct0ali ali"
      + " WHERE bok.id.nuorden=alc.id.nuorden AND bok.id.nbooking=alc.id.nbooking"
      + " AND ali.id.cdaliass=bok.cdalias AND ali.fhfinal=99991231 AND ali.indNeting = 'S'"
      + " AND alc.feejeliq >= ?1"
      + " AND ((alc.cdestadoasig >= ?2 AND (alc.estadoentrec.idestado = ?4 OR alc.estadoentrec.idestado = ?6))"
      + " OR (alc.cdestadoasig = ?3 AND alc.estadoentrec.idestado = ?5))"
      + " AND EXISTS (SELECT 1 FROM Tmct0movimientoecc m, Tmct0CuentasDeCompensacion cc"
      + "             WHERE m.tmct0desglosecamara.tmct0alc = alc and m.cuentaCompensacionDestino = cc.cdCodigo and cc.cuentaClearing = 'S' and m.cdestadomov ='9')"
      + " ORDER BY alc.estadoentrec DESC")
  List<Tmct0alc> findByEnvioFicheroMegaraClienteNetting(Date feejeliq, Integer AsigPdteLiquidar,
      Integer AsigTratandose, Integer EntrecPdteEnviar, Integer EntrecPdteEnviarAnulacion, Integer EntrecIncidencia);

  @Query("SELECT A FROM Tmct0alc A WHERE year(feejeliq)=:year AND cdestliq=:cdestliq "
      + "AND A.cdestadoasig IN (:lEstadosAsig) AND A.estadocont=:estadocont")
  @Transactional
  public List<Tmct0alc> findOrdenesParaContabilizar(@Param("year") int year, @Param("cdestliq") String cdestliq,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig, @Param("estadocont") Tmct0estado estadocont);

  @Transactional
  public List<Tmct0alc> findFirst100ByFeejeliqAndCdestliqAndCdestadoasigInAndEstadocont(@Param("feejeliq") Date date,
      @Param("cdestliq") String cdestliq, @Param("cdestadoasig") List<Integer> lEstadosAsig,
      @Param("estadocont") Tmct0estado estadocont);

  @Transactional
  public List<Tmct0alc> findFirst50ByCdestliqAndCdestadoasigInAndEstadocont(@Param("cdestliq") String cdestliq,
      @Param("cdestadoasig") List<Integer> lEstadosAsig, @Param("estadocont") Tmct0estado estadocont);

  @Transactional
  public List<Tmct0alc> findFirst100ByCdestliqAndCdestadoasigInAndEstadocont(@Param("cdestliq") String cdestliq,
      @Param("cdestadoasig") List<Integer> lEstadosAsig, @Param("estadocont") Tmct0estado estadocont);

  @Query("SELECT A FROM Tmct0alc A WHERE A IN (SELECT DC.tmct0alc FROM Tmct0desglosecamara DC,"
      + "Tmct0CuentasDeCompensacion CC WHERE DC.tmct0infocompensacion.cdctacompensacion=CC.cdCodigo AND "
      + "CC.cdCodigo='00P') AND A.cdestadoasig IN (:lEstadosAsig)")
  public List<Tmct0alc> findByEstadosAsig(@Param("lEstadosAsig") List<Integer> lEstadosAsig);

  @Query("SELECT A FROM Tmct0alc A WHERE A IN (SELECT DC.tmct0alc FROM Tmct0desglosecamara DC,"
      + "Tmct0CuentasDeCompensacion CC WHERE DC.tmct0infocompensacion.cdctacompensacion=CC.cdCodigo AND "
      + "CC.cdCodigo='00P') AND A.cdestadoasig IN (:lEstadosAsig)")
  public List<Tmct0alc> findByEstadosAsig(@Param("lEstadosAsig") List<Integer> lEstadosAsig, Pageable pageable);

  @Query("UPDATE Tmct0alc SET contabilizado = :contabilizado WHERE tipoMovimiento = :tipoMovimiento")
  public void updateContabilizadoByTipoMovimiento(@Param("tipoMovimiento") String tipoMovimiento,
      @Param("contabilizado") boolean contabilizado);

  @Query("SELECT A FROM Tmct0alc A WHERE A.id.nucnfliq = :nucnfliq and A.id.nucnfclt like :nucnfclt ")
  public Tmct0alc findByNucnfliqAndLikeNucnfclt(@Param("nucnfliq") Short nucnfliq, @Param("nucnfclt") String nucnfclt);

  @Query("SELECT A FROM Tmct0alc A WHERE A.id.nbooking = :nbooking and A.id.nucnfliq = :nucnfliq and A.id.nucnfclt = :nucnfclt ")
  public Tmct0alc findByNbookingAndNucnfliqAndNucnfclt(@Param("nbooking") String nbooking,
      @Param("nucnfliq") Short nucnfliq, @Param("nucnfclt") String nucnfclt);

  @Query("SELECT A FROM Tmct0alc A WHERE A.tmct0alo.id = :alloid ")
  public List<Tmct0alc> findByAlloId(@Param("alloid") Tmct0aloId alloId);

  @Query("SELECT A FROM Tmct0alc A WHERE A.tmct0alo.id = :alloid AND A.cdestadoasig > 0 ")
  public List<Tmct0alc> findByAlloIdActivos(@Param("alloid") Tmct0aloId alloId);

  @Query("SELECT ALC FROM Tmct0alc ALC, Tmct0desglosecamara DC "
      + "WHERE  DC = :desglosecamara  AND  DC.tmct0alc = ALC ")
  public Tmct0alc findByTmct0desglosecamaras(@Param("desglosecamara") Tmct0desglosecamara desglosecamara);

  @Query("select alcs from Tmct0alo alo join alo.tmct0alcs alcs where alcs.estadocont.idestado = :estado "
      + "and alo.cdalias in :codigosAlias")
  public List<Tmct0alc> findAlcsByEstadoContable(@Param("estado") final Integer estado,
      @Param("codigosAlias") final Set<String> codigosAlias);

  @Query("select alcs from Tmct0alo alo join alo.tmct0alcs alcs where alcs.estadocont.idestado = :estado and alo.cdalias in :codigosAlias and alcs.feejeliq <= :fechaHasta")
  public List<Tmct0alc> findAlcsByEstadoContableAndFeejeliq(@Param("estado") final Integer estado,
      @Param("codigosAlias") final Set<String> codigosAlias, @Param("fechaHasta") Date fechaHasta);

  @Query("select alcs from Tmct0alo alo join alo.tmct0alcs alcs where alcs.estadocont.idestado = :estado "
      + "and alo.cdalias in :codigosAlias and alcs.feejeliq >= :fechaDesde and alcs.feejeliq <= :fechaHasta")
  public List<Tmct0alc> findAlcsByEstadoContableToDate(@Param("estado") final Integer estado,
      @Param("codigosAlias") final Set<String> codigosAlias, @Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta);

  /***
   * Funcion que devuelve los ALCs cuyo flag "liquidadocliente" es '0' o es NULL
   * Y en alguno de los estados de la lista de estados cdestadoentrec
   * 
   * @param cdestadoentrec
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc " + " WHERE (alc.liquidadocliente = '0' OR alc.liquidadocliente IS NULL) "
      + " AND alc.estadoentrec.idestado in :cdestadoentrec ")
  public List<Tmct0alc> findAllGroupByCdEstadoentrecAndNoTratadoCliente(
      @Param(value = "cdestadoentrec") List<Integer> cdestadoentrec);

  /***
   * Funcion que devuelve los ALCs cuyo flag "liquidadocliente" es '0' o es
   * NULL, EstadoEnviadoCuentaVirtual es distinto de 'L' (leido) y alguno de los
   * estados de la lista de estados cdestadoentrec
   * 
   * @param cdestadoentrec
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc " + " WHERE (alc.liquidadocliente = '0' OR alc.liquidadocliente IS NULL) "
      + " AND alc.enviadoCuentaVirtual != 'L'" + " AND alc.estadoentrec.idestado in :cdestadoentrec ")
  public List<Tmct0alc> findAllByCdEstadoentrecAndNoTratadoClienteAndEstadoEnviadoCuentaVirtual(
      @Param(value = "cdestadoentrec") List<Integer> cdestadoentrec);


  @Query("SELECT alc FROM Tmct0alc alc inner join alc.tmct0alo alo "
      + "where alc.estadoentrec.idestado is not null "
      + "and NOT EXISTS (SELECT 1 FROM  AlcOrdenes ord WHERE ord.nbooking = alc.id.nbooking "
      + "and ord.nuorden = alc.id.nuorden "
      + "and ord.nucnfliq = alc.id.nucnfliq "
      + "and ord.nucnfclt = alc.id.nucnfclt "
      + "and ord.netting is not null) " // quitamos alc que han sido neteadas
      + "and EXISTS (select 1 from Tmct0desglosecamara dc join dc.tmct0infocompensacion ic, Tmct0CuentasDeCompensacion cc  "
      + "where dc.tmct0alc = alc and ic.cdctacompensacion = cc.cdCodigo and cc.cuentaClearing = 'S' ) "
      + "and ('N'=:filtrofcontratacionDe or alc.feejeliq >= :fcontratacionDe) "
      + "and ('N'=:filtrofcontratacionA or alc.feejeliq <= :fcontratacionA)  "
      + "and ('N'=:filtrofliquidacionDe or alc.feopeliq >= :fliquidacionDe) "
      + "and ('N'=:filtrofliquidacionA or alc.feopeliq <= :fliquidacionA)  "
      + "and ('N'=:filtroEstado or alc.estadoentrec.idestado=:estadoclearing) "
      + "and ('N'=:filtrobooking or alc.id.nbooking=:nbooking) " + "and ('N'=:filtronif or alc.cdniftit=:cdniftit) "
      + "and ('N'=:filtroNbTitular or alc.nbtitliq like :nbtitliq) "
      + "and ('N'=:filtroBiccle or alc.cdcustod = :biccle) "
      + "and ('N'=:filtroImporteNetoDesde or alc.imnfiliq >= :importeNetoDesde) "
      + "and ('N'=:filtroImporteNetoHasta or alc.imnfiliq <= :importeNetoHasta) "
      + "and ('N'=:filtroTitulosDesde or alc.nutitliq >= :titulosDesde) "
      + "and ('N'=:filtroTitulosHasta or alc.nutitliq <= :titulosHasta) and alc.cdestadoasig =:cdestadoasig")
  public List<Tmct0alc> findByEntregaRecepcionCliente(@Param("fcontratacionDe") Date fcontratacionDe,
      @Param("filtrofcontratacionDe") String filtrofcontratacionDe, @Param("fcontratacionA") Date fcontratacionA,
      @Param("filtrofcontratacionA") String filtrofcontratacionA, @Param("fliquidacionDe") Date fliquidacionDe,
      @Param("filtrofliquidacionDe") String filtrofliquidacionDe, @Param("fliquidacionA") Date fliquidacionA,
      @Param("filtrofliquidacionA") String filtrofliquidacionA, @Param("estadoclearing") Integer estado,
      @Param("filtroEstado") String filtroEstado, @Param("nbooking") String nbooking,
      @Param("filtrobooking") String filtrobooking, @Param("cdniftit") String cdniftit,
      @Param("filtronif") String filtronif, @Param("nbtitliq") String nbtitliq,
      @Param("filtroNbTitular") String filtroNbTitular, @Param("biccle") String biccle,
      @Param("filtroBiccle") String filtroBiccle, @Param("importeNetoDesde") BigDecimal importeNetoDesde,
      @Param("filtroImporteNetoDesde") String filtroImporteNetoDesde,
      @Param("importeNetoHasta") BigDecimal importeNetoHasta,
      @Param("filtroImporteNetoHasta") String filtroImporteNetoHasta, @Param("titulosDesde") BigDecimal titulosDesde,
      @Param("filtroTitulosDesde") String filtroTitulosDesde, @Param("titulosHasta") BigDecimal titulosHasta,
      @Param("filtroTitulosHasta") String filtroTitulosHasta, @Param("cdestadoasig") Integer cdestadoasig);

  @Query("SELECT alc FROM Tmct0alc alc, AlcOrdenes ord WHERE ord.netting = :netting "
      + "AND ord.nbooking = alc.id.nbooking " + "AND ord.nuorden = alc.id.nuorden "
      + "AND ord.nucnfliq = alc.id.nucnfliq " + "AND ord.nucnfclt = alc.id.nucnfclt")
  public List<Tmct0alc> findByNetting(@Param("netting") Netting netting);

  @Query("SELECT MAX(s3.fechaValor) FROM AlcOrdenes ord, Tmct0alc t0alc, S3Liquidacion s3 WHERE t0alc=:alc "
      + "AND ord.nbooking = t0alc.id.nbooking " + "AND ord.nuorden = t0alc.id.nuorden "
      + "AND ord.nucnfliq = t0alc.id.nucnfliq " + "AND ord.nucnfclt = t0alc.id.nucnfclt "
      + "AND ord.referenciaS3 = s3.idFichero")
  public Date getMaxFechaValorS3LiqByAlc(@Param("alc") Tmct0alc tmct0alc);

  @Query("SELECT MAX(s3.fechaValor) FROM S3Liquidacion s3 WHERE s3.idFichero =:referenciaS3")
  public Date getMaxFechaValorS3LiqByRefS3(@Param("referenciaS3") String referenciaS3);

  @Query(value = "select trim(bok.cdalias) alias,trim(ali.descrali) descripcion,alc.feejeliq,alc.feopeliq,"
      + "trim(alo.cdtpoper) cv,alc.nutitliq,alo.imcammed,alc.imefeagr,alc.imnfiliq,"
      + "trim(alo.cdisin) isin,trim(bok.nbooking) booking,trim(alo.nuorden) orden,"
      + "trim(alc.nucnfclt) desglose,trim(alc.nucnfliq) desTitular "
      + "from Tmct0alo alo, Tmct0bok bok, Tmct0ord ord, Tmct0ali ali, Tmct0alc alc "
      + "LEFT OUTER JOIN TMCT0_APUNTE_CONTABLE apt " + "ON alc.NUORDEN=apt.NUORDEN and alc.NBOOKING=apt.NBOOKING "
      + "and alc.NUCNFCLT=apt.NUCNFCLT and alc.NUCNFLIQ=apt.NUCNFLIQ "
      + "LEFT OUTER JOIN (select nucnfclt,nucnfliq from tmct0afi where nudnicif in ("
      + "select keyvalue from tmct0cfg where application = 'SBRMV' and process = 'SVB' and keyname = 'svb.cif')) afi "
      + "ON alc.NUCNFCLT=afi.NUCNFCLT and alc.NUCNFLIQ=afi.NUCNFLIQ "
      + "where alc.nuorden = alo.nuorden and alc.nbooking = alo.nbooking and alc.nucnfclt = alo.nucnfclt "
      + "and alo.nuorden = bok.nuorden and alo.nbooking = bok.nbooking and bok.nuorden = ord.nuorden "
      + "and bok.cdalias = ali.cdaliass " + "and (:ftrFechTradeIni='N' or alc.feejeliq>=:fechTradeIni) "
      + "and (:ftrFechTradeFin='N' or alc.feejeliq<=:fechTradeFin) "
      + "and (:ftrSettlementIni='N' or alc.feopeliq>=:settlementIni) "
      + "and (:ftrSettlementFin='N' or alc.feopeliq<=:settlementFin) "
      + "and (:ftrAlias='N' or bok.cdalias IN (:alias)) "
      + "and (:ftrTipologia='N' or alo.cdtpoper=:tipologia) "
      + "and (:ftrEstadocont='N' or alc.cdestadocont IN (:estadocont)) "
      // +
      // "and (:ftrLEstadoAsig='N' or alc.cdestadoasig IN (:lEstadoAsig)) "
      + "and (:ftrLEstadoAsig='N' or :lEstadoAsig<>:lEstadoAsig) "
      + "and (:ftrEstadoentrec='N' or :estadoentrec<>:estadoentrec) " + "and (:ftrCuenta='N' or :cuenta<>:cuenta) "
      // +
      // "and (:ftrEstadoentrec='N' or alc.cdestadoentrec IN (:estadoentrec)) "
      // + "and (:ftrCuenta='N' or apt.cuenta IN (:cuenta)) "
      + "and (:tipoOrden='ALL' or decode(afi.nucnfclt, NULL,'TER','CPT')=:tipoOrden)", nativeQuery = true)
  public List<Object[]> findInformeContable(@Param("fechTradeIni") Date fechTradeIni,
      @Param("ftrFechTradeIni") String ftrFechTradeIni, @Param("fechTradeFin") Date fechTradeFin,
      @Param("ftrFechTradeFin") String ftrFechTradeFin, @Param("settlementIni") Date settlementIni,
      @Param("ftrSettlementIni") String ftrSettlementIni, @Param("settlementFin") Date settlementFin,
      @Param("ftrSettlementFin") String ftrSettlementFin, @Param("alias") List<String> alias,
      @Param("ftrAlias") String ftrAlias, @Param("tipologia") String tipologia,
      @Param("ftrTipologia") String ftrTipologia, @Param("estadocont") List<String> estadocont,
      @Param("ftrEstadocont") String ftrEstadocont, @Param("lEstadoAsig") List<Integer> lEstadoAsig,
      @Param("ftrLEstadoAsig") String ftrCdestadoasig, @Param("estadoentrec") List<Integer> estadoentrec,
      @Param("ftrEstadoentrec") String ftrEstadoentrec, @Param("cuenta") List<Integer> cuenta,
      @Param("ftrCuenta") String ftrCuenta, @Param("tipoOrden") String tipoOrden);

  @Query(value = "select trim(bok.cdalias) alias,trim(ali.descrali) descripcion,alc.feejeliq,alc.feopeliq,"
      + "trim(alo.cdtpoper) cv,alc.nutitliq,alo.imcammed,alc.imefeagr,alc.imnfiliq,"
      + "trim(alo.cdisin) isin,trim(bok.nbooking) booking,trim(alo.nuorden) orden,"
      + "trim(alc.nucnfclt) desglose,trim(alc.nucnfliq) desTitular "
      + "from Tmct0alo alo, Tmct0bok bok, Tmct0ord ord, Tmct0ali ali, Tmct0alc alc "
      + "LEFT OUTER JOIN TMCT0_APUNTE_CONTABLE apt " + "ON alc.NUORDEN=apt.NUORDEN and alc.NBOOKING=apt.NBOOKING "
      + "and alc.NUCNFCLT=apt.NUCNFCLT and alc.NUCNFLIQ=apt.NUCNFLIQ "
      + "LEFT OUTER JOIN (select nucnfclt,nucnfliq from tmct0afi where nudnicif in ("
      + "select keyvalue from tmct0cfg where application = 'SBRMV' and process = 'SVB' and keyname = 'svb.cif')) afi "
      + "ON alc.NUCNFCLT=afi.NUCNFCLT and alc.NUCNFLIQ=afi.NUCNFLIQ "
      + "where alc.nuorden = alo.nuorden and alc.nbooking = alo.nbooking and alc.nucnfclt = alo.nucnfclt "
      + "and alo.nuorden = bok.nuorden and alo.nbooking = bok.nbooking and bok.nuorden = ord.nuorden "
      + "and bok.cdalias = ali.cdaliass " + "and (:ftrFechTradeIni='N' or alc.feejeliq>=:fechTradeIni) "
      + "and (:ftrFechTradeFin='N' or alc.feejeliq<=:fechTradeFin) "
      + "and (:ftrSettlementIni='N' or alc.feopeliq>=:settlementIni) "
      + "and (:ftrSettlementFin='N' or alc.feopeliq<=:settlementFin) "
      + "and (:ftrAlias='N' or bok.cdalias IN (:alias)) ", nativeQuery = true)
  public List<Object[]> findInformeContable(@Param("fechTradeIni") Date fechTradeIni,
      @Param("ftrFechTradeIni") String ftrFechTradeIni, @Param("fechTradeFin") Date fechTradeFin,
      @Param("ftrFechTradeFin") String ftrFechTradeFin, @Param("settlementIni") Date settlementIni,
      @Param("ftrSettlementIni") String ftrSettlementIni, @Param("settlementFin") Date settlementFin,
      @Param("ftrSettlementFin") String ftrSettlementFin, @Param("alias") List<String> alias,
      @Param("ftrAlias") String ftrAlias);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.cdestadoasig>=:idestado and alc.concEfMercado>=:fechamercado ORDER BY alc.imefeagr DESC ")
  public List<Tmct0alc> findByEstadoGreaterOrEqualThan(@Param("idestado") Integer idestado,
      @Param("fechamercado") Date fechamercado);

  public List<Tmct0alc> findByIdNucnfclt(String nucnfclt);

  @Query("SELECT alc FROM Tmct0alc alc WHERE (alc.estadoentrec=455 or alc.estadoentrec=450) and alc.concEfCliente>=:fechamercado ORDER BY alc.imefeagr DESC ")
  public List<Tmct0alc> findAlcsCliente(@Param("fechamercado") Date fechamercado);

  public Tmct0alc findByIdNuordenAndIdNbookingAndIdNucnfclt(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt);

  /**
   * Devuelve la lista de alc que cumplan las condiciones.
   * 
   * @param nuorden
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nuorden = :nuorden AND alc.cdestadoasig IN (:lEstadosAsig)")
  public List<Tmct0alc> findAlcByOrdenNuorden(@Param("nuorden") String nuorden,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nbooking = :nbooking AND alc.cdestadoasig IN (:lEstadosAsig)")
  public List<Tmct0alc> findAlcByOrdenNbooking(@Param("nbooking") String nbooking,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nucnfclt = :nucnfclt AND alc.cdestadoasig IN (:lEstadosAsig)")
  public List<Tmct0alc> findAlcByOrdenNucnfclt(@Param("nucnfclt") String nucnfclt,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig);

  /**
   * Devuelve la lista de alc que cumplan las condiciones.
   * 
   * @param nureford
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc, Tmct0ord ord WHERE ord.nureford = :nureford AND alc.id.nuorden = ord.nuorden AND alc.cdestadoasig IN (:lEstadosAsig) ORDER BY alc.id.nbooking ASC")
  public List<Tmct0alc> findAlcByOrdenNureford(@Param("nureford") String nureford,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig);


  /**
   * Devuelve la lista de alc que cumplan las condiciones.
   * 
   * @param nuorden
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc,Tmct0bok bok,Tmct0ali ali,Tmct0ord ord WHERE bok.id.nuorden=alc.id.nuorden "
      + "AND bok.id.nbooking=alc.id.nbooking AND ali.id.cdaliass=bok.cdalias AND ali.fhfinal=99991231 "
      + "and ali.retailSpecialOp='S' AND ord.cdisin=:cdisin AND ord.fecontra=:fecontra "
      + "AND alc.cdestadoasig IN (:lEstadosAsig)")
  public List<Tmct0alc> findAlcByCdisinAndFecontraAndCdCuenta(@Param("cdisin") String cdisin,
      @Param("fecontra") Date fecontra, @Param("lEstadosAsig") List<Integer> lEstadosAsig);


  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nuorden = :nuorden AND alc.id.nbooking = :nbooking AND alc.id.nucnfclt = :nucnfclt AND alc.id.nucnfliq = :nucnfliq")
  public Tmct0alc findByNuordenAndNbookingAndNucnfcltAndNucnfliq(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  /**
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nuorden = :nuorden AND alc.id.nbooking = :nbooking AND alc.id.nucnfclt = :nucnfclt AND alc.id.nucnfliq = :nucnfliq AND alc.estadoentrec IN (:estadoEntrec) AND alc.cdestadoasig IN (:lEstadosAsig)")
  public Tmct0alc findByNuordenAndNbookingAndNucnfcltAndNucnfliqAndCdestadoasigAndCdestadoentrec(
      @Param("nuorden") String nuorden, @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt,
      @Param("nucnfliq") Short nucnfliq, @Param("lEstadosAsig") List<Integer> lEstadosAsig,
      @Param("estadoEntrec") List<Tmct0estado> estadoEntrec);

  public Tmct0alc findByIdNuordenAndIdNbookingAndIdNucnfcltAndIdNucnfliq(String nuorden, String nbooking,
      String nucnfclt, Short nucnfliq);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nbooking = :nbooking and alc.id.nucnfliq = :nucnfliq and alc.id.nucnfclt = :nucnfclt AND alc.cdestadoasig IN (:lEstadosAsig) AND alc.estadocont = :estadosCont")
  public Tmct0alc findByNbookingAndNucnfliqAndNucnfcltAndCdestadoasigAndCdestadocont(
      @Param("nbooking") String nbooking, @Param("nucnfliq") Short nucnfliq, @Param("nucnfclt") String nucnfclt,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig, @Param("estadosCont") Tmct0estado estadosCont);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nbooking = :nbooking and alc.id.nucnfliq = :nucnfliq and alc.id.nucnfclt = :nucnfclt AND alc.estadoentrec IN (:estadoEntrec)")
  public Tmct0alc findByNbookingAndNucnfliqAndNucnfcltAndCdestadoentrec(@Param("nbooking") String nbooking,
      @Param("nucnfliq") Short nucnfliq, @Param("nucnfclt") String nucnfclt,
      @Param("estadoEntrec") List<Tmct0estado> estadoEntrec);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nbooking = :nbooking and alc.id.nucnfliq = :nucnfliq and alc.id.nucnfclt = :nucnfclt AND alc.estadoentrec IN (:estadoEntrec) AND alc.cdestadoasig IN (:lEstadosAsig)")
  public Tmct0alc findByNbookingAndNucnfliqAndNucnfcltAndCdestadoasigAndCdestadoentrec(
      @Param("nbooking") String nbooking, @Param("nucnfliq") Short nucnfliq, @Param("nucnfclt") String nucnfclt,
      @Param("lEstadosAsig") List<Integer> lEstadosAsig, @Param("estadoEntrec") List<Tmct0estado> estadoEntrec);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.tmct0alo.cdalias = :alo_alias AND alc.tmct0alo.cdisin = :isin AND alc.estadoentrec IN (:estadoEntrec)")
  public List<Tmct0alc> findByAloaliasAandIsinAndCdestadoentrec(@Param("alo_alias") String alo_alias,
      @Param("isin") String isin, @Param("estadoEntrec") List<Integer> estadoEntrec);

  @Query(value = "SELECT tmct0alc.nuorden, tmct0alc.nbooking, tmct0alc.nucnfclt, tmct0alc.nucnfliq, tmct0alc.feejeliq, tmct0alc.nutitliq,"
      + "     tmct0alc.imnfiliq, tmct0alc.imcomdvo, tmct0alc.pccomdev, tmct0alc.nbtitliq, tmct0bok.cdtpoper, tmct0bok.nbvalors, tmct0bok.cdisin"
      + " FROM tmct0alc tmct0alc, tmct0bok tmct0bok"
      + " WHERE tmct0bok.cdalias = :alias AND tmct0alc.feejeliq >= :fechaDesde AND tmct0alc.feejeliq <= :fechaHasta"
      + "       AND tmct0bok.nuorden=tmct0alc.nuorden AND tmct0bok.nbooking=tmct0alc.nbooking"
      + " AND EXISTS (SELECT 1 FROM tmct0_alc_estadocont alcEstadoCont"
      + "             WHERE tmct0alc.nuorden=alcEstadoCont.nuorden AND tmct0alc.nbooking=alcEstadoCont.nbooking"
      + "                   AND tmct0alc.nucnfclt=alcEstadoCont.nucnfclt AND tmct0alc.nucnfliq=alcEstadoCont.nucnfliq"
      + "                   AND alcEstadoCont.tipoestado = :tipoEstado AND alcEstadoCont.estadocont = :estadoCont)", nativeQuery = true)
  public List<Object[]> findAllDataAndDvoByFechaAndAlias(@Param("alias") String alias,
      @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta,
      @Param("tipoEstado") Integer tipoEstado, @Param("estadoCont") Integer estadoCont);

  @Query(value = "SELECT tmct0alc.nuorden, tmct0alc.nbooking, tmct0alc.nucnfclt, tmct0alc.nucnfliq, tmct0alc.feejeliq, tmct0alc.nutitliq,"
      + "     tmct0alc.imnfiliq, tmct0alc.imcombrk, tmct0alc.pccombrk, tmct0alc.nbtitliq, tmct0bok.cdtpoper, tmct0bok.nbvalors, tmct0bok.cdisin"
      + " FROM tmct0alc tmct0alc, tmct0bok tmct0bok"
      + " WHERE tmct0bok.cdalias = :alias AND tmct0alc.feejeliq >= :fechaDesde AND tmct0alc.feejeliq <= :fechaHasta"
      + "       AND tmct0bok.nuorden=tmct0alc.nuorden AND tmct0bok.nbooking=tmct0alc.nbooking"
      + " AND EXISTS (SELECT 1 FROM tmct0_alc_estadocont alcEstadoCont"
      + "             WHERE tmct0alc.nuorden=alcEstadoCont.nuorden AND tmct0alc.nbooking=alcEstadoCont.nbooking"
      + "                   AND tmct0alc.nucnfclt=alcEstadoCont.nucnfclt AND tmct0alc.nucnfliq=alcEstadoCont.nucnfliq"
      + "                   AND alcEstadoCont.tipoestado = :tipoEstado AND alcEstadoCont.estadocont = :estadoCont)", nativeQuery = true)
  public List<Object[]> findAllDataAndBrkByFechaAndAlias(@Param("alias") String alias,
      @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta,
      @Param("tipoEstado") Integer tipoEstado, @Param("estadoCont") Integer estadoCont);


  @Query("SELECT alc FROM Tmct0alc alc, Tmct0bok bok"
      + " WHERE bok.cdalias = :alias AND alc.feejeliq >= :fechaDesde AND alc.feejeliq <= :fechaHasta AND bok.id.nuorden=alc.id.nuorden AND bok.id.nbooking=alc.id.nbooking")
  public List<Tmct0alc> findAlc(@Param("alias") String alias, @Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta);

  /**
   * Busca los ALCs de con fecha de ejecución especificada y con estado de
   * asignación entre los especificados.
   * 
   * @param estadosAsigUno
   * @param estadosAsigDos
   * @param feejeliq
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc WHERE feejeliq = :feejeliq AND alc.cdestadoasig >= :estadosAsigUno AND alc.cdestadoasig < :estadosAsigDos")
  public List<Tmct0alc> findByEstadosAsigAndFeejeliq(@Param("estadosAsigUno") Integer estadosAsigUno,
      @Param("estadosAsigDos") Integer estadosAsigDos, @Param("feejeliq") Date feejeliq);

  /**
   * 
   * @param feejeliq
   * @return
   */
  @Query("SELECT alc FROM Tmct0alc alc"
      + " WHERE feejeliq = :feejeliq AND alc.cdestadoasig=:estadosAsigPdteLiquidar"
      + "       AND EXISTS (SELECT 1 FROM Tmct0desglosecamara dc WHERE dc.tmct0alc=alc"
      + " AND ( dc.tmct0infocompensacion.cdctacompensacion IS NULL OR trim(dc.tmct0infocompensacion.cdctacompensacion)=''))")
  public List<Tmct0alc> findByNullCompCtaAndFeejeliq(@Param("estadosAsigPdteLiquidar") Integer estadosAsigPdteLiquidar,
      @Param("feejeliq") Date feejeliq);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nuorden=:nuorden and alc.id.nbooking = :nbooking and alc.cdestadoasig > 0")
  List<Tmct0alc> findAllByNuordenAndNbookingAndActivos(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking);

  @Query("SELECT alc.id FROM Tmct0alc alc WHERE alc.id.nuorden=:nuorden and alc.id.nbooking = :nbooking and alc.cdestadoasig > 0 "
      + " and alc.estadotit.idestado != :notcdestadotit")
  List<Tmct0alcId> findAllTmct0alcIdByNuordenAndNbookingAndActivosAndNotCdestadotit(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("notcdestadotit") int notCdestadotit);

  @Query("SELECT alc.id FROM Tmct0alc alc WHERE alc.id.nuorden=:nuorden and alc.id.nbooking = :nbooking and alc.cdestadoasig > 0 "
      + " and alc.referenciaTitular is null ")
  List<Tmct0alcId> findAllTmct0alcIdByNuordenAndNbookingAndActivosAndSinReferenciaTitular(
      @Param("nuorden") String nuorden, @Param("nbooking") String nbooking);

  @Query("SELECT distinct alc.feejeliq FROM Tmct0alc alc WHERE "
      + " alc.estadotit.idestado != :notcdestadotit and alc.estadotit.idestado < :cdestadotitLt and alc.cdestadoasig > 0 and alc.referenciaTitular is null  ")
  List<Date> findAllFeejeliqBySinReferenciaAndNotCdestadotitAndCdestadotitLt(
      @Param("notcdestadotit") int notcdestadotit, @Param("cdestadotitLt") int cdestadotitLt);

  @Query("SELECT distinct alc.feejeliq FROM Tmct0alc alc WHERE "
      + " alc.estadotit.idestado in :listcdestadotit and alc.cdestadoasig >= :cdestadoasigGte and alc.referenciaTitular is not null ")
  List<Date> findAllFeejeliqByConReferenciaAndInCdestadotitAndCdestadoasigGte(
      @Param("listcdestadotit") List<Integer> listCdestadotit, @Param("cdestadoasigGte") int cdestadoasigGte);

  @Query("SELECT distinct alc.tmct0alo.tmct0bok.id FROM Tmct0alc alc WHERE "
      + " alc.feejeliq = :feejeliq and alc.cdestadoasig > 0 and alc.estadotit.idestado != :notcdestadotit "
      + " and alc.referenciaTitular is null  "
      + " and (alc.tmct0alo.tmct0bok.tmct0ord.cdorigen = :cdorigen or alc.tmct0alo.tmct0bok.tmct0ord.cdclente = :cdclente ) "
      + " and alc.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv ")
  List<Tmct0bokId> findAllTmct0bokIdByFeejeliqAndSinReferenciaTitularRouting(@Param("feejeliq") Date feejeliq,
      @Param("isscrdiv") char isscrdiv, @Param("notcdestadotit") int notcdestadotit,
      @Param("cdorigen") String cdorigen, @Param("cdclente") String cdclente, Pageable page);

  @Query("SELECT distinct alc.tmct0alo.tmct0bok.id FROM Tmct0alc alc, Tmct0afi afi WHERE "
      + " alc.tmct0alo.id = afi.tmct0alo.id and alc.id.nucnfliq = afi.id.nucnfliq "
      + " and alc.feejeliq = :feejeliq and alc.cdestadoasig > 0 and alc.estadotit.idestado != :notcdestadotit "
      + " and alc.referenciaTitular is null "
      + " and alc.tmct0alo.tmct0bok.tmct0ord.cdorigen != :cdorigen and alc.tmct0alo.tmct0bok.tmct0ord.cdclente != :cdclente  "
      + " and alc.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv and afi.cdinacti = 'A' ")
  List<Tmct0bokId> findAllTmct0bokIdByFeejeliqAndSinReferenciaTitularNoRouting(@Param("feejeliq") Date feejeliq,
      @Param("isscrdiv") char isscrdiv, @Param("notcdestadotit") int notcdestadotit,
      @Param("cdorigen") String cdorigen, @Param("cdclente") String cdclente, Pageable page);

  @Query("SELECT distinct alc.tmct0alo.tmct0bok.id FROM Tmct0alc alc, Tmct0afi afi WHERE "
      + " alc.tmct0alo.id = afi.tmct0alo.id and alc.id.nucnfliq = afi.id.nucnfliq "
      + " and alc.feejeliq = :feejeliq and alc.cdestadoasig >= :cdestadoasigGte and alc.estadotit.idestado in :listCdestadotit "
      + " and alc.referenciaTitular is not null and alc.tmct0alo.tmct0bok.casepti = 'S' "
      + " and alc.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv and afi.cdinacti = 'A' ")
  List<Tmct0bokId> findAllTmct0bokIdByFeejeliqInCdestadotitAndCdestadoasigGteConReferenciaTitular(
      @Param("feejeliq") Date feejeliq, @Param("isscrdiv") char isscrdiv,
      @Param("listCdestadotit") List<Integer> inCdestadotit, @Param("cdestadoasigGte") int cdestadoasigGte,
      Pageable page);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nbooking = :nbooking")
  List<Tmct0alc> findByNbooking(@Param("nbooking") String nbooking);

  @Query("SELECT alc FROM Tmct0alc alc, AlcOrdenes ord where ord.nbooking = alc.id.nbooking "
      + "and ord.nuorden = alc.id.nuorden " + "and ord.nucnfliq = alc.id.nucnfliq "
      + "and ord.nucnfclt = alc.id.nucnfclt " + "and ord.referenciaS3 = :referencias3")
  public List<Tmct0alc> findByReferencias3(@Param("referencias3") String referencias3);

  @Query(value = "SELECT ALC FROM Tmct0alc ALC WHERE ALC.id.nuorden=:nuorden")
  List<Tmct0alc> findByNuorden(@Param("nuorden") String nuorden);

  @Query(value = "SELECT new sibbac.business.wrappers.database.data.Tmct0AlcData(ALC) FROM Tmct0alc ALC WHERE ALC.id.nuorden=:nuorden")
  List<Tmct0AlcData> findDataByNuorden(@Param("nuorden") String nuorden);

  @Transactional
  @Modifying
  @Query(value = "update bsnbpsql.TMCT0_DESGLOSECAMARA_ENVIO_RT set ESTADO='C' where iddesglosecamara in "
      + "(select iddesglosecamara from bsnbpsql.TMCT0desglosecamara "
      + "where nuorden =:nuorden AND nbooking = :nbooking and nucnfclt = :nucnfclt and nucnfliq = :nucnfliq)", nativeQuery = true)
  void updateDesgloseEnvioRtDespuesAltaAfi(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  @Transactional
  @Modifying
  @Query(value = "update bsnbpsql.TMCT0ENVIOTITULARIDAD set fenvio=null,frecepcion=null,ESTADO='P' where IDENVIOTITULARIDAD in ( select IDENVIOTITULARIDAD from TMCT0_DESGLOSECAMARA_ENVIO_RT where IDDESGLOSECAMARA in ( select iddesglosecamara from tmct0desglosecamara where nuorden =:nuorden AND "
      + "nbooking = :nbooking AND nucnfclt =:nucnfclt AND nucnfliq =:nucnfliq))", nativeQuery = true)
  void updateEnvioTitularidadDespuesAltaAfi(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  @Transactional
  @Modifying
  @Query(value = " update bsnbpsql.TMCT0desglosecamara set cdestadotit=:cdestadotit, AUDIT_FECHA_CAMBIO=CURRENT_DATE where nuorden =:nuorden AND nbooking = :nbooking and nucnfclt = :nucnfclt and nucnfliq = :nucnfliq", nativeQuery = true)
  void updateDesgloseCamaraDespuesAltaAfi(@Param("cdestadotit") int idEstado, @Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  @Transactional
  @Modifying
  @Query(value = "update bsnbpsql.TMCT0alc set cdestadotit=205, ID_REFERENCIA_TITULAR=NULL, RFTITAUD='', CDREFTIT='', FHAUDIT=CURRENT_DATE where nbooking = :nbooking and nucnfclt = :nucnfclt and nucnfliq = :nucnfliq", nativeQuery = true)
  void updateAlcDespuesAltaAfi(@Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt,
      @Param("nucnfliq") Short nucnfliq);

  @Transactional
  @Modifying
  @Query(value = "update bsnbpsql.TMCT0alo set cdestadotit=205, FHAUDIT=CURRENT_DATE where nbooking = :nbooking and nucnfclt = :nucnfclt", nativeQuery = true)
  void updateAloDespuesAltaAfi(@Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt);

  @Transactional
  @Modifying
  @Query(value = "update bsnbpsql.TMCT0bok set cdestadotit=205, FHAUDIT=CURRENT_DATE where nbooking = :nbooking", nativeQuery = true)
  void updateBokDespuesAltaAfi(@Param("nbooking") String nbooking);

  @Transactional
  @Modifying
  @Query(value = "delete from bsnbpsql.TMCT0_DESGLOSECAMARA_ENVIO_RT where iddesglosecamara in (select iddesglosecamara from bsnbpsql.TMCT0desglosecamara where nuorden=:nuorden AND nbooking = :nbooking and nucnfclt = :nucnfclt and nucnfliq = :nucnfliq)", nativeQuery = true)
  void deleteDesgloseCamaraDespuesAltaAfi(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") Short nucnfliq);

  @Query(value = "select IDINFOCOMP from tmct0infocompensacion where CDMIEMBROCOMPENSADOR='V012' and (CDREFASIGNACION='2898' or CDREFASIGNACION='2899' or CDREFASIGNACION='0049P' or CDREFASIGNACION='0049') and IDINFOCOMP=:idInfoComp", nativeQuery = true)
  Long getInfoCompFromAlc(@Param("idInfoComp") Long idInfoComp);

  @Query("SELECT DISTINCT alc"
      + " FROM Tmct0alc alc, Tmct0movimientoecc mecc join mecc.tmct0desglosecamara dc"
      + " WHERE alc.id.nbooking=:nbooking"
      + "       AND dc.tmct0alc.id.nuorden=alc.id.nuorden AND dc.tmct0alc.id.nbooking=alc.id.nbooking AND dc.tmct0alc.id.nucnfclt=alc.id.nucnfclt AND dc.tmct0alc.id.nucnfliq=alc.id.nucnfliq"
      + "       AND mecc.cdestadomov=9" + "       AND mecc.cuentaCompensacionDestino IN :cdCodigosValidos")
  List<Tmct0alc> findByNbookingAndCuentaCompensacionDestino(@Param("nbooking") String nbooking,
      @Param("cdCodigosValidos") List<String> cdCodigosValidos);

  @Query("SELECT o FROM Tmct0alc o WHERE o.id.nuorden =:nuorden AND o.id.nbooking=:nbooking AND o.id.nucnfclt=:nucnfclt AND o.cdestadoasig!=:estado")
  List<Tmct0alc> getAlcs(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("estado") int estado);

  @Query("SELECT o FROM Tmct0alc o WHERE o.id.nuorden =:nuorden AND o.id.nbooking=:nbooking AND o.id.nucnfclt=:nucnfclt")
  List<Tmct0alc> getAlcs(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt);

  @Query("SELECT o FROM Tmct0alc o WHERE o.id.nuorden =:nuorden AND o.id.nbooking=:nbooking AND o.id.nucnfclt=:nucnfclt AND o.id.nucnfliq=:nucnfliq ")
  Tmct0alc getAlc(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") short nucnfliq);

  @Query("select distinct new sibbac.business.wrappers.database.dto.EnvioCorretajeTmct0alcDataDTO(alc.id.nuorden, alc.id.nbooking, alc.id.nucnfclt, alc.id.nucnfliq, "
      + " alc.nutitliq, alc.imefeagr, alc.imfinsvb, cli.cif, cli.descli, ord.cdisin, alc.cdentdep, ord.cdtpoper) "
      + " from Tmct0alc alc, Tmct0ord ord, Tmct0ali ali, Tmct0cli cli where alc.id.nuorden = ord.id.nuorden and ord.cdcuenta = ali.id.cdaliass and ali.fhfinal = 99991231 "
      + " and ali.id.cdbrocli = cli.id.cdbrocli and cli.fhfinal = 99991231 "
      + " and alc.feejeliq <= :toFechaContratacion and alc.feopeliq >= :toFechaLiquidacion and alc.cdenvliq ='O' and alc.corretajePti = 'F' and alc.estadoentrec.idestado = :cdestadoentrecEq "
      + " and alc.cdestadoasig >= :cdestadoasigGte and alc.imfinsvb <> 0")
  List<EnvioCorretajeTmct0alcDataDTO> getEnvioCorretajeTmct0alcSinEnviar(
      @Param("toFechaContratacion") Date toFechaContratacionLte,
      @Param("toFechaLiquidacion") Date toFechaLiquidacionGte, @Param("cdestadoasigGte") Integer cdestadoasigGte,
      @Param("cdestadoentrecEq") Integer cdestadoentrecEq);

  @Modifying
  @Query(value = "UPDATE Tmct0alc o SET o.CDESTADOENTREC = :cdestadoentrec, o.fhaudit=:fhaudit WHERE "
      + " o.nuorden =:nuorden AND o.nbooking=:nbooking and o.CDESTADOENTREC <> :cdestadoentrec "
      + " AND o.nucnfclt=:nucnfclt AND o.nucnfliq=:nucnfliq ", nativeQuery = true)
  Integer updateEstadoEntregaRecepcion(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") short nucnfliq,
      @Param("cdestadoentrec") Integer cdestadoentrec, @Param("fhaudit") Date fhaudit);

  @Modifying
  @Query(value = "UPDATE Tmct0alc o SET o.CDESTADOENTREC = :cdestadoentrec, o.fhaudit=:fhaudit WHERE "
      + " o.nuorden =:nuorden AND o.nbooking=:nbooking "
      + " AND o.nucnfclt=:nucnfclt AND o.nucnfliq=:nucnfliq and o.CDESTADOENTREC = :whereCdestadoEntRec ", nativeQuery = true)
  Integer updateEstadoEntregaRecepcionByCdestadoentrec(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") short nucnfliq,
      @Param("cdestadoentrec") Integer cdestadoentrec, @Param("whereCdestadoEntRec") Integer whereCdestadoEntRec,
      @Param("fhaudit") Date fhaudit);

  @Modifying
  @Query(value = "UPDATE Tmct0desglosecamara o SET o.CDESTADOENTREC = :cdestadoentrec, o.AUDIT_FECHA_CAMBIO=:fhaudit WHERE "
      + " o.nuorden =:nuorden AND o.nbooking=:nbooking  "
      + " AND o.nucnfclt=:nucnfclt AND o.nucnfliq=:nucnfliq and o.CDESTADOENTREC = :whereCdestadoEntRec ", nativeQuery = true)
  Integer updateEstadoEntregaRecepcionDesgloseCamaraByCdestadoentrec(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") short nucnfliq,
      @Param("cdestadoentrec") Integer cdestadoentrec, @Param("whereCdestadoEntRec") Integer whereCdestadoEntRec,
      @Param("fhaudit") Date fhaudit);

  @Query("SELECT new sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista17DTO( b.id, b.fealta, b.hoalta, b.fecontra,"
      + " b.fevalor, sum(dt.imefectivo), sum(dt.imtitulos), b.imcbmerc, b.cdmoniso, dt.cdPlataformaNegociacion, '', sum(dt.imcomisn), sum(dt.imcanoncontreu), "
      + " a.canoncontrpagoclte) "
      + " FROM Tmct0ord o, Tmct0bok b, Tmct0alo a, Tmct0alc c, Tmct0desglosecamara d, Tmct0desgloseclitit dt, Tmct0movimientoecc m, "
      + " Tmct0movimientooperacionnuevaecc mn  "
      + " WHERE o.nuorden = b.id.nuorden AND b.id.nuorden=a.id.nuorden and b.id.nbooking=a.id.nbooking "
      + " and a.id.nuorden=c.id.nuorden and a.id.nbooking=c.id.nbooking and a.id.nucnfclt=c.id.nucnfclt"
      + " and c.id.nuorden=d.tmct0alc.id.nuorden and c.id.nbooking=d.tmct0alc.id.nbooking and c.id.nucnfclt=d.tmct0alc.id.nucnfclt "
      + " and c.id.nucnfliq=d.tmct0alc.id.nucnfliq and c.nutitliq = b.nutiteje "
      + " and d.iddesglosecamara=dt.tmct0desglosecamara.iddesglosecamara "
      + " and d.iddesglosecamara= m.tmct0desglosecamara.iddesglosecamara "
      + " and m.idmovimiento = mn.tmct0movimientoecc.idmovimiento and mn.nudesglose=dt.nudesglose "
      + " and m.miembroCompensadorDestino in :compensadores and m.codigoReferenciaAsignacion in :referencias "
      + " and b.cdalias not in :aliass and d.tmct0estadoByCdestadoasig.idestado >= :estado "
      + " and length(trim(c.cdrefban)) = 20 and c.cdrefban like :refban "
      + " and (b.cdindimp <> 'S' or b.cdindimp is null) "
      + " and c.feejeliq >= :sincedate "
      + " and m.cdestadomov = :estadomov "
      + " and b.casepti = 'S' AND (o.nureford = '' OR o.nureford IS NULL )"
      + " GROUP BY b.id.nuorden, b.id.nbooking, b.fealta, b.hoalta, b.fecontra,"
      + " b.fevalor,  b.imcbmerc, b.cdmoniso, dt.cdPlataformaNegociacion, a.canoncontrpagoclte"
      + " ORDER BY b.id.nbooking ASC")
  List<ConfirmacionMinorista17DTO> findAllConfirmacionMinorista17(@Param("compensadores") List<String> compensadores,
      @Param("referencias") List<String> referencias, @Param("aliass") List<String> notInAliass,
      @Param("refban") String cdrefbanStart, @Param("estado") Integer estado, @Param("estadomov") String estadomov,
      @Param("sincedate") Date sincedate);

  @Query("SELECT new sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista17DTO( b.id, b.fealta, b.hoalta, b.fecontra,"
      + " b.fevalor, sum(dt.imefectivo), sum(dt.imtitulos), b.imcbmerc, b.cdmoniso, dt.cdPlataformaNegociacion, '', sum(dt.imcomisn), sum(dt.imcanoncontreu), "
      + " a.canoncontrpagoclte) "
      + " FROM Tmct0ord o, Tmct0bok b, Tmct0alc c, Tmct0alo a, Tmct0desglosecamara d, Tmct0desgloseclitit dt, Tmct0movimientoecc m, "
      + " Tmct0movimientooperacionnuevaecc mn  "
      + " WHERE o.nuorden = b.id.nuorden "
      + " and b.id.nuorden=a.id.nuorden and b.id.nbooking=a.id.nbooking "
      + " and a.id.nuorden=c.id.nuorden and a.id.nbooking=c.id.nbooking and a.id.nucnfclt=c.id.nucnfclt "
      + " and c.id.nuorden=d.tmct0alc.id.nuorden and c.id.nbooking=d.tmct0alc.id.nbooking and c.id.nucnfclt=d.tmct0alc.id.nucnfclt "
      + " and c.id.nucnfliq=d.tmct0alc.id.nucnfliq "
      + " and d.iddesglosecamara=dt.tmct0desglosecamara.iddesglosecamara "
      + " and d.iddesglosecamara= m.tmct0desglosecamara.iddesglosecamara "
      + " and m.idmovimiento = mn.tmct0movimientoecc.idmovimiento and mn.nudesglose=dt.nudesglose "
      + " and m.miembroCompensadorDestino in :compensadores and m.codigoReferenciaAsignacion in :referencias "
      + " and d.tmct0estadoByCdestadoasig.idestado >= :estado "
      + " and m.cdestadomov = :estadomov "
      + " and b.id.nuorden = :nuorden and b.id.nbooking = :nbooking "
      + " GROUP BY b.id.nuorden, b.id.nbooking, b.fealta, b.hoalta, b.fecontra,"
      + " b.fevalor,  b.imcbmerc, b.cdmoniso, dt.cdPlataformaNegociacion, a.canoncontrpagoclte"
      + " ORDER BY b.id.nbooking ASC")
  List<ConfirmacionMinorista17DTO> findAllConfirmacionMinorista17(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("compensadores") List<String> compensadores,
      @Param("referencias") List<String> referencias, @Param("estado") Integer estado,
      @Param("estadomov") String estadomov);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nuorden = :nuorden and alc.id.nbooking = :nbooking and alc.id.nucnfclt=:nucnfclt and alc.cdestadoasig = :cdestadoasig ")
  public List<Tmct0alc> findAllByNuordenAndNbookingAndNucnfcltAndCdestadoasig(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("cdestadoasig") int cdestadoasig);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id.nuorden, a.id.nbooking, a.id.nucnfclt, "
      + " a.id.nucnfliq, a.feopeliq, a.cdrefban, a.estadotit.idestado, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alc a, Tmct0ord o  "
      + " WHERE a.id.nuorden = o.nuorden AND a.id.nuorden = :nuorden AND a.estadotit.idestado < :estadotit "
      + " AND a.cdestadoasig > 0 ")
  List<ControlTitularesDTO> findControlTitularesByNuordenAndCdestadotitLt(@Param("nuorden") String nuorden,
      @Param("estadotit") Integer cdestadotitLt);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id.nuorden, a.id.nbooking, a.id.nucnfclt, "
      + " a.id.nucnfliq, a.feopeliq, a.cdrefban, a.estadotit.idestado, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alc a, Tmct0ord o  "
      + " WHERE a.id.nuorden = o.nuorden AND a.id.nuorden = :pnuorden "
      + " AND a.id.nbooking = :pnbooking AND a.id.nucnfclt = :pnucnfclt " + " AND a.cdestadoasig > 0 ")
  List<ControlTitularesDTO> findControlTitularesByAlloId(@Param("pnuorden") String nuorden,
      @Param("pnbooking") String nbooking, @Param("pnucnfclt") String nucnfclt);

  @Query(value = "SELECT new sibbac.business.wrappers.database.dto.ControlTitularesDTO(o.cdclsmdo, a.id.nuorden, a.id.nbooking, a.id.nucnfclt, "
      + " a.id.nucnfliq, a.feopeliq, a.cdrefban, a.estadotit.idestado, o.cdclente, o.isscrdiv ) "
      + " FROM Tmct0alc a, Tmct0ord o  "
      + " WHERE a.id.nuorden = o.nuorden AND a.id = :alcId "
      + " AND a.cdestadoasig > 0 ")
  ControlTitularesDTO findControlTitularesByAlcId(@Param("alcId") Tmct0alcId id);

  @Modifying
  @Query(value = "UPDATE Tmct0alc c SET c.cdrefban = :pcdrefban, c.fhaudit = :audit WHERE c.id = :alcid ")
  Integer updateCdrefbanByAlcId(@Param("pcdrefban") String cdrefban, @Param("alcid") Tmct0alcId alcId,
      @Param("audit") Date audit);

  @Query("SELECT alc FROM Tmct0alc alc, Factura f, FacturaSugerida fs, FacturaDetalle fd, AlcOrdenDetalleFactura aodf, AlcOrdenes ao"
      + " WHERE alc.id.nuorden = ao.nuorden AND alc.id.nbooking = ao.nbooking"
      + "       AND alc.id.nucnfclt = ao.nucnfclt AND alc.id.nucnfliq = ao.nucnfliq"
      + "       AND ao.id = aodf.alcOrden.id AND aodf.facturaDetalle.id = fd.id AND fd.facturaSugerida.id = fs.id AND fs.id = f.facturaSugerida.id"
      + "       AND f.id=:facturaId AND alc.cdestadoasig >= :estadoAsig AND alc.estadocont.id != :estadoCont")
  List<Tmct0alc> findAlcsByFactura(@Param("facturaId") Long facturaId, @Param("estadoAsig") Integer estadoAsig,
      @Param("estadoCont") Integer estadoCont, Pageable pageable);

  @Modifying
  @Query("UPDATE Tmct0alc dc SET dc.estadotit.idestado = :pcdestadotit, dc.fhaudit = :fhaudit "
      + " WHERE dc.tmct0alo.id = :aloId AND dc.cdestadoasig > 0")
  Integer updateCdestadotitByTmct0aloId(@Param("aloId") Tmct0aloId aloId, @Param("pcdestadotit") int cdestadoatit,
      @Param("fhaudit") Date fhaudit);

  @Modifying
  @Query("UPDATE Tmct0alc dc SET dc.estadotit.idestado = :pcdestadotit, dc.fhaudit = :fhaudit "
      + " WHERE dc.id = :alcId AND dc.cdestadoasig > 0")
  Integer updateCdestadotitByTmct0alcId(@Param("alcId") Tmct0alcId alcId, @Param("pcdestadotit") int cdestadoatit,
      @Param("fhaudit") Date fhaudit);

  @Query(value = "SELECT r FROM Tmct0alc c, Tmct0referenciatitular r "
      + " WHERE r.idreferencia = c.referenciaTitular.idreferencia AND c.id = :alcId ")
  Tmct0referenciatitular findTmct0referenciatitular(@Param("alcId") Tmct0alcId alcId);

  /**
   * Localiza solo los Alcs de una orden que esta listos para enviar y tienen
   * movimientos ecc dados de alta
   * @param nuorden Numero de la orde
   * @return lista de Alcs que cumplen con las condiciones, puede ser vacío.
   */
  @Query("SELECT DISTINCT alc FROM Tmct0alc alc " + "JOIN FETCH alc.tmct0desglosecamaras des "
      + "JOIN des.tmct0movimientos mov " + "WHERE alc.id.nuorden = :nuorden AND " + "alc.cdestadoasig > 0 AND "
      + "des.tmct0estadoByCdestadoasig.idestado > 0 AND " + "mov.cdestadomov = '9' AND "
      + "mov.cuentaCompensacionDestino is not null AND " + "mov.cuentaCompensacionDestino != '' ")
  List<Tmct0alc> findConDesglosesPorEnviar(@Param("nuorden") String nuorden);

  @Query(value = "SELECT sum(a.nutitliq) FROM Tmct0alc a WHERE a.tmct0alo.id = :aloId AND a.cdestadoasig > 0 ")
  BigDecimal getSumNutitliqByTmct0aloId(@Param("aloId") Tmct0aloId aloId);

  @Query("SELECT alc FROM Tmct0alc alc WHERE alc.id.nuorden = :nuorden AND alc.id.nbooking = :nbooking AND alc.id.nucnfclt = :nucnfclt AND alc.id.nucnfliq = :nucnfliq")
  public List<Tmct0alc> findAlcByNuordenNbookingNuncfcltNucnfliq(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nucnfliq") short nucnfliq);

  @Query(value = "SELECT C.NUORDEN, C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ,"
      + "  C.IMCANONCONTREU = SUM(DT.IMCANONCONTREU), C.IMCANONCONTRDV = SUM(DT.IMCANONCONTRDV)"
      + " FROM TMCT0BOK B "
      + "INNER JOIN TMCT0ALC C ON B.NUORDEN = C.NUORDEN AND B.NBOOKING = C.NBOOKING  "
      + "INNER JOIN TMCT0DESGLOSECAMARA DC ON C.NUORDEN = DC.NUORDEN AND C.NBOOKING = DC.NBOOKING AND C.NUCNFCLT = DC.NUCNFCLT AND C.NUCNFLIQ = DC.NUCNFLIQ "
      + "INNER JOIN TMCT0DESGLOSECLITIT DT ON DC.IDDESGLOSECAMARA = DT.IDDESGLOSECAMARA "
      + "WHERE B.FETRADE= :fetrade AND B.CDESTADOASIG >= :cdestadoasig AND C.CDESTADOASIG >= :cdestadoasig AND DC.CDESTADOASIG >= :cdestadoasig "
      + "GROUP BY C.NUORDEN, C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ, C.NUTITLIQ, C.IMEFEAGR, C.IMTOTBRU, C.IMCANONCONTREU, C.IMCANONCONTRDV, C.IMAJUSVB, C.IMCOMBCO, "
      + " C.IMCOMISN, C.IMCOMBRK, C.IMFINSVB, C.IMCOMDVO, C.IMCOMSVB, C.IMNFILIQ, C.IMNTBROK, C.IMCOBRADO, C.IMCONCILIADO, C.IMEFE_MERCADO_COBRADO, C.IMEFE_CLIENTE_COBRADO, C.IMCANON_COBRADO, "
      + " C.IMCOM_COBRADO, C.IMBRK_PAGADO, C.IMDVO_PAGADO "
      + "HAVING  C.NUTITLIQ = SUM(DT.IMTITULOS) AND ( C.IMEFEAGR <> SUM(DT.IMEFECTIVO) OR C.IMTOTBRU <> SUM(DT.IMTOTBRU) OR C.IMCANONCONTREU <> SUM(DT.IMCANONCONTREU) "
      + "OR C.IMCANONCONTRDV <> SUM(DT.IMCANONCONTRDV) OR  C.IMAJUSVB <> SUM(DT.IMAJUSVB) OR C.IMCOMBCO <> SUM(DT.IMCOMBCO) OR C.IMCOMISN <> SUM(DT.IMCOMISN) OR C.IMCOMBRK <> SUM(DT.IMCOMBRK) "
      + "OR C.IMFINSVB <> SUM(DT.IMFINSVB) OR C.IMCOMDVO <> SUM(DT.IMCOMDVO) OR C.IMCOMSVB <> SUM(DT.IMCOMSVB) OR C.IMNFILIQ <> SUM(DT.IMNFILIQ) OR C.IMNTBROK <> SUM(DT.IMNTBROK) "
      + "OR C.IMCOBRADO <> SUM(DT.IMCOBRADO) OR C.IMEFE_MERCADO_COBRADO <> SUM(DT.IMEFE_MERCADO_COBRADO) OR C.IMEFE_CLIENTE_COBRADO <> SUM(DT.IMEFE_CLIENTE_COBRADO) "
      + " OR C.IMCANON_COBRADO <> SUM(DT.IMCANON_COBRADO) OR C.IMCOM_COBRADO <> SUM(DT.IMCOM_COBRADO) OR C.IMBRK_PAGADO <> SUM(DT.IMBRK_PAGADO) "
      + " OR C.IMDVO_PAGADO <> SUM(DT.IMDVO_PAGADO) "
      + " OR SUM(DT.IMCOBRADO) IS NULL OR SUM(DT.IMEFE_MERCADO_COBRADO) IS NULL OR SUM(DT.IMEFE_CLIENTE_COBRADO) IS NULL "
      + " OR SUM(DT.IMCANON_COBRADO) IS NULL OR SUM(DT.IMCOM_COBRADO) IS NULL OR SUM(DT.IMBRK_PAGADO) IS NULL OR SUM(DT.IMDVO_PAGADO) IS NULL"
      + " ) " + " ORDER BY C.NUORDEN, C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ ", nativeQuery = true)
  public List<Object[]> findAllAlcIdWithCalculosMalRepartidosByFetradeAndCdestadoasigGte(
      @Param("fetrade") Date fetrade, @Param("cdestadoasig") int cdestadoasigGte);

  @Query(value = "select distinct c.FEEJELIQ "
      + " from Tmct0alc c where c.CANON_CALCULADO = :canonCalculado and c.CDESTADOASIG > 0 "
      + " and c.ID_REFERENCIA_TITULAR > 0 ", nativeQuery = true)
  List<Date> findAllDistinctFetradeByCanonCalculadoAndConReferenciaTitular(@Param("canonCalculado") char canonCalculado);

  @Query(value = "SELECT B.FETRADE, B.CDISIN, B.CDTPOPER FROM ( SELECT B.NUORDEN, B.NBOOKING, B.FETRADE, B.CDISIN, B.CDTPOPER "
      + " FROM TMCT0BOK B WHERE B.FETRADE = :feejeliq AND B.CDESTADOASIG > 0 AND B.CASEPTI = 'S' ) B "
      + " INNER JOIN ( SELECT C.NUORDEN, C.NBOOKING FROM TMCT0ALC C WHERE C.FEEJELIQ = :feejeliq AND C.CDESTADOASIG > 0 AND C.CANON_CALCULADO = :canonCalculado "
      + " AND C.ID_REFERENCIA_TITULAR > 0 ) C ON B.NUORDEN = C.NUORDEN AND B.NBOOKING = C.NBOOKING "
      + " INNER JOIN TMCT0ORD O ON O.NUORDEN = B.NUORDEN AND O.ISSCRDIV = :isscrdiv "
      + " GROUP BY B.FETRADE, B.CDISIN, B.CDTPOPER ", nativeQuery = true)
  List<Object[]> findAllDistinctFetradeCdisinCdtpoperByFeejeliqAndCanonCalculadoAndConReferenciaTitularAndCasado(
      @Param("feejeliq") Date feejeliq, @Param("canonCalculado") char canonCalculado, @Param("isscrdiv") char isscrdiv);

  @Query("select distinct c.tmct0alo.tmct0bok.id from Tmct0alc c "
      + " where c.canonCalculado = :canonCalculado and c.cdestadoasig = 0 "
      + " and c.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv")
  List<Tmct0bokId> findAllTmct0bokIdByCanonCalculadoAndBaja(@Param("canonCalculado") char canonCalculado,
      @Param("isscrdiv") char isscrdiv);

  @Query("select c.id from Tmct0alc c where c.tmct0alo.tmct0bok.id = :bookid and c.cdestadoasig = 0 and c.canonCalculado = :canonCalculado  ")
  List<Tmct0alcId> findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndBaja(@Param("bookid") Tmct0bokId bookId,
      @Param("canonCalculado") char canonCalculado);

  @Query("select distinct c.tmct0alo.id from Tmct0alc c "
      + " where c.tmct0alo.tmct0bok.fetrade = :fetrade and c.tmct0alo.tmct0bok.cdisin = :cdisin "
      + " and c.tmct0alo.tmct0bok.cdtpoper = :sentido and c.cdestadoasig > 0 "
      + " and c.canonCalculado = :canonCalculado and c.referenciaTitular.idreferencia > 0 "
      + " and c.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv and c.tmct0alo.tmct0bok.casepti = 'S' "
      + " order by c.id.nucnfclt asc ")
  List<Tmct0aloId> findAllTmct0AloIdByFechaAndCdisinAndSentidoAndCasadoSinCanonCalculadoConReferenciaTitular(
      @Param("fetrade") Date fetrade, @Param("cdisin") String cdisin, @Param("sentido") char sentido,
      @Param("canonCalculado") char canonCalculado, @Param("isscrdiv") char isscrdiv);

  @Query("select c.id from Tmct0alc c where c.tmct0alo.tmct0bok.id = :bookid and c.cdestadoasig > 0 and c.canonCalculado = :canonCalculado and c.referenciaTitular.idreferencia > 0 "
      + " order by c.id.nuorden asc, c.id.nbooking asc, c.id.nucnfclt asc, c.id.nucnfliq asc ")
  List<Tmct0alcId> findAllTmct0alcIdByTmct0bookIdAndCanonCalculadoAndConReferenciaTitular(
      @Param("bookid") Tmct0bokId bookId, @Param("canonCalculado") char canonCalculado);

  @Modifying
  @Query("UPDATE Tmct0alc c SET c.canonCalculado = :calculado, c.fhaudit = :fhaudit, c.cdusuaud = :cdusuaud "
      + " WHERE c.referenciaTitular.idreferencia = :idreferenciaTitular AND c.feejeliq = :fecontratacion AND c.cdestadoasig > 0")
  int updateFlagCanonCalculado(@Param("idreferenciaTitular") long idReferenciaTitular,
      @Param("fecontratacion") Date fecontratacion, @Param("calculado") char calculado, @Param("fhaudit") Date fhaudit,
      @Param("cdusuaud") String cdusuaud);

  @Modifying
  @Query("UPDATE Tmct0alc c SET c.canonCalculado = :calculado, c.fhaudit = :fhaudit, c.cdusuaud = :cdusuaud "
      + " WHERE c.id = :id and c.canonCalculado != :calculado ")
  int updateFlagCanonCalculadoByAlcId(@Param("id") Tmct0alcId id, @Param("calculado") char calculado,
      @Param("fhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud);
  
  @Query(value = "SELECT " +  
      "alc.nuorden, alc.nbooking, alc.NUCNFCLT, alc.NUCNFLIQ nl " +  
      "FROM tmct0desgloseclitit DCT " +  
      "INNER JOIN tmct0desglosecamara DCM " +  
      "ON DCM.iddesglosecamara = DCT.iddesglosecamara " +  
      "AND DCM.fecontratacion = DCT.feejecuc " +  
      "AND DCM.cdestadoasig > 0 " +  
      "INNER JOIN tmct0alc ALC " +  
      "ON ALC.nuorden = DCM.nuorden " +  
      "AND ALC.nbooking = DCM.nbooking " +  
      "AND ALC.nucnfclt = DCM.nucnfclt " +  
      "AND ALC.nucnfliq = DCM.nucnfliq " +  
      "AND ALC.feejeliq = DCT.feejecuc " +  
      "AND ALC.cdestadoasig > 0 " +  
      "INNER JOIN tmct0alo ALO " +  
      "ON ALO.nuorden = ALC.nuorden " +  
      "AND ALO.nbooking = ALC.nbooking " +  
      "AND ALO.nucnfclt = ALC.nucnfclt " +  
      "AND ALO.feoperac = DCT.feejecuc " +  
      "AND ALO.cdestadoasig > 0 " +  
      "INNER JOIN tmct0bok BOK " +  
      "ON BOK.nuorden = ALO.nuorden " +  
      "AND BOK.nbooking = ALO.nbooking " +  
      "AND BOK.fetrade = DCT.feejecuc " +  
      "AND BOK.cdestadoasig > 0 " +  
      "AND Date(DCT.feejecuc) BETWEEN :dateIni AND :dateFin " + 
      "AND COALESCE(DCT.cuenta_compensacion_destino, '') = '' " +  
      "AND DCT.miembro_compensador_destino IN :listaMiembrosCompensadorDestino " +  
      "AND DCT.cd_ref_asignacion IN :listaRefAsignacion " +  
      "AND ALC.cdestadocont_canon IN :listaEstadosCanon " 
      , nativeQuery = true) 
  List<Object[]> findAllTmct0alcOpsNoCobrablesCanon( 
      @Param("listaEstadosCanon")final Set<String> listaEstadosCanon, 
      @Param("listaMiembrosCompensadorDestino")final Set<String> listaMiembrosCompensadorDestino, 
      @Param("listaRefAsignacion")final Set<String> listaRefAsignacion, 
      @Param("dateIni") final Date dateIni,  
      @Param("dateFin") final Date dateFin); 
   
  @Query(value = "SELECT " +  
      "alc.nuorden, alc.nbooking, alc.NUCNFCLT, alc.NUCNFLIQ nl " +  
      "FROM tmct0desgloseclitit DCT " +  
      "INNER JOIN tmct0desglosecamara DCM " +  
      "ON DCM.iddesglosecamara = DCT.iddesglosecamara " +  
      "AND DCM.fecontratacion = DCT.feejecuc " +  
      "AND DCM.cdestadoasig > 0 " +  
      "INNER JOIN tmct0alc ALC " +  
      "ON ALC.nuorden = DCM.nuorden " +  
      "AND ALC.nbooking = DCM.nbooking " +  
      "AND ALC.nucnfclt = DCM.nucnfclt " +  
      "AND ALC.nucnfliq = DCM.nucnfliq " +  
      "AND ALC.feejeliq = DCT.feejecuc " +  
      "AND ALC.cdestadoasig > 0 " +  
      "INNER JOIN tmct0alo ALO " +  
      "ON ALO.nuorden = ALC.nuorden " +  
      "AND ALO.nbooking = ALC.nbooking " +  
      "AND ALO.nucnfclt = ALC.nucnfclt " +  
      "AND ALO.feoperac = DCT.feejecuc " +  
      "AND ALO.cdestadoasig > 0 " +  
      "INNER JOIN tmct0bok BOK " +  
      "ON BOK.nuorden = ALO.nuorden " +  
      "AND BOK.nbooking = ALO.nbooking " +  
      "AND BOK.fetrade = DCT.feejecuc " +  
      "AND BOK.cdestadoasig > 0 " +  
      "AND Date(DCT.feejecuc) BETWEEN :dateIni AND :dateFin " + 
      "AND COALESCE(DCT.cuenta_compensacion_destino, '') = '' " +  
      "AND DCT.miembro_compensador_destino IN :listaMiembrosCompensadorDestino " +  
      "AND DCT.cd_ref_asignacion IN :listaRefAsignacion " +  
      "AND ALC.cdestadocont IN :listaEstadosCorretaje " 
      , nativeQuery = true) 
  List<Object[]> findAllTmct0alcOpsNoCobrablesCorretaje( 
      @Param("listaEstadosCorretaje")final Set<String> listaEstadosCorretaje, 
      @Param("listaMiembrosCompensadorDestino")final Set<String> listaMiembrosCompensadorDestino, 
      @Param("listaRefAsignacion")final Set<String> listaRefAsignacion, 
      @Param("dateIni") final Date dateIni,  
      @Param("dateFin") final Date dateFin); 

} // Tmct0alcDao
