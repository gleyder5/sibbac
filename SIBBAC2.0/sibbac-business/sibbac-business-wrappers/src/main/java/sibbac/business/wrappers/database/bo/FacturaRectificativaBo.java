package sibbac.business.wrappers.database.bo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.dao.FacturaRectificativaDao;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.dao.specifications.FacturaRectificativaSpecificacions;
import sibbac.business.wrappers.database.dto.FacturaDTO;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaDetalle;
import sibbac.business.wrappers.database.model.FacturaRectificativa;
import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.business.wrappers.database.model.Idioma;
import sibbac.business.wrappers.database.model.Monedas;
import sibbac.business.wrappers.database.model.Parametro;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.business.wrappers.database.model.TipoDeDocumento;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.common.ExportType;
import sibbac.common.PDFMail;
import sibbac.common.XLSFactura;
import sibbac.common.currency.CurrencyConverter;
import sibbac.common.export.excel.ExcelBuilder;
import sibbac.common.export.pdf.PDFBuilder;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.JobPK;

@Service
public class FacturaRectificativaBo extends AbstractBo<FacturaRectificativa, Long, FacturaRectificativaDao> {

  private static final Character COMPRA = 'C';

  @Autowired
  private Tmct0estadoBo estadoBo;

  @Autowired
  private IdiomaBo idiomaBo;

  @Autowired
  private MonedasBo monedaBo;

  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  @Autowired
  private FacturaBo facturaBo;

  @Autowired
  private TipoDeDocumentoBo tipoDeDocumentoBo;

  @Autowired
  private NumeradoresBo numeradoresBo;

  @Autowired
  private ExcelBuilder excelBuilder;
  
  @Autowired
  private TextosIdiomasBo textoIdiomaBo;
  
  @Autowired
  private SendMail sendMail;
  
  @Autowired
  private EtiquetasDao etiquetasDao;
  
  @Autowired
  private ParametroBo parametroBo;
  
  @Autowired
  private PDFBuilder pdfBuilder;

  @Autowired
  private GestorServicios gestorServicios;

  @Autowired
  private Tmct0infocompensacionDao infoCompDao;
  
  @Autowired
  private Tmct0alcBo alcBo;

  @Transactional
  public List<FacturaRectificativa> generarFacturasRectificativas() {
    List<FacturaRectificativa> facturasRectificativas = new ArrayList<FacturaRectificativa>();

    // Buscamos las facturas que estÃ©n en estado: pte de rectificar.
    final List<Factura> facturas = facturaBo.getFacturasPtesDeRectificar();
    if (CollectionUtils.isNotEmpty(facturas)) {

      // Estados de Factura
      final Tmct0estado estadoRectificada = estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.RECTIFICADA);

      // Estados contables
      final Tmct0estado ecRechazada = estadoBo.findByIdContabilidad(EstadosEnumerados.CONTABILIDAD.RECHAZADA);
      final Tmct0estado ecAnulada = estadoBo.findByIdContabilidad(EstadosEnumerados.CONTABILIDAD.ANULADA);
      final Tmct0estado ecPteGenerarFactura = estadoBo.findByIdContabilidad(EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA);
      final Tmct0estado ecFacturadaPteCobro = estadoBo.findByIdContabilidad(EstadosEnumerados.CONTABILIDAD.FACTURADA_PDTE_COBRO);

      final Tmct0estado estadoPendienteEnvio = estadoBo.findByIdFacturaRectificativa(EstadosEnumerados.FACTURA_RECTIFICATIVA.PTE_ENVIAR);
      final Tmct0estado estadoAlcLibre = estadoBo.findByIdAlcOrden(EstadosEnumerados.ALC_ORDENES.LIBRE);
      final Idioma idioma = idiomaBo.findByCodigo(Idioma.COD_IDIOMA_ES);
      final Monedas moneda = monedaBo.findByCodigo(Monedas.COD_MONEDA_EUR);
      final TipoDeDocumento tipoDeDocumento = tipoDeDocumentoBo.getTipoDeDocumento(TipoDeDocumento.RECTIFICATIVA);
      final Date fechaCreacion = new Date();
      Tmct0estado estadoContable = null;
      for (final Factura factura : facturas) {
        FacturaRectificativa facturaRectificativa = crearFacturaRectificativa(estadoPendienteEnvio, idioma, moneda, tipoDeDocumento, fechaCreacion,
                                                                              factura);
        factura.setEstado(estadoRectificada);

        // El estado "contable".
        estadoContable = factura.getEstadocont();
        if (estadoContable.equals(ecFacturadaPteCobro)) {
          factura.setEstadocont(ecAnulada);
        } else if (estadoContable.equals(ecPteGenerarFactura)) {
          factura.setEstadocont(ecRechazada);
        } else {
          LOG.warn("WARNING: Para marcar una factura como rectificada, el estado contable no es el apropiado: [{} ({})]",
                   estadoContable.getIdestado(), estadoContable.getNombre());
        }

        try {
          facturaRectificativa = dao.save(facturaRectificativa);
        } catch (Exception e) {
          LOG.error("Error grabado la factrua rectificativa [" + facturaRectificativa.getNumeroFactura() + "]", e);
        }
        facturaBo.save(factura);
        liberarAlcOrdenes(estadoAlcLibre, factura);
      }
    }
    return facturasRectificativas;
  }

  /**
   * @param estadoAlcLibre
   * @param factura
   */
  private void liberarAlcOrdenes(final Tmct0estado estadoAlcLibre, final Factura factura) {
    final List<AlcOrdenes> alcOrdenes = factura.getAlcOrdenes();
    for (final AlcOrdenes alcOrden : alcOrdenes) {
      alcOrden.setEstado(estadoAlcLibre);
    }
    alcOrdenesBo.save(alcOrdenes);
  }

  /**
   * @param estadoPendienteEnvio
   * @param idioma
   * @param moneda
   * @param tipoDeDocumento
   * @param fechaCreacion
   * @param factura
   * @param facturaRectificativa
   */
  private FacturaRectificativa crearFacturaRectificativa(final Tmct0estado estadoPendienteEnvio,
                                                         final Idioma idioma,
                                                         final Monedas moneda,
                                                         final TipoDeDocumento tipoDeDocumento,
                                                         final Date fechaCreacion,
                                                         final Factura factura) {
    final FacturaRectificativa facturaRectificativa = new FacturaRectificativa();
    facturaRectificativa.setFactura(factura);
    facturaRectificativa.setEstado(estadoPendienteEnvio);
    facturaRectificativa.setFechaCreacion(fechaCreacion);
    facturaRectificativa.setFechaFactura(fechaCreacion);
    facturaRectificativa.setMoneda(moneda);
    Periodos periodo = factura.getPeriodo();
    Identificadores identificador = periodo.getIdentificador();
    final Long numeroFactura = numeradoresBo.calcularNumeroSiguiente(tipoDeDocumento, identificador);
    facturaRectificativa.setNumeroFactura(numeroFactura);
    facturaRectificativa.setTipoDeDocumento(tipoDeDocumento);
    return facturaRectificativa;
  }

  @Transactional
  public List<FacturaRectificativa> enviarFacturas(final JobPK jobPk) {
    final String prefix = "[FacturaRectificativaBo::enviarFacturas] ";
    final Tmct0estado estadoPteEnvio = estadoBo.findByIdFacturaRectificativa(EstadosEnumerados.FACTURA_RECTIFICATIVA.PTE_ENVIAR);
    final List<FacturaRectificativa> facturasPteEnvio = (List<FacturaRectificativa>) dao.findByEstado(estadoPteEnvio);
    List<FacturaRectificativa> facturasEnviadas = new ArrayList<FacturaRectificativa>();
    if (CollectionUtils.isNotEmpty(facturasPteEnvio)) {
      final Tmct0estado estadoPteCobro = estadoBo.findByIdFacturaRectificativa(EstadosEnumerados.FACTURA_RECTIFICATIVA.ENVIADA);

      for (final FacturaRectificativa factura : facturasPteEnvio) {
        LOG.trace(prefix + "FacturaRectificativa [" + factura.getNumeroFactura() + "] PTE_ENVIO");
        LOG.trace(prefix + "FacturaRectificativa [" + factura.getNumeroFactura() + "] Enviando correo");
        enviarCorreo(jobPk, factura);
        LOG.trace(prefix + "FacturaRectificativa [" + factura.getNumeroFactura() + "] Correo enviado");
        factura.setEstado(estadoPteCobro);
        facturasEnviadas.add(factura);
        LOG.trace(prefix + "  Factura enviada [ID: {}, Numero Factura: {}]", factura.getId(), factura.getNumeroFactura());

      }
      facturasEnviadas = dao.save(facturasEnviadas);
    }
    return facturasEnviadas;
  }

  /**
   * @param facturas
   * @return
   */
  private List<FacturaDTO> getListadoDTO(final Collection<FacturaRectificativa> facturas) {
    final List<FacturaDTO> resultado = new ArrayList<FacturaDTO>();
    if (CollectionUtils.isNotEmpty(facturas)) {
      for (final FacturaRectificativa factura : facturas) {
        final FacturaDTO facturaDto = new FacturaDTO(factura);
        resultado.add(facturaDto);
      }
    }
    return resultado;
  }

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturas(final Long aliasId, final Date fechaDesde, final Date fechaHasta, final Integer idEstado) {
    final List<Integer> estados = new ArrayList<Integer>();
    if (idEstado != null) {
      estados.add(idEstado);
    }
    final Collection<FacturaRectificativa> facturas = dao.findAll(FacturaRectificativaSpecificacions.listadoFriltrado(aliasId, fechaDesde,
                                                                                                                      fechaHasta, estados));
    return getListadoDTO(facturas);
  }

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturas(final Long numeroFactura) {
    final Collection<FacturaRectificativa> facturas = dao.findAll(FacturaRectificativaSpecificacions.listadoNumeroFactura(numeroFactura));
    return getListadoDTO(facturas);
  }

  /**
   * @param rectificativa
   */
  public void enviarCorreo(final JobPK jobPk, final FacturaRectificativa rectificativa) {
    String prefix = "[FacturaRectificativaBo::enviarCorreo] ";
    // Generamos el excel
    InputStream inputExcel = crearExcel(rectificativa);
    // Generamos el PDF
    final InputStream inputPDF = crearPDF(rectificativa, false);

    // Recuperamos los contactos
    final Alias alias = rectificativa.getAlias();
    // final List< Contacto > contactos = contactosBo.findAllByAlias( alias
    // );
    final List<Contacto> contactos = gestorServicios.getContactos(alias, jobPk);
    final List<String> destinatarios = new ArrayList<String>();

    if (contactos == null || contactos.isEmpty()) {
      LOG.error(prefix + "ERROR: No se puede enviar el correo a este alias [{}] porque carece de contactos!", alias.getCdaliass());
    } else {
      for (Contacto destinatario : contactos) {
        String[] mail = destinatario.getEmail().split(";");
        for (int i = 0; i < mail.length; i++) {
          destinatarios.add(mail[i].trim());
        }
      }

      // Nombre del fichero destino (<Nombre>.<extension>)
      final List<String> nameDest = new ArrayList<String>();
      /*
       * nameDest.add( "Ordenes.xls" ); nameDest.add( "FacturaRectificativa_" + rectificativa.getNumeroFactura() + ".pdf" );
       */
      Date now = new Date();
      nameDest.add(this.generateFileNameForAttachment(ExportType.EXCEL, rectificativa, now));
      nameDest.add(this.generateFileNameForAttachment(ExportType.PDF_RECTIFICATIVA, rectificativa, now));

      // InputStream con datos de entrada
      final List<InputStream> inputstream = new ArrayList<InputStream>();
      inputstream.add(inputExcel);
      inputstream.add(inputPDF);

      // Montamos el mapping con los datos que se van a sustituir en el
      // cuerpo
      // del Mail
      Map<String, String> mapping = createMappingCuerpoMail(rectificativa);

      // Recuperamos el body de base de datos
      String body = textoIdiomaBo.recuperarTextoIdioma("H", alias.getIdioma());
      body = reemplazarKeys(body, mapping);

      String asunto = textoIdiomaBo.recuperarTextoIdioma("G", alias.getIdioma());
      asunto = reemplazarKeys(asunto, mapping);

      try {
        sendMail.sendMail(destinatarios, "Factura Rectificativa", body, inputstream, nameDest, null);
      } catch (MailSendException | IllegalArgumentException | MessagingException | IOException e) {
        LOG.error("Error enviando el correo de facturas", e.getCause());
      }
    }
  }

  private String generateFileNameForAttachment(ExportType e, final FacturaRectificativa r, final Date d) {
    StringBuffer s = new StringBuffer();

    s.append(e.getFileName());
    s.append("_");
    s.append("[" + String.valueOf(r.getNumeroFactura()) + "]");
    s.append("_");
    s.append("[" + FormatDataUtils.convertDateToFileName(d) + "]");
    s.append(".");
    if (e.equals(ExportType.EXCEL)) {
      s.append("xls");
    } else {
      s.append("pdf");
    }

    return s.toString();
  }

  /**
   * @param factura
   * @return
   */
  private InputStream crearExcel(final FacturaRectificativa facturaRectificativa) {
    final Factura factura = facturaRectificativa.getFactura();
    final List<AlcOrdenes> ordenes = factura.getAlcOrdenes();
    final List<XLSFactura> lineas = new ArrayList<XLSFactura>();
    String referenciaAsignacion = null;
    final Map<String, String> lineasCabeceras = textoIdiomaBo.recuperarCabecerasExcelFactura(factura.getAlias().getCodigoIdioma());
    Locale locale = new Locale(factura.getAlias().getCodigoIdioma());
    Tmct0alcId alcId;
    Tmct0alc alc;
    
    if (CollectionUtils.isNotEmpty(ordenes)) {
      for (AlcOrdenes orden : ordenes) {
        alcId = orden.createAlcIdFromFields();
        alc = alcBo.findById(alcId);
        if(alc == null) {
            LOG.warn("[crearExcel] procesada una orden que no tiene alc asignada: {}", alcId);
            continue;
        }
        referenciaAsignacion = getReferenciaAsignacion(alc.getIdinfocomp());
        final XLSFactura linea = new XLSFactura(FormatDataUtils.convertDateToString(alc.getFeejeliq(), FormatDataUtils.DATE_FORMAT_SEPARATOR),
                alc.getNucnfclt() + alc.getNucnfliq().toString(), 
                alc.getCdtpoper() == COMPRA ? lineasCabeceras.get("GENERAL_ABREV_COMPRA") : lineasCabeceras.get("GENERAL_ABREV_VENTA"),
                alc.getCdisin(), alc.getNbvalors(), alc.getCdmercad(), alc.getNbtitliq(),
                referenciaAsignacion, alc.getNutitliq(), BigDecimal.ZERO, alc.getImcomisn());
        lineas.add(linea);
      }
    }

    InputStream inputExcel = null;
    try {
      final ByteArrayOutputStream out = excelBuilder.generaFactura(factura.getNumeroFactura().toString(), lineas, lineasCabeceras, locale);
      inputExcel = new ByteArrayInputStream(out.toByteArray());

    } catch (Exception e) {
      LOG.error("Error generando un PDF" + e);
    }

    return inputExcel;
  }

  private String getReferenciaAsignacion(final Long idInfoCompensacion) {
    String result = "";
    Tmct0infocompensacion infoComp = null;
    if (idInfoCompensacion != null) {
      infoComp = infoCompDao.findOne(idInfoCompensacion);
      if (infoComp != null && StringUtils.isNotEmpty(infoComp.getCdrefasignacion())) {
        result = infoComp.getCdrefasignacion();
      }
    }

    return result;

  }

  /**
   * @param rectificativa
   * @param facturaDetalle
   * @return
   */
  private InputStream crearPDF(final FacturaRectificativa rectificativa, boolean duplicado) {
    InputStream inputPDF = null;
    try {
      Factura factura = rectificativa.getFactura();
      FacturaDetalle facturaDetalle = factura.getDetalleFactura(FacturaDetalle.NUMERO_LINEA_FACTURA);
      List<Parametro> parametros = (List<Parametro>) parametroBo.findAll();

      Map<String, String> literales = textoIdiomaBo.recuperarTextosFacturas(factura.getAlias().getCodigoIdioma());

      final Parametro parametro = parametros.get(0);
      final ByteArrayOutputStream out = pdfBuilder.generaFactura(new PDFMail(factura.getNombreAlias(), rectificativa.getFechaFactura(),
                                                                             factura.getDireccion(), rectificativa.getNumeroFactura(),
                                                                             parametro.getRegistroMercantil(), facturaDetalle.getImfinsvb(),
                                                                             rectificativa.getImfinsvb(), rectificativa.getImporteImpuestos(),
                                                                             rectificativa.getImporteTotal(), parametro.CuentaBancariaToString(),
                                                                             parametro.IbanToString(), parametro.getBIC(), parametro.getSociedad(),
                                                                             duplicado, true), literales);
      inputPDF = new ByteArrayInputStream(out.toByteArray());
    } catch (Exception e) {
      LOG.error("Error generando un PDF" + e);
    }

    return inputPDF;
  }

  public Map<String, String> createMappingCuerpoMail(FacturaRectificativa rectificativa) {
    String key = null;
    String value = null;
    Factura factura = rectificativa.getFactura();
    Map<String, String> mapping = new HashMap<String, String>();
    List<Etiquetas> etiquetas = null;
    try {
      etiquetas = (List<Etiquetas>) etiquetasDao.findAllByTabla("Factura");
      for (Etiquetas etiqueta : etiquetas) {
        key = etiqueta.getEtiqueta();
        String campo = etiqueta.getCampo();
        switch (campo) {
          case "NombreAlias":
            value = factura.getNombreAlias();
            break;
          case "FechaFactura":
            value = FormatDataUtils.convertDateToStringSeparator((Date) factura.getFechaFactura()).toString();
            break;
          case "Cif":
            value = factura.getCif();
            break;
          case "NumeroFactura":
            value = factura.getNumeroFactura().toString();
            break;
          case "Imfinsvb":
            value = CurrencyConverter.convertToEuro(factura.getImporteTotal());
            break;
          default:
            value = "<No data>";
        }
        mapping.put(key, value);
      }
    } catch (Exception e) {
      // Nothing
    }
    try {
      etiquetas = (List<Etiquetas>) etiquetasDao.findAllByTabla("FacturaRectificativa");
      for (Etiquetas etiqueta : etiquetas) {
        key = etiqueta.getEtiqueta();
        String campo = etiqueta.getCampo();
        switch (campo) {
          case "fechaFactura":
            value = FormatDataUtils.convertDateToStringSeparator((Date) rectificativa.getFechaFactura()).toString();
            break;
          case "NBCIF":
            value = factura.getCif();
            break;
          case "numeroFactura":
            value = rectificativa.getNumeroFactura().toString();
            break;
          case "NBNOMBREALIAS":
            value = factura.getNombreAlias();
            break;
          default:
            value = "<No data>";
        }
        mapping.put(key, value);
      }
    } catch (Exception e) {
      // Nothing
    }
    return mapping;
  }

  public String reemplazarKeys(String body, Map<String, String> mapping) {
    Set<String> keys = mapping.keySet();
    String value = null;
    for (String key : keys) {
      value = mapping.get(key).trim();
      body = body.replace(key, value);
    }
    return body;
  }

}
