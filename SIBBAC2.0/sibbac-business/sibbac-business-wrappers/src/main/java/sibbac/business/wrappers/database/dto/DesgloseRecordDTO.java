package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.canones.commons.ReglasCanonesDTO;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.data.Tmct0AloDataOperacionesEspecialesPartenon;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.OrdenComercial;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoCambio;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoOperacion;
import sibbac.business.wrappers.database.model.RecordBean;

public class DesgloseRecordDTO extends RecordBean {

  /**
	 * 
	 */
  private static final long serialVersionUID = -1080836452429154553L;

  private Long id;
  private String codEmprl;
  private String emprCTVA;
  private String centCTVA;
  private String dpteCTVA;
  private String numeroOrden;
  private String numeroReferenciaOrden;
  private Short nucnfliq;
  private Date fechaProcesamiento;
  private String codigoSV;
  private Date fechaEjecucion;
  private String codigoBolsaEjecucion;
  private String codigoValorISO;
  private TipoOperacion tipoOperacion;
  private TipoCambio tipoCambio;
  private BigDecimal titulosEjecutados;
  private BigDecimal nominalEjecutado;
  private BigDecimal cambioOperacion;
  private String descripcionValor;
  private String bicTitular;
  private String tipoSaldo;
  private String referenciaModelo;
  private String segmento;
  private OrdenComercial ordenComercial;
  private Integer porcesado;
  // Campos para crear los registros 10 y 15 para el fichero de respuesta MQ.
  private Date fhLote;
  private Date hlote;
  private List<BigDecimal> listaTitulos;
  private List<Date> listaFhEjec;
  private List<Date> listaHejec;
  private Date fLiqui;
  private List<String> listaCDOrgNeg;
  private List<String> listaNumEjeCu;
  private List<String> listaNEjecMer;
  private String refTitu;
  private String refAdic;
  private String divisa;
  private BigDecimal corretajes;
  private BigDecimal canCont;
  private BigDecimal payerComission;
  private Character abcanCom;
  private BigDecimal canComp;

  private Short nupareje;

  private String ccv;

  private Tmct0AloDataOperacionesEspecialesPartenon aloDataOperacionesEspecialesPartenon;

  private List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesUtilizadas;

  private List<DesgloseRecordEjecucionesDTO> ejecuciones;

  private boolean isLastDesglose = false;

  private boolean isRouting = false;
  private boolean isOperacionEspecial = false;

  private CanonesConfigDTO canonesConfig;
  private Tcli0comDTO tclicomDto;

  private int index = 0;

  public DesgloseRecordDTO() {

  }// public DesgloseRecordDTO()

  public DesgloseRecordDTO(String numeroOrden) {
    this.setNumeroOrden(numeroOrden);
  }// public DesgloseRecordDTO()

  public DesgloseRecordDTO(long id, String numeroOrden, String numeroReferenciaOrden, BigDecimal titulosEjecutados,
      String codigoBolsaEjecucion, String ordenComercial, String clearer, String tipoSaldo) {
    this.setId(id);
    this.setNumeroOrden(numeroOrden);
    this.setNumeroReferenciaOrden(numeroReferenciaOrden);
    this.setTitulosEjecutados(titulosEjecutados);
    this.setCodigoBolsaEjecucion(codigoBolsaEjecucion);
    OrdenComercial ordenComercialAux = null;
    if (StringUtils.isNotBlank(ordenComercial)) {
      ordenComercialAux = OrdenComercial.getOrdenComercial(ordenComercial.toString());
    }
    this.setOrdenComercial(ordenComercialAux);
    this.setCodEmprl(clearer);
    this.setTipoSaldo(tipoSaldo);
  }

  public DesgloseRecordDTO(long id, String codEmprl, String emprCTVA, String centCTVA, String dpteCTVA,
      String numeroOrden, String numeroReferenciaOrden, short nucnfliq, Date fechaProcesamiento, String codigoSV,
      Date fechaEjecucion, String codigoBolsaEjecucion, String codigoValorISO, TipoOperacion tipoOperacion,
      TipoCambio tipoCambio, BigDecimal titulosEjecutados, BigDecimal nominalEjecutado, BigDecimal cambioOperacion,
      String descripcionValor, String bicTitular, String tipoSaldo, String referenciaModelo, String segmento,
      String ordenComercial, Integer porcesado, String ccv) {
    this.setId(id);
    this.setCodEmprl(codEmprl);
    this.setEmprCTVA(emprCTVA);
    this.setCentCTVA(centCTVA);
    this.setDpteCTVA(dpteCTVA);
    this.setNumeroOrden(numeroOrden);
    this.setNumeroReferenciaOrden(numeroReferenciaOrden);
    this.setNucnfliq(nucnfliq);
    this.setFechaProcesamiento(fechaProcesamiento);
    this.setCodigoSV(codigoSV);
    this.setFechaEjecucion(fechaEjecucion);
    this.setCodigoBolsaEjecucion(codigoBolsaEjecucion);
    this.setCodigoValorISO(codigoValorISO);
    this.setTipoOperacion(tipoOperacion);
    this.setTipoCambio(tipoCambio);
    this.setTitulosEjecutados(titulosEjecutados);
    this.setNominalEjecutado(nominalEjecutado);
    this.setCambioOperacion(cambioOperacion);
    this.setDescripcionValor(descripcionValor);
    this.setBicTitular(bicTitular);
    this.setTipoSaldo(tipoSaldo);
    this.setReferenciaModelo(referenciaModelo);
    this.setSegmento(segmento);
    OrdenComercial ordenComercialAux = null;
    if (ordenComercial != null) {
      ordenComercialAux = OrdenComercial.getOrdenComercial(ordenComercial.toString());
      // ordenComercialAux = ordenComercial;
    }
    this.setOrdenComercial(ordenComercialAux);
    this.setPorcesado(porcesado);
    this.setRefAdic(ccv);
    this.setRefTitu(ccv);
    this.setCcv(ccv);
  }

  public DesgloseRecordDTO(long id, String codEmprl, String emprCTVA, String centCTVA, String dpteCTVA,
      String numeroOrden, String numeroReferenciaOrden, short nucnfliq, Date fechaProcesamiento, String codigoSV,
      Date fechaEjecucion, String codigoBolsaEjecucion, String codigoValorISO, TipoOperacion tipoOperacion,
      TipoCambio tipoCambio, BigDecimal titulosEjecutados, BigDecimal nominalEjecutado, BigDecimal cambioOperacion,
      String descripcionValor, String bicTitular, String tipoSaldo, String referenciaModelo, String segmento,
      String ordenComercial, Integer porcesado, String ccv, List<DesgloseRecordEjecucionesDTO> ejecuciones) {
    this(id, codEmprl, emprCTVA, centCTVA, dpteCTVA, numeroOrden, numeroReferenciaOrden, nucnfliq, fechaProcesamiento,
        codigoSV, fechaEjecucion, codigoBolsaEjecucion, codigoValorISO, tipoOperacion, tipoCambio, titulosEjecutados,
        nominalEjecutado, cambioOperacion, descripcionValor, bicTitular, tipoSaldo, referenciaModelo, segmento,
        ordenComercial, porcesado, ccv);
    this.ejecuciones = ejecuciones;

  }

  public Date getfLiqui() {
    return fLiqui;
  }

  public void setfLiqui(Date fLiqui) {
    this.fLiqui = fLiqui;
  }

  public String getCcv() {
    return ccv;
  }

  public void setCcv(String ccv) {
    this.ccv = ccv;
  }

  /**
   * @return the codEmprl
   */
  public Long getId() {
    return id;
  }

  /**
   * @param codEmprl the codEmprl to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the codEmprl
   */
  public String getCodEmprl() {
    return codEmprl;
  }

  /**
   * @param codEmprl the codEmprl to set
   */
  public void setCodEmprl(String codEmprl) {
    this.codEmprl = codEmprl;
  }

  /**
   * @return the emprCTVA
   */
  public String getEmprCTVA() {
    return emprCTVA;
  }

  /**
   * @param emprCTVA the emprCTVA to set
   */
  public void setEmprCTVA(String emprCTVA) {
    this.emprCTVA = emprCTVA;
  }

  /**
   * @return the centCTVA
   */
  public String getCentCTVA() {
    return centCTVA;
  }

  /**
   * @param centCTVA the centCTVA to set
   */
  public void setCentCTVA(String centCTVA) {
    this.centCTVA = centCTVA;
  }

  /**
   * @return the dpteCTVA
   */
  public String getDpteCTVA() {
    return dpteCTVA;
  }

  /**
   * @param dpteCTVA the dpteCTVA to set
   */
  public void setDpteCTVA(String dpteCTVA) {
    this.dpteCTVA = dpteCTVA;
  }

  /**
   * @return the numeroOrden
   */
  public String getNumeroOrden() {
    return numeroOrden;
  }

  /**
   * @param numeroOrden the numeroOrden to set
   */
  public void setNumeroOrden(String numeroOrden) {
    this.numeroOrden = numeroOrden;
  }

  /**
   * @return the numeroReferenciaOrden
   */
  public String getNumeroReferenciaOrden() {
    return numeroReferenciaOrden;
  }

  /**
   * @param numeroReferenciaOrden the numeroReferenciaOrden to set
   */
  public void setNumeroReferenciaOrden(String numeroReferenciaOrden) {
    this.numeroReferenciaOrden = numeroReferenciaOrden;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @return the fechaProcesamiento
   */
  public Date getFechaProcesamiento() {
    return fechaProcesamiento;
  }

  /**
   * @param fechaProcesamiento the fechaProcesamiento to set
   */
  public void setFechaProcesamiento(Date fechaProcesamiento) {
    this.fechaProcesamiento = fechaProcesamiento;
  }

  /**
   * @return the codigoSV
   */
  public String getCodigoSV() {
    return codigoSV;
  }

  /**
   * @param codigoSV the codigoSV to set
   */
  public void setCodigoSV(String codigoSV) {
    this.codigoSV = codigoSV;
  }

  /**
   * @return the fechaEjecucion
   */
  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  /**
   * @param fechaEjecucion the fechaEjecucion to set
   */
  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  /**
   * @return the codigoBolsaEjecucion
   */
  public String getCodigoBolsaEjecucion() {
    return codigoBolsaEjecucion;
  }

  /**
   * @param codigoBolsaEjecucion the codigoBolsaEjecucion to set
   */
  public void setCodigoBolsaEjecucion(String codigoBolsaEjecucion) {
    this.codigoBolsaEjecucion = codigoBolsaEjecucion;
  }

  /**
   * @return the codigoValorISO
   */
  public String getCodigoValorISO() {
    return codigoValorISO;
  }

  /**
   * @param codigoValorISO the codigoValorISO to set
   */
  public void setCodigoValorISO(String codigoValorISO) {
    this.codigoValorISO = codigoValorISO;
  }

  /**
   * @return the tipoOperacion
   */
  public TipoOperacion getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * @param tipoOperacion the tipoOperacion to set
   */
  public void setTipoOperacion(TipoOperacion tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * @return the tipoCambio
   */
  public TipoCambio getTipoCambio() {
    return tipoCambio;
  }

  /**
   * @param tipoCambio the tipoCambio to set
   */
  public void setTipoCambio(TipoCambio tipoCambio) {
    this.tipoCambio = tipoCambio;
  }

  /**
   * @return the titulosEjecutados
   */
  public BigDecimal getTitulosEjecutados() {
    return titulosEjecutados;
  }

  /**
   * @param titulosEjecutados the titulosEjecutados to set
   */
  public void setTitulosEjecutados(BigDecimal titulosEjecutados) {
    this.titulosEjecutados = titulosEjecutados;
  }

  /**
   * @return the nominalEjecutado
   */
  public BigDecimal getNominalEjecutado() {
    return nominalEjecutado;
  }

  /**
   * @param nominalEjecutado the nominalEjecutado to set
   */
  public void setNominalEjecutado(BigDecimal nominalEjecutado) {
    this.nominalEjecutado = nominalEjecutado;
  }

  /**
   * @return the cambioOperacion
   */
  public BigDecimal getCambioOperacion() {
    return cambioOperacion;
  }

  /**
   * @param cambioOperacion the cambioOperacion to set
   */
  public void setCambioOperacion(BigDecimal cambioOperacion) {
    this.cambioOperacion = cambioOperacion;
  }

  /**
   * @return the descripcionValor
   */
  public String getDescripcionValor() {
    return descripcionValor;
  }

  /**
   * @param descripcionValor the descripcionValor to set
   */
  public void setDescripcionValor(String descripcionValor) {
    this.descripcionValor = descripcionValor;
  }

  /**
   * @return the bicTitular
   */
  public String getBicTitular() {
    return bicTitular;
  }

  /**
   * @param bicTitular the bicTitular to set
   */
  public void setBicTitular(String bicTitular) {
    this.bicTitular = bicTitular;
  }

  /**
   * @return the tipoSaldo
   */
  public String getTipoSaldo() {
    return tipoSaldo;
  }

  /**
   * @param tipoSaldo the tipoSaldo to set
   */
  public void setTipoSaldo(String tipoSaldo) {
    this.tipoSaldo = tipoSaldo;
  }

  /**
   * @return the referenciaModelo
   */
  public String getReferenciaModelo() {
    return referenciaModelo;
  }

  /**
   * @param referenciaModelo the referenciaModelo to set
   */
  public void setReferenciaModelo(String referenciaModelo) {
    this.referenciaModelo = referenciaModelo;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @return the ordenComercial
   */
  public OrdenComercial getOrdenComercial() {
    return ordenComercial;
  }

  /**
   * @param ordenComercial the ordenComercial to set
   */
  public void setOrdenComercial(OrdenComercial ordenComercial) {
    this.ordenComercial = ordenComercial;
  }

  /**
   * @return the porcesado
   */
  public Integer getPorcesado() {
    return porcesado;
  }

  /**
   * @param porcesado the porcesado to set
   */
  public void setPorcesado(Integer porcesado) {
    this.porcesado = porcesado;
  }

  public Date getFhLote() {
    return this.fhLote;
  }

  public void setFhLote(Date fhLote) {
    this.fhLote = fhLote;
  }

  public Date getHlote() {
    return this.hlote;
  }

  public void setHlote(Date hlote) {
    this.hlote = hlote;
  }

  public List<Date> getListaFhEjec() {
    return this.listaFhEjec;
  }

  public void setListaFhEjec(List<Date> listaFhEjec) {
    this.listaFhEjec = listaFhEjec;
  }

  public List<Date> getListaHejec() {
    return this.listaHejec;
  }

  public void setListaHejec(List<Date> listaHejec) {
    this.listaHejec = listaHejec;
  }

  public Date getFLiqui() {
    return this.fLiqui;
  }

  public void setFLiqui(Date fLiqui) {
    this.fLiqui = fLiqui;
  }

  public List<String> getListaCDOrgNeg() {
    return this.listaCDOrgNeg;
  }

  public void setListaCDOrgNeg(List<String> listaCDOrgNeg) {
    this.listaCDOrgNeg = listaCDOrgNeg;
  }

  public List<String> getListaNumEjeCu() {
    return this.listaNumEjeCu;
  }

  public void setListaNumEjeCu(List<String> listaNumEjeCu) {
    this.listaNumEjeCu = listaNumEjeCu;
  }

  public List<String> getListaNEjecMer() {
    return this.listaNEjecMer;
  }

  public void setListaNEjecMer(List<String> listaNEjecMer) {
    this.listaNEjecMer = listaNEjecMer;
  }

  public String getRefTitu() {
    return this.refTitu;
  }

  public void setRefTitu(String reftitular) {
    this.refTitu = reftitular;
  }

  public String getRefAdic() {
    return this.refAdic;
  }

  public void setRefAdic(String refAdic) {
    this.refAdic = refAdic;
  }

  public String getDivisa() {
    return this.divisa;
  }

  public void setDivisa(String divisa) {
    this.divisa = divisa;
  }

  public BigDecimal getCorretajes() {
    return this.corretajes;
  }

  public void setCorretajes(BigDecimal corretajes) {
    this.corretajes = corretajes;
  }

  public BigDecimal getCanCont() {
    return this.canCont;
  }

  public void setCanCont(BigDecimal canCont) {
    this.canCont = canCont;
  }

  public Character getAbcanCom() {
    return this.abcanCom;
  }

  public void setAbcanCom(Character abcanCom) {
    this.abcanCom = abcanCom;
  }

  public BigDecimal getCanComp() {
    return this.canComp;
  }

  public void setCanComp(BigDecimal canComp) {
    this.canComp = canComp;
  }

  public Short getNupareje() {
    return nupareje;
  }

  public void setNupareje(Short nupareje) {
    this.nupareje = nupareje;
  }

  public List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> getEjecucionesUtilizadas() {
    return ejecucionesUtilizadas;
  }

  public void setEjecucionesUtilizadas(List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionesUtilizadas) {
    this.ejecucionesUtilizadas = ejecucionesUtilizadas;
  }

  public List<BigDecimal> getListaTitulos() {
    return listaTitulos;
  }

  public void setListaTitulos(List<BigDecimal> listaTitulos) {
    this.listaTitulos = listaTitulos;
  }

  public BigDecimal getPayerComission() {
    return payerComission;
  }

  public void setPayerComission(BigDecimal payerComission) {
    this.payerComission = payerComission;
  }

  public Tmct0AloDataOperacionesEspecialesPartenon getAloDataOperacionesEspecialesPartenon() {
    return aloDataOperacionesEspecialesPartenon;
  }

  public void setAloDataOperacionesEspecialesPartenon(
      Tmct0AloDataOperacionesEspecialesPartenon aloDataOperacionesEspecialesPartenon) {
    this.aloDataOperacionesEspecialesPartenon = aloDataOperacionesEspecialesPartenon;
  }

  public List<DesgloseRecordEjecucionesDTO> getEjecuciones() {
    return ejecuciones;
  }

  public void setEjecuciones(List<DesgloseRecordEjecucionesDTO> ejecuciones) {
    this.ejecuciones = ejecuciones;
  }

  public boolean isLastDesglose() {
    return isLastDesglose;
  }

  public void setLastDesglose(boolean isLastDesglose) {
    this.isLastDesglose = isLastDesglose;
  }

  public Tcli0comDTO getTclicomDto() {
    return tclicomDto;
  }

  public void setTclicomDto(Tcli0comDTO tclicomDto) {
    this.tclicomDto = tclicomDto;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  public boolean isRouting() {
    return isRouting;
  }

  public void setRouting(boolean isRouting) {
    this.isRouting = isRouting;
  }

  public boolean isOperacionEspecial() {
    return isOperacionEspecial;
  }

  public void setOperacionEspecial(boolean isOperacionEspecial) {
    this.isOperacionEspecial = isOperacionEspecial;
  }


  public CanonesConfigDTO getCanonesConfig() {
    return canonesConfig;
  }

  public void setCanonesConfig(CanonesConfigDTO canonesConfig) {
    this.canonesConfig = canonesConfig;
  }

  @Override
  public String toString() {
    return "DesgloseRecordDTO [id=" + id + ", codEmprl=" + codEmprl + ", emprCTVA=" + emprCTVA + ", centCTVA="
        + centCTVA + ", dpteCTVA=" + dpteCTVA + ", numeroOrden=" + numeroOrden + ", numeroReferenciaOrden="
        + numeroReferenciaOrden + ", nucnfliq=" + nucnfliq + ", fechaProcesamiento=" + fechaProcesamiento
        + ", codigoSV=" + codigoSV + ", fechaEjecucion=" + fechaEjecucion + ", codigoBolsaEjecucion="
        + codigoBolsaEjecucion + ", codigoValorISO=" + codigoValorISO + ", tipoOperacion=" + tipoOperacion
        + ", tipoCambio=" + tipoCambio + ", titulosEjecutados=" + titulosEjecutados + ", nominalEjecutado="
        + nominalEjecutado + ", cambioOperacion=" + cambioOperacion + ", descripcionValor=" + descripcionValor
        + ", bicTitular=" + bicTitular + ", tipoSaldo=" + tipoSaldo + ", referenciaModelo=" + referenciaModelo
        + ", segmento=" + segmento + ", ordenComercial=" + ordenComercial + ", porcesado=" + porcesado + ", fhLote="
        + fhLote + ", hlote=" + hlote + ", listaFhEjec=" + listaFhEjec + ", listaHejec=" + listaHejec + ", fLiqui="
        + fLiqui + ", listaCDOrgNeg=" + listaCDOrgNeg + ", listaNumEjeCu=" + listaNumEjeCu + ", listaNEjecMer="
        + listaNEjecMer + ", reftitular=" + refTitu + ", refAdic=" + refAdic + ", divisa=" + divisa + ", corretajes="
        + corretajes + ", canCont=" + canCont + ", abcanCom=" + abcanCom + ", canComp=" + canComp + ", nupareje="
        + nupareje + "]";
  }

}// public class DesgloseRecordDTO implements Serializable {
