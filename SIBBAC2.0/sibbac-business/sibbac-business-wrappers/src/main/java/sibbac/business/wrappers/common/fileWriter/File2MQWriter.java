package sibbac.business.wrappers.common.fileWriter;


import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.util.FicherosUtils;
import sibbac.common.SIBBACBusinessException;


//@Component
public class File2MQWriter {
	protected static final Logger	LOG			= LoggerFactory.getLogger( File2MQWriter.class );
	private Boolean					isForServer	= false;

	public void writeIntoQueue	( String queueConnectionFactoryName, String queueName, String fileName, String filePath )
			throws SIBBACBusinessException {
		LOG.debug( "[File2MQWriter :: writeIntoQueue] Init (QueueConnectionFactory=" + queueConnectionFactoryName + ", QueueName="
				+ queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")" );
		QueueConnectionFactory queueConnectionFactory = null;
		QueueConnection queueConnection = null;
		Queue queue = null;
		QueueSession queueSession = null;
		QueueSender queueSender = null;
		File file = null;
		try {
			file = FicherosUtils.fileToTemp( fileName, filePath );
		} catch ( IOException e1 ) {
			LOG.error( "[File2MQWriter :: writeIntoQueue] Error obteniendo y renombrando el fichero: " + e1.getMessage() );
			LOG.debug( "[File2MQWriter :: writeIntoQueue] Error --> ", e1 );
		}

		if ( file != null && file.exists() && file.length() > 0 ) {
			Integer hadError = 0;
			try {
				String initialContext = "java:comp/env";
				Context ctx = null;
				if ( isForServer ) {
					LOG.debug( "[File2MQWriter :: writeIntoQueue] Mirando el contexto inicial sin usar: " + initialContext );
					ctx = ( Context ) new InitialContext();
				} else {
					LOG.debug( "[File2MQWriter :: writeIntoQueue] Mirando el contexto inicial usando: " + initialContext );
					ctx = ( Context ) new InitialContext().lookup( initialContext );
				}
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Obteniendo recurso JNDI QueueConnectionFactory=" + queueConnectionFactoryName );
				queueConnectionFactory = ( QueueConnectionFactory ) ctx.lookup( queueConnectionFactoryName );
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Obteniendo recurso JNDI Queue=" + queueName );
				queue = ( Queue ) ctx.lookup( queueName );
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Se va a crear la conexión" );
				queueConnection = ( QueueConnection ) queueConnectionFactory.createQueueConnection();
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Conexión creada, se va a crear la sesión" );
				queueSession = ( QueueSession ) queueConnection.createQueueSession( true, Session.AUTO_ACKNOWLEDGE );
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Sesión creada, se va a crear el sender" );
				queueSender = ( QueueSender ) queueSession.createSender( queue );
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Sender creado, se le da comienzo a la conexión" );
				queueConnection.start();
				hadError = sendLines( file, queueSession, queueSender );
				if ( hadError == 0 ) {
					FicherosUtils.fileToTratados( file );
				} else {
					FicherosUtils.fileToErroneos( file );
				}
			} catch ( Exception e ) {
				LOG.error( "[File2MQWriter :: writeIntoQueue] Error. Líneas mandadas=" + hadError + " - " + e.getMessage() );
				LOG.debug( "[File2MQWriter :: writeIntoQueue] -> ERROR: ", e );
			} finally {
				try {
					if ( queueSender != null ) {
						queueSender.close();
					}
				} catch ( Exception e ) {
					LOG.error( "[File2MQWriter :: writeIntoQueue] Error cerrando el productor: " + e.getMessage() );
				}
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Productor cerrado, se va a cerrar la sesión" );

				try {
					if ( queueSession != null ) {
						queueSession.close();
					}
				} catch ( Exception e ) {
					LOG.error( "[File2MQWriter :: writeIntoQueue] Error cerrando la sesión: " + e.getMessage() );
				}
				LOG.debug( "[File2MQWriter :: writeIntoQueue] Sesión cerrada, se va a cerrar la conexión" );
				try {
					if ( queueConnection != null ) {
						queueConnection.close();
						LOG.debug( "[File2MQWriter :: writeIntoQueue] Conexión cerrada" );
					}
				} catch ( Exception e ) {
					LOG.error( "[File2MQWriter :: writeIntoQueue] Error cerrando la conexión: " + e.getMessage() );
				}
				if ( hadError != 0 ) {
					LOG.debug( "[File2MQWriter :: writeIntoQueue] Fin de la conexión - Mandadas " + hadError + " líneas (QueueName="
							+ queueName + ", FileName=" + fileName + ", FilePath=" + filePath + ")" );
				} else {
					LOG.debug( "[File2MQWriter :: writeIntoQueue] Fin de la conexión - Fichero enviado con éxito (QueueName=" + queueName
							+ ", FileName=" + fileName + ", FilePath=" + filePath + ")" );
				}
			}
		}
	}

	public Integer sendLines( File file, QueueSession queueSession, QueueSender queueSender ) {
		Integer sentLines = 0;
		TextMessage textMessage = null;
		Scanner sc = null;
		try {
			sc = new Scanner( file, "UTF-8" );
		} catch ( Exception e ) {
			LOG.error( "[File2MQWriter :: sendLines] Error abriendo el fichero" );
		}
		while ( sc.hasNextLine() ) {
			String fileLine = sc.nextLine();
			try {
				LOG.debug( "[MQ2FileWriter :: sendLines] Se va a crear el mensaje de texto con: " + fileLine );
				textMessage = ( TextMessage ) queueSession.createTextMessage();
				textMessage.setText( fileLine );
				queueSender.send( textMessage );
				LOG.debug( "[MQ2FileWriter :: sendLines] Mensaje mandado a cola: " + textMessage.getText() );
				queueSession.commit();
			} catch ( Exception e ) {
				LOG.error( "[File2MQWriter :: sendLines] Error mandando mensaje de texto. Líneas mandadas hasta ahora=" + sentLines + " - "
						+ e.getMessage() );
				sc.close();
				return sentLines;
			}
			sentLines++;
		}
		sc.close();
		LOG.debug( "[File2MQWriter :: sendLines] Fin de la lectura del fichero - Mandadas " + sentLines + " líneas" );
		return 0;
	}

}
