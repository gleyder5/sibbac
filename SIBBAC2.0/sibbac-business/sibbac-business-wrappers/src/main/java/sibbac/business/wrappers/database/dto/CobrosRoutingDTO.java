package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.fase0.database.model.Tmct0estado;

/**
 * @version 1.0
 * @author XI316153
 */
public class CobrosRoutingDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = 1L;

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(CobrosRoutingDTO.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Número de orden. */
  private String nuorden;

  /** Número de booking. */
  private String nbooking;

  /** Alo. */
  private String nucnfclt;

  /** Alc. */
  private Short nucnfliq;

  /** Ajuste. */
  private BigDecimal imajusvb;

  /** Comisión cliente. */
  private BigDecimal imcomisn;

  /** Canón de contratación. */
  private BigDecimal imcanoncontreu;

  /** Estado contable. */
  private Tmct0estado estadocont;

  /** Estado de asignación. */
  private Integer cdestadoasig;

  /** Estado de entrada/salida. */
  private Tmct0estado estadoentrec;

  /** Fecha de ejecución. */
  private Date feejeliq;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   * @param imajusvb
   * @param imcomisn
   * @param imcanoncontreu
   * @param estadocont
   * @param cdestadoasig
   * @param estadoentrec
   * @param feejeliq
   */
  public CobrosRoutingDTO(String nuorden,
                          String nbooking,
                          String nucnfclt,
                          Short nucnfliq,
                          BigDecimal imajusvb,
                          BigDecimal imcomisn,
                          BigDecimal imcanoncontreu,
                          Tmct0estado estadocont,
                          Integer cdestadoasig,
                          // Tmct0estado estadoentrec,
                          Date feejeliq) {
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.imajusvb = imajusvb;
    this.imcomisn = imcomisn;
    this.imcanoncontreu = imcanoncontreu;
    this.estadocont = estadocont;
    this.cdestadoasig = cdestadoasig;
    // this.estadoentrec = estadoentrec;
    this.feejeliq = feejeliq;
  } // CobrosRoutingDTO

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "CobrosRoutingDTO [nuorden=" + nuorden + ", nbooking=" + nbooking + ", nucnfclt=" + nucnfclt + ", nucnfliq=" + nucnfliq
        + ", imajusvb=" + imajusvb + ", imcomisn=" + imcomisn + ", imcanoncontreu=" + imcanoncontreu + ", estadocont="
        + estadocont.getNombre() + ", cdestadoasig=" + cdestadoasig + ", estadoentrec=" + estadoentrec.getNombre() + ", feejeliq="
        + feejeliq + "]";
  } // toString

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @return the imajusvb
   */
  @Column(name = "IMAJUSVB", precision = 18, scale = 4)
  public BigDecimal getImajusvb() {
    return imajusvb;
  }

  /**
   * @return the imcomisn
   */
  @Column(name = "IMCOMISN", nullable = false, precision = 18, scale = 8)
  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  /**
   * @return the imcanoncontreu
   */
  @Column(name = "IMCANONCONTREU", precision = 18, scale = 8)
  public BigDecimal getImcanoncontreu() {
    return imcanoncontreu;
  }

  /**
   * @return the estadocont
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOCONT")
  public Tmct0estado getEstadocont() {
    return estadocont;
  }

  /**
   * @return the cdestadoasig
   */
  @Column(name = "CDESTADOASIG", precision = 4)
  public Integer getCdestadoasig() {
    return cdestadoasig;
  }

  /**
   * @return the estadoentrec
   */
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CDESTADOENTREC")
  public Tmct0estado getEstadoentrec() {
    return estadoentrec;
  }

  /**
   * @return the feejeliq
   */
  @Temporal(TemporalType.DATE)
  @Column(name = "FEEJELIQ", length = 10)
  public Date getFeejeliq() {
    return feejeliq;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @param imajusvb the imajusvb to set
   */
  public void setImajusvb(BigDecimal imajusvb) {
    this.imajusvb = imajusvb;
  }

  /**
   * @param imcomisn the imcomisn to set
   */
  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  /**
   * @param imcanoncontreu the imcanoncontreu to set
   */
  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  /**
   * @param estadocont the estadocont to set
   */
  public void setEstadocont(Tmct0estado estadocont) {
    this.estadocont = estadocont;
  }

  /**
   * @param cdestadoasig the cdestadoasig to set
   */
  public void setCdestadoasig(Integer cdestadoasig) {
    this.cdestadoasig = cdestadoasig;
  }

  /**
   * @param estadoentrec the estadoentrec to set
   */
  public void setEstadoentrec(Tmct0estado estadoentrec) {
    this.estadoentrec = estadoentrec;
  }

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

} // CobrosRoutingDTO
