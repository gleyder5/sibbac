package sibbac.business.wrappers.database.bo.partenonReader;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.wrappers.common.fileReader.SibbacFixedLengthFileReader;
import sibbac.business.wrappers.database.bo.CodigoErrorBo;
import sibbac.business.wrappers.database.bo.Tmct0paisesBo;
import sibbac.business.wrappers.database.bo.Tmct0provinciasBo;
import sibbac.business.wrappers.database.bo.partenonReader.PartenonFieldSetMapper.PartenonFields;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedFileReader;

/**
 * Reader para el fichero de titulares de Partenón.
 * 
 * @author XI316153
 * @see SibbacFixedLengthFileReader
 * @see OperacionEspecialRecordBean
 */
@Service(value = "partenonFileReader")
public class PartenonFileReader extends WrapperMultiThreadedFileReader<OperacionEspecialRecordBean> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(PartenonFileReader.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Numero de operaciones a realizar en cada commit de base de datos. */
  @Value("${sibbac.wrappers.titulares.routing.transaction.size:1}")
  private Integer transactionSize;

  @Value("${sibbac.wrappers.titulares.routing.thread.size:25}")
  private Integer threadSize;

  /** Objeto para el acceso a la tabla <code>TMCT0PAISES</code>. */
  @Autowired
  protected Tmct0paisesBo tmct0paisesBo;

  /** Objeto para el acceso a la tabla <code>TMCT0PROVINCIAS</code>. */
  @Autowired
  protected Tmct0provinciasBo tmct0provinciasBo;

  @Autowired
  protected Tmct0cfgBo tmct0cfgBo;

  @Autowired
  protected CodigoErrorBo codBo;

  @Autowired
  private Tmct0paisesBo paisesBo;

  // Estos sirven para cuando viene un mismo titular partido en dos lineas

  /**
   * Flag para saber de donde recuperar las ops
   * */
  protected boolean operacionEspecial = false;

  /** Lista de días fectivos. */
  protected List<Date> festivosList;

  protected Map<String, Tmct0paises> isosPaises;
  protected Map<String, Tmct0paises> isosPaisesHacienda;
  protected Map<String, List<String>> nombresCiudadPorDistrito;

  /** Mapa de errores. */
  protected Map<String, CodigoErrorData> erroresMap;

  @Qualifier(value = "PartenonTitularesRunnable")
  @Autowired
  protected PartenonTitularesRunnable titularesRunnable;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /** Constructor I. LLama al constructor de la clase padre. */
  public PartenonFileReader() {
    super();
  } // PartenonFileReader

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  @Override
  protected boolean isInBlock(RecordBean beanPrev, RecordBean bean) {
    boolean inBlock = true;
    if (beanPrev != null && bean != null) {
      PartenonRecordBean beanPrevP = (PartenonRecordBean) beanPrev;
      PartenonRecordBean beanP = (PartenonRecordBean) bean;
      inBlock = beanPrevP.getNumOrden().equals(beanP.getNumOrden());
    }

    return inBlock;

  }

  protected Object clone() throws CloneNotSupportedException {

    return this;
  } // clone

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  @Override
  protected LineMapper<OperacionEspecialRecordBean> initLineMapper() {
    return new PatternMatchingCompositeLineMapper<OperacionEspecialRecordBean>();
  }

  /*
   * @see sibbac.business.wrappers.common.fileReader.SibbacFixedLengthFileReader#initializeLineTokenizers()
   */
  @Override
  protected void initLineTokenizers(LineMapper<OperacionEspecialRecordBean> lineMapper) {
    LOG.debug("[PartenonFileReader :: initializeLineTokenizers] Init");
    final Map<String, LineTokenizer> tokenizers = new HashMap<String, LineTokenizer>();
    final FixedLengthTokenizer partenonLineTokenizer = new FixedLengthTokenizer();
    partenonLineTokenizer.setNames(PartenonFields.getFieldNames());
    partenonLineTokenizer.setColumns(convertLengthsIntoRanges(PartenonFields.getPartenonLengths()));
    partenonLineTokenizer.setStrict(false);
    tokenizers.put("20*", partenonLineTokenizer);
    ((PatternMatchingCompositeLineMapper<OperacionEspecialRecordBean>) lineMapper).setTokenizers(tokenizers);
    LOG.debug("[PartenonFileReader :: initializeLineTokenizers] Fin");
    // return tokenizers;
  } // initializeLineTokenizers

  /*
   * @see sibbac.business.wrappers.common.fileReader.SibbacFixedLengthFileReader#initializeFieldSetMappers()
   */
  @Override
  protected void initFieldSetMappers(LineMapper<OperacionEspecialRecordBean> lineMapper) {
    LOG.debug("[PartenonFileReader :: initializeFieldSetMappers] Init");
    final Map<String, FieldSetMapper<OperacionEspecialRecordBean>> fieldSetMappers = new HashMap<String, FieldSetMapper<OperacionEspecialRecordBean>>();
    fieldSetMappers.put("20*", new PartenonFieldSetMapper<PartenonRecordBean>());
    ((PatternMatchingCompositeLineMapper<OperacionEspecialRecordBean>) lineMapper).setFieldSetMappers(fieldSetMappers);
    LOG.debug("[PartenonFileReader :: initializeFieldSetMappers] Fin");
  } // initializeFieldSetMappers

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  @Override
  public IRunnableBean<OperacionEspecialRecordBean> getBeanToProcessBlock() {
    this.titularesRunnable.initDataPreRun(festivosList, erroresMap, isosPaises, isosPaisesHacienda,
                                          nombresCiudadPorDistrito, false);
    return this.titularesRunnable;
  }

  @Override
  public int getThreadSize() {
    int threadSizeLocal = threadSize;
    if (threadSizeLocal > 50) {
      threadSizeLocal = 25;
      LOG.warn("[PartenonFileReader :: getThreadSize] Demasiados hilos configurados para la tarea {} usaremos {}",
               threadSize, threadSizeLocal);
    }
    return threadSizeLocal;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public void preExecuteTask() {
    festivosList = tmct0cfgBo.findHolidaysBolsa();

    erroresMap = new HashMap<String, CodigoErrorData>();
    List<CodigoErrorData> listaErrores = codBo.findAllByGrupoOrderByCodigoDataAsc("Routing Titulares");
    if (listaErrores != null) {
      for (CodigoErrorData error : listaErrores) {
        erroresMap.put(error.getCodigo(), error);
      } // for
    } // if

    isosPaises = paisesBo.findMapAllActivosByIsonnum();

    isosPaisesHacienda = paisesBo.findMapAllActivosByCdhacienda();

    nombresCiudadPorDistrito = tmct0provinciasBo.findMapAllActivas();

  }

  @Override
  public void postExecuteTask() {

  }

} // PartenonFileReader
