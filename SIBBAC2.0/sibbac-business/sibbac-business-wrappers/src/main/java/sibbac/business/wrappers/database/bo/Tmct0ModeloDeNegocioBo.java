package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0ModeloDeNegocioDao;
import sibbac.business.wrappers.database.model.Tmct0IndicadorDeCapacidad;
import sibbac.business.wrappers.database.model.Tmct0ModeloDeNegocio;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0ModeloDeNegocioBo extends AbstractBo< Tmct0ModeloDeNegocio, Long, Tmct0ModeloDeNegocioDao> {

	/** The Constant LOG. */
	private static final Logger	LOG	= LoggerFactory.getLogger( Tmct0ModeloDeNegocioBo.class );
	
	public List< Tmct0ModeloDeNegocio > findAll() {    
		return this.dao.findAll(new Sort(Sort.Direction.ASC,"nbDescripcion"));
	}
	
	public Tmct0ModeloDeNegocio findByNbDescripcion(String nbDescripcion){
		return this.dao.findByNbDescripcion(nbDescripcion);
	}
}
