package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;


/**
 * 
 * @author dortega
 * 
 */
public class SettlementChainBicNameAllocation implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = -2588435285732944890L;

  /**
   * Settlement Clearer Name
   */
  public String sclname = "";

  /**
   * Settlement Clearer Bic
   */
  public String sclbic = "";

  /**
   * Settlement Clearer Local Custodian Name
   */
  public String scllcname = "";

  /**
   * Settlement Clearer Local Custodian Bic
   */
  public String scllcbic = "";

  /**
   * Settlement Clearer Global Custodian Name
   */
  public String sclgcname = "";

  /**
   * Settlement Clearer Global Custodian Bic
   */
  public String sclgcbic = "";

  /**
   * Settlement Clearer Beneficiary Name
   */
  public String sclbename = "";

  /**
   * Settlement Clearer Beneficiary Bic
   */
  public String sclbebic = "";

  /**
   * Settlement Custodian Name
   */
  public String scuname = "";

  /**
   * Settlement Custodian Bic
   */
  public String scubic = "";

  /**
   * Settlement Custodian Local Custodian Name
   */
  public String sculcname = "";

  /**
   * Settlement Custodian Local Custodian Bic
   */
  public String sculcbic = "";

  /**
   * Settlement Custodian Global Custodian Name
   */
  public String scugcname = "";

  /**
   * Settlement Custodian Global Custodian Bic
   */
  public String scugcbic = "";

  /**
   * Settlement Custodian Beneficiary Name
   */
  public String scubename = "";

  /**
   * Settlement Custodian Beneficiary Bic
   */
  public String scubebic = "";

  public SettlementChainBicNameAllocation() {

  }

  /**
   * @return el sclname
   */
  public String getSclname() {
    return sclname;
  }

  /**
   * @return el sclbic
   */
  public String getSclbic() {
    return sclbic;
  }

  /**
   * @return el scllcname
   */
  public String getScllcname() {
    return scllcname;
  }

  /**
   * @return el scllcbic
   */
  public String getScllcbic() {
    return scllcbic;
  }

  /**
   * @return el sclgcname
   */
  public String getSclgcname() {
    return sclgcname;
  }

  /**
   * @return el sclgcbic
   */
  public String getSclgcbic() {
    return sclgcbic;
  }

  /**
   * @return el sclbename
   */
  public String getSclbename() {
    return sclbename;
  }

  /**
   * @return el sclbebic
   */
  public String getSclbebic() {
    return sclbebic;
  }

  /**
   * @return el scuname
   */
  public String getScuname() {
    return scuname;
  }

  /**
   * @return el scubic
   */
  public String getScubic() {
    return scubic;
  }

  /**
   * @return el sculcname
   */
  public String getSculcname() {
    return sculcname;
  }

  /**
   * @return el sculcbic
   */
  public String getSculcbic() {
    return sculcbic;
  }

  /**
   * @return el scugcname
   */
  public String getScugcname() {
    return scugcname;
  }

  /**
   * @return el scugcbic
   */
  public String getScugcbic() {
    return scugcbic;
  }

  /**
   * @return el scubename
   */
  public String getScubename() {
    return scubename;
  }

  /**
   * @return el scubebic
   */
  public String getScubebic() {
    return scubebic;
  }

  /**
   * @param sclname el sclname a establecer
   */
  public void setSclname(String sclname) {
    this.sclname = sclname;
  }

  /**
   * @param sclbic el sclbic a establecer
   */
  public void setSclbic(String sclbic) {
    this.sclbic = sclbic;
  }

  /**
   * @param scllcname el scllcname a establecer
   */
  public void setScllcname(String scllcname) {
    this.scllcname = scllcname;
  }

  /**
   * @param scllcbic el scllcbic a establecer
   */
  public void setScllcbic(String scllcbic) {
    this.scllcbic = scllcbic;
  }

  /**
   * @param sclgcname el sclgcname a establecer
   */
  public void setSclgcname(String sclgcname) {
    this.sclgcname = sclgcname;
  }

  /**
   * @param sclgcbic el sclgcbic a establecer
   */
  public void setSclgcbic(String sclgcbic) {
    this.sclgcbic = sclgcbic;
  }

  /**
   * @param sclbename el sclbename a establecer
   */
  public void setSclbename(String sclbename) {
    this.sclbename = sclbename;
  }

  /**
   * @param sclbebic el sclbebic a establecer
   */
  public void setSclbebic(String sclbebic) {
    this.sclbebic = sclbebic;
  }

  /**
   * @param scuname el scuname a establecer
   */
  public void setScuname(String scuname) {
    this.scuname = scuname;
  }

  /**
   * @param scubic el scubic a establecer
   */
  public void setScubic(String scubic) {
    this.scubic = scubic;
  }

  /**
   * @param sculcname el sculcname a establecer
   */
  public void setSculcname(String sculcname) {
    this.sculcname = sculcname;
  }

  /**
   * @param sculcbic el sculcbic a establecer
   */
  public void setSculcbic(String sculcbic) {
    this.sculcbic = sculcbic;
  }

  /**
   * @param scugcname el scugcname a establecer
   */
  public void setScugcname(String scugcname) {
    this.scugcname = scugcname;
  }

  /**
   * @param scugcbic el scugcbic a establecer
   */
  public void setScugcbic(String scugcbic) {
    this.scugcbic = scugcbic;
  }

  /**
   * @param scubename el scubename a establecer
   */
  public void setScubename(String scubename) {
    this.scubename = scubename;
  }

  /**
   * @param scubebic el scubebic a establecer
   */
  public void setScubebic(String scubebic) {
    this.scubebic = scubebic;
  }

}
