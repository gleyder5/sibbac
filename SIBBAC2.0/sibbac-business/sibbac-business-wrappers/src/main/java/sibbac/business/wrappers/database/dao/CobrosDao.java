package sibbac.business.wrappers.database.dao;


import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Cobros;


@Component
public interface CobrosDao extends JpaRepository< Cobros, Long >, JpaSpecificationExecutor< Cobros > {

	@Query( "Select cob from Cobros cob " + "where cob.fecha = :fechaAyer " )
	public List< Cobros > findCobrosYesterday( @Param( "fechaAyer" ) Date fechaAyer );

	public List< Cobros > findByFechaEnvioMailNull();

	public List< Cobros > findByContabilizado( boolean contabilizado );

}
