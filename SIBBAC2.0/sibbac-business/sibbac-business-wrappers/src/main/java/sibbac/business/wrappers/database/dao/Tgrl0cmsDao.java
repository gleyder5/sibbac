package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cms;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cmsId;

@Repository
public interface Tgrl0cmsDao extends JpaRepository<Tgrl0cms, Tgrl0cmsId> {

  @Query("select max(c.id.nuregcom) from Tgrl0cms c where c.id.nuoprout = :pnuoprout and c.id.nupareje = :pnupareje")
  Short findMaxNuregcomByNuoproutAndNupareje(@Param("pnuoprout") Integer nuoprout, @Param("pnupareje") short nupareje);

  @Query("select sum(c.imregcom), sum(c.imregdiv) from Tgrl0cms c where c.id.nuoprout = :pnuoprout and c.id.nupareje = :pnupareje"
      + " and c.cdsvbbrk = :pcdsvbbrk and c.tpregcom = :ptpregcom")
  List<Object[]> findImregcomImregdivByNuoproutAndNuparejeAndCdsvbbrkAndTpregcom(@Param("pnuoprout") Integer nuoprout,
      @Param("pnupareje") short nupareje, @Param("pcdsvbbrk") Character cdsvbbrk, @Param("ptpregcom") Character tpregcom);

  @Query("select  c.cdcleare, c.cdbroker, sum(c.imregcom), sum(c.imregdiv) from Tgrl0cms c where c.id.nuoprout = :pnuoprout and c.id.nupareje = :pnupareje"
      + " and c.cdsvbbrk = :pcdsvbbrk and c.tpregcom = :ptpregcom" + " group by c.cdcleare, c.cdbroker ")
  List<Object[]> findImregcomImregdivByNuoproutAndNuparejeAndCdsvbbrkAndTpregcomGroupByCdcleareCdbroker(
      @Param("pnuoprout") Integer nuoprout, @Param("pnupareje") short nupareje, @Param("pcdsvbbrk") Character cdsvbbrk,
      @Param("ptpregcom") Character tpregcom);

}
