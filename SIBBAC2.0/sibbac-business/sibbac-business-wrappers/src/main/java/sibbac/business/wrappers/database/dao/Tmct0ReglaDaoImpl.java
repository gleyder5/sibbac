package sibbac.business.wrappers.database.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.common.DTOUtils;
import sibbac.business.wrappers.database.dto.GestionReglasDTO;
import sibbac.business.wrappers.database.model.Tmct0parametrizacion;


@Repository
public class Tmct0ReglaDaoImpl {
	
	@PersistenceContext
	private EntityManager		em;
	private static final String	TAG	= MovimientoCuentaAliasDAOImpl.class.getName();
	private static final Logger	LOG	= LoggerFactory.getLogger( TAG );
	


	@SuppressWarnings("unchecked")
    public List< Tmct0parametrizacion > findAllDTO1( Long idRegla, Long camaraCompensacion, Long cuentaCompensacion, Long miembroCompensador, String referenciaAsignacion,
			Long nemotecnico, Long segmento, String referenciaCliente, String referenciaExterna, Long capacidad, Long modeloNegocio) {
		
		final List< Tmct0parametrizacion > listaParametri = new ArrayList< Tmct0parametrizacion >();
		
		boolean whereEmpty = true;
		
		try{
			final StringBuilder select = new StringBuilder("");
			final StringBuilder from   = new StringBuilder("");
			final StringBuilder where = new StringBuilder("");

			select.append("SELECT DISTINCT ID_REGLA "); 
			from.append("FROM TMCT0_PARAMETRIZACION "); 
				if ( idRegla != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append( "ID_REGLA = '"+idRegla+"' " );
				}
				
				
	
				if ( camaraCompensacion != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_CAMARA = '"+camaraCompensacion+"' ");
				}
				
				
				if ( cuentaCompensacion != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_CUENTA_COMPENSACION = '"+cuentaCompensacion+"'");
				}
				
				
				if ( miembroCompensador != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_COMPENSADOR = '"+miembroCompensador+"'");
				}
				
				if ( referenciaAsignacion != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("CD_REFERENCIA_ASIGNACION = '"+referenciaAsignacion+"'");
				}

				if ( nemotecnico != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_NEMOTECNICO = '"+nemotecnico+"'");
				}

				if ( segmento != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_SEGMENTO = '"+segmento+"'");
				}

				if ( referenciaCliente != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("REFERENCIA_CLIENTE = '"+referenciaCliente+"'");
				}
				
				if ( referenciaExterna != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("REFERENCIA_EXTERNA = '"+referenciaExterna+"'");
				}
				
				if ( capacidad != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_INDICADOR_CAPACIDAD = '"+capacidad+"'");
				}
				

				if ( modeloNegocio != null ) {
					if ( !whereEmpty ) {
						where.append( " and " );
					} else {
						where.append( " where " );
						whereEmpty = false;
					}
					where.append("ID_MODELO_DE_NEGOCIO = '"+modeloNegocio+"'");
				}

				Query query;
				query = em.createNativeQuery( select.append( from.append( where ) ).toString() );
				List< Object > resultList;
  			    resultList = query.getResultList();
				 
				 
				 if ( CollectionUtils.isNotEmpty( resultList ) ) {
						for ( Object obj : resultList ) {
							Tmct0parametrizacion alc = DTOUtils.convertObjectParametrizacionTransaccionesDTO( ( Object ) obj );
			
							listaParametri.add( alc );
						}
					}	

		}
		catch(RuntimeException ex){
			LOG.error("[findAllDTO1] Error inesperado", ex);
		}
		return listaParametri;
	}
	
	
    @SuppressWarnings("unchecked")
	public List<GestionReglasDTO> findReglasFiltro(String cdCodigo, Long camaraCompensacion, Long cuentaCompensacion, 
			Long miembroCompensador, String referenciaAsignacion, Long nemotecnico, Long segmento, String referenciaCliente, 
			String referenciaExterna, Long capacidad, Long modeloNegocio, String estado, String alias, String subCta){
		
		StringBuilder select = new StringBuilder();
		StringBuilder from   = new StringBuilder();
		StringBuilder where  = new StringBuilder();
		boolean whereEmpty = true;
		
		select.append("select regla.id_regla, regla.cd_codigo, regla.nb_descripcion, regla.fecha_creacion, regla.activo, "
					+ "count( parame.id_regla ) parametrizaciones,  (select count(*) from tmct0ali alic where regla.ID_REGLA = alic.ID_REGLA ) alias, "
					+ "(select count(*) from tmct0act actc where regla.ID_REGLA = actc.ID_REGLA ) subcuentas ");
		from.append("from tmct0_regla regla ");
		
		from.append("left join tmct0_parametrizacion parame on regla.id_regla = parame.id_regla and parame.activo = '1'");

		if ( cdCodigo != null ) {
			where.append( " where " );
			whereEmpty = false;
			where.append( "regla.cd_codigo =:cdCodigo" );
		}
		
		if ( estado != null && !estado.equals("2")) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "regla.activo = :estado" );
		}else if(estado.equals("2")){
			estado = null;
		}
		

		if ( camaraCompensacion != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_camara =:camaraCompensacion" );
		}	
		
		if ( cuentaCompensacion != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_cuenta_compensacion =:cuentaCompensacion" );
		}
		
		if ( miembroCompensador != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_compensador =:miembroCompensador" );
		}
		
		if ( referenciaAsignacion != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.cd_referencia_asignacion =:referenciaAsignacion" );
		}
		
		if ( nemotecnico != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_nemotecnico =:nemotecnico" );
		}		
		
		if ( segmento != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_segmento =:segmento" );
		}		
		
		if ( referenciaCliente != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.referencia_cliente =:referenciaCliente" );
		}		
		
		if ( referenciaExterna != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.referencia_externa =:referenciaExterna" );
		}	
		
		if ( capacidad != null ) {  // OJOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_indicador_capacidad=:capacidad" );
		}			

		if ( modeloNegocio != null ) {
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "parame.id_modelo_de_negocio=:modeloNegocio" );
			
		}	
		
		
		if ( alias != null ) {
			from.append(" left join tmct0ali ali on  regla.id_regla = ali.id_regla");
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "ali.cdaliass = :alias" );
		}
		
		if ( subCta != null ) {
			from.append(" left join tmct0act act on  regla.id_regla = act.id_regla");
			if ( !whereEmpty ) {
				where.append( " and " );
			} else {
				where.append( " where " );
				whereEmpty = false;
			}
			where.append( "act.cdsubcta = :subCta" );
		}
		
		where.append(" group by regla.id_regla,regla.cd_codigo, regla.nb_descripcion,regla.fecha_creacion, regla.activo");
		final List< GestionReglasDTO > reglasList = new ArrayList< GestionReglasDTO >();
		try{
		
			final Query query = em.createNativeQuery( select.append( from.append( where ) ).toString() );
	
			setParameters( query, cdCodigo, camaraCompensacion, cuentaCompensacion, miembroCompensador, referenciaAsignacion, 
					nemotecnico, segmento, referenciaCliente, referenciaExterna, capacidad, modeloNegocio, estado, alias, subCta,
					"cdCodigo", "camaraCompensacion", "cuentaCompensacion", "miembroCompensador", "referenciaAsignacion", 
					"nemotecnico", "segmento", "referenciaCliente", "referenciaExterna", "capacidad", "modeloNegocio", "estado", "alias", "subCta");
			
            final List< Object > resultList = query.getResultList();
			if ( CollectionUtils.isNotEmpty( resultList ) ) {
				for ( Object obj : resultList ) {
					GestionReglasDTO regla = DTOUtils.convertObjectGestionReglassDTO( ( Object[] ) obj );
					reglasList.add(regla);
				}
			}
		}
		catch(RuntimeException ex){
			LOG.error("[findReglasFiltro] Error inesperado", ex);
		}
		return reglasList;

	}
	
	private void setParameters( Query query, String cdCodigo, Long camaraCompensacion, Long cuentaCompensacion, 
			Long miembroCompensador, String referenciaAsignacion, Long nemotecnico, Long segmento, String referenciaCliente, 
			String referenciaExterna, Long capacidad, Long modeloNegocio, String estado, String alias, String subCta,
			String cdCodigoParam, String camaraCompensacionParam, 
			String cuentaCompensacionParam, String miembroCompensadorParam, String referenciaAsignacionParam, 
			String nemotecnicoParam, String segmentoParam, String referenciaClienteParam,String referenciaExternaParam, 
			String capacidadParam, String modeloNegocioParam, String estadoParam, String aliasParam, String subCtaParam	){
		
		
		if ( cdCodigo != null ) {
			query.setParameter( cdCodigoParam, cdCodigo );
		}
		
		if ( camaraCompensacion != null ) {
			query.setParameter( camaraCompensacionParam, camaraCompensacion );
		}		
		
		if ( cuentaCompensacion != null ) {
			query.setParameter( cuentaCompensacionParam, cuentaCompensacion );
		}
		
		if ( miembroCompensador != null ) {
			query.setParameter( miembroCompensadorParam, miembroCompensador );
		}		
		
		if ( referenciaAsignacion != null ) {
			query.setParameter( referenciaAsignacionParam, referenciaAsignacion );
		}
		
		if ( nemotecnico != null ) {
			query.setParameter( nemotecnicoParam, nemotecnico );
		}		
		
		if ( segmento != null ) {
			query.setParameter( segmentoParam, segmento );
		}
		
		if ( referenciaCliente != null ) {
			query.setParameter( referenciaClienteParam, referenciaCliente );
		}		

		if ( referenciaExterna != null ) {
			query.setParameter( referenciaExternaParam, referenciaExterna );
		}
		
		if ( capacidad != null ) {
			query.setParameter( capacidadParam, capacidad );
		}		
		
		if ( modeloNegocio != null ) {
			query.setParameter( modeloNegocioParam, modeloNegocio );
		}
		
		if ( estado != null ) {
			query.setParameter( estadoParam, estado );
		}	
		
		if ( alias != null ) {
			query.setParameter( aliasParam, alias );
		}
		
		if ( subCta != null ) {
			query.setParameter( subCtaParam, subCta );
		}
		
	}
	

}
