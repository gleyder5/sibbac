package sibbac.business.wrappers.database.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class Tmct0movimientoeccDaoImp {

  @PersistenceContext
  private EntityManager em;

  private static final String QUERY_MISMOID_DATOS_DIFERENTES = "SELECT IDDESGLOSECAMARA,MIEMBRO_COMPENSADOR_DESTINO,CD_REF_ASIGNACION,CUENTA_COMPENSACION_DESTINO "
                                                               + "FROM bsnbpsql.tmct0movimientoecc WHERE cdestadomov = 9 "
                                                               + "GROUP BY IDDESGLOSECAMARA,MIEMBRO_COMPENSADOR_DESTINO,CD_REF_ASIGNACION,CUENTA_COMPENSACION_DESTINO "
                                                               + "HAVING COUNT(DISTINCT iddesglosecamara) > 1 ";

  private static final String QUERY_ID_NO_REPETIDOS = "select IDDESGLOSECAMARA, MIEMBRO_COMPENSADOR_DESTINO, CD_REF_ASIGNACION, CUENTA_COMPENSACION_DESTINO from bsnbpsql.tmct0movimientoecc where cdestadomov=9 and iddesglosecamara in "
                                                      + "(select dcm.iddesglosecamara from bsnbpsql.tmct0desglosecamara dcm inner join bsnbpsql.TMCT0DESGLOSECLITIT dct on dct.iddesglosecamara=dcm.iddesglosecamara and dct.MIEMBRO_COMPENSADOR_DESTINO IS NULL AND dct.CD_REF_ASIGNACION IS NULL AND dct.CUENTA_COMPENSACION_DESTINO IS NULL "
                                                      + "where dcm.cdestadoasig>=65 group by dcm.IDDESGLOSECAMARA) and ((MIEMBRO_COMPENSADOR_DESTINO is not null and CD_REF_ASIGNACION is not null) or CUENTA_COMPENSACION_DESTINO is not null) "
                                                      + "group by IDDESGLOSECAMARA, MIEMBRO_COMPENSADOR_DESTINO, CD_REF_ASIGNACION, CUENTA_COMPENSACION_DESTINO having count(distinct iddesglosecamara)=1 ";

  public List<Object[]> findDistinctIdDesglose() {
    Query query = em.createNativeQuery(QUERY_ID_NO_REPETIDOS);

    return query.getResultList();

  }

  public List<Object[]> findMismoIdDesgloseDistintosValoresError() {
    Query query = em.createNativeQuery(QUERY_MISMOID_DATOS_DIFERENTES);

    return query.getResultList();

  }

}
