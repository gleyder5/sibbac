package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cliHistorical;

@Repository
public class Tmct0cliHistoricalDaoImp {
	@PersistenceContext
	private EntityManager	em;
	
	/**
	 * 
	 * @param idClient
	 * @param fechaDesde
	 * @param tipoDato
	 * @return List<Tmct0cliHistorical>
	 */
	public List<Tmct0cliHistorical> findHistoricoClientesByFiltro(
		Long idClient, Date fechaDesde, String tipoDato) {
		
		Query query;
		List< Tmct0cliHistorical > result;
		StringBuffer qlString = new StringBuffer( "SELECT " );
		qlString.append( " historico " );
		qlString.append( " FROM Tmct0cliHistorical historico " );
		qlString.append( " WHERE 1=1 " );
		
		if (idClient != null) {
			qlString.append( "AND historico.idClient = :idClient " );
		}
		
		if (fechaDesde != null) {
			qlString.append( "AND historico.fhmod >= :fechaDesde " );
		}
		
		if (tipoDato != null) {
			qlString.append( "AND historico.tpdata = :tipoDato " );
		}
		
		try {
			query = em.createQuery( qlString.toString() );
			if (idClient != null) {
				query.setParameter( "idClient", idClient );
			}
			if (fechaDesde != null) {
				query.setParameter( "fechaDesde", fechaDesde );
			}
			if (tipoDato != null) {
				query.setParameter( "tipoDato", tipoDato );
			}
			
			result = query.getResultList();
		} catch ( PersistenceException ex ) {
			result = null;
		}
		return result;
	}
	
}
