package sibbac.business.wrappers.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.quartz.DateBuilder;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.fichero1015.EnvioFichero1015;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * @author XI316153
 *
 */
@SIBBACJob(group = Task.GROUP_WRAPPERS.NAME,
           name = Task.GROUP_WRAPPERS.JOB_OPERACIONES_ESPECIALES_2040_GENERAR_FICHERO_1015,
           interval = 5,
           delay = 1,
           intervalUnit = DateBuilder.IntervalUnit.MINUTE)
@DisallowConcurrentExecution
public class TaskOperacionesEspecialesGenerarFichero1015 extends WrapperTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskOperacionesEspecialesGenerarFichero1015.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${sibbac.wrappers.operacion.especial.1015.codigos.comerciales}")
  private String codigosComerciales;

  @Autowired
  private EnvioFichero1015 envioFichero1015;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.business.fallidos.tasks.FallidosTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_WRITE_1015_FILES;
  } // determinarTipoApunte

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void executeTask() throws Exception {
    final Long startTime = System.currentTimeMillis();
    LOG.debug("[executeTask] Iniciando la tarea de creacion de ficheros con registros 10 y 15 ...");
    List<String> codigos = new ArrayList<String>();
    if (StringUtils.isNotEmpty(codigosComerciales)) {
      String[] codigosArr = codigosComerciales.split(",");
      if (codigosArr != null && codigosArr.length > 0) {
        codigos.addAll(Arrays.asList(codigosArr));
      } // if
    } // if
    
    // try {
    // LOG.info("[TaskOperacionesEspecialesGenerarFichero1015 :: executeTask] pasando operaciones que no envian 10 15 a ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR");
    // int countPdteTitular =
    // desgloseRecordBeanBo.updateProcesadoPdteCargarTitularDesglosesRecordBeanOrdenComercialNoEnvian1015(codigos);
    // LOG.info("[TaskOperacionesEspecialesGenerarFichero1015 :: executeTask] se han pasado {} desgloses a  ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR",
    // countPdteTitular);
    // } catch (Exception e) {
    // LOG.warn("[executeTask] Incidencia ocurrida durante la actualizacion de ops sin 10 15 '" + e.getMessage()
    // + "' ...", e);
    // }

    try {

      envioFichero1015.executeTask();

    } catch (Exception ex) {
      LOG.warn("[executeTask] Incidencia ocurrida durante la creacion de ficheros con registros 10 y 15 '"
                   + ex.getMessage() + "' ...", ex);
      throw ex;
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[executeTask] Finalizada la tarea de creacion de ficheros con registros 10 y 15 ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
               + "]");
    } // finally

    try {

      envioFichero1015.appendOutFiles();

    } catch (Exception ex) {
      LOG.warn("[executeTask] Incidencia ocurrida durante la append de ficheros con registros 10 y 15 '"
                   + ex.getMessage() + "' ...", ex);
      throw ex;
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[executeTask] Finalizada la tarea de append de ficheros con registros 10 y 15 ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
               + "]");
    } // finally
  } // executeTask

} // TaskOperacionesEspecialesGenerarFichero1015
