package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.RecordatorioFacturaPendienteDao;
import sibbac.business.wrappers.database.dao.TextoDao;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.AliasRecordatorio;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.Idioma;
import sibbac.business.wrappers.database.model.RecordatorioFacturaPendiente;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.business.wrappers.database.model.TextosIdiomas;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.JobPK;

/**
 * @author pbellon
 */
@Service
public class RecordatorioFacturaPendienteBo
		extends AbstractBo<RecordatorioFacturaPendiente, Long, RecordatorioFacturaPendienteDao> {

	private static final String APP_NAME = "SBRMV";
	private static final String PROC_NAME = "FACTURA";
	private static final String KEY_NAME = "periodo.recordatorio";

	@Autowired
	private FacturaBo facturaBo;
	@Autowired
	private TextoBo textoBo;
	@Autowired
	private TextoDao textoDao;
	@Autowired
	private SendMail sendMail;
	@Autowired
	private FacturaDao facturaDao;
	@Autowired
	private Tmct0estadoBo estadoBo;
	@Autowired
	private AliasRecordatoriosBo aliasRecordatoriosBo;
	@Autowired
	private TextosIdiomasBo textosIdiomasBo;
	@Autowired
	private Tmct0cfgBo tmct0cfgBo;
	@Autowired
	private AliasBo aliasBo;
	@Autowired
	private EtiquetasBo etiquetasBo;
	@Autowired
	private GestorServicios gestorServicios;

	protected static final Logger LOG = LoggerFactory.getLogger(RecordatorioFacturaPendienteBo.class);

	@Transactional
	public List<RecordatorioFacturaPendiente> generarRecordatoriosDeFacturas(final List<String> taskMessages) {
		final String prefix = "[RecordatorioFacturaPendienteBo::generarRecordatoriosDeFacturas] ";
		List<RecordatorioFacturaPendiente> recordatoriosFacturaPendiente = new ArrayList<RecordatorioFacturaPendiente>();

		// Llamamos al metodo que obtiene las facturas que necesitan mandar un
		// recordatorio.
		LOG.trace(prefix + "Localizando las facturas que tienen pendiente un recordatorio...");
		final Integer diasIntervaloInicial = calcularIntervaloInicialRecordatorio(prefix, taskMessages);
		if (diasIntervaloInicial == null) {
			taskMessages.add("Se para el proceso por problemas de configuracion");
		} else {
			final List<Factura> facturasPendientes = recuperarFacturasPendientesCobro();
			final Tmct0estado estadoPendiente = estadoBo
					.findByIdFacturaPendienteRecordatorio(EstadosEnumerados.FACTURA_PENDIENTE_RECORDATORIO.PENDIENTE);
			final Date fechaActual = new Date();
			if (CollectionUtils.isNotEmpty(facturasPendientes)) {
				for (final Factura factura : facturasPendientes) {
					final Long idAlias = factura.getIdAlias();
					taskMessages.add("Comprobando factura pendiente de cobro num. " + factura.getNumeroFactura()
							+ "del alias " + factura.getCdaliass());
					final AliasRecordatorio aliasRecordatorio = aliasRecordatoriosBo.findByIdAlias(idAlias);
					BigDecimal importeMinimo = BigDecimal.ONE;
					if (aliasRecordatorio != null) {
						importeMinimo = aliasRecordatorio.getImporteMinimo();
					}
					RecordatorioFacturaPendiente recordatorioFacturaPendiente = dao.findByFactura_Id(factura.getId());
					final BigDecimal importePendiente = factura.getImportePendiente();
					if (importePendiente.compareTo(importeMinimo) >= 0) {
						if (recordatorioFacturaPendiente != null) {
							final Tmct0estado estado = recordatorioFacturaPendiente.getEstado();
							if (estado.getIdestado()
									.equals(EstadosEnumerados.FACTURA_PENDIENTE_RECORDATORIO.ENVIADO.getId())) {
								taskMessages.add("Actualizando recordatorio");
                // Calculamos la fecha desde el último enviado, sumando la frecuencia del recordatorio
                // configurado
								final Date fechaRecordatorio = añadirDias(
										recordatorioFacturaPendiente.getFechaRecordatorio(), fechaActual,
										aliasRecordatorio.getFrecuencia());
								recordatorioFacturaPendiente.setFechaRecordatorio(fechaRecordatorio);
								recordatorioFacturaPendiente.setEstado(estadoPendiente);
								recordatoriosFacturaPendiente.add(recordatorioFacturaPendiente);
							}
						} else {
              // Si no hay recordatorio, usamos la configuración el primer recordatorio para mandar el correo
							taskMessages.add("Creando recordatorio");
							recordatorioFacturaPendiente = new RecordatorioFacturaPendiente();
							final Date fechaRecordatorio = añadirDias(factura.getFechaFactura(), fechaActual,
									diasIntervaloInicial);
							recordatorioFacturaPendiente.setEstado(estadoPendiente);
							recordatorioFacturaPendiente.setFactura(factura);
							recordatorioFacturaPendiente.setFechaRecordatorio(fechaRecordatorio);
							recordatoriosFacturaPendiente.add(recordatorioFacturaPendiente);
						}

					} else {
						if (recordatorioFacturaPendiente != null) {
							taskMessages.add("Borrando recordatorio");
							dao.delete(recordatorioFacturaPendiente);
						}
					}
				}
				if (CollectionUtils.isNotEmpty(recordatoriosFacturaPendiente)) {
					recordatoriosFacturaPendiente = dao.save(recordatoriosFacturaPendiente);
				}
			}

		}
		return recordatoriosFacturaPendiente;
	}

	/**
	 * @param dias
	 * @param fecha
	 * @return
	 */
	private Date añadirDias(Date fecha, final Date fechaActual, final Integer dias) {
		DateTime dateTime = new DateTime(fecha.getTime());
		dateTime = dateTime.plusDays(dias);
		fecha = new Date(dateTime.getMillis());
		if (fecha.before(fechaActual)) {
			fecha = fechaActual;
		}

		return fecha;
	}

	/**
	 * Metodo que obtiene las facturas con estado pendiente de enviar y las
	 * envia por email.
	 * 
	 * @param jobPk
	 */
	@Transactional
	public void enviarMailRecordatorio(final JobPK jobPk, final List<String> taskMessages) throws Exception {
		final String prefix = "[RecordatorioFacturaPendienteBo::enviarMailRecordatorio] ";
		final Date fechaActual = new Date();
		final List<RecordatorioFacturaPendiente> recordatoriosFacturaPendiente = dao
				.findAllByEstado_IdestadoAndFechaRecordatorioBefore(
						EstadosEnumerados.FACTURA_PENDIENTE_RECORDATORIO.PENDIENTE.getId(), fechaActual);
		if (CollectionUtils.isNotEmpty(recordatoriosFacturaPendiente)) {
			taskMessages
					.add("Se han recuperado [" + recordatoriosFacturaPendiente.size() + "] recordatorios para enviar");
			final Tmct0estado estadoEnvidado = estadoBo
					.findByIdFacturaPendienteRecordatorio(EstadosEnumerados.FACTURA_PENDIENTE_RECORDATORIO.ENVIADO);
			for (RecordatorioFacturaPendiente recordatorioFacturaPendiente : recordatoriosFacturaPendiente) {
				final Factura factura = recordatorioFacturaPendiente.getFactura();
				taskMessages.add("Recordatorio para la factura [ " + factura.getNumeroFactura() + "]");
				final Tmct0estado estadoFactura = factura.getEstado();
				if (estadoFactura.getIdestado().equals(EstadosEnumerados.FACTURA.PTE_COBRO.getId())
						|| estadoFactura.getIdestado().equals(EstadosEnumerados.FACTURA.COBRADO_PARCIALMENTE.getId())) {
					final Alias alias = factura.getAlias();
					final Map<String, String> mapping = this.facturaBo.createMappingCuerpoMail(factura);
					final Idioma idioma = alias.getIdioma();
					final String subject = this.facturaBo
							.reemplazarKeys(textosIdiomasBo.recuperarTextoIdioma("K", idioma), mapping);
					LOG.trace(prefix + "   Subject: [{}]...", subject);
					final String body = this.facturaBo.reemplazarKeys(textosIdiomasBo.recuperarTextoIdioma("L", idioma),
							mapping);
					LOG.trace(prefix + "      Body: [{}]...", body);

					final List<String> destinatarios = Contacto
							.getCorreosContactos(gestorServicios.getContactos(alias, jobPk));
					final List<String> gestores = ListUtils
							.subtract(Contacto.getCorreosContactos(factura.getContactosGestores()), destinatarios);
					sendMail.sendMail(destinatarios, subject, body, gestores);
					taskMessages.add("Correo enviado");
				} else {
					taskMessages.add("La factura ya ha sido pagada. No se envia el recordatorio");
				}
				recordatorioFacturaPendiente.setEstado(estadoEnvidado);
				recordatorioFacturaPendiente.setFechaRecordatorio(fechaActual);
				this.save(recordatorioFacturaPendiente);
			}
		}
	}

	public TextosIdiomas getTextosidiomas(Long idAlias, Texto texto) {
		String prefix = "[RecordatorioFacturaPendienteBo::getTextosidiomas] ";
		TextosIdiomas textosIdiomas = null;
		LOG.trace(prefix + "Buscando el alias: [{}]", idAlias);
		Alias alias = aliasBo.findById(idAlias);
		if (alias != null) {
			LOG.trace(prefix + "Buscando el idioma del alias: [{}]", idAlias);
			Idioma idioma = alias.getIdioma();
			if (idioma != null) {
				LOG.trace(prefix + "Buscando los textos para el idioma [{}] del alias: [{}]", idioma, idAlias);
				textosIdiomas = textosIdiomasBo.findByIdiomaAndTexto(idioma, texto);
			} else {
				LOG.warn(prefix + "Error localizando el idioma del alias: [{}]", idAlias);
			}
		} else {
			LOG.warn(prefix + "Error localizando el alias: [{}]", idAlias);
		}
		return textosIdiomas;
	}

	/**
	 * @param prefix
	 * @throws SIBBACBusinessException
	 */
	private Integer calcularIntervaloInicialRecordatorio(final String prefix, final List<String> taskMessages) {
		Integer diferencia = null;
		LOG.trace(prefix + "Buscando el valor de configuracion (dias): [{}::{}::{}].", APP_NAME, PROC_NAME, KEY_NAME);
		final Tmct0cfg tmct0cfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(APP_NAME, PROC_NAME, KEY_NAME);
		if (tmct0cfg != null) {
			try {
				diferencia = FormatDataUtils.convertStringToInteger(tmct0cfg.getKeyvalue());
				LOG.trace(prefix + "[Numero de días de la configuacion inicial de los correos {}].", diferencia);
			} catch (SIBBACBusinessException e) {
				taskMessages.add("La configuracion inicial no es correcta");
				LOG.error(prefix + "[La configuracion inicial no es correcta.]");
			}
		} else {
			taskMessages.add("No existe configuracion inicial para el envio del correo");
			LOG.error(prefix + "[No existe configuracion inicial para el envio del correo.]");

		}
		return diferencia;
	}

	/**
	 * @return
	 */
	private List<Factura> recuperarFacturasPendientesCobro() {
		final List<Tmct0estado> estadosPteCobro = new ArrayList<Tmct0estado>();
		estadosPteCobro.add(estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.PTE_COBRO));
		estadosPteCobro.add(estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.COBRADO_PARCIALMENTE));
		final List<Factura> facturasPendientes = facturaDao.findAllByEstadoIn(estadosPteCobro);
		return facturasPendientes;
	}

}
