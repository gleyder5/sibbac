package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0tfi;
import sibbac.business.wrappers.database.model.Tmct0tfiId;

@Repository
public interface Tmct0tfiDao extends JpaRepository<Tmct0tfi, Tmct0tfiId> {

  @Query("SELECT o FROM Tmct0tfi o WHERE o.id.cddclave =:cddclave AND o.fhfinal >= :fecha")
  List<Tmct0tfi> getFisByCddclaveAndFhfinal(@Param("cddclave") String cddclave, @Param("fecha") BigDecimal fecha);

  @Query("SELECT o FROM Tmct0tfi o WHERE o.id.cdaliass = :pcdaliass AND o.id.cdsubcta = :pcdsubcta "
      + " AND o.id.centro = :pcentro AND o.id.cdmercad = :pcdmercad "
      + " AND o.fhinicio <= :pfetrade AND o.fhfinal >= :pfetrade "
      + " ORDER BY o.id.cdaliass ASC, o.id.cdsubcta ASC, o.id.centro ASC, "
      + " o.id.cdmercad ASC, o.tipotit ASC, o.id.numsec ASC ")
  List<Tmct0tfi> findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(@Param("pcdaliass") String cdaliass,
      @Param("pcdsubcta") String cdsubcta, @Param("pcentro") String centro, @Param("pcdmercad") String cdmercad,
      @Param("pfetrade") BigDecimal fecha);  
  
  
  @Query("SELECT o FROM Tmct0tfi o WHERE o.id.cdaliass = :pcdaliass AND o.id.cdsubcta = :pcdsubcta "
      + " AND o.id.centro = :pcentro AND o.id.cdmercad = :pcdmercad "
      + " AND o.fhinicio <= :pfetrade AND o.fhfinal >= :pfetrade and o.tptiprep <> 'R' "
      + " ORDER BY o.id.cdaliass ASC, o.id.cdsubcta ASC, o.id.centro ASC, "
      + " o.id.cdmercad ASC, o.tipotit ASC, o.id.numsec ASC ")
  List<Tmct0tfi> findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetradeNoRepresentantes(@Param("pcdaliass") String cdaliass,
      @Param("pcdsubcta") String cdsubcta, @Param("pcentro") String centro, @Param("pcdmercad") String cdmercad,
      @Param("pfetrade") BigDecimal fecha);


  @Query(nativeQuery = true, value = "SELECT F.CDDCLAVE, F.TPDOMICI, F.NBDOMICI, F.NUDOMICI, "
      + " F.NBCIUDAD, F.NBPROVIN, F.CDPOSTAL, F.CDDEPAIS, F.NUDNICIF, F.NBCLIENT, F.NBCLIEN1, F.NBCLIEN2, "
      + " F.TPTIPREP, F.CDNACTIT, F.FHMODIFI, F.HRMODIFI, F.CDUSUARI, F.CDORDTIT, F.RFTITAUD, "
      + " F.CDDDEBIC, F.TPIDENTI, F.TPSOCIED, F.TPNACTIT, F.CDUSUAUD, F.FHAUDIT, "
      + " F.FHINICIO, F.FHFINAL, F.NUMSEC, F.CDHOLDER, F.CATEGORY, F.CDREFBAN, "
      + " F.FECHA_NACIMIENTO FROM TMCT0FIS F WHERE F.CDHOLDER = :cdholder "
      + " AND F.FHFINAL >= :fecha AND F.FHINICIO <= :fecha " + " ORDER BY F.NUMSEC DESC ")
  List<Object[]> findAllHolderDataTmct0fisByCdholderAndFetrade(@Param("cdholder") String cdholder,
      @Param("fecha") BigDecimal fecha);
  
  @Query(nativeQuery = true, value = "SELECT F.CDDCLAVE, F.TPDOMICI, F.NBDOMICI, F.NUDOMICI, "
      + " F.NBCIUDAD, F.NBPROVIN, F.CDPOSTAL, F.CDDEPAIS, F.NUDNICIF, F.NBCLIENT, F.NBCLIEN1, F.NBCLIEN2, "
      + " F.TPTIPREP, F.CDNACTIT, F.FHMODIFI, F.HRMODIFI, F.CDUSUARI, F.CDORDTIT, F.RFTITAUD, "
      + " F.CDDDEBIC, F.TPIDENTI, F.TPSOCIED, F.TPNACTIT, F.CDUSUAUD, F.FHAUDIT, "
      + " F.FHINICIO, F.FHFINAL, F.NUMSEC, F.CDHOLDER, F.CATEGORY, F.CDREFBAN, "
      + " F.FECHA_NACIMIENTO FROM TMCT0FIS F WHERE F.CDDCLAVE = :cddclave "
      + " AND F.FHFINAL >= :fecha AND F.FHINICIO <= :fecha " + " ORDER BY F.NUMSEC DESC ")
  List<Object[]> findAllHolderDataTmct0fisByCddclaveAndFetrade(@Param("cddclave") String cddclave,
      @Param("fecha") BigDecimal fecha);

  @Query(nativeQuery = true, value = "SELECT F.CDDCLAVE, F.TPDOMICI, F.NBDOMICI, F.NUDOMICI, "
      + " F.NBCIUDAD, F.NBPROVIN, F.CDPOSTAL, F.CDDEPAIS, F.NUDNICIF, F.NBCLIENT, F.NBCLIEN1, F.NBCLIEN2, "
      + " F.TPTIPREP, F.CDNACTIT, F.FHMODIFI, F.HRMODIFI, F.CDUSUARI, F.CDORDTIT, F.RFTITAUD, "
      + " F.CDDDEBIC, F.TPIDENTI, F.TPSOCIED, F.TPNACTIT, F.CDUSUAUD, F.FHAUDIT, "
      + " F.FHINICIO, F.FHFINAL, F.NUMSEC, F.CDHOLDER, F.CATEGORY, F.CDREFBAN, "
      + " F.FECHA_NACIMIENTO FROM TMCT0FIS F WHERE F.NBCLIEN1 = :nbclient1 "
      + " AND F.FHFINAL >= :fecha AND F.FHINICIO <= :fecha AND.TPSOCIED = 'J' ORDER BY F.NUMSEC DESC ")
  List<Object[]> findAllHolderDataTmct0fisByNbclient1rAndFetradeAndJuridico(@Param("nbclient1") String nbclient1,
      @Param("fecha") BigDecimal fecha);

  @Query(nativeQuery = true, value = "SELECT F.CDDCLAVE, F.TPDOMICI, F.NBDOMICI, F.NUDOMICI, "
      + " F.NBCIUDAD, F.NBPROVIN, F.CDPOSTAL, F.CDDEPAIS, F.NUDNICIF, F.NBCLIENT, F.NBCLIEN1, F.NBCLIEN2, "
      + " F.TPTIPREP, F.CDNACTIT, F.FHMODIFI, F.HRMODIFI, F.CDUSUARI, F.CDORDTIT, F.RFTITAUD, "
      + " F.CDDDEBIC, F.TPIDENTI, F.TPSOCIED, F.TPNACTIT, F.CDUSUAUD, F.FHAUDIT, "
      + " F.FHINICIO, F.FHFINAL, F.NUMSEC, F.CDHOLDER, F.CATEGORY, F.CDREFBAN, "
      + " F.FECHA_NACIMIENTO FROM TMCT0FIS F WHERE (F.CDHOLDER like :cdholder "
      + " OR F.NBCLIENT LIKE :cdholder OR F.NBCLIEN1 LIKE :cdholder OR F.NBCLIEN2 LIKE :cdholder "
      + " OR (F.NBCLIENT || F.NBCLIEN1 || F.NBCLIEN2) LIKE :cdholder) "
      + " AND F.FHFINAL >= :fecha AND F.FHINICIO <= :fecha ORDER BY F.NUMSEC DESC ")
  List<Object[]> findAllHoldersForAutocompleter(@Param("cdholder") String holderData, @Param("fecha") BigDecimal fecha);

}
