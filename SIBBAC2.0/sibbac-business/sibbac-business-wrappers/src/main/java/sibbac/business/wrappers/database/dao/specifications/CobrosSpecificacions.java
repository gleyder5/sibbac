package sibbac.business.wrappers.database.dao.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.Cobros;


public class CobrosSpecificacions {

	protected static final Logger	LOG	= LoggerFactory.getLogger( CobrosSpecificacions.class );

	public static Specification< Cobros > listadoFiltrado( final Long idAlias, final Date fechaDesde, final Date fechaHasta,
			final Long numCobro ) {

		return new Specification< Cobros >() {

			public Predicate toPredicate( Root< Cobros > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Cobros > specs = null;
				Predicate predicate = null;

				// Especificamos...

				// Alias?
				if ( idAlias != null ) {
					specs = Specifications.where( aliasEs( idAlias ) );
				}

				// Fecha Desde?
				if ( fechaDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaDesde( fechaDesde ) );
					} else {
						specs = specs.and( fechaDesde( fechaDesde ) );
					}
				}

				// Fecha Hasta?
				if ( fechaHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaHasta( fechaHasta ) );
					} else {
						specs = specs.and( fechaHasta( fechaHasta ) );
					}
				}

				// Numero de cobro?
				if ( numCobro != null ) {
					if ( specs == null ) {
						specs = Specifications.where( numeroDeCobro( numCobro ) );
					} else {
						specs = specs.and( numeroDeCobro( numCobro ) );
					}
				}

				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};

	}

	public static Specification< Cobros > aliasEs( final Long aliasId ) {
		return new Specification< Cobros >() {

			@Override
			public Predicate toPredicate( Root< Cobros > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate aliasPredicate = null;
				if ( aliasId != null ) {
					aliasPredicate = builder.equal( root.< Cobros > get( "alias" ).< Long > get( "id" ), aliasId );
				}
				return aliasPredicate;
			}
		};
	}

	public static Specification< Cobros > fechaDesde( final Date fecha ) {
		return new Specification< Cobros >() {

			public Predicate toPredicate( Root< Cobros > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fechaFactura" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Cobros > fechaHasta( final Date fecha ) {
		return new Specification< Cobros >() {

			public Predicate toPredicate( Root< Cobros > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fechaFactura" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Cobros > numeroDeCobro( final Long numCobro ) {
		return new Specification< Cobros >() {

			public Predicate toPredicate( Root< Cobros > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				if ( numCobro != null ) {
					fechaPredicate = builder.equal( root.< Long > get( "cobro" ), numCobro );
				}
				return fechaPredicate;
			}
		};
	}

}
