package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class AlcEntregaRecepcionClienteDTO {

  private String nbooking;
  private String nuorden;
  private String nucnfclt;
  private Short nucnfliq;

  private BigDecimal nutitliq;
  private BigDecimal imefeagr;
  private BigDecimal imcomisn;
  private BigDecimal imnfiliq;
  private BigDecimal imcanoncompdv;
  private BigDecimal imcanoncompeu;
  private BigDecimal imcanoncontrdv;
  private BigDecimal imcanoncontreu;
  private BigDecimal imcanonliqdv;
  private BigDecimal imcanonliqeu;
  private String cdniftit;
  private String nbtitliq;
  private String cdcustod;
  private String ourpar;
  private String theirpar;
  private Date feejeliq;
  private Character cdtpoper;
  private String cdalias;
  private String cdisin;
  private String nbvalors;
  private String descrali;
  private String cdrefban;

  private String nombre;
  private Integer idestado;

  private String referencias3;

  private String pata;

  private String desglosecamara;
  private String estdesglose;

  private Date fechaLiquidacion;

  private Date fechaLiquidacionTeorica;
  
  private Character estadoInstruccion;
  private String cuentaCompensancionDestino;
  
  private BigInteger alcOrdenesId;
  
  private BigInteger alcOrdenMercadoId;

  public AlcEntregaRecepcionClienteDTO() {
    super();
  }

  public AlcEntregaRecepcionClienteDTO(String nbooking,
                                       String nuorden,
                                       String nucnfclt,
                                       Short nucnfliq,
                                       BigDecimal nutitliq,
                                       BigDecimal imefeagr,
                                       BigDecimal imcomisn,
                                       BigDecimal imnfiliq,
                                       BigDecimal imcanoncompdv,
                                       BigDecimal imcanoncompeu,
                                       BigDecimal imcanoncontrdv,
                                       BigDecimal imcanoncontreu,
                                       BigDecimal imcanonliqdv,
                                       BigDecimal imcanonliqeu,
                                       String cdniftit,
                                       String nbtitliq,
                                       String cdcustod,
                                       String ourpar,
                                       String theirpar,
                                       Date feejeliq,
                                       Character cdtpoper,
                                       String cdalias,
                                       String cdisin,
                                       String nbvalors,
                                       String descrali,
                                       String cdrefban,
                                       String nombre,
                                       Integer idestado,
                                       String referencias3,
                                       String pata,
                                       String desglosecamara,
                                       String estdesglose,
                                       Character estadoInstruccion,
                                       String cuentaCompensacionDestino) {
    super();
    this.nbooking = nbooking;
    this.nuorden = nuorden;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.nutitliq = nutitliq;
    this.imefeagr = imefeagr;
    this.imcomisn = imcomisn;
    this.imnfiliq = imnfiliq;
    this.imcanoncompdv = imcanoncompdv;
    this.imcanoncompeu = imcanoncompeu;
    this.imcanoncontrdv = imcanoncontrdv;
    this.imcanoncontreu = imcanoncontreu;
    this.imcanonliqdv = imcanonliqdv;
    this.imcanonliqeu = imcanonliqeu;
    this.cdniftit = cdniftit;
    this.nbtitliq = nbtitliq;
    this.cdcustod = cdcustod;
    this.ourpar = ourpar;
    this.theirpar = theirpar;
    this.feejeliq = feejeliq;
    this.cdtpoper = cdtpoper;
    this.cdalias = cdalias;
    this.cdisin = cdisin;
    this.nbvalors = nbvalors;
    this.descrali = descrali;
    this.cdrefban = cdrefban;
    this.nombre = nombre;
    this.idestado = idestado;
    this.referencias3 = referencias3;
    this.pata = pata;
    this.desglosecamara = desglosecamara;
    this.estdesglose = estdesglose;
    this.estadoInstruccion = estadoInstruccion;
    this.cuentaCompensancionDestino = cuentaCompensacionDestino;
  }

  public String getNbooking() {
    return nbooking;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public String getNucnfclt() {
    return nucnfclt;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public Short getNucnfliq() {
    return nucnfliq;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public BigDecimal getNutitliq() {
    return nutitliq;
  }

  public void setNutitliq(BigDecimal nutitliq) {
    this.nutitliq = nutitliq;
  }

  public BigDecimal getImefeagr() {
    return imefeagr;
  }

  public void setImefeagr(BigDecimal imefeagr) {
    this.imefeagr = imefeagr;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public BigDecimal getImnfiliq() {
    return imnfiliq;
  }

  public void setImnfiliq(BigDecimal imnfiliq) {
    this.imnfiliq = imnfiliq;
  }

  public BigDecimal getImcanoncompdv() {
    return imcanoncompdv;
  }

  public void setImcanoncompdv(BigDecimal imcanoncompdv) {
    this.imcanoncompdv = imcanoncompdv;
  }

  public BigDecimal getImcanoncompeu() {
    return imcanoncompeu;
  }

  public void setImcanoncompeu(BigDecimal imcanoncompeu) {
    this.imcanoncompeu = imcanoncompeu;
  }

  public BigDecimal getImcanoncontrdv() {
    return imcanoncontrdv;
  }

  public void setImcanoncontrdv(BigDecimal imcanoncontrdv) {
    this.imcanoncontrdv = imcanoncontrdv;
  }

  public BigDecimal getImcanoncontreu() {
    return imcanoncontreu;
  }

  public void setImcanoncontreu(BigDecimal imcanoncontreu) {
    this.imcanoncontreu = imcanoncontreu;
  }

  public BigDecimal getImcanonliqdv() {
    return imcanonliqdv;
  }

  public void setImcanonliqdv(BigDecimal imcanonliqdv) {
    this.imcanonliqdv = imcanonliqdv;
  }

  public BigDecimal getImcanonliqeu() {
    return imcanonliqeu;
  }

  public void setImcanonliqeu(BigDecimal imcanonliqeu) {
    this.imcanonliqeu = imcanonliqeu;
  }

  public String getCdniftit() {
    return cdniftit;
  }

  public void setCdniftit(String cdniftit) {
    this.cdniftit = cdniftit;
  }

  public String getNbtitliq() {
    return nbtitliq;
  }

  public void setNbtitliq(String nbtitliq) {
    this.nbtitliq = nbtitliq;
  }

  public String getCdcustod() {
    return cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public String getOurpar() {
    return ourpar;
  }

  public void setOurpar(String ourpar) {
    this.ourpar = ourpar;
  }

  public String getTheirpar() {
    return theirpar;
  }

  public void setTheirpar(String theirpar) {
    this.theirpar = theirpar;
  }

  public Date getFeejeliq() {
    return feejeliq;
  }

  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public String getCdalias() {
    return cdalias;
  }

  public void setCdalias(String cdalias) {
    this.cdalias = cdalias;
  }

  public String getCdisin() {
    return cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public String getNbvalors() {
    return nbvalors;
  }

  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  public String getDescrali() {
    return descrali;
  }

  public void setDescrali(String descrali) {
    this.descrali = descrali;
  }

  public String getCdrefban() {
    return cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Integer getIdestado() {
    return idestado;
  }

  public void setIdestado(Integer idestado) {
    this.idestado = idestado;
  }

  public String getReferencias3() {
    return referencias3;
  }

  public void setReferencias3(String referencias3) {
    this.referencias3 = referencias3;
  }

  public String getPata() {
    return pata;
  }

  public void setPata(String pata) {
    this.pata = pata;
  }

  public String getDesglosecamara() {
    return desglosecamara;
  }

  public void setDesglosecamara(String desglosecamara) {
    this.desglosecamara = desglosecamara;
  }

  public String getEstdesglose() {
    return estdesglose;
  }

  public void setEstdesglose(String estdesglose) {
    this.estdesglose = estdesglose;
  }

  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  public Date getFechaLiquidacionTeorica() {
    return fechaLiquidacionTeorica;
  }

  public void setFechaLiquidacionTeorica(Date fechaLiquidacionTeorica) {
    this.fechaLiquidacionTeorica = fechaLiquidacionTeorica;
  }
  
  public void setAlcOrdenesId(BigInteger alcOrdenesId) {
      this.alcOrdenesId = alcOrdenesId;
  }
  
  public BigInteger getAlcOrdenesId() {
      return alcOrdenesId;
  }
  
  public void setAlcOrdenMercadoId(BigInteger alcOrdenMercadoId) {
      this.alcOrdenMercadoId = alcOrdenMercadoId;
  }
  
  public BigInteger getAlcOrdenMercadoId() {
      return alcOrdenMercadoId;
  }

  public static AlcEntregaRecepcionClienteDTO convertObjectToAlcEntregaRecepcionTodosDTO(Object[] valuesFromQuery,
                                                                                         Integer tipo) {
    AlcEntregaRecepcionClienteDTO alcERCDTO = new AlcEntregaRecepcionClienteDTO();
    BigDecimal nucnfliq = (BigDecimal) valuesFromQuery[4];
    String nucnfliqtext = String.valueOf(nucnfliq);
    alcERCDTO.setNombre((String) valuesFromQuery[5]);
    alcERCDTO.setIdestado((Integer) valuesFromQuery[23]);
    alcERCDTO.setNbooking((String) valuesFromQuery[2]);
    alcERCDTO.setNucnfclt((String) valuesFromQuery[3]);
    alcERCDTO.setNucnfliq((Short) Short.parseShort(nucnfliqtext));
    alcERCDTO.setNuorden((String) valuesFromQuery[1]);
    //
    alcERCDTO.setCdtpoper((Character) valuesFromQuery[7]);
    alcERCDTO.setCdalias((String) valuesFromQuery[8]);
    alcERCDTO.setCdisin((String) valuesFromQuery[9]);
    alcERCDTO.setNbvalors((String) valuesFromQuery[10]);
    alcERCDTO.setCdcustod((String) valuesFromQuery[19]);
    alcERCDTO.setDescrali((String) valuesFromQuery[24]);
    //
    alcERCDTO.setNutitliq((BigDecimal) valuesFromQuery[6]);
    alcERCDTO.setImefeagr((BigDecimal) valuesFromQuery[11]);//

    if (valuesFromQuery[12] instanceof Integer) {
      if ((Integer) valuesFromQuery[12] == 0) {
        alcERCDTO.setImcomisn(BigDecimal.ZERO);
      }
    } else {
      alcERCDTO.setImcomisn((BigDecimal) valuesFromQuery[12]);
    }

    if (valuesFromQuery[13] instanceof Integer) {
      if ((Integer) valuesFromQuery[13] == 0) {
        alcERCDTO.setImnfiliq(BigDecimal.ZERO);
      }
    } else {
      alcERCDTO.setImnfiliq((BigDecimal) valuesFromQuery[13]);
    }
    alcERCDTO.setImcanoncompeu((BigDecimal) valuesFromQuery[14]);
    alcERCDTO.setImcanoncontreu((BigDecimal) valuesFromQuery[15]);
    alcERCDTO.setImcanonliqeu((BigDecimal) valuesFromQuery[16]);
    alcERCDTO.setCdniftit((String) valuesFromQuery[17]);
    alcERCDTO.setNbtitliq((String) valuesFromQuery[18]);
    alcERCDTO.setCdrefban((String) valuesFromQuery[20]);
    alcERCDTO.setOurpar((String) valuesFromQuery[21]);
    alcERCDTO.setTheirpar((String) valuesFromQuery[22]);//
    alcERCDTO.setFeejeliq((Date) valuesFromQuery[26]);
    alcERCDTO.setReferencias3((String) valuesFromQuery[25]);
    alcERCDTO.setPata((String) valuesFromQuery[0]);
    String txt_des_camara;
    if (tipo == 2 || tipo == 3) {
      BigInteger des_camara = (BigInteger) valuesFromQuery[27];
      txt_des_camara = des_camara.toString();
    } else {
      Integer des_camara = (Integer) valuesFromQuery[27];
      txt_des_camara = des_camara.toString();
    }
    alcERCDTO.setDesglosecamara(txt_des_camara);
    alcERCDTO.setEstdesglose((String) valuesFromQuery[28]);
    alcERCDTO.setFechaLiquidacionTeorica((Date) valuesFromQuery[29]);
    alcERCDTO.setCuentaCompensancionDestino((String) valuesFromQuery[30]);
    alcERCDTO.setEstadoInstruccion((Character) valuesFromQuery[31]);
    alcERCDTO.setAlcOrdenesId((BigInteger)valuesFromQuery[32]);
    alcERCDTO.setAlcOrdenMercadoId((BigInteger)valuesFromQuery[33]);
    return alcERCDTO;
  }

public Character getEstadoInstruccion() {
	return estadoInstruccion;
}

public void setEstadoInstruccion(Character estadoInstruccion) {
	this.estadoInstruccion = estadoInstruccion;
}

public String getCuentaCompensancionDestino() {
	return cuentaCompensancionDestino;
}

public void setCuentaCompensancionDestino(String cuentaCompensancionDestino) {
	this.cuentaCompensancionDestino = cuentaCompensancionDestino;
}

}
