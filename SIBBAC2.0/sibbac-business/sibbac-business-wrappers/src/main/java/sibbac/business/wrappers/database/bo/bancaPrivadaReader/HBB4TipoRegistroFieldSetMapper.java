package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.common.utils.FormatDataUtils;

public class HBB4TipoRegistroFieldSetMapper implements FieldSetMapper<HBB4TipoRegistroRecordBean> {

  public final static String delimiter = ";";

  public enum HBB4TipoRegistro {
    TITULAR(1),
    DESGLSE(0);

    public int tipo;

    private HBB4TipoRegistro(int tipo) {
      this.tipo = tipo;
    }
  }

  public enum HBB4TipoRegistroFields {
    REFEXTERNA("refExterna", 16),
    FECHACUADRE("fechaCuadre", 8),
    TIPOREGISTRO("tipoRegistro", 1);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private HBB4TipoRegistroFields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      HBB4TipoRegistroFields[] fields = HBB4TipoRegistroFields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getHBB4Lengths() {
      HBB4TipoRegistroFields[] fields = HBB4TipoRegistroFields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public HBB4TipoRegistroRecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    HBB4TipoRegistroRecordBean hbb4bean = new HBB4TipoRegistroRecordBean();

    hbb4bean.setRefExterna(fieldSet.readString(HBB4TipoRegistroFields.REFEXTERNA.text).trim());
    hbb4bean.setFechaCuadre(fieldSet.readDate(HBB4TipoRegistroFields.FECHACUADRE.text, FormatDataUtils.DATE_FORMAT));
    hbb4bean.setTipoRegistroHBB(fieldSet.readBigDecimal(HBB4TipoRegistroFields.TIPOREGISTRO.text));

    return hbb4bean;
  }

}
