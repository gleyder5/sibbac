package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.Tmct0ejeData;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0ejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchema;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.tmct0eje.dto.Tmct0ejeInformacionMercadoDTO;

@Repository
public interface Tmct0ejeDao extends JpaRepository<Tmct0eje, Tmct0ejeId> {

  @Query("select e from Tmct0ejeFeeSchema e where e.tmct0eje.id = :id ")
  Tmct0ejeFeeSchema findTmct0ejeFeeSchemaById(@Param("id") Tmct0ejeId id);

  @Query(value = "SELECT * FROM TMCT0EJE EJE WHERE EJE.IDCASE=:caseId", nativeQuery = true)
  List<Tmct0eje> findByCaseId(@Param("caseId") Long caseId);

  @Query("Select eje from Tmct0eje eje WHERE eje.id.nuorden=:nuorden AND eje.id.nbooking=:nbooking ")
  List<Tmct0eje> findAllByNbookingAndNuorden(@Param("nbooking") String nbooking, @Param("nuorden") String nuorden);

  @Query("Select eje from Tmct0eje eje, Tmct0ejecucionalocation ea "
      + " WHERE eje.id = ea.tmct0eje.id AND ea.tmct0grupoejecucion.tmct0alo.id.nuorden = :nuorden "
      + " AND ea.tmct0grupoejecucion.tmct0alo.id.nbooking = :nbooking "
      + " AND ea.tmct0grupoejecucion.tmct0alo.id.nucnfclt = :nucnfclt")
  List<Tmct0eje> findAllByNbookingAndNuordenAndNucnfclt(@Param("nbooking") String nbooking,
      @Param("nuorden") String nuorden, @Param("nucnfclt") String nucnfclt);

  @Query("Select new sibbac.business.wrappers.database.data.Tmct0ejeData(eje) from Tmct0eje eje WHERE eje.id.nuorden=:nuorden AND eje.id.nbooking=:nbooking ")
  List<Tmct0ejeData> findAllTmct0ejeDataByNbookingAndNuorden(@Param("nbooking") String nbooking,
      @Param("nuorden") String nuorden);

  @Query("Select eje from Tmct0eje eje WHERE eje.id.nbooking=:nbooking AND eje.id.nuorden=:nuorden AND eje.id.nuejecuc=:nuejecuc")
  List<Tmct0eje> findAllByNbookingAndNuordenAndNuejecuc(@Param("nbooking") String nbooking,
      @Param("nuorden") String nuorden, @Param("nuejecuc") String nuejecuc);

  // @Query(value =
  // "select new sibbac.business.wrappers.database.dto.Tmct0ejeInformacionMercadoCaseDTO(e.cdisin, e.cdtpoper, e.feejecuc, e.cdmiembromkt, e.segmenttradingvenue, "
  // +
  // " e.tpopebol, e.nuopemer, e.nurefere, e.nuorden, e.nbooking, e.nuejecuc, e.nutiteje, e.nutittotal) from tmct0eje e where e.feejecuc >=:fechaEjecucion "
  // +
  // " and e.casepti ='N' and exists ( select 1 from tmct0bok b where e.nuorden=b.nuorden and e.nbooking=b.nbooking and b.cdestadoasig > 0) ",
  // nativeQuery = true)
  // List<Tmct0ejeInformacionMercadoCaseDTO>
  // findAllCasePendingSinceDate(@Param("fechaEjecucion") Date
  // fechaEjecucion);

  @Query("select new sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0ejeInformacionMercadoCaseDTO(e.cdisin, e.cdtpoper, e.feejecuc, e.cdMiembroMkt, e.segmenttradingvenue, "
      + " e.tpopebol, e.nuopemer, e.nurefere, e.id.nuorden, e.id.nbooking, e.id.nuejecuc, e.nutiteje, e.nutittotal) from Tmct0eje e, Tmct0bok b "
      + " where e.id.nuorden=b.id.nuorden and e.id.nbooking=b.id.nbooking and e.feejecuc >=:fechaEjecucion "
      + " and e.casepti ='N' and b.tmct0estadoByCdestadoasig.idestado > 0 "
      + " order by e.feejecuc asc, e.cdisin asc, e.cdtpoper asc, e.cdMiembroMkt asc, e.segmenttradingvenue asc, "
      + " e.tpopebol asc, e.nuopemer asc, e.nurefere asc")
  List<Tmct0ejeInformacionMercadoCaseDTO> findAllCasePendingSinceDate(@Param("fechaEjecucion") Date fechaEjecucion);

  /**
   * update flag casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   * @param nuejecuc
   * @param idCase
   * @param fhaudit
   */
  @Modifying
  @Query(value = "UPDATE TMCT0EJE SET CASEPTI = 'S', IDCASE = :pidCase, FHAUDIT = :pfhaudit "
      + " WHERE NUORDEN = :pnuorden AND NBOOKING = :pnbooking AND NUEJECUC = :pnuejecuc ", nativeQuery = true)
  Integer updateCasadoPti(@Param("pnuorden") String nuorden, @Param("pnbooking") String nbooking,
      @Param("pnuejecuc") String nuejecuc, @Param("pidCase") Long idCase, @Param("pfhaudit") Date fhaudit);

  @Query("SELECT count(*) from Tmct0eje e where e.id.nuorden=:nuorden and e.id.nbooking=:nbooking and e.casepti ='N' ")
  Integer countNoCasadoPti(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking);

  @Query("SELECT distinct  new sibbac.business.wrappers.database.tmct0eje.dto.Tmct0ejeInformacionMercadoDTO("
      + " e.cdisin, e.feejecuc, e.cdtpoper, e.clearingplatform, e.segmenttradingvenue, e.tpopebol, "
      + " e.cdMiembroMkt, e.nuopemer, e.nurefere )"
      + "from Tmct0eje e where e.id.nuorden = :nuorden and e.id.nbooking = :nbooking "
      + " and exists (select 1 from Tmct0eje e2 where e2.cdisin = e.cdisin and e2.feejecuc = e.feejecuc and e2.cdtpoper = e.cdtpoper "
      + "            and e2.clearingplatform = e.clearingplatform and e2.segmenttradingvenue = e.segmenttradingvenue and e2.tpopebol = e.tpopebol "
      + "            and e2.cdMiembroMkt = e.cdMiembroMkt and e2.nuopemer = e.nuopemer and e2.nurefere = e.nurefere "
      + "            and e2.id.nuorden = e.id.nuorden and e2.id.nbooking = e.id.nbooking "
      + "            group by e2.cdisin, e2.feejecuc, e2.cdtpoper, e2.clearingplatform, e2.segmenttradingvenue, e2.tpopebol, "
      + "                     e2.cdMiembroMkt, e2.nuopemer, e2.nurefere having count(*) > 1 )")
  List<Tmct0ejeInformacionMercadoDTO> findDistinctTmct0ejeInformacionMercadoDTOEjecucionesDuplicadasByNuordenAndNbooking(
      @Param("nuorden") String nuorden, @Param("nbooking") String nbooking);

  // @Query("SELECT e from Tmct0eje e where e.cdisin = :cdisin and e.feejecuc = :feejecuc and e.cdtpoper = :cdtpoper "
  // +
  // "            and e.clearingplatform = :clearingplatform and e.segmenttradingvenue = :segmenttradingvenue and e.tpopebol = :tpopebol "
  // +
  // "            and e.cdMiembroMkt = :cdMiembroMkt and e.nuopemer = :nuopemer and e.nurefere = :nurefere ")
  // List<Tmct0eje> findAllByInformacionMercado(@Param("nuorden") String
  // nuorden, @Param("nbooking") String nbooking);

  List<Tmct0eje> findAllByCdisinAndFeejecucAndCdtpoperAndClearingplatformAndSegmenttradingvenueAndTpopebolAndCdMiembroMktAndNuopemerAndNurefereAndIdNuordenAndIdNbooking(
      String cdisin, Date feejecuc, Character cdtpoper, String clearingplatform, String segmenttradingvenue,
      String tpopebol, String cdMiembroMkt, String nuopemer, String nurefere, String nuorden, String nbooking);

  @Query("SELECT e.nutittotal from Tmct0eje e where e.id.nuorden=:nuorden and e.id.nbooking=:nbooking and e.nurefere = :nurefere ")
  BigDecimal getTitulosEjecucion(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nurefere") String nurefere);

} // Tmct0ejeDao
