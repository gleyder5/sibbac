package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.dao.Tmct0SelectsDao;
import sibbac.business.wrappers.database.model.Tmct0Selects;
import sibbac.database.bo.AbstractBo;


/**
 * The Class Tmct0SelectsBo.
 */
@Service
public class Tmct0SelectsBo extends AbstractBo< Tmct0Selects, Long, Tmct0SelectsDao > {

	/**
	 *	Devuelve DTO con clave-valor de select populado.
	 *	@param catalogo catalogo
	 *	@return List<SelectDTO> List<SelectDTO>   
	 */
	public List<SelectDTO> getListSelectDTOByCatalogo(String catalogo) {
		List<SelectDTO> catalogoList = new ArrayList<SelectDTO>();
		List<Tmct0Selects> selects = this.dao.findByCatalogo(catalogo);
		if (CollectionUtils.isNotEmpty(selects)) {
			for (Tmct0Selects sel : selects) {
				catalogoList.add(new SelectDTO(String.valueOf(sel.getClave()).trim(), String.valueOf(sel.getValor()).trim()));
			}
		}
		return catalogoList;
	}
}
