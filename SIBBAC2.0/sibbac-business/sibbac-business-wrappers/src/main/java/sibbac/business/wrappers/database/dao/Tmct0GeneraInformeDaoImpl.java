package sibbac.business.wrappers.database.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class Tmct0GeneraInformeDaoImpl {

	@PersistenceContext
	private EntityManager em;

	private static final String TAG = Tmct0GeneraInformeDaoImpl.class.getName();
	private static final Logger LOG = LoggerFactory.getLogger(TAG);

	/**
	 * Establece los parametros en la query especificada y la ejecuta.
	 * 
	 * @param sqlQuery
	 *            Query a preparar y ejecutar.
	 * @param params
	 *            Parametros a establecer en la query.
	 * @return <code>List<Object[]> - </code>Lista de resultados devueltos por
	 *         la query.
	 * @author XI316153
	 */
	@Transactional
	public List<Object[]> prepareAndExecuteNativeQuery(String sqlQuery, Map<String, Object> params) {
		Query query = em.createNativeQuery(sqlQuery);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	} // prepareAndExecuteQuery

}
