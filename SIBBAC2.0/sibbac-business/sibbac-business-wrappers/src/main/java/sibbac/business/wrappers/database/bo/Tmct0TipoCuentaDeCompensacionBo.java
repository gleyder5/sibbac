package sibbac.business.wrappers.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0TipoCuentaDeCompensacionDao;
import sibbac.business.wrappers.database.data.TipoCuentaDeCompensacionData;
import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0TipoCuentaDeCompensacionBo extends AbstractBo< Tmct0TipoCuentaDeCompensacion, Long, Tmct0TipoCuentaDeCompensacionDao > {
    public List<TipoCuentaDeCompensacionData>findAllData(){
	return dao.findAllData();
    }
}
