package sibbac.business.wrappers.database.dao;


import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.data.AliasFacturableData;
import sibbac.business.wrappers.database.dto.AliasSubcuentaDTO;
import sibbac.business.wrappers.database.model.AliasSubcuenta;


@Component
public interface AliasSubcuentaDao extends JpaRepository< AliasSubcuenta, Long > {

	@Query( "SELECT ascta FROM AliasSubcuenta ascta inner join ascta.tmct0act act WHERE act.id.cdsubcta=:cdsubcta" )
	public AliasSubcuenta findByCodigoSubcuenta( @Param( "cdsubcta" ) String cdsubcta );

	@Query( "SELECT new sibbac.business.wrappers.database.data.AliasFacturableData (ascta.aliasFacturacion, ascta.tmct0act.id.cdaliass, ascta.tmct0act.id.cdsubcta) FROM AliasSubcuenta ascta where ascta.aliasFacturacion.tmct0ali.id.cdaliass in (:cdaliasAProcesar)" )
	public
			List< AliasFacturableData > findDistinctAliasFacturacion( @Param( "cdaliasAProcesar" ) final Set< String > cdaliasAProcesar );

	@Query( "select new sibbac.business.wrappers.database.dto.AliasSubcuentaDTO(ascta.id, ascta.aliasFacturacion, ascta.tmct0act.id.cdsubcta, ascta.tmct0act.descrali) from AliasSubcuenta ascta, Alias a where a.id = :idAlias and ascta.tmct0act.id.cdaliass = a.tmct0ali.id.cdaliass" )
	public
			List< AliasSubcuentaDTO > findAllAliasSubcuentas( @Param( "idAlias" ) final Long idAlias );

}
