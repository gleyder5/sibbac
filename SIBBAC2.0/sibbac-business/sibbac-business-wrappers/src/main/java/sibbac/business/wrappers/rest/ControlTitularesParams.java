package sibbac.business.wrappers.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0afiId;

class ControlTitularesParams implements FuenteTitulares {

  private static final Logger LOG = LoggerFactory.getLogger(ControlTitularesParams.class);

  private static final String REFERENCIAS = "referencias";

  private static final String TITULARES = "titulares";

  private static final String REF_NUORDEN = "nuorden";

  private static final String REF_NBOOKING = "nbooking";

  private static final String REF_NUCNFCLT = "nucnfclt";

  private static final String REF_NUCNFLIQ = "nucnfliq";

  private static final String FILTER_NBCLIENT = "nbcliente";

  private static final String FILTER_NBCLIENT1 = "nbcliente1";

  private static final String FILTER_NBCLIENT2 = "nbcliente2";

  private static final String FILTER_PAISRES = "cddepais";

  private static final String FILTER_NB_DOMICILIO = "nbdomici";

  private static final String FILTER_TP_DOMICILIO = "tpdomici";

  private static final String FILTER_NU_DOMICILIO = "nudomici";

  private static final String FILTER_PROVINCIA = "nbprovin";

  private static final String FILTER_COD_POSTAL = "cdpostal";

  private static final String FILTER_TIPDOC = "tpidenti";

  private static final String FILTER_CIUDAD = "nbciudad";

  private static final String FILTER_NUDNICIF = "nudnicif";

  private static final String FILTER_TIPNAC = "tpnactit";

  private static final String FILTER_NACIONALIDAD = "cdnactit";

  private static final String FILTER_TPSOCIED = "tpsocied";

  private static final String FILTER_TPTIPREP = "tptiprep";

  private static final String FILTER_PARTICIP = "particip";

  private static final String FILTER_CCV = "ccv";

  private static final String PARAM_REF_CLIENTE = "refCliente";

  private final List<Tmct0afiId> listAfiIds;

  private final List<Long> listAfiErrorIds;

  private List<Map<String, String>> titulares;

  private int cantidadReferencias;

  private int cantidadTitulares;

  private boolean valido;

  /**
   * @param filtros
   * @return
   */
  private static String processString(final String filterString) {
    String processedString = null;
    if (filterString != null) {
      processedString = filterString;
    }
    return processedString;
  }

  private static String processStringNombre(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    else if (filterString != null) {
      processedString = filterString;
    }
    return processedString;
  }

  private static Character processCharacter(String filterString) {
    Character processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString.trim().charAt(0);
    }
    return processedString;
  }

  private static BigDecimal processBigDecimal(String filterString) {
    BigDecimal processedBigDecimal = null;
    if (StringUtils.isNotEmpty(filterString)) {
      if (filterString.trim().equals("<null>")) {
        processedBigDecimal = new BigDecimal("-1");
      }
      else {
        processedBigDecimal = new BigDecimal(filterString);
      }
    }
    return processedBigDecimal;
  }

  ControlTitularesParams() {
    listAfiIds = new ArrayList<>();
    listAfiErrorIds = new ArrayList<>();
    valido = false;
  }

  void cargaParametros(List<Map<String, String>> params) {
    valido = true;
    if (params.isEmpty())
      return;
    determinaCantidades(params.get(0));
    if (valido) {
      parseaReferencias(params.subList(1, cantidadReferencias + 1));
      titulares = params.subList(cantidadReferencias + 1, cantidadReferencias + cantidadTitulares + 1);
    }
  }

  int getCantidadReferencias() {
    return cantidadReferencias;
  }

  int getCantidadTitulares() {
    return cantidadTitulares;
  }

  @Override
  public List<Tmct0afiId> getListAfiIds() {
    return listAfiIds;
  }

  @Override
  public List<Long> getListAfiErrorIds() {
    return listAfiErrorIds;
  }
  
  public TitularesDTO getTitular() {
    final TitularesDTO dto;
    final Map<String, String> elemento;

    if (cantidadTitulares < 1) {
      return null;
    }
    elemento = titulares.get(0);
    dto = new TitularesDTO();
    dto.setNbclient(processString(elemento.get(FILTER_NBCLIENT)));
    dto.setNbclient1(processString(elemento.get(FILTER_NBCLIENT1)));
    dto.setNbclient2(processString(elemento.get(FILTER_NBCLIENT2)));
    dto.setNudnicif(processString(elemento.get(FILTER_NUDNICIF)));
    dto.setTipnactit(processCharacter(elemento.get(FILTER_TIPNAC)));
    dto.setNacionalidad(processString(elemento.get(FILTER_NACIONALIDAD)));
    dto.setTipdoc(processCharacter(elemento.get(FILTER_TIPDOC)));
    dto.setPaisres(processString(elemento.get(FILTER_PAISRES)));
    dto.setProvincia(processString(elemento.get(FILTER_PROVINCIA)));
    dto.setPoblacion(processString(elemento.get(FILTER_CIUDAD)));
    dto.setCodpostal(processString(elemento.get(FILTER_COD_POSTAL)));
    dto.setTptiprep(processCharacter(elemento.get(FILTER_TPTIPREP)));
    dto.setTipopers(processCharacter(elemento.get(FILTER_TPSOCIED)));
    dto.setParticip(processBigDecimal(elemento.get(FILTER_PARTICIP)));
    dto.setCcv(processString(elemento.get(FILTER_CCV)));
    // Domicilio
    dto.setTpdomici(processString(elemento.get(FILTER_TP_DOMICILIO)));
    dto.setNbdomici(processString(elemento.get(FILTER_NB_DOMICILIO)));
    dto.setNudomici(processString(elemento.get(FILTER_NU_DOMICILIO)));
    //
    dto.setRefCliente(processString(elemento.get(PARAM_REF_CLIENTE)));
    return dto;
  }

  @Override
  public List<PartenonRecordBean> listTitulares() {
    final List<PartenonRecordBean> beanList;
    PartenonRecordBean bean;

    beanList = new ArrayList<>();
    for (Map<String, String> elemento : titulares) {
      bean = new PartenonRecordBean();
      bean.setNbclient(processStringNombre(elemento.get(FILTER_NBCLIENT)));
      bean.setNbclient1(processStringNombre(elemento.get(FILTER_NBCLIENT1)));
      bean.setNbclient2(processStringNombre(elemento.get(FILTER_NBCLIENT2)));
      bean.setNifbic(processString(elemento.get(FILTER_NUDNICIF)));
      bean.setIndNac(processCharacter(elemento.get(FILTER_TIPNAC)));
      bean.setNacionalidad(processString(elemento.get(FILTER_NACIONALIDAD)));
      bean.setTipoDocumento(processCharacter(elemento.get(FILTER_TIPDOC)));
      bean.setPaisResidencia(processString(elemento.get(FILTER_PAISRES)));
      bean.setProvincia(processString(elemento.get(FILTER_PROVINCIA)));
      bean.setPoblacion(processString(elemento.get(FILTER_CIUDAD)));
      bean.setDistrito(processString(elemento.get(FILTER_COD_POSTAL)));
      bean.setTiptitu(processCharacter(elemento.get(FILTER_TPTIPREP)));
      bean.setTipoPersona(processCharacter(elemento.get(FILTER_TPSOCIED)));
      bean.setParticip(processBigDecimal(elemento.get(FILTER_PARTICIP)));
      bean.setCcv(processString(elemento.get(FILTER_CCV)));
      bean.setNumOrden(processString(elemento.get(PARAM_REF_CLIENTE)));
      // Domicilio
      bean.setTpdomici(processString(elemento.get(FILTER_TP_DOMICILIO)));
      bean.setNbdomici(processString(elemento.get(FILTER_NB_DOMICILIO)));
      bean.setNudomici(processString(elemento.get(FILTER_NU_DOMICILIO)));
      beanList.add(bean);
    }
    return beanList;
  }

  @Override
  public List<TitularesDTO> listTitularesDto() {
    List<TitularesDTO> l = new ArrayList<TitularesDTO>();
    TitularesDTO dto;

    if (cantidadTitulares < 1) {
      return null;
    }
    for (Map<String, String> elemento : titulares) {
      dto = new TitularesDTO();
      dto.setNbclient(processString(elemento.get(FILTER_NBCLIENT)));
      dto.setNbclient1(processString(elemento.get(FILTER_NBCLIENT1)));
      dto.setNbclient2(processString(elemento.get(FILTER_NBCLIENT2)));
      dto.setNudnicif(processString(elemento.get(FILTER_NUDNICIF)));
      dto.setTipnactit(processCharacter(elemento.get(FILTER_TIPNAC)));
      dto.setNacionalidad(processString(elemento.get(FILTER_NACIONALIDAD)));
      dto.setTipdoc(processCharacter(elemento.get(FILTER_TIPDOC)));
      dto.setPaisres(processString(elemento.get(FILTER_PAISRES)));
      dto.setProvincia(processString(elemento.get(FILTER_PROVINCIA)));
      dto.setPoblacion(processString(elemento.get(FILTER_CIUDAD)));
      dto.setCodpostal(processString(elemento.get(FILTER_COD_POSTAL)));
      dto.setTptiprep(processCharacter(elemento.get(FILTER_TPTIPREP)));
      dto.setTipopers(processCharacter(elemento.get(FILTER_TPSOCIED)));
      dto.setParticip(processBigDecimal(elemento.get(FILTER_PARTICIP)));
      dto.setCcv(processString(elemento.get(FILTER_CCV)));
      // Domicilio
      dto.setTpdomici(processString(elemento.get(FILTER_TP_DOMICILIO)));
      dto.setNbdomici(processString(elemento.get(FILTER_NB_DOMICILIO)));
      dto.setNudomici(processString(elemento.get(FILTER_NU_DOMICILIO)));
      //
      dto.setRefCliente(processString(elemento.get(PARAM_REF_CLIENTE)));
      l.add(dto);
    }
    return l;
  }

  void parseaReferencia(Map<String, String> elemento) {
    final Tmct0afiId afiId;
    final String afiIdValue;

    if (elemento.get("iderror") != null) {
      afiIdValue = elemento.get("iderror");
      try {
        listAfiErrorIds.add(Long.valueOf(afiIdValue));
      }
      catch (NumberFormatException nfex) {
        LOG.warn("El parametro Id no cumple con el formato de entero largo {} | Mensaje {}", afiIdValue,
            nfex.getMessage());
        valido = false;
      }
    }
    else {
      afiId = new Tmct0afiId();
      try {
        afiId.setNuorden(elemento.get(REF_NUORDEN));
        afiId.setNbooking(elemento.get(REF_NBOOKING));
        afiId.setNucnfclt(elemento.get(REF_NUCNFCLT));
        afiId.setNucnfliq(Short.parseShort(elemento.get(REF_NUCNFLIQ)));
        if (elemento.get("nusecuen") != null) {
          afiId.setNusecuen(new BigDecimal(elemento.get("nusecuen")));
        }
        listAfiIds.add(afiId);
      }
      catch (RuntimeException rex) {
        LOG.warn("[parseaReferencia] La referencia a afi no cumple con el formato esperado: {}. Mensaje {}",
            elemento.toString(), rex.getMessage());
        valido = false;
      }
    }
  }

  private void determinaCantidades(Map<String, String> elemento) {
    try {
      cantidadReferencias = Integer.parseInt(elemento.get(REFERENCIAS));
      cantidadTitulares = Integer.parseInt(elemento.get(TITULARES));
    }
    catch (RuntimeException rex) {
      LOG.warn("[determinaCantidades] La primera linea no tiene el formato esperado: {}. Mensaje {}",
          elemento.toString(), rex.getMessage());
      valido = false;
    }
  }

  private void parseaReferencias(List<Map<String, String>> referencias) {
    for (Map<String, String> elemento : referencias) {
      parseaReferencia(elemento);
    }
  }

}
