package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Servicios;
import sibbac.tasks.database.Job;


// TODO: Auto-generated Javadoc
/**
 * The Interface ServiciosDao.
 */
@Component
public interface ServiciosDao extends JpaRepository< Servicios, Long > {

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the servicios
	 */
	Servicios findById( final Long id );

	/**
	 * Find by nombre servicio.
	 *
	 * @param name the name
	 * @return the servicios
	 */
	Servicios findByNombreServicio( final String name );

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.jpa.repository.JpaRepository#findAll()
	 */
	List< Servicios > findAll();

	/**
	 * Find all by activo.
	 *
	 * @param activo the activo
	 * @return the list
	 */
	List< Servicios > findAllByActivo( boolean activo );

	/**
	 * findByDescripcionservicio.
	 *
	 * @param job the job
	 * @return the servicios
	 */
	Servicios findServiciosByJob( final Job job );

	/**
	 * Find servicios by configuracion alias and.
	 *
	 * @param configuracionAlias the configuracion alias
	 * @return the servicios
	 */
	List< Servicios > findAllByConfiguracionAlias( final boolean configuracionAlias );

	/**
	 * Find all byconfiguracion contactos.
	 *
	 * @param configuracionContactos the configuracion contactos
	 * @return the list
	 */
	List< Servicios > findAllByconfiguracionContactos( final boolean configuracionContactos );
}
