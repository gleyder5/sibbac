/**
 * 
 */
package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.bo.ReglasCanonesBo;
import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.common.RunnableProcess;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.Tcli0comBo;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0ejecucionalocationBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.data.Tmct0AloDataOperacionesEspecialesPartenon;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedDbReader;
import sibbac.common.SIBBACBusinessException;

/**
 * @author xIS16630
 *
 */
@Service(value = "procesarDesglosesDbReader")
public class ProcesarDesglosesDbReader extends
    WrapperMultiThreadedDbReader<OperacionEspecialProcesarDesgloseRecordBean> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(WrapperMultiThreadedDbReader.class);

  /** Numero de operaciones a realizar en cada commit de base de datos. */
  @Value("${sibbac.wrappers.transaction.size}")
  private Integer transactionSize;

  @Value("${sibbac.wrappers.thread.size}")
  private Integer threadSize;

  @Value("${sibbac.wrappers.large.query.page.size}")
  private Integer largeQueryPageSize;

  @Value("${sibbac.wrappers.operacion.especial.desglose.max.execution.pages}")
  private Integer maxExecutionPages;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;
  @Autowired
  private Tmct0alcBo alcBo;
  @Autowired
  private Tmct0ejeBo ejeBo;

  @Autowired
  private Tcli0comBo tcli0comBo;

  @Autowired
  private Tmct0cfgBo configBo;

  @Autowired
  private Tmct0CompensadorBo compensadorBo;

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0ValoresBo tmct0ValoresBo;

  @Autowired
  private Tmct0ejecucionalocationBo ealoBo;

  @Autowired
  private ReglasCanonesBo reglasCanonesBo;

  @Qualifier(value = "desgloseProcesorRunnable")
  @Autowired
  private DesgloseProcesorRunnable desgloseRunnable;

  @Override
  protected boolean isInBlock(RecordBean previousBean, RecordBean bean) {
    return false;
  }

  @Override
  public IRunnableBean<OperacionEspecialProcesarDesgloseRecordBean> getBeanToProcessBlock() {
    return desgloseRunnable;
  }

  @Override
  public int getThreadSize() {
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @SuppressWarnings("unchecked")
  private List<DesgloseRecordDTO> getListaOperacionesEspecialesSinDesglosar(String numorden)
      throws SIBBACBusinessException {
    final List<DesgloseRecordDTO> list = (List<DesgloseRecordDTO>) Collections
        .synchronizedList(new ArrayList<DesgloseRecordDTO>());

    final Pageable page = new PageRequest(0, maxExecutionPages * largeQueryPageSize);
    LOG.debug("[ProcesarDesglosesDbReader :: getListaOperacionesEspecialesSinDesglosar] buscando lista de nurefords en EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR");
    LOG.debug("[ProcesarDesglosesDbReader :: getListaOperacionesEspecialesSinDesglosar] nurefords {}", page);
    List<String> nurefords = desgloseRecordBeanBo.findAllNurefordOperacionesEspecialesByProcesadoAndNumorden(numorden,
        EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS.getId(), page);

    LOG.debug("[ProcesarDesglosesDbReader :: getListaOperacionesEspecialesSinDesglosar] encontrados {} nurefors",
        nurefords.size());
    if (CollectionUtils.isNotEmpty(nurefords)) {

      if (nurefords.size() < largeQueryPageSize) {
        List<DesgloseRecordDTO> listSub = desgloseRecordBeanBo.findAllTitulosEjecutadosByListNureford(nurefords);
        LOG.debug("[ProcesarDesglosesDbReader :: run] encontrados {} desgloses.", listSub.size());
        list.addAll(listSub);
      }
      else {
        final ObserverProcess myObserver = new ObserverProcess();
        try {
          int posIni = 0;
          int posEnd = 0;

          while (posEnd < nurefords.size()) {
            posEnd += largeQueryPageSize;
            if (posEnd > nurefords.size()) {
              posEnd = nurefords.size();
            }

            LOG.debug(
                "[ProcesarDesglosesDbReader :: getListaOperacionesEspecialesSinDesglosar] pageable posIni : {} posEnd : {}",
                posIni, posEnd);
            final List<String> nurefordSubList = new ArrayList<String>(nurefords.subList(posIni, posEnd));
            LOG.debug("[ProcesarDesglosesDbReader :: getListaOperacionesEspecialesSinDesglosar] Lanzando hilo para leer...");

            RunnableProcess<PartenonRecordBean> process = new RunnableProcess<PartenonRecordBean>(myObserver) {

              @Override
              public void run() {
                LOG.debug("[ProcesarDesglosesDbReader :: run] inicio.");
                try {
                  LOG.debug("[ProcesarDesglosesDbReader :: run] buscando desgloses.");
                  List<DesgloseRecordDTO> listSub = desgloseRecordBeanBo
                      .findAllTitulosEjecutadosByListNureford(nurefordSubList);
                  LOG.debug("[ProcesarDesglosesDbReader :: run] encontrados {} desgloses.", listSub.size());
                  list.addAll(listSub);
                  LOG.debug("[ProcesarDesglosesDbReader :: run] lista total despues de añadir {} desgloses.",
                      list.size());
                }
                catch (Exception e) {
                  myObserver.addErrorProcess(new SIBBACBusinessException(
                      "Incidencia con sublista : " + nurefordSubList, e));
                }
                finally {
                  myObserver.sutDownThread(this);
                }
                LOG.debug("[ProcesarDesglosesDbReader :: run] fin.");
              }
            };
            executeProcessInThread(myObserver, process);

            LOG.debug("[ProcesarDesglosesDbReader :: getListaOperacionesEspecialesSinDesglosar] hilo lanzado");
            posIni = posEnd;
          }
        }
        catch (Exception e) {
          closeExecutorNow();
          throw new SIBBACBusinessException(e);
        }
        finally {

          closeExecutor();

          nurefords.clear();
        }

      }
      if (getObserver().getException() != null) {
        list.clear();
        throw new SIBBACBusinessException(getObserver().getException());
      }
    }

    Collections.sort(list, new Comparator<DesgloseRecordDTO>() {

      @Override
      public int compare(DesgloseRecordDTO arg0, DesgloseRecordDTO arg1) {
        int titulos = arg0.getTitulosEjecutados().compareTo(arg1.getTitulosEjecutados());
        if (titulos == 0) {
          return arg0.getNumeroReferenciaOrden().compareTo(arg1.getNumeroReferenciaOrden());
        }
        else {
          return titulos;
        }

      }
    });
    return list;
  }

  @Override
  @Transactional
  public void preExecuteTask() throws SIBBACBusinessException {
    LOG.trace("[ProcesarDesglosesDbReader :: preExecuteTask] inicio ...");

    List<DesgloseRecordDTO> ordenes = desgloseRecordBeanBo
        .findAllOrdenesProcesadas(EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.DESGLOSES_CORRECTOS_PDTE_CREAR_ALLOS
            .getId());
    this.pasarScriptDividend(ordenes);
    List<OperacionEspecialProcesarDesgloseRecordBean> operaciones = getListaDesglosesOrdenes(ordenes);

    beanIterator = operaciones.iterator();

    LOG.trace("[ProcesarDesglosesDbReader :: preExecuteTask] fin");
  }

  private void pasarScriptDividend(List<DesgloseRecordDTO> ordenes) {
    Integer count;
    for (DesgloseRecordDTO desgloseRecordDTO : ordenes) {
      try {
        count = desgloseRecordBeanBo.countByNumeroOrden(desgloseRecordDTO.getNumeroOrden());
        if (count >= 1000) {
          ordBo.pasarScriptDividendNewTransaction(desgloseRecordDTO.getNumeroOrden(), true);
        }
      }
      catch (Exception e) {
        LOG.warn("[pasarScriptDividend]no se ha podido cambiar la orden a sd:{}-{}",
            desgloseRecordDTO.getNumeroOrden(), e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private List<OperacionEspecialProcesarDesgloseRecordBean> getListaDesglosesOrdenes(List<DesgloseRecordDTO> ordenes)
      throws SIBBACBusinessException {
    final List<OperacionEspecialProcesarDesgloseRecordBean> operaciones = Collections
        .synchronizedList(new ArrayList<OperacionEspecialProcesarDesgloseRecordBean>());

    final CanonesConfigDTO canonesConfig = reglasCanonesBo.getCanonesConfig();
    canonesConfig.setWriteTmct0log(false);
    final Map<String, String> corretajePtiCompensadores = new HashMap<String, String>();
    final Map<String, Tmct0CamaraCompensacion> cdcamarasByCdcodigo = new HashMap<String, Tmct0CamaraCompensacion>();
    final ObserverProcess myObserver = new ObserverProcess();

    try {
      for (DesgloseRecordDTO orden : ordenes) {
        final String numOrden = orden.getNumeroOrden();
        LOG.info("[ProcesarDesglosesDbReader :: preExecuteTask]  buscando allo con cdestadoasig EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL...");
        if (canonesConfig.getListReglasCanon() == null || canonesConfig.getListReglasCanon().isEmpty()) {
          logBo.insertarRegistroNewTransaction(numOrden, "",
              "INCIDENCIA-No encontrada las reglas de canon, el proceso no puede continuar.", "", "OP_ESP");
          continue;
        }

        RunnableProcess<PartenonRecordBean> process = new RunnableProcess<PartenonRecordBean>(myObserver) {

          @Override
          public void run() {
            LOG.debug("[ProcesarDesglosesDbReader :: run] inicio.");
            try {
              Tmct0ord ord;
              Tmct0bok bok;
              boolean isRouting;

              Tmct0aloId alloId = null;
              Tmct0AloData alo = null;
              Tcli0comDTO cliCom;
              Tmct0Compensador cc;
              String corretajePti;
              Long idCamaraCompensacion;
              List<OperacionEspecialProcesarDesgloseRecordBean> operacionesByOrden;
              OperacionEspecialProcesarDesgloseRecordBean op;
              Tmct0AloDataOperacionesEspecialesPartenon data;
              List<DesgloseRecordDTO> desgloses;

              BigDecimal titulosPenditesAllo;
              Tmct0alo alloHibernate = aloBo.findByNuordenAndCdestadoasig(numOrden,
                  EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL.getId());
              if (alloHibernate == null) {
                LOG.info("[ProcesarDesglosesDbReader :: preExecuteTask]  buscando allo con mismo numero de titulos que el booking...");
                alloHibernate = aloBo.findByNuordenIgualTitulosBooking(numOrden);
              }

              if (alloHibernate != null) {
                alloId = alloHibernate.getId();
                LOG.info(
                    "[ProcesarDesglosesDbReader :: preExecuteTask] Encontrado AlloId : {} con mismos titulos que booking.",
                    alloId);
                if (alloHibernate != null) {

                  alo = new Tmct0AloData(alloHibernate, true);
                }
              }
              else {
                LOG.warn("[ProcesarDesglosesDbReader :: preExecuteTask] INCIDENCIA-NO encontrado AlloId con mismos titulos que booking.");
                logBo.insertarRegistroNewTransaction(numOrden, "",
                    "INCIDENCIA-NO encontrado AlloId con mismos titulos que booking.", "", "OP_ESP");
                return;
              }

              if (alo != null) {
                ord = ordBo.findById(numOrden);
                isRouting = ordBo.isRouting(ord);

                LOG.debug("[ProcesarDesglosesDbReader :: preExecuteTask] buscando ejealos del alo id {}...",
                    alo.getId());
                List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionAllocationDatas = ealoBo
                    .findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOConECCByAlloId(alo.getId());

                if (CollectionUtils.isNotEmpty(ejecucionAllocationDatas)) {
                  quitarTitulosEjeAloDesglosado(alo.getTmct0bokId(), ejecucionAllocationDatas);

                  LOG.debug("[ProcesarDesglosesDbReader :: preExecuteTask] encontrados  {} ejealos.",
                      ejecucionAllocationDatas.size());

                  data = new Tmct0AloDataOperacionesEspecialesPartenon(alo, ejecucionAllocationDatas);

                  titulosPenditesAllo = BigDecimal.ZERO;
                  for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO titulosDisponibles : ejecucionAllocationDatas) {
                    titulosPenditesAllo = titulosPenditesAllo.add(titulosDisponibles.getNutitulos());
                  }
                  data.setNumTitulos(titulosPenditesAllo);

                  LOG.debug(
                      "[ProcesarDesglosesDbReader :: preExecuteTask] buscando desgloses op especiales nuorden {}...",
                      numOrden);
                  try {
                    desgloses = getListaOperacionesEspecialesSinDesglosar(numOrden);
                  }
                  catch (SIBBACBusinessException e1) {
                    LOG.warn("[ProcesarDesglosesDbReader :: preExecuteTask] Incidencia recuperando los desgloses.", e1);
                    return;
                  }

                  operacionesByOrden = new ArrayList<OperacionEspecialProcesarDesgloseRecordBean>();
                  int countDesgloses = 0;
                  bok = bokBo.findById(alo.getTmct0bokId());
                  cliCom = tcli0comBo.findTcli0comByCdproducAndNuctapro("008",
                      Integer.parseInt(bok.getCdalias().trim()));
                  if (cliCom == null) {
                    cliCom = new Tcli0comDTO("", (short) 0, BigDecimal.ZERO, BigDecimal.ZERO);
                  }
                  for (DesgloseRecordDTO dto : desgloses) {
                    dto.setCanonesConfig(canonesConfig);
                    dto.setRouting(isRouting);
                    dto.setOperacionEspecial(true);

                    countDesgloses++;
                    LOG.trace("[ProcesarDesglosesDbReader :: preExecuteTask]  desglose Nº: {} bean {}", countDesgloses,
                        dto);

                    aloBo.recoverEconomicDataToDesgloseDto(data, dto);
                    aloBo.recoverTitlesFromExecutions(data, dto);

                    for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO eje : dto.getEjecucionesUtilizadas()) {
                      if (StringUtils.isNotBlank(eje.getMiembroDestino())) {
                        corretajePti = corretajePtiCompensadores.get(eje.getMiembroDestino().trim());
                        if (corretajePti == null) {
                          cc = compensadorBo.findByNbNombre(eje.getMiembroDestino());
                          if (cc != null && cc.getCorretajePti() != null) {
                            corretajePti = cc.getCorretajePti().toString();
                          }
                          else {
                            corretajePti = " ";
                          }
                          corretajePtiCompensadores.put(eje.getMiembroDestino().trim(), corretajePti);
                        }

                        eje.setCorretajePtiMiembroDestino(corretajePti);
                      }

                      if (StringUtils.isNotBlank(eje.getCdcamara())) {
                        Tmct0CamaraCompensacion camaraCompensacionHibernate = camaraCompensacionBo.findByCdCodigo(
                            cdcamarasByCdcodigo, eje.getCdcamara());
                        idCamaraCompensacion = camaraCompensacionHibernate.getIdCamaraCompensacion();

                        eje.setIdCamara(idCamaraCompensacion);
                      }
                    }// fin for ejecuciones

                    op = new OperacionEspecialProcesarDesgloseRecordBean(numOrden);
                    dto.setTclicomDto(cliCom);
                    op.setDesglose(dto);
                    operacionesByOrden.add(op);
                    // }
                    LOG.trace("[ProcesarDesglosesDbReader :: preExecuteTask]  desglose Nº: {} bean {}", countDesgloses,
                        op);
                    if (countDesgloses % 1000 == 0) {
                      LOG.info(
                          "[ProcesarDesglosesDbReader :: preExecuteTask] asignando ejecuciones del desglose orden : {}",
                          countDesgloses);
                    }
                  }// fin for
                  if (CollectionUtils.isNotEmpty(desgloses)) {
                    desgloses.clear();
                  }

                  if (data.getNumTitulos().compareTo(BigDecimal.ZERO) == 0) {
                    LOG.info("[ProcesarDesglosesDbReader :: preExecuteTask] El numero de titulos coincide con allo.");
                  }
                  else {
                    LOG.warn("[ProcesarDesglosesDbReader :: preExecuteTask] INCIDENCIA-El numero de titulos no coincide con allo.");
                    logBo.insertarRegistroNewTransaction(alloHibernate.getId().getNuorden(), alloHibernate.getId()
                        .getNbooking(), alloHibernate.getId().getNucnfclt(),
                        "INCIDENCIA-El numero de titulos no coincide con allo.", "", "MIDDLE");
                  }

                  operaciones.addAll(operacionesByOrden);
                  operacionesByOrden.clear();

                }
                else {
                  LOG.warn("[ProcesarDesglosesDbReader :: preExecuteTask] No hemos encontrado los grupos de ejecucion, operacionecc y movimientos que necesitamos. AllodID: {} "
                      + alloId);
                  logBo
                      .insertarRegistroNewTransaction(
                          alloHibernate.getId().getNuorden(),
                          alloHibernate.getId().getNbooking(),
                          alloHibernate.getId().getNucnfclt(),
                          "INCIDENCIA-No hemos encontrado los grupos de ejecucion, operacionecc y movimientos que necesitamos.",
                          "", "MIDDLE");
                }
              }
              else {// si alo es null
                LOG.warn(
                    "[ProcesarDesglosesDbReader :: preExecuteTask] El numero de orden : {} no tiene alos listos para desglosar.",
                    numOrden);
                logBo.insertarRegistroNewTransaction(numOrden, "", "INCIDENCIA-No tiene alos listos para desglosar.",
                    "", "MIDDLE");
              }
            }
            catch (Exception e) {
              myObserver.addErrorProcess(e);
            }
            finally {
              myObserver.sutDownThread(this);
            }
            LOG.debug("[ProcesarDesglosesDbReader :: run] fin.");
          }
        };

        if (operaciones.size() > maxExecutionPages * largeQueryPageSize) {
          LOG.info("[ProcesarDesglosesDbReader :: preExecuteTask] Hemos cargado suficientes desgloses. {}",
              operaciones.size());
          break;
        }
        else {

        }
        executeProcessInThread(myObserver, process);
        LOG.info("[ProcesarDesglosesDbReader :: preExecuteTask]  bean: {}", orden);

      }// fin for
    }
    catch (Exception e) {
      closeExecutorNow();
      getObserver().addErrorProcess(e);
    }
    finally {
      closeExecutor();
    }
    // TODO es por si no ha respetado el orden de insercion en la lista
    Collections.sort(operaciones, new Comparator<OperacionEspecialProcesarDesgloseRecordBean>() {

      @Override
      public int compare(OperacionEspecialProcesarDesgloseRecordBean arg0,
          OperacionEspecialProcesarDesgloseRecordBean arg1) {
        return arg0.getDesglose().getNumeroOrden().compareTo(arg1.getDesglose().getNumeroOrden());
      }
    });
    return operaciones;
  }

  private void quitarTitulosEjeAloDesglosado(Tmct0bokId bookId,
      List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> ejecucionAllocationDatas) {
    LOG.trace("[ProcesarDesglosesDbReader :: quitarTitulosEjeAloDesglosado] inicio");
    List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> titulosConsumidos = ealoBo
        .findAllTitulosDesglososadosEjeAloOperacionEspecialByBookId(bookId);

    if (CollectionUtils.isNotEmpty(titulosConsumidos)) {
      List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> toRemove = new ArrayList<EjecucionAlocationProcesarDesglosesOpEspecialesDTO>();
      for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO titulosConsumidosEje : titulosConsumidos) {
        for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO titulosDisponibles : ejecucionAllocationDatas) {
          if (titulosConsumidosEje.getTmct0ejeId().equals(titulosDisponibles.getTmct0ejeId())
              && titulosDisponibles.getNutitulos().compareTo(BigDecimal.ZERO) > 0) {
            LOG.info(
                "[ProcesarDesglosesDbReader :: quitarTitulosEjeAloDesglosado] Encontrados ejealos que ya se han desglosado {}.",
                titulosConsumidosEje);
            if (titulosDisponibles.getNutitulos().compareTo(titulosConsumidosEje.getNutitulos()) > 0) {
              titulosDisponibles.setNutitulos(titulosDisponibles.getNutitulos().subtract(
                  titulosConsumidosEje.getNutitulos()));
            }
            else {
              titulosConsumidosEje.setNutitulos(titulosConsumidosEje.getNutitulos().subtract(
                  titulosDisponibles.getNutitulos()));
              titulosDisponibles.setNutitulos(BigDecimal.ZERO);
              toRemove.add(titulosDisponibles);
            }
          }
        }
      }
      ejecucionAllocationDatas.removeAll(toRemove);
      toRemove.clear();
    }
    titulosConsumidos.clear();
    LOG.trace("[ProcesarDesglosesDbReader :: quitarTitulosEjeAloDesglosado] fin");
  }

  @Override
  public void postExecuteTask() {
    LOG.trace("[ProcesarDesglosesDbReader :: postExecuteTask] inicio");
    try {
      List<Tmct0bokId> bokIds = aloBo
          .findAllBookingIdContainsTmct0aloByCdestadoasig(EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE
              .getId());

      LOG.trace(
          "[ProcesarDesglosesDbReader :: postExecuteTask] encontrados books en estado EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE; {}",
          bokIds);
      if (CollectionUtils.isNotEmpty(bokIds)) {
        for (Tmct0bokId tmct0bokId : bokIds) {
          try {
            aloBo.revisarEstadosTratandoseOperacionesEspecialesNewTransaction(tmct0bokId);
          }
          catch (Exception e) {
            LOG.warn(
                "[ProcesarDesglosesDbReader :: postExecuteTask] INCIDENCIA- revisando estados al finalizar el desglose op. esp. {}",
                tmct0bokId, e);
          }
        }
      }
    }
    catch (Exception e) {
      LOG.warn(
          "[ProcesarDesglosesDbReader :: postExecuteTask] INCIDENCIA- query busqueda de bookngs en estado EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE. ",
          e);
    }

    LOG.trace("[ProcesarDesglosesDbReader :: postExecuteTask] fin");
  }

}
