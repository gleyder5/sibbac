package sibbac.business.wrappers.database.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class Tmct0bokDaoImp {
  private static final String[] queryUpdateCdestadocont = { "update tmct0bok set cdestadocont=", " where nbooking='", "' and nuorden='", "'"
  };

  @PersistenceContext
  private EntityManager em;

  public int updateCdestadocont(int estadocont, String nbooking, String nuorden) {
    return em.createNativeQuery(new StringBuffer(queryUpdateCdestadocont[0]).append(estadocont)
                                                                            .append(queryUpdateCdestadocont[1])
                                                                            .append(nbooking)
                                                                            .append(queryUpdateCdestadocont[2])
                                                                            .append(nuorden)
                                                                            .append(queryUpdateCdestadocont[3])
                                                                            .toString()).executeUpdate();
  } // updateCdestadocont
  
  /**
   * Establece los parametros en la query especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>List<Object[]> - </code>Lista de resultados devueltos por la query.
   * @author XI316153
   */
  @Transactional
  public List<Object[]> prepareAndExecuteNativeQuery(String sqlQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.getResultList();
  } // prepareAndExecuteQuery

  /**
   * Establece los parametros en la query de actualización especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>Integer - </code>Número de registros actualizados.
   * @author XI316153
   */
  @Modifying
  @Transactional
  public Integer prepareAndExecuteNativeUpdate(String sqlUpdateQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlUpdateQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.executeUpdate();
  } // prepareAndExecuteQuery
  
} // Tmct0bokDaoImp
