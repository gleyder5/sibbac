package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.bo.instrucciones.liquidacion.SettlementDataBo;
import sibbac.business.wrappers.database.dao.Tgrl0ordDao;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ord;

@Service
public class Tgrl0ordBo extends AbstractBo<Tgrl0ord, Integer, Tgrl0ordDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tgrl0ordBo.class);

  @Autowired
  private Tmct0brkBo brkBo;

  @Autowired
  private Tmct0xasBo xasBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tgrl0ejeBo ejeBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0dbsBo dbsBo;

  @Autowired
  private SettlementDataBo settlementDataBo;

  @Autowired
  private Tmct0afiBo afiBo;

  @Autowired
  private Tgrl0dirBo dirBo;

  @Autowired
  private Tmct0logBo logBo;

  public Tgrl0ordBo() {
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void mapearTablasAS400CargaInicialNoRouting(Tmct0bok bok, String auditUser) throws SIBBACBusinessException {

    // Tmct0bok bok = bokBo.findById(bookId);

    List<Tmct0alo> listAlos = aloBo.findAllByNuordenAndNbookingActivosInternacional(bok.getId().getNuorden(), bok
        .getId().getNbooking());

    if (CollectionUtils.isNotEmpty(listAlos)) {

      try {
        this.mapearTgrl0ordAndEjes(listAlos.get(0).getTmct0bok().getTmct0ord(), listAlos);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- haciendo el mapeo de las tablas del AS400, %s",
            e.getMessage()), e);
      }
      try {
        this.cargarTitularesInternacional(bok, listAlos, auditUser);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- cargando titulares internacional, %s",
            e.getMessage()), e);
      }
    }
    else {
      LOG.warn("[cargarTitularesInternacional] no tiene alos...");
    }
  }

  private void cargarTitularesInternacional(Tmct0bok bok, List<Tmct0alo> listAlos, String auditUser)
      throws SIBBACBusinessException {
    List<Holder> holders = null;
    LOG.debug("[cargarTitularesInternacional] desglosa el alias...");
    boolean desglosa = settlementDataBo.desglosaAliasInternacional(bok);
    if (!desglosa) {
      LOG.debug("[cargarTitularesInternacional] buscamos holders del alias {} NO DESGLOSA...", bok.getCdalias());
      // el primero de alta
      holders = settlementDataBo.getListHoldersInternacional(listAlos.get(0), desglosa);
    }

    bok.setIndatfis('S');

    for (Tmct0alo tmct0alo : listAlos) {
      if (desglosa) {
        LOG.debug("[cargarTitularesInternacional] buscamos holders del alias {} DESGLOSA...", tmct0alo.getCdalias());
        holders = settlementDataBo.getListHoldersInternacional(tmct0alo, desglosa);
      }
      this.cargarTitularesAloInternacional(tmct0alo, holders, auditUser);

      if (tmct0alo.getIndatfis() != null && tmct0alo.getIndatfis() != 'S') {
        bok.setIndatfis(tmct0alo.getIndatfis());
        bok.setFhaudit(new Date());
        bok.setCdusuaud(auditUser);
        bok.setErdatfis(tmct0alo.getErdatfis());
      }
      bokBo.save(bok);
    }
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public void mapearTablasAS400CargaInicialRouting(Tmct0bok bok, String auditUser) throws SIBBACBusinessException {

    List<Holder> holders = null;
    LOG.debug("[cargarTitularesInternacional] desglosa el alias...");
    // Tmct0bok bok = bokBo.findById(bookId);
    List<Tmct0alo> listAlos = aloBo.findAllByNuordenAndNbookingActivosInternacional(bok.getId().getNuorden(), bok
        .getId().getNbooking());

    if (CollectionUtils.isNotEmpty(listAlos)) {

      try {
        this.mapearTgrl0ordAndEjes(bok.getTmct0ord(), listAlos);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- haciendo el mapeo de las tablas del AS400, %s",
            e.getMessage()), e);
      }

      bok.setIndatfis('S');

      for (Tmct0alo tmct0alo : listAlos) {
        this.cargarTitularesAloInternacional(tmct0alo, holders, auditUser);
        if (tmct0alo.getIndatfis() != null && tmct0alo.getIndatfis() != 'S') {
          bok.setIndatfis(tmct0alo.getIndatfis());
          bok.setFhaudit(new Date());
          bok.setCdusuaud(auditUser);
          bok.setDsobserv(tmct0alo.getDsobserv());
        }
        bokBo.save(bok);
      }
    }
    else {
      LOG.warn("[cargarTitularesInternacional] no tiene alos...");
    }
  }

  private void cargarTitularesAloInternacional(Tmct0alo alo, List<Holder> holders, String auditUser)
      throws SIBBACBusinessException {
    LOG.debug("[cargarTitularesInternacional] cargando los titulares tgrl0dir y nom nuoprout: {} nupareje {}...",
        alo.getNuoprout(), alo.getNupareje());

    if (CollectionUtils.isEmpty(holders)) {
      LOG.warn("[cargarTitularesInternacional] No tiene titulares...");
      alo.setErdatfis("No holder data.");
      alo.setIndatfis('N');
      logBo.insertarRegistro(alo, new Date(), new Date(), "", alo.getTmct0sta().getDsestado(), "No holder data.",
          "SIBBAC20");
    }
    else {
      alo.setIndatfis('S');
      alo.setErdatfis("");
      afiBo.grabarAfisSinErroresInternacional(alo, holders, auditUser);

    }
    alo.setFhaudit(new Date());
    alo.setCdusuaud(auditUser);
    aloBo.save(alo);
  }

  private void mapearTgrl0ordAndEjes(Tmct0ord ord, List<Tmct0alo> listAlos) throws Exception {
    Tgrl0ord tgrl0ord = this.loadTgrl0ord(ord);
    for (Tmct0alo tmct0alo : listAlos) {
      ejeBo.loadTgrl0eje(tgrl0ord, tmct0alo);
    }
  }

  public void mapearTgrl0ordAndEjes(Tmct0bokId booking) throws Exception {
    Tgrl0ord ord = this.loadTgrl0ord(booking.getNuorden());
    List<Tmct0alo> listAlos = aloBo.findAllByNuordenAndNbookingActivosInternacional(booking.getNuorden(),
        booking.getNbooking());

    for (Tmct0alo tmct0alo : listAlos) {
      ejeBo.loadTgrl0eje(ord, tmct0alo);
    }
  }

  /**
   * GRABAR TGRL0ORD
   * 
   * @param ordernum numero de orden a contabilizar
   * @throws Exception
   * @generated
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tgrl0ord loadTgrl0ord(String ordernum) throws Exception {
    Tmct0ord tmct0ord = ordBo.findById(ordernum);
    return this.loadTgrl0ord(tmct0ord);
  }

  /**
   * GRABAR TGRL0ORD
   * 
   * @param ordernum numero de orden a contabilizar
   * @throws Exception
   * @generated
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tgrl0ord loadTgrl0ord(Tmct0ord tmct0ord) throws Exception {
    boolean isNew = false;
    try {

      // Order
      Tgrl0ord tgrl0ord = dao.findOne(tmct0ord.getNuoprout());

      // Extranjero
      if ('E' == tmct0ord.getCdclsmdo()) {
        // Executions
        Integer countAlosActivos = aloBo.countByNuordenActivosInternacional(tmct0ord.getNuorden());

        // Order
        if (tgrl0ord == null) {
          tgrl0ord = new Tgrl0ord();
          tgrl0ord.setNuoprout(tmct0ord.getNuoprout());
          mapping(tgrl0ord, tmct0ord);
          isNew = true;
        }
        if (countAlosActivos != null) {
          tgrl0ord.setNuejecuc(countAlosActivos.shortValue());
        }
        else {
          tgrl0ord.setNuejecuc((short) 0);
        }

        tgrl0ord = dao.save(tgrl0ord);
        dbsBo.insertRegister(tgrl0ord, isNew);
        return tgrl0ord;
        // Nacional
      }
      else {
        LOG.warn("NO HAY QUE HACER NADA EN LAS TGRL PARA NACIONAL.");
      }

    }

    catch (Exception e) {
      throw e;
    }
    return null;

  }

  @Transactional
  public void mapping(Tgrl0ord tgrl0ord, Tmct0ord tmct0ord) throws Exception {

    boolean foreigMarket = 'E' == tmct0ord.getCdclsmdo();
    /*
     * NUOPROUT N� Routing TMCT0ORD.NUOPROUT
     */
    tgrl0ord.setNuoprout(tmct0ord.getNuoprout());
    // Controlar si nuevo
    // if (!isNew)
    // loadFromPKFields();
    // NUCONTRA Nº Contrato Credito Ceros
    tgrl0ord.setNucontra(new BigDecimal(0));

    // CDREFCRE Referencia Credito Ceros
    tgrl0ord.setCdrefcre('0');

    // CDUSUARI Usuario TMCT0ORD.CDUSUARI
    tgrl0ord.setCdusuari(tmct0ord.getCdusuari().trim());

    // CDCLIENT Cliente (producto+cuenta) TMCT0ORD.CDCUENTA
    if (!foreigMarket) {
      try {
        String cdcuenta5 = "";
        if (tmct0ord.getCdcuenta().trim().length() > 5) {
          cdcuenta5 = tmct0ord.getCdcuenta().trim()
              .substring(tmct0ord.getCdcuenta().trim().length() - 4, tmct0ord.getCdcuenta().trim().length() + 1);
        }
        else {
          cdcuenta5 = tmct0ord.getCdcuenta().trim();
        }
        tgrl0ord.setCdclient(String.format("008%s", StringUtils.leftPad(cdcuenta5.trim(), 5, "0")));
      }
      catch (Exception e) {
        LOG.trace(e.getMessage(), e);
        tgrl0ord.setCdclient("008");
      }
    }
    else {
      tgrl0ord.setCdclient(tmct0ord.getCdcuenta().trim());
    }

    // CDISINVV Valor ISIN TMCT0ORD.CDISIN
    tgrl0ord.setCdisinvv(tmct0ord.getCdisin().trim());

    // NUSERIEV Serie Valor Blancos
    tgrl0ord.setNuseriev("");

    if (brkBo.isCdbrokerMTF(tmct0ord.getCdbroker())) {
      tgrl0ord.setCdbolsas("ME");
    }
    else {
      tgrl0ord.setCdbolsas(xasBo.getCdbrokerCdbolsas(tmct0ord.getCdbroker()));
    }
    // TPOPERAC Tipo de operacion TMCT0ORD.CDTPOPER
    tgrl0ord.setTpoperac(tmct0ord.getCdtpoper());

    // TPORDENS Tipo de orden TMCT0ORD.CDTIPORD Fichero Conversion
    tgrl0ord.setTpordens(xasBo.getCdtipordTpordens(tmct0ord.getCdtipord()));
    // TPMERCAD Mercado "X" -> Mercado Extranjero Mercado Nacional depende del

    // campo TMCT0ORD.CDMERCAD y se deber�
    // acceder al fichero conversi�n Fichero Conversion
    if (foreigMarket) {
      tgrl0ord.setTpmercad('X');
    }
    else {
      tgrl0ord.setTpmercad(xasBo.getCdmercadTpmercad(tmct0ord.getCdmercad()));
    }

    // CDLOTPIC Codigo Lotes/Picos Blancos
    tgrl0ord.setCdlotpic(' ');

    // FHRECORD F. recepcion orden TMCT0ORD.FEALTA (aaaammdd)
    tgrl0ord.setFhrecord(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(tmct0ord
        .getFealta())));

    // HORECORD H. recepcion orden TMCT0ORD.FEALTA (hhmmss)
    tgrl0ord.setHorecord(FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertTimeToString(tmct0ord
        .getHoalta())));

    // FHCONTRA F. Contratacion TMCT0ORD.FEEJECUC (aaaammdd)
    tgrl0ord.setFhcontra(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(tmct0ord
        .getFeejecuc())));

    // FHCANCEL F. Cancelacion Ceros
    tgrl0ord.setFhcancel(0);

    // HOCANCEL H. Cancelacion Ceros
    tgrl0ord.setHocancel(0);

    // FHPLAVAL F. Plazo validez TMCT0ORD.FEVALIDE (aaaammdd)
    tgrl0ord.setFhplaval(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(tmct0ord
        .getFevalide())));

    // TPCONTRA Tipo contratacion TMCT0ORD.CDTPCAMB
    tgrl0ord.setTpcontra(xasBo.getCdtpcambTpcontra(tmct0ord.getCdtpcamb()));

    if ('T' == tmct0ord.getCdtpcamb()) {
      // NUCANORD Cantidad ordenada TMCT0ORD.NUTITORD
      tgrl0ord.setNucanord(tmct0ord.getNutitord().multiply(tmct0ord.getImnomval()).setScale(2, RoundingMode.HALF_UP));

      // TOCANEJE Cantidad ejecutada TMCT0ORD.NUTITEJE
      tgrl0ord.setTocaneje(tmct0ord.getNutiteje().multiply(tmct0ord.getImnomval()).setScale(2, RoundingMode.HALF_UP));
    }
    else {
      // NUCANORD Cantidad ordenada TMCT0ORD.NUTITORD
      tgrl0ord.setNucanord(tmct0ord.getNutitord().setScale(2, RoundingMode.HALF_UP));

      // TOCANEJE Cantidad ejecutada TMCT0ORD.NUTITEJE
      tgrl0ord.setTocaneje(tmct0ord.getNutiteje().setScale(2, RoundingMode.HALF_UP));
    }

    // NUCANPDT Cantidad pendiente TMCTORD.NUTITORD - TMCTORD.NUTITEJE
    tgrl0ord.setNucanpdt(tmct0ord.getNutitord().subtract(tmct0ord.getNutiteje()).setScale(2, RoundingMode.HALF_UP));

    // NUCANCAN Cantidad cancelada Ceros
    tgrl0ord.setNucancan(new BigDecimal(0));

    // NUCANOCU Cantidad oculta Ceros
    tgrl0ord.setNucanocu(new BigDecimal(0));

    // NUCANMIN Cantidad minima Ceros
    tgrl0ord.setNucanmin(new BigDecimal(0));

    // TPCAMBIO Tipo cambio TMCT0ORD.CDTPCAMB Fichero Conversion
    tgrl0ord.setTpcambio(xasBo.getCdtpcambCdtpcambio(tmct0ord.getCdtpcamb()));

    // IMLIMCAM Limite cambio TMCT0ORD.IMLIMITE
    tgrl0ord.setImlimcam(tmct0ord.getImlimite().setScale(4, RoundingMode.HALF_UP));

    // IMCABSTO Cambio ON STOP Ceros
    tgrl0ord.setImcabsto(new BigDecimal(0));

    // CDOPEMER Operador mercado TMCT0EJE.CDUSUCON
    // List<Tmct0eje> tmct0ejeList =
    // JdoGetRelation.getListTmct0eje(tmct0ord.getNuorden(), pm);
    List<Tmct0bok> tmct0boks = tmct0ord.getTmct0boks();
    for (Tmct0bok tmct0bok : tmct0boks) {
      List<Tmct0eje> tmct0ejes = tmct0bok.getTmct0ejes();
      for (Tmct0eje tmct0eje : tmct0ejes) {
        tgrl0ord.setCdopemer(tmct0eje.getCdusucon().trim());
        break;
      }
      break;
    }

    // CDESTADO Estado orden "A" -> Orden dada de alta "B" -> Orden dada de baja
    tgrl0ord.setCdestado('A');

    // NUCEDENT Cedente credito Ceros
    tgrl0ord.setNucedent(new BigDecimal(0));

    // NUPARCAN Parcial cancelacion credito Ceros
    tgrl0ord.setNuparcan((short) 0);

    // CDGESTOR Codigo gestor Blancos
    tgrl0ord.setCdgestor("");

    // CDSISLIV Sistema de liquidacion
    tgrl0ord.setCdsisliv(' ');

    // CDCLASEV Clase valor
    tgrl0ord.setCdclasev("");

    // IMNOMVAL Nominal valor TMCT0ORD.IMNOMVAL
    tgrl0ord.setImnomval(tmct0ord.getImnomval().setScale(4, RoundingMode.HALF_UP));

    // PCCOMISN Porcentaje comision TMCT0ORD.POFPACLI
    tgrl0ord.setPccomisn(tmct0ord.getPofpacli().setScale(3, RoundingMode.HALF_UP));
    if (tgrl0ord.getPccomisn().abs().compareTo(new BigDecimal("999.999")) > 0) {
      tgrl0ord.setPccomisn(BigDecimal.ZERO);
    }

    // CDESTORD Situacion orden "E" -> Totalmente ejecutada "B" -> Baja "A" ->
    // Alta
    tgrl0ord.setCdestord('A');
    if (tgrl0ord.getNucanpdt().compareTo(BigDecimal.ZERO) == 0) {
      tgrl0ord.setCdestord('E');
    }

    // IMLCINTR Limite cambio int. en mercado Ceros
    tgrl0ord.setImlcintr(new BigDecimal(0));

    // CDORIGEN Codigo de origen TMCT0ORD.CDORIGEN Fichero Conversion
    // CDORIGEN Codigo de origen TMCT0ORD.CDORIGEN Fichero Conversion
    if (ordBo.isRouting(tmct0ord)) {
      String cdorigen = xasBo.getCdcuentaCdorigen(tmct0ord.getCdcuenta());

      if (StringUtils.isBlank(cdorigen)) {
        cdorigen = xasBo.getCdclenteCdorigen(tmct0ord.getCdclente());

        if (StringUtils.isBlank(cdorigen)) {
          cdorigen = xasBo.getCdorigenCdorigen(tmct0ord.getCdorigen());
        }
      }
      tgrl0ord.setCdorigen(cdorigen.trim());
    }
    else {
      try {
        String cdorigen = xasBo.getCdorigenCdorigen(tmct0ord.getCdorigen());
        tgrl0ord.setCdorigen(cdorigen);
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando el CDORIGEN de CDORIGEN: '%s'. %s",
            tgrl0ord.getCdorigen(), e.getMessage()), e);
      }
    }
    // CDDERECH Codigo de derechos TMCT0ORD.XTDERECH
    tgrl0ord.setCdderech(tmct0ord.getCdindgas());

    // CDCOMISN Codigo de comision "P" -> Mercado Extranjero TCLI0COM.CDCOMISN
    // -> Mercado Nacional A TCLI0COM se
    // accede por campo CDCLIENT
    if (foreigMarket) {
      tgrl0ord.setCdcomisn('P');
    }
    else {
      tgrl0ord.setCdcomisn(' ');

      if (tgrl0ord.getCdcomisn() == ' ') {
        tgrl0ord.setCdcomisn('S'); // Defecto
      }
      if (tgrl0ord.getCdcomisn() == 'O') {
        tgrl0ord.setCdcomisn('S'); // Defecto
      }
    }

    // TOCANCOM Cantidad Titulos comunicada Stratus TMCT0ORD.NUTITEJE
    tgrl0ord.setTocancom(tmct0ord.getNutiteje().setScale(2, RoundingMode.HALF_UP));

    // TPSUBMER Tipo Submercado "EX" -> Mercado Extranjero Para mercado nacional
    // se tomara de TMCT0EJE.CDMERCAD y se
    // ira al fichero de conversion Fichero Conversion
    if (foreigMarket) {
      tgrl0ord.setTpsubmer("EX");
    }
    else {
      tgrl0ord.setTpsubmer(xasBo.getCdmercadTpsubmer(tmct0ord.getCdmercad()));
    }

    // NUOPCLTE Nº Operacion Cliente TMCT0ORD.NUREFORD
    tgrl0ord.setNuopclte(StringUtils.substring(tmct0ord.getNureford(), 0, 16).trim());

    // CDDIVISA Codigo de Divisa TMCT0ORD.CDMONISO
    tgrl0ord.setCddivisa(tmct0ord.getCdmoniso().trim());

    // CDWAREHO Codigo de WareHouse "N"
    tgrl0ord.setCdwareho('N');

    // FHSOLCAN F. Solicitud Cancelacion Ceros
    tgrl0ord.setFhsolcan(0);

    // HOSOLCAN H. Solicitud Cancelacion Ceros
    tgrl0ord.setHosolcan(0);
    tgrl0ord.setTporigen("");
    if (tgrl0ord.getCdorigen() != null && !tgrl0ord.getCdorigen().trim().isEmpty()) {
      try {
        List<String> tporigenList = dao.findAllTporigenTval0corByCdorigen(tgrl0ord.getCdorigen());
        if (tporigenList != null && tporigenList.size() > 0) {
          final String tporigen = tporigenList.get(0);
          tgrl0ord.setTporigen(tporigen.trim());
        }
      }
      catch (Exception e) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA-Recuperando el TPORIGEN de CDORIGEN: '%s'. %s",
            tgrl0ord.getCdorigen(), e.getMessage()), e);
      }
    }
    else {
      tgrl0ord.setTporigen("");
    }

    // CDCAMMED Codigo Cambio Medio "N"
    tgrl0ord.setCdcammed('N');

    // CDCANENT Codigo Canal de entrada TMCT0ORD.CDCANAL Fichero Conversion
    tgrl0ord.setCdcanent(xasBo.getCdcanalCdcanent(tmct0ord.getCdcanal()));

    // TPRESNEG Tipo Restriccion Negociacion "000"
    tgrl0ord.setTpresneg("000");

    // TPEJECUC Tipo Restriccion Ejecucion "000"
    tgrl0ord.setTpejecuc("000");

  }
}
