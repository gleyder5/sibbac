package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.S3LiquidacionDao;
import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.S3Liquidacion;
import sibbac.database.bo.AbstractBo;

@Service
public class S3LiquidacionBo extends AbstractBo<S3Liquidacion, Long, S3LiquidacionDao> {

  public List<S3Liquidacion> findByidFichero(String idFichero) {
    return dao.findByidFichero(idFichero);
  }

  public List<S3Liquidacion> findByidFicheroOrdAudit_date(String idFichero) {
    return dao.findByidFicheroOrdAudit_date(idFichero);
  }

  public List<S3Liquidacion> findByNbookingAndNucnflictAndNcnfliq(String nbooking, String nucnfclt, short ncnfliq) {
    return dao.findByNbookingAndNucnflictAndNcnfliq(nbooking, nucnfclt, ncnfliq);
  }

  public List<S3Liquidacion> findByNetting(Netting netting) {
    return dao.findByNettingOrder(netting);
  }

  public List<S3Liquidacion> findByNettingOrdAudit_date(Netting netting) {
    return dao.findByNettingOrderAudit_date(netting);
  }
  
  public List<S3Liquidacion> findByReferenciaS3(String referenciaS3) {
    return dao.findByReferenciaS3(referenciaS3);
  }

}
