package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0pkd;
import sibbac.business.wrappers.database.model.Tmct0pkdId;

@Repository
public interface Tmct0pkdDao extends JpaRepository<Tmct0pkd, Tmct0pkdId> {

}
