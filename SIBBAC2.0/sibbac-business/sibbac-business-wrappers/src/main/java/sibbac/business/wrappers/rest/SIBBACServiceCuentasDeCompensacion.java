package sibbac.business.wrappers.rest;

import java.io.Serializable;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.MercadoBo;
import sibbac.business.wrappers.database.bo.SistemaLiquidacionBo;
import sibbac.business.wrappers.database.bo.TipoCuentaConciliacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentaLiquidacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0EntidadRegistroBo;
import sibbac.business.wrappers.database.bo.Tmct0TipoCuentaDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0TipoNeteoBo;
import sibbac.business.wrappers.database.data.CompensadorData;
import sibbac.business.wrappers.database.data.CuentaDeCompensacionData;
import sibbac.business.wrappers.database.data.CuentaLiquidacionData;
import sibbac.business.wrappers.database.data.TipoCuentaConciliacionData;
import sibbac.business.wrappers.database.data.TipoCuentaDeCompensacionData;
import sibbac.business.wrappers.database.data.TipoNeteoData;
import sibbac.business.wrappers.database.model.SistemaLiquidacion;
import sibbac.business.wrappers.database.model.TipoCuentaConciliacion;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentaLiquidacion;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;
import sibbac.business.wrappers.database.model.Tmct0Mercado;
import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

/**
 * The Class SIBBACServiceCuentaLiquidacion.
 */
@SIBBACService
public class SIBBACServiceCuentasDeCompensacion implements SIBBACServiceBean {

    private static final String TAG = SIBBACServiceCuentasDeCompensacion.class
	    .getName();

    /** The Constant CMD_GET_CUENTA_LIQUIDACION. */
    private static final String CMD_GET_CUENTA_LIQUIDACION = "getCuentaLiquidacion";
    /** Obtiene Todas las cuentas de compensacion **/
    private static final String CMD_GET_CUENTAS_COMPENSACION = "getCuentasDeCompensacion";
    /** The Constant CMD_ADD_CUENTA_LIQUIDACION. */
    private static final String CMD_ADD_CUENTA_COMPENSACION = "addCuentaCompensacion";

    /** The Constant CMD_GET_TIPO_CUENTA. */
    private static final String CMD_GET_TIPO_CUENTA = "getTipoCuenta";

    /** The Constant CMD_GET_TIPO_NETEO. */
    private static final String CMD_GET_TIPO_NETEO = "getTipoNeteo";

    /** The Constant CMD_GET_CUENTA_RELACIONADA. */
    private static final String CMD_GET_CUENTA_RELACIONADA = "getCuentaRelacionada";
    /** The Constant CMD_GET_SISTEMAS_LIQUIDACION. */
    private static final String CMD_GET_SISTEMAS_LIQUIDACION = "getSistemasLiquidacion";

    /** The Constant CMD_GET_TIPO_FECHA. */
    private static final String CMD_GET_TIPO_FICHERO = "getTipoFichero";

    /** The Constant CMD_GET_TIPO_FECHA. */
    private static final String CMD_GET_CUENTA_COMPENSACION = "getCuentaCompensacion";
    /** The Constant CMD_GET_TIPO_FECHA. */
    private static final String CMD_GET_TIPO_ER = "getTipoER";
    /** Obtiene las relaciones entre cuentas de liquidacion y mercados */
    private static final String CMD_GET_LINKED_CUENTA_MERCADO = "getCuentasMercado";
    /** Compensadores **/
    private static final String CMD_GET_LISTA_COMPENSADORES = "getListaCompensadores";
    /** Tipo Cuenta Compensacion **/
    private static final String CMD_GET_LISTA_TIPO_CUENTA_COMPENSACION = "getTiposCuentaCompensacion";
    // RESULTADOS CONSTANTS
    private static final String RESULT_CUENTAS_LIQUIDACION = "result_cuentas_liquidacion";
    private static final String RESULT_CUENTAS_MERCADOS = "result_cuentas_mercados";
    private static final String RESULT_CUENTAS_COMPENSACION = "result_cuentas_compensacion";
    private static final String RESULT_SISTEMAS_LIQUIDACION = "result_sistemas_liquidacion";

    // util
    // private ConciliacionUtil util = ConciliacionUtil.getInstance();

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    // Servicios
    @Autowired
    private Tmct0CuentaLiquidacionBo bo;
    @Autowired
    private Tmct0TipoNeteoBo tpNeteoBo;
    @Autowired
    private TipoCuentaConciliacionBo tipoCuentaBo;
    // @Autowired
    // private RelacionCuentaCompMercadoBo relacionCuentaMercadoBo;
    @Autowired
    private MercadoBo mercadoBo;
    @Autowired
    private Tmct0CuentasDeCompensacionBo ccBo;
    @Autowired
    private Tmct0EntidadRegistroBo entidadRegistroBo;
    @Autowired
    private Tmct0CompensadorBo compensadorBo;
    @Autowired
    private Tmct0TipoCuentaDeCompensacionBo tipoCuentaCompensacionBo;
    @Autowired
    private SistemaLiquidacionBo sistemaBo;
    @Autowired
    private SistemaLiquidacionBo sistemaLiquidacionBo;

    /*
     * (non-Javadoc)
     * 
     * @see
     * sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans.
     * WebRequest)
     */
    @Override
    public WebResponse process(WebRequest webRequest)
	    throws SIBBACBusinessException {

	WebResponse webResponse = new WebResponse();
	String prefix = "[SIBBACServiceCuentaLiquidacion ::process] ";
	String command = webRequest.getAction();
	Map<String, Object> result = null;
	Map<String, String> params;

	if (webRequest.getFilters() != null) {
	    params = webRequest.getFilters();
	} else {
	    params = new HashMap<String, String>();
	}
	Map<String, Serializable> filters = normalizeParams(params);
	if (command == null) {
	    command = "";
	}

	LOG.debug(prefix + "Processing a web request...");
	LOG.debug(prefix + "+ Command.: [{}]", command);

	switch (command) {
	case CMD_GET_CUENTAS_COMPENSACION:
	    result = getCuentasDeCompensacion(filters);
	    break;
	case CMD_GET_CUENTA_LIQUIDACION:
	    result = getCuentaLiquidacion(filters);
	    break;
	case CMD_ADD_CUENTA_COMPENSACION:
	    try {
		result = addCuentaCompensacion(filters);
	    } catch (NumberFormatException | ClassCastException
		    | SQLIntegrityConstraintViolationException
		    | PersistenceException ex) {
		LOG.error(ex.getMessage(), ex);
		webResponse.setError(ex.getMessage());
	    }
	    break;
	case CMD_GET_TIPO_CUENTA:
	    result = getTipoCuenta(params);
	    break;
	case CMD_GET_TIPO_NETEO:
	    result = getTipoNeteo(params);
	    break;
	case CMD_GET_CUENTA_RELACIONADA:
	    result = getCuentaRelacionada(params);
	    break;
	case CMD_GET_TIPO_FICHERO:
	    result = getTipoFichero(params);
	    break;
	case CMD_GET_CUENTA_COMPENSACION:
	    result = getCuentaCompensacion(params);
	    break;
	case CMD_GET_TIPO_ER:// se usa directamente la del servicio
	    // EntidadRegistro
	    result = getTipoER(params);
	    break;
	case CMD_GET_LINKED_CUENTA_MERCADO:
	    result = getCuentasMercado(params);
	    break;
	case CMD_GET_LISTA_COMPENSADORES:
	    result = getListaCompensadores();
	    break;
	case CMD_GET_LISTA_TIPO_CUENTA_COMPENSACION:
	    result = getTiposCuentaCompensacion();
	    break;
	case CMD_GET_SISTEMAS_LIQUIDACION:
	    result = getSistemasLiquidacion();
	    break;
	}

	if (result == null) {
	    result = new HashMap<String, Object>();
	}
	webResponse.setResultados(result);

	return webResponse;
    }

    private Map<String, Object> getTiposCuentaCompensacion() {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	List<TipoCuentaDeCompensacionData> resultList = tipoCuentaCompensacionBo
		.findAllData();
	resultMap.put("result_tipos_cuenta_compensacion", resultList);
	return resultMap;
    }

    private Map<String, Object> getListaCompensadores() {
	Map<String, Object> resultMap = new HashMap<String, Object>();
	List<CompensadorData> resultList = compensadorBo.findAllData();
	resultMap.put("result_lista_compensadores", resultList);
	return resultMap;
    }

    /**
     * Convierte el tipo de los valores de los filtros a tipos Serializables
     * 
     * @param params
     * @return
     */
    private Map<String, Serializable> normalizeParams(Map<String, String> params) {
	Map<String, Serializable> result = new HashMap<String, Serializable>();
	for (Map.Entry<String, String> entry : params.entrySet()) {
	    if (entry.getValue() != null && !"".equals(entry.getValue())
		    && !entry.getValue().equals("0")) {
		if (entry.getKey().equals("idMercado")
			|| entry.getKey().equals("idsCuentaCompensacion")
			|| entry.getKey().equals("tmct0CuentaLiquidacion.id")) {
		    String[] ids = entry.getValue().split(",");
		    List<Long> idList = new LinkedList<Long>();
		    for (String strId : ids) {
			try {
			    parseLong(idList, strId);
			} catch (NumberFormatException ex) {
			    LOG.error(ex.getMessage(), ex);
			}
		    }
		    result.put(entry.getKey(), (Serializable) idList);
		} else if (entry.getKey().contains(".id")
			|| entry.getKey().startsWith("id")) {
		    result.put(entry.getKey(), Long.valueOf(entry.getValue()));
		} else if (entry.getKey().equals("frecuenciaEnvioExtracto")
			|| entry.getKey().startsWith("id")) {
		    long frec = 0L;
		    try {
			frec = Long.parseLong(entry.getValue().toString().trim());
		    } catch (NumberFormatException ex) {
			LOG.error(ex.getMessage(), ex);
		    }
		    result.put(entry.getKey(), frec);
		}else if(entry.getKey().equals("numCuentaER")){
		    long numCtaEr = 0L;
		    try {
			numCtaEr = Long.parseLong(entry.getValue().toString().trim());
		    } catch (NumberFormatException ex) {
			LOG.error(ex.getMessage(), ex);
		    }
		    result.put(entry.getKey(), numCtaEr);
		} else {
		    result.put(entry.getKey(), entry.getValue());
		}
	    }
	}
	return result;
    }

    private void parseLong(List<Long> idList, String strId)
	    throws NumberFormatException {
	long id = Long.parseLong(strId.trim());
	idList.add(id);
    }

    /**
     * Gets the tipo fecha.
     *
     * @param params
     *            the params
     * @return the tipo fecha
     */
    private Map<String, Object> getTipoFichero(Map<String, String> params) {

	Map<String, Object> result = new HashMap<String, Object>();
	String[] tFechas = { "norma 43", "a medida" };
	result.put("result_tipo_fichero", tFechas);
	return result;
    }

    /**
     * Gets the cuenta relacionada.
     *
     * @param params
     *            the params
     * @return the cuenta relacionada
     */
    private Map<String, Object> getCuentaRelacionada(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<CuentaDeCompensacionData> cuentas = ccBo.findAllData();
	result.put(RESULT_CUENTAS_COMPENSACION, cuentas);
	return result;
    }

    /**
     * Gets the tipo neteo.
     *
     * @param params
     *            the params
     * @return the tipo neteo
     */
    private Map<String, Object> getTipoNeteo(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<TipoNeteoData> neteos = tpNeteoBo.findAllData();

	result.put("result_tipo_Neteo", neteos);
	return result;
    }

    /**
     * Gets the tipo cuenta.
     *
     * @param params
     *            the params
     * @return the tipo cuenta
     */
    private Map<String, Object> getTipoCuenta(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<TipoCuentaConciliacionData> tipos = tipoCuentaBo.findAllData();
	result.put("result_tipo_cuenta", tipos);
	return result;
    }

    /**
     * Adds the cuenta liquidacion.
     *
     * @param params
     *            the params
     * @return the map
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> addCuentaCompensacion(
	    Map<String, Serializable> params) throws NumberFormatException,
	    ClassCastException, SQLIntegrityConstraintViolationException,
	    PersistenceException {
	Map<String, Object> result = new HashMap<String, Object>();
	if (params.size() > 0) {
	    Tmct0CuentasDeCompensacion cc;
	    if (params.get("idCtaCompensacion") != null
		    && !params.get("idCtaCompensacion").equals("")) {
		long idCtaComp = Long.parseLong(params.get("idCtaCompensacion")
			.toString().trim());
		cc = ccBo.findByIdCuentaCompensacion(idCtaComp);
	    } else {
		cc = new Tmct0CuentasDeCompensacion();
	    }
	    List<Tmct0Mercado> mercados = new LinkedList<Tmct0Mercado>();
	    Tmct0CuentaLiquidacion cl = null;
	    for (Map.Entry<String, Serializable> entry : params.entrySet()) {
		if (entry.getValue() != null && !"".equals(entry.getKey())) {
		    final String nameParam = entry.getKey();
		    final Serializable valor = entry.getValue();
		    switch (nameParam) {
		    case ("frecuenciaEnvioExtracto"):
			cc.setFrecuenciaEnvioExtracto((long) valor);
			break;
		    case ("entidadRegistro"):
			long idER = Long.parseLong(valor.toString().trim());
			Tmct0EntidadRegistro er = entidadRegistroBo
				.findById(idER);
			cc.setEntidadRegistro(er);
			break;
		    case ("idMercado"):
			mercados = mercadoBo.findByIdIn((List<Long>) valor);
			break;
		    case ("numCuentaER"):
			long numCta = 0L;
			numCta = Long.parseLong(valor.toString().trim());
			cc.setNumCuentaER(numCta);
			break;
		    case ("esCuentaPropia"):
			// por el campo esCuentaPropia se sabe tambien el tipo
			// de cuenta ya que contiene el id del tipo de cuenta,
			// el cual si
			// tiene el valor 1 es propia
			if (valor.equals("1")) {
			    cc.setEsCuentaPropia(true);
			} else {
			    cc.setEsCuentaPropia(false);
			}
			long idTp = Long.valueOf(valor.toString().trim());
			TipoCuentaConciliacion tpCta = tipoCuentaBo
				.findById(idTp);
			cc.setTipoCuentaConciliacion(tpCta);
			break;
		    case ("tipoNeteoTitulo"):
			long idTpNto = Long.parseLong(valor.toString().trim());
			Tmct0TipoNeteo tpNto = tpNeteoBo.findById(idTpNto);
			cc.setTipoNeteoTitulo(tpNto);
			break;
		    case "tipoNeteoEfectivo":
			long idTpNtoEf = Long
				.parseLong(valor.toString().trim());
			Tmct0TipoNeteo tpNtoEf = tpNeteoBo.findById(idTpNtoEf);
			cc.setTipoNeteoEfectivo(tpNtoEf);
			break;
		    case "cdCodigo":
			String cdCodigo = valor.toString().trim();
			cc.setCdCodigo(cdCodigo);
			break;
		    case "tipoFicheroConciliacion":
			cc.setTipoFicheroConciliacion(valor.toString());
			break;
		    case "idCuentaLiquidacion":
			long idCl = Long.parseLong(valor.toString());
			cl = bo.findById(idCl);
			cc.setTmct0CuentaLiquidacion(cl);
			break;
		    case "idCompensador":
			long idCmp = Long.parseLong(valor.toString());
			Tmct0Compensador cmp = compensadorBo.findById(idCmp);
			cc.setTmct0Compensador(cmp);
			break;
		    case "idTipoCuentaComp":
			long idTpCtaCmp = Long.parseLong(valor.toString());
			Tmct0TipoCuentaDeCompensacion tpCtaCmp = tipoCuentaCompensacionBo
				.findById(idTpCtaCmp);
			cc.setTmct0TipoCuentaDeCompensacion(tpCtaCmp);
			break;
		    case "idTipoCuentaConciliacion":
			long idTpCta = Long.parseLong(valor.toString());
			TipoCuentaConciliacion tc = tipoCuentaBo
				.findById(idTpCta);
			cc.setTipoCuentaConciliacion(tc);
			break;
		    case "esClearing":
			cc.setCuentaClearing(valor.toString().trim().charAt(0));
			break;
		    case "subcustodio":
			cc.setSubcustodioDomestico(valor.toString());
			break;
		    case "numCtaSubcustodio":
			cc.setNumeroCuentaSubcustodio(valor.toString());
			break;
		    case "idSistemaLiquidacion":
			long idSL = Long.parseLong(valor.toString().trim());
			SistemaLiquidacion sl = sistemaLiquidacionBo
				.findById(idSL);
			cc.setSistemaLiquidacion(sl);
			break;
		    case "cdCuentaCompensacion":
			String cdCuentaCompensacion = valor.toString().trim();
			cc.setCdCuentaCompensacion(cdCuentaCompensacion);
			break;
		    case "codigoS3":
			String codS3 = valor.toString();
			cc.setCdCodigoS3(codS3);
			break;
		    }
		}
	    }
	    try {
		cc = ccBo.save(cc);
		guardarMercados(mercados, cc);

	    } catch (Exception ex) {
		LOG.error(ex.getMessage(), ex);
	    }
	    /**
	     * En principio las cuentas de liquidacion se crearan primero y
	     * luego en la pantalla del CRUD de cuentas de compesnacion de la
	     * fase1 se crearan las ctas de compensacion y se le añadian las de
	     * liquidacion
	     **/
	    // guardarCuentasDeCompensacion(ccList, cl);
	}
	return result;
    }

    private void guardarMercados(List<Tmct0Mercado> mercados,
	    Tmct0CuentasDeCompensacion cc) {
	if (!CollectionUtils.isEmpty(cc.getMercados())) {
	    for (int i = 0; i < cc.getMercados().size(); i++) {
		cc.getMercados().remove(i);
	    }
	    ccBo.save(cc);
	}
	cc.setMercados(mercados);
	ccBo.save(cc);
    }

    @SuppressWarnings("unused")
    private void guardarCuentasDeCompensacion(
	    List<Tmct0CuentasDeCompensacion> ccList, Tmct0CuentaLiquidacion cl)
	    throws Exception {
	for (Tmct0CuentasDeCompensacion cc : ccList) {
	    cc.setTmct0CuentaLiquidacion(cl);
	}
	ccBo.save(ccList);
    }

    /**
     * Gets the cuenta liquidacion.
     *
     * @param filters
     *            the params
     * @return the cuenta liquidacion
     */
    private Map<String, Object> getCuentaLiquidacion(
	    Map<String, Serializable> filters) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<CuentaLiquidacionData> listResult = bo.findAllFiltered(filters);
	result.put(RESULT_CUENTAS_LIQUIDACION, listResult);
	return result;
    }

    private Map<String, Object> getCuentasDeCompensacion(
	    Map<String, Serializable> filters) {
	Map<String, Object> result = new HashMap<String, Object>();
	List<CuentaDeCompensacionData> resultList = ccBo.findAllData(filters);
	result.put(RESULT_CUENTAS_COMPENSACION, resultList);
	return result;
    }

    /**
     * Gets the tipo fecha.
     *
     * @param params
     *            the params
     * @return the tipo fecha
     */
    private Map<String, Object> getCuentaCompensacion(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	String[] cc = { "5421456452145642", "5421567861456654",
		"5478964521456654", "5523652656145675" };
	result.put("result_cuentas_compensatorias", cc);
	return result;
    }

    /**
     * Obtine tipos Entidad Registro
     * 
     * @param params
     * @return
     */
    private Map<String, Object> getTipoER(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	String[] tER = { "DCV", "ECC", "Custodio" };
	result.put("result_tipo_ER", tER);
	return result;
    }

    private Map<String, Object> getCuentasMercado(Map<String, String> params) {
	Map<String, Object> result = new HashMap<String, Object>();
	result.put(RESULT_CUENTAS_MERCADOS, mercadoBo.findAllData());
	return result;
    }

    private Map<String, Object> getSistemasLiquidacion() {
	Map<String, Object> result = new HashMap<String, Object>();
	result.put(RESULT_SISTEMAS_LIQUIDACION, sistemaBo.findAll());
	return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getAvailableCommands()
     */
    @Override
    public List<String> getAvailableCommands() {
	return Arrays.asList(new String[] { CMD_GET_CUENTA_LIQUIDACION,
		CMD_ADD_CUENTA_COMPENSACION, CMD_GET_CUENTAS_COMPENSACION,
		CMD_GET_TIPO_CUENTA, CMD_GET_TIPO_NETEO,
		CMD_GET_CUENTA_RELACIONADA, CMD_GET_TIPO_FICHERO,
		CMD_GET_CUENTA_COMPENSACION, CMD_GET_TIPO_ER,
		CMD_GET_LINKED_CUENTA_MERCADO, CMD_GET_LISTA_COMPENSADORES,
		CMD_GET_LISTA_TIPO_CUENTA_COMPENSACION });
    }

    /*
     * (non-Javadoc)
     * 
     * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
     */
    @Override
    public List<String> getFields() {
	return Arrays.asList(new String[] {});
    }

}
