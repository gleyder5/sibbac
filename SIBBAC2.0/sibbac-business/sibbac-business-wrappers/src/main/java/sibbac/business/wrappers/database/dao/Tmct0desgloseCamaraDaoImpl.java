package sibbac.business.wrappers.database.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.data.Tmct0desgloseCamaraData;

@Repository
public class Tmct0desgloseCamaraDaoImpl {

  @PersistenceContext
  private EntityManager em;

  private static final String TAG = Tmct0desgloseCamaraDaoImpl.class.getName();
  private static final Logger LOG = LoggerFactory.getLogger(TAG);
  private static final String[] queryUpdateCdestadocont = { "update Tmct0desglosecamara set cdestadocont=", " where iddesglosecamara="
  };

  /**
   * Establece los parametros en la query especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>List<Object[]> - </code>Lista de resultados devueltos por la query.
   * @author XI316153
   */
  @Transactional
  public List<Object[]> prepareAndExecuteNativeQuery(String sqlQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.getResultList();
  } // prepareAndExecuteQuery

  /**
   * Establece los parametros en la query de actualización especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>Integer - </code>Número de registros actualizados.
   * @author XI316153
   */
  @Modifying
  @Transactional
  public Integer prepareAndExecuteNativeUpdate(String sqlUpdateQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlUpdateQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.executeUpdate();
  } // prepareAndExecuteQuery

  @SuppressWarnings("unchecked")
  /**
   * Obtiene una lista de objetos desgloseCamara con el sumatorio de Efectivos de la coleccion asociada
   * desgloseclitit, los objetos desgloseCamara, tienen que estar asociados a una cuenta de liquidacion
   * @param sentido
   * @param estado
   * @param filtros
   * @return
   */
  public List<Tmct0desgloseCamaraData> getDesglosesCamaraBySentidoAndEstado(char sentido,
                                                                            int estado1,
                                                                            int estado2,
                                                                            Map<String, Serializable> filtros) {
    String prefix = "[" + TAG + "::getDesglosesCamaraBySentidoAndEstado] ";
    List<Tmct0desgloseCamaraData> resultList = new LinkedList<>();
    String selectSumEfectivoClitit = "SELECT sum(ct.imefectivo) FROM Tmct0desgloseclitit ct WHERE ct.tmct0desglosecamara.iddesglosecamara = dc.iddesglosecamara ";
    // StringBuilder selectCtaCompInCtaLiqu = new StringBuilder(
    // "SELECT cl.tmct0CuentasDeCompensacion.cdCodigo FROM Tmct0CuentaLiquidacion cl WHERE cl.tmct0CuentasDeCompensacion.cuentaClearing = 'S' "
    // );

    /*
     * StringBuilder selectCtaCompInCtaLiqu = new StringBuilder(
     * "SELECT cc.cdCodigo FROM Tmct0CuentaLiquidacion cl, Tmct0CuentasDeCompensacion cc WHERE cc.cuentaClearing = 'S' AND cc.tmct0CuentaLiquidacion.id = cl.id "
     * + "AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo !='CI' " +
     * "AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo != 'CP' " );
     */

    StringBuilder selectCtaCompInCtaLiqu = new StringBuilder(
                                                             "SELECT cc.cdCodigo FROM Tmct0CuentaLiquidacion cl JOIN cl.tmct0CuentasDeCompensacionList cc WHERE cc.cuentaClearing = 'S' AND cc.tmct0CuentaLiquidacion.id = cl.id "
                                                                 + "AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo !='CI' "
                                                                 + "AND cc.tmct0TipoCuentaDeCompensacion.cdCodigo != 'CP' ");

    String fcontratacionBtw = "AND dc.fecontratacion BETWEEN :fcontratacionDe AND :fcontratacionA ";
    String estadoOr = "(dc.tmct0estadoByCdestadoasig.idestado = :estado1 OR dc.tmct0estadoByCdestadoasig.idestado = :estado2) ";

    StringBuilder selectSb = new StringBuilder(
                                               "SELECT new sibbac.business.wrappers.database.data.Tmct0desgloseCamaraData(dc.iddesglosecamara as idDesgloseCamara, ").append("(")
                                                                                                                                                                     .append(selectSumEfectivoClitit)
                                                                                                                                                                     .append(") as efectivo, ")
                                                                                                                                                                     .append("dc.isin as isin, ")
                                                                                                                                                                     .append("dc.tmct0infocompensacion.cdctacompensacion as codCtaDeCompensacion, ")
                                                                                                                                                                     .append("alc.id as idAlc, ")
                                                                                                                                                                     .append("alc.imcanoncompeu as imcanoncompeu, ")
                                                                                                                                                                     .append("alc.imcanoncontreu as imcanoncontreu, ")
                                                                                                                                                                     .append("alc.imcanonliqeu as imcanonliqeu, ")
                                                                                                                                                                     .append("alc.imcomisn) ")
                                                                                                                                                                     .append("FROM Tmct0desglosecamara dc JOIN dc.tmct0alc alc ");

    StringBuilder where = new StringBuilder("WHERE ").append(estadoOr).append("AND ").append("dc.sentido =:sentido ");

    // se añaden restricciones segun los parametros recibidos
    if (filtros != null) {

      for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
        if (entry.getValue() == null || "".equals(entry.getValue().toString()) || entry.getKey() == null
            || "".equals(entry.getKey())) {
          continue;
        }
        if ((!entry.getKey().equals("fcontratacionDe") && !entry.getKey().equals("fcontratacionA") && !entry.getKey()
                                                                                                            .trim()
                                                                                                            .equals("codCtaDeCompensacion"))) {
          where.append("AND ").append("dc.").append(entry.getKey()).append(" =:").append(entry.getKey()).append(" ");
        } else if (entry.getKey().equals("fcontratacionDe")) {
          // si vienen las dos fechas se hace un between
          if (checkIfKeyExist("fcontratacionA", filtros)) {
            where.append(fcontratacionBtw);
          } else {
            where.append("AND dc.fecontratacion >=:fcontratacionDe ");
          }
        } else if (entry.getKey().equals("fcontratacionA")) {
          if (!checkIfKeyExist("fcontratacionDe", filtros)) {
            where.append("AND dc.fecontratacion <=:fcontratacionA ");
          }
        } else if (entry.getKey().equals("codCtaDeCompensacion")) {

          String codCtaDeCompensacion = entry.getValue().toString(); // CFF 23/02/2016.
          if (codCtaDeCompensacion.indexOf("#") != -1) {
            codCtaDeCompensacion = codCtaDeCompensacion.replace("#", "");
            selectCtaCompInCtaLiqu.append("AND cc.cdCodigo <> :codCtaDeCompensacion ");
          } else
            selectCtaCompInCtaLiqu.append("AND cc.cdCodigo = :codCtaDeCompensacion ");
        }
      }
      where.append("AND dc.tmct0infocompensacion.cdctacompensacion in (").append(selectCtaCompInCtaLiqu).append(") ");
    }
    selectSb.append(where.toString());
    LOG.debug(prefix + ">> A punto de ejecutar la siguiente query: " + selectSb.toString());
    Query query = null;
    try {
      query = em.createQuery(selectSb.toString());
      if (filtros != null) {
        String key = null;
        Object value = null;
        Date fecha = null;
        java.sql.Date sqlFecha = null;
        for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
          key = entry.getKey();
          value = entry.getValue();
          if (key.startsWith("f")) {
            // es una fecha...
            fecha = (Date) value;
            sqlFecha = new java.sql.Date(fecha.getTime());
            LOG.debug(prefix + "> [ FECHA] [{}=={} ({})]", key, sqlFecha, sqlFecha.getClass().getName());
            query.setParameter(key, value);
          } else {
            LOG.debug(prefix + "> [NORMAL] [{}=={} ({})]", key, value, value.getClass().getName());
            query.setParameter(key, value);
          }
        }
      }
      query.setParameter("sentido", sentido);
      query.setParameter("estado1", estado1);
      query.setParameter("estado2", estado2);
      resultList = query.getResultList();
    } catch (RuntimeException ex) {
      LOG.error(prefix + "ERROR(): {}", ex.getClass().getName(), ex.getMessage());
    }
    LOG.debug(prefix + ">> Devolviendo resultado de la query resultList: " + resultList);
    return resultList;
  }

  private boolean checkIfKeyExist(String key, Map<String, Serializable> params) {
    boolean exist = false;
    if (params.get(key) != null && !params.get(key).equals("")) {
      exist = true;
    }
    return exist;
  }

  public int updateCdestadocont(int estadocont, Long iddesglosecamara) {
    return em.createNativeQuery(new StringBuffer(queryUpdateCdestadocont[0]).append(estadocont)
                                                                            .append(queryUpdateCdestadocont[1])
                                                                            .append(iddesglosecamara).toString())
             .executeUpdate();
  }
}
