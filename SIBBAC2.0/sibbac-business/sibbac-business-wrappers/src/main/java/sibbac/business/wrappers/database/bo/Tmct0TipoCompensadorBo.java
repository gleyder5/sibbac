package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0TipoCompensadorDao;
import sibbac.business.wrappers.database.model.Tmct0TipoCompensador;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0TipoCompensadorBo extends AbstractBo< Tmct0TipoCompensador, Long, Tmct0TipoCompensadorDao > {

}
