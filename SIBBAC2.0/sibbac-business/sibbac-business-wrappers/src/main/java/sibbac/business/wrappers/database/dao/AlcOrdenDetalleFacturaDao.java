package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.AlcOrdenDetalleFactura;


@Component
public interface AlcOrdenDetalleFacturaDao extends JpaRepository< AlcOrdenDetalleFactura, Long > {

	@Query( "SELECT AOFD FROM AlcOrdenDetalleFactura AOFD WHERE AOFD.facturaDetalle.id =:ID_FACTURA_DETALLE" )
	public List< AlcOrdenDetalleFactura > findAllAlcOrdenDetalleFacturaByID_FACTURA_DETALLE(
			@Param( "ID_FACTURA_DETALLE" ) Long facturaDetalleId );

}
