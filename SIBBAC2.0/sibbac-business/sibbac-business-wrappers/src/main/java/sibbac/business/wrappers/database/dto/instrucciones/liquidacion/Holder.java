package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Holder implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 7675827084493006081L;
  private String cddclave;
  private String nbclient;
  private String nb1apell;
  private String nb2apell;
  private String nbcomple;
  private String nudnicif;
  private String nuclient;
  private String cddepais;
  private String cdordtit;
  private String rftitaud;
  private Character tptiprep;
  private String nbdomici;
  private String nbciudad;
  private String tpdomici;
  private String nudomici;
  private String nbprovin;
  private String cdpostal;
  private String nbclien1;
  private String nbclien2;
  private String cdnactit;
  private Character tpfisjur;
  private String cdddebic;
  private Character tpidenti, tpsocied, tpnactit;
  private String cdholder;

  private Date fechaNacimiento;

  // TIPO DE PROCEDENCIA I-ILQ-R-ROUTING-M-MANUAL/PANTALLA
  private Character tpproced;

  private BigDecimal particip;

  public Holder() {
    super();

    cddclave = "";
    nbclient = "";
    nb1apell = "";
    nb2apell = "";
    nbcomple = "";
    nudnicif = "";
    nuclient = "";
    cddepais = "";
    cdordtit = "";
    rftitaud = "";
    nbdomici = "";
    nbciudad = "";
    tpdomici = "";
    nudomici = "";
    nbprovin = "";
    cdpostal = "";
    nbclien1 = "";
    nbclien2 = "";
    cdnactit = "";
    tpfisjur = ' ';
    cdddebic = "";
    tpidenti = ' ';
    tpsocied = ' ';
    tpnactit = ' ';
    cdholder = "";
    tptiprep = ' ';
    particip = null;
    fechaNacimiento = null;
    tpproced = ' ';

  }

  public Holder(Holder toCopy) {
    this();
    if (toCopy != null) {
      cddclave = toCopy.getCddclave();
      nbclient = toCopy.getNbclient();
      nb1apell = toCopy.getNb1apell();
      nb2apell = toCopy.getNb2apell();
      nbcomple = toCopy.getNbcomple();
      nudnicif = toCopy.getNudnicif();
      nuclient = toCopy.getNuclient();
      cddepais = toCopy.getCddepais();
      cdordtit = toCopy.getCdordtit();
      rftitaud = toCopy.getRftitaud();
      nbdomici = toCopy.getNbdomici();
      nbciudad = toCopy.getNbciudad();
      tpdomici = toCopy.getTpdomici();
      nudomici = toCopy.getNudomici();
      nbprovin = toCopy.getNbprovin();
      cdpostal = toCopy.getCdpostal();
      nbclien1 = toCopy.getNbclien1();
      nbclien2 = toCopy.getNbclien2();
      cdnactit = toCopy.getCdnactit();
      tpfisjur = toCopy.getTpfisjur();
      cdddebic = toCopy.getCdddebic();
      tpidenti = toCopy.getTpidenti();
      tpsocied = toCopy.getTpsocied();
      tpnactit = toCopy.getTpnactit();
      cdholder = toCopy.getCdholder();
      tptiprep = toCopy.getTptiprep();
      particip = toCopy.getParticip();
      fechaNacimiento = toCopy.getFechaNacimiento();
      tpproced = toCopy.getTpproced();
    }
  }

  public String getCddclave() {
    return cddclave;
  }

  public void setCddclave(String cddclave) {
    this.cddclave = cddclave;
  }

  public String getNbclient() {
    return nbclient;
  }

  public void setNbclient(String nbclient) {
    this.nbclient = nbclient;
  }

  public String getNb1apell() {
    return nb1apell;
  }

  public void setNb1apell(String nb1apell) {
    this.nb1apell = nb1apell;
  }

  public String getNb2apell() {
    return nb2apell;
  }

  public void setNb2apell(String nb2apell) {
    this.nb2apell = nb2apell;
  }

  public String getNbcomple() {
    return nbcomple;
  }

  public String getNbcomple_60() {
    if (nbcomple.length() > 60) {
      return nbcomple.substring(0, 60);
    }
    else {
      return nbcomple;
    }
  }

  public void setNbcomple_60(String s) {
  }

  public String getNbcomple_40() {
    if (nbcomple.length() > 40) {
      return nbcomple.substring(0, 40);
    }
    else {
      return nbcomple;
    }
  }

  public void setNbcomple(String nbcomple) {
    this.nbcomple = nbcomple;
  }

  public String getNudnicif() {
    return nudnicif;
  }

  public void setNudnicif(String nudnicif) {
    this.nudnicif = nudnicif;
  }

  public String getNuclient() {
    return nuclient;
  }

  public void setNuclient(String nuclient) {
    this.nuclient = nuclient;
  }

  public String getCddepais() {
    return cddepais;
  }

  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  public String getCdordtit() {
    return cdordtit;
  }

  public void setCdordtit(String cdordtit) {
    this.cdordtit = cdordtit;
  }

  public String getRftitaud() {
    return rftitaud;
  }

  public void setRftitaud(String rftitaud) {
    this.rftitaud = rftitaud;
  }

  public Character getTptiprep() {
    return tptiprep;
  }

  public void setTptiprep(Character tptiprep) {
    this.tptiprep = tptiprep;
  }

  public String getNbdomici() {
    return nbdomici;
  }

  public void setNbdomici(String nbdomici) {
    this.nbdomici = nbdomici;
  }

  public String getNbciudad() {
    return nbciudad;
  }

  public void setNbciudad(String nbciudad) {
    this.nbciudad = nbciudad;
  }

  public String getTpdomici() {
    return tpdomici;
  }

  public void setTpdomici(String tpdomici) {
    this.tpdomici = tpdomici;
  }

  public String getNudomici() {
    return nudomici;
  }

  public void setNudomici(String nudomici) {
    this.nudomici = nudomici;
  }

  public String getNbprovin() {
    return nbprovin;
  }

  public void setNbprovin(String nbprovin) {
    this.nbprovin = nbprovin;
  }

  public String getCdpostal() {
    return cdpostal;
  }

  public void setCdpostal(String cdpostal) {
    this.cdpostal = cdpostal;
  }

  public String getNbclien1() {
    return nbclien1;
  }

  public void setNbclien1(String nbclien1) {
    this.nbclien1 = nbclien1;
  }

  public String getNbclien2() {
    return nbclien2;
  }

  public void setNbclien2(String nbclien2) {
    this.nbclien2 = nbclien2;
  }

  public String getCdnactit() {
    return cdnactit;
  }

  public void setCdnactit(String cdnactit) {
    this.cdnactit = cdnactit;
  }

  public Character getTpfisjur() {
    return tpfisjur;
  }

  public void setTpfisjur(Character tpfisjur) {
    this.tpfisjur = tpfisjur;
  }

  public String getCdddebic() {
    return cdddebic;
  }

  public void setCdddebic(String cdddebic) {
    this.cdddebic = cdddebic;
  }

  public Character getTpidenti() {
    return tpidenti;
  }

  public void setTpidenti(Character tpidenti) {
    this.tpidenti = tpidenti;
  }

  public Character getTpsocied() {
    return tpsocied;
  }

  public void setTpsocied(Character tpsocied) {
    this.tpsocied = tpsocied;
  }

  public Character getTpnactit() {
    return tpnactit;
  }

  public void setTpnactit(Character tpnactit) {
    this.tpnactit = tpnactit;
  }

  /**
   * @return the cdholder
   */
  public String getCdholder() {
    return cdholder;
  }

  /**
   * @param cdholder the cdholder to set
   */
  public void setCdholder(String cdholder) {
    this.cdholder = cdholder;
  }

  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  public Character getTpproced() {
    return tpproced;
  }

  public void setTpproced(Character tpproced) {
    this.tpproced = tpproced;
  }

  public BigDecimal getParticip() {
    return particip;
  }

  public void setParticip(BigDecimal particip) {
    this.particip = particip;
  }

  public boolean isEmpty() {
    return "".equals(nbclient.trim()) && "".equals(nb1apell.trim()) && "".equals(nb2apell.trim())
        && "".equals(nbcomple.trim()) && "".equals(nudnicif.trim()) && "".equals(nuclient.trim())
        && "".equals(cdordtit.trim()) && "".equals(cddepais.trim()) && "".equals(rftitaud.trim())
        && "".equals(cdholder.trim()) && fechaNacimiento == null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Holder [cddclave=" + cddclave + ", nbclient=" + nbclient + ", nb1apell=" + nb1apell + ", nb2apell="
        + nb2apell + ", nbcomple=" + nbcomple + ", nudnicif=" + nudnicif + ", nuclient=" + nuclient + ", cddepais="
        + cddepais + ", cdordtit=" + cdordtit + ", rftitaud=" + rftitaud + ", tptiprep=" + tptiprep + ", nbdomici="
        + nbdomici + ", nbciudad=" + nbciudad + ", tpdomici=" + tpdomici + ", nudomici=" + nudomici + ", nbprovin="
        + nbprovin + ", cdpostal=" + cdpostal + ", nbclien1=" + nbclien1 + ", nbclien2=" + nbclien2 + ", cdnactit="
        + cdnactit + ", tpfisjur=" + tpfisjur + ", cdddebic=" + cdddebic + ", tpidenti=" + tpidenti + ", tpsocied="
        + tpsocied + ", tpnactit=" + tpnactit + ", cdholder=" + cdholder + "]";
  }

}
