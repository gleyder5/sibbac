package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0ctgDao;
import sibbac.business.wrappers.database.model.Tmct0ctg;
import sibbac.business.wrappers.database.model.Tmct0ctgId;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ctgBo extends AbstractBo<Tmct0ctg, Tmct0ctgId, Tmct0ctgDao> {

  public List<Tmct0ctg> findAllByNuoden(String nuorden) {
    return dao.findAllByNuoden(nuorden);
  }

  public List<Tmct0ctg> findAllByNuodenAndNbooking(String nuorden, String nbooking) {
    return dao.findAllByNuodenAndNbooking(nuorden, nbooking);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public int deleteByNuodenAndNbooking(String nuorden, String nbooking) {
    return dao.deleteByNuodenAndNbooking(nuorden, nbooking);
  }

}
