package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.util.ArrayList;
import java.util.List;

import sibbac.business.fase0.database.model.Tmct0dir;
import sibbac.business.fase0.database.model.Tmct0est;

public class SettlementData {

  // Datos
  private SettlementDataClient settlementDataClient = null;
  private SettlementDataClearerDTO clearer = null;
  private SettlementDataClearerDTO custodian = null;
  private SettlementDataClearerInstructionsDTO tmct0ilq = null;
  private Holder holder = null;
  private List<Holder> listHolder = new ArrayList<Holder>();
  private List<SettlementDataClientEstatisticsDTO> listTmct0est = new ArrayList<SettlementDataClientEstatisticsDTO>();
  private List<SettlementDataClientDirectionsDTO> listTmct0dir = new ArrayList<SettlementDataClientDirectionsDTO>();

  // Flags
  private boolean dataClient = false;
  private boolean dataClearer = false;
  private boolean dataCustodian = false;
  private boolean dataIL = false;
  private boolean dataTFI = false;
  private boolean dataEST = false;
  private boolean dataDIR = false;
  private boolean clearerMultiple = false;

  public SettlementData() {
    settlementDataClient = new SettlementDataClient();
    clearer = new SettlementDataClearerDTO();
    custodian = new SettlementDataClearerDTO();
    tmct0ilq = new SettlementDataClearerInstructionsDTO();
    holder = new Holder();
    listHolder = new ArrayList<Holder>();
    listTmct0est = new ArrayList<SettlementDataClientEstatisticsDTO>();
    listTmct0dir = new ArrayList<SettlementDataClientDirectionsDTO>();
    dataClient = false;
    dataClearer = false;
    dataCustodian = false;
    dataIL = false;
    dataTFI = false;
    dataEST = false;
    dataDIR = false;
    clearerMultiple = false;
    initSettlementData();
  }

  public SettlementData(SettlementData toCopy) {
    try {
      this.clearer = new SettlementDataClearerDTO(toCopy.getClearer());
      this.custodian = new SettlementDataClearerDTO(toCopy.getCustodian());
      this.tmct0ilq = new SettlementDataClearerInstructionsDTO(toCopy.getTmct0ilq());
      this.holder = new Holder(toCopy.holder);

      this.listHolder = new ArrayList<Holder>();
      if (toCopy.getListHolder() != null) {
        for (Holder holderF : toCopy.getListHolder()) {
          this.listHolder.add(new Holder(holderF));
        }
      }
      listTmct0est = new ArrayList<SettlementDataClientEstatisticsDTO>(toCopy.listTmct0est);
      listTmct0dir = new ArrayList<SettlementDataClientDirectionsDTO>(toCopy.listTmct0dir);
      this.dataClearer = new Boolean(toCopy.dataClearer);
      this.dataCustodian = new Boolean(toCopy.dataCustodian);
      this.dataIL = new Boolean(toCopy.dataIL);
      this.dataTFI = new Boolean(toCopy.dataTFI);
      dataEST = new Boolean(toCopy.dataEST);
      dataDIR = new Boolean(toCopy.dataDIR);
      this.clearerMultiple = new Boolean(toCopy.clearerMultiple);

      this.settlementDataClient = new SettlementDataClient(toCopy.settlementDataClient);

    }
    catch (Exception e) {

    }
  }

  public SettlementDataClearerDTO getClearer() {
    return clearer;
  }

  public void setClearer(SettlementDataClearerDTO clearer) {
    this.clearer = clearer;
  }

  public SettlementDataClearerDTO getCustodian() {
    return custodian;
  }

  public void setCustodian(SettlementDataClearerDTO custodian) {
    this.custodian = custodian;
  }

  public SettlementDataClearerInstructionsDTO getTmct0ilq() {
    return tmct0ilq;
  }

  public void setTmct0ilq(SettlementDataClearerInstructionsDTO tmct0ilq) {
    this.tmct0ilq = tmct0ilq;
  }

  public SettlementDataClient getDataClient() {
    return settlementDataClient;
  }

  public void setDataClient(SettlementDataClient dataClient) {
    this.settlementDataClient = dataClient;
  }

  public Holder getHolder() {
    return holder;
  }

  public void setHolder(Holder holder) {
    this.holder = holder;
  }

  public List<Holder> getListHolder() {
    return listHolder;
  }

  public void setListHolder(List<Holder> listHolder) {
    this.listHolder = listHolder;
  }

  public List<SettlementDataClientEstatisticsDTO> getListTmct0est() {
    return listTmct0est;
  }

  public void setListTmct0est(List<SettlementDataClientEstatisticsDTO> listTmct0est) {
    this.listTmct0est = listTmct0est;
  }

  public void setListTmct0est2(List<Tmct0est> listTmct0est) {
    if (this.listTmct0est != null) {
      this.listTmct0est.clear();
    }
    else {
      this.listTmct0est = new ArrayList<SettlementDataClientEstatisticsDTO>();
    }
    if (listTmct0est != null) {
      SettlementDataClientEstatisticsDTO dto;
      for (Tmct0est tmct0est : listTmct0est) {
        dto = new SettlementDataClientEstatisticsDTO(tmct0est);
        this.listTmct0est.add(dto);
      }
    }

  }

  public List<SettlementDataClientDirectionsDTO> getListTmct0dir() {
    return listTmct0dir;
  }

  public void setListTmct0dir(List<SettlementDataClientDirectionsDTO> listTmct0dir) {
    this.listTmct0dir = listTmct0dir;
  }

  public void setListTmct0dir2(List<Tmct0dir> listTmct0dir) {
    if (this.listTmct0dir != null) {
      this.listTmct0dir.clear();
    }
    else {
      this.listTmct0dir = new ArrayList<SettlementDataClientDirectionsDTO>();
    }
    if (listTmct0dir != null) {
      SettlementDataClientDirectionsDTO dto;
      for (Tmct0dir tmct0dir : listTmct0dir) {
        dto = new SettlementDataClientDirectionsDTO(tmct0dir);
        this.listTmct0dir.add(dto);
      }
    }
  }

  public boolean isDataClearer() {
    return dataClearer;
  }

  public void setDataClearer(boolean dataClearer) {
    this.dataClearer = dataClearer;
  }

  public boolean isDataCustodian() {
    return dataCustodian;
  }

  public void setDataCustodian(boolean dataCustodian) {
    this.dataCustodian = dataCustodian;
  }

  public boolean isDataIL() {
    return dataIL;
  }

  public void setDataIL(boolean dataIL) {
    this.dataIL = dataIL;
  }

  public boolean isDataClient() {
    return dataClient;
  }

  public void setDataClientBoolean(boolean dataClient) {
    this.dataClient = dataClient;
  }

  public boolean isDataTFI() {
    return dataTFI;
  }

  public void setDataTFI(boolean dataTFI) {
    this.dataTFI = dataTFI;
  }

  public boolean isDataEST() {
    return dataEST;
  }

  public void setDataEST(boolean dataEST) {
    this.dataEST = dataEST;
  }

  public boolean isDataDIR() {
    return dataDIR;
  }

  public void setDataDIR(boolean dataDIR) {
    this.dataDIR = dataDIR;
  }

  public boolean isClearerMultiple() {
    return clearerMultiple;
  }

  public void setClearerMultiple(boolean clearerMultiple) {
    this.clearerMultiple = clearerMultiple;
  }

  private void initSettlementData() {
    clearer.initData();

    custodian.initData();

    tmct0ilq.initData();

  }
  
  

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((clearer == null) ? 0 : clearer.hashCode());
    result = prime * result + (clearerMultiple ? 1231 : 1237);
    result = prime * result + ((custodian == null) ? 0 : custodian.hashCode());
    result = prime * result + (dataClearer ? 1231 : 1237);
    result = prime * result + (dataClient ? 1231 : 1237);
    result = prime * result + (dataCustodian ? 1231 : 1237);
    result = prime * result + (dataDIR ? 1231 : 1237);
    result = prime * result + (dataEST ? 1231 : 1237);
    result = prime * result + (dataIL ? 1231 : 1237);
    result = prime * result + (dataTFI ? 1231 : 1237);
    result = prime * result + ((holder == null) ? 0 : holder.hashCode());
    result = prime * result + ((listHolder == null) ? 0 : listHolder.hashCode());
    result = prime * result + ((listTmct0dir == null) ? 0 : listTmct0dir.hashCode());
    result = prime * result + ((listTmct0est == null) ? 0 : listTmct0est.hashCode());
    result = prime * result + ((settlementDataClient == null) ? 0 : settlementDataClient.hashCode());
    result = prime * result + ((tmct0ilq == null) ? 0 : tmct0ilq.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    SettlementData other = (SettlementData) obj;
    if (clearer == null) {
      if (other.clearer != null)
        return false;
    }
    else if (!clearer.equals(other.clearer))
      return false;
    if (clearerMultiple != other.clearerMultiple)
      return false;
    if (custodian == null) {
      if (other.custodian != null)
        return false;
    }
    else if (!custodian.equals(other.custodian))
      return false;
    if (dataClearer != other.dataClearer)
      return false;
    if (dataClient != other.dataClient)
      return false;
    if (dataCustodian != other.dataCustodian)
      return false;
    if (dataDIR != other.dataDIR)
      return false;
    if (dataEST != other.dataEST)
      return false;
    if (dataIL != other.dataIL)
      return false;
    if (dataTFI != other.dataTFI)
      return false;
    if (holder == null) {
      if (other.holder != null)
        return false;
    }
    else if (!holder.equals(other.holder))
      return false;
    if (listHolder == null) {
      if (other.listHolder != null)
        return false;
    }
    else if (!listHolder.equals(other.listHolder))
      return false;
    if (listTmct0dir == null) {
      if (other.listTmct0dir != null)
        return false;
    }
    else if (!listTmct0dir.equals(other.listTmct0dir))
      return false;
    if (listTmct0est == null) {
      if (other.listTmct0est != null)
        return false;
    }
    else if (!listTmct0est.equals(other.listTmct0est))
      return false;
    if (settlementDataClient == null) {
      if (other.settlementDataClient != null)
        return false;
    }
    else if (!settlementDataClient.equals(other.settlementDataClient))
      return false;
    if (tmct0ilq == null) {
      if (other.tmct0ilq != null)
        return false;
    }
    else if (!tmct0ilq.equals(other.tmct0ilq))
      return false;
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SettlementData [settlementDataClient=" + settlementDataClient + ", clearer=" + clearer + ", custodian="
        + custodian + ", tmct0ilq=" + tmct0ilq + ", holder=" + holder + ", listHolder=" + listHolder
        + ", listTmct0est=" + listTmct0est + ", listTmct0dir=" + listTmct0dir + ", dataClient=" + dataClient
        + ", dataClearer=" + dataClearer + ", dataCustodian=" + dataCustodian + ", dataIL=" + dataIL + ", dataTFI="
        + dataTFI + ", dataEST=" + dataEST + ", dataDIR=" + dataDIR + ", clearerMultiple=" + clearerMultiple + "]";
  }
}
