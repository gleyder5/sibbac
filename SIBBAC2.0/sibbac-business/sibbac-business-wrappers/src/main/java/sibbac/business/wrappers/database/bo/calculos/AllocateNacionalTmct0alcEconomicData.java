package sibbac.business.wrappers.database.bo.calculos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.canones.bo.CalculoCanonesNacionalBo;
import sibbac.business.canones.commons.CanonesConfigDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.calculos.dto.CalculosEconomicosFlagsDTO;
import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TiposComision;
import sibbac.common.utils.SibbacEnums.TypeResultProcess;

/**
 * Realiza los cálculos económicos de un objeto <code>Tmct0alc</code>.
 * 
 * @author XIS16630
 * @see AbstractProcess
 */
@Service
@Scope(value = "prototype")
public class AllocateNacionalTmct0alcEconomicData {
  protected static final Logger LOG = LoggerFactory.getLogger(AllocateNacionalTmct0alcEconomicData.class);

  @Autowired
  private CalculoCanonesNacionalBo calculoCanonesNacionalBo;

  /**
   * Objeto <code>Tmct0alc</code> sobre el que realizar los cálculos económicos.
   */
  protected Tmct0alc tmct0alc = null;

  /** Lista de objetos <code>DesgloseCliTit</code> del <code>Tmct0alc</code>. */
  private List<Tmct0desgloseclitit> activeDesgloseCliTitList = null;

  /** Indica si el objeto <code>Tmct0alc</code> se puede modificar. */
  private Boolean canAllocateTmct0alc = null;

  /** Indica si el mercado paga canones de contratación. */
  private Boolean mktPayCanonContr = null;

  /** Indica si el mercado paga canones de compensación. */
  private Boolean mktPayCanonComp = null;

  /** Indica si el mercado paga canones de liquidación. */
  private Boolean mktPayCanonLiq = null;

  /** Indica si el cliente paga canones de contratación. */
  private Boolean cltePayCanonContr = null;

  /** Indica si el cliente paga canones de compensación. */
  private Boolean cltePayCanonComp = null;

  /** Indica si el cliente paga canones de liquidación. */
  private Boolean cltePayCanonLiq = null;

  private Tcli0comDTO tcli0com;

  private CanonesConfigDTO canonesConfig;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Constructor I. Crea un nuevo objeto de la clase.
   * 
   * @param tmct0alc Objeto <code>Tmct0alc</code>.
   * @param hpm Gestor de acceso a base de datos.
   */
  public AllocateNacionalTmct0alcEconomicData(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> desgloses, Tcli0comDTO tcli,
      CanonesConfigDTO canonesConfig) {
    this.tmct0alc = tmct0alc;
    this.activeDesgloseCliTitList = desgloses;
    this.tcli0com = tcli;
    this.canonesConfig = canonesConfig;

  } // AllocateNacionalTmct0alcEconomicData

  public void inizializeNullsDesglosesClitit() {
    LOG.trace("## ALCEData ## inizializeNullsDesglosesClitit ## Inicio");
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      inicializarNullsDesgloseCliTit(desgloseCliTit);
    } // for
    LOG.trace("## ALCEData ## inizializeNullsDesglosesClitit ## Fin");
  }

  /* ~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS ~~~~~~~~~~~~~~~~~~~~ */

  /* ~~~~~~~~~~~~~~~~~~~~ BRUTO MERCADO ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> sumariza el
   * importe efectivo (IMEFECTIVO) y lo inserta en el campo
   * <code>IMEFEAGR</code> del <code>Tmct0alc</code>.
   *
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void calcularBrutoMercado() {
    LOG.trace("## ALCEData ## calcularBrutoMercado ## Inicio");

    BigDecimal imefeagr = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      imefeagr = imefeagr.add(desgloseCliTit.getImefectivo());
    } // for

    tmct0alc.setImefeagr(imefeagr.setScale(2, BigDecimal.ROUND_HALF_UP));
    LOG.trace("## ALCEData ## calcularBrutoMercado ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());
    LOG.trace("## ALCEData ## calcularBrutoMercado ## Fin");
  } // calcularBrutoMercado

  /**
   * Calculo de imcomsis para dwh
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void calcularImcomsis() {
    LOG.trace("## ALCEData ## calcularImcomsis ## Inicio");
    if (getTcli0com() != null) {
      LOG.trace("## ALCEData ## calcularImcomsis ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());
      LOG.trace("## ALCEData ## calcularImcomsis ## tcli0com.getPccomsis(): {} ", getTcli0com().getPccomsis());
      BigDecimal imcomsis = tmct0alc.getImefeagr().multiply(getTcli0com().getPccomsis())
          .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
      LOG.trace("## ALCEData ## calcularImcomsis ## tmct0alc.getImcomsis(): {} ", imcomsis);
      tmct0alc.setImcomsis(imcomsis);
    }
    else {
      LOG.warn("## ALCEData ## calcularImcomsis ## tcli0com == NULL ");
      tmct0alc.setImcomsis(BigDecimal.ZERO);
    }
    LOG.trace("## ALCEData ## calcularImcomsis ## tmct0alc.getImcomsis(): {} ", tmct0alc.getImcomsis());
    repartirImcomsis();
    LOG.trace("## ALCEData ## calcularImcomsis ## Fin");
  } // calcularBrutoMercado

  /**
   * Calculo de imbansis para dwh
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void calcularImbansis() {
    LOG.trace("## ALCEData ## calcularImbansis ## Inicio");
    if (getTcli0com() != null) {
      LOG.trace("## ALCEData ## calcularImbansis ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());
      LOG.trace("## ALCEData ## calcularImbansis ## tcli0com.getPcbansis(): {} ", getTcli0com().getPcbansis());
      BigDecimal imbansis = tmct0alc.getImefeagr().multiply(getTcli0com().getPcbansis())
          .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
      LOG.trace("## ALCEData ## calcularImbansis ## tmct0alc.getImbansis(): {} ", imbansis);
      tmct0alc.setImbansis(imbansis);
    }
    else {
      LOG.warn("## ALCEData ## calcularImcomsis ## tcli0com == NULL ");
      tmct0alc.setImbansis(BigDecimal.ZERO);
    }
    LOG.trace("## ALCEData ## calcularImbansis ## tmct0alc.getImbansis(): {} ", tmct0alc.getImbansis());
    repartirImbansis();
    LOG.trace("## ALCEData ## calcularImbansis ## Fin");
  } // calcularBrutoMercado

  /* ~~~~~~~~~~~~~~~~~~~~ BRUTO CLIENTE ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> reparte el
   * importe bruto cliente (IMTOTBRU) del <code>Tmct0alc</code> .<br>
   * Establece el campo <code>IMTOTBRU</code> del <code>DesgloseCliTit</code>
   * con el resultado del reparto.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void repartirBrutoCliente() {
    LOG.trace("## ALCEData ## repartirBrutoCliente ## Inicio");
    LOG.trace("## ALCEData ## repartirBrutoCliente ## tmct0alc.getImtotbru(): {}", tmct0alc.getImtotbru());
    LOG.trace("## ALCEData ## repartirBrutoCliente ## tmct0alc.getNutitliq(): {}", tmct0alc.getNutitliq());

    int i = 0;
    BigDecimal imtotbru;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirBrutoCliente ## desgloseCliTit.getImtitulos(): {}",
            desgloseCliTit.getImtitulos());

        imtotbru = desgloseCliTit.getImtitulos().multiply(tmct0alc.getImtotbru())
            .divide(tmct0alc.getNutitliq(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImtotbru(imtotbru);
        acumulado = acumulado.add(imtotbru);
      }
      else {
        LOG.trace("## ALCEData ## repartirBrutoCliente ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImtotbru(tmct0alc.getImtotbru().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirBrutoCliente ## desgloseCliTit.getImtotbru(): {}", desgloseCliTit.getImtotbru());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirBrutoCliente ## Fin");
  } // repartirBrutoCliente

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN CLIENTE ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> calcula la
   * comisión cliente (IMCOMISN) a partir del valor del campo (PCCOMISN) del
   * <code>Tmct0alo</code>.<br>
   * Establece el campo <code>IMCOMISN</code> del <code>DesgloseCliTit</code>
   * con el resultado del reparto.
   * <p>
   * Comisión cobrada al cliente.<br>
   * Si es calculado (PCCOMISN): resultante de aplicar un % de comisión al bruto
   * cliente.<br>
   * Si es un importe (IMCOMISN): es informado directamente (automático o
   * manual).
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void calcularComisionCliente() {
    LOG.trace("## ALCEData ## calcularComisionCliente ## Inicio");
    LOG.trace("## ALCEData ## calcularComisionCliente ## tmct0alc.getTmct0alo().getPccomisn(): {}", tmct0alc
        .getTmct0alo().getPccomisn());

    BigDecimal imcomisn;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      LOG.trace("## ALCEData ## calcularComisionCliente ## desgloseCliTit.getImtotbru(): {}",
          desgloseCliTit.getImtotbru());
      imcomisn = desgloseCliTit.getImtotbru().multiply(tmct0alc.getTmct0alo().getPccomisn())
          .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
      LOG.trace("## ALCEData ## calcularComisionCliente ## imcomisn: {}", imcomisn);
      acumulado = acumulado.add(imcomisn);
    } // for
    tmct0alc.setImcomisn(acumulado.setScale(2, BigDecimal.ROUND_HALF_UP));
    LOG.trace("## ALCEData ## calcularComisionCliente ## tmct0alc.getImcomisn(): {}", tmct0alc.getImcomisn());

    repartirComisionCliente();
    LOG.trace("## ALCEData ## calcularComisionCliente ## Fin");
  } // calcularComisionCliente

  /* ~~~~~~~~~~~~~~~~~~~~ CÁNONES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> calcula los
   * derechos de liquidación e inserta el sumatorio en el campo
   * <code>IMDERLIQ</code> del <code>Tmct0alc</code>.<br>
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  public void calcularCanones(CanonesConfigDTO configCanones, Tmct0alc tmct0alc,
      List<Tmct0desgloseclitit> desgloseCliTitList) throws EconomicDataException {
    LOG.trace("## ALCEData ## calcularCanones ## Inicio");
    BigDecimal canonCompensacion = BigDecimal.ZERO;
    tmct0alc.setImcanoncompdv(canonCompensacion);
    tmct0alc.setImcanoncompeu(canonCompensacion);
    LOG.trace("## ALCEData ## calcularCanones ## ImCanonCompEu: {} ", tmct0alc.getImcanoncompeu());

    StringBuilder sb = null;

    BigDecimal imderliq_alo = tmct0alc.getTmct0alo().getImgasliqeu();
    BigDecimal imderges_alo = tmct0alc.getTmct0alo().getImgasgeseu();

    sb = new StringBuilder("## ALCEData ## calcularCanones ## imderliq_alo: ").append(imderliq_alo);
    LOG.trace(sb.toString());

    sb = new StringBuilder("## ALCEData ## calcularCanones ## imderges_alo: ").append(imderges_alo);
    LOG.trace(sb.toString());
    try {
      calculoCanonesNacionalBo.calcularCanonForDesglose(tmct0alc, configCanones, tmct0alc.getCdusuaud());
    }
    catch (SIBBACBusinessException e) {
      throw new EconomicDataException(e.getMessage(), e);
    }

    LOG.trace("## ALCEData ## calcularCanones ## ImCanonContrEu final: {} ", tmct0alc.getImcanoncontreu());
    LOG.trace("## ALCEData ## calcularCanones ## ImCanonContrDv final: {} ", tmct0alc.getImcanoncontrdv());

    LOG.trace("## ALCEData ## calcularCanones ## Fin");
  } // calcularCanones

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> calcula los
   * derechos de liquidación e inserta el sumatorio en el campo
   * <code>IMDERLIQ</code> del <code>Tmct0alc</code>.<br>
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void calcularCanones() throws EconomicDataException {
    this.calcularCanones(this.canonesConfig, getTmct0alc(), getActiveDesgloseCliTitList());
  } // calcularCanones

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> reparte los
   * cánones.<br>
   * 
   * @throws PoolPersistanceManagerException Si ocurre algún error al obtener el
   * gestor de acceso a base de datos.
   */
  private static void repartirCanonContratacionEu(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> desglosesCliTitList,
      BigDecimal imcanoncontreu) {
    LOG.trace("## ALCEData ## repartirCanonContratacionEu ## Inicio");
    LOG.trace("## ALCEData ## repartirCanonContratacionEu ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imCanonContrEu;

    BigDecimal acumuladoImCanonContrEu = BigDecimal.ZERO;

    for (Tmct0desgloseclitit desgloseCliTit : desglosesCliTitList) {
      i++;
      if (i != desglosesCliTitList.size()) {
        LOG.trace("## ALCEData ## repartirCanonContratacionEu ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        // Canon de liquidación

        // Canon de contratación
        imCanonContrEu = desgloseCliTit.getImefectivo().multiply(imcanoncontreu)
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);

        desgloseCliTit.setImcanoncontreu(imCanonContrEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        acumuladoImCanonContrEu = acumuladoImCanonContrEu.add(imCanonContrEu);

      }
      else {
        desgloseCliTit.setImcanoncontreu(imcanoncontreu.subtract(acumuladoImCanonContrEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));
      } // else
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirCanonContratacionEu ## Fin");
  } // repartirCanones

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> reparte los
   * cánones.<br>
   * 
   * @throws PoolPersistanceManagerException Si ocurre algún error al obtener el
   * gestor de acceso a base de datos.
   */
  protected void repartirCanones() {
    repartirCanones(getTmct0alc(), getActiveDesgloseCliTitList(), getTmct0alc().getImcanonliqdv(), getTmct0alc()
        .getImcanoncontrdv(), getTmct0alc().getImcanoncompdv());
    repartirCanonContratacionEu(getTmct0alc(), getActiveDesgloseCliTitList(), getTmct0alc().getImcanoncontreu());
  }

  protected void copiarCanonContreuToCanoncontrdv() {

    for (Tmct0desgloseclitit dct : getActiveDesgloseCliTitList()) {
      dct.setImcanoncontrdv(dct.getImcanoncontreu().setScale(2, RoundingMode.HALF_UP));
    }
  }

  protected void copiarCanonContrdvToCanoncontreu() {

    for (Tmct0desgloseclitit dct : getActiveDesgloseCliTitList()) {
      dct.setImcanoncontreu(dct.getImcanoncontrdv().setScale(2, RoundingMode.HALF_UP));
    }
  }

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> reparte los
   * cánones.<br>
   * 
   * @throws PoolPersistanceManagerException Si ocurre algún error al obtener el
   * gestor de acceso a base de datos.
   */
  private static void repartirCanones(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> desglosesCliTitList,
      BigDecimal imCanonLiquidacion, BigDecimal imcanonContratacion, BigDecimal imcanonCompensacion) {
    LOG.trace("## ALCEData ## repartirCanones ## Inicio");
    LOG.trace("## ALCEData ## repartirCanones ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imCanonLiqEu;
    BigDecimal imCanonContrEu;

    BigDecimal imCanonCompEu;

    BigDecimal acumuladoImCanonLiqEu = BigDecimal.ZERO;
    BigDecimal acumuladoImCanonContrEu = BigDecimal.ZERO;

    BigDecimal acumuladoImCanonCompEu = BigDecimal.ZERO;

    for (Tmct0desgloseclitit desgloseCliTit : desglosesCliTitList) {
      i++;
      if (i != desglosesCliTitList.size()) {
        LOG.trace("## ALCEData ## repartirCanones ## desgloseCliTit.getImefectivo(): {} ",
            desgloseCliTit.getImefectivo());

        // Canon de liquidación
        imCanonLiqEu = desgloseCliTit.getImefectivo().multiply(imCanonLiquidacion)
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);

        desgloseCliTit.setImcanonliqeu(imCanonLiqEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImcanonliqdv(imCanonLiqEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        acumuladoImCanonLiqEu = acumuladoImCanonLiqEu.add(imCanonLiqEu);

        // Canon de contratación
        imCanonContrEu = desgloseCliTit.getImefectivo().multiply(imcanonContratacion)
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcanoncontreu(imCanonContrEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImcanoncontrdv(imCanonContrEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        acumuladoImCanonContrEu = acumuladoImCanonContrEu.add(imCanonContrEu);

        imCanonCompEu = desgloseCliTit.getImefectivo().multiply(imcanonCompensacion)
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);

        desgloseCliTit.setImcanoncompeu(imCanonCompEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImcanoncompdv(imCanonCompEu.setScale(2, BigDecimal.ROUND_HALF_UP));
        acumuladoImCanonCompEu = acumuladoImCanonCompEu.add(imCanonCompEu);

      }
      else {
        desgloseCliTit.setImcanonliqeu(imCanonLiquidacion.subtract(acumuladoImCanonLiqEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImcanonliqdv(imCanonLiquidacion.subtract(acumuladoImCanonLiqEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));

        desgloseCliTit.setImcanoncontreu(imcanonContratacion.subtract(acumuladoImCanonContrEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImcanoncontrdv(imcanonContratacion.subtract(acumuladoImCanonContrEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));

        desgloseCliTit.setImcanoncompeu(imcanonCompensacion.subtract(acumuladoImCanonCompEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImcanoncompdv(imcanonCompensacion.subtract(acumuladoImCanonCompEu).setScale(2,
            BigDecimal.ROUND_HALF_UP));
      } // else
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirCanones ## Fin");
  } // repartirCanones

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN BANCO ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Calcula la comisión a abonar a la entidad liquidadora.
   * <p>
   * Comisión a abonar a la entidad liquidadora.<br>
   * Si es calculado: resultante de aplicar el % de comisión al bruto mercado.<br>
   * Si es un importe: es informado directamente por el usuario de BackOffice.
   */
  protected void calcularComisionBanco() {
    LOG.trace("## ALCEData ## calcularComisionBanco ## Inicio");

    if (BigDecimal.ZERO.compareTo(tmct0alc.getPccombco()) == 0
        && BigDecimal.ZERO.compareTo(tmct0alc.getImcombco()) != 0) {
      tmct0alc.setPccombco(tmct0alc.getImcombco().multiply(new BigDecimal(100))
          .divide(tmct0alc.getImefeagr(), 4, BigDecimal.ROUND_HALF_UP));
    }
    else {
      tmct0alc.setImcombco(tmct0alc.getImefeagr().multiply(tmct0alc.getPccombco())
          .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
    } // else

    LOG.trace("## ALCEData ## calcularComisionBanco ## tmct0alc.getPccombco(): {} ", tmct0alc.getPccombco());
    LOG.trace("## ALCEData ## calcularComisionBanco ## tmct0alc.getImcombco(): {} ", tmct0alc.getImcombco());
    repartirComisionBanco();
    LOG.trace("## ALCEData ## calcularComisionBanco ## Fin");
  } // calcularComisionBanco

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN ORDENANTE. PAYER ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * 
   */
  private void calcularComisionOrdenante() {
    LOG.trace("## ALCEData ## calcularComisionOrdenante ## Inicio");

    if (BigDecimal.ZERO.compareTo(tmct0alc.getPccomdev()) == 0
        && BigDecimal.ZERO.compareTo(tmct0alc.getImcomdvo()) != 0) {
      tmct0alc.setPccomdev(tmct0alc.getImcomdvo().multiply(new BigDecimal(100))
          .divide(tmct0alc.getImefeagr(), 4, BigDecimal.ROUND_HALF_UP));
    }
    else {
      tmct0alc.setImcomdvo(tmct0alc.getImefeagr().multiply(tmct0alc.getPccomdev())
          .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
    } // else
    LOG.trace("## ALCEData ## calcularComisionOrdenante ## tmct0alc.setPccomdev(): {} ", tmct0alc.getPccomdev());
    LOG.trace("## ALCEData ## calcularComisionOrdenante ## tmct0alc.setImcomdvo(): {} ", tmct0alc.getImcomdvo());
    LOG.trace("## ALCEData ## calcularComisionOrdenante ## Fin");
  } // calcularRebate

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN DEVOLUCION. REBATE ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Calcula la comisión solicitada por el ordenante.
   * <p>
   * Comisión solicitada por el ordenante que se incorpora en el cálculo del
   * neto del cliente. No se repercute contra la Com.SV.<br>
   * Si es calculado: resultante de aplicar el % de comisión al efectivo bruto.<br>
   * Si es un importe: es informado directamente por el usuario de BackOffice.<br>
   * La comisión se contabiliza en un auxiliar independiente a favor del cliente
   * <br>
   * Corresponde al rebate / Comision de devolución de la pantalla de desglose.
   * NO es la comisión de ordenante.
   */
  protected void calcularRebate() {
    LOG.trace("## ALCEData ## calcularRebate ## Inicio");

    if (BigDecimal.ZERO.compareTo(tmct0alc.getPccombrk()) == 0
        && BigDecimal.ZERO.compareTo(tmct0alc.getImcombrk()) != 0) {
      tmct0alc.setPccombrk(tmct0alc.getImcombrk().multiply(new BigDecimal(100))
          .divide(tmct0alc.getImefeagr(), 4, BigDecimal.ROUND_HALF_UP));
    }
    else {
      tmct0alc.setImcombrk(tmct0alc.getImefeagr().multiply(tmct0alc.getPccombrk())
          .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
    } // else
    LOG.trace("## ALCEData ## calcularRebate ## tmct0alc.getPccombrk(): {} ", tmct0alc.getPccombrk());
    LOG.trace("## ALCEData ## calcularRebate ## tmct0alc.getImcombrk(): {} ", tmct0alc.getImcombrk());
    repartirRebate();
    LOG.trace("## ALCEData ## calcularRebate ## Fin");
  } // calcularRebate

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN SV ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Calcula la Profit Commission de cada 'ald' e inserta el sumatorio en el
   * 'alc' en el campo 'IMCOMSVB'.
   * <p>
   * -- viejo: Es la Com.Cliente +- Com.Ajuste.
   * <p>
   * --nuevo: Com.Liquidacion - Canon.Contratacion - Canon.Compensacion -
   * Canon.Liquidacion - Com.Ordenante - Com.Banco
   */
  protected void calcularProfitCommission() {
    calcularProfitCommission(getTmct0alc(), getActiveDesgloseCliTitList());
  } // calcularComisionSvb

  public static void calcularProfitCommission(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> dcts) {
    LOG.trace("## ALCEData ## calcularProfitCommission ## Inicio");

    // if ( tmct0alc.getImajusvb() == null ) {
    // tmct0alc.setImajusvb( BigDecimal.ZERO );
    // } // if
    // LOG.trace( this,
    // "## ALCEData ## calcularProfitCommission ## tmct0alc.getImajusvb(): {} ",
    // tmct0alc.getImajusvb()
    // );
    // tmct0alc.setImcomsvb(tmct0alc.getImcomisn().add(tmct0alc.getImajusvb()).setScale(2,
    // BigDecimal.ROUND_UP));

    // TODO CAMBIADO
    // "ALC!imfinsvb - ALC!imcanoncontreu -
    // ALC!imcanoncompeu - ALC!imcanonliqeu - ALC!imcomdev - ALC!imcombco"
    tmct0alc.setImcomsvb(tmct0alc.getImfinsvb().subtract(tmct0alc.getImcanoncontreu())
        .subtract(tmct0alc.getImcanoncompeu()).subtract(tmct0alc.getImcanonliqeu()).subtract(tmct0alc.getImcomdvo())
        .subtract(tmct0alc.getImcombco()).setScale(2, BigDecimal.ROUND_HALF_UP));

    LOG.trace("## ALCEData ## calcularProfitCommission ## tmct0alc.getImcomsvb(): {} ", tmct0alc.getImcomsvb());
    repartirProfitCommission(tmct0alc, dcts);
    LOG.trace("## ALCEData ## calcularProfitCommission ## Fin");
  }

  /* ~~~~~~~~~~~~~~~~~~~~ COMISIÓN DE LIQUIDACIÓN ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Calcula la comisión de liquidación de cada 'ald' e inserta el sumatorio en
   * el 'alc' en el campo 'IMFINSVB'.
   * <p>
   * --nuevo : <br>
   * Si el ciente paga gastos <br>
   * Canon.Contratacion + Canon.Compensacion + Canon.Liquidacion + Com.Cliente+
   * Com.Ajuste + Com.Ordenante + Com.Banco <br>
   * Si El cliente no paga los gastos <br>
   * Com.Ajuste + Com.Cliente + Com.Ordenante - Ajuste.Precio
   * <p>
   * --viejo : Es la comisión que se ha enviado a liquidar, calculada como
   * Com.Cliente +- Com.Ordenante y es lo que va a recibirse de Iberclear en
   * Modo Orden o modo Rectora.
   */
  public static void calcularComisionLiquidacion(boolean clientPayCanon, Tmct0alc tmct0alc,
      List<Tmct0desgloseclitit> dcts) {
    LOG.trace("## ALCEData ## calcularComisionLiquidacion ## Inicio");

    if (clientPayCanon) {
      LOG.trace("## ALCEData ## calcularComisionLiquidacion ## El cliente paga gastos.");
      tmct0alc.setImfinsvb(tmct0alc.getImcanoncompeu().add(tmct0alc.getImcanoncontreu())
          .add(tmct0alc.getImcanonliqeu()).add(tmct0alc.getImcomisn()).add(tmct0alc.getImajusvb())
          .add(tmct0alc.getImcomdvo()).add(tmct0alc.getImcombco()).setScale(2, BigDecimal.ROUND_UP));
    }
    else {
      LOG.debug("## ALCEData ## calcularComisionLiquidacion ## El cliente NO paga gastos.");
      if ('C' == tmct0alc.getTmct0alo().getCdtpoper()) {
        LOG.debug("## ALCEData ## calcularComisionLiquidacion ## El cliente NO paga gastos, COMPRA.");
        tmct0alc.setImfinsvb(tmct0alc.getImnfiliq().subtract(tmct0alc.getImefeagr()));
      }
      else {
        LOG.debug("## ALCEData ## calcularComisionLiquidacion ## El cliente NO paga gastos, VENTA.");
        tmct0alc.setImfinsvb(tmct0alc.getImefeagr().subtract(tmct0alc.getImnfiliq()));
      }
    }

    LOG.trace("## ALCEData ## calcularComisionLiquidacion ## tmct0alc.getImfinsvb(): {} ", tmct0alc.getImfinsvb());
    LOG.trace("## ALCEData ## calcularComisionLiquidacion ## Fin");

    repartirComisionLiquidacion(tmct0alc, dcts);
  } // calcularComisionLiquidacion

  /**
   * Calcula la comisión de liquidación de cada 'ald' e inserta el sumatorio en
   * el 'alc' en el campo 'IMFINSVB'.
   * <p>
   * --nuevo : <br>
   * Si el ciente paga gastos <br>
   * Canon.Contratacion + Canon.Compensacion + Canon.Liquidacion + Com.Cliente+
   * Com.Ajuste + Com.Ordenante + Com.Banco <br>
   * Si El cliente no paga los gastos <br>
   * Com.Ajuste + Com.Cliente + Com.Ordenante - Ajuste.Precio
   * <p>
   * --viejo : Es la comisión que se ha enviado a liquidar, calculada como
   * Com.Cliente +- Com.Ordenante y es lo que va a recibirse de Iberclear en
   * Modo Orden o modo Rectora.
   */
  protected void calcularComisionLiquidacion() {
    boolean clientPayCanon = getCltePayCanonComp() || getCltePayCanonContr() || getCltePayCanonLiq();
    calcularComisionLiquidacion(clientPayCanon, getTmct0alc(), getActiveDesgloseCliTitList());

  } // calcularComisionLiquidacion

  protected void repartirComisionLiquidacion() {
    repartirComisionLiquidacion(getTmct0alc(), getActiveDesgloseCliTitList());
  }

  /**
   * Para cada <code>DesgloseCliTit</code> del <code>Tmct0alc</code> reparte el
   * importe comision liquidacion (IMFINSVB) del <code>Tmct0alc</code> .<br>
   * Establece el campo <code>IMFINSVB</code> del <code>DesgloseCliTit</code>
   * con el resultado del reparto.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected static void repartirComisionLiquidacion(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> dcts) {
    LOG.trace("## ALCEData ## repartirComisionLiquidacion ## Inicio");
    LOG.trace("## ALCEData ## repartirComisionLiquidacion ## tmct0alc.getImfinsvb(): {} ", tmct0alc.getImfinsvb());
    LOG.trace("## ALCEData ## repartirComisionLiquidacion ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imfinsvb;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : dcts) {
      i++;
      if (i != dcts.size()) {
        LOG.trace("## ALCEData ## repartirComisionLiquidacion ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imfinsvb = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImfinsvb())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImfinsvb(imfinsvb);
        acumulado = acumulado.add(imfinsvb);
      }
      else {
        LOG.trace("## ALCEData ## repartirComisionLiquidacion ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImfinsvb(tmct0alc.getImfinsvb().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirComisionLiquidacion ## desgloseCliTit.getImfinsvb(): "
          + desgloseCliTit.getImfinsvb());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirComisionLiquidacion ## Fin");
  } // repartirBrutoCliente

  /* ~~~~~~~~~~~~~~~~~~~~ AJUSTE PRECIO ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Calcula Ajuste por Precio<br>
   * Si el cliente paga los canones ==0<br>
   * Si el cliente no los paga si C imtotbru - imefeagr si V imefeagr - imtotbru
   * 
   */
  protected void calcularPriceAjustment() {
    LOG.trace("## ALCEData ## calcularPriceAjustment ## Inicio");
    if (getCltePayCanonComp() || getCltePayCanonContr() || getCltePayCanonLiq()) {
      LOG.trace("## ALCEData ## calcularPriceAjustment ## El cliente PAGA los canones.");
      tmct0alc.setImntbrok(BigDecimal.ZERO);
    }
    else {
      LOG.trace("## ALCEData ## calcularPriceAjustment ## El cliente NO PAGA los canones.");
      if ('C' == tmct0alc.getTmct0alo().getCdtpoper()) {
        tmct0alc.setImntbrok(tmct0alc.getImtotbru().subtract(tmct0alc.getImefeagr()));
      }
      else {
        tmct0alc.setImntbrok(tmct0alc.getImefeagr().subtract(tmct0alc.getImtotbru()));
      }
      // tmct0alc.setImntbrok(tmct0alc.getImtotbru().subtract(tmct0alc.getImefeagr()));
    }

    LOG.trace("## ALCEData ## calcularPriceAjustment ## IMNTBROK: {} ", tmct0alc.getImntbrok());
    repartirPriceAjustment();
    LOG.trace("## ALCEData ## calcularPriceAjustment ## Fin");
  } // calcularNetoMercado

  /*
   * ~~~~~~~~~~~~~~~~~~~~ NETO CLIENTE SI EL CLIENTE ASUME GASTOS
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * Calcula el neto final si el cliente paga canones <br>
   * Si es compra<br>
   * Efectivo + Canones que paga + Com.Cliente + Com.Ordenante+ Com.Banco +
   * Com.Ajuste<br>
   * Si es venta<br>
   * Efectivo - Canones que paga - Com.Cliente - Com.Ordenante - Com.Banco +
   * Com.Ajuste<br>
   * 
   */
  public static void calcularNetoClienteFinal(boolean cltePayCanonComp, boolean cltPayCanonContr,
      boolean cltePayCanonLiq, Tmct0alc tmct0alc, List<Tmct0desgloseclitit> dcts) {
    LOG.trace("## ALCEData ## calcularNetoClienteFinal ## Inicio");

    BigDecimal imTotCanones = BigDecimal.ZERO;
    if (cltePayCanonComp) {
      imTotCanones = imTotCanones.add(tmct0alc.getImcanoncompeu());
    }
    if (cltPayCanonContr) {
      imTotCanones = imTotCanones.add(tmct0alc.getImcanoncontreu());
    }
    if (cltePayCanonLiq) {
      imTotCanones = imTotCanones.add(tmct0alc.getImcanonliqeu());
    }
    LOG.trace("## ALCEData ## calcularNetoClienteFinal ## imTotCanones: {} ", imTotCanones);

    LOG.trace("## ALCEData ## calcularNetoClienteFinal ## Operacion: {} ", tmct0alc.getTmct0alo().getCdtpoper());

    if ('C' == tmct0alc.getTmct0alo().getCdtpoper()) {
      tmct0alc.setImnfiliq(tmct0alc.getImefeagr().add(imTotCanones).add(tmct0alc.getImcomisn())
          .add(tmct0alc.getImcomdvo()).add(tmct0alc.getImajusvb()).add(tmct0alc.getImcombco())
          .setScale(2, BigDecimal.ROUND_HALF_UP));
    }
    else {
      tmct0alc.setImnfiliq(tmct0alc.getImefeagr().subtract(imTotCanones).subtract(tmct0alc.getImcomisn())
          .subtract(tmct0alc.getImcomdvo()).subtract(tmct0alc.getImcombco()).add(tmct0alc.getImajusvb())
          .setScale(2, BigDecimal.ROUND_HALF_UP));
    } // else

    // if ( "C".equalsIgnoreCase( tmct0alc.getTmct0alo().getCdtpoper().trim() )
    // ) {
    // tmct0alc.setImnfiliq( tmct0alc.getImefeagr().add( imTotCanones ).add(
    // tmct0alc.getImcomsvb() ).add(
    // tmct0alc.getImcombco() )
    // .add( tmct0alc.getImcomdvo() ).setScale( 2, BigDecimal.ROUND_HALF_UP ) );
    // } else {
    // tmct0alc.setImnfiliq( tmct0alc.getImefeagr().subtract( imTotCanones
    // ).subtract( tmct0alc.getImcomsvb() )
    // .subtract( tmct0alc.getImcombco() ).subtract( tmct0alc.getImcomdvo()
    // ).setScale( 2, BigDecimal.ROUND_HALF_UP ) );
    // } // else
    LOG.trace("## ALCEData ## calcularNetoClienteFinal ## IMNFILIQ: {} ", tmct0alc.getImnfiliq());

    // Se establece el campo IMNETLIQ con el mismo valor que IMNFILIQ
    tmct0alc.setImnetliq(tmct0alc.getImnfiliq());
    LOG.trace("## ALCEData ## calcularNetoClienteFinal ## IMNETLIQ: {} ", tmct0alc.getImnetliq());

    repartirNetoClienteFinal(cltePayCanonComp, cltPayCanonContr, cltePayCanonLiq, tmct0alc, dcts);

    LOG.trace("## ALCEData ## calcularNetoClienteFinal ## Fin");
  } // calcularNetoClienteFinal

  /**
   * Calcula el neto final si el cliente paga canones <br>
   * Si es compra<br>
   * Efectivo + Canones que paga + Com.Cliente + Com.Ordenante+ Com.Banco +
   * Com.Ajuste<br>
   * Si es venta<br>
   * Efectivo - Canones que paga - Com.Cliente - Com.Ordenante - Com.Banco +
   * Com.Ajuste<br>
   * 
   */
  protected void calcularNetoClienteFinal() {
    calcularNetoClienteFinal(getCltePayCanonComp(), getCltePayCanonContr(), getCltePayCanonLiq(), getTmct0alc(),
        getActiveDesgloseCliTitList());

  } // calcularNetoClienteFinal

  /*
   * ~~~~~~~~~~~~~~~~~~~~ NETO CLIENTE SI EL CLIENTE NO ASUME GASTOS
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * Calcula el neto final cliente del 'alc', lo inserta en el campo 'IMNFILIQ'
   * y lo reparte entre sus 'alds'.
   * 
   * @ Si ocurre algún error al obtener el gestor de acceso a base de datos.
   */
  protected void calcularNetoClienteFinalPrecioNetoMedio() {
    LOG.trace("## ALCEData ## calcularNetoClienteFinalPrecioNetoMedio ## Inicio");
    BigDecimal nuevoNeto = tmct0alc.getTmct0alo().getImtotnet().multiply(tmct0alc.getNutitliq())
        .divide(tmct0alc.getTmct0alo().getNutitcli(), 2, BigDecimal.ROUND_HALF_UP);
    LOG.trace("## ALCEData ## calcularNetoClienteFinalPrecioNetoMedio ## nuevoNeto: {}", nuevoNeto);

    tmct0alc.setImnfiliq(nuevoNeto);
    LOG.trace("## ALCEData ## calcularNetoClienteFinalPrecioNetoMedio ## IMNFILIQ: {}", tmct0alc.getImnfiliq());

    // Se establece el campo IMNETLIQ con el mismo valor que IMNFILIQ
    tmct0alc.setImnetliq(nuevoNeto);
    LOG.trace("## ALCEData ## calcularNetoClienteFinalPrecioNetoMedio ## IMNETLIQ: {}", tmct0alc.getImnetliq());

    repartirNetoClienteFinal(getCltePayCanonComp(), getCltePayCanonContr(), getCltePayCanonLiq(), getTmct0alc(),
        getActiveDesgloseCliTitList());

    LOG.trace("## ALCEData ## calcularNetoClienteFinalPrecioNetoMedio ## Fin");
  } // calcularNetoClienteFinalPrecioNetoMedio

  // /**
  // *
  // * @return
  // */
  // private BigDecimal getComparacionGE() {
  // LOG.trace("## ALCEData ## getComparacionGE ## Inicio");
  // BigDecimal comparacionGE = BigDecimal.ZERO;
  //
  // BigDecimal imTotCanones = BigDecimal.ZERO;
  // imTotCanones =
  // imTotCanones.add(tmct0alc.getImcanoncompeu().add(tmct0alc.getImcanoncontreu()
  // .add(tmct0alc.getImcanonliqeu())));
  //
  // LOG.trace("## ALCEData ## getComparacionGE ## imTotCanones: {} ",
  // imTotCanones);
  // LOG.trace("## ALCEData ## getComparacionGE ## Operacion: {} ",
  // tmct0alc.getTmct0alo().getCdtpoper());
  //
  // if (new Character('C').compareTo(tmct0alc.getTmct0alo().getCdtpoper()) ==
  // 0) {
  // comparacionGE =
  // tmct0alc.getImefeagr().add(tmct0alc.getImcomisn()).add(tmct0alc.getImcomdvo()).add(imTotCanones)
  // .add(tmct0alc.getImcombco()).setScale(2, BigDecimal.ROUND_HALF_UP);
  //
  // } else {
  // comparacionGE =
  // tmct0alc.getImefeagr().subtract(tmct0alc.getImcomisn()).subtract(tmct0alc.getImcomdvo())
  // .subtract(imTotCanones).subtract(tmct0alc.getImcombco())
  // .setScale(2, BigDecimal.ROUND_HALF_UP);
  // } // else
  //
  // LOG.trace("## ALCEData ## getComparacionGE ## comparacionGE: {} ",
  // comparacionGE);
  // return comparacionGE;
  // } // getComparacionGE

  // /**
  // * Ajusta la diferencia entre el neto mercado anterior a la modificación
  // contra la comisión cliente. Se recalcula la
  // * comisión SV, la comisión de liquidación y el neto mercado.
  // *
  // * @param ajuste Ajuste.
  // */
  // private void ajusteComisionClientePorComparacionGE(BigDecimal ajuste) {
  // LOG.trace("## ALCEData ## ajusteComisionClientePorComparacionGE ## Inicio");
  // LOG.trace("## ALCEData ## ajusteComisionClientePorComparacionGE ## ajuste: "
  // + ajuste);
  //
  // LOG.trace("## ALCEData ## ajusteComisionClientePorComparacionGE ## Imcomisn antes: "
  // + tmct0alc.getImcomisn());
  // tmct0alc.setImcomisn(tmct0alc.getImcomisn().subtract(ajuste).setScale(2,
  // BigDecimal.ROUND_HALF_UP));
  // LOG.trace("## ALCEData ## ajusteComisionClientePorComparacionGE ## Imcomisn despues: "
  // + tmct0alc.getImcomisn());
  // calcularPriceAjustment();
  // calcularComisionLiquidacion(); // Depende de la comisión cliente
  // calcularProfitCommission(); // Depende de la comision liquidacion
  // LOG.trace("## ALCEData ## ajusteComisionClientePorComparacionGE ## Fin");
  // } // ajusteComisionClientePorComparacionGE

  /* ~~~~~~~~~~~~~~~~~~~~ AJUSTE POR DIFERENCIAS CON ALO ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Ajusta la diferencia entre el neto mercado anterior a la modificación
   * contra la comisión cliente. Se recalcula la comisión SV, la comisión de
   * liquidación y el neto mercado.
   * 
   * @param oldNetoClienteFinal Anterior neto.
   */
  protected void ajusteRepartirNetoClienteFinal(BigDecimal oldNetoClienteFinal) {
    LOG.trace("## ALCEData ## ajusteRepartirNetoClienteFinal ## Inicio");
    if (isCanAllocateTmct0alc()) {
      ajustePorNetoClienteFinalPreAud(oldNetoClienteFinal);
    }
    else {
      LOG.error("## ALCEData ## ajusteRepartirNetoClienteFinal ## NO ALOCATE");
      // ajustePorNetoClienteFinalPostAud(oldNetoClienteFinal);
    } // else

    // repartirNetoClienteFinal();
    LOG.trace("## ALCEData ## ajusteRepartirNetoClienteFinal ## Fin");
  } // ajusteRepartirNetoClienteFinal

  /*
   * ~~~~~~~~~~~~~~~~~~~~ AJUSTE POR MODIFICACIÓN DE NETO CLIENTE FINAL
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * Ajusta la diferencia entre el neto mercado anterior a la modificación
   * contra la comisión cliente. Se recalcula la comisión SV, la comisión de
   * liquidación y el neto mercado.
   * 
   * @param oldNetoClienteFinal Anterior neto.
   */
  public void ajustePorNetoClienteFinalPreAud(BigDecimal oldNetoClienteFinal) {
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPreAud ## Inicio");
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPreAud ## OLD tmct0alc.imnfiliq: {} ", oldNetoClienteFinal);
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPreAud ## NEW tmct0alc.imnfiliq: {} ", tmct0alc.getImnfiliq());

    BigDecimal ajuste = BigDecimal.ZERO;
    if ('C' == this.tmct0alc.getTmct0alo().getCdtpoper()) {
      ajuste = ajuste.add(this.tmct0alc.getImnfiliq().subtract(oldNetoClienteFinal));
    }
    else {
      ajuste = ajuste.add(oldNetoClienteFinal.subtract(this.tmct0alc.getImnfiliq()));
    } // else

    LOG.trace("ajustePorNetoClienteFinalPreAud ajuste == {} ", ajuste);
    LOG.trace("ajustePorNetoClienteFinalPreAud OLD tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
    tmct0alc.setImajusvb(tmct0alc.getImajusvb().add(ajuste));
    LOG.trace("ajustePorNetoClienteFinalPreAud NEW tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
    repartirAjustmentCommission();
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPreAud ## Fin");

  } // ajustePorNetoClienteFinalPreAud

  /**
   * @param oldNetoClienteFinal
   */
  public void ajustePorNetoClienteFinalPostAud(BigDecimal oldNetoClienteFinal) {
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPostAud ## Inicio");

    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPostAud ## OLD tmct0alc.imnfiliq: {} ", oldNetoClienteFinal);
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPostAud ## NEW tmct0alc.imnfiliq: {} ", tmct0alc.getImnfiliq());
    LOG.trace("ajustePorNetoClienteFinalPostAud() ## tmct0alc.cdenvliq == {} ", tmct0alc.getCdenvliq());
    if (TiposComision.PARTENON_ROUTING.equals(tmct0alc.getCdenvliq().trim())
        || TiposComision.CORRETAJE_PTI.equals(tmct0alc.getCdenvliq().trim())) {

      if (!getCltePayCanonComp() && !getCltePayCanonContr() && !getCltePayCanonLiq()) {
        LOG.trace("ajustePorNetoClienteFinalPostAud() ## El cliente NO paga los gastos.");
        BigDecimal ajuste = BigDecimal.ZERO;
        if ('C' == this.tmct0alc.getTmct0alo().getCdtpoper()) {
          ajuste = ajuste.add(this.tmct0alc.getImnfiliq().subtract(oldNetoClienteFinal));
        }
        else {
          ajuste = ajuste.add(oldNetoClienteFinal.subtract(this.tmct0alc.getImnfiliq()));
        } // else

        LOG.trace("ajustePorNetoClienteFinalPostAud ajuste == {} ", ajuste);
        LOG.trace("ajustePorNetoClienteFinalPostAud OLD tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
        tmct0alc.setImajusvb(tmct0alc.getImajusvb().add(ajuste));
        LOG.trace("ajustePorNetoClienteFinalPostAud NEW tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
      }

    }
    else if (TiposComision.CLEARING.equals(tmct0alc.getCdenvliq().trim())
        && tmct0alc.getEstadoentrec().getIdestado() < EstadosEnumerados.CLEARING.LIQUIDADA.getId()
        || TiposComision.FACTURACION.equals(tmct0alc.getCdenvliq().trim())
        && tmct0alc.getEstadocont().getIdestado() <= EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA
            .getId()) {

      if (TiposComision.CLEARING.equals(tmct0alc.getCdenvliq().trim())) {
        LOG.trace("ajustePorNetoClienteFinalPostAud() ## Clearing < EstadosEnumerados.CLEARING.LIQUIDADA.getId()");
      }
      else if (TiposComision.FACTURACION.equals(tmct0alc.getCdenvliq().trim())) {
        LOG.trace("ajustePorNetoClienteFinalPostAud() ## Facturacion <= EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId()");
      }
      BigDecimal ajuste = BigDecimal.ZERO;
      if ('C' == this.tmct0alc.getTmct0alo().getCdtpoper()) {
        ajuste = ajuste.add(this.tmct0alc.getImnfiliq().subtract(oldNetoClienteFinal));
      }
      else {
        ajuste = ajuste.add(oldNetoClienteFinal.subtract(this.tmct0alc.getImnfiliq()));
      } // else

      LOG.trace("ajustePorNetoClienteFinalPostAud ajuste == {} ", ajuste);
      LOG.trace("ajustePorNetoClienteFinalPostAud OLD tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
      tmct0alc.setImajusvb(tmct0alc.getImajusvb().add(ajuste));
      LOG.trace("ajustePorNetoClienteFinalPostAud NEW tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
    }
    repartirAjustmentCommission();
    LOG.trace("## ALCEData ## ajustePorNetoClienteFinalPostAud ## Fin");
  } // ajustePorNetoClienteFinalPostAud

  /*
   * ~~~~~~~~~~~~~~~~~~~~ AJUSTE POR MODIFICACIÓN DE COMISIÓN DE BANCO
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * 
   * @param oldComisionBanco @
   */
  public void ajustePorComisionBancoPreAud(BigDecimal oldComisionBanco) {
    LOG.trace("## ALCEData ## ajustePorComisionBancoPreAud ## Inicio");
    calcularComisionBanco();

    LOG.trace("ajustePorComisionBancoPreAud OLD tmct0alc.imcombco == {} ", oldComisionBanco);
    LOG.trace("ajustePorComisionBancoPreAud NEW tmct0alc.imcombco == {} ", tmct0alc.getImcombco());

    if (getCltePayCanonComp() || getCltePayCanonContr() || getCltePayCanonLiq()) {
      LOG.trace("ajustePorComisionBancoPreAud() ## El cliente paga los gastos.");

      LOG.trace("ajustePorComisionBancoPreAud OLD tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());
      calcularNetoClienteFinal();
      LOG.trace("ajustePorComisionBancoPreAud NEW tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());

      LOG.trace("ajustePorComisionBancoPreAud OLD tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());
      calcularComisionLiquidacion();
      LOG.trace("ajustePorComisionBancoPreAud NEW tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());

    }
    else {
      LOG.trace("ajustePorComisionBancoPreAud() ## El cliente NO paga los gastos.");

      LOG.trace("ajustePorComisionBancoPreAud OLD tmct0alc.imcomsvb == {} ", tmct0alc.getImcomsvb());
      calcularProfitCommission();
      LOG.trace("ajustePorComisionBancoPreAud NEW tmct0alc.imcomsvb == {} ", tmct0alc.getImcomsvb());
    }

  } // ajustePorComisionBancoPreAud

  /**
   *
   * @param oldComisionBanco @
   */
  public void ajustePorComisionBancoPostAud(BigDecimal oldComisionBanco) {

    calcularComisionBanco();

    LOG.trace("ajustePorComisionBancoPostAud OLD tmct0alc.imcombco == {} ", oldComisionBanco);
    LOG.trace("ajustePorComisionBancoPostAud NEW tmct0alc.imcombco == {} ", tmct0alc.getImcombco());

    LOG.trace("ajustePorComisionBancoPostAud() ## tmct0alc.cdenvliq == {} ", tmct0alc.getCdenvliq());
    if (TiposComision.PARTENON_ROUTING.equals(tmct0alc.getCdenvliq().trim())
        || TiposComision.CORRETAJE_PTI.equals(tmct0alc.getCdenvliq().trim())) {

      if (!getCltePayCanonComp() && !getCltePayCanonContr() && !getCltePayCanonLiq()) {
        LOG.trace("ajustePorComisionBancoPostAud() ## El cliente NO paga los gastos.");
        BigDecimal ajuste = BigDecimal.ZERO;
        if ('C' == this.tmct0alc.getTmct0alo().getCdtpoper()) {
          ajuste = ajuste.add(this.tmct0alc.getImcombco().subtract(oldComisionBanco));
        }
        else {
          ajuste = ajuste.add(oldComisionBanco.subtract(this.tmct0alc.getImcombco()));
        } // else

        LOG.trace("ajustePorComisionBancoPostAud ajuste == {} ", ajuste);
        LOG.trace("ajustePorComisionBancoPostAud OLD tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
        tmct0alc.setImajusvb(tmct0alc.getImajusvb().add(ajuste));
        LOG.trace("ajustePorComisionBancoPostAud NEW tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
      }

    }
    else if (TiposComision.CLEARING.equals(tmct0alc.getCdenvliq().trim())
        && tmct0alc.getEstadoentrec().getIdestado() < EstadosEnumerados.CLEARING.LIQUIDADA.getId()
        || TiposComision.FACTURACION.equals(tmct0alc.getCdenvliq().trim())
        && tmct0alc.getEstadocont().getIdestado() <= EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA
            .getId()) {

      if (TiposComision.CLEARING.equals(tmct0alc.getCdenvliq().trim())) {
        LOG.trace("ajustePorComisionBancoPostAud() ## Clearing < EstadosEnumerados.CLEARING.LIQUIDADA.getId()");
      }
      else if (TiposComision.FACTURACION.equals(tmct0alc.getCdenvliq().trim())) {
        LOG.trace("ajustePorComisionBancoPostAud() ## Facturacion <= EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId()");
      }
      if (getCltePayCanonComp() || getCltePayCanonContr() || getCltePayCanonLiq()) {
        LOG.trace("ajustePorComisionBancoPostAud() ## El cliente paga los gastos.");
        LOG.trace("ajustePorComisionBancoPostAud OLD tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());
        calcularNetoClienteFinal();
        LOG.trace("ajustePorComisionBancoPostAud NEW tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());

        LOG.trace("ajustePorComisionBancoPostAud OLD tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());
        calcularComisionLiquidacion();
        LOG.trace("ajustePorComisionBancoPostAud NEW tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());

      }
      else {
        LOG.trace("ajustePorComisionBancoPostAud() ## El cliente NO paga los gastos.");

        LOG.trace("ajustePorComisionBancoPostAud OLD tmct0alc.imcomsvb == {} ", tmct0alc.getImcomsvb());
        calcularProfitCommission();
        LOG.trace("ajustePorComisionBancoPostAud NEW tmct0alc.imcomsvb == {} ", tmct0alc.getImcomsvb());
      }
    }
    repartirAjustmentCommission();
    // BigDecimal ajuste = BigDecimal.ZERO;

  } // ajustePorComisionBancoPostAud

  /*
   * ~~~~~~~~~~~~~~~~~~~~ AJUSTE POR MODIFICACIÓN DE COMISIÓN ORDENANTE. PAYER
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * 
   * @param oldComisionOrdenante @
   */
  public void ajustePorComisionOrdenantePreAud(BigDecimal oldComisionOrdenante) {
    calcularComisionOrdenante();
    LOG.trace("ajustePorComisionOrdenantePreAud OLD tmct0alc.imcomdvo == {} ", oldComisionOrdenante);
    LOG.trace("ajustePorComisionOrdenantePreAud NEW tmct0alc.imcomdvo == {} ", tmct0alc.getImcomdvo());

    LOG.trace("ajustePorComisionOrdenantePreAud OLD tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());
    calcularNetoClienteFinal();
    LOG.trace("ajustePorComisionOrdenantePreAud NEW tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());

    LOG.trace("ajustePorComisionOrdenantePreAud OLD tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());
    calcularComisionLiquidacion();
    LOG.trace("ajustePorComisionOrdenantePreAud NEW tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());
  } // ajustePorComisionOrdenantePreAud

  /**
   *
   * @param oldComisionOrdenante @
   */
  public void ajustePorComisionOrdenantePostAud(BigDecimal oldComisionOrdenante) {
    calcularComisionOrdenante();

    LOG.trace("ajustePorComisionOrdenantePostAud OLD tmct0alc.imcomdvo == {} ", oldComisionOrdenante);
    LOG.trace("ajustePorComisionOrdenantePostAud NEW tmct0alc.imcomdvo == {} ", tmct0alc.getImcomdvo());

    LOG.trace("ajustePorComisionOrdenantePostAud() ## tmct0alc.cdenvliq == {} ", tmct0alc.getCdenvliq());
    if (TiposComision.PARTENON_ROUTING.equals(tmct0alc.getCdenvliq().trim())
        || TiposComision.CORRETAJE_PTI.equals(tmct0alc.getCdenvliq().trim())) {

      if (!getCltePayCanonComp() && !getCltePayCanonContr() && !getCltePayCanonLiq()) {
        LOG.trace("ajustePorComisionOrdenantePostAud() ## El cliente NO paga los gastos.");
        BigDecimal ajuste = BigDecimal.ZERO;
        if ('C' == this.tmct0alc.getTmct0alo().getCdtpoper()) {
          ajuste = ajuste.add(this.tmct0alc.getImcomdvo().subtract(oldComisionOrdenante));
        }
        else {
          ajuste = ajuste.add(oldComisionOrdenante.subtract(this.tmct0alc.getImcomdvo()));
        } // else

        LOG.trace("ajustePorComisionOrdenantePostAud ajuste == {} ", ajuste);
        LOG.trace("ajustePorComisionOrdenantePostAud OLD tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
        tmct0alc.setImajusvb(tmct0alc.getImajusvb().add(ajuste));
        LOG.trace("ajustePorComisionOrdenantePostAud NEW tmct0alc.imajusvb == {} ", tmct0alc.getImajusvb());
      }

    }
    else if (TiposComision.CLEARING.equals(tmct0alc.getCdenvliq().trim())
        && tmct0alc.getEstadoentrec().getIdestado() < EstadosEnumerados.CLEARING.LIQUIDADA.getId()
        || TiposComision.FACTURACION.equals(tmct0alc.getCdenvliq().trim())
        && tmct0alc.getEstadocont().getIdestado() <= EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA
            .getId()) {

      if (TiposComision.CLEARING.equals(tmct0alc.getCdenvliq().trim())) {
        LOG.trace("ajustePorComisionOrdenantePostAud() ## Clearing < EstadosEnumerados.CLEARING.LIQUIDADA.getId()");
      }
      else if (TiposComision.FACTURACION.equals(tmct0alc.getCdenvliq().trim())) {
        LOG.trace("ajustePorComisionOrdenantePostAud() ## Facturacion <= EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId()");
      }

      LOG.trace("ajustePorComisionOrdenantePostAud OLD tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());
      calcularNetoClienteFinal();
      LOG.trace("ajustePorComisionOrdenantePostAud NEW tmct0alc.imnfiliq == {} ", tmct0alc.getImnfiliq());

      LOG.trace("ajustePorComisionOrdenantePostAud OLD tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());
      calcularComisionLiquidacion();
      LOG.trace("ajustePorComisionOrdenantePostAud NEW tmct0alc.imfinsvb == {} ", tmct0alc.getImfinsvb());

    }
    repartirAjustmentCommission();
  } // ajustePorComisionOrdenantePostAud

  /*
   * ~~~~~~~~~~~~~~~~~~~~ AJUSTE POR MODIFICACIÓN DE COMISIÓN DEVOLUCION. REBATE
   * ~~~~~~~~~~~~~~~~~~~~
   */

  /**
   * 
   * @param oldComisionDevolucion @
   */
  public void ajustePorComisionDevolucionPreAud() {
    calcularRebate();
  } // ajustePorComisionDevolucionPreAud

  /**
   *
   * @param oldComisionDevolucion @
   */
  public void ajustePorComisionDevolucionPostAud() {
    calcularRebate();
  } // ajustePorComisionDevolucionPostAud

  // public void sumarizarBrutoMercadoFromAud() {
  // Collection<AllocateNacionalTmct0aldEconomicData> tmct0alds =
  // getListActiveAndSortedByReadOnlyTmct0ald();
  //
  // BigDecimal imefeagr = BigDecimal.ZERO;
  // for (AllocateNacionalTmct0aldEconomicData
  // allocateNacionalTmct0aldEconomicData : tmct0alds) {
  // if (!allocateNacionalTmct0aldEconomicData.isReject()) {
  // imefeagr =
  // imefeagr.add(allocateNacionalTmct0aldEconomicData.getTmct0ald().getImefeaud());
  // }
  // }
  // tmct0alc.setImefeagr(imefeagr.setScale(2, BigDecimal.ROUND_HALF_UP));
  // }

  // public void calcularComisionBancoPostAud() {
  // calcularComisionBancoIntern();
  // repartirComisionBanco();
  //
  // }

  // private void ajusteComisionClienteComisionBanco() {
  // if (BigDecimal.ZERO.compareTo(this.tmct0alc.getImcombco()) != 0) {
  // if ("C".equalsIgnoreCase(this.tmct0alc.getTmct0alo().getCdtpoper().trim()))
  // {
  // this.tmct0alc.setImcomisn(this.tmct0alc.getImcomisn().subtract(this.tmct0alc.getImcombco())
  // .setScale(2, BigDecimal.ROUND_HALF_UP));
  // } else {
  // this.tmct0alc.setImcomisn(this.tmct0alc.getImcomisn().add(this.tmct0alc.getImcombco())
  // .setScale(2, BigDecimal.ROUND_HALF_UP));
  // }
  // repartirComisionCliente();
  // }
  //
  // }

  // private void repartirAjusteSvb() {
  // BigDecimal acumulado = BigDecimal.ZERO;
  // Collection<AllocateNacionalTmct0aldEconomicData> collection =
  // getListActiveAndSortedByReadOnlyTmct0ald();
  // int i = 0;
  // for (AllocateNacionalTmct0aldEconomicData tmct0aldEconomicData :
  // collection) {
  // i++;
  // if (!this.updateReadOnlyData && tmct0aldEconomicData.isReadOnlyTmct0ald())
  // {
  // acumulado =
  // acumulado.add(tmct0aldEconomicData.getTmct0ald().getImajusvb());
  // } else {
  // if (i != collection.size()) {
  // BigDecimal imajusvb =
  // this.tmct0alc.getImajusvb().multiply(tmct0aldEconomicData.getTmct0ald().getImefeaud())
  // .divide(this.tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
  // tmct0aldEconomicData.getTmct0ald().setImajusvb(imajusvb);
  //
  // acumulado =
  // acumulado.add(tmct0aldEconomicData.getTmct0ald().getImajusvb());
  // } else {
  // tmct0aldEconomicData.getTmct0ald().setImajusvb(this.tmct0alc.getImajusvb().subtract(acumulado)
  // .setScale(2, BigDecimal.ROUND_HALF_UP));
  // }
  // }
  // }
  // }

  // public void ajusteRepartirNetoClienteFidessa(BigDecimal acumulado) {
  // this.tmct0alc.setImtotnet(this.tmct0alc.getTmct0alo().getImtotnet()
  // .subtract(acumulado.subtract(this.tmct0alc.getImtotnet())));
  // repartirNetoClienteFidessa();
  // }

  public static void inicializarNullsTmct0alc(Tmct0alc tmct0alc) {
    LOG.trace("inicializarNullsTmct0alc Inicio {}", tmct0alc.getId());
    if (tmct0alc.getNutitliq() == null) {
      tmct0alc.setNutitliq(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setNutitliq to 0.0");
    }
    if (tmct0alc.getImajusvb() == null) {
      tmct0alc.setImajusvb(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImajusvb to 0.0");
    }

    if (tmct0alc.getImefeagr() == null) {
      tmct0alc.setImefeagr(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImefeagr to 0.0");
    }

    if (tmct0alc.getImcombco() == null) {
      tmct0alc.setImcombco(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcombco to 0.0");
    }

    if (tmct0alc.getImcomisn() == null) {
      tmct0alc.setImcomisn(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcomisn to 0.0");
    }

    if (tmct0alc.getImcombrk() == null) {
      tmct0alc.setImcombrk(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcombrk to 0.0");
    }
    if (tmct0alc.getImcomdvo() == null) {
      tmct0alc.setImcomdvo(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcomdvo to 0.0");
    }
    if (tmct0alc.getImcomsvb() == null) {
      tmct0alc.setImcomsvb(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcomsvb to 0.0");
    }
    if (tmct0alc.getImfinsvb() == null) {
      tmct0alc.setImfinsvb(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImfinsvb to 0.0");
    }
    if (tmct0alc.getImnfiliq() == null) {
      tmct0alc.setImnfiliq(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImnfiliq to 0.0");
    }
    if (tmct0alc.getImnetliq() == null) {
      tmct0alc.setImnetliq(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImnetliq to 0.0");
    }
    if (tmct0alc.getImntbrok() == null) {
      tmct0alc.setImntbrok(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImntbrok to 0.0");
    }
    if (tmct0alc.getImtotbru() == null) {
      tmct0alc.setImtotbru(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImtotbru to 0.0");
    }
    if (tmct0alc.getImtotnet() == null) {
      tmct0alc.setImtotnet(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImtotnet to 0.0");
    }

    if (tmct0alc.getImderges() == null) {
      tmct0alc.setImderges(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImderges to 0.0");
    }

    if (tmct0alc.getImcanoncontreu() == null) {
      tmct0alc.setImcanoncontreu(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcanoncontreu to 0.0");
    }

    if (tmct0alc.getImcanoncontrdv() == null) {
      tmct0alc.setImcanoncontrdv(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcanoncontrdv to 0.0");
    }

    if (tmct0alc.getImderliq() == null) {
      tmct0alc.setImderliq(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImderliq to 0.0");
    }

    if (tmct0alc.getImcanonliqeu() == null) {
      tmct0alc.setImcanonliqeu(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcanonliqeu to 0.0");
    }

    if (tmct0alc.getImcanonliqdv() == null) {
      tmct0alc.setImcanonliqdv(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcanonliqdv to 0.0");

    }

    if (tmct0alc.getImcanoncompeu() == null) {
      tmct0alc.setImcanoncompeu(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcanoncompeu to 0.0");
    }

    if (tmct0alc.getImcanoncompdv() == null) {
      tmct0alc.setImcanoncompdv(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcanoncompdv to 0.0");
    }

    if (tmct0alc.getImbansis() == null) {
      tmct0alc.setImbansis(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImbansis to 0.0");
    }
    if (tmct0alc.getImcomsis() == null) {
      tmct0alc.setImcomsis(BigDecimal.ZERO);
      LOG.trace("inicializarNullsTmct0alc tmct0alc.setImcomsis to 0.0");
    }
    LOG.trace("inicializarNullsTmct0alc Fin {}", tmct0alc.getId());
  }// clearEconomicData

  public static void inicializarNullsDesgloseCliTit(Tmct0desgloseclitit dct) {
    LOG.trace("inicializarNullsDesgloseCliTit Inicio {}", dct.getNudesglose());
    if (dct.getImtitulos() == null) {
      dct.setImtitulos(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImtitulos to 0.0");
    }
    if (dct.getImajusvb() == null) {
      dct.setImajusvb(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImajusvb to 0.0");
    }

    if (dct.getImefectivo() == null) {
      dct.setImefectivo(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImefectivo to 0.0");
    }

    if (dct.getImcombco() == null) {
      dct.setImcombco(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcombco to 0.0");
    }

    if (dct.getImcomisn() == null) {
      dct.setImcomisn(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcomisn to 0.0");
    }

    if (dct.getImcombrk() == null) {
      dct.setImcombrk(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcombrk to 0.0");
    }
    if (dct.getImcomdvo() == null) {
      dct.setImcomdvo(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcomdvo to 0.0");
    }
    if (dct.getImcomsvb() == null) {
      dct.setImcomsvb(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcomsvb to 0.0");
    }
    if (dct.getImfinsvb() == null) {
      dct.setImfinsvb(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImfinsvb to 0.0");
    }
    if (dct.getImnfiliq() == null) {
      dct.setImnfiliq(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImnfiliq to 0.0");
    }
    if (dct.getImnetliq() == null) {
      dct.setImnetliq(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImnetliq to 0.0");
    }
    if (dct.getImntbrok() == null) {
      dct.setImntbrok(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImntbrok to 0.0");
    }
    if (dct.getImtotbru() == null) {
      dct.setImtotbru(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImtotbru to 0.0");
    }
    if (dct.getImtotnet() == null) {
      dct.setImtotnet(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImtotnet to 0.0");
    }

    if (dct.getImcanoncontreu() == null) {
      dct.setImcanoncontreu(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanoncontreu to 0.0");
    }

    if (dct.getImcanoncontrdv() == null) {
      dct.setImcanoncontrdv(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanoncontrdv to 0.0");
    }

    if (dct.getImcanonliqeu() == null) {
      dct.setImcanonliqeu(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanonliqeu to 0.0");
    }

    if (dct.getImcanonliqdv() == null) {
      dct.setImcanonliqdv(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanonliqdv to 0.0");

    }

    if (dct.getImcanoncompeu() == null) {
      dct.setImcanoncompeu(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanoncompeu to 0.0");
    }

    if (dct.getImcanoncompdv() == null) {
      dct.setImcanoncompdv(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanoncompdv to 0.0");
    }

    if (dct.getImbansis() == null) {
      dct.setImbansis(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImbansis to 0.0");
    }
    if (dct.getImcomsis() == null) {
      dct.setImcomsis(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcomsis to 0.0");
    }

    if (dct.getImcobrado() == null) {
      dct.setImcobrado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcobrado to 0.0");
    }
    // if (dct.getImconciliado() == null) {
    // dct.setImconciliado(BigDecimal.ZERO);
    // LOG.trace("inicializarNullsDesgloseCliTit dct.setImconciliado to 0.0");
    // }
    if (dct.getImefeMercadoCobrado() == null) {
      dct.setImefeMercadoCobrado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImefeMercadoCobrado to 0.0");
    }
    if (dct.getImefeClienteCobrado() == null) {
      dct.setImefeClienteCobrado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImefeClienteCobrado to 0.0");
    }
    if (dct.getImcanonCobrado() == null) {
      dct.setImcanonCobrado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcanonCobrado to 0.0");
    }
    if (dct.getImcomCobrado() == null) {
      dct.setImcomCobrado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImcomCobrado to 0.0");
    }
    if (dct.getImbrkPagado() == null) {
      dct.setImbrkPagado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImbrkPagado to 0.0");
    }
    if (dct.getImdvoPagado() == null) {
      dct.setImdvoPagado(BigDecimal.ZERO);
      LOG.trace("inicializarNullsDesgloseCliTit dct.setImdvoPagado to 0.0");
    }
    LOG.trace("inicializarNullsDesgloseCliTit Fin {}", dct.getNudesglose());
  }// clearEconomicData

  public static void clearEconomicData(CalculosEconomicosFlagsDTO flags, Tmct0alc tmct0alc) {
    inicializarNullsTmct0alc(tmct0alc);
    tmct0alc.setNutitliq(BigDecimal.ZERO);
    LOG.trace("clearEconomicData tmct0alc.setNutitliq to 0.0");
    if (flags.isRepartirComisionAjuste()) {
      tmct0alc.setImajusvb(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImajusvb to 0.0");
    }
    if (flags.isCalcularBrutoMercado()) {
      tmct0alc.setImefeagr(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImefeagr to 0.0");
    }

    if (flags.isCalcularComisionBanco()) {
      tmct0alc.setImcombco(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcombco to 0.0");
    }

    if (flags.isCalcularComisionCliente()) {
      tmct0alc.setImcomisn(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcomisn to 0.0");
    }

    if (flags.isCalcularComisionDevolucion()) {
      tmct0alc.setImcombrk(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcombrk to 0.0");
    }
    if (flags.isRepartirComisionOrdenante()) {
      tmct0alc.setImcomdvo(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcomdvo to 0.0");
    }
    if (flags.isCalcularComisionBeneficio()) {
      tmct0alc.setImcomsvb(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcomsvb to 0.0");
    }
    if (flags.isCalcularComisionLiquidacion()) {
      tmct0alc.setImfinsvb(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImfinsvb to 0.0");
    }
    if (flags.isCalcularNetoCliente()) {
      tmct0alc.setImnfiliq(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImnfiliq to 0.0");
      tmct0alc.setImnetliq(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImnetliq to 0.0");
    }
    if (flags.isCalcularAjustePrecio()) {
      tmct0alc.setImntbrok(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImntbrok to 0.0");
    }
    if (flags.isRepartirBrutoClienteFidessa()) {
      tmct0alc.setImtotbru(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImtotbru to 0.0");
    }
    if (flags.isRepartirNetoClienteFidessa()) {
      tmct0alc.setImtotnet(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImtotnet to 0.0");
    }

    if (flags.isCalcularCanones()) {
      tmct0alc.setImderges(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImderges to 0.0");
      tmct0alc.setImcanoncontreu(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanoncontreu to 0.0");
      tmct0alc.setImcanoncontrdv(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanoncontrdv to 0.0");
      tmct0alc.setImderliq(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImderliq to 0.0");
      tmct0alc.setImcanonliqeu(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanonliqeu to 0.0");
      tmct0alc.setImcanonliqdv(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanonliqdv to 0.0");

      tmct0alc.setImcanoncompeu(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanoncompeu to 0.0");
      tmct0alc.setImcanoncompdv(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanoncompdv to 0.0");
    }

    if (flags.isCalcularImbansis()) {
      tmct0alc.setImbansis(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImbansis to 0.0");
    }
    if (flags.isCalcularImcomsis()) {
      tmct0alc.setImcomsis(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcomsis to 0.0");
    }

    if (flags.isRepartirImcobrado()) {
      tmct0alc.setImcobrado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcobrado to 0.0");
    }
    // if (flags.isRepartirConciliado()) {
    // tmct0alc.setImconciliado(BigDecimal.ZERO);
    // LOG.trace("clearEconomicData tmct0alc.setImconciliado to 0.0");
    // }

    if (flags.isRepartirImefeMercadoCobrado()) {
      tmct0alc.setImefeMercadoCobrado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImefeMercadoCobrado to 0.0");
    }

    if (flags.isRepartirImefeClienteCobrado()) {
      tmct0alc.setImefeClienteCobrado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImefeClienteCobrado to 0.0");
    }

    if (flags.isRepartirImcanonCobrado()) {
      tmct0alc.setImcanonCobrado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcanonCobrado to 0.0");
    }
    if (flags.isRepartirImcomCobrado()) {
      tmct0alc.setImcomCobrado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImcomCobrado to 0.0");
    }

    if (flags.isRepartirImbrkPagado()) {
      tmct0alc.setImbrkPagado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImbrkPagado to 0.0");
    }

    if (flags.isRepartirImdvoPagado()) {
      tmct0alc.setImdvoPagado(BigDecimal.ZERO);
      LOG.trace("clearEconomicData tmct0alc.setImdvoPagado to 0.0");
    }
  } // clearEconomicData

  public static void acumulateFromExecutionGroup(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> dcts) {
    acumulateFromExecutionGroup(new CalculosEconomicosFlagsDTO(), tmct0alc, dcts);
  }

  protected static void acumulateFromExecutionGroup(CalculosEconomicosFlagsDTO flags, Tmct0alc tmct0alc,
      List<Tmct0desgloseclitit> dcts) {
    LOG.trace("acumulateFromExecutionGroup inicio.");
    clearEconomicData(flags, tmct0alc);
    for (Tmct0desgloseclitit dct : dcts) {
      inicializarNullsDesgloseCliTit(dct);

      tmct0alc.setNutitliq(tmct0alc.getNutitliq().add(dct.getImtitulos()).setScale(0, BigDecimal.ROUND_HALF_UP));
      LOG.trace("acumulateFromExecutionGroup tmct0alc.setNutitliq == {}.", tmct0alc.getNutitliq());
      tmct0alc.setImajusvb(tmct0alc.getImajusvb().add(dct.getImajusvb()).setScale(2, BigDecimal.ROUND_HALF_UP));
      LOG.trace("acumulateFromExecutionGroup tmct0alc.setImajusvb == {}.", tmct0alc.getImajusvb());
      if (flags.isCalcularBrutoMercado()) {
        tmct0alc.setImefeagr(tmct0alc.getImefeagr().add(dct.getImefectivo()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImefeagr == {}.", tmct0alc.getImefeagr());
      }

      if (flags.isCalcularComisionBanco()) {
        tmct0alc.setImcombco(tmct0alc.getImcombco().add(dct.getImcombco()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcombco == {}.", tmct0alc.getImcombco());
      }
      if (flags.isCalcularComisionDevolucion()) {
        tmct0alc.setImcombrk(tmct0alc.getImcombrk().add(dct.getImcombrk()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcombrk == {}.", tmct0alc.getImcombrk());
      }
      if (flags.isRepartirComisionOrdenante()) {
        tmct0alc.setImcomdvo(tmct0alc.getImcomdvo().add(dct.getImcomdvo()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcomdvo == {}.", tmct0alc.getImcomdvo());
      }
      if (flags.isCalcularComisionCliente()) {
        tmct0alc.setImcomisn(tmct0alc.getImcomisn().add(dct.getImcomisn()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcomisn == {}.", tmct0alc.getImcomisn());
      }
      if (flags.isCalcularComisionBeneficio()) {
        tmct0alc.setImcomsvb(tmct0alc.getImcomsvb().add(dct.getImcomsvb()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcomsvb == {}.", tmct0alc.getImcomsvb());
      }
      if (flags.isCalcularCanones()) {
        tmct0alc.setImderges(tmct0alc.getImderges().add(dct.getImcanoncontreu()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImderges == {}.", tmct0alc.getImderges());
        tmct0alc.setImcanoncontreu(tmct0alc.getImcanoncontreu().add(dct.getImcanoncontreu())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcanoncontreu == {}.", tmct0alc.getImcanoncontreu());
        tmct0alc.setImcanoncontrdv(tmct0alc.getImcanoncontrdv().add(dct.getImcanoncontrdv())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcanoncontrdv == {}.", tmct0alc.getImcanoncontrdv());
        tmct0alc.setImderliq(tmct0alc.getImderliq().add(dct.getImcanonliqeu()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImderliq == {}.", tmct0alc.getImderliq());
        tmct0alc.setImcanonliqeu(tmct0alc.getImcanonliqeu().add(dct.getImcanonliqeu())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcanonliqeu == {}.", tmct0alc.getImcanonliqeu());
        tmct0alc.setImcanonliqdv(tmct0alc.getImcanonliqdv().add(dct.getImcanonliqdv())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcanonliqdv == {}.", tmct0alc.getImcanonliqdv());
        tmct0alc.setImcanoncompeu(tmct0alc.getImcanoncompeu().add(dct.getImcanoncompeu())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcanoncompeu == {}.", tmct0alc.getImcanoncompeu());
        tmct0alc.setImcanoncompdv(tmct0alc.getImcanoncompdv().add(dct.getImcanoncompdv())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcanoncompdv == {}.", tmct0alc.getImcanoncompdv());
      }
      if (flags.isCalcularComisionLiquidacion()) {
        tmct0alc.setImfinsvb(tmct0alc.getImfinsvb().add(dct.getImfinsvb()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImfinsvb == {}.", tmct0alc.getImfinsvb());
      }
      if (flags.isCalcularNetoCliente()) {
        tmct0alc.setImnetliq(tmct0alc.getImnetliq().add(dct.getImnetliq()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImnetliq == {}.", tmct0alc.getImnetliq());
        tmct0alc.setImnfiliq(tmct0alc.getImnfiliq().add(dct.getImnfiliq()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImnfiliq == {}.", tmct0alc.getImnfiliq());
      }
      if (flags.isCalcularAjustePrecio()) {
        tmct0alc.setImntbrok(tmct0alc.getImntbrok().add(dct.getImntbrok()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImntbrok == {}.", tmct0alc.getImntbrok());
      }
      if (flags.isRepartirBrutoClienteFidessa()) {
        tmct0alc.setImtotbru(tmct0alc.getImtotbru().add(dct.getImtotbru()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImtotbru == {}.", tmct0alc.getImtotbru());
      }
      if (flags.isRepartirNetoClienteFidessa()) {
        tmct0alc.setImtotnet(tmct0alc.getImtotnet().add(dct.getImtotnet()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImtotnet == {}.", tmct0alc.getImtotnet());
      }
      if (flags.isCalcularImbansis()) {
        tmct0alc.setImbansis(tmct0alc.getImbansis().add(dct.getImbansis()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImbansis == {}.", tmct0alc.getImbansis());
      }

      if (flags.isCalcularImcomsis()) {
        tmct0alc.setImcomsis(tmct0alc.getImcomsis().add(dct.getImcomsis()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcomsis == {}.", tmct0alc.getImcomsis());
      }

      if (flags.isRepartirImcobrado()) {
        tmct0alc.setImcobrado(tmct0alc.getImcobrado().add(dct.getImcobrado()).setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.setImcobrado == {}.", tmct0alc.getImcobrado());
      }
      // if (flags.isRepartirConciliado()) {
      // tmct0alc.setImconciliado(tmct0alc.getImconciliado().add(dct.getImconciliado())
      // .setScale(2, BigDecimal.ROUND_HALF_UP));
      // LOG.trace("acumulateFromExecutionGroup tmct0alc.setImconciliado == {}.",
      // tmct0alc.getImconciliado());
      // }

      if (flags.isRepartirImefeMercadoCobrado()) {
        tmct0alc.setImefeMercadoCobrado(tmct0alc.getImefeMercadoCobrado().add(dct.getImefeMercadoCobrado())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.getImefeMercadoCobrado == {}.",
            tmct0alc.getImefeMercadoCobrado());
      }

      if (flags.isRepartirImefeClienteCobrado()) {
        tmct0alc.setImefeClienteCobrado(tmct0alc.getImefeClienteCobrado().add(dct.getImefeClienteCobrado())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.getImefeClienteCobrado == {}.",
            tmct0alc.getImefeClienteCobrado());
      }

      if (flags.isRepartirImcanonCobrado()) {
        tmct0alc.setImcanonCobrado(tmct0alc.getImcanonCobrado().add(dct.getImcanonCobrado())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.getImcanonCobrado == {}.", tmct0alc.getImcanonCobrado());
      }
      if (flags.isRepartirImcomCobrado()) {
        tmct0alc.setImcomCobrado(tmct0alc.getImcomCobrado().add(dct.getImcomCobrado())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.getImcomCobrado == {}.", tmct0alc.getImcomCobrado());
      }

      if (flags.isRepartirImbrkPagado()) {
        tmct0alc.setImbrkPagado(tmct0alc.getImbrkPagado().add(dct.getImbrkPagado())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.getImbrkPagado == {}.", tmct0alc.getImbrkPagado());
      }

      if (flags.isRepartirImdvoPagado()) {
        tmct0alc.setImdvoPagado(tmct0alc.getImdvoPagado().add(dct.getImdvoPagado())
            .setScale(2, BigDecimal.ROUND_HALF_UP));
        LOG.trace("acumulateFromExecutionGroup tmct0alc.getImdvoPagado == {}.", tmct0alc.getImdvoPagado());
      }
    }
    LOG.trace("acumulateFromExecutionGroup final.");
  }

  public TypeResultProcess acumulateFromExecutionGroup(CalculosEconomicosFlagsDTO flags) {
    acumulateFromExecutionGroup(flags, getTmct0alc(), getActiveDesgloseCliTitList());
    return TypeResultProcess.FINISH_OK;
  }

  /* ~~~~~~~~~~~~~~~~~~~~ MÉTODOS DE UTILIDAD ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Obtiene la lista de los <code>DesgloseCliTit</code> del
   * <code>Tmct0alc</code> que no están rechazados.
   * 
   * @return <code>java.util.List<DesgloseCliTit> - </code>Lista de
   * <code>DesgloseCliTit</code> activos.
   */
  private List<Tmct0desgloseclitit> getActiveDesgloseCliTitList() {
    return activeDesgloseCliTitList;
  } // getActiveDesgloseCliTitList

  /* ~~~~~~~~~~~~~~~~~~~~ GETTERS ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Devuelve el objeto <code>Tmct0alc</code>.
   * 
   * @return <code>sibbac.database.model.corbtsql.Tmct0alc - </code>Objeto
   * Tmct0alc.
   */
  public Tmct0alc getTmct0alc() {
    return tmct0alc;
  } // getTmct0alc

  /**
   * Indica si el objeto <code>Tmct0alc</code> se puede modificar.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el objeto
   * <code>Tmct0alc</code> se puede modificar; <code>false</code> en caso
   * contrario.
   */
  public Boolean isCanAllocateTmct0alc() {
    if (canAllocateTmct0alc == null) {
      canAllocateTmct0alc = this.tmct0alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId();
    } // if
    return canAllocateTmct0alc;
  } // isCanAllocateTmct0alc

  /**
   * Indica si el mercado paga canones de contratación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el mercado
   * paga canones de contratación; <code>false</code> en caso contrario.
   */
  public Boolean getMktPayCanonContr() {
    if (mktPayCanonContr == null) {
      mktPayCanonContr = 1 == tmct0alc.getTmct0alo().getCanoncontrpagomkt();
    }
    return mktPayCanonContr;
  } // getMktPayCanonContr

  /**
   * Indica si el mercado paga canones de compensación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el mercado
   * paga canones de compensación; <code>false</code> en caso contrario.
   */
  public Boolean getMktPayCanonComp() {
    if (mktPayCanonComp == null) {
      mktPayCanonComp = 1 == tmct0alc.getTmct0alo().getCanoncomppagomkt();
    }
    return mktPayCanonComp;
  } // getMktPayCanonComp

  /**
   * Indica si el mercado paga canones de liquidación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el mercado
   * paga canones de liquidación; <code>false</code> en caso contrario.
   */
  public Boolean getMktPayCanonLiq() {
    if (mktPayCanonLiq == null) {
      mktPayCanonLiq = 1 == tmct0alc.getTmct0alo().getCanonliqpagomkt();
    }
    return mktPayCanonLiq;
  } // getMktPayCanonLiq

  /**
   * Indica si el cliente paga canones de contratación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el cliente
   * paga canones de contratación; <code>false</code> en caso contrario.
   */
  public Boolean getCltePayCanonContr() {
    if (cltePayCanonContr == null) {
      cltePayCanonContr = 1 == tmct0alc.getTmct0alo().getCanoncontrpagoclte();
    }
    return cltePayCanonContr;
  } // getCltePayCanonContr

  /**
   * Indica si el cliente paga canones de compensación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el cliente
   * paga canones de compensación; <code>false</code> en caso contrario.
   */
  public Boolean getCltePayCanonComp() {
    if (cltePayCanonComp == null) {
      cltePayCanonComp = 1 == tmct0alc.getTmct0alo().getCanoncomppagoclte();
    }
    return cltePayCanonComp;
  } // getCltePayCanonComp

  /**
   * Indica si el cliente paga canones de liquidación.
   * 
   * @return <code>java.lang.Boolean - </code><code>true</code> si el cliente
   * paga canones de liquidación; <code>false</code> en caso contrario.
   */
  public Boolean getCltePayCanonLiq() {
    if (cltePayCanonLiq == null) {
      cltePayCanonLiq = 1 == tmct0alc.getTmct0alo().getCanonliqpagoclte();
    }
    return cltePayCanonLiq;
  } // getCltePayCanonLiq

  public Tcli0comDTO getTcli0com() {
    return tcli0com;
  }

  public void repartirNetoClienteFidessa() {
    LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## Inicio");
    LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## tmct0alc.getImtotnet(): {}", tmct0alc.getImtotnet());
    LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## tmct0alc.getNutitliq(): {}", tmct0alc.getNutitliq());

    int i = 0;
    BigDecimal imtotnet;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## desgloseCliTit.getImtitulos(): {}",
            desgloseCliTit.getImtitulos());

        imtotnet = desgloseCliTit.getImtitulos().multiply(tmct0alc.getImtotnet())
            .divide(tmct0alc.getNutitliq(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImtotnet(imtotnet);
        acumulado = acumulado.add(imtotnet);
      }
      else {
        LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImtotnet(tmct0alc.getImtotnet().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## desgloseCliTit.getImtotnet(): {}",
          desgloseCliTit.getImtotnet());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirNetoClienteFidessa ## Fin");

  }

  public void repartirComisionCliente() {
    LOG.trace("## ALCEData ## repartirComisionCliente ## Inicio");
    LOG.trace("## ALCEData ## repartirComisionCliente ## tmct0alc.getImcomisn(): {}", tmct0alc.getImcomisn());
    LOG.trace("## ALCEData ## repartirComisionCliente ## tmct0alc.getImtotbru(): {}", tmct0alc.getImtotbru());

    int i = 0;
    BigDecimal imcomisn;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirComisionCliente ## desgloseCliTit.getImtotbru(): {}",
            desgloseCliTit.getImtotbru());

        imcomisn = desgloseCliTit.getImtotbru().multiply(tmct0alc.getImcomisn())
            .divide(tmct0alc.getImtotbru(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcomisn(imcomisn);
        acumulado = acumulado.add(imcomisn);
      }
      else {
        LOG.trace("## ALCEData ## repartirComisionCliente ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImcomisn(tmct0alc.getImcomisn().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirComisionCliente ## desgloseCliTit.getImcomisn(): {}",
          desgloseCliTit.getImcomisn());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirComisionCliente ## Fin");

  }

  public void repartirComisionOrdenante() {
    LOG.trace("## ALCEData ## repartirComisionOrdenante ## Inicio");
    LOG.trace("## ALCEData ## repartirComisionOrdenante ## tmct0alc.getImcomdvo(): {} ", tmct0alc.getImcomdvo());
    LOG.trace("## ALCEData ## repartirComisionOrdenante ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcomdvo;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirComisionOrdenante ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imcomdvo = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcomdvo())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcomdvo(imcomdvo);
        acumulado = acumulado.add(imcomdvo);
      }
      else {
        LOG.trace("## ALCEData ## repartirComisionOrdenante ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImcomdvo(tmct0alc.getImcomdvo().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirComisionOrdenante ## desgloseCliTit.getImcomdvo(): "
          + desgloseCliTit.getImcomdvo());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirComisionOrdenante ## Fin");

  }

  public void repartirImcomsis() {
    LOG.trace("## ALCEData ## repartirImcomsis ## Inicio");
    LOG.trace("## ALCEData ## repartirImcomsis ## tmct0alc.getImcomsis(): {} ", tmct0alc.getImcomsis());
    LOG.trace("## ALCEData ## repartirImcomsis ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcomsis;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImcomsis ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imcomsis = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcomsis())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcomsis(imcomsis);
        acumulado = acumulado.add(imcomsis);
      }
      else {
        LOG.trace("## ALCEData ## repartirImcomsis ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImcomsis(tmct0alc.getImcomsis().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImcomsis ## desgloseCliTit.getImcomsis(): {} ", desgloseCliTit.getImcomsis());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImcomsis ## Fin");

  }

  public void repartirImbansis() {
    LOG.trace("## ALCEData ## repartirImbansis ## Inicio");
    LOG.trace("## ALCEData ## repartirImbansis ## tmct0alc.getImbansis(): {} ", tmct0alc.getImbansis());
    LOG.trace("## ALCEData ## repartirImbansis ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imbansis;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImbansis ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imbansis = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImbansis())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImbansis(imbansis);
        acumulado = acumulado.add(imbansis);
      }
      else {
        LOG.trace("## ALCEData ## repartirImbansis ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImbansis(tmct0alc.getImbansis().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImbansis ## desgloseCliTit.getImbansis(): {} ", desgloseCliTit.getImbansis());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImbansis ## Fin");

  }

  public void repartirComisionBanco() {
    LOG.trace("## ALCEData ## repartirComisionBanco ## Inicio");
    LOG.trace("## ALCEData ## repartirComisionBanco ## tmct0alc.getImcombco(): {} ", tmct0alc.getImcombco());
    LOG.trace("## ALCEData ## repartirComisionBanco ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcombco;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirComisionBanco ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imcombco = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcombco())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcombco(imcombco);
        acumulado = acumulado.add(imcombco);
      }
      else {
        LOG.trace("## ALCEData ## repartirComisionBanco ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImcombco(tmct0alc.getImcombco().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirComisionBanco ## desgloseCliTit.getImcombco(): {} ",
          desgloseCliTit.getImcombco());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirComisionBanco ## Fin");

  }

  public void repartirRebate() {
    LOG.trace("## ALCEData ## repartirRebate ## Inicio");
    LOG.trace("## ALCEData ## repartirRebate ## tmct0alc.getImcombrk(): {} ", tmct0alc.getImcombrk());
    LOG.trace("## ALCEData ## repartirRebate ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcombrk;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirRebate ## desgloseCliTit.getImefectivo(): {} ",
            desgloseCliTit.getImefectivo());

        imcombrk = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcombrk())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcombrk(imcombrk);
        acumulado = acumulado.add(imcombrk);
      }
      else {
        LOG.trace("## ALCEData ## repartirRebate ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImcombrk(tmct0alc.getImcombrk().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirRebate ## desgloseCliTit.getImcombrk(): {} ", desgloseCliTit.getImcombrk());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirRebate ## Fin");

  }

  protected void repartirNetoClienteFinal() {
    repartirNetoClienteFinal(getCltePayCanonComp(), getCltePayCanonContr(), getCltePayCanonLiq(), getTmct0alc(),
        getActiveDesgloseCliTitList());
  }

  private static void repartirNetoClienteFinal(boolean cltePayCanonComp, boolean cltPayCanonContr,
      boolean cltePayCanonLiq, Tmct0alc tmct0alc, List<Tmct0desgloseclitit> dcts) {
    LOG.trace("## ALCEData ## repartirNetoClienteFinal ## Inicio");
    LOG.trace("## ALCEData ## repartirNetoClienteFinal ## tmct0alc.getNutitliq(): {}", tmct0alc.getNutitliq());
    LOG.trace("## ALCEData ## repartirNetoClienteFinal ## tmct0alc.getImnfiliq(): {}", tmct0alc.getImnfiliq());
    LOG.trace("## ALCEData ## repartirNetoClienteFinal ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imnfiliq;
    BigDecimal acumulado = BigDecimal.ZERO;
    boolean clientePaga = cltePayCanonComp || cltPayCanonContr || cltePayCanonLiq;
    for (Tmct0desgloseclitit desgloseCliTit : dcts) {
      i++;
      if (i != dcts.size()) {
        if (clientePaga) {
          LOG.trace("## ALCEData ## repartirNetoClienteFinal ## desgloseCliTit.getImefectivo(): {}",
              desgloseCliTit.getImefectivo());

          imnfiliq = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImnfiliq())
              .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
          desgloseCliTit.setImnfiliq(imnfiliq);
          desgloseCliTit.setImnetliq(imnfiliq.setScale(2, BigDecimal.ROUND_HALF_UP));
          acumulado = acumulado.add(imnfiliq);
        }
        else {
          LOG.trace("## ALCEData ## repartirNetoClienteFinal ## desgloseCliTit.getImtitulos(): {}",
              desgloseCliTit.getImtitulos());

          imnfiliq = desgloseCliTit.getImtitulos().multiply(tmct0alc.getImnfiliq())
              .divide(tmct0alc.getNutitliq(), 2, BigDecimal.ROUND_HALF_UP);
          desgloseCliTit.setImnfiliq(imnfiliq);
          desgloseCliTit.setImnetliq(imnfiliq.setScale(2, BigDecimal.ROUND_HALF_UP));
          acumulado = acumulado.add(imnfiliq);
        }

      }
      else {
        LOG.trace("## ALCEData ## repartirNetoClienteFinal ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImnfiliq(tmct0alc.getImnfiliq().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
        desgloseCliTit.setImnetliq(tmct0alc.getImnfiliq().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirNetoClienteFinal ## desgloseCliTit.getImnfiliq(): {}",
          desgloseCliTit.getImnfiliq());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirNetoClienteFinal ## Fin");

  }

  public void repartirProfitCommission() {
    repartirProfitCommission(getTmct0alc(), getActiveDesgloseCliTitList());
  }

  protected static void repartirProfitCommission(Tmct0alc tmct0alc, List<Tmct0desgloseclitit> dcts) {
    LOG.trace("## ALCEData ## repartirProfitCommission ## Inicio");
    LOG.trace("## ALCEData ## repartirProfitCommission ## tmct0alc.getImcomsvb(): {} ", tmct0alc.getImcomsvb());
    LOG.trace("## ALCEData ## repartirProfitCommission ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcomsvb;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : dcts) {
      i++;
      if (i != dcts.size()) {
        LOG.trace("## ALCEData ## repartirProfitCommission ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imcomsvb = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcomsvb())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcomsvb(imcomsvb);
        acumulado = acumulado.add(imcomsvb);
      }
      else {
        LOG.trace("## ALCEData ## repartirProfitCommission ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImcomsvb(tmct0alc.getImcomsvb().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirProfitCommission ## desgloseCliTit.getImcomsvb(): "
          + desgloseCliTit.getImcomsvb());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirProfitCommission ## Fin");

  }

  public void repartirPriceAjustment() {
    LOG.trace("## ALCEData ## repartirPriceAjustment ## Inicio");
    LOG.trace("## ALCEData ## repartirPriceAjustment ## tmct0alc.getImntbrok(): {} ", tmct0alc.getImntbrok());
    LOG.trace("## ALCEData ## repartirPriceAjustment ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imntbrok;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirPriceAjustment ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imntbrok = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImntbrok())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImntbrok(imntbrok);
        acumulado = acumulado.add(imntbrok);
      }
      else {
        LOG.trace("## ALCEData ## repartirPriceAjustment ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImntbrok(tmct0alc.getImntbrok().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirPriceAjustment ## desgloseCliTit.getImntbrok(): "
          + desgloseCliTit.getImntbrok());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirPriceAjustment ## Fin");

  }

  public void repartirAjustmentCommission() {
    LOG.trace("## ALCEData ## repartirAjustmentCommission ## Inicio");
    LOG.trace("## ALCEData ## repartirAjustmentCommission ## tmct0alc.getImajusvb(): {} ", tmct0alc.getImajusvb());
    LOG.trace("## ALCEData ## repartirAjustmentCommission ## tmct0alc.getImefeagr(): {} ", tmct0alc.getImefeagr());
    if (tmct0alc.getImajusvb() == null) {
      tmct0alc.setImajusvb(BigDecimal.ZERO);
    }
    int i = 0;
    BigDecimal imajussbv;
    BigDecimal acumulado = BigDecimal.ZERO;
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirAjustmentCommission ## desgloseCliTit.getImefectivo(): "
            + desgloseCliTit.getImefectivo());

        imajussbv = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImajusvb())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImajusvb(imajussbv);
        acumulado = acumulado.add(imajussbv);
      }
      else {
        LOG.trace("## ALCEData ## repartirAjustmentCommission ## acumulado_ultimo: {} ", acumulado);
        desgloseCliTit.setImajusvb(tmct0alc.getImajusvb().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirAjustmentCommission ## desgloseCliTit.getImajusvb(): "
          + desgloseCliTit.getImajusvb());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirAjustmentCommission ## Fin");

  }

  protected void repartirImcobrado() {
    LOG.trace("## ALCEData ## repartirImcobrado ## Inicio");
    LOG.trace("## ALCEData ## repartirImcobrado ## tmct0alc.getImcobrado(): {}", tmct0alc.getImcobrado());
    LOG.trace("## ALCEData ## repartirImcobrado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImcobrado() == null) {
      tmct0alc.setImcobrado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImcobrado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imcobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcobrado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcobrado(imcobrado);
        acumulado = acumulado.add(imcobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImcobrado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImcobrado(tmct0alc.getImcobrado().subtract(acumulado).setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImcobrado ## desgloseCliTit.getImcobrado(): {}", desgloseCliTit.getImcobrado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImcobrado ## Fin");

  }

  // protected void repartirImconciliado() {
  // LOG.trace("## ALCEData ## repartirImconciliado ## Inicio");
  // LOG.trace("## ALCEData ## repartirImconciliado ## tmct0alc.getImcobrado(): {}",
  // tmct0alc.getImconciliado());
  // LOG.trace("## ALCEData ## repartirImconciliado ## tmct0alc.getImefeagr(): {}",
  // tmct0alc.getImefeagr());
  //
  // int i = 0;
  // BigDecimal imconciliado;
  // BigDecimal acumulado = BigDecimal.ZERO;
  // if (tmct0alc.getImconciliado() == null) {
  // tmct0alc.setImconciliado(BigDecimal.ZERO);
  // }
  // for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
  // i++;
  // if (i != getActiveDesgloseCliTitList().size()) {
  // LOG.trace("## ALCEData ## repartirImconciliado ## desgloseCliTit.getImefectivo(): {}",
  // desgloseCliTit.getImefectivo());
  //
  // imconciliado =
  // desgloseCliTit.getImefectivo().multiply(tmct0alc.getImconciliado())
  // .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
  // desgloseCliTit.setImconciliado(imconciliado);
  // acumulado = acumulado.add(imconciliado);
  // }
  // else {
  // LOG.trace("## ALCEData ## repartirImconciliado ## acumulado_ultimo: {}",
  // acumulado);
  // desgloseCliTit.setImconciliado(tmct0alc.getImconciliado().subtract(acumulado)
  // .setScale(2, BigDecimal.ROUND_HALF_UP));
  // } // else
  // LOG.trace("## ALCEData ## repartirImconciliado ## desgloseCliTit.getImconciliado(): {}",
  // desgloseCliTit.getImconciliado());
  // } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())
  //
  // LOG.trace("## ALCEData ## repartirImconciliado ## Fin");
  //
  // }

  protected void repartirImefeMercadoCobrado() {
    LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## Inicio");
    LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## tmct0alc.getImefeMercadoCobrado(): {}",
        tmct0alc.getImefeMercadoCobrado());
    LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imefeMercadoCobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImefeMercadoCobrado() == null) {
      tmct0alc.setImefeMercadoCobrado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imefeMercadoCobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImefeMercadoCobrado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImefeMercadoCobrado(imefeMercadoCobrado);
        acumulado = acumulado.add(imefeMercadoCobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImefeMercadoCobrado(tmct0alc.getImefeMercadoCobrado().subtract(acumulado)
            .setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## desgloseCliTit.getImefeMercadoCobrado(): {}",
          desgloseCliTit.getImefeMercadoCobrado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImefemercadoCobrado ## Fin");

  }

  protected void repartirImefeClienteCobrado() {
    LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## Inicio");
    LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## tmct0alc.getImefeClienteCobrado(): {}",
        tmct0alc.getImefeClienteCobrado());
    LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imefeClienteCobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImefeClienteCobrado() == null) {
      tmct0alc.setImefeClienteCobrado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imefeClienteCobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImefeClienteCobrado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImefeClienteCobrado(imefeClienteCobrado);
        acumulado = acumulado.add(imefeClienteCobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImefeClienteCobrado(tmct0alc.getImefeClienteCobrado().subtract(acumulado)
            .setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## desgloseCliTit.getImefeClienteCobrado(): {}",
          desgloseCliTit.getImefeClienteCobrado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImefeClienteCobrado ## Fin");

  }

  protected void repartirImcanonCobrado() {
    LOG.trace("## ALCEData ## repartirImcanonCobrado ## Inicio");
    LOG.trace("## ALCEData ## repartirImcanonCobrado ## tmct0alc.getImcanonCobrado(): {}", tmct0alc.getImcanonCobrado());
    LOG.trace("## ALCEData ## repartirImcanonCobrado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcanonCobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImcanonCobrado() == null) {
      tmct0alc.setImcanonCobrado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImcanonCobrado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imcanonCobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcanonCobrado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcanonCobrado(imcanonCobrado);
        acumulado = acumulado.add(imcanonCobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImcanonCobrado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImcanonCobrado(tmct0alc.getImcanonCobrado().subtract(acumulado)
            .setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImcanonCobrado ## desgloseCliTit.getImcanonCobrado(): {}",
          desgloseCliTit.getImcanonCobrado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImcanonCobrado ## Fin");

  }

  protected void repartirImcomCobrado() {
    LOG.trace("## ALCEData ## repartirImcomCobrado ## Inicio");
    LOG.trace("## ALCEData ## repartirImcomCobrado ## tmct0alc.getImcomCobrado(): {}", tmct0alc.getImcomCobrado());
    LOG.trace("## ALCEData ## repartirImcomCobrado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcanonCobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImcomCobrado() == null) {
      tmct0alc.setImcomCobrado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImcomCobrado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imcanonCobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImcomCobrado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImcomCobrado(imcanonCobrado);
        acumulado = acumulado.add(imcanonCobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImcomCobrado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImcomCobrado(tmct0alc.getImcomCobrado().subtract(acumulado)
            .setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImcomCobrado ## desgloseCliTit.getImcomCobrado(): {}",
          desgloseCliTit.getImcomCobrado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImcomCobrado ## Fin");

  }

  protected void repartirImbrkPagado() {
    LOG.trace("## ALCEData ## repartirImbrkPagado ## Inicio");
    LOG.trace("## ALCEData ## repartirImbrkPagado ## tmct0alc.getImbrkPagado(): {}", tmct0alc.getImbrkPagado());
    LOG.trace("## ALCEData ## repartirImbrkPagado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcanonCobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImbrkPagado() == null) {
      tmct0alc.setImbrkPagado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImbrkPagado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imcanonCobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImbrkPagado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImbrkPagado(imcanonCobrado);
        acumulado = acumulado.add(imcanonCobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImbrkPagado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImbrkPagado(tmct0alc.getImbrkPagado().subtract(acumulado)
            .setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImbrkPagado ## desgloseCliTit.getImbrkPagado(): {}",
          desgloseCliTit.getImbrkPagado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImbrkPagado ## Fin");

  }

  protected void repartirImdvoPagado() {
    LOG.trace("## ALCEData ## repartirImdvoPagado ## Inicio");
    LOG.trace("## ALCEData ## repartirImdvoPagado ## tmct0alc.getImdvoPagado(): {}", tmct0alc.getImdvoPagado());
    LOG.trace("## ALCEData ## repartirImdvoPagado ## tmct0alc.getImefeagr(): {}", tmct0alc.getImefeagr());

    int i = 0;
    BigDecimal imcanonCobrado;
    BigDecimal acumulado = BigDecimal.ZERO;
    if (tmct0alc.getImdvoPagado() == null) {
      tmct0alc.setImdvoPagado(BigDecimal.ZERO);
    }
    for (Tmct0desgloseclitit desgloseCliTit : getActiveDesgloseCliTitList()) {
      i++;
      if (i != getActiveDesgloseCliTitList().size()) {
        LOG.trace("## ALCEData ## repartirImdvoPagado ## desgloseCliTit.getImefectivo(): {}",
            desgloseCliTit.getImefectivo());

        imcanonCobrado = desgloseCliTit.getImefectivo().multiply(tmct0alc.getImdvoPagado())
            .divide(tmct0alc.getImefeagr(), 2, BigDecimal.ROUND_HALF_UP);
        desgloseCliTit.setImdvoPagado(imcanonCobrado);
        acumulado = acumulado.add(imcanonCobrado);
      }
      else {
        LOG.trace("## ALCEData ## repartirImdvoPagado ## acumulado_ultimo: {}", acumulado);
        desgloseCliTit.setImdvoPagado(tmct0alc.getImdvoPagado().subtract(acumulado)
            .setScale(2, BigDecimal.ROUND_HALF_UP));
      } // else
      LOG.trace("## ALCEData ## repartirImdvoPagado ## desgloseCliTit.getImdvoPagado(): {}",
          desgloseCliTit.getImdvoPagado());
    } // for (DesgloseCliTit desgloseCliTit : getActiveDesgloseCliTitList())

    LOG.trace("## ALCEData ## repartirImdvoPagado ## Fin");

  }

} // AllocateNacionalTmct0alcEconomicData

