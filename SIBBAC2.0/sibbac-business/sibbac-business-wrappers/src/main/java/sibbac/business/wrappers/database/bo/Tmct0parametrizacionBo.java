package sibbac.business.wrappers.database.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.wrappers.database.dao.Tmct0ReglaDaoImpl;
import sibbac.business.wrappers.database.dao.Tmct0parametrizacionDao;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0parametrizacion;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0parametrizacionBo extends AbstractBo<Tmct0parametrizacion, Long, Tmct0parametrizacionDao> {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0parametrizacionBo.class);
  @Autowired
  private Tmct0ReglaDaoImpl daoImpl;

  @Autowired
  private Tmct0parametrizacionDao dao;

  @Autowired
  private Tmct0actBo actBo;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private Tmct0ReglaBo reglaBo;

  public List<Tmct0parametrizacion> findDTOForService(Long idRegla, Long camaraCompensacion, Long cuentaCompensacion,
      Long miembroCompensador, String referenciaAsignacion, Long nemotecnico, Long segmento, String referenciaCliente,
      String referenciaExterna, Long capacidad, Long modeloNegocio) {

    return daoImpl.findAllDTO1(idRegla, camaraCompensacion, cuentaCompensacion, miembroCompensador,
        referenciaAsignacion, nemotecnico, segmento, referenciaCliente, referenciaExterna, capacidad, modeloNegocio);
  }

  public List<Tmct0parametrizacionDTO> findParametrizacionesByIdRegla(BigInteger idRegla) {
    List<Tmct0parametrizacionDTO> listaParametrizacionDTO = new ArrayList<Tmct0parametrizacionDTO>();
    List<Tmct0parametrizacion> listaParametrizacion = this.dao.findParametrizacionesByIdRegla(idRegla);
    for (Tmct0parametrizacion param : listaParametrizacion) {
      Tmct0parametrizacionDTO paramDTO = new Tmct0parametrizacionDTO(param);
      listaParametrizacionDTO.add(paramDTO);
    }
    return listaParametrizacionDTO;
  }

  public Tmct0parametrizacionDTO findParametrizacionByCdaliass(String cdaliass, String camaraCompensacion,
      String segmento, String modeloNegocio, String referenciaCliente, String referenciaExterna) {

    Tmct0parametrizacionDTO result = null;
    Tmct0Regla regla;
    Long idRegla = null;
    String cuenta = cdaliass;
    String subcuenta = null;

    if (cdaliass.contains("|")) {
      cuenta = cdaliass.split("\\|")[1];
      subcuenta = cdaliass.split("\\|")[0];
      List<Tmct0act> acts = actBo.getSubCtaBro(cuenta, subcuenta, Tmct0ali.fhfinActivos);

      if (CollectionUtils.isNotEmpty(acts)) {
        for (Tmct0act tmct0act : acts) {
          idRegla = tmct0act.getIdRegla();
          if (idRegla != null) {
            regla = reglaBo.findIdReglaActivo(new BigInteger(idRegla.toString()));
            if (regla != null) {
              result = findParametrizacionByIdRegla(new BigInteger(idRegla.toString()), camaraCompensacion, segmento,
                  modeloNegocio, referenciaCliente, referenciaExterna);
            }
            if (result != null) {
              break;
            }
          }
        }
      }
    }

    if (result == null) {
      Tmct0ali ali = aliBo.findByCdaliass(cuenta, Tmct0ali.fhfinActivos);
      if (ali != null) {
        idRegla = ali.getIdRegla();
      }

    }
    if (idRegla != null && result == null) {
      if (idRegla != null) {
        regla = reglaBo.findIdReglaActivo(new BigInteger(idRegla.toString()));
        if (regla != null) {
          result = findParametrizacionByIdRegla(new BigInteger(idRegla.toString()), camaraCompensacion, segmento,
              modeloNegocio, referenciaCliente, referenciaExterna);
        }

      }
    }
    return result;

  }

  /**
   * Devuelve la parametrizacion con mas coincidencias
   * 
   * @param idRegla
   * @param camaraCompensacion
   * @param segmento
   * @param modeloNegocio
   * @param referenciaCliente
   * @param referenciaExterna
   * @return
   */
  public Tmct0parametrizacionDTO findParametrizacionByIdRegla(BigInteger idRegla, String camaraCompensacion,
      String segmento, String modeloNegocio, String referenciaCliente, String referenciaExterna) {
    Tmct0parametrizacionDTO result = null;
    Tmct0parametrizacionDTO tmct0parametrizacionDTO;
    List<Tmct0parametrizacionDTO> listaParametrizacionDTO = findParametrizacionesByIdRegla(idRegla);
    if (CollectionUtils.isNotEmpty(listaParametrizacionDTO)) {
      int[] arrayCoincidencias = new int[listaParametrizacionDTO.size()];

      int coincidencias;
      for (int i = 0; i < listaParametrizacionDTO.size(); i++) {
        tmct0parametrizacionDTO = listaParametrizacionDTO.get(i);
        coincidencias = -1;

        if (tmct0parametrizacionDTO.getCd_codigoCamaraComp().equals(StringUtils.trim(camaraCompensacion))) {
          coincidencias++;
          if (StringUtils.isNotBlank(modeloNegocio)) {
            if (tmct0parametrizacionDTO.getCd_codigoModeloNegocio() != null
                && tmct0parametrizacionDTO.getCd_codigoModeloNegocio().equals(StringUtils.trim(modeloNegocio))) {
              coincidencias++;
            }
          }

          if (StringUtils.isNotBlank(segmento)) {
            if (tmct0parametrizacionDTO.getCd_codigoSegmento() != null
                && tmct0parametrizacionDTO.getCd_codigoSegmento().equals(StringUtils.trim(segmento))) {
              coincidencias++;
            }
          }

          if (StringUtils.isNotBlank(referenciaCliente)) {
            if (tmct0parametrizacionDTO.getReferenciaCliente() != null
                && tmct0parametrizacionDTO.getReferenciaCliente().equals(StringUtils.trim(referenciaCliente))) {
              coincidencias++;
            }
          }

          if (StringUtils.isNotBlank(referenciaExterna)) {
            if (tmct0parametrizacionDTO.getReferenciaExterna() != null
                && tmct0parametrizacionDTO.getReferenciaExterna().equals(StringUtils.trim(referenciaExterna))) {
              coincidencias++;
            }
          }
        }
        arrayCoincidencias[i] = coincidencias;
      }
      int maxCoincidencias = -1;
      for (int i = 0; i < arrayCoincidencias.length; i++) {
        if (arrayCoincidencias[i] > maxCoincidencias) {
          maxCoincidencias = arrayCoincidencias[i];
          result = listaParametrizacionDTO.get(i);
        }
      }
    }
    return result;
  }

  //
  // public Tmct0parametrizacionDTO findParametrizacionByIdParametrizacion(Long
  // idParametrizacion){
  // // Tmct0parametrizacionDTO parametrizacionDTO = new
  // Tmct0parametrizacionDTO();
  // Tmct0parametrizacion resulParametrizacion =
  // this.dao.findParametrizacionesByIdParametrizacion(idParametrizacion);
  // Tmct0parametrizacionDTO paramDTO = null;
  // if(resulParametrizacion !=null){
  // paramDTO = new Tmct0parametrizacionDTO(resulParametrizacion);
  //
  // }
  // return paramDTO;
  //
  // }

  public Tmct0parametrizacionDTO findParametrizacionByIdParametrizacion(Long idParametrizacion) {
    // Tmct0parametrizacionDTO parametrizacionDTO = new
    // Tmct0parametrizacionDTO();
    Tmct0parametrizacion resulParametrizacion = this.dao.findParametrizacionesByIdParametrizacion(idParametrizacion);
    Tmct0parametrizacionDTO paramDTO = null;
    if (resulParametrizacion != null) {
      paramDTO = new Tmct0parametrizacionDTO(resulParametrizacion);

    }
    return paramDTO;

  }

  public Tmct0parametrizacion findParametrizacionesByIdParame(Long idParametrizacion) {
    // Tmct0parametrizacionDTO parametrizacionDTO = new
    // Tmct0parametrizacionDTO();
    Tmct0parametrizacion resulParametrizacion = this.dao.findParametrizacionesByIdParametrizacion(idParametrizacion);

    return resulParametrizacion;

  }

  @Transactional
  public List<Tmct0parametrizacionDTO> findParametrizacionesByManyReglas(List<BigInteger> idReglas) {
    List<Tmct0parametrizacionDTO> listaParametrizacionDTO = new ArrayList<Tmct0parametrizacionDTO>();
    List<Tmct0parametrizacion> listaParametrizacion = this.dao.findParametrizacionesByManyReglas(idReglas);
    for (Tmct0parametrizacion param : listaParametrizacion) {
      Tmct0parametrizacionDTO paramDTO = new Tmct0parametrizacionDTO(param);
      listaParametrizacionDTO.add(paramDTO);
    }
    return listaParametrizacionDTO;
  }

  @Transactional
  public Map<String, String> modificacionContacto(Map<String, String> params) {

    LOG.trace(" Se inicia la modificacion de un contacto ");
    Map<String, String> resultado = new HashMap<String, String>();
    return resultado;

  }

  public Tmct0parametrizacionDTO getParametrizacion(Tmct0alo alo, String camaraCompensacion) {
    String cdaliass = alo.getCdalias();
    String segmento = "";
    String modeloNegocio = alo.getTmct0bok().getTmct0ord().getCdmodnego();
    String referenciaCliente = "";
    String referenciaExterna = "";
    return findParametrizacionByCdaliass(cdaliass, camaraCompensacion, segmento, modeloNegocio, referenciaCliente,
        referenciaExterna);
  }

  public Tmct0parametrizacionDTO getParametrizacion(Map<String, Tmct0parametrizacionDTO> cached, Tmct0alo alo, String camaraCompensacion) {
    camaraCompensacion = StringUtils.trim(camaraCompensacion);
    String key = getKey(alo, camaraCompensacion);
    Tmct0parametrizacionDTO p = cached.get(key);
    if (p == null) {
      synchronized (cached) {
        p = cached.get(key);
        if (p == null) {
          p = getParametrizacion(alo, camaraCompensacion);
          cached.put(key, p);
        }
      }
    }
    return p;
  }

  public Tmct0parametrizacionDTO getParametrizacion(Tmct0alo alo, BigInteger idRegla, String camaraCompensacion) {
    String segmento = "";
    String modeloNegocio = alo.getTmct0bok().getTmct0ord().getCdmodnego();
    String referenciaCliente = "";
    String referenciaExterna = "";
    return findParametrizacionByIdRegla(idRegla, camaraCompensacion, segmento, modeloNegocio, referenciaCliente,
        referenciaExterna);
  }

  public Tmct0parametrizacionDTO getParametrizacion(Map<String, Tmct0parametrizacionDTO> cached, Tmct0alo alo, BigInteger idRegla, String camaraCompensacion) {
    camaraCompensacion = StringUtils.trim(camaraCompensacion);
    String key = getKey(alo, idRegla, camaraCompensacion);
    Tmct0parametrizacionDTO p = cached.get(key);
    if (p == null) {
      synchronized (cached) {
        p = cached.get(key);
        if (p == null) {
          p = getParametrizacion(alo, idRegla, camaraCompensacion);
          cached.put(key, p);
        }
      }
    }
    if (p != null) {
      return new Tmct0parametrizacionDTO(p);
    }
    else {
      return p;
    }
  }

  private String getKey(Tmct0alo alo, String camaraCompensacion) {
    String cdaliass = alo.getCdalias();
    String segmento = "";
    String modeloNegocio = alo.getTmct0bok().getTmct0ord().getCdmodnego();
    String referenciaCliente = "";
    String referenciaExterna = "";
    return String.format("%s_%s_%s_%s_%s_%s", cdaliass, segmento, modeloNegocio, referenciaCliente, referenciaExterna,camaraCompensacion);
  }

  private String getKey(Tmct0alo alo, BigInteger idRegla, String camaraCompensacion) {
    String cdaliass = idRegla.toString();
    String segmento = "";
    String modeloNegocio = alo.getTmct0bok().getTmct0ord().getCdmodnego();
    String referenciaCliente = "";
    String referenciaExterna = "";
    return String.format("%s_%s_%s_%s_%s_%s", cdaliass, segmento, modeloNegocio, referenciaCliente, referenciaExterna,camaraCompensacion);
  }
}
