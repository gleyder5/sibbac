package sibbac.business.wrappers.database.dto;

import java.math.BigInteger;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import sibbac.business.wrappers.database.model.Tmct0parametrizacion;

public class Tmct0parametrizacionDTO {

  private Long idParametrizacion;
  // private Tmct0Regla tmct0Regla;
  private String descripcionParametri;
  private BigInteger idRegla;
  private Long idCamaraComp;
  private String cd_codigoCamaraComp;// Tmct0CamaraCompensacion
  private String nb_descripcionCamaraComp;// Tmct0CamaraCompensacion
  // private Tmct0Segmento tmct0Segmento; //Tmct0CamaraCompensacion
  private int idSegmento;
  private String cd_codigoSegmento;// Tmct0Segmento
  private String nb_descripcionSegmento;// Tmct0Segmento
  // private Tmct0ModeloDeNegocio tmct0ModeloDeNegocio;
  private Long idModeloNegocio;
  private String cd_codigoModeloNegocio;// Tmct0ModeloDeNegocio
  private String nb_descripcionModeloNegocio;// Tmct0ModeloDeNegocio
  // private Tmct0IndicadorDeCapacidad tmct0IndicadorCapacidad;
  private Long idCapacidad;
  private String cd_codigoIndicCapacidad;// Tmct0IndicadorDeCapacidad
  private String nb_descripcionIndicCapacidad;// Tmct0IndicadorDeCapacidad
  private String referenciaCliente;
  private String referenciaExterna;
  private String referenciaAsignacion;
  private String activo;
  private Date fecha_inicio;
  private Date fecha_fin;
  private Long idCuentaCompensacion;
  private String cd_codigoCuentaCompensacion; // Tmct0CuentaCompensacion;
  private String cdCodigoCuentaCompensacion; // Tmct0CuentaCompensacion;
  private Long idNemotecnico;
  private String nb_nombreNmotecnico; // Tmct0Nemotecnicos
  private Long idCompensador;
  private String nb_descripcionCompensador;// Tmct0Compensador
  private String nbNombreCompensador;// Tmct0Compensador
  private String cdCodigoRegla;
  private String descripcionRegla;

  private boolean isManual;
  private boolean isExcel;

  public Tmct0parametrizacionDTO() {
  }

  public Tmct0parametrizacionDTO(Tmct0parametrizacionDTO tmct0parametrizacion) {
    idParametrizacion = tmct0parametrizacion.getIdParametrizacion();
    descripcionParametri = tmct0parametrizacion.getDescripcionParametri();
    idRegla = tmct0parametrizacion.getIdRegla();
    cdCodigoRegla = tmct0parametrizacion.getCdCodigoRegla();
    descripcionRegla = tmct0parametrizacion.getDescripcionRegla();

    cd_codigoCamaraComp = tmct0parametrizacion.getCd_codigoCamaraComp();
    nb_descripcionCamaraComp = tmct0parametrizacion.getNb_descripcionCamaraComp();
    idCamaraComp = tmct0parametrizacion.getIdCamaraComp();

    cd_codigoSegmento = tmct0parametrizacion.getCd_codigoSegmento();
    nb_descripcionSegmento = tmct0parametrizacion.getNb_descripcionSegmento();
    idSegmento = tmct0parametrizacion.getIdSegmento();

    cd_codigoModeloNegocio = tmct0parametrizacion.getCd_codigoModeloNegocio();
    nb_descripcionModeloNegocio = tmct0parametrizacion.getNb_descripcionModeloNegocio();
    idModeloNegocio = tmct0parametrizacion.getIdNemotecnico();

    cd_codigoIndicCapacidad = tmct0parametrizacion.getCd_codigoIndicCapacidad();
    nb_descripcionIndicCapacidad = tmct0parametrizacion.getNb_descripcionIndicCapacidad();
    idCapacidad = tmct0parametrizacion.getIdCapacidad();
    referenciaCliente = tmct0parametrizacion.getReferenciaCliente();
    referenciaExterna = tmct0parametrizacion.getReferenciaExterna();
    referenciaAsignacion = tmct0parametrizacion.getReferenciaExterna();
    activo = tmct0parametrizacion.getActivo();
    fecha_inicio = tmct0parametrizacion.getFecha_inicio();
    fecha_fin = tmct0parametrizacion.getFecha_fin();

    cd_codigoCuentaCompensacion = tmct0parametrizacion.getCd_codigoCuentaCompensacion();
    cdCodigoCuentaCompensacion = tmct0parametrizacion.getCdCodigoCuentaCompensacion();
    idCuentaCompensacion = tmct0parametrizacion.getIdCuentaCompensacion();

    nb_nombreNmotecnico = tmct0parametrizacion.getNb_nombreNmotecnico();
    idNemotecnico = tmct0parametrizacion.getIdNemotecnico();

    nb_descripcionCompensador = tmct0parametrizacion.getNb_descripcionCompensador();
    idCompensador = tmct0parametrizacion.getIdCompensador();
    nbNombreCompensador = tmct0parametrizacion.getNbNombreCompensador();
  }

  public Tmct0parametrizacionDTO(Tmct0parametrizacion tmct0parametrizacion) {

    idParametrizacion = tmct0parametrizacion.getIdParametrizacion();
    descripcionParametri = tmct0parametrizacion.getDescripcion();
    idRegla = tmct0parametrizacion.getTmct0Regla().getIdRegla();
    cdCodigoRegla = tmct0parametrizacion.getTmct0Regla().getCdCodigo();
    descripcionRegla = tmct0parametrizacion.getTmct0Regla().getNbDescripcion();

    if (tmct0parametrizacion.getTmct0CamaraCompensacion() != null) {
      cd_codigoCamaraComp = tmct0parametrizacion.getTmct0CamaraCompensacion().getCdCodigo();
      nb_descripcionCamaraComp = tmct0parametrizacion.getTmct0CamaraCompensacion().getNbDescripcion();
      idCamaraComp = tmct0parametrizacion.getTmct0CamaraCompensacion().getIdCamaraCompensacion();

    }
    if (tmct0parametrizacion.getTmct0Segmento() != null) {
      cd_codigoSegmento = tmct0parametrizacion.getTmct0Segmento().getCd_codigo();
      nb_descripcionSegmento = tmct0parametrizacion.getTmct0Segmento().getNbDescripcion();
      idSegmento = tmct0parametrizacion.getTmct0Segmento().getId_segmento();
    }
    if (tmct0parametrizacion.getTmct0ModeloDeNegocio() != null) {

      cd_codigoModeloNegocio = tmct0parametrizacion.getTmct0ModeloDeNegocio().getCd_codigo();
      nb_descripcionModeloNegocio = tmct0parametrizacion.getTmct0ModeloDeNegocio().getNbDescripcion();
      idModeloNegocio = tmct0parametrizacion.getTmct0ModeloDeNegocio().getId_modelo_de_negocio();
    }
    if (tmct0parametrizacion.getTmct0IndicadorCapacidad() != null) {

      cd_codigoIndicCapacidad = tmct0parametrizacion.getTmct0IndicadorCapacidad().getCd_codigo();
      nb_descripcionIndicCapacidad = tmct0parametrizacion.getTmct0IndicadorCapacidad().getDescripcion();
      idCapacidad = tmct0parametrizacion.getTmct0IndicadorCapacidad().getId_indicador_capacidad();
    }
    referenciaCliente = tmct0parametrizacion.getReferenciaCliente();
    referenciaExterna = tmct0parametrizacion.getReferenciaExterna();
    referenciaAsignacion = tmct0parametrizacion.getCdReferenciaAsignacion();
    activo = tmct0parametrizacion.getActivo();
    fecha_inicio = tmct0parametrizacion.getFecha_inicio();
    fecha_fin = tmct0parametrizacion.getFecha_fin();
    if (tmct0parametrizacion.getTmct0CuentasDeCompensacion() != null) {

      cd_codigoCuentaCompensacion = tmct0parametrizacion.getTmct0CuentasDeCompensacion().getCdCodigo();
      cdCodigoCuentaCompensacion = tmct0parametrizacion.getTmct0CuentasDeCompensacion().getCdCodigo();
      idCuentaCompensacion = tmct0parametrizacion.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion();
    }
    if (tmct0parametrizacion.getTmct0Nemotecnicos() != null) {

      nb_nombreNmotecnico = tmct0parametrizacion.getTmct0Nemotecnicos().getNbnombre();
      idNemotecnico = tmct0parametrizacion.getTmct0Nemotecnicos().getId_nemotecnico();
    }
    if (tmct0parametrizacion.getTmct0Compensador() != null) {

      nb_descripcionCompensador = tmct0parametrizacion.getTmct0Compensador().getNbDescripcion();
      idCompensador = tmct0parametrizacion.getTmct0Compensador().getIdCompensador();
      nbNombreCompensador = tmct0parametrizacion.getTmct0Compensador().getNbNombre();
    }

  }

  public Long getIdParametrizacion() {
    return idParametrizacion;
  }

  public void setIdParametrizacion(Long idParametrizacion) {
    this.idParametrizacion = idParametrizacion;
  }

  public BigInteger getIdRegla() {
    return idRegla;
  }

  public void setIdRegla(BigInteger idRegla) {
    this.idRegla = idRegla;
  }

  public String getCd_codigoCamaraComp() {
    return cd_codigoCamaraComp;
  }

  public void setCd_codigoCamaraComp(String cd_codigoCamaraComp) {
    this.cd_codigoCamaraComp = cd_codigoCamaraComp;
  }

  public String getNb_descripcionCamaraComp() {
    return nb_descripcionCamaraComp;
  }

  public void setNb_descripcionCamaraComp(String nb_descripcionCamaraComp) {
    this.nb_descripcionCamaraComp = nb_descripcionCamaraComp;
  }

  public String getCd_codigoSegmento() {
    return cd_codigoSegmento;
  }

  public void setCd_codigoSegmento(String cd_codigoSegmento) {
    this.cd_codigoSegmento = cd_codigoSegmento;
  }

  public String getNb_descripcionSegmento() {
    return nb_descripcionSegmento;
  }

  public void setNb_descripcionSegmento(String nb_descripcionSegmento) {
    this.nb_descripcionSegmento = nb_descripcionSegmento;
  }

  public String getCd_codigoModeloNegocio() {
    return cd_codigoModeloNegocio;
  }

  public void setCd_codigoModeloNegocio(String cd_codigoModeloNegocio) {
    this.cd_codigoModeloNegocio = cd_codigoModeloNegocio;
  }

  public String getNb_descripcionModeloNegocio() {
    return nb_descripcionModeloNegocio;
  }

  public void setNb_descripcionModeloNegocio(String nb_descripcionModeloNegocio) {
    this.nb_descripcionModeloNegocio = nb_descripcionModeloNegocio;
  }

  public String getCd_codigoIndicCapacidad() {
    return cd_codigoIndicCapacidad;
  }

  public void setCd_codigoIndicCapacidad(String cd_codigoIndicCapacidad) {
    this.cd_codigoIndicCapacidad = cd_codigoIndicCapacidad;
  }

  public String getNb_descripcionIndicCapacidad() {
    return nb_descripcionIndicCapacidad;
  }

  public void setNb_descripcionIndicCapacidad(String nb_descripcionIndicCapacidad) {
    this.nb_descripcionIndicCapacidad = nb_descripcionIndicCapacidad;
  }

  public String getReferenciaCliente() {
    return referenciaCliente;
  }

  public void setReferenciaCliente(String referenciaCliente) {
    this.referenciaCliente = referenciaCliente;
  }

  public String getReferenciaExterna() {
    return referenciaExterna;
  }

  public void setReferenciaExterna(String referenciaExterna) {
    this.referenciaExterna = referenciaExterna;
  }

  public String getReferenciaAsignacion() {
    return referenciaAsignacion;
  }

  public void setReferenciaAsignacion(String referenciaAsignacion) {
    this.referenciaAsignacion = referenciaAsignacion;
  }

  public String getActivo() {
    return activo;
  }

  public void setActivo(String activo) {
    this.activo = activo;
  }

  public Date getFecha_inicio() {
    return fecha_inicio;
  }

  public void setFecha_inicio(Date fecha_inicio) {
    this.fecha_inicio = fecha_inicio;
  }

  public Date getFecha_fin() {
    return fecha_fin;
  }

  public void setFecha_fin(Date fecha_fin) {
    this.fecha_fin = fecha_fin;
  }

  public String getCd_codigoCuentaCompensacion() {
    return cd_codigoCuentaCompensacion;
  }

  public void setCd_codigoCuentaCompensacion(String cd_codigo) {
    this.cd_codigoCuentaCompensacion = cd_codigo;
  }

  public String getNb_nombreNmotecnico() {
    return nb_nombreNmotecnico;
  }

  public void setNb_nombreNmotecnico(String nb_nombre) {
    this.nb_nombreNmotecnico = nb_nombre;
  }

  public String getNb_descripcionCompensador() {
    return nb_descripcionCompensador;
  }

  public void setNb_descripcionCompensador(String nb_descripcion) {
    this.nb_descripcionCompensador = nb_descripcion;
  }

  public Long getIdCamaraComp() {
    return idCamaraComp;
  }

  public void setIdCamaraComp(Long idCamaraComp) {
    this.idCamaraComp = idCamaraComp;
  }

  public int getIdSegmento() {
    return idSegmento;
  }

  public void setIdSegmento(int idSegmento) {
    this.idSegmento = idSegmento;
  }

  public Long getIdModeloNegocio() {
    return idModeloNegocio;
  }

  public void setIdModeloNegocio(Long idModeloNegocio) {
    this.idModeloNegocio = idModeloNegocio;
  }

  public Long getIdCapacidad() {
    return idCapacidad;
  }

  public void setIdCapacidad(Long idCapacidad) {
    this.idCapacidad = idCapacidad;
  }

  public Long getIdCuentaCompensacion() {
    return idCuentaCompensacion;
  }

  public void setIdCuentaCompensacion(Long idCuentaCompensacion) {
    this.idCuentaCompensacion = idCuentaCompensacion;
  }

  public Long getIdNemotecnico() {
    return idNemotecnico;
  }

  public void setIdNemotecnico(Long idNemotecnico) {
    this.idNemotecnico = idNemotecnico;
  }

  public Long getIdCompensador() {
    return idCompensador;
  }

  public void setIdCompensador(Long idCompensador) {
    this.idCompensador = idCompensador;
  }

  public String getDescripcionParametri() {
    return descripcionParametri;
  }

  public void setDescripcionParametri(String descripcionParametri) {
    this.descripcionParametri = descripcionParametri;
  }

  public String getCdCodigoRegla() {
    return cdCodigoRegla;
  }

  public void setCdCodigoRegla(String cdCodigoRegla) {
    this.cdCodigoRegla = cdCodigoRegla;
  }

  public String getDescripcionRegla() {
    return descripcionRegla;
  }

  public void setDescripcionRegla(String descripcionRegla) {
    this.descripcionRegla = descripcionRegla;
  }

  public String getCdCodigoCuentaCompensacion() {
    return cdCodigoCuentaCompensacion;
  }

  public String getNbNombreCompensador() {
    return nbNombreCompensador;
  }

  public void setCdCodigoCuentaCompensacion(String cdCodigoCuentaCompensacion) {
    this.cdCodigoCuentaCompensacion = cdCodigoCuentaCompensacion;
  }

  public void setNbNombreCompensador(String nbNombreCompensador) {
    this.nbNombreCompensador = nbNombreCompensador;
  }

  public boolean isManual() {
    return isManual;
  }

  public boolean isExcel() {
    return isExcel;
  }

  public void setManual(boolean isManual) {
    this.isManual = isManual;
  }

  public void setExcel(boolean isExcel) {
    this.isExcel = isExcel;
  }

  public boolean isOk() {
    return StringUtils.isNotBlank(getCdCodigoCuentaCompensacion()) || StringUtils.isNotBlank(getNbNombreCompensador())
        && StringUtils.isNotBlank(getReferenciaAsignacion());
  }

  @Override
  public String toString() {
    return String
        .format(
            "Tmct0parametrizacionDTO [idParametrizacion=%s, descripcionParametri=%s, idRegla=%s, idCamaraComp=%s, cd_codigoCamaraComp=%s, nb_descripcionCamaraComp=%s, idSegmento=%s, cd_codigoSegmento=%s, nb_descripcionSegmento=%s, idModeloNegocio=%s, cd_codigoModeloNegocio=%s, nb_descripcionModeloNegocio=%s, idCapacidad=%s, cd_codigoIndicCapacidad=%s, nb_descripcionIndicCapacidad=%s, referenciaCliente=%s, referenciaExterna=%s, referenciaAsignacion=%s, activo=%s, fecha_inicio=%s, fecha_fin=%s, idCuentaCompensacion=%s, cd_codigoCuentaCompensacion=%s, cdCodigoCuentaCompensacion=%s, idNemotecnico=%s, nb_nombreNmotecnico=%s, idCompensador=%s, nb_descripcionCompensador=%s, nbNombreCompensador=%s, cdCodigoRegla=%s, descripcionRegla=%s, isManual=%s, isExcel=%s]",
            idParametrizacion, descripcionParametri, idRegla, idCamaraComp, cd_codigoCamaraComp,
            nb_descripcionCamaraComp, idSegmento, cd_codigoSegmento, nb_descripcionSegmento, idModeloNegocio,
            cd_codigoModeloNegocio, nb_descripcionModeloNegocio, idCapacidad, cd_codigoIndicCapacidad,
            nb_descripcionIndicCapacidad, referenciaCliente, referenciaExterna, referenciaAsignacion, activo,
            fecha_inicio, fecha_fin, idCuentaCompensacion, cd_codigoCuentaCompensacion, cdCodigoCuentaCompensacion,
            idNemotecnico, nb_nombreNmotecnico, idCompensador, nb_descripcionCompensador, nbNombreCompensador,
            cdCodigoRegla, descripcionRegla, isManual, isExcel);
  }

}
