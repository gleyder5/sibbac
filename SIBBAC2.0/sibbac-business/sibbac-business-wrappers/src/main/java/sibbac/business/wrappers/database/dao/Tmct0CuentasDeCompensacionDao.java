package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.data.CuentaDeCompensacionData;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;

public interface Tmct0CuentasDeCompensacionDao extends JpaRepository<Tmct0CuentasDeCompensacion, Long> {
  
  Tmct0CuentasDeCompensacion findByCdCodigo(String cdCodigo);
  
  Tmct0CuentasDeCompensacion findByClientNumberAndAccountNumberAndSubaccountNumber(String clientNumber, String accountNumber, String subaccountNumber);

  @Query("SELECT CC FROM Tmct0desglosecamara DC,Tmct0CuentasDeCompensacion CC WHERE DC.tmct0infocompensacion."
         + "cdctacompensacion=CC.cdCodigo AND DC.tmct0alc=:tmct0alc")
  public Tmct0CuentasDeCompensacion findFirst1ByAlc(@Param("tmct0alc") Tmct0alc tmct0alc);

  @Query(value = "SELECT CC.CUENTA_CLEARING from Tmct0desglosecamara DC,TMCT0_CUENTAS_DE_COMPENSACION CC,"
                 + "TMCT0INFOCOMPENSACION IC WHERE IC.cdctacompensacion=CC.cd_codigo and DC.IDINFOCOMPENSACION=IC.IDINFOCOMP "
                 + "AND DC.nuorden=:nuorden and DC.nbooking=:nbooking and DC.nucnfclt=:nucnfclt and DC.nucnfliq=:nucnfliq",
         nativeQuery = true)
  public String findFirst1ByAlc(@Param("nuorden") String nuorden,
                                @Param("nbooking") String nbooking,
                                @Param("nucnfclt") String nucnfclt,
                                @Param("nucnfliq") Short nucnfliq);

  @Query("SELECT CC FROM Tmct0desglosecamara DC,Tmct0CuentasDeCompensacion CC "
         + "WHERE DC.tmct0infocompensacion.cdctacompensacion= CC.cdCodigo AND DC = :desglosecamara")
  public Tmct0CuentasDeCompensacion findByTmct0desglosecamara(@Param("desglosecamara") Tmct0desglosecamara desglosecamara);

  /**
   * Devuelve todos los ids de cuentas de compensacion que son clearing
   * 
   * @return
   */
  @Query("SELECT cc.idCuentaCompensacion FROM Tmct0CuentasDeCompensacion cc WHERE cc.cuentaClearing = 'S'")
  long[] findByCuentaClearingIsTrue();

  @Query("select cc from Tmct0desglosecamara dc join dc.tmct0infocompensacion ic, Tmct0CuentasDeCompensacion cc  "
         + "where dc.tmct0alc = :alc " + "and ic.cdctacompensacion = cc.cdCodigo ")
  public List<Tmct0CuentasDeCompensacion> findByTmct0alc(@Param("alc") Tmct0alc tmct0alc);

  @Query("SELECT new sibbac.business.wrappers.database.data.CuentaDeCompensacionData(cc) FROM Tmct0CuentasDeCompensacion cc WHERE cc.tmct0CuentaLiquidacion is null")
  List<CuentaDeCompensacionData> findAllDataNotCtaLiquidacion();

  @Query("SELECT new sibbac.business.wrappers.database.data.CuentaDeCompensacionData(cc) FROM Tmct0CuentasDeCompensacion cc")
  List<CuentaDeCompensacionData> findAllData();

  List<Tmct0CuentasDeCompensacion> findByidCuentaCompensacionIn(List<Long> ids);

  @Query("SELECT max(cc.idCuentaCompensacion) FROM Tmct0CuentasDeCompensacion cc")
  Long findMaxId();

  Tmct0CuentasDeCompensacion findByidCuentaCompensacion(long idCtaComp);

  @Query("SELECT cc.cdCodigo FROM Tmct0CuentasDeCompensacion cc WHERE cc.s3 = 'S'")
  String findCuentaCompensacionS3();
  
  @Query(value="SELECT CD_CODIGO_S3 FROM TMCT0_CUENTAS_DE_COMPENSACION " +
          "WHERE CD_CODIGO = :cdCodigo ", nativeQuery = true)
  String findCodigoS3ByCodigo(@Param("cdCodigo") String cdCodigo);

  @Query(value="SELECT cc.CD_CODIGO FROM TMCT0_CUENTAS_DE_COMPENSACION cc " +
          "INNER JOIN TMCT0_TIPO_CUENTA_DE_COMPENSACION tcc ON " +
          "cc.ID_TIPO_CUENTA = tcc.ID_TIPO_CUENTA " +
          "WHERE tcc.CD_CODIGO LIKE 'CT%'", nativeQuery = true)
  List<String> findCuentasTerceros();
  
  @Query("SELECT cc.cdCodigo FROM Tmct0CuentasDeCompensacion cc "
      + "WHERE cc.tmct0TipoCuentaDeCompensacion.cdCodigo = :cdCodigo")
  List<String> findByTipoCuenta(@Param("cdCodigo") String cdCodigo);
  
  @Query(value = "SELECT CC.CD_CODIGO AS CTA, "
      + "TC.NB_DESCRIPCION AS TIPO, CC.CD_CUENTA_COMPENSACION AS CTA_IBERCLEAR, "
      + "CC.CLIENT_NUMBER||'/'||CC.ACCOUNT_NUMBER||'/'||CC.SUBACCOUNT_NUMBER AS CTA_EUROCCP "
      + "FROM TMCT0_CUENTAS_DE_COMPENSACION CC "
      + "INNER JOIN TMCT0_TIPO_CUENTA_DE_COMPENSACION TC ON CC.ID_TIPO_CUENTA=TC.ID_TIPO_CUENTA"
      + " ORDER BY CC.CD_CODIGO ASC, TC.NB_DESCRIPCION ASC, CC.CD_CUENTA_COMPENSACION ASC ", nativeQuery = true)
  List<Object[]> labelValueList();
  
}
