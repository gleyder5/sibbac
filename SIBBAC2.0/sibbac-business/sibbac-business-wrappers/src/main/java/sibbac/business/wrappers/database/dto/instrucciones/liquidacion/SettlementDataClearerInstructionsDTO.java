package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;

import java.io.Serializable;
import java.math.BigDecimal;

import sibbac.business.fase0.database.model.Tmct0ilq;
import sibbac.common.helper.CloneObjectHelper;

public class SettlementDataClearerInstructionsDTO implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = -7035139285025958627L;

  public SettlementDataClearerInstructionsDTO() {
    initData();
  }

  public SettlementDataClearerInstructionsDTO(SettlementDataClearerInstructionsDTO ilq) {
    this();
    CloneObjectHelper.copyBasicFields(ilq, this, null);
  }

  public SettlementDataClearerInstructionsDTO(Tmct0ilq ilq) {
    this();
    CloneObjectHelper.copyBasicFields(ilq, this, null);

    cdaliass = ilq.getId().getCdaliass();
    cdsubcta = ilq.getId().getCdsubcta();
    centro = ilq.getId().getCentro();
    cdmercad = ilq.getId().getCdmercad();
    cdcleare = ilq.getId().getCdcleare();
  }

  /**
   * Table Field ACCTATCLE. ACCOUNT TO CLEARER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String acctatcle;

  /**
   * Table Field ACCTATCUS. ACCOUNT TO CUSTODIAN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String acctatcus;

  /**
   * Table Field CDALIASS. CODIGO ALIAS CUENTA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdaliass;

  /**
   * Table Field CDBROCLI. CODIGO CLIENTE Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdbrocli;

  /**
   * Table Field CDCLEARE. ENTIDAD LIQUIDADORA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdcleare;

  /**
   * Table Field CDCOMISN. TIPO GESTION COMISION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdcomisn;

  /**
   * Table Field CDCUSTOD. ENTIDAD LIQUIDADORA CUSTODIO FINAL Documentación
   * columna <!-- begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdcustod;

  /**
   * Table Field CDMERCAD. TIPO MERCADO APLICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdmercad;

  /**
   * Table Field CDMODORD. MODO ORDEN SCLV Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdmodord;

  /**
   * Table Field CDREFBAN. REFERENCIA BANCARIA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdrefban;

  /**
   * Table Field CDSUBCTA. CODIGO SUBCUENTA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdsubcta;

  /**
   * Table Field CDUSUAUD. USUARIO AUDITORIA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String cdusuaud;

  /**
   * Table Field CENTRO. CENTRO APLICACION Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String centro;

  /**
   * Table Field DESCRALI. DESCRIPCION ALIAS Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String descrali;

  /**
   * Table Field DESGLOSE. TIPO DESGLOSE LIQUIDADORA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected Character desglose;

  /**
   * Table Field FHSENDFI. DATE SEND FIDESSA Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal fhsendfi;

  /**
   * Table Field FIDESSA. FIDESSA STATUS I/D/U Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected Character fidessa;

  /**
   * Table Field OURPAR. Columna importada OURPAR Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String ourpar;

  /** NUEVAS COLUMNAS LIQUIDACION EXTRANJERO **/

  /**
   * Table Field PCCOMBCO. % COMISION BANCO Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal pccombco;

  /**
   * Table Field PCCOMBRK. % COMISION BROKER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.math.BigDecimal pccombrk;

  /**
   * Referencia titular
   * */

  protected java.lang.String rftitaud;

  /**
   * Table Field ROUTING. ROUTING S/N Documentación columna <!-- begin-user-doc
   * --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected Character routing;

  /**
   * Table Field TCLIENTE. TIPO CLIENTE Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tcliente;

  /**
   * Table Field THEIRPAR. Columna importada THEIRPAR Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String theirpar;

  /**
   * execution capacity (propios/terceros)
   * */
  protected String tpdsaldo;

  /**
   * Table Field TPSETCLE. TIPO LIQUIDACION CLEARER Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tpsetcle;

  /**
   * Table Field TPSETCUS. TIPO LIQUIDACION CUSTODIAN Documentación columna <!--
   * begin-user-doc --> <!-- end-user-doc -->
   * 
   * @generated
   **/
  protected java.lang.String tpsetcus;

  /**
   * @return the acctatcle
   */
  public java.lang.String getAcctatcle() {
    return acctatcle;
  }

  /**
   * @param acctatcle the acctatcle to set
   */
  public void setAcctatcle(java.lang.String acctatcle) {
    this.acctatcle = acctatcle;
  }

  /**
   * @return the acctatcus
   */
  public java.lang.String getAcctatcus() {
    return acctatcus;
  }

  /**
   * @param acctatcus the acctatcus to set
   */
  public void setAcctatcus(java.lang.String acctatcus) {
    this.acctatcus = acctatcus;
  }

  /**
   * @return the cdaliass
   */
  public java.lang.String getCdaliass() {
    return cdaliass;
  }

  /**
   * @param cdaliass the cdaliass to set
   */
  public void setCdaliass(java.lang.String cdaliass) {
    this.cdaliass = cdaliass;
  }

  /**
   * @return the cdbrocli
   */
  public java.lang.String getCdbrocli() {
    return cdbrocli;
  }

  /**
   * @param cdbrocli the cdbrocli to set
   */
  public void setCdbrocli(java.lang.String cdbrocli) {
    this.cdbrocli = cdbrocli;
  }

  /**
   * @return the cdcleare
   */
  public java.lang.String getCdcleare() {
    return cdcleare;
  }

  /**
   * @param cdcleare the cdcleare to set
   */
  public void setCdcleare(java.lang.String cdcleare) {
    this.cdcleare = cdcleare;
  }

  /**
   * @return the cdcomisn
   */
  public java.lang.String getCdcomisn() {
    return cdcomisn;
  }

  /**
   * @param cdcomisn the cdcomisn to set
   */
  public void setCdcomisn(java.lang.String cdcomisn) {
    this.cdcomisn = cdcomisn;
  }

  /**
   * @return the cdcustod
   */
  public java.lang.String getCdcustod() {
    return cdcustod;
  }

  /**
   * @param cdcustod the cdcustod to set
   */
  public void setCdcustod(java.lang.String cdcustod) {
    this.cdcustod = cdcustod;
  }

  /**
   * @return the cdmercad
   */
  public java.lang.String getCdmercad() {
    return cdmercad;
  }

  /**
   * @param cdmercad the cdmercad to set
   */
  public void setCdmercad(java.lang.String cdmercad) {
    this.cdmercad = cdmercad;
  }

  /**
   * @return the cdmodord
   */
  public java.lang.String getCdmodord() {
    return cdmodord;
  }

  /**
   * @param cdmodord the cdmodord to set
   */
  public void setCdmodord(java.lang.String cdmodord) {
    this.cdmodord = cdmodord;
  }

  /**
   * @return the cdrefban
   */
  public java.lang.String getCdrefban() {
    return cdrefban;
  }

  /**
   * @param cdrefban the cdrefban to set
   */
  public void setCdrefban(java.lang.String cdrefban) {
    this.cdrefban = cdrefban;
  }

  /**
   * @return the cdsubcta
   */
  public java.lang.String getCdsubcta() {
    return cdsubcta;
  }

  /**
   * @param cdsubcta the cdsubcta to set
   */
  public void setCdsubcta(java.lang.String cdsubcta) {
    this.cdsubcta = cdsubcta;
  }

  /**
   * @return the cdusuaud
   */
  public java.lang.String getCdusuaud() {
    return cdusuaud;
  }

  /**
   * @param cdusuaud the cdusuaud to set
   */
  public void setCdusuaud(java.lang.String cdusuaud) {
    this.cdusuaud = cdusuaud;
  }

  /**
   * @return the centro
   */
  public java.lang.String getCentro() {
    return centro;
  }

  /**
   * @param centro the centro to set
   */
  public void setCentro(java.lang.String centro) {
    this.centro = centro;
  }

  /**
   * @return the descrali
   */
  public java.lang.String getDescrali() {
    return descrali;
  }

  /**
   * @param descrali the descrali to set
   */
  public void setDescrali(java.lang.String descrali) {
    this.descrali = descrali;
  }

  /**
   * @return the desglose
   */
  public Character getDesglose() {
    return desglose;
  }

  /**
   * @param desglose the desglose to set
   */
  public void setDesglose(Character desglose) {
    this.desglose = desglose;
  }

  /**
   * @return the fhsendfi
   */
  public java.math.BigDecimal getFhsendfi() {
    return fhsendfi;
  }

  /**
   * @param fhsendfi the fhsendfi to set
   */
  public void setFhsendfi(java.math.BigDecimal fhsendfi) {
    this.fhsendfi = fhsendfi;
  }

  /**
   * @return the fidessa
   */
  public Character getFidessa() {
    return fidessa;
  }

  /**
   * @param fidessa the fidessa to set
   */
  public void setFidessa(Character fidessa) {
    this.fidessa = fidessa;
  }

  /**
   * @return the ourpar
   */
  public java.lang.String getOurpar() {
    return ourpar;
  }

  /**
   * @param ourpar the ourpar to set
   */
  public void setOurpar(java.lang.String ourpar) {
    this.ourpar = ourpar;
  }

  /**
   * @return the pccombco
   */
  public java.math.BigDecimal getPccombco() {
    return pccombco;
  }

  /**
   * @param pccombco the pccombco to set
   */
  public void setPccombco(java.math.BigDecimal pccombco) {
    this.pccombco = pccombco;
  }

  /**
   * @return the pccombrk
   */
  public java.math.BigDecimal getPccombrk() {
    return pccombrk;
  }

  /**
   * @param pccombrk the pccombrk to set
   */
  public void setPccombrk(java.math.BigDecimal pccombrk) {
    this.pccombrk = pccombrk;
  }

  /**
   * @return the rftitaud
   */
  public java.lang.String getRftitaud() {
    return rftitaud;
  }

  /**
   * @param rftitaud the rftitaud to set
   */
  public void setRftitaud(java.lang.String rftitaud) {
    this.rftitaud = rftitaud;
  }

  /**
   * @return the routing
   */
  public Character getRouting() {
    return routing;
  }

  /**
   * @param routing the routing to set
   */
  public void setRouting(Character routing) {
    this.routing = routing;
  }

  /**
   * @return the tcliente
   */
  public java.lang.String getTcliente() {
    return tcliente;
  }

  /**
   * @param tcliente the tcliente to set
   */
  public void setTcliente(java.lang.String tcliente) {
    this.tcliente = tcliente;
  }

  /**
   * @return the theirpar
   */
  public java.lang.String getTheirpar() {
    return theirpar;
  }

  /**
   * @param theirpar the theirpar to set
   */
  public void setTheirpar(java.lang.String theirpar) {
    this.theirpar = theirpar;
  }

  /**
   * @return the tpdsaldo
   */
  public String getTpdsaldo() {
    return tpdsaldo;
  }

  /**
   * @param tpdsaldo the tpdsaldo to set
   */
  public void setTpdsaldo(String tpdsaldo) {
    this.tpdsaldo = tpdsaldo;
  }

  /**
   * @return the tpsetcle
   */
  public java.lang.String getTpsetcle() {
    return tpsetcle;
  }

  /**
   * @param tpsetcle the tpsetcle to set
   */
  public void setTpsetcle(java.lang.String tpsetcle) {
    this.tpsetcle = tpsetcle;
  }

  /**
   * @return the tpsetcus
   */
  public java.lang.String getTpsetcus() {
    return tpsetcus;
  }

  /**
   * @param tpsetcus the tpsetcus to set
   */
  public void setTpsetcus(java.lang.String tpsetcus) {
    this.tpsetcus = tpsetcus;
  }

  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public void initData() {
    setAcctatcle("");
    // setBiccle("");// No se utiliza se toma el de cle
    // setBiccus("");// No se utiliza se toma el de cle
    setCdaliass("");
    setCdcleare("");
    // setCdcliter("");// No se utiliza
    setCdcomisn("");
    // setCdcontra("");// No se utiliza
    setCdcustod("");
    setCdmercad("");
    setCdmodord("");
    setCdrefban("");
    setCdsubcta("");
    setCdusuaud("");
    setCentro("");
    setDesglose(' ');
    // setFhaudit(BigDecimal.ZERO);
    // setFhfinal( BigDecimal.ZERO );
    // setFhinicio( BigDecimal.ZERO );
    // setNumsec( BigDecimal.ZERO );
    setPccombco(BigDecimal.ZERO);
    setTcliente("");

    // setNuctaval(""); // No se utiliza
    setOurpar("");
    setTheirpar("");
    setPccombrk(BigDecimal.ZERO);
    setTpdsaldo("");
    setCdbrocli("");
    setTpsetcle("");
    setTpsetcus("");
    setAcctatcus("");
    setDescrali("");
    setRouting(' ');
    setFidessa(' ');
    setFhsendfi(BigDecimal.ZERO);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SettlementDataClearerInstructionsDTO [acctatcle=" + acctatcle + ", acctatcus=" + acctatcus + ", cdaliass="
        + cdaliass + ", cdbrocli=" + cdbrocli + ", cdcleare=" + cdcleare + ", cdcomisn=" + cdcomisn + ", cdcustod="
        + cdcustod + ", cdmercad=" + cdmercad + ", cdmodord=" + cdmodord + ", cdrefban=" + cdrefban + ", cdsubcta="
        + cdsubcta + ", cdusuaud=" + cdusuaud + ", centro=" + centro + ", descrali=" + descrali + ", desglose="
        + desglose + ", fhsendfi=" + fhsendfi + ", fidessa=" + fidessa + ", ourpar=" + ourpar + ", pccombco="
        + pccombco + ", pccombrk=" + pccombrk + ", rftitaud=" + rftitaud + ", routing=" + routing + ", tcliente="
        + tcliente + ", theirpar=" + theirpar + ", tpdsaldo=" + tpdsaldo + ", tpsetcle=" + tpsetcle + ", tpsetcus="
        + tpsetcus + "]";
  }

}
