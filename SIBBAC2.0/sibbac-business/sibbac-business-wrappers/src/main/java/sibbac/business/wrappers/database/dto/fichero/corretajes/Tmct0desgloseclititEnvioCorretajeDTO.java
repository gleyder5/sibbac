package sibbac.business.wrappers.database.dto.fichero.corretajes;

import java.math.BigDecimal;
import java.util.Date;

public class Tmct0desgloseclititEnvioCorretajeDTO {

  private Date fechaEjecucion;
  private Date fechaOrden;
  private Date fechaLiquidacion;
  private String miembroMercado;
  private BigDecimal titulos;
  private BigDecimal efectivo;
  public Tmct0desgloseclititEnvioCorretajeDTO(Date fechaEjecucion,
                                              Date fechaOrden,
                                              Date fechaLiquidacion,
                                              String miembroMercado,
                                              BigDecimal titulos,
                                              BigDecimal efectivo) {
    super();
    this.fechaEjecucion = fechaEjecucion;
    this.fechaOrden = fechaOrden;
    this.fechaLiquidacion = fechaLiquidacion;
    this.miembroMercado = miembroMercado;
    this.titulos = titulos;
    this.efectivo = efectivo;
  }
  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }
  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }
  public Date getFechaOrden() {
    return fechaOrden;
  }
  public void setFechaOrden(Date fechaOrden) {
    this.fechaOrden = fechaOrden;
  }
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }
  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }
  public String getMiembroMercado() {
    return miembroMercado;
  }
  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }
  public BigDecimal getTitulos() {
    return titulos;
  }
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }
  public BigDecimal getEfectivo() {
    return efectivo;
  }
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }
  @Override
  public String toString() {
    return "Tmct0desgloseclititEnvioCorretajeDTO [fechaEjecucion=" + fechaEjecucion + ", fechaOrden=" + fechaOrden
           + ", fechaLiquidacion=" + fechaLiquidacion + ", miembroMercado=" + miembroMercado + ", titulos=" + titulos
           + ", efectivo=" + efectivo + "]";
  }
  

}
