package sibbac.business.wrappers.database.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dto.Tmct0cliDTO;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.dto.ClienteDTO;

@Repository
public class Tmct0cliDaoImp {
	
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0cliDaoImp.class);
	
	@PersistenceContext
	private EntityManager	em;
	
	@SuppressWarnings("unchecked")
	
	public List<Tmct0cliDTO> findDesglosesMacro(Date fcontratacionDe, Date fcontratacionA, List<String> aliasVector, List<String> clientesVector){
		
		Integer pdtDesgloseaAnual = ASIGNACIONES.PDTE_DESGLOSE_MANUAL.getId();
		
		final StringBuilder select =  new StringBuilder("select c.descli,a.cdalias,a.feejecuc,a.nbooking,a.cdisin,a.cdtpoper,a.imcbmerc,a.nutiteje ");
		final StringBuilder from   =  new StringBuilder("from bsnbpsql.tmct0bok a, bsnbpsql.tmct0ali b, bsnbpsql.tmct0cli c ");
		final StringBuilder where  =  new StringBuilder("where a.cdestadoasig = "+pdtDesgloseaAnual+" and a.cdalias = b.cdaliass and b.fhfinal=99991231 and b.cdbrocli=c.cdbrocli and c.fhfinal=99991231 ");
		
		if(fcontratacionDe != null){
			where.append(" AND a.feejecuc >= :fcontratacionDe ");
		}
		
		if(fcontratacionA != null){
			where.append(" AND a.feejecuc <= :fcontratacionA ");
		}
		
		if(aliasVector != null  && aliasVector.size() > 0  ){
			String primero = aliasVector.get(0);
			if(primero.indexOf("#") != -1 ){
				aliasVector.set(0, primero.replace("#",""));
				where.append("and b.cdaliass not in (:aliasVector) ");
			}else
				where.append("and b.cdaliass in (:aliasVector) ");
		}
		
		if(clientesVector != null  && clientesVector.size() > 0  ){
			String primero = clientesVector.get(0);
			if(primero.indexOf("#") != -1 ){
				clientesVector.set(0, primero.replace("#",""));
				where.append("and b.cdaliass not in (:clientesVector) ");
			}else
				where.append("and c.cdbrocli in (:clientesVector) ");
		}
		where.append("order by cdalias,nbooking");
		
		Query query = null;
		
//		String seleInv = FormatQueryInv.formatQueryNativa(select.append( from.append( where ) ).toString());
		
		query = em.createNativeQuery( select.append( from.append( where ) ).toString() );
		
		setParametersWithVectors( query,  fcontratacionDe, fcontratacionA, aliasVector, clientesVector, "fcontratacionDe", "fcontratacionA", "aliasVector", "clientesVector");
		
		List<Tmct0cliDTO> listaDesgloses = new ArrayList<Tmct0cliDTO>();
		Tmct0cliDTO cliDto = null;
		List< Object > resultList = null;
		
		try{
			resultList = query.getResultList();
			
			if ( CollectionUtils.isNotEmpty( resultList ) ) {
				for ( Object obj : resultList ) {

					cliDto =  Tmct0cliDTO.convertObjectToClienteDTO( ( Object[] ) obj );
					listaDesgloses.add(cliDto);
				}
			}
		}catch(Exception ex){
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}

		return listaDesgloses;
		
		
	}
	
	private void setParametersWithVectors(Query query, Date fcontratacionDe, Date fcontratacionA, List<String> aliasVector, List<String> clientesVector, 
			String fcontratacionDeParamName, String fcontratacionAParamName, String aliasvectorParamName, String clientesVectorParamName ){
	
		if ( fcontratacionDe != null ) {
			query.setParameter( fcontratacionDeParamName, fcontratacionDe );
		}
		
		if ( fcontratacionA != null ) {
			query.setParameter( fcontratacionAParamName, fcontratacionA );
		}		
		
		if ( aliasVector != null && aliasVector.size() > 0  ) {
			query.setParameter( aliasvectorParamName, aliasVector );
		}		
		
		if ( clientesVector != null && clientesVector.size() > 0  ) {
			query.setParameter( clientesVectorParamName, clientesVector );
		}				
		
	}
	
	
	
	
	
	/**
	 * 
	 * @param idCliente
	 * @param tipoDocumento
	 * @param numDocumento
	 * @param tipoResidencia
	 * @param codPersonaBDP
	 * @param numeroFactura
	 * @param tipoCliente
	 * @param naturalezaCliente
	 * @param estadoCliente
	 * @param fechaAlta
	 * @param fechaBaja
	 * @param codigoKGR
	 * @param tipoNacionalidad
	 * @param codigoPais
	 * @param categorizacion
	 * @param controlPBC
	 * @param motivoPBC
	 * @return
	 */
	public List<ClienteDTO> findClientesFiltro(Long idCliente, String tipoDocumento, String numDocumento, String tipoResidencia, String codPersonaBDP, String numeroFactura, 
			String tipoCliente, String naturalezaCliente, String estadoCliente, Date fechaAlta, Date fechaBaja, String codigoKGR, String tipoNacionalidad, String codigoPais,
			String categorizacion, String controlPBC, String motivoPBC, String riesgoPBC, Date fhRevRiesgoPbc,Long idClienteNot, String tipoDocumentoNot, String numDocumentoNot, String tipoResidenciaNot, String codPersonaBDPNot, String numeroFacturaNot, 
			String tipoClienteNot, String naturalezaClienteNot, String estadoClienteNot, String codigoKGRNot, String tipoNacionalidadNot, String codigoPaisNot,
			String categorizacionNot, String controlPBCNot, String motivoPBCNot, String riesgoPBCNot) {

		List<ClienteDTO> clientes = new ArrayList<ClienteDTO>();
		Query query;
		//String prefix = "[" + TAG + "::getContactoyServiciosPorAlias] ";
		StringBuffer qlString = new StringBuffer( "SELECT " );
		qlString.append( " cliente.idclient, cliente.descli, cliente.tpidenti, cliente.cif, cliente.tpreside, cliente.cdbdp, cliente.tpclienn, cliente.tpfisjur, cliente.cdestado, cliente.fhinicio, cliente.fhfinal, cliente.cdkgr, cliente.tpNacTit, cliente.cdNacTit, cliente.cdcat, cliente.cddeprev, cliente.motiPrev, cliente.RIESGO_PBC, cliente.FH_REV_RIESGO_PBC " );
		qlString.append( " FROM bsnbpsql.tmct0cli cliente " );
		qlString.append( " WHERE 1=1 " );
		
		if (idCliente != null) {
			qlString.append( "AND cliente.idClient = :idCliente " );
		}
		if (idClienteNot != null) {
			qlString.append( "AND cliente.idClient <> :idClienteNot " );
		}
		
		if (tipoDocumento != null && !tipoDocumento.isEmpty()) {
			qlString.append( " AND cliente.tpidenti = :tipoDocumento " );
		}
		if (tipoDocumentoNot != null && !tipoDocumentoNot.isEmpty()) {
			qlString.append( " AND cliente.tpidenti <> :tipoDocumentoNot " );
		}
		
		if (numDocumento != null && !numDocumento.isEmpty()) {
			qlString.append( " AND upper(cliente.cif) = :numDocumento " );
		}
		if (numDocumentoNot != null && !numDocumentoNot.isEmpty()) {
			qlString.append( " AND upper(cliente.cif) <> :numDocumentoNot " );
		}
		
		if (tipoResidencia != null && !tipoResidencia.isEmpty()) {
			qlString.append( " AND cliente.tpreside = :tipoResidencia " );
		}
		if (tipoResidenciaNot != null && !tipoResidenciaNot.isEmpty()) {
			qlString.append( " AND cliente.tpreside <> :tipoResidenciaNot " );
		}
		
		if (codPersonaBDP != null && !codPersonaBDP.isEmpty()) {
			qlString.append( " AND upper(cliente.cdbdp) = :codPersonaBDP " );
		}
		if (codPersonaBDPNot != null && !codPersonaBDPNot.isEmpty()) {
			qlString.append( " AND upper(cliente.cdbdp) <> :codPersonaBDPNot " );
		}
		
		if (tipoCliente != null && !tipoCliente.isEmpty()) {
			qlString.append( " AND cliente.tpclienn = :tipoCliente " );
		}
		if (tipoClienteNot != null && !tipoClienteNot.isEmpty()) {
			qlString.append( " AND cliente.tpclienn <> :tipoClienteNot " );
		}
		
		if (naturalezaCliente != null && !naturalezaCliente.isEmpty()) {
			qlString.append( " AND cliente.tpfisjur = :naturalezaCliente " );
		}
		if (naturalezaClienteNot != null && !naturalezaClienteNot.isEmpty()) {
			qlString.append( " AND cliente.tpfisjur <> :naturalezaClienteNot " );
		}
		
		if (estadoCliente != null && !estadoCliente.isEmpty()) {
			qlString.append( " AND cliente.cdestado = :estadoCliente " );
		}
		if (estadoClienteNot != null && !estadoClienteNot.isEmpty()) {
			qlString.append( " AND cliente.cdestado <> :estadoClienteNot " );
		}
		
		if (fechaAlta != null ) {
			qlString.append( " AND cliente.fhinicio = :fechaAlta " );
		}
		
    if (fechaBaja != null ) {
      qlString.append( " AND cliente.fhfinal = :fechaBaja " );
    }
		
    if (fhRevRiesgoPbc != null ) {
      qlString.append( " AND cliente.FH_REV_RIESGO_PBC = :fhRevRiesgoPbc " );
    }

		if (codigoKGR != null && !codigoKGR.isEmpty()) {
			qlString.append( " AND upper(cliente.cdkgr) = :codigoKGR " );
		}
		if (codigoKGRNot != null && !codigoKGRNot.isEmpty()) {
			qlString.append( " AND upper(cliente.cdkgr) <> :codigoKGRNot " );
		}
		
		if (tipoNacionalidad != null && !tipoNacionalidad.isEmpty()) {
			qlString.append( " AND cliente.tpNacTit = :tipoNacionalidad " );
		}
		if (tipoNacionalidadNot != null && !tipoNacionalidadNot.isEmpty()) {
			qlString.append( " AND cliente.tpNacTit <> :tipoNacionalidadNot " );
		}
		
		if (codigoPais != null && !codigoPais.isEmpty()) {
			qlString.append( " AND cliente.cdNacTit = :codigoPais " );
		}
		if (codigoPaisNot != null && !codigoPaisNot.isEmpty()) {
			qlString.append( " AND cliente.cdNacTit <> :codigoPaisNot " );
		}
		
		if (categorizacion != null && !categorizacion.isEmpty()) {
			qlString.append( " AND cliente.cdcat = :categorizacion " );
		}
		if (categorizacionNot != null && !categorizacionNot.isEmpty()) {
			qlString.append( " AND cliente.cdcat <> :categorizacionNot " );
		}
		
		if (controlPBC != null && !controlPBC.isEmpty()) {
			qlString.append( " AND cliente.cddeprev = :controlPBC " );
		}
		if (controlPBCNot != null && !controlPBCNot.isEmpty()) {
			qlString.append( " AND cliente.cddeprev <> :controlPBCNot " );
		}
		
    if (motivoPBC != null && !motivoPBC.isEmpty()) {
      qlString.append( " AND cliente.motiPrev = :motivoPBC " );
    }
    if (motivoPBCNot != null && !motivoPBCNot.isEmpty()) {
      qlString.append( " AND cliente.motiPrev <> :motivoPBCNot " );
    }
    
    if (riesgoPBC != null && !riesgoPBC.isEmpty()) {
      qlString.append( " AND cliente.RIESGO_PBC = :riesgoPbc " );
    }
    if (riesgoPBCNot != null && !riesgoPBCNot.isEmpty()) {
      qlString.append( " AND cliente.RIESGO_PBC <> :riesgoPbcNot " );
    }    
		
		try {
			query = em.createNativeQuery( qlString.toString() );
			if (idCliente != null) {
				query.setParameter( "idCliente", idCliente );
			}
			if (tipoDocumento != null && !tipoDocumento.isEmpty()) {
				query.setParameter( "tipoDocumento", (Character) tipoDocumento.toCharArray()[0] );
			}
			if (numDocumento != null && !numDocumento.isEmpty()) {
				query.setParameter( "numDocumento", numDocumento );
			}			
			if (tipoResidencia != null && !tipoResidencia.isEmpty()) {
				query.setParameter( "tipoResidencia", tipoResidencia );
			}			
			if (codPersonaBDP != null && !codPersonaBDP.isEmpty()) {
				query.setParameter( "codPersonaBDP", codPersonaBDP );
			}			
			if (tipoCliente != null && !tipoCliente.isEmpty()) {
				query.setParameter( "tipoCliente", (Character) tipoCliente.toCharArray()[0] );
			}			
			if (naturalezaCliente != null && !naturalezaCliente.isEmpty()) {
				query.setParameter( "naturalezaCliente", 
					StringHelper.suppressBracketsFromCharacter((Character) naturalezaCliente.toCharArray()[0]));
			}			
			if (estadoCliente != null && !estadoCliente.isEmpty()) {
				query.setParameter( "estadoCliente", (Character) estadoCliente.toCharArray()[0] );
			}
			if (fechaAlta != null ) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String str = sdf.format(fechaAlta);
				query.setParameter( "fechaAlta", new Integer(str) );
				
			}
			if (fechaBaja != null ) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String str = sdf.format(fechaBaja);
				query.setParameter( "fechaBaja", new Integer(str) );
			}
			
	    if (fhRevRiesgoPbc != null ) {
        query.setParameter( "fhRevRiesgoPbc", fhRevRiesgoPbc );
	    }			
			if (codigoKGR != null && !codigoKGR.isEmpty()) {
				query.setParameter( "codigoKGR", codigoKGR );
			}
			if (tipoNacionalidad != null && !tipoNacionalidad.isEmpty()) {
				query.setParameter( "tipoNacionalidad", 
					StringHelper.suppressBracketsFromCharacter((Character) tipoNacionalidad.toCharArray()[0]));
			}
			// TODO - Ver chequeo por vacio porque Antartica no tiene codigo. 
			if (codigoPais != null && !codigoPais.isEmpty()) {
				query.setParameter( "codigoPais", codigoPais.trim() );
			}
			
			if (categorizacion != null && !categorizacion.isEmpty()) {
				query.setParameter( "categorizacion", categorizacion );
			}
			if (controlPBC != null && !controlPBC.isEmpty()) {
				query.setParameter( "controlPBC", 
					StringHelper.suppressBracketsFromCharacter((Character) controlPBC.toCharArray()[0]) );
			}
      if (motivoPBC != null && !motivoPBC.isEmpty()) {
        query.setParameter( "motivoPbc", motivoPBC );
      }
      if (riesgoPBC != null && !riesgoPBC.isEmpty()) {
        query.setParameter( "riesgoPbc", riesgoPBC );
      }
		
			if (idClienteNot != null) {
				query.setParameter( "idClienteNot", idClienteNot );
			}
			if (tipoDocumentoNot != null && !tipoDocumentoNot.isEmpty()) {
				query.setParameter( "tipoDocumentoNot", (Character) tipoDocumentoNot.toCharArray()[0] );
			}
			if (numDocumentoNot != null && !numDocumentoNot.isEmpty()) {
				query.setParameter( "numDocumentoNot", numDocumentoNot );
			}			
			if (tipoResidenciaNot != null && !tipoResidenciaNot.isEmpty()) {
				query.setParameter( "tipoResidenciaNot", tipoResidenciaNot );
			}			
			if (codPersonaBDPNot != null && !codPersonaBDPNot.isEmpty()) {
				query.setParameter( "codPersonaBDPNot", codPersonaBDPNot );
			}			
			if (tipoClienteNot != null && !tipoClienteNot.isEmpty()) {
				query.setParameter( "tipoClienteNot", (Character) tipoClienteNot.toCharArray()[0] );
			}			
			if (naturalezaClienteNot != null && !naturalezaClienteNot.isEmpty()) {
				query.setParameter( "naturalezaClienteNot", 
					StringHelper.suppressBracketsFromCharacter((Character) naturalezaClienteNot.toCharArray()[0]));
			}			
			if (estadoClienteNot != null && !estadoClienteNot.isEmpty()) {
				query.setParameter( "estadoClienteNot", (Character) estadoClienteNot.toCharArray()[0] );
			}
			if (codigoKGRNot != null && !codigoKGRNot.isEmpty()) {
				query.setParameter( "codigoKGRNot", codigoKGRNot );
			}
			if (tipoNacionalidadNot != null && !tipoNacionalidadNot.isEmpty()) {
				query.setParameter( "tipoNacionalidadNot", 
					StringHelper.suppressBracketsFromCharacter((Character) tipoNacionalidadNot.toCharArray()[0]));
			}
			// TODO - Ver chequeo por vacio porque Antartica no tiene codigo. 
			if (codigoPaisNot != null && !codigoPaisNot.isEmpty()) {
				query.setParameter( "codigoPaisNot", codigoPaisNot.trim() );
			}
			
			if (categorizacionNot != null && !categorizacionNot.isEmpty()) {
				query.setParameter( "categorizacionNot", categorizacionNot );
			}
			if (controlPBCNot != null && !controlPBCNot.isEmpty()) {
				query.setParameter( "controlPBCNot", 
					StringHelper.suppressBracketsFromCharacter((Character) controlPBCNot.toCharArray()[0]) );
			}
			if (motivoPBCNot != null && !motivoPBCNot.isEmpty()) {
				query.setParameter( "motivoPBCNot", motivoPBCNot );
			}
			
			List<Object[]> resultList;
			
			try{
				resultList = query.getResultList();
				
				if ( CollectionUtils.isNotEmpty( resultList ) ) {
					for ( Object obj : resultList ) {

						ClienteDTO dto =  ClienteDTO.convertObjectToClienteDTO( ( Object[] ) obj );
						clientes.add(dto);
						
					}
				}
			} catch(Exception ex) {
				LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
			}
			
		} catch ( PersistenceException ex ) {
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
		return clientes;
		
	}
	
	public List<Object[]> findAllProyection() {
		TypedQuery<Object[]> query = em.createQuery(
			"SELECT cl.idClient, cl.cdbrocli FROM Tmct0cli AS cl", Object[].class);
		return query.getResultList();
	}
	
	public List<Object[]> findClientsByDescription(String descripCliente) {
		TypedQuery<Object[]> query = em.createQuery(
			"SELECT cl.idClient, cl.cdbrocli FROM Tmct0cli AS cl WHERE cl.cdbrocli LIKE :descripCliente ORDER BY cl.cdbrocli ASC", Object[].class);
		query.setParameter("descripCliente", "%" + descripCliente.trim() + "%");
		return query.getResultList();
	}
	
}
