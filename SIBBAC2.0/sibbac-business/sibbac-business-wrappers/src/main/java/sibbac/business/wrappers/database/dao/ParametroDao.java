package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Parametro;


@Component
public interface ParametroDao extends JpaRepository< Parametro, Long > {

	List< Parametro > findAll();

}
