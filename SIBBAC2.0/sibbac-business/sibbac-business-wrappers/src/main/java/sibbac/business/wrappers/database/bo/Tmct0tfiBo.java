package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0tfiDao;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.model.Tmct0tfi;
import sibbac.business.wrappers.database.model.Tmct0tfiId;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0tfiBo extends AbstractBo<Tmct0tfi, Tmct0tfiId, Tmct0tfiDao> {

  /************************************ QUERYS ********************************************/

  public List<Tmct0tfi> findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(String cdaliass, String cdsubcta,
      String centro, String cdmercad, BigDecimal fecha) {
    return dao.findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(cdaliass, cdsubcta, centro, cdmercad, fecha);
  }

  public List<Tmct0tfi> findAllByCdaliassSoloRepresentantes(String cdaliass, String cdsubcta) {

    String centro = "";
    String cdmercad = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    BigDecimal fecha = new BigDecimal(sdf.format(new Date()));

    return dao.findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetradeNoRepresentantes(cdaliass, cdsubcta, centro,
        cdmercad, fecha);
  }

  public List<Tmct0tfi> findAllByCdaliass(String cdaliass) {

    String cdsubcta = "";
    String centro = "";
    String cdmercad = "";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    BigDecimal fecha = new BigDecimal(sdf.format(new Date()));

    return dao.findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(cdaliass, cdsubcta, centro, cdmercad, fecha);
  }

  /************************************ FIN QUERYS ******************************************/

  @Transactional
  public void delete(String codigo) {

    StringBuilder fechaFinal = new StringBuilder();
    Calendar fechaHoy = Calendar.getInstance();
    fechaFinal.append(fechaHoy.get(Calendar.YEAR));
    int month = fechaHoy.get(Calendar.MONTH) + 1;
    if (month < 10) {
      fechaFinal.append("0");
    }

    fechaFinal.append(month);
    int day = fechaHoy.get(Calendar.DAY_OF_MONTH) - 1;
    if (day < 10) {
      fechaFinal.append("0");
    }
    fechaFinal.append(day);
    List<Tmct0tfi> list = dao.getFisByCddclaveAndFhfinal(codigo, new BigDecimal(fechaFinal.toString()));
    if (list != null && !list.isEmpty()) {

      for (Tmct0tfi tmct0tfi : list) {
        tmct0tfi.setFhfinal(new BigDecimal(fechaFinal.toString()));
        this.save(tmct0tfi);
      }
    }

  }

  public List<Tmct0tfi> getByCddclaveAndFhfinal(String codigo) throws SIBBACBusinessException {
    StringBuilder fechaFinal = new StringBuilder();
    Calendar fechaHoy = Calendar.getInstance();
    fechaFinal.append(fechaHoy.get(Calendar.YEAR));
    int month = fechaHoy.get(Calendar.MONTH) + 1;
    if (month < 10) {
      fechaFinal.append("0");
    }

    fechaFinal.append(month);
    int day = fechaHoy.get(Calendar.DAY_OF_MONTH) - 1;
    if (day < 10) {
      fechaFinal.append("0");
    }
    fechaFinal.append(day);
    return dao.getFisByCddclaveAndFhfinal(codigo, new BigDecimal(fechaFinal.toString()));

  }

  public List<Holder> findAllHoldersByCdholder(String cdholder, BigDecimal fetrade) {
    List<Holder> holders = new ArrayList<Holder>();
    List<Object[]> listaDatos = dao.findAllHolderDataTmct0fisByCdholderAndFetrade(cdholder, fetrade);
    if (CollectionUtils.isNotEmpty(listaDatos)) {
      Holder holder;
      for (Object[] data : listaDatos) {
        holder = new Holder();
        this.loadData(holder, data);
        holders.add(holder);
      }
    }
    return holders;
  }

  public List<Holder> findAllHoldersByCddclave(String cddclave, BigDecimal fetrade) {
    List<Holder> holders = new ArrayList<Holder>();
    List<Object[]> listaDatos = dao.findAllHolderDataTmct0fisByCddclaveAndFetrade(cddclave, fetrade);
    if (CollectionUtils.isNotEmpty(listaDatos)) {
      Holder holder;
      for (Object[] data : listaDatos) {
        holder = new Holder();
        this.loadData(holder, data);
        holders.add(holder);
      }
    }
    return holders;
  }

  public List<Holder> findAllHoldersByNbclient1AndJuridico(String razonSozial, BigDecimal fetrade) {
    List<Holder> holders = new ArrayList<Holder>();
    List<Object[]> listaDatos = dao.findAllHolderDataTmct0fisByNbclient1rAndFetradeAndJuridico(razonSozial, fetrade);
    if (CollectionUtils.isNotEmpty(listaDatos)) {
      Holder holder;
      for (Object[] data : listaDatos) {
        holder = new Holder();
        this.loadData(holder, data);
        holders.add(holder);
      }
    }
    return holders;
  }

  private void loadData(Holder holder, Object[] data) {
    holder.setCddclave((String) data[0]);
    holder.setTpdomici((String) data[1]);
    holder.setNbdomici((String) data[2]);
    holder.setNudomici((String) data[3]);
    holder.setNbciudad((String) data[4]);
    holder.setNbprovin((String) data[5]);
    holder.setCdpostal((String) data[6]);
    holder.setCddepais((String) data[7]);
    holder.setNudnicif((String) data[8]);
    holder.setNbclient((String) data[9]);
    holder.setNbclien1((String) data[10]);
    holder.setNbclien2((String) data[11]);
    holder.setTptiprep((Character) data[12]);
    holder.setCdnactit((String) data[13]);
    holder.setCdordtit((String) data[17]);
    holder.setRftitaud((String) data[18]);
    holder.setCdddebic((String) data[19]);
    holder.setTpidenti((Character) data[20]);
    holder.setTpsocied((Character) data[21]);
    holder.setTpnactit((Character) data[22]);
    holder.setCdholder((String) data[28]);
    holder.setFechaNacimiento((Date) data[31]);
  }

  public List<Holder> findAllHoldersByCdholderCached(Map<String, List<Holder>> cached, String cdholder,
      BigDecimal fetrade) {
    String key = String.format("%s_%s", StringUtils.trim(cdholder), fetrade);
    List<Holder> res = cached.get(key);
    if (res == null) {
      synchronized (cached) {
        if ((res = cached.get(key)) == null) {
          res = findAllHoldersByCdholder(cdholder, fetrade);
          cached.put(key, res);
        }
      }
    }
    return getNewCopy(res);
  }

  private List<Holder> getNewCopy(List<Holder> cached) {

    List<Holder> newCopy = new ArrayList<Holder>();
    if (cached != null) {
      for (Holder holder : cached) {
        newCopy.add(new Holder(holder));
      }
    }
    return newCopy;
  }

  public List<Holder> findAllHoldersByNbclient1AndJuridicoCached(Map<String, List<Holder>> cached, String razonSocial,
      BigDecimal fetrade) {
    String key = String.format("%s_%s", StringUtils.trim(razonSocial), fetrade);
    List<Holder> res = cached.get(key);
    if (res == null) {
      synchronized (cached) {
        if ((res = cached.get(key)) == null) {
          res = findAllHoldersByNbclient1AndJuridico(razonSocial, fetrade);
          cached.put(key, res);
        }
      }
    }
    return getNewCopy(res);
  }

  public List<Holder> findAllHolderForAutocompleter(String holderData, BigDecimal fetrade) {
    Holder holder;
    List<Holder> listHolders = new ArrayList<Holder>();
    holderData = holderData.replace(" ", "%");
    holderData = String.format("%s%s%s", "%", holderData, "%");
    List<Object[]> o = dao.findAllHoldersForAutocompleter(holderData, fetrade);
    for (Object[] objects : o) {
      holder = new Holder();
      this.loadData(holder, objects);
      listHolders.add(holder);
    }
    return listHolders;
  }
}
