package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.FacturaRectificativa;

@Component
public interface FacturaRectificativaDao extends JpaRepository< FacturaRectificativa, Long >,
		JpaSpecificationExecutor< FacturaRectificativa > {

	public List< FacturaRectificativa > findByEstado( final Tmct0estado estado );
	
	@Query("SELECT f FROM FacturaRectificativa f WHERE f.estado.id not in (:estado) and (f.enviado is null or f.enviado = false) and f.fechaCreacion >= :fechaDesde and f.fechaCreacion <= :fechaHasta")
	public List<FacturaRectificativa> findRectificativasNoEnviadasByIdEstadoDistinctBetweenFechas(@Param("estado") List<Integer> estado, @Param("fechaDesde") Date fechaDesde, @Param("fechaHasta") Date fechaHasta);
	
	@Modifying @Query("UPDATE FacturaRectificativa SET enviado = true WHERE id = :id")
	public int marcaEnviado(@Param("id") long id);

}
