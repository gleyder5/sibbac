package sibbac.business.wrappers.common;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

public class ObserverProcess implements Serializable{

  /**
   * 
   */
  private static final long serialVersionUID = 4045832429454222876L;
  private Integer iProcesos;
  private Integer iProcesosFinalizado;
  private Exception exception;
  private Collection<Object> hilos;

  public ObserverProcess() {
    iProcesos = 0;
    iProcesosFinalizado = 0;
    hilos = new HashSet<Object>();
  }

  public void addErrorProcess(Exception exception) {
    this.exception = exception;
  }

  public void setProcessFinish(boolean procesoFinish) {
    if (procesoFinish) {
      iProcesosFinalizado++;
    }
  }

  public boolean isProcessFinish() {
    return (iProcesos.compareTo(iProcesosFinalizado) == 0);
  }

  public Exception getException() {
    return exception;
  }

  public void setException(Exception exception) {
    this.exception = exception;
  }

  private void addProceso() {
    iProcesos++;
  }

  private void addFinalizado() {
    iProcesosFinalizado++;
  }

  public synchronized void addThread(Object thread) {
    hilos.add(thread);
    addProceso();
  }

  public synchronized void sutDownThread(Object thread) {
    hilos.remove(thread);
    addFinalizado();
  }

  public int threadsMonitorized() {
    return hilos.size();
  }
}
