package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.CodigoTpnactit;


@Repository
public interface CodigoTpnactitDao extends JpaRepository< CodigoTpnactit, String > {

	public CodigoTpnactit findByCodigo( String codigo );

}
