package sibbac.business.wrappers.database.filter;

import java.util.Date;
import java.util.List;
import java.util.Map;
import static java.lang.String.format;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.DynamicColumn;
import sibbac.database.QueryFilter;

public class FinalHoldersQuery implements QueryFilter {
  
  private final FinalHoldersFilter filter;
  
  private static final String SELECT =  "SELECT * FROM TMCT0FIS ";
  
  private static final String WHERE
      = "WHERE NBCLIENT LIKE '%%%1$s%%' OR NBCLIEN1 LIKE '%%%1$s%%' OR NBCLIEN2 LIKE '%%%1$s%%' "
      + "OR CDDCLAVE LIKE '%%%1$s%%' ";

  public FinalHoldersQuery(FinalHoldersFilter filter) {
    this.filter = filter;
  }
  
  
  @Override
  public String getCountQuery() {
    return null;
  }

  @Override
  public String getSelectQuery() throws SIBBACBusinessException {
    final StringBuilder builder;
    
    builder = new StringBuilder(SELECT);
    if(filter.isThereName()) {
      builder.append(format(WHERE, filter.getName().toUpperCase()));
    }
    return builder.toString();
  }

  @Override
  public int setCountResults(Map<Date, Long> dates) {
    return 0;
  }

  @Override
  public boolean isCountNecessary() {
    return false;
  }

  @Override
  public boolean isIsoFormat() {
    return false;
  }

  @Override
  public String getSingleFilter() {
    return filter.getName();
  }

  @Override
  public List<DynamicColumn> getColumns() {
    return null;
  }


}
