package sibbac.business.wrappers.database.dto.assembler;

import java.text.ParseException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.AddressDao;
import sibbac.business.wrappers.database.dao.Tmct0ClientDao;
import sibbac.business.wrappers.database.model.Tmct0Address;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_ADDRESSES
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0AddressAssembler {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0AddressAssembler.class);

	@Autowired
	AddressDao addressDao;
	
	@Autowired
	Tmct0ClientDao clientDao;
	
	/**
	 * Realiza la conversion entre los datos del front y la entity a gestionar en back
	 * 
	 * @param paramsObjects -> datos del front
	 * @return Entity - Entity a gestionar en la parte back
	 */
	public Tmct0Address getTmct0AddressEntity(Map<?, ?> datosCliente) throws ParseException {
		LOG.debug("Inicio de la transformación getTmct0AddressEntityList");
		Map<?, ?> address = (Map<?, ?>) datosCliente.get("address");
		Tmct0Address miEntity = new Tmct0Address();
		if (address.get("idAddress") != null) {
			Long laddress = Long.valueOf(address.get("idAddress").toString());
			if (laddress != 0) {
				miEntity = this.addressDao.findOne(laddress);
			}
		}
		for (Map.Entry<?, ?> entry : address.entrySet()) {
			switch (entry.getKey().toString()) {
			case "cdDePais":
				miEntity.setCdDePais((String) entry.getValue());
				break;
			case "tpDomici":
				miEntity.setTpDomici((String) entry.getValue());
				break;
			case "nbDomici":
				miEntity.setNbDomici((String) entry.getValue());
				break;
			case "nuDomici":
				miEntity.setNuDomici((String) entry.getValue());
				break;
			case "nbCiudad":
				miEntity.setNbCiudad((String) entry.getValue());
				break;
			case "nbProvin":
				miEntity.setNbProvin((String) entry.getValue());
				break;
			case "cdPostal":
				miEntity.setCdPostal((String) entry.getValue());
				break;
			case "idClient":
				miEntity.setCliente( clientDao.findOne(new Long((Integer) entry.getValue())));
				break;
			}
		}
		return miEntity; 
	}

}	
