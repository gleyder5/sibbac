package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;

public class EnvioCorretajeTmct0alcDataDTO {

  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private short nucnfliq;
  private BigDecimal titulos;
  private BigDecimal efectivo;
  private BigDecimal corretaje;
  private String alias;
  private String descripcionAlias;
  private String isin;
  private String entidadParticipante;
  private Character sentido;
  public EnvioCorretajeTmct0alcDataDTO(String nuorden,
                                       String nbooking,
                                       String nucnfclt,
                                       short nucnfliq,
                                       BigDecimal titulos,
                                       BigDecimal efectivo,
                                       BigDecimal corretaje,
                                       String alias,
                                       String descripcionAlias,
                                       String isin,
                                       String entidadParticipante,
                                       Character sentido) {
    super();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.titulos = titulos;
    this.efectivo = efectivo;
    this.corretaje = corretaje;
    this.alias = alias;
    this.descripcionAlias = descripcionAlias;
    this.isin = isin;
    this.entidadParticipante = entidadParticipante;
    this.sentido = sentido;
  }
  public String getNuorden() {
    return nuorden;
  }
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }
  public String getNbooking() {
    return nbooking;
  }
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }
  public String getNucnfclt() {
    return nucnfclt;
  }
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }
  public short getNucnfliq() {
    return nucnfliq;
  }
  public void setNucnfliq(short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }
  public BigDecimal getTitulos() {
    return titulos;
  }
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }
  public BigDecimal getEfectivo() {
    return efectivo;
  }
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }
  public BigDecimal getCorretaje() {
    return corretaje;
  }
  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }
  public String getAlias() {
    return alias;
  }
  public void setAlias(String alias) {
    this.alias = alias;
  }
  public String getDescripcionAlias() {
    return descripcionAlias;
  }
  public void setDescripcionAlias(String descripcionAlias) {
    this.descripcionAlias = descripcionAlias;
  }
  public String getIsin() {
    return isin;
  }
  public void setIsin(String isin) {
    this.isin = isin;
  }
  public String getEntidadParticipante() {
    return entidadParticipante;
  }
  public void setEntidadParticipante(String entidadParticipante) {
    this.entidadParticipante = entidadParticipante;
  }
  public Character getSentido() {
    return sentido;
  }
  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }
  @Override
  public String toString() {
    return "EnvioCorretajeTmct0alcDataDTO [nuorden=" + nuorden + ", nbooking=" + nbooking + ", nucnfclt=" + nucnfclt
           + ", nucnfliq=" + nucnfliq + ", titulos=" + titulos + ", efectivo=" + efectivo + ", corretaje=" + corretaje
           + ", alias=" + alias + ", descripcionAlias=" + descripcionAlias + ", isin=" + isin
           + ", entidadParticipante=" + entidadParticipante + ", sentido=" + sentido + "]";
  }
  
  
  

}
