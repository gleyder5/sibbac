package sibbac.business.wrappers.database.bo.bancaPrivadaWriter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ord;

public class Record18Bean extends RecordBean {

  protected static final Logger LOG = LoggerFactory.getLogger(Record18Bean.class);

  /**
	 * 
	 */
  private static final long serialVersionUID = 1216714022871615352L;
  // Tam 2
  private Integer tipoRegistroR18;
  // Tam 16
  private String numrefer;
  // Tam 8
  private String fecontr;
  // Tam 6
  private String horcontr;
  // Tam 3
  private String crefext;
  // Tam 16
  private String refextsv;
  // Tam 3
  private String codorgej;
  // Tam 4
  private String emprctva;
  // Tam 4
  private String centctva;
  // Tam 2
  private String dgcoctva;
  // Tam 3
  private String prodctva;
  // Tam 3
  private String dptectva;
  // Tam 1
  private Character tipopera;
  // Tam 12
  private String codisin;
  // Tam 1
  private String tipocamb;
  // Tam 10 (6,4) BIGDECIMAL
  private String cambio;
  // Tam 4
  private String intfinan;
  // Tam 8
  private String fecliqui;
  // Tam 11
  private String numtitej;
  // Tam 15 (13,2) BIGDECIMAL
  private String nominej;
  // Tam 3
  private String canal;
  // Tam 2
  private String tipoprop;
  // Tam 16
  private String nejecmer;
  // Tam 20
  private String numejecu;
  // Tam 4
  private String cdorgneg;
  // Tam 9
  private String cdbroker;
  // Tam 20
  private String reftit;
  // Tam 6
  private String coderr;
  // Tam 20
  private String refadic;
  // Tam 60
  private String descerr;

  public Record18Bean(Tmct0ord ord,
                      Tmct0eje eje,
                      Tmct0alo alo,
                      String refTitular,
                      String referenciaExtSv,
                      BigDecimal nutitulos) {

    this.tipoRegistroR18 = 18;
    setTipoRegistro(tipoRegistroR18 + "");
    refTitular = StringUtils.rightPad(refTitular, 20);
    referenciaExtSv = StringUtils.rightPad(referenciaExtSv, 16);
    this.numrefer = ord.getNuorden().trim();

    DateFormat df = new SimpleDateFormat("yyyyMMdd");
    Date feccontrBruta = eje.getFeejecuc();
    this.fecontr = df.format(feccontrBruta);
    DateFormat hf = new SimpleDateFormat("HHmmss");
    Date hordcontrBruta = eje.getHoejecuc();
    this.horcontr = hf.format(hordcontrBruta);
    this.crefext = "SPB";
    this.refextsv = referenciaExtSv.substring(0, 16);
    this.codorgej = StringHelper.cdBrokerToCode(eje.getCdbroker());

    // String refTitular = alo.getReftitular();
    this.emprctva = refTitular.substring(0, 4);
    this.centctva = refTitular.substring(4, 8);
    // this.dgcoctva = refTitular.substring( 8, 10 );
    // this.prodctva = refTitular.substring( 10, 13 );
    this.dgcoctva = "**";
    this.prodctva = "400";
    this.dptectva = refTitular.substring(13, 20);
    this.tipopera = ord.getCdtpoper();
    this.codisin = ord.getCdisin();
    this.tipocamb = "E";
    this.cambio = StringHelper.formatBigDecimal(eje.getImcbmerc().setScale(4, BigDecimal.ROUND_HALF_UP), 6, 4);
    this.intfinan = "V025";
    Date fecliquiBruta = eje.getFevalor();
    this.fecliqui = df.format(fecliquiBruta);
    this.numtitej = StringHelper.formatBigDecimal(nutitulos, 11, 0);
    this.nominej = "000000000000000";
    this.canal = ord.getCdcanal();
    this.tipoprop = "06";
    try {
      String nejecmerPrevio = StringUtils.leftPad(eje.getNurefere().trim().trim(), 9, "0");
      this.nejecmer = StringHelper.padStringWithSpacesRight(nejecmerPrevio, 16);
      this.numejecu = StringHelper.padStringWithSpacesRight(eje.getId().getNuejecuc().trim(), 20);
    } catch (Exception e) {
      LOG.error("[Record18Bean :: Constructor] Error convirtiendo el nejecmer/numejecu: " + e.getMessage());
    }
    this.cdorgneg = eje.getExecutionTradingVenue();
    this.cdbroker = "0000";
    // this.reftit = alo.getReftitular();
    this.reftit = emprctva + centctva + dgcoctva + prodctva + dptectva;
    this.coderr = "      ";
    try {
      this.refadic = StringHelper.padStringWithSpacesRight(alo.getId().getNucnfclt().trim(), 20);
    } catch (Exception e) {
      LOG.error("[Record18Bean :: Constructor] Error convirtiendo refadic: " + e.getMessage());
    }
    this.descerr = "                                                            ";
  }

  public Integer getTipoRegistroR18() {
    return tipoRegistroR18;
  }

  public void setTipoRegistroR18(Integer tipoRegistroR18) {
    this.tipoRegistroR18 = tipoRegistroR18;
  }

  public String getNumrefer() {
    return numrefer;
  }

  public void setNumrefer(String numrefer) {
    this.numrefer = numrefer;
  }

  public String getFecontr() {
    return fecontr;
  }

  public void setFecontr(String fecontr) {
    this.fecontr = fecontr;
  }

  public String getHorcontr() {
    return horcontr;
  }

  public void setHorcontr(String horcontr) {
    this.horcontr = horcontr;
  }

  public String getCrefext() {
    return crefext;
  }

  public void setCrefext(String crefext) {
    this.crefext = crefext;
  }

  public String getRefextsv() {
    return refextsv;
  }

  public void setRefextsv(String refextsv) {
    this.refextsv = refextsv;
  }

  public String getCodorgej() {
    return codorgej;
  }

  public void setCodorgej(String codorgej) {
    this.codorgej = codorgej;
  }

  public String getEmprctva() {
    return emprctva;
  }

  public void setEmprctva(String emprctva) {
    this.emprctva = emprctva;
  }

  public String getCentctva() {
    return centctva;
  }

  public void setCentctva(String centctva) {
    this.centctva = centctva;
  }

  public String getDgcoctva() {
    return dgcoctva;
  }

  public void setDgcoctva(String dgcoctva) {
    this.dgcoctva = dgcoctva;
  }

  public String getProdctva() {
    return prodctva;
  }

  public void setProdctva(String prodctva) {
    this.prodctva = prodctva;
  }

  public String getDptectva() {
    return dptectva;
  }

  public void setDptectva(String dptectva) {
    this.dptectva = dptectva;
  }

  public Character getTipopera() {
    return tipopera;
  }

  public void setTipopera(Character tipopera) {
    this.tipopera = tipopera;
  }

  public String getCodisin() {
    return codisin;
  }

  public void setCodisin(String codisin) {
    this.codisin = codisin;
  }

  public String getTipocamb() {
    return tipocamb;
  }

  public void setTipocamb(String tipocamb) {
    this.tipocamb = tipocamb;
  }

  public String getCambio() {
    return cambio;
  }

  public void setCambio(String cambio) {
    this.cambio = cambio;
  }

  public String getIntfinan() {
    return intfinan;
  }

  public void setIntfinan(String intfinan) {
    this.intfinan = intfinan;
  }

  public String getFecliqui() {
    return fecliqui;
  }

  public void setFecliqui(String fecliqui) {
    this.fecliqui = fecliqui;
  }

  public String getNumtitej() {
    return numtitej;
  }

  public void setNumtitej(String numtitej) {
    this.numtitej = numtitej;
  }

  public String getNominej() {
    return nominej;
  }

  public void setNominej(String nominej) {
    this.nominej = nominej;
  }

  public String getCanal() {
    return canal;
  }

  public void setCanal(String canal) {
    this.canal = canal;
  }

  public String getTipoprop() {
    return tipoprop;
  }

  public void setTipoprop(String tipoprop) {
    this.tipoprop = tipoprop;
  }

  public String getNejecmer() {
    return nejecmer;
  }

  public void setNejecmer(String nejecmer) {
    this.nejecmer = nejecmer;
  }

  public String getNumejecu() {
    return numejecu;
  }

  public void setNumejecu(String numejecu) {
    this.numejecu = numejecu;
  }

  public String getCdorgneg() {
    return cdorgneg;
  }

  public void setCdorgneg(String cdorgneg) {
    this.cdorgneg = cdorgneg;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public String getReftit() {
    return reftit;
  }

  public void setReftit(String reftit) {
    this.reftit = reftit;
  }

  public String getCoderr() {
    return coderr;
  }

  public void setCoderr(String coderr) {
    this.coderr = coderr;
  }

  public String getRefadic() {
    return refadic;
  }

  public void setRefadic(String refadic) {
    this.refadic = refadic;
  }

  public String getDescerr() {
    return descerr;
  }

  public void setDescerr(String descerr) {
    this.descerr = descerr;
  }

  /**
   * Transforma el entero (integer) que recibe en una cadena del tamaño que se le pase en length rellenando con ceros
   * por la izqueirda
   * 
   * @param integer
   * @param length
   * @return
   */
  public String integerToFixedLengthString(Integer integer, Integer length) throws Exception {
    String finalString = integer.toString();
    if (finalString.length() == length) {
      return finalString;
    } else if (finalString.length() < length) {
      int diferencia = length - finalString.length();
      finalString = String.format("%0" + diferencia + "d", integer);
      return finalString;
    } else {
      throw new Exception();
    }
  }

  /**
   * Rellena la cadena con espacios hasta llegar a la longitud
   */
  public String padStringWithSpaces(String finalString, Integer length) throws Exception {
    if (finalString.length() == length) {
      return finalString;
    } else if (finalString.length() < length) {
      finalString = String.format("%" + length + "s", finalString);
      return finalString;
    } else {
      throw new Exception();
    }
  }

}
