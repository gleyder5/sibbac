package sibbac.business.wrappers.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.FacturaBo;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

// For Testing:
@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_FACTURA, interval = 7, intervalUnit = IntervalUnit.MINUTE)
// @SIBBACJob( name = Task.GROUP_FACTURACION.JOB_FACTURA, group =
// Task.GROUP_FACTURACION.JOB_FACTURA, interval = 1, intervalUnit =
// IntervalUnit.HOUR )
public class TaskFactura extends WrapperTaskConcurrencyPrevent {

    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskFactura.class);

    @Autowired
    protected FacturaBo facturaBo;

    public TaskFactura() {
    }

    @Override
    public void executeTask() {
	final String prefix = "[TaskFactura] ";
	executeTask(prefix);

    }

    private void executeTask(final String prefix) {
	final List<String> taskMessages = new ArrayList<String>();
	try {
	    LOG.trace(prefix + "[Started...]");
	    final List<Factura> facturas = facturaBo
		    .generarFacturas(taskMessages);
	    if (CollectionUtils.isNotEmpty(facturas)) {
		taskMessages.add("Generadas " + facturas.size() + " facturas");
	    } else {
		taskMessages.add("No se han generado facturas");
	    }
	} catch (Exception e) {
	    LOG.debug(prefix + "ERROR: " + e.getMessage());
	    taskMessages.add("ERROR: " + e.getMessage());
	    throw e;
	} finally {
	    if (CollectionUtils.isNotEmpty(taskMessages)) {
		for (final String taskMessage : taskMessages) {
		    blog(prefix + "[" + taskMessage + "]");
		}
	    }
	    LOG.trace(prefix + "[Finished...]");
	}
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_FACTURAS_FACTURA;
    }

}
