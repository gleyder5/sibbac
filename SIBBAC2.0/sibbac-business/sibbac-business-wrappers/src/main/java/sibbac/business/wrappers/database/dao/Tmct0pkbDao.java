package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ctd;
import sibbac.business.wrappers.database.model.Tmct0pkb;

@Repository
public interface Tmct0pkbDao extends JpaRepository<Tmct0pkb, BigDecimal> {

  @Query(value = "SELECT NEXT VALUE FOR PKBRO_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  Integer getNewPkbroker();

  @Query(value = "SELECT NEXT VALUE FOR NPKB_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  Integer getNewNuagrpkb();

  @Query(value = "SELECT NEXT VALUE FOR NBRK_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  Integer getNewNuagrbrk();

  List<Tmct0pkb> findAllByNuagrpkb(Integer nuagrpkb);

  @Query("select d from Tmct0pke e, Tmct0ctd d "
      + "where e.id.nuorden=d.id.nuorden and e.id.nbooking=d.id.nbooking and e.id.cdbroker=d.id.cdbroker"
      + " and e.id.cdmercad=d.id.cdmercad and e.id.nucnfclt=d.id.nucnfclt and e.id.nualose1=d.id.nualosec "
      + " and e.id.pkbroker = :ppkbroker ")
  List<Tmct0ctd> findAllTmct0ctdByPkbroker(@Param("ppkbroker") Integer pkbroker);

  @Query("select d from Tmct0pke e, Tmct0ctd d, Tmct0pkb b where b.pkbroker=e.id.pkbroker "
      + " and e.id.nuorden=d.id.nuorden and e.id.nbooking=d.id.nbooking and e.id.cdbroker=d.id.cdbroker"
      + " and e.id.cdmercad=d.id.cdmercad and e.id.nucnfclt=d.id.nucnfclt and e.id.nualose1=d.id.nualosec "
      + " and b.nuagrpkb = :pnuagrpkb ")
  List<Tmct0ctd> findAllTmct0ctdByNuagrpkb(@Param("pnuagrpkb") Integer nuagrpkb);

}
