package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.Tmct0eje;

public class Tmct0ejeDTO {
  private BigDecimal nutiteje;
  private BigDecimal imcbmerc;

  public Tmct0ejeDTO() {
    this.nutiteje = BigDecimal.ZERO;
    this.imcbmerc = BigDecimal.ZERO;
  }

  public Tmct0ejeDTO(Tmct0eje eje) {
    this.nutiteje = eje.getNutiteje();
    this.imcbmerc = eje.getImcbmerc();
  }

  public BigDecimal getNutiteje() {
    return nutiteje;
  }

  public void setNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje;
  }

  public BigDecimal getImcbmerc() {
    return imcbmerc;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  public void addNutiteje(BigDecimal nutiteje) {
    this.nutiteje = nutiteje.add(this.nutiteje);
  }

}
