package sibbac.business.wrappers.database.dto;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0alo;

public class TransaccionesDTO {
	
	private String							nuorden;
	private String							nbooking;
	private String							nucnfclt;
	private String							cdalias;
	private Date							fetrade;
	private String 							cdisin;
	private BigDecimal						nutitcli;
	private BigDecimal						eutotbru;
	private BigDecimal						imgatdvs;
	private String							nombrealias;
	private Date                            fevalor;
	private String                          nbvalors;
	private BigDecimal						eutotnet;
	private String 							mercado;
//	private String                          nucnfliq;
	
	
	public TransaccionesDTO() {
	}

	public TransaccionesDTO(Tmct0alo alo) {
		
		
		
	}

	public String getNuorden() {
		return nuorden;
	}


	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}


	public String getNbooking() {
		return nbooking;
	}


	public void setNbooking(String nbooking) {
		this.nbooking = nbooking;
	}


	public String getNucnfclt() {
		return nucnfclt;
	}


	public void setNucnfclt(String nucnfclt) {
		this.nucnfclt = nucnfclt;
	}


	public String getCdalias() {
		return cdalias;
	}


	public void setCdalias(String cdalias) {
		this.cdalias = cdalias;
	}


	public Date getFetrade() {
		return fetrade;
	}


	public void setFetrade(Date fetrade) {
		this.fetrade = fetrade;
	}


	public String getCdisin() {
		return cdisin;
	}


	public void setCdisin(String cdisin) {
		this.cdisin = cdisin;
	}


	public BigDecimal getNutitcli() {
		return nutitcli;
	}


	public void setNutitcli(BigDecimal nutitcli) {
		this.nutitcli = nutitcli;
	}


	public BigDecimal getEutotbru() {
		return eutotbru;
	}


	public void setEutotbru(BigDecimal eutotbru) {
		this.eutotbru = eutotbru;
	}


	public BigDecimal getImgatdvs() {
		return imgatdvs;
	}


	public void setImgatdvs(BigDecimal imgatdvs) {
		this.imgatdvs = imgatdvs;
	}


	public String getNombrealias() {
		return nombrealias;
	}


	public void setNombrealias(String nombrealias) {
		this.nombrealias = nombrealias;
	}

	public Date getFevalor() {
		return fevalor;
	}

	public void setFevalor(Date fevalor) {
		this.fevalor = fevalor;
	}

	public String getNbvalors() {
		return nbvalors;
	}

	public void setNbvalors(String nbvalors) {
		this.nbvalors = nbvalors;
	}

	public BigDecimal getEutotnet() {
		return eutotnet;
	}

	public void setEutotnet(BigDecimal eutotnet) {
		this.eutotnet = eutotnet;
	}

	public String getMercado() {
		return mercado;
	}

	public void setMercado(String mercado) {
		this.mercado = mercado;
	}
	
	
	
	
	// Setters y Getters.
	
}
