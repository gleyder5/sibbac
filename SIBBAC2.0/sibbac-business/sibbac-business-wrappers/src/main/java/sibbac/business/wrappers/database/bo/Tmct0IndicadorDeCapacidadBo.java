package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0IndicadorDeCapacidadDao;
import sibbac.business.wrappers.database.model.Tmct0IndicadorDeCapacidad;
import sibbac.business.wrappers.database.model.Tmct0Segmento;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0IndicadorDeCapacidadBo extends AbstractBo<Tmct0IndicadorDeCapacidad, Long, Tmct0IndicadorDeCapacidadDao> {

	/** The Constant LOG. */
	private static final Logger	LOG	= LoggerFactory.getLogger( Tmct0IndicadorDeCapacidadBo.class );
	
	public List< Tmct0IndicadorDeCapacidad > findAll() {    
		return this.dao.findAll(new Sort(Sort.Direction.ASC,"descripcion"));
	}
	public Tmct0IndicadorDeCapacidad findByDescripcion(String descripcion){
		return this.dao.findByDescripcion(descripcion);
	}
}
