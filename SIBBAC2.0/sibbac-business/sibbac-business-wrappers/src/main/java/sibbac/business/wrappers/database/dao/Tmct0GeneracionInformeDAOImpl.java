package sibbac.business.wrappers.database.dao;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import sibbac.common.SIBBACBusinessException;

@Repository
public class Tmct0GeneracionInformeDAOImpl {
  
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0GeneracionInformeDAOImpl.class);
  
  @Value("${generacioninformes.limite.registros.lectura.query}")
  private int limiteLecturaRegistrosQuery;
  
  @PersistenceContext
  private EntityManager em;

  @SuppressWarnings("unchecked")
  public List<Object[]> ejecutaQuery(String query) throws SIBBACBusinessException {
    LOG.debug("Ejecutando informe query: {}", query);
    List<Object[]> resultado =null;
    try{
      resultado = em.createNativeQuery(query).getResultList();
    }catch(Exception e){
      throw new SIBBACBusinessException(MessageFormat.format("Error en Query: {0} - {1}", query, e.getMessage()), e);   
    }    
    LOG.debug("Resultados de la query {}", resultado.size());    
    return resultado;
  }
  
  
  
  @SuppressWarnings("unchecked")
  public int ejecutaQueryToFile(String query, List<Integer> iColumnasOrdencacion, String rutaInformes, String nbDescription, 
                         Date genInicio, Date fechaEjecucion) throws SIBBACBusinessException {
    
    LOG.debug("Ejecutando informe query: " + query);
    
    int numRows = 0;
    List<Object[]> resultados = new ArrayList<>();
    SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
    SimpleDateFormat sdCampoDate = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat sdCampoTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat sdCampoTime = new SimpleDateFormat("HH:mm:ss");
    
    try{
      
      int beginIndex = 0;
      int numResult = limiteLecturaRegistrosQuery;
      
      String fechaEjecucionAux = "";
      if (null==fechaEjecucion || "".equals(fechaEjecucion)) {
        fechaEjecucionAux = sdf.format(new Date());
      } else {
        fechaEjecucionAux = sdf.format(fechaEjecucion);
      }
  
   try(FileWriter  fw = new FileWriter( rutaInformes + "temp/" + nbDescription + genInicio.getTime()  + fechaEjecucionAux + ".csv", true);
       BufferedWriter  bw = new BufferedWriter(fw);
       PrintWriter  out = new PrintWriter(bw);){

      
        do{
          LOG.debug("Memoria OK: " + Runtime.getRuntime().freeMemory() + fechaEjecucionAux);
          resultados = em.createNativeQuery(query).setFirstResult(beginIndex).setMaxResults(numResult).getResultList(); 
          
          if(resultados!= null && !resultados.isEmpty()){
            
            numRows = numRows + resultados.size();
            int restaIndex = beginIndex == 0?1:2;
                for (Object[] resultado : resultados) {
                  for (int j=0;j<resultado.length;j++) {
                    if(j<(iColumnasOrdencacion.size())){
                        if(resultado[iColumnasOrdencacion.get(j)] instanceof BigInteger){
                            if(j == (resultado.length-restaIndex)){
                              out.print(String.valueOf(((BigInteger)resultado[iColumnasOrdencacion.get(j)]).longValue()));
                            }else{
                              out.print(String.valueOf(((BigInteger)resultado[iColumnasOrdencacion.get(j)]).longValue()) + ";");
                            }   
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof BigDecimal){
                            if(j == (resultado.length-restaIndex)){
                              out.print(String.valueOf(((BigDecimal)resultado[iColumnasOrdencacion.get(j)]).doubleValue()));
                            }else{
                              out.print(String.valueOf(((BigDecimal)resultado[iColumnasOrdencacion.get(j)]).doubleValue()) + ";");
                            }
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof Time){
                            if(j == (resultado.length-restaIndex)){
                              out.print(sdCampoTime.format((Time)resultado[iColumnasOrdencacion.get(j)]));
                            }else{
                              out.print(sdCampoTime.format((Time)resultado[iColumnasOrdencacion.get(j)]) + ";");
                            }
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof Date){
                            if(j == (resultado.length-restaIndex)){
                              out.print(sdCampoDate.format((Date)resultado[iColumnasOrdencacion.get(j)]));
                            }else{
                              out.print(sdCampoDate.format((Date)resultado[iColumnasOrdencacion.get(j)]) + ";");
                            }
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof Timestamp){
                            if(j == (resultado.length-restaIndex)){
                              out.print(sdCampoTimestamp.format((Timestamp)resultado[iColumnasOrdencacion.get(j)]));
                            }else{
                              out.print(sdCampoTimestamp.format((Timestamp)resultado[iColumnasOrdencacion.get(j)]) + ";");
                            }
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof Double){
                            if(j == (resultado.length-restaIndex)){
                              out.print(String.valueOf((Double)resultado[iColumnasOrdencacion.get(j)]));
                            }else{
                              out.print(String.valueOf((Double)resultado[iColumnasOrdencacion.get(j)]) + ";");
                            }
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof Character){
                            if(j == (resultado.length-restaIndex)){
                              out.print(String.valueOf((Character)resultado[iColumnasOrdencacion.get(j)]));
                            }else{
                              out.print(String.valueOf((Character)resultado[iColumnasOrdencacion.get(j)]) + ";");
                            }
                        }else if(resultado[iColumnasOrdencacion.get(j)] instanceof Short){
                            if(j == (resultado.length-restaIndex)){
                              out.print(String.valueOf((Short)resultado[iColumnasOrdencacion.get(j)]));
                            }else{
                              out.print(String.valueOf((Short)resultado[iColumnasOrdencacion.get(j)]) + ";");
                            }     
                        }else{
                          if(resultado[iColumnasOrdencacion.get(j)]!=null){
                              if(j == (resultado.length-restaIndex)){
                                out.print("string" + resultado[iColumnasOrdencacion.get(j)].toString());
                              }else{
                                out.print("string" + resultado[iColumnasOrdencacion.get(j)].toString() + ";");
                              } 
                          }else{
                            if(j == (resultado.length-restaIndex)){
                                out.print("");
                              }else{
                                out.print(";");
                              } 
                          }
                        }
                      }
                    }
                  out.print("\n");
                }
                
                beginIndex = beginIndex + numResult;
                
          }
                
         }while(resultados!= null && !resultados.isEmpty());
       }// autoclose buferedwriter
    
        
     
      
    }catch(Exception e){
      throw new SIBBACBusinessException(MessageFormat.format("Error en Query: {0} - {1}", query, e.getMessage()), e);
    }
    LOG.debug("Resultados de la query {}", resultados.size());    
    
    return numRows;
  }

}
