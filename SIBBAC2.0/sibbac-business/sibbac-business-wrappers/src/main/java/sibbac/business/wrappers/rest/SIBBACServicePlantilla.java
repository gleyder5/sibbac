package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.PlantillaBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServicePlantilla implements SIBBACServiceBean {

	private static final Logger	LOG							= LoggerFactory.getLogger( SIBBACServicePlantilla.class );

	@Autowired
	private PlantillaBo			plantillaBo;

	public static final String	CMD_CREATE_PLANTILLA		= "createPlantilla";
	public static final String	CMD_MODIFICACION_PLANTILLA	= "modificacionPlantilla";
	public static final String	CMD_GETLISTA_PLANTILLA		= "getListaPlantilla";

	public static final String	PLANTILLA_PARAM_ID			= "id";
	public static final String	PLANTILLA_PARAM_ALIAS_ID	= "aliasId";
	public static final String	PLANTILLA_PARAM_NOMBRE		= "nombre";
	public static final String	PLANTILLA_PARAM_DESCRIPCION	= "descripcion";

	public SIBBACServicePlantilla() {
		LOG.trace( "[ServicePlantilla::<constructor>] Initiating SIBBAC Service for \"Plantilla\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {

		LOG.trace( "[ServiceCobros::process] Processing a web request..." );
		// prepara webResponse
		WebResponse webResponse = new WebResponse();
		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();
		// obtiene comandos(paths)
		LOG.trace( "[ServicePlantilla::process] Command.: [{}]", webRequest.getAction() );

		// selecciona la accion
		switch ( webRequest.getAction() ) {
			case CMD_CREATE_PLANTILLA:
				webResponse.setResultados( plantillaBo.createPlantilla( params ) );
				break;

			case CMD_GETLISTA_PLANTILLA:
				webResponse.setResultados( plantillaBo.getListaPlantilla() );
				break;

			case CMD_MODIFICACION_PLANTILLA:
				webResponse.setResultados( plantillaBo.modificacionPlantilla( params ) );
				break;

			default:
				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;
		}

		return webResponse;

	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_CREATE_PLANTILLA );
		commands.add( CMD_MODIFICACION_PLANTILLA );
		commands.add( CMD_GETLISTA_PLANTILLA );

		return commands;
	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		return fields;
	}

}
