package sibbac.business.wrappers.database.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.AnotacioneccData;

@Repository
public class Tmct0anotacioneccDaoImpl {

  @PersistenceContext
  private EntityManager em;
  private static final String TAG = Tmct0anotacioneccDaoImpl.class.getName();
  private static final Logger LOG = LoggerFactory.getLogger(TAG);

  /**
   * Obtiene una lista de objetos anotacioneccData sin paginar
   *
   * @param tipoCuentaPropia
   * @param idTipoCuentaIndividual
   * @param filtros
   * @return
   */
  public List<AnotacioneccData> findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentido(long tipoCuentaPropia,
      long idTipoCuentaIndividual, Map<String, Serializable> filtros) {
    return findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentidoWithPagination(tipoCuentaPropia,
        idTipoCuentaIndividual, filtros, -1, -1);
  }

  /**
   * Obtiene una lista de objetos AnotacioneccData paginados
   *
   * @param tipoCuentaPropia
   * @param idTipoCuentaIndividual
   * @param filtros
   * @param startPosition
   * @param maxResult
   * @return
   */
  @SuppressWarnings("unchecked")
  public List<AnotacioneccData> findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentidoWithPagination(
      long tipoCuentaPropia, long idTipoCuentaIndividual, Map<String, Serializable> filtros, int startPosition,
      int maxResult) {
    String prefix = "[" + TAG + "::findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentidoWithPagination] ";
    List<AnotacioneccData> resultList = new LinkedList<>();
    String querystring = "SELECT A.cdisin, C.CD_CODIGO, A.cdsentido, A.imprecio, sum(A.imtitulosdisponibles) as titulos, sum(A.imefectivodisponible) as efectivo "
        + " FROM TMCT0ANOTACIONECC A INNER JOIN TMCT0_CUENTAS_DE_COMPENSACION C ON A.IDCUENTACOMP = C.ID_CUENTA_COMPENSACION "
        + " WHERE C.ID_TIPO_CUENTA in (:tipoCuentaPropia,:tipoCuentaIndividual) ";
    String groupBy = " group by a.cdisin, a.cdsentido, a.imprecio, C.CD_CODIGO ";
    String orderBy = " ";
    String fcontratacionBtw = " AND A.fcontratacion BETWEEN :fcontratacionDe AND :fcontratacionA";
    String fliquidacionBtw = " AND A.fliqteorica BETWEEN :fliquidacionDe AND :fliquidacionA";
    LOG.debug(prefix + "Entrando en el metodo filtros:  " + filtros);
    StringBuilder sb = new StringBuilder(querystring);
    /*
     * para depurar try{ List<Object[]> lista = new LinkedList<>(); Query
     * queryTest = em.createQuery(testQuery); queryTest.setFirstResult(0);
     * queryTest.setMaxResults(100); lista = queryTest.getResultList();
     * LOG.debug("Test Registro extraidos: "+lista.size()); }catch(Exception
     * ex){ // LOG.error(ex.getMessage(), ex); }
     */

    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      if (entry.getValue() != null) {
        if (!"".equals(entry.getKey())
            && (!entry.getKey().equals("fcontratacionDe") && !entry.getKey().equals("fcontratacionA")
                && !entry.getKey().equals("fliquidacionDe") && !entry.getKey().equals("fliquidacionA") && !entry
                .getKey().trim().equals("idCuentaDeCompensacion"))) {
          sb.append(" AND A.").append(entry.getKey()).append("=:").append(entry.getKey());
        }
        else if (entry.getKey().equals("fcontratacionDe")) {
          // si vienen las dos fechas se hace un between
          if (checkIfKeyExist("fcontratacionA", filtros)) {
            sb.append(fcontratacionBtw);
          }
          else {
            sb.append(" AND A.fcontratacion >=:fcontratacionDe");
          }
        }
        else if (entry.getKey().equals("fcontratacionA")) {
          // si vienen las dos fechas se hace un between
          if (!checkIfKeyExist("fcontratacionDe", filtros)) {
            sb.append(" AND a.fcontratacion <=:fcontratacionA");
          }
        }
        else if (entry.getKey().equals("fliquidacionDe")) {
          // si vienen las dos fechas se hace un between
          if (checkIfKeyExist("fliquidacionA", filtros)) {
            sb.append(fliquidacionBtw);
          }
          else {
            sb.append(" AND A.fliqteorica >=:fliquidacionDe");
          }
        }
        else if (entry.getKey().equals("fliquidacionA")) {
          // si vienen las dos fechas se hace un between
          if (!checkIfKeyExist("fliquidacionDe", filtros)) {
            sb.append(" AND A.fliqteorica <=:fliquidacionA");
          }
        }
        else if (entry.getKey().equals("idCuentaDeCompensacion")) {
          sb.append(" AND A.IDCUENTACOMP").append("=:").append(entry.getKey());
        }
      }
    }

    sb.append(groupBy).append(orderBy);
    LOG.debug(prefix + "Query String:  " + sb.toString());
    Query query = null;
    try {
      query = em.createNativeQuery(sb.toString());
    }
    catch (PersistenceException ex) {
      LOG.error(prefix + "ERROR(): {}", ex.getClass().getName(), ex.getMessage());
      return resultList;
    }

    LOG.debug(prefix + "Agregando datos a los filtros...");
    String key = null;
    Object value = null;
    Date fecha = null;
    java.sql.Date sqlFecha = null;
    for (Map.Entry<String, Serializable> entry : filtros.entrySet()) {
      key = entry.getKey();
      value = entry.getValue();
      if (key.startsWith("f")) {
        // es una fecha...
        fecha = (Date) value;
        sqlFecha = new java.sql.Date(fecha.getTime());
        LOG.debug(prefix + "> [ FECHA] [{}=={} ({})]", key, sqlFecha, sqlFecha.getClass().getName());
        query.setParameter(key, value);
      }
      else {
        LOG.debug(prefix + "> [NORMAL] [{}=={} ({})]", key, value, value.getClass().getName());
        query.setParameter(key, value);
      }
    }

    LOG.debug(prefix + "Query creada y parametros seteados query: " + query);
    LOG.debug(prefix + "tipoCuentaPropia: " + tipoCuentaPropia);
    LOG.debug(prefix + "idTipoCuentaIndividual: " + idTipoCuentaIndividual);

    query.setParameter("tipoCuentaPropia", tipoCuentaPropia);
    query.setParameter("tipoCuentaIndividual", idTipoCuentaIndividual);
    if (startPosition >= 0 && maxResult > 0) {
      query.setFirstResult(startPosition);
      query.setMaxResults(maxResult);
    }
    try {
      LOG.debug(prefix + "Inicio consulta {}...", sb.toString());
      List<Object[]> listaResultados = query.getResultList();
      if (listaResultados != null && !listaResultados.isEmpty()) {
        for (Object[] o : listaResultados) {
          if (((BigDecimal) o[3]).compareTo(BigDecimal.ZERO) != 0
              || ((BigDecimal) o[5]).compareTo(BigDecimal.ZERO) != 0) {
            resultList.add(new AnotacioneccData((String) o[0], (String) o[1], (char) o[2], (BigDecimal) o[3],
                (BigDecimal) o[4], (BigDecimal) o[5]));
          }
        }
        listaResultados.clear();
      }

      LOG.debug(prefix + "Fin consulta...");
    }
    catch (PersistenceException ex) {
      LOG.error(prefix + "ERROR(): {}", ex.getClass().getName(), ex.getMessage());
    }
    return resultList;
  }

  private boolean checkIfKeyExist(String key, Map<String, Serializable> params) {
    boolean exist = false;
    if (params.get(key) != null) {
      exist = true;
    }
    return exist;
  }

}
