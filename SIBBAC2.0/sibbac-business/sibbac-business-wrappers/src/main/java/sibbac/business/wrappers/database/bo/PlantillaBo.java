package sibbac.business.wrappers.database.bo;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.PlantillaDao;
import sibbac.business.wrappers.database.dto.PlantillaDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Plantilla;
import sibbac.database.bo.AbstractBo;


@Service
public class PlantillaBo extends AbstractBo< Plantilla, Long, PlantillaDao > {

	@Autowired
	AliasBo	aliasBo;

	public Map< String, PlantillaDTO > getListaPlantilla() {

		LOG.trace( "[PlantillaBo] start getListaPlantilla..." );
		Map< String, PlantillaDTO > ListadoPlantillas = new HashMap< String, PlantillaDTO >();
		List< Plantilla > listaPlantilla = ( List< Plantilla > ) this.findAll();

		for ( Plantilla plantilla : listaPlantilla ) {
			PlantillaDTO plantillaDTO = this.plantillaToDTO( plantilla );
			ListadoPlantillas.put( plantilla.getId().toString(), plantillaDTO );
		}
		LOG.trace( "[PlantillaBo] end getListaPlantilla..." );

		return ListadoPlantillas;
	}

	public PlantillaDTO plantillaToDTO( Plantilla plantilla ) {

		PlantillaDTO plantillaDTO = null;
		LOG.trace( "[PlantillaBo] start plantillaToDTO..." );

		if ( plantilla != null ) {
			plantillaDTO = new PlantillaDTO( plantilla.getId(), plantilla.getAlias().getId(), plantilla.getNombre(),
					plantilla.getDescripcion() );
		}
		LOG.trace( "[PlantillaBo] end plantillaToDTO..." );

		return plantillaDTO;

	}

	public Map< String, PlantillaDTO > createPlantilla( Map< String, String > params ) {

		LOG.trace( "[PlantillaBo::createPlantilla] start..." );
		Alias alias = null;
		Map< String, PlantillaDTO > ListadoPlantillas = new HashMap< String, PlantillaDTO >();

		if ( params != null ) {

			Plantilla plantilla = new Plantilla();
			try {
				alias = aliasBo.findById( Long.valueOf( params.get( Constantes.PLANTILLA_PARAM_ALIAS_ID ) ) );
				LOG.trace( "[PlantillaBo::createPlantilla] alias obtenido createPlantilla..." );
			} catch ( RuntimeException e ) {
				LOG.error( e.getMessage() );
				LOG.trace( "[PlantillaBo::createPlantilla] error al buscar un alias..." + e.getMessage() );
			}

			if ( alias != null ) {
				plantilla.setAlias( alias );
				plantilla.setNombre( params.get( Constantes.PLANTILLA_PARAM_NOMBRE ) );
				plantilla.setDescripcion( params.get( Constantes.PLANTILLA_PARAM_DESCRIPCION ) );

				try {
					this.save( plantilla );
					LOG.trace( "[PlantillaBo::createPlantilla] salvando..." );
				} catch ( RuntimeException e ) {
					LOG.error( e.getMessage() );
					LOG.trace( "[PlantillaBo::createPlantilla] error al guardar..." + e.getMessage() );
				}

				PlantillaDTO plantillaDTO = new PlantillaDTO( plantilla.getId(), plantilla.getAlias().getId(), plantilla.getNombre(),
						plantilla.getDescripcion() );
				ListadoPlantillas.put( plantilla.getId().toString(), plantillaDTO );
				LOG.trace( "[PlantillaBo::createPlantilla] end..." );
			}
		}

		return ListadoPlantillas;

	}

	public Map< String, PlantillaDTO > modificacionPlantilla( Map< String, String > params ) {

		Map< String, PlantillaDTO > ListadoPlantillas = new HashMap< String, PlantillaDTO >();
		Alias alias = null;

		if ( params != null ) {

			Plantilla plantilla = null;
			try {
				plantilla = this.findById( Long.valueOf( params.get( Constantes.PLANTILLA_PARAM_ID ) ) );
				alias = aliasBo.findById( Long.valueOf( params.get( Constantes.PLANTILLA_PARAM_ALIAS_ID ) ) );
			} catch ( RuntimeException e ) {
				LOG.error( e.getMessage() );
			}

			if ( plantilla != null && alias != null ) {

				plantilla.setAlias( alias );
				plantilla.setNombre( params.get( Constantes.PLANTILLA_PARAM_NOMBRE ) );
				plantilla.setDescripcion( params.get( Constantes.PLANTILLA_PARAM_DESCRIPCION ) );

				try {
					this.save( plantilla );
				} catch ( RuntimeException e ) {
					LOG.error( e.getMessage() );
				}

				PlantillaDTO plantillaDTO = new PlantillaDTO( plantilla.getId(), plantilla.getAlias().getId(), plantilla.getNombre(),
						plantilla.getDescripcion() );
				ListadoPlantillas.put( plantilla.getId().toString(), plantillaDTO );
			}
		}

		return ListadoPlantillas;

	}

}
