package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Identificadores;


@Component
public interface IdentificadoresDao extends JpaRepository< Identificadores, Long > {

}
