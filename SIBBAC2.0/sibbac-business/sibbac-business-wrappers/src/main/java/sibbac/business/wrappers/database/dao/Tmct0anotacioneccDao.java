package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.AnotacioneccData;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;

@Repository
public interface Tmct0anotacioneccDao extends JpaRepository<Tmct0anotacionecc, Long>,
    JpaSpecificationExecutor<Tmct0anotacionecc> {

  Tmct0anotacionecc findByCdoperacionecc(String cdoperacionecc);

  /**
   * 
   * @param tipoCuenta
   * @return
   */
  @Query("SELECT a FROM Tmct0anotacionecc a WHERE a.nuoperacionprev = :nuoperacionprev ORDER BY a.imtitulosdisponibles ASC")
  List<Tmct0anotacionecc> findByNuoperacionprev(@Param("nuoperacionprev") String nuoperacionprev);

  @Query("SELECT a FROM Tmct0anotacionecc a WHERE a.nuoperacioninic = :nuoperacioninic and a.cdsentido = :sentido and a.cdoperacionecc != a.nuoperacionprev ")
  List<Tmct0anotacionecc> findByNuoperacioninicAndSentidoAndNotNovacion(
      @Param("nuoperacioninic") String nuoperacioninic, @Param("sentido") char sentido);

  List<Tmct0anotacionecc> findByNuoperacioninic(String nuoperacioninic);

  @Query("SELECT a FROM Tmct0anotacionecc a WHERE a.nuoperacioninic = :nuoperacioninic and a.cdsentido = :sentido and a.imtitulosdisponibles > 0 ")
  List<Tmct0anotacionecc> findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(
      @Param("nuoperacioninic") String nuoperacioninic, @Param("sentido") char sentido);

  @Query("SELECT a FROM Tmct0anotacionecc a WHERE a.nuoperacioninic = :nuoperacioninic and a.cdsentido = :sentido and a.imtitulosdisponibles > 0 and a.nutitulos >= :nutitulos "
      + "order by a.idanotacionecc desc")
  List<Tmct0anotacionecc> findAllByNuoperacioninicOrderDescendenteAndTitulosDisponibles(
      @Param("nuoperacioninic") String nuoperacioninic, @Param("sentido") char sentido,
      @Param("nutitulos") BigDecimal nutitulos);

  @Query("SELECT a FROM Tmct0anotacionecc a WHERE a.nuoperacioninic = :nuoperacioninic and a.cdsentido = :sentido and a.nutitulos >= :nutitulos "
      + "order by a.idanotacionecc desc")
  List<Tmct0anotacionecc> findAllByNuoperacioninicOrderDescendente(@Param("nuoperacioninic") String nuoperacioninic,
      @Param("sentido") char sentido, @Param("nutitulos") BigDecimal nutitulos);

  /**
   * 
   * @param fcontratacion
   * @param isin
   * @param tmct0CuentasDeCompensacion
   * @return
   */
  List<Tmct0anotacionecc> findByFcontratacionAndCdisinAndTmct0CuentasDeCompensacion(Date fcontratacion, String isin,
      Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion);

  /**
   * 
   * @param fcontratacion
   * @param isin
   * @param tmct0CuentasDeCompensacion
   * @param movimientoecc
   * @return
   */
  Tmct0anotacionecc findByFcontratacionAndCdisinAndTmct0CuentasDeCompensacionAndTmct0movimientoecc(Date fcontratacion,
      String isin, Tmct0CuentasDeCompensacion tmct0CuentasDeCompensacion, Tmct0movimientoecc movimientoecc);

  /**
   * 
   * @param tmct0movimientoecc
   * @return
   */
  List<Tmct0anotacionecc> findByTmct0movimientoecc(Tmct0movimientoecc tmct0movimientoecc);

  /**
   * 
   * @param tipoCuenta
   * @return
   */
  @Query("SELECT a FROM Tmct0anotacionecc a WHERE a.tmct0CuentasDeCompensacion.tmct0TipoCuentaDeCompensacion.idTipoCuenta =:tipoCuenta")
  List<Tmct0anotacionecc> findByTipoCuenta(@Param(value = "tipoCuenta") long tipoCuenta);

  /**
   * 
   * @param
   * @return
   */
  @Query("SELECT new sibbac.business.wrappers.database.data.AnotacioneccData"
      + "(a.cdcamara, a.cdisin, a.tmct0CuentasDeCompensacion.cdCodigo, a.fcontratacion, a.fregistroecc, a.cdsentido, a.nutitulos, a.imefectivo) "
      + "FROM Tmct0anotacionecc a WHERE a.tmct0CuentasDeCompensacion.cdCodigo='00D'")
  List<AnotacioneccData> findAnotacionesCuenta00DForDiaria();

  /**
   * Prueba jony
   */
  @Query(value = "SELECT T.CDOPERACIONECC, T.CDSENTIDO, T.FCONTRATACION, T.CDCAMARA, T.CDSEGMENTO, T.NUEXEMKT, T.NUORDENMKT, T.CDMIEMBROMKT, T.CDOPERACIONMKT, T.CDISIN, SUM(T.NUTITULOS) "
      + "FROM (SELECT A.CDOPERACIONECC, A.CDSENTIDO, A.FCONTRATACION, A.CDCAMARA, A.CDSEGMENTO, A.NUEXEMKT, A.NUORDENMKT, A.CDMIEMBROMKT, A.CDOPERACIONMKT, A.CDISIN, A.NUTITULOS "
      + "FROM TMCT0ANOTACIONECC A WHERE A.IDCUENTACOMP = 225 AND A.FCONTRATACION = :fcontratacion UNION SELECT A.CDOPERACIONECC, A.CDSENTIDO, A.FCONTRATACION, A.CDCAMARA, A.CDSEGMENTO, A.NUEXEMKT, A.NUORDENMKT, A.CDMIEMBROMKT, A.CDOPERACIONMKT, A.CDISIN, -A.NUTITULOS "
      + "FROM TMCT0ANOTACIONECC A WHERE (A.IDCUENTACOMP <> 225 OR A.IDCUENTACOMP IS NULL) AND A.FCONTRATACION = :fcontratacion "
      + ") AS T GROUP BY T.CDOPERACIONECC, T.CDSENTIDO, T.FCONTRATACION, T.CDCAMARA, T.CDSEGMENTO, T.NUEXEMKT, T.NUORDENMKT, T.CDMIEMBROMKT, T.CDOPERACIONMKT, T.CDISIN "
      + "HAVING SUM(T.NUTITULOS) > 0 ORDER BY T.CDOPERACIONECC, T.CDSENTIDO, T.FCONTRATACION, T.CDCAMARA, T.CDSEGMENTO, T.NUEXEMKT, T.NUORDENMKT, T.CDMIEMBROMKT, T.CDOPERACIONMKT, T.CDISIN  ", nativeQuery = true)
  List<Object[]> findAnotacionesCuenta00DForDiaria(@Param(value = "fcontratacion") Date fcontratacion);

  /**
   * 
   * @param tipoCuenta
   * @return
   */
  @Query("SELECT a, sum(a.nutitulos) as titulos, sum(a.imefectivo) as efectivos FROM Tmct0anotacionecc a WHERE a.tmct0CuentasDeCompensacion.tmct0TipoCuentaDeCompensacion.idTipoCuenta =:tipoCuenta group by a.cdisin, a.tmct0CuentasDeCompensacion.tmct0TipoCuentaDeCompensacion.idTipoCuenta, a.imprecio, a.fcontratacion, a.cdsentido")
  List<Object[]> findByTipoCuentaGroupByIsinAndCuentaAndTradeDateAndSentido(@Param(value = "tipoCuenta") long tipoCuenta);

  @Query("SELECT o.cdoperacionecc FROM Tmct0anotacionecc o, Tmct0CuentasDeCompensacion c, Tmct0TipoCuentaDeCompensacion tc "
      + "  WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND c.tmct0TipoCuentaDeCompensacion.idTipoCuenta = tc.idTipoCuenta "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND tc.cdCodigo = 'CD' "
      + " AND o.cdcamara = :camara and o.envios3 != 'S' ")
  List<String> findAllCdoperacioneccEnvioS3CuentaDiariaAndCamara(@Param("fecontratacion") Date fecontratacion,
      @Param("camara") String camara);

  @Query("SELECT o.cdoperacionecc FROM Tmct0anotacionecc o, Tmct0CuentasDeCompensacion c, Tmct0TipoCuentaDeCompensacion tc "
      + "  WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND c.tmct0TipoCuentaDeCompensacion.idTipoCuenta = tc.idTipoCuenta "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND tc.cdCodigo = 'CD' "
      + " AND o.cdsegmento not in (:segmentos) AND o.cdcamara = :camara and o.envios3 != 'S' ")
  List<String> findAllCdoperacioneccEnvioS3CuentaDiariaAndNotInSegmentosAndCamara(@Param("fecontratacion") Date fecontratacion,
      @Param("segmentos") List<String> segmentosNotIn,@Param("camara") String camara);
  
  @Query("SELECT o.cdoperacionecc FROM Tmct0anotacionecc o, Tmct0CuentasDeCompensacion c"
      + " WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND c.cdCodigo in (:ctas) "
      + " AND o.cdcamara = :camara and o.envios3 != 'S' ")
  List<String> findAllCdoperacioneccEnvioS3ByListCuentasAndCamara(@Param("fecontratacion") Date fecontratacion,
      @Param("ctas") List<String> ctas, @Param("camara") String camara);

  @Query("SELECT o.cdoperacionecc FROM Tmct0anotacionecc o, Tmct0CuentasDeCompensacion c"
      + " WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND c.cdCodigo in (:ctas) "
      + " AND o.cdsegmento not in (:segmentos) AND o.cdcamara = :camara and o.envios3 != 'S' ")
  List<String> findAllCdoperacioneccEnvioS3ByListCuentasAndNotInSegmentosAndCamara(@Param("fecontratacion") Date fecontratacion,
      @Param("ctas") List<String> ctas, @Param("segmentos") List<String> segmentosNotIn, @Param("camara") String camara);

  @Query("SELECT o.cdoperacionecc FROM Tmct0anotacionecc o, Tmct0CuentasDeCompensacion c, Tmct0TipoCuentaDeCompensacion tc "
      + "  WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND c.tmct0TipoCuentaDeCompensacion.idTipoCuenta = tc.idTipoCuenta "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND tc.cdCodigo = 'CD' "
      + " AND o.cdsegmento in (:segmentos) AND o.cdcamara = :camara and o.envios3 != 'S' ")
  List<String> findAllCdoperacioneccEnvioS3CuentaDiariaAndInSegmentosAndCamara(@Param("fecontratacion") Date fecontratacion,
      @Param("segmentos") List<String> segmentosIn, @Param("camara") String camara);

  @Query("SELECT o.cdoperacionecc FROM Tmct0anotacionecc o, Tmct0CuentasDeCompensacion c"
      + " WHERE o.tmct0CuentasDeCompensacion.idCuentaCompensacion = c.idCuentaCompensacion "
      + " AND o.fcontratacion = :fecontratacion AND o.imtitulosdisponibles > 0 AND c.cdCodigo in (:ctas) "
      + " AND o.cdsegmento in (:segmentos) AND o.cdcamara = :camara and o.envios3 != 'S' ")
  List<String> findAllCdoperacioneccEnvioS3ByListCuentasAndInSegmentosAndCamara(@Param("fecontratacion") Date fecontratacion,
      @Param("ctas") List<String> ctas, @Param("segmentos") List<String> segmentosIn, @Param("camara") String camara);
}
