package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0bokDao;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.SibbacEnums.TypeCdestado;
import sibbac.common.utils.SibbacEnums.TypeCdtipest;
import sibbac.database.bo.AbstractBo;

/**
 * @author xIS16630
 *
 */
@Service
public class Tmct0bokBo extends AbstractBo<Tmct0bok, Tmct0bokId, Tmct0bokDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0bokBo.class);

  @Autowired
  private Tmct0aloBo aloBo;

  @Autowired
  private Tmct0ejeBo ejeBo;

  @Autowired
  private Tmct0ctgBo ctgBo;

  @Autowired
  private Tmct0grupoejecucionBo grBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejeAloBo;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0desgloseCamaraBo dcBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0DesgloseCamaraEnvioRtDao rtDao;

  public Character findCasepti(Tmct0bokId bookingId) {
    return dao.findCasepti(bookingId);
  }

  /**
   * tmct0bok.cdusublo=auditUser, este metodo tiene el problema de si necesitas
   * tocar un booking la transaccion no te lo va a permitir por ello lo mas
   * inteligente seria realizar el bloqueo de usuario con una tabla nueva,
   * podria ser tmct0bok_lock, la pk de esta tabla sera la misma que el booking
   * mas los datos de auditoria y usuario que esta bloqueando, de tal modo que
   * el bloqueo sera insertar en esa tabla de manera atomica y el desbloqueo
   * sera borrar el registro de la tabla
   * 
   * @param bookId
   * @param auditUser
   * @throws LockUserException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void bloquearBooking(Tmct0bokId bookId, String auditUser) throws LockUserException {
    LOG.trace("[bloquearBooking] inicio");
    Date fhaudit = new Date();

    String cdusublo = dao.findCdusubloByBookingId(bookId);
    int updateRegister = dao.bloquearBooking(bookId, auditUser, fhaudit);

    if (updateRegister <= 0) {
      fhaudit = dao.findFhauditByBookingId(bookId);
      String msg = String.format("Booking %s/%s bloqueado por otro usuario %s desde %s", bookId.getNuorden(),
          bookId.getNbooking(), cdusublo, fhaudit);
      LOG.warn("[bloquearBooking] {}", msg);
      throw new LockUserException(msg);
    }

    LOG.trace("[bloquearBooking] booking {} bloqueado por usuario: {} hora: {}", bookId, auditUser, fhaudit);
    LOG.trace("[bloquearBooking] fin");
  }

  /**
   * tmct0bok.cdusublo=auditUser, este metodo tiene el problema de si necesitas
   * tocar un booking la transaccion no te lo va a permitir por ello lo mas
   * inteligente seria realizar el bloqueo de usuario con una tabla nueva,
   * podria ser tmct0bok_lock, la pk de esta tabla sera la misma que el booking
   * mas los datos de auditoria y usuario que esta bloqueando, de tal modo que
   * el bloqueo sera insertar en esa tabla de manera atomica y el desbloqueo
   * sera borrar el registro de la tabla
   * 
   * @param bookId
   * @param auditUser
   * @throws LockUserException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void bloquearBooking(Tmct0bokId bookId, int cdestadoAsig, String auditUser) throws LockUserException {
    this.bloquearBooking(bookId, auditUser);
    dao.updateCdestadoasigByTmct0bokId(bookId, cdestadoAsig, new Date());
  }

  /**
   * tmct0bok.cdusublo=auditUser, este metodo tiene el problema de si necesitas
   * tocar un booking la transaccion no te lo va a permitir por ello lo mas
   * inteligente seria realizar el bloqueo de usuario con una tabla nueva,
   * podria ser tmct0bok_lock, la pk de esta tabla sera la misma que el booking
   * mas los datos de auditoria y usuario que esta bloqueando, de tal modo que
   * el bloqueo sera insertar en esa tabla de manera atomica y el desbloqueo
   * sera borrar el registro de la tabla
   * 
   * @param bookId
   * @param auditUser
   * @throws LockUserException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void bloquearBooking(Tmct0bokId bookId, int cdestadoAsig, int cdestadotit, String auditUser)
      throws LockUserException {
    this.bloquearBooking(bookId, cdestadoAsig, auditUser);
    dao.updateCdestadotitByTmct0bokId(bookId, cdestadotit, new Date(), auditUser);
    aloBo.updateCdestadotitByTmct0bokId(bookId, EstadosEnumerados.TITULARIDADES.EN_CURSO.getId(), auditUser);
  }

  // TODO LOS METODOS DESBLOQUEAR PUEDEN DAR PROBLEMAS, HAY QUE HACERLO EN UNA
  // SOLA QUERY
  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void desBloquearBooking(Tmct0bokId bookId, String auditUser) throws LockUserException {
    LOG.trace("[desBloquearBooking] inicio");
    if (dao.desBloquearBooking(bookId, auditUser, new Date()) <= 0) {
      String msg = String.format("Booking %s/%s no se ha podido desbloquear por usuario %s", bookId.getNuorden(),
          bookId.getNbooking(), auditUser);
      LOG.warn("[bloquearBooking] {}", msg);
      throw new LockUserException(msg);
    }
    LOG.trace("[desBloquearBooking] final");
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void desBloquearBooking(Tmct0bokId bookId, int cdestadoasig, String auditUser) throws LockUserException {
    dao.updateCdestadoasigByTmct0bokId(bookId, cdestadoasig, new Date());
    this.desBloquearBooking(bookId, auditUser);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void desBloquearBooking(Tmct0bokId bookId, int cdestadoasig, int cdestadotit, String auditUser)
      throws LockUserException {
    dao.updateCdestadotitByTmct0bokId(bookId, cdestadotit, new Date(), auditUser);
    this.desBloquearBooking(bookId, cdestadoasig, auditUser);
  }

  public List<Tmct0bok> findForFicheroSPB(List<String> cdcuenta, Date fechaDesde, Date fechaHasta,
      List<Integer> workDaysOfWeekConfigList, List<Date> holidaysConfigList) {
    List<Tmct0bok> bookings = new ArrayList<Tmct0bok>();
    Date fechaBucle = new Date(fechaDesde.getTime());
    do {
      bookings.addAll(dao.findAllByFecontraAndInCdcuenta(cdcuenta, fechaBucle));
      fechaBucle = DateHelper.getNextWorkDate(fechaBucle, 1, workDaysOfWeekConfigList, holidaysConfigList);
    }
    while (fechaBucle.compareTo(fechaHasta) <= 0);
    return bookings;
  }

  public List<Tmct0bok> findByNbooking(String nbooking) {
    return dao.findByNbooking(nbooking);
  }

  /**
   * 
   * Actualiza el booking a casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   */
  public Integer updateFlagCasadoPti(String nuorden, String nbooking) {
    return dao.updateFlagCasadoPti(nuorden, nbooking, new Date());
  }

  public Integer updateEnviadoConfirmacionMinorista1617(List<Tmct0bokId> bookings) {
    int count = 0;
    for (Tmct0bokId tmct0bokId : bookings) {
      count += dao.updateEnviadoConfirmacionMinorista1617(tmct0bokId, EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId(),
          new Date(), "CONF_MINO");
    }
    return count;

  }

  public Integer updateCdestadoasigByTmct0bokId(Tmct0bokId bokId, int cdestadoasigTo) {
    return dao.updateCdestadoasigByTmct0bokId(bokId, cdestadoasigTo, new Date());
  }

  public BigDecimal findNutitejeById(Tmct0bokId bokId) {
    return dao.findNutitejeById(bokId);
  }

  public List<Tmct0bokId> findAllConfirmacionMinorista(List<String> notInAliass, Date sincedate) {
    return dao.findAllConfirmacionMinorista(notInAliass, EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
        sincedate);
  }

  public boolean booksValidosEnvioS3(String nuorden) {
    final List<Object[]> incompletos;

    LOG.debug("[booksValidosEnvioS3] Inicio...");
    incompletos = dao.booksIncompletos(nuorden, ASIGNACIONES.PDTE_LIQUIDAR.getId(), CLEARING.PDTE_FICHERO.getId(),
        CLEARING.RECHAZADO_FICHERO.getId(), CLEARING.INCIDENCIA.getId());
    if (incompletos.isEmpty()) {
      LOG.debug("[booksValidosEnvioS3] Los bookings de la orden {} estan completos.", nuorden);
      return true;
    }
    for (Object[] objs : incompletos) {
      LOG.warn("[booksValidosEnvioS3] El número de titulos a enviar ({}) del booking {} de la orden {} difiere de los "
          + "titulos ejecutados ({})", objs[2], objs[0], nuorden, objs[1]);
    }
    return false;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void updateCdestadoInternacional(Tmct0bok tmct0bok, TypeCdestado cdestado, String cdusuaud) {
    LOG.debug("[Tmct0bokBo :: updateCdestadoInternacional] update estado a {}.", cdestado);
    // Tmct0sta staBook = staBo.findById(new Tmct0staId(cdestado.getValue(),
    // TypeCdtipest.TMCT0BOK.getValue()));
    // tmct0bok.setTmct0sta(staBook);
    tmct0bok.setTmct0sta(new Tmct0sta(new Tmct0staId(cdestado.getValue(), TypeCdtipest.TMCT0BOK.getValue())));
    tmct0bok.setFhaudit(new Date());
    tmct0bok.setCdusuaud(cdusuaud);
    tmct0bok = save(tmct0bok);
    List<Tmct0alo> alos = aloBo.findAllByNuordenAndNbookingActivosInternacional(tmct0bok.getId().getNuorden(), tmct0bok
        .getId().getNbooking());
    for (Tmct0alo tmct0alo : alos) {
      LOG.debug("[Tmct0bokBo :: updateCdestadoInternacional] update estado a {}-{}.", cdestado, tmct0alo.getId());
      tmct0alo.setTmct0sta(new Tmct0sta(new Tmct0staId(cdestado.getValue(), TypeCdtipest.TMCT0ALO.getValue())));
      tmct0alo.setFhaudit(new Date());
      tmct0alo.setCdusuaud(cdusuaud);
      if (cdestado.getValue().equals(TypeCdestado.FOREING_WAIT_CONFIRM_BROKER.getValue())) {
        LOG.debug("[Tmct0bokBo :: updateCdestadoInternacional] Estado FOREING_WAIT_CONFIRM_BROKER limpiamos nuagrpkb y pkentity.");
        tmct0alo.setNuagrpkb(0);
        tmct0alo.setPkentity("");
      }
      tmct0alo = aloBo.save(tmct0alo);

      aloBo.updateCentersInternacionalTmct0alo(tmct0alo, cdestado, cdusuaud);
    }

  }
  

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void deleteTmct0bokNewTransaction(Tmct0bokId tmct0bokId) {
    Tmct0bok tmct0bok = findById(tmct0bokId);
    LOG.debug("Deleted booking: {}", tmct0bok.getId());
    int deletes = ctgBo.deleteByNuodenAndNbooking(tmct0bok.getId().getNuorden(), tmct0bok.getId().getNbooking());
    LOG.debug("Deleted ctgs: {}", deletes);

    List<Tmct0alo> alos = aloBo.findAllByNuordenAndNbooking(tmct0bok.getId().getNuorden(), tmct0bok.getId()
        .getNbooking());
    // hay que borrar tb los ctgs

    List<Tmct0grupoejecucion> grues;
    List<Tmct0ejecucionalocation> ejeAlos;
    List<Tmct0alc> alcs;
    List<Tmct0desglosecamara> dcs;
    List<Tmct0DesgloseCamaraEnvioRt> dcrts;

    List<Tmct0desgloseclitit> dcts;
    List<Tmct0movimientoecc> movs;
    List<Tmct0movimientooperacionnuevaecc> movns;
    if (CollectionUtils.isNotEmpty(alos)) {
      for (Tmct0alo tmct0alo : alos) {
        movs = movBo.findByTmct0aloId(tmct0alo.getId());
        if (CollectionUtils.isNotEmpty(movs)) {
          LOG.debug("[deleteTmct0bok] Borrando movs de disociacion...");
          for (Tmct0movimientoecc mov : movs) {

            movns = mov.getTmct0movimientooperacionnuevaeccs();
            if (CollectionUtils.isNotEmpty(movns)) {
              movNBo.delete(movns);
            }

          }
          movBo.delete(movs);
        }

        alcs = tmct0alo.getTmct0alcs();
        if (CollectionUtils.isNotEmpty(alcs)) {
          for (Tmct0alc tmct0alc : alcs) {
            LOG.debug("[deleteTmct0bok] Borrando alds...");
            // alds = tmct0alc.getFk_tmct0alc_1();
            // if (CollectionUtils.isNotEmpty(alds)) {
            // pm.getPersistenceManager().deletePersistentAll(alds);
            // }
            // LOG.debug("[deleteTmct0bok] Borrando ales...");
            // ales = tmct0alc.getFk_tmct0alc_2();
            // if (CollectionUtils.isNotEmpty(ales)) {
            // pm.getPersistenceManager().deletePersistentAll(ales);
            // }
            LOG.debug("[deleteTmct0bok] Borrando dcs...");
            dcs = tmct0alc.getTmct0desglosecamaras();
            if (CollectionUtils.isNotEmpty(dcs)) {
              for (Tmct0desglosecamara desgloseCamara : dcs) {

                LOG.debug("[deleteTmct0bok] Borrando rts...");
                dcrts = desgloseCamara.getTmct0DesgloseCamaraEnvioRt();
                if (CollectionUtils.isNotEmpty(dcrts)) {
                  rtDao.delete(dcrts);
                }
                movs = desgloseCamara.getTmct0movimientos();
                if (CollectionUtils.isNotEmpty(movs)) {
                  for (Tmct0movimientoecc mov : movs) {
                    LOG.debug("[deleteTmct0bok] Borrando movs nuev...");
                    movns = mov.getTmct0movimientooperacionnuevaeccs();
                    if (CollectionUtils.isNotEmpty(movns)) {
                      movNBo.delete(movns);
                    }

                  }
                  LOG.debug("[deleteTmct0bok] Borrando movs...");
                  movBo.delete(movs);
                }
                LOG.debug("[deleteTmct0bok] Borrando dcts...");
                dcts = desgloseCamara.getTmct0desgloseclitits();
                if (CollectionUtils.isNotEmpty(dcts)) {
                  dctBo.delete(dcts);
                }

              }// fin dcs
              LOG.debug("[deleteTmct0bok] Borrando dcs...");
              dcBo.delete(dcs);
            }// fin dcs
          }// fin for alcs
          LOG.debug("[deleteTmct0bok] Borrando alcs...");
          alcBo.delete(alcs);
        }// fin alcs

        grues = tmct0alo.getTmct0grupoejecucions();
        if (CollectionUtils.isNotEmpty(grues)) {
          for (Tmct0grupoejecucion ge : grues) {
            LOG.debug("[deleteTmct0bok] Borrando ejealos...");
            ejeAlos = ge.getTmct0ejecucionalocations();
            if (CollectionUtils.isNotEmpty(ejeAlos)) {
              ejeAloBo.delete(ejeAlos);
            }
          }
          LOG.debug("[deleteTmct0bok] Borrando grues...");
          grBo.delete(grues);
        }
      }// fin for alos
      LOG.debug("[deleteTmct0bok] Borrando alos...");
      aloBo.delete(alos);
    }// fin alos
    LOG.debug("[deleteTmct0bok] Borrando gres-gae-gal...");
    // Collection<Tmct0gre> gres = tmct0bok.getFk_tmct0bok_3();
    // if (CollectionUtils.isNotEmpty(gres)) {
    // pm.getPersistenceManager().deletePersistentAll(gres);
    // }
    LOG.debug("[deleteTmct0bok] Borrando ejes...");
    List<Tmct0eje> ejes = ejeBo.findAllByNbookingAndNuorden(tmct0bok.getId().getNbooking(), tmct0bok.getId()
        .getNuorden());
    if (CollectionUtils.isNotEmpty(ejes)) {
      ejeBo.delete(ejes);
    }
    LOG.debug("[deleteTmct0bok] Borrando bok...");
    delete(tmct0bok);
  }

  @Transactional
  public void validarCasePti(Tmct0bokId bookingId) throws SIBBACBusinessException {
    Character casePti = findCasepti(bookingId);
    if (casePti == null) {
      Tmct0bok bok = findById(bookingId);
      if (bok == null) {
        throw new SIBBACBusinessException("Booking no encontrado.");
      }
      else {
        throw new SIBBACBusinessException("Booking no esta casado, no se permite el asignacion/traspaso.");
      }
    }
    else if (casePti != 'S') {
      throw new SIBBACBusinessException("Booking no esta casado, no se permite el asignacion/traspaso.");
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED, rollbackFor = Exception.class)
  public void updateCdestadoasigDownAllNewTransaction(Tmct0bokId bookId, int cdestadoasig, String auditUser) {
    this.updateCdestadoasigDownAll(bookId, cdestadoasig, auditUser);
  }

  /**
   * 
   * @param bookId
   * @param cdestadoasig -> de momento sólo se usa para asignarle el estado 28 - PDTE_ACTUALIZAR_CODIGOS_OPERACION
   * @param auditUser
   */
  @Transactional
  public void updateCdestadoasigDownAll(Tmct0bokId bookId, int cdestadoasig, String auditUser) {
    Tmct0bok bok = findById(bookId);
    bok.setTmct0estadoByCdestadoasig(new Tmct0estado(cdestadoasig));
    bok.setCdusuaud(auditUser);
    bok.setFhaudit(new Date());
    List<Tmct0alc> listAlcs;
    List<Tmct0desglosecamara> listDcs;
    List<Tmct0alo> listAlos = bok.getTmct0alos();

    if (CollectionUtils.isNotEmpty(listAlos)) {

      for (Tmct0alo alo : listAlos) {
        if (alo.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
          alo.setTmct0estadoByCdestadoasig(new Tmct0estado(cdestadoasig));
          alo.setCdusuaud(auditUser);
          alo.setFhaudit(new Date());
          listAlcs = alo.getTmct0alcs();
          if (CollectionUtils.isNotEmpty(listAlcs)) {
            for (Tmct0alc alc : listAlcs) {
              if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
                alc.setCdestadoasig(cdestadoasig);
                alc.setCdusuaud(auditUser);
                alc.setFhaudit(new Date());
                listDcs = alc.getTmct0desglosecamaras();
                if (CollectionUtils.isNotEmpty(listDcs)) {
                  for (Tmct0desglosecamara dc : listDcs) {
                    if (dc.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA
                        .getId()) {
                      dc.setAuditFechaCambio(new Date());
                      dc.setAuditUser(auditUser);
                      dc.setTmct0estadoByCdestadoasig(new Tmct0estado(cdestadoasig));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  public boolean isBookingCompletamenteDesglosado(Tmct0bok bok) {
    BigDecimal titulosDesgloses = dctBo.sumTitulosByTmct0bokId(bok.getId());
    return titulosDesgloses != null && bok.getNutiteje().compareTo(titulosDesgloses) == 0;
  }

}
