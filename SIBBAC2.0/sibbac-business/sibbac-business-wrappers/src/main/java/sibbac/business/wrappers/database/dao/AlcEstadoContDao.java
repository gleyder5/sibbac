package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.wrappers.database.model.AlcEstadoCont;
import sibbac.business.wrappers.database.model.Tmct0alc;

@Repository
public interface AlcEstadoContDao extends JpaRepository<AlcEstadoCont, Long> {
  public AlcEstadoCont findByAlcAndTipoEstado(Tmct0alc alc, TIPO_ESTADO tipoestado);

  @Query(value = "select count(*) from tmct0_alc_estadocont cont where cont.nbooking = :nbooking and cont.nuorden = :nuorden and cont.nucnfclt = :nucnfclt and cont.nucnfliq = :nucnfliq and "
                 + "cont.tipoestado = :tipoestado and cont.estadocont = :estadocont",
         nativeQuery = true)
  public Long findCountByNBookinAndNuordenAndNucnfcltAndNuCnfliqAndTipoestadoAndEstadocont(@Param("nbooking") String nbooking,
                                                                                           @Param("nuorden") String nuorden,
                                                                                           @Param("nucnfliq") Short nucnfliq,
                                                                                           @Param("nucnfclt") String nucnfclt,
                                                                                           @Param("tipoestado") int tipoestado,
                                                                                           @Param("estadocont") int estadocont);

  @Query(value = "select tipoestado, estadocont from tmct0_alc_estadocont cont"
                 + " where cont.nbooking = :nbooking and cont.nuorden = :nuorden and cont.nucnfclt = :nucnfclt and cont.nucnfliq = :nucnfliq",
         nativeQuery = true)
  public List<Object[]> findByAlc2(@Param("nuorden") String nuorden,
                                   @Param("nbooking") String nbooking,
                                   @Param("nucnfclt") String nucnfclt,
                                   @Param("nucnfliq") Short nucnfliq);

}
