package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.common.utils.FormatDataUtils;

public class HBB4TitularFieldSetMapper implements FieldSetMapper<HBB4TitularRecordBean> {

  public final static String delimiter = ";";

  public enum HBB4TitularFields {
    REFEXTERNA("refExterna", 16),
    FECHACUADRE("fechaCuadre", 8),
    TIPOREGISTRO("tipoRegistro", 1),
    NUMEROTITULAR("numeroTitular", 2),
    NOMBRE("nombre", 60),
    APELLIDO1("apellido1", 40),
    APELLIDO2("apellido2", 40),
    DOCUMENTOCLIENTE("documentoCliente", 40),
    TITULARIDAD("titularidad", 1),
    CODIFDEPOSITANTE("codIfDepositante", 26),
    REFERENCIACENTRAL("referenciaCentral", 16),
    NUMEROOPERACION("numeroOperacion", 8),
    TIPOPERSONA("tipoPersona", 1),
    TIPODOCUMENTO("tipoDocumento", 1),
    PARTICIP("particip", 5),
    INDNAC("indnac", 1),
    NACIONALIDAD("nacionalidad", 3);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private HBB4TitularFields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      HBB4TitularFields[] fields = HBB4TitularFields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getHBB4Lengths() {
      HBB4TitularFields[] fields = HBB4TitularFields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public HBB4TitularRecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    HBB4TitularRecordBean hbb4bean = new HBB4TitularRecordBean();
    hbb4bean.setRefExterna(fieldSet.readString(HBB4TitularFields.REFEXTERNA.text).trim());
    hbb4bean.setFechaCuadre(fieldSet.readDate(HBB4TitularFields.FECHACUADRE.text, FormatDataUtils.DATE_FORMAT));
    hbb4bean.setTipoRegistroHBB(fieldSet.readBigDecimal(HBB4TitularFields.TIPOREGISTRO.text));
    hbb4bean.setNumeroTitular(fieldSet.readBigDecimal(HBB4TitularFields.NUMEROTITULAR.text));
    hbb4bean.setNombre(fieldSet.readString(HBB4TitularFields.NOMBRE.text).trim());
    hbb4bean.setApellido1(fieldSet.readString(HBB4TitularFields.APELLIDO1.text).trim());
    hbb4bean.setApellido2(fieldSet.readString(HBB4TitularFields.APELLIDO2.text).trim());
    hbb4bean.setDocumentoCliente(fieldSet.readString(HBB4TitularFields.DOCUMENTOCLIENTE.text).trim());
    String titularida = fieldSet.readString(HBB4TitularFields.TITULARIDAD.text);
    if (StringUtils.isNoneBlank(titularida)) {
      hbb4bean.setTitularidad(titularida.charAt(0));
    } else {
      hbb4bean.setTitularidad(' ');
    }
    hbb4bean.setCodIfDepositante(fieldSet.readString(HBB4TitularFields.CODIFDEPOSITANTE.text).trim());
    hbb4bean.setReferenciaCentral(fieldSet.readString(HBB4TitularFields.REFERENCIACENTRAL.text).trim());
    hbb4bean.setNumeroOperacion(fieldSet.readString(HBB4TitularFields.NUMEROOPERACION.text));
    String tipoPersona = fieldSet.readString(HBB4TitularFields.TIPOPERSONA.text);
    if (StringUtils.isNotBlank(tipoPersona)) {
      hbb4bean.setTipoPersona(tipoPersona.charAt(0));
    } else {
      hbb4bean.setTipoPersona(' ');
    }
    String tipoDocumento = fieldSet.readString(HBB4TitularFields.TIPODOCUMENTO.text);
    if (StringUtils.isNotBlank(tipoDocumento)) {
      hbb4bean.setTipoDocumento(tipoDocumento.charAt(0));
    } else {
      hbb4bean.setTipoDocumento(' ');
    }

    BigDecimal particip = fieldSet.readBigDecimal(HBB4TitularFields.PARTICIP.text);
    if (particip == null) {
      hbb4bean.setParticip(BigDecimal.ZERO);
    } else {
      hbb4bean.setParticip(particip.divide(new BigDecimal(100)));
    }
    String indNac = fieldSet.readString(HBB4TitularFields.INDNAC.text);
    if (StringUtils.isNotBlank(indNac)) {
      hbb4bean.setIndNac(indNac.charAt(0));
    } else {
      hbb4bean.setIndNac(' ');
    }

    hbb4bean.setNacionalidad(fieldSet.readString(HBB4TitularFields.NACIONALIDAD.text).trim());

    return hbb4bean;
  }

}
