package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.TipoDeDocumentoBo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceTipoDeDocumento implements SIBBACServiceBean {

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServiceTipoDeDocumento.class );

	@Autowired
	private TipoDeDocumentoBo	tipoDeDocumentoBo;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACServiceTipoDeDocumento() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {

		LOG.debug( "[SIBBACServiceTipoDeDocumento::process] Processing a web request..." );

		String command = webRequest.getAction();

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.debug( "[SIBBACServiceTipoDeDocumento::process]. Command: [{}]", command );

		switch ( command ) {

			case Constantes.CMD_GETLISTA_TIPO_DE_DOCUMENTOS:

				webResponse.setResultados( tipoDeDocumentoBo.getListaTipoDeDocumentos() );
				break;

			case Constantes.CMD_ALTA_TIPO_DE_DOCUMENTO:

				webResponse.setResultados( tipoDeDocumentoBo.altaTipoDeDocumento( params ) );
				break;

			default:

				webResponse.setError( "Comando incorrecto" );
				LOG.error( "No le llega ningun comando correcto" );
				break;

		}

		return webResponse;

	}

	@Override
	public List< String > getAvailableCommands() {

		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_GETLISTA_TIPO_DE_DOCUMENTOS );
		commands.add( Constantes.CMD_ALTA_TIPO_DE_DOCUMENTO );
		return commands;

	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		fields.add( "descripcion" );

		return fields;
	}

}
