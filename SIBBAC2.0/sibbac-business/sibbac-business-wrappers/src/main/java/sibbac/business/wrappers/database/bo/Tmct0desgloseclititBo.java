package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.Tmct0desgloseclititDaoImpl;
import sibbac.business.fase0.database.data.Tmct0desgloseclititData;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.common.threads.ProcesaListaTmct0DesgloseClititRunnable;
import sibbac.business.wrappers.database.dao.Tmct0desgloseclititDao;
import sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.DesgloseClititCaseEjecucionesInformacionMercadoDTO;
import sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista16DTO;
import sibbac.business.wrappers.database.dto.fichero.corretajes.Tmct0desgloseclititEnvioCorretajeDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0desgloseclititBo extends AbstractBo<Tmct0desgloseclitit, Long, Tmct0desgloseclititDao> {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0desgloseclititBo.class);

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0operacioncdeccBo opBo;

  @Autowired
  private Tmct0ejeBo ejeBo;

  @Autowired
  private Tmct0brkBo brokerBo;

  private static final String NOMBRE_BO = "Tmct0desgloseclititBo";

  /**
   * Variable que controla si el proceso ha tenido o no errores.
   */
  private int iNumErrores;

  /**
   * Ejecutor de los hilos.
   */
  private ExecutorService executor;

  private List<String> listaErroneos = new ArrayList<String>();

  /**
   * Valor recuperado del propertis con el numero de hilos que se ejecutaran a
   * la vez
   */
  @Value("${sibbac.accounting.desgloseclitit.numThreadsPool}")
  private Integer numThreadsPool;
  /**
   * Valor recuperado del propertis con el numero de objetos que procesa cada
   * hilo
   */
  @Value("${sibbac.accounting.desgloseclitit.numRegistrosThread}")
  private Integer numRegistrosThread;

  @Autowired
  private Tmct0desgloseclititDaoImpl dcimpDao;

  @Autowired
  private Tmct0anotacioneccBo anBo;

  public BigDecimal sumImtitulosByCdoperacionecc(String cdoperacionecc) {
    return dao.sumImtitulosByCdoperacionecc(cdoperacionecc);
  }

  public List<Tmct0desgloseclitit> findAllByNudesgloses(List<Long> nudesgloses) {
    return dao.findAllByNudesgloses(nudesgloses);
  }

  public List<String> getLcdoperacion() {
    return this.dao.getLCdoperacion();
  }

  public List<Object[]> getTitulosCuentaDiariaConFiltro(List<String> operaciones, HashMap<String, Object> filtros) {
    return dcimpDao.getTitulosCuentaDiariaConFiltro(operaciones, filtros);
  }

  public List<Tmct0desgloseclitit> findAllByTmct0alcId(Tmct0alcId alcId) {
    return dao.findAllByTmct0alcId(alcId);
  }

  public List<Tmct0desgloseclitit> findAllByTmct0alcIdAndCamaraAndCuentaCompensacion(Tmct0alcId alcId,
      String camaraCompensacion, String cuentaCompensacion) {
    return dao.findAllByTmct0alcIdAndCamaraAndCuentaCompensacion(alcId, camaraCompensacion, cuentaCompensacion);
  }

  public List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByImprecioByAloId(Tmct0aloId aloId) {
    return dao.sumImtitulosGroupByImprecioByAloId(aloId);
  }

  public List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloId(
      Tmct0aloId aloId) {
    return dao
        .sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloId(aloId);
  }

  public List<Object[]> sumTitulosByPriceAndAlcId(Tmct0alcId alcId) {
    return dao.sumTitulosByPriceAndAlcId(alcId);
  }

  /**
   * 
   * @param isin
   * @param tpoper
   * @param feEjecucion
   * @param cdEstadoAsig
   * @param cuentaClearing
   * @return
   */
  public List<Date> getListaDistinctFeejecucForParams(String isin, Character tpoper, Date feEjecucion,
      Integer cdEstadoAsig, Character cuentaClearing) {
    return dao.getListaDistinctFeejecucForParams(isin, tpoper, feEjecucion, cdEstadoAsig, cuentaClearing);
  }

  public List<Tmct0desgloseclititData> findForDiaria(Map<String, Serializable> filtros) {
    return dcimpDao.findForDiaria(filtros);
  }

  public List<Tmct0desgloseclitit> findByIddesglosecamaraIn(List<Long> iddesglosecamaraList) {
    return dao.findByIddesglosecamaraIn(iddesglosecamaraList);
  }

  public List<Tmct0desgloseclitit> findByIddesglosecamara(Long iddesglosecamaraList) {
    return dao.findByIddesglosecamara(iddesglosecamaraList);
  }

  public List<Tmct0desgloseclitit> findByTmct0alcId(Tmct0alcId alcId) {
    return dao.findByTmct0alcId(alcId);
  }

  public List<Tmct0desgloseclitit> findAllByBookingIdAndSinTitulosDiponiblesAnotacionForDesglose(Tmct0bokId bookId) {
    return dao.findAllByBookingIdAndSinTitulosDiponiblesAnotacionForDesglose(bookId);
  }

  public List<Long> findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(Date fliquidacion, char isscrdiv) {
    return dao.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
  }

  public List<Long> findAllIdsByNuordenAndNbookingAndTitulosDisponiblesAndDesglosesDeAlta(Tmct0bokId bookId) {
    return dao.findAllIdsByNuordenAndNbookingAndTitulosDisponiblesAndDesglosesDeAlta(bookId.getNuorden(),
        bookId.getNbooking());
  }

  public List<Tmct0bokId> findAllBookingIdByFliquidacionAndTitulosDisponiblesAndMasDe1DesglosesDeAlta(
      Date fliquidacion, char isscrdiv) {
    List<Tmct0bokId> res = new ArrayList<Tmct0bokId>();
    List<Object[]> l = dao.findAllBookingIdByFliquidacionAndTitulosDisponiblesAndMasDe1DesglosesDeAlta(fliquidacion,
        isscrdiv);
    for (Object[] o : l) {
      res.add(new Tmct0bokId((String) o[1], (String) o[0]));
    }
    return res;
  }

  public List<Tmct0bokId> findAllBookingIdByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(Date fliquidacion,
      char isscrdiv) {
    List<Tmct0bokId> res = new ArrayList<Tmct0bokId>();
    List<Object[]> l = dao
        .findAllBookingIdByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
    for (Object[] o : l) {
      res.add(new Tmct0bokId((String) o[1], (String) o[0]));
    }
    return res;
  }

  /**
   * 
   * Actualiza datos de mercado en desgloseclitit
   * 
   * @param nuorden
   * @param nbooking
   * @param nuejecuc
   * @param fhaudit
   * @param cdusuaud
   * @param indicadorCotizacion
   * @param cdoperacionecc
   * @param segmentoMercado
   * @param plataformaNegociacion
   * @param fechaOrdenMercado
   * @param horaOrdenMercado
   */
  public Integer updateDatosMercado(String nuorden, String nbooking, String nuejecuc, Date fhaudit, String cdusuaud,
      Character indicadorCotizacion, String cdoperacionecc, String segmentoMercado, String plataformaNegociacion,
      Date fechaOrdenMercado, Date horaOrdenMercado) {
    return dao.updateDatosMercado(nuorden, nbooking, nuejecuc, fhaudit, cdusuaud, indicadorCotizacion, cdoperacionecc,
        segmentoMercado, plataformaNegociacion, fechaOrdenMercado, horaOrdenMercado);
  }

  /**
   * Recupera los desgloses de un booking que no tienen el nuopeinic insertado
   * */
  public List<DesgloseClititCaseEjecucionesInformacionMercadoDTO> findDesglosesSinDatosMercadoByNuordenNbooking(
      String nuorden, String nbooking) {
    return dao.findDesglosesSinDatosMercadoByNuordenNbooking(nuorden, nbooking);
  }

  public List<Tmct0desgloseclititEnvioCorretajeDTO> findDesglosesFicheroCorretaje(String nuorden, String nbooking,
      String nucnfclt, short nucnfliq) {
    return dao.findDesglosesFicheroCorretaje(nuorden, nbooking, nucnfclt, nucnfliq);
  }

  public Integer updateDatosEnvioFicheroCorretaje(String nuorden, String nbooking, String nucnfclt, short nucnfliq,
      String cdmiembromkt, Date feejecuc, String idFicheroCC) {
    return dao.updateDatosEnvioFicheroCorretaje(nuorden, nbooking, nucnfclt, nucnfliq, cdmiembromkt, feejecuc,
        idFicheroCC);
  }

  public List<ConfirmacionMinorista16DTO> findAllConfirmacionMinorista16(List<String> compensadores,
      List<String> referencias, List<String> notInAliass, String cdrefbanStart, Integer estado, Date sincedate) {

    return this.dao.findAllConfirmacionMinorista16(compensadores, referencias, notInAliass, cdrefbanStart, estado, "9",
        sincedate);
  }

  public Integer updateDatosCompensacionDesgloseClitit(List<Object[]> listaProcesar) {/*
                                                                                       * Long
                                                                                       * iddesglose
                                                                                       * ,
                                                                                       * String
                                                                                       * miembroDestino
                                                                                       * ,
                                                                                       * String
                                                                                       * codigoReferencia
                                                                                       * ,
                                                                                       * String
                                                                                       * cuentaDestino
                                                                                       * )
                                                                                       * {
                                                                                       */
    executor = Executors.newFixedThreadPool(numThreadsPool);
    int iNumHilo = 0;
    iNumHilo++;
    int i = 0;
    int j = 0;

    int paso = numRegistrosThread; // Cada hilo va a procesar el numero de
                                   // registros indicado en esta variable
    // Se recorre la lista principal y se crean sublistas que se van insertado a
    // traves de jobs.
    while (i < listaProcesar.size()) {
      j = i + paso;
      if (j > listaProcesar.size()) {
        j = listaProcesar.size();
      }
      final List<Object[]> subLista = listaProcesar.subList(i, j > listaProcesar.size() ? listaProcesar.size() : j);
      LOG.debug("[" + NOMBRE_BO + " :: Creo la lista con elementos de   " + i + " hasta " + j);

      // Nueva version
      iNumHilo++;
      ejecutarHilo(subLista, iNumHilo);
      i = j; // Se actualiza el paso
    }
    try {
      executor.shutdown();
      executor.awaitTermination(5, TimeUnit.HOURS);
    }
    catch (InterruptedException e) {
      LOG.error("[Tmct0desgloseclititBo :: updateDatosCompensacionDesgloseClitit] -  Se ha interrumpido algún thread en el control de cuantos quedan vivos ");
      LOG.error(e.getMessage(), e);
      shutdownAllRunnable();

    }
    LOG.debug("[Tmct0desgloseclititBo :: updateDatosCompensacionDesgloseClitit] Numero de errores: {0}", iNumErrores);
    return iNumHilo;
  }

  /**
   * Crea un nuevo hilo y lo incorpora al pool
   * 
   * @param listaProcesar - Lista con los elementos que tiene que procesar el
   * hilo
   * @param iNumHilo -- Numero identificativo del hilo
   */
  private void ejecutarHilo(List<Object[]> listaProcesar, int iNumHilo) {
    ProcesaListaTmct0DesgloseClititRunnable runnable = new ProcesaListaTmct0DesgloseClititRunnable(listaProcesar, this,
        iNumHilo);
    executor.execute(runnable);
  }

  /**
   * Elimina el job recibido y si ha habido errores, marca al proceso principal
   * la situacion que el hilo tenia errores
   * 
   * @param current - Job que se quiere eliminar
   * @param iNumJob -
   * @param tieneErrores
   */
  public void shutdownRunnable(ProcesaListaTmct0DesgloseClititRunnable current, int iNumJob, boolean tieneErrores) {
    LOG.debug("[" + NOMBRE_BO + " ::SE ELIMINA EL JOB " + iNumJob);

    if (tieneErrores) {
      LOG.debug("[" + NOMBRE_BO + " ::EL JOB TENIA ERRORES: " + iNumJob);
      iNumErrores++;
    }
  }

  /**
   * Elimina los jobs pendientes del pool.
   */
  public void shutdownAllRunnable() {
    int hilosPendientes;

    LOG.error("[" + NOMBRE_BO + " ::ERROR NO CONTROLADO- SE BORRAR TODOS LOS JOBS ");
    hilosPendientes = executor.shutdownNow().size();
    if (hilosPendientes > 0)
      LOG.error("[{0}::HAN QUEDADO {1} HILOS PENDIENTES SIN EJECUTAR", NOMBRE_BO, hilosPendientes);
  }

  public void actualizarListaDesgloseClitit(List<Object[]> listaDesgloseClitit, int iNumJob) {

    LOG.debug("[" + NOMBRE_BO + " ::INICIO DE LA ACTUALIZACION DE LOS REGISTROS DEL JOB " + iNumJob
        + " Registros a procesar:  " + listaDesgloseClitit.size());

    BigInteger bI;

    for (Object[] movimientoEcc : listaDesgloseClitit) {
      if (movimientoEcc[1] == null && movimientoEcc[2] == null && movimientoEcc[3] == null) {
        listaErroneos.add(movimientoEcc[0].toString());

      }
      else {

        bI = (BigInteger) movimientoEcc[0];
        String miembreDestino = (movimientoEcc[1] == null) ? "" : movimientoEcc[1].toString();
        String codigoReferencia = (movimientoEcc[2] == null) ? "" : movimientoEcc[2].toString();
        String cuentaDestino = (movimientoEcc[3] == null) ? "" : movimientoEcc[3].toString();

        dao.updateDatosCompensacionDesgloseClitit(bI.longValue(), miembreDestino, codigoReferencia, cuentaDestino);

        LOG.debug("UPDATE: ");
      }
    }

    LOG.debug("[" + NOMBRE_BO + " ::FIN DE LA ACTUALIZACION DE LOS REGISTROS DEL JOB " + iNumJob
        + " Registros a procesar:  " + listaDesgloseClitit.size());

  }

  public List<String> getListaErroneos() {
    return listaErroneos;
  }

  public List<ConfirmacionMinorista16DTO> findAllConfirmacionMinorista16(String nuorden, String nbooking,
      List<String> compensadores, List<String> referencias) {
    return dao.findAllConfirmacionMinorista16(nuorden, nbooking, compensadores, referencias,
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(), "9");
  }

  public List<Tmct0desgloseclitit> findAllByCdoperacionecc(String cdoperacionecc) {
    return dao.findAllByCdoperacionecc(cdoperacionecc);
  }

  public List<Tmct0desgloseclitit> findByCdoperacioneccAndCdestadotitLt(String cdoperacionecc, int cdestadotitLt) {
    return dao.findByCdoperacioneccAndCdestadotitLt(cdoperacionecc, cdestadotitLt);
  }

  public List<Tmct0desgloseclitit> findByFeejecucAndNumeroOperacionDcvAndSentidoAndCdestadotitLt(
      String numeroOperacionDcv, Date feejecuc, char sentido, int cdestadotitLt) {
    return dao.findByFeejecucAndNumeroOperacionDcvAndSentidoAndCdestadotitLt(numeroOperacionDcv, feejecuc, sentido,
        cdestadotitLt);
  }

  public List<Long> findAllNudesgloseByDatosMercado(String cdisin, Date feejecuc, Character sentido, String camara,
      String segmento, String cdoperacionmkt, String cdmiembromkt, String nuordenmkt, String nuexemkt) {
    return dao.findAllNudesgloseByDatosMercado(cdisin, feejecuc, sentido, camara, segmento, cdoperacionmkt,
        cdmiembromkt, nuordenmkt, nuexemkt);
  }

  public List<Tmct0desgloseclitit> findAllByDatosMercado(String cdisin, Date feejecuc, Character sentido,
      String camara, String segmento, String cdoperacionmkt, String cdmiembromkt, String nuordenmkt, String nuexemkt,
      Pageable page) {
    return dao.findAllByDatosMercado(cdisin, feejecuc, sentido, camara, segmento, cdoperacionmkt, cdmiembromkt,
        nuordenmkt, nuexemkt, page);
  }

  public List<Tmct0desgloseclitit> findAllByDatosMercadoAndNuordenAndNbooking(String cdisin, Date feejecuc,
      Character sentido, String camara, String segmento, String cdoperacionmkt, String cdmiembromkt, String nuordenmkt,
      String nuexemkt, String nuorden, String nbooking, Pageable page) {
    return dao.findAllByDatosMercadoAndNuordenAndNbooking(cdisin, feejecuc, sentido, camara, segmento, cdoperacionmkt,
        cdmiembromkt, nuordenmkt, nuexemkt, nuorden, nbooking, page);
  }

  public List<Tmct0desgloseclitit> findAllByNuordenAndNbookingAndNucnfcltAndNuejecuc(String nuorden, String nbooking,
      String nucnfclt, String nuejecuc) {
    return dao.findAllByNuordenAndNbookingAndNucnfcltAndNuejecuc(nuorden, nbooking, nucnfclt, nuejecuc);
  }

  public List<Tmct0desgloseclitit> filtarByCuentaCompensacion(List<Tmct0desgloseclitit> dcts, String ctaOrigen,
      String ctaDestino) {
    Tmct0anotacionecc an;
    List<Tmct0desgloseclitit> res = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : dcts) {
      if (StringUtils.isNotBlank(dct.getCuentaCompensacionDestino())
          && (ctaOrigen == null || ctaOrigen.trim().equals(dct.getCuentaCompensacionDestino().trim()))
          && !ctaDestino.trim().equals(dct.getCuentaCompensacionDestino().trim())) {
        res.add(dct);
      }
      else {
        an = anBo.findByCdoperacionecc(dct.getCdoperacionecc());
        if (an != null && an.getTmct0CuentasDeCompensacion() != null
            && (ctaOrigen == null || an.getTmct0CuentasDeCompensacion().getCdCodigo().trim().equals(ctaOrigen.trim()))
            && !an.getTmct0CuentasDeCompensacion().getCdCodigo().trim().equals(ctaDestino.trim())) {
          dct.setCuentaCompensacionDestino(an.getTmct0CuentasDeCompensacion().getCdCodigo());
          res.add(dct);
        }
      }
    }
    return res;
  }

  public List<Tmct0desgloseclitit> filtarByConEcc(List<Tmct0desgloseclitit> dcts, List<String> segmentosSinEcc) {
    List<Tmct0desgloseclitit> res = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : dcts) {
      if (dct.getCdsegmento() != null && !segmentosSinEcc.contains(dct.getCdsegmento().trim())) {
        res.add(dct);
      }
    }
    return res;
  }

  public List<Tmct0desgloseclitit> filtarByCdoperacionecc(List<Tmct0desgloseclitit> dcts, String cdoperacionecc) {
    List<Tmct0desgloseclitit> res = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : dcts) {
      if (dct.getCdoperacionecc() != null && dct.getCdoperacionecc().trim().equals(cdoperacionecc)) {
        res.add(dct);
      }
    }
    return res;
  }

  public List<Tmct0desgloseclitit> filtarByCdoperacionecc(List<Tmct0desgloseclitit> dcts,
      List<String> listCdoperacionecc) {
    List<Tmct0desgloseclitit> res = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : dcts) {
      if (dct.getCdoperacionecc() != null && listCdoperacionecc.contains(dct.getCdoperacionecc().trim())) {
        res.add(dct);
      }
    }
    return res;
  }

  public List<Tmct0desgloseclitit> filtarById(List<Tmct0desgloseclitit> dcts, List<Long> listNudesgloses) {
    List<Tmct0desgloseclitit> res = new ArrayList<Tmct0desgloseclitit>();
    for (Tmct0desgloseclitit dct : dcts) {
      if (listNudesgloses.contains(dct.getNudesglose())) {
        res.add(dct);
      }
    }
    return res;
  }

  public void repartirCalculos(Tmct0desgloseclitit original, Tmct0desgloseclitit nuevo,
      BigDecimal titulosDesgloseOriginal) {
    LOG.debug("[INICIO] repartirCalculos() nudesglose =  {}, Titulos Original : {}, Titulos Disociar : {}  ",
        original.getNudesglose(), original.getImtitulos(), titulosDesgloseOriginal);
    // titulos originales del desglose
    BigDecimal nutitOriginal = original.getImtitulos();
    nuevo.setImtitulos(original.getImtitulos().subtract(titulosDesgloseOriginal));
    original.setImtitulos(titulosDesgloseOriginal);
    LOG.debug("repartirCalculos() imefectivo Original = {}", original.getImefectivo());
    // efectivo
    original.setImefectivo(original.getImefectivo().multiply(original.getImtitulos())
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() imefectivo Disociado = {}", original.getImefectivo());
    nuevo.setImefectivo(nuevo.getImefectivo().subtract(original.getImefectivo()));
    LOG.debug("repartirCalculos() imefectivo Nuevo = {}", nuevo.getImefectivo());
    // canon
    LOG.debug("repartirCalculos() ImCanonCompDv Original = {}", original.getImcanoncompdv());
    original.setImcanoncompdv(original.getImcanoncompdv().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() ImCanonCompDv Disociado = {}", original.getImcanoncompdv());
    nuevo.setImcanoncompdv(nuevo.getImcanoncompdv().subtract(original.getImcanoncompdv()));
    LOG.debug("repartirCalculos() ImCanonCompDv Nuevo = {}", nuevo.getImcanoncompdv());

    LOG.debug("repartirCalculos() ImCanonCompEu Original = {}", original.getImcanoncompeu());
    original.setImcanoncompeu(original.getImcanoncompeu().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() ImCanonCompEu Disociado = {}", original.getImcanoncompeu());
    nuevo.setImcanoncompeu(nuevo.getImcanoncompeu().subtract(original.getImcanoncompeu()));
    LOG.debug("repartirCalculos() ImCanonCompEu Nuevo = {}", nuevo.getImcanoncompeu());

    LOG.debug("repartirCalculos() ImCanonContrDv Original = {}", original.getImcanoncontrdv());
    original.setImcanoncontrdv(original.getImcanoncontrdv().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() ImCanonCompEu Original = {}", original.getImcanoncontrdv());
    nuevo.setImcanoncontrdv(nuevo.getImcanoncontrdv().subtract(original.getImcanoncontrdv()));
    LOG.debug("repartirCalculos() ImCanonCompEu Nuevo = {}", nuevo.getImcanoncontrdv());

    LOG.debug("repartirCalculos() ImCanonContrEu Original = " + original.getImcanoncontreu());
    original.setImcanoncontreu(original.getImcanoncontreu().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() ImCanonContrEu Disociado = {}" + original.getImcanoncontreu());
    nuevo.setImcanoncontreu(nuevo.getImcanoncontreu().subtract(original.getImcanoncontreu()));
    LOG.debug("repartirCalculos() ImCanonContrEu Nuevo = {}", nuevo.getImcanoncontreu());

    LOG.debug("repartirCalculos() ImCanonLiqDv Original = {}", original.getImcanonliqdv());
    original.setImcanonliqdv(original.getImcanonliqdv().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() ImCanonLiqDv Disociado = {}", original.getImcanonliqdv());
    nuevo.setImcanonliqdv(nuevo.getImcanonliqdv().subtract(original.getImcanonliqdv()));
    LOG.debug("repartirCalculos() ImCanonLiqDv Nuevo = {}", nuevo.getImcanonliqdv());

    LOG.debug("repartirCalculos() ImCanonLiqEu Original = {}", original.getImcanonliqeu());
    original.setImcanonliqeu(original.getImcanonliqeu().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() ImCanonLiqEu Disociado = {}", original.getImcanonliqeu());
    nuevo.setImcanonliqeu(nuevo.getImcanonliqeu().subtract(original.getImcanonliqeu()));
    LOG.debug("repartirCalculos() ImCanonLiqEu Nuevo = {}", nuevo.getImcanonliqeu());

    LOG.debug("repartirCalculos() Imtotbru Original = {}", original.getImtotbru());
    original.setImtotbru(original.getImtotbru().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() Imtotbru Disociado = {}", original.getImtotbru());
    nuevo.setImtotbru(nuevo.getImtotbru().subtract(original.getImtotbru()));
    LOG.debug("repartirCalculos() Imtotbru Nuevo = {}", nuevo.getImtotbru());

    LOG.debug("repartirCalculos() Imcombco Original = {}", original.getImcombco());
    original.setImcombco(original.getImcombco().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() Imcombco Disociado = {}", original.getImcombco());
    nuevo.setImcombco(nuevo.getImcombco().subtract(original.getImcombco()));
    LOG.debug("repartirCalculos() Imcombco Nuevo = {}", nuevo.getImcombco());
    if (original.getImajusvb() == null) {
      original.setImajusvb(BigDecimal.ZERO);
    }
    LOG.debug("repartirCalculos() Imajusvb Original = {}", original.getImajusvb());
    original.setImajusvb(original.getImajusvb().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() Imajusvb Disociado = {}", original.getImajusvb());
    nuevo.setImajusvb(nuevo.getImajusvb().subtract(original.getImajusvb()));
    LOG.debug("repartirCalculos() Imajusvb Nuevo = {}", nuevo.getImajusvb());

    LOG.debug("repartirCalculos() Imbansis Original = {}", original.getImbansis());
    original.setImbansis(original.getImbansis().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() Imbansis Disociado = {}", original.getImbansis());
    nuevo.setImbansis(nuevo.getImbansis().subtract(original.getImbansis()));
    LOG.debug("repartirCalculos() Imbansis Nuevo = {}", nuevo.getImbansis());

    LOG.debug("repartirCalculos() Imcombrk Original = {}", original.getImcombrk());
    original.setImcombrk(original.getImcombrk().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() Imcombrk Disociado = {}", original.getImcombrk());
    nuevo.setImcombrk(nuevo.getImcombrk().subtract(original.getImcombrk()));
    LOG.debug("repartirCalculos() Imcombrk Nuevo = {}", nuevo.getImcombrk());

    LOG.debug("repartirCalculos() getImcomdvo Original = {}", original.getImcomdvo());
    original.setImcomdvo(original.getImcomdvo().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImcomdvo Disociado = {}", original.getImcomdvo());
    nuevo.setImcomdvo(nuevo.getImcomdvo().subtract(original.getImcomdvo()));
    LOG.debug("repartirCalculos() getImcomdvo Nuevo = {}", nuevo.getImcomdvo());

    LOG.debug("repartirCalculos() getImcomisn Original = {}", original.getImcomisn());
    original.setImcomisn(original.getImcomisn().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImcomisn Disociado = {}", original.getImcomisn());
    nuevo.setImcomisn(nuevo.getImcomisn().subtract(original.getImcomisn()));
    LOG.debug("repartirCalculos() getImcomisn Nuevo = {}", nuevo.getImcomisn());

    LOG.debug("repartirCalculos() getImcomsis Original = {}", original.getImcomsis());
    original.setImcomsis(original.getImcomsis().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImcomsis Disociado = {}", original.getImcomsis());
    nuevo.setImcomsis(nuevo.getImcomsis().subtract(original.getImcomsis()));
    LOG.debug("repartirCalculos() getImcomsis Nuevo = {}", nuevo.getImcomsis());

    LOG.debug("repartirCalculos() getImcomsvb Original = " + original.getImcomsvb());
    original.setImcomsvb(original.getImcomsvb().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImcomsvb Disociado = {}", original.getImcomsvb());
    nuevo.setImcomsvb(nuevo.getImcomsvb().subtract(original.getImcomsvb()));
    LOG.debug("repartirCalculos() getImcomsvb Nuevo = {}", nuevo.getImcomsvb());

    LOG.debug("repartirCalculos() getImfinsvb Original = {}", original.getImfinsvb());
    original.setImfinsvb(original.getImfinsvb().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImfinsvb Disociado = {}", original.getImfinsvb());
    nuevo.setImfinsvb(nuevo.getImfinsvb().subtract(original.getImfinsvb()));
    LOG.debug("repartirCalculos() getImfinsvb Nuevo = {}", nuevo.getImfinsvb());

    LOG.debug("repartirCalculos() getImnetliq Original = {}", original.getImnetliq());
    original.setImnetliq(original.getImnetliq().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImnetliq Disociado = {}", original.getImnetliq());
    nuevo.setImnetliq(nuevo.getImnetliq().subtract(original.getImnetliq()));
    LOG.debug("repartirCalculos() getImnetliq Nuevo = {}", nuevo.getImnetliq());

    LOG.debug("repartirCalculos() getImnfiliq Original = {}", original.getImnfiliq());
    original.setImnfiliq(original.getImnfiliq().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImnfiliq Disociado = {}", original.getImnfiliq());
    nuevo.setImnfiliq(nuevo.getImnfiliq().subtract(original.getImnfiliq()));
    LOG.debug("repartirCalculos() getImnfiliq Nuevo = {}", nuevo.getImnfiliq());

    LOG.debug("repartirCalculos() getImntbrok Original = {}", original.getImntbrok());
    original.setImntbrok(original.getImntbrok().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImntbrok Disociado = {}", original.getImntbrok());
    nuevo.setImntbrok(nuevo.getImntbrok().subtract(original.getImntbrok()));
    LOG.debug("repartirCalculos() getImntbrok Nuevo = {}", nuevo.getImntbrok());

    LOG.debug("repartirCalculos() getImtotnet Original = {}", original.getImtotnet());
    original.setImtotnet(original.getImtotnet().multiply(titulosDesgloseOriginal)
        .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
    LOG.debug("repartirCalculos() getImtotnet Disociado = {}", original.getImtotnet());
    nuevo.setImtotnet(nuevo.getImtotnet().subtract(original.getImtotnet()));
    LOG.debug("repartirCalculos() getImtotnet Nuevo = {}", nuevo.getImtotnet());

    if (original.getImcobrado() == null) {
      original.setImcobrado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() setImcobrado Original = {}", original.getImcobrado());
      original.setImcobrado(original.getImcobrado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() setImcobrado Disociado = {}", original.getImcobrado());
      nuevo.setImcobrado(nuevo.getImcobrado().subtract(original.getImcobrado()));
      LOG.debug("repartirCalculos() setImcobrado Nuevo = {}", nuevo.getImtotnet());
    }

    if (original.getImefeMercadoCobrado() == null) {
      original.setImefeMercadoCobrado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() getImefeMercadoCobrado Original = {}", original.getImefeMercadoCobrado());
      original.setImefeMercadoCobrado(original.getImefeMercadoCobrado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() getImefeMercadoCobrado Disociado = {}", original.getImefeMercadoCobrado());
      nuevo.setImefeMercadoCobrado(nuevo.getImefeMercadoCobrado().subtract(original.getImefeMercadoCobrado()));
      LOG.debug("repartirCalculos() getImefeMercadoCobrado Nuevo = {}", nuevo.getImefeMercadoCobrado());
    }

    if (original.getImefeClienteCobrado() == null) {
      original.setImefeClienteCobrado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() getImefeClienteCobrado Original = {}", original.getImefeClienteCobrado());
      original.setImefeClienteCobrado(original.getImefeClienteCobrado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() getImefeClienteCobrado Disociado = {}", original.getImefeClienteCobrado());
      nuevo.setImefeClienteCobrado(nuevo.getImefeClienteCobrado().subtract(original.getImefeClienteCobrado()));
      LOG.debug("repartirCalculos() getImefeClienteCobrado Nuevo = {}", nuevo.getImefeClienteCobrado());
    }

    if (original.getImcanonCobrado() == null) {
      original.setImcanonCobrado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() getImcanonCobrado Original = {}", original.getImcanonCobrado());
      original.setImcanonCobrado(original.getImcanonCobrado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() getImcanonCobrado Disociado = {}", original.getImcanonCobrado());
      nuevo.setImcanonCobrado(nuevo.getImcanonCobrado().subtract(original.getImcanonCobrado()));
      LOG.debug("repartirCalculos() getImcanonCobrado Nuevo = {}", nuevo.getImcanonCobrado());
    }

    if (original.getImcomCobrado() == null) {
      original.setImcomCobrado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() getImcomCobrado Original = {}", original.getImcomCobrado());
      original.setImcomCobrado(original.getImcomCobrado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() getImcomCobrado Disociado = {}", original.getImcomCobrado());
      nuevo.setImcomCobrado(nuevo.getImcomCobrado().subtract(original.getImcomCobrado()));
      LOG.debug("repartirCalculos() getImcomCobrado Nuevo = {}", nuevo.getImcomCobrado());
    }

    if (original.getImbrkPagado() == null) {
      original.setImbrkPagado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() getImbrkPagado Original = {}", original.getImbrkPagado());
      original.setImbrkPagado(original.getImbrkPagado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() getImbrkPagado Disociado = {}", original.getImbrkPagado());
      nuevo.setImbrkPagado(nuevo.getImbrkPagado().subtract(original.getImbrkPagado()));
      LOG.debug("repartirCalculos() getImbrkPagado Nuevo = {}", nuevo.getImbrkPagado());
    }

    if (original.getImdvoPagado() == null) {
      original.setImdvoPagado(BigDecimal.ZERO);
    }
    else {
      LOG.debug("repartirCalculos() getImdvoPagado Original = {}", original.getImdvoPagado());
      original.setImdvoPagado(original.getImdvoPagado().multiply(titulosDesgloseOriginal)
          .divide(nutitOriginal, 2, BigDecimal.ROUND_HALF_UP));
      LOG.debug("repartirCalculos() getImdvoPagado Disociado = {}", original.getImdvoPagado());
      nuevo.setImdvoPagado(nuevo.getImdvoPagado().subtract(original.getImdvoPagado()));
      LOG.debug("repartirCalculos() getImdvoPagado Nuevo = {}", nuevo.getImdvoPagado());
    }

    LOG.debug("[FIN] repartirCalculos() nudesglose =  {}", original.getNudesglose());
  }

  public void cargarDatosFromAnotacion(Tmct0alcId alcId, Tmct0desgloseclitit dct, Tmct0anotacionecc an, String auditUser) {
    this.cargarCodigosOperacionFromAn(alcId, dct, an, auditUser);
    this.cargarCuentaCompensacionFromAn(alcId, dct, an, auditUser);
  }

  @Transactional
  public void cargarCuentaCompensacionFromAn(Tmct0alcId alcId, Tmct0desgloseclitit dct, Tmct0anotacionecc an,
      String auditUser) {
    String ctaOrigen = dct.getCuentaCompensacionDestino();
    if (StringUtils.isBlank(ctaOrigen)
        || !StringUtils.equals(ctaOrigen, an.getTmct0CuentasDeCompensacion().getCdCodigo())) {
      ctaOrigen = an.getTmct0CuentasDeCompensacion().getCdCodigo();
      dct.setCuentaCompensacionDestino(ctaOrigen);
      dct.setMiembroCompensadorDestino("");
      dct.setCodigoReferenciaAsignacion("");
      dct.setAuditFechaCambio(new Date());
      dct.setAuditUser(auditUser);
      String msg = String.format("Cargada cta en el desglose: '%s' op ecc: '%s'", ctaOrigen, dct.getCdoperacionecc());
      LOG.trace("[cargarCuentaCompensacionFromAn] grabando registro en tmct0log con informacion {}...", msg);
      logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
    }
  }

  @Transactional
  public void cargarCodigosOperacionFromAn(Tmct0alcId alcId, Tmct0desgloseclitit dct, Tmct0anotacionecc an,
      String auditUser) {
    if (StringUtils.isBlank(dct.getCdoperacionecc())) {
      dct.setCdoperacionecc(an.getCdoperacionecc());
      dct.setNuoperacionprev(an.getNuoperacionprev());
      dct.setNuoperacioninic(an.getNuoperacioninic());
      dct.setAuditFechaCambio(new Date());
      dct.setAuditUser(auditUser);
      String msg = String.format("Cargada cod. op. ecc. en el desglose op ecc: '%s'", dct.getCdoperacionecc());
      LOG.trace("[cargarCuentaCompensacionFromAn] grabando registro en tmct0log con informacion {}...", msg);
      logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);
    }
  }

  @Transactional
  public void actualizarCodigosOperacion(Tmct0movimientoeccFidessa movFidessa, Tmct0desgloseclitit dct, String auditUser) {
    String msg;
    String ctaOrigen = dct.getCuentaCompensacionDestino();
    dct.setNuoperacionprev(movFidessa.getNuoperacionprev());
    dct.setCdoperacionecc(movFidessa.getCdoperacionecc());
    dct.setMiembroCompensadorDestino(movFidessa.getMiembroCompensadorDestino());
    dct.setCodigoReferenciaAsignacion(movFidessa.getCdrefasignacion());
    dct.setCuentaCompensacionDestino(movFidessa.getCuentaCompensacionDestino());
    dct.setAuditFechaCambio(new Date());
    dct.setAuditUser(auditUser);
    if (StringUtils.isNotBlank(movFidessa.getCuentaCompensacionDestino())) {
      msg = String.format(
          "Cambiados codigos desglose: '%s' de op a :'%s' y prev: '%s' cta.origen: '%s' cta.destino: '%s'",
          dct.getNudesglose(), dct.getCdoperacionecc(), dct.getNuoperacionprev(), ctaOrigen,
          movFidessa.getCuentaCompensacionDestino());

    }
    else {
      msg = String.format(
          "Cambiados codigos desglose: '%s' de op a :'%s' y prev: '%s' cta.origen: '%s' miembro: '%s' ref.asig: '%s'",
          dct.getNudesglose(), dct.getCdoperacionecc(), dct.getNuoperacionprev(), ctaOrigen,
          movFidessa.getMiembroCompensadorDestino(), movFidessa.getCdrefasignacion());
    }

    LOG.trace("[actualizarCodigosOperacion] grabando registro en tmct0log con informacion {}...", msg);
    Tmct0alcId alcId = dct.getTmct0desglosecamara().getTmct0alc().getId();
    logBo.insertarRegistro(alcId.getNuorden(), alcId.getNbooking(), alcId.getNucnfclt(), msg, "", auditUser);

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void recuperarAnotaciones(Tmct0alcId alcId, List<Tmct0desgloseclitit> listDcts, String auditUser) {
    Tmct0anotacionecc an;
    Tmct0operacioncdecc op;
    for (Tmct0desgloseclitit dct : listDcts) {
      if (dct.getAnotacion() == null) {
        if (StringUtils.isBlank(dct.getCdoperacionecc())) {
          op = opBo.findTmct0operacioncdeccByInformacionMercado(dct.getNuordenmkt(), dct.getNurefexemkt(),
              dct.getFeejecuc(), dct.getTpopebol(), dct.getCdisin(), dct.getCdsegmento(), dct.getCdsentido(),
              dct.getCdmiembromkt());
          if (op != null) {
            an = anBo.findByCdoperacionecc(op.getCdoperacionecc());
            dct.setAnotacion(an);
          }
        }
        else {
          an = anBo.findByCdoperacionecc(dct.getCdoperacionecc());
          dct.setAnotacion(an);
        }
      }

      if (dct.getAnotacion() != null) {
        cargarDatosFromAnotacion(alcId, dct, dct.getAnotacion(), auditUser);
      }
    }
  }

  public BigDecimal sumTitulosByTmct0bokId(Tmct0bokId bookId) {
    return dao.sumTitulosByTmct0bokId(bookId);
  }

  @Transactional
  public void loadTmct0ejeFeeSchema(Tmct0desgloseclitit dct) {
    if (dct.getTmct0ejeFeeSchema() == null) {
      Tmct0ejeId id = new Tmct0ejeId();
      id.setNuorden(dct.getTmct0desglosecamara().getTmct0alc().getId().getNuorden());
      id.setNbooking(dct.getTmct0desglosecamara().getTmct0alc().getId().getNbooking());
      id.setNuejecuc(dct.getNuejecuc());
      dct.setTmct0ejeFeeSchema(ejeBo.findTmct0ejeFeeSchemaById(id));
    }
  }

  @Transactional
  public void loadTmct0ejeFeeSchema(List<Tmct0desgloseclitit> listDcts) {
    for (Tmct0desgloseclitit dct : listDcts) {
      this.loadTmct0ejeFeeSchema(dct);
    }
  }

  @Transactional
  public void loadIsMTF(List<Tmct0desgloseclitit> listDcts) {
    final Map<String, Boolean> cachedValues = new HashMap<String, Boolean>();
    for (Tmct0desgloseclitit dct : listDcts) {
      dct.setMtf(brokerBo.isCdbrokerMtf(cachedValues, dct.getCdPlataformaNegociacion()));
    }
    cachedValues.clear();
  }

}
