package sibbac.business.wrappers.database.dao.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0anotacionecc;


public class AnotacionEccSpecificacions {

	protected static final Logger	LOG	= LoggerFactory.getLogger( AnotacionEccSpecificacions.class );


	public static Specification< Tmct0anotacionecc > fechaContratacionDesde( final Date fecha ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::fechaContratacionDesde] [fcontratacion>={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fcontratacion" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0anotacionecc > fechaContratacionHasta( final Date fecha ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::fechaContratacionHasta] [fcontratacion<={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fcontratacion" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0anotacionecc > fechaLiquidacionDesde( final Date fecha ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::fechaLiquidacionDesde] [fliqteorica=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fliqteorica" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0anotacionecc > fechaLiquidacionHasta( final Date fecha ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::fechaLiquidacionHasta] [fliqteorica=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fliqteorica" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}
	
	public static Specification< Tmct0anotacionecc > isin( final String isin ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate isinPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::isin] [isin=={}]", isin );
				if ( isin != null ) {
					isinPredicate = builder.equal( root.< String > get( "cdisin" ), isin );
				}
				return isinPredicate;
			}
		};
	}
	
	public static Specification< Tmct0anotacionecc > camara( final String camara ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::camara] [camara=={}]", camara );
				if ( camara != null ) {
					camaraPredicate = builder.equal( root.< String > get( "cdcamara" ), camara );
				}
				return camaraPredicate;
			}
		};
	}

	public static Specification< Tmct0anotacionecc > codCuenta( final String codCuenta ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::camara] [camara=={}]", codCuenta );
				if ( codCuenta != null ) {
					camaraPredicate = builder.equal( root.<Tmct0CuentasDeCompensacion> get("tmct0CuentasDeCompensacion").< String > get( "cdCodigo" ), codCuenta );
				}
				return camaraPredicate;
			}
		};
	}
	
	public static Specification< Tmct0anotacionecc > notCodCuenta( final String codCuenta ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::camara] [tmct0CuentasDeCompensacion.cdCodigo=={}]", codCuenta );
				if ( codCuenta != null ) {
					camaraPredicate = builder.notEqual( root.<Tmct0CuentasDeCompensacion> get("tmct0CuentasDeCompensacion").< String > get( "cdCodigo" ), codCuenta );
				}
				return camaraPredicate;
			}
		};
	}
	
	public static Specification< Tmct0anotacionecc > isNullCodCuenta( ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[AnotacionEccSpecificacions::camara] [tmct0CuentasDeCompensacion.codCuenta=={}]" );
				camaraPredicate = builder.isNull( root.<Tmct0CuentasDeCompensacion> get("tmct0CuentasDeCompensacion").< String > get( "cdCodigo" ) );
				
				return camaraPredicate;
			}
		};
	}
	
	public static Specification< Tmct0anotacionecc > listadoFriltrado( final Date fContratacionDesde, final Date fContratacionHasta,  final Date fLiquidacionDesde, final Date fLiquidacionHasta, final String camara, final String isin, final String codCuenta, final boolean coincidirCuenta ) {
		return new Specification< Tmct0anotacionecc >() {

			public Predicate toPredicate( Root< Tmct0anotacionecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0anotacionecc > specs = null;
				if ( fContratacionDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaContratacionDesde( fContratacionDesde ) );
					}

				}
				if ( fContratacionHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaContratacionHasta( fContratacionHasta ) );
					} else {
						specs = specs.and( fechaContratacionHasta( fContratacionHasta ) );
					}

				}
				if ( fLiquidacionDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaLiquidacionDesde( fLiquidacionDesde ) );
					} else {
						specs = specs.and( fechaLiquidacionDesde( fLiquidacionDesde ) );
					}

				}
				if ( fLiquidacionHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaLiquidacionHasta( fLiquidacionHasta ) );
					} else {
						specs = specs.and( fechaLiquidacionHasta( fLiquidacionHasta ) );
					}

				}
				if ( isin != null ) {
					if ( specs == null ) {
						specs = Specifications.where( isin( isin ) );
					} else {
						specs = specs.and( isin( isin ) );
					}

				}
				if ( camara != null ) {
					if ( specs == null ) {
						specs = Specifications.where( camara( camara ) );
					} else {
						specs = specs.and( camara( camara ) );
					}

				}
				if ( codCuenta != null ) {
					if ( specs == null ) {
						if(coincidirCuenta){
							specs = Specifications.where( codCuenta( codCuenta ) );
						}else{
							specs = Specifications.where( notCodCuenta( codCuenta ) ).or( isNullCodCuenta() );
						}
					} else {
						if(coincidirCuenta){	
							specs = specs.and( codCuenta( codCuenta ) );
						}else{
							specs = specs.and( notCodCuenta( codCuenta ) );
						}
					}

				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}
}
