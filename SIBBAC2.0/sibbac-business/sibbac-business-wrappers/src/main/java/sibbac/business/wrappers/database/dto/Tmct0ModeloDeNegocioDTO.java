package sibbac.business.wrappers.database.dto;

import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0ModeloDeNegocio;

public class Tmct0ModeloDeNegocioDTO {
	
	
	private Long id_modelo_de_negocio;
	private String audit_user;
	private String nbDescripcion;
	private String cd_codigo;
	private Date audit_fecha_cambio;
	
	
	public Tmct0ModeloDeNegocioDTO() {}


	public Tmct0ModeloDeNegocioDTO(Tmct0ModeloDeNegocio tmct0ModeloDeNegocio) {
		
		id_modelo_de_negocio = tmct0ModeloDeNegocio.getId_modelo_de_negocio();
		audit_user = tmct0ModeloDeNegocio.getAudit_user();
		nbDescripcion = tmct0ModeloDeNegocio.getNbDescripcion();
		cd_codigo = tmct0ModeloDeNegocio.getCd_codigo();
		audit_fecha_cambio = tmct0ModeloDeNegocio.getAudit_fecha_cambio();
	}


	public Long getId_modelo_de_negocio() {
		return id_modelo_de_negocio;
	}


	public void setId_modelo_de_negocio(Long id_modelo_de_negocio) {
		this.id_modelo_de_negocio = id_modelo_de_negocio;
	}


	public String getAudit_user() {
		return audit_user;
	}


	public void setAudit_user(String audit_user) {
		this.audit_user = audit_user;
	}


	public String getNbDescripcion() {
		return nbDescripcion;
	}


	public void setNbDescripcion(String nb_descripcion) {
		this.nbDescripcion = nb_descripcion;
	}


	public String getCd_codigo() {
		return cd_codigo;
	}


	public void setCd_codigo(String cd_codigo) {
		this.cd_codigo = cd_codigo;
	}


	public Date getAudit_fecha_cambio() {
		return audit_fecha_cambio;
	}


	public void setAudit_fecha_cambio(Date audit_fecha_cambio) {
		this.audit_fecha_cambio = audit_fecha_cambio;
	}
	
	

}
