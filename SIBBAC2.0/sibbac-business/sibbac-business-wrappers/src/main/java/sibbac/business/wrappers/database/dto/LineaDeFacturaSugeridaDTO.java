package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;


public class LineaDeFacturaSugeridaDTO {

	private Long		id;
	private Long		idfacturaSugerida;
	private Integer		numero;
	private String		concepto;
	private Long		idMoneda;
	private BigDecimal	importe;
	private Long		idPeriodo;
	private Long		idAlcOrdenLineaDeFacturaSugerida;

	public LineaDeFacturaSugeridaDTO( Long id, Long idfacturaSugerida, Integer numero, String concepto, Long idMoneda, BigDecimal importe,
			Long idPeriodo, Long idAlcOrdenLineaDeFacturaSugerida ) {
		super();
		this.id = id;
		this.idfacturaSugerida = idfacturaSugerida;
		this.numero = numero;
		this.concepto = concepto;
		this.idMoneda = idMoneda;
		this.importe = importe;
		this.idPeriodo = idPeriodo;
		this.idAlcOrdenLineaDeFacturaSugerida = idAlcOrdenLineaDeFacturaSugerida;
	}

	public LineaDeFacturaSugeridaDTO() {

	}

	public Long getId() {
		return id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Long getIdfacturaSugerida() {
		return idfacturaSugerida;
	}

	public void setIdfacturaSugerida( Long idfacturaSugerida ) {
		this.idfacturaSugerida = idfacturaSugerida;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero( Integer numero ) {
		this.numero = numero;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto( String concepto ) {
		this.concepto = concepto;
	}

	public Long getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda( Long idMoneda ) {
		this.idMoneda = idMoneda;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	public Long getIdPeriodo() {
		return idPeriodo;
	}

	public void setIdPeriodo( Long idPeriodo ) {
		this.idPeriodo = idPeriodo;
	}

	public Long getIdAlcOrdenLineaDeFacturaSugerida() {
		return idAlcOrdenLineaDeFacturaSugerida;
	}

	public void setIdAlcOrdenLineaDeFacturaSugerida( Long idAlcOrdenLineaDeFacturaSugerida ) {
		this.idAlcOrdenLineaDeFacturaSugerida = idAlcOrdenLineaDeFacturaSugerida;
	}

}
