package sibbac.business.wrappers.database.bo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.dao.Tmct0ejecucionalocationDao;
import sibbac.business.wrappers.database.data.Tmct0ejecucionalocationData;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ejecucionalocationBo extends AbstractBo<Tmct0ejecucionalocation, Long, Tmct0ejecucionalocationDao> {

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Transactional
  public List<Tmct0ejecucionalocationData> findAllByNuorden(String nuorden) {
    return this.dao.findAllByNuorden(nuorden);
  }

  public List<Tmct0ejecucionalocation> findAllByNuordenAndNbooking(String nuorden, String nbooking) {
    return this.dao.findAllByNuordenAndNbooking(nuorden, nbooking);
  }

  @Transactional
  public List<Tmct0ejecucionalocation> findAllByNuordenTransactional(String nuorden) {
    return this.dao.findAllByNuordenTransactional(nuorden);
  }

  public List<Tmct0ejecucionalocationData> findAllDataByAlloId(Tmct0aloId id) {
    return this.dao.findAllDataByAlloId(id);
  }

  public List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOConECCByAlloId(
      Tmct0aloId id) {
    return this.dao.findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOConECCByAlloId(id);
  }

  public List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOSinECCByAlloId(
      Tmct0aloId id) {
    return this.dao.findAllEjecucionAlocationProcesarDesglosesOpEspecialesDTOSinECCByAlloId(id);
  }

  public List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> findAllTitulosDesglososadosEjeAloOperacionEspecialByBookId(
      Tmct0bokId id) {
    return this.dao.findAllTitulosDesglososadosEjeAloOperacionEspecialByBookId(id,
        EstadosEnumerados.ASIGNACIONES.PDTE_OPERATIVA_ESPECIAL_TRATANDOSE.getId());
  }

  public List<Tmct0ejecucionalocation> findAllByAlloIdAndInCamarasCompensacion(Tmct0aloId id, String camarasCompensacion) {
    return dao.findAllByAlloIdAndCamarasCompensacion(id, camarasCompensacion);
  }

  public List<Tmct0ejecucionalocation> findAllByAlloIdAndCamarasCompensacion(Tmct0aloId id,
      List<String> camarasCompensacion) {
    return this.dao.findAllByAlloIdAndInCamarasCompensacion(id, camarasCompensacion);
  }

  public List<Tmct0ejecucionalocation> findAllByAlloId(Tmct0aloId id) {
    return dao.findAllByAlloId(id);
  }

  public List<Tmct0ejecucionalocation> findAllByDatosMercado(String cdisin, Date feejecuc, char sentido, String camara,
      String segmento, String cdoperacionmkt, String cdmiembromkt, String nuordenmkt, String nuexemkt, Pageable page) {
    return dao.findAllByDatosMercado(cdisin, feejecuc, sentido, camara, segmento, cdoperacionmkt, cdmiembromkt,
        nuordenmkt, nuexemkt, page);
  }

  public List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByImprecioByAloId(Tmct0aloId aloId) {
    return dao.sumImtitulosGroupByImprecioByAloId(aloId);
  }

  public List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndNotCase(
      Tmct0aloId aloId) {
    return dao
        .sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndNotCase(aloId);
  }

  public List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndCase(
      Tmct0aloId aloId) {
    return dao
        .sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloIdAndCase(aloId);

  }

  /**
   * @param bokId
   */
  public void recalcularTitulosPendientesDesglose(Tmct0bokId bokId) {
    List<Tmct0ejecucionalocation> ejealos = findAllByNuordenAndNbooking(bokId.getNuorden(), bokId.getNbooking());
    List<Tmct0desgloseclitit> dcts;
    for (Tmct0ejecucionalocation ea : ejealos) {
      ea.setNutitulosPendientesDesglose(ea.getNutitulos());
      ea.setAuditFechaCambio(new Date());
      dcts = dctBo.findAllByNuordenAndNbookingAndNucnfcltAndNuejecuc(ea.getTmct0grupoejecucion().getTmct0alo().getId()
          .getNuorden(), ea.getTmct0grupoejecucion().getTmct0alo().getId().getNbooking(), ea.getTmct0grupoejecucion()
          .getTmct0alo().getId().getNucnfclt(), ea.getTmct0eje().getId().getNuejecuc());

      for (Tmct0desgloseclitit dct : dcts) {
        ea.setNutitulosPendientesDesglose(ea.getNutitulosPendientesDesglose().subtract(dct.getImtitulos()));
      }
    }
    save(ejealos);
  }

  /**
   * @param aloId
   */
  public void recalcularTitulosPendientesDesglose(Tmct0aloId aloId) {
    List<Tmct0ejecucionalocation> ejealos = findAllByAlloId(aloId);
    List<Tmct0desgloseclitit> dcts;
    for (Tmct0ejecucionalocation ea : ejealos) {
      ea.setNutitulosPendientesDesglose(ea.getNutitulos());
      ea.setAuditFechaCambio(new Date());
      dcts = dctBo.findAllByNuordenAndNbookingAndNucnfcltAndNuejecuc(ea.getTmct0grupoejecucion().getTmct0alo().getId()
          .getNuorden(), ea.getTmct0grupoejecucion().getTmct0alo().getId().getNbooking(), ea.getTmct0grupoejecucion()
          .getTmct0alo().getId().getNucnfclt(), ea.getTmct0eje().getId().getNuejecuc());

      for (Tmct0desgloseclitit dct : dcts) {
        ea.setNutitulosPendientesDesglose(ea.getNutitulosPendientesDesglose().subtract(dct.getImtitulos()));
      }
    }
    save(ejealos);
  }
}// public class Tmct0ejecucionalocationBo extends
// AbstractBo<Tmct0ejecucionalocation, Long, Tmct0ejecucionalocationDao>
// {
