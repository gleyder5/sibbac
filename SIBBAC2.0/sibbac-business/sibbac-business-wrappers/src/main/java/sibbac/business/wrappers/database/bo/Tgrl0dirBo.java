package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.wrappers.database.dao.Tgrl0dirDao;
import sibbac.business.wrappers.database.dao.Tgrl0nomDao;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0dir;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0dirId;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0eje;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ejeId;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0nom;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0nomId;

@Service
public class Tgrl0dirBo extends AbstractBo<Tgrl0dir, Tgrl0dirId, Tgrl0dirDao> {

  private static final Logger LOG = LoggerFactory.getLogger(Tgrl0dirBo.class);

  @Autowired
  private Tmct0dbsBo dbsBo;

  @Autowired
  private Tgrl0nomDao nomDao;

  @Autowired
  private Tgrl0ejeBo ejeBo;

  public Tgrl0dirBo() {
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public short grabarTitulares(Tmct0alo alo, List<Holder> listHolders) throws SIBBACBusinessException {
    LOG.trace("[grabarTitulares] buscando mapeo desglose en tablas AS400(tgrl0eje): {}...", alo.getId());
    Tgrl0eje eje = ejeBo.findById(new Tgrl0ejeId(alo.getNuoprout(), alo.getNupareje()));
    if (eje != null) {
      return grabarTitulares(eje, listHolders);
    }
    else {
      throw new SIBBACBusinessException("Tablas del AS400(tgrl0eje) no estan inicializadas.");
    }
  }

  @Transactional(propagation = Propagation.REQUIRED)
  public short grabarTitulares(Tgrl0eje eje, List<Holder> listHolders) throws SIBBACBusinessException {
    Tgrl0nom titular;
    Tgrl0dirId idDireccion;
    if (CollectionUtils.isNotEmpty(listHolders)) {
      LOG.trace("[grabarTitulares] buscando titulares antiguos id: {}...", eje.getId());
      List<Tgrl0dir> direccionesAntiguas = dao.findAllByNuoproutAndNuparejeAndActivo(eje.getId().getNuoprout(), eje
          .getId().getNupareje());
      short nusectit = 1;
      BigDecimal fecha = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));
      if (CollectionUtils.isNotEmpty(direccionesAntiguas)) {
        LOG.trace("[grabarTitulares] encontrados titulares antiguos id: {}, los damos de baja...", eje.getId());
        for (Tgrl0dir direccion : direccionesAntiguas) {
          direccion.setFhbajano(fecha);
          save(direccion);
          LOG.trace("[grabarTitulares] dado de baja direccion: {}...", direccion);
          LOG.trace("[grabarTitulares] insertando direccion en tmct0dbs...");
          dbsBo.insertRegister(direccion, true);
          LOG.trace("[grabarTitulares] insertado direccion en tmct0dbs...");
        }
      }
      LOG.trace("[grabarTitulares] buscando nuevo nusecnbr id: {}...", eje.getId());
      Short nusecnbr = dao.maxNusecnbrByNuoproutAndNupareje(eje.getId().getNuoprout(), eje.getId().getNupareje());
      if (nusecnbr == null) {
        nusecnbr = 0;
      }
      nusecnbr++;
      idDireccion = new Tgrl0dirId(eje.getId().getNuoprout(), eje.getId().getNupareje(), nusecnbr);
      LOG.trace("[grabarTitulares] insertando direccion id: {}...", idDireccion);
      Tgrl0dir direccion = new Tgrl0dir();
      direccion.setId(idDireccion);
      direccion.setTgrl0eje(eje);
      Holder principal = listHolders.get(0);
      this.loadData(principal, direccion);
      direccion.setFhbajano(BigDecimal.ZERO);
      direccion.setFhentsvb(fecha);
      direccion.setFhenvbol(BigDecimal.ZERO);
      save(direccion);
      LOG.trace("[grabarTitulares] insertada direccion: {}...", direccion);

      LOG.trace("[grabarTitulares] insertando direccion en tmct0dbs...");
      dbsBo.insertRegister(direccion, true);
      LOG.trace("[grabarTitulares] insertado direccion en tmct0dbs...");
      for (Holder holder : listHolders) {
        LOG.trace("[grabarTitulares] insertado titular: {}...", holder);
        titular = new Tgrl0nom();
        titular.setId(new Tgrl0nomId(eje.getId().getNuoprout(), eje.getId().getNupareje(), nusecnbr, nusectit++));
        titular.setDireccion(direccion);
        this.loadData(holder, titular);
        nomDao.save(titular);
        LOG.trace("[grabarTitulares] insertado titular: {}...", titular);
        LOG.trace("[grabarTitulares] insertando titular en tmct0dbs...");
        dbsBo.insertRegister(titular, true);
        LOG.trace("[grabarTitulares] insertado titular en tmct0dbs...");
      }

      return nusecnbr;
    }
    else {
      return -1;
    }

  }

  /**
   * Genera datos Fiscales nuevos(Tmct0afi)
   * 
   * @param tmct0alc desglose a cargar datos fiscales
   * @param holder holder
   * @param hpm persistent
   * @throws SIBBACBusinessException
   */
  private void loadData(Holder holder, Tgrl0dir direccion) throws SIBBACBusinessException {

    // Datos
    direccion.setCddepais(holder.getCddepais().trim());
    direccion.setCdpostal(holder.getCdpostal().trim());
    direccion.setNbciudad(holder.getNbciudad().trim());
    direccion.setNbdomici(holder.getNbdomici().trim());
    direccion.setNbprovin(holder.getNbprovin().trim());
    direccion.setNudomici(holder.getNudomici().trim());
    direccion.setTpdomici(holder.getTpdomici().trim());
    direccion.setTpproced(holder.getTpproced());

  }

  /**
   * Genera datos Fiscales nuevos(Tmct0afi)
   * 
   * @param tmct0alc desglose a cargar datos fiscales
   * @param holder holder
   * @param hpm persistent
   */
  private void loadData(Holder holder, Tgrl0nom titular) {

    // Datos
    titular.setCdnactit(holder.getCdnactit().trim());
    titular.setNbclient(holder.getNbclient().trim());
    titular.setNbclien1(holder.getNbclien1().trim());
    titular.setNbclien2(holder.getNbclien2().trim());
    titular.setNudnicif(holder.getNudnicif().trim());
    titular.setTptiprep(holder.getTptiprep());

    titular.setTpidenti(holder.getTpidenti());
    titular.setCdddebic(holder.getCdddebic());
    titular.setTpsocied(holder.getTpsocied());
    titular.setTpnactit(holder.getTpnactit());

  }

}
