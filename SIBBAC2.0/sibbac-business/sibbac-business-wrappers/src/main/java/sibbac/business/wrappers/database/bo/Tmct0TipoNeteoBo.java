package sibbac.business.wrappers.database.bo;


import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0TipoNeteoDao;
import sibbac.business.wrappers.database.data.TipoNeteoData;
import sibbac.business.wrappers.database.model.Tmct0TipoNeteo;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0TipoNeteoBo extends AbstractBo< Tmct0TipoNeteo, Long, Tmct0TipoNeteoDao > {

	public List< TipoNeteoData > findAllData() {
		List< TipoNeteoData > resltList = new LinkedList< TipoNeteoData >();
		resltList.addAll( dao.findAllData() );
		return resltList;
	}
}
