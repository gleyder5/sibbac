package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.EtiquetasBo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceEtiquetas implements SIBBACServiceBean {

	@Autowired
	private EtiquetasBo etiquetasBo;

	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceEtiquetas.class);

	public SIBBACServiceEtiquetas() {
		LOG.trace("[SIBBACServiceEtiquetas::<constructor>] Initiating SIBBAC Service for \"Etiquetas\".");
	}

	@Override
	public WebResponse process(WebRequest webRequest) {

		LOG.trace("[SIBBACServiceEtiquetas::process] Processing a web request...");

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// obtiene comandos(paths)
		LOG.trace("[SIBBACServiceEtiquetas::process] Command.: [{}]", webRequest.getAction());

		// selecciona la accion
		switch (webRequest.getAction()) {

		case Constantes.CMD_GET_ETIQUETAS:

			// Hace el set de la devolucion de todos los identificador de la
			// tabla identificador
			webResponse.setResultados(etiquetasBo.getListaEtiquetas());
			LOG.trace("Rellenado el WebResponse con la lista de Etiquetas");

			break;

		default:

			webResponse.setError("No le llega ningun comando correcto");
			LOG.error("No le llega ningun comando correcto");

			break;

		}

		return webResponse;
	}

	@Override
	public List<String> getAvailableCommands() {
		List<String> commands = new ArrayList<String>();
		commands.add(Constantes.CMD_GET_ETIQUETAS);

		return commands;
	}

	@Override
	public List<String> getFields() {
		return Arrays.asList(new String[] { "id", "etiqueta", "tabla", "campo" });
	}

}
