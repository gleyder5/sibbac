package sibbac.business.wrappers.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.dao.Tmct0aliDao;
import sibbac.business.fase0.database.dto.Tmct0actDTO;
import sibbac.business.fase0.database.dto.Tmct0aliDTO;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0aliId;
import sibbac.business.wrappers.common.DTOUtils;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0CompensadorBo;
import sibbac.business.wrappers.database.bo.Tmct0CuentasDeCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0IndicadorDeCapacidadBo;
import sibbac.business.wrappers.database.bo.Tmct0ModeloDeNegocioBo;
import sibbac.business.wrappers.database.bo.Tmct0NemotecnicosBo;
import sibbac.business.wrappers.database.bo.Tmct0ReglaBo;
import sibbac.business.wrappers.database.bo.Tmct0SegmentoBo;
import sibbac.business.wrappers.database.bo.Tmct0parametrizacionBo;
import sibbac.business.wrappers.database.dao.FestivoDao;
import sibbac.business.wrappers.database.dao.Tmct0parametrizacionDao;
import sibbac.business.wrappers.database.data.CompensadorData;
import sibbac.business.wrappers.database.data.CuentaDeCompensacionData;
import sibbac.business.wrappers.database.dto.GestionReglasDTO;
import sibbac.business.wrappers.database.dto.Tmct0IndicadorDeCapacidadDTO;
import sibbac.business.wrappers.database.dto.Tmct0ModeloDeNegocioDTO;
import sibbac.business.wrappers.database.dto.Tmct0NemotecnicosDTO;
import sibbac.business.wrappers.database.dto.Tmct0SegmentoDTO;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.model.Festivo;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0IndicadorDeCapacidad;
import sibbac.business.wrappers.database.model.Tmct0ModeloDeNegocio;
import sibbac.business.wrappers.database.model.Tmct0Nemotecnicos;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0Segmento;
import sibbac.business.wrappers.database.model.Tmct0parametrizacion;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
@Transactional
public class SIBBACServiceGestionReglas implements SIBBACServiceBean {
  private static DateFormat dF = new SimpleDateFormat("dd/MM/yyyy");

  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

  @Autowired
  private Tmct0parametrizacionDao tmct0parametrizacionDao;

  @Autowired
  private Tmct0ReglaBo tmct0ReglaBo;

  @Autowired
  private Tmct0NemotecnicosBo tmct0NemotecnicosBo;

  @Autowired
  private Tmct0IndicadorDeCapacidadBo tmct0IndicadorDeCapacidadBo;

  @Autowired
  private Tmct0ModeloDeNegocioBo tmct0ModeloDeNegocioBo;

  @Autowired
  private Tmct0SegmentoBo tmct0SegmentoBo;

  @Autowired
  private Tmct0parametrizacionBo tmct0parametrizacionBo;

  @Autowired
  private Tmct0CamaraCompensacionBo tmct0CamaraCompensacionBo;

  @Autowired
  private Tmct0CuentasDeCompensacionBo tmct0TipoCuentaDeCompensacionBo;

  @Autowired
  private Tmct0CompensadorBo tmct0CompensadorBo;

  @Autowired
  private Tmct0actBo tmct0actBo;

  @Autowired
  private Tmct0aliBo tmct0aliBo;

  @Autowired
  private Tmct0aliDao tmct0aliDao;

  @Autowired
  private FestivoDao festivoDao;

  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGestionReglas.class);
  private static final String NOMBRE_SERVICIO = "SIBBACServiceGestionReglas";
  private static final String CMD_INFORME_LIST = "getListRules";
  private static final String CMD_NEMOTECNICOS_LIST = "getListNemotecnicos";
  private static final String CMD_INDICADOR_DE_CAPACIDAD_LIST = "getListCapacidad";
  private static final String CMD_MODELO_DE_NEGOCIO_LIST = "getListModeloDeNegocio";
  private static final String CMD_SEGMENTO_LIST = "getListSegmento";
  private static final String CMD_PARAMETRIZACION_BY_ID = "getParametrizacionesById";
  private static final String CMD_MODIFICAR_REGLA = "getModificarRegla";
  private static final String CMD_CAMARA_COMPENSACION_LIST = "getCamaraCompensacionList";
  private static final String CMD_CODIGO_LIST = "getCodigoList";
  private static final String CMD_IMPORTAR_REGLA = "importarRegla";

  private static final String CMD_CUENTAS_COMPENSACION_LIST = "getCuentasCompensacionList";
  private static final String CMD_MIEMBROS_COMPENSACION_LIST = "getMiembrosCompensacionList";
  private static final String CMD_MODIFICAR_PARAMETRIZACION = "getModificarParametrizacion";
  private static final String CMD_PARAMETRIZACION_ACTIVA_BYIDREGLA = "getParametrizacionesActivasByIdRegla";
  private static final String CMD_REGLAS_FILTRO_LIST = "getReglasPorFiltros";
  private static final String CMD_PARAMETRIZACIONES_LIST = "getParametrizacionesList";
  private static final String CMD_ALTA_REGLA = "getAltaRegla";
  private static final String CMD_ALTA_PARAMETRIZACION = "getAltaParametrizaciones";

  private static final String CMD_ELIMINAR_REGLA = "getEliminarRegla";

  private static final String CMD_ASIGNAR_ALIAS = "getAsignarAlias";
  private static final String CMD_ASIGNAR_SUBCTA = "getAsignarSubCta";

  private static final String CMD_EXPORTAR_PARAMETRIZACION = "getExportarParam";

  private static final String CMD_ALIAS_LIST = "getAliasList";
  private static final String CMD_SUBCTA_LIST = "getSubCtaList";

  private static final String FILTER_IDREGLA = "idRegla";
  private static final String FILTER_CDCODIGO = "cdCodigo";
  private static final String FILTER_DESCRIPCION = "descripcion";
  private static final String FILTER_FECHA_CREACION = "fecha_creacion";
  private static final String FILTER_REGLA_ACTIVA = "regla_activa";
  private static final String FILTER_FECHA_INICIO = "fecha_inicio";
  private static final String FILTER_FECHA_FIN = "fecha_fin";
  private static final String FILTER_REFERENCIA_CLIENTE = "referenciaCliente";
  private static final String FILTER_REFERENCIA_EXTERNA = "referenciaExterna";
  private static final String FILTER_CAMARA_COMPENSACION = "camaraCompensacion";
  private static final String FILTER_CUENTA_COMPENSACION = "cuentaCompensacion";
  private static final String FILTER_IDPARAMETRIZACION = "idParametrizacion";
  private static final String FILTER_MIEMBRO_COMPENSADOR = "miembroCompensador";
  private static final String FILTER_REFERENCIA_ASIGNACION = "referenciaAsignacion";
  private static final String FILTER_SEGMENTO = "segmento";
  private static final String FILTER_NEMOTECNICO = "nemotecnico";
  private static final String FILTER_CAPACIDAD = "capacidad";
  private static final String FILTER_MODELO_NEGOCIO = "modeloNegocio";
  private static final String FILTER_ESTADO = "estado";
  private static final String FILTER_ALIAS = "alias";
  private static final String FILTER_SUBCTA = "subCta";
  private static final String FILTER_FILE = "file";
  private static final String FILTER_FILENAME = "filename";

  private static final String ACTUALIZAR_ACTIVO = "getActualizarActivo";

  private static final String DESCRIPCION_PARAMETRIZACION = "descripcionParametrizacion";

  private static final String ID_CAMARA_COMPENSACION = "IdCamaraCompensacion";
  private static final String CAMARA_COMPENSACION_CODIGO = "codigoCamaraComp";
  private static final String CAMARA_COMPENSACION_DESCRIPCION = "descripcionCamaraComp";

  private static final String ID_CUENTA_COMPENSACION = "IdCuentaCompensacion";
  private static final String CUENTA_COMPENSACION_CDCODIGO = "ctaCompensacionCdCodigo";
  private static final String ID_MIEMBRO_COMPENSADOR = "IdMiembroCompensador";

  private static final String ID_NEMOTECNICO = "IdNemotecnico";
  private static final String NEMOTECNICO_NBNOMBRE = "nemotecnicoNbNombre";

  private static final String ID_SEGMENTO = "IdSegmento";
  private static final String SEGMENTO_CDCODIGO = "segmentoCdCodigo";
  private static final String SEGMENTO_NBDESCRIPCION = "segmentoNbDescripcion";

  private static final String ID_CAPACIDAD = "IdCapacidad";
  private static final String CAPACIDAD_CDCODIGO = "capacidadCdCodigo";
  private static final String CAPACIDAD_DESCRIPCION = "capacidadDescripcion";

  private static final String ID_MODELO_NEGOCIO = "IdModeloNegocio";
  private static final String MODELO_NEGOCIO_CDCODIGO = "modeloNegocioCdCodigo";
  private static final String MODELO_NEGOCIO_NBDESCRIPCION = "modeloNegocioNbDescripcion";

  private static final String REGLA_AND_PARAMETRIZACION = "parametrizacion";
  private static final String PARAMETRIZACION_BY_REGLA = "parametrizacionesByRegla";
  private static final String PARAMETRIZACION_BY_IDPARAM = "parametrizacionesByIdParam";
  private static final String PARAMETRIZACION_ELIMINAR = "getEliminarParametrizacion";

  private static final String RETURN_ASIGNAR_ALIAS = "resultado_asignacionAlias";
  private static final String RETURN_ASIGNAR_SUBCTA = "resultado_asignacionSubCta";

  private static final String RESULT_PARAMETRIZACION_LIST_EXPORT = "listaParametrizacionExport";
  private static final String RESULT_ALIAS_LIST_EXPORT = "listaAliasExport";
  private static final String RESULT_SUBCTA_LIST_EXPORT = "listaSubCtaExport";
  private static final String RETURN_IMPORT_FILE = "resultado_importarRegla";

  private static final String FOLDER_DESTINO = "/sbrmv/ficheros/temporal/";

  private static String cdCodigo;
  private static Long camaraCompensacion;
  private static Long cuentaCompensacion;
  private static Long miembroCompensador;
  private static String referenciaAsignacion;
  private static Long nemotecnico;
  private static String referenciaCliente;
  private static String referenciaExterna;
  private static Long segmento;
  private static Long capacidad;
  private static Long modeloNegocio;
  private static String estado;
  private static String alias;
  private static String subCta;

  public SIBBACServiceGestionReglas() {

  }

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
    final String prefix = "[SIBBACServiceGestionReglas::process] ";
    LOG.debug(prefix + "Processing a web request...");

    final String command = webRequest.getAction();
    LOG.debug(prefix + "+ Command.: [{}]", command);

    final WebResponse webResponse = new WebResponse();

    switch (command) {
      case CMD_CODIGO_LIST:
        this.getCodigoList(webRequest, webResponse);
        break;
      case CMD_NEMOTECNICOS_LIST:
        this.getListNemotecnicos(webRequest, webResponse);
        break;
      case CMD_INDICADOR_DE_CAPACIDAD_LIST:
        this.getListCapacidad(webRequest, webResponse);
        break;
      case CMD_MODELO_DE_NEGOCIO_LIST:
        this.getListModeloDeNegocio(webRequest, webResponse);
        break;
      case CMD_SEGMENTO_LIST:
        this.getListSegmento(webRequest, webResponse);
        break;
      case CMD_PARAMETRIZACION_BY_ID:
        this.getParametrizacionesById(webRequest, webResponse);
        break;
      case CMD_MODIFICAR_REGLA:
        this.getModificarRegla(webRequest, webResponse);
        break;
      case CMD_CAMARA_COMPENSACION_LIST:
        this.getCamaraCompensacionList(webRequest, webResponse);
        break;
      case CMD_CUENTAS_COMPENSACION_LIST:
        this.getCuentasCompensacionList(webRequest, webResponse);
        break;
      case CMD_MIEMBROS_COMPENSACION_LIST:
        this.getMiembrosCompensacionList(webRequest, webResponse);
        break;
      case CMD_MODIFICAR_PARAMETRIZACION:
        this.getModificarParametrizacion(webRequest, webResponse);
        break;
      case CMD_PARAMETRIZACION_ACTIVA_BYIDREGLA:
        this.getParametrizacionesActivasByIdRegla(webRequest, webResponse);
        break;
      case CMD_REGLAS_FILTRO_LIST:
        this.getReglasPorFiltros(webRequest, webResponse);
        break;
      case CMD_PARAMETRIZACIONES_LIST:
        this.getParametrizacionesList(webRequest, webResponse);
        break;
      case CMD_ALTA_REGLA:
        this.getAltaRegla(webRequest, webResponse);
        break;
      case CMD_ALTA_PARAMETRIZACION:
        this.getAltaParametrizaciones(webRequest, webResponse);
        break;
      case CMD_ELIMINAR_REGLA:
        this.getEliminarRegla(webRequest, webResponse);
        break;
      case PARAMETRIZACION_ELIMINAR:
        this.getEliminarParametrizacion(webRequest, webResponse);
        break;
      case PARAMETRIZACION_BY_IDPARAM:
        this.parametrizacionesByIdParam(webRequest, webResponse);
        break;
      case ACTUALIZAR_ACTIVO:
        this.getActualizarActivo(webRequest, webResponse);
        break;
      case CMD_ASIGNAR_ALIAS:
        this.getAsignarAlias(webRequest, webResponse);
        break;
      case CMD_ASIGNAR_SUBCTA:
        this.getAsignarSubCta(webRequest, webResponse);
        break;
      case CMD_EXPORTAR_PARAMETRIZACION:
        this.getExportarParam(webRequest, webResponse);
        break;
      case CMD_ALIAS_LIST:
        this.getAliasList(webRequest, webResponse);
        break;
      case CMD_SUBCTA_LIST:
        this.getSubCtaList(webRequest, webResponse);
        break;
      case CMD_IMPORTAR_REGLA:
        this.importarRegla(webRequest, webResponse);
        break;
      default:
        webResponse.setError(command + " is not a valid action. Valid actions are: " + this.getAvailableCommands());
        break;

    }
    return webResponse;

  }

  private void getActualizarActivo(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getActualizarActivo] Init");
    Map<String, String> filters = webRequest.getFilters();

    final BigInteger idRegla = new BigInteger((filters.get(FILTER_IDREGLA)));
    final String activo = (filters.get(FILTER_REGLA_ACTIVA));
    Map<String, String> resultados = new Hashtable<String, String>();

    String result;

    result = tmct0ReglaBo.updateActivoRegla(idRegla, activo);

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getActualizarActivo] Terminando update a la tabla TMCT0_REGLA. ");
    resultados.put("resultado_modificado", result);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getActualizarActivo] Fin ");

  }

  @Transactional
  private void getEliminarParametrizacion(WebRequest webRequest, WebResponse webResponse)
      throws SIBBACBusinessException {
    Map<String, String> resultados = new HashMap<String, String>();

    Map<String, String> filters = webRequest.getFilters();

    final Long idParametrizacion = Long.parseLong(filters.get(FILTER_IDPARAMETRIZACION));

    Tmct0parametrizacion id_parametrizacion = tmct0parametrizacionBo.findById(idParametrizacion);

    if (id_parametrizacion != null) {
      tmct0parametrizacionBo.delete(id_parametrizacion);
    }

    resultados.put(CMD_ELIMINAR_REGLA, "Se han eliminado correctamente");
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getEliminarRegla] Fin ");

  }

  private void getEliminarRegla(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {
    Map<String, String> resultados = new HashMap<String, String>();
    List<Map<String, String>> params = webRequest.getParams();

    Map registro = null;
    BigInteger idRegla = null;
    String estado = null;
    Integer nCont = 0;

    Iterator it = params.iterator();
    while (it.hasNext()) {
      registro = (Map) it.next();
      idRegla = new BigInteger((String) registro.get("idRegla"));
      estado = (String) registro.get("estado");
      tmct0ReglaBo.updateActivoRegla(idRegla, estado);
      nCont++;
    }
    if (nCont > 1) {
      resultados.put(CMD_ELIMINAR_REGLA, "Se han activado/desactivado " + nCont.toString() + " reglas correctamente");
    }
    else {
      resultados.put(CMD_ELIMINAR_REGLA, "Se ha activado/desactivado la regla correctamente");
    }

    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getEliminarRegla] Fin ");

  }

  @Transactional
  private void getAltaParametrizaciones(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filters = webRequest.getFilters();
    Map<String, String> resultado = new HashMap<String, String>();

    final BigInteger idRegla = new BigInteger(filters.get(FILTER_IDREGLA));

    java.util.Date fechaSistema = new Date();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getAltaRegla] Se inicia la creacion del objeto Regla");

    Tmct0parametrizacion parametrizacion = new Tmct0parametrizacion();

    Tmct0Regla id_regla = tmct0ReglaBo.findIdRegla(idRegla);
    parametrizacion.setTmct0Regla(id_regla);

    parametrizacion.setDescripcion("descripcion1");

    try {
      parametrizacion.setTmct0CamaraCompensacion(tmct0CamaraCompensacionBo.findById(Long.parseLong(filters.get(
          "camaraCompensacion1").trim())));
    }
    catch (NumberFormatException e) {
    }

    try {
      parametrizacion.setTmct0Nemotecnicos(tmct0NemotecnicosBo.findById(Long.parseLong(filters.get("nemotecnico1")
          .trim())));
    }
    catch (NumberFormatException e) {
    }

    try {
      parametrizacion.setTmct0CuentasDeCompensacion(tmct0TipoCuentaDeCompensacionBo.findById(Long.parseLong(filters
          .get("cuentaCompensacion1").trim())));
    }
    catch (NumberFormatException e) {
    }

    try {
      parametrizacion.setTmct0Segmento(tmct0SegmentoBo.findById(Integer.parseInt(filters.get("segmento1").trim())));
    }
    catch (NumberFormatException e) {
    }

    try {
      parametrizacion.setTmct0IndicadorCapacidad(tmct0IndicadorDeCapacidadBo.findById(Long.parseLong(filters.get(
          "capacidad1").trim())));
    }
    catch (NumberFormatException e) {
    }

    try {
      parametrizacion.setTmct0Compensador(tmct0CompensadorBo.findById(Long.parseLong(filters.get("miembroCompensador1")
          .trim())));
    }
    catch (NumberFormatException e) {
    }

    parametrizacion.setReferenciaCliente(filters.get("referenciaCliente1"));

    try {
      parametrizacion.setTmct0ModeloDeNegocio(tmct0ModeloDeNegocioBo.findById(Long.parseLong(filters.get(
          "modeloNegocio1").trim())));
    }
    catch (NumberFormatException e) {
    }

    parametrizacion.setCdReferenciaAsignacion(filters.get("referenciaAsignacion1"));

    parametrizacion.setReferenciaExterna(filters.get("referenciaExterna1"));

    Date fechaInicio = null;
    Date fechaFin = null;

    if (filters.get("fecha_fin") != null && filters.get("fecha_fin") != "") {
      try {
        fechaFin = dF.parse(filters.get(FILTER_FECHA_FIN));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }

      parametrizacion.setFecha_fin(fechaFin);
    }
    if (filters.get("fecha_inicio") != null && filters.get("fecha_inicio") != "") {
      try {
        fechaInicio = dF.parse(filters.get(FILTER_FECHA_INICIO));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }
      parametrizacion.setFecha_inicio(fechaInicio);
    }
    else {
      java.util.Date fechaInicioMenosUnDia = getDiaHabil(sumarRestarDiasFecha(fechaSistema, -1));
      parametrizacion.setFecha_inicio(fechaInicioMenosUnDia);
    }
    parametrizacion.setActivo("1");
    parametrizacion.setAuditFechaCambio(fechaSistema);
    parametrizacion.setAuditUsuario("");

    if (parametrizacion != null) {

      parametrizacion = tmct0parametrizacionBo.save(parametrizacion);

      LOG.debug("Se ha insertado correctamente la nueva parametrizacion");
    }

    parametrizacion = tmct0parametrizacionBo.save(parametrizacion);

    LOG.debug("Se ha insertado correctamente la nueva parametrización");

    resultado.put(CMD_ALTA_PARAMETRIZACION, "El registro ha sido insertado correctamente");
    webResponse.setResultados(resultado);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getAltaRegla] Fin ");
  }

  @Transactional
  private void getAltaRegla(WebRequest webRequest, WebResponse webResponse) {

    try {
      Map<String, String> filters = webRequest.getFilters();
      Map<String, String> resultado = new HashMap<String, String>();
      List<Tmct0Regla> listReglas = new ArrayList<Tmct0Regla>();
      java.util.Date fechaMenosUnDia = getDiaHabil(sumarRestarDiasFecha(new Date(), -1));

      LOG.debug("[" + NOMBRE_SERVICIO + " :: getAltaRegla] Se inicia la creacion del objeto Regla");
      Tmct0Regla regla = new Tmct0Regla();
      Tmct0Regla reg1 = new Tmct0Regla();

      listReglas = tmct0ReglaBo.findAll();

      reg1 = listReglas.get(listReglas.size() - 1);
      BigInteger idReglaLast = reg1.getIdRegla();
      BigInteger adicion = new BigInteger("1");
      BigInteger idRegla = new BigInteger(idReglaLast.add(adicion).toString());

      Tmct0Regla re = tmct0ReglaBo.findIdRegla(idRegla);

      if (re == null) {
        regla.setCdCodigo(filters.get(FILTER_CDCODIGO));
        regla.setNbDescripcion(filters.get(FILTER_DESCRIPCION));
        regla.setFechaCreacion(fechaMenosUnDia);
        regla.setAuditFechaCambio(new Date());
        regla.setAuditUser("");
        regla.setActivo("1");

      }

      if (re != null) {
        LOG.debug("[" + NOMBRE_SERVICIO + " :: getAltaRegla] La regla ya existe y no puede ser insertada");
      }
      else {
        try {
          tmct0ReglaBo.save(regla);
          resultado.put(CMD_ALTA_REGLA, "El registro ha sido insertado correctamente");
          webResponse.setResultados(resultado);
          LOG.debug("Se ha insertado correctamente la nueva regla");
        }
        catch (Exception ex) {
          LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
        }

      }

      LOG.debug("[" + NOMBRE_SERVICIO + " :: getAltaRegla] Fin ");
    }
    catch (Exception ex) {
      LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
    }
  }

  private void getParametrizacionesList(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getParametrizacionesList] Init");
    Map<String, String> filters = webRequest.getFilters();
    final BigInteger idRegla = new BigInteger(filters.get(FILTER_IDREGLA));

    final Map<String, List<Tmct0parametrizacionDTO>> resultados = new HashMap<String, List<Tmct0parametrizacionDTO>>();
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getParametrizacionesById] Comenzando consulta a TMCT0_PARAMETRIZACION");
    List<Tmct0parametrizacionDTO> listaParametrizacion = tmct0parametrizacionBo.findParametrizacionesByIdRegla(idRegla);
    LOG.debug("[NOMBRE_SERVICIO"
        + " :: getParametrizacionesById] Terminando consulta a TMCT0_PARAMETRIZACION. Obtenidos "
        + listaParametrizacion.size() + " registros");

    resultados.put("resultados_parametrizacion", listaParametrizacion);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getParametrizacionesById] Fin ");
  }

  private void getReglasPorFiltros(WebRequest webRequest, WebResponse webResponse) throws SIBBACBusinessException {

    cdCodigo = null;
    camaraCompensacion = null;
    cuentaCompensacion = null;
    miembroCompensador = null;
    referenciaAsignacion = null;
    nemotecnico = null;
    segmento = null;
    referenciaCliente = null;
    referenciaExterna = null;
    capacidad = null;
    modeloNegocio = null;
    estado = null;
    alias = null;
    subCta = null;

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getReglasPorFiltros] Init");
    Map<String, String> filters = new HashMap<String, String>();
    filters = webRequest.getFilters();

    final Map<String, List<GestionReglasDTO>> resultados = new HashMap<String, List<GestionReglasDTO>>();

    List<Tmct0parametrizacion> listaReglas = new ArrayList<Tmct0parametrizacion>();

    if (filters.get(FILTER_CDCODIGO) != null && filters.get(FILTER_CDCODIGO) != "") {

      cdCodigo = (String) filters.get(FILTER_CDCODIGO);

    }

    if (filters.get(FILTER_CAMARA_COMPENSACION) != null && filters.get(FILTER_CAMARA_COMPENSACION) != "") {

      camaraCompensacion = Long.parseLong(filters.get(FILTER_CAMARA_COMPENSACION));
    }

    if (filters.get(FILTER_CUENTA_COMPENSACION) != null && filters.get(FILTER_CUENTA_COMPENSACION) != "") {

      cuentaCompensacion = Long.parseLong(filters.get(FILTER_CUENTA_COMPENSACION));
    }

    if (filters.get(FILTER_MIEMBRO_COMPENSADOR) != null && filters.get(FILTER_MIEMBRO_COMPENSADOR) != "") {

      miembroCompensador = Long.parseLong(filters.get(FILTER_MIEMBRO_COMPENSADOR));
    }

    if (filters.get(FILTER_REFERENCIA_ASIGNACION) != null && filters.get(FILTER_REFERENCIA_ASIGNACION) != "") {

      referenciaAsignacion = (filters.get(FILTER_REFERENCIA_ASIGNACION));
    }

    if (filters.get(FILTER_NEMOTECNICO) != null && filters.get(FILTER_NEMOTECNICO) != "") {

      nemotecnico = Long.parseLong(filters.get(FILTER_NEMOTECNICO));
    }

    if (filters.get(FILTER_SEGMENTO) != null && filters.get(FILTER_SEGMENTO) != "") {

      segmento = Long.parseLong(filters.get(FILTER_SEGMENTO));
    }

    if (filters.get(FILTER_REFERENCIA_CLIENTE) != null && filters.get(FILTER_REFERENCIA_CLIENTE) != "") {

      referenciaCliente = (filters.get(FILTER_REFERENCIA_CLIENTE));
    }

    if (filters.get(FILTER_REFERENCIA_EXTERNA) != null && filters.get(FILTER_REFERENCIA_EXTERNA) != "") {

      referenciaExterna = (filters.get(FILTER_REFERENCIA_EXTERNA));
    }

    if (filters.get(FILTER_CAPACIDAD) != null && filters.get(FILTER_CAPACIDAD) != "") {

      capacidad = Long.parseLong(filters.get(FILTER_CAPACIDAD));
    }

    if (filters.get(FILTER_MODELO_NEGOCIO) != null && filters.get(FILTER_MODELO_NEGOCIO) != "") {

      modeloNegocio = Long.parseLong(filters.get(FILTER_MODELO_NEGOCIO));
    }

    if (filters.get(FILTER_ESTADO) != null && filters.get(FILTER_ESTADO) != "") {
      estado = (filters.get(FILTER_ESTADO));
    }

    if (StringUtils.isNotEmpty(filters.get(FILTER_ALIAS))) {
      alias = (filters.get(FILTER_ALIAS));

    }

    if (StringUtils.isNotEmpty(filters.get(FILTER_SUBCTA))) {
      subCta = (filters.get(FILTER_SUBCTA));

    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getReglasPorFiltros] Comenzando consulta a TMCT0_REGLA");

    List<GestionReglasDTO> listaGestionReglas = tmct0ReglaBo.findReglasFiltro(cdCodigo, camaraCompensacion,
        cuentaCompensacion, miembroCompensador, referenciaAsignacion, nemotecnico, segmento, referenciaCliente,
        referenciaExterna, capacidad, modeloNegocio, estado, alias, subCta);

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getReglasPorFiltros] Terminando consulta a TMCT0_REGLA. Obtenidos "
        + listaGestionReglas.size() + " registros");

    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getMiembrosCompensacionList] Terminando consulta a TMCT0_COMPENSADOR. Obtenidos " + listaReglas.size()
        + " registros");

    resultados.put(CMD_INFORME_LIST, listaGestionReglas);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getReglasPorFiltros] Fin ");

  }

  private void getMiembrosCompensacionList(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getMiembrosCompensacionList] Init");
    final Map<String, List<CompensadorData>> resultados = new HashMap<String, List<CompensadorData>>();
    List<CompensadorData> listaCompensadores = new ArrayList<CompensadorData>();
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getMiembrosCompensacionList] Comenzando consulta a TMCT0_COMPENSADOR");
    for (Tmct0Compensador gestionCompensadores : tmct0CompensadorBo.findAll()) {
      listaCompensadores.add(new CompensadorData(gestionCompensadores));
    }
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getMiembrosCompensacionList] Terminando consulta a TMCT0_COMPENSADOR. Obtenidos "
        + listaCompensadores.size() + " registros");

    resultados.put("compensador_list", listaCompensadores);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCuentasCompensacionList] Fin ");
  }

  /**
   * Método que devuelve una lista de cuentas de compensación.
   * 
   * @param webRequest
   * @param webResponse
   */

  private void getCuentasCompensacionList(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCuentasCompensacionList] Init");
    final Map<String, List<CuentaDeCompensacionData>> resultados = new HashMap<String, List<CuentaDeCompensacionData>>();
    List<CuentaDeCompensacionData> listaCuentaCompensacion = new ArrayList<CuentaDeCompensacionData>();
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getCuentasCompensacionList] Comenzando consulta a TMCT0_CUANTAS_DE_COMPENSACION");
    for (Tmct0CuentasDeCompensacion gestionCuentasCompensacion : tmct0TipoCuentaDeCompensacionBo.findAll()) {
      listaCuentaCompensacion.add(new CuentaDeCompensacionData(gestionCuentasCompensacion));
    }
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getCuentasCompensacionList] Terminando consulta a TMCT0_CUENTAS_DE_COMPENSACION. Obtenidos "
        + listaCuentaCompensacion.size() + " registros");

    resultados.put("cuentas_compensacion_list", listaCuentaCompensacion);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCuentasCompensacionList] Fin ");
  }

  /**
   * Método que devuelve una lista de cámara de compensación (CCP).
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getCamaraCompensacionList(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCamaraCompensacionList] Init");
    List<Tmct0CamaraCompensacion> camaraCompensacionList = new ArrayList<Tmct0CamaraCompensacion>();
    try {
      camaraCompensacionList = tmct0CamaraCompensacionBo.findAll();
    }
    catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getCamaraCompensacionList] Error obteniendo resultados de la tabla ");
    }
    final Map<String, List<Tmct0CamaraCompensacion>> resultados = new HashMap<String, List<Tmct0CamaraCompensacion>>();
    resultados.put("resultados_CCP", camaraCompensacionList);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCamaraCompensacionList] Fin");
  }

  private void getCamaraCompensacionById(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCamaraCompensacionList] Init");

    Map<String, String> filters = webRequest.getFilters();

    final Long idCamara = Long.parseLong(filters.get(ID_CAMARA_COMPENSACION));
    final Map<String, Tmct0CamaraCompensacion> resultados = new HashMap<String, Tmct0CamaraCompensacion>();

    Tmct0CamaraCompensacion camaraCompensacion = new Tmct0CamaraCompensacion();
    try {
      camaraCompensacion = tmct0CamaraCompensacionBo.findById(idCamara);
    }
    catch (Exception e) {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getCamaraCompensacionList] Error obteniendo resultados de la tabla ");
    }

    resultados.put("resultados", camaraCompensacion);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCamaraCompensacionList] Fin");
  }

  /**
   * Servicio que devuelve una lista con los operaciones existentes.
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getModificarRegla(WebRequest webRequest, WebResponse webResponse) {

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarRegla] Init");
    Map<String, String> filters = webRequest.getFilters();

    final BigInteger idRegla = new BigInteger(filters.get(FILTER_IDREGLA));
    final Map<String, String> resultados = new HashMap<String, String>();
    try {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarRegla] Comenzando update a TMCT0_REGLA");
      Tmct0Regla regla = tmct0ReglaBo.findIdRegla(idRegla);

      regla.setCdCodigo(filters.get("cdCodigo"));
      regla.setNbDescripcion(filters.get("descripcion"));

      Date fecha1 = null;
      try {
        fecha1 = dF.parse(filters.get("fecha_creacion"));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }

      java.util.Date fechaMenosUnDia = getDiaHabil(fecha1);

      regla.setFechaCreacion(fechaMenosUnDia);

      if (regla != null) {
        regla = tmct0ReglaBo.save(regla);
        LOG.debug("Se ha modificado la regla");
      }

      LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarRegla] Terminando update a la tabla TMCT0_REGLA. ");
      resultados.put("resultado_modificado", "Actualización realizada con éxito");
      webResponse.setResultados(resultados);
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarRegla] Fin ");
    }
    catch (Exception ex) {
      LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
    }
  }

  private void getModificarParametrizacion(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarParametrizacion] Init");
    Map<String, String> filters = webRequest.getFilters();

    final Long idParametrizacion = Long.parseLong(filters.get(FILTER_IDPARAMETRIZACION));
    final Map<String, Boolean> resultados = new HashMap<String, Boolean>();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarParametrizacion] Comenzando update a TMCT0_PARAMETRIZACION");

    Tmct0parametrizacion parametrizacion = tmct0parametrizacionBo.findById(idParametrizacion);

    if (filters.get("descripcionParametrizacion") != null && filters.get("descripcionParametrizacion") != "") {
      parametrizacion.setDescripcion(filters.get("descripcionParametrizacion"));
    }

    if (filters.get("idCamaraCompensacion") != null && filters.get("idCamaraCompensacion") != "") {
      Long idCamara = Long.parseLong(filters.get("idCamaraCompensacion").trim());
      Tmct0CamaraCompensacion camaraCompensacion = tmct0CamaraCompensacionBo.findById(idCamara);
      parametrizacion.setTmct0CamaraCompensacion(camaraCompensacion);
    }

    if (filters.get("idCuentaCompensacion") != null && filters.get("idCuentaCompensacion") != "") {
      Long idCuentaCompensac = Long.parseLong(filters.get("idCuentaCompensacion").trim());
      Tmct0CuentasDeCompensacion idCuentaCompensacion = tmct0TipoCuentaDeCompensacionBo.findById(idCuentaCompensac);
      parametrizacion.setTmct0CuentasDeCompensacion(idCuentaCompensacion);
    }

    if (filters.get("idCompensador") != null && filters.get("idCompensador") != "") {
      Long idCompensad = Long.parseLong(filters.get("idCompensador").trim());
      Tmct0Compensador idCompensador = tmct0CompensadorBo.findById(idCompensad);
      parametrizacion.setTmct0Compensador(idCompensador);
    }

    if (filters.get("referenciaAsignacion") != null && filters.get("referenciaAsignacion") != "") {
      parametrizacion.setCdReferenciaAsignacion(filters.get("referenciaAsignacion"));
    }

    if (filters.get("id_nemotecnico") != null && filters.get("id_nemotecnico") != "") {
      Long idNemotecnico = Long.parseLong(filters.get("id_nemotecnico").trim());
      Tmct0Nemotecnicos nemotecnico = tmct0NemotecnicosBo.findById(idNemotecnico);
      parametrizacion.setTmct0Nemotecnicos(nemotecnico);
    }

    if (filters.get("id_segmento") != null && filters.get("id_segmento") != "") {
      int id_segment = Integer.parseInt(filters.get("id_segmento").trim());
      Tmct0Segmento id_segmento = tmct0SegmentoBo.findById(id_segment);
      parametrizacion.setTmct0Segmento(id_segmento);
    }

    if (filters.get("referenciaCliente") != null && filters.get("referenciaCliente") != "") {
      parametrizacion.setReferenciaCliente(filters.get("referenciaCliente"));
    }

    if (filters.get("referenciaExterna") != null && filters.get("referenciaExterna") != "") {
      parametrizacion.setReferenciaExterna(filters.get("referenciaExterna"));
    }

    if (filters.get("id_indicador_capacidad") != null && filters.get("id_indicador_capacidad") != "") {
      Long idIndicadorCapacid = Long.parseLong(filters.get("id_indicador_capacidad").trim());
      Tmct0IndicadorDeCapacidad id_indicador_capacidad = tmct0IndicadorDeCapacidadBo.findById(idIndicadorCapacid);
      parametrizacion.setTmct0IndicadorCapacidad(id_indicador_capacidad);
    }

    if (filters.get("id_modelo_de_negocio") != null && filters.get("id_modelo_de_negocio") != "") {
      Long idModeloNegoci = Long.parseLong(filters.get("id_modelo_de_negocio").trim());
      Tmct0ModeloDeNegocio id_modelo_de_negocio = tmct0ModeloDeNegocioBo.findById(idModeloNegoci);
      parametrizacion.setTmct0ModeloDeNegocio(id_modelo_de_negocio);
    }

    Date fecha1 = null;

    if (filters.get("fecha_fin") != null && filters.get("fecha_fin") != "") {
      try {
        fecha1 = dF.parse(filters.get("fecha_fin"));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }
      parametrizacion.setFecha_fin(fecha1);
    }
    else {
      parametrizacion.setFecha_fin(null);
    }

    Date fechaInicio = null;
    if (filters.get("fecha_inicio") != null && filters.get("fecha_inicio") != "") {
      try {
        fechaInicio = dF.parse(filters.get(FILTER_FECHA_INICIO));
      }
      catch (ParseException e) {
        LOG.error("No valida la fecha", e);
      }
      parametrizacion.setFecha_inicio(fechaInicio);
    }
    else {
      java.util.Date fechaInicioMenosUnDia = getDiaHabil(sumarRestarDiasFecha(new Date(), -1));
      parametrizacion.setFecha_inicio(fechaInicioMenosUnDia);
    }

    if (parametrizacion != null) {
      tmct0parametrizacionBo.save(parametrizacion);
      LOG.debug("Se ha modificado la parametrización");
    }

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarRegla] Terminando update a la tabla TMCT0_PARAMETRIZACION. ");
    resultados.put("resultado_modificado", true);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getModificarParametrizacion] Fin ");

  }

  private void getParametrizacionesActivasByIdRegla(WebRequest webRequest, WebResponse webResponse) {
    // LOG.debug( "[" + NOMBRE_SERVICIO +
    // " :: getParametrizacionesActivasByIdRegla] Init" );
    // final Map< String, List< Tmct0parametrizacionDTO >> resultados = new
    // HashMap< String, List<Tmct0parametrizacionDTO >>();
    // List< Tmct0parametrizacionDTO > listaParametrizacionesActivas = new
    // ArrayList< Tmct0parametrizacionDTO >();
    //
    // LOG.debug( "[" + NOMBRE_SERVICIO +
    // " :: getParametrizacionesActivasByIdRegla] Comenzando consulta a TMCT0_PARAMETRIZACIONES"
    // );
    // for ( Tmct0parametrizacion gestionParametrizacionesActivas :
    // tmct0parametrizacionBo.findParametrizacionesByIdReglaActivas()) {
    // listaParametrizacionesActivas.add( new
    // Tmct0parametrizacionDTO(gestionParametrizacionesActivas) );
    //
    // }
    // LOG.debug( "[" + NOMBRE_SERVICIO +
    // " :: getParametrizacionesActivasByIdRegla] Terminando consulta a TMCT0_PARAMETRIZACIONES. Obtenidos "
    // + listaParametrizacionesActivas.size()
    // + " registros" );
    //
    // resultados.put( CMD_INFORME_LIST, listaParametrizacionesActivas );
    // webResponse.setResultados( resultados );
    // LOG.debug( "[" + NOMBRE_SERVICIO +
    // " :: getParametrizacionesActivasByIdRegla] Fin " );

  }

  private void parametrizacionesByIdParam(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getParametrizacionesByIdParametrizacion] Init");
    try {
      Map<String, String> filters = webRequest.getFilters();

      final Long idParametrizacion = Long.parseLong(filters.get(FILTER_IDPARAMETRIZACION));

      final Map<String, Tmct0parametrizacion> resultados = new HashMap<String, Tmct0parametrizacion>();
      LOG.debug("[" + NOMBRE_SERVICIO
          + " :: getParametrizacionesByIdParametrizacion] Comenzando consulta a TMCT0_PARAMETRIZACION");
      Tmct0parametrizacion Parametrizacion = tmct0parametrizacionBo.findById(idParametrizacion);
      LOG.debug("["
          + NOMBRE_SERVICIO
          + " :: getParametrizacionesByIdParametrizacion] Terminando consulta a TMCT0_PARAMETRIZACION. Parametrización a modificar ");
      resultados.put("parametrizacionesByIdParam", Parametrizacion);
      webResponse.setResultados(resultados);
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getParametrizacionesById] Fin ");
    }
    catch (Exception e) {
      LOG.error("Error: ", e.getClass().getName(), e.getMessage());
    }
  }

  /**
   * 
   * @param webRequest
   * @param webResponse
   */
  private void getParametrizacionesById(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + CMD_PARAMETRIZACION_BY_ID + "] Init");
    Map<String, String> filters = webRequest.getFilters();

    final BigInteger idRegla = new BigInteger(filters.get(FILTER_IDREGLA));

    final Map<String, List<Tmct0parametrizacionDTO>> resultados = new HashMap<String, List<Tmct0parametrizacionDTO>>();
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + CMD_PARAMETRIZACION_BY_ID
        + "] Comenzando consulta a TMCT0_PARAMETRIZACION");
    List<Tmct0parametrizacionDTO> listaParametrizacion = tmct0parametrizacionBo.findParametrizacionesByIdRegla(idRegla);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + CMD_PARAMETRIZACION_BY_ID
        + "] Terminando consulta a TMCT0_PARAMETRIZACION. Obtenidos " + listaParametrizacion.size() + " registros");
    resultados.put("resultados_parametrizacion", listaParametrizacion);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: " + CMD_PARAMETRIZACION_BY_ID + "] Fin ");

  }

  private void getListSegmento(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListSegmento] Init");
    final Map<String, List<Tmct0SegmentoDTO>> resultados = new HashMap<String, List<Tmct0SegmentoDTO>>();
    List<Tmct0SegmentoDTO> listaSegmento = new ArrayList<Tmct0SegmentoDTO>();
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListSegmento] Comenzando consulta a TMCT0_SEGMENTO");
    for (Tmct0Segmento gestionSegmento : tmct0SegmentoBo.findAll()) {
      listaSegmento.add(new Tmct0SegmentoDTO(gestionSegmento));
    }
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListSegmento] Terminando consulta a TMCT0_SEGMENTO. Obtenidos "
        + listaSegmento.size() + " registros");

    resultados.put("segment_list", listaSegmento);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListSegmento] Fin ");
  }

  private void getListModeloDeNegocio(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListModeloDeNegocio] Init");
    final Map<String, List<Tmct0ModeloDeNegocioDTO>> resultados = new HashMap<String, List<Tmct0ModeloDeNegocioDTO>>();
    List<Tmct0ModeloDeNegocioDTO> listaModeloDeNegocio = new ArrayList<Tmct0ModeloDeNegocioDTO>();
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListModeloDeNegocio] Comenzando consulta a TMCT0_MODELO_DE_NEGOCIO");
    for (Tmct0ModeloDeNegocio gestionModeloDeNegocio : tmct0ModeloDeNegocioBo.findAll()) {
      listaModeloDeNegocio.add(new Tmct0ModeloDeNegocioDTO(gestionModeloDeNegocio));
    }
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getListModeloDeNegocio] Terminando consulta a TMCT0_MODELO_DE_NEGOCIO. Obtenidos "
        + listaModeloDeNegocio.size() + " registros");

    resultados.put("modelo_de_negocio_list", listaModeloDeNegocio);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListModeloDeNegocio] Fin ");
  }

  private void getListCapacidad(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListCapacidad] Init");
    final Map<String, List<Tmct0IndicadorDeCapacidadDTO>> resultados = new HashMap<String, List<Tmct0IndicadorDeCapacidadDTO>>();
    List<Tmct0IndicadorDeCapacidadDTO> listaGestionReglas = new ArrayList<Tmct0IndicadorDeCapacidadDTO>();
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListCapacidad] Comenzando consulta a TMCT0_REGLA");
    for (Tmct0IndicadorDeCapacidad gestionReglas : tmct0IndicadorDeCapacidadBo.findAll()) {
      listaGestionReglas.add(new Tmct0IndicadorDeCapacidadDTO(gestionReglas));
    }
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getListCapacidad] Terminando consulta a TMCT0_INDICADOR_DE_CAPACIDAD. Obtenidos "
        + listaGestionReglas.size() + " registros");

    resultados.put("capacidad_list", listaGestionReglas);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListCapacidad] Fin ");
  }

  private void getListNemotecnicos(WebRequest webRequest, WebResponse webResponse) {
    List<Tmct0Nemotecnicos> lista = tmct0NemotecnicosBo.findAll();
    lista.size();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListNemotecnicos] Init");
    final Map<String, List<Tmct0NemotecnicosDTO>> resultados = new HashMap<String, List<Tmct0NemotecnicosDTO>>();
    List<Tmct0NemotecnicosDTO> listaNemotecnicos = new ArrayList<Tmct0NemotecnicosDTO>();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListNemotecnicos] Comenzando consulta a TMCT0_NEMOTECNICOS");
    for (Tmct0Nemotecnicos gestionNemotecnicos : tmct0NemotecnicosBo.findAll()) {
      listaNemotecnicos.add(new Tmct0NemotecnicosDTO(gestionNemotecnicos));
    }
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListNemotecnicos] Terminando consulta a TMCT0_NEMOTECNICOS. Obtenidos "
        + listaNemotecnicos.size() + " registros");

    resultados.put("nemotecnicos_list", listaNemotecnicos);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getListNemotecnicos] Fin ");
  }

  /**
   * @param filtros
   * @return
   */
  private String processString(final String filterString) {
    String processedString = null;
    if (StringUtils.isNotEmpty(filterString)) {
      processedString = filterString;
    }
    return processedString;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> commands = new ArrayList<String>();
    commands.add(CMD_INFORME_LIST);
    return commands;
  }

  @Override
  public List<String> getFields() {

    return null;
  }

  /**
   * GET REGLAS AND PARAM
   * @return
   */
  @Transactional
  public List<Map<String, Object>> getReglasAndParam() {
    LOG.trace("SIBBACServicePeriodicidades <getReglasAndParam>");

    List<Tmct0Regla> reglas = tmct0ReglaBo.findAll();

    List<Map<String, Object>> resultadosReglas = new ArrayList<Map<String, Object>>();
    for (Tmct0Regla regla : reglas) {

      if ("1".equals(regla.getActivo())) {

        Map<String, Object> resultadoRegla = new Hashtable<String, Object>();

        resultadoRegla.put(FILTER_CDCODIGO, regla.getCdCodigo().toString());
        resultadoRegla.put(FILTER_DESCRIPCION, regla.getNbDescripcion().toString());
        resultadoRegla.put(FILTER_FECHA_CREACION, regla.getFechaCreacion().toString());
        resultadoRegla.put(FILTER_REGLA_ACTIVA, String.valueOf(regla.getActivo()));
        resultadoRegla.put(FILTER_IDREGLA, regla.getIdRegla().toString());

        List<Tmct0parametrizacion> params = tmct0parametrizacionDao.findParametrizacionesByIdRegla(regla.getIdRegla());

        List<Map<String, String>> resultadosParametrizaciones = new ArrayList<Map<String, String>>();

        int nParametrizaciones = 0;

        for (Tmct0parametrizacion param : params) {

          Map<String, String> resultadosParam = new Hashtable<String, String>();

          if ("1".equals(param.getActivo())) {
            nParametrizaciones++;
            String idParam;
            String idCamara;
            String idCtaCompe;
            String idMiembroComp;
            String idNemotecnico;
            String idSegmento;
            String idCapacidad;
            String idModeloNegocio;

            Tmct0parametrizacionDTO par = DTOUtils.convertObjectToParametrizacionesDTO(param);

            if (par.getIdParametrizacion() != null) {
              idParam = String.valueOf(par.getIdParametrizacion());
              resultadosParam.put(FILTER_IDPARAMETRIZACION, idParam.toString());
            }

            if (par.getDescripcionParametri() != null) {
              resultadosParam.put(DESCRIPCION_PARAMETRIZACION, par.getDescripcionParametri().toString());
            }

            // cámara compensación
            if (par.getCd_codigoCamaraComp() != null) {
              resultadosParam.put(CAMARA_COMPENSACION_CODIGO, par.getCd_codigoCamaraComp().toString());
            }

            if (par.getNb_descripcionCamaraComp() != null) {
              resultadosParam.put(CAMARA_COMPENSACION_DESCRIPCION, par.getNb_descripcionCamaraComp().toString());
            }

            if (par.getIdCamaraComp() != null) {

              idCamara = String.valueOf(par.getIdCamaraComp());
              resultadosParam.put(ID_CAMARA_COMPENSACION, idCamara.toString());
            }

            // cuenta compensación
            if (par.getCd_codigoCuentaCompensacion() != null) {
              resultadosParam.put(CUENTA_COMPENSACION_CDCODIGO, par.getCd_codigoCuentaCompensacion().toString());
            }

            if (par.getIdCuentaCompensacion() != null) {

              idCtaCompe = String.valueOf(par.getIdCuentaCompensacion());
              resultadosParam.put(ID_CUENTA_COMPENSACION, idCtaCompe.toString());
            }

            // miembro compensador
            if (par.getNb_descripcionCompensador() != null) {
              resultadosParam.put(FILTER_MIEMBRO_COMPENSADOR, par.getNb_descripcionCompensador().toString());
            }

            if (par.getIdCompensador() != null) {

              idMiembroComp = String.valueOf(par.getIdCompensador());
              resultadosParam.put(ID_MIEMBRO_COMPENSADOR, idMiembroComp.toString());
            }

            if (par.getReferenciaAsignacion() != null) {
              resultadosParam.put(FILTER_REFERENCIA_ASIGNACION, par.getReferenciaAsignacion().toString());
            }

            // nemotécnico
            if (par.getNb_nombreNmotecnico() != null) {
              resultadosParam.put(NEMOTECNICO_NBNOMBRE, par.getNb_nombreNmotecnico().toString());
            }

            if (par.getIdNemotecnico() != null) {

              idNemotecnico = String.valueOf(par.getIdNemotecnico());
              resultadosParam.put(ID_NEMOTECNICO, idNemotecnico.toString());
            }

            // segmento
            if (par.getCd_codigoSegmento() != null) {
              resultadosParam.put(SEGMENTO_CDCODIGO, par.getCd_codigoSegmento().toString());
            }

            if (par.getNb_descripcionSegmento() != null) {
              resultadosParam.put(SEGMENTO_NBDESCRIPCION, par.getNb_descripcionSegmento().toString());
            }

            if (par.getIdSegmento() != 0) {

              idSegmento = String.valueOf(par.getIdSegmento());
              resultadosParam.put(ID_SEGMENTO, idSegmento.toString());
            }

            if (par.getReferenciaCliente() != null) {
              resultadosParam.put(FILTER_REFERENCIA_CLIENTE, par.getReferenciaCliente().toString());
            }

            if (par.getReferenciaExterna() != null) {
              resultadosParam.put(FILTER_REFERENCIA_EXTERNA, par.getReferenciaExterna().toString());
            }

            // capacidad
            if (par.getCd_codigoIndicCapacidad() != null) {
              resultadosParam.put(CAPACIDAD_CDCODIGO, par.getCd_codigoIndicCapacidad().toString());
            }

            if (par.getNb_descripcionIndicCapacidad() != null) {
              resultadosParam.put(CAPACIDAD_DESCRIPCION, par.getNb_descripcionIndicCapacidad().toString());
            }

            if (par.getIdCapacidad() != null) {

              idCapacidad = String.valueOf(par.getIdCapacidad());
              resultadosParam.put(ID_CAPACIDAD, idCapacidad.toString());
            }

            // modelo de negocio
            if (par.getCd_codigoModeloNegocio() != null) {
              resultadosParam.put(MODELO_NEGOCIO_CDCODIGO, par.getCd_codigoModeloNegocio().toString());
            }

            if (par.getNb_descripcionModeloNegocio() != null) {
              resultadosParam.put(MODELO_NEGOCIO_NBDESCRIPCION, par.getNb_descripcionModeloNegocio().toString());
            }

            if (par.getIdModeloNegocio() != null) {

              idModeloNegocio = String.valueOf(par.getIdModeloNegocio());
              resultadosParam.put(ID_MODELO_NEGOCIO, idModeloNegocio.toString());
            }

            if (par.getFecha_fin() != null) {
              resultadosParam.put(FILTER_FECHA_FIN, par.getFecha_fin().toString());
            }

            resultadosParametrizaciones.add(resultadosParam);

          }// if("1".equals(param.getActivo())){

        }// for ( Tmct0parametrizacion

        resultadoRegla.put(PARAMETRIZACION_BY_REGLA, nParametrizaciones);
        resultadoRegla.put(REGLA_AND_PARAMETRIZACION, resultadosParametrizaciones);
        resultadosReglas.add(resultadoRegla);
        LOG.trace("SIBBACServicePeriodicidades <getReglasAndParam> resultadosReglas : " + resultadosReglas);
      }// if
    }// for regla

    return resultadosReglas;
  }

  /**
   * GET REGLAS AND PARAM POR FILTRO
   * @param idRegl
   * @param camaraCompensacion
   * @param cuentaCompensacion
   * @param miembroCompensador
   * @param referenciaAsignacion
   * @param nemotecnico
   * @param segmento
   * @param referenciaCliente
   * @param referenciaExterna
   * @param capacidad
   * @param modeloNegocio
   * @return
   */
  @Transactional
  public List<Map<String, Object>> getReglasAndParam1(Long idRegl, Long camaraCompensacion, Long cuentaCompensacion,
      Long miembroCompensador, String referenciaAsignacion, Long nemotecnico, Long segmento, String referenciaCliente,
      String referenciaExterna, Long capacidad, Long modeloNegocio) {
    LOG.trace("SIBBACServicePeriodicidades <getReglasAndParam1>");

    List<Tmct0parametrizacion> params = tmct0parametrizacionBo.findDTOForService(idRegl, camaraCompensacion,
        cuentaCompensacion, miembroCompensador, referenciaAsignacion, nemotecnico, segmento, referenciaCliente,
        referenciaExterna, capacidad, modeloNegocio);
    BigInteger idRegla;
    Tmct0Regla regla;

    List<Map<String, Object>> resultadosReglas = new ArrayList<Map<String, Object>>();

    for (Tmct0parametrizacion parame : params) {
      if (parame.getTmct0Regla() != null && parame.getTmct0Regla().getIdRegla() != null) {
        idRegla = parame.getTmct0Regla().getIdRegla();
        regla = tmct0ReglaBo.findIdRegla(idRegla);

        Map<String, Object> resultadoRegla = new Hashtable<String, Object>();

        resultadoRegla.put(FILTER_CDCODIGO, regla.getCdCodigo().toString());
        resultadoRegla.put(FILTER_DESCRIPCION, regla.getNbDescripcion().toString());
        resultadoRegla.put(FILTER_FECHA_CREACION, regla.getFechaCreacion().toString());
        resultadoRegla.put(FILTER_REGLA_ACTIVA, String.valueOf(regla.getActivo()));
        resultadoRegla.put(FILTER_IDREGLA, regla.getIdRegla().toString());

        List<Tmct0parametrizacion> paramstr = tmct0parametrizacionDao
            .findParametrizacionesByIdRegla(regla.getIdRegla());

        List<Map<String, String>> resultadosParametrizaciones = new ArrayList<Map<String, String>>();

        int nParametrizaciones = 0;

        for (Tmct0parametrizacion param : paramstr) {

          Map<String, String> resultadosParam = new Hashtable<String, String>();

          if ("1".equals(param.getActivo())) {
            nParametrizaciones++;
            String idParam;
            String idCamara;
            String idCtaCompe;
            String idMiembroComp;
            String idNemotecnico;
            String idSegmento;
            String idCapacidad;
            String idModeloNegocio;

            Tmct0parametrizacionDTO par = DTOUtils.convertObjectToParametrizacionesDTO(param);

            if (par.getIdParametrizacion() != null) {
              idParam = String.valueOf(par.getIdParametrizacion());
              resultadosParam.put(FILTER_IDPARAMETRIZACION, idParam.toString());
            }

            if (par.getDescripcionParametri() != null) {
              resultadosParam.put(DESCRIPCION_PARAMETRIZACION, par.getDescripcionParametri().toString());
            }

            // cámara compensación
            if (par.getCd_codigoCamaraComp() != null) {
              resultadosParam.put(CAMARA_COMPENSACION_CODIGO, par.getCd_codigoCamaraComp().toString());
            }

            if (par.getNb_descripcionCamaraComp() != null) {
              resultadosParam.put(CAMARA_COMPENSACION_DESCRIPCION, par.getNb_descripcionCamaraComp().toString());
            }

            if (par.getIdCamaraComp() != null) {

              idCamara = String.valueOf(par.getIdCamaraComp());
              resultadosParam.put(ID_CAMARA_COMPENSACION, idCamara.toString());
            }

            // cuenta compensación
            if (par.getCd_codigoCuentaCompensacion() != null) {
              resultadosParam.put(CUENTA_COMPENSACION_CDCODIGO, par.getCd_codigoCuentaCompensacion().toString());
            }

            if (par.getIdCuentaCompensacion() != null) {

              idCtaCompe = String.valueOf(par.getIdCuentaCompensacion());
              resultadosParam.put(ID_CUENTA_COMPENSACION, idCtaCompe.toString());
            }

            // miembro compensador
            if (par.getNb_descripcionCompensador() != null) {
              resultadosParam.put(FILTER_MIEMBRO_COMPENSADOR, par.getNb_descripcionCompensador().toString());
            }

            if (par.getIdCompensador() != null) {

              idMiembroComp = String.valueOf(par.getIdCompensador());
              resultadosParam.put(ID_MIEMBRO_COMPENSADOR, idMiembroComp.toString());
            }

            if (par.getReferenciaAsignacion() != null) {
              resultadosParam.put(FILTER_REFERENCIA_ASIGNACION, par.getReferenciaAsignacion().toString());
            }

            // nemotécnico
            if (par.getNb_nombreNmotecnico() != null) {
              resultadosParam.put(NEMOTECNICO_NBNOMBRE, par.getNb_nombreNmotecnico().toString());
            }

            if (par.getIdNemotecnico() != null) {

              idNemotecnico = String.valueOf(par.getIdNemotecnico());
              resultadosParam.put(ID_NEMOTECNICO, idNemotecnico.toString());
            }

            // segmento
            if (par.getCd_codigoSegmento() != null) {
              resultadosParam.put(SEGMENTO_CDCODIGO, par.getCd_codigoSegmento().toString());
            }

            if (par.getNb_descripcionSegmento() != null) {
              resultadosParam.put(SEGMENTO_NBDESCRIPCION, par.getNb_descripcionSegmento().toString());
            }

            if (par.getIdSegmento() != 0) {

              idSegmento = String.valueOf(par.getIdSegmento());
              resultadosParam.put(ID_SEGMENTO, idSegmento.toString());
            }

            if (par.getReferenciaCliente() != null) {
              resultadosParam.put(FILTER_REFERENCIA_CLIENTE, par.getReferenciaCliente().toString());
            }

            if (par.getReferenciaExterna() != null) {
              resultadosParam.put(FILTER_REFERENCIA_EXTERNA, par.getReferenciaExterna().toString());
            }

            // capacidad
            if (par.getCd_codigoIndicCapacidad() != null) {
              resultadosParam.put(CAPACIDAD_CDCODIGO, par.getCd_codigoIndicCapacidad().toString());
            }

            if (par.getNb_descripcionIndicCapacidad() != null) {
              resultadosParam.put(CAPACIDAD_DESCRIPCION, par.getNb_descripcionIndicCapacidad().toString());
            }

            if (par.getIdCapacidad() != null) {

              idCapacidad = String.valueOf(par.getIdCapacidad());
              resultadosParam.put(ID_CAPACIDAD, idCapacidad.toString());
            }

            // modelo de negocio
            if (par.getCd_codigoModeloNegocio() != null) {
              resultadosParam.put(MODELO_NEGOCIO_CDCODIGO, par.getCd_codigoModeloNegocio().toString());
            }

            if (par.getNb_descripcionModeloNegocio() != null) {
              resultadosParam.put(MODELO_NEGOCIO_NBDESCRIPCION, par.getNb_descripcionModeloNegocio().toString());
            }

            if (par.getIdModeloNegocio() != null) {

              idModeloNegocio = String.valueOf(par.getIdModeloNegocio());
              resultadosParam.put(ID_MODELO_NEGOCIO, idModeloNegocio.toString());
            }

            if (par.getFecha_fin() != null) {
              resultadosParam.put(FILTER_FECHA_FIN, par.getFecha_fin().toString());
            }

            resultadosParametrizaciones.add(resultadosParam);

          }// if
        }// for parametrizaciones

        resultadoRegla.put(PARAMETRIZACION_BY_REGLA, nParametrizaciones);
        resultadoRegla.put(REGLA_AND_PARAMETRIZACION, resultadosParametrizaciones);
        resultadosReglas.add(resultadoRegla);
        LOG.trace("SIBBACServicePeriodicidades <getReglasAndParam> resultadosReglas : " + resultadosReglas);

      }// if

    }// for

    return resultadosReglas;
  }

  private void getAsignarAlias(WebRequest webRequest, WebResponse webResponse) {
    try {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getAsignarAlias] Init");
      Map<String, String> filters = webRequest.getFilters();
      boolean lCambio = false;

      final BigInteger idRegla = new BigInteger((filters.get(FILTER_IDREGLA)));

      List<Tmct0ali> listAlias = tmct0aliDao.findCdAliassById_regla(idRegla.longValue());
      for (Tmct0ali tmct0ali : listAlias) {
        tmct0ali.setIdRegla(null);
        tmct0aliBo.save(tmct0ali);
      }

      String[] parts = filters.get(FILTER_ALIAS).split(";");
      for (String part : parts) {
        Tmct0ali ali = tmct0aliBo.findFirst1ByCdaliass(part);
        if (ali != null) {
          ali.setIdRegla(idRegla.longValue());
          tmct0aliBo.save(ali);
          lCambio = true;
        }
      }
      Map<String, String> resultado = new Hashtable<String, String>();
      if (lCambio) {
        resultado.put(RETURN_ASIGNAR_ALIAS, "La asignación a alias se ha realizado correctamente");
      }
      else {
        resultado.put(RETURN_ASIGNAR_ALIAS,
            "La asignación a alias NO se ha realizado correctamente. Por que no existe ese alias.");
      }
      webResponse.setResultados(resultado);
    }
    catch (Exception ex) {
      LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
      Map<String, String> resultado = new Hashtable<String, String>();
      resultado.put(RETURN_ASIGNAR_ALIAS, ex.getMessage());
      webResponse.setResultados(resultado);
    }

  }

  private void getAsignarSubCta(WebRequest webRequest, WebResponse webResponse) {
    try {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getAsignarSuCta Init");
      Integer fhfinal = 99991231;
      boolean lCambio = false;

      Map<String, String> filters = webRequest.getFilters();

      final BigInteger idRegla = new BigInteger((filters.get(FILTER_IDREGLA)));
      final String alias = (filters.get(FILTER_ALIAS));
      List<Tmct0act> listAct = null;
      String[] parts = filters.get(FILTER_SUBCTA).split(";");
      for (String part : parts) {
        listAct = tmct0actBo.getSubCtaBro(alias, part, fhfinal);
        if (listAct != null && listAct.size() > 0) {
          for (Tmct0act act : listAct) {
            act.setIdRegla(idRegla.longValue());
            tmct0actBo.save(act);
            lCambio = true;
          }
        }
      }
      Map<String, String> resultado = new Hashtable<String, String>();
      if (lCambio) {
        resultado.put(RETURN_ASIGNAR_SUBCTA, "La asignación a SubCuentas se ha realizado correctamente");
      }
      else {
        resultado
            .put(RETURN_ASIGNAR_SUBCTA,
                "La asignación a SubCuentas NO se ha realizado correctamente. Por que no existe esa combinación de alias subCuenta");
      }
      webResponse.setResultados(resultado);
    }
    catch (Exception ex) {
      LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
      Map<String, String> resultado = new Hashtable<String, String>();
      resultado.put(RETURN_ASIGNAR_SUBCTA, ex.getMessage());
      webResponse.setResultados(resultado);
    }

  }

  private void getCodigoList(WebRequest webRequest, WebResponse webResponse) {
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCodigoList] Init");
    final Map<String, List<Tmct0Regla>> resultados = new HashMap<String, List<Tmct0Regla>>();

    List<Tmct0Regla> listaCodigo = new ArrayList<Tmct0Regla>();
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getCuentasCompensacionList] Comenzando consulta a TMCT0_CUANTAS_DE_COMPENSACION");
    for (Tmct0Regla gestionCodigo : tmct0ReglaBo.findAll()) {
      listaCodigo.add(new Tmct0Regla(gestionCodigo.getIdRegla(), gestionCodigo.getCdCodigo(), gestionCodigo
          .getNbDescripcion(), gestionCodigo.getFechaCreacion(), gestionCodigo.getAuditFechaCambio(), gestionCodigo
          .getAuditUser(), gestionCodigo.getActivo()));
    }
    LOG.debug("[" + NOMBRE_SERVICIO
        + " :: getCuentasCompensacionList] Terminando consulta a TMCT0_CUENTAS_DE_COMPENSACION. Obtenidos "
        + listaCodigo.size() + " registros");

    resultados.put("result_codigo", listaCodigo);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getCuentasCompensacionList] Fin ");
  }

  private void getExportarParam(WebRequest webRequest, WebResponse webResponse) {

    try {
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam Init");

      List<HashMap<String, Object>> listaResult = new ArrayList<HashMap<String, Object>>();
      List<HashMap<String, Object>> listaResultAlias = new ArrayList<HashMap<String, Object>>();
      List<HashMap<String, Object>> listaResultSubCta = new ArrayList<HashMap<String, Object>>();

      List<Map<String, String>> params = webRequest.getParams();
      Integer countRow = 0;
      Map registro = null;
      BigInteger idRegla = null;

      Long idReglaL = null;

      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > Inicio Parametrizaciones");
      List<Tmct0parametrizacionDTO> listaParametrizacion = new ArrayList<Tmct0parametrizacionDTO>();
      Iterator it = params.iterator();

      List<BigInteger> idReglas = new ArrayList<BigInteger>();
      List<Long> idReglasLong = new ArrayList<Long>();
      while (it.hasNext()) {
        registro = (Map) it.next();
        idRegla = new BigInteger((String) registro.get("idRegla"));
        idReglaL = new Long((String) registro.get("idRegla"));
        idReglas.add(idRegla);
        idReglasLong.add(idReglaL);
      }

      HashMap<String, Object> parame = new HashMap<String, Object>();
      listaParametrizacion = tmct0parametrizacionBo.findParametrizacionesByManyReglas(idReglas);
      for (Tmct0parametrizacionDTO para : listaParametrizacion) {
        parame = hashExport(para);
        listaResult.add(parame);
      }

      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > fin Parametrizaciones");
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > Inicio Alias");

      Map<String, List<HashMap<String, Object>>> result = new HashMap<String, List<HashMap<String, Object>>>();
      result.put(RESULT_PARAMETRIZACION_LIST_EXPORT, listaResult);
      listaResult = null;
      countRow = 0;
      registro = null;
      List<Tmct0aliDTO> listaAlias = new ArrayList<Tmct0aliDTO>();
      it = params.iterator();
      listaAlias = tmct0aliBo.findAliasByManyReglas(idReglasLong);
      HashMap<String, Object> alias = new HashMap<String, Object>();
      for (Tmct0aliDTO alia : listaAlias) {
        alias = hashExportAlias(alia);
        listaResultAlias.add(alias);
      }

      HashMap<String, Object> subCta;
      result.put(RESULT_ALIAS_LIST_EXPORT, listaResultAlias);
      listaResultAlias = null;
      countRow++;
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > Fin Alias");
      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > Inicio SubCta");
      registro = null;
      List<Tmct0actDTO> listaSubCta = new ArrayList<Tmct0actDTO>();
      Integer countPag = 0;
      listaSubCta = tmct0actBo.findSubctaByManyReglas(idReglasLong);
      for (Tmct0actDTO subCt : listaSubCta) {
        subCta = hashExportSubCta(subCt);
        listaResultSubCta.add(subCta);
        countRow++;
        if (countRow == 65535) {
          countPag++;
          String nombreResult = RESULT_SUBCTA_LIST_EXPORT + String.valueOf(countPag);
          result.put(nombreResult, listaResultSubCta);
          listaResultSubCta = new ArrayList<HashMap<String, Object>>();
          countRow = 0;

        }
      }

      countPag++;
      String nombreResult = RESULT_SUBCTA_LIST_EXPORT + String.valueOf(countPag);
      result.put(nombreResult, listaResultSubCta);

      LOG.debug("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > fin SubCta");
      listaResultSubCta = null;
      webResponse.setResultados(result);

    }
    catch (Exception ex) {
      LOG.error("[" + NOMBRE_SERVICIO + " :: getExportarParam -- > fin SubCta traza : " + ex.toString());
      LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
    }

  }

  private HashMap<String, Object> hashExport(Tmct0parametrizacionDTO para) {

    HashMap<String, Object> parame = new HashMap<String, Object>();

    parame.put("idRegla", para.getIdRegla());
    parame.put("cdCodigoRegla", para.getCdCodigoRegla());
    parame.put("descripcion", para.getDescripcionRegla());
    parame.put("idParam", para.getIdParametrizacion());
    parame.put("camara", para.getNb_descripcionCamaraComp());
    parame.put("segmento", para.getNb_descripcionSegmento());
    parame.put("refCliente", para.getReferenciaCliente());
    parame.put("refExterna", para.getReferenciaExterna());
    parame.put("capacidad", para.getNb_descripcionIndicCapacidad());
    parame.put("modelNegocio", para.getNb_descripcionModeloNegocio());
    parame.put("nemotecnico", para.getNb_nombreNmotecnico());
    parame.put("miembro", para.getNb_descripcionCompensador());
    parame.put("ctaCompe", para.getCd_codigoCuentaCompensacion());
    parame.put("refAsignacion", para.getReferenciaAsignacion());
    parame.put("fechafin", para.getFecha_fin());

    return parame;

  }

  private HashMap<String, Object> hashExportAlias(Tmct0aliDTO alia) {

    HashMap<String, Object> alias = new HashMap<String, Object>();

    alias.put("idRegla", alia.getIdRegla());
    alias.put("cdCodigoRegla", alia.getCdCodigoRegla());
    alias.put("cdAliass", alia.getAlias());
    alias.put("nombre", alia.getDescripcion());

    return alias;

  }

  private HashMap<String, Object> hashExportSubCta(Tmct0actDTO act) {

    HashMap<String, Object> subCta = new HashMap<String, Object>();

    subCta.put("idRegla", act.getIdRegla());
    subCta.put("cdCodigoRegla", act.getCdCodigoRegla());
    subCta.put("cdAliass", act.getCdAliass());
    subCta.put("cdSubCta", act.getCdSubCta());
    subCta.put("nombre", act.getDesacrali());

    return subCta;

  }

  private void getAliasList(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filters = webRequest.getFilters();

    Long idRegla = new Long(Long.parseLong(filters.get(FILTER_IDREGLA)));

    List<Tmct0ali> lista = tmct0aliBo.findCdAliassById_regla(idRegla);
    lista.size();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getAliasList] Init");
    final Map<String, List<Tmct0ali>> resultados = new HashMap<String, List<Tmct0ali>>();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getAliasList] Comenzando consulta a TMCT0ALI");

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getAliasList] Terminando consulta a TMCT0_NEMOTECNICOS. Obtenidos "
        + lista.size() + " registros");

    resultados.put("resultados_lista_alias", lista);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getAliasList] Fin ");
  }

  private void getSubCtaList(WebRequest webRequest, WebResponse webResponse) {

    Map<String, String> filters = webRequest.getFilters();

    Long idRegla = new Long(Long.parseLong(filters.get(FILTER_IDREGLA)));

    List<Tmct0act> lista = tmct0actBo.findSubctaById_regla(idRegla);
    lista.size();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getSubCtaList] Init");
    final Map<String, List<Tmct0act>> resultados = new HashMap<String, List<Tmct0act>>();

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getSubCtaList] Comenzando consulta a TMCT0ALI");

    LOG.debug("[" + NOMBRE_SERVICIO + " :: getSubCtaList] Terminando consulta a TMCT0_NEMOTECNICOS. Obtenidos "
        + lista.size() + " registros");

    resultados.put("resultados_lista_subcta", lista);
    webResponse.setResultados(resultados);
    LOG.debug("[" + NOMBRE_SERVICIO + " :: getSubCtaList] Fin ");
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  private void importarRegla(WebRequest webRequest, WebResponse webResponse) {

    LOG.debug("[" + NOMBRE_SERVICIO + " :: importarRegla] Init");
    Map<String, String> filters = webRequest.getFilters();
    final String excelFile = processString(filters.get(FILTER_FILE));
    final String excelFileName = processString(filters.get(FILTER_FILENAME));
    byte[] data = Base64.decodeBase64(excelFile);

    File folderDestino = new File(FOLDER_DESTINO);
    folderDestino.mkdirs();

    try (OutputStream stream = new FileOutputStream(FOLDER_DESTINO + excelFileName)) {
      stream.write(data);
      LOG.debug("[" + NOMBRE_SERVICIO + " :: importarRegla] Se guarda el fichero " + FOLDER_DESTINO + " "
          + excelFileName + " por el usuario: <>");

    }
    catch (FileNotFoundException e) {
      LOG.error("[" + NOMBRE_SERVICIO + " :: importarRegla] Falla la subida del fichero " + FOLDER_DESTINO + " "
          + excelFileName + " por el usuario: <>");
      LOG.error("Error: ", e.getClass().getName(), e.getMessage());
    }
    catch (IOException e) {
      LOG.error("[" + NOMBRE_SERVICIO + " :: importarRegla] Falla la subida del fichero " + FOLDER_DESTINO + " "
          + excelFileName + " por el usuario: <>");
      LOG.error("Error: ", e.getClass().getName(), e.getMessage());
    }

    Integer nCountRegla = 0, nCountParam = 0, nCountAlias = 0, nCountSubCta = 0;
    Integer lineaError = 0, columnaError = 0, paginaError = 0;
    boolean lError = false;

    Tmct0Regla regla = null;

    Tmct0Regla reglaBusqueda = null;
    Tmct0parametrizacion para = null;
    Tmct0ali alias = null;
    Tmct0aliId aliasId = null;

    String msgError = "Ha habido los siguientes errores : <br/>";

    String cdCodigoOld = "";

    FileInputStream fis = null;
    Date fechaAuditoria = new Date();
    BigInteger idRegla = null;
    try {

      fis = new FileInputStream(FOLDER_DESTINO + excelFileName);
      HSSFWorkbook workbook = new HSSFWorkbook(fis);
      HSSFSheet sheet = workbook.getSheetAt(0);
      Integer countRow = 0;
      Integer countCell;
      paginaError++;
      Iterator rows = sheet.rowIterator();
      while (rows.hasNext()) {
        HSSFRow row = (HSSFRow) rows.next();
        if (countRow > 0) {
          regla = new Tmct0Regla();
          para = new Tmct0parametrizacion();
          Iterator cells = row.cellIterator();
          countCell = 0;
          while (cells.hasNext()) {
            try {
              HSSFCell cell = (HSSFCell) cells.next();
              switch (countCell) {
                case 0:
                  regla.setCdCodigo(cell.getStringCellValue());
                  if (regla.getCdCodigo() == null)
                    regla.setCdCodigo("");

                  para.setTmct0Regla(regla);
                  break;
                case 1:
                  regla.setNbDescripcion(cell.getStringCellValue());
                  if (regla.getNbDescripcion() == null)
                    regla.setNbDescripcion("");

                  para.setDescripcion("Parametrización " + countRow.toString());

                  break;
                case 2:
                  para.setTmct0CamaraCompensacion(tmct0CamaraCompensacionBo.findByNbDescripcion(cell
                      .getStringCellValue()));
                  break;
                case 3:
                  para.setTmct0Segmento(tmct0SegmentoBo.findByNbDescripcion(cell.getStringCellValue()));
                  break;
                case 4:
                  para.setReferenciaCliente(cell.getStringCellValue());
                  break;
                case 5:
                  para.setReferenciaExterna(cell.getStringCellValue());
                  break;
                case 6:
                  para.setTmct0IndicadorCapacidad(tmct0IndicadorDeCapacidadBo.findByDescripcion(cell
                      .getStringCellValue()));
                  break;
                case 7:
                  para.setTmct0ModeloDeNegocio(tmct0ModeloDeNegocioBo.findByNbDescripcion(cell.getStringCellValue()));
                  break;
                case 8:
                  para.setTmct0Nemotecnicos(tmct0NemotecnicosBo.findByNombre(cell.getStringCellValue()));
                  break;
                case 9:
                  para.setTmct0Compensador(tmct0CompensadorBo.findByNbDescripcion(cell.getStringCellValue()));
                  break;
                case 10:
                  para.setTmct0CuentasDeCompensacion(tmct0TipoCuentaDeCompensacionBo.findByCdCodigo(cell
                      .getStringCellValue()));
                  break;
                case 11:
                  para.setCdReferenciaAsignacion(cell.getStringCellValue());
                  break;
                case 12:
                  if (cell.getCellType() == 1) {
                    String strFecha = cell.getStringCellValue();
                    SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");

                    Date fecha = null;
                    try {

                      fecha = formatoDelTexto.parse(strFecha);
                    }
                    catch (ParseException ex) {
                      LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
                    }
                    para.setFecha_fin(fecha);

                    if (para.getFecha_fin() == null) {

                    }

                  }
                  else {
                    para.setFecha_fin(cell.getDateCellValue());
                  }

              }
            }
            catch (Exception exfh) {
              lError = true;
              lineaError = countRow;
              columnaError = countCell;
              msgError += "En la linea " + lineaError + " y la columna " + columnaError + " de la pestaña "
                  + paginaError;
              LOG.error("Error: ", exfh.getClass().getName(), exfh.getMessage());
            }
            countCell++;
          }
          try {
            java.util.Date fechaSistema = new Date();

            if (!regla.getCdCodigo().equals(cdCodigoOld)) {
              Tmct0Regla buscaRegla = tmct0ReglaBo.findByCdCodigo(regla.getCdCodigo());
              if (buscaRegla == null) {
                Tmct0Regla regla2 = new Tmct0Regla();

                regla2.setCdCodigo(regla.getCdCodigo());
                regla2.setNbDescripcion(regla.getNbDescripcion());

                regla2.setFechaCreacion(fechaSistema);
                regla2.setAuditFechaCambio(fechaSistema);
                regla2.setAuditUser("Carlos");
                if (fechaSistema.before(para.getFecha_fin())) {
                  regla2.setActivo("1");
                }
                else {
                  regla2.setActivo("0");
                }
                cdCodigoOld = regla2.getCdCodigo();
                Tmct0Regla retorno = tmct0ReglaBo.save(regla2);
                idRegla = retorno.getIdRegla();
                nCountRegla++;
              }
              else {
                idRegla = buscaRegla.getIdRegla();
              }

            }
            Tmct0parametrizacion parametrizacion = new Tmct0parametrizacion();

            Tmct0Regla id_regla = tmct0ReglaBo.findIdRegla(idRegla);
            parametrizacion.setTmct0Regla(id_regla);

            if (para.getDescripcion() != null)
              parametrizacion.setDescripcion(para.getDescripcion());

            if (para.getTmct0CamaraCompensacion() != null) {
              Long idCamara = para.getTmct0CamaraCompensacion().getIdCamaraCompensacion();
              Tmct0CamaraCompensacion camaraCompensacion = tmct0CamaraCompensacionBo.findById(idCamara);
              parametrizacion.setTmct0CamaraCompensacion(camaraCompensacion);
            }

            if (para.getTmct0Nemotecnicos() != null) {
              Long idNemotecnico = para.getTmct0Nemotecnicos().getId_nemotecnico();
              Tmct0Nemotecnicos nemotecnico = tmct0NemotecnicosBo.findById(idNemotecnico);
              parametrizacion.setTmct0Nemotecnicos(nemotecnico);
            }

            if (para.getTmct0CuentasDeCompensacion() != null) {
              Long idCuentaCompensac = para.getTmct0CuentasDeCompensacion().getIdCuentaCompensacion();
              Tmct0CuentasDeCompensacion idCuentaCompensacion = tmct0TipoCuentaDeCompensacionBo
                  .findById(idCuentaCompensac);
              parametrizacion.setTmct0CuentasDeCompensacion(idCuentaCompensacion);
            }

            if (para.getTmct0Segmento() != null) {
              int id_segment = para.getTmct0Segmento().getId_segmento();
              Tmct0Segmento id_segmento = tmct0SegmentoBo.findById(id_segment);
              parametrizacion.setTmct0Segmento(id_segmento);
            }

            if (para.getTmct0IndicadorCapacidad() != null) {
              Long idIndicadorCapacid = para.getTmct0IndicadorCapacidad().getId_indicador_capacidad();
              Tmct0IndicadorDeCapacidad id_indicador_capacidad = tmct0IndicadorDeCapacidadBo
                  .findById(idIndicadorCapacid);
              parametrizacion.setTmct0IndicadorCapacidad(id_indicador_capacidad);
            }

            if (para.getTmct0Compensador() != null) {
              Long idCompensad = para.getTmct0Compensador().getIdCompensador();
              Tmct0Compensador idCompensador = tmct0CompensadorBo.findById(idCompensad);
              parametrizacion.setTmct0Compensador(idCompensador);
            }

            if (para.getReferenciaCliente() != null)
              parametrizacion.setReferenciaCliente(para.getReferenciaCliente());

            if (para.getTmct0ModeloDeNegocio() != null) {
              Long idModeloNegoci = para.getTmct0ModeloDeNegocio().getId_modelo_de_negocio();
              Tmct0ModeloDeNegocio id_modelo_de_negocio = tmct0ModeloDeNegocioBo.findById(idModeloNegoci);
              parametrizacion.setTmct0ModeloDeNegocio(id_modelo_de_negocio);
            }

            if (para.getCdReferenciaAsignacion() != null)
              parametrizacion.setCdReferenciaAsignacion(para.getCdReferenciaAsignacion());

            if (para.getReferenciaExterna() != null)
              parametrizacion.setReferenciaExterna(para.getReferenciaExterna());

            if (para.getFecha_fin() != null)
              parametrizacion.setFecha_fin(para.getFecha_fin());
            else
              parametrizacion.setFecha_fin(fechaSistema);

            if (fechaAuditoria.before(parametrizacion.getFecha_fin())) {
              parametrizacion.setActivo("1");
            }
            else {
              parametrizacion.setActivo("0");
            }
            parametrizacion.setFecha_inicio(fechaSistema);
            parametrizacion.setAuditFechaCambio(fechaSistema);
            parametrizacion.setAuditUsuario("Carlos");

            parametrizacion = tmct0parametrizacionBo.save(parametrizacion);
            nCountParam++;
          }
          catch (Exception exsa) {
            lError = true;
            lineaError = countRow;
            columnaError = countCell;
            msgError += "En la linea " + lineaError + " de la pestaña " + paginaError + "<br/>";
            LOG.error("Error: ", exsa.getClass().getName(), exsa.getMessage());
          }
        }
        countRow++;

      }

      paginaError++;
      String cdCodigo = null;
      sheet = workbook.getSheetAt(1);
      countRow = 0;
      rows = sheet.rowIterator();
      String cdaliass = "";
      while (rows.hasNext()) {
        HSSFRow row = (HSSFRow) rows.next();
        if (countRow > 0) {
          try {
            alias = new Tmct0ali();
            Iterator cells = row.cellIterator();
            countCell = 0;
            while (cells.hasNext()) {
              try {
                HSSFCell cell = (HSSFCell) cells.next();
                switch (countCell) {
                  case 0:
                    cdCodigo = cell.getStringCellValue();
                    break;
                  case 1:
                    aliasId = new Tmct0aliId();

                    if (cell.getCellType() == 1) {
                      aliasId.setCdaliass(cell.getStringCellValue());
                    }
                    else {
                      cdaliass = Double.toString(cell.getNumericCellValue());
                      if (cdaliass.indexOf(".") != -1) {
                        cdaliass = cdaliass.substring(0, cdaliass.indexOf("."));
                      }
                      aliasId.setCdaliass(cdaliass);

                    }

                    alias.setId(aliasId);
                    break;
                }
              }
              catch (Exception exr) {
                lError = true;
                lineaError = countRow;
                columnaError = countCell;
                msgError += "En la linea " + lineaError + " y la columna " + columnaError + " de la pestaña "
                    + paginaError + "<br/>";
                LOG.error("Error: ", exr.getClass().getName(), exr.getMessage());
              }
              countCell++;
            }
            reglaBusqueda = tmct0ReglaBo.findByCdCodigo(cdCodigo);

            cdaliass = alias.getCdaliass();

            alias = tmct0aliBo.findFirst1ByCdaliass(cdaliass);

            if (alias != null) {
              alias.setIdRegla(reglaBusqueda.getIdRegla().longValue());
              alias.setFhaudit(fechaAuditoria);
              tmct0aliBo.save(alias);
              nCountAlias++;
            }
          }
          catch (Exception exfh) {
            lError = true;
            lineaError = countRow;
            msgError += "En la linea " + lineaError + " de la pestaña " + paginaError + "<br/>";
            LOG.error("Error: ", exfh.getClass().getName(), exfh.getMessage());
          }
        }
        countRow++;
      }

      paginaError++;
      cdCodigo = null;
      sheet = workbook.getSheetAt(2);
      countRow = 0;
      rows = sheet.rowIterator();
      String cdSubCta = "";
      while (rows.hasNext()) {
        HSSFRow row = (HSSFRow) rows.next();

        if (countRow > 0) {
          try {

            Iterator cells = row.cellIterator();
            countCell = 0;
            while (cells.hasNext()) {
              try {
                HSSFCell cell = (HSSFCell) cells.next();
                switch (countCell) {
                  case 0:
                    cdCodigo = cell.getStringCellValue();
                    break;
                  case 1:
                    if (cell.getCellType() == 1) {
                      cdaliass = cell.getStringCellValue();
                    }
                    else {
                      cdaliass = Double.toString(cell.getNumericCellValue());
                      if (cdaliass.indexOf(".") != -1) {
                        cdaliass = cdaliass.substring(0, cdaliass.indexOf("."));
                      }
                    }
                    break;
                  case 2:
                    if (cell.getCellType() == 1) {
                      cdaliass = cell.getStringCellValue();
                    }
                    else {
                      cdSubCta = Double.toString(cell.getNumericCellValue());
                      if (cdSubCta.indexOf(".") != -1) {
                        cdSubCta = cdSubCta.substring(0, cdSubCta.indexOf("."));
                      }
                    }
                    break;

                }
              }
              catch (Exception exr) {
                lError = true;
                lineaError = countRow;
                columnaError = countCell;
                msgError += "En la linea " + lineaError + " y la columna " + columnaError + " de la pestaña "
                    + paginaError + "<br/>";
                LOG.error("Error: ", exr.getClass().getName(), exr.getMessage());
              }
              countCell++;
            }

            Tmct0act act = new Tmct0act();

            act = tmct0actBo.getAliasSubCta(cdaliass, cdSubCta);

            if (act != null) {
              reglaBusqueda = tmct0ReglaBo.findByCdCodigo(cdCodigo);

              act.setIdRegla(reglaBusqueda.getIdRegla().longValue());
              act.setFhaudit(fechaAuditoria);

              tmct0actBo.save(act);
              nCountSubCta++;
            }
          }
          catch (Exception exfh) {
            lError = true;
            lineaError = countRow;
            msgError += "En la linea " + lineaError + " de la pestaña " + paginaError + "<br/>";
            LOG.error("Error: ", exfh.getClass().getName(), exfh.getMessage());
          }
        }
        countRow++;
      }

    }
    catch (IOException e) {
      LOG.error("Error: ", e.getClass().getName(), e.getMessage());
    }
    finally {
      if (fis != null) {
        try {
          fis.close();
          File fileDelete = new File(FOLDER_DESTINO + excelFileName);
          fileDelete.delete();
        }
        catch (Exception exf) {
          lError = true;
          LOG.error("Error: ", exf.getClass().getName(), exf.getMessage());
        }
      }
      Map<String, String> resultado = new Hashtable<String, String>();
      if (!lError)
        resultado.put(RETURN_IMPORT_FILE, "Se han importado " + nCountRegla + " reglas correctamente. Con "
            + nCountParam + " parametrizaciones importadas correctamente." + " Con " + nCountAlias + " Alias y "
            + nCountSubCta + " SubCuentas.");
      else
        resultado.put(RETURN_IMPORT_FILE, msgError);

      webResponse.setResultados(resultado);
    }
  }

  private Date getDiaHabil(Date fecha) {

    List<Festivo> diasFestivos = festivoDao.findAll();

    for (Festivo festivo : diasFestivos) {
      if (festivo.getFecha().equals(fecha)) {
        fecha = getDiaHabil(sumarRestarDiasFecha(fecha, -1));
      }
    }

    return fecha;

  }

  public Date sumarRestarDiasFecha(Date fecha, int dias) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(fecha); // Configuramos la fecha que se recibe
    calendar.add(Calendar.DAY_OF_YEAR, dias); // numero de días a añadir, o restar en caso de días<0
    return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
  }

}
