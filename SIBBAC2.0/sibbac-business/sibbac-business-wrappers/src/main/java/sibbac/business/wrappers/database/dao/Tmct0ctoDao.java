package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0ctoId;

@Repository
public interface Tmct0ctoDao extends JpaRepository<Tmct0cto, Tmct0ctoId> {

  @Query("select c from Tmct0cto c where c.id.nuorden = :nuorden ")
  List<Tmct0cto> findAllByNuorden(@Param("nuorden") String nuorden);

}
