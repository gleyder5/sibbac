package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO;
import sibbac.database.bsnbpsql.tcli.model.Tcli0com;
import sibbac.database.bsnbpsql.tcli.model.Tcli0comId;

@Repository
public interface Tcli0comDao extends JpaRepository<Tcli0com, Tcli0comId> {

  @Query("select new sibbac.business.wrappers.database.bo.calculos.dto.Tcli0comDTO(c.id.cdproduc, c.id.nuctapro, c.pccomsis, c.pcbansis) from Tcli0com c  "
      + "where c.id.nuctapro = :pnuctapro  and c.id.cdproduc = :pcdproduc  order by c.id.cdproduc asc, c.id.nuctapro asc, c.pcbansis desc, c.pccomsis desc, c.fhmodifi desc")
  Tcli0comDTO findAllTcli0comByCdproducAndNuctapro(@Param("pcdproduc") String cdproduc, @Param("pnuctapro") int cdaliass);

}
