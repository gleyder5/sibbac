package sibbac.business.wrappers.helper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.common.Constantes;

/**
 * Helper de utilidad para representacion de loggers generico interproyectos.
 * @deprecated El uso de esta clase de utilería impide la correcta traza de las líneas dónde
 * ocurren los errores, dificultando la solución de incidencias.
 */
@Deprecated
public class GenericLoggers {

  /**
   * Representacion de log de inicio / fin.
   * 
   * @param classLogged: clase de logueo
   * @param funcionalidad: nombre de la funcionalidad involucrada
   * @param methodName: nombre metodo
   * @param inicio: si es true se toma como inicio, caso contrario es false 
   * 	Ejemplo: XXXXX. Inicio
   */
  public static void logDebugInicioFin(
		  Class<?> classLogged, String funcionalidad, String methodName, boolean inicio) {
	StringBuffer sbLog = new StringBuffer();
	sbLog.append(methodName).append(" - ").append(funcionalidad);
    if (inicio) {
    	sbLog.append(". Inicio");
    } else {
    	sbLog.append(". Fin");
    }
    Logger LOG = LoggerFactory.getLogger(classLogged);
    LOG.debug(sbLog.toString());
  }

  /**
   * Representacion de log de Error.
   * 
   * @param classLogged: clase de logueo
   * @param funcionalidad: nombre de la funcionalidad involucrada
   * @param methodName: nombre metodo
   * @param mensajeError: mensaje de error preferentemente o stack trace
   * 	Ejemplo: XXXXX. Error: xxxx
   */
  public static void logError(
		  Class<?> classLogged, String funcionalidad, String methodName, String mensajeError) {

	StringBuffer sbLog = new StringBuffer();
	sbLog.append(methodName).append(" - ").append(funcionalidad);
	sbLog.append(". Error ");
    if (StringUtils.isNotBlank(mensajeError)) {
    	sbLog.append(mensajeError);
    }
    Logger LOG = LoggerFactory.getLogger(classLogged);
    LOG.error(sbLog.toString());
  }
  
  /**
   * Representacion de log de Debug.
   * 
   * @param classLogged: clase de logueo
   * @param funcionalidad: nombre de la funcionalidad involucrada
   * @param methodName: nombre metodo
   * @param mensaje: mensaje
   * 	Ejemplo: XXXXX. xxxx
   */
  public static void logDebug(
		  Class<?> classLogged, String funcionalidad, String methodName, String mensaje) {

	StringBuffer sbLog = new StringBuffer();
	sbLog.append(methodName).append(" - ").append(funcionalidad).append(" ");
    if (StringUtils.isNotBlank(mensaje)) {
    	sbLog.append(mensaje);
    }
    Logger LOG = LoggerFactory.getLogger(classLogged);
    LOG.debug(sbLog.toString());
  }
  
  
  /**
   *  	Logging de inicio/fin para interfaces emitidas.
   * 	@param classLogged: clase de logueo
   * 	@param methodName: nombre metodo
   *    @param inicio: si es true es INICIO, false es FIN
   */
  public static void logDebugInicioFinInterfEmitidas(
		 Class<?> classLogged, String methodName, boolean inicio) {
	  logDebugInicioFin(classLogged, Constantes.INTERFACES_EMITIDAS, methodName, inicio);
  }

  /**
   * 	Logging de error interfaces emitidas.
   * 	@param classLogged: clase de logueo 	
   * 	@param methodName: nombre metodo
   * 	@param mensajeError: mensaje de error
   */
  public static void logErrorInterfEmitidas(
		  Class<?> classLogged, String methodName, String mensajeError) {
	  logError(classLogged, Constantes.INTERFACES_EMITIDAS, methodName, mensajeError);
  }
  
  /**
   * 	Logging de debug interfaces emitidas.
   * 	@param classLogged: clase de logueo 	
   * 	@param methodName: nombre metodo
   * 	@param mensajeError: mensaje de error
   */
  public static void logDebugInterfEmitidas(
		  Class<?> classLogged, String methodName, String mensaje) {
	  logDebug(classLogged, Constantes.INTERFACES_EMITIDAS, methodName, mensaje);
  }
    
}
