package sibbac.business.wrappers.database.dao.specifications;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;


public class OperacioncdeccSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( OperacioncdeccSpecifications.class );

	public static Specification< Tmct0operacioncdecc > fechaContratacionDesde( final Date fecha ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::fechaContratacionDesde] [fcontratacion=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fcontratacion" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > fechaContratacionHasta( final Date fecha ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::fechaContratacionHasta] [fcontratacion=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fcontratacion" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > imtitulosdisponiblesgGreaterThan() {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate greaterThanPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::imtitulosdisponiblesMoreThan] [imtitulosdisponibles>]", 0 );
				greaterThanPredicate = builder.greaterThan( root.< BigDecimal > get( "imtitulosdisponibles" ), BigDecimal.ZERO );
				return greaterThanPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > fechaLiquidacionDesde( final Date fecha ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::fechaLiquidacionDesde] [fliqteorica=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fliqteorica" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > fechaLiquidacionHasta( final Date fecha ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::fechaLiquidacionHasta] [fliqteorica=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fliqteorica" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > isin( final String isin ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate isinPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::isin] [isin=={}]", isin );
				if ( isin != null ) {
					if(isin.indexOf("#") != -1)
						isinPredicate = builder.notEqual( root.< String > get( "cdisin" ), isin.replace("#", "") );
					else
						isinPredicate = builder.equal( root.< String > get( "cdisin" ), isin );
				}
				return isinPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > camara( final String camara ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::camara] [camara=={}]", camara );
				if ( camara != null ) {
					if(camara.indexOf("#") != -1)
						camaraPredicate = builder.equal( root.< String > get( "cdcamara" ), camara.replace("#", "") );
					else
						camaraPredicate = builder.equal( root.< String > get( "cdcamara" ), camara );
				}
				return camaraPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > codCuenta( final String codCuenta ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::cdCodigo] [cdCodigo=={}]", codCuenta );
				if ( codCuenta != null ) {
					if(codCuenta.indexOf("#") != -1)
						camaraPredicate = builder.notEqual( root.< Tmct0CuentasDeCompensacion > get( "tmct0CuentasDeCompensacion" ).< String > get( "cdCodigo" ), codCuenta.replace("#", "") );
					else
						camaraPredicate = builder.equal( root.< Tmct0CuentasDeCompensacion > get( "tmct0CuentasDeCompensacion" ).< String > get( "cdCodigo" ), codCuenta );
				}
				return camaraPredicate;
			}
		};
	}
	
	public static Specification< Tmct0operacioncdecc > codCuentaDiaria( final String codCuenta ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::cdCodigo] [cdCodigo=={}]", codCuenta );
				if ( codCuenta != null ) {
					if(codCuenta.indexOf("#") != -1)
						camaraPredicate = builder.notEqual( root.< Tmct0CuentasDeCompensacion > get( "tmct0CuentasDeCompensacion" )
								.< Tmct0TipoCuentaDeCompensacion >get("tmct0TipoCuentaDeCompensacion")
								.< String > get( "cdCodigo" ), codCuenta.replace("#", "") );
					else
						camaraPredicate = builder.equal( root.< Tmct0CuentasDeCompensacion > get( "tmct0CuentasDeCompensacion" )
								.< Tmct0TipoCuentaDeCompensacion >get("tmct0TipoCuentaDeCompensacion")
								.< String > get( "cdCodigo" ), codCuenta );						
				}
				return camaraPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > notCodCuenta( final String codCuenta ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::camara] [tmct0CuentasDeCompensacion.cdCodigo=={}]", codCuenta );
				if ( codCuenta != null ) {
					camaraPredicate = builder.notEqual( root.< Tmct0CuentasDeCompensacion > get( "tmct0CuentasDeCompensacion" )
							.< String > get( "cdCodigo" ), codCuenta );
				}
				return camaraPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > isNullCodCuenta() {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate camaraPredicate = null;
				LOG.trace( "[OperacioncdeccSpecifications::camara] [tmct0CuentasDeCompensacion.codCuenta=={}]" );
				camaraPredicate = builder.isNull( root.< Tmct0CuentasDeCompensacion > get( "tmct0CuentasDeCompensacion" ).< String > get(
						"cdCodigo" ) );

				return camaraPredicate;
			}
		};
	}

	public static Specification< Tmct0operacioncdecc > listadoFriltrado( final Date fContratacionDesde, final Date fContratacionHasta,
			final Date fLiquidacionDesde, final Date fLiquidacionHasta, final String camara, final String isin, final String codCuenta,
			final boolean coincidirCuenta ) {
		return new Specification< Tmct0operacioncdecc >() {

			public Predicate toPredicate( Root< Tmct0operacioncdecc > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0operacioncdecc > specs = null;
				if ( fContratacionDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaContratacionDesde( fContratacionDesde ) );
					}

				}
				if ( fContratacionHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaContratacionHasta( fContratacionHasta ) );
					} else {
						specs = specs.and( fechaContratacionHasta( fContratacionHasta ) );
					}

				}
				if ( fLiquidacionDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaLiquidacionDesde( fLiquidacionDesde ) );
					} else {
						specs = specs.and( fechaLiquidacionDesde( fLiquidacionDesde ) );
					}

				}
				if ( fLiquidacionHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaLiquidacionHasta( fLiquidacionHasta ) );
					} else {
						specs = specs.and( fechaLiquidacionHasta( fLiquidacionHasta ) );
					}

				}
				if ( isin != null ) {
					if ( specs == null ) {
						specs = Specifications.where( isin( isin ) );
					} else {
						specs = specs.and( isin( isin ) );
					}

				}
				if ( camara != null ) {
					if ( specs == null ) {
						specs = Specifications.where( camara( camara ) );
					} else {
						specs = specs.and( camara( camara ) );
					}

				}
				if ( codCuenta != null ) {
					if ( specs == null ) {
						if ( coincidirCuenta ) {
							specs = Specifications.where( codCuenta( codCuenta ) );
						} else {
							specs = Specifications.where( notCodCuenta( codCuenta ) ).or( isNullCodCuenta() );
						}
					} else {
						if ( coincidirCuenta ) {
							specs = specs.and( codCuenta( codCuenta ) );
						} else {
							specs = specs.and( notCodCuenta( codCuenta ) );
						}
					}

				}
				if ( specs == null ) {
					specs = Specifications.where( imtitulosdisponiblesgGreaterThan() );
				} else {
					specs = specs.and( imtitulosdisponiblesgGreaterThan() );
				}
				if ( specs == null ) {
					specs = Specifications.where( codCuentaDiaria("CD" ) );
				} else {
					specs = specs.and( codCuentaDiaria("CD" ) );
				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}
}
