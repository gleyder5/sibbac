package sibbac.business.wrappers.database.bo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.dao.EtiquetasDao;
import sibbac.business.wrappers.database.dao.FacturaDao;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.dao.specifications.FacturaSpecificacions;
import sibbac.business.wrappers.database.dto.FacturaDTO;
import sibbac.business.wrappers.database.dto.FacturaTransicionDTO;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Etiquetas;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaDetalle;
import sibbac.business.wrappers.database.model.FacturaSugerida;
import sibbac.business.wrappers.database.model.GruposImpositivos;
import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.business.wrappers.database.model.Monedas;
import sibbac.business.wrappers.database.model.Parametro;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.business.wrappers.database.model.TipoDeDocumento;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.common.ExportType;
import sibbac.common.PDFMail;
import sibbac.common.XLSFactura;
import sibbac.common.currency.CurrencyConverter;
import sibbac.common.export.excel.ExcelBuilder;
import sibbac.common.export.pdf.PDFBuilder;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.JobPK;

@Service
public class FacturaBo extends AbstractBo<Factura, Long, FacturaDao> {

  private static final Character COMPRA = 'C';

  @Autowired
  private FacturaSugeridaBo facturaSugeridaBo;
  @Autowired
  private Tmct0estadoBo estadoBo;
  @Autowired
  private MonedasBo monedaBo;
  @Autowired
  private PeriodosBo periodoBo;
  @Autowired
  private TipoDeDocumentoBo tipoDeDocumentoBo;
  @Autowired
  private NumeradoresBo numeradoresBo;
  @Autowired
  private Tmct0cliBo tmct0cliBo;
  @Autowired
  private GruposImpositivosBo grupoImpositivoBo;
  @Autowired
  private PDFBuilder pdfBuilder;
  @Autowired
  private ExcelBuilder excelBuilder;
  @Autowired
  private ParametroBo parametroBo;
  @Autowired
  private TextosIdiomasBo textoIdiomaBo;
  @Autowired
  private SendMail sendMail;
  @Autowired
  private EtiquetasDao etiquetasDao;
  @Autowired
  private GestorServicios gestorServicios;
  @Autowired
  private Tmct0infocompensacionDao infoCompDao;
  @Autowired
  private Tmct0alcBo alcBo;
  @Autowired
  private FacturaDao facturaDao;

  /**
   * Genera las facturas en base a la lista de facturas sugeridas
   * 
   * @return Lista de facturas generadas
   */
  @Transactional
  public List<Factura> generarFacturas(final List<String> taskMessages) {
    final String prefix = "[FacturaBo::generarFacturas] ";
    final List<Factura> facturasGeneradas = new ArrayList<Factura>();
    final Date fechaActual = new Date();

    final List<FacturaSugerida> facturasSugeridas = facturaSugeridaBo.findByEstado(EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA);
    // Si hay facturas sugeridas...
    if (CollectionUtils.isEmpty(facturasSugeridas)) {
      taskMessages.add("No se han localizado facturas sugeridas en estado " + EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA.name());
    } else {
      taskMessages.add("Se han localizado " + facturasSugeridas.size() + " facturas sugeridas en estado "
                       + EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA.name());

      final Tmct0estado estadoFacturada = estadoBo.findByIdFacturaSugerida(EstadosEnumerados.FACTURA_SUGERIDA.FACTURADA);
      final Tmct0estado estadoPendienteEnvio = estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.PTE_ENVIO);
      final Tmct0estado estadoDevengadaPendienteFacturar = estadoBo.findByIdContabilidad(EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA);
      final Monedas moneda = monedaBo.findByCodigo(Monedas.COD_MONEDA_EUR);
      final TipoDeDocumento tipoDeDocumento = tipoDeDocumentoBo.getTipoDeDocumento(TipoDeDocumento.FACTURA);
      boolean informacionCompleta = true;
      final Periodos periodoActivo = periodoBo.getPeriodosActivo();
      if (periodoActivo == null) {
        informacionCompleta = false;
        taskMessages.add("Error de configuración: Periodo contable no configurado para esta fecha.");
        LOG.error(prefix + "[Error de configuración: Periodo contable no configurado para esta fecha.]");
      }
      Identificadores identificador = null;
      if (informacionCompleta) {
        LOG.trace(prefix + "[Periodo activo: {}]", periodoActivo.getNombre());
        identificador = periodoActivo.getIdentificador();
        if (identificador == null) {
          informacionCompleta = false;
          taskMessages.add("Error de configuración: Identificador no configurado para este periodo contable.");
          LOG.error("[Error de configuración: Identificador no configurado para este periodo contable.]");
        }
      }
      GruposImpositivos grupoImpositivo = null;
      if (informacionCompleta) {
        LOG.trace(prefix + "[Identificador: {}]", identificador);
        grupoImpositivo = grupoImpositivoBo.findGrupoImpositivoActivo();
        if (grupoImpositivo == null) {
          informacionCompleta = false;
          taskMessages.add("Error de configuración: Grupo Impositivo no configurado para esta fecha.");
          LOG.error("[Error de configuración: Grupo Impositivo no configurado para esta fecha.]");
        }
      }
      if (informacionCompleta) {
        LOG.trace(prefix + "Tenemos toda la informacion necesaria. Iniciando el bucle...");
        for (FacturaSugerida facturaSugerida : facturasSugeridas) {
          LOG.trace(prefix + "+ Factura sugerida: [{}]", facturaSugerida.getId());
          informacionCompleta = true;
          Factura factura = crearFactura(fechaActual, estadoPendienteEnvio, estadoDevengadaPendienteFacturar, moneda, periodoActivo, identificador,
                                         facturaSugerida, tipoDeDocumento, grupoImpositivo, taskMessages);
          if (factura != null) {
            this.save(factura);
            taskMessages.add("Factura generada correctamente " + factura.getNumeroFactura());
            LOG.trace(prefix + "  Factura generada.");
            facturasGeneradas.add(factura);
            LOG.trace(prefix + "  Cambiando el estado a la factura sugerida.");
            facturaSugerida = facturaSugeridaBo.actualizarEstado(facturaSugerida, estadoFacturada);
            taskMessages.add("Factura sugerida actualizada");
          } else {
            LOG.error("Ha sido imposible generar la factura.");
          }

        }
      } else {
        LOG.error("No es posible generar facturas por ausencia de informacion");
      }
    }
    return facturasGeneradas;
  }

  /**
   * @param fechaActual
   * @param estadoPendienteEnvio
   * @param estadoContable
   * @param moneda
   * @param periodoActivo
   * @param identificador
   * @param facturaSugerida
   * @param alias
   * @param tipoDeDocumento
   * @param grupoImpositivo
   * @param estadoAlcLibre
   * @param taskMessages
   * @return
   */
  private Factura crearFactura(final Date fechaActual,
                               final Tmct0estado estadoPendienteEnvio,
                               final Tmct0estado estadoContable,
                               final Monedas moneda,
                               final Periodos periodoActivo,
                               final Identificadores identificador,
                               final FacturaSugerida facturaSugerida,
                               final TipoDeDocumento tipoDeDocumento,
                               final GruposImpositivos grupoImpositivo,
                               final List<String> taskMessages) {
    final String prefix = "[FacturaBo::generarFacturas] ";
    final Factura factura = new Factura();
    boolean informacionCompleta = true;
    final Alias alias = facturaSugerida.getAlias();
    LOG.trace(prefix + "[Generando una nueva factura para el cdalias {}]", alias.getCdaliass());
    taskMessages.add("Procesando la factura sugerida para el cdalias " + alias.getCdaliass());
    final String cdbrocli = alias.getCdbrocli();
    final Tmct0cli tmct0cli = tmct0cliBo.buscarClienteActivo(cdbrocli);
    if (tmct0cli == null) {
      informacionCompleta = false;
      LOG.error(prefix + "[Error de configuración: No ha sido posible localizar el cliente activo para el alias de facturacion: {}]", cdbrocli);
      taskMessages.add("Error de configuración: No ha sido posible localizar el cliente activo para el alias de facturacion " + cdbrocli);
    }
    Long numeroFactura = null;
    if (informacionCompleta) {
      LOG.trace(prefix + "    + Cliente activo: [CIF=={}]", tmct0cli.getCif());
      numeroFactura = numeradoresBo.calcularNumeroSiguiente(tipoDeDocumento, identificador);
      if (numeroFactura == null) {
        informacionCompleta = false;
        LOG.error(prefix + "[Error de configuración: No ha sido posible obtener un numero de factura!]");
        taskMessages.add("Error de configuración: No ha sido posible obtener un numero de factura!");
      }
    }
    if (informacionCompleta) {
      LOG.trace(prefix + "    + Numero de Factura: [{}]", numeroFactura);

      LOG.trace(prefix + "    Generando la factura...");
      factura.setCif(tmct0cli.getCif());
      factura.setDireccion(alias.getDireccionCompleta());
      factura.setFacturaSugerida(facturaSugerida);
      factura.setEstado(estadoPendienteEnvio);
      factura.setEstadocont(estadoContable);
      factura.setFechaCreacion(fechaActual);
      factura.setFechaFactura(fechaActual);
      factura.setMoneda(moneda);
      factura.setNombreAlias(alias.getDescrali());
      factura.setTipoDeDocumento(tipoDeDocumento);
      factura.setNumeroFactura(numeroFactura);
      factura.setPeriodo(periodoActivo);
      factura.setPorcentaje(grupoImpositivo.getPorcentaje());
      LOG.trace(prefix + "    Factura generada correctamente.");
    } else {
      taskMessages.add("Error: No se ha generado la factura para el cdalias " + alias.getCdaliass());
    }
    return factura;
  }

  /**
   * Recupera las facturas rectificativas en estado <code>EstadosEnumerados.FACTURA.PTE_COBRO</code> y la marca como
   * <code>EstadosEnumerados.FACTURA.RECTIFICATIVA</code>
   * 
   * @param idsFacturas
   * @return
   */
  @Transactional
  public List<Long> marcarFacturasRectificar(final List<Long> idsFacturas) {
    final String prefix = "[FacturaBo::marcarFacturasRectificar] ";
    List<Factura> facturas = (List<Factura>) dao.findAll(idsFacturas);
    List<Long> idsFacturasMarcadas = new ArrayList<Long>();
    if (CollectionUtils.isNotEmpty(facturas)) {
      Tmct0estado estadoPteRectificar = estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.PTE_RECTIFICAR);

      for (Factura factura : facturas) {
        final Tmct0estado estadoActual = factura.getEstado();
        LOG.trace(prefix + "Explorando la factura [ID: {}] [estado=={} vs {}]", factura.getId(), estadoActual.getNombre(),
                  estadoPteRectificar.getNombre());
        if (EstadosEnumerados.FACTURA.PTE_COBRO.getId().equals(estadoActual.getIdestado())) {
          LOG.trace(prefix + "+ Factura en estado PDT_COBRO");
          factura.setEstado(estadoPteRectificar);
          factura = dao.save(factura);
          idsFacturasMarcadas.add(factura.getId());
          LOG.trace(prefix + "  Factura rectificada [ID: {}]", factura.getId());
        } else {
          LOG.info(prefix + "- Factura no rectificable [ID: {}]", factura.getId());
        }
      }
    }
    return idsFacturasMarcadas;
  }

  /**
   * Envía las facturas que estén pendientes de enviar.
   * 
   * @param jobPk Primary key de la tarea que ejecuta la operación.
   * @param taskMessages Lista de mensajes generados durante el proceso.
   * @return <code>List<Factura> - </code>Lista de facturas generadas.
   */
  @Transactional
  public List<Factura> enviarFacturas(JobPK jobPk, final List<String> taskMessages) {
    final String prefix = "[FacturaBo::enviarFacturas] ";

    final Tmct0estado estadoPteEnvio = estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.PTE_ENVIO);

    List<Factura> facturasPteEnvio = (List<Factura>) dao.findByEstado(estadoPteEnvio);
    List<Factura> facturasEnviadas = new ArrayList<Factura>();

    if (CollectionUtils.isNotEmpty(facturasPteEnvio)) {
      taskMessages.add("Se han encontrado " + facturasPteEnvio.size() + " facturas en estado " + EstadosEnumerados.FACTURA.PTE_ENVIO.name());
      final Tmct0estado estadoPteCobro = estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.PTE_COBRO);

      for (final Factura facturaPteEnvio : facturasPteEnvio) {
        final Long numeroFactura = facturaPteEnvio.getNumeroFactura();
        taskMessages.add("Enviando correo de la factura " + numeroFactura);
        LOG.trace(prefix + "Factura [" + numeroFactura + "] PTE_ENVIO");
        this.enviarCorreo(jobPk, facturaPteEnvio, false, taskMessages);
        taskMessages.add("Correo enviado para la factura " + numeroFactura);
        LOG.trace(prefix + "Factura [" + numeroFactura + "] Correo enviado");
        facturaPteEnvio.setEstado(estadoPteCobro);
        facturasEnviadas.add(facturaPteEnvio);
        LOG.trace(prefix + "  Factura enviada [ID: {}, Numero Factura: {}]", facturaPteEnvio.getId(), numeroFactura);
      } // for

      facturasEnviadas = dao.save(facturasEnviadas);
    } else {
      taskMessages.add("No se han encontrado facturas en estado " + EstadosEnumerados.FACTURA.PTE_ENVIO.name());
    } // else

    return facturasEnviadas;
  } // enviarFacturas

  /**
   * Genera la factura especificada en pdf y xls y las envía por correo electrñonico.
   * 
   * @param jobPk Primary key de la tarea que ejecuta la operación.
   * @param facturaPteEnvio Factura a generar y enviar.
   * @param duplicado Indica si lo que se debe generar es un duplicado de la factura.
   * @param taskMessages Lista de mensajes generados durante el proceso.
   */
  public void enviarCorreo(final JobPK jobPk, final Factura facturaPteEnvio, boolean duplicado, final List<String> taskMessages) {
    String prefix = "[FacturaBo::enviarCorreo] ";
    LOG.debug(prefix + "[JobPK=={}]", jobPk);

    // Generamos el excel
    final InputStream inputExcel = crearExcel(facturaPteEnvio);
    // Generamos el PDF
    final InputStream inputPDF = crearPDF(facturaPteEnvio, duplicado);

    // Recuperamos los contactos
    final Alias alias = facturaPteEnvio.getAlias();
    // final List< Contacto > contactos = contactosBo.findAllByAlias( alias );
    final List<Contacto> contactos = gestorServicios.getContactos(alias, jobPk);

    if (contactos == null || contactos.isEmpty()) {
      LOG.error(prefix + "ERROR: No se puede enviar el correo a este alias [{}] porque carece de contactos!", alias.getCdaliass());
    } else {
      final List<String> destinatarios = new ArrayList<String>();

      for (Contacto destinatario : contactos) {
        String[] mail = destinatario.getEmail().split(";");
        for (int i = 0; i < mail.length; i++) {
          destinatarios.add(mail[i].trim());
        }
      }

      // Nombre del fichero destino (<Nombre>.<extension>)
      final List<String> nameDest = new ArrayList<String>();
      Date now = new Date();
      ExportType exportType = (duplicado) ? ExportType.PDF_DUPLICADO : ExportType.PDF;
      nameDest.add(this.generateFileNameForAttachment(ExportType.EXCEL, facturaPteEnvio, now));
      nameDest.add(this.generateFileNameForAttachment(exportType, facturaPteEnvio, now));

      // InputStream con datos de entrada
      final List<InputStream> inputstream = new ArrayList<InputStream>();
      inputstream.add(inputExcel);
      inputstream.add(inputPDF);

      // Montamos el mapping con los datos que se van a sustituir en el cuerpo
      // del Mail
      Map<String, String> mapping = createMappingCuerpoMail(facturaPteEnvio);

      // Recuperamos el body de base de datos

      /*
       * ==============================================================================================================================================
       * TOMAS. Modifico esto para que en el correo se tenga en cuenta si lo que se envía es un duplicado o es el original de la factura. Hasta ahora
       * se están enviando en el asunto y en el cuerpo lo parametrizado para duplicados en los dos casos String body =
       * textoIdiomaBo.recuperarTextoIdioma( "F", alias.getIdioma() ); body = reemplazarKeys( body, mapping ); String asunto =
       * textoIdiomaBo.recuperarTextoIdioma( "E", alias.getIdioma() ); asunto = reemplazarKeys( asunto, mapping );
       * ------------------------------------
       * ----------------------------------------------------------------------------------------------------------
       */
      String asunto = null;
      String body = null;

      if (duplicado) {
        asunto = textoIdiomaBo.recuperarTextoIdioma("E", alias.getIdioma());
        body = textoIdiomaBo.recuperarTextoIdioma("F", alias.getIdioma());
      } else {
        asunto = textoIdiomaBo.recuperarTextoIdioma("I", alias.getIdioma());
        body = textoIdiomaBo.recuperarTextoIdioma("J", alias.getIdioma());
      }

      body = reemplazarKeys(body, mapping);
      asunto = reemplazarKeys(asunto, mapping);

      /*----------------------------------------------------------------------------------------------------------------------------------------------
       * TOMAS. Fin del cambio
       ==============================================================================================================================================*/

      try {
        sendMail.sendMail(destinatarios, "Factura", body, inputstream, nameDest, null);
      } catch (MailSendException | IllegalArgumentException | MessagingException | IOException e) {
        LOG.error("Error enviando el correo de facturas", e.getCause());
      }
    }
  }

  private String generateFileNameForAttachment(ExportType e, final Factura f, final Date d) {
    StringBuffer s = new StringBuffer();

    s.append(e.getFileName());
    s.append("_");
    s.append("[" + String.valueOf(f.getNumeroFactura()) + "]");
    s.append("_");
    s.append("[" + FormatDataUtils.convertDateToFileName(d) + "]");
    s.append(".");
    if (e.equals(ExportType.EXCEL)) {
      s.append("xls");
    } else {
      s.append("pdf");
    }

    return s.toString();
  }

  /**
   * 
   * @param jobPk
   * @param idFactura
   * @param duplicado
   * @param webResponse
   * @return
   */
  @Transactional
  public boolean enviarDuplicado(final JobPK jobPk, Long idFactura, boolean duplicado, final List<String> taskMessages) {
    final String prefix = "[FacturaBo::enviarDuplicado] ";
    LOG.debug(prefix + "[JobPK=={}] [IDFactura=={}]", jobPk, idFactura);

    boolean enviado = true;

    if (idFactura != null) {
      Factura factura = findById(idFactura);
      if (factura != null) {
        Tmct0estado estado = factura.getEstado();
        if (!EstadosEnumerados.FACTURA.PTE_ENVIO.getId().equals(estado.getIdestado())) {
          enviarCorreo(jobPk, factura, true, taskMessages);
        } else {
          enviado = false;
          LOG.error("La factura [ " + factura.getId() + " ] tiene estado pendiente de enviar, por lo que no se puede enviar un duplicado");
          taskMessages.add("La factura [ " + factura.getId() + " ] tiene estado pendiente de enviar, por lo que no se puede enviar un duplicado");
        }
      } else {
        enviado = false;
        LOG.error("La factura no existe");
        taskMessages.add("La factura no existe.");
      }
    }
    return enviado;
  }

  /**
   * Crea un fichero Excel con los datos de la factuara especificada.
   * 
   * @param facturaPteEnvio Factura.
   * @return <code>InputStream - </code>Stream con los datos del fichero Excel generado.
   */
  @Transactional
  public InputStream crearExcel(final Factura facturaPteEnvio) {
    //TODO AVG Recuperar la colección de ordenes ordenada por fecha
    final List<AlcOrdenes> ordenes = facturaPteEnvio.getAlcOrdenes();
    final Map<String, String> lineasCabeceras = textoIdiomaBo.recuperarCabecerasExcelFactura(facturaPteEnvio.getAlias().getCodigoIdioma());

    Locale locale = new Locale(facturaPteEnvio.getAlias().getCodigoIdioma());

    final List<XLSFactura> lineas = new ArrayList<XLSFactura>();

    String referenciaAsignacion = null;
    Tmct0alcId alcId;
    Tmct0alc alc;
    if (CollectionUtils.isNotEmpty(ordenes)) {
      for (AlcOrdenes orden : ordenes) {
        alcId = orden.createAlcIdFromFields();
        alc = alcBo.findById(alcId);
        if(alc == null) {
            LOG.warn("[crearExcel]: Se ha buscado un alc no existente relacionado con la orden actual: {}", alcId);
            continue;
        }
        referenciaAsignacion = getReferenciaAsignacion(alc.getIdinfocomp());
        final XLSFactura linea = new XLSFactura(FormatDataUtils.convertDateToString(alc.getFeejeliq(), 
                FormatDataUtils.DATE_FORMAT_SEPARATOR),
                alc.getNucnfclt() + alc.getNucnfliq().toString(), 
                alc.getCdtpoper() == COMPRA ? lineasCabeceras.get("GENERAL_ABREV_COMPRA"): lineasCabeceras.get("GENERAL_ABREV_VENTA"),
                alc.getCdisin(), alc.getNbvalors(), alc.getCdmercad(), alc.getNbtitliq(),
                referenciaAsignacion, alc.getNutitliq(), alc.getTmct0alo().getImcammed(), alc.getImfinsvb()/* , 0.0 */);
        lineas.add(linea);
      } // for
    } // if

    InputStream inputExcel = null;
    try {
      final ByteArrayOutputStream out = excelBuilder.generaFactura(facturaPteEnvio.getNumeroFactura().toString(), lineas, lineasCabeceras, locale);

      // ALBERTO: Para probar en DES la factura sin servidor de correo
      // OutputStream outStream = null;
      // outStream = new FileOutputStream(System.getProperty("user.dir") + System.getProperty("file.separator") + "pruebaAdjuntoFactura.xls");
      // out.writeTo(outStream);
      // outStream.close();

      inputExcel = new ByteArrayInputStream(out.toByteArray());
    } catch (Exception e) {
      LOG.error("Error generando un PDF" + e);
    } // catch

    return inputExcel;
  } // crearExcel

  private String getReferenciaAsignacion(final Long idInfoCompensacion) {
    String result = "";
    if ((idInfoCompensacion != null) && !NumberUtils.LONG_ZERO.equals(idInfoCompensacion)) {
      Tmct0infocompensacion infoComp = null;
      infoComp = infoCompDao.findOne(idInfoCompensacion);
      if (infoComp != null && StringUtils.isNotEmpty(infoComp.getCdrefasignacion())) {
        result = infoComp.getCdrefasignacion();
      }
    }
    return result;

  }

  /**
   * Crea un pdf con los datos de la factura especificada.
   * 
   * @param facturaPteEnvio Factura.
   * @param duplicado Indica si se ha de generar un duplicado de la factura.
   * @return <code>InputStream - </code>Stream con los datos del fichero pdf generado.
   */
  private InputStream crearPDF(final Factura facturaPteEnvio, boolean duplicado) {
    InputStream inputPDF = null;
    try {
      FacturaDetalle facturaDetalle = facturaPteEnvio.getDetalleFactura(FacturaDetalle.NUMERO_LINEA_FACTURA);
      List<Parametro> parametros = (List<Parametro>) parametroBo.findAll();

      Map<String, String> literales = textoIdiomaBo.recuperarTextosFacturas(facturaPteEnvio.getAlias().getCodigoIdioma());

      final Parametro parametro = parametros.get(0);
      final ByteArrayOutputStream out = pdfBuilder.generaFactura(new PDFMail(facturaPteEnvio.getNombreAlias(), facturaPteEnvio.getFechaFactura(),
                                                                             facturaPteEnvio.getDireccion(), facturaPteEnvio.getNumeroFactura(),
                                                                             parametro.getRegistroMercantil(), facturaDetalle.getImfinsvb(),
                                                                             facturaPteEnvio.getImfinsvb(), facturaPteEnvio.getImporteImpuestos(),
                                                                             facturaPteEnvio.getImporteTotal(), parametro.CuentaBancariaToString(),
                                                                             parametro.IbanToString(), parametro.getBIC(), parametro.getSociedad(),
                                                                             duplicado), literales);

      // ALBERTO: Para probar en DES la factura sin servidor de correo
      // OutputStream outStream = null;
      // outStream = new FileOutputStream(System.getProperty("user.dir") + System.getProperty("file.separator") + "pruebaAdjuntoFactura.pdf");
      // out.writeTo(outStream);
      // outStream.close();

      inputPDF = new ByteArrayInputStream(out.toByteArray());
    } catch (Exception e) {
      LOG.error("Error generando un PDF" + e);
    } // catch

    return inputPDF;
  } // crearPDF

  public List<Factura> getFacturasPtesDeRectificar() {
    Tmct0estado estadoPteRectificar = estadoBo.findByIdFactura(EstadosEnumerados.FACTURA.PTE_RECTIFICAR);
    return this.dao.findByEstado(estadoPteRectificar);
  }

  /**
   * @param facturas
   * @return
   */
  private List<FacturaDTO> getListadoDTO(final Collection<Factura> facturas) {
    final List<FacturaDTO> resultado = new ArrayList<FacturaDTO>();
    if (CollectionUtils.isNotEmpty(facturas)) {
      for (final Factura factura : facturas) {
        final FacturaDTO facturaDto = new FacturaDTO(factura);

        resultado.add(facturaDto);
      }
    }
    return resultado;
  }  

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturas(final Long aliasId, final Date fechaDesde, final Date fechaHasta, final Integer idEstado) {
    final List<Integer> estados = new ArrayList<Integer>();
    if (idEstado != null) {
      estados.add(idEstado);
    }
    final List<Factura> facturas = dao.findAll(FacturaSpecificacions.listadoFriltrado(aliasId, fechaDesde, fechaHasta, estados, null));
    return getListadoDTO(facturas);

  }

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturas(final Long numeroFactura) {
    final Collection<Factura> facturas = dao.findAll(FacturaSpecificacions.listadoNumeroFactura(numeroFactura));
    return getListadoDTO(facturas);
  }

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturasEnviadas(final Long aliasId, final Date fechaDesde, final Date fechaHasta) {
    final List<Integer> estados = new ArrayList<Integer>();
    estados.add(EstadosEnumerados.FACTURA.PTE_COBRO.getId());
    final Collection<Factura> facturas = dao.findAll(FacturaSpecificacions.listadoFriltrado(aliasId, fechaDesde, fechaHasta, estados, null));
    return getListadoDTO(facturas);

  }

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturasEnviadas(final Long numeroFactura) {
    final List<Integer> estados = new ArrayList<Integer>();
    estados.add(EstadosEnumerados.FACTURA.PTE_COBRO.getId());
    final Collection<Factura> facturas = dao.findAll(FacturaSpecificacions.listadoNumeroFacturaEstado(numeroFactura, estados));
    return getListadoDTO(facturas);

  }

  @Transactional
  public List<FacturaTransicionDTO> recuperarTransiciones(final Long idFactura) {
    final List<Number> revisions = getAllRevisions(idFactura);
    final List<FacturaTransicionDTO> transiciones = new ArrayList<FacturaTransicionDTO>();
    if (CollectionUtils.isNotEmpty(revisions)) {
      Tmct0estado estadoAnterior = null;
      for (Number number : revisions) {
        final Factura factura = getRevisionForId(idFactura, number);
        final Tmct0estado estadoActual = factura.getEstado();
        if ((estadoAnterior == null) || !estadoActual.getIdestado().equals(estadoAnterior.getIdestado())) {
          estadoAnterior = estadoActual;
          final Date fechaRevision = getRevisionDate(number);
          transiciones.add(new FacturaTransicionDTO(factura.getNumeroFactura(), estadoActual.getNombre(),
                                                    FormatDataUtils.convertDateTimeToString(fechaRevision)));
        }
      }
    }
    return transiciones;
  }

  /**
   * @return
   */
  @Transactional
  public List<FacturaDTO> getListadoFacturasPendientesCobro(final Long aliasId, final Date fechaDesde, final Date fechaHasta) {
    final List<Integer> estados = new ArrayList<Integer>();
    estados.add(EstadosEnumerados.FACTURA.PTE_COBRO.getId());
    estados.add(EstadosEnumerados.FACTURA.COBRADO_PARCIALMENTE.getId());
    final Collection<Factura> facturas = dao.findAll(FacturaSpecificacions.listadoFriltrado(aliasId, fechaDesde, fechaHasta, estados, null));
    return getListadoDTO(facturas);

  }

  /**
   * Crea un mapa en el que se relacionan las etiquetas a sustituir en el mail con los valores a insertar.
   * 
   * @param factura Factura de la que obtener los valores.
   * @return <code>Map<String, String> - </code>Mapa con la relación entre etiqueta y valor a sustituir en el cuerpo del mail.
   */
  public Map<String, String> createMappingCuerpoMail(final Factura factura) {
    String key = null;
    String value = null;
    Map<String, String> mapping = new HashMap<String, String>();
    final List<Etiquetas> etiquetas = (List<Etiquetas>) etiquetasDao.findAllByTabla("Factura");
    for (Etiquetas etiqueta : etiquetas) {
      key = etiqueta.getEtiqueta();
      String campo = etiqueta.getCampo();
      switch (campo) {
        case "NombreAlias":
          value = factura.getNombreAlias();
          break;
        case "FechaFactura":
          value = FormatDataUtils.convertDateToStringSeparator((Date) factura.getFechaFactura()).toString();
          break;
        case "Cif":
          value = factura.getCif();
          break;
        case "NumeroFactura":
          value = factura.getNumeroFactura().toString();
          break;
        case "Imfinsvb":
          value = CurrencyConverter.convertToEuro(factura.getImporteTotal());
          break;
        default:
          value = "<No data>";
      }
      mapping.put(key, value.trim());
    }
    return mapping;
  } // createMappingCuerpoMail

  /**
   * Sustituye las variables del parámetro body con las especificadas en el mapping.
   * 
   * @param body Cadena donde sustituir las variables.
   * @param mapping Mapeo entre ñas variables y los valores a mostrar.
   * @return <code>String - </code>Cadena con las variables sustituidas.
   */
  public String reemplazarKeys(String body, Map<String, String> mapping) {
    for (final String key : mapping.keySet()) {
      body = body.replace(key, mapping.get(key));
    } // for

    return body;
  } // reemplazarKeys


} // FacturaBo
