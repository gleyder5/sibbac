/**
 * 
 */
package sibbac.business.wrappers.database.dto.caseejecuciones;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xIS16630
 */
public class Tmct0caseoperacionejeInformacionMercadoCaseDTO {

  /**
   * IDCASE
   * */
  private long idCase;

  /**
   * IDOPERACIONCDECC
   * */
  private long idOperacionEcc;

  /**
   * IDOPERACIONCDECC
   * */
  private String cdoperacionecc;

  /**
   * CDISIN
   */
  private String isin;
  /**
   * CDSENTIDO
   * */
  private char sentido;
  /**
   * FEEXEMKT
   * */
  private Date fechaEjecucion;
  /**
   * FEORDENMKT
   * */
  private Date fechaOrdenMercado;
  /**
   * HOORDENMKT
   * */
  private Date horaOrdenMercado;
  /**
   * CD_PLATAFORMA_NEGOCIACION
   * */
  private String plataformaNegociacion;
  /**
   * CDINDCOTIZACION
   * */
  private Character indicadorCotizacion;
  /**
   * CDMIEMBROMKT
   * */
  private String miembroMercado;
  /**
   * CDSEGMENTO
   * */
  private String segmentoEjecucion;
  /**
   * CDOPERACIONMKT
   * */
  private String tipoOperacionBolsa;
  /**
   * NUORDENMKT
   * */
  private String numeroOperacionMercado;
  /**
   * NUEXEMKT
   * */
  private String numeroEjecucionMercado;

  /**
   * NUTITIULOSTOTAL
   * */
  private BigDecimal titulosTotales;
  /**
   * NUTITILOSPENDCASE
   * */
  private BigDecimal titulosPendientesCase;

  /** Indica si el case ha sido actualizado y por lo tanto debe de ejecutarse un update del mismo en base de datos. */
  private Boolean updated;

  /**
   * @param idCase
   * @param idOperacionEcc
   * @param cdoperacionecc
   * @param isin
   * @param sentido
   * @param fechaEjecucion
   * @param fechaOrdenMercado
   * @param horaOrdenMercado
   * @param plataformaNegociacion
   * @param indicadorCotizacion
   * @param miembroMercado
   * @param segmentoEjecucion
   * @param tipoOperacionBolsa
   * @param numeroOperacionMercado
   * @param numeroEjecucionMercado
   * @param titulosTotales
   * @param titulosPendientesCase
   */
  public Tmct0caseoperacionejeInformacionMercadoCaseDTO(long idCase,
                                                        long idOperacionEcc,
                                                        String cdoperacionecc,
                                                        String isin,
                                                        Character sentido,
                                                        Date fechaEjecucion,
                                                        Date fechaOrdenMercado,
                                                        Date horaOrdenMercado,
                                                        String plataformaNegociacion,
                                                        Character indicadorCotizacion,
                                                        String miembroMercado,
                                                        String segmentoEjecucion,
                                                        String tipoOperacionBolsa,
                                                        String numeroOperacionMercado,
                                                        String numeroEjecucionMercado,
                                                        BigDecimal titulosTotales,
                                                        BigDecimal titulosPendientesCase) {
    this.idCase = idCase;
    this.idOperacionEcc = idOperacionEcc;
    this.cdoperacionecc = cdoperacionecc;
    this.isin = isin;
    this.sentido = sentido;
    this.fechaEjecucion = fechaEjecucion;
    this.fechaOrdenMercado = fechaOrdenMercado;
    this.horaOrdenMercado = horaOrdenMercado;
    this.plataformaNegociacion = plataformaNegociacion;
    this.indicadorCotizacion = indicadorCotizacion;
    this.miembroMercado = miembroMercado;
    this.segmentoEjecucion = segmentoEjecucion;
    this.tipoOperacionBolsa = tipoOperacionBolsa;
    this.numeroOperacionMercado = numeroOperacionMercado;
    this.numeroEjecucionMercado = numeroEjecucionMercado;
    this.titulosTotales = titulosTotales;
    this.titulosPendientesCase = titulosPendientesCase;
    updated = Boolean.FALSE;
  }

  public long getIdCase() {
    return idCase;
  }

  public void setIdCase(long idCase) {
    this.idCase = idCase;
  }

  public long getIdOperacionEcc() {
    return idOperacionEcc;
  }

  public void setIdOperacionEcc(long idOperacionEcc) {
    this.idOperacionEcc = idOperacionEcc;
  }

  public String getCdoperacionecc() {
    return cdoperacionecc;
  }

  public void setCdoperacionecc(String cdoperacionecc) {
    this.cdoperacionecc = cdoperacionecc;
  }

  public String getIsin() {
    return isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public char getSentido() {
    return sentido;
  }

  public void setSentido(char sentido) {
    this.sentido = sentido;
  }

  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  public Date getFechaOrdenMercado() {
    return fechaOrdenMercado;
  }

  public void setFechaOrdenMercado(Date fechaOrdenMercado) {
    this.fechaOrdenMercado = fechaOrdenMercado;
  }

  public Date getHoraOrdenMercado() {
    return horaOrdenMercado;
  }

  public void setHoraOrdenMercado(Date horaOrdenMercado) {
    this.horaOrdenMercado = horaOrdenMercado;
  }

  public String getPlataformaNegociacion() {
    return plataformaNegociacion;
  }

  public void setPlataformaNegociacion(String plataformaNegociacion) {
    this.plataformaNegociacion = plataformaNegociacion;
  }

  public Character getIndicadorCotizacion() {
    return indicadorCotizacion;
  }

  public void setIndicadorCotizacion(Character indicadorCotizacion) {
    this.indicadorCotizacion = indicadorCotizacion;
  }

  public String getMiembroMercado() {
    return miembroMercado;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  public String getSegmentoEjecucion() {
    return segmentoEjecucion;
  }

  public void setSegmentoEjecucion(String segmentoEjecucion) {
    this.segmentoEjecucion = segmentoEjecucion;
  }

  public String getTipoOperacionBolsa() {
    return tipoOperacionBolsa;
  }

  public void setTipoOperacionBolsa(String tipoOperacionBolsa) {
    this.tipoOperacionBolsa = tipoOperacionBolsa;
  }

  public String getNumeroOperacionMercado() {
    return numeroOperacionMercado;
  }

  public void setNumeroOperacionMercado(String numeroOperacionMercado) {
    this.numeroOperacionMercado = numeroOperacionMercado;
  }

  public String getNumeroEjecucionMercado() {
    return numeroEjecucionMercado;
  }

  public void setNumeroEjecucionMercado(String numeroEjecucionMercado) {
    this.numeroEjecucionMercado = numeroEjecucionMercado;
  }

  public BigDecimal getTitulosTotales() {
    return titulosTotales;
  }

  public void setTitulosTotales(BigDecimal titulosTotales) {
    this.titulosTotales = titulosTotales;
  }

  public BigDecimal getTitulosPendientesCase() {
    return titulosPendientesCase;
  }

  public void setTitulosPendientesCase(BigDecimal titulosPendientesCase) {
    this.titulosPendientesCase = titulosPendientesCase;
  }

  public boolean isUpdated() {
    return updated;
  }

  public void setUpdated(boolean updated) {
    this.updated = updated;
  }

  @Override
  public String toString() {
    return "Tmct0caseoperacionejeInformacionMercadoCaseDTO [idCase=" + idCase + ", idOperacionEcc=" + idOperacionEcc
           + ", cdoperacionecc=" + cdoperacionecc + ", isin=" + isin + ", sentido=" + sentido + ", fechaEjecucion="
           + fechaEjecucion + ", fechaOrdenMercado=" + fechaOrdenMercado + ", horaOrdenMercado=" + horaOrdenMercado
           + ", plataformaNegociacion=" + plataformaNegociacion + ", indicadorCotizacion=" + indicadorCotizacion
           + ", miembroMercado=" + miembroMercado + ", segmentoEjecucion=" + segmentoEjecucion
           + ", tipoOperacionBolsa=" + tipoOperacionBolsa + ", numeroOperacionMercado=" + numeroOperacionMercado
           + ", numeroEjecucionMercado=" + numeroEjecucionMercado + ", titulosTotales=" + titulosTotales
           + ", titulosPendientesCase=" + titulosPendientesCase + ", updated=" + updated + "]";
  }

}
