package sibbac.business.wrappers.database.bo;


import java.util.Date;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.PeriodosDao;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.database.bo.AbstractBo;


/**
 * The Class PeriodosBo.
 */
@Service
public class PeriodosBo extends AbstractBo< Periodos, Long, PeriodosDao > {
    
	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.database.bo.AbstractBo#save(java.lang.Object)
	 */
	public Periodos save( final Periodos per ) {
		Date inicioDate = per.getFechaInicio();
		Date finDate = per.getFechaFinal();
		Long conPeriodosLong = countByFechaInicioAndFechaFinal( inicioDate, finDate );
		try{
		    if ( conPeriodosLong != 0 ) {
			return null;
			} else {
			return this.dao.save( per );
			}
		}catch(Exception ex){
		    LOG.error(ex.getMessage(), ex);
		}
		return null;

	}

	/**
	 * Count by fecha inicio periodo and fecha final periodo.
	 *
	 * @param altaFechaInicio
	 *            the alta fecha inicio
	 * @param altaFechaFinal
	 *            the alta fecha final
	 * @return the long
	 */
	public Long countByFechaInicioAndFechaFinal( Date altaFechaInicio, Date altaFechaFinal ) {
		return this.dao.countByFechaInicioAndFechaFinal( altaFechaInicio, altaFechaFinal );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.database.bo.AbstractBo#delete(java.lang.Object)
	 */
	public void delete( final Periodos periodos ) {

		this.dao.delete( this.findById( periodos.getId() ) );
	}

	/**
	 * Recupera el periodo activo
	 * 
	 * @return
	 */
	public Periodos getPeriodosActivo() {
		final Date fechaActual = new Date();

		return dao.findFirstByFechaInicioBeforeAndFechaFinalAfter( fechaActual, fechaActual );
	}

}
