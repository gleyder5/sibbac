package sibbac.business.wrappers.database.dto.caseejecuciones;

import java.util.Date;

public class DesgloseClititCaseEjecucionesInformacionMercadoDTO {

  /**
   * 
   */
  private static final long serialVersionUID = 7746117485634140116L;
  /**
   * CDISIN
   */
  private final String isin;
  /**
   * CDTPOPER
   * */
  private char sentido;
  /**
   * FEEJECUC
   * */
  private Date fechaEjecucion;
  /**
   * CDMIEMBROMKT
   * */
  private final String miembroMercado;
  /**
   * SEGMENTTRADINGVENUE
   * */
  private final String segmentoEjecucion;
  /**
   * TPOPEBOL
   * */
  private final String tipoOperacionBolsa;
  /**
   * NUOPEMER
   * */
  private final String numeroOperacionMercado;
  /**
   * NUREFERE
   * */
  private final String numeroEjecucionMercado;

  /**
   * NUEJECUC
   * */
  private final String nuejecuc;

  public DesgloseClititCaseEjecucionesInformacionMercadoDTO(String isin,
                                                char sentido,
                                                Date fechaEjecucion,
                                                String miembroMercado,
                                                String segmentoEjecucion,
                                                String tipoOperacionBolsa,
                                                String numeroOperacionMercado,
                                                String numeroEjecucionMercado,
                                                String nuejecuc) {
    super();
    this.isin = isin;
    this.sentido = sentido;
    this.fechaEjecucion = fechaEjecucion;
    this.miembroMercado = miembroMercado;
    this.segmentoEjecucion = segmentoEjecucion;
    this.tipoOperacionBolsa = tipoOperacionBolsa;
    this.numeroOperacionMercado = numeroOperacionMercado;
    this.numeroEjecucionMercado = numeroEjecucionMercado;
    this.nuejecuc = nuejecuc;
  }

  public char getSentido() {
    return sentido;
  }

  public void setSentido(char sentido) {
    this.sentido = sentido;
  }

  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public String getIsin() {
    return isin;
  }

  public String getMiembroMercado() {
    return miembroMercado;
  }

  public String getSegmentoEjecucion() {
    return segmentoEjecucion;
  }

  public String getTipoOperacionBolsa() {
    return tipoOperacionBolsa;
  }

  public String getNumeroOperacionMercado() {
    return numeroOperacionMercado;
  }

  public String getNumeroEjecucionMercado() {
    return numeroEjecucionMercado;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fechaEjecucion == null) ? 0 : fechaEjecucion.hashCode());
    result = prime * result + ((isin == null) ? 0 : isin.hashCode());
    result = prime * result + ((miembroMercado == null) ? 0 : miembroMercado.hashCode());
    result = prime * result + ((nuejecuc == null) ? 0 : nuejecuc.hashCode());
    result = prime * result + ((numeroEjecucionMercado == null) ? 0 : numeroEjecucionMercado.hashCode());
    result = prime * result + ((numeroOperacionMercado == null) ? 0 : numeroOperacionMercado.hashCode());
    result = prime * result + ((segmentoEjecucion == null) ? 0 : segmentoEjecucion.hashCode());
    result = prime * result + sentido;
    result = prime * result + ((tipoOperacionBolsa == null) ? 0 : tipoOperacionBolsa.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DesgloseClititCaseEjecucionesInformacionMercadoDTO other = (DesgloseClititCaseEjecucionesInformacionMercadoDTO) obj;
    if (fechaEjecucion == null) {
      if (other.fechaEjecucion != null)
        return false;
    } else if (!fechaEjecucion.equals(other.fechaEjecucion))
      return false;
    if (isin == null) {
      if (other.isin != null)
        return false;
    } else if (!isin.equals(other.isin))
      return false;
    if (miembroMercado == null) {
      if (other.miembroMercado != null)
        return false;
    } else if (!miembroMercado.equals(other.miembroMercado))
      return false;
    if (nuejecuc == null) {
      if (other.nuejecuc != null)
        return false;
    } else if (!nuejecuc.equals(other.nuejecuc))
      return false;
    if (numeroEjecucionMercado == null) {
      if (other.numeroEjecucionMercado != null)
        return false;
    } else if (!numeroEjecucionMercado.equals(other.numeroEjecucionMercado))
      return false;
    if (numeroOperacionMercado == null) {
      if (other.numeroOperacionMercado != null)
        return false;
    } else if (!numeroOperacionMercado.equals(other.numeroOperacionMercado))
      return false;
    if (segmentoEjecucion == null) {
      if (other.segmentoEjecucion != null)
        return false;
    } else if (!segmentoEjecucion.equals(other.segmentoEjecucion))
      return false;
    if (sentido != other.sentido)
      return false;
    if (tipoOperacionBolsa == null) {
      if (other.tipoOperacionBolsa != null)
        return false;
    } else if (!tipoOperacionBolsa.equals(other.tipoOperacionBolsa))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "DesgloseClititCaseEjecucionesInformacionMercadoDTO [isin=" + isin + ", sentido=" + sentido + ", fechaEjecucion="
           + fechaEjecucion + ", miembroMercado=" + miembroMercado + ", segmentoEjecucion=" + segmentoEjecucion
           + ", tipoOperacionBolsa=" + tipoOperacionBolsa + ", numeroOperacionMercado=" + numeroOperacionMercado
           + ", numeroEjecucionMercado=" + numeroEjecucionMercado + ", nuejecuc=" + nuejecuc + "]";
  }
  
  
  

}
