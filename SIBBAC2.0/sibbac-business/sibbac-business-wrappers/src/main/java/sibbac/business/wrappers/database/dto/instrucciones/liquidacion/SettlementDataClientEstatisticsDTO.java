package sibbac.business.wrappers.database.dto.instrucciones.liquidacion;


import java.io.Serializable;

import sibbac.business.fase0.database.model.Tmct0est;
import sibbac.common.helper.CloneObjectHelper;


public class SettlementDataClientEstatisticsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -3381684622391241502L;

	public SettlementDataClientEstatisticsDTO() {
		// TODO Auto-generated constructor stub
	}

	public SettlementDataClientEstatisticsDTO( Tmct0est est ) {
		this();
		CloneObjectHelper.copyBasicFields( est, this, null );
	}

	/**
	 * Table Field CDBROCLI.
	 * CLIENT
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdbrocli;

	/**
	 * Table Field CDALIASS.
	 * ALIAS
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdaliass;
	/**
	 * Table Field CDSUBCTA.
	 * SUBCUENTA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdsubcta;
	/**
	 * Table Field NUMSEC.
	 * SECUENCIA
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.math.BigDecimal	numsec;
	/**
	 * Table Field CDSALEID.
	 * SALES
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdsaleid;
	/**
	 * Table Field CDMANAGE.
	 * MANAGER
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdmanage;
	/**
	 * Table Field STGROUP.
	 * Statistic Group
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stgroup;
	/**
	 * Table Field STLEVEL0.
	 * Statistic Level 0
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlevel0;
	/**
	 * Table Field STLEVEL1.
	 * Statistic Level 1
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlevel1;
	/**
	 * Table Field STLEVEL2.
	 * Statistic Level 2
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlevel2;
	/**
	 * Table Field STLEVEL3.
	 * Statistic Level 3
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlevel3;
	/**
	 * Table Field STLEVEL4.
	 * Statistic Level 4
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlevel4;
	/**
	 * Table Field CDSALELM.
	 * SALES M
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdsalelm;
	/**
	 * Table Field CDMANALM.
	 * MANAGER M
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdmanalm;
	/**
	 * Table Field STGROULM.
	 * Statistic Group LM
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stgroulm;
	/**
	 * Table Field STLEV0LM.
	 * Statistic Level 0 LM
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlev0lm;

	/**
	 * Table Field STLEV1LM.
	 * Statistic Level 1 LM
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlev1lm;

	/**
	 * Table Field STLEV2LM.
	 * Statistic Level 2 LM
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlev2lm;
	/**
	 * Table Field STLEV3LM.
	 * Statistic Level 3 LM
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlev3lm;
	/**
	 * Table Field STLEV4LM.
	 * Statistic Level 4 LM
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		stlev4lm;
	/**
	 * Table Field CDOTHEID.
	 * OTHER RETAIL ID
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdotheid;
	/**
	 * Table Field CDKGR.
	 * KGR CODE
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdkgr;
	/**
	 * Table Field IDAUX.
	 * ID AUXILIAR
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		idaux;
	/**
	 * Table Field CDUSUAUD.
	 * AUDIT USER
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.lang.String		cdusuaud;
	/**
	 * Table Field FHAUDIT.
	 * AUDIT DATE
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.sql.Timestamp	fhaudit;
	/**
	 * Table Field FHINICIO.
	 * INIT DATE
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.math.BigDecimal	fhinicio;
	/**
	 * Table Field FHFINAL.
	 * FINAL DATE
	 * Documentación columna
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 **/
	protected java.math.BigDecimal	fhfinal;

	/**
	 * @return the cdbrocli
	 */
	public java.lang.String getCdbrocli() {
		return cdbrocli;
	}

	/**
	 * @param cdbrocli the cdbrocli to set
	 */
	public void setCdbrocli( java.lang.String cdbrocli ) {
		this.cdbrocli = cdbrocli;
	}

	/**
	 * @return the cdaliass
	 */
	public java.lang.String getCdaliass() {
		return cdaliass;
	}

	/**
	 * @param cdaliass the cdaliass to set
	 */
	public void setCdaliass( java.lang.String cdaliass ) {
		this.cdaliass = cdaliass;
	}

	/**
	 * @return the cdsubcta
	 */
	public java.lang.String getCdsubcta() {
		return cdsubcta;
	}

	/**
	 * @param cdsubcta the cdsubcta to set
	 */
	public void setCdsubcta( java.lang.String cdsubcta ) {
		this.cdsubcta = cdsubcta;
	}

	/**
	 * @return the numsec
	 */
	public java.math.BigDecimal getNumsec() {
		return numsec;
	}

	/**
	 * @param numsec the numsec to set
	 */
	public void setNumsec( java.math.BigDecimal numsec ) {
		this.numsec = numsec;
	}

	/**
	 * @return the cdsaleid
	 */
	public java.lang.String getCdsaleid() {
		return cdsaleid;
	}

	/**
	 * @param cdsaleid the cdsaleid to set
	 */
	public void setCdsaleid( java.lang.String cdsaleid ) {
		this.cdsaleid = cdsaleid;
	}

	/**
	 * @return the cdmanage
	 */
	public java.lang.String getCdmanage() {
		return cdmanage;
	}

	/**
	 * @param cdmanage the cdmanage to set
	 */
	public void setCdmanage( java.lang.String cdmanage ) {
		this.cdmanage = cdmanage;
	}

	/**
	 * @return the stgroup
	 */
	public java.lang.String getStgroup() {
		return stgroup;
	}

	/**
	 * @param stgroup the stgroup to set
	 */
	public void setStgroup( java.lang.String stgroup ) {
		this.stgroup = stgroup;
	}

	/**
	 * @return the stlevel0
	 */
	public java.lang.String getStlevel0() {
		return stlevel0;
	}

	/**
	 * @param stlevel0 the stlevel0 to set
	 */
	public void setStlevel0( java.lang.String stlevel0 ) {
		this.stlevel0 = stlevel0;
	}

	/**
	 * @return the stlevel1
	 */
	public java.lang.String getStlevel1() {
		return stlevel1;
	}

	/**
	 * @param stlevel1 the stlevel1 to set
	 */
	public void setStlevel1( java.lang.String stlevel1 ) {
		this.stlevel1 = stlevel1;
	}

	/**
	 * @return the stlevel2
	 */
	public java.lang.String getStlevel2() {
		return stlevel2;
	}

	/**
	 * @param stlevel2 the stlevel2 to set
	 */
	public void setStlevel2( java.lang.String stlevel2 ) {
		this.stlevel2 = stlevel2;
	}

	/**
	 * @return the stlevel3
	 */
	public java.lang.String getStlevel3() {
		return stlevel3;
	}

	/**
	 * @param stlevel3 the stlevel3 to set
	 */
	public void setStlevel3( java.lang.String stlevel3 ) {
		this.stlevel3 = stlevel3;
	}

	/**
	 * @return the stlevel4
	 */
	public java.lang.String getStlevel4() {
		return stlevel4;
	}

	/**
	 * @param stlevel4 the stlevel4 to set
	 */
	public void setStlevel4( java.lang.String stlevel4 ) {
		this.stlevel4 = stlevel4;
	}

	/**
	 * @return the cdsalelm
	 */
	public java.lang.String getCdsalelm() {
		return cdsalelm;
	}

	/**
	 * @param cdsalelm the cdsalelm to set
	 */
	public void setCdsalelm( java.lang.String cdsalelm ) {
		this.cdsalelm = cdsalelm;
	}

	/**
	 * @return the cdmanalm
	 */
	public java.lang.String getCdmanalm() {
		return cdmanalm;
	}

	/**
	 * @param cdmanalm the cdmanalm to set
	 */
	public void setCdmanalm( java.lang.String cdmanalm ) {
		this.cdmanalm = cdmanalm;
	}

	/**
	 * @return the stgroulm
	 */
	public java.lang.String getStgroulm() {
		return stgroulm;
	}

	/**
	 * @param stgroulm the stgroulm to set
	 */
	public void setStgroulm( java.lang.String stgroulm ) {
		this.stgroulm = stgroulm;
	}

	/**
	 * @return the stlev0lm
	 */
	public java.lang.String getStlev0lm() {
		return stlev0lm;
	}

	/**
	 * @param stlev0lm the stlev0lm to set
	 */
	public void setStlev0lm( java.lang.String stlev0lm ) {
		this.stlev0lm = stlev0lm;
	}

	/**
	 * @return the stlev1lm
	 */
	public java.lang.String getStlev1lm() {
		return stlev1lm;
	}

	/**
	 * @param stlev1lm the stlev1lm to set
	 */
	public void setStlev1lm( java.lang.String stlev1lm ) {
		this.stlev1lm = stlev1lm;
	}

	/**
	 * @return the stlev2lm
	 */
	public java.lang.String getStlev2lm() {
		return stlev2lm;
	}

	/**
	 * @param stlev2lm the stlev2lm to set
	 */
	public void setStlev2lm( java.lang.String stlev2lm ) {
		this.stlev2lm = stlev2lm;
	}

	/**
	 * @return the stlev3lm
	 */
	public java.lang.String getStlev3lm() {
		return stlev3lm;
	}

	/**
	 * @param stlev3lm the stlev3lm to set
	 */
	public void setStlev3lm( java.lang.String stlev3lm ) {
		this.stlev3lm = stlev3lm;
	}

	/**
	 * @return the stlev4lm
	 */
	public java.lang.String getStlev4lm() {
		return stlev4lm;
	}

	/**
	 * @param stlev4lm the stlev4lm to set
	 */
	public void setStlev4lm( java.lang.String stlev4lm ) {
		this.stlev4lm = stlev4lm;
	}

	/**
	 * @return the cdotheid
	 */
	public java.lang.String getCdotheid() {
		return cdotheid;
	}

	/**
	 * @param cdotheid the cdotheid to set
	 */
	public void setCdotheid( java.lang.String cdotheid ) {
		this.cdotheid = cdotheid;
	}

	/**
	 * @return the cdkgr
	 */
	public java.lang.String getCdkgr() {
		return cdkgr;
	}

	/**
	 * @param cdkgr the cdkgr to set
	 */
	public void setCdkgr( java.lang.String cdkgr ) {
		this.cdkgr = cdkgr;
	}

	/**
	 * @return the idaux
	 */
	public java.lang.String getIdaux() {
		return idaux;
	}

	/**
	 * @param idaux the idaux to set
	 */
	public void setIdaux( java.lang.String idaux ) {
		this.idaux = idaux;
	}

	/**
	 * @return the cdusuaud
	 */
	public java.lang.String getCdusuaud() {
		return cdusuaud;
	}

	/**
	 * @param cdusuaud the cdusuaud to set
	 */
	public void setCdusuaud( java.lang.String cdusuaud ) {
		this.cdusuaud = cdusuaud;
	}

	/**
	 * @return the fhaudit
	 */
	public java.sql.Timestamp getFhaudit() {
		return fhaudit;
	}

	/**
	 * @param fhaudit the fhaudit to set
	 */
	public void setFhaudit( java.sql.Timestamp fhaudit ) {
		this.fhaudit = fhaudit;
	}

	/**
	 * @return the fhinicio
	 */
	public java.math.BigDecimal getFhinicio() {
		return fhinicio;
	}

	/**
	 * @param fhinicio the fhinicio to set
	 */
	public void setFhinicio( java.math.BigDecimal fhinicio ) {
		this.fhinicio = fhinicio;
	}

	/**
	 * @return the fhfinal
	 */
	public java.math.BigDecimal getFhfinal() {
		return fhfinal;
	}

	/**
	 * @param fhfinal the fhfinal to set
	 */
	public void setFhfinal( java.math.BigDecimal fhfinal ) {
		this.fhfinal = fhfinal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SettlementDataClientEstadisticsDTO [cdbrocli=" + cdbrocli + ", cdaliass=" + cdaliass + ", cdsubcta=" + cdsubcta
				+ ", numsec=" + numsec + ", cdsaleid=" + cdsaleid + ", cdmanage=" + cdmanage + ", stgroup=" + stgroup + ", stlevel0="
				+ stlevel0 + ", stlevel1=" + stlevel1 + ", stlevel2=" + stlevel2 + ", stlevel3=" + stlevel3 + ", stlevel4=" + stlevel4
				+ ", cdsalelm=" + cdsalelm + ", cdmanalm=" + cdmanalm + ", stgroulm=" + stgroulm + ", stlev0lm=" + stlev0lm + ", stlev1lm="
				+ stlev1lm + ", stlev2lm=" + stlev2lm + ", stlev3lm=" + stlev3lm + ", stlev4lm=" + stlev4lm + ", cdotheid=" + cdotheid
				+ ", cdkgr=" + cdkgr + ", idaux=" + idaux + ", cdusuaud=" + cdusuaud + ", fhaudit=" + fhaudit + ", fhinicio=" + fhinicio
				+ ", fhfinal=" + fhfinal + "]";
	}

}
