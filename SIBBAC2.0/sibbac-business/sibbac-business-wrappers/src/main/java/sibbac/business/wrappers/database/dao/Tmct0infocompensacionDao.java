package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;

@Repository
public interface Tmct0infocompensacionDao extends JpaRepository<Tmct0infocompensacion, Long> {

  @Query("SELECT c FROM Tmct0infocompensacion c, Tmct0desglosecamara d WHERE d.tmct0infocompensacion.idinfocomp = c.idinfocomp  AND d.iddesglosecamara = :pdidesglosecamara")
  Tmct0infocompensacion findByIddesgloseCamara(@Param("pdidesglosecamara") long iddesgloseCamara);

  @Query("SELECT c.cdctacompensacion, c.cdmiembrocompensador, c.cdrefasignacion "
      + " FROM Tmct0infocompensacion c, Tmct0desglosecamara d WHERE d.tmct0infocompensacion.idinfocomp = c.idinfocomp  "
      + " AND d.iddesglosecamara = :pdidesglosecamara and d.tmct0alc.id = :alcId  and d.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<String[]> findAllDistinctInformacionCompensacionByAlcId(@Param("alcId") Tmct0alcId alcid);

  @Query("SELECT c FROM Tmct0infocompensacion c, Tmct0desglosecamara d WHERE d.tmct0infocompensacion.idinfocomp = c.idinfocomp  "
      + " AND d.iddesglosecamara = :pdidesglosecamara and d.tmct0alc.id = :alcId  and d.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0infocompensacion> findAllTmct0infocompensacionByAlcId(@Param("alcId") Tmct0alcId alcid);

}
