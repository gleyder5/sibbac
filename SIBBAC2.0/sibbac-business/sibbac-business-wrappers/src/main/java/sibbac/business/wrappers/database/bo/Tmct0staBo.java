package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0staDao;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0staBo extends AbstractBo< Tmct0sta, Tmct0staId, Tmct0staDao > {

	public Tmct0sta findById( Tmct0staId id ) {
		return dao.findById( id );
	}

}
