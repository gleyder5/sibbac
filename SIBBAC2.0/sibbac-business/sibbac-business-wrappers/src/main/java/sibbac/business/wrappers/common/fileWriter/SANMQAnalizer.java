package sibbac.business.wrappers.common.fileWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.util.FicherosUtils;
import sibbac.common.SIBBACBusinessException;

@Component
public class SANMQAnalizer {

  protected static final Logger LOG = LoggerFactory.getLogger(SANMQAnalizer.class);

  private static final String FILE_NAME_OTROS = "OTROS.TMP";
  private static final String FILE_NAME_TIPO_20 = "NOMBRES.TMP";
  private static final int REFRE_START_POSITION = 21;
  private static final int REFRE_SIZE = 16;
  private static final int NUMORDEN_START_POSITION = 37;
  private static final int NUMORDEN_SIZE = 10;
  private static final int NUREFORD_START_POSITION = 6;
  private static final int NUREFORD_SIZE = 10;
  private static final String FILE_DESGLOSE_PREFIJO = "DESGLOSE_";
  private static final String FILE_DESGLOSE_EXTENSION = ".DAT";
  private static final String FILE_GZ_EXTENSION = ".GZ";
  private static final String FILE_TMP_EXTENSION = ".TMP";
  private static final String FILE_APPEND_EXTENSION = ".APPEND";

  // private static final String FILE_ANALIZANDO_EXTENSION = ".analizando";

  public void analizeFile(String fileName, String filePath) throws SIBBACBusinessException {
    LOG.debug("[SANMQAnalizer :: analizeFile] Init (FileName=" + fileName + ", FilePath=" + filePath + ")");
    File file = null;
    file = getFileForAnalisis(fileName, filePath);

    if (file != null && file.exists() && file.length() > 0) {

      try {
        readLines(file, filePath);
        FicherosUtils.fileToTratados(file);
      } catch (Exception e) {
        throw new SIBBACBusinessException(e);
      }

    }

  }

  private void readLines(File file, String filePathSt) throws SIBBACBusinessException {

    SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
    LOG.debug("Comienzo proceso SANMQANALIZER: " + sdf.format(new Date()));

    String fileName = null;
    Collection<Path> ficheros = new HashSet<Path>();
    BufferedWriter bw = null;
    Path filePath;
    Path filePathLast = null;
    String fileLine = null;
    String nurefordPrev = null;
    BufferedReader br = null;
    try {
      Path path = Paths.get(new URL(filePathSt).toURI());
      if (java.nio.file.Files.notExists(path)) {
        java.nio.file.Files.createDirectories(path);
      }
      br = new BufferedReader(new InputStreamReader(java.nio.file.Files.newInputStream(file.toPath(),
                                                                                       StandardOpenOption.READ),
                                                    StandardCharsets.ISO_8859_1));
      int countLines = 0;
      try {
        while ((fileLine = br.readLine()) != null) {
          countLines++;
          LOG.trace("[SANMQAnalizer :: analizeFile] linea fichero: {}", fileLine);
          if (fileLine.startsWith("20")) {
            if (nurefordPrev != null) {
              if (!nurefordPrev.trim().equals(getNurefordFrom20(fileLine).trim())) {
                fileName = FILE_NAME_TIPO_20;
              }
            } else if (fileName == null) {
              fileName = FILE_NAME_TIPO_20;
            }
          } else if (fileLine.startsWith("40")) {
            fileName = getTmpFileNameFrom40(fileLine);
            nurefordPrev = getNumordenFrom40(fileLine);
          } else {
            fileName = FILE_NAME_OTROS;
          }

          if (fileName.equals(FILE_NAME_OTROS)) {
            LOG.warn("[SANMQAnalizer :: analizeFile] Linea no reconocida va al fichero otros num. linea: {} linea: {}. ",
                     countLines, fileLine);
          } else if (fileName.equals(FILE_NAME_TIPO_20)) {
            if (countLines % 5000 == 0) {
              LOG.debug("[SANMQAnalizer :: analizeFile] Linea 20 reconocida para fichero de titulares num. linea: {} linea: {}. ",
                        countLines, fileLine);
            }
          } else {
            if (countLines % 5000 == 0) {
              LOG.debug("[SANMQAnalizer :: analizeFile] Linea reconocida para fichero de desgloses. num. linea: {} linea: {}. ",
                        countLines, fileLine);
            }
          }

          filePath = Paths.get(path.toString(), fileName);

          if (bw == null || filePathLast == null || !filePath.equals(filePathLast)) {
            if (bw != null) {
              bw.close();
            }
            bw = new BufferedWriter(new OutputStreamWriter(Files.newOutputStream(filePath, StandardOpenOption.WRITE,
                                                                                 StandardOpenOption.APPEND,
                                                                                 StandardOpenOption.CREATE),
                                                           StandardCharsets.ISO_8859_1));

          }

          bw.write(fileLine);
          bw.newLine();
          ficheros.add(filePath);
          filePathLast = filePath;
        }
      } catch (Exception e) {
        throw new SIBBACBusinessException("INCIDENCIA  recorriendo el fichero en linea : " + countLines + " - "
                                          + fileLine, e);
      } finally {
        if (bw != null) {
          bw.close();
        }
      }
      for (Path pathFor : ficheros) {
        boolean gzipInput = isComprimirInput(pathFor);
        if (java.nio.file.Files.size(pathFor) == 0) {
          java.nio.file.Files.delete(pathFor);
        } else {
          String fileBaseName = FilenameUtils.getBaseName(pathFor.toString());
          Path ficheroDestino = Paths.get(path.toString(), fileBaseName + FILE_DESGLOSE_EXTENSION);
          Path ficheroDestinoGz = Paths.get(path.toString(), fileBaseName + FILE_DESGLOSE_EXTENSION + FILE_GZ_EXTENSION);
          if (java.nio.file.Files.exists(ficheroDestinoGz)) {
            ficheroDestino = ficheroDestinoGz;
          }
          if (Files.exists(ficheroDestino)) {
            appendFileAngGzipIfNecessary(pathFor, ficheroDestino);
          } else {
            if (gzipInput) {
              gzip(pathFor);
            } else {
              Files.move(pathFor, ficheroDestino, StandardCopyOption.ATOMIC_MOVE);
            }
          }
          Files.deleteIfExists(pathFor);
        }
      }

    } catch (Exception e) {
      throw new SIBBACBusinessException(e);
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          throw new SIBBACBusinessException(e);
        }
      }
    }

    LOG.debug("Fin proceso SANMQANALIZER: " + sdf.format(new Date()));

  }

  private boolean isComprimirInput(Path file) throws IOException {
    return Files.size(file) > 25 * 1024 * 1024;
  }

  private void appendFileAngGzipIfNecessary(Path pathFor, Path ficheroDestino) throws IOException {

    Path path = pathFor.getParent();

    String fileBaseName = FilenameUtils.getBaseName(ficheroDestino.toString());
    fileBaseName = FilenameUtils.getBaseName(fileBaseName);

    Path ficheroDestinoTmp = Paths.get(path.toString(), fileBaseName + FILE_TMP_EXTENSION + FILE_APPEND_EXTENSION);

    InputStreamReader isr = new InputStreamReader(java.nio.file.Files.newInputStream(pathFor, StandardOpenOption.READ),
                                                  StandardCharsets.ISO_8859_1);
    OutputStreamWriter osw = null;
    boolean outPutIsGzipped = ficheroDestino.toString().toUpperCase().endsWith(FILE_GZ_EXTENSION);
    if (outPutIsGzipped) {
      ficheroDestinoTmp = Paths.get(path.toString(), fileBaseName + FILE_TMP_EXTENSION + FILE_GZ_EXTENSION
                                                     + FILE_APPEND_EXTENSION);
    }

    java.nio.file.Files.move(ficheroDestino, ficheroDestinoTmp, StandardCopyOption.ATOMIC_MOVE);
    if (outPutIsGzipped) {

      osw = new OutputStreamWriter(
                                   new GZIPOutputStream(java.nio.file.Files.newOutputStream(ficheroDestinoTmp,
                                                                                            StandardOpenOption.WRITE,
                                                                                            StandardOpenOption.APPEND)),
                                   StandardCharsets.ISO_8859_1);

    } else {
      osw = new OutputStreamWriter(java.nio.file.Files.newOutputStream(ficheroDestinoTmp, StandardOpenOption.WRITE,
                                                                       StandardOpenOption.APPEND),
                                   StandardCharsets.ISO_8859_1);
    }

    IOUtils.copy(isr, osw);
    isr.close();
    osw.close();
    boolean gzipInput = isComprimirInput(ficheroDestinoTmp);
    if (!outPutIsGzipped && gzipInput) {
      gzip(ficheroDestinoTmp);
    } else {
      java.nio.file.Files.move(ficheroDestinoTmp, ficheroDestino, StandardCopyOption.ATOMIC_MOVE);
    }
  }

  private void gzip(Path file) throws IOException {
    Path path = file.getParent();
    String fileBaseName = FilenameUtils.getBaseName(file.toString());
    Path ficheroDestinoTmpGz = Paths.get(path.toString(), fileBaseName + FILE_TMP_EXTENSION + FILE_GZ_EXTENSION);
    Path ficheroDestinoGz = Paths.get(path.toString(), fileBaseName + FILE_DESGLOSE_EXTENSION + FILE_GZ_EXTENSION);
    InputStreamReader isr = new InputStreamReader(java.nio.file.Files.newInputStream(file, StandardOpenOption.READ),
                                                  StandardCharsets.ISO_8859_1);

    OutputStreamWriter osw = new OutputStreamWriter(
                                                    new GZIPOutputStream(
                                                                         java.nio.file.Files.newOutputStream(ficheroDestinoTmpGz,
                                                                                                             StandardOpenOption.WRITE,
                                                                                                             StandardOpenOption.CREATE)),
                                                    StandardCharsets.ISO_8859_1);
    IOUtils.copy(isr, osw);
    isr.close();
    osw.close();

    java.nio.file.Files.move(ficheroDestinoTmpGz, ficheroDestinoGz, StandardCopyOption.ATOMIC_MOVE);
  }

  // private String getFinalFileNameFrom40(String fileLine) {
  // return FILE_DESGLOSE_PREFIJO + getRefreFrom40(fileLine).trim() + FILE_DESGLOSE_EXTENSION;
  // }

  private String getTmpFileNameFrom40(String fileLine) {
    return FILE_DESGLOSE_PREFIJO + getRefreFrom40(fileLine) + FILE_TMP_EXTENSION;
  }

  private String getNumordenFrom40(String fileLine) {
    if (fileLine.length() > NUMORDEN_START_POSITION + NUMORDEN_SIZE) {
      return fileLine.substring(NUMORDEN_START_POSITION, NUMORDEN_START_POSITION + NUMORDEN_SIZE);
    }
    return "";
  }

  private String getNurefordFrom20(String fileLine) {
    if (fileLine.length() > NUREFORD_START_POSITION + NUREFORD_SIZE) {
      return fileLine.substring(NUREFORD_START_POSITION, NUREFORD_START_POSITION + NUREFORD_SIZE);
    }
    return "";
  }

  private String getRefreFrom40(String fileLine) {
    if (fileLine.length() > REFRE_START_POSITION + REFRE_SIZE) {
      return fileLine.substring(REFRE_START_POSITION, REFRE_START_POSITION + REFRE_SIZE);
    }
    return "";
  }

  private File getFileForAnalisis(String fileName, String filePath) {
    File file = null;
    try {
      file = FicherosUtils.fileToTemp(fileName, filePath, ".analizando");
    } catch (IOException e1) {
      LOG.trace("[SANMQAnalizer :: getFileForAnalisis] No se encuentra el fichero: " + filePath + fileName + ": "
                + e1.getMessage());
    }
    return file;
  }

}
