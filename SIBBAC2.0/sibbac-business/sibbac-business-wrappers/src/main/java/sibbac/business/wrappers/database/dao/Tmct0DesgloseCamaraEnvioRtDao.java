package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alc;

@Repository
public interface Tmct0DesgloseCamaraEnvioRtDao extends JpaRepository<Tmct0DesgloseCamaraEnvioRt, Long> {

  List<Tmct0DesgloseCamaraEnvioRt> findAllByNudesglose(long nudesglose);

  List<Tmct0DesgloseCamaraEnvioRt> findAllByFecontratacionAndCriterioComunicacionTitularAndCdmiembromktAndEstado(
      Date fecontratacion, String criterioComunicacionTitular, String cdmiembromkt, Character estado, Pageable page);

  @Query("SELECT t FROM Tmct0DesgloseCamaraEnvioRt t "
      + " WHERE t.desgloseCamara.iddesglosecamara = :pIdDesgloseCamara AND t.estado = :pCdestado ")
  List<Tmct0DesgloseCamaraEnvioRt> findAllByIdDesgloseCamaraAndCdestado(
      @Param("pIdDesgloseCamara") long iddesglosecamara, @Param("pCdestado") Character estado);

  @Query("SELECT distinct t.desgloseCamara.tmct0alc FROM Tmct0DesgloseCamaraEnvioRt t "
      + " WHERE t.envioTitularidad.idenviotitularidad = :pidEnvioTitularidad AND t.estado <> :notCdestado "
      + " and t.desgloseCamara.tmct0estadoByCdestadoasig.idestado >= :cdestadoasig ")
  List<Tmct0alc> findAllAlcsByIdEnvioTitularidadAndNotCdestadoAndCdestadoasigGte(
      @Param("pidEnvioTitularidad") long idEnvioTitularidad, @Param("notCdestado") char notCdestado,
      @Param("cdestadoasig") int cdestadoasig);

  @Query("SELECT t FROM Tmct0DesgloseCamaraEnvioRt t "
      + " WHERE t.envioTitularidad.idenviotitularidad = :pidEnvioTitularidad AND t.estado <> :notCdestado AND t.estado <> :notCdestado1 ")
  List<Tmct0DesgloseCamaraEnvioRt> findAllByIdEnvioTitularidadAndNotCdestado(
      @Param("pidEnvioTitularidad") long idEnvioTitularidad, @Param("notCdestado") Character notCdestado,
      @Param("notCdestado1") Character notCdestado1);

  @Query(value = "SELECT RT.FECONTRATACION FROM ( "
      + " SELECT D.IDDESGLOSECAMARA FROM TMCT0DESGLOSECAMARA D WHERE D.FELIQUIDACION BETWEEN :pfeliquidacion1 AND :pfeliquidacion2 "
      + " AND D.CDESTADOASIG >= :pestadoAsignGte AND D.CDESTADOTIT = :pcdestadotit AND D.IDCAMARA = :pcdcamara "
      + " ) D INNER JOIN TMCT0_DESGLOSECAMARA_ENVIO_RT RT ON D.IDDESGLOSECAMARA = RT.IDDESGLOSECAMARA AND RT.ESTADO = :pEstadoEnvioRt "
      + " GROUP BY  RT.FECONTRATACION ORDER BY RT.FECONTRATACION ASC ", nativeQuery = true)
  List<Date> findAllDistintsFecontratacionCamaraAndFinicioAndEstado(@Param("pfeliquidacion1") Date feliquidacion1,
      @Param("pfeliquidacion2") Date feliquidacion2, @Param("pcdcamara") Long cdcamara,
      @Param("pEstadoEnvioRt") Character estadoEnvioRt, @Param("pestadoAsignGte") int estadoAsignacionGte,@Param("pcdestadotit") int estadoTitularidad);

  @Query(value = "SELECT RT.FECONTRATACION, RT.CRITERIO_COMUNICACION_TITULAR, RT.CDMIEMBROMKT FROM (SELECT D.IDDESGLOSECAMARA, D.NUORDEN, D.NBOOKING, D.NUCNFCLT, D.NUCNFLIQ "
      + " FROM TMCT0DESGLOSECAMARA D WHERE D.FECONTRATACION = :pfecontratacion AND D.CDESTADOASIG >= :pestadoAsignGte "
      + " AND D.CDESTADOTIT = :pestadoTitularidad AND D.IDCAMARA = :pcdcamara) D "
      + " INNER JOIN TMCT0_DESGLOSECAMARA_ENVIO_RT RT ON D.IDDESGLOSECAMARA = RT.IDDESGLOSECAMARA AND RT.ESTADO = :pEstadoEnvioRt "
      + " INNER JOIN (SELECT TI.IDENVIOTITULARIDAD FROM TMCT0ENVIOTITULARIDAD TI WHERE TI.FEINICIO = :pfecontratacion AND TI.ESTADO = :pestadoEnvioTitularidad "
      + " AND TI.IDCAMARA = :pcdcamara) TI ON RT.IDENVIOTITULARIDAD = TI.IDENVIOTITULARIDAD "
      + " INNER JOIN TMCT0AFI A ON D.NUORDEN=A.NUORDEN AND D.NBOOKING=A.NBOOKING AND D.NUCNFCLT=A.NUCNFCLT AND D.NUCNFLIQ=A.NUCNFLIQ AND A.CDINACTI = 'A' "
      + " GROUP BY RT.FECONTRATACION, RT.CRITERIO_COMUNICACION_TITULAR, RT.CDMIEMBROMKT "
      + " ORDER BY RT.FECONTRATACION ASC, RT.CRITERIO_COMUNICACION_TITULAR ASC, RT.CDMIEMBROMKT ASC ", nativeQuery = true)
  List<Object[]> findAllIdsByCamaraAndFinicioAndEstado(@Param("pfecontratacion") Date fecontratacion,
      @Param("pcdcamara") Long cdcamara, @Param("pestadoEnvioTitularidad") Character estadoEnvioTitularidad,
      @Param("pEstadoEnvioRt") Character estadoEnvioRt, @Param("pestadoAsignGte") int estadoAsignacionGte,
      @Param("pestadoTitularidad") int estadoTitularidad);

  @Query("select r from Tmct0DesgloseCamaraEnvioRt r where r.envioTitularidad.idenviotitularidad = :idEnvioTitularidad "
      + " and r.criterioComunicacionTitular = :criterioCommTit and r.estado = :estado"
      + " and r.desgloseCamara.tmct0estadoByCdestadoasig.idestado >= :cdestadoasig  "
      + " and r.desgloseCamara.sentido = :sentido ")
  List<Tmct0DesgloseCamaraEnvioRt> findAllByIdEnviotitularidadAndCriterioComunicacionTitularAndEstadoAndCdestadoasigGteAndSentido(
      @Param("idEnvioTitularidad") long idEnvioTitularidad, @Param("criterioCommTit") String criterioCommTitular,
      @Param("estado") char estado, @Param("cdestadoasig") int cdestadoasig, @Param("sentido") char sentido);

}
