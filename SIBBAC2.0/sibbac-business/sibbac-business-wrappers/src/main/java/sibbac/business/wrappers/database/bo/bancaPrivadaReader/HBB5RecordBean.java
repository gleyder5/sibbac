package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.math.BigDecimal;
import java.util.Date;

public class HBB5RecordBean extends HBBNRecordBean {
	/**
   * 
   */
  private static final long serialVersionUID = 4163957155719871411L;
  private String refExterna;
	private Date fechaCuadre;
	private String claveCalle;
	private String direccion;
	private String municipio;
	private String provincia;
	private BigDecimal codigoPostal;
	private String nacionalExtranjero;
	private BigDecimal pais;
	private BigDecimal tipoEnvio;
	private String filler;


	public String getRefExterna() {
		return refExterna;
	}
	public void setRefExterna(String refExterna) {
		this.refExterna = refExterna;
	}
	public Date getFechaCuadre() {
		return fechaCuadre;
	}
	public void setFechaCuadre(Date fechaCuadre) {
		this.fechaCuadre = fechaCuadre;
	}
	public String getClaveCalle() {
		return claveCalle;
	}
	public void setClaveCalle(String claveCalle) {
		this.claveCalle = claveCalle;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public BigDecimal getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(BigDecimal codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getNacionalExtranjero() {
		return nacionalExtranjero;
	}
	public void setNacionalExtranjero(String nacionalExtranjero) {
		this.nacionalExtranjero = nacionalExtranjero;
	}
	public BigDecimal getPais() {
		return pais;
	}
	public void setPais(BigDecimal pais) {
		this.pais = pais;
	}
	public BigDecimal getTipoEnvio() {
		return tipoEnvio;
	}
	public void setTipoEnvio(BigDecimal tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}

}
