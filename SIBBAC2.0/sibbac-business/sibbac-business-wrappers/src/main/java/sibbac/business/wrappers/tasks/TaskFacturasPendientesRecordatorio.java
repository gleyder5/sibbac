package sibbac.business.wrappers.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.RecordatorioFacturaPendienteBo;
import sibbac.business.wrappers.database.model.RecordatorioFacturaPendiente;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_RECORDATORIO_FACTURAS_PENDIENTES, interval = 1, intervalUnit = IntervalUnit.DAY)
public class TaskFacturasPendientesRecordatorio extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private RecordatorioFacturaPendienteBo recordatorioFacturaPendienteBo;

  protected static final Logger LOG = LoggerFactory.getLogger(TaskFacturasPendientesRecordatorio.class);

  @Override
  public void executeTask() {
    final String prefix = TaskFacturasPendientesRecordatorio.class.getName();
    executeTask(prefix);
  }

  private void executeTask(final String prefix) {
    final List<String> taskMessages = new ArrayList<String>();
    try {
      LOG.debug(prefix + "[Started...]");
      final List<RecordatorioFacturaPendiente> recordatoriosFacturaPendiente = recordatorioFacturaPendienteBo
          .generarRecordatoriosDeFacturas(taskMessages);
      if (CollectionUtils.isNotEmpty(recordatoriosFacturaPendiente)) {
        LOG.trace(prefix + "Generados " + recordatoriosFacturaPendiente.size()
            + " recordatorios de facturas pendientes.");
        taskMessages
            .add("Generados " + recordatoriosFacturaPendiente.size() + " recordatorios de facturas pendientes.");
      }
      else {
        LOG.debug(prefix + "No se han generado recordatorios de facturas pendientes.");
        taskMessages.add("No se han generado recordatorios de facturas pendientes.");
      }
    }
    catch (Exception e) {
      LOG.debug(prefix + "ERROR: " + e.getMessage());
      taskMessages.add("ERROR: " + e.getMessage());
      throw e;
    }
    finally {
      if (CollectionUtils.isNotEmpty(taskMessages)) {
        for (final String taskMessage : taskMessages) {
          blog(prefix + "[" + taskMessage + "]");
        }
      }
      LOG.trace(prefix + "[Finished...]");
    }
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_FACTURAS_FACTURAS_PENDIENTE_RECORDATORIO;
  }
}
