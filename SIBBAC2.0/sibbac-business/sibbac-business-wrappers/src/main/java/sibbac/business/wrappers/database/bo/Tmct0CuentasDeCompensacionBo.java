package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.wrappers.database.dao.Tmct0CuentasDeCompensacionDao;
import sibbac.business.wrappers.database.dao.Tmct0CuentasDeCompensacionDaoImpl;
import sibbac.business.wrappers.database.data.CuentaDeCompensacionData;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.database.bo.AbstractBo;

@Repository
@Transactional(readOnly = true)
public class Tmct0CuentasDeCompensacionBo extends
    AbstractBo<Tmct0CuentasDeCompensacion, Long, Tmct0CuentasDeCompensacionDao> {
  @PersistenceContext
  private EntityManager em;

  @Autowired
  private Tmct0CuentasDeCompensacionDaoImpl daoImpl;

  public List<Tmct0CuentasDeCompensacion> findAll() {
    return this.dao.findAll(new Sort(Sort.Direction.ASC, "cdCodigo"));
  }

  public Tmct0CuentasDeCompensacion findByCdCodigo(final String cdCodigo) {
    return this.dao.findByCdCodigo(cdCodigo);
  }

  public Tmct0CuentasDeCompensacion findByClientNumberAndAccountNumberAndSubaccountNumber(String clientNumber,
      String accountNumber, String subaccountNumber) {
    return dao.findByClientNumberAndAccountNumberAndSubaccountNumber(clientNumber, accountNumber, subaccountNumber);
  }

  public Tmct0CuentasDeCompensacion findByAlc(Tmct0alc tmct0alc) {
    return dao.findFirst1ByAlc(tmct0alc);
  }

  public Tmct0CuentasDeCompensacion findByTmct0desglosecamara(Tmct0desglosecamara desglosecamara) {
    return dao.findByTmct0desglosecamara(desglosecamara);
  }

  public long[] findByCuentaClearingIsTrue() {
    return dao.findByCuentaClearingIsTrue();
  }

  public List<Tmct0CuentasDeCompensacion> findByTmct0alc(Tmct0alc tmct0alc) {
    return dao.findByTmct0alc(tmct0alc);
  }

  public List<CuentaDeCompensacionData> findAllData() {
    List<CuentaDeCompensacionData> resultList = new LinkedList<CuentaDeCompensacionData>();
    resultList.addAll(dao.findAllData());
    return resultList;
  }

  public List<Tmct0CuentasDeCompensacion> findByidCuentaCompensacionIn(List<Long> ids) {
    List<Tmct0CuentasDeCompensacion> resultList = new LinkedList<Tmct0CuentasDeCompensacion>();
    resultList.addAll(dao.findByidCuentaCompensacionIn(ids));
    return resultList;
  }

  public List<CuentaDeCompensacionData> findAllData(Map<String, Serializable> filters) {
    List<CuentaDeCompensacionData> resultList = new LinkedList<CuentaDeCompensacionData>();
    resultList.addAll(daoImpl.findAllData(filters));

    return resultList;
  }

  @Transactional
  public Tmct0CuentasDeCompensacion save(Tmct0CuentasDeCompensacion cc) {
    if (cc.getIdCuentaCompensacion() != null) {
      em.merge(cc);
    }
    else {
      em.persist(cc);
    }
    em.flush();
    return cc;
  }

  public Long findMaxId() {
    return dao.findMaxId();
  }

  public Tmct0CuentasDeCompensacion findByIdCuentaCompensacion(long idCtaComp) {
    return dao.findByidCuentaCompensacion(idCtaComp);
  }

  public String findCuentaCompensacionS3() {
    return dao.findCuentaCompensacionS3();
  }

  public List<String> findCuentasTerceros() {
    return dao.findCuentasTerceros();
  }

  public String findCodigoS3ByCodigo(String cdCodigo) {
    return dao.findCodigoS3ByCodigo(cdCodigo);
  }

  public List<String> findByTipoCuenta(String cdCodigo) {
    return dao.findByTipoCuenta(cdCodigo);
  }

  public List<LabelValueObject> labelValueList() {
    final List<LabelValueObject> list;
    final StringBuilder label;

    list = new ArrayList<>();
    list.add(0, new LabelValueObject("", ""));
    label = new StringBuilder();
    for (Object[] arr : dao.labelValueList()) {
      int i = 0;
      if (arr != null) {
        for (Object a : arr) {
          if (i++ == 0) {
            label.append(a);
          }
          else if (i == 1) {
            label.append("-(").append(a).append(")");
          }
          else if (i == 2) {
            label.append("-").append(a);
          }
        }
      }
      if (arr[3] != null) {
        label.append('-').append(arr[3].toString());
      }
      list.add(new LabelValueObject(label.toString(), arr[0].toString()));
      label.setLength(0);
    }
    return list;
  }

  public boolean hasEuroCppConfig(String cta) {
    Tmct0CuentasDeCompensacion cuenta = findByCdCodigo(cta);
    if (cuenta != null) {
      return cuenta.getClientNumber() != null && !cuenta.getClientNumber().trim().isEmpty()
          && cuenta.getAccountNumber() != null && !cuenta.getAccountNumber().trim().isEmpty()
          && cuenta.getSubaccountNumber() != null && !cuenta.getSubaccountNumber().trim().isEmpty();
    }
    return false;
  }

  public boolean hasEuroCppConfig(Map<String, Boolean> cached, String cta) {
    if (cta != null) {
      cta = cta.trim();
    }
    Boolean res = cached.get(cta);
    if (res == null) {
      synchronized (cached) {
        res = cached.get(cta);
        if (res == null) {
          res = hasEuroCppConfig(cta);
          cached.put(cta, res);
        }
      }
    }

    return res;

  }

  public Tmct0CuentasDeCompensacion findByCdodigoCached(Map<String, Tmct0CuentasDeCompensacion> cached, String codigo) {
    codigo = StringUtils.trim(codigo);
    Tmct0CuentasDeCompensacion cuenta = cached.get(codigo);
    if (cuenta == null) {
      synchronized (cached) {
        if ((cuenta = cached.get(codigo)) == null) {
          cuenta = findByCdCodigo(codigo);
          cached.put(codigo, cuenta);
        }
      }
    }
    return cuenta;
  }

}
