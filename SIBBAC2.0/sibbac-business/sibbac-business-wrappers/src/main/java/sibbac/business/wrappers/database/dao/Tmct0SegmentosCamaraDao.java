package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0SegmentosCamara;


@Repository
public interface Tmct0SegmentosCamaraDao extends JpaRepository< Tmct0SegmentosCamara, Long > {

	/**
	 * 
	 * @author XI316153
	 */
	@Query( "SELECT sc FROM Tmct0SegmentosCamara sc WHERE sc.tmct0CamaraCompensacion.idCamaraCompensacion = :idcamara" )
	public List< Tmct0SegmentosCamara > findByCamara( @Param( "idcamara" ) Long idcamara );

} // Tmct0SegmentosCamaraDao
