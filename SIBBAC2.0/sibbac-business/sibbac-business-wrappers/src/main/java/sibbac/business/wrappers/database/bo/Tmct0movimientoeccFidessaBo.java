package sibbac.business.wrappers.database.bo;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0movimientoeccFidessaDao;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIConstants.MO_CONSTANTS.TIPO_MOVIMIENTO;

@Service
public class Tmct0movimientoeccFidessaBo extends
    AbstractBo<Tmct0movimientoeccFidessa, Long, Tmct0movimientoeccFidessaDao> {

  public Tmct0movimientoeccFidessaBo() {
  }

  public List<Tmct0movimientoeccFidessa> findAllByCdrefmovimiento(String cdrefmovimiento) {
    return dao.findAllByCdrefmovimiento(cdrefmovimiento);
  }

  public List<Tmct0movimientoeccFidessa> findAllByCdrefmovimientoecc(String cdrefmovimiento) {
    return dao.findAllByCdrefmovimientoecc(cdrefmovimiento);
  }

  public List<Tmct0movimientoeccFidessa> findAllMovimientosGiveUpPdteAceptarCompensador(Date fcontratacion) {
    return dao.findAllByFcontratacionAndCdtipomovAndCdestadomov(fcontratacion, TIPO_MOVIMIENTO.GIVE_UP, "6");
  }

  public List<Tmct0movimientoeccFidessa> findAllByCdrefmovimientoAndFliquidacion(String cdrefmovimiento,
      Date fliquidacion) {
    return dao.findAllByCdrefmovimientoAndFliquidacion(cdrefmovimiento, fliquidacion);
  }

  public List<Tmct0movimientoeccFidessa> findAllByCdrefmovimientoeccAndFliquidacion(String cdrefmovimiento,
      Date fliquidacion) {
    return dao.findAllByCdrefmovimientoeccAndFliquidacion(cdrefmovimiento, fliquidacion);
  }

  public List<Tmct0movimientoeccFidessa> findAllByCdoperacionecc(String cdoperacionecc) {
    return dao.findAllByCdoperacionecc(cdoperacionecc);
  }

  public List<Tmct0movimientoeccFidessa> findAllByNuoperacionprev(String Nuoperacionprev) {
    return dao.findAllByNuoperacionprev(Nuoperacionprev);
  }

  public List<Long> findAllIdsByCdoperacionecc(String cdoperacionecc) {
    return dao.findAllIdsByCdoperacionecc(cdoperacionecc);
  }

  // public List<TitulosByInformacionAsignacionDTO>
  // findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByIddesglosecamaraAndInCestadomov(
  // long iddesglosecamara, List<String> cdestadosmov) {
  // return dao
  // .findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByIddesglosecamaraAndInCestadomov(
  // iddesglosecamara, cdestadosmov);
  // }
  //
  // public List<TitulosByInformacionAsignacionDTO>
  // findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByAlcIdAndInCestadomov(
  // Tmct0alcId alcId, List<String> estadosEccActivos) {
  // return dao
  // .findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByAlcIdAndInCestadomov(
  // alcId, estadosEccActivos);
  // }
  //
  // public List<Tmct0movimientoeccFidessa>
  // findAllByAlcIdAndInCestadomov(Tmct0alcId alcId,
  // List<String> estadosEccActivos) {
  // return dao.findAllByAlcIdAndInCestadomov(alcId, estadosEccActivos);
  // }

  public List<Tmct0movimientoeccFidessa> findAllByNuoperacionprevAndCuentaCompensacionDestino(String nuoperacionprev,
      String cuentaCompensacionDestino) {
    return dao.findAllByNuoperacionprevAndCuentaCompensacionDestino(nuoperacionprev, cuentaCompensacionDestino);
  }

  public List<Tmct0movimientoeccFidessa> findAllByNuoperacionprevAndMiembroCompensadorDestinoAndCdRefAsignacion(
      String nuoperacionprev, String miembroCompensadorDestino, String cdRefAsignacion) {
    return dao.findAllByNuoperacionprevAndMiembroCompensadorDestinoAndCdRefAsignacion(nuoperacionprev,
        miembroCompensadorDestino, cdRefAsignacion);
  }

  public List<Date> findAllFliquidacionByAndTitulosDisponibles() {
    return dao.findAllFliquidacionByAndTitulosDisponibles();
  }

  public List<Long> findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(Date fliquidacion, char isscrdiv) {
    return dao.findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(fliquidacion, isscrdiv);
  }

  public List<Tmct0movimientoeccFidessa> findAllByTmct0alcIdAndTitulosDisponibles(Tmct0alcId alcId) {
    return dao.findAllByTmct0alcIdAndTitulosDisponibles(alcId);
  }

}