package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.database.bsnbpsql.tgrl.model.Tgrl0eje;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0ejeId;

@Repository
public interface Tgrl0ejeDao extends JpaRepository<Tgrl0eje, Tgrl0ejeId> {

}
