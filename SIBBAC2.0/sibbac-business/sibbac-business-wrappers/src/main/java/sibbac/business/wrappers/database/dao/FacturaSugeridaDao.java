package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.FacturaSugerida;
import sibbac.business.wrappers.database.model.Periodos;


public interface FacturaSugeridaDao extends JpaRepository< FacturaSugerida, Long >, JpaSpecificationExecutor< FacturaSugerida > {

	public List< FacturaSugerida > findAllByOrderByFechaCreacionAsc();

	public List< FacturaSugerida > findByEstado( Tmct0estado estado );

	public List< FacturaSugerida > findByAliasAndPeriodo( Alias alias, Periodos periodo );

}
