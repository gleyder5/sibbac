package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0provinciasDao;
import sibbac.business.wrappers.database.model.Tmct0provincias;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0provinciasBo extends AbstractBo<Tmct0provincias, String, Tmct0provinciasDao> {

  /**
   * Devuelve todas los códigos y nombres de las provincias activas.
   * 
   * @return <code>Map<String, String> - </code>Mapa ccon el código de la provincia como clave y el nombre como valor.
   */
  public Map<String, String> findAllCodigosAndNombresActivos() {
    Map<String, String> codigos = new HashMap<String, String>();
    List<Object[]> resultados = dao.findAllCodigosAndNombresActivos();
    for (Object[] resultado : resultados) {
      codigos.put(((String) resultado[0]).trim(), ((String) resultado[1]).trim());
    }
    return codigos;
  } // findAllCodigosAndNombresActivos

  public Tmct0provincias findByCdcodigo(String cdcodigo) {
    return dao.findBycdcodigo(cdcodigo);
  }

  public List<Tmct0provincias> findAllActivas() {
    return dao.findAllActivas();
  }

  public Map<String, List<String>> findMapAllActivas() {
    List<Tmct0provincias> provincias = findAllActivas();
    Map<String, List<String>> nombresCiudadPorDistrito = new HashMap<String, List<String>>();
    if (CollectionUtils.isNotEmpty(provincias)) {
      List<String> nombres;
      List<String> nombresTrim;
      String[] nombresA;
      for (Tmct0provincias tmct0provincias : provincias) {

        nombresTrim = new ArrayList<String>();
        nombres = nombresCiudadPorDistrito.get(tmct0provincias.getCdcodigo().trim());
        if (nombres == null) {
          nombres = new ArrayList<String>();
        }
        if (StringUtils.isNotBlank(tmct0provincias.getNbprovin2())) {
          nombresA = tmct0provincias.getNbprovin2().split(";");
          if (nombresA != null && nombresA.length > 0) {
            nombres.addAll(Arrays.asList(nombresA));
          }
        }
        if (StringUtils.isNotBlank(tmct0provincias.getNbprovin())) {
          nombresA = tmct0provincias.getNbprovin().split(";");
          if (nombresA != null && nombresA.length > 0) {
            nombres.addAll(Arrays.asList(nombresA));
          }
        }

        for (String nombre : nombres) {
          nombresTrim.add(nombre.trim());
        }
        if (CollectionUtils.isNotEmpty(nombresTrim)) {
          nombresCiudadPorDistrito.put(tmct0provincias.getCdcodigo().trim(), nombresTrim);
        }
      }
    }
    return nombresCiudadPorDistrito;
  }

} // Tmct0provinciasBo
