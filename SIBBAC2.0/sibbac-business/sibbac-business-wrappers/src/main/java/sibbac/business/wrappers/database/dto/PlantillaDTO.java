package sibbac.business.wrappers.database.dto;


public class PlantillaDTO {

	private Long	idPlantilla;

	private Long	idAlias;

	private String	nombre;

	private String	descripcion;

	public PlantillaDTO() {

	}

	public PlantillaDTO( Long idPlantilla, Long idAlias, String nombre, String descripcion ) {

		this.idPlantilla = idPlantilla;
		this.idAlias = idAlias;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public Long getIdPlantilla() {
		return idPlantilla;
	}

	public void setIdPlantilla( Long idPlantilla ) {
		this.idPlantilla = idPlantilla;
	}

	public Long getIdAlias() {
		return idAlias;
	}

	public void setIdAlias( Long idAlias ) {
		this.idAlias = idAlias;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
