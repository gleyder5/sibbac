package sibbac.business.wrappers.database.bo;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.dto.EnvioTitularidadIdAndCdmiembromktDTO;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0enviotitularidadBo extends AbstractBo<Tmct0enviotitularidad, Long, Tmct0enviotitularidadDao> {

  /****************************************** QUERYS ******************************************/

  public List<EnvioTitularidadIdAndCdmiembromktDTO> findAllEnvioTitularidadForOrsFileSend(Date fechaContratacion,
      Long idCamara) {
    List<EnvioTitularidadIdAndCdmiembromktDTO> res = new ArrayList<EnvioTitularidadIdAndCdmiembromktDTO>();
    // 'P'-PENDIENTE/'C'-CANCELADO
    List<Object[]> l = dao.findAllIdsByCamaraAndFecontratacionAndEstado(fechaContratacion, idCamara, 'P', 'C',
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(), EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());
    for (Object[] o : l) {
      res.add(new EnvioTitularidadIdAndCdmiembromktDTO(((BigInteger) o[0]).longValue(), (String) o[1]));
    }

    l = dao.findAllIdsByCamaraAndFecontratacionAndEstado(fechaContratacion, idCamara, 'P', 'C',
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
        EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId());
    for (Object[] o : l) {
      res.add(new EnvioTitularidadIdAndCdmiembromktDTO(((BigInteger) o[0]).longValue(), (String) o[1]));
    }

    return res;
  }

  public List<Date> findAllDistinctFeinicioByCamaraAndBetweenFeliquidacionAndPdteEnviar(Date fliquidacion1,
      Date feliquidacion2, Long idCamara) {

    List<Date> d = dao.findAllDistinctFeinicioByCamaraAndBetweenFeliquidacionAndEstado(fliquidacion1, feliquidacion2,
        idCamara, 'P', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
        EstadosEnumerados.TITULARIDADES.ENVIANDOSE.getId());
    d.addAll(dao.findAllDistinctFeinicioByCamaraAndBetweenFeliquidacionAndEstado(fliquidacion1, feliquidacion2,
        idCamara, 'P', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
        EstadosEnumerados.TITULARIDADES.ENVIANDOSE_RECTIFICACION.getId()));

    List<Date> fechasSinDuplicados = new ArrayList<Date>();
    for (Date date : d) {
      if (!fechasSinDuplicados.contains(date)) {
        fechasSinDuplicados.add(date);
      }
    }
    return fechasSinDuplicados;
  }

  public Tmct0enviotitularidad findByReferenciaTitularAndReferenciaAdicionalAndFechaInicioAndCdmiembromktAndCdcamara(
      String refTit, String refAdicional, Date fechaInicio, String cdmiembro, String cdcamara) {
    return dao.findByReferenciaTitularAndReferenciaAdicionalAndFechaInicioAndCdmiembromktAndCdcamara(refTit,
        refAdicional, fechaInicio, cdmiembro, cdcamara);
  }

  public Tmct0enviotitularidad crearEnvioTitularidad(Tmct0desglosecamara dc, String miembroMercado,
      Tmct0referenciatitular refTit, String auditUser) {
    Tmct0enviotitularidad ti = new Tmct0enviotitularidad();
    ti.setCamaraCompensacion(dc.getTmct0CamaraCompensacion());
    ti.setCdmiembromkt(miembroMercado);
    ti.setEstado('P');
    ti.setFeinicio(dc.getFecontratacion());
    ti.setTmct0referenciatitular(refTit);
    ti.setAuditFechaCambio(new Date());
    ti.setAuditUser(auditUser);
    return ti;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public Tmct0enviotitularidad crearEnvioTitularidadNewTransaction(Tmct0desglosecamara dc, String miembroMercado,
      Tmct0referenciatitular refTit, String auditUser) {
    Tmct0enviotitularidad ti = this.crearEnvioTitularidad(dc, miembroMercado, refTit, auditUser);
    save(ti);
    return ti;
  }

  /****************************************** FIN QUERYS ******************************************/
}
