package sibbac.business.wrappers.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.bean.ServiceTextoBean;
import sibbac.business.wrappers.database.dao.TextoDao;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;


@Service
public class TextoBo extends AbstractBo< Texto, Long, TextoDao > {

	/**
	 * @return
	 */
	public List< Texto > getTextos() {
		return this.findAll();
	}

	@Transactional
	public void modificarTexto( final ServiceTextoBean serviceTextoBean ) throws SIBBACBusinessException {
		final Long id = serviceTextoBean.getId();
		Texto texto = this.findById( id );
		texto.setDescripcion( serviceTextoBean.getDescripcion() );
		texto = save( texto );
	}

}
