package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.PartenonRecordBeanBo;
import sibbac.business.wrappers.database.bo.partenonReader.PartenonTitularesRunnable;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;

@Service(value = "titularesOperacionesEspecialesRunnable")
public class TitularesOperacionesEspecialesRunnable extends PartenonTitularesRunnable {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TitularesOperacionesEspecialesRunnable.class);

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Autowired
  private PartenonRecordBeanBo partenonRecordBeanBo;

  public TitularesOperacionesEspecialesRunnable() {
    super();
  }

  @Override
  protected void savePreviousBlock(List<OperacionEspecialRecordBean> bloqueMismaOrde) {
    LOG.trace("[TitularesOperacionesEspecialesRunnable :: savePreviousBlock] inicio.");
    // PartenonRecordBean recordBean = (PartenonRecordBean) bloqueMismaOrde.get(0);
    // List<String> nurefOrds = new ArrayList<String>();
    // nurefOrds.add(recordBean.getNumOrden());
    // List<PartenonRecordBean> titulares =
    // partenonRecordBeanBo.findAllTitularesOperacionesEspecialesByListNureford(nurefOrds);
    // bloqueMismaOrde.clear();
    // for (PartenonRecordBean partenonRecordBean : titulares) {
    // partenonRecordBean.loadDireccion();
    // partenonRecordBean.loadNombreAppelidos();
    // bloqueMismaOrde.add(partenonRecordBean);
    // }

    super.savePreviousBlock(bloqueMismaOrde);

    if (CollectionUtils.isNotEmpty(bloqueMismaOrde)) {
      PartenonRecordBean partenonRecordBean = (PartenonRecordBean) bloqueMismaOrde.get(0);
      int count = desgloseRecordBeanBo.updateProcesadoByPartenonRecordBeanId(partenonRecordBean.getId(),
                                                                             EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.TITULAR_CARGADO.getId());

      LOG.trace("[TitularesOperacionesEspecialesRunnable :: savePreviousBlock] actualizados {} prcesado = ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.TITULAR_CARGADO",
                count);
    }
    LOG.trace("[TitularesOperacionesEspecialesRunnable :: savePreviousBlock] final.");
  }
}
