package sibbac.business.wrappers.database.dto.caseejecuciones;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author xIS16630
 *
 */
/**
 * @author xIS16630
 *
 */
public class Tmct0ejeInformacionMercadoCaseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2097693679831885429L;

	/**
	 * CDISIN
	 */
	private String isin;
	/**
	 * CDTPOPER
	 * */
	private char sentido;
	/**
	 * FEEJECUC
	 * */
	private Date fechaEjecucion;
	/**
	 * CDMIEMBROMKT
	 * */
	private String miembroMercado;
	/**
	 * SEGMENTTRADINGVENUE
	 * */
	private String segmentoEjecucion;
	/**
	 * TPOPEBOL
	 * */
	private String tipoOperacionBolsa;
	/**
	 * NUOPEMER
	 * */
	private String numeroOperacionMercado;
	/**
	 * NUREFERE
	 * */
	private String numeroEjecucionMercado;
	/**
	 * NUORDEN
	 * */
	private String numeroOrden;
	/**
	 * NBOOKING
	 * */
	private String numeroBooking;
	/**
	 * NUEJECUC
	 * */
	private String numeroEjecucion;
	/**
	 * NUTITIEJE
	 * */
	private BigDecimal titulos;
	/**
	 * NUTITTOTAL
	 * */
	private BigDecimal titulosTotales;

	

	public Tmct0ejeInformacionMercadoCaseDTO(String isin, char sentido, Date fechaEjecucion, String miembroMercado,
			String segmentoEjecucion, String tipoOperacionBolsa, String numeroOperacionMercado, String numeroEjecucionMercado,
			String numeroOrden, String numeroBooking, String numeroEjecucion, BigDecimal titulos, BigDecimal titulosTotales) {
		super();
		this.isin = isin;
		this.sentido = sentido;
		this.fechaEjecucion = fechaEjecucion;
		this.miembroMercado = miembroMercado;
		this.segmentoEjecucion = segmentoEjecucion;
		this.tipoOperacionBolsa = tipoOperacionBolsa;
		this.numeroOperacionMercado = numeroOperacionMercado;
		this.numeroEjecucionMercado = numeroEjecucionMercado;
		this.numeroOrden = numeroOrden;
		this.numeroBooking = numeroBooking;
		this.numeroEjecucion = numeroEjecucion;
		this.titulos = titulos;
		this.titulosTotales = titulosTotales;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public char getSentido() {
		return sentido;
	}

	public void setSentido(char sentido) {
		this.sentido = sentido;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public String getMiembroMercado() {
		return miembroMercado;
	}

	public void setMiembroMercado(String miembroMercado) {
		this.miembroMercado = miembroMercado;
	}

	public String getSegmentoEjecucion() {
		return segmentoEjecucion;
	}

	public void setSegmentoEjecucion(String segmentoEjecucion) {
		this.segmentoEjecucion = segmentoEjecucion;
	}

	public String getTipoOperacionBolsa() {
		return tipoOperacionBolsa;
	}

	public void setTipoOperacionBolsa(String tipoOperacionBolsa) {
		this.tipoOperacionBolsa = tipoOperacionBolsa;
	}

	public String getNumeroOperacionMercado() {
		return numeroOperacionMercado;
	}

	public void setNumeroOperacionMercado(String numeroOperacionMercado) {
		this.numeroOperacionMercado = numeroOperacionMercado;
	}

	public String getNumeroEjecucionMercado() {
		return numeroEjecucionMercado;
	}

	public void setNumeroEjecucionMercado(String numeroEjecucionMercado) {
		this.numeroEjecucionMercado = numeroEjecucionMercado;
	}

	public String getNumeroOrden() {
		return numeroOrden;
	}

	public void setNumeroOrden(String numeroOrden) {
		this.numeroOrden = numeroOrden;
	}

	public String getNumeroBooking() {
		return numeroBooking;
	}

	public void setNumeroBooking(String numeroBooking) {
		this.numeroBooking = numeroBooking;
	}

	public String getNumeroEjecucion() {
		return numeroEjecucion;
	}

	public void setNumeroEjecucion(String numeroEjecucion) {
		this.numeroEjecucion = numeroEjecucion;
	}

	public BigDecimal getTitulos() {
		return titulos;
	}

	public void setTitulos(BigDecimal titulos) {
		this.titulos = titulos;
	}

	public BigDecimal getTitulosTotales() {
		return titulosTotales;
	}

	public void setTitulosTotales(BigDecimal titulosTotales) {
		this.titulosTotales = titulosTotales;
	}

	@Override
	public String toString() {
		return "Tmct0ejeInformacionMercadoCaseDTO [isin=" + isin + ", sentido=" + sentido + ", fechaEjecucion=" + fechaEjecucion
				+ ", miembroMercado=" + miembroMercado + ", segmentoEjecucion=" + segmentoEjecucion + ", tipoOperacionBolsa="
				+ tipoOperacionBolsa + ", numeroOperacionMercado=" + numeroOperacionMercado + ", numeroEjecucionMercado="
				+ numeroEjecucionMercado + ", numeroOrden=" + numeroOrden + ", numeroBooking=" + numeroBooking + ", numeroEjecucion="
				+ numeroEjecucion + ", titulos=" + titulos + ", titulosTotales=" + titulosTotales + "]";
	}

}
