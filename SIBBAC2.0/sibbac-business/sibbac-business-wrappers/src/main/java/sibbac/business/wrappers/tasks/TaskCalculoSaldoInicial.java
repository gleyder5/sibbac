package sibbac.business.wrappers.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.Tmct0SaldoInicialCuentaBo;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_CONCILIACION.NAME, name = Task.GROUP_CONCILIACION.JOB_CALCULO_SALDO_INICIAL, interval = 1, delay = 0, intervalUnit = IntervalUnit.HOUR)
public class TaskCalculoSaldoInicial extends WrapperTaskConcurrencyPrevent {
    private static final String TAG = TaskCalculoSaldoInicial.class.getName();
    @Autowired
    Tmct0SaldoInicialCuentaBo saldoInicialBo;

    @Override
    public void executeTask() {
	    try {
		LOG.debug(TAG
			+ " >>===>>> Ejecutando task para calculo de saldoInicial .......");
		saldoInicialBo.processMovimientoCuentaAlias();
		LOG.debug(TAG+" :: execute ==>  Terminada la tarea de calculo inicial");
	    } catch (Exception e) {
		LOG.error(e.getMessage(), e);
		throw e;
	    }
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.CONCILIACION_SALDO_INICIAL;
    }
}
