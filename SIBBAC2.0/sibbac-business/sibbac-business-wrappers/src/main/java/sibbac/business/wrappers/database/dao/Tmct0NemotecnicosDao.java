package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0Nemotecnicos;

@Repository
public interface Tmct0NemotecnicosDao extends JpaRepository< Tmct0Nemotecnicos, Long > {
	
	@Query("SELECT nemotec FROM Tmct0Nemotecnicos nemotec WHERE nemotec.nbnombre=:nbNombre ")
	public Tmct0Nemotecnicos findByNbNombre(@Param("nbNombre") String nbNemotecnico );

}
