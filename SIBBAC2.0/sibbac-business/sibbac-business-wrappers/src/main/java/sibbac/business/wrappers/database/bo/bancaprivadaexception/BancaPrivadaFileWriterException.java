package sibbac.business.wrappers.database.bo.bancaprivadaexception;

public class BancaPrivadaFileWriterException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = -3169053865600930177L;

  public BancaPrivadaFileWriterException() {
    // TODO Auto-generated constructor stub
  }

  public BancaPrivadaFileWriterException(String arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  public BancaPrivadaFileWriterException(Throwable arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  public BancaPrivadaFileWriterException(String arg0, Throwable arg1) {
    super(arg0, arg1);
    // TODO Auto-generated constructor stub
  }

  public BancaPrivadaFileWriterException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
    // TODO Auto-generated constructor stub
  }

}
