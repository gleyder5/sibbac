package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.TextosIdiomasDao;
import sibbac.business.wrappers.database.dto.TextoIdiomaDTO;
import sibbac.business.wrappers.database.model.Idioma;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.business.wrappers.database.model.TextosIdiomas;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class TextosIdiomasBo extends AbstractBo<TextosIdiomas, Long, TextosIdiomasDao> {
    
  private static final String[] CLAVES_FACTURA = {"M","N","O","P","Q","R","S","T","U","W",
          "FACT_CONCEPTO_DETALLE","PERIODO", "IN_FAVOUR", "BENEFICIARY_BIC", "BENEFICIARY"};

  @Autowired
  private TextoBo textoBo;

  @Autowired
  private IdiomaBo idiomaBo;

  @Transactional
  public List<TextoIdiomaDTO> getListaTextosIdiomas() {
    final List<TextoIdiomaDTO> resultados = textosIdiomasToListDto((List<TextosIdiomas>) this.findAll());
    return resultados;
  }

  public TextosIdiomas findByIdiomaAndTexto(Idioma idioma, Texto texto) {
    return this.dao.findByIdiomaAndTexto(idioma, texto);
  }

  /**
   * @param textosIdiomasList
   * @return
   */
  private List<TextoIdiomaDTO> textosIdiomasToListDto(final List<TextosIdiomas> textosIdiomasList) {
    final List<TextoIdiomaDTO> resultados = new ArrayList<TextoIdiomaDTO>();
    if (CollectionUtils.isNotEmpty(textosIdiomasList)) {
      for (final TextosIdiomas textoIdioma : textosIdiomasList) {
        resultados.add(new TextoIdiomaDTO(textoIdioma));
      }

    }
    return resultados;
  }

  @Transactional
  public TextosIdiomas createTextosIdioma(final String descripcion, final Long idIdioma, final Long idTexto) throws SIBBACBusinessException {
    final Idioma idioma = idiomaBo.findById(idIdioma);
    if (idioma == null) {
    	throw new SIBBACBusinessException("No se ha recuperado el idioma con id [" + idIdioma + "]");
    }
    final Texto texto = textoBo.findById(idTexto);
    if (texto == null) {
    	throw new SIBBACBusinessException("No se ha recuperado el texto con id [" + idTexto + "]");
    }

    final TextosIdiomas textoIdioma = new TextosIdiomas();
    textoIdioma.setDescripcion(descripcion);
    textoIdioma.setIdioma(idioma);
    textoIdioma.setTexto(texto);

    return this.save(textoIdioma);

  }

  @Transactional
  public List<TextoIdiomaDTO> findTextoIdiomaByIdTexto(final Long idTexto) {
    final List<TextoIdiomaDTO> resultados = textosIdiomasToListDto(this.dao.findAllByTexto_Id(idTexto));
    return resultados;
  }

  @Transactional
  public TextoIdiomaDTO updateTextoIdioma(TextoIdiomaDTO textoIdiomaDTO) {
    final Long idTextoIdioma = textoIdiomaDTO.getIdTextoIdioma();
    TextosIdiomas textoIdioma = null;
    if (idTextoIdioma != null) {
      textoIdioma = this.findById(idTextoIdioma);
    }
    if (textoIdioma == null) {
      textoIdioma = new TextosIdiomas();
      final Idioma idioma = idiomaBo.findById(textoIdiomaDTO.getIdIdioma());
      textoIdioma.setIdioma(idioma);
      final Texto texto = textoBo.findById(textoIdiomaDTO.getIdTexto());
      textoIdioma.setTexto(texto);
    }
    textoIdioma.setDescripcion(textoIdiomaDTO.getDescripcion());
    this.dao.save(textoIdioma);
    return textoIdiomaDTO;
  }

  @Transactional
  public Map<String, String> recuperarCabecerasExcelFactura(final String codigoIdioma) {

    final List<String> tipos = new ArrayList<String>();
    tipos.add("FACT_EXCL_FECHA_OPERACION");
    tipos.add("FACT_EXCL_REFERENCIA");
    tipos.add("FACT_EXCL_COMPRA_VENTA");
    tipos.add("FACT_EXCL_ISIN");
    tipos.add("FACT_EXCL_NB_VALOR");
    tipos.add("FACT_EXCL_MERCADO");
    tipos.add("FACT_EXCL_SUBCUENTA");
    tipos.add("FACT_EXCL_NEMOTECNICO_GU");
    tipos.add("FACT_EXCL_TOTAL_TITULOS");
    tipos.add("FACT_EXCL_PRECIO_MEDIO");
    tipos.add("FACT_EXCL_CORRETAJE_TOTAL");
    tipos.add("FACT_EXCL_CORRETAJE_ORDEN");
    tipos.add("GENERAL_ABREV_COMPRA");
    tipos.add("GENERAL_ABREV_VENTA");

    return recuperarListaTextoIdioma(codigoIdioma, tipos);
  }

  @Transactional
  public Map<String, String> recuperarCabecerasExcelInformeOperaciones(final String codigoIdioma) {

    final List<String> tipos = new ArrayList<String>();
    tipos.add("INF_OPERACIONES_XLS_FECHA_OPERACION");
    tipos.add("INF_OPERACIONES_XLS_FECHA_VALOR");
    tipos.add("INF_OPERACIONES_XLS_NUESTRA_REFERENCIA");
    tipos.add("INF_OPERACIONES_XLS_COMPRA_VENTA");
    tipos.add("INF_OPERACIONES_XLS_ISIN");
    tipos.add("INF_OPERACIONES_XLS_NOMBRE_VALOR");
    tipos.add("INF_OPERACIONES_XLS_MERCADO_CENTRO_CONTRATACION");
    tipos.add("INF_OPERACIONES_XLS_TOTAL_TITULOS_EJEC_POR_ORDEN");
    tipos.add("INF_OPERACIONES_XLS_TITULAR_SUBCUENTA");
    tipos.add("INF_OPERACIONES_XLS_TOTAL_PRECIO_MEDIO_NETO_DE_LA_ORDEN");
    tipos.add("INF_OPERACIONES_XLS_CORRETAJE_TOTAL_ORDEN");
    tipos.add("INF_OPERACIONES_XLS_ESTADO_OPERACION");
    tipos.add("INF_OPERACIONES_XLS_DATO_DISCREPANTE");

    return recuperarListaTextoIdioma(codigoIdioma, tipos);
  }

  @Transactional
  public Map<String, String> recuperarCabecerasExcelIntereses(final String codigoIdioma) {
    List<String> tipos = new ArrayList<String>();
    tipos.add("INTERESES_EXCL_DIAS_DIFERENCIA");
    tipos.add("INTERESES_EXCL_FECHA_CONTRATACION");
    tipos.add("INTERESES_EXCL_FECHA_LIQUIDACION");
    tipos.add("INTERESES_EXCL_FECHA_OPERACION");
    tipos.add("INTERESES_EXCL_IMPORTE_NETO_TOTAL");
    tipos.add("INTERESES_EXCL_INTERES_DEUDORES");
    tipos.add("INTERESES_EXCL_ISIN");
    tipos.add("INTERESES_EXCL_MERCADO");
    tipos.add("INTERESES_EXCL_NB_VALOR");
    tipos.add("INTERESES_EXCL_PRECIO_MEDIO");
    tipos.add("INTERESES_EXCL_REFERENCIA");
    tipos.add("INTERESES_EXCL_SENTIDO");
    tipos.add("INTERESES_EXCL_SUBCUENTA");
    tipos.add("INTERESES_EXCL_TIPO_INTERES");
    tipos.add("INTERESES_EXCL_TOTAL_TITULOS");

    return recuperarListaTextoIdioma(codigoIdioma, tipos);
  }

  /**
   * Recupera los textos encontrados en la lista
   * @param codigoIdioma
   * @param resultados
   * @param tipos
   */
  private Map<String, String> recuperarListaTextoIdioma(final String codigoIdioma, final List<String> tipos) {
    final Map<String, String> resultados;
    final List<TextosIdiomas> literales;

    resultados = new HashMap<>();
    literales = dao.findByTexto_TipoInAndIdioma_Codigo(tipos, codigoIdioma);
    for (TextosIdiomas literal : literales) {
      resultados.put(literal.getTexto().getTipo().toString(), literal.getDescripcion());
    }
    return resultados;
  }

  @Transactional
  public Map<String, String> recuperarTextosFacturas(final String codigoIdioma) throws SIBBACBusinessException {
    return recuperarListaTextoIdioma(codigoIdioma, Arrays.asList(CLAVES_FACTURA));
  }

  public String recuperarTextoIdioma(final String tipo, final Idioma idioma) {
    final TextosIdiomas textoIdioma = dao.findByTexto_TipoAndIdioma(tipo, idioma);
    return textoIdioma.getDescripcion();
  }
}
