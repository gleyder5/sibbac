package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Idioma;


@Component
public interface IdiomaDao extends JpaRepository< Idioma, Long > {

	public Idioma findByCodigo( String codigo );
	
	public Idioma findByDescripcion( String descripcion );
}
