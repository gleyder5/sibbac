package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.bean.ServiceTextoBean;
import sibbac.business.wrappers.database.bo.TextoBo;
import sibbac.business.wrappers.database.model.Texto;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class ServiceTextos
 */
@SIBBACService
public class SIBBACServiceTexto implements SIBBACServiceBean {

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServiceTexto.class );

	@Autowired
	private TextoBo				textoBo;

	public SIBBACServiceTexto() {
		LOG.trace( "[ServiceTextos::<constructor>] Initiating SIBBAC Service for \"Texto\"." );
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.trace( "[ServiceTextos::process] Processing a web request..." );
		final WebResponse webResponse = new WebResponse();
		final Map< String, Object > resultados = new HashMap< String, Object >();
		// extrae parametros del httpRequest
		final Map< String, String > filters = webRequest.getFilters();
		final SIBBACServiceTextoCommand action = getCommand( webRequest.getAction() );
		LOG.trace( "[ServiceTextos::process] Command.: [{}]", action.getCommand() );
		try {
			switch ( action ) {
				case CMD_GET_LISTA_TEXTOS:
					LOG.trace( " Llamada el WebResponse con la lista de TEXTOS " );
					resultados.put( "listado", getListaTextos() );
					break;

				case CMD_MODIFICAR_TEXTO:
					// comprueba que los parametros han sido enviados
					modificarTexto( new ServiceTextoBean( filters ) );
					LOG.trace( "Rellenado el WebResponse con la modificacion del contacto" );
					break;

				default:
					webResponse.setError( "Comando incorrecto" );
					LOG.error( "No le llega ningun comando correcto" );
					break;
			}
			resultados.put( "status", "OK" );
		} catch ( SIBBACBusinessException e ) {
			resultados.put( "status", "KO" );
			webResponse.setError( "Se ha producido un error al ejecutar la petición" );
		}
		webResponse.setResultados( resultados );
		return webResponse;
	}

	private Map< String, Texto > getListaTextos() {

		final Map< String, Texto > resultados = new HashMap< String, Texto >();

		final List< Texto > textos = textoBo.getTextos();
		for ( final Texto texto : textos ) {
			resultados.put( texto.getId().toString(), texto );
		}
		return resultados;
	}

	private Map< String, Texto > modificarTexto( final ServiceTextoBean serviceTextoBean ) throws SIBBACBusinessException {
		final Map< String, Texto > resultados = new HashMap< String, Texto >();
		textoBo.modificarTexto( serviceTextoBean );
		return resultados;
	}

	private enum SIBBACServiceTextoCommand {

		CMD_GET_LISTA_TEXTOS( "getListaTextos" ), CMD_MODIFICAR_TEXTO( "modificarTexto" ), CMD_NO_COMMAND( "" );

		private String	command;

		/**
		 * Instantiates a new SIBBACServiceTextoCommand
		 *
		 * @param command
		 *            the command
		 */
		private SIBBACServiceTextoCommand( String command ) {
			this.command = command;
		}

		/**
		 * Gets the command.
		 *
		 * @return the command
		 */
		public String getCommand() {
			return command;
		}

	}

	/**
	 * Gets the command.
	 *
	 * @param command
	 *            the command
	 * @return the command
	 */
	private SIBBACServiceTextoCommand getCommand( final String command ) {
		final SIBBACServiceTextoCommand[] commands = SIBBACServiceTextoCommand.values();
		SIBBACServiceTextoCommand commandRequested = null;
		for ( int i = 0; i < commands.length; i++ ) {
			if ( commands[ i ].getCommand().equalsIgnoreCase( command ) ) {
				commandRequested = commands[ i ];
			}
		}
		if ( commandRequested == null ) {
			commandRequested = SIBBACServiceTextoCommand.CMD_NO_COMMAND;
			LOG.debug( "No se reconoce el comando [{}]", command );
		} else {
			LOG.debug( "Se selecciona el comando [{}]", command );
		}
		return commandRequested;
	}

	/**
	 * Gets the available commands.
	 *
	 * @return the available commands
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > result = new ArrayList< String >();
		final SIBBACServiceTextoCommand[] commands = SIBBACServiceTextoCommand.values();
		for ( int i = 0; i < commands.length; i++ ) {
			result.add( commands[ i ].getCommand() );
		}
		return result;
	}

	@Override
	public List< String > getFields() {
		return null;
	}
}
