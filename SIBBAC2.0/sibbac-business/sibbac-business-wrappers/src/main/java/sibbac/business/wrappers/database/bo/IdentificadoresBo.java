package sibbac.business.wrappers.database.bo;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.IdentificadoresDao;
import sibbac.business.wrappers.database.dto.IdentificadoresDTO;
import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.database.bo.AbstractBo;


@Service
public class IdentificadoresBo extends AbstractBo< Identificadores, Long, IdentificadoresDao > {

	public Identificadores findById( long codigo ) {
		return this.dao.findOne( codigo );
	}

	public List< IdentificadoresDTO > getLista() {
		List< Identificadores > entities = ( List< Identificadores > ) findAll();
		List< IdentificadoresDTO > resultList = new LinkedList< IdentificadoresDTO >();
		for ( Identificadores entity : entities ) {
			IdentificadoresDTO dto = entityIdentificadoresToDTO( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	// Devuelve una lista de los identificadores
	public Map< String, IdentificadoresDTO > getListaIdentificadores() {

		Map< String, IdentificadoresDTO > resultados = new HashMap< String, IdentificadoresDTO >();
		List< IdentificadoresDTO > listaIdenficadores = ( List< IdentificadoresDTO > ) this.getLista();

		for ( IdentificadoresDTO identificadoresDTO : listaIdenficadores ) {
			resultados.put( identificadoresDTO.getId().toString(), identificadoresDTO );
		}

		return resultados;
	}

	public IdentificadoresDTO entityIdentificadoresToDTO( Identificadores entity ) {
		IdentificadoresDTO dto = new IdentificadoresDTO();

		if ( entity != null ) {
			dto.setId( entity.getId() );
			dto.setIndentificadores( entity.getIndentificadores() );

		}
		return dto;
	}

	public Identificadores rellenarObjeto( Map< String, String > params ) {
		LOG.trace( "Se inicia la creacion del objeto Identificador" );
		Identificadores identificadores = new Identificadores();
		identificadores.setIndentificadores( ( Long.parseLong( params.get( Constantes.IDENTIFICADORESS_PARAM_IDENTIFICADORES ) ) ) );
		LOG.trace( " Se ha creado el objeto identificadores " );
		return identificadores;

	}

	@Transactional
	public Map< String, String > createIdentificadores( Map< String, String > params ) {

		Map< String, String > resultado = new HashMap< String, String >();

		String result = "";

		if ( this.save( this.rellenarObjeto( params ) ) != null ) {
			result = "Se ha creado el Identificador";
		} else {
			result = "No se ha podido crear el Identificador";
		}

		resultado.put( "Resultado de la operacion:", result );
		return resultado;

	}
}
