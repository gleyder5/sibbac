package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.wrappers.database.bo.FacturaBo;
import sibbac.business.wrappers.database.bo.RecordatorioFacturaPendienteBo;
import sibbac.business.wrappers.database.bo.TextoBo;
import sibbac.business.wrappers.database.dto.FacturaDTO;
import sibbac.business.wrappers.database.dto.FacturaTransicionDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.database.JobPK;
// import sibbac.business.wrappers.database.dto.FacturaPendienteCobroDTO;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceFactura implements SIBBACServiceBean {

	@Autowired
	private FacturaBo						facturaBo;
	@Autowired
	private TextoBo							textoBo;
	@Autowired
	private RecordatorioFacturaPendienteBo	recordatorioFacturaPendienteBo;
	@Autowired
	private Tmct0estadoBo					estadoBo;

	private static final Logger				LOG								= LoggerFactory.getLogger( SIBBACServiceFactura.class );

	private static final String				CMD_MARCAR_RECTIFICAR			= "marcarRectificar";
	private static final String				CMD_LISTA_FACTURAS				= "getListadoFacturas";
	private static final String				CMD_LISTA_FACTURAS_ENVIADAS		= "getListadoFacturasEnviadas";
	private static final String				CMD_LISTA_FACTURAS_PTE_COBRO	= "getListadoFacturasPendienteCobros";
	private static final String				CMD_TRANSICIONES_FACTURA		= "getTransicionesFactura";
	private static final String				CMD_ENVIAR_DUPLICADO			= "enviarDuplicado";

	private static final String				FILTER_ID_FACTURA				= "idFactura";
	private static final String				FILTER_ID_ALIAS					= "idAlias";
	private static final String				FILTER_FECHA_DESDE				= "fechaDesde";
	private static final String				FILTER_FECHA_HASTA				= "fechaHasta";
	private static final String				FILTER_ID_ESTADO				= "idEstado";
	private static final String				FILTER_NUMERO_FACTURA			= "numeroFactura";
	private static final String				RESULT_MARCAR_RECTIFICAR		= "marcarRectificar";
	private static final String				RESULT_LISTA_FACTURAS			= "listadoFacturas";

	public SIBBACServiceFactura() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		final String prefix = "[SIBBACServiceFactura::process] ";
		LOG.debug( prefix + "Processing a web request..." );

		final String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );

		final WebResponse webResponse = new WebResponse();

		switch ( command ) {
			case CMD_ENVIAR_DUPLICADO:
				this.enviarDuplicado( webRequest, webResponse );
				break;

			case CMD_LISTA_FACTURAS_ENVIADAS:
				this.getListadoFacturasEnviadas( webRequest, webResponse );
				break;

			case CMD_LISTA_FACTURAS_PTE_COBRO:
				this.getListadoFacturasPendientesCobro( webRequest, webResponse );
				break;

			case CMD_LISTA_FACTURAS:
				this.getListadoFacturas( webRequest, webResponse );
				break;

			case CMD_MARCAR_RECTIFICAR:
				final List< Map< String, String >> parametros = webRequest.getParams();
				if ( CollectionUtils.isNotEmpty( parametros ) ) {
					final List< Long > idsFacturas = getIdsFacturas( parametros );
					LOG.trace( "Recibidas " + idsFacturas.size() + " facturas para marcar" );
					final List< Long > facturasMarcadas = facturaBo.marcarFacturasRectificar( idsFacturas );
					if ( CollectionUtils.isNotEmpty( facturasMarcadas ) ) {
						LOG.trace( "Se han marcado " + facturasMarcadas.size() + " facturas" );
					} else {
						LOG.error( "No se han marcado ninguna factura" );
					}
					final Map< String, Object > resultadoMarcarFacturas = new HashMap< String, Object >();
					resultadoMarcarFacturas.put( RESULT_MARCAR_RECTIFICAR, "OK" );
					webResponse.setResultados( resultadoMarcarFacturas );
				} else {
					webResponse.setError( "No se han recibido facturas para marcar" );
				}

				break;
			case CMD_TRANSICIONES_FACTURA:
				this.getTrasicionesFactura( webRequest, webResponse );
				break;
			default:
				webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
				break;
		}
		return webResponse;
	}

	private void getListadoFacturas( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isNotEmpty( filtros ) ) {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			final Long numeroFactura = getNumeroFactura( filtros.get( FILTER_NUMERO_FACTURA ) );
			if ( numeroFactura != null ) {
				facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturas( numeroFactura ) );
			} else {
				final Integer idEstado = getIdEstado( filtros.get( FILTER_ID_ESTADO ) );
				final Long idAlias = getIdAlias( filtros.get( FILTER_ID_ALIAS ) );
				final Date fechaDesde = convertirStringADate( filtros.get( FILTER_FECHA_DESDE ) );
				final Date fechaHasta = convertirStringADate( filtros.get( FILTER_FECHA_HASTA ) );
				facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturas( idAlias, fechaDesde, fechaHasta, idEstado ) );
			}
			webResponse.setResultados( facturas );
		} else {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturas( null, null, null, null ) );
			webResponse.setResultados( facturas );
		}
	}

	/**
	 * @param filtros
	 * @return
	 */
	private Date convertirStringADate( final String fechaString ) {
		Date fecha = null;
		if ( StringUtils.isNotEmpty( fechaString ) ) {
			fecha = FormatDataUtils.convertStringToDate( fechaString );
		}
		return fecha;
	}

	/**
	 * @param idEstadoStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Integer getIdEstado( final String idEstadoStr ) throws SIBBACBusinessException {
		Integer idEstado = null;
		try {
			idEstado = FormatDataUtils.convertStringToInteger( idEstadoStr );
		} catch ( SIBBACBusinessException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idEstado\": [" + idEstadoStr + "] " );
		}

		return idEstado;
	}

	/**
	 * @param numeroFactura
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getNumeroFactura( final String numeroFacturaStr ) throws SIBBACBusinessException {
		Long numeroFactura = null;
		try {
			numeroFactura = FormatDataUtils.convertStringToLong( numeroFacturaStr );
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idEstado\": [" + numeroFactura + "] " );
		}
		return numeroFactura;
	}

	/**
	 * @param idFacturaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdFactura( final String idFacturaStr ) throws SIBBACBusinessException {
		Long idFactura = null;
		try {
			if ( StringUtils.isNotBlank( idFacturaStr ) ) {
				idFactura = NumberUtils.createLong( idFacturaStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idFactrua\": [" + idFacturaStr + "] " );
		}
		return idFactura;
	}

	/**
	 * @param idAliasStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAlias( final String idAliasStr ) throws SIBBACBusinessException {
		Long idAlias = null;
		try {
			if ( StringUtils.isNotBlank( idAliasStr ) ) {
				idAlias = NumberUtils.createLong( idAliasStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] " );
		}
		return idAlias;
	}

	private void getListadoFacturasEnviadas( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isNotEmpty( filtros ) ) {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			final Long numeroFactura = getNumeroFactura( filtros.get( FILTER_NUMERO_FACTURA ) );
			if ( numeroFactura != null ) {
				facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturasEnviadas( numeroFactura ) );
			} else {
				final Long idAlias = getIdAlias( filtros.get( FILTER_ID_ALIAS ) );
				final Date fechaDesde = convertirStringADate( filtros.get( FILTER_FECHA_DESDE ) );
				final Date fechaHasta = convertirStringADate( filtros.get( FILTER_FECHA_HASTA ) );
				facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturasEnviadas( idAlias, fechaDesde, fechaHasta ) );
			}
			webResponse.setResultados( facturas );
		} else {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturasEnviadas( null, null, null ) );
			webResponse.setResultados( facturas );
		}
	}

	private void getListadoFacturasPendientesCobro( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isNotEmpty( filtros ) ) {

			final Long idAlias = getIdAlias( filtros.get( FILTER_ID_ALIAS ) );
			final Date fechaDesde = convertirStringADate( filtros.get( FILTER_FECHA_DESDE ) );
			final Date fechaHasta = convertirStringADate( filtros.get( FILTER_FECHA_HASTA ) );
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturasPendientesCobro( idAlias, fechaDesde, fechaHasta ) );
			webResponse.setResultados( facturas );
		} else {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			facturas.put( RESULT_LISTA_FACTURAS, facturaBo.getListadoFacturasPendientesCobro( null, null, null ) );
			webResponse.setResultados( facturas );
		}
	}

	/**
	 * @param parametros
	 * @return
	 */
	private List< Long > getIdsFacturas( final List< Map< String, String >> parametros ) {
		final List< Long > idsFacturasSugeridas = new ArrayList< Long >();
		for ( Map< String, String > parametro : parametros ) {
			idsFacturasSugeridas.add( NumberUtils.createLong( parametro.get( FILTER_ID_FACTURA ) ) );
		}
		return idsFacturasSugeridas;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_MARCAR_RECTIFICAR );
		commands.add( CMD_LISTA_FACTURAS );
		commands.add( CMD_LISTA_FACTURAS_PTE_COBRO );
		commands.add( CMD_LISTA_FACTURAS_ENVIADAS );
		commands.add( CMD_TRANSICIONES_FACTURA );
		commands.add( CMD_ENVIAR_DUPLICADO );

		return commands;
	}

	@Override
	public List< String > getFields() {

		return null;
	}

	private void getTrasicionesFactura( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se ha especificado el filtro [" + FILTER_ID_FACTURA + "]" );
		}
		final Map< String, List< FacturaTransicionDTO >> transiciones = new HashMap< String, List< FacturaTransicionDTO >>();
		final Long idFactura = getIdFactura( filtros.get( FILTER_ID_FACTURA ) );
		if ( idFactura == null ) {
			throw new SIBBACBusinessException( "El filtro [" + FILTER_ID_FACTURA + "] no es válido" );
		}
		transiciones.put( RESULT_LISTA_FACTURAS, facturaBo.recuperarTransiciones( idFactura ) );
		webResponse.setResultados( transiciones );

	}

	private void enviarDuplicado( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {

		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se ha especificado el filtro [" + FILTER_ID_FACTURA + "]" );
		}

		final Long idFactura = getIdFactura( filtros.get( FILTER_ID_FACTURA ) );
		final List< String > taskMessages = new ArrayList< String >();
		JobPK jobPk = new JobPK( Task.GROUP_FACTURACION.NAME, Task.GROUP_FACTURACION.JOB_ENVIAR_DUPLICADO_FACTURA );
		Boolean envio = facturaBo.enviarDuplicado( jobPk, idFactura, true, taskMessages );

		Map< String, Boolean > resultadoCorreo = new Hashtable< String, Boolean >();
		resultadoCorreo.put( "enviado", envio );
		webResponse.setResultados( resultadoCorreo );
	}

}
