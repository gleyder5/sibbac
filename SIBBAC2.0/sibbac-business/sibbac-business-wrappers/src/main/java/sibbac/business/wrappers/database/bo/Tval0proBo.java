package sibbac.business.wrappers.database.bo;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tval0proDao;
import sibbac.business.wrappers.database.model.Tval0pro;
import sibbac.database.bo.AbstractBo;


@Service
public class Tval0proBo extends AbstractBo< Tval0pro, String, Tval0proDao > {

	public List< String > findAllCodigosActivos() {
		List<String> codigos = new ArrayList< String >();
		try{
			List<Object> resultados =  this.dao.findAllCodigos();
			for(Object resultado:resultados){
				codigos.add( (String) resultado );
			}
		}catch(Exception e){
			LOG.error( "Tval0proBo -- Error creando lista de códigos postales "+e.getMessage() );
		}
		return codigos;
	}

	
	public Map< String, String > findAllCodigosAndNombresActivos() {
		Map< String, String >  codigos = new HashMap< String, String >();
		try{
			List<Object[]> resultados =  this.dao.findAllCodigosAndNombres();
			for(Object [] resultado:resultados){
				codigos.put( (String) resultado[0], (String) resultado[1] );
			}
		}catch(Exception e){
			LOG.error( "Tval0proBo -- Error creando lista de códigos postales "+e.getMessage() );
		}
		return codigos;
	}

}
