package sibbac.business.wrappers.database.dao;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.CuentaDeCompensacionData;

@Repository
public class Tmct0CuentasDeCompensacionDaoImpl {
    private static final String TAG = Tmct0CuentasDeCompensacionDaoImpl.class.getName();
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    
    @PersistenceContext
    private EntityManager em;
    @SuppressWarnings("unchecked")
    public List<CuentaDeCompensacionData>findAllData(Map<String, Serializable>filters) {
	List<CuentaDeCompensacionData> resultList = new LinkedList<CuentaDeCompensacionData>();
	StringBuilder select = new StringBuilder("SELECT new sibbac.business.wrappers.database.data.CuentaDeCompensacionData(cc) FROM ");
	StringBuilder from = new StringBuilder("Tmct0CuentasDeCompensacion cc ");
	StringBuilder where = new StringBuilder();
	//query utils
	String fromRelMkt = ", RelacionCuentaCompMercado rel ";
	String whereInRelMkt = "rel.relacionCuentaCompMercadoPK.idCtaCompensacion.idCuentaCompensacion = cc.idCuentaCompensacion AND rel.relacionCuentaCompMercadoPK.idMercado.id in:idMercado ";
	String whereInCtaLiq = "cc.tmct0CuentaLiquidacion.id in:tmct0CuentaLiquidacion_id ";
	
	where = costructCriteria(filters, from, fromRelMkt,
		whereInRelMkt, whereInCtaLiq);
	Query query = null;
	try{
	    query = em.createQuery(select.append(from).append(where).toString());
	    String nameParam = "";
	    for (Map.Entry<String, Serializable> entry : filters.entrySet()) {
		if (entry.getValue() != null) {
		    if (entry.getKey().indexOf(".") > 0) {
			nameParam = entry.getKey().replace(".", "_");
			query.setParameter(nameParam, entry.getValue());
		    } else {
			query.setParameter(entry.getKey(), entry.getValue());
		    }
		}
	    }
	    resultList.addAll(query.getResultList());
	}catch(Exception ex){
	    LOG.error(ex.getMessage(), ex);
	}
	return resultList;
    }
    
    private StringBuilder costructCriteria(Map<String, Serializable> filters,
	    StringBuilder from, String fromRelMkt, String whereInRelMkt,
	    String whereInCtaLiq) {
	String key;
	String criteria;
	StringBuilder where = new StringBuilder();
	boolean addedWhere = false;
	for (Map.Entry<String, Serializable> entry : filters.entrySet()) {
	    if (entry.getValue() != null) {
		if (!addedWhere) {
		    addedWhere = true;
		    where.append("WHERE ");
		} else {
		    where.append("AND ");
		}
		if (entry.getKey().equals("idMercado")) {
		    from.append(fromRelMkt);
		    where.append(whereInRelMkt);
		} else if (entry.getKey().equals("tmct0CuentaLiquidacion.id")) {
		    where.append(whereInCtaLiq);
		} else {
		    key = entry.getKey();
		    // si es un campo de otra tabla, se prepara el nombre del
		    // parametro para que no contenga el . cambiando el punto
		    // por _
		    criteria = "cc."
			    + key
			    + " =:"
			    + ((key.indexOf(".") != -1) ? key.replace(".", "_")
				    + " " : key + " ");
		    where.append(criteria);
		}
	    }
	}
	return where;
    }
}
