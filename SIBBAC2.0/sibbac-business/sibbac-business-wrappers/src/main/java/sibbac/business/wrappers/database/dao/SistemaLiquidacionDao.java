package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sibbac.business.wrappers.database.model.SistemaLiquidacion;

public interface SistemaLiquidacionDao extends JpaRepository<SistemaLiquidacion, Long> {
    
}
