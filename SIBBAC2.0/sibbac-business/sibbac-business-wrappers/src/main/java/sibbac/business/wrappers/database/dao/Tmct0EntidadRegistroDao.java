package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.wrappers.database.data.EntidadRegistroData;
import sibbac.business.wrappers.database.model.Tmct0EntidadRegistro;


public interface Tmct0EntidadRegistroDao extends JpaRepository< Tmct0EntidadRegistro, Long >,
		JpaSpecificationExecutor< Tmct0EntidadRegistro > {

	@Query( "SELECT new sibbac.business.wrappers.database.data.EntidadRegistroData(er) FROM Tmct0EntidadRegistro er" )
	public List< EntidadRegistroData > findAllData();

}
