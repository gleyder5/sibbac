package sibbac.business.wrappers.database.tmct0eje.dto;

import java.io.Serializable;
import java.util.Date;

public class Tmct0ejeInformacionMercadoDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private final String cdisin;
  private final Date feejecuc;
  private final Character cdtpoper;
  private final String clearingplatform;
  private final String segmenttradingvenue;

  private final String tpopebol;
  private final String cdMiembroMkt;
  private final String nuopemer;
  private final String nurefere;

  public Tmct0ejeInformacionMercadoDTO(String cdisin,
                                       Date feejecuc,
                                       Character cdtpoper,
                                       String clearingplatform,
                                       String segmenttradingvenue,
                                       String tpopebol,
                                       String cdMiembroMkt,
                                       String nuopemer,
                                       String nurefere) {
    super();
    this.cdisin = cdisin;
    this.feejecuc = feejecuc;
    this.cdtpoper = cdtpoper;
    this.clearingplatform = clearingplatform;
    this.segmenttradingvenue = segmenttradingvenue;
    this.tpopebol = tpopebol;
    this.cdMiembroMkt = cdMiembroMkt;
    this.nuopemer = nuopemer;
    this.nurefere = nurefere;
  }

  public String getCdisin() {
    return cdisin;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public String getClearingplatform() {
    return clearingplatform;
  }

  public String getSegmenttradingvenue() {
    return segmenttradingvenue;
  }

  public String getTpopebol() {
    return tpopebol;
  }

  public String getCdMiembroMkt() {
    return cdMiembroMkt;
  }

  public String getNuopemer() {
    return nuopemer;
  }

  public String getNurefere() {
    return nurefere;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cdMiembroMkt == null) ? 0 : cdMiembroMkt.hashCode());
    result = prime * result + ((cdisin == null) ? 0 : cdisin.hashCode());
    result = prime * result + ((cdtpoper == null) ? 0 : cdtpoper.hashCode());
    result = prime * result + ((clearingplatform == null) ? 0 : clearingplatform.hashCode());
    result = prime * result + ((feejecuc == null) ? 0 : feejecuc.hashCode());
    result = prime * result + ((nuopemer == null) ? 0 : nuopemer.hashCode());
    result = prime * result + ((nurefere == null) ? 0 : nurefere.hashCode());
    result = prime * result + ((segmenttradingvenue == null) ? 0 : segmenttradingvenue.hashCode());
    result = prime * result + ((tpopebol == null) ? 0 : tpopebol.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Tmct0ejeInformacionMercadoDTO other = (Tmct0ejeInformacionMercadoDTO) obj;
    if (cdMiembroMkt == null) {
      if (other.cdMiembroMkt != null)
        return false;
    } else if (!cdMiembroMkt.equals(other.cdMiembroMkt))
      return false;
    if (cdisin == null) {
      if (other.cdisin != null)
        return false;
    } else if (!cdisin.equals(other.cdisin))
      return false;
    if (cdtpoper == null) {
      if (other.cdtpoper != null)
        return false;
    } else if (!cdtpoper.equals(other.cdtpoper))
      return false;
    if (clearingplatform == null) {
      if (other.clearingplatform != null)
        return false;
    } else if (!clearingplatform.equals(other.clearingplatform))
      return false;
    if (feejecuc == null) {
      if (other.feejecuc != null)
        return false;
    } else if (!feejecuc.equals(other.feejecuc))
      return false;
    if (nuopemer == null) {
      if (other.nuopemer != null)
        return false;
    } else if (!nuopemer.equals(other.nuopemer))
      return false;
    if (nurefere == null) {
      if (other.nurefere != null)
        return false;
    } else if (!nurefere.equals(other.nurefere))
      return false;
    if (segmenttradingvenue == null) {
      if (other.segmenttradingvenue != null)
        return false;
    } else if (!segmenttradingvenue.equals(other.segmenttradingvenue))
      return false;
    if (tpopebol == null) {
      if (other.tpopebol != null)
        return false;
    } else if (!tpopebol.equals(other.tpopebol))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Tmct0ejeInformacionMercadoDTO [cdisin=" + cdisin + ", feejecuc=" + feejecuc + ", cdtpoper=" + cdtpoper
           + ", clearingplatform=" + clearingplatform + ", segmenttradingvenue=" + segmenttradingvenue + ", tpopebol="
           + tpopebol + ", cdMiembroMkt=" + cdMiembroMkt + ", nuopemer=" + nuopemer + ", nurefere=" + nurefere + "]";
  }

  
  
}
