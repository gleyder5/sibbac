package sibbac.business.wrappers.database.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.ParametroDao;
import sibbac.business.wrappers.database.model.Parametro;
import sibbac.database.bo.AbstractBo;

@Service
public class ParametroBo extends AbstractBo< Parametro, Long, ParametroDao > {

	protected static final Logger	LOG	= LoggerFactory.getLogger( ParametroBo.class );

	public Map< String, Parametro > getListaParametro() {

		LOG.trace( "Se inicia la creacion de la lista de parametros" );
		Map< String, Parametro > resultados = new HashMap< String, Parametro >();
		List< Parametro > listaParametros = ( List< Parametro > ) this.findAll();

		for ( Parametro parametro : listaParametros ) {
			resultados.put( parametro.getId().toString(), parametro );
		}
		LOG.trace( "Se ha creado la lista de parametros" );

		return resultados;
	}

}
