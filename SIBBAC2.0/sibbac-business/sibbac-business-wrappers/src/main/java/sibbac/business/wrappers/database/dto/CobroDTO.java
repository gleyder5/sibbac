package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Cobros;


// TODO: Auto-generated Javadoc
/**
 * The Class CobroDTO.
 */
public class CobroDTO {

	/** The id. */
	private Long					id;

	/** The id alias. */
	private Long					idAlias;

	private String					nombreAlias;

	/** The fecha contabilizacion. */
	private Date					fechaContabilizacion;

	/** The importe cobrado. */
	private BigDecimal				importeCobrado;

	/** The comisiones. */
	private BigDecimal				comisiones;

	/** The cobro. */
	private Long					cobro;

	/** The lineas cobro dto. */
	private List< LineaCobroDTO >	lineasCobroDto;

	/**
	 * Instantiates a new cobro dto.
	 */
	public CobroDTO( final Cobros cobro ) {
		this.id = cobro.getId();
		final Alias alias = cobro.getAlias();
		this.idAlias = alias.getId();
		this.nombreAlias = alias.formarNombreAlias();
		this.fechaContabilizacion = cobro.getFecha() ;
		this.importeCobrado = cobro.getImporte();
		this.comisiones = cobro.getComisiones();
		this.cobro = cobro.getCobro();

	};

	/**
	 * Instantiates a new cobro dto.
	 *
	 * @param idAlias the id alias
	 * @param fechaContabilizacion the fecha contabilizacion
	 * @param importeCobrado the importe cobrado
	 */
	public CobroDTO( Long idAlias, Date fechaContabilizacion, BigDecimal importeCobrado ) {
		super();
		this.idAlias = idAlias;
		this.fechaContabilizacion = fechaContabilizacion;
		this.importeCobrado = importeCobrado;
	}

	/**
	 * Adds the linea cobro.
	 *
	 * @param idFactura the id factura
	 * @param importeAplicado the importe aplicado
	 */
	public void addLineaCobro( final Long idFactura, final BigDecimal importeAplicado, final boolean cierreFactura ) {
		if ( CollectionUtils.isEmpty( lineasCobroDto ) ) {
			lineasCobroDto = new ArrayList< LineaCobroDTO >();
		}
		lineasCobroDto.add( new LineaCobroDTO( idFactura, importeAplicado, cierreFactura ) );
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}

	/**
	 * Gets the id alias.
	 *
	 * @return the idAlias
	 */
	public Long getIdAlias() {
		return idAlias;
	}

	/**
	 * Sets the id alias.
	 *
	 * @param idAlias the idAlias to set
	 */
	public void setIdAlias( Long idAlias ) {
		this.idAlias = idAlias;
	}

	/**
	 * @return the nombreAlias
	 */
	public String getNombreAlias() {
		return nombreAlias;
	}

	/**
	 * @param nombreAlias the nombreAlias to set
	 */
	public void setNombreAlias( String nombreAlias ) {
		this.nombreAlias = nombreAlias;
	}

	/**
	 * Gets the fecha contabilizacion.
	 *
	 * @return the fechaContabilizacion
	 */
	public Date getFechaContabilizacion() {
		return fechaContabilizacion;
	}

	/**
	 * Sets the fecha contabilizacion.
	 *
	 * @param fechaContabilizacion the fechaContabilizacion to set
	 */
	public void setFechaContabilizacion( Date fechaContabilizacion ) {
		this.fechaContabilizacion = fechaContabilizacion;
	}

	/**
	 * Gets the importe cobrado.
	 *
	 * @return the importeCobrado
	 */
	public BigDecimal getImporteCobrado() {
		return importeCobrado;
	}

	/**
	 * Sets the importe cobrado.
	 *
	 * @param importeCobrado the importeCobrado to set
	 */
	public void setImporteCobrado( BigDecimal importeCobrado ) {
		this.importeCobrado = importeCobrado;
	}

	/**
	 * Gets the comisiones.
	 *
	 * @return the comisiones
	 */
	public BigDecimal getComisiones() {
		return comisiones;
	}

	/**
	 * Sets the comisiones.
	 *
	 * @param comisiones the comisiones to set
	 */
	public void setComisiones( BigDecimal comisiones ) {
		this.comisiones = comisiones;
	}

	/**
	 * Gets the lineas cobro dto.
	 *
	 * @return the lineasCobroDto
	 */
	public List< LineaCobroDTO > getLineasCobroDto() {
		return lineasCobroDto;
	}

	/**
	 * Sets the lineas cobro dto.
	 *
	 * @param lineasCobroDto the lineasCobroDto to set
	 */
	public void setLineasCobroDto( List< LineaCobroDTO > lineasCobroDto ) {
		this.lineasCobroDto = lineasCobroDto;
	}

	/**
	 * Sets the cobro.
	 *
	 * @param cobro the new cobro
	 */
	public void setCobro( Long cobro ) {
		this.cobro = cobro;
	}

	/**
	 * Gets the cobro.
	 *
	 * @return the cobro
	 */
	public Long getCobro() {
		return cobro;
	}

}
