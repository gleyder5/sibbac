package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.AliasRecordatorio;


@Component
public interface AliasRecordatoriosDao extends JpaRepository< AliasRecordatorio, Long > {

	public AliasRecordatorio findByAlias_Id( final Long idAlias );
}
