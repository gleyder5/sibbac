package sibbac.business.wrappers.database.dto;


public class AliasDireccionDTO {

	private Long	id;
	private String	cdAlias;
	private String	cdBroCli;
	private String	calle;
	private String	numero;
	private String	bloque;
	private String	poblacion;
	private String	codigoPostal;
	private String	provincia;
	private String	pais;

	public AliasDireccionDTO() {

	}

	public AliasDireccionDTO( Long id, String cdAlias, String cdBroCli, String calle, String numero, String bloque, String poblacion,
			String codigoPostal, String provincia, String pais ) {

		this.id = id;
		this.cdAlias = cdAlias;
		this.cdBroCli = cdBroCli;
		this.calle = calle;
		this.numero = numero;
		this.bloque = bloque;
		this.poblacion = poblacion;
		this.codigoPostal = codigoPostal;
		this.provincia = provincia;
		this.pais = pais;
	}

	public Long getId() {
		return id;
	}

	public String getCdAlias() {
		return cdAlias;
	}

	public String getCdBroCli() {
		return cdBroCli;
	}

	public String getCalle() {
		return calle;
	}

	public String getNumero() {
		return numero;
	}

	public String getBloque() {
		return bloque;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public String getProvincia() {
		return provincia;
	}

	public String getPais() {
		return pais;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setCdAlias( String cdAlias ) {
		this.cdAlias = cdAlias;
	}

	public void setCdBroCli( String cdBroCli ) {
		this.cdBroCli = cdBroCli;
	}

	public void setCalle( String calle ) {
		this.calle = calle;
	}

	public void setNumero( String numero ) {
		this.numero = numero;
	}

	public void setBloque( String bloque ) {
		this.bloque = bloque;
	}

	public void setPoblacion( String poblacion ) {
		this.poblacion = poblacion;
	}

	public void setCodigoPostal( String codigoPostal ) {
		this.codigoPostal = codigoPostal;
	}

	public void setProvincia( String provincia ) {
		this.provincia = provincia;
	}

	public void setPais( String pais ) {
		this.pais = pais;
	}

}
