package sibbac.business.wrappers.database.desglose.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo.CriteriosComunicacionTitulares;
import sibbac.business.wrappers.database.bo.Tmct0CamaraCompensacionBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0caseoperacionejeBo;
import sibbac.business.wrappers.database.bo.Tmct0operacioncdeccBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0parametrizacionBo;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.dto.Tmct0parametrizacionDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0caseoperacioneje;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.business.wrappers.database.model.Tmct0operacioncdecc;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.TiposSibbac.TiposComision;
import sibbac.common.TiposSibbac.TiposEnvioComisionPti;
import sibbac.common.utils.FormatDataUtils;

@Service
public class DesgloseTmct0alcHelper {

  @Autowired
  private Tmct0CamaraCompensacionBo camaraCompensacionBo;

  @Autowired
  private Tmct0caseoperacionejeBo caseOperacionEjeBo;

  @Autowired
  private Tmct0operacioncdeccBo operacionCdEccBo;

  @Autowired
  private Tmct0ordBo ordBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0parametrizacionBo parametrizacionReglaBo;

  @Autowired
  private Tmct0cfgBo cfgBo;
  
  @Value("${operativaespecial.depositaria.default.value}")
  private String codigoDepositaria;
  
  @Value("${operativaespecial.liquidadora.default.value}")
  private String codigoLiquidadora;  

  public DesgloseTmct0alcHelper() {

  }

  public Tmct0alc crearDesglosesAlcRevisionDesgloseIf(DesgloseRecordDTO dto, List<Tmct0ejecucionalocation> ejealos,
      Tmct0alo alo, Tmct0referenciatitular refTit, Tmct0infocompensacion ic) {

    // CREAMOS ALLOCATION TO CLEARER

    Tmct0alc alc = new Tmct0alc();
    alc.setTmct0alo(alo);

    // TODO CAMBIAR PARA QUE RELLENEMOS LOS DATOS SEGUN LAS BAJAS QUE HAYAMOS
    // HECHO
    inizializeAlcOperacionEspecial(dto, alo, refTit, alc);
    alc.setReferenciaTitular(refTit);

    Tmct0desglosecamara dc;
    Tmct0desgloseclitit dt;
    Tmct0movimientoecc mov;
    Tmct0movimientooperacionnuevaecc movn;
    Tmct0ejecucionalocation ejeAlo = null;
    Tmct0grupoejecucion grupo = null;
    Character corretajePtiMiembro;
    Map<Tmct0ejeId, Tmct0grupoejecucion> grupos = new HashMap<Tmct0ejeId, Tmct0grupoejecucion>();
    for (Tmct0ejecucionalocation tmct0ejecucionalocation : ejealos) {
      ejeAlo = tmct0ejecucionalocation;
      grupo = ejeAlo.getTmct0grupoejecucion();
      grupos.put(ejeAlo.getTmct0eje().getId(), grupo);
    }

    Map<String, Tmct0desglosecamara> desglosesCamara = new HashMap<String, Tmct0desglosecamara>();

    for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO eje : dto.getEjecucionesUtilizadas()) {
      dc = desglosesCamara.get(eje.getCdcamara());
      if (dc == null) {
        dc = new Tmct0desglosecamara();
        dc.setTmct0alc(alc);
        inizializeDesgloseCamara(dto, eje, dc);
        dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.TRATANDOSE_IF.getId());

        ic = new Tmct0infocompensacion();
        inizializeInfoCompensacion(ic, eje);

        // ic = infoCompensacionDao.save(ic);
        dc.setTmct0infocompensacion(ic);
        // dc = desgloseCamaraBo.save(dc);

        if (StringUtils.isNotBlank(eje.getCorretajePtiMiembroDestino())) {
          corretajePtiMiembro = eje.getCorretajePtiMiembroDestino().charAt(0);
        }
        else {
          corretajePtiMiembro = ' ';
        }

        alcBo.setTipoEnvioCorretaje(alc, dto.isRouting(), dto.isOperacionEspecial(), ic.getCdctacompensacion(),
            ic.getCdmiembrocompensador(), null, corretajePtiMiembro);
        // infoCompensacionDao.save(ic);
        alc.setIdinfocomp(ic.getIdinfocomp());

        desglosesCamara.put(eje.getCdcamara(), dc);
        alc.getTmct0desglosecamaras().add(dc);
      }
      else {
        dc.setNutitulos(dc.getNutitulos().add(eje.getNutitulos()));
      }

      grupo = grupos.get(eje.getTmct0ejeId());
      dt = new Tmct0desgloseclitit();
      dt.setTmct0desglosecamara(dc);

      inizializeDesgloseClitit(dt, dto, grupo, eje);
      // dt = desgloseClititBo.save(dt);
      dc.getTmct0desgloseclitits().add(dt);
      eje.setDesgloseRelacionado(dt);
      mov = new Tmct0movimientoecc();
      mov.setTmct0desglosecamara(dc);
      inizializeMovimientoEcc(mov, dt, dto, eje);
      // mov = movimientoEccBo.save(mov);
      dc.getTmct0movimientos().add(mov);
      movn = new Tmct0movimientooperacionnuevaecc();

      inizializeMovimientoOperacioNueva(alc, movn, dt, dto, eje);
      movn.setTmct0movimientoecc(mov);
      movn.setTmct0desglosecamara(dc);
      // movimientoOpNuevaBo.save(movn);
      mov.getTmct0movimientooperacionnuevaeccs().add(movn);

    }

    return alc;

  }

  public Tmct0alc crearDesglosesAlcOperacionEspecial(DesgloseRecordDTO dto, List<Tmct0ejecucionalocation> ejealos,
      Tmct0alo alo) {

    // CREAMOS ALLOCATION TO CLEARER

    // CREAMOS LA REFERENCIA DE TITULAR
    Tmct0referenciatitular refTit = new Tmct0referenciatitular();
    inizializeReferenciaTitular(dto, refTit);

    // ñapa para que tengamos siempre una reftitular nueva en caso que con el id
    // del dto no nos sirva
    // refTit = referenciaTitularDao.save(refTit);
    // refTit.setCdreftitularpti(getNewReferenciaTitular(refTit.getIdreferencia()));

    Tmct0alc alc = new Tmct0alc();
    alc.setTmct0alo(alo);

    inizializeAlcOperacionEspecial(dto, alo, refTit, alc);
    alc.setReferenciaTitular(refTit);

    Tmct0desglosecamara dc;
    Tmct0infocompensacion ic;
    Tmct0desgloseclitit dt;
    Tmct0movimientoecc mov;
    Tmct0movimientooperacionnuevaecc movn;
    Tmct0enviotitularidad ti;
    Tmct0DesgloseCamaraEnvioRt rt;
    Map<String, Tmct0enviotitularidad> tisPorMiembro = new HashMap<String, Tmct0enviotitularidad>();
    Tmct0ejecucionalocation ejeAlo = null;
    Tmct0grupoejecucion grupo = null;
    Map<Tmct0ejeId, Tmct0grupoejecucion> grupos = new HashMap<Tmct0ejeId, Tmct0grupoejecucion>();
    for (Tmct0ejecucionalocation tmct0ejecucionalocation : ejealos) {
      ejeAlo = tmct0ejecucionalocation;
      grupo = ejeAlo.getTmct0grupoejecucion();
      grupos.put(ejeAlo.getTmct0eje().getId(), grupo);
    }

    Map<String, Tmct0desglosecamara> desglosesCamara = new HashMap<String, Tmct0desglosecamara>();

    for (EjecucionAlocationProcesarDesglosesOpEspecialesDTO eje : dto.getEjecucionesUtilizadas()) {
      dc = desglosesCamara.get(eje.getCdcamara());
      if (dc == null) {
        dc = new Tmct0desglosecamara();
        dc.setTmct0alc(alc);
        inizializeDesgloseCamara(dto, eje, dc);
        dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.TRATANDOSE_IF.getId());

        ic = new Tmct0infocompensacion();
        inizializeInfoCompensacion(ic, eje);

        // ic = infoCompensacionDao.save(ic);
        dc.setTmct0infocompensacion(ic);
        // dc = desgloseCamaraBo.save(dc);

        Character corretajeMiembro = ' ';
        if (StringUtils.isNotBlank(eje.getCorretajePtiMiembroDestino())) {
          corretajeMiembro = eje.getCorretajePtiMiembroDestino().charAt(0);
        }

        alcBo.setTipoEnvioCorretaje(alc, dto.isRouting(), dto.isOperacionEspecial(), ic.getCdctacompensacion(),
            ic.getCdmiembrocompensador(), null, corretajeMiembro);

        alc.setCorretajePti(null);

        // infoCompensacionDao.save(ic);
        alc.setIdinfocomp(ic.getIdinfocomp());

        desglosesCamara.put(eje.getCdcamara(), dc);
        alc.getTmct0desglosecamaras().add(dc);
      }
      else {
        dc.setNutitulos(dc.getNutitulos().add(eje.getNutitulos()));
      }

      grupo = grupos.get(eje.getTmct0ejeId());
      dt = new Tmct0desgloseclitit();
      dt.setTmct0desglosecamara(dc);

      inizializeDesgloseClitit(dt, dto, grupo, eje);
      // dt = desgloseClititBo.save(dt);
      dc.getTmct0desgloseclitits().add(dt);
      eje.setDesgloseRelacionado(dt);
      if (StringUtils.isNotBlank(eje.getCdoperacionecc())) {
        mov = new Tmct0movimientoecc();
        mov.setTmct0desglosecamara(dc);
        inizializeMovimientoEcc(mov, dt, dto, eje);
        // mov = movimientoEccBo.save(mov);
        dc.getTmct0movimientos().add(mov);
        movn = new Tmct0movimientooperacionnuevaecc();

        inizializeMovimientoOperacioNueva(alc, movn, dt, dto, eje);
        movn.setTmct0movimientoecc(mov);
        movn.setTmct0desglosecamara(dc);
        // movimientoOpNuevaBo.save(movn);
        mov.getTmct0movimientooperacionnuevaeccs().add(movn);
      }
      // TODO SI HAY MTF HAY QUE METER LA CTA
      ti = tisPorMiembro.get(dt.getCdcamara().trim() + dt.getCdmiembromkt().trim());
      if (ti == null) {
        ti = new Tmct0enviotitularidad();
        inizializeEnvioTitularidad(dt, ti);
        ti.setTmct0referenciatitular(refTit);
        refTit.getTmct0enviotitularidads().add(ti);
        tisPorMiembro.put(dt.getCdcamara().trim() + dt.getCdmiembromkt().trim(), ti);
      }
      rt = new Tmct0DesgloseCamaraEnvioRt();
      inizializeDesgloseCamaraEnvioRt(dt, refTit, rt);
      rt.setEnvioTitularidad(ti);
      rt.setDesgloseCamara(dc);

      // ti.getTmct0DesgloseCamaraEnvioRt().add(rt);
      dc.getTmct0DesgloseCamaraEnvioRt().add(rt);

    }
    
    alc.setCanonCalculado('S');

    if (grupos != null) {
      grupos.clear();
    }
    return alc;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public Tmct0alc crearDesglosesAlc(Tmct0ord ord, Tmct0bok bok, Tmct0alo alo, List<Tmct0ejecucionalocation> ejealos,
      List<Tmct0eje> ejes, SettlementData settlementData) throws SIBBACBusinessException {
    Tmct0eje ejeFor;
    Tmct0desglosecamara dc;
    Tmct0infocompensacion ic;
    Tmct0desgloseclitit dt;
    Tmct0caseoperacioneje caseOpEje;
    Tmct0operacioncdecc opCdEcc;
    Tmct0parametrizacionDTO parametrizacion;
    Tmct0alc alc = new Tmct0alc();
    Short nucnfliq = alcBo.getMaxNucnfliqByTmct0aloId(alo.getId());
    if (nucnfliq == null) {
      nucnfliq = new Short("1");
    }
    else {
      nucnfliq = new Short(nucnfliq.intValue() + 1 + "");

    }
    alc.setId(new Tmct0alcId(alo.getId(), nucnfliq));
    alc.setTmct0alo(alo);
    inizializeAlc(ord, bok, alo, ejes, alc, settlementData);

    Tmct0ejecucionalocation ejeAlo = null;
    Tmct0grupoejecucion grupo = null;
    Map<Tmct0ejeId, Tmct0grupoejecucion> grupos = new HashMap<Tmct0ejeId, Tmct0grupoejecucion>();
    Map<Tmct0ejeId, Tmct0eje> ejesById = new HashMap<Tmct0ejeId, Tmct0eje>();
    Map<Long, Tmct0caseoperacioneje> caseById = new HashMap<Long, Tmct0caseoperacioneje>();
    Map<Long, Tmct0operacioncdecc> opEccById = new HashMap<Long, Tmct0operacioncdecc>();
    for (Tmct0ejecucionalocation tmct0ejecucionalocation : ejealos) {
      ejeAlo = tmct0ejecucionalocation;
      grupo = ejeAlo.getTmct0grupoejecucion();
      grupos.put(ejeAlo.getTmct0eje().getId(), grupo);
    }

    for (Tmct0eje eje : ejes) {
      ejesById.put(eje.getId(), eje);
      if (eje.getTmct0caseoperacioneje() != null) {
        if ((caseOpEje = caseById.get(eje.getTmct0caseoperacioneje().getIdcase())) == null) {
          caseOpEje = caseOperacionEjeBo.findById(eje.getTmct0caseoperacioneje().getIdcase());
          caseById.put(eje.getTmct0caseoperacioneje().getIdcase(), caseOpEje);
          opCdEcc = operacionCdEccBo.findById(caseOpEje.getTmct0operacioncdecc().getIdoperacioncdecc());
          opEccById.put(eje.getTmct0caseoperacioneje().getIdcase(), opCdEcc);
        }
      }
    }

    Map<String, Tmct0desglosecamara> desglosesCamara = new HashMap<String, Tmct0desglosecamara>();

    boolean isRouting = ordBo.isRouting(ord);
    boolean isScriptDividend = ordBo.isScriptDividend(ord);
    alc.setNutitliq(BigDecimal.ZERO);
    String cdoperacionMktSinEcc = cfgBo.getTpopebolsSinEcc();
    for (Tmct0ejecucionalocation ejeAloFor : ejealos) {
      ejeFor = ejesById.get(ejeAloFor.getTmct0eje().getId());
      dc = desglosesCamara.get(ejeFor.getClearingplatform());
      if (dc == null) {
        dc = new Tmct0desglosecamara();
        dc.setTmct0alc(alc);
        inizializeDesgloseCamara(bok, ejeAloFor, ejeFor, dc);
        dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.TRATANDOSE_IF.getId());
        ic = new Tmct0infocompensacion();
        if (!cdoperacionMktSinEcc.contains(StringUtils.trim(ejeFor.getTpopebol()))) {

          if (dc.getAsigorden() == 'S') {
            ic.setCdctacompensacion(ejeFor.getCtacomp());
            ic.setCdmiembrocompensador(ejeFor.getMiembrocomp());
            ic.setCdrefasignacion(ejeFor.getRefasignacion());
          }
          else {
            parametrizacion = parametrizacionReglaBo.findParametrizacionByCdaliass(alo.getCdalias(),
                ejeFor.getClearingplatform(), "", ord.getCdmodnego(), "", "");
            if (parametrizacion != null) {
              ic.setCdctacompensacion(parametrizacion.getCdCodigoCuentaCompensacion());
              ic.setCdmiembrocompensador(parametrizacion.getNbNombreCompensador());
              ic.setCdrefasignacion(parametrizacion.getReferenciaAsignacion());
            }
            else {
            }
          }
          // inizializeInfoCompensacion(ic, eje.getTmct0eje());

        }
        // ic = infoCompensacionDao.save(ic);
        dc.setTmct0infocompensacion(ic);

        // dc = desgloseCamaraBo.save(dc);

        // infoCompensacionDao.save(ic);
        alc.setIdinfocomp(ic.getIdinfocomp());
        alcBo.setTipoEnvioCorretaje(alc, isRouting, isScriptDividend, ic.getCdctacompensacion(),
            ic.getCdmiembrocompensador(), null, null);
        desglosesCamara.put(ejeFor.getClearingplatform(), dc);
        alc.getTmct0desglosecamaras().add(dc);
      }
      else {
        dc.setNutitulos(dc.getNutitulos().add(ejeAloFor.getNutitulosPendientesDesglose()));
      }

      grupo = grupos.get(ejeFor.getId());
      dt = new Tmct0desgloseclitit();
      dt.setTmct0desglosecamara(dc);
      caseOpEje = null;
      opCdEcc = null;
      if (ejeFor.getTmct0caseoperacioneje() != null) {
        caseOpEje = caseById.get(ejeFor.getTmct0caseoperacioneje().getIdcase());
        opCdEcc = opEccById.get(ejeFor.getTmct0caseoperacioneje().getIdcase());
      }
      inizializeDesgloseClitit(dt, ejeAloFor.getTmct0grupoejecucion(), ejeAloFor, ejeFor, caseOpEje, opCdEcc);
      // dt = desgloseClititBo.save(dt);
      dc.getTmct0desgloseclitits().add(dt);
      ejeAloFor.setNutitulosPendientesDesglose(BigDecimal.ZERO);
      alc.setNutitliq(alc.getNutitliq().add(dt.getImtitulos()));

    }
    caseById.clear();
    opEccById.clear();
    return alc;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void inizializeAlc(Tmct0ord ord, Tmct0bok bok, Tmct0alo alo, List<Tmct0eje> ejes, Tmct0alc alc,
      SettlementData settlementData) {

    if (ejes != null && ejes.get(0).getAsignadocomp() != null && ejes.get(0).getAsignadocomp() == 1) {
      alc.setAsigorden('S');
    }
    else {
      alc.setAsigorden('N');
    }

    if (settlementData.isDataCustodian() || settlementData.isDataClearer()) {
      alc.setCdcleari('S');
      alc.setClearing(true);
    }
    else {
      alc.setCdcleari('N');
      alc.setClearing(false);
    }

    alc.setCdcomisn(settlementData.getTmct0ilq().getCdcomisn());
    alc.setCddepcle("");
    alc.setCdcustod("");
    if (settlementData.isDataClearer()) {
      if (settlementData.getClearer() != null && settlementData.getClearer().getCdcleare() != null)
        alc.setCdentliq(settlementData.getClearer().getCdcleare().trim());

      // if (settlementData.getClearer().getCdordena() != null
      // && settlementData.getClearer().getCdordena().compareTo(BigDecimal.ZERO)
      // != 0) {
      // String cdordena =
      // StringUtils.leftPad(settlementData.getClearer().getCdordena().toString(),
      // 4, "0");
      // if (cdordena.equals(settlementData.getClearer().getCdcleare().trim()))
      // {
      // if (settlementData.getClearer().getBiccle() != null) {
      // alc.setCdordtit(settlementData.getClearer().getBiccle().trim());
      // }
      // else {
      // alc.setCdordtit(cdordena);
      // }
      // }
      // else {
      // alc.setCdordtit(cdordena);
      // }
      //
      // }
    }
    else {
      alc.setCdentliq("");
    }
    if (settlementData.isDataCustodian()) {
      if (settlementData.getCustodian() != null && settlementData.getCustodian().getCdcleare() != null) {
        alc.setCdentdep(settlementData.getCustodian().getCdcleare().trim());
      }
      if (settlementData.getCustodian().getBiccle() != null) {
        alc.setCdcustod(settlementData.getCustodian().getBiccle().trim());
      }
      alc.setCddepcle("1");
    }
    else {
      alc.setCdentdep("");
      alc.setCddepcle(" ");
    }
    if (settlementData.isDataTFI() && !settlementData.getHolder().isEmpty()) {
      if (settlementData.getHolder().getCdholder() != null) {
        alc.setCdniftit(settlementData.getHolder().getCdholder().trim());
      }
      if (settlementData.getHolder().getCdordtit() != null
          && !settlementData.getHolder().getCdordtit().trim().isEmpty()) {
        alc.setCdordtit(settlementData.getHolder().getCdordtit().trim());

      }
      else if (settlementData.getHolder().getCdholder() != null) {
        alc.setCdordtit(settlementData.getHolder().getCdholder().trim());
      }

      if (StringUtils.isBlank(settlementData.getHolder().getNbclient().trim())) {
        alc.setNbtitliq(StringUtils.substring(settlementData.getHolder().getNbclien1(), 0, 60));
      }
      else {
        String user = String.format("%s * %s * %s", settlementData.getHolder().getNbclient().trim(),
            settlementData.getHolder().getNbclien1().trim(), settlementData.getHolder().getNbclien2().trim()).trim();
        alc.setNbtitliq(StringUtils.substring(user, 0, 60));
      }
    }
    else {
      alc.setCdniftit("");
      alc.setNbtitliq("");
      String cdordtit = String.format("%s%s", alc.getId().getNucnfclt().trim().replaceAll("\\D", ""),
          StringUtils.leftPad(alc.getId().getNucnfliq() + "", 4, "0"));
      alc.setCdordtit(cdordtit);
    }

    if (alc.getCdordtit().length() > 25) {
      alc.setCdordtit(alc.getCdordtit().substring(0, 25));
    }

    alc.setCdenvliq("");

    alc.setCdenvmor("");
    alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    alc.setCdestliq("A");
    alc.setCdmodord(settlementData.getTmct0ilq().getCdmodord().trim());
    alc.setCdmoniso(alo.getCdmoniso());

    

    alc.setAcctatcle(alcBo.getAcctatcle(alo, settlementData));

    // Referencia bancaria
    // si es 0049 la recibida en el fichero de Partenon PENDIENTE*-
    boolean isRouting = ordBo.isRouting(ord);
    boolean isSd = ordBo.isScriptDividend(ord);
    if (isRouting || isSd) {
      alc.setCdenvliq(TiposComision.PARTENON_ROUTING);
    }
    else {
      alc.setCdenvliq(TiposComision.CORRETAJE_PTI);
    }

    alc.setCdrefban(alcBo.getCdrefban(alo, settlementData));

    alc.setCdsntprt(alo.getCdtpoper());

    if (ord.getCdtyp() != null) {
      alc.setCdtyp(ord.getCdtyp());
    }
    else {
      alc.setCdtyp(' ');
    }
    alc.setCdusuaud("SIBBAC2");

    alc.setCorretajePti('M');
    alc.setDesenliq("");
    alc.setEnviadoCuentaVirtual('N');

    alc.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadocontMercado(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadocontCanon(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadocontDevolucion(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadoentrec(new Tmct0estado());
    alc.getEstadoentrec().setIdestado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
    alc.setEstadotit(new Tmct0estado());
    alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());
    alc.setFeejeliq(alo.getFeoperac());
    alc.setFeopeliq(alo.getFevalor());
    alc.setFhaudit(new Date());
    alc.setFlagliq('0');
    if (alc.getId() == null) {
      alc.setId(new Tmct0alcId(alo.getId().getNbooking(), alo.getId().getNuorden(), new Short("1"), alo.getId()
          .getNucnfclt()));
    }
    alc.setImajusvb(BigDecimal.ZERO);
    alc.setImcanoncompdv(BigDecimal.ZERO);
    alc.setImcanoncompeu(BigDecimal.ZERO);
    alc.setImcanoncontrdv(BigDecimal.ZERO);
    alc.setImcanoncontreu(BigDecimal.ZERO);
    alc.setImcanonliqdv(BigDecimal.ZERO);
    alc.setImcanonliqeu(BigDecimal.ZERO);
    alc.setImcobrado(BigDecimal.ZERO);
    alc.setImcombco(BigDecimal.ZERO);
    alc.setImcombrk(BigDecimal.ZERO);
    alc.setImcomdvo(BigDecimal.ZERO);
    alc.setImcomisn(BigDecimal.ZERO);
    alc.setImcomsvb(BigDecimal.ZERO);
    alc.setImderges(BigDecimal.ZERO);
    alc.setImderliq(BigDecimal.ZERO);
    alc.setImefeagr(BigDecimal.ZERO);
    alc.setImfinsvb(BigDecimal.ZERO);
    alc.setImnetliq(BigDecimal.ZERO);
    alc.setImnfiliq(BigDecimal.ZERO);
    alc.setImntbrok(BigDecimal.ZERO);
    alc.setImtotbru(BigDecimal.ZERO);
    alc.setImtotnet(BigDecimal.ZERO);

    alc.setNurefbrk("");
    alc.setNutitliq(alo.getNutitcli());
    alc.setNuversion(1);
    alc.setOurpar("");
    alc.setPccomisn(alo.getPccomisn());
    alc.setPkentity("");
    alc.setRftitaud("");
    alc.setCdreftit("");
    alc.setTheirpar("");
    alc.setTmct0desglosecamaras(new ArrayList<Tmct0desglosecamara>());
    alc.setTmct0sta(new Tmct0sta());
    alc.getTmct0sta().setId(new Tmct0staId("800", "ASIGNACION"));
    alc.setTpdsaldo(settlementData.getTmct0ilq().getTpdsaldo());
    // alc.setTmct0desglosecamaras(new ArrayList<Tmct0desglosecamara>());

    alc.setPccombco(settlementData.getTmct0ilq().getPccombco());
    alc.setPccombrk(settlementData.getTmct0ilq().getPccombrk());

    alc.setOurpar(settlementData.getTmct0ilq().getOurpar().trim());
    alc.setTheirpar(settlementData.getTmct0ilq().getTheirpar().trim());
  }

  private void inizializeAlcOperacionEspecial(DesgloseRecordDTO dto, Tmct0alo alo, Tmct0referenciatitular refTit,
      Tmct0alc alc) {
    alc.setId(new Tmct0alcId(alo.getId().getNbooking(), alo.getId().getNuorden(), new Short("1"), alo.getId()
        .getNucnfclt()));
    alc.setAcctatcle("");
    alc.setAsigorden('S');
    alc.setCdcleari('N');
    alc.setCdcomisn("R");
    alc.setCddepcle("");
    alc.setCdcustod("");
    alc.setCdentdep(codigoDepositaria);
    alc.setCdentliq(codigoLiquidadora);
    if (StringUtils.isNotBlank(dto.getCodEmprl())) {
      alc.setCdentliq(dto.getCodEmprl());
      alc.setCdentdep(dto.getCodEmprl());
    }

    alc.setCdenvliq("");

    alc.setCdenvmor("");
    alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
    alc.setCdestliq("A");
    alc.setCdmodord("N");
    alc.setCdmoniso(dto.getDivisa());
    alc.setCdniftit("");
    String cdordtit = String.format("%s%s", alc.getId().getNucnfclt().trim().replaceAll("\\D", ""),
        StringUtils.leftPad(alc.getId().getNucnfliq() + "", 4, "0"));
    alc.setCdordtit(cdordtit);
    if (alc.getCdordtit().length() > 25) {
      alc.setCdordtit(alc.getCdordtit().substring(0, 25));
    }
    alc.setCdrefban(dto.getCcv());
    alc.setCdsntprt(alo.getCdtpoper());

    alc.setCdtyp(' ');
    if (StringUtils.isNotBlank(dto.getTipoSaldo())) {
      alc.setCdtyp(dto.getTipoSaldo().charAt(0));
    }
    alc.setCdusuaud("OP_ESP");
    alc.setClearing(false);
    alc.setCorretajePti(null);
    alc.setDesenliq("");
    alc.setEnviadoCuentaVirtual('N');
    alc.setEstadocont(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadocontMercado(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadocontCanon(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadocontDevolucion(new Tmct0estado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId()));
    alc.setEstadoentrec(new Tmct0estado());
    alc.getEstadoentrec().setIdestado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
    alc.setEstadotit(new Tmct0estado());
    alc.getEstadotit().setIdestado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());
    alc.setFeejeliq(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getFeoperac());
    alc.setFeopeliq(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getFevalor());
    alc.setFhaudit(new Date());
    alc.setFlagliq('0');

    alc.setImajusvb(BigDecimal.ZERO);
    alc.setImcanoncompdv(BigDecimal.ZERO);
    alc.setImcanoncompeu(BigDecimal.ZERO);
    alc.setImcanoncontrdv(BigDecimal.ZERO);
    alc.setImcanoncontreu(BigDecimal.ZERO);
    alc.setImcanonliqdv(BigDecimal.ZERO);
    alc.setImcanonliqeu(BigDecimal.ZERO);
    alc.setImcobrado(BigDecimal.ZERO);
    alc.setImcombco(BigDecimal.ZERO);
    alc.setImcombrk(BigDecimal.ZERO);
    alc.setImcomdvo(BigDecimal.ZERO);
    alc.setImcomisn(dto.getCorretajes());
    alc.setImcomsvb(BigDecimal.ZERO);
    alc.setImderges(BigDecimal.ZERO);
    alc.setImderliq(BigDecimal.ZERO);
    alc.setImefeagr(BigDecimal.ZERO);
    alc.setImfinsvb(BigDecimal.ZERO);
    alc.setImnetliq(BigDecimal.ZERO);
    alc.setImnfiliq(BigDecimal.ZERO);
    alc.setImntbrok(BigDecimal.ZERO);
    alc.setImtotbru(BigDecimal.ZERO);
    alc.setImtotnet(BigDecimal.ZERO);
    alc.setNbtitliq("");
    alc.setNurefbrk("");
    alc.setNutitliq(dto.getTitulosEjecutados());
    alc.setNuversion(1);
    alc.setOurpar("");
    alc.setPccombco(BigDecimal.ZERO);
    alc.setPccombrk(BigDecimal.ZERO);
    alc.setPccomisn(BigDecimal.ZERO);
    alc.setPkentity("");
    alc.setRftitaud(refTit.getCdreftitularpti());
    alc.setCdreftit(refTit.getCdreftitularpti());
    alc.setTheirpar("");
    alc.setTmct0desglosecamaras(new ArrayList<Tmct0desglosecamara>());
    alc.setTmct0sta(new Tmct0sta());
    alc.getTmct0sta().setId(new Tmct0staId("800", "ASIGNACION"));
    alc.setTpdsaldo("T");
    // alc.setTmct0desglosecamaras(new ArrayList<Tmct0desglosecamara>());
  }

  private String getNewReferenciaTitular(long id) {
    String refMovbase36 = Long.toString(id, Character.MAX_RADIX);
    String refTitular = "R"
        + FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.DATE_FORMAT_REF_TIT_OP_ESPECIAL)
        + StringUtils.leftPad(refMovbase36, 7, '0');
    return refTitular;
  }

  private void inizializeReferenciaTitular(DesgloseRecordDTO dto, Tmct0referenciatitular ref) {
    ref.setAuditFechaCambio(new Date());
    ref.setAuditUser("OP_ESP");
    try {
      ref.setCdreftitularpti(getNewReferenciaTitular(dto.getId()));
    }
    catch (Exception e) {
      ref.setCdreftitularpti(dto.getRefTitu());
    }
    if (dto.getRefAdic() == null) {
      ref.setReferenciaAdicional("");
    }
    else {
      ref.setReferenciaAdicional(dto.getRefAdic());
    }
  }

  private void inizializeEnvioTitularidad(Tmct0desgloseclitit dt, Tmct0enviotitularidad ti) {
    ti.setAuditFechaCambio(new Date());
    ti.setAuditUser("OP_ESP");
    ti.setEstado('P');// P-PDTE ENVIAR, C-CANCELADO, A-ACEPTADO, R-RECHAZADO
    ti.setFeinicio(dt.getFeejecuc());
    ti.setCdmiembromkt(dt.getCdmiembromkt());
    ti.setCamaraCompensacion(dt.getTmct0desglosecamara().getTmct0CamaraCompensacion());
  }

  private void inizializeDesgloseCamaraEnvioRt(Tmct0desgloseclitit dt, Tmct0referenciatitular refTit,
      Tmct0DesgloseCamaraEnvioRt rt) {
    rt.setAudit_fecha_cambio(new Date());
    rt.setAudit_user("OP_ESP");
    rt.setCdmiembromkt(dt.getCdmiembromkt());
    rt.setCdoperacionecc(dt.getCdoperacionecc());
    if (StringUtils.isNotBlank(dt.getCdoperacionecc())) {
      rt.setCriterioComunicacionTitular(dt.getCdoperacionecc());
      rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.NUMERO_OPERACION_ECC);
    }
    else if (StringUtils.isNotBlank(dt.getNumeroOperacionDCV())) {
      rt.setCriterioComunicacionTitular(dt.getNumeroOperacionDCV());
      rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.NUMERO_OPERACION_DCV);
    }
    else if (StringUtils.isNotBlank(dt.getNuordenmkt())) {
      rt.setCriterioComunicacionTitular(dt.getNuordenmkt());// YYYYMMDDXXXXXXXXX
      rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.NUMERO_ORDEN_MERCADO);

    }

    rt.setEstado('P');
    rt.setFecontratacion(dt.getFeejecuc());
    rt.setImtitulos(dt.getImtitulos());
    rt.setNudesglose(dt.getNudesglose());
    rt.setNuordenmkt(dt.getNuordenmkt());
    rt.setReferenciaTitular(refTit.getCdreftitularpti());
    rt.setCdPlataformaNegociacion(dt.getCdPlataformaNegociacion());
    rt.setTmct0desgloseclitit(dt);
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void inizializeDesgloseCamara(Tmct0bok bok, Tmct0ejecucionalocation ejealo, Tmct0eje eje,
      Tmct0desglosecamara dc) throws SIBBACBusinessException {
    if (eje.getAsignadocomp() != null && eje.getAsignadocomp() == 1) {
      dc.setAsigorden('S');
    }
    else {
      dc.setAsigorden('N');
    }

    dc.setAuditFechaCambio(new Date());
    dc.setAuditUser("SIBBAC20");
    dc.setCasepti(bok.getCasepti());
    dc.setEnvios3('N');
    dc.setFecontratacion(eje.getFeejecuc());
    dc.setFeliquidacion(eje.getFevalor());
    dc.setImputacionpropia('N');
    dc.setIsin(eje.getCdisin());
    dc.setNutitulos(ejealo.getNutitulosPendientesDesglose());
    // ejealo.setNutitulosPendientesDesglose(BigDecimal.ZERO);
    dc.setSentido(eje.getCdtpoper());
    Tmct0CamaraCompensacion camara = null;
    try {
      camara = camaraCompensacionBo.findByCdCodigo(StringUtils.trim(eje.getClearingplatform()));
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(MessageFormat.format("No se ha podido recuperar la camara: {0}. Err: {1}",
          eje.getClearingplatform(), e.getMessage()), e);
    }
    dc.setAsigorden((eje.getAsignadocomp() != null && eje.getAsignadocomp() == 1) ? 'S' : 'N');
    if (camara == null) {
      throw new SIBBACBusinessException(String.format("No encontrada la camara '%s'", eje.getClearingplatform()));
    }
    dc.setTmct0CamaraCompensacion(camara);
    // TODO LA CAMARA
    // if (eje.getIdCamara() != null) {
    // dc.setTmct0CamaraCompensacion(new Tmct0CamaraCompensacion());
    // dc.getTmct0CamaraCompensacion().setIdCamaraCompensacion(eje.getIdCamara());
    // }
    // dc.setTmct0desgloseclitits(new ArrayList<Tmct0desgloseclitit>());
    // dc.setTmct0enviotitularidad(tmct0enviotitularidad);
    dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
    dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId());
    dc.setTmct0estadoByCdestadocont(new Tmct0estado());
    dc.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
    dc.setTmct0estadoByCdestadoentrec(new Tmct0estado());
    dc.getTmct0estadoByCdestadoentrec().setIdestado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
    dc.setTmct0estadoByCdestadotit(new Tmct0estado());
    dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());
    // dc.setTmct0infocompensacion(tmct0infocompensacion);
    // dc.setTmct0movimientooperacionnuevaeccs(tmct0movimientooperacionnuevaeccs);

  }

  private void inizializeDesgloseCamara(DesgloseRecordDTO dto, EjecucionAlocationProcesarDesglosesOpEspecialesDTO eje,
      Tmct0desglosecamara dc) {

    if (eje != null && StringUtils.isNotBlank(eje.getCuentaCompensacion())
        || StringUtils.isNotBlank(eje.getMiembroDestino()) && StringUtils.isNotBlank(eje.getCdrefasignacion())) {
      dc.setAsigorden('S');
    }
    else {
      dc.setAsigorden('N');
    }

    dc.setAuditFechaCambio(new Date());
    dc.setAuditUser("OP_ESP");
    dc.setCasepti('S');
    dc.setEnvios3('N');
    dc.setFecontratacion(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getFeoperac());
    dc.setFeliquidacion(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getFevalor());
    dc.setImputacionpropia('N');
    dc.setIsin(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdisin());
    dc.setNutitulos(eje.getNutitulos());
    dc.setSentido(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdtpoper());
    if (eje.getIdCamara() != null) {
      dc.setTmct0CamaraCompensacion(new Tmct0CamaraCompensacion());
      dc.getTmct0CamaraCompensacion().setIdCamaraCompensacion(eje.getIdCamara());
    }
    // dc.setTmct0desgloseclitits(new ArrayList<Tmct0desgloseclitit>());
    // dc.setTmct0enviotitularidad(tmct0enviotitularidad);
    dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
    dc.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId());
    dc.setTmct0estadoByCdestadocont(new Tmct0estado());
    dc.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
    dc.setTmct0estadoByCdestadoentrec(new Tmct0estado());
    dc.getTmct0estadoByCdestadoentrec().setIdestado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
    dc.setTmct0estadoByCdestadotit(new Tmct0estado());
    dc.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.PDTE_ENVIAR.getId());
    // dc.setTmct0infocompensacion(tmct0infocompensacion);
    // dc.setTmct0movimientooperacionnuevaeccs(tmct0movimientooperacionnuevaeccs);

  }

  private void inizializeInfoCompensacion(Tmct0infocompensacion ic,
      EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucion) {
    ic.setAuditFechaCambio(new Date());
    ic.setAuditUser("OP_ESP");
    ic.setCdctacompensacion(ejecucion.getCuentaCompensacion());
    ic.setCdmiembrocompensador(ejecucion.getMiembroDestino());
    ic.setCdrefasignacion(ejecucion.getCdrefasignacion());
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void inizializeDesgloseClitit(Tmct0desgloseclitit dt, Tmct0grupoejecucion grupo,
      Tmct0ejecucionalocation ejealo, Tmct0eje eje, Tmct0caseoperacioneje caseOpEje, Tmct0operacioncdecc opCdEcc) {

    dt.setAuditFechaCambio(new Date());
    dt.setAuditUser("OP_ESP");
    dt.setCdcamara(eje.getClearingplatform());
    dt.setCddivisa(eje.getCdmoniso());
    dt.setCdindcotizacion(eje.getIndCotizacion());
    dt.setCdisin(eje.getCdisin());
    dt.setCdmiembromkt(eje.getCdMiembroMkt());

    dt.setCdPlataformaNegociacion(eje.getExecutionTradingVenue());

    dt.setCdsegmento(eje.getSegmenttradingvenue());
    dt.setCdsentido(eje.getCdtpoper());
    dt.setFeejecuc(eje.getFeejecuc());
    if (eje.getHoejecuc() == null) {
      dt.setHoejecuc(eje.getHoejecuc());
    }
    else {
      dt.setHoejecuc(eje.getHoordenmkt());
    }

    dt.setFeordenmkt(eje.getFeordenmkt());
    dt.setHoordenmkt(eje.getHoordenmkt());
    if (grupo != null) {
      dt.setIdgrupoeje(grupo.getIdgrupoeje());
    }

    dt.setImcanoncompdv(BigDecimal.ZERO);
    dt.setImcanoncompeu(BigDecimal.ZERO);
    dt.setImcanoncontrdv(BigDecimal.ZERO);
    dt.setImcanoncontreu(BigDecimal.ZERO);
    dt.setImcanonliqdv(BigDecimal.ZERO);
    dt.setImcanonliqeu(BigDecimal.ZERO);
    dt.setImcomisn(BigDecimal.ZERO);
    dt.setImefectivo(ejealo.getNutitulosPendientesDesglose().multiply(eje.getImcbmerc())
        .setScale(2, RoundingMode.HALF_UP));
    dt.setImfinsvb(BigDecimal.ZERO);
    dt.setImprecio(eje.getImcbmerc());
    dt.setImtitulos(ejealo.getNutitulosPendientesDesglose());
    dt.setImtotbru(BigDecimal.ZERO);
    dt.setNuejecuc(eje.getId().getNuejecuc());
    dt.setMiembroCompensadorDestino("");
    dt.setCuentaCompensacionDestino("");
    dt.setCodigoReferenciaAsignacion("");
    if (caseOpEje != null) {
      dt.setNuoperacioninic(caseOpEje.getCdoperacionecc());
      dt.setNuoperacionprev(caseOpEje.getCdoperacionecc());
      dt.setCdoperacion(caseOpEje.getCdoperacionecc());
      dt.setCdoperacionecc(caseOpEje.getCdoperacionecc());
      dt.setCdPlataformaNegociacion(caseOpEje.getCdPlataformaNegociacion());
      if (opCdEcc != null) {
        dt.setCdindcotizacion(opCdEcc.getCdindcotizacion());
        dt.setCuentaCompensacionDestino(opCdEcc.getTmct0CuentasDeCompensacion().getCdCodigo());
      }
    }
    else {
      dt.setNuoperacioninic("");
      dt.setNuoperacionprev("");
      dt.setCdoperacion("");
      dt.setCdoperacionecc("");
    }

    dt.setNuordenmkt(eje.getNuopemer());
    dt.setNurefexemkt(eje.getNurefere());
    dt.setTpopebol(eje.getTpopebol());
    if (eje.getNutittotal() != null && eje.getNutittotal().compareTo(ejealo.getNutitulosPendientesDesglose()) != 0) {
      dt.setIsEjecucionPartida('S');
    }
    else {
      dt.setIsEjecucionPartida('N');
    }
  }

  private void inizializeDesgloseClitit(Tmct0desgloseclitit dt, DesgloseRecordDTO dto, Tmct0grupoejecucion grupo,
      EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucion) {
    dt.setAuditFechaCambio(new Date());
    dt.setAuditUser("OP_ESP");
    dt.setCdcamara(ejecucion.getCdcamara());
    dt.setCddivisa(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdmoniso());
    dt.setCdindcotizacion(ejecucion.getCdindcotizacion());
    dt.setCdisin(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdisin());
    dt.setCdmiembromkt(ejecucion.getCdmiembromkt());
    dt.setCdoperacion(ejecucion.getNuoperacionprev());
    dt.setCdoperacionecc(ejecucion.getCdoperacionecc());

    dt.setCdPlataformaNegociacion(ejecucion.getCdPlataformaNegociacion());

    dt.setCdsegmento(ejecucion.getCdsegmento());
    dt.setCdsentido(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdtpoper());
    dt.setFeejecuc(ejecucion.getFeejecuc());
    if (ejecucion.getHoejecuc() == null) {
      dt.setHoejecuc(ejecucion.getHoejecuc());
    }
    else {
      dt.setHoejecuc(ejecucion.getHoordenmkt());
    }

    dt.setFeordenmkt(ejecucion.getFeordenmkt());
    dt.setHoordenmkt(ejecucion.getHoordenmkt());
    if (grupo != null) {
      dt.setIdgrupoeje(grupo.getIdgrupoeje());
    }

    dt.setImcanoncompdv(BigDecimal.ZERO);
    dt.setImcanoncompeu(BigDecimal.ZERO);
    if (ejecucion.getCanonContratacionAplicado() != null) {
      dt.setImcanoncontreu(ejecucion.getCanonContratacionAplicado());
    }
    else {
      dt.setImcanoncontreu(BigDecimal.ZERO);
    }

    if (ejecucion.getCanonContratacionTeorico() != null) {
      dt.setImcanoncontrdv(ejecucion.getCanonContratacionTeorico());
    }
    else {
      dt.setImcanoncontrdv(BigDecimal.ZERO);
    }

    dt.setImcanonliqdv(BigDecimal.ZERO);
    dt.setImcanonliqeu(BigDecimal.ZERO);
    dt.setImcomisn(BigDecimal.ZERO);
    dt.setImefectivo(ejecucion.getNutitulos().multiply(ejecucion.getImpreciomkt()).setScale(2, RoundingMode.HALF_UP));
    dt.setImfinsvb(BigDecimal.ZERO);
    dt.setImprecio(ejecucion.getImpreciomkt());
    dt.setImtitulos(ejecucion.getNutitulos());
    dt.setImtotbru(BigDecimal.ZERO);
    dt.setNuejecuc(ejecucion.getTmct0ejeId().getNuejecuc());
    dt.setNuoperacioninic(ejecucion.getNuoperacionprev());
    dt.setNuoperacionprev(ejecucion.getNuoperacionprev());
    dt.setNuordenmkt(ejecucion.getNuordenmkt());
    dt.setNurefexemkt(ejecucion.getNuexemkt());
    dt.setTpopebol(ejecucion.getTpopebol());
    // TODO falta recuperar los titulos totales de la ejecucion y compararlos
    // con los del dt para saber si esta partida
    // o no
    dt.setIsEjecucionPartida('S');
  }

  private void inizializeMovimientoEcc(Tmct0movimientoecc mov, Tmct0desgloseclitit dt, DesgloseRecordDTO dto,
      EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucion) {
    mov.setAuditFechaCambio(new Date());
    mov.setAuditUser("OP_ESP");
    mov.setCdestadomov(ejecucion.getCdestadomov());
    mov.setCdindcotizacion(ejecucion.getCdindcotizacion());
    mov.setCdrefinternaasig("");
    mov.setCdrefmovimiento(ejecucion.getCdrefmovimiento());
    mov.setCdrefmovimientoecc(ejecucion.getCdrefmovimientoecc());
    mov.setCdrefnotificacion(ejecucion.getCdrefnotificacion());
    mov.setCdrefnotificacionprev(ejecucion.getCdrefnotificacionprev());
    mov.setCdtipomov(ejecucion.getCdtipomov());
    mov.setCodigoReferenciaAsignacion(ejecucion.getCdrefasignacion());
    mov.setCuentaCompensacionDestino(ejecucion.getCuentaCompensacion());
    mov.setImefectivo(dt.getImefectivo());
    mov.setImputacionPropia('N');
    mov.setImtitulos(dt.getImtitulos());
    mov.setIsin(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdisin());
    mov.setMiembroCompensadorDestino(ejecucion.getMiembroDestino());
    mov.setSentido(dto.getAloDataOperacionesEspecialesPartenon().getAloData().getCdtpoper());
    // mov.set

  }

  private void inizializeMovimientoOperacioNueva(Tmct0alc alc, Tmct0movimientooperacionnuevaecc movn,
      Tmct0desgloseclitit dt, DesgloseRecordDTO dto, EjecucionAlocationProcesarDesglosesOpEspecialesDTO ejecucion) {
    movn.setAuditFechaCambio(new Date());
    movn.setAuditUser("OP_ESP");
    movn.setCdmiembromkt(ejecucion.getCdmiembromkt());
    movn.setCdoperacionecc(ejecucion.getCdoperacionecc());
    movn.setImefectivo(dt.getImefectivo());
    movn.setImprecio(dt.getImprecio());
    movn.setImtitulos(dt.getImtitulos());
    movn.setNuoperacioninic(ejecucion.getNuoperacionprev());
    movn.setNuoperacionprev(ejecucion.getNuoperacionprev());
    movn.setNudesglose(dt.getNudesglose());
    movn.setIdMovimientoFidessa(ejecucion.getIdMovimientoFidessa());
    if (StringUtils.isNotBlank(alc.getCdenvliq()) && TiposComision.CORRETAJE_PTI.equals(alc.getCdenvliq().trim())
        && alc.getCorretajePti() != null && TiposEnvioComisionPti.MESSAGE_PTI == alc.getCorretajePti()) {
      movn.setCorretaje(dt.getImfinsvb());
    }
    else {
      movn.setCorretaje(BigDecimal.ZERO);
    }
    movn.setTmct0desgloseclitit(dt);
  }

}
