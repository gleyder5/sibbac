package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0TipoEr;


@Repository
public interface Tmct0TipoErDao extends JpaRepository< Tmct0TipoEr, Integer > {

	public List< Tmct0TipoEr >  findAllByNombre( final String nombre );
	
}
