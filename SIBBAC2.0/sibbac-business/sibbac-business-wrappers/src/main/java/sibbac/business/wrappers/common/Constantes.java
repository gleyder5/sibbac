package sibbac.business.wrappers.common;


public class Constantes {

	// --------------------- Modulo de Contactos --------------------------------------

	public static final String FORMATO_FECHA_ISO 						= "yyMMdd";
	public static final String FORMATO_FECHA_ISO_GUIONES 				= "yyyy-MM-dd";
	
	// Operaciones de Contactos

	public static final String	CMD_ALTA_CONTACTO						= "altaContacto";
	public static final String	CMD_BAJA_CONTACTO						= "bajaContacto";
	public static final String	CMD_MODIFICACION_CONTACTO				= "modificacionContacto";
	public static final String	CMD_CAMBIOSTATUS						= "cambioStatus";
	public static final String	CMD_GETLISTA_CONTACTOS					= "getListaContactos";

	// Parametros que se envia y recibe desde el front

	public static final String	PARAM_CONTACTO_ID						= "id";
	public static final String	PARAM_ALIAS_ID							= "aliasId";
	public static final String	PARAM_CDALIASS							= "cdaliass";
	public static final String	PARAM_DESCRIPCION						= "descripcion";
	public static final String	PARAM_TELEFONO1							= "telefono1";
	public static final String	PARAM_TELEFONO2							= "telefono2";
	public static final String  PARAM_CDETRALI             = "cdEtrali";
	public static final String	PARAM_MOVIL								= "movil";
	public static final String	PARAM_FAX								= "fax";
	public static final String	PARAM_EMAIL								= "email";
	public static final String	PARAM_COMENTARIOS						= "comentarios";
	public static final String	PARAM_POSICIONCARGO						= "posicionCargo";
	public static final String	PARAM_ACTIVO							= "activo";
	public static final String	PARAM_NOMBRE							= "nombre";
	public static final String	PARAM_APELLIDO1							= "apellido1";
	public static final String	PARAM_APELLIDO2							= "apellido2";
	public static final String	PARAM_DEFECTO							= "defecto";
	public static final String	PARAM_CONTACTO_GESTOR					= "gestor";

	// ---------------------Fin Modulo Contacto--------------------------------------

	// Operaciones de TipoDeDocumentos

	public static final String	CMD_GETLISTA_TIPO_DE_DOCUMENTOS			= "getListaTipoDeDocumentos";

	// ---------------------Fin Modulo TipoDeDocumentos-----------------------------------------

	// --------------------- Modulo de GruposImpositivos --------------------------------------

	// Operaciones de Grupos Impositivos

	public static final String	CMD_GET_GRUPOS_IMPOSITIVOS				= "getGruposImpositivos";
	public static final String	CMD_ALTA								= "createGrupoImpositivo";
	public static final String	GRUPOS_IMPOSITIVOS_MODIFICACION			= "modificarGruposImpositivos";

	// Parametros que se envia y recibe desde el front

	public static final String	PARAM_ID_GRUPOIMPOSITIVO				= "id";
	public static final String	PARAM_FHVALIDODESDE						= "validoDesde";
	public static final String	PARAM_CANCELADO							= "cancelado";
	public static final String	PARAM_CATEGORIA							= "categoria";
	public static final String	PARAM_DESCRIPCION_GRUPOSIMPOSITIVOS		= "descripcion";
	public static final String	PARAM_PORCENTAJE						= "porcentaje";

	// ---------------------Fin Modulo TipoDeDocumentos------------------------------------------

	// --------------------- Modulo de Tipo De Documento --------------------------------------

	// Operaciones de Tipo De Documento

	public static final String	CMD_GET_FACTURA_RECTIFICATIVA			= "getFacturaRectificativa";
	public static final String	CMD_ALTA_TIPO_DE_DOCUMENTO				= "altaTipoDeDocumento";

	// Parametros que se envia y recibe desde el front

	public static final String	PARAM_FACTURA_RECTIFICATIVA_ID			= "idFacturaRectificativa";

	// ---------------------Fin Modulo Tipo De Documento------------------------------------------

	// --------------------- Modulo de Identificadores------------------------------

	public static final String	CMD_CREATE_IDENTIFICADORES				= "createIdentificadores";
	public static final String	CMD_GET_IDENTIFICADORES					= "getListaIdentificadores";

	public static final String	IDENTIFICADORES_PARAM_ID				= "idIdentificadores";
	public static final String	IDENTIFICADORESS_PARAM_IDENTIFICADORES	= "identificadores";

	// ---------------------Fin Modulo Identificadores---------------

	// --------------------- Modulo de Numeradores --------------------------

	/** The Constant NUMERADORES_INICIO. */
	public static final String	NUMERADORES_INICIO						= "inicio";
	/** The Constant NUMERADORES_FIN. */
	public static final String	NUMERADORES_FIN							= "fin";
	/** The Constant NUMERADORES_IDENTIFICADORES. */
	public static final String	NUMERADORES_IDENTIFICADORES				= "identificadoresId";
	/** The Constant NUMERADORES_SIGUIENTE. */
	public static final String	NUMERADORES_SIGUIENTE					= "siguiente";
	/** The Constant NUMERADORES_TIPODEDOCUMENTO. */
	public static final String	NUMERADORES_TIPODEDOCUMENTO				= "tipoDeDocumentoId";
	/** The Constant CMD_CREATE_NUMERADORES. */
	public static final String	CMD_CREATE_NUMERADORES					= "createNumeradores";
	/** The Constant CMD_GET_PARAM_NUMERADORES. */
	public static final String	CMD_GET_PARAM_NUMERADORES				= "getNumeradores";
	// ---------------------Fin Modulo de Numeradores --------------------------

	// --------------------- Modulo de Factura Pendiente Cobro -----------------------------

	// Operaciones de Factura Pendiente Cobro

	// public static final String CMD_MARCAR_FACTURAR = "marcarFacturar";
	public static final String	CMD_LISTAR_FACTURAS						= "listFacturasPendsCobro";

	// Parametros que se envia y recibe desde el front

	public static final String	PARAM_ALIAS								= "idsAlias";
	public static final String	PARAM_FECHA								= "fecha";

	// ---------------------Fin Modulo de Factura Pendiente Cobro -------------------------------------

	// --------------------- Modulo de Periodos -----------------------------
	// protected static String simpleName = "SIBBACServicePeriodos";

	// public static final String KEY_COMMAND = "command";

	// Consulta
	public static final String	CMD_CREATE_PERIODOS						= "createPeriodos";
	public static final String	CMD_GET_PARAM_PERIODOS					= "getPeriodos";
	public static final String	CMD_GET_PARAM_ESTADOS					= "getEstados";
	// Borrado
	public static final String	CMD_DELETE_PERIODOS						= "deletePeriodos";

	// cancela numeradores
	public static final String	CANCELADO								= "cancelado";

	// public static final String NUMERADORES_ID = "id";
	// Parametros
	public static final String	PERIODOS_NOMBRE							= "nombre";
	public static final String	PERIODOS_FECHAINICIOPERIODO				= "fechaInicioPeriodo";
	public static final String	PERIODOS_FECHAFINALPERIODO				= "fechaFinalPeriodo";
	public static final String	PERIODOS_IDENTIFICADORES				= "identificadores";
	public static final String	PERIODOS_ESTADO							= "estado";

	// lista de periodo
	// public static final String Periodo = "periodo";

	// Enumerado
	public static final Integer	CMD_PERIODO_ENUMERADO					= 8;

	// --------------------- Fin Modulo de Periodos -----------------------------

	// --------------------- Modulo de AliasDescripciones ---------

	// Operaciones de AliasDescripciones

	public static final String	CMD_CREATE_ALIASDIRECCIONES				= "createAliasDirecciones";
	public static final String	CMD_MODIFICACION_ALIASDIRECCIONES		= "modificacionAliasDirecciones";
	public static final String	CMD_GETLISTA_ALIASDIRECCIONES			= "getListaAliasDirecciones";

	// Parametros que se envia y recibe desde el front

	public static final String	ALIASDIRECCIONES_PARAM_ID				= "id";
	public static final String	ALIASDIRECCIONES_PARAM_ALIAS_ID			= "aliasId";
	public static final String	ALIASDIRECCIONES_PARAM_CALLE			= "calle";
	public static final String	ALIASDIRECCIONES_PARAM_NUMERO			= "numero";
	public static final String	ALIASDIRECCIONES_PARAM_BLOQUE			= "bloque";
	public static final String	ALIASDIRECCIONES_PARAM_POBLACION		= "poblacion";
	public static final String	ALIASDIRECCIONES_PARAM_CODIGOPOSTAL		= "codigoPostal";
	public static final String	ALIASDIRECCIONES_PARAM_PROVINCIA		= "provincia";
	public static final String	ALIASDIRECCIONES_PARAM_PAIS				= "pais";

	// ---------------------Fin Modulo AliasDescripciones----------------

	// --------------------- Modulo de Plantilla ---------

	// Operaciones de Plantilla

	public static final String	CMD_CREATE_PLANTILLA					= "createPlantilla";
	public static final String	CMD_MODIFICACION_PLANTILLA				= "modificacionPlantilla";
	public static final String	CMD_GETLISTA_PLANTILLA					= "getListaPlantilla";
	public static final Character PLANTILLA_ALTA = 'A';
	public static final Character PLANTILLA_BAJA = 'B';

	// Parametros que se envia y recibe desde el front

	public static final String	PLANTILLA_PARAM_ID						= "id";
	public static final String	PLANTILLA_PARAM_ALIAS_ID				= "aliasId";
	public static final String	PLANTILLA_PARAM_NOMBRE					= "nombre";
	public static final String	PLANTILLA_PARAM_DESCRIPCION				= "descripcion";
	public static final String	IDIOMA_ESPANOL				= "ESPAÑOL";
	public static final String	IDIOMA_INGLES				= "INGLES";

	// ---------------------Fin Modulo Plantilla----------------

	// --------------------- Modulo de AliasImporteMinimo ---------

	// Operaciones de AliasDescripciones

	public static final String	CMD_MODIFICACION_ALIAS_IMPORTEMINIMO	= "modificacionAliasImporteMinimo";
	public static final String	CMD_GETLISTA_ALIAS_IMPORTEMINIMO		= "getListaAliasImporteMinimo";

	// Parametros que se envia y recibe desde el front

	public static final String	ALIASIMPORTEMINIMO_PARAM_ID				= "id";
	public static final String	ALIASIMPORTEMINIMO_PARAM_ALIAS_ID		= "aliasId";
	public static final String	ALIASIMPORTEMINIMO_IMPORTE				= "importeMinimo";
	public static final String	ALIASIMPORTEMINIMO_TEXTOS_ID			= "textoId";
	public static final String	ALIASIMPORTEMINIMO_IDIOMA_ID			= "idiomaId";
	public static final String	ALIASIMPORTEMINIMO_PLANTILLA_ID			= "plantillaId";
	public static final String	ALIASIMPORTEMINIMO_RECORDATORIO			= "recordatorio";
	public static final String	ALIASIMPORTEMINIMO_FRECUENCIA			= "frecuencia";

	// ---------------------Fin Modulo AliasImporteMinimo----------------

	// --------------------- Modulo de OrdenesFactura --------------------------------------
	public static final String	CMD_GET_ORDENESFACTURA					= "getListaOrdenesFactura";
	public static final String	ORDENESFACTURA_PARAM_FACTURA_ID			= "facturaId";

	// ---------------------Fin Modulo OrdenesFactura------------------------------

	// --------------------- Modulo de Etiquetas --------------------------------------
	public static final String	CMD_GET_ETIQUETAS						= "getListaEtiquetas";
	
	
	public enum TipoAliasBancaPrivada{
	  ALIAS_CTM, ALIAS_NO_CTM;
	}

	// ---------------------Fin Modulo OrdenesFactura------------------------------

	// --------------------- Modulo Interfaces Emitidas ------------------------------------
	public static final String INTERFACES_EMITIDAS = "INTERFACES EMITIDAS";
	public static final String GRUPO_INTERFACES = "Interfaces";
	public static final String GRUPO_ESTADOS = "Estados";
	
	// ---------------------Fin Modulo Interfaces Emitidas ------------------------------
	
	// --------------------- Modulo Utilities ------------------------------------
	public static final String UTILITIES = "UTILITIES";
	// ---------------------Fin Modulo Utilities ------------------------------
	
	// --------------------- Modulo Generacion Informes ------------------------------------
	public static final String GENERACION_INFORMES = "GENERACION INFORMES";
	// ---------------------Fin Modulo Utilities ------------------------------
	
	// --------------------- Modulo Facturas Manuales ------------------------------------
	public static final String GRUPO_ENTREGABIEN = "Entrega Bien";
	public static final String ESTADO_DEFECTO_CREACION = "FACTURA GUARDADA";
	public static final String ESTADO_CONTABILIZADA = "FACTURA CONTABILIZADA";
	public static final String ESTADO_ANULADA = "FACTURA ANULADA";
	public static final String ESTADO_PDTE_CONTABILIZAR = "FACTURA PDTE. CONTABILIZAR";
	public static final String ESTADO_BAJA = "FACTURA BAJA";
	public static final String CLAVE_COMENTARIO_POR_DEFECTO = "comentarioPorDefecto";
	// --------------------- Fin Modulo Facturas Manuales ------------------------------------
	
	// --------------------- Modulo Generador Pdf ------------------------------------
	public static final String GRUPO_GENERADORPDF = "GeneradorPdf";
	public static final String CLAVE_NOMBRE_SANTANDER = "NOMBRE SANTANDER";
	public static final String CLAVE_CIF_SANTANDER = "CIF SANTANDER";
	public static final String CLAVE_BENEFICIARY = "BENEFICIARY";
	public static final String CLAVE_BENEFICIARY_BIC = "BENEFICIARY BIC";
	public static final String CLAVE_BIC_CODE = "BIC CODE";
	public static final String CLAVE_IBAN = "IBAN";
	public static final String CLAVE_BANK_ACCOUNT = "BANK ACCOUNT";
	public static final String CLAVE_DESCRIPCION_SANTANDER = "DESCRIPCION SANTANDER";
	public static final String CLAVE_LEGAL = "LEGAL";
	public static final String CLAVE_DIRECCION_SANTANDER = "DIRECCION SANTANDER";
	public static final String CLAVE_PROVINCIA_SANTANDER = "PROVINCIA SANTANDER";
	public static final String CLAVE_PAIS_SANTANDER = "PAIS SANTANDER";
	public static final String CLAVE_PIE_PAGINA_GRUPO_IVA = "PIE PAGINA GRUPO IVA";
	public static final String CLAVE_BASE_IMPONIBLE_ANTERIOR = "BASE IMPONIBLE ANTERIOR";
	public static final String CLAVE_BASE_IMPONIBLE_ACTUAL = "BASE IMPONIBLE ACTUAL";
	public static final String CLAVE_NOMBRE_RECTIFICATIVA = "NOMBRE RECTIFICATIVA";
	public static final String CLAVE_NOMBRE_NORMAL = "NOMBRE NORMAL";
	public static final String CLAVE_NOMBRE_GRUPO_IVA = "NOMBRE GRUPO IVA";
	public static final String CLAVE_NOMBRE_RECTIFICATIVA_GRUPO_IVA = "NOMBRE RECTIFICATIVA GRUPO IVA";
	// --------------------- Fin Modulo Generador Pdf ------------------------------------
	
	// --------------------- Usuario SIBBAC ------------------------------------
	public static final String USUARIO_SIBBAC = "SIBBAC";
	public static final Long VERSION = Long.valueOf(0);
	public static final String PREFIX_TIPO = "INFORME_";
	public static final String SUFFIX_TIPO_SUBJECT = "_SUBJECT";
	public static final String SUFFIX_TIPO_BODY = "_BODY";
	public static final String PREFIX_DESCRIPCIONES = "Informe ";
	public static final String SUFFIX_DESCRIPCIONES_BODY = ". Body";
	public static final String SUFFIX_DESCRIPCIONES_SUBJECT = ". Subject";
	public static final String ALTA = "ALTA";
	public static final String MODIFICACION = "MODIFICACION";
	public static final String CLAVE_JOB_GENERAR_INFORMES = "batchInformes";
	public static final String VALOR_JOB_GENERAR_INFORMES = "BatchGenerarInformes";
	
	// --------------------- Usuario SIBBAC ------------------------------------
}
