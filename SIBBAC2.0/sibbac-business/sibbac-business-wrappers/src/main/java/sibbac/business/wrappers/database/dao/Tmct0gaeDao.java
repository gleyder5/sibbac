package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0gae;
import sibbac.business.wrappers.database.model.Tmct0gaeId;

@Repository
public interface Tmct0gaeDao extends JpaRepository<Tmct0gae, Tmct0gaeId> {

  @Query("select g.tmct0gal.imcambio, sum(g.nutitasig) "
      + " from Tmct0gae g where g.id.nuorden = :nuorden and g.id.nbooking = :nbooking and g.id.nucnfclt = :nucnfclt "
      + " group by g.tmct0gal.imcambio")
  List<Object[]> sumTitulosByPriceAndAloId(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nucnfclt") String nucnfclt);

}
