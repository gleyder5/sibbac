package sibbac.business.wrappers.database.bo.bancaPrivadaWriter;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dto.HoraEjeccucionDTO;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.common.Constantes.TipoAliasBancaPrivada;
import sibbac.business.wrappers.common.fileWriter.SibbacFileWriter;
import sibbac.business.wrappers.database.bo.DateUtilService;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.bancaprivadaexception.BancaPrivadaFileWriterException;
import sibbac.business.wrappers.database.bo.bancaprivadaexception.BancaPrivadaWriterAliasNotFoundException;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;

@Component
public class BancaPrivadaWriter extends SibbacFileWriter<OperacionEspecialRecordBean> {

  protected static final Logger LOG = LoggerFactory.getLogger(BancaPrivadaWriter.class);

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${sibbac.wrappers.banca.privada.1819.file.tmp.name:BANCA_PRIVADA_1819.TMP}")
  private String tmpFileName;

  // fileName = "/tempORW3.VAL.CTL.BDK.V6JR1819.D" + FormatDataUtils.convertDateToString(new Date()).substring(2)
  // + ".BAN1819";
  @Value("${sibbac.wrappers.banca.privada.1819.file.prefix:ORW3.VAL.CTL.BDK.V6JR1819.D}")
  private String finalFileNamePrefix;

  @Value("${sibbac.wrappers.banca.privada.1819.file.sufix:.BAN1819}")
  private String finalFileNameSufix;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0ejeBo ejeBo;
  
  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private DateUtilService dateUtilService;

  private FlatFileItemWriter<RecordBean> writer;
  private DelimitedLineAggregator<RecordBean> delLineAgg;
  private BeanWrapperFieldExtractor<RecordBean> fieldExtractor18;
  private BeanWrapperFieldExtractor<RecordBean> fieldExtractor19;

  String fileName = null;

  @Autowired
  public BancaPrivadaWriter(@Value("${sibbac.wrappers.partenonOutFolder}") String folderName) {
    super(folderName);
    LOG.debug("[BancaPrivadaWriter :: Constructor] Init - Folder: " + folderName);

    initializeDelimitedLineAggregator();
    initalizeFieldExtractor18();
    initializeFieldExtractor19();

    LOG.debug("[BancaPrivadaWriter :: Constructor] Fin - Folder: " + folderName);
  }

  private void initalizeWriter(File file) throws BancaPrivadaFileWriterException {
    LOG.debug("[BancaPrivadaWriter :: initalizeWriter] Init");
    try {
      writer = new FlatFileItemWriter<RecordBean>();
      writer.setAppendAllowed(true);
      writer.setShouldDeleteIfEmpty(true);
      writer.setTransactional(true);
      writer.setShouldDeleteIfExists(true);

      writer.setResource(new FileSystemResource(file));
      writer.open(new ExecutionContext());
      writer.setLineAggregator(delLineAgg);
    } catch (Exception e) {
      LOG.debug("[BancaPrivadaWriter :: initalizeWriter] Incidencia abriendo fichero: " + e.getMessage());
      throw new BancaPrivadaFileWriterException("No se ha podido inicializar el writer.", e);
    }
    LOG.debug("[BancaPrivadaWriter :: initalizeWriter] Fin");
  }

  private void initializeDelimitedLineAggregator() {
    LOG.debug("[BancaPrivadaWriter :: initializeDelimitedLineAggregator] Init");
    delLineAgg = new DelimitedLineAggregator<RecordBean>();
    delLineAgg.setDelimiter("");
    LOG.debug("[BancaPrivadaWriter :: initializeDelimitedLineAggregator] Fin");
  }

  private void initalizeFieldExtractor18() {
    LOG.debug("[BancaPrivadaWriter :: initalizeFieldExtractor18] Init");
    fieldExtractor18 = new BeanWrapperFieldExtractor<RecordBean>();
    fieldExtractor18.setNames(new String[] { "tipoRegistro", "numrefer", "fecontr", "horcontr", "crefext", "refextsv", "codorgej", "emprctva", "centctva", "dgcoctva", "prodctva", "dptectva", "tipopera", "codisin", "tipocamb", "cambio", "intfinan", "fecliqui", "numtitej", "nominej", "canal", "tipoprop", "nejecmer", "numejecu", "cdorgneg", "cdbroker", "reftit", "coderr", "refadic", "descerr"
    });
    LOG.debug("[BancaPrivadaWriter :: initalizeFieldExtractor18] Fin");
  }

  private void initializeFieldExtractor19() {
    LOG.debug("[BancaPrivadaWriter :: initializeFieldExtractor19] Init");
    fieldExtractor19 = new BeanWrapperFieldExtractor<RecordBean>();
    fieldExtractor19.setNames(new String[] { "tipoRegistro", "numrefer", "fhlote", "hlote", "feccontr", "fliqui", "efecneto", "numtitej", "nomijec", "cbomedio", "broker", "divisa", "cencont", "cdorecc", "comibroker", "comcontr", "corretajes", "cancont", "abcancom", "cancomp", "canliq", "reftit", "coderr", "descerr", "filler"
    });
    LOG.debug("[BancaPrivadaWriter :: initializeFieldExtractor19] Fin");
  }

  @Override
  @Transactional
  public boolean writeIntoFile() throws SIBBACBusinessException {

    LOG.debug("[BancaPrivadaWriter :: writeIntoFile] Init");
    LOG.debug("[BancaPrivadaWriter :: writeIntoFile] Config file prefix: {}", finalFileNamePrefix);
    LOG.debug("[BancaPrivadaWriter :: writeIntoFile] Config file sufix: {}", finalFileNameSufix);
    LOG.debug("[BancaPrivadaWriter :: writeIntoFile] Config file tmp: {}", tmpFileName);

    HoraEjeccucionDTO horaEjecucion = cfgBo.getIfIsHoraEjecucionFicheroSpb1819();

    if (horaEjecucion == null) {
      LOG.warn("[BancaPrivadaWriter :: writeIntoFile] no esta planificado para ejecutar a esta hora.");
      return false;
    }

    fileName = tmpFileName + "_"
               + FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.CONCURRENT_FILE_DATETIME_FORMAT);

    File file = new File(folderName, fileName);
    file.getParentFile().mkdirs();
    try {
      if (!file.exists()) {
        file.createNewFile();
      }
    } catch (IOException e1) {
      throw new SIBBACBusinessException("Incidencia al crear el fichero: " + fileName, e1);

    }
    boolean hasWorked = false;
    try {
      initalizeWriter(file);

      Date fechaLote = new Date();
      List<Integer> workDaysOfWeek = dateUtilService.getWorkDaysOfWeek();
      List<Date> holidays = dateUtilService.getHolidays();
      Date fechaInicio = DateHelper.getPreviousWorkDate(new Date(), horaEjecucion.getdDiaDesde(), workDaysOfWeek,
                                                        holidays);

      Date fechaFinal = DateHelper.getPreviousWorkDate(new Date(), horaEjecucion.getdDiaHasta(), workDaysOfWeek,
                                                       holidays);
      hasWorked = writeAliasFromTmct0cfg(Tmct0cfgConfig.FICHERO_SPB_ALIAS_CODE, TipoAliasBancaPrivada.ALIAS_NO_CTM,
                                         fechaInicio, fechaFinal, fechaLote, workDaysOfWeek, holidays);

      hasWorked = writeAliasFromTmct0cfg(Tmct0cfgConfig.FICHERO_SPB_ALIAS_CODE_CTM, TipoAliasBancaPrivada.ALIAS_CTM,
                                         fechaInicio, fechaFinal, fechaLote, workDaysOfWeek, holidays);
    } catch (BancaPrivadaWriterAliasNotFoundException | BancaPrivadaFileWriterException e) {
      throw new SIBBACBusinessException(e);
    }

    LOG.debug("[BancaPrivadaWriter :: writeIntoFile] Fin");
    return hasWorked;
  }

  @Transactional
  private boolean writeAliasFromTmct0cfg(Tmct0cfgConfig cfgEnum,
                                         TipoAliasBancaPrivada tipoAlias,
                                         Date fechaInicio,
                                         Date fechaFinal,
                                         Date fechaLote,
                                         List<Integer> workDaysOfWeekConfigList,
                                         List<Date> holidaysConfigList) throws BancaPrivadaWriterAliasNotFoundException,
                                                                       BancaPrivadaFileWriterException {
    String[] aliasesSeparados;
    String aliases = "";
    try {
      Tmct0cfg cfg = cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName, cfgEnum.getProcess(),
                                                                cfgEnum.getKeyname());
      if (cfg == null) {
        return false;
      }
      aliases = cfg.getKeyvalue();
      aliasesSeparados = aliases.split(",");
    } catch (Exception e) {

      // writer.close();
      throw new BancaPrivadaWriterAliasNotFoundException("No se ha podido recuperar los alias de la cfg: " + cfgEnum);
    }

    LOG.trace("[BancaPrivadaWriter :: wirteAliasFromTmct0cfg] Obtenidos los alias " + aliases + " de Tmct0cfg");

    List<String> aliasList = Arrays.asList(aliasesSeparados);
    if (CollectionUtils.isNotEmpty(aliasList)) {

      List<Tmct0bok> boks;
      try {
        boks = bokBo.findForFicheroSPB(Arrays.asList(aliasesSeparados), fechaInicio, fechaFinal,
                                       workDaysOfWeekConfigList, holidaysConfigList);
        LOG.debug("[BancaPrivadaWriter :: wirteAliasFromTmct0cfg] Obtenidos " + boks.size() + " bookings");
      } catch (Exception e) {

        // writer.close();
        throw new BancaPrivadaFileWriterException("No ha podido recuperar la lista de bookings para los alias: "
                                                  + aliasList, e);
      }

      writeBookingsIntoFile(boks, tipoAlias, fechaLote);
    } else {
      LOG.info("[BancaPrivadaWriter :: wirteAliasFromTmct0cfg] No hay alias configurados para {}", cfgEnum);
    }
    return true;
  }

  @Transactional
  private void writeBookingsIntoFile(List<Tmct0bok> boks, TipoAliasBancaPrivada tipoAlias, Date fechaLote) throws BancaPrivadaFileWriterException {
    for (Tmct0bok bok : boks) {
      try {
        writeBookingIntoFile(bok, tipoAlias, fechaLote);
      } catch (Exception e) {
        throw new BancaPrivadaFileWriterException("Incidencia en el booking.id: " + bok.getId(), e);
      }
    }// fin for books
  }

  @Transactional
  private void writeBookingIntoFile(Tmct0bok bok, TipoAliasBancaPrivada tipoAlias, Date fechaLote) throws Exception {
    // Por cada booking obtengo su
    List<String> cdRefBans = new ArrayList<String>();
    Map<String, List<Tmct0alc>> listaAlcsPorCdRefBan = new HashMap<String, List<Tmct0alc>>();
    for (Tmct0alo alo : bok.getTmct0alos()) {
      if (alo.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
        for (Tmct0alc alc : alo.getTmct0alcs()) {
          if (alc.getCdestadoasig() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
            if ((StringUtils.isBlank(alc.getCdenvmor()) || "N".equals(alc.getCdenvmor().trim()))) {

              String cdRefBan = alc.getCdrefban().trim();

              if (cdRefBans.contains(cdRefBan)) {
                List<Tmct0alc> listaAlos = listaAlcsPorCdRefBan.get(cdRefBan);
                listaAlos.add(alc);
              } else {
                List<Tmct0alc> listaAlos = new ArrayList<Tmct0alc>();
                listaAlos.add(alc);
                listaAlcsPorCdRefBan.put(cdRefBan, listaAlos);
                cdRefBans.add(cdRefBan);
              }
            }
          }
        }// fin for alcs
      }
    }// fin for alos

    for (String cdRefBan : cdRefBans) {
      List<Tmct0alc> alcs = listaAlcsPorCdRefBan.get(cdRefBan);
      // Totales para todas las ejecucionesalocaciones de la misma
      // referencia titular
      BigDecimal imtotbruTotal = BigDecimal.ZERO;
      BigDecimal nutitcliTotal = BigDecimal.ZERO; // DIVIDIR POR ESTO
      BigDecimal imcomsimTotal = BigDecimal.ZERO;
      BigDecimal imcanoncontreuTotal = BigDecimal.ZERO;
      BigDecimal imcanoncompensacionTotal = BigDecimal.ZERO;
      BigDecimal imcanonliquidacionTotal = BigDecimal.ZERO;
      BigDecimal cambioMedio = BigDecimal.ZERO;
      List<Record18Bean> lista18s = new ArrayList<Record18Bean>();
      Tmct0ord ordFor19 = null;
      Tmct0alo aloFor19 = null;
      Tmct0eje ejeFor19 = null;
      BigDecimal efectivoEjecuciones = BigDecimal.ZERO;

      for (Tmct0alc alc : alcs) {
        // Dato para el cambio medio

        try {
          List<Tmct0desgloseclitit> desglosesCliTits = new ArrayList<Tmct0desgloseclitit>();
          for (Tmct0desglosecamara ejecucion : alc.getTmct0desglosecamaras()) {
            if (ejecucion.getTmct0estadoByCdestadoasig().getIdestado() > EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId()) {
              desglosesCliTits.addAll(ejecucion.getTmct0desgloseclitits());
            }
          }

          // delLineAgg.setFieldExtractor(fieldExtractor18);
          for (Tmct0desgloseclitit desgloseclitit : desglosesCliTits) {
            try {
              if (ordFor19 == null) {
                ordFor19 = desgloseclitit.getTmct0desglosecamara().getTmct0alc().getTmct0alo().getTmct0bok()
                                         .getTmct0ord();
                aloFor19 = desgloseclitit.getTmct0desglosecamara().getTmct0alc().getTmct0alo();
                ejeFor19 = ejeBo.findById(new Tmct0ejeId(aloFor19.getId().getNuorden(), aloFor19.getId().getNbooking(),
                                                         desgloseclitit.getNuejecuc()));
              }

              LOG.trace("[BancaPrivadaWriter :: writeBookingsIntoFile] Rellenando registro 18 para desgloseclitit : "
                        + desgloseclitit.getNudesglose());
              Tmct0eje eje = ejeBo.findById(new Tmct0ejeId(aloFor19.getId().getNuorden(), aloFor19.getId()
                                                                                                  .getNbooking(),
                                                           desgloseclitit.getNuejecuc()));
              Tmct0ord ord = desgloseclitit.getTmct0desglosecamara().getTmct0alc().getTmct0alo().getTmct0bok()
                                           .getTmct0ord();
              Tmct0alo alo = desgloseclitit.getTmct0desglosecamara().getTmct0alc().getTmct0alo();

              String cdRefExtSv = alo.getRefbanif();

              if (StringUtils.isBlank(cdRefExtSv)) {
                if (tipoAlias == TipoAliasBancaPrivada.ALIAS_CTM) {
                  cdRefExtSv = alo.getId().getNucnfclt().trim().substring(0, 11);
                } else {
                  cdRefExtSv = alc.getAcctatcle();
                }
              }
              Record18Bean r18bean = new Record18Bean(ord, eje, alo, cdRefBan, cdRefExtSv,
                                                      desgloseclitit.getImtitulos());
              lista18s.add(r18bean);
            } catch (Exception e) {
              throw new BancaPrivadaFileWriterException(
                                                        "No se ha podido crear el registro 18 para el desgloseclitit.id: "
                                                            + desgloseclitit.getNudesglose(), e);
            }
          }
        } catch (Exception e) {

          throw new BancaPrivadaFileWriterException("No se ha podido crear el registro 18 para el alc.id: "
                                                    + alc.getId(), e);

        }
        efectivoEjecuciones = efectivoEjecuciones.add(alc.getImefeagr());
        imtotbruTotal = imtotbruTotal.add(alc.getImtotbru());
        nutitcliTotal = nutitcliTotal.add(alc.getNutitliq());
        imcomsimTotal = imcomsimTotal.add(alc.getImcomisn());
        imcanoncontreuTotal = imcanoncontreuTotal.add(alc.getImcanoncontreu());
        imcanoncompensacionTotal = imcanoncompensacionTotal.add(alc.getImcanoncompeu());
        imcanonliquidacionTotal = imcanonliquidacionTotal.add(alc.getImcanonliqeu());
      }

      if (nutitcliTotal.compareTo(BigDecimal.ZERO) != 0) {
        cambioMedio = efectivoEjecuciones.divide(nutitcliTotal, 15, RoundingMode.HALF_UP);
      }
      LOG.trace("[BancaPrivadaWriter :: writeBookingsIntoFile] Rellenando registro 19 para el cdRefBan : " + cdRefBan);
      Record19Bean record19 = null;

      try {
        if (ordFor19 != null && aloFor19 != null && ejeFor19 != null) {
          record19 = new Record19Bean(fechaLote, ordFor19, aloFor19, ejeFor19, imtotbruTotal, nutitcliTotal,
                                      imcomsimTotal, imcanoncontreuTotal, imcanoncompensacionTotal,
                                      imcanonliquidacionTotal, cdRefBan, cambioMedio);
        }
      } catch (Exception e1) {
        throw new BancaPrivadaFileWriterException("No se ha podido crear el registro 19 para el cdRefBan: " + cdRefBan
                                                  + " booking.id: " + bok.getId(), e1);
      }
      if (CollectionUtils.isNotEmpty(lista18s) && record19 != null) {
        writeIntoFile(lista18s, record19);

        for (Tmct0alc alc : listaAlcsPorCdRefBan.get(cdRefBan)) {
          try {
            updateStates(alc);
          } catch (Exception e) {
            throw new BancaPrivadaFileWriterException(
                                                      "No se ha podido actualizar el estado del alc.id: " + alc.getId(),
                                                      e);

          }
          logBo.insertarRegistro(alc, new Date(), new Date(), "", alc.getCdestadoasig(), "Ok-Enviado 18-19");
        }
      }
    }// fin for alcs
  }

  public boolean closeAndRenameFile() {
    LOG.debug("[BancaPrivadaWriter :: closeAndRenameFile] Init (Folder: " + folderName + ")");
    boolean success = true;
    try {
      writer.close();
      LOG.trace("[BancaPrivadaWriter :: closeAndRenameFile] Fichero cerrado");
    } catch (Exception e) {
      LOG.warn("[BancaPrivadaWriter :: closeAndRenameFile] Excepción cerrando el fichero " + e.getMessage());
      return false;
    }
    try {
      File fileOrigen = new File(folderName, fileName);
      String finalFileName = getNewFinalFileName();

      // Comprobamos si el fichero esta vacio
      if (fileOrigen.exists()) {
        File fileDestino = new File(folderName, finalFileName);
        if (fileDestino.exists()) {
          Date date = new Date();
          File fileDestinoRepetido = new File(folderName, finalFileName + date.getTime());
          success = fileOrigen.renameTo(fileDestinoRepetido);
        } else {
          success = fileOrigen.renameTo(fileDestino);
        }
        LOG.debug("[BancaPrivadaWriter :: closeAndRenameFile] Fichero renombrado");
      } else {
        LOG.info("[BancaPrivadaWriter :: closeAndRenameFile] No se ha escrito nada.");
      }
    } catch (Exception e) {
      LOG.warn("[BancaPrivadaWriter :: closeAndRenameFile] Incidencia renombrando fichero creado " + e.getMessage());
      return false;
    }
    LOG.debug("[BancaPrivadaWriter :: closeAndRenameFile] Fin (Folder: " + folderName + ")");
    return success;

  }

  @Transactional
  private void updateStates(Tmct0alc alc) {

    if (StringUtils.isBlank(alc.getCdenvmor()) || "N".equals(alc.getCdenvmor().trim())) {
      alc.setCdenvmor("S");
      alcBo.save(alc);
    }

  }

  private void writeIntoFile(List<Record18Bean> lista18s, Record19Bean record19) throws Exception {
    delLineAgg.setFieldExtractor(fieldExtractor18);
    writer.write(lista18s);
    delLineAgg.setFieldExtractor(fieldExtractor19);
    List<Record19Bean> lista19 = new ArrayList<Record19Bean>();
    lista19.add(record19);
    writer.write(lista19);
  }

  private String getNewFinalFileName() {
    return finalFileNamePrefix + FormatDataUtils.convertDateToString(new Date()).substring(2) + finalFileNameSufix;
  }

}
