package sibbac.business.wrappers.database.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sibbac.business.wrappers.database.model.Tmct0Selects;

@Repository
public interface Tmct0SelectsDao extends JpaRepository< Tmct0Selects, Long > {

	@Query( "SELECT tsel FROM Tmct0Selects tsel WHERE catalogo =:catalogo" )
	public List<Tmct0Selects> findByCatalogo( @Param( "catalogo" ) String catalogo);
	
}
