package sibbac.business.wrappers.database.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0movimientoeccFidessa;

@Repository
public interface Tmct0movimientoeccFidessaDao extends JpaRepository<Tmct0movimientoeccFidessa, Long> {

  List<Tmct0movimientoeccFidessa> findAllByCdrefmovimiento(String cdrefmovimiento);

  List<Tmct0movimientoeccFidessa> findAllByCdrefmovimientoecc(String cdrefmovimiento);

  List<Tmct0movimientoeccFidessa> findAllByFcontratacionAndCdtipomovAndCdestadomov(Date fcontratacion,
      String cdtipomov, String cdestadomov);

  List<Tmct0movimientoeccFidessa> findAllByCdrefmovimientoAndFliquidacion(String cdrefmovimiento, Date fliquidacion);

  List<Tmct0movimientoeccFidessa> findAllByCdrefmovimientoeccAndFliquidacion(String cdrefmovimiento, Date fliquidacion);

  List<Tmct0movimientoeccFidessa> findAllByNuoperacionprevAndCuentaCompensacionDestino(String nuoperacionprev,
      String cuentaCompensacionDestino);

  List<Tmct0movimientoeccFidessa> findAllByNuoperacionprevAndMiembroCompensadorDestinoAndCdRefAsignacion(
      String nuoperacionprev, String miembroCompensadorDestino, String cdRefAsignacion);

  List<Tmct0movimientoeccFidessa> findAllByCdoperacionecc(String cdoperacionecc);

  List<Tmct0movimientoeccFidessa> findAllByNuoperacionprev(String Nuoperacionprev);

  @Query("select m.idMovimientoFidessa from Tmct0movimientoeccFidessa m where m.cdoperacionecc = :cdoperacionecc")
  List<Long> findAllIdsByCdoperacionecc(@Param("cdoperacionecc") String cdoperacionecc);

  @Query("SELECT m.fliquidacion FROM Tmct0movimientoeccFidessa m WHERE m.imtitulosPendientesAsignar > 0 "
      + " GROUP BY m.fliquidacion ")
  List<Date> findAllFliquidacionByAndTitulosDisponibles();

  @Query(value = "SELECT DISTINCT MN.ID_MOVIMIENTO_FIDESSA FROM TMCT0MOVIMIENTOECC_FIDESSA MN INNER JOIN TMCT0DESGLOSECLITIT DT ON MN.CDOPERACIONECC=DT.CDOPERACIONECC "
      + " INNER JOIN TMCT0DESGLOSECAMARA DC ON DT.IDDESGLOSECAMARA = DT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0ORD ORD ON DC.NUORDEN = ORD.NUORDEN "
      + " WHERE MN.FLIQUIDACION = :fliquidacion AND MN.IMTITULOS_PENDIENTES_ASIGNAR > 0 "
      + " AND DC.CDESTADOASIG > 0 AND ORD.ISSCRDIV = :isscrdiv AND MN.CDOPERACIONECC <> '' ", nativeQuery = true)
  List<Long> findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(@Param("fliquidacion") Date fliquidacion,
      @Param("isscrdiv") char isscrdiv);

  @Query("SELECT distinct mn FROM Tmct0desgloseclitit o, Tmct0movimientoeccFidessa mn WHERE "
      + " mn.nuoperacionprev = o.cdoperacionecc AND o.tmct0desglosecamara.tmct0alc.id = :alcId "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 AND mn.imtitulosPendientesAsignar > 0 ")
  List<Tmct0movimientoeccFidessa> findAllByTmct0alcIdAndTitulosDisponibles(@Param("alcId") Tmct0alcId alcId);

  // @Query("select new sibbac.business.wrappers.database.dto.asignaciones.TitulosByInformacionAsignacionDTO("
  // +
  // "sum(mf.imtitulosPendientesAsignar), mf.cuentaCompensacionDestino, mf.miembroCompensadorDestino, mf.cdRefAsignacion) "
  // + "from Tmct0movimientoeccFidessa mf, Tmct0desgloseclitit dct "
  // +
  // "where mf.nuoperacionprev = dct.nuoperacionprev  and dct.tmct0desglosecamara.iddesglosecamara = :iddesglosecamara and mf.cdestadomov in (:estadosecc) "
  // +
  // " and not exits (from Tmct0movimientooperacionnuevaecc as mn where dct.nudesglose = mn.nudesglose and mn.tmct0movimientoecc.cdestadomov in (:estadosecc) ) "
  // +
  // "group by mf.miembroCompensadorDestino, mf.cdRefAsignacion, mf.cuentaCompensacionDestino ")
  // List<TitulosByInformacionAsignacionDTO>
  // findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByIddesglosecamaraAndInCestadomov(
  // @Param("iddesglosecamara") long iddesglosecamara, @Param("estadosecc")
  // List<String> estadosEccActivos);
  //
  // @Query("select new sibbac.business.wrappers.database.dto.asignaciones.TitulosByInformacionAsignacionDTO("
  // +
  // "sum(mf.imtitulosPendientesAsignar), mf.cuentaCompensacionDestino, mf.miembroCompensadorDestino, mf.cdRefAsignacion) "
  // + "from Tmct0movimientoeccFidessa mf, Tmct0desgloseclitit dct "
  // +
  // "where mf.nuoperacionprev = dct.nuoperacionprev  and dct.tmct0desglosecamara.tmct0alc.id = :alcId and mf.cdestadomov in (:estadosecc) "
  // +
  // " and not exits (from Tmct0movimientooperacionnuevaecc as mn where dct.nudesglose = mn.nudesglose and mn.tmct0movimientoecc.cdestadomov in (:estadosecc) ) "
  // + " and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
  // +
  // "group by mf.miembroCompensadorDestino, mf.cdRefAsignacion, mf.cuentaCompensacionDestino ")
  // List<TitulosByInformacionAsignacionDTO>
  // findAllSumTitulosPteAsignarGroupByCompensadorAndCdRefAsignacionAndCuentaCompensacionDestinoByAlcIdAndInCestadomov(
  // @Param("alcId") Tmct0alcId alcId, @Param("estadosecc") List<String>
  // estadosEccActivos);
  //
  // @Query("select mf "
  // + "from Tmct0movimientoeccFidessa mf, Tmct0desgloseclitit dct "
  // +
  // "where mf.nuoperacionprev = dct.nuoperacionprev and dct.tmct0desglosecamara.tmct0alc.id = :alcId and mf.cdestadomov in (:estadosecc)  "
  // + "and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  // List<Tmct0movimientoeccFidessa>
  // findAllByAlcIdAndInCestadomov(@Param("alcId") Tmct0alcId alcId,
  // @Param("estadosecc") List<String> estadosEccActivos);

  // List<Tmct0movimientoeccFidessa>
  // findAllByCdrefmovimientoeccAndFliquidacionAndCdoperacionecc(String
  // cdrefmovimiento,
  // Date fliquidacion, String cdoperacionecc);
  //
  // List<Tmct0movimientoeccFidessa>
  // findAllByCdrefmovimientoeccAndFliquidacionAndNuoperacionprev(String
  // cdrefmovimiento,
  // Date fliquidacion, String nuoperacionprev);

}
