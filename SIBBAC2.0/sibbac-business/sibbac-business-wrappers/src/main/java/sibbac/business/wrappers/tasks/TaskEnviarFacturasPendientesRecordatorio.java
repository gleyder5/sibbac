package sibbac.business.wrappers.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.RecordatorioFacturaPendienteBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_ENVIAR_RECORDATORIO_FACTURAS_PENDIENTES, interval = 1, intervalUnit = IntervalUnit.DAY)
public class TaskEnviarFacturasPendientesRecordatorio extends WrapperTaskConcurrencyPrevent {

    @Autowired
    RecordatorioFacturaPendienteBo recordatorioFacturaPendienteBo;

    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskEnviarFacturasPendientesRecordatorio.class);
    @Autowired
    private Tmct0menBo menBo;

    @Override
    public void executeTask() {

	final String prefix = "[TaskEnviarFacturasPendientesRecordatorio::execute]";

	LOG.debug(prefix
		+ " :: execute ==> Detectando si se está ejecutando la tarea ....");
	final List<String> taskMessages = new ArrayList<String>();
	try {
	    executeTask(prefix, taskMessages);
	    LOG.debug(prefix
		    + " :: execute Se acabó la tarea y se dejó en estado disponible");
	} catch (Exception e) {
	    LOG.debug(prefix + "ERROR: " + e.getMessage());
	    taskMessages.add("ERROR: " + e.getMessage());
	    
	}finally {
	    if (CollectionUtils.isNotEmpty(taskMessages)) {
		for (final String taskMessage : taskMessages) {
		    blog(prefix + "[" + taskMessage + "]");
		}
	    }
	    LOG.trace(prefix + "[Finished...]");
	}
    }

    private void executeTask(final String prefix, final List<String> taskMessages) throws Exception {
	
	
	    LOG.debug(prefix + "[Started...]");
	    recordatorioFacturaPendienteBo.enviarMailRecordatorio(this.jobPk,
		    taskMessages);
	    LOG.trace(prefix + "Examinando los mensajes de vuelta, si hay...");
	    taskMessages.add("Finalizado el envio de recordatorios");
	
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_FACTURAS_ENVIO_RECORDATORIO;
    }
}
