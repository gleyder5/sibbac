package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0pkc;
import sibbac.business.wrappers.database.model.Tmct0pkcId;

@Repository
public interface Tmct0pkcDao extends JpaRepository<Tmct0pkc, Tmct0pkcId> {

}
