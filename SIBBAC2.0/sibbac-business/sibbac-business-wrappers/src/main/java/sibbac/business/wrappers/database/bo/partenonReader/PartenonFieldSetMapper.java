package sibbac.business.wrappers.database.bo.partenonReader;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.OperacionEspecialFieldSetMapper;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.RecordBean.TipoDocumento;
import sibbac.business.wrappers.database.model.RecordBean.TipoTitular;

public class PartenonFieldSetMapper<T extends PartenonRecordBean> extends
    OperacionEspecialFieldSetMapper<OperacionEspecialRecordBean> {

  public enum PartenonFields {
    TIPOREG("tipoRegistro", 2), CODEMPR("codeMpr", 4), NUMORDEN("numOrden", 10), IDSENC("idSenc", 2), NIFBIC("nifbic",
        11), NOMAPELL("nomapell", 40), DOMICILI("domicili", 40), DISTRITO("distrito", 5), POBLACIO("poblacion", 20), PROVINCIA(
        "provincia", 20), PAIS("pais", 20), NACLIDAD("nacionalidad", 3), TIPTITU("tiptitu", 1), PARTICIP("particip", 5), BOLSA(
        "bolsa", 2), PAISRE("paisResidencia", 3), CODBIC("codigoBic", 11), TIPDOC("tipoDocumento", 1), TIPPER(
        "tipoPersona", 1), INDNAC("indNac", 1), CCV("ccv", 20), FILLER("filler", 6);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private PartenonFields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      PartenonFields[] fields = PartenonFields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getPartenonLengths() {
      PartenonFields[] fields = PartenonFields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public PartenonRecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    PartenonRecordBean partenonBean = new PartenonRecordBean();
    partenonBean.setCodeMpr(fieldSet.readString(PartenonFields.CODEMPR.text).trim());
    partenonBean.setNumOrden(fieldSet.readString(PartenonFields.NUMORDEN.text).trim());
    partenonBean.setIdSenc(fieldSet.readBigDecimal(PartenonFields.IDSENC.text));

    partenonBean.setTipoRegistro(fieldSet.readString(PartenonFields.TIPOREG.text));

    try {
      String nomApell = fieldSet.readString(PartenonFields.NOMAPELL.text).trim();
      partenonBean.setNomapell(nomApell);
      if (nomApell.indexOf("*") == -1) {
        int indicePrimerSubstring = 50;
        if (nomApell.length() < 50) {
          indicePrimerSubstring = nomApell.length();
        }
        partenonBean.setNbclient(" ");
        partenonBean.setNbclient1(nomApell.substring(0, indicePrimerSubstring));
        if (nomApell.length() > 50) {
          int indiceSegundoSubstring = 30;
          if (nomApell.length() < 80) {
            indiceSegundoSubstring = nomApell.length() - 50;
          }
          partenonBean.setNbclient2(nomApell.substring(50, 50 + indiceSegundoSubstring));
        }
        else {
          partenonBean.setNbclient2(" ");
        }
      }
      else {
        String[] names = nomApell.split("\\*");
        partenonBean.setNomapell(nomApell);
        partenonBean.setNbclient(names[0].trim());
        partenonBean.setNbclient1(names[1].trim());
        if (names.length > 2) {
          partenonBean.setNbclient2(names[2].trim());
        }
        else {
          partenonBean.setNbclient2("");
        }
      }
    }
    catch (Exception e) {
      partenonBean.setNomapell(fieldSet.readString(PartenonFields.NOMAPELL.text).trim());
    }
    partenonBean.setDomicili(fieldSet.readString(PartenonFields.DOMICILI.text).trim());
    partenonBean.setDistrito(fieldSet.readString(PartenonFields.DISTRITO.text).trim());
    partenonBean.setPoblacion(fieldSet.readString(PartenonFields.POBLACIO.text).trim());
    partenonBean.setProvincia(fieldSet.readString(PartenonFields.PROVINCIA.text).trim());
    partenonBean.setPais(fieldSet.readString(PartenonFields.PAIS.text).trim());
    partenonBean.setNacionalidad(fieldSet.readString(PartenonFields.NACLIDAD.text).trim());
    if (StringUtils.isNotEmpty(fieldSet.readString(PartenonFields.TIPTITU.text))) {
      partenonBean.setTiptitu(fieldSet.readChar(PartenonFields.TIPTITU.text));
    }
    // Division porque tiene 2 cifras decimales
    partenonBean.setParticip(fieldSet.readBigDecimal(PartenonFields.PARTICIP.text).divide(new BigDecimal(100)));

    partenonBean.setBolsa(fieldSet.readString(PartenonFields.BOLSA.text).trim());
    partenonBean.setPaisResidencia(fieldSet.readString(PartenonFields.PAISRE.text).trim());

    if (StringUtils.isNotEmpty(fieldSet.readString(PartenonFields.TIPDOC.text))) {
      partenonBean.setTipoDocumento(fieldSet.readChar(PartenonFields.TIPDOC.text));
    }
    if (StringUtils.isNotEmpty(fieldSet.readString(PartenonFields.TIPPER.text))) {
      partenonBean.setTipoPersona(fieldSet.readChar(PartenonFields.TIPPER.text));
    }
    if (StringUtils.isNotEmpty(fieldSet.readString(PartenonFields.INDNAC.text))) {
      partenonBean.setIndNac(fieldSet.readChar(PartenonFields.INDNAC.text));
    }
    partenonBean.setCcv(fieldSet.readString(PartenonFields.CCV.text).trim());
    partenonBean.setFiller(fieldSet.readString(PartenonFields.FILLER.text).trim());
    String bic = fieldSet.readString(PartenonFields.CODBIC.text).trim();

    if (partenonBean.getTipoDocumento() != null
        && partenonBean.getTipoDocumento() == TipoDocumento.BIC.getTipoDocumento() && StringUtils.isNotBlank(bic)) {
      partenonBean.setNifbic(bic);
    }
    else {
      partenonBean.setNifbic(fieldSet.readString(PartenonFields.NIFBIC.text).trim());
    }

    return partenonBean;
  }
}
