package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0pke;
import sibbac.business.wrappers.database.model.Tmct0pkeId;

@Repository
public interface Tmct0pkeDao extends JpaRepository<Tmct0pke, Tmct0pkeId> {

}
