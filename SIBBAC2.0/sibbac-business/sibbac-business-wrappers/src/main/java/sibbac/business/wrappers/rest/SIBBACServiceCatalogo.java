package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tval0vpbBo;
import sibbac.business.wrappers.common.SelectDTO;
import sibbac.business.wrappers.database.bo.TipoDeDocumentoBo;
import sibbac.business.wrappers.database.dao.Tmct0SelectsDao;
import sibbac.business.wrappers.database.dao.Tmct0paisesDao;
import sibbac.business.wrappers.database.dao.Tmct0provinciasDao;
import sibbac.business.wrappers.database.model.Tmct0Selects;
import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0provincias;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bsnvasql.model.Tval0vpb;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceCatalogo implements SIBBACServiceBean {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceAlias.class);

	@Autowired
	private TipoDeDocumentoBo tipoDocumentoBo;

	@Autowired
	private Tmct0paisesDao tmct0paisesDao;

	@Autowired
	private Tmct0provinciasDao tmct0provinciasDao;
	
	@Autowired
	private Tmct0SelectsDao tmct0SelectsDao;
	  
	@Autowired
	private Tval0vpbBo tpdomiciBo; 
	

	public SIBBACServiceCatalogo() {
	}

	@Override
	public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {
		LOG.debug("[SIBBACServiceCatalogo::process] " + "Processing a web request...");

		// prepara webResponse
		WebResponse webResponse = new WebResponse();
		Map<String, Object> salida = new HashMap<String, Object>();

		// obtiene comandos(paths)
		final String action = webRequest.getAction();
		LOG.debug("[SIBBACServiceAlias::process] " + "+ Command.: [{}]", action);

		switch (action) {
		case "tiposDeDocumento":
			salida.put("tiposDeDocumento", this.getTiposDeDocumento());
			break;
		case "tiposDeResidencia":
			salida.put("tiposDeResidencia", this.getTiposDeResidencia());
			break;
		case "tiposDeCliente":
			salida.put("tiposDeCliente", this.getTiposDeCliente());
			break;
		case "naturalezaDeCliente":
			salida.put("naturalezaDeCliente", this.getNaturalezaDeCliente());
			break;
		case "estadoDeCliente":
			salida.put("estadoDeCliente", this.getEstadosDeCliente());
			break;
		case "tiposDeNacionalidad":
			salida.put("tiposDeNacionalidad", this.getTiposDeNacionalidad());
			break;
		case "nacionalidades":
			salida.put("nacionalidades", this.getNacionalidades());
			break;
		case "nacionalidadesCodigoPostal":
			salida.put("nacionalidadesCodigoPostal", this.getNacionalidadesCodPostal());
			break;
		case "provincias":
			salida.put("provincias", this.getProvincias());
			break;
		case "categorizacionesCliente":
			salida.put("categorizacionesCliente", this.getCategorizacionesCliente());
			break;
		case "siNo":
			salida.put("siNo", this.getSiNo());
			break;
		case "motivosPBC":
			salida.put("motivosPBC", this.getMotivosPBC());
			break;
		case "direcciones":
			salida.put("direcciones", this.getDirecciones());
			break;
		case "testDeConveniencia":
			salida.put("testDeConveniencia", this.getTestDeConveniencia());
			break;
		case "testDeIdoneidad":
			salida.put("testDeIdoneidad", this.getTestDeIdoneidad());
			break;
		case "categorias":
			salida.put("categorias", this.getCategorias());
			break;
		case "ambitosCorporativos":
			salida.put("ambitosCorporativos", this.getAmbitosCorporativos());
			break;
		case "categoriasFiscal":
			salida.put("categoriasFiscal", this.getCategoriasFiscal());
			break;
		case "tiposDomicilio":
			salida.put("tiposDomicilio", this.getTiposDomicilio());
			break;
		default:
			salida.put("tiposDeDocumento", this.getTiposDeDocumento());
			salida.put("tiposDeResidencia", this.getTiposDeResidencia());
			salida.put("tiposDeCliente", this.getTiposDeCliente());
			salida.put("naturalezaDeCliente", this.getNaturalezaDeCliente());
			salida.put("estadosDeCliente", this.getEstadosDeCliente());
			salida.put("tiposDeNacionalidad", this.getTiposDeNacionalidad());
			salida.put("nacionalidades", this.getNacionalidades());
			salida.put("nacionalidadesCodigoPostal", this.getNacionalidadesCodPostal());
			salida.put("categorizacionesCliente", this.getCategorizacionesCliente());
			salida.put("siNo", this.getSiNo());
			salida.put("motivosPBC", this.getMotivosPBC());
			salida.put("direcciones", this.getDirecciones());
			salida.put("testDeConveniencia", this.getTestDeConveniencia());
			salida.put("testDeIdoneidad", this.getTestDeIdoneidad());
			salida.put("categorias", this.getCategorias());
			salida.put("ambitosCorporativos", this.getAmbitosCorporativos());
			salida.put("categoriasFiscal", this.getCategoriasFiscal());
			salida.put("provincias", this.getProvincias());
			salida.put("tiposDomicilio", this.getTiposDomicilio());
			salida.put("riesgosPBC", this.getRiesgosPBC());
		}

		webResponse.setResultados(salida);
		return webResponse;
	}

	private List<SelectDTO> getTiposDeDocumento() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("tiposDeDocumento");
	}

	private List<SelectDTO> getTiposDeResidencia() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("tiposDeResidencia");
	}

	private List<SelectDTO> getTiposDeCliente() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("tiposDeCliente");
	}

	private List<SelectDTO> getNaturalezaDeCliente() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("naturalezaDeCliente");
	}

	private List<SelectDTO> getEstadosDeCliente() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("estadosDeCliente");
	}

	private List<SelectDTO> getTiposDeNacionalidad() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("tiposDeNacionalidad");
	}

	private List<SelectDTO> getNacionalidades() throws SIBBACBusinessException {
		List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
		for (Tmct0paises pais : tmct0paisesDao.findAllActivos()) {
			catalogo.add(new SelectDTO(String.valueOf(pais.getCdhacienda()).trim(), pais.getNbpais()));
		}
		return catalogo;
	}

	private List<SelectDTO> getNacionalidadesCodPostal() throws SIBBACBusinessException {
		List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
		for (Tmct0paises pais : tmct0paisesDao.findAllActivos()) {
			catalogo.add(new SelectDTO(pais.getNbpais(), pais.getCdhacienda()));
		}
		return catalogo;
	}

	private List<SelectDTO> getProvincias() throws SIBBACBusinessException {
		List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
		for (Tmct0provincias p : tmct0provinciasDao.findAllActivas()) {
			catalogo.add(new SelectDTO(p.getCdcodigo(), p.getNbprovin()));
		}
		return catalogo;
	}

	private List<SelectDTO> getTiposDomicilio() throws SIBBACBusinessException {
		List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
		for (Tval0vpb tipoDomicilio : tpdomiciBo.findAll()) {
			catalogo.add(new SelectDTO(tipoDomicilio.getCdviapub(),
					"(" + tipoDomicilio.getCdviapub() + ") " + tipoDomicilio.getNbviapub()));
		}
		return catalogo;
	}

	private List<SelectDTO> getCategorizacionesCliente() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("categorizacionesCliente");
	}

	private List<SelectDTO> getSiNo() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("siNo");
	}

  private List<SelectDTO> getMotivosPBC() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("motivosPBC");
  }
  
  private List<SelectDTO> getRiesgosPBC() throws SIBBACBusinessException {
    return this.getListSelectDTOByCatalogo("riesgosPBC");
  }

	private List<SelectDTO> getDirecciones() throws SIBBACBusinessException {
		List<SelectDTO> catalogo = new ArrayList<SelectDTO>();
		return catalogo;
	}

	private List<SelectDTO> getTestDeConveniencia() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("testDeConveniencia");
	}

	private List<SelectDTO> getTestDeIdoneidad() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("testDeIdoneidad");
	}

	private List<SelectDTO> getCategorias() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("categorias");
	}

	private List<SelectDTO> getAmbitosCorporativos() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("ambitosCorporativos");
	}

	private List<SelectDTO> getCategoriasFiscal() throws SIBBACBusinessException {
		return this.getListSelectDTOByCatalogo("categoriasFiscal");
	}

	@Override
	public List<String> getAvailableCommands() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getFields() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 *	Devuelve DTO con clave-valor de select populado.
	 *	@param catalogo catalogo
	 *	@return List<SelectDTO> List<SelectDTO>   
	 */
	private List<SelectDTO> getListSelectDTOByCatalogo(String catalogo) {
		List<SelectDTO> catalogoList = new ArrayList<SelectDTO>();
		List<Tmct0Selects> selects = this.tmct0SelectsDao.findByCatalogo(catalogo);
		if (CollectionUtils.isNotEmpty(selects)) {
			for (Tmct0Selects sel : selects) {
				catalogoList.add(new SelectDTO(String.valueOf(sel.getClave()).trim(), String.valueOf(sel.getValor()).trim()));
			}
		}
		return catalogoList;
	}

}
