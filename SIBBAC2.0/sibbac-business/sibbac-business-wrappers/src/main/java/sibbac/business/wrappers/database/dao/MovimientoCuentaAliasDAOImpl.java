package sibbac.business.wrappers.database.dao;


import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;


@Component
public class MovimientoCuentaAliasDAOImpl {

	@PersistenceContext
	private EntityManager		em;
	private static final String	TAG	= MovimientoCuentaAliasDAOImpl.class.getName();
	private static final Logger	LOG	= LoggerFactory.getLogger( TAG );

	@SuppressWarnings( "unchecked" )
	public List< MovimientoCuentaAliasData > findAll( Map< String, Serializable > filtros ) {
		List< MovimientoCuentaAliasData > resultList = new LinkedList< MovimientoCuentaAliasData >();
		/*
		 * String select =
		 * "SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(m.cdcodigocuentaliq, m.cdaliass, a.descrali, m.settlementdate, m.isin, "
		 * + "v.descripcion, m.mercado, m.cdoperacion, m.signoanotacion, m.tradedate, m.titulos, m.imefectivo, a.fhinicio, a.fhfinal) ";
		 */
		String select = "SELECT new sibbac.business.wrappers.database.data.MovimientoCuentaAliasData(m.cdcodigocuentaliq, m.cdaliass, m.settlementdate, m.isin, "
				+ "v.descripcion, m.mercado, m.cdoperacion, m.signoanotacion, m.tradedate, m.titulos, m.imefectivo) ";
		String from = "FROM Tmct0MovimientoCuentaAlias m, Tmct0Valores v ";
		/*
		 * StringBuilder where = new StringBuilder(
		 * "WHERE m.isin = v.codigoDeValor AND m.cdaliass = a.id.cdaliass ");
		 */
		StringBuilder where = new StringBuilder( "WHERE m.isin = v.codigoDeValor " );
		// String groupBy =
		// "GROUP BY m.settlementdate, m.isin, v.descripcion, m.mercado, m.cdoperacion, m.signoanotacion, m.tradedate, m.titulos, m.imefectivo, m.cdcodigocuentaliq ";
		// String having = "AND max(a.id.numsec) = a.id.numsec ";
		String sort = "ORDER BY m.isin, m.cdaliass, m.cdcodigocuentaliq, m.id";
		// fechas
		String fcontratacionBtw = "AND m.tradedate BETWEEN :fcontratacionDe AND :fcontratacionA ";
		String fliquidacionBtw = "AND m.settlementdate BETWEEN :fliquidacionDe AND :fliquidacionA ";
		// Fecha liquidacion
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyyMMdd" );
		String queryFechaAlias = "";
		BigDecimal lFliquidacion = BigDecimal.ZERO;
		if ( filtros.get( "fliquidacionDe" ) != null ) {
			Date fLiquidacion = ( Date ) filtros.get( "fliquidacionDe" );
			String strFLiq = sdf.format( fLiquidacion );

			try {
				lFliquidacion = BigDecimal.valueOf( Long.valueOf( strFLiq ) );
				queryFechaAlias = "AND a.fhinicio >= " + lFliquidacion + " AND a.fhfinal <= " + lFliquidacion + ") ";
			} catch ( NumberFormatException ex ) {
				LOG.error( ex.getMessage(), ex );
			}
			if ( !queryFechaAlias.equals( "" ) ) {
				// where.append(queryFechaAlias);
			}
		}
		// query string filan
		StringBuilder querySB = new StringBuilder( select ).append( from );
		// se construye la query segun los filtros
		for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
			if ( entry.getValue() != null
					&& !"".equals( entry.getKey() )
					&& ( !entry.getKey().equals( "fcontratacionDe" ) && !entry.getKey().equals( "fcontratacionA" )
							&& !entry.getKey().equals( "fliquidacionDe" ) && !entry.getKey().equals( "fliquidacionA" ) ) ) {
				where.append( "AND m." ).append( entry.getKey() ).append( "=:" ).append( entry.getKey() ).append( " " );
			} else if ( entry.getKey().equals( "fcontratacionDe" ) && entry.getValue() != null ) {
				// si vienen las dos fechas se hace un between
				if ( checkIfKeyExist( "fcontratacionA", filtros ) ) {
					where.append( fcontratacionBtw );
				} else {
					where.append( "AND m.tradedate >=:fcontratacionDe " );
				}
			} else if ( entry.getKey().equals( "fliquidacionDe" ) && entry.getValue() != null ) {
				// si vienen las dos fechas se hace un between
				if ( checkIfKeyExist( "fliquidacionA", filtros ) ) {
					where.append( fliquidacionBtw );
				} else {
					where.append( "AND m.settlementdate >=:fliquidacionDe " );
				}
			}
		}
		if ( !"".equals( queryFechaAlias ) ) {
			// where.append(having)
			// .append(groupBy);
		}
		querySB.append( where ).append( sort );
		Query query = null;
		try {
			query = em.createQuery( querySB.toString() );
			for ( Map.Entry< String, Serializable > entry : filtros.entrySet() ) {
				query.setParameter( entry.getKey(), entry.getValue() );
			}
			resultList = query.getResultList();
		} catch ( PersistenceException ex ) {
			LOG.error( ex.getMessage(), ex );
		} catch ( IllegalArgumentException ex ) {
			LOG.error( ex.getMessage(), ex );
		} catch ( NullPointerException ex ) {
			LOG.error( ex.getMessage(), ex );
		}
		if ( lFliquidacion != BigDecimal.ZERO ) {
			eliminarRegistrosRepetidos( resultList, lFliquidacion );
		}
		return resultList;
	}

	@SuppressWarnings( "unchecked" )
	private List< MovimientoCuentaAliasData > eliminarRegistrosRepetidos( List< MovimientoCuentaAliasData > resultList, BigDecimal fliq ) {
		// List<MovimientoCuentaAliasData>copy = new CopyOnWriteArrayList<MovimientoCuentaAliasData>();
		// copy.addAll(resultList);
		StringBuilder select = new StringBuilder( "SELECT a.descrali, a.fhinicio, a.fhfinal FROM Tmct0ali a " );
		StringBuilder where = new StringBuilder( "WHERE CAST(a.fhinicio as integer) <= " + fliq + " AND CAST(a.fhfinal as integer) >= "
				+ fliq + " AND a.id.cdaliass =:alias " );
		StringBuilder sbQuery = new StringBuilder( select ).append( where );
		// si no se ha encontrado ninguna coincidencia no se puede eliminar
		// de la lista
		// boolean encontrado = false;
		for ( int i = 0; i < resultList.size(); i++ ) {
			MovimientoCuentaAliasData first = resultList.get( i );
			String alias = ( first.getCdalias() != null ) ? first.getCdalias().trim() : "";
			if ( alias == null || "".equals( alias ) ) {
				continue;
			}
			Query query = null;
			Object[] result = null;
			try {
				query = em.createQuery( sbQuery.toString() );
				query.setParameter( "alias", alias );
				List< Object[] > lista = query.getResultList();
				if ( lista != null && lista.size() > 0 ) {
					result = lista.get( 0 );
				}
			} catch ( IllegalArgumentException | NullPointerException | PersistenceException ex ) {
				LOG.error( ex.getMessage(), ex );
				continue;
			}
			if ( result != null ) {
				String descripcion = ( String ) result[ 0 ];
				Integer fhinicio = ( Integer ) result[ 1 ];
				Integer fhfinal = ( Integer ) result[ 2 ];
				first.setDescali( descripcion );
				first.setFhinicio( fhinicio );
				first.setFhfinal( fhfinal );
			}
			/*
			 * for(int z = i+1; z< copy.size(); z++){
			 * MovimientoCuentaAliasData second = copy.get(z);
			 * if(first.getCamara().equals(second.getCamara()) &&
			 * first.getCdalias().equals(second.getCdalias()) &&
			 * first.getCdoperacion().equals(second.getCdoperacion()) &&
			 * first.getFliquidacion().equals(second.getFliquidacion())){
			 * encontrado = true;
			 * if(first.getFhinicio() != null && first.getFhinicio() != BigDecimal.ZERO && first.getFhfinal() != null && first.getFhfinal()
			 * != BigDecimal.ZERO){
			 * if(second.getFhinicio() != null && second.getFhinicio() != BigDecimal.ZERO && second.getFhfinal() != null &&
			 * second.getFhfinal() != BigDecimal.ZERO){
			 * if(first.getFhinicio().longValue() >= fliq.longValue() && fliq.longValue() >= first.getFhfinal().longValue() ){
			 * copy.remove(second);
			 * break;
			 * }else{
			 * copy.remove(first);
			 * }
			 * }
			 * }
			 * }else{
			 * //
			 * }
			 * }
			 */
		}
		return resultList;
	}

	private boolean checkIfKeyExist( String key, Map< String, Serializable > params ) {
		boolean exist = false;
		if ( params.get( key ) != null && !params.get( key ).equals( "" ) ) {
			exist = true;
		}
		return exist;
	}
}
