package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.DesgloseTitularDAO;
import sibbac.business.wrappers.database.model.Tmct0DesgloseTitular;
import sibbac.database.bo.AbstractBo;
@Service
public class DesgloseTitularBO extends
	AbstractBo<Tmct0DesgloseTitular, Long, DesgloseTitularDAO> {

}
