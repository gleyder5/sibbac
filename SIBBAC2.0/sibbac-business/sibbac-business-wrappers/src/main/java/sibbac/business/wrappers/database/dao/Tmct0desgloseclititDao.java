package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.DesgloseClititCaseEjecucionesInformacionMercadoDTO;
import sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista16DTO;
import sibbac.business.wrappers.database.dto.fichero.corretajes.Tmct0desgloseclititEnvioCorretajeDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;

@Repository
public interface Tmct0desgloseclititDao extends JpaRepository<Tmct0desgloseclitit, Long> {

  @Query("SELECT d from Tmct0desgloseclitit d where d.nudesglose in :nudesgloses ")
  List<Tmct0desgloseclitit> findAllByNudesgloses(@Param("nudesgloses") List<Long> nudesgloses);

  @Query("SELECT distinct d.cdoperacion from Tmct0desgloseclitit d where d.cdoperacion!=null")
  List<String> getLCdoperacion();

  @Query("select sum(d.imtitulos) as titulostotales, d.cdisin, d.tmct0desglosecamara.fecontratacion, d.tmct0desglosecamara.feliquidacion, "
      + " d.cdsentido , d.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado, d.imprecio, d.cdcamara, "
      // + "d.cdmiembromkt, "
      + " min(d.nuordenmkt), min(d.nurefexemkt) "
      + " from Tmct0desgloseclitit d where d.cdoperacion in :operaciones "
      + " group by d.cdisin, d.tmct0desglosecamara.fecontratacion, d.tmct0desglosecamara.feliquidacion, d.cdsentido , "
      + " d.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado, d.imprecio, d.cdcamara")
  // + ", d.cdmiembromkt")
  List<Object[]> getTitulosCuentaPropia(@Param("operaciones") List<String> operaciones);

  @Query("SELECT o FROM Tmct0desgloseclitit o WHERE  o.tmct0desglosecamara.tmct0alc.id = :alcId "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0desgloseclitit> findAllByTmct0alcId(@Param("alcId") Tmct0alcId alcId);

  @Query("SELECT o FROM Tmct0desgloseclitit o WHERE  o.tmct0desglosecamara.tmct0alc.id = :alcId "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " and o.tmct0desglosecamara.tmct0CamaraCompensacion.cdCodigo = :camara"
      + " and o.cuentaCompensacionDestino = :cta ")
  List<Tmct0desgloseclitit> findAllByTmct0alcIdAndCamaraAndCuentaCompensacion(@Param("alcId") Tmct0alcId alcId,
      @Param("camara") String camaraCompensacion, @Param("cta") String cuentaCompensacion);

  @Query("SELECT new sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO(d.imprecio, sum(imtitulos)) "
      + " from Tmct0desgloseclitit d where d.tmct0desglosecamara.tmct0alc.tmct0alo.id = :aloId "
      + " and d.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0" + " GROUP BY d.imprecio")
  List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByImprecioByAloId(@Param("aloId") Tmct0aloId aloId);

  
  @Query("SELECT d.imprecio, sum(imtitulos) "
      + " from Tmct0desgloseclitit d where d.tmct0desglosecamara.tmct0alc.id = :alcId "
      + " and d.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 GROUP BY d.imprecio")
  List<Object[]> sumTitulosByPriceAndAlcId(@Param("alcId") Tmct0alcId alcId);

  @Query("SELECT new sibbac.business.wrappers.database.dto.TitulosDesgloseManualAgrupadosDTO(d.cuentaCompensacionDestino, d.miembroCompensadorDestino, "
      + " d.codigoReferenciaAsignacion, sum(imtitulos)) "
      + " from Tmct0desgloseclitit d where d.tmct0desglosecamara.tmct0alc.tmct0alo.id = :aloId "
      + " and d.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0"
      + " GROUP BY d.cuentaCompensacionDestino, d.miembroCompensadorDestino, d.codigoReferenciaAsignacion")
  List<TitulosDesgloseManualAgrupadosDTO> sumImtitulosGroupByCuentaCompensacionDestinoAndMiembroCompensadorDestinoAndCodigoReferenciaAsignacionByAloId(
      @Param("aloId") Tmct0aloId aloId);

  /**
   * 
   * @param isin
   * @param tpoper
   * @param feEjecucion
   * @param cdEstadoAsig
   * @param cuentaClearing
   * @return
   */
  @Query(value = "SELECT DISTINCT(DCT.FEEJECUC) FROM TMCT0DESGLOSECAMARA DC, ( SELECT FEEJECUC, IDDESGLOSECAMARA FROM TMCT0DESGLOSECLITIT DCT WHERE CDISIN = :isin AND CDSENTIDO = :tpoper AND FEEJECUC >= :feEjecucion AND IMTITULOS > NUTITFALLIDOS ) DCT WHERE DC.CDESTADOASIG = :cdEstadoAsig AND DC.IDDESGLOSECAMARA = DCT.IDDESGLOSECAMARA AND IDINFOCOMPENSACION IN ( SELECT IDINFOCOMP FROM TMCT0INFOCOMPENSACION WHERE CDCTACOMPENSACION IN ( SELECT CD_CODIGO FROM TMCT0_CUENTAS_DE_COMPENSACION WHERE CUENTA_CLEARING = :cuentaClearing ) ) ORDER BY DCT.FEEJECUC ASC", nativeQuery = true)
  List<Date> getListaDistinctFeejecucForParams(@Param("isin") String isin, @Param("tpoper") Character tpoper,
      @Param("feEjecucion") Date feEjecucion, @Param("cdEstadoAsig") Integer cdEstadoAsig,
      @Param("cuentaClearing") Character cuentaClearing);

  /**
   * 
   * @param desglosecamara
   * @return
   */
  @Query(value = "SELECT SUM(IMEFECTIVO) FROM TMCT0DESGLOSECLITIT WHERE iddesglosecamara = :desglosecamara GROUP BY iddesglosecamara", nativeQuery = true)
  BigDecimal findByTmct0desglosecamaraGroupByFields(@Param("desglosecamara") Long desglosecamara);

  @Query(value = "SELECT SUM(cli.IMEFECTIVO) FROM TMCT0_ALC_ORDEN_MERCADO mer "
      + "INNER JOIN TMCT0DESGLOSECLITIT cli ON mer.IDDESGLOSECAMARA = cli.IDDESGLOSECAMARA "
      + "WHERE mer.IDALC_ORDEN = :alcOrdenId AND mer.ESTADO = 'A'", nativeQuery = true)
  BigDecimal sumByAlcOrdenes(@Param("alcOrdenId") Long alcOrdenId);

  @Query("SELECT o FROM Tmct0desgloseclitit o WHERE  o.tmct0desglosecamara.iddesglosecamara IN (:iddesglosecamaraList )")
  List<Tmct0desgloseclitit> findByIddesglosecamaraIn(@Param("iddesglosecamaraList") List<Long> iddesglosecamaraList);

  @Query("SELECT o FROM Tmct0desgloseclitit o WHERE  o.tmct0desglosecamara.iddesglosecamara = :iddesglosecamara ")
  List<Tmct0desgloseclitit> findByIddesglosecamara(@Param("iddesglosecamara") Long iddesglosecamaraList);

  @Query("SELECT o FROM Tmct0desgloseclitit o WHERE  o.tmct0desglosecamara.tmct0alc.id = :alcId "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0desgloseclitit> findByTmct0alcId(@Param("alcId") Tmct0alcId alcId);

  /**
   * 
   * Actualiza datos de mercado en desgloseclitit
   * 
   * @param nuorden
   * @param nbooking
   * @param nuejecuc
   * @param fhaudit
   * @param cdusuaud
   * @param indicadorCotizacion
   * @param cdoperacionecc
   * @param segmentoMercado
   * @param plataformaNegociacion
   * @param fechaOrdenMercado
   * @param horaOrdenMercado
   */
  @Modifying
  @Query(value = "UPDATE TMCT0DESGLOSECLITIT DCT SET DCT.AUDIT_FECHA_CAMBIO = :pfhaudit, DCT.AUDIT_USER = :cdusuaud, "
      + " DCT.CDINDCOTIZACION = :pcdincot, DCT.CDOPERACION = :pcdoperacionecc, DCT.CDOPERACIONECC = :pcdoperacionecc, DCT.NUOPERACIONPREV = :pcdoperacionecc, "
      + " DCT.NUOPERACIONINIC = :pcdoperacionecc, DCT.CDSEGMENTO = :psegmento, DCT.CD_PLATAFORMA_NEGOCIACION = :pplataforma, DCT.FEORDENMKT = :pfeordenmkt, "
      + " DCT.HOORDENMKT = :phoordenmkt "
      + " WHERE DCT.NUEJECUC=:nuejecuc AND EXISTS (SELECT 1 FROM TMCT0DESGLOSECAMARA DC WHERE DCT.IDDESGLOSECAMARA=DC.IDDESGLOSECAMARA "
      + "  AND DC.NUORDEN=:nuorden AND DC.NBOOKING=:nbooking AND DC.CDESTADOASIG > 0 ) ", nativeQuery = true)
  public Integer updateDatosMercado(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("nuejecuc") String nuejecuc, @Param("pfhaudit") Date fhaudit, @Param("cdusuaud") String cdusuaud,
      @Param("pcdincot") Character indicadorCotizacion, @Param("pcdoperacionecc") String cdoperacionecc,
      @Param("psegmento") String segmentoMercado, @Param("pplataforma") String plataformaNegociacion,
      @Param("pfeordenmkt") Date fechaOrdenMercado, @Param("phoordenmkt") Date horaOrdenMercado);

  /**
   * Recupera los desgloses de un booking que no tienen el nuopeinic insertado
   * */
  @Query("select  new sibbac.business.wrappers.database.dto.caseejecuciones.DesgloseClititCaseEjecucionesInformacionMercadoDTO("
      + " dct.cdisin, dc.sentido, dct.feejecuc, dct.cdmiembromkt, dct.cdsegmento, dct.tpopebol, dct.nuordenmkt, dct.nurefexemkt, dct.nuejecuc) "
      + " from Tmct0desgloseclitit dct, Tmct0desglosecamara dc where dc.iddesglosecamara = dct.tmct0desglosecamara.iddesglosecamara "
      + " and dc.tmct0alc.tmct0alo.tmct0bok.id.nuorden = :pnuorden and dc.tmct0alc.tmct0alo.tmct0bok.id.nbooking = :pnbooking "
      + " and dc.tmct0estadoByCdestadoasig.idestado > 0 " + " and dct.nuoperacioninic = '' ")
  public List<DesgloseClititCaseEjecucionesInformacionMercadoDTO> findDesglosesSinDatosMercadoByNuordenNbooking(
      @Param("pnuorden") String nuorden, @Param("pnbooking") String nbooking);

  /**
   * Recupera los desgloses de un booking que no tienen el nuopeinic insertado
   * */
  @Query("select  new sibbac.business.wrappers.database.dto.fichero.corretajes.Tmct0desgloseclititEnvioCorretajeDTO("
      + " dct.feejecuc, dct.feordenmkt, dc.feliquidacion, dct.cdmiembromkt, sum(dct.imtitulos), sum(dct.imefectivo) ) "
      + " from Tmct0desgloseclitit dct, Tmct0desglosecamara dc where dc.iddesglosecamara = dct.tmct0desglosecamara.iddesglosecamara "
      + " and dc.tmct0alc.id.nuorden = :pnuorden and dc.tmct0alc.id.nbooking = :pnbooking "
      + " and dc.tmct0alc.id.nucnfclt = :pnucnfclt and dc.tmct0alc.id.nucnfliq = :pnucnfliq "
      + " and dc.tmct0estadoByCdestadoasig.idestado > 0 "
      + " group by dct.feejecuc, dct.feordenmkt, dc.feliquidacion, dct.cdmiembromkt "
      + " order by sum(dct.imtitulos) desc, dct.cdmiembromkt asc ")
  public List<Tmct0desgloseclititEnvioCorretajeDTO> findDesglosesFicheroCorretaje(@Param("pnuorden") String nuorden,
      @Param("pnbooking") String nbooking, @Param("pnucnfclt") String nucnfclt, @Param("pnucnfliq") short nucnfliq);

  @Modifying
  @Query(value = "UPDATE TMCT0DESGLOSECLITIT DCT SET DCT.ID_FICHERO_CC = :pidFicheroCC WHERE "
      + " EXISTS (SELECT 1 FROM TMCT0DESGLOSECAMARA DC WHERE DCT.IDDESGLOSECAMARA = DC.IDDESGLOSECAMARA "
      + " AND DC.NUORDEN = :pnuorden AND DC.NBOOKING = :pnbooking AND DC.NUCNFCLT = :pnucnfclt AND DC.NUCNFLIQ = :pnucnfliq "
      + " AND DC.CDESTADOASIG > 0 ) AND DCT.CDMIEMBROMKT = :pcdmiembromkt AND DCT.FEEJECUC = :pfeejecuc ", nativeQuery = true)
  public Integer updateDatosEnvioFicheroCorretaje(@Param("pnuorden") String nuorden,
      @Param("pnbooking") String nbooking, @Param("pnucnfclt") String nucnfclt, @Param("pnucnfliq") short nucnfliq,
      @Param("pcdmiembromkt") String cdmiembromkt, @Param("pfeejecuc") Date feejecuc,
      @Param("pidFicheroCC") String idFicheroCC);

  @Query("SELECT new sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista16DTO( b.id, e.feejecuc, e.hoejecuc, "
      + " e.cdbroker, c.cdrefban, e.cdtpoper, e.cdisin, dt.imprecio, e.fevalor, sum(dt.imtitulos), "
      + " dt.nurefexemkt, e.id.nuejecuc, dt.cdPlataformaNegociacion, '' , '' ) "
      + " FROM Tmct0ord o, Tmct0bok b, Tmct0alo a, Tmct0alc c, Tmct0desglosecamara d, Tmct0desgloseclitit dt, Tmct0eje e, Tmct0movimientoecc m, "
      + " Tmct0movimientooperacionnuevaecc mn  "
      + " WHERE o.nuorden = b.id.nuorden AND b.id.nuorden=a.id.nuorden and b.id.nbooking=a.id.nbooking "
      + " and a.id.nuorden=c.id.nuorden and a.id.nbooking=c.id.nbooking and a.id.nucnfclt=c.id.nucnfclt"
      + " and c.id.nuorden=d.tmct0alc.id.nuorden and c.id.nbooking=d.tmct0alc.id.nbooking and c.id.nucnfclt=d.tmct0alc.id.nucnfclt "
      + " and c.id.nucnfliq=d.tmct0alc.id.nucnfliq and c.nutitliq = b.nutiteje "
      + " and d.iddesglosecamara=dt.tmct0desglosecamara.iddesglosecamara "
      + " and d.iddesglosecamara= m.tmct0desglosecamara.iddesglosecamara "
      + " and m.idmovimiento = mn.tmct0movimientoecc.idmovimiento and mn.nudesglose=dt.nudesglose "
      + " and a.id.nuorden=e.id.nuorden and a.id.nbooking=e.id.nbooking and dt.nuejecuc=e.id.nuejecuc "
      + " and m.miembroCompensadorDestino in :compensadores and m.codigoReferenciaAsignacion in :referencias "
      + " and b.cdalias not in :aliass and d.tmct0estadoByCdestadoasig.idestado >= :estado "
      + " and length(trim(c.cdrefban)) = 20 and c.cdrefban like :refban "
      + " and (b.cdindimp <> 'S' or b.cdindimp is null ) "
      + " and m.cdestadomov = :estadomov "
      + " and c.feejeliq >= :sincedate "
      + " and b.casepti = 'S' AND (o.nureford = '' OR o.nureford IS NULL ) "
      + " GROUP BY b.id.nuorden, b.id.nbooking, e.feejecuc, e.hoejecuc, e.cdbroker, c.cdrefban, e.cdtpoper, e.cdisin, dt.imprecio, e.fevalor, "
      + " dt.nurefexemkt, e.id.nuejecuc, dt.cdPlataformaNegociacion " + " ORDER BY b.id.nbooking ASC ")
  List<ConfirmacionMinorista16DTO> findAllConfirmacionMinorista16(@Param("compensadores") List<String> compensadores,
      @Param("referencias") List<String> referencias, @Param("aliass") List<String> notInAliass,
      @Param("refban") String cdrefbanStart, @Param("estado") Integer estado, @Param("estadomov") String estadomov,
      @Param("sincedate") Date sincedate);

  @Query("SELECT new sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista16DTO( b.id, e.feejecuc, e.hoejecuc, "
      + " e.cdbroker, c.cdrefban, e.cdtpoper, e.cdisin, dt.imprecio, e.fevalor, sum(dt.imtitulos), "
      + " dt.nurefexemkt, e.id.nuejecuc, dt.cdPlataformaNegociacion, '' , '' ) "
      + " FROM Tmct0ord o, Tmct0bok b, Tmct0alc c, Tmct0desglosecamara d, Tmct0desgloseclitit dt, Tmct0eje e, Tmct0movimientoecc m, "
      + " Tmct0movimientooperacionnuevaecc mn  "
      + " WHERE o.nuorden = b.id.nuorden "
      + "  and c.id.nuorden=b.id.nuorden and c.id.nbooking=b.id.nbooking "
      + " and c.id.nuorden=d.tmct0alc.id.nuorden and c.id.nbooking=d.tmct0alc.id.nbooking and c.id.nucnfclt=d.tmct0alc.id.nucnfclt "
      + " and c.id.nucnfliq=d.tmct0alc.id.nucnfliq  "
      + " and d.iddesglosecamara=dt.tmct0desglosecamara.iddesglosecamara "
      + " and d.iddesglosecamara= m.tmct0desglosecamara.iddesglosecamara "
      + " and m.idmovimiento = mn.tmct0movimientoecc.idmovimiento and mn.nudesglose=dt.nudesglose "
      + " and c.id.nuorden=e.id.nuorden and c.id.nbooking=e.id.nbooking and dt.nuejecuc=e.id.nuejecuc "
      + " and m.miembroCompensadorDestino in :compensadores and m.codigoReferenciaAsignacion in :referencias "
      + " and  d.tmct0estadoByCdestadoasig.idestado >= :estado "
      + " and m.cdestadomov = :estadomov "
      + " and b.id.nuorden= :nuorden and b.id.nbooking =:nbooking "
      + " GROUP BY b.id.nuorden, b.id.nbooking, e.feejecuc, e.hoejecuc, e.cdbroker, c.cdrefban, e.cdtpoper, e.cdisin, dt.imprecio, e.fevalor, "
      + " dt.nurefexemkt, e.id.nuejecuc, dt.cdPlataformaNegociacion " + " ORDER BY b.id.nbooking ASC ")
  List<ConfirmacionMinorista16DTO> findAllConfirmacionMinorista16(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("compensadores") List<String> compensadores,
      @Param("referencias") List<String> referencias, @Param("estado") Integer estado,
      @Param("estadomov") String estadomov);

  @Query("SELECT o FROM Tmct0desgloseclitit o WHERE o.cdoperacionecc = :cdoperacionecc "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 and o.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.casepti = 'S' ")
  List<Tmct0desgloseclitit> findAllByCdoperacionecc(@Param("cdoperacionecc") String cdoperacionecc);

  @Query("SELECT o FROM Tmct0desgloseclitit o, Tmct0alo a WHERE o.tmct0desglosecamara.tmct0alc.tmct0alo.id = a.id "
      + " AND  o.cdoperacionecc = :cdoperacionecc "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadotit.idestado < :estadotitularLt "
      + " AND a.cdestadotit < :estadotitularLt " + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " AND a.tmct0estadoByCdestadoasig.idestado >0 ")
  List<Tmct0desgloseclitit> findByCdoperacioneccAndCdestadotitLt(@Param("cdoperacionecc") String cdoperacionecc,
      @Param("estadotitularLt") int cdestadotitLt);

  @Query("SELECT o FROM Tmct0desgloseclitit o, Tmct0alo a WHERE o.tmct0desglosecamara.tmct0alc.tmct0alo.id = a.id "
      + " AND o.feejecuc = :feejecuc AND o.numeroOperacionDCV = :numeroOperacionDcv "
      + " AND o.numeroOperacionDCV = :numeroOperacionDcv AND o.cdsentido = :sentido "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadotit.idestado < :estadotitularLt "
      + " AND a.cdestadotit < :estadotitularLt " + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " AND a.tmct0estadoByCdestadoasig.idestado >0 ")
  List<Tmct0desgloseclitit> findByFeejecucAndNumeroOperacionDcvAndSentidoAndCdestadotitLt(
      @Param("numeroOperacionDcv") String numeroOperacionDcv, @Param("feejecuc") Date feejecuc,
      @Param("sentido") char sentido, @Param("estadotitularLt") int cdestadotitLt);

  @Query("SELECT o FROM Tmct0desgloseclitit o, Tmct0alo a WHERE o.tmct0desglosecamara.tmct0alc.tmct0alo.id = a.id "
      + " AND o.feejecuc = :feejecuc  AND o.numeroOperacionDCV = :numeroOperacionDcv "
      + " AND o.numeroOperacionDCV = :numeroOperacionDcv  AND o.cdsentido = :sentido "
      + " AND o.tmct0desglosecamara.tmct0estadoByCdestadotit.idestado < :estadotitularLt "
      + " AND a.cdestadotit < :estadotitularLt " + " AND o.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " AND a.tmct0estadoByCdestadoasig.idestado >0 ")
  List<Tmct0desgloseclitit> findBy(@Param("numeroOperacionDcv") String numeroOperacionDcv,
      @Param("feejecuc") Date feejecuc, @Param("sentido") Character sentido, @Param("estadotitularLt") int cdestadotitLt);

  @Query("SELECT dct.nudesglose FROM Tmct0desgloseclitit dct, Tmct0eje eje "
      + "WHERE eje.cdisin = :cdisin AND eje.feejecuc = :feejecuc AND eje.cdtpoper = :sentido "
      + " AND eje.clearingplatform = :camara AND eje.segmenttradingvenue = :segmento "
      + " AND eje.tpopebol = :cdoperacionmkt AND eje.cdMiembroMkt = :cdmiembromkt "
      + " AND eje.nuopemer = :nuordenmkt AND eje.nurefere = :nuexemkt "
      + " AND dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = eje.tmct0bok.id "
      + " AND dct.nuejecuc =  eje.id.nuejecuc "
      + " AND dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 " + " ORDER BY dct.nudesglose asc")
  List<Long> findAllNudesgloseByDatosMercado(@Param("cdisin") String cdisin, @Param("feejecuc") Date feejecuc,
      @Param("sentido") Character sentido, @Param("camara") String camara, @Param("segmento") String segmento,
      @Param("cdoperacionmkt") String cdoperacionmkt, @Param("cdmiembromkt") String cdmiembromkt,
      @Param("nuordenmkt") String nuordenmkt, @Param("nuexemkt") String nuexemkt);

  @Query("SELECT dct FROM Tmct0desgloseclitit dct, Tmct0eje eje "
      + "WHERE eje.cdisin = :cdisin AND eje.feejecuc = :feejecuc AND eje.cdtpoper = :sentido "
      + " AND eje.clearingplatform = :camara AND eje.segmenttradingvenue = :segmento "
      + " AND eje.tpopebol = :cdoperacionmkt AND eje.cdMiembroMkt = :cdmiembromkt "
      + " AND eje.nuopemer = :nuordenmkt AND eje.nurefere = :nuexemkt "
      + " AND dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = eje.tmct0bok.id "
      + " AND dct.nuejecuc =  eje.id.nuejecuc "
      + " AND dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 " + " ORDER BY dct.nudesglose asc")
  List<Tmct0desgloseclitit> findAllByDatosMercado(@Param("cdisin") String cdisin, @Param("feejecuc") Date feejecuc,
      @Param("sentido") Character sentido, @Param("camara") String camara, @Param("segmento") String segmento,
      @Param("cdoperacionmkt") String cdoperacionmkt, @Param("cdmiembromkt") String cdmiembromkt,
      @Param("nuordenmkt") String nuordenmkt, @Param("nuexemkt") String nuexemkt, Pageable page);

  @Query("SELECT dct FROM Tmct0desgloseclitit dct, Tmct0eje eje "
      + "WHERE eje.cdisin = :cdisin AND eje.feejecuc = :feejecuc AND eje.cdtpoper = :sentido "
      + " AND eje.clearingplatform = :camara AND eje.segmenttradingvenue = :segmento "
      + " AND eje.tpopebol = :cdoperacionmkt AND eje.cdMiembroMkt = :cdmiembromkt "
      + " AND eje.nuopemer = :nuordenmkt AND eje.nurefere = :nuexemkt "
      + " AND dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = eje.tmct0bok.id "
      + " AND dct.nuejecuc =  eje.id.nuejecuc "
      + " AND dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " AND dct.tmct0desglosecamara.tmct0alc.id.nuorden = :nuorden "
      + " AND dct.tmct0desglosecamara.tmct0alc.id.nbooking = :nbooking " + " ORDER BY dct.nudesglose asc ")
  List<Tmct0desgloseclitit> findAllByDatosMercadoAndNuordenAndNbooking(@Param("cdisin") String cdisin,
      @Param("feejecuc") Date feejecuc, @Param("sentido") Character sentido, @Param("camara") String camara,
      @Param("segmento") String segmento, @Param("cdoperacionmkt") String cdoperacionmkt,
      @Param("cdmiembromkt") String cdmiembromkt, @Param("nuordenmkt") String nuordenmkt,
      @Param("nuexemkt") String nuexemkt, @Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      Pageable page);

  @Query("SELECT dct FROM Tmct0desgloseclitit dct, Tmct0eje eje "
      + "WHERE dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = eje.tmct0bok.id "
      + " AND dct.nuejecuc =  eje.id.nuejecuc AND dct.tmct0desglosecamara.tmct0alc.id.nuorden = :nuorden "
      + " AND dct.tmct0desglosecamara.tmct0alc.id.nbooking = :nbooking "
      + " AND dct.tmct0desglosecamara.tmct0alc.id.nucnfclt = :nucnfclt AND dct.nuejecuc = :nuejecuc "
      + " AND dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0desgloseclitit> findAllByNuordenAndNbookingAndNucnfcltAndNuejecuc(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking, @Param("nucnfclt") String nucnfclt, @Param("nuejecuc") String nuejecuc);

  @Modifying
  @Transactional
  @Query(value = "UPDATE Tmct0desgloseclitit SET miembroCompensadorDestino=:miembroCompensadorDestino, codigoReferenciaAsignacion=:codigoReferenciaAsignacion, "
      + " cuentaCompensacionDestino=:cuentaCompensacionDestino WHERE tmct0desglosecamara.iddesglosecamara=:iddesglosecamara")
  public Integer updateDatosCompensacionDesgloseClitit(@Param("iddesglosecamara") Long iddesglosecamara,
      @Param("miembroCompensadorDestino") String miembroCompensadorDestino,
      @Param("codigoReferenciaAsignacion") String codigoReferenciaAsignacion,
      @Param("cuentaCompensacionDestino") String cuentaCompensacionDestino);

  @Query("select distinct dct from Tmct0anotacionecc an, Tmct0desgloseclitit dct where "
      + " dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = :bookingId "
      + " and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " and dct.cdoperacionecc = an.cdoperacionecc and an.imtitulosdisponibles < dct.imtitulos ")
  List<Tmct0desgloseclitit> findAllByBookingIdAndSinTitulosDiponiblesAnotacionForDesglose(
      @Param("bookingId") Tmct0bokId bookId);

  @Query("SELECT sum(d.imtitulos) from Tmct0desgloseclitit d where d.cdoperacionecc = :cdoperacionecc "
      + " and d.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  BigDecimal sumImtitulosByCdoperacionecc(@Param("cdoperacionecc") String cdoperacionecc);

  @Query(value = "SELECT SUM(DT.IMTITULOS) FROM TMCT0DESGLOSECLITIT DT "
      + " INNER JOIN TMCT0DESGLOSECAMARA D ON D.IDDESGLOSECAMARA=DT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0BOK B ON D.NUORDEN=B.NUORDEN AND D.NBOOKING=B.NBOOKING "
      + " WHERE DT.NUOPERACIONINIC = :nuoperacioninic AND D.CDESTADOASIG > 0 AND B.CDESTADOASIG >0 AND B.CASEPTI='S' "
      + " AND EXISTS (SELECT 1 FROM TMCT0AFI F WHERE D.NUORDEN=F.NUORDEN AND D.NBOOKING=F.NBOOKING AND D.NUCNFCLT=F.NUCNFCLT AND D.NUCNFLIQ=F.NUCNFLIQ AND F.CDHOLDER = :cdholder AND F.CDINACTI = 'A') ", nativeQuery = true)
  BigDecimal sumImtitulosByNuoperacioninicAndExistsCdholder(@Param("nuoperacioninic") String nuoperacionini,
      @Param("cdholder") String cdholder);

  @Query(value = "SELECT SUM(DT.IMTITULOS) FROM TMCT0DESGLOSECLITIT DT "
      + " INNER JOIN TMCT0DESGLOSECAMARA D ON D.IDDESGLOSECAMARA=DT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0BOK B ON D.NUORDEN=B.NUORDEN AND D.NBOOKING=B.NBOOKING "
      + " WHERE DT.NUOPERACIONINIC = :nuoperacioninic AND D.CDESTADOASIG > 0 AND B.CDESTADOASIG >0 AND B.CASEPTI='S' "
      + " AND EXISTS (SELECT 1 FROM TMCT0AFI F WHERE D.NUORDEN=F.NUORDEN AND D.NBOOKING=F.NBOOKING AND D.NUCNFCLT=F.NUCNFCLT AND D.NUCNFLIQ=F.NUCNFLIQ AND F.CDHOLDER <> :cdholder AND F.CDINACTI = 'A') ", nativeQuery = true)
  BigDecimal sumImtitulosByNuoperacioninicAndExistsDistinctCdholder(@Param("nuoperacioninic") String nuoperacionini,
      @Param("cdholder") String cdholder);

  @Query("select dct.tmct0desglosecamara.tmct0alc.id from Tmct0desgloseclitit dct where dct.nuoperacioninic = :nuoperacioninic "
      + "and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0alcId> findAllAlcIdByNuoperacioinic(@Param("nuoperacioninic") String nuoperacionini);
  
  @Query("select sum(dct.imtitulos) from Tmct0desgloseclitit dct where dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = :bookId "
      + "and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  BigDecimal sumTitulosByTmct0bokId(@Param("bookId") Tmct0bokId bookId);

  @Query(value = "SELECT DISTINCT DT.NUDESGLOSE FROM TMCT0MOVIMIENTOECC_FIDESSA MN INNER JOIN TMCT0DESGLOSECLITIT DT ON MN.NUOPERACIONPREV = DT.CDOPERACIONECC "
      + " INNER JOIN TMCT0DESGLOSECAMARA DC ON DT.IDDESGLOSECAMARA = DT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0ORD ORD ON DC.NUORDEN = ORD.NUORDEN "
      + " INNER JOIN TMCT0BOK B ON DC.NUORDEN = B.NUORDEN AND DC.NBOOKING = B.NBOOKING "
      + " WHERE MN.FLIQUIDACION = :fliquidacion AND MN.IMTITULOS_PENDIENTES_ASIGNAR > 0 "
      + " AND DC.FELIQUIDACION = MN.FLIQUIDACION AND DC.CDESTADOASIG > 0 AND ORD.ISSCRDIV = :isscrdiv AND DT.CDOPERACIONECC <> '' "
      + " AND B.CASEPTI = 'S' ", nativeQuery = true)
  List<Long> findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(@Param("fliquidacion") Date fliquidacion,
      @Param("isscrdiv") char isscrdiv);

  // @Query(value =
  // "SELECT DISTINCT dt.nudesglose FROM Tmct0movimientoeccFidessa mn INNER JOIN Tmct0desgloseclitit dt ON mn.nuoperacionprev = dt.cdoperacionecc "
  // +
  // " WHERE mn.fliquidacion = :fliquidacion AND mn.imtitulosPendientesAsignar > 0 "
  // +
  // " AND dt.tmct0desglosecamara.feliquidacion =  mn.fliquidacion AND dt.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
  // +
  // " AND dt.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv AND dt.cdoperacionecc != '' "
  // + " AND dt.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.casepti = 'S' ")
  // List<Long>
  // findAllIdsByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(@Param("fliquidacion")
  // Date fliquidacion,
  // @Param("isscrdiv") char isscrdiv);

  @Query(value = "SELECT B.NUORDEN, B.NBOOKING FROM TMCT0BOK B "
      + "INNER JOIN TMCT0ORD ORD ON B.NUORDEN = ORD.NUORDEN "
      + " WHERE B.FEVALOR =:fliquidacion AND B.CDESTADOASIG > 0 AND ORD.ISSCRDIV = :isscrdiv AND B.CASEPTI = 'S' "
      + " AND 1 < (SELECT COUNT(*) FROM TMCT0DESGLOSECAMARA DC INNER JOIN TMCT0DESGLOSECLITIT DT ON DC.IDDESGLOSECAMARA=DT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0MOVIMIENTOECC_FIDESSA MN ON MN.NUOPERACIONPREV = DT.CDOPERACIONECC "
      + " WHERE DC.NUORDEN=B.NUORDEN AND DC.NBOOKING = B.NBOOKING AND DC.CDESTADOASIG > 0 AND MN.IMTITULOS_PENDIENTES_ASIGNAR > 0)"
      + " GROUP BY B.NUORDEN, B.NBOOKING ", nativeQuery = true)
  List<Object[]> findAllBookingIdByFliquidacionAndTitulosDisponiblesAndMasDe1DesglosesDeAlta(
      @Param("fliquidacion") Date fliquidacion, @Param("isscrdiv") char isscrdiv);

  @Query(value = "SELECT DC.NUORDEN, DC.NBOOKING FROM TMCT0MOVIMIENTOECC_FIDESSA MN "
      + " INNER JOIN TMCT0DESGLOSECLITIT DT ON MN.NUOPERACIONPREV = DT.CDOPERACIONECC "
      + " INNER JOIN TMCT0DESGLOSECAMARA DC ON DT.IDDESGLOSECAMARA = DC.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0ORD O ON DC.NUORDEN = O.NUORDEN "
      + " INNER JOIN TMCT0BOK B ON DC.NUORDEN = B.NUORDEN AND DC.NBOOKING = B.NBOOKING "
      + " WHERE MN.FLIQUIDACION = :fliquidacion AND  MN.IMTITULOS_PENDIENTES_ASIGNAR > 0 AND O.ISSCRDIV = :isscrdiv AND DC.CDESTADOASIG > 0  AND MN.CDOPERACIONECC <> '' "
      + " AND B.CASEPTI = 'S' GROUP BY DC.NUORDEN, DC.NBOOKING", nativeQuery = true)
  List<Object[]> findAllBookingIdByFliquidacionAndTitulosDisponiblesAndDesglosesDeAlta(
      @Param("fliquidacion") Date fliquidacion, @Param("isscrdiv") char isscrdiv);

  @Query(value = "SELECT DISTINCT DT.NUDESGLOSE FROM TMCT0DESGLOSECLITIT DT "
      + " INNER JOIN TMCT0DESGLOSECAMARA DC ON DT.IDDESGLOSECAMARA = DT.IDDESGLOSECAMARA "
      + " INNER JOIN TMCT0MOVIMIENTOECC_FIDESSA MN ON MN.NUOPERACIONPREV = DT.CDOPERACIONECC "
      + " WHERE DC.NUORDEN = :nuorden AND DC.NBOOKING = :nbooking AND DC.CDESTADOASIG > 0 AND DC.FELIQUIDACION = MN.FLIQUIDACION AND MN.IMTITULOS_PENDIENTES_ASIGNAR > 0 "
      + " AND DT.CDOPERACIONECC <> '' ", nativeQuery = true)
  List<Long> findAllIdsByNuordenAndNbookingAndTitulosDisponiblesAndDesglosesDeAlta(@Param("nuorden") String nuorden,
      @Param("nbooking") String nbooking);

} // Tmct0desgloseclititDao
