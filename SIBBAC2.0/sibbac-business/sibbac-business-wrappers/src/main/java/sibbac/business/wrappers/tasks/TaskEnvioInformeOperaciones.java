package sibbac.business.wrappers.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.data.InformeOpercacionData;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_WRAPPERS.NAME, name = Task.GROUP_WRAPPERS.JOB_ENVIO_EXCEL_VIA_MAIL_OPERACIONES, interval = 1, intervalUnit = IntervalUnit.HOUR)
public class TaskEnvioInformeOperaciones extends WrapperTaskConcurrencyPrevent {
    private static final String TAG = TaskEnvioInformeOperaciones.class
	    .getName();
    @Autowired
    private Tmct0alcBo alcBo;
    @Autowired
    private AliasBo aliasBo;
    @Autowired
    private GestorServicios gestorServicios;

    @Override
    public void executeTask() {

	LOG.debug(TAG + " Starting ...");

	try {
	    executeTask2();
	} catch (Exception e) {
	    LOG.error(e.getMessage(), e);
	    throw e;
	}

    }

    private void executeTask2() {
	final List<Alias> listaAlias = gestorServicios.isActive(this.jobPk);
	if (CollectionUtils.isNotEmpty(listaAlias)) {
	    LOG.debug(TAG
		    + " Hay alias programados para el envio del Informe de operaciones .... ");

	    for (Alias alias : listaAlias) {
		final String prefix = "[TaskEnvioInformaOperaciones]";
		final List<String> taskMessages = new ArrayList<String>();
		LOG.debug(TAG
			+ " Extracción de datos para la generación del informe de operaciones.... ");
		String cdalias = alias.getTmct0ali().getId().getCdaliass();
		List<InformeOpercacionData> lista = alcBo
			.getInformeOperacionData(cdalias);
		LOG.debug(TAG
			+ " Fin de la extracción de operaciones de la tabla TMCT0_ALC ");
		LOG.debug(TAG + "Se han obtenido " + lista.size()
			+ " operaciones..");
		if (CollectionUtils.isNotEmpty(lista)) {
		    try {
			LOG.debug(prefix + "[Started...]");
			boolean result = alcBo.enviarCorreo(this.jobPk, lista,
				alias, false, taskMessages);
			if (result) {
			    taskMessages.add(TAG + " Enviadas " + lista.size()
				    + " operaciones");
			} else {
			    taskMessages.add(TAG
				    + " No se han enviado las operaciones");
			}
		    } finally {
			if (CollectionUtils.isNotEmpty(taskMessages)) {
			    for (final String taskMessage : taskMessages) {
				blog(prefix + "[" + taskMessage + "]");
			    }
			}
			LOG.trace(prefix + "[Finished...]");
		    }
		}
	    }
	} else {
	    LOG.debug(TAG + " No hay tareas programadas para ningún contacto.");
	}
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_OPERACIONES_ENVIO_INFORME;
    }

}
