package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.common.utils.FormatDataUtils;

public class HBB7FieldSetMapper implements FieldSetMapper<HBB7RecordBean> {

  public final static String delimiter = ";";

  public enum HBB7Fields {
    FECHACUADRE("fechaCuadre", 8), INDICADOR_INICIO_FINAL("indicadorInicioFinal", 1), NUORDEN("nuorden", 20);

    private final String text;
    private final int length;

    /**
     * @param text
     */
    private HBB7Fields(final String text, int length) {
      this.text = text;
      this.length = length;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return text;
    }

    public static String[] getFieldNames() {
      HBB7Fields[] fields = HBB7Fields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].text;
      }
      return namesArray;
    }

    public static int[] getHBB7Lengths() {
      HBB7Fields[] fields = HBB7Fields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].length;
      }
      return lengthsArray;
    }
  }

  @Override
  public HBB7RecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    HBB7RecordBean hbb4bean = new HBB7RecordBean();
    hbb4bean.setFechaCuadre(fieldSet.readDate(HBB7Fields.FECHACUADRE.text, FormatDataUtils.DATE_FORMAT));
    hbb4bean.setIndicadorInicioFinal(IndicadorInicioFinalHBB.getIndicador(fieldSet
        .readString(HBB7Fields.INDICADOR_INICIO_FINAL.text)));
    hbb4bean.setNuorden(fieldSet.readString(HBB7Fields.NUORDEN.text));

    return hbb4bean;
  }

}
