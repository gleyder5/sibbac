package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.CodigoErrorDao;
import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.model.CodigoError;
import sibbac.database.bo.AbstractBo;

@Service
public class CodigoErrorBo extends
		AbstractBo<CodigoError, Long, CodigoErrorDao> {

	public CodigoError findByCodigo(String codigo) {
		CodigoError error = null;
		try {
			error = dao.findByCodigo(codigo);
		} catch (Exception e) {
			error = null;
		}

		return error;
	}
	
	
	public CodigoErrorData findByCodigoData(String codigo) {
		CodigoErrorData error = null;
		try {
			error = dao.findByCodigoData(codigo);
		} catch (Exception e) {
			error = null;
		}

		return error;
	}
	
	public List<CodigoError> findAllByGrupoOrderByCodigoAsc(String grupo){
		return dao.findAllByGrupoOrderByCodigoAsc(grupo);
	}
	
	public List<CodigoErrorData> findAllByGrupoOrderByCodigoDataAsc(String grupo){
		return dao.findAllByGrupoOrderByCodigoDataAsc(grupo);
	}
}
