package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0MovimientoCuentaAliasDao;
import sibbac.business.wrappers.database.dao.Tmct0MovimientoCuentaAliasDaoImpl;
import sibbac.business.wrappers.database.data.MovimientoCuentaAliasData;
import sibbac.business.wrappers.database.data.TuplaIsinAlias;
import sibbac.business.wrappers.database.model.Tmct0MovimientoCuentaAlias;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0MovimientoCuentaAliasBo extends
    AbstractBo<Tmct0MovimientoCuentaAlias, Long, Tmct0MovimientoCuentaAliasDao> {

  @Autowired
  private Tmct0MovimientoCuentaAliasDaoImpl daoImpl;

  public Date findMinTradedate() {
    return dao.findMinTradedate();
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByIsinAndAliasAndCuentaLiquidacionAndFecha(String isin, String alias,
      String codigoCuentaLiquidacion, Date fecha) {

    return dao.findAllByFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(fecha, isin, alias, codigoCuentaLiquidacion);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByIsinAndCdcodigocuentaliqAndFecha(String isin,
      String codigoCuentaLiquidacion, Date fecha) {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.DATE, 0);
    c.setTime(fecha);
    Date fechaAnterior = fecha;
    fechaAnterior = c.getTime();
    Date fechaPosterior = new Date(fechaAnterior.getTime());
    c.add(Calendar.DATE, 1);
    fechaPosterior = c.getTime();
    fechaAnterior = DateUtils.setHours(fechaAnterior, 0);
    fechaAnterior = DateUtils.setMinutes(fechaAnterior, 0);
    fechaAnterior = DateUtils.setSeconds(fechaAnterior, 0);
    fechaPosterior = DateUtils.setHours(fechaPosterior, 0);
    fechaPosterior = DateUtils.setMinutes(fechaPosterior, 0);
    fechaPosterior = DateUtils.setSeconds(fechaPosterior, 0);
    return dao.findAllByIsinAndCdcodigocuentaliqAndAuditDateBetween(isin, codigoCuentaLiquidacion, fechaAnterior,
        fechaPosterior);
  }

  public Long getSumTitulosByIsinAndAliasAndCuentaLiquidacionAndFecha(String isin, String alias,
      String codigoCuentaLiquidacion, Date fecha) {
    return dao.findSumTitulosByFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(fecha, isin, alias,
        codigoCuentaLiquidacion);
    // return null;
  }

  public List<Tmct0MovimientoCuentaAlias> findAlByDateRangeAndIsinAndAliasAndCuentaLiquidacion(Date fechaInicio,
      Date fechaFin, String isin, String alias, String codigoCuentaLiquidacion) {

    return dao.findAllByRangeFechaAndIsinAndAliasAndCodigoCuentaLiquidacion(fechaInicio, fechaFin, isin, alias,
        codigoCuentaLiquidacion);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByRangeFechaAndIsinAndAlias(Date fechaInicio, Date fechaFin,
      String isin, String alias, String cdcodigocuentaliq) {

    return dao.findAllByRangeFechaAndIsinAndAlias(fechaInicio, fechaFin, isin, alias, cdcodigocuentaliq);
  }

  public List<?> findAllSumByIsinAndCuentaLiquidacionAndFechaGroupByIsinAndCuentaLiquidacion(
      Map<String, Serializable> filters) {
    return daoImpl.findAllSumByIsinAndCuentaLiquidacionAndFechaGroupByIsinAndCuentaLiquidacion(filters);
  }

  public List<MovimientoCuentaAliasData> findAllMovGroupBySentido(Map<String, Serializable> filter, String sortBy) {
    return daoImpl.findAllMovGroupBySentido(filter, sortBy);
  }

  @Transactional
  public List<MovimientoCuentaAliasData> findAll(Map<String, Serializable> filters, String[] cdOperaciones) {
    List<MovimientoCuentaAliasData> resultList = daoImpl.findAll(filters, cdOperaciones);
    return resultList;
  }

  public List<MovimientoCuentaAliasData> findMovimientosAutomaticosNoGrabados() {
    return dao.findMovimientosAutomaticosNoGrabados();
  }

  @Transactional
  public MovimientoCuentaAliasData findOneSumByDateGroupedByPack(Date date, String isin, String alias,
      String codigoCuentaLiquidacion) {
    Date dateAnt = date;
    dateAnt = DateUtils.setHours(dateAnt, 0);
    dateAnt = DateUtils.setMinutes(dateAnt, 0);
    dateAnt = DateUtils.setSeconds(dateAnt, 0);
    Date datePost = DateUtils.addDays(dateAnt, 1);
    MovimientoCuentaAliasData result = dao.findOneSumByDateGroupedByPack(dateAnt, datePost, isin, alias,
        codigoCuentaLiquidacion);
    return result;
  }

  public List<MovimientoCuentaAliasData> findAllSumByDateAndCodigoCuentaLiqGroupedByIsinCuentaLiq(Date date,
      String codigoCuentaLiquidacion) {
    Date dateAnt = date;
    dateAnt = DateUtils.setHours(dateAnt, 0);
    dateAnt = DateUtils.setMinutes(dateAnt, 0);
    dateAnt = DateUtils.setSeconds(dateAnt, 0);
    Date datePost = DateUtils.addDays(dateAnt, 1);
    List<MovimientoCuentaAliasData> result = dao.findAllSumByDateAndCodigoCuentaLiqGroupedByIsinCuentaLiq(dateAnt,
        datePost, codigoCuentaLiquidacion);
    return result;
  }

  public List<MovimientoCuentaAliasData> findAllSumByIsinAndDateAndCodigoCuentaLiqGroupedByIsinCuentaLiq(String isin,
      Date date, String codigoCuentaLiquidacion) {
    Date dateAnt = date;
    dateAnt = DateUtils.setHours(dateAnt, 0);
    dateAnt = DateUtils.setMinutes(dateAnt, 0);
    dateAnt = DateUtils.setSeconds(dateAnt, 0);
    Date datePost = DateUtils.addDays(dateAnt, 1);
    List<MovimientoCuentaAliasData> result = dao.findAllSumByIsinAndDateAndCodigoCuentaLiqGroupedByIsinCuentaLiq(isin,
        dateAnt, datePost, codigoCuentaLiquidacion);
    return result;
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByFecha(Date fecha) {

    return dao.findAllBySettlementdate(fecha);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByCdaliass(String cdaliass) {

    return dao.findAllByCdaliass(cdaliass);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllBySettlementdateGreaterThanEqualAndCdaliassInAndIsinIn(
      Date settlementdate, List<String> cdaliass, List<String> isin) {

    return dao.findAllBySettlementdateGreaterThanEqualAndCdaliassInAndIsinIn(settlementdate, cdaliass, isin);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllBySettlementdateGreaterThanEqualAndCdcodigocuentaliqAndCdaliassInAndIsinIn(
      Date settlementdate, String cdcodigocuentaliq, List<String> cdaliass, List<String> isin) {

    return dao.findAllBySettlementdateGreaterThanEqualAndCdcodigocuentaliqAndCdaliassInAndIsinIn(settlementdate,
        cdcodigocuentaliq, cdaliass, isin);
  }

  public List<MovimientoCuentaAliasData> findAll(Map<String, Serializable> filtros, String sortBy, String[] operaciones) {
    List<MovimientoCuentaAliasData> resultList = daoImpl.findAll(filtros, sortBy, operaciones);
    return resultList;
  }

  /**
   * Extrae la cantidad de títulos pendientes de liquidar
   * @param fecha
   * @param isin
   * @param alias
   * @param cdcodigocuentaliq
   * @return
   */
  public List<Object[]> findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionNoLiquidados(Date fecha,
      String isin, String alias, String cdcodigocuentaliq) {
    return dao.findSumTitulosByTradedateLteAndIsinAndAliasAndCodigoCuentaLiquidacionNoLiquidados(fecha, isin, alias,
        cdcodigocuentaliq);
  }

  /**
   * Extrae la cantidad de títulos LIQUIDADOS
   * @param fecha
   * @param isin
   * @param alias
   * @param cdcodigocuentaliq
   * @return
   */
  public List<Object[]> findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(Date fecha,
      String isin, String alias, String cdcodigocuentaliq) {
    return dao.findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(fecha, isin, alias,
        cdcodigocuentaliq);
  }

  public List<Object[]> findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
      Date fecha, String isin, String alias, String cdcodigocuentaliq, String[] listCdoperacion) {
    return dao.findSumTitulosByTradedateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(fecha,
        isin, alias, cdcodigocuentaliq, listCdoperacion);
  }

  public List<Object[]> findSumTitulosBySettlementdateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
      Date fecha, String isin, String alias, String cdcodigocuentaliq, String[] listCdoperacion) {
    return dao.findSumTitulosBySettlementdateLtAndIsinAndAliasAndCodigoCuentaAndCdoperacionInLiquidacionLiquidados(
        fecha, isin, alias, cdcodigocuentaliq, listCdoperacion);
  }

  public List<Object[]> findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
      Date fechaIni, Date fechaFin, String isin, String alias, String cdcodigocuentaliq, String[] listCdoperacion) {
    return dao
        .findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
            fechaIni, fechaFin, isin, alias, cdcodigocuentaliq, listCdoperacion);
  }

  public List<Object[]> findSumTitulosBySettlementdateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
      Date fechaIni, Date fechaFin, String isin, String alias, String cdcodigocuentaliq, String[] listCdoperacion) {
    return dao
        .findSumTitulosBySettlementdateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionAndCdoperacionInLiquidados(
            fechaIni, fechaFin, isin, alias, cdcodigocuentaliq, listCdoperacion);
  }

  /**
   * Extrae la cantidad de títulos LIQUIDADOS
   * @param fecha
   * @param isin
   * @param alias
   * @param cdcodigocuentaliq
   * @return
   */
  public List<Object[]> findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      Date fechaIni, Date fechaFin, String isin, String alias, String cdcodigocuentaliq) {
    return dao.findSumTitulosByTradedateGteAndTradedatedateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
        fechaIni, fechaFin, isin, alias, cdcodigocuentaliq);
  }

  /**
   * Extrae la cantidad de títulos liquidados menor que la fecha...
   * @param settlementDateLt
   * @param isin
   * @param alias
   * @param cdcodigocuentaliq
   * @return
   */
  public List<Object[]> findSumTitulosBySettlementdateteLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      Date settlementDateLt, String isin, String alias, String cdcodigocuentaliq) {
    return dao.findSumTitulosBySettlementdateteLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(settlementDateLt,
        isin, alias, cdcodigocuentaliq);
  }

  /**
   * Extrae la cantidad de títulos lidados entre dos fechas dadas
   * @param fechaIni gte
   * @param fechaFin lt
   * @param isin
   * @param alias
   * @param cdcodigocuentaliq
   * @return
   */
  public List<Object[]> findSumTitulosBySettlementdateGteAndSettlementdateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
      Date fechaIni, Date fechaFin, String isin, String alias, String cdcodigocuentaliq) {
    return dao.findSumTitulosBySettlementdateGteAndSettlementdateLtAndIsinAndAliasAndCodigoCuentaLiquidacionLiquidados(
        fechaIni, fechaFin, isin, alias, cdcodigocuentaliq);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiq(Date fechadesde,
      String codigoCuentaLiquidacion) {
    return this.dao.findAllByFechaLiquidacionAndCodigoCuentaLiq(fechadesde, codigoCuentaLiquidacion);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinal(Date fechadesde,
      Date fechahasta, String codigoCuentaLiquidacion) {
    return this.dao.findAllByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinal(fechadesde, fechahasta,
        codigoCuentaLiquidacion);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiqAndIsin(Date fechadesde,
      String codigoCuentaLiquidacion, String isin) {
    return this.dao.findAllByFechaLiquidacionAndCodigoCuentaLiqAndIsin(fechadesde, codigoCuentaLiquidacion, isin);
  }

  public List<Tmct0MovimientoCuentaAlias> findAllByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinal(
      Date fechadesde, Date fechahasta, String codigoCuentaLiquidacion, String isin) {
    return this.dao.findAllByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinal(fechadesde, fechahasta,
        codigoCuentaLiquidacion, isin);
  }

  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinal(Date fechadesde,
      Date fechahasta, String codigoCuentaLiquidacion) {
    return this.dao.findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinal(fechadesde, fechahasta,
        codigoCuentaLiquidacion);
  }

  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinalAndTitulosNotZero(
      Date fechadesde, Date fechahasta, String codigoCuentaLiquidacion) {
    return this.dao.findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndFechaFinalAndTitulosNotZero(fechadesde,
        fechahasta, codigoCuentaLiquidacion);
  }

  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinal(
      Date fechadesde, Date fechahasta, String codigoCuentaLiquidacion, String isin) {
    return this.dao.findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinal(fechadesde, fechahasta,
        codigoCuentaLiquidacion, isin);
  }

  public List<MovimientoCuentaAliasData> findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinalAndTitulosNotZero(
      Date fechadesde, Date fechahasta, String codigoCuentaLiquidacion, String isin) {
    return this.dao.findAllDataByFechaLiquidacionAndCodigoCuentaLiqAndIsinAndFechaFinalAndTitulosNotZero(fechadesde,
        fechahasta, codigoCuentaLiquidacion, isin);
  }

  public Date findLastUpdateDateTimeInOperaciones(String[] operaciones) {
    java.util.Date date = dao.findLastUpdateDateTimeInOperaciones(operaciones);
    if (date == null) {
      date = new java.util.Date();
    }
    return date;
  }

  // Estos sirven para que si se mete un movimiento nuevo que no esta en saldos
  // tambien se busque por el
  public List<TuplaIsinAlias> getTuplas(Date fecha) {
    return this.dao.getTuplas(fecha);
  }

  public List<TuplaIsinAlias> getTuplasByIsin(Date fecha, String isin) {
    return this.dao.getTuplasByIsin(fecha, isin);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsin(Date fecha, String isin) {
    return this.dao.getTuplasByNotIsin(fecha, isin);
  }

  public List<TuplaIsinAlias> getTuplasByAlias(Date fecha, String alias) {
    return this.dao.getTuplasByAlias(fecha, alias);
  }

  public List<TuplaIsinAlias> getTuplasByNotAlias(Date fecha, String alias) {
    return this.dao.getTuplasByNotAlias(fecha, alias);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByIsinAndAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByNotIsinAndNotAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByIsinAndNotAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndAlias(Date fecha, String isin, String alias) {
    return this.dao.getTuplasByNotIsinAndAlias(fecha, isin, alias);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndNotAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndAliasAndCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndAliasAndCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndNotAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndAliasAndNotCuenta(Date fecha, String isin, String alias,
      String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndAliasAndNotCuenta(fecha, isin, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndNotCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndNotCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByIsinAndNotCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByIsinAndNotCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotIsinAndCuenta(Date fecha, String isin, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotIsinAndCuenta(fecha, isin, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByCuenta(Date fecha, String cdcodigocuentaliq) {
    return this.dao.getTuplasByCuenta(fecha, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotCuenta(Date fecha, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotCuenta(fecha, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByAliasAndCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByAliasAndCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotAliasAndNotCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotAliasAndNotCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByAliasAndNotCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByAliasAndNotCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public List<TuplaIsinAlias> getTuplasByNotAliasAndCuenta(Date fecha, String alias, String cdcodigocuentaliq) {
    return this.dao.getTuplasByNotAliasAndCuenta(fecha, alias, cdcodigocuentaliq);
  }

  public int updateGuardadoSaldo(MovimientoCuentaAliasData data, boolean guardado) {
    return dao.updateGuardadoSaldo(data.getCdalias(), data.getIsin(), data.getCdcodigocuentaliq(),
        data.getSettlementDate(), guardado);
  }

  public List<Tmct0MovimientoCuentaAlias> findByRefMovimiento(String refMovimiento) {
    return dao.findByRefMovimiento(refMovimiento);
  }

}
