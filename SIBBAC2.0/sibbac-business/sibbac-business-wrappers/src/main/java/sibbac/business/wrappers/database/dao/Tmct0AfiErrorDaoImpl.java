package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.common.DTOUtils;
import sibbac.business.wrappers.common.fileReader.ValidationHelper.CodError;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.model.Tmct0AfiError;
import sibbac.business.wrappers.database.model.Tmct0alcId;

@Repository
public class Tmct0AfiErrorDaoImpl {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0AfiErrorDaoImpl.class);

  @PersistenceContext
  private EntityManager em;

  @SuppressWarnings("unchecked")
  public List<TitularesDTO> findAllDTO(Date fechaDesde,
                                       Date fechaHasta,
                                       Character mercadoNacInt,
                                       Character sentido,
                                       String nudnicif,
                                       String codError,
                                       String nbclient,
                                       String nbclient1,
                                       String nbclient2,
                                       String nbvalors,
                                       Character tpnactit,
                                       String isin,
                                       String alias,
                                       Character tipodocumento,
                                       String paisres,
                                       String tpdomici,
                                       String nbdomici,
                                       String nudomici,
                                       String provincia,
                                       String codpostal,
                                       String nureford,
                                       Short hasErrors) {
    final StringBuilder select = new StringBuilder(
                                                   "select afiError.ID id, alo.NUCNFCLT NUCNFCLT, alo.NBOOKING NBOOKING, afiError.NUCNFLIQ, afiError.NBCLIENT, afiError.NBCLIENT1, afiError.NBCLIENT2, afiError.TPIDENTI, afiError.TPSOCIED, afiError.NUDNICIF, afiError.CCV, afiError.CDDEPAIS, afiError.TPNACTIT, afiError.TPDOMICI, afiError.NBDOMICI, afiError.NUDOMICI, afiError.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, afiError.CDPOSTAL, afiError.NBCIUDAD, afiError.NBPROVIN, afiError.TPTIPREP, afiError.CDNACTIT, afiError.PARTICIP, afiError.NUORDEN, ord.NUTITEJE  ");
    final StringBuilder from = new StringBuilder(" from TMCT0_AFI_ERROR afiError ");
    final StringBuilder where = new StringBuilder(" ");
    boolean whereEmpty = true;
    boolean joinAlc = false;
    final String fechaDesdeParamName = "feejeliqDesde";
    if (fechaDesde != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq >=:" + fechaDesdeParamName);
    }

    final String fechaHastaParamName = "feejeliqHasta";
    if (fechaHasta != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq <=:" + fechaHastaParamName);
    }

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER <=:" + sentidoParamName);
    }

    final String isinParamName = "isin";
    if (isin != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDISIN =:" + isinParamName);
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nudnicif =:" + nudnicifParamName);
    }

    final String nbvalorsParamName = "nbvalors";
    if (nbvalors != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.NBVALORS=:" + nbvalorsParamName);
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.NUREFORD=:" + nurefordParamName);
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nbclient =:" + nbclientParamName);
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nbclient1 =:" + nbclient1ParamName);
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nbclient2 =:" + nbclient2ParamName);
    }

    final String aliasParamName = "alias";
    if (alias != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ord.CDCUENTA =:" + aliasParamName);
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afiError.TPIDENTI = '' OR afiError.TPIDENTI IS NULL) ");
      } else {
        where.append("afiError.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String codErrorParamName = "codError";
    if (codError != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("codigo.CODIGO=:" + codErrorParamName);
      from.append(" INNER JOIN TMCT0_AFI_ERROR_CODIGO_ERROR afiErrorCodigo ON afiError.ID = afiErrorCodigo.ID_AFI_ERROR "
                  + " INNER JOIN TMCT0_CODIGO_ERROR codigo ON codigo.ID = afiErrorCodigo.ID_CODIGO_ERROR ");

    }

    from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afiError.NUCNFCLT AND alo.NBOOKING = afiError.NBOOKING AND alo.NUORDEN = afiError.NUORDEN ");

    // Si se buscan los que no tienen ALO asociado
    if (hasErrors == 3) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append(" alo.nbooking IS null ");
    } else {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append(" alo.nbooking IS NOT null ");

    }

    from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");

    if (joinAlc) {
      from.append(" INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT  AND alc.nucnfliq = afiError.nucnfliq ");
    }
    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());

    setParameters(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, nbvalors, codError,
                  mercadoNacInt, sentido, isin, alias, null, null, null, null, null, null, null, null, null, nureford,
                  null, tipodocumento, null, null, null, fechaDesdeParamName, fechaHastaParamName, nudnicifParamName,
                  nbclientParamName, nbclient1ParamName, nbclient2ParamName, nbvalorsParamName, codErrorParamName,
                  mercadoNacIntParamName, sentidoParamName, isinParamName, aliasParamName, null, null, null, null,
                  null, null, null, null, null, nurefordParamName, null, tipodocumentoParamName, null, null, null);

    final List<Object> resultList = query.getResultList();
    final List<TitularesDTO> titularesList = new ArrayList<TitularesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TitularesDTO titular = DTOUtils.convertObjectToTitularesDTO((Object[]) obj);
        titular.setAfiError(true);
        titularesList.add(titular);
      }
    }

    return titularesList;
  }

  private void setParameters(Query query,
                             Date fechaDesde,
                             Date fechaHasta,
                             String nudnicif,
                             String nbclient,
                             String nbclient1,
                             String nbclient2,
                             String nbvalors,
                             String codError,
                             Character mercadoNacInt,
                             Character sentido,
                             String isin,
                             String alias,
                             String paisres,
                             String provincia,
                             String codpostal,
                             Character tpnactit,
                             String ciudad,
                             Character tptiprep,
                             Character tpsocied,
                             BigDecimal particip,
                             String ccv,
                             String nureford,
                             String nacionalidad,
                             Character tipodocumento,
                             String tpdomici,
                             String nbdomici,
                             String nudomici,
                             String fechaDesdeParamName,
                             String fechaHastaParamName,
                             String nudnicifParamName,
                             String nbclientParamName,
                             String nbclient1ParamName,
                             String nbclient2ParamName,
                             String nbvalorsParamName,
                             String codErrorParamName,
                             String mercadoNacIntParamName,
                             String sentidoParamName,
                             String isinParamName,
                             String aliasParamName,
                             String paisresParamName,
                             String provinciaParamName,
                             String codpostalParamName,
                             String tpnactitParamName,
                             String ciudadParamName,
                             String tptiprepParamName,
                             String tpsociedParamName,
                             String participParamName,
                             String ccvParamName,
                             String nurefordParamName,
                             String nacionalidadParamName,
                             String tipodocumentoParamName,
                             String tpdomiciParamName,
                             String nbdomiciParamName,
                             String nudomiciParamName) {
    if (fechaDesde != null) {
      query.setParameter(fechaDesdeParamName, fechaDesde, TemporalType.DATE);
    }
    if (fechaHasta != null) {
      query.setParameter(fechaHastaParamName, fechaHasta, TemporalType.DATE);
    }
    if (nudnicif != null && !nudnicif.equals("<null>")) {
      query.setParameter(nudnicifParamName, nudnicif);
    }
    if (nbclient != null && !nbclient.equals("<null>")) {
      query.setParameter(nbclientParamName, nbclient);
    }
    if (nbclient1 != null && !nbclient1.equals("<null>")) {
      query.setParameter(nbclient1ParamName, nbclient1);
    }
    if (nbclient2 != null && !nbclient2.equals("<null>")) {
      query.setParameter(nbclient2ParamName, nbclient2);
    }
    if (nbvalors != null) {
      query.setParameter(nbvalorsParamName, nbvalors);
    }
    if (codError != null) {
      query.setParameter(codErrorParamName, codError);
    }
    if (mercadoNacInt != null) {
      query.setParameter(mercadoNacIntParamName, mercadoNacInt);
    }
    if (sentido != null) {
      query.setParameter(sentidoParamName, sentido);
    }
    if (isin != null) {
      query.setParameter(isinParamName, isin);
    }
    if (alias != null) {
      query.setParameter(aliasParamName, alias);
    }
    if (paisres != null && !paisres.equals("<null>")) {
      query.setParameter(paisresParamName, paisres);
    }
    if (provincia != null && !provincia.equals("<null>")) {
      query.setParameter(provinciaParamName, provincia);
    }
    if (codpostal != null && !codpostal.equals("<null>")) {
      query.setParameter(codpostalParamName, codpostal);
    }
    if (tpnactit != null && !tpnactit.equals('-')) {
      query.setParameter(tpnactitParamName, tpnactit);
    }
    if (ciudad != null && !ciudad.equals("<null>")) {
      query.setParameter(ciudadParamName, ciudad);
    }
    if (tptiprep != null && !tptiprep.equals('-')) {
      query.setParameter(tptiprepParamName, tptiprep);
    }
    if (tpsocied != null && !tpsocied.equals('-')) {
      query.setParameter(tpsociedParamName, tpsocied);
    }
    if (particip != null && particip.compareTo(new BigDecimal("-1")) != 0) {
      query.setParameter(participParamName, particip);
    }
    if (ccv != null && !ccv.equals("<null>")) {
      query.setParameter(ccvParamName, ccv);
    }
    if (nacionalidad != null && !nacionalidad.equals("<null>")) {
      query.setParameter(nacionalidadParamName, nacionalidad);
    }
    if (nureford != null && !nureford.equals("<null>")) {
      query.setParameter(nurefordParamName, nureford);
    }
    if (tipodocumento != null && !tipodocumento.equals('-')) {
      query.setParameter(tipodocumentoParamName, tipodocumento);
    }
    if (tpdomici != null && !tpdomici.equals("<null>")) {
      query.setParameter(tpdomiciParamName, tpdomici);
    }
    if (nbdomici != null && !nbdomici.equals("<null>")) {
      query.setParameter(nbdomiciParamName, nbdomici);
    }
    if (nudomici != null && !nudomici.equals("<null>")) {
      query.setParameter(nudomiciParamName, nudomici);
    }
  }

  private void setParametersWithVectors(Query query,
                                        Date fechaDesde,
                                        Date fechaHasta,
                                        List<String> nudnicif,
                                        List<String> nbclient,
                                        List<String> nbclient1,
                                        List<String> nbclient2,
                                        String codError,
                                        Character mercadoNacInt,
                                        Character sentido,
                                        List<String> isin,
                                        List<String> alias,
                                        String paisres,
                                        String provincia,
                                        String codpostal,
                                        Character tpnactit,
                                        String ciudad,
                                        Character tptiprep,
                                        Character tpsocied,
                                        BigDecimal particip,
                                        String ccv,
                                        String nureford,
                                        String nacionalidad,
                                        Character tipodocumento,
                                        String tpdomici,
                                        String nbdomici,
                                        String nudomici,
                                        String nbooking,
                                        String fechaDesdeParamName,
                                        String fechaHastaParamName,
                                        String nudnicifParamName,
                                        String nbclientParamName,
                                        String nbclient1ParamName,
                                        String nbclient2ParamName,
                                        String codErrorParamName,
                                        String mercadoNacIntParamName,
                                        String sentidoParamName,
                                        String isinParamName,
                                        String aliasParamName,
                                        String paisresParamName,
                                        String provinciaParamName,
                                        String codpostalParamName,
                                        String tpnactitParamName,
                                        String ciudadParamName,
                                        String tptiprepParamName,
                                        String tpsociedParamName,
                                        String participParamName,
                                        String ccvParamName,
                                        String nurefordParamName,
                                        String nacionalidadParamName,
                                        String tipodocumentoParamName,
                                        String tpdomiciParamName,
                                        String nbdomiciParamName,
                                        String nudomiciParamName,
                                        String nbookingParamName,
                                        Integer firstRow,
                                        String firstRowParamName,
                                        Integer lastRow,
                                        String lastRowParamName) {

    if (firstRow != null) {
      query.setParameter(firstRowParamName, firstRow);
    }
    if (lastRow != null) {
      query.setParameter(lastRowParamName, lastRow);
    }

    if (fechaDesde != null) {
      query.setParameter(fechaDesdeParamName, fechaDesde, TemporalType.DATE);
    }
    if (fechaHasta != null) {
      query.setParameter(fechaHastaParamName, fechaHasta, TemporalType.DATE);
    }
    if (nudnicif != null && nudnicif.size() > 0) {
      query.setParameter(nudnicifParamName, nudnicif);
    }
    if (nbclient != null && nbclient.size() > 0) {
      query.setParameter(nbclientParamName, nbclient);
    }
    if (nbclient1 != null && nbclient1.size() > 0) {
      query.setParameter(nbclient1ParamName, nbclient1);
    }
    if (nbclient2 != null && nbclient2.size() > 0) {
      query.setParameter(nbclient2ParamName, nbclient2);
    }
    if (codError != null) {
      query.setParameter(codErrorParamName, codError);
    }
    if (mercadoNacInt != null) {
      query.setParameter(mercadoNacIntParamName, mercadoNacInt);
    }
    if (sentido != null) {
      query.setParameter(sentidoParamName, sentido);
    }
    if (nbooking != null) {
      query.setParameter(nbookingParamName, nbooking);
    }
    if (isin != null && isin.size() > 0) {
      query.setParameter(isinParamName, isin);
    }
    if (alias != null && alias.size() > 0) {
      query.setParameter(aliasParamName, alias);
    }
    if (paisres != null && !paisres.equals("<null>")) {
      query.setParameter(paisresParamName, paisres);
    }
    if (provincia != null && !provincia.equals("<null>")) {
      query.setParameter(provinciaParamName, provincia);
    }
    if (codpostal != null && !codpostal.equals("<null>")) {
      query.setParameter(codpostalParamName, codpostal);
    }
    if (tpnactit != null && !tpnactit.equals('-')) {
      query.setParameter(tpnactitParamName, tpnactit);
    }
    if (ciudad != null && !ciudad.equals("<null>")) {
      query.setParameter(ciudadParamName, ciudad);
    }
    if (tptiprep != null && !tptiprep.equals('-')) {
      query.setParameter(tptiprepParamName, tptiprep);
    }
    if (tpsocied != null && !tpsocied.equals('-')) {
      query.setParameter(tpsociedParamName, tpsocied);
    }
    if (particip != null && particip.compareTo(new BigDecimal("-1")) != 0) {
      query.setParameter(participParamName, particip);
    }
    if (ccv != null && !ccv.equals("<null>")) {
      query.setParameter(ccvParamName, ccv);
    }
    if (nacionalidad != null && !nacionalidad.equals("<null>")) {
      query.setParameter(nacionalidadParamName, nacionalidad);
    }
    if (nureford != null && !nureford.equals("<null>")) {
      query.setParameter(nurefordParamName, nureford);
    }
    if (tipodocumento != null && !tipodocumento.equals('-')) {
      query.setParameter(tipodocumentoParamName, tipodocumento);
    }
    if (tpdomici != null && !tpdomici.equals("<null>")) {
      query.setParameter(tpdomiciParamName, tpdomici);
    }
    if (nbdomici != null && !nbdomici.equals("<null>")) {
      query.setParameter(nbdomiciParamName, nbdomici);
    }
    if (nudomici != null && !nudomici.equals("<null>")) {
      query.setParameter(nudomiciParamName, nudomici);
    }
  }

  @SuppressWarnings("unchecked")
  public List<BigInteger> findAllIds(Date fechaDesde,
                                     Date fechaHasta,
                                     Character mercadoNacInt,
                                     Character sentido,
                                     String nudnicif,
                                     String codError,
                                     String nbclient,
                                     String nbclient1,
                                     String nbclient2,
                                     String nbvalors,
                                     Character tpnactit,
                                     String isin,
                                     String alias,
                                     Character tipodocumento,
                                     String paisres,
                                     String tpdomici,
                                     String nbdomici,
                                     String nudomici,
                                     String provincia,
                                     String codpostal,
                                     String ciudad,
                                     Character tptiprep,
                                     Character tpsocied,
                                     BigDecimal particip,
                                     String ccv,
                                     String nureford,
                                     String nacionalidad) {
    final StringBuilder select = new StringBuilder("select afiError.ID id ");
    final StringBuilder from = new StringBuilder(" from TMCT0_AFI_ERROR afiError ");
    final StringBuilder where = new StringBuilder(" ");
    boolean whereEmpty = true;
    boolean joinAlc = false;
    final String fechaDesdeParamName = "feejeliqDesde";
    if (fechaDesde != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq >=:" + fechaDesdeParamName);
    }

    final String fechaHastaParamName = "feejeliqHasta";
    if (fechaHasta != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq <=:" + fechaHastaParamName);
    }

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER <=:" + sentidoParamName);
    }

    final String isinParamName = "isin";
    if (isin != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDISIN =:" + isinParamName);
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudnicif.equals("<null>")) {
        where.append("(afiError.nudnicif = '' OR afiError.nudnicif IS NULL) ");
      } else {
        where.append("afiError.nudnicif =:" + nudnicifParamName);
      }
    }

    final String nbvalorsParamName = "nbvalors";
    if (nbvalors != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.NBVALORS=:" + nbvalorsParamName);
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }

      if (nbclient.equals("<null>")) {
        where.append("(afiError.nbclient = '' OR afiError.nbclient IS NULL) ");
      } else {
        where.append("afiError.nbclient =:" + nbclientParamName);
      }
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient1.equals("<null>")) {
        where.append("(afiError.nbclient1 = '' OR afiError.nbclient1 IS NULL) ");
      } else {
        where.append("afiError.nbclient1 =:" + nbclient1ParamName);
      }
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient2.equals("<null>")) {
        where.append("(afiError.nbclient2 = '' OR afiError.nbclient2 IS NULL) ");
      } else {
        where.append("afiError.nbclient2 =:" + nbclient2ParamName);
      }
    }

    final String aliasParamName = "alias";
    if (alias != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ord.CDCUENTA =:" + aliasParamName);
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals('-')) {
        where.append("(afiError.TPIDENTI = '' OR afiError.TPIDENTI IS NULL) ");
      } else {
        where.append("afiError.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String tpnactitParamName = "tpnactit";
    if (tpnactit != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpnactit.equals('-')) {
        where.append("(afiError.TPNACTIT = '' OR afiError.TPNACTIT IS NULL) ");
      } else {
        where.append("afiError.TPNACTIT=:" + tpnactitParamName);
      }
    }

    final String paisresParamName = "paisres";
    if (paisres != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (paisres.equals("<null>")) {
        where.append("(afiError.CDDEPAIS = '' OR afiError.CDDEPAIS IS NULL) ");
      } else {
        where.append("afiError.CDDEPAIS =:" + paisresParamName);
      }
    }

    final String provinciaParamName = "provincia";
    if (provincia != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (provincia.equals("<null>")) {
        where.append("(afiError.NBPROVIN = '' OR afiError.NBPROVIN IS NULL) ");
      } else {
        where.append("afiError.NBPROVIN =:" + provinciaParamName);
      }
    }

    final String codpostalParamName = "codpostal";
    if (codpostal != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codpostal.equals("<null>")) {
        where.append("(afiError.CDPOSTAL = '' OR afiError.CDPOSTAL IS NULL) ");
      } else {
        where.append("afiError.CDPOSTAL =:" + codpostalParamName);
      }
    }

    final String tpdomiciParamName = "tpdomici";
    if (tpdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpdomici.equals("<null>")) {
        where.append("(afiError.TPDOMICI = '' OR afiError.TPDOMICI IS NULL) ");
      } else {
        where.append("afiError.TPDOMICI =:" + tpdomiciParamName);
      }
    }

    final String nbdomiciParamName = "nbdomici";
    if (nbdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbdomici.equals("<null>")) {
        where.append("(afiError.NBDOMICI = '' OR afiError.NBDOMICI IS NULL) ");
      } else {
        where.append("afiError.NBDOMICI =:" + nbdomiciParamName);
      }
    }

    final String nudomiciParamName = "nudomici";
    if (nudomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudomici.equals("<null>")) {
        where.append("(afiError.NUDOMICI = '' OR afiError.NUDOMICI IS NULL) ");
      } else {
        where.append("afiError.NUDOMICI =:" + nudomiciParamName);
      }
    }

    final String ciudadParamName = "ciudad";
    if (ciudad != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (ciudad.equals("<null>")) {
        where.append("(afiError.NBCIUDAD = '' OR afiError.NBCIUDAD IS NULL) ");
      } else {
        where.append("afiError.NBCIUDAD =:" + ciudadParamName);
      }
    }

    final String tptiprepParamName = "tptiprep";
    if (tptiprep != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tptiprep.equals('-')) {
        where.append("(afiError.TPTIPREP = '' OR afiError.TPTIPREP IS NULL) ");
      } else {
        where.append("afiError.TPTIPREP =:" + tptiprepParamName);
      }
    }

    final String tpsociedParamName = "tpsocied";
    if (tpsocied != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpsocied.equals('-')) {
        where.append("(afiError.TPSOCIED = '' OR afiError.TPSOCIED IS NULL) ");
      } else {
        where.append("afiError.TPSOCIED =:" + tpsociedParamName);
      }
    }

    final String participParamName = "particip";
    if (particip != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (particip.compareTo(new BigDecimal("-1")) == 0) {
        where.append("(afiError.PARTICIP = '' OR afiError.PARTICIP IS NULL) ");
      } else {
        where.append("afiError.PARTICIP =:" + participParamName);
      }
    }

    final String ccvParamName = "ccv";
    if (ccv != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (ccv.equals("<null>")) {
        where.append("(afiError.CCV = '' OR afiError.CCV IS NULL) ");
      } else {
        where.append("afiError.CCV =:" + ccvParamName);
      }
    }

    final String nacionalidadParamName = "nacionalidad";
    if (nacionalidad != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nacionalidad.equals("<null>")) {
        where.append("(afiError.CDNACTIT = '' OR afiError.CDNACTIT IS NULL) ");
      } else {
        where.append("afiError.CDNACTIT =:" + nacionalidadParamName);
      }
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.equals("<null>")) {
        where.append("(afiError.NUREFORD = '' OR afiError.NUREFORD IS NULL) ");
      } else {
        where.append("afiError.NUREFORD =:" + nurefordParamName);
      }
    }

    final String codErrorParamName = "codError";
    if (codError != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("codigo.CODIGO=:" + codErrorParamName);
      from.append(" INNER JOIN TMCT0_AFI_ERROR_CODIGO_ERROR afiErrorCodigo ON afiError.ID = afiErrorCodigo.ID_AFI_ERROR "
                  + " INNER JOIN TMCT0_CODIGO_ERROR codigo ON codigo.ID = afiErrorCodigo.ID_CODIGO_ERROR ");

    }

    from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afiError.NUCNFCLT AND alo.NBOOKING = afiError.NBOOKING AND alo.NUORDEN = afiError.NUORDEN ");

    from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");

    if (joinAlc) {
      from.append(" INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT  AND alc.nucnfliq = afiError.nucnfliq ");
    }
    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());

    setParameters(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, nbvalors, codError,
                  mercadoNacInt, sentido, isin, alias, paisres, provincia, codpostal, tpnactit, ciudad, tptiprep,
                  tpsocied, particip, ccv, nureford, nacionalidad, tipodocumento, tpdomici, nbdomici, nudomici,
                  fechaDesdeParamName, fechaHastaParamName, nudnicifParamName, nbclientParamName, nbclient1ParamName,
                  nbclient2ParamName, nbvalorsParamName, codErrorParamName, mercadoNacIntParamName, sentidoParamName,
                  isinParamName, aliasParamName, paisresParamName, provinciaParamName, codpostalParamName,
                  tpnactitParamName, ciudadParamName, tptiprepParamName, tpsociedParamName, participParamName,
                  ccvParamName, nurefordParamName, nacionalidadParamName, tipodocumentoParamName, tpdomiciParamName,
                  nbdomiciParamName, nudomiciParamName);

    final List<BigInteger> resultList = (List<BigInteger>) query.getResultList();

    return resultList;

  }

  public List<Tmct0AfiError> findAllWithNoErrors() {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError LEFT JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(" WHERE err.id = null OR err.codigo = 'COD022' ");
    String sort = " ORDER BY afiError.nureford ";
    StringBuilder querySB = new StringBuilder(select).append(from);
    // se construye la query segun los filtros
    querySB.append(where).append(sort);
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findAllWithNurefordAndNoErrors() {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError LEFT JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(
                                            " WHERE (afiError.tmct0alo.id.nucnfclt='' OR afiError.tmct0alo.id.nucnfclt IS NULL) AND (err is null OR err.codigo in ('COD019', 'COD020', 'COD021','COD022' )) ");
    String sort = " ORDER BY afiError.nureford ";
    StringBuilder querySB = new StringBuilder(select).append(from);
    // se construye la query segun los filtros
    querySB.append(where).append(sort);
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findAllWithAlcAndNoErrors() {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError JOIN afiError.tmct0alo alo LEFT JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(
                                            " WHERE afiError.nucnfliq != 0 AND afiError.nucnfliq IS NOT NULL AND (err is null OR err.codigo in ('COD019', 'COD020', 'COD021','COD022' )) ");
    String sort = " ORDER BY afiError.tmct0alo.id.nuorden, afiError.tmct0alo.id.nbooking, afiError.tmct0alo.id.nucnfclt, afiError.nucnfliq  ";
    StringBuilder querySB = new StringBuilder(select).append(from);
    // se construye la query segun los filtros
    querySB.append(where).append(sort);
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    
    return resultList;
  }

  public List<Tmct0AfiError> findAllWithAloAndNoErrors() {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError JOIN afiError.tmct0alo alo LEFT JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(
                                            " WHERE (afiError.nucnfliq IS NULL OR afiError.nucnfliq=0) AND (err is null OR err.codigo in ('COD019', 'COD020', 'COD021','COD022' )) ");
    String sort = " ORDER BY afiError.tmct0alo.id.nuorden, afiError.tmct0alo.id.nbooking, afiError.tmct0alo.id.nucnfclt ";
    StringBuilder querySB = new StringBuilder(select).append(from);
    // se construye la query segun los filtros
    querySB.append(where).append(sort);
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findPartnersWithErrorsByNureford(String nureford) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(" WHERE err.codigo != 'COD022' AND afiError.nureford = :nureford");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nureford", nureford);
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findPartnersWithErrorsByAlcId(Tmct0alcId alcId) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(
                                            " WHERE err.codigo != '"
                                                + CodError.ERROROTROTIT
                                                + "' AND afiError.tmct0alo.id.nuorden = :nuorden AND afiError.tmct0alo.id.nbooking = :nbooking AND afiError.tmct0alo.id.nucnfclt = :nucnfclt AND afiError.nucnfliq = :nucnfliq ");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nuorden", alcId.getNuorden());
      query.setParameter("nbooking", alcId.getNbooking());
      query.setParameter("nucnfclt", alcId.getNucnfclt());
      query.setParameter("nucnfliq", alcId.getNucnfliq());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findPartnersWithErrorsByAloId(Tmct0aloId aloId) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError JOIN afiError.errors err ";
    StringBuilder where = new StringBuilder(
                                            " WHERE err.codigo != '"
                                                + CodError.ERROROTROTIT
                                                + "' AND afiError.tmct0alo.id.nuorden = :nuorden AND "
                                                + "afiError.tmct0alo.id.nbooking = :nbooking AND afiError.tmct0alo.id.nucnfclt = :nucnfclt AND "
                                                + "(afiError.nucnfliq IS NULL OR afierror.nucnfliq=0) ");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nuorden", aloId.getNuorden());
      query.setParameter("nbooking", aloId.getNbooking());
      query.setParameter("nucnfclt", aloId.getNucnfclt());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findPartnersByAlcId(Tmct0alcId alcId, Long afiErrorId) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError ";
    StringBuilder where = new StringBuilder(
                                            " WHERE afiError.id != :afiErrorId AND afiError.tmct0alo.id.nuorden = :nuorden AND "
                                                + "afiError.tmct0alo.id.nbooking = :nbooking AND afiError.tmct0alo.id.nucnfclt = :nucnfclt AND "
                                                + "afiError.nucnfliq=:nucnfliq ) ");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nuorden", alcId.getNuorden());
      query.setParameter("nbooking", alcId.getNbooking());
      query.setParameter("nucnfclt", alcId.getNucnfclt());
      query.setParameter("nucnfliq", alcId.getNucnfliq());
      query.setParameter("afiErrorId", afiErrorId);
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findGroupByAlcId(Tmct0alcId alcId) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError ";
    StringBuilder where = new StringBuilder(
                                            " WHERE afiError.tmct0alo.id.nuorden = :nuorden AND "
                                                + "afiError.tmct0alo.id.nbooking = :nbooking AND afiError.tmct0alo.id.nucnfclt = :nucnfclt AND "
                                                + "afiError.nucnfliq=:nucnfliq ) ");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nuorden", alcId.getNuorden());
      query.setParameter("nbooking", alcId.getNbooking());
      query.setParameter("nucnfclt", alcId.getNucnfclt());
      query.setParameter("nucnfliq", alcId.getNucnfliq());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findPartnersByAloId(Tmct0aloId aloId, Long afiErrorId) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError ";
    StringBuilder where = new StringBuilder(
                                            " WHERE afiError.id != :afiErrorId AND afiError.tmct0alo.id.nuorden = :nuorden AND "
                                                + "afiError.tmct0alo.id.nbooking = :nbooking AND afiError.tmct0alo.id.nucnfclt = :nucnfclt AND "
                                                + "(afiError.nucnfliq IS NULL OR afierror.nucnfliq=0) ");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nuorden", aloId.getNuorden());
      query.setParameter("nbooking", aloId.getNbooking());
      query.setParameter("nucnfclt", aloId.getNucnfclt());
      query.setParameter("afiErrorId", afiErrorId);
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<Tmct0AfiError> findGroupByAloId(Tmct0aloId aloId) {
    List<Tmct0AfiError> resultList = new LinkedList<Tmct0AfiError>();
    String select = "SELECT afiError ";
    String from = "FROM Tmct0AfiError afiError ";
    StringBuilder where = new StringBuilder(
                                            " WHERE afiError.tmct0alo.id.nuorden = :nuorden AND "
                                                + "afiError.tmct0alo.id.nbooking = :nbooking AND afiError.tmct0alo.id.nucnfclt = :nucnfclt AND "
                                                + "(afiError.nucnfliq IS NULL OR afierror.nucnfliq=0) ");
    StringBuilder querySB = new StringBuilder(select).append(from).append(where);
    // se construye la query segun los filtros
    Query query = null;
    try {
      query = em.createQuery(querySB.toString());
      query.setParameter("nuorden", aloId.getNuorden());
      query.setParameter("nbooking", aloId.getNbooking());
      query.setParameter("nucnfclt", aloId.getNucnfclt());
      resultList = query.getResultList();
    } catch (PersistenceException ex) {

    }
    return resultList;
  }

  public List<TitularesDTO> findAllDTO(Date fechaDesde,
                                       Date fechaHasta,
                                       Character mercadoNacInt,
                                       Character sentido,
                                       List<String> nudnicif,
                                       String codError,
                                       List<String> nbclient,
                                       List<String> nbclient1,
                                       List<String> nbclient2,
                                       Character tpnactit,
                                       List<String> isin,
                                       List<String> alias,
                                       Character tipodocumento,
                                       String paisres,
                                       String tpdomici,
                                       String nbdomici,
                                       String nudomici,
                                       String provincia,
                                       String codpostal,
                                       String nureford,
                                       String nbooking,
                                       Integer firstRow,
                                       Integer lastRow,
                                       Short hasErrors) {
    final StringBuilder selectExterno = new StringBuilder("SELECT * FROM (");
    final StringBuilder select = new StringBuilder(
                                                   "select afiError.ID id, alo.NUCNFCLT NUCNFCLT, alo.NBOOKING NBOOKING, afiError.NUCNFLIQ, "
                                                       + "afiError.NBCLIENT, afiError.NBCLIENT1, afiError.NBCLIENT2, afiError.TPIDENTI, "
                                                       + "afiError.TPSOCIED, afiError.NUDNICIF, afiError.CCV, afiError.CDDEPAIS, afiError.TPNACTIT, "
                                                       + "afiError.TPDOMICI, afiError.NBDOMICI, afiError.NUDOMICI, afiError.NUREFORD, ord.CDCLSMDO, "
                                                       + "alo.CDTPOPER, alo.CDISIN, afiError.CDPOSTAL, afiError.NBCIUDAD, afiError.NBPROVIN, "
                                                       + "afiError.TPTIPREP, afiError.CDNACTIT, afiError.PARTICIP, afiError.NUORDEN, ord.NUTITEJE, "
                                                       + "ali.CDALIASS, ali.DESCRALI, ");
    if (mercadoNacInt == 'N') {
      select.append("alc.FEEJELIQ, ROW_NUMBER() OVER (ORDER BY afiError.id DESC) AS row_number ");
    } else {
      select.append("alo.FEOPERAC, ROW_NUMBER() OVER (ORDER BY afiError.id DESC) AS row_number ");
    }

    final StringBuilder from = new StringBuilder(" from TMCT0_AFI_ERROR afiError ");
    final StringBuilder where = new StringBuilder(" ");
    final StringBuilder whereExterno = new StringBuilder();
    boolean whereEmpty = true;
    boolean whereExternoEmpty = true;

    final String firstRowParamName = "firstRow";
    if (firstRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number> :" + firstRowParamName);
    } // if (firstRow != null)

    final String lastRowParamName = "lastRow";
    if (lastRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number<= :" + lastRowParamName);
    } // if (lastRow != null)

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq >=:" + fechaDesdeParamName);
      } // if (fechaDesde != null)

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq <=:" + fechaHastaParamName);
      } // if (fechaHasta != null)
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC >=:" + fechaDesdeParamName);
      } // if (fechaDesde != null)

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC <=:" + fechaHastaParamName);
      } // if (fechaHasta != null)
    } // else

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    } // if (sentido != null)

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbooking.indexOf("#") != -1) {
        nbooking = nbooking.replace("#", "");
        where.append("alo.NBOOKING <>:" + nbookingParamName);
      } else {
        where.append("alo.NBOOKING =:" + nbookingParamName);
      }
    } // if (nbooking != null)

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (isin.get(0).indexOf("#") != -1) {
        isin.set(0, isin.get(0).replace("#", ""));
        where.append("alo.CDISIN NOT IN :" + isinParamName);
      } else {
        where.append("alo.CDISIN IN :" + isinParamName);
      }
    } // if (isin != null && isin.size() > 0)

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null && nudnicif.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudnicif.get(0).indexOf("#") != -1) {
        nudnicif.set(0, nudnicif.get(0).replace("#", ""));
        where.append("afiError.nudnicif NOT IN :" + nudnicifParamName);
      } else {
        where.append("afiError.nudnicif IN :" + nudnicifParamName);
      }
    } // if (nudnicif != null && nudnicif.size() > 0)

    final String nurefordParamName = "nureford";
    if (nureford != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.indexOf("#") != -1) {
        nureford = nureford.replace("#", "");
        where.append("afiError.NUREFORD<>:" + nurefordParamName);
      } else {
        where.append("afiError.NUREFORD=:" + nurefordParamName);
      }
    } // if (nureford != null)

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (hasErrors != 3) {
      if (mercadoNacInt != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
      }
    } else {
      mercadoNacInt = null;
    } // else

    final String nbclientParamName = "nbclient";
    if (nbclient != null && nbclient.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient.get(0).indexOf("#") != -1) {
        nbclient.set(0, nbclient.get(0).replace("#", ""));
        where.append("afiError.nbclient NOT IN :" + nbclientParamName);
      } else {
        where.append("afiError.nbclient IN :" + nbclientParamName);
      }
    } // if (nbclient != null && nbclient.size() > 0)

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null && nbclient1.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient1.get(0).indexOf("#") != -1) {
        nbclient1.set(0, nbclient1.get(0).replace("#", ""));
        where.append("afiError.nbclient1 NOT IN :" + nbclient1ParamName);
      } else {
        where.append("afiError.nbclient1 IN :" + nbclient1ParamName);
      }
    } // if (nbclient1 != null && nbclient1.size() > 0)

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null && nbclient2.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient2.get(0).indexOf("#") != -1) {
        nbclient2.set(0, nbclient2.get(0).replace("#", ""));
        where.append("afiError.nbclient2 NOT IN :" + nbclient2ParamName);
      } else {
        where.append("afiError.nbclient2 IN :" + nbclient2ParamName);
      }
    } // if (nbclient2 != null && nbclient2.size() > 0)

    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (alias.get(0).indexOf("#") != -1) {
        alias.set(0, alias.get(0).replace("#", ""));
        where.append("ord.CDCUENTA NOT IN :" + aliasParamName);
      } else {
        where.append("ord.CDCUENTA IN :" + aliasParamName);
      }
    } // if (alias != null && alias.size() > 0)

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afiError.TPIDENTI = '' OR afiError.TPIDENTI IS NULL) ");
      } else {
        where.append("afiError.TPIDENTI =:" + tipodocumentoParamName);
      }
    } // if (tipodocumento != null)

    final String codErrorParamName = "codError";
    if (codError != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codError.indexOf("#") != -1) {
        codError = codError.replace("#", "");
        where.append("codigo.CODIGO<>:" + codErrorParamName);
      } else {
        where.append("codigo.CODIGO=:" + codErrorParamName);
      }
      from.append(" INNER JOIN TMCT0_AFI_ERROR_CODIGO_ERROR afiErrorCodigo ON afiError.ID = afiErrorCodigo.ID_AFI_ERROR "
                  + " INNER JOIN TMCT0_CODIGO_ERROR codigo ON codigo.ID = afiErrorCodigo.ID_CODIGO_ERROR ");
    } // if (codError != null)

    from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afiError.NUCNFCLT AND alo.NBOOKING = afiError.NBOOKING AND alo.NUORDEN = afiError.NUORDEN ");

    // Si se buscan los que no tienen ALO asociado
    if (hasErrors == 3) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append(" alo.nbooking IS null ");
    } else {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append(" alo.nbooking IS NOT null ");
    } // else

    from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");

    if (mercadoNacInt != null && mercadoNacInt == 'N') {
      from.append(" INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT AND alc.nucnfliq = afiError.nucnfliq  ");
    } else {
      from.append(" LEFT JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT AND alc.nucnfliq = afiError.nucnfliq ");
    } // else

    from.append(" LEFT JOIN TMCT0ALI ali ON ord.cdcuenta = ali.CDALIASS AND FHFINAL = 99991231 ");

    String sQuery = selectExterno.append(select.append(from.append(where))).append(") afiInterno ")
                                 .append(whereExterno).toString();

    LOG.debug(" Consulta erroneso: " + sQuery);

    final Query query = em.createNativeQuery(sQuery);

    setParametersWithVectors(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, codError,
                             mercadoNacInt, sentido, isin, alias, null, null, null, null, null, null, null, null, null,
                             nureford, null, tipodocumento, null, null, null, nbooking, fechaDesdeParamName,
                             fechaHastaParamName, nudnicifParamName, nbclientParamName, nbclient1ParamName,
                             nbclient2ParamName, codErrorParamName, mercadoNacIntParamName, sentidoParamName,
                             isinParamName, aliasParamName, null, null, null, null, null, null, null, null, null,
                             nurefordParamName, null, tipodocumentoParamName, null, null, null, nbookingParamName,
                             firstRow, firstRowParamName, lastRow, lastRowParamName);

    LOG.debug(" Inicio ejecucion: ");

    final List<Object> resultList = query.getResultList();

    LOG.debug(" Fin ejecucion: ");

    // MFG 05/08/2016 se incluye en el dto la lista de errores.

    final List<TitularesDTO> titularesList = new ArrayList<TitularesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TitularesDTO titular = DTOUtils.convertObjectToTitularesDTO((Object[]) obj);
        titular.setAfiError(true);
        // titular.setErrorList(getListErroresTitular(titular.getId()));
        titularesList.add(titular);
      } // for
    } // if

    return titularesList;
  } // findAllDTO

  public List<Map<String, String>> getListErroresTitular(Long afiErrorId) {
    String sQuery = "select  codigo.codigo, codigo.DESCRIPCION from TMCT0_AFI_ERROR_CODIGO_ERROR afiErrorCodigo INNER JOIN TMCT0_CODIGO_ERROR codigo ON codigo.ID = afiErrorCodigo.ID_CODIGO_ERROR WHERE afiErrorCodigo.ID_AFI_ERROR = :idAfiError";
    final Query query = em.createNativeQuery(sQuery);
    query.setParameter("idAfiError", afiErrorId);
    LOG.debug(" Inicio ejecucion Obtener errores: ");

    final List<Object> resultList = query.getResultList();

    LOG.debug(" Fin ejecucion Obtener errores: ");

    List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();

    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        Object[] valuesFromQuery = (Object[]) obj;
        Map<String, String> mapeoErrores = new HashMap<String, String>();
        mapeoErrores.put("codigo", (String) valuesFromQuery[0]);
        mapeoErrores.put("descripcion", (String) valuesFromQuery[1]);
        errorList.add(mapeoErrores);
      }
    }

    return errorList;

  }

  public List<TitularesDTO> findAllDTOWithoutAfi(Date fechaDesde,
                                                 Date fechaHasta,
                                                 Character mercadoNacInt,
                                                 Character sentido,
                                                 List<String> nudnicif,
                                                 String codError,
                                                 List<String> nbclient,
                                                 List<String> nbclient1,
                                                 List<String> nbclient2,
                                                 Character tpnactit,
                                                 List<String> isin,
                                                 List<String> alias,
                                                 Character tipodocumento,
                                                 String paisres,
                                                 String tpdomici,
                                                 String nbdomici,
                                                 String nudomici,
                                                 String provincia,
                                                 String codpostal,
                                                 String nureford,
                                                 String nbooking,
                                                 Integer firstRow,
                                                 Integer lastRow,
                                                 Short hasErrors) {
    final StringBuilder selectExterno = new StringBuilder("SELECT * FROM (");
    final StringBuilder select = new StringBuilder("");
    final StringBuilder from = new StringBuilder("");

    if (mercadoNacInt == 'N') {
      select.append("select 0 id, alo.NUCNFCLT, alo.NBOOKING, alc.NUCNFLIQ, '', '', '', '', '', '', '', '', '', '', '', '',"
                    + " ord.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, '', '', '', '', '', 0, ord.NUORDEN, ord.NUTITEJE, ali.CDALIASS, ali.DESCRALI, '', ");
      select.append("alc.FEEJELIQ, ROW_NUMBER() OVER (ORDER BY alc.NUCNFCLT DESC) AS row_number ");
      from.append(" from TMCT0ALC alc");
      from.append(" LEFT JOIN TMCT0ORD ord ON alc.NUORDEN = ord.NUORDEN ");
      from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = alc.NUCNFCLT AND alo.NBOOKING = alc.NBOOKING AND alo.NUORDEN = alc.NUORDEN ");
    } else {
      select.append("select 0 id, alo.NUCNFCLT, alo.NBOOKING, '', '', '', '', '', '', '', '', '', '', '', '', '',"
                    + " ord.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, '', '', '', '', '', 0, ord.NUORDEN, ord.NUTITEJE, ali.CDALIASS, ali.DESCRALI, '', ");
      select.append("alo.FEOPERAC, ROW_NUMBER() OVER (ORDER BY alo.NUCNFCLT DESC) AS row_number ");
      from.append(" from TMCT0ALO alo");
      from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");
    }

    final StringBuilder where = new StringBuilder(" ");
    final StringBuilder whereExterno = new StringBuilder();
    boolean whereEmpty = true;
    boolean whereExternoEmpty = true;

    final String firstRowParamName = "firstRow";
    if (firstRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number>:" + firstRowParamName);
    } // if (firstRow != null)

    final String lastRowParamName = "lastRow";
    if (lastRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number<=:" + lastRowParamName);
    } // if (lastRow != null)

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq >=:" + fechaDesdeParamName);
      } // if (fechaDesde != null)

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq <=:" + fechaHastaParamName);
      } // if (fechaHasta != null)
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC >=:" + fechaDesdeParamName);
      } // if (fechaDesde != null)

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC <=:" + fechaHastaParamName);
      } // if (fechaHasta != null)
    } // else

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    } // if (sentido != null)

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbooking.indexOf("#") != -1) {
        nbooking = nbooking.replace("#", "");
        where.append("alo.NBOOKING <>:" + nbookingParamName);
      } else {
        where.append("alo.NBOOKING =:" + nbookingParamName);
      }
    } // if (nbooking != null)

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (isin.get(0).indexOf("#") != -1) {
        isin.set(0, isin.get(0).replace("#", ""));
        where.append("alo.CDISIN NOT IN :" + isinParamName);
      } else {
        where.append("alo.CDISIN IN :" + isinParamName);
      }
    } // if (isin != null && isin.size() > 0)

    final String nurefordParamName = "nureford";
    if (nureford != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.indexOf("#") != -1) {
        nureford = nureford.replace("#", "");
        where.append("ord.NUREFORD<>:" + nurefordParamName);
      } else {
        where.append("ord.NUREFORD=:" + nurefordParamName);
      }
    } // if (nureford != null)

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (hasErrors != 3) {
      if (mercadoNacInt != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
      }
    } else {
      mercadoNacInt = null;
    } // else

    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (alias.get(0).indexOf("#") != -1) {
        alias.set(0, alias.get(0).replace("#", ""));
        where.append("ord.CDCUENTA NOT IN :" + aliasParamName);
      } else {
        where.append("ord.CDCUENTA IN :" + aliasParamName);
      }
    } // if (alias != null && alias.size() > 0)

    if (mercadoNacInt == 'N') {
      where.append(" and not exists (select 1 from bsnbpsql.tmct0afi b where b.nuorden=alc.nuorden and b.nbooking=alc.nbooking and b.nucnfclt=alc.nucnfclt and b.nucnfliq=alc.nucnfliq and b.cdinacti='A')");
    } else {
      where.append(" and not exists (select 1 from bsnbpsql.tmct0afi b where b.nuorden=alo.nuorden and b.nbooking=alo.nbooking and b.nucnfclt=alo.nucnfclt and b.cdinacti='A')");
    }

    from.append(" LEFT JOIN TMCT0ALI ali ON ord.cdcuenta = ali.CDALIASS AND FHFINAL = 99991231 ");

    final Query query = em.createNativeQuery(selectExterno.append(select.append(from.append(where)))
                                                          .append(") afiInterno ").append(whereExterno).toString());

    setParametersWithVectors(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, codError,
                             mercadoNacInt, sentido, isin, alias, null, null, null, null, null, null, null, null, null,
                             nureford, null, tipodocumento, null, null, null, nbooking, fechaDesdeParamName,
                             fechaHastaParamName, null, null, null, null, null, mercadoNacIntParamName,
                             sentidoParamName, isinParamName, aliasParamName, null, null, null, null, null, null, null,
                             null, null, nurefordParamName, null, null, null, null, null, nbookingParamName, firstRow,
                             firstRowParamName, lastRow, lastRowParamName);

    final List<Object> resultList = query.getResultList();

    final List<TitularesDTO> titularesList = new ArrayList<TitularesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TitularesDTO titular = DTOUtils.convertObjectToTitularesDTO((Object[]) obj);
        titular.setAfiError(false);
        titularesList.add(titular);
      } // for
    } // if

    return titularesList;
  } // findAllDTOWithoutAfi

  public int countAllDTOWithoutAfi(Date fechaDesde,
                                   Date fechaHasta,
                                   Character mercadoNacInt,
                                   Character sentido,
                                   List<String> nudnicif,
                                   String codError,
                                   List<String> nbclient,
                                   List<String> nbclient1,
                                   List<String> nbclient2,
                                   Character tpnactit,
                                   List<String> isin,
                                   List<String> alias,
                                   Character tipodocumento,
                                   String paisres,
                                   String tpdomici,
                                   String nbdomici,
                                   String nudomici,
                                   String provincia,
                                   String codpostal,
                                   String nureford,
                                   String nbooking,
                                   Integer firstRow,
                                   Integer lastRow,
                                   Short hasErrors) {
    final StringBuilder selectExterno = new StringBuilder("SELECT count(*) FROM (");
    final StringBuilder select = new StringBuilder("");
    final StringBuilder from = new StringBuilder("");

    if (mercadoNacInt == 'N') {
      select.append("select 0 id, alo.NUCNFCLT, alo.NBOOKING, alc.NUCNFLIQ, '', '', '', '', '', '', '', '', '', '', '', '',"
                    + " ord.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, '', '', '', '', '', 0, ord.NUORDEN, ord.NUTITEJE, ali.CDALIASS, ali.DESCRALI, '', ");
      select.append("alc.FEEJELIQ ");
      from.append(" from TMCT0ALC alc");
      from.append(" LEFT JOIN TMCT0ORD ord ON alc.NUORDEN = ord.NUORDEN ");
      from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = alc.NUCNFCLT AND alo.NBOOKING = alc.NBOOKING AND alo.NUORDEN = alc.NUORDEN ");
    } else {
      select.append("select 0 id, alo.NUCNFCLT, alo.NBOOKING, '', '', '', '', '', '', '', '', '', '', '', '', '',"
                    + " ord.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, '', '', '', '', '', 0, ord.NUORDEN, ord.NUTITEJE, ali.CDALIASS, ali.DESCRALI, '', ");
      select.append("alo.FEOPERAC ");
      from.append(" from TMCT0ALO alo");
      from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");
    }

    final StringBuilder where = new StringBuilder(" ");
    final StringBuilder whereExterno = new StringBuilder();
    boolean whereEmpty = true;
    boolean whereExternoEmpty = true;

    final String firstRowParamName = "firstRow";
    if (firstRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number>:" + firstRowParamName);
    } // if (firstRow != null)

    final String lastRowParamName = "lastRow";
    if (lastRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number<=:" + lastRowParamName);
    } // if (lastRow != null)

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq >=:" + fechaDesdeParamName);
      } // if (fechaDesde != null)

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq <=:" + fechaHastaParamName);
      } // if (fechaHasta != null)
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC >=:" + fechaDesdeParamName);
      } // if (fechaDesde != null)

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC <=:" + fechaHastaParamName);
      } // if (fechaHasta != null)
    } // else

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    } // if (sentido != null)

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbooking.indexOf("#") != -1) {
        nbooking = nbooking.replace("#", "");
        where.append("alo.NBOOKING <>:" + nbookingParamName);
      } else {
        where.append("alo.NBOOKING =:" + nbookingParamName);
      }
    } // if (nbooking != null)

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (isin.get(0).indexOf("#") != -1) {
        isin.set(0, isin.get(0).replace("#", ""));
        where.append("alo.CDISIN NOT IN :" + isinParamName);
      } else {
        where.append("alo.CDISIN IN :" + isinParamName);
      }
    } // if (isin != null && isin.size() > 0)

    final String nurefordParamName = "nureford";
    if (nureford != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.indexOf("#") != -1) {
        nureford = nureford.replace("#", "");
        where.append("ord.NUREFORD<>:" + nurefordParamName);
      } else {
        where.append("ord.NUREFORD=:" + nurefordParamName);
      }
    } // if (nureford != null)

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (hasErrors != 3) {
      if (mercadoNacInt != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
      }
    } else {
      mercadoNacInt = null;
    } // else

    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (alias.get(0).indexOf("#") != -1) {
        alias.set(0, alias.get(0).replace("#", ""));
        where.append("ord.CDCUENTA NOT IN :" + aliasParamName);
      } else {
        where.append("ord.CDCUENTA IN :" + aliasParamName);
      }
    } // if (alias != null && alias.size() > 0)

    if (mercadoNacInt == 'N') {
      where.append(" and not exists (select 1 from bsnbpsql.tmct0afi b where b.nuorden=alc.nuorden and b.nbooking=alc.nbooking and b.nucnfclt=alc.nucnfclt and b.nucnfliq=alc.nucnfliq and b.cdinacti='A')");
    } else {
      where.append(" and not exists (select 1 from bsnbpsql.tmct0afi b where b.nuorden=alo.nuorden and b.nbooking=alo.nbooking and b.nucnfclt=alo.nucnfclt and b.cdinacti='A')");
    }

    from.append(" LEFT JOIN TMCT0ALI ali ON ord.cdcuenta = ali.CDALIASS AND FHFINAL = 99991231 ");

    final Query query = em.createNativeQuery(selectExterno.append(select.append(from.append(where)))
                                                          .append(") afiInterno ").append(whereExterno).toString());

    setParametersWithVectors(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, codError,
                             mercadoNacInt, sentido, isin, alias, null, null, null, null, null, null, null, null, null,
                             nureford, null, tipodocumento, null, null, null, nbooking, fechaDesdeParamName,
                             fechaHastaParamName, null, null, null, null, null, mercadoNacIntParamName,
                             sentidoParamName, isinParamName, aliasParamName, null, null, null, null, null, null, null,
                             null, null, nurefordParamName, null, null, null, null, null, nbookingParamName, firstRow,
                             firstRowParamName, lastRow, lastRowParamName);

    return (int) query.getResultList().get(0);
  } // countAllDTOWithoutAfi

  public int countFindAllDTOPageable(Date fechaDesde,
                                     Date fechaHasta,
                                     Character mercadoNacInt,
                                     Character sentido,
                                     List<String> nudnicif,
                                     String codError,
                                     List<String> nbclient,
                                     List<String> nbclient1,
                                     List<String> nbclient2,
                                     Character tpnactit,
                                     List<String> isin,
                                     List<String> alias,
                                     Character tipodocumento,
                                     String paisres,
                                     String tpdomici,
                                     String nbdomici,
                                     String nudomici,
                                     String provincia,
                                     String codpostal,
                                     String nureford,
                                     String nbooking,
                                     Integer firstRow,
                                     Integer lastRow,
                                     Short hasErrors) {
    final StringBuilder select = new StringBuilder("SELECT COUNT (*)");
    // if ( mercadoNacInt == 'N' ) {
    // select.append(
    // "alc.FEEJELIQ , ROW_NUMBER() OVER (ORDER BY afiError.id DESC) AS row_number  "
    // );
    // } else {
    // select.append(
    // "alo.FEOPERAC , ROW_NUMBER() OVER (ORDER BY afiError.id DESC) AS row_number  "
    // );
    // }

    final StringBuilder from = new StringBuilder(" from TMCT0_AFI_ERROR afiError ");
    final StringBuilder where = new StringBuilder(" ");
    final StringBuilder whereExterno = new StringBuilder();
    boolean whereEmpty = true;
    boolean whereExternoEmpty = true;

    final String firstRowParamName = "firstRow";
    // if ( firstRow != null ) {
    // if ( !whereExternoEmpty ) {
    // whereExterno.append( " and " );
    // } else {
    // whereExterno.append( " where " );
    // whereExternoEmpty = false;
    // }
    // whereExterno.append( "afiInterno.row_number>:" + firstRowParamName );
    // }
    //
    final String lastRowParamName = "lastRow";
    // if ( lastRow != null ) {
    // if ( !whereExternoEmpty ) {
    // whereExterno.append( " and " );
    // } else {
    // whereExterno.append( " where " );
    // whereExternoEmpty = false;
    // }
    // whereExterno.append( "afiInterno.row_number<=:" + lastRowParamName );
    // }

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq >=:" + fechaDesdeParamName);
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alc.feejeliq <=:" + fechaHastaParamName);
      }
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC >=:" + fechaDesdeParamName);
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("alo.FEOPERAC <=:" + fechaHastaParamName);
      }
    }

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    }

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.NBOOKING =:" + nbookingParamName);
    }

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDISIN IN :" + isinParamName);
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null && nudnicif.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nudnicif IN :" + nudnicifParamName);
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.NUREFORD=:" + nurefordParamName);
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (hasErrors != 3) {
      if (mercadoNacInt != null) {

        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
      }
    } else {
      mercadoNacInt = null;
    }
    final String nbclientParamName = "nbclient";
    if (nbclient != null && nbclient.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nbclient IN :" + nbclientParamName);
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null && nbclient1.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nbclient1 IN :" + nbclient1ParamName);
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null && nbclient2.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afiError.nbclient2 IN :" + nbclient2ParamName);
    }

    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ord.CDCUENTA IN :" + aliasParamName);
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afiError.TPIDENTI = '' OR afiError.TPIDENTI IS NULL) ");
      } else {
        where.append("afiError.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String codErrorParamName = "codError";
    if (codError != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("codigo.CODIGO=:" + codErrorParamName);
      from.append(" INNER JOIN TMCT0_AFI_ERROR_CODIGO_ERROR afiErrorCodigo ON afiError.ID = afiErrorCodigo.ID_AFI_ERROR "
                  + " INNER JOIN TMCT0_CODIGO_ERROR codigo ON codigo.ID = afiErrorCodigo.ID_CODIGO_ERROR ");

    }

    from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afiError.NUCNFCLT AND alo.NBOOKING = afiError.NBOOKING AND alo.NUORDEN = afiError.NUORDEN ");

    // Si se buscan los que no tienen ALO asociado
    if (hasErrors == 3) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append(" alo.nbooking IS null ");
    } else {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append(" alo.nbooking IS NOT null ");

    }

    from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");

    if (mercadoNacInt != null && mercadoNacInt == 'N') {
      from.append(" INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT  AND alc.nucnfliq = afiError.nucnfliq  ");
    } else {
      from.append(" LEFT JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT  AND alc.nucnfliq = afiError.nucnfliq ");
    }

    from.append(" LEFT JOIN TMCT0ALI ali ON ord.cdcuenta = ali.CDALIASS AND FHFINAL = 99991231 ");

    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());
    firstRow = null;
    lastRow = null;
    setParametersWithVectors(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, codError,
                             mercadoNacInt, sentido, isin, alias, null, null, null, null, null, null, null, null, null,
                             nureford, null, tipodocumento, null, null, null, nbooking, fechaDesdeParamName,
                             fechaHastaParamName, nudnicifParamName, nbclientParamName, nbclient1ParamName,
                             nbclient2ParamName, codErrorParamName, mercadoNacIntParamName, sentidoParamName,
                             isinParamName, aliasParamName, null, null, null, null, null, null, null, null, null,
                             nurefordParamName, null, tipodocumentoParamName, null, null, null, nbookingParamName,
                             firstRow, firstRowParamName, lastRow, lastRowParamName);

    return (int) query.getResultList().get(0);
  }

  public EntityManager getEntityManager() {
    return this.em;
  }
}
