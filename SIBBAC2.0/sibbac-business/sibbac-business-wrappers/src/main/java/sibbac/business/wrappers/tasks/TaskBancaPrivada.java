package sibbac.business.wrappers.tasks;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FilenameUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.bo.bancaPrivadaReader.BancaPrivadaFileReader;
import sibbac.common.HttpGetFiles;
import sibbac.common.HttpGetFilesConfigDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ROUTING.NAME, name = Task.GROUP_ROUTING.JOB_BANCA_PRIVADA, interval = 1, delay = 0, intervalUnit = IntervalUnit.HOUR)
public class TaskBancaPrivada extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOG = LoggerFactory.getLogger(TaskBancaPrivada.class);

  @Autowired
  private ApplicationContext ctx;

  @Value("${sibbac.wrappers.BancaPrivadaFile:}")
  private String fileName;

  @Value("${sibbac.wrappers.banca.privada.file.download.url:}")
  private String url;
  @Value("${sibbac.wrappers.banca.privada.file.download.url.username:}")
  private String username;
  @Value("${sibbac.wrappers.banca.privada.file.download.url.password:}")
  private String password;

  @Value("${sibbac.wrappers.banca.privada.file.download.pattern:}")
  private String filePattern;

  @Value("${sibbac.wrappers.banca.privada.file.download.charset:}")
  private Charset fileCharset;

  @Value("${sibbac.wrappers.banca.privada.file.download.end.mark:}")
  private String fileNameEndMark;

  @Value("${sibbac.wrappers.banca.privada.file.download.find.data.pattern:}")
  private String findDataPattern;

  @Value("${sibbac.wrappers.banca.privada.file.download.find.init.data.token:}")
  private String dataFromThisToken;

  @Value("${sibbac.wrappers.banca.privada.file.download.find.init.file.token:}")
  private String inicioFicheroPattern;
  @Value("${sibbac.wrappers.banca.privada.file.download.find.end.file.token:}")
  private String finFicheropattern;

  @Value("${sibbac.wrappers.banca.privada.file.download.find.log.date.pos.end:}")
  private int logDatePosEnd;

  @Value("${sibbac.wrappers.banca.privada.file.download.find.log.date.pattern:}")
  private String logDatePattern;

  @Value("${sibbac.wrappers.banca.privada.file.download.max.files:}")
  private int maxGetFiles;

  @Value("${sibbac.wrappers.banca.privada.file.download.last.modified.date.format:}")
  private String lastModifiedFileDatePattern;

  @Autowired
  private Tmct0cfgBo configBo;

  @Autowired
  private HttpGetFiles httpGetFiles;

  @Override
  public void executeTask() throws Exception {

    final BancaPrivadaFileReader reader;

    LOG.debug("[executeTask] Init");

    this.descargarFicheroBanifUlbridge();
    reader = ctx.getBean(BancaPrivadaFileReader.class);
    try {
      reader.readFromFile(false, fileCharset.name());
    }
    catch (SIBBACBusinessException e) {
      LOG.error("[executeTask] Error de regla de negocio", e);
      throw e;
    }
    LOG.debug("[executeTask] Fin");
  }

  private void descargarFicheroBanifUlbridge() {
    String linea;
    if (url != null && !url.trim().isEmpty()) {

      try {
        Date fechaUltimoFicheroProcesado = null;

        SimpleDateFormat dateFormatLogDate = new SimpleDateFormat(logDatePattern);
        SimpleDateFormat humanDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        int countOutputFiles = 0;
        boolean inicio = false;
        boolean fin = false;

        Tmct0cfg configLastModifiedDate = configBo.getBanifLastModifiedDateFromProcessedFile();
        if (configLastModifiedDate == null) {
          throw new SIBBACBusinessException("INCIDENCIA-No encontrado registro las modified date from processed file.");
        }

        if (configLastModifiedDate != null && configLastModifiedDate.getKeyvalue() != null
            && !configLastModifiedDate.getKeyvalue().trim().isEmpty()) {
          try {
            fechaUltimoFicheroProcesado = humanDateFormat.parse(configLastModifiedDate.getKeyvalue().trim());
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(String.format(
                "INCIDENCIA-No se ha podido parsear la fecha de ultima modificacion %s", configLastModifiedDate
                    .getKeyvalue().trim()), e);
          }
        }

        Date logDate = null;
        Date firstLogDate = null;
        Date firstFileModifiedDate = null;
        Date firstLogDateFromComfig = null;

        Tmct0cfg configLastLogDate = configBo.getBanifLastLogDateFromProcessedFile();
        if (configLastLogDate == null) {
          throw new SIBBACBusinessException("INCIDENCIA-No encontrado registro las log date from processed file.");
        }

        if (configLastLogDate != null && configLastLogDate.getKeyvalue() != null
            && !configLastLogDate.getKeyvalue().trim().isEmpty()) {
          try {
            firstLogDateFromComfig = humanDateFormat.parse(configLastLogDate.getKeyvalue().trim());
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(String.format(
                "INCIDENCIA-No se ha podido parsear la fecha del ultimo log leido %s", configLastModifiedDate
                    .getKeyvalue().trim()), e);
          }
        }

        String outputpath = Paths.get(fileName).getParent().toString();
        String localFileName = Paths.get(fileName).getFileName().toString();

        String baseName = FilenameUtils.getBaseName(localFileName);
        String concurrent = FormatDataUtils.convertDateToConcurrentFileName(new Date());
        Path tmpOutput = Paths.get(outputpath, String.format("%s_%s.TMP", baseName, concurrent));
        Path tmpPath = Paths.get(outputpath, "tmp");
        if (Files.notExists(tmpPath)) {
          Files.createDirectories(tmpPath);
        }
        Path output = Paths.get(outputpath, localFileName);

        HttpGetFilesConfigDTO config = new HttpGetFilesConfigDTO(url, username, password, filePattern, fileNameEndMark,
            tmpPath.toString(), fechaUltimoFicheroProcesado, maxGetFiles, lastModifiedFileDatePattern, true);
        List<Path> listPaths = httpGetFiles.donwloadFiles(config);

        if (listPaths != null && !listPaths.isEmpty()) {

          try {

            boolean exists = Files.exists(output);

            if (exists) {
              Files.move(output, tmpOutput, StandardCopyOption.ATOMIC_MOVE);
            }

            boolean hasInicioToken = inicioFicheroPattern != null && !inicioFicheroPattern.trim().isEmpty();
            boolean hasFinalToken = inicioFicheroPattern != null && !inicioFicheroPattern.trim().isEmpty();
            int countLineas;
            try (OutputStream os = Files.newOutputStream(tmpOutput, exists ? StandardOpenOption.APPEND
                : StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.ISO_8859_1));) {
              for (Path input : listPaths) {
                if (firstFileModifiedDate == null
                    || firstFileModifiedDate.getTime() < Files.getLastModifiedTime(input).toMillis()) {
                  firstFileModifiedDate = new Date(Files.getLastModifiedTime(input).toMillis());
                }
                try (InputStream is = Files.newInputStream(input, StandardOpenOption.READ);
                    GZIPInputStream gis = new GZIPInputStream(is);
                    BufferedReader br = new BufferedReader(new InputStreamReader(gis, fileCharset));) {
                  countLineas = 0;
                  while ((linea = br.readLine()) != null) {
                    countLineas++;
                    try {
                      logDate = this.getLogLineDate(linea, logDatePosEnd, dateFormatLogDate);

                      if (logDate != null && firstLogDateFromComfig != null
                          && logDate.getTime() < firstLogDateFromComfig.getTime()) {
                        continue;
                      }
                      if (firstLogDate == null || logDate != null && firstLogDate.compareTo(logDate) < 0) {
                        firstLogDate = logDate;
                      }

                      if (findDataPattern == null || findDataPattern.trim().isEmpty() || linea.matches(findDataPattern)) {

                        if (!hasInicioToken && !inicio) {
                          inicio = true;
                        }
                        else if (!inicio && linea.matches(inicioFicheroPattern)) {
                          inicio = true;
                        }
                        else if (!fin && linea.matches(finFicheropattern)) {
                          fin = true;
                        }

                        if (inicio) {
                          if (dataFromThisToken != null && !dataFromThisToken.trim().isEmpty()) {
                            linea = linea.substring(linea.indexOf(dataFromThisToken));
                          }

                          bw.write(linea);
                          bw.newLine();
                        }
                        if (inicio && fin) {
                          countOutputFiles++;
                          break;
                        }
                      }
                    }
                    catch (Exception e) {
                      LOG.trace("INCIENCIA-Linea: {} fichero: {} txt: {}. {}", countLineas, input.toString(), linea,
                          e.getMessage(), e);
                      continue;
                    }
                  }// fin while line

                  if (inicio && fin) {
                    break;
                  }
                }// try bufferedreader
                if (inicio && fin) {
                  break;
                }
              }// for inputs
            }// try outputstream

            if (!hasFinalToken && inicio) {
              fin = true;
              countOutputFiles++;
            }

            if (!inicio || inicio && fin) {
              if (firstFileModifiedDate != null && configLastModifiedDate != null) {
                configLastModifiedDate.setKeyvalue(humanDateFormat.format(firstFileModifiedDate));
                configBo.save(configLastModifiedDate);
              }

              if (firstLogDate != null && configLastLogDate != null) {
                configLastLogDate.setKeyvalue(humanDateFormat.format(firstLogDate));
                configBo.save(configLastLogDate);
              }
            }

            if (countOutputFiles <= 0 && !exists) {
              Files.deleteIfExists(tmpOutput);
            }
            else {
              Files.move(tmpOutput, output, StandardCopyOption.ATOMIC_MOVE);
            }
          }
          catch (Exception e) {
            Files.deleteIfExists(tmpOutput);
            throw new SIBBACBusinessException(e.getMessage(), e);
          }
          finally {
            for (Path path : listPaths) {
              Files.deleteIfExists(path);
            }
          }
        }
      }
      catch (Exception e) {
        LOG.warn(e.getMessage(), e);
      }
    }
  }

  private Date getLogLineDate(String linea, int lastPosDate, DateFormat df) throws ParseException {
    String d = getStLogLineDate(linea, lastPosDate, df);
    if (d != null) {
      return df.parse(d);
    }
    return null;
  }

  private String getStLogLineDate(String linea, int lastPosDate, DateFormat df) throws ParseException {
    if (linea.length() >= lastPosDate) {
      return linea.substring(0, lastPosDate);
    }
    return null;
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_ROUTING_TITULARES_BANCA_PRIVADA;
  }

}
