package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CuentaVirtual;


@Repository
public interface Tmct0CuentaVirtualDao extends JpaRepository< Tmct0CuentaVirtual, Long > {

}
