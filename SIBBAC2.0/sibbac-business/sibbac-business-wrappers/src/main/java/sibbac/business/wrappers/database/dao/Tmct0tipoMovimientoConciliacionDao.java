package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0TipoMovimientoConciliacion;


/**
 * The Interface Tmct0tipoMovimientoConciliacionDao.
 */
@Repository
public interface Tmct0tipoMovimientoConciliacionDao extends JpaRepository< Tmct0TipoMovimientoConciliacion, Long > {

	Tmct0TipoMovimientoConciliacion findByTipoOperacion( String tipoOperacion );

}
