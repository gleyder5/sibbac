package sibbac.business.wrappers.database.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sibbac.business.wrappers.database.dao.Tmct0menErrorHistDao;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.database.model.Tmct0menErrorHist;
import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.SIBBACTaskException;
import sibbac.database.bo.AbstractBo;

import java.util.Date;

@Service
public class Tmct0menErrorHistBo extends AbstractBo<Tmct0menErrorHist, String, Tmct0menErrorHistDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0menErrorHistBo.class);

  @Autowired
  private Tmct0staBo tmct0staBo;
  @Autowired
  private Tmct0menErrorHistDao tmct0menErrorHistDao ;


  private void register(Tmct0men tmct0men,Exception ex) {
	 if(ex instanceof SIBBACBusinessException) {
		 ex = (SIBBACBusinessException) ex;
		 try{
	           String message = ex.getMessage();
	           String cause  ="";
	           if(ex.getCause()!=null) {
	        	   cause  = ex.getCause().getLocalizedMessage();
	        	   message +="\n Cause:" +cause;
	           }
	           final Tmct0sta estado = tmct0staBo.findById(tmct0men.getTmct0sta().getId());
		  	  Tmct0menErrorHist last = 	tmct0menErrorHistDao.findByFhauditAndCdmensaAndTmct0sta(new Date(),tmct0men,estado);// verificamos y si existe obtenemos el ultimo registro de error del dia
	           if(	   last!=null 	&&
	        		   last.getCdmensa().getCdmensa().equals(tmct0men.getCdmensa()) &&
	        		   last.getCdmensa().getDsmensa().equals(tmct0men.getDsmensa())
	        		   ){
						   if(last.getNumExecution()==null) {
							   last.setNumExecution(1);
						   }
						   last.setFhaudit(new Date());
						   last.setNumExecution(last.getNumExecution()+1);
						   this.save(last);
			   }else{
	           		//TODO Ajustar para soportar el guardado del stackTrace para mayor detalle (probablemente colocar message como CLOB o varchar(3000)  )
				   this.save(new Tmct0menErrorHist().
						setCdmensa(tmct0men).
						setMessage(message).
						setTmct0sta(estado));
			   }
	    }catch(Exception exx){
	      LOG.error("Error  fatal al intentar reportar el error del proceso ejecutado : [{}] - {}",tmct0men.getCdmensa(),tmct0men.getDsmensa());
	      LOG.error("Motivo: {}" ,exx.getCause());
	      if(LOG.isDebugEnabled()) {
	    	  ex.printStackTrace();
	      }
	    }
	 }
  }
  public void registerError(Tmct0men tmct0men, SIBBACTaskException ex){
    LOG.info("Registrando error en ejecución de Proceso: [{}]  -  {}",tmct0men.getCdmensa(),tmct0men.getDsmensa());
    register(tmct0men, ex);
  }

  public void registerError(Tmct0men tmct0men, SIBBACBusinessException ex){
	    LOG.info("Registrando error en ejecución de Proceso: [{}]  -  {}",tmct0men.getCdmensa(),tmct0men.getDsmensa());
	    register(tmct0men,ex);
  }

}