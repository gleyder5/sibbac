package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Plantilla;


@Component
public interface PlantillaDao extends JpaRepository< Plantilla, Long > {

}
