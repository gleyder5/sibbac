package sibbac.business.wrappers.tasks.thread.result;

import java.io.Serializable;
import java.nio.file.Path;

import org.springframework.batch.item.file.FlatFileItemWriter;

public class WrapperMultiThreadedFlatFileItemWriterResultBean implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5392514994194568328L;

  @SuppressWarnings("rawtypes")
  private final FlatFileItemWriter writer;
  private final Path path;

  @SuppressWarnings("rawtypes")
  public WrapperMultiThreadedFlatFileItemWriterResultBean(FlatFileItemWriter writer, Path path) {
    super();
    this.writer = writer;
    this.path = path;
  }

  @SuppressWarnings("rawtypes")
  public FlatFileItemWriter getWriter() {
    return writer;
  }

  public Path getPath() {
    return path;
  }

}
