package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0sta;
import sibbac.business.wrappers.database.model.Tmct0staId;


@Repository
public interface Tmct0staDao extends JpaRepository< Tmct0sta, Tmct0staId > {

	Tmct0sta findById( final Tmct0staId id );

}
