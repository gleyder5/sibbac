package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.dto.ActualizacionCodigosOperacionEccDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0bokIdDTO;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;

@Repository
public interface Tmct0desgloseCamaraDao extends JpaRepository<Tmct0desglosecamara, Long> {

  // Tiene que estar en el estado de envio de fichero como Pdte. fichero o
  // Rechazado fichero del desglose camara
  // Tambien tiene que estar un estado mayor de Pdte. Liquidar (Desglose camara)
  // Tiene que estar en el codigo estado asignacion > rechazada (ALC)
  // Tiene que tener la cuenta clearing a 'S'
  @Query("select DC from Tmct0desglosecamara DC,  Tmct0CuentasDeCompensacion CC "
      + "where DC.tmct0alc.feejeliq > ?1 and DC.tmct0alc.cdestadoasig >= ?2 "
      + "and DC.tmct0estadoByCdestadoasig.idestado >= ?2  "
      + "and (DC.tmct0estadoByCdestadoentrec is null or DC.tmct0estadoByCdestadoentrec.idestado = ?3 or DC.tmct0estadoByCdestadoentrec.idestado = ?4)  "
      + "and DC.tmct0infocompensacion.cdctacompensacion = CC.cdCodigo " + "and CC.cuentaClearing = 'S' ")
  List<Tmct0desglosecamara> findByEnvioFicheroMegaraMercado(Date feejeliq, Integer PdteLiquidar, Integer PdteFichero,
      Integer RechazadoFichero);

  Tmct0desglosecamara findFirst1Bytmct0alc(Tmct0alc tmct0alc);

  @Query("SELECT DC FROM Tmct0desglosecamara DC, Tmct0movimientooperacionnuevaecc MO "
      + "WHERE MO=:movimientooperacionnuevaecc AND MO.tmct0desglosecamara=DC  ")
  Tmct0desglosecamara findByMovimientooperacionnuevaecc(
      @Param("movimientooperacionnuevaecc") Tmct0movimientooperacionnuevaecc movimientooperacionnuevaecc);

  @Query("SELECT count(d) FROM Tmct0desglosecamara d where d.tmct0alc.id = :alcId and d.tmct0estadoByCdestadoasig.idestado = :cdestadoasig ")
  int countDesglosesCamaraByAlcIdAndCdestadoasig(@Param("alcId") Tmct0alcId alcId,
      @Param("cdestadoasig") int cdestadoasig);

  @Query("SELECT count(d) FROM Tmct0desglosecamara d where d.tmct0alc.id = :alcId and d.tmct0estadoByCdestadoasig.idestado > 0 ")
  int countDesglosesCamaraByAlcIdAndCdestadoasigAndAlta(@Param("alcId") Tmct0alcId alcId);

  @Query("SELECT count(d) FROM Tmct0desglosecamara d where d.tmct0alc.id = :alcId and d.tmct0estadoByCdestadoasig.idestado <= :cdestadoasiglte "
      + " and d.tmct0estadoByCdestadoasig.idestado > 0 and d.iddesglosecamara != :iddesglosecamara ")
  int countDesglosesCamaraByAlcIdAndCdestadoasigLteAndDistinctIddesgloseCamara(@Param("alcId") Tmct0alcId alcId,
      @Param("cdestadoasiglte") int cdestadoasigLte, @Param("iddesglosecamara") long iddesglosecamara);

  @Modifying
  @Query(nativeQuery = true, value = "UPDATE tmct0desglosecamara set cdestadocont=:idEstado where iddesglosecamara=:iddesglosecamara")
  public int update(@Param("iddesglosecamara") Long iddesglosecamara, @Param("idEstado") int idEstado);

  /**
   * 
   * Actualiza el desglose camara a casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   */
  @Modifying
  @Query(value = "UPDATE TMCT0DESGLOSECAMARA DC SET DC.CASEPTI= 'S', DC.AUDIT_FECHA_CAMBIO=:fhaudit WHERE DC.NUORDEN=:nuorden AND DC.NBOOKING=:nbooking "
      + " AND DC.CDESTADOASIG > 0 ", nativeQuery = true)
  public Integer updateFlagCasadoPti(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking,
      @Param("fhaudit") Date fhaudit);

  @Query("select distinct  new sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0bokIdDTO(b.id.nuorden, b.id.nbooking) "
      + " from Tmct0desglosecamara dc, Tmct0bok b where dc.tmct0alc.tmct0alo.tmct0bok.id.nuorden = b.id.nuorden "
      + " and dc.tmct0alc.tmct0alo.tmct0bok.id.nbooking = b.id.nbooking and dc.fecontratacion >= :sinceDate "
      + " and dc.tmct0estadoByCdestadoasig.idestado > 0 and b.casepti = 'S' and dc.casepti = 'N' ")
  public List<Tmct0bokIdDTO> findBookingsIdConDesglosesCamaraSinCasar(@Param("sinceDate") Date sinceDate);

  @Query("SELECT dc FROM Tmct0desglosecamara dc WHERE dc.tmct0alc.id = :alcId AND dc.tmct0estadoByCdestadoasig.idestado = :pcdestadoasig "
      + " AND dc.tmct0CamaraCompensacion.cdCodigo IN :camaras ")
  List<Tmct0desglosecamara> findAllByTmct0alcIdAndCdestadoasigAndInCamarasCompensacion(
      @Param("alcId") Tmct0alcId alcId, @Param("pcdestadoasig") int cdestadoasig,
      @Param("camaras") List<String> camarasCompensacion);

  @Query("SELECT dc FROM Tmct0desglosecamara dc WHERE dc.tmct0alc.id = :alcId AND dc.tmct0estadoByCdestadoasig.idestado > 0 "
      + " AND dc.tmct0CamaraCompensacion.cdCodigo IN :camaras ")
  List<Tmct0desglosecamara> findAllByTmct0alcActivosIdAndCdestadoasigAndInCamarasCompensacion(
      @Param("alcId") Tmct0alcId alcId, @Param("camaras") List<String> camarasCompensacion);

  @Query("SELECT dc FROM Tmct0desglosecamara dc WHERE dc.tmct0alc.id = :alcId AND dc.tmct0estadoByCdestadoasig.idestado > 0 ")
  List<Tmct0desglosecamara> findAllByTmct0alcActivosIdAndCdestadoasig(@Param("alcId") Tmct0alcId alcId);

  @Query("SELECT sum(dc.nutitulos) FROM Tmct0desglosecamara dc WHERE dc.tmct0alc.id = :alcId ")
  BigDecimal getSumNutitulosByTmct0alcId(@Param("alcId") Tmct0alcId alcId);

  @Query("SELECT dc FROM Tmct0desglosecamara dc WHERE dc.tmct0alc.id = :alcId ")
  List<Tmct0desglosecamara> findAllByTmct0alcId(@Param("alcId") Tmct0alcId alcId);

  @Modifying
  @Query("UPDATE Tmct0desglosecamara dc SET dc.tmct0estadoByCdestadotit.idestado = :pcdestadotit, dc.auditFechaCambio = :fhaudit "
      + " WHERE dc.tmct0alc.id = :alcId AND dc.tmct0estadoByCdestadoasig.idestado > 0")
  Integer updateCdestadotitByTmct0alcId(@Param("alcId") Tmct0alcId alcId, @Param("pcdestadotit") int cdestadoatit,
      @Param("fhaudit") Date fhaudit);

  @Modifying
  @Query("UPDATE Tmct0desglosecamara dc SET dc.tmct0estadoByCdestadotit.idestado = :pcdestadotit, dc.auditFechaCambio = :fhaudit "
      + " WHERE dc.tmct0estadoByCdestadoasig.idestado > 0 "
      + " AND dc.tmct0alc.id IN  (SELECT alc.id FROM Tmct0alc alc where alc.tmct0alo.id = :aloId ) ")
  Integer updateCdestadotitByTmct0aloId(@Param("aloId") Tmct0aloId aloId, @Param("pcdestadotit") int cdestadoatit,
      @Param("fhaudit") Date fhaudit);

  @Query("select dc.fecontratacion, b.cdisin, dc.sentido "
      + " from Tmct0desglosecamara dc, Tmct0desgloseclitit dct, Tmct0movimientoeccFidessa mf, Tmct0bok b "
      + " where dct.tmct0desglosecamara.iddesglosecamara = dc.iddesglosecamara and dct.nuoperacionprev = mf.nuoperacionprev"
      + " and dc.tmct0alc.id.nuorden = b.id.nuorden and dc.tmct0alc.id.nbooking = b.id.nbooking "
      + " and dc.tmct0estadoByCdestadoasig.idestado > 0 and mf.imtitulosPendientesAsignar > 0 and b.casepti='S' ")
  List<Object[]> findAllFContratacionCdisinSentidoByDesglosesAltaAndCasadoptiAndMovimientoFidessaConTitulosPendientes();

  // @Query("select dc.tmct0alc"
  // +
  // " from Tmct0desglosecamara dc inner join dc.tmct0desgloseclitits dct inner join Tmct0movimientoeccFidessa mf on dct.nuoperacionprev = mf.nuoperacionprev"
  // +
  // " inner join Tmct0bok b on dc.tmct0alc.id.nuorden = b.id.nuorden and dc.tmct0alc.id.nbooking = b.id.nbooking "
  // +
  // " left join Tmct0movimientooperacionnuevaecc mn on mn.nudesglose = dc.nudesglose and mn.tmct0movimientoecc.cdestadomov not in ('9', '6', '21', '5')"
  // +
  // " where dc.fecontratacion = :fecontratacion and dc.tmct0estadoByCdestadoasig.idestado > 0 and mf.imtitulosPendientesAsignar > 0 and b.casepti='S' "
  // + " and b.cdisin = :cdisin and b.cdtpoper = :sentido and mn is null "
  // + " ")
  @Query("select dc.tmct0alc"
      + " from Tmct0desglosecamara dc, Tmct0desgloseclitit dct, Tmct0movimientoeccFidessa mf, Tmct0bok b "
      + " where dct.tmct0desglosecamara.iddesglosecamara = dc.iddesglosecamara and dct.nuoperacionprev = mf.nuoperacionprev"
      + " and dc.tmct0alc.id.nuorden = b.id.nuorden and dc.tmct0alc.id.nbooking = b.id.nbooking "
      + " and dc.fecontratacion = :fecontratacion and dc.tmct0estadoByCdestadoasig.idestado > 0 and dc.tmct0estadoByCdestadoasig.idestado < :cdestadoasigLt "
      + " and mf.imtitulosPendientesAsignar > 0 and b.casepti='S' "
      + " and b.cdisin = :cdisin and b.cdtpoper = :sentido ")
  List<Tmct0alc> findAllAlcsByFcontratacionAndCdisinAndSentidoAndCdestadoasigLt(
      @Param("fecontratacion") Date fecontratacion, @Param("cdisin") String cdisin, @Param("sentido") char sentido,
      @Param("cdestadoasigLt") int cdestadoasigLt);

  @Query("select distinct new sibbac.business.wrappers.database.dto.ActualizacionCodigosOperacionEccDTO(d.fecontratacion, d.isin, d.sentido,"
      + "d.tmct0estadoByCdestadoasig.idestado) "
      + " from Tmct0desglosecamara d where d.tmct0estadoByCdestadoasig.idestado = :cdestadoasig "
      + " and d.tmct0alc.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv "
      + " order by d.fecontratacion desc, d.isin desc, d.sentido desc ")
  List<ActualizacionCodigosOperacionEccDTO> findAllDatesCdisinCdsentidoByCdestadoasig(
      @Param("cdestadoasig") int cdestadoasig, @Param("isscrdiv") char isScrdiv);

  @Query("select distinct dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id from Tmct0anotacionecc an, Tmct0desgloseclitit dct where "
      + " an.fcontratacion = :fecontratacion and an.imtitulosdisponibles < dct.imtitulos and an.cdisin = :cdisin and an.cdsentido = :sentido "
      + " and dct.cdoperacionecc = an.cdoperacionecc "
      + " and dct.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado = :cdestadoasig "
      + " and dct.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.casepti = 'S' ")
  List<Tmct0bokId> findAllBookingIdByDesglosesSinTitulosDiponiblesAnotacion(
      @Param("fecontratacion") Date fecontratacion, @Param("cdisin") String cdisin, @Param("sentido") char sentido,
      @Param("cdestadoasig") int cdestadoasig);

  @Query(value = "SELECT DC.NUORDEN, DC.NBOOKING, DC.NUCNFCLT, DC.NUCNFLIQ  "
      + " FROM (SELECT B.NUORDEN, B.NBOOKING, B.FETRADE, B.CDISIN, B.CDTPOPER FROM TMCT0BOK B WHERE B.FEVALOR > :feliquidacionGt AND B.CDESTADOASIG > 0 AND B.CASEPTI = 'S') B "
      + " INNER JOIN (SELECT C.NUORDEN, C.NBOOKING, C.NUCNFCLT, C.NUCNFLIQ FROM TMCT0ALC C WHERE C.FEOPELIQ > :feliquidacionGt AND C.CDESTADOASIG > 0 AND C.CANON_CALCULADO='S') C ON B.NUORDEN = C.NUORDEN AND B.NBOOKING = C.NBOOKING "
      + " INNER JOIN (SELECT DC.NUORDEN, DC.NBOOKING, DC.NUCNFCLT, DC.NUCNFLIQ FROM TMCT0DESGLOSECAMARA DC WHERE DC.FELIQUIDACION >:feliquidacionGt AND DC.CDESTADOASIG = :cdestadoasig) DC ON C.NUORDEN = DC.NUORDEN AND C.NBOOKING = DC.NBOOKING AND C.NUCNFCLT = DC.NUCNFCLT AND C.NUCNFLIQ = DC.NUCNFLIQ ", nativeQuery = true)
  List<Object[]> findAllAlcIdByFeliquidacionGtAndCdestadoasigAndCamraCompensacionAndCasadoptiAndCanonCalculado(
      @Param("feliquidacionGt") Date feliquidacionGt, @Param("cdestadoasig") int cdestadoasig);

}
