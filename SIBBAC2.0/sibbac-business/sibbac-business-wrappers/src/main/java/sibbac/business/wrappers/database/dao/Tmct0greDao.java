package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0gre;
import sibbac.business.wrappers.database.model.Tmct0greId;

@Repository
public interface Tmct0greDao extends JpaRepository<Tmct0gre, Tmct0greId> {

  @Query("select g from Tmct0gre g where g.id.nuorden = :nuorden and g.id.nbooking = :nbooking ")
  List<Tmct0gre> findAllByNuordenAndNbooking(@Param("nuorden") String nuorden, @Param("nbooking") String nbooking);

}
