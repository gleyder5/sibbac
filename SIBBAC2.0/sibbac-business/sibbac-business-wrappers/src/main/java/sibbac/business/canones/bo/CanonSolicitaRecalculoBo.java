package sibbac.business.canones.bo;

import java.util.Date;
import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.canones.dao.CanonAgrupadoReglaDao;
import sibbac.business.canones.dao.CanonSolicitaRecalculoDao;
import sibbac.business.canones.model.CanonSolicitaRecalculo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0referenciaTitularBo;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.database.bo.AbstractBo;

/**
 * Objeto de reglas de negocio para la solicitud de recálculo de cánon.
 */
@Service
public class CanonSolicitaRecalculoBo extends AbstractBo<CanonSolicitaRecalculo, BigInteger, CanonSolicitaRecalculoDao> {

  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0referenciaTitularBo referenciaTitularBo;
  @Autowired
  private CanonAgrupadoReglaDao canonAgrupadoReglaDao;

  /**
   * Agrega, si es posible, una solicitud de recálculo de cánon
   * @param titular Referencia de titular.
   * @param fecha Fecha de contratación.
   * @param cdusuaud usuario de auditoría
   * @return verdadero en caso de que se haya agregado la solicitud
   */
  @Transactional
  public boolean agregaSolicitud(Date fecha, Long idTitular, String cdusuaud) {
    final CanonSolicitaRecalculo solicitud;
    if (idTitular == null || fecha == null) {
      return false;
    }
    if (existeSolicitud(idTitular.toString(), fecha)) {
      return false;
    }
    solicitud = new CanonSolicitaRecalculo();
    solicitud.setRefTitular(idTitular.toString());
    solicitud.setFecha(fecha);
    solicitud.setFhaudit(new Date());
    solicitud.setCdUsuaud(cdusuaud);
    return dao.save(solicitud).getId() != null;
  }

  @Transactional(isolation = Isolation.SERIALIZABLE, propagation = Propagation.REQUIRES_NEW)
  public void resuelveSolicitud(CanonSolicitaRecalculo solicitud) {
    LOG.trace("[resuelveSolicitud] inicio {}", solicitud);
    long idReferencia = Long.parseLong(solicitud.getRefTitular());
    LOG.trace("[resuelveSolicitud] buscando referencia titular id {} ...", idReferencia);
    Tmct0referenciatitular refTitular = referenciaTitularBo.findById(idReferencia);
    if (refTitular != null) {
      LOG.trace("[resuelveSolicitud] actualizando importes de canon agrupado para fecha: {}, titular: {}...",
          solicitud.getFecha(), refTitular.getCdreftitularpti());
      int importesActualizados = canonAgrupadoReglaDao.updateImportesaCero(solicitud.getFecha(),
          refTitular.getCdreftitularpti(), new Date(), "SIBBAC20");
      LOG.trace("[resuelveSolicitud] se han actualizado {} importes de canon agrupado para fecha: {}, titular: {}.",
          importesActualizados, solicitud.getFecha(), refTitular.getCdreftitularpti());

      LOG.trace("[resuelveSolicitud] mandando recalcular Alcs fecha: {}, titular: {}...", solicitud.getFecha(),
          refTitular.getCdreftitularpti());
      int alcsRecalcular = alcBo.updateFlagCanonCalculado(idReferencia, solicitud.getFecha(), 'N', "SIBBAC20");

      LOG.trace("[resuelveSolicitud] actualizados {} Alcs fecha: {}, titular: {}...", alcsRecalcular,
          solicitud.getFecha(), refTitular.getCdreftitularpti());
    }
    else {
      LOG.warn("[resuelveSolicitud] no encontrada id referencia titular {}", idReferencia);
    }
    LOG.trace("[resuelveSolicitud] borrando registro id {}...", solicitud.getId());
    dao.delete(solicitud.getId());
    LOG.trace("[resuelveSolicitud] fin {}", solicitud);
  }

  /**
   * Confirma la existencia de un registro de solicitud de recálculo de cánon a
   * través de un conteo en la tabla.
   * @param titular Referencia de titular.
   * @param fecha Fecha de contratación.
   * @return verdadero en caso de existir un registro, falso en caso contrario.
   */
  private boolean existeSolicitud(String titular, Date fecha) {
    return dao.conteoTitularFecha(titular, fecha) > 0;
  }

}
