package sibbac.business.wrappers.tasks;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.DateBuilder.IntervalUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.PartenonRecordBeanBo;
import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.OperacionEspecialFileReader;
import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.TitularesOperacionesEspecialesDbReader;
import sibbac.business.wrappers.database.bo.partenonReader.PartenonFileReader;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_ROUTING.NAME,
           name = Task.GROUP_ROUTING.JOB_OPERACIONES_ESPECIALES_CARGAR_TITULARES,
           interval = 1,
           delay = 30,
           intervalUnit = IntervalUnit.MINUTE)
@DisallowConcurrentExecution
public class TaskOperacionesEspecialesCargarTitulares extends WrapperTaskConcurrencyPrevent {

  /** Numero de operaciones a realizar en cada commit de base de datos. */
  @Value("${sibbac.wrappers.transaction.size}")
  private Integer transactionSize;

  @Value("${sibbac.wrappers.routingTitularesFile}")
  String path;

  @Qualifier(value = "operacionEspecialFileReader")
  @Autowired
  private OperacionEspecialFileReader opEspecialesFileReader;

  @Qualifier(value = "titularesOperacionesEspecialesDbReader")
  @Autowired
  private TitularesOperacionesEspecialesDbReader titularesOpEspecialesDbReader;

  @Qualifier(value = "partenonFileReader")
  @Autowired
  private PartenonFileReader partenonFileReader;

  @Autowired
  private PartenonRecordBeanBo partenonRecordBeanBo;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Override
  public void executeTask() {
    try {
      LOG.debug("[" + Task.GROUP_ROUTING.NAME + " : " + ":" + Task.GROUP_ROUTING.JOB_OPERACIONES_ESPECIALES_CARGAR_TITULARES + "] Init");
      File dir = new File(File.separator + FilenameUtils.getPath(path) + File.separator);
      LOG.debug("[" + Task.GROUP_ROUTING.NAME + " : " + ":" + Task.GROUP_ROUTING.JOB_OPERACIONES_ESPECIALES_CARGAR_TITULARES
                + "] Directorio: " + dir.getPath());

      File[] foundFiles = dir.listFiles(new FilenameFilter() {

        public boolean accept(File dir, String name) {
          return name.matches("ESPECIAL_.*ORMA.*\\.DAT");
        }
      });
      LOG.debug("[" + Task.GROUP_ROUTING.NAME + " : " + ":" + Task.GROUP_ROUTING.JOB_OPERACIONES_ESPECIALES_CARGAR_TITULARES
                + "] Encontrados " + foundFiles.length + " que empiecen con 'ESPECIAL_.*ORMA.*\\.DAT'");
      if (foundFiles.length > 0) {
        List<File> files = Arrays.asList(foundFiles);
        for (File file : files) {
          this.partenonFileReader.executeTask(file.getName(), false, StandardCharsets.ISO_8859_1);
        }
        // opEspecialesFileReader.readFromSeveralEspecialesFiles(files, true, "UTF-8");
      }
      this.loadSpecialHoldersFromDbAllocated();
    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
    }

  }

  /**
   * Carga de titulares especiales de operaciones que ya se han desglosado.
   * 
   * @throws SIBBACBusinessException
   * */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private void loadSpecialHoldersFromDbAllocated() throws SIBBACBusinessException {
    LOG.info("[loadSpecialHoldersFromDbAllocated] inicio.");

    try {

      this.titularesOpEspecialesDbReader.executeTask();

    } catch (Exception e) {
      throw new SIBBACBusinessException(e);
    }

    LOG.info("[loadSpecialHoldersFromDbAllocated] final.");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_ROUTING_TITULARES_ESPECIALES;
  }

}
