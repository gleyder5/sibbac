package sibbac.business.wrappers.tasks.thread;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.validation.BindException;

import sibbac.common.utils.FormatDataUtils;

public abstract class WrapperAbstractFieldSetMapper<T> implements FieldSetMapper<T>, WrapperFieldSetMapper<T> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(WrapperAbstractFieldSetMapper.class);

  public WrapperAbstractFieldSetMapper() {
  }

  public abstract WrapperFileReaderFieldEnumInterface[] getFields();

  public abstract T getNewInstance();

  public String[] getFieldNames() {
    WrapperFileReaderFieldEnumInterface[] fields = getFields();
    final String[] namesArray = new String[fields.length];
    for (int i = 0; i < fields.length; i++) {
      namesArray[i] = fields[i].getName();
    }
    return namesArray;
  }

  public int[] getLengths() {
    WrapperFileReaderFieldEnumInterface[] fields = getFields();
    final int[] lengthsArray = new int[fields.length];
    for (int i = 0; i < fields.length; i++) {
      lengthsArray[i] = fields[i].getLength();
    }
    return lengthsArray;
  }

  public String getFormat() {
    StringBuffer sb = new StringBuffer("");
    WrapperFileReaderFieldEnumInterface[] fields = getFields();
    for (WrapperFileReaderFieldEnumInterface wrapperFileReaderFieldEnumInterface : fields) {
      sb.append("%-").append(wrapperFileReaderFieldEnumInterface.getLength()).append("s");

    }
    return sb.toString();
  }

  /**
   * 
   * @param fieldLenghts
   * @return
   */
  public Range[] getColumnsRanges() {
    int[] lengths = getLengths();
    Range[] ranges = new Range[lengths.length];
    int distAux = 1;
    for (int i = 0; i < lengths.length; i++) {
      if (lengths[i] > 0) {
        Range auxRange = new Range(distAux, distAux + lengths[i] - 1);
        distAux += lengths[i];
        ranges[i] = auxRange;
      }
    }
    return ranges;
  } // convertLengthsIntoRanges

  @Override
  public LineAggregator<T> getLineAggregator() {
    FormatterLineAggregator<T> lineAggregator = new FormatterLineAggregator<T>();

    BeanWrapperFieldExtractor<T> fieldExtractor = new BeanWrapperFieldExtractor<T>();
    fieldExtractor.setNames(getFieldNames());
    lineAggregator.setFormat(getFormat());
    lineAggregator.setFieldExtractor(fieldExtractor);
    return lineAggregator;
  }

  /**
   * Este metodo realiza el mapeo del objeto T atraves de reflection
   * 
   * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
   */
  @Override
  public T mapFieldSet(FieldSet fieldSet) throws BindException {
    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] inicio");

    String methodName;
    T record = getNewInstance();

    if (record != null) {
      Map<String, Method> methods = new HashMap<String, Method>();

      Method[] methodsAr = record.getClass().getMethods();
      for (Method method : methodsAr) {
        methods.put(method.getName(), method);
      }

      LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] new instance of T, record: {}", record);
      if (getFields() != null && getFields().length > 0) {
        for (WrapperFileReaderFieldEnumInterface field : getFields()) {

          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] field: {}", field);
          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] field.name: {}", field.getName());
          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] field.length: {}", field.getLength());
          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] field.scale: {}", field.getScale());
          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] field.type: {}", field.getFielType());
          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] value assigned to field: '{}'",
                    fieldSet.readString(field.getName()));
          methodName = "set" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);

          LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] acces field of T by methodName: {}", methodName);
          try {
            Method methodSet = methods.get(methodName);

            if (methodSet == null) {
              methodSet = record.getClass().getMethod(methodName);
            }

            if (methodSet != null) {

              LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] method: {}", methodSet);

              Object value = null;
              if (field.getFielType() == WrapperFileReaderFieldType.NUMERIC) {
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] decimal read as bigdecimal...");
                BigDecimal valueAux = fieldSet.readBigDecimal(field.getName());
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] initial value readed: {}", valueAux);
                if (valueAux != null) {
                  if (field.getScale() > 0) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] applying scale...");
                    valueAux = valueAux.movePointLeft(field.getScale());
                  }

                  if (methodSet.getParameterTypes()[0].equals(BigDecimal.class)) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] parse bigdecimal...");
                    value = valueAux;
                  } else if (methodSet.getParameterTypes()[0].equals(Integer.class)) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] parse integer...");
                    value = Integer.parseInt(valueAux.toString());
                  } else if (methodSet.getParameterTypes()[0].equals(BigInteger.class)) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] parse biginteger...");
                    value = valueAux.toBigInteger();
                  } else if (methodSet.getParameterTypes()[0].equals(Long.class)) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] parse long...");
                    value = Long.parseLong(valueAux.toString());
                  } else if (methodSet.getParameterTypes()[0].equals(Short.class)) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] parse short...");
                    value = Short.parseShort(valueAux.toString());
                  } else if (methodSet.getParameterTypes()[0].equals(Double.class)) {
                    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] parse double...");
                    value = Double.parseDouble(valueAux.toString());
                  }
                  LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] final value readed: {}", value);
                }
              } else if (field.getFielType() == WrapperFileReaderFieldType.DATE) {

                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] read date...");
                String valueAux = fieldSet.readString(field.getName());
                if (!"00000000".equals(valueAux)) {
                  value = fieldSet.readDate(field.getName(), FormatDataUtils.DATE_FORMAT);
                } else {
                  LOG.warn("[WrapperAbstractFieldSetMapper :: mapFieldSet] value: {} is not valid for a DATE field.",
                           valueAux);
                }
              } else if (field.getFielType() == WrapperFileReaderFieldType.TIME) {
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] read date...");
                value = fieldSet.readDate(field.getName(), FormatDataUtils.TIME_FORMAT);
              } else if (field.getFielType() == WrapperFileReaderFieldType.TIMESTAMP) {
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] read date...");
                value = fieldSet.readDate(field.getName(), FormatDataUtils.TIMESTAMP_FORMAT);
              } else if (field.getFielType() == WrapperFileReaderFieldType.CHAR) {
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] read string...");
                String valueAux = fieldSet.readString(field.getName());
                if (valueAux != null && StringUtils.isNotBlank(valueAux.toString())) {
                  value = valueAux.toString().charAt(0);
                }
              } else {
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] default read as string...");
                value = fieldSet.readString(field.getName());
              }
              if (value != null) {
                LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] value : {}", value);
                methodSet.invoke(record, value);
              }
            } else {
              throw new BindException(record, "INCIDENCIA- Method '" + methodSet + "' not found: " + field + " ");
            }
          } catch (NoSuchMethodException e) {
            throw new BindException(record, "INCIDENCIA- Los fields del mapper y del objeto no coinciden, field: "
                                            + field + " " + e.getMessage());
          } catch (SecurityException e) {
            throw new BindException(record, "INCIDENCIA- Field: " + field + " " + e.getMessage());
          } catch (IllegalArgumentException e) {
            throw new BindException(record, "INCIDENCIA- Field: " + field + " " + e.getMessage());
          } catch (InvocationTargetException e) {
            throw new BindException(record, "INCIDENCIA- Field: " + field + " " + e.getMessage());
          } catch (IllegalAccessException e) {
            throw new BindException(record, "INCIDENCIA- Field: " + field + " " + e.getMessage());
          }
        }
      } else {
        throw new BindException(record, "INCIDENCIA- Fields nof found. Check getFields() implementation");
      }
      methods.clear();
    } else {
      throw new BindException(record,
                              "INCIDENCIA- New instance of record missing. Check getNewInstance() implementation");
    }
    LOG.trace("[WrapperAbstractFieldSetMapper :: mapFieldSet] final");
    return record;
  }
}
