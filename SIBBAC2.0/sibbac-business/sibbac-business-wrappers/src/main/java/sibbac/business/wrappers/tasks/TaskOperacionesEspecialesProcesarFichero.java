package sibbac.business.wrappers.tasks;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.operacionesEspecialesReader.OperacionEspecialFileReader;
import sibbac.business.wrappers.util.FicherosUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_WRAPPERS.NAME,
           name = Task.GROUP_WRAPPERS.JOB_OPERACION_ESPECIAL_PROCESAR_FICHERO_2040,
           interval = 5,
           delay = 30,
           intervalUnit = IntervalUnit.MINUTE)
@DisallowConcurrentExecution
public class TaskOperacionesEspecialesProcesarFichero extends WrapperTaskConcurrencyPrevent {

  /** Numero de operaciones a realizar en cada commit de base de datos. */
  @Value("${sibbac.wrappers.transaction.size}")
  private Integer transactionSize;

  @Qualifier(value = "operacionEspecialFileReader")
  @Autowired
  private OperacionEspecialFileReader opEspecialesFileReader;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Value("${MQ.partenon.desgloses.in.folder}")
  private String path;

  @Value("${sibbac.wrappers.operacionesEspeciales.file.pattern:{DESGLOSE_*.DAT*,MQ_PARTENON_DESGLOSES_SAN_IN*.DAT*}}")
  private String filePattern;

  @Value("${sibbac.wrappers.operacion.especial.desglose.process.num.files.check.procesado:100}")
  private int numeroFicherosCheckProcesado;

  @Override
  public void executeTask() throws Exception {
    LOG.debug("[" + Task.GROUP_WRAPPERS.NAME + " : " + ":" + Task.GROUP_WRAPPERS.JOB_OPERACION_ESPECIAL_PROCESAR_FICHERO_2040 + "] Init");
    File dir = new File(path + File.separator);
    LOG.debug("[" + Task.GROUP_WRAPPERS.NAME + " : " + ":" + Task.GROUP_WRAPPERS.JOB_OPERACION_ESPECIAL_PROCESAR_FICHERO_2040
              + "] Directorio: " + dir.getPath());
    Path rootPath = Paths.get(path);
    if (Files.notExists(rootPath)) {
      Files.createDirectories(rootPath);
    }
    int index = 0;
    boolean checkProcesadoFinal = true;
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(rootPath, filePattern);) {

      for (Path file : directoryStream) {
        index++;
        checkProcesadoFinal = true;
        try {
          opEspecialesFileReader.executeTask(file.toString(), false, StandardCharsets.ISO_8859_1);
          FicherosUtils.fileToTratados(opEspecialesFileReader.getFile().toFile());
        } catch (Exception e) {
          LOG.warn("[TaskOperacionesEspecialesProcesarFichero :: executeTask]Incidencia con el fichero : {}", file.toString(), e);
        }
        if (index % numeroFicherosCheckProcesado == 0) {
          checkProcesadoFinal = false;
          this.desgloseRecordBeanBo.revisarTitulosDesgloses();

        }
      }
    }
    // TODO revisar si es necesario o mas optimo pasar esta revision dentro del bucle para que al terminar de procesar
    // cada fichero se cambie a estado 2 o 1
    if (checkProcesadoFinal) {
      this.desgloseRecordBeanBo.revisarTitulosDesgloses();
    }

    LOG.debug("[" + Task.GROUP_WRAPPERS.NAME + " : " + ":" + Task.GROUP_WRAPPERS.JOB_OPERACION_ESPECIAL_PROCESAR_FICHERO_2040 + "] Fin");
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_OPERACIONES_ESPECIALES;
  }

}
