package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.model.PartenonRecordBean;

public interface PartenonRecordBeanDao extends JpaRepository<PartenonRecordBean, Long> {

  @Query("SELECT p FROM PartenonRecordBean p JOIN FETCH p.desgloseRecordBean pDesg WHERE pDesg.porcesado = :pprocesado ORDER BY p.numOrden ASC, p.idSenc ASC, p.id ASC ")
  public List<PartenonRecordBean> findAllTitularesOperacionesEspecialesDesglosadas(@Param("pprocesado") int procesado,
                                                                                   Pageable page);

  @Query("SELECT p FROM PartenonRecordBean p WHERE p.numOrden in :listnureford ORDER BY p.numOrden ASC, p.idSenc ASC, p.id ASC ")
  public List<PartenonRecordBean> findAllTitularesOperacionesByListNureford(@Param("listnureford") List<String> procesado);
  
  @Query("SELECT p.ccv FROM PartenonRecordBean p WHERE p.numOrden = :pnureford ")
  public List<String> findAllCcvByNureford(@Param("pnureford") String nureford);
  
  
  @Query("SELECT p.numOrden, p.ccv FROM PartenonRecordBean p WHERE p.numOrden in :pnureford ")
  public List<Object[]> findAllCcvByListNureford(@Param("pnureford") List< String> nureford);

}
