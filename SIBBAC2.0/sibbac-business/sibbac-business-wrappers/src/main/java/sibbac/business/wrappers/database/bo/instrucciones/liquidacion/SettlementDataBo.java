package sibbac.business.wrappers.database.bo.instrucciones.liquidacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.bo.Tmct0blqBo;
import sibbac.business.fase0.database.bo.Tmct0brkBo;
import sibbac.business.fase0.database.bo.Tmct0cleInternacionalBo;
import sibbac.business.fase0.database.bo.Tmct0cleNacionalBo;
import sibbac.business.fase0.database.bo.Tmct0clmBo;
import sibbac.business.fase0.database.bo.Tmct0ilqBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.fase0.database.model.Tmct0blq;
import sibbac.business.fase0.database.model.Tmct0brk;
import sibbac.business.fase0.database.model.Tmct0cleInternacional;
import sibbac.business.fase0.database.model.Tmct0cleNacional;
import sibbac.business.fase0.database.model.Tmct0clm;
import sibbac.business.fase0.database.model.Tmct0ilq;
import sibbac.business.wrappers.database.bo.Tmct0cliBo;
import sibbac.business.wrappers.database.bo.Tmct0ordBo;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.ClientDniName;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.Holder;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.HolderTipoOrigenDatosType;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBicName;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBicNameAllocation;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementChainBroker;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementData;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataBroker;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClearer;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClearerDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClearerInstructionsDTO;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClient;
import sibbac.business.wrappers.database.dto.instrucciones.liquidacion.SettlementDataClientDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0tfi;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

@Service
public class SettlementDataBo {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(SettlementDataBo.class);

  @Autowired
  private Tmct0cleNacionalBo tmct0cleNacionalBo;

  @Autowired
  private Tmct0cleInternacionalBo tmct0cleIternacionalBo;

  @Autowired
  private Tmct0clmBo entidadesBo;

  @Autowired
  private Tmct0ilqBo tmct0ilqBo;

  @Autowired
  private Tmct0tfiBo tmct0tfiBo;

  @Autowired
  private Tmct0cliBo tmct0cliBo;

  @Autowired
  private Tmct0ordBo tmct0ordBo;

  @Autowired
  private Tmct0blqBo tmct0blqBo;

  @Autowired
  private Tmct0brkBo tmct0brkBo;

  @Autowired
  private Tmct0xasBo tmct0xasBo;

  @Autowired
  private Tmct0aliBo tmct0aliBo;

  public SettlementDataBo() {
  }

  public SettlementData getSettlementDataNacionalCuenta(Tmct0alo alo) throws SIBBACBusinessException {

    LOG.trace("[SettlementDataBo :: getSettlementDataNacional] inicio.");
    String cdaliassAlo = alo.getCdalias();
    if (cdaliassAlo.contains("|")) {
      cdaliassAlo = cdaliassAlo.split("\\|")[1];
    }
    String cdmercadNacional = "";
    String centroNacional = "";
    Date feoperac = alo.getFeoperac();
    String cdclearerOrd = tmct0ordBo.findCdcleareById(alo.getId().getNuorden());
    String cdholder = alo.getCdholder();

    SettlementData res = this.getSettlementData(cdaliassAlo, cdmercadNacional, centroNacional, feoperac, cdholder,
        cdclearerOrd, true);
    LOG.trace("[SettlementDataBo :: getSettlementDataNacional] fin {}.", res);
    return res;
  }

  public SettlementData getSettlementDataNacional(Tmct0alo alo) throws SIBBACBusinessException {

    LOG.trace("[SettlementDataBo :: getSettlementDataNacional] inicio.");
    String cdaliassAlo = alo.getCdalias();
    String cdmercadNacional = "";
    String centroNacional = "";
    Date feoperac = alo.getFeoperac();
    String cdclearerOrd = tmct0ordBo.findCdcleareById(alo.getId().getNuorden());
    String cdholder = alo.getCdholder();

    SettlementData res = this.getSettlementData(cdaliassAlo, cdmercadNacional, centroNacional, feoperac, cdholder,
        cdclearerOrd, true);
    LOG.trace("[SettlementDataBo :: getSettlementDataNacional] fin {}.", res);
    return res;
  }

  public SettlementData getSettlementDataInternacional(Tmct0alo alo) throws SIBBACBusinessException {

    LOG.trace("[SettlementDataBo :: getSettlementDataInternacional] inicio.");
    String cdaliassAlo = alo.getCdalias();
    String cdmercadInternacional = "";
    String centroInternacional = "MMEE";
    Date feoperac = alo.getFeoperac();
    String cdclearerOrd = tmct0ordBo.findCdcleareById(alo.getId().getNuorden());
    String cdholder = alo.getCdholder();
    SettlementData res = this.getSettlementData(cdaliassAlo, cdmercadInternacional, centroInternacional, feoperac,
        cdholder, cdclearerOrd, false);

    LOG.trace("[SettlementDataBo :: getSettlementDataInternacional] fin {}.", res);
    return res;
  }

  private SettlementData getSettlementData(String cdaliassAlo, String cdmercad, String centro, Date feoperac,
      String cdholder, String cdclearerOrd, boolean nacional) throws SIBBACBusinessException {
    LOG.trace("[SettlementDataBo :: getSettlementData] inicio.");
    SettlementData res = new SettlementData();

    Tmct0cli tmct0cli;
    SettlementDataClearer sdc;

    List<Tmct0cleNacional> listTmct0cleNacional;
    List<Tmct0ilq> listIlqs;
    List<Tmct0tfi> listTfis;
    List<Holder> holders;
    Tmct0ilq tmct0ilq = null;
    Tmct0cleNacional tmct0cle = null;
    Tmct0cleNacional tmct0cleCustod = null;

    String cdaliass = cdaliassAlo;
    String cdsubcta = "";
    BigDecimal feoperacBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(feoperac));

    String cdclearer = cdclearerOrd;
    if (cdaliassAlo.contains("|")) {
      String[] ctaSubctaArray = cdaliassAlo.split("\\|");
      cdaliass = StringUtils.trim(ctaSubctaArray[1]);
      cdsubcta = StringUtils.trim(ctaSubctaArray[0]);
    }

    listIlqs = getListTmct0ilqs(cdaliass, cdsubcta, centro, cdmercad, feoperacBd, nacional);

    if (CollectionUtils.isNotEmpty(listIlqs)) {
      res.setDataIL(true);
      tmct0ilq = listIlqs.get(0);
    }

    if (tmct0ilq != null) {
      res.setTmct0ilq(new SettlementDataClearerInstructionsDTO(tmct0ilq));
      res.setDataIL(true);
      if (StringUtils.isBlank(cdclearer) && StringUtils.isNotBlank(tmct0ilq.getId().getCdcleare())) {
        cdclearer = tmct0ilq.getId().getCdcleare();
      }
    }
    else {
      res.getTmct0ilq().setCdaliass(cdaliass);
      res.getTmct0ilq().setCdsubcta(cdsubcta);
      res.setDataIL(false);
    }
    if (res.isDataIL()) {
      if (StringUtils.isNotBlank(cdclearer)) {
        if (nacional) {
          listTmct0cleNacional = tmct0cleNacionalBo.findByFechaAndCdcleare(feoperac, cdclearer);
          if (CollectionUtils.isNotEmpty(listTmct0cleNacional)) {
            if (listTmct0cleNacional.size() > 1) {
              LOG.warn("[SettlementDataBo :: getSettlementData] INCIDENCIA- CLEARER MULTIPLE.");
              res.setClearerMultiple(true);
            }
            tmct0cle = listTmct0cleNacional.get(0);
            res.setDataClearer(true);
            res.setClearer(new SettlementDataClearerDTO(tmct0cle));
          }
          else {
            res.setDataClearer(false);
          }
        }
        else {
          sdc = getSettlementDataClearer(cdaliassAlo, tmct0ilq.getId().getCdcleare(), centro, cdmercad,
              tmct0ilq.getTpsetcle(), feoperacBd.intValue());
          if (sdc.isDataClearer()) {
            res.setClearer(sdc.getTmct0cle());
            res.setDataClearer(sdc.isDataClearer());
            res.setClearerMultiple(sdc.isClearerMultiple());
          }
        }
      }

      if (tmct0ilq != null && StringUtils.isNotBlank(tmct0ilq.getCdcustod())) {
        if (nacional) {
          listTmct0cleNacional = tmct0cleNacionalBo.findByFechaAndCdcleare(feoperac, tmct0ilq.getCdcustod());
          if (CollectionUtils.isNotEmpty(listTmct0cleNacional)) {
            tmct0cleCustod = listTmct0cleNacional.get(0);
            res.setDataCustodian(true);
            res.setCustodian(new SettlementDataClearerDTO(tmct0cleCustod));
          }
          else {
            res.setDataCustodian(false);
          }
        }
        else {
          sdc = getSettlementDataClearer(cdaliassAlo, tmct0ilq.getCdcustod(), centro, cdmercad, tmct0ilq.getTpsetcus(),
              feoperacBd.intValue());
          if (sdc.isDataClearer()) {
            res.setCustodian(sdc.getTmct0cle());
            res.setDataCustodian(sdc.isDataClearer());
          }
        }
      }

      if (!nacional && StringUtils.isNotBlank(tmct0ilq.getCdbrocli())) {
        tmct0cli = tmct0cliBo.findByCdbrocliAndFecha(tmct0ilq.getCdbrocli(), feoperacBd.intValue());
        if (tmct0cli != null) {
          SettlementDataClient sdcli = new SettlementDataClient();
          sdcli.setDataClient(true);
          sdcli.setTmct0cli(new SettlementDataClientDTO(tmct0cli));
          res.setDataClient(sdcli);
          // TODO FALTA POR IMPLEMENTAR LA PARTE DE DIRECCIONES Y ESTADISTICOS
          // // Recuperar Datos de Estadisticas del Alias
          // listTmct0est =
          // Tools_tmct0est.getListTmct0est(settlementData.getTmct0ilq().getCdbrocli(),
          // settlementData.getTmct0ilq().getCdaliass(), fetrade, hpm);
          // listTmct0est = (List<Tmct0est>)
          // hpm.getPersistenceManager().detachCopyAll(listTmct0est);
          // if ((listTmct0est != null) && (!listTmct0est.isEmpty())) {
          // settlementData.setListTmct0est2( listTmct0est);
          // settlementData.setDataEST(true);
          // }
          // // Recuperar Direcciones de la IL
          // listTmct0dir =
          // Tools_tmct0dir.getListTmct0dir(settlementData.getTmct0ilq().getCdbrocli(),
          // settlementData.getTmct0ilq().getCdaliass(),
          // settlementData.getTmct0ilq().getCdsubcta(), fetrade, hpm);
          // listTmct0dir = (List<Tmct0dir>)
          // hpm.getPersistenceManager().detachCopyAll(listTmct0dir);
          // if ((listTmct0dir != null) && (!listTmct0dir.isEmpty())) {
          // settlementData.setListTmct0dir2(listTmct0dir);
          // settlementData.setDataDIR(true);
          // }
        }
      }
      else {
        LOG.debug("[SettlementDataBo :: getSettlementData] tmct0ilq.cdbrocli is null...");
      }

      if (StringUtils.isBlank(cdholder)) {

        LOG.debug("[SettlementDataBo :: getSettlementData] Buscando tmct0tfi's...");
        if (StringUtils.isNotBlank(res.getTmct0ilq().getCdaliass())) {
          if (nacional) {
            listTfis = tmct0tfiBo.findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(res.getTmct0ilq()
                .getCdaliass(), res.getTmct0ilq().getCdsubcta(), centro, cdmercad, feoperacBd);
          }
          else {
            final boolean desglosa = this.desgloseByAlias(cdaliass, feoperacBd);

            listTfis = this.getListTmct0tfiInternacional(cdaliass, cdsubcta, "MMEE", "", feoperacBd, desglosa);
            if (listTfis == null || listTfis.size() == 0) {
              listTfis = this.getListTmct0tfiInternacional(cdaliass, cdsubcta, "", "", feoperacBd, desglosa);
            }
          }

          if (CollectionUtils.isNotEmpty(listTfis)) {
            res.getListHolder().addAll(this.getListHoldersFromTmct0tfi(listTfis, feoperacBd));
            if (CollectionUtils.isNotEmpty(res.getListHolder())) {
              res.setHolder(res.getListHolder().get(0));
              res.setDataTFI(true);
            }

          }
        }
      }
      else {
        LOG.debug("[SettlementDataBo :: getSettlementData] Buscando holder {} desglose |FINALHOLDER.", cdholder);
        holders = tmct0tfiBo.findAllHoldersByCdholder(cdholder, feoperacBd);
        if (CollectionUtils.isNotEmpty(holders)) {
          holders.get(0).setTptiprep('T');
          res.setHolder(holders.get(0));
          res.getListHolder().add(holders.get(0));
          res.setDataTFI(true);
        }
      }
    }
    LOG.trace("[SettlementDataBo :: getSettlementData] fin {}.", res);
    return res;
  }

  public List<Holder> getListHoldersInternacional(Tmct0alo alo) throws SIBBACBusinessException {
    return this.getListHoldersInternacional(alo, this.desglosaAliasInternacional(alo));
  }

  public List<Holder> getListHoldersInternacional(Tmct0alo alo, boolean desglosa) throws SIBBACBusinessException {
    String cdaliassAlo = alo.getCdalias().trim();
    Date feoperac = alo.getFeoperac();
    String cdaliass = cdaliassAlo;
    String cdsubcta = "";
    BigDecimal feoperacBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(feoperac));

    if (cdaliassAlo.contains("|")) {
      String[] ctaSubctaArray = cdaliassAlo.split("\\|");
      cdaliass = StringUtils.trim(ctaSubctaArray[1]);
      cdsubcta = StringUtils.trim(ctaSubctaArray[0]);
    }
    List<Tmct0tfi> listTfi = this.getListTmct0tfiInternacional(cdaliass, cdsubcta, "MMEE", "", feoperacBd, desglosa);
    if (listTfi == null || listTfi.size() == 0) {
      listTfi = this.getListTmct0tfiInternacional(cdaliass, cdsubcta, "", "", feoperacBd, desglosa);
    }

    return this.getListHoldersFromTmct0tfi(listTfi, feoperacBd);
  }

  private List<Holder> getListHoldersFromTmct0tfi(List<Tmct0tfi> listTfis, BigDecimal feoperacBd) {
    List<Holder> holders = new ArrayList<Holder>();
    List<Holder> holdersAux;
    if (CollectionUtils.isNotEmpty(listTfis)) {

      for (Tmct0tfi tmct0tfi : listTfis) {
        holdersAux = tmct0tfiBo.findAllHoldersByCddclave(StringUtils.trim(tmct0tfi.getId().getCddclave()), feoperacBd);
        if (CollectionUtils.isNotEmpty(holdersAux)) {
          if (StringUtils.isNotBlank(tmct0tfi.getTptiprep())) {
            holdersAux.get(0).setTptiprep(tmct0tfi.getTptiprep().charAt(0));
          }
          // TIPO DE PROCEDENCIA I-ILQ-R-ROUTING-M-MANUAL/PANTALLA
          holdersAux.get(0).setTpproced(HolderTipoOrigenDatosType.INSTRUCCIONES);
          holders.add(holdersAux.get(0));
        }
        else {
          LOG.warn("[getListHoldersFromTmct0tfi] titular no encontrado: {}", tmct0tfi.getId().getCddclave());
        }
      }

    }
    return holders;
  }

  public boolean desglosaAliasInternacional(Tmct0bok bok) throws SIBBACBusinessException {
    String cdalias = bok.getCdalias();
    BigDecimal feoperacBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(bok
        .getFetrade()));
    return this.desgloseByAlias(cdalias, feoperacBd);
  }

  public boolean desglosaAliasInternacional(Tmct0alo alo) throws SIBBACBusinessException {
    String cdalias = alo.getCdalias();
    BigDecimal feoperacBd = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alo
        .getFeoperac()));
    return this.desgloseByAlias(cdalias, feoperacBd);
  }

  private boolean desgloseByAlias(String cdaliass, BigDecimal feoperacBd) {
    if (cdaliass.contains("|")) {
      String[] ctaSubctaArray = cdaliass.split("\\|");
      cdaliass = StringUtils.trim(ctaSubctaArray[1]);
    }
    Tmct0ali ali = tmct0aliBo.findByCdaliassFechaActual(cdaliass, feoperacBd.intValue());
    return ali != null && ali.getDesglose() != null && ali.getDesglose() == 'Y';
  }

  /*
   * private List<Tmct0tfi> getListTmct0tfiInternacional(String cdaliass, String
   * cdsubcta, String cdmercad, BigDecimal feoperacBd, boolean desglosa) {
   * 
   * return getListTmct0tfiInternacional(cdaliass, cdsubcta, "", cdmercad,
   * feoperacBd, desglosa); }
   */

  private List<Tmct0tfi> getListTmct0tfiInternacional(String cdaliass, String cdsubcta, String centro, String cdmercad,
      BigDecimal feoperacBd, boolean desglosa) {
    List<Tmct0tfi> listTfis;
    if (desglosa) {
      listTfis = tmct0tfiBo.findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(cdaliass, cdsubcta, centro,
          cdmercad, feoperacBd);
    }
    else {
      listTfis = tmct0tfiBo.findAllByCdaliassAndCdsubctaAndCentroAndCdmercadAndFetrade(cdaliass, "", centro, cdmercad,
          feoperacBd);
    }

    return listTfis;
  }

  private List<Tmct0ilq> getListTmct0ilqs(String cdaliass, String cdsubcta, String centro, String cdmercad,
      BigDecimal feoperacBd, boolean nacional) {

    List<Tmct0ilq> listIlqs = tmct0ilqBo
        .findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(cdaliass, cdsubcta, centro,
            cdmercad, feoperacBd);

    if (CollectionUtils.isEmpty(listIlqs)) {

      if (nacional) {
        if (StringUtils.isNotBlank(cdsubcta)) {
          listIlqs = tmct0ilqBo.findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(
              cdaliass, "", centro, cdmercad, feoperacBd);

        }
      }
      else {
        listIlqs = tmct0ilqBo.findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(
            cdaliass, cdsubcta, "", cdmercad, feoperacBd);
        if (CollectionUtils.isEmpty(listIlqs)) {

          listIlqs = tmct0ilqBo.findAllByIdCdaliassAndIdCdsubctaAndIdCentroAndIdCdmercadAndLteFhinicioAndLtFhfinal(
              cdaliass, cdsubcta, "", "", feoperacBd);

        }
      }
    }
    return listIlqs;
  }

  private SettlementDataClearer getSettlementDataClearerInternal(String cdaliass, String cdsubcta, String cdcleare,
      String centro, String cdmercad, String tpsettle, Integer fetrade) {
    LOG.debug("[SettlementDataBo :: getSettlementDataClearerInternal] {}-{}-{}-{}-{}-{}", cdaliass, cdcleare, centro,
        cdmercad, tpsettle, fetrade);
    SettlementDataClearer sdc = new SettlementDataClearer();

    List<Tmct0cleInternacional> tmct0cles = tmct0cleIternacionalBo
        .findAllTmct0cleByCdcleareAndCentroAndCdmercadAndCdaliassAndCdsubctaAndTpsettleAndFecha(cdcleare, centro,
            cdmercad, cdaliass, cdsubcta, tpsettle, fetrade);

    if (CollectionUtils.isNotEmpty(tmct0cles)) {
      sdc.setTmct0cle(new SettlementDataClearerDTO(tmct0cles.get(0)));
      sdc.setErrormsg("");
      sdc.setDataClearer(true);
      if (tmct0cles.size() > 1) {
        sdc.setClearerMultiple(true);
      }
    }
    else {
      LOG.debug("[SettlementDataBo :: getSettlementDataClearerInternal] No Clearer instructions found.");
      sdc.setErrormsg("No Clearer instructions found.");
    }
    return sdc;
  }

  private SettlementDataClearer getSettlementDataClearerInternal2(String cdaliass, String cdsubcta, String cdcleare,
      String centro, String cdmercad, String tpsettle, Integer fetrade) {

    SettlementDataClearer sdc = this.getSettlementDataClearerInternal(cdaliass, cdsubcta, cdcleare, centro, cdmercad,
        tpsettle, fetrade);
    if (!sdc.isDataClearer() && StringUtils.isNotBlank(centro)) {
      sdc = this.getSettlementDataClearerInternal(cdaliass, cdsubcta, cdcleare, "", cdmercad, tpsettle, fetrade);
    }
    if (!sdc.isDataClearer() && StringUtils.isNotBlank(cdmercad)) {
      sdc = this.getSettlementDataClearerInternal(cdaliass, cdsubcta, cdcleare, "", "", tpsettle, fetrade);
    }

    return sdc;
  }

  private SettlementDataClearer getSettlementDataClearer(String cdaliass, String cdsubcta, String cdcleare,
      String centro, String cdmercad, String tpsettle, Integer fetrade) {

    SettlementDataClearer sdc = getSettlementDataClearerInternal2(cdaliass, cdsubcta, cdcleare, centro, cdmercad,
        tpsettle, fetrade);
    if (!sdc.isDataClearer() && StringUtils.isNotBlank(cdsubcta)) {
      sdc = getSettlementDataClearerInternal2(cdaliass, "", cdcleare, centro, cdmercad, tpsettle, fetrade);
    }

    if (!sdc.isDataClearer() && StringUtils.isNotBlank(cdaliass)) {
      sdc = getSettlementDataClearerInternal2("", "", cdcleare, centro, cdmercad, tpsettle, fetrade);
    }
    return sdc;
  }

  /**
   * @param cdaliass cdsubcta|cta
   * @param cdcleare
   * @param centro
   * @param cdmercad
   * @param tpsettle
   * @param fetrade
   * @return
   */
  private SettlementDataClearer getSettlementDataClearer(String cdaliass, String cdcleare, String centro,
      String cdmercad, String tpsettle, Integer fetrade) {
    LOG.debug("[SettlementDataBo :: getSettlementDataClearer] {}-{}-{}-{}-{}-{}", cdaliass, cdcleare, centro, cdmercad,
        tpsettle, fetrade);
    String cdsubcta = "";
    if (cdaliass.contains("|")) {
      cdsubcta = cdaliass.split("\\|")[0];
      cdaliass = cdaliass.split("\\|")[1];
    }
    SettlementDataClearer sdc = getSettlementDataClearer(cdaliass, cdsubcta, cdcleare, centro, cdmercad, tpsettle,
        fetrade);
    return sdc;
  }

  private SettlementDataBroker getSettlementDataBrokerInternal(String cdbroker, String centro, String cdmercad,
      String tpsetcle, BigDecimal fecha) {
    SettlementDataBroker sdb = new SettlementDataBroker();
    LOG.debug("[SettlementDataBo :: getSettlementDataBrokerInternal] {}-{}-{}-{}-{}", cdbroker, centro, cdmercad,
        tpsetcle, fecha);
    List<Tmct0blq> list = tmct0blqBo.findAllTmct0blqByCdbrokerAndCentroAndCdmercadAndTpsettleAndFecha(cdbroker, centro,
        cdmercad, tpsetcle, fecha);

    if (CollectionUtils.isNotEmpty(list)) {
      sdb.setTmct0blq(list.get(0));
      sdb.setDataIL(true);
      LOG.debug("[SettlementDataBo :: getSettlementDataBrokerInternal] buscando broker: {}", sdb.getTmct0blq().getId()
          .getCdbroker());
      Tmct0brk tmct0brk = tmct0brkBo.findByCdbroker(sdb.getTmct0blq().getId().getCdbroker());
      if (tmct0brk != null) {
        sdb.setTmct0brk(tmct0brk);
        sdb.setDataBroker(true);
      }
    }

    return sdb;
  }

  public SettlementDataBroker getSettlementDataBrokerInternacional(String cdbroker, String cdmercad, String tpsetcle,
      BigDecimal fecha) {
    LOG.debug("[SettlementDataBo :: getSettlementChainBrokerInternacional] buscando  cdbroker mtf: {}...", cdbroker);
    String cdbrokerXas = tmct0xasBo.getCdbrokerMtf(cdbroker);
    if (StringUtils.isNotBlank(cdbrokerXas)) {
      LOG.debug("[SettlementDataBo :: getSettlementChainBrokerInternacional] buscando cdbroker {} mtf: {}...",
          cdbroker, cdbrokerXas);
      cdbroker = cdbrokerXas;
    }

    String cdmercadXas = tmct0xasBo.getCdmercadMtf(cdmercad);
    if (StringUtils.isNotBlank(cdmercadXas)) {
      LOG.debug("[SettlementDataBo :: getSettlementChainBrokerInternacional] buscando cdmercad {} mtf: {}...",
          cdmercad, cdmercadXas);
      cdmercad = cdmercadXas;
    }

    SettlementDataBroker sdb = getSettlementDataBrokerInternal(cdbroker, "MMEE", cdmercad, tpsetcle, fecha);
    if (!sdb.isDataIL()) {
      sdb = getSettlementDataBrokerInternal(cdbroker, "", "", tpsetcle, fecha);
    }

    return sdb;
  }

  public SettlementChainBroker getSettlementChainBrokerInternacional(String cdbroker, String cdmercad, String tpsetcle,
      BigDecimal fecha) {
    LOG.debug("[SettlementDataBo :: getSettlementChainBrokerInternacional] buscando cocenliq cdbroker: {}...", cdbroker);
    String cocenliq = tmct0brkBo.findCocenliqByCdbroker(cdbroker);
    if (StringUtils.isNotBlank(cocenliq)) {
      LOG.debug("[SettlementDataBo :: getSettlementChainBrokerInternacional] encontrado cocenliq {} cdbroker: {}...",
          cocenliq, cdbroker);
      cdbroker = cocenliq;

    }
    SettlementChainBroker scb = new SettlementChainBroker(getSettlementDataBrokerInternacional(cdbroker, cdmercad,
        tpsetcle, fecha));

    return scb;

  }

  public SettlementChainBicNameAllocation getSettlementChainBicNameAllocation(Tmct0alo tmct0alo)
      throws SIBBACBusinessException {
    LOG.debug("[SettlementDataBo :: getSettlementChainBicNameAllocation] inicio...");
    SettlementChainBicNameAllocation scbna = new SettlementChainBicNameAllocation();
    BigDecimal fecha = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(tmct0alo
        .getFeoperac()));
    // CLEARER
    SettlementDataClearer settlementChainClearer = getSettlementDataClearer(tmct0alo.getCdalias(),
        tmct0alo.getCdcleare(), "MMEE", tmct0alo.getTmct0bok().getTmct0ord().getCdmercad(), tmct0alo.getTpsetcle(),
        fecha.intValue());

    // sclname & sclbic
    scbna.setSclname(settlementChainClearer.getTmct0cle().getDscleare());
    scbna.setSclbic(settlementChainClearer.getTmct0cle().getBiccle());

    // scllcname & scllcbic
    scbna.setScllcname(settlementChainClearer.getTmct0cle().getDslocal());
    scbna.setScllcbic(settlementChainClearer.getTmct0cle().getBiclocal());

    // sclgcname & sclgcbic
    scbna.setSclgcname(settlementChainClearer.getTmct0cle().getDsglobal());
    scbna.setSclgcbic(settlementChainClearer.getTmct0cle().getBicglobal());

    // sclbename & sclbebic
    scbna.setSclbename(settlementChainClearer.getTmct0cle().getDsbenefi());
    scbna.setSclbebic(settlementChainClearer.getTmct0cle().getBicbenefi());

    // CUSTODIO
    settlementChainClearer = getSettlementDataClearer(tmct0alo.getCdalias(), tmct0alo.getCdcustod(), "MMEE", tmct0alo
        .getTmct0bok().getTmct0ord().getCdmercad(), tmct0alo.getTpsetcus(), fecha.intValue());

    // scuname & scubic
    scbna.setScuname(settlementChainClearer.getTmct0cle().getDscleare());
    scbna.setScubic(settlementChainClearer.getTmct0cle().getBiccle());

    // sculcname & sculcbic
    scbna.setSculcname(settlementChainClearer.getTmct0cle().getDslocal());
    scbna.setSculcbic(settlementChainClearer.getTmct0cle().getBiclocal());

    // scugcname & scugcbic
    scbna.setScugcname(settlementChainClearer.getTmct0cle().getDsglobal());
    scbna.setScugcbic(settlementChainClearer.getTmct0cle().getBicglobal());

    // scubename & scubebic
    scbna.setScubename(settlementChainClearer.getTmct0cle().getDsbenefi());
    scbna.setScubebic(settlementChainClearer.getTmct0cle().getBicbenefi());
    LOG.debug("[SettlementDataBo :: getSettlementChainBicNameAllocation] fin...");
    return scbna;
  }

  public SettlementChainBicNameAllocation getSettlementChainBicNameAllocation(Tmct0cta tmct0cta)
      throws SIBBACBusinessException {
    SettlementChainBicNameAllocation scbna = new SettlementChainBicNameAllocation();
    BigDecimal fecha = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(tmct0cta
        .getFetrade()));
    // CLEARER
    SettlementDataClearer settlementChainClearer = getSettlementDataClearer(tmct0cta.getCdalias(),
        tmct0cta.getCdcleare(), "MMEE", tmct0cta.getId().getCdmercad(), tmct0cta.getTpsetcle(), fecha.intValue());

    // sclname & sclbic
    scbna.setSclname(settlementChainClearer.getTmct0cle().getDscleare());
    scbna.setSclbic(settlementChainClearer.getTmct0cle().getBiccle());

    // scllcname & scllcbic
    scbna.setScllcname(settlementChainClearer.getTmct0cle().getDslocal());
    scbna.setScllcbic(settlementChainClearer.getTmct0cle().getBiclocal());

    // sclgcname & sclgcbic
    scbna.setSclgcname(settlementChainClearer.getTmct0cle().getDsglobal());
    scbna.setSclgcbic(settlementChainClearer.getTmct0cle().getBicglobal());

    // sclbename & sclbebic
    scbna.setSclbename(settlementChainClearer.getTmct0cle().getDsbenefi());
    scbna.setSclbebic(settlementChainClearer.getTmct0cle().getBicbenefi());

    // CUSTODIO
    settlementChainClearer = getSettlementDataClearer(tmct0cta.getCdalias(), tmct0cta.getCdcustod(), "MMEE", tmct0cta
        .getId().getCdmercad(), tmct0cta.getTpsetcus(), fecha.intValue());

    // scuname & scubic
    scbna.setScuname(settlementChainClearer.getTmct0cle().getDscleare());
    scbna.setScubic(settlementChainClearer.getTmct0cle().getBiccle());

    // sculcname & sculcbic
    scbna.setSculcname(settlementChainClearer.getTmct0cle().getDslocal());
    scbna.setSculcbic(settlementChainClearer.getTmct0cle().getBiclocal());

    // scugcname & scugcbic
    scbna.setScugcname(settlementChainClearer.getTmct0cle().getDsglobal());
    scbna.setScugcbic(settlementChainClearer.getTmct0cle().getBicglobal());

    // scubename & scubebic
    scbna.setScubename(settlementChainClearer.getTmct0cle().getDsbenefi());
    scbna.setScubebic(settlementChainClearer.getTmct0cle().getBicbenefi());
    return scbna;
  }

  public SettlementChainBicName getSettlementChainBicName(String cdcleare, Integer fecha) {
    SettlementChainBicName res = null;
    // List<Tmct0cleInternacional> cleares =
    // tmct0cleIternacionalBo.findByFechaAndCdcleare(fecha, cdcleare);
    List<Tmct0clm> cleares = entidadesBo.findAllByFechaAndCdcleare(fecha, cdcleare);
    if (CollectionUtils.isNotEmpty(cleares)) {
      res = new SettlementChainBicName(cleares.get(0).getBiccle(), cleares.get(0).getDscleare());
    }
    else {
      res = new SettlementChainBicName("", "");
    }
    return res;
  }

  public ClientDniName getClientDniName(String cdholder, BigDecimal fecha) {
    List<Holder> holders = tmct0tfiBo.findAllHoldersByCdholder(cdholder, fecha);
    if (CollectionUtils.isNotEmpty(holders)) {
      return new ClientDniName(holders.get(0));
    }
    else {
      return new ClientDniName();
    }
  }

  public SettlementData getSettlmentDataNacional(Map<String, SettlementData> cached, Tmct0alo alo)
      throws SIBBACBusinessException {
    String key = getKeySettlementDataNacional(alo);
    SettlementData settlementData = cached.get(key);
    if (settlementData == null) {
      synchronized (cached) {
        settlementData = cached.get(key);
        if (settlementData == null) {
          settlementData = getSettlementDataNacional(alo);
          cached.put(key, settlementData);
        }
      }
    }
    return new SettlementData(settlementData);
  }

  public SettlementData getSettlmentDataInternacional(Map<String, SettlementData> cached, Tmct0alo alo)
      throws SIBBACBusinessException {
    String key = getKeySettlementDataInternacional(alo);
    SettlementData settlementData = cached.get(key);
    if (settlementData == null) {
      synchronized (cached) {
        settlementData = cached.get(key);
        if (settlementData == null) {
          settlementData = getSettlementDataNacional(alo);
          cached.put(key, settlementData);
        }
      }
    }
    return new SettlementData(settlementData);
  }

  private String getKeySettlementDataNacional(Tmct0alo alo) {
    String cdaliassAlo = alo.getCdalias();
    String cdmercadNacional = "";
    String centroNacional = "";
    Date feoperac = alo.getFeoperac();
    String cdclearerOrd = tmct0ordBo.findCdcleareById(alo.getId().getNuorden());
    String cdholder = alo.getCdholder();
    return String.format("%s_%s_%s_%s_%s_%s", cdaliassAlo.trim(), cdmercadNacional.trim(), centroNacional.trim(),
        feoperac, cdclearerOrd.trim(), cdholder.trim());
  }

  private String getKeySettlementDataInternacional(Tmct0alo alo) {
    String cdaliassAlo = alo.getCdalias();
    String cdmercadNacional = "";
    String centroNacional = "MMEE";
    Date feoperac = alo.getFeoperac();
    String cdclearerOrd = tmct0ordBo.findCdcleareById(alo.getId().getNuorden());
    String cdholder = alo.getCdholder();
    return String.format("%s_%s_%s_%s_%s_%s", cdaliassAlo.trim(), cdmercadNacional.trim(), centroNacional.trim(),
        feoperac, cdclearerOrd.trim(), cdholder.trim());
  }

  public List<SettlementDataClearerDTO> getClearerNacional(Tmct0alo alo, String clearer) {
    List<SettlementDataClearerDTO> res = new ArrayList<SettlementDataClearerDTO>();
    String cdmercadNacional = "";
    String centroNacional = "";
    List<Tmct0cleNacional> clearersnacional = tmct0cleNacionalBo.findByFechaAndCdcleare(alo.getFeoperac(), clearer,
        centroNacional, cdmercadNacional);
    for (Tmct0cleNacional tmct0cleNacional : clearersnacional) {
      res.add(new SettlementDataClearerDTO(tmct0cleNacional));
    }
    return res;
  }

  public List<SettlementDataClearerDTO> getAllClearersNacionalActivos() {
    List<SettlementDataClearerDTO> res = new ArrayList<SettlementDataClearerDTO>();
    List<Tmct0cleNacional> clearersnacional = tmct0cleNacionalBo.findAllActiveNacional();
    for (Tmct0cleNacional tmct0cleNacional : clearersnacional) {
      res.add(new SettlementDataClearerDTO(tmct0cleNacional));
    }
    return res;
  }

  public List<SettlementDataClearerDTO> getClearerInternacional(Tmct0alo alo, String clearer) {
    List<SettlementDataClearerDTO> res = new ArrayList<SettlementDataClearerDTO>();

    // List<Tmct0cleInternacional> clearersnacional =
    // tmct0cleIternacionalBo.findByFechaAndCdcleare(alo.getFeoperac(),
    // clearer);
    // for (Tmct0cleInternacional tmct0cleNacional : clearersnacional) {
    // res.add(new SettlementDataClearerDTO(tmct0cleNacional));
    // }
    return res;
  }

  public List<SettlementDataClearerDTO> getClearerNacional(Map<String, List<SettlementDataClearerDTO>> cached,
      Tmct0alo alo, String clearer) {
    String key = String.format("%s_%s", alo.getFeoperac(), StringUtils.trim(clearer));
    List<SettlementDataClearerDTO> listClearers = cached.get(key);
    if (listClearers == null) {
      synchronized (cached) {
        if ((listClearers = cached.get(key)) == null) {
          listClearers = getClearerNacional(alo, clearer);
          cached.put(key, listClearers);
        }
      }

    }
    return listClearers;
  }
}
