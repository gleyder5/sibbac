package sibbac.business.wrappers.database.dao.specifications;


import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.business.wrappers.database.model.FacturaRectificativa;
import sibbac.business.wrappers.database.model.FacturaSugerida;


public class FacturaRectificativaSpecificacions {

	protected static final Logger	LOG	= LoggerFactory.getLogger( FacturaRectificativaSpecificacions.class );

	public static Specification< FacturaRectificativa > numeroFactura( final Long numeroFactura ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate numeroFacturaPredicate = null;
				LOG.trace( "[FacturaSpecificacions::numeroFactura] [numeroFactura=={}]", numeroFactura );
				if ( numeroFactura != null ) {
					numeroFacturaPredicate = builder.equal( root.< Long > get( "numeroFactura" ), numeroFactura );
				}
				return numeroFacturaPredicate;
			}
		};
	}

	public static Specification< FacturaRectificativa > alias( final Long aliasId ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate aliasPredicate = null;
				LOG.trace( "[FacturaRectificativaSpecificacions::alias] [aliasId=={}]", aliasId );
				if ( aliasId != null ) {
					aliasPredicate = builder.equal( root.< Factura > get( "factura" ).< FacturaSugerida > get( "facturaSugerida" )
							.< Alias > get( "alias" ).< Long > get( "id" ), aliasId );
				}
				return aliasPredicate;
			}
		};
	}

	public static Specification< FacturaRectificativa > fechaFacturaPosterior( final Date fecha ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[FacturaRectificativaSpecificacions::fechaFacturaPosterior] [fecha=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.lessThanOrEqualTo( root.< Date > get( "fechaFactura" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< FacturaRectificativa > fechaFacturaAnterior( final Date fecha ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate fechaPredicate = null;
				LOG.trace( "[FacturaRectificativaSpecificacions::fechaFacturaAnterior] [fecha=={}]", fecha );
				if ( fecha != null ) {
					fechaPredicate = builder.greaterThanOrEqualTo( root.< Date > get( "fechaFactura" ), fecha );
				}
				return fechaPredicate;
			}
		};
	}

	public static Specification< FacturaRectificativa > estados( final List< Integer > estados ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate estadosPredicate = null;
				LOG.trace( "[FacturaRectificativaSpecificacions::estados] [estados=={}]", estados );
				if ( CollectionUtils.isNotEmpty( estados ) ) {
					final Path< Tmct0estado > estado = root.< Tmct0estado > get( "estado" );
					estadosPredicate = estado.in( estados );
				}
				return estadosPredicate;
			}
		};
	}

	public static Specification< FacturaRectificativa > listadoFriltrado( final Long aliasId, final Date fechaDesde, final Date fechaHasta,
			final List< Integer > estados ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< FacturaRectificativa > specs = null;
				if ( aliasId != null ) {
					specs = Specifications.where( alias( aliasId ) );
				}
				if ( fechaDesde != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaFacturaPosterior( fechaDesde ) );
					} else {
						specs = specs.and( fechaFacturaPosterior( fechaDesde ) );
					}
				}
				if ( fechaHasta != null ) {
					if ( specs == null ) {
						specs = Specifications.where( fechaFacturaAnterior( fechaHasta ) );
					} else {
						specs = specs.and( fechaFacturaAnterior( fechaHasta ) );
					}

				}
				if ( CollectionUtils.isNotEmpty( estados ) ) {
					if ( specs == null ) {
						specs = Specifications.where( estados( estados ) );
					} else {
						specs = specs.and( estados( estados ) );
					}

				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}

	public static Specification< FacturaRectificativa > listadoNumeroFactura( final Long numeroFactura ) {
		return new Specification< FacturaRectificativa >() {

			public Predicate toPredicate( Root< FacturaRectificativa > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< FacturaRectificativa > specs = null;
				if ( numeroFactura != null ) {
					specs = Specifications.where( numeroFactura( numeroFactura ) );
				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}

		};
	}
}
