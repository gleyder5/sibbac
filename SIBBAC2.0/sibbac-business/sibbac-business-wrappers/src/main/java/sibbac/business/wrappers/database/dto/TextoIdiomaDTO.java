package sibbac.business.wrappers.database.dto;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.model.TextosIdiomas;


public class TextoIdiomaDTO {

	public static final String	UTF8	= "UTF-8";
	private static final Logger	LOG		= LoggerFactory.getLogger( TextoIdiomaDTO.class );

	private Long				idTextoIdioma;
	private String				descripcion;
	private Long				idTexto;
	private Long				idIdioma;

	public TextoIdiomaDTO() {
		super();
	}

	public TextoIdiomaDTO( final TextosIdiomas textoIdioma ) {
		this();
		this.idTextoIdioma = textoIdioma.getId();
		this.descripcion = textoIdioma.getDescripcion();
		this.idTexto = textoIdioma.getTexto().getId();
		this.idIdioma = textoIdioma.getIdioma().getId();

		this.decodeDescripcion();
	}

	public TextoIdiomaDTO( Long idTextoIdioma, String descripcion ) {
		this();
		this.idTextoIdioma = idTextoIdioma;
		this.descripcion = descripcion;
		this.decodeDescripcion();
	}

	public TextoIdiomaDTO( Long idTextoIdioma, String descripcion, Long idTexto, Long idIdioma ) {
		this();
		this.idTextoIdioma = idTextoIdioma;
		this.descripcion = descripcion;
		this.idTexto = idTexto;
		this.idIdioma = idIdioma;
		this.decodeDescripcion();
	}

	protected void decodeDescripcion() {

		// Hack por si viene codificada la descripcion
		// See: http://stackoverflow.com/questions/607176/java-equivalent-to-javascripts-encodeuricomponent-that-produces-identical-outpu
		try {
			this.descripcion = URLDecoder.decode( this.descripcion, UTF8 );
		} catch ( UnsupportedEncodingException e ) {
			LOG.warn( "Unable to decode incoming description: [{}]", this.descripcion );
		}

	}

	public Long getIdTextoIdioma() {
		return idTextoIdioma;
	}

	public void setIdTextoIdioma( Long idTextoIdioma ) {
		this.idTextoIdioma = idTextoIdioma;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	public Long getIdTexto() {
		return idTexto;
	}

	public void setIdTexto( Long idTexto ) {
		this.idTexto = idTexto;
	}

	public Long getIdIdioma() {
		return idIdioma;
	}

	public void setIdIdioma( Long idIdioma ) {
		this.idIdioma = idIdioma;
	}

}
