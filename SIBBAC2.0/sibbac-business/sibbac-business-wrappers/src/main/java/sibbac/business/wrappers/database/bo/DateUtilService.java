package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.dao.FestivoDao;
import sibbac.business.wrappers.database.model.Festivo;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;


@Service
public class DateUtilService {

    @Autowired
    private Tmct0cfgDao tmct0cfgDao;
    
    @Autowired
    private FestivoDao festivoDao;
    
    private List<Date>holidaysConfigList;
    private List<Integer>workDaysOfWeekConfigList;
    /**
     * Obtiene una lista con los días de vacaciones configurados en la tabla tmct0cfg.
     * 
     * @return <code>List<Date> - </code>Lista de días de vacaciones.
     */
    public List<Date> getHolidays() {
      if (holidaysConfigList == null) {
        // Días de vacaciones configurados
        List<Tmct0cfg> configList = tmct0cfgDao.findByAplicationAndProcessAndLikeKeyname("SBRMV",
                                                                                         "CONFIG",
                                                                                         "holydays.anual.days%");
        if (configList != null && !configList.isEmpty()) {
          holidaysConfigList = new ArrayList<Date>(configList.size());

          for (Tmct0cfg tmct0cfg : configList) {
            holidaysConfigList.add(FormatDataUtils.convertStringToDate(tmct0cfg.getKeyvalue(), "yyyy-MM-dd"));
          } // for
        } else {
          holidaysConfigList = new ArrayList<Date>();
        } // else
      } // if

      return holidaysConfigList;
    } // getHolidays

    /**
     * Obtiene una lista con los días de laborables configurados en la tabla tmct0cfg.
     * 
     * @return <code>List<Integer> - </code>Lista de días de la semana laborables.
     */
    public List<Integer> getWorkDaysOfWeek() {
      if (workDaysOfWeekConfigList == null) {
        // Días de la semana laborables configurados
        Tmct0cfg tmct0cfg = tmct0cfgDao.findByAplicationAndProcessAndKeyname("SBRMV", "CONFIG", "week.work.days");

        if (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && !tmct0cfg.getKeyvalue().isEmpty()) {
          String[] workDays = tmct0cfg.getKeyvalue().split(",");
          workDaysOfWeekConfigList = new ArrayList<Integer>(workDays.length);

          for (String workDay : workDays) {
            workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
          } // for
        } else {
          workDaysOfWeekConfigList = new ArrayList<Integer>();
        } // else
      } // if

      return workDaysOfWeekConfigList;
    } // getWorkDaysOfWeek
    
    /**
     *	Devuelve true si es fecha festiva - en tabla TMCT0_FESTIVOS.
     *	@param date date
     *	@return boolean boolean
     */
    public boolean isFechaFestiva(Date date) {
    	if (date != null) {
    		String dateAsString = DateHelper.convertDateToString(date, "yyyy-MM-dd");
    		Festivo festivo = festivoDao.findByFecha(DateHelper.convertStringToDate(dateAsString, "yyyy-MM-dd"));
        	return festivo != null;    	
    	}
    	return false;
    }
}
