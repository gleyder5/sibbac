package sibbac.business.wrappers.database.filter;

import java.util.Date;
import java.util.List;

public interface TitularesFilter  {

  public TipoConsultaEnum getTipoConsulta();

  public Date getFechaDesde();

  public Date getFechaHasta();

  public char getMercadoNacInt();

  public Character getSentido();

  public List<String> getNudnicif();

  public String getErrorCode();

  public List<String> getNbclient();

  public List<String> getNbclient1();
  
  public List<String> getNbclient2();

  public List<String> getIsin();

  public List<String> getAlias();

  public String getRefCliente();

  public String getNbooking();

  public boolean isAliasEq();

  public boolean isNbclientEq();
  
  public boolean isNbclient1Eq();

  public boolean isNbclient2Eq();

  public boolean isNudnicifEq();

  public boolean isIsinEq();

  public boolean isRefClienteEq();

  public boolean isNbookingEq();

  public boolean isErrorCodeEq();
  
  public boolean isByPass();
  
  public boolean isExportation();
  
  public void setExportation(boolean exportation);
  
  public enum TipoConsultaEnum {
    BUSQUEDA_TODOS_LOS_TITULARES, BUSQUEDA_TITULARES_SIN_ERROR, BUSQUEDA_TITULARES_CON_ERROR, 
    BUSQUEDA_TITULARES_ROUTING_SIN_ORDEN_SV, BUSQUEDA_DESGLOSES_SIN_TITULAR;
  }

}
