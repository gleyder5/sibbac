package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sibbac.business.wrappers.database.model.Tmct0DesgloseTitular;

public interface DesgloseTitularDAO extends JpaRepository<Tmct0DesgloseTitular, Long> {

}
