package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.HashMap;

/**
 *	DTO parametros necesarios para armar los datos a grabar 
 *	en el fichero de envio Partenon. 
 */
public class ParamsFicheroEnvioPartenonDTO implements Serializable {

	/**
	 * Numero de serie generado automáticamente
	 */
	private static final long serialVersionUID = -1054511312961832687L;
	
	private final Map<String, String> procedencias;
	
	private final Map<String, String> procedencias2;
	
	// procedencia.anticipo_situacion (bundle) - Campo 8
	private String procedenciaAnticipoSituacion;
	
	// procedencia2.anticipo_situacion (bundle) - Campo 8
	private String procedencia2AnticipoSituacion;
	
	// propuesta.anticipo_situacion (bundle) - Campo 9
	private String propuestaAnticipoSituacion;
	
	// routing.alias.anticipo_situacion (bundle) - Campo 9
	private String routingAliasAnticipoSituacion;
	
	// especial.alias.anticipo_situacion (bundle) - Campo 9
	private String especialAliasAnticipoSituacion;

	/**
	 *	Constructor no-arg. 
	 */
	public ParamsFicheroEnvioPartenonDTO() {
		procedencias = new HashMap<>();
		procedencias2 = new HashMap<>();
	}

	/**
	 *	Constructor arg.
	 *	@param procedenciaAnticipoSituacion	 
	 *	@param procedencia2AnticipoSituacion
	 *	@param propuestaAnticipoSituacion
	 *	@param routingAliasAnticipoSituacion
	 *	@param especialAliasAnticipoSituacion
	 */
	public ParamsFicheroEnvioPartenonDTO(
			String procedenciaAnticipoSituacion,
			String procedencia2AnticipoSituacion,
			String propuestaAnticipoSituacion,
			String routingAliasAnticipoSituacion,
			String especialAliasAnticipoSituacion) {
		this();
		this.procedenciaAnticipoSituacion = procedenciaAnticipoSituacion;
		this.procedencia2AnticipoSituacion = procedencia2AnticipoSituacion;
		this.propuestaAnticipoSituacion = propuestaAnticipoSituacion;
		this.routingAliasAnticipoSituacion = routingAliasAnticipoSituacion;
		this.especialAliasAnticipoSituacion = especialAliasAnticipoSituacion;
		despliegaProcedencia();
		despliegaProcedencia2();
	}

	/**
	 *	@return  procedenciaAnticipoSituacion
	 */
	public String getProcedenciaAnticipoSituacion() {
		return this.procedenciaAnticipoSituacion;
	}

	/**
	 *	@param procedenciaAnticipoSituacion 
	 */
	public void setProcedenciaAnticipoSituacion(String procedenciaAnticipoSituacion) {
		this.procedenciaAnticipoSituacion = procedenciaAnticipoSituacion;
		despliegaProcedencia();
	}

	/**
	 *	@return  procedencia2AnticipoSituacion
	 */
	public String getProcedencia2AnticipoSituacion() {
		return this.procedencia2AnticipoSituacion;
	}

	/**
	 *	@param procedencia2AnticipoSituacion 
	 */
	public void setProcedencia2AnticipoSituacion(
			String procedencia2AnticipoSituacion) {
		this.procedencia2AnticipoSituacion = procedencia2AnticipoSituacion;
		despliegaProcedencia2();
	}

	/**
	 *	@return  propuestaAnticipoSituacion
	 */
	public String getPropuestaAnticipoSituacion() {
		return this.propuestaAnticipoSituacion;
	}

	/**
	 *	@param propuestaAnticipoSituacion 
	 */
	public void setPropuestaAnticipoSituacion(String propuestaAnticipoSituacion) {
		this.propuestaAnticipoSituacion = propuestaAnticipoSituacion;
	}

	/**
	 *	@return  routingAliasAnticipoSituacion
	 */
	public String getRoutingAliasAnticipoSituacion() {
		return this.routingAliasAnticipoSituacion;
	}

	/**
	 *	@param routingAliasAnticipoSituacion 
	 */
	public void setRoutingAliasAnticipoSituacion(
			String routingAliasAnticipoSituacion) {
		this.routingAliasAnticipoSituacion = routingAliasAnticipoSituacion;
	}

	/**
	 *	@return  especialAliasAnticipoSituacion
	 */
	public String getEspecialAliasAnticipoSituacion() {
		return this.especialAliasAnticipoSituacion;
	}

	/**
	 *	@param especialAliasAnticipoSituacion 
	 */
	public void setEspecialAliasAnticipoSituacion(
			String especialAliasAnticipoSituacion) {
		this.especialAliasAnticipoSituacion = especialAliasAnticipoSituacion;
	}

	public String getProcedencia(String codigo) {
		final String trimed;
		
		trimed = Objects.requireNonNull(codigo).trim();
	  return procedencias.get(trimed);
	}
	
	public String getProcedencia2(String codigo) {
		final String trimed;
		
		trimed = Objects.requireNonNull(codigo).trim();
	  return procedencias2.get(trimed);
	}
	
	private void despliegaProcedencia() {
	  final String[] listaProcedencias;
	  String[] elementos;
	  
	  procedencias.clear();
	  listaProcedencias = procedenciaAnticipoSituacion.split(",");
	  for(final String p : listaProcedencias) {
	    elementos = p.split("-");
	    if(elementos.length > 1) {
	      procedencias.put(elementos[0].trim(), elementos[1]);
	    }
	  }
	}
	
	private void despliegaProcedencia2() {
	  final String[] listaProcedencias;
	  String[] elementos;
	  
	  procedencias2.clear();
	  listaProcedencias = procedencia2AnticipoSituacion.split(",");
	  for(final String p : listaProcedencias) {
	    elementos = p.split("-");
	    if(elementos.length > 1) {
	      procedencias2.put(elementos[0].trim(), String.format("%s-%s", elementos[1].trim(), elementos[0].trim()));
	    }
	  }
	}
	
}