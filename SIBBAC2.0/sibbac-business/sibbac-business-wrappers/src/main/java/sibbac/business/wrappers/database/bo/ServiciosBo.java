package sibbac.business.wrappers.database.bo;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.ServiciosDao;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.database.bo.AbstractBo;
import sibbac.tasks.database.Job;


// TODO: Auto-generated Javadoc
/**
 * The Class ServiciosBo.
 */
@Service
public class ServiciosBo extends AbstractBo< Servicios, Long, ServiciosDao > {

	/**
	 * Find servicios by job.
	 *
	 * @param descripcion the descripcion
	 * @return the servicios
	 */
	public Servicios findServiciosByJob( Job descripcion ) {
		return this.dao.findServiciosByJob( descripcion );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.database.bo.AbstractBo#findById(java.io.Serializable)
	 */
	public Servicios findById( final Long id ) {
		return this.dao.findById( id );
	}

	/**
	 * Creates the servicio.
	 *
	 * @param nombreServicio the nombre servicio
	 * @param descripcionServicio the descripcion servicio
	 * @param activo the activo
	 * @return the servicios
	 */
	public Servicios createServicio( String nombreServicio, String descripcionServicio, boolean activo ) {
		Servicios dto = new Servicios();
		dto.setNombreServicio( nombreServicio );
		dto.setDescripcionServicio( descripcionServicio );
		dto.setActivo( activo );
		return this.dao.save( dto );
	}

	/**
	 * Delete servicio.
	 *
	 * @param id the id
	 * @return true, if successful
	 */
	public boolean deleteServicio( Long id ) {

		Servicios toDelete = this.findById( id );
		if ( toDelete == null ) {
			return false;
		}
		this.dao.delete( toDelete );
		return true;
	}

	/**
	 * Find servicios by configuracion alias.
	 *
	 * @param configuracionAlias the configuracion alias
	 * @return the list
	 */
	List< Servicios > findServiciosByConfiguracionAlias( final Boolean configuracionAlias ) {
		List< Servicios > resultado = this.dao.findAllByConfiguracionAlias( configuracionAlias );
		if ( resultado == null ) {
			resultado = new ArrayList< Servicios >();
		}
		return resultado;
	}

	/**
	 * Find servicios by configuracion contacto.
	 *
	 * @param configuracioContacto the configuracio contacto
	 * @return the list
	 */
	List< Servicios > findServiciosByConfiguracionContacto( final Boolean configuracioContacto ) {
		List< Servicios > resultado = this.dao.findAllByconfiguracionContactos( configuracioContacto );
		if ( resultado == null ) {
			resultado = new ArrayList< Servicios >();
		}
		return resultado;
	}

	/**
	 * Update activo servicio.
	 *
	 * @param id the id
	 * @param activo the activo
	 * @return true, if successful
	 */
	public boolean updateActivoServicio( Long id, Boolean activo ) {
		Servicios toUpdate = this.findById( id );
		Servicios updated = null;

		if ( toUpdate == null ) {
			return false;
		}

		toUpdate.setActivo( activo );
		try {
			updated = this.dao.save( toUpdate );
		} catch ( Exception e ) {
			LOG.debug( "Error saving Servicios" );
		}
		if ( updated == null ) {
			return false;
		}
		return true;
	}

}
