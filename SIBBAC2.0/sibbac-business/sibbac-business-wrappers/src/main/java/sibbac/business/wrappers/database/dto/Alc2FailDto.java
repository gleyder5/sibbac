package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Arrays;

import sibbac.common.utils.FormatDataUtils;

/**
 * @version 1.0
 * @author XI316153
 */
public class Alc2FailDto implements Serializable, Comparable<Alc2FailDto> {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -1046976859801864528L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Número de orden. */
  private String nuorden;

  /** Número de booking. */
  private String nbooking;

  /** Número de desglose a cliente. */
  private String nucnfclt;

  /** Número de desglose liquidación. */
  private Short nucnfliq;

  /** Tipo de operación. */
  private Character tipoOperacion;

  /** Código del alias (cuenta). */
  private String cdAlias;

  /** Descripcion del alias. */
  private String dsAlias;

  /** Código del valor. */
  private String cdIsin;

  /** Descripcion del valor. */
  private String dsIsin;

  /** Fecha de contratación. */
  private Date feejeliq;

  /** Fecha de liquidación. */
  private Date feopeliq;

  /** Código de la cuenta de compensación. */
  private String cdCuentaCompensacion;

  /** Código de la cuenta de liquidación. */
  private String cdCuentaLiquidacion;

  /** Número de títulos. */
  private BigDecimal titulos;

  /** Importe efectivo. */
  private BigDecimal efectivo;

  /** Importe neto de la operación. */
  private BigDecimal neto;

  /** Número de referencia de la operación enviado a S3. */
  private String referenciaS3;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /** Constructor I. Constructor por defecto. */
  public Alc2FailDto() {
  } // Alc2FailDto

  /**
   * Constructor II. Crea un nuevo objeto de la clase con los datos especificados.
   * 
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   * @param tipoOperacion
   * @param cdAlias
   * @param dsAlias
   * @param cdIsin
   * @param dsIsin
   * @param feejeliq
   * @param feopeliq
   * @param cdCuentaCompensacion
   * @param cdCuentaLiquidación
   * @param titulos
   * @param efectivo
   */
  public Alc2FailDto(String nuorden,
                     String nbooking,
                     String nucnfclt,
                     Short nucnfliq,
                     Character tipoOperacion,
                     String cdAlias,
                     String dsAlias,
                     String cdIsin,
                     String dsIsin,
                     Date feejeliq,
                     Date feopeliq,
                     BigDecimal titulos,
                     BigDecimal efectivo,
                     BigDecimal neto,
                     String cdCuentaCompensacion,
                     String cdCuentaLiquidacion,
                     String referencias3) {
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.nucnfclt = nucnfclt;
    this.nucnfliq = nucnfliq;
    this.tipoOperacion = tipoOperacion;
    this.cdAlias = cdAlias;
    this.dsAlias = dsAlias;
    this.cdIsin = cdIsin;
    this.dsIsin = dsIsin;
    this.feejeliq = feejeliq;
    this.feopeliq = feopeliq;
    this.titulos = titulos;
    this.efectivo = efectivo;
    this.neto = neto;
    this.cdCuentaCompensacion = cdCuentaCompensacion;
    this.cdCuentaLiquidacion = cdCuentaLiquidacion;
    this.referenciaS3 = referencias3;
  } // Alc2FailDto

  /**
   * Constructor III. Crea un nuevo objeto de la clase a partir del array especificado.
   * 
   * @param alc2fail
   */
  public Alc2FailDto(Object[] alc2failArg) {
	Object[] alc2fail = Arrays.copyOf(alc2failArg, alc2failArg.length);
    this.nuorden = String.valueOf(alc2fail[0]).trim();
    this.nbooking = String.valueOf(alc2fail[1]).trim();
    this.nucnfclt = String.valueOf(alc2fail[2]).trim();
    this.nucnfliq = Short.valueOf(alc2fail[3].toString());
    this.tipoOperacion = String.valueOf(alc2fail[4]).charAt(0);
    this.cdAlias = String.valueOf(alc2fail[5]).trim();
    this.dsAlias = String.valueOf(alc2fail[6]).trim();
    this.cdIsin = String.valueOf(alc2fail[7]).trim();
    this.dsIsin = String.valueOf(alc2fail[8]).trim();
    this.feejeliq = FormatDataUtils.convertStringToDate(alc2fail[9].toString(),
                                                        FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    this.feopeliq = FormatDataUtils.convertStringToDate(alc2fail[10].toString(),
                                                        FormatDataUtils.DATE_FORMAT_DASH_SEPARATOR);
    this.titulos = new BigDecimal(alc2fail[11].toString());
    this.efectivo = new BigDecimal(alc2fail[12].toString());
    this.neto = new BigDecimal(alc2fail[13].toString());
    this.cdCuentaCompensacion = String.valueOf(alc2fail[14]);
    this.cdCuentaLiquidacion = String.valueOf(alc2fail[15]);
    this.referenciaS3 = alc2fail[16] != null ? String.valueOf(alc2fail[16]).trim() : null;
  } // Alc2FailDto

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Alc2FailDto [nuorden=" + nuorden + ", nbooking=" + nbooking + ", nucnfclt=" + nucnfclt + ", nucnfliq="
           + nucnfliq + ", tipoOperacion=" + tipoOperacion + ", cdAlias=" + cdAlias + ", dsAlias=" + dsAlias
           + ", cdIsin=" + cdIsin + ", dsIsin=" + dsIsin + ", feejeliq=" + feejeliq + ", feopeliq=" + feopeliq
           + ", cdCuentaCompensacion=" + cdCuentaCompensacion + ", cdCuentaLiquidacion=" + cdCuentaLiquidacion
           + ", titulos=" + titulos + ", efectivo=" + efectivo + ", neto: " + neto + ", referenciaS3: " + referenciaS3
           + "]";
  } // toString

  /**
   * Compara el objeto actual con el especificado.<br>
   * Los objetos son iguales si son del mismo tipo de operación.
   * 
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(Alc2FailDto o) {
    return o.tipoOperacion.compareTo(this.getTipoOperacion());
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @return the tipoOperacion
   */
  public Character getTipoOperacion() {
    return tipoOperacion;
  }

  /**
   * @return the cdAlias
   */
  public String getCdAlias() {
    return cdAlias;
  }

  /**
   * @return the dsAlias
   */
  public String getDsAlias() {
    return dsAlias;
  }

  /**
   * @return the cdIsin
   */
  public String getCdIsin() {
    return cdIsin;
  }

  /**
   * @return the dsIsin
   */
  public String getDsIsin() {
    return dsIsin;
  }

  /**
   * @return the feejeliq
   */
  public Date getFeejeliq() {
    return feejeliq;
  }

  /**
   * @return the feopeliq
   */
  public Date getFeopeliq() {
    return feopeliq;
  }

  /**
   * @return the cdCuentaCompensacion
   */
  public String getCdCuentaCompensacion() {
    return cdCuentaCompensacion;
  }

  /**
   * @return the cdCuentaLiquidacion
   */
  public String getCdCuentaLiquidacion() {
    return cdCuentaLiquidacion;
  }

  /**
   * @return the titulos
   */
  public BigDecimal getTitulos() {
    return titulos;
  }

  /**
   * @return the efectivo
   */
  public BigDecimal getEfectivo() {
    return efectivo;
  }

  /**
   * @return the neto
   */
  public BigDecimal getNeto() {
    return neto;
  }

  /**
   * @return the referenciaS3
   */
  public String getReferenciaS3() {
    return referenciaS3;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @param tipoOperacion the tipoOperacion to set
   */
  public void setTipoOperacion(Character tipoOperacion) {
    this.tipoOperacion = tipoOperacion;
  }

  /**
   * @param cdAlias the cdAlias to set
   */
  public void setCdAlias(String cdAlias) {
    this.cdAlias = cdAlias;
  }

  /**
   * @param dsAlias the dsAlias to set
   */
  public void setDsAlias(String dsAlias) {
    this.dsAlias = dsAlias;
  }

  /**
   * @param cdIsin the cdIsin to set
   */
  public void setCdIsin(String cdIsin) {
    this.cdIsin = cdIsin;
  }

  /**
   * @param dsIsin the dsIsin to set
   */
  public void setDsIsin(String dsIsin) {
    this.dsIsin = dsIsin;
  }

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  /**
   * @param feopeliq the feopeliq to set
   */
  public void setFeopeliq(Date feopeliq) {
    this.feopeliq = feopeliq;
  }

  /**
   * @param cdCuentaCompensacion the cdCuentaCompensacion to set
   */
  public void setCdCuentaCompensacion(String cdCuentaCompensacion) {
    this.cdCuentaCompensacion = cdCuentaCompensacion;
  }

  /**
   * @param cdCuentaLiquidación the cdCuentaLiquidación to set
   */
  public void setCdCuentaLiquidacion(String cdCuentaLiquidacion) {
    this.cdCuentaLiquidacion = cdCuentaLiquidacion;
  }

  /**
   * @param titulos the titulos to set
   */
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  /**
   * @param efectivo the efectivo to set
   */
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  /**
   * @param neto the neto to set
   */
  public void setNeto(BigDecimal neto) {
    this.neto = neto;
  }

  /**
   * @param referenciaS3 the referenciaS3 to set
   */
  public void setReferenciaS3(String referenciaS3) {
    this.referenciaS3 = referenciaS3;
  }

} // Alc2FailDto
