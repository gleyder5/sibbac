package sibbac.business.wrappers.database.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.PartenonRecordBeanDao;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.database.bo.AbstractBo;

@Service
public class PartenonRecordBeanBo extends AbstractBo<PartenonRecordBean, Long, PartenonRecordBeanDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(PartenonRecordBeanBo.class);
  

  public List<PartenonRecordBean> findAllTitularesOperacionesEspecialesByListNureford(List<String> nurefOrds) {
   

    return dao.findAllTitularesOperacionesByListNureford(nurefOrds);
  }

  public List<String> findAllCcvByNureford(String nureford) {
    return dao.findAllCcvByNureford(nureford);
  }

  public Map<String, String> findAllCcvByListNureford(List<String> nureford) {
    Map<String, String> result = new HashMap<String, String>();
    List<Object[]> l = dao.findAllCcvByListNureford(nureford);
    for (Object[] ccvs : l) {
      result.put((String) ccvs[0], (String) ccvs[1]);
    }

    return result;

  }
}
