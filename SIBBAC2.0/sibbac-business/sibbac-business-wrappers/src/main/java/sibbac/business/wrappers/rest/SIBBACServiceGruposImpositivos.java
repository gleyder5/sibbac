package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.GruposImpositivosBo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceGruposImpositivos implements SIBBACServiceBean {

	@Autowired
	protected GruposImpositivosBo gruposImpositivosBO;

	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGruposImpositivos.class);

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACServiceGruposImpositivos() {
		LOG.trace(
				"[SIBBACServiceGruposImpositivos::<constructor>] Initiating SIBBAC Service for \"GruposImpositivos\".");
	}

	@Override
	public WebResponse process(WebRequest webRequest) {

		LOG.trace("[SIBBACServiceGruposImpositivos::process] Processing a web request...");

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map<String, String> params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.trace("[SIBBACServiceGruposImpositivos::process]. Command: [{}]", webRequest.getAction());

		switch (webRequest.getAction()) {
		case Constantes.CMD_ALTA:

			if (params == null) {
				webResponse.setError("Por favor indique los filtros de búsqueda.");
				return webResponse;
			}

			webResponse.setResultados(gruposImpositivosBO.createGrupoImpositivo(params));
			LOG.trace("Se rellena el webresponse con el grupo impositivo creado");

			break;

		case Constantes.CMD_GET_GRUPOS_IMPOSITIVOS:

			webResponse.setResultados(gruposImpositivosBO.getGruposImpositivos());
			LOG.trace("Se rellena el webresponse con la lista de los grupos impositivos");

			break;
		case Constantes.GRUPOS_IMPOSITIVOS_MODIFICACION:

			// comprueba que los parametros han sido enviados
			if (params == null) {
				webResponse.setError("Por favor indique los filtros de búsqueda.");
				return webResponse;
			}

			// HAce el set en el webresponse del resultado de modificar un
			// contacto en la tabla contacto.
			LOG.trace("[Entra en el comando GRUPOS_IMPOSITIVOS_MODIFICACION de Grupo Impositivo]");
			webResponse.setResultados(gruposImpositivosBO.modificarGruposImpositivos(params));
			LOG.trace("Rellenado el WebResponse con la modificacion del Grupos Impositivos");

			break;

		default:

			webResponse.setError("Comando incorrecto");
			LOG.error("No le llega ningun comando correcto");

			break;

		}

		return webResponse;

	}

	@Override
	public List<String> getAvailableCommands() {

		List<String> commands = new ArrayList<String>();
		commands.add(Constantes.CMD_ALTA);
		commands.add(Constantes.CMD_GET_GRUPOS_IMPOSITIVOS);
		commands.add(Constantes.GRUPOS_IMPOSITIVOS_MODIFICACION);
		return commands;

	}

	@Override
	public List<String> getFields() {
		List<String> fields = new LinkedList<String>();
		fields.add("IdGrupoImpositivo");
		fields.add("porcentaje");
		fields.add("validoDesde");
		fields.add("categoria");
		fields.add("descripcion");
		fields.add("cancelado");

		return fields;
	}
}
