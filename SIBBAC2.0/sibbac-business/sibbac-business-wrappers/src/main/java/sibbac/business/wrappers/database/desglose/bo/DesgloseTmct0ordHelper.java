package sibbac.business.wrappers.database.desglose.bo;

import sibbac.business.wrappers.database.model.Tmct0ord;

@Deprecated
public class DesgloseTmct0ordHelper {

  public static boolean isRouting(String cdorigen, String cdclente) {
    return cdorigen != null && cdorigen.trim().toUpperCase().equals("ROUTING") || cdclente != null
        && cdclente.trim().toUpperCase().equals("ROUTING_BCH");
  }

  public static boolean isRouting(Tmct0ord ord) {
    return ord != null && isRouting(ord.getCdorigen(), ord.getCdclente());
  }
}
