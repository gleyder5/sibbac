package sibbac.business.wrappers.database.bo;

import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Temporal;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.FestivoDao;
import sibbac.business.wrappers.database.model.Festivo;
import sibbac.database.bo.AbstractBo;

@Service
public class FestivoBo extends AbstractBo<Festivo, Integer, FestivoDao> {

  public Festivo findById(final Integer id) {
    return this.dao.findById(id);
  }

  public Festivo findByFecha(@Temporal(TemporalType.DATE) Date fecha) {
    return this.dao.findByFecha(fecha);
  }

  public Long countByFecha(@Temporal(TemporalType.DATE) Date fecha) {
    return this.dao.countByFecha(fecha);
  }

  /**
   * Cuenta los festivos del mercado desde una fecha dada
   * */
  public int countFestivosByFechaAndCdmercad(Date fecha, String mercado) {
    return dao.countByFechaGteAndCdmercad(fecha, mercado);
  }
  
  /**
   * Recupera todos los festivos por mercado
   * */
  public List<Date> findAllFestivosNacionales() {
    return dao.findAllFechaByCdmercad("MAD");
  }

  /**
   * Recupera todos los festivos por mercado
   * */
  public List<Date> findAllFestivosByMercado(String mercado) {
    return dao.findAllFechaByCdmercad(mercado);
  }

  public Festivo createFestivo(Date fecha, String descripcion) {
    Festivo dto = new Festivo();
    dto.setFecha(fecha);
    dto.setDescripcion(descripcion);
    return this.dao.save(dto);
  }

  public boolean deleteFestivo(Integer id) {
    Festivo toDelete = this.findById(id);
    if (toDelete == null) {
      return false;
    }
    this.dao.delete(toDelete);
    return true;
  }

  public boolean updateFestivo(Integer id, Date date, String descripcion) {
    Festivo toUpdate = this.findById(id);

    if (toUpdate == null) {
      return false;
    }

    if (date != null) {
      toUpdate.setFecha(date);
    }
    if (descripcion != null) {
      toUpdate.setDescripcion(descripcion);
    }

    this.dao.save(toUpdate);

    return true;
  }

}
