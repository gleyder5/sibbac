package sibbac.business.wrappers.database.dto;


public class IdiomaDTO {

	private Long	id;
	private String	codigo;
	private String	descripcion;

	public IdiomaDTO() {

	}

	public IdiomaDTO( Long id, String codigo, String descripcion ) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public Long getId() {
		return id;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
