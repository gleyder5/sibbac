package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.AlcOrdenesBo;
import sibbac.business.wrappers.database.bo.FacturaBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceOrdenesFactura implements SIBBACServiceBean {

	@Autowired
	private FacturaBo facturaBo;

	@Autowired
	private AlcOrdenesBo alcOrdenesBo;

	@Autowired
	private Tmct0alcBo tmct0alcBo;

	private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceOrdenesFactura.class);

	public SIBBACServiceOrdenesFactura() {
	}

	@Override
	public WebResponse process(WebRequest webRequest) {
		LOG.debug("[SIBBACServiceOrdenesFactura::process] " + "Processing a web request...");

		WebResponse webResponse = new WebResponse();

		Map<String, String> params = webRequest.getFilters();

		LOG.debug("[SIBBACServiceOrdenesFactura::process] " + "+ Command.: [{}]", webRequest.getAction());

		switch (webRequest.getAction()) {

		case Constantes.CMD_GET_ORDENESFACTURA:
			webResponse.setResultados(alcOrdenesBo.getListaOrdenesFactura(params));
			break;
		default:
			webResponse.setError("No recibe ningun comando");
			LOG.error("No le llega ningun comando");
			break;
		}

		return webResponse;
	}

	public List<String> getAvailableCommands() {
		List<String> commands = new ArrayList<String>();

		commands.add(Constantes.CMD_GET_ORDENESFACTURA);

		return commands;
	}

	@Override
	public List<String> getFields() {
		List<String> fields = new LinkedList<String>();
		fields.add("facturaId");

		return fields;
	}

}
