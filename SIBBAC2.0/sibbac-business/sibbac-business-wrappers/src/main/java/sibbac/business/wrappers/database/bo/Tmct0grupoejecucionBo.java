package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.dao.Tmct0grupoejecucionDao;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0grupoejecucionBo extends AbstractBo<Tmct0grupoejecucion, Long, Tmct0grupoejecucionDao> {

  public List<String> findAllCdcamaraByAloId(Tmct0aloId aloId) {
    return dao.findAllCdcamaraByAloId(aloId);
  }

  public List<Tmct0grupoejecucion> findAllByTmct0aloId(Tmct0aloId aloId) {
    return dao.findAllByTmct0aloId(aloId);
  }

}
