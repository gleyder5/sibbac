package sibbac.business.wrappers.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.bancaPrivadaWriter.BancaPrivadaWriter;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@SIBBACJob(group = Task.GROUP_ROUTING.NAME,
           name = Task.GROUP_ROUTING.JOB_ESCRITURA_SPB,
           delay = 10,
           jobType = SIBBACJobType.CRON_JOB,
           cronExpression = "0 30 11,20 ? * MON-FRI")
public class TaskEscrituraSPB extends WrapperTaskConcurrencyPrevent {

  @Autowired
  private BancaPrivadaWriter writer;

  @Override
  @Transactional
  public void executeTask() throws Exception {
    LOG.debug("[TaskEscrituraSPB :: executeTask] Inicio");
    try {
      writer.writeIntoFile();
    } catch (Exception e) {
      throw e;
    } finally {
      writer.closeAndRenameFile();
      LOG.debug("[TaskEscrituraSPB :: executeTask] Fin");
    }

  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_OTROS_ESCRITURA_SPB;
  }

}
