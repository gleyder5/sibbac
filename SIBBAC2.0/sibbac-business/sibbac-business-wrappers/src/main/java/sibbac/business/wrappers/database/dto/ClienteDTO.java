package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * DTO para trabajar con clientes
 * 
 * @author adrian.prause
 *
 */
public class ClienteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6336295126742722529L;

	private Long idClient;
	private String descli;
	private Character tpidenti;
	private String cif;
	private String tpreside;
	private String cdbdp;
	private Character tpclienn;
	private Character tpfisjur;
	private Character cdestado;
	private Integer fhinicio;
	private Integer fhfinal;
	private String cdkgr;
	private Character tpNacTit;
	private String cdNacTit;
	private String cdcat;
	private Character cddeprev;
	private String motiPrev;
	private String riesgoPbc;
	private Date fhRevRiesgoPbc;
	
	public Date getFhRevRiesgoPbc() {
    return fhRevRiesgoPbc;
  }

  public void setFhRevRiesgoPbc(Date fhRevRiesgoPbc) {
    this.fhRevRiesgoPbc = fhRevRiesgoPbc;
  }

  public Long getId() {
		return idClient;
	}
	
	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	public String getDescli() {
		return descli;
	}

	public void setDescli(String descli) {
		this.descli = descli;
	}

	public Character getTpidenti() {
		return tpidenti;
	}

	public void setTpidenti(Character tpidenti) {
		this.tpidenti = tpidenti;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getTpreside() {
		return tpreside;
	}

	public void setTpreside(String tpreside) {
		this.tpreside = tpreside;
	}

	public String getCdbdp() {
		return cdbdp;
	}

	public void setCdbdp(String cdbdp) {
		this.cdbdp = cdbdp;
	}

	public Character getTpclienn() {
		return tpclienn;
	}

	public void setTpclienn(Character tpclienn) {
		this.tpclienn = tpclienn;
	}

	public Character getTpfisjur() {
		return tpfisjur;
	}

	public void setTpfisjur(Character tpfisjur) {
		this.tpfisjur = tpfisjur;
	}

	public Character getCdestado() {
		return cdestado;
	}

	public void setCdestado(Character cdestado) {
		this.cdestado = cdestado;
	}

	public Integer getFhinicio() {
		return fhinicio;
	}

	public void setFhinicio(Integer fhinicio) {
		this.fhinicio = fhinicio;
	}

	public Integer getFhfinal() {
		return fhfinal;
	}

	public void setFhfinal(Integer fhfinal) {
		this.fhfinal = fhfinal;
	}

	public String getCdkgr() {
		return cdkgr;
	}

	public void setCdkgr(String cdkgr) {
		this.cdkgr = cdkgr;
	}

	public Character getTpNacTit() {
		return tpNacTit;
	}

	public void setTpNacTit(Character tpNacTit) {
		this.tpNacTit = tpNacTit;
	}

	public String getCdNacTit() {
		return cdNacTit;
	}

	public void setCdNacTit(String cdNacTit) {
		this.cdNacTit = cdNacTit;
	}

	public String getCdcat() {
		return cdcat;
	}

	public void setCdcat(String cdcat) {
		this.cdcat = cdcat;
	}

	public Character getCddeprev() {
		return cddeprev;
	}

	public void setCddeprev(Character cddeprev) {
		this.cddeprev = cddeprev;
	}

	public String getMotiPrev() {
		return motiPrev;
	}

	public void setMotiPrev(String motiPrev) {
		this.motiPrev = motiPrev;
	}
	
	

	public String getRiesgoPbc() {
    return riesgoPbc;
  }

  public void setRiesgoPbc(String riesgoPbc) {
    this.riesgoPbc = riesgoPbc;
  }

  public static ClienteDTO convertObjectToClienteDTO(Object[] obj) {
    ClienteDTO dto = new ClienteDTO();
    dto.setIdClient(((BigInteger) obj[0]).longValue());
    if (obj[1] != null)
      dto.setDescli(((String) obj[1]).trim());
    dto.setTpidenti((Character) obj[2]);
    if (obj[3] != null)
      dto.setCif(((String) obj[3]).trim());
    if (obj[4] != null)
      dto.setTpreside(((String) obj[4]).trim());
    if (obj[5] != null)
      dto.setCdbdp(((String) obj[5]).trim());
    dto.setTpclienn((Character) obj[6]);
    dto.setTpfisjur((Character) obj[7]);
    dto.setCdestado((Character) obj[8]);
    if (obj[9] != null)
      dto.setFhinicio(((BigDecimal) obj[9]).intValue());
    if (obj[10] != null)
      dto.setFhfinal(((BigDecimal) obj[10]).intValue());
    if (obj[11] != null)
      dto.setCdkgr(((String) obj[11]).trim());
    dto.setTpNacTit((Character) obj[12]);
    if (obj[13] != null)
      dto.setCdNacTit(((String) obj[13]).trim());
    if (obj[14] != null)
      dto.setCdcat(((String) obj[14]).trim());
    dto.setCddeprev((Character) obj[15]);
    if (obj[16] != null)
      dto.setMotiPrev(((String) obj[16]).trim());
    if (obj[17] != null)
      dto.setRiesgoPbc(((String) obj[17]).trim());
    if (obj[18] != null)
      dto.setFhRevRiesgoPbc(((Date) obj[18]));    
    return dto;
  }

}
