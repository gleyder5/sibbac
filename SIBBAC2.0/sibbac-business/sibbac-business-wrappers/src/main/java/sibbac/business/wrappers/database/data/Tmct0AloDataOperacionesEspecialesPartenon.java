package sibbac.business.wrappers.database.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import sibbac.business.wrappers.database.dto.EjecucionAlocationProcesarDesglosesOpEspecialesDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;

public class Tmct0AloDataOperacionesEspecialesPartenon implements Serializable {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  private Tmct0AloData aloData;
  private Tmct0AloData aloDataOriginal;
  private Tmct0alo alo;
  private List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listEjecucionAlocacionData;
  private BigDecimal numTitulos = BigDecimal.ZERO;
  private BigDecimal numTitulosOriginal = BigDecimal.ZERO;
  private BigDecimal imcomisn = BigDecimal.ZERO;
  private BigDecimal imcomisnOriginal = BigDecimal.ZERO;
  private BigDecimal imcomisnAcumulado = BigDecimal.ZERO;
  private BigDecimal imcomdev = BigDecimal.ZERO;
  private BigDecimal imcomdevOriginal = BigDecimal.ZERO;
  private BigDecimal imcomdevAcumulado = BigDecimal.ZERO;

  private BigDecimal imcamonContrEu = BigDecimal.ZERO;
  private BigDecimal imcamonContrEuOriginal = BigDecimal.ZERO;
  private BigDecimal imcamonContrEuAcumulado = BigDecimal.ZERO;

  private Boolean savedAlo = Boolean.FALSE;

  public Tmct0AloDataOperacionesEspecialesPartenon() {
    this.aloData = new Tmct0AloData();
    this.aloDataOriginal = new Tmct0AloData();
  }

  public Tmct0AloDataOperacionesEspecialesPartenon(Tmct0AloData aloData) {
    this.aloData = aloData;
    this.aloDataOriginal = Tmct0AloData.copy(aloData);
    this.alo = Tmct0AloData.dataToEntity(aloDataOriginal);
    this.setNumTitulos(new BigDecimal(aloData.getNutitcli().longValue()));
    this.setNumTitulosOriginal(new BigDecimal(aloData.getNutitcli().longValue()));
    this.setImcomisn(new BigDecimal(aloData.getImcomisn().doubleValue()));
    this.setImcomisnOriginal(new BigDecimal(aloData.getImcomisn().doubleValue()));
    this.setImcomdev(new BigDecimal(aloData.getImcomdev().doubleValue()));
    this.setImcomdevOriginal(new BigDecimal(aloData.getImcomdev().doubleValue()));
    this.setImcamonContrEuOriginal(new BigDecimal(aloData.getImcanoncontreu().toString()));
  }

  public Tmct0AloDataOperacionesEspecialesPartenon(Tmct0alo alo) {
    this(new Tmct0AloData(alo));
  }

  public Tmct0AloDataOperacionesEspecialesPartenon(Tmct0alo alo, boolean onlyLoadAloData) {
    this(new Tmct0AloData(alo, onlyLoadAloData));
  }

  public Tmct0AloDataOperacionesEspecialesPartenon(Tmct0AloData alo,
                                                   List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listEjecucionAlocacion) {
    this(alo);
    if (CollectionUtils.isNotEmpty(listEjecucionAlocacion)) {
      listEjecucionAlocacionData = listEjecucionAlocacion;
    }// if (CollectionUtils.isNotEmpty(listEjecucionAlocacion)) {

  }// public Tmct0AloDataOperacionesEspecialesPartenon(Tmct0alo alo,
   // List<Tmct0ejecucionalocation> listEjecucionAlocacion) {

  public Tmct0AloData getAloData() {
    return aloData;
  }

  public void setAloData(Tmct0AloData aloData) {
    this.aloData = aloData;
  }

  public Tmct0AloData getAloDataOriginal() {
    return aloDataOriginal;
  }

  public void setAloDataOriginal(Tmct0AloData aloData) {
    this.aloDataOriginal = aloData;
  }

  public Tmct0alo getAlo() {
    return alo;
  }

  public void setAlo(Tmct0alo alo) {
    this.alo = alo;
  }

  public List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> getListEjecucionAlocacionData() {
    return listEjecucionAlocacionData;
  }

  public void setListEjecucionAlocacionData(List<EjecucionAlocationProcesarDesglosesOpEspecialesDTO> listEjecucionAlocacionData) {
    this.listEjecucionAlocacionData = listEjecucionAlocacionData;
  }

  public BigDecimal getNumTitulos() {
    return numTitulos;
  }

  public void setNumTitulos(BigDecimal numTitulos) {
    this.numTitulos = numTitulos;
  }

  public BigDecimal getNumTitulosOriginal() {
    return numTitulosOriginal;
  }

  public void setNumTitulosOriginal(BigDecimal numTitulosOriginal) {
    this.numTitulosOriginal = numTitulosOriginal;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public BigDecimal getImcomisnOriginal() {
    return imcomisnOriginal;
  }

  public void setImcomisnOriginal(BigDecimal imcomisnOriginal) {
    this.imcomisnOriginal = imcomisnOriginal;
  }

  public BigDecimal getImcomdev() {
    return imcomdev;
  }

  public void setImcomdev(BigDecimal imcomdev) {
    this.imcomdev = imcomdev;
  }

  public BigDecimal getImcomdevOriginal() {
    return imcomdevOriginal;
  }

  public void setImcomdevOriginal(BigDecimal imcomdevOriginal) {
    this.imcomdevOriginal = imcomdevOriginal;
  }

  public BigDecimal getImcomisnAcumulado() {
    return imcomisnAcumulado;
  }

  public void setImcomisnAcumulado(BigDecimal imcomisnAcumulado) {
    this.imcomisnAcumulado = imcomisnAcumulado;
  }

  public BigDecimal getImcomdevAcumulado() {
    return imcomdevAcumulado;
  }

  public void setImcomdevAcumulado(BigDecimal imcomdevAcumulado) {
    this.imcomdevAcumulado = imcomdevAcumulado;
  }

  public void setSavedAlo(Boolean savedAlo) {
    this.savedAlo = savedAlo;
  }

  public Boolean getSavedAlo() {

    return savedAlo;
  }

  public BigDecimal getImcamonContrEuOriginal() {
    return imcamonContrEuOriginal;
  }

  public void setImcamonContrEuOriginal(BigDecimal imcamonContrEuOriginal) {
    this.imcamonContrEuOriginal = imcamonContrEuOriginal;
  }

  public BigDecimal getImcamonContrEuAcumulado() {
    return imcamonContrEuAcumulado;
  }

  public void setImcamonContrEuAcumulado(BigDecimal imcamonContrEuAcumulado) {
    this.imcamonContrEuAcumulado = imcamonContrEuAcumulado;
  }

  public BigDecimal getImcamonContrEu() {
    return imcamonContrEu;
  }

  public void setImcamonContrEu(BigDecimal imcamonContrEu) {
    this.imcamonContrEu = imcamonContrEu;
  }

  @Override
  public String toString() {
    return "Tmct0AloDataOperacionesEspecialesPartenon [aloData=" + aloData + ", aloDataOriginal=" + aloDataOriginal
           + ", alo=" + alo + ", listEjecucionAlocacionData=" + listEjecucionAlocacionData + ", numTitulos="
           + numTitulos + ", numTitulosOriginal=" + numTitulosOriginal + ", imcomisn=" + imcomisn
           + ", imcomisnOriginal=" + imcomisnOriginal + ", imcomisnAcumulado=" + imcomisnAcumulado + ", imcomdev="
           + imcomdev + ", imcomdevOriginal=" + imcomdevOriginal + ", imcomdevAcumulado=" + imcomdevAcumulado
           + ", savedAlo=" + savedAlo + "]";
  }

}// public class Tmct0AloDataOperacionesEspecialesPartenon implements
// Serializable {
