package sibbac.business.wrappers.database.dto;


public class CamaraCompensacionDTO {

	private String	cdCodigo;
	private String	nbDescripcion;

	public CamaraCompensacionDTO( String cdCodigo, String nbDescripcion ) {
		super();
		this.cdCodigo = cdCodigo;
		this.nbDescripcion = nbDescripcion;
	}

	/**
	 * @return the cdCodigo
	 */
	public String getCdCodigo() {
		return cdCodigo;
	}

	/**
	 * @param cdCodigo the cdCodigo to set
	 */
	public void setCdCodigo( String cdCodigo ) {
		this.cdCodigo = cdCodigo;
	}

	/**
	 * @return the nbDescripcion
	 */
	public String getNbDescripcion() {
		return nbDescripcion;
	}

	/**
	 * @param nbDescripcion the nbDescripcion to set
	 */
	public void setNbDescripcion( String nbDescripcion ) {
		this.nbDescripcion = nbDescripcion;
	}

}
