package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.mapping.PatternMatchingCompositeLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.partenonReader.PartenonFieldSetMapper;
import sibbac.business.wrappers.database.bo.partenonReader.PartenonFieldSetMapper.PartenonFields;
import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.business.wrappers.tasks.thread.WrapperMultiThreadedFileReader;

@Component(value = "operacionEspecialFileReader")
public class OperacionEspecialFileReader extends WrapperMultiThreadedFileReader<OperacionEspecialRecordBean> {

  private final static String ID_LINEA_DESGLOSE = "40";
  private final static String ID_LINEA_TITULAR = "20";

  private static final Logger LOG = LoggerFactory.getLogger(OperacionEspecialFileReader.class);

  /** Numero de operaciones a realizar en cada commit de base de datos. */
  @Value("${sibbac.wrappers.transaction.size}")
  private Integer transactionSize;

  @Value("${sibbac.wrappers.thread.size}")
  private Integer threadSize;

  public OperacionEspecialFileReader() {
    super();
  }

  public OperacionEspecialFileReader(String fileName) {
    super(fileName, false, StandardCharsets.ISO_8859_1);
  }

  @Qualifier(value = "OperacionEspecialFileReaderRunnable")
  @Autowired
  private OperacionEspecialFileReaderRunnable opEspecialRunnable;

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Override
  protected LineMapper<OperacionEspecialRecordBean> initLineMapper() {
    return new PatternMatchingCompositeLineMapper<OperacionEspecialRecordBean>();
  }

  protected void initLineTokenizers(LineMapper<OperacionEspecialRecordBean> lineMapper) {
    final Map<String, LineTokenizer> tokenizers = new HashMap<String, LineTokenizer>();
    final FixedLengthTokenizer partenonLineTokenizer = new FixedLengthTokenizer();
    partenonLineTokenizer.setNames(PartenonFields.getFieldNames());
    partenonLineTokenizer.setColumns(convertLengthsIntoRanges(PartenonFields.getPartenonLengths()));
    partenonLineTokenizer.setStrict(false);
    final FixedLengthTokenizer operacionEspecialLineTokenizer = new FixedLengthTokenizer();
    operacionEspecialLineTokenizer.setNames(DesgloseFieldSetMapper.DesgloseFields.getNombresCampos());
    operacionEspecialLineTokenizer.setColumns(convertLengthsIntoRanges(DesgloseFieldSetMapper.DesgloseFields.getTamañosCampos()));
    operacionEspecialLineTokenizer.setStrict(false);
    tokenizers.put(ID_LINEA_DESGLOSE + "*", operacionEspecialLineTokenizer);
    tokenizers.put(ID_LINEA_TITULAR + "*", partenonLineTokenizer);
    ((PatternMatchingCompositeLineMapper<OperacionEspecialRecordBean>) lineMapper).setTokenizers(tokenizers);
    // return tokenizers;

  }

  protected void initFieldSetMappers(LineMapper<OperacionEspecialRecordBean> lineMapper) {
    final Map<String, FieldSetMapper<OperacionEspecialRecordBean>> fieldSetMappers = new HashMap<String, FieldSetMapper<OperacionEspecialRecordBean>>();
    fieldSetMappers.put(ID_LINEA_DESGLOSE + "*", new DesgloseFieldSetMapper<DesgloseRecordBean>());
    fieldSetMappers.put(ID_LINEA_TITULAR + "*", new PartenonFieldSetMapper<PartenonRecordBean>());
    ((PatternMatchingCompositeLineMapper<OperacionEspecialRecordBean>) lineMapper).setFieldSetMappers(fieldSetMappers);

  }

  /*
   * @see
   * sibbac.business.wrappers.common.fileReader.SibbacFileReader#comprobarDistintaOrden(sibbac.business.wrappers.common
   * .fileReader.RecordBean)
   */
  @Override
  protected boolean isInBlock(final RecordBean previousBean, final RecordBean bean) {
    boolean inBlock = true;
    LOG.trace("[OperacionEspecialFileReader :: isInBlock] Init");
    if (ID_LINEA_DESGLOSE.equals(bean.getTipoRegistro())) {
      // DesgloseRecordBean recordBeanD = (DesgloseRecordBean) bean;
      // OperacionEspecialRecordBean previousBeanD = (OperacionEspecialRecordBean) previousBean;
      //
      // if (previousBeanD != null && bean != null) {
      // // Primero de un bloque
      //
      // inBlock = recordBeanD.getNumeroReferenciaOrden().equals(previousBeanD.get);
      // // Pertenece al mismo bloque
      // }// else if ( partenonRecordBean.getNumOrden().equals( mismaOrden.getNumOrden() ) ) {
      //
      // LOG.trace("[OperacionEspecialFileReader :: isInBlock] Fin");
      inBlock = previousBean == null;
    }
    return inBlock;
  } // comprobarDistintaOrden

  @Override
  public IRunnableBean<OperacionEspecialRecordBean> getBeanToProcessBlock() {
    return opEspecialRunnable;
  }

  @Override
  public int getThreadSize() {
    if (threadSize > 50) {
      LOG.warn("[OperacionEspecialFileReader :: getThreadSize] Demasiados hilos configurados para la tarea {}",
               threadSize);
    }
    return threadSize;
  }

  @Override
  public int getTransactionSize() {
    return transactionSize;
  }

  @Override
  public void preExecuteTask() {
    LOG.trace("[OperacionEspecialFileReader :: preExecuteTask] inicio");
    LOG.trace("[OperacionEspecialFileReader :: preExecuteTask] ifinnicio");
  }

  @Override
  public void postExecuteTask() {
    LOG.trace("[OperacionEspecialFileReader :: postExecuteTask] inicio");
    // this.desgloseRecordBeanBo.revisarTitulosDesgloses();
    LOG.trace("[OperacionEspecialFileReader :: postExecuteTask] fin");
  }

}
