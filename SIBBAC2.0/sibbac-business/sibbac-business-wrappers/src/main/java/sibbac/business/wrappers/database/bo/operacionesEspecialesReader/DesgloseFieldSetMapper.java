package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoCambio;
import sibbac.business.wrappers.database.model.DesgloseRecordBean.TipoOperacion;
import sibbac.common.utils.FormatDataUtils;

public class DesgloseFieldSetMapper<T extends DesgloseRecordBean> extends
                                                                  OperacionEspecialFieldSetMapper<OperacionEspecialRecordBean> {

  public enum DesgloseFields {
    TIPO_REGISTRO("tipoRegistro", 2),
    CODE_MPRL("codeMprl", 4),
    EMPR_CTVA("emprCTVA", 4),
    CENT_CTVA("centCTVA", 4),
    DPTE_CTVA("dpteCTVA", 7),
    REFERENCIA("referencia", 16),
    NUMERO_ORDEN("numeroOrden", 10),
    CODIGO_SV("codigoSV", 4),
    FECHA_EJECUCION("fechaEjecucion", 8),
    CODIGO_BOLSA_EJECUCION("codigoBolsaEjecucion", 3),
    CODIGO_VALOR_ISO("codigoValorISO", 12),
    FILLER1("filler1", 2),
    TIPO_OPERACION("tipoOperacion", 1),
    TIPO_CAMBIO("tipoCambio", 1),
    TITULOS_EJECUTADOS("titulosEjecutados", 11),
    NOMINAL_EJECUTADO("nominalEjecutado", 15),
    CAMBIO_OPERACION("cambioOperacion", 10),
    DESCRIPCION_VALOR("descripcionValor", 32),
    BIC_TITULAR("bicTitular", 11),
    TIPO_SALDO("tipoSaldo", 1),
    REFERENCIA_MODELO("referenciaModelo", 35),
    SEGMENTO("segmento", 3),
    ORDEN_COMERCIAL("ordenComercial", 2);

    private final String nombreCampo;
    private final int tamañoCampo;

    /**
     * @param nombreCampo
     */
    private DesgloseFields(final String nombreCampo, int tampañoCampo) {
      this.nombreCampo = nombreCampo;
      this.tamañoCampo = tampañoCampo;
    }

    /**
     * @return the nombreCampo
     */
    public String getNombreCampo() {
      return nombreCampo;
    }

    /**
     * @return the tamañoCampo
     */
    public int getTamañoCampo() {
      return tamañoCampo;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
      return nombreCampo;
    }

    public static String[] getNombresCampos() {
      DesgloseFields[] fields = DesgloseFields.values();
      final String[] namesArray = new String[fields.length];
      for (int i = 0; i < fields.length; i++) {
        namesArray[i] = fields[i].nombreCampo;
      }
      return namesArray;
    }

    public static int[] getTamañosCampos() {
      DesgloseFields[] fields = DesgloseFields.values();
      final int[] lengthsArray = new int[fields.length];
      for (int i = 0; i < fields.length; i++) {
        lengthsArray[i] = fields[i].tamañoCampo;
      }
      return lengthsArray;
    }
  }

  private static final String ORMA1 = "ORMA1";
  private static final Integer LONGITUD_NUMERO_ORDEN = 20;

  private String completarNumeroOrden(String sNumeroOrden) {
    /*
     * if (sNumeroOrden.indexOf(ORMA1) == -1) { sNumeroOrden+= ORMA1; }//if (sNumeroOrden.indexOf(ORMA1) == -1) while
     * (sNumeroOrden.length() < LONGITUD_NUMERO_ORDEN) { sNumeroOrden = "0"+sNumeroOrden; }//while
     * (sNumeroOrden.length() < LONGITUD_NUMERO_ORDEN)
     */
    return sNumeroOrden;

  }// private String completarNumeroOrden(String sNumeroOrden) {

  @Override
  public DesgloseRecordBean mapFieldSet(FieldSet fieldSet) throws BindException {
    final DesgloseRecordBean desgloseRecordBean = new DesgloseRecordBean();
    desgloseRecordBean.setTipoRegistro(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.TIPO_REGISTRO.getNombreCampo())));
    desgloseRecordBean.setCodEmprl(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.CODE_MPRL.getNombreCampo())));
    desgloseRecordBean.setEmprCTVA(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.EMPR_CTVA.getNombreCampo())));
    desgloseRecordBean.setCentCTVA(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.CENT_CTVA.getNombreCampo())));
    desgloseRecordBean.setDpteCTVA(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.DPTE_CTVA.getNombreCampo())));
    String sNumeroOrden = StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.REFERENCIA.getNombreCampo()));
    desgloseRecordBean.setNumeroOrden(completarNumeroOrden(sNumeroOrden));
    desgloseRecordBean.setNumeroReferenciaOrden(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.NUMERO_ORDEN.getNombreCampo())));
    desgloseRecordBean.setCodigoSV(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.CODIGO_SV.getNombreCampo())));
    desgloseRecordBean.setFechaEjecucion(fieldSet.readDate(DesgloseFields.FECHA_EJECUCION.getNombreCampo(),
                                                           FormatDataUtils.DATE_FORMAT));
    desgloseRecordBean.setCodigoBolsaEjecucion(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.CODIGO_BOLSA_EJECUCION.getNombreCampo())));
    desgloseRecordBean.setCodigoValorISO(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.CODIGO_VALOR_ISO.getNombreCampo())));
    desgloseRecordBean.setFiller1(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.FILLER1.getNombreCampo())));
    desgloseRecordBean.setTipoOperacion(TipoOperacion.getTipoOperacion(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.TIPO_OPERACION.getNombreCampo()))));
    desgloseRecordBean.setTipoCambio(TipoCambio.getTipoCambio(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.TIPO_CAMBIO.getNombreCampo()))));
    desgloseRecordBean.setTitulosEjecutados(fieldSet.readBigDecimal(DesgloseFields.TITULOS_EJECUTADOS.getNombreCampo()));
    desgloseRecordBean.setNominalEjecutado(fieldSet.readBigDecimal(DesgloseFields.NOMINAL_EJECUTADO.getNombreCampo())
                                                   .divide(new BigDecimal(100)));
    desgloseRecordBean.setCambioOperacion(fieldSet.readBigDecimal(DesgloseFields.CAMBIO_OPERACION.getNombreCampo())
                                                  .divide(new BigDecimal(10000)));
    desgloseRecordBean.setDescripcionValor(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.DESCRIPCION_VALOR.getNombreCampo())));
    desgloseRecordBean.setBicTitular(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.BIC_TITULAR.getNombreCampo())));
    desgloseRecordBean.setTipoSaldo(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.TIPO_SALDO.getNombreCampo())));
    desgloseRecordBean.setReferenciaModelo(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.REFERENCIA_MODELO.getNombreCampo())));
    desgloseRecordBean.setSegmento(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.SEGMENTO.getNombreCampo())));
    desgloseRecordBean.setOrdenComercial(StringUtils.trimToEmpty(fieldSet.readString(DesgloseFields.ORDEN_COMERCIAL.getNombreCampo())));
    return desgloseRecordBean;
  }
}
