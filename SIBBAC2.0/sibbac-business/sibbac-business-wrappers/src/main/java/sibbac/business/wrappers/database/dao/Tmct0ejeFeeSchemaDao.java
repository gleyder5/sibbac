package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchema;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchemaId;

@Repository
public interface Tmct0ejeFeeSchemaDao extends JpaRepository<Tmct0ejeFeeSchema, Tmct0ejeFeeSchemaId> {

}
