package sibbac.business.wrappers.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import sibbac.common.SIBBACBusinessException;


/**
 * Clase de utilidad para procesamiento de Excel.
 */
public class ExcelProcessingHelper {

	/**
	 * recupera el valor de una celda de excel
	 * 
	 * @param cell
	 * @return
	 */
	public static String getCellValue(Cell cell) {

		String result = "";

		if (cell != null) {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					result = cell.getDateCellValue().toString();
				} else {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					result = cell.getStringCellValue();
				}
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				result = String.valueOf(cell.getBooleanCellValue());
				break;
			default:
				break;
			}
		}
		return result.trim();
	}

	/**
	 * 	Reemplaza el valor de celda de un Excel y lo pasa a BigDecimal.
	 * 	@param valorCelda valorCelda
	 * 	@return BigDecimal BigDecimal  
	 */
	public static BigDecimal getValorCeldaABigDecimalAdaptada(String valorCelda) {
		return new BigDecimal(valorCelda.replace(",", "."));
	}

	/**
	 *	Desdobla ficheros por tipo.
	 *	@param files files
	 *	@param tipo tipo	 
	 */
	public static List<File> desdoblarFicherosPorTipo(List<File> files, String tipo) {
		List<File> filesTipoFiltrado = new ArrayList<File>();
		for (File file : files) {
			if (file.getName().toLowerCase().startsWith(tipo)) {
				filesTipoFiltrado.add(file);
			}
		}
		return filesTipoFiltrado;
	}

	/**
	 * 	Recupera el valor de una celda de excel para TLM.
	 * 	@param cell
	 * 	@return String
	 * 	@throws SIBBACBusinessException 
	 */
	public static String getCellValueTLM(Cell cell) throws SIBBACBusinessException {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String result = "";

		if (cell != null) {
			switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				result = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				if (DateUtil.isCellDateFormatted(cell)) {
					result = formatter.format(cell.getDateCellValue());
				} else {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					result = cell.getStringCellValue();
				}
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				result = String.valueOf(cell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				try {
					result = String.valueOf(cell.getStringCellValue());
				} catch (Exception ex) {
					Double valor = cell.getNumericCellValue();
					result = String.valueOf(valor);
				}
				break;
			default:
				break;
			}
		}
		return result.trim();
	}

	/**
	 *	Puesta a punto del archivo Excel. 
	 * 	@param excelFileName
	 * 	@param inputStream
	 * 	@throws SIBBACBusinessException, IOException
	 * 	@return Workbook 
	 */
	public static Workbook openWorkbook(String excelFileName, InputStream inputStream) throws SIBBACBusinessException, 
	IOException {
		if ("xlsx".equalsIgnoreCase(getFileExtension(excelFileName))) 
			return new XSSFWorkbook(inputStream);
		if ("xls".equalsIgnoreCase(getFileExtension(excelFileName))) {
			POIFSFileSystem fs = new POIFSFileSystem(inputStream);
			return new HSSFWorkbook(fs);
		}
		throw new SIBBACBusinessException("El fichero no corresponde con el formato adecuado para la importacion");
	}

	/**
	 * Recupera la extension de un fichero.
	 * @param fileName
	 * @return
	 */
	public static String getFileExtension(String fileName) {
		int mid = fileName.lastIndexOf(".");
		return fileName.substring(mid + 1, fileName.length());
	}

	/**
	 *	Devuelve true si la extension es valida.
	 *	@param fileName
	 *	@param extensionesFichero
	 *	@return boolean 
	 */
	public static boolean isExtensionValida(
		String fileName, List<String> extensionesFichero) {
		if (StringUtils.isNotBlank(fileName) && CollectionUtils.isNotEmpty(extensionesFichero)) {
			for (String extension : extensionesFichero) {
				if (extension.equalsIgnoreCase(getFileExtension(fileName))) {
					return true;
				}
			}
		}
		return false;
	}
} 
