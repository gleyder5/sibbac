package sibbac.business.wrappers.database.bo.calculos.dto;

import java.io.Serializable;

public class CalculosEconomicosFlagsDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -8332759870879424598L;

  private boolean calcularBrutoMercado = true;
  private boolean repartirBrutoClienteFidessa = true;
  private boolean repartirNetoClienteFidessa = true;
  private boolean calcularComisionCliente = true;
  private boolean repartirComisionOrdenante = true;
  private boolean calcularImcomsis = true;
  private boolean calcularImbansis = true;
  private boolean calcularCanones = true;
  private boolean calcularComisionBanco = true;
  private boolean calcularComisionDevolucion = true;
  private boolean calcularNetoCliente = true;
  private boolean calcularComisionLiquidacion = true;
  private boolean calcularComisionBeneficio = true;
  private boolean calcularAjustePrecio = true;
  private boolean repartirComisionAjuste = true;

  private boolean repartirImcobrado = true;
  // private boolean repartirConciliado = true;
  private boolean repartirImefeMercadoCobrado = true;
  private boolean repartirImefeClienteCobrado = true;
  private boolean repartirImcanonCobrado = true;
  private boolean repartirImcomCobrado = true;
  private boolean repartirImbrkPagado = true;
  private boolean repartirImdvoPagado = true;

  private boolean repartirBrutoClienteFidessaDct = true;
  private boolean repartirNetoClienteFidessaDct = true;
  private boolean calcularComisionClienteDct = true;
  private boolean repartirComisionOrdenanteDct = true;

  public CalculosEconomicosFlagsDTO() {
  }

  public void setAllFlag(boolean value) {
    calcularBrutoMercado = value;
    repartirBrutoClienteFidessa = value;
    repartirNetoClienteFidessa = value;
    calcularComisionCliente = value;
    repartirComisionOrdenante = value;
    calcularImcomsis = value;
    calcularImbansis = value;
    calcularCanones = value;
    calcularComisionBanco = value;
    calcularComisionDevolucion = value;
    calcularNetoCliente = value;
    calcularComisionLiquidacion = value;
    calcularComisionBeneficio = value;
    calcularAjustePrecio = value;
    repartirComisionAjuste = value;

    repartirImcobrado = value;
    // repartirConciliado = value;
    repartirImefeMercadoCobrado = value;
    repartirImefeClienteCobrado = value;
    repartirImcanonCobrado = value;
    repartirImcomCobrado = value;
    repartirImbrkPagado = value;
    repartirImdvoPagado = value;

    repartirBrutoClienteFidessaDct = value;
    repartirNetoClienteFidessaDct = value;
    calcularComisionClienteDct = value;
    repartirComisionOrdenanteDct = value;
  }

  public boolean isCalcularBrutoMercado() {
    return calcularBrutoMercado;
  }

  public boolean isRepartirBrutoClienteFidessa() {
    return repartirBrutoClienteFidessa;
  }

  public boolean isRepartirNetoClienteFidessa() {
    return repartirNetoClienteFidessa;
  }

  public boolean isCalcularComisionCliente() {
    return calcularComisionCliente;
  }

  public boolean isRepartirComisionOrdenante() {
    return repartirComisionOrdenante;
  }

  public boolean isCalcularImcomsis() {
    return calcularImcomsis;
  }

  public boolean isCalcularImbansis() {
    return calcularImbansis;
  }

  public boolean isCalcularCanones() {
    return calcularCanones;
  }

  public boolean isCalcularComisionBanco() {
    return calcularComisionBanco;
  }

  public boolean isCalcularComisionDevolucion() {
    return calcularComisionDevolucion;
  }

  public boolean isCalcularNetoCliente() {
    return calcularNetoCliente;
  }

  public boolean isCalcularComisionLiquidacion() {
    return calcularComisionLiquidacion;
  }

  public boolean isCalcularComisionBeneficio() {
    return calcularComisionBeneficio;
  }

  public boolean isCalcularAjustePrecio() {
    return calcularAjustePrecio;
  }

  public void setCalcularBrutoMercado(boolean calcularBrutoMercado) {
    this.calcularBrutoMercado = calcularBrutoMercado;
  }

  public void setRepartirBrutoClienteFidessa(boolean repartirBrutoClienteFidessa) {
    this.repartirBrutoClienteFidessa = repartirBrutoClienteFidessa;
  }

  public void setRepartirNetoClienteFidessa(boolean repartirNetoClienteFidessa) {
    this.repartirNetoClienteFidessa = repartirNetoClienteFidessa;
  }

  public void setCalcularComisionCliente(boolean calcularComisionCliente) {
    this.calcularComisionCliente = calcularComisionCliente;
  }

  public void setRepartirComisionOrdenante(boolean repartirComisionOrdenante) {
    this.repartirComisionOrdenante = repartirComisionOrdenante;
  }

  public void setCalcularImcomsis(boolean calcularImcomsis) {
    this.calcularImcomsis = calcularImcomsis;
  }

  public void setCalcularImbansis(boolean calcularImbansis) {
    this.calcularImbansis = calcularImbansis;
  }

  public void setCalcularCanones(boolean calcularCanones) {
    this.calcularCanones = calcularCanones;
  }

  public void setCalcularComisionBanco(boolean calcularComisionBanco) {
    this.calcularComisionBanco = calcularComisionBanco;
  }

  public void setCalcularComisionDevolucion(boolean calcularComisionDevolucion) {
    this.calcularComisionDevolucion = calcularComisionDevolucion;
  }

  public void setCalcularNetoCliente(boolean calcularNetoCliente) {
    this.calcularNetoCliente = calcularNetoCliente;
  }

  public void setCalcularComisionLiquidacion(boolean calcularComisionLiquidacion) {
    this.calcularComisionLiquidacion = calcularComisionLiquidacion;
  }

  public void setCalcularComisionBeneficio(boolean calcularComisionBeneficio) {
    this.calcularComisionBeneficio = calcularComisionBeneficio;
  }

  public void setCalcularAjustePrecio(boolean calcularAjustePrecio) {
    this.calcularAjustePrecio = calcularAjustePrecio;
  }

  public boolean isRepartirImcobrado() {
    return repartirImcobrado;
  }

  // public boolean isRepartirConciliado() {
  // return repartirConciliado;
  // }

  public boolean isRepartirImefeMercadoCobrado() {
    return repartirImefeMercadoCobrado;
  }

  public boolean isRepartirImefeClienteCobrado() {
    return repartirImefeClienteCobrado;
  }

  public boolean isRepartirImcanonCobrado() {
    return repartirImcanonCobrado;
  }

  public boolean isRepartirImcomCobrado() {
    return repartirImcomCobrado;
  }

  public boolean isRepartirImbrkPagado() {
    return repartirImbrkPagado;
  }

  public boolean isRepartirImdvoPagado() {
    return repartirImdvoPagado;
  }

  public void setRepartirImcobrado(boolean repartirImcobrado) {
    this.repartirImcobrado = repartirImcobrado;
  }

  // public void setRepartirConciliado(boolean repartirConciliado) {
  // this.repartirConciliado = repartirConciliado;
  // }

  public void setRepartirImefeMercadoCobrado(boolean repartirImefeMercadoCobrado) {
    this.repartirImefeMercadoCobrado = repartirImefeMercadoCobrado;
  }

  public void setRepartirImefeClienteCobrado(boolean repartirImefeClienteCobrado) {
    this.repartirImefeClienteCobrado = repartirImefeClienteCobrado;
  }

  public void setRepartirImcanonCobrado(boolean repartirImcanonCobrado) {
    this.repartirImcanonCobrado = repartirImcanonCobrado;
  }

  public void setRepartirImcomCobrado(boolean repartirImcomCobrado) {
    this.repartirImcomCobrado = repartirImcomCobrado;
  }

  public void setRepartirImbrkPagado(boolean repartirImbrkPagado) {
    this.repartirImbrkPagado = repartirImbrkPagado;
  }

  public void setRepartirImdvoPagado(boolean repartirImdvoPagado) {
    this.repartirImdvoPagado = repartirImdvoPagado;
  }

  public boolean isRepartirComisionAjuste() {
    return repartirComisionAjuste;
  }

  public void setRepartirComisionAjuste(boolean repartirComisionAjuste) {
    this.repartirComisionAjuste = repartirComisionAjuste;
  }

  public boolean isRepartirBrutoClienteFidessaDct() {
    return repartirBrutoClienteFidessaDct;
  }

  public boolean isRepartirNetoClienteFidessaDct() {
    return repartirNetoClienteFidessaDct;
  }

  public boolean isCalcularComisionClienteDct() {
    return calcularComisionClienteDct;
  }

  public boolean isRepartirComisionOrdenanteDct() {
    return repartirComisionOrdenanteDct;
  }

  public void setRepartirBrutoClienteFidessaDct(boolean repartirBrutoClienteFidessaDct) {
    this.repartirBrutoClienteFidessaDct = repartirBrutoClienteFidessaDct;
  }

  public void setRepartirNetoClienteFidessaDct(boolean repartirNetoClienteFidessaDct) {
    this.repartirNetoClienteFidessaDct = repartirNetoClienteFidessaDct;
  }

  public void setCalcularComisionClienteDct(boolean calcularComisionClienteDct) {
    this.calcularComisionClienteDct = calcularComisionClienteDct;
  }

  public void setRepartirComisionOrdenanteDct(boolean repartirComisionOrdenanteDct) {
    this.repartirComisionOrdenanteDct = repartirComisionOrdenanteDct;
  }

  @Override
  public String toString() {
    return "CalculosEconomicosFlagsDTO [calcularBrutoMercado=" + calcularBrutoMercado
        + ", repartirBrutoClienteFidessa=" + repartirBrutoClienteFidessa + ", repartirNetoClienteFidessa="
        + repartirNetoClienteFidessa + ", calcularComisionCliente=" + calcularComisionCliente
        + ", repartirComisionOrdenante=" + repartirComisionOrdenante + ", calcularImcomsis=" + calcularImcomsis
        + ", calcularImbansis=" + calcularImbansis + ", calcularCanones=" + calcularCanones
        + ", calcularComisionBanco=" + calcularComisionBanco + ", calcularComisionDevolucion="
        + calcularComisionDevolucion + ", calcularNetoCliente=" + calcularNetoCliente
        + ", calcularComisionLiquidacion=" + calcularComisionLiquidacion + ", calcularComisionBeneficio="
        + calcularComisionBeneficio + ", calcularAjustePrecio=" + calcularAjustePrecio + "]";
  }

}
