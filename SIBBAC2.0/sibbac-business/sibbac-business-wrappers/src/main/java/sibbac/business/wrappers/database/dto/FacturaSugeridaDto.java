package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.FacturaSugerida;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.common.utils.FormatDataUtils;


public class FacturaSugeridaDto {

	private Long		idFacturaSugerida;
	private Integer		idEstado;
	private String		descEstado;
	private String		fechaCreacion;
	private String		fechaInicio;
	private String		fechaFin;
	private String		cdAlias;
	private String		nombreAlias;
	private Long		idPeriodo;
	private String		descPeriodo;
	private String		comentarios;
	private BigDecimal	importe;
	private Integer		nContacto;
	private Integer		nDireccion;
	private Integer		nCIF;

	public FacturaSugeridaDto( final FacturaSugerida facturaSugerida, final Alias aliasFacturacion, final String cif ) {
		this.idFacturaSugerida = facturaSugerida.getId();
		final Tmct0estado estado = facturaSugerida.getEstado();
		this.idEstado = estado.getIdestado();
		this.descEstado = estado.getNombre();
		final Periodos periodo = facturaSugerida.getPeriodo();
		this.idPeriodo = periodo.getId();
		this.cdAlias = aliasFacturacion.getCdaliass();
		this.nDireccion = aliasFacturacion.getAliasDireccion() != null ? 1 : 0;
		this.nContacto = aliasFacturacion.getContactoDefecto() != null ? 1 : 0;
		this.nCIF = StringUtils.isNotEmpty( cif ) ? 1 : 0;
		this.nombreAlias = aliasFacturacion.getDescrali();
		this.comentarios = facturaSugerida.getComentarios();
		this.fechaCreacion = FormatDataUtils.convertDateToString( facturaSugerida.getFechaCreacion() );
		this.fechaInicio = FormatDataUtils.convertDateToString( facturaSugerida.getFechaInicio() );
		this.fechaFin = FormatDataUtils.convertDateToString( facturaSugerida.getFechaFin() );
		this.importe = facturaSugerida.getImfinsvb();
	}

	/**
	 * @return the idFacturaSugerida
	 */
	public Long getIdFacturaSugerida() {
		return idFacturaSugerida;
	}

	/**
	 * @param idFacturaSugerida
	 *            the idFacturaSugerida to set
	 */
	public void setIdFacturaSugerida( Long idFacturaSugerida ) {
		this.idFacturaSugerida = idFacturaSugerida;
	}

	/**
	 * @return the idEstado
	 */
	public Integer getIdEstado() {
		return idEstado;
	}

	/**
	 * @param idEstado
	 *            the idEstado to set
	 */
	public void setIdEstado( Integer idEstado ) {
		this.idEstado = idEstado;
	}

	/**
	 * @return the descEstado
	 */
	public String getDescEstado() {
		return descEstado;
	}

	/**
	 * @param descEstado
	 *            the descEstado to set
	 */
	public void setDescEstado( String descEstado ) {
		this.descEstado = descEstado;
	}

	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @return the fechaInicio
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio the fechaInicio to set
	 */
	public void setFechaInicio( String fechaInicio ) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return the fechaFin
	 */
	public String getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin the fechaFin to set
	 */
	public void setFechaFin( String fechaFin ) {
		this.fechaFin = fechaFin;
	}

	/**
	 * @param fechaCreacion
	 *            the fechaCreacion to set
	 */
	public void setFechaCreacion( String fechaCreacion ) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the cdAlias
	 */
	public String getCdAlias() {
		return cdAlias;
	}

	/**
	 * @param cdAlias
	 *            the cdAlias to set
	 */
	public void setCdAlias( String cdAlias ) {
		this.cdAlias = cdAlias;
	}

	/**
	 * @return the nombreAlias
	 */
	public String getNombreAlias() {
		return nombreAlias;
	}

	/**
	 * @param nombreAlias
	 *            the nombreAlias to set
	 */
	public void setNombreAlias( String nombreAlias ) {
		this.nombreAlias = nombreAlias;
	}

	/**
	 * @return the idPeriodo
	 */
	public Long getIdPeriodo() {
		return idPeriodo;
	}

	/**
	 * @param idPeriodo
	 *            the idPeriodo to set
	 */
	public void setIdPeriodo( Long idPeriodo ) {
		this.idPeriodo = idPeriodo;
	}

	/**
	 * @return the descPeriodo
	 */
	public String getDescPeriodo() {
		return descPeriodo;
	}

	/**
	 * @param descPeriodo
	 *            the descPeriodo to set
	 */
	public void setDescPeriodo( String descPeriodo ) {
		this.descPeriodo = descPeriodo;
	}

	/**
	 * @return the comentarios
	 */
	public String getComentarios() {
		return comentarios;
	}

	/**
	 * @param comentarios
	 *            the comentarios to set
	 */
	public void setComentarios( String comentarios ) {
		this.comentarios = comentarios;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte( BigDecimal importe ) {
		this.importe = importe;
	}

	/**
	 * @return the nContacto
	 */
	public Integer getnContacto() {
		return nContacto;
	}

	/**
	 * @param nContacto the nContacto to set
	 */
	public void setnContacto( Integer nContacto ) {
		this.nContacto = nContacto;
	}

	/**
	 * @return the nDireccion
	 */
	public Integer getnDireccion() {
		return nDireccion;
	}

	/**
	 * @param nDireccion the nDireccion to set
	 */
	public void setnDireccion( Integer nDireccion ) {
		this.nDireccion = nDireccion;
	}

	/**
	 * @return the nPlantilla
	 */
	public Integer getnCIF() {
		return nCIF;
	}

	/**
	 * @param nPlantilla the nPlantilla to set
	 */
	public void setnCIF( Integer nCIF ) {
		this.nCIF = nCIF;
	}

}
