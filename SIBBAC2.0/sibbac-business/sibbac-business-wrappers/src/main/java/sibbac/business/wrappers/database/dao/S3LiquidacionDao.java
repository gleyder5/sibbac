package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Netting;
import sibbac.business.wrappers.database.model.S3Liquidacion;

@Repository
public interface S3LiquidacionDao extends JpaRepository<S3Liquidacion, Long> {

  public List<S3Liquidacion> findByidFichero(String idFichero);

  @Query("SELECT L FROM S3Liquidacion L WHERE L.nbooking=:nbooking AND L.nucnfclt=:nucnfclt AND L.nucnfliq=:nucnfliq ORDER BY L.id")
  public List<S3Liquidacion> findByNbookingAndNucnflictAndNcnfliq(@Param("nbooking") String nbooking,
                                                                  @Param("nucnfclt") String nucnflct,
                                                                  @Param("nucnfliq") short ncnfliq);

  @Query("SELECT L FROM S3Liquidacion L WHERE L.nbooking=:nbooking AND L.nucnfclt=:nucnfclt AND L.nucnfliq=:nucnfliq ORDER BY L.id")
  public List<S3Liquidacion> findByNbookingAndNucnflictAndNcnfliqOrder(@Param("nbooking") String nbooking,
                                                                       @Param("nucnfclt") String nucnflct,
                                                                       @Param("nucnfliq") short ncnfliq);

  @Query("SELECT L FROM S3Liquidacion L WHERE L.idnetting=:netting ORDER BY L.id")
  public List<S3Liquidacion> findByNettingOrder(@Param("netting") Netting netting);

  public List<S3Liquidacion> findByContabilizadoAndCdestadoS3AndCdorigen(Character contabilizado,
                                                                         String cdestadoS3,
                                                                         Character cdorigen);

  @Query("SELECT L FROM S3Liquidacion L WHERE L.idFichero=:idFichero ORDER BY L.auditDate")
  public List<S3Liquidacion> findByidFicheroOrdAudit_date(@Param("idFichero") String idFichero);

  @Query("SELECT L FROM S3Liquidacion L WHERE L.idnetting=:netting ORDER BY L.auditDate")
  public List<S3Liquidacion> findByNettingOrderAudit_date(@Param("netting") Netting netting);

  /**
   * 
   * @param contabilizado
   * @return
   * @author XI316153
   */
  @Query("SELECT L FROM S3Liquidacion L WHERE L.cdestadoS3= :cdestadoS3 AND L.contabilizado = :contabilizado AND L.idnetting IS NULL ORDER BY L.id ASC")
  public List<S3Liquidacion> findClearingByContabilizado(@Param("cdestadoS3") String cdestadoS3,
                                                         @Param("contabilizado") Character contabilizado);

  /**
   * 
   * @param contabilizado
   * @return
   * @author XI316153
   */
  @Query("SELECT L FROM S3Liquidacion L WHERE L.cdestadoS3= :cdestadoS3 AND L.contabilizado = :contabilizado AND L.idnetting IS NOT NULL ORDER BY L.id ASC")
  public List<S3Liquidacion> findNettingByContabilizado(@Param("cdestadoS3") String cdestadoS3,
                                                        @Param("contabilizado") Character contabilizado);

  /**
   * 
   * @param idFichero
   * @param cdestadoS3
   * @param fechaFichero
   * @return
   */
  // @Query("SELECT L FROM S3Liquidacion L WHERE L.idFichero= :idFichero AND L.cdestadoS3= :cdestadoS3 AND L.fechaFichero = :fechaFichero")
  @Query(value = "SELECT ID FROM BSNBPSQL.TMCT0_S3LIQUIDACION"
                 + " WHERE FECHA_FICHERO=:fechaFichero AND IDFICHERO=:idFichero AND CDESTADOS3=:cdestadoS3"
                 + " AND FECHAOPERACION=:fechaOperacion AND FECHAVALOR=:fechaValor"
                 + " AND NUTIULOLIQUIDADOS=:nutituloLiquidados AND IMPORTENETOLIQUIDADO=:importeNetoLiquidado",
         nativeQuery = true)
  public List<Long> findIdByFields(@Param("fechaFichero") Date fechaFichero,
                                   @Param("idFichero") String idFichero,
                                   @Param("cdestadoS3") String cdestadoS3,
                                   @Param("fechaOperacion") Date fechaOperacion,
                                   @Param("fechaValor") Date fechaValor,
                                   @Param("nutituloLiquidados") BigDecimal nutituloLiquidados,
                                   @Param("importeNetoLiquidado") BigDecimal importeNetoLiquidado);

  /**
   * 
   * @param idFichero
   * @param cdestadoS3
   * @return
   */
  public List<S3Liquidacion> findByidFicheroAndCdestadoS3(String idFichero, String cdestadoS3);

  /**
   * 
   * @param idFichero
   * @param cdestadoS3
   * @return
   */
  @Query("SELECT sum(L.nutituloLiquidados), sum(L.importeNetoLiquidado), L.cdestadoS3 FROM S3Liquidacion L"
         + " WHERE L.idFichero=:idFichero AND L.cdestadoS3=:cdestadoS3" + " GROUP BY L.cdestadoS3")
  public Object[][] findByIdFicheroAndCdestadoS3(@Param("idFichero") String idFichero,
                                                 @Param("cdestadoS3") String cdestadoS3);

  /**
   * 
   * @param idFichero
   * @param cdestadoS3
   * @return
   */
  @Query("SELECT sum(L.nutituloLiquidados), sum(L.importeNetoLiquidado), L.cdestadoS3 FROM S3Liquidacion L"
         + " WHERE L.idFichero IN (:idFichero) AND L.cdestadoS3=:cdestadoS3" + " GROUP BY L.cdestadoS3")
  public Object[][] findByIdFicheroListAndCdestadoS3(@Param("idFichero") List<String> idFichero,
                                                     @Param("cdestadoS3") String cdestadoS3);
  
  @Query("SELECT sum(L.nutituloLiquidados) FROM S3Liquidacion L WHERE L.idFichero = :referenciaS3 AND "
          + "L.cdestadoS3 = :estado")
  public BigDecimal sumTitulosLiquidadosPorReferencia(@Param("referenciaS3") String referenciaS3, 
          @Param("estado") String estado);
  
  @Query("SELECT l FROM S3Liquidacion l WHERE l.idFichero = :referenciaS3")
  public List<S3Liquidacion> findByReferenciaS3(@Param("referenciaS3") String referenciaS3);

} // S3LiquidacionDao
