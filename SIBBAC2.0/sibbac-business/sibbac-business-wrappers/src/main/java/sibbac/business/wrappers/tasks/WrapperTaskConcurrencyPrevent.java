package sibbac.business.wrappers.tasks;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.bo.Tmct0menErrorHistBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.SIBBACTaskException;
import sibbac.common.SIBBACTaskExecutionErrorType;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums;
import sibbac.tasks.SIBBACTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Inet4Address;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public abstract class WrapperTaskConcurrencyPrevent extends SIBBACTask {

  @Value("${environment}")
  protected String environment;

  @Value("${sibbac.process.exception.mail.to}")
  protected String mailTo;

  @Value("${sibbac.process.exception.mail.cc}")
  protected String mailCc;

  @Value("${sibbac.process.exception.mail.subject}")
  protected String mailSubject;

  /**
   * Para cambiar el estado de la tarea.
   */
  @Autowired
  protected Tmct0menBo menBo;
  @Autowired
  protected Tmct0menErrorHistBo errorHistBo;

  @Autowired
  protected SendMail mail;

  public abstract void executeTask() throws Exception;

  public abstract TIPO determinarTipoApunte();

  @Override
  public void execute() {

    // Ejecucion de la tarea en si
    LOG.trace(">>> LAUNCHING TASK EXECUTION.");
    // Para asegurarse de que se inicializa el tipo de apunte y el
    // nombre de la clase (para las trazas)
    TIPO tipoApunte = determinarTipoApunte();
    // Hay tareas que controlan si se están ejecutando independientemente,
    // por eso se pone if, si tipoApunte no es nulo se controla desde aquí.
    if (tipoApunte != null) {
      Tmct0men tmct0men = null;
      TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
      TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
      TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
      try {
        LOG.debug("execute ==> Detectando si se está ejecutando la tarea ....");
        Tmct0men men  =  menBo.findByTipo(tipoApunte);
        final  boolean  intervalTimeValid = menBo.validateIntervalTime(men);
        if(!intervalTimeValid){
            throw new SIBBACTaskException(StringUtils.trim("La tarea "+men.getDsmensa()+" no se ha ejecutado porque no se encuentra dentro del intervalo de tiempo asignado en la tabla TMCT0MEN."), SIBBACTaskExecutionErrorType.NOT_IN_INTERVAL_TIME);
        }
        if(!menBo.validarHost(men, Inet4Address.getLocalHost().getHostName())) {//validacion para permitir la ejecucion de tarea dentro de los nodos especificados en la tabla y separados por ; y  por hora
        	throw new SIBBACTaskException(StringUtils.trim("La tarea "+men.getDsmensa()+" no se ha ejecutado, el entorno actual no se encuentra en los hosts permitidos."),SIBBACTaskExecutionErrorType.HOST_NOT_ALLOWED);
        }
          tmct0men = menBo.putEstadoMEN(tipoApunte, estadoIni, estadoExe);

          try {
        	this.executeTask();
            menBo.putEstadoMEN(tmct0men, estadoIni);
            LOG.debug("execute Se acabó la tarea y se dejó en estado disponible");
          } catch (Exception ex) {
              errorHistBo.registerError(men,ex);
              this.sendMailWithExceptionMessage(tmct0men, ex);
              menBo.putEstadoMEN(tmct0men, estadoError);
              LOG.warn("Intento de ejecución de la tarea. {}", ex.getMessage(), ex);
          }
      }
      catch (SIBBACTaskException ex) {
          LOG.info(ex.getMessage());
          LOG.trace(ex.getMessage(), ex);
          registerError(tmct0men,tipoApunte,ex);

      }
      catch (SIBBACBusinessException ex) {
          LOG.info(ex.getMessage());
          LOG.trace(ex.getMessage(), ex);
          registerError(tmct0men,tipoApunte,ex);
      }
      catch (Exception ex) {
    	  LOG.warn("{}  cause: {}", ex.getMessage(), ex.getCause(), ex);
          registerError(tmct0men,tipoApunte,ex);
      }
    }
    else {
      try {
        this.executeTask();
      }
      catch (Exception e) {
        LOG.warn(" Incidencia ejecutando la tarea. {}", e.getMessage(), e);
        this.sendMailWithExceptionMessage(null, e);
      }
    }

  }
  private void registerError(Tmct0men tmct0men, TIPO tipoApunte,Exception ex){
    Tmct0men men  =  menBo.findByTipo(tipoApunte);
    errorHistBo.registerError(men,ex);
  }

  @Transactional
  protected void sendMailWithExceptionMessage(Tmct0men tmct0men, Exception ex) {
    this.sendMailWithExceptionMessage(tmct0men, ex, null);
  }

  @Transactional
  protected void sendMailWithExceptionMessage(Tmct0men tmct0men, Exception ex, List<File> files) {
    LOG.trace("[sendMailWithExceptionMessage] inicio.");
    String cdmensa;
    String dsmensa;
    List<String> to;
    List<String> cc;
    List<InputStream> listIsToAttach;
    List<String> listNamesFilesAttach;

    String mailBodyReplaced;
    String mailSubjectReplaced;
    try {
      cdmensa = "UNDEFINED";
      dsmensa = this.getClass().getName();

      if (tmct0men != null) {
        cdmensa = StringUtils.trim(tmct0men.getCdmensa());
        dsmensa = StringUtils.trim(tmct0men.getDsmensa());
      }
      to = new ArrayList<String>();
      cc = new ArrayList<String>();

      if (files != null && !files.isEmpty()) {
        listIsToAttach = new ArrayList<InputStream>();
        listNamesFilesAttach = new ArrayList<String>();
        for (File f : files) {
          listIsToAttach.add(new FileInputStream(f));
          listNamesFilesAttach.add(f.getName());
        }
      }
      else {
        listIsToAttach = null;
        listNamesFilesAttach = null;
      }

      try {
        LOG.trace("[sendMailWithExceptionMessage] montando el to.");
        if (StringUtils.isNotBlank(mailTo)) {
          to.addAll(Arrays.asList(mailTo.replace(",", ";").split(";")));
        }

        LOG.trace("[sendMailWithExceptionMessage] montando el cc.");
        if (StringUtils.isNotBlank(mailCc)) {
          cc.addAll(Arrays.asList(mailCc.replace(",", ";").split(";")));
        }
        if (CollectionUtils.isEmpty(to) && CollectionUtils.isNotEmpty(cc)) {
          LOG.warn("[sendMailWithExceptionMessage] hay cc sin to.");
          to.addAll(cc);
          cc.clear();
        }
        if (CollectionUtils.isNotEmpty(to)) {
          try (StringWriter errors = new StringWriter(); PrintWriter pw = new PrintWriter(errors)) {
            ex.printStackTrace(pw);
            LOG.trace("[sendMailWithExceptionMessage] montando subject del email...");
            mailSubjectReplaced = MessageFormat.format(mailSubject, cdmensa, dsmensa, new Date(), environment);
            LOG.trace("[sendMailWithExceptionMessage] montando body del email con la exception...");
            mailBodyReplaced = String.format("%s\\n\\n%s", mailSubjectReplaced, errors.toString());
            LOG.trace("[sendMailWithExceptionMessage] enviando email a to: {}  cc: {} \nsubject: {} \nbody:\n {}...",
                to, cc, mailSubjectReplaced, mailBodyReplaced);
            mail.sendMail(to, mailSubjectReplaced, mailBodyReplaced, listIsToAttach, listNamesFilesAttach, cc);
          }

        }
        else {
          LOG.warn("[sendMailWithExceptionMessage] No se ha configurado destinatario para enviar el email con la excepcion.");
        }
      }
      catch (Exception e) {
        throw e;
      }
      
    }
    catch (Exception e) {
      LOG.warn("No se ha podido mandar el correo con el error. {}", e.getMessage(), e);
    }
    LOG.trace("[sendMailWithExceptionMessage] fin.");
  }

}
