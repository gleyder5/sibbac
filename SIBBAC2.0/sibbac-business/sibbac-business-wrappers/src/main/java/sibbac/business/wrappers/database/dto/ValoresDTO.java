package sibbac.business.wrappers.database.dto;


/**
 * 
 * @author Cristina
 *
 */
public class ValoresDTO {

	private String	codigo;
	private String	descripcion;

	/**
	 * @param codigo
	 * @param descripcion
	 */
	public ValoresDTO( String codigo, String descripcion ) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo( String codigo ) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

}
