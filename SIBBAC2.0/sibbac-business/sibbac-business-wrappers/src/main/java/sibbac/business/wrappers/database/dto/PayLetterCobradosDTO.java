package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @version 1.0
 * @author XI316153
 */
public class PayLetterCobradosDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -6393274807760240753L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  private Date feejeliq;
  private String nbooking;
 
  private char tpoper;
  private String nbvalor;
  private String isin;
  private BigDecimal titulos;
  private BigDecimal efectivo;
  private BigDecimal devolucion;
 
  // CFF 13/10/2015
  private String nuorden;
  private String nucnfclt;
  private short nucnfliq;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * 
   * @param nbooking
   * @param nucnfliq
   * @param nucnfclt
   * @param feejeliq
   * @param titulos
   * @param efectivo
   * @param devolucion
   * @param tpoper
   * @param nbvalor
   */
  public PayLetterCobradosDTO(String nbooking, /* short nucnfliq, String nucnfclt, */
                      Date feejeliq,
                      BigDecimal titulos,
                      BigDecimal efectivo,
                      BigDecimal devolucion,
                      char tpoper,
                      String nbvalor,
                      String isin,
                      String nuorden, //CFF 13/05/2015.
                      String nucnfclt,   //CFF 13/05/2015.
		  			  short nucnfliq	  //CFF 13/05/2015.
		  		) {
    this.feejeliq = feejeliq;
    this.nbooking = nbooking;
    // this.nucnfclt = nucnfclt;
    // this.nucnfliq = new BigDecimal(nucnfliq);
    this.tpoper = tpoper;
    this.nbvalor = nbvalor;
    this.isin = isin;
    this.titulos = titulos;
    this.efectivo = efectivo;
    this.devolucion = devolucion;
    this.nuorden = nuorden;   //CFF 13/05/2015.
    this.nucnfclt = nucnfclt;    //CFF 13/05/2015.
    this.nucnfliq = nucnfliq;  //CFF 13/05/2015.

    // this.realizado = this.devolucion;
    // this.pendiente = BigDecimal.ZERO;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PayLetterDTO [feejeliq=" + feejeliq + ", nbooking=" + nbooking + ", tpoper=" + tpoper + ", nbvalor=" + nbvalor + ", isin="
        + isin + ", titulos=" + titulos + ", efectivo=" + efectivo + ", devolucion=" + devolucion + "]";
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the feejeliq
   */
  public Date getFeejeliq() {
    return feejeliq;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  // /**
  // * @return the nucnfclt
  // */
  // public String getNucnfclt() {
  // return nucnfclt;
  // }
  //
  // /**
  // * @return the nucnfliq
  // */
  // public BigDecimal getNucnfliq() {
  // return nucnfliq;
  // }

  /**
   * @return the tpoper
   */
  public char getTpoper() {
    return tpoper;
  }

  /**
   * @return the nbvalor
   */
  public String getNbvalor() {
    return nbvalor;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the titulos
   */
  public BigDecimal getTitulos() {
    return titulos;
  }

  /**
   * @return the efectivo
   */
  public BigDecimal getEfectivo() {
    return efectivo;
  }

  /**
   * @return the devolucion
   */
  public BigDecimal getDevolucion() {
    return devolucion;
  }

  // /**
  // * @return the realizado
  // */
  // public BigDecimal getRealizado() {
  // return realizado;
  // }
  //
  // /**
  // * @return the pendiente
  // */
  // public BigDecimal getPendiente() {
  // return pendiente;
  // }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  // /**
  // * @param nucnfclt the nucnfclt to set
  // */
  // public void setNucnfclt(String nucnfclt) {
  // this.nucnfclt = nucnfclt;
  // }
  //
  // /**
  // * @param nucnfliq the nucnfliq to set
  // */
  // public void setNucnfliq(BigDecimal nucnfliq) {
  // this.nucnfliq = nucnfliq;
  // }

  /**
   * @param tpoper the tpoper to set
   */
  public void setTpoper(char tpoper) {
    this.tpoper = tpoper;
  }

  /**
   * @param nbvalor the nbvalor to set
   */
  public void setNbvalor(String nbvalor) {
    this.nbvalor = nbvalor;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param titulos the titulos to set
   */
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  /**
   * @param efectivo the efectivo to set
   */
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  /**
   * @param devolucion the devolucion to set
   */
  public void setDevolucion(BigDecimal devolucion) {
    this.devolucion = devolucion;
  }

  
  //CFF 13/10/2015.
	public String getNuorden() {
		return nuorden;
	}
	
	public void setNuorden(String nuorden) {
		this.nuorden = nuorden;
	}
	

	public String getNucnfclt() {
		return nucnfclt;
	}

	public void setNucnfclt(String nucnfclt) {
		this.nucnfclt = nucnfclt;
	}

	public short getNucnfliq() {
		return nucnfliq;
	}
	
	public void setNucnfliq(short nucnfliq) {
		this.nucnfliq = nucnfliq;
	}
	
	

  // /**
  // * @param realizado the realizado to set
  // */
  // public void setRealizado(BigDecimal realizado) {
  // this.realizado = realizado;
  // }
  //
  // /**
  // * @param pendiente the pendiente to set
  // */
  // public void setPendiente(BigDecimal pendiente) {
  // this.pendiente = pendiente;
  // }


  
  
  
} // AfectacionDTO

