package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0bokDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0bokDynamicValuesId;

@Repository
public interface Tmct0bokDynamicValuesDao extends JpaRepository<Tmct0bokDynamicValues, Tmct0bokDynamicValuesId> {

}
