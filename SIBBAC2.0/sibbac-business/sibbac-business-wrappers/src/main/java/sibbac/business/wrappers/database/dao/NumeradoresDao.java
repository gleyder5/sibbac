package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.business.wrappers.database.model.Numeradores;
import sibbac.business.wrappers.database.model.TipoDeDocumento;


@Component
public interface NumeradoresDao extends JpaRepository< Numeradores, Long > {

	public List< Numeradores > findAllByTipoDeDocumento( TipoDeDocumento tipoDeDocumento );

	@Transactional
	public Numeradores
			findAllByTipoDeDocumentoAndIdentificador( final TipoDeDocumento tipoDeDocumento, final Identificadores identificador );

}
