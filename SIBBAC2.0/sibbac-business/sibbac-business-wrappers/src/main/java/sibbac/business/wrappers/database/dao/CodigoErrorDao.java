package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.data.CodigoErrorData;
import sibbac.business.wrappers.database.model.CodigoError;

@Repository
public interface CodigoErrorDao extends JpaRepository<CodigoError, Long> {

    public CodigoError findByCodigo(String codigo);

    @Query("SELECT new sibbac.business.wrappers.database.data.CodigoErrorData(c.id, c.codigo, c.descripcion, c.grupo) FROM CodigoError c WHERE c.codigo=:codigo")
    public CodigoErrorData findByCodigoData(@Param("codigo") String codigo);

    public List<CodigoError> findAllByCodigo(String codigo);

    public List<CodigoError> findAllByGrupoOrderByCodigoAsc(String grupo);

    @Query("SELECT new sibbac.business.wrappers.database.data.CodigoErrorData(c.id, c.codigo, c.descripcion, c.grupo) FROM CodigoError c WHERE c.grupo=:grupo")
    public List<CodigoErrorData> findAllByGrupoOrderByCodigoDataAsc(
	    @Param("grupo") String grupo);
    
    @Query("SELECT new sibbac.business.wrappers.database.data.CodigoErrorData(c.id, c.codigo, c.descripcion, c.grupo) FROM CodigoError c WHERE c.codigo IN (:codigo) order by c.codigo")
    public List<CodigoErrorData> findByCodigoOrderByCodigoDataAsc(@Param("codigo")List<String>  codigos) ;
   

}
