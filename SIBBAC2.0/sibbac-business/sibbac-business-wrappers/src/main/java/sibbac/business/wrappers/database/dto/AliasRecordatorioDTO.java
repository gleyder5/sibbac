package sibbac.business.wrappers.database.dto;


import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.AliasRecordatorio;


public class AliasRecordatorioDTO {

	private Long		idAliasRecordatorio;
	private Long		idAlias;
	private BigDecimal	importeMinimo;
	private Integer		frecuencia;

	public AliasRecordatorioDTO() {

	}

	public AliasRecordatorioDTO( final AliasRecordatorio aliasRecordatorio ) {

		this.idAliasRecordatorio = aliasRecordatorio.getId();
		this.idAlias = aliasRecordatorio.getAlias().getId();
		this.importeMinimo = aliasRecordatorio.getImporteMinimo();
		this.frecuencia = aliasRecordatorio.getFrecuencia();

	}

	public AliasRecordatorioDTO( final Long idAliasRecordatorio, final Long idAlias, final BigDecimal importeMinimo,
			final Integer frecuencia ) {

		this.idAlias = idAlias;
		this.idAliasRecordatorio = idAliasRecordatorio;
		this.importeMinimo = importeMinimo;
		this.frecuencia = frecuencia;

	}

	public Long getIdAliasRecordatorio() {
		return idAliasRecordatorio;
	}

	/**
	 * @return the idAlias
	 */
	public Long getIdAlias() {
		return idAlias;
	}

	/**
	 * @param idAlias the idAlias to set
	 */
	public void setIdAlias( Long idAlias ) {
		this.idAlias = idAlias;
	}

	public BigDecimal getImporteMinimo() {
		return importeMinimo;
	}

	public void setIdAliasRecordatorio( Long idAliasRecordatorio ) {
		this.idAliasRecordatorio = idAliasRecordatorio;
	}

	public void setImporteMinimo( BigDecimal importeMinimo ) {
		this.importeMinimo = importeMinimo;
	}

	public Integer getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia( Integer frecuencia ) {
		this.frecuencia = frecuencia;
	}

}
