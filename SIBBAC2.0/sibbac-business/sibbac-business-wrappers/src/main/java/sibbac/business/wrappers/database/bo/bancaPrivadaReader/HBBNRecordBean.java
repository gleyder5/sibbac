package sibbac.business.wrappers.database.bo.bancaPrivadaReader;


import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.RecordBean;


public class HBBNRecordBean extends RecordBean {

	/**
   * 
   */
  private static final long serialVersionUID = -7053696736389329375L;
  private BigDecimal codOrigen;
	private BigDecimal secuencia;

	public BigDecimal getCodOrigen() {
		return codOrigen;
	}

	public void setCodOrigen( BigDecimal codOrigen ) {
		this.codOrigen = codOrigen;
	}

	public BigDecimal getSecuencia() {
		return secuencia;
	}

	public void setSecuencia( BigDecimal secuencia ) {
		this.secuencia = secuencia;
	}

}
