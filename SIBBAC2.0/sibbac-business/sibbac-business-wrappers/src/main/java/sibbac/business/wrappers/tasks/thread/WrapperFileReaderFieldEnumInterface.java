package sibbac.business.wrappers.tasks.thread;

public interface WrapperFileReaderFieldEnumInterface {

  public String getName();

  public int getLength();

  public int getScale();

  public WrapperFileReaderFieldType getFielType();

  // TODO NECESITAMOS EL PATTERN PARA LAS FECHAS????

}
