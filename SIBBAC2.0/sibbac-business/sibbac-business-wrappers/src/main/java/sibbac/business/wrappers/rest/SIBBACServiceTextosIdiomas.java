package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.TextosIdiomasBo;
import sibbac.business.wrappers.database.dto.TextoIdiomaDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * The Class SIBBACServiceTextosIdiomas
 */
@SIBBACService
public class SIBBACServiceTextosIdiomas implements SIBBACServiceBean {

	private static final String	CMD_UPDATE_TEXTOSIDIOMAS			= "updateTextosIdiomas";
	private static final String	CMD_FIND_TEXTOSIDIOMAS_BYIDTEXTO	= "findTextoIdiomaByIdTexto";
	private static final String	CMD_CREATE_TEXTOSIDIOMAS			= "createTextosIdiomas";
	private static final String	CMD_GET_TEXTOSIDIOMAS				= "getListaTextosIdiomas";

	private static final String	RESULTADO							= "resultado";
	private static final String	RESULTADO_SUCCESS					= "SUCCESS";
	private static final String	ID_TEXTO_IDIOMA						= "id";
	private static final String	DESCRIPCION							= "descripcion";
	private static final String	ID_IDIOMA							= "idIdiomas";
	private static final String	ID_TEXTO							= "idTexto";													;

	/** The Constant LOG. */
	private static final Logger	LOG									= LoggerFactory.getLogger( SIBBACServiceTextosIdiomas.class );

	/** The textosIdioma bo. */
	@Autowired
	private TextosIdiomasBo		textosIdiomasBo;

	public SIBBACServiceTextosIdiomas() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		LOG.debug( "[SIBBACServiceTextosIdiomas::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// obtiene comandos(paths)
		String action = webRequest.getAction();
		LOG.debug( "[SIBBACServiceTextosIdiomas::process] " + "+ Command.: [{}]", action );
		switch ( action ) {
			case SIBBACServiceTextosIdiomas.CMD_CREATE_TEXTOSIDIOMAS:
				this.createTextoIdioma( webRequest, webResponse );
				break;
			case SIBBACServiceTextosIdiomas.CMD_GET_TEXTOSIDIOMAS:
				this.getListaTextosIdiomas( webRequest, webResponse );
				break;
			case SIBBACServiceTextosIdiomas.CMD_FIND_TEXTOSIDIOMAS_BYIDTEXTO:
				this.findTextoIdiomaByIdTexto( webRequest, webResponse );
				break;
			case SIBBACServiceTextosIdiomas.CMD_UPDATE_TEXTOSIDIOMAS:
				this.updateTextosIdiomas( webRequest, webResponse );
				break;
			default:
				webResponse.setError( "No recibe ningun comando" );
				LOG.error( "No le llega ningun comando" );
				break;
		}

		return webResponse;
	}

	private void getListaTextosIdiomas( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, List< TextoIdiomaDTO > > resultados = new HashMap< String, List< TextoIdiomaDTO > >();
		resultados.put( RESULTADO, textosIdiomasBo.getListaTextosIdiomas() );
		webResponse.setResultados( resultados );
	}

	private void updateTextosIdiomas( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idTextoIdioma = getIdTextoIdioma( filters.get( SIBBACServiceTextosIdiomas.ID_TEXTO_IDIOMA ) );
		final String descripcion = getDescripcion( filters.get( DESCRIPCION ) );
		final Long idTexto = getIdTexto( filters.get( SIBBACServiceTextosIdiomas.ID_TEXTO ) );
		final Long idIdioma = getIdIdioma( filters.get( SIBBACServiceTextosIdiomas.ID_IDIOMA ) );
		LOG.debug( "[SIBBACServiceTextosIdiomas::updateTextosIdiomas] [{}=={}]", DESCRIPCION, descripcion );

		final TextoIdiomaDTO textoIdiomaDTO = new TextoIdiomaDTO( idTextoIdioma, descripcion, idTexto, idIdioma );
		final Map< String, TextoIdiomaDTO > resultados = new HashMap< String, TextoIdiomaDTO >();
		resultados.put( RESULTADO, textosIdiomasBo.updateTextoIdioma( textoIdiomaDTO ) );
		webResponse.setResultados( resultados );
	}

	private void createTextoIdioma( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final String descripcion = getDescripcion( StringUtils.trimToNull( filters.get( DESCRIPCION ) ) );
		final Long idIdioma = getIdIdioma( filters.get( ID_IDIOMA ) );
		final Long idTexto = getIdTexto( filters.get( ID_TEXTO ) );

		textosIdiomasBo.createTextosIdioma( descripcion, idTexto, idIdioma );
		final Map< String, String > resultados = new HashMap< String, String >();
		resultados.put( RESULTADO, RESULTADO_SUCCESS );
		webResponse.setResultados( resultados );
	}

	private void findTextoIdiomaByIdTexto( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filters = webRequest.getFilters();
		if ( MapUtils.isEmpty( filters ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idTexto = getIdTexto( filters.get( ID_TEXTO ) );
		final Map< String, List< TextoIdiomaDTO > > resultados = new HashMap< String, List< TextoIdiomaDTO > >();
		resultados.put( RESULTADO, textosIdiomasBo.findTextoIdiomaByIdTexto( idTexto ) );
		webResponse.setResultados( resultados );
	}

	/**
	 * @param filters
	 * @return
	 */
	private String getDescripcion( final String descripcion ) throws SIBBACBusinessException {
		if ( StringUtils.isBlank( descripcion ) ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"descripcion\": [" + descripcion + "] " );
		}
		return descripcion;
	}

	/**
	 * @param idTextoIdiomaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdTexto( final String idTextoIdiomaStr ) throws SIBBACBusinessException {
		Long idTextoIdioma = null;
		try {
			if ( StringUtils.isNotBlank( idTextoIdiomaStr ) ) {
				idTextoIdioma = NumberUtils.createLong( idTextoIdiomaStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idTextoIdioma\": [" + idTextoIdiomaStr + "] " );
		}
		return idTextoIdioma;
	}

	/**
	 * @param idIdiomaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdIdioma( final String idIdiomaStr ) throws SIBBACBusinessException {
		Long idIdioma = null;
		try {
			if ( StringUtils.isNotBlank( idIdiomaStr ) ) {
				idIdioma = NumberUtils.createLong( idIdiomaStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idIdioma\": [" + idIdiomaStr + "] " );
		}
		return idIdioma;
	}

	/**
	 * @param idTextoStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdTextoIdioma( final String idTextoStr ) throws SIBBACBusinessException {
		Long idTexto = null;
		try {
			if ( StringUtils.isNotBlank( idTextoStr ) ) {
				idTexto = NumberUtils.createLong( idTextoStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idTexto\": [" + idTextoStr + "] " );
		}
		return idTexto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( SIBBACServiceTextosIdiomas.CMD_CREATE_TEXTOSIDIOMAS );
		commands.add( SIBBACServiceTextosIdiomas.CMD_GET_TEXTOSIDIOMAS );
		commands.add( SIBBACServiceTextosIdiomas.CMD_FIND_TEXTOSIDIOMAS_BYIDTEXTO );
		commands.add( SIBBACServiceTextosIdiomas.CMD_UPDATE_TEXTOSIDIOMAS );

		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	}

}
