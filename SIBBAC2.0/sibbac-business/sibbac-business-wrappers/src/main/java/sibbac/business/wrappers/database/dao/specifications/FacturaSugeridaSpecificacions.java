package sibbac.business.wrappers.database.dao.specifications;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.FacturaSugerida;

public class FacturaSugeridaSpecificacions {

  protected static final Logger LOG = LoggerFactory.getLogger(FacturaSugeridaSpecificacions.class);

  public static Specification<FacturaSugerida> alias(final Long idAlias) {
    return new Specification<FacturaSugerida>() {

      public Predicate toPredicate(Root<FacturaSugerida> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate aliasPredicate = null;
        LOG.trace("[FacturaSugeridaSpecificacions::alias] [aliasId=={}]", idAlias);
        if (idAlias != null) {
          aliasPredicate = builder.equal(root.<Alias> get("alias").<Long> get("id"), idAlias);
        }
        return aliasPredicate;
      }
    };
  }

  public static Specification<FacturaSugerida> estados(final List<Integer> estados) {
    return new Specification<FacturaSugerida>() {

      public Predicate toPredicate(Root<FacturaSugerida> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate estadosPredicate = null;
        if (CollectionUtils.isNotEmpty(estados)) {
          LOG.trace("[FacturaSugeridaSpecificacions::estados] [estados=={}]", estados);
          final Path<Tmct0estado> estado = root.<Tmct0estado> get("estado");
          estadosPredicate = estado.in(estados);
        }
        return estadosPredicate;
      }
    };
  }

  public static Specification<FacturaSugerida> fechaCreacion(final Date fechaCreacionHasta) {
    return new Specification<FacturaSugerida>() {

      public Predicate toPredicate(Root<FacturaSugerida> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Predicate fechaCreacionHastaPredicate = null;
        if (fechaCreacionHasta != null) {
          LOG.trace("[FacturaSugeridaSpecificacions::fechaCreacion] [fechaCreacion=={}]", fechaCreacionHasta);
          fechaCreacionHastaPredicate = builder.lessThanOrEqualTo(root.<Date> get("fechaCreacion"), fechaCreacionHasta);
        }
        return fechaCreacionHastaPredicate;
      }
    };
  }

  public static Specification<FacturaSugerida> listadoFriltrado(final Long idAlias, final List<Integer> estados, final Date fechaCreacionHasta) {
    return new Specification<FacturaSugerida>() {

      public Predicate toPredicate(Root<FacturaSugerida> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        Specifications<FacturaSugerida> specs = null;
        if (idAlias != null) {
          specs = Specifications.where(alias(idAlias));
        }

        if (CollectionUtils.isNotEmpty(estados)) {
          if (specs == null) {
            specs = Specifications.where(estados(estados));
          } else {
            specs = specs.and(estados(estados));
          }
        }

        if (fechaCreacionHasta != null) {
          if (specs == null) {
            specs = Specifications.where(fechaCreacion(fechaCreacionHasta));
          } else {
            specs = specs.and(fechaCreacion(fechaCreacionHasta));
          }
        }

        Predicate predicate = null;
        if (specs != null) {
          predicate = specs.toPredicate(root, query, builder);
        }
        return predicate;
      }

    };
  }
}
