package sibbac.business.wrappers.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase que proporcionará métodos reutilizables por varios proyectos.
 */
public class Helper {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Helper.class);

	/**
	 * Método que devuelve una lista de fechas partiendo de una fecha inicio y una fecha fin .
	 *
	 * @param dFechaInicio - Fecha de inicio
	 * @param dFechaFin - Fecha de Fin
	 * @return Lista con todos los dias que hay entre las fechas dadas
	 * @throws Exception
	 */
	public static List<Date> separarFechas(Date dFechaInicio, Date dFechaFin) throws Exception {
		LOG.debug("Entra en separarFechas");

		List<Date> lResultado = new ArrayList<Date>();

		Date diaSiguiente;
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

		try {
			// Tratamiento de las fechas de inicio y final.
			// Se tiene que incluir la fecha de inicio que no puede ser nula.
			lResultado.add(dFechaInicio);

			// Si la fecha final es nula, se inicializa con la fecha actual sin horas minutos y segundos
			if (dFechaFin == null) {

				dFechaFin = formatoFecha.parse(formatoFecha.format(new Date()));
			}

			diaSiguiente = dFechaInicio;

			Calendar calendar = Calendar.getInstance();
			while (diaSiguiente.before(dFechaFin)) {
				calendar.setTime(diaSiguiente);
				calendar.add(Calendar.DAY_OF_YEAR, 1);
				diaSiguiente = calendar.getTime();
				lResultado.add(diaSiguiente);
			}

		} catch (Exception e) {
			LOG.error("separarFechas - Se produjo un error al obtener las fechas " + e.getMessage());
			throw e;
		}

		LOG.debug("Fin en separarFechas");

		return lResultado;

	}
	
	/**
	 *	Se utiliza en filtros que contengan los operadores ["=", "≠", "~", "≥", "≤"].
	 *	Devuelve un fragmento de consulta a evaluar en la query segun operador.
	 *	@param expresion
	 *	@param operador
	 *	@param valor 
	 *	@return String[] 
	 */
	public static String[] getQueryFragmentByOperator(
		String expression, String operator, String valor) {
		String[] valores = new String[2];
		StringBuffer sbFragment = new StringBuffer();
		switch (operator) {
		case "IGUAL":
			// EQUALS
			valores[0] = sbFragment.append(" = " + expression).toString();
			valores[1] = valor;
			break;
		case "DISTINTO":
			// DISTINCT
			valores[0] = sbFragment.append(" <> " + expression).toString();
			valores[1] = valor;
			break;
		case "CONTIENE":
			// CONTAINS
			valores[0] = sbFragment.append(" LIKE " + expression).toString();
			valores[1] = "%" + valor + "%";
			break;
		case "COMIENZA":
			// START WITH
			valores[0] = sbFragment.append(" LIKE " + expression).toString();
			valores[1] = valor + "%";
			break;
		case "FINALIZA":
			// END WITH
			valores[0] = sbFragment.append(" LIKE " + expression).toString();
			valores[1] = "%" + valor;
			break;
		default:
			break;
		}
		return valores;
	}

}
