package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo.CriteriosComunicacionTitulares;
import sibbac.business.wrappers.database.dao.Tmct0DesgloseCamaraEnvioRtDao;
import sibbac.business.wrappers.database.dao.Tmct0enviotitularidadDao;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0enviotitularidad;
import sibbac.business.wrappers.database.model.Tmct0referenciatitular;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0DesgloseCamaraEnvioRtBo extends
    AbstractBo<Tmct0DesgloseCamaraEnvioRt, Long, Tmct0DesgloseCamaraEnvioRtDao> {

  @Autowired
  private Tmct0enviotitularidadDao tiDao;

  public Tmct0DesgloseCamaraEnvioRtBo() {
  }

  public List<Tmct0DesgloseCamaraEnvioRt> findAllByNudesglose(long nudesglose) {
    return dao.findAllByNudesglose(nudesglose);
  }

  public List<Tmct0alc> findAllAlcsByIdEnvioTitularidadAndNotCdestadoDeAlta(long idEnvioTitularidad) {
    return dao.findAllAlcsByIdEnvioTitularidadAndNotCdestadoAndCdestadoasigGte(idEnvioTitularidad, 'C',
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
  }

  public List<Tmct0DesgloseCamaraEnvioRt> findAllByIdEnvioTitularidadAndNotCdestado(long idEnvioTitularidad,
      Character notCdestado, Character notCdestado1) {
    return dao.findAllByIdEnvioTitularidadAndNotCdestado(idEnvioTitularidad, notCdestado, notCdestado1);
  }

  public List<Tmct0DesgloseCamaraEnvioRt> findAllForReceiveRt(String refTit, Date fechaInicio, String cdmiembro,
      String cdcamara, String criterioCommTitular, char sentido) {
    List<Tmct0DesgloseCamaraEnvioRt> res = new ArrayList<Tmct0DesgloseCamaraEnvioRt>();
    Tmct0enviotitularidad ti = tiDao.findByReferenciaTitularAndFechaInicioAndCdmiembromktAndCdcamara(refTit,
        fechaInicio, cdmiembro, cdcamara);
    if (ti != null) {

      res.addAll(dao.findAllByIdEnviotitularidadAndCriterioComunicacionTitularAndEstadoAndCdestadoasigGteAndSentido(
          ti.getIdenviotitularidad(), criterioCommTitular, 'E', EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId(),
          sentido));
    }
    return res;
  }

  public Tmct0DesgloseCamaraEnvioRt crearDesgloseCamaraEnvioRt(Tmct0desglosecamara dc, Tmct0desgloseclitit dct,
      Tmct0referenciatitular refTit, Tmct0enviotitularidad ti, String camaraNacional, String camaraEuroccp,
      String auditUser) {
    return this.crearDesgloseCamaraEnvioRt(dc, dct, dct.getImtitulos(), refTit, ti, camaraNacional, camaraEuroccp,
        auditUser);
  }

  public Tmct0DesgloseCamaraEnvioRt crearDesgloseCamaraEnvioRt(Tmct0desglosecamara dc, Tmct0desgloseclitit dct,
      BigDecimal titulos, Tmct0referenciatitular refTit, Tmct0enviotitularidad ti, String camaraNacional,
      String camaraEuroccp, String auditUser) {
    Tmct0DesgloseCamaraEnvioRt rt = new Tmct0DesgloseCamaraEnvioRt();
    rt.setCdmiembromkt(ti.getCdmiembromkt());
    rt.setCdoperacionecc(dct.getCdoperacionecc());
    rt.setCdPlataformaNegociacion(dct.getCdPlataformaNegociacion());

    rt.setEnvioTitularidad(ti);
    rt.setDesgloseCamara(dc);
    rt.setEstado('P');
    rt.setFecontratacion(dc.getFecontratacion());

    if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(camaraEuroccp)) {
      rt.setCriterioComunicacionTitular(String.format("%s%s", dc.getSentido() == 'C' ? "B" : "S", dct.getNurefexemkt()
          .trim()));
      rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.EUROCCP_EXECUTION_ID);
      rt.setNuordenmkt("");
    }
    else if (dc.getTmct0CamaraCompensacion().getCdCodigo().trim().equals(camaraNacional)) {
      if (dct.getCdoperacionecc() != null && !dct.getCdoperacionecc().trim().isEmpty()) {
        rt.setCriterioComunicacionTitular(dct.getCdoperacionecc());
        rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.NUMERO_OPERACION_ECC);
      }
      else if (dct.getNumeroOperacionDCV() != null && dct.getNumeroOperacionDCV().trim().isEmpty()) {
        rt.setCriterioComunicacionTitular(dct.getNumeroOperacionDCV());
        rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.NUMERO_OPERACION_DCV);
      }
      else if (dct.getNuordenmkt() != null && !dct.getNuordenmkt().trim().isEmpty()) {
        rt.setCriterioComunicacionTitular(String.format("%s%s",
            FormatDataUtils.convertDateToString(dc.getFecontratacion()), dct.getNuordenmkt().trim()));
        rt.setIdentificacionCriterioComunicacionTitular(CriteriosComunicacionTitulares.NUMERO_ORDEN_MERCADO);
      }
      rt.setNuordenmkt(dct.getNuordenmkt().trim());
    }

    rt.setImtitulos(titulos);

    if (dct.getNudesglose() != null) {
      rt.setNudesglose(dct.getNudesglose());
    }

    rt.setReferenciaTitular(refTit.getCdreftitularpti());
    rt.setTmct0desgloseclitit(dct);
    rt.setAudit_fecha_cambio(new Date());
    rt.setAudit_user(auditUser);
    return rt;
  }
}
