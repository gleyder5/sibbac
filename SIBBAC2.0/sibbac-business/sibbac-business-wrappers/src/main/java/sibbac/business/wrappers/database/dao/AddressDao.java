package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import sibbac.business.wrappers.database.model.Tmct0Address;

@Component
public interface AddressDao extends JpaRepository< Tmct0Address, Long > {

}
