package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CONTABILIDAD;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_COMPROBANTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_ESTADO;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.data.InformeOpercacionData;
import sibbac.business.wrappers.database.desglose.bo.DesgloseTmct0aloHelper;
import sibbac.business.wrappers.database.dto.AlcEntregaRecepcionClienteDTO;
import sibbac.business.wrappers.database.dto.DatosForQueryPartenonDTO;
import sibbac.business.wrappers.database.dto.RechazoApunteDto;
import sibbac.business.wrappers.database.model.AlcEstadoCont;
import sibbac.business.wrappers.database.model.S3Errores;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.helper.GenericLoggers;
import sibbac.common.SIBBACBusinessException;

@Repository
public class Tmct0alcDaoImp {
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0alcDaoImp.class.getName());
  private static final DateFormat dF1 = new SimpleDateFormat("dd/MM/yyyy");
  private static final List<Integer> SIN_LIQUIDAR = new ArrayList<Integer>();
  private static final List<Integer> LIQUIDADO_MERCADO = new ArrayList<Integer>();
  private static final List<Integer> IMPUTADA = new ArrayList<Integer>();
  private static final List<Integer> SIN_DEVENGAR = new ArrayList<Integer>();
  private static final List<Integer> DEVENGADO = new ArrayList<Integer>();
  private static final List<Integer> FACTURADO_SIN_COBRAR = new ArrayList<Integer>();
  private static final List<Integer> COBRADO = new ArrayList<Integer>();
  private static final Map<Integer, List<Integer>> L_TIPOLOGIA = new HashMap<Integer, List<Integer>>();
  // 17-10-2016. Se optimiza la query que estaba haciendo FULL SCAN de varias tablas
  private static final String queryFindRouting = "SELECT DISTINCT {0} FROM tmct0bok bok,tmct0alo alo,tmct0ali ali,tmct0alc alc, tmct0desglosecamara dc, tmct0movimientoecc mec "
                                                 + "WHERE alo.nuorden=bok.nuorden AND alo.nbooking=bok.nbooking and alc.nuorden=alo.nuorden and "
                                                 + "alc.nbooking=alo.nbooking and alc.nucnfclt=alo.nucnfclt and ali.cdaliass=bok.cdalias and "
                                                 + "ali.fhfinal=99991231 and bok.fetrade>:oneYearOld AND alc.cdestadoasig IN (:lEstadosAsig) AND "
                                                 + "alc.cdestadocont IN (:lEstadocont) and dc.nuorden=alc.nuorden and dc.nbooking=alc.nbooking and "
                                                 + "dc.nucnfclt=alc.nucnfclt and dc.nucnfliq=alc.nucnfliq and dc.cdestadoasig IN (:lEstadosAsig) and "
                                                 + "mec.iddesglosecamara=dc.iddesglosecamara and mec.cdestadomov=9 and coalesce(mec.CUENTA_COMPENSACION_DESTINO,'''')='''' and "
                                                 + "mec.miembro_compensador_destino=:keyvalue and mec.CD_REF_ASIGNACION in (:cdauxili) and :cdestliq=:cdestliq";

  private static final String fieldsRouting = "alc.feejeliq,alc.nuorden,alc.nbooking,alc.nucnfclt,bok.cdalias,"
                                              + "alc.nucnfliq,alc.feopeliq,alc.cdestadocont,alc.nbtitliq,ali.auxiliar,alc.imcomdvo,alc.imcombrk,ali.isgrupo,"
                                              + "alc.imajusvb,alc.imcomisn,alc.imcanoncompeu,alc.imcanoncontreu,alc.imcanonliqeu,alo.canoncontrpagoclte";

  private static final String orderRouting = " order by alc.feejeliq,bok.cdalias";

  // 17-10-2016. Se optimiza la query que estaba haciendo FULL SCAN de varias tablas
  private static final String queryFindClearing = "SELECT DISTINCT {0} FROM tmct0bok bok,tmct0ali ali,tmct0alc alc, tmct0desglosecamara dc, tmct0movimientoecc mec "
                                                  + "WHERE alc.nuorden=bok.nuorden AND alc.nbooking=bok.nbooking and ali.cdaliass=bok.cdalias and "
                                                  + "ali.fhfinal=99991231 and bok.fetrade>:oneYearOld and alc.feopeliq<=CURRENT_DATE AND "
                                                  + "alc.cdestadoasig IN (:lEstadosAsig) AND alc.cdestadocont IN (:lEstadocont) and dc.nuorden=alc.nuorden and "
                                                  + "dc.nbooking=alc.nbooking and dc.nucnfclt=alc.nucnfclt and dc.nucnfliq=alc.nucnfliq and "
                                                  + "dc.cdestadoasig IN (:lEstadosAsig) and mec.iddesglosecamara=dc.iddesglosecamara and mec.cdestadomov=9 and "
                                                  + "coalesce(mec.CUENTA_COMPENSACION_DESTINO,'''')>'''' and "
                                                  + ":cdauxili=:cdauxili and :keyvalue=:keyvalue and :cdestliq=:cdestliq";

  private static final String fieldsClearing = "alc.feejeliq,alc.nuorden,alc.nbooking,alc.nucnfclt,bok.cdalias,"
                                               + "alc.nucnfliq,alc.feopeliq,alc.cdestadocont,alc.nbtitliq,ali.auxiliar,alc.imcomdvo,alc.imcombrk,ali.isgrupo,"
                                               + "alc.imfinsvb";

  private static final String orderClearing = "";

  // 17-10-2016. Se optimiza la query que estaba haciendo FULL SCAN de varias tablas
  private static final String queryFindCorretaje = "SELECT DISTINCT {0} FROM tmct0bok bok,tmct0ali ali,tmct0alc alc, tmct0desglosecamara dc, tmct0movimientoecc mec "
                                                   + "WHERE alc.nuorden=bok.nuorden AND alc.nbooking=bok.nbooking and ali.cdaliass=bok.cdalias and "
                                                   + "ali.fhfinal=99991231 and bok.fetrade>:oneYearOld and alc.feopeliq<=CURRENT_DATE AND "
                                                   + "alc.cdestadoasig IN (:lEstadosAsig) and alc.cdestadocont IN (:lEstadocont) and dc.nuorden=alc.nuorden and "
                                                   + "dc.nbooking=alc.nbooking and dc.nucnfclt=alc.nucnfclt and dc.nucnfliq=alc.nucnfliq and "
                                                   + "dc.cdestadoasig IN (:lEstadosAsig) and mec.iddesglosecamara=dc.iddesglosecamara and mec.cdestadomov=9 and "
                                                   + "coalesce(mec.CUENTA_COMPENSACION_DESTINO,'''')='''' and coalesce(alc.corretaje_pti,'''')>'''' and "
                                                   + ":cdauxili=:cdauxili and :keyvalue=:keyvalue and :cdestliq=:cdestliq";

  private static final String fieldsCorretaje = "alc.feejeliq,alc.nuorden,alc.nbooking,alc.nucnfclt,bok.cdalias,"
                                                + "alc.nucnfliq,alc.feopeliq,alc.cdestadocont,alc.nbtitliq,ali.auxiliar,alc.imcomdvo,alc.imcombrk,ali.isgrupo,"
                                                + "alc.imfinsvb";

  private static final String orderCorretaje = " order by alc.feejeliq";

  // 17-10-2016. Se optimiza la query que estaba haciendo FULL SCAN de varias tablas
  private static final String queryFindFacturacion = "SELECT DISTINCT {0} FROM tmct0bok bok,tmct0ali ali,tmct0alc alc, tmct0desglosecamara dc, tmct0movimientoecc mec "
                                                     + "WHERE alc.nuorden=bok.nuorden AND alc.nbooking=bok.nbooking and ali.cdaliass=bok.cdalias and "
                                                     + "ali.fhfinal=99991231 and bok.fetrade>:oneYearOld and alc.feopeliq<=CURRENT_DATE AND alc.cdestliq=:cdestliq AND "
                                                     + "alc.cdestadoasig IN (:lEstadosAsig) and alc.cdestadocont IN (:lEstadocont) and dc.nuorden=alc.nuorden and "
                                                     + "dc.nbooking=alc.nbooking and dc.nucnfclt=alc.nucnfclt and dc.nucnfliq=alc.nucnfliq and "
                                                     + "dc.cdestadoasig IN (:lEstadosAsig) and mec.iddesglosecamara=dc.iddesglosecamara and mec.cdestadomov=9 and "
                                                     + "coalesce(mec.CUENTA_COMPENSACION_DESTINO,'''')='''' and coalesce(alc.corretaje_pti,'''')='''' and "
                                                     + "mec.CD_REF_ASIGNACION not in (:cdauxili) and :keyvalue=:keyvalue ";

  private static final String fieldsFacturacion = "alc.feejeliq,alc.nuorden,alc.nbooking,alc.nucnfclt,bok.cdalias,"
                                                  + "alc.nucnfliq,alc.feopeliq,alc.cdestadocont,alc.nbtitliq,ali.auxiliar,alc.imcomdvo,alc.imcombrk,ali.isgrupo,"
                                                  + "alc.imfinsvb";

  private static final String orderFacturacion = " order by alc.feejeliq,bok.cdalias";

  private static final String count = "count(*)";

  private static final String queryFindInforme = "select trim(bok.cdalias),trim(bok.descrali),alc.feejeliq,"
                                                 + "alc.feopeliq,trim(bok.cdtpoper),alc.nutitliq,bok.imcbmerc,alc.imefeagr,coalesce(alc.imcanoncompeu,0)+"
                                                 + "coalesce(alc.imcanoncontreu,0)+coalesce(alc.imcanonliqeu,0),alc.imcomisn,alc.imnfiliq,trim(bok.cdisin),"
                                                 + "trim(bok.nbooking),trim(bok.nuorden),trim(alc.nucnfclt),trim(alc.nucnfliq),case when des.desgloses>0 "
                                                 + "and alc.cdestadocont=320 then ''Liquidado mercado pero no pata cliente'' else est.nombre end estado,"
                                                 + "apt.audit_date,alc.imajusvb,coalesce(cobrado,0) cobrado,alo.canoncontrpagoclte,alc.imfinsvb,alc.imcomdvo,"
                                                 + "alc.imcombrk,alc.imcombco,alc.imntbrok, (select ord.nureford from tmct0ord ord where ord.nuorden=alc.nuorden) as nureford "
                                                 + "from tmct0bok bok,tmct0alo alo,tmct0estado est,tmct0alc alc left join "
                                                 + "tmct0_alc_estadocont est on tipoestado=10 and alc.nuorden=est.nuorden and alc.nbooking=est.nbooking and "
                                                 + "alc.nucnfclt=est.nucnfclt and alc.nucnfliq=est.nucnfliq left join (select nuorden,nbooking,nucnfclt,"
                                                 + "nucnfliq,sum(case when cdestadocont=330 then 1 else 0 end) desgloses from tmct0desglosecamara group by "
                                                 + "nuorden,nbooking,nucnfclt,nucnfliq) des on des.nuorden=alc.nuorden and des.nbooking=alc.nbooking and "
                                                 + "des.nucnfclt=alc.nucnfclt and des.nucnfliq=alc.nucnfliq left join (select nuorden,nbooking,nucnfliq,"
                                                 + "nucnfclt,sum(a.importe) cobrado FROM tmct0_apunte_contable_alc a,tmct0_apunte_contable b,"
                                                 + "tmct0_cuenta_contable c where a.apunte=b.id and b.cuenta=c.id and c.cuenta in (1021101) group by nuorden,"
                                                 + "nbooking,nucnfclt,nucnfliq) cbr on cbr.nuorden=alc.nuorden and cbr.nbooking=alc.nbooking and cbr.nucnfclt="
                                                 + "alc.nucnfclt and cbr.nucnfliq=alc.nucnfliq{0}{1} where bok.nuorden=alc.nuorden and bok.nbooking="
                                                 + "alc.nbooking and alc.nuorden=alo.nuorden and alc.nbooking=alo.nbooking and alc.nucnfclt="
                                                 + "alo.nucnfclt and est.idestado=alc.cdestadocont{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}";


  private static final String queryFindInformeEPLO = "SELECT trim(bok.cdalias),trim(bok.descrali),alc.feejeliq,"
                                                     + "alc.feopeliq,trim(bok.cdtpoper),alc.nutitliq,bok.imcbmerc,alc.imefeagr,"
                                                     + "coalesce(alc.imcanoncompeu,0)+coalesce(alc.imcanoncontreu,0)+coalesce(alc.imcanonliqeu,0),alc.imcomisn,"
                                                     + "alc.imnfiliq,trim(bok.cdisin),trim(bok.nbooking),trim(bok.nuorden),trim(alc.nucnfclt),"
                                                     + "trim(alc.nucnfliq),est.nombre,null,alc.imajusvb,"
                                                     + "coalesce(cobrado,0) cobrado,alo.canoncontrpagoclte,alc.imfinsvb,alc.imcomdvo,"
                                                     + "alc.imcombrk,alc.imcombco,alc.imntbrok, (select ord.nureford from tmct0ord ord where ord.nuorden=alc.nuorden) as nureford "
                                                     + "FROM tmct0alc alc"
                                                     + " left join (select nuorden,nbooking,nucnfliq,"
                                                     + "nucnfclt,sum(a.importe) cobrado FROM tmct0_apunte_contable_alc a,tmct0_apunte_contable b,"
                                                     + "tmct0_cuenta_contable c where a.apunte=b.id and b.cuenta=c.id and c.cuenta in (1021101) group by nuorden,"
                                                     + "nbooking,nucnfclt,nucnfliq) cbr on cbr.nuorden=alc.nuorden and cbr.nbooking=alc.nbooking and cbr.nucnfclt="
                                                     + "alc.nucnfclt and cbr.nucnfliq=alc.nucnfliq,"
                                                     + "tmct0bok bok, tmct0estado est,tmct0desglosecamara dc,tmct0infocompensacion inf,tmct0alo alo "
                                                     + "WHERE bok.nuorden=alc.nuorden and "
                                                     + "bok.nbooking=alc.nbooking and "
                                                     + "est.idestado=alc.cdestadocont and alc.nuorden=dc.nuorden and alc.nbooking=dc.nbooking and "
                                                     + "alc.nucnfclt=dc.nucnfclt and alc.nucnfliq=dc.nucnfliq and dc.idinfocompensacion=inf.idinfocomp and "
                                                     + "alc.nuorden=alo.nuorden and alc.nbooking=alo.nbooking and alc.nucnfclt=alo.nucnfclt and "
                                                     + "inf.cdctacompensacion=''00P''{0}{1}{2}{3}{4}";

  private static final String queryTipoOrden = " left join (SELECT afi.nuorden,afi.nbooking,afi.nucnfclt,"
                                               + "afi.nucnfliq FROM tmct0afi afi,tmct0cfg cfg WHERE afi.nudnicif=cfg.keyvalue and cfg.APPLICATION="
                                               + "'SBRMV' AND process='SVB' AND keyname='svb.cif' group by afi.nuorden,afi.nbooking,afi.nucnfclt,afi."
                                               + "nucnfliq) afi on alc.nuorden=afi.nuorden and alc.nbooking=afi.nbooking and alc.NUCNFCLT="
                                               + "afi.NUCNFCLT and alc.NUCNFLIQ=afi.NUCNFLIQ";

  private static final String querySinContabilizar = " left join(select nbooking,nuorden,nucnfliq,nucnfclt,min(audit_date)"
                                                     + " audit_date from tmct0_apunte_contable_alc group by nbooking,nuorden,nucnfliq,nucnfclt) apt on"
                                                     + " alc.nbooking=apt.nbooking and alc.nuorden=apt.nuorden and alc.nucnfliq=apt.nucnfliq"
                                                     + " and alc.nucnfclt=apt.nucnfclt";

  private static final String queryContabilizado1 = ",(select nbooking,nuorden,nucnfliq,nucnfclt,min(alc.audit_date)"
                                                    + " audit_date from tmct0_apunte_contable apt,tmct0_apunte_contable_alc alc where apt.id=alc.apunte"
                                                    + "{0}{1} group by nbooking,nuorden,nucnfliq,nucnfclt) apt";

  private static final String queryContabilizado2 = " and alc.nbooking=apt.nbooking and alc.nuorden=apt.nuorden and "
                                                    + "alc.nucnfliq=apt.nucnfliq and alc.nucnfclt=apt.nucnfclt";

  private static final String QUERY_RECHAZOS_ALCS = "SELECT alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ FROM BSNBPSQL.TMCT0ALC alc"
                                                    + " WHERE alc.CDESTADOASIG = 0 AND alc.CDESTADOCONT IN (315, 316, 320)";

  private static final String QUERY_RECHAZOS_APUNTES = "SELECT {0}"
                                                       + " FROM BSNBPSQL.TMCT0ALC alc, BSNBPSQL.TMCT0_APUNTE_CONTABLE apt, BSNBPSQL.TMCT0_APUNTE_CONTABLE_ALC aptAlc"
                                                       + " WHERE alc.NUORDEN = ''{1}'' AND alc.NBOOKING = ''{2}'' AND alc.NUCNFCLT = ''{3}'' AND alc.NUCNFLIQ = {4}"
                                                       + "       AND alc.NUORDEN = aptAlc.NUORDEN AND alc.NBOOKING = aptAlc.NBOOKING AND alc.NUCNFCLT = aptAlc.NUCNFCLT AND alc.NUCNFLIQ = aptAlc.NUCNFLIQ"
                                                       + "       AND aptAlc.APUNTE = apt.ID AND apt.TIPO_COMPROBANTE IN (70, 72, 75, 77, 80)";

  private static final String FIELDS_RECHAZOS_APUNTES = "apt.APUNTE, apt.CONCEPTO, apt.DESCRIPCION, apt.FECHA, apt.IMPORTE, apt.OPERATIVA,"
                                                        + " apt.TIPO_COMPROBANTE, apt.TIPO_MOVIMIENTO, apt.NUMERO, apt.CUENTA, aptAlc.IMPORTE as aptalcImporte";
  private static final String FETCH = " FETCH FIRST {0,number,#} ROWS ONLY";

  /**
   * Calcula la suma de cánones y corretajes de los tmct0alc cuyas ordenes tengan los números de referencias
   * especificados.
   */
  private static final String QUERY_SUM_CANON_CORRETAJE = "SELECT SUM (imcanoncontreu), SUM (imcomisn), SUM (imajusvb), COUNT(*), ali.cdaliass, alc.feejeliq"
                                                          + " FROM BSNBPSQL.TMCT0ORD ord, BSNBPSQL.TMCT0BOK bok, BSNBPSQL.TMCT0ALC alc, BSNBPSQL.TMCT0ALI ali"
                                                          + " WHERE ord.nureford IN ({0})"
                                                          + "       AND ord.nuorden = bok.nuorden AND bok.cdalias = ali.cdaliass AND ali.fhfinal='99991231'"
                                                          + "       AND bok.nuorden = alc.nuorden AND bok.nbooking = alc.nbooking AND alc.cdestadoasig IN ({1})"
                                                          + " GROUP BY ali.cdaliass, alc.feejeliq";

  /**
   * Calcula la suma de cánones y corretajes de los tmct0alc cuyas ordenes tengan los números de referencias
   * especificados.
   */
  private static final String QUERY_SUM_CANON_CORRETAJE_CTA_123 = "SELECT SUM (imcanoncontreu), SUM (imcomisn), SUM (imajusvb), COUNT(*), ali.cdaliass, alc.feejeliq"
                                                                  + " FROM BSNBPSQL.TMCT0BOK bok, BSNBPSQL.TMCT0ALC alc, BSNBPSQL.TMCT0ALI ali"
                                                                  + " WHERE alc.nucnfclt  IN ({0}) AND  bok.cdalias = ali.cdaliass AND ali.fhfinal='99991231' "
                                                                  + "       AND bok.nuorden = alc.nuorden AND bok.nbooking = alc.nbooking AND alc.cdestadoasig IN ({1})"
                                                                  + " GROUP BY ali.cdaliass, alc.feejeliq";

  // No contemplo cobros parciales

  private static final String QUERY_CORRETAJES_BME = "SELECT alc.nuorden, alc.nbooking, alc.nucnfclt, alc.nucnfliq,"
                                                     + "     (COALESCE(alc.imfinsvb, 0) - COALESCE(alc.imcobrado, 0)) IMPORTE, alc.feejeliq, alc.cdestadoasig"
                                                     + " FROM BSNBPSQL.TMCT0ALC alc"
                                                     + " WHERE alc.cdestadoasig IN ({0}) AND alc.cdestadocont={1} AND alc.feopeliq=:feopeliq";


  /**
   * Actualiza el estado contable de los tmct0bok cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_BOK_CDESTADOCONT_BY_NUREFORD = "UPDATE BSNBPSQL.TMCT0BOK bok SET bok.CDESTADOCONT={0}"
                                                                    + " WHERE bok.NUORDEN IN (SELECT ord.NUORDEN FROM BSNBPSQL.TMCT0ORD ord WHERE ord.NUREFORD IN ({1}))";

  /**
   * Actualiza el estado contable de los tmct0alo cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_ALO_CDESTADOCONT_BY_NUREFORD = "UPDATE BSNBPSQL.TMCT0ALO alo SET alo.CDESTADOCONT={0}"
                                                                    + " WHERE alo.NUORDEN IN (SELECT ord.NUORDEN FROM BSNBPSQL.TMCT0ORD ord WHERE ord.NUREFORD IN ({1}))";

  /**
   * Actualiza el estado contable de los tmct0alc cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_ALC_CDESTADOCONT_BY_NUREFORD = "UPDATE BSNBPSQL.TMCT0ALC alc SET alc.CDESTADOCONT={0}"
                                                                    + " WHERE alc.NUORDEN IN (SELECT ord.NUORDEN FROM BSNBPSQL.TMCT0ORD ord WHERE ord.NUREFORD IN ({1}))";

  /**
   * Actualiza el estado contable de los tmct0bok cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_BOK_CDESTADOCONT_BY_NUREFORD_CTA_123 = "UPDATE BSNBPSQL.TMCT0BOK bok SET bok.CDESTADOCONT={0}"
                                                                            + " WHERE (bok.NUORDEN, bok.NBOOKING) IN (SELECT alo.NUORDEN, ALO.NBOOKING FROM BSNBPSQL.TMCT0ALO alo WHERE alo.NUCNFCLT IN ({1}))";

  /**
   * Actualiza el estado contable de los tmct0alo cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_ALO_CDESTADOCONT_BY_NUREFORD_CTA_123 = "UPDATE BSNBPSQL.TMCT0ALO alo SET alo.CDESTADOCONT={0}"
                                                                            + " WHERE alo.NUCNFCLT IN ({1})";

  /**
   * Actualiza el estado contable de los tmct0alc cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_ALC_CDESTADOCONT_BY_NUREFORD_CTA_123 = "UPDATE BSNBPSQL.TMCT0ALC alc SET alc.CDESTADOCONT={0}"
                                                                            + " WHERE alc.NUCNFCLT IN ({1})";

  /**
   * Actualiza el estado contable de los tmct0bok cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_BOK_CDESTADOCONT_BY_CLAVE = "UPDATE BSNBPSQL.TMCT0BOK bok SET bok.CDESTADOCONT={0}"
                                                                 + " WHERE bok.NUORDEN = :nuorden AND bok.nbooking = :nbooking";

  /**
   * Actualiza el estado contable de los tmct0alo cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_ALO_CDESTADOCONT_BY_CLAVE = "UPDATE BSNBPSQL.TMCT0ALO alo SET alo.CDESTADOCONT={0}"
                                                                 + " WHERE alo.NUORDEN = :nuorden AND alo.nbooking = :nbooking AND alo.nucnfclt = :nucnfclt";

  /**
   * Actualiza el estado contable de los tmct0alc cuyas ordenes tengan los números de referencias especificados.
   */
  private static final String UPDATE_ALC_CDESTADOCONT_BY_CLAVE = "UPDATE BSNBPSQL.TMCT0ALC alc SET alc.CDESTADOCONT={0}"
                                                                 + " WHERE alc.NUORDEN = :nuorden AND alc.nbooking = :nbooking AND alc.nucnfclt = :nucnfclt AND alc.nucnfliq = :nucnfliq";

  private static final String queryUpdate = "update tmct0alc set cdestadocont=:estadocont where nbooking=:nbooking "
                                            + "and nuorden=:nuorden and nucnfliq=:nucnfliq and nucnfclt=:nucnfclt";


  private static final String QUERY_CANONBME = "WITH MovimientoECC2 (nuorden, nbooking, nucnfclt, nucnfliq, iddesglosecamara, cartera) AS ("
                                               + " SELECT alc2.nuorden, alc2.nbooking, alc2.nucnfclt, alc2.nucnfliq, dcm2.iddesglosecamara,"
                                               + "        CASE WHEN mec2.CUENTA_COMPENSACION_DESTINO IN (:cuentaPropiaList) THEN 1 ELSE 0 END cartera"
                                               + " FROM bsnbpsql.tmct0alc alc2, bsnbpsql.tmct0desglosecamara dcm2, bsnbpsql.tmct0movimientoecc mec2"
                                               + " WHERE alc2.cdestadoasig >= :estadoAsignacion"
                                               + "       AND alc2.nuorden = dcm2.nuorden AND alc2.nbooking = dcm2.nbooking AND alc2.nucnfclt = dcm2.nucnfclt AND alc2.nucnfliq = dcm2.nucnfliq"
                                               + "       AND dcm2.cdestadoasig >= :estadoAsignacion AND dcm2.iddesglosecamara = mec2.iddesglosecamara AND mec2.cdestadomov = :cdestadomov"
                                               + " GROUP BY alc2.nuorden, alc2.nbooking, alc2.nucnfclt, alc2.nucnfliq, dcm2.iddesglosecamara, mec2.CUENTA_COMPENSACION_DESTINO)"
                                               + " SELECT mec.cartera,"
                                               + "        ali.CDALIASS cdaliass,"
                                               + "        sum(dct.IMCANONCONTREU) canon,"
                                               + "        dct.CDMIEMBROMKT,"
                                               + "        alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ,"
                                               + "        ali.AUXILIARCARTERAS auxiliarcarteras"
                                               + " FROM BSNBPSQL.TMCT0ALC alc"
                                               + "      INNER JOIN BSNBPSQL.TMCT0BOK bok ON alc.NUORDEN = bok.NUORDEN AND alc.NBOOKING = bok.NBOOKING"
                                               + "      INNER JOIN BSNBPSQL.TMCT0ALI ali ON ali.cdaliass = bok.cdalias AND ali.fhfinal = :fhFinalAliasActivo"
                                               + "      INNER JOIN BSNBPSQL.TMCT0DESGLOSECAMARA dcm ON dcm.NUORDEN = alc.NUORDEN AND dcm.NBOOKING = alc.NBOOKING AND dcm.NUCNFCLT = alc.NUCNFCLT AND dcm.NUCNFLIQ = alc.NUCNFLIQ AND dcm.CDESTADOASIG >= :estadoAsignacion"
                                               + "      INNER JOIN BSNBPSQL.TMCT0DESGLOSECLITIT dct ON dct.IDDESGLOSECAMARA = dcm.IDDESGLOSECAMARA"
                                               + "      INNER JOIN MovimientoECC2 mec ON mec.NUORDEN = mec.NUORDEN AND mec.NBOOKING = mec.NBOOKING AND mec.NUCNFCLT = mec.NUCNFCLT AND mec.NUCNFLIQ = alc.NUCNFLIQ AND mec.iddesglosecamara = dcm.iddesglosecamara"
                                               + " WHERE alc.CDESTADOASIG >= :estadoAsignacion"
                                               + "       AND alc.FEEJELIQ >= :fechaInicial AND alc.FEEJELIQ <= :fechaFinal AND mec.cartera in (0, 1)"
                                               + " GROUP BY mec.cartera, ali.cdaliass, ali.auxiliarcarteras, dct.cdmiembromkt, alc.NUORDEN, alc.NBOOKING, alc.NUCNFCLT, alc.NUCNFLIQ";

  private static final String sumCuentaTipo = "SELECT sum(a.importe) FROM TMCT0_APUNTE_CONTABLE_ALC a,"
                                              + "TMCT0_APUNTE_CONTABLE b,TMCT0_CUENTA_CONTABLE c WHERE a.apunte=b.id and b.cuenta=c.id and c.cuenta=:cuenta "
                                              + "and b.TIPO_COMPROBANTE=:tipo_comprobante and a.NUORDEN=:nuorden and a.NBOOKING=:nbooking "
                                              + "and a.NUCNFCLT=:nucnfclt and a.NUCNFLIQ=:nucnfliq";

  @Autowired
  private Tmct0alcDao tmct0alcDao;
  @Autowired
  private Tmct0aloDaoImp aloDao;
  @Autowired
  private Tmct0bokDaoImp bokDao;
  @Autowired
  private Tmct0desgloseCamaraDaoImpl desgloseDao;
  @Autowired
  private AlcEstadoContDaoImp alcEstadoContDao;
  @PersistenceContext
  private EntityManager em;

  static {
    for (ASIGNACIONES value : ASIGNACIONES.values()) {
      if (value.getId() != ASIGNACIONES.RECHAZADA.getId()) {
        if (value.getId() <= ASIGNACIONES.PDTE_LIQUIDAR.getId()) {
          SIN_LIQUIDAR.add(value.getId());
        } else {
          LIQUIDADO_MERCADO.add(value.getId());
        }
        if (value.getId() < ASIGNACIONES.LIQUIDADA_TEORICA.getId()) {
          IMPUTADA.add(value.getId());
        }
      }
    }
    SIN_DEVENGAR.add(300);
    SIN_DEVENGAR.add(301);
    SIN_DEVENGAR.add(302);
    SIN_DEVENGAR.add(303);
    SIN_DEVENGAR.add(310);
    SIN_DEVENGAR.add(312);
    DEVENGADO.add(315);
    DEVENGADO.add(316);
    DEVENGADO.add(320);
    FACTURADO_SIN_COBRAR.add(325);
    COBRADO.add(330);
    L_TIPOLOGIA.put(70, Arrays.asList(70, 71));
    L_TIPOLOGIA.put(72, Arrays.asList(72, 73, 74));
    L_TIPOLOGIA.put(75, Arrays.asList(75, 76, 79));
  }

  /**
   * Establece los parametros en la query especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>List<Object[]> - </code>Lista de resultados devueltos por la query.
   * @author XI316153
   */
  @SuppressWarnings("unchecked")
@Transactional
  public List<Object[]> prepareAndExecuteNativeQuery(String sqlQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.getResultList();
  } // prepareAndExecuteQuery

  /**
   * Establece los parametros en la query de actualización especificada y la ejecuta.
   * 
   * @param sqlQuery Query a preparar y ejecutar.
   * @param params Parametros a establecer en la query.
   * @return <code>Integer - </code>Número de registros actualizados.
   * @author XI316153
   */
  @Modifying
  @Transactional
  public Integer prepareAndExecuteNativeUpdate(String sqlUpdateQuery, Map<String, Object> params) {
    Query query = em.createNativeQuery(sqlUpdateQuery);
    for (Map.Entry<String, Object> entry : params.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    return query.executeUpdate();
  } // prepareAndExecuteQuery

  @SuppressWarnings("unchecked")
  public List<Object[]> findInformeContable(Map<String, String> filters) throws ParseException {
    Query query;
    List<Integer> list;
    String argEstadoAsig = "";
    String argConta1 = "";
    String argConta2 = "";
    String argAlias = "";
    String argApunte = "";
    String argEstadoCont = "";
    String argEstadoEntrec = "";
    String argTipoOrden = "";
    String argTipologia = "";
    String estadoLiquidacion = "";
    List<Integer> lEstadoAsig = new ArrayList<Integer>();
    Map<String, Object> parameters = new HashMap<String, Object>();
    boolean prefijadaEPLO = "EPLO".equals(filters.get("prefijadas"));
    String argFechTradeIni = putDate(parameters, filters, "fechTradeIni", " and alc.feejeliq>=:");
    String argFechTradeFin = putDate(parameters, filters, "fechTradeFin", " and alc.feejeliq<=:");
    String argSettlementIni = putDate(parameters, filters, "settlementIni", " and alc.feopeliq>=:");
    String argSettlementFin = putDate(parameters, filters, "settlementFin", " and alc.feopeliq<=:");
    if (prefijadaEPLO) {
      lEstadoAsig.addAll(IMPUTADA);
    } else {
      String value;
      List<Integer> lEstadoCont = new ArrayList<Integer>();
      String vTipoOrden = filters.get("tipoOrden");
      boolean tipoOrden = !"ALL".equals(vTipoOrden);
      String vTipologia = filters.get("tipologia");
      boolean tipologia = !vTipologia.isEmpty();
      String vCuenta = filters.get("cuenta");
      boolean cuenta = !vCuenta.isEmpty();
      argTipologia = tipoOrden ? queryTipoOrden : "";
      if (tipologia || cuenta) {
        String argument1_0 = "";
        String argument1_1 = "";
        if (tipologia) {
          argument1_0 = " and tipo_comprobante in (:tipologia)";
          list = new ArrayList<Integer>();
          for (int tipo : getListInt(vTipologia)) {
            list.addAll(L_TIPOLOGIA.get(tipo));
          }
          parameters.put("tipologia", list);
        }
        if (cuenta) {
          argument1_1 = " and apt.cuenta in (:cuenta)";
          parameters.put("cuenta", getListInt(vCuenta));
        }
        argConta1 = MessageFormat.format(queryContabilizado1, argument1_0, argument1_1);
        argConta2 = queryContabilizado2;
      } else {
        argConta1 = querySinContabilizar;
      }
      if (!(value = filters.get("alias")).isEmpty()) {
        List<String> lAlias = new ArrayList<String>();
        String[] split = filters.get("alias").split(",\\s*");
        for (int i = 0; i < split.length; i++) {
          lAlias.add(split[i]);
        }
        parameters.put("alias", lAlias);
        argAlias = " and bok.cdalias in (:alias)";
      }
      if (!(value = filters.get("tipoApunte")).isEmpty()) {
        if ("C".equals(value)) {
          argApunte = " and alo.cdtpoper='C'";
        } else if ("E".equals(value)) {
          argApunte = " and alo.cdtpoper='C' and alc.cdestadoentrec is not null";
        } else if ("V".equals(value)) {
          argApunte = " and alo.cdtpoper='V'";
        } else if ("R".equals(value)) {
          argApunte = " and alo.cdtpoper='V' and alc.cdestadoentrec is not null";
        }
      }
      for (String string : filters.get("estadoContable").split(",")) {
        if ("SD".equals(string)) {
          lEstadoCont.addAll(SIN_DEVENGAR);
        } else if ("DV".equals(string)) {
          lEstadoCont.addAll(DEVENGADO);
        } else if ("FC".equals(string)) {
          lEstadoCont.addAll(FACTURADO_SIN_COBRAR);
        } else if ("CB".equals(string)) {
          lEstadoCont.addAll(COBRADO);
        }
      }
      if (!lEstadoCont.isEmpty()) {
        parameters.put("lEstadoCont", lEstadoCont);
        argEstadoCont = " and alc.cdestadocont in (:lEstadoCont)";
      }
      for (String string : filters.get("estadoLiquidacion").split(",")) {
        if ("SLQ".equals(string)) {
          estadoLiquidacion = " and alc.cdestadocont<>330 and des.desgloses=0";
        } else if ("MRC".equals(string)) {
          estadoLiquidacion = " and alc.cdestadocont<>330 and des.desgloses>0";
        } else if ("CLT".equals(string)) {
          estadoLiquidacion = " and alc.cdestadocont=330 and des.desgloses=0";
        } else if ("CMP".equals(string)) {
          estadoLiquidacion = " and alc.cdestadocont=330 and des.desgloses>0";
        }
      }
      if (tipoOrden) {
        argTipoOrden = " and afi.nucnfclt is " + ("TER".equals(vTipoOrden) ? "null" : "not null");
      }
    }
    if (!lEstadoAsig.isEmpty()) {
      parameters.put("lEstadoAsig", lEstadoAsig);
      argEstadoAsig = " and alc.cdestadoasig in (:lEstadoAsig)";
    }
    String qs = prefijadaEPLO ? MessageFormat.format(queryFindInformeEPLO, argFechTradeIni, argFechTradeFin,
                                                     argSettlementIni, argSettlementFin, argEstadoAsig)
                             : MessageFormat.format(queryFindInforme, argTipologia, argConta1, argConta2,
                                                    argFechTradeIni, argFechTradeFin, argSettlementIni,
                                                    argSettlementFin, argAlias, argApunte, argEstadoCont,
                                                    argEstadoAsig, argEstadoEntrec, argTipoOrden, estadoLiquidacion);
    query = em.createNativeQuery(qs);
    for (Entry<String, Object> entry : parameters.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }
    List<Object[]> resultList = new LinkedList<Object[]>();
    try {
      resultList.addAll(query.getResultList());
    } catch (PersistenceException ex) {
      LOG.error(ex.getMessage() + " cause: " + ex.getCause(), ex);
    }
    return resultList;
  }

  private static String putDate(Map<String, Object> parameters,
                                Map<String, String> filters,
                                String key,
                                String condicion) throws ParseException {
    String salida = "";
    String value = filters.get(key);
    if (!value.isEmpty()) {
      parameters.put(key, dF1.parse(value));
      salida = condicion + key;
    }
    return salida;
  }

  private static List<Integer> getListInt(String values) {
    List<Integer> list = new ArrayList<Integer>();
    for (String value : values.split(",")) {
      list.add(Integer.valueOf(value));
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  public List<InformeOpercacionData> getInformeOperacionData(String cdalias) {
    List<InformeOpercacionData> resultList = new LinkedList<InformeOpercacionData>();
    // estados
    final int PDTE_CASE = EstadosEnumerados.CLEARING.PDTE_CASE.getId();
    final int CASADA_PDTE_LIQUIDACION = EstadosEnumerados.CLEARING.CASADA_PDTE_LIQUIDACION.getId();
    final int LIQUIDACION_FALLIDA = EstadosEnumerados.CLEARING.LIQUIDACION_FALLIDA.getId();
    // final int PDTE_LIQUIDACION_PARCIAL = EstadosEnumerados.CLEARING.PDTE_LIQUIDACION_PARCIAL.getId();
    // final int PDTE_LIQUIDACION_CLIENTE = EstadosEnumerados.CLEARING.PDTE_LIQUIDACION_CLIENTE.getId();
    final int LIQUIDADA_PARCIAL = EstadosEnumerados.CLEARING.LIQUIDADA_PARCIAL.getId();
    final int LIQUIDADA = EstadosEnumerados.CLEARING.LIQUIDADA.getId();
    final int LIQUIDADA_PROPIA = EstadosEnumerados.CLEARING.LIQUIDADA_PROPIA.getId();
    String strIdEstado = "alc.estadoentrec.idestado = ";
    StringBuilder estadosCriteria = new StringBuilder();
    estadosCriteria.append("(").append(strIdEstado).append(PDTE_CASE).append(" OR ");
    estadosCriteria.append(strIdEstado).append(CASADA_PDTE_LIQUIDACION).append(" OR ");
    estadosCriteria.append(strIdEstado).append(LIQUIDACION_FALLIDA).append(" OR ");
    // estadosCriteria.append(strIdEstado).append(PDTE_LIQUIDACION_PARCIAL).append(" OR ");
    // estadosCriteria.append(strIdEstado).append(PDTE_LIQUIDACION_CLIENTE).append(" OR ");
    estadosCriteria.append(strIdEstado).append(LIQUIDADA_PARCIAL).append(" OR ");
    estadosCriteria.append(strIdEstado).append(LIQUIDADA).append(" OR ");
    estadosCriteria.append(strIdEstado).append(LIQUIDADA_PROPIA).append(") ");
    //
    StringBuilder selectDserror = new StringBuilder("SELECT err FROM S3Errores err WHERE ").append("err.s3liquidacion.nbooking = :nbooking AND ")
                                                                                           .append("err.s3liquidacion.nucnfclt = :nucnfclt AND ")
                                                                                           .append("err.s3liquidacion.nucnfliq = :nucnfliq ")
                                                                                           .append("ORDER BY err.auditDate DESC");
    //
    StringBuilder select = new StringBuilder("SELECT new sibbac.business.wrappers.database.data.InformeOpercacionData(").append("alc.feejeliq, ")
                                                                                                                        .append("alc.feopeliq, ")
                                                                                                                        .append("alc.id, ")
                                                                                                                        .append("alc.tmct0alo.cdtpoper, ")
                                                                                                                        .append("alc.tmct0alo.cdisin, ")
                                                                                                                        .append("alc.tmct0alo.nbvalors, ")
                                                                                                                        /*
                                                                                                                         * MERCADO
                                                                                                                         * FALTA
                                                                                                                         */
                                                                                                                        .append("alc.nutitliq, ")
                                                                                                                        .append("alc.nbtitliq, ")
                                                                                                                        .append("alc.tmct0alo.imcamnet, ")
                                                                                                                        .append("alc.imcomisn, ")
                                                                                                                        .append("alc.estadoentrec.nombre) ");
    StringBuilder from = new StringBuilder("FROM Tmct0alc alc ");
    StringBuilder where = new StringBuilder("WHERE ").append(estadosCriteria).append("AND ")
                                                     .append("alc.tmct0alo.tmct0bok.cdalias =:cdalias ");
    try {
      Query query = em.createQuery(select.append(from).append(where).toString());
      query.setParameter("cdalias", cdalias);

      resultList.addAll(query.getResultList());
    } catch (PersistenceException ex) {
      LOG.error(ex.getMessage(), ex);
    }
    List<S3Errores> errList = null;
    // terminar de rellenar los datos de error
    for (InformeOpercacionData info : resultList) {
      try {
        Query queryS3 = em.createQuery(selectDserror.toString());
        queryS3.setParameter("nbooking", info.getNuestraReferencia().getNbooking());
        queryS3.setParameter("nucnfclt", info.getNuestraReferencia().getNucnfclt());
        queryS3.setParameter("nucnfliq", info.getNuestraReferencia().getNucnfliq());
        errList = (queryS3.getResultList());
        S3Errores err = (errList != null && errList.size() > 0) ? errList.get(0) : null;
        if (err != null) {
          info.prepareErrorData(err);
        }
      } catch (PersistenceException ex) {
        LOG.error(ex.getMessage(), ex);
      }
    }

    return resultList;
  }

  private Date getOneYearOld() {
    Calendar cal = new GregorianCalendar();
    cal.add(Calendar.YEAR, -1);
    return cal.getTime();
  }

  public Query getQuery(TIPO_COMPROBANTE tipoComprobante,
                        String cdestliq,
                        List<Integer> lEstadosAsig,
                        List<Integer> lEstadocont,
                        String keyvalue,
                        List<String> cdauxili,
                        Date fCierre,
                        Integer numrows) {
    Query query = null;
    switch (tipoComprobante) {
      case DEVENGO_ROUTING:
        query = em.createNativeQuery(numrows == null ? MessageFormat.format(queryFindRouting, count)
                                                    : (MessageFormat.format(queryFindRouting, fieldsRouting)
                                                       + orderRouting + MessageFormat.format(FETCH, numrows)));
        break;
      case DEVENGO_CLEARING:
        query = em.createNativeQuery(numrows == null ? MessageFormat.format(queryFindClearing, count)
                                                    : (MessageFormat.format(queryFindClearing, fieldsClearing)
                                                       + orderClearing + MessageFormat.format(FETCH, numrows)));
        break;
      case DEVENGO_CORRETAJE:
        query = em.createNativeQuery(numrows == null ? MessageFormat.format(queryFindCorretaje, count)
                                                    : (MessageFormat.format(queryFindCorretaje, fieldsCorretaje)
                                                       + orderCorretaje + MessageFormat.format(FETCH, numrows)));
        break;
      case DEVENGO_FACTURACION:
        query = em.createNativeQuery(numrows == null ? MessageFormat.format(queryFindFacturacion, count)
                                                    : (MessageFormat.format(queryFindFacturacion, fieldsFacturacion)
                                                       + orderFacturacion + MessageFormat.format(FETCH, numrows)));
        break;
      default:
          return null;
    }
    query.setParameter("oneYearOld", getOneYearOld());
    query.setParameter("cdestliq", cdestliq);
    query.setParameter("lEstadosAsig", lEstadosAsig);
    query.setParameter("lEstadocont", lEstadocont);
    query.setParameter("keyvalue", keyvalue);
    query.setParameter("cdauxili", cdauxili);
    return query;
  }

  public int contAlcParaDevengar(TIPO_COMPROBANTE tipoComprobante,
                                 String cdestliq,
                                 List<Integer> lEstadosAsig,
                                 List<Integer> lEstadocont,
                                 String keyvalue,
                                 List<String> cdauxili,
                                 Date fCierre) {
    return (int) getQuery(tipoComprobante, cdestliq, lEstadosAsig, lEstadocont, keyvalue, cdauxili, fCierre, null).getSingleResult();
  }

  @SuppressWarnings("unchecked")
  public List<Tmct0alc> findAlcParaDevengar(TIPO_COMPROBANTE tipoComprobante,
                                            String cdestliq,
                                            List<Integer> lEstadosAsig,
                                            List<Integer> lEstadocont,
                                            String keyvalue,
                                            List<String> cdauxili,
                                            Date fCierre,
                                            Integer numrows) {
    Tmct0alc alc;
    Tmct0alo alo;
    Tmct0bok bok;
    BigInteger bI;
    List<Tmct0alc> list = new ArrayList<Tmct0alc>();
    for (Object[] bean : (List<Object[]>) getQuery(tipoComprobante, cdestliq, lEstadosAsig, lEstadocont, keyvalue,
                                                   cdauxili, fCierre, numrows).getResultList()) {
      list.add(alc = new Tmct0alc(new Tmct0alcId(((String) bean[2]).trim(), ((String) bean[1]).trim(),
                                                 ((BigDecimal) bean[5]).shortValue(), ((String) bean[3]).trim())));
      alc.setFeejeliq((Date) bean[0]);
      alc.setTmct0alo(alo = new Tmct0alo());
      alo.setCdalias(((String) bean[4]).trim());
      alc.setFeopeliq((Date) bean[6]);
      alc.setEstadocont(new Tmct0estado((Integer) bean[7]));
      alc.setNbtitliq(((String) bean[8]).trim());
      alc.setAuxiliar((bI = (BigInteger) bean[9]) == null ? null : bI.longValue());
      alc.setImcomdvo((BigDecimal) bean[10]);
      alc.setImcombrk((BigDecimal) bean[11]);
      alo.setTmct0bok(bok = new Tmct0bok());
      bok.setGrupo(bean[12] == null ? Boolean.FALSE : 1 == (short) bean[12]);
      switch (tipoComprobante) {
        case DEVENGO_ROUTING:
          alc.setImajusvb((BigDecimal) bean[13]);
          alc.setImcomisn((BigDecimal) bean[14]);
          alc.setImcanoncompeu((BigDecimal) bean[15]);
          alc.setImcanoncontreu((BigDecimal) bean[16]);
          alc.setImcanonliqeu((BigDecimal) bean[17]);
          alo.setCanoncontrpagoclte((int) bean[18]);
          break;
        case DEVENGO_CLEARING:
        case DEVENGO_CORRETAJE:
        case DEVENGO_FACTURACION:
          alc.setImfinsvb((BigDecimal) bean[13]);
          break;
        default:
      }
    }
    return list;
  }

  @SuppressWarnings("unchecked")
  public List<Object[]> findAlcsDevengadosRechazados(Integer transactionSize) {
    LOG.debug("[findAlcsDevengadosRechazados] Inicio ...");
    return (List<Object[]>) em.createNativeQuery(QUERY_RECHAZOS_ALCS + MessageFormat.format(FETCH, transactionSize))
                              .getResultList();
  } // findAlcsDevengadosRechazados

  /**
   * Ejecuta la query de consulta de apuntes contables de devengo a retorceder.
   * 
   * @param transactionSize Número de registros a devolver.
   * @return <code>List<Map<String, Object>> - </code>Lista de apuntes contables a retroceder.
   */
  @SuppressWarnings("unchecked")
  public List<RechazoApunteDto> findRechazos(String nuorden, String nbooking, String nucnfclt, Short nucnfliq) {
    LOG.debug("[findRechazos] Inicio ...");
    List<RechazoApunteDto> rechazosList = new ArrayList<RechazoApunteDto>();
    // RechazoApunteDto rechazoApunteDto = null;
    String query = MessageFormat.format(QUERY_RECHAZOS_APUNTES, FIELDS_RECHAZOS_APUNTES, nuorden, nbooking, nucnfclt,
                                        nucnfliq + "");
    LOG.debug("[findRechazos] Query: {}", query);
    List<Object[]> resultQuery = (List<Object[]>) em.createNativeQuery(query).getResultList();
    for (Object[] rechazo : resultQuery) {
      // rechazoApunteDto = new RechazoApunteDto(rechazo);
      // rechazosList.add(rechazoApunteDto);
      // rechazosList.add(new RechazoApunteDto(rechazo));
      rechazosList.add(new RechazoApunteDto(nuorden, nbooking, nucnfclt, nucnfliq, rechazo));
    } // for

    LOG.debug("[findRechazos] Fin ...");
    return rechazosList;
  } // findRechazos

  @Modifying
  @Transactional
  public int updateCdestadocont(int estadocont, String nbooking, String nuorden, Short nucnfliq, String nucnfclt) throws SIBBACBusinessException {
    LOG.debug("Updatando el ALC {}/{}/{}/{} a cdestadocont={}", nuorden, nbooking, nucnfclt, nucnfliq, estadocont);
    int number = 0;
    Query query = em.createNativeQuery(queryUpdate);
    query.setParameter("estadocont", estadocont);
    query.setParameter("nbooking", nbooking);
    query.setParameter("nuorden", nuorden);
    query.setParameter("nucnfliq", nucnfliq);
    query.setParameter("nucnfclt", nucnfclt);
    if ((number = query.executeUpdate()) == 0) {
      LOG.error("No modifica el cdestadocont de ningun ALC {}/{}/{}/{} a cdestadocont={}", nuorden, nbooking, nucnfclt,
                nucnfliq, estadocont);
      throw new SIBBACBusinessException("No modifica el cdestadocont de ningun ALC " + nuorden + "/" + nbooking + "/"
                                        + nucnfclt + "/" + nucnfliq);
    }
    alcEstadoContDao.updateOrInsertCdestadocont(estadocont, nbooking, nuorden, nucnfliq, nucnfclt, 10);
    LOG.debug("Updatado el ALC {}/{}/{}/{} a cdestadocont={}", nuorden, nbooking, nucnfclt, nucnfliq, estadocont);
    return number;
  }

  @Transactional
  public void updateCdestadocont(int estadocont, Tmct0alcId alcId) throws SIBBACBusinessException {
    updateCdestadocont(estadocont, alcId.getNbooking(), alcId.getNuorden(), alcId.getNucnfliq(), alcId.getNucnfclt());
    LOG.debug("El ALC {}/{}/{}/{} se ha puesto a cdestadocont={}", alcId.getNuorden(), alcId.getNbooking(),
              alcId.getNucnfclt(), alcId.getNucnfliq(), estadocont);
  }

  @Modifying
  @Transactional
  private void escalaEstadoAlc(Tmct0alc alc, int estadoCont) throws SIBBACBusinessException {
    LOG.debug("Escalando el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
    aloDao.updateCdestadocont(CONTABILIDAD.EN_CURSO.getId(), alc.getNucnfclt(), alc.getNbooking(), alc.getNuorden());
    bokDao.updateCdestadocont(CONTABILIDAD.EN_CURSO.getId(), alc.getNbooking(), alc.getNuorden());
    updateCdestadocont(estadoCont, alc.getNbooking(), alc.getNuorden(), alc.getNucnfliq(), alc.getNucnfclt());
    LOG.debug("Escalado el ALC {}/{}/{}/{}", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(), alc.getNucnfliq());
  }

  @Transactional
  public void escalaAlcDesglose(Tmct0alc alc, int estadoCont, TIPO_ESTADO tipoEstado) throws SIBBACBusinessException {
    LOG.debug("Escalando el ALC {}/{}/{}/{} y desgloses", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(),
              alc.getNucnfliq());
    escalaEstadoAlc(alc, estadoCont);
    if (TIPO_ESTADO.CORRETAJE_O_CANON == tipoEstado) {
      for (Tmct0desglosecamara desglose : alc.getTmct0desglosecamaras()) {
        desgloseDao.updateCdestadocont(estadoCont, desglose.getIddesglosecamara());
      }
    }
    LOG.debug("Escalado el ALC {}/{}/{}/{} y desgloses", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(),
              alc.getNucnfliq());
  }

  public void escalaEstadoAlc(Tmct0alc alc,
                              Tmct0estado estadocont,
                              Tmct0estado estadoEnCurso,
                              TIPO_ESTADO... tiposEstado) throws SIBBACBusinessException {
    LOG.debug("Escalando el ALC {}/{}/{}/{} sin desgloses", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(),
              alc.getNucnfliq());
    alc.setEstadocont(estadocont);
    alc.getTmct0alo().setTmct0estadoByCdestadocont(estadoEnCurso);
    alc.getTmct0alo().getTmct0bok().setTmct0estadoByCdestadocont(estadoEnCurso);
    for (TIPO_ESTADO tipoEstado : tiposEstado) {
      alc.getEstadosContables().add(new AlcEstadoCont(alc, tipoEstado));
    }
    LOG.debug("Escalado el ALC {}/{}/{}/{} sin desgloses", alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(),
              alc.getNucnfliq());
  }

  @Transactional
  public void setEstadocont(Tmct0alc alc, Tmct0estado estadoCont, TIPO_ESTADO tipoEstado) throws SIBBACBusinessException {
    escalaAlcDesglose(alc, estadoCont.getIdestado(), tipoEstado);
  }

  @SuppressWarnings("unchecked")
  public List<AlcEntregaRecepcionClienteDTO> findByEntregaRecepcionTodos(Date fcontratacionDe,
                                                                         Date fcontratacionA,
                                                                         Date fliquidacionDe,
                                                                         Date fliquidacionA,
                                                                         String estado,
                                                                         String nbooking,
                                                                         String cdniftit,
                                                                         String nbtitliq,
                                                                         String biccle,
                                                                         BigDecimal importeNetoDesde,
                                                                         BigDecimal importeNetoHasta,
                                                                         BigDecimal titulosDesde,
                                                                         BigDecimal titulosHasta,
                                                                         Integer estadoAsig,
                                                                         String isin,
                                                                         String alias,
                                                                         String referencias3,
                                                                         String cif_svb,
                                                                         String sentidoBusqueda,
                                                                         Integer tipo,
                                                                         String estadoInstruccion,
                                                                         String cuenta) throws SIBBACBusinessException{
    final List<AlcEntregaRecepcionClienteDTO> lista_alc = new ArrayList<>();
    final boolean alta;
    
    alta = estadoInstruccion != null ? estadoInstruccion.equals("A") : true;
    try {
      LOG.debug("[Tmct0alcDaoImp::findByEntregaRecepcionTodos] Entra en el metodo");
      //Pata cliente
      final StringBuilder select = new StringBuilder();
      final StringBuilder from = new StringBuilder();
      final StringBuilder where = new StringBuilder();
      //Pata mercado
      final StringBuilder selectUnion = new StringBuilder();
      final StringBuilder fromUnion = new StringBuilder();
      final StringBuilder whereUnion = new StringBuilder();
      final StringBuilder having = new StringBuilder();
      Integer pendEnviar = CLEARING.PDTE_FICHERO.getId();

      if (tipo == 1 || tipo == 3) {
        select.append("SELECT 'C', alc.nuorden, alc.nbooking, alc.nucnfclt, alc.nucnfliq, est.nombre, alc.nutitliq titulos, "); // 6
        select.append("bok.cdtpoper, bok.cdalias, bok.cdisin, bok.nbvalors, alc.imefeagr efectivo,alc.imcomisn, alc.imnfiliq, "); // 13
        select.append("alc.imcanoncompeu, alc.imcanoncontreu, alc.imcanonliqeu, alc.cdniftit, alc.nbtitliq, "); // 18
        select.append("alc.cdcustod, alc.cdrefban, alc.ourpar, alc.theirpar, alc.cdestadoentrec, bok.descrali, "); // 24
        select.append("ord.referencias3, alc.feejeliq, 0 as desglosecamara, est2.nombre as estdesglose, "); // 28
        select.append("alc.feopeliq, ord.cuenta_compensacion_destino as cuentaCompensacionDestino, ord.estado as estadoInstruccion, ");
        select.append("ord.id alc_orden_id, null alc_orden_mercado_id ");
        from.append("FROM Tmct0alc alc ");
        from.append("inner join tmct0estado est on alc.cdestadoentrec = est.idestado ");
        from.append("inner join tmct0estado est2 ON alc.cdestadoasig = est2.idestado ");
        from.append("inner join tmct0bok bok on alc.nbooking = bok.nbooking ");
        from.append("inner join tmct0desglosecamara dc on alc.nuorden = dc.nuorden "); 
        from.append("and alc.nbooking = dc.nbooking and alc.nucnfclt=dc.nucnfclt and alc.nucnfliq = dc.nucnfliq "); 
        from.append("and dc.cdestadoasig >= CASE :estadoInstruccion WHEN 'B' THEN 0 ELSE 1 END ");
        if(alta)
            from.append("left ");
        else
            from.append("inner ");
        from.append("join tmct0_alc_orden ord on alc.nbooking = ord.nbooking and alc.nuorden = ord.nuorden and alc.nucnfclt = ord.nucnfclt and alc.nucnfliq = ord.nucnfliq ");
        from.append("AND ord.estado = :estadoInstruccion ");
        where.append("WHERE alc.feejeliq = bok.fetrade ");
        where.append("AND ( alc.cdestadoentrec = case :estadoInstruccion when 'B' then 402 else -1 end or alc.cdestadoentrec<>case :estadoInstruccion when 'A' then 402 else -1 end ) ");
        where.append("AND (EXISTS (select 1 from Tmct0movimientoecc mec ");
        where.append("inner join Tmct0_Cuentas_De_Compensacion cc on cc.CD_CODIGO = mec.CUENTA_COMPENSACION_DESTINO and cc.cuenta_clearing = 'S' ");
        where.append("where mec.IDDESGLOSECAMARA=dc.iddesglosecamara and mec.cdestadomov=9 ");
        if(cuenta != null && !cuenta.isEmpty())
            where.append("and mec.cuenta_compensacion_destino = :cuenta ");
        where.append(")) ");
      }
      if (tipo == 2 || tipo == 3) {
        if (tipo == 3) {
          selectUnion.append("Union ");
        }
        selectUnion.append("Select 'M', dc.nuorden, dc.nbooking, dc.nucnfclt, ");
        selectUnion.append("dc.nucnfliq, est.nombre, SUM(ct.imtitulos) titulos, bok.cdtpoper, bok.cdalias, bok.cdisin, ");
        selectUnion.append("bok.nbvalors,SUM(ct.imefectivo) efectivo,0 imcomisn,0 imnfiliq,SUM(ct.imcanoncompeu) imcanoncompeu, SUM(ct.imcanoncontreu) imcanoncontreu, SUM(ct.imcanonliqeu) imcanonliqeu,'' cdniftit,'' nbtitliq, '' cdcustod,");
        selectUnion.append("'' cdrefban,'' ourpar,'' theirpar, dc.cdestadoentrec,bok.descrali, ord.referencias3, dc.fecontratacion, ");
        selectUnion.append("dc.iddesglosecamara as desglosecamara, est2.nombre as estdesglose, dc.feliquidacion, ord.cuenta_compensacion_destino as cuentaCompensacionDestino, ord.estado as estadoInstruccion, ");
        selectUnion.append("ord.id alc_orden_id, mer.id alc_orden_mercado_id "); 
        fromUnion.append("FROM Tmct0desglosecamara dc ");
        fromUnion.append("INNER JOIN tmct0estado est on dc.cdestadoentrec = est.idestado ");
        fromUnion.append("INNER JOIN tmct0estado est2 ON dc.cdestadoasig = est2.idestado ");
        fromUnion.append("INNER JOIN Tmct0desgloseclitit ct ON dc.iddesglosecamara=ct.IDDESGLOSECAMARA ");
        fromUnion.append("INNER JOIN tmct0bok bok on dc.nbooking = bok.nbooking  ");
        if(alta)
            fromUnion.append("LEFT ");
        else
            fromUnion.append("INNER ");
        fromUnion.append("JOIN tmct0_alc_orden_mercado mer on dc.iddesglosecamara = mer.iddesglosecamara ");
        fromUnion.append("AND mer.estado = :estadoInstruccion ");
        if(alta)
            fromUnion.append("LEFT ");
        else
            fromUnion.append("INNER ");
        fromUnion.append("JOIN tmct0_alc_orden ord on ord.id=mer.IDALC_ORDEN ");
        fromUnion.append("AND ord.estado = :estadoInstruccion ");
        whereUnion.append("AND (dc.cdestadoentrec=case :estadoInstruccion when 'B' then 402 else -1 end or dc.cdestadoentrec<>case :estadoInstruccion when 'A' then 402 else -1 end) ");
        whereUnion.append("AND (EXISTS (select 1 from Tmct0movimientoecc mec ");
        whereUnion.append("INNER JOIN Tmct0_Cuentas_De_Compensacion cc on cc.CD_CODIGO = mec.CUENTA_COMPENSACION_DESTINO and cc.cuenta_clearing = 'S' ");
        whereUnion.append("where mec.IDDESGLOSECAMARA = dc.iddesglosecamara and mec.cdestadomov=9 ");
        if(cuenta != null && !cuenta.isEmpty())
            whereUnion.append("and mec.cuenta_compensacion_destino = :cuenta ");
        whereUnion.append(")) ");
        whereUnion.append("AND dc.FECONTRATACION=bok.fetrade ");
      }
      if (fcontratacionDe != null) {
        where.append("AND alc.feejeliq >= :fcontratacionDe ");
        whereUnion.append("AND dc.fecontratacion >= :fcontratacionDe  ");
      }

      if (fcontratacionA != null) {
        where.append("AND alc.feejeliq <= :fcontratacionA ");
        whereUnion.append("AND dc.fecontratacion <= :fcontratacionA ");
      }

      if (fliquidacionDe != null) {
        where.append("AND alc.feopeliq >= :fliquidacionDe ");
        whereUnion.append("AND dc.feliquidacion >= :fliquidacionDe ");
      }

      if (fliquidacionA != null) {
        where.append("AND alc.feopeliq <= :fliquidacionA ");
        whereUnion.append("AND dc.feliquidacion <= :fliquidacionA ");
      }

      Integer estadoInt = 0;
      if (estado != null && !estado.isEmpty()) {
        if (estado.indexOf("#") != -1) {
          estado = estado.replace("#", "");
          where.append("AND alc.cdestadoentrec <> :estadoInt ");
          whereUnion.append("AND dc.cdestadoentrec <> :estadoInt ");
        } else {
          where.append("AND alc.cdestadoentrec = :estadoInt ");
          whereUnion.append("AND dc.cdestadoentrec = :estadoInt ");
        }
        estadoInt = Integer.valueOf(estado);

      }

      if (nbooking != null && !nbooking.isEmpty()) {

        if (nbooking.indexOf("#") != -1) {
          nbooking = nbooking.replace("#", "");
          where.append("AND alc.nbooking <> :nbooking ");
          whereUnion.append("AND dc.nbooking <> :nbooking ");
        } else {
          where.append("AND alc.nbooking = :nbooking ");
          whereUnion.append("AND dc.nbooking = :nbooking ");
        }

      }

      if (cdniftit != null && !cdniftit.isEmpty()) {

        if (cdniftit.indexOf("#") != -1) {
          cdniftit = cdniftit.replace("#", "");
          where.append("AND alc.cdniftit <> :cdniftit ");
        } else {
          where.append("AND alc.cdniftit = :cdniftit ");
        }
        if (tipo == 2)
          cdniftit = null;
      }

      if (nbtitliq != null && !nbtitliq.isEmpty()) {
        if (nbtitliq.indexOf("#") != -1) {
          nbtitliq = nbtitliq.replace("#", "");
          nbtitliq = "%" + nbtitliq + "%";
          where.append("AND alc.nbtitliq not like :nbtitliq ");
        } else {
          where.append("AND alc.nbtitliq like :nbtitliq ");
        }
        if (tipo == 2)
          nbtitliq = null;

      }

      if (biccle != null && !biccle.isEmpty()) {
        if (biccle.indexOf("#") != -1) {
          biccle = biccle.replace("#", "");
          where.append("AND alc.cdcustod <> :biccle ");
        } else {
          where.append("AND alc.cdcustod = :biccle ");
        }
      }

      if (!BigDecimal.ZERO.equals(importeNetoDesde)) {
        where.append("AND alc.imefeagr >= :importeNetoDesde ");
        if (having.length() > 0) {
          having.append(" AND SUM(ct.imefectivo) >= :importeNetoDesde ");
        } else {
          having.append(" SUM(ct.imefectivo) >= :importeNetoDesde ");
        }
      }

      if (!BigDecimal.ZERO.equals(importeNetoHasta)) {
        where.append(" AND alc.imefeagr <= :importeNetoHasta");
        if (having.length() > 0) {
          having.append(" AND SUM(ct.imefectivo) <= :importeNetoHasta");
        } else {
          having.append(" SUM(ct.imefectivo) <= :importeNetoHasta");
        }
      }

      if (!BigDecimal.ZERO.equals(titulosDesde)) {
        where.append(" AND alc.nutitliq >= :titulosDesde");
        if (having.length() > 0) {
          having.append(" AND sum(dc.NUTITULOS) >= :titulosDesde");
        } else {
          having.append(" sum(dc.NUTITULOS) >= :titulosDesde");
        }
      }

      if (!BigDecimal.ZERO.equals(titulosHasta)) {
        where.append(" AND alc.nutitliq <= :titulosHasta");
        if (having.length() > 0) {
          having.append(" AND sum(dc.NUTITULOS) <= :titulosHasta");
        } else {
          having.append(" sum(dc.NUTITULOS) <= :titulosHasta");
        }
      }

      if (isin != null && !isin.isEmpty()) {
        if (isin.indexOf("#") != -1) {
          isin = isin.replace("#", "");
          where.append("AND bok.cdisin <> :isin ");
          whereUnion.append("AND bok.cdisin <> :isin ");
        } else {
          where.append("AND bok.cdisin = :isin ");
          whereUnion.append("AND bok.cdisin = :isin ");
        }
      }

      if (alias != null && !alias.isEmpty()) {
        if (alias.indexOf("#") != -1) {
          alias = alias.replace("#", "");
          where.append("AND bok.cdalias <> :alias ");
          whereUnion.append(" AND bok.cdalias <> :alias ");
        } else {
          where.append("AND bok.cdalias = :alias ");
          whereUnion.append(" AND bok.cdalias = :alias ");
        }
      }

      if (referencias3 != null && !referencias3.isEmpty()) {
        if (referencias3.indexOf("#") != -1) {
          referencias3 = referencias3.replace("#", "");
          where.append("AND ord.referencias3 <> :referencias3 ");
          whereUnion.append("AND ord.referencias3 <> :referencias3");
        } else {
          where.append("AND ord.referencias3 = :referencias3 ");
          whereUnion.append("AND ord.referencias3 = :referencias3 ");
        }
      }

      if (sentidoBusqueda != null && !sentidoBusqueda.isEmpty()) {

        if (sentidoBusqueda.length() == 1 && !(sentidoBusqueda.indexOf("#") != -1)) {
          where.append(" and bok.cdtpoper = :sentidoBusqueda ");
          whereUnion.append(" and bok.cdtpoper = :sentidoBusqueda ");
        } else {
          if (sentidoBusqueda.indexOf("#") != -1) {
            sentidoBusqueda = sentidoBusqueda.replace("#", "");
            if (sentidoBusqueda.length() == 1) {
              where.append(" and bok.cdtpoper <> :sentidoBusqueda ");
              whereUnion.append(" and bok.cdtpoper <> :sentidoBusqueda ");
            } else {
              String valor1 = sentidoBusqueda.substring(0, 1);
              String valor2 = sentidoBusqueda.substring(1, 2);
              sentidoBusqueda = "";
              if (tipo == 1) {
                where.append(" and bok.cdtpoper not in ('")
                        .append(valor1).append("','").append(valor2).append("') ");
              } else if (tipo == 2) {
                whereUnion.append(" and bok.cdtpoper not in ('").append(valor1)
                        .append("','").append(valor2).append("') ");
              } else {
                where.append(" and bok.cdtpoper <> '" + valor1 + "' ");
                whereUnion.append(" and bok.cdtpoper <> '" + valor2 + "' ");
              }
            }
          } else {
            String valor1 = sentidoBusqueda.substring(0, 1);
            String valor2 = sentidoBusqueda.substring(1, 2);
            sentidoBusqueda = "";
            if (tipo == 1) {
              where.append(" and bok.cdtpoper in ('").append(valor1).append("','").append(valor2).append("') ");
            } else if (tipo == 2) {
              whereUnion.append(" and bok.cdtpoper in ('").append(valor1).append("','").append(valor2).append("') ");
            } else {
              where.append(" and bok.cdtpoper = '").append(valor1).append("'");
              whereUnion.append(" and bok.cdtpoper = '" + valor2 + "'");
            }
          }
        }
      }
      
      whereUnion.append(" GROUP BY dc.nuorden,dc.nbooking,dc.nucnfclt,dc.nucnfliq,est.nombre,bok.cdtpoper, ")
          .append("bok.cdalias,bok.cdisin,bok.nbvalors,dc.cdestadoentrec,bok.descrali,ord.referencias3, ")
          .append("dc.fecontratacion,dc.iddesglosecamara,est2.nombre ,dc.feliquidacion, ")
          .append("ord.cuenta_compensacion_destino,ord.estado, ord.id, mer.id ");
      if (having.length() > 0) {
        whereUnion.append("HAVING ").append(having);
      }
      Query query = null;
      
      String stringQuery = "";
      if (tipo == 1) {
          stringQuery = select.append(from).append(where).toString();
      }
      else {
          //Se ajusta el WHERE union reemplazando el primer AND.
          if (whereUnion.substring(0, 3).equals("AND"))
              whereUnion.replace(0, 3, "WHERE");
          if (tipo == 2) {
            stringQuery = new StringBuilder(selectUnion).append(fromUnion).append(whereUnion).toString();  
          }
          if (tipo == 3) {
            stringQuery = new StringBuilder(select).append(from).append(where)
                    .append(selectUnion).append(fromUnion).append(whereUnion).toString();
          }
      }
      LOG.debug("[Tmct0alcDaoImp::findByEntregaRecepcionTodos]query: {}", stringQuery);
      query  = em.createNativeQuery(stringQuery);

      LOG.debug("[findByEntregaRecepcionTodos] fcontratacionDe :{}, fcontratacionA :{}, fliquidacionDe :{}, " +
      "fliquidacionA :{}, estado  :{}, nbooking  :{}, cdniftit  :{}, nbtitliq  :{}, biccle  :{}, " +
      "importeNetoDesde  :{}, importeNetoHasta  :{}, titulosDesde  :{}, titulosHasta  :{}, estadoAsig  :{}, " +
      "isin  :{}, alias  :{}, referencias3  :{}, sentidoBusqueda  :{}, pendEnviar  :{}, estadoInstruccion : {}", 
      fcontratacionDe, fcontratacionA, fliquidacionDe, fliquidacionA, estado, nbooking, cdniftit,
      nbtitliq, biccle, importeNetoDesde, importeNetoHasta, titulosDesde, titulosHasta, estadoAsig,
      isin, alias, referencias3, sentidoBusqueda, pendEnviar, estadoInstruccion);

      setParameters(query, alta, fcontratacionDe, fcontratacionA, fliquidacionDe, fliquidacionA, estadoInt, nbooking,
                    cdniftit, nbtitliq, biccle, importeNetoDesde, importeNetoHasta, titulosDesde, titulosHasta,
                    estadoAsig, isin, alias, referencias3, sentidoBusqueda, pendEnviar, estadoInstruccion, cuenta,
                    "fcontratacionDe",
                    "fcontratacionA", "fliquidacionDe", "fliquidacionA", "estadoInt", "nbooking", "cdniftit",
                    "nbtitliq", "biccle", "importeNetoDesde", "importeNetoHasta", "titulosDesde", "titulosHasta",
                    "estadoAsig", "isin", "alias", "referencias3", "sentidoBusqueda", "pendEnviar", "estadoInstruccion",
                    "cuenta"); 
      List<Object> resultList;
      resultList = query.getResultList();
      LOG.debug("[Tmct0alcDaoImp::findByEntregaRecepcionTodos] resultados obtenidos : {}", resultList.size());

      if (CollectionUtils.isNotEmpty(resultList)) {
        for (Object obj : resultList) {
          AlcEntregaRecepcionClienteDTO alc = AlcEntregaRecepcionClienteDTO
              .convertObjectToAlcEntregaRecepcionTodosDTO((Object[]) obj, tipo);
          Date fechaLiquidacion = tmct0alcDao.getMaxFechaValorS3LiqByRefS3(alc.getReferencias3());
          alc.setFechaLiquidacion(fechaLiquidacion);
          lista_alc.add(alc);
        }
      }


    } catch (RuntimeException ex) {
      LOG.error("[findByEntregaRecepcionTodos] Error en generación dinámica de consulta", ex);
      throw new SIBBACBusinessException("Error inesperado en consulta Entrega/Recepción", ex);
    }

    return lista_alc;

  }

  private void setParameters(Query query, boolean alta,
                             Date fcontratacionDe,
                             Date fcontratacionA,
                             Date fliquidacionDe,
                             Date fliquidacionA,
                             Integer estadoInt,
                             String nbooking,
                             String cdniftit,
                             String nbtitliq,
                             String biccle,
                             BigDecimal importeNetoDesde,
                             BigDecimal importeNetoHasta,
                             BigDecimal titulosDesde,
                             BigDecimal titulosHasta,
                             Integer estadoAsig,
                             String isin,
                             String alias,
                             String referencias3,
                             String sentidoBusqueda,
                             Integer pendEnviar,
                             String estadoInstruccion,
                             String cuenta,
                             String fcontratacionDeParam,
                             String fcontratacionAParam,
                             String fliquidacionDeParam,
                             String fliquidacionAParam,
                             String estadoIntParam,
                             String nbookingParam,
                             String cdniftitParam,
                             String nbtitliqParam,
                             String biccleParam,
                             String importeNetoDesdeParam,
                             String importeNetoHastaParam,
                             String titulosDesdeParam,
                             String titulosHastaParam,
                             String estadoAsigParam,
                             String isinParam,
                             String aliasParam,
                             String referencias3Param,
                             String sentidoBusquedaParam,
                             String pendEnviarParam,
                             String estadoInstruccionParam,
                             String cuentaParam) {

    if (fcontratacionDe != null) {
      query.setParameter(fcontratacionDeParam, fcontratacionDe);
    }

    if (fcontratacionA != null) {
      query.setParameter(fcontratacionAParam, fcontratacionA);
    }

    if (fliquidacionDe != null) {
      query.setParameter(fliquidacionDeParam, fliquidacionDe);
    }

    if (fliquidacionA != null) {
      query.setParameter(fliquidacionAParam, fliquidacionA);
    }

    if (estadoInt != null && estadoInt != 0) {
      query.setParameter(estadoIntParam, estadoInt);
    }

    if (nbooking != null && !nbooking.isEmpty()) {
      query.setParameter(nbookingParam, nbooking);
    }

    if (cdniftit != null && !cdniftit.isEmpty()) {
      query.setParameter(cdniftitParam, cdniftit);
    }

    if (nbtitliq != null && !nbtitliq.isEmpty()) {
      query.setParameter(nbtitliqParam, nbtitliq);
    }

    if (biccle != null && !biccle.isEmpty()) {
      query.setParameter(biccleParam, biccle);
    }

    if (importeNetoDesde != null && !BigDecimal.ZERO.equals(importeNetoDesde)) {
      query.setParameter(importeNetoDesdeParam, importeNetoDesde);
    }

    if (importeNetoHasta != null && !BigDecimal.ZERO.equals(importeNetoHasta)) {
      query.setParameter(importeNetoHastaParam, importeNetoHasta);
    }

    if (titulosDesde != null && !BigDecimal.ZERO.equals(titulosDesde)) {
      query.setParameter(titulosDesdeParam, titulosDesde);
    }

    if (titulosHasta != null && !BigDecimal.ZERO.equals(titulosHasta)) {
      query.setParameter(titulosHastaParam, titulosHasta);
    }

    //TODO: Eliminar el parametro de estadoAsig
    /*if (alta && estadoAsig != null) {
      query.setParameter(estadoAsigParam, estadoAsig);
    }*/

    if (isin != null && !isin.isEmpty()) {
      query.setParameter(isinParam, isin);
    }

    if (alias != null && !alias.isEmpty()) {
      query.setParameter(aliasParam, alias);
    }

    if (referencias3 != null && !referencias3.isEmpty()) {
      query.setParameter(referencias3Param, referencias3);
    }

    if (sentidoBusqueda != null && !sentidoBusqueda.isEmpty()) {
      query.setParameter(sentidoBusquedaParam, sentidoBusqueda);
    }

    //TODO Eliminar el parametro pendEnviar
    /*if (alta && pendEnviar != null) {
      query.setParameter(pendEnviarParam, pendEnviar);
    }*/
    
    if (estadoInstruccion != null && !estadoInstruccion.isEmpty())
        query.setParameter(estadoInstruccionParam, estadoInstruccion);
    
    if (cuenta != null && !cuenta.isEmpty())
        query.setParameter(cuentaParam, cuenta);

  }

  /**
   * Ejecuta la query de búsqueda de operaciones a las que devengar el canon de contratación de BME.
   * 
   * @param mesPasado
   * @return
   */
  @SuppressWarnings("unchecked")
public List<Object[]> findCanonBme(List<String> cuentaPropiaList,
                                     Integer fhFinalAliasActivo,
                                     Integer cdestadomov,
                                     Date fechaInicial,
                                     Date fechaFinal,
                                     Integer estadoAsignacion) {
    Query query = em.createNativeQuery(QUERY_CANONBME);

    query.setParameter("cuentaPropiaList", cuentaPropiaList);
    query.setParameter("fhFinalAliasActivo", fhFinalAliasActivo);
    query.setParameter("cdestadomov", cdestadomov);
    query.setParameter("fechaInicial", fechaInicial);
    query.setParameter("fechaFinal", fechaFinal);
    query.setParameter("estadoAsignacion", estadoAsignacion);
    return query.getResultList();
  } // findCanonBme

  public BigDecimal sumCuentaTipo(int cuenta, int tipo_comprobante, Tmct0alc alc) {
    Query query = em.createNativeQuery(sumCuentaTipo);
    query.setParameter("nuorden", alc.getNuorden());
    query.setParameter("nbooking", alc.getNbooking());
    query.setParameter("nucnfclt", alc.getNucnfclt());
    query.setParameter("nucnfliq", alc.getNucnfliq());
    query.setParameter("cuenta", cuenta);
    query.setParameter("tipo_comprobante", tipo_comprobante);
    BigDecimal bD = (BigDecimal) query.getSingleResult();
    return bD == null ? BigDecimal.ZERO : bD.setScale(4, BigDecimal.ROUND_HALF_UP);
  }

  public BigDecimal sumDevengosCliente(Tmct0alc alc) {
    return sumCuentaTipo(1122107, 72, alc);
  }

  /**
   * Obtiene la suma de canones y corretajes de las ordenes que cumplas las condiciones.
   * 
   * @param nurefordList Lista de números de referencia de las ordenes.
   * @param lEstadosAsig Lista de estados de asignación permitidos de las ordenes.
   * @return Sumatorio de los cánones y corretajes.
   */
  public Object[] sumCanonAndCorretaje(String nurefordList, String lEstadosAsig) {

    LOG.debug("[Tmct0alcDaoImp :: sumCanonAndCorretaje] Buscando corretaje ordenes.");
    Object[] corretajes = (Object[]) em.createNativeQuery(MessageFormat.format(QUERY_SUM_CANON_CORRETAJE, nurefordList,
                                                                               lEstadosAsig)).getSingleResult();
    if (corretajes != null) {
      return corretajes;
    } else {
      LOG.debug("[Tmct0alcDaoImp :: sumCanonAndCorretaje] Buscando corretaje ordenes cta 123-barrido san.");
      String nurefordListCta123St = DesgloseTmct0aloHelper.getListPartenonNurefordCta123(nurefordList);

      Object[] corretajesCta123 = (Object[]) em.createNativeQuery(MessageFormat.format(QUERY_SUM_CANON_CORRETAJE_CTA_123,
                                                                                       nurefordListCta123St,
                                                                                       lEstadosAsig)).getSingleResult();
      return corretajesCta123;
    }

  } // sumCanonAndCorretaje

  @SuppressWarnings("unchecked")
  public List<Object[]> findAlcsForCorretajesBME(String estadosAsig, Integer estadoCont, Date feopeliq) {
    Query query = em.createNativeQuery(MessageFormat.format(QUERY_CORRETAJES_BME, estadosAsig, estadoCont));
    query.setParameter("feopeliq", feopeliq);
    return (List<Object[]>) query.getResultList();
  } // findAlcsForCorretajesBME

  /**
   * 
   * @param nurefordList
   * @param estadoAlc
   * @param estadoAloBok
   */
  @Modifying
  @Transactional
  public void updateEstadoCont(String nurefordList, Integer estadoAlc, Integer estadoAloBok) {
    int countReg = 0;
    countReg += em.createNativeQuery(MessageFormat.format(UPDATE_BOK_CDESTADOCONT_BY_NUREFORD, estadoAloBok,
                                                          nurefordList)).executeUpdate();
    countReg += em.createNativeQuery(MessageFormat.format(UPDATE_ALO_CDESTADOCONT_BY_NUREFORD, estadoAloBok,
                                                          nurefordList)).executeUpdate();
    countReg += em.createNativeQuery(MessageFormat.format(UPDATE_ALC_CDESTADOCONT_BY_NUREFORD, estadoAlc, nurefordList))
                  .executeUpdate();
    if (countReg == 0) {
      String nurefordListCta123St = DesgloseTmct0aloHelper.getListPartenonNurefordCta123(nurefordList);
      em.createNativeQuery(MessageFormat.format(UPDATE_BOK_CDESTADOCONT_BY_NUREFORD_CTA_123, estadoAloBok,
                                                nurefordListCta123St)).executeUpdate();
      em.createNativeQuery(MessageFormat.format(UPDATE_ALO_CDESTADOCONT_BY_NUREFORD_CTA_123, estadoAloBok,
                                                nurefordListCta123St)).executeUpdate();
      em.createNativeQuery(MessageFormat.format(UPDATE_ALC_CDESTADOCONT_BY_NUREFORD_CTA_123, estadoAlc,
                                                nurefordListCta123St)).executeUpdate();
    }
  } // setEstadoCont

  @Modifying
  @Transactional
  public void updateEstadoContByClave(List<Tmct0alcId> alcIdList, Integer estadoAlc, Integer estadoAloBok) {
    Query query;
    for (Tmct0alcId tmct0alcId : alcIdList) {
      query = em.createNativeQuery(MessageFormat.format(UPDATE_BOK_CDESTADOCONT_BY_CLAVE, estadoAloBok));
      query.setParameter("nuorden", tmct0alcId.getNuorden());
      query.setParameter("nbooking", tmct0alcId.getNbooking());
      query.executeUpdate();

      query = em.createNativeQuery(MessageFormat.format(UPDATE_ALO_CDESTADOCONT_BY_CLAVE, estadoAloBok));
      query.setParameter("nuorden", tmct0alcId.getNuorden());
      query.setParameter("nbooking", tmct0alcId.getNbooking());
      query.setParameter("nucnfclt", tmct0alcId.getNucnfclt());
      query.executeUpdate();

      query = em.createNativeQuery(MessageFormat.format(UPDATE_ALC_CDESTADOCONT_BY_CLAVE, estadoAlc));
      query.setParameter("nuorden", tmct0alcId.getNuorden());
      query.setParameter("nbooking", tmct0alcId.getNbooking());
      query.setParameter("nucnfclt", tmct0alcId.getNucnfclt());
      query.setParameter("nucnfliq", tmct0alcId.getNucnfliq());
      query.executeUpdate();
    }
  } // setEstadoCont
  
	/**
	 * Ejecucion de la query de envio de ordenes partenón.
	 *
	 * @param datosForQueryPartenonDTO
	 * @throws SIBBACBusinessException
	 */
	public List<Object[]> ejecutarQueryEnvioOrdenesPartenon(
			DatosForQueryPartenonDTO datosForQueryPartenonDTO)
			throws SIBBACBusinessException {

		StringBuilder consulta = new StringBuilder(
				"SELECT alc.nuorden, alc.nbooking, alc.nucnfclt, alc.nucnfliq, dcli.feejecuc, "
						+ " dcli.cdmiembromkt, dcli.cdsentido, dcli.imtitulos, dcli.cdisin, dcli.imprecio, "
						+ " dcli.nurefexemkt, ord.cdcuenta, ord.nureford, rt.referencia_adicional, dcli.cd_ref_asignacion "
						+ " FROM TMCT0ALC alc "
						+ " JOIN TMCT0ORD ord ON alc.nuorden = ord.nuorden "
						+ " JOIN TMCT0DESGLOSECAMARA dc ON alc.nuorden = dc.nuorden AND alc.nbooking = dc.nbooking AND alc.nucnfclt = dc.nucnfclt "
						+ " AND alc.nucnfliq = dc.nucnfliq AND dc.cdestadoasig >= :cdestadoasig "
						+ " JOIN TMCT0DESGLOSECLITIT dcli ON dc.iddesglosecamara = dcli.iddesglosecamara AND dcli.cd_ref_asignacion in (:cdrefasignacion) "
						+ " AND dcli.miembro_compensador_destino in (:cdmiembrocompensador) AND dcli.cd_plataforma_negociacion in (:cdPlataformaNegociacion) "
						+ " JOIN TMCT0REFERENCIATITULAR rt ON alc.id_referencia_titular=rt.idreferencia "
						+ " WHERE alc.feejeliq = :fechaEjecucion AND alc.cdestadoasig >= :cdestadoasig AND dc.cdestadotit = :cdestadotit AND alc.desenliq <> 'S'");

		LOG.debug("Creamos y ejecutamos la query: " + consulta.toString());
		Query query = em.createNativeQuery(consulta.toString());

		// cdestadoasig
		query.setParameter("cdestadoasig", datosForQueryPartenonDTO.getCdestadoasig());
		
		// cdrefasignacion
		query.setParameter("cdrefasignacion", datosForQueryPartenonDTO.getCdrefasignacionList());
		
		// cdmiembrocompensador
		query.setParameter("cdmiembrocompensador", datosForQueryPartenonDTO.getCdmiembrocompensadorList());
		
		// cdPlataformaNegociacion
		query.setParameter("cdPlataformaNegociacion", datosForQueryPartenonDTO.getCdPlataformaNegociacionList());
		
		// fechaEjecucion
		query.setParameter("fechaEjecucion", datosForQueryPartenonDTO.getFechaEjecucion());
		
		// cdestadotit
		query.setParameter("cdestadotit", datosForQueryPartenonDTO.getCdestadotit());
		
		List<Object[]> resultList = query.getResultList();
		
		GenericLoggers.logDebug(
			Tmct0alcDaoImp.class, Constantes.UTILITIES, "ejecutarQueryEnvioOrdenesPartenon", 
			"Fecha ejecucion: " + datosForQueryPartenonDTO.getFechaEjecucion() 
				+ " -> Registros devueltos por hilo: " + (CollectionUtils.isNotEmpty(resultList)? resultList.size() : "0"));

		return resultList;
	}

	/**
	 *	Se actualiza el campo desenliq de una lista de cuartetos identificadores 
	 *	de alcs
	 *	@param desenliq
	 *	@param listaTramosIdsALCs
	 *	@throws SIBBACBusinessException 
	 */
	@Transactional
	@Modifying
	public void updateDesenliqALCs(Character desenliq, List<String> listaTramosIdsALCs) throws SIBBACBusinessException {
		StringBuffer consulta = new StringBuffer();
		if (CollectionUtils.isNotEmpty(listaTramosIdsALCs)) {
			for (int j = 0; j < listaTramosIdsALCs.size(); j++) {
				consulta.append(
					"UPDATE BSNBPSQL.TMCT0ALC ALC SET ALC.DESENLIQ = '" + desenliq 
						+ "' WHERE (ALC.NUORDEN, ALC.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ) IN (VALUES ");
				consulta.append(listaTramosIdsALCs.get(j));
				consulta.append(" )");
				LOG.debug("Creamos y ejecutamos la query con tramo: " + j + " -> " + consulta.toString());
				Query query = em.createNativeQuery(consulta.toString());
				query.executeUpdate();
				// Se vacia la expresion anterior.
				consulta = new StringBuffer();
			}
		}
	}
  
} // Tmct0alcDaoImp
