package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;


@Repository
public interface Tmct0grupoejecucionDao extends JpaRepository< Tmct0grupoejecucion, Long > {
  
  @Query("SELECT DISTINCT  g.cdcamara FROM Tmct0grupoejecucion g WHERE g.tmct0alo.id = :alloId ")
  List<String> findAllCdcamaraByAloId(@Param("alloId") Tmct0aloId aloId);
  
  @Query("SELECT g FROM Tmct0grupoejecucion g WHERE g.tmct0alo.id = :alloId ")
  List<Tmct0grupoejecucion> findAllByTmct0aloId(@Param("alloId") Tmct0aloId aloId);

}
