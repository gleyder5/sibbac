/**
 * 
 */
package sibbac.business.wrappers.database.dto.confirmacionminotista;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import sibbac.business.wrappers.database.model.Tmct0bokId;

/**
 * @author xIS16630
 *
 */
public class ConfirmacionMinorista16DTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -2633983785847416200L;

  // e.id.nuorden, e.feejecuc, e.hoejecuc, e.cdbroker, c.cdrefban, e.cdtpoper, e.cdisin, dt.imprecio, e.fevalor,
  // sum(dt.imtitulos), "
  // + " dt.nurefexemkt, e.id.nuejecuc, dt.cdPlataformaNegociacion

  private Tmct0bokId bookingId;
  private Date feejecuc;
  private Date hoejecuc;
  private String cdbroker;
  private String cdrefban;
  private Character cdtpoper;
  private String cdisin;
  private BigDecimal imprecio;
  private Date fevalor;
  private BigDecimal imtitulos;
  private String nuexemkt;
  private String nuejecuc;
  private String plataformaNegociacion;
  private String clearer;
  private String custodio;
  
  private String nureford;

  public ConfirmacionMinorista16DTO(Tmct0bokId bookingId,
                                    Date feejecuc,
                                    Date hoejecuc,
                                    String cdbroker,
                                    String cdrefban,
                                    Character cdtpoper,
                                    String cdisin,
                                    BigDecimal imprecio,
                                    Date fevalor,
                                    BigDecimal imtitulos,
                                    String nuexemkt,
                                    String nuejecuc,
                                    String plataformaNegociacion,
                                    String clearer,
                                    String custodio) {
    super();
    this.bookingId = bookingId;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.cdbroker = cdbroker;
    this.cdrefban = cdrefban;
    this.cdtpoper = cdtpoper;
    this.cdisin = cdisin;
    this.imprecio = imprecio;
    this.fevalor = fevalor;
    this.imtitulos = imtitulos;
    this.nuexemkt = nuexemkt;
    this.nuejecuc = nuejecuc;
    this.plataformaNegociacion = plataformaNegociacion;
    this.clearer = clearer;
    this.custodio = custodio;

  }

  public Tmct0bokId getBookingId() {
    return bookingId;
  }

  public void setBookingId(Tmct0bokId nbooking) {
    this.bookingId = nbooking;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public Date getHoejecuc() {
    return hoejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public String getCdrefban() {
    return cdrefban;
  }

  public void setCdrefban(String cdrefban) {
    this.cdrefban = cdrefban;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public String getCdisin() {
    return cdisin;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public BigDecimal getImprecio() {
    return imprecio;
  }

  public void setImprecio(BigDecimal imprecio) {
    this.imprecio = imprecio;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public BigDecimal getImtitulos() {
    return imtitulos;
  }

  public void setImtitulos(BigDecimal imtitulos) {
    this.imtitulos = imtitulos;
  }

  public String getNuexemkt() {
    return nuexemkt;
  }

  public void setNuexemkt(String nuexemkt) {
    this.nuexemkt = nuexemkt;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public String getPlataformaNegociacion() {
    return plataformaNegociacion;
  }

  public void setPlataformaNegociacion(String plataformaNegociacion) {
    this.plataformaNegociacion = plataformaNegociacion;
  }

  public String getClearer() {
    return clearer;
  }

  public void setClearer(String clearer) {
    this.clearer = clearer;
  }

  public String getCustodio() {
    return custodio;
  }

  public void setCustodio(String custodio) {
    this.custodio = custodio;
  }

  @Override
  public String toString() {
    return "ConfirmacionMinorista16DTO [nbooking=" + bookingId + ", feejecuc=" + feejecuc + ", hoejecuc=" + hoejecuc
           + ", cdbroker=" + cdbroker + ", cdrefban=" + cdrefban + ", cdtpoper=" + cdtpoper + ", cdisin=" + cdisin
           + ", imprecio=" + imprecio + ", fevalor=" + fevalor + ", imtitulos=" + imtitulos + ", nuexemkt=" + nuexemkt
           + ", nuejecuc=" + nuejecuc + ", plataformaNegociacion=" + plataformaNegociacion + ", clearer=" + clearer
           + ", custodio=" + custodio + "]";
  }

}
