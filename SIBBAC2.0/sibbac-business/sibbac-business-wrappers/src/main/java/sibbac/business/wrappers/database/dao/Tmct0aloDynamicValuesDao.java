package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0aloDynamicValues;
import sibbac.business.wrappers.database.model.Tmct0aloDynamicValuesId;

@Repository
public interface Tmct0aloDynamicValuesDao extends JpaRepository<Tmct0aloDynamicValues, Tmct0aloDynamicValuesId> {

}
