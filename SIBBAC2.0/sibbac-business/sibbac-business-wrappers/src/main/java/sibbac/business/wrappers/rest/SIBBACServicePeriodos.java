package sibbac.business.wrappers.rest;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.bo.IdentificadoresBo;
import sibbac.business.wrappers.database.bo.PeriodosBo;
import sibbac.business.wrappers.database.bo.TipoDeDocumentoBo;
import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServicePeriodos implements SIBBACServiceBean {

	private static final Logger	LOG	= LoggerFactory.getLogger( SIBBACServicePeriodos.class );

	private static final String TAG = SIBBACServicePeriodos.class.getName();

	@Autowired
	private PeriodosBo			periodosBo;

	@Autowired
	private TipoDeDocumentoBo	tipoDeDocumentoBo;

	@Autowired
	private IdentificadoresBo	identificadoresBo;

	@Autowired
	private Tmct0estadoBo		tmct0estadoBo;

	// ----------------------------------------------- Bean Constructors

	public SIBBACServicePeriodos() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		LOG.debug( "[SIBBACServicePeriodos::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// extrae parametros del httpRequest
		Map< String, String > params = webRequest.getFilters();

		// obtiene comandos(paths)
		LOG.debug( "[SIBBACServicePeriodos::process] " + "+ Command.: [{}]", webRequest.getAction() );

		switch ( webRequest.getAction() ) {
			case Constantes.CMD_CREATE_PERIODOS:
				try{
				    webResponse.setResultados( this.createPeriodos( params ) );
				}catch(Exception ex){
				    LOG.error(TAG+"::CMD_CREATE_PERIODOS err: "+ex.getMessage() + " cause: "+ex.getCause(), ex);
				    webResponse.setError("Error al crear el periodo.");
				}
				break;
			case Constantes.CMD_GET_PARAM_PERIODOS:
				webResponse.setResultados( this.getPeriodos() );
				break;
			case Constantes.CMD_GET_PARAM_ESTADOS:
				webResponse.setResultados( this.getEstados() );
				break;
			case Constantes.CMD_DELETE_PERIODOS:
				webResponse.setResultados( this.deletePeriodos( params ) );
			default:
				webResponse.setError( "No recibe ningun comando correcto" );
				LOG.error( "No le llega ningun comando" );
				break;
		}

		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_CREATE_PERIODOS );
		commands.add( Constantes.CMD_GET_PARAM_PERIODOS );
		commands.add( Constantes.CMD_GET_PARAM_ESTADOS );
		commands.add( Constantes.CMD_DELETE_PERIODOS );

		return commands;
	}

	@Override
	public List< String > getFields() {
		List< String > fields = new LinkedList< String >();
		fields.add( "nombre" );
		fields.add( "fechaInicioPeriodo" );
		fields.add( "fechaFinalPeriodo" );
		fields.add( "identificadores" );
		fields.add( "estado" );
		return fields;
	}

	public Map< String, Boolean > createPeriodos( Map< String, String > params ) throws Exception{

		Periodos periodos = new Periodos();

		Tmct0estado estado = new Tmct0estado();

		Identificadores identificadores = new Identificadores();

		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );

		Date fechaInicioPeriodo = new Date();
		Date fechaFinalPeriodo = new Date();

		try {
			if ( params.get( Constantes.PERIODOS_FECHAINICIOPERIODO ) != null ) {
				fechaInicioPeriodo = dateFormat.parse( params.get( Constantes.PERIODOS_FECHAINICIOPERIODO ) );
			} else {
			}
			fechaFinalPeriodo = dateFormat.parse( params.get( Constantes.PERIODOS_FECHAFINALPERIODO ) );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
			throw e;
		}

		if ( params.get( Constantes.PERIODOS_NOMBRE ) != null ) {
			periodos.setNombre( params.get( Constantes.PERIODOS_NOMBRE ) );
		}
		periodos.setFechaInicio( fechaInicioPeriodo );
		periodos.setFechaFinal( fechaFinalPeriodo );

		if ( params.get( Constantes.PERIODOS_IDENTIFICADORES ) != null ) {
			identificadores.setIndentificadores( Long.parseLong( params.get( Constantes.PERIODOS_IDENTIFICADORES ) ) );
			periodos.setIdentificador( identificadores );
		}

		if ( params.get( Constantes.PERIODOS_ESTADO ) != null ) {
			estado.setIdestado( Integer.parseInt( params.get( Constantes.PERIODOS_ESTADO ) ) );
			
			periodos.setEstado( estado );
		}

		Map< String, Boolean > resultado = new HashMap< String, Boolean >();
		Boolean result = false;
		try{
		    if ( periodosBo.save( periodos ) != null ) {
			result = true;
			}
		}catch(PersistenceException ex){
		    LOG.error(ex.getMessage() + " Cause: "+ex.getCause(), ex);
		   throw ex;
		}
		resultado.put( "result", result );
		return resultado;
	}

	public Map< String, Periodos > getPeriodos() {

		Map< String, Periodos > resultados = new Hashtable< String, Periodos >();
		List< Periodos > listaPeriodos = ( List< Periodos > ) periodosBo.findAll();

		for ( Periodos periodos : listaPeriodos ) {
			resultados.put( periodos.getId().toString(), periodos );
		}

		return resultados;
	}

	public Map< String, Boolean > deletePeriodos( Map< String, String > params ) {

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result = false;

		String ids = params.get( "ID" );

		periodosBo.delete( periodosBo.findById( NumberUtils.createLong( ids ) ) );

		resultado.put( "result", result );
		return resultado;
	}

	public Map< String, Tmct0estado > getEstados() {

		Map< String, Tmct0estado > resultados = new Hashtable< String, Tmct0estado >();
		Tmct0estado estado = new Tmct0estado();
		estado.setIdtipoestado( Constantes.CMD_PERIODO_ENUMERADO );
		List< Tmct0estado > listaEstados = tmct0estadoBo.findByIdtipoestado( estado );

		for ( Tmct0estado estados : listaEstados ) {
			// resultados.put(String.valueOf(estados.getIdestado()), estados);
			resultados.put( String.valueOf( estados.getNombre() ), estados );
		}

		return resultados;
	}

}
