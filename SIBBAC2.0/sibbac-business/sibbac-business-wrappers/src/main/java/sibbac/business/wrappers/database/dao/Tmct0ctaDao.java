package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cta;
import sibbac.business.wrappers.database.model.Tmct0ctaId;

@Repository
public interface Tmct0ctaDao extends JpaRepository<Tmct0cta, Tmct0ctaId> {

  @Query("select g from Tmct0cta g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking")
  List<Tmct0cta> findAllByNuodenAndNbooking(@Param("pnuorden") String nuorden, @Param("pnbooking") String nbooking);

  @Query("select g from Tmct0cta g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking and g.id.nucnfclt = :pnucnfclt ")
  List<Tmct0cta> findAllByNuodenAndNbookingAndNucnfclt(@Param("pnuorden") String nuorden,
      @Param("pnbooking") String nbooking, @Param("pnucnfclt") String nucnfclt);

  /**
   * private String nbooking; private String cdbroker; private String nuorden;
   * private String cdmercad;
   **/
  @Query("select g from Tmct0cta g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking "
      + " and g.id.cdbroker = :pcdbroker and g.id.cdmercad = :pcdmercad and g.id.nucnfclt = :pnucnfclt ")
  List<Tmct0cta> findAllByNuordenAndNbookingAndCdbrokerAndCdmercadAndNucnfclt(@Param("pnuorden") String nuorden,
      @Param("pnbooking") String nbooking, @Param("pcdbroker") String cdbroker, @Param("pcdmercad") String cdmercad,
      @Param("pnucnfclt") String nucnfclt);

  @Query("select distinct g.tmct0sta.id.cdestado from Tmct0cta g where g.id.nuorden = :pnuorden and g.id.nbooking = :pnbooking"
      + " and g.id.cdbroker = :pcdbroker and g.id.cdmercad = :pcdmercad ")
  List<String> findAllDistinctCdestadoInTmct0ctas(@Param("pnuorden") String nuorden,
      @Param("pnbooking") String nbooking, @Param("pcdbroker") String cdbroker, @Param("pcdmercad") String cdmercad);

}
