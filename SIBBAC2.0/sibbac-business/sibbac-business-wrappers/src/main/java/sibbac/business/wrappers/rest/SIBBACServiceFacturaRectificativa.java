package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.FacturaRectificativaBo;
import sibbac.business.wrappers.database.dto.FacturaDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceFacturaRectificativa implements SIBBACServiceBean {

	@Autowired
	private FacturaRectificativaBo	facturaRectificativaBo;

	private static final Logger		LOG						= LoggerFactory.getLogger( SIBBACServiceFacturaRectificativa.class );
	private static final String		CMD_LISTA_FACTURAS		= "getListadoFacturasRectificativas";

	private static final String		FILTER_ID_ALIAS			= "idAlias";
	private static final String		FILTER_FECHA_DESDE		= "fechaDesde";
	private static final String		FILTER_FECHA_HASTA		= "fechaHasta";
	private static final String		FILTER_ID_ESTADO		= "idEstado";
	private static final String		FILTER_NUMERO_FACTURA	= "numeroFactura";

	private static final String		RESULT_LISTA_FACTURAS	= "listadoFacturas";

	/* Comandos */

	/* Filtros */

	public SIBBACServiceFacturaRectificativa() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		final String prefix = "[SIBBACServiceFacturaRectificativa::process] ";
		LOG.debug( prefix + "Processing a web request..." );

		final String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );

		WebResponse webResponse = new WebResponse();

		switch ( command ) {
			case CMD_LISTA_FACTURAS:
				this.getListadoFacturas( webRequest, webResponse );
				break;
			default:
				webResponse.setError( command + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
				break;
		}
		return webResponse;
	}

	private void getListadoFacturas( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isNotEmpty( filtros ) ) {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			final Long numeroFactura = getNumeroFactura( filtros.get( FILTER_NUMERO_FACTURA ) );
			if ( numeroFactura != null ) {
				facturas.put( RESULT_LISTA_FACTURAS, facturaRectificativaBo.getListadoFacturas( numeroFactura ) );
			} else {

				final Integer idEstado = getIdEstado( filtros.get( FILTER_ID_ESTADO ) );
				final Long idAlias = getIdAlias( filtros.get( FILTER_ID_ALIAS ) );
				final Date fechaDesde = convertirStringADate( filtros.get( FILTER_FECHA_DESDE ) );
				final Date fechaHasta = convertirStringADate( filtros.get( FILTER_FECHA_HASTA ) );
				facturas.put( RESULT_LISTA_FACTURAS, facturaRectificativaBo.getListadoFacturas( idAlias, fechaDesde, fechaHasta, idEstado ) );
			}
			webResponse.setResultados( facturas );
		} else {
			final Map< String, List< FacturaDTO >> facturas = new HashMap< String, List< FacturaDTO >>();
			facturas.put( RESULT_LISTA_FACTURAS, facturaRectificativaBo.getListadoFacturas( null, null, null, null ) );
			webResponse.setResultados( facturas );
		}
	}

	/**
	 * @param filtros
	 * @return
	 */
	private Date convertirStringADate( final String fechaString ) {
		Date fecha = null;
		if ( StringUtils.isNotEmpty( fechaString ) ) {
			fecha = FormatDataUtils.convertStringToDate( fechaString );
		}
		return fecha;
	}

	/**
	 * @param idEstadoStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Integer getIdEstado( final String idEstadoStr ) throws SIBBACBusinessException {
		Integer idEstado = null;
		try {
			idEstado = FormatDataUtils.convertStringToInteger( idEstadoStr );
		} catch ( SIBBACBusinessException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idEstado\": [" + idEstadoStr + "] " );
		}

		return idEstado;
	}

	/**
	 * @param numeroFactura
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getNumeroFactura( final String numeroFacturaStr ) throws SIBBACBusinessException {
		Long numeroFactura = null;
		try {
			numeroFactura = FormatDataUtils.convertStringToLong( numeroFacturaStr );
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idEstado\": [" + numeroFactura + "] " );
		}
		return numeroFactura;
	}

	/**
	 * @param idAliasStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAlias( final String idAliasStr ) throws SIBBACBusinessException {
		Long idAlias = null;
		try {
			if ( StringUtils.isNotBlank( idAliasStr ) ) {
				idAlias = NumberUtils.createLong( idAliasStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] " );
		}
		return idAlias;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_LISTA_FACTURAS );
		return commands;
	}

	@Override
	public List< String > getFields() {
		return null;
	}

}
