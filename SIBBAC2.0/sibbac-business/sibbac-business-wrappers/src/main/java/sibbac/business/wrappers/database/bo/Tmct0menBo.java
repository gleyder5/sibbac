package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.dto.SessionStatus;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.dao.Tmct0menDao;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.business.wrappers.database.model.Tmct0staId;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0menBo extends AbstractBo<Tmct0men, String, Tmct0menDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(Tmct0menBo.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Formato del campo <code>cdmensa</code>. */
  private static NumberFormat nF1 = new DecimalFormat("000");

  /** TODO. */
  private static final String CDTIPEST = "TMCT0MSC";

  /**
   * Query para actualizar el estado, la fecha de auditoría y el hostname de la
   * máquina desde donde se ejecuta la query siempre que el estado sea uno
   * especificado.
   */
  private static final String UPDATE_ESTADO = "UPDATE tmct0men SET cdestado=:estadoFin, cdusuaud=:cdusuaud, fhaudit=:fhaudit, urlpath=:urlpath"
      + " WHERE cdmensa=:cdmensa AND cdestado=:estadoIni";

  /**
   * Query para actualizar la fecha de auditoría y el hostname de la máquina
   * desde donde se ejecuta la query para un registro especificado.
   */
  private static final String UPDATE_FHAUDIT = "UPDATE tmct0men SET fhaudit=:fhaudit, urlpath=:urlpath WHERE cdmensa=:cdmensa";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private Tmct0staBo tmct0staBo;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @PersistenceContext
  private EntityManager entityManager;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * 
   * @param cdmensa
   * @return
   */
  public Tmct0men findByCdmensa(final String cdmensa) {
    return this.dao.findByCdmensa(cdmensa);
  } // findByCdmensa

  /**
   * 
   * @param cdmensa
   * @return
   */
  public Tmct0men findByTipo(final TIPO tipo) {
    String cdmensa = StringUtils.leftPad(String.format("%d", tipo.getTipo()), 3, "0");
    return this.dao.findByCdmensa(cdmensa);
  } // findByCdmensa


  public boolean validateIntervalTime(Tmct0men men) {
    boolean intervalValid = true;
    final Time currentTime =  new Time(Calendar.getInstance().getTimeInMillis());
    if(men != null && men.getHoraInicio()!=null && men.getHoraFin()!=null ) {
    	if(currentTime.getTime() <= men.getHoraFin().getTime() && currentTime.getTime() >= men.getHoraInicio().getTime()) {
    		intervalValid= true ; 
    	}else {
    		intervalValid =false;
    	}
    }    
    return intervalValid;
  }
  public boolean validarHost(  Tmct0men men ,String currentHost) {
    boolean validHost = false;
    if(StringUtils.isEmpty(men.getHostPermitidos()) || men.getHostPermitidos()==null) {
    	validHost = true;
    }
    else {
    	final String[] urls= men.getHostPermitidos().split(";");
    	if(urls.length>0) {
    		validHost = Arrays.asList(urls).contains(currentHost);
    	}
    }
    return validHost;
  } // fi
  /**
   * 
   * @param tmct0men
   * @param listoGenerar
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void putEstadoMENAndIncrement(Tmct0men tmct0men, TMCT0MSC listoGenerar) {
    Date now = new Date();
    tmct0men.setTmct0sta(tmct0staBo.findById(new Tmct0staId(listoGenerar.getCdmensa(), CDTIPEST)));
    tmct0men.setFhaudit(now);
    if (DateUtils.isSameDay(now, tmct0men.getFemensa())) {
      tmct0men.setNuenvio(tmct0men.getNuenvio() + 1);
    }
    else {
      tmct0men.setNuenvio(1);
      tmct0men.setFemensa(now);
    }
    tmct0men.setHrmensa(now);
    try {
      tmct0men.setUrlpath(InetAddress.getLocalHost().getHostName());
    }
    catch (UnknownHostException e) {
      tmct0men.setUrlpath("Error obteniendo HostName");
    }
    save(tmct0men);
  } // putEstadoMENAndIncrement

  /**
   * 
   * @param tmct0men
   * @param listoGenerar
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void putEstadoMEN(Tmct0men tmct0men, TMCT0MSC listoGenerar) {
    this.putEstadoMEN(tmct0men, listoGenerar, "SIBBAC20");
  } // putEstadoMEN

  /**
   * 
   * @param tmct0men
   * @param listoGenerar
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void putEstadoMEN(Tmct0men tmct0men, TMCT0MSC listoGenerar, String cdusuaud) {
    Date now = new Date();
    tmct0men.setTmct0sta(tmct0staBo.findById(new Tmct0staId(listoGenerar.getCdmensa(), CDTIPEST)));
    tmct0men.setFhaudit(now);
    tmct0men.setCdusuaud(cdusuaud);
    try {
      tmct0men.setUrlpath(InetAddress.getLocalHost().getHostName());
    }
    catch (UnknownHostException e) {
      tmct0men.setUrlpath("Error obteniendo HostName");
    }
    save(tmct0men);
  } // putEstadoMEN

  /**
   * 
   * @param tipo
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void updateFhauditMEN(TIPO tipo) {
    Query query = entityManager.createNativeQuery(UPDATE_FHAUDIT);
    query.setParameter("cdmensa", nF1.format(tipo.getTipo()));
    try {
      query.setParameter("urlpath", InetAddress.getLocalHost().getHostName());
    }
    catch (UnknownHostException e) {
      query.setParameter("urlpath", "Error obteniendo HostName");
    }
    query.setParameter("fhaudit", new Date());
    query.executeUpdate();
  } // updateFhauditMEN

  /**
   * 
   * @param tipo
   * @param estadoIni
   * @param estadoFin
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Tmct0men putEstadoMEN(TIPO tipo, TMCT0MSC estadoIni, TMCT0MSC estadoFin, String cdusuaud)
      throws SIBBACBusinessException {
    Date now = new Date();
    Query query = entityManager.createNativeQuery(UPDATE_ESTADO);
    query.setParameter("cdmensa", nF1.format(tipo.getTipo()));
    query.setParameter("estadoIni", estadoIni.getCdmensa());
    query.setParameter("estadoFin", estadoFin.getCdmensa());
    query.setParameter("fhaudit", now);
    query.setParameter("cdusuaud", StringUtils.substring(cdusuaud, 0, 10));
    try {
      query.setParameter("urlpath", InetAddress.getLocalHost().getHostName());
    }
    catch (UnknownHostException e) {
      query.setParameter("urlpath", "Error obteniendo HostName");
    }
    int update = query.executeUpdate();
    if (update == 0) {
      throw new SIBBACBusinessException("No hay registros en Men del tipo " + tipo.getTipo() + ":"
          + tipo.getDescripcion() + " en el estado " + estadoIni.getCdmensa());
    }
    return dao.getOne(nF1.format(tipo.getTipo()));
  } // putEstadoMEN

  /**
   * 
   * @param tipo
   * @param estadoIni
   * @param estadoFin
   * @return
   * @throws SIBBACBusinessException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Tmct0men putEstadoMEN(TIPO tipo, TMCT0MSC estadoIni, TMCT0MSC estadoFin) throws SIBBACBusinessException {
    return putEstadoMEN(tipo, estadoIni, estadoFin, "SIBBAC20");
  } // putEstadoMEN

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void updateFhauditAndFemensaAndHrmensaAndIncrementNuenvio(Tmct0men tmct0men, Calendar calFichero) {
    Date now = new Date();
    tmct0men.setFhaudit(now);
    if (DateUtils.isSameDay(now, tmct0men.getFemensa())) {
      Calendar cal = Calendar.getInstance();
      cal.setTime(tmct0men.getFemensa());
      cal.setTime(tmct0men.getHrmensa());
      if (!cal.equals(calFichero)) {
        tmct0men.setNuenvio(tmct0men.getNuenvio() + 1);
        tmct0men.setHrmensa(calFichero.getTime());
      }
    }
    else {
      tmct0men.setNuenvio(1);
      tmct0men.setFemensa(calFichero.getTime());
      tmct0men.setHrmensa(calFichero.getTime());
    }

    try {
      tmct0men.setUrlpath(InetAddress.getLocalHost().getHostName());
    }
    catch (UnknownHostException e) {
      tmct0men.setUrlpath("Error obteniendo HostName");
    }
    save(tmct0men);
  } // updateFhauditAndFemensaAndHrmensaAndIncrementNuenvio

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void updateFhauditAndFemensaAndHrmensaAndResetNuenvio(Tmct0men tmct0men, Calendar calFichero) {
    tmct0men.setFhaudit(new Date());
    tmct0men.setFemensa(calFichero.getTime());
    tmct0men.setHrmensa(calFichero.getTime());
    tmct0men.setNuenvio(1);

    try {
      tmct0men.setUrlpath(InetAddress.getLocalHost().getHostName());
    }
    catch (UnknownHostException e) {
      tmct0men.setUrlpath("Error obteniendo HostName");
    }
    save(tmct0men);
  } // updateFhauditAndFemensaAndHrmensaAndResetNuenvio

  /**
   * @param isSd
   * @throws SIBBACBusinessException
   */
  public boolean isSessionOpened(boolean isSd) throws SIBBACBusinessException {
    LOG.debug("isSessionOpened()[INICIO] ");
    SessionStatus bean = getSessionStatus(isSd, new Date());
    boolean sessionOpened = bean.isOpened();
    LOG.debug("isSessionOpened()[FIN] session opened : " + sessionOpened);
    return sessionOpened;
  }

  public SessionStatus getSessionStatus(boolean isSd, Date now) throws SIBBACBusinessException {
    LOG.debug("getSessionStatus()[INICIO] ");
    SessionStatus bean = new SessionStatus(now);

    // Fin de semana?
    // if (Tools_date.isWeekEnd(now)) {
    // LOG.debug(PREFIX + "getSessionStatus() is weekend");
    // bean.setWeekEnd(true);
    // bean.setStatus(SessionStatus.STATUS.WEEKEND);
    // LOG.debug(PREFIX + "getSessionStatus() [FIN] bean :" + bean.toString());
    // return bean;
    // }

    // No es Script Dividend?
    if (isSd) {
      LOG.debug("getSessionStatus() is script dividend");
      bean.setOpened(true);
      bean.setScriptDividend(true);
      bean.setStatus(SessionStatus.STATUS.SCRIPT_DIVIDEND);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    // El JDO.
    Tmct0men men = findByCdmensa(StringUtils.leftPad(String.format("%s", TIPO_APUNTE.PTI_FIN_DIA.getTipo()), 3, "0"));
    if (men == null) {
      bean.setError(SessionStatus.ERROR.DATA_ACCESS);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    // Cogemos el estado de cierre manual de sesion.
    Character x = men.getCdGeneralPtiSessionStatus();
    if (x != null && x != ' ') {
      LOG.debug("getSessionStatus() session is closed");
      bean.setUserClosedTheSession(true);
      bean.setStatus(SessionStatus.STATUS.USER_CLOSED);
      bean.setGeneralClosed(true);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    // Tenemos JDO. Vemos si hay hora de inicio de sesion.
    String horaInicioDeSesion = getHoraInicioSesion();
    if (horaInicioDeSesion == null || "".equals(horaInicioDeSesion.trim())) {
      // No hay hora de inicio...
      bean.setError(SessionStatus.ERROR.NO_INI_TIME);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    bean.setInicioSesion(FormatDataUtils.convertStringToTime(horaInicioDeSesion, FormatDataUtils.TIME_FORMAT));

    // Hay hora de inicio de sesion. Algunas variables mas.
    if (isAntesDe(now, FormatDataUtils.convertStringToTime(horaInicioDeSesion, FormatDataUtils.TIME_FORMAT))) {
      bean.setBeforeStart(true);
      bean.setStatus(SessionStatus.STATUS.SESSION_BEFORE);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    // Estamos DESPUES de la hora de inicio. Cogemos la hora de fin de sesion
    // que haya, la del dia anterior que quede
    // guardada o
    // la de hoy que haya llegado con un FS, y la que haya indicado el
    // usuario...
    Date fmensa = men.getFemensa();
    Date horaFinDeSesion = men.getHrmensa();
    if (!isAntesDeFecha(fmensa, now)) {

      if (horaFinDeSesion == null) {
        bean.setError(SessionStatus.ERROR.NO_END_TIME);
        LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
        return bean;
      }

      bean.setFinSesion(new Date(horaFinDeSesion.getTime()));

      // Si estamos en medio de la sesion...
      if (isAntesDe(now, horaFinDeSesion)) {
        bean.setUserWindowAvailable(false);
        bean.setOpened(true);
        bean.setStatus(SessionStatus.STATUS.SESSION_OPENED);
        LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
        return bean;
      }
    }
    else {
      bean.setUserWindowAvailable(false);
      bean.setOpened(true);
      bean.setStatus(SessionStatus.STATUS.SESSION_OPENED);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    // ===> Estamos fuera de la sesion. Veamos si hay inicio manual.
    // Ademas, hay que comprobar que dicha hora sea posterior a la de fin de
    // sesion.
    // Por defecto...
    bean.setUserWindowAvailable(true);
    bean.setOpened(false);
    bean.setStatus(SessionStatus.STATUS.SESSION_CLOSED);

    Date horaIniDeSesionPorUsuario = men.getCdUserSessionOpen();
    if (horaIniDeSesionPorUsuario == null) {
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }
    bean.setInicioUserSesion(new Date(horaIniDeSesionPorUsuario.getTime()));
    if (horaIniDeSesionPorUsuario.before(horaFinDeSesion)) {
      men.setCdUserSessionOpen(null);
      bean.setInicioUserSesion(null);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }
    bean.setUserSessionStart(true);

    // Si estamos antes de la sesion manual...
    if (isAntesDe(now, horaIniDeSesionPorUsuario)) {
      bean.setStatus(SessionStatus.STATUS.MANUAL_SESSION_BEFORE);
      LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());
      return bean;
    }

    // La sesion "manual" esta abierta.
    bean.setOpened(true);
    bean.setStatus(SessionStatus.STATUS.MANUAL_SESSION_AFTER);
    bean.setUserWindowAvailable(false);
    LOG.debug("getSessionStatus() [FIN] bean : {}", bean.toString());

    return bean;
  }

  public String getHoraInicioSesion() {
    return tmct0cfgBo.getHoraInicioSessionPti();

  }

  public boolean isAntesDe(Date hora, Date contra) throws SIBBACBusinessException {
    BigDecimal hHora = FormatDataUtils
        .convertStringToBigDecimal(FormatDataUtils.convertDateToString(hora, FormatDataUtils.TIME_FORMAT));
    BigDecimal hContra = FormatDataUtils
        .convertStringToBigDecimal(FormatDataUtils.convertDateToString(contra, FormatDataUtils.TIME_FORMAT));

    // Si estamos ANTES de la hora de inicio...
    return (hHora.compareTo(hContra) < 0);
  }

  private boolean isAntesDeFecha(Date fecha, Date contra) throws SIBBACBusinessException {
    BigDecimal hHora = FormatDataUtils
        .convertStringToBigDecimal(FormatDataUtils.convertDateToString(fecha, FormatDataUtils.DATE_FORMAT));
    BigDecimal hContra = FormatDataUtils
        .convertStringToBigDecimal(FormatDataUtils.convertDateToString(contra, FormatDataUtils.DATE_FORMAT));

    // Si estamos ANTES de la hora de inicio...
    return (hHora.compareTo(hContra) < 0);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
  public void openPtiSession() throws SIBBACBusinessException {
    LOG.info("[openPtiSession] inicio");
    LOG.info("[openPtiSession] buscando registro men fin dia...");
    Tmct0men men = findByCdmensa(StringUtils.leftPad(String.format("%s", TIPO_APUNTE.PTI_FIN_DIA.getTipo()), 3, "0"));
    if (men == null) {
      throw new SIBBACBusinessException(
          MessageFormat.format("No se ha encontrado registro men: {0}-{1} para abrir la sesion de pti.",
              TIPO_APUNTE.PTI_FIN_DIA.getTipo(), TIPO_APUNTE.PTI_FIN_DIA.name()));
    }

    LOG.info("[openPtiSession] limpiando flags de fin de session...");
    men.setCdUserSessionOpen(null);
    men.setCdGeneralPtiSessionStatus(' ');

    save(men);
    LOG.info("[openPtiSession] Buscando config desactivacion manual de inputacion S3...");
    Tmct0cfg dailyDeactivate = tmct0cfgBo.getPtiImputacionesDailyDeactivate();
    if (dailyDeactivate != null) {
      LOG.info("[openPtiSession] limpiando inactivacion manual de la imputacion S3...");
      dailyDeactivate.setKeyvalue("0");
      tmct0cfgBo.save(dailyDeactivate);
    }
    LOG.info("[openPtiSession] fin");
  }

  public String estadoDisponible(final String cdmensa) {

    Object[] estado = this.dao.findByCdMensaAndGetEstado(cdmensa);

    return estado[0].toString();
  }

} // Tmct0menBo
