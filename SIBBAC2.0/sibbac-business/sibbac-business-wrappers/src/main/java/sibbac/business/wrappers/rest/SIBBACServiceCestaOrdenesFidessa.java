package sibbac.business.wrappers.rest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.SendMail;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceCestaOrdenesFidessa implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceCestaOrdenesFidessa.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  private static final String FILE_CHARSET = "ISO-8859-1";

  @Autowired
  private SendMail sendMail;

  @Value("${ruta_servidor.partenon.in}")
  private String rutaFicherosFaseUno;

  @Value("${ruta_servidor.fidessa}")
  private String rutaFicherosFidessa;

  @Value("${ruta_servidor.fidessa.tratados}")
  private String rutaFicherosTratados;

  @Value("${pattern_ficheros.partenon.in}")
  private String pattern_ficheros;

  @Value("${caracteres.inicio.desglose}")
  private String sCadenaDesglose;

  @Value("${ruta_servidor.python}")
  private String sRutaPhyton;

  @Value("${python.in.emails}")
  private String sMailsPhyton;

  @Value("${python.in.emails.subjects}")
  private String sMailSubjectPhyton;

  @Value("${python.in.emails.body}")
  private String sMailBodyPhyton;

  @Value("${python.in.nombreFicheroAdjunto}")
  private String sNombreFicheroAdjunto;

  @Value("${cabecera_ficheroXLSX_fase2_columna1}")
  private String sValorCabeceraColumna1;

  @Value("${cabecera_ficheroXLSX_fase2_columna2}")
  private String sValorCabeceraColumna2;

  private enum ComandosCestaOrdenesFidessa {
    /** Inicializa los datos del filtro de pantalla */
    OBTENER_FICHEROS("obtenerFicheros"),
    NUMERO_DESGLOSES("obtenerNumeroDesgloses"),
    EJECUTAR_FASE_UNO("ejecutarFaseUno"),
    EJECUTAR_FASE_DOS("ejecutarFaseDos"),
    SIN_COMANDO("");

    /** The command. */
    private String command;

    private ComandosCestaOrdenesFidessa(String command) {
      this.command = command;
    }

    public String getCommand() {
      return command;
    }
  }

  @Override
  public List<String> getAvailableCommands() {

    return null;
  }

  @Override
  public List<String> getFields() {

    return null;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosCestaOrdenesFidessa getCommand(String command) {
    ComandosCestaOrdenesFidessa[] commands = ComandosCestaOrdenesFidessa.values();
    ComandosCestaOrdenesFidessa result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosCestaOrdenesFidessa.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {

    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    ComandosCestaOrdenesFidessa command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceCestaOrdenesFidessa");

      switch (command) {
        case OBTENER_FICHEROS:
          result.setResultados(obtenerFicheros());
          break;
        case NUMERO_DESGLOSES:
          result.setResultados(obtenerNumeroDesgloses(paramsObjects));
          break;

        case EJECUTAR_FASE_UNO:
          result.setResultados(ejecutarFaseUno(paramsObjects));
          break;
        case EJECUTAR_FASE_DOS:
          result.setResultados(ejecutarFaseDos(paramsObjects));
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceCestaOrdenesFidessa");
    return result;
  }

  /**
   * metodo que obtiene el listado de ficheros disponibles en la ruta.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> obtenerFicheros() throws SIBBACBusinessException {

    LOG.debug("Iniciando obtenerFicheros");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {
      List<SelectDTO> listaFicheros = new ArrayList<SelectDTO>();

      Path pDirectorioIn = Paths.get(rutaFicherosFaseUno);

      SelectDTO miDto = null;

      try (DirectoryStream<Path> stream = Files.newDirectoryStream(pDirectorioIn, pattern_ficheros);) {
        for (Path entry : stream) {

          if (!Files.isDirectory(entry)) {
            miDto = new SelectDTO(entry.getFileName().toString(), entry.getFileName().toString());
            listaFicheros.add(miDto);
          }
        }
      }
      mSalida.put("listaFicheros", listaFicheros);

    } catch (Exception e) {
      LOG.error("Error metodo obtenerFicheros: " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al obtener el listado de ficheros de la ruta " + rutaFicherosFaseUno);
    }

    LOG.debug("Fin obtenerFicheros de la ruta " + rutaFicherosFaseUno);

    return mSalida;
  }

  /**
   * metodo que procesa el fichero seleccionado y devuelve el numero de desgloses. Desglose: Linea que comienza por 40
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> obtenerNumeroDesgloses(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Iniciando obtenerNumeroDesgloses");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    int iNumDesgloses = 0;

    try {
      List<SelectDTO> listaFicheros = new ArrayList<SelectDTO>();

      String sFichero = paramsObjects.get("fichero").toString();

      if (sFichero != null) {

        Path pFicheroProcesar = Paths.get(rutaFicherosFaseUno + "//" + sFichero);

        List<String> lineasFichero = Files.readAllLines(pFicheroProcesar, Charset.forName(FILE_CHARSET));

        if (lineasFichero.size() > 0) {
          for (int i = 0; i < lineasFichero.size(); i++) {
            String sLinea = lineasFichero.get(i);

            if (sLinea.startsWith(sCadenaDesglose)) {
              iNumDesgloses++;
            }
          }
        }

      } else {
        throw new SIBBACBusinessException("No se ha recibido un fichero válido.");
      }

      mSalida.put("numeroDesgloses", iNumDesgloses);

    } catch (SIBBACBusinessException e1) {
      throw e1;
    } catch (Exception e) {
      LOG.error("Error metodo obtenerNumeroDesgloses " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al obtener los desgloses");
    }

    LOG.debug("Fin obtenerNumeroDesgloses para el fichero: ");

    return mSalida;
  }

  /**
   * metodo que procesa el fichero seleccionado y devuelve el numero de desgloses. Desglose: Linea que comienza por 40
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> ejecutarFaseUno(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Iniciando ejecutacion Fase Uno");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    try {

      String sFichero = paramsObjects.get("fichero").toString();

      if (sFichero != null) {
        Path pFicheroProcesar = Paths.get(rutaFicherosFaseUno + "//" + sFichero);
        Path pRutaDestino = Paths.get(sRutaPhyton + "//" + sFichero);
        try {

          Files.move(pFicheroProcesar, pRutaDestino, StandardCopyOption.REPLACE_EXISTING);
          LOG.debug("Fase 1 -> Fichero movido");
        } catch (Exception e) {
          LOG.error("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseUno] ERROR moviendo  el fichero a la ruta ejecucion python: "
                    + e.getMessage());
          throw new SIBBACBusinessException("No se podido mover el fichero a la ruta de ejecucion de Phyton.");
        }

        // Ejecucion del proceso pyton.

        try {

          LOG.debug("Fase 1 -> llamamos a proceso python");
          String sFicheroPY = sRutaPhyton + "//procBarridos.py";
          String sFicheroProcesar = sRutaPhyton + "//" + sFichero;

          // Se convierten los parametros a path para poder pasarlos al processbuider y que el sonar no lo detecte como
          // una injeccion de codigo.
          Path pFicheroProcesarDest = Paths.get(sFicheroProcesar);
          Path pFicherosFicheroPY = Paths.get(sFicheroPY);

          ProcessBuilder pb = new ProcessBuilder("python", pFicherosFicheroPY.toString(), "-f", "FASE1", "-nf",
                                                 pFicheroProcesarDest.toString());
          LOG.debug("Fase 1 -> Le inicio");
          Process p = pb.start();

          int iResultado = -1;

          while (iResultado < 0) {
            try {
              iResultado = p.exitValue();
            } catch (Exception e) {
              LOG.debug("Fase 1 -> Todavia se esta ejecutando");
              iResultado = -1;
              Thread.sleep(1000);
            }
          }

          LOG.debug("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseUno] Resultado proceso: " + iResultado);

          // Se procesa la salida y se muestra en el log
          BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

          String sLinea = null;

          while ((sLinea = in.readLine()) != null) {
            LOG.debug("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseUno] Resultado proceso: " + sLinea);
          }

          if (iResultado > 0) {
            throw new SIBBACBusinessException("La ejecucion del proceso python ha devuelvo un error - Revise el log");
          }

          // Si proceso ok

          // Se mueve el fichero que el python ha procesado a tratados.
          Files.move(pRutaDestino, Paths.get(rutaFicherosTratados + "/" + pRutaDestino.getFileName()),
                     StandardCopyOption.REPLACE_EXISTING);

          // Enviar fichero generado por correo.
          // Se convierte el fichero a xlsx

          // Se busca el fichero csv

          String sFicheroCsv = rutaFicherosFidessa + "//" + getFicheroCSVFidessa();

          Path pFicherosXslx = convertCsvToXmls(sFicheroCsv);

          // Se envian un correo con el xslx adjunto.
          try {

            final List<InputStream> inputstream = new ArrayList<InputStream>();
            final List<String> nameDest = new ArrayList<String>();

            nameDest.add(pFicherosXslx.getFileName().toString());
            inputstream.add(new FileInputStream(pFicherosXslx.toFile()));

            LOG.debug("Fase 1 -> Enviando correo a :" + sMailsPhyton);

            sendMail.sendMail(Arrays.asList(sMailsPhyton.split(";")), sMailSubjectPhyton, sMailBodyPhyton, inputstream,
                              nameDest, null);

            LOG.debug("Fase 1 -> Correo enviado");

          } catch (Exception e2) {
            LOG.error("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseUno] ERROR enviando el mail con el fichero: "
                      + e2.getMessage());
            throw new SIBBACBusinessException("No se podido enviar el mail con el excel del resultado");

          }

          // Se borra el fichero.
          Files.delete(pFicherosXslx);

        } catch (SIBBACBusinessException e3) {
          throw e3;
        } catch (Exception e) {
          LOG.error("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseUno] ERROR ejecutando el proceso: "
                    + e.getMessage());
          throw new SIBBACBusinessException("Se ha producido un error en la ejecucion del proceso python.");
        }

      } else {
        throw new SIBBACBusinessException("No se ha recibido un fichero válido.");
      }

    } catch (SIBBACBusinessException e1) {
      throw e1;
    } catch (Exception e) {
      LOG.error("Error metodo obtenerNumeroDesgloses " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al obtener los desgloses");
    }

    LOG.debug("Fin ejecutarFaseUno");

    return mSalida;
  }

  private String getFicheroCSVFidessa() throws SIBBACBusinessException {
    String sResultado = null;
    List<String> listaFicheros = new ArrayList<String>();
    try {

      Path pFicheroCsvGenerado = Paths.get(rutaFicherosFidessa);
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(pFicheroCsvGenerado, "*.csv");) {
        for (Path entry : stream) {

          if (!Files.isDirectory(entry)) {
            LOG.debug("FICHERO " + entry.getFileName().toString());
            listaFicheros.add(entry.getFileName().toString());
          }
        }
      }
      if (listaFicheros.size() == 0) {
        throw new SIBBACBusinessException("No existe ningún fichero csv en el directorio Fidessa.");
      }

      if (listaFicheros.size() > 1) {
        throw new SIBBACBusinessException("En el directorio fidessa,  existe mas de un fichero csv.");
      }
    } catch (SIBBACBusinessException e1) {
      throw e1;
    } catch (Exception e) {
      LOG.error("Error metodo getFicheroCSVFidessa " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la busqueda del fichero csv de la carpeta fidessa.");
    }

    sResultado = listaFicheros.get(0);

    return sResultado;
  }

  /**
   * metodo que procesa el fichero seleccionado y devuelve el numero de desgloses. Desglose: Linea que comienza por 40
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> ejecutarFaseDos(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Iniciando ejecución Fase Dos");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    List<tempDTO> lValoresXLSX = new ArrayList<tempDTO>(); // Contiene los valores leidos del fichero excel separados

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    String dateToConcat = dateFormat.format(new Date());

    try {

      final String excelFile = paramsObjects.get("file").toString();
      final String excelFileName = paramsObjects.get("filename").toString();
      byte[] data = Base64.decodeBase64((String) paramsObjects.get("file"));

      String sCabeceraCsvFinal = null;

      InputStream inputStream = new ByteArrayInputStream(data);

      if (excelFile != null) {
        Sheet sheet;
        XSSFWorkbook wbXLSX = new XSSFWorkbook(inputStream);
        sheet = wbXLSX.getSheetAt(0);

        Iterator<Row> rows = sheet.rowIterator();
        Row rowHead = (Row) rows.next();

        // Se comprueba la cabecera.

        Iterator<Cell> celdas = rowHead.cellIterator();
        Cell celda1 = rowHead.getCell(0);
        if (!celda1.getStringCellValue().equals(sValorCabeceraColumna1)) {
          throw new SIBBACBusinessException("La primera columna de la excel no tiene el título: "
                                            + sValorCabeceraColumna1);
        }

        Cell celda2 = rowHead.getCell(1);

        if (!celda2.getStringCellValue().equals(sValorCabeceraColumna2)) {
          throw new SIBBACBusinessException("La segunda columna de la excel no tiene el título: "
                                            + sValorCabeceraColumna2);
        }

        sCabeceraCsvFinal = celda1.getStringCellValue() + ";" + celda2.getStringCellValue();

        // Se leen el resto de celsdas.

        while (rows.hasNext()) {
          Row row = (Row) rows.next();
          celda1 = row.getCell(0);
          if (celda1.getCellType() != Cell.CELL_TYPE_NUMERIC) {
            throw new SIBBACBusinessException("La columna titulo tiene valores que no son numericos ");
          }
          int iValorTitulos = (int) celda1.getNumericCellValue();
          celda2 = row.getCell(1);
          celda2.setCellType(Cell.CELL_TYPE_STRING);
          tempDTO miDto = new tempDTO(iValorTitulos, celda2.getStringCellValue());

          lValoresXLSX.add(miDto);
        }

      } else {
        throw new SIBBACBusinessException("No se ha recibido un fichero válido.");
      }

      // Se procesa el fichero csv resultado de la fase Uno.

      List<tempDTO> lValoresCSV = new ArrayList<tempDTO>(); // Contiene los valores leidos del fichero csv

      String sFicheroCsv = rutaFicherosFidessa + "/" + getFicheroCSVFidessa();

      LOG.debug(sFicheroCsv);

      // Se recorre el csv y se forma un array con los datos de la columna numero de titulos y fichero
      Path pFicheroCSVFase1 = Paths.get(sFicheroCsv);
      String sNombreFichero = pFicheroCSVFase1.getFileName().toString();
      List<String> lineasFichero = Files.readAllLines(pFicheroCSVFase1, Charset.forName(FILE_CHARSET));

      if (lineasFichero.size() > 0) {
        // Se comienza desde la fila 1 porque la primera es la cabecera que no hay que procesar
        for (int i = 1; i < lineasFichero.size(); i++) {
          int iValorTitulos = -1;
          String sFichero = null;

          String sLinea = lineasFichero.get(i);
          StringTokenizer st = new StringTokenizer(sLinea, ";");
          int iColumnna = 1;
          while (st.hasMoreElements()) {
            String sValor = st.nextElement().toString();
            if (iColumnna == 3) { // El Titulo esta en la columna 3;
              iValorTitulos = Integer.valueOf(sValor);
            }
            if (iColumnna == 6) { // El Fichero esta en la columna 6;
              sFichero = sValor;
            }

            iColumnna++;
          }

          lValoresCSV.add(new tempDTO(iValorTitulos, sFichero));

        }
      } else {
        throw new SIBBACBusinessException("Esta vacio el fichero : " + sFicheroCsv);
      }

      if (lValoresCSV.size() != lValoresXLSX.size()) {
        LOG.warn("Numero de ordes distinto en el csv y la excel: CSV Fase 1 - " + lValoresCSV.size()
                 + " Excel cargada - " + lValoresXLSX.size());
        throw new SIBBACBusinessException("La Excel cargada no contiene todas las órdenes del barrido");
      }

      // Los ordeno y los recorro comprobando si el numero de titulos es correcto.
      // Formando la lista que al final se escribira en el fichero.

      Collections.sort(lValoresXLSX);
      Collections.sort(lValoresCSV);

      List<String> lFilasCSVFinal = new ArrayList<String>();

      for (int i = 0; i < lValoresCSV.size(); i++) {
        if (lValoresCSV.get(i).getiNumTitulos() != lValoresXLSX.get(i).getiNumTitulos()) {
          throw new SIBBACBusinessException("La orden: " + lValoresXLSX.get(i).getsValor()
                                            + " no tiene los titulos correctos ");
        } else {
          lFilasCSVFinal.add(lValoresXLSX.get(i).getsValor() + ";" + lValoresCSV.get(i).getsValor());
        }
      }

      // Se crea el fichero csvFinal que va a tratar en la fase2
      String sNombreFicheroTemporal = null;
      BufferedWriter wSalida = null;
      Path pFicheroCSVFase2 = null;
      PrintStream printStream = null;
      try {

        sNombreFicheroTemporal = sNombreFichero.replace(".csv", "_FASE2_") + dateToConcat + ".csv";

        pFicheroCSVFase2 = Paths.get(sRutaPhyton + "/" + sNombreFicheroTemporal);
        pFicheroCSVFase2.toFile().createNewFile();

        printStream = new PrintStream(new FileOutputStream(pFicheroCSVFase2.toFile(), true), true, FILE_CHARSET);
        for (String sCadena : lFilasCSVFinal) {
          printStream.println(sCadena);
        }

        printStream.close();

      } catch (Exception e) {
        LOG.error("Error crear fichero temporal csv " + e.getMessage(), e);
        throw new SIBBACBusinessException(
                                          "Se ha producido al escribir el fichero csv que utiliza phyton en la fase 2 - Revise el log");
      } finally {
        if (printStream != null) {
          printStream.close();
        }
      }

      // Se ejecuta la segunda fase del proceso phyton.

      String sFicheroPY = sRutaPhyton + "//procBarridos.py";
      String sFicheroProcesar = sRutaPhyton + "//" + sNombreFicheroTemporal;

      Path pFicheroProcesarDest = Paths.get(sFicheroProcesar);

      ProcessBuilder pb = new ProcessBuilder("python", sFicheroPY, "-f", "FASE2", "-nf",
                                             pFicheroProcesarDest.toString());
      LOG.debug("Fase 2 -> Inicio");
      Process p = pb.start();

      int iResultado = -1;

      while (iResultado < 0) {
        try {
          iResultado = p.exitValue();
        } catch (Exception e) {
          LOG.debug("Fase 2 -> Todavia se esta ejecutando");
          iResultado = -1;
          Thread.sleep(1000);
        }
      }

      LOG.debug("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseDOS] Resultado proceso: " + iResultado);

      if (iResultado > 0) {
        throw new SIBBACBusinessException("La ejecucion del proceso python ha devuelvo un error - Revise el log");
      }

      // Se procesa la salida.
      BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

      String sLinea = null;

      while ((sLinea = in.readLine()) != null) {
        LOG.debug("[SIBBACServiceCestaOrdenesFidessa :: ejecutarFaseDos] Resultado proceso: " + sLinea);
      }

      Files.move(pFicheroCSVFase1,
                 Paths.get(rutaFicherosTratados + "/"
                           + pFicheroCSVFase1.getFileName().toString().replace(".csv", "_" + dateToConcat + ".csv")),
                 StandardCopyOption.REPLACE_EXISTING);
      Files.move(pFicheroCSVFase2.toAbsolutePath(),
                 Paths.get(rutaFicherosTratados + "/" + pFicheroCSVFase2.getFileName()),
                 StandardCopyOption.REPLACE_EXISTING);

      borraFicherosDirectorio(sRutaPhyton + "/tmp/");
      moverFicherosCarpetaBarridos();

      LOG.debug("Todo se ha ejecutado ok");
      // En este punto todo esta ok y solo tengo que escribir el fichero csv en la carpeta fidesssa

      // String sFicheroCsv = rutaFicherosFidessa + "//" + getFicheroCSVFidessa();

    } catch (SIBBACBusinessException e1) {
      throw e1;
    } catch (Exception e) {
      LOG.error("Error metodo ejecutarFaseDos " + e.getMessage(), e);
      throw new SIBBACBusinessException("Se ha producido un error al ejecutar la Fase Dos, - Revise el log");
    }

    LOG.debug("Fin ejecutarFaseDos");

    return mSalida;
  }

  private Path convertCsvToXmls(String sFileCsv) throws SIBBACBusinessException {
    try {

      Path pFicheroProcesar = Paths.get(sFileCsv);
      Path pResultado = null;
      XSSFWorkbook workBook = new XSSFWorkbook();
      XSSFSheet sheet = workBook.createSheet("sheet1");
      List<String> lineasFichero = Files.readAllLines(pFicheroProcesar, Charset.forName(FILE_CHARSET));
      int RowNum = 0;
      if (lineasFichero.size() > 0) {
        for (int i = 0; i < lineasFichero.size(); i++) {
          XSSFRow currentRow = sheet.createRow(RowNum++);
          String sLinea = lineasFichero.get(i);
          StringTokenizer st = new StringTokenizer(sLinea, ";");
          int iColumnna = 0;
          while (st.hasMoreElements()) {
            String sValor = st.nextElement().toString();
            currentRow.createCell(iColumnna).setCellValue(sValor);
            iColumnna++;
          }
        }
      }

      String sFicheroXslx = sFileCsv.replace(".csv", ".xlsx");
      FileOutputStream fileOutputStream = null;
      fileOutputStream = new FileOutputStream(sFicheroXslx);
      workBook.write(fileOutputStream);
      workBook.close();

      pResultado = Paths.get(sFicheroXslx);
      return pResultado;

    } catch (Exception e) {
      LOG.error("Error metodo convertCsvToXmls " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al convertir el fichero csv resultado de la fase 1.");
    }

  }

  private void borraFicherosDirectorio(String sDirectorio) throws SIBBACBusinessException {
    LOG.debug("Borrarmos ficheros de la carpeta temporal");
    List<String> listaFicheros = new ArrayList<String>();
    try {

      Path pDirectorioBorrar = Paths.get(sDirectorio);
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(pDirectorioBorrar, "*.*");) {
        for (Path entry : stream) {

          if (!Files.isDirectory(entry)) {
            Files.delete(entry);
          }
        }
      }
    } catch (Exception e) {
      LOG.error("Error metodo borraFicherosDirectorio " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en el borrado de los ficheros de la ruta ." + sDirectorio);
    }
    LOG.debug("Fin Borrar ficheros de la carpeta temporal");
  }

  private void moverFicherosCarpetaBarridos() throws SIBBACBusinessException {
    LOG.debug("Movemos Ficheros de la carpeta barridos");
    try {
      Path pDirectorioOrigen = Paths.get(sRutaPhyton + "/barrido/");
      Path pFicherosMover = Paths.get(rutaFicherosFaseUno + "/");
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(pDirectorioOrigen, "*.*");) {
        for (Path entry : stream) {

          if (!Files.isDirectory(entry)) {
            Files.move(entry, Paths.get(pFicherosMover.toString() + "/" + entry.getFileName().toString()),
                       StandardCopyOption.REPLACE_EXISTING);
          }
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo moverFicherosCarpetaBarridos " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al mover los ficheros de barridos .");
    }

    LOG.debug("Fin - Mover ficheros de la carpeta barridos");
  }

  // Clase privada para utilizar los titulos y su valores y poder ordenarlos por numero de titulos.
  private class tempDTO implements Comparable {
    int iNumTitulos;
    String sValor;

    public tempDTO(int iNumTitulos, String sValor) {
      this.iNumTitulos = iNumTitulos;
      this.sValor = sValor;
    }

    public int getiNumTitulos() {
      return iNumTitulos;
    }

    public void setiNumTitulos(int iNumTitulos) {
      this.iNumTitulos = iNumTitulos;
    }

    public String getsValor() {
      return sValor;
    }

    public void setsValor(String sValor) {
      this.sValor = sValor;
    }

    @Override
    public int compareTo(Object o) {

      int iCompareTitulos = ((tempDTO) o).getiNumTitulos();

      return iCompareTitulos - this.iNumTitulos;

    }
  }

}
