package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Arrays;

/**
 * @version 1.0
 * @author XI316153
 */
public class PayLetterDTO implements Serializable {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -6393274807760240753L;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  private Date feejeliq;
  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private Short nucnfliq;
  private String tpoper;
  private String nbvalor;
  private String isin;
  private BigDecimal titulos;
  private BigDecimal efectivo;
  private BigDecimal devolucion;
  private BigDecimal pcdevolucion;
  private String nbtitliq;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * 
   * @param nuorden
   * @param nbooking
   * @param nucnfclt
   * @param nucnfliq
   * @param feejeliq
   * @param titulos
   * @param efectivo
   * @param devolucion
   * @param pcdevolucion
   * @param nbtitliq
   * @param tpoper
   * @param nbvalor
   * @param isin
   */
  public PayLetterDTO(String nuorden,
                      String nbooking,
                      String nucnfclt,
                      Short nucnfliq,
                      Date feejeliq,
                      BigDecimal titulos,
                      BigDecimal efectivo,
                      BigDecimal devolucion,
                      BigDecimal pcdevolucion,
                      String nbtitliq,
                      Character tpoper,
                      String nbvalor,
                      String isin) {
    this.nuorden = nuorden.trim();
    this.nbooking = nbooking.trim();
    this.nucnfclt = nucnfclt.trim();
    this.nucnfliq = nucnfliq;
    this.feejeliq = feejeliq;
    this.titulos = titulos;
    this.efectivo = efectivo;
    this.devolucion = devolucion;
    this.pcdevolucion = pcdevolucion;
    this.nbtitliq = nbtitliq;
    this.tpoper = String.valueOf(tpoper).trim();
    this.nbvalor = nbvalor.trim();
    this.isin = isin.trim();
  } // PayLetterDTO

  /**
   * 
   * @param tmct0alc
   */
  public PayLetterDTO(Object[] tmct0alcArg) {
	Object[] tmct0alc = Arrays.copyOf(tmct0alcArg, tmct0alcArg.length);
	this.nuorden = String.valueOf(tmct0alc[0]).trim();
	this.nbooking = String.valueOf(tmct0alc[1]).trim();
	this.nucnfclt = String.valueOf(tmct0alc[2]).trim();
	this.nucnfliq = Short.valueOf(tmct0alc[3].toString());
	this.feejeliq = (Date) tmct0alc[4];
	this.titulos = new BigDecimal(tmct0alc[5].toString());
	this.efectivo = new BigDecimal(tmct0alc[6].toString());
	this.devolucion = new BigDecimal(tmct0alc[7].toString());
	this.pcdevolucion = new BigDecimal(tmct0alc[8].toString());
	this.nbtitliq = String.valueOf(tmct0alc[9]).trim();
	this.tpoper = String.valueOf(tmct0alc[10]).trim();
	this.nbvalor = String.valueOf(tmct0alc[11]).trim();
	this.isin = String.valueOf(tmct0alc[12]).trim();
  } // PayLetterDTO

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "PayLetterDTO [feejeliq=" + feejeliq + ", nuorden=" + nuorden + ", nbooking=" + nbooking + ", nucnfclt=" + nucnfclt + ", nucnfliq="
           + nucnfliq + ", tpoper=" + tpoper + ", nbvalor=" + nbvalor + ", isin=" + isin + ", titulos=" + titulos + ", efectivo=" + efectivo
           + ", devolucion=" + devolucion + ", pcdevolucion=" + pcdevolucion + ", nbtitliq=" + nbtitliq + "]";
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

  /**
   * @return the feejeliq
   */
  public Date getFeejeliq() {
    return feejeliq;
  }

  /**
   * @return the nuorden
   */
  public String getNuorden() {
    return nuorden;
  }

  /**
   * @return the nbooking
   */
  public String getNbooking() {
    return nbooking;
  }

  /**
   * @return the nucnfclt
   */
  public String getNucnfclt() {
    return nucnfclt;
  }

  /**
   * @return the nucnfliq
   */
  public Short getNucnfliq() {
    return nucnfliq;
  }

  /**
   * @return the tpoper
   */
  public String getTpoper() {
    return tpoper;
  }

  /**
   * @return the nbvalor
   */
  public String getNbvalor() {
    return nbvalor;
  }

  /**
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * @return the titulos
   */
  public BigDecimal getTitulos() {
    return titulos;
  }

  /**
   * @return the efectivo
   */
  public BigDecimal getEfectivo() {
    return efectivo;
  }

  /**
   * @return the devolucion
   */
  public BigDecimal getDevolucion() {
    return devolucion;
  }

  /**
   * @return the pcdevolucion
   */
  public BigDecimal getPcdevolucion() {
    return pcdevolucion;
  }

  /**
   * @return the nbtitliq
   */
  public String getNbtitliq() {
    return nbtitliq;
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SETTERS

  /**
   * @param feejeliq the feejeliq to set
   */
  public void setFeejeliq(Date feejeliq) {
    this.feejeliq = feejeliq;
  }

  /**
   * @param nuorden the nuorden to set
   */
  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  /**
   * @param nbooking the nbooking to set
   */
  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  /**
   * @param nucnfclt the nucnfclt to set
   */
  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  /**
   * @param nucnfliq the nucnfliq to set
   */
  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  /**
   * @param tpoper the tpoper to set
   */
  public void setTpoper(String tpoper) {
    this.tpoper = tpoper;
  }

  /**
   * @param nbvalor the nbvalor to set
   */
  public void setNbvalor(String nbvalor) {
    this.nbvalor = nbvalor;
  }

  /**
   * @param isin the isin to set
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * @param titulos the titulos to set
   */
  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  /**
   * @param efectivo the efectivo to set
   */
  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  /**
   * @param devolucion the devolucion to set
   */
  public void setDevolucion(BigDecimal devolucion) {
    this.devolucion = devolucion;
  }

  /**
   * @param pcdevolucion the pcdevolucion to set
   */
  public void setPcdevolucion(BigDecimal pcdevolucion) {
    this.pcdevolucion = pcdevolucion;
  }

  /**
   * @param nbtitliq the nbtitliq to set
   */
  public void setNbtitliq(String nbtitliq) {
    this.nbtitliq = nbtitliq;
  }

} // PayLetterDTO
