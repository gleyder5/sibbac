package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0mscId;
import sibbac.business.wrappers.database.model.Tmct0msc;


@Repository
public interface Tmct0mscDao extends JpaRepository< Tmct0msc, Tmct0mscId > {

}
