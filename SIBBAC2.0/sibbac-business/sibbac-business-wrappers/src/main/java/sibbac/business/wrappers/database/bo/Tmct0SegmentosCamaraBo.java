package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0SegmentosCamaraDao;
import sibbac.business.wrappers.database.model.Tmct0SegmentosCamara;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0SegmentosCamaraBo extends AbstractBo< Tmct0SegmentosCamara, Long, Tmct0SegmentosCamaraDao > {

}
