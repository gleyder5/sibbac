package sibbac.business.wrappers.database.bo.bancaPrivadaReader;

import java.util.Date;

public class HBB7RecordBean extends HBBNRecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 3223274589183334345L;

  private Date fechaCuadre;
  private IndicadorInicioFinalHBB indicadorInicioFinal;

  private String nuorden;

  public Date getFechaCuadre() {
    return fechaCuadre;
  }

  public void setFechaCuadre(Date fechaCuadre) {
    this.fechaCuadre = fechaCuadre;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public IndicadorInicioFinalHBB getIndicadorInicioFinal() {
    return indicadorInicioFinal;
  }

  public void setIndicadorInicioFinal(IndicadorInicioFinalHBB indicadorInicioFinal) {
    this.indicadorInicioFinal = indicadorInicioFinal;
  }

}
