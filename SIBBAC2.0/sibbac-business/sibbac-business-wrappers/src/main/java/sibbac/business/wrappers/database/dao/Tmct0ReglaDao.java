package sibbac.business.wrappers.database.dao;


import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0Regla;


@Repository
public interface Tmct0ReglaDao extends JpaRepository< Tmct0Regla, BigInteger > {
	
	@Query("SELECT regla FROM Tmct0Regla regla WHERE regla.idRegla=:idRegla ")
	public Tmct0Regla findIdRegla(@Param("idRegla") BigInteger idRegla );

	@Query("SELECT regla FROM Tmct0Regla regla WHERE regla.cdCodigo=:cdCodigo ")
	public Tmct0Regla findByCdCodigo(@Param("cdCodigo") String cdCodigo );
	
	
	@Query("SELECT regla FROM Tmct0Regla regla WHERE regla.idRegla=:idRegla AND regla.activo = '1' ")
  Tmct0Regla findIdReglaActivo(@Param("idRegla") BigInteger idRegla );
	
	@Query("SELECT regla FROM Tmct0Regla regla WHERE regla.activo = '1' ORDER BY regla.cdCodigo ASC, regla.nbDescripcion ASC")
  List<Tmct0Regla> findAllByActivo();
	
}
