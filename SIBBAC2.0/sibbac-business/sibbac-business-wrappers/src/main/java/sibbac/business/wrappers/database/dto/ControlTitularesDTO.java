package sibbac.business.wrappers.database.dto;

import java.io.Serializable;
import java.util.Date;

import sibbac.business.fase0.database.model.Tmct0aloId;

public class ControlTitularesDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -32562336644006232L;
  private final Character mercado;
  private final Tmct0aloId alloId;
  private final Short nucnfliq;
  private final Date fechaLiquidacion;
  private final String ccv;
  private final Integer estadoTitularidad;

  private final String clienteRouting;
  private final Character isScriptDividend;

  public ControlTitularesDTO(Character mercado, Tmct0aloId alloId, Short nucnfliq, Date fechaLiquidacion, String ccv,
      Integer estadoTitularidad, String clienteRouting, Character isScriptDividend) {
    super();
    this.mercado = mercado;
    this.alloId = alloId;
    this.nucnfliq = nucnfliq;
    this.fechaLiquidacion = fechaLiquidacion;
    this.ccv = ccv;
    this.estadoTitularidad = estadoTitularidad;
    this.clienteRouting = clienteRouting;
    this.isScriptDividend = isScriptDividend;
  }

  public ControlTitularesDTO(Character mercado, String nuorden, String nbooking, String nucnfclt, short nucnfliq,
      Date fechaLiquidacion, String ccv, Integer estadoTitularidad, String clienteRouting, Character isScriptDividend) {
    this(mercado, new Tmct0aloId(nucnfclt, nbooking, nuorden), nucnfliq, fechaLiquidacion, ccv, estadoTitularidad,
        clienteRouting, isScriptDividend);

  }

  public ControlTitularesDTO(Character mercado, Tmct0aloId alloId, Date fechaLiquidacion, String ccv,
      Integer estadoTitularidad, String clienteRouting, Character isScriptDividend) {
    this(mercado, alloId, (short) 0, fechaLiquidacion, ccv, estadoTitularidad, clienteRouting, isScriptDividend);
  }

  public Character getMercado() {
    return mercado;
  }

  public Tmct0aloId getAlloId() {
    return alloId;
  }

  public short getNucnfliq() {
    return nucnfliq;
  }

  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  public String getCcv() {
    return ccv;
  }

  public Integer getEstadoTitularidad() {
    return estadoTitularidad;
  }

  public String getClienteRouting() {
    return clienteRouting;
  }

  public Character getIsScriptDividend() {
    return isScriptDividend;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((alloId == null) ? 0 : alloId.hashCode());
    result = prime * result + ((ccv == null) ? 0 : ccv.hashCode());
    result = prime * result + ((clienteRouting == null) ? 0 : clienteRouting.hashCode());
    result = prime * result + ((estadoTitularidad == null) ? 0 : estadoTitularidad.hashCode());
    result = prime * result + ((fechaLiquidacion == null) ? 0 : fechaLiquidacion.hashCode());
    result = prime * result + ((isScriptDividend == null) ? 0 : isScriptDividend.hashCode());
    result = prime * result + ((mercado == null) ? 0 : mercado.hashCode());
    result = prime * result + ((nucnfliq == null) ? 0 : nucnfliq.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ControlTitularesDTO other = (ControlTitularesDTO) obj;
    if (alloId == null) {
      if (other.alloId != null)
        return false;
    }
    else if (!alloId.equals(other.alloId))
      return false;
    if (ccv == null) {
      if (other.ccv != null)
        return false;
    }
    else if (!ccv.equals(other.ccv))
      return false;
    if (clienteRouting == null) {
      if (other.clienteRouting != null)
        return false;
    }
    else if (!clienteRouting.equals(other.clienteRouting))
      return false;
    if (estadoTitularidad == null) {
      if (other.estadoTitularidad != null)
        return false;
    }
    else if (!estadoTitularidad.equals(other.estadoTitularidad))
      return false;
    if (fechaLiquidacion == null) {
      if (other.fechaLiquidacion != null)
        return false;
    }
    else if (!fechaLiquidacion.equals(other.fechaLiquidacion))
      return false;
    if (isScriptDividend == null) {
      if (other.isScriptDividend != null)
        return false;
    }
    else if (!isScriptDividend.equals(other.isScriptDividend))
      return false;
    if (mercado == null) {
      if (other.mercado != null)
        return false;
    }
    else if (!mercado.equals(other.mercado))
      return false;
    if (nucnfliq == null) {
      if (other.nucnfliq != null)
        return false;
    }
    else if (!nucnfliq.equals(other.nucnfliq))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "ControlTitularesDTO [mercado=" + mercado + ", alloId=" + alloId + ", nucnfliq=" + nucnfliq
        + ", fechaLiquidacion=" + fechaLiquidacion + ", ccv=" + ccv + ", estadoTitularidad=" + estadoTitularidad
        + ", clienteRouting=" + clienteRouting + ", isScriptDividend=" + isScriptDividend + "]";
  }

}
