package sibbac.business.wrappers.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.dto.Tmct0actDTO;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACServiceTmct0Act implements SIBBACServiceBean{
	@Autowired
	private Tmct0actBo				tmct0actBo;

	public static final String	CMD_GET_SUBCTA						= "getListaSubCta";
	private static final String	RESULTADO_LISTA_SUBCTA				= "listaSubCta";
	private static final String	FILTER_ALIAS 						= "alias";
	
	
	private static final Logger	LOG									= LoggerFactory.getLogger( SIBBACServiceAlias.class );
	
	public SIBBACServiceTmct0Act() {
		

	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		LOG.debug( "[SIBBACServiceAlias::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// obtiene comandos(paths)
		final String action = webRequest.getAction();
		LOG.debug( "[SIBBACServiceTmct0Act::process] " + "+ Command.: [{}]", action );

		switch ( action ) {
			case SIBBACServiceTmct0Act.CMD_GET_SUBCTA:
				this.getSubCta( webRequest, webResponse );
				break;
			default:
				webResponse.setError( "No recibe ningun comando" );
				LOG.error( "No le llega ningun comando" );
				break;
		}
		
		return webResponse;
	}
	
	
	private void getSubCta( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		try{
			Map< String, String > filters = webRequest.getFilters();
			final Map< String, List< Tmct0actDTO >> resultados = new HashMap< String, List< Tmct0actDTO >>();
			final List< Tmct0actDTO > lista = new ArrayList< Tmct0actDTO >();
			String alias =  filters.get( FILTER_ALIAS );
			
			List< Tmct0act > resul = null;
			resul =  tmct0actBo.getListaSubCta(alias);
			if(resul != null){
				for ( Tmct0act obj :  resul ){
					Tmct0actDTO actDTO = new Tmct0actDTO(obj.getCdsubcta(), obj.getCdbroclifacturacion());
					actDTO.setDesacrali(obj.getDescrali());
					lista.add(actDTO);
					
				}
			}
			resultados.put( RESULTADO_LISTA_SUBCTA, lista );
			webResponse.setResultados( resultados );
		}catch(Exception ex){
			LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
		}
	}

	@Override
	public List<String> getAvailableCommands() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getFields() {
		// TODO Auto-generated method stub
		return null;
	}	
	
}
