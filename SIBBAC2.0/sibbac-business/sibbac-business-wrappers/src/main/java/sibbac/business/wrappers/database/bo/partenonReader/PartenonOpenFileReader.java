package sibbac.business.wrappers.database.bo.partenonReader;

import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.common.fileReader.SibbacFixedLengthFileReader;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;

/**
 * Reader para el fichero de titulares de Partenón.
 * 
 * @author XI316153
 * @see SibbacFixedLengthFileReader
 * @see OperacionEspecialRecordBean
 */
@Component
public class PartenonOpenFileReader extends PartenonFileReader {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(PartenonOpenFileReader.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * Constructor I. LLama al constructor de la clase padre.
   */
  public PartenonOpenFileReader() {
    super();
  } // PartenonOpenFileReader

  /**
   * Constructor II. Llama al constructor de la clase padre pasandole el fichero a procesar.
   * 
   * @param fileName Ruta absoluta al fichero a procesar.
   */
  public PartenonOpenFileReader(String fileName) {
    super();
    file = Paths.get(fileName);
  } // PartenonOpenFileReader

} // PartenonOpenFileReader
