package sibbac.business.wrappers.rest;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;

@SIBBACService
public class SIBBACUrlManagerService implements SIBBACServiceBean {
    private static final Logger LOG = LoggerFactory
	    .getLogger(SIBBACUrlManagerService.class);
    private static final String GET_SIBBAC_URL = "getSibbacUrl";
    private static final String RESULT_URL = "result_url";
    @Autowired
    private Tmct0cfgBo bo;

    @Override
    public WebResponse process(WebRequest webRequest)
	    throws SIBBACBusinessException {
	WebResponse response = new WebResponse();
	switch (webRequest.getAction()) {
	case GET_SIBBAC_URL:
	    getSibbacUrl(response, webRequest.getFilters());
	    break;
	}
	return response;
    }

    @Override
    public List<String> getAvailableCommands() {
	List<String> commands = new LinkedList<String>();
	commands.add(GET_SIBBAC_URL);
	return commands;
    }

    @Override
    public List<String> getFields() {
	// TODO Auto-generated method stub
	return null;
    }

    private void getSibbacUrl(WebResponse response, Map<String, String> filter) {
	if (filter == null || filter.get("aplication") == null
		|| filter.get("process") == null
		|| filter.get("keyname") == null) {
	    response.setError("Por favor mande los parámetros requeridos: aplication, process y keyname");
	} else {
	    String aplication = filter.get("aplication");
	    String process = filter.get("process");
	    String keyname = filter.get("keyname");
	    try {
		Map<String, Object> result = new HashMap<String, Object>();
		Tmct0cfg cfg = bo.findByAplicationAndProcessAndKeyname(
			aplication, process, keyname);
		result.put(RESULT_URL, cfg.getKeyvalue());
		response.setResultados(result);
	    } catch (Exception ex) {
		LOG.error(ex.getMessage(), ex);
		response.setError(ex.getMessage());
	    }
	}
    }

}
