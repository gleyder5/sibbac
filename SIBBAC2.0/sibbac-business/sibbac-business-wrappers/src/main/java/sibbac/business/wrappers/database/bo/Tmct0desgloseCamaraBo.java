package sibbac.business.wrappers.database.bo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0desgloseCamaraDao;
import sibbac.business.wrappers.database.dao.Tmct0desgloseCamaraDaoImpl;
import sibbac.business.wrappers.database.dao.Tmct0infocompensacionDao;
import sibbac.business.wrappers.database.data.Tmct0desgloseCamaraData;
import sibbac.business.wrappers.database.dto.ActualizacionCodigosOperacionEccDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0bokIdDTO;
import sibbac.business.wrappers.database.model.Tmct0DesgloseCamaraEnvioRt;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0desglosecamara;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0infocompensacion;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;
import sibbac.business.wrappers.database.model.Tmct0movimientooperacionnuevaecc;
import sibbac.common.helper.CloneObjectHelper;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0desgloseCamaraBo extends AbstractBo<Tmct0desglosecamara, Long, Tmct0desgloseCamaraDao> {

  @Autowired
  private Tmct0desgloseCamaraDaoImpl daoImpl;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  @Autowired
  private Tmct0infocompensacionDao icDao;

  @Autowired
  private Tmct0movimientoeccBo movBo;

  @Autowired
  private Tmct0movimientooperacionnuevaeccBo movNuevaBo;

  @Autowired
  private Tmct0alcBo tmct0alcBo;

  public List<Object[]> findAllFContratacionCdisinSentidoByDesglosesAltaAndCasadoptiAndMovimientoFidessaConTitulosPendientes() {
    return dao.findAllFContratacionCdisinSentidoByDesglosesAltaAndCasadoptiAndMovimientoFidessaConTitulosPendientes();
  }

  public List<Tmct0alc> findAllAlcsByFcontratacionAndCdisinAndSentidoAndCdestadoasigLt(Date fecontratacion,
      String cdisin, char sentido, int cdestadoasigLt) {
    return dao.findAllAlcsByFcontratacionAndCdisinAndSentidoAndCdestadoasigLt(fecontratacion, cdisin, sentido,
        cdestadoasigLt);
  }

  public Tmct0desglosecamara findFirst1Bytmct0alc(Tmct0alc tmct0alc) {
    return dao.findFirst1Bytmct0alc(tmct0alc);
  }

  public Tmct0desglosecamara findByMovimientooperacionnuevaecc(
      Tmct0movimientooperacionnuevaecc movimientooperacionnuevaecc) {
    return dao.findByMovimientooperacionnuevaecc(movimientooperacionnuevaecc);
  }

  public int countDesglosesCamaraByAlcIdAndCdestadoasig(Tmct0alcId alcId, int cdestadoasig) {
    return dao.countDesglosesCamaraByAlcIdAndCdestadoasig(alcId, cdestadoasig);
  }

  public int countDesglosesCamaraByAlcIdAndCdestadoasigAndAlta(Tmct0alcId alcId) {
    return dao.countDesglosesCamaraByAlcIdAndCdestadoasigAndAlta(alcId);
  }

  public int countDesglosesCamaraByAlcIdAndCdestadoasigLteAndDistinctIddesgloseCamara(Tmct0alcId alcId,
      int cdestadoasigLte, long iddesglosecamara) {
    return dao.countDesglosesCamaraByAlcIdAndCdestadoasigLteAndDistinctIddesgloseCamara(alcId, cdestadoasigLte,
        iddesglosecamara);
  }

  public List<Tmct0desgloseCamaraData> obtenerDesglocesCamaraAgrupadosPorSentido(char sentido, int estado1,
      int estado2, Map<String, Serializable> filtros) {
    return daoImpl.getDesglosesCamaraBySentidoAndEstado(sentido, estado1, estado2, filtros);
  }

  public List<ActualizacionCodigosOperacionEccDTO> findAllDatesCdisinCdsentidoByForActualizacionCodigosOperacionEcc(
      int estadoAsignacion, char isSd) {
    return dao.findAllDatesCdisinCdsentidoByCdestadoasig(estadoAsignacion, isSd);
  }

  public ActualizacionCodigosOperacionEccDTO findAllBookingIdByDesglosesSinTitulosDiponiblesAnotacion(
      ActualizacionCodigosOperacionEccDTO data, int cdestadoasig) {
    data.getBookings().addAll(
        dao.findAllBookingIdByDesglosesSinTitulosDiponiblesAnotacion(data.getFcontratacion(), data.getCdisin(),
            data.getCdsentido(), cdestadoasig));
    return data;
  }

  public List<Tmct0desgloseCamaraData> getDesglosesCamaraGroupByIsin(char sentido, int estado1, int estado2,
      Map<String, Serializable> filtros) {
    List<Tmct0desgloseCamaraData> desgloses = daoImpl.getDesglosesCamaraBySentidoAndEstado(sentido, estado1, estado2,
        filtros);
    List<Tmct0desgloseCamaraData> agrupados = new ArrayList<Tmct0desgloseCamaraData>();
    List<Tmct0desgloseCamaraData> copyDesgloses = new CopyOnWriteArrayList<Tmct0desgloseCamaraData>();
    copyDesgloses.addAll(desgloses);

    for (int i = 0; i < desgloses.size(); i++) {
      //
      Tmct0desgloseCamaraData parent = desgloses.get(i);
      if (parent.getIsin() == null || isinInList(parent.getIsin(), parent.getCodCtaDeCompensacion(), agrupados)) {
        continue;
      }
      // total efectivos
      BigDecimal efectivos = (parent.getEfectivo() == null) ? BigDecimal.ZERO : parent.getEfectivo();
      // total corretaje
      BigDecimal totalCorretajes = (parent.getCorretaje() == null) ? BigDecimal.ZERO : parent.getCorretaje();
      // canones
      BigDecimal imcanoncompeu = (parent.getImcanoncompeu() == null) ? BigDecimal.ZERO : parent.getImcanoncompeu();
      BigDecimal imcanoncontreu = (parent.getImcanoncontreu() == null) ? BigDecimal.ZERO : parent.getImcanoncontreu();
      BigDecimal imcanonliqeu = (parent.getImcanonliqeu() == null) ? BigDecimal.ZERO : parent.getImcanonliqeu();

      for (int z = i + 1; z < copyDesgloses.size(); z++) {
        Tmct0desgloseCamaraData comparable = desgloses.get(z);
        if (parent.getIsin() != null && comparable.getIsin() != null && parent.getIsin().equals(comparable.getIsin())) {
          if (parent.getCodCtaDeCompensacion() != null && comparable.getCodCtaDeCompensacion() != null
              && parent.getCodCtaDeCompensacion().equals(comparable.getCodCtaDeCompensacion())) {

            BigDecimal toSum = BigDecimal.ZERO;
            toSum = (comparable.getEfectivo() != null) ? comparable.getEfectivo() : toSum;
            efectivos = efectivos.add(toSum);
            BigDecimal sumCorretaje = BigDecimal.ZERO;
            sumCorretaje = (comparable.getCorretaje() != null) ? comparable.getCorretaje() : sumCorretaje;
            totalCorretajes = totalCorretajes.add(sumCorretaje);

            // comparacion de alc canones
            // si el desgloseCamara que pasa por aqui tiene distinto alc
            // se suman los canones
            if (!parent.getIdAlc().equals(comparable.getIdAlc())) {
              imcanoncompeu = imcanoncompeu.add((comparable.getImcanoncompeu() != null) ? comparable.getImcanoncompeu()
                  : BigDecimal.ZERO);
              imcanoncontreu = imcanoncontreu.add((comparable.getImcanoncontreu() != null) ? comparable
                  .getImcanoncontreu() : BigDecimal.ZERO);
              imcanonliqeu = imcanonliqeu.add((comparable.getImcanonliqeu() != null) ? comparable.getImcanonliqeu()
                  : BigDecimal.ZERO);
            }
          }
        }// Cambio esta llave para que solo acumule los canones por isin y
         // cuenta.
      }
      parent.setImcanoncompeu(imcanoncompeu);
      parent.setImcanoncontreu(imcanoncontreu);
      parent.setImcanonliqeu(imcanonliqeu);
      //
      try {
        parent.setEfectivo(efectivos);
        parent.setCorretaje(totalCorretajes);
        calcularTotales(parent);
      }
      catch (NullPointerException ex) {
        LOG.error(ex.getMessage(), ex);
      }
      agrupados.add(parent);
    }
    return agrupados;
  }

  private void calcularTotales(Tmct0desgloseCamaraData parent) throws NullPointerException {
    BigDecimal totalCanones = parent.getImcanoncompeu().add(parent.getImcanoncontreu()).add(parent.getImcanonliqeu());
    BigDecimal sumCanonsYEfectivos = parent.getEfectivo().add(totalCanones);
    parent.setTotalCanones(totalCanones);
    BigDecimal sumCorretajeYEfectivo = parent.getCorretaje().add(parent.getEfectivo());
    parent.setSumaCanonesYEfectivos(sumCanonsYEfectivos);
    parent.setTotalCorretajeYEfectivo(sumCorretajeYEfectivo);
  }

  private boolean isinInList(String isin, String codCtaDeCompensacion, List<Tmct0desgloseCamaraData> agrupados) {
    boolean isInList = false;
    for (Tmct0desgloseCamaraData data : agrupados) {
      if (data.getIsin().equals(isin) && codCtaDeCompensacion.equals(data.getCodCtaDeCompensacion())) {
        isInList = true;
        break;
      }
    }
    return isInList;
  }

  /**
   * 
   * Actualiza el desglose camara a casadopti='S'
   * 
   * @param nuorden
   * @param nbooking
   */
  public Integer updateFlagCasadoPti(String nuorden, String nbooking) {
    return dao.updateFlagCasadoPti(nuorden, nbooking, new Date());
  }

  /**
   * Devuelve la lista de bookings que contienen desgloses camara sin casar
   * */
  public List<Tmct0bokIdDTO> findBookingsIdConDesglosesCamaraSinCasar(Date sinceDate) {
    return dao.findBookingsIdConDesglosesCamaraSinCasar(sinceDate);
  }

  public List<Tmct0desglosecamara> findAllByTmct0alcIdAndCdestadoasigAndInCamarasCompensacion(Tmct0alcId alcId,
      int cdestadoasig, List<String> camarasCompensacion) {
    return dao.findAllByTmct0alcIdAndCdestadoasigAndInCamarasCompensacion(alcId, cdestadoasig, camarasCompensacion);
  }

  public List<Tmct0desglosecamara> findAllByTmct0alcActivosIdAndCdestadoasigAndInCamarasCompensacion(Tmct0alcId alcId,
      List<String> camarasCompensacion) {

    return dao.findAllByTmct0alcActivosIdAndCdestadoasigAndInCamarasCompensacion(alcId, camarasCompensacion);
  }

  public List<Tmct0desglosecamara> findAllByTmct0alcActivosIdAndCdestadoasig(Tmct0alcId alcId) {
    return dao.findAllByTmct0alcActivosIdAndCdestadoasig(alcId);
  }

  public BigDecimal getSumNutitulosByTmct0alcId(Tmct0alcId alcId) {
    return dao.getSumNutitulosByTmct0alcId(alcId);
  }

  public List<Tmct0desglosecamara> findAllByTmct0alcId(Tmct0alcId alcId) {
    return dao.findAllByTmct0alcId(alcId);
  }

  public List<Tmct0alcId> findAllAlcsWithDesglosesPdteAsignar() {
    List<Object[]> l = dao
        .findAllAlcIdByFeliquidacionGtAndCdestadoasigAndCamraCompensacionAndCasadoptiAndCanonCalculado(new Date(),
            EstadosEnumerados.ASIGNACIONES.PDTE_ENVIAR.getId());

    List<Tmct0alcId> res = new ArrayList<Tmct0alcId>();
    for (Object[] o : l) {
      res.add(new Tmct0alcId((String) o[1], (String) o[0], ((BigDecimal) o[3]).shortValue(), (String) o[2]));
    }
    return res;
  }

  public Integer updateCdestadotitByTmct0alcId(Tmct0alcId alcId, int cdestadoatit) {
    return dao.updateCdestadotitByTmct0alcId(alcId, cdestadoatit, new Date());
  }

  public Integer updateCdestadotitByTmct0aloId(Tmct0aloId alcId, int cdestadoatit) {
    return dao.updateCdestadotitByTmct0aloId(alcId, cdestadoatit, new Date());
  }

  public void persistAllInOrden(Tmct0desglosecamara dc) {
    List<Tmct0movimientooperacionnuevaecc> movsn;
    icDao.save(dc.getTmct0infocompensacion());
    List<Tmct0desgloseclitit> dcts = new ArrayList<Tmct0desgloseclitit>(dc.getTmct0desgloseclitits());
    dc.getTmct0desgloseclitits().clear();
    List<Tmct0movimientoecc> movs = new ArrayList<Tmct0movimientoecc>(dc.getTmct0movimientos());
    dc.getTmct0movimientos().clear();

    dc = save(dc);
    dcts = dctBo.save(dcts);
    for (Tmct0movimientoecc mov : movs) {
      movsn = new ArrayList<Tmct0movimientooperacionnuevaecc>(mov.getTmct0movimientooperacionnuevaeccs());
      mov.getTmct0movimientooperacionnuevaeccs().clear();
      mov = movBo.save(mov);
      movsn = movNuevaBo.save(movsn);
    }
  }

  public Tmct0desglosecamara inizializeTmct0DesgloseCamaraRechazado(Tmct0desglosecamara original) {
    Tmct0desglosecamara dcBaja = new Tmct0desglosecamara();
    CloneObjectHelper.copyBasicFields(original, dcBaja, null);
    dcBaja.setIddesglosecamara(null);
    dcBaja.setTmct0DesgloseCamaraEnvioRt(new ArrayList<Tmct0DesgloseCamaraEnvioRt>());
    dcBaja.setTmct0desgloseclitits(new ArrayList<Tmct0desgloseclitit>());
    dcBaja.setTmct0movimientos(new ArrayList<Tmct0movimientoecc>());
    Tmct0infocompensacion ic = new Tmct0infocompensacion();
    CloneObjectHelper.copyBasicFields(original.getTmct0infocompensacion(), ic, null);
    ic.setIdinfocomp(null);
    dcBaja.setTmct0infocompensacion(ic);
    dcBaja.setNutitulos(BigDecimal.ZERO);
    dcBaja.setTmct0estadoByCdestadoasig(new Tmct0estado());
    dcBaja.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.RECHAZADA.getId());
    dcBaja.setTmct0estadoByCdestadocont(new Tmct0estado());
    dcBaja.getTmct0estadoByCdestadocont().setIdestado(EstadosEnumerados.CONTABILIDAD.PDTE_DEVENGO.getId());
    dcBaja.setTmct0estadoByCdestadoentrec(new Tmct0estado());
    dcBaja.getTmct0estadoByCdestadoentrec().setIdestado(EstadosEnumerados.CLEARING.PDTE_FICHERO.getId());
    dcBaja.setTmct0estadoByCdestadotit(new Tmct0estado());
    dcBaja.getTmct0estadoByCdestadotit().setIdestado(EstadosEnumerados.TITULARIDADES.RECHAZADA.getId());
    return dcBaja;
  }

  public boolean isTitulosDCOk(Tmct0alc alc) {
    BigDecimal nutitulosDc = dao.getSumNutitulosByTmct0alcId(alc.getId());
    return alc.getNutitliq() != null && nutitulosDc != null && alc.getNutitliq().compareTo(nutitulosDc) == 0;
  }

  @Transactional
  public void ponerEstadoAsignacion(Tmct0desglosecamara dc, int estadoAsignacion, String auditUser) {
    Tmct0alc alc = dc.getTmct0alc();
    Tmct0alo alo = alc.getTmct0alo();
    Tmct0bok bok = alo.getTmct0bok();
    if (dc.getTmct0estadoByCdestadoasig().getIdestado() != estadoAsignacion) {
      dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
      dc.getTmct0estadoByCdestadoasig().setIdestado(estadoAsignacion);
      dc.setAuditFechaCambio(new Date());
      dc.setAuditUser(auditUser);
    }
    if (alc.getCdestadoasig() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      alc.setFhaudit(new Date());
      alc.setCdusuaud(auditUser);
    }
    if (alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
      alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      alo.setFhaudit(new Date());
      alo.setCdusuaud(auditUser);
    }
    if (bok.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      bok.setTmct0estadoByCdestadoasig(new Tmct0estado());
      bok.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      bok.setFhaudit(new Date());
      bok.setCdusuaud(auditUser);
    }
  }

  @Transactional
  public void ponerEstadoAsignacionUpToAllo(Tmct0desglosecamara dc, int estadoAsignacion, String auditUser) {
    Tmct0alc alc = dc.getTmct0alc();
    Tmct0alo alo = alc.getTmct0alo();
    if (dc.getTmct0estadoByCdestadoasig().getIdestado() != estadoAsignacion) {
      dc.setTmct0estadoByCdestadoasig(new Tmct0estado());
      dc.getTmct0estadoByCdestadoasig().setIdestado(estadoAsignacion);
      dc.setAuditFechaCambio(new Date());
      dc.setAuditUser(auditUser);
    }
    if (alc.getCdestadoasig() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      alc.setCdestadoasig(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      alc.setFhaudit(new Date());
      alc.setCdusuaud(auditUser);
    }
    if (alo.getTmct0estadoByCdestadoasig().getIdestado() != EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId()) {
      alo.setTmct0estadoByCdestadoasig(new Tmct0estado());
      alo.getTmct0estadoByCdestadoasig().setIdestado(EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId());
      alo.setFhaudit(new Date());
      alo.setCdusuaud(auditUser);
    }
  }

}
