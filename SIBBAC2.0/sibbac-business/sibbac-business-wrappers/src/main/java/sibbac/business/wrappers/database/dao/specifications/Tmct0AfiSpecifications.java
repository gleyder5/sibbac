package sibbac.business.wrappers.database.dao.specifications;


import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import sibbac.business.wrappers.database.model.Tmct0afi;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0ord;


public class Tmct0AfiSpecifications {

	protected static final Logger	LOG	= LoggerFactory.getLogger( Tmct0AfiSpecifications.class );

	public static Specification< Tmct0afi > nudnicif( final String nudnicif ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nudnicifPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::nudnicif] [nudnicif=={}]", nudnicif );
				if ( nudnicif != null ) {
					nudnicifPredicate = builder.equal( root.< String > get( "nudnicif" ), nudnicif );
				}
				return nudnicifPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > nbclient( final String nbclient ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbclientPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::nbclient] [nbclient=={}]", nbclient );
				if ( nbclient != null ) {
					nbclientPredicate = builder.equal( root.< String > get( "nbclient" ), nbclient );
				}
				return nbclientPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > nbclient1( final String nbclient1 ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbclientPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::nbclient1] [nbclien1t=={}]", nbclient1 );
				if ( nbclient1 != null ) {
					nbclientPredicate = builder.equal( root.< String > get( "nbclient1" ), nbclient1 );
				}
				return nbclientPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > nbclient2( final String nbclient2 ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nbclientPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::nbclient2] [nbclient2=={}]", nbclient2 );
				if ( nbclient2 != null ) {
					nbclientPredicate = builder.equal( root.< String > get( "nbclient2" ), nbclient2 );
				}
				return nbclientPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > nbValors( final String nbValors ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nudnicifPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::nbvalors] [nbvalors=={}]", nbValors );
				if ( nbValors != null ) {
					nudnicifPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "nbvalors" ), nbValors );
				}
				return nudnicifPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > sentido( final Character sentido ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate sentidoPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::sentido] [sentido=={}]", sentido );
				if ( sentido != null ) {
					sentidoPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "cdtpoper" ), sentido );
				}
				return sentidoPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > mercadoNacInt( final Character nacint ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate mercadoNacIntPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::mercadoNacInt] [mercadoNacInt=={}]", nacint );
				if ( nacint != null ) {
					mercadoNacIntPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< Tmct0bok > get( "tmct0bok" )
							.< Tmct0ord > get( "tmct0ord" ).< Character > get( "cdclsmdo" ), nacint );
				}
				return mercadoNacIntPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > nureford( final String nureford ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate nuRefordPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::nureford] [nureford=={}]", nureford );
				if ( nureford != null ) {
					nuRefordPredicate = builder.equal(
							root.< Tmct0alo > get( "tmct0alo" ).< Tmct0bok > get( "tmct0bok" ).< Tmct0ord > get( "tmct0ord" )
									.< String > get( "nureford" ), nureford );
				}
				return nuRefordPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > isin( final String isin ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate isinPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::isin] [isin=={}]", isin );
				if ( isin != null ) {
					isinPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "cdisin" ), isin );
				}
				return isinPredicate;
			}
		};
	}
	
	public static Specification< Tmct0afi > alias( final String alias ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate aliasPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::alias] [alias=={}]", alias );
				if ( alias != null ) {
					aliasPredicate = builder.equal( root.< Tmct0alo > get( "tmct0alo" ).< String > get( "cdalias" ), alias );
				}
				return aliasPredicate;
			}
		};
	}

	public static Specification< Tmct0afi > tpnactit( final Character tpnactit ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Predicate sentidoPredicate = null;
				LOG.trace( "[Tmct0afiSpecifications::sentido] [sentido=={}]", tpnactit );
				if ( tpnactit != null ) {
					sentidoPredicate = builder.equal( root.< String > get( "tpnactit" ), tpnactit );
				}
				return sentidoPredicate;
			}
		};
	}
	
	public static Specification< Tmct0afi > listadoFriltrado( final Date fechaDesde, final Date fechaHasta, final Character mercadoNacInt,
			final Character sentido, final String nudnicif, final String nbclient, final String nbclient1, final String nbclient2,
			final String nbvalors, final Character tpnactit, final String isin, final String alias ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0afi > specs = null;

				if ( nudnicif != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nudnicif( nudnicif ) );
					}
				}

				if ( nbclient != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbclient( nbclient ) );
					} else {
						specs = specs.and( Specifications.where( nbclient( nbclient ) ) );
					}

				}

				if ( nbclient1 != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbclient1( nbclient1 ) );
					} else {
						specs = specs.and( Specifications.where( nbclient1( nbclient1 ) ) );
					}

				}

				if ( nbclient2 != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbclient2( nbclient2 ) );
					} else {
						specs = specs.and( Specifications.where( nbclient2( nbclient2 ) ) );
					}

				}
				if ( nbvalors != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nbValors( nbvalors ) );
					} else {
						specs = specs.and( Specifications.where( nbValors( nbvalors ) ) );
					}

				}

				if ( sentido != null ) {
					if ( specs == null ) {
						specs = Specifications.where( sentido( sentido ) );
					} else {
						specs = specs.and( Specifications.where( sentido( sentido ) ) );
					}

				}

				if ( mercadoNacInt != null ) {
					if ( specs == null ) {
						specs = Specifications.where( mercadoNacInt( mercadoNacInt ) );
					} else {
						specs = specs.and( Specifications.where( mercadoNacInt( mercadoNacInt ) ) );
					}

				}
				
				if ( isin != null ) {
					if ( specs == null ) {
						specs = Specifications.where( isin( isin ) );
					} else {
						specs = specs.and( Specifications.where( isin( isin ) ) );
					}

				}				

				if ( alias != null ) {
					if ( specs == null ) {
						specs = Specifications.where( alias( alias ) );
					} else {
						specs = specs.and( Specifications.where( alias( alias ) ) );
					}

				}				

				if ( tpnactit != null ) {
					if ( specs == null ) {
						specs = Specifications.where( tpnactit( tpnactit ) );
					} else {
						specs = specs.and( Specifications.where( tpnactit( tpnactit ) ) );
					}

				}
				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}
		};
	}

	public static Specification< Tmct0afi > listadoNureford( final String nureford ) {
		return new Specification< Tmct0afi >() {

			public Predicate toPredicate( Root< Tmct0afi > root, CriteriaQuery< ? > query, CriteriaBuilder builder ) {
				Specifications< Tmct0afi > specs = null;

				if ( nureford != null ) {
					if ( specs == null ) {
						specs = Specifications.where( nureford( nureford ) );
					}
				}

				Predicate predicate = null;
				if ( specs != null ) {
					predicate = specs.toPredicate( root, query, builder );
				}
				return predicate;
			}
		};
	}

}
