package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0ord;

@Repository
public interface Tmct0ordDao extends JpaRepository<Tmct0ord, String> {

  Tmct0ord findByNuorden(String nuorden);

  Tmct0ord findByNureford(String nureford);

  List<Tmct0ord> findAllByNureford(String nureford);

  Tmct0ord findFirstByNurefordOrderByNuorden(String nureford);

  @Query(value = "SELECT nuorden, cdclsmdo FROM TMCT0ORD WHERE nureford=:nureford order by nuorden asc fetch first 1 rows only", nativeQuery = true)
  List<Object[]> findFirstByNurefordOrderByNuordenNative(@Param("nureford") String nureford);

  @Query(value = "SELECT nuorden, cdclsmdo, isscrdiv FROM TMCT0ORD WHERE nuorden = :nuorden", nativeQuery = true)
  List<Object[]> findNuordenCdclsmdoIsscrdivByNuordenNative(@Param("nuorden") String nuorden);

  @Query("SELECT o.cdcleare FROM Tmct0ord o WHERE o.nuorden = :nuorden ")
  String findCdcleareById(@Param("nuorden") String nuorden);

  @Query(value = "SELECT NEXT VALUE FOR NUOPRO_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  BigDecimal getNewNuoproutSequence();
}
