package sibbac.business.wrappers.tasks;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.bo.DateUtilService;
import sibbac.business.wrappers.database.bo.FacturaSugeridaBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.FacturaSugerida;
import sibbac.common.utils.ConfigUtil;
import sibbac.common.utils.DateHelper;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

// For Testing:
// @SIBBACJob( group=Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_FACTURA_SUGERIDA, interval = 30, intervalUnit =
// IntervalUnit.MINUTE )

// For REAL:
@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_FACTURA_SUGERIDA, interval = 1, intervalUnit = IntervalUnit.DAY)
public class TaskFacturaSugerida extends WrapperTaskConcurrencyPrevent {

    private static final String EXECUTE_TASK2 = "executeTask2";
    private static final String TAG = TaskFacturaSugerida.class.getName();

    protected static final Logger LOG = LoggerFactory
	    .getLogger(TaskFacturaSugerida.class);

    @Autowired
    private FacturaSugeridaBo facturaSugeridaBo;

    @Autowired
    private GestorServicios gestorServicios;
    @Autowired
    private DateUtilService dateUtilService;
    /* Configuracion de facturas */
    private static ConfigUtil configUtil = ConfigUtil.getInstance();
    private static PropertiesConfiguration facturacionConfig = configUtil.getFacturacionConfig();
    /**/
    public TaskFacturaSugerida() {
    }

    @Override
    public void executeTask() {
	final String prefix = "[TaskFacturaSugerida]";
	LOG.trace(prefix + "[Started...]");
	executeTask2();
	LOG.trace(prefix + "[Finished...]");
    }

    private void executeTask2(){
	int delay = facturacionConfig.getInt("facturacion.delay.days");
	 Date dMenosDelay = DateHelper.getPreviousWorkDate(Calendar.getInstance().getTime(), new Integer(delay), dateUtilService.getWorkDaysOfWeek(), dateUtilService.getHolidays());
	/* LOG */
	 LOG.debug("{}::{} Se va a recuperar la lista de alias para facturar con fecha {}", TAG, EXECUTE_TASK2, dMenosDelay);
	/**/
	final List<Alias> alias = gestorServicios.isActive(this.jobPk, dMenosDelay);
	/* LOG */
	LOG.debug("{}::{} Se han encontrado {} alias", TAG, EXECUTE_TASK2, alias == null ? 0 : alias.size());
	/**/
	if (CollectionUtils.isNotEmpty(alias)) {
	    final List<FacturaSugerida> facturasSugeridas = facturaSugeridaBo
		    .generarFacturasSugeridas(alias, dMenosDelay, null);
	    if (CollectionUtils.isNotEmpty(facturasSugeridas)) {
		blog("[Gerenadas " + facturasSugeridas.size()
			+ " facturas sugeridas");
	    } else {
		blog("[No se han generado facturas sugeridas");
	    }
	} else {
	    blog("No existen datos para la ejecución");
	}
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
	return TIPO_APUNTE.TASK_FACTURAS_SUGERIDA;
    }

}
