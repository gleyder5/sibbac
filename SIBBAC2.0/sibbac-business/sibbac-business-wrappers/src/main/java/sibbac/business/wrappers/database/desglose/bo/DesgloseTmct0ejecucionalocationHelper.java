package sibbac.business.wrappers.database.desglose.bo;

import java.math.BigDecimal;

import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.model.Tmct0grupoejecucion;

public class DesgloseTmct0ejecucionalocationHelper {

  public static void inizializeTmct0ejecucionAlocation(Tmct0ejecucionalocation ejealo,
                                                      Tmct0grupoejecucion grupoEjecucion,
                                                      BigDecimal nutitulos,
                                                      Tmct0ejeId ejeid) {
    ejealo.setTmct0grupoejecucion(grupoEjecucion);
    ejealo.setNutitulos(nutitulos);

    ejealo.setNutitulosPendientesDesglose(nutitulos);

    ejealo.setTmct0eje(new Tmct0eje());
    ejealo.getTmct0eje().setId(ejeid);

  }

}
