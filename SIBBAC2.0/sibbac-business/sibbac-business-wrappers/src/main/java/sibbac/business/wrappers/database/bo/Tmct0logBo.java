package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.Tmct0estadoDao;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.Tmct0logDao;
import sibbac.business.wrappers.database.data.Tmct0AlcData;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.data.Tmct0EstadoData;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0log;
import sibbac.business.wrappers.database.model.Tmct0logId;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0logBo extends AbstractBo<Tmct0log, Tmct0logId, Tmct0logDao> {
  @PersistenceContext
  private EntityManager em;
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0logBo.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Objeto para el acceso a la tabla <code>TMCT0_ESTADO</code>. */
  @Autowired
  private Tmct0estadoDao estadoDao;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Devuelve el siguiente número de secuencia para la inserción de registros en
   * la tabla {@link Tmct0log}.
   * 
   * @return <code>java.math.BigDecimal - </code>Número de secuencia a usar.
   */
  public BigDecimal getSequence() {
    LOG.trace("Petición de secuencia para la inserción de registros en la tabla Tmct0log");
    return dao.getSequence();
  } // getSequence

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void insertarRegistroNewTransaction(Tmct0alc alc, Date lgfecha, Date lghora, String nuejecuc,
      Tmct0estado tmct0estado, String descripcion) throws SIBBACBusinessException {
    try {
      insertarRegistro(alc, lgfecha, lghora, nuejecuc, tmct0estado, descripcion);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e);
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void insertarRegistroNewTransaction(Tmct0alo alo, Date lgfecha, Date lghora, String nuejecuc,
      Tmct0estado tmct0estado, String descripcion) throws SIBBACBusinessException {
    try {
      insertarRegistro(alo, lgfecha, lghora, nuejecuc, tmct0estado, descripcion);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e);
    }
  }

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alc Alc.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param tmct0estado Estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  @Transactional
  public void insertarRegistro(Tmct0alo alo, Date lgfecha, Date lghora, String nuejecuc, Tmct0estado tmct0estado,
      String descripcion, String auditUser) throws SIBBACBusinessException {
    insertarRegistro(alo, lgfecha, lghora, nuejecuc, tmct0estado.getNombre(), descripcion, auditUser);
  } // insertarRegistro

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alc Alc.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param tmct0estado Estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  @Transactional
  public void insertarRegistro(Tmct0alo alo, Date lgfecha, Date lghora, String nuejecuc, String tmct0estado,
      String descripcion, String auditUser) throws SIBBACBusinessException {
    if (alo == null) {
      throw new SIBBACBusinessException("El parámetro alo no puede ser nulo.");
    }
    else if (tmct0estado == null) {
      throw new SIBBACBusinessException("El parámetro tmct0estado no puede ser nulo.");
    }
    else if (descripcion == null) {
      throw new SIBBACBusinessException("El parámetro descripcion no puede ser nulo.");
    }
    else {
      Tmct0log datos_log = new Tmct0log();
      Date ahora = Calendar.getInstance().getTime();

      datos_log.setId(new Tmct0logId(alo.getId().getNuorden(), getSequence()));
      datos_log.setNbooking(alo.getId().getNbooking());
      datos_log.setNucnfclt(alo.getId().getNucnfclt());
      datos_log.setLgfecha(lgfecha == null ? ahora : lgfecha);
      datos_log.setLghora(lghora == null ? ahora : lghora);
      datos_log.setNuejecuc(nuejecuc == null ? "" : nuejecuc);

      datos_log.setLgcodest("");

      datos_log.setLgdesest(StringUtils.substring(StringUtils.trim(tmct0estado), 0, 40));
      datos_log.setLgdescri(StringUtils.substring(StringUtils.trim(descripcion), 0, 200));
      datos_log.setFhaudit(new Date());
      datos_log.setCdusuaud(auditUser);
      save(datos_log);
    } // else
  } // insertarRegistro

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alc Alc.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param tmct0estado Estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  @Transactional
  public void insertarRegistro(Tmct0alo alo, Date lgfecha, Date lghora, String nuejecuc, Tmct0estado tmct0estado,
      String descripcion) throws SIBBACBusinessException {
    insertarRegistro(alo, lgfecha, lghora, nuejecuc, tmct0estado, descripcion, "SIBBAC20");
  } // insertarRegistro

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alc Alc.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param tmct0estado Estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  @Transactional
  public void insertarRegistro(Tmct0alc alc, Date lgfecha, Date lghora, String nuejecuc, Tmct0estado tmct0estado,
      String descripcion) throws SIBBACBusinessException {
    if (alc == null) {
      throw new SIBBACBusinessException("El parámetro alc no puede ser nulo.");
    }
    else if (tmct0estado == null) {
      throw new SIBBACBusinessException("El parámetro tmct0estado no puede ser nulo.");
    }
    else if (descripcion == null) {
      throw new SIBBACBusinessException("El parámetro descripcion no puede ser nulo.");
    }
    else {
      Tmct0log datos_log = new Tmct0log();
      Date ahora = Calendar.getInstance().getTime();

      datos_log.setId(new Tmct0logId(alc.getId().getNuorden(), getSequence()));
      datos_log.setNbooking(alc.getId().getNbooking());
      datos_log.setNucnfclt(alc.getId().getNucnfclt());
      datos_log.setLgfecha(lgfecha == null ? ahora : lgfecha);
      datos_log.setLghora(lghora == null ? ahora : lghora);
      datos_log.setNuejecuc(nuejecuc == null ? "" : nuejecuc);
      if (tmct0estado.getIdestado() > 999) {
        datos_log.setLgcodest("");
      }
      else {
        datos_log.setLgcodest(String.valueOf(tmct0estado.getIdestado()));
      }
      datos_log.setLgdesest(StringUtils.substring(StringUtils.trim(tmct0estado.getNombre()), 0, 40));
      datos_log.setLgdescri(StringUtils.substring(StringUtils.trim(descripcion), 0, 200));
      datos_log.setFhaudit(new Date());
      datos_log.setCdusuaud("SIBBAC20");
      save(datos_log);
    } // else
  } // insertarRegistro

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alc Alc.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param idEstado Número de estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  @Transactional
  public void insertarRegistro(Tmct0alc alc, Date lgfecha, Date lghora, String nuejecuc, Integer idEstado,
      String descripcion) throws Exception {
    if (alc == null) {
      throw new Exception("El parámetro alc no puede ser nulo.");
    }
    else if (idEstado == null) {
      throw new Exception("El parámetro idEstado no puede ser nulo.");
    }
    else if (descripcion == null) {
      throw new Exception("El parámetro descripcion no puede ser nulo.");
    }
    else {
      Tmct0estado tmct0estado = estadoDao.findOne(idEstado);

      if (tmct0estado == null) {
        throw new Exception(MessageFormat.format(
            "No existe en base de datos ningún estado con el Id especificado [estado={0}]", idEstado));
      }
      else {
        Tmct0log datos_log = new Tmct0log();
        Date ahora = Calendar.getInstance().getTime();

        datos_log.setId(new Tmct0logId(alc.getId().getNuorden(), getSequence()));
        datos_log.setNbooking(alc.getId().getNbooking());
        datos_log.setNucnfclt(alc.getId().getNucnfclt());
        datos_log.setLgfecha(lgfecha == null ? ahora : lgfecha);
        datos_log.setLghora(lghora == null ? ahora : lghora);
        datos_log.setNuejecuc(nuejecuc == null ? "" : nuejecuc);
        if (tmct0estado.getIdestado() > 999) {
          datos_log.setLgcodest("");
        }
        else {
          datos_log.setLgcodest(String.valueOf(tmct0estado.getIdestado()));
        }
        datos_log.setLgdesest(StringUtils.substring(StringUtils.trim(tmct0estado.getNombre()), 0, 40));
        datos_log.setLgdescri(StringUtils.substring(StringUtils.trim(descripcion), 0, 200));

        save(datos_log);
      } // else
    } // else
  } // insertarRegistro

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alc Alc.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param idEstado Número de estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  @Transactional
  public void insertarRegistro(Tmct0AlcData alc, Date lgfecha, Date lghora, String nuejecuc,
      Tmct0EstadoData tmct0estado, String descripcion, String cdusuaud) throws Exception {
    if (alc == null) {
      throw new Exception("El parámetro alc no puede ser nulo.");
    }
    else if (descripcion == null) {
      throw new Exception("El parámetro descripcion no puede ser nulo.");
    }
    else {
      Tmct0log datos_log = new Tmct0log();
      Date ahora = Calendar.getInstance().getTime();

      datos_log.setId(new Tmct0logId(alc.getId().getNuorden(), getSequence()));
      datos_log.setNbooking(alc.getId().getNbooking());
      datos_log.setNucnfclt(alc.getId().getNucnfclt());
      datos_log.setLgfecha(lgfecha == null ? ahora : lgfecha);
      datos_log.setLghora(lghora == null ? ahora : lghora);
      datos_log.setNuejecuc(nuejecuc == null ? "" : nuejecuc);
      if (tmct0estado != null) {
        if (tmct0estado.getIdestado() > 999) {
          datos_log.setLgcodest("");
        }
        else {
          datos_log.setLgcodest(String.valueOf(tmct0estado.getIdestado()));
        }
        datos_log.setLgdesest(StringUtils.substring(StringUtils.trim(tmct0estado.getNombre()), 0, 40));

      }
      else {
        datos_log.setLgcodest("");
        datos_log.setLgdesest("");
      }
      datos_log.setLgdescri(StringUtils.substring(StringUtils.trim(descripcion), 0, 200));
      datos_log.setCdusuaud(StringUtils.substring(StringUtils.trim(cdusuaud), 0, 10));
      save(datos_log);
    } // else
  } // insertarRegistro

  /**
   * Inserta un registro en la tabla {@link Tmct0log}.
   * 
   * @param alo Alo.
   * @param lgfecha Fecha.
   * @param lghora Hora.
   * @param nuejecuc Número de ejecución.
   * @param idEstado Número de estado.
   * @param descripcion Descripción.
   * @throws Exception Si alguno de los parámetros requeridos es nulo.
   */
  public void insertarRegistro(Tmct0AloData alo, Date lgfecha, Date lghora, String nuejecuc, Integer idEstado,
      String descripcion, String cdusuaud) throws Exception {
    if (alo == null) {
      throw new Exception("El parámetro alo no puede ser nulo.");
    }
    else if (descripcion == null) {
      throw new Exception("El parámetro descripcion no puede ser nulo.");
    }
    else {

      Tmct0estado tmct0estado;
      try {
        tmct0estado = estadoDao.findOne(idEstado);
      }
      catch (Exception e) {
        tmct0estado = null;
      }
      Tmct0log datos_log = new Tmct0log();
      Date ahora = Calendar.getInstance().getTime();

      datos_log.setId(new Tmct0logId(alo.getId().getNuorden().trim(), getSequence()));
      datos_log.setNbooking(alo.getId().getNbooking().trim());
      datos_log.setNucnfclt(alo.getId().getNucnfclt().trim());
      datos_log.setLgfecha(lgfecha == null ? ahora : lgfecha);
      datos_log.setLghora(lghora == null ? ahora : lghora);
      datos_log.setNuejecuc(nuejecuc == null ? "" : nuejecuc);
      if (tmct0estado != null) {
        if (tmct0estado.getIdestado() > 999) {
          datos_log.setLgcodest("");
        }
        else {
          datos_log.setLgcodest(String.valueOf(tmct0estado.getIdestado()));
        }
        datos_log.setLgdesest(StringUtils.substring(StringUtils.trim(tmct0estado.getNombre()), 0, 40));
      }
      else {
        datos_log.setLgcodest("");
        datos_log.setLgdesest("");
      }
      datos_log.setLgdescri(StringUtils.substring(StringUtils.trim(descripcion), 0, 200));
      datos_log.setCdusuaud(StringUtils.substring(StringUtils.trim(cdusuaud), 0, 10));
      save(datos_log);
      datos_log.getId();
    } // else
  } // insertarRegistro

  public void hacerLogGuardadoNacional(List<Tmct0AlcData> alcs, String message) {
    for (Tmct0AlcData alc : alcs) {
      try {
        this.insertarRegistro(alc, null, null, null, alc.getEstadotit(), message, "SIBBAC");
      }
      catch (Exception e) {
        LOG.error("[Tmct0logBo :: hacerLogGuardadoNacional] Exception saving into log {}", e.getMessage());
      }
    }
  }

  public void hacerLogGuardadoExtranjero(List<Tmct0AloData> alos, String message) {
    for (Tmct0AloData alo : alos) {
      try {
        this.insertarRegistro(alo, null, null, null, alo.getCdestadotit(), message, "SIBBAC");
      }
      catch (Exception e) {
        LOG.error("[Tmct0logBo :: hacerLogGuardadoExtranjero] Exception saving into log {}", e.getMessage());
      }
    }
  }

  @Transactional
  public void insertarRegistro(String nuorden, String nbooking, String msg, String descripcionEstado,
      String usuarioAuditoria) {
    insertarRegistro(nuorden, nbooking, "", msg, descripcionEstado, usuarioAuditoria);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void insertarRegistroNewTransaction(String nuorden, String nbooking, String msg, String descripcionEstado,
      String usuarioAuditoria) {
    insertarRegistro(nuorden, nbooking, "", msg, descripcionEstado, usuarioAuditoria);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void insertarRegistroNewTransaction(String nuorden, String nbooking, String nucnfclt, String msg,
      String descripcionEstado, String usuarioAuditoria) {
    insertarRegistro(nuorden, nbooking, nucnfclt, msg, descripcionEstado, usuarioAuditoria);
  }

  @Transactional
  public void insertarRegistro(String nuorden, String nbooking, String nucnfclt, String msg, String descripcionEstado,
      String usuarioAuditoria) {
    descripcionEstado = StringUtils.substring(StringUtils.trim(descripcionEstado), 0, 40);
    usuarioAuditoria = StringUtils.substring(StringUtils.trim(usuarioAuditoria), 0, 10);
    msg = StringUtils.substring(StringUtils.trim(msg), 0, 200);
    if (msg == null) {
      msg = "Mensaje NULL no valido para insertar en el log.";
      LOG.warn("[insertarRegistro] {}/{}/{} {}", nuorden, nbooking, nucnfclt, msg);
      // throw new
      // SIBBACBusinessException(MessageFormat.format("{0}/{1}/{2} {3}",
      // nuorden, nbooking, nucnfclt, msg));
    }
    this.dao.insertarRegistro(nuorden, nbooking, nucnfclt, msg, descripcionEstado, new Date(), usuarioAuditoria);

  }
} // Tmct0logBo
