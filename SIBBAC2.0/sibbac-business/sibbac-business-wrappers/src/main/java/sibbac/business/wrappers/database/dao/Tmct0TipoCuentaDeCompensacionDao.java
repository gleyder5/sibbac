package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.wrappers.database.data.TipoCuentaDeCompensacionData;
import sibbac.business.wrappers.database.model.Tmct0TipoCuentaDeCompensacion;

public interface Tmct0TipoCuentaDeCompensacionDao extends JpaRepository< Tmct0TipoCuentaDeCompensacion, Long > {
    @Query("SELECT new sibbac.business.wrappers.database.data.TipoCuentaDeCompensacionData(tc) FROM Tmct0TipoCuentaDeCompensacion tc")
    List<TipoCuentaDeCompensacionData> findAllData();

}
