package sibbac.business.wrappers.common.threads;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.database.bo.Tmct0swiBo;
import sibbac.business.wrappers.database.model.Tmct0swi;

/**
 * 
 * Procesa la lista de objetos Tmct0swi en un hilo de ejecucion independiente.
 *
 */
public class ProcesaListaTmct0swiRunnable implements Runnable {

  /** Lista de elementos a procesar */
  private List<Tmct0swi> listaTmct0swi = null;

  /** Bo con los metodos que realizan las operaciones */
  private Tmct0swiBo miTmct0swiBo = null;

  /** Identificador de hilo que se esta creando. */
  private int iNumJob = 0;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(ProcesaListaTmct0swiRunnable.class);
  private static final String NOMBRE_SERVICIO = "ProcesaListaTmct0swiRunnable";

  public ProcesaListaTmct0swiRunnable(List<Tmct0swi> listaTmct0swi, Tmct0swiBo miTmct0swiBo, int iNumJob) {
    this.listaTmct0swi = listaTmct0swi;
    this.miTmct0swiBo = miTmct0swiBo;
    this.iNumJob = iNumJob;
  }

  @Override
  public void run() {
    try {
      // Metodo que inserta los registros
      miTmct0swiBo.insertaRegistrosLista(listaTmct0swi, iNumJob);
      // Metodo que elimina el hilo de la lista de hilos
      miTmct0swiBo.shutdownRunnable(this, iNumJob, false);

    } catch (Exception e) {

      LOG.error("[" + NOMBRE_SERVICIO + " :: Error en la ejcucion del hilo ]  " + iNumJob);
      LOG.error("[" + NOMBRE_SERVICIO + " :: Exception: ]  " + e.getMessage(), e);
      // Se marca en el proceso principal que se ha producido un error para notificar del mismo al usuario
      miTmct0swiBo.shutdownRunnable(this, iNumJob, true);

    } finally {
      miTmct0swiBo.shutdownRunnable(this, iNumJob, false);
    }
  }

}
