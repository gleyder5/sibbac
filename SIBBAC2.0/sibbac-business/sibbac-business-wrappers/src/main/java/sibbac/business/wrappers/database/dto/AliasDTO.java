package sibbac.business.wrappers.database.dto;


import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Idioma;


public class AliasDTO {

	private Long	id;
	private String	nombre;
	private Long	idIdioma;
	private String	descipcionIdioma;
	private Long	idAliasFacturacion;
	private String	nombreAliasFacturacion;
	
	public AliasDTO(){
		
	}

	public AliasDTO( final Alias alias ) {
		this( alias.getId(), alias.getAliasFacturacion(), alias.getIdioma(), alias.getCdaliass(), alias.getDescrali() );
	}

	public AliasDTO( final Long id, final String nombre, final String descrali ) {
		this.id = id;
		this.nombre = Alias.formarNombreAlias( nombre, descrali );
	}

	public AliasDTO( final Long id, final Alias aliasFacturacion, final Idioma idioma, final String nombre, final String nombreBroCli ) {
		this( id, nombre, nombreBroCli );
		if ( idioma != null ) {
			this.idIdioma = idioma.getId();
			this.descipcionIdioma = idioma.getDescripcion();
		}
		if ( aliasFacturacion != null ) {
			this.idAliasFacturacion = aliasFacturacion.getId();
			this.nombreAliasFacturacion = aliasFacturacion.formarNombreAlias();
		}
	}
	
	public AliasDTO( final Long id, final Idioma idioma, final String nombre, final String nombreBroCli ) {
		this( id, nombre, nombreBroCli );
		if ( idioma != null ) {
			this.idIdioma = idioma.getId();
			this.descipcionIdioma = idioma.getDescripcion();
		}
	}

	public Long getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public void setNombre( String nombre ) {
		this.nombre = nombre;
	}

	/**
	 * @return the idIdioma
	 */
	public Long getIdIdioma() {
		return idIdioma;
	}

	/**
	 * @param idIdioma the idIdioma to set
	 */
	public void setIdIdioma( Long idIdioma ) {
		this.idIdioma = idIdioma;
	}

	/**
	 * @return the descipcionIdioma
	 */
	public String getDescipcionIdioma() {
		return descipcionIdioma;
	}

	/**
	 * @param descipcionIdioma the descipcionIdioma to set
	 */
	public void setDescipcionIdioma( String descipcionIdioma ) {
		this.descipcionIdioma = descipcionIdioma;
	}

	/**
	 * @return the idAliasFacturacion
	 */
	public Long getIdAliasFacturacion() {
		return idAliasFacturacion;
	}

	/**
	 * @param idAliasFacturacion the idAliasFacturacion to set
	 */
	public void setIdAliasFacturacion( Long idAliasFacturacion ) {
		this.idAliasFacturacion = idAliasFacturacion;
	}

	/**
	 * @return the nombreAliasFacturacion
	 */
	public String getNombreAliasFacturacion() {
		return nombreAliasFacturacion;
	}

	/**
	 * @param nombreAliasFacturacion the nombreAliasFacturacion to set
	 */
	public void setNombreAliasFacturacion( String nombreAliasFacturacion ) {
		this.nombreAliasFacturacion = nombreAliasFacturacion;
	}

}
