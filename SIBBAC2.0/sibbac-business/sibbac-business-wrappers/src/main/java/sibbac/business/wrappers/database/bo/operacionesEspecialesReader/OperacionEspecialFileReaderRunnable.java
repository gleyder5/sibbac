package sibbac.business.wrappers.database.bo.operacionesEspecialesReader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.ObserverProcess;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.PartenonRecordBeanBo;
import sibbac.business.wrappers.database.model.DesgloseRecordBean;
import sibbac.business.wrappers.database.model.OperacionEspecialRecordBean;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;
import sibbac.common.SIBBACBusinessException;

@Component(value = "OperacionEspecialFileReaderRunnable")
public class OperacionEspecialFileReaderRunnable extends IRunnableBean<OperacionEspecialRecordBean> {

  private final static String ID_LINEA_DESGLOSE = "40";

  private static final Logger LOG = LoggerFactory.getLogger(OperacionEspecialFileReaderRunnable.class);

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Autowired
  private PartenonRecordBeanBo partenonRecordBeanBo;

  public OperacionEspecialFileReaderRunnable() {
    super();
  }

  public void preRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED,
                 propagation = Propagation.REQUIRES_NEW,
                 rollbackFor = { SIBBACBusinessException.class, Exception.class
                 })
  public void run(ObserverProcess observer, List<OperacionEspecialRecordBean> beans, int order) throws SIBBACBusinessException {
    LOG.trace("[OperacionEspecialFileReaderRunnable :: run] inicio.");
    int ordenInterno = order - beans.size();
    OperacionEspecialRecordBean desglose = null;
    for (OperacionEspecialRecordBean operacionEspecialRecordBean : beans) {
      if (ID_LINEA_DESGLOSE.equals(operacionEspecialRecordBean.getTipoRegistro())) {
        desglose = processLineDesglose((DesgloseRecordBean) operacionEspecialRecordBean, ordenInterno);
      } else {
        processLineTitular((DesgloseRecordBean) desglose, (PartenonRecordBean) operacionEspecialRecordBean,
                           ordenInterno);
      }
    }
    LOG.trace("[OperacionEspecialFileReaderRunnable :: run] fin.");
  }

  public void postRun(ObserverProcess observer) throws SIBBACBusinessException {

  }

  // @Transactional
  // protected boolean processBean(final OperacionEspecialRecordBean desglose,
  // final OperacionEspecialRecordBean titular,
  // final int lineNumber) throws SIBBACBusinessException {
  // boolean resultado = true;
  // final String idMensaje = bean.getIdMensaje();
  // if (ID_LINEA_DESGLOSE.equals(idMensaje)) {
  // LOG.trace("Leída línea [{}] de tipo desglose", lineNumber);
  // desgloseRecordBean = (DesgloseRecordBean) bean;
  // resultado = processLineDesglose(desgloseRecordBean, lineNumber);
  // } else {
  // LOG.trace("Leída línea [{}] de tipo titular", lineNumber);
  // resultado = processLineTitular(desglose,(PartenonRecordBean) bean, lineNumber);
  // }
  // LOG.trace("Finalizado el procesamiento de la línea [{}]", lineNumber);
  // return resultado;
  // }

  @Transactional
  private DesgloseRecordBean processLineDesglose(DesgloseRecordBean bean, final int lineNumber) throws SIBBACBusinessException {

    bean.setNucnfliq(new Short("1"));
    bean.setFechaProcesamiento(new Date());
    bean.setPorcesado(NumberUtils.INTEGER_ZERO);
    bean = desgloseRecordBeanBo.save(bean);
    return bean;
  }

  @Transactional
  private PartenonRecordBean processLineTitular(DesgloseRecordBean desgloseRecordBean,
                                                PartenonRecordBean bean,
                                                final int lineNumber) throws SIBBACBusinessException {

    if (desgloseRecordBean == null) {
      List<String> nurefors = new ArrayList<String>();
      nurefors.add(bean.getNumOrden());
      LOG.warn("[OperacionEspecialFileReaderRunnable :: processLineTitular] INCIDENCIA no tenemos '40' lo buscamos en bbdd, nureford: {}",
               bean.getNumOrden());
      List<DesgloseRecordBean> desgloses = desgloseRecordBeanBo.findAllByNureford(nurefors);
      if (CollectionUtils.isNotEmpty(desgloses)) {
        LOG.info("[OperacionEspecialFileReaderRunnable :: processLineTitular] Encontrado el '40' en bbdd, nureford: {}",
                 bean.getNumOrden());
        desgloseRecordBean = desgloses.get(0);
      }
    }
    if (desgloseRecordBean == null) {
      LOG.warn("[OperacionEspecialFileReaderRunnable :: processLineTitular] INCIDENCIA procesando titular, no encontrado desglose '40' {}",
               bean);
    }
    bean.setDesgloseRecordBean(desgloseRecordBean);
    bean = partenonRecordBeanBo.save(bean);
    return bean;
  }

  @Override
  public IRunnableBean<OperacionEspecialRecordBean> clone() {
    return this;
  }

}
