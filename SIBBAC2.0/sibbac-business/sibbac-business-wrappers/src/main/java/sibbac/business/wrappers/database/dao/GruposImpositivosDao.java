package sibbac.business.wrappers.database.dao;


import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.GruposImpositivos;


@Component
public interface GruposImpositivosDao extends JpaRepository< GruposImpositivos, Long > {

	public GruposImpositivos findByValidoDesdeBeforeAndCancelado( final Date fechaActual, final String cancelado );

}
