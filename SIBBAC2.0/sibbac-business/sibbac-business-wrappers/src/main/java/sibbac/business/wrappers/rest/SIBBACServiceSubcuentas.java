package sibbac.business.wrappers.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.database.bo.AliasSubcuentaBo;
import sibbac.business.wrappers.database.dto.AliasSubcuentaDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceSubcuentas implements SIBBACServiceBean {

	/** The aliass bo. */
	@Autowired
	private AliasSubcuentaBo	aliasSubcuentaBo;

	private static final String	CMD_GET_SUBCUENTAS			= "getListaSubcuentas";
	private static final String	CMD_UPD_SUBCUENTA			= "modificarSubcuenta";

	private static final String	FILTER_ID_ALIAS				= "idAlias";
	private static final String	FILTER_ID_SUBCUENTA			= "idSubcuenta";
	private static final String	FILTER_ID_ALIAS_FACTURACION	= "idAliasFacturacion";

	private static final String	RESULTADO_LISTA_SUBCUENTAS	= "listaSubcuentas";
	private static final String	RESULTADO					= "resultado";
	private static final String	RESULTADO_SUCCESS			= "SUCCESS";
	/** The Constant LOG. */
	private static final Logger	LOG							= LoggerFactory.getLogger( SIBBACServiceSubcuentas.class );

	public SIBBACServiceSubcuentas() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) throws SIBBACBusinessException {
		LOG.debug( "[SIBBACServiceSubcuentas::process] " + "Processing a web request..." );

		// prepara webResponse
		WebResponse webResponse = new WebResponse();

		// obtiene comandos(paths)
		final String action = webRequest.getAction();
		LOG.debug( "[SIBBACServiceAlias::process] " + "+ Command.: [{}]", action );

		switch ( action ) {
			case SIBBACServiceSubcuentas.CMD_GET_SUBCUENTAS:
				this.getSubcuentas( webRequest, webResponse );
				break;
			case SIBBACServiceSubcuentas.CMD_UPD_SUBCUENTA:
				this.updateAliasSubcuenta( webRequest, webResponse );
				break;
			default:
				webResponse.setError( "No recibe ningun comando" );
				LOG.error( "No le llega ningun comando" );
				break;
		}

		return webResponse;
	}

	private void getSubcuentas( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idAlias = getIdSubcuenta( filtros.get( FILTER_ID_ALIAS ) );
		final Map< String, List< AliasSubcuentaDTO >> resultados = new HashMap< String, List< AliasSubcuentaDTO >>();
		resultados.put( RESULTADO_LISTA_SUBCUENTAS, aliasSubcuentaBo.getListaSubcuentas( idAlias ) );
		webResponse.setResultados( resultados );
	}

	private void updateAliasSubcuenta( WebRequest webRequest, WebResponse webResponse ) throws SIBBACBusinessException {
		final Map< String, String > filtros = webRequest.getFilters();
		if ( MapUtils.isEmpty( filtros ) ) {
			throw new SIBBACBusinessException( "No se han recuperado filtros en la peticion" );
		}
		final Long idAliasSbucuenta = getIdSubcuenta( filtros.get( FILTER_ID_SUBCUENTA ) );
		final Long idAliasFacturacion = getIdAlias( filtros.get( FILTER_ID_ALIAS_FACTURACION ) );
		aliasSubcuentaBo.updateAliasSubcuenta( idAliasSbucuenta, idAliasFacturacion );
		final Map< String, String > resultados = new HashMap< String, String >();
		resultados.put( RESULTADO, RESULTADO_SUCCESS );
		webResponse.setResultados( resultados );
	}

	/**
	 * @param idSubcuentaStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdSubcuenta( final String idSubcuentaStr ) throws SIBBACBusinessException {
		Long idSubcuenta = null;
		try {
			if ( StringUtils.isNotBlank( idSubcuentaStr ) ) {
				idSubcuenta = NumberUtils.createLong( idSubcuentaStr );
			} else {
				throw new SIBBACBusinessException( "El valor de \"idSubcuenta\": [" + idSubcuentaStr + "] es obligatorio " );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idSubcuenta\": [" + idSubcuentaStr + "] " );
		}
		return idSubcuenta;
	}

	/**
	 * @param idAliasStr
	 * @return
	 * @throws SIBBACBusinessException
	 */
	private Long getIdAlias( final String idAliasStr ) throws SIBBACBusinessException {
		Long idAlias = null;
		try {
			if ( StringUtils.isNotBlank( idAliasStr ) ) {
				idAlias = NumberUtils.createLong( idAliasStr );
			}
		} catch ( NumberFormatException e ) {
			throw new SIBBACBusinessException( "Error leyendo el valor de \"idAlias\": [" + idAliasStr + "] " );
		}
		return idAlias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		final List< String > commands = new ArrayList< String >();
		commands.add( SIBBACServiceSubcuentas.CMD_GET_SUBCUENTAS );
		commands.add( SIBBACServiceSubcuentas.CMD_UPD_SUBCUENTA );
		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		return null;
	}

}
