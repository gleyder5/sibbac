package sibbac.business.wrappers.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.wrappers.common.fileReader.SibbacFileReader;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.tasks.IRunnableBean;

public class RunnableProcess<T extends RecordBean> implements Runnable {

  protected static final Logger LOG = LoggerFactory.getLogger(RunnableProcess.class);

  private IRunnableBean<T> iRunnableBean = null;
  private SibbacFileReader<T> sibbacFileReader = null;
  protected ObserverProcess observerProcess = null;
  private List<HashMap<String, Object>> lista = null;

  private List<T> beanList;
  private int fileLineNumber;

  public RunnableProcess(ObserverProcess observerProcess) {
    this.observerProcess = observerProcess;
  }

  public RunnableProcess(SibbacFileReader<T> sibbacFileReader,
                         ObserverProcess observerProcess,
                         List<HashMap<String, Object>> lista) {
    this(observerProcess);
    this.sibbacFileReader = sibbacFileReader;
    this.lista = lista;
  }

  public RunnableProcess(SibbacFileReader<T> sibbacFileReader,
                         ObserverProcess observerProcess,
                         List<T> beans,
                         int lineNumber) {
    this.sibbacFileReader = sibbacFileReader;
    this.observerProcess = observerProcess;
    this.beanList = new ArrayList<T>();
    this.beanList.addAll(beans);
    this.fileLineNumber = lineNumber;

  }

  public RunnableProcess(IRunnableBean<T> iRunnableBean, ObserverProcess observerProcess, List<T> beans, int lineNumber) {
    this.iRunnableBean = iRunnableBean;
    this.observerProcess = observerProcess;
    this.beanList = new ArrayList<T>();
    this.beanList.addAll(beans);
    this.fileLineNumber = lineNumber;

  }

  @Override
  public void run() {
    LOG.debug("[RunnableProcess :: run] Init ");

    try {

      if (this.iRunnableBean != null) {
        // Method m = this.iRunnableBean.getClass().getMethod("run");
        // m.invoke(m);

        this.iRunnableBean.preRun(observerProcess);
        this.iRunnableBean.run(observerProcess, beanList, fileLineNumber);
        this.iRunnableBean.postRun(observerProcess);
      } else if (CollectionUtils.isEmpty(lista) && CollectionUtils.isNotEmpty(beanList)) {
        runBlock(beanList);
      } else {
        boolean ok = true;
        while (ok) {
          ok = false;
          if (!sibbacFileReader.getError()) {
            HashMap<String, Object> hmOrden = null;
            synchronized (lista) {
              if (lista != null && !lista.isEmpty()) {
                hmOrden = (HashMap<String, Object>) lista.remove(0);
                ok = true;
              }
            }

            if (ok) {
              fileLineNumber = (Integer) hmOrden.get("lineNumber");
              Integer lineOrden = (Integer) hmOrden.get("lineOrden");
              try {
                LOG.debug("[RunnableProcess :: run] tratando la linea: " + fileLineNumber);
                LOG.debug("[RunnableProcess :: run] tratando la orden numero : " + lineOrden);
                Integer lineNumberInterno = 0;
                beanList = (List<T>) hmOrden.get("beanList");

                lineNumberInterno = sibbacFileReader.saveAndCommitRunnable(beanList, lineNumberInterno);
              } catch (Exception e) {
                LOG.error("[RunnableProcess :: run] error guardando la linea numero: " + fileLineNumber);
                LOG.error("[RunnableProcess :: run] error guardando la orden numero: " + lineOrden);
                sibbacFileReader.setError(true);
                observerProcess.addErrorProcess(e);
              }
            }
          }// if (!sibbacFileReader.getError())
        }// while (ok)
      }
    } catch (Exception e) {
      LOG.warn("[RunnableProcess :: run] Incidencia en el bloque de linea : {} ## beanlist : {}", fileLineNumber,
               beanList);
      LOG.warn("[RunnableProcess :: run] Incidencia", e);
      if (sibbacFileReader != null) {
        sibbacFileReader.setError(true);
      }

      observerProcess.addErrorProcess(new Exception("Incidencia en linea : " + fileLineNumber + " beanlist : "
                                                    + beanList, e));
    } finally {
      if (sibbacFileReader != null) {
        sibbacFileReader.shutdownRunnable(this);
      }
      if (observerProcess != null) {
        observerProcess.sutDownThread(this);
      }
    }
    LOG.debug("[RunnableProcess :: run] Fin ");
  }// public void run() {

  private void runBlock(List<T> beanList) {
    if (CollectionUtils.isNotEmpty(beanList)) {
      try {
        LOG.debug("[RunnableProcess :: runBlock] tratando bloque que empieza en la linea: " + fileLineNumber);
        Integer lineNumberInterno = fileLineNumber - beanList.size();
        lineNumberInterno = sibbacFileReader.saveAndCommitRunnable(beanList, fileLineNumber);
      } catch (Exception e) {
        LOG.warn("[RunnableProcess :: runBlock] Incidencia guardando la linea numero: " + fileLineNumber);
        LOG.warn("[RunnableProcess :: runBlock] Incidencia guardando la beanlist: " + beanList);
        // if (CollectionUtils.size(beanList) > 1) {
        // int pos = 0;
        // while (pos < beanList.size()) {
        // try {
        // LOG.debug("[RunnableProcess :: runBlock] tratando la linea: " + fileLineNumber);
        // Integer lineNumberInterno = 0;
        // lineNumberInterno = sibbacFileReader.saveAndCommitRunnable(beanList.subList(pos, pos + 1), fileLineNumber);
        // } catch (Exception ex) {
        // LOG.warn("[RunnableProcess :: runBlock] Incidencia guardando la linea numero: " + fileLineNumber);
        // LOG.warn("[RunnableProcess :: runBlock] Incidencia guardando la beanlist: " + beanList);
        //
        // sibbacFileReader.setError(true);
        // observerProcess.addErrorProcess(ex);
        // }
        // pos++;
        // }// fin while
        // } else {
        sibbacFileReader.setError(true);
        observerProcess.addErrorProcess(e);
        // }
      }
    }
  }

}
