package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.wrappers.database.data.CompensadorData;
import sibbac.business.wrappers.database.model.Tmct0Compensador;
import sibbac.business.wrappers.database.model.Tmct0CuentasDeCompensacion;

@Repository
public interface Tmct0CompensadorDao extends JpaRepository< Tmct0Compensador, Long > {

	@Query( "SELECT COMP FROM Tmct0Compensador COMP, Tmct0CuentasDeCompensacion CC "
			+ "WHERE CC.tmct0Compensador = COMP AND CC = :cuentacompensacion " )
	Tmct0Compensador findByTmct0cuentasdecompensacion( @Param( "cuentacompensacion" ) Tmct0CuentasDeCompensacion cuentacompensacion );
	@Query("SELECT new sibbac.business.wrappers.database.data.CompensadorData(c) FROM Tmct0Compensador c")
	List<CompensadorData>findAllData();
	
	@Query("SELECT compensador FROM Tmct0Compensador compensador WHERE compensador.nbDescripcion=:nbDescripcion ")
	public Tmct0Compensador findByNbDescripcion(@Param("nbDescripcion") String nbDescripcion );
	
  public Tmct0Compensador findByNbNombre(String nbNombre );
  
  @Query("SELECT new sibbac.business.fase0.database.dto.LabelValueObject("
      + "CONCAT(c.nbNombre,'-(',c.nbDescripcion,')'), c.nbNombre) "
      + "FROM Tmct0Compensador c ORDER BY c.nbNombre ASC, c.nbDescripcion ASC")
  public List<LabelValueObject> labelValueList();
	
}
