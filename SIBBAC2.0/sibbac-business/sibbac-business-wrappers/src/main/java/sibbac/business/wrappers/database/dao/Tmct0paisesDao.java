package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.wrappers.database.model.Tmct0paises;
import sibbac.business.wrappers.database.model.Tmct0paisesId;

public interface Tmct0paisesDao extends JpaRepository<Tmct0paises, Tmct0paisesId> {

  @Query("SELECT o FROM Tmct0paises o WHERE o.id.cdestado = 'A' and o.id.cdisoalf2 =:cdisoalf2")
  public Tmct0paises findBycdisoalf2(@Param("cdisoalf2") String cdisoalf2);

  @Query("SELECT o FROM Tmct0paises o WHERE o.id.cdestado = 'A' order by o.nbpais")
  public List<Tmct0paises> findAllActivos();

  @Query("SELECT o FROM Tmct0paises o WHERE o.id.cdisonum =:isoNum and o.id.cdestado = 'A'")
  public Tmct0paises findByisoNumActivo(@Param("isoNum") String isoNum);
  
  @Query("SELECT o FROM Tmct0paises o WHERE o.cdhacienda =:cdhacienda and o.id.cdestado = 'A'")
  public Tmct0paises findByCdHaciendaActivo(@Param("cdhacienda") String cdhacienda);

  @Query("SELECT trim(o.cdhacienda) FROM Tmct0paises o WHERE o.id.cdestado = 'A'")
  public List<String> findAllCdhaciendaActivos();

  @Query("SELECT trim(o.id.cdisonum) FROM Tmct0paises o WHERE o.id.cdestado = 'A'")
  public List<String> findAllCdisonumActivos();

} // Tmct0paisesDao
