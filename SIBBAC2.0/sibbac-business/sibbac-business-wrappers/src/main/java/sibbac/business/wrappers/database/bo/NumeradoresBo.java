package sibbac.business.wrappers.database.bo;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
// import javax.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.NumeradoresDao;
import sibbac.business.wrappers.database.dto.NumeradoresDTO;
import sibbac.business.wrappers.database.model.Identificadores;
import sibbac.business.wrappers.database.model.Numeradores;
import sibbac.business.wrappers.database.model.TipoDeDocumento;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;


@Service
public class NumeradoresBo extends AbstractBo< Numeradores, Long, NumeradoresDao > {

	@Autowired
	private IdentificadoresBo	identificadoresBo;

	@Autowired
	private TipoDeDocumentoBo	tipoDeDocumentoBo;

	/**
	 * @param tipoDeDocumento
	 * @param identificador
	 * @return
	 */
	@Transactional
	public Long calcularNumeroSiguiente( final TipoDeDocumento tipoDeDocumento, final Identificadores identificador ) {

		Numeradores numerador = dao.findAllByTipoDeDocumentoAndIdentificador( tipoDeDocumento, identificador );

		numerador.incrContador();
		numerador = save( numerador );
		return numerador.getSiguiente();
	}

	public List< NumeradoresDTO > getLista() {
		List< Numeradores > entities = ( List< Numeradores > ) findAll();
		List< NumeradoresDTO > resultList = new LinkedList< NumeradoresDTO >();
		for ( Numeradores entity : entities ) {
			NumeradoresDTO dto = entityNumeradoresToDTO( entity );
			resultList.add( dto );
		}
		return resultList;
	}

	// Devuelve una lista de los numeradores
	public Map< String, NumeradoresDTO > getListaNumeradores() {

		Map< String, NumeradoresDTO > resultados = new HashMap< String, NumeradoresDTO >();
		List< NumeradoresDTO > listaNumeradores = ( List< NumeradoresDTO > ) this.getLista();

		for ( NumeradoresDTO numeradoresDTO : listaNumeradores ) {
			resultados.put( numeradoresDTO.getId().toString(), numeradoresDTO );
		}

		return resultados;
	}

	public NumeradoresDTO entityNumeradoresToDTO( Numeradores entity ) {
		NumeradoresDTO dto = new NumeradoresDTO();

		if ( entity != null ) {
			dto.setId( entity.getId() );
			dto.setInicio( entity.getInicio() );
			dto.setFin( entity.getFin() );
			dto.setSiguiente( entity.getSiguiente() );
			dto.setIdentificadores( entity.getIdentificador().getIndentificadores() );
			dto.setTipodedocumento( entity.getTipoDeDocumento().getDescripcion() );

		}
		return dto;
	}

	public Numeradores rellenarObjeto( Map< String, String > params ) throws SIBBACBusinessException {
		Long inicio = NumberUtils.createLong( params.get( Constantes.NUMERADORES_INICIO ) );
		Long fin = NumberUtils.createLong( params.get( Constantes.NUMERADORES_FIN ) );
		Long siguiente = NumberUtils.createLong( params.get( Constantes.NUMERADORES_SIGUIENTE ) );
		Long IdIdentificador = NumberUtils.createLong( params.get( Constantes.NUMERADORES_IDENTIFICADORES ) );
		Long idTipoDeDocumento = Long.parseLong( params.get( Constantes.NUMERADORES_TIPODEDOCUMENTO ) );

		Identificadores identificador = identificadoresBo.findById( IdIdentificador );
		TipoDeDocumento tipoDeDocumento = tipoDeDocumentoBo.findById( idTipoDeDocumento );
		if ( dao.findAllByTipoDeDocumentoAndIdentificador( tipoDeDocumento, identificador ) != null ) {
			throw new SIBBACBusinessException( "La paridad de tipo de documento e identificador ya ha sido ingresado con anterioridad" );
		}

		Numeradores numerador = new Numeradores();

		numerador.setInicio( inicio );
		numerador.setFin( fin );
		numerador.setSiguiente( siguiente );
		numerador.setSiguiente( inicio );
		numerador.setIdentificador( identificador );
		numerador.setTipoDeDocumento( tipoDeDocumento );

		return numerador;

	}

	// @Transactional
	public Map< String, String > createNumeradores( Map< String, String > params ) throws SIBBACBusinessException {

		Map< String, String > resultado = new HashMap< String, String >();

		Numeradores numerador = this.rellenarObjeto( params );
		if ( numerador.getInicio().compareTo( numerador.getFin() ) < 0 ) {
			numerador = save( numerador );

			resultado.put( "Resultado de la operacion:", "Se ha creado el Numerador" );
		} else {
			throw new SIBBACBusinessException( "No se ha podido crear el numerador. El intervalo de datos introducidos no es correcto" );
		}

		return resultado;

	}

	/**
	 * Este metodo devuelve un objeto Identificador en base al id alias que se
	 * le pasa y que lleva el objeto Numerador.
	 *
	 * @param params
	 * @return
	 */
	public Identificadores findIdentificadoresById( Map< String, String > params ) {

		LOG.trace( " Llamada al metodo findIdentificadoresById del numeradorBO " );
		return identificadoresBo.findById( Long.parseLong( params.get( Constantes.NUMERADORES_IDENTIFICADORES ) ) );

	}

	/**
	 * Este metodo devuelve un objeto TipoDeDocumento en base al id
	 * tipoDeDocumento que se le pasa y que lleva el objeto Numerador.
	 *
	 * @param params
	 * @return
	 */

	public TipoDeDocumento findTipoDeDocumentoById( Map< String, String > params ) {

		LOG.trace( " Llamada al metodo findIdentificadoresById del numeradorBO " );
		return tipoDeDocumentoBo.findById( Long.parseLong( params.get( Constantes.NUMERADORES_TIPODEDOCUMENTO ) ) );

	}
}
