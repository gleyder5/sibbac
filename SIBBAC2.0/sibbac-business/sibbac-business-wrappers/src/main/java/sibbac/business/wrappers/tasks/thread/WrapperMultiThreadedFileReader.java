/**
 * 
 */
package sibbac.business.wrappers.tasks.thread;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.model.RecordBean;

/**
 * @author xIS16630
 *
 */
@SuppressWarnings("rawtypes")
public abstract class WrapperMultiThreadedFileReader<T extends RecordBean> extends WrapperMultiThreaded {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(WrapperMultiThreadedFileReader.class);
  protected Charset charset;
  protected Path file;
  protected Path testigo;
  protected boolean hasTestigo = false;

  private LineIterator lineIterator;

  protected LineMapper<T> lineMapper = null;

  /**
   * 
   */
  public WrapperMultiThreadedFileReader() {
    super();
    this.charset = StandardCharsets.ISO_8859_1;
    // Map<String, LineTokenizer> tokenizers = initializeLineTokenizers();
    // Map<String, FieldSetMapper<T>> fieldSetMappers = initializeFieldSetMappers();
    //
    // final PatternMatchingCompositeLineMapper<T> compositeLineMapper = new PatternMatchingCompositeLineMapper<T>();
    // if (tokenizers != null && !tokenizers.isEmpty()) {
    // compositeLineMapper.setTokenizers(tokenizers);
    // }
    // if (fieldSetMappers != null && !fieldSetMappers.isEmpty()) {
    // compositeLineMapper.setFieldSetMappers(fieldSetMappers);
    // }
    this.lineMapper = initLineMapper();
    initLineTokenizers(this.lineMapper);
    initFieldSetMappers(this.lineMapper);

    // ini
  }

  /**
   * 
   */
  public WrapperMultiThreadedFileReader(String fileName, boolean hasTestigo, Charset charset) {
    this();
    if (StringUtils.isNotBlank(fileName)) {
      this.file = Paths.get(fileName);
    }
    this.charset = charset;
    this.hasTestigo = hasTestigo;
  }

  protected abstract void initLineTokenizers(LineMapper<T> lineMapper);

  protected abstract void initFieldSetMappers(LineMapper<T> lineMapper);

  protected abstract LineMapper<T> initLineMapper();

  @Transactional
  public void executeTask(String fileName, boolean hasTestigo, Charset charset) throws Exception {
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] inicio");
    this.hasTestigo = hasTestigo;
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] hasTestigo == {}", hasTestigo);
    this.file = Paths.get(fileName);
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] file == {}", fileName);
    if (charset != null) {
      this.charset = charset;
    }
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] charset == {}", this.charset);
    this.executeTask();
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] fin");
  }

  @Override
  @Transactional
  public void executeTask() throws Exception {
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] inicio");

    if (Files.exists(this.file)) {
      if (this.checkTestigo(this.hasTestigo)) {
        String originalFileName = this.file.toString();
        String procesandoFileName = this.file.toString().substring(0, this.file.toString().indexOf("."))
                                    + ".procesando";
        Path procesandoFile = Paths.get(procesandoFileName);
        if (!Files.exists(procesandoFile)) {
          this.file = Files.move(this.file, procesandoFile, StandardCopyOption.ATOMIC_MOVE);
        } else {
          throw new Exception("Encontrado fichero : " + procesandoFileName + " ya en el filesystem.");
        }
        this.openFile(originalFileName);

        try {
          super.executeTask();
          if (this.hasTestigo) {
            Files.deleteIfExists(testigo);
          }
        } catch (Exception e) {
          throw e;
        } finally {
          closeFile();
        }
      } else {
        LOG.warn("[WrapperMultireadedFileReader :: executeTask] Testigo no encontrado : {}", this.testigo.toString());
      }
    } else {
      LOG.info("[WrapperMultireadedFileReader :: executeTask] Fichero no encontrado : {}", this.file.toString());
    }
    LOG.trace("[WrapperMultireadedFileReader :: executeTask] fin");
  }

  public boolean hasNextObjectToProcess() throws Exception {
    boolean hasNext = lineIterator != null && lineIterator.hasNext();

    return hasNext;
  }

  public T getNextObjectToProcess(int lineNumber) throws Exception {
    T bean = null;
    if (lineIterator != null) {
      String linea = lineIterator.next();
      char caraterRaroFinalFichero = 26;
      if (StringUtils.isNotBlank(linea) && !StringUtils.contains(linea, caraterRaroFinalFichero)) {

        try {
          bean = this.lineMapper.mapLine(linea, lineNumber);
        } catch (Exception e) {
          Exception ex = new Exception("No se ha podido mapear la linea: " + lineNumber + " Msg: " + linea, e);
          getObserver().addErrorProcess(ex);
          throw ex;

        }
      } else {
        LOG.warn("[WrapperMultireadedFileReader :: getNextObjectToProcess] Linea Numero : {} esta en blanco.",
                 lineNumber);
      }
    }
    return bean;
  }

  /**
   * 
   * @param hasTestigo
   * @return
   */
  protected boolean checkTestigo(boolean hasTestigo) {
    boolean continueProcessing = true;
    if (hasTestigo) {
      testigo = Paths.get(FilenameUtils.removeExtension(file.toString()).concat(".TST"));
      try {
        if (!Files.exists(testigo)) {
          // Si no existe el testigo no se podra procesar
          continueProcessing = false;
          LOG.debug("[WrapperMultireadedFileReader :: checkTestigo] No sera procesado porque no existe testigo: "
                    + testigo.toString() + ")");
        }
      } catch (Exception e) {
        // Si no existe el testigo no se podra procesar
        continueProcessing = false;
        LOG.debug("[WrapperMultireadedFileReader :: checkTestigo] No sera procesado porque no existe testigo: "
                  + testigo + ")");
      }
    }
    return continueProcessing;
  } // canBeProcessed

  /**
   * 
   * @return
   * @throws Exception
   */
  protected boolean openFile(String originalFileName) throws Exception {
    LOG.debug("[WrapperMultireadedFileReader :: openFile] inicio");
    boolean continueProcessing = true;
    if (lineIterator == null) {

      try {
        boolean gzipped = originalFileName.toUpperCase().endsWith(".GZ");
        // boolean zipped = file.toString().toUpperCase().endsWith(".ZIP");
        if (gzipped) {
          LOG.debug("[WrapperMultireadedFileReader :: openFile] fichero comprimido en GZ");
          lineIterator = IOUtils.lineIterator(new GZIPInputStream(new FileInputStream(file.toFile())),
                                              this.charset.name());
          // } else if (zipped) {
          // LOG.debug("[WrapperMultireadedFileReader :: openFile] fichero comprimido en ZIP");
          // ZipFile zip = new ZipFile(file.toFile(), this.charset);
          // ZipEntry zipEntry = zip.entries().nextElement();
          // lineIterator = IOUtils.lineIterator(zip.getInputStream(zipEntry), this.charset.name());
          // zip.close();
        } else {
          lineIterator = FileUtils.lineIterator(file.toFile(), this.charset.name());
        }
      } catch (IOException e) {
        Exception ex = new Exception("No se ha podido abrir el fichero: " + file, e);
        throw ex;
      }
    }
    LOG.debug("[WrapperMultireadedFileReader :: openFile] final");
    return continueProcessing;
  } // canBeProcessed

  /**
   * 
   * @param fichero
   * @param hasTestigo
   * @return
   */
  @SuppressWarnings("unused")
  private boolean checkTestigo(Path file, boolean hasTestigo) {
    this.file = file;
    return checkTestigo(hasTestigo);

  } // canBeProcessed

  protected void closeFile() {
    if (lineIterator != null) {
      lineIterator.close();
    }
    lineIterator = null;

  }

  public Path getFile() {
    return file;
  }

}
