package sibbac.business.wrappers.database.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.common.Constantes;
import sibbac.business.wrappers.database.dao.ContactoDao;
import sibbac.business.wrappers.database.dao.ContactoDaoImpl;
import sibbac.business.wrappers.database.data.CombinacionData;
import sibbac.business.wrappers.database.data.ContactosData;
import sibbac.business.wrappers.database.data.MatrizServiciosContactosData;
import sibbac.business.wrappers.database.data.ServicioActivoData;
import sibbac.business.wrappers.database.data.ServicioData;
import sibbac.business.wrappers.database.data.ServiciosContactosData;
import sibbac.business.wrappers.database.dto.ContactoDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

/**
 * The Class ContactoBo.
 */
@Service
@Transactional
public class ContactoBo extends AbstractBo<Contacto, Long, ContactoDao> {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ContactoBo.class);
	private static final String TAG = ContactoBo.class.getName();
	/** The alias bo. */
	@Autowired
	private AliasBo aliasBO;

	/** The contacto dao impl. */
	@Autowired
	private ContactoDaoImpl contactoDaoImpl;

	@Autowired
	private ServiciosBo serviciosBo;

	/**
	 * Gets the contactoy servicios por alias.
	 *
	 * @param idAlias
	 *            the id alias
	 * @return the contactoy servicios por alias
	 */
	public MatrizServiciosContactosData getContactoyServiciosPorAlias(Long idAlias, Boolean configuracionContacto) {
		MatrizServiciosContactosData matrizServiciosContactosData = new MatrizServiciosContactosData();
		Set<ServicioData> listaServicios = new HashSet<ServicioData>();
		Set<ContactosData> listaContactos = new HashSet<ContactosData>();
		Boolean isActive;
		CombinacionData combinacionData;
		List<ServiciosContactosData> serviciosContactosDatas = contactoDaoImpl.getContactoyServiciosPorAlias(idAlias,
				configuracionContacto);
		List<Servicios> servicios = serviciosBo.findServiciosByConfiguracionContacto(configuracionContacto);
		List<Contacto> contactos = findAllContactoByID_ALIAS(idAlias);
		for (Servicios servicio : servicios) {
			listaServicios.add(new ServicioData(servicio.getId(), servicio.getNombreServicio(),
					servicio.getDescripcionServicio()));
		}
		for (Contacto contacto : contactos) {
			listaContactos.add(new ContactosData(contacto.getId(), contacto.getNombre(), contacto.getApellido1(),
					contacto.getApellido2(), contacto.getTelefono1(), contacto.getEmail()));
		}
		matrizServiciosContactosData.setServicios(new ArrayList<ServicioData>(listaServicios));
		matrizServiciosContactosData.setContactos(new ArrayList<ContactosData>(listaContactos));
		for (ContactosData contacto : listaContactos) {
			combinacionData = new CombinacionData(contacto.getIdContacto());
			for (ServicioData servicio : listaServicios) {
				isActive = Boolean.FALSE;
				for (ServiciosContactosData serviciosContactosData : serviciosContactosDatas) {
					if (serviciosContactosData.getContacto().equals(contacto)
							&& serviciosContactosData.getServicio().equals(servicio)) {
						isActive = Boolean.TRUE;
					}
				}
				combinacionData.add(new ServicioActivoData(servicio.getIdServicio(), isActive));
			}
			matrizServiciosContactosData.add(combinacionData);
		}
		return matrizServiciosContactosData;
	}

	public List<Contacto> findContactosFiltro(Long idCliente, String nombre, String posicion, String telefono,
			String email, String fax, Long idAddress, String nombreNot, String posicionNot, String telefonoNot,
			String emailNot, String faxNot, Long idAddressNot) {
		return contactoDaoImpl.findContactosFiltro(idCliente, nombre, posicion, telefono, email, fax, idAddress, nombreNot, posicionNot, telefonoNot, emailNot, faxNot, idAddressNot);
	}

	/**
	 * Devuelve una lista de los contactos.
	 *
	 * @param aliasId
	 *            the alias id
	 * @return the lista
	 */
	public List<ContactoDTO> getLista(String cdaliass) {

		LOG.trace(" Llamada al metodo getLista del contactoBO ");
		List<Contacto> entities = (List<Contacto>) dao.findAllByAlias_Tmct0ali_Id_Cdaliass(cdaliass);
		List<ContactoDTO> resultList = new LinkedList<ContactoDTO>();

		for (Contacto entity : entities) {
			ContactoDTO dto = entityContactoToDTO(entity);
			resultList.add(dto);
		}

		return resultList;

	}

	/**
	 * Carga los datos de un objeto contacto en un DTO.
	 *
	 * @param entity
	 *            the entity
	 * @return the contacto dto
	 */
	public ContactoDTO entityContactoToDTO(Contacto entity) {

		LOG.trace(" Llamada al metodo entityContactoToDTO del contactoBO ");
		ContactoDTO dto = new ContactoDTO();

		if (entity != null) {
			dto.setId(entity.getId());
			dto.setCdAlias(entity.getAlias().getCdaliass());
			dto.setApellido1(entity.getApellido1());
			dto.setApellido2(entity.getApellido2());
			dto.setComentarios(entity.getComentarios());
			dto.setActivo(entity.getActivo());
			dto.setDefecto(entity.getDefecto());
			dto.setGestor(entity.getGestor());
			dto.setDescripcion(entity.getDescripcion());
			dto.setEmail(entity.getEmail());
			dto.setFax(entity.getFax());
			dto.setMovil(entity.getMovil());
			dto.setNombre(entity.getNombre());
			dto.setPosicionCargo(entity.getPosicionCargo());
			dto.setTelefono1(entity.getTelefono1());
			dto.setTelefono2(entity.getTelefono2());
			dto.setCdEtrali(entity.getCdEtrali());
		}

		return dto;

	}

	/**
	 * Busca todos los contactos por el alias.
	 *
	 * @param alias
	 *            the alias
	 * @return the list
	 */
	public List<Contacto> findAllByAlias(Alias alias) {

		LOG.trace(" LLamada al metodo findAllByAlias del contactoBO ");
		return this.dao.findAllByAlias(alias);

	}

	/**
	 * Como los metodos save y delete reciben los datos un objeto contacto, este
	 * metodo sirve para no duplicar codigo, se rellena aqui y se pasa a cada
	 * metodo el objeto creado.
	 *
	 * @param params
	 *            the params
	 * @return the contacto
	 */
	@Transactional
	public Contacto rellenarObjeto(Map<String, String> params) {

		LOG.debug(TAG + " Se inicia la creacion del objeto Contacto");

		Contacto contacto = new Contacto();
		contacto.setActivo(params.get(Constantes.PARAM_ACTIVO).toUpperCase().charAt(0));
		contacto.setAlias(this.findAliasByCdaliass(params));
		contacto.setApellido1(params.get(Constantes.PARAM_APELLIDO1));
		contacto.setApellido2(params.get(Constantes.PARAM_APELLIDO2));
		contacto.setCdEtrali(params.get(Constantes.PARAM_CDETRALI));
		contacto.setComentarios(params.get(Constantes.PARAM_COMENTARIOS));
		contacto.setDefecto(Boolean.valueOf(params.get(Constantes.PARAM_DEFECTO)));
		contacto.setGestor(Boolean.valueOf(params.get(Constantes.PARAM_CONTACTO_GESTOR)));
		// Insertar Contacto primera vez.
		LOG.debug(TAG + " va a llamar a contactoPrimeroInsertar");
		contactoPrimeraInsertar(contacto);
		contacto.setDescripcion(params.get(Constantes.PARAM_DESCRIPCION));
		contacto.setEmail(params.get(Constantes.PARAM_EMAIL));
		contacto.setFax(params.get(Constantes.PARAM_FAX));
		contacto.setMovil(params.get(Constantes.PARAM_MOVIL));
		contacto.setNombre(params.get(Constantes.PARAM_NOMBRE));
		contacto.setPosicionCargo(params.get(Constantes.PARAM_POSICIONCARGO));
		contacto.setTelefono1(params.get(Constantes.PARAM_TELEFONO1));
		contacto.setTelefono2(params.get(Constantes.PARAM_TELEFONO2));

		LOG.debug(" Se ha creado el objeto contacto ");

		return contacto;

	}

	/**
	 * Se da de alta un nuevo contacto con los datos pasados por parametro
	 * (params) en la tabla Contacto.
	 *
	 * @param params
	 *            the params
	 * @return the map
	 */
	@Transactional
	public Map<String, String> altaContacto(Map<String, String> params) throws Exception {

		LOG.debug(TAG + " Se inicia el alta de un contacto ");
		Map<String, String> resultado = new HashMap<String, String>();
		String result = "";
		LOG.debug(TAG + " Se va a rellenar el objeto contacto con los parámetros recibidos......");
		Contacto contacto = this.rellenarObjeto(params);
		LOG.debug(TAG + " Objeto contacto relleno e instanciado " + contacto.getId());
		LOG.debug(TAG + " Ahora se va a guardar el contacto ....");
		if (contacto.getId() != null && contacto.getId().longValue() > 0) {
			LOG.debug(TAG
					+ " El contacto ya existe en la base de datos, por lo tanto se recupera y se pone en el contexto de persistencia ....");
			contacto = dao.findOne(contacto.getId());
		}
		LOG.debug(TAG + " Contacto a dar de alta :: " + contacto.toString());
		if (dao.save(contacto) != null) {

			result = "Se ha creado el contacto";
			LOG.debug(TAG + " Se ha creado el contacto ");

		} else {

			result = "No se ha podido crear el contacto";
			LOG.debug(TAG + " No se ha podido crear el contacto ");
		}

		resultado.put("Resultado de la operacion:", result);
		return resultado;

	}

	/**
	 * En base a los parametros (params) que vienen del front, se elimina el
	 * contacto .
	 *
	 * @param params
	 *            the params
	 * @return the map
	 * @throws SIBBACBusinessException
	 *             the SIBBAC business exception
	 */
	@Transactional
	public Map<String, String> bajaContacto(Map<String, String> params) throws SIBBACBusinessException {

		LOG.trace(" Se inicia la baja de un contacto ");
		Map<String, String> resultado = new HashMap<String, String>();

		String result = "";
		Contacto contacto = this.findById(Long.parseLong(params.get(Constantes.PARAM_CONTACTO_ID)));
		if (contacto.getDefecto().equals(true)) {
			throw new SIBBACBusinessException(
					"No es posible eliminar este contacto ya que es un contacto por defecto.");

		} else {
			this.delete(contacto);

			result = "Se ha borrado el contacto";
			LOG.trace(" Se ha borrado el contacto ");
			resultado.put("Resultado de la operacion:", result);

		}

		return resultado;

		// this.delete( contacto );
		//
		// result = "Se ha borrado el contacto";
		// LOG.trace( " Se ha borrado el contacto " );
		// resultado.put( "Resultado de la operacion:", result );
		// return resultado;

	}

	/**
	 * Devuelve una lista de los contactos de la tabla Contacto.
	 *
	 * @param params
	 *            the params
	 * @return the lista contactos
	 */
	@Transactional
	public Map<String, ContactoDTO> getListaContactos(Map<String, String> params) {

		LOG.trace(" Se inicia el metodo getListaContactos ");
		Map<String, ContactoDTO> resultados = new HashMap<String, ContactoDTO>();
		final String cdaliass = StringUtils.trimToNull(params.get(Constantes.PARAM_CDALIASS));
		List<ContactoDTO> listaContactos = (List<ContactoDTO>) this.getLista(cdaliass);

		for (ContactoDTO contactoDTO : listaContactos) {
			resultados.put(contactoDTO.getId().toString(), contactoDTO);
		}

		return resultados;
	}

	/**
	 * Cambia el status del contacto segun los parametros recibidos, donde se le
	 * pasa el id del contacto y el nuevo valor del campo "activo".
	 *
	 * @param params
	 *            the params
	 * @return the map
	 */
	@Transactional
	public Map<String, String> cambioStatus(Map<String, String> params) {

		LOG.trace(" Se inicia el cambio de status de un contacto ");
		Map<String, String> resultado = new HashMap<String, String>();

		Contacto contacto = this.findById(Long.parseLong(params.get(Constantes.PARAM_CONTACTO_ID)));
		// contacto.setComentarios(params.get(Constantes.PARAM_COMENTARIOS));
		String n = String.valueOf(Constantes.PARAM_ACTIVO);
		contacto.setActivo(params.get(n).toUpperCase().charAt(0));
		// String yes = "s"; String no = "n";
		// if (n.equals(yes) || n.equals(no)) {
		// contacto.setActivo(params.get(n).charAt(0));
		// }else {
		// throw new IllegalArgumentException("Error parámetro");
		// }

		String result = "";

		if (this.save(contacto) != null) {

			result = "Se ha modificado el contacto";
			LOG.trace(" Se ha modificado el contacto ");

		} else {

			result = "No se modificado el contacto";
			LOG.trace(" No se modificado el contacto ");

		}

		resultado.put("result", result);
		return resultado;

	}

	/**
	 * Cambia un objeto contacto recuperado de la tabla contacto modificando los
	 * campos que se pasan por parametro (params).
	 *
	 * @param params
	 *            the params
	 * @return the map
	 */
	@Transactional
	public Map<String, String> modificacionContacto(Map<String, String> params) {

		LOG.trace(" Se inicia la modificacion de un contacto ");
		Map<String, String> resultado = new HashMap<String, String>();

		if (params == null) {
			return null;
		}

		// Contacto contacto = new Contacto();
		Contacto contacto = this.findById(Long.parseLong(params.get(Constantes.PARAM_CONTACTO_ID)));

		contacto.setApellido1(params.get(Constantes.PARAM_APELLIDO1));
		contacto.setApellido2(params.get(Constantes.PARAM_APELLIDO2));
		contacto.setComentarios(params.get(Constantes.PARAM_COMENTARIOS));
		contacto = this.findById(Long.parseLong(params.get(Constantes.PARAM_CONTACTO_ID)));
		contacto.setDefecto(Boolean.parseBoolean(params.get(Constantes.PARAM_DEFECTO)));
		contacto.setGestor(Boolean.parseBoolean(params.get(Constantes.PARAM_CONTACTO_GESTOR)));
		contacto.setActivo(params.get(Constantes.PARAM_ACTIVO).toUpperCase().charAt(0));

		// Busqueda para marcar contacto actual y desmarcar el anterior.
		contactoDefecto(contacto);
		contacto.setDescripcion(params.get(Constantes.PARAM_DESCRIPCION));
		contacto.setEmail(params.get(Constantes.PARAM_EMAIL));
		contacto.setFax(params.get(Constantes.PARAM_FAX));
		contacto.setMovil(params.get(Constantes.PARAM_MOVIL));
		contacto.setNombre(params.get(Constantes.PARAM_NOMBRE));
		contacto.setPosicionCargo(params.get(Constantes.PARAM_POSICIONCARGO));
		contacto.setTelefono1(params.get(Constantes.PARAM_TELEFONO1));
		contacto.setTelefono2(params.get(Constantes.PARAM_TELEFONO2));
		contacto.setCdEtrali(params.get(Constantes.PARAM_CDETRALI));

		String result = "";

		if (this.save(contacto) != null) {

			result = "Se ha modificado el contacto";
			LOG.trace(" Se ha modificado el contacto ");

		} else {

			result = "No se modificado el contacto";
			LOG.trace(" No se modificado el contacto ");

		}

		resultado.put("result", result);
		return resultado;

	}

	/**
	 * Lanza el UPDATE sobre la tabla Contacto.
	 *
	 * @param activo
	 *            the activo
	 * @param id
	 *            the id
	 * @return the contacto
	 */
	public Contacto changeStatusActive(char activo, final Long id) {

		LOG.trace(" Llamada al metodo changeStatusActive del contactoBO ");

		activo = Character.toUpperCase(activo);
		return this.dao.changeStatusActive(activo, id);
	}

	/**
	 * Este metodo devuelve un objeto Alias en base al id alias que se le pasa y
	 * que lleva el objeto Contacto.
	 *
	 * @param params
	 *            the params
	 * @return the alias
	 */
	public Alias findAliasByCdaliass(Map<String, String> params) {

		LOG.debug(TAG + " Llamada al metodo findAliasById del contactoBO ");
		return aliasBO.findByCdaliass(StringUtils.trimToEmpty(params.get(Constantes.PARAM_CDALIASS)));

	}

	/**
	 * Este método marca un contacto por defecto elegido por el usuario, luego
	 * busca y desmarca el que estaba marcado por defecto con anterioridad.
	 *
	 * @param contacto
	 *            the contacto
	 */

	@Transactional
	public void contactoDefecto(Contacto contacto) {

		LOG.trace(" Llamada al metodo contactoDefecto del contactoBO ");

		// List< Contacto > listaContactos = ( List< Contacto > ) findAll();
		List<Contacto> listaContactos = (List<Contacto>) dao.findAllByAlias_Id(contacto.getAlias().getId());

		int i;
		for (i = 0; i < listaContactos.size(); i++) {
			Contacto contactoActualLista = (Contacto) listaContactos.get(i);

			if (contactoActualLista.getAlias().getId().equals(contacto.getAlias().getId())
					&& contactoActualLista.getDefecto().equals(true)) {
				contactoActualLista.setDefecto(false);
				contacto.setDefecto(true);
				LOG.trace(" modificacion del contacto por defecto  de contactoDefecto del contactoBO ");
			}
		}

	}

	/**
	 * Contacto primera insertar.
	 *
	 * @param contacto
	 *            the contacto
	 */

	public void contactoPrimeraInsertar(Contacto contacto) {

		LOG.debug(" Llamada al metodo contactoPrimeraInsertar del contactoBO ");

		// List< Contacto > listaContactos = ( List< Contacto > ) findAll();
		List<Contacto> listaContactos = (List<Contacto>) dao.findAllByAlias_Id(contacto.getAlias().getId());
		int numeroContactoMismoAlias = 0;
		int i;
		for (i = 0; i < listaContactos.size(); i++) {
			Contacto contactoActualLista = (Contacto) listaContactos.get(i);

			if (contactoActualLista.getAlias().getId().equals(contacto.getAlias().getId())) {
				numeroContactoMismoAlias = numeroContactoMismoAlias + 1;

				LOG.debug(" Primer Contacto añadido por defecto. ");
			}

		}
		if (numeroContactoMismoAlias != 0) {
			contacto.setDefecto(false);
		} else {
			contacto.setDefecto(true);
		}
	}

	/**
	 * Find all contacto by i d_ alias.
	 *
	 * @param idAlias
	 *            the id alias
	 * @return the list
	 */
	public List<Contacto> findAllContactoByID_ALIAS(long idAlias) {

		return dao.findAllByAlias_Id(idAlias);
	}
	
	public Contacto findById(Long id) {
		return dao.findById(id);
	}

}
