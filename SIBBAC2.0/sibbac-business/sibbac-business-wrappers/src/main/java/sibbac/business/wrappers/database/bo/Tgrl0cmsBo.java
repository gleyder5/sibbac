package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0dbsBo;
import sibbac.business.fase0.database.bo.Tmct0xasBo;
import sibbac.business.wrappers.database.dao.Tgrl0cmsDao;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cms;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0cmsId;
import sibbac.database.bsnbpsql.tgrl.model.Tgrl0eje;

@Service
public class Tgrl0cmsBo extends AbstractBo<Tgrl0cms, Tgrl0cmsId, Tgrl0cmsDao> {

  @Autowired
  private Tmct0xasBo xasBo;

  @Autowired
  private Tmct0dbsBo dbsBo;

  public Tgrl0cmsBo() {
  }

  /**
   * 
   * @param tmct0ord orden
   * @param tgrl0eje ejecuciones
   * @param tmct0alosdo allocations
   * @param tipCom tipo comision
   * @param tpregcom tipo registro
   * @param anular true- cambia signo a importes para anular -false- no cambia
   * signo a importes
   * @param msgbundle mensajes
   */
  public Tgrl0cms mapping(Tmct0ord tmct0ord, Tgrl0eje tgrl0eje, Tmct0alo tmct0alosdo, String tipCom, String tpregcom,
      boolean anular) throws Exception {

    Tgrl0cms cms = new Tgrl0cms();
    cms.setId(new Tgrl0cmsId(tgrl0eje.getId().getNuoprout(), tgrl0eje.getId().getNupareje(), null));
    cms.setTgrl0eje(tgrl0eje);

    boolean foreignMarket = 'E' == tmct0ord.getCdclsmdo();
    /*
     * ***************** recuperacion objetos *****************
     */

    /*
     * ***************** mapeo objetos *****************
     */
    /*
     * Tipo comision: SVB = Comision SVB BRK = Comision Broker BAN = Comision
     * Banco CUS = Comision Custodio DEV = Comision Devolucion
     */
    cms.setImregcom(new BigDecimal(0));
    cms.setImregdiv(new BigDecimal(0));
    if ("SVB".equals(tipCom)) {
      if (foreignMarket) {
        cms.setCdsvbbrk('P');
      }
      else {
        cms.setCdsvbbrk(tgrl0eje.getCdcomisn());
      }
      if (anular) {
        cms.setImregcom(tmct0alosdo.getImsvbeu().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
        cms.setImregdiv(tmct0alosdo.getImsvb().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
      }
      else {
        cms.setImregcom(tmct0alosdo.getImsvbeu().setScale(2, RoundingMode.HALF_UP));
        cms.setImregdiv(tmct0alosdo.getImsvb().setScale(2, RoundingMode.HALF_UP));
      }
    }
    else {
      if ("BRK".equals(tipCom)) {
        if (foreignMarket) {
          cms.setCdsvbbrk('K');
        }
        else {
          cms.setCdsvbbrk('B');
        }
        if (anular) {
          cms.setImregcom(tmct0alosdo.getImfibreu().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
          cms.setImregdiv(tmct0alosdo.getImfibrdv().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
        }
        else {
          cms.setImregcom(tmct0alosdo.getImfibreu().setScale(2, RoundingMode.HALF_UP));
          cms.setImregdiv(tmct0alosdo.getImfibrdv().setScale(2, RoundingMode.HALF_UP));
        }
      }
      else {
        if ("BAN".equals(tipCom)) {
          if (foreignMarket) {
            cms.setCdsvbbrk(' ');
          }
          else {
            cms.setCdsvbbrk('N');
          }
          if (anular) {
            cms.setImregcom(tmct0alosdo.getImcomieu().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
            cms.setImregdiv(tmct0alosdo.getImcomisn().setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(-1)));
          }
          else {
            cms.setImregcom(tmct0alosdo.getImcomieu().setScale(2, RoundingMode.HALF_UP));
            cms.setImregdiv(tmct0alosdo.getImcomisn().setScale(2, RoundingMode.HALF_UP));
          }
        }
        else {
          if ("CUS".equals(tipCom)) {
            if (foreignMarket) {
              cms.setCdsvbbrk('C');
            }
            else {
              cms.setCdsvbbrk(' ');
            }
          }
          else {
            if ("DEV".equals(tipCom)) {
              if (foreignMarket) {
                cms.setCdsvbbrk('V');
              }
              else {
                cms.setCdsvbbrk('D');
              }
              if (anular) {
                cms.setImregcom(tmct0alosdo.getEucomdev().setScale(2, RoundingMode.HALF_UP)
                    .multiply(new BigDecimal(-1)));
                cms.setImregdiv(tmct0alosdo.getImcomdev().setScale(2, RoundingMode.HALF_UP)
                    .multiply(new BigDecimal(-1)));
              }
              else {
                cms.setImregcom(tmct0alosdo.getEucomdev().setScale(2, RoundingMode.HALF_UP));
                cms.setImregdiv(tmct0alosdo.getImcomdev().setScale(2, RoundingMode.HALF_UP));
              }
            }
          }
        }
      }
    }

    /*
     * NUOPROUT N� Operaci�n Routing TGRL0EJE.NUOPROUT
     */
    cms.getId().setNuoprout(tgrl0eje.getId().getNuoprout());

    /*
     * NUPAREJE N� Operaci�n de ejecuci�n TGRL0EJE.NUPAREJE
     */
    cms.getId().setNupareje(tgrl0eje.getId().getNupareje());

    /*
     * NUREGCOM N� Registro comisi�n Contador Secuencial
     */
    // setNuregcom(nuregcom);
    cms.getId().setNuregcom(getId(tgrl0eje.getId().getNuoprout(), tgrl0eje.getId().getNupareje()));
    // setNuregcom(new BigDecimal(0));
    /*
     * TPREGCOM Tipo registro comisi�n "A" -> Inicial "C" -> Cobrada "D" ->
     * Diferencia Mercado Extranjero cuando se haya realizado el cobro por
     * ficheros recibidos 0036 y 0049
     */
    cms.setTpregcom(tpregcom.charAt(0));

    /*
     * FHMOVCOM F. Movimiento, liquidaciones, procesos Fecha sistema (aaaammdd)
     */
    cms.setFhmovcom(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(new Date())));

    /*
     * FHCOLICM F. entrada del registro en la B.Datos Fecha sistema (aaaammdd)
     */
    cms.setFhcolicm(FormatDataUtils.convertStringToInteger(FormatDataUtils.convertDateToString(new Date())));

    /*
     * CDESTADO Estado "A" -> Alta "B" -> Baja
     */
    cms.setCdestado('A');

    /*
     * CDUSRCOM Usuario que realiza el alta Seg�n TGRL0CMS.TPREGCOM "ROUTING" ->
     * TPREGCOM = "A" "LIQ-0036" -> TPREGCOM = "C" (solo M.Extranjero)
     * "LIQ-0049" -> TPREGCOM = "C" (solo M.Extranjero)
     */
    if (tpregcom.equalsIgnoreCase("A")) {
      cms.setCdusrcom("ROUTING");
    }
    else if (tpregcom.equalsIgnoreCase("C")) {
      // setCdusrcom("LIQ-0036");
      cms.setCdusrcom("LIQ-" + getClearer(tmct0alosdo.getCdcleare()).trim());
    }

    /*
     * FHENTCON F. entrada en contabilidad Cero
     */
    cms.setFhentcon(0);

    /*
     * CDMONISO Divisa (ISO 3 posiciones alfanum�ricas) TMCT0ALO.CDMONISO
     */
    cms.setCdmoniso(tmct0alosdo.getCdmoniso());
    cms = save(cms);
    dbsBo.insertRegister(cms, true);
    return cms;
  }

  /**
   * RECUPERAR CLEARER
   * 
   * @param clearer allocation del booking
   * @return clearer el clearer convertido
   * 
   * @generated
   */
  private String getClearer(String clearer) {

    String clearerRes = xasBo.getCdcleareClearer(clearer);
    if (StringUtils.isBlank(clearerRes)) {
      clearerRes = "0036";
    }
    return clearerRes;
  }

  /**
   * 
   * @param nuoprout
   * @param nupareje
   * @return
   */
  public short getId(int nuoprout, Short nupareje) {
    Short nuregcom = dao.findMaxNuregcomByNuoproutAndNupareje(nuoprout, nupareje);
    if (nuregcom == null) {
      nuregcom = (short) 0;
    }
    return ++nuregcom;
  }
}
