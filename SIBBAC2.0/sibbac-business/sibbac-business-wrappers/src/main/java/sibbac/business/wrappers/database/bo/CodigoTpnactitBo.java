package sibbac.business.wrappers.database.bo;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.CodigoTpnactitDao;
import sibbac.business.wrappers.database.model.CodigoTpnactit;
import sibbac.database.bo.AbstractBo;

@Service
public class CodigoTpnactitBo extends
		AbstractBo<CodigoTpnactit, String, CodigoTpnactitDao> {

	public CodigoTpnactit findByCodigo(String codigo) {
		CodigoTpnactit tpnactit = null;
		try {
			tpnactit = dao.findByCodigo(codigo);
		} catch (Exception e) {
			tpnactit = null;
		}

		return tpnactit;
	}
}
