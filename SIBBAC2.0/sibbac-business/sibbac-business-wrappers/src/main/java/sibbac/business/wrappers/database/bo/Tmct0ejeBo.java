package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0ejeDao;
import sibbac.business.wrappers.database.data.Tmct0ejeData;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0ejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0desgloseclitit;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ejeFeeSchema;
import sibbac.business.wrappers.database.model.Tmct0ejeId;
import sibbac.business.wrappers.database.model.Tmct0ejecucionalocation;
import sibbac.business.wrappers.database.tmct0eje.dto.Tmct0ejeInformacionMercadoDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ejeBo extends AbstractBo<Tmct0eje, Tmct0ejeId, Tmct0ejeDao> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ejeBo.class);

  @Autowired
  private Tmct0logBo logBo;

  @Autowired
  private Tmct0ejecucionalocationBo ejeAloBo;

  @Autowired
  private Tmct0desgloseclititBo dctBo;

  /******************************************************** QUERYS ***************************************************************************/

  public Tmct0ejeFeeSchema findTmct0ejeFeeSchemaById(Tmct0ejeId id) {
    return dao.findTmct0ejeFeeSchemaById(id);
  }

  public List<Tmct0eje> findByCaseId(Long caseId) {
    return dao.findByCaseId(caseId);
  }

  public List<Tmct0eje> findAllByNbookingAndNuorden(String nbooking, String nuorden) {
    return dao.findAllByNbookingAndNuorden(nbooking, nuorden);
  }

  public List<Tmct0eje> findAllByNbookingAndNuordenAndNucnfclt(String nbooking, String nuorden, String nucnfclt) {
    return dao.findAllByNbookingAndNuordenAndNucnfclt(nbooking, nuorden, nucnfclt);
  }

  public List<Tmct0ejeData> findAllTmct0ejeDataByNbookingAndNuorden(String nbooking, String nuorden) {
    return dao.findAllTmct0ejeDataByNbookingAndNuorden(nbooking, nuorden);
  }

  public List<Tmct0eje> findAllByNbookingAndNuordenAndNuejecuc(String nbooking, String nuorden, String nuejecuc) {
    return dao.findAllByNbookingAndNuordenAndNuejecuc(nbooking, nuorden, nuejecuc);
  }

  public List<Tmct0ejeInformacionMercadoCaseDTO> findAllCasePendingSinceDate(Date fechaEjecucion) {
    return dao.findAllCasePendingSinceDate(fechaEjecucion);
  }

  public Integer updateCasadoPti(String nuorden, String nbooking, String nuejecuc, long idcase) {
    return dao.updateCasadoPti(nuorden, nbooking, nuejecuc, idcase, new Date());
  }

  public Integer countNoCasadoPti(String nuorden, String nbooking) {
    return dao.countNoCasadoPti(nuorden, nbooking);
  }

  /******************************************************** QUERYS ***************************************************************************/

  /******************************************************** BUSSINES METHODS ***************************************************************************/

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void agruparEjecucionesArreglarDesgloses(Tmct0bokId bokId, int pageSize) throws SIBBACBusinessException {
    LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] inicio {}", bokId);
    LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] buscando ejecuciones duplicadas {} ...", bokId);
    boolean agrupadoEjeAlo;
    List<Tmct0eje> ejes;
    Tmct0eje ejeAgrupar;
    int countAgrupados;
    List<Tmct0ejecucionalocation> ejeAlos;
    List<Tmct0ejecucionalocation> ejeAlosAgrupar = null;
    List<Tmct0desgloseclitit> dcts;
    String msgLog;
    BigDecimal nutitDesglosesClitit;
    boolean ejeAloConTitulos;
    List<Tmct0ejeInformacionMercadoDTO> infoMercadoEjecucionesDuplicadas = dao
        .findDistinctTmct0ejeInformacionMercadoDTOEjecucionesDuplicadasByNuordenAndNbooking(bokId.getNuorden(),
            bokId.getNbooking());

    LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] encontradas {} ejecuciones duplicadas {} ...",
        infoMercadoEjecucionesDuplicadas, bokId);

    if (CollectionUtils.isEmpty(infoMercadoEjecucionesDuplicadas)) {
      LOG.info("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] las ejecuciones estan bien.");
      logBo.insertarRegistro(bokId.getNuorden(), bokId.getNbooking(),
          "CUADRE_ECC-ARREGLO_DESGLOSES-No hay ejecuciones disociadas por fidessa.", "", "CECC_AR_EJ");
    }
    else {
      LOG.info("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] las ejecuciones estan bien.");
      logBo.insertarRegistro(bokId.getNuorden(), bokId.getNbooking(),
          "CUADRE_ECC-ARREGLO_DESGLOSES-INCIDENCIA-Hay ejecuciones disociadas por fidessa.", "", "CECC_AR_EJ");

      for (Tmct0ejeInformacionMercadoDTO imDto : infoMercadoEjecucionesDuplicadas) {
        LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] buscando ejecuciones de {} duplicadas {} ...",
            bokId, imDto);
        ejes = dao
            .findAllByCdisinAndFeejecucAndCdtpoperAndClearingplatformAndSegmenttradingvenueAndTpopebolAndCdMiembroMktAndNuopemerAndNurefereAndIdNuordenAndIdNbooking(
                imDto.getCdisin(), imDto.getFeejecuc(), imDto.getCdtpoper(), imDto.getClearingplatform(),
                imDto.getSegmenttradingvenue(), imDto.getTpopebol(), imDto.getCdMiembroMkt(), imDto.getNuopemer(),
                imDto.getNurefere(), bokId.getNuorden(), bokId.getNbooking());

        LOG.debug(
            "[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] encontradas {} ejecuciones de {} duplicadas {} ...",
            ejes.size(), bokId, imDto);
        if (CollectionUtils.isNotEmpty(ejes) && ejes.size() > 1) {
          ejeAgrupar = null;
          ejeAloConTitulos = false;
          for (Tmct0eje eje : ejes) {

            if (ejeAgrupar == null) {
              ejeAgrupar = eje;
            }
            else {
              LOG.debug(
                  "[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] los titulos {} de la eje {} se van a acumular en {} que tiene {} titulos",
                  eje.getNutiteje(), eje.getId(), ejeAgrupar.getId(), ejeAgrupar.getNutiteje());
              ejeAgrupar.setNutiteje(ejeAgrupar.getNutiteje().add(eje.getNutiteje()));
              LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] la ejecucion {} ahora tiene {} titulos",
                  ejeAgrupar.getId(), ejeAgrupar.getNutiteje());
              ejeAlosAgrupar = ejeAgrupar.getTmct0ejecucionalocations();
              ejeAlos = eje.getTmct0ejecucionalocations();
              countAgrupados = 0;
              for (Tmct0ejecucionalocation ea : ejeAlos) {
                agrupadoEjeAlo = false;

                for (Tmct0ejecucionalocation eaAgrupado : ejeAlosAgrupar) {
                  if (ea.getTmct0grupoejecucion().getIdgrupoeje() == eaAgrupado.getTmct0grupoejecucion()
                      .getIdgrupoeje()) {
                    eaAgrupado.setNutitulos(eaAgrupado.getNutitulos().add(ea.getNutitulos()));
                    eaAgrupado.setAuditFechaCambio(new Date());
                    if (eaAgrupado.getNutitulosPendientesDesglose().compareTo(BigDecimal.ZERO) < 0) {
                      eaAgrupado.setNutitulosPendientesDesglose(BigDecimal.ZERO);
                    }
                    if (ea.getNutitulosPendientesDesglose().compareTo(BigDecimal.ZERO) > 0) {
                      eaAgrupado.setNutitulosPendientesDesglose(eaAgrupado.getNutitulosPendientesDesglose().add(
                          ea.getNutitulosPendientesDesglose()));
                    }

                    if (eaAgrupado.getNutitulosPendientesDesglose().compareTo(BigDecimal.ZERO) < 0) {
                      eaAgrupado.setNutitulosPendientesDesglose(BigDecimal.ZERO);
                    }
                    agrupadoEjeAlo = true;
                    countAgrupados++;
                    if (eaAgrupado.getNutitulosPendientesDesglose().compareTo(BigDecimal.ZERO) > 0) {
                      ejeAloConTitulos = true;
                    }
                  }
                }

                if (!agrupadoEjeAlo) {
                  LOG.warn("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] no se agrupado ejealo {}.",
                      ea.getIdejecucionalocation(), bokId, imDto);

                }
              }// fir agrupar ejealos
              if (countAgrupados == ejeAlos.size()) {

                msgLog = MessageFormat.format("CUADRE_ECC-ARREGLAR_DESGLOSES-La ejecucion {0} se a agrupado con {1}",
                    eje.getId().getNuejecuc(), ejeAgrupar.getId().getNuejecuc());
                logBo.insertarRegistro(bokId.getNuorden(), bokId.getNbooking(), msgLog, "", "CECC_AR_EJ");
                LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] borrando ejealos: {}", ejeAlos);
                ejeAloBo.delete(ejeAlos);
                LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] borrando eje: {}", eje.getId());
                delete(eje);
              }
              else {
                msgLog = MessageFormat
                    .format(
                        "CUADRE_ECC-ARREGLAR_DESGLOSES-INCIDENCIA- La ejeID: {0} no se ha podido agrupar con ninguna otra ejecucion.",
                        eje.getId().getNuejecuc());
                throw new SIBBACBusinessException(msgLog);
              }

            }
          }// fin for ejes

          Pageable page = new PageRequest(0, pageSize);

          do {

            LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] buscando dct's de alta...");
            dcts = dctBo.findAllByDatosMercadoAndNuordenAndNbooking(imDto.getCdisin(), imDto.getFeejecuc(),
                imDto.getCdtpoper(), imDto.getClearingplatform(), imDto.getSegmenttradingvenue(), imDto.getTpopebol(),
                imDto.getCdMiembroMkt(), imDto.getNuopemer(), imDto.getNurefere(), bokId.getNuorden(),
                bokId.getNbooking(), page);
            if (CollectionUtils.isNotEmpty(dcts)) {
              page = page.next();
              nutitDesglosesClitit = BigDecimal.ZERO;
              for (Tmct0desgloseclitit dct : dcts) {
                if (dct.getNuejecuc() == null || !dct.getNuejecuc().equals(ejeAgrupar.getId().getNuejecuc())) {
                  msgLog = MessageFormat.format(
                      "CUADRE_ECC-ARREGLAR_DESGLOSES-Actualizado el desglose {0} de nuejecuc {1} a {2}",
                      dct.getNudesglose(), dct.getNuejecuc(), ejeAgrupar.getId().getNuejecuc());
                  logBo.insertarRegistro(bokId.getNuorden(), bokId.getNbooking(), msgLog, "", "CECC_AR_EJ");
                  dct.setNuejecuc(ejeAgrupar.getId().getNuejecuc());
                  dct.setAuditFechaCambio(new Date());
                  dctBo.save(dct);
                }
                nutitDesglosesClitit = nutitDesglosesClitit.add(dct.getImtitulos());

              }// fin for dcts

              if (ejeAloConTitulos) {
                if (ejeAgrupar.getNutiteje().compareTo(nutitDesglosesClitit) == 0) {
                  if (CollectionUtils.isNotEmpty(ejeAlosAgrupar)) {
                    for (Tmct0ejecucionalocation eaAgrupado : ejeAlosAgrupar) {
                      eaAgrupado.setNutitulosPendientesDesglose(BigDecimal.ZERO);
                      eaAgrupado.setAuditFechaCambio(new Date());
                    }
                  }
                }
              }

            }
          }
          while (dcts.size() == page.getPageSize());
          ejeAgrupar = save(ejeAgrupar);
          ejeAloBo.save(ejeAgrupar.getTmct0ejecucionalocations());
        }
        else {
          LOG.warn(
              "[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] las ejecuciones estan NO estan duplicadas {}-{}.",
              bokId, imDto);
        }
      }// fin for im duplicados
    }

    LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] buscando los grupos de ejcucion para revisar los titulos...");

    ejeAloBo.recalcularTitulosPendientesDesglose(bokId);

    LOG.debug("[Tmct0ejeBo :: agruparEjecucionesArreglarDesgloses] fin {}", bokId);
  }
  /******************************************************** BUSSINES METHODS ***************************************************************************/

}
