package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.Monedas;


@Component
public interface MonedasDao extends JpaRepository< Monedas, String > {

	public Monedas findByCodigo( String codigo );
}
