package sibbac.business.wrappers.database.bo;

import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0ctoDao;
import sibbac.business.wrappers.database.model.Tmct0cto;
import sibbac.business.wrappers.database.model.Tmct0ctoId;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ctoBo extends AbstractBo<Tmct0cto, Tmct0ctoId, Tmct0ctoDao> {
  public List<Tmct0cto> findAllByNuorden(String nuorden) {
    return dao.findAllByNuorden(nuorden);
  }
}
