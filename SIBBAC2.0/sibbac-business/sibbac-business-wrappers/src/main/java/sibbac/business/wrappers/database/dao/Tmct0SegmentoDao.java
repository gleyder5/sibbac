package sibbac.business.wrappers.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0CamaraCompensacion;
import sibbac.business.wrappers.database.model.Tmct0Segmento;;

@Repository
public interface Tmct0SegmentoDao  extends JpaRepository< Tmct0Segmento,Integer>{
	
	@Query("SELECT segmento FROM Tmct0Segmento segmento WHERE segmento.nbDescripcion=:nbDescripcion ")
	public Tmct0Segmento findByNbDescripcion(@Param("nbDescripcion") String nbDescripcion );

}
