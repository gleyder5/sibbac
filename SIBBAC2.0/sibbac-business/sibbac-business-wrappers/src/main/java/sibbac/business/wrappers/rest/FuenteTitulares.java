package sibbac.business.wrappers.rest;

import java.util.List;

import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.model.PartenonRecordBean;
import sibbac.business.wrappers.database.model.Tmct0afiId;

public interface FuenteTitulares {

  TitularesDTO getTitular();

  List<Tmct0afiId> getListAfiIds();
  
  List<Long> getListAfiErrorIds();
  
  List<PartenonRecordBean> listTitulares();
  
  List<TitularesDTO> listTitularesDto();
  
}