package sibbac.business.wrappers.database.bo;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.LineaDeCobroDao;
import sibbac.business.wrappers.database.dto.FacturaLineaDeCobrosDTO;
import sibbac.business.wrappers.database.model.Cobros;
import sibbac.business.wrappers.database.model.LineaDeCobro;
import sibbac.database.bo.AbstractBo;


@Service
public class LineaDeCobroBo extends AbstractBo< LineaDeCobro, Long, LineaDeCobroDao > {

	protected static final Logger	LOG	= LoggerFactory.getLogger( LineaDeCobroBo.class );

	@Autowired
	private CobrosBo				cobrosBo;

	public Map< String, FacturaLineaDeCobrosDTO > getFacturaLineaDeCobros( Long idCobros ) {

		Map< String, FacturaLineaDeCobrosDTO > ListaFacturaLineaDeCobrosDTO = new HashMap< String, FacturaLineaDeCobrosDTO >();
		List< LineaDeCobro > lista = null;
		// List<LineaDeCobro> lista = this.dao.findFacturaLineaDeCobrosByIdCobros(idCobros);
		try {
			Cobros cobro = cobrosBo.findById( idCobros );
			if ( cobro != null ) {
				lista = this.dao.findAllByCobro( cobro );
			}
		} catch ( Exception e ) {
			LOG.error( e.getMessage() );
		}

		for ( LineaDeCobro lineaDeCobro : lista ) {
			FacturaLineaDeCobrosDTO dto = facturaLineaDeCobrosDataToFacturaLineaDeCobrosDTO( lineaDeCobro );
			ListaFacturaLineaDeCobrosDTO.put( lineaDeCobro.getId().toString(), dto );
		}

		return ListaFacturaLineaDeCobrosDTO;
	}

	public FacturaLineaDeCobrosDTO facturaLineaDeCobrosDataToFacturaLineaDeCobrosDTO( LineaDeCobro lineaDeCobro ) {

		FacturaLineaDeCobrosDTO dto = null;

		if ( lineaDeCobro != null ) {
			dto = new FacturaLineaDeCobrosDTO();

			dto.setFechaFactura( lineaDeCobro.getFactura().getFechaFactura() );
			dto.setIdCobros( lineaDeCobro.getCobro().getId() );
			dto.setIdFactura( lineaDeCobro.getFactura().getId() );
			dto.setIdLineaDeCobro( lineaDeCobro.getId() );
			dto.setImporte( lineaDeCobro.getImporte() );
			dto.setImporteAplicado( lineaDeCobro.getImporteAplicado() );
			dto.setNumeroFactura( lineaDeCobro.getFactura().getNumeroFactura() );
		}

		return dto;
	}

}
