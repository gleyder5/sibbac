package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.dto.DatosAsignacionDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.business.wrappers.database.model.Tmct0movimientoecc;

@Repository
public interface Tmct0movimientoeccDao extends JpaRepository<Tmct0movimientoecc, Long> {

  @Query(value = "SELECT NEXT VALUE FOR MO_PTI_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  BigDecimal getNewMOPTISequence();

  List<Tmct0movimientoecc> findAllByCdrefmovimientoecc(String cdrefmovimientoecc);

  @Query("select m from Tmct0movimientoecc m where m.cdrefmovimientoecc = :cdrefmovimientoecc and m.cdestadomov in (:cdestamodosmov)")
  List<Tmct0movimientoecc> findAllByCdrefmovimientoeccAndInCdestadosmov(
      @Param("cdrefmovimientoecc") String cdrefmovimientoecc, @Param("cdestamodosmov") List<String> cdestadomovActivos);

  List<Tmct0movimientoecc> findAllByCdrefmovimiento(String cdrefmovimiento);

  @Query("select m from Tmct0movimientoecc m where m.cdrefmovimientoecc = :cdrefmovimientoecc "
      + " and m.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 and m.tmct0desglosecamara.feliquidacion = :feliquidacion")
  List<Tmct0movimientoecc> findAllByCdrefmovimientoeccAndFeliquidacionAndDesgloseActivo(
      @Param("cdrefmovimientoecc") String cdrefmovimientoecc, @Param("feliquidacion") Date feliquidacion);

  @Query("select m from Tmct0movimientoecc m where m.cdrefmovimientoecc = :cdrefmovimientoecc "
      + " and  m.tmct0alo.tmct0estadoByCdestadoasig.idestado > 0 and m.tmct0alo.fevalor = :feliquidacion ")
  List<Tmct0movimientoecc> findAllByCdrefmovimientoeccAndFeliquidacionAndAloActivo(
      @Param("cdrefmovimientoecc") String cdrefmovimientoecc, @Param("feliquidacion") Date feliquidacion);

  @Query("select distinct m.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id from Tmct0movimientoecc m where m.cdestadoasig = :cdestadoasig "
      + " and m.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv ")
  List<Tmct0bokId> findAllBookingIdByMovimientoeccCdestadoasigFromDesgloseCamara(
      @Param("cdestadoasig") int cdestadoasigmovimiento, @Param("isscrdiv") char isScrdiv);

  @Query("select distinct m.tmct0alo.tmct0bok.id from Tmct0movimientoecc m where m.cdestadoasig = :cdestadoasig "
      + " and m.tmct0alo.tmct0bok.tmct0ord.isscrdiv = :isscrdiv ")
  List<Tmct0bokId> findAllBookingIdByMovimientoeccCdestadoasigFromTmct0alo(
      @Param("cdestadoasig") int cdestadoasigmovimiento, @Param("isscrdiv") char isScrdiv);

  @Query("select distinct m.tmct0desglosecamara.iddesglosecamara "
      + " from Tmct0movimientoecc m where m.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id = :tmct0bokId and m.cdestadoasig = :cdestadoasig ")
  List<Long> findAllIddesglosecamaraByTmct0bokIdAndMovimientoeccCdestadoasig(
      @Param("tmct0bokId") Tmct0bokId tmct0bokId, @Param("cdestadoasig") int cdestadoasigmovimiento);

  @Query("select distinct m.tmct0alo.id "
      + " from Tmct0movimientoecc m where m.tmct0alo.tmct0bok.id = :tmct0bokId and m.cdestadoasig = :cdestadoasig ")
  List<Tmct0aloId> findAllTmct0aloIdByTmct0bokIdAndMovimientoeccCdestadoasig(
      @Param("tmct0bokId") Tmct0bokId tmct0bokId, @Param("cdestadoasig") int cdestadoasigmovimiento);

  /***
   * Funcion que devuelve los movimientosECC que tienen el flag
   * "enviadocuentacirtual" a '0' o a NULL Y en estado cdestadomov
   * 
   * @param cdestadomov
   * @return
   */
  @Query(value = "SELECT o FROM Tmct0movimientoecc o"
      + " WHERE (o.enviadocuentavirtual = '0' OR o.enviadocuentavirtual IS NULL) "
      + " AND o.cdestadomov = :cdestadomov ")
  List<Tmct0movimientoecc> findAllNoEnviadosCuentaVirtualByCdestadomov(@Param(value = "cdestadomov") String cdestadomov);

  /***
   * Funcion que devuelve los movimientosECC que tienen el flag
   * "enviadocuentacirtual" a '0' o a NULL Y en estado cdestadomov
   * 
   * @param cdestadomov
   * @return
   */
  // @Query( value = "SELECT o FROM Tmct0movimientoecc o" +
  // " WHERE (o.enviadoCuentaAlias = false OR o.enviadoCuentaAlias IS NULL) "
  // + " AND o.cdestadomov = :cdestadomov " )
  @Query(value = "SELECT o FROM Tmct0movimientoecc o" + " WHERE o.cdestadomov = :cdestadomov ")
  List<Tmct0movimientoecc> findAllNoEnviadosCuentaAliasByCdestadomov(@Param(value = "cdestadomov") String cdestadomov);

  /***
   * Funcion que devuelve los datos para insertar en
   * TMCT0_MOVIMIENTO_CUENTA_VIRTUAL, agrupando por: movECC.isin,
   * desgCamara.fecontratacion, movECC.sentido, infoComp.cdctacompensacion,
   * alo.cdaliass, camaraComp.cd_codigo, desgClitit.precio para aquellos
   * movimientosECC en estado "cdestadomov" (q se pasa como parametro) y que NO
   * hayan sido tratados ya
   * 
   * @param cdestadomov
   * @return
   */

  /***
   * Funcion que devuelve un movimientoECC cuto iddesglsoecamara se pasa como
   * parametro
   * 
   * @param iddesglosecamara
   * @return el movimientoECC cuyo iddesglosecamara es iddesglosecamara
   */
  @Query(value = "SELECT m FROM Tmct0movimientoecc m WHERE "
      + "m.tmct0desglosecamara.iddesglosecamara = :iddesglosecamara")
  List<Tmct0movimientoecc> findByIdDesgloseCamara(@Param(value = "iddesglosecamara") Long iddesglosecamara);

  @Query(value = "SELECT m FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.iddesglosecamara = :iddesglosecamara and m.cdestadomov != :notCdestadomov")
  List<Tmct0movimientoecc> findByIdDesgloseCamaraAndNotCdestadomov(
      @Param(value = "iddesglosecamara") Long iddesglosecamara, @Param(value = "notCdestadomov") String cdestadomov);

  /***
   * Funcion que devuelve un movimientoECC cuto iddesglsoecamara se pasa como
   * parametro
   * 
   * @param iddesglosecamara
   * @return el movimientoECC cuyo iddesglosecamara es iddesglosecamara
   */
  @Query(value = "SELECT m FROM Tmct0movimientoecc m WHERE " + "m.tmct0alo.id = :aloId")
  List<Tmct0movimientoecc> findByTmct0aloId(@Param(value = "aloId") Tmct0aloId aloId);

  /**
   * 
   * @param iddesglosecamara
   * @return
   */
  @Query(value = "SELECT distinct (m.cuentaCompensacionDestino) FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.iddesglosecamara IN (:iddesglosescamara) AND m.cdestadomov = '9'")
  List<String> findDistinctCuentaCompensacionByIdDesglosesCamara(
      @Param("iddesglosescamara") List<Long> iddesglosescamara);

  @Query(value = "SELECT distinct new sibbac.business.wrappers.database.dto.DatosAsignacionDTO (m.cuentaCompensacionDestino, m.miembroCompensadorDestino, m.codigoReferenciaAsignacion) "
      + " FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.iddesglosecamara =:iddesglosescamara AND m.cdestadomov = '9'")
  List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigByIddesglosecamara(
      @Param("iddesglosescamara") Long iddesglosescamara);

  @Query(value = "SELECT count(mn) "
      + " FROM Tmct0movimientooperacionnuevaecc mn, MovimientoEccS3 ms3 WHERE mn.cdoperacionecc = ms3.cdoperacionecc "
      + " AND mn.tmct0movimientoecc.tmct0desglosecamara.iddesglosecamara =:iddesglosescamara "
      + " AND mn.tmct0movimientoecc.cdestadomov = '9'")
  int countMovimientosImputadosS3ByIdesglosecamara(@Param("iddesglosescamara") Long iddesglosescamara);

  @Query(value = "SELECT count(mn) "
      + " FROM Tmct0movimientooperacionnuevaecc mn, MovimientoEccS3 ms3 WHERE mn.cdoperacionecc = ms3.cdoperacionecc "
      + " AND mn.tmct0movimientoecc.tmct0desglosecamara.tmct0alc.id =:alcId "
      + " AND mn.tmct0movimientoecc.cdestadomov = '9' "
      + " AND mn.tmct0movimientoecc.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0")
  int countMovimientosImputadosS3ByAlcId(@Param("alcId") Tmct0alcId alcId);

  @Query(value = "SELECT distinct new sibbac.business.wrappers.database.dto.DatosAsignacionDTO (m.cuentaCompensacionDestino, m.miembroCompensadorDestino, m.codigoReferenciaAsignacion) "
      + " FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.tmct0alc.id = :alcId AND m.cdestadomov = '9' AND m.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 "
      + " and m.tmct0desglosecamara.tmct0alc.cdestadoasig > 0 ")
  List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigByAlcId(@Param("alcId") Tmct0alcId alcId);

  @Query(value = "SELECT distinct new sibbac.business.wrappers.database.dto.DatosAsignacionDTO (m.cuentaCompensacionDestino, m.miembroCompensadorDestino, m.codigoReferenciaAsignacion) "
      + " FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.tmct0alc.tmct0alo.tmct0bok.id =:bookId AND m.cdestadomov = '9' ")
  List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigFromDesglosesCamaraByBookingId(
      @Param("bookId") Tmct0bokId bookingId);

  @Query(value = "SELECT distinct new sibbac.business.wrappers.database.dto.DatosAsignacionDTO (m.cuentaCompensacionDestino, m.miembroCompensadorDestino, m.codigoReferenciaAsignacion) "
      + " FROM Tmct0movimientoecc m WHERE m.tmct0alo.tmct0bok.id =:bookId AND m.cdestadomov = '9' ")
  List<DatosAsignacionDTO> findAllDistinctCtaMiembroRefAsigFromAloByBookingId(@Param("bookId") Tmct0bokId bookingId);

  /**
   * 
   * @param iddesglosecamara
   * @return
   */
  @Query(value = "SELECT distinct (m.cuentaCompensacionDestino) FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.tmct0alc.id = :alcId "
      + " AND m.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 AND m.cdestadomov = '9' ")
  List<String> findDistinctCuentaCompensacionByTmct0alcId(@Param(value = "alcId") Tmct0alcId alcId);

  @Query(value = "select CUENTA_COMPENSACION_DESTINO, sum(imtitulos) from bsnbpsql.TMCT0MOVIMIENTOECC where iddesglosecamara in "
      + "(select iddesglosecamara from bsnbpsql.tmct0desglosecamara where NUORDEN=:nuOrden and cdestadoentrec=:estadoEntrec "
      + "and cdestadoasig>=:estadoAsig) and cdestadomov=9 group by CUENTA_COMPENSACION_DESTINO", nativeQuery = true)
  List<Object[]> findCuentaCompensacionByNuOrden(@Param(value = "nuOrden") String nuOrden,
      @Param(value = "estadoEntrec") Integer estadoEntrec, @Param(value = "estadoAsig") Integer estadoAsig);

  @Query(value = "SELECT count(*) FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.iddesglosecamara = :idDesgloseCamara"
      + " AND m.cuentaCompensacionDestino = :cuentaCompensacionDestino")
  Integer countByIdDesgloseCamaraCuenta(@Param(value = "idDesgloseCamara") Long idDesgloseCamara,
      @Param(value = "cuentaCompensacionDestino") String cuentaCompensacionDestino);

  @Query(value = "SELECT sum(m.imtitulos) FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.iddesglosecamara = :idDesgloseCamara"
      + " AND m.cdestadomov = :cdestadomov ")
  BigDecimal getSumNutitulosByIddesglosecamaraAndCdestadomov(@Param(value = "idDesgloseCamara") Long idDesgloseCamara,
      @Param(value = "cdestadomov") String cdestadomov);

  @Query(value = "SELECT sum(m.imtitulos) FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.iddesglosecamara = :idDesgloseCamara"
      + " AND m.cdestadomov in ( :cdestadomov )")
  BigDecimal getSumNutitulosByIddesglosecamaraAndInCdestadomov(
      @Param(value = "idDesgloseCamara") Long idDesgloseCamara, @Param(value = "cdestadomov") List<String> cdestadomov);

  @Query(value = "SELECT sum(m.imtitulos) FROM Tmct0movimientoecc m WHERE m.tmct0desglosecamara.tmct0alc.id = :alcId"
      + " AND m.cdestadomov in ( :cdestadomov ) and m.tmct0desglosecamara.tmct0estadoByCdestadoasig.idestado > 0 ")
  BigDecimal getSumNutitulosByTmct0alcIdAndInCdestadomov(@Param(value = "alcId") Tmct0alcId alcId,
      @Param(value = "cdestadomov") List<String> cdestadomov);

}
