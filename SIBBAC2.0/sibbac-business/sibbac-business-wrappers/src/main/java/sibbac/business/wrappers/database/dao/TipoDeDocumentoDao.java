package sibbac.business.wrappers.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.TipoDeDocumento;


@Component
public interface TipoDeDocumentoDao extends JpaRepository< TipoDeDocumento, Long > {

	public TipoDeDocumento findByDescripcion( final String descripcion );
}
