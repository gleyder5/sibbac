package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.bo.Tmct0actBo;
import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.dto.LabelValueObject;
import sibbac.business.fase0.database.model.Tmct0act;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.wrappers.database.dao.Tmct0ReglaDao;
import sibbac.business.wrappers.database.dao.Tmct0ReglaDaoImpl;
import sibbac.business.wrappers.database.dto.GestionReglasDTO;
import sibbac.business.wrappers.database.model.Tmct0Regla;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;

/**
 * The Class Tmct0ReglaBo.
 */
@Service
public class Tmct0ReglaBo extends AbstractBo<Tmct0Regla, BigInteger, Tmct0ReglaDao> {

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ReglaBo.class);

  @Autowired
  private Tmct0ReglaDaoImpl tmct0ReglaDaoImpl;

  @Autowired
  private Tmct0ReglaDao tmct0ReglaDao;

  @Autowired
  private Tmct0aliBo aliasBo;
  @Autowired
  private Tmct0actBo subcuentasBo;

  public Tmct0Regla findIdRegla(BigInteger idRegla) {
    return tmct0ReglaDao.findIdRegla(idRegla);

  }

  public Tmct0Regla findIdReglaActivo(BigInteger idRegla) {
    return dao.findIdReglaActivo(idRegla);
  }

  public String updateActivoRegla(BigInteger idRegla, String activo) {
    Tmct0Regla toUpdate = this.findIdRegla(idRegla);
    java.util.Date fechaSistema = new Date();
    try {
      if (toUpdate != null) {
        if (activo.equals("1")) {
          toUpdate.setActivo("0");
        }
        else {
          toUpdate.setActivo("1");
        }
      }
      else {
        LOG.warn("[updateActivoRegla] No se ha localizado la regla a actualizar: {}", idRegla);
        return null;
      }
      toUpdate.setAuditFechaCambio(fechaSistema);
      this.dao.save(toUpdate);
    }
    catch (RuntimeException ex) {
      LOG.error("[updateActivoRegla] Error inesperado al actualizar", ex);
    }
    return toUpdate.getActivo();
  }

  public List<GestionReglasDTO> findReglasFiltro(String cdCodigo, Long camaraCompensacion, Long cuentaCompensacion,
      Long miembroCompensador, String referenciaAsignacion, Long nemotecnico, Long segmento, String referenciaCliente,
      String referenciaExterna, Long capacidad, Long modeloNegocio, String estado, String alias, String subCta) {

    return tmct0ReglaDaoImpl.findReglasFiltro(cdCodigo, camaraCompensacion, cuentaCompensacion, miembroCompensador,
        referenciaAsignacion, nemotecnico, segmento, referenciaCliente, referenciaExterna, capacidad, modeloNegocio,
        estado, alias, subCta);
  }

  public Tmct0Regla findByCdCodigo(String cdCodigo) {
    return this.dao.findByCdCodigo(cdCodigo);
  }

  public Tmct0Regla getRegla(Tmct0alo alo) {
    Tmct0Regla regla = null;
    Long idRegla = null;
    String cdalias = alo.getCdalias().trim();
    String cdsubcta = "";
    BigDecimal fecha = new BigDecimal("99991231");
    try {
      fecha = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(alo.getFeoperac()));
    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }

    if (cdalias.contains("|")) {
      String[] cdaliasSplit = cdalias.split("\\|");
      cdalias = cdaliasSplit[1];
      cdsubcta = cdaliasSplit[0];
      List<Tmct0act> act = subcuentasBo.findByCdaliassAndCdsubctaAndFecha(cdalias, cdsubcta, fecha.intValue());
      if (act != null && !act.isEmpty()) {
        idRegla = act.get(0).getIdRegla();
      }
      if (idRegla != null) {
        regla = dao.findIdReglaActivo(BigInteger.valueOf(idRegla));

      }
    }
    if (regla == null) {
      Tmct0ali ali = aliasBo.findByCdaliassFechaActual(cdalias, fecha.intValue());
      if (ali != null) {
        idRegla = ali.getIdRegla();
        if (idRegla != null) {
          regla = dao.findIdReglaActivo(BigInteger.valueOf(idRegla));
        }
      }
    }
    return regla;
  }

  public Tmct0Regla getReglaCached(Map<String, Tmct0Regla> cached, Tmct0alo alo) throws SIBBACBusinessException {
    String key = alo.getCdalias().trim();
    Tmct0Regla regla = cached.get(key);
    if (regla == null) {
      synchronized (cached) {
        regla = cached.get(key);
        if (regla == null) {
          regla = getRegla(alo);
          cached.put(key, regla);
        }
      }
    }
    return regla;
  }

  public List<LabelValueObject> labelValueList() {
    List<LabelValueObject> res = new ArrayList<LabelValueObject>();
    List<Tmct0Regla> listReglas = dao.findAllByActivo();
    res.add(new LabelValueObject("", ""));
    for (Tmct0Regla tmct0Regla : listReglas) {
      res.add(new LabelValueObject(String.format("%s-%s", tmct0Regla.getCdCodigo(),
          StringUtils.substring(tmct0Regla.getNbDescripcion(), 0, 20)), tmct0Regla.getIdRegla().toString()));
    }
    return res;
  }
}
