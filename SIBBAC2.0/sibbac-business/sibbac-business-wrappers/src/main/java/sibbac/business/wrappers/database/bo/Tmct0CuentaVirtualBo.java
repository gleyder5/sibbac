package sibbac.business.wrappers.database.bo;


import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.dao.Tmct0CuentaVirtualDao;
import sibbac.business.wrappers.database.model.Tmct0CuentaVirtual;
import sibbac.database.bo.AbstractBo;


@Service
public class Tmct0CuentaVirtualBo extends AbstractBo< Tmct0CuentaVirtual, Long, Tmct0CuentaVirtualDao > {

}
