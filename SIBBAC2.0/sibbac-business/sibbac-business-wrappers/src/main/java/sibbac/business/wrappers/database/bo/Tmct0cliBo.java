package sibbac.business.wrappers.database.bo;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.fase0.database.dto.Tmct0cliDTO;
import sibbac.business.fase0.database.model.Tmct0ali;
import sibbac.business.wrappers.database.dao.Tmct0cliDao;
import sibbac.business.wrappers.database.dao.Tmct0cliDaoImp;
import sibbac.business.wrappers.database.dto.ClienteDTO;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0cliBo extends AbstractBo< Tmct0cli, Long, Tmct0cliDao > {

	@Autowired
	private Tmct0cliDaoImp daoImp;
	/**
	 * 
	 * Recupera el cliente activo
	 * 
	 * @param cdbrocli
	 * 
	 * @return
	 */
	public Tmct0cli buscarClienteActivo( final String cdbrocli ) {
		return dao.findByCdbrocli( cdbrocli, Tmct0ali.fhfinActivos );
	}
	
	public Tmct0cli findByCdbrocliAndFecha(String cdbrocli, int fhfinal ){
	   return dao.findByCdbrocliAndFecha(cdbrocli, fhfinal);
	}
	
	public List<Tmct0cliDTO>  findDesglosesMacro(Date fDesde, Date fHasta, List<String> aliasVector, List<String> clientesVector){
		return daoImp.findDesglosesMacro(fDesde, fHasta, aliasVector, clientesVector);
	}
	
	public List<ClienteDTO> findClientesFiltro(Long idCliente, String tipoDocumento, String numDocumento, String tipoResidencia, String codPersonaBDP, String numeroFactura, 
			String tipoCliente, String naturalezaCliente, String estadoCliente, Date fechaAlta, Date fechaBaja, String codigoKGR, String tipoNacionalidad, String codigoPais,
			String categorizacion, String controlPBC, String motivoPBC, String riesgoPBC, Date fhRevRiesgoPbc,Long idClienteNot, String tipoDocumentoNot, String numDocumentoNot, String tipoResidenciaNot, String codPersonaBDPNot, String numeroFacturaNot, 
			String tipoClienteNot, String naturalezaClienteNot, String estadoClienteNot, String codigoKGRNot, String tipoNacionalidadNot, String codigoPaisNot,
			String categorizacionNot, String controlPBCNot, String motivoPBCNot,String riesgoPBCNot) {
		return daoImp.findClientesFiltro(idCliente, tipoDocumento, numDocumento, tipoResidencia, codPersonaBDP, numeroFactura, 
				tipoCliente, naturalezaCliente, estadoCliente, fechaAlta, fechaBaja, codigoKGR, tipoNacionalidad, codigoPais,
				categorizacion, controlPBC, motivoPBC, riesgoPBC, fhRevRiesgoPbc, idClienteNot, tipoDocumentoNot, numDocumentoNot, tipoResidenciaNot, codPersonaBDPNot, numeroFacturaNot, 
				tipoClienteNot, naturalezaClienteNot, estadoClienteNot, codigoKGRNot, tipoNacionalidadNot, codigoPaisNot,
				categorizacionNot, controlPBCNot, motivoPBCNot, riesgoPBCNot);
	}

	public Boolean isClienteExistente(Tmct0cli cliente) {
		if (cliente.getId() != null) {
			List<Tmct0cli> clientes = dao.findByCdbrocliAndNotId(cliente.getCdbrocli(), cliente.getId());
			return clientes != null && !clientes.isEmpty();
		} else {
			List<Tmct0cli> clientes = dao.findByCdbrocli(cliente.getCdbrocli());
			return clientes != null && !clientes.isEmpty();
		}
	}

}
