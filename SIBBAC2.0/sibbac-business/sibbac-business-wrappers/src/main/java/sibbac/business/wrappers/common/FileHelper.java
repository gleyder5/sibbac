package sibbac.business.wrappers.common;

import java.io.File;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

/**
 * Clase de utilidad para procesamiento de archivos.
 */
public class FileHelper {

  /** Referencia para la insercion de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FileHelper.class);

  /**
   * Mueve el fichero indicado en el 1er parametro a la carpeta indicada por el 2do parametro y le precede los
   * datos de fecha actual de la forma: nombre_fichero.ext -> nombre_fichero_yyyymmdd_hhmmss.ext
   * 
   * @param fichero fichero
   * @param folderDestino folderDestino
   * @param caracteresASuprimir caracteresASuprimir
   */
  public static void moveFile(File fichero, File folderDestino, String caracteresASuprimir) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("_yyyyMMdd");
    SimpleDateFormat hourFormat = new SimpleDateFormat("_hhmmss");
    try {
      Date hoy = new Date();
      String dateToConcat = dateFormat.format(hoy);
      String hourToConcat = hourFormat.format(hoy);

      String[] ficheroExt = new String[2];
      int posicion = fichero.getName().indexOf(".");
      if (posicion > -1) {
        ficheroExt[0] = fichero.getName().substring(0, posicion);
        ficheroExt[1] = fichero.getName().substring(posicion, fichero.getName().length());
      }

      LOG.debug("<<<<<" + folderDestino + ">>>>>>");
      LOG.debug(">>>>>>"
          + ficheroExt[0].concat(dateToConcat).concat(hourToConcat)
              .concat(suprimirContenidoNombreFichero((String) ficheroExt[1], caracteresASuprimir)) + "<<<<<<");

      if (StringUtils.isNotBlank(caracteresASuprimir)) {
        Files.move(
            fichero,
            new File(folderDestino, ficheroExt[0].concat(dateToConcat).concat(hourToConcat)
                .concat(suprimirContenidoNombreFichero((String) ficheroExt[1], caracteresASuprimir))));
      }
      else {
        Files.move(
            fichero,
            new File(folderDestino, ficheroExt[0].concat(dateToConcat).concat(hourToConcat)
                .concat((String) ficheroExt[1])));
      }

      LOG.debug("[SibbacFileReader :: moveFile] Fichero movido a carpeta " + folderDestino);
    }
    catch (Exception e) {
      LOG.error("[SibbacFileReader :: moveFile] Error moviendo fichero procesado (FILENAME: " + fichero.getName() + ")");
    }
  }

  /**
   * Obtiene lista de ficheros en un directorio.
   * @param ruta
   * @param extension: Es la extension, si se pone null no se tiene en cuenta
   * @return List<File>
   */
  public static List<File> getListaArchivosEnDirectorioByExtension(String ruta, String extension) {
    File rutaFile = new File(ruta);

    List<File> listaNombresFicheros = new ArrayList<File>();
    if (rutaFile != null && rutaFile.exists()) {
      for (File file : rutaFile.listFiles()) {
        if (file.isFile()) {
          String[] extensionArchivo = file.getName().split("\\.");
          if (extensionArchivo != null && extensionArchivo.length > 1) {
            if (StringUtils.isNotBlank(extension)) {
              if (extensionArchivo[extensionArchivo.length - 1].equalsIgnoreCase(extension)) {
                listaNombresFicheros.add(file);
              }
            }
            else {
              listaNombresFicheros.add(file);
            }
          }
        }
      }
    }
    else {
      LOG.error("ERROR, la ruta ''" + ruta + "'' NO EXISTE en el servidor");
    }
    
    return listaNombresFicheros;
  }

  /**
   * Obtiene lista de ficheros en un directorio por nombre.
   * @param ruta
   * @param extension: Es la extension, si se pone null no se tiene en cuenta
   * @return List<File>
   */
  public static List<File> getListaArchivosEnDirectorioByName(String ruta, String name) {
    File rutaFile = new File(ruta);
    List<File> listaNombresFicheros = new ArrayList<File>();
    if (rutaFile != null) {
      for (File file : rutaFile.listFiles()) {
        if (file.isFile()) {
          if (file.getName().equals(name)) {
            listaNombresFicheros.add(file);
          }
        }
      }
    }
    return listaNombresFicheros;
  }

  /**
   * Devuelve objeto Path para poder renombrar el archivo.
   * @param archivoArenombrar archivoArenombrar
   * @param nuevoNombreArchivo nuevoNombreArchivo
   * @return Path Path
   */
  public static Path renombrarArchivo(Path archivoArenombrar, String nuevoNombreArchivo) {
    return archivoArenombrar.getParent().resolve(nuevoNombreArchivo);
  }

  /**
   * Supresion de caracteres en nombre del fichero.
   * @param nombreArchivo nombreArchivo
   * @param caracteresASuprimir caracteresASuprimir
   * @return String String
   */
  public static String suprimirContenidoNombreFichero(String nombreArchivo, String caracteresASuprimir) {
    if (StringUtils.isNotBlank(nombreArchivo) && nombreArchivo.contains(caracteresASuprimir)) {
      return nombreArchivo.replace(caracteresASuprimir, "");
    }
    return nombreArchivo;
  }

  /**
   * Se usa para recortar el .procesando de ficheros Excel.
   * @param nombreFichero nombreFichero
   * @return String String
   */
  public static String getNombreFicheroFiltrado(String nombreFichero) {
    return nombreFichero.replace(".procesando", "");
  }

}
