package sibbac.business.wrappers.database.bo.fichero1015;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.DesgloseRecordBeanBo;
import sibbac.business.wrappers.database.bo.PartenonRecordBeanBo;
import sibbac.business.wrappers.database.dto.DesgloseRecordDTO;
import sibbac.business.wrappers.database.dto.DesgloseRecordEjecucionesDTO;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.tasks.thread.exception.ItemWriterMultithreadException;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;

@Service
public class EnvioFichero1015SubList {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(EnvioFichero1015SubList.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private DesgloseRecordBeanBo desgloseRecordBeanBo;

  @Autowired
  private PartenonRecordBeanBo partenonRecordBeanBo;

  @Autowired
  private SendMail mail;

  /** Ruta al fichero de configuración del proceso. */
  @Value("${MQ.partenon.desgloses.out.folder}")
  private String path;

  @Value("${MQ.partenon.desgloses.out.file}")
  private String file;

  @Value("${sibbac.wrappers.operacion.especial.1015.file.prefix}")
  private String filePrefix;

  @Value("${sibbac.wrappers.operacion.especial.1015.mail.to}")
  private String mailTo;

  @Value("${sibbac.wrappers.operacion.especial.1015.mail.cc}")
  private String mailCc;

  @Value("${sibbac.wrappers.operacion.especial.1015.mail.subject}")
  private String mailSubject;

  @Value("${sibbac.wrappers.operacion.especial.1015.mail.body}")
  private String mailBody;

  @Value("${sibbac.wrappers.operacion.especial.1015.clearing.platform}")
  private String clearingPlatform;

  @Value("${sibbac.wrappers.operacion.especial.1015.codigos.comerciales:23,24,25,26,28,30,91}")
  private String codigosComerciales;

  // private FlatFileItemWriter<String> writer;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * @param desgloses
   * @param pageNumber
   * @param codigosComerciales
   * @return
   * @throws ItemWriterMultithreadException
   * @throws SIBBACBusinessException
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void crearFicheroMQ(FlatFileItemWriter<String> writer, List<DesgloseRecordDTO> desgloses,
      List<String> codigosComerciales) throws SIBBACBusinessException {
    LOG.debug("[crearFicheroMQ] Inicio ...");

    Map<String, List<DesgloseRecordEjecucionesDTO>> ejecuciones = null;
    Map<String, String> ccvs = null;

    try {

      boolean marcarEnviado = true;
      boolean updateId = true;

      String ccv;
      // DesgloseRecordBean desglose;
      // DesgloseRecordDTO orden;
      List<String> listaLineas = null;
      Date fechaLote = null;
      int count = 0;
      List<String> nucnfclts;
      List<String> nurefords = new ArrayList<String>();

      Map<String, List<DesgloseRecordDTO>> mapDesglosesPorOrden = new HashMap<String, List<DesgloseRecordDTO>>();
      Map<String, List<String>> mapNucnfctlPorOrden = new HashMap<String, List<String>>();
      List<DesgloseRecordDTO> desglosesSubList;
      for (DesgloseRecordDTO orden : desgloses) {
        desglosesSubList = mapDesglosesPorOrden.get(orden.getNumeroOrden().trim());

        if (desglosesSubList == null) {
          desglosesSubList = new ArrayList<DesgloseRecordDTO>();
          mapDesglosesPorOrden.put(orden.getNumeroOrden().trim(), desglosesSubList);
        }
        desglosesSubList.add(orden);

        nucnfclts = mapNucnfctlPorOrden.get(orden.getNumeroOrden().trim());

        if (nucnfclts == null) {
          nucnfclts = new ArrayList<String>();
          mapNucnfctlPorOrden.put(orden.getNumeroOrden().trim(), nucnfclts);
        }
        nucnfclts.add(Tmct0alo.CTE_PARTENON + orden.getNumeroReferenciaOrden());

        nurefords.add(orden.getNumeroReferenciaOrden());
      }

      ccvs = partenonRecordBeanBo.findAllCcvByListNureford(nurefords);
      nurefords.clear();

      for (String numOrden : mapDesglosesPorOrden.keySet()) {
        nucnfclts = mapNucnfctlPorOrden.get(numOrden);
        desglosesSubList = mapDesglosesPorOrden.get(numOrden);

        LOG.debug("[crearFicheroMQ] Consultando ejecuciones para numOrden: '{}' y nucnfclts: '{}' ...", numOrden,
            nucnfclts);
        ejecuciones = desgloseRecordBeanBo.findAllDesglosesFor1015ByNuordenAndNucnfclts(numOrden, nucnfclts);
        nucnfclts.clear();

        for (DesgloseRecordDTO orden : desglosesSubList) {
          listaLineas = new ArrayList<String>();
          count++;
          if (count % 500 == 0) {
            LOG.info("[crearFicheroMQ] Enviando 10 15 orden envio {} de {} bean : {}", count, desglosesSubList.size(),
                orden);
          }

          try {
            marcarEnviado = true;
            updateId = false;
            LOG.trace("[crearFicheroMQ] Procesando : " + orden);

            if (orden.getOrdenComercial() != null) {
              // orden =
              // desgloseRecordBeanBo.findDesgloseRecordDTOById(ordenFor.getId());

              String codOrden = orden.getOrdenComercial().getOrdenComercial().toString();

              if (codigosComerciales.contains(codOrden)) {
                LOG.trace("[crearFicheroMQ] Buscando ejecuciones de '{}'",
                    Tmct0alo.CTE_PARTENON + orden.getNumeroReferenciaOrden());
                orden.setEjecuciones(ejecuciones.get(Tmct0alo.CTE_PARTENON + orden.getNumeroReferenciaOrden()));
                // orden.setEjecuciones(desgloseRecordBeanBo.findAllDesglosesFor1015ByNuordenAndNucnfclt(orden.getNumeroOrden(),
                // Tmct0alo.CTE_PARTENON
                // + orden.getNumeroReferenciaOrden()));
                if (CollectionUtils.isNotEmpty(orden.getEjecuciones())) {
                  if (StringUtils.isBlank(orden.getRefTitu()) || StringUtils.isBlank(orden.getRefAdic())) {
                    ccv = ccvs.get(orden.getNumeroReferenciaOrden());

                    orden.setRefTitu(ccv);
                    orden.setRefAdic(ccv);
                  }
                  if (fechaLote == null) {
                    Date fechaEjecucion = orden.getEjecuciones().get(0).getFeejecuc();
                    fechaLote = FormatDataUtils.convertStringToDate(
                        FormatDataUtils.convertDateToString(fechaEjecucion, FormatDataUtils.DATE_FORMAT) + "090000",
                        FormatDataUtils.TIMESTAMP_FORMAT);
                  }

                  orden.setHlote(fechaLote);
                  orden.setFhLote(fechaLote);
                  listaLineas = processLine10(orden);
                  if (CollectionUtils.isNotEmpty(listaLineas)) {
                    listaLineas.addAll(processLine15(orden, clearingPlatform));
                    updateId = true;
                  }
                  else {
                    LOG.warn(
                        "[EnvioFichero1015 :: crearFicheroMQ] La orden : {} con la propuesta : {} no ha generado registro 10.",
                        orden.getNumeroOrden(), orden.getNumeroReferenciaOrden());
                    marcarEnviado = false;
                    updateId = false;
                  } // else
                }
                else {
                  LOG.trace(
                      "[EnvioFichero1015 :: crearFicheroMQ] La orden : {} con la propuesta : {} no tiene desgloses.",
                      orden.getNumeroOrden(), orden.getNumeroReferenciaOrden());
                  marcarEnviado = false;
                  updateId = false;
                } // else
              } // if (codigos.contains(codOrden))
              else {
                LOG.trace("[EnvioFichero1015 :: crearFicheroMQ] La orden : {} no tiene un orden comercial valido : {}",
                    orden, codigosComerciales);
                marcarEnviado = false;
                updateId = true;
              }

              if (marcarEnviado) {
                writer.write(listaLineas);
              } // if (marcarEnviado)

            }
            else {
              LOG.trace("[EnvioFichero1015 :: crearFicheroMQ] La orden : {} no tiene un orden comercial valido : {}",
                  orden, codigosComerciales);
              updateId = true;
            } // else

            if (updateId) {
              desgloseRecordBeanBo.updateProcesadoById(orden.getId(),
                  EstadosEnumerados.ESTADO_DESGLOSES_OPERACIONES_ESPECIALES.ENVIADO_10_15_PDTE_CARGAR_TITULAR.getId());
            }
          }
          catch (Exception e) {
            throw new SIBBACBusinessException(e);
          }
        } // fin for

      }

    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e);
    }
    finally {
      if (ejecuciones != null) {
        ejecuciones.clear();
      }
      if (ccvs != null) {
        ccvs.clear();
      }

    }

    LOG.debug("[crearFicheroMQ] Fin ...");
  } // crearFicheroMQ

  public void deleteFile(Path filePath) throws SIBBACBusinessException {
    LOG.debug("[EnvioFichero1015SubList :: deleteFile] inicio.");
    try {
      if (filePath != null) {
        Files.deleteIfExists(filePath);
      }
    }
    catch (IOException e) {
      LOG.error("Error ", e.getClass().getName(), e.getMessage());
      throw new SIBBACBusinessException(e);
    }
    LOG.debug("[EnvioFichero1015SubList :: deleteFile] fin.");
  }

  /**
   * @throws IOException
   * 
   */
  public void closeAndDeleteIfEmpty(Path filePath, FlatFileItemWriter<String> writer) throws SIBBACBusinessException {
    LOG.debug("[EnvioFichero1015SubList :: closeAndDeleteIfEmpty] cerrando el writer.");
    if (writer != null) {
      writer.close();
    }
    LOG.debug("[EnvioFichero1015SubList :: closeAndDeleteIfEmpty] writer cerrado.");

  } // closeWithOutMove

  /**
   * 
   * @param pageNumber
   * @throws IOException
   */
  public synchronized void sendEmailAndMoveSendPath(Path filePath, String banco, long pageNumber)
      throws SIBBACBusinessException {
    LOG.debug("[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] inicio.");

    try {
      if (filePath != null && Files.exists(filePath) && Files.size(filePath) > 0) {
        Path finalFileName = getNewFilePath(banco, pageNumber);
        LOG.debug("[sendEmailAndMoveSendPath] nombre fichero origen: {}", filePath.toString());
        LOG.debug("[sendEmailAndMoveSendPath] nombre fichero destino: {}", finalFileName.toString());
        enviarEmail(getTmpPath(path).toString(), filePath, finalFileName.toFile().getName());

        LOG.debug("[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] moviendo el fichero de {} a {}",
            filePath.toString(), finalFileName.toString());
        int countReintentosMove = 0;
        long timeSleepEachMoveException = 1000;
        boolean reintentar = true;
        while (reintentar) {
          reintentar = false;
          try {
            filePath = Files.move(filePath, finalFileName, StandardCopyOption.ATOMIC_MOVE);
          }
          catch (Exception e) {
            LOG.warn(
                "[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] INCIDENCIA, no se ha podido renombrar el fichero al destino definitivo : {} : {}",
                filePath, finalFileName, e);

            if (countReintentosMove++ <= 10) {

              LOG.warn(
                  "[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] Vamos a esperar {}ms para reintentar el move del fichero. Reintento nº: {}",
                  timeSleepEachMoveException, countReintentosMove);
              reintentar = true;
              wait(timeSleepEachMoveException);
            }
            else {
              throw new SIBBACBusinessException("Incidencia renombrando el fichero de " + filePath + " a "
                  + finalFileName + " Nº de intentos: " + countReintentosMove, e);
            }

          }
        }
        LOG.debug("[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] fichero movido de {} a {}",
            filePath.toString(), finalFileName.toString());
        if (Files.exists(filePath)) {
          LOG.info("[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] Fichero envio MQ creado : {}",
              filePath.toString());
        }
        else {
          LOG.warn(
              "[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] No se ha podido crear el fichero envio MQ : {}",
              filePath.toString());
        }
      }
      else {
        LOG.warn("[sendEmailAndMoveSendPath] El fichero origen no existe. Se omite el envio ...");
      }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Incidencia con el fichero page number: " + pageNumber, e);
    }

    LOG.debug("[EnvioFichero1015SubList :: sendEmailAndMoveSendPath] fin.");
  } // closeAndMove

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * 
   * @param pageNumber
   * @throws IOException
   */
  public FlatFileItemWriter<String> initWriter(Path filePath) {
    LOG.debug("[initWriter] Inicio ...");

    FlatFileItemWriter<String> writer = new FlatFileItemWriter<String>();
    writer.setEncoding(StandardCharsets.ISO_8859_1.name());
    writer.setForceSync(true);
    writer.setAppendAllowed(false);
    writer.setShouldDeleteIfEmpty(true);
    writer.setShouldDeleteIfExists(false);
    writer.setTransactional(true);
    writer.setName(FormatDataUtils.convertDateToConcurrentFileName(new Date()) + "_" + this.hashCode());
    LineAggregator<String> lineAggregator = new PassThroughLineAggregator<String>();
    writer.setLineAggregator(lineAggregator);
    writer.setResource(new PathResource(filePath));
    writer.open(new ExecutionContext());
    LOG.debug("[initWriter] Fin ...");
    return writer;
  } // initWriter

  /**
   * 
   * @param pageNumber
   * @return
   * @throws IOException
   */
  public Path getNewTmpFilePath(String banco, long pageNumber) throws SIBBACBusinessException {
    LOG.debug("[getNewTmpFilePath] Inicio ...");

    String baseName = FilenameUtils.getBaseName(file);
    String concurrent = FormatDataUtils.convertDateToConcurrentFileName(new Date());
    String tmpFileName = String.format("%s_%s_%s.TMP_%d_%s_%d", filePrefix, banco, baseName, pageNumber, concurrent,
        this.hashCode());

    Path filePath = Paths.get(getTmpPath(path).toString(), tmpFileName);
    LOG.debug("[getNewTmpFilePath] Fin ...");
    return filePath;
  } // getNewTmpFilePath

  /**
   * 
   * @param pageNumber
   * @return
   */
  private Path getNewFilePath(String banco, long pageNumber) {
    LOG.debug("[getNewFilePath] Inicio ...");
    String concurrent = FormatDataUtils.convertDateToConcurrentFileName(new Date());
    String name = String.format("%s_%s_%s_%d_%s", filePrefix, banco, file, pageNumber, concurrent);
    Path filePath = Paths.get(path, name);
    LOG.debug("[getNewFilePath] Fin ...");
    return filePath;
  } // getNewFilePath

  /**
   * 
   * @param orden
   * @return
   */
  private List<String> processLine10(DesgloseRecordDTO orden) {
    LOG.trace("[processLine10] Inicio ...");
    List<String> listaResult = new LinkedList<String>();
    SimpleDateFormat sdfDate = new SimpleDateFormat("YYYYMMdd");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
    String tipoReg = "10";
    String numOrden = calcularLongitudOrden(orden.getNumeroReferenciaOrden());
    String fhLote = sdfDate.format(orden.getFhLote());
    String hLote = sdfTime.format(orden.getHlote());

    String nomineEjec = obtenerTitulosFormateados(15, BigDecimal.ZERO);
    Character tipCamb = orden.getTipoCambio().getTipoOperacion().charAt(0);

    String codVaiso = obtenerStringFormateado(12, orden.getCodigoValorISO());
    String filler2 = obtenerStringFormateado(2, "");
    String codOrgEj = obtenerStringFormateado(3, orden.getCodigoBolsaEjecucion());
    String obcSavB = obtenerStringFormateado(4, orden.getCodigoSV());
    String cdBroker = obtenerStringFormateado(4, "0000");
    String refTitu = obtenerStringFormateado(20, orden.getRefTitu());
    String refAdic = obtenerStringFormateado(20, orden.getRefAdic());
    BigDecimal titulosAcumulados = BigDecimal.ZERO;
    for (DesgloseRecordEjecucionesDTO eje : orden.getEjecuciones()) {
      titulosAcumulados = titulosAcumulados.add(eje.getNutiteje());
      String cboEjecW = StringHelper.formatBigDecimal(eje.getPrecio(), 6, 4);
      String fhEjec = sdfDate.format(eje.getFeejecuc());
      String hEjec = sdfTime.format(eje.getHoejecuc());

      String cdOrgNeg = obtenerStringFormateado(4, eje.getPlataformaNegociacion());
      String numEjeCu = StringUtils.trim(eje.getNuejecuc());

      String nEjecMer = StringUtils.leftPad(StringUtils.trim(eje.getNuexemkt()), 9, "0");
      String numTiteJ = StringHelper.formatBigDecimal(eje.getNutiteje(), 11, 0);
      StringBuilder linea = new StringBuilder(tipoReg).append(numOrden).append(fhLote).append(hLote).append(fhEjec)
          .append(hEjec).append(numTiteJ).append(nomineEjec).append(tipCamb).append(cboEjecW).append(codVaiso)
          .append(filler2).append(codOrgEj).append(obcSavB).append(cdOrgNeg).append(cdBroker).append(numEjeCu)
          .append("    ").append(nEjecMer).append("       ").append(refTitu).append(refAdic);
      listaResult.add(linea.toString());
    }
    // LOG.debug("[processLine10] Creacion terminada ... Comprobando titulos ...");
    // LOG.debug("[processLine10] orden.getTitulosEjecutados(): " +
    // orden.getTitulosEjecutados());
    // LOG.debug("[processLine10] titulosAcumulados: " + titulosAcumulados);
    if (orden.getTitulosEjecutados().compareTo(titulosAcumulados) != 0) {
      LOG.warn(
          "[EnvioFichero1015 :: processLine10] La orden : {} con la propuesta : {} tiene desgloses pero los titulos no coinciden con el 40 id : {}.",
          orden.getNumeroOrden(), orden.getNumeroReferenciaOrden(), orden.getId());
      listaResult.clear();
    }

    LOG.trace("[processLine10] Fin ...");
    return listaResult;
  } // processLine10

  /**
   * 
   * @param orden
   * @param cdOrEcc
   * @return
   */
  private List<String> processLine15(DesgloseRecordDTO orden, String cdOrEcc) {
    LOG.trace("[processLine15] Inicio ...");
    SimpleDateFormat sdfDate = new SimpleDateFormat("YYYYMMdd");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
    DesgloseRecordEjecucionesDTO eje = orden.getEjecuciones().get(0);
    List<String> listaResult = new LinkedList<String>();
    String tipoReg = "15";
    String numOrden = calcularLongitudOrden(orden.getNumeroReferenciaOrden());
    String fhLote = sdfDate.format(orden.getFhLote());
    String hLote = sdfTime.format(orden.getHlote());
    String fLiqui = sdfDate.format(eje.getFeliquidacion());
    String broker = obtenerStringFormateado(4, "0000");

    String nunTitEj = StringHelper.formatBigDecimal(orden.getTitulosEjecutados(), 11, 0);
    String nomiJec = StringHelper.formatBigDecimal(BigDecimal.ZERO, 15, 0);

    BigDecimal corretaje = BigDecimal.ZERO;
    BigDecimal canonContr = BigDecimal.ZERO;
    BigDecimal canonLiq = BigDecimal.ZERO;
    BigDecimal canonComp = BigDecimal.ZERO;

    BigDecimal efectivoEnvio = BigDecimal.ZERO;
    BigDecimal efectivoPrecioMedio = BigDecimal.ZERO;
    BigDecimal efectivoAcumulado = BigDecimal.ZERO;
    BigDecimal precioMedio = null;
    Map<BigDecimal, BigDecimal> titulosPrecio = new HashMap<BigDecimal, BigDecimal>();
    for (DesgloseRecordEjecucionesDTO ejef : orden.getEjecuciones()) {

      corretaje = corretaje.add(ejef.getImcomisn());
      canonContr = canonContr.add(ejef.getCanonContr());
      canonLiq = canonLiq.add(ejef.getCanonLiq());
      canonComp = canonComp.add(ejef.getCanonComp());
      // efectivo = efectivo.add(ejef.getEfectivo());

      efectivoAcumulado = efectivoAcumulado.add(ejef.getEfectivo());

      if (titulosPrecio.get(ejef.getPrecio()) == null) {
        titulosPrecio.put(ejef.getPrecio(), BigDecimal.ZERO);
      }
      titulosPrecio.put(ejef.getPrecio(), titulosPrecio.get(ejef.getPrecio()).add(ejef.getNutiteje()));
    }

    for (BigDecimal precioEjecucion : titulosPrecio.keySet()) {
      efectivoEnvio = efectivoEnvio.add(precioEjecucion.multiply(titulosPrecio.get(precioEjecucion)).setScale(2,
          RoundingMode.HALF_UP));
    }

    // mas de un precio
    if (titulosPrecio.keySet().size() > 1) {
      precioMedio = efectivoEnvio.divide(orden.getTitulosEjecutados(), 5, RoundingMode.HALF_UP);
    }
    else {
      precioMedio = eje.getPrecio();
    }

    efectivoPrecioMedio = orden.getTitulosEjecutados().multiply(precioMedio).setScale(2, RoundingMode.HALF_UP);

    if (efectivoAcumulado.compareTo(efectivoEnvio) != 0) {
      LOG.warn("[EnvioFichero1015 :: processLine15] el efectivo acumulado es {} distinto al enviado {}",
          efectivoAcumulado, efectivoEnvio);
    }

    if (efectivoPrecioMedio.compareTo(efectivoEnvio) != 0) {
      LOG.warn("[EnvioFichero1015 :: processLine15] los titulos totales x precio medio es {} distinto al enviado {}",
          efectivoPrecioMedio, efectivoEnvio);
    }

    titulosPrecio.clear();

    orden.setCambioOperacion(efectivoEnvio.divide(orden.getTitulosEjecutados(), 5, RoundingMode.HALF_UP));
    String cboMedio = StringHelper.formatBigDecimal(precioMedio, 7, 5);

    String efecTej = StringHelper.formatBigDecimal(efectivoEnvio, 13, 2);

    String divisa = obtenerStringFormateado(3, orden.getDivisa());

    if (StringUtils.isNotBlank(eje.getDivisa())) {
      divisa = eje.getDivisa();
    }
    else if (StringUtils.isBlank(divisa)) {
      divisa = "EUR";
    }
    String cobTobol = StringHelper.formatBigDecimalWithSign(BigDecimal.ZERO, 11, 0);
    String comContr = StringHelper.formatBigDecimalWithSign(BigDecimal.ZERO, 11, 0);

    String corretajes = StringHelper.formatBigDecimalWithSign(corretaje, 9, 2);
    String canCont = StringHelper.formatBigDecimalWithSign(canonContr, 9, 2);
    Character abCanCom = eje.getClientPayCanonComp() == 1 ? 'S' : 'N';
    String canComp = StringHelper.formatBigDecimalWithSign(canonComp, 9, 2);
    String canLiq = StringHelper.formatBigDecimalWithSign(canonLiq, 11, 0);
    String filler = obtenerStringFormateado(63, "");
    cdOrEcc = obtenerStringFormateado(11, cdOrEcc);
    // for (int i = 0; i < orden.getListaCDOrgNeg().size(); i++) {
    String fhEjec = sdfDate.format(eje.getFeejecuc());
    String cdOrgNeg = obtenerStringFormateado(4, eje.getPlataformaNegociacion());
    StringBuilder linea = new StringBuilder(tipoReg).append(numOrden).append(fhLote).append(hLote).append(fhEjec)
        .append(fLiqui).append(broker).append(nunTitEj).append(nomiJec).append(efecTej).append(cboMedio).append(divisa)
        .append(cdOrgNeg).append(cdOrEcc).append(cobTobol).append(comContr).append(corretajes).append(canCont)
        .append(abCanCom).append(canComp).append(canLiq).append(filler);
    listaResult.add(linea.toString());
    // }// for (int i=0; i<orden.getListaCDOrgNeg().size(); i++){

    LOG.trace("[processLine15] Fin ...");
    return listaResult;
  } // processLine15]

  /**
   * 
   * @param numCharacters
   * @param titulosEjecutados
   * @return
   */
  private String obtenerTitulosFormateados(int numCharacters, BigDecimal titulosEjecutados) {
    StringBuilder result = new StringBuilder();
    String titulosString = titulosEjecutados.toString();
    while (result.length() + titulosString.length() < numCharacters) {
      result.append("0");
    }
    if (titulosString.length() > numCharacters) {
      titulosString = titulosString.substring(titulosString.length() - numCharacters, titulosString.length());
    }
    result.append(titulosString);
    return result.toString();
  } // obtenerTitulosFormateados

  /**
   * 
   * @param longitud
   * @param string
   * @return
   */
  private String obtenerStringFormateado(int longitud, String string) {
    if (string == null) {
      string = "";
    }
    StringBuilder result = new StringBuilder();
    while (result.length() + string.length() < longitud) {
      result.append(" ");
    }// while (result.length() + string.length() < longitud) {
    if (string.length() > longitud) {
      string = string.substring(0, longitud);
    }
    result.append(string);
    return result.toString();
  } // obtenerStringFormateado

  // /**
  // *
  // * @param longitudEnteros
  // * @param longitudDecimales
  // * @param titulosEjecutados
  // * @return
  // */
  // private String obtenerCambioFormateados(int longitudEnteros, int
  // longitudDecimales, BigDecimal titulosEjecutados) {
  // StringBuilder result = new StringBuilder();
  // int longitudParteDecimal = titulosEjecutados.scale();
  // int precision = titulosEjecutados.precision();
  // int longitudParteEntera = precision - longitudParteDecimal;
  // StringBuilder resultParteEntera = new StringBuilder("0");
  // StringBuilder resultParteDecimal = new StringBuilder("");
  // if (longitudParteEntera >= 0) {
  // String parteEnteraString = titulosEjecutados.toBigInteger().toString();
  // String parteDecimalString = "";
  // if (longitudParteDecimal != 0) {
  // parteDecimalString =
  // titulosEjecutados.toString().substring(longitudParteEntera + 1, precision +
  // 1);
  // }
  // resultParteEntera = new StringBuilder(parteEnteraString);
  // resultParteDecimal = new StringBuilder(parteDecimalString);
  // }
  // resultParteEntera = new
  // StringBuilder(this.obtenerTitulosFormateados(longitudEnteros,
  // new BigDecimal(resultParteEntera.toString())));
  // while (resultParteDecimal.length() < longitudDecimales) {
  // resultParteDecimal.append("0");
  // }
  // if (resultParteDecimal.length() > longitudDecimales) {
  // resultParteDecimal = new StringBuilder(resultParteDecimal.substring(0,
  // longitudDecimales));
  // }
  // result.append(resultParteEntera).append(resultParteDecimal);
  // return result.toString();
  // } // obtenerCambioFormateados

  /**
   * 
   * @param numeroOrden
   * @return
   */
  private String calcularLongitudOrden(String numeroOrden) {
    StringBuilder result = new StringBuilder(numeroOrden.trim());
    if (result.length() < 10) {
      while (result.length() < 10) {
        result.append(" ");
      }
    }
    else {
      result = new StringBuilder(result.substring(result.length() - 10, result.length()));
    }
    return result.toString();
  } // calcularLongitudOrden

  private Path getTmpPath(String rootDir) throws SIBBACBusinessException {
    Path tmp = Paths.get(rootDir, "tmp");
    return tmp;
  }

  /**
   * 
   * @param tmpPath
   * @param file
   * @param myFinalFileName
   */
  private void enviarEmail(String tmpPath, Path file, String myFinalFileName) {
    LOG.info("[enviarEmail] ## inicio");
    Path fileToMail = null;

    try {
      if (Files.exists(file) && Files.size(file) > 0) {
        List<String> to = new ArrayList<String>();
        if (StringUtils.isNotBlank(mailTo)) {
          LOG.info("[enviarEmail] ## Enviando correo a TO: {}", mailTo);
          to.addAll(Arrays.asList(mailTo.split(",")));
        }
        List<String> cc = new ArrayList<String>();
        if (StringUtils.isNotBlank(mailCc)) {
          LOG.info("[enviarEmail] ## Enviando correo a CC: {}", mailCc);
          cc.addAll(Arrays.asList(mailCc.split(",")));
        }

        // if (CollectionUtils.isEmpty(to) && CollectionUtils.isNotEmpty(cc)) {
        // to.addAll(cc);
        // cc.clear();
        // }

        if (CollectionUtils.isNotEmpty(to) || CollectionUtils.isNotEmpty(cc)) {

          // si el fichero es mayor de 24Mb lo comprimimos en gz
          boolean gzip = Files.size(file) >= (24 * 1024 * 1024);
          // String envioMailFileName = myFinalFileName.substring(0,
          // myFinalFileName.indexOf(".")) + "_EMAIL_"
          // + myFinalFileName.substring(myFinalFileName.indexOf("."));
          fileToMail = Paths.get(tmpPath, "MAIL_" + myFinalFileName + (gzip ? ".gz" : ""));

          ReadableByteChannel channelIn = Files.newByteChannel(file, StandardOpenOption.READ);

          InputStream is = Channels.newInputStream(channelIn);

          WritableByteChannel channelOut = Files.newByteChannel(fileToMail, StandardOpenOption.CREATE,
              StandardOpenOption.WRITE);

          OutputStream out = null;
          if (gzip) {
            LOG.info("[enviarEmail] ## El fichero debe ir comprimido.");
            out = new GZIPOutputStream(Channels.newOutputStream(channelOut));
          }
          else {
            out = Channels.newOutputStream(channelOut);
          }
          IOUtils.copy(is, out);
          is.close();
          channelIn.close();
          out.close();
          channelOut.close();
          try {
            LOG.info("[enviarEmail] ## Enviando correo con fichero: {}", fileToMail.toString());
            List<InputStream> path2attach = new ArrayList<InputStream>();
            List<String> nameDest = new ArrayList<String>();
            is = Files.newInputStream(fileToMail);
            path2attach.add(is);
            nameDest.add(fileToMail.getFileName().toString());

            mail.sendMail(to, mailSubject, mailBody, path2attach, nameDest, cc);
          }
          catch (Exception e) {
            throw e;
          }
          finally {
            if (is != null) {
              is.close();
            }
          }
        }
        else {
          LOG.info("[enviarEmail] ## Ningun destinatario configurado para enviar el fichero por correo. {}",
              file.toString());
        }
      }
    }
    catch (Exception e) {
      LOG.warn("[enviarEmail] incidencia ", e.getMessage());
      LOG.debug("[enviarEmail] incidencia ", e);
    }
    finally {
      if (fileToMail != null) {
        try {
          Files.deleteIfExists(fileToMail);
        }
        catch (IOException e) {
          LOG.warn("[enviarEmail] incidencia ", e.getMessage());
          LOG.debug("[enviarEmail] incidencia ", e);
        }
      }
    }

    LOG.info("[enviarEmail] ## fin");
  } // enviarEmail

  // /**
  // *
  // * @param pathToSave
  // * @param fileName
  // * @param result
  // * @param lines
  // * @return
  // */
  // private boolean writeLinesInFile(String pathToSave, String fileName,
  // List<String> lines) {
  // boolean result = true;
  // String fileNameTmp = fileName.substring(0, fileName.indexOf(".")) + ".TMP";
  // File toWrite = new File(pathToSave + File.separator + fileNameTmp);
  // FileWriter writer = null;
  // BufferedWriter buff = null;
  // try {
  //
  // int count = 0;
  // while (toWrite.exists() && count++ < 50) {
  // try {
  // Thread.sleep(50);
  // } catch (InterruptedException e) {
  //
  // }
  // toWrite = new File(pathToSave + File.separator + fileNameTmp + "_"
  // + FormatDataUtils.convertDateToConcurrentFileName(new Date()));
  // }
  // writer = new FileWriter(toWrite.getAbsoluteFile());
  //
  // buff = new BufferedWriter(writer);
  // for (String line : lines) {
  // buff.write(line + "\n");
  // }
  // } catch (IOException e) {
  // e.printStackTrace();
  // result = false;
  // } finally {
  // if (writer != null) {
  // try {
  // buff.close();
  // writer.close();
  // } catch (IOException e) {
  // e.printStackTrace();
  // }
  // }
  // }
  // if (toWrite.exists() && toWrite.length() > 0) {
  // File finalFile = new File(pathToSave + File.separator + fileName);
  // int count = 0;
  // while (finalFile.exists() && count++ < 50) {
  // try {
  // Thread.sleep(50);
  // } catch (InterruptedException e) {
  //
  // }
  // finalFile = new File(pathToSave + File.separator + fileName + "_"
  // + FormatDataUtils.convertDateToConcurrentFileName(new Date()));
  // }
  //
  // enviarEmail(pathToSave, Paths.get(toWrite.getAbsolutePath()),
  // finalFile.getName());
  //
  // toWrite.renameTo(finalFile);
  // } else if (toWrite.length() == 0) {
  // toWrite.delete();
  // }
  // return result;
  // }

} // EnvioFichero1015SubList
