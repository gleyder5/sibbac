package sibbac.business.wrappers.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import sibbac.business.wrappers.database.data.TipoCuentaLiquidacionData;
import sibbac.business.wrappers.database.model.TipoCuentaLiquidacion;


public interface TipoCuentaLiquidacionDao extends JpaRepository< TipoCuentaLiquidacion, Long > {

	@Query( "SELECT new sibbac.business.wrappers.database.data.TipoCuentaLiquidacionData(tp) FROM TipoCuentaLiquidacion tp" )
	List< TipoCuentaLiquidacionData > findAllData();
}
