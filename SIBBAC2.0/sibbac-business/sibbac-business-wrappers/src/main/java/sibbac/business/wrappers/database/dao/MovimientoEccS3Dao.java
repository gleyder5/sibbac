package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.MovimientoEccS3;

@Repository
public interface MovimientoEccS3Dao extends JpaRepository<MovimientoEccS3, Long> {

  List<MovimientoEccS3> findAllByCdrefmovimiento(String cdrefmovimiento);

  MovimientoEccS3 findByCdrefmovimientoAndNuoperacionprev(String cdrefmovimiento, String nuoperacionprev);

}
