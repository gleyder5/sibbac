package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.dao.Tmct0ordDao;
import sibbac.business.wrappers.database.data.Tmct0AlcData;
import sibbac.business.wrappers.database.data.Tmct0AloData;
import sibbac.business.wrappers.database.dto.OrdTitularesDTO;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.business.wrappers.database.model.Tmct0bok;
import sibbac.business.wrappers.database.model.Tmct0eje;
import sibbac.business.wrappers.database.model.Tmct0ord;
import sibbac.common.TiposSibbac.TipoRouting;
import sibbac.database.bo.AbstractBo;

@Service
public class Tmct0ordBo extends AbstractBo<Tmct0ord, String, Tmct0ordDao> {

  @Autowired
  Tmct0ejeBo ejeBo;

  public BigDecimal getNewNuoproutSequence() {
    return dao.getNewNuoproutSequence();
  }

  public Tmct0ord findByNuorden(String nuorden) {
    return dao.findByNuorden(nuorden);
  }

  public Tmct0ord findByNureford(String nureford) {
    return dao.findByNureford(nureford);
  }

  public List<Tmct0ord> findAllByNureford(String nureford) {
    return dao.findAllByNureford(nureford);
  }

  public Tmct0ord findFirstByNurefordOrderByNuorden(String nureford) {
    return dao.findFirstByNurefordOrderByNuorden(nureford);
  }

  public List<Tmct0eje> findEjesByNureford(String nureford) {
    Tmct0ord ord = dao.findFirstByNurefordOrderByNuorden(nureford);
    List<Tmct0eje> ejes = new ArrayList<Tmct0eje>();
    List<Tmct0bok> boks = ord.getTmct0boks();
    if (boks != null) {
      for (Tmct0bok bok : boks) {
        // ejes.addAll( bok.getTmct0ejes() );
        ejes.addAll(ejeBo.findAllByNbookingAndNuorden(bok.getId().getNbooking(), bok.getId().getNuorden()));
      }
    }

    return ejes;
  }

  public List<Tmct0eje> findEjesByBooking(String nuorden, String nbooking) {
    // Tmct0ord ord = dao.findFirstByNurefordOrderByNuorden( nureford );
    List<Tmct0eje> ejes = new ArrayList<Tmct0eje>();
    // List< Tmct0bok > boks = ord.getTmct0boks();
    // if ( boks != null ) {
    // for ( Tmct0bok bok : boks ) {
    // ejes.addAll( bok.getTmct0ejes() );
    // ejes.addAll( ejeBo.findAllByNbookingAndNuorden(
    // bok.getId().getNbooking(), bok.getId().getNuorden() ) );
    ejes.addAll(ejeBo.findAllByNbookingAndNuorden(nbooking, nuorden));
    // }
    // }

    return ejes;
  }

  public List<Tmct0alo> getAlosFromNumorden(String numorden) throws IncorrectResultSizeDataAccessException {
    Tmct0ord ord = this.findFirstByNurefordOrderByNuorden(numorden);
    List<Tmct0alo> alos = new ArrayList<Tmct0alo>();
    if (ord == null) {
      throw new IncorrectResultSizeDataAccessException(1, 0);
    }
    else {
      for (Tmct0bok bok : ord.getTmct0boks()) {
        alos.addAll(bok.getTmct0alos());
      }
    }
    return alos;
  }

  public List<Tmct0AloData> getAlosDataFromNumorden(String numorden) throws IncorrectResultSizeDataAccessException {
    Tmct0ord ord = this.findFirstByNurefordOrderByNuorden(numorden);
    List<Tmct0AloData> alos = new ArrayList<Tmct0AloData>();
    if (ord == null) {
      throw new IncorrectResultSizeDataAccessException(1, 0);
    }
    else {
      for (Tmct0bok bok : ord.getTmct0boks()) {
        alos.addAll(Tmct0AloData.entityListToDataList(bok.getTmct0alos()));
      }
    }
    return alos;
  }

  public List<Tmct0alc> getAlcsFromNumorden(String numorden) throws IncorrectResultSizeDataAccessException {
    Tmct0ord ord = this.findFirstByNurefordOrderByNuorden(numorden);
    if (ord == null) {
      throw new IncorrectResultSizeDataAccessException(1, 0);
    }
    return ord.getTmct0alcs();
  }

  /**
   * Devuelve una lista de objetos Tmct0AlcData parseados de Tmct0alc
   * 
   * @param numorden
   * @return List< Tmct0AlcData >
   * @throws IncorrectResultSizeDataAccessException
   */
  public List<Tmct0AlcData> getAlcsDataFromNumorden(String numorden) throws IncorrectResultSizeDataAccessException {
    Tmct0ord ord = this.findFirstByNurefordOrderByNuorden(numorden);
    if (ord == null) {
      throw new IncorrectResultSizeDataAccessException(1, 0);
    }
    List<Tmct0AlcData> resultList = Tmct0AlcData.entityListToDataList(ord.getTmct0alcs());
    return resultList;
  }

  public OrdTitularesDTO findFirstByNurefordOrderByNuordenNative(String nureford) {
    OrdTitularesDTO ord = null;
    List<Object[]> resultadosQuery = this.dao.findFirstByNurefordOrderByNuordenNative(nureford);
    if (resultadosQuery.size() > 0) {
      ord = new OrdTitularesDTO((String) resultadosQuery.get(0)[0], (Character) resultadosQuery.get(0)[1]);
    }
    return ord;
  }

  public OrdTitularesDTO findByNuordenNative(String nuorden) {
    OrdTitularesDTO ord = null;
    List<Object[]> resQuery = dao.findNuordenCdclsmdoIsscrdivByNuordenNative(nuorden);
    if (CollectionUtils.isNotEmpty(resQuery)) {
      ord = new OrdTitularesDTO((String) resQuery.get(0)[0], (Character) resQuery.get(0)[1],
          (Character) resQuery.get(0)[2]);
    }
    return ord;
  }

  public String findCdcleareById(String nuorden) {
    return dao.findCdcleareById(nuorden);
  }

  public boolean isRouting(String cdorigen, String cdclente) {
    return cdorigen != null && cdorigen.trim().toUpperCase().equals(TipoRouting.CDORIGEN) || cdclente != null
        && cdclente.trim().toUpperCase().equals(TipoRouting.CDCLENTE);
  }

  public boolean isRouting(Tmct0ord ord) {
    return ord != null && isRouting(ord.getCdorigen(), ord.getCdclente());
  }

  public boolean isScriptDividend(Tmct0ord ord) {
    return ord != null && ord.getIsscrdiv() == 'S';
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void pasarScriptDividendNewTransaction(String nuorden, boolean sd) {
    Tmct0ord ord = findById(nuorden);
    if (ord != null) {
      if (sd && !this.isScriptDividend(ord)) {
        ord.setIsscrdiv('S');
        ord.setFhaudit(new Date());
        save(ord);
      }
      else if (!sd && this.isScriptDividend(ord)) {
        ord.setIsscrdiv('N');
        ord.setFhaudit(new Date());
        save(ord);
      }
    }
  }

}
