package sibbac.business.wrappers.database.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.dao.FacturaSugeridaDao;
import sibbac.business.wrappers.database.dao.Tmct0alcDaoImp;
import sibbac.business.wrappers.database.dao.specifications.FacturaSugeridaSpecificacions;
import sibbac.business.wrappers.database.data.AliasFacturableData;
import sibbac.business.wrappers.database.dto.FacturaSugeridaDto;
import sibbac.business.wrappers.database.model.AlcOrdenes;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.FacturaDetalle;
import sibbac.business.wrappers.database.model.FacturaSugerida;
import sibbac.business.wrappers.database.model.Monedas;
import sibbac.business.wrappers.database.model.Periodos;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0cli;
import sibbac.database.bo.AbstractBo;

/**
 * @author Raquel
 *
 */
@Service
public class FacturaSugeridaBo extends AbstractBo<FacturaSugerida, Long, FacturaSugeridaDao> {

  protected static final Logger LOG = LoggerFactory.getLogger(FacturaSugeridaBo.class);

  @Autowired
  private Tmct0estadoBo estadoBo;

  @Autowired
  private AliasBo aliasBo;

  @Autowired
  private AliasSubcuentaBo aliasSubcuentaBo;

  @Autowired
  private Tmct0alcBo tmct0alcBo;

  @Autowired
  private PeriodosBo periodoBo;

  @Autowired
  private MonedasBo monedaBo;

  @Autowired
  private AlcOrdenesBo alcOrdenesBo;

  @Autowired
  private Tmct0cliBo tmct0cliBo;
  @Autowired
  private Tmct0alcDaoImp tmct0alcDaoImp;

  /**
   * 
   * Recupera facturas sugeridas para el estado que se pasa por parametro
   * 
   * @param estado
   * @return
   */
  public List<FacturaSugerida> findByEstado(final EstadosEnumerados.FACTURA_SUGERIDA estadoEnum) {
    final Tmct0estado estado = new Tmct0estado(estadoEnum.getId());
    return this.findByEstado(estado);
  }

  /**
   * Recupera las lis de facturas para el estado que se pasa por parametro
   * 
   * @param estado
   * @return
   */
  private List<FacturaSugerida> findByEstado(final Tmct0estado estado) {
    return dao.findByEstado(estado);
  }

  /**
   * Extrae los cdAlias de la lista de alias
   * 
   * @param aliasProcesar
   * @return
   */
  private Set<String> getCdaliasAProcesar(final List<Alias> aliasProcesar) {
    final Set<String> cdaliasProcesar = new HashSet<String>();
    for (final Alias alias : aliasProcesar) {
      cdaliasProcesar.add(alias.getCdaliass());
    }
    return cdaliasProcesar;
  }

  /**
   * @param aliasAProcesar
   * @return
   */
  private Map<Alias, Set<String>> recuperarAliasFacturables(final List<Alias> aliasAProcesar) {
    final Set<String> cdaliasAProcesar = getCdaliasAProcesar(aliasAProcesar);
    final List<AliasFacturableData> aliasFacturablesList = new ArrayList<AliasFacturableData>();
    aliasFacturablesList.addAll(aliasSubcuentaBo.recuperarAliasFactruablesSubcuentas(cdaliasAProcesar));
    aliasFacturablesList.addAll(aliasBo.recuperarAliasFactruablesAlias(cdaliasAProcesar));
    final Map<Alias, Set<String>> aliasFacturables = new HashMap<Alias, Set<String>>();
    if (CollectionUtils.isNotEmpty(aliasFacturablesList)) {
      for (AliasFacturableData aliasFacturableData : aliasFacturablesList) {
        final Alias alias = aliasFacturableData.getAlias();
        Set<String> cdalias = aliasFacturables.get(alias);
        if (CollectionUtils.isEmpty(cdalias)) {
          cdalias = new HashSet<String>();
          aliasFacturables.put(alias, cdalias);
        }
        cdalias.add(aliasFacturableData.getCdAlias());
      }

    }
    return aliasFacturables;
  }

  // /**
  // *
  // * @param aliasAProcesar
  // * @param fechaHasta
  // * @return
  // */
  // @Transactional
  // public List<FacturaSugerida> generarFacturasSugeridas(final List<Alias> aliasAProcesar) {
  // return generarFacturasSugeridas(aliasAProcesar, null);
  // }

  /**
   * Genera la lista de facturas sugerdias en base a las ordenes
   * 
   * @return
   */
  @Transactional
  public List<FacturaSugerida> generarFacturasSugeridas(final List<Alias> aliasAProcesar, Date fechaDesde, Date fechaHasta) {
    final String prefix = "[FacturaSugeridaBo::generarFacturasSugeridas] ";
    final List<FacturaSugerida> facturasSugeridas = new ArrayList<FacturaSugerida>();
    LOG.trace(prefix + "[Recuperando alias y subcuentas facturables...]");
    final Map<Alias, Set<String>> aliasFacturables = recuperarAliasFacturables(aliasAProcesar);
    if (MapUtils.isNotEmpty(aliasFacturables)) {
      LOG.trace(prefix + "[Recuperando [{}] alias facturables]", aliasFacturables.size());
      final Tmct0estado estadoSugerida = estadoBo.findByIdFacturaSugerida(EstadosEnumerados.FACTURA_SUGERIDA.SUGERIDA);
      final Monedas moneda = monedaBo.findByCodigo(Monedas.COD_MONEDA_EUR);
      final Periodos periodoActivo = periodoBo.getPeriodosActivo();
      final Tmct0estado estadoAlcSugerido = estadoBo.findByIdAlcOrden(EstadosEnumerados.ALC_ORDENES.SUGERIDO);
      final Tmct0estado estadoAlcLibre = estadoBo.findByIdAlcOrden(EstadosEnumerados.ALC_ORDENES.LIBRE);
      final Set<Alias> aliasSet = aliasFacturables.keySet();
      for (final Alias alias : aliasSet) {
        final FacturaSugerida facturaSugerida = generarFacturaSugerida(prefix, estadoSugerida, moneda, periodoActivo, estadoAlcSugerido,
                                                                       estadoAlcLibre, alias, aliasFacturables.get(alias), fechaDesde, fechaHasta);
        if (facturaSugerida != null) {
          facturasSugeridas.add(facturaSugerida);
        }
      }
    } else {
      LOG.debug(prefix + "No hay alias facturables");
    }

    return facturasSugeridas;
  }

  /**
   * @param prefix
   * @param facturasSugeridas
   * @param aliasFacturables
   * @param estadoSugerida
   * @param moneda
   * @param periodoActivo
   * @param estadoAlcSugerido
   * @param estadoAlcLibre
   * @param alias
   */
  private FacturaSugerida generarFacturaSugerida(final String prefix,
                                                 final Tmct0estado estadoSugerida,
                                                 final Monedas moneda,
                                                 final Periodos periodoActivo,
                                                 final Tmct0estado estadoAlcSugerido,
                                                 final Tmct0estado estadoAlcLibre,
                                                 final Alias alias,
                                                 final Set<String> aliasFacturables,
                                                 Date fechaDesde,
                                                 Date fechaHasta) {
    LOG.trace(prefix + "[Recuperando los códigos de alias para el alias de facturación [{}/{}/{}]...]", alias.getCdaliass(), fechaDesde, fechaHasta);
    long iniTime = System.currentTimeMillis();
    int estado = EstadosEnumerados.CONTABILIDAD.DEVENGADA_PDTE_GENERAR_FACTURA.getId();
    /* ALCS */
    final List<Tmct0alc> alcs = new LinkedList<Tmct0alc>();

    if (fechaDesde == null) {
      LOG.debug(prefix + "Parametro fechaDesde nulo, buscando por alias y estado hasta fecha");
      alcs.addAll(tmct0alcBo.findAlcsByEstadoContableAndFeejeliq(estado, aliasFacturables, fechaHasta));
    } else {
      if (fechaHasta == null) {
        Calendar fechaHastaC = Calendar.getInstance();
        fechaHastaC.setTime(fechaDesde);
        fechaHastaC.set(Calendar.DAY_OF_MONTH, fechaHastaC.getActualMaximum(Calendar.DAY_OF_MONTH));

        LOG.debug(prefix + "Se van a obtener los alcs cuya feejeliq este entre : {}-{}", fechaDesde, fechaHastaC.getTime());

        alcs.addAll(tmct0alcBo.findAlcsByEstadoContableToDate(estado, aliasFacturables, fechaDesde, fechaHastaC.getTime()));
      } 
    }
    LOG.debug(prefix + "Se han obtenido {} alcs.", alcs.size());
    long endTime = System.currentTimeMillis();
    LOG.trace(prefix + "+ The query took {} milliseconds", (endTime - iniTime));
    FacturaSugerida facturaSugerida = null;
    if (CollectionUtils.isNotEmpty(alcs)) {
      LOG.trace(prefix + "[Se han recuperado órdenes para facturar para el alias de facturación [{}]...]", alias.getCdaliass());
      final FacturaDetalle facturaDetalle = new FacturaDetalle();
      Date fechaInicio = null;
      Date fechaFin = null;
      for (Tmct0alc tmct0alc : alcs) {
        final AlcOrdenes alcOrden = getAlcOrden(estadoAlcLibre, tmct0alc);
        if (EstadosEnumerados.ALC_ORDENES.LIBRE.getId() == alcOrden.getEstado().getIdestado().intValue()) {
          final Date feejeliq = tmct0alc.getFeejeliq();
          if ((fechaInicio == null) || (feejeliq.compareTo(fechaInicio) < 0)) {
            fechaInicio = feejeliq;
          }
          if ((fechaFin == null) || (feejeliq.compareTo(fechaFin) > 0)) {
            fechaFin = feejeliq;
          }
          LOG.trace(prefix + "[El ALC existe. Está libre. Lo tratamos]");
          alcOrden.setEstado(estadoAlcSugerido);
          BigDecimal importeDevengos = tmct0alc.getImfinsvb();
          facturaDetalle.addAlcOrden(alcOrden, importeDevengos);
        } else {
          LOG.trace(prefix + "[El ALC existe. No está libre. Lo saltamos]");
        }
      }
      if (CollectionUtils.isNotEmpty(facturaDetalle.getAlcOrdenes())) {
        /*
         * El campo getImfinsvb en realidad es el campo imcomisn que se setea antes dentro del método facturaDetalle.addAlcOrden( alcOrden )
         */
        final BigDecimal imfinsvb = facturaDetalle.getImfinsvb();

        facturaSugerida = crearFacturaSugerida(estadoSugerida, moneda, periodoActivo, alias, imfinsvb, fechaInicio, fechaFin);
        facturaSugerida.addDetalleFactura(facturaDetalle);
        facturaSugerida = save(facturaSugerida);
        LOG.debug(prefix + "[Generada una sugerencia de factura para el alias {}]", alias.getCdaliass());
      } else {
        LOG.trace(prefix + "[Todas las ordenes para el alias de facturación [{}] ya han sido facturadas previamente...]", alias.getCdaliass());
      }
    } else {
      LOG.trace(prefix + "[No se han recuperado órdenes para facturar para el alias de facturación [{}]...]", alias.getCdaliass());
    }
    return facturaSugerida;
  }

  /**
   * @param facturasSugeridas
   * @param estadoSugerida
   * @param moneda
   * @param periodoActivo
   * @param tmct0alcAliasData
   * @return
   */
  private FacturaSugerida crearFacturaSugerida(final Tmct0estado estado,
                                               final Monedas moneda,
                                               final Periodos periodo,
                                               final Alias alias,
                                               final BigDecimal imfinsvb,
                                               Date fechaInicio,
                                               Date fechaFin) {
    final FacturaSugerida facturaSugerida = new FacturaSugerida();
    facturaSugerida.setAlias(alias);
    facturaSugerida.setPeriodo(periodo);
    facturaSugerida.setMoneda(moneda);
    facturaSugerida.setFechaCreacion(new Date());
    facturaSugerida.setEstado(estado);
    facturaSugerida.setImfinsvb(imfinsvb);
    facturaSugerida.setFechaInicio(fechaInicio);
    facturaSugerida.setFechaFin(fechaFin);

    return facturaSugerida;
  }

  /**
   * @param prefix
   * @param estadoAlcLibre
   * @param tmct0alcAliasData
   * @return
   */
  private AlcOrdenes getAlcOrden(final Tmct0estado estadoAlcLibre, Tmct0alc tmct0alc) {
    AlcOrdenes alcOrden = alcOrdenesBo.findByTmct0alc(tmct0alc);
    if (alcOrden == null) {
      alcOrden = new AlcOrdenes();
      alcOrden.setNbooking(tmct0alc.getNbooking());
      alcOrden.setNuorden(tmct0alc.getNuorden());
      alcOrden.setNucnfliq(tmct0alc.getNucnfliq());
      alcOrden.setNucnfclt(tmct0alc.getNucnfclt());
      alcOrden.setEstado(estadoAlcLibre);
    }
    return alcOrden;
  }

  /**
   * Recupera las facturas sugeridas en estado <code>EstadosEnumerados.FACTURA_SUGERIDA.SUGERIDA</code> y la marca como
   * <code>EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA</code>
   * 
   * @param idsFacturaSugerida
   * @return
   */
  @Transactional
  public List<Long> marcarFacturasSugeridasFacturar(final List<Long> idsFacturaSugerida) {
    final String prefix = "[FacturaSugeridaBo::marcarFacturasSugeridasFacturar] ";
    final List<Long> idsFacturasSugeridasMarcadas = new ArrayList<Long>();
    final List<FacturaSugerida> facturasSugeridas = (List<FacturaSugerida>) dao.findAll(idsFacturaSugerida);
    if (CollectionUtils.isNotEmpty(facturasSugeridas)) {
      final Tmct0estado estadoAceptada = estadoBo.findById(EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA.getId());
      for (FacturaSugerida facturaSugerida : facturasSugeridas) {
        final Tmct0estado estadoActual = facturaSugerida.getEstado();
        if (EstadosEnumerados.FACTURA_SUGERIDA.SUGERIDA.getId().equals(estadoActual.getIdestado())) {
          facturaSugerida.setEstado(estadoAceptada);
          facturaSugerida = dao.save(facturaSugerida);
          idsFacturasSugeridasMarcadas.add(facturaSugerida.getId());
        } else {
          LOG.info(prefix + "Factura Sugerida no facturable [ID: {}]", facturaSugerida.getId());
        }
      }
    }
    return idsFacturasSugeridasMarcadas;
  }

  /**
   * Recupera el listado de facturas sugeridas
   * 
   * @return
   */
  @Transactional
  public List<FacturaSugeridaDto> getListadoFacturasSugeridas(final Long idAlias, final Integer idEstado) {
    final List<Integer> estados = new ArrayList<Integer>();
    if (idEstado != null) {
      estados.add(idEstado);
    } else {
      estados.add(EstadosEnumerados.FACTURA_SUGERIDA.SUGERIDA.getId());
      estados.add(EstadosEnumerados.FACTURA_SUGERIDA.ACEPTADA.getId());
    }
    final List<FacturaSugerida> facturasSugeridas = dao.findAll(FacturaSugeridaSpecificacions.listadoFriltrado(idAlias, estados, null));
    return getListadoDto(facturasSugeridas);
  }

  /**
   * Recupera el listado de facturas sugeridas
   * 
   * @return
   */
  @Transactional
  public List<FacturaSugeridaDto> getListadoCrearFacturas(final Long idAlias, Date fechaCreacionHasta) {
    final List<Integer> estados = new ArrayList<Integer>();
    estados.add(EstadosEnumerados.FACTURA_SUGERIDA.SUGERIDA.getId());
    final List<FacturaSugerida> facturasSugeridas = dao.findAll(FacturaSugeridaSpecificacions.listadoFriltrado(idAlias, estados, fechaCreacionHasta));
    return getListadoDto(facturasSugeridas);
  }

  /**
   * @param facturasSugeridas
   * @return
   */
  private List<FacturaSugeridaDto> getListadoDto(final List<FacturaSugerida> facturasSugeridas) {
    final List<FacturaSugeridaDto> resultado = new ArrayList<FacturaSugeridaDto>();
    if (CollectionUtils.isNotEmpty(facturasSugeridas)) {
      for (final FacturaSugerida facturaSugerida : facturasSugeridas) {
        final Alias aliasFacturacion = facturaSugerida.getAlias();
        final Tmct0cli tmct0cli = tmct0cliBo.buscarClienteActivo(aliasFacturacion.getCdbrocli());
        final String cif = tmct0cli != null ? tmct0cli.getCif() : "";
        final FacturaSugeridaDto facturaSugeridaDto = new FacturaSugeridaDto(facturaSugerida, aliasFacturacion, cif);
        resultado.add(facturaSugeridaDto);
      }
    }
    return resultado;
  }

  /**
   * Actualiza el estado de la factura sugerida
   * 
   * @param facturaSugeridaAceptada
   * @param estadoFacturada
   * @return
   */
  @Transactional
  public FacturaSugerida actualizarEstado(final FacturaSugerida facturaSugeridaAceptada, final Tmct0estado estadoFacturada) {
    facturaSugeridaAceptada.setEstado(estadoFacturada);
    return this.save(facturaSugeridaAceptada);
  }
}
