package sibbac.business.wrappers.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.CollectionUtils;
import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.wrappers.database.bo.FacturaBo;
import sibbac.business.wrappers.database.model.Factura;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Tarea que envía por mail las facturas.
 * 
 * @author XI316153
 * @see WrapperTaskConcurrencyPrevent
 */
@SIBBACJob(group = Task.GROUP_FACTURACION.NAME, name = Task.GROUP_FACTURACION.JOB_ENVIAR_FACTURA, interval = 1, intervalUnit = IntervalUnit.HOUR)
public class TaskEnvioFactura extends WrapperTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskFactura.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Factura</code>. */
  @Autowired
  private FacturaBo facturaBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent#determinarTipoApunte()
   */
  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_FACTURAS_ENVIO_FACTURA;
  } // determinarTipoApunte

  /*
   * @see sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent#executeTask()
   */
  @Override
  public void executeTask() {
    LOG.debug("[executeTask] Iniciando la tarea de envio de facturas ...");
    Long startTime = System.currentTimeMillis();
    final List<String> taskMessages = new ArrayList<String>();
    
    try {
      final List<Factura> facturas = facturaBo.enviarFacturas(this.jobPk, taskMessages);
      if (CollectionUtils.isNotEmpty(facturas)) {
        taskMessages.add("Enviadas " + facturas.size() + " facturas");
      } else {
        taskMessages.add("No se han enviado facturas");
      } // else
    } catch (Exception ex) {
      throw ex;
    } finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[executeTask] Finalizado tarea de envio de facturas ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
      if (CollectionUtils.isNotEmpty(taskMessages)) {
        for (final String taskMessage : taskMessages) {
          blog("executeTask [" + taskMessage + "]");
        }
      } // if
    } // finally
  } // executeTask

} // TaskEnvioFactura
