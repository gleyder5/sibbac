package sibbac.business.wrappers.database.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import sibbac.business.wrappers.database.model.RecordatorioFacturaPendiente;


@Component
public interface RecordatorioFacturaPendienteDao extends JpaRepository< RecordatorioFacturaPendiente, Long > {

	RecordatorioFacturaPendiente findByFactura_Id( final @Param( "id" ) Long id );

	List< RecordatorioFacturaPendiente > findAllByEstado_IdestadoAndFechaRecordatorioBefore( final Integer idEstado,
			final Date fechaActual );
}
