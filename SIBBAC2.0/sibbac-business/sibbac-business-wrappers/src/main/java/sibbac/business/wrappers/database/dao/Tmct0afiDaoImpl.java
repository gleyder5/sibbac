package sibbac.business.wrappers.database.dao;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.common.DTOUtils;
import sibbac.business.wrappers.database.dto.TitularesDTO;
import sibbac.business.wrappers.database.model.Tmct0afiId;

@Repository
public class Tmct0afiDaoImpl {

  @PersistenceContext
  private EntityManager em;
  
  private static final String TAG = Tmct0afiDaoImpl.class.getName();
  private static final String COUNT_FIND_ALLDTO_PAGEABLE = "countFindAllDTOPageable";
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0afiDaoImpl.class);

  @SuppressWarnings("unchecked")
  public List<TitularesDTO> findAllDTO(Date fechaDesde,
                                       Date fechaHasta,
                                       Character mercadoNacInt,
                                       Character sentido,
                                       String nudnicif,
                                       String nbclient,
                                       String nbclient1,
                                       String nbclient2,
                                       String nbvalors,
                                       Character tpnactit,
                                       String isin,
                                       String alias,
                                       Character tipodocumento,
                                       String paisres,
                                       String tpdomici,
                                       String nbdomici,
                                       String nudomici,
                                       String provincia,
                                       String codpostal,
                                       String nureford) {
    final StringBuilder select = new StringBuilder(
                                                   "SELECT alo.NUCNFCLT, alo.NBOOKING NBOOKING, afi.NUCNFLIQ, afi.NBCLIENT, afi.NBCLIENT1, afi.NBCLIENT2, afi.TPIDENTI, afi.TPSOCIED, "
                                                       + "afi.NUDNICIF, afi.CCV, afi.CDDEPAIS, afi.TPNACTIT, afi.TPDOMICI, afi.NBDOMICI, afi.NUDOMICI, ord.NUREFORD, ord.CDCLSMDO, "
                                                       + "alo.CDTPOPER, alo.CDISIN, afi.CDPOSTAL, afi.NBCIUDAD, afi.NBPROVIN, afi.TPTIPREP, afi.CDNACTIT, afi.PARTICIP,  afi.NUORDEN, "
                                                       + "afi.NUSECUEN, ord.NUTITEJE, afi.CDDDEBIC ");
    final StringBuilder from = new StringBuilder(" from TMCT0AFI afi ");
    final StringBuilder where = new StringBuilder(" where afi.CDINACTI='A' ");
    boolean whereEmpty = false;
    boolean joinAlc = false;
    final String fechaDesdeParamName = "feejeliqDesde";
    if (fechaDesde != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq >=:" + fechaDesdeParamName);
    }

    final String fechaHastaParamName = "feejeliqHasta";
    if (fechaHasta != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq <=:" + fechaHastaParamName);
    }

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    }

    final String aliasParamName = "alias";
    if (alias != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ord.CDCUENTA =:" + aliasParamName);
    }

    final String isinParamName = "isin";
    if (isin != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDISIN =:" + isinParamName);
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afi.nudnicif =:" + nudnicifParamName);
    }

    final String nbvalorsParamName = "nbvalors";
    if (nbvalors != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.NBVALORS=:" + nbvalorsParamName);
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.NUREFORD=:" + nurefordParamName);
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afi.nbclient =:" + nbclientParamName);
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afi.nbclient1 =:" + nbclient1ParamName);
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("afi.nbclient2 =:" + nbclient2ParamName);
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afi.TPIDENTI = '' OR afi.TPIDENTI IS NULL) ");
      } else {
        where.append("afi.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String tpnactitParamName = "tpnactit";
    if (tpnactit != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpnactit.equals('-')) {
        where.append("(afi.TPNACTIT = '' OR afi.TPNACTIT IS NULL) ");
      } else {
        where.append("afi.TPNACTIT=:" + tpnactitParamName);
      }
    }

    final String paisresParamName = "paisres";
    if (paisres != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (paisres.equals("<null>")) {
        where.append("(afi.CDDEPAIS = '' OR afi.CDDEPAIS IS NULL) ");
      } else {
        where.append("afi.CDDEPAIS =:" + paisresParamName);
      }
    }

    final String provinciaParamName = "provincia";
    if (provincia != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (provincia.equals("<null>")) {
        where.append("(afi.NBPROVIN = '' OR afi.NBPROVIN IS NULL) ");
      } else {
        where.append("afi.NBPROVIN =:" + provinciaParamName);
      }
    }

    final String codpostalParamName = "codpostal";
    if (codpostal != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codpostal.equals("<null>")) {
        where.append("(afi.CDPOSTAL = '' OR afi.CDPOSTAL IS NULL) ");
      } else {
        where.append("afi.CDPOSTAL =:" + codpostalParamName);
      }
    }

    final String tpdomiciParamName = "tpdomici";
    if (tpdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpdomici.equals("<null>")) {
        where.append("(afi.TPDOMICI = '' OR afi.TPDOMICI IS NULL) ");
      } else {
        where.append("afi.TPDOMICI =:" + tpdomiciParamName);
      }
    }

    final String nbdomiciParamName = "nbdomici";
    if (nbdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbdomici.equals("<null>")) {
        where.append("(afi.NBDOMICI = '' OR afi.NBDOMICI IS NULL) ");
      } else {
        where.append("afi.NBDOMICI =:" + nbdomiciParamName);
      }
    }

    final String nudomiciParamName = "nudomici";
    if (nudomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudomici.equals("<null>")) {
        where.append("(afi.NUDOMICI = '' OR afi.NUDOMICI IS NULL) ");
      } else {
        where.append("afi.NUDOMICI =:" + nudomiciParamName);
      }
    }

    from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afi.NUCNFCLT AND alo.NBOOKING = afi.NBOOKING AND alo.NUORDEN = afi.NUORDEN ");

    from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");

    if (joinAlc) {
      from.append(" INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT ");
    }
    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());

    setParameters(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, nbvalors, mercadoNacInt,
                  sentido, isin, alias, paisres, provincia, codpostal, tpnactit, null, null, null, null, null, null,
                  nureford, tipodocumento, tpdomici, nbdomici, nudomici, fechaDesdeParamName, fechaHastaParamName,
                  nudnicifParamName, nbclientParamName, nbclient1ParamName, nbclient2ParamName, nbvalorsParamName,
                  mercadoNacIntParamName, sentidoParamName, isinParamName, aliasParamName, paisresParamName,
                  provinciaParamName, codpostalParamName, tpnactitParamName, null, null, null, null, null, null,
                  nurefordParamName, tipodocumentoParamName, tpdomiciParamName, nbdomiciParamName, nudomiciParamName);

    final List<Object> resultList = query.getResultList();
    final List<TitularesDTO> titularesList = new ArrayList<TitularesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TitularesDTO titular = DTOUtils.convertObjectToTitularesNoIdDTO((Object[]) obj);
        titular.setAfiError(false);
        titularesList.add(titular);
      }
    }

    return titularesList;
  }

  @SuppressWarnings("unchecked")
  public List<Tmct0afiId> findAllIds(Date fechaDesde,
                                     Date fechaHasta,
                                     Character mercadoNacInt,
                                     Character sentido,
                                     String nudnicif,
                                     String nbclient,
                                     String nbclient1,
                                     String nbclient2,
                                     String nbvalors,
                                     Character tpnactit,
                                     String isin,
                                     String alias,
                                     Character tipodocumento,
                                     String paisres,
                                     String tpdomici,
                                     String nbdomici,
                                     String nudomici,
                                     String provincia,
                                     String codpostal,
                                     String ciudad,
                                     Character tptiprep,
                                     Character tpsocied,
                                     BigDecimal particip,
                                     String ccv,
                                     String nacionalidad,
                                     String nureford) {
    final StringBuilder select = new StringBuilder(
                                                   "select afi.NUORDEN NUORDEN, afi.NBOOKING NBOOKING, afi.NUCNFCLT NUCNFCLT, afi.NUCNFLIQ NUCNFLIQ, afi.NUSECUEN NUSECUEN ");
    final StringBuilder from = new StringBuilder(" from TMCT0AFI afi ");
    final StringBuilder where = new StringBuilder(" ");
    boolean whereEmpty = true;
    boolean joinAlc = false;
    final String fechaDesdeParamName = "feejeliqDesde";
    if (fechaDesde != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq >=:" + fechaDesdeParamName);
    }

    final String fechaHastaParamName = "feejeliqHasta";
    if (fechaHasta != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alc.feejeliq <=:" + fechaHastaParamName);
    }

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      joinAlc = true;
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER <=:" + sentidoParamName);
    }

    final String isinParamName = "isin";
    if (isin != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDISIN =:" + isinParamName);
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudnicif.equals("<null>")) {
        where.append("(afi.nudnicif = '' OR afi.nudnicif IS NULL) ");
      } else {
        where.append("afi.nudnicif =:" + nudnicifParamName);
      }
    }

    final String nbvalorsParamName = "nbvalors";
    if (nbvalors != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.NBVALORS=:" + nbvalorsParamName);
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }

      if (nbclient.equals("<null>")) {
        where.append("(afi.nbclient = '' OR afi.nbclient IS NULL) ");
      } else {
        where.append("afi.nbclient =:" + nbclientParamName);
      }
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient1.equals("<null>")) {
        where.append("(afi.nbclient1 = '' OR afi.nbclient1 IS NULL) ");
      } else {
        where.append("afi.nbclient1 =:" + nbclient1ParamName);
      }
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient2.equals("<null>")) {
        where.append("(afi.nbclient2 = '' OR afi.nbclient2 IS NULL) ");
      } else {
        where.append("afi.nbclient2 =:" + nbclient2ParamName);
      }
    }

    final String aliasParamName = "alias";
    if (alias != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ord.CDCUENTA =:" + aliasParamName);
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afi.TPIDENTI = '' OR afi.TPIDENTI IS NULL) ");
      } else {
        where.append("afi.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String tpnactitParamName = "tpnactit";
    if (tpnactit != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpnactit.equals('-')) {
        where.append("(afi.TPNACTIT = '' OR afi.TPNACTIT IS NULL) ");
      } else {
        where.append("afi.TPNACTIT= =:" + tpnactitParamName);
      }
    }

    final String paisresParamName = "paisres";
    if (paisres != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (paisres.equals("<null>")) {
        where.append("(afi.CDDEPAIS = '' OR afi.CDDEPAIS IS NULL) ");
      } else {
        where.append("afi.CDDEPAIS =:" + paisresParamName);
      }
    }

    final String provinciaParamName = "provincia";
    if (provincia != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (provincia.equals("<null>")) {
        where.append("(afi.NBPROVIN = '' OR afi.NBPROVIN IS NULL) ");
      } else {
        where.append("afi.NBPROVIN =:" + provinciaParamName);
      }
    }

    final String codpostalParamName = "codpostal";
    if (codpostal != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codpostal.equals("<null>")) {
        where.append("(afi.CDPOSTAL = '' OR afi.CDPOSTAL IS NULL) ");
      } else {
        where.append("afi.CDPOSTAL =:" + codpostalParamName);
      }
    }

    final String tpdomiciParamName = "tpdomici";
    if (tpdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpdomici.equals("<null>")) {
        where.append("(afi.TPDOMICI = '' OR afi.TPDOMICI IS NULL) ");
      } else {
        where.append("afi.TPDOMICI =:" + tpdomiciParamName);
      }
    }

    final String nbdomiciParamName = "nbdomici";
    if (nbdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbdomici.equals("<null>")) {
        where.append("(afi.NBDOMICI = '' OR afi.NBDOMICI IS NULL) ");
      } else {
        where.append("afi.NBDOMICI =:" + nbdomiciParamName);
      }
    }

    final String nudomiciParamName = "nudomici";
    if (nudomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudomici.equals("<null>")) {
        where.append("(afi.NUDOMICI = '' OR afi.NUDOMICI IS NULL) ");
      } else {
        where.append("afi.NUDOMICI =:" + nudomiciParamName);
      }
    }

    final String ciudadParamName = "ciudad";
    if (ciudad != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (ciudad.equals("<null>")) {
        where.append("(afi.NBCIUDAD = '' OR afi.NBCIUDAD IS NULL) ");
      } else {
        where.append("afi.NBCIUDAD =:" + ciudadParamName);
      }
    }

    final String tptiprepParamName = "tptiprep";
    if (tptiprep != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tptiprep.equals("<null>")) {
        where.append("(afi.TPTIPREP = '' OR afi.TPTIPREP IS NULL) ");
      } else {
        where.append("afi.TPTIPREP =:" + tptiprepParamName);
      }
    }

    final String tpsociedParamName = "tpsocied";
    if (tpsocied != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpsocied.equals("<null>")) {
        where.append("(afi.TPSOCIED = '' OR afi.TPSOCIED IS NULL) ");
      } else {
        where.append("afi.TPSOCIED =:" + tpsociedParamName);
      }
    }

    final String participParamName = "particip";
    if (particip != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (particip.equals("<null>")) {
        where.append("(afi.PARTICIP = '' OR afi.PARTICIP IS NULL) ");
      } else {
        where.append("afi.PARTICIP =:" + participParamName);
      }
    }

    final String ccvParamName = "ccv";
    if (ccv != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (ccv.equals("<null>")) {
        where.append("(afi.CCV = '' OR afi.CCV IS NULL) ");
      } else {
        where.append("afi.CCV =:" + ccvParamName);
      }
    }

    final String nacionalidadParamName = "nacionalidad";
    if (nacionalidad != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nacionalidad.equals("<null>")) {
        where.append("(afi.CDNACTIT = '' OR afi.CDNACTIT IS NULL) ");
      } else {
        where.append("afi.CDNACTIT =:" + nacionalidadParamName);
      }
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.equals("<null>")) {
        where.append("(ord.NUREFORD = '' OR ord.NUREFORD IS NULL) ");
      } else {
        where.append("ord.NUREFORD =:" + nurefordParamName);
      }
    }

    from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afi.NUCNFCLT AND alo.NBOOKING = afi.NBOOKING AND alo.NUORDEN = afi.NUORDEN ");

    from.append(" LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN ");

    if (joinAlc) {
      from.append(" INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT ");
    }
    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());

    setParameters(query, fechaDesde, fechaHasta, nudnicif, nbclient, nbclient1, nbclient2, nbvalors, mercadoNacInt,
                  sentido, isin, alias, paisres, provincia, codpostal, tpnactit, ciudad, tptiprep, tpsocied, particip,
                  ccv, nacionalidad, nureford, tipodocumento, tpdomici, nbdomici, nudomici, fechaDesdeParamName,
                  fechaHastaParamName, nudnicifParamName, nbclientParamName, nbclient1ParamName, nbclient2ParamName,
                  nbvalorsParamName, mercadoNacIntParamName, sentidoParamName, isinParamName, aliasParamName,
                  paisresParamName, provinciaParamName, codpostalParamName, tpnactitParamName, ciudadParamName,
                  tptiprepParamName, tpsociedParamName, participParamName, ccvParamName, nacionalidadParamName,
                  nurefordParamName, tipodocumentoParamName, tpdomiciParamName, nbdomiciParamName, nudomiciParamName);

    final List<Object[]> resultList = (List<Object[]>) query.getResultList();
    List<Tmct0afiId> afiIds = new ArrayList<Tmct0afiId>();

    for (Object[] afiKeys : resultList) {
      afiIds.add(new Tmct0afiId((String) afiKeys[0], (String) afiKeys[1], (String) afiKeys[2],
                                ((BigDecimal) afiKeys[3]).shortValue(), (BigDecimal) afiKeys[4]));
    }

    return afiIds;

  }

  private void setParameters(Query query,
                             Date fechaDesde,
                             Date fechaHasta,
                             String nudnicif,
                             String nbclient,
                             String nbclient1,
                             String nbclient2,
                             String nbvalors,
                             Character mercadoNacInt,
                             Character sentido,
                             String isin,
                             String alias,
                             String paisres,
                             String provincia,
                             String codpostal,
                             Character tpnactit,
                             String ciudad,
                             Character tptiprep,
                             Character tpsocied,
                             BigDecimal particip,
                             String ccv,
                             String nacionalidad,
                             String nureford,
                             Character tipodocumento,
                             String tpdomici,
                             String nbdomici,
                             String nudomici,
                             String fechaDesdeParamName,
                             String fechaHastaParamName,
                             String nudnicifParamName,
                             String nbclientParamName,
                             String nbclient1ParamName,
                             String nbclient2ParamName,
                             String nbvalorsParamName,
                             String mercadoNacIntParamName,
                             String sentidoParamName,
                             String isinParamName,
                             String aliasParamName,
                             String paisresParamName,
                             String provinciaParamName,
                             String codpostalParamName,
                             String tpnactitParamName,
                             String ciudadParamName,
                             String tptiprepParamName,
                             String tpsociedParamName,
                             String participParamName,
                             String ccvParamName,
                             String nacionalidadParamName,
                             String nurefordParamName,
                             String tipodocumentoParamName,
                             String tpdomiciParamName,
                             String nbdomiciParamName,
                             String nudomiciParamName) {
    if (fechaDesde != null) {
      query.setParameter(fechaDesdeParamName, fechaDesde, TemporalType.DATE);
    }
    if (fechaHasta != null) {
      query.setParameter(fechaHastaParamName, fechaHasta, TemporalType.DATE);
    }
    if (nudnicif != null) {
      query.setParameter(nudnicifParamName, nudnicif);
    }
    if (nbclient != null) {
      query.setParameter(nbclientParamName, nbclient);
    }
    if (nbclient1 != null) {
      query.setParameter(nbclient1ParamName, nbclient1);
    }
    if (nbclient2 != null) {
      query.setParameter(nbclient2ParamName, nbclient2);
    }
    if (nbvalors != null) {
      query.setParameter(nbvalorsParamName, nbvalors);
    }
    if (mercadoNacInt != null) {
      query.setParameter(mercadoNacIntParamName, mercadoNacInt);
    }
    if (sentido != null) {
      query.setParameter(sentidoParamName, sentido);
    }
    if (isin != null) {
      query.setParameter(isinParamName, isin);
    }
    if (alias != null) {
      query.setParameter(aliasParamName, alias);
    }
    if (paisres != null && !paisres.equals("<null>")) {
      query.setParameter(paisresParamName, paisres);
    }
    if (provincia != null && !provincia.equals("<null>")) {
      query.setParameter(provinciaParamName, provincia);
    }
    if (codpostal != null && !codpostal.equals("<null>")) {
      query.setParameter(codpostalParamName, codpostal);
    }
    if (tpnactit != null && !tpnactit.equals('-')) {
      query.setParameter(tpnactitParamName, tpnactit);
    }
    if (ciudad != null && !ciudad.equals("<null>")) {
      query.setParameter(ciudadParamName, ciudad);
    }
    if (tptiprep != null && !tptiprep.equals('-')) {
      query.setParameter(tptiprepParamName, tptiprep);
    }
    if (tpsocied != null && !tpsocied.equals('-')) {
      query.setParameter(tpsociedParamName, tpsocied);
    }
    if (tipodocumento != null && !tipodocumento.equals('-')) {
      query.setParameter(tipodocumentoParamName, tipodocumento);
    }
    if (particip != null && !particip.equals("<null>")) {
      query.setParameter(participParamName, particip);
    }
    if (ccv != null && !ccv.equals("<null>")) {
      query.setParameter(ccvParamName, ccv);
    }
    if (nacionalidad != null && !nacionalidad.equals("<null>")) {
      query.setParameter(nacionalidadParamName, nacionalidad);
    }
    if (nureford != null && !nureford.equals("<null>")) {
      query.setParameter(nurefordParamName, nureford);
    }
    if (tpdomici != null && !tpdomici.equals("<null>")) {
      query.setParameter(tpdomiciParamName, tpdomici);
    }
    if (nbdomici != null && !nbdomici.equals("<null>")) {
      query.setParameter(nbdomiciParamName, nbdomici);
    }
    if (nudomici != null && !nudomici.equals("<null>")) {
      query.setParameter(nudomiciParamName, nudomici);
    }
  }

  /**
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param mercadoNacInt
   * @param sentido
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param tpnactit
   * @param isin
   * @param alias
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param nureford
   * @param nbooking
   * @return
   */
  public Integer countFindAllDTOPageable(Date fechaDesde,
                                         Date fechaHasta,
                                         Character mercadoNacInt,
                                         Character sentido,

                                         List<String> nudnicif,
                                         List<String> nbclient,
                                         List<String> nbclient1,
                                         List<String> nbclient2,
                                         Character tpnactit,
                                         List<String> isin,
                                         List<String> alias,
                                         Character tipodocumento,
                                         String paisres,
                                         String tpdomici,
                                         String nbdomici,
                                         String nudomici,
                                         String provincia,
                                         String codpostal,
                                         String nureford,
                                         String nbooking) {
    final StringBuilder select = new StringBuilder("SELECT COUNT(*) ");

    StringBuilder from = null;
    // final StringBuilder where = new StringBuilder(
    // " where afi.CDINACTI='A' " );
    final StringBuilder where = new StringBuilder("");
    if (mercadoNacInt == 'N') {
      where.append(" where afi.nuorden = alc.nuorden AND afi.nbooking = alc.nbooking AND afi.nucnfclt = alc.nucnfclt AND afi.nucnfliq = alc.nucnfliq and afi.CDINACTI='A' ");
      from = new StringBuilder(" from TMCT0ALC alc ");
    } else {
      where.append(" where afi.nuorden = alo.nuorden AND afi.nbooking = alo.nbooking AND afi.nucnfclt = alo.nucnfclt and afi.CDINACTI='A' ");
      from = new StringBuilder(" from TMCT0ALO alo ");
    }

    boolean whereEmpty = false;

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";

    DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
    String fDesde;
    String fHasta;
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fDesde = dFormat.format(fechaDesde);
        // where.append( "alc.feejeliq >=:" + fechaDesdeParamName );
        where.append("alc.feejeliq >=date('" + fDesde + "')");
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fHasta = dFormat.format(fechaHasta);
        // where.append( "alc.feejeliq <=:" + fechaHastaParamName );
        where.append("alc.feejeliq <=date('" + fHasta + "')");
      }
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fDesde = dFormat.format(fechaDesde);
        // where.append( "alo.FEOPERAC >=:" + fechaDesdeParamName );
        where.append("alo.FEOPERAC >=date('" + fDesde + "')");
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fHasta = dFormat.format(fechaHasta);
        // where.append( "alo.FEOPERAC <=:" + fechaHastaParamName );
        where.append("alo.FEOPERAC <=date('" + fHasta + "')");
      }
    }

    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ord.CDTPOPER =:" + sentidoParamName);
    }

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbooking.indexOf("#") != -1) {
        nbooking = nbooking.replace("#", "");
        where.append("alo.NBOOKING <>:" + nbookingParamName);
      } else
        where.append("alo.NBOOKING =:" + nbookingParamName);
    }

    boolean aliasInv = false;
    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      String cadena = "";
      Integer nCont = 0;
      for (String alia : alias) {
        if (nCont == 0) {
          if (alia.indexOf("#") != -1) {
            alia = alia.replace("#", "");
            aliasInv = true;
          }
          cadena += "'" + alia + "'";
        } else
          cadena += ",'" + alia + "'";
        nCont++;

      }
      if (!aliasInv)
        where.append("ord.CDCUENTA IN (" + cadena + ")");
      else
        where.append("ord.CDCUENTA NOT IN (" + cadena + ")");
      // where.append( "ord.CDCUENTA IN :" + aliasParamName );
    }

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (isin.get(0).indexOf("#") != -1) {
        isin.set(0, isin.get(0).replace("#", ""));
        where.append("ord.CDISIN NOT IN:" + isinParamName);
      } else {
        where.append("ord.CDISIN IN:" + isinParamName);
      }
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null && nudnicif.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudnicif.get(0).indexOf("#") != -1) {
        nudnicif.set(0, nudnicif.get(0).replace("#", ""));
        where.append("(afi.nudnicif NOT IN ( :" + nudnicifParamName + " ) OR  afi.cdddebic NOT IN ( :"
                     + nudnicifParamName + " ) ) ");
      } else {
        where.append("(afi.nudnicif IN ( :" + nudnicifParamName + " ) OR  afi.cdddebic IN ( :" + nudnicifParamName
                     + " ) ) ");
      }
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.indexOf("#") != -1) {
        nureford = nureford.replace("#", "");
        where.append("(ORD.NUREFORD<>:" + nurefordParamName+" OR alo.nucnfclt<>'PARTENON'||:"+nurefordParamName+")");
      } else {
        where.append("(ORD.NUREFORD=:" + nurefordParamName+" OR alo.nucnfclt='PARTENON'||:"+nurefordParamName+")");
      }
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }

      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null && nbclient.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient.get(0).indexOf("#") != -1) {
        nbclient.set(0, nbclient.get(0).replace("#", ""));
        where.append("afi.nbclient NOT IN :" + nbclientParamName);
      } else {
        where.append("afi.nbclient IN :" + nbclientParamName);
      }
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null && nbclient1.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient1.get(0).indexOf("#") != -1) {
        nbclient1.set(0, nbclient1.get(0).replace("#", ""));
        where.append("afi.nbclient1 NOT IN :" + nbclient1ParamName);
      } else {
        where.append("afi.nbclient1 IN :" + nbclient1ParamName);
      }
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null && nbclient2.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient2.get(0).indexOf("#") != -1) {
        nbclient2.set(0, nbclient2.get(0).replace("#", ""));
        where.append("afi.nbclient2 NOT IN :" + nbclient2ParamName);
      } else {
        where.append("afi.nbclient2 IN :" + nbclient2ParamName);
      }
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afi.TPIDENTI = '' OR afi.TPIDENTI IS NULL) ");
      } else {
        where.append("afi.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String tpnactitParamName = "tpnactit";
    if (tpnactit != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpnactit.equals('-')) {
        where.append("(afi.TPNACTIT = '' OR afi.TPNACTIT IS NULL) ");
      } else {
        where.append("afi.TPNACTIT=:" + tpnactitParamName);
      }
    }

    final String paisresParamName = "paisres";
    if (paisres != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (paisres.equals("<null>")) {
        where.append("(afi.CDDEPAIS = '' OR afi.CDDEPAIS IS NULL) ");
      } else {
        where.append("afi.CDDEPAIS =:" + paisresParamName);
      }
    }

    final String provinciaParamName = "provincia";
    if (provincia != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (provincia.equals("<null>")) {
        where.append("(afi.NBPROVIN = '' OR afi.NBPROVIN IS NULL) ");
      } else {
        where.append("afi.NBPROVIN =:" + provinciaParamName);
      }
    }

    final String codpostalParamName = "codpostal";
    if (codpostal != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codpostal.equals("<null>")) {
        where.append("(afi.CDPOSTAL = '' OR afi.CDPOSTAL IS NULL) ");
      } else {
        where.append("afi.CDPOSTAL =:" + codpostalParamName);
      }
    }

    final String tpdomiciParamName = "tpdomici";
    if (tpdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpdomici.equals("<null>")) {
        where.append("(afi.TPDOMICI = '' OR afi.TPDOMICI IS NULL) ");
      } else {
        where.append("afi.TPDOMICI =:" + tpdomiciParamName);
      }
    }

    final String nbdomiciParamName = "nbdomici";
    if (nbdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbdomici.equals("<null>")) {
        where.append("(afi.NBDOMICI = '' OR afi.NBDOMICI IS NULL) ");
      } else {
        where.append("afi.NBDOMICI =:" + nbdomiciParamName);
      }
    }

    final String nudomiciParamName = "nudomici";
    if (nudomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudomici.equals("<null>")) {
        where.append("(afi.NUDOMICI = '' OR afi.NUDOMICI IS NULL) ");
      } else {
        where.append("afi.NUDOMICI =:" + nudomiciParamName);
      }
    }

    // from.append(
    // " LEFT JOIN TMCT0ALO alo ON alo.NUCNFCLT = afi.NUCNFCLT AND alo.NBOOKING = afi.NBOOKING AND alo.NUORDEN = afi.NUORDEN "
    // );
    // from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUORDEN = alc.NUORDEN AND alo.NBOOKING = alc.NBOOKING AND alo.NUCNFCLT = alc.NUCNFCLT");

    // from.append( " LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN "
    // );
    if (mercadoNacInt == 'N') {
      from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUORDEN = alc.NUORDEN AND alo.NBOOKING = alc.NBOOKING AND alo.NUCNFCLT = alc.NUCNFCLT ");
    }
    // from.append( " LEFT JOIN TMCT0ORD ord ON alo.NUORDEN = ord.NUORDEN "
    // );
    from.append(" LEFT JOIN TMCT0ORD ord ON ord.NUORDEN = alo.NUORDEN ");

    // if ( mercadoNacInt == 'N' ) {
    // from.append(
    // " INNER JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT "
    // );
    // } else {
    // from.append(
    // " LEFT JOIN TMCT0ALC alc ON alc.NBOOKING = alo.NBOOKING AND alc.NUORDEN = alo.NUORDEN AND alc.NUCNFCLT = alo.NUCNFCLT "
    // );
    // }

    // from.append(
    // " LEFT JOIN TMCT0ALI ali ON ord.cdcuenta = ali.CDALIASS AND FHFINAL = 99991231 "
    // );
    from.append(" LEFT JOIN TMCT0ALI ali ON ali.cdaliass = ord.cdcuenta AND ali.FHFINAL = 99991231 ");

    from.append(",TMCT0AFI afi ");
    String qs = select.append(from.append(where)).toString();
    final Query query = em.createNativeQuery(qs);

    setParametersWithVectors(query, null, null, nudnicif, nbclient, nbclient1, nbclient2, mercadoNacInt, sentido, isin,
                             null, paisres, provincia, codpostal, tpnactit, null, null, null, null, null, null,
                             nureford, nbooking, fechaDesdeParamName, fechaHastaParamName, nudnicifParamName,
                             nbclientParamName, nbclient1ParamName, nbclient2ParamName, mercadoNacIntParamName,
                             sentidoParamName, isinParamName, aliasParamName, paisresParamName, provinciaParamName,
                             codpostalParamName, tpnactitParamName, null, null, null, null, null, null,
                             nurefordParamName, nbookingParamName, null, null, null, null);

    LOG.debug("******************************************************");
    LOG.debug("******************************************************");
    LOG.debug("*************** CONTANDO TITULARES ROUTING ***********");
    LOG.debug("******************************************************");
    LOG.debug("{} :: {} query: {}", TAG, COUNT_FIND_ALLDTO_PAGEABLE, qs);
    Integer result = 0;
    try {
      result = (Integer) query.getSingleResult();
      LOG.debug(TAG.concat(" :: ").concat(COUNT_FIND_ALLDTO_PAGEABLE).concat(" Se han encontrado: ")
                   .concat(String.valueOf(result)).concat(" registros."));
    } catch (PersistenceException ex) {
      LOG.error(TAG.concat(" :: ").concat(COUNT_FIND_ALLDTO_PAGEABLE).concat(ex.getMessage()).concat(" cause: ")
                   .concat(ex.getCause().toString()), ex);
    }
    LOG.debug("******************************************************");
    return result;
  }

  /**
   * Obtiene control de titulares paginados
   * 
   * @param fechaDesde
   * @param fechaHasta
   * @param mercadoNacInt
   * @param sentido
   * @param nudnicif
   * @param nbclient
   * @param nbclient1
   * @param nbclient2
   * @param tpnactit
   * @param isin
   * @param alias
   * @param tipodocumento
   * @param paisres
   * @param tpdomici
   * @param nbdomici
   * @param nudomici
   * @param provincia
   * @param codpostal
   * @param nureford
   * @param nbooking
   * @param startPosition
   * @param maxResult
   * @return
   */
  @SuppressWarnings("unchecked")
public List<TitularesDTO> findAllDTOPageable(Date fechaDesde,
                                               Date fechaHasta,
                                               Character mercadoNacInt,
                                               Character sentido,
                                               List<String> nudnicif,
                                               List<String> nbclient,
                                               List<String> nbclient1,
                                               List<String> nbclient2,
                                               Character tpnactit,
                                               List<String> isin,
                                               List<String> alias,
                                               Character tipodocumento,
                                               String paisres,
                                               String tpdomici,
                                               String nbdomici,
                                               String nudomici,
                                               String provincia,
                                               String codpostal,
                                               String nureford,
                                               String nbooking,
                                               Integer startPosition,
                                               Integer maxResult) {
    final StringBuilder select = new StringBuilder(
                                                   "SELECT alo.NUCNFCLT NUCNFCLT, alo.NBOOKING NBOOKING, afi.NUCNFLIQ, afi.NBCLIENT, afi.NBCLIENT1, "
                                                       + "afi.NBCLIENT2, afi.TPIDENTI, afi.TPSOCIED, afi.NUDNICIF, afi.CCV, afi.CDDEPAIS, afi.TPNACTIT, "
                                                       + "afi.TPDOMICI, afi.NBDOMICI, afi.NUDOMICI, ord.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, "
                                                       + "afi.CDPOSTAL, afi.NBCIUDAD, afi.NBPROVIN, afi.TPTIPREP, afi.CDNACTIT, afi.PARTICIP,  afi.NUORDEN, "
                                                       + "afi.NUSECUEN, ord.NUTITEJE,  ali.CDALIASS, ali.DESCRALI, afi.CDDDEBIC, ");
    if (mercadoNacInt == 'N') {
      // select.append(
      // "alc.FEEJELIQ , ROW_NUMBER() OVER (ORDER BY afi.nuorden DESC, afi.nbooking DESC, afi.nucnfclt DESC, afi.nucnfliq DESC) AS row_number  "
      // );
      select.append("alc.FEEJELIQ , ROW_NUMBER() OVER () AS row_number  ");
    } else {
      // select.append(
      // "alo.FEOPERAC , ROW_NUMBER() OVER (ORDER BY afi.nuorden DESC, afi.nbooking DESC, afi.nucnfclt DESC, afi.nucnfliq DESC) AS row_number  "
      // );
      select.append("alo.FEOPERAC , ROW_NUMBER() OVER () AS row_number  ");
    }
    // final StringBuilder from = new StringBuilder( " from TMCT0AFI afi "
    // );
    StringBuilder from = null;
    // final StringBuilder where = new StringBuilder(
    // " where afi.CDINACTI='A' " );
    final StringBuilder where = new StringBuilder("");
    if (mercadoNacInt == 'N') {
      where.append(" where afi.nuorden = alc.nuorden AND afi.nbooking = alc.nbooking AND afi.nucnfclt = alc.nucnfclt AND afi.nucnfliq = alc.nucnfliq and afi.CDINACTI='A' ");
      from = new StringBuilder(" from TMCT0ALC alc ");
    } else {
      where.append(" where afi.nuorden = alo.nuorden AND afi.nbooking = alo.nbooking AND afi.nucnfclt = alo.nucnfclt and afi.CDINACTI='A' ");
      from = new StringBuilder(" from TMCT0ALO alo ");
    }
    boolean whereEmpty = false;

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";

    DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
    String fDesde;
    String fHasta;
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fDesde = dFormat.format(fechaDesde);
        // where.append( "alc.feejeliq >=:" + fechaDesdeParamName );
        where.append("alc.feejeliq >=date('" + fDesde + "')");
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fHasta = dFormat.format(fechaHasta);
        // where.append( "alc.feejeliq <=:" + fechaHastaParamName );
        where.append("alc.feejeliq <=date('" + fHasta + "')");
      }
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fDesde = dFormat.format(fechaDesde);
        // where.append( "alo.FEOPERAC >=:" + fechaDesdeParamName );
        where.append("alo.FEOPERAC >=date('" + fDesde + "')");
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fHasta = dFormat.format(fechaHasta);
        // where.append( "alo.FEOPERAC <=:" + fechaHastaParamName );
        where.append("alo.FEOPERAC <=date('" + fHasta + "')");
      }
    }
    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    }

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbooking.indexOf("#") != -1) {
        nbooking = nbooking.replace("#", "");
        where.append("alo.NBOOKING <>:" + nbookingParamName);
      } else {
        where.append("alo.NBOOKING =:" + nbookingParamName);
      }
    }

    boolean aliasInv = false;
    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      String cadena = "";
      Integer nCont = 0;
      for (String alia : alias) {
        if (nCont == 0) {
          if (alia.indexOf("#") != -1) {
            alia = alia.replace("#", "");
            aliasInv = true;
          }
          cadena += "'" + alia + "'";
        } else
          cadena += ",'" + alia + "'";
        nCont++;

      }
      if (!aliasInv)
        where.append("ord.CDCUENTA IN (" + cadena + ")");
      else
        where.append("ord.CDCUENTA NOT IN (" + cadena + ")");
      // where.append( "ord.CDCUENTA IN :" + aliasParamName );
    }

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (isin.get(0).indexOf("#") != -1) {
        isin.set(0, isin.get(0).replace("#", ""));
        where.append("alo.CDISIN NOT IN:" + isinParamName);
      } else {
        where.append("alo.CDISIN IN:" + isinParamName);
      }
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null && nudnicif.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudnicif.get(0).indexOf("#") != -1) {
        nudnicif.set(0, nudnicif.get(0).replace("#", ""));
        where.append("(afi.nudnicif NOT IN ( :" + nudnicifParamName + " ) OR  afi.cdddebic NOT IN ( :"
                     + nudnicifParamName + "  ) ) ");
      } else {
        where.append("(afi.nudnicif IN ( :" + nudnicifParamName + " ) OR  afi.cdddebic IN ( :" + nudnicifParamName
                     + "  ) ) ");
      }
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.indexOf("#") != -1) {
        nureford = nureford.replace("#", "");
        where.append("ORD.NUREFORD<>:" + nurefordParamName);
      } else {
        where.append("ORD.NUREFORD=:" + nurefordParamName);
      }
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null && nbclient.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient.get(0).indexOf("#") != -1) {
        nbclient.set(0, nbclient.get(0).replace("#", ""));
        where.append("afi.nbclient NOT IN :" + nbclientParamName);
      } else {
        where.append("afi.nbclient IN :" + nbclientParamName);
      }
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null && nbclient1.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient1.get(0).indexOf("#") != -1) {
        nbclient1.set(0, nbclient1.get(0).replace("#", ""));
        where.append("afi.nbclient1 NOT IN :" + nbclient1ParamName);
      } else {
        where.append("afi.nbclient1 IN :" + nbclient1ParamName);
      }
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null && nbclient2.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient2.get(0).indexOf("#") != -1) {
        nbclient2.set(0, nbclient2.get(0).replace("#", ""));
        where.append("afi.nbclient2 NOT IN :" + nbclient2ParamName);
      } else {
        where.append("afi.nbclient2 IN :" + nbclient2ParamName);
      }
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afi.TPIDENTI = '' OR afi.TPIDENTI IS NULL) ");
      } else {
        where.append("afi.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String tpnactitParamName = "tpnactit";
    if (tpnactit != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpnactit.equals('-')) {
        where.append("(afi.TPNACTIT = '' OR afi.TPNACTIT IS NULL) ");
      } else {
        where.append("afi.TPNACTIT=:" + tpnactitParamName);
      }
    }

    final String paisresParamName = "paisres";
    if (paisres != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (paisres.equals("<null>")) {
        where.append("(afi.CDDEPAIS = '' OR afi.CDDEPAIS IS NULL) ");
      } else {
        where.append("afi.CDDEPAIS =:" + paisresParamName);
      }
    }

    final String provinciaParamName = "provincia";
    if (provincia != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (provincia.equals("<null>")) {
        where.append("(afi.NBPROVIN = '' OR afi.NBPROVIN IS NULL) ");
      } else {
        where.append("afi.NBPROVIN =:" + provinciaParamName);
      }
    }

    final String codpostalParamName = "codpostal";
    if (codpostal != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codpostal.equals("<null>")) {
        where.append("(afi.CDPOSTAL = '' OR afi.CDPOSTAL IS NULL) ");
      } else {
        where.append("afi.CDPOSTAL =:" + codpostalParamName);
      }
    }

    final String tpdomiciParamName = "tpdomici";
    if (tpdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpdomici.equals("<null>")) {
        where.append("(afi.TPDOMICI = '' OR afi.TPDOMICI IS NULL) ");
      } else {
        where.append("afi.TPDOMICI =:" + tpdomiciParamName);
      }
    }

    final String nbdomiciParamName = "nbdomici";
    if (nbdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbdomici.equals("<null>")) {
        where.append("(afi.NBDOMICI = '' OR afi.NBDOMICI IS NULL) ");
      } else {
        where.append("afi.NBDOMICI =:" + nbdomiciParamName);
      }
    }

    final String nudomiciParamName = "nudomici";
    if (nudomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudomici.equals("<null>")) {
        where.append("(afi.NUDOMICI = '' OR afi.NUDOMICI IS NULL) ");
      } else {
        where.append("afi.NUDOMICI =:" + nudomiciParamName);
      }
    }

    if (mercadoNacInt == 'N') {
      from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUORDEN = alc.NUORDEN AND alo.NBOOKING = alc.NBOOKING AND alo.NUCNFCLT = alc.NUCNFCLT");
    }

    from.append(" LEFT JOIN TMCT0ORD ord ON ord.NUORDEN = alo.NUORDEN ");

    from.append(" LEFT JOIN TMCT0ALI ali ON ali.cdaliass = ord.cdcuenta AND ali.FHFINAL = 99991231 ");

    from.append(",TMCT0AFI afi ");

    final Query query = em.createNativeQuery(select.append(from.append(where)).toString());

    setParametersWithVectors(query, null, null, nudnicif, nbclient, nbclient1, nbclient2, mercadoNacInt, sentido, isin,
                             null, paisres, provincia, codpostal, tpnactit, null, null, null, null, null, null,
                             nureford, nbooking, fechaDesdeParamName, fechaHastaParamName, nudnicifParamName,
                             nbclientParamName, nbclient1ParamName, nbclient2ParamName, mercadoNacIntParamName,
                             sentidoParamName, isinParamName, aliasParamName, paisresParamName, provinciaParamName,
                             codpostalParamName, tpnactitParamName, null, null, null, null, null, null,
                             nurefordParamName, nbookingParamName, null, null, null, null);
    query.setFirstResult(startPosition);
    query.setMaxResults(maxResult);
    List<Object> resultList = null;
    try {
      resultList = query.getResultList();
    } catch (Exception ex) {
    	LOG.error("Error: ", ex.getClass().getName(), ex.getMessage());
    }
    final List<TitularesDTO> titularesList = new ArrayList<TitularesDTO>();
    if (CollectionUtils.isNotEmpty(resultList)) {
      for (Object obj : resultList) {
        TitularesDTO titular = DTOUtils.convertObjectToTitularesNoIdDTO((Object[]) obj);
        titular.setAfiError(false);
        titularesList.add(titular);
      }
    }

    return titularesList;
  }

  @SuppressWarnings("unchecked")
public List<TitularesDTO> findAllDTO(Date fechaDesde,
                                       Date fechaHasta,
                                       Character mercadoNacInt,
                                       Character sentido,
                                       List<String> nudnicif,
                                       List<String> nbclient,
                                       List<String> nbclient1,
                                       List<String> nbclient2,
                                       Character tpnactit,
                                       List<String> isin,
                                       List<String> alias,
                                       Character tipodocumento,
                                       String paisres,
                                       String tpdomici,
                                       String nbdomici,
                                       String nudomici,
                                       String provincia,
                                       String codpostal,
                                       String nureford,
                                       String nbooking,
                                       Integer firstRow,
                                       Integer lastRow) {
    final StringBuilder selectExterno = new StringBuilder("SELECT * FROM (");
    final StringBuilder select = new StringBuilder("SELECT alo.NUCNFCLT NUCNFCLT, alo.NBOOKING NBOOKING, afi.NUCNFLIQ, afi.NBCLIENT, afi.NBCLIENT1, "
                                                       + "afi.NBCLIENT2, afi.TPIDENTI, afi.TPSOCIED, afi.NUDNICIF, afi.CCV, afi.CDDEPAIS, afi.TPNACTIT, "
                                                       + "afi.TPDOMICI, afi.NBDOMICI, afi.NUDOMICI, ord.NUREFORD, ord.CDCLSMDO, alo.CDTPOPER, alo.CDISIN, "
                                                       + "afi.CDPOSTAL, afi.NBCIUDAD, afi.NBPROVIN, afi.TPTIPREP, afi.CDNACTIT, afi.PARTICIP,  afi.NUORDEN, "
                                                       + "afi.NUSECUEN, ord.NUTITEJE,  ali.CDALIASS, ali.DESCRALI, afi.CDDDEBIC, ");
    if (mercadoNacInt == 'N') {
      select.append("alc.FEEJELIQ , ROW_NUMBER() OVER (ORDER BY afi.nuorden DESC, afi.nbooking DESC, afi.nucnfclt DESC, afi.nucnfliq DESC) AS row_number  ");
    } else {
      select.append("alo.FEOPERAC , ROW_NUMBER() OVER (ORDER BY afi.nuorden DESC, afi.nbooking DESC, afi.nucnfclt DESC, afi.nucnfliq DESC) AS row_number  ");
    }
    // final StringBuilder from = new StringBuilder( " from TMCT0AFI afi "
    // ); // );
    StringBuilder from = null;
    // final StringBuilder where = new StringBuilder(
    // " where afi.CDINACTI='A' " );
    final StringBuilder where = new StringBuilder("");
    if (mercadoNacInt == 'N') {
      where.append(" where afi.nuorden = alc.nuorden AND afi.nbooking = alc.nbooking AND afi.nucnfclt = alc.nucnfclt AND afi.nucnfliq = alc.nucnfliq and afi.CDINACTI='A' ");
      from = new StringBuilder(" from TMCT0ALC alc ");
    } else {
      where.append(" where afi.nuorden = alo.nuorden AND afi.nbooking = alo.nbooking AND afi.nucnfclt = alo.nucnfclt and afi.CDINACTI='A' ");
      from = new StringBuilder(" from TMCT0ALO alo ");
    }
    final StringBuilder whereExterno = new StringBuilder();
    boolean whereEmpty = false;
    boolean whereExternoEmpty = true;

    final String firstRowParamName = "firstRow";
    if (firstRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number>:" + firstRowParamName);
    }

    final String lastRowParamName = "lastRow";
    if (lastRow != null) {
      if (!whereExternoEmpty) {
        whereExterno.append(" and ");
      } else {
        whereExterno.append(" where ");
        whereExternoEmpty = false;
      }
      whereExterno.append("afiInterno.row_number<=:" + lastRowParamName);
    }

    final String fechaDesdeParamName = "feejeliqDesde";
    final String fechaHastaParamName = "feejeliqHasta";

    DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
    String fDesde;
    String fHasta;
    if (mercadoNacInt == 'N') {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fDesde = dFormat.format(fechaDesde);
        // where.append( "alc.feejeliq >=:" + fechaDesdeParamName );
        where.append("alc.feejeliq >=date('" + fDesde + "')");
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fHasta = dFormat.format(fechaHasta);
        // where.append( "alc.feejeliq <=:" + fechaHastaParamName );
        where.append("alc.feejeliq <=date('" + fHasta + "')");
      }
    } else {
      if (fechaDesde != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fDesde = dFormat.format(fechaDesde);
        // where.append( "alo.FEOPERAC >=:" + fechaDesdeParamName );
        where.append("alo.FEOPERAC >=date('" + fDesde + "')");
      }

      if (fechaHasta != null) {
        if (!whereEmpty) {
          where.append(" and ");
        } else {
          where.append(" where ");
          whereEmpty = false;
        }
        fHasta = dFormat.format(fechaHasta);
        // where.append( "alo.FEOPERAC <=:" + fechaHastaParamName );
        where.append("alo.FEOPERAC <=date('" + fHasta + "')");
      }
    }
    final String sentidoParamName = "sentido";
    if (sentido != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("alo.CDTPOPER =:" + sentidoParamName);
    }

    final String nbookingParamName = "nbooking";
    if (nbooking != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbooking.indexOf("#") != -1) {
        nbooking = nbooking.replace("#", "");
        where.append("alo.NBOOKING <>:" + nbookingParamName);
      } else {
        where.append("alo.NBOOKING =:" + nbookingParamName);
      }
    }
    boolean aliasInv = false;
    final String aliasParamName = "alias";
    if (alias != null && alias.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      String cadena = "";
      Integer nCont = 0;
      for (String alia : alias) {
        if (nCont == 0) {
          if (alia.indexOf("#") != -1) {
            alia = alia.replace("#", "");
            aliasInv = true;
          }
          cadena += "'" + alia + "'";
        } else
          cadena += ",'" + alia + "'";
        nCont++;

      }
      if (!aliasInv)
        where.append("ord.CDCUENTA IN (" + cadena + ")");
      else
        where.append("ord.CDCUENTA NOT IN (" + cadena + ")");
      // where.append( "ord.CDCUENTA IN :" + aliasParamName );
    }

    final String isinParamName = "isin";
    if (isin != null && isin.size() > 0) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (isin.get(0).indexOf("#") != -1) {
        isin.set(0, isin.get(0).replace("#", ""));
        where.append("alo.CDISIN NOT IN ( :" + isinParamName + " ) ");
      } else {
        where.append("alo.CDISIN IN ( :" + isinParamName + " ) ");
      }
    }

    final String nudnicifParamName = "nudnicif";
    if (nudnicif != null && nudnicif.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudnicif.get(0).indexOf("#") != -1) {
        nudnicif.set(0, nudnicif.get(0).replace("#", ""));
        where.append(" ( afi.nudnicif NOT IN ( :" + nudnicifParamName + " ) OR afi.cdddebic  NOT IN ( :"
                     + nudnicifParamName + " ) ) ");
      } else {
        where.append(" ( afi.nudnicif IN ( :" + nudnicifParamName + " ) OR afi.cdddebic  IN ( :" + nudnicifParamName
                     + " ) ) ");
      }
    }

    final String nurefordParamName = "nureford";
    if (nureford != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nureford.indexOf("#") != -1) {
        nureford = nureford.replace("#", "");
        where.append("(ORD.NUREFORD<>:" + nurefordParamName+" OR alo.nucnfclt<>'PARTENON'||:"+nurefordParamName+")");
      } else {
        where.append("(ORD.NUREFORD=:" + nurefordParamName+" OR alo.nucnfclt='PARTENON'||:"+nurefordParamName+")");
      }
    }

    final String mercadoNacIntParamName = "mercadoNacInt";
    if (mercadoNacInt != null) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      where.append("ORD.CDCLSMDO=:" + mercadoNacIntParamName);
    }

    final String nbclientParamName = "nbclient";
    if (nbclient != null && nbclient.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient.get(0).indexOf("#") != -1) {
        nbclient.set(0, nbclient.get(0).replace("#", ""));
        where.append("afi.nbclient NOT IN ( :" + nbclientParamName + ") ");
      } else {
        where.append("afi.nbclient IN ( :" + nbclientParamName + ") ");
      }
    }

    final String nbclient1ParamName = "nbclient1";
    if (nbclient1 != null && nbclient1.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient1.get(0).indexOf("#") != -1) {
        nbclient1.set(0, nbclient1.get(0).replace("#", ""));
        where.append("afi.nbclient1 NOT IN ( :" + nbclient1ParamName + ") ");
      } else {
        where.append("afi.nbclient1 IN ( :" + nbclient1ParamName + ") ");
      }
    }

    final String nbclient2ParamName = "nbclient2";
    if (nbclient2 != null && nbclient2.size() > 0) {

      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbclient2.get(0).indexOf("#") != -1) {
        nbclient2.set(0, nbclient2.get(0).replace("#", ""));
        where.append("afi.nbclient2 NOT IN ( :" + nbclient2ParamName + ")");
      } else {
        where.append("afi.nbclient2 IN ( :" + nbclient2ParamName + ")");
      }
    }

    final String tipodocumentoParamName = "tipodocumento";
    if (tipodocumento != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tipodocumento.equals("-")) {
        where.append("(afi.TPIDENTI = '' OR afi.TPIDENTI IS NULL) ");
      } else {
        where.append("afi.TPIDENTI =:" + tipodocumentoParamName);
      }
    }

    final String tpnactitParamName = "tpnactit";
    if (tpnactit != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpnactit.equals('-')) {
        where.append("(afi.TPNACTIT = '' OR afi.TPNACTIT IS NULL) ");
      } else {
        where.append("afi.TPNACTIT=:" + tpnactitParamName);
      }
    }

    final String paisresParamName = "paisres";
    if (paisres != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (paisres.equals("<null>")) {
        where.append("(afi.CDDEPAIS = '' OR afi.CDDEPAIS IS NULL) ");
      } else {
        where.append("afi.CDDEPAIS =:" + paisresParamName);
      }
    }

    final String provinciaParamName = "provincia";
    if (provincia != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (provincia.equals("<null>")) {
        where.append("(afi.NBPROVIN = '' OR afi.NBPROVIN IS NULL) ");
      } else {
        where.append("afi.NBPROVIN =:" + provinciaParamName);
      }
    }

    final String codpostalParamName = "codpostal";
    if (codpostal != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (codpostal.equals("<null>")) {
        where.append("(afi.CDPOSTAL = '' OR afi.CDPOSTAL IS NULL) ");
      } else {
        where.append("afi.CDPOSTAL =:" + codpostalParamName);
      }
    }

    final String tpdomiciParamName = "tpdomici";
    if (tpdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (tpdomici.equals("<null>")) {
        where.append("(afi.TPDOMICI = '' OR afi.TPDOMICI IS NULL) ");
      } else {
        where.append("afi.TPDOMICI =:" + tpdomiciParamName);
      }
    }

    final String nbdomiciParamName = "nbdomici";
    if (nbdomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nbdomici.equals("<null>")) {
        where.append("(afi.NBDOMICI = '' OR afi.NBDOMICI IS NULL) ");
      } else {
        where.append("afi.NBDOMICI =:" + nbdomiciParamName);
      }
    }

    final String nudomiciParamName = "nudomici";
    if (nudomici != null) {
      if (!whereEmpty) {
        where.append(" and ");
      } else {
        where.append(" where ");
        whereEmpty = false;
      }
      if (nudomici.equals("<null>")) {
        where.append("(afi.NUDOMICI = '' OR afi.NUDOMICI IS NULL) ");
      } else {
        where.append("afi.NUDOMICI =:" + nudomiciParamName);
      }
    }

    if (mercadoNacInt == 'N') {
      from.append(" LEFT JOIN TMCT0ALO alo ON alo.NUORDEN = alc.NUORDEN AND alo.NBOOKING = alc.NBOOKING AND alo.NUCNFCLT = alc.NUCNFCLT");
    }
    from.append(" LEFT JOIN TMCT0ORD ord ON ord.NUORDEN = alo.NUORDEN ");
    from.append(" LEFT JOIN TMCT0ALI ali ON ali.cdaliass = ord.cdcuenta AND ali.FHFINAL = 99991231 ");
    from.append(",TMCT0AFI afi ");

    final Query query = em.createNativeQuery(selectExterno.append(select.append(from.append(where)))
                                                          .append(") afiInterno ").append(whereExterno).toString());

    setParametersWithVectors(query, null, null, nudnicif, nbclient, nbclient1, nbclient2, mercadoNacInt, sentido, isin,
                             null, paisres, provincia, codpostal, tpnactit, null, null, null, null, null, null,
                             nureford, nbooking, fechaDesdeParamName, fechaHastaParamName, nudnicifParamName,
                             nbclientParamName, nbclient1ParamName, nbclient2ParamName, mercadoNacIntParamName,
                             sentidoParamName, isinParamName, aliasParamName, paisresParamName, provinciaParamName,
                             codpostalParamName, tpnactitParamName, null, null, null, null, null, null,
                             nurefordParamName, nbookingParamName, firstRow, firstRowParamName, lastRow,
                             lastRowParamName);
    final List<TitularesDTO> titularesList = new ArrayList<TitularesDTO>();
    try {
      LOG.debug("******************************************************");
      LOG.debug("******************************************************");
      LOG.debug("*************** OBTENIENDO TITULARES ROUTING ***********");
      LOG.debug("******************************************************");
      LOG.debug("{} :: {} query: {}", TAG, "CONSULTA A AFI", selectExterno);

      final List<Object> resultList = query.getResultList();
      if (CollectionUtils.isNotEmpty(resultList)) {
        for (Object obj : resultList) {
          TitularesDTO titular = DTOUtils.convertObjectToTitularesNoIdDTO((Object[]) obj);
          titular.setAfiError(false);
          titularesList.add(titular);
        }
      }
    } catch (Exception e) {
      LOG.debug("[Tmct0afiDaoImpl - findAllDTO] WARNING - Excepción en consulta a afi: " + e.getMessage());
    }
    return titularesList;
  }

  private void setParametersWithVectors(Query query,
                                        Date fechaDesde,
                                        Date fechaHasta,
                                        List<String> nudnicif,
                                        List<String> nbclient,
                                        List<String> nbclient1,
                                        List<String> nbclient2,
                                        Character mercadoNacInt,
                                        Character sentido,
                                        List<String> isin,
                                        List<String> alias,
                                        String paisres,
                                        String provincia,
                                        String codpostal,
                                        Character tpnactit,
                                        String ciudad,
                                        Character tptiprep,
                                        Character tpsocied,
                                        BigDecimal particip,
                                        String ccv,
                                        String nacionalidad,
                                        String nureford,
                                        String nbooking,
                                        String fechaDesdeParamName,
                                        String fechaHastaParamName,
                                        String nudnicifParamName,
                                        String nbclientParamName,
                                        String nbclient1ParamName,
                                        String nbclient2ParamName,
                                        String mercadoNacIntParamName,
                                        String sentidoParamName,
                                        String isinParamName,
                                        String aliasParamName,
                                        String paisresParamName,
                                        String provinciaParamName,
                                        String codpostalParamName,
                                        String tpnactitParamName,
                                        String ciudadParamName,
                                        String tptiprepParamName,
                                        String tpsociedParamName,
                                        String participParamName,
                                        String ccvParamName,
                                        String nacionalidadParamName,
                                        String nurefordParamName,
                                        String nbookingParamName,
                                        Integer firstRow,
                                        String firstRowParamName,
                                        Integer lastRow,
                                        String lastRowParamName) {

    if (firstRow != null) {
      query.setParameter(firstRowParamName, firstRow);
    }
    if (lastRow != null) {
      query.setParameter(lastRowParamName, lastRow);
    }
    if (fechaDesde != null) {
      query.setParameter(fechaDesdeParamName, fechaDesde, TemporalType.DATE);
    }
    if (fechaHasta != null) {
      query.setParameter(fechaHastaParamName, fechaHasta, TemporalType.DATE);
    }
    if (nudnicif != null && nudnicif.size() > 0) {
      query.setParameter(nudnicifParamName, nudnicif);
    }
    if (nbclient != null && nbclient.size() > 0) {
      query.setParameter(nbclientParamName, nbclient);
    }
    if (nbclient1 != null && nbclient1.size() > 0) {
      query.setParameter(nbclient1ParamName, nbclient1);
    }
    if (nbclient2 != null && nbclient2.size() > 0) {
      query.setParameter(nbclient2ParamName, nbclient2);
    }
    if (mercadoNacInt != null) {
      query.setParameter(mercadoNacIntParamName, mercadoNacInt);
    }
    if (sentido != null) {
      query.setParameter(sentidoParamName, sentido);
    }
    if (nbooking != null) {
      query.setParameter(nbookingParamName, nbooking);
    }
    if (isin != null && isin.size() > 0) {
      query.setParameter(isinParamName, isin);
    }
    if (alias != null && alias.size() > 0) {
      query.setParameter(aliasParamName, alias);
    }
    if (paisres != null && !paisres.equals("<null>")) {
      query.setParameter(paisresParamName, paisres);
    }
    if (provincia != null && !provincia.equals("<null>")) {
      query.setParameter(provinciaParamName, provincia);
    }
    if (codpostal != null && !codpostal.equals("<null>")) {
      query.setParameter(codpostalParamName, codpostal);
    }
    if (tpnactit != null && !tpnactit.equals('-')) {
      query.setParameter(tpnactitParamName, tpnactit);
    }
    if (ciudad != null && !ciudad.equals("<null>")) {
      query.setParameter(ciudadParamName, ciudad);
    }
    if (tptiprep != null && !tptiprep.equals('-')) {
      query.setParameter(tptiprepParamName, tptiprep);
    }
    if (tpsocied != null && !tpsocied.equals('-')) {
      query.setParameter(tpsociedParamName, tpsocied);
    }
    if (particip != null && !particip.equals("<null>")) {
      query.setParameter(participParamName, particip);
    }
    if (ccv != null && !ccv.equals("<null>")) {
      query.setParameter(ccvParamName, ccv);
    }
    if (nacionalidad != null && !nacionalidad.equals("<null>")) {
      query.setParameter(nacionalidadParamName, nacionalidad);
    }
    if (nureford != null && !nureford.equals("<null>")) {
      query.setParameter(nurefordParamName, nureford);
    }
  }

}
