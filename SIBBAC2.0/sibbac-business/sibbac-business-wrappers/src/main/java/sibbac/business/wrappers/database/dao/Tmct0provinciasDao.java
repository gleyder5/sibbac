package sibbac.business.wrappers.database.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0provincias;

@Repository
public interface Tmct0provinciasDao extends JpaRepository<Tmct0provincias, String> {

  @Query("SELECT o FROM Tmct0provincias o WHERE o.cdcodigo = :pcdcodigo ")
  public Tmct0provincias findBycdcodigo( @Param("pcdcodigo")String cdcodigo);

  @Query("SELECT o FROM Tmct0provincias o WHERE o.cdestado = 'A' order by o.nbprovin")
  public List<Tmct0provincias> findAllActivas();

  @Query("SELECT o.cdcodigo, o.nbprovin FROM Tmct0provincias o WHERE o.cdestado = 'A'")
  public List<Object[]> findAllCodigosAndNombresActivos();

} // Tmct0proviciasDao
