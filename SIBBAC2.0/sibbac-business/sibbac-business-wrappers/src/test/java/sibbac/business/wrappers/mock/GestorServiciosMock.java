package sibbac.business.wrappers.mock;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.tasks.database.JobPK;

@Component
public class GestorServiciosMock implements GestorServicios {

    @Override
    public List<Alias> isActive(JobPK jobPk) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Alias> isActive(JobPK jobPk, Date time) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Contacto> getContactos(Alias alias, JobPK jobPk) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Contacto> getContactos(Alias alias, JobPK jobPk, Date time) {
        // TODO Auto-generated method stub
        return null;
    }

}
