package sibbac.business.minijobs.tasks;

import org.junit.Test;
import org.junit.Before;

import sibbac.common.utils.CompressFileUtil;

public class TestTaskFileBackup {
    
    private final CompressFileUtil compressFileUtil;
    
    private TaskFileBackup taskFileBackup;

    public TestTaskFileBackup() {
        compressFileUtil = new CompressFileUtil();
    }
    
    @Before
    public void before() {
        taskFileBackup = new TaskFileBackup();
        taskFileBackup.compressFileUtilInstance = compressFileUtil;
    }
    
    @Test
    public void testExecuteTask() throws Exception{
        taskFileBackup.executeTask();
    }
}
