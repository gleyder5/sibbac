package sibbac.business.minijobs.tasks;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
public class TestFicherosPti {
	
	private static final Logger LOG = LoggerFactory.getLogger(TestFicherosPti.class);

	@Autowired( required = false )
	PTIFileProcess		ptiFileProcess;

//	@Autowired
//	OperacionFallidoBo	operacionFallidoBo;

	@Ignore
	@Test
	public void testPtiFilesProcessAnFallidos() {
		assertNotNull( ptiFileProcess );

		try {
			ptiFileProcess.process();
		} catch ( Exception e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
			fail();
		}
	}

	// @Test
	// @Ignore
	// public void testJobPti(){
	// job.execute();
	// }

}
