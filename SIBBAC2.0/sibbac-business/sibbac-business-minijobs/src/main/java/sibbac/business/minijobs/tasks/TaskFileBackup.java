package sibbac.business.minijobs.tasks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.quartz.DateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.common.utils.CompressFileUtil;
import sibbac.common.utils.ConfigUtil;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_MINIJOBS.NAME, name = Task.GROUP_MINIJOBS.JOB_FILES_CUSTOMIZE, interval = 2, delay = 1, 
intervalUnit = DateBuilder.IntervalUnit.MONTH)
public class TaskFileBackup extends MinijobsTaskConcurrencyPrevent {
    
    private static final Logger LOG = LoggerFactory.getLogger(TaskFileBackup.class);
  
    private static final String CREAR_BACKUP_DE_FICHEROS_POR_MESES = "crearBackupDeFicherosPorMeses";
    
    private static final String TAG = TaskFileBackup.class.getName();

    private final ConfigUtil configUtil;

    @Autowired
    CompressFileUtil compressFileUtilInstance;
    
    public TaskFileBackup() {
        configUtil = ConfigUtil.getInstance();
    }

    /**
     * Comprime ficheros que llevan más de dos meses bajo la raiz
     * smbrv/ficheros, los pasa a backup y los borra
     */
    @Override
    public synchronized void executeTask() throws Exception {
        try {
            crearBackupDeFicherosPorMeses();
            groupBackupFilesByYear();
            LOG.info("[executeTask] Compresión finalizada");
        }
        catch (IOException ioex) {
          LOG.error("Error de entrada/salida en tarea de compresión", ioex);
          throw ioex;
        }
        catch (RuntimeException ex) {
            LOG.error("[executeTask] Error inesperado al comprimir ficheros", ex);
            throw ex;
        }
    }

    /**
     * Comprime los ficheros zip que están en la carpeta backup en un solo
     * fichero zip bajo un directorio cuyo nombre es el del año anterior al que
     * se ha entrado, posteriormente, elimina los zips sueltos
     * @throws IOException 
     */
    private void groupBackupFilesByYear() throws IOException {
        final int lastYear = configUtil.getConfig().getInt("filesystem.backup.year");
        final int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        // Si hace 2 años desde el último barrido de bakups, se hace un nuevo barrido
        if (lastYear + 1 < currentYear) {
            List<String> listToZip = new ArrayList<>();
            // Crea el backup de ficheros
            boolean result = compressFilesByYear(lastYear, listToZip);
            if (result) {
                removeFiles(listToZip);
                // Actualizar ultimo año comprimido en el fichero de configuracion
                configUtil.getConfig().setProperty("filesystem.backup.year", currentYear);
                configUtil.saveConfig();
            }
            else {
              LOG.info("[groupBackupFilesByYear] No se ha comprimido el agno {}", lastYear);
            }
        }
    }

    /**
     * agrupa los ficheros comprimidos en la carpeta backup en un solo fichero
     * zip y lo mete en la carpeta cuyo nombre es el año anterior al que se
     * acaba de pasar.
     * @param lastYear
     * @param listToZip
     * @return
     */
    private boolean compressFilesByYear(int lastYear, List<String> listToZip) throws IOException {
        // Directorio raiz donde se va a crear el zip 
        String dirToZip = configUtil.getConfig().getString("filesystem.backup.path");
        String newFileZip = new StringBuilder(dirToZip).append(File.separator)
                .append(lastYear).append(File.separator).append("year_").append(lastYear).append(".zip").toString();
        File dirToSave = new File(newFileZip).getParentFile();
        if (!dirToSave.exists()) {
            if(!dirToSave.mkdirs()) {
                LOG.warn("[compressFilesByYear] No se ha podido crear el path {}", dirToSave.getAbsolutePath());
                return false;
            }
        }
        File[] filesToGroup = new File(dirToZip).listFiles();
        // Obtiene la lista de ficheros a comprimir
        addFilesToCompressByYear(filesToGroup, listToZip, String.valueOf(lastYear));
        // Crea o añade al fichero zip la lista de ficheros 
        compressFileUtilInstance.compressDirectory(listToZip, newFileZip);
        return true;
    }

    /**
     * Añade los ficheros que no sean directorios y cuyo año corresponda al que
     * se quiere agrupar a la lista de ficheros a zipear
     * 
     * @param filesToGroup
     * @param listToZip
     */
    private void addFilesToCompressByYear(File[] filesToGroup, List<String> listToZip, String year) {
        final String yearZip;
        
        yearZip = year.concat(".zip");
        for (File fileEntry : filesToGroup) {
            if (!fileEntry.isDirectory() && fileEntry.getName().endsWith(yearZip)) {
                listToZip.add(fileEntry.getAbsolutePath());
            }
        }
    }

    private void removeFiles(List<String> listToZip) {
        File toRemove;
        
        try {
            for (String toDelete : listToZip) {
                toRemove = new File(toDelete);
                if(!toRemove.delete())
                    LOG.warn("[removeFiles] No se ha eliminado el fichero {}", toDelete);
            }
        }
        catch(SecurityException scex) {
            LOG.error("[removeFiles] Error de seguridad al intentar borrar fichero", scex);
        }
    }

    /**
     * se encarga de comprimir ficheros que llevan más de 2 (configurable) meses
     * en el directorio de ficheros y pasarlo a la carpeta de backup
     * (configurable en sibbac.filesystem.properties)
     */
    private void crearBackupDeFicherosPorMeses() throws IOException {
        final PropertiesConfiguration config;
        final String pathToSearch, pathToSave;
        final File backup;
        final String[] pathsToExclude;
        final int monthsAmoung;
        final List<String> toDeleteList;
        
        // Variables necesarias para llevar a cabo el backup 
        config = configUtil.getConfig();
        pathToSearch = Objects.requireNonNull(config.getString("filesystem.ficheros.path"));
        pathToSave = Objects.requireNonNull(config.getString("filesystem.backup.path"));
        pathsToExclude = Objects.requireNonNull(config.getStringArray("filesystem.backup.exclude.path"));
        monthsAmoung = configUtil.getConfig().getInt("filesystem.months.amoung.backup");
        // Comprobar si existe el directorio de backup, si no, se crea
        backup = new File(pathToSave);
        if (!backup.exists()) {
            backup.mkdirs();
        }
        LOG.debug(TAG + "::" + CREAR_BACKUP_DE_FICHEROS_POR_MESES
                + " Se va a crear el backup de ficheros en la carpeta: " + backup.getAbsolutePath());
        toDeleteList = compressFileUtilInstance.backupAllFilesGivenMonthsAmoungBeforeIn(pathToSearch, pathToSave,
                pathsToExclude, monthsAmoung);
        // Se eliminan todos los ficheros una vez que se han comprimidos
        if (CollectionUtils.isNotEmpty(toDeleteList)) {
            removeFiles(toDeleteList);
        }
        else 
            LOG.info("[crearBackupDeFicherosPorMeses] No se encontraron ficheros para respaldar.");
    }

    @Override
    public TIPO_APUNTE determinarTipoApunte() {
        return TIPO_APUNTE.MINIJOBS_TASK_FILE_BACKUP;
    }

}