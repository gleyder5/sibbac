package sibbac.business.minijobs.tasks;


import java.io.IOException;


public interface PTIFileProcess {

	public void checkDirStructure() throws IOException, Exception;

	public void unzipPtiFiles() throws IOException;

	public void process() throws IOException, Exception;

//	public Path getIfTemporalFile();
}
