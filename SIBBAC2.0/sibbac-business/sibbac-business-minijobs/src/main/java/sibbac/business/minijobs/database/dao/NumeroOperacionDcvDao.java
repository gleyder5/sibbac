package sibbac.business.minijobs.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.minijobs.database.model.NumeroOperacionDcv;


@Component
public interface NumeroOperacionDcvDao extends JpaRepository< NumeroOperacionDcv, Long > {

}
