package sibbac.business.minijobs.tasks;

import org.quartz.DateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

@SIBBACJob(group = Task.GROUP_FALLIDOS.NAME,
           name = Task.GROUP_FALLIDOS.JOB_PTIFILES,
           interval = 1,
           delay = 1,
           intervalUnit = DateBuilder.IntervalUnit.HOUR)
public class PTIFilesJob extends MinijobsTaskConcurrencyPrevent {

  private static final Logger LOG = LoggerFactory.getLogger(PTIFilesJob.class);

  @Autowired
  PTIFileProcess ptiFileProcess;
  
  @Override
  public void executeTask() throws Exception {
    LOG.info("[executeTask] Inicio...");
    // procesamos ficheros
    try {
      ptiFileProcess.process();
    }
    catch(Exception ex) {
      LOG.error("[executeTask] Error al procesar Fallidos Pti", ex);
      throw ex;
    }
    finally {
      LOG.info("[executeTask] Fin");
    }
  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.MINIJOBS_PTI_FILES;
  }

}
