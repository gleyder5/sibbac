package sibbac.business.minijobs.tasks;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.quartz.DateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.bo.Tmct0estadoBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.ASIGNACIONES;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.fase0.database.model.Tmct0estado;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.dao.Tmct0aloDao;
import sibbac.business.wrappers.database.dao.Tmct0bokDao;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Proceso que cambia el estado de asignación de los <code>Tmct0alc</code> bajo
 * distintas condiciones.
 * 
 * @author XI316153
 */
@SIBBACJob(group = Task.GROUP_MINIJOBS.NAME, name = Task.GROUP_MINIJOBS.JOB_LIQ_STATE_CHANGE, interval = 1, delay = 1, intervalUnit = DateBuilder.IntervalUnit.DAY, startTime = "23:00:00")
public class LiqStateChange extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(LiqStateChange.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0bok</code>. */
  @Autowired
  private Tmct0bokDao tmct0bokDao;

  /** Business object para la tabla <code>Tmct0alo</code>. */
  @Autowired
  private Tmct0aloDao tmct0aloDao;

  /** Business object para la tabla <code>Tmct0alc</code>. */
  @Autowired
  private Tmct0alcDao tmct0alcDao;

  /** Business object para la tabla <code>Tmct0alc</code>. */
  @Autowired
  private Tmct0alcBo tmct0alcBo;

  /** Business object para la tabla <code>Tmct0cfg</code>. */
  @Autowired
  private Tmct0cfgDao tmct0cfgDao;

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  /** Business object para la tabla <code>Tmct0log</code>. */
  @Autowired
  private Tmct0logBo tmct0logBo;

  /** Business object para la tabla <code>Tmct0cfg</code>. */
  @Autowired
  private Tmct0estadoBo tmct0estadoBo;

  private Date hoy = Calendar.getInstance().getTime();
  private List<Date> holidaysConfigList = null;
  private List<Integer> workDaysOfWeekConfigList = null;

  private Tmct0estado estadoAsigLiquidadaDefinitiva;
  private Tmct0estado estadoAsigLiquidadaTeorica;
  private Tmct0estado estadoAsigEnCurso;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES

  /**
   * Constructor I.
   */
  public LiqStateChange() {
  } // LiqStateChange

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void execute() {
    // Se marca el momento de inicio de la tarea
    final Long startTime = System.currentTimeMillis();

    try {
      LOG.debug("[LiqStateChange :: execute] Iniciando tarea de cambio de estados ...");

      // Se bloquea el semaforo
      Tmct0men tmct0men = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_MINIJOBS_LIQ_STATE_CHANGE, TMCT0MSC.LISTO_GENERAR,
          TMCT0MSC.EN_EJECUCION);

      LOG.debug("[LiqStateChange :: execute] Semáforo bloqueado ...");

      estadoAsigLiquidadaDefinitiva = tmct0estadoBo.findById(ASIGNACIONES.LIQUIDADA_DEFINITIVA.getId());
      estadoAsigLiquidadaTeorica = tmct0estadoBo.findById(ASIGNACIONES.LIQUIDADA_TEORICA.getId());
      estadoAsigEnCurso = tmct0estadoBo.findById(ASIGNACIONES.EN_CURSO.getId());
      ;

      Boolean error = false;

      try {
        // Se cambia el estado de todos los ALCS cuya fecha de contratación sea
        // la de hoy menos 8 días laborables no
        // festivos y su estado de de asignación sea mayor o igual que
        // ASIGNACIONES.LIQUIDADA_TEORICA y menor que
        // ASIGNACIONES.LIQUIDADA_DEFINITIVA

        Date dMenosOcho = DateHelper.getPreviousWorkDate(hoy, new Integer(8), getWorkDaysOfWeek(), getHolidays());
        LOG.debug("[LiqStateChange :: execute] dMenosOcho: " + dMenosOcho);
        ;

        tmct0alcBo.changeAsigStateDMenosOcho(dMenosOcho, hoy, estadoAsigLiquidadaDefinitiva, estadoAsigEnCurso);

      }
      catch (Exception e) {
        LOG.error("[LiqStateChange :: execute] Error realizando el cambio de estados dMenosOcho ...", e);
        error = true;
      } // catch

      try {
        // Se cambia el estado de todos los desgloses cámara cuya fecha de
        // contratación sea la de hoy menos 3 días
        // laborables no festivos y cuya cuenta de compensación esté vacía o sea
        // nula al estado
        // ASIGNACIONES.LIQUIDADA_TEORICA.

        Date dMenosTres = DateHelper.getPreviousWorkDate(hoy, new Integer(3), getWorkDaysOfWeek(), getHolidays());
        LOG.debug("[LiqStateChange :: execute] dMenosTres: " + dMenosTres);

        // alcList.clear();
        tmct0alcBo.changeAsigStateDMenosTres(dMenosTres, hoy, estadoAsigLiquidadaTeorica, estadoAsigEnCurso);

      }
      catch (Exception e) {
        LOG.error("[LiqStateChange :: execute] Error realizando el cambio de estados dMenosTres ...", e);
        error = true;
      } // catch

      if (error) {
        LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.EN_ERROR");
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      }
      else {
        LOG.info("[TaskEnvioFicheroMegara :: execute] Estableciendo estado TMCT0MSC.LISTO_GENERAR");
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.LISTO_GENERAR);
      } // else

    }
    catch (Exception e) {
      LOG.warn("[LiqStateChange :: execute] Error intentando bloquear el semaforo ... " + e.getMessage());
    }
    finally {
      final Long endTime = System.currentTimeMillis();
      LOG.debug("[LiqStateChange :: execute] Finalizada la tarea de cambio de estados ..."
          + String.format(
              " [ %d min y %d sec ]",
              TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
              TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                  - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))));
    } // finally
  } // execute

  // /**
  // * Cambia el estado de asignación de todos los desgloses camara recibidos
  // cuya cuenta de compensación esté vacía o
  // sea
  // * nula al estado ASIGNACIONES.LIQUIDADA_TEORICA.
  // *
  // * @param alcList Lista de <code>Tmct0alc</code> a cambiar el estado.
  // */
  // @Transactional
  // private void changeAsigStateDMenosTres(List<Tmct0alc> alcList) {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Inicio ...");
  //
  // Boolean cambio = false;
  //
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Cambiando estado de "
  // + alcList.size() + " alcs ...");
  // for (Tmct0alc tmct0alc : alcList) {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Procesando alc: "
  // + tmct0alc.getId().toString());
  //
  // for (Tmct0desglosecamara tmct0desglosecamara :
  // tmct0alc.getTmct0desglosecamaras()) {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Procesando dc: "
  // + tmct0desglosecamara.getIddesglosecamara());
  // if (tmct0desglosecamara.getTmct0infocompensacion().getCdctacompensacion()
  // == null
  // ||
  // tmct0desglosecamara.getTmct0infocompensacion().getCdctacompensacion().trim().isEmpty())
  // {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] CDCTACOMPENSACION null o vacio");
  // tmct0desglosecamara.setTmct0estadoByCdestadoasig(estadoAsigLiquidadaTeorica);
  // tmct0desglosecamara.setAuditFechaCambio(hoy);
  // cambio = true;
  // } else {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] CDCTACOMPENSACION no vacio: "
  // + tmct0desglosecamara.getTmct0infocompensacion().getCdctacompensacion());
  // } // else
  // } // for (Tmct0desglosecamara tmct0desglosecamara :
  // tmct0alc.getTmct0desglosecamaras())
  //
  // if (cambio) {
  // tmct0alc.setCdestadoasig(ASIGNACIONES.EN_CURSO.getId());
  // tmct0alc.setFhaudit(hoy);
  // // Se salvan también los desglosescamara por el Cascade.Persist
  // tmct0alcDao.save(tmct0alc);
  //
  // Tmct0alo alo = tmct0alc.getTmct0alo();
  // alo.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
  // alo.setFhaudit(hoy);
  // tmct0aloDao.save(alo);
  //
  // Tmct0bok bok = alo.getTmct0bok();
  // bok.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
  // bok.setFhaudit(hoy);
  // tmct0bokDao.save(bok);
  //
  // try {
  // tmct0logBo.insertarRegistro(tmct0alc,
  // hoy,
  // hoy,
  // null,
  // ASIGNACIONES.LIQUIDADA_TEORICA.getId(),
  // "LiqStateChange. ASIGNACIONES.LIQUIDADA_TEORICA");
  // } catch (Exception ex) {
  // LOG.warn("[LiqStateChange :: changeAsigStateDMenosTres] Error cambiando estado dMenosTres en Tmct0log ... ",
  // ex.getMessage());
  // } // catch
  // } // if (cambio)
  //
  // cambio = false; // Se resetea el estado de la variable
  // } // for (Tmct0alc tmct0alc : alcList)
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosTres] Fin ...");
  // } // changeAsigStateDMenosTres
  //
  // /**
  // * Cambia el estado de asignación de todos los ALCS recibidos al estado
  // ASIGNACIONES.LIQUIDADA_DEFINITIVA.
  // *
  // * @param alcList Lista de <code>Tmct0alc</code> a cambiar el estado.
  // */
  // @Transactional
  // private void changeAsigStateDMenosOcho(List<Tmct0alc> alcList) {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Inicio ...");
  //
  // Boolean cambio = false;
  //
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Cambiando estado de "
  // + alcList.size() + " alcs ...");
  // for (Tmct0alc tmct0alc : alcList) {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Procesando alc: "
  // + tmct0alc.getId().toString());
  //
  // for (Tmct0desglosecamara tmct0desglosecamara :
  // tmct0alc.getTmct0desglosecamaras()) {
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Procesando dc: "
  // + tmct0desglosecamara.getIddesglosecamara());
  // tmct0desglosecamara.setTmct0estadoByCdestadoasig(estadoAsigLiquidadaDefinitiva);
  // tmct0desglosecamara.setAuditFechaCambio(hoy);
  // cambio = true;
  // } // for
  //
  // if (cambio) {
  // tmct0alc.setCdestadoasig(ASIGNACIONES.EN_CURSO.getId());
  // tmct0alc.setFhaudit(hoy);
  // // Se salvan también los desglosescamara por el Cascade.Persist
  // tmct0alcDao.save(tmct0alc);
  //
  // Tmct0alo alo = tmct0alc.getTmct0alo();
  // alo.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
  // alo.setFhaudit(hoy);
  // tmct0aloDao.save(alo);
  //
  // Tmct0bok bok = alo.getTmct0bok();
  // bok.setTmct0estadoByCdestadoasig(estadoAsigEnCurso);
  // bok.setFhaudit(hoy);
  // tmct0bokDao.save(bok);
  //
  // try {
  // tmct0logBo.insertarRegistro(tmct0alc,
  // hoy,
  // hoy,
  // null,
  // ASIGNACIONES.LIQUIDADA_DEFINITIVA.getId(),
  // "LiqStateChange. ASIGNACIONES.LIQUIDADA_DEFINITIVA");
  // } catch (Exception ex) {
  // LOG.warn("[LiqStateChange :: changeAsigStateDMenosOcho] Error cambiando estado dMenosOcho en Tmct0log ... ",
  // ex.getMessage());
  // } // catch
  // } // if (cambio)
  //
  // cambio = false; // Se resetea el estado de la variable
  // } // for (Tmct0alc tmct0alc : alcList)
  // LOG.debug("[LiqStateChange :: changeAsigStateDMenosOcho] Fin ...");
  // } // changeAsigStateDMenosOcho

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS DE UTILIDAD

  /**
   * Obtiene una lista con los días de vacaciones configurados en la tabla
   * tmct0cfg.
   * 
   * @return <code>List<Date> - </code>Lista de días de vacaciones.
   */
  private List<Date> getHolidays() {
    if (holidaysConfigList == null) {
      // Días de vacaciones configurados
      List<Tmct0cfg> configList = tmct0cfgDao.findByAplicationAndProcessAndLikeKeyname("SBRMV", "CONFIG",
          "holydays.anual.days%");
      if (configList != null && !configList.isEmpty()) {
        holidaysConfigList = new ArrayList<Date>(configList.size());

        for (Tmct0cfg tmct0cfg : configList) {
          holidaysConfigList.add(FormatDataUtils.convertStringToDate(tmct0cfg.getKeyvalue(), "yyyy-MM-dd"));
        } // for
      }
      else {
        holidaysConfigList = new ArrayList<Date>();
      } // else
    } // if

    return holidaysConfigList;
  } // getHolidays

  /**
   * Obtiene una lista con los días de laborables configurados en la tabla
   * tmct0cfg.
   * 
   * @return <code>List<Integer> - </code>Lista de días de la semana laborables.
   */
  private List<Integer> getWorkDaysOfWeek() {
    if (workDaysOfWeekConfigList == null) {
      // Días de la semana laborables configurados
      Tmct0cfg tmct0cfg = tmct0cfgDao.findByAplicationAndProcessAndKeyname("SBRMV", "CONFIG", "week.work.days");

      if (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && !tmct0cfg.getKeyvalue().isEmpty()) {
        String[] workDays = tmct0cfg.getKeyvalue().split(",");
        workDaysOfWeekConfigList = new ArrayList<Integer>(workDays.length);

        for (String workDay : workDays) {
          workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
        } // for
      }
      else {
        workDaysOfWeekConfigList = new ArrayList<Integer>();
      } // else
    } // if

    return workDaysOfWeekConfigList;
  } // getWorkDaysOfWeek

} // LiqStateChange
