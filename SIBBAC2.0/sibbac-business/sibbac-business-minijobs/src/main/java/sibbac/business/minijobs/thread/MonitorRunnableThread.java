package sibbac.business.minijobs.thread;

public interface MonitorRunnableThread<T> {
    void shutdownRunnable(T current);

    void logError(String message, Throwable th);
}
