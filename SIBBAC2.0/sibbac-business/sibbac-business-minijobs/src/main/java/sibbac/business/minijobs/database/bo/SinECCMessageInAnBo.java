package sibbac.business.minijobs.database.bo;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.minijobs.database.dao.NumeroOperacionDcvDao;
import sibbac.business.minijobs.database.model.NumeroOperacionDcv;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R01;
import sibbac.pti.messages.an.R02;
import sibbac.pti.messages.an.R03;
import sibbac.pti.messages.an.R05;

@Service
public class SinECCMessageInAnBo extends AbstractBo<NumeroOperacionDcv, Long, NumeroOperacionDcvDao> {

  private String prefix = "[Sin Ecc AN ]";

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void saveNumerosOperacionDcvsNewTransaction(List<AN> ans) {
    for (AN an : ans) {
      saveNumeroOperacionDcv(an);
    }
  }

  @Transactional
  public void saveNumeroOperacionDcv(AN an) {
    R01 r01 = null;
    R02 r02 = null;
    R03 r03 = null;
    R05 r05 = null;
    if (an.hasRecords(PTIMessageRecordType.R01)) {
      r01 = (R01) an.getAll(PTIMessageRecordType.R01).get(0);
    }
    if (an.hasRecords(PTIMessageRecordType.R02)) {
      r02 = (R02) an.getAll(PTIMessageRecordType.R02).get(0);
    }
    if (an.hasRecords(PTIMessageRecordType.R03)) {
      r03 = (R03) an.getAll(PTIMessageRecordType.R03).get(0);
    }
    if (an.hasRecords(PTIMessageRecordType.R05)) {
      r05 = (R05) an.getAll(PTIMessageRecordType.R05).get(0);
    }
    String cdoperacionmktSinEcc = cfgBo.getTpopebolsSinEcc();
    if (r02 != null && r03 != null) {
      if (cdoperacionmktSinEcc != null && r02.getCodigoDeOperacionMercado() != null
          && cdoperacionmktSinEcc.contains(StringUtils.trim(r02.getCodigoDeOperacionMercado()))) {

        Time hordenmkg = new Time(r02.getHoraDeLaOrdenDeMercado().getTime());
        Time hoNegociacion = new Time(r02.getHoraNegociacion().getTime());
        String sentido = r02.getSentido();
        NumeroOperacionDcv nDcv = new NumeroOperacionDcv();
        nDcv.setAudit_fecha_alta(new Timestamp(System.currentTimeMillis()));
        nDcv.setAudit_fecha_cambio(new Timestamp(System.currentTimeMillis()));
        nDcv.setAudit_user_alta("SINECC");
        nDcv.setAudit_user_cambio("SINECC");
        nDcv.setCdindcotizacion(r02.getIndicadorCotizacion());
        nDcv.setCdisin(r02.getCodigoDeValor());
        nDcv.setCdmiembromkt(r02.getMiembroMercado());
        nDcv.setCdoperacionmkt(r02.getCodigoDeOperacionMercado());
        nDcv.setCdPlataformaNegociacion(r02.getIdentificacionDeLaPlataforma());
        nDcv.setCdsegmento(r02.getSegmento());
        nDcv.setCdsentido("1".equalsIgnoreCase(sentido.trim()) ? "C" : "V");
        nDcv.setCodigoDcv(r03.getCodigoDCV());
        if (r05 != null) {
          nDcv.setFliqteorica(r05.getFechaLiquidacionTeorica());
        } else if (r01 != null) {
          nDcv.setFliqteorica(r01.getFechaLiquidacionTeorica());
        }
        nDcv.setFnegociacion(r02.getFechaNegociacion());
        nDcv.setFordenmkt(r02.getFechaDeLaOrdenDeMercado());
        nDcv.setHnegociacion(hoNegociacion);
        nDcv.setHordenmkt(hordenmkg);
        nDcv.setImefectivo(r02.getImporteEfectivo());
        nDcv.setImprecio(r02.getPrecio());
        nDcv.setImtitulos(r02.getValoresImporteNominal());
        nDcv.setNuexemkt(r02.getNumeroDeEjecucionDeMercado() + "");
        nDcv.setNuordenmkt(r02.getNumeroDeLaOrdenDeMercado() + "");
        nDcv.setNumeroOperacionDCV(r03.getNumeroOperacionDCV());
        LOG.debug("[{}] Encontrado AN en fichero ## DCV : {} ## IBO : {} ## miembro : {} ## nuordenmkt : {} ## nuexemkt : {} ## ISIN : {}\n"
                      + "## sentido : {} ## segmento : {} ## cdoperacionmkt : {} ## fnegociacion : {} ## titulos : {} ## precio : {} ",
                  prefix, nDcv.getCodigoDcv(), nDcv.getNumeroOperacionDCV(), nDcv.getCdmiembromkt(),
                  nDcv.getNuordenmkt(), nDcv.getNuexemkt(), nDcv.getCdisin(), nDcv.getCdsentido(),
                  nDcv.getCdsegmento(), nDcv.getCdoperacionmkt(), nDcv.getFnegociacion(), nDcv.getImtitulos(),
                  nDcv.getImprecio());
        this.dao.save(nDcv);
      }
    }
  }
}
