package sibbac.business.minijobs.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.comunicaciones.pti.commons.PtiCommomns;
import sibbac.business.fallidos.utils.FileUtils;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.minijobs.database.bo.SinECCMessageInAnBo;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.pti.PTIException;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageVersion;
import sibbac.pti.messages.Header;
import sibbac.pti.messages.an.AN;

@Component
public class PTIFileProcessImpl implements PTIFileProcess {
  
  private static final Logger LOG = LoggerFactory.getLogger(PTIFileProcessImpl.class);

  private static final int REINTENTOS = 15;

  @Value("${sibbac.files.pti.root.dir}")
  private String rootDir;

  @Value("${sibbac.files.pti.temporal.dir}")
  private String tempDir;

  @Value("${sibbac.files.pti.failed.dir}")
  private String errorDir;

  @Value("${sibbac.files.pti.processed.dir}")
  private String processedDir;

  @Value("${sibbac.files.pti.cnmv.dir}")
  private String cnmvDir;

  @Value("${sibbac.files.pti.canones.dir}")
  private String canonesDir;

  @Value("${sibbac.files.pti.otros.dir}")
  private String otrosDir;

  @Value("${sibbac.files.pti.informes.dir:informes}")
  private String informesDir;

  @Value("${sibbac.files.pti.corretajes.dir:corretajes}")
  private String corretajesDir;

  @Value("${sibbac.files.pti.pattern}")
  private String ptiFilePattern;

  @Value("${sibbac.files.pti.patternTRCNMV}")
  private String ptiFilePatternTRCNMV;

  @Value("${sibbac.files.pti.patternTRXML}")
  private String ptiFilePatternTRXML;

  @Value("${sibbac.files.pti.patternV025_CM}")
  private String ptiFilePatternV025_CM;

  @Value("${sibbac.files.pti.patternANF6OP}")
  private String ptiFilePatternANF6OP;

  @Value("${sibbac.files.pti.patternANFDEX}")
  private String ptiFilePatternANFDEX;

  @Value("${sibbac.files.pti.patternANMSOP}")
  private String ptiFilePatternANMSOP;

  @Value("${sibbac.files.pti.patternTIF6}")
  private String ptiFilePatternTIF6;

  @Value("${sibbac.files.pti.patternTIFD}")
  private String ptiFilePatternTIFD;

  @Value("${sibbac.files.pti.extension}")
  private String ptiFileExtension;

  @Value("${sibbac.files.pti.zip.extension}")
  private String ptiZipExtension;

  @Value("${sibbac.files.pti.patterncnmv}")
  private String ptiFilePatternCNMV;

  @Value("${sibbac.files.pti.patterncanones}")
  private String ptiFilePatternCanones;

  @Value("${sibbac.files.pti.patternotros}")
  private String ptiFilePatternOtros;

  @Value("${sibbac.files.pti.pattern.informes:*_INF*}")
  private String ptiFilePatternInformes;

  @Value("${sibbac.files.pti.pattern.corretaje:*_IC*}")
  private String ptiFilePatternCorretajes;

  private Path ptiRootFilesPath;

  private Path ptiTemporalFilesPath;

  private Path ptiErrorFilesPath;

  private Path ptiProcessedFilesPath;

  private Path pticnmvFilesPath;

  private Path ptiCanonesFilesPath;

  private Path ptiOtrosFilesPath;

  private Path ptiInformesFilesPath;

  private Path ptiCorretajesFilesPath;

  // Intermediarios Financieros.
  @Value("${sibbac.files.pti.if.prefix}")
  private String ifPrefix;

  @Value("${sibbac.files.pti.if.suffix}")
  private String ifSuffix;

  @Autowired
  private PtiCommomns ptiCommons;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Autowired
  private SinECCMessageInAnBo sinEccMsgInAnBo;

  @Override
  public void checkDirStructure() throws IOException, Exception {

    if (StringUtils.isEmpty(ptiFilePattern) || StringUtils.isEmpty(ptiFileExtension)
        || StringUtils.isEmpty(ptiZipExtension)) {
      throw new Exception("Empty properties");
    }

    ptiRootFilesPath = Paths.get(rootDir);
    ptiTemporalFilesPath = Paths.get(rootDir, tempDir);
    ptiErrorFilesPath = Paths.get(rootDir, errorDir);
    ptiProcessedFilesPath = Paths.get(rootDir, processedDir);
    pticnmvFilesPath = Paths.get(rootDir, cnmvDir);
    ptiCanonesFilesPath = Paths.get(rootDir, canonesDir);
    ptiOtrosFilesPath = Paths.get(rootDir, otrosDir);
    ptiInformesFilesPath = Paths.get(rootDir, informesDir);
    ptiCorretajesFilesPath = Paths.get(rootDir, corretajesDir);

    LOG.info("[ Root Dir ] : " + ptiRootFilesPath);

    // rutas de los ficheros

    LOG.info("[ Temp Dir ] : " + ptiTemporalFilesPath);
    LOG.info("[ Error Dir ] : " + ptiErrorFilesPath);
    LOG.info("[ Processed Dir ] : " + ptiProcessedFilesPath);

    // creacion directorios
    Files.createDirectories(ptiRootFilesPath);
    Files.createDirectories(ptiTemporalFilesPath);
    Files.createDirectories(ptiErrorFilesPath);
    Files.createDirectories(ptiProcessedFilesPath);
    Files.createDirectories(pticnmvFilesPath);
    Files.createDirectories(ptiCanonesFilesPath);
    Files.createDirectories(ptiOtrosFilesPath);
    Files.createDirectories(ptiInformesFilesPath);
    Files.createDirectories(ptiCorretajesFilesPath);

  }

  @Override
  public void unzipPtiFiles() throws IOException {

    // descomprimimos los zips y los movemos
    try (DirectoryStream<Path> stream = Files.newDirectoryStream(ptiRootFilesPath, ptiFilePattern + ptiZipExtension);) {
      for (Path path : stream) {
        try {
          FileUtils.unzipFile(ptiTemporalFilesPath.toString(), path.toFile());
        }
        catch (IOException e) {
          LOG.error("[unzipPtiFiles] error al descomprimir el fichero {} : {}", path.toString(), e.getMessage());
          FileUtils.moveFile(path, ptiErrorFilesPath.resolve(path.getFileName()));
        }
      }
    }
  }

  @Override
  public void process() throws IOException, Exception {
    LOG.debug("[process] Inicio...");
    // comprobamos que la estructura de dirs esta creada
    checkDirStructure();
    // Ajustes y Fallido: Procesamos los archivos zip de AN que hay en el
    // directorio
    processDirectoryFiles(ptiRootFilesPath, ptiFilePattern);
    moveDirectoryFiles();
    LOG.debug("[process] Fin");
  }

  private void processDirectoryFiles(Path directoryPath, String filePattern) throws SIBBACBusinessException {
    Path movePath = ptiProcessedFilesPath;
    LOG.debug("[processDirectoryFiles] Inicio");
    try {
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath, filePattern + ptiZipExtension);) {
        // COPIAMOS LOS FICHEROS PARA EL if
        copyAllToIF(stream, true);
      }

      try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath, filePattern + ptiZipExtension);) {
        for (Path path : stream) {
          File ptiFile = path.toFile();
          if (ptiFile.isDirectory()) {
            processDirectoryFiles(path, filePattern);
          }
          else {

            try {
              processZipFile(ptiFile);
            }
            catch (Exception ex) {
              LOG.error("[processDirectoryFiles] error al procesar el fichero {} : {} ", ptiFile.getAbsolutePath(), ex);
              movePath = ptiErrorFilesPath;
            }
            try {
              // mover a procesado o erroneo
              FileUtils.moveFile(path, movePath.resolve(path.getFileName()));
            }
            catch (IOException ioe) {
              LOG.error("[processDirectoryFiles] error al mover el fichero {} : {} ", ptiFile.getAbsolutePath(), ioe);
            }
          }
        }
      }
    }
    catch (IOException ioe) {
      LOG.error("[processDirectoryFiles] error al recuperar los ficheros a procesar: {}", ioe.getMessage());
    }
    LOG.debug("[processDirectoryFiles] Fin");
  }

  private void moveDirectoryFiles(String pattern, Path movePath) {
    LOG.debug("[moveDirectoryFiles] Inicio");
    try {
      // mover ficheros cnmv
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(ptiRootFilesPath, pattern);) {
        for (Path path : stream) {
          File ptiFile = path.toFile();
          try {

            // mover a procesado o erroneo
            FileUtils.moveFile(path, movePath.resolve(path.getFileName()));

          }
          catch (IOException ioe) {
            LOG.error("[moveDirectoryFiles] error al mover el fichero {} : {} ", ptiFile.getAbsolutePath(), ioe);
          }
        }
      }
      LOG.debug("[moveDirectoryFiles] Fin");
    }
    catch (IOException ioe) {
      LOG.error("[moveDirectoryFiles] error al recuperar los ficheros a mover: {}", ioe.getMessage());
    }
  }

  private void moveDirectoryFiles() {
    LOG.debug("[moveDirectoryFiles] Inicio");
    this.moverFicheros(ptiFilePatternCNMV, pticnmvFilesPath);
    this.moverFicheros(ptiFilePatternCanones, ptiCanonesFilesPath);
    this.moverFicheros(ptiFilePatternOtros, ptiOtrosFilesPath);
    this.moverFicheros(ptiFilePatternInformes, ptiInformesFilesPath);
    this.moverFicheros(ptiFilePatternCorretajes, ptiCorretajesFilesPath);
    LOG.debug("[moveDirectoryFiles] Fin");
  }

  private void moverFicheros(String ptiFilePattern, Path movePath) {
    String[] paternFicheros = ptiFilePattern.split(";");
    if (paternFicheros != null && paternFicheros.length > 0) {
      for (int i = 0; i < paternFicheros.length; i++) {
        moveDirectoryFiles(paternFicheros[i], movePath);
      }
    }
  }

  private void processZipFile(File zipFile) throws ZipException, IOException, PTIMessageException, PTIException,
      SIBBACBusinessException {
    ZipFile zip = new ZipFile(zipFile);
    Enumeration<? extends ZipEntry> zipEntries = zip.entries();
    InputStream isFile = null;

    while (zipEntries.hasMoreElements()) {
      ZipEntry entry = zipEntries.nextElement();
      if (!entry.isDirectory()) {
        LOG.info("[processZipFile] Process file: {}", entry.getName());
        isFile = zip.getInputStream(entry);
        processFile(isFile);
      }
    }
    zip.close();
  }

  private Path getNewTmpIfFileName(String fileName, int countFiles) throws SIBBACBusinessException {
    final Path res;
    final String fileNameTmp;
    String message;
    boolean exists;
    int reintento = 0;
    
    fileNameTmp = new StringBuilder().append(ifPrefix).append("_").append(countFiles).append("_")
        .append(FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.CONCURRENT_FILE_DATETIME_FORMAT))
        .append(".TMP").toString();
    res = Paths.get(ptiTemporalFilesPath.toString(), fileNameTmp);
    askForDeletion(res);
    do {
      exists = Files.exists(res);
      if (exists) {
        try {
          Thread.sleep(100);
        }
        catch (InterruptedException e) {
          LOG.error("[getNewTmpIfFileName] Se ha interrumpido la espera activa del método", e);
          break;
        }
      }
    }
    while (exists && ++reintento < REINTENTOS);
    if(exists) {
      message = MessageFormat.format("Se ha esperado {0} veces la eliminación del fichero {1} y continua existiendo", 
          reintento, fileNameTmp);
      LOG.error("[getNewTmpIfFileName] {}", message);
      throw new SIBBACBusinessException(message);
    }
    return res;
  }

  private Path getNewFinalIfFileName(String fileName, boolean gzipped) throws SIBBACBusinessException {
    final Path res;
    final String fileNameTmp, message;
    boolean exists;
    int reintentos = 0;
    
    fileNameTmp = new StringBuilder().append(ifPrefix).append(fileName)
        .append(fileName.endsWith(ifSuffix) ? "" : ifSuffix)
        .append(gzipped ? ".GZ" : "").toString();
    res = Paths.get(ptiTemporalFilesPath.toString(), fileNameTmp);
    askForDeletion(res);
    do {
      exists = Files.exists(res);
      if (exists) {
        try {
          Thread.sleep(100);
        }
        catch (InterruptedException e) {
          LOG.error("[getNewFinalIfFileName] Interrupción de la espera activa", e);
          break;
        }
      }
    }
    while (exists && ++reintentos < REINTENTOS);
    if(exists) {
      message = MessageFormat.format("Se ha esperado {0} veces la eliminación del fichero {1} y continua existiendo", 
          reintentos, fileNameTmp);
      LOG.error("[getNewFinalIfFileName] {}", message);
      throw new SIBBACBusinessException(message);
    }
    return res;
  }

  private void copyAllToIF(DirectoryStream<Path> directoryStream, boolean gzippedOutPut) throws IOException, 
      SIBBACBusinessException {
    int countFiles = 0;
    boolean zipped;
    boolean gzipped;
    InputStream isFile = null;
    ZipFile zip;
    Enumeration<? extends ZipEntry> zipEntries;
    ZipEntry entry;
    for (Path path : directoryStream) {
      LOG.info("[copyAllToIF] Process file: {}", path.toString());
      zipped = path.toString().toUpperCase().endsWith(".ZIP");
      gzipped = path.toString().toUpperCase().endsWith(".GZ");

      if (zipped) {
        zip = new ZipFile(path.toFile());
        zipEntries = zip.entries();
        while (zipEntries.hasMoreElements()) {
          entry = zipEntries.nextElement();
          if (!entry.isDirectory()) {
            LOG.info("[copyAllToIF] Process file: {}", entry.getName());
            countFiles++;
            zipped = entry.getName().toUpperCase().endsWith(".ZIP");
            gzipped = entry.getName().toUpperCase().endsWith(".GZ");
            isFile = zip.getInputStream(entry);
            copyIFFile(countFiles, gzipped, gzippedOutPut, entry.getName(), isFile);
          }
        }
        zip.close();
      }
      else {
        countFiles++;
        isFile = Files.newInputStream(path, StandardOpenOption.READ);
        copyIFFile(countFiles, gzipped, gzippedOutPut, path.toFile().getName(), isFile);
      }
    }
  }

  private void copyIFFile(int countFiles, boolean gzipped, boolean gzippedOutPut, String fileName, InputStream isFile)
      throws IOException, SIBBACBusinessException {
    OutputStream os;
    Path outputPath = getNewTmpIfFileName(fileName, countFiles);
    Path finalOutputpath = getNewFinalIfFileName(fileName, !gzipped && gzippedOutPut);

    if (gzipped && !gzippedOutPut) {
      isFile = new GZIPInputStream(isFile);
    }
    if (gzippedOutPut && !gzipped) {
      os = new GZIPOutputStream(Files.newOutputStream(outputPath, StandardOpenOption.CREATE, StandardOpenOption.WRITE));
    }
    else {
      os = Files.newOutputStream(outputPath, StandardOpenOption.CREATE, StandardOpenOption.WRITE);
    }
    IOUtils.copyLarge(isFile, os);
    isFile.close();
    os.close();
    Files.move(outputPath, finalOutputpath, StandardCopyOption.ATOMIC_MOVE);
    LOG.info("[copyIFFile] Creado fichero IF: {}", finalOutputpath.toString());
  }

  private void processFile(InputStream ptiFile) throws PTIMessageException, IOException, PTIException,
      SIBBACBusinessException {
    int leidos = 0, anDetectados = 0;
    PTIMessageVersion version = null;
    
    LOG.debug("[processFile] Inicio...");
    if (PTIMessageFactory.getCredentials() == null) {
      ptiCommons.initPTICredentials();
    }
    final List<AN> ansRevisarDcv = new ArrayList<>();
    try (InputStreamReader isReader = new InputStreamReader(ptiFile);
        BufferedReader brPtiIn = new BufferedReader(isReader)) {
      String line = null;
      while ((line = brPtiIn.readLine()) != null) {
        if (line.startsWith("AN")) {
          anDetectados++;
          Header head = (Header) PTIMessageFactory.parseHeader(line);
          if(version == null) {
            version = cfgBo.getPtiMessageVersionFromTmct0cfg(head.getFechaDeEnvio());
          }
          AN an = (AN) PTIMessageFactory.parse(version, line);
          ansRevisarDcv.add(an);
          if (ansRevisarDcv.size() >= 100) {
            try {
              sinEccMsgInAnBo.saveNumerosOperacionDcvsNewTransaction(ansRevisarDcv);
              ansRevisarDcv.clear();
            }
            catch (RuntimeException e) {
              LOG.error("[processFile] Error inesperado al revisar ficheros ANS ", e);
            }
          }
        }
        if(++leidos % 1000 == 0) {
          LOG.info("[processFile] En proceso: {} leidos, {} ANS detectados", leidos, anDetectados);
        }
      }// fin while
      if (CollectionUtils.isNotEmpty(ansRevisarDcv)) {
        try {
          sinEccMsgInAnBo.saveNumerosOperacionDcvsNewTransaction(ansRevisarDcv);
          ansRevisarDcv.clear();
        }
        catch (RuntimeException e) {
          LOG.error("[processFile] Error inesperado al revisar ficheros ANS ", e);
        }
      }
      LOG.info("[processFile] Fin. {} leidos, {} ANS detectados", leidos, anDetectados);
    }
    catch (IOException e) {
      LOG.error("[processFile] Error al leer", e);
    }
  }
  
  private void askForDeletion(Path res) throws SIBBACBusinessException {
    String message;
    
    try {
      if(Files.exists(res)) {
        Files.delete(res);
      }
    }
    catch(IOException ioex) {
      message = MessageFormat.format("Error al solicitar la eliminacion del fichero {0}", res.getFileName());
      LOG.error("[getNewFinalIfFileName] {}", message, ioex);
      throw new SIBBACBusinessException(message, ioex);
    }
  }

}
