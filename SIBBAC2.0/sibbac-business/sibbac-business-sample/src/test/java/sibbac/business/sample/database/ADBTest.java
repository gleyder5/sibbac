package sibbac.business.sample.database;


import javax.annotation.PostConstruct;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import sibbac.business.sample.database.bo.ClienteBo;
import sibbac.business.sample.database.bo.CuentaBancoBo;
import sibbac.business.sample.database.bo.CuentaBo;
import sibbac.business.sample.database.bo.EntidadFinancieraBo;
import sibbac.test.BaseTest;


public abstract class ADBTest extends BaseTest {

	protected static final Logger	LOG					= LoggerFactory.getLogger( ADBTest.class );

	protected static boolean		testDataInjected	= false;

	protected String				prefix;

	@Autowired
	protected ApplicationContext	applicationContext;

	@Autowired
	protected ClienteBo				boCliente			= null;

	@Autowired
	protected CuentaBo				boCuenta			= null;

	@Autowired
	protected CuentaBancoBo			boCuentaBanco		= null;

	@Autowired
	protected EntidadFinancieraBo	boEntidadFinanciera	= null;

	public ADBTest() {
	}

	@PostConstruct
	public void init() {
		String prefix = "[ADBTest::init(@PostConstruct) ] ";
		if ( !testDataInjected ) {
			LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
			testDataInjected = true;
		}
	}

	@Before
	public void before() {
		this.prefix = "";
	}

}
