package sibbac.business.sample.database;


// Internal
import static org.junit.Assume.assumeNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@Transactional
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestCuentaBanco extends ADBTest {

	/*
	 * For "create-drop" / "drop-create" systems, this can be avoided.
	 * 
	 * @Before
	 * 
	 * @Test
	 * 
	 * @SqlGroup({
	 * 
	 * @Sql("/load.sql"),
	 * 
	 * @Sql(scripts = "/cleanup.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, config = @SqlConfig(transactionMode =
	 * SqlConfig.TransactionMode.ISOLATED))
	 * })
	 * 
	 * @Transactional
	 * public void test00DataLoad() {
	 * }
	 */

	@Test
	public void test01AltaDeCuenta() {
		assumeNotNull( boCuentaBanco );
	}

}
