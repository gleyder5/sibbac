package sibbac.business.sample.database;


// Internal
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.util.List;
import java.util.Set;

/*
 * Hack: 2015.04.22 15:05
 * Por peticion expresa del Departamento de Arquitectura Tecnica de ISBAN/Santander,
 * es necesario incorporar la anotacion "Transactional" de Spring en vez de la estandar.

// import javax.transaction.Transactional;
*/
import org.springframework.transaction.annotation.Transactional;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import sibbac.business.sample.database.model.Cliente;
import sibbac.business.sample.database.model.Cuenta;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestAuditoria extends ADBTest {

	protected static final Logger	LOG			= LoggerFactory.getLogger( TestAuditoria.class );

	private static Long				idCliente	= 1L;

	@Test
	public void test01AltaDeClienteDeAuditoria() {
		assumeNotNull( boCliente );
		LOG.debug( "Insertando un nuevo cliente en la base de datos..." );
		Cliente cliente = this.altaDeCliente( "Un nuevo cliente para auditoria" );
		LOG.debug( "Cliente insertado: [{}]", cliente );
		this.idCliente = cliente.getId();
		this.listaClientes();
		this.infoDeCliente( cliente );
	}

	@Test
	public void test02ClienteDeAuditoria() {
		assumeNotNull( boCliente );
		LOG.debug( "Veamos los clientes que hay en la base de datos..." );
		this.listaClientes();
		LOG.debug( "Buscando al BetaTester por el ID Cliente BetaTester: " + idCliente );
		Cliente cliente = boCliente.findById( idCliente );
		LOG.debug( "Cliente detectado: [{}]", cliente );
		assertNotNull( "Cliente nulo", cliente );
		this.infoDeCliente( cliente );
	}

	@Test
	@Transactional
	public void test03AddCuentaAClienteDeAuditoria() {
		assumeNotNull( boCliente );
		LOG.debug( "Buscando un cliente con id: [{}]", idCliente );
		Cliente cliente = boCliente.findById( idCliente );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		LOG.debug( "Creando una cuenta para el cliente con id: [{}]", idCliente );
		Cuenta cuenta = new Cuenta();
		LOG.debug( "> Cuenta creada. Estableciendo el propietario." );
		cuenta.setOwner( cliente );
		assertNotNull( "Cuenta nulo", cuenta );
		LOG.debug( "> Guardando la cuenta." );
		boCuenta.save( cuenta );
		LOG.debug( "Cuenta: " + cuenta );

		LOG.debug( "ADDING Cuenta to Cliente..." );
		cliente.addCuenta( cuenta );
		LOG.debug( "ADDED Cuenta to Cliente." );

		this.infoDeCliente( cliente );
	}

	@Test
	public void test04ClienteConCuentaDeAuditoria() {
		assumeNotNull( boCliente );
		Cliente cliente = boCliente.findById( idCliente );
		assertNotNull( "Cliente nulo", cliente );
		listaCuentasDeCliente( cliente );
	}

	@Test
	public void test05ModificarClienteDeAuditoria() {
		assumeNotNull( boCliente );
		Cliente cliente = boCliente.findById( idCliente );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		cliente.setActive( false );
		boCliente.save( cliente );
		assertFalse( "Cliente aun activo!", cliente.isActive() );
		LOG.debug( "Cliente desactivado: " + cliente );
	}

	@Test
	public void test06AuditoriaDeCliente() {
		assumeNotNull( boCliente );
		Cliente cliente = boCliente.findById( idCliente );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		List< Number > revisiones = boCliente.getAllRevisions( idCliente );
		assertNotNull( "Sin revisiones", revisiones );
		LOG.debug( "Revisiones: " + revisiones.size() );
		Cliente clienteAuditado = null;
		for ( Number revision : revisiones ) {
			LOG.debug( "+ Revision: " + revision );
			clienteAuditado = boCliente.getRevisionForId( idCliente, revision );
			LOG.debug( "  Informacion de la Revision: " + revision );
			this.infoDeClienteAuditado( cliente, clienteAuditado, revision );
		}
		LOG.debug( "Fin de las {} Revisiones.", revisiones.size() );
	}

	// ------------------------------------------------ Internal Methods

	private void listaClientes() {
		assumeNotNull( boCliente );
		LOG.debug( "Buscando la lista de clientes..." );
		List< Cliente > clientes = ( List< Cliente > ) boCliente.findAll();
		assertNotNull( "Clientes nulo", clientes );
		assertTrue( "Clientes vacio", clientes != null );
		LOG.debug( "Numero de clientes: " + clientes.size() );

		for ( Cliente customer : clientes ) {
			listaCuentasDeCliente( customer );
		}
	}

	private void listaCuentasDeCliente( final Cliente customer ) {
		Cliente cliente = customer;
		LOG.debug( "+ Cliente: " + cliente );
		if ( cliente.hasAccounts() ) {
			for ( Cuenta cuenta : cliente.getCuentas() ) {
				LOG.debug( "  > Cuenta: " + cuenta );
			}
		}
	}

	private void infoDeCliente( final Cliente cliente ) {
		this.infoDeCliente( cliente, "" );
	}

	private void infoDeCliente( final Cliente cliente, final String prefix ) {
		LOG.debug( prefix + "**** Info de Cliente: ****************************" );
		LOG.debug( prefix + "+ ID: [{}]", cliente.getId() );
		LOG.debug( prefix + "  Name: [{}]", cliente.getName() );
		LOG.debug( prefix + "  Username: [{}]", cliente.getUsername() );
		LOG.debug( prefix + "  Password: [{}]", cliente.getPassword() );
		LOG.debug( prefix + "  Email: [{}]", cliente.getEmail() );
		LOG.debug( prefix + "  AUDIT: [{}/{}]", cliente.getAuditUser(), cliente.getAuditDate() );
		Set< Cuenta > cuentas = cliente.getCuentas();
		if ( cuentas == null || cuentas.isEmpty() ) {
			LOG.debug( prefix + "  Sin cuentas" );
		} else {
			LOG.debug( prefix + "  Con cuentas" );
			for ( Cuenta cuenta : cuentas ) {
				LOG.debug( prefix + "  + Cuenta: [{}]", cuenta );
			}
		}
		LOG.debug( prefix + "**** Info de Cliente: ****************************" );
	}

	private void infoDeClienteAuditado( final Cliente cliente, final Cliente clienteAuditado, final Number revision ) {
		LOG.debug( prefix + "**** Info de Cliente AUDITADO: *******************" );
		this.infoDeCliente( cliente, "  " );
		LOG.debug( prefix + "  >>> AUDITORIA DE CLIENTE: [revision=={}]", revision );
		this.infoDeCliente( clienteAuditado, "  " );
		LOG.debug( prefix + "**** Info de Cliente AUDITADO: *******************" );
	}

	private Cliente altaDeCliente( final String name ) {
		Cliente cliente = new Cliente();
		cliente.setName( name );
		cliente.setEmail( "paco@pepe.com" );
		cliente.setUsername( "AuditUsername" );
		cliente.setPassword( "Auditpassword" );
		assertNotNull( "Cliente[" + name + "] nulo", cliente );
		LOG.debug( "SAVING Cliente[" + name + "]: " + cliente );
		try {
			boCliente.save( cliente );
			LOG.debug( "SAVED Cliente[" + name + "]: " + cliente );
		} catch ( DataIntegrityViolationException e ) {
			LOG.warn( "WARN({}): {}", e.getClass().getName(), e.getMessage() );
			List< Cliente > clientes = boCliente.findByName( name );
			if ( clientes != null ) {
				cliente = clientes.get( 0 );
				LOG.debug( "RETRIEVED Cliente[" + name + "]: " + cliente );
			}
		}
		return cliente;
	}

}
