package sibbac.business.sample.database;


// Internal
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.sample.database.model.Cliente;
import sibbac.business.sample.database.model.Cuenta;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@Transactional
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestCuenta extends ADBTest {

	private static Long	idBetaTester	= 1L;

	@Test
	public void test01AltaDeCuenta() {
		assumeNotNull( boCuenta );
		idBetaTester = this.altaDeCuenta( "Una nueva cuenta" );
		this.altaDeCuenta( "Otra nueva cuenta" );
		this.listaCuentas();
	}

	@Test
	public void test02Cuenta() {
		assumeNotNull( boCuenta );
		Cuenta cuenta = boCuenta.findById( idBetaTester );
		assertNotNull( "Cuenta nula", cuenta );
		LOG.debug( prefix + "Cuenta: " + cuenta );

		LOG.debug( prefix + "Cuenta[id]: " + cuenta.getId() );
		LOG.debug( prefix + "Cuenta[owner]: " + cuenta.getOwner() );
	}

	@Test
	public void test03DesactivarCuenta() {
		assumeNotNull( boCuenta );
		Cuenta cuenta = boCuenta.findById( idBetaTester );
		assertNotNull( "Cuenta nula", cuenta );
		LOG.debug( prefix + "Cuenta: " + cuenta );

		cuenta.setActive( false );
		boCuenta.save( cuenta );
		assertFalse( "Cuenta aun activa!", cuenta.isActive() );
		LOG.debug( prefix + "Cuenta desactivada: " + cuenta );
	}

	@Test
	public void test04ActivarCuenta() {
		assumeNotNull( boCuenta );
		Cuenta cuenta = boCuenta.findById( idBetaTester );
		assertNotNull( "Cuenta nula", cuenta );
		LOG.debug( prefix + "Cuenta: " + cuenta );

		cuenta.setActive( true );
		boCuenta.save( cuenta );
		assertTrue( "Cuenta aun inactiva!", cuenta.isActive() );
		LOG.debug( prefix + "Cuenta activada: " + cuenta );
	}

	// ------------------------------------------------ Internal Methods

	private void listaCuentas() {
		assumeNotNull( boCuenta );
		List< Cuenta > cuentas = ( List< Cuenta > ) boCuenta.findAll();
		assertNotNull( "Cuentas nulas", cuentas );
		assertTrue( "Cuentas vacia", cuentas != null );
		LOG.debug( prefix + "Numero de cuentas: " + cuentas.size() );
		for ( Cuenta cuenta : cuentas ) {
			LOG.debug( prefix + "+ Cuenta: " + cuenta );
		}
	}

	private Long altaDeCuenta( final String name ) {
		// Primero, nos aseguramos de tener un cliente.
		assumeNotNull( boCliente );
		List< Cliente > clientes = ( List< Cliente > ) boCliente.findAll();
		assertNotNull( "Clientes nulo", clientes );
		assertTrue( "Clientes vacio", clientes != null );
		Cliente cliente = clientes.get( clientes.size() - 1 );
		LOG.debug( prefix + "Cliente escogido: " + cliente );

		Cuenta cuenta = new Cuenta();
		cuenta.setOwner( cliente );
		assertNotNull( "Cuenta[" + name + "] nulo", cuenta );
		LOG.debug( prefix + "SAVING Cuenta[" + name + "]: " + cuenta );
		try {
			boCuenta.save( cuenta );
		} catch ( DataIntegrityViolationException e ) {
			LOG.warn( "WARN({}): {}", e.getClass().getName(), e.getMessage() );
			List< Cuenta > cuentas = boCuenta.findByOwner( cliente );
			if ( cuentas != null ) {
				cuenta = cuentas.get( 0 );
			}
		}
		LOG.debug( prefix + "SAVED Cuenta[" + name + "]: " + cuenta );
		return cuenta.getId();
	}

}
