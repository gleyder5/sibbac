package sibbac.business.sample.database;


// Internal
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.sample.database.model.Cliente;
import sibbac.business.sample.database.model.Cuenta;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestCliente extends ADBTest {

	protected static final Logger	LOG				= LoggerFactory.getLogger( TestCliente.class );

	private static Long				idBetaTester	= 1L;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test01AltaDeCliente() {
		assumeNotNull( boCliente );
		this.altaDeCliente( "Un nuevo cliente" );
		idBetaTester = this.altaDeCliente( "Otro nuevo cliente" );
		this.listaClientes();
		LOG.debug( "ID Cliente BetaTester: " + idBetaTester );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test02Cliente() {
		assumeNotNull( boCliente );
		LOG.debug( "Veamos los clientes que hay en la base de datos..." );
		this.listaClientes();
		LOG.debug( "Buscando al BetaTester por el ID Cliente BetaTester: " + idBetaTester );
		Cliente cliente = boCliente.findById( idBetaTester );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		LOG.debug( "Cliente[id]: " + cliente.getId() );
		LOG.debug( "Cliente[name]: " + cliente.getName() );
		LOG.debug( "Cliente[cuentas]: " + cliente.getCuentas() );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test03AddCuentaACliente() {
		assumeNotNull( boCliente );
		LOG.debug( "Buscando un cliente con id: [{}]", idBetaTester );
		Cliente cliente = boCliente.findById( idBetaTester );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		LOG.debug( "Creando una cuenta para el cliente con id: [{}]", idBetaTester );
		Cuenta cuenta = new Cuenta();
		LOG.debug( "> Cuenta creada. Estableciendo el propietario." );
		cuenta.setOwner( cliente );
		assertNotNull( "Cuenta nulo", cuenta );
		LOG.debug( "> Guardando la cuenta." );
		boCuenta.save( cuenta );
		LOG.debug( "Cuenta: " + cuenta );

		LOG.debug( "ADDING Cuenta to Cliente..." );
		cliente.addCuenta( cuenta );
		LOG.debug( "ADDED Cuenta to Cliente." );

		// this.listaClientes();
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test04ClienteConCuenta() {
		assumeNotNull( boCliente );
		Cliente cliente = boCliente.findById( idBetaTester );
		assertNotNull( "Cliente nulo", cliente );
		listaCuentasDeCliente( cliente );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test05DesactivarCliente() {
		assumeNotNull( boCliente );
		Cliente cliente = boCliente.findById( idBetaTester );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		cliente.setActive( false );
		boCliente.save( cliente );
		assertFalse( "Cliente aun activo!", cliente.isActive() );
		LOG.debug( "Cliente desactivado: " + cliente );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test06ActivarCliente() {
		assumeNotNull( boCliente );
		Cliente cliente = boCliente.findById( idBetaTester );
		assertNotNull( "Cliente nulo", cliente );
		LOG.debug( "Cliente: " + cliente );

		cliente.setActive( true );
		boCliente.save( cliente );
		assertTrue( "Cliente aun inactivo!", cliente.isActive() );
		LOG.debug( "Cliente activado: " + cliente );
	}

	// ------------------------------------------------ Internal Methods

	private void listaClientes() {
		assumeNotNull( boCliente );
		List< Cliente > clientes = ( List< Cliente > ) boCliente.findAll();
		assertNotNull( "Clientes nulo", clientes );
		assertTrue( "Clientes vacio", clientes != null );
		LOG.debug( "Numero de clientes: " + clientes.size() );

		for ( Cliente customer : clientes ) {
			listaCuentasDeCliente( customer );
		}
	}

	private void listaCuentasDeCliente( final Cliente customer ) {
		Cliente cliente = customer;
		LOG.debug( "+ Cliente: " + cliente );
		if ( cliente.hasAccounts() ) {
			for ( Cuenta cuenta : cliente.getCuentas() ) {
				LOG.debug( "  > Cuenta: " + cuenta );
			}
		}
	}

	private Long altaDeCliente( final String name ) {
		Cliente cliente = new Cliente();
		cliente.setName( name );
		assertNotNull( "Cliente[" + name + "] nulo", cliente );
		LOG.debug( "SAVING Cliente[" + name + "]: " + cliente );
		try {
			boCliente.save( cliente );
			LOG.debug( "SAVED Cliente[" + name + "]: " + cliente );
		} catch ( DataIntegrityViolationException e ) {
			LOG.warn( "WARN({}): {}", e.getClass().getName(), e.getMessage() );
			List< Cliente > clientes = boCliente.findByName( name );
			if ( clientes != null ) {
				cliente = clientes.get( 0 );
				LOG.trace( "GOT First Cliente[" + name + "]: " + cliente );
			} else {
				LOG.trace( "NO First Cliente[" + name + "]: " + cliente );
			}
		}
		LOG.debug( "END SAVING Cliente[" + name + "]: " + cliente );
		return cliente.getId();
	}

}
