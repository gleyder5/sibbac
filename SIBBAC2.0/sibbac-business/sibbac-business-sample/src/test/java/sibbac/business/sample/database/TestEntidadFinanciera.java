package sibbac.business.sample.database;


// Internal
import static org.junit.Assume.assumeNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
// Java
// JUnit
// Spring


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@Transactional
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestEntidadFinanciera extends ADBTest {

	@Test
	public void test01AltaDeEntidadFinanciera() {
		assumeNotNull( boEntidadFinanciera );
	}

}
