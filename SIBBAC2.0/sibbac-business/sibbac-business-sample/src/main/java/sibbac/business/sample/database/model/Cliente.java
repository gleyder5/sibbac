package sibbac.business.sample.database.model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents a {@link sibbac.business.sample.database.model.Cliente customer}.
 * 
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.SAMPLE.CUSTOMER )
@Entity
@Audited
public class Cliente extends ATable< Cliente > {

	// ------------------------------------------------- Bean properties

	/**
	 * Whether the {@link sibbac.database.model.Entidad entity} is active or not.
	 */
	@Column( name = "active" )
	protected boolean		active;

	/**
	 * The {@link sibbac.business.sample.database.model.Cliente customer}'s name.
	 */
	@Column( name = "name", unique = true )
	protected String		name;

	/**
	 * The {@link sibbac.business.sample.database.model.Cliente customer}'s username.
	 */
	@Column( name = "username" )
	protected String		username;

	/**
	 * The {@link sibbac.business.sample.database.model.Cliente customer}'s password.
	 */
	@Column( name = "password" )
	@NotAudited
	protected String		password;

	/**
	 * The {@link sibbac.business.sample.database.model.Cliente customer}'s email.
	 */
	@Column( name = "email" )
	protected String		email;

	/**
	 * The {@link sibbac.business.sample.database.model.Cliente customer}'s {@link sibbac.business.sample.database.model.Cuenta accounts}.
	 */
	@OneToMany( targetEntity = Cuenta.class, mappedBy = "owner", fetch = FetchType.EAGER )
	private Set< Cuenta >	cuentas;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The {@link sibbac.business.sample.database.model.Cliente customer}'s default constructor.
	 */
	public Cliente() {
		this.active = true;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s id.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s id.
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 */
	public boolean isActive() {
		return this.active;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s name.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s name.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s username.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s username.
	 */
	public String getUsername() {
		return this.username;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s password.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s password.
	 */
	public String getPassword() {
		return this.password;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s email.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s email.
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s {@link sibbac.business.sample.database.model.Cuenta
	 * accounts}.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s {@link sibbac.business.sample.database.model.Cuenta
	 *         accounts}.
	 */
	public Set< Cuenta > getCuentas() {
		return cuentas;
	}

	/*
	 * Returns whether the {@link sibbac.database.model.Cliente customer} has {@link sibbac.database.model.Cuenta accounts}.
	 * 
	 * @return Whether the {@link sibbac.database.model.Cliente customer} has {@link sibbac.database.model.Cuenta accounts}.
	 */
	public boolean hasAccounts() {
		return ( this.cuentas != null && !this.cuentas.isEmpty() );
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s id.
	 * 
	 * @param id The {@link sibbac.business.sample.database.model.Cliente customer}'s id.
	 */
	public void setId( final Long id ) {
		this.id = id;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 * 
	 * @param active The {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 */
	public void setActive( final boolean active ) {
		this.active = active;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s name.
	 * 
	 * @param name The {@link sibbac.business.sample.database.model.Cliente customer}'s name.
	 */
	public void setName( final String name ) {
		this.name = name;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s username.
	 * 
	 * @param name The {@link sibbac.business.sample.database.model.Cliente customer}'s username.
	 */
	public void setUsername( final String username ) {
		this.username = username;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s password.
	 * 
	 * @param name The {@link sibbac.business.sample.database.model.Cliente customer}'s password.
	 */
	public void setPassword( final String password ) {
		this.password = password;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s email.
	 * 
	 * @param name The {@link sibbac.business.sample.database.model.Cliente customer}'s email.
	 */
	public void setEmail( final String email ) {
		this.email = email;
	}

	/**
	 * Sets the the {@link sibbac.business.sample.database.model.Cliente customer}'s {@link sibbac.business.sample.database.model.Cuenta
	 * accounts}.
	 * 
	 * @param accounts The {@link sibbac.business.sample.database.model.Cliente customer}'s
	 *            {@link sibbac.business.sample.database.model.Cuenta accounts}.
	 */
	public void setCuentas( final Set< Cuenta > accounts ) {
		this.cuentas = accounts;
	}

	// ------------------------------------------------ Business Methods

	/**
	 * Adds a {@link sibbac.business.sample.database.model.Cliente customer}'s {@link sibbac.business.sample.database.model.Cuenta account}.
	 * 
	 * @param accounts The {@link sibbac.business.sample.database.model.Cliente customer}'s
	 *            {@link sibbac.business.sample.database.model.Cuenta account} to be
	 *            added.
	 */
	public void addCuenta( final Cuenta account ) {
		account.setOwner( this );
		if ( this.cuentas == null ) {
			this.cuentas = new HashSet< Cuenta >();
		}
		this.cuentas.add( account );
	}

	/**
	 * Deletes a {@link sibbac.business.sample.database.model.Cliente customer}'s {@link sibbac.business.sample.database.model.Cuenta
	 * account}.
	 * 
	 * @param accounts The {@link sibbac.business.sample.database.model.Cliente customer}'s
	 *            {@link sibbac.business.sample.database.model.Cuenta account} to be
	 *            deleted.
	 */
	public Cuenta delCuenta( final Cuenta account ) {
		Cuenta deletedAccount = null;
		if ( this.cuentas != null ) {
			if ( this.cuentas.contains( account ) ) {
				account.setOwner( null );
				if ( this.cuentas.remove( account ) ) {
					deletedAccount = account;
				}
			}
		}
		return deletedAccount;
	}

	// ------------------------------------------------ Internal Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[id==").append(this.id)
				.append("] [active==").append(this.active)
				.append("] [name==").append(this.name)
				.append("] [username==").append(this.username)
				//.append("] [password==").append(this.password)
				.append("] [<N> Cuentas==").append( ( this.cuentas != null ) ? this.cuentas.size() : 0 )
				.append("]");
		return bulder.toString();
	}

}
