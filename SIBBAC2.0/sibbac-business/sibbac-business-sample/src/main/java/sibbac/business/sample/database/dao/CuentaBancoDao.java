package sibbac.business.sample.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.sample.database.model.CuentaBanco;
import sibbac.business.sample.database.model.SICACode;


@Component
public interface CuentaBancoDao extends JpaRepository< CuentaBanco, SICACode > {

}
