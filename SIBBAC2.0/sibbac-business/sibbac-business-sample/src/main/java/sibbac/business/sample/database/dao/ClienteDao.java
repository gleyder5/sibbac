package sibbac.business.sample.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import sibbac.business.sample.database.model.Cliente;


@Component
public interface ClienteDao extends JpaRepository< Cliente, Long > {

	List< Cliente > findByName( final String name );

	Cliente findByUsername( final String username );

	@Query( "select u from Cliente u where ( ( UPPER(u.username) = UPPER(?1) ) OR ( UPPER(u.email) = UPPER(?1) ) ) and u.password = ?2" )
	Cliente validaCredenciales( final String login, final String password );

}
