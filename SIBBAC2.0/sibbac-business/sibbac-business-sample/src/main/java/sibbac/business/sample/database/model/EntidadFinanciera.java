package sibbac.business.sample.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;


/**
 * Class that represents a {@link sibbac.business.sample.database.model.EntidadFinanciera Finance Entity}.
 * 
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
@Table( name = DBConstants.SAMPLE.BANK )
@Entity
@Audited
public class EntidadFinanciera implements Comparable< EntidadFinanciera > {

	// ------------------------------------------------- Bean properties

	/**
	 * The bank code.
	 */
	@Id
	@Column( name = "id", length = 4, nullable = false )
	protected String	id;

	/**
	 * The bank name.
	 */
	@Column
	protected String	name;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public EntidadFinanciera() {
	}

	/**
	 * The parametrized constructor.
	 * 
	 * @param id The entity id.
	 */
	public EntidadFinanciera( final String id ) {
		this( id, null );
	}

	/**
	 * The parametrized constructor.
	 * 
	 * @param id The entity id.
	 * @param name The name.
	 */
	public EntidadFinanciera( final String id, final String name ) {
		this.id = id;
		this.name = name;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Returns the id.
	 * 
	 * @return The id.
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * Returns the name.
	 * 
	 * @return The name.
	 */
	public String getName() {
		return this.name;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Sets the id.
	 * 
	 * @param id The id.
	 */
	public void setId( final String id ) {
		this.id = id;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name The name.
	 */
	public void setName( final String name ) {
		this.name = name;
	}

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	@Override
	public int compareTo( final EntidadFinanciera ef ) {
		if ( this.id == null ) {
			return -1;
		}
		if ( ef.id == null ) {
			return -2;
		}
		boolean sameId = this.id.equalsIgnoreCase( ef.getId() );
		boolean sameName = this.name.equalsIgnoreCase( ef.getName() );
		return ( sameId && sameName ) ? 0 : -3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[Entidad Financiera(" + this.id + ")==" + this.name + "]";
	}

}
