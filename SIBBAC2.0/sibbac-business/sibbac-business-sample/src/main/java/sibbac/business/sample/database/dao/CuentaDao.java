package sibbac.business.sample.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.sample.database.model.Cliente;
import sibbac.business.sample.database.model.Cuenta;


@Component
public interface CuentaDao extends JpaRepository< Cuenta, Long > {

	List< Cuenta > findByOwner( final Cliente owner );

}
