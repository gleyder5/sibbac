package sibbac.business.sample.database.bo;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.sample.database.dao.CuentaBancoDao;
import sibbac.business.sample.database.model.CuentaBanco;
import sibbac.business.sample.database.model.SICACode;
import sibbac.database.bo.AbstractBo;


/**
 * Hello world!
 */
@Service
@Transactional
public class CuentaBancoBo extends AbstractBo< CuentaBanco, SICACode, CuentaBancoDao > {

	@Autowired
	private CuentaBancoDao	dao;

	public CuentaBanco findById( final SICACode id ) {
		return dao.findOne( id );
	}

	public List< CuentaBanco > findAll() {
		return ( List< CuentaBanco > ) dao.findAll();
	}

	public CuentaBanco create( final CuentaBanco entity ) {
		return dao.save( entity );
	}

}
