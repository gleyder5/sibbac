package sibbac.business.sample.database.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATable;


/**
 * Class that represents an {@link sibbac.business.sample.database.model.Cuenta account}.
 * 
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.SAMPLE.ACCOUNT )
@Entity
@Audited
public class Cuenta extends ATable< Cuenta > {

	// ------------------------------------------------- Bean properties

	/**
	 * Whether the {@link sibbac.business.sample.database.model.Cuenta account} is active or not.
	 */
	@Column( name = "active" )
	protected boolean	active;

	/**
	 * The {@link sibbac.business.sample.database.model.Cuenta account}'s owner.
	 */
	@ManyToOne( targetEntity = Cliente.class, cascade = CascadeType.ALL )
	@JoinColumn( name = "idOwner", nullable = false )
	private Cliente		owner;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The {@link sibbac.business.sample.database.model.Cuenta account}'s default constructor.
	 */
	public Cuenta() {
		this.active = true;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cuenta account}'s id.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cuenta account}'s id.
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cuenta account}'s status.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cuenta account}'s status.
	 */
	public boolean isActive() {
		return this.active;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cuenta account}'s {@link sibbac.business.sample.database.model.Cliente
	 * owner}.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cuenta account}'s {@link sibbac.business.sample.database.model.Cliente
	 *         owner}.
	 */
	public Cliente getOwner() {
		return this.owner;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cuenta account}'s id.
	 * 
	 * @param id The {@link sibbac.business.sample.database.model.Cuenta account}'s id.
	 */
	public void setId( final Long id ) {
		this.id = id;
	}

	/**
	 * Sets whether the {@link sibbac.business.sample.database.model.Cuenta account} is active or not.
	 * 
	 * @param active The {@link sibbac.business.sample.database.model.Cuenta account}'s active status.
	 */
	public void setActive( final boolean active ) {
		this.active = active;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cuenta account}'s {@link sibbac.business.sample.database.model.Cliente owner}.
	 * 
	 * @param owner The {@link sibbac.business.sample.database.model.Cuenta account}'s {@link sibbac.business.sample.database.model.Cliente
	 *            owner}.
	 */
	public void setOwner( final Cliente owner ) {
		this.owner = owner;
	}

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[id==" + this.id + "] [active==" + this.active + "] [owner=="
				+ ( ( this.owner != null ) ? this.owner.getName() : "<Sin asignar>" ) + "]";
	}

}
