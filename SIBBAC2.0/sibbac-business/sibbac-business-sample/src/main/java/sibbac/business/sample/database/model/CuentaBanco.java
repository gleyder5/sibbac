package sibbac.business.sample.database.model;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;


/**
 * Class that represents a {@link sibbac.business.sample.database.model.CuentaBanco bank account}.
 * 
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
@Table( name = DBConstants.SAMPLE.BANK_ACCOUNT )
@Entity
@Audited
public class CuentaBanco {

	// ------------------------------------------------- Bean properties

	/**
	 * The {@link sibbac.business.sample.database.model.CuentaBanco bank account}'s id.
	 */
	@EmbeddedId
	protected SICACode	id;

	/**
	 * Whether the {@link sibbac.database.model.Entidad Bank Account} is active or not.
	 */
	@Column( name = "active" )
	protected boolean	active;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The {@link sibbac.business.sample.database.model.CuentaBanco bank account}'s default constructor.
	 */
	public CuentaBanco() {
		this.active = true;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Returns the {@link sibbac.business.sample.database.model.CuentaBanco bank account}'s id.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.CuentaBanco bank account}'s id.
	 */
	public SICACode getId() {
		return this.id;
	}

	/**
	 * Returns the {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 * 
	 * @return The {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 */
	public boolean isActive() {
		return this.active;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Sets the {@link sibbac.business.sample.database.model.CuentaBanco bank account}'s id.
	 * 
	 * @param id The {@link sibbac.business.sample.database.model.CuentaBanco bank account}'s id.
	 */
	public void setId( final SICACode id ) {
		this.id = id;
	}

	/**
	 * Sets the {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 * 
	 * @param active The {@link sibbac.business.sample.database.model.Cliente customer}'s status.
	 */
	public void setActive( final boolean active ) {
		this.active = active;
	}

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[id==" + this.id + "] [active==" + this.active + "]";
	}

}
