package sibbac.business.sample.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.sample.database.dao.CuentaDao;
import sibbac.business.sample.database.model.Cliente;
import sibbac.business.sample.database.model.Cuenta;
import sibbac.database.bo.AbstractBo;


/**
 * Hello world!
 */
@Service
public class CuentaBo extends AbstractBo< Cuenta, Long, CuentaDao > {

	public List< Cuenta > findByOwner( final Cliente owner ) {
		return this.dao.findByOwner( owner );
	}

}
