package sibbac.business.sample.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.sample.database.model.EntidadFinanciera;


@Component
public interface EntidadFinancieraDao extends JpaRepository< EntidadFinanciera, String > {

}
