package sibbac.business.sample.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import sibbac.database.DBConstants;


/**
 * Class that represents a {@link sibbac.business.sample.database.model.SICACode SICA Code}.
 * 
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
@SuppressWarnings( "serial" )
@Embeddable
public class SICACode implements Serializable, Comparable< SICACode > {

	// ------------------------------------------------- Bean properties

	/**
	 * The bank entity.
	 */
	@ManyToOne( targetEntity = EntidadFinanciera.class, fetch = FetchType.EAGER )
	// @Column( length = 4, nullable = false )
	protected EntidadFinanciera	entidad;

	/**
	 * The bank office.
	 */
	@Column( length = 4, nullable = false )
	protected String			oficina;

	/**
	 * The bank control digits.
	 */
	@Column( length = 2, nullable = false )
	protected String			digitos;

	/**
	 * The bank account.
	 */
	@Column( length = 4, nullable = false )
	protected String			cuenta;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SICACode() {
	}

	/**
	 * The parametrized constructor.
	 * 
	 * @param entidad The entity.
	 * @param oficina The office.
	 * @param cuenta The account numbers.
	 */
	public SICACode( final EntidadFinanciera entidad, final String oficina, final String cuenta ) {
		this.entidad = entidad;
		this.oficina = oficina;
		this.cuenta = cuenta;
		this.digitos = this.calculate();
	}

	/**
	 * The parametrized constructor.
	 * 
	 * @param entidad The entity.
	 * @param oficina The office.
	 * @param cuenta The account numbers.
	 */
	public SICACode( final String entidad, final String oficina, final String cuenta ) {
		this.entidad = new EntidadFinanciera( entidad );
		this.oficina = oficina;
		this.cuenta = cuenta;
		this.digitos = this.calculate();
	}

	/**
	 * The parametrized constructor.
	 * 
	 * @param entidad The entity.
	 * @param oficina The office.
	 * @param digitos The control digits.
	 * @param cuenta The account numbers.
	 */
	public SICACode( final EntidadFinanciera entidad, final String oficina, final String digitos, final String cuenta ) {
		this.entidad = entidad;
		this.oficina = oficina;
		this.cuenta = cuenta;
		this.digitos = digitos;
	}

	/**
	 * The parametrized constructor.
	 * 
	 * @param entidad The entity.
	 * @param oficina The office.
	 * @param digitos The control digits.
	 * @param cuenta The account numbers.
	 */
	public SICACode( final String entidad, final String oficina, final String digitos, final String cuenta ) {
		this.entidad = new EntidadFinanciera( entidad );
		this.oficina = oficina;
		this.cuenta = cuenta;
		this.digitos = digitos;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Returns the entity.
	 * 
	 * @return The entity.
	 */
	public EntidadFinanciera getEntidad() {
		return this.entidad;
	}

	/**
	 * Returns the office.
	 * 
	 * @return The office.
	 */
	public String getOficina() {
		return this.oficina;
	}

	/**
	 * Returns the control digits.
	 * 
	 * @return The control digits.
	 */
	public String getDigitos() {
		return this.digitos;
	}

	/**
	 * Returns the account numbers.
	 * 
	 * @return The account numbers.
	 */
	public String getCuenta() {
		return this.cuenta;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Sets the entity.
	 * 
	 * @param entidad The entity.
	 */
	public void setEntidad( final EntidadFinanciera entidad ) {
		this.entidad = entidad;
	}

	/**
	 * Sets the office.
	 * 
	 * @param oficina The office.
	 */
	public void setOficina( final String oficina ) {
		this.oficina = oficina;
	}

	/**
	 * Sets the control digits.
	 * 
	 * @param digitos The control digits.
	 */
	public void setDigitos( final String digitos ) {
		this.digitos = digitos;
	}

	/**
	 * Sets the account numbers.
	 * 
	 * @param cuenta The account numbers.
	 */
	public void setCuenta( final String cuenta ) {
		this.cuenta = cuenta;
	}

	// ------------------------------------------------ Business Methods

	/**
	 * 
	 * La forma de calcular el digito de control es esta:<br/>
	 * <ul>
	 * <li>Para obtener el primer digito de control:
	 * <ul>
	 * <li>La primera cifra del banco se multiplica por 4.</li>
	 * <li>La segunda cifra del banco se multiplica por 8.</li>
	 * <li>La tercera cifra del banco se multiplica por 5.</li>
	 * <li>La cuarta cifra del banco se multiplica por 10.</li>
	 * <li>La primera cifra de la entidad se multiplica por 9.</li>
	 * <li>La segunda cifra de la entidad se multiplica por 7.</li>
	 * <li>La tercera cifra de la entidad se multiplica por 3.</li>
	 * <li>La cuarta cifra de la entidad se multiplica por 6.</li>
	 * <li>Se suman todos los resultados obtenidos.</li>
	 * <li>Se divide entre 11 y nos quedamos con el resto de la division.</li>
	 * <li>A 11 le quitamos el resto anterior, y ese el el primer digito de control, con la salvedad de que si nos da 10, el digito es 1</li>
	 * </ul>
	 * </li>
	 * <li>Para obtener el segundo digito de control:
	 * <ul>
	 * <li>La primera cifra de la cuenta se multiplica por 1</li>
	 * <li>La primera cifra de la cuenta se multiplica por 2</li>
	 * <li>La primera cifra de la cuenta se multiplica por 4</li>
	 * <li>La primera cifra de la cuenta se multiplica por 8</li>
	 * <li>La primera cifra de la cuenta se multiplica por 5</li>
	 * <li>La primera cifra de la cuenta se multiplica por 10</li>
	 * <li>La primera cifra de la cuenta se multiplica por 9</li>
	 * <li>La primera cifra de la cuenta se multiplica por 7</li>
	 * <li>La primera cifra de la cuenta se multiplica por 3</li>
	 * <li>La primera cifra de la cuenta se multiplica por 6</li>
	 * <li>Se suman todos los resultados obtenidos.</li>
	 * <li>Se divide entre 11 y nos quedamos con el resto de la division.</li>
	 * <li>A 11 le quitamos el resto anterior, y ese el el segundo digito de control, con la salvedad de que si nos da 10, el digito es 1</li>
	 * </ul>
	 * </li>
	 * </ul>
	 */
	private String calculate() {
		StringBuffer sb = new StringBuffer( "00" );
		sb.append( this.entidad.getId() );
		sb.append( this.oficina );
		StringBuffer dc = new StringBuffer();
		dc.append( this.getDigitFor( sb.toString() ) );
		dc.append( this.getDigitFor( this.cuenta ) );
		return dc.toString();
	}

	/**
	 * Validates the digits.
	 */
	public boolean validate() {
		return this.calculate().equals( this.digitos );
	}

	// ------------------------------------------------ Internal Methods

	private int getDigitFor( final String cadena ) {
		int total = 0, result = 0;
		for ( int n = 0; n < cadena.length(); n++ ) {
			total += cadena.charAt( n ) * DBConstants.PESOS[ n ];
		}
		result = 11 - ( total % 11 );
		return ( result == 11 ) ? 0 : ( ( result == 10 ) ? 1 : result );
	}

	/**
	 * Devuelve una cadena de texto con toda la informacion del codigo SICA.
	 * 
	 * @return An {@link java.lang.String} with the SICA's FQDN.
	 */
	private String getCode() {
		StringBuffer sb = new StringBuffer();
		sb.append( this.entidad.getId() );
		sb.append( this.oficina );
		sb.append( this.digitos );
		sb.append( this.cuenta );
		return sb.toString();
	}

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	@Override
	public int compareTo( final SICACode sica ) {
		String thisCode = this.getCode();
		String otherCode = sica.getCode();
		return thisCode.compareTo( otherCode );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[Cuenta==" + this.entidad.getId() + "." + this.oficina + "." + this.digitos + "." + this.cuenta + "]";
	}

}
