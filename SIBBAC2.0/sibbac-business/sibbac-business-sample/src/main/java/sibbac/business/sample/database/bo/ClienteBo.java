package sibbac.business.sample.database.bo;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.sample.database.dao.ClienteDao;
import sibbac.business.sample.database.model.Cliente;
import sibbac.database.bo.AbstractBo;


/**
 * Hello world!
 */
@Service
@Transactional
public class ClienteBo extends AbstractBo< Cliente, Long, ClienteDao > {

	public static final int	PAGE_SIZE	= 10;

	public List< Cliente > findByName( final String name ) {
		return this.dao.findByName( name );
	}

	public Cliente findByUsername( final String username ) {
		return this.dao.findByUsername( username );
	}

	public Cliente validaCredenciales( final String login, final String password ) {
		return this.dao.validaCredenciales( login, password );
	}

	public Page< Cliente > findFirst10() {
		Pageable request = new PageRequest( 0, PAGE_SIZE, Sort.Direction.ASC, "id" );
		return this.dao.findAll( request );
	}

}
