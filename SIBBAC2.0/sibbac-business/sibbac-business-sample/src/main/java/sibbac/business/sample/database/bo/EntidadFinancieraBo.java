package sibbac.business.sample.database.bo;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.sample.database.dao.EntidadFinancieraDao;
import sibbac.business.sample.database.model.EntidadFinanciera;
import sibbac.database.bo.AbstractBo;


/**
 * Hello world!
 */
@Service
@Transactional
public class EntidadFinancieraBo extends AbstractBo< EntidadFinanciera, String, EntidadFinancieraDao > {
}
