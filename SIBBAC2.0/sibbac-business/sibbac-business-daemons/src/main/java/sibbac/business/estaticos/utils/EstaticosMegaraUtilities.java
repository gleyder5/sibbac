package sibbac.business.estaticos.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.betbtdta.dao.BetpfeclDao;
import sibbac.database.betbtdta.dao.Betpfect1Dao;
import sibbac.database.betbtdta.dao.BetpfectDao;
import sibbac.database.betbtdta.dao.Betpfesuc1Dao;
import sibbac.database.betbtdta.dao.BetpfesucDao;
import sibbac.database.betbtdta.model.Betpfecl;
import sibbac.database.betbtdta.model.Betpfect;
import sibbac.database.betbtdta.model.Betpfect1;
import sibbac.database.betbtdta.model.Betpfect1Id;
import sibbac.database.betbtdta.model.BetpfectId;
import sibbac.database.bsnbpsql.tcli.dao.Tcli0hpcDao;
import sibbac.database.bsnbpsql.tcli.dao.Tcli0pclDao;
import sibbac.database.bsnbpsql.tcli.model.Tcli0hpc;
import sibbac.database.bsnbpsql.tcli.model.Tcli0hpcId;
import sibbac.database.bsnbpsql.tcli.model.Tcli0pcl;
import sibbac.database.bsnbpsql.tcli.model.Tcli0pclId;
import sibbac.database.megbpdta.bo.Fmeg0aliBo;
import sibbac.database.megbpdta.model.Fmeg0ali;

@Service
public class EstaticosMegaraUtilities {

  protected static final Logger LOG = LoggerFactory.getLogger(EstaticosMegaraUtilities.class);

  @Autowired
  private Fmeg0aliBo fmg0aliBo;

  @Autowired
  private BetpfeclDao betpfeclDao;

  @Autowired
  private BetpfectDao betpfectDao;

  @Autowired
  private Betpfect1Dao betpfect1Dao;

  @Autowired
  private BetpfesucDao betpfesucDao;

  @Autowired
  private Betpfesuc1Dao betpfesuc1Dao;

  @Autowired
  private Tcli0pclDao tcli0pclDao;

  @Autowired
  private Tcli0hpcDao tcli0hpcDao;

  @Autowired
  private EntityManagerFactory emf;

  private static final String AUDIT_USER = "SIB20EST";

  private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
  private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

  @Transactional(rollbackFor = Exception.class)
  public BigDecimal createNuctapro(String cdalias, String broclient, String action) throws Exception {
    BigDecimal nuctapro = null;

    if (cdalias != null && broclient != null) {
      try {

        // 1º Comprobamos si la cuenta(alias) opera para nacional

        if (isNationalMarket(cdalias)) {

          // Comprobamos si ya existe nuctapro para esa cuenta

          nuctapro = getNuctaproAccount(cdalias);

          if ("I".equals(action)) {

            // Si no existe se crea la cuenta/subcuenta de nacional y la
            // relación en la tabla betpfect
            if (nuctapro == null) {

              BigDecimal nuclient = getNuclient(broclient);
              if (nuclient == null) {

                throw new Exception(" No existe relaci�n con el broclient " + broclient.trim()
                    + " en la tabla de relaciones Betpfecl");
              }

              if (existNuctapro(new BigDecimal(cdalias)))
                throw new Exception("Existe ya una cuenta en nacional para la cuenta " + cdalias);
              // nuctapro = getNuctapro();
              else
                nuctapro = new BigDecimal(cdalias);

              createAccountSubaccount(nuctapro, nuclient, nuctapro);
              createHistoricAccountSubaccount(nuctapro, nuclient, nuctapro, "A");
              createAccountRelation(broclient, cdalias, nuctapro, false);
              createAccountRelationClone(broclient, cdalias, nuctapro, false);

            }

          }

        }

      }

      catch (Exception ex) {
        throw ex;
      }
      finally {

      }
    }

    return nuctapro;
  }

  private boolean isNationalMarket(String cdalias) {

    boolean isNationalMarket = false;

    if (cdalias != null) {

      Fmeg0ali fmeg0ali = fmg0aliBo.findById(cdalias);

      if (fmeg0ali != null) {
        final String stlev1lm = fmeg0ali.getStlev1lm();

        if (stlev1lm != null && !"".equals(stlev1lm.trim()))
          isNationalMarket = true;
      }

    }
    return isNationalMarket;
  }

  public BigDecimal getNuclient(String broclient) throws Exception {

    BigDecimal clienteNacional = null;
    if (broclient != null) {

      List<Betpfecl> listBetpfecl = betpfeclDao.findAllByIdEcloms(broclient);
      if (listBetpfecl != null && !listBetpfecl.isEmpty()) {
        final Betpfecl betpfecl = (Betpfecl) listBetpfecl.get(0);
        clienteNacional = betpfecl.getId().getEclcli();
      }

    }
    return clienteNacional;
  }

  public boolean existNuctapro(BigDecimal nuctapro) throws Exception {

    boolean existNuctapro = false;
    if (nuctapro != null) {
      existNuctapro = tcli0pclDao.existsNuctapro(nuctapro) > 0;
    }
    return existNuctapro;
  }

  public BigDecimal getNuctaproAccount(String cdalias) {

    BigDecimal nuctapro = null;
    if (cdalias != null) {
      final List<BigDecimal> result = betpfectDao.findAllNuctaproByAlias(new BigDecimal(cdalias.trim()));
      if (result != null && !result.isEmpty()) {
        nuctapro = (BigDecimal) result.get(0);
      }
      else {
        LOG.warn("[getNuctaproAccount] no se ha recuperado nuctapro para datos: {}", new BigDecimal(cdalias.trim()));
      }
    }
    return nuctapro;
  }

  public String getAlias(String cdproduc, BigDecimal nuctapro) {

    String alias = null;

    List<BigDecimal> result = betpfectDao.findAllAliasByCdproductAndNuctapro(cdproduc, nuctapro);

    if (result != null && result.size() > 0) {
      alias = ((BigDecimal) result.get(0)).toString();
    }
    if (alias != null && cdproduc.equals(alias.toString())) {
      alias = null;
    }

    return alias;
  }

  @Transactional
  public void createAccountRelation(String broclient, String alias, BigDecimal nuctapro, boolean isDB2)
      throws Exception {

    if (broclient != null && alias != null && nuctapro != null) {
      try {
        final Date d = new Date();
        final String dateAsString = dateFormat.format(d);
        final String timeAsString = timeFormat.format(d);
        Betpfect reg = new Betpfect();
        reg.setId(new BetpfectId(broclient, new BigDecimal(alias), "008"));
        reg.setEctcta(nuctapro);
        reg.setEctfal(new BigDecimal(dateAsString));
        reg.setEctfba(BigDecimal.ZERO);
        reg.setEcthmo(timeAsString);
        reg.setEctumo(AUDIT_USER);
        betpfectDao.save(reg);

      }
      catch (final Exception ex) {
        LOG.warn("ERROR UPDATING MIFID DATA");
        throw ex;
      }
      finally {
      }
    }

  }

  @Transactional(rollbackFor = Exception.class)
  public void createAccountRelationClone(String broclient, String alias, BigDecimal nuctapro, boolean isDB2)
      throws Exception {
    if (broclient != null && alias != null && nuctapro != null) {
      try {
        final Date d = new Date();
        final String dateAsString = dateFormat.format(d);
        final String timeAsString = timeFormat.format(d);
        Betpfect1 reg = new Betpfect1();
        reg.setId(new Betpfect1Id(broclient, new BigDecimal(alias), "008"));
        reg.setEctcta(nuctapro);
        reg.setEctfal(new BigDecimal(dateAsString));
        reg.setEctfba(BigDecimal.ZERO);
        reg.setEcthmo(timeAsString);
        reg.setEctumo(AUDIT_USER);
        betpfect1Dao.save(reg);
      }
      catch (final Exception ex) {
        LOG.warn("ERROR UPDATING MIFID DATA");
        throw ex;
      }
      finally {
      }
    }

  }

  @Transactional(rollbackFor = Exception.class)
  public void createAccountSubaccount(BigDecimal nuctapro, BigDecimal nuclient, BigDecimal nuctagrl, String nutituls,
      String nuposici) throws Exception {

    if (nuctapro != null && nuclient != null && nuctagrl != null) {
      Tcli0pcl reg = new Tcli0pcl();
      reg.setId(new Tcli0pclId("008", nuctapro, nuclient));
      reg.setNutituls(nutituls);
      reg.setNuposici(nuposici);
      reg.setNuctagrl(nuctagrl);
      reg.setCddebaja("");
      reg.setCdprogrl("008");
      final Date d = new Date();
      final String dateAsString = dateFormat.format(d);
      reg.setFhmodifi(new BigDecimal(dateAsString));
      reg.setCdusrmod(AUDIT_USER);
      reg.setTptiprep("T");
      tcli0pclDao.save(reg);

    }

  }

  public void createAccountSubaccount(BigDecimal nuctapro, BigDecimal nuclient, BigDecimal nuctagrl) throws Exception {
    createAccountSubaccount(nuctapro, nuclient, nuctagrl, "01", "01");

  }

  public void createHistoricAccountSubaccount(BigDecimal nuctapro, BigDecimal nuclient, BigDecimal nuctagrl,
      final String action) throws Exception {
    createHistoricAccountSubaccount(nuctapro, nuclient, nuctagrl, "01", "01", action);

  }

  public void createHistoricAccountSubaccount(BigDecimal nuctapro, BigDecimal nuclient, BigDecimal nuctagrl,
      String nutituls, String nuposici, final String action) throws Exception {

    if (nuctapro != null && nuclient != null && nuctagrl != null && action != null) {
      final Date d = new Date();
      final String dateAsString = dateFormat.format(d);
      final String timeAsString = (timeFormat.format(d)).replaceAll("\\:", "");
      Tcli0hpc reg = new Tcli0hpc();
      reg.setId(new Tcli0hpcId(new BigDecimal(dateAsString), new BigDecimal(timeAsString), AUDIT_USER, action, "008",
          nuctapro, nuclient));

      // reg.setId(new Tcli0pclId("008", nuctapro, nuclient));
      reg.setNutituls(nutituls);
      reg.setNuposici(nuposici);
      reg.setNuctagrl(nuctagrl);
      reg.setCdprogrl("008");
      reg.setTptiprep("T");
      tcli0hpcDao.save(reg);
    }

  }

  public BigDecimal getNuctaproSubAccount(String cdalias, String cdsubcta, String subAccountBroclient) {

    BigDecimal nuctapro = null;

    cdalias = StringUtils.trim(cdalias);
    cdsubcta = StringUtils.trim(cdsubcta);

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();

      final List<BigDecimal> result = betpfesucDao.findAllNuctaproByIdBroClientAndIdCdaliassAndIdCdsubcta(
          subAccountBroclient, new BigDecimal(cdalias), new BigDecimal(cdsubcta));

      if (result.isEmpty()) {
        LOG.warn("[getNuctaproSubAccount] no se ha recuperado nuctapro para los datos: {}-{}-{}", subAccountBroclient,
            new BigDecimal(cdalias), new BigDecimal(cdsubcta));
      }
      else {
        nuctapro = (BigDecimal) result.get(0);
      }
      tx.commit();

    }
    catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();

      }
      em.close();
    }

    if (nuctapro != null && cdalias.equals(nuctapro.toString()))
      nuctapro = null;

    return nuctapro;

  }

  public boolean isAliasUnified(String cdalias) {

    return true;

  }

  public boolean isAliasUnifiedSibbac(String cdalias) {
    return true;

  }

  public boolean isAccountUnified(BigDecimal nuctapro) {
    return true;

  }

}
