package sibbac.business.daemons.ork.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.UniqueConstraint;
import static javax.persistence.InheritanceType.SINGLE_TABLE;
import static javax.persistence.DiscriminatorType.CHAR;

/**
 * Metadatos de un campo del fichero proveniente de Fidessa para MIFID II ORK
 * @author XI326526
 */
@Entity
@Inheritance(strategy = SINGLE_TABLE)
@Table(name = "TMCT0_ORK_FIDESSA_FIELD", uniqueConstraints=@UniqueConstraint(columnNames = {"POSITION"}))
@DiscriminatorColumn(name = "DISC", discriminatorType = CHAR)
@DiscriminatorValue("A")
public abstract class OrkFidessaField implements Serializable {
	
	/**
	 * Número de serie generado
	 */
	private static final long serialVersionUID = 3804450963548981537L;

	/**
	 * Nombre del campo
	 */
	@Id
	@Column(name = "NAME", nullable = false)
	private String name;
	
	/**
	 * Posición del campo dentro del fichero CSV
	 */
	@Column(name = "POSITION", nullable = false)
	private int position;
	
	/**
	 * Tipo JDBC del dato contenido en el campo {@link java.sql.Types}
	 */
	@Column(name = "TYPE", nullable = false) 
	private int type;

  /**
   * Valor por defecto para cuando el campo viene nulo en formato CSV (";;")
   */
	@Column(name = "DEFAULT_VALUE", nullable = true)
  private String defaultValue;
  
  /**
   * @return el valor por defecto pare este campo Fidessa.
   */
	public String getDefaultValue() {
    return defaultValue;
  }

	/**
	 * @param defaultValue determina el valor por defecto para este campo Fidessa.
	 */
  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

	/**
	 * @return el nombre del campo.
	 */
  public String getName() {
		return name;
	}

  /**
   * @param name determina el nombre del campo.
   */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return la posición del campo en el fichero Fidessa.
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position determina la posición del campo en el fichero Fidessa.
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return tipo JDBC del campo Fidessa.
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type determina el tipo JDBC del campo Fidessa como está determinado por {@link java.sql.Types}
	 */
	public void setType(int type) {
		this.type = type;
	}
  
	@Override
	public boolean equals(Object other) {
		final OrkFidessaField dynafield;
		
		if(other instanceof OrkFidessaField) {
			dynafield = OrkFidessaField.class.cast(other); 
			return name.equals(dynafield.name);
		}
		return false;
	}

	@Override
	public int hashCode() {
		if(name == null) {
			return 0;
		}
		return name.hashCode();
	}

	@Override
	public String toString() {
		return String.format("{%s - %d}", name, position);
	}

}
