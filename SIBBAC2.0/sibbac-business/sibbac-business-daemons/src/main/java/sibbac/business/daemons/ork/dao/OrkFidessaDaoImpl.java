package sibbac.business.daemons.ork.dao;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.common.SIBBACBusinessException;
import sibbac.business.daemons.ork.common.ResultSetIterator;

@Repository
public class OrkFidessaDaoImpl implements OrkFidessaDao {
	
	private static final Logger LOG = getLogger(OrkFidessaDaoImpl.class);
	
	private static final String QUERY = 
	    "SELECT i.NUORDENMKT, i.MERCADO, i.FECHA_ALTA, i.FECHA_VALIDEZ, i.MIC_SEGMENTO, " +
	        " a.VALUE as CDALIASS, FIS.TIPO_ID_MIFID, FIS.ID_MIFID, " +
	        " d.DYNAFIELD, d.VALUE " +
	        "FROM TMCT0_ORK_FIDESSA i " +
	        "LEFT JOIN TMCT0_ORK_FIDESSA_DETAIL a " +
	        " ON i.NUORDENMKT = a.NUORDENMKT AND i.FECHA_ALTA = a.FECHA_ALTA AND i.MIC_SEGMENTO = a.MIC_SEGMENTO AND a.DYNAFIELD = 'CDALIASS' " +
	        "LEFT JOIN TMCT0TFI TFI " +
	        " ON TFI.CDALIASS = a.VALUE AND TFI.CDSUBCTA = '' " +
	        " AND TFI.FHINICIO <= CAST(varchar_format(i.FECHA_ALTA, 'YYYYMMDD') AS INT) " +
	        " AND TFI.FHFINAL >= CAST(varchar_format(i.FECHA_ALTA, 'YYYYMMDD') AS INT) " +
	        "LEFT JOIN BSNBPSQL.TMCT0FIS FIS " +
	        " ON TFI.CDDCLAVE=FIS.CDDCLAVE " +
	        "LEFT JOIN TMCT0_ORK_FIDESSA_DETAIL d " +
	        " ON i.NUORDENMKT = d.NUORDENMKT AND i.FECHA_ALTA = d.FECHA_ALTA AND i.MIC_SEGMENTO = d.MIC_SEGMENTO AND d.DYNAFIELD != 'CDALIASS' " +
	        "WHERE i.TRATADO = 'N' " +
	        "ORDER BY i.NUORDENMKT, i.FECHA_ALTA, i.MIC_SEGMENTO, FIS.ID_MIFID ";
	
	private static final String COUNT_QUERY = "SELECT COUNT(TRATADO) FROM TMCT0_ORK_FIDESSA WHERE TRATADO = 'N'";
	
	@Autowired
	private DataSource ds;
	
	@Override
	public void iterateOverInsercionDate(ResultSetIterator iterator) throws SIBBACBusinessException {
		final String message;
		
		try(Connection con = ds.getConnection()) {
			con.setReadOnly(true);
		  try(Statement stmt = con.createStatement();
		  		ResultSet rs = stmt.executeQuery(QUERY)) {
		  	iterator.iterate(rs);
		  }
		  con.commit();
		}
		catch(SQLException sqlex) {
			message = "Error de base de datos al consultar entradas fidessa";
			LOG.error("[iterateOverInsercionDate] {}", message, sqlex);
			throw new SIBBACBusinessException(message, sqlex);
		}
	}

	@Override
	public boolean existenSinTratar() throws SIBBACBusinessException {
		final String message;
		
		try(Connection con = ds.getConnection()) {
			con.setReadOnly(true);
		  try(Statement stmt = con.createStatement();
		  		ResultSet rs = stmt.executeQuery(COUNT_QUERY)) {
		  	if(rs.next()) {
		  		return rs.getInt(1) > 0;
		  	}
		  	return false;
		  }
		  finally {
		    con.commit();
		  }
		}
		catch(SQLException sqlex) {
			message = "Error de base de datos al consultar registros sin tratar fidessa";
			LOG.error("[existenSinTratar] {}", message, sqlex);
			throw new SIBBACBusinessException(message, sqlex);
		}
	}

}
