package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * MarketCounterpartyMegara.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class MarketCounterpartyMegara extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(MarketCounterpartyMegara.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The counterparty_mnemonic. */
  private String counterparty_mnemonic;

  /** The counterparty_name. */
  private String counterparty_name;

  /** The country_domicile. */
  private String country_domicile;

  /** The status_indicator. */
  private String status_indicator;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new market counterparty_megara.
   */
  protected MarketCounterpartyMegara() {

    this.tipoActualizacion = "";
    this.counterparty_mnemonic = "";
    this.counterparty_name = "";
    this.country_domicile = "";
    this.status_indicator = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("marketCounterparty: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element MARKET_COUNTERPARTY = document.createElement("MARKET_COUNTERPARTY");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element COUNTERPARTY_MNEMONIC = document.createElement("COUNTERPARTY_MNEMONIC");
      COUNTERPARTY_MNEMONIC.appendChild(document.createTextNode(getCounterparty_mnemonic()));

      Element COUNTERPARTY_NAME = document.createElement("COUNTERPARTY_NAME");
      COUNTERPARTY_NAME.appendChild(document.createTextNode(getCounterparty_name()));

      Element TYPE_QUALIFIER = document.createElement("TYPE_QUALIFIER");
      TYPE_QUALIFIER.appendChild(document.createTextNode("BROKER"));

      Element COUNTRY_DOMICILE = document.createElement("COUNTRY_DOMICILE");
      COUNTRY_DOMICILE.appendChild(document.createTextNode(getCountry_domicile()));

      Element EXCHANGE = document.createElement("EXCHANGE");

      Element EXCHANGE_MNEMONIC = document.createElement("EXCHANGE_MNEMONIC");

      Element EXCHANGE_IDENTIFIER = document.createElement("EXCHANGE_IDENTIFIER");

      Element FIRM_CODE = document.createElement("FIRM_CODE");

      Element CUSTOMER_CODE_1 = document.createElement("CUSTOMER_CODE_1");

      Element CUSTOMER_CODE_2 = document.createElement("CUSTOMER_CODE_2");

      Element STATUS_INDICATOR = document.createElement("STATUS_INDICATOR");
      STATUS_INDICATOR.appendChild(document.createTextNode(getStatus_indicator()));

      Element BACK_OFFICE_CODE = document.createElement("BACK_OFFICE_CODE");

      Element BROKER_CONFIRMS = document.createElement("BROKER_CONFIRMS");

      Element PERSHING_G2 = document.createElement("PERSHING_G2");

      // ---------------------------------------------------------

      data.appendChild(COUNTERPARTY_MNEMONIC);
      data.appendChild(COUNTERPARTY_NAME);
      data.appendChild(TYPE_QUALIFIER);
      data.appendChild(COUNTRY_DOMICILE);
      data.appendChild(EXCHANGE);
      data.appendChild(EXCHANGE_MNEMONIC);
      data.appendChild(EXCHANGE_IDENTIFIER);
      data.appendChild(FIRM_CODE);
      data.appendChild(CUSTOMER_CODE_1);
      data.appendChild(CUSTOMER_CODE_2);
      data.appendChild(STATUS_INDICATOR);
      data.appendChild(BACK_OFFICE_CODE);
      data.appendChild(BROKER_CONFIRMS);
      data.appendChild(PERSHING_G2);

      // ---------------------------------------------------------

      MARKET_COUNTERPARTY.appendChild(header);
      MARKET_COUNTERPARTY.appendChild(data);

      document.appendChild(MARKET_COUNTERPARTY);

      logger.debug("marketCounterparty: estructura documento XML completada");

      // ---------------------------------------------------------

      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("marketCounterparty: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("marketCounterparty: error en la generacion del docuemento XML");
      logger.warn("marketCounterparty: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a market counterparty";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a market counterparty";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a market counterparty";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("marketCounterparty: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, DDMMdd0000.xml donde
   * DD es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getMarketCounterparties();
    Path path = Paths.get(getRutaDestino(), String.format("DD%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("marketCounterparty: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the counterparty_mnemonic.
   * 
   * @return the counterparty_mnemonic
   */
  protected String getCounterparty_mnemonic() {
    return counterparty_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the counterparty_mnemonic.
   * 
   * @param counterparty_mnemonic the new counterparty_mnemonic
   */
  protected void setCounterparty_mnemonic(String counterparty_mnemonic) {
    this.counterparty_mnemonic = counterparty_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the counterparty_name.
   * 
   * @return the counterparty_name
   */
  protected String getCounterparty_name() {
    return counterparty_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the counterparty_name.
   * 
   * @param counterparty_name the new counterparty_name
   */
  protected void setCounterparty_name(String counterparty_name) {
    this.counterparty_name = counterparty_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the status_indicator.
   * 
   * @return the status_indicator
   */
  protected String getStatus_indicator() {
    return status_indicator;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the status_indicator.
   * 
   * @param status_indicator the new status_indicator
   */
  protected void setStatus_indicator(String status_indicator) {
    this.status_indicator = status_indicator;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the country_domicile.
   * 
   * @return the country_domicile
   */
  protected String getCountry_domicile() {
    return country_domicile;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the country_domicile.
   * 
   * @param country_domicile the new country_domicile
   */
  protected void setCountry_domicile(String country_domicile) {
    this.country_domicile = country_domicile;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}
