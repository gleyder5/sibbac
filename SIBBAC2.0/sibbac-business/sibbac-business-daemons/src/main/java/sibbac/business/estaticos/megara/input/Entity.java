package sibbac.business.estaticos.megara.input;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sibbac.business.estaticos.fidessa.fda.output.EntityMarketCounterparty;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0eadBo;
import sibbac.database.megbpdta.bo.Fmeg0eidBo;
import sibbac.database.megbpdta.bo.Fmeg0entBo;
import sibbac.database.megbpdta.bo.Fmeg0eotBo;
import sibbac.database.megbpdta.model.Fmeg0ead;
import sibbac.database.megbpdta.model.Fmeg0eadId;
import sibbac.database.megbpdta.model.Fmeg0eid;
import sibbac.database.megbpdta.model.Fmeg0eidId;
import sibbac.database.megbpdta.model.Fmeg0ent;
import sibbac.database.megbpdta.model.Fmeg0eot;
import sibbac.database.megbpdta.model.Fmeg0eotId;

@Service
@Scope(value = "prototype")
public class Entity {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(Entity.class);

  @Autowired
  private Fmeg0entBo fmeg0entBo;

  @Autowired
  private Fmeg0eotBo fmeg0eotBo;

  @Autowired
  private Fmeg0eadBo fmeg0eadBo;

  @Autowired
  private Fmeg0eidBo fmeg0eidBo;

  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private static List<String> atrData;

  private static List<String> atrOtherId;

  private static List<String> atrIdBySE;

  private static List<String> atrOtherAddress;

  private static List<String> atrEntityRole;

  private Document document;

  private Fmeg0ent objFmeg0ent;

  public Entity(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("entityType");
    atrData.add("identifier");
    atrData.add("identifier2");
    atrData.add("name");
    atrData.add("nationality");
    atrData.add("residence");
    atrData.add("legalGroup");
    atrData.add("dateIncorporation");
    atrData.add("mainContact");
    atrData.add("otherContact");
    atrData.add("otherName");
    atrData.add("bic");
    atrData.add("birthday");
    atrData.add("civility");
    atrData.add("familyName1");
    atrData.add("familyName2");
    atrData.add("firstName");
    atrData.add("gendar");
    atrData.add("secondName");
    atrData.add("thirdName");
    atrData.add("description");
    atrData.add("issuerType");

    atrOtherId = new ArrayList<>();
    atrOtherId.add("identifier");
    atrOtherId.add("idType");
    atrOtherId.add("description");
    atrOtherId.add("documentType");

    atrIdBySE = new ArrayList<>();
    atrIdBySE.add("identifier");
    atrIdBySE.add("stockExchange");
    atrIdBySE.add("description");

    atrOtherAddress = new ArrayList<>();
    atrOtherAddress.add("adresseType");
    atrOtherAddress.add("contactFunction");
    atrOtherAddress.add("contactName");
    atrOtherAddress.add("purpose");
    atrOtherAddress.add("amailaddress");
    atrOtherAddress.add("zipCode");
    atrOtherAddress.add("location");
    atrOtherAddress.add("location2");
    atrOtherAddress.add("street");
    atrOtherAddress.add("streetNumber");
    atrOtherAddress.add("flat");
    atrOtherAddress.add("afaxnumber");
    atrOtherAddress.add("aemailaddress");
    atrOtherAddress.add("aswiftaddress");
    atrOtherAddress.add("aphonenumber");
    atrOtherAddress.add("aoasysaddress");
    atrOtherAddress.add("asequaladdress");
    atrOtherAddress.add("atelexnumber");
    atrOtherAddress.add("afileaddress");

    atrEntityRole = new ArrayList<>();
    atrEntityRole.add("role");

    objFmeg0ent = new Fmeg0ent();
    objFmeg0ent.setCdentro1("");
    objFmeg0ent.setCdentro2("");
    objFmeg0ent.setTpentity("");
    objFmeg0ent.setCdident1("");
    objFmeg0ent.setCdident2("");
    objFmeg0ent.setNbnameid("");
    objFmeg0ent.setCdnation("");
    objFmeg0ent.setCdreside("");
    objFmeg0ent.setLegalgru("");
    objFmeg0ent.setFhincorp("");
    objFmeg0ent.setNbmainct("");
    objFmeg0ent.setNbothect("");
    objFmeg0ent.setNbothena("");
    objFmeg0ent.setCdbiccid("");
    objFmeg0ent.setFhbirthd("");
    objFmeg0ent.setTpcivili("");
    objFmeg0ent.setNbfamil1("");
    objFmeg0ent.setNbfamil2("");
    objFmeg0ent.setNbfirstt("");
    objFmeg0ent.setTpgendar("");
    objFmeg0ent.setNbsecond("");
    objFmeg0ent.setNbthirdd("");
    objFmeg0ent.setDescrip5("");
    objFmeg0ent.setTpissuer("");
    objFmeg0ent.setFhdebaja("");
    objFmeg0ent.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {

      if (INPUT.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        input();
      }
      else {
        delete();
      }

      logger.info("Archivo Entity tratado correctamente");
      RegistroControl registroControl = ctx.getBean(RegistroControl.class, "Entity", objFmeg0ent.getCdident1(),
          nombreXML, "I", "Procesado fichero estatico", "Entity", "MG");
      registroControl.write();
      directorios.moverDirOk(nombreXML);

      logger.info("Generacion XML Market_Counterparty");

      try {
        ctx.getBean(EntityMarketCounterparty.class, objFmeg0ent).execXML();
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
      RegistroControl registroControl = ctx.getBean(RegistroControl.class, "Entity", objFmeg0ent.getCdident1(),
          nombreXML, "E", e.getMessage(), "Entity", "MG");
      registroControl.write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void input() throws Exception {

    String objPrimaryKey = "";
    Fmeg0ent objFmeg0ent_pers = new Fmeg0ent();

    tratarCampos();

    objPrimaryKey = objFmeg0ent.getCdident1();

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();

      objFmeg0ent_pers = fmeg0entBo.findById(objPrimaryKey);

      if (objFmeg0ent_pers == null) {
        objFmeg0ent.setTpmodeid(INPUT);
        objFmeg0ent = fmeg0entBo.save(objFmeg0ent, true);

        tratarHijos();

        // TMCT0ENT

        tx.commit();
      }
      else if ("".equals(objFmeg0ent_pers.getFhdebaja().trim())) {
        fmeg0entBo.delete(objFmeg0ent_pers);
        // UPDATE
        objFmeg0ent.setTpmodeid(UPDATE);
        objFmeg0ent = fmeg0entBo.save(objFmeg0ent, true);

        tratarHijos();

        tx.commit();
      }
      else {

        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0entBo.delete(objFmeg0ent_pers);
        objFmeg0ent.setTpmodeid(INPUT);
        objFmeg0ent = fmeg0entBo.save(objFmeg0ent, true);

        tratarHijos();

        tx.commit();
      }

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    String objPrimaryKey;
    Fmeg0ent objFmeg0ent_pers = new Fmeg0ent();

    tratarCampos();

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    objPrimaryKey = objFmeg0ent.getCdident1();
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    // transaccion

    try {
      tx.begin();

      objFmeg0ent_pers = fmeg0entBo.findById(objPrimaryKey);
      if (objFmeg0ent_pers == null) {
        throw new Exception("El registro en cuestion se encuentra en estado de 'BAJA LOGICA'");
      }
      else if ("".equals(objFmeg0ent_pers.getFhdebaja().trim())) {
        objFmeg0ent_pers.setFhdebaja(fechaSt);
        objFmeg0ent_pers.setTpmodeid(DELETE);
        objFmeg0ent = fmeg0entBo.save(objFmeg0ent_pers, false);
      }
      tx.commit();
    }

    catch (Exception e) {
      throw e;
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  private void tratarCampos() throws DOMException, Exception {

    try {
      // tx.begin();
      Element dataElement = (Element) document.getElementsByTagName("data").item(0);

      if (document.getElementsByTagName("entityRoles").getLength() != 0) {
        tratarEntityRoles(document.getElementsByTagName("entityRoles").item(0).getChildNodes());
      }

      for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
        if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
            && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

          switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
            case 0:// entityType
              objFmeg0ent.setTpentity(convertirMegaraAs400.execConvertir("TPENTITY", ((Element) dataElement
                  .getChildNodes().item(i)).getAttribute("value")));
              break;
            case 1:// identifier
              objFmeg0ent.setCdident1(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 2:// identifier2
              objFmeg0ent.setCdident2(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 3:// name
              objFmeg0ent.setNbnameid(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 4:// nationality
              objFmeg0ent.setCdnation(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 5:// residence
              objFmeg0ent.setCdreside(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 6:// legalGroup
              objFmeg0ent.setLegalgru(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 7:// dateIncorporation
              objFmeg0ent.setFhincorp(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 8:// mainContact
              objFmeg0ent.setNbmainct(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 9:// otherContact
              objFmeg0ent.setNbothect(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 10:// otherName
              objFmeg0ent.setNbothena(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 11:// bic
              objFmeg0ent.setCdbiccid(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 12:// birthday
              objFmeg0ent.setFhbirthd(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 13:// civility
              objFmeg0ent.setTpcivili(convertirMegaraAs400.execConvertir("TPCIVILI", ((Element) dataElement
                  .getChildNodes().item(i)).getAttribute("value")));
              break;
            case 14:// familyName1
              objFmeg0ent.setNbfamil1(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 15:// familyName2
              objFmeg0ent.setNbfamil2(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 16:// firstName
              objFmeg0ent.setNbfirstt(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 17:// gendar
              objFmeg0ent.setTpgendar(convertirMegaraAs400.execConvertir("TPGENDAR", ((Element) dataElement
                  .getChildNodes().item(i)).getAttribute("value")));
              break;
            case 18:// secondName
              objFmeg0ent.setNbsecond(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 19:// thirdName
              objFmeg0ent.setNbthirdd(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 20:// description
              objFmeg0ent.setDescrip5(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            case 21:// issuerType
              objFmeg0ent.setTpissuer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
              break;
            default:
              ;
          }
        }
      }
      // tx.commit();
    }
    catch (Exception e) {
      throw e;
    }
    finally {
      // tx.rollback();
      // em2.close();
    }

  }

  private void tratarHijos() throws DOMException, Exception {

    for (int i = 0; i < (document.getElementsByTagName("otherId").getLength()); i++) {

      if (document.getElementsByTagName("otherId").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarOtherId(((Element) document.getElementsByTagName("otherId").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("officialAddress").getLength()); i++) {

      if (document.getElementsByTagName("officialAddress").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarOfficialAddress(((Element) document.getElementsByTagName("officialAddress").item(i)));
      }

    }

    for (int i = 0; i < (document.getElementsByTagName("otherAddress").getLength()); i++) {

      if (document.getElementsByTagName("otherAddress").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarOtherAddress(((Element) document.getElementsByTagName("otherAddress").item(i)));
      }

    }

    for (int i = 0; i < (document.getElementsByTagName("idBySE").getLength()); i++) {

      if (document.getElementsByTagName("idBySE").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarIdBySE(((Element) document.getElementsByTagName("idBySE").item(i)));
      }

    }

  }

  private void tratarOtherId(Node otherId) throws DOMException, Exception {

    Fmeg0eot objFmeg0eot = new Fmeg0eot();
    objFmeg0eot.setId(new Fmeg0eotId(objFmeg0ent.getCdident1(), null));
    objFmeg0eot.setCdidenti("");
    objFmeg0eot.setTpidtype("");
    objFmeg0eot.setDescrip1("");
    objFmeg0eot.setTpdocume("");

    NodeList listField = otherId.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrOtherId.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// identifier
              objFmeg0eot.setCdidenti(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 1:// idType
              objFmeg0eot.setTpidtype(convertirMegaraAs400.execConvertir("TPIDTYPE", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 2:// description
              objFmeg0eot.setDescrip1(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// documentType
              objFmeg0eot.setTpdocume(convertirMegaraAs400.execConvertir("TPDOCUME", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0eot.setFmeg0ent(objFmeg0ent);

    fmeg0eotBo.save(objFmeg0eot, true);
  }

  private void tratarOtherAddress(Node otherAddress) throws DOMException, Exception {

    Fmeg0ead objFmeg0ead = new Fmeg0ead();
    objFmeg0ead.setTpaddres("");
    objFmeg0ead.setContfunc("");
    objFmeg0ead.setContname("");
    objFmeg0ead.setPurposee("");
    objFmeg0ead.setMailaddr("");
    objFmeg0ead.setCdzipcod("");
    objFmeg0ead.setNblocat1("");
    objFmeg0ead.setNblocat2("");
    objFmeg0ead.setNbstreet("");
    objFmeg0ead.setNuflattt("");
    objFmeg0ead.setNufaxnum("");
    objFmeg0ead.setEmailadd("");
    objFmeg0ead.setSwiftadd("");
    objFmeg0ead.setNuphonee("");
    objFmeg0ead.setIdoasysa("");
    objFmeg0ead.setIdsequal("");
    objFmeg0ead.setNutelexx("");
    objFmeg0ead.setNofilead("");
    objFmeg0ead.setCdindadd("");

    NodeList listField = otherAddress.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {
          switch (atrOtherAddress.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// adresseType
              objFmeg0ead.setTpaddres(convertirMegaraAs400.execConvertir("TPADDRES", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 1:// contactFunction
              objFmeg0ead.setContfunc(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// contactName
              objFmeg0ead.setContname(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// purpose
              objFmeg0ead.setPurposee(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 4:// amailaddress
              objFmeg0ead.setMailaddr(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 5:// zipCode
              objFmeg0ead.setCdzipcod(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 6:// location
              objFmeg0ead.setNblocat1(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 7:// location2
              objFmeg0ead.setNblocat2(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 8:// street
              objFmeg0ead.setNbstreet(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 9:// streetNumber
              objFmeg0ead.setNustreet(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 10:// flat
              objFmeg0ead.setNuflattt(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 11:// afaxnumber
              objFmeg0ead.setNufaxnum(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 12:// aemailaddress
              /*
               * AML 2011/08/31 Se trunca a 200 caracteres el valor del campo
               * del email recibido en el xml.
               */
              String emailAdd = listField.item(i).getAttributes().item(1).getNodeValue();
              if (emailAdd.length() > 200)
                emailAdd = emailAdd.substring(0, 200);
              objFmeg0ead.setEmailadd(emailAdd);
              break;
            case 13:// aswiftaddress
              objFmeg0ead.setSwiftadd(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 14:// aphonenumber
              objFmeg0ead.setNuphonee(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 15:// aoasysaddress
              objFmeg0ead.setIdoasysa(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 16:// asequaladdress
              objFmeg0ead.setIdsequal(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 17:// atelexnumber
              objFmeg0ead.setNutelexx(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 18:// afileaddress
              objFmeg0ead.setNofilead(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0ead.setCdindadd("OTHER");

    objFmeg0ead.setFmeg0ent(objFmeg0ent);

    fmeg0eadBo.save(objFmeg0ead, true);

  }

  private void tratarOfficialAddress(Node otherAddress) throws DOMException, Exception {

    Fmeg0ead objFmeg0ead = new Fmeg0ead();
    objFmeg0ead.setId(new Fmeg0eadId(objFmeg0ent.getCdident1(), null));
    objFmeg0ead.setTpaddres("");
    objFmeg0ead.setContfunc("");
    objFmeg0ead.setContname("");
    objFmeg0ead.setPurposee("");
    objFmeg0ead.setMailaddr("");
    objFmeg0ead.setCdzipcod("");
    objFmeg0ead.setNblocat1("");
    objFmeg0ead.setNblocat2("");
    objFmeg0ead.setNbstreet("");
    objFmeg0ead.setNuflattt("");
    objFmeg0ead.setNufaxnum("");
    objFmeg0ead.setEmailadd("");
    objFmeg0ead.setSwiftadd("");
    objFmeg0ead.setNuphonee("");
    objFmeg0ead.setIdoasysa("");
    objFmeg0ead.setIdsequal("");
    objFmeg0ead.setNutelexx("");
    objFmeg0ead.setNofilead("");
    objFmeg0ead.setCdindadd("");

    NodeList listField = otherAddress.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrOtherAddress.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// adresseType
              objFmeg0ead.setTpaddres(convertirMegaraAs400.execConvertir("TPADDRES", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 1:// contactFunction
              objFmeg0ead.setContfunc(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// contactName
              objFmeg0ead.setContname(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// purpose
              objFmeg0ead.setPurposee(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 4:// amailaddress
              objFmeg0ead.setMailaddr(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 5:// zipCode
              objFmeg0ead.setCdzipcod(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 6:// location
              objFmeg0ead.setNblocat1(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 7:// location2
              objFmeg0ead.setNblocat2(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 8:// street
              objFmeg0ead.setNbstreet(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 9:// streetNumber
              objFmeg0ead.setNustreet(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 10:// flat
              objFmeg0ead.setNuflattt(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 11:// afaxnumber
              objFmeg0ead.setNufaxnum(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 12:// aemailaddress
              objFmeg0ead.setEmailadd(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 13:// aswiftaddress
              objFmeg0ead.setSwiftadd(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 14:// aphonenumber
              objFmeg0ead.setNuphonee(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 15:// aoasysaddress
              objFmeg0ead.setIdoasysa(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 16:// asequaladdress
              objFmeg0ead.setIdsequal(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 17:// atelexnumber
              objFmeg0ead.setNutelexx(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 18:// afileaddress
              objFmeg0ead.setNofilead(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0ead.setCdindadd("OFFICIAL");

    objFmeg0ead.setFmeg0ent(objFmeg0ent);

    fmeg0eadBo.save(objFmeg0ead, true);

  }

  private void tratarIdBySE(Node idBySE) throws DOMException, Exception {

    Fmeg0eid objFmeg0eid = new Fmeg0eid();
    objFmeg0eid.setId(new Fmeg0eidId(objFmeg0ent.getCdident1(), null));
    objFmeg0eid.setCdidbyse("");
    objFmeg0eid.setCdmarket("");
    objFmeg0eid.setDescrip1("");

    NodeList listField = idBySE.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrIdBySE.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// identifier
              objFmeg0eid.setCdidbyse(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 1:// stockExchange
              objFmeg0eid.setCdmarket(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// description
              objFmeg0eid.setDescrip1(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0eid.setFmeg0ent(objFmeg0ent);
    fmeg0eidBo.save(objFmeg0eid, true);
  }

  private void tratarEntityRoles(NodeList entityRoles) throws DOMException, Exception {

    for (int i = 0; i < entityRoles.getLength(); i++) {
      Node entityRole = entityRoles.item(i);
      if (entityRole.getNodeType() == Node.ELEMENT_NODE) {
        tratarEntityRole(entityRole);
      }
    }
  }

  private void tratarEntityRole(Node entityRole) throws DOMException, Exception {

    NodeList listField = entityRole.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrEntityRole.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// role
              if ("Custodian".equals(listField.item(i).getAttributes().item(1).getNodeValue())) {

                objFmeg0ent.setCdentro1(convertirMegaraAs400.execConvertir("CDENTRO1", listField.item(i)
                    .getAttributes().item(1).getNodeValue()));
              }
              else {
                if ("Broker".equals(listField.item(i).getAttributes().item(1).getNodeValue())) {
                  objFmeg0ent.setCdentro2(convertirMegaraAs400.execConvertir("CDENTRO2", listField.item(i)
                      .getAttributes().item(1).getNodeValue()));
                }
              }
              break;
            default:
              ;
          }
        }
      }
    }

  }

}