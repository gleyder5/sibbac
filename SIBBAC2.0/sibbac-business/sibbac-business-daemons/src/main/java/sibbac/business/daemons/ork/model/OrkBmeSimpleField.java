package sibbac.business.daemons.ork.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import sibbac.business.daemons.ork.common.ResolvedValue;

/**
 * Entidad que representa un campo del fichero ORK generado que refleja directamente el valor de un campo
 * Fidessa.
 * @author XI326526
 */
@Entity
@Table(name = "TMCT0_ORK_BME_SIMPLE_FIELD")
public class OrkBmeSimpleField extends OrkBmeField {

  /**
   * Serial version.
   */
  private static final long serialVersionUID = 8680928830435578620L;
  
  @OneToOne(optional = false)
  @JoinColumn(name = "FIDESSA_FIELD", nullable = false)
  private OrkFidessaField orkFidessaField;

  /**
   * @return el campo Fidessa referido por este campo de fichero ORK.
   */
  public OrkFidessaField getOrkFidessaField() {
    return orkFidessaField;
  }

  /**
   * @param orkFidessaField determina el campo Fidessa cuyo valor será introducido en este campo en el fichero ORK. 
   */
  public void setOrkFidessaField(OrkFidessaField orkFidessaField) {
    this.orkFidessaField = orkFidessaField;
  }

  
  /**
   * {@inheritDoc} Recupera el valor de su campo Fidessa asociado.
   */
  @Override
  public Object[] getSolvedData(ResolvedValue rv) {
    return rv.getDynaData(orkFidessaField.getName());
  }

}
