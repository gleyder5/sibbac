package sibbac.business.estaticos.utils.xml;

/**
 * @author user
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 * @viz.diagram NewXmlException.tpx
 */
public class NewXmlException extends Exception {

	/**
   * 
   */
  private static final long serialVersionUID = 6677998108698912284L;

  /**
	 * Constructor for NewXmlException.
	 */
	public NewXmlException() {
		super("Excepci�n al crear documento XML ");
	}

	/**
	 * Constructor for NewXmlException.
	 * @param s
	 */
	public NewXmlException(String s) {
		super("Excepci�n al crear documento XML " + s);
	}

}
