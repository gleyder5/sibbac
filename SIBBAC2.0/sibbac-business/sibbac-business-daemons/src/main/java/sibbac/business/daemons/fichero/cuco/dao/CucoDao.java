package sibbac.business.daemons.fichero.cuco.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cli;

@Repository
public interface CucoDao extends JpaRepository<Tmct0cli, String> {

  /**
   * Query para CUCO, ORIGINAL versión contador KGR y contador Detalle
   */
  public static final String CUCO_QUERY = 
      " SELECT " + 
      "  UNO.KGR, " + 
      "  SUBSTR(UNO.DETALLE, 1,16) AS DETALLE1, " + 
      "  SUBSTR(UNO.DETALLE, 17,28) || TRIM(VARCHAR_FORMAT((UNO.PRECIO_REDONDEO * UNO.IMTITULOS), '00000000000000.00')) || SUBSTR(UNO.DETALLE,62, 28) AS DETALLE2 " + 
      " FROM ( " + 
      "  SELECT " + 
      "    CLI.CDKGR KGR, " + 
      "    TO_CHAR(CURRENT_DATE,'yyyymmdd') || '| '|| " + 
      "    TO_CHAR(CURRENT_DATE,'yymmdd') || '|' || " + 
      "    'SVB_KT|' || " + 
      "    CASE WHEN MIN(BOK.CDTPOPER) ='C' THEN 'B' ELSE 'S' END || '|' || " + 
      "    TO_CHAR(BOK.FEEJECUC,'yyyymmdd') || '|' || " + 
      "    TO_CHAR(BOK.FEVALOR, 'yyyymmdd') || '|' || " + 
      "    TRIM(VARCHAR_FORMAT(SUM(ALO.EUTOTBRU), '00000000000000.00')) || '|' || " + 
      "    LEFT(TRIM(COALESCE(CLI.CDKGR,'')) || '         ',9) || '|' || " + 
      "    TRIM(VARCHAR_FORMAT(SUM(ALO.EUTOTBRU),'00000000000000.00')) " + 
      "    DETALLE, " + 
      "    SUM(ALO.IMCAMMED) AS IMPRECIO, " + 
      "    SUM(ALO.NUTITCLI) AS IMTITULOS, " + 
      "    SUM(ALO.EUTOTBRU) AS IMEFECTIVO, " + 
      "    DECIMAL(ROUND(DECIMAL(SUM(ALO.EUTOTBRU), 15, 2) / DECIMAL(SUM(NUTITCLI), 15, 0), 2), 15, 2) AS PRECIO_REDONDEO " + 
      "  FROM " + 
      "    TMCT0ORD ORD " + 
      "    JOIN TMCT0BOK BOK ON BOK.NUORDEN = ORD.NUORDEN AND BOK.FEVALOR > :currentDate AND ((ORD.CDCLSMDO = 'E' AND BOK.CDESTADO BETWEEN 300 AND 400) OR (ORD.CDCLSMDO = 'N' AND BOK.CDESTADOASIG > 0 AND BOK.CDESTADOASIG < 85 )) " + 
      "    JOIN TMCT0ALO ALO ON ALO.NUORDEN = BOK.NUORDEN AND ALO.NBOOKING = BOK.NBOOKING AND ALO.FEVALOR > :currentDate " + 
      "    JOIN TMCT0ALI AS ALI ON ALI.CDALIASS = BOK.CDALIAS AND ALI.FHFINAL >= TO_CHAR(:currentDate, 'yyyymmdd') " + 
      "    JOIN TMCT0CLI AS CLI ON CLI.CDBROCLI = ALI.CDBROCLI AND CLI.FHFINAL >= TO_CHAR(:currentDate,'yyyymmdd') " + 
      "    JOIN TMCT0EST AS EST ON EST.CDALIASS = BOK.CDALIAS AND EST.FHFINAL >= TO_CHAR(:currentDate,'yyyymmdd')" + 
      "    JOIN TMCT0ONI ONI ON  ONI.CDNIVEL1 <> 'MIN' AND ONI.NUORDEN1 = EST.STLEV1LM " + 
      "  GROUP BY " + 
      "    CLI.CDKGR, BOK.FEEJECUC, BOK.FEVALOR, BOK.CDTPOPER " + 
      "  ORDER BY " + 
      "    CLI.CDKGR, BOK.FEEJECUC, BOK.FEVALOR " + 
      ") AS UNO";
     

  /**
   * Query para CUCO con Paginación
   */
  public static final String CUCO_PAGE_QUERY = CUCO_QUERY + " LIMIT :offset, :pageSize";

  /**
   * Query para CUCO
   */
  public static final String CUCO_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + CUCO_QUERY + ")";
  
  @Query(value = CUCO_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListByPageCUCO(@Param("currentDate") Date currentDate, @Param("offset") Integer offset,  @Param("pageSize") Integer pageSize);


  /**
   * Obtiene los datos para la generación del fichero CUCO sin paginación
   * @return
   */
  @Query(value = CUCO_QUERY, nativeQuery = true)
  public List<Object[]> findAllListCUCO(@Param("currentDate") Date currentDate);

  /**
   * Obtiene los datos para la generación del fichero CUCO sin paginación
   * @return
   */
  @Query(value = CUCO_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListCUCO();

}