package sibbac.business.daemons.ork.bo;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.ork.model.OrkBmeField;
import sibbac.business.daemons.ork.model.OrkBmeSearch;
import sibbac.business.daemons.ork.model.OrkBmeSearchedField;

class OrkResolver implements Runnable {
	
	private static final Logger LOG = getLogger(OrkResolver.class);
	
	private OrkProcessorImpl orkProcessor;
	
  /**
   * Definiciones de los campos del fichero de salida ORK
   */
	private List<OrkBmeSearchedField> searchedBuilders;
	
	private OrkPetition currentPetition;
	
	@Override
	public void run() {
		LOG.trace("[run] Inicio de hilo resolutor");
		try {
  		try {
  			while(!(currentPetition = orkProcessor.takePetition()).isEnd()) {
  			  currentPetition.preValidate();
  			  if(currentPetition.isSuccess()) {
  	        resolvePetition();
  			  }
  				orkProcessor.putResolvedPetition(currentPetition);
  			}
  		}
      catch(RuntimeException rex) {
        LOG.error("[run] Error no controlado en el resolutor de peticiones", rex);
      }
      orkProcessor.putPetition(currentPetition); //Es la peticion de finalizacion, para que la tomen los otros hilos
		}
		catch (InterruptedException e) {
			LOG.warn("[run] Error de interrupcion del hilo de peticiones.", e);
		}
    LOG.info("[run] Fin de hilo del resolutor.");
	}
	
	void setOutputBuilders(List<OrkBmeField> outputBuilders) {
		searchedBuilders = new ArrayList<>();
		for(OrkBmeField f : outputBuilders) {
		  if(f instanceof OrkBmeSearchedField) {
		    searchedBuilders.add(OrkBmeSearchedField.class.cast(f));
		  }
		}
	}
	
	void setOrkProcessor(OrkProcessorImpl orkProcessor) {
		this.orkProcessor = orkProcessor;
	}
	
	private void resolvePetition() throws InterruptedException {
	  final Map<String, Object[]> solvedSearches;
	  String message;
		OrkBmeSearch search;
		Object[] mapped;
		
		solvedSearches = new HashMap<>();
		for(final OrkBmeSearchedField field : searchedBuilders) {
			search = field.getOrkBmeSearch();
			mapped = solvedSearches.get(search.getName());
			if (mapped == null) {
			  mapped = orkProcessor.resolve(field, currentPetition);
	      solvedSearches.put(search.getName(), mapped);
			}
			if(field.isRequired() && (mapped == null || mapped.length == 0)) {
			  message = String.format(field.getErrorMsg(), currentPetition.getParamValues(search.getParamNames()));
			  currentPetition.fail(message);
			  break;
			}
			currentPetition.setSolved(field.getName(), mapped);
		}
	}

}
