package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.MarketCounterpartyMegara;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.model.Fmeg0ent;

@Service
@Scope(value = "prototype")
public class EntityMarketCounterparty extends MarketCounterpartyMegara {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EntityMarketCounterparty.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  private Fmeg0ent objFmeg0ent;

  private boolean esBroker = true;

  public EntityMarketCounterparty(Fmeg0ent objFmeg0ent) {

    super();

    this.objFmeg0ent = objFmeg0ent;

  }

  public boolean execXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    if (esBroker) {

      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();

    }
    else {
      return false;
    }

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      // ----------------------------------------------------------------------------

      if (!("B".equals(objFmeg0ent.getCdentro2().trim()))) {
        esBroker = false;
        logger.info("XML Market_Counterparty no se corresponde con un Broker");
        throw new Exception("XML Market_Counterparty no se corresponde con un Broker");
      }

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0ent.getTpmodeid());

      // ----------------------------------------------------------------------------

      setCounterparty_mnemonic(objFmeg0ent.getCdident1().trim());

      // ----------------------------------------------------------------------------

      setCounterparty_name(objFmeg0ent.getNbnameid().trim());

      // ----------------------------------------------------------------------------

      setCountry_domicile(objFmeg0ent.getCdreside().trim());

      // ----------------------------------------------------------------------------

      if ("D".equals(objFmeg0ent.getTpmodeid().trim())) {
        setStatus_indicator("I");
      }
      else {
        setStatus_indicator("A");
      }

      // ----------------------------------------------------------------------------
      tx.commit();
    }
    catch (Exception e) {
      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "MARKET COUNTERPARTY", getCounterparty_mnemonic(),
          getNombreCortoXML(), "I", "", "MarketCounterparty", "FI");

    }
    else {
      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());

      result = ctx.getBean(RegistroControl.class, "MARKET COUNTERPARTY", getCounterparty_mnemonic(),
          getNombreCortoXML(), "E", getMensajeError(), "MarketCounterparty", "FI");

    }

    return result;

  }

}