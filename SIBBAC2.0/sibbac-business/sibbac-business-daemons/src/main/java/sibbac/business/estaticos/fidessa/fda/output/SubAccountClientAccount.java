/*
 * Se modifica la clase SubAccountClientAccount para realizar una mejora solicitada por 
 * Paloma Moreno el dia 14 de noviembre de 2008
 * 
 * La mejora consiste en mover el valor del elemento Alert al elemento FUND
 * para ello unicamente se modifica el metodo que recoge el valor de la rutina de 
 * tratamiento, es decir: setAlert(..); pasa a ser setFund(..);
 * 
 * */

package sibbac.business.estaticos.fidessa.fda.output;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ClientAccountMegara;
import sibbac.business.estaticos.megara.inout.AliasOutPut;
import sibbac.business.estaticos.utils.EstaticosMegaraUtilities;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0fisBoSoloEstaticos;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.business.wrappers.database.model.Tmct0tfi;
import sibbac.common.utils.SibbacEnums;
import sibbac.database.megbpdta.bo.Fmeg0aliBo;
import sibbac.database.megbpdta.bo.Fmeg0shlBo;
import sibbac.database.megbpdta.bo.Fmeg0sotBo;
import sibbac.database.megbpdta.bo.Fmeg0suaBo;
import sibbac.database.megbpdta.bo.Fmeg0sucBo;
import sibbac.database.megbpdta.model.Fmeg0ali;
import sibbac.database.megbpdta.model.Fmeg0shl;
import sibbac.database.megbpdta.model.Fmeg0sot;
import sibbac.database.megbpdta.model.Fmeg0sua;
import sibbac.database.megbpdta.model.Fmeg0suaId;
import sibbac.database.megbpdta.model.Fmeg0suc;

@Service
@Scope(value = "prototype")
public class SubAccountClientAccount extends ClientAccountMegara {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SubAccountClientAccount.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private EstaticosMegaraUtilities utilities;

  @Autowired
  private Fmeg0aliBo fmeg0aliBo;

  @Autowired
  private Fmeg0suaBo fmeg0suaBo;

  @Autowired
  private Fmeg0sucBo fmeg0sucBo;

  @Autowired
  private Fmeg0shlBo fmeg0hslBo;

  @Autowired
  private Fmeg0sotBo fmeg0sotBo;

  /** Campo <code>application</code> de la tabla <code>tmct0cfg</code>. */
  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private Tmct0fisBoSoloEstaticos tmct0fisBo;

  @Autowired
  private Tmct0tfiBo tmct0tfiBo;

  private Fmeg0sua objFmeg0sua;

  private String tipoAcualizacion;

  private boolean esExtranjero = true;

  public SubAccountClientAccount(Fmeg0sua objFmeg0sua, String tipoActualizacion) {

    super();

    this.objFmeg0sua = objFmeg0sua;

    this.tipoAcualizacion = tipoActualizacion;

  }

  public boolean procesarXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    if (esExtranjero) {

      // generar el archivo XML

      /*
       * AML 17/01/2012
       */
      // genera archivo cuentas

      /*
       * AML 20/03/2012 No se informan las cuentas ya que supone mucha carga
       */

      /*
       * AML 21/03/2012 Se informan las cuentas solo cuando es alta
       */

      if (tipoAcualizacion.equals("I")) {
        try {
          Fmeg0ali alias = getAlias(getClient_mnemonic());

          ctx.getBean(AliasOutPut.class, alias).execXML();
        }
        catch (Exception ex) {
          logger.warn(ex.getMessage(), ex);
        }
      }

      // generar el archivo XML

      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();

    }
    else {
      result = false;
    }

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    Fmeg0suaId pkFmeg0sua = objFmeg0sua.getId();

    try {
      
      if ("H".equals(tipoAcualizacion))
      {
        setTipoActualizacion("U");
        setAccount_mnemonic(objFmeg0sua.getId().getCdsubcta());
        setAccount_name(objFmeg0sua.getId().getCdaliass());
        setClient_mnemonic("FINALHOLDERS");        
        setCountryOfDomicile("ES");
        setFund(objFmeg0sua.getId().getCdsubcta());
        setStatus_indicator("A");
        setCif_alt_type(objFmeg0sua.getId().getCdsubcta());
        setConfirmation_method("MANUAL");
        
        
      }
      
      else
      {

      tx.begin();

      this.objFmeg0sua = fmeg0suaBo.findById(pkFmeg0sua);
      // ----------------------------------------------------------------------------

      setTipoActualizacion(tipoAcualizacion);

      // ----------------------------------------------------------------------------

      Fmeg0suc objFmeg0suc;
      String pkFmeg0suc = objFmeg0sua.getId().getCdsubcta().trim();

      objFmeg0suc = fmeg0sucBo.findById(pkFmeg0suc);

      /* MCG 07/12/2009 */
      // ----------------------------------------------------------------------------

      try {

        List<Fmeg0shl> listFmeg0shl = fmeg0hslBo.findAllByIdCdsubcta(objFmeg0suc.getCdsubcta());

        if (listFmeg0shl.isEmpty()) {
          logger.warn("[obtenerDatos] no se han obtenido datos de titulares para la subcuenta: {}",
              objFmeg0suc.getCdsubcta());
        }
        else {
          setCif_alt_type(((Fmeg0shl) listFmeg0shl.get(0)).getCdholder().trim());
        }

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

      // ----------------------------------------------------------------------------

      setAccount_mnemonic(objFmeg0suc.getCdsubcta().trim());

      // ----------------------------------------------------------------------------

      setAccount_name(objFmeg0suc.getNbnameid().trim());

      // ----------------------------------------------------------------------------

      setClient_mnemonic(objFmeg0sua.getId().getCdaliass().trim());

      // ----------------------------------------------------------------------------
      try {

        setConfirmation_method("MANUAL");
        List<Fmeg0sot> list_sot = fmeg0sotBo.findAllByIdCdsubcta(objFmeg0suc.getCdsubcta().trim());

        if (list_sot.isEmpty()) {
          logger.warn("[obtenerDatos] no se ha obtenido registro fmeg0sot para la subcuenta: {}",
              objFmeg0suc.getCdsubcta());
        }
        else {
          int i = 0;
          boolean salir = false;

          while ((i++ < list_sot.size()) && !salir) {

            if ("AC".equals(((Fmeg0sot) list_sot.get(i - 1)).getTpidtype().trim())) {
              setConfirmation_method("");
              setFund(((Fmeg0sot) list_sot.get(i - 1)).getCdotheid().trim());
              salir = true;
            }

          }
        }

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      // ----------------------------------------------------------------------------

      if (!"D".equals(objFmeg0suc.getTpmodeid().trim())) {
        setStatus_indicator("A");
      }
      else {
        setStatus_indicator("I");
      }

      // ----------------------------------------------------------------------------

      // Obtención campos mifid
      try {

        // Se comprueba si se van a generar los campos
        final Tmct0cfg tmct0cfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
            SibbacEnums.Tmct0cfgConfig.ESTATICOS_CAMPOS_MIFID_FLAG.getProcess(),
            SibbacEnums.Tmct0cfgConfig.ESTATICOS_CAMPOS_MIFID_FLAG.getKeyname());

        final boolean envioCamposMifid = (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && "Y".equals(tmct0cfg
            .getKeyvalue()));
        setEnvioCamposMifid(envioCamposMifid);

        if (envioCamposMifid) {

          String mifidClientType = "";
          String mifidClientCode="";
          List<Tmct0tfi> tmct0tfiList = tmct0tfiBo.findAllByCdaliassSoloRepresentantes(objFmeg0sua.getId().getCdaliass().trim(), objFmeg0suc.getCdsubcta().trim());
          if (tmct0tfiList != null && tmct0tfiList.size() >0)
          {
            final Tmct0tfi tmct0tfi = tmct0tfiList.get(0);
            final String cdholder = tmct0tfi.getId().getCddclave();
            List<Tmct0fis> listtmct0fis = tmct0fisBo.findAllByCddclaveOrderNumsecdesc(cdholder);
            if (listtmct0fis == null || listtmct0fis.isEmpty()) {
              throw new Exception(String.format(
                  "No encontrado el titular  '%s' del alias '%s' dado de alta en el maestro de titulares (tmct0fis)",
                  cdholder,objFmeg0sua.getId().getCdaliass().trim()));
            }
            Tmct0fis tmct0fis = listtmct0fis.get(0);
            listtmct0fis.clear();
            String tpsocied = tmct0fis.getTpsocied();
            mifidClientType = ("F".equals(tpsocied))?"H": "L";
            mifidClientCode = tmct0fis.getIdMifid();            
            if (mifidClientCode != null)
            {
              mifidClientCode = mifidClientCode.trim();
            }
                
          }
          
          
          // MIFID_CLIENT_TYPE
          setMifidClientType(mifidClientType);       
          
          // MIFID_CLIENT_CODE
          setMifidClientCode(mifidClientCode);

        }

        // ----------------------------------------------------------------------------

      }
      catch (Exception e) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage());
        logger.trace(e.getMessage(),e);

      }

      tx.commit();

      /*
       * AML 2012/03/20 Se obtiene el nuctapro para a�adirlo al campo STRA
       */
      BigDecimal nuctaproN = utilities.getNuctaproSubAccount(objFmeg0sua.getId().getCdaliass(),
          objFmeg0suc.getCdsubcta(), objFmeg0suc.getCdbrocli());

      if (nuctaproN != null) {
        String nuctapro = nuctaproN.toString();
        for (int toprepend = 5 - nuctapro.length(); toprepend > 0; toprepend--) {
          nuctapro = "0" + nuctapro;
        }
        // AML 2012-12-14
        // No se actualiza stra
        // setStra("008" +nuctapro.toString());
      }
    }
    }
    catch (Exception e) {
      setMensajeError(e.getMessage());
      logger.warn("ERROR EN LA GENERACION DEL ARCHIVO XML");
      logger.warn("Traza: {}", e.getMessage());
      logger.trace(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {
      result = ctx.getBean(RegistroControl.class, "CLIENT ACCOUNT", getAccount_mnemonic(), getNombreCortoXML(), "I",
          "", "ClientAccount", "FI");

    }
    else {

      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());
      result = ctx.getBean(RegistroControl.class, "CLIENT ACCOUNT", getAccount_mnemonic(), getNombreCortoXML(), "E",
          getMensajeError(), "ClientAccount", "FI");

    }

    return result;

  }

  private Fmeg0ali getAlias(String cdaliass) {
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    Fmeg0ali fmeg0ali = null;
    try {
      // tx.setNontransactionalRead(true);
      tx.begin();
      fmeg0ali = fmeg0aliBo.findById(cdaliass);
      fmeg0ali.setTpmodeid("U");
      fmeg0aliBo.save(fmeg0ali, false);

      tx.commit();

    }
    catch (Exception e) {

      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);

    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }

      em.close();

    }
    return fmeg0ali;
  }

}