package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ClientGroupsMegara;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.bo.Fmeg0brcBo;
import sibbac.database.megbpdta.model.Fmeg0brc;

@Service
@Scope(value = "prototype")
public class BroClientClientGroup extends ClientGroupsMegara {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(BroClientClientGroup.class);

  private Fmeg0brc objFmeg0brc;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0brcBo fmeg0brcBo;

  public BroClientClientGroup(Fmeg0brc objFmeg0brc) {

    super();

    this.objFmeg0brc = objFmeg0brc;

  }

  public boolean execXML() {

    boolean result = true;

    try {

      // obtener los datos necesarios para rellenar los campos del archivo
      // XML
      obtenerDatos();

      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();

    }
    catch (Exception e) {
      logger.warn("ERROR EN EL PROCESO GENERACION DEL ARCHIVO CLIENT_GROUP");
      logger.warn(e.getMessage(), e);
      result = false;
    }

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    String pkFmeg0brc;

    pkFmeg0brc = objFmeg0brc.getCdbrocli();
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {

      tx.begin();

      this.objFmeg0brc = fmeg0brcBo.findById(pkFmeg0brc);

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0brc.getTpmodeid());

      // ----------------------------------------------------------------------------

      setClientgroup_mnemonic(objFmeg0brc.getCdbrocli().trim());

      // ----------------------------------------------------------------------------

      setClientgroup_name(objFmeg0brc.getDescript().trim());

      // ----------------------------------------------------------------------------
      tx.commit();

    }
    catch (Exception e) {

      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "CLIENT GROUP", getClientgroup_mnemonic(), getNombreCortoXML(), "I",
          "", "ClientGroups", "FI");
    }
    else {
      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());
      result = ctx.getBean(RegistroControl.class, "CLIENT GROUP", getClientgroup_mnemonic(), getNombreCortoXML(), "E",
          getMensajeError(), "ClientGroups", "FI");
    }

    return result;

  }

}