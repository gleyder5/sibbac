package sibbac.business.daemons.batch.model;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.BatchException;

class DirectStep extends AbstractStep {
	
	private static final Logger LOG = getLogger(DirectStep.class);
	
	private Query query;
	
	DirectStep(final Query query) {
		this.query = query;
	}

	@Override
  public boolean sendToMail() {
    return query.getType() == Query.TO_MAIL;
  }

  @Override
  public boolean sentToBuffer() {
    return query.getType() == Query.TO_BUFFER;
  }

  @Override
  public boolean readBuffer() {
    return false;
  }

  @Override
	void execute() throws BatchException {
		final StringBuilder sqlBuilder;
		
		LOG.debug("[execute] Inicio execucion directa del query [{}] con [{}] parametros", query.getId(),
				query.getParameters().size());
		sqlBuilder = new StringBuilder(query.getQuery());
		for(BatchParam actualParam : query.getParameters()) {
		  actualParam.autoFill(sqlBuilder);
		}
		executeStatement(sqlBuilder.toString(), query.getType());
	}
  
}
