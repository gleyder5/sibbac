package sibbac.business.daemons.fichero.corretajes.bo;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.fichero.corretajes.dao.FicheroCCDao;
import sibbac.business.daemons.fichero.corretajes.exceptions.EnvioFicheroCCException;
import sibbac.business.daemons.fichero.corretajes.model.FicheroCC;
import sibbac.business.daemons.fichero.corretajes.utils.FicheroCCNameKey;
import sibbac.business.daemons.fichero.corretajes.utils.FicheroCCNameKeyMap;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dto.EnvioCorretajeTmct0alcDataDTO;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;
import sibbac.database.bo.AbstractBo;
import sibbac.pti.PTICredentials;
import sibbac.pti.PTIException;
import sibbac.pti.PTIFileEnvironment;
import sibbac.pti.PTIFileExtension;
import sibbac.pti.PTIFileType;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.common.utils.PTIFileNameHelper;

@Service
public class FicheroCCBo extends AbstractBo<FicheroCC, Long, FicheroCCDao> {

  private static final Logger LOG = LoggerFactory.getLogger(FicheroCCBo.class);

  private static final String LIMIT_HOUR_FTL_D1 = "114500";
  private static final String LIMIT_HOUR_FINAL_DAY_FILE = "183000";

  @Value("${daemons.envio.fichero.corretaje.environment}")
  private PTIFileEnvironment environment;

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${sibbac20.script.dividen}")
  private Boolean sibbac20ScriptDividen;

  @Value("${daemons.envio.fichero.corretaje.out.path}")
  private String outPutPath;

  @Value("${daemons.envio.fichero.corretaje.destino}")
  private String destinoFichero;

  /** Número máximo de ejecuciones a procesar en una transacción. */
  @Value("${daemons.envio.fichero.corretaje.transaction.size}")
  private int transaction_size;

  /** Bussines object de configuracion en bbdd <code>Tmct0cfg</code> */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  /** Bussines object de configuracion en bbdd <code>Tmct0cfg</code> */
  @Autowired
  private Tmct0cfgDao tmct0cfgDao;

  @Autowired
  private Tmct0alcBo tmct0alcBo;

  @Autowired
  private FicheroCCSubList ficheroCCSubList;

  /** Bussines object de semaforos <code>Tmct0men</code> */
  @Autowired
  private Tmct0menBo tmct0menBo;

  /** Lista de días configurados como festivos. */
  private List<Date> holidaysConfigList = null;

  /** Lista de dias configurados como laborables. */
  private List<Integer> workDaysOfWeekConfigList = null;

  public FicheroCCNameKey getNewFicheroCCName(String origen, String destino, PTIFileType tipo) {
    LOG.trace("getNewFicheroCCName({}, {}, {})", destino, tipo, PTIFileExtension.TMP);
    String idFicheroCC = dao.getIdFicheroCcSequence() + "";
    LOG.trace("getNewFicheroCCName() Nueva secuencia para ID_FICHERO_CC {}", idFicheroCC);
    String fileName = PTIFileNameHelper.getNewFileName(environment, origen, destino, tipo, PTIFileExtension.TMP);
    LOG.trace("getNewFicheroCCName() Nueva nombre de fichero {}", fileName);
    FicheroCCNameKey name = new FicheroCCNameKey(fileName, idFicheroCC);
    LOG.trace("getNewFicheroCCName() == {}", name);
    return name;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void envioFicheroCC() {
    LOG.info("[envioFicheroCC] Inicio.");
    LOG.info("[envioFicheroCC] OutPut path {}", outPutPath);
    final Long startTime = System.currentTimeMillis();
    Tmct0men semaforo;
    boolean error = false;
    boolean incrementSentCount = false;

    try {

      LOG.trace("[envioFicheroCC] ## Bloqueando semaforo {} ...", TIPO_TAREA.TASK_DAEMONS_ENVIO_FICHERO_CC);
      semaforo = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_DAEMONS_ENVIO_FICHERO_CC, TMCT0MSC.LISTO_GENERAR,
                                         TMCT0MSC.EN_EJECUCION);

      Map<FicheroCCNameKeyMap, FicheroCCNameKey> fileNames = new HashMap<FicheroCCNameKeyMap, FicheroCCNameKey>();
      try {

        if (PTIMessageFactory.getCredentials() == null) {
          initPTICredentials();
        }

        Date now = new Date();

        int daysDescontarLiquidacion = 1;
        int daysDescontarContratacion = 1;

        PTIFileType tipoInformacion = PTIFileType.CC;
        String hourLimitSendFtlD1Cfg = getLimitHourSendFtlD1FromCfg();

        String hourLimitSendFinalDayFileCfg = getLimitHourSendFinalDayFileFromCfg();
        Date hourLimitSendFtlD1 = FormatDataUtils.convertStringToDate(hourLimitSendFtlD1Cfg,
                                                                      FormatDataUtils.TIME_FORMAT);

        Date hourLimitSendFinalDayFile = FormatDataUtils.convertStringToDate(hourLimitSendFinalDayFileCfg,
                                                                             FormatDataUtils.TIME_FORMAT);

        if (FormatDataUtils.compareTimeOnly(now, hourLimitSendFtlD1) > 0) {
          // han pasado las 12 del medio dia
          daysDescontarLiquidacion += 1;
        } else {
          // no han pasado las 12 del medio dia. si no han pasado
        }

        if (FormatDataUtils.compareTimeOnly(now, hourLimitSendFinalDayFile) > 0) {
          // End of day file
          tipoInformacion = PTIFileType.CCFD;
          daysDescontarContratacion = 0;
        } else {
        }

        LOG.info("[envioFicheroCC] Tipo informacion a enviar {} ", tipoInformacion);

        LOG.info("[envioFicheroCC] Se va a recuperar operaciones desde FTL - {} to D - {} ...",
                 daysDescontarLiquidacion, daysDescontarContratacion);

        Date toFechaLiquidacion = DateHelper.getNextWorkDate(now, daysDescontarLiquidacion, getWorkDaysOfWeek(),
                                                             getHolidays());

        Date toFechaContratacion = DateHelper.getPreviousWorkDate(now, daysDescontarContratacion, getWorkDaysOfWeek(),
                                                                  getHolidays());

        LOG.info("[envioFicheroCC] Buscando operaciones con fecha de liquidacion menor o igual a {} y fecha de contratacion menor o igual a {} ...",
                 toFechaLiquidacion, toFechaContratacion);

        int posIni = 0;
        int posEnd = transaction_size;

        List<EnvioCorretajeTmct0alcDataDTO> alcs = tmct0alcBo.getEnvioCorretajeTmct0alcSinEnviar(toFechaContratacion,
                                                                                                 toFechaLiquidacion);
        if (!alcs.isEmpty()) {

          if (transaction_size > alcs.size()) {
            posEnd = alcs.size();
          }
          List<EnvioCorretajeTmct0alcDataDTO> alcsSubList;
          while (posIni < posEnd) {
            LOG.trace("[envioFicheroCC] ## posIni == {} posEnd == {} total == {}", posIni, posEnd, alcs.size());
            alcsSubList = alcs.subList(posIni, posEnd);
            try {

              ficheroCCSubList.execute(alcsSubList, outPutPath, fileNames, environment, tipoInformacion, destinoFichero);
              incrementSentCount = true;
            } catch (Exception e) {
              LOG.warn("Incidencia enviando el fichero CC", e);
              LOG.trace("Incidencia con la sublista {}", alcsSubList);
              if (transaction_size > 1) {
                LOG.trace("[envioFicheroCC] ## El bloque de tamaño {} ha fallado voy a intentarlo de 1 en 1.",
                          transaction_size);
                executeAfterBlockException(alcsSubList, fileNames, tipoInformacion);
                // executeCaseAfterBlockException(ejecucionesSubList, cases);
              }
            } // catch

            posIni = posEnd;
            if (alcs.size() < posIni + transaction_size) {
              posEnd = alcs.size();
            } else {
              posEnd += transaction_size;
            }
          }

        }
      } catch (Exception e) {
        LOG.warn("[envioFicheroCC] ", e);
        error = true;
      } finally {
        if (!fileNames.isEmpty()) {
          try {
            this.renameFilesEnvio(fileNames);
          } catch (Exception e) {
            error = true;
            LOG.warn("[envioFicheroCC] ## Incidencia renombrando los ficheros para el envio.", e);
          }
          fileNames.clear();
        }
      }

      if (error) {
        LOG.info("[envioFicheroCC] ## Cambiando estado a TERMINACION NO CORRECTA ...");
        tmct0menBo.putEstadoMEN(semaforo, TMCT0MSC.EN_ERROR);
      } else {
        LOG.info("[envioFicheroCC] ## Cambiando estado a TMCT0MSC.LISTO_GENERAR ...");
        if (incrementSentCount) {
          tmct0menBo.putEstadoMENAndIncrement(semaforo, TMCT0MSC.LISTO_GENERAR);
        } else {
          tmct0menBo.putEstadoMEN(semaforo, TMCT0MSC.LISTO_GENERAR);
        }
      }
    } catch (SIBBACBusinessException e) {
      LOG.warn("[envioFicheroCC] No se puede bloquear el semaforo ## {} ## {}",
               TIPO_TAREA.TASK_DAEMONS_ENVIO_FICHERO_CC, e.getMessage());
    } catch (Exception e) {
      LOG.warn("[envioFicheroCC] ", e);
    } finally {

      final Long endTime = System.currentTimeMillis();
      LOG.info("[envioFicheroCC] Finalizada la tarea de envio fichero CC ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
               + "]");
    } // finally
  }

  private void renameFilesEnvio(Map<FicheroCCNameKeyMap, FicheroCCNameKey> fileNames) throws EnvioFicheroCCException {
    LOG.trace("[renameFilesEnvio] ## Inicio.");
    File file;
    File path = new File(outPutPath);
    for (FicheroCCNameKey fileName : fileNames.values()) {
      file = new File(outPutPath, fileName.getFileName());
      try {
        if (file.exists()) {
          if (file.length() > 0) {
            if (file.getName().endsWith(PTIFileExtension.TMP.toString())) {
              PTIFileNameHelper.changeFileToSendExtension(path, file, PTIFileExtension.DAT);
            }
          } else {
            LOG.warn("renameFilesEnvio() ## Incidencia fichero borrado {}", file.getAbsolutePath());
            file.delete();
          }
        }
      } catch (Exception e) {
        throw new EnvioFicheroCCException("renameFilesEnvio() ## Incidencia al renombrar el fichero " + fileName, e);
      }

    }// fin for
    LOG.trace("[renameFilesEnvio] ## Fin.");
  }

  /**
   * Metodo que ejecuta el envio fichero cc despues de haber ocurrido una excepcion en un bloque, el nuevo bloque
   * transaccional es de 1 en 1
   * 
   */
  @Transactional
  private void executeAfterBlockException(List<EnvioCorretajeTmct0alcDataDTO> alcs,
                                          Map<FicheroCCNameKeyMap, FicheroCCNameKey> fileNames,
                                          PTIFileType tipoInformacion) {
    LOG.debug("[executeAfterBlockException] ## Inicio");
    int countErroresEnTransacion = 0;
    List<EnvioCorretajeTmct0alcDataDTO> alcsSubList;
    for (int i = 0; i < alcs.size(); i++) {
      if (countErroresEnTransacion < transaction_size / 10) {
        alcsSubList = alcs.subList(i, i + 1);
        try {
          this.ficheroCCSubList.execute(alcsSubList, outPutPath, fileNames, environment, tipoInformacion, destinoFichero);
        } catch (Exception e) {
          LOG.warn("[executeAfterBlockException]", e);
          countErroresEnTransacion++;
        }
      } else {
        LOG.warn("[executeAfterBlockException] ## Se han producido demasiados errores, abortamos el bloque.");
        break;
      }
    }
    LOG.debug("[executeAfterBlockException] ## Fin");
  }

  private String getLimitHourSendFtlD1FromCfg() {
    Tmct0cfgConfig cfg = Tmct0cfgConfig.PTI_ENVIO_FICHERO_CC_FTL_D1_LIMIT_HOUR;
    try {
      Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                                    cfg.getProcess(), cfg.getKeyname());
      return datosConfiguracion.getKeyvalue().trim();
    } catch (Exception e) {
      LOG.warn("[getLimitHourSendFtlD1FromCfg] ## Problema al recuperar la tmct0cfg." + cfg, e);
    }
    return LIMIT_HOUR_FTL_D1;
  }

  private String getLimitHourSendFinalDayFileFromCfg() {
    Tmct0cfgConfig cfg = Tmct0cfgConfig.PTI_ENVIO_FICHERO_CC_FINAL_DAY_FILE_LIMIT_HOUR;
    try {
      Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                                    cfg.getProcess(), cfg.getKeyname());
      return datosConfiguracion.getKeyvalue().trim();
    } catch (Exception e) {
      LOG.warn("[getLimitHourSendFinalDayFileFromCfg] ## Problema al recuperar la tmct0cfg." + cfg, e);
    }
    return LIMIT_HOUR_FINAL_DAY_FILE;
  }

  public void initPTICredentials() throws PTIException {

    LOG.info("[initPTICredentials] ## Loading PTI Credentials...");
    PTICredentials pti;
    try {
      pti = new PTICredentials();
      Tmct0cfg origen = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                        PTICredentials.KEY_PROCESS,
                                                                        PTICredentials.KEY_ORIGEN);
      Tmct0cfg usuarioOrigen = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                               PTICredentials.KEY_PROCESS,
                                                                               PTICredentials.KEY_USUARIO_ORIGEN);
      Tmct0cfg destino = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                         PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_DESTINO);
      Tmct0cfg usurioDestino = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                               PTICredentials.KEY_PROCESS,
                                                                               PTICredentials.KEY_USUARIO_DESTINO);
      Tmct0cfg miembro = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                         PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_MIEMBRO);

      Tmct0cfg usuarioMiembro = null;

      if (sibbac20ScriptDividen) {
        usuarioMiembro = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                         PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_USUARIO_MIEMBRO_SCRIPT_DIVIDEND);
      } else {
        usuarioMiembro = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                         PTICredentials.KEY_PROCESS,
                                                                         PTICredentials.KEY_USUARIO_MIEMBRO_NORMAL);
      }
      pti.setOrigen(origen.getKeyvalue());
      pti.setUsuarioOrigen(usuarioOrigen.getKeyvalue());
      pti.setDestino(destino.getKeyvalue());
      pti.setUsuarioDestino(usurioDestino.getKeyvalue());
      pti.setMiembro(miembro.getKeyvalue());
      pti.setUsuarioMiembro(usuarioMiembro.getKeyvalue());
      PTIMessageFactory.setCredentials(pti);
      LOG.info("[initPTICredentials] ## PTI Credentials loaded {}", pti);
    } catch (Exception e) {
      LOG.warn("[initPTICredentials] ## Incidencia al inicializar las credenciales de pti ", e);
      throw new PTIException(e);
    }

  }

  /**
   * Obtiene una lista con los días de vacaciones configurados en la tabla tmct0cfg.
   * 
   * @return <code>List<Date> - </code>Lista de días de vacaciones.
   */
  private List<Date> getHolidays() {
    if (holidaysConfigList == null) {
      // Días de vacaciones configurados
      List<Tmct0cfg> configList = tmct0cfgDao.findByAplicationAndProcessAndLikeKeyname(sibbac20ApplicationName,
                                                                                       "CONFIG", "holydays.anual.days%");
      if (configList != null && !configList.isEmpty()) {
        holidaysConfigList = new ArrayList<Date>(configList.size());

        for (Tmct0cfg tmct0cfg : configList) {
          holidaysConfigList.add(FormatDataUtils.convertStringToDate(tmct0cfg.getKeyvalue(), "yyyy-MM-dd"));
        } // for
      } else {
        holidaysConfigList = new ArrayList<Date>();
      } // else
    } // if

    return holidaysConfigList;
  } // getHolidays

  /**
   * Obtiene una lista con los días de laborables configurados en la tabla tmct0cfg.
   * 
   * @return <code>List<Integer> - </code>Lista de días de la semana laborables.
   */
  private List<Integer> getWorkDaysOfWeek() {
    if (workDaysOfWeekConfigList == null) {
      // Días de la semana laborables configurados
      Tmct0cfg tmct0cfg = tmct0cfgDao.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName, "CONFIG",
                                                                           "week.work.days");

      if (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && !tmct0cfg.getKeyvalue().isEmpty()) {
        String[] workDays = tmct0cfg.getKeyvalue().split(",");
        workDaysOfWeekConfigList = new ArrayList<Integer>(workDays.length);

        for (String workDay : workDays) {
          workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
        } // for
      } else {
        workDaysOfWeekConfigList = new ArrayList<Integer>();
      } // else
    } // if

    return workDaysOfWeekConfigList;
  } // getWorkDaysOfWeek

}
