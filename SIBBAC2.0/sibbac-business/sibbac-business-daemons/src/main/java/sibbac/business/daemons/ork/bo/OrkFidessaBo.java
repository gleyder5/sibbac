package sibbac.business.daemons.ork.bo;

import java.io.File;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.common.SIBBACBusinessException;

/**
 * Objeto de reglas de negocio para la generacion de los ficheros Order Record Keepint (ORK)
 * que tienen dependencia con la funcionalidad Batch
 */
public interface OrkFidessaBo {
	
	/**
	 * Ejecuta un proceso Batch para la lectura de información de Fidessa para la generación
	 * de los ficheros ORK
	 * @param config Configuración del proceso ORL
	 * @param batch Un objeto Batch completamente configurado y operaciona.
	 * @throws BatchException En caso de error en la operación del batch
	 * @throws SIBBACBusinessException En caso de violación de regla de negocio.
	 */
	void lecturaFidessa(OrkMercadosConfig config, Batch batch, File origen) throws BatchException, 
			SIBBACBusinessException;
	
	/**
	 * Verifica si hay entidades OrkFidessa pendientes de ser tratados
	 * @return verdadero en caso de existan entidades OrkFidessa sin tratar
	 * @throws SIBBACBusinessException 
	 */
	boolean existenSinTratar() throws SIBBACBusinessException;

	/**
	 * A partir de un fichero CSV se actualizan las entidades OrkFidessa que han sido enviadas
	 * o presentan un error.
	 * @param config Configuración de mercados.
	 * @param updateInfoFile Fichero con formato CSB
	 * @throws SIBBACBusinessException
	 */
  void updateBatch(Batch batch, File updateInfoFile, int updateTimeOut) throws SIBBACBusinessException;
  
  OrkFidessaFieldCollection getFieldCollection();

	
}
