package sibbac.business.daemons.batch.model;

import java.util.List;
import static java.text.MessageFormat.format;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.BatchException;

class SameBufferStep extends BufferedStep {
	
	private static final Logger LOG = getLogger(SameBufferStep.class);
	
	private final List<Query> queries;
	
	private Query actualQuery;
	
	private String queryId;
	
	SameBufferStep(final List<Query> queries) {
		this.queries = queries;
	}

  @Override
  public boolean sendToMail() {
    for(Query query : queries) {
      if(query.getType() == Query.TO_MAIL) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean sentToBuffer() {
    for(Query query : queries) {
      if(query.getType() == Query.TO_BUFFER) {
        return true;
      }
    }
    return false;
  }

  @Override 
  void execute() throws BatchException {
    for(final Query actualQuery : queries) {
      this.actualQuery = actualQuery;
      queryId = actualQuery.getId().toString();
      super.execute();
    }
  }
	
	@Override
	void execute(String values[]) throws BatchException {
		final String message;
		final StringBuilder sqlBuilder;
		
		sqlBuilder = new StringBuilder(actualQuery.getQuery());
	  try {
      fillParameters(sqlBuilder, actualQuery.getParameters(), values);
      executeStatement(sqlBuilder.toString(), actualQuery.getType());
    }
		catch(BatchException sqlex) {
			message = format("Error al ejecutar sentencia SQL del query [{0}]", queryId);
			LOG.error("[execute] {}", message);
			throw new BatchException(message, sqlex);
		}
	}
	
}