package sibbac.business.daemons.ork.bo;

import java.io.File;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import static java.text.MessageFormat.format;
import static java.lang.Thread.currentThread;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.Executors.newSingleThreadExecutor;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.BatchProcessor;
import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.business.daemons.ork.dao.OrkFidessaDao;
import sibbac.business.daemons.ork.dao.OrkFidessaFieldDao;
import sibbac.common.SIBBACBusinessException;

@Service
public class OrkFidessaBoImpl implements OrkFidessaBo {
	
	private static final Logger LOG = getLogger(OrkFidessaBoImpl.class);
	
	private OrkFidessaDao orkFidessaDao;
	
	private OrkFidessaFieldDao orkFidessaFieldDao;
	
	private ApplicationContext ctx;
	
	@Override
	public void lecturaFidessa(OrkMercadosConfig config, Batch batch, File origen) throws BatchException, 
			SIBBACBusinessException {
		final int modificados;
		
		LOG.trace("[lecturaFidessa] Inicio...");
		batch.setMaxTime(config.getLecturaTimeout());
		try {
		  modificados = executeBatch(batch, origen, config.getBatchTempPath(), config.getLecturaTimeout());
			LOG.info("Fin de lectura fidesa insertando {} registros", modificados);
			config.moveFidessaFileToTratados(origen);
		} 
		catch (InterruptedException e) {
			throw new SIBBACBusinessException("Se ha interrumpido el batch de lectura de fidessa", e);
		}
		catch(ExecutionException e) {
			LOG.error("Error de ejecucion de batch de lectura fidessa", e.getCause());
			throw new SIBBACBusinessException("Error lectura fidessa", e.getCause());
		}
	}
	
	@Override
	public boolean existenSinTratar() throws SIBBACBusinessException {
		return orkFidessaDao.existenSinTratar();
	}

	@Override
  public void updateBatch(Batch batch, File updateInfoFile, int timeout) throws SIBBACBusinessException {
    final int actualizados;
    
    try {
      actualizados = executeBatch(batch, updateInfoFile, updateInfoFile.getParentFile(), timeout);
      updateInfoFile.delete();
      LOG.info("[updateBatch] Finalizado, habiendo actualizado {} registros", actualizados);
    } 
    catch (InterruptedException e) {
      throw new SIBBACBusinessException("Se ha interrumpido el batch de actualización de fidessa", e);
    }
    catch(ExecutionException e) {
      LOG.error("Error de ejecucion de batch de actualización fidessa", e.getCause());
      throw new SIBBACBusinessException("Error actualización fidessa", e.getCause());
    }
  }

	@Override
	public OrkFidessaFieldCollection getFieldCollection() {
	  return new OrkFidessaFieldCollection(orkFidessaFieldDao.findFixedFields(), orkFidessaFieldDao.findDynamicFields());
	}
	
	@Autowired
	void setOrkFidessaFieldDao(OrkFidessaFieldDao orkDynafieldDao) {
		this.orkFidessaFieldDao = orkDynafieldDao;
	}
	
	@Autowired
	void setOrkFidessaDao(OrkFidessaDao orkFidessaDao) {
		this.orkFidessaDao = orkFidessaDao;
	}
	 
	@Autowired
	void setApplicationContext(ApplicationContext ctx) {
		this.ctx = ctx;
	}
	
	private int executeBatch(Batch batch, File origen, File tempDir, int timeOut) throws InterruptedException,
	    ExecutionException {
    final BatchProcessor batchProcessor;
    final ExecutorService executor;
    final Future<Integer> future;
    final int modificados;
    final String threadName;

    LOG.info("[executeBatch] Inicio...");
    batch.setMaxTime(timeOut);
    batchProcessor = ctx.getBean(BatchProcessor.class);
    batchProcessor.setBatch(batch);
    batchProcessor.setOrigen(origen);
    batchProcessor.setWorkdirectory(tempDir);
    threadName = format("{0}_Batch",currentThread().getName()) ;
    executor = newSingleThreadExecutor(new SingleThreadFactory(threadName));
    future = executor.submit(batchProcessor);
    batchProcessor.setFuture(future);
    executor.shutdown();
    executor.awaitTermination(timeOut, MINUTES);
    modificados = future.get();
    LOG.info("[executeBatch] Fin modificando {} registros", modificados);
    return modificados;
	}
  

}
