/**
 * # LoadConfigurationException.java
 * # Fecha de creación: 02/03/2016
 */
package sibbac.business.daemons.exceptions;

/**
 * Se lanza si ocurre algún error al cargar la configuración.
 * 
 * @author XI316153
 * @version 1.0
 * @since SIBBAC2.0
 */
public class LoadConfigurationException extends Exception {

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTANTES ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serialización de objetos. */
  private static final long serialVersionUID = -3663021150693493739L;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTORES ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Constructor I. Genera una nueva excepción con el mensaje especificado.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   */
  public LoadConfigurationException(String mensaje) {
    super(mensaje);
  } // LoadConfigurationException

  /**
   * Constructor II. Genera una nueva excepción con el mensaje especificado y la excepción producida.
   * 
   * @param mensaje Mensaje a mostrar cuando se lanza la excepción.
   * @param causa Causa por la que se ha lanzado la excepción.
   */
  public LoadConfigurationException(String mensaje, Throwable causa) {
    super(mensaje, causa);
  } // LoadConfigurationException

} // LoadConfigurationException
