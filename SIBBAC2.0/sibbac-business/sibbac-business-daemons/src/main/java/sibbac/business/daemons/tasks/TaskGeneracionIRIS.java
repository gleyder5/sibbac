package sibbac.business.daemons.tasks;

import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.daemons.fichero.iris.bo.GenerateIRISFile;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_ENVIO_FICHEROS_IRIS, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 21 ? * MON-FRI")
public class TaskGeneracionIRIS extends WrapperTaskConcurrencyPrevent {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOGGGER = LoggerFactory.getLogger(TaskGeneracionIRIS.class);

  /**
   * Tamaño de la página para
   */
  @Value("${daemons.envio.fichero.iris.db.page.size:1000}")
  private int dbPageSize;

  /**
   * Ruta donde se deposita el fichero
   */
  @Value("${daemons.envio.fichero.iris.out.path:/sbrmv/ficheros/iris/out}")
  private String outPutDir;

  /**
   * Nombre del fichero generado por el proceso
   */
  @Value("${daemons.envio.fichero.iris.file:FGLB0CON.txt}")
  private String fileName;

  /**
   * Generador de Fichero IRIS
   */
  @Autowired
  private GenerateIRISFile irisFileGenerator;

  @Override
  public void executeTask() throws SIBBACBusinessException {
    try {
      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.debug("Se inicia la tarea");
      }

      /**
       * Seteamos el tamaño de las páginas
       */
      irisFileGenerator.setDbPageSize(dbPageSize);
      
      /**
       * Reiniciamos los contadores del fichero
       */
      irisFileGenerator.resetCounters();
      
      /**
       * Seteamos la fecha actual por si se alarga la tarea y se sigue ejecutando en el día siguiente
       */
      //final Date currentDate = FormatDataUtils.convertStringToDate("2018-02-12", FormatDataUtils.DATE_FORMAT_1);
      final Date currentDate = new Date();
      
      irisFileGenerator.setCurrentDate(currentDate);
      
      /**
       * Seteamos la ruta y el fichero de salida
       */
      irisFileGenerator.generateFile(outPutDir, fileName);

      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.info("Se termina la tarea");
      }
    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(e);
    }
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_IRIS;
  }

}
