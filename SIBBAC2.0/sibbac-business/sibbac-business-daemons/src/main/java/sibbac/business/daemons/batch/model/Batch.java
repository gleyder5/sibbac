package sibbac.business.daemons.batch.model;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.Serializable;
import static java.util.Objects.requireNonNull;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.JoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.DirectTypeEnum;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;

@Entity
@Table(name = "TMCT0_BATCH")
public class Batch implements Serializable {

  private static final long serialVersionUID = -8037029628966197936L;
  
  private static final Logger LOG = getLogger(Batch.class);

  @Id
  @Column(name = "CODE", length = 5)
  private String code;
  
  @ManyToOne
  @JoinColumn(name = "ID_REGLASPERIODICIDAD")
  private ReglaPeriodicidad periodicidad;
  
  @Column(name = "DESCRIPTION")
  private String description;
  
  /**
   * Fecha a partir de la cual la tarea estará activa
   */
  @Column(name = "STARTS")
  @Temporal(TemporalType.DATE)
  private Date starts;
  
  /**
   * Lista de direcciones de correo, separados por coma, a donde se enviarán los correos electronicos en el caso
   * de los queries tipo 'M'
   */
  @Column(name = "RECEPTORS", length = 200)
  private String receptors;
  
  /**
   * Asunto del correo electrónico a generar en los queries tipo 'M'
   */
  @Column(name = "SUBJECT", length = 200)
  private String subject;
  
  /**
   * Número máximo de correos electronicos enviados por una Query
   */
  @Column(name = "MAX_MAILS")
  private int maxMails;
  
  /**
   * Tiempo máximo de ejecución de la tarea en segundos
   */
  @Column(name = "MAX_TIME")
  private int maxTime;
  
  @Column(name = "ACTIVE")
  private boolean active;
  
  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name = "CODE", referencedColumnName = "CODE")
  private List<Query> queries;
  
  @Transient
  private List<Step> steps;
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setPeriodicidad(ReglaPeriodicidad periodicidad) {
    this.periodicidad = periodicidad;
  }
  
  public ReglaPeriodicidad getPeriodicidad() {
    return periodicidad;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getDescription() {
    return description;
  }
  
  public void setStarts(Date starts) {
    this.starts = starts;
  }
  
  public Date getStarts() {
    return starts;
  }
  
  public String getReceptors() {
    return receptors;
  }

  public boolean isActive() {
    return active;
  }

  public void setReceptors(String receptors) {
    this.receptors = receptors;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public List<Query> getQueries() {
    return queries;
  }
  
  public void setQueries(List<Query> queries) {
    this.queries = queries;
  }
  
  public void setMaxMails(int maxMails) {
    this.maxMails = maxMails;
  }
  
  public int geMaxMails() {
    return maxMails;
  }
  
  public void setMaxTime(int maxTime) {
    this.maxTime = maxTime;
  }
  
  public int getMaxTime() {
    return maxTime;
  }
  
  public String getSubject() {
    return subject;
  }
  
  public void setSubject(String subject) {
    this.subject = subject;
  }
  
  @Transient
  public Iterable<Step> getSteps() {
  	return steps;
  }
  
  @Transient
	public List<String> receptorList() {
	  final String[] split;
	  
	  if(receptors == null || receptors.isEmpty()) {
	    return Collections.emptyList();
	  }
	  split = receptors.split(";");
	  return Arrays.asList(split);
	}

  @PostLoad
  public void loadSteps() {
    final Queue<Query> queries;
    Query currentQuery, nextQuery;
    steps = new ArrayList<>();
    
    queries = orderedQueries();
    if(queries == null) {
      steps = Collections.emptyList();
      return;
    }
    while((currentQuery = queries.poll()) != null) {
      switch(currentQuery.getDirectType()) {
        case DIRECT:
          steps.add(new DirectStep(currentQuery));
          break;
        case FROM_BUFFER:
          nextQuery = queries.peek();
          if(nextQuery == null) {
            steps.add(new SimpleStep(currentQuery));
          }
          else {
            switch(nextQuery.getDirectType()) {
              case DIRECT: case FROM_BUFFER:
                steps.add(new SimpleStep(currentQuery));
                break;
              case IN_SAME_LINE:
                steps.add(new SameLineStep(loadSameKind(currentQuery, queries, DirectTypeEnum.IN_SAME_LINE)));
                break;
              case IN_SAME_BUFFER:
                steps.add(new SameBufferStep(loadSameKind(currentQuery, queries, DirectTypeEnum.IN_SAME_BUFFER)));
            }
          }
          break;
        case IN_SAME_LINE: case IN_SAME_BUFFER:
          LOG.info("Se ha encontrado una query de continuacion cuando se esperaba una de inicio: {}", 
              currentQuery.getId().toString());
          throw new IllegalAccessError("Error en la definición de la lista de queries");
      }
    }
  }
  
  private List<Query> loadSameKind(Query currentQuery, Queue<Query> queries, DirectTypeEnum type) {
    final List<Query> tempList;
    Query nextQuery;
    
    tempList = new ArrayList<>();
    tempList.add(currentQuery);
    do {
      tempList.add(queries.poll());
    } 
    while((nextQuery = queries.peek()) != null 
        && nextQuery.getDirectType() == type);
    return tempList;
  }

  private Queue<Query> orderedQueries() {
    final List<Query> filtered;
    int lastOrder = -1, actualOrder, parameterSize;
    
    filtered = new ArrayList<>();
    for(Query q : queries) {
      if(q.isActive()) {
      	parameterSize = requireNonNull(q.getParameters(), "Query con lista de parametros nula").size();
        filtered.add(q);
        LOG.debug("[orderedQueries] cargado query {} con {} parametros", q.getId().toString(), parameterSize);
      }
    }
    if(filtered.isEmpty()) {
      LOG.warn("El Batch {} tienen la lista de queries vacía", code);
      return null;
    }
    Collections.sort(filtered);
    for(Query q : filtered) {
      actualOrder = requireNonNull(q.getId(), "Llave primaria de query nula").getOrder();
      if(actualOrder < 0) {
        LOG.warn("Query con orden negativo: {}", q.getId().toString());
        return null;
      }
      if(lastOrder == actualOrder) {
        LOG.warn("Query con orden repetido: {}", actualOrder);
        return null;
      }
      lastOrder = actualOrder; 
    }
    return new ArrayDeque<>(filtered);
  }

}
