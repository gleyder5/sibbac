package sibbac.business.estaticos.utils.registrocontrol;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.megbpdta.dao.Fmeg0fieDao;
import sibbac.database.megbpdta.model.Fmeg0fie;
import sibbac.database.megbpdta.model.Fmeg0fieId;

/**
 * RegistroControl.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
@Service
@Scope(value = "prototype")
public class RegistroControl {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(RegistroControl.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The nominter. */
  private String nominter;

  /** The cdindeti. */
  private String cdindeti;

  /** The nomxml. */
  private String nomxml;

  /** The estafich. */
  private String estafich;

  /** The nomensa. */
  private String nomensa;

  /** The nompgm. */
  private String nompgm;

  /** The tpficher. */
  private String tpficher;

  @Autowired
  private Fmeg0fieDao fmeg0fieDao;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new registro control.
   * 
   * Inserte un registro en el fichero de control FMEG0FIE.
   * 
   */
  public RegistroControl() {

  }

  /**
   * Instantiates a new registro control.
   * 
   * Inserte un registro en el fichero de control FMEG0FIE.
   * 
   * @param nominter the nominter
   * @param cdindeti the cdindeti
   * @param nomxml the nomxml
   * @param estafich the estafich
   * @param nomensa the nomensa
   * @param nompgm the nompgm
   * @param tpficher the tpficher
   */
  public RegistroControl(String nominter, String cdindeti, String nomxml, String estafich, String nomensa,
      String nompgm, String tpficher) {
    logger.debug("registroControl: nuevo registro de control");

    this.setData(nominter, cdindeti, nomxml, estafich, nomensa, nompgm, tpficher);

    logger.debug("registroControl: datos registro de control completado");
  }

  public RegistroControl(Fmeg0fie data) {
    this(data.getNominter(), data.getCdindeti(), data.getId().getNomxml(), data.getEstafich(), data.getNomensa(), data
        .getNompgm(), data.getTpficher());

    logger.debug("registroControl: datos registro de control completado");
  }

  private void setData(Fmeg0fie data) {
    this.setData(data.getNominter(), data.getCdindeti(), data.getId().getNomxml(), data.getEstafich(),
        data.getNomensa(), data.getNompgm(), data.getTpficher());
  }

  private void setData(String nominter, String cdindeti, String nomxml, String estafich, String nomensa, String nompgm,
      String tpficher) {
    if (nominter.length() > 30) {
      this.nominter = nominter.substring(0, 30);
    }
    else {
      this.nominter = nominter;
    }

    if (cdindeti.length() > 40) {
      this.cdindeti = cdindeti.substring(0, 40);
    }
    else {
      this.cdindeti = cdindeti;
    }

    if (nomxml.length() > 15) {
      this.nomxml = nomxml.substring(0, 15);
    }
    else {
      this.nomxml = nomxml;
    }

    this.estafich = estafich;

    if (nomensa.length() > 100) {
      this.nomensa = nomensa.substring(0, 100);
    }
    else {
      this.nomensa = nomensa;
    }

    if (nompgm.length() > 20) {
      this.nompgm = nompgm.substring(0, 20);
    }
    else {
      this.nompgm = nompgm;
    }

    if (tpficher.length() > 2) {
      this.tpficher = tpficher.substring(0, 2);
    }
    else {
      this.tpficher = tpficher;
    }
  }

  public void loadDataFromNomxml(String nomxml) throws SIBBACBusinessException {
    List<Fmeg0fie> list = fmeg0fieDao.findAllByIdNomxmlOrderByFecprocDescAndHorprocDesc(nomxml);
    if (list.size() > 0) {
      this.setData(list.get(0));
    }
    else {
      throw new SIBBACBusinessException(String.format("No encontrado registro control para xml %s", nomxml));
    }
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------
  @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void writeNewTransaction() {
    this.write();
  }

  /**
   * Write.
   */
  @Transactional
  public void write() {

    logger.debug("registroControl: nuevo registro de control");

    logger.debug("registroControl: inicio obtencion datos");

    Fmeg0fie objFmeg0fie = new Fmeg0fie();
    objFmeg0fie.setId(new Fmeg0fieId("", BigDecimal.ZERO, BigDecimal.ZERO));
    Date fecha = new Date();

    try {

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      String fecproc = fechaFormato.format(fecha);

      DateFormat horaFormato = new SimpleDateFormat("HHmmssSSS");
      String horproc = horaFormato.format(fecha);

      DateFormat horaFormatoAct = new SimpleDateFormat("HHmmss");
      String horactu = horaFormatoAct.format(fecha);

      objFmeg0fie.getId().setFecproc(new BigDecimal(fecproc));
      objFmeg0fie.getId().setHorproc(new BigDecimal(horproc));
      objFmeg0fie.setNominter(nominter);
      objFmeg0fie.setCdindeti(cdindeti);
      objFmeg0fie.getId().setNomxml(nomxml);
      objFmeg0fie.setEstafich(estafich);

      /* AML 06-10-2010 */
      /*
       * Si se produce un error: estafich == "E", se realiza llamada a un
       * comando del sistema informando del error
       */

      /* AML 11-07-2011 */
      // Se parametriza en el fichero parametrozEjecuci�n.properties el listado
      // de usuarios del AS400 a los que enviar
      // el mensaje de error

      objFmeg0fie.setNomensa(nomensa);
      objFmeg0fie.setNompgm(nompgm);
      objFmeg0fie.setTpficher(tpficher);

      objFmeg0fie.setNomusua("SIBBAC20");
      objFmeg0fie.setFecactu(new BigDecimal(fecproc));
      objFmeg0fie.setHoractu(new BigDecimal(horactu));

      logger.debug("registroControl: inicio insert");

      fmeg0fieDao.save(objFmeg0fie);

      logger
          .debug(
              "registroControl: insert completado registroControl: {}, Identificador: {}, Nombre XML: {}, Estado fichero: {}, Mensaje: {}, Tipo fichero: {}",
              nominter, cdindeti, nomxml, estafich, nomensa, tpficher);

    }
    catch (ConcurrentModificationException e) {

      logger.warn("registroControl: error insert, reintento de insercion");

      this.write();

    }

    catch (Exception e) {

      logger.warn("registroControl: error insert");
      logger
          .warn(
              "registroControl: insert completado registroControl: {}, Identificador: {}, Nombre XML: {}, Estado fichero: {}, Mensaje: {}, Tipo fichero: {}",
              nominter, cdindeti, nomxml, estafich, nomensa, tpficher);

      logger.warn("registroControl: {}", e.getMessage(), e);

    }
    finally {

    }
  }

  /**
   * Sets the cdindeti.
   * 
   * @param cdindeti the new cdindeti
   */
  public void setCdindeti(String cdindeti) {
    this.cdindeti = cdindeti;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the estafich.
   * 
   * @return the estafich
   */
  public String getEstafich() {
    return estafich;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the estafich.
   * 
   * @param estafich the new estafich
   */
  public void setEstafich(String estafich) {
    this.estafich = estafich;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nomensa.
   * 
   * @return the nomensa
   */
  public String getNomensa() {
    return nomensa;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nomensa.
   * 
   * @param nomensa the new nomensa
   */
  public void setNomensa(String nomensa) {
    if (nomensa.length() > 100) {
      this.nomensa = nomensa.substring(0, 100);
    }
    else {
      this.nomensa = nomensa;
    }
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nominter.
   * 
   * @return the nominter
   */
  public String getNominter() {
    return nominter;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nominter.
   * 
   * @param nominter the new nominter
   */
  public void setNominter(String nominter) {
    this.nominter = nominter;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nompgm.
   * 
   * @return the nompgm
   */
  public String getNompgm() {
    return nompgm;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nompgm.
   * 
   * @param nompgm the new nompgm
   */
  public void setNompgm(String nompgm) {
    this.nompgm = nompgm;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nomxml.
   * 
   * @return the nomxml
   */
  public String getNomxml() {
    return nomxml;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nomxml.
   * 
   * @param nomxml the new nomxml
   */
  public void setNomxml(String nomxml) {
    this.nomxml = nomxml;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tpficher.
   * 
   * @return the tpficher
   */
  public String getTpficher() {
    return tpficher;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tpficher.
   * 
   * @param tpficher the new tpficher
   */
  public void setTpficher(String tpficher) {
    this.tpficher = tpficher;
  }

}