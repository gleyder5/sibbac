/**
 * 
 */
package sibbac.business.daemons.tasks;

import java.util.concurrent.TimeUnit;

import org.quartz.DateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.daemons.confirmacionminotista.bo.ConfirmacionMinoristaBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * @author xIS16630
 *
 */
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME,
           name = Task.GROUP_DAEMONS.JOB_CONFIRMACION_MINORISTA,
           interval = 5,
           delay = 1,
           intervalUnit = DateBuilder.IntervalUnit.MINUTE)
public class TaskConfirmacionMinorista extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskConfirmacionMinorista.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  @Autowired
  private ConfirmacionMinoristaBo confirmacionMinoristaBo;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * (non-Javadoc)
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  public void execute() {
    final Long startTime = System.currentTimeMillis();
    LOG.debug("[execute] Iniciando la tarea de Daemons ...");

    Tmct0men tmct0men = null;
    boolean error = false;
    boolean incrementSentCount = false;
    try {
      LOG.debug("[execute] Bolqueando semaforo {} ...", TIPO_TAREA.TASK_CONFIRMACION_MINORISTA);

      tmct0men = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_CONFIRMACION_MINORISTA, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);

      /*************************************** NEGOCIO ***********************************************/
      // Path ficheroConfirmacionMinorista = null;
      try {
        // envio fichero
        confirmacionMinoristaBo.executeTask();
        incrementSentCount = true;
      } catch (Exception e) {
        LOG.warn("[execute] incidencia ", e.getMessage());
        LOG.debug("[execute] incidencia ", e);
        error = true;
      } finally {
        try {
          confirmacionMinoristaBo.closeWriterAndRenameFile();
        } catch (Exception e) {
          error = true;
          LOG.warn("[execute] incidencia ", e.getMessage());
          LOG.debug("[execute] incidencia ", e);

        }
      }
      /*************************************** FIN NEGOCIO ***********************************************/

      if (error) {
        LOG.info("[execute] ## Cambiando estado a TERMINACION NO CORRECTA ...");
        tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.EN_ERROR);
      } else {

        LOG.info("[execute] ## Cambiando estado a TMCT0MSC.LISTO_GENERAR ...");
        if (incrementSentCount) {
          tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.LISTO_GENERAR);
        } else {
          tmct0menBo.putEstadoMEN(tmct0men, TMCT0MSC.LISTO_GENERAR);
        }
      }
    } catch (SIBBACBusinessException e) {
      LOG.warn("[execute] No se puede bloquear el semaforo ## {} ## {}", TIPO_TAREA.TASK_CONFIRMACION_MINORISTA, e.getMessage());
    } catch (Exception e) {
      LOG.warn("[execute] ", e);

    } finally {

      final Long endTime = System.currentTimeMillis();
      LOG.info("[execute] Finalizada la tarea de confirmacion de minoristas ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally

  }

} // TaskConfirmacionMinorista
