package sibbac.business.estaticos.megara.inout;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.SubAccountClientAccount;
import sibbac.database.megbpdta.bo.Fmeg0sucBo;
import sibbac.database.megbpdta.model.Fmeg0sua;

@Service
@Scope(value = "prototype")
public class SubAccountByAliasOutPut {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SubAccountByAliasOutPut.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0sucBo fmeg0sucBo;

  private Fmeg0sua objFmeg0sua;

  public SubAccountByAliasOutPut(Fmeg0sua objFmeg0sua) {

    super();

    this.objFmeg0sua = objFmeg0sua;

  }

  public boolean procesarSalida() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    validacionGeneracionXML();

    return result;

  }

  private void validacionGeneracionXML() {

    logger.debug("Inicio obtencion de datos");

    if (esAsociacion()) {
      ctx.getBean(SubAccountClientAccount.class, objFmeg0sua, objFmeg0sua.getTpmodeid()).procesarXML();

      // AML 10/05/2012
      // Se elimina la generaci�n del fichero de CLIENT, ya que se genera al
      // crear el de subcuentas
      // new AliasClient(objFmeg0sua).execXML();

      /*
       * M.C.G. 13/01/2010 A partir de la fecha, adem�s de generar el xml CLIENT
       * ACCOUNT tambi�n van a generar los xml de los SETT MAP
       */

      /* if ("I".equals(objFmeg0sua.getTpmodeid())) { */

      if (!"D".equals(objFmeg0sua.getTpmodeid())) {

        /* new SubAccount_ConfirmationRule(objFmeg0sua).procesarXML(); */
        ctx.getBean(InstruccionLiquidacionSubAccountOutPut.class, objFmeg0sua, objFmeg0sua.getTpmodeid())
            .procesarSalidaSubAccount();
      }

    }

  }

  private boolean esAsociacion() {

    boolean esAsociacion = false;

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {

      tx.begin();

      esAsociacion = fmeg0sucBo.countByCdsubctaAndTpmodeidDistinctD(objFmeg0sua.getId().getCdsubcta()) > 0;

      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage());
    }
    finally {
      if (tx.isActive()) {

        tx.rollback();
      }
      em.close();
    }

    return esAsociacion;

  }
}