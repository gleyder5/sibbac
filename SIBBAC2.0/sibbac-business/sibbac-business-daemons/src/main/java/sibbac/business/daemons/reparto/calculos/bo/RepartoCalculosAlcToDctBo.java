package sibbac.business.daemons.reparto.calculos.bo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.bo.Tmct0aliBo;
import sibbac.business.fase0.database.model.Tmct0aloId;
import sibbac.business.wrappers.database.bo.Tmct0ValoresBo;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.calculos.CalculosEconomicosNacionalBo;
import sibbac.business.wrappers.database.bo.calculos.dto.CalculosEconomicosFlagsDTO;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.model.Tmct0alc;
import sibbac.business.wrappers.database.model.Tmct0alo;
import sibbac.common.SIBBACBusinessException;

@Service
public class RepartoCalculosAlcToDctBo {

  private static final Logger LOG = LoggerFactory.getLogger(RepartoCalculosAlcToDctBo.class);

  @Autowired
  private Tmct0aloBo aloBo;
  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private Tmct0aliBo aliBo;

  @Autowired
  private Tmct0ValoresBo valoresBo;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
  public void repartirCalculosAlcToDct(List<RepartoCalculosAlctToDctDTO> listAlcId, String audituser)
      throws EconomicDataException, SIBBACBusinessException {
    Tmct0alc alc;
    Boolean isAliasBonificado = null;
    Boolean isIsinDerechos = null;
    CalculosEconomicosFlagsDTO flags;
    CalculosEconomicosNacionalBo calculos;
    List<Tmct0alc> listAlcs = new ArrayList<Tmct0alc>();

    if (listAlcId != null && !listAlcId.isEmpty()) {

      Map<Tmct0aloId, List<RepartoCalculosAlctToDctDTO>> mapAlcsAgrupados = this.agruparAlcsByAlo(listAlcId);

      for (Tmct0aloId key : mapAlcsAgrupados.keySet()) {
        LOG.trace("[repartirCalculosAlcToDct] inicio: {}", key);
        if (listAlcId.get(0).getFlags() == null) {
          throw new SIBBACBusinessException("La configuracion del reparto debe llegar informada.");
        }
        Tmct0alo alo = aloBo.findById(key);

        for (RepartoCalculosAlctToDctDTO id : mapAlcsAgrupados.get(key)) {

          flags = id.getFlags();
          if (flags == null) {
            throw new SIBBACBusinessException("La configuracion del reparto debe llegar informada.");
          }
          else {
            alc = alcBo.findById(id.getAlcId());
            alc.setFhaudit(new Date());
            alc.setCdusuaud(audituser);
            listAlcs.add(alc);
            calculos = new CalculosEconomicosNacionalBo(alo, listAlcs);

            if (isAliasBonificado == null) {
              isAliasBonificado = aliBo.isAliasBonificado(alo.getCdalias());
            }
            if (isIsinDerechos == null) {
              isIsinDerechos = valoresBo.isIsinDerechos(alo.getCdisin());
            }
            if (!isAliasBonificado && !isIsinDerechos
                && alc.getImcanoncontreu().compareTo(alc.getImcanoncontrdv()) != 0) {
              alc.setImcanoncontrdv(alc.getImcanoncontreu().setScale(2, RoundingMode.HALF_UP));
              calculos.copiarCanonContreuToCanoncontrdv();
            }
            else if (alc.getImcanoncontreu().compareTo(BigDecimal.ZERO) == 0
                && alc.getImcanoncontrdv().compareTo(BigDecimal.ZERO) != 0) {
              alc.setImcanoncontreu(alc.getImcanoncontrdv().setScale(2, RoundingMode.HALF_UP));
              calculos.copiarCanonContrdvToCanoncontreu();
            }
            else if (alc.getImcanoncontreu().compareTo(BigDecimal.ZERO) != 0
                && alc.getImcanoncontrdv().compareTo(BigDecimal.ZERO) == 0) {
              alc.setImcanoncontrdv(alc.getImcanoncontreu().setScale(2, RoundingMode.HALF_UP));
              calculos.copiarCanonContreuToCanoncontrdv();
            }
          }

          calculos.repartirCalculosAlcEnDct(flags);
          alcBo.save(alc);
          listAlcs.clear();
        }
        LOG.trace("[repartirCalculosAlcToDct] fin: {}", key);
      }

    }

  }

  private Map<Tmct0aloId, List<RepartoCalculosAlctToDctDTO>> agruparAlcsByAlo(List<RepartoCalculosAlctToDctDTO> listAlcs) {
    Tmct0aloId key;
    Map<Tmct0aloId, List<RepartoCalculosAlctToDctDTO>> map = new HashMap<Tmct0aloId, List<RepartoCalculosAlctToDctDTO>>();
    for (RepartoCalculosAlctToDctDTO id : listAlcs) {
      key = new Tmct0aloId(id.getAlcId().getNucnfclt(), id.getAlcId().getNbooking(), id.getAlcId().getNuorden());
      if (map.get(key) == null) {
        map.put(key, new ArrayList<RepartoCalculosAlctToDctDTO>());
      }

      map.get(key).add(id);
    }
    return map;
  }
}
