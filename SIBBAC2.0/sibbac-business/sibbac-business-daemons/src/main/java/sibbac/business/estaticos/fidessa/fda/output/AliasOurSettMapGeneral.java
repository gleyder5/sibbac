package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.OurSettMapGeneralMegara;
import sibbac.business.estaticos.utils.conversiones.ConvertirAs400Fidessa;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.model.Fmeg0ael;

@Service
@Scope(value = "prototype")
public class AliasOurSettMapGeneral extends OurSettMapGeneralMegara {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AliasOurSettMapGeneral.class);

  private Fmeg0ael objFmeg0ael;

  private String tipoModificacion;

  @Autowired
  private ConvertirAs400Fidessa convertirAs400Fidessa;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  public AliasOurSettMapGeneral(Fmeg0ael objFmeg0ael, String tipoModificacion) {

    super();

    this.objFmeg0ael = objFmeg0ael;

    this.tipoModificacion = tipoModificacion;

  }

  public boolean procesarXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    /* AML 06-10-2010 */
    /*
     * Si market es MDR no se genera fichero
     */

    final String market = getEligible_clearer_mkt();

    if (!"MDR".equals(market)) {

      // generar el archivo XML
      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();
    }

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {

      tx.begin();

      // ----------------------------------------------------------------------------

      setTipoActualizacion(tipoModificacion);

      // ----------------------------------------------------------------------------

      setClient_mnemonic(objFmeg0ael.getId().getCdaliass().trim());

      // ----------------------------------------------------------------------------

      setEligible_clearer_entity(objFmeg0ael.getCdcleare().trim());

      // ----------------------------------------------------------------------------

      setEligible_clearer_mkt(objFmeg0ael.getCdmarket().trim());

      // ----------------------------------------------------------------------------

      if ("".equals(objFmeg0ael.getAccatcle().trim())) {
        setEligible_clearer_acc("MANUAL_ENTRY");
      }
      else {
        setEligible_clearer_acc(objFmeg0ael.getAccatcle().trim());
      }

      // ----------------------------------------------------------------------------

      setEligible_clearer_sett_type(convertirAs400Fidessa.execConvertir("TPSETTLE", objFmeg0ael.getTpsettle().trim()));

      tx.commit();

      // ----------------------------------------------------------------------------

    }
    catch (Exception e) {
      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "OUR SETT MAP GENERAL", getClient_mnemonic(), getNombreCortoXML(),
          "I", "", "Alias_GeneralOurMap", "FI");
    }
    else {
      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());
      result = ctx.getBean(RegistroControl.class, "OUR SETT MAP GENERAL", getClient_mnemonic(), getNombreCortoXML(),
          "E", getMensajeError(), "Alias_GeneralOurMap", "FI");

    }

    return result;

  }

}