package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.OurSettMapSpecificMegara;
import sibbac.business.estaticos.utils.conversiones.ConvertirAs400Fidessa;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.model.Fmeg0sel;

@Service
@Scope(value = "prototype")
public class SubAccountOurSettMapSpecific extends OurSettMapSpecificMegara {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SubAccountOurSettMapSpecific.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private ConvertirAs400Fidessa convertirAs400Fidessa;

  private Fmeg0sel objFmeg0sel;

  private String tipoModificacion;

  private String alias;

  public SubAccountOurSettMapSpecific(Fmeg0sel objFmeg0sel, String tipoModificacion) {

    super();

    this.objFmeg0sel = objFmeg0sel;

    this.tipoModificacion = tipoModificacion;

    this.alias = "";

  }

  public SubAccountOurSettMapSpecific(Fmeg0sel objFmeg0sel, String tipoModificacion, String alias) {

    super();

    this.objFmeg0sel = objFmeg0sel;

    this.tipoModificacion = tipoModificacion;

    this.alias = alias;

  }

  public boolean procesarXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    /* AML 06-10-2010 */
    /*
     * Si market es MDR no se genera fichero
     */

    final String market = getEligible_clearer_mkt();

    if (!"MDR".equals(market)) {

      // generar el archivo XML
      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();

    }

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {

      tx.begin();

      // ----------------------------------------------------------------------------

      setTipoActualizacion(tipoModificacion);

      // ----------------------------------------------------------------------------

      setClient_mnemonic(alias);

      // ----------------------------------------------------------------------------

      setAccount_mnemonic(objFmeg0sel.getId().getCdsubcta().trim());

      // ----------------------------------------------------------------------------

      setEligible_clearer_entity(objFmeg0sel.getCdcleare().trim());

      // ----------------------------------------------------------------------------

      setEligible_clearer_mkt(objFmeg0sel.getCdmarket().trim());

      // ----------------------------------------------------------------------------

      if ("".equals(objFmeg0sel.getAccatcle().trim())) {
        setEligible_clearer_acc("MANUAL_ENTRY");
      }
      else {
        setEligible_clearer_acc(objFmeg0sel.getAccatcle().trim());
      }

      // ----------------------------------------------------------------------------

      setEligible_clearer_sett_type(convertirAs400Fidessa.execConvertir("TPSETTLE", objFmeg0sel.getTpsettle().trim()));

      // ----------------------------------------------------------------------------
      tx.commit();
    }
    catch (Exception e) {
      setMensajeError(e.getMessage());
      logger.warn(e.getMessage());
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "OUR SETT MAP SPECIFIC", getAccount_mnemonic(), getNombreCortoXML(),
          "I", "", "Alias_SpecificOurMap", "FI");

    }
    else {

      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());

      result = ctx.getBean(RegistroControl.class, "OUR SETT MAP SPECIFIC", getAccount_mnemonic(), getNombreCortoXML(),
          "E", getMensajeError(), "Alias_SpecificOurMap", "FI");

    }

    return result;

  }

}