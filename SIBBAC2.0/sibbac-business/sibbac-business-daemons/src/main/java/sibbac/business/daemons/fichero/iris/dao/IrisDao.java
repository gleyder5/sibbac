package sibbac.business.daemons.fichero.iris.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import sibbac.business.wrappers.database.model.Tmct0cli;

@Repository
public interface IrisDao extends JpaRepository<Tmct0cli, String> {

  /**
   * Query para IRIS, ORIGINAL versión contador KGR y contador Detalle
   */
  public static final String IRIS_QUERY = 
      " SELECT " + 
      "  CLI.CDKGR KGR, " + 
      "  'B00StdI#BATCHGLCS#     217      1:    ' " + 
      "  || TO_CHAR(CURRENT_DATE,'yymmdd') AS DETALLE1, CHR(28) " + 
      "  || '2:EQSH  ' || CHR(28) " + 
      "  || '3:A' || CHR(28) " + 
      "  || '4:P' || CHR(28) " + 
      "  || '5:'  || LEFT(TRIM(COALESCE(CLI.CDKGR,''))||'         ',9) || CHR(28) " + 
      "  || '65:98es' || CHR(28) " + 
      "  || '7:SVB' || CHR(28)" + 
      "  || '10:' || TO_CHAR(BOK.FEEJECUC,'dd/mm/yy') || CHR(28) " + 
      "  || '12:' || TO_CHAR(BOK.FEVALOR ,'dd/mm/yy') || CHR(28) " + 
      "  || '15:' || CASE WHEN MIN(BOK.CDTPOPER) ='C' THEN 'B' ELSE 'S' END || CHR(28) " + 
      "  || '17:' || TO_CHAR(BOK.FEVALOR ,'dd/mm/yy') || CHR(28) " + 
      "  || '18:' || TRIM(VARCHAR_FORMAT(SUM(ALO.EUTOTBRU),'0000000000000.00')) || CHR(28) " + 
      "  || '20:EUR' || CHR(28) " + 
      "  || '60:' || TRIM(VARCHAR_FORMAT(TRUNC(SUM(ALO.NUTITCLI),0),'00000000000')) || CHR (28) " + 
      "  || '62:SVBSHS '|| CHR(28) " + 
      "  || '70:Y'|| CHR(28) " + 
      "  || '101:0000000000' AS DETALLE2, " + 
      "  TRIM(VARCHAR_FORMAT(SUM(ALO.EUTOTBRU),'0000000000000.00')) AS EFECTIVO, " + 
      "  TRIM(VARCHAR_FORMAT(TRUNC(SUM(ALO.NUTITCLI),0),'00000000000')) AS NTITULOS " + 
      " FROM " + 
      "  TMCT0ORD ORD " + 
      "  JOIN TMCT0BOK BOK ON BOK.FEVALOR > :currentDate AND (( ORD.CDCLSMDO = 'E' AND BOK.CDESTADO BETWEEN 300 AND 400) OR (ORD.CDCLSMDO = 'N' AND BOK.CDESTADOASIG > 0 AND BOK.CDESTADOASIG < 85))  AND  BOK.NUORDEN  = ORD.NUORDEN " + 
      "  JOIN TMCT0ALO ALO ON ALO.NUORDEN = BOK.NUORDEN AND ALO.NBOOKING = BOK.NBOOKING  AND ALO.FEVALOR > :currentDate " + 
      "  JOIN TMCT0ALI ALI ON ALI.CDALIASS = BOK.CDALIAS AND ALI.FHFINAL >= TO_CHAR(:currentDate,'yyyymmdd') " + 
      "  JOIN TMCT0CLI CLI ON CLI.CDBROCLI = ALI.CDBROCLI AND CLI.FHFINAL >= TO_CHAR(:currentDate,'yyyymmdd') " + 
      "  JOIN TMCT0EST EST ON EST.CDALIASS = BOK.CDALIAS AND EST.FHFINAL >= TO_CHAR(:currentDate,'yyyymmdd') " + 
      "  INNER JOIN TMCT0ONI ONI ON ONI.CDNIVEL1<> 'MIN' AND ONI.NUORDEN1 <> EST.STLEV1LM " + 
      " GROUP BY " + 
      "  CLI.CDKGR,BOK.FEEJECUC,BOK.FEVALOR ,BOK.CDTPOPER " + 
      " ORDER BY " + 
      "  CLI.CDKGR,BOK.FEEJECUC,BOK.FEVALOR ,BOK.CDTPOPER";
      
  /**
   * Query para IRIS con Paginación
   */
  public static final String IRIS_PAGE_QUERY = IRIS_QUERY + " LIMIT :offset, :pageSize";

  /**
   * Query para IRIS
   */
  public static final String IRIS_PAGE_COUNT_QUERY = "SELECT COUNT(*) FROM (" + IRIS_QUERY + ")";
  
  @Query(value = IRIS_PAGE_QUERY, nativeQuery = true)
  public List<Object[]> findAllListByPageIRIS(@Param("currentDate") Date currentDate, @Param("offset") Integer offset,  @Param("pageSize") Integer pageSize);

  /**
   * Obtiene los datos para la generación del fichero IRIS sin paginación
   * @return
   */
  @Query(value = IRIS_QUERY, nativeQuery = true)
  public List<Object[]> findAllListIRIS(@Param("currentDate") Date currentDate);

  /**
   * Obtiene los datos para la generación del fichero IRIS sin paginación
   * @return
   */
  @Query(value = IRIS_PAGE_COUNT_QUERY, nativeQuery = true)
  public Integer countAllListIRIS(@Param("currentDate") Date currentDate);

}