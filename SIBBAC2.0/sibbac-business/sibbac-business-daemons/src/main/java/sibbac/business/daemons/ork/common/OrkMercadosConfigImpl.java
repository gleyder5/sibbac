package sibbac.business.daemons.ork.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import static java.util.Objects.requireNonNull;
import static java.nio.file.Files.move;
import static java.text.MessageFormat.format;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.SibbacConfig;

@Component
@Scope(SCOPE_PROTOTYPE)
public class OrkMercadosConfigImpl implements OrkMercadosConfig {
	
	static final String FIDESSA_PATH_DEFAULT = "fidessa/ork/in";
	
	static final String FIDESSA_FILENAME_DEFAULT = "fidessa_ork_*.csv";
	
	static final String BME_PATH_DEFAULT = "CCP/BME/out/ORK";
	
	static final String BME_FILENAME_DEFAULT = "INB_549300VXHFB1JDHP7C26_549300VXHFB1JDHP7C26_ORK_AAAAMMDD_SSS.EXT";
	
	static final int READ_TIMEOUT_DEFAULT = 3_600;
	
	static final int WRITE_TIMEOUT_DEFAULT = 3_600;
	
	static final int UPDATE_TIMEOUT_DEFAULT = 3_600;
	
	static final int MAX_THREADS_DEFAULT = 5;
	
	static final String BME_FILENAME_INFIX = "AAAAMMDD_SSS";
	
	static final String TEMP_PREFIX = "TEMP_";
	
	/**
	 * Proporción por defecto entre las peticiones y los hilos de ejecución
	 */
	static final int PETITIONS_DEFAULT_PROPORTION = 3;
	
	private static final Logger LOG = getLogger(OrkMercadosConfigImpl.class);
  
	private Properties props;
	
	private SibbacConfig sibbacConfig;
	
	@Override
	public File[] getFicherosFidessa() throws SIBBACBusinessException {
		final File fidessaPath;
		final String pattern;
		
		fidessaPath = getFidessaPath();
		pattern = props.getProperty(FIDESSA_FILENAME, FIDESSA_FILENAME_DEFAULT);
		return fidessaPath.listFiles(new FidessaFilter(pattern));
	}
	
	@Override
	public File getBatchTempPath() throws SIBBACBusinessException {
		final File batchPath;
		
		batchPath = new File(getFidessaPath(), "temp");
		if(!batchPath.exists() && !batchPath.mkdirs()) {
			throw new SIBBACBusinessException(format("El directorio {0} no existe y no se puede crear.", 
					batchPath.getAbsolutePath()));
		}
		return batchPath;
	}

	@Override
	public int getLecturaTimeout() {
		final String strValue;
		
		strValue = props.getProperty(TIMEOUT_IN, Integer.toString(READ_TIMEOUT_DEFAULT));
		try {
			return Integer.parseInt(strValue);
		}
		catch(NumberFormatException ex) {
			LOG.warn("[getLecturaTimeout] Error de parseo, se retorna valor default de {} minutos", READ_TIMEOUT_DEFAULT);
		}
		return READ_TIMEOUT_DEFAULT;
	}

	@Override
	public int getEscrituraTimeout() {
		final String strValue;
		
		strValue = props.getProperty(WRITE_TIMEOUT_PROPERTY, Integer.toString(WRITE_TIMEOUT_DEFAULT));
		try {
			return Integer.parseInt(strValue);
		}
		catch(NumberFormatException ex) {
			LOG.warn("[getEscrituraTimeout] Error de parseo, se retorna valor default de {} minutos", WRITE_TIMEOUT_DEFAULT);
		}
		return WRITE_TIMEOUT_DEFAULT;
	}
	
	@Override
	public int getMaxThreads() {
		final String strValue;
		
		strValue = props.getProperty(MAX_THREADS_PROPERTY, Integer.toString(MAX_THREADS_DEFAULT));
		try {
			return Integer.parseInt(strValue);
		}
		catch(NumberFormatException ex) {
			LOG.warn("[getMaxThreads] Error de parseo, se retorna valor default de {} hilos", MAX_THREADS_DEFAULT);
		}
		return MAX_THREADS_DEFAULT;
	}

	@Override
	public int getMaxPetitions() {
		final String strValue;
		final int porDefecto;
		
		strValue = props.getProperty(MAX_PETITIONS_PROPERTY);
		if(strValue != null) {
			try {
				return Integer.parseInt(strValue);
			}
			catch(NumberFormatException ex) {
				LOG.warn(
						"[getMaxPetitions] Error de parseo de propriedad {}, Valor recibido: [{}]. Se calculara valor por defecto",
						MAX_PETITIONS_PROPERTY, strValue);
			}
		}
		porDefecto = getMaxThreads() * PETITIONS_DEFAULT_PROPORTION;
		LOG.info("[getMaxPetitions] Se devuelve valor calculado de máximo de peticiones: {}", porDefecto);
		return porDefecto;
	}

	@Override
	public OrkFiles getOrkFiles(int sequence) throws SIBBACBusinessException {
	  return new OrkFiles(getOrkFile(sequence), getBatchTempFile());
	}
	
	@Override
  public void moveFidessaFileToTratados(File origen) throws SIBBACBusinessException {
  	final String original, newName, message;
  	final File fileToMove;
  	
  	original = origen.getName();
  	newName = nameWithDateAndHour(original, new Date());
  	fileToMove = new File(getPathFidessaTratados(), newName);
  	try {
  		move(origen.toPath(), fileToMove.toPath(), StandardCopyOption.ATOMIC_MOVE);
  	}
  	catch(IOException ioex) {
  		message = format("Error al intentar mover el fichero Fidessa %s a la ubicación %s", newName, 
  				fileToMove.getAbsolutePath());
  		LOG.error("[moveFile] {}", message, ioex);
  		throw new SIBBACBusinessException(message, ioex);
  	}
  }

  @Override
  public int getUpdateTimeout() {
    final String strValue;
    
    strValue = props.getProperty(UPDATE_TIMEOUT_PROPERTY, Integer.toString(UPDATE_TIMEOUT_DEFAULT));
    try {
      return Integer.parseInt(strValue);
    }
    catch(NumberFormatException ex) {
      LOG.warn("[getEscrituraTimeout] Error de parseo, se retorna valor default de {} minutos", UPDATE_TIMEOUT_DEFAULT);
    }
    return UPDATE_TIMEOUT_DEFAULT;
  }
  
  @Override
  public String getProperty(String key) throws SIBBACBusinessException {
    final String strValue, message;
    
    strValue = props.getProperty(key);
    if(strValue == null) {
      message = format("No se encuentra la propiedad requerida {0} en el fichero de configuración mercados", key);
      LOG.error("[getProperty] {}", message);
      throw new SIBBACBusinessException(message);
    }
    return strValue;
  }

  @Autowired
	public void setSibbacConfig(SibbacConfig sibbacConfig) {
		this.sibbacConfig = sibbacConfig;
		props = sibbacConfig.loadPropertyFile(PROPERTIES_FILE_NAME);
	}
	
	String nameWithDateAndHour(String filename, Date now) {
		final DateFormat df;
		final String name, ext;
		final int pos;
		
		df = new SimpleDateFormat("_yyyyMMdd_HHmm");
		pos = filename.lastIndexOf(".");
		name = filename.substring(0, pos);
		ext = filename.substring(pos);
		return new StringBuilder(name).append(df.format(now)).append(ext).toString();
	}
	
	String nameWithDateAndSequence(String filenamePattern, Date now, int sequence) throws SIBBACBusinessException {
		final DateFormat df;
		final String prefix, suffix;
		final int pos;
		
		requireNonNull(filenamePattern, "El patron de nombre de fichero es requerido");
		pos = filenamePattern.lastIndexOf(BME_FILENAME_INFIX);
		if(pos <= -1) {
			throw new SIBBACBusinessException(format("El patrón {0} no contiene la cadena {1}", filenamePattern, 
					BME_FILENAME_INFIX));
		}
		prefix = filenamePattern.substring(0, pos);
		suffix = filenamePattern.substring(pos + BME_FILENAME_INFIX.length());
		df = new SimpleDateFormat("yyyyMMdd_");
		return new StringBuilder(TEMP_PREFIX).append(prefix).append(df.format(now)).append(String.format("%03d", sequence))
				.append(suffix).toString();
	}
	
	private File getOrkFile(int sequence) throws SIBBACBusinessException {
  	final String bmePath, fileName, filePattern;
  	final File dir;
  	String message;
  	
  	bmePath = props.getProperty(BME_PATH);
  	if(bmePath == null) {
  		dir = new File(sibbacConfig.getSIBBACFolder(), BME_PATH_DEFAULT);
  		LOG.info("No se encuentra la propiedad {}, se utiliza el valor default {}", BME_PATH, dir);
  	}
  	else {
  		dir = new File(bmePath);
  	}
  	if(!dir.exists() && !dir.mkdirs()) {
  		message = format("El directorio de salida BME {0} no existe y no se puede crear", dir.getAbsolutePath());
  		LOG.error("[getOrkFile] {}", message);
  		throw new SIBBACBusinessException(message);
  	}
  	filePattern = props.getProperty(BME_FILENAME, BME_FILENAME_DEFAULT);
  	fileName = nameWithDateAndSequence(filePattern, new Date(), sequence);
  	return new File(dir, fileName);
  }

  private File getPathFidessaTratados() throws SIBBACBusinessException {
		String tratadosPath;
		File p;
		
		tratadosPath = props.getProperty(FIDESSSA_TRATADOS_PATH);
		if(tratadosPath == null) {
			p = new File(getFidessaPath(), "tratados");
		}
		else {
			p = new File(tratadosPath);
		}
		if(!p.exists() && !p.mkdirs()) {
			throw new SIBBACBusinessException(format("El directorio {0} para fidessa tratados no existe y no se puede crear",
					p.getAbsolutePath())); 
		}
		return p;
	}

	private File getFidessaPath() throws SIBBACBusinessException {
		final File workingDirectory;
		final String workingPath;
		
		workingPath = props.getProperty(FIDESSA_PATH);
	  if(workingPath == null) {
	  	workingDirectory = new File(sibbacConfig.getSIBBACFolder(), FIDESSA_PATH_DEFAULT); 
	  }
	  else {
	  	workingDirectory = new File(workingPath);
	  }
		if(!workingDirectory.exists() && !workingDirectory.mkdirs()) {
			throw new SIBBACBusinessException(format("El directorio {0} no existe y no se puede crear", workingPath));
		}
		return workingDirectory;
	}

	private File getBatchTempFile() throws SIBBACBusinessException {
	  try {
		return File.createTempFile("UPDI", ".csv", getBatchTempPath());
	  }
	  catch(IOException ioex) {
		throw new SIBBACBusinessException("Al intentar crear un fichero temporal para batch de actualizacion", ioex);
	  }
	}
	
}
