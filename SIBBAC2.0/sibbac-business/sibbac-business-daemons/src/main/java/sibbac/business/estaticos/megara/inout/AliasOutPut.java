package sibbac.business.estaticos.megara.inout;

import javax.persistence.EntityManagerFactory;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.AliasClient;
import sibbac.database.megbpdta.model.Fmeg0ali;

/**
 * Alias Output genera los archivos xml correspondientes a la entrada en la
 * aplicacion de un alias
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 * 
 */
@Scope(value = "prototype")
@Service
public class AliasOutPut {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(AliasOutPut.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private Fmeg0ali objFmeg0ali;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * validacionGeneracionXML
   * 
   * realiza una validacion para comprobar si se debe seguir a delante con la
   * generacion de archivos
   * 
   */

  public AliasOutPut(Fmeg0ali objFmeg0ali) {

    super();

    this.objFmeg0ali = objFmeg0ali;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * validacionGeneracionXML
   * 
   * realiza una validacion para comprobar si se debe seguir a delante con la
   * generacion de archivos
   * 
   */

  public boolean execXML() {

    final String alias = objFmeg0ali.getCdaliass();

    if (alias != null
        && !("22645".equals(alias.trim()) || "22671".equals(alias.trim()) || "22672".equals(alias.trim()))) {
      validacionGeneracionXML();
    }

    boolean result = true;

    return result;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * validacionGeneracionXML
   * 
   * realiza una validacion para comprobar si se debe seguir a delante con la
   * generacion de archivos
   * 
   */
  private void validacionGeneracionXML() {

    logger.debug("Inicio obtencion de datos");

    AliasClient aliasClient = ctx.getBean(AliasClient.class, objFmeg0ali);

    aliasClient.execXML();

    if (!"D".equals(objFmeg0ali.getTpmodeid())) {

      InstruccionLiquidacionAliasOutPut ilqAlias = ctx.getBean(InstruccionLiquidacionAliasOutPut.class, objFmeg0ali,
          objFmeg0ali.getTpmodeid());
      ilqAlias.procesarSalidaAlias();
    }

    // }

  }
}