package sibbac.business.daemons.ork.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

import sibbac.business.daemons.batch.model.AbstractParam;

/**
 * Parámetro de una consulta OrkBme. Cada parametro de las consultas para generar el ORK está relacionado con un
 * campo Fidessa {@link sibbac.business.daemons.ork.model.OrkFidessaField} 
 * @author XI326526
 * @see sibbac.business.daemons.ork.model.OrkBmeQuery
 */
@Entity
@Table(name = "TMCT0_ORK_BME_QUERY_PARAM")
public class OrkBmeQueryParam extends AbstractParam implements Serializable {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 1697918336320779881L;

  @EmbeddedId
  private OrkBmeQueryParamId id;
  
  @OneToOne(optional = false)
  @JoinColumn(name = "FIDESSA_FIELD", nullable = false)
  private OrkFidessaField orkFidessaField;

  /**
   * @return Llave primaria del parámetro.
   */
  public OrkBmeQueryParamId getId() {
    return id;
  }

  /**
   * @param id Determina la llave primaria que identifica al parámetro.
   */
  public void setId(OrkBmeQueryParamId id) {
    this.id = id;
  }

  /**
   * @return el campo fidessa asociado
   */
  public OrkFidessaField getOrkFidessaField() {
    return orkFidessaField;
  }

  /**
   * @param orkFidessaField determina el campo fidessa del que este parámetro obtiene su valor.
   */
  public void setOrkFidessaField(OrkFidessaField orkFidessaField) {
    this.orkFidessaField = orkFidessaField;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((orkFidessaField == null) ? 0 : orkFidessaField.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    OrkBmeQueryParam other = (OrkBmeQueryParam) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (orkFidessaField == null) {
      if (other.orkFidessaField != null)
        return false;
    } else if (!orkFidessaField.equals(other.orkFidessaField))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return id.toString();
  }

  /**
   * @return el valor recuperado del método {@link sibbac.business.daemons.ork.model.OrkBmeQueryParamId#toString()} 
   */
  @Override
  protected String getIdString() {
    return id.toString();
  }
  
}
