package sibbac.business.daemons.batch;

import java.util.List;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.common.utils.SendMail;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SendMailExecutor extends DestinyExecutor {
  
  private static final Logger LOG = LoggerFactory.getLogger(SendMailExecutor.class);
  
  private static final String PROLOGO = "Resultado de la consulta ";
  
  private static final String FALLIDO = "No se encuentra componente de envío de correo electronico";
  
  private SendMail sendMail;
  
  private int maxSendedMails;
  
  private List<String> to;
  
  private String subject;
  
  private String infoQuery;
  
  private ResultSetMetaData md;
  
  @Autowired
  void setSendMail(SendMail sendMail) {
    this.sendMail = sendMail;
  }
  
  void setMaxSendedMails(int maxSendedMails) {
    this.maxSendedMails = maxSendedMails;
  }
  
  void setTo(List<String> to) {
    this.to = to;
  }
  
  void setSubject(String subject) {
    this.subject = subject;
  }

  @Override
  void executeStatement(Connection con, String query) throws BatchException {
    try(Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query)) {
      processResultSet(rs);
    }
    catch(SQLException sqlex) {
      throw new BatchException("Error de base de datos al obtener resultados para enviar por mail", sqlex);
    }
  }

  String prepareMail(ResultSet rs) throws SQLException {
    final StringBuilder body;
    int columnCount;
    
    columnCount = md.getColumnCount();
    body = new StringBuilder(PROLOGO).append(infoQuery);
    body.append('\n');
    for(int i = 1; i <= columnCount; i++) {
      body.append(md.getColumnName(i)).append(": ");
      body.append(format(rs, i, md.getColumnType(i)));
    }
    return body.toString();
  }

  private void processResultSet(ResultSet rs) throws SQLException, BatchException {
    String body;
    int count = 0;

    LOG.debug("[processResultSet] Inicio");
    try {
      md = rs.getMetaData();
      while(count++ < maxSendedMails && rs.next()) {
        body = prepareMail(rs);
        if(sendMail != null) {
          sendMail.sendMail(to, subject, body);
        }
        else {
          LOG.warn("[executeStatement] {} Correo fallido a {} con cuerpo {}", FALLIDO, to, body);
        }
      }
      addReturnedRows(count);
      if(count >= maxSendedMails) {
        LOG.warn("[processResultSet] Se ha generado el número máximo de correos para la consulta {0}", infoQuery);
      }
    }
    catch(IllegalArgumentException | MessagingException ex) {
      throw new BatchException("Error al enviar correo electronico", ex);
    }
    catch(IOException iex) {
      throw new BatchException("Error inesperado en la construccion del correo electronico", iex);
    }
    catch(RuntimeException ex) {
      throw new BatchException("Error inesperado de tiempo de ejecucion del envio de correo electronico", ex);
    }
  }

}
