package sibbac.business.daemons.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.text.MessageFormat;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.BatchException;

class FillBufferExecutor extends DestinyExecutor {

	private static final Logger LOG = getLogger(FillBufferExecutor.class);
	
	private final File destino;
	
  private final PrintWriter writer;
  
  FillBufferExecutor(final File destino) throws BatchException {
  	final String message;
  	
  	this.destino = destino;
  	try {
	  	writer = new PrintWriter(new BufferedWriter(
	        new OutputStreamWriter(new FileOutputStream(destino), BatchProcessor.CHAR_SET)));
  	}
    catch (IOException ioex) {
      message = MessageFormat.format("Al abrir fichero buffer {0} a llenar", destino.getName());
      LOG.warn("[executeToBufferDirect]{} | Error: {}", message, ioex);
      throw new BatchException(message, ioex);
    }
  }
  
  @Override 
  void executeStatement(Connection con, String query) throws BatchException {
    try(Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query)) {
      writeResultSet(rs);
    }
    catch(SQLException sqlex) {
      throw new BatchException("Al ejecutar una sentencia destinada a llenar buffer", sqlex);
    }
  }
  
  @Override
  void close() {
 		writer.close();
 		super.close();
  }

  File getFile() {
    return destino;
  }
  
  private synchronized void writeResultSet(ResultSet rs) throws SQLException {
    final ResultSetMetaData md;
    final int columns;
    int returned = 0;
    boolean first;
    String value;

    md = rs.getMetaData();
    columns = md.getColumnCount();
    while(rs.next()) {
      first = true;
      for(int i = 1; i <= columns; i++) {
        if(first) first = false; else writer.print(';');
        value = format(rs, i, md.getColumnType(i));
        if(value != null) {
          writer.print('"');
          writer.print(value);
          writer.print('"');
        }
      }
      writer.print(";\r\n");
      returned++;
    }
    addReturnedRows(returned);
  }

}
