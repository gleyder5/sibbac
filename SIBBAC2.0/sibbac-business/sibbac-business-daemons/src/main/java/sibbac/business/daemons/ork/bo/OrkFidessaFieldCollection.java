package sibbac.business.daemons.ork.bo;

import java.util.List;
import java.util.ArrayList;
import static java.sql.Types.DATE;
import static java.sql.Types.VARCHAR;
import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.common.SIBBACBusinessException;
import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.common.DirectTypeEnum;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.batch.model.BatchParam;
import sibbac.business.daemons.batch.model.ParamId;
import sibbac.business.daemons.batch.model.Query;
import sibbac.business.daemons.batch.model.QueryId;
import sibbac.business.daemons.ork.model.OrkFidessaFixedField;
import sibbac.business.daemons.ork.model.OrkFidessaDynaField;
import sibbac.business.daemons.ork.model.OrkFidessaField;
import static sibbac.business.daemons.ork.dao.OrkFidessaDao.DELETE_EXISTANT;
import static sibbac.business.daemons.ork.dao.OrkFidessaDao.DETAIL_INSERT;
import static sibbac.business.daemons.ork.dao.OrkFidessaDao.MAIN_INSERT;
import static sibbac.business.daemons.ork.dao.OrkFidessaDao.UPDATE_MAIN;

public class OrkFidessaFieldCollection {
  
  private static final Logger LOG = getLogger(OrkFidessaFieldCollection.class);
  
  private static final String BATCH_CODE = "ORK";

  private final List<OrkFidessaFixedField> fixedFields;
  
  private final List<OrkFidessaFixedField> indexFields;
  
  private final List<OrkFidessaDynaField> dynaFields;
  
  private final List<OrkFidessaField> allFields;
  
  OrkFidessaFieldCollection(final List<OrkFidessaFixedField> fixedFields, 
      final List<OrkFidessaDynaField> dynaFields) {
    this.fixedFields = requireNonNull(fixedFields, "La lista de campos fijos es requerida");
    this.dynaFields = requireNonNull(dynaFields, "La lista de campos dinámicos es requerida");
    indexFields = new ArrayList<>();
    for(OrkFidessaFixedField ff : fixedFields) {
      if(ff.isKey()) {
        indexFields.add(ff);
      }
    }
    allFields = new ArrayList<>();
    allFields.addAll(fixedFields);
    allFields.addAll(dynaFields);
  }
  
  public Iterable<OrkFidessaFixedField> itFixedFields() {
    return fixedFields;
  }
  
  public Iterable<OrkFidessaFixedField> itIndexFields() {
    return indexFields;
  }
  
  /**
   * Crea un objeto Batch que procesará los ficheros provenientes de Fidessa
   * @return Un Batch creado en tiempo de ejecución
   * @throws BatchException En caso de error en la configuración
   * @throws SIBBACBusinessException En caso de violación de regla de negocio.
   */
  public Batch loadOrkBatch() throws BatchException, SIBBACBusinessException {
    final List<Query> queries;
    final Batch batch;
    final int lastField, lastPos;
    int currentOrder;
    
    LOG.debug("[loadOrkBatch] Inicio...");
    if(dynaFields.isEmpty()) {
      throw new BatchException("Los campos dinamicos del Batch de Ork estan vacíos. Confirme");
    }
    LOG.info("[loadOrkBatch] Se crea Batch con {} campos dinámicos", dynaFields.size());
    batch = new Batch();
    batch.setActive(true);
    batch.setCode(BATCH_CODE);
    queries = new ArrayList<>();
    queries.add(buildDeleteExistantQuery());
    queries.add(buildMainQuery());
    currentOrder = queries.size();
    lastField = allFields.size() - 1;
    lastPos = allFields.get(lastField).getPosition();
    for(OrkFidessaDynaField dynafield : dynaFields) {
      queries.add(buildDetailQuery(lastPos, dynafield, ++currentOrder));
    }
    batch.setQueries(queries);
    batch.loadSteps();
    LOG.debug("[loadOrkBatch] Fin. Se retorna batch con {} queries", batch.getQueries().size());
    return batch;
  }
  
  public Batch loadUpdateBatch() throws SIBBACBusinessException {
    final Query query;
    final Batch batch;
    final List<BatchParam> params;
    
    LOG.trace("[updateBatch] Inicio");
    query = queryFactory(1, DirectTypeEnum.FROM_BUFFER, UPDATE_MAIN);
    parameterFactory(query, 1, 2, 3, 4, 5, 6, 7);
    params = query.getParameters();
    params.get(1).setType(DATE);
    params.get(5).setType(DATE);
    batch = new Batch();
    batch.setActive(true);
    batch.setCode(BATCH_CODE);
    batch.setQueries(singletonList(query));
    batch.loadSteps();
    return batch;
  }

  private Query buildDeleteExistantQuery() {
    final Query deleteQuery;
    
    deleteQuery = queryFactory(1, DirectTypeEnum.FROM_BUFFER, DELETE_EXISTANT);
    parameterFactory(deleteQuery, indexFields);
    return deleteQuery;
  }

  private Query buildMainQuery() {
    final Query mainQuery;
    
    mainQuery = queryFactory(2, DirectTypeEnum.IN_SAME_LINE, MAIN_INSERT);
    parameterFactory(mainQuery, fixedFields);
    return mainQuery;
  }
  
  private Query buildDetailQuery(int lastField, OrkFidessaDynaField field, int order) {
    final Query detailQuery;
    final List<BatchParam> params;
    final int size;
    
    size = indexFields.size();
    detailQuery = queryFactory(order, DirectTypeEnum.IN_SAME_LINE, DETAIL_INSERT);
    parameterFactory(detailQuery, indexFields);
    parameterFactory(detailQuery, lastField, field.getPosition());
    params = detailQuery.getParameters();
    params.get(size).setValue(field.getName());
    params.get(size).setFixed(true);
    params.get(size + 1).setValue(field.getDefaultValue());
    return detailQuery;
  }

  private Query queryFactory(int order, DirectTypeEnum direct, String sql) {
    final QueryId id;
    final Query query;
    
    id = new QueryId();
    id.setCode(BATCH_CODE);
    id.setOrder(order);
    query = new Query();
    query.setId(id);
    query.setDirectType(direct);
    query.setQuery(sql);
    query.setType(Query.NORMAL);
    query.setActive(1);
    return query;
  }

  private <S extends OrkFidessaField> void parameterFactory(Query query, List<S> fidessaFields) {
    final QueryId id;
    final List<BatchParam> parameters;
    BatchParam param;
    ParamId paramId;
    
    id = query.getId();
    parameters = new ArrayList<>();
    for(OrkFidessaField ff : fidessaFields) {
      paramId = new ParamId();
      paramId.setCode(id.getCode()); //relacion de las entidades
      paramId.setOrder(id.getOrder()); //relacion de las entidades
      paramId.setParam(ff.getPosition()); //Posicion del parametro en el fichero
      param = new BatchParam();
      param.setId(paramId);
      param.setValue(ff.getDefaultValue());
      param.setType(ff.getType());
      parameters.add(param);
    }
    query.setParameters(parameters);
  }

  private void parameterFactory(Query query, int ...paramPos) {
    final QueryId id;
    final int len;
    final List<BatchParam> parameters;
    BatchParam param;
    ParamId paramId;
    
    id = query.getId();
    parameters = query.getParameters();
    len = paramPos.length;
    for(int i = 0; i < len; i++) {
      paramId = new ParamId();
      paramId.setCode(id.getCode());
      paramId.setOrder(id.getOrder());
      paramId.setParam(paramPos[i]);
      param = new BatchParam();
      param.setId(paramId);
      param.setType(VARCHAR);
      param.setValue(null);
      parameters.add(param);
    }
  }
  
}
