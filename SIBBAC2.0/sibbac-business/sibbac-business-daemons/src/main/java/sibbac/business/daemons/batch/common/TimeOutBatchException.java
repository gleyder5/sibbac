package sibbac.business.daemons.batch.common;

@SuppressWarnings("serial")
public class TimeOutBatchException extends BatchException {
  
  public TimeOutBatchException(String message) {
    super(message);
  }

  public TimeOutBatchException(String message, Exception cause) {
    super(message, cause);
  }

}
