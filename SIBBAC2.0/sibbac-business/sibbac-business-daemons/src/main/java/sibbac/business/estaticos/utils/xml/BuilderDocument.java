package sibbac.business.estaticos.utils.xml;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;

/**
 * Esta clase crea un documento XML.
 * 
 * @author Jos� Juan Escudero
 * @author Trentisa I+D
 * @version 1.0
 * @viz.diagram BuilderDocument.tpx
 */
public class BuilderDocument {
    private String qualifiedName = "";

    private String publicId = "";

    private String systemId = "";

    private String namespaceURI = "";

    private Document doc = null;

    /**
     * Constructor for BuilderDocument. No genera el documento XML
     */
    public BuilderDocument() throws NewXmlException {
        super();
    }

    /**
     * Constructor for BuilderDocument. Genera el documento XML
     * 
     * @param qualifiedName
     * @param publicId
     * @param systemId
     */
    public BuilderDocument(String qualifiedName, String publicId,
            String systemId) throws NewXmlException {
        super();
        this.qualifiedName = qualifiedName;
        this.publicId = publicId;
        this.systemId = systemId;
        makeDoc();
    }

    /**
     * Constructor for BuilderDocument. Genera el documento XML
     * 
     * @param qualifiedName
     * @param publicId
     * @param systemId
     * @param namespaceUri
     */
    public BuilderDocument(String qualifiedName, String publicId,
            String systemId, String namespaceUri) throws NewXmlException {
        super();
        this.qualifiedName = qualifiedName;
        this.publicId = publicId;
        this.systemId = systemId;
        this.namespaceURI = namespaceUri;
        makeDoc();
    }

    /**
     * Method makeDoc. Crea el documento XML con las definiciones dadas
     * 
     * @return Document
     */
    public Document makeDoc() throws NewXmlException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            DOMImplementation di = db.getDOMImplementation();
            DocumentType dt = di.createDocumentType(qualifiedName, publicId,
                    systemId);
            doc = di.createDocument(namespaceURI, qualifiedName, dt);
        } catch (FactoryConfigurationError e) {
            throw new NewXmlException(
                    "Error de configuraci�n de Doument Builder Factory \n" + e
                            + ":" + e.getMessage());
        } catch (ParserConfigurationException e) {
            throw new NewXmlException("Error de configuraci�n de Parser \n" + e
                    + ":" + e.getMessage());
        }
        return doc;
    }

    /**
     * Method getDoc. Retorna el documento XML
     * 
     * @return Document
     * 
     * @uml.property name="doc"
     */
    public Document getDoc() {
        return doc;
    }

    /**
     * Returns the publicId.
     * 
     * @return String
     * 
     * @uml.property name="publicId"
     */
    public String getPublicId() {
        return publicId;
    }

    /**
     * Returns the qualifiedName.
     * 
     * @return String
     * 
     * @uml.property name="qualifiedName"
     */
    public String getQualifiedName() {
        return qualifiedName;
    }

    /**
     * Returns the systemId.
     * 
     * @return String
     * 
     * @uml.property name="systemId"
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * Sets the publicId.
     * 
     * @param publicId
     *            The publicId to set
     * 
     * @uml.property name="publicId"
     */
    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    /**
     * Sets the qualifiedName.
     * 
     * @param qualifiedName
     *            The qualifiedName to set
     * 
     * @uml.property name="qualifiedName"
     */
    public void setQualifiedName(String qualifiedName) {
        this.qualifiedName = qualifiedName;
    }

    /**
     * Sets the systemId.
     * 
     * @param systemId
     *            The systemId to set
     * 
     * @uml.property name="systemId"
     */
    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    /**
     * Returns the namespaceURI.
     * 
     * @return String
     * 
     * @uml.property name="namespaceURI"
     */
    public String getNamespaceURI() {
        return namespaceURI;
    }

    /**
     * Sets the namespaceURI.
     * 
     * @param namespaceURI
     *            The namespaceURI to set
     * 
     * @uml.property name="namespaceURI"
     */
    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }

    /**
     * Sets the doc.
     * 
     * @param doc
     *            The doc to set
     * 
     * @uml.property name="doc"
     */
    public void setDoc(Document doc) {
        this.doc = doc;
    }
}
