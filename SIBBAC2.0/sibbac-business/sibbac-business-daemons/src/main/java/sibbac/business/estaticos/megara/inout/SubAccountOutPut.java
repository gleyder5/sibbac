package sibbac.business.estaticos.megara.inout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.SubAccountClientAccount;
import sibbac.database.megbpdta.bo.Fmeg0suaBo;
import sibbac.database.megbpdta.model.Fmeg0sua;
import sibbac.database.megbpdta.model.Fmeg0suaId;
import sibbac.database.megbpdta.model.Fmeg0suc;

// Eliminacion de la validacion de broker realizado a peticion de Paloma Moreno en implantacion dia 16 de octubre de 2008
@Service
@Scope(value = "prototype")
public class SubAccountOutPut {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(SubAccountOutPut.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0suaBo fmeg0suaBo;

  private Fmeg0suc objFmeg0suc;

  private String tipoActualizacion;

  public SubAccountOutPut(Fmeg0suc objFmeg0suc) {

    super();

    this.objFmeg0suc = objFmeg0suc;

    tipoActualizacion = objFmeg0suc.getTpmodeid();

  }

  public boolean procesarSalida() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    validacionGeneracionXML();

    return result;

  }

  private void validacionGeneracionXML() {

    logger.debug("Inicio obtencion de datos");

    List<Fmeg0sua> list_asociaciones = asociacionesSubcuenta();

    if (0 < list_asociaciones.size()) {

      for (int i = 0; i < list_asociaciones.size(); i++) {
        ctx.getBean(SubAccountClientAccount.class, (Fmeg0sua) list_asociaciones.get(i), tipoActualizacion)
            .procesarXML();

        if (!"D".equals(objFmeg0suc.getTpmodeid())) {

          /**
           * new SubAccount_ConfirmationRule( (Fmeg0sua)
           * list_asociaciones.get(i)).procesarXML();
           */
          ctx.getBean(InstruccionLiquidacionSubAccountOutPut.class, (Fmeg0sua) list_asociaciones.get(i),
              objFmeg0suc.getTpmodeid()).procesarSalidaSubAccount();
        }

      }

    }
    else if ("H".equals(objFmeg0suc.getTpmodeid()))
    {
      Fmeg0sua fmeg0sua = new Fmeg0sua();
      Fmeg0suaId fmeg0suaId = new Fmeg0suaId();
      fmeg0suaId.setCdaliass(objFmeg0suc.getNbnameid());
      fmeg0suaId.setCdsubcta(objFmeg0suc.getCdsubcta());
      
      fmeg0sua.setTpmodeid("H");
      fmeg0sua.setId(fmeg0suaId);
      
      ctx.getBean(SubAccountClientAccount.class, fmeg0sua, "H").procesarXML();
      
    }
  }

  private List<Fmeg0sua> asociacionesSubcuenta() {
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    List<Fmeg0sua> list = null;

    try {

      tx.begin();
      list = fmeg0suaBo.findAllByCdsubctaAndTpmodeidDistinctD(objFmeg0suc.getCdsubcta());
      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage());
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

    return list;

  }
}