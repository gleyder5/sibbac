package sibbac.business.daemons.ork.model;

import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;

/**
 * Entidad que representa un campo del Fichero Fidessa que será almacenado en la entidad OrkFidessaDetail
 * @author XI326526
 */
@Entity
@DiscriminatorValue("D")
public class OrkFidessaDynaField extends OrkFidessaField {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 3307063462600445397L;

}
