package sibbac.business.daemons.batch.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static java.lang.String.format;

import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.JoinColumns;
import javax.persistence.JoinColumn;
import static javax.persistence.CascadeType.ALL;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.common.DirectTypeEnum;

@Entity
@Table(name = "TMCT0_BATCH_QUERY")
public class Query implements Serializable, Comparable<Query> {
  
  /**
   * Constante que indica que la query intenta consultar datos para enviar por correo electrónico
   */
  public static final char TO_MAIL = 'M';
  
  /**
   * Constante que indica que la query intenta consultar datos para almacenarlos en un buffer intermedio
   */
  public static final char TO_BUFFER = 'B';
  
  /**
   * Constante que indica que se intenta realizar una actualización en la base de datos
   */
  public static final char NORMAL = 'N';

  private static final long serialVersionUID = -284680915515055344L;
  
  @EmbeddedId
  private QueryId id;
  
  @Lob
  @Column(name = "QUERY")
  private String query;
  
  @Column(name = "DESCRIPTION", length = 50)
  private String description;
  
  @Column(name = "TYPE")
  private char type;
  
  @Column(name = "DIRECT")
  private int direct;
  
  @Column(name = "ACTIVE")
  private int active;
  
  @OneToMany(cascade = ALL)
  @JoinColumns({@JoinColumn(name = "CODE"), @JoinColumn(name = "ORDER") })
  @OrderBy("param")
  private List<BatchParam> parameters = new ArrayList<>();

  public QueryId getId() {
    return id;
  }

  public String getQuery() {
    return query;
  }

  public String getDescription() {
    return description;
  }

  public char getType() {
    return type;
  }

  public int getActive() {
    return active;
  }

  public void setId(QueryId id) {
    this.id = id;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setType(char type) {
    this.type = type;
  }

  public void setActive(int active) {
    this.active = active;
  }
  
  public boolean isActive() {
    return active == 1;
  }
  
  public void setDirect(int direct) {
    this.direct = direct;
  }
  
  public int getDirect() {
    return direct;
  }
  
  public boolean isDirect() {
    return direct == 1;
  }
  
  public List<BatchParam> getParameters() {
    return parameters;
  }
  
  public void setParameters(List<BatchParam> parameters) {
    this.parameters = parameters;
  }
  
  @Transient
  public DirectTypeEnum getDirectType() {
  	DirectTypeEnum values[];
  	
  	values = DirectTypeEnum.values();
  	if(direct < 0 || direct >= values.length) {
  		throw new IllegalArgumentException(format("Valor incorrecto del campo direct: %d", direct));
  	}
  	return values[direct];
  }
  
  @Transient
  public void setDirectType(DirectTypeEnum directType) {
  	setDirect(directType.ordinal());
  }

	@Override
  public int compareTo(Query o) {
    return Integer.compare(id.getOrder(), o.id.getOrder());
  }
	
	BatchException campoDirectoIncorrecto() {
  	return new BatchException(format("El valor [%s] del campo direct de la query [%s] es incorrecto",
				direct, id.toString()));
  }
  
}
