package sibbac.business.daemons.tasks;

import static java.text.MessageFormat.format;
import static org.slf4j.LoggerFactory.getLogger;
import static sibbac.tasks.enums.SIBBACJobType.CRON_JOB;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.daemons.batch.BatchProcessor;
import sibbac.business.daemons.batch.bo.BatchBo;
import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.utils.SendMail;
import sibbac.tasks.Task.GROUP_DAEMONS;
import sibbac.tasks.annotations.SIBBACJob;

@DisallowConcurrentExecution
@SIBBACJob(group = GROUP_DAEMONS.NAME, name = GROUP_DAEMONS.JOB_BATCH, jobType = CRON_JOB, 
cronExpression = "0 * * ? * *")
public class TaskBatch extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOG = getLogger(TaskBatch.class);

  @Autowired
  private GestorPeriodicidades gestorPeriodicidades;

  @Autowired
  private BatchBo batchBo;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private SendMail sendMail;

  @Value("${sibbac.batch.threads: 5}")
  private int threads;

  @Value("${sibbac.batch.path:/sbrmv/ficheros/batch/}")
  private String workPath;

  /**
   * Minutos de espera para que el ejecutor realice toda la tarea.
   */
  @Value("${sibbac.batch.timeout: 60}")
  private int timeout;

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.TASK_DAEMONS_BATCH;
  }

  @Override
  public void executeTask() {
    final List<Batch> batchList;
    final List<BatchProcessor> processorList;
    final Date dateTime;
    final File workdirectory;
    ExecutorService executor = null;
    Future<Integer> submited;
    BatchProcessor batchProcessor;
    boolean onTime, allRight;
    int cancelled = 0;

    workdirectory = new File(workPath);
    dateTime = new Date();
    processorList = new ArrayList<>();
    onTime = true;
    try {
      executor = Executors.newFixedThreadPool(threads, new NamedThreadFactory());
      batchList = batchBo.findAllPeriodicActiveBatchs();
      for (Batch batch : batchList) {
        if (gestorPeriodicidades.isReglaActiva(batch.getPeriodicidad(), dateTime)) {
          batchProcessor = ctx.getBean(BatchProcessor.class);
          batchProcessor.setBatch(batch);
          batchProcessor.setWorkdirectory(workdirectory);
          submited = executor.submit(batchProcessor);
          batchProcessor.setFuture(submited);
          processorList.add(batchProcessor);
        }
      }
      executor.shutdown();
      onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);
      if (!onTime) {
        for (BatchProcessor processor : processorList) {
          if (!processor.isDone()) {
          	processor.cancel();
            cancelled++;
          }
        }
        LOG.error("[sendBatch] Ha finalizado el tiempo de espera de {} minutos, se solicita cancelar {} tareas",
            timeout, cancelled);
      }
    }
    catch (InterruptedException iex) {
      LOG.error("[sendBatch] Se ha interrumpido el hilo en espera con mensaje {}", iex.getMessage());
    }
    catch (RuntimeException rex) {
      LOG.error("[sendBatch] Error no esperado al lanzar o ejecutar tareas batch", rex);
    }
    if(executor != null && !executor.isShutdown()) {
  		executor.shutdownNow();
    }
    allRight = true;
    for (BatchProcessor processor : processorList) {
      allRight &= checkFuture(processor);
    }
    if (!(onTime && allRight)) {
      LOG.error("La tarea batch ha finalizado fuera de tiempo o con batch con errores");
    }
  }

  private boolean checkFuture(BatchProcessor processor) {
  	final Batch batch;
  	int modificados;
  	
    batch = processor.getBatch();
  	try {
      modificados = processor.getResult();
      LOG.debug("El batch {} ha finalizado modificando {} renglones", batch.getCode(),
      		modificados);
      return true;
    }
    catch (ExecutionException eex) {
      notifyBatchFailure(batch.getCode(), batch.receptorList(), eex);
    }
    catch (InterruptedException e) {
      LOG.warn("[checkFuture] Se ha cancelado el hilo del batch {}", batch.getCode(), e);
    }
    catch (RuntimeException ex) {
      LOG.warn("[checkFuture] Error inesperado en la evaluacion del resultado de batch {}", batch.getCode(), ex);
    }
    return false;
  }

  private void notifyBatchFailure(String code, List<String> receptors, ExecutionException eex) {
    final Throwable cause;
    final BatchException bex;

    cause = eex.getCause();
    if(cause != null) {
    	if(cause instanceof BatchException) {
        bex = BatchException.class.cast(cause);
        LOG.error("[notifyBatchFailure] Batch {} finalizado con error", code, cause);
        if (receptors.isEmpty()) {
          LOG.warn(
          		"[notifyBatchFailure] Batch {} no contiene destinatarios, no se notifica error por correo electronico",
              code);
          return;
        }
        try {
          try (StringWriter errors = new StringWriter(); PrintWriter pw = new PrintWriter(errors)) {
            bex.printStackTrace(pw);
            sendMail.sendMail(receptors, format("Error en ejecucion batch {0}", code), format(
                "El batch con codigo {0} ha finalizado con error\nMensaje:{1}\nTraza de error:\n{2}", code, 
                bex.getMessage(), errors.toString()));
          }
          
          LOG.debug("[notifyBatchFailure] Se ha enviado un email notificando de un error en el batch {} "
              + "a los siguientes destinatarios: {}", code, receptors);
        }
        catch (Exception ex) {
          LOG.error("[notifyBatchFailure] Error al intentar enviar un email notificando un error en el batch {}", 
          		code, ex);
        }
    		return;
    	}
    	LOG.error("[notifyBatchFailure] Hilo de ejecución finalizado con error no controlado para batch {}", code, cause);
    	return;
    }
    LOG.error("[notifyBatchFailure] Hilo de ejecucion finalizado con causa nula para batch", code, eex);
  }

  private class NamedThreadFactory implements ThreadFactory {

    private int count = 0;

    @Override
    public Thread newThread(Runnable r) {
      final Thread thread;

      thread = new Thread(r);
      thread.setName(String.format("SIBBAC_Batch-%d", count++));
      return thread;
    }

  }

}
