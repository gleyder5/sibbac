package sibbac.business.daemons.batch;

import java.sql.Connection;
import java.sql.SQLException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.currentThread;
import static java.util.Objects.requireNonNull;
import static java.text.MessageFormat.format;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.common.TimeOutBatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.batch.model.BatchEnvironment;
import sibbac.business.daemons.batch.model.Query;
import sibbac.business.daemons.batch.model.Step;

@Component
@Scope(SCOPE_PROTOTYPE)
public class BatchProcessor extends BatchEnvironment implements Callable<Integer> {

  static final String CHAR_SET = "UTF-8";

  private static final Logger LOG = getLogger(BatchProcessor.class);
  
  private static final Integer ZERO = new Integer(0);
  
  private final List<File> forDeletion;
  
  private final NormalExecutor normalExecutor;
  
  private SendMailExecutor sendMailExecutor;
  
  private FillBufferExecutor fillBufferExecutor;

  private File origen;
  
  private ApplicationContext ctx;

  private DataSource ds;

  private boolean compress;
  
  private int threadsForBuffer;

  
  /**
   * Batch sobre el que este hilo va a trabajar
   */
  private Batch batch;

  private File workdirectory;
  
  private Future<Integer> future;
  
  private long stopTime;
  
  private long startTime;
  
  public BatchProcessor() {
  	forDeletion = new ArrayList<>();
  	normalExecutor = new NormalExecutor();
  }
  
  public void setBatch(Batch batch) {
    this.batch = batch;
  }
  
  public void setWorkdirectory(File workdirectory) {
    this.workdirectory = workdirectory;
  }

  public void setFuture(Future<Integer> future) {
    this.future = future;
  }
  
  public Batch getBatch() {
  	return batch;
  }
  
  public Integer getResult() throws ExecutionException, InterruptedException {
  	return future.get();
  }
  
  public boolean isDone() {
  	return future.isDone();
  }
  
  public void cancel() {
  	future.cancel(true);
  }
  
  public void setOrigen(File origen) {
  	this.origen = origen; 
  }

  @Override
  public Integer call() throws Exception {
    startTimeCounters();
    if(!confirmAlive()) {
      return ZERO;
    }
    LOG.debug("[call] Inicio batch codigo {}", batch.getCode());
    if (!batch.isActive()) {
      LOG.info("[call] Se salta el batch {} porque esta inactivo", batch.getCode());
      return ZERO;
    }
    if (workdirectory.mkdirs()) {
      LOG.info("[call] Se han creado los directorios necesarios de trabajo");
    }
    if (!workdirectory.exists() || !workdirectory.isDirectory()) {
      throw new BatchException(format("La ruta {0} no existe o no es un directorio", workdirectory.getName()));
    }
    try {
      for(Step currentStep : batch.getSteps()) {
        if(!confirmAlive()) {
          break;
        }
        openExecutors(currentStep);
        currentStep.execute(this);
        closeExecutors();
      }
    }
    finally {
      disposeFiles();
      if(fillBufferExecutor != null) {
        fillBufferExecutor.close();
      }
      normalExecutor.close();
    }
    LOG.debug("[call] Fin batch codigo {}", batch.getCode());
    return new Integer(normalExecutor.getAffectedRows());
  }

  @Override
  protected void executeStatement(String query, char type) throws BatchException {
    final DestinyExecutor executor;
    String message;
    
    executor = getExecutor(type);
    try(Connection con = ds.getConnection()) {
      try {
        con.setAutoCommit(false);
        executor.executeStatement(con, query);
        con.commit();
      }  
      catch(BatchException ex) {
        LOG.warn("[executeStatement] Error de regla de proceso de Batch capturado para la sentencia [{}]: {}",
            query, "Se cancela la transacción y se relanza excepcion", ex);
        con.rollback();
        throw ex;
      }
      catch(RuntimeException ex) {
        message = format("Error no controlado al ejecutar sentencia [{0}]", query);
        LOG.error("[executeStatement]{}", message, ex);
        con.rollback();
        throw new BatchException(message, ex);
      }
    }
    catch(SQLException | RuntimeException ex) {
      message = format(
          "Error en operación de conexión de base de datos (obtener o devolver) para ejecutar la sentencia [{0}]", 
          query);
      LOG.error("[executeStatement] {}", message, ex);
      throw new BatchException(message, ex);
    }
  }
  
  @Override
  protected Reader readerFromOrigin() throws BatchException, IOException {
    if (origen == null) {
      throw new BatchException("No existe un fichero con resultados previos");
    }
    return new BufferedReader(new InputStreamReader(new FileInputStream(origen), CHAR_SET));
  }
  
  @Override
  protected boolean confirmAlive() throws TimeOutBatchException {
    final String message;
    final long actualTime;
    
    if(future != null && future.isCancelled()) {
      message = format("Se recibe que proceso del Batch {} ha sido cancelado", batch.getCode());
      LOG.warn("[confirmAlive] {}", message);
      return false;
    }
    actualTime = currentTimeMillis();
    if(actualTime > stopTime) {
      throw throwTimeOut("confirmAlive", actualTime);
    }
    return true;
  }
  
  @Override
  protected void batchExecute(Iterable<String> queries) throws BatchException {
    try(Connection con = ds.getConnection()) {
      normalExecutor.batchExecute(con, queries);
    }
    catch(SQLException sql) {
      throw new BatchException("Error al recuperar conexión de base de datos para ejecución en batch");
    }
  }
  
  @Override
  protected int getThreadsForBuffer() {
    return threadsForBuffer;
  }
  
  @Autowired
  void setDs(DataSource ds) {
    this.ds = ds;
  }
  
  @Autowired
  void setApplicationContext(ApplicationContext ctx) {
    this.ctx = ctx;
  }
  
  @Value("${batch.workdirectory ?: /sbrmv/ficheros/batch}")
  void setWorkDirectory(String workdirectory) {
    this.workdirectory = new File(workdirectory);
  }

  @Value("${batch.compress : true}")
  void setCompress(boolean compress) {
    this.compress = compress;
  }
  
  @Value("${batch.threadsForBuffer : 5}")
  void setThreadsForBuffer(int threadsForBuffer) {
    this.threadsForBuffer = threadsForBuffer;
  }
  
  void startTimeCounters() throws TimeOutBatchException {
    requireNonNull(batch);
    startTime = currentTimeMillis();
    stopTime = startTime + (batch.getMaxTime() * 1000);
  }

  private DestinyExecutor getExecutor(char type) {
    final DestinyExecutor executor;
    
    switch(type) {
      case Query.NORMAL:
        executor = normalExecutor;
        break;
      case Query.TO_BUFFER:
        executor = fillBufferExecutor;
        break;
      case Query.TO_MAIL:
        executor = sendMailExecutor;
        break;
      default:
        throw new IllegalAccessError(format("Tipo de query erroneo: {0}", type));
    }
    if(executor == null) {
      throw new IllegalAccessError(format("Se ha solicitado un executor fuera de secuencia: {0}", type));
    }
    return executor;
  }

  private void openExecutors(Step currentStep) throws BatchException {
    if(currentStep.sendToMail()) {
      sendMailExecutor = ctx.getBean(SendMailExecutor.class);
      sendMailExecutor.setMaxSendedMails(batch.geMaxMails());
      sendMailExecutor.setTo(batch.receptorList());
      sendMailExecutor.setSubject(batch.getSubject());
    }
    if(currentStep.readBuffer() && fillBufferExecutor != null) {
      fillBufferExecutor.close();
      origen = fillBufferExecutor.getFile();
      forDeletion.add(origen);
      fillBufferExecutor = null;
    }
    if(currentStep.sentToBuffer() && fillBufferExecutor == null) {
      createBufferFile();
    }
  }
  
  private void closeExecutors() {
    if(sendMailExecutor != null) {
      sendMailExecutor.close();
      sendMailExecutor = null;
    }
  }

  private TimeOutBatchException throwTimeOut(String sender, long actualTime) {
    return throwTimeOut(sender, actualTime, null);
  }
  
  private TimeOutBatchException throwTimeOut(String sender, long actualTime, Exception cause)  {
    final String message;
    
    message = format(
        "El proceso batch {0} se cancela por llegar al tiempo limite {1} s transcurridos {2} ms", 
        batch.getCode(), batch.getMaxTime(), actualTime - startTime);
    LOG.warn("[{}] {}", sender, message);
    return new TimeOutBatchException(message, cause);
  }
  
  private void disposeFiles() {
    final Thread thread;
    final String threadName;
    
    try {
      threadName = new StringBuilder(currentThread().getName()).append("_disposer").toString();
      thread = new Thread(new FileDisposer(workdirectory, forDeletion, batch.getCode(), compress));
      thread.setName(threadName);
      thread.start();
    }
    catch(RuntimeException rex) {
      LOG.error("[disposeFiles] Error al iniciar hilo para tratar ficheros intermedios del batch {}", batch.getCode(),
          rex);
    }
  }
  
  private void createBufferFile() throws BatchException{
		final String temporalNamePrefix, message;
		final File destino;
		
		temporalNamePrefix = format("BUFF_{0}_", batch.getCode());
		try {
	    destino = File.createTempFile(temporalNamePrefix, ".csv", workdirectory);
	    fillBufferExecutor = new FillBufferExecutor(destino);
		}
		catch(IOException ioex) {
			message = format("Error al crear fichero temporal de buffer con prefijo {0}", temporalNamePrefix);
			LOG.error("[bufferForward] {}", message, ioex);
			throw new BatchException(message, ioex);
		}
  }

}
  	

