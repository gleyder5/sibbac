package sibbac.business.daemons.batch.model;

import static java.util.concurrent.Executors.newFixedThreadPool;
import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import sibbac.business.daemons.batch.common.BatchException;

/**
 * Clase base de los Steps de Batch que leen información desde un fichero buffer
 * intermedio. Gestiona el control multihilo.
 */
abstract class BufferedStep extends AbstractStep {

  /**
   * Bitácora de la clase
   */
  private static final Logger LOG = getLogger(BufferedStep.class);

  /**
   * Cola bloqueante que coordina los Steps concurrentes
   */
  private BlockingQueue<ConcurrentStep> tqueue;

  /**
   * Entorno de ejecucion del batch
   */
  private BatchEnvironment environment;

  /**
   * Número total de hilos que ejecutan Steps concurrentes
   */
  private int totalThreads;

  /**
   * @see Step#execute(BatchEnvironment)
   */
  @Override
  public void execute(final BatchEnvironment environment) throws BatchException {
    this.environment = environment;
    totalThreads = environment.getThreadsForBuffer();
    tqueue = new ArrayBlockingQueue<>(totalThreads);
    super.execute(environment);
  }

  /**
   * Indica que se requiere lectura desde un fichero buffer intermedio
   * @return verdadero para todas las subclases
   */
  @Override
  public final boolean readBuffer() {
    return true;
  }

  abstract void execute(String values[]) throws BatchException;

  @Override
  void execute() throws BatchException {
    final String message;
    final FromBufferParamLoader paramLoader;
    final ExecutorService executor;
    ConcurrentStep concurrentStep;
    Future<ConcurrentStep> f;
    String[] values;

    executor = newFixedThreadPool((totalThreads * 2) + 1, new ThreadFactory() {
      private int i = 0;

      @Override
      public Thread newThread(Runnable r) {
        String n = String.format("%s_%d", Thread.currentThread().getName(), i++);
        return new Thread(r, n);
      }
    });
    try {
      for (int i = 0; i < totalThreads; i++) {
        tqueue.put(new ConcurrentStep());
      }
      try (Reader reader = readerFromOrigin()) {
        paramLoader = new FromBufferParamLoader(reader);
        executor.submit(paramLoader);
        while ((values = paramLoader.loadParams()).length > 0) {
          concurrentStep = tqueue.take();
          if (!concurrentStep.isAlive()) {
            break;
          }
          concurrentStep.setValues(values);
          f = executor.submit(concurrentStep);
          executor.submit(new Returner(f));
        }
      }
      executor.shutdown();
      executor.awaitTermination(60, TimeUnit.SECONDS);
    }
    catch (IOException ioex) {
      message = "Error de lectura de buffer";
      LOG.error("[execute] {}", message, ioex);
      throw new BatchException(message, ioex);
    }
    catch (InterruptedException iex) {
      throw new BatchException("Error al lanzar los hilos batch", iex);
    }
  }

  Reader readerFromOrigin() throws BatchException, IOException {
    return environment.readerFromOrigin();
  }

  void fillParameters(StringBuilder sqlBuilder, Iterable<BatchParam> itParams, String[] values) {
    int pos;

    for (BatchParam param : itParams) {
      pos = param.getId().getParam();
      if (pos >= 1) {
        if (pos <= values.length) {
          param.fillParameterWithString(sqlBuilder, values[pos - 1]);
        }
        else {
          param.autoFill(sqlBuilder);
        }
      }
      else {
        LOG.trace("[fillParameters] Se omite el parametro {}", param.getIdString());
      }
    }
  }

  void batchExecute(Iterable<String> queries) throws BatchException {
    environment.batchExecute(queries);
  }

  private class Returner implements Runnable {

    private final Future<ConcurrentStep> f;

    Returner(Future<ConcurrentStep> f) {
      this.f = f;
    }

    @Override
    public void run() {
      final ConcurrentStep cs;

      try {
        try {
          cs = f.get();
          tqueue.put(cs);
        }
        catch (ExecutionException e) {
          LOG.error("Error inesperado en recuperacion de hilo", e.getCause());
          tqueue.put(new ConcurrentStep(false));
        }
      }
      catch (InterruptedException iex) {
        LOG.error("[Returner run] Se ha interrumpido el hilo returner", iex);
      }
    }

  }

  private class ConcurrentStep implements Callable<ConcurrentStep> {

    private boolean alive;

    private String[] values;

    ConcurrentStep() {
      alive = true;
    }

    ConcurrentStep(boolean alive) {
      this.alive = alive;
    }

    @Override
    public ConcurrentStep call() {
      if (!alive) {
        return this;
      }
      try {
        execute(values);
      }
      catch (BatchException | RuntimeException ex) {
        LOG.error("[ConcurrentStep.call] Error en la ejecucion de un paso", ex);
      }
      return this;
    }

    void setValues(String values[]) {
      if (values == null) {
        this.values = new String[0];
      }
      else {
        this.values = Arrays.copyOf(values, values.length);
      }
    }

    boolean isAlive() {
      return alive;
    }

  }

}
