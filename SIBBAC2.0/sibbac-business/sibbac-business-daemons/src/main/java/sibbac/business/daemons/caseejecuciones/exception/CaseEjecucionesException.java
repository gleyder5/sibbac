/**
 * 
 */
package sibbac.business.daemons.caseejecuciones.exception;

/**
 * @author xIS16630
 *
 */
public class CaseEjecucionesException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 318260686382693190L;

	/**
	 * 
	 */
	public CaseEjecucionesException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public CaseEjecucionesException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public CaseEjecucionesException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public CaseEjecucionesException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public CaseEjecucionesException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
