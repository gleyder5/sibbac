package sibbac.business.daemons.ork.common;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ResultSetIterator {
	
	void iterate(ResultSet resultSet) throws SQLException;

}
