package sibbac.business.daemons.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.commons.io.IOUtils;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControlTaskEstaticos;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.jms.JmsMessagesBo;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_ESTATICOS_ENVIO_FICHEROS_FDA_FIDESSA, jobType = SIBBACJobType.CRON_JOB, cronExpression = "30 * 7-23 ? * MON-FRI")
public class TaskEstaticosMegaraEnvioFdaFidessa extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEstaticosMegaraEnvioFdaFidessa.class);

  @Value("${estaticos.fda.folder.out:}")
  private String rutaOutFda;

  @Value("${estaticos.fda.jndi.name:}")
  private String jndiName;

  @Value("${estaticos.fda.mq.host:}")
  private String host;

  @Value("${estaticos.fda.mq.port:}")
  private int port;
  @Value("${estaticos.fda.mq.channel:}")
  private String channel;

  @Value("${estaticos.fda.mq.manager:}")
  private String manager;

  @Value("${estaticos.fda.mq.user:}")
  private String user;

  @Value("${estaticos.fda.mq.password:}")
  private String pass;

  @Value("${estaticos.fda.mq.queue:}")
  private String destination;

  @Value("${estaticos.fda.out.file.pattern:}")
  private String filePattern;

  @Autowired
  @Qualifier(value = "JmsMessagesBo")
  private JmsMessagesBo jmsMessagesBo;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private RegistroControlTaskEstaticos registroControlTaskEstaticosBo;

  @Override
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio.");
    String text;
    int count = 0;
    ZipEntry entry;
    TextMessage tm;
    RegistroControl control;
    Path pathIn = Paths.get(rutaOutFda);
    Path tratados = Paths.get(rutaOutFda, "tratados");
    if (Files.notExists(tratados)) {
      Files.createDirectories(tratados);
    }
    Path backup = Paths.get(tratados.toString(),
        String.format("backup_%s.zip", FormatDataUtils.convertDateToConcurrentFileName(new Date())));
    LOG.trace("[executeTask] generando nombre fichero backup: {}.", backup.toString());
    ConnectionFactory cf = null;
    try {
      cf = jmsMessagesBo.getConnectionFactoryFromContext(jndiName);
    }
    catch (Exception e1) {
      cf = jmsMessagesBo.getNewJmsConnectionFactory(host, port, channel, manager, user, pass);
    }

    if (cf == null) {
      throw new SIBBACBusinessException("No se ha podido establecer la conexion jms con el fda de fidessa.");
    }
    try (Connection connection = jmsMessagesBo.getNewConnection(cf)) {
      connection.start();
      try (Session session = jmsMessagesBo.getNewSession(connection, true)) {
        try (MessageProducer mp = jmsMessagesBo.getNewProducer(session, destination)) {

          try (OutputStream os = Files.newOutputStream(backup, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
              ZipOutputStream zis = new ZipOutputStream(os);) {
            LOG.trace("[executeTask] buscando ficheros con el pattern...");
            control = ctx.getBean(RegistroControl.class);
            try (DirectoryStream<Path> files = Files.newDirectoryStream(pathIn, filePattern)) {
              for (Path path : files) {
                try {
                  // ENVIAR POR MQ
                  LOG.info("[executeTask] enviando fichero: {}...", path.toString());

                  tm = session.createTextMessage();
                  jmsMessagesBo.setBasicJmsMQMDProperties(tm, 1208);
                  try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);) {
                    text = String.format("%s %s", path.getFileName().toString(),
                        IOUtils.toString(is, StandardCharsets.ISO_8859_1.name()));
                    tm.setText(text);
                  }
                  jmsMessagesBo.sendJmsMessage(mp, tm);

                  LOG.trace("[executeTask] aniadiendo a backup{} el fichero: {}...", backup.toString(), path.toString());
                  entry = new ZipEntry(path.getFileName().toString());
                  zis.putNextEntry(entry);
                  try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);) {
                    IOUtils.copyLarge(is, zis);
                  }
                  zis.closeEntry();
                  LOG.trace("[executeTask] borrando fichero origen: {}...", path.toString());
                  Files.deleteIfExists(path);

                  tm.acknowledge();
                  session.commit();
                  count++;

                  registroControlTaskEstaticosBo.generarRegistroControl(control, path.getFileName().toString(),
                      "Enviado FDA", "P");

                }
                catch (Exception e) {
                  registroControlTaskEstaticosBo.generarRegistroControl(control, path.getFileName().toString(),
                      e.getMessage(), "E");
                  throw e;
                }
              }
            }
          }
          catch (IOException e) {

            throw e;
          }
          finally {
            if (count == 0) {
              LOG.info("[executeTask] borrando fichero backup: {}, no se ha enviado nada...", backup.toString());
              Files.deleteIfExists(backup);
            }
          }

        }
        catch (Exception e) {
          throw e;
        }
        finally {
          session.rollback();
        }
      }
    }

    LOG.info("[executeTask] fin.");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_ENVIO_FICHEROS_ESTATICOS_FDA_FIDESSA;
  }

}
