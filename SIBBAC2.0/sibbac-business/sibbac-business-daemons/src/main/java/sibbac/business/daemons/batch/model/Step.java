package sibbac.business.daemons.batch.model;

import sibbac.business.daemons.batch.common.BatchException;

/**
 * Representa un paso en la ejecución del Batch. Cada paso puede estar compuesto
 * por una o varias queries. 
 */
public interface Step {
	
  /**
   * Cuerpo principal del Step, ejecuta la o las sentencias asociadas, comunicandose
   * con el entorno (environment)
   * @param Entorno que ejecuta el código SQL de las queries y controla el
   * proceso
   * @throws BatchException En caso de error de lógica expecífica del Batch
   */
	void execute(BatchEnvironment environment) throws BatchException;
	
	/**
	 * Indica si este paso envía correos electrónicos
	 * @return depende de la subclase
	 */
	boolean sendToMail();
	
	/**
	 * Indica si este paso envía la información a un fichero buffer intermedio
	 * @return depende de la subclase
	 */
	boolean sentToBuffer();
	
	/**
	 * Indica si este paso lee desde un fichero buffer intermedio
	 * @return depende de la subclase
	 */
	boolean readBuffer();
  
}
