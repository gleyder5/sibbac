package sibbac.business.daemons.ork.bo;

import java.util.concurrent.ThreadFactory;
import static java.lang.Thread.currentThread;
import static java.text.MessageFormat.format;

class ResolverThreadFactory implements ThreadFactory {

	private final String pattern;
	
	private int count;
	
	ResolverThreadFactory(String suffix) {
		count = 0;
		pattern = new StringBuilder()
				.append(currentThread().getName())
				.append('_')
				.append(suffix)
				.append("_{0}").toString();
	}
	
	@Override
	public Thread newThread(Runnable runnable) {
		return new Thread(runnable, format(pattern, count++));
	}

}
