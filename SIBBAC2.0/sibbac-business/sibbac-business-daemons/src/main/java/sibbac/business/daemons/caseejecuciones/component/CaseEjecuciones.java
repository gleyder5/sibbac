package sibbac.business.daemons.caseejecuciones.component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.caseejecuciones.util.CaseEjecucionesKeyMapDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.fase0.database.dao.Tmct0cfgDao;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0bokIdDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0caseoperacionejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0ejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;

@Service
public class CaseEjecuciones {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  protected static final Logger LOG = LoggerFactory.getLogger(CaseEjecuciones.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  /** Número máximo de ejecuciones a procesar en una transacción. */
  @Value("${daemons.caseejecuciones.transaction.size}")
  private int transaction_size;

  /** Bussines object de ejecuciones <code>Tmct0eje</code> */
  @Autowired
  private Tmct0ejeBo tmct0ejeBo;

  /** Bussines object de desglosescamara <code>Tmct0desglosecamara</code> */
  @Autowired
  private Tmct0desgloseCamaraBo tmct0desglosecamaraBo;

  /** Bussines object de semaforos <code>Tmct0men</code> */
  @Autowired
  private Tmct0menBo tmct0menBo;

  /** Bussines object de configuracion en bbdd <code>Tmct0cfg</code> */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  /** Bussines object de configuracion en bbdd <code>Tmct0cfg</code> */
  @Autowired
  private Tmct0cfgDao tmct0cfgDao;

  /** Bussines object de case ejecuiones en sublistas transaccionales */
  @Autowired
  private CaseEjecucionesSubList caseejecucionesSubList;

  /** Lista de días configurados como festivos. */
  private List<Date> holidaysConfigList = null;

  /** Lista de dias configurados como laborables. */
  private List<Integer> workDaysOfWeekConfigList = null;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PÚBLICOS

  /**
   * Metodo principal del case ejecuciones y bookings</br>Recupera la lista de ejccuiones sin casar y las casa
   */
  @Transactional
  public void execute() {
    final Long startTime = System.currentTimeMillis();
    LOG.debug("execute() ## Inicio");
    Tmct0men semaforo;
    try {

      LOG.trace("execute() ## Bloqueando semaforo {} ...", TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES);
      semaforo = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES, TMCT0MSC.LISTO_GENERAR,
                                         TMCT0MSC.EN_EJECUCION);
      Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases = new HashMap<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO>();

      try {
        LOG.trace("execute() ## Tamaño de transaccion == {}", transaction_size);

        int daysPrevious = 10;
        LOG.trace("execute() ## Recuperando la cfg {} ...", Tmct0cfgConfig.PTI_CASE_SCAN_DAYS_PREV);

        try {
          Tmct0cfg datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                                        Tmct0cfgConfig.PTI_CASE_SCAN_DAYS_PREV.getProcess(),
                                                                                        Tmct0cfgConfig.PTI_CASE_SCAN_DAYS_PREV.getKeyname());
          daysPrevious = Integer.parseInt(datosConfiguracion.getKeyvalue().trim());
        } catch (Exception e) {
          LOG.warn("execute() ## Problema al recuperar la tmct0cfg.", e);
        }

        Date sinceDate = DateHelper.getPreviousWorkDate(new Date(), daysPrevious, getWorkDaysOfWeek(), getHolidays());

        LOG.trace("execute() ## Buscando tmct0eje que casar...");
        this.casarEjecuciones(sinceDate, cases);

        LOG.trace("execute() ## Buscando tmct0desglosecamara que casar...");
        this.casarDesglosesCamara(sinceDate, cases);

        LOG.trace("execute() ## Cambiando estado a TMCT0MSC.LISTO_GENERAR ...");
        tmct0menBo.putEstadoMENAndIncrement(semaforo, TMCT0MSC.LISTO_GENERAR);
      } catch (Exception e) {
        LOG.warn("execute() ", e);
        tmct0menBo.putEstadoMEN(semaforo, TMCT0MSC.EN_ERROR);
      } finally {
        if (cases != null) {
          cases.clear();
        }
      } // finally
    } catch (SIBBACBusinessException e) {
      LOG.warn("execute() No se puede bloquear el semaforo ## {} ## {}", TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES,
               e.getMessage());
    } catch (Exception e) {
      LOG.warn("execute() ", e);
    } finally {

      final Long endTime = System.currentTimeMillis();
      LOG.info("[execute] Finalizada la tarea de Case ... ["
               + String.format("%d min, %d sec",
                               TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
                               TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                                   - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime)))
               + "]");
    } // finally
    LOG.debug("execute() ## Fin");
  } // execute

  @Transactional
  private void casarEjecuciones(Date sinceDate,
                                Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) {
    LOG.trace("casarEjecuciones() ## Buscando tmct0eje que casar desde {} ...", sinceDate);

    List<Tmct0ejeInformacionMercadoCaseDTO> ejecuciones = tmct0ejeBo.findAllCasePendingSinceDate(sinceDate);

    int posIni = 0;
    int posEnd = transaction_size;

    if (ejecuciones != null && !ejecuciones.isEmpty()) {
      LOG.debug("casarEjecuciones() ## Encontradas {} ejecucuiones que casar.", ejecuciones.size());
      List<Tmct0ejeInformacionMercadoCaseDTO> ejecucionesSubList = null;

      if (transaction_size > ejecuciones.size()) {
        posEnd = ejecuciones.size();
      }

      while (posIni < posEnd) {
        LOG.trace("casarEjecuciones() ## Ejecutando sublist posIni = {} posEnd = {} tamaño total = {}", posIni, posEnd,
                  ejecuciones.size());
        ejecucionesSubList = ejecuciones.subList(posIni, posEnd);

        try {
          caseejecucionesSubList.casarEjecuciones(ejecucionesSubList, cases);
          tmct0menBo.updateFhauditMEN(TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES);
        } catch (Exception e) {
          LOG.warn("Incidencia casando ejecuciones casarEjecuciones()", e);
          cases.clear();
          if (transaction_size > 1) {
            LOG.trace("casarEjecuciones() El bloque de tamaño {} ha fallado voy a intentarlo de 1 en 1.",
                      transaction_size);
            executeCaseAfterBlockException(ejecucionesSubList, cases);
          }
        } // catch

        posIni = posEnd;
        if (ejecuciones.size() < posIni + transaction_size) {
          posEnd = ejecuciones.size();
        } else {
          posEnd += transaction_size;
        }
      } // while (posIni < posEnd)
      ejecuciones.clear();

    } else {
      LOG.trace("casarEjecuciones() ## No se ha encontrado nada que procesar.");

    }
  }

  @Transactional
  private void casarDesglosesCamara(Date sinceDate,
                                    Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) {
    LOG.trace("casarDesglosesCamara() ## Buscando tmct0desglosecamara que casar...");
    LOG.trace("casarDesglosesCamara() ## Buscando tmct0desglosecamara que casar desde {} ...", sinceDate);

    List<Tmct0bokIdDTO> desglosesCamaraSinCasar = tmct0desglosecamaraBo.findBookingsIdConDesglosesCamaraSinCasar(sinceDate);

    if (desglosesCamaraSinCasar != null && !desglosesCamaraSinCasar.isEmpty()) {
      int posIni = 0;
      int posEnd = transaction_size;
      LOG.debug("execute() ## Encontradas {} tmct0desglosecamara que casar.", desglosesCamaraSinCasar.size());

      List<Tmct0bokIdDTO> desglosesCamaraSubList = null;
      if (transaction_size > desglosesCamaraSinCasar.size()) {
        posEnd = desglosesCamaraSinCasar.size();
      }

      while (posIni < posEnd) {
        LOG.trace("casarDesglosesCamara() ## Ejecutando sublist posIni = {} posEnd = {} tamaño total = {}", posIni,
                  posEnd, desglosesCamaraSinCasar.size());
        desglosesCamaraSubList = desglosesCamaraSinCasar.subList(posIni, posEnd);
        try {
          caseejecucionesSubList.updateFlagsCasePtiDesgloseCamara(desglosesCamaraSubList, cases);
          tmct0menBo.updateFhauditMEN(TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES);
        } catch (Exception e) {
          LOG.warn("Incidencia casando ejecuciones casarDesglosesCamara()", e);
          if (transaction_size > 1) {
            LOG.trace("casarDesglosesCamara() El bloque de tamaño {} ha fallado voy a intentarlo de 1 en 1.",
                      transaction_size);
            executeCaseDesgloseCamaraAfterBlockException(desglosesCamaraSubList, cases);
          }
        } // catch

        posIni = posEnd;
        if (desglosesCamaraSinCasar.size() < posIni + transaction_size) {
          posEnd = desglosesCamaraSinCasar.size();
        } else {
          posEnd += transaction_size;
        }
      } // while (posIni < posEnd)

      desglosesCamaraSinCasar.clear();
    } else {
      LOG.trace("casarDesglosesCamara() ## No se ha encontrado nada que procesar.");
    }
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS PRIVADOS

  /**
   * Metodo que ejecuta el case despues de haber ocurrido una excepcion en un bloque, el nuevo bloque transaccional es
   * de 1 en 1
   * 
   * @param ejecucionesSubList
   * @param cases
   */
  @Transactional
  private void executeCaseAfterBlockException(List<Tmct0ejeInformacionMercadoCaseDTO> ejecucionesSubList,
                                              Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) {
    LOG.debug("executeCaseAfterBlockException() ## Inicio");
    int countErroresEnTransacion = 0;
    List<Tmct0ejeInformacionMercadoCaseDTO> ejecucionesSubListException = null;
    for (int i = 0; i < ejecucionesSubList.size(); i++) {
      if (countErroresEnTransacion < transaction_size / 10) {
        ejecucionesSubListException = ejecucionesSubList.subList(i, i + 1);
        try {
          caseejecucionesSubList.casarEjecuciones(ejecucionesSubListException, cases);
        } catch (Exception e) {
          cases.clear();
          LOG.warn("executeCaseAfterBlockException()", e);
          countErroresEnTransacion++;
        }
      } else {
        LOG.warn("executeCaseAfterBlockException() ## Se han producido demasiados errores, abortamos el bloque.");
        break;
      }
    }
    LOG.debug("executeCaseAfterBlockException() ## Fin");
  }

  /**
   * Metodo que ejecuta el case despues de haber ocurrido una excepcion en un bloque, el nuevo bloque transaccional es
   * de 1 en 1
   * 
   * @param ejecucionesSubList
   * @param cases
   */
  @Transactional
  private void executeCaseDesgloseCamaraAfterBlockException(List<Tmct0bokIdDTO> desgloses,
                                                            Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) {
    LOG.debug("executeCaseDesgloseCamaraAfterBlockException() ## Inicio");
    int countErroresEnTransacion = 0;
    List<Tmct0bokIdDTO> desglosesSubList = null;
    for (int i = 0; i < desgloses.size(); i++) {
      if (countErroresEnTransacion < transaction_size / 10) {
        desglosesSubList = desgloses.subList(i, i + 1);
        try {
          caseejecucionesSubList.updateFlagsCasePtiDesgloseCamara(desglosesSubList, cases);
        } catch (Exception e) {
          cases.clear();
          LOG.warn("executeCaseDesgloseCamaraAfterBlockException()", e);
          countErroresEnTransacion++;
        }
      } else {
        LOG.warn("executeCaseDesgloseCamaraAfterBlockException() ## Se han producido demasiados errores, abortamos el bloque.");
        break;
      }
    }
    LOG.debug("executeCaseDesgloseCamaraAfterBlockException() ## Fin");
  }

  /**
   * Obtiene una lista con los días de vacaciones configurados en la tabla tmct0cfg.
   * 
   * @return <code>List<Date> - </code>Lista de días de vacaciones.
   */
  private List<Date> getHolidays() {
    if (holidaysConfigList == null) {
      // Días de vacaciones configurados
      List<Tmct0cfg> configList = tmct0cfgDao.findByAplicationAndProcessAndLikeKeyname("SBRMV", "CONFIG",
                                                                                       "holydays.anual.days%");
      if (configList != null && !configList.isEmpty()) {
        holidaysConfigList = new ArrayList<Date>(configList.size());

        for (Tmct0cfg tmct0cfg : configList) {
          holidaysConfigList.add(FormatDataUtils.convertStringToDate(tmct0cfg.getKeyvalue(), "yyyy-MM-dd"));
        } // for
      } else {
        holidaysConfigList = new ArrayList<Date>();
      } // else
    } // if

    return holidaysConfigList;
  } // getHolidays

  /**
   * Obtiene una lista con los días de laborables configurados en la tabla tmct0cfg.
   * 
   * @return <code>List<Integer> - </code>Lista de días de la semana laborables.
   */
  private List<Integer> getWorkDaysOfWeek() {
    if (workDaysOfWeekConfigList == null) {
      // Días de la semana laborables configurados
      Tmct0cfg tmct0cfg = tmct0cfgDao.findByAplicationAndProcessAndKeyname("SBRMV", "CONFIG", "week.work.days");

      if (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && !tmct0cfg.getKeyvalue().isEmpty()) {
        String[] workDays = tmct0cfg.getKeyvalue().split(",");
        workDaysOfWeekConfigList = new ArrayList<Integer>(workDays.length);

        for (String workDay : workDays) {
          workDaysOfWeekConfigList.add(Integer.valueOf(workDay));
        } // for
      } else {
        workDaysOfWeekConfigList = new ArrayList<Integer>();
      } // else
    } // if

    return workDaysOfWeekConfigList;
  } // getWorkDaysOfWeek

} // CaseEjecuciones
