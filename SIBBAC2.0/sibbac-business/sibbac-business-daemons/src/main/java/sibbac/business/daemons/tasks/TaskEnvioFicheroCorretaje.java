package sibbac.business.daemons.tasks;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.fichero.corretajes.bo.FicheroCCBo;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

//@SIBBACJob(group = Task.GROUP_DAEMONS.NAME,
//           name = Task.GROUP_DAEMONS.JOB_ENVIO_FICHERO_CORRETAJE,
//           intervalUnit = IntervalUnit.DAY,
//           interval = 1,
//           startTime = "18:00:00.00")
//@DisallowConcurrentExecution
//@SIBBACJob(group = Task.GROUP_DAEMONS.NAME,
//           name = Task.GROUP_DAEMONS.JOB_ENVIO_FICHERO_CORRETAJE,
//           intervalUnit = IntervalUnit.MINUTE,
//           interval = 1)
public class TaskEnvioFicheroCorretaje extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  private static final Logger LOG = LoggerFactory.getLogger(TaskEnvioFicheroCorretaje.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private FicheroCCBo ficheroCCBo;

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void execute() {

    LOG.info("execute() Inicio.");
    ficheroCCBo.envioFicheroCC();
    LOG.info("execute() Fin.");
  }

}
