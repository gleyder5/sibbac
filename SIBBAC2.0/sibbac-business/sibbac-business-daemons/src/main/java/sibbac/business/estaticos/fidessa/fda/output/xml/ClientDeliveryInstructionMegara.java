package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * ClientDeliveryInstructionMegara.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class ClientDeliveryInstructionMegara extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static Logger logger = LoggerFactory.getLogger(ClientDeliveryInstructionMegara.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The client_mnemonic. */
  private String client_mnemonic;

  /** The account_mnemonic. */
  private String account_mnemonic;

  /** The mnemonic. */
  private String mnemonic;

  /** The entity_name. */
  private String entity_name;

  /** The entity_bic. */
  private String entity_bic;

  /** The market. */
  private String market;

  /** The market_name. */
  private String market_name;

  /** The place_of_settlement. */
  private String place_of_settlement;

  /** The settlement_type. */
  private String settlement_type;

  /** The local_cust_mn. */
  private String local_cust_mn;

  /** The local_cust_name. */
  private String local_cust_name;

  /** The local_cust_bic. */
  private String local_cust_bic;

  /** The local_cust_acc. */
  private String local_cust_acc;

  /** The local_cust_bic_id. */
  private String local_cust_bic_id;

  /** The global_cust_mn. */
  private String global_cust_mn;

  /** The global_cust_name. */
  private String global_cust_name;

  /** The global_cust_bic. */
  private String global_cust_bic;

  /** The global_cust_acc. */
  private String global_cust_acc;

  /** The global_cust_bic_id. */
  private String global_cust_bic_id;

  /** The beneficiary_mn. */
  private String beneficiary_mn;

  /** The beneficiary_name. */
  private String beneficiary_name;

  /** The beneficiary_bic. */
  private String beneficiary_bic;

  /** The beneficiary_acc. */
  private String beneficiary_acc;

  /** The beneficiary_bic_id. */
  private String beneficiary_bic_id;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  // * MCG 26-04-2010 */
  /* INCLUYO FECHA DE VALIDEZ */

  private String efectiveFrom;
  private String efectiveTo;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new client delivery instruction_megara.
   */
  protected ClientDeliveryInstructionMegara() {

    this.tipoActualizacion = "";
    this.client_mnemonic = "";
    this.account_mnemonic = "";
    this.mnemonic = "";
    this.entity_name = "";
    this.entity_bic = "";
    this.market = "";
    this.market_name = "";
    this.place_of_settlement = "";
    this.settlement_type = "";
    this.local_cust_mn = "";
    this.local_cust_name = "";
    this.local_cust_bic = "";
    this.local_cust_acc = "";
    this.local_cust_bic_id = "";
    this.global_cust_mn = "";
    this.global_cust_name = "";
    this.global_cust_bic = "";
    this.global_cust_acc = "";
    this.global_cust_bic_id = "";
    this.beneficiary_mn = "";
    this.beneficiary_name = "";
    this.beneficiary_bic = "";
    this.beneficiary_acc = "";
    this.beneficiary_bic_id = "";

    /* MCG 26-04-2010 */
    /* INCLUYO FECHA DE VALIDEZ */

    this.efectiveFrom = "";
    this.efectiveTo = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("clientDeliveryInstruction: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element CLIENT_DELIVERY_INSTRUCTIONS = document.createElement("CLIENT_DELIVERY_INSTRUCTIONS");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element CLIENT_MNEMONIC = document.createElement("CLIENT_MNEMONIC");
      CLIENT_MNEMONIC.appendChild(document.createTextNode(getClient_mnemonic()));

      Element ACCOUNT_MNEMONIC = document.createElement("ACCOUNT_MNEMONIC");
      ACCOUNT_MNEMONIC.appendChild(document.createTextNode(getAccount_mnemonic()));

      Element MNEMONIC = document.createElement("MNEMONIC");
      MNEMONIC.appendChild(document.createTextNode(getMnemonic()));

      Element ENTITY_NAME = document.createElement("ENTITY_NAME");
      ENTITY_NAME.appendChild(document.createTextNode(getEntity_name()));

      Element ENTITY_BIC = document.createElement("ENTITY_BIC");
      ENTITY_BIC.appendChild(document.createTextNode(getEntity_bic()));

      Element MARKET = document.createElement("MARKET");
      MARKET.appendChild(document.createTextNode(getMarket()));

      Element MARKET_NAME = document.createElement("MARKET_NAME");
      MARKET_NAME.appendChild(document.createTextNode(getMarket_name()));

      Element PLACE_OF_SETTLEMENT = document.createElement("PLACE_OF_SETTLEMENT");
      PLACE_OF_SETTLEMENT.appendChild(document.createTextNode(getPlace_of_settlement()));

      Element SETTLEMENT_TYPE = document.createElement("SETTLEMENT_TYPE");
      SETTLEMENT_TYPE.appendChild(document.createTextNode(getSettlement_type()));

      Element LOCAL_CUST_MN = document.createElement("LOCAL_CUST_MN");
      LOCAL_CUST_MN.appendChild(document.createTextNode(getLocal_cust_mn()));

      Element LOCAL_CUST_NAME = document.createElement("LOCAL_CUST_NAME");
      LOCAL_CUST_NAME.appendChild(document.createTextNode(getLocal_cust_name()));

      Element LOCAL_CUST_BIC = document.createElement("LOCAL_CUST_BIC");
      LOCAL_CUST_BIC.appendChild(document.createTextNode(getLocal_cust_bic()));

      Element LOCAL_CUST_ACC = document.createElement("LOCAL_CUST_ACC");
      LOCAL_CUST_ACC.appendChild(document.createTextNode(getLocal_cust_acc()));

      Element LOCAL_CUST_BIC_ID = document.createElement("LOCAL_CUST_BIC_ID");
      LOCAL_CUST_BIC_ID.appendChild(document.createTextNode(getLocal_cust_bic_id()));

      Element GLOBAL_CUST_MN = document.createElement("GLOBAL_CUST_MN");
      GLOBAL_CUST_MN.appendChild(document.createTextNode(getGlobal_cust_mn()));

      Element GLOBAL_CUST_NAME = document.createElement("GLOBAL_CUST_NAME");
      GLOBAL_CUST_NAME.appendChild(document.createTextNode(getGlobal_cust_name()));

      Element GLOBAL_CUST_BIC = document.createElement("GLOBAL_CUST_BIC");
      GLOBAL_CUST_BIC.appendChild(document.createTextNode(getGlobal_cust_bic()));

      Element GLOBAL_CUST_ACC = document.createElement("GLOBAL_CUST_ACC");
      GLOBAL_CUST_ACC.appendChild(document.createTextNode(getGlobal_cust_acc()));

      Element GLOBAL_CUST_BIC_ID = document.createElement("GLOBAL_CUST_BIC_ID");
      GLOBAL_CUST_BIC_ID.appendChild(document.createTextNode(getGlobal_cust_bic_id()));

      Element BENEFICIARY_MN = document.createElement("BENEFICIARY_MN");
      BENEFICIARY_MN.appendChild(document.createTextNode(getBeneficiary_mn()));

      Element BENEFICIARY_NAME = document.createElement("BENEFICIARY_NAME");
      BENEFICIARY_NAME.appendChild(document.createTextNode(getBeneficiary_name()));

      Element BENEFICIARY_BIC = document.createElement("BENEFICIARY_BIC");
      BENEFICIARY_BIC.appendChild(document.createTextNode(getBeneficiary_bic()));

      Element BENEFICIARY_ACC = document.createElement("BENEFICIARY_ACC");
      BENEFICIARY_ACC.appendChild(document.createTextNode(getBeneficiary_acc()));

      Element BENEFICIARY_BIC_ID = document.createElement("BENEFICIARY_BIC_ID");
      BENEFICIARY_BIC_ID.appendChild(document.createTextNode(getBeneficiary_bic_id()));

      /* AML 26-04-2010 */
      /* INCLUYO FECHA DE VALIDEZ */

      Element EFFECTIVE_FROM = document.createElement("EFFECTIVE_FROM");
      EFFECTIVE_FROM.appendChild(document.createTextNode(getDateFrom()));

      Element EFFECTIVE_TO = document.createElement("EFFECTIVE_TO");
      EFFECTIVE_TO.appendChild(document.createTextNode(getDateTo()));

      // ---------------------------------------------------------

      data.appendChild(CLIENT_MNEMONIC);
      data.appendChild(ACCOUNT_MNEMONIC);
      data.appendChild(MNEMONIC);
      data.appendChild(ENTITY_NAME);
      data.appendChild(ENTITY_BIC);
      data.appendChild(MARKET);
      data.appendChild(MARKET_NAME);
      data.appendChild(PLACE_OF_SETTLEMENT);
      data.appendChild(SETTLEMENT_TYPE);
      data.appendChild(LOCAL_CUST_MN);
      data.appendChild(LOCAL_CUST_NAME);
      data.appendChild(LOCAL_CUST_BIC);
      data.appendChild(LOCAL_CUST_ACC);
      data.appendChild(LOCAL_CUST_BIC_ID);
      data.appendChild(GLOBAL_CUST_MN);
      data.appendChild(GLOBAL_CUST_NAME);
      data.appendChild(GLOBAL_CUST_BIC);
      data.appendChild(GLOBAL_CUST_ACC);
      data.appendChild(GLOBAL_CUST_BIC_ID);
      data.appendChild(BENEFICIARY_MN);
      data.appendChild(BENEFICIARY_NAME);
      data.appendChild(BENEFICIARY_BIC);
      data.appendChild(BENEFICIARY_ACC);
      data.appendChild(BENEFICIARY_BIC_ID);

      /* AML 26-04-2010 */
      /* INCLUYO FECHA DE VALIDEZ */

      data.appendChild(EFFECTIVE_FROM);
      data.appendChild(EFFECTIVE_TO);

      // ---------------------------------------------------------

      CLIENT_DELIVERY_INSTRUCTIONS.appendChild(header);
      CLIENT_DELIVERY_INSTRUCTIONS.appendChild(data);

      document.appendChild(CLIENT_DELIVERY_INSTRUCTIONS);

      logger.debug("clientDeliveryInstruction: estructura documento XML completada");

      // ---------------------------------------------------------

      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("clientDeliveryInstruction: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("clientDeliveryInstruction: error en la generacion del docuemento XML");
      logger.warn("clientDeliveryInstruction: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a client delivery instruction";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a client delivery instruction";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a client delivery instruction";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("clientDeliveryInstruction: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, HAMMdd0000.xml donde
   * HA es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getClientDeliveryInstructions();
    Path path = Paths.get(getRutaDestino(), String.format("HA%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("ClientDeliveryInstructions: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the account_mnemonic.
   * 
   * @return the account_mnemonic
   */
  protected String getAccount_mnemonic() {
    return account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the account_mnemonic.
   * 
   * @param account_mnemonic the new account_mnemonic
   */
  protected void setAccount_mnemonic(String account_mnemonic) {
    this.account_mnemonic = account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the beneficiary_acc.
   * 
   * @return the beneficiary_acc
   */
  protected String getBeneficiary_acc() {
    return beneficiary_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the beneficiary_acc.
   * 
   * @param beneficiary_acc the new beneficiary_acc
   */
  protected void setBeneficiary_acc(String beneficiary_acc) {
    this.beneficiary_acc = beneficiary_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the beneficiary_bic.
   * 
   * @return the beneficiary_bic
   */
  protected String getBeneficiary_bic() {
    return beneficiary_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the beneficiary_bic.
   * 
   * @param beneficiary_bic the new beneficiary_bic
   */
  protected void setBeneficiary_bic(String beneficiary_bic) {
    this.beneficiary_bic = beneficiary_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the beneficiary_bic_id.
   * 
   * @return the beneficiary_bic_id
   */
  protected String getBeneficiary_bic_id() {
    return beneficiary_bic_id;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the beneficiary_bic_id.
   * 
   * @param beneficiary_bic_id the new beneficiary_bic_id
   */
  protected void setBeneficiary_bic_id(String beneficiary_bic_id) {
    this.beneficiary_bic_id = beneficiary_bic_id;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the beneficiary_name.
   * 
   * @return the beneficiary_name
   */
  protected String getBeneficiary_name() {
    return beneficiary_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the beneficiary_name.
   * 
   * @param beneficiary_name the new beneficiary_name
   */
  protected void setBeneficiary_name(String beneficiary_name) {
    this.beneficiary_name = beneficiary_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the beneficiary_mn.
   * 
   * @return the beneficiary_mn
   */
  protected String getBeneficiary_mn() {
    return beneficiary_mn;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the beneficiary_mn.
   * 
   * @param beneficiary_mn the new beneficiary_mn
   */
  protected void setBeneficiary_mn(String beneficiary_mn) {
    this.beneficiary_mn = beneficiary_mn;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_mnemonic.
   * 
   * @return the client_mnemonic
   */
  protected String getClient_mnemonic() {
    return client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_mnemonic.
   * 
   * @param client_mnemonic the new client_mnemonic
   */
  protected void setClient_mnemonic(String client_mnemonic) {
    this.client_mnemonic = client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the entity_bic.
   * 
   * @return the entity_bic
   */
  protected String getEntity_bic() {
    return entity_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the entity_bic.
   * 
   * @param entity_bic the new entity_bic
   */
  protected void setEntity_bic(String entity_bic) {
    this.entity_bic = entity_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the entity_name.
   * 
   * @return the entity_name
   */
  protected String getEntity_name() {
    return entity_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the entity_name.
   * 
   * @param entity_name the new entity_name
   */
  protected void setEntity_name(String entity_name) {
    this.entity_name = entity_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the global_cust_acc.
   * 
   * @return the global_cust_acc
   */
  protected String getGlobal_cust_acc() {
    return global_cust_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the global_cust_acc.
   * 
   * @param global_cust_acc the new global_cust_acc
   */
  protected void setGlobal_cust_acc(String global_cust_acc) {
    this.global_cust_acc = global_cust_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the global_cust_bic.
   * 
   * @return the global_cust_bic
   */
  protected String getGlobal_cust_bic() {
    return global_cust_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the global_cust_bic.
   * 
   * @param global_cust_bic the new global_cust_bic
   */
  protected void setGlobal_cust_bic(String global_cust_bic) {
    this.global_cust_bic = global_cust_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the global_cust_bic_id.
   * 
   * @return the global_cust_bic_id
   */
  protected String getGlobal_cust_bic_id() {
    return global_cust_bic_id;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the global_cust_bic_id.
   * 
   * @param global_cust_bic_id the new global_cust_bic_id
   */
  protected void setGlobal_cust_bic_id(String global_cust_bic_id) {
    this.global_cust_bic_id = global_cust_bic_id;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the global_cust_mn.
   * 
   * @return the global_cust_mn
   */
  protected String getGlobal_cust_mn() {
    return global_cust_mn;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the global_cust_mn.
   * 
   * @param global_cust_mn the new global_cust_mn
   */
  protected void setGlobal_cust_mn(String global_cust_mn) {
    this.global_cust_mn = global_cust_mn;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the global_cust_name.
   * 
   * @return the global_cust_name
   */
  protected String getGlobal_cust_name() {
    return global_cust_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the global_cust_name.
   * 
   * @param global_cust_name the new global_cust_name
   */
  protected void setGlobal_cust_name(String global_cust_name) {
    this.global_cust_name = global_cust_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the local_cust_acc.
   * 
   * @return the local_cust_acc
   */
  protected String getLocal_cust_acc() {
    return local_cust_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the local_cust_acc.
   * 
   * @param local_cust_acc the new local_cust_acc
   */
  protected void setLocal_cust_acc(String local_cust_acc) {
    this.local_cust_acc = local_cust_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the local_cust_bic.
   * 
   * @return the local_cust_bic
   */
  protected String getLocal_cust_bic() {
    return local_cust_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the local_cust_bic.
   * 
   * @param local_cust_bic the new local_cust_bic
   */
  protected void setLocal_cust_bic(String local_cust_bic) {
    this.local_cust_bic = local_cust_bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the local_cust_bic_id.
   * 
   * @return the local_cust_bic_id
   */
  protected String getLocal_cust_bic_id() {
    return local_cust_bic_id;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the local_cust_bic_id.
   * 
   * @param local_cust_bic_id the new local_cust_bic_id
   */
  protected void setLocal_cust_bic_id(String local_cust_bic_id) {
    this.local_cust_bic_id = local_cust_bic_id;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the local_cust_mn.
   * 
   * @return the local_cust_mn
   */
  protected String getLocal_cust_mn() {
    return local_cust_mn;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the local_cust_mn.
   * 
   * @param local_cust_mn the new local_cust_mn
   */
  protected void setLocal_cust_mn(String local_cust_mn) {
    this.local_cust_mn = local_cust_mn;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the local_cust_name.
   * 
   * @return the local_cust_name
   */
  protected String getLocal_cust_name() {
    return local_cust_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the local_cust_name.
   * 
   * @param local_cust_name the new local_cust_name
   */
  protected void setLocal_cust_name(String local_cust_name) {
    this.local_cust_name = local_cust_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the market.
   * 
   * @return the market
   */
  protected String getMarket() {
    return market;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the market.
   * 
   * @param market the new market
   */
  protected void setMarket(String market) {
    this.market = market;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the market_name.
   * 
   * @return the market_name
   */
  protected String getMarket_name() {
    return market_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the market_name.
   * 
   * @param market_name the new market_name
   */
  protected void setMarket_name(String market_name) {
    this.market_name = market_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mnemonic.
   * 
   * @return the mnemonic
   */
  protected String getMnemonic() {
    return mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mnemonic.
   * 
   * @param mnemonic the new mnemonic
   */
  protected void setMnemonic(String mnemonic) {
    this.mnemonic = mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the place_of_settlement.
   * 
   * @return the place_of_settlement
   */
  protected String getPlace_of_settlement() {
    return place_of_settlement;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the place_of_settlement.
   * 
   * @param place_of_settlement the new place_of_settlement
   */
  protected void setPlace_of_settlement(String place_of_settlement) {
    this.place_of_settlement = place_of_settlement;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the settlement_type.
   * 
   * @return the settlement_type
   */
  protected String getSettlement_type() {
    return settlement_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the settlement_type.
   * 
   * @param settlement_type the new settlement_type
   */
  protected void setSettlement_type(String settlement_type) {
    this.settlement_type = settlement_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /* AML 26-04-2010 */
  /* INCLUYO FECHA DE VALIDEZ */

  /**
   * Gets/Sets the efectiveFrom. Gets/Sets the efectiveTo
   * @param beneficiary_bic_id the new beneficiary_bic_id
   */
  protected String getDateFrom() {
    return efectiveFrom;
  }

  protected void setDateFrom(String dateFrom) {
    this.efectiveFrom = dateFrom;
  }

  protected String getDateTo() {
    return efectiveTo;
  }

  protected void setDateTo(String dateto) {
    this.efectiveTo = dateto;
  }

}
