package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * ClientConfirmationRuleMegara.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class ClientConfirmationRuleMegara extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClientConfirmationRuleMegara.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The client_mnemonic. */
  private String client_mnemonic;

  /** The confirm_using. */
  private String confirm_using;

  /** The recipient_name. */
  private String recipient_name;

  /** The recipient_address. */
  private String recipient_address;

  /** The account_mnemonic. */
  private String account_mnemonic;

  /** The confirmation_template. */
  private String confirmation_template;

  /** The cancellation_template. */
  private String cancellation_template;

  /** The template_language. */
  private String template_language;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new client confirmation rule_megara.
   */
  protected ClientConfirmationRuleMegara() {

    this.tipoActualizacion = "";
    this.client_mnemonic = "";
    this.confirm_using = "";
    this.recipient_name = "";
    this.recipient_address = "";
    this.account_mnemonic = "";
    this.confirmation_template = "";
    this.cancellation_template = "";
    this.template_language = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Limpiar obj.
   */
  protected void limpiarObj() {

    this.tipoActualizacion = "";
    this.client_mnemonic = "";
    this.confirm_using = "";
    this.recipient_name = "";
    this.recipient_address = "";
    this.account_mnemonic = "";
    this.template_language = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("clientConfirmationRule: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element CLIENT_CONFIRMATION_RULE = document.createElement("CLIENT_CONFIRMATION_RULE");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element CLIENT_MNEMONIC = document.createElement("CLIENT_MNEMONIC");
      CLIENT_MNEMONIC.appendChild(document.createTextNode(getClient_mnemonic()));

      Element RULE_TYPE = document.createElement("RULE_TYPE");
      RULE_TYPE.appendChild(document.createTextNode("SECONDARY"));

      Element CONFIRM_TYPE = document.createElement("CONFIRM_TYPE");
      CONFIRM_TYPE.appendChild(document.createTextNode("DETAILED"));

      Element CONFIRM_USING = document.createElement("CONFIRM_USING");
      CONFIRM_USING.appendChild(document.createTextNode(getConfirm_using()));

      Element RECIPIENT_NAME = document.createElement("RECIPIENT_NAME");
      RECIPIENT_NAME.appendChild(document.createTextNode(getRecipient_name()));

      Element RECIPIENT_ADDRESS = document.createElement("RECIPIENT_ADDRESS");
      RECIPIENT_ADDRESS.appendChild(document.createTextNode(getRecipient_address()));

      Element RECIPIENT_LOCATION = document.createElement("RECIPIENT_LOCATION");

      Element RECIPIENT_ADDRESS_LOCATION = document.createElement("RECIPIENT_ADDRESS_LOCATION");

      Element CONFIRM_WHEN = document.createElement("CONFIRM_WHEN");
      CONFIRM_WHEN.appendChild(document.createTextNode("DETAILED_TRADE_AFFIRMED"));

      Element INSTRUMENT_TYPE = document.createElement("INSTRUMENT_TYPE");

      Element COUNTRY_REGISTER = document.createElement("COUNTRY_REGISTER");

      Element TRADED_CURRENCY = document.createElement("TRADED_CURRENCY");

      Element ACCOUNT_MNEMONIC = document.createElement("ACCOUNT_MNEMONIC");
      ACCOUNT_MNEMONIC.appendChild(document.createTextNode(getAccount_mnemonic()));

      Element PRIORITY = document.createElement("PRIORITY");

      Element CONFIRMATION_TEMPLATE = document.createElement("CONFIRMATION_TEMPLATE");
      CONFIRMATION_TEMPLATE.appendChild(document.createTextNode(getConfirmation_template()));

      Element CANCELLATION_TEMPLATE = document.createElement("CANCELLATION_TEMPLATE");
      CANCELLATION_TEMPLATE.appendChild(document.createTextNode(getCancellation_template()));

      Element TEMPLATE_LANGUAGE = document.createElement("TEMPLATE_LANGUAGE");
      TEMPLATE_LANGUAGE.appendChild(document.createTextNode(getTemplate_language()));

      // ---------------------------------------------------------

      data.appendChild(CLIENT_MNEMONIC);
      data.appendChild(RULE_TYPE);
      data.appendChild(CONFIRM_TYPE);
      data.appendChild(CONFIRM_USING);
      data.appendChild(RECIPIENT_NAME);
      data.appendChild(RECIPIENT_ADDRESS);
      data.appendChild(RECIPIENT_LOCATION);
      data.appendChild(RECIPIENT_ADDRESS_LOCATION);
      data.appendChild(CONFIRM_WHEN);
      data.appendChild(INSTRUMENT_TYPE);
      data.appendChild(COUNTRY_REGISTER);
      data.appendChild(TRADED_CURRENCY);
      data.appendChild(ACCOUNT_MNEMONIC);
      data.appendChild(PRIORITY);
      data.appendChild(CONFIRMATION_TEMPLATE);
      data.appendChild(CANCELLATION_TEMPLATE);
      data.appendChild(TEMPLATE_LANGUAGE);

      // ---------------------------------------------------------

      CLIENT_CONFIRMATION_RULE.appendChild(header);
      CLIENT_CONFIRMATION_RULE.appendChild(data);

      document.appendChild(CLIENT_CONFIRMATION_RULE);

      logger.debug("clientConfirmationRule: estructura documento XML completada");

      // ---------------------------------------------------------

      nombreXML = generarNombreXML(fecha);
      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("clientConfirmationRule: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("clientConfirmationRule: error en la generacion del docuemento XML");
      logger.warn("clientConfirmationRule: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a client confirmation rule";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a client confirmation rule";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a client confirmation rule";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("clientConfirmationRule: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, FFMMdd0000.xml donde
   * FF es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getClientConfirmationsRules();
    Path path = Paths.get(getRutaDestino(), String.format("FF%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("ClientConfirmationsRules: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_mnemonic.
   * 
   * @param client_mnemonic the new client_mnemonic
   */
  protected void setClient_mnemonic(String client_mnemonic) {
    this.client_mnemonic = client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_mnemonic.
   * 
   * @return the client_mnemonic
   */
  protected String getClient_mnemonic() {
    return client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the confirm_using.
   * 
   * @param confirm_using the new confirm_using
   */
  protected void setConfirm_using(String confirm_using) {
    this.confirm_using = confirm_using;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the confirm_using.
   * 
   * @return the confirm_using
   */
  protected String getConfirm_using() {
    return confirm_using;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the recipient_name.
   * 
   * @param recipient_name the new recipient_name
   */
  protected void setRecipient_name(String recipient_name) {
    this.recipient_name = recipient_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the recipient_name.
   * 
   * @return the recipient_name
   */
  protected String getRecipient_name() {
    return recipient_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the recipient_address.
   * 
   * @param recipient_address the new recipient_address
   */
  protected void setRecipient_address(String recipient_address) {
    this.recipient_address = recipient_address;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the recipient_address.
   * 
   * @return the recipient_address
   */
  protected String getRecipient_address() {
    return recipient_address;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the account_mnemonic.
   * 
   * @param account_mnemonic the new account_mnemonic
   */
  protected void setAccount_mnemonic(String account_mnemonic) {
    this.account_mnemonic = account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the account_mnemonic.
   * 
   * @return the account_mnemonic
   */
  protected String getAccount_mnemonic() {
    return account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the template_language.
   * 
   * @param template_language the new template_language
   */
  protected void setTemplate_language(String template_language) {
    this.template_language = template_language;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the template_language.
   * 
   * @return the template_language
   */
  protected String getTemplate_language() {
    return template_language;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the confirmation_template.
   * 
   * @return the confirmation_template
   */
  protected String getConfirmation_template() {
    return confirmation_template;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the confirmation_template.
   * 
   * @param confirmatin_template the new confirmation_template
   */
  protected void setConfirmation_template(String confirmatin_template) {
    this.confirmation_template = confirmatin_template;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the cancellation_template.
   * 
   * @return the cancellation_template
   */
  protected String getCancellation_template() {
    return cancellation_template;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the cancellation_template.
   * 
   * @param cancellation_template the new cancellation_template
   */
  protected void setCancellation_template(String cancellation_template) {
    this.cancellation_template = cancellation_template;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}
