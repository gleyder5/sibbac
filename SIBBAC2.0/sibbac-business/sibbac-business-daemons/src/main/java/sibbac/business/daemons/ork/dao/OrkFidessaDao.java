package sibbac.business.daemons.ork.dao;

import sibbac.business.daemons.ork.common.ResultSetIterator;
import sibbac.common.SIBBACBusinessException;

public interface OrkFidessaDao {

	static final String DELETE_EXISTANT
			= "DELETE FROM TMCT0_ORK_FIDESSA "
			+ "WHERE NUORDENMKT = ? AND FECHA_ALTA = ? AND MIC_SEGMENTO = ? ";

	static final String MAIN_INSERT 
			= "INSERT INTO TMCT0_ORK_FIDESSA (NUORDENMKT, MERCADO, FECHA_ALTA, FECHA_VALIDEZ, MIC_SEGMENTO, "
			+ "FHAUDIT, TRATADO) "
			+ "VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP, 'N')";

	static final String DETAIL_INSERT 
			= "INSERT INTO TMCT0_ORK_FIDESSA_DETAIL (NUORDENMKT, FECHA_ALTA, MIC_SEGMENTO, DYNAFIELD, VALUE) "
	    + "VALUES (?, ?, ?, ?, ?)";
	
	static final String UPDATE_MAIN 
	    = "UPDATE TMCT0_ORK_FIDESSA SET "
	    + "TRATADO = ?, FECHA_ENVIO_MERCADO = ?, SECUENCIA_FICHERO = ?, ERROR = ?, FHAUDIT = CURRENT_TIMESTAMP "
      + "WHERE NUORDENMKT = ? AND FECHA_ALTA = ? AND MIC_SEGMENTO = ? ";
	
	void iterateOverInsercionDate(ResultSetIterator iterator) throws SIBBACBusinessException;
	
	/**
	 * Verifica si hay entidades OrkFidessa pendientes de ser tratados
	 * @return verdadero en caso de existan entidades OrkFidessa sin tratar
	 * @throws SIBBACBusinessException 
	 */
	boolean existenSinTratar() throws SIBBACBusinessException;

}
