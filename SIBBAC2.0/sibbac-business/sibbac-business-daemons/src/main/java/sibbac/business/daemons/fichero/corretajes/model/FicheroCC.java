package sibbac.business.daemons.fichero.corretajes.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TMCT0_FICHERO_CC")
public class FicheroCC implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -2936267882466269689L;
  private long id;
  private String idFicheroCC;
  private String codigoError;
  private String descripcionError;
  private String miembroMercado;
  private String idCliente;
  private String descripcionCliente;
  private String referenciaInformativa;
  private String iban;
  private Date fechaEjecucion;
  private Date fechaOrden;
  private Date fechaLiquidacion;
  private String isin;
  private Character sentido;
  private BigDecimal titulos;
  private BigDecimal efectivo;
  private String entidadParticipante;
  private BigDecimal corretaje;
  private Character indicadorAceptacion;
  private String nombreFichero;
  private String nuorden;
  private String nbooking;
  private String nucnfclt;
  private Short nucnfliq;
  private Long iddesglosecamara;
  private Long nudesglose;
  private String auditUser;
  private Date auditDate;
  private Long version;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", unique = true, nullable = false)
  public long getId() {
    return id;
  }

  @Column(name = "ID_FICHERO_CC", length = 20, nullable = true)
  public String getIdFicheroCC() {
    return idFicheroCC;
  }

  @Column(name = "CD_ERROR", length = 3, nullable = true)
  public String getCodigoError() {
    return codigoError;
  }

  @Column(name = "DS_ERROR", length = 40, nullable = true)
  public String getDescripcionError() {
    return descripcionError;
  }

  @Column(name = "CDMIEMBROMKT", length = 4, nullable = true)
  public String getMiembroMercado() {
    return miembroMercado;
  }

  @Column(name = "ID_CLIENTE", length = 20, nullable = true)
  public String getIdCliente() {
    return idCliente;
  }

  @Column(name = "DS_CLIENTE", length = 40, nullable = true)
  public String getDescripcionCliente() {
    return descripcionCliente;
  }

  @Column(name = "REF_INFORMATIVA", length = 20, nullable = true)
  public String getReferenciaInformativa() {
    return referenciaInformativa;
  }

  @Column(name = "IBAN", length = 24, nullable = true)
  public String getIban() {
    return iban;
  }

  @Column(name = "FECHA_EJECUCION", nullable = true)
  public Date getFechaEjecucion() {
    return fechaEjecucion;
  }

  @Column(name = "FECHA_ORDEN", nullable = true)
  public Date getFechaOrden() {
    return fechaOrden;
  }

  @Column(name = "FECHA_LIQUIDACION", nullable = true)
  public Date getFechaLiquidacion() {
    return fechaLiquidacion;
  }

  @Column(name = "CDISIN", length = 12, nullable = true)
  public String getIsin() {
    return isin;
  }

  @Column(name = "SENTIDO", length = 1, nullable = true)
  public Character getSentido() {
    return sentido;
  }

  @Column(name = "TITULOS", precision = 18, scale = 6, nullable = true)
  public BigDecimal getTitulos() {
    return titulos;
  }

  @Column(name = "EFECTIVO", precision = 15, scale = 2, nullable = true)
  public BigDecimal getEfectivo() {
    return efectivo;
  }

  @Column(name = "ENTIDAD_PARTICIPANTE", length = 11, nullable = true)
  public String getEntidadParticipante() {
    return entidadParticipante;
  }

  @Column(name = "CORRETAJE", precision = 15, scale = 2, nullable = true)
  public BigDecimal getCorretaje() {
    return corretaje;
  }

  @Column(name = "INDICADOR_ACEPTACION", length = 1, nullable = true)
  public Character getIndicadorAceptacion() {
    return indicadorAceptacion;
  }

  @Column(name = "NOMBRE_FICHERO", length = 255, nullable = true)
  public String getNombreFichero() {
    return nombreFichero;
  }

  @Column(name = "NUORDEN", length = 20, nullable = true)
  public String getNuorden() {
    return nuorden;
  }

  @Column(name = "NBOOKING", length = 20, nullable = true)
  public String getNbooking() {
    return nbooking;
  }

  @Column(name = "NUCNFCLT", length = 20, nullable = true)
  public String getNucnfclt() {
    return nucnfclt;
  }

  @Column(name = "NUCNFLIQ", precision = 4, scale = 0, nullable = true)
  public Short getNucnfliq() {
    return nucnfliq;
  }

  @Column(name = "IDDESGLOSECAMARA", nullable = true)
  public Long getIddesglosecamara() {
    return iddesglosecamara;
  }

  @Column(name = "NUDESGLOSE", nullable = true)
  public Long getNudesglose() {
    return nudesglose;
  }

  @Column(name = "AUDIT_USER", length = 255, nullable = true)
  public String getAuditUser() {
    return auditUser;
  }

  @Column(name = "AUDIT_DATE", nullable = true)
  public Date getAuditDate() {
    return auditDate;
  }

  @Column(name = "VERSION", nullable = true)
  public Long getVersion() {
    return version;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setIdFicheroCC(String idFicheroCC) {
    this.idFicheroCC = idFicheroCC;
  }

  public void setCodigoError(String codigoError) {
    this.codigoError = codigoError;
  }

  public void setDescripcionError(String descripcionError) {
    this.descripcionError = descripcionError;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  public void setIdCliente(String idCliente) {
    this.idCliente = idCliente;
  }

  public void setDescripcionCliente(String descripcionCliente) {
    this.descripcionCliente = descripcionCliente;
  }

  public void setReferenciaInformativa(String referenciaInformativa) {
    this.referenciaInformativa = referenciaInformativa;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public void setFechaEjecucion(Date fechaEjecucion) {
    this.fechaEjecucion = fechaEjecucion;
  }

  public void setFechaOrden(Date fechaOrden) {
    this.fechaOrden = fechaOrden;
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) {
    this.fechaLiquidacion = fechaLiquidacion;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setTitulos(BigDecimal titulos) {
    this.titulos = titulos;
  }

  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

  public void setEntidadParticipante(String entidadParticipante) {
    this.entidadParticipante = entidadParticipante;
  }

  public void setCorretaje(BigDecimal corretaje) {
    this.corretaje = corretaje;
  }

  public void setIndicadorAceptacion(Character indicadorAceptacion) {
    this.indicadorAceptacion = indicadorAceptacion;
  }

  public void setNombreFichero(String nombreFichero) {
    this.nombreFichero = nombreFichero;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setNucnfclt(String nucnfclt) {
    this.nucnfclt = nucnfclt;
  }

  public void setNucnfliq(Short nucnfliq) {
    this.nucnfliq = nucnfliq;
  }

  public void setIddesglosecamara(Long iddesglosecamara) {
    this.iddesglosecamara = iddesglosecamara;
  }

  public void setNudesglose(Long nudesglose) {
    this.nudesglose = nudesglose;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

}
