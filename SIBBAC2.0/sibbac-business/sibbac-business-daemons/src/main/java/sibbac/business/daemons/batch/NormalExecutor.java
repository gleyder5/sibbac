package sibbac.business.daemons.batch;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.BatchException;

class NormalExecutor extends DestinyExecutor {
  
  private static final Logger LOG = getLogger(NormalExecutor.class);
  
  @Override
  void executeStatement(Connection con, String query) throws BatchException {
    try(Statement stmt = con.createStatement()) {
      addAffectedRows(stmt.executeUpdate(query));
    }
    catch(SQLException ex) {
      throw new BatchException("Al hacer una execución de actualizacion", ex);
    }
  }

  void batchExecute(Connection con, Iterable<String> sentences) throws BatchException {
    final int res[];
    SQLException next;
    int total = 0;
    
    try(Statement stmt = con.createStatement()) {
      for(String s : sentences) {
        stmt.addBatch(s);
      }
      res = stmt.executeBatch();
      for(int r : res) {
        total += r;
      }
      addAffectedRows(total);
    }
    catch(SQLException ex) {
      next = ex;
      while((next = next.getNextException()) != null) {
        LOG.error("[batchExecute] Error provocado en batch:", next);
      }
      throw new BatchException("Al hacer una execución en Batch", ex);
    }
  }

}
