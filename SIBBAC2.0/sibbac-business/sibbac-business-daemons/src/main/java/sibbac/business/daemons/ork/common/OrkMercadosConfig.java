package sibbac.business.daemons.ork.common;

import java.io.File;

import sibbac.common.SIBBACBusinessException;

public interface OrkMercadosConfig {
	
	public static final String PROPERTIES_FILE_NAME = "sibbac.mercados.properties";
	
	static final String TIMEOUT_IN = "timeout_fidessa.ork.in";
	
	static final String FIDESSA_PATH = "ruta_fichero_fidessa.ork.in";
	
	static final String FIDESSA_FILENAME = "nombre_fichero_fidessa.ork.in";
	
	static final String FIDESSSA_TRATADOS_PATH = "ruta_fichero_fidessa.ork.in.tratados";
	
	static final String BME_PATH = "ruta_fichero_bme.ork.out";
	
	static final String BME_FILENAME = "nombre_fichero_bme.ork.out";
	
	static final String MAX_PETITIONS_PROPERTY = "max_petitions.ork.out";
	
	static final String MAX_THREADS_PROPERTY = "max_threads.ork.out";
	
	static final String WRITE_TIMEOUT_PROPERTY = "sibbac.ork.timeout";
	
	static final String UPDATE_TIMEOUT_PROPERTY = "update_timeout.ork.out";
	
	File[] getFicherosFidessa() throws SIBBACBusinessException;
	
	File getBatchTempPath() throws SIBBACBusinessException;

	int getLecturaTimeout();
	
	int getEscrituraTimeout();
	
	int getUpdateTimeout();

	int getMaxPetitions();
	
	int getMaxThreads();

	OrkFiles getOrkFiles(int sequence) throws SIBBACBusinessException;

	void moveFidessaFileToTratados(File origen) throws SIBBACBusinessException;
	
	String getProperty(String key) throws SIBBACBusinessException;

}
