package sibbac.business.daemons.batch.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;

@Entity
@Table(name = "TMCT0_BATCH_QUERY_PARAM")
public class BatchParam extends AbstractParam implements Comparable<BatchParam> {

  private static final long serialVersionUID = -5053696155176207790L;
    
  @EmbeddedId
  private ParamId id;
  
  public ParamId getId() {
    return id;
  }

  public void setId(ParamId id) {
    this.id = id;
  }
  
  @Override
  public String toString() {
	  return String.format("Id: {%s}, type: %d", id.toString(), getType());
	}
  
  @Override
	public int compareTo(BatchParam other) {
		return id.compareTo(other.id);
	}

	@Override
	protected String getIdString() {
		return id.toString();
	}

}
