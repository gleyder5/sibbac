package sibbac.business.daemons.ork.bo;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.common.SIBBACBusinessException;
import sibbac.business.daemons.ork.dao.OrkBmeFicherosDao;
import sibbac.business.daemons.ork.dao.OrkFidessaDao;
import sibbac.business.daemons.ork.model.OrkBmeFicheros;

@Service
public class OrkBmeBoImpl implements OrkBmeBo {
	
	private static final Logger LOG = getLogger(OrkBmeBoImpl.class);
	
	private ApplicationContext ctx;

	private OrkFidessaDao orkFidessaDao;
	
	private OrkBmeFicherosDao ficherosDao;
	
	@Override
	public File generaOrk(OrkFidessaFieldCollection fieldCollection) throws SIBBACBusinessException {
		final OrkInputIterator iterator;
		final OrkProcessor orkProcessor;
		final String message;
		
		orkProcessor = ctx.getBean(OrkProcessor.class);
	  orkProcessor.setCache(Collections.synchronizedMap(new HashMap<String, Object[]>()));
	  orkProcessor.setOrkFidessaFieldCollection(fieldCollection);
		iterator = orkProcessor.getInputIterator();
		orkFidessaDao.iterateOverInsercionDate(iterator);
		try {
			return orkProcessor.getOrkFile();
		}
		catch(InterruptedException iex) {
			message = "Interrupción en los hilos de generacion del fichero ORK";
			LOG.error("[generacionOrk] {}", message, iex);
			throw new SIBBACBusinessException(message, iex);
		} 
	}
	
	@Override
	@Transactional
	public int getSequence() {
		final Date now;
		OrkBmeFicheros orkBmeFicheros = null;
		
		now = new Date();
		try {
			orkBmeFicheros = ficherosDao.findOne(now);
			if(orkBmeFicheros == null) {
				orkBmeFicheros = OrkBmeFicheros.createNow(now);
			}
			return orkBmeFicheros.increment();
		}
		finally {
			if(orkBmeFicheros != null) {
				ficherosDao.save(orkBmeFicheros);
			}
		}
	}
	
	@Autowired
	void setOrkFidessaDao(OrkFidessaDao orkInputDao) {
		this.orkFidessaDao = orkInputDao;
	}
	
	@Autowired
	void setApplicationContext(ApplicationContext ctx) {
		this.ctx = ctx;
	}

	@Autowired
	void setOrkBmeFicherosDao(OrkBmeFicherosDao ficherosDao) {
		this.ficherosDao = ficherosDao;
	}
	
}
