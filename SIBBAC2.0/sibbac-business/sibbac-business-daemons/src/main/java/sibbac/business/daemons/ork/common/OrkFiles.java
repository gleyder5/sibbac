package sibbac.business.daemons.ork.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.file.StandardCopyOption;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import static java.nio.file.Files.move;
import static java.text.MessageFormat.format;
import static javax.xml.bind.DatatypeConverter.printHexBinary;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.common.SIBBACBusinessException;

public class OrkFiles {
  
  private static final Logger LOG = getLogger(OrkFiles.class);
  
  private final File tmpOrkFile;
  
  private final File updateInfoFile;
  
  OrkFiles(final File tmpOrkFile, final File updateInfoFile) {
    this.tmpOrkFile = tmpOrkFile;
    this.updateInfoFile = updateInfoFile;
  }
  
  public PrintWriter getOrkWriter(MessageDigest md) throws IOException {
    final OutputStream dos;
    final Writer writer;
    
    dos = new DigestOutputStream(new FileOutputStream(tmpOrkFile), md);
    writer = new OutputStreamWriter(dos, "UTF-8");
    return new PrintWriter(new BufferedWriter(writer));
  }
  
  public PrintWriter getUpdateInfoWriter() throws IOException {
    final Writer writer;
    
    writer = new OutputStreamWriter(new FileOutputStream(updateInfoFile), "UTF-8");
    return new PrintWriter(new BufferedWriter(writer));
  }
  
  public void moveOrkFile(byte[] hash) throws SIBBACBusinessException {
    final String temporalName, finalName, message;
    final File orkFile;
    
    temporalName = tmpOrkFile.getName();
    finalName = new StringBuilder(temporalName.substring(5)).append('_').append(printHexBinary(hash)).toString();
    orkFile = new File(tmpOrkFile.getParentFile(), finalName);
    try {
      move(tmpOrkFile.toPath(), orkFile.toPath(), StandardCopyOption.ATOMIC_MOVE);
    }
    catch(IOException ioex) {
      message = format("Error al intentar mover el fichero {0} a la ubicacion {1}", toString(),
          orkFile.getAbsolutePath());
      LOG.error("[getOrkFile] {}", message, ioex);
      throw new SIBBACBusinessException(message, ioex);
    } 
   
  }
  
  public File getUpdateInfoFile() {
    return updateInfoFile;
  }

  @Override
  public String toString() {
    return new StringBuilder()
        .append(tmpOrkFile.getAbsolutePath()).append(", ")
        .append(updateInfoFile.getAbsolutePath()).toString();
  }
  
}
