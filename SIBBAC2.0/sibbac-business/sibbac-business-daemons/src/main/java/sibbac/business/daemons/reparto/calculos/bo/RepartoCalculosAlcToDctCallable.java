package sibbac.business.daemons.reparto.calculos.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.common.SIBBACBusinessException;

@Service
@Scope(value = "prototype")
public class RepartoCalculosAlcToDctCallable implements Callable<Void> {

  @Autowired
  private RepartoCalculosAlcToDctBo repartoCalculosAlcToDctBo;

  private List<RepartoCalculosAlctToDctDTO> listAlcId;

  private String auditUser;

  public RepartoCalculosAlcToDctCallable(List<RepartoCalculosAlctToDctDTO> alcId, String auditUser) {
    this.listAlcId = new ArrayList<RepartoCalculosAlctToDctDTO>(alcId);
    this.auditUser = auditUser;

  }

  @Override
  public Void call() throws EconomicDataException, SIBBACBusinessException {
    repartoCalculosAlcToDctBo.repartirCalculosAlcToDct(listAlcId, auditUser);
    return null;
  }

}
