package sibbac.business.estaticos.megara.input;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sibbac.business.estaticos.megara.inout.ComisionesOutPut;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0comBo;
import sibbac.database.megbpdta.bo.Fmeg0ctrBo;
import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0comId;
import sibbac.database.megbpdta.model.Fmeg0ctr;
import sibbac.database.megbpdta.model.Fmeg0ctrId;

@Service
@Scope(value = "prototype")
public class Comisiones {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(Comisiones.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0comBo fmeg0comBo;

  @Autowired
  private Fmeg0ctrBo meg0ctrBo;

  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;
  
  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private static List<String> atrData;

  private static List<String> atrTramos;

  private Document document;

  private Fmeg0com obj1Fmeg0com;

  private Fmeg0com obj2Fmeg0com;

  private int numeroRegistros;

  public Comisiones(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("numreg");

    atrData.add("cdtpcomi1");
    atrData.add("modo1");
    atrData.add("cdcomisi1");
    atrData.add("cdclente1");
    atrData.add("cdcuenta1");
    atrData.add("cdsubaccount1");
    atrData.add("cdbroker1");
    atrData.add("cdtrato1");
    atrData.add("cdvia1");
    atrData.add("dsgtomdo1");
    atrData.add("cdreside1");
    atrData.add("xtbroper1");
    atrData.add("cdindice1");
    atrData.add("cdclsmdo1");
    atrData.add("cdcanal1");
    atrData.add("cdmercad1");
    atrData.add("cdtpmerc1");
    atrData.add("cdorigen1");
    atrData.add("cdtpoper1");
    atrData.add("cdtpoptv1");
    atrData.add("cdactivo1");
    atrData.add("cdnifemi1");
    atrData.add("cdisn1");
    atrData.add("cdperio1");
    atrData.add("feinivig1");
    atrData.add("fefinvig1");
    atrData.add("cdtarifa1");
    atrData.add("cdtptari1");
    atrData.add("imfijo1");
    atrData.add("potarifa1");
    atrData.add("cdunidad1");
    atrData.add("imminimo1");
    atrData.add("immaximo1");
    atrData.add("imdescue1");
    atrData.add("cdmoniso1");
    atrData.add("cdtpdevo1");
    atrData.add("xttramos1");
    atrData.add("cdtpcalc1");
    atrData.add("cdtptram1");
    atrData.add("xtexiste1");
    atrData.add("feestado1");
    atrData.add("xtexcepc1");

    atrData.add("cdtpcomi2");
    atrData.add("modo2");
    atrData.add("cdcomisi2");
    atrData.add("cdclente2");
    atrData.add("cdcuenta2");
    atrData.add("cdsubaccount2");
    atrData.add("cdbroker2");
    atrData.add("cdtrato2");
    atrData.add("cdvia2");
    atrData.add("dsgtomdo2");
    atrData.add("cdreside2");
    atrData.add("xtbroper2");
    atrData.add("cdindice2");
    atrData.add("cdclsmdo2");
    atrData.add("cdcanal2");
    atrData.add("cdmercad2");
    atrData.add("cdtpmerc2");
    atrData.add("cdorigen2");
    atrData.add("cdtpoper2");
    atrData.add("cdtpoptv2");
    atrData.add("cdactivo2");
    atrData.add("cdnifemi2");
    atrData.add("cdisn2");
    atrData.add("cdperio2");
    atrData.add("feinivig2");
    atrData.add("fefinvig2");
    atrData.add("cdtarifa2");
    atrData.add("cdtptari2");
    atrData.add("imfijo2");
    atrData.add("potarifa2");
    atrData.add("cdunidad2");
    atrData.add("imminimo2");
    atrData.add("immaximo2");
    atrData.add("imdescue2");
    atrData.add("cdmoniso2");
    atrData.add("cdtpdevo2");
    atrData.add("xttramos2");
    atrData.add("cdtpcalc2");
    atrData.add("cdtptram2");
    atrData.add("xtexiste2");
    atrData.add("feestado2");
    atrData.add("xtexcepc2");

    atrTramos = new ArrayList<>();
    atrTramos.add("cdtpcomi");
    atrTramos.add("cdcomisi");
    atrTramos.add("cdtarifa");
    atrTramos.add("cdtramo");
    atrTramos.add("cdcltram");
    atrTramos.add("imlimmin");
    atrTramos.add("imlimmax");
    atrTramos.add("imfijo");
    atrTramos.add("cdestado");
    atrTramos.add("potarifa");
    atrTramos.add("feestado");

    numeroRegistros = 0;

    obj1Fmeg0com = null;
    obj2Fmeg0com = null;

  }

  public void procesarXML_In(String nombreXML) {

    try {

      analizarXML();

      if (("A".equals(obj1Fmeg0com.getTpmodoop())) || ("M".equals(obj1Fmeg0com.getTpmodoop()))) {
        inputComisiones(1);
      }
      else {
        deleteComisiones(1);
      }

      if (2 == numeroRegistros) {

        if (("A".equals(obj2Fmeg0com.getTpmodoop())) || ("M".equals(obj2Fmeg0com.getTpmodoop()))) {
          inputComisiones(2);
        }
        else {
          deleteComisiones(2);
        }

      }

      String ident = obj1Fmeg0com.getId().getCdtpcomi() + "_";

      if (("I".equals(obj1Fmeg0com.getId().getCdtpcomi())) || ("M".equals(obj1Fmeg0com.getId().getCdtpcomi()))) {
        ident = ident + obj1Fmeg0com.getId().getCdcomisi().trim() + "_" + obj1Fmeg0com.getId().getCdtarifa().trim();
        if (2 == numeroRegistros) {
          ident = ident + "|" + obj2Fmeg0com.getId().getCdtarifa().trim();
        }

      }
      else {

        ident = ident + obj1Fmeg0com.getCdaliass() + "_" + obj1Fmeg0com.getCdbrocli() + "_"
            + obj1Fmeg0com.getCdbroker() + "_" + obj1Fmeg0com.getCdsubcta();

      }

      logger.debug("Archivo XML tratado correctamente");
      ctx.getBean(RegistroControl.class, "Comisiones", ident, nombreXML, "I", "Procesado fichero estatico", "Comisiones", "MT").write();
      directorios.moverDirOk(nombreXML);

      try {
        ctx.getBean(ComisionesOutPut.class, obj1Fmeg0com, obj2Fmeg0com).generarSalida();
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

    }
    catch (Exception e) {

      String ident = obj1Fmeg0com.getId().getCdtpcomi().trim() + "_" + obj1Fmeg0com.getId().getCdcomisi().trim() + "_"
          + obj1Fmeg0com.getId().getCdtarifa().trim();

      logger.info(e.getMessage());
      ctx.getBean(RegistroControl.class, "Comisiones", ident, nombreXML, "E", e.getMessage(), "Comisiones", "MT")
          .write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void inputComisiones(int numReg) throws Exception {

    Fmeg0comId pkFmeg0com = new Fmeg0comId();

    Fmeg0com obj_persFmeg0com = new Fmeg0com();

    if (1 == numReg) {
      pkFmeg0com.setCdtpcomi(obj1Fmeg0com.getId().getCdtpcomi());
      pkFmeg0com.setCdcomisi(obj1Fmeg0com.getId().getCdcomisi());
      pkFmeg0com.setCdtarifa(obj1Fmeg0com.getId().getCdtarifa());
    }
    else {
      pkFmeg0com.setCdtpcomi(obj2Fmeg0com.getId().getCdtpcomi());
      pkFmeg0com.setCdcomisi(obj2Fmeg0com.getId().getCdcomisi());
      pkFmeg0com.setCdtarifa(obj2Fmeg0com.getId().getCdtarifa());
    }

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      obj_persFmeg0com = fmeg0comBo.findById(pkFmeg0com);
      if (obj_persFmeg0com == null) {
        if (1 == numReg) {
          obj1Fmeg0com.setTpmodeid(INPUT);
          obj1Fmeg0com = fmeg0comBo.save(obj1Fmeg0com, true);
        }
        else {
          obj2Fmeg0com.setTpmodeid(INPUT);
          obj2Fmeg0com = fmeg0comBo.save(obj2Fmeg0com, true);
        }

        tratarHijos();

        tx.commit();
      }
      else if ("".equals(obj_persFmeg0com.getFhdebaja().trim())) {
        // UPDATE

        // enviamos el xml de borrado antes de la actucalizacion y posterior
        // envio de una modificacion.
        DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
        String fechaSt = fechaFormato.format(new Date());
        obj_persFmeg0com.setFhdebaja(fechaSt);
        obj_persFmeg0com.setTpmodeid(DELETE);

        tx.commit();

        try {
          ctx.getBean(ComisionesOutPut.class, obj_persFmeg0com, null).generarSalida();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }

        tx.begin();
        fmeg0comBo.delete(obj_persFmeg0com);

        if (1 == numReg) {
          obj1Fmeg0com.setTpmodeid(UPDATE);
          obj1Fmeg0com = fmeg0comBo.save(obj1Fmeg0com, true);
        }
        else {
          obj2Fmeg0com.setTpmodeid(UPDATE);
          obj2Fmeg0com = fmeg0comBo.save(obj2Fmeg0com, true);
        }

        tratarHijos();

        tx.commit();
      }
      else {

        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0comBo.delete(obj_persFmeg0com);

        if (1 == numReg) {
          obj1Fmeg0com.setTpmodeid(INPUT);
          obj1Fmeg0com = fmeg0comBo.save(obj1Fmeg0com, true);
        }
        else {
          obj2Fmeg0com.setTpmodeid(INPUT);
          obj2Fmeg0com = fmeg0comBo.save(obj2Fmeg0com, true);
        }

        tratarHijos();

        tx.commit();
      }

    }
    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void deleteComisiones(int numReg) throws Exception {

    Fmeg0comId pkFmeg0com = new Fmeg0comId();
    Fmeg0com obj_persFmeg0com = new Fmeg0com();

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    if (1 == numReg) {
      pkFmeg0com.setCdtpcomi(obj1Fmeg0com.getId().getCdtpcomi());
      pkFmeg0com.setCdcomisi(obj1Fmeg0com.getId().getCdcomisi());
      pkFmeg0com.setCdtarifa(obj1Fmeg0com.getId().getCdtarifa());
    }
    else {
      pkFmeg0com.setCdtpcomi(obj2Fmeg0com.getId().getCdtpcomi());
      pkFmeg0com.setCdcomisi(obj2Fmeg0com.getId().getCdcomisi());
      pkFmeg0com.setCdtarifa(obj2Fmeg0com.getId().getCdtarifa());
    }

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      obj_persFmeg0com = fmeg0comBo.findById(pkFmeg0com);
      if (obj_persFmeg0com == null) {
        throw new Exception("EL REGISTRO NO PUEDE DARSE DE BAJA, NO EXISTE EN LA BASE DE DATOS");
      }
      else if ("".equals(obj_persFmeg0com.getFhdebaja().trim())) {
        obj_persFmeg0com.setFhdebaja(fechaSt);
        obj_persFmeg0com.setTpmodeid(DELETE);

        obj_persFmeg0com = fmeg0comBo.save(obj_persFmeg0com, false);
        if (numReg == 1) {
          obj1Fmeg0com = obj_persFmeg0com;
        }
        else {
          obj2Fmeg0com = obj_persFmeg0com;
        }

      }
      else {
        throw new Exception("EL REGISTRO NO PUEDE DARSE DE BAJA, SE ENCUENTRA EN ESTADO DE BAJA LOGICA");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void analizarXML() throws DOMException, Exception {

    StringTokenizer stAux;

    String bdAux;

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {

          case 0:// numreg

            numeroRegistros = Integer.parseInt(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));

            initObj(numeroRegistros);
            break;
          // -----------------------------------------------------------------

          case 1:// cdtpcomi
            obj1Fmeg0com.getId().setCdtpcomi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 2:// modo
            obj1Fmeg0com.setTpmodoop(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));

            if ((!"A".equals(obj1Fmeg0com.getTpmodoop())) && ("M".equals(obj1Fmeg0com.getTpmodoop()))
                && ("C".equals(obj1Fmeg0com.getTpmodoop()))) {
              throw new Exception("EL CAMPO 'tpmodoop1' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 3:// cdcomisi
            obj1Fmeg0com.getId().setCdcomisi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 4:// cdclente
            obj1Fmeg0com.setCdbrocli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 5:// cdcuenta
            obj1Fmeg0com.setCdaliass(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 6:// cdsubaccount
            obj1Fmeg0com.setCdsubcta(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 7:// cdbroker
            obj1Fmeg0com.setCdbroker(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 8:// cdtrato
            obj1Fmeg0com.setCdtratoo(convertirMegaraAs400.execConvertir("CDTRATOO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 9:// cdvia
            obj1Fmeg0com.setTpcdviaa(convertirMegaraAs400.execConvertir("TPCDVIAA", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 10:// dsgtomdo
            obj1Fmeg0com.setDsgtomdo(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 11:// cdreside
            obj1Fmeg0com.setCdreside(convertirMegaraAs400.execConvertir("CDRESIDE", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 12:// cdxtbrop
            obj1Fmeg0com.setCdxtbrop(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 13:// cdindice
            obj1Fmeg0com.setCdindice(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 14:// cdclsmdo
            obj1Fmeg0com.setCdclsmdo(convertirMegaraAs400.execConvertir("CDCLSMDO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 15:// cdcanall
            obj1Fmeg0com.setCdcanall(convertirMegaraAs400.execConvertir("CDCANALL", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 16:// cdmarket
            obj1Fmeg0com.setCdmarket(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 17:// cdtpmerc
            obj1Fmeg0com.setCdtpmerc(convertirMegaraAs400.execConvertir("CDTPMERC", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 18:// cdorigen
            obj1Fmeg0com.setCdorigen(convertirMegaraAs400.execConvertir("CDORIGEN", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 19:// cdtpopert
            obj1Fmeg0com.setCdtpoper(convertirMegaraAs400.execConvertir("CDTPOPER", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 20:// cdtpoptv
            obj1Fmeg0com.setCdtpoptv(convertirMegaraAs400.execConvertir("CDTPOPTV", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 21:// cdactivo
            obj1Fmeg0com.setCdactivo(convertirMegaraAs400.execConvertir("CDACTIVO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 22:// cdnifemi
            obj1Fmeg0com.setCdnifemi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 23:// cdisin
            obj1Fmeg0com.setCdisinvv(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 24:// cdperio
            obj1Fmeg0com.setCdperiod(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 25:// feinvig
            obj1Fmeg0com.setFhinivig(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 26:// fefinvig
            obj1Fmeg0com.setFhfinvig(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 27:// cdtarifa
            obj1Fmeg0com.getId().setCdtarifa(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 28:// cdtptari
            obj1Fmeg0com.setCdtptari(convertirMegaraAs400.execConvertir("CDTPTARI", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 29:// imfijo

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj1Fmeg0com.setImcomfij(new BigDecimal(0));
              }
              else {
                obj1Fmeg0com.setImcomfij(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'imfijo1' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 30:// potarifa

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj1Fmeg0com.setPctarifa(new BigDecimal(0));
              }
              else {
                obj1Fmeg0com.setPctarifa(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'potarifa1' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 31:// cdunidad
            obj1Fmeg0com.setCdunidad(convertirMegaraAs400.execConvertir("CDUNIDAD", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 32:// imminimo

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj1Fmeg0com.setImminimo(new BigDecimal(0));
              }
              else {
                obj1Fmeg0com.setImminimo(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'imminimo1' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 33:// immaximo

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj1Fmeg0com.setImmaximo(new BigDecimal(0));
              }
              else {
                obj1Fmeg0com.setImmaximo(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'immaximo1' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 34:// imdescue

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj1Fmeg0com.setImdescue(new BigDecimal(0));
              }
              else {
                obj1Fmeg0com.setImdescue(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'imdescue1' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 35:// cdmoniso
            obj1Fmeg0com.setCdmoniso(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 36:// cdtpdevo
            obj1Fmeg0com.setCdtpdevo(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 37:// xttramos
            obj1Fmeg0com.setCdtramos(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 38:// cdtpcalc
            obj1Fmeg0com.setCdtpcalc(convertirMegaraAs400.execConvertir("CDTPCALC", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 39:// cdtptram
            obj1Fmeg0com.setCdtptram(convertirMegaraAs400.execConvertir("CDTPTRAM", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 40:// xtexiste
            obj1Fmeg0com.setCdexiste(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 41:// feestado
            obj1Fmeg0com.setFhestado(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 42:// xtexcepc
            obj1Fmeg0com.setCdexcepc(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          // -----------------------------------------------------------------

          case 43:// cdtpcomi
            obj2Fmeg0com.getId().setCdtpcomi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 44:// modo
            obj2Fmeg0com.setTpmodoop(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));

            if ((!"A".equals(obj2Fmeg0com.getTpmodoop())) && ("M".equals(obj2Fmeg0com.getTpmodoop()))
                && ("C".equals(obj2Fmeg0com.getTpmodoop()))) {
              throw new Exception("EL CAMPO 'tpmodoop2' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 45:// cdcomisi
            obj2Fmeg0com.getId().setCdcomisi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 46:// cdclente
            obj2Fmeg0com.setCdbrocli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 47:// cdcuenta
            obj2Fmeg0com.setCdaliass(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 48:// cdsubaccount
            obj2Fmeg0com.setCdsubcta(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 49:// cdbroker
            obj2Fmeg0com.setCdbroker(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 50:// cdtrato
            obj2Fmeg0com.setCdtratoo(convertirMegaraAs400.execConvertir("CDTRATOO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 51:// cdvia
            obj2Fmeg0com.setTpcdviaa(convertirMegaraAs400.execConvertir("TPCDVIAA", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 52:// dsgtomdo
            obj2Fmeg0com.setDsgtomdo(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 53:// cdreside
            obj2Fmeg0com.setCdreside(convertirMegaraAs400.execConvertir("CDRESIDE", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 54:// cdxtbrop
            obj2Fmeg0com.setCdxtbrop(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 55:// cdindice
            obj2Fmeg0com.setCdindice(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 56:// cdclsmdo
            obj2Fmeg0com.setCdclsmdo(convertirMegaraAs400.execConvertir("CDCLSMDO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 57:// cdcanall
            obj2Fmeg0com.setCdcanall(convertirMegaraAs400.execConvertir("CDCANALL", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 58:// cdmarket
            obj2Fmeg0com.setCdmarket(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 59:// cdtpmerc
            obj2Fmeg0com.setCdtpmerc(convertirMegaraAs400.execConvertir("CDTPMERC", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 60:// cdorigen
            obj2Fmeg0com.setCdorigen(convertirMegaraAs400.execConvertir("CDORIGEN", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 61:// cdtpopert
            obj2Fmeg0com.setCdtpoper(convertirMegaraAs400.execConvertir("CDTPOPER", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 62:// cdtpoptv
            obj2Fmeg0com.setCdtpoptv(convertirMegaraAs400.execConvertir("CDTPOPTV", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 63:// cdactivo
            obj2Fmeg0com.setCdactivo(convertirMegaraAs400.execConvertir("CDACTIVO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 64:// cdnifemi
            obj2Fmeg0com.setCdnifemi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 65:// cdisin
            obj2Fmeg0com.setCdisinvv(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 66:// cdperio
            obj2Fmeg0com.setCdperiod(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 67:// feinvig
            obj2Fmeg0com.setFhinivig(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 68:// fefinvig
            obj2Fmeg0com.setFhfinvig(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 69:// cdtarifa
            obj2Fmeg0com.getId().setCdtarifa(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 70:// cdtptari
            obj2Fmeg0com.setCdtptari(convertirMegaraAs400.execConvertir("CDTPTARI", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 71:// imfijo

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj2Fmeg0com.setImcomfij(new BigDecimal(0));
              }
              else {
                obj2Fmeg0com.setImcomfij(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'imfijo2' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 72:// potarifa

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj2Fmeg0com.setPctarifa(new BigDecimal(0));
              }
              else {
                obj2Fmeg0com.setPctarifa(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'potarifa2' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 73:// cdunidad
            obj2Fmeg0com.setCdunidad(convertirMegaraAs400.execConvertir("CDUNIDAD", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 74:// imminimo

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj2Fmeg0com.setImminimo(new BigDecimal(0));
              }
              else {
                obj2Fmeg0com.setImminimo(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'imminimo2' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 75:// immaximo

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj2Fmeg0com.setImmaximo(new BigDecimal(0));
              }
              else {
                obj2Fmeg0com.setImmaximo(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'immaximo2' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 76:// imdescue

            try {

              stAux = new StringTokenizer(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), ".");

              bdAux = "";
              while (stAux.hasMoreElements())
                bdAux += stAux.nextElement();

              bdAux = bdAux.replace(',', '.');

              if ("".equals(bdAux.trim())) {
                obj2Fmeg0com.setImdescue(new BigDecimal(0));
              }
              else {
                obj2Fmeg0com.setImdescue(new BigDecimal(bdAux));
              }

            }
            catch (Exception e) {
              throw new Exception("EL CAMPO 'imdescue2' CONTIENE UN VALOR INCORRECTO");
            }

            break;

          case 77:// cdmoniso
            obj2Fmeg0com.setCdmoniso(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 78:// cdtpdevo
            obj2Fmeg0com.setCdtpdevo(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 79:// xttramos
            obj2Fmeg0com.setCdtramos(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 80:// cdtpcalc
            obj2Fmeg0com.setCdtpcalc(convertirMegaraAs400.execConvertir("CDTPCALC", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 81:// cdtptram
            obj2Fmeg0com.setCdtptram(convertirMegaraAs400.execConvertir("CDTPTRAM", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 82:// xtexiste
            obj2Fmeg0com.setCdexiste(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 83:// feestado
            obj2Fmeg0com.setFhestado(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          case 84:// xtexcepc
            obj2Fmeg0com.setCdexcepc(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          default:
            ;
        }
      }
    }

  }

  private void tratarHijos() throws DOMException, Exception {

    for (int i = 0; i < (document.getElementsByTagName("row").getLength()); i++) {

      if (document.getElementsByTagName("row").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarTramos(((Element) document.getElementsByTagName("row").item(i)));
      }

    }
  }

  private void tratarTramos(Node tramo) throws DOMException, Exception {

    StringTokenizer stAux;

    String bdAux;

    Fmeg0ctr objFmeg0ctr = new Fmeg0ctr();
    objFmeg0ctr.setId(new Fmeg0ctrId("", "", "", ""));
    objFmeg0ctr.getId().setCdtramoo("");
    objFmeg0ctr.setImlimnim(new BigDecimal(0));
    objFmeg0ctr.setImlimnim(new BigDecimal(0));
    objFmeg0ctr.setImfijonu(new BigDecimal(0));
    objFmeg0ctr.setCdestado("");
    objFmeg0ctr.setPotarifa(new BigDecimal(0));
    objFmeg0ctr.setFhestado("");

    NodeList listField = tramo.getChildNodes();

    String cdtpcomi = null;

    String cdcomisi = null;

    String cdtarifa = null;

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrTramos.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {

            case 0:// cdtpcomi
              cdtpcomi = (listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            case 1:// cdcomisi
              cdcomisi = (listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            case 2:// cdtarifa
              cdtarifa = (listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            case 3:// cdtramo
              objFmeg0ctr.getId().setCdtramoo(listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            case 4:// cdcltram
              objFmeg0ctr.setCdcltram(listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            case 5:// imlimnim

              try {

                stAux = new StringTokenizer(listField.item(i).getAttributes().item(1).getNodeValue(), ".");

                bdAux = "";
                while (stAux.hasMoreElements())
                  bdAux += stAux.nextElement();

                bdAux = bdAux.replace(',', '.');

                if ("".equals(bdAux.trim())) {
                  objFmeg0ctr.setImlimnim(new BigDecimal(0));
                }
                else {
                  objFmeg0ctr.setImlimnim(new BigDecimal(bdAux));
                }

              }
              catch (Exception e) {
                throw new Exception("EL CAMPO 'row => imlimnim' CONTIENE UN VALOR INCORRECTO");
              }

              break;

            case 6:// imlimmax

              try {

                stAux = new StringTokenizer(listField.item(i).getAttributes().item(1).getNodeValue(), ".");

                bdAux = "";
                while (stAux.hasMoreElements())
                  bdAux += stAux.nextElement();

                bdAux = bdAux.replace(',', '.');

                if ("".equals(bdAux.trim())) {
                  objFmeg0ctr.setImlimmax(new BigDecimal(0));
                }
                else {
                  objFmeg0ctr.setImlimmax(new BigDecimal(bdAux));
                }

              }
              catch (Exception e) {
                throw new Exception("EL CAMPO 'row => imlimmax' CONTIENE UN VALOR INCORRECTO");
              }

              break;

            case 7:// imfijo

              try {

                stAux = new StringTokenizer(listField.item(i).getAttributes().item(1).getNodeValue(), ".");

                bdAux = "";
                while (stAux.hasMoreElements())
                  bdAux += stAux.nextElement();

                bdAux = bdAux.replace(',', '.');

                if ("".equals(bdAux.trim())) {
                  objFmeg0ctr.setImfijonu(new BigDecimal(0));
                }
                else {
                  objFmeg0ctr.setImfijonu(new BigDecimal(bdAux));
                }

              }
              catch (Exception e) {
                throw new Exception("EL CAMPO 'row => imfijo' CONTIENE UN VALOR INCORRECTO");
              }

              break;

            case 8:// cdestado
              objFmeg0ctr.setCdestado(convertirMegaraAs400.execConvertir("CDESTADO", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;

            case 9:// potarifa

              try {

                stAux = new StringTokenizer(listField.item(i).getAttributes().item(1).getNodeValue(), ".");

                bdAux = "";
                while (stAux.hasMoreElements())
                  bdAux += stAux.nextElement();

                bdAux = bdAux.replace(',', '.');

                if ("".equals(bdAux.trim())) {
                  objFmeg0ctr.setPotarifa(new BigDecimal(0));
                }
                else {
                  objFmeg0ctr.setPotarifa(new BigDecimal(bdAux));
                }

              }
              catch (Exception e) {
                throw new Exception("EL CAMPO 'row => potarifa' CONTIENE UN VALOR INCORRECTO");
              }

              break;

            case 10:// feestado
              objFmeg0ctr.setFhestado(listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            default:
              ;
          }
        }
      }
    }

    Fmeg0com objFmeg0com = null;
    Fmeg0comId pkFmeg0com = new Fmeg0comId(cdtpcomi, cdcomisi, cdtarifa);

    if (!(null == cdtpcomi) && !(null == cdcomisi) && !(null == cdtarifa)) {

      objFmeg0com = fmeg0comBo.findById(pkFmeg0com);

      objFmeg0ctr.setFmeg0com(objFmeg0com);
    }
    else {
      if (!(null == obj2Fmeg0com)) {
        objFmeg0ctr.setFmeg0com(obj2Fmeg0com);
      }
      else {
        objFmeg0ctr.setFmeg0com(obj1Fmeg0com);
      }
    }
    meg0ctrBo.save(objFmeg0ctr, true);

  }

  private void initObj(int numeroRegistros) throws Exception {

    switch (numeroRegistros) {

      case 1:
        obj1Fmeg0com = new Fmeg0com();
        obj1Fmeg0com.setId(new Fmeg0comId("", "", ""));
        obj1Fmeg0com.setIdnumreg(new BigDecimal(numeroRegistros));
        obj1Fmeg0com.getId().setCdtpcomi("");
        obj1Fmeg0com.setTpmodoop("");
        obj1Fmeg0com.setCdbrocli("");
        obj1Fmeg0com.setCdaliass("");
        obj1Fmeg0com.setCdsubcta("");
        obj1Fmeg0com.setCdbroker("");
        obj1Fmeg0com.setCdtratoo("");
        obj1Fmeg0com.setTpcdviaa("");
        obj1Fmeg0com.setDsgtomdo("");
        obj1Fmeg0com.setCdreside("");
        obj1Fmeg0com.setCdxtbrop("");
        obj1Fmeg0com.setCdindice("");
        obj1Fmeg0com.setCdclsmdo("");
        obj1Fmeg0com.setCdcanall("");
        obj1Fmeg0com.setCdmarket("");
        obj1Fmeg0com.setCdtpmerc("");
        obj1Fmeg0com.setCdorigen("");
        obj1Fmeg0com.setCdtpoper("");
        obj1Fmeg0com.setCdtpoptv("");
        obj1Fmeg0com.setCdactivo("");
        obj1Fmeg0com.setCdnifemi("");
        obj1Fmeg0com.setCdisinvv("");
        obj1Fmeg0com.setCdperiod("");
        obj1Fmeg0com.setFhinivig("");
        obj1Fmeg0com.setFhfinvig("");
        obj1Fmeg0com.setCdtptari("");
        obj1Fmeg0com.setImcomfij(new BigDecimal(0));
        obj1Fmeg0com.setPctarifa(new BigDecimal(0));
        obj1Fmeg0com.setCdunidad("");
        obj1Fmeg0com.setImminimo(new BigDecimal(0));
        obj1Fmeg0com.setImmaximo(new BigDecimal(0));
        obj1Fmeg0com.setImdescue(new BigDecimal(0));
        obj1Fmeg0com.setCdmoniso("");
        obj1Fmeg0com.setCdtpdevo("");
        obj1Fmeg0com.setCdtramos("");
        obj1Fmeg0com.setCdtpcalc("");
        obj1Fmeg0com.setCdtptram("");
        obj1Fmeg0com.setCdexiste("");
        obj1Fmeg0com.setFhestado("");
        obj1Fmeg0com.setCdexcepc("");
        obj1Fmeg0com.setFhdebaja("");
        obj1Fmeg0com.setTpmodeid("");
        break;
      case 2:
        obj1Fmeg0com = new Fmeg0com();
        obj1Fmeg0com.setId(new Fmeg0comId("", "", ""));
        obj1Fmeg0com.setIdnumreg(new BigDecimal(numeroRegistros));
        obj1Fmeg0com.setTpmodoop("");
        obj1Fmeg0com.setCdbrocli("");
        obj1Fmeg0com.setCdaliass("");
        obj1Fmeg0com.setCdsubcta("");
        obj1Fmeg0com.setCdbroker("");
        obj1Fmeg0com.setCdtratoo("");
        obj1Fmeg0com.setTpcdviaa("");
        obj1Fmeg0com.setDsgtomdo("");
        obj1Fmeg0com.setCdreside("");
        obj1Fmeg0com.setCdxtbrop("");
        obj1Fmeg0com.setCdindice("");
        obj1Fmeg0com.setCdclsmdo("");
        obj1Fmeg0com.setCdcanall("");
        obj1Fmeg0com.setCdmarket("");
        obj1Fmeg0com.setCdtpmerc("");
        obj1Fmeg0com.setCdorigen("");
        obj1Fmeg0com.setCdtpoper("");
        obj1Fmeg0com.setCdtpoptv("");
        obj1Fmeg0com.setCdactivo("");
        obj1Fmeg0com.setCdnifemi("");
        obj1Fmeg0com.setCdisinvv("");
        obj1Fmeg0com.setCdperiod("");
        obj1Fmeg0com.setFhinivig("");
        obj1Fmeg0com.setFhfinvig("");
        obj1Fmeg0com.setCdtptari("");
        obj1Fmeg0com.setImcomfij(new BigDecimal(0));
        obj1Fmeg0com.setPctarifa(new BigDecimal(0));
        obj1Fmeg0com.setCdunidad("");
        obj1Fmeg0com.setImminimo(new BigDecimal(0));
        obj1Fmeg0com.setImmaximo(new BigDecimal(0));
        obj1Fmeg0com.setImdescue(new BigDecimal(0));
        obj1Fmeg0com.setCdmoniso("");
        obj1Fmeg0com.setCdtpdevo("");
        obj1Fmeg0com.setCdtramos("");
        obj1Fmeg0com.setCdtpcalc("");
        obj1Fmeg0com.setCdtptram("");
        obj1Fmeg0com.setCdexiste("");
        obj1Fmeg0com.setFhestado("");
        obj1Fmeg0com.setCdexcepc("");
        obj1Fmeg0com.setFhdebaja("");
        obj1Fmeg0com.setTpmodeid("");

        obj2Fmeg0com = new Fmeg0com();
        obj2Fmeg0com.setId(new Fmeg0comId("", "", ""));
        obj2Fmeg0com.setIdnumreg(new BigDecimal(numeroRegistros));
        obj2Fmeg0com.setTpmodoop("");
        obj2Fmeg0com.setCdbrocli("");
        obj2Fmeg0com.setCdaliass("");
        obj2Fmeg0com.setCdsubcta("");
        obj2Fmeg0com.setCdbroker("");
        obj2Fmeg0com.setCdtratoo("");
        obj2Fmeg0com.setTpcdviaa("");
        obj2Fmeg0com.setDsgtomdo("");
        obj2Fmeg0com.setCdreside("");
        obj2Fmeg0com.setCdxtbrop("");
        obj2Fmeg0com.setCdindice("");
        obj2Fmeg0com.setCdclsmdo("");
        obj2Fmeg0com.setCdcanall("");
        obj2Fmeg0com.setCdmarket("");
        obj2Fmeg0com.setCdtpmerc("");
        obj2Fmeg0com.setCdorigen("");
        obj2Fmeg0com.setCdtpoper("");
        obj2Fmeg0com.setCdtpoptv("");
        obj2Fmeg0com.setCdactivo("");
        obj2Fmeg0com.setCdnifemi("");
        obj2Fmeg0com.setCdisinvv("");
        obj2Fmeg0com.setCdperiod("");
        obj2Fmeg0com.setFhinivig("");
        obj2Fmeg0com.setFhfinvig("");
        obj2Fmeg0com.setCdtptari("");
        obj2Fmeg0com.setImcomfij(new BigDecimal(0));
        obj2Fmeg0com.setPctarifa(new BigDecimal(0));
        obj2Fmeg0com.setCdunidad("");
        obj2Fmeg0com.setImminimo(new BigDecimal(0));
        obj2Fmeg0com.setImmaximo(new BigDecimal(0));
        obj2Fmeg0com.setImdescue(new BigDecimal(0));
        obj2Fmeg0com.setCdmoniso("");
        obj2Fmeg0com.setCdtpdevo("");
        obj2Fmeg0com.setCdtramos("");
        obj2Fmeg0com.setCdtpcalc("");
        obj2Fmeg0com.setCdtptram("");
        obj2Fmeg0com.setCdexiste("");
        obj2Fmeg0com.setFhestado("");
        obj2Fmeg0com.setCdexcepc("");
        obj2Fmeg0com.setFhdebaja("");
        obj2Fmeg0com.setTpmodeid("");
        break;

      default:
        throw new Exception("EL CAMPO 'numreg' CONTIENE UN VALOR INCORRECTO");

    }

  }

}