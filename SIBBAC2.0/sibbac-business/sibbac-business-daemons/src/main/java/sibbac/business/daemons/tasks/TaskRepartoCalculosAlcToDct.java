package sibbac.business.daemons.tasks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.reparto.calculos.bo.RepartoCalculosAlcToDctMultithread;
import sibbac.business.daemons.reparto.calculos.bo.RepartoCalculosAlctToDctDTO;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.database.bo.calculos.dto.CalculosEconomicosFlagsDTO;
import sibbac.business.wrappers.database.dao.Tmct0alcDao;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_REPARTO_CALCULOS_ALC_TO_DCT, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 15 5,20 ? * MON-FRI")
public class TaskRepartoCalculosAlcToDct extends WrapperTaskConcurrencyPrevent {

  private static final Logger LOG = LoggerFactory.getLogger(TaskRepartoCalculosAlcToDct.class);

  @Autowired
  private Tmct0alcDao alcDao;

  @Autowired
  private RepartoCalculosAlcToDctMultithread reparto;

  @Autowired
  private Tmct0cfgBo cfgBo;

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio.");
    Date fetrade;
    Calendar fetradeCalendar;
    Calendar nowCalendar;

    Tmct0cfg firstFetradeCfg = cfgBo.getFirstDateRepartirCalculos();
    if (firstFetradeCfg == null) {
      throw new SIBBACBusinessException("Fecha inicio proceso reparto calculos no configurada.");
    }

    Tmct0cfg lastFetradeCfg = cfgBo.getLastDateRepartirCalculos();
    if (lastFetradeCfg == null) {
      throw new SIBBACBusinessException("Fecha fin proceso reparto calculos no configurada.");
    }
    BigDecimal firstFetrade = FormatDataUtils.convertStringToBigDecimal(firstFetradeCfg.getKeyvalue().trim());
    BigDecimal lastFetrade = null;
    if (!lastFetradeCfg.getKeyvalue().trim().isEmpty()) {
      lastFetrade = FormatDataUtils.convertStringToBigDecimal(lastFetradeCfg.getKeyvalue().trim());
    }

    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(new Date()));

    while (lastFetrade == null && now.compareTo(firstFetrade) >= 0 || lastFetrade != null
        && firstFetrade.compareTo(lastFetrade) <= 0) {

      this.repartirCalculosFase1(firstFetrade);

      fetrade = FormatDataUtils.convertStringToDate(firstFetrade.toString());
      fetradeCalendar = Calendar.getInstance();
      fetradeCalendar.setTime(fetrade);
      fetradeCalendar.add(Calendar.DAY_OF_YEAR, 1);
      fetrade = fetradeCalendar.getTime();
      firstFetrade = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertDateToString(fetrade));

      if (firstFetrade.add(new BigDecimal(30)).compareTo(now) < 0) {
        firstFetradeCfg.setKeyvalue(firstFetrade.toString());
        cfgBo.saveAndFlush(firstFetradeCfg);
      }
      nowCalendar = Calendar.getInstance();
      if (nowCalendar.get(Calendar.HOUR_OF_DAY) == 0) {
        break;
      }
    }

    lastFetradeCfg.setKeyvalue("");

    cfgBo.saveAndFlush(lastFetradeCfg);
    LOG.info("[executeTask] fin.");
  }

  private void repartirCalculosFase1(BigDecimal firstFetrade) throws SIBBACBusinessException {
    int pos = 0;
    int posEnd = 0;
    List<Object[]> listAlcIdCalculosEconomicosFase1;
    List<RepartoCalculosAlctToDctDTO> listAlcs;
    LOG.info("[executeTask] buscando calculos mal repartidos dia: {}", firstFetrade);
    listAlcIdCalculosEconomicosFase1 = alcDao.findAllAlcIdWithCalculosMalRepartidosByFetradeAndCdestadoasigGte(
        FormatDataUtils.convertStringToDate(firstFetrade.toString()),
        EstadosEnumerados.ASIGNACIONES.PDTE_LIQUIDAR.getId());
    LOG.info("[executeTask] Se han encontrado {} alcs mal repartidos.", listAlcIdCalculosEconomicosFase1.size());

    while (pos < listAlcIdCalculosEconomicosFase1.size()) {
      posEnd = Math.min(posEnd + 100000, listAlcIdCalculosEconomicosFase1.size());
      LOG.info("[executeTask] Lanzando de {} hasta {}.", pos, posEnd);
      listAlcs = this.recuperarListAlcIdFase1(listAlcIdCalculosEconomicosFase1.subList(pos, posEnd));
      if (!listAlcs.isEmpty())
        reparto.repartirCalculosAlcToDct(listAlcs, "REPARTOCA1");

      listAlcs.clear();
      pos = posEnd;
    }
    listAlcIdCalculosEconomicosFase1.clear();
  }

  private List<RepartoCalculosAlctToDctDTO> recuperarListAlcIdFase1(List<Object[]> data) {
    List<RepartoCalculosAlctToDctDTO> listAlcs = new ArrayList<RepartoCalculosAlctToDctDTO>();
    RepartoCalculosAlctToDctDTO id;
    CalculosEconomicosFlagsDTO flags;
    for (Object[] o : data) {
      id = new RepartoCalculosAlctToDctDTO();
      id.setAlcId(new Tmct0alcId((String) o[1], (String) o[0], Short.parseShort(((BigDecimal) o[3]).toString()),
          (String) o[2]));
      flags = new CalculosEconomicosFlagsDTO();
      flags.setRepartirBrutoClienteFidessa(false);
      flags.setRepartirNetoClienteFidessa(false);
      flags.setCalcularComisionCliente(false);
      flags.setRepartirComisionOrdenante(false);
      flags.setCalcularCanones(o[4] != null && !(boolean) o[4] || o[5] != null && !(boolean) o[5]);
      id.setFlags(flags);
      listAlcs.add(id);
    }
    return listAlcs;
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_REPARTO_CALCULOS_ECONOMICOS_ALC_TO_DCT;
  }

}
