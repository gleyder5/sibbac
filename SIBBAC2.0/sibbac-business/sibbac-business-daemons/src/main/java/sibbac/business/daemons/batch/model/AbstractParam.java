package sibbac.business.daemons.batch.model;

import java.io.Serializable;
import java.sql.Types;
import static java.text.MessageFormat.format;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

@MappedSuperclass
public abstract class AbstractParam implements Serializable {

	/**
	 * Default servial version
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String QUOTED = "'%s'";
	
	private static final String QUOTED_DATE = "'%tF'";
	
	/**
	 * Local logger
	 */
	private static final Logger LOG = getLogger(AbstractParam.class);

  @Column(name = "TYPE")
  private int type;

  @Column(name = "VALUE", length = 50)
  private String value;
  
  @Transient
  private boolean fixed = false;
 
  public int getType() {
    return type;
  }

  public String getValue() {
    return value;
  }

  public void setType(int type) {
    this.type = type;
  }

  public void setValue(String value) {
    this.value = value;
  }
  
  public void setFixed(boolean fixed) {
    this.fixed = fixed;
  }
  
  @Transient
  public void fillParameter(StringBuilder sqlBuilder, Object actualValue) {
    final String message; 
    final int pos;

    pos = sqlBuilder.indexOf("?");
    if(pos < 0) {
      return;
    }
    if(fixed || actualValue == null) {
      if(value == null) {
        setNull(sqlBuilder);
      }
      else {
        fillParameterWithString(sqlBuilder, value);
      }
      return;
    }
    try {
      switch (type) {
        case Types.BIGINT:
        case Types.DECIMAL:
        case Types.DOUBLE:
        case Types.FLOAT:
        case Types.REAL:
        case Types.INTEGER:
        case Types.SMALLINT:
        case Types.TINYINT:
          sqlBuilder.replace(pos, pos + 1, actualValue.toString());
          return;
        case Types.BOOLEAN:
          sqlBuilder.replace(pos, pos + 1, String.format(QUOTED, 
              Boolean.class.cast(actualValue).booleanValue()  ? "S" : "N"));
          return;
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
        case Types.CLOB:
        case Types.OTHER:
          sqlBuilder.replace(pos, pos + 1, String.format(QUOTED, actualValue.toString()));
          return;
        case Types.DATE:
        case Types.TIME:
        case Types.TIMESTAMP:
          sqlBuilder.replace(pos, pos + 1, String.format(QUOTED_DATE, actualValue));
          return;
        default:
          message = format("Tipo JDBC no gestionado: {0}, con valor {1}", type, actualValue);
          LOG.error("[fillParameter]{}", message);
          throw new IllegalArgumentException(message);
      }
    }
    catch(RuntimeException rex) {
      LOG.error("[fillParameterWithString] Error no controlado en Param {}  con valor {} en posicion {}",
          getIdString(), actualValue, pos, rex);
    }
    LOG.trace("[fillParameterWithString] Se asigna nulo para la posicion {} en Param {}", pos, getIdString());
    setNull(sqlBuilder);
  }

  protected abstract String getIdString();
  
  @Transient
  void autoFill(StringBuilder sqlBuilder) {
    if(value == null) {
      setNull(sqlBuilder);
      return;
    }
    fillParameterWithString(sqlBuilder, value);
  }
  
  @Transient
  void fillParameterWithString(StringBuilder sqlBuilder, String actualValue) {
    final String message; 
    final boolean relleno;
    final int pos;

    pos = sqlBuilder.indexOf("?");
    if(pos < 0) {
      return;
    }
    if(fixed || actualValue == null) {
      if(value == null) {
        setNull(sqlBuilder);
        return;
      }
      actualValue = value;
    }
    relleno = !actualValue.trim().isEmpty();
    try {
      switch (type) {
        case Types.BIGINT:
        case Types.DECIMAL:
        case Types.DOUBLE:
        case Types.FLOAT:
        case Types.REAL:
        case Types.INTEGER:
        case Types.SMALLINT:
        case Types.TINYINT:
          if(relleno) {
            sqlBuilder.replace(pos, pos + 1, actualValue);
            return;
          }
          break;
        case Types.BOOLEAN:
          sqlBuilder.replace(pos, pos + 1, String.format(QUOTED, 
              actualValue.length() > 0 ? actualValue.substring(0, 1) : "N"));
          return;
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
        case Types.CLOB:
        case Types.OTHER:
          sqlBuilder.replace(pos, pos + 1, String.format(QUOTED, actualValue));
          return;
        case Types.DATE:
        case Types.TIME:
        case Types.TIMESTAMP:
          if(relleno) {
            if(!actualValue.contains("-") && actualValue.length() == 8) {
              actualValue = new StringBuilder(actualValue).insert(4, '-').insert(7, '-').toString();
            }
            sqlBuilder.replace(pos, pos + 1, String.format(QUOTED, actualValue));
            return;
          }
          break;
        default:
          message = format("Tipo JDBC no gestionado: {0}, con valor {1}", type, actualValue);
          LOG.error("[fillParameter]{}", message);
          throw new IllegalArgumentException(message);
      }
    }
    catch(RuntimeException rex) {
      LOG.error("[fillParameterWithString] Error no controlado en Param {}  con valor {} en posicion {}",
          getIdString(), actualValue, pos, rex);
    }
    LOG.trace("[fillParameterWithString] Se asigna nulo para la posicion {} en Param {}", pos, getIdString());
    setNull(sqlBuilder);
  }

  /**
   * Sustituye el primer signo ? en la sentencia con la igualdad a nulo apropiada 
   * @param sqlBuilder StringBuilder que contiene la sentencia a modificar
   */
  @Transient
  void setNull(StringBuilder sqlBuilder) {
    final int quotePos, wherePos;
    final String upper, untilPos;
    int equalityPos;
    
    quotePos = sqlBuilder.indexOf("?");
    if(quotePos < 0) {
      return;
    }
    upper = sqlBuilder.toString().toUpperCase();
    if(upper.startsWith("SELECT")) {
      wherePos = upper.indexOf("FROM ");
    }
    else {
      wherePos = upper.indexOf("WHERE ");
    }
    if(wherePos >= 0 && quotePos > wherePos) {
      untilPos = sqlBuilder.toString().substring(0, quotePos);
      equalityPos = untilPos.lastIndexOf("!=");
      if(equalityPos < 0) {
        equalityPos = untilPos.lastIndexOf("<>");
      }
      if(equalityPos >= 0) {
        sqlBuilder.replace(equalityPos, quotePos + 1, "IS NOT NULL");
        return;
      }
      equalityPos = untilPos.lastIndexOf("=");
      if(equalityPos >= 0) {
        sqlBuilder.replace(equalityPos, quotePos + 1, "IS NULL");
        return;
      }
    }
    sqlBuilder.replace(quotePos, quotePos + 1, "NULL");
  }
  
}
