package sibbac.business.estaticos.fidessa.fda.output;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ClientDeliveryInstructionMegara;
import sibbac.business.estaticos.utils.conversiones.ConvertirAs400Fidessa;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.bo.Fmeg0chaBo;
import sibbac.database.megbpdta.bo.Fmeg0entBo;
import sibbac.database.megbpdta.model.Fmeg0cha;
import sibbac.database.megbpdta.model.Fmeg0ent;

@Service
@Scope(value = "prototype")
public class SLASettlementChainClientDeliveryInstruction extends ClientDeliveryInstructionMegara {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory
      .getLogger(SLASettlementChainClientDeliveryInstruction.class);

  private Fmeg0cha objFmeg0cha;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;
  @Autowired
  private Fmeg0chaBo fmeg0chaBo;

  @Autowired
  private Fmeg0entBo fmeg0entBo;

  @Autowired
  private ConvertirAs400Fidessa convertirAs400Fidessa;

  public SLASettlementChainClientDeliveryInstruction(Fmeg0cha objFmeg0cha) {

    super();

    this.objFmeg0cha = objFmeg0cha;

  }

  public boolean procesarXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    generarXML();

    RegistroControl objRegistroControl = generarRegistroControl();

    objRegistroControl.write();

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    String pkFmeg0cha = objFmeg0cha.getCdinsliq();

    try {

      tx.begin();

      // ----------------------------------------------------------------------------

      this.objFmeg0cha = fmeg0chaBo.findById(pkFmeg0cha);

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0cha.getTpmodeid().trim());

      // ----------------------------------------------------------------------------

      setClient_mnemonic(objFmeg0cha.getCdaliass().trim());

      // ----------------------------------------------------------------------------

      setAccount_mnemonic(objFmeg0cha.getCdsubcta().trim());

      // ----------------------------------------------------------------------------

      setMnemonic(objFmeg0cha.getIdcountp().trim());

      // ----------------------------------------------------------------------------

      try {

        String pkFmeg0ent = objFmeg0cha.getIdcountp().trim();
        Fmeg0ent objFmeg0ent = fmeg0entBo.findById(pkFmeg0ent);

        setEntity_name(objFmeg0ent.getNbnameid().trim());
        setEntity_bic(objFmeg0ent.getCdbiccid().trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
        if ("".equals(getMensajeError())) {
          setMensajeError("No se ha encontrado la entidad: " + objFmeg0cha.getIdcountp().trim());
        }

      }

      // ----------------------------------------------------------------------------

      setMarket(objFmeg0cha.getCdmarket().trim());

      try {

        String pkFmeg0ent = objFmeg0cha.getCdmarket().trim();
        Fmeg0ent objFmeg0ent = fmeg0entBo.findById(pkFmeg0ent);

        setMarket_name(objFmeg0ent.getNbnameid().trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

      // ----------------------------------------------------------------------------

      setPlace_of_settlement(objFmeg0cha.getCdbicmar().trim());

      // ----------------------------------------------------------------------------

      try {

        setSettlement_type(convertirAs400Fidessa.execConvertir("TPSETTLE", objFmeg0cha.getTpsettle()).trim());
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
        if ("".equals(getMensajeError())) {
          setMensajeError(e.getMessage());
        }
      }

      // ----------------------------------------------------------------------------

      setLocal_cust_mn(objFmeg0cha.getCdloccus().trim());

      try {

        String pkFmeg0ent = objFmeg0cha.getCdloccus().trim();
        Fmeg0ent objFmeg0ent = fmeg0entBo.findById(pkFmeg0ent);

        setLocal_cust_name(objFmeg0ent.getNbnameid().trim());
        setLocal_cust_bic(objFmeg0ent.getCdbiccid().trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
        if ("".equals(getMensajeError())) {
          setMensajeError("No se ha encontrado la entidad: " + objFmeg0cha.getCdloccus().trim());
        }
      }

      setLocal_cust_acc(objFmeg0cha.getAcloccus().trim());
      setLocal_cust_bic_id(objFmeg0cha.getIdloccus().trim());

      // ----------------------------------------------------------------------------

      setGlobal_cust_mn(objFmeg0cha.getCdglocus().trim());

      try {

        String pkFmeg0ent = objFmeg0cha.getCdglocus().trim();
        Fmeg0ent objFmeg0ent = fmeg0entBo.findById(pkFmeg0ent);

        setGlobal_cust_name(objFmeg0ent.getNbnameid().trim());
        setGlobal_cust_bic(objFmeg0ent.getCdbiccid().trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

      setGlobal_cust_acc(objFmeg0cha.getAcglocus().trim());
      setGlobal_cust_bic_id(objFmeg0cha.getIdglocus().trim());

      // ----------------------------------------------------------------------------

      setBeneficiary_mn(objFmeg0cha.getCdbenefi().trim());

      try {

        String pkFmeg0ent = objFmeg0cha.getCdbenefi().trim();
        Fmeg0ent objFmeg0ent = fmeg0entBo.findById(pkFmeg0ent);

        setBeneficiary_name(objFmeg0ent.getNbnameid().trim());
        setBeneficiary_bic(objFmeg0ent.getCdbiccid().trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

      setBeneficiary_acc(objFmeg0cha.getAcbenefi().trim());
      setBeneficiary_bic_id(objFmeg0cha.getIdbenefi().trim());

      // ----------------------------------------------------------------------------
      /* MCG 18-03-2010 */
      /* INCLUYO FECHA DE VALIDEZ */

      /* AML 21-04-2010 */
      /*
       * Adapto Formatos. Se guarda en AS400 como yyyyMMdd y quieren dd/MM/yyyy
       */
      SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
      SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

      String dateFromAsString1 = objFmeg0cha.getFhdafrom().trim();
      String dateToAsString1 = objFmeg0cha.getFhdateto().trim();

      Date dateFrom = sdf1.parse(dateFromAsString1);
      Date dateTo = sdf1.parse(dateToAsString1);

      /* AML 14-06-2010 */
      /*
       * Si las fechas efectivas son anteriores a 14/06/2010 se ponen a
       * 01/01/2008
       */
      Date umbral = sdf1.parse("20100614");
      Date fechaPorDefecto = sdf1.parse("20080101");

      if (umbral.compareTo(dateFrom) > 0)
        dateFrom = fechaPorDefecto;

      if (umbral.compareTo(dateTo) > 0)
        dateTo = fechaPorDefecto;

      String dateFromAsString2 = sdf2.format(dateFrom);
      String dateToAsString2 = sdf2.format(dateTo);

      setDateFrom(dateFromAsString2);
      setDateTo(dateToAsString2);

      // ----------------------------------------------------------------------------

      tx.commit();

    }
    catch (Exception e) {

      logger.warn(e.getMessage(), e);
      setMensajeError(e.getMessage());

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    String ident = getClient_mnemonic();

    if (!"".equals(getAccount_mnemonic())) {
      ident = ident + "_" + getAccount_mnemonic();
    }

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "CLIENT DELIVERY INSTRUCTION", ident, getNombreCortoXML(), "I", "",
          "ClientDeliveryInstruction", "FI");

    }
    else {
      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());
      result = ctx.getBean(RegistroControl.class, "CLIENT DELIVERY INSTRUCTION", ident, getNombreCortoXML(), "E",
          getMensajeError(), "ClientDeliveryInstruction", "FI");
    }

    return result;

  }
}