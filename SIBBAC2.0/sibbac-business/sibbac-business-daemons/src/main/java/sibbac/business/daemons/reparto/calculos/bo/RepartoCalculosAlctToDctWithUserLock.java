package sibbac.business.daemons.reparto.calculos.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.calculos.exception.EconomicDataException;
import sibbac.business.wrappers.database.exceptions.LockUserException;
import sibbac.business.wrappers.database.model.Tmct0bokId;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class RepartoCalculosAlctToDctWithUserLock {

  private static final Logger LOG = LoggerFactory.getLogger(RepartoCalculosAlctToDctWithUserLock.class);

  @Autowired
  private Tmct0bokBo bokBo;

  @Autowired
  private Tmct0aloBo aloBo;
  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private RepartoCalculosAlcToDctBo reparto;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.NOT_SUPPORTED)
  public void repartirCalculosAlcToDct(Tmct0bokId bookId, List<RepartoCalculosAlctToDctDTO> listAlcId, String auditUser)
      throws EconomicDataException {
    boolean imLock = false;
    int pos, posEnd;
    Future<Void> future;
    RepartoCalculosAlcToDctCallable callable;

    try {
      bokBo.bloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.TRATANDOSE.getId(), auditUser);
      imLock = true;

      if (listAlcId.size() > 100) {
        ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
        tfb.setNameFormat("Thread-RepartoCalculosBooking-%d");

        ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());
        List<Future<Void>> listFutures = new ArrayList<Future<Void>>();

        pos = 0;
        posEnd = 0;
        while (pos < listAlcId.size()) {
          posEnd = Math.min(posEnd + 100, listAlcId.size());
          callable = ctx.getBean(RepartoCalculosAlcToDctCallable.class, listAlcId.subList(pos, posEnd), auditUser);
          future = executor.submit(callable);
          listFutures.add(future);
          pos = posEnd;
        }

        try {
          executor.shutdown();

          executor.awaitTermination((long) 2 * listFutures.size() + 30, TimeUnit.MINUTES);

          for (Future<Void> future2 : listFutures) {
            future2.get(10, TimeUnit.SECONDS);
          }
        }
        catch (Exception e) {
          LOG.warn(e.getMessage());
          LOG.trace(e.getMessage(), e);
          executor.shutdownNow();
        }
        finally {
          listFutures.clear();
        }
      }
      else {
        reparto.repartirCalculosAlcToDct(listAlcId, auditUser);
      }

    }
    catch (Exception e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }
    finally {
      if (imLock) {
        try {
          bokBo.desBloquearBooking(bookId, EstadosEnumerados.ASIGNACIONES.EN_CURSO.getId(), auditUser);
        }
        catch (LockUserException e) {
          LOG.warn(e.getMessage());
          LOG.trace(e.getMessage(), e);
        }
      }
    }
  }

}
