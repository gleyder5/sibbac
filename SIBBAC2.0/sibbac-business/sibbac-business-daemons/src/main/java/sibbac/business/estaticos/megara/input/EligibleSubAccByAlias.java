package sibbac.business.estaticos.megara.input;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import sibbac.business.estaticos.megara.inout.SubAccountByAliasOutPut;
import sibbac.business.estaticos.utils.conversiones.ConvertirBoolean;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0suaBo;
import sibbac.database.megbpdta.model.Fmeg0sua;
import sibbac.database.megbpdta.model.Fmeg0suaId;

@Service
@Scope(value = "prototype")
public class EligibleSubAccByAlias {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(EligibleSubAccByAlias.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0suaBo fmeg0suaBo;
  
  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private static List<String> atrData;

  private Document document;

  private Fmeg0sua objFmeg0sua;

  public EligibleSubAccByAlias(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("alias");
    atrData.add("subAccount");
    atrData.add("isDefault");

    objFmeg0sua = new Fmeg0sua();
    objFmeg0sua.setId(new Fmeg0suaId("", ""));
    objFmeg0sua.setIsdefaul("");
    objFmeg0sua.setFhdebaja("");
    objFmeg0sua.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {

      if (INPUT.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        input();
      }
      else {
        delete();
      }

      // 26/03/2012
      // PROVISIONAL

      /*
       * if ("U".equals(objFmeg0sua.getTpmodeid())) { new
       * RegistroControl("SubAccount by Alias", objFmeg0sua.getCdsubcta() + "|"
       * + objFmeg0sua.getCdaliass(), nombreXML, "W",
       * "No se procesa por ser fichero de Modificaci�n",
       * "ElegibleSubAccByAlias", "MG").write();
       * directorios.moverDirOk(nombreXML); } else
       */
      {

        /*
         * AML 22/03/2012 Se obtiene el nuctapro para la subcuenta
         */

        /*
         * final Utilities utilities = new Utilities();
         * utilities.startConnection(); utilities.createNuctaproForSubAccount(
         * objFmeg0sua.getCdaliass(), objFmeg0sua.getCdsubcta(),
         * objFmeg0sua.getTpmodeid());
         */

        // AML 28/02/2012

        // En la tabla de log (fmeg0fie) se informar� subacc|alias

        logger.debug("Archivo XML tratado correctamente");
        ctx.getBean(RegistroControl.class, "SubAccount by Alias",
            objFmeg0sua.getId().getCdsubcta() + "|" + objFmeg0sua.getId().getCdaliass(), nombreXML, "I", "Procesado fichero estatico",
            "ElegibleSubAccByAlias", "MG").write();
        directorios.moverDirOk(nombreXML);

        try {
          ctx.getBean(SubAccountByAliasOutPut.class, objFmeg0sua).procesarSalida();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
      }

    }
    catch (Exception e) {
      logger.info(e.getMessage(), e);
      ctx.getBean(RegistroControl.class, "SubAccount by Alias", objFmeg0sua.getId().getCdaliass(), nombreXML, "E",
          e.getMessage(), "ElegibleSubAccByAlias", "MG").write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void input() throws Exception {

    Fmeg0sua objFmeg0sua_pers = new Fmeg0sua();
    objFmeg0sua_pers.setId(new Fmeg0suaId());
    tratarCampos();
    Fmeg0suaId objPrimaryKey = new Fmeg0suaId(objFmeg0sua.getId().getCdaliass(), objFmeg0sua.getId().getCdsubcta());

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0sua_pers = fmeg0suaBo.findById(objPrimaryKey);
      if (objFmeg0sua_pers == null) {
        // INSERT
        objFmeg0sua.setTpmodeid(INPUT);
        objFmeg0sua = fmeg0suaBo.save(objFmeg0sua, true);

        tx.commit();
      }
      else if ("".equals(objFmeg0sua_pers.getFhdebaja().trim())) {
        // UPDATE
        fmeg0suaBo.delete(objFmeg0sua_pers);
        objFmeg0sua.setTpmodeid(UPDATE);
        objFmeg0sua = fmeg0suaBo.save(objFmeg0sua, true);

        tx.commit();
      }
      else {
        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0suaBo.delete(objFmeg0sua_pers);
        objFmeg0sua.setTpmodeid(INPUT);
        objFmeg0sua = fmeg0suaBo.save(objFmeg0sua, true);
        tx.commit();
      }

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    Fmeg0sua objFmeg0sua_pers = new Fmeg0sua();
    objFmeg0sua_pers.setId(new Fmeg0suaId());
    tratarCampos();

    Fmeg0suaId objPrimaryKey = new Fmeg0suaId(objFmeg0sua.getId().getCdaliass(), objFmeg0sua.getId().getCdsubcta());

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0sua_pers = fmeg0suaBo.findById(objPrimaryKey);
      if (objFmeg0sua_pers == null) {
        throw new Exception("EL REGISTRO NO PUEDE DARSE DE BAJA, NO EXISTE EN LA BASE DE DATOS");
      }
      else if ("".equals(objFmeg0sua_pers.getFhdebaja().trim())) {
        objFmeg0sua_pers.setFhdebaja(fechaSt);
        objFmeg0sua_pers.setTpmodeid(DELETE);
        objFmeg0sua = fmeg0suaBo.save(objFmeg0sua_pers, false);
      }
      else {
        throw new Exception("EL REGISTRO NO PUEDE DARSE DE BAJA, SE ENCUENTRA EN ESTADO DE BAJA LOGICA");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void tratarCampos() throws DOMException, Exception {

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// alias
            objFmeg0sua.getId().setCdaliass(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// subAccount
            objFmeg0sua.getId().setCdsubcta(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// isDefault
            objFmeg0sua.setIsdefaul(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));

            break;
          default:
            ;
        }
      }
    }

  }

}
