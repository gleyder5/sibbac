package sibbac.business.daemons.batch;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.utils.FormatDataUtils;

class FileDisposer implements Runnable {
  
  private static final Logger LOG = LoggerFactory.getLogger(FileDisposer.class);
  
  private final File workdirectory;
  
  private final List<File> fileList;
  
  private final String batchCode;
  
  private final boolean compress;
  
  FileDisposer(File workdirectory, List<File> fileList, String batchCode, boolean compress) {
    this.workdirectory = workdirectory;
    this.fileList = fileList;
    this.batchCode = batchCode;
    this.compress = compress;
  }

  @Override
  public void run() {
    final List<File> noEmptyFileList;
    
    try {
      if(compress) {
        noEmptyFileList = new ArrayList<>();
        for(File file : fileList) {
          if(file.length() > 0) {
            noEmptyFileList.add(file);
          }
        }
        if(!noEmptyFileList.isEmpty()) {
          LOG.debug("Se decide compactar los ficheros {} intermedios del batch {}", noEmptyFileList.size(), batchCode);
          compress(noEmptyFileList);
        }
      }
      for(File file : fileList) {
        file.delete();
      }
    }
    catch(RuntimeException rex) {
      LOG.warn("[run] Error inesperado al disponer de los ficheros intermedios del batch {}", batchCode, rex);
    }
  }

  private void compress(List<File> forCompress) {
    final File zippedFile;
    final String zippedName;
    final byte[] byteBuffer;
    ZipEntry entry;
    int readed;
    
    LOG.debug("[compress] Inicio");
    byteBuffer = new byte[1_024 * 1_024];
    zippedName = new StringBuilder(batchCode).append("_").append(FormatDataUtils.convertDateToString(new Date(),
        FormatDataUtils.DATE_FORMAT_REF_TIT_OP_ESPECIAL)).append(".zip").toString();
    zippedFile = new File(workdirectory, zippedName);
    try(ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zippedFile)))) {
      for(File file : forCompress) {
        entry = new ZipEntry(file.getName());
        zos.putNextEntry(entry);
        try(InputStream is = new BufferedInputStream(new FileInputStream(file))) {
          while((readed = is.read(byteBuffer, 0, byteBuffer.length)) > 0) {
            zos.write(byteBuffer, 0, readed);
          }
        }
        zos.closeEntry();
      }
      LOG.debug("[compress] Fin");
    }
    catch(IOException ioex) {
      LOG.warn("[compress] Inicidencia de I/O al empacar los ficheros de trabajo", ioex);
    }
    catch(RuntimeException rex) {
      LOG.error("[compress] Error inesperado al empacar ficheros de trabajo", rex);
    }
  }

}
