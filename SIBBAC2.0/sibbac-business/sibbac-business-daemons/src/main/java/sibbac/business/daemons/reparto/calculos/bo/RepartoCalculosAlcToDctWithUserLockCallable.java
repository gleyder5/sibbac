package sibbac.business.daemons.reparto.calculos.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.database.model.Tmct0bokId;

@Service
@Scope(value = "prototype")
public class RepartoCalculosAlcToDctWithUserLockCallable implements Callable<Void> {

  private Tmct0bokId bookId;
  private List<RepartoCalculosAlctToDctDTO> listAlcId;
  private String auditUser;

  @Autowired
  private RepartoCalculosAlctToDctWithUserLock reparto;

  public RepartoCalculosAlcToDctWithUserLockCallable(Tmct0bokId bookId, List<RepartoCalculosAlctToDctDTO> listAlcId, String auditUser) {
    this.bookId = bookId;
    this.listAlcId = new ArrayList<RepartoCalculosAlctToDctDTO>(listAlcId);
    this.auditUser = auditUser;
  }

  @Override
  public Void call() throws Exception {
    reparto.repartirCalculosAlcToDct(bookId, listAlcId, auditUser);
    return null;
  }

}
