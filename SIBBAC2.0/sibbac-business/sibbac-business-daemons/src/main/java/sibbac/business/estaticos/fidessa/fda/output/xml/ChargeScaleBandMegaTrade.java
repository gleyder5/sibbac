package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * ChargeScaleBand_megatrade.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class ChargeScaleBandMegaTrade extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ChargeScaleBandMegaTrade.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The scale_name. */
  private String scale_name;

  /** The band_number. */
  private String band_number;

  /** The lower_bound. */
  private String lower_bound;

  /** The upper_bound. */
  private String upper_bound;

  /** The percentage_rate. */
  private String percentage_rate;

  /** The plus_amount. */
  private String plus_amount;

  /** The calculation_method. */
  private String calculation_method;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new charge scale band_megatrade.
   */
  protected ChargeScaleBandMegaTrade() {

    this.tipoActualizacion = "";

    this.scale_name = "";

    this.band_number = "";

    this.lower_bound = "";

    this.upper_bound = "";

    this.percentage_rate = "";

    this.plus_amount = "";

    this.calculation_method = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("chargeScaleBand: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------
      Element CHARGE_SCALE_BAND = document.createElement("CHARGE_SCALE_BAND");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------
      Element SCALE_NAME = document.createElement("SCALE_NAME");
      SCALE_NAME.appendChild(document.createTextNode(getScale_name()));

      Element BAND_NUMBER = document.createElement("BAND_NUMBER");
      BAND_NUMBER.appendChild(document.createTextNode(getBand_number()));

      Element LOWER_BOUND = document.createElement("LOWER_BOUND");
      LOWER_BOUND.appendChild(document.createTextNode(getLower_bound()));

      Element UPPER_BOUND = document.createElement("UPPER_BOUND");
      UPPER_BOUND.appendChild(document.createTextNode(getUpper_bound()));

      Element PERCENTAGE_RATE = document.createElement("PERCENTAGE_RATE");
      PERCENTAGE_RATE.appendChild(document.createTextNode(getPercentage_rate()));

      Element PLUS_AMOUNT = document.createElement("PLUS_AMOUNT");
      PLUS_AMOUNT.appendChild(document.createTextNode(getPlus_amount()));

      Element CALCULATION_METHOD = document.createElement("CALCULATION_METHOD");
      CALCULATION_METHOD.appendChild(document.createTextNode(getCalculation_method()));

      Element CALCULATION_BASIS = document.createElement("CALCULATION_BASIS");

      // ---------------------------------------------------------

      data.appendChild(SCALE_NAME);
      data.appendChild(BAND_NUMBER);
      data.appendChild(LOWER_BOUND);
      data.appendChild(UPPER_BOUND);
      data.appendChild(PERCENTAGE_RATE);
      data.appendChild(PLUS_AMOUNT);
      data.appendChild(CALCULATION_METHOD);
      data.appendChild(CALCULATION_BASIS);

      // ---------------------------------------------------------

      CHARGE_SCALE_BAND.appendChild(header);
      CHARGE_SCALE_BAND.appendChild(data);

      document.appendChild(CHARGE_SCALE_BAND);

      logger.debug("chargeScaleBand: estructura documento XML completada");

      // ---------------------------------------------------------

      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("chargeScaleBand: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("chargeScaleBand: error en la generacion del docuemento XML");
      logger.warn("chargeScaleBand: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a charge scale band";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a charge scale band";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a charge scale band";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("chargeScaleBand: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, KBMMdd0000.xml donde
   * KB es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getChargeScaleBand();
    Path path = Paths.get(getRutaDestino(), String.format("KB%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("ChargeScaleBand: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the band_number.
   * 
   * @return the band_number
   */
  protected String getBand_number() {
    return band_number;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the band_number.
   * 
   * @param band_number the new band_number
   */
  protected void setBand_number(String band_number) {
    this.band_number = band_number;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the calculation_method.
   * 
   * @return the calculation_method
   */
  protected String getCalculation_method() {
    return calculation_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the calculation_method.
   * 
   * @param calculation_method the new calculation_method
   */
  protected void setCalculation_method(String calculation_method) {
    this.calculation_method = calculation_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the lower_bound.
   * 
   * @return the lower_bound
   */
  protected String getLower_bound() {
    return lower_bound;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the lower_bound.
   * 
   * @param lower_bound the new lower_bound
   */
  protected void setLower_bound(String lower_bound) {
    this.lower_bound = lower_bound;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the percentage_rate.
   * 
   * @return the percentage_rate
   */
  protected String getPercentage_rate() {
    return percentage_rate;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the percentage_rate.
   * 
   * @param percentage_rate the new percentage_rate
   */
  protected void setPercentage_rate(String percentage_rate) {
    this.percentage_rate = percentage_rate;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the plus_amount.
   * 
   * @return the plus_amount
   */
  protected String getPlus_amount() {
    return plus_amount;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the plus_amount.
   * 
   * @param plus_amount the new plus_amount
   */
  protected void setPlus_amount(String plus_amount) {
    this.plus_amount = plus_amount;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the scale_name.
   * 
   * @return the scale_name
   */
  protected String getScale_name() {
    return scale_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the scale_name.
   * 
   * @param scale_name the new scale_name
   */
  protected void setScale_name(String scale_name) {
    this.scale_name = scale_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the upper_bound.
   * 
   * @return the upper_bound
   */
  protected String getUpper_bound() {
    return upper_bound;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the upper_bound.
   * 
   * @param upper_bound the new upper_bound
   */
  protected void setUpper_bound(String upper_bound) {
    this.upper_bound = upper_bound;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}
