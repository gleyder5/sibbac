package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ChargeScaleMegaTrade;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.bo.Fmeg0comBo;
import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0comId;
import sibbac.database.megbpdta.model.Fmeg0ctr;

@Service
@Scope(value = "prototype")
public class ComisionesChargeScale extends ChargeScaleMegaTrade {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ComisionesChargeScale.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0comBo fmeg0comBo;

  private Fmeg0ctr objFmeg0ctr;

  private Fmeg0com objFmeg0com;

  private String CHARGE_SCALE_NAME;

  public ComisionesChargeScale(Fmeg0ctr objFmeg0ctr, String charge_scale_name) {

    super();

    this.objFmeg0ctr = objFmeg0ctr;

    this.CHARGE_SCALE_NAME = charge_scale_name;

  }

  public boolean execXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    generarXML();

    RegistroControl objRegistroControl = generarRegistroControl();

    objRegistroControl.write();

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      Fmeg0comId pkFmeg0com = new Fmeg0comId(objFmeg0ctr.getId().getCdtpcomi(), objFmeg0ctr.getId().getCdcomisi(),
          objFmeg0ctr.getId().getCdtarifa());

      this.objFmeg0com = fmeg0comBo.findById(pkFmeg0com);

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0com.getTpmodeid().trim());

      // ----------------------------------------------------------------------------

      setScale_name(CHARGE_SCALE_NAME);

      // ----------------------------------------------------------------------------

      if ("1".equals(objFmeg0com.getCdunidad().trim())) {
        setCalculation_method("PCT");
      }
      else {
        if ("3".equals(objFmeg0com.getCdunidad().trim())) {
          setCalculation_method("BP");
        }
        else {
          if ("2".equals(objFmeg0com.getCdunidad().trim()) || "4".equals(objFmeg0com.getCdunidad().trim())) {
            setCalculation_method("PER");
          }
          else {
            if ("".equals(objFmeg0com.getCdunidad().trim())) {
              setCalculation_method("FLAT");
            }
          }
        }
      }

      // ----------------------------------------------------------------------------

      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
      setMensajeError(e.getMessage());

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "CHARGE SCALE", getScale_name(), getNombreCortoXML(), "I", "",
          "ChargeScale", "FI");

    }
    else {
      result = ctx.getBean(RegistroControl.class, "CHARGE SCALE", getScale_name(), getNombreCortoXML(), "E",
          getMensajeError(), "ChargeScale", "FI");
    }

    return result;

  }

}