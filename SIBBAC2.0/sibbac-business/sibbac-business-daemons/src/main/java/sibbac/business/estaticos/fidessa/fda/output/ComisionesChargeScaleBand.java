package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ChargeScaleBandMegaTrade;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.bo.Fmeg0comBo;
import sibbac.database.megbpdta.bo.Fmeg0ctrBo;
import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0comId;
import sibbac.database.megbpdta.model.Fmeg0ctr;

@Service
@Scope(value = "prototype")
public class ComisionesChargeScaleBand extends ChargeScaleBandMegaTrade {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ComisionesChargeScaleBand.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0comBo fmeg0comBo;

  @Autowired
  private Fmeg0ctrBo fmeg0ctrBo;

  private Fmeg0ctr objFmeg0ctr;

  private Fmeg0com objFmeg0com;

  private String CHARGE_SCALE_NAME;

  private int contador;

  public ComisionesChargeScaleBand(Fmeg0ctr objFmeg0ctr, String charge_scale_name, int contador) {

    super();

    this.objFmeg0ctr = objFmeg0ctr;

    this.CHARGE_SCALE_NAME = charge_scale_name;

    this.contador = contador;

  }

  public boolean execXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    generarXML();

    RegistroControl objRegistroControl = generarRegistroControl();

    objRegistroControl.write();

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();
      // String cdtpcomi, String cdcomisi, String cdtarifa
      Fmeg0comId pkFmeg0com = new Fmeg0comId(objFmeg0ctr.getId().getCdtpcomi(), objFmeg0ctr.getId().getCdcomisi(),
          objFmeg0ctr.getId().getCdtarifa());

      this.objFmeg0com = fmeg0comBo.findById(pkFmeg0com);

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0com.getTpmodeid().trim());

      // ----------------------------------------------------------------------------

      setScale_name(CHARGE_SCALE_NAME);

      // ----------------------------------------------------------------------------

      setBand_number(String.valueOf(contador));

      // ----------------------------------------------------------------------------

      setLower_bound(objFmeg0ctr.getImlimnim().toString());

      // ----------------------------------------------------------------------------

      setUpper_bound(objFmeg0ctr.getImlimmax().toString());

      // ----------------------------------------------------------------------------

      setPercentage_rate(objFmeg0ctr.getPotarifa().toString());

      // ----------------------------------------------------------------------------

      setPlus_amount(objFmeg0ctr.getImfijonu().toString());

      // ----------------------------------------------------------------------------

      if ("1".equals(objFmeg0com.getCdunidad().trim())) {
        setCalculation_method("PCT");
      }
      else {
        if ("3".equals(objFmeg0com.getCdunidad().trim())) {
          setCalculation_method("BP");
        }
        else {
          if ("2".equals(objFmeg0com.getCdunidad().trim()) || "4".equals(objFmeg0com.getCdunidad().trim())) {
            setCalculation_method("PER");
          }
          else {
            if ("".equals(objFmeg0com.getCdunidad().trim())) {
              setCalculation_method("FLAT");
            }
          }
        }
      }

      // ----------------------------------------------------------------------------

      tx.commit();

    }
    catch (Exception e) {
      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);

    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "CHARGE SCALE BAND", getScale_name(), getNombreCortoXML(), "I", "",
          "ChargeScaleBand", "FI");
    }
    else {
      result = ctx.getBean(RegistroControl.class, "CHARGE SCALE BAND", getScale_name(), getNombreCortoXML(), "E",
          getMensajeError(), "ChargeScaleBand", "FI");
    }

    return result;

  }

}