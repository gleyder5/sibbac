package sibbac.business.daemons.batch.common;

import sibbac.common.SIBBACBusinessException;

public class BatchException extends SIBBACBusinessException {
	
	/** serial version por defecto */
	private static final long serialVersionUID = 1L;

	public BatchException(String message) {
		super(message);
	}

	public BatchException(String message, Exception cause) {
		super(message, cause);
	}

}
