package sibbac.business.daemons.reparto.calculos.bo;

import sibbac.business.wrappers.database.bo.calculos.dto.CalculosEconomicosFlagsDTO;
import sibbac.business.wrappers.database.model.Tmct0alcId;

public class RepartoCalculosAlctToDctDTO {

  private Tmct0alcId alcId;

  private CalculosEconomicosFlagsDTO flags;

  public RepartoCalculosAlctToDctDTO() {

  }

  public Tmct0alcId getAlcId() {
    return alcId;
  }

  public CalculosEconomicosFlagsDTO getFlags() {
    return flags;
  }

  public void setAlcId(Tmct0alcId alcId) {
    this.alcId = alcId;
  }

  public void setFlags(CalculosEconomicosFlagsDTO flags) {
    this.flags = flags;
  }
}
