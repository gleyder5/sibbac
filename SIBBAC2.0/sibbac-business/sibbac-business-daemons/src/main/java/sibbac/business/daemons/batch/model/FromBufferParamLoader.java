package sibbac.business.daemons.batch.model;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.io.IOException;
import java.util.ArrayList;
import java.io.Reader;
import java.io.StreamTokenizer;
import static java.io.StreamTokenizer.TT_EOF;
import static java.io.StreamTokenizer.TT_EOL;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.BatchException;

class FromBufferParamLoader implements Runnable {
  
  private static final Logger LOG = getLogger(FromBufferParamLoader.class);
  
  private static final int PUNTO_Y_COMA = 59;
  
  private static final int COMILLAS = 34;
  
  private final Estado estadoInicial;
  
  private final Estado esperandoComa;

  private final StreamTokenizer tokenizer;
  
  private final List<String> values;
  
  private final BlockingQueue<String[]> queue;
  
  private BatchException throwable;
  
  boolean endOfFile;

  FromBufferParamLoader(Reader reader) {
    queue = new ArrayBlockingQueue<>(100);
    tokenizer = new StreamTokenizer(reader);
    tokenizer.eolIsSignificant(true);
    tokenizer.quoteChar('"');
    values = new ArrayList<>();
    estadoInicial = new EstadoInicial();
    esperandoComa = new EsperandoComa();
  }
  
  @Override
  public void run() {
    String response[], message;
    Estado estado;
    
    LOG.debug("[run] Inicio de hilo de lectura");
    try {
      try {
        while(!endOfFile) {
          estado = estadoInicial;
          do {
            estado = estado.recibeLectura(tokenizer.nextToken());
          }
          while(estado != estadoInicial && !(estado instanceof EstadoFinal));
          if(values.isEmpty()) {
            queue.put(new String[0]);
            return;
          }
          response = new String[values.size()];
          values.toArray(response);
          values.clear();
          queue.put(response);
        }
      }
      catch(IOException ioex) {
        message = "Error de entrada salida en la lectura del buffer";
        LOG.error("[run] {}", message, ioex);
        throwable = new BatchException(message, ioex);
      }
      catch(RuntimeException rex) {
        message = "Error no controlado en la lectura asincrona del buffer";
        LOG.error("[run] {}", message, rex);
        throwable = new BatchException(message, rex);
      }
      catch(BatchException rex) {
        LOG.error("[run] Error en la ejecución del hilo de lectura del batch desde Buffer", rex);
        throwable = rex;
      }
      queue.put(new String[0]);
    }
    catch(InterruptedException iex) {
      LOG.error("[run] Se ha interrumpido el hilo de lectrua del batch desde Buffer", iex);
    }
    LOG.debug("[run] Fin de hilo de lectura");
  }
  
  String[] loadParams() throws BatchException {
    if(throwable != null) {
      throw throwable;
    }
    try {
      return queue.take();
    }
    catch(InterruptedException iex) {
      throw new BatchException("Interrupcion en la recuperacion de datos desde buffer", iex);
    }
  }
  
  abstract class Estado {
    abstract Estado recibeLectura(int type) throws BatchException;
  }
  
  class EstadoInicial extends Estado {
    
    @Override
    Estado recibeLectura(int type) throws BatchException {
      switch(type) {
        case TT_EOL:
          throw new BatchException("Se esperaba un valor de parametro y se ha encontrado fin de linea");
        case TT_EOF:
          return new EstadoFinal();
        case COMILLAS:
          if(tokenizer.sval != null) {
            values.add(tokenizer.sval);
            return esperandoComa;
          }
        default:
          throw new BatchException("Error de formato de fichero buffer, se esperaba valor");
      }
    }
    
  }
  
  class EsperandoComa extends Estado {
    
    final EsperandoValor esperandoValor = new EsperandoValor();
    
    @Override
    Estado recibeLectura(int type) throws BatchException {
      if(type == PUNTO_Y_COMA) {
        return esperandoValor;
      }
      throw new BatchException("Error de formato de fichero buffer, se esperaba ';'");
    }
    
  }
  
  class EsperandoValor extends Estado {
    
    @Override
    Estado recibeLectura(int type) throws BatchException {
      switch(type) {
        case COMILLAS:
          if(tokenizer.sval != null) {
            if(tokenizer.sval.equals(";")) {
              values.add(null);
              return this;
            }
            values.add(tokenizer.sval);
            return esperandoComa;
          }
          break;
        case PUNTO_Y_COMA:
          values.add(null);
          return this;
        case TT_EOL:
          return estadoInicial;
      }
      throw new BatchException("Error de formato de fichero buffer, se esperaba valor");
    }
    
  }
  
  class EstadoFinal extends Estado {
    
    EstadoFinal() throws BatchException {
      endOfFile = true;
    }
    
    @Override
    Estado recibeLectura(int type) throws BatchException {
      throw new BatchException("No se esperaban lecturas");
    }
    
  }
  
}
