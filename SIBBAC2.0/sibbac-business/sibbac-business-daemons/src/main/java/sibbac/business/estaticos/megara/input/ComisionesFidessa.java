package sibbac.business.estaticos.megara.input;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.business.wrappers.database.bo.Tcli0comBo;
import sibbac.database.bsnbpsql.tcli.model.Tcli0com;
import sibbac.database.bsnbpsql.tcli.model.Tcli0comId;

@Service
@Scope(value = "prototype")
public class ComisionesFidessa {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ComisionesFidessa.class);
  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Tcli0comBo tcli0comBo;
  
  @Autowired
  protected DirectoriosEstaticos directorios;

  private String UPDATE_HISTORIC_COMISSION_SENTENCE = "INSERT INTO SCHEMA.TCLI0HCO (CDACCION,CDCAMMED,CDCOMISN,CDDERLIQ,CDGLOBAL,CDIVAPAR,CDPRODUC,CDPROTER,CDTASPAR,CDTIPCAL,CDUSRHIS,FHENTHIS,HOENTHIS,IDEJELIQ,IMMINCOM,IMRIESCO,IMRIESUT,IMTRAMO1,IMTRAMO2,IMTRAMO3,NUCTAPRO,PCBANSIS,PCCOMIS1,PCCOMIS2,PCCOMIS3,PCCOMIS4,PCCOMSIS,PCSVBSIS,TPCONFIR,TPRIESGO) SELECT CDACCION_VALUE ,CDCAMMED,CDCOMISN,CDDERLIQ,CDGLOBAL,CDIVAPAR,CDPRODUC,CDPROTER,CDTASPAR,CDTIPCAL,CDUSRMOD,FHMODIFI,replace(char(current time), ':', ''),IDEJELIQ,IMMINCOM,IMRIESCO,IMRIESUT,IMTRAMO1,IMTRAMO2,IMTRAMO3,NUCTAPRO,PCBANSIS,PCCOMIS1,PCCOMIS2,PCCOMIS3,PCCOMIS4,PCCOMSIS,PCSVBSIS,TPCONFIR,TPRIESGO FROM SCHEMA.TCLI0COM WHERE CDPRODUC = CDPRODUC_VALUE AND NUCTAPRO = NUCTAPRO_VALUE  FETCH FIRST 1 ROW ONLY";

  private final String UPDATE = "U";

  private Document document;

  private BigDecimal nuctapro = new BigDecimal(0);
  private BigDecimal chargeValue = new BigDecimal(0);
  private BigDecimal minimunCharge = new BigDecimal(0);

  public ComisionesFidessa(Document dcmnt) {

    document = dcmnt;

  }

  public void procesarXML_In(String nombreXML) {

    try {

      if (UPDATE.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        updateCommission();
      }

      logger.debug("Archivo XML tratado correctamente");
      ctx.getBean(RegistroControl.class, "Comisiones de Fidessa", "008" + nuctapro.toString(), nombreXML, "I", "Procesado fichero estatico",
          "Comisiones de Fidessa", "FD").write();
      directorios.moverDirOk(nombreXML);

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
      ctx.getBean(RegistroControl.class, "Comisiones de Fidessa", "008" + nuctapro.toString(), nombreXML, "E",
          e.getMessage(), "Comisiones de Fidessa", "FD").write();
      directorios.moverDirError(nombreXML);
    }
    finally {

    }

  }

  private void updateCommission() throws Exception {

    for (int i = 0; i < (document.getElementsByTagName("information").getLength()); i++) {

      if (document.getElementsByTagName("information").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarComission(((Element) document.getElementsByTagName("information").item(i)));
      }

    }

  }

  private void tratarComission(Node commission) throws DOMException, Exception {

    String cdproduc = "008";

    final String updateLevel = (commission.getAttributes()).getNamedItem("UPDATE_LEVEL").getNodeValue();

    if (!"COUNTERPARTY".equals(updateLevel))
      throw new Exception(updateLevel + " NOT SUPPORTED");

    final String clientCode = (commission.getAttributes()).getNamedItem("CLIENT_CODE").getNodeValue();

    if (clientCode == null || "".equals(clientCode.trim()))
      return;
    nuctapro = new BigDecimal(clientCode.split(cdproduc)[1]);

    final String comissionCharge = (commission.getAttributes()).getNamedItem("COMISSION_CHARGE").getNodeValue();

    if (comissionCharge == null || "".equals(comissionCharge.trim()))
      return;

    String chargeValueAsString = (comissionCharge.split("\\/")[0]).split("\\_BP_")[1];

    final int indexCharge = chargeValueAsString.indexOf("pb");
    int porcentage = 100;
    if (indexCharge != -1) {
      chargeValueAsString = chargeValueAsString.substring(0, indexCharge);
      porcentage = 100;
    }

    chargeValue = new BigDecimal(
        new Double(new BigDecimal((chargeValueAsString)).doubleValue() / porcentage).toString());

    try {
      minimunCharge = new BigDecimal(comissionCharge.split("\\bp_")[1].split("\\/")[0]);
    }
    catch (Exception e) {

    }

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {

      tx.begin();
      final Tcli0comId objPrimaryKey = new Tcli0comId(cdproduc, (short) nuctapro.intValue());

      Tcli0com objTcli0com = null;
      String cdaccion = "'M'";

      final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
      final Date d = new Date();

      final BigDecimal fhenthis = new BigDecimal(sdf1.format(d));

      try {
        objTcli0com = tcli0comBo.findById(objPrimaryKey);

        // UPDATE
        // pm.deletePersistent(objTcli0comDelete);

        if (objTcli0com != null) {

          objTcli0com.setPccomis1(chargeValue);
          objTcli0com.setImmincom(minimunCharge);
          objTcli0com.setCdusrmod("Fidessa");
          objTcli0com.setFhmodifi(fhenthis);
          tcli0comBo.save(objTcli0com);

          tx.commit();
        }
        else {
          // INSERT
          objTcli0com = new Tcli0com();
          objTcli0com.setId(new Tcli0comId(cdproduc, (short) nuctapro.intValue()));
          initialize(objTcli0com);
          cdaccion = "'A'";
          objTcli0com.setPccomis1(chargeValue);
          objTcli0com.setImmincom(minimunCharge);
          objTcli0com.setCdusrmod("Fidessa");
          objTcli0com.setFhmodifi(fhenthis);
          tcli0comBo.save(objTcli0com);

          tx.commit();
        }

      }
      catch (Exception exception) {
        throw exception;
      }

      String sentence = UPDATE_HISTORIC_COMISSION_SENTENCE.replaceAll("SCHEMA", "BSNBPSQL");
      sentence = sentence.replaceAll("CDACCION_VALUE", cdaccion);
      sentence = sentence.replaceAll("CDPRODUC_VALUE", "'008'");
      sentence = sentence.replaceAll("NUCTAPRO_VALUE", "'" + nuctapro + "'");

      Query query = em.createNativeQuery(sentence);

      query.executeUpdate();

    }
    catch (Exception e) {
      throw e;
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void initialize(Tcli0com objTcli0com) {

    if (objTcli0com != null) {
      objTcli0com.setId(new Tcli0comId());
      final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
      final Date d = new Date();

      final BigDecimal fhenthis = new BigDecimal(sdf1.format(d));

      objTcli0com.setCdcammed(' ');
      objTcli0com.setCdcomisn(' ');
      objTcli0com.setCdderliq('N');
      objTcli0com.setCdglobal("");
      objTcli0com.setCdivapar(' ');
      objTcli0com.getId().setCdproduc("");
      objTcli0com.setCdproter('T');
      objTcli0com.setCdtaspar(' ');
      objTcli0com.setCdtipcal(' ');
      objTcli0com.setCdusrmod("");
      objTcli0com.setFhmodifi(fhenthis);
      objTcli0com.setImmincom(new BigDecimal(0.0));
      objTcli0com.setImriesco(new BigDecimal(0.0));
      objTcli0com.setImriesut(new BigDecimal(0.0));
      objTcli0com.setImtramo1(new BigDecimal(0.0));
      objTcli0com.setImtramo2(new BigDecimal(0.0));
      objTcli0com.setImtramo3(new BigDecimal(0.0));
      objTcli0com.getId().setNuctapro((short) 0);
      objTcli0com.setPcbansis(new BigDecimal(0.0));
      objTcli0com.setPccomis1(new BigDecimal(0.0));
      objTcli0com.setPccomis2(new BigDecimal(0.0));
      objTcli0com.setPccomis3(new BigDecimal(0.0));
      objTcli0com.setPccomis4(new BigDecimal(0.0));
      objTcli0com.setPccomsis(new BigDecimal(0.0));
      objTcli0com.setPcsvbsis(new BigDecimal(0.0));
      objTcli0com.setTpconfir(' ');
      objTcli0com.setTpriesgo("");
    }

  }

}