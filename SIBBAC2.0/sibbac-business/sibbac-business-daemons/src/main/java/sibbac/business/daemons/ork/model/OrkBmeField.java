package sibbac.business.daemons.ork.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Transient;

import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.business.daemons.ork.common.ResolvedValue;
import sibbac.common.SIBBACBusinessException;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;

/**
 * Representa un campo del fichero de salida ORK 
 * @author XI326526
 */
@Entity
@Inheritance(strategy = TABLE_PER_CLASS)
public abstract class OrkBmeField implements Serializable, Comparable<OrkBmeField> {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -2170037513478994913L;
	
	@Id
	@Column(name = "ORDER")
	private int order;
	
	@Column(name = "NAME", length = 50)
	private String name;
	
	@Column(name = "PRINT_FORMAT")
	private String printFormat;
	
	@Column(name = "REQUIRED")
	private char required;
	
	@Column(name = "ERROR_MSG")
	private String errorMsg;
	
  /**
   * Método polimórfico que recupera el valor correspondiente a este campo a partir del conjunto de valores 
   * consultados.
   * @param rv Conjunto de valores recuperados donde viene el dato.
   * @return El dato recuperado
   */
	@Transient
	public abstract Object[] getSolvedData(ResolvedValue rv);

  /**
   * @return La posición del campo en el fichero de salida
   */
	public int getOrder() {
		return order;
	}

	/**
	 * @param order Determina la posición que tiene el campmo en el fichero de salida
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * @return El formato de impresión
	 */
	public String getPrintFormat() {
		return printFormat;
	}

	/**
	 * @param format Determina el formato que se usará para insertar el valor del campo en el fichero de salida. 
	 */
	public void setPrintFormat(String format) {
		this.printFormat = format;
	}

  /**
   * @return el nombre del campo
   */
	public String getName() {
    return name;
  }

	/**
	 * @param name Determina el nombre del campo
	 */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * @return 'S' si el campo es requerido, otro valor en caso contrario.
   */
  public char getRequired() {
    return required;
  }
  
  /**
   * @param required Determina si el campo es requerido
   */
  public void setRequired(char required) {
    this.required = required;
  }
  
  /**
   * @return mensaje de error específico para el campo.
   */
  public String getErrorMsg() {
    return errorMsg;
  }

  /**
   * @param errorMsg Determina el mensaje de error para el campo.
   */
  public void setErrorMsg(String errorMsg) {
    this.errorMsg = errorMsg;
  }

  /**
   * @return verdadero en caso de que el campo required sea igual a 'S'
   */
  @Transient
  public boolean isRequired() {
    return required == 'S';
  }
  
  /**
   * Método polimórfico para tomar en cuenta los valores especificados en el objeto de configuración. Implementación vacía.
   * @param config Objeto de configuración
   * @throws SIBBACBusinessException En caso de error en subclases
   */
  @Transient
  public void loadProperties(OrkMercadosConfig config) throws SIBBACBusinessException {
  }
  
  /**
   * Representación en formato cadena del campo order.
   */
	@Override
	public String toString() {
		return Integer.toString(order);
	}

	/**
	 * Compara los campos solo tomando en cuenta el campo order.
	 */
	@Override
	public int compareTo(OrkBmeField o) {
		return Integer.compare(order, o.order);
	}

}
