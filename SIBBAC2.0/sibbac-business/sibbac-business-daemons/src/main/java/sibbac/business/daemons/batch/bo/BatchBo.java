package sibbac.business.daemons.batch.bo;

import java.util.List;
import static java.util.Objects.requireNonNull;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.dao.BatchDao;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.database.bo.AbstractBo;

@Service
public class BatchBo extends AbstractBo<Batch, String, BatchDao> {
  
  /**
   * Recupera todos los procesos Batch que tienen asociada una regla de periocidad y que están activos,
   * forzando la carga de queries y parámetros.
   * @return lista de batch, puede ser una lista vacía
   */
	@Transactional
  public List<Batch> findAllPeriodicActiveBatchs() {
    return dao.findAllPeriodicActiveBatchs();
  }
  
	/**
	 * Recupera un proceso Batch con llave primaria code siempre y cuando el batch esté activo,
	 * forzando la carga los queries y parámetros
	 * @param code Llave primaria del Batch
	 * @return Un batch activo con esa llave primaria o nulo
	 */
  @Transactional
  public Batch findActiveByCode(String code) throws BatchException {
  	requireNonNull(code, "El parametro code es requerido en findActiveByCode");
  	return dao.findActiveByCode(code);
  }
  
}
