package sibbac.business.estaticos.fidessa.fda.output.xml;

import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.estaticos.utils.sesion.ContadoresEstaticos;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;

public class ConfigEstaticosMegaraGeneracionXmls {

  protected String mensajeError;

  @Autowired
  protected ContadoresEstaticos contadores;

  @Autowired
  protected DirectoriosEstaticos directorios;

  protected String getRutaDestino() {

    String rutaDestino = "";

    if (!"".equals(mensajeError.trim())) {
      rutaDestino = directorios.getDirError();
    }
    else {
      rutaDestino = directorios.getDirFi();
    }

    return rutaDestino;
  }

}
