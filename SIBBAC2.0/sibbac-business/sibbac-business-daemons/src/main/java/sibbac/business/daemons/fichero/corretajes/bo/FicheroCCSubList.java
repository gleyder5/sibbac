package sibbac.business.daemons.fichero.corretajes.bo;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.fichero.corretajes.exceptions.EnvioFicheroCCException;
import sibbac.business.daemons.fichero.corretajes.utils.FicheroCCNameKey;
import sibbac.business.daemons.fichero.corretajes.utils.FicheroCCNameKeyMap;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.CLEARING;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.dto.EnvioCorretajeTmct0alcDataDTO;
import sibbac.business.wrappers.database.dto.fichero.corretajes.Tmct0desgloseclititEnvioCorretajeDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.pti.PTIException;
import sibbac.pti.PTIFileEnvironment;
import sibbac.pti.PTIFileType;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.common.SIBBACPTICommons;
import sibbac.pti.messages.cc.CC;
import sibbac.pti.messages.cc.R00;

@Component
public class FicheroCCSubList {

  private static final Logger LOG = LoggerFactory.getLogger(FicheroCCSubList.class);

  @Autowired
  private EntityManager em;

  /** Business object to <code>Tmct0alc</code> */
  @Autowired
  private Tmct0alcBo tmct0alcBo;

  /** Business object to <code>Tmct0desgloseclitit</code> */
  @Autowired
  private Tmct0desgloseclititBo tmct0desgloseclititBo;

  /** Business object to <code>Tmct0cfg</code> */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  /** Business for <code>FicheroCC</code> */
  @Autowired
  private FicheroCCBo ficheroCCBo;

  /** Map to contains ibans for market */
  private Map<String, String> ibans = new HashMap<String, String>();

  /** OutPut Streams for different files */
  private Map<FicheroCCNameKeyMap, ByteArrayOutputStream> outputStreams = new HashMap<FicheroCCNameKeyMap, ByteArrayOutputStream>();

  @Transactional(isolation = Isolation.READ_UNCOMMITTED,
                 propagation = Propagation.REQUIRES_NEW,
                 rollbackFor = { IOException.class, PTIMessageException.class, Exception.class
                 })
  public void execute(List<EnvioCorretajeTmct0alcDataDTO> alcs,
                      String path,
                      Map<FicheroCCNameKeyMap, FicheroCCNameKey> fileNames,
                      PTIFileEnvironment environment,
                      PTIFileType tipoInformacion,
                      String destino) throws EnvioFicheroCCException, PTIException {
    BufferedWriter bw;

    String cdmiembromkt;
    String iban;
    String ptiFormat;

    List<Tmct0desgloseclititEnvioCorretajeDTO> desgloses;
    Tmct0desgloseclititEnvioCorretajeDTO desglose;
    Integer result;

    FicheroCCNameKeyMap key;
    FicheroCCNameKey name;
    for (EnvioCorretajeTmct0alcDataDTO alc : alcs) {
      try {
        LOG.trace("execute() Procesando {}", alc);

        if (alc.getAlias() == null || alc.getAlias().trim().isEmpty()) {
          LOG.warn("execute() ## Incidencia no tenemos datos del id cliente para el alc {}", alc);
          continue;
        }

        LOG.trace("execute() Buscando los desgloses por miembro con mas titulos...");
        desgloses = tmct0desgloseclititBo.findDesglosesFicheroCorretaje(alc.getNuorden(), alc.getNbooking(),
                                                                        alc.getNucnfclt(), alc.getNucnfliq());
        if (desgloses.isEmpty()) {
          LOG.warn("execute() ## Incidencia El alc no tiene desgloses por enviar CC {}", alc);
          continue;
        }
        desglose = desgloses.get(0);
        LOG.trace("execute() ## Datos del miembro com mas titulos {}", desglose);

        cdmiembromkt = desglose.getMiembroMercado();
        LOG.trace("execute() ## El corretaje se va a enviar al miembro {}", cdmiembromkt);

        key = new FicheroCCNameKeyMap(cdmiembromkt);

        name = fileNames.get(key);
        if (name == null) {
          name = ficheroCCBo.getNewFicheroCCName(key.getMarket(), destino, tipoInformacion);
          fileNames.put(key, name);
        }

        LOG.trace("execute() ## El corretaje se va a enviar en el fichero id {}", name.getIdFicheroCC());

        iban = getIban(cdmiembromkt);
        LOG.trace("execute() ## El cobro del corretaje en la cuenta IBAN {}", iban);

        ptiFormat = crearMensajeCC(alc, desglose, cdmiembromkt, iban, name.getIdFicheroCC());

        bw = new BufferedWriter(new OutputStreamWriter(getOutputStream(key)));
        bw.write(ptiFormat);
        bw.newLine();
        bw.close();

        LOG.trace("execute() ## Marcando los desgloses con el numero de envio de fichero CC {}", name.getIdFicheroCC());
        result = tmct0desgloseclititBo.updateDatosEnvioFicheroCorretaje(alc.getNuorden(), alc.getNbooking(),
                                                                        alc.getNucnfclt(), alc.getNucnfliq(),
                                                                        cdmiembromkt, desglose.getFechaEjecucion(),
                                                                        name.getIdFicheroCC());
        LOG.trace("execute() ## Desgloses actualizados con los datos de envio cc {}", result);
        result = tmct0alcBo.updateEstadoEntregaRecepcion(alc.getNuorden(), alc.getNbooking(), alc.getNucnfclt(),
                                                         alc.getNucnfliq(), CLEARING.PDTE_ENVIAR_ANULACION_PARCIAL);

        LOG.trace("execute() ## Actualizado estado entrega a {} reg modificados {}", CLEARING.PDTE_ENVIAR_ANULACION_PARCIAL,
                  result);
      } catch (Exception e) {
        throw new EnvioFicheroCCException("execute() ## Incidencia con el alc " + alc, e);
      }

    }

    try {
      LOG.trace("execute() EntityManager flush antes de escribir en ficheros...");
      em.flush();
    } catch (Exception e) {
      throw new EnvioFicheroCCException("execute() ## Incidencia haciendo flush con los alcs " + alcs, e);
    }

    writeFiles(path, fileNames, environment, tipoInformacion);

    LOG.trace("execute() Limpiando output streams.");
    outputStreams.clear();
  }

  @Transactional
  private String crearMensajeCC(EnvioCorretajeTmct0alcDataDTO alc,
                                Tmct0desgloseclititEnvioCorretajeDTO desglose,
                                String cdmiembromkt,
                                String iban,
                                String idFicheroCC) throws PTIMessageException, SIBBACBusinessException {

    CC cc = (CC) PTIMessageFactory.getInstance(tmct0cfgBo.getPtiMessageVersionFromTmct0cfg(new Date()),
                                               PTIMessageType.CC);
    SIBBACPTICommons.setDestinoAndUsuarioDestino(cc, cdmiembromkt);
    R00 r00 = (R00) cc.getRecordInstance(PTIMessageRecordType.R00);
    r00.setCorretaje(alc.getCorretaje());
    r00.setDescripcionCliente(alc.getDescripcionAlias());
    r00.setEfectivo(alc.getEfectivo());
    r00.setEntidadParticipante(alc.getEntidadParticipante());
    r00.setFechaEjecucion(desglose.getFechaEjecucion());
    r00.setFechaLiquidacion(desglose.getFechaLiquidacion());
    r00.setFechaOrden(desglose.getFechaOrden());
    r00.setIban("");// FIXME QUE PONER???
    r00.setIdentificadorCliente(alc.getAlias());// FIXME QUE PONER???
    r00.setIdentificadorFichero(idFicheroCC);
    r00.setIndicadorAceptacion(" ");// FIXME QUE PONER???
    r00.setISIN(alc.getIsin());
    r00.setMiembroMercado(cdmiembromkt);
    r00.setReferenciaInformativa("");// FIXME QUE PONER
    r00.setSentido(alc.getSentido().equals("C") ? "1" : "2");
    r00.setTitulos(alc.getTitulos());
    cc.addRecord(PTIMessageRecordType.R00, r00);
    cc.setFechaDeEnvio();
    String ptiFormat = cc.toPTIFormat();
    LOG.trace("crearMensajeCC() Generando PtiMessage :\n{}\n{}", cc, ptiFormat);
    return ptiFormat;
  }

  private String getIban(String market) {
    if (ibans == null) {
      ibans = new HashMap<String, String>();
    }
    String iban = ibans.get(market);
    if (iban == null) {
      // TODO QUERY A TMCT0_CUENTA_BANCARIA
      iban = "ES5200385777180016008720";
      ibans.put(market, iban);
    }
    return iban;
  }

  private ByteArrayOutputStream getOutputStream(FicheroCCNameKeyMap market) {
    if (outputStreams == null) {
      outputStreams = new HashMap<FicheroCCNameKeyMap, ByteArrayOutputStream>();
    }
    ByteArrayOutputStream os = outputStreams.get(market);
    if (os == null) {
      os = new ByteArrayOutputStream();
      outputStreams.put(market, os);
    }
    return os;
  }

  private void writeFiles(String path,
                          Map<FicheroCCNameKeyMap, FicheroCCNameKey> fileNames,
                          PTIFileEnvironment environment,
                          PTIFileType tipoInformacion) throws EnvioFicheroCCException {
    LOG.trace("writeFiles Inicio.");
    ByteArrayOutputStream os;
    File file;
    FileOutputStream fos;
    FicheroCCNameKey fileName;
    byte[] buffer;
    for (FicheroCCNameKeyMap key : outputStreams.keySet()) {
      fileName = fileNames.get(key);
      try {
        os = outputStreams.get(key);
        buffer = os.toByteArray();
        if (buffer.length > 0) {
          file = new File(path, fileName.getFileName());
          LOG.info("writeFiles Escribiendo en fichero {}...", file.getAbsolutePath());
          fos = new FileOutputStream(file, true);
          fos.write(buffer);
          fos.close();
        }
      } catch (Exception e) {
        throw new EnvioFicheroCCException("writeFiles() ## Incidencia escribiendo en el fichero " + fileName, e);
      }
    }// fin for
    LOG.trace("writeFiles Fin.");
  }

}
