package sibbac.business.daemons.batch.model;

import sibbac.business.daemons.batch.common.BatchException;

/**
 * Implementación de la interfaz {@link Step} que ejecuta una sola query por línea
 * en su lectura de un Buffer.
 * @author XI326526
 *
 */
class SimpleStep extends BufferedStep  {
	
	private final Query query;
	
	SimpleStep(final Query query) {
		this.query = query;
	}

	@Override
  public boolean sendToMail() {
    return query.getType() == Query.TO_MAIL;
  }

  @Override
  public boolean sentToBuffer() {
    return query.getType() == Query.TO_BUFFER;
  }

  @Override
	void execute(final String values[]) throws BatchException {
		final StringBuilder sqlBuilder;
		
		sqlBuilder = new StringBuilder(query.getQuery());
    fillParameters(sqlBuilder, query.getParameters(), values);
    executeStatement(sqlBuilder.toString(), query.getType());
	}

}