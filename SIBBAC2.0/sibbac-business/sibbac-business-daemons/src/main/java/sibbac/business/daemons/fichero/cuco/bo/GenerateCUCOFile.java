package sibbac.business.daemons.fichero.cuco.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.daemons.fichero.cuco.dao.CucoDao;
import sibbac.common.file.AbstractGenerateFile;

@Service("cucoFileGenerator")
public class GenerateCUCOFile extends AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateCUCOFile.class);

  private static final String CUCO_LINE_SEPARATOR = "\n";

  private static final String CUCO_FILE_HEADER = String.format("%1$-500s",
      " FHPROCES|NUMREFER|CDORIGEN|TPOPERAC|FHEJECUC|FHLIQUID|EFEC-IRIS|CPTY|EFEC-AS4");

  /**
   * Contiene el valor del ultimo KGR del registro
   */
  private String kgr = "";

  /**
   * Contiene el valor del contador KGR del fichero
   */
  private int contKgr;

  /**
   * Contiene el valor del contador detalle del fichero
   */
  private int contDetalle;

  /**
   * DAO de Acceso a Base de Datos
   */
  @Autowired
  private CucoDao cucoDao;

  private Date currentDate;

  private List<Object[]> cucoData;

  /**
   * Reinicia los contadores
   */
  public void resetCounters() {
    this.kgr = "";
    this.contKgr = 0;
    this.contDetalle = 0;
  }

  @Override
  public String getLineSeparator() {
    return CUCO_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    if (pageable.getOffset() < cucoData.size()) {
      if (pageable.getOffset() + pageable.getPageSize() >= cucoData.size()) {
        return cucoData.subList(pageable.getOffset(), cucoData.size());
      }
      else {
        return cucoData.subList(pageable.getOffset(), pageable.getOffset() + pageable.getPageSize());
      }
    }
    else {
      return new ArrayList<>();
    }
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    Integer countData = 0;
    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza la consulta de CUCO, para la fecha '" + currentDate + "'");
    }

    long initTimeMillis = System.currentTimeMillis();
    cucoData = cucoDao.findAllListCUCO(currentDate);

    if (!cucoData.isEmpty()) {
      countData = cucoData.size();
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se termina la consulta de CUCO encontrados {} elementos en {} ms", countData,
          (System.currentTimeMillis() - initTimeMillis));
    }
    return countData;
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    writeLine(outWriter, CUCO_FILE_HEADER);

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se ha escrito la cabecera del fichero");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    for (Object[] objects : fileData) {
      if (!kgr.equals((String) objects[0])) {
        kgr = (String) objects[0];
        contKgr = contKgr + 1;
        contDetalle = 1;
      }
      else {
        contDetalle = contDetalle + 1;
      }

      final String detalle1 = (String) objects[1];
      final String contadorKGR = String.format("%03d", contKgr);
      final String contadorDetalle = String.format("%04d", contDetalle);
      final String detalle2 = (String) objects[2];

      writeLine(outWriter, detalle1 + contadorKGR + contadorDetalle + detalle2);
    }
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }

  public Date getCurrentDate() {
    return currentDate;
  }

  public void setCurrentDate(Date currentDate) {
    this.currentDate = currentDate;
  }

}
