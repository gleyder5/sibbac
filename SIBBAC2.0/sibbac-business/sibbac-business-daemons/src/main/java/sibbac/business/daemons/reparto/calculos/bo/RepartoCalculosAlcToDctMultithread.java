package sibbac.business.daemons.reparto.calculos.bo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0aloBo;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.SIBBACBusinessException;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

@Service
public class RepartoCalculosAlcToDctMultithread {

  private static final Logger LOG = LoggerFactory.getLogger(RepartoCalculosAlcToDctMultithread.class);

  @Autowired
  private Tmct0aloBo aloBo;
  @Autowired
  private Tmct0alcBo alcBo;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private RepartoCalculosAlctToDctWithUserLock reparto;

  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void repartirCalculosAlcToDct(List<RepartoCalculosAlctToDctDTO> listAlcs, String auditUser)
      throws SIBBACBusinessException {
    RepartoCalculosAlcToDctWithUserLockCallable callable;
    List<Future<Void>> listFutures = new ArrayList<Future<Void>>();
    ThreadFactoryBuilder tfb = new ThreadFactoryBuilder();
    tfb.setNameFormat("Thread-RepartoCalculosMultithread-%d");
    ExecutorService executor = Executors.newFixedThreadPool(25, tfb.build());
    Map<Tmct0bokId, List<RepartoCalculosAlctToDctDTO>> map = this.agruparAlcsByAlo(listAlcs);

    for (Tmct0bokId key : map.keySet()) {
      callable = ctx.getBean(RepartoCalculosAlcToDctWithUserLockCallable.class, key, map.get(key), auditUser);
      listFutures.add(executor.submit(callable));
    }
    try {
      executor.shutdown();

      executor.awaitTermination((long) 5 * listFutures.size() + 4 * 60, TimeUnit.MINUTES);

      for (Future<Void> future : listFutures) {
        future.get(10, TimeUnit.SECONDS);
      }
    }
    catch (InterruptedException | ExecutionException | TimeoutException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
      executor.shutdownNow();
    }
    finally {
      listFutures.clear();
      map.clear();
    }

  }

  private Map<Tmct0bokId, List<RepartoCalculosAlctToDctDTO>> agruparAlcsByAlo(List<RepartoCalculosAlctToDctDTO> listAlcs) {
    Tmct0bokId key;
    Map<Tmct0bokId, List<RepartoCalculosAlctToDctDTO>> map = new HashMap<Tmct0bokId, List<RepartoCalculosAlctToDctDTO>>();
    for (RepartoCalculosAlctToDctDTO id : listAlcs) {
      key = new Tmct0bokId(id.getAlcId().getNbooking(), id.getAlcId().getNuorden());
      if (map.get(key) == null) {
        map.put(key, new ArrayList<RepartoCalculosAlctToDctDTO>());
      }

      map.get(key).add(id);
    }
    return map;
  }

}
