/**
 * 
 */
package sibbac.business.daemons.tasks;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.estaticos.EstaticosMegaraInputBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

/**
 * @author xIS16630
 *
 */
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_ESTATICOS_MEGARA_INPUT, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 * 7-23 ? * *")
public class TaskEstaticosMegaraInput extends WrapperTaskConcurrencyPrevent {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskEstaticosMegaraInput.class);

  @Autowired
  private EstaticosMegaraInputBo estaticosMegaraInputBo;

  @Override
  public void executeTask() throws Exception {
    LOG.info("[TaskEstaticosMegaraInput] inicio.");
    estaticosMegaraInputBo.execute();
    LOG.info("[TaskEstaticosMegaraInput] fin.");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_PROCESAMIENTO_FICHEROS_ESTATICOS;
  }

}