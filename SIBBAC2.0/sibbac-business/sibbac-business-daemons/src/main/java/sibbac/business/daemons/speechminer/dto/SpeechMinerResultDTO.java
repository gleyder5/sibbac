package sibbac.business.daemons.speechminer.dto;

public class SpeechMinerResultDTO {


    private String cdbrocli;
    private String descli;
    private String cdaliass;
    private String descrali;
    private Character tpidenti;
    private String cif;
    private String cdcat;
    private String cdkgr;
    private String cdbdp;
    private Character tpclienn;
    private String fhfinal;
    private String cdestado;
    private Character tpfisjur;

    public SpeechMinerResultDTO() {
    }

    //new     SpeechMinerResultDTO(cli.tpfisjur, ali.id.cdbrocli,cli.descli,ali.id.cdaliass,ali.descrali,cli.cif,cli.tpidenti,cli.cdcat,cli.cdkgr,cli.cdbdp,cli.tpclienn)



    public SpeechMinerResultDTO(Character tpfisjur,String cdbrocli, String descli, String cdaliass, String descrali,  String cif,Character tpidenti, String cdcat, String cdkgr, String cdbdp, Character tpclienn) {
        this.tpfisjur= tpfisjur;
        this.cdbrocli = cdbrocli;
        this.descli = descli;
        this.cdaliass = cdaliass;
        this.descrali = descrali;
        this.tpidenti = tpidenti;
        this.cif = cif;
        this.cdcat = cdcat;
        this.cdkgr = cdkgr;
        this.cdbdp = cdbdp;
        this.tpclienn = tpclienn;
    }


    public String getCdbrocli() {
        return cdbrocli;
    }

    public void setCdbrocli(String cdbrocli) {
        this.cdbrocli = cdbrocli;
    }

    public String getDescli() {
        return descli;
    }

    public void setDescli(String descli) {
        this.descli = descli;
    }

    public String getCdaliass() {
        return cdaliass;
    }

    public void setCdaliass(String cdaliass) {
        this.cdaliass = cdaliass;
    }

    public String getDescrali() {
        return descrali;
    }

    public void setDescrali(String descrali) {
        this.descrali = descrali;
    }

    public Character getTpidenti() {
        return tpidenti;
    }

    public void setTpidenti(Character tpidenti) {
        this.tpidenti = tpidenti;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCdcat() {
        return cdcat;
    }

    public void setCdcat(String cdcat) {
        this.cdcat = cdcat;
    }

    public String getCdkgr() {
        return cdkgr;
    }

    public void setCdkgr(String cdkgr) {
        this.cdkgr = cdkgr;
    }

    public String getCdbdp() {
        return cdbdp;
    }

    public void setCdbdp(String cdbdp) {
        this.cdbdp = cdbdp;
    }

    public Character getTpclienn() {
        return tpclienn;
    }

    public void setTpclienn(Character tpclienn) {
        this.tpclienn = tpclienn;
    }

    public String getFhfinal() {
        return fhfinal;
    }

    public void setFhfinal(String fhfinal) {
        this.fhfinal = fhfinal;
    }

    public String getCdestado() {
        return cdestado;
    }

    public void setCdestado(String cdestado) {
        this.cdestado = cdestado;
    }

    public Character getTpfisjur() {
        return tpfisjur;
    }

    public void setTpfisjur(Character tpfisjur) {
        this.tpfisjur = tpfisjur;
    }

}
