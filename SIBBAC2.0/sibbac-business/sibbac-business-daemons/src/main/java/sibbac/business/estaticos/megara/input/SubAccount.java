package sibbac.business.estaticos.megara.input;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import sibbac.business.estaticos.megara.inout.SubAccountOutPut;
import sibbac.business.estaticos.utils.conversiones.ConvertirBoolean;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0sadBo;
import sibbac.database.megbpdta.bo.Fmeg0secBo;
import sibbac.database.megbpdta.bo.Fmeg0selBo;
import sibbac.database.megbpdta.bo.Fmeg0shlBo;
import sibbac.database.megbpdta.bo.Fmeg0sotBo;
import sibbac.database.megbpdta.bo.Fmeg0sucBo;
import sibbac.database.megbpdta.model.Fmeg0sad;
import sibbac.database.megbpdta.model.Fmeg0sadId;
import sibbac.database.megbpdta.model.Fmeg0sec;
import sibbac.database.megbpdta.model.Fmeg0secId;
import sibbac.database.megbpdta.model.Fmeg0sel;
import sibbac.database.megbpdta.model.Fmeg0selId;
import sibbac.database.megbpdta.model.Fmeg0shl;
import sibbac.database.megbpdta.model.Fmeg0shlId;
import sibbac.database.megbpdta.model.Fmeg0sot;
import sibbac.database.megbpdta.model.Fmeg0sotId;
import sibbac.database.megbpdta.model.Fmeg0suc;

@Service
@Scope(value = "prototype")
public class SubAccount {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(SubAccount.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0sucBo fmeg0sucBo;

  @Autowired
  private Fmeg0sotBo fmeg0sotBo;

  @Autowired
  private Fmeg0sadBo fmeg0sadBo;

  @Autowired
  private Fmeg0shlBo fmeg0shlBo;

  @Autowired
  private Fmeg0secBo fmeg0secBo;

  @Autowired
  private Fmeg0selBo fmeg0selBo;

  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";
  
  private final String RESEND = "H";

  private static List<String> atrData;

  private static List<String> atrOtherId;

  private static List<String> atrOtherAddress;

  private static List<String> atrHolder;

  private static List<String> atrEligibleCustodian;

  private static List<String> atrEligibleClearer;

  private Document document;

  private Fmeg0suc objFmeg0suc;

  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;

  public SubAccount(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<String>();
    atrData.add("identifier");
    atrData.add("name");
    atrData.add("broClient");
    atrData.add("otherIdentifier");
    atrData.add("description");

    atrOtherId = new ArrayList<String>();
    atrOtherId.add("identifier");
    atrOtherId.add("idType");
    atrOtherId.add("description");
    atrOtherId.add("documentType");

    atrOtherAddress = new ArrayList<String>();
    atrOtherAddress.add("adresseType");
    atrOtherAddress.add("contactFunction");
    atrOtherAddress.add("contactName");
    atrOtherAddress.add("purpose");
    atrOtherAddress.add("mailaddr");
    atrOtherAddress.add("zipCode");
    atrOtherAddress.add("location");
    atrOtherAddress.add("location2");
    atrOtherAddress.add("street");
    atrOtherAddress.add("streetNumber");
    atrOtherAddress.add("flat");
    atrOtherAddress.add("afaxnumber");
    atrOtherAddress.add("aemailaddress");
    atrOtherAddress.add("aswiftaddress");
    atrOtherAddress.add("aphonenumber");
    atrOtherAddress.add("aoasysaddress");
    atrOtherAddress.add("asequaladdress");
    atrOtherAddress.add("atelexnumber");
    atrOtherAddress.add("afileaddress");

    atrHolder = new ArrayList<String>();
    atrHolder.add("externalReference");
    atrHolder.add("isMain");
    atrHolder.add("participationPercentage");
    atrHolder.add("holder");
    atrHolder.add("holderType");

    atrEligibleCustodian = new ArrayList<String>();
    atrEligibleCustodian.add("settlementType");
    atrEligibleCustodian.add("mainEntityId");
    atrEligibleCustodian.add("market");
    atrEligibleCustodian.add("clearingMode");
    atrEligibleCustodian.add("custodian");
    atrEligibleCustodian.add("accountAtCustodian");

    atrEligibleClearer = new ArrayList<String>();
    atrEligibleClearer.add("settlementType");
    atrEligibleClearer.add("mainEntityId");
    atrEligibleClearer.add("market");
    atrEligibleClearer.add("clearingMode");
    atrEligibleClearer.add("clearer");
    atrEligibleClearer.add("accountAtClearer");

    objFmeg0suc = new Fmeg0suc();
    objFmeg0suc.setCdsubcta("");
    objFmeg0suc.setNbnameid("");
    objFmeg0suc.setCdbrocli("");
    objFmeg0suc.setCdotiden("");
    objFmeg0suc.setDescript("");
    objFmeg0suc.setFhdebaja("");
    objFmeg0suc.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {
      final String modeIn = document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue();

      if (INPUT.equals(modeIn)) {
        input();
      }
      else if (RESEND.equals(modeIn)) {
        load();
      }
      else {
        delete();
      }

      logger.debug("Archivo XML tratado correctamente");
      ctx.getBean(RegistroControl.class, "SubAccount", objFmeg0suc.getCdsubcta(), nombreXML, "I",
          "Procesado fichero estatico", "SubAccount", "MG").write();
      directorios.moverDirOk(nombreXML);

      try {
        ctx.getBean(SubAccountOutPut.class, objFmeg0suc).procesarSalida();
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

    }
    catch (Exception e) {
      logger.info(e.getMessage(), e);
      ctx.getBean(RegistroControl.class, "SubAccount", objFmeg0suc.getCdsubcta(), nombreXML, "E", e.getMessage(),
          "SubAccount", "MG").write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void load() throws Exception {
    tratarCampos();
    objFmeg0suc.setTpmodeid("H");
    
  }
  private void input() throws Exception {

    String objPrimaryKey;
    Fmeg0suc objFmeg0suc_pers = new Fmeg0suc();

    tratarCampos();

    objPrimaryKey = objFmeg0suc.getCdsubcta();

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();

      objFmeg0suc_pers = fmeg0sucBo.findById(objPrimaryKey);
      if (objFmeg0suc_pers == null) {
        // INSERT
        objFmeg0suc.setTpmodeid(INPUT);
        objFmeg0suc = fmeg0sucBo.save(objFmeg0suc, true);

        tratarHijos();

        tx.commit();
      }
      else if ("".equals(objFmeg0suc_pers.getFhdebaja().trim())) {
        // UPDATE
        fmeg0sucBo.delete(objFmeg0suc_pers);
        objFmeg0suc.setTpmodeid(UPDATE);
        objFmeg0suc = fmeg0sucBo.save(objFmeg0suc, true);

        tratarHijos();

        tx.commit();
      }
      else {

        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0sucBo.delete(objFmeg0suc_pers);
        objFmeg0suc.setTpmodeid(INPUT);
        objFmeg0suc = fmeg0sucBo.save(objFmeg0suc, true);

        tratarHijos();

        tx.commit();
      }

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    String objPrimaryKey;
    Fmeg0suc objFmeg0suc_pers = new Fmeg0suc();

    tratarCampos();

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    objPrimaryKey = objFmeg0suc.getCdsubcta();

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0suc_pers = fmeg0sucBo.findById(objPrimaryKey);
      if (objFmeg0suc_pers == null) {
        throw new Exception("No existe la identificacion de la entidad para darlo de baja");
      }
      else if ("".equals(objFmeg0suc_pers.getFhdebaja().trim())) {
        objFmeg0suc_pers.setFhdebaja(fechaSt);
        objFmeg0suc_pers.setTpmodeid(DELETE);
        objFmeg0suc = fmeg0sucBo.save(objFmeg0suc_pers, false);
      }
      else {
        throw new Exception("El registro en cuestion se encuentra en estado de 'BAJA LOGICA'");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void tratarCampos() throws DOMException, Exception {

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// identifier
            objFmeg0suc.setCdsubcta(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// name
            objFmeg0suc.setNbnameid(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// broClient
            objFmeg0suc.setCdbrocli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// otherIdentifier
            objFmeg0suc.setCdotiden(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 4:// description
            objFmeg0suc.setDescript(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          default:
            ;
        }
      }
    }

  }

  private void tratarHijos() throws DOMException, Exception {

    for (int i = 0; i < (document.getElementsByTagName("otherId").getLength()); i++) {

      if (document.getElementsByTagName("otherId").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarOtherId(((Element) document.getElementsByTagName("otherId").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("otherAddress").getLength()); i++) {

      if (document.getElementsByTagName("otherAddress").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarOtherAddress(((Element) document.getElementsByTagName("otherAddress").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("holder").getLength()); i++) {

      if (document.getElementsByTagName("holder").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarHolder(((Element) document.getElementsByTagName("holder").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("eligibleCustodian").getLength()); i++) {

      if (document.getElementsByTagName("eligibleCustodian").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarEligibleCustodian(((Element) document.getElementsByTagName("eligibleCustodian").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("eligibleClearer").getLength()); i++) {

      if (document.getElementsByTagName("eligibleClearer").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarEligibleClearer(((Element) document.getElementsByTagName("eligibleClearer").item(i)));
      }

    }
  }

  private void tratarOtherId(Node otherId) throws DOMException, Exception {

    Fmeg0sot objFmeg0sot = new Fmeg0sot();
    objFmeg0sot.setId(new Fmeg0sotId(objFmeg0suc.getCdsubcta(), null));
    objFmeg0sot.setCdotheid("");
    objFmeg0sot.setTpidtype("");
    objFmeg0sot.setDescrip1("");
    objFmeg0sot.setTpdocume("");

    for (int i = 0; i < otherId.getChildNodes().getLength(); i++) {
      if ((otherId.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(otherId.getChildNodes().item(i).getNodeName()))) {

        switch (atrOtherId.indexOf(((Element) otherId.getChildNodes().item(i)).getAttribute("name"))) {

          case 0:// identifier
            objFmeg0sot.setCdotheid(((Element) otherId.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// idType
            objFmeg0sot.setTpidtype((convertirMegaraAs400).execConvertir("TPIDTYPE", ((Element) otherId.getChildNodes()
                .item(i)).getAttribute("value")));
            break;
          case 2:// description
            objFmeg0sot.setDescrip1(((Element) otherId.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// documentType
            objFmeg0sot.setTpdocume((convertirMegaraAs400).execConvertir("TPDOCUME", ((Element) otherId.getChildNodes()
                .item(i)).getAttribute("value")));
            break;
          default:
            ;
        }
      }

    }

    objFmeg0sot.setFmeg0suc(objFmeg0suc);
    fmeg0sotBo.save(objFmeg0sot, true);
  }

  private void tratarOtherAddress(Node otherAddress) throws DOMException, Exception {

    Fmeg0sad objFmeg0sad = new Fmeg0sad();
    objFmeg0sad.setId(new Fmeg0sadId(objFmeg0suc.getCdsubcta(), null));
    objFmeg0sad.setTpaddres("");
    objFmeg0sad.setContfunc("");
    objFmeg0sad.setContname("");
    objFmeg0sad.setPurposee("");
    objFmeg0sad.setMailaddr("");
    objFmeg0sad.setCdzipcod("");
    objFmeg0sad.setNblocat1("");
    objFmeg0sad.setNblocat2("");
    objFmeg0sad.setNbstreet("");
    objFmeg0sad.setNustreet("");
    objFmeg0sad.setNuflattt("");
    objFmeg0sad.setNufaxnum("");
    objFmeg0sad.setEmailadd("");
    objFmeg0sad.setSwiftadd("");
    objFmeg0sad.setNuphonee("");
    objFmeg0sad.setIdoasysa("");
    objFmeg0sad.setIdsequal("");
    objFmeg0sad.setNutelexx("");
    objFmeg0sad.setNofilead("");
    objFmeg0sad.setCdindadd("");

    for (int i = 0; i < otherAddress.getChildNodes().getLength(); i++) {
      if ((otherAddress.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(otherAddress.getChildNodes().item(i).getNodeName()))) {

        switch (atrOtherAddress.indexOf(((Element) otherAddress.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// adresseType

            objFmeg0sad.setTpaddres((convertirMegaraAs400).execConvertir("TPADDRES", ((Element) otherAddress
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 1:// contactFunction
            objFmeg0sad.setContfunc(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// contactName
            objFmeg0sad.setContname(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// purpose
            objFmeg0sad.setPurposee(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 4:// mailaddr
            objFmeg0sad.setMailaddr(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 5:// zipCode
            objFmeg0sad.setCdzipcod(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 6:// location
            objFmeg0sad.setNblocat1(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 7:// location2
            objFmeg0sad.setNblocat2(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 8:// street
            objFmeg0sad.setNbstreet(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 9:// streetNumber
            objFmeg0sad.setNustreet(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 10:// flat
            objFmeg0sad.setNuflattt(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 11:// afaxnumber
            objFmeg0sad.setNufaxnum(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 12:// aemailaddress
            objFmeg0sad.setEmailadd(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 13:// aswiftaddress
            objFmeg0sad.setSwiftadd(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 14:// aphonenumber
            objFmeg0sad.setNuphonee(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 15:// aoasysaddress
            objFmeg0sad.setIdoasysa(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 16:// asequaladdress
            objFmeg0sad.setIdsequal(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 17:// atelexnumber
            objFmeg0sad.setNutelexx(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 18:// afileaddress
            objFmeg0sad.setNofilead(((Element) otherAddress.getChildNodes().item(i)).getAttribute("value"));
            break;
          default:
            ;
        }
      }

    }

    objFmeg0sad.setFmeg0suc(objFmeg0suc);
    fmeg0sadBo.save(objFmeg0sad, true);

  }

  private void tratarHolder(Node holder) throws DOMException, Exception {

    Fmeg0shl objFmeg0shl = new Fmeg0shl();
    objFmeg0shl.setId(new Fmeg0shlId(objFmeg0suc.getCdsubcta(), null));
    objFmeg0shl.setCdextref("");
    objFmeg0shl.setIsmainal("");
    objFmeg0shl.setPartperc("");
    objFmeg0shl.setCdholder("");
    objFmeg0shl.setTpholder("");

    for (int i = 0; i < holder.getChildNodes().getLength(); i++) {
      if ((holder.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(holder.getChildNodes().item(i).getNodeName()))) {

        switch (atrHolder.indexOf(((Element) holder.getChildNodes().item(i)).getAttribute("name"))) {

          case 0:// externalreference
            objFmeg0shl.setCdextref(((Element) holder.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// isMain
            objFmeg0shl.setIsmainal(new ConvertirBoolean().execConvertir(((Element) holder.getChildNodes().item(i))
                .getAttribute("value")));
            break;
          case 2:// participationPercentage
            objFmeg0shl.setPartperc(((Element) holder.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// holder
            objFmeg0shl.setCdholder(((Element) holder.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 4:// holderType

            objFmeg0shl.setTpholder((convertirMegaraAs400).execConvertir("TPHOLDER", ((Element) holder.getChildNodes()
                .item(i)).getAttribute("value")));
            break;
          default:
            ;
        }
      }

    }

    objFmeg0shl.setFmeg0suc(objFmeg0suc);
    fmeg0shlBo.save(objFmeg0shl, true);

  }

  private void tratarEligibleCustodian(Node eligibleCustodian) throws DOMException, Exception {

    Fmeg0sec objFmeg0sec = new Fmeg0sec();
    objFmeg0sec.setId(new Fmeg0secId(objFmeg0suc.getCdsubcta(), null));
    objFmeg0sec.setTpsettle("");
    objFmeg0sec.setIdentitm("");
    objFmeg0sec.setCdmarket("");
    objFmeg0sec.setCdcustod("");
    objFmeg0sec.setIdenvfid("N");

    for (int i = 0; i < eligibleCustodian.getChildNodes().getLength(); i++) {
      if ((eligibleCustodian.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(eligibleCustodian.getChildNodes().item(i).getNodeName()))) {

        switch (atrEligibleCustodian
            .indexOf(((Element) eligibleCustodian.getChildNodes().item(i)).getAttribute("name"))) {

          case 0:// settlementType

            objFmeg0sec.setTpsettle((convertirMegaraAs400).execConvertir("TPSETTLE", ((Element) eligibleCustodian
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 1:// mainEntityId
            objFmeg0sec.setIdentitm(((Element) eligibleCustodian.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// market
            objFmeg0sec.setCdmarket(((Element) eligibleCustodian.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// clearingMode

            break;
          case 4:// custodian
            objFmeg0sec.setCdcustod(((Element) eligibleCustodian.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 5:// accountAtCustodian

            break;
          default:
            ;
        }
      }

    }

    objFmeg0sec.setFmeg0suc(objFmeg0suc);
    fmeg0secBo.save(objFmeg0sec, true);

  }

  private void tratarEligibleClearer(Node eligibleClearer) throws DOMException, Exception {

    Fmeg0sel objFmeg0sel = new Fmeg0sel();
    objFmeg0sel.setId(new Fmeg0selId(objFmeg0suc.getCdsubcta(), null));
    objFmeg0sel.setTpsettle("");
    objFmeg0sel.setIdentitm("");
    objFmeg0sel.setCdmarket("");
    objFmeg0sel.setTpclemod("");
    objFmeg0sel.setCdcleare("");
    objFmeg0sel.setAccatcle("");
    objFmeg0sel.setIdenvfid("N");

    for (int i = 0; i < eligibleClearer.getChildNodes().getLength(); i++) {
      if ((eligibleClearer.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(eligibleClearer.getChildNodes().item(i).getNodeName()))) {

        switch (atrEligibleClearer.indexOf(((Element) eligibleClearer.getChildNodes().item(i)).getAttribute("name"))) {

          case 0:// settlementType
            objFmeg0sel.setTpsettle((convertirMegaraAs400).execConvertir("TPSETTLE", ((Element) eligibleClearer
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 1:// mainEntityId
            objFmeg0sel.setIdentitm(((Element) eligibleClearer.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// market
            objFmeg0sel.setCdmarket(((Element) eligibleClearer.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// clearingMode
            objFmeg0sel.setTpclemod((convertirMegaraAs400).execConvertir("TPCLEMOD", ((Element) eligibleClearer
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 4:// clearer
            objFmeg0sel.setCdcleare(((Element) eligibleClearer.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 5:// accountAtClearer
            objFmeg0sel.setAccatcle(((Element) eligibleClearer.getChildNodes().item(i)).getAttribute("value"));
            break;
          default:
            ;
        }
      }
    }

    objFmeg0sel.setFmeg0suc(objFmeg0suc);
    fmeg0selBo.save(objFmeg0sel, true);
  }

}