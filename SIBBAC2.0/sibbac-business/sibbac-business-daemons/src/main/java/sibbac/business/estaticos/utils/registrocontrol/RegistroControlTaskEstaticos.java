package sibbac.business.estaticos.utils.registrocontrol;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RegistroControlTaskEstaticos {

  private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(RegistroControlTaskEstaticos.class);

  public void generarRegistroControl(RegistroControl control, String fileName, String nomensa, String estafich) {
    try {
      control.loadDataFromNomxml(fileName);
      control.setEstafich(estafich);
      control.setNomensa(nomensa);
      control.writeNewTransaction();
    }
    catch (Exception e) {
      LOG.debug("[generarRegistroControl] no se ha podido escribir el registro de control del envio: {}-{}-{}",
          fileName, nomensa, e.getMessage());
      try {
        control.setNomxml(fileName);
        control.setTpficher("");
        control.setNominter("");
        control.setCdindeti("");
        control.setNompgm("");
        control.setEstafich(estafich);
        control.setNomensa(nomensa);
        control.writeNewTransaction();
      }
      catch (Exception e1) {
        LOG.warn("[generarRegistroControl] no se ha podido escribir el registro de control del envio: {}-{}", fileName,
            nomensa, e1);
      }
    }
  }

}
