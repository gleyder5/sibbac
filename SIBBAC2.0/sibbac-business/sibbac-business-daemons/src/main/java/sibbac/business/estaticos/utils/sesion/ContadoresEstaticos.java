package sibbac.business.estaticos.utils.sesion;

import java.math.BigInteger;
import java.text.MessageFormat;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class ContadoresEstaticos {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ContadoresEstaticos.class);

  @Autowired
  private EntityManagerFactory emf;

  @Value("${estaticos.sequence.next.value.sql.sentence:}")
  private String nextValSentence;

  /**
   * Gets the client groups.
   * 
   * @return the client groups
   */
  public String getClientGroups() {
    return this.getNewSequenceAs36("ClientGroups");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the clients.
   * 
   * @return the clients
   */
  public String getClients() {
    return this.getNewSequenceAs36("Clients");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client accounts.
   * 
   * @return the client accounts
   */
  public String getClientAccounts() {
    return this.getNewSequenceAs36("ClientAccounts");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client confirmations rules.
   * 
   * @return the client confirmations rules
   */
  public String getClientConfirmationsRules() {
    return this.getNewSequenceAs36("ClientConfirmationsRules");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general our sett map.
   * 
   * @return the general our sett map
   */
  public String getGeneralOurSettMap() {
    return this.getNewSequenceAs36("GeneralOurSettMap");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the specific our sett map.
   * 
   * @return the specific our sett map
   */
  public String getSpecificOurSettMap() {
    return this.getNewSequenceAs36("SpecificOurSettMap");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general your sett map.
   * 
   * @return the general your sett map
   */
  public String getGeneralYourSettMap() {
    return this.getNewSequenceAs36("GeneralYourSettMap");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the specific your sett map.
   * 
   * @return the specific your sett map
   */
  public String getSpecificYourSettMap() {
    return this.getNewSequenceAs36("SpecificYourSettMap");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general delivery instructions.
   * 
   * @return the general delivery instructions
   */
  public String getGeneralDeliveryInstructions() {
    return this.getNewSequenceAs36("GeneralDeliveryInstructions");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general agents.
   * 
   * @return the general agents
   */
  public String getGeneralAgents() {
    return this.getNewSequenceAs36("GeneralAgents");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client delivery instructions.
   * 
   * @return the client delivery instructions
   */
  public String getClientDeliveryInstructions() {
    return this.getNewSequenceAs36("ClientDeliveryInstructions");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the charge scale.
   * 
   * @return the charge scale
   */
  public String getChargeScale() {
    return this.getNewSequenceAs36("ChargeScale");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the charge scale band.
   * 
   * @return the charge scale band
   */
  public String getChargeScaleBand() {
    return this.getNewSequenceAs36("ChargeScaleBand");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the charge rules.
   * 
   * @return the charge rules
   */
  public String getChargeRules() {
    return this.getNewSequenceAs36("ChargeRules");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the market counterparties.
   * 
   * @return the market counterparties
   */
  public String getMarketCounterparties() {
    return this.getNewSequenceAs36("MarketCounterparties");

  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client groups um.
   * 
   * @return the client groups um
   */
  public String getClientGroupsUM() {
    return this.getNewSequenceAs36("ClientGroupsUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the clients um.
   * 
   * @return the clients um
   */
  public String getClientsUM() {
    return this.getNewSequenceAs36("ClientsUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client accounts um.
   * 
   * @return the client accounts um
   */
  public String getClientAccountsUM() {
    return this.getNewSequenceAs36("ClientAccountsUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client confirmations rules um.
   * 
   * @return the client confirmations rules um
   */
  public String getClientConfirmationsRulesUM() {
    return this.getNewSequenceAs36("ClientConfirmationsRulesUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general our sett map um.
   * 
   * @return the general our sett map um
   */
  public String getGeneralOurSettMapUM() {
    return this.getNewSequenceAs36("GeneralOurSettMapUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the specific our sett map um.
   * 
   * @return the specific our sett map um
   */
  public String getSpecificOurSettMapUM() {
    return this.getNewSequenceAs36("SpecificOurSettMapUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general your sett map um.
   * 
   * @return the general your sett map um
   */
  public String getGeneralYourSettMapUM() {
    return this.getNewSequenceAs36("GeneralYourSettMapUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the specific your sett map um.
   * 
   * @return the specific your sett map um
   */
  public String getSpecificYourSettMapUM() {
    return this.getNewSequenceAs36("SpecificYourSettMapUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general delivery instructions um.
   * 
   * @return the general delivery instructions um
   */
  public String getGeneralDeliveryInstructionsUM() {
    return this.getNewSequenceAs36("GeneralDeliveryInstructionsUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the general agents um.
   * 
   * @return the general agents um
   */
  public String getGeneralAgentsUM() {
    return this.getNewSequenceAs36("GeneralAgentsUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the client delivery instructions um.
   * 
   * @return the client delivery instructions um
   */
  public String getClientDeliveryInstructionsUM() {
    return this.getNewSequenceAs36("ClientDeliveryInstructionsUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the charge scale um.
   * 
   * @return the charge scale um
   */
  public String getChargeScaleUM() {
    return this.getNewSequenceAs36("ChargeScaleUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the charge scale band um.
   * 
   * @return the charge scale band um
   */
  public String getChargeScaleBandUM() {
    return this.getNewSequenceAs36("ChargeScaleBandUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the charge rules um.
   * 
   * @return the charge rules um
   */
  public String getChargeRulesUM() {
    return this.getNewSequenceAs36("ChargeRulesUM");
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the market counterparties um.
   * 
   * @return the market counterparties um
   */
  public String getMarketCounterpartiesUM() {
    return this.getNewSequenceAs36("MarketCounterpartiesUM");
  }

  private String getNewSequenceAs36(String sequenceName) {
    String seq = "";
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      Query q = em.createNativeQuery(MessageFormat.format(nextValSentence, sequenceName));
      BigInteger l = (BigInteger) q.getSingleResult();

      tx.commit();
      seq = Long.toString(l.longValue(), Character.MAX_RADIX);
      seq = StringUtils.leftPad(seq, 8, "0").toUpperCase();

      logger.trace("[getNewSequenceAsBase64] secuence = {}", seq);
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }
    return seq;
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

}
