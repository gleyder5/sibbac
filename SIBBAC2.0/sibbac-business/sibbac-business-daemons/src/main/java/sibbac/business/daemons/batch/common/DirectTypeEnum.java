package sibbac.business.daemons.batch.common;

public enum DirectTypeEnum {
	
	FROM_BUFFER, DIRECT, IN_SAME_LINE, IN_SAME_BUFFER;

}
