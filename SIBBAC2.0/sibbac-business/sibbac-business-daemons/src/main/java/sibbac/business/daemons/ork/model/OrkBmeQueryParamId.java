package sibbac.business.daemons.ork.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;

/**
 * Llave primaria para la entidad {@link OrkBmeQueryParam}
 * @author XI326526
 *
 */
@Embeddable
public class OrkBmeQueryParamId implements Serializable {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 4413572500273219676L;

  @Column(name = "NAME", length = 50)
  private String name;
  
  @Column(name = "POSITION")
  private int pos;

  /**
   * @return el nombre del parámetro ORK
   */
  public String getName() {
    return name;
  }

  /**
   * @param name determina el nombre del parámetro ORK, principalmente para identificación.
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return la posición del argumento entre los signos de interrogación
   */
  public int getPos() {
    return pos;
  }

  /**
   * @param pos Determina la posición del argumento entre los signos de interrogacion de 
   * {@link OrkBmeQuery#getSqlQuery()}
   */
  public void setPos(int pos) {
    this.pos = pos;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + pos;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    OrkBmeQueryParamId other = (OrkBmeQueryParamId) obj;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (pos != other.pos)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return String.format("{name: %s, pos: %d}", name, pos);
  }
  
}
