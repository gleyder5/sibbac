package sibbac.business.daemons.ork.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import static javax.persistence.TemporalType.DATE;
import static javax.persistence.TemporalType.TIME;

/**
 * Lleva el control del número de secuencia de los ficheros generados para BME
 */
@Entity
@Table(name = "TMCT0_ORK_BME_FICHEROS")
public class OrkBmeFicheros implements Serializable {
	
	/**
	 * Serial Version
	 */
	private static final long serialVersionUID = 7918502923557233589L;
	
	/**
	 * Crea en tiempo de ejecución una instancia persistente con los campos rellenos
	 * @param now valor para los campos fecha y hora
	 * @return Una nueva instancia
	 */
	public static OrkBmeFicheros createNow(Date now) {
		OrkBmeFicheros orkBmeFicheros;
		
		orkBmeFicheros = new OrkBmeFicheros();
		orkBmeFicheros.setFecha(now);
		orkBmeFicheros.setHora(now);
		orkBmeFicheros.setSecuencia(0);
		return orkBmeFicheros;
	}

	/**
	 * Fecha de emisión del fichero
	 */
	@Id
	@Column(name = "FECHA_FICHERO")
	@Temporal(DATE)
	private Date fecha;
	
	/**
	 * Hora de emisión del fichero
	 */
	@Column(name = "HORA_FICHERO")
	@Temporal(TIME)
	private Date hora;
	
	/**
	 * Secuencia del fichero
	 */
	@Column(name = "SECUENCIA_FICHERO")
	private int secuencia;

	/**
	 * @return fecha de creación del fichero
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha Determina la marca de la creación del fichero
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return hora de creación del fichero
	 */
	public Date getHora() {
		return hora;
	}

	/**
	 * @param hora Determina la hora de la creación del fichero.
	 */
	public void setHora(Date hora) {
		this.hora = hora;
	}

	/**
	 * @return número de secuencia que identifica y numera la generación del fichero, con un número incrementado en uno
	 * por cada día.
	 */
	public int getSecuencia() {
		return secuencia;
	}

	/**
	 * @param secuencia Determina el número de secuencia del fichero ORK generado
	 */
	public void setSecuencia(int secuencia) {
		this.secuencia = secuencia;
	}
	
	/**
	 * Actualiza los campos para indicar la creación de un nuevo fichero ORK
	 * @return número de secuencia actual
	 */
	@Transient
	public int increment() {
		hora = new Date();
		return ++secuencia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((hora == null) ? 0 : hora.hashCode());
		result = prime * result + secuencia;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrkBmeFicheros other = (OrkBmeFicheros) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (hora == null) {
			if (other.hora != null)
				return false;
		} else if (!hora.equals(other.hora))
			return false;
		if (secuencia != other.secuencia)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("{secuencia : %d}", secuencia);
	}

}
