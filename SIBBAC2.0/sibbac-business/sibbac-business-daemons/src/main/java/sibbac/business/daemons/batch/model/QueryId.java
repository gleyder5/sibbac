package sibbac.business.daemons.batch.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;

@Embeddable
public class QueryId implements Serializable {
  
  private static final long serialVersionUID = -4836312615365765990L;

  @Column(name = "CODE", length = 5)
  private String code;
  
  @Column(name = "ORDER")
  private int order;

  public String getCode() {
    return code;
  }

  public int getOrder() {
    return order;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setOrder(int order) {
    this.order = order;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((code == null) ? 0 : code.hashCode());
    result = prime * result + order;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    QueryId other = (QueryId) obj;
    if (code == null) {
      if (other.code != null)
        return false;
    }
    else if (!code.equals(other.code))
      return false;
    if (order != other.order)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    return new StringBuilder().append("{code:").append(code).append(", order: ").append(order).append("}").toString();
  }

}
