package sibbac.business.daemons.fichero.iris.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import sibbac.business.daemons.fichero.iris.dao.IrisDao;
import sibbac.common.file.AbstractGenerateFile;

@Service("irisFileGenerator")
public class GenerateIRISFile extends AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateIRISFile.class);

  private static final String IRIS_LINE_SEPARATOR = "\r\n";

  private static final String IRIS_FILE_HEADER = String.format("%1$-230s", "S00StdI#BATCHGLCS#BATCH");

  private static final String IRIS_FILE_FOOTER = String.format("%1$-230s", "S00StdI#BATCHGLCS#BATCH");

  /**
   * Contiene el valor del ultimo KGR del registro
   */
  private String kgr = "";

  /**
   * Contiene el valor del contador KGR del fichero
   */
  private int contKgr;

  /**
   * Contiene el valor del contador detalle del fichero
   */
  private int contDetalle;

  /**
   * DAO de Acceso a Base de Datos
   */
  @Autowired
  private IrisDao irisDao;

  private Date currentDate;

  private List<Object[]> irisData;

  /**
   * Reinicia los contadores
   */
  public void resetCounters() {
    this.kgr = "";
    this.contKgr = 0;
    this.contDetalle = 0;
  }

  /**
   * Obtiene el separador de linea
   */
  @Override
  public String getLineSeparator() {
    return IRIS_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    if (pageable.getOffset() < irisData.size()) {
      if (pageable.getOffset() + pageable.getPageSize() >= irisData.size()) {
        return irisData.subList(pageable.getOffset(), irisData.size());
      }
      else {
        return irisData.subList(pageable.getOffset(), pageable.getOffset() + pageable.getPageSize());
      }
    }
    else {
      return new ArrayList<>();
    }
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    Integer countData = 0;
    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza la consulta de IRIS, para la fecha '" + currentDate + "'");
    }

    long initTimeMillis = System.currentTimeMillis();
    irisData = irisDao.findAllListIRIS(currentDate);

    if (!irisData.isEmpty()) {
      countData = irisData.size();
    }

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se termina la consulta de IRIS encontrados {} elementos en {} ms", countData,
          (System.currentTimeMillis() - initTimeMillis));
    }
    return countData;
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    writeLine(outWriter, IRIS_FILE_HEADER);

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se ha escrito la cabecera del fichero");
    }
  }

  /**
   * Procesa los registros del fichero
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {

    for (Object[] objects : fileData) {
      if (!kgr.equals((String) objects[0])) {
        kgr = (String) objects[0];
        contKgr = contKgr + 1;
        contDetalle = 1;
      }
      else {
        contDetalle = contDetalle + 1;
      }

      final String detalle1 = (String) objects[1];
      final String contadorKGR = String.format("%03d", contKgr);
      final String contadorDetalle = String.format("%04d", contDetalle);
      final String detalle2 = (String) objects[2];

      final String orden = detalle1 + contadorKGR + contadorDetalle + detalle2;

      final String efectivo = (String) objects[3];
      final String nTitulos = (String) objects[4];

      writeLine(outWriter, corregirEfectivoOrdenes(orden, efectivo, nTitulos));
    }
  }

  /**
   * Calcula los decimales del campo 18. El cálculo se hace en java, es por eso
   * que la query devuelve un campo con el valor de EFECTIVO y NTITULOS
   * @param orden
   * @param efectivo
   * @param ntitulos
   * @return
   */
  private String corregirEfectivoOrdenes(String detalle, String efectivoStr, String ntitulosStr) {

    final Double efectivo = new Double(efectivoStr);
    final long ntitulos = Long.parseLong(ntitulosStr);

    final Double precioCalculado = Double.valueOf((double) Math.round(Double.valueOf(efectivo / ntitulos) * 100) / 100);

    final long parteEnteraPrecioCalculado = precioCalculado.longValue();

    final String parteDecimalPrecioCalculado = precioCalculado.toString()
        .substring(precioCalculado.toString().indexOf((char) 46) + 1);

    final String newValue = "0000000000000".substring((Long.toString(parteEnteraPrecioCalculado)).length())
        + parteEnteraPrecioCalculado + "." + parteDecimalPrecioCalculado
        + "00".substring(parteDecimalPrecioCalculado.length());

    final String newDetalle = detalle.substring(0, 139) + newValue + Character.toString((char) 28)
        + detalle.substring(156);

    if (LOG.isDebugEnabled()) {
      LOG.debug(String.format("Efectivo: %s, Titulos: %s, Precio Calculado Redondeado: %s", efectivoStr, ntitulosStr,
          newValue));
    }

    return newDetalle;
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    writeLine(outWriter, IRIS_FILE_FOOTER);

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se ha escrito el pie del fichero");
    }
  }

  public Date getCurrentDate() {
    return currentDate;
  }

  public void setCurrentDate(Date currentDate) {
    this.currentDate = currentDate;
  }

}
