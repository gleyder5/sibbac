/*
 * Cambios realizados el dia 15 de enero de 2009 para a�adir una nueva funcionalidad
 * 
 * Se condiciona la generacion del xml ChargeRules a que el valor del campo CDCLSMDO sea distinto de N
 * para ello crearemos un nuevo atributo esMercadoExtranjero donde almacenaremos la condicion de envio
 * ademas en el metodo obtenerDatos() a�adiremos el codigo necesario para gestionar este atributo y lo 
 * utilizaremos en en metodo execXML() para generar o no el xml.
 *
 * 
 * */

package sibbac.business.estaticos.fidessa.fda.output;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ChargeRulesMegaTrade;
import sibbac.business.estaticos.utils.conversiones.ConvertirAs400Fidessa;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.bo.Fmeg0comBo;
import sibbac.database.megbpdta.bo.Fmeg0ctrBo;
import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0comId;
import sibbac.database.megbpdta.model.Fmeg0ctr;

/**
 * ComisionesChargeRules.
 * 
 * @version 1.0
 * @author trenti
 * @since 15 de enero de 2009
 */
@Service
@Scope(value = "prototype")
public class ComisionesChargeRules extends ChargeRulesMegaTrade {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ComisionesChargeRules.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0comBo fmeg0comBo;

  @Autowired
  private Fmeg0ctrBo fmeg0ctrBo;

  @Autowired
  private ConvertirAs400Fidessa convertirAs400Fidessa;

  private Fmeg0com objFmeg0com;

  private String resultDesc;

  /*
   * ----------------------------------------------------------------------------
   * -- nuevo atributo para almacenar la condicion de envio.
   * --------------------
   * ----------------------------------------------------------
   */
  private boolean esMercadoExtranjero = true;

  private boolean esGastosMercado = false;

  private boolean esImpuestosMercado = false;

  private String newCharge_name = "";

  private String newCounterparty_type = "";

  private String newDescription = "";

  public ComisionesChargeRules(Fmeg0com objFmeg0com) {

    super();

    this.resultDesc = "";

    this.objFmeg0com = objFmeg0com;

  }

  public String execXML() {

    obtenerDatos();

    /*
     * --------------------------------------------------------------------------
     * ---- condicionamos la generacion del xml al estado del atributo
     * esMercadoExtranjero
     * ------------------------------------------------------
     * ------------------------
     */
    if (esMercadoExtranjero) {

      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();

      // comprobar reenvio segun nuevas condiciones
      if (esGastosMercado || esImpuestosMercado) {
        setCharge_name(newCharge_name);
        setCounterparty_type(newCounterparty_type);
        // nueva descripci�n (MCG)
        setDescription(newDescription);

        generarXML();

        RegistroControl objRegistroControl2 = generarRegistroControl();

        objRegistroControl2.write();

      }

    }

    return this.resultDesc;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    Fmeg0comId pkFmeg0com = new Fmeg0comId(objFmeg0com.getId().getCdtpcomi().trim(), objFmeg0com.getId().getCdcomisi()
        .trim(), objFmeg0com.getId().getCdtarifa().trim());

    try {
      tx.begin();

      this.objFmeg0com = fmeg0comBo.findById(pkFmeg0com);

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0com.getTpmodeid());

      // ----------------------------------------------------------------------------

      try {

        setCharge_name(convertirAs400Fidessa.execConvertir("CDTPCOMI", objFmeg0com.getId().getCdtpcomi()));

      }
      catch (Exception e) {
        if ("".equals(getMensajeError().trim())) {
          setMensajeError(e.getMessage());
          logger.warn(e.getMessage(), e);
        }
      }

      // ----------------------------------------------------------------------------

      // Alias
      if (!"".equals(objFmeg0com.getCdaliass().trim())) {

        generarCampoDescripcionAlias();

      }
      else {
        // Subcuenta
        if (!"".equals(objFmeg0com.getCdsubcta().trim())) {

          generarCampoDescripcionSubcuenta();

        }
        else {
          // Broker
          if (!"".equals(objFmeg0com.getCdbroker().trim())) {

            generarCampoDescripcionBroker();

          }
          else {
            // Gastos Mercado
            if ("M".equals(objFmeg0com.getId().getCdtpcomi().trim())) {

              generarCampoDescripcionGastosMercado();

            }
            else {
              // Impuestos
              if ("I".equals(objFmeg0com.getId().getCdtpcomi().trim())) {

                generarCampoDescripcionImpuestos();

              }
            }
          }
        }
      }

      this.resultDesc = getDescription();

      // ---------------------------------------------------------------------------

      /*
       * ------------------------------------------------------------------------
       * - codigo para gestionar el nuevo atributo que utilizaremos para decidir
       * si se realiza la generacion del xml
       * 
       * lo colocamos a esta altura justo despues de rellenar el atributo
       * resultDesc donde almacenamos la descripcion que utilizamos en la
       * generacion de otros xml.
       * 
       * generamos una excepcion para no seguir con la recuperacion de los
       * campos del xml
       * 
       * ------------------------------------------------------------------------
       * --
       */
      if ("N".equals(objFmeg0com.getCdclsmdo().trim())) {
        esMercadoExtranjero = false;
        throw new Exception("XML ChargeRules no se corresponde con un mercado extranjero");
      }

      // ----------------------------------------------------------------------------

      if ("N".equals(objFmeg0com.getCdtramos().trim())) {
        if (("0.0000").equals(objFmeg0com.getImcomfij().toString().trim())) {
          setCharge_value(objFmeg0com.getPctarifa().toString());
        }
        else {
          setCharge_value(objFmeg0com.getImcomfij().toString());
        }

      }

      // ----------------------------------------------------------------------------

      try {

        if ("N".equals(objFmeg0com.getCdtramos().trim())) {
          if (!("0.0000").equals(objFmeg0com.getPctarifa().toString())) {
            setCharge_method(convertirAs400Fidessa.execConvertir("CDUNIDAD", objFmeg0com.getCdunidad().trim()));
          }
        }
        else {
          setCharge_method("SLIDING_SCALE");
        }

      }
      catch (Exception e) {
        if ("".equals(getMensajeError().trim())) {
          logger.warn(e.getMessage(), e);
          setMensajeError(e.getMessage());
        }
      }

      // ----------------------------------------------------------------------------

      if ("2" == objFmeg0com.getCdunidad().trim()) {
        setPer_amount_value("PERTHOUSAND");
      }
      else {
        if ("4".equals(objFmeg0com.getCdunidad().trim())) {
          setPer_amount_value("PERMILLION");
        }
      }

      // ----------------------------------------------------------------------------

      if ("S".equals(objFmeg0com.getCdtramos().trim())) {
        setCharge_scale_name(resultDesc);
      }

      // ----------------------------------------------------------------------------

      setExchange(objFmeg0com.getCdmarket().trim());

      // ----------------------------------------------------------------------------

      if (("C".equals(objFmeg0com.getId().getCdtpcomi().trim()) || "E".equals(objFmeg0com.getId().getCdtpcomi().trim()))
          && "".equals(objFmeg0com.getCdsubcta().trim())) {
        setCounterparty_type("C");
      }
      else {
        if (("C".equals(objFmeg0com.getId().getCdtpcomi().trim()) || "E".equals(objFmeg0com.getId().getCdtpcomi()
            .trim()))
            && !("".equals(objFmeg0com.getCdsubcta().trim()))) {
          setCounterparty_type("S");
        }
        else {
          if ("B".equals(objFmeg0com.getId().getCdtpcomi().trim())) {
            setCounterparty_type("M");
          }
          else {
            if ("M".equals(objFmeg0com.getId().getCdtpcomi().trim())) {
              esGastosMercado = true;
              setCounterparty_type("C");
              newCounterparty_type = "M";

              try {
                newCharge_name = convertirAs400Fidessa.execConvertir("CDTPCOMI", "BM");
              }
              catch (Exception e) {
                logger.warn(e.getMessage(), e);
              }

            }
            else {
              if ("I".equals(objFmeg0com.getId().getCdtpcomi().trim())) {
                esImpuestosMercado = true;
                setCounterparty_type("C");
                newCounterparty_type = "M";
                try {
                  newCharge_name = convertirAs400Fidessa.execConvertir("CDTPCOMI", "BI");
                }
                catch (Exception e) {
                  logger.warn(e.getMessage(), e);
                }
              }
            }
          }
        }
      }

      // ----------------------------------------------------------------------------

      if ("C".equals(objFmeg0com.getId().getCdtpcomi().trim()) || "E".equals(objFmeg0com.getId().getCdtpcomi().trim())) {
        setCounterparty_mnemonic(objFmeg0com.getCdaliass().trim());
      }
      else {
        if ("B".equals(objFmeg0com.getId().getCdtpcomi().trim())) {
          setCounterparty_mnemonic(objFmeg0com.getCdbroker().trim());
        }
      }

      // ----------------------------------------------------------------------------

      if (("C".equals(objFmeg0com.getId().getCdtpcomi().trim()) || "E".equals(objFmeg0com.getId().getCdtpcomi().trim()))
          && !("".equals(objFmeg0com.getCdsubcta().trim()))) {
        setAccount_mnemonic(objFmeg0com.getCdsubcta().trim());
      }
      // ----------------------------------------------------------------------------

      setCharge_currency(objFmeg0com.getCdmoniso().trim());

      // ----------------------------------------------------------------------------

      setMinimum_charge(objFmeg0com.getImminimo().toString());

      // ----------------------------------------------------------------------------

      setMaximum_charge(objFmeg0com.getImmaximo().toString());

      // ----------------------------------------------------------------------------

      setEffective_from(objFmeg0com.getFhinivig().trim());

      // ----------------------------------------------------------------------------

      setEffective_to(objFmeg0com.getFhfinvig().trim());

      // ----------------------------------------------------------------------------

      setMarket_type(convertirAs400Fidessa.execConvertir("CDTPMERC", objFmeg0com.getCdtpmerc().trim()));

      // ----------------------------------------------------------------------------

      setInstrument_type(convertirAs400Fidessa.execConvertir("CDACTIVO", objFmeg0com.getCdactivo().trim()));

      // ----------------------------------------------------------------------------

      setFirm_buy_sell(convertirAs400Fidessa.execConvertir("CDTPOPER", objFmeg0com.getCdtpoper().trim()));

      // ----------------------------------------------------------------------------

      setIsin(objFmeg0com.getCdisinvv().trim());

      // ----------------------------------------------------------------------------

      setOrigin(convertirAs400Fidessa.execConvertir("CDORIGEN", objFmeg0com.getCdorigen().trim()));

      // ----------------------------------------------------------------------------

      setChannel(convertirAs400Fidessa.execConvertir("CDCANALL", objFmeg0com.getCdcanall().trim()));

      // ----------------------------------------------------------------------------

      setHandling_treatment(convertirAs400Fidessa.execConvertir("CDTRATOO", objFmeg0com.getCdtratoo().trim()));

      // ----------------------------------------------------------------------------

      setCommunication_method(convertirAs400Fidessa.execConvertir("TPCDVIAA", objFmeg0com.getTpcdviaa().trim()));

      // ----------------------------------------------------------------------------

      tx.commit();

    }
    catch (Exception e) {

      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  private void generarCampoDescripcionAlias() {

    try {

      String tramo1 = objFmeg0com.getCdaliass().trim();

      if ("N".equals(objFmeg0com.getCdclsmdo().trim())) {
        tramo1 = tramo1 + "_LOCAL_CTA_";
      }
      else {
        tramo1 = tramo1 + "_FOREIGN_CTA_";
      }

      String tramo2 = objFmeg0com.getCdbrocli().trim();

      String tramo3 = "_";

      if (!"".equals(objFmeg0com.getCdisinvv().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdisinvv().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmarket().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdmarket().trim() + "_";
      }

      String aux = convertirAs400Fidessa.execConvertir("CDACTIVO", objFmeg0com.getCdactivo().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDORIGEN", objFmeg0com.getCdorigen().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDTPOPER", objFmeg0com.getCdtpoper().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }
      aux = convertirAs400Fidessa.execConvertir("CDTPOPTV", objFmeg0com.getCdtpoptv().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }

      if (!"".equals(objFmeg0com.getCdtpmerc().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdtpmerc().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdcanall().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdcanall().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdtramos().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdtramos().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmoniso().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdmoniso().trim();
      }

      if ("N".equals(objFmeg0com.getCdtramos())) {

        tramo3 = tramo3 + "_" + objFmeg0com.getPctarifa().toString();

      }
      else {
        List<Fmeg0ctr> listTcli0ctr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(objFmeg0com.getId()
            .getCdtpcomi().trim(), objFmeg0com.getId().getCdcomisi().trim(), objFmeg0com.getId().getCdtarifa().trim());

        for (int i = 0; i < listTcli0ctr.size(); i++) {
          tramo3 = tramo3 + "_" + ((Fmeg0ctr) listTcli0ctr.get(i)).getPotarifa().toString();
        }
      }

      int desLong = (tramo1 + tramo2 + tramo3).length();

      if (80 >= desLong) {
        setDescription(tramo1 + tramo2 + tramo3);
      }
      else {
        if ((desLong - 80) < tramo2.length()) {
          setDescription(tramo1 + tramo2.substring(0, (tramo2.length() - (desLong - 80))) + tramo3);
        }
        else {
          setDescription((tramo1 + tramo3).substring(0, 80));
        }
      }

    }
    catch (Exception e) {
      if ("".equals(getMensajeError().trim())) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage(), e);
      }
    }

  }

  private void generarCampoDescripcionSubcuenta() {

    try {

      String tramo1 = objFmeg0com.getCdsubcta().trim();

      if ("N".equals(objFmeg0com.getCdclsmdo().trim())) {
        tramo1 = tramo1 + "_LOCAL_SUB_";
      }
      else {
        tramo1 = tramo1 + "_FOREIGN_SUB_";
      }

      tramo1 = tramo1 + objFmeg0com.getCdaliass().trim() + "_";

      String tramo2 = objFmeg0com.getCdbrocli().trim();

      String tramo3 = "_";

      if (!"".equals(objFmeg0com.getCdisinvv().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdisinvv().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmarket().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdmarket().trim() + "_";
      }

      String aux = convertirAs400Fidessa.execConvertir("CDACTIVO", objFmeg0com.getCdactivo().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDORIGEN", objFmeg0com.getCdorigen().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDTPOPER", objFmeg0com.getCdtpoper().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }
      aux = convertirAs400Fidessa.execConvertir("CDTPOPTV", objFmeg0com.getCdtpoptv().trim());
      if (!"".equals(aux)) {
        tramo3 = tramo3 + aux + "_";
      }

      if (!"".equals(objFmeg0com.getCdtpmerc().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdtpmerc().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdcanall().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdcanall().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdtramos().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdtramos().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmoniso().trim())) {
        tramo3 = tramo3 + objFmeg0com.getCdmoniso().trim();
      }

      if ("N".equals(objFmeg0com.getCdtramos())) {

        tramo3 = tramo3 + "_" + objFmeg0com.getPctarifa().toString();

      }
      else {
        List<Fmeg0ctr> listTcli0ctr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(objFmeg0com.getId()
            .getCdtpcomi().trim(), objFmeg0com.getId().getCdcomisi().trim(), objFmeg0com.getId().getCdtarifa().trim());

        for (int i = 0; i < listTcli0ctr.size(); i++) {
          tramo3 = tramo3 + "_" + ((Fmeg0ctr) listTcli0ctr.get(i)).getPotarifa().toString();
        }
      }

      int desLong = (tramo1 + tramo2 + tramo3).length();

      if (80 >= desLong) {
        setDescription(tramo1 + tramo2 + tramo3);
      }
      else {
        if ((desLong - 80) < tramo2.length()) {
          setDescription(tramo1 + tramo2.substring(0, (tramo2.length() - (desLong - 80))) + tramo3);
        }
        else {
          setDescription((tramo1 + tramo3).substring(0, 80));
        }
      }

    }
    catch (Exception e) {
      if ("".equals(getMensajeError().trim())) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage(), e);
      }
    }

  }

  private void generarCampoDescripcionBroker() {

    try {

      String descripcion = objFmeg0com.getCdbroker().trim() + "_BRK_";

      if (!"".equals(objFmeg0com.getCdisinvv().trim())) {
        descripcion = descripcion + objFmeg0com.getCdisinvv().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmarket().trim())) {
        descripcion = descripcion + objFmeg0com.getCdmarket().trim() + "_";
      }

      String aux = convertirAs400Fidessa.execConvertir("CDTPOPER", objFmeg0com.getCdtpoper().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDTRATOO", objFmeg0com.getCdtratoo().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("TPCDVIAA", objFmeg0com.getTpcdviaa().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      if (!"".equals(objFmeg0com.getCdtramos().trim())) {
        descripcion = descripcion + objFmeg0com.getCdtramos().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmoniso().trim())) {
        descripcion = descripcion + objFmeg0com.getCdmoniso().trim();
      }

      if ("N".equals(objFmeg0com.getCdtramos())) {

        descripcion = descripcion + "_" + objFmeg0com.getPctarifa().toString();

      }
      else {
        List<Fmeg0ctr> listTcli0ctr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(objFmeg0com.getId()
            .getCdtpcomi().trim(), objFmeg0com.getId().getCdcomisi().trim(), objFmeg0com.getId().getCdtarifa().trim());

        for (int i = 0; i < listTcli0ctr.size(); i++) {
          descripcion = descripcion + "_" + ((Fmeg0ctr) listTcli0ctr.get(i)).getPotarifa().toString();
        }
      }

      if (80 >= descripcion.length()) {
        setDescription(descripcion);
      }
      else {
        setDescription(descripcion.substring(0, 80));
      }

    }
    catch (Exception e) {
      if ("".equals(getMensajeError().trim())) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage(), e);
      }
    }

  }

  /* MIGUEL */

  private void generarCampoDescripcionGastosMercado() {

    try {

      String descripcion = objFmeg0com.getCdmarket().trim() + "_FESS_";

      String aux = convertirAs400Fidessa.execConvertir("CDCLSMDO", objFmeg0com.getCdclsmdo().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDTPOPER", objFmeg0com.getCdtpoper().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDRESIDE", objFmeg0com.getCdreside().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDACTIVO", objFmeg0com.getCdactivo().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      if (!"".equals(objFmeg0com.getCdtramos().trim())) {
        descripcion = descripcion + objFmeg0com.getCdtramos().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmoniso().trim())) {
        descripcion = descripcion + objFmeg0com.getCdmoniso().trim();
      }

      if ("N".equals(objFmeg0com.getCdtramos())) {

        descripcion = descripcion + "_" + objFmeg0com.getPctarifa().toString();

      }
      else {
        List<Fmeg0ctr> listTcli0ctr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(objFmeg0com.getId()
            .getCdtpcomi().trim(), objFmeg0com.getId().getCdcomisi().trim(), objFmeg0com.getId().getCdtarifa().trim());

        for (int i = 0; i < listTcli0ctr.size(); i++) {
          descripcion = descripcion + "_" + ((Fmeg0ctr) listTcli0ctr.get(i)).getPotarifa().toString();
        }
      }

      if (80 >= descripcion.length()) {
        setDescription(descripcion);

      }
      else {
        setDescription(descripcion.substring(0, 80));
      }

      newCounterparty_type = "M";

      // Nueva Descripci�n (MCG)

      newDescription = objFmeg0com.getCdmarket().trim() + "_BROK" + descripcion.substring(4);

    }
    catch (Exception e) {
      if ("".equals(getMensajeError().trim())) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage(), e);
      }
    }

  }

  private void generarCampoDescripcionImpuestos() {

    try {

      String descripcion = objFmeg0com.getCdmarket().trim() + "_TAX_";

      if (!"".equals(objFmeg0com.getCdisinvv().trim())) {
        descripcion = descripcion + objFmeg0com.getCdisinvv().trim() + "_";
      }

      String aux = convertirAs400Fidessa.execConvertir("CDTPOPER", objFmeg0com.getCdtpoper().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      aux = convertirAs400Fidessa.execConvertir("CDRESIDE", objFmeg0com.getCdreside().trim());
      if (!"".equals(aux)) {
        descripcion = descripcion + aux + "_";
      }

      if (!"".equals(objFmeg0com.getCdtramos().trim())) {
        descripcion = descripcion + objFmeg0com.getCdtramos().trim() + "_";
      }

      if (!"".equals(objFmeg0com.getCdmoniso().trim())) {
        descripcion = descripcion + objFmeg0com.getCdmoniso().trim();
      }

      if ("N".equals(objFmeg0com.getCdtramos())) {

        descripcion = descripcion + "_" + objFmeg0com.getPctarifa().toString();

      }
      else {
        List<Fmeg0ctr> listTcli0ctr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(objFmeg0com.getId()
            .getCdtpcomi().trim(), objFmeg0com.getId().getCdcomisi().trim(), objFmeg0com.getId().getCdtarifa().trim());

        for (int i = 0; i < listTcli0ctr.size(); i++) {
          descripcion = descripcion + "_" + ((Fmeg0ctr) listTcli0ctr.get(i)).getPotarifa().toString();
        }
      }

      if (80 >= descripcion.length()) {
        setDescription(descripcion);
      }
      else {
        setDescription(descripcion.substring(0, 80));
      }

      // Nueva Descripci�n (MCG)

      newDescription = objFmeg0com.getCdmarket().trim() + "_BROK" + descripcion.substring(4);

    }
    catch (Exception e) {
      if ("".equals(getMensajeError().trim())) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage(), e);
      }
    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "CHARGE RULES", getDescription(), getNombreCortoXML(), "I", "",
          "ChargeRule", "FI");
    }
    else {
      result = ctx.getBean(RegistroControl.class, "CHARGE RULES", getDescription(), getNombreCortoXML(), "E",
          getMensajeError(), "ChargeRule", "FI");
    }

    return result;

  }

}