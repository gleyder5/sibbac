package sibbac.business.daemons.fichero.corretajes.utils;

import java.io.Serializable;

public class FicheroCCNameKeyMap implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -6675166662750266536L;

  private final String market;

  public FicheroCCNameKeyMap(String market) {
    super();
    this.market = market;
  }

  public String getMarket() {
    return market;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((market == null) ? 0 : market.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FicheroCCNameKeyMap other = (FicheroCCNameKeyMap) obj;
    if (market == null) {
      if (other.market != null)
        return false;
    } else if (!market.equals(other.market))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "FicheroCCNameKeyMap [market=" + market + "]";
  }
  
  
  

}
