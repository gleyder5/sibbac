/**
 * 
 */
package sibbac.business.daemons.tasks;

import java.util.concurrent.TimeUnit;

import org.quartz.DateBuilder.IntervalUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.daemons.caseejecuciones.component.CaseEjecuciones;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * @author XI316153
 *
 */
//@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_CASE, interval = 10, intervalUnit = IntervalUnit.SECOND)
public class TaskDaemons extends SIBBACTask {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskDaemons.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  /** Business object para la tabla <code>Tmct0men</code>. */
  @Autowired
  private Tmct0menBo tmct0menBo;

  /** Business object para la operativa CASE <code>Tmct0eje</code>. */
  @Autowired
  private CaseEjecuciones caseEjecuciones;

  /*
   * @see sibbac.tasks.SIBBACTask#execute()
   */
  @Override
  public void execute() {

    final Long startTime = System.currentTimeMillis();
    LOG.debug("[execute] Iniciando la tarea de Daemons ...");

    Tmct0men tmct0men;
    try {
      LOG.debug("[execute] Bolqueando semaforo {} ...", TIPO_TAREA.TASK_DAEMONS);

      tmct0men = tmct0menBo.putEstadoMEN(TIPO_TAREA.TASK_DAEMONS, TMCT0MSC.LISTO_GENERAR, TMCT0MSC.EN_EJECUCION);

      /********************************************* CASE EJECUCIONES **********************************************/
      LOG.debug("[execute] Ejecutando el proceso de Case de Ejecuciones  ...");
      try {
        this.caseEjecuciones.execute();
      }
      catch (Exception e) {
        LOG.warn("[execute] Incidencia al ejecutar la tarea de Case de Ejecuciones ... ", e.getMessage());
      } // catch

      this.tmct0menBo.putEstadoMENAndIncrement(tmct0men, TMCT0MSC.LISTO_GENERAR);
    }
    catch (SIBBACBusinessException e) {
      LOG.warn("[execute] No se puede bloquear el semaforo {} ... " + e.getMessage(), TIPO_TAREA.TASK_DAEMONS);
    }
    finally {
      final Long endTime = System.currentTimeMillis();
      LOG.info("[execute] Finalizada la tarea Daemons ... ["
          + String.format(
              "%d min, %d sec",
              TimeUnit.MILLISECONDS.toMinutes(endTime - startTime),
              TimeUnit.MILLISECONDS.toSeconds(endTime - startTime)
                  - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(endTime - startTime))) + "]");
    } // finally
  } // execute

} // TaskEscalados
