package sibbac.business.estaticos.megara.inout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.AliasYourSettMapGeneral;
import sibbac.business.estaticos.fidessa.fda.output.AliasYourSettMapSpecific;
import sibbac.business.estaticos.fidessa.fda.output.SLASettlementChainClientDeliveryInstruction;
import sibbac.business.estaticos.fidessa.fda.output.SLASettlementChainGeneralAgents;
import sibbac.business.estaticos.fidessa.fda.output.SLASettlementChainGeneralDeliveryInstruction;
import sibbac.business.estaticos.fidessa.fda.output.SubAccountYourSettMapGeneral;
import sibbac.business.estaticos.fidessa.fda.output.SubAccountYourSettMapSpecific;
import sibbac.database.megbpdta.bo.Fmeg0aecBo;
import sibbac.database.megbpdta.bo.Fmeg0aelBo;
import sibbac.database.megbpdta.bo.Fmeg0secBo;
import sibbac.database.megbpdta.bo.Fmeg0selBo;
import sibbac.database.megbpdta.model.Fmeg0aec;
import sibbac.database.megbpdta.model.Fmeg0ael;
import sibbac.database.megbpdta.model.Fmeg0cha;
import sibbac.database.megbpdta.model.Fmeg0sec;
import sibbac.database.megbpdta.model.Fmeg0sel;

@Service
@Scope(value = "prototype")
public class InstruccionLiquidacionSLAOutPut {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InstruccionLiquidacionSLAOutPut.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0aelBo fmeg0aelBo;

  @Autowired
  private Fmeg0selBo fmeg0selBo;

  @Autowired
  private Fmeg0aecBo fmeg0aecBo;

  @Autowired
  private Fmeg0secBo fmeg0secBo;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private Fmeg0cha objFmeg0cha;

  private String tipoModificacion;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  public InstruccionLiquidacionSLAOutPut(Fmeg0cha objFmeg0cha) {

    super();

    this.objFmeg0cha = objFmeg0cha;

    this.tipoModificacion = objFmeg0cha.getTpmodeid().trim();

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  public void procesarSalidaSLA() {

    if ("".equals(objFmeg0cha.getCdaliass().trim()) && "".equals(objFmeg0cha.getCdsubcta().trim())) {

      generarInstruccionLiquidacionOUR();

    }
    else {

      generarInstruccionLiquidacionYOUR();

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarInstruccionLiquidacionOUR() {

    ctx.getBean(SLASettlementChainGeneralDeliveryInstruction.class, objFmeg0cha).procesarXML();
    ctx.getBean(SLASettlementChainGeneralAgents.class, objFmeg0cha).procesarXML();

    if ("D".equals(tipoModificacion.trim())) {
      generarOurSettMapDelete();
    }
    else {
      generarOurSettMapInsert();
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarOurSettMapInsert() {

  }

  private void generarOurSettMapDelete() {

    List<Fmeg0ael> listAel = null;
    List<Fmeg0sel> listsel = null;

    // ------------------------------------------------------------------------------

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      try {
        tx.begin();
        listAel = fmeg0aelBo.findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(objFmeg0cha.getIdcountp().trim(),
            objFmeg0cha.getTpsettle().trim(), "S");

        tx.commit();

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {

        if (tx.isActive()) {
          tx.rollback();
        }

      }

      for (int i = 0; i < listAel.size(); i++) {

        Fmeg0ael objFmeg0ael = ((Fmeg0ael) listAel.get(i));

        try {
          tx.begin();
          objFmeg0ael.setIdenvfid("N");
          fmeg0aelBo.save(objFmeg0ael, false);
          tx.commit();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
        finally {

          if (tx.isActive()) {
            tx.rollback();
          }

        }
      }

      // ------------------------------------------------------------------------------

      try {
        tx.begin();

        listsel = fmeg0selBo.findAllByIdCdcleareAndTpsettleAndIdenvfidAndTpmodeidNeqD(objFmeg0cha.getIdcountp().trim(),
            objFmeg0cha.getTpsettle().trim(), "S");

        tx.commit();

      }
      catch (Exception e) {

      }
      finally {

        if (tx.isActive()) {
          tx.rollback();
        }

      }

      for (int i = 0; i < listsel.size(); i++) {

        Fmeg0sel objFmeg0sel = ((Fmeg0sel) listsel.get(i));

        try {
          tx.begin();
          objFmeg0sel.setIdenvfid("N");
          fmeg0selBo.save(objFmeg0sel, false);
          tx.commit();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
        finally {

          if (tx.isActive()) {
            tx.rollback();
          }

        }
      }
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarInstruccionLiquidacionYOUR() {

    ctx.getBean(SLASettlementChainClientDeliveryInstruction.class, objFmeg0cha).procesarXML();

    if (!"".equals(objFmeg0cha.getCdsubcta().trim())) {

      if ("D".equals(tipoModificacion.trim())) {
        generarYourSubctaDelete();
      }
      else {
        generarYourSubctaInsert();
      }

    }
    else {
      if ("D".equals(tipoModificacion.trim())) {
        generarYourAliasDelete();
      }
      else {
        generarYourAliasInsert();
      }

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarYourAliasInsert() {

    List<Fmeg0aec> list = null;

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    // ------------------------------------------------------------------------------

    try {
      try {
        tx.begin();
        list = fmeg0aecBo.findAllByIdCdaliassCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(objFmeg0cha.getCdaliass()
            .trim(), objFmeg0cha.getIdcountp().trim(), objFmeg0cha.getTpsettle().trim(), "N");

        tx.commit();
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {

        if (tx.isActive()) {
          tx.rollback();
        }

      }

      for (int i = 0; i < list.size(); i++) {

        Fmeg0aec objFmeg0aec = list.get(i);

        if (!("".equals(objFmeg0aec.getCdmarket().trim()))) {
          ctx.getBean(AliasYourSettMapSpecific.class, objFmeg0aec, tipoModificacion).procesarXML();
        }
        else {
          ctx.getBean(AliasYourSettMapGeneral.class, objFmeg0aec, tipoModificacion).procesarXML();
        }
        try {
          tx.begin();
          objFmeg0aec.setIdenvfid("S");
          fmeg0aecBo.save(objFmeg0aec, false);
          tx.commit();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
        finally {
          if (tx.isActive()) {
            tx.rollback();
          }

        }
      }
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarYourSubctaInsert() {

    List<Fmeg0sec> list = null;

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    // ------------------------------------------------------------------------------

    try {
      try {

        tx.begin();

        list = fmeg0secBo.findAllByIdCdsubctaAndIdCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(objFmeg0cha
            .getCdsubcta().trim(), objFmeg0cha.getIdcountp().trim(), objFmeg0cha.getTpsettle().trim(), "N");

        tx.commit();
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {
        if (tx.isActive()) {
          tx.rollback();
        }

      }

      for (int i = 0; i < list.size(); i++) {

        Fmeg0sec objFmeg0sec = list.get(i);

        if (!("".equals(objFmeg0sec.getCdmarket().trim()))) {
          ctx.getBean(SubAccountYourSettMapSpecific.class, objFmeg0sec, tipoModificacion,
              objFmeg0cha.getCdaliass().trim()).procesarXML();
        }
        else {
          ctx.getBean(SubAccountYourSettMapGeneral.class, objFmeg0sec, tipoModificacion,
              objFmeg0cha.getCdaliass().trim()).procesarXML();
        }

        try {
          tx.begin();
          objFmeg0sec.setIdenvfid("S");
          fmeg0secBo.save(objFmeg0sec, false);
          tx.commit();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
        finally {

          if (tx.isActive()) {
            tx.rollback();
          }

        }
      }
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarYourAliasDelete() {

    List<Fmeg0aec> list = null;

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    // ------------------------------------------------------------------------------

    try {
      try {
        tx.begin();

        list = fmeg0aecBo.findAllByIdCdaliassCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(objFmeg0cha.getCdaliass()
            .trim(), objFmeg0cha.getIdcountp().trim(), objFmeg0cha.getTpsettle().trim(), "S");

        tx.commit();
      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {

        if (tx.isActive()) {
          tx.rollback();
        }

      }

      for (int i = 0; i < list.size(); i++) {

        Fmeg0aec objFmeg0aec = list.get(i);

        try {
          tx.begin();
          objFmeg0aec.setIdenvfid("N");
          fmeg0aecBo.save(objFmeg0aec, false);
          tx.commit();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
        finally {

          if (tx.isActive()) {
            tx.rollback();
          }

        }
      }
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private void generarYourSubctaDelete() {

    List<Fmeg0sec> list = null;

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    // ------------------------------------------------------------------------------

    try {
      try {

        tx.begin();
        list = fmeg0secBo.findAllByIdCdsubctaAndIdCdcustodAndTpsettleAndIdenvfidAndTpmodeidNeqD(objFmeg0cha
            .getCdsubcta().trim(), objFmeg0cha.getIdcountp().trim(), objFmeg0cha.getTpsettle().trim(), "S");

        tx.commit();
      }
      catch (Exception e) {

        logger.warn(e.getMessage(), e);

      }
      finally {

        if (tx.isActive()) {
          tx.rollback();
        }

      }

      for (int i = 0; i < list.size(); i++) {

        Fmeg0sec objFmeg0sec = new Fmeg0sec();
        objFmeg0sec = ((Fmeg0sec) list.get(i));

        try {
          tx.begin();
          objFmeg0sec.setIdenvfid("N");
          fmeg0secBo.save(objFmeg0sec, false);
          tx.commit();
        }
        catch (Exception e) {
          logger.warn(e.getMessage(), e);
        }
        finally {

          if (tx.isActive()) {
            tx.rollback();
          }

        }
      }
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}