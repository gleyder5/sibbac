package sibbac.business.daemons.ork.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.daemons.ork.model.OrkFidessaDynaField;
import sibbac.business.daemons.ork.model.OrkFidessaField;
import sibbac.business.daemons.ork.model.OrkFidessaFixedField;

@Repository
public interface OrkFidessaFieldDao extends JpaRepository<OrkFidessaField, String> {
	
	@Query("SELECT f FROM OrkFidessaDynaField f WHERE position > 0 ORDER BY f.position")
	List<OrkFidessaDynaField> findDynamicFields();
	
	@Query("SELECT f FROM OrkFidessaFixedField f ORDER BY f.position")
	List<OrkFidessaFixedField> findFixedFields();

}
