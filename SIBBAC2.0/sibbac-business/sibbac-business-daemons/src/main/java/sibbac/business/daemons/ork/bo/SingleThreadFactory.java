package sibbac.business.daemons.ork.bo;

import java.util.concurrent.ThreadFactory;

class SingleThreadFactory implements ThreadFactory {
	
	private final String name;
	
	SingleThreadFactory(String name) {
		this.name = name;
	}

	@Override
	public Thread newThread(Runnable runnable) {
		return new Thread(runnable, name);
	}
	
}