package sibbac.business.daemons.ork.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.daemons.ork.model.OrkBmeField;

@Repository
public interface OrkBmeFieldDao extends JpaRepository<OrkBmeField, Integer> {
	
	@Query("SELECT b FROM OrkBmeField b ORDER BY b.order")
	List<OrkBmeField> findAllFetchedParams();

}
