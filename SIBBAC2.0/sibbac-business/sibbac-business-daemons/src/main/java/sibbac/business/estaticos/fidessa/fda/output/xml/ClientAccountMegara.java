/*
 * Se modifica la clase ClientAccount para realizar una mejora solicitada por 
 * Paloma Moreno el dia 14 de noviembre de 2008
 * 
 * La mejora consiste en mover el valor del elemento Alert al elemento FUND
 * para ello se crea un nuevo atributo denominado fund asi como sus metodos de
 * obtencion y establecimiento, se inicializa el valor a "", se elimina la carga del 
 * elemento Alert y se carga el elemento FUND con su valor correspondiente.
 * 
 * */

package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * ClientAccountMegara.
 * 
 * @version 1.1
 * @author trenti
 * @since 17 de noviembre de 2008
 * 
 */
public class ClientAccountMegara extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ClientAccountMegara.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The account_mnemonic. */
  private String account_mnemonic;

  /** The account_name. */
  private String account_name;

  /** The client_mnemonic. */
  private String client_mnemonic;

  /** The confirmation_method. */
  private String confirmation_method;

  /** The oasys. */
  private String oasys;

  /** The alert. */
  private String alert;

  /* MCG 07/12/2009 */
  /** The cif_alt_type. */
  private String cif_alt_type;

  // Cambios a realizar por consulta telefonica 17-nov-08, Paloma Moreno
  /** The fund. */
  private String fund;

  /** The status_indicator. */
  private String status_indicator;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  /* AML 20/03/2012 */

  /** The Stra */
  private String stra = null;
  
  private boolean envioCamposMifid = false;
  private String mifidClientType = null;
  private String mifidClientCode = null;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

 

  /**
   * Instantiates a new client account_megara.
   */
  protected ClientAccountMegara() {

    this.tipoActualizacion = "";
    this.account_mnemonic = "";
    this.account_name = "";
    this.client_mnemonic = "";
    this.confirmation_method = "";
    this.oasys = "";
    this.alert = "";

    /* MCG 07/12/2009 */
    this.cif_alt_type = "";

    // Cambios a realizar por consulta telefonica 17-nov-08, Paloma Moreno
    this.fund = "";
    this.status_indicator = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("clientAccount: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element CLIENT_ACCOUNT = document.createElement("CLIENT_ACCOUNT");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element ACCOUNT_MNEMONIC = document.createElement("ACCOUNT_MNEMONIC");
      ACCOUNT_MNEMONIC.appendChild(document.createTextNode(getAccount_mnemonic()));

      Element ACCOUNT_NAME = document.createElement("ACCOUNT_NAME");
      ACCOUNT_NAME.appendChild(document.createTextNode(getAccount_name()));

      Element CLIENT_MNEMONIC = document.createElement("CLIENT_MNEMONIC");
      CLIENT_MNEMONIC.appendChild(document.createTextNode(getClient_mnemonic()));

      Element ACCOUNT_TYPE = document.createElement("ACCOUNT_TYPE");
      ACCOUNT_TYPE.appendChild(document.createTextNode("A"));

      Element REGISTERED_CHARITY = document.createElement("REGISTERED_CHARITY");

      Element COUNTRY_OF_DOMICILE = document.createElement("COUNTRY_OF_DOMICILE");
      COUNTRY_OF_DOMICILE.appendChild(document.createTextNode(getCountryOfDomicile()));

      Element CONFIRMATION_METHOD = document.createElement("CONFIRMATION_METHOD");
      CONFIRMATION_METHOD.appendChild(document.createTextNode(getConfirmation_method()));

      Element OASYS = document.createElement("OASYS");
      OASYS.appendChild(document.createTextNode(getOasys()));

      Element ALERT = document.createElement("ALERT");
      // Cambios a realizar por consulta telefonica 17-nov-08, Paloma
      // Moreno
      // ALERT.appendChild(document.createTextNode(getAlert()));

      Element FUND = document.createElement("FUND");
      // Cambios a realizar por consulta telefonica 17-nov-08, Paloma
      // Moreno
      // Aqu� va el c�digo del alert
      FUND.appendChild(document.createTextNode(getFund()));

      Element PERSHING = document.createElement("PERSHING");

      Element CUSTOMER_CODE_1 = document.createElement("CUSTOMER_CODE_1");

      Element CUSTOMER_CODE_2 = document.createElement("CUSTOMER_CODE_2");

      Element STATUS_INDICATOR = document.createElement("STATUS_INDICATOR");
      STATUS_INDICATOR.appendChild(document.createTextNode(getStatus_indicator()));

      Element BACK_OFFICE_CODE = document.createElement("BACK_OFFICE_CODE");

      /* MCG 07/12/2009 */
      Element CIF_ALT_TYPE = document.createElement("CIF_ALT_TYPE");
      CIF_ALT_TYPE.appendChild(document.createTextNode(getCif_alt_type()));

      /* AML 20/03/2012 */
      Element STRA = document.createElement("STRA");

      if (stra != null && !"".equals(stra))
        STRA.appendChild(document.createTextNode(stra));

      // ---------------------------------------------------------

      data.appendChild(ACCOUNT_MNEMONIC);
      data.appendChild(ACCOUNT_NAME);
      data.appendChild(CLIENT_MNEMONIC);
      data.appendChild(ACCOUNT_TYPE);
      data.appendChild(REGISTERED_CHARITY);
      data.appendChild(COUNTRY_OF_DOMICILE);
      data.appendChild(CONFIRMATION_METHOD);
      data.appendChild(OASYS);
      data.appendChild(ALERT);
      data.appendChild(FUND);
      data.appendChild(PERSHING);
      data.appendChild(CUSTOMER_CODE_1);
      data.appendChild(CUSTOMER_CODE_2);
      data.appendChild(STATUS_INDICATOR);
      data.appendChild(BACK_OFFICE_CODE);

      /* MCG 07/12/2009 */
      data.appendChild(CIF_ALT_TYPE);

      /* AML 20/03/2012 */
      data.appendChild(STRA);
      
      
      final boolean envioCamposMifid = getEnvioCamposMifid();

      if (envioCamposMifid) {
        Element MIFID_CLIENT_TYPE = document.createElement("MIFID_CLIENT_TYPE");
        
        Element MIFID_CLIENT_CODE = document.createElement("MIFID_CLIENT_CODE");
        
        
        MIFID_CLIENT_TYPE.appendChild(document.createTextNode(getMifidClientType()));
                
        MIFID_CLIENT_CODE.appendChild(document.createTextNode(getMifidClientCode()));
        
        
        data.appendChild(MIFID_CLIENT_TYPE);
        
        data.appendChild(MIFID_CLIENT_CODE);
        
      }


      // ---------------------------------------------------------

      CLIENT_ACCOUNT.appendChild(header);
      CLIENT_ACCOUNT.appendChild(data);

      document.appendChild(CLIENT_ACCOUNT);

      logger.debug("clientAccount: estructura documento XML completada");

      // Creacion del fichero XML.
      // ---------------------------------------------------------
      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("clientAccount: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("clientAccount: error en la generacion del docuemento XML");
      logger.warn("clientAccount: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a client account";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a client account";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a client account";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("clientAccount: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, ECMMdd0000.xml donde
   * EC es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getClientAccounts();
    Path path = Paths.get(getRutaDestino(), String.format("EC%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("ClientAccounts: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the account_mnemonic.
   * 
   * @param account_mnemonic the new account_mnemonic
   */
  protected void setAccount_mnemonic(String account_mnemonic) {
    this.account_mnemonic = account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the account_mnemonic.
   * 
   * @return the account_mnemonic
   */
  protected String getAccount_mnemonic() {
    return account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the account_name.
   * 
   * @param account_name the new account_name
   */
  protected void setAccount_name(String account_name) {
    this.account_name = account_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the account_name.
   * 
   * @return the account_name
   */
  protected String getAccount_name() {
    return account_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_mnemonic.
   * 
   * @param client_mnemonic the new client_mnemonic
   */
  protected void setClient_mnemonic(String client_mnemonic) {
    this.client_mnemonic = client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_mnemonic.
   * 
   * @return the client_mnemonic
   */
  protected String getClient_mnemonic() {
    return client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the confirmation_method.
   * 
   * @param confirmation_method the new confirmation_method
   */
  protected void setConfirmation_method(String confirmation_method) {
    this.confirmation_method = confirmation_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the confirmation_method.
   * 
   * @return the confirmation_method
   */
  protected String getConfirmation_method() {
    return confirmation_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the alert.
   * 
   * @param alert the new alert
   */
  protected void setAlert(String alert) {
    this.alert = alert;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the alert.
   * 
   * @return the alert
   */
  protected String getAlert() {
    return alert;
  }

  /* MCG 07/12/2009 */
  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the cif_alt_type.
   * 
   * @param alert the new cif_alt_type
   */
  protected void setCif_alt_type(String cif_alt_type) {
    this.cif_alt_type = cif_alt_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the cif_alt_type.
   * 
   * @return the cif_alt_type
   */
  protected String getCif_alt_type() {
    return cif_alt_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the status_indicator.
   * 
   * @param status_indicator the new status_indicator
   */
  protected void setStatus_indicator(String status_indicator) {
    this.status_indicator = status_indicator;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the status_indicator.
   * 
   * @return the status_indicator
   */
  protected String getStatus_indicator() {
    return status_indicator;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the oasys.
   * 
   * @return the oasys
   */
  protected String getOasys() {
    return oasys;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the oasys.
   * 
   * @param oasys the new oasys
   */
  protected void setOasys(String oasys) {
    this.oasys = oasys;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // Cambios a realizar por consulta telefonica, Paloma Moreno
  protected String getFund() {
    return fund;
  }

  // Cambios a realizar por consulta telefonica, Paloma Moreno
  protected void setFund(String fund) {
    this.fund = fund;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /* AML 20/03/2012 */
  public String getStra() {
    return stra;
  }

  public void setStra(String stra) {
    this.stra = stra;
  }
  
  public boolean getEnvioCamposMifid() {
    return envioCamposMifid;
  }

  public void setEnvioCamposMifid(boolean envioCamposMifid) {
    this.envioCamposMifid = envioCamposMifid;
  }

  public String getMifidClientType() {
    return mifidClientType;
  }

  public void setMifidClientType(String mifidClientType) {
    this.mifidClientType = mifidClientType;
  }

  public String getMifidClientCode() {
    return mifidClientCode;
  }

  public void setMifidClientCode(String mifidClientCode) {
    this.mifidClientCode = mifidClientCode;
  }
  private String countryOfDomicile="GB";

  public String getCountryOfDomicile() {
    return countryOfDomicile;
  }

  public void setCountryOfDomicile(String countryOfDomicile) {
    this.countryOfDomicile = countryOfDomicile;
  }
  
  
  
}
