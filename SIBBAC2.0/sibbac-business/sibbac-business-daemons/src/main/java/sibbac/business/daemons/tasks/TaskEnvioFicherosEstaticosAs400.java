package sibbac.business.daemons.tasks;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTPClient;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControlTaskEstaticos;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.FtpAs400Generico;
import sibbac.common.FtpSendFileDTO;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_ENVIO_FICHEROS_AS400, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 * 7-23 ? * MON-FRI")
public class TaskEnvioFicherosEstaticosAs400 extends WrapperTaskConcurrencyPrevent {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TaskEnvioFicherosEstaticosAs400.class);

  @Value("${estaticos.as400.ftp.folder.out:}")
  private String rutaFicheros;

  @Value("${estaticos.as400.ftp.remote.folder.out:}")
  private String rutaDestino;

  @Value("${estaticos.as400.ftp.file.pattern:}")
  private String patternFicheros;

  @Value("${ftp.url}")
  private String url;
  @Value("${ftp.port}")
  private String port;
  @Value("${ftp.username}")
  private String username;
  @Value("${ftp.password}")
  private String password;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private RegistroControlTaskEstaticos registroControlTaskEstaticosBo;

  @Override
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio buscando ficheros en {} pattern {}.", rutaFicheros, patternFicheros);

    java.util.List<FtpSendFileDTO> ficherosEncontrados = new ArrayList<FtpSendFileDTO>();
    Path path = Paths.get(rutaFicheros);
    Path pathTratados = Paths.get(rutaFicheros, "tratados");

    if (Files.notExists(pathTratados)) {
      Files.createDirectories(pathTratados);
    }

    try (DirectoryStream<Path> ficheros = java.nio.file.Files.newDirectoryStream(path, patternFicheros);) {
      for (Path file : ficheros) {
        LOG.trace("[executeTask] encontrado fichero: {}", file.toString());
        ficherosEncontrados.add(new FtpSendFileDTO(file, file.getFileName().toString(), pathTratados));
      }
    }
    if (ficherosEncontrados.size() > 0) {
      RegistroControl control = ctx.getBean(RegistroControl.class);
      FtpAs400Generico ftpSibbacAs400 = new FtpAs400Generico();
      FTPClient ftp = ftpSibbacAs400.connect(url, port, username, password, rutaDestino);
      if (ftp != null) {
        try {
          int i = 0;
          while (i < ficherosEncontrados.size()) {

            try {
              ftpSibbacAs400.send(ftp, ficherosEncontrados.subList(i, i + 1));
              registroControlTaskEstaticosBo.generarRegistroControl(control, ficherosEncontrados.get(i).getFile()
                  .getFileName().toString(), "Enviado AS400", "P");
            }
            catch (Exception e) {
              LOG.warn("[executeTask] no se ha podido escribir el registro de control del envio al fda: {}", path
                  .getFileName().toString(), e);
              registroControlTaskEstaticosBo.generarRegistroControl(control, (String) ficherosEncontrados.get(i)
                  .getFile().getFileName().toString(), e.getMessage(), "E");
            }
            i++;
          }

        }
        catch (Exception e) {
          throw e;
        }
        finally {
          ftp.disconnect();
          ficherosEncontrados.clear();
        }
      }
    }
    LOG.info("[executeTask] fin.");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_ENVIO_FICHEROS_ESTATICOS_AS400;
  }

}
