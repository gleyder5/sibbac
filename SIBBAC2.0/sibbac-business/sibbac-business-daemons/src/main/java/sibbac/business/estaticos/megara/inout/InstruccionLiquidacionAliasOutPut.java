package sibbac.business.estaticos.megara.inout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.AliasOurSettMapGeneral;
import sibbac.business.estaticos.fidessa.fda.output.AliasOurSettMapSpecific;
import sibbac.business.estaticos.fidessa.fda.output.AliasYourSettMapGeneral;
import sibbac.business.estaticos.fidessa.fda.output.AliasYourSettMapSpecific;
import sibbac.database.megbpdta.bo.Fmeg0aecBo;
import sibbac.database.megbpdta.bo.Fmeg0aelBo;
import sibbac.database.megbpdta.model.Fmeg0aec;
import sibbac.database.megbpdta.model.Fmeg0ael;
import sibbac.database.megbpdta.model.Fmeg0ali;

/**
 * InstruccionLiquidacionSubAccount Genera los archivos xml correspondientes a
 * instrucciones de liquidacion generados por la entrada de un archivo Alias
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 * 
 */
@Service
@Scope(value = "prototype")
public class InstruccionLiquidacionAliasOutPut {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(InstruccionLiquidacionAliasOutPut.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0aelBo fmeg0aelBo;

  @Autowired
  private Fmeg0aecBo fmeg0aecBo;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private Fmeg0ali objFmeg0ali = null;

  private String tipoModificacion = "";

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Constructor de clase
   */
  public InstruccionLiquidacionAliasOutPut(Fmeg0ali objFmeg0ali, String tipoModificacion) {

    super();

    this.objFmeg0ali = objFmeg0ali;

    this.tipoModificacion = tipoModificacion.trim();

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * procesarSalidaAlias
   * 
   * Comprueba si se el alias se encuentra de baja en caso de no ser asi genera
   * los xml correspondientes a las instrucciones de liquidacion
   */
  public void procesarSalidaAlias() {

    if (!esBaja(tipoModificacion)) {

      generarInstruccionesLiquidacion();

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionesLiquidacion
   * 
   * genera dos listas que se corresponden con los registros de los ficheros
   * Fmeg0ael y Fmeg0aec dependientes del alias que hemos tratado, por cada uno
   * de los registros recuperados intentamos generar la instruccion de
   * liquidacion correspondiente
   * 
   */
  private void generarInstruccionesLiquidacion() {

    List<Fmeg0ael> listAel = null;
    List<Fmeg0aec> listAec = null;
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      String alias = objFmeg0ali.getCdaliass();

      listAel = fmeg0aelBo.findAllByIdCdaliass(alias);

      listAec = fmeg0aecBo.findAllByIdCdaliass(alias);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

    for (int i = 0; i < listAel.size(); i++) {

      generarInstruccionLiquidacion((Fmeg0ael) listAel.get(i));

    }

    for (int i = 0; i < listAec.size(); i++) {

      generarInstruccionLiquidacion((Fmeg0aec) listAec.get(i));

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionLiquidacion
   * 
   * recibiendo un registro del fichero Fmeg0ael comprueba si es mercado
   * extranjero, nacional o no viene identificado, dependiendo de lo cual,
   * generara, o genera la instruccion de sociedad correspondiente.
   * 
   */

  private void generarInstruccionLiquidacion(Fmeg0ael objFmeg0ael) {

    if ("".equals(objFmeg0ael.getCdmarket().trim())) {
      generarInstruccionSociedadGeneralAlias(objFmeg0ael);
    }
    else {
      generarInstruccionSociedadSpecificAlias(objFmeg0ael);
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionLiquidacion
   * 
   * recibiendo un registro del fichero Fmeg0aec comprueba si es mercado
   * extranjero, nacional o no viene identificado, dependiendo de lo cual,
   * generara, o genera la instruccion de sociedad correspondiente.
   * 
   */
  private void generarInstruccionLiquidacion(Fmeg0aec objFmeg0aec) {

    if ("".equals(objFmeg0aec.getCdmarket().trim())) {
      generarInstruccionCustodioGeneralAlias(objFmeg0aec);
    }
    else {
      generarInstruccionCustodioSpecificAlias(objFmeg0aec);
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionSociedadGeneralAlias
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Our
   * Sett Map General y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros Our Sett Map General se van a generar existan o no existan en
   * el fichero FMEG0CHA.
   */

  private void generarInstruccionSociedadGeneralAlias(Fmeg0ael objFmeg0ael) {

    AliasOurSettMapGeneral aliasOurSettMapGeneral = ctx.getBean(AliasOurSettMapGeneral.class, objFmeg0ael,
        tipoModificacion);
    aliasOurSettMapGeneral.procesarXML();
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      objFmeg0ael.setIdenvfid("S");
      fmeg0aelBo.save(objFmeg0ael, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

    /* } */

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionSociedadGeneralAlias
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Our
   * Sett Map Specific y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros Our Sett Map Specific se van a generar existan o no existan en
   * el fichero FMEG0CHA.
   */

  private void generarInstruccionSociedadSpecificAlias(Fmeg0ael objFmeg0ael) {

    Map<String, Object> paramQuery = new HashMap<>();
    paramQuery.put("countp", objFmeg0ael.getCdcleare().trim());
    paramQuery.put("settle", objFmeg0ael.getTpsettle().trim());
    paramQuery.put("mercado", objFmeg0ael.getCdmarket().trim());
    paramQuery.put("fecha", generarFecha());
    AliasOurSettMapSpecific aliasOurSettMapSpecific = ctx.getBean(AliasOurSettMapSpecific.class, objFmeg0ael,
        tipoModificacion);
    aliasOurSettMapSpecific.procesarXML();
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      objFmeg0ael.setIdenvfid("S");
      fmeg0aelBo.save(objFmeg0ael, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

    /* } */

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionSociedadGeneralAlias
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Your
   * Sett Map General y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros your Sett Map General se van a generar existan o no existan en
   * el fichero FMEG0CHA.
   */

  private void generarInstruccionCustodioGeneralAlias(Fmeg0aec objFmeg0aec) {
    AliasYourSettMapGeneral aliasYourSettMapGeneral = ctx.getBean(AliasYourSettMapGeneral.class, objFmeg0aec,
        tipoModificacion);
    aliasYourSettMapGeneral.procesarXML();
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      objFmeg0aec.setIdenvfid("S");
      fmeg0aecBo.save(objFmeg0aec, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionSociedadGeneralAlias
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Your
   * Sett Map Specific y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros Your Sett Map Specific se van a generar existan o no existan
   * en el fichero FMEG0CHA.
   */

  private void generarInstruccionCustodioSpecificAlias(Fmeg0aec objFmeg0aec) {
    AliasYourSettMapSpecific AliasYourSettMapSpecific = ctx.getBean(AliasYourSettMapSpecific.class, objFmeg0aec,
        tipoModificacion);

    AliasYourSettMapSpecific.procesarXML();
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();
      objFmeg0aec.setIdenvfid("S");
      fmeg0aecBo.save(objFmeg0aec, false);
      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

    /* } */

  }

  // -----------------------------------------------------------------------------
  // -----------------------------------------------------------------------------

  /**
   * Metodo para la validacion de mercado extranjero
   * 
   * Devuelve un valor un valor booleano indicando si el estado del alias es
   * baja o no
   * 
   * @param mercado
   * @return boolean
   */
  private boolean esBaja(String estado) {
    boolean result = false;

    if ("D".equals(estado)) {

      result = true;
    }

    return result;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Metodo para generar la fecha actual en un formato especifico para realizar
   * los accesos a la tabla FMEG0CHA
   * 
   * @param
   * @return String
   */
  private String generarFecha() {

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    return fechaFormato.format(new Date());

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}