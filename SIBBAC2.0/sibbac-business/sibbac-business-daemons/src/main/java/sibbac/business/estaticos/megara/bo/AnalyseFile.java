package sibbac.business.estaticos.megara.bo;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import sibbac.business.estaticos.megara.input.Alias;
import sibbac.business.estaticos.megara.input.BroClient;
import sibbac.business.estaticos.megara.input.BrokerByStockExchange;
import sibbac.business.estaticos.megara.input.Comisiones;
import sibbac.business.estaticos.megara.input.ComisionesFidessa;
import sibbac.business.estaticos.megara.input.EligibleSubAccByAlias;
import sibbac.business.estaticos.megara.input.Entity;
import sibbac.business.estaticos.megara.input.SLASettlementChain;
import sibbac.business.estaticos.megara.input.SubAccount;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.xml.NewXmlException;
import sibbac.business.estaticos.utils.xml.ReadDocument;
import sibbac.business.estaticos.utils.xml.ReadXmlException;

/*
 * Clase para leer un fichero xml
 * detecta el tipo de fichero xml que esta analizando y dependiendo de ello 
 * llama a una clase u otra para tratarlo 
 * 
 * Para determinar el tipo de fichero xml analiza el atributo interface de la cabecera
 */

@Service
@Scope(value = "prototype")
public class AnalyseFile extends ReadDocument {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(AnalyseFile.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos directorios;

  final String PROCESADO = "P";

  final String ERROR = "E";

  private List<String> tipoXML;

  private String nombreXML;

  public AnalyseFile(String nombreXML) throws NewXmlException, ReadXmlException {

    super();

    this.nombreXML = nombreXML;

    tipoXML = new ArrayList<>();
    tipoXML.add("Entity");
    tipoXML.add("BroClient");
    tipoXML.add("BrokerByStockExchange");
    tipoXML.add("Alias");
    tipoXML.add("SLASettlementChain");
    tipoXML.add("SubAccount");
    tipoXML.add("EligibleSubAccByAlias");
    tipoXML.add("Holder");
    tipoXML.add("UPDCOMM");

  }

  @PostConstruct
  private void postConstruct() throws ReadXmlException {
    Path file = Paths.get(directorios.getDirIn(), nombreXML);
    readDoc(file.toString());
    logger.debug("Comienza analisis del fichero: ", file.toString());
  }

  public void processFile() {

    Document document = getDoc();
    /*
     * comparamos el valor del nodo "interface" con el vector tipoXML para
     * determinar el modelo de XML que vamos a tratar
     */

    String _entity = document.getElementsByTagName("entity").item(0).getFirstChild().getNodeValue();

    if ("1".equals(_entity)) {
      // MEGATRADE COMISIONES
      ctx.getBean(Comisiones.class, document).procesarXML_In(nombreXML);

    }
    else {
      // INTERFACE MEGARA
      Node _mode = document.getElementsByTagName("mode").item(0);
      Node _interface = document.getElementsByTagName("interface").item(0);

      /* AML 21-07-2011 */
      /* Se va a procesar los ficheros Holder */
      final String interfaz = _interface.getFirstChild().getNodeValue();
      if ((tipoXML.indexOf(interfaz) >= 0) && (tipoXML.indexOf(interfaz) <= tipoXML.size())) {

        if ("I".equals(_mode.getFirstChild().getNodeValue()) || ("D".equals(_mode.getFirstChild().getNodeValue()))
            || "UPDCOMM".equals(interfaz) || "R".equals(_mode.getFirstChild().getNodeValue())|| "H".equals(_mode.getFirstChild().getNodeValue())) {

          switch (tipoXML.indexOf(interfaz)) {

            case 0: // Entity (Entidades)
              ctx.getBean(Entity.class, document).procesarXML_In(nombreXML);

              break;

            case 1: // BroClient (Otros datos del cliente)
              ctx.getBean(BroClient.class, document).procesarXML_In(nombreXML);

              break;

            case 2: // BrokerByStockExchange (Broker por Mercados)
              ctx.getBean(BrokerByStockExchange.class, document).procesarXML_In(nombreXML);

              break;

            case 3: // Alias (Cuentas) logger.info("Inicio tratamiento
              // Alias");

              ctx.getBean(Alias.class, document).procesarXML_In(nombreXML);

              break;

            case 4: // SLASettlementChain (Instrucciones Liquidacion)
              ctx.getBean(SLASettlementChain.class, document).procesarXML_In(nombreXML);

              break;

            case 5: // SubAccount (Subcuentas)
              ctx.getBean(SubAccount.class, document).procesarXML_In(nombreXML);

              break;

            case 6: // EligibleSubAccByAlias (Subcuentas - Alias)
              ctx.getBean(EligibleSubAccByAlias.class, document).procesarXML_In(nombreXML);

              break;
            /* AML 21-07-2011 */
            /* Se va a procesar los ficheros Holder */
            case 7: // Holders
              // ctx.getBean(Holder.class, document).procesarXML_In(nombreXML);

              break;
            case 8: // Comisiones Fidessa
              ctx.getBean(ComisionesFidessa.class, document).procesarXML_In(nombreXML);

              break;

            default:
              ;
          }

        }
        else {
          logger.warn("TIPO DE ACTUALIZACION NO CONTEMPLADO");
          RegistroControl registroControl = ctx.getBean(RegistroControl.class, _interface.getFirstChild()
              .getNodeValue(), "", nombreXML, "E", "TIPO DE ACTUALIZACION NO CONTEMPLADO", "AnalyseFile", "MG");
          registroControl.write();
          directorios.moverDirError(nombreXML);
        }
      }
      else {
        logger.warn("VALOR DE INTERFACE NO CONTEMPLADO");
        /*
         * AML 28/09/11 Si es un interface no contemplado, se indica cual es y
         * se generar un warning y no un error
         */
        RegistroControl registroControl = ctx.getBean(RegistroControl.class, interfaz, "", nombreXML, "W",
            "El INTERFACE " + interfaz.toUpperCase() + " NO ESTA CONTEMPLADO", "AnalyseFile", "MG");
        registroControl.write();
        directorios.moverDirError(nombreXML);
      }
    }
  }
}
