package sibbac.business.daemons.ork.bo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.security.MessageDigest;
import static java.util.Objects.requireNonNull;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.ork.common.OrkFiles;
import sibbac.business.daemons.ork.model.OrkBmeField;
import static sibbac.common.utils.FormatDataUtils.DATE_FORMAT;

class OrkWriter implements Callable<byte[]> {
	
  static final String CVS_FORMAT = "\"%s\";";
  
	private static final Logger LOG = getLogger(OrkWriter.class);
	
  private final OrkFiles orkFiles;
	
	private final BlockingQueue<OrkPetition> resolvedPetitions;
	
	private final List<OrkBmeField> builders;
	
	private final String sequence;
	
	OrkWriter(OrkFiles orkFiles, int sequence,
			BlockingQueue<OrkPetition> resolvedPetitions, List<OrkBmeField> builders) {
		this.orkFiles = requireNonNull(orkFiles, "El fichero orkFile es obligatorio");
		this.sequence = Integer.toString(sequence);
		this.resolvedPetitions = requireNonNull(resolvedPetitions, "La cola de peticiones es obligatoria");
		this.builders = requireNonNull(builders, "La lista de constructores del fichero ork es obligatoria");
	}

	@Override
	public byte[] call() throws Exception {
		final MessageDigest md;
		final DateFormat df;
		final String sendDate;
		OrkPetition resolved;
		int lineas = 0;
		
		df = new SimpleDateFormat(DATE_FORMAT);
		sendDate = df.format(new Date());
		md = MessageDigest.getInstance("MD5");
		LOG.info("[run] Inicio de escritura de ficheros ORK {}", orkFiles.toString());
		try(PrintWriter orkWriter = orkFiles.getOrkWriter(md);
				PrintWriter updateInfoWriter = orkFiles.getUpdateInfoWriter()) {
			while(!(resolved = resolvedPetitions.take()).isEnd()) {
				updateInfoWriter.printf(CVS_FORMAT, "S");
				updateInfoWriter.printf(CVS_FORMAT, sendDate);
				updateInfoWriter.printf(CVS_FORMAT, sequence);
			  resolved.validate(builders);
        if(resolved.isSuccess()) {
          resolved.printResolved(orkWriter, builders);
          updateInfoWriter.print(";");
        }
        else {
          resolved.printError(updateInfoWriter);
        }
        resolved.printId(updateInfoWriter);
        updateInfoWriter.println();
        lineas++;
        if(lineas % 500 == 0) {
          LOG.info("[run] Lineas escritas: {}", lineas);
        }
			}
			LOG.info("[run] Finalizacion exitosa de escritura de ficheros ORK {} con  {} lineas", 
			    orkFiles.toString(), lineas);
		} 
		catch(InterruptedException e) {
			LOG.error("[run] La cola de peticiones resueltas ha sido interrumpido", e);
		  throw e;
		}
		catch(IOException ioex) {
			LOG.error("[run] Error en la escritura de una peticion resuelta. Fin de ejecución", ioex);
			throw ioex;
		}
		catch(RuntimeException rex) {
			LOG.error("[run] Error no controlado durante la escritura de peticiones resueltas", rex);
			throw rex;
		}
		return md.digest();
	}
	
}
