package sibbac.business.daemons.confirmacionminotista.bo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.FormatterLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.confirmacionminotista.dto.ConfirmacionMinorista16RecordBean;
import sibbac.business.daemons.confirmacionminotista.dto.ConfirmacionMinorista17RecordBean;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista16DTO;
import sibbac.business.wrappers.database.dto.confirmacionminotista.ConfirmacionMinorista17DTO;
import sibbac.business.wrappers.database.model.RecordBean;
import sibbac.business.wrappers.database.model.Tmct0bokId;
import sibbac.common.utils.DateHelper;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;

/**
 * @author xIS16630
 *
 */
@Component
public class ConfirmacionMinoristaBo {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(ConfirmacionMinoristaBo.class);

  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Value("${daemons.confirmacionminotista.root.dir}")
  private String rootDir;
  @Value("${daemons.confirmacionminotista.tmp.file}")
  private String tmpFileName;
  @Value("${daemons.confirmacionminotista.final.file}")
  private String finalFileName;
  @Value("${daemons.confirmacionminotista.clearing.members}")
  private String compensadoresConfig;
  @Value("${daemons.confirmacionminotista.clearing.member.referencies}")
  private String referenciasConfig;
  @Value("${daemons.confirmacionminotista.like.cdrefban}")
  private String cdrefbanStartConfig;
  // 4716,22672,22680,22645
  @Value("${daemons.confirmacionminotista.not.aliass}")
  private String notInAliassConfig;
  @Value("${daemons.confirmacionminotista.clearing.platform}")
  private String clearingPlatform;
  // 3531
  @Value("${daemons.confirmacionminotista.clearer}")
  private String cdentliq;

  @Autowired
  private SendMail mail;

  @Value("${daemons.confirmacionminotista.mail.to}")
  private String mailTo;

  @Value("${daemons.confirmacionminotista.mail.cc}")
  private String mailCc;

  @Value("${daemons.confirmacionminotista.mail.subject}")
  private String mailSubject;

  @Value("${daemons.confirmacionminotista.mail.body}")
  private String mailBody;

  private Path confirmacionMinoristaFilePath;

  @Autowired
  private Tmct0desgloseclititBo tmct0desgloseclititBo;

  @Autowired
  private Tmct0bokBo tmct0bokBo;

  @Autowired
  private Tmct0alcBo tmct0alcBo;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  @Autowired
  private Tmct0logBo logBo;

  private FlatFileItemWriter<RecordBean> writer;

  private FormatterLineAggregator<RecordBean> lineAgregator16;
  private FormatterLineAggregator<RecordBean> lineAgregator17;

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio.");

    Path workPath = Paths.get(rootDir);
    LOG.info("[executeTask] recuperada ruta de trabajo : {}", workPath);
    confirmacionMinoristaFilePath = Paths.get(rootDir, tmpFileName);
    LOG.info("[executeTask] usando fichero temporal : {}", confirmacionMinoristaFilePath);

    initWriter();

    // para contener las ordenes a marcar como enviadas
    Collection<Tmct0bokId> bookings = new HashSet<Tmct0bokId>();
    LOG.info("[executeTask] compensadores: {} referencias: {}", compensadoresConfig, referenciasConfig);
    LOG.info("[executeTask] not aliass: {} ", notInAliassConfig);
    LOG.info("[executeTask] cdrefban like: {} ", cdrefbanStartConfig);
    List<String> compensadores = new ArrayList<String>();
    compensadores.addAll(Arrays.asList(compensadoresConfig.split(",")));
    List<String> referencias = new ArrayList<String>();
    referencias.addAll(Arrays.asList(referenciasConfig.split(",")));
    List<String> notInAliass = new ArrayList<String>();
    notInAliass.addAll(Arrays.asList(notInAliassConfig.split(",")));

    try {
      Tmct0cfg aliasSpb = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                          Tmct0cfgConfig.FICHERO_SPB_ALIAS_CODE.getProcess(),
                                                                          Tmct0cfgConfig.FICHERO_SPB_ALIAS_CODE.getKeyname());
      if (aliasSpb != null) {
        String[] aliasSpbArray = aliasSpb.getKeyvalue().trim().split(",");
        if (aliasSpbArray.length > 0) {
          notInAliass.addAll(Arrays.asList(aliasSpbArray));
          LOG.info("[executeTask] Add SBP not aliass: {} ", notInAliass);
        }
      }
      Tmct0cfg aliasCtm = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                          Tmct0cfgConfig.FICHERO_SPB_ALIAS_CODE_CTM.getProcess(),
                                                                          Tmct0cfgConfig.FICHERO_SPB_ALIAS_CODE_CTM.getKeyname());

      if (aliasCtm != null) {
        String[] aliasCtmArray = aliasCtm.getKeyvalue().trim().split(",");
        if (aliasCtmArray.length > 0) {
          notInAliass.addAll(Arrays.asList(aliasCtmArray));
          LOG.info("[executeTask] Add CTM not aliass add : {} ", notInAliass);
        }
      }
    } catch (Exception e) {
      LOG.warn("Incidencia no se ha podido recuperar los alias SBP y CTM", e);
    }

    Date sincedate = getSinceDate();
    LOG.info("[executeTask] buscando bookings desde {}...", sincedate);
    List<Tmct0bokId> tmct0bokIds = tmct0bokBo.findAllConfirmacionMinorista(notInAliass, sincedate);

    for (Tmct0bokId tmct0bokId : tmct0bokIds) {
      LOG.info("[executeTask] buscando registros 16 desde {}...", sincedate);
      List<ConfirmacionMinorista16DTO> cm16s = tmct0desgloseclititBo.findAllConfirmacionMinorista16(tmct0bokId.getNuorden(),
                                                                                                    tmct0bokId.getNbooking(),
                                                                                                    compensadores,
                                                                                                    referencias);
      String cdrefban = null;
      if (CollectionUtils.isNotEmpty(cm16s)) {

        boolean enviar = true;

        BigDecimal titulos16 = BigDecimal.ZERO;
        for (ConfirmacionMinorista16DTO confirmacionMinorista16DTO : cm16s) {
          titulos16 = titulos16.add(confirmacionMinorista16DTO.getImtitulos());
          if (confirmacionMinorista16DTO.getCdrefban() == null
              || confirmacionMinorista16DTO.getCdrefban().trim().length() != 20
              || (cdrefbanStartConfig != null && !confirmacionMinorista16DTO.getCdrefban()
                                                                            .matches(cdrefbanStartConfig.replace("%",
                                                                                                                 ".*")))) {
            enviar = false;
            cdrefban = confirmacionMinorista16DTO.getCdrefban();
            break;
          }
        }

        if (enviar) {
          LOG.info("[executeTask] buscando registros 17 desde {}...", sincedate);

          List<ConfirmacionMinorista17DTO> cm17s = tmct0alcBo.findAllConfirmacionMinorista17(tmct0bokId.getNuorden(),
                                                                                             tmct0bokId.getNbooking(),
                                                                                             compensadores, referencias);

          if (CollectionUtils.isNotEmpty(cm17s)) {
            BigDecimal titulos17 = BigDecimal.ZERO;

            for (ConfirmacionMinorista17DTO confirmacionMinorista17DTO : cm17s) {
              titulos17 = titulos17.add(confirmacionMinorista17DTO.getNutitliq());
            }

            if (titulos16.compareTo(titulos17) == 0) {
              bookings = new ArrayList<Tmct0bokId>();
              writeRegistros16Y17(cm16s, cm17s, bookings);

              if (CollectionUtils.isNotEmpty(bookings)) {
                logBo.insertarRegistro(tmct0bokId.getNuorden(), tmct0bokId.getNbooking(), "OK-Enviado 16-17", "",
                                       "ENV_16-17");
                LOG.info("[executeTask] Se han escrito 16 17 {} en fichero.", bookings);
              } else {
                LOG.info("[executeTask] No se ha enviado nada.");
                logBo.insertarRegistro(tmct0bokId.getNuorden(), tmct0bokId.getNbooking(),
                                       "INCIDENCIA-No se ha generado el 16-17", "", "ENV_16-17");
                continue;
              }
            } else {
              LOG.warn("[executeTask] Los titulos del 16 : {} y del 17 : {} no coinciden .", titulos16, titulos17);
              logBo.insertarRegistro(tmct0bokId.getNuorden(), tmct0bokId.getNbooking(),
                                     "INCIDENCIA-NO envia 16-17, tit '16' : " + titulos16 + " no coinciden tit '17' : "
                                         + titulos17, "", "ENV_16-17");
              enviar = false;
            }
            bookings.clear();
          }
        } else {
          logBo.insertarRegistro(tmct0bokId.getNuorden(), tmct0bokId.getNbooking(),
                                 "OK-El cdrefban : '" + cdrefban + "' no envia 16-17. Debe empezar por '"
                                     + cdrefbanStartConfig + "'", "", "ENV_16-17");
        }

      } else {
        logBo.insertarRegistro(tmct0bokId.getNuorden(), tmct0bokId.getNbooking(),
                               "OK-No tiene que enviar 16-17. Solo Envian Comp:" + compensadoresConfig + ". Refs:"
                                   + referenciasConfig, "", "ENV_16-17");
      }

      bookings = new ArrayList<Tmct0bokId>();
      bookings.add(tmct0bokId);
      LOG.info("[executeTask] Marcando flag para no volver a procesar {} .", bookings);
      Integer actualizados = updateEnviadoConfirmacionMinorista1617(new ArrayList<Tmct0bokId>(bookings));
      LOG.info("[executeTask] Se han marcado {} ordenes flag para no volver a procesar .", actualizados);

    }


    LOG.info("[executeTask] final.");
  }

  private Date getSinceDate() throws ParseException {
    LOG.trace("[getSinceDate] inicio.");
    int daysPrevious = 7;

    try {
      Tmct0cfg cfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
                                                                     Tmct0cfgConfig.CONFIRMACION_MINORISTA_SCAN_DAYS_PREV.getProcess(),
                                                                     Tmct0cfgConfig.CONFIRMACION_MINORISTA_SCAN_DAYS_PREV.getKeyname());
      daysPrevious = Integer.parseInt(cfg.getKeyvalue().trim());
    } catch (Exception e) {
      LOG.warn("[getSinceDate] Incidencia NO CONFIG : {}", Tmct0cfgConfig.CONFIRMACION_MINORISTA_SCAN_DAYS_PREV);
    }

    Date sincedate = DateHelper.getPreviousWorkDate(new Date(), daysPrevious, tmct0cfgBo.getWorkDaysOfWeek(),
                                                    tmct0cfgBo.getHolidays());
    LOG.trace("[getSinceDate] fin == {}.", sincedate);
    return sincedate;
  }

  private void initLineAgregator16() {
    lineAgregator16 = new FormatterLineAggregator<RecordBean>();

    BeanWrapperFieldExtractor<RecordBean> bwfe = new BeanWrapperFieldExtractor<RecordBean>();

    String[] names = new String[] { "tipoRegistro", "nbooking1St", "feejecucSt", "hoejecucSt", "fillerPostHoejecucSt", "cdbrokerSt", "cdrefban1St", "cdtpoperSt", "cdisinSt", "evalueSt", "imcbmercSt", "cdentliqSt", "fevalorSt", "nutitejeSt", "fillerPostNutitejeSt", "nurefereSt", "fillerPostNurefereSt", "nuejecucSt", "fillerPostNuejecucSt", "executionTradingVenueSt", "fillerPostExecutionTradingVenueSt", "cdrefban2St", "fillerPostCdrefban2St", "nbooking2St", "filler"
    };
    bwfe.setNames(names);
    lineAgregator16.setFieldExtractor(bwfe);

    StringBuilder format = new StringBuilder("%-2s");// idMensaje
    format.append("%-16s");// nuorden
    format.append("%-8s"); // feejecuc
    format.append("%-6s");// hojecuc
    format.append("%-19s");// filler
    format.append("%-3s");// cdbroker
    format.append("%-20s");// cdrefban
    format.append("%-1s");// cdtpoper
    format.append("%-12s");// cdisin
    format.append("%-1s");// evlue
    format.append("%-9s");// imcbmerc
    format.append("%-4s");// cdentliq
    format.append("%-8s");// fevalor
    format.append("%-10s");// nutiteje
    format.append("%-20s");// filler
    format.append("%-9s");// nurefere
    format.append("%-7s");// filler
    format.append("%-16s");// nuejecuc
    format.append("%-4s");// filler
    format.append("%-4s");// execution trading venue
    format.append("%-4s");// filler
    format.append("%-20s");// cdrefban
    format.append("%-6s");// filler
    format.append("%-16s");// nuorden
    format.append("%-64s");// filler

    // lineAgregator16.setFormat("%-2s%-16s%-8s%-6s%-19s%-3s%-20s%-1s%-12s%-1s%-9s%-4s%-8s%-10s%-20s%-9s%-7s%-16s%-4s%-4s%-4s%-20s%-6s%-16s%-64s");
    lineAgregator16.setFormat(format.toString());
    lineAgregator16.setMaximumLength(291);
    lineAgregator16.setMinimumLength(291);

  }

  private void initLineAgregator17() {
    lineAgregator17 = new FormatterLineAggregator<RecordBean>();

    BeanWrapperFieldExtractor<RecordBean> bwfe = new BeanWrapperFieldExtractor<RecordBean>();

    String[] names = new String[] { "tipoRegistro", "nbookingSt", "fealtaSt", "hoaltaSt", "fecontraSt", "fevalorSt", "imefeagrSt", "nutitliqSt", "numerico1St", "imcbmercSt", "cdmonisoSt", "plataformaNegociacionSt", "codigoEccSt", "numerico2St", "numerico3St", "imcomisnSt", "imcontrSt", "nvalueSt", "numerico4St", "numerico5St", "filler"
    };
    bwfe.setNames(names);
    lineAgregator17.setFieldExtractor(bwfe);

    StringBuilder format = new StringBuilder("%-2s");// idMensaje
    format.append("%-16s");// nuorden
    format.append("%-8s"); // fealta
    format.append("%-6s");// hoalta
    format.append("%-8s"); // fecontra
    format.append("%-8s"); // fevalor
    format.append("%-16s");// imefeagr
    format.append("%-11s");// nutitliq
    format.append("%-13s");// numerico1
    format.append("%-18s");// imcbmerc
    format.append("%-3s");// moniso
    format.append("%-4s");// executing trading venue
    format.append("%-11s");// ecc
    format.append("%-12s");// numerico2
    format.append("%-12s");// numerico3
    format.append("%-12s");// imcomisn
    format.append("%-12s");// imcomtreu
    format.append("%-1s");// nvalue
    format.append("%-12s");// numerico4
    format.append("%-12s");// numerico5
    format.append("%-57s");// filler

    lineAgregator17.setFormat(format.toString());
    lineAgregator17.setMaximumLength(255);
    lineAgregator17.setMinimumLength(238);
  }

  private void initWriter() {
    LOG.trace("[initWriter] inicio.");
    writer = new FlatFileItemWriter<RecordBean>();
    writer.setAppendAllowed(false);
    writer.setShouldDeleteIfEmpty(true);
    writer.setShouldDeleteIfExists(false);
    writer.setTransactional(true);
    initLineAgregator16();
    initLineAgregator17();
    writer.setLineAggregator(lineAgregator16);
    writer.setResource(new PathResource(confirmacionMinoristaFilePath));
    writer.open(new ExecutionContext());
    LOG.trace("[initWriter] fin.");
  }

  public Path closeWriterAndRenameFile() throws IOException {

    LOG.info("[closeWriterAndRenameFile] inicio.");
    if (writer != null) {
      writer.close();
      LOG.info("[closeWriterAndRenameFile] writer closed.");
      if (Files.exists(confirmacionMinoristaFilePath)) {

        String myFinalFileName = finalFileName;
        Path finalFile = Paths.get(rootDir, myFinalFileName);

        if (Files.exists(finalFile)) {
          LOG.info("[closeWriterAndRenameFile] fichero de envio existe: {}", finalFile);
          myFinalFileName = finalFileName + "_"
                            + FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.FILE_DATETIME_FORMAT);
          Path finalFileDuplicated = Paths.get(rootDir, myFinalFileName);

          if (!Files.exists(finalFileDuplicated)) {
            finalFile = finalFileDuplicated;
          }
          LOG.info("[closeWriterAndRenameFile] New file name: {}", finalFile.getFileName().toString());
        }
        LOG.info("[closeWriterAndRenameFile] ## Enviando por correo el fichero ...");

        try {
          enviarEmail(confirmacionMinoristaFilePath, myFinalFileName);
        } catch (Exception e) {
          LOG.warn("[closeWriterAndRenameFile] Incidencia ", e);
        }

        finalFile = Files.move(confirmacionMinoristaFilePath, finalFile, StandardCopyOption.ATOMIC_MOVE);
        LOG.info("[closeWriterAndRenameFile] file rename to: {}", finalFile.getFileName().toString());
        return finalFile;
      } else {
        LOG.warn("[closeWriterAndRenameFile] fichero no renombrado.");
      }
    }

    LOG.info("[closeWriterAndRenameFile] fin.");
    return null;
  }

  private void enviarEmail(Path file, String myFinalFileName) {
    LOG.info("[enviarEmail] ## inicio");
    Path fileToMail = null;

    try {
      if (Files.exists(file) && Files.size(file) > 0) {
        List<String> to = new ArrayList<String>();
        if (StringUtils.isNotBlank(mailTo)) {
          LOG.info("[enviarEmail] ## Enviando correo a TO: {}", mailTo);
          to.addAll(Arrays.asList(mailTo.split(",")));
        }
        List<String> cc = new ArrayList<String>();
        if (StringUtils.isNotBlank(mailCc)) {
          LOG.info("[enviarEmail] ## Enviando correo a CC: {}", mailCc);
          cc.addAll(Arrays.asList(mailCc.split(",")));
        }



        if (CollectionUtils.isNotEmpty(to) || CollectionUtils.isNotEmpty(cc)) {

          // si el fichero es mayor de 24Mb lo comprimimos en gz
          boolean gzip = Files.size(file) >= (24 * 1024 * 1024);

          fileToMail = Paths.get(rootDir, myFinalFileName + (gzip ? ".gz" : ""));

          WritableByteChannel channel = Files.newByteChannel(fileToMail, StandardOpenOption.CREATE,
                                                             StandardOpenOption.WRITE);

          OutputStream out = null;
          if (gzip) {
            LOG.info("[enviarEmail] ## El fichero debe ir comprimido.");
            out = new GZIPOutputStream(Channels.newOutputStream(channel));
          } else {
            out = Channels.newOutputStream(channel);
          }
          Files.copy(file, out);
          out.close();

          LOG.info("[enviarEmail] ## Enviando correo con fichero: {}", fileToMail.toString());
          List<InputStream> path2attach = new ArrayList<InputStream>();
          List<String> nameDest = new ArrayList<String>();
          InputStream is = Files.newInputStream(fileToMail);
          path2attach.add(is);
          nameDest.add(fileToMail.getFileName().toString());

          mail.sendMail(to, mailSubject, mailBody, path2attach, nameDest, cc);
        } else {
          LOG.info("[enviarEmail] ## Ningun destinatario configurado para enviar el fichero por correo. {}",
                   file.toString());
        }
      }
    } catch (Exception e) {
      LOG.warn("[enviarEmail] incidencia ", e.getMessage());
      LOG.debug("[enviarEmail] incidencia ", e);
    } finally {
      if (fileToMail != null) {
        try {
          Files.deleteIfExists(fileToMail);
        } catch (IOException e) {
          LOG.warn("[enviarEmail] incidencia ", e.getMessage());
          LOG.debug("[enviarEmail] incidencia ", e);
        }
      }
    }

    LOG.info("[enviarEmail] ## fin");
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  private Integer updateEnviadoConfirmacionMinorista1617(List<Tmct0bokId> nbookings) {
    LOG.info("[updateEnviadoConfirmacionMinorista1617] inicio.");
    Integer count = tmct0bokBo.updateEnviadoConfirmacionMinorista1617(nbookings);
    LOG.info("[updateEnviadoConfirmacionMinorista1617] final == {}.", count);
    return count;
  }

  private List<ConfirmacionMinorista16RecordBean> convertR16(List<ConfirmacionMinorista16DTO> cm16s,
                                                             Collection<Tmct0bokId> bookings) {
    ConfirmacionMinorista16RecordBean r16;
    List<ConfirmacionMinorista16RecordBean> r16s = new ArrayList<ConfirmacionMinorista16RecordBean>();

    for (ConfirmacionMinorista16DTO cm16 : cm16s) {
      LOG.trace("[convertR16] Creado r16 desde  {}", cm16);
      r16 = new ConfirmacionMinorista16RecordBean();
      r16.setNuorden(cm16.getBookingId().getNuorden());
      r16.setNbooking1(cm16.getBookingId().getNbooking());
      r16.setNbooking2(cm16.getBookingId().getNbooking());
      r16.setCdbroker(StringHelper.cdBrokerToCode(cm16.getCdbroker()));
      r16.setCdentliq(cdentliq);
      r16.setCdisin(cm16.getCdisin());
      r16.setCdrefban1(cm16.getCdrefban());
      r16.setCdrefban2(cm16.getCdrefban());
      r16.setCdtpoper(cm16.getCdtpoper());
      r16.setExecutionTradingVenue(cm16.getPlataformaNegociacion());
      r16.setFeejecuc(cm16.getFeejecuc());
      r16.setFevalor(cm16.getFevalor());
      r16.setHoejecuc(cm16.getHoejecuc());
      r16.setImcbmerc(cm16.getImprecio());
      r16.setNuejecuc(cm16.getNuejecuc());
      r16.setNurefere(cm16.getNuexemkt());
      r16.setNutiteje(cm16.getImtitulos());
      LOG.trace("[convertR16] Creado r16 {}", r16);
      r16s.add(r16);

      bookings.add(cm16.getBookingId());
    }

    return r16s;
  }

  private List<ConfirmacionMinorista17RecordBean> convertR17(List<ConfirmacionMinorista17DTO> cm17s,
                                                             Collection<Tmct0bokId> ordenes) {
    ConfirmacionMinorista17RecordBean r17;
    List<ConfirmacionMinorista17RecordBean> r17s = new ArrayList<ConfirmacionMinorista17RecordBean>();
    for (ConfirmacionMinorista17DTO cm17 : cm17s) {
      LOG.trace("[convertR16] Creado r17 desde  {}", cm17);
      r17 = new ConfirmacionMinorista17RecordBean();
      r17.setNuorden(cm17.getBookingId().getNuorden());
      r17.setNbooking(cm17.getBookingId().getNbooking());
      r17.setCdmoniso(cm17.getCdmoniso());
      r17.setCodigoEcc(clearingPlatform);
      r17.setFealta(cm17.getFealta());
      r17.setFecontra(cm17.getFecontra());
      r17.setFevalor(cm17.getFevalor());
      r17.setHoalta(cm17.getHoalta());
      r17.setImcbmerc(cm17.getImcbmerc());
      if (cm17.getImcomisn() != null) {
        r17.setImcomisn(cm17.getImcomisn());
      } else {

        LOG.warn("[convertR17] Incidencia orden : {} cm17.getImcomisn() == NULL", cm17.getBookingId());
      }
      if (cm17.getClientPayCanonContr() == 1) {
        if (cm17.getImcontr() != null) {
          r17.setImcontr(cm17.getImcontr());
        } else {
          LOG.warn("[convertR17] Incidencia orden : {} cm17.getImcontr() == NULL", cm17.getBookingId());
        }
      }
      if (cm17.getImefeagr() != null) {
        r17.setImefeagr(cm17.getImefeagr());
      } else {
        LOG.warn("[convertR17] Incidencia orden : {} cm17.getImefeagr() == NULL", cm17.getBookingId());
      }
      if (cm17.getNutitliq() != null) {
        r17.setNutitliq(cm17.getNutitliq());
      } else {
        LOG.warn("[convertR17] Incidencia orden : {} cm17.getNutitliq() == NULL", cm17.getBookingId());
      }
      r17.setNvalue('N');
      r17.setPlataformaNegociacion(cm17.getPlataformaNegociacion());

      LOG.trace("[convertR17] Creado r17 {}", r17);
      r17s.add(r17);

      ordenes.add(cm17.getBookingId());
    }

    return r17s;
  }

  /**
   * Escribe los registros en el fichero
   * 
   * @param cm16s lista de dtos con los registros 16 a enviar
   * @param cm17s lista de dt0s con los regirstros 17 a enviar
   * @param bookings lista de bookings a devolver para actualizar el flag de enviado
   * @throws Exception
   */
  @Transactional
  private void writeRegistros16Y17(List<ConfirmacionMinorista16DTO> cm16s,
                                   List<ConfirmacionMinorista17DTO> cm17s,
                                   Collection<Tmct0bokId> bookings) throws Exception {
    LOG.info("[writeRegistros16Y17] inicio");

    LOG.info("[writeRegistros16Y17] Creando registros 16...");
    Collection<Tmct0bokId> bookingsR16 = new HashSet<Tmct0bokId>();
    List<ConfirmacionMinorista16RecordBean> r16s = convertR16(cm16s, bookingsR16);
    LOG.info("[writeRegistros16Y17] Creando registros 17...");
    Collection<Tmct0bokId> bookingsR17 = new HashSet<Tmct0bokId>();
    List<ConfirmacionMinorista17RecordBean> r17s = convertR17(cm17s, bookingsR17);
    // hacemos interseccion entre las dos collecciones de tal modod que solo obtenemos las ordenes que tienen registros
    // 16 y 17
    LOG.info("[writeRegistros16Y17] Revisando que todas las bookings tengan reg's 16 y 17...");
    @SuppressWarnings("unchecked")
    Collection<Tmct0bokId> bookingsWrite = CollectionUtils.intersection(bookingsR16, bookingsR17);
    if (CollectionUtils.isNotEmpty(bookingsWrite)) {

      List<ConfirmacionMinorista16RecordBean> r16sWrite = new ArrayList<ConfirmacionMinorista16RecordBean>();
      aniadirR16ToWrite(bookingsWrite, bookingsR16, r16s, r16sWrite);

      List<ConfirmacionMinorista17RecordBean> r17sWrite = new ArrayList<ConfirmacionMinorista17RecordBean>();
      aniadirR17ToWrite(bookingsWrite, bookingsR17, r17s, r17sWrite);

      LOG.info("[writeRegistros16Y17] Escribiendo {} registros 16...", r16sWrite.size());
      writer.setLineAggregator(lineAgregator16);
      writer.write(r16sWrite);

      r16sWrite.clear();
      LOG.info("[writeRegistros16Y17] Escribiendo {} registros 17...", r17sWrite.size());
      writer.setLineAggregator(lineAgregator17);
      writer.write(r17sWrite);
      r17sWrite.clear();

      LOG.info("[writeRegistros16Y17] Añadiendo {} ordenes a la lista para marcar como enviadas.", bookingsWrite.size());
      bookings.addAll(bookingsWrite);

      bookingsWrite.clear();
    } else {
      LOG.info("[writeRegistros16Y17] No hay registros 16 y 17 que enviar.");
    }
    LOG.info("[writeRegistros16Y17] fin");
  }

  /**
   * Añade la lista r16s a la lista r16sWrite si hay que enviarlo
   * 
   * @param bookingsWrite bookings que se van a enviar
   * @param bookingsR16 bookings con registro 16
   * @param r16s todos registros 16
   * @param r16sWrite registros 16 que se van a enviar
   */
  private void aniadirR16ToWrite(Collection<Tmct0bokId> bookingsWrite,
                                 Collection<Tmct0bokId> bookingsR16,
                                 List<ConfirmacionMinorista16RecordBean> r16s,
                                 List<ConfirmacionMinorista16RecordBean> r16sWrite) {
    // Sacamos las ordenes que no tienen 16
    @SuppressWarnings("unchecked")
    Collection<Tmct0bokId> sinR16 = CollectionUtils.disjunction(bookingsWrite, bookingsR16);
    boolean noEmptySinR16 = CollectionUtils.isNotEmpty(sinR16);
    bookingsR16.clear();

    LOG.info("[aniadirR16ToWrite] Añadiendo los registros 16 a la lista para escribir...");
    if (noEmptySinR16) {
      LOG.warn("[aniadirR16ToWrite] Las ordenes no tienen R16 : {}", sinR16);
      Tmct0bokId id;
      for (ConfirmacionMinorista16RecordBean confirmacionMinorista16RecordBean : r16s) {
        id = new Tmct0bokId(confirmacionMinorista16RecordBean.getNbooking1(),
                            confirmacionMinorista16RecordBean.getNuorden());
        if (!sinR16.contains(id)) {
          r16sWrite.add(confirmacionMinorista16RecordBean);
        }
      }
      sinR16.clear();
    } else {
      r16sWrite.addAll(r16s);
    }

    r16s.clear();
  }

  /**
   * Añade la lista r17s a la lista r17sWrite si hay que enviarlo
   * 
   * @param bookingsWrite bookings que se van a enviar
   * @param bookingsR17 ordenes con registro 17
   * @param r17s todos registros 17
   * @param r17sWrite registros 17 que se van a enviar
   */
  private void aniadirR17ToWrite(Collection<Tmct0bokId> bookingsWrite,
                                 Collection<Tmct0bokId> bookingsR17,
                                 List<ConfirmacionMinorista17RecordBean> r17s,
                                 List<ConfirmacionMinorista17RecordBean> r17sWrite) {
    // sacamos las ordenes que no tienen 17
    @SuppressWarnings("unchecked")
    Collection<Tmct0bokId> sinR17 = CollectionUtils.disjunction(bookingsWrite, bookingsR17);
    boolean noEmptySinR17 = CollectionUtils.isNotEmpty(sinR17);
    bookingsR17.clear();

    LOG.info("[aniadirR17ToWrite] Añadiendo los registros 17 a la lista para escribir...");
    if (noEmptySinR17) {
      LOG.warn("[aniadirR17ToWrite] Las ordenes no tienen R17 : {}", sinR17);
      Tmct0bokId id;
      for (ConfirmacionMinorista17RecordBean confirmacionMinorista17RecordBean : r17s) {
        id = new Tmct0bokId(confirmacionMinorista17RecordBean.getNbooking(),
                            confirmacionMinorista17RecordBean.getNuorden());
        if (!sinR17.contains(id)) {
          r17sWrite.add(confirmacionMinorista17RecordBean);
        }
      }
      sinR17.clear();
    } else {
      r17sWrite.addAll(r17s);
    }
    r17s.clear();
  }
}
