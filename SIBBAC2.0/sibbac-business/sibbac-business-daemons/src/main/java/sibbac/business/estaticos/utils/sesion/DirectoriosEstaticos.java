package sibbac.business.estaticos.utils.sesion;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DirectoriosEstaticos {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(DirectoriosEstaticos.class);

  @Value("${estaticos.folder.in:}")
  private String dirFicheros;

  @Value("${estaticos.folder.in.ok:}")
  private String dirOk;

  @Value("${estaticos.folder.in.error:}")
  private String dirError;

  @Value("${estaticos.fda.folder.out:}")
  private String dirFdaFidessa;

  /**
   * Instantiates a new directorios.
   * 
   * 
   */
  public DirectoriosEstaticos() throws Exception {

  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the dir in.
   * 
   * @return the dir in
   */
  public String getDirIn() {
    return dirFicheros;
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the dir ok.
   * 
   * @return the dir ok
   */
  public String getDirOk() {
    return dirOk;
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the dir error.
   * 
   * @return the dir error
   */
  public String getDirError() {
    return dirError;
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Gets the dir fi.
   * 
   * @return the dir fi
   */
  public String getDirFi() {
    return dirFdaFidessa;
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Mover dir ok.
   * 
   * @param nombreXML the nombre xml
   */
  public void moverDirOk(String nombreXML) {
    Path origen = Paths.get(getDirIn(), nombreXML);
    Path destino = Paths.get(getDirOk(), nombreXML);
    try {
      Files.move(origen, destino, StandardCopyOption.ATOMIC_MOVE);
    }
    catch (Exception e) {
      logger.warn("No se ha podido mover el archivo XML {} a la carpeta ok", nombreXML);
      logger.warn(e.getMessage(), e);
    }
  }

  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

  /**
   * Mover dir error.
   * 
   * @param nombreXML the nombre xml
   */
  public void moverDirError(String nombreXML) {
    Path origen = Paths.get(getDirIn(), nombreXML);
    Path destino = Paths.get(getDirError(), nombreXML);
    try {
      Files.move(origen, destino, StandardCopyOption.ATOMIC_MOVE);
    }
    catch (Exception e) {
      logger.warn("No se ha podido mover el archivo XML {} a la carpeta de errores", nombreXML);
      logger.warn(e.getMessage(), e);
    }
  }
  // --------------------------------------------------------------------------
  // --------------------------------------------------------------------------

}