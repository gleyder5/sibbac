package sibbac.business.estaticos.utils.conversiones;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.megbpdta.dao.Fmeg0cmgDao;
import sibbac.database.megbpdta.model.Fmeg0cmg;
import sibbac.database.megbpdta.model.Fmeg0cmgId;

/**
 * ConvertirMegaraAs400.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
@Service
public class ConvertirMegaraAs400 {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ConvertirMegaraAs400.class);

  @Autowired
  private Fmeg0cmgDao fmeg0cmgDao;

  /**
   * Exec convertir.
   * 
   * Realiza la conversion de datos entre Megara y el AS400.
   * 
   * @param idcamas4 the idcamas4
   * @param dsmegara the dsmegara
   * 
   * @return the string
   * 
   * @throws Exception the exception
   */
  @Transactional
  public String execConvertir(String idcamas4, String dsmegara) throws Exception {

    String dsaas400 = "";
    // List<Fmeg0cmg> list = fmeg0cmgDao.findAll();
    Fmeg0cmg objFmeg0cmg = fmeg0cmgDao.findOne(new Fmeg0cmgId(idcamas4.trim(), dsmegara.trim()));
    logger.debug("Inicio conversion de datos Megara - AS400, idcamas4= {}  dsmegara= {}", idcamas4, dsmegara);

    try {

      dsaas400 = objFmeg0cmg.getDsaas400().trim();

    }
    catch (Exception e) {

      logger.debug("Error conversion de datos Megara - AS400, idcamas4= {} dsmegara= {}. {}", idcamas4, dsmegara,
          e.getMessage());
      throw new Exception("Error conversion de datos Megara - AS400, idcamas4= " + idcamas4 + " dsmegara= " + dsmegara
          + ". " + e.getMessage(), e);

    }

    logger.debug("Fin conversion datos de Megara - AS400, dsaas400= {}", dsaas400);

    return dsaas400;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}