package sibbac.business.estaticos.megara.input;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import sibbac.business.estaticos.fidessa.fda.output.BroClientClientGroup;
import sibbac.business.estaticos.megara.inout.AliasOutPut;
import sibbac.business.estaticos.utils.conversiones.ConvertirBoolean;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0aliBo;
import sibbac.database.megbpdta.bo.Fmeg0bhlBo;
import sibbac.database.megbpdta.bo.Fmeg0brcBo;
import sibbac.database.megbpdta.model.Fmeg0ali;
import sibbac.database.megbpdta.model.Fmeg0bhl;
import sibbac.database.megbpdta.model.Fmeg0bhlId;
import sibbac.database.megbpdta.model.Fmeg0brc;

@Service
@Scope(value = "prototype")
public class BroClient {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(BroClient.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0brcBo fmeg0brcBo;

  @Autowired
  private Fmeg0bhlBo fmeg0bhlBo;

  @Autowired
  private Fmeg0aliBo fmeg0ali;

  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;

  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private static List<String> atrData;

  private static List<String> atrHolder;

  private Document document;

  private Fmeg0brc objFmeg0brc;

  public BroClient(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("identifier");
    atrData.add("activeClient");
    atrData.add("deactivationPeriodicity");
    atrData.add("creationRequest");
    atrData.add("description");
    atrData.add("hasAuthorization");
    atrData.add("hasRiskAuthorization");
    atrData.add("residenceType");
    atrData.add("managementCategory");
    atrData.add("mediaOfOrigin");
    atrData.add("representant");
    atrData.add("office");
    atrData.add("deactivationMethod");
    atrData.add("deactivationType");
    atrData.add("fiscalCategory");
    atrData.add("maxGroupDepth");
    atrData.add("subjectToTax");
    atrData.add("bankReference");
    atrData.add("miFIDCategory");
    atrData.add("clientNature");

    atrHolder = new ArrayList<>();
    atrHolder.add("externalReference");
    atrHolder.add("isMain");
    atrHolder.add("participationPercentage");
    atrHolder.add("holder");
    atrHolder.add("holderType");

    objFmeg0brc = new Fmeg0brc();
    objFmeg0brc.setCdbrocli("");
    objFmeg0brc.setIsactcli("");
    objFmeg0brc.setTpdesper("");
    objFmeg0brc.setDescript("");
    objFmeg0brc.setIsauthor("");
    objFmeg0brc.setIsriskau("");
    objFmeg0brc.setTpreside("");
    objFmeg0brc.setTpmancat("");
    objFmeg0brc.setMdlorigi("");
    objFmeg0brc.setCdoffice("");
    objFmeg0brc.setTpdesmet("");
    objFmeg0brc.setTpdesact("");
    objFmeg0brc.setCtfiscal("");
    objFmeg0brc.setNumaxgro(new BigDecimal(0));
    objFmeg0brc.setIssubtax("");
    objFmeg0brc.setCdrefbnk("");
    objFmeg0brc.setCtgmifid("");
    objFmeg0brc.setTpclienn("");
    objFmeg0brc.setFhdebaja("");
    objFmeg0brc.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {

      if (INPUT.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        input();
      }
      else {
        delete();
      }

      logger.debug("Archivo XML tratado correctamente");
      RegistroControl registroControl = ctx.getBean(RegistroControl.class, "BroClient", objFmeg0brc.getCdbrocli(),
          nombreXML, "I", "Procesado fichero estatico", "BroClient", "MG");
      registroControl.write();
      directorios.moverDirOk(nombreXML);

      if (INPUT.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        try {
          BroClientClientGroup broClientClientGroup = ctx.getBean(BroClientClientGroup.class, objFmeg0brc);
          broClientClientGroup.execXML();

          /*
           * AML 12/09/11 Genera el fichero Client si existe el alias asociado
           * al broclient
           */
          generarClient();
        }
        catch (Exception e) {
        }
      }

    }
    catch (Exception e) {
      logger.info(e.getMessage(), e);
      RegistroControl registroControl = ctx.getBean(RegistroControl.class, "BroClient", objFmeg0brc.getCdbrocli(),
          nombreXML, "E", e.getMessage(), "BroClient", "MG");
      registroControl.write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void input() throws Exception {

    String objPrimaryKey;
    Fmeg0brc objFmeg0brc_pers = new Fmeg0brc();

    tratarCampos();

    objPrimaryKey = objFmeg0brc.getCdbrocli();

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    // transaccion

    try {
      tx.begin();

      objFmeg0brc_pers = fmeg0brcBo.findById(objPrimaryKey);

      if (objFmeg0brc_pers == null) {
        objFmeg0brc.setTpmodeid(INPUT);
        fmeg0brcBo.save(objFmeg0brc, true);

        tratarHijos();

        tx.commit();

      }

      else if ("".equals(objFmeg0brc_pers.getFhdebaja().trim())) {
        // UPDATE
        fmeg0brcBo.delete(objFmeg0brc_pers);
        objFmeg0brc.setTpmodeid(UPDATE);
        fmeg0brcBo.save(objFmeg0brc, true);

        tratarHijos();

        tx.commit();
      }
      else {

        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0brcBo.delete(objFmeg0brc_pers);
        objFmeg0brc.setTpmodeid(INPUT);
        fmeg0brcBo.save(objFmeg0brc, true);

        tratarHijos();

        tx.commit();
      }

    }

    catch (Exception e) {
      throw new Exception(e.getMessage(), e);
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    String objPrimaryKey;
    Fmeg0brc objFmeg0brc_pers = new Fmeg0brc();

    tratarCampos();

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    objPrimaryKey = objFmeg0brc.getCdbrocli();

    // transaccion

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();
      objFmeg0brc_pers = fmeg0brcBo.findById(objPrimaryKey);
      if (objFmeg0brc_pers == null) {
        throw new Exception("No existe la identificacion de la entidad para darlo de baja");
      }
      else if ("".equals(objFmeg0brc_pers.getFhdebaja().trim())) {
        objFmeg0brc_pers.setFhdebaja(fechaSt);
        objFmeg0brc_pers.setTpmodeid(DELETE);
        objFmeg0brc = fmeg0brcBo.save(objFmeg0brc_pers, false);
      }
      else {
        throw new Exception("El registro en cuestion se encuentra en estado de 'BAJA LOGICA'");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void tratarCampos() throws DOMException, Exception {

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// identifier
            objFmeg0brc.setCdbrocli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// activeClient
            objFmeg0brc.setIsactcli(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 2:// deactivationPeriodicity
            objFmeg0brc.setTpdesper(convertirMegaraAs400.execConvertir("TPDESPER", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 3:// creationRequest
            // NO USAR, SEGUN ESPECIFICACIONES
            break;
          case 4:// description
            objFmeg0brc.setDescript(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 5:// hasAuthorization
            objFmeg0brc.setIsauthor(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 6:// hasRiskAuthorization
            objFmeg0brc.setIsriskau(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 7:// residenceType

            objFmeg0brc.setTpreside(convertirMegaraAs400.execConvertir("TPRESIDE", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 8:// managementCategory

            objFmeg0brc.setTpmancat(convertirMegaraAs400.execConvertir("TPMANCAT", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 9:// mediaOfOrigin
            objFmeg0brc.setMdlorigi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 10:// representant
            // NO USAR, SEGUN ESPECIFICACIONES
            break;
          case 11:// office
            objFmeg0brc.setCdoffice(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 12:// deactivationMethod

            objFmeg0brc.setTpdesmet(convertirMegaraAs400.execConvertir("TPDESMET", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 13:// deactivationType

            objFmeg0brc.setTpdesact(convertirMegaraAs400.execConvertir("TPDESACT", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 14:// fiscalCategory
            objFmeg0brc.setCtfiscal(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 15:// maxGroupDepth
            objFmeg0brc.setNumaxgro(new BigDecimal(0));
            break;
          case 16:// subjectToTax
            objFmeg0brc.setIssubtax(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 17:// bankReference
            objFmeg0brc.setCdrefbnk(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 18:// miFIDCategory

            objFmeg0brc.setCtgmifid(convertirMegaraAs400.execConvertir("CTGMIFID", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 19:// clientNature

            objFmeg0brc.setTpclienn(convertirMegaraAs400.execConvertir("TPCLIENN", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          default:
            ;
        }
      }
    }

  }

  private void tratarHijos() throws DOMException, Exception {

    for (int i = 0; i < (document.getElementsByTagName("holder").getLength()); i++) {

      if (document.getElementsByTagName("holder").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarHolder(((Element) document.getElementsByTagName("holder").item(i)));
      }

    }
  }

  private void tratarHolder(Node holder) throws DOMException, Exception {

    Fmeg0bhl objFmeg0bhl = new Fmeg0bhl();
    objFmeg0bhl.setId(new Fmeg0bhlId(objFmeg0brc.getCdbrocli(), null));
    objFmeg0bhl.setCdextref("");
    objFmeg0bhl.setIsmainal("");
    objFmeg0bhl.setPartperc("");
    objFmeg0bhl.setCdholder("");
    objFmeg0bhl.setTpholder("");

    for (int i = 0; i < holder.getChildNodes().getLength(); i++) {
      if ((holder.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(holder.getChildNodes().item(i).getNodeName()))) {

        switch (atrHolder.indexOf(((Element) holder.getChildNodes().item(i)).getAttribute("name"))) {

          case 0:// externalreference
            objFmeg0bhl.setCdextref(((Element) holder.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// isMain
            objFmeg0bhl.setIsmainal(new ConvertirBoolean().execConvertir(((Element) holder.getChildNodes().item(i))
                .getAttribute("value")));
            break;
          case 2:// participationPercentage
            objFmeg0bhl.setPartperc(((Element) holder.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 3:// holder
            objFmeg0bhl.setCdholder(((Element) holder.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 4:// holderType
            objFmeg0bhl.setTpholder(convertirMegaraAs400.execConvertir("TPHOLDER", ((Element) holder.getChildNodes()
                .item(i)).getAttribute("value")));
            break;
          default:
            ;
        }
      }
    }

    objFmeg0bhl.setFmeg0brc(objFmeg0brc);
    fmeg0bhlBo.save(objFmeg0bhl, true);

  }

  /*
   * AML 12/09/11 M�todo para generar el fichero de Client si existe el Alias*
   */

  private void generarClient() {

    final List<Fmeg0ali> objFmeg0aliList = getFmeg0Ali();

    try {
      if (objFmeg0aliList != null) {
        final int size = objFmeg0aliList.size();

        for (int i = 0; i < size; i++) {
          final Fmeg0ali objFmeg0Ali = (Fmeg0ali) objFmeg0aliList.get(i);

          // AML 08/02/2012
          // Genera todos los ficheros asociados al alias, CLIENT y SETT MAP

          // new AliasClient(objFmeg0Ali).execXML();
          AliasOutPut aliasOutput = ctx.getBean(AliasOutPut.class, objFmeg0Ali);
          aliasOutput.execXML();
        }
      }
    }
    catch (Exception e) {
      logger.warn("broclient: no se encuentra correspondencia en la Fmeg0Ali para el broclient por el motivo  "
          + e.getMessage());
    }

  }

  /*
   * AML 12/09/11 M�todo para obtener el alias a partir del broclient*
   */

  private List<Fmeg0ali> getFmeg0Ali() {
    List<Fmeg0ali> objFmeg0AliList = null;

    final String cdBrocli = objFmeg0brc.getCdbrocli().trim();

    if (cdBrocli != null) {
      // SesionMegbtdta.getPm().flush();
      EntityManager em = emf.createEntityManager();
      EntityTransaction tx = em.getTransaction();
      tx.begin();

      try {
        List<Fmeg0ali> listFmeg0Brc = fmeg0ali.findAllByCdbrocliAndTpmodeidDistinctD(cdBrocli);
        objFmeg0AliList = new ArrayList<>();

        final int size = listFmeg0Brc.size();

        for (int i = 0; i < size; i++) {
          final Fmeg0ali objFmeg0Ali = listFmeg0Brc.get(i);
          objFmeg0Ali.setTpmodeid("U");
          objFmeg0AliList.add(objFmeg0Ali);
          fmeg0ali.save(objFmeg0Ali, false);
        }

        tx.commit();

      }
      catch (Exception e) {

        logger.warn("broclient: no se encuentra correspondencia en la Fmeg0Ali para el broclient:  " + cdBrocli);
      }
      finally {
        if (tx.isActive()) {
          tx.rollback();
        }
        em.close();
      }
    }
    return objFmeg0AliList;
  }
}