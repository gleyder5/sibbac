package sibbac.business.estaticos;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.megara.bo.AnalyseDirIn;

/*
 * Clase principal MEGBTJAV
 * 
 * Inicializa logger, 
 * configura el gestor de persistencia 
 * y los directorios que usa para el tratamiento de los archivos
 * 
 * Crea un thread LookDirIn que cada 'x' tiempo mira si 
 * han llegado nuevos ficheros xml al directorio de entrada
 * en cuyo caso inicia su analisis
 * 
 */

@Service
public class EstaticosMegaraInputBo {

  private final static org.slf4j.Logger logger = LoggerFactory.getLogger(EstaticosMegaraInputBo.class);

  @Autowired
  private ApplicationContext ctx;

  // --------------------------------------------------------------------------

  public void execute() {
    logger.info("inicio");
    AnalyseDirIn analyse = ctx.getBean(AnalyseDirIn.class);
    // ejecucion programa
    analyse.exec();
    logger.info("fin");

  }
}
