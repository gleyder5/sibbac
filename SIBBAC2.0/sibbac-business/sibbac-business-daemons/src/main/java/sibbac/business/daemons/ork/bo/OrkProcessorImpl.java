package sibbac.business.daemons.ork.bo;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import static java.lang.Thread.currentThread;
import static java.text.MessageFormat.format;
import static java.util.Objects.requireNonNull;
import static java.util.Collections.unmodifiableList;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.Executors.newSingleThreadExecutor;
import static java.util.concurrent.TimeUnit.MINUTES;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import sibbac.business.daemons.ork.common.OrkFiles;
import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.business.daemons.ork.dao.OrkBmeFieldDao;
import sibbac.business.daemons.ork.model.OrkBmeField;
import sibbac.business.daemons.ork.model.OrkBmeQuery;
import sibbac.business.daemons.ork.model.OrkBmeQueryParam;
import sibbac.business.daemons.ork.model.OrkBmeSearch;
import sibbac.business.daemons.ork.model.OrkBmeSearchedField;
import sibbac.common.SIBBACBusinessException;

@Component
@Scope(SCOPE_PROTOTYPE)
public class OrkProcessorImpl implements OrkProcessor {
	
	private static final Logger LOG = getLogger(OrkProcessorImpl.class);
	
	private final AtomicInteger consultados;
	
	private final AtomicInteger mapeados;
	
  private final BlockingQueue<OrkPetition> resolvedOrkPetitions;
  
	private OrkMercadosConfig config;
	
	private ApplicationContext ctx;

	private DataSource ds;
	
	private BlockingQueue<OrkPetition> orkPetitions; 
	
	private OrkFidessaFieldCollection fieldCollection;
	
	private List<OrkBmeField> builders;
	
	private Map<String, Object[]> cache;
	
	private OrkFiles orkFiles;
	
	private Future<byte[]> hashFuture;
	
	/**
	 * Executor para realizar las consultas asíncronas
	 */
	private ExecutorService resolverExecutor;
	
	private ExecutorService writerExecutor;
	
	public OrkProcessorImpl() {
	  consultados = new AtomicInteger();
	  mapeados = new AtomicInteger();
    resolvedOrkPetitions = new LinkedTransferQueue<>();
	}

	@Override
  public void setCache(Map<String, Object[]> cache) {
  	this.cache = cache;
  }

  @Override
  public void setOrkFidessaFieldCollection(OrkFidessaFieldCollection fieldCollection) {
    this.fieldCollection = fieldCollection;
  }

  @Override
  public OrkInputIterator getInputIterator() throws SIBBACBusinessException {
    for(OrkBmeField field : builders) {
      field.loadProperties(config);
    }
  	buildResolverExecutor();
  	buildWriterExecutor();
  	return new OrkInputIterator(orkPetitions, requireNonNull(fieldCollection, 
  	    "No se ha suministrado la colección de campos"));
  }

  @Override
	public File getOrkFile() throws InterruptedException, SIBBACBusinessException {
		final byte[] hash;
		String message;
		
		orkPetitions.put(OrkPetition.endPetition());
		resolverExecutor.awaitTermination(config.getLecturaTimeout(), MINUTES);
		resolvedOrkPetitions.put(OrkPetition.endPetition());
		try {
			hash = hashFuture.get(config.getEscrituraTimeout(), MINUTES);
			shutdown();
			orkFiles.moveOrkFile(hash);
	    LOG.info("[getOrkFile] Consultados: {}, de mapa: {}", consultados.get(), mapeados.get());
			return orkFiles.getUpdateInfoFile();
		}
		catch(ExecutionException eex) {
			message = format("Se ha capturado error de ejecución en la escritura del fichero ork temporal {0}",
					orkFiles.toString());
			LOG.error("[getOrkFile] {}", message, eex.getCause());
			throw new SIBBACBusinessException(message, eex.getCause());
		}
		catch (TimeoutException e) {
			hashFuture.cancel(true);
			shutdown();
			message = format("Timeout de escritura de fichero {0}. Se aborta la operacion", orkFiles.toString());
			LOG.error("[getOrkFile] {}", message, e);
			throw new SIBBACBusinessException(message, e);
		}
	}
	
	Object[] resolve(OrkBmeSearchedField field, OrkPetition currentPetition) {    
    final OrkBmeSearch orkBmeSearch;
    final String key;
    OrkBmeQuery orkBmeQuery = null;
  	Object[] response, paramValues = null;
  	
  	orkBmeSearch = field.getOrkBmeSearch();
  	LOG.trace("[resolve] Se solicita la busqueda {} con los parametros {}", orkBmeSearch.getName());
  	if(orkBmeSearch.isCacheable()) {
  	  paramValues = currentPetition.getParamValues(orkBmeSearch.getParamNames());
  	  if(paramValues != null && paramValues.length > 0) {
        key = orkBmeSearch.getKey(paramValues);
        if(cache.containsKey(key)) {
          mapeados.incrementAndGet();
          return cache.get(key);
        }
  	  }
  	  else {
  	    key = null;
  	  }
  	}
  	else {
  	  key = null;
  	}
  	try(Connection con = ds.getConnection()) {
      con.setReadOnly(true);
      orkBmeQuery = orkBmeSearch.getQuery();
      try {
        do {
          paramValues = currentPetition.getParamValues(orkBmeQuery.getParamNames());
          response = resolveQuery(con, orkBmeQuery, paramValues);
          if(response != null) {
            if(key != null && !cache.containsKey(key)) { //Si se ha generado llave
              LOG.debug("[resolve] Se guarda la clave [{}] en cache", key);
              cache.put(key, response);
            }
            consultados.incrementAndGet();
            return response;
          }
        }
        while((orkBmeQuery = orkBmeQuery.getFail()) != null);
      }
      finally {
        con.commit();
      }
  	}
    catch(SQLException | RuntimeException sqlex) {
      if(orkBmeQuery != null) {
        LOG.error("[resolve] Error en la ejecución de una subconsulta Ork {} con parametros {}",
            orkBmeQuery.getName(), paramValues, sqlex);
      }
      else {
        LOG.error("[resolve] Error intentando resolver el campo {}", field.getName(), sqlex);
      }
    }
    return new Object[0];
  }

  OrkPetition takePetition() throws InterruptedException {
    synchronized (orkPetitions) {
      LOG.trace("[takePetition] peticion en espera");
      return orkPetitions.take();
    }
  }

  void putPetition(OrkPetition petition) throws InterruptedException {
    orkPetitions.put(petition);
  }

  void putResolvedPetition(OrkPetition petition) throws InterruptedException {
    resolvedOrkPetitions.put(petition);
  }

  @PostConstruct
	void init() {
		orkPetitions = new ArrayBlockingQueue<>(config.getMaxPetitions());
	}
		
	@Autowired
	void setOrkConfig(OrkMercadosConfig config) {
		this.config = config;
	}
	
	@Autowired
	void setDataSource(DataSource ds) {
		this.ds = ds;
	}

	@Autowired
	void setApplicationContext(ApplicationContext ctx) {
		this.ctx = ctx;
	}

	@Autowired
	void setOrkOutputBuilderDao(OrkBmeFieldDao orkOutputBuilderDao) {
		builders = unmodifiableList(orkOutputBuilderDao.findAllFetchedParams());
	}
	
	private void buildResolverExecutor() {
		final int maxThreads;
		OrkResolver orkResolver;
		
		maxThreads = config.getMaxThreads();
		resolverExecutor = newFixedThreadPool(maxThreads, new ResolverThreadFactory("Res"));
		for(int i = 0; i < maxThreads; i++) {
			orkResolver = new OrkResolver();
			orkResolver.setOrkProcessor(this);
			orkResolver.setOutputBuilders(builders);
			resolverExecutor.submit(orkResolver);
		}
		resolverExecutor.shutdown();
	}

	private void buildWriterExecutor() throws SIBBACBusinessException {
		final OrkWriter writer;
		final String writerThreadName;
		final int seq;
		final OrkBmeBo orkBmeBo;
		
		orkBmeBo = ctx.getBean(OrkBmeBo.class);
		seq = orkBmeBo.getSequence();
		orkFiles = config.getOrkFiles(seq);
		writer = new OrkWriter(orkFiles, seq, resolvedOrkPetitions, builders);
		writerThreadName = format("{0}_Writer", currentThread().getName());
		writerExecutor = newSingleThreadExecutor(new SingleThreadFactory(writerThreadName));
		hashFuture = writerExecutor.submit(writer);
		writerExecutor.shutdown();
	}
	
	private void shutdown() {
		if(cache != null) {
			cache.clear();
			cache = null;
		}
		LOG.info("[shutdown] Estado de los ejecutores: writer: {}, resolved: {}", writerExecutor.isTerminated(),
				resolverExecutor.isTerminated());
	}

	private Object[] resolveQuery(Connection con, OrkBmeQuery orkBmeQuery, Object[] paramValues) {
    final ResultSetMetaData md;
    final Object[] response;
    final StringBuilder sqlBuilder;
    final String query;
    final int size;
    int i = 0;

    sqlBuilder = new StringBuilder(orkBmeQuery.getFinalQuery());
    for(OrkBmeQueryParam p : orkBmeQuery.getParams()) {
      p.fillParameter(sqlBuilder, paramValues[i++]);
    }
    query = sqlBuilder.toString();
    try(Statement pstmt = con.createStatement();
      ResultSet rs = pstmt.executeQuery(query)) {
			if(rs.next()) {
				md = rs.getMetaData();
				size = md.getColumnCount();
				response = new Object[size];
				for(i = 0; i < size; i++) {
					response[i] = rs.getObject(i + 1);
				}
				if(rs.next()) {
					LOG.warn("[resolve] La consulta ha generado más de una línea de resultados: ", query);
				}
				return response;
			}
		}
		catch(SQLException | RuntimeException sqlex) {
	    LOG.error("[resolveQuery] Error la consulta [{}] ", query, sqlex);
		}
		return null;
	}

}
