package sibbac.business.daemons.fichero.corretajes.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.daemons.fichero.corretajes.model.FicheroCC;

@Repository
public interface FicheroCCDao extends JpaRepository<FicheroCC, Long>, JpaSpecificationExecutor<FicheroCC>{
  
  
  @Query(value = "SELECT NEXT VALUE FOR ID_FICHERO_CC_SEQ FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  public BigDecimal getIdFicheroCcSequence();
}
