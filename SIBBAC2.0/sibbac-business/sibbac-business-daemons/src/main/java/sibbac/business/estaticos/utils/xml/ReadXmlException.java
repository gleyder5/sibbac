package sibbac.business.estaticos.utils.xml;

/**
 * @author user
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 * @viz.diagram ReadXmlException.tpx
 */
public class ReadXmlException extends Exception {

	/**
   * 
   */
  private static final long serialVersionUID = -3619233857686926356L;

  /**
	 * Constructor for ReadXmlException.
	 */
	public ReadXmlException() {
		super("Excepci�n al Leer documento XML ");
	}

	/**
	 * Constructor for ReadXmlException.
	 * @param s
	 */
	public ReadXmlException(String s) {
		super("Excepci�n al Leer documento XML " + s);
	}

}
