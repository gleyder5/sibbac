package sibbac.business.estaticos.utils.conversiones;

import org.slf4j.LoggerFactory;

/**
 * ConvertirBoolean.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class ConvertirBoolean {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ConvertirBoolean.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new convertir boolean.
   */
  public ConvertirBoolean() {

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Exec convertir.
   * 
   * Realiza una conversion de datos booleana si recibe como parametro "true" lo
   * transformara en el valor "1" y si recibe "false" o blanco, lo transforma en
   * el valor "0", para cualquier otro valor genera un error de transformacion.
   * 
   * @param entrada the entrada
   * 
   * @return the string
   * 
   * @throws Exception the exception
   */
  public String execConvertir(String entrada) throws Exception {

    logger.debug("Inicio conversion de datos boolean, entrada= " + entrada);

    String salida = "";

    if ("true".equals(entrada)) {
      salida = "1";
    }
    else {
      if (("false".equals(entrada)) || ("".equals(entrada.trim()))) {
        salida = "0";
      }
      else {
        logger.debug("Error conversion, imposible convertir: " + entrada + ", en '0' o '1'");
        throw new Exception("Error conversion, imposible convertir: " + entrada + ", en '0' o '1'");
      }
    }

    logger.debug("Fin conversion datos boolean salida= " + salida);

    return salida;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------
}