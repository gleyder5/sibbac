package sibbac.business.estaticos.megara.bo;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;

/*
 * Thread que cada x tiempo mira si hay ficheros xml en el directorio de entrada en caso de haberlos, 
 * los recoge en una lista y uno a uno lee los ficheros
 */
@Service
public class AnalyseDirIn {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(AnalyseDirIn.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private DirectoriosEstaticos directorios;

  // --------------------------------------------------------------------------

  public AnalyseDirIn() {
  }

  // --------------------------------------------------------------------------

  public void exec() {

    logger.info("Chequeo directorio de entrada");
    try {

      File[] listFiles = null;

      // creamos una lista con los ficheros xml que existen en el
      // directorio de entrada
      Path dirIn = Paths.get(directorios.getDirIn());
      if (Files.notExists(dirIn)) {
        Files.createDirectories(dirIn);
      }
      Path dirErr = Paths.get(directorios.getDirError());
      if (Files.notExists(dirErr)) {
        Files.createDirectories(dirErr);
      }
      Path dirFi = Paths.get(directorios.getDirFi());
      if (Files.notExists(dirFi)) {
        Files.createDirectories(dirFi);
      }
      Path dirOk = Paths.get(directorios.getDirOk());
      if (Files.notExists(dirOk)) {
        Files.createDirectories(dirOk);
      }

      directorios = ctx.getBean(DirectoriosEstaticos.class);

      listFiles = new File(directorios.getDirIn()).listFiles();
      Arrays.sort(listFiles);

      logger.info("Recuperados: {} archivos XML.", listFiles.length);

      // si hay ficheros los leemos
      for (int i = 0; i < listFiles.length; i++) {

        logger.info("");
        logger.info("Analisis del archivo: " + (i + 1) + ", " + listFiles[i].getName());
        try {
          if (listFiles[i].isFile()) {
            AnalyseFile analyseFile = ctx.getBean(AnalyseFile.class, listFiles[i].getName());
            analyseFile.processFile();
          }
        }
        catch (Exception e) {

          logger.debug("ARCHIVO XML CON ERROR DE FORMATO");
          logger.debug(e.getMessage(), e);
          RegistroControl registroControl = ctx.getBean(RegistroControl.class, "", "", listFiles[i].getName(), "E",
              "ARCHIVO XML CON ERROR DE FORMATO", "AnalyseDirIn", "MG");
          registroControl.write();
          directorios.moverDirError(listFiles[i].getName());

        }

      }

    }
    catch (Exception e) {
      logger.warn("Se ha producido un fallo en el acceso al directorio de entrada {}", e.getMessage(), e);
    }

  }

  // --------------------------------------------------------------------------

}
