package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * ChargeRulesMegaTrade.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class ChargeRulesMegaTrade extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ChargeRulesMegaTrade.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The charge_name. */
  private String charge_name;

  /** The description. */
  private String description;

  /** The charge_value. */
  private String charge_value;

  /** The charge_method. */
  private String charge_method;

  /** The per_amount_value. */
  private String per_amount_value;

  /** The charge_scale_name. */
  private String charge_scale_name;

  /** The exchange. */
  private String exchange;

  /** The counterparty_type. */
  private String counterparty_type;

  /** The counterparty_mnemonic. */
  private String counterparty_mnemonic;

  /** The account_mnemonic. */
  private String account_mnemonic;

  /** The charge_currency. */
  private String charge_currency;

  /** The minimum_charge. */
  private String minimum_charge;

  /** The maximum_charge. */
  private String maximum_charge;

  /** The effective_from. */
  private String effective_from;

  /** The effective_to. */
  private String effective_to;

  /** The market_type. */
  private String market_type;

  /** The instrument_type. */
  private String instrument_type;

  /** The firm_buy_sell. */
  private String firm_buy_sell;

  /** The isin. */
  private String isin;

  /** The origin. */
  private String origin;

  /** The channel. */
  private String channel;

  /** The handling_treatment. */
  private String handling_treatment;

  /** The communication_method. */
  private String communication_method;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new charge rules_megatrade.
   */
  protected ChargeRulesMegaTrade() {

    this.tipoActualizacion = "";
    this.charge_name = "";
    this.description = "";
    this.charge_value = "";
    this.charge_method = "";
    this.per_amount_value = "";
    this.charge_scale_name = "";
    this.exchange = "";
    this.counterparty_type = "";
    this.counterparty_mnemonic = "";
    this.account_mnemonic = "";
    this.charge_currency = "";
    this.minimum_charge = "";
    this.maximum_charge = "";
    this.effective_from = "";
    this.effective_to = "";
    this.market_type = "";
    this.instrument_type = "";
    this.firm_buy_sell = "";
    this.isin = "";
    this.origin = "";
    this.channel = "";
    this.handling_treatment = "";
    this.communication_method = "";
    this.nombreXML = "";
    this.fecha = new Date();
    this.mensajeError = "";

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("chargeRules: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element CHARGE_RULES = document.createElement("CHARGE_RULES");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion().trim()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element CHARGE_NAME = document.createElement("CHARGE_NAME");
      CHARGE_NAME.appendChild(document.createTextNode(getCharge_name()));

      Element DESCRIPTION = document.createElement("DESCRIPTION");
      DESCRIPTION.appendChild(document.createTextNode(getDescription()));

      Element CHARGE_VALUE = document.createElement("CHARGE_VALUE");
      CHARGE_VALUE.appendChild(document.createTextNode(getCharge_value()));

      Element CHARGE_METHOD = document.createElement("CHARGE_METHOD");
      CHARGE_METHOD.appendChild(document.createTextNode(getCharge_method()));

      Element PER_AMOUNT_VALUE = document.createElement("PER_AMOUNT_VALUE");
      PER_AMOUNT_VALUE.appendChild(document.createTextNode(getPer_amount_value()));

      Element CHARGE_SCALE_NAME = document.createElement("CHARGE_SCALE_NAME");
      CHARGE_SCALE_NAME.appendChild(document.createTextNode(getCharge_scale_name()));

      Element EXCHANGE = document.createElement("EXCHANGE");
      EXCHANGE.appendChild(document.createTextNode(getExchange()));

      Element TRANSACTION_TYPE = document.createElement("TRANSACTION_TYPE");

      Element COUNTERPARTY_TYPE = document.createElement("COUNTERPARTY_TYPE");
      COUNTERPARTY_TYPE.appendChild(document.createTextNode(getCounterparty_type()));

      Element COUNTERPARTY_MNEMONIC = document.createElement("COUNTERPARTY_MNEMONIC");
      COUNTERPARTY_MNEMONIC.appendChild(document.createTextNode(getCounterparty_mnemonic()));

      Element ACCOUNT_MNEMONIC = document.createElement("ACCOUNT_MNEMONIC");
      ACCOUNT_MNEMONIC.appendChild(document.createTextNode(getAccount_mnemonic()));

      Element CHARGE_CURRENCY = document.createElement("CHARGE_CURRENCY");
      CHARGE_CURRENCY.appendChild(document.createTextNode(getCharge_currency()));

      Element CHARGE_BASIS = document.createElement("CHARGE_BASIS");
      CHARGE_BASIS.appendChild(document.createTextNode("GROSS_CONSIDERATION"));

      Element ROUNDING = document.createElement("ROUNDING");
      ROUNDING.appendChild(document.createTextNode("N"));

      Element ROUNDING_PRECISION = document.createElement("ROUNDING_PRECISION");

      Element MINIMUM_CHARGE = document.createElement("MINIMUM_CHARGE");
      MINIMUM_CHARGE.appendChild(document.createTextNode(getMinimum_charge()));

      Element MAXIMUM_CHARGE = document.createElement("MAXIMUM_CHARGE");
      MAXIMUM_CHARGE.appendChild(document.createTextNode(getMaximum_charge()));

      Element PAID_RECEIVED = document.createElement("PAID_RECEIVED");

      Element INCLUSIVE = document.createElement("INCLUSIVE");

      Element DEFAULT_RULE = document.createElement("DEFAULT_RULE");

      Element EXACT_MATCH = document.createElement("EXACT_MATCH");

      Element HANDLING_INSTRUCTIONS = document.createElement("HANDLING_INSTRUCTIONS");

      Element CLIENT_LIST_NAME = document.createElement("CLIENT_LIST_NAME");

      Element EFFECTIVE_FROM = document.createElement("EFFECTIVE_FROM");
      EFFECTIVE_FROM.appendChild(document.createTextNode(getEffective_from()));

      Element EFFECTIVE_TO = document.createElement("EFFECTIVE_TO");
      EFFECTIVE_TO.appendChild(document.createTextNode(getEffective_to()));

      Element MARKET = document.createElement("MARKET");

      Element MARKET_TYPE = document.createElement("MARKET_TYPE");
      MARKET_TYPE.appendChild(document.createTextNode(getMarket_type()));

      Element MARKET_SEGMENT = document.createElement("MARKET_SEGMENT");

      Element INSTRUMENT_TYPE = document.createElement("INSTRUMENT_TYPE");
      INSTRUMENT_TYPE.appendChild(document.createTextNode(getInstrument_type()));

      Element FIRM_BUY_SELL = document.createElement("FIRM_BUY_SELL");
      FIRM_BUY_SELL.appendChild(document.createTextNode(getFirm_buy_sell()));

      Element ISIN = document.createElement("ISIN");
      ISIN.appendChild(document.createTextNode(getIsin()));

      Element INSTRUMENT_LIST_NAME = document.createElement("INSTRUMENT_LIST_NAME");

      Element ORIGIN = document.createElement("ORIGIN");
      ORIGIN.appendChild(document.createTextNode(getOrigin()));

      Element CHANNEL = document.createElement("CHANNEL");
      CHANNEL.appendChild(document.createTextNode(getChannel()));

      Element HANDLING_TREATMENT = document.createElement("HANDLING_TREATMENT");
      HANDLING_TREATMENT.appendChild(document.createTextNode(getHandling_treatment()));

      Element COMMUNICATION_METHOD = document.createElement("COMMUNICATION_METHOD");
      COMMUNICATION_METHOD.appendChild(document.createTextNode(getCommunication_method()));

      Element COUNTRY_OF_DOMICILE = document.createElement("COUNTRY_OF_DOMICILE");

      Element EXCHANGE_ROUTE = document.createElement("EXCHANGE_ROUTE");

      Element CCV = document.createElement("CCV");

      // ---------------------------------------------------------

      data.appendChild(CHARGE_NAME);
      data.appendChild(DESCRIPTION);
      data.appendChild(CHARGE_VALUE);
      data.appendChild(CHARGE_METHOD);
      data.appendChild(PER_AMOUNT_VALUE);
      data.appendChild(CHARGE_SCALE_NAME);
      data.appendChild(EXCHANGE);
      data.appendChild(TRANSACTION_TYPE);
      data.appendChild(COUNTERPARTY_TYPE);
      data.appendChild(COUNTERPARTY_MNEMONIC);
      data.appendChild(ACCOUNT_MNEMONIC);
      data.appendChild(CHARGE_CURRENCY);
      data.appendChild(CHARGE_BASIS);
      data.appendChild(ROUNDING);
      data.appendChild(ROUNDING_PRECISION);
      data.appendChild(MINIMUM_CHARGE);
      data.appendChild(MAXIMUM_CHARGE);
      data.appendChild(PAID_RECEIVED);
      data.appendChild(INCLUSIVE);
      data.appendChild(DEFAULT_RULE);
      data.appendChild(EXACT_MATCH);
      data.appendChild(HANDLING_INSTRUCTIONS);
      data.appendChild(CLIENT_LIST_NAME);
      data.appendChild(EFFECTIVE_FROM);
      data.appendChild(EFFECTIVE_TO);
      data.appendChild(MARKET);
      data.appendChild(MARKET_TYPE);
      data.appendChild(MARKET_SEGMENT);
      data.appendChild(INSTRUMENT_TYPE);
      data.appendChild(FIRM_BUY_SELL);
      data.appendChild(ISIN);
      data.appendChild(INSTRUMENT_LIST_NAME);
      data.appendChild(ORIGIN);
      data.appendChild(CHANNEL);
      data.appendChild(HANDLING_TREATMENT);
      data.appendChild(COMMUNICATION_METHOD);
      data.appendChild(COUNTRY_OF_DOMICILE);
      data.appendChild(EXCHANGE_ROUTE);
      data.appendChild(CCV);

      // ---------------------------------------------------------

      CHARGE_RULES.appendChild(header);
      CHARGE_RULES.appendChild(data);

      document.appendChild(CHARGE_RULES);

      logger.debug("chargeRules: estructura documento XML completada");

      // ---------------------------------------------------------

      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("chargeRules: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("chargeRules: error en la generacion del docuemento XML");
      logger.warn("chargeRules: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a charge rules";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a charge rules";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a charge rules";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("chargeRules: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, KCMMdd0000.xml donde
   * KC es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getChargeRules();

    Path path = Paths.get(getRutaDestino(), String.format("KC%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("ChargeRules: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the account_mnemonic.
   * 
   * @return the account_mnemonic
   */
  protected String getAccount_mnemonic() {
    return account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the account_mnemonic.
   * 
   * @param account_mnemonic the new account_mnemonic
   */
  protected void setAccount_mnemonic(String account_mnemonic) {
    this.account_mnemonic = account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the channel.
   * 
   * @return the channel
   */
  protected String getChannel() {
    return channel;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the channel.
   * 
   * @param channel the new channel
   */
  protected void setChannel(String channel) {
    this.channel = channel;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the charge_currency.
   * 
   * @return the charge_currency
   */
  protected String getCharge_currency() {
    return charge_currency;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the charge_currency.
   * 
   * @param charge_currency the new charge_currency
   */
  protected void setCharge_currency(String charge_currency) {
    this.charge_currency = charge_currency;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the charge_method.
   * 
   * @return the charge_method
   */
  protected String getCharge_method() {
    return charge_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the charge_method.
   * 
   * @param charge_method the new charge_method
   */
  protected void setCharge_method(String charge_method) {
    this.charge_method = charge_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the charge_name.
   * 
   * @return the charge_name
   */
  protected String getCharge_name() {
    return charge_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the charge_name.
   * 
   * @param charge_name the new charge_name
   */
  protected void setCharge_name(String charge_name) {
    this.charge_name = charge_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the charge_scale_name.
   * 
   * @return the charge_scale_name
   */
  protected String getCharge_scale_name() {
    return charge_scale_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the charge_scale_name.
   * 
   * @param charge_scale_name the new charge_scale_name
   */
  protected void setCharge_scale_name(String charge_scale_name) {
    this.charge_scale_name = charge_scale_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the charge_value.
   * 
   * @return the charge_value
   */
  protected String getCharge_value() {
    return charge_value;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the charge_value.
   * 
   * @param charge_value the new charge_value
   */
  protected void setCharge_value(String charge_value) {
    this.charge_value = charge_value;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the communication_method.
   * 
   * @return the communication_method
   */
  protected String getCommunication_method() {
    return communication_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the communication_method.
   * 
   * @param communication_method the new communication_method
   */
  protected void setCommunication_method(String communication_method) {
    this.communication_method = communication_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the counterparty_mnemonic.
   * 
   * @return the counterparty_mnemonic
   */
  protected String getCounterparty_mnemonic() {
    return counterparty_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the counterparty_mnemonic.
   * 
   * @param counterparty_mnemonic the new counterparty_mnemonic
   */
  protected void setCounterparty_mnemonic(String counterparty_mnemonic) {
    this.counterparty_mnemonic = counterparty_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the counterparty_type.
   * 
   * @return the counterparty_type
   */
  protected String getCounterparty_type() {
    return counterparty_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the counterparty_type.
   * 
   * @param counterparty_type the new counterparty_type
   */
  protected void setCounterparty_type(String counterparty_type) {
    this.counterparty_type = counterparty_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the description.
   * 
   * @return the description
   */
  protected String getDescription() {
    return description;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the description.
   * 
   * @param description the new description
   */
  protected void setDescription(String description) {
    this.description = description;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the effective_from.
   * 
   * @return the effective_from
   */
  protected String getEffective_from() {
    return effective_from;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the effective_from.
   * 
   * @param effective_from the new effective_from
   */
  protected void setEffective_from(String effective_from) {
    this.effective_from = effective_from;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the effective_to.
   * 
   * @return the effective_to
   */
  protected String getEffective_to() {
    return effective_to;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the effective_to.
   * 
   * @param effective_to the new effective_to
   */
  protected void setEffective_to(String effective_to) {
    this.effective_to = effective_to;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the exchange.
   * 
   * @return the exchange
   */
  protected String getExchange() {
    return exchange;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the exchange.
   * 
   * @param exchange the new exchange
   */
  protected void setExchange(String exchange) {
    this.exchange = exchange;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the firm_buy_sell.
   * 
   * @return the firm_buy_sell
   */
  protected String getFirm_buy_sell() {
    return firm_buy_sell;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the firm_buy_sell.
   * 
   * @param firm_buy_sell the new firm_buy_sell
   */
  protected void setFirm_buy_sell(String firm_buy_sell) {
    this.firm_buy_sell = firm_buy_sell;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the handling_treatment.
   * 
   * @return the handling_treatment
   */
  protected String getHandling_treatment() {
    return handling_treatment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the handling_treatment.
   * 
   * @param handling_treatment the new handling_treatment
   */
  protected void setHandling_treatment(String handling_treatment) {
    this.handling_treatment = handling_treatment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the instrument_type.
   * 
   * @return the instrument_type
   */
  protected String getInstrument_type() {
    return instrument_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the instrument_type.
   * 
   * @param instrument_type the new instrument_type
   */
  protected void setInstrument_type(String instrument_type) {
    this.instrument_type = instrument_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the isin.
   * 
   * @return the isin
   */
  protected String getIsin() {
    return isin;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the isin.
   * 
   * @param isin the new isin
   */
  protected void setIsin(String isin) {
    this.isin = isin;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the market_type.
   * 
   * @return the market_type
   */
  protected String getMarket_type() {
    return market_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the market_type.
   * 
   * @param market_type the new market_type
   */
  protected void setMarket_type(String market_type) {
    this.market_type = market_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the maximum_charge.
   * 
   * @return the maximum_charge
   */
  protected String getMaximum_charge() {
    return maximum_charge;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the maximum_charge.
   * 
   * @param maximum_charge the new maximum_charge
   */
  protected void setMaximum_charge(String maximum_charge) {
    this.maximum_charge = maximum_charge;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the minimum_charge.
   * 
   * @return the minimum_charge
   */
  protected String getMinimum_charge() {
    return minimum_charge;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the minimum_charge.
   * 
   * @param minimum_charge the new minimum_charge
   */
  protected void setMinimum_charge(String minimum_charge) {
    this.minimum_charge = minimum_charge;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the origin.
   * 
   * @return the origin
   */
  protected String getOrigin() {
    return origin;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the origin.
   * 
   * @param origin the new origin
   */
  protected void setOrigin(String origin) {
    this.origin = origin;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the per_amount_value.
   * 
   * @return the per_amount_value
   */
  protected String getPer_amount_value() {
    return per_amount_value;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the per_amount_value.
   * 
   * @param per_amount_value the new per_amount_value
   */
  protected void setPer_amount_value(String per_amount_value) {
    this.per_amount_value = per_amount_value;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}