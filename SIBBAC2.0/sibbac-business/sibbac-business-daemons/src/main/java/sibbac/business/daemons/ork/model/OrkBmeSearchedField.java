package sibbac.business.daemons.ork.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.business.daemons.ork.common.ResolvedValue;
import sibbac.common.SIBBACBusinessException;

/**
 * Entidad que representa un campo del fichero ORK que es resultado de una búsqueda 
 * @author XI326526
 */
@Entity
@Table(name = "TMCT0_ORK_BME_SEARCHED_FIELD")
public class OrkBmeSearchedField extends OrkBmeField {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 2725871830714998428L;
  
  @OneToOne(optional = false)
  @JoinColumn(name = "SEARCH", nullable = false)
  private OrkBmeSearch orkBmeSearch;

  /**
   * @return búsqueda que proporciona valor a este campo.
   */
  public OrkBmeSearch getOrkBmeSearch() {
    return orkBmeSearch;
  }

  /**
   * @param orkBmeSearch Determina la búsqueda que proporciona valor a este campo
   */
  public void setOrkBmeSearch(OrkBmeSearch orkBmeSearch) {
    this.orkBmeSearch = orkBmeSearch;
  }

  /**
   * {@inheritDoc} Recupera el valor correspondiente a este campo por nombre
   */
  @Override
  @Transient
  public Object[] getSolvedData(ResolvedValue rv) {
    return rv.getResolvedData(getName());
  }
  
  /**
   * {@inheritDoc} Ejecuta el método {@link OrkBmeSearch#loadProperties(OrkMercadosConfig)}
   */
  @Override
  @Transient
  public void loadProperties(OrkMercadosConfig config) throws SIBBACBusinessException {
    orkBmeSearch.loadProperties(config);
  }

}
