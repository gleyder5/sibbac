package sibbac.business.estaticos.megara.input;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import sibbac.business.estaticos.utils.conversiones.ConvertirBoolean;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0bbmBo;
import sibbac.database.megbpdta.bo.Fmeg0bseBo;
import sibbac.database.megbpdta.model.Fmeg0bbm;
import sibbac.database.megbpdta.model.Fmeg0bbmId;
import sibbac.database.megbpdta.model.Fmeg0bse;
import sibbac.database.megbpdta.model.Fmeg0bseId;

@Service
@Scope(value = "prototype")
public class BrokerByStockExchange {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(BrokerByStockExchange.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;
  
  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;

  @Autowired
  private Fmeg0bseBo fmeg0bseBo;

  @Autowired
  private Fmeg0bbmBo fmeg0bbmBo;
  
  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private static List<String> atrData;

  private static List<String> atrBrokerMedia;

  private Document document;

  private Fmeg0bse objFmeg0bse;

  public BrokerByStockExchange(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("stockExchange");
    atrData.add("broker");
    atrData.add("language");
    atrData.add("billMarketCharge");
    atrData.add("identifier");
    atrData.add("isActive");
    atrData.add("isDefaultBroker");
    atrData.add("maxValidityDay");

    atrBrokerMedia = new ArrayList<>();
    atrBrokerMedia.add("active");
    atrBrokerMedia.add("preferenceOrder");
    atrBrokerMedia.add("media");

    objFmeg0bse = new Fmeg0bse();
    objFmeg0bse.setId(new Fmeg0bseId("", ""));
    objFmeg0bse.setCdlangua("");
    objFmeg0bse.setBillmark("");
    objFmeg0bse.setIdbystoc("");
    objFmeg0bse.setIsactive("");
    objFmeg0bse.setIsdefbro("");
    objFmeg0bse.setMaxvaday(new BigDecimal(0));
    objFmeg0bse.setFhdebaja("");
    objFmeg0bse.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {

      if (INPUT.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        input();
      }
      else {
        delete();
      }

      logger.debug("Archivo XML tratado correctamente");
      ctx.getBean(RegistroControl.class, "Broker By Stock Exchange",
          objFmeg0bse.getId().getCdstoexc() + objFmeg0bse.getId().getCdbroker(), nombreXML, "I", "Procesado fichero estatico",
          "BrokerByStockExchange", "MG").write();
      directorios.moverDirOk(nombreXML);

    }
    catch (Exception e) {
      logger.info(e.getMessage());
      ctx.getBean(RegistroControl.class, "Broker By Stock Exchange",
          objFmeg0bse.getId().getCdstoexc() + objFmeg0bse.getId().getCdbroker(), nombreXML, "E", e.getMessage(),
          "BrokerByStockExchange", "MG").write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void input() throws Exception {

    Fmeg0bse objFmeg0bse_pers = new Fmeg0bse();

    tratarCampos();

    Fmeg0bseId objPrimaryKey = new Fmeg0bseId(objFmeg0bse.getId().getCdstoexc(), objFmeg0bse.getId().getCdbroker());

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0bse_pers = fmeg0bseBo.findById(objPrimaryKey);
      if (objFmeg0bse_pers == null) {
        // INSERT
        objFmeg0bse.setTpmodeid(INPUT);
        fmeg0bseBo.save(objFmeg0bse, true);

        tratarHijos();

        tx.commit();
      }
      else if ("".equals(objFmeg0bse_pers.getFhdebaja().trim())) {
        // UPDATE
        fmeg0bseBo.delete(objFmeg0bse_pers);

        objFmeg0bse.setTpmodeid(UPDATE);
        fmeg0bseBo.save(objFmeg0bse, true);

        tratarHijos();

        tx.commit();
      }
      else {

        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0bseBo.delete(objFmeg0bse_pers);
        objFmeg0bse.setTpmodeid(INPUT);
        fmeg0bseBo.save(objFmeg0bse, true);

        tratarHijos();

        tx.commit();
      }

    }
    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    Fmeg0bse objFmeg0bse_pers = new Fmeg0bse();

    tratarCampos();

    Fmeg0bseId objPrimaryKey = new Fmeg0bseId(objFmeg0bse.getId().getCdstoexc(), objFmeg0bse.getId().getCdbroker());

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0bse_pers = fmeg0bseBo.findById(objPrimaryKey);
      if (objFmeg0bse_pers == null) {
        throw new Exception("No existe la identificacion de la entidad para darlo de baja");
      }
      else if ("".equals(objFmeg0bse_pers.getFhdebaja().trim())) {
        objFmeg0bse_pers.setFhdebaja(fechaSt);
        objFmeg0bse_pers.setTpmodeid(DELETE);
        objFmeg0bse = fmeg0bseBo.save(objFmeg0bse_pers, false);

      }
      else {
        throw new Exception("El registro en cuestion se encuentra en estado de 'BAJA LOGICA'");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void tratarCampos() throws DOMException, Exception {

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// stockExchange
            objFmeg0bse.getId().setCdstoexc(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// broker
            objFmeg0bse.getId().setCdbroker(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// language

            objFmeg0bse.setCdlangua((convertirMegaraAs400).execConvertir("CDLANGUA", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 3:// billMarketCharge
            objFmeg0bse.setBillmark(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 4:// identifier
            objFmeg0bse.setIdbystoc(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 5:// isActive
            objFmeg0bse.setIsactive(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 6:// isDefaultBroker
            objFmeg0bse.setIsdefbro(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 7:// maxValidityDay
            if ("".equals((((Element) dataElement.getChildNodes().item(i)).getAttribute("value")).trim())) {
              objFmeg0bse.setMaxvaday(new BigDecimal(0));
            }
            else {
              objFmeg0bse.setMaxvaday(new BigDecimal(((Element) dataElement.getChildNodes().item(i))
                  .getAttribute("value")));
            }
            break;

          default:
            ;
        }
      }
    }

  }

  private void tratarHijos() throws DOMException, Exception {

    for (int i = 0; i < (document.getElementsByTagName("brokerMedia").getLength()); i++) {

      if (document.getElementsByTagName("brokerMedia").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarBrokerMedia(((Element) document.getElementsByTagName("brokerMedia").item(i)));
      }

    }
  }

  private void tratarBrokerMedia(Element brokerMedia) throws Exception {

    Fmeg0bbm objFmeg0bbm = new Fmeg0bbm();
    objFmeg0bbm.setId(new Fmeg0bbmId(objFmeg0bse.getId().getCdstoexc(), objFmeg0bse.getId().getCdbroker(), null));
    objFmeg0bbm.setIsactive("");
    objFmeg0bbm.setPrefeord("");
    objFmeg0bbm.setIdmediaa("");

    for (int i = 0; i < brokerMedia.getChildNodes().getLength(); i++) {
      if ((brokerMedia.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(brokerMedia.getChildNodes().item(i).getNodeName()))) {

        switch (atrBrokerMedia.indexOf(((Element) brokerMedia.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// active
            objFmeg0bse.setIsactive(new ConvertirBoolean()
                .execConvertir(((Element) brokerMedia.getChildNodes().item(i)).getAttribute("value")));

            break;
          case 1:// preferenceOrder
            objFmeg0bbm.setPrefeord(((Element) brokerMedia.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// media
            objFmeg0bbm.setIdmediaa(((Element) brokerMedia.getChildNodes().item(i)).getAttribute("value"));
            break;

          default:
            ;
        }
      }

    }

    objFmeg0bbm.setFmeg0bse(objFmeg0bse);
    fmeg0bbmBo.save(objFmeg0bbm, true);
  }

}