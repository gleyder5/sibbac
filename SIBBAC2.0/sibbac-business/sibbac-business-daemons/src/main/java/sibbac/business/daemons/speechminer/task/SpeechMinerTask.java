package sibbac.business.daemons.speechminer.task;

import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import sibbac.business.fase0.database.dao.EstadosEnumerados;
import sibbac.business.daemons.speechminer.bo.SpeechMinerBo;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_FONECTIC.NAME, name = Task.GROUP_FONECTIC.JOB_GENERAR_REPORTE_SPEECHMINER,
        jobType = SIBBACJobType.CRON_JOB,
        cronExpression = "0 0 20 ? * MON-FRI"
        )
public class SpeechMinerTask extends WrapperTaskConcurrencyPrevent {


    @Autowired
    private SpeechMinerBo speechMinerBo;


    @Override
    public void executeTask() throws Exception {
		LOG.debug("[executeTask] Inicio de proceso SpeechMiner");
        this.generarFicheroSpeechminer();
        LOG.debug("[executeTask] Fin de proceso SpeechMiner");
    }

    @Override
    public EstadosEnumerados.TIPO determinarTipoApunte() {
        return EstadosEnumerados.TIPO_APUNTE.TASK_GENERAR_REPORTE_SPEECH_MINER;
    }

    public void generarFicheroSpeechminer() throws SIBBACBusinessException {
        speechMinerBo.generateFile();
    }	

}
