package sibbac.business.daemons.batch;

import java.sql.Date;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.business.daemons.batch.common.BatchException;
import static sibbac.common.utils.FormatDataUtils.convertDateTimeToString;
import static sibbac.common.utils.FormatDataUtils.convertDateToString;
import static sibbac.common.utils.FormatDataUtils.convertTimeToString;

abstract class DestinyExecutor {
  
  private static final int EACH_LINES = 500;
  
  private static final Logger LOG = LoggerFactory.getLogger(DestinyExecutor.class);
  
  private final AtomicInteger executions;
  
  private final AtomicInteger affectedRows;
  
  private final AtomicInteger returnedRows;
  
  DestinyExecutor() {
    executions = new AtomicInteger();
    affectedRows = new AtomicInteger();
    returnedRows = new AtomicInteger();
  }
  
  abstract void executeStatement(Connection con, String query) throws BatchException;
  
  void close() {
    LOG.debug("[logExecutions] Ejecuciones: {}, registros afectados: {}, registros consultados: {}",  
        executions, affectedRows, returnedRows); 
  }
  
  final String format(ResultSet rs, int pos, int type) throws SQLException {
    final Date date;

    switch(type) {
      case Types.DATE:
        date = rs.getDate(pos);
        if(rs.wasNull()) return null;
        return convertDateToString(date);
      case Types.TIME:
        date = rs.getDate(pos);
        if(rs.wasNull()) return null;
        return convertTimeToString(date);
      case Types.TIMESTAMP:
        date = rs.getDate(pos);
        if(rs.wasNull()) return null;
        return convertDateTimeToString(date);
      default:
        return rs.getString(pos);
    }
  }
  
  final void addAffectedRows(int rows) {
    affectedRows.addAndGet(rows);
    incExecutions();
  }
  
  final void addReturnedRows(int rows) {
    returnedRows.addAndGet(rows);
    incExecutions();
  }
    
  final int getAffectedRows() {
  	return affectedRows.get();
  }
  
  private void incExecutions() {
    final int ex;
    
    ex = executions.incrementAndGet();
    if(ex % EACH_LINES == 0) {
      LOG.info("Ejecuciones {}, actualizaciones {}, lineas consultadas: {}", ex, affectedRows.get(), 
          returnedRows.get());
    }
  }

}
