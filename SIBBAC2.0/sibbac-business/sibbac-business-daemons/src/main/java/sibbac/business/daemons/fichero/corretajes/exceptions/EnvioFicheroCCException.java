/**
 * 
 */
package sibbac.business.daemons.fichero.corretajes.exceptions;

/**
 * @author xIS16630
 *
 */
public class EnvioFicheroCCException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 5557016750608894430L;

  /**
   * 
   */
  public EnvioFicheroCCException() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public EnvioFicheroCCException(String arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public EnvioFicheroCCException(Throwable arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public EnvioFicheroCCException(String arg0, Throwable arg1) {
    super(arg0, arg1);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   * @param arg2
   * @param arg3
   */
  public EnvioFicheroCCException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
    // TODO Auto-generated constructor stub
  }

}
