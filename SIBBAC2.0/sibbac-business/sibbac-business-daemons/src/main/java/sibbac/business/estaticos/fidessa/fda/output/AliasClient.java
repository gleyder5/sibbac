package sibbac.business.estaticos.fidessa.fda.output;

/* MCG 09/12/2009 */
/*import java.math.BigDecimal; */
/*import sibbac.business.estaticos.halee.jdo.bsnbpsql.Tcli0dir; */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.ClientMegara;
import sibbac.business.estaticos.utils.EstaticosMegaraUtilities;
import sibbac.business.estaticos.utils.conversiones.ConvertirAs400Fidessa;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.bo.Tmct0fisBoSoloEstaticos;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.wrappers.database.model.Tmct0tfi;
import sibbac.business.wrappers.database.bo.Tmct0tfiBo;
import sibbac.common.utils.SibbacEnums;
import sibbac.database.megbpdta.bo.Fmeg0aadBo;
import sibbac.database.megbpdta.bo.Fmeg0aliBo;
import sibbac.database.megbpdta.bo.Fmeg0aotBo;
import sibbac.database.megbpdta.bo.Fmeg0brcBo;
import sibbac.database.megbpdta.bo.Fmeg0entBo;
import sibbac.database.megbpdta.bo.Fmeg0eotBo;
import sibbac.database.megbpdta.bo.Fmeg0suaBo;
import sibbac.database.megbpdta.model.Fmeg0aad;
import sibbac.database.megbpdta.model.Fmeg0ali;
import sibbac.database.megbpdta.model.Fmeg0brc;
import sibbac.database.megbpdta.model.Fmeg0ent;
import sibbac.database.megbpdta.model.Fmeg0sua;

/* MCG 09/12/2009 */
/*import sibbac.business.estaticos.halee.jdo.megbpdta.Fmeg0aad;
 import sibbac.business.estaticos.halee.jdo.megbpdta.primarykeys.Fmeg0aadPrimaryKey; */

@Service
@Scope(value = "prototype")
public class AliasClient extends ClientMegara {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(AliasClient.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0aliBo fmeg0aliBo;

  @Autowired
  private Fmeg0brcBo fmeg0brcBo;

  @Autowired
  private Fmeg0suaBo fmeg0suaBo;

  @Autowired
  private Fmeg0aadBo fmeg0aadBo;

  @Autowired
  private Fmeg0entBo fmeg0entBo;

  @Autowired
  private Fmeg0eotBo fmeg0eotBo;

  @Autowired
  private Fmeg0aotBo fmeg0aotBo;

  @Autowired
  private ConvertirAs400Fidessa convertirAs400Fidessa;

  @Autowired
  private EstaticosMegaraUtilities estaticosMegaraUtilities;

  /** Campo <code>application</code> de la tabla <code>tmct0cfg</code>. */
  @Value("${sibbac20.application.name}")
  private String sibbac20ApplicationName;

  @Autowired
  private Tmct0cfgBo tmct0cfgBo;
  
  @Autowired
  private Tmct0fisBoSoloEstaticos tmct0fisBo;
  
  @Autowired
  private Tmct0tfiBo tmct0tfiBo;
  
  

  private Fmeg0ali objFmeg0ali;

  public AliasClient(Fmeg0ali objFmeg0ali) {

    super();

    this.objFmeg0ali = objFmeg0ali;

  }

  public AliasClient(Fmeg0sua objFmeg0sua) {

    super();

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    String pkFmeg0ali;

    pkFmeg0ali = objFmeg0sua.getId().getCdaliass();

    try {
      tx.begin();

      objFmeg0ali = fmeg0aliBo.findById(pkFmeg0ali);

      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
      setMensajeError(e.getMessage());

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

  }

  public boolean execXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    generarXML();

    RegistroControl objRegistroControl = generarRegistroControl();

    objRegistroControl.write();

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    String pkFmeg0ali;

    pkFmeg0ali = objFmeg0ali.getCdaliass();

    /* MCG 09/12/2009 */
    /* Fmeg0aad objFMEG0AAD = null; */
    /* AML 25/01/2011 */
    /* DECIMAL_PLACES */
    // Si nuclient es 20370(BGI) o 21240(BLACKROCK) vale 6
    // 4 en el resto de los casos

    final String cdaliass = objFmeg0ali.getCdaliass().trim();
    String decimalPlaces = null;
    if ("20370".equals(cdaliass) || "21240".equals(cdaliass) || "22471".equals(cdaliass))
      decimalPlaces = "6";
    else
      decimalPlaces = "4";

    setDecimalPlaces(decimalPlaces);

    try {
      tx.begin();

      this.objFmeg0ali = fmeg0aliBo.findById(pkFmeg0ali);

      // ----------------------------------------------------------------------------

      setTipoActualizacion(objFmeg0ali.getTpmodeid());

      // ----------------------------------------------------------------------------

      setClient_mnemonic(objFmeg0ali.getCdaliass().trim());

      // ----------------------------------------------------------------------------

      setClient_name(objFmeg0ali.getDescrali().trim());

      // ----------------------------------------------------------------------------

      setClient_group(objFmeg0ali.getCdbrocli().trim());

      // ----------------------------------------------------------------------------

      try {

        Fmeg0brc objFmeg0brc;
        String pkFmeg0brc;
        pkFmeg0brc = objFmeg0ali.getCdbrocli().trim();

        objFmeg0brc = fmeg0brcBo.findById(pkFmeg0brc);

        setClient_type(convertirAs400Fidessa.execConvertir("TPCLIENN", objFmeg0brc.getTpclienn()).trim());

        setMifid_classification(convertirAs400Fidessa.execConvertir("CTGMIFID", objFmeg0brc.getCtgmifid()).trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
        if ("".equals(getMensajeError().trim())) {
          setMensajeError(e.getMessage());
        }
      }

      // ----------------------------------------------------------------------------

      /*
       * AML 07/03/2011 Si la cuenta es nueva tpModeid = 'I' se informa de que
       * tiene cuentas asociadas HAS_ACCOUNT = 'Y'
       */

      String hasAccount = "N";

      if ("I".equals(objFmeg0ali.getTpmodeid().trim())) {
        hasAccount = "Y";
      }
      else {
        int count = fmeg0suaBo.countByCdaliassAndTpmodeidDistinctD(cdaliass);

        if (0 < count)
          hasAccount = "Y";
        else
          hasAccount = "N";
      }

      setHas_account(hasAccount);

      // ----------------------------------------------------------------------------

      List<Fmeg0aad> listFmeg0add = fmeg0aadBo.findAllByIdCdaliass(cdaliass);

      // ----------------------------------------------------------------------------

      setConfirmation_method("MANUAL");
      setOasys("");

      int i = 0;
      boolean salir = false;

      while ((i++ < listFmeg0add.size()) && !salir) {

        if ("OA".equals(((Fmeg0aad) listFmeg0add.get(i - 1)).getTpaddres().trim())) {
          setConfirmation_method("CTM");
          setOasys(((Fmeg0aad) listFmeg0add.get(i - 1)).getIdoasysa().trim());
          salir = true;
        }
        else {

          Fmeg0aad fmeg0aad = (Fmeg0aad) listFmeg0add.get(i - 1);
          if ("SQ".equals((fmeg0aad).getTpaddres().trim())) {
            setConfirmation_method("TRAM");

            /* AML 22-04-2010 */
            /* Se genera el campo BIC con IDSEQUAL */

            setBic(fmeg0aad.getIdsequal().trim());
            salir = true;
          }
        }

      }

      // ----------------------------------------------------------------------------

      if ("1".equals(objFmeg0ali.getIsrecall().trim()) && "1".equals(objFmeg0ali.getIsreccon().trim())) {
        setConfirmation_level("BLOCK");
      }
      else {
        if ("1".equals(objFmeg0ali.getIsrecall().trim()) && "0".equals(objFmeg0ali.getIsreccon().trim())) {
          setConfirmation_level("ALLOC");
        }
        else {
          setConfirmation_level("");
        }
      }

      // ----------------------------------------------------------------------------

      try {

        String pkFmeg0ent = objFmeg0ali.getCdbrocli().trim();

        Fmeg0ent objFmeg0ent = fmeg0entBo.findById(pkFmeg0ent);

        List<String> listCdidentiFromFmeg0eot = fmeg0eotBo.findAllCdidentiByCdident1AndTpidtype(
            objFmeg0ent.getCdident1(), "F");

        if (listCdidentiFromFmeg0eot.size() > 0) {
          setCif_alt_type(listCdidentiFromFmeg0eot.get(0).trim());
        }
        else {
          setCif_alt_type("");
        }

        // ----------------------------------------------------------------------------

        setCountry_of_domicile(objFmeg0ent.getCdreside().trim());

      }
      catch (Exception e) {
        logger.warn(e.getMessage());
        logger.trace(e.getMessage(),e);
      }
      // ----------------------------------------------------------------------------

      try {
        setPrimary_sales_trader(convertirAs400Fidessa.execConvertir("CDTRADER", objFmeg0ali.getCdtrader()).trim());
        
      }
      catch (Exception e) {
        setPrimary_sales_trader("");
      }
      // ----------------------------------------------------------------------------

      if (!"D".equals(objFmeg0ali.getTpmodeid().trim())) {
        setStatus_indicator("A");
      }
      else {
        setStatus_indicator("I");
      }

      // ----------------------------------------------------------------------------

      // ----------------------------------------------------------------------------

      if ("1".equals(objFmeg0ali.getIsownacc().trim())) {
        setFor_own_account("P");
      }
      else {
        if ("0".equals(objFmeg0ali.getIsownacc().trim())) {
          setFor_own_account("T");
        }
      }

      // ----------------------------------------------------------------------------

      if ("1".equals(objFmeg0ali.getIspaymar().trim())) {
        setMarket_fees("S");
      }
      else {
        if ("0".equals(objFmeg0ali.getIspaymar().trim())) {
          setMarket_fees("N");
        }
      }

      // ----------------------------------------------------------------------------

      if ("1".equals(objFmeg0ali.getIsreqalo().trim())) {
        setRequire_allocation("Y");
      }
      else {
        if ("0".equals(objFmeg0ali.getIsreqalo().trim())) {
          setRequire_allocation("N");
        }
      }

      // Obtención campos mifid
      try {

        // Se comprueba si se van a generar los campos
        final Tmct0cfg tmct0cfg = tmct0cfgBo.findByAplicationAndProcessAndKeyname(sibbac20ApplicationName,
            SibbacEnums.Tmct0cfgConfig.ESTATICOS_CAMPOS_MIFID_FLAG.getProcess(),
            SibbacEnums.Tmct0cfgConfig.ESTATICOS_CAMPOS_MIFID_FLAG.getKeyname());

        final boolean envioCamposMifid = (tmct0cfg != null && tmct0cfg.getKeyvalue() != null && "Y".equals(tmct0cfg
            .getKeyvalue()));
        setEnvioCamposMifid(envioCamposMifid);

        if (envioCamposMifid) {

          String mifidClientType = "";
          String mifidClientCode="";
          List<Tmct0tfi> tmct0tfiList = tmct0tfiBo.findAllByCdaliassSoloRepresentantes(cdaliass, "");
          if (tmct0tfiList != null && tmct0tfiList.size() >0)
          {
            final Tmct0tfi tmct0tfi = tmct0tfiList.get(0);
            final String cdholder = tmct0tfi.getId().getCddclave();
            List<Tmct0fis> listtmct0fis = tmct0fisBo.findAllByCddclaveOrderNumsecdesc(cdholder);
            if (listtmct0fis == null || listtmct0fis.isEmpty()) {
              throw new Exception(String.format(
                  "No encontrado el titular  '%s' del alias '%s' dado de alta en el maestro de titulares (tmct0fis)",
                  cdholder, cdaliass));
            }
            Tmct0fis tmct0fis = listtmct0fis.get(0);
            listtmct0fis.clear();
            String tpsocied = tmct0fis.getTpsocied();
            mifidClientType = ("F".equals(tpsocied))?"H": "L";
            mifidClientCode = tmct0fis.getIdMifid();            
            if (mifidClientCode != null)
            {
              mifidClientCode = mifidClientCode.trim();
            }
                
          }
          
          
          // MIFID_CLIENT_TYPE
          setMifidClientType(mifidClientType);
          

          // MIFID_BASIC_CONTRACT
          final String mifidBasicContract = fmeg0aotBo.findDescrip1ByCdaliassAndTpidtypeAndCdotheid(cdaliass, "MF",
              "CONTRATO_BASICO");
          
          if(mifidBasicContract == null || "".equals(mifidBasicContract.trim()) || mifidBasicContract.trim().equals("S") || mifidBasicContract.trim().equals("Y")){
            setMifidBasicContract("Y");  
          }else{
            setMifidBasicContract("N");
          }
          
          
          // MIFID_CLIENT_CODE
          setMifidClientCode(mifidClientCode);

          // MIFID_OTC_CONSENT
          final String consentOtc = fmeg0aotBo.findDescrip1ByCdaliassAndTpidtypeAndCdotheid(cdaliass, "MF",
              "CONSENT_OTC");
          
          if(consentOtc==null || "".equals(consentOtc.trim()) || consentOtc.trim().equals("S") || consentOtc.trim().equals("Y")){
            setMifidOtcConsent("Y");  
          }else{
            setMifidOtcConsent("N");
          }
          
          
          // MIFID_TOT_VOLUME_PUB_CONSENT
          final String consentPublVolTot = fmeg0aotBo.findDescrip1ByCdaliassAndTpidtypeAndCdotheid(cdaliass, "MF",
              "CONSENT_PUBL_VOL_TOT");
          setMifidTotVolumePubConsent(consentPublVolTot.trim());

          // DECISION_MAKERS
          
          
          List<Tmct0fis> tmct0fisList = getDecisionMakerList(cdaliass);
          setDecisionMakerList(tmct0fisList);

        }

        // ----------------------------------------------------------------------------

      }
      catch (Exception e) {
        setMensajeError(e.getMessage());
        logger.warn(e.getMessage());
        logger.trace(e.getMessage(),e);

      }

      tx.commit();

      /*
       * AML 2013/11/20 Si la cuenta est� unificada el stra es '008' + alias
       */

      // final boolean aliasUnified =
      // estaticosMegaraUtilities.isAliasUnified(objFmeg0ali.getCdaliass());

      /*
       * AML 2012/03/20 Se obtiene el nuctapro para a�adirlo al campo STRA
       */

      // final BigDecimal nuctaproN = (aliasUnified)? new
      // BigDecimal(objFmeg0ali.getCdaliass()):
      // utilities.getNuctaproAccount(objFmeg0ali.getCdaliass());

      final BigDecimal nuctaproN = new BigDecimal(objFmeg0ali.getCdaliass());

      if (nuctaproN != null) {
        String nuctapro = nuctaproN.toString();
        for (int toprepend = 5 - nuctapro.length(); toprepend > 0; toprepend--) {
          nuctapro = "0" + nuctapro;
        }

        setStra("008" + nuctapro.toString());
      }

      final boolean aliasUnifiedSibbac = estaticosMegaraUtilities.isAliasUnifiedSibbac(objFmeg0ali.getCdaliass());
      setIsAliasUnified(aliasUnifiedSibbac);

    }
    catch (Exception e) {

      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);

    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private List<Tmct0fis> getDecisionMakerList(String cdaliass) throws Exception {
    List<Tmct0fis> tmct0fisList = new ArrayList<Tmct0fis>();
    
    List<Tmct0tfi> tmct0tfiList = tmct0tfiBo.findAllByCdaliass(cdaliass);
    int count = 0;
    
    for (Tmct0tfi tmct0fi: tmct0tfiList)
    {
      if (count == 25)
        break;
      String cdholder=tmct0fi.getId().getCddclave();
      List<Tmct0fis> listtmct0fis = tmct0fisBo.findAllByCddclaveOrderNumsecdesc(cdholder);
      if (listtmct0fis == null || listtmct0fis.isEmpty()) {
        throw new Exception(String.format(
            "No encontrado el titular  '%s' del alias '%s' dado de alta en el maestro de titulares (tmct0fis)",
            cdholder, cdaliass));
      }
      Tmct0fis tmct0fis = listtmct0fis.get(0);
      listtmct0fis.clear();
      final String numPersonaSan = tmct0fis.getNumPersonaSan();
      if (numPersonaSan != null && !"".equals(numPersonaSan.trim()))    {   
        tmct0fis.setTptiprep(tmct0fi.getTptiprep());
        tmct0fisList.add(tmct0fis);
        count ++;
      }
      
    }
    
    return tmct0fisList;
  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {

      result = ctx.getBean(RegistroControl.class, "CLIENT", getClient_mnemonic(), getNombreCortoXML(), "I", "",
          "Client", "FI");

    }
    else {
      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());
      result = ctx.getBean(RegistroControl.class, "CLIENT", getClient_mnemonic(), getNombreCortoXML(), "E",
          getMensajeError(), "Client", "FI");
    }

    return result;

  }

}