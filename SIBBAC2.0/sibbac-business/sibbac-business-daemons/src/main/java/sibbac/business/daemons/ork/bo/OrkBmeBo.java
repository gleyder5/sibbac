package sibbac.business.daemons.ork.bo;

import java.io.File;

import sibbac.common.SIBBACBusinessException;

/**
 * Objeto de negocio que genera en sistema de ficheros el archivo Order Record Keeping a partir 
 * de la información almacenada en las tablas OrkInput y OrkOutputBuilder
 *
 */
public interface OrkBmeBo {

	/**
	 * Método de generación del fichero
	 * @param config Objeto de configuración
	 * @throws SIBBACBusinessException En caso de una violación de las reglas de negocio.
	 */
	File generaOrk(OrkFidessaFieldCollection fidessaFieldCollection) throws SIBBACBusinessException;

	int getSequence();
	
}
