package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;

/**
 * ClientMegara.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class ClientMegara extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ClientMegara.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The client_mnemonic. */
  private String client_mnemonic;

  /** The client_name. */
  private String client_name;

  /** The client_group. */
  private String client_group;

  /** The client_type. */
  private String client_type;

  /** The has_account. */
  private String has_account;

  /** The confirmation_method. */
  private String confirmation_method;

  /** The confirmation_level. */
  private String confirmation_level;

  /** The cif_alt_type. */
  private String cif_alt_type;

  private String primary_sales_trader;

  /** The country_of_domicile. */
  private String country_of_domicile;

  /** The oasys. */
  private String oasys;

  /** The status_indicator. */
  private String status_indicator;

  /** The mifid_classification. */
  private String mifid_classification;

  /** The portfolio. */
  private String portfolio;

  /** The for_own_account. */
  private String for_own_account;

  /** The market_fees. */
  private String market_fees;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  /* MCG 09/12/2009 */
  /** The require_allocation. */
  private String require_allocation;

  /* MCG 09/12/2009 */
  /** The Bic. */
  private String bic;

  /* AML 25/01/2011 */

  /** The Decimal places */
  private String decimalPlaces;

  /* AML 20/03/2012 */

  /** The Stra */
  private String stra = null;

  /* AML 04/02/2013 */
  private boolean isAliasUnified = false;

  /* AML 30/12/2017 */
  private boolean envioCamposMifid = false;

  private String mifidClientType = null;
  private String mifidBasicContract = null;
  private String mifidClientCode = null;
  private String mifidOtcConsent = null;
  private String mifidTotVolumePubConsent = null;

  private List<Tmct0fis> decisionMakerList = null;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new client_megara.
   */
  protected ClientMegara() {

    this.tipoActualizacion = "";
    this.client_mnemonic = "";
    this.client_name = "";
    this.client_group = "";
    this.client_type = "";
    this.has_account = "";
    this.confirmation_method = "";
    this.confirmation_level = "";
    this.cif_alt_type = "";
    this.primary_sales_trader = "";
    this.country_of_domicile = "";
    this.oasys = "";
    this.status_indicator = "";
    this.mifid_classification = "";
    this.portfolio = "";
    this.for_own_account = "";
    this.market_fees = "";

    /* MCG 09/12/2009 */
    this.require_allocation = "";

    /* MCG 09/12/2009 */
    this.bic = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("client: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element CLIENT = document.createElement("CLIENT");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element CLIENT_MNEMONIC = document.createElement("CLIENT_MNEMONIC");
      CLIENT_MNEMONIC.appendChild(document.createTextNode(getClient_mnemonic()));

      Element CLIENT_NAME = document.createElement("CLIENT_NAME");
      CLIENT_NAME.appendChild(document.createTextNode(getClient_name()));

      Element CLIENT_GROUP = document.createElement("CLIENT_GROUP");
      CLIENT_GROUP.appendChild(document.createTextNode(getClient_group()));

      Element CLIENT_TYPE = document.createElement("CLIENT_TYPE");
      CLIENT_TYPE.appendChild(document.createTextNode(getClient_type()));

      Element HAS_ACCOUNTS = document.createElement("HAS_ACCOUNTS");
      HAS_ACCOUNTS.appendChild(document.createTextNode(getHas_account()));

      Element CONFIRMATION_METHOD = document.createElement("CONFIRMATION_METHOD");
      CONFIRMATION_METHOD.appendChild(document.createTextNode(getConfirmation_method()));

      Element CONFIRMATION_LEVEL = document.createElement("CONFIRMATION_LEVEL");
      CONFIRMATION_LEVEL.appendChild(document.createTextNode(getConfirmation_level()));

      Element INSTRUMENT_SYMBOLOGY = document.createElement("INSTRUMENT_SYMBOLOGY");
      INSTRUMENT_SYMBOLOGY.appendChild(document.createTextNode("ISIN"));

      Element CHARGE_PRESENTATION = document.createElement("CHARGE_PRESENTATION");
      CHARGE_PRESENTATION.appendChild(document.createTextNode("3"));

      Element CIF_ALT_TYPE = document.createElement("CIF_ALT_TYPE");
      CIF_ALT_TYPE.appendChild(document.createTextNode(getCif_alt_type()));

      Element PRIMARY_SALES_TRADER = document.createElement("PRIMARY_SALES_TRADER");
      PRIMARY_SALES_TRADER.appendChild(document.createTextNode(getPrimary_sales_trader()));

      Element REPRESENTATIVE = document.createElement("REPRESENTATIVE");

      Element COUNTRY_OF_DOMICILE = document.createElement("COUNTRY_OF_DOMICILE");
      COUNTRY_OF_DOMICILE.appendChild(document.createTextNode(getCountry_of_domicile()));

      Element GROSS_PRICE_BASIS = document.createElement("GROSS_PRICE_BASIS");

      // AML 07/05/2012
      if ("I".equals(getTipoActualizacion()))
        GROSS_PRICE_BASIS.appendChild(document.createTextNode("G"));

      Element AGREED_PRICE_BASIS = document.createElement("AGREED_PRICE_BASIS");

      // AML 07/05/2012
      if ("I".equals(getTipoActualizacion()))
        AGREED_PRICE_BASIS.appendChild(document.createTextNode("G"));

      Element DECIMAL_PLACES = document.createElement("DECIMAL_PLACES");

      // AML 07/05/2012
      if ("I".equals(getTipoActualizacion()))
        DECIMAL_PLACES.appendChild(document.createTextNode(getDecimalPlaces()));

      Element ALLOW_WAREHOUSING = document.createElement("ALLOW_WAREHOUSING");

      Element WAREHOUSE_DURATION = document.createElement("WAREHOUSE_DURATION");

      Element WAREHOUSE_LIMIT = document.createElement("WAREHOUSE_LIMIT");

      Element OASYS = document.createElement("OASYS");
      OASYS.appendChild(document.createTextNode(getOasys()));

      Element ALERT = document.createElement("ALERT");

      Element FUND = document.createElement("FUND");

      Element PERSHING = document.createElement("PERSHING");

      Element CUSTOMER_CODE_1 = document.createElement("CUSTOMER_CODE_1");

      Element CUSTOMER_CODE_2 = document.createElement("CUSTOMER_CODE_2");

      Element STATUS_INDICATOR = document.createElement("STATUS_INDICATOR");
      STATUS_INDICATOR.appendChild(document.createTextNode(getStatus_indicator()));

      Element BACK_OFFICE_CODE = document.createElement("BACK_OFFICE_CODE");

      Element PERMISSIONING_GROUP = document.createElement("PERMISSIONING_GROUP");

      Element CONFIDENTIALITY = document.createElement("CONFIDENTIALITY");

      Element MIFID_CLASSIFICATION = document.createElement("MIFID_CLASSIFICATION");
      MIFID_CLASSIFICATION.appendChild(document.createTextNode(getMifid_classification()));

      Element FOR_OWN_ACCOUNT = document.createElement("FOR_OWN_ACCOUNT");
      FOR_OWN_ACCOUNT.appendChild(document.createTextNode(getFor_own_account()));

      Element MARKET_FEES = document.createElement("MARKET_FEES");
      MARKET_FEES.appendChild(document.createTextNode(getMarket_fees()));

      /*
       * AML 19/12/2011
       * 
       * Se elimina campo allow_dma
       */
      // Element ALLOW_DMA = document.createElement("ALLOW_DMA");

      Element EXCHANGE_TYPE = document.createElement("EXCHANGE_TYPE");

      // AML 18/05/2012
      // AML 04/02/2013
      // AML 07/11/2013 Se deja el EXCHANGE TYPE FIJO A a

      // final String exchangeTypeValue = ((stra != null &&
      // !("".equals(stra.trim()))) || isAliasUnified) ? "A": "F";
      final String exchangeTypeValue = "A";

      EXCHANGE_TYPE.appendChild(document.createTextNode(exchangeTypeValue));

      /* MCG 09/12/2009 */
      Element REQUIRE_ALLOCATION = document.createElement("REQUIRE_ALLOCATION");
      REQUIRE_ALLOCATION.appendChild(document.createTextNode(getRequire_allocation()));

      /* AML 22/04/2010 */
      Element BIC = document.createElement("BIC");
      String bicValue = getBic();

      if (bicValue != null && !"".equals(bicValue))
        BIC.appendChild(document.createTextNode(bicValue));

      /* AML 20/03/2012 */
      Element STRA = document.createElement("STRA");

      // final String backOfficeSibbacValue = (isAliasUnified) ? "Y" : "N";

      // AML 2014-05-05
      // El stra se informar� siempre
      // if (stra != null && !"".equals(stra) &&
      // "N".equals(backOfficeSibbacValue))
      STRA.appendChild(document.createTextNode(stra));

      /* AML 12/04/2012 */
      Element ONLY_MODIFY_CLIENT = document.createElement("ONLY_MODIFY_CLIENT");
      Element CCV = document.createElement("CCV");

      /* AML 09/05/2013 */
      Element BACKOFFICE_SIBBAC = document.createElement("BACKOFFICE_SIBBAC");
      /* AML 08/11/2013 */
      // Si la BACKOFFICE_SIBBAC:Y(CUENTA UNIFICADA)/N

      // AML 2014-05-05
      // El backofficeSIBBAC ir� siempre a Y
      // BACKOFFICE_SIBBAC.appendChild(document.createTextNode(backOfficeSibbacValue));
      BACKOFFICE_SIBBAC.appendChild(document.createTextNode("Y"));

      // ---------------------------------------------------------

      data.appendChild(CLIENT_MNEMONIC);
      data.appendChild(CLIENT_NAME);
      data.appendChild(CLIENT_GROUP);
      data.appendChild(CLIENT_TYPE);
      data.appendChild(HAS_ACCOUNTS);
      data.appendChild(CONFIRMATION_METHOD);
      data.appendChild(CONFIRMATION_LEVEL);
      data.appendChild(INSTRUMENT_SYMBOLOGY);
      data.appendChild(CHARGE_PRESENTATION);
      data.appendChild(CIF_ALT_TYPE);
      data.appendChild(PRIMARY_SALES_TRADER);
      data.appendChild(REPRESENTATIVE);
      data.appendChild(COUNTRY_OF_DOMICILE);
      data.appendChild(GROSS_PRICE_BASIS);
      data.appendChild(AGREED_PRICE_BASIS);
      data.appendChild(DECIMAL_PLACES);
      data.appendChild(ALLOW_WAREHOUSING);
      data.appendChild(WAREHOUSE_DURATION);
      data.appendChild(WAREHOUSE_LIMIT);
      data.appendChild(OASYS);
      data.appendChild(ALERT);
      data.appendChild(FUND);
      data.appendChild(PERSHING);
      data.appendChild(CUSTOMER_CODE_1);
      data.appendChild(CUSTOMER_CODE_2);
      data.appendChild(STATUS_INDICATOR);
      data.appendChild(BACK_OFFICE_CODE);
      data.appendChild(PERMISSIONING_GROUP);
      data.appendChild(CONFIDENTIALITY);
      data.appendChild(MIFID_CLASSIFICATION);
      data.appendChild(FOR_OWN_ACCOUNT);
      data.appendChild(MARKET_FEES);
      /*
       * AML 19/12/2011
       * 
       * Se elimina campo allow_dma
       */
      // data.appendChild(ALLOW_DMA);
      data.appendChild(EXCHANGE_TYPE);

      /* MCG 09/12/2009 */
      data.appendChild(REQUIRE_ALLOCATION);

      /* MCG 09/12/2009 */
      data.appendChild(BIC);

      /* AML 20/03/2012 */
      data.appendChild(STRA);

      /* AML 12/04/2012 */
      data.appendChild(ONLY_MODIFY_CLIENT);
      data.appendChild(CCV);
      data.appendChild(BACKOFFICE_SIBBAC);

      /* AML 29/12/2017 */
      // AML 2017-12-29
      // Creación Campos Mifid
      final boolean envioCamposMifid = getEnvioCamposMifid();

      if (envioCamposMifid) {

        Element MIFID_CLIENT_TYPE = document.createElement("MIFID_CLIENT_TYPE");
        Element MIFID_BASIC_CONTRACT = document.createElement("MIFID_BASIC_CONTRACT");
        Element MIFID_CLIENT_CODE = document.createElement("MIFID_CLIENT_CODE");
        Element MIFID_OTC_CONSENT = document.createElement("MIFID_OTC_CONSENT");
        Element MIFID_TOT_VOLUME_PUB_CONSENT = document.createElement("MIFID_TOT_VOLUME_PUB_CONSENT");

        MIFID_CLIENT_TYPE.appendChild(document.createTextNode(getMifidClientType()));
        MIFID_BASIC_CONTRACT.appendChild(document.createTextNode(getMifidBasicContract()));
        MIFID_CLIENT_CODE.appendChild(document.createTextNode(getMifidClientCode()));
        MIFID_OTC_CONSENT.appendChild(document.createTextNode(getMifidOtcConsent()));
        MIFID_TOT_VOLUME_PUB_CONSENT.appendChild(document.createTextNode(getMifidTotVolumePubConsent()));

        data.appendChild(MIFID_CLIENT_TYPE);
        data.appendChild(MIFID_BASIC_CONTRACT);
        data.appendChild(MIFID_CLIENT_CODE);
        data.appendChild(MIFID_OTC_CONSENT);
        data.appendChild(MIFID_TOT_VOLUME_PUB_CONSENT);

        Element DECISION_MAKERS = document.createElement("DECISION_MAKERS");

        final List<Tmct0fis> tmct0fisList = getDecisionMakerList();
        if (tmct0fisList != null) {
          for (Tmct0fis tmct0fis : tmct0fisList) {
            Element DECISION_MAKER = document.createElement("DECISION_MAKER");            

            Element ORDENANTE_CODE = document.createElement("ORDENANTE_CODE");
            final String ordenanteCode = tmct0fis.getNumPersonaSan();
            ORDENANTE_CODE.appendChild(document.createTextNode((ordenanteCode == null) ? null : ordenanteCode.trim()));
            
            Element DECISION_MAKER_NAME = document.createElement("DECISION_MAKER_NAME");
            final String tpsocied = tmct0fis.getTpsocied();
            final String decisionMakerName = ("J".equals(tpsocied)) ? tmct0fis.getNbclien1() : (tmct0fis.getNbclient()
                .trim() + " " + tmct0fis.getNbclien1().trim() + " " + tmct0fis.getNbclien2().trim());
            DECISION_MAKER_NAME.appendChild(document.createTextNode(decisionMakerName));

            Element ORDENANTE_TYPE = document.createElement("ORDENANTE_TYPE");
            ORDENANTE_TYPE.appendChild(document.createTextNode(tpsocied));

            Element TITULAR_TYPE = document.createElement("TITULAR_TYPE");
            final String tptiprep = tmct0fis.getTptiprep();
            TITULAR_TYPE.appendChild(document.createTextNode(tptiprep));

            
            DECISION_MAKER.appendChild(ORDENANTE_CODE);
            DECISION_MAKER.appendChild(DECISION_MAKER_NAME);            
            DECISION_MAKER.appendChild(ORDENANTE_TYPE);
            DECISION_MAKER.appendChild(TITULAR_TYPE);

            DECISION_MAKERS.appendChild(DECISION_MAKER);

          }
        }

        data.appendChild(DECISION_MAKERS);
      }

      // ---------------------------------------------------------

      CLIENT.appendChild(header);
      CLIENT.appendChild(data);

      document.appendChild(CLIENT);

      logger.debug("client: estructura documento XML completada");

      // ---------------------------------------------------------

      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("client: docuemnto xml generado", new DOMSource(document).toString());

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("client: error en la generacion del docuemento XML");
      logger.warn("client: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a client";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a client";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a client";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("client: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, EBMMdd0000.xml donde
   * EB es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getClients();
    Path path = Paths.get(getRutaDestino(), String.format("EB%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("Clients: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_mnemonic.
   * 
   * @param client_mnemonic the new client_mnemonic
   */
  protected void setClient_mnemonic(String client_mnemonic) {
    this.client_mnemonic = client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_mnemonic.
   * 
   * @return the client_mnemonic
   */
  protected String getClient_mnemonic() {
    return client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_name.
   * 
   * @param client_name the new client_name
   */
  protected void setClient_name(String client_name) {
    this.client_name = client_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_name.
   * 
   * @return the client_name
   */
  protected String getClient_name() {
    return client_name;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_group.
   * 
   * @param client_group the new client_group
   */
  protected void setClient_group(String client_group) {
    this.client_group = client_group;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_group.
   * 
   * @return the client_group
   */
  protected String getClient_group() {
    return client_group;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_type.
   * 
   * @param client_type the new client_type
   */
  protected void setClient_type(String client_type) {
    this.client_type = client_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_type.
   * 
   * @return the client_type
   */
  protected String getClient_type() {
    return client_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the has_account.
   * 
   * @param has_account the new has_account
   */
  protected void setHas_account(String has_account) {
    this.has_account = has_account;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the has_account.
   * 
   * @return the has_account
   */
  protected String getHas_account() {
    return has_account;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the confirmation_method.
   * 
   * @param confirmation_method the new confirmation_method
   */
  protected void setConfirmation_method(String confirmation_method) {
    this.confirmation_method = confirmation_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the confirmation_method.
   * 
   * @return the confirmation_method
   */
  protected String getConfirmation_method() {
    return confirmation_method;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the confirmation_level.
   * 
   * @param confirmation_level the new confirmation_level
   */
  protected void setConfirmation_level(String confirmation_level) {
    this.confirmation_level = confirmation_level;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the confirmation_level.
   * 
   * @return the confirmation_level
   */
  protected String getConfirmation_level() {
    return confirmation_level;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the cif_alt_type.
   * 
   * @param cif_alt_type the new cif_alt_type
   */
  protected void setCif_alt_type(String cif_alt_type) {
    this.cif_alt_type = cif_alt_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the cif_alt_type.
   * 
   * @return the cif_alt_type
   */
  protected String getCif_alt_type() {
    return cif_alt_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  protected void setPrimary_sales_trader(String primary_sales_trader) {
    this.primary_sales_trader = primary_sales_trader;
  }

  protected String getPrimary_sales_trader() {
    return primary_sales_trader;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the country_of_domicile.
   * 
   * @param country_of_domicile the new country_of_domicile
   */
  protected void setCountry_of_domicile(String country_of_domicile) {
    this.country_of_domicile = country_of_domicile;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the country_of_domicile.
   * 
   * @return the country_of_domicile
   */
  protected String getCountry_of_domicile() {
    return country_of_domicile;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the oasys.
   * 
   * @param oasys the new oasys
   */
  protected void setOasys(String oasys) {
    this.oasys = oasys;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the oasys.
   * 
   * @return the oasys
   */
  protected String getOasys() {
    return oasys;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the status_indicator.
   * 
   * @param status_indicator the new status_indicator
   */
  protected void setStatus_indicator(String status_indicator) {
    this.status_indicator = status_indicator;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the status_indicator.
   * 
   * @return the status_indicator
   */
  protected String getStatus_indicator() {
    return status_indicator;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mifid_classification.
   * 
   * @param mifid_classification the new mifid_classification
   */
  protected void setMifid_classification(String mifid_classification) {
    this.mifid_classification = mifid_classification;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mifid_classification.
   * 
   * @return the mifid_classification
   */
  protected String getMifid_classification() {
    return mifid_classification;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the portfolio.
   * 
   * @param portfolio the new portfolio
   */
  protected void setPortfolio(String portfolio) {
    this.portfolio = portfolio;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the portfolio.
   * 
   * @return the portfolio
   */
  protected String getPortfolio() {
    return portfolio;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the for_own_account.
   * 
   * @param for_own_account the new for_own_account
   */
  protected void setFor_own_account(String for_own_account) {
    this.for_own_account = for_own_account;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the for_own_account.
   * 
   * @return the for_own_account
   */
  protected String getFor_own_account() {
    return for_own_account;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the market_fees.
   * 
   * @param market_fees the new market_fees
   */
  protected void setMarket_fees(String market_fees) {
    this.market_fees = market_fees;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the market_fees.
   * 
   * @return the market_fees
   */
  protected String getMarket_fees() {
    return market_fees;
  }

  /* MCG 09/12/2009 */
  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the require_allocation.
   * 
   * @param require_allocation the new require_allocation
   */
  protected void setRequire_allocation(String require_allocation) {
    this.require_allocation = require_allocation;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the require_allocation.
   * 
   * @return the require_allocation
   */
  protected String getRequire_allocation() {

    return require_allocation;
  }

  /* MCG 09/12/2009 */
  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the bic
   * 
   * @param require_allocation the new bic
   */
  protected void setBic(String bic) {
    this.bic = bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the require_allocation.
   * 
   * @return the require_allocation
   */
  protected String getBic() {
    return bic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /* AML 25/01/2011 */

  protected String getDecimalPlaces() {
    return decimalPlaces;
  }

  protected void setDecimalPlaces(String decimalPlaces) {
    this.decimalPlaces = decimalPlaces;
  }

  /* AML 20/03/2012 */
  protected String getStra() {
    return stra;
  }

  protected void setStra(String stra) {
    this.stra = stra;
  }

  /* AML 04/02/2012 */

  protected boolean getIsAliasUnified() {
    return isAliasUnified;
  }

  protected void setIsAliasUnified(boolean value) {
    isAliasUnified = value;
  }

  protected boolean getEnvioCamposMifid() {
    return envioCamposMifid;
  }

  protected void setEnvioCamposMifid(boolean value) {
    envioCamposMifid = value;
  }

  protected String getMifidClientType() {
    return mifidClientType;
  }

  protected void setMifidClientType(String mifidClientType) {
    this.mifidClientType = mifidClientType;
  }

  protected String getMifidBasicContract() {
    return mifidBasicContract;
  }

  protected void setMifidBasicContract(String mifidBasicContract) {
    this.mifidBasicContract = mifidBasicContract;
  }

  protected String getMifidClientCode() {
    return mifidClientCode;
  }

  protected void setMifidClientCode(String mifidClientCode) {
    this.mifidClientCode = mifidClientCode;
  }

  protected String getMifidOtcConsent() {
    return mifidOtcConsent;
  }

  protected void setMifidOtcConsent(String mifidOtcConsent) {
    this.mifidOtcConsent = mifidOtcConsent;
  }

  protected String getMifidTotVolumePubConsent() {
    return mifidTotVolumePubConsent;
  }

  protected void setMifidTotVolumePubConsent(String mifidTotVolumePubConsent) {
    this.mifidTotVolumePubConsent = mifidTotVolumePubConsent;
  }

  public List<Tmct0fis> getDecisionMakerList() {
    return decisionMakerList;
  }

  public void setDecisionMakerList(List<Tmct0fis> decisionMakerList) {
    this.decisionMakerList = decisionMakerList;
  }
}