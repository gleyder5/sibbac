package sibbac.business.daemons.fichero.corretajes.utils;

import java.io.Serializable;

public class FicheroCCNameKey implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 540495904982230287L;
  private final String fileName;
  private final String idFicheroCC;

  public FicheroCCNameKey(String fileName, String idFicheroCC) {
    super();
    this.fileName = fileName;
    this.idFicheroCC = idFicheroCC;
  }

  public String getFileName() {
    return fileName;
  }

  public String getIdFicheroCC() {
    return idFicheroCC;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
    result = prime * result + ((idFicheroCC == null) ? 0 : idFicheroCC.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FicheroCCNameKey other = (FicheroCCNameKey) obj;
    if (fileName == null) {
      if (other.fileName != null)
        return false;
    } else if (!fileName.equals(other.fileName))
      return false;
    if (idFicheroCC == null) {
      if (other.idFicheroCC != null)
        return false;
    } else if (!idFicheroCC.equals(other.idFicheroCC))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "FicheroCCNameKey [fileName=" + fileName + ", idFicheroCC=" + idFicheroCC + "]";
  }
  
  

}
