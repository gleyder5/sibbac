package sibbac.business.daemons.ork.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import sibbac.business.daemons.ork.common.ResolvedValue;

import javax.persistence.Column;

/**
 * Representación más simple de un campo de salida de ORK
 * @author XI326526
 */
@Entity
@Table(name = "TMCT0_ORK_BME_CONSTANT_FIELD")
public class OrkBmeConstantField extends OrkBmeField {
  
  /**
   * Serial version
   */
  private static final long serialVersionUID = -4606516358648979785L;

  @Column(name = "VALUE")
  private String value;

  /**
   * @return el valor del campo
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value Determina el valor del campo
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * Esta implementación recupera directamente el valor del campo value.
   * @param rv No se toma en cuenta y puede ser nulo
   * @return El valor del campo value
   */
  @Override
  public Object[] getSolvedData(ResolvedValue rv) {
    return new Object[] { value };
  }

}
