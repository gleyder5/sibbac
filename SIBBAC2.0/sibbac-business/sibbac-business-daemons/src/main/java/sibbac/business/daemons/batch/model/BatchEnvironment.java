package sibbac.business.daemons.batch.model;

import java.io.Reader;
import java.io.IOException;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.common.TimeOutBatchException;

public abstract class BatchEnvironment {
	
	/**
	 * Solicita la ejecución de una consulta SQL
	 * @param query consulta a ejecutar
	 * @param type tipo de la query que se solicita
	 * @return número de lineas modificadas
	 * @throws BatchException Ante cualquier error
	 */
	protected abstract void executeStatement(String query, char type) throws BatchException;

	protected abstract Reader readerFromOrigin() throws BatchException, IOException;

	protected abstract boolean confirmAlive() throws TimeOutBatchException;

  protected abstract void batchExecute(Iterable<String> queries) throws BatchException;
  
  protected abstract int getThreadsForBuffer();
	
}
