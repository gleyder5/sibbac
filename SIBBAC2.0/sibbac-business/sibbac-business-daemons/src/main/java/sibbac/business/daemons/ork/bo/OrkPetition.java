package sibbac.business.daemons.ork.bo;

import java.util.Map;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.ork.common.ResolvedValue;
import sibbac.business.daemons.ork.model.OrkBmeField;
import sibbac.business.daemons.ork.model.OrkFidessaFixedField;
import static sibbac.business.daemons.ork.bo.OrkWriter.CVS_FORMAT;

class OrkPetition implements Serializable, ResolvedValue {
	
	static final String DYNA_FIELD = "DYNAFIELD";
	
	static final String DYNA_VALUE = "VALUE";
	
	static final String ERROR_SIGNAL = "<ERROR>";

	/**
	 * Automatically generated serial versionn UID
	 */
	private static final long serialVersionUID = -2754207361418458090L;
	
	private static final Logger LOG = getLogger(OrkPetition.class);
	
	private static final String[] PROTESIC_FIELDS = {"CDALIASS", "TIPO_ID_MIFID", "ID_MIFID"};
	
  private final OrkFidessaFieldCollection fieldCollection;
  
  private final Map<String, Object> dynadata;
	
	private final Map<String, Object[]> resolvedData;
	
	private final boolean end;
	
	private boolean success;
	
  private String error;

  private Throwable failCause;
	
	static OrkPetition endPetition() {
		return new OrkPetition(true, null);
	}
	
	OrkPetition(OrkFidessaFieldCollection fieldCollection) {
		this(false, fieldCollection);
	}
		
	private OrkPetition(boolean end, OrkFidessaFieldCollection fieldCollection) {
		this.end = end;
    this.fieldCollection = fieldCollection;
    if(end) {
      dynadata = null;
      resolvedData = null;
    }
    else {
      dynadata = new HashMap<>();
      resolvedData = new HashMap<>();
    }
		success = true;
	}

  @Override
  public Object[] getDynaData(String name) {
    return new Object[] { dynadata.get(name) };
  }
  
  @Override
  public Object[] getResolvedData(String name) {
    return resolvedData.get(name);
  }

	boolean isEnd() {
		return end;
	}
	
	boolean isSuccess() {
		return success;
	}

	boolean load(ResultSet rs) throws SQLException {
		boolean hasnext;
		if(end) {
			throw new IllegalStateException("Se está intentado cargar informacion en una peticion de final");
		}
		if(rs == null) {
		  fail("Ha llegado un resultado nulo");
		  return false;
		}
		for(OrkFidessaFixedField ff : fieldCollection.itFixedFields()) {
			dynadata.put(ff.getName(), rs.getObject(ff.getName()));
		}
		for(String n : PROTESIC_FIELDS) {
	    dynadata.put(n, rs.getObject(n));
		}
		do {
			dynadata.put(rs.getString(DYNA_FIELD), rs.getObject(DYNA_VALUE));
		}
		while((hasnext = rs.next()) && sameRecord(rs));
		return hasnext;
	}
	
	void preValidate() {
    final Object cdaliass, idMifid;

    if(!success) {
      return;
    }
    try {
      cdaliass = dynadata.get("CDALIASS"); 
      if(cdaliass == null || cdaliass.toString().trim().isEmpty()) {
        fail("Alias no informado");
        return;
      }
      idMifid = dynadata.get("ID_MIFID");
      if(idMifid == null || idMifid.toString().trim().isEmpty()) {
        fail(String.format("Alias inexistente o inactivo: %s", cdaliass));
        return;
      }
    }
    catch(RuntimeException rex) {
      LOG.error("[validate] Error en validacion de petición", rex);
      fail(rex);
    }
	}
	
	void validate(List<OrkBmeField> outputBuilders) {
	  Object[] data;
	  
	  if(!success) {
	    return;
	  }
	  try {
  	  for(OrkBmeField builder : outputBuilders) {
  	    if(builder.isRequired()) {
          data = builder.getSolvedData(this);
  	      if (data == null || data.length == 0) {
            fail(builder.getErrorMsg());
            break;
  	      }
  	    }
  	  }
	  }
	  catch(RuntimeException rex) {
	    LOG.error("[validate] Error en validacion de petición", rex);
	    fail(rex);
	  }
	}
	
	void printResolved(PrintWriter printer, List<OrkBmeField> outputBuilders) throws IOException {
		Object[] data = null;
		
		for(OrkBmeField builder : outputBuilders) {
			try {
				data = builder.getSolvedData(this);
				if(data != null && data.length > 0) {
				  printer.print('\"');
	        printer.printf(builder.getPrintFormat(), data);
          printer.print('\"');
				}
				printer.print(";");
			}
			catch(IllegalFormatException ifex) {
				LOG.error("[sendToPrinter] Error al tratar de aplicar el formato [{}] al array {} del builder {}", 
						builder.getPrintFormat(), data, builder.getName(), ifex);
				printer.print(ERROR_SIGNAL);
			}
		}
		printer.println();
	}
	
	void printError(PrintWriter printer) throws IOException {
	  printer.printf(CVS_FORMAT, failCause != null ? failCause.getMessage() : error);
	}
	
	void printId(PrintWriter printer) throws IOException {
	  for(OrkFidessaFixedField ff : fieldCollection.itIndexFields()) {
	    printer.printf(CVS_FORMAT, dynadata.get(ff.getName()));
	  }
	}
	
	void setSolved(String query, Object[] results) {
	  if(results.length == 1) {
	    dynadata.put(query, results[0]);
	  }
	  resolvedData.put(query, results);
	}
	
	void fail(Throwable cause) {
		success = false;
		failCause = cause;
		LOG.trace("[fail] Petición con excepcion. Datos: {}, mensaje: {}", dynadata, cause.getMessage());
	}
	
	void fail(String error) {
	  success = false;
	  this.error = error;
	  LOG.trace("[fail] Petición con error de validacion. Datos: {}, mensaje: {}", dynadata, error);
	}
	
  Object[] getParamValues(final String[] paramNames) {
    final Object[] paramValues;
    int i = 0;
    
    paramValues = new Object[paramNames.length];
    for(String name : paramNames) {
      paramValues[i++] = dynadata.get(name);
    }
    return paramValues;
  }

  private boolean sameRecord(ResultSet rs) throws SQLException {
		Object v;
		
		for(OrkFidessaFixedField ff : fieldCollection.itIndexFields()) {
			v = rs.getObject(ff.getName());
			if(v == null || !v.equals(dynadata.get(ff.getName()))) {
				return false;
			}
		}
		v = rs.getObject("ID_MIFID");
		if(v != null && dynadata.get("ID_MIFID") != null) {
		  return v.equals(dynadata.get("ID_MIFID"));
		}
		return true;
	}

	
}
