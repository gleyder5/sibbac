package sibbac.business.estaticos.megara.input;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import sibbac.business.estaticos.megara.inout.InstruccionLiquidacionSLAOutPut;
import sibbac.business.estaticos.utils.conversiones.ConvertirBoolean;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0aliBo;
import sibbac.database.megbpdta.bo.Fmeg0chaBo;
import sibbac.database.megbpdta.bo.Fmeg0suaBo;
import sibbac.database.megbpdta.bo.Fmeg0sucBo;
import sibbac.database.megbpdta.model.Fmeg0cha;

@Service
@Scope(value = "prototype")
public class SLASettlementChain {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(SLASettlementChain.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;
  
  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;

  @Autowired
  private Fmeg0chaBo fmeg0chaBo;

  @Autowired
  private Fmeg0sucBo fmeg0sucBo;

  @Autowired
  private Fmeg0aliBo fmeg0aliBo;

  @Autowired
  private Fmeg0suaBo fmeg0suaBo;
  
  @Autowired
  protected DirectoriosEstaticos directorios;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private static List<String> atrData;

  private Document document;

  private Fmeg0cha objFmeg0cha;

  public SLASettlementChain(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("identifier");
    atrData.add("description");
    atrData.add("dateFrom");
    atrData.add("dateTo");
    atrData.add("counterpartIdentifier");
    atrData.add("beneficiary");
    atrData.add("beneficiaryAccount");
    atrData.add("beneficiaryId");
    atrData.add("globalCustodian");
    atrData.add("globalCustodianAccount");
    atrData.add("globalCustodianId");
    atrData.add("localCustodian");
    atrData.add("localCustodianAccount");
    atrData.add("localCustodianId");
    atrData.add("sendBeneficiary");
    atrData.add("settlementType");
    atrData.add("broClient");
    atrData.add("alias");
    atrData.add("eligibleSubAccByAlias");
    atrData.add("market");
    atrData.add("placeOfSettlement");

    objFmeg0cha = new Fmeg0cha();
    objFmeg0cha.setCdinsliq("");
    objFmeg0cha.setDescript("");
    objFmeg0cha.setFhdafrom("");
    objFmeg0cha.setFhdateto("");
    objFmeg0cha.setIdcountp("");
    objFmeg0cha.setCdbenefi("");
    objFmeg0cha.setAcbenefi("");
    objFmeg0cha.setIdbenefi("");
    objFmeg0cha.setCdglocus("");
    objFmeg0cha.setAcglocus("");
    objFmeg0cha.setIdglocus("");
    objFmeg0cha.setCdloccus("");
    objFmeg0cha.setAcloccus("");
    objFmeg0cha.setIdloccus("");
    objFmeg0cha.setIssendbe("");
    objFmeg0cha.setTpsettle("");
    objFmeg0cha.setCdbrocli("");
    objFmeg0cha.setCdaliass("");
    objFmeg0cha.setCdsubcta("");
    objFmeg0cha.setCdmarket("");
    objFmeg0cha.setCdbicmar("");
    objFmeg0cha.setFhdebaja("");
    objFmeg0cha.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {

      if (INPUT.equals(document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue())) {
        input();
      }
      else {
        delete();
      }

      logger.debug("Archivo XML tratado correctamente");
      ctx.getBean(RegistroControl.class, "SettlementChain", objFmeg0cha.getCdinsliq(), nombreXML, "I", "Procesado fichero estatico",
          "SLASettlementChain", "MG").write();

      directorios.moverDirOk(nombreXML);

      try {

        /*
         * AML 23/02/2012 Genera la instrucci�n de liquidaci�n solo si la
         * cuenta, subcuenta y la relaci�n entre ellas est� activa
         */

        if (activadasCuentasSubcuentas()) {
          ctx.getBean(InstruccionLiquidacionSLAOutPut.class, objFmeg0cha).procesarSalidaSLA();

        }

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }

    }
    catch (Exception e) {
      logger.info(e.getMessage());
      ctx.getBean(RegistroControl.class, "SettlementChain", objFmeg0cha.getCdinsliq(), nombreXML, "E", e.getMessage(),
          "SLASettlementChain", "MG").write();
      directorios.moverDirError(nombreXML);
    }

  }

  private void input() throws Exception {

    String objPrimaryKey;
    Fmeg0cha objFmeg0cha_pers = new Fmeg0cha();

    tratarCampos();

    objPrimaryKey = objFmeg0cha.getCdinsliq();

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0cha_pers = fmeg0chaBo.findById(objPrimaryKey);

      if (objFmeg0cha_pers == null) {
        // INSERT
        objFmeg0cha.setTpmodeid(INPUT);
        objFmeg0cha = fmeg0chaBo.save(objFmeg0cha, true);
        tx.commit();
      }
      else if ("".equals(objFmeg0cha_pers.getFhdebaja().trim())) {
        // UPDATE
        fmeg0chaBo.delete(objFmeg0cha_pers);
        objFmeg0cha.setTpmodeid(UPDATE);
        objFmeg0cha = fmeg0chaBo.save(objFmeg0cha, true);

        tx.commit();
      }
      else {
        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0chaBo.delete(objFmeg0cha_pers);
        objFmeg0cha.setTpmodeid(INPUT);
        objFmeg0cha = fmeg0chaBo.save(objFmeg0cha, true);
        tx.commit();
      }
    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    String objPrimaryKey;
    Fmeg0cha objFmeg0cha_pers = new Fmeg0cha();

    tratarCampos();

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    objPrimaryKey = objFmeg0cha.getCdinsliq();

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0cha_pers = fmeg0chaBo.findById(objPrimaryKey);
      if (objFmeg0cha_pers == null) {
        throw new Exception("No existe la identificacion de la entidad para darlo de baja");
      }
      else if ("".equals(objFmeg0cha_pers.getFhdebaja().trim())) {
        objFmeg0cha_pers.setFhdebaja(fechaSt);
        objFmeg0cha_pers.setTpmodeid(DELETE);
        objFmeg0cha = fmeg0chaBo.save(objFmeg0cha_pers, false);
      }
      else {
        throw new Exception("El registro en cuestion se encuentra en estado de 'BAJA LOGICA'");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void tratarCampos() throws DOMException, Exception {

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// identifier
            objFmeg0cha.setCdinsliq(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// description
            objFmeg0cha.setDescript(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// dateFrom
            String anoF = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value").substring(6, 10);
            String mesF = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value").substring(3, 5);
            String diaF = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value").substring(0, 2);
            objFmeg0cha.setFhdafrom(anoF + mesF + diaF);
            break;
          case 3:// dateTo
            /*
             * AML 05/09/11 El valor por defecto ser� 31123000
             */
            String dateTo = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value");

            if ("".equals(dateTo.trim()))
              dateTo = "31/12/3000";

            String anoT = dateTo.substring(6, 10);
            String mesT = dateTo.substring(3, 5);
            String diaT = dateTo.substring(0, 2);
            objFmeg0cha.setFhdateto(anoT + mesT + diaT);
            break;
          case 4:// counterpartIdentifier
            objFmeg0cha.setIdcountp(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 5:// beneficiary
            objFmeg0cha.setCdbenefi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 6:// beneficiaryAccount
            objFmeg0cha.setAcbenefi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 7:// beneficiaryId
            objFmeg0cha.setIdbenefi(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 8:// globalCustodian
            objFmeg0cha.setCdglocus(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 9:// globalCustodianAccount
            objFmeg0cha.setAcglocus(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 10:// globalCustodianId
            objFmeg0cha.setIdglocus(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 11:// localCustodian
            objFmeg0cha.setCdloccus(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 12:// localCustodianAccount
            objFmeg0cha.setAcloccus(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 13:// localCustodianAccount
            objFmeg0cha.setIdloccus(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 14:// sendBeneficiary
            objFmeg0cha.setIssendbe(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 15:// settlementType

            objFmeg0cha.setTpsettle((convertirMegaraAs400).execConvertir("TPSETTLE", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;

          case 16:// broClient
            objFmeg0cha.setCdbrocli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 17:// alias
            objFmeg0cha.setCdaliass(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 18:// eligibleSubAccByAlias

            StringTokenizer tokens = new StringTokenizer(
                ((Element) dataElement.getChildNodes().item(i)).getAttribute("value"), "|");

            if (1 <= tokens.countTokens()) {
              objFmeg0cha.setCdsubcta(tokens.nextToken());
            }
            if (1 <= tokens.countTokens()) {
              objFmeg0cha.setCdbrocli(tokens.nextToken());
            }
            if (1 <= tokens.countTokens()) {
              objFmeg0cha.setCdaliass(tokens.nextToken());
            }

            break;

          case 19:// market
            objFmeg0cha.setCdmarket(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 20:// placeOfSettlement
            objFmeg0cha.setCdbicmar(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          default:
            ;
        }
      }
    }

  }

  /*
   * AML 23/02/2012 Genera la instrucci�n de liquidaci�n solo si la cuenta,
   * subcuenta y la relaci�n entre ellas est� activa
   */

  private boolean activadasCuentasSubcuentas() {

    final boolean activadas = subcuentaActiva() && cuentaActiva() && relacionActiva();

    return activadas;

  }

  private boolean subcuentaActiva() {

    boolean subcuentaActiva = false;

    final String subcta = objFmeg0cha.getCdsubcta();

    if (subcta == null || "".equals(subcta.trim())) {
      subcuentaActiva = true;
    }
    else {

      // transaccion
      EntityManager em = emf.createEntityManager();
      EntityTransaction tx = em.getTransaction();
      // tx.setRetainValues(true);

      try {

        tx.begin();
        Integer count = fmeg0sucBo.countByCdsubctaAndTpmodeidDistinctD(subcta);
        subcuentaActiva = count != null && count > 0;
        tx.commit();

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {
        if (tx.isActive()) {
          tx.rollback();
        }
        em.close();
      }
    }
    return subcuentaActiva;

  }

  private boolean cuentaActiva() {

    boolean cuentaActiva = false;

    final String alias = objFmeg0cha.getCdaliass();

    if (alias == null || "".equals(alias.trim())) {
      cuentaActiva = true;
    }
    else {

      // transaccion
      EntityManager em = emf.createEntityManager();
      EntityTransaction tx = em.getTransaction();

      try {

        tx.begin();
        Integer count = fmeg0aliBo.countByCdaliassAndTpmodeidDistinctD(alias);

        cuentaActiva = count != null && count > 0;

        tx.commit();

      }
      catch (Exception e) {
        logger.warn(e.getMessage());
      }
      finally {
        if (tx.isActive()) {

          tx.rollback();
        }
        em.close();
      }

    }

    return cuentaActiva;

  }

  private boolean relacionActiva() {

    boolean relacionActiva = false;

    final String alias = objFmeg0cha.getCdaliass();

    final String subcta = objFmeg0cha.getCdsubcta();

    if ((subcta == null || "".equals(subcta.trim()) || (alias == null || "".equals(alias.trim())))) {
      relacionActiva = true;
    }
    else {

      // transaccion
      EntityManager em = emf.createEntityManager();
      EntityTransaction tx = em.getTransaction();

      try {

        tx.begin();
        Integer count = fmeg0suaBo.countByCdaliassAndCdsubctaAndTpmodeidDistinctD(alias, subcta);
        relacionActiva = count != null && count > 0;

        tx.commit();

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {
        if (tx.isActive()) {

          tx.rollback();
        }
        em.close();
      }

    }

    return relacionActiva;

  }
}
