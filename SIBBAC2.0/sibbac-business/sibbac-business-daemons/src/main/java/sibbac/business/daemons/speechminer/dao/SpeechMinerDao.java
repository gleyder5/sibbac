package sibbac.business.daemons.speechminer.dao;


import sibbac.business.daemons.speechminer.dto.SpeechMinerResultDTO;


import java.util.List;


public interface SpeechMinerDao {
  public List<SpeechMinerResultDTO> getSpeechMinerResult();
}

