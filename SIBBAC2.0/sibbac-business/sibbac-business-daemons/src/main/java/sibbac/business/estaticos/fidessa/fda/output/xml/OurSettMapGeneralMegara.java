package sibbac.business.estaticos.fidessa.fda.output.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * OurSettMapGeneralMegara.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
public class OurSettMapGeneralMegara extends ConfigEstaticosMegaraGeneracionXmls {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OurSettMapGeneralMegara.class);

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /** The tipo actualizacion. */
  private String tipoActualizacion;

  /** The client_mnemonic. */
  private String client_mnemonic;

  /** The account_mnemonic. */
  private String account_mnemonic;

  /** The eligible_clearer_entity. */
  private String eligible_clearer_entity;

  /** The eligible_clearer_mkt. */
  private String eligible_clearer_mkt;

  /** The eligible_clearer_sett_type. */
  private String eligible_clearer_sett_type;

  /** The eligible_clearer_acc. */
  private String eligible_clearer_acc;

  /** The nombre xml. */
  private String nombreXML;

  /** The fecha. */
  private Date fecha;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new our sett map general_megara.
   */
  protected OurSettMapGeneralMegara() {
    this.tipoActualizacion = "";
    this.client_mnemonic = "";
    this.account_mnemonic = "";
    this.eligible_clearer_entity = "";
    this.eligible_clearer_mkt = "";
    this.eligible_clearer_sett_type = "";
    this.eligible_clearer_acc = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Limpiar obbj.
   */
  protected void limpiarObj() {
    this.tipoActualizacion = "";
    this.client_mnemonic = "";
    this.account_mnemonic = "";
    this.eligible_clearer_entity = "";
    this.eligible_clearer_mkt = "";
    this.eligible_clearer_sett_type = "";
    this.eligible_clearer_acc = "";

    this.nombreXML = "";

    this.fecha = new Date();

    this.mensajeError = "";
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar xml.
   * 
   * Genera la estructura del documento Xml, declaracion, arbol de elementos...
   */
  protected void generarXML() {

    logger.debug("ourSettMapGeneral: incio estructura documento XML");

    try {

      // ---------------------------------------------------------

      DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
      DateFormat horaFormato = new SimpleDateFormat("HH:mm:ss");
      String dateSt = fechaFormato.format(fecha) + " " + horaFormato.format(fecha);

      // ---------------------------------------------------------

      TransformerFactory transFactory = TransformerFactory.newInstance();
      Transformer transformer = transFactory.newTransformer();

      transformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.ENCODING, StandardCharsets.ISO_8859_1.name());
      transformer.setOutputProperty(javax.xml.transform.OutputKeys.VERSION, "1.0");

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder constructor = factory.newDocumentBuilder();

      Document document = constructor.newDocument();

      // ---------------------------------------------------------

      Element OUR_SETT_MAP_GENERAL = document.createElement("OUR_SETT_MAP_GENERAL");

      Element header = document.createElement("header");
      Element data = document.createElement("data");

      Element date = document.createElement("date");
      date.appendChild(document.createTextNode(dateSt));

      Element operation = document.createElement("operation");
      operation.appendChild(document.createTextNode(getTipoActualizacion()));

      Element comment = document.createElement("comment");
      comment.appendChild(document.createTextNode(generarComment()));

      header.appendChild(date);
      header.appendChild(operation);
      header.appendChild(comment);

      // ---------------------------------------------------------

      Element CLIENT_MNEMONIC = document.createElement("CLIENT_MNEMONIC");
      CLIENT_MNEMONIC.appendChild(document.createTextNode(getClient_mnemonic()));

      Element ACCOUNT_MNEMONIC = document.createElement("ACCOUNT_MNEMONIC");
      ACCOUNT_MNEMONIC.appendChild(document.createTextNode(getAccount_mnemonic()));

      Element ELIGIBLE_CLEARER_ENTITY = document.createElement("ELIGIBLE_CLEARER_ENTITY");
      ELIGIBLE_CLEARER_ENTITY.appendChild(document.createTextNode(getEligible_clearer_entity()));

      Element ELIGIBLE_CLEARER_MKT = document.createElement("ELIGIBLE_CLEARER_MKT");
      ELIGIBLE_CLEARER_MKT.appendChild(document.createTextNode(getEligible_clearer_mkt()));

      Element ELIGIBLE_CLEARER_SETT_TYPE = document.createElement("ELIGIBLE_CLEARER_SETT_TYPE");
      ELIGIBLE_CLEARER_SETT_TYPE.appendChild(document.createTextNode(getEligible_clearer_sett_type()));

      Element ELIGIBLE_CLEARER_ACC = document.createElement("ELIGIBLE_CLEARER_ACC");
      ELIGIBLE_CLEARER_ACC.appendChild(document.createTextNode(getEligible_clearer_acc()));

      // ---------------------------------------------------------

      data.appendChild(CLIENT_MNEMONIC);
      data.appendChild(ACCOUNT_MNEMONIC);
      data.appendChild(ELIGIBLE_CLEARER_ENTITY);
      data.appendChild(ELIGIBLE_CLEARER_MKT);
      data.appendChild(ELIGIBLE_CLEARER_SETT_TYPE);
      data.appendChild(ELIGIBLE_CLEARER_ACC);

      // ---------------------------------------------------------

      OUR_SETT_MAP_GENERAL.appendChild(header);
      OUR_SETT_MAP_GENERAL.appendChild(data);

      document.appendChild(OUR_SETT_MAP_GENERAL);

      logger.debug("ourSettMapGeneral: estructura documento XML completada");

      // Creacion del fichero XML.
      // ---------------------------------------------------------
      nombreXML = generarNombreXML(fecha);

      try (FileOutputStream fileXML = new FileOutputStream(new File(nombreXML));) {
        transformer.transform(new DOMSource(document), new StreamResult(new OutputStreamWriter(fileXML,
            StandardCharsets.ISO_8859_1)));
      }

      logger.debug("ourSettMapGeneral: docuemnto xml generado");

      // ---------------------------------------------------------

    }
    catch (Exception e) {
      logger.warn("ourSettMapGeneral: error en la generacion del docuemento XML");
      logger.warn("ourSettMapGeneral: " + e.getMessage());
      setMensajeError("EL DOCUMENTO XML NO SE HA GENERADO CORRECTAMENTE");
    }
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar comment.
   * 
   * Genera el contenido del campo comment que aparece en la cabecera del Xml
   * 
   * @return the string
   */
  private String generarComment() {
    String comment = "";

    if ("I".equals(getTipoActualizacion())) {
      comment = "Insertion of a our sett map general";
    }
    else {
      if ("U".equals(getTipoActualizacion())) {
        comment = "Update of a our sett map general";
      }
      else {
        if ("D".equals(getTipoActualizacion())) {
          comment = "Delete of a our sett map general";
        }
        else {
          comment = "";
        }
      }
    }

    logger.debug("ourSettMapGeneral: comment " + comment);

    return comment;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Generar nombre xml.
   * 
   * Genera el nombre del Xml en el formato especificado, GCMMdd0000.xml donde
   * GC es el identificador del modelo xml MMdd se corresponde con la fecha y
   * 0000 es un contador.
   * 
   * @param fecha the fecha
   * 
   * @return the string
   * 
   * @throws ContadoresException the contadores exception
   * @throws DirectoriosException the directorios exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private String generarNombreXML(Date fecha) throws Exception {

    String contador = contadores.getGeneralOurSettMap();
    Path path = Paths.get(getRutaDestino(), String.format("GC%s.xml", contador));
    String nombreXML = path.toString();

    logger.debug("GeneralOurSettMap: nombre XML {}", nombreXML);

    return nombreXML;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre corto xml.
   * 
   * Obtiene el nombre del documento Xml.
   * 
   * @return the nombre corto xml
   */
  /*
   * AML 20/09/11 Se modifica para controlar que el contador pueda exceder de 4
   * d�gitos
   */
  protected String getNombreCortoXML() {

    String nombre = this.nombreXML;

    if (nombre.length() > 15) {
      nombre = nombre.substring(nombre.length() - 15, nombre.length());
      if ("/".equals(nombre.substring(0, 1)))
        nombre = nombre.substring(1);

    }
    return nombre;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the account_mnemonic.
   * 
   * @return the account_mnemonic
   */
  protected String getAccount_mnemonic() {
    return account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the account_mnemonic.
   * 
   * @param account_mnemonic the new account_mnemonic
   */
  protected void setAccount_mnemonic(String account_mnemonic) {
    this.account_mnemonic = account_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the eligible_clearer_acc.
   * 
   * @return the eligible_clearer_acc
   */
  protected String getEligible_clearer_acc() {
    return eligible_clearer_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the eligible_clearer_acc.
   * 
   * @param eligible_clearer_acc the new eligible_clearer_acc
   */
  protected void setEligible_clearer_acc(String eligible_clearer_acc) {
    this.eligible_clearer_acc = eligible_clearer_acc;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the eligible_clearer_entity.
   * 
   * @return the eligible_clearer_entity
   */
  protected String getEligible_clearer_entity() {
    return eligible_clearer_entity;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the eligible_clearer_entity.
   * 
   * @param eligible_clearer_entity the new eligible_clearer_entity
   */
  protected void setEligible_clearer_entity(String eligible_clearer_entity) {
    this.eligible_clearer_entity = eligible_clearer_entity;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the eligible_clearer_mkt.
   * 
   * @return the eligible_clearer_mkt
   */
  protected String getEligible_clearer_mkt() {
    return eligible_clearer_mkt;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the eligible_clearer_mkt.
   * 
   * @param eligible_clearer_mkt the new eligible_clearer_mkt
   */
  protected void setEligible_clearer_mkt(String eligible_clearer_mkt) {
    this.eligible_clearer_mkt = eligible_clearer_mkt;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the eligible_clearer_sett_type.
   * 
   * @return the eligible_clearer_sett_type
   */
  protected String getEligible_clearer_sett_type() {
    return eligible_clearer_sett_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the eligible_clearer_sett_type.
   * 
   * @param eligible_clearer_sett_type the new eligible_clearer_sett_type
   */
  protected void setEligible_clearer_sett_type(String eligible_clearer_sett_type) {
    this.eligible_clearer_sett_type = eligible_clearer_sett_type;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the tipo actualizacion.
   * 
   * @return the tipo actualizacion
   */
  protected String getTipoActualizacion() {
    return tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the tipo actualizacion.
   * 
   * @param tipoActualizacion the new tipo actualizacion
   */
  protected void setTipoActualizacion(String tipoActualizacion) {
    this.tipoActualizacion = tipoActualizacion;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the client_mnemonic.
   * 
   * @return the client_mnemonic
   */
  protected String getClient_mnemonic() {
    return client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the client_mnemonic.
   * 
   * @param client_mnemonic the new client_mnemonic
   */
  protected void setClient_mnemonic(String client_mnemonic) {
    this.client_mnemonic = client_mnemonic;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the fecha.
   * 
   * @return the fecha
   */
  protected Date getFecha() {
    return fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the fecha.
   * 
   * @param fecha the new fecha
   */
  protected void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the mensaje error.
   * 
   * @return the mensaje error
   */
  protected String getMensajeError() {
    return mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the mensaje error.
   * 
   * @param mensajeError the new mensaje error
   */
  protected void setMensajeError(String mensajeError) {
    this.mensajeError = mensajeError;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Gets the nombre xml.
   * 
   * @return the nombre xml
   */
  protected String getNombreXML() {
    return nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Sets the nombre xml.
   * 
   * @param nombreXML the new nombre xml
   */
  protected void setNombreXML(String nombreXML) {
    this.nombreXML = nombreXML;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}
