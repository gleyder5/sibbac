/**
 * 
 */
package sibbac.business.daemons.confirmacionminotista.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.RecordBean;

/**
 * @author xIS16630
 *
 */
public class ConfirmacionMinorista17RecordBean extends RecordBean {

  /**
   * 
   */
  private static final long serialVersionUID = 2075517257980540245L;

  private String nuorden;
  private String nbooking;
  private Date fealta;
  private Date hoalta;
  private Date fecontra;
  private Date fevalor;
  private BigDecimal imefeagr = BigDecimal.ZERO;
  private BigDecimal nutitliq = BigDecimal.ZERO;
  private BigDecimal numerico1 = BigDecimal.ZERO;
  private BigDecimal imcbmerc = BigDecimal.ZERO;
  private String cdmoniso;
  private String plataformaNegociacion;
  private String codigoEcc;
  private BigDecimal numerico2 = BigDecimal.ZERO;
  private BigDecimal numerico3 = BigDecimal.ZERO;
  private BigDecimal imcomisn = BigDecimal.ZERO;
  private BigDecimal imcontr = BigDecimal.ZERO;
  private Character nvalue;
  private BigDecimal numerico4 = BigDecimal.ZERO;
  private BigDecimal numerico5 = BigDecimal.ZERO;
  private String filler;

  public ConfirmacionMinorista17RecordBean() {
    super();
    setTipoRegistro("17");
  }

  public ConfirmacionMinorista17RecordBean(String nuorden,
                                           String nbooking,
                                           Date fealta,
                                           Date hoalta,
                                           Date fecontra,
                                           Date fevalor,
                                           BigDecimal imefeagr,
                                           BigDecimal nutitliq,
                                           BigDecimal numerico1,
                                           BigDecimal imcbmerc,
                                           String cdmoniso,
                                           String plataformaNegociacion,
                                           String codigoEcc,
                                           BigDecimal numerico2,
                                           BigDecimal numerico3,
                                           BigDecimal imcomisn,
                                           BigDecimal imcontr,
                                           Character nvalue,
                                           BigDecimal numerico4,
                                           BigDecimal numerico5,
                                           String filler) {
    this();
    this.nuorden = nuorden;
    this.nbooking = nbooking;
    this.fealta = fealta;
    this.hoalta = hoalta;
    this.fecontra = fecontra;
    this.fevalor = fevalor;
    this.imefeagr = imefeagr;
    this.nutitliq = nutitliq;
    this.numerico1 = numerico1;
    this.imcbmerc = imcbmerc;
    this.cdmoniso = cdmoniso;
    this.plataformaNegociacion = plataformaNegociacion;
    this.codigoEcc = codigoEcc;
    this.numerico2 = numerico2;
    this.numerico3 = numerico3;
    this.imcomisn = imcomisn;
    this.imcontr = imcontr;
    this.nvalue = nvalue;
    this.numerico4 = numerico4;
    this.numerico5 = numerico5;
    this.filler = filler;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public void setNbooking(String nbooking) {
    this.nbooking = nbooking;
  }

  public void setFealta(Date fealta) {
    this.fealta = fealta;
  }

  public void setHoalta(Date hoalta) {
    this.hoalta = hoalta;
  }

  public void setFecontra(Date fecontra) {
    this.fecontra = fecontra;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setImefeagr(BigDecimal imefeagr) {
    this.imefeagr = imefeagr;
  }

  public void setNutitliq(BigDecimal nutitliq) {
    this.nutitliq = nutitliq;
  }

  public void setNumerico1(BigDecimal numerico1) {
    this.numerico1 = numerico1;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  public void setCdmoniso(String cdmoniso) {
    this.cdmoniso = cdmoniso;
  }

  public void setPlataformaNegociacion(String plataformaNegociacion) {
    this.plataformaNegociacion = plataformaNegociacion;
  }

  public void setCodigoEcc(String codigoEcc) {
    this.codigoEcc = codigoEcc;
  }

  public void setNumerico2(BigDecimal numerico2) {
    this.numerico2 = numerico2;
  }

  public void setNumerico3(BigDecimal numerico3) {
    this.numerico3 = numerico3;
  }

  public void setImcomisn(BigDecimal imcomisn) {
    this.imcomisn = imcomisn;
  }

  public void setImcontr(BigDecimal imcontr) {
    this.imcontr = imcontr;
  }

  public void setNvalue(Character nvalue) {
    this.nvalue = nvalue;
  }

  public void setNumerico4(BigDecimal numerico4) {
    this.numerico4 = numerico4;
  }

  public void setNumerico5(BigDecimal numerico5) {
    this.numerico5 = numerico5;
  }

  public void setFiller(String filler) {
    this.filler = filler;
  }

  public String getNbooking() {
    return nbooking;
  }

  public Date getFealta() {
    return fealta;
  }

  public Date getHoalta() {
    return hoalta;
  }

  public Date getFecontra() {
    return fecontra;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public BigDecimal getImefeagr() {
    return imefeagr;
  }

  public BigDecimal getNutitliq() {
    return nutitliq;
  }

  public BigDecimal getNumerico1() {
    return numerico1;
  }

  public BigDecimal getImcbmerc() {
    return imcbmerc;
  }

  public String getCdmoniso() {
    return cdmoniso;
  }

  public String getPlataformaNegociacion() {
    return plataformaNegociacion;
  }

  public String getCodigoEcc() {
    return codigoEcc;
  }

  public BigDecimal getNumerico2() {
    return numerico2;
  }

  public BigDecimal getNumerico3() {
    return numerico3;
  }

  public BigDecimal getImcomisn() {
    return imcomisn;
  }

  public BigDecimal getImcontr() {
    return imcontr;
  }

  public Character getNvalue() {
    return nvalue;
  }

  public BigDecimal getNumerico4() {
    return numerico4;
  }

  public BigDecimal getNumerico5() {
    return numerico5;
  }

  public String getFiller() {
    return filler;
  }

  public String getNbookingSt() {
    String value = null;
    if (nbooking != null) {
      value = StringUtils.substring(nbooking, 0, 16);
    }
    return value;
  }

  public String getFealtaSt() {
    String value = null;
    if (fealta != null) {
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
      value = df.format(fealta);
    }
    return value;
  }

  public String getHoaltaSt() {
    String value = null;
    if (hoalta != null) {
      SimpleDateFormat df = new SimpleDateFormat("HHmmss");
      value = df.format(hoalta);
    }
    return value;
  }

  public String getFecontraSt() {
    String value = null;
    if (fecontra != null) {
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
      value = df.format(fecontra);
    }
    return value;
  }

  public String getFevalorSt() {
    String value = null;
    if (fevalor != null) {
      SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
      value = df.format(fevalor);
    }
    return value;
  }

  public String getImefeagrSt() {
    String value = null;
    if (imefeagr != null) {
      value = StringHelper.formatBigDecimalWithSign(imefeagr, 13, 2);
    }
    return value;
  }

  public String getNutitliqSt() {
    String value = null;
    if (nutitliq != null) {
      value = StringHelper.formatBigDecimal(nutitliq, 11, 0);
    }
    return value;
  }

  public String getNumerico1St() {
    String value = null;
    if (numerico1 != null) {
      value = StringHelper.formatBigDecimal(numerico1, 13, 0);
    }
    return value;
  }

  public String getImcbmercSt() {
    String value = null;
    if (imcbmerc != null) {
      value = StringHelper.formatBigDecimal(imcbmerc, 9, 9);
    }
    return value;
  }

  public String getCdmonisoSt() {
    String value = null;
    if (cdmoniso != null) {
      value = StringUtils.substring(cdmoniso, 0, 3);
    }
    return value;
  }

  public String getPlataformaNegociacionSt() {
    String value = null;
    if (plataformaNegociacion != null) {
      value = StringUtils.substring(plataformaNegociacion, 0, 4);
    }
    return value;
  }

  public String getCodigoEccSt() {
    String value = null;
    if (codigoEcc != null) {
      value = StringUtils.substring(codigoEcc, 0, 11);
    }
    return value;
  }

  public String getNumerico2St() {
    String value = null;
    if (numerico2 != null) {
      value = StringHelper.formatBigDecimalWithSign(numerico2, 11, 0);
    }
    return value;
  }

  public String getNumerico3St() {
    String value = null;
    if (numerico3 != null) {
      value = StringHelper.formatBigDecimalWithSign(numerico3, 11, 0);
    }
    return value;
  }

  public String getImcomisnSt() {
    String value = null;
    if (imcomisn != null) {
      value = StringHelper.formatBigDecimalWithSign(imcomisn, 9, 2);
    }
    return value;
  }

  public String getImcontrSt() {
    String value = null;
    if (imcontr != null) {
      value = StringHelper.formatBigDecimalWithSign(imcontr, 9, 2);
    }
    return value;
  }

  public String getNvalueSt() {
    String value = null;
    if (nvalue != null) {
      value = StringUtils.substring(nvalue + "", 0, 1);
    }
    return value;
  }

  public String getNumerico4St() {
    String value = null;
    if (numerico4 != null) {
      value = StringHelper.formatBigDecimalWithSign(numerico4, 11, 0);
    }
    return value;
  }

  public String getNumerico5St() {
    String value = null;
    if (numerico5 != null) {
      value = StringHelper.formatBigDecimalWithSign(numerico5, 11, 0);
    }
    return value;
  }

  public String getFillerSt() {
    String value = null;
    if (filler != null) {
      value = StringUtils.substring(filler, 0, 57);
    }
    return value;
  }

  @Override
  public String toString() {
    return "ConfirmacionMinorista17RecordBean [nuorden=" + nuorden + ", nbooking=" + nbooking + ", fealta=" + fealta
           + ", hoalta=" + hoalta + ", fecontra=" + fecontra + ", fevalor=" + fevalor + ", imefeagr=" + imefeagr
           + ", nutitliq=" + nutitliq + ", numerico1=" + numerico1 + ", imcbmerc=" + imcbmerc + ", cdmoniso="
           + cdmoniso + ", plataformaNegociacion=" + plataformaNegociacion + ", codigoEcc=" + codigoEcc
           + ", numerico2=" + numerico2 + ", numerico3=" + numerico3 + ", imcomisn=" + imcomisn + ", imcontr="
           + imcontr + ", nvalue=" + nvalue + ", numerico4=" + numerico4 + ", numerico5=" + numerico5 + ", filler="
           + filler + "]";
  }

}
