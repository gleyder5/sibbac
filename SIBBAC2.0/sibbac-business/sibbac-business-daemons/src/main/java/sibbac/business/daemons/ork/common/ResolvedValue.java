package sibbac.business.daemons.ork.common;

public interface ResolvedValue {
  
  Object[] getDynaData(String name);
  
  Object[] getResolvedData(String name);

}
