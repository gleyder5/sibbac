package sibbac.business.daemons.batch.model;

import static java.util.Objects.requireNonNull;

import sibbac.business.daemons.batch.common.BatchException;

/**
 * Clase base de todas las implementaciones de los Step de un Batch.
 */
abstract class AbstractStep implements Step {
	
	/**
	 * Retiene el entorno de ejecucion que ha sido alimentado por el método execute, aislandolo de las subclases
	 */
  private BatchEnvironment environment;
	
  /**
   * @see Step#execute(BatchEnvironment)
   */
	@Override
	public void execute(final BatchEnvironment environment) throws BatchException {
		this.environment = requireNonNull(environment, "El entorno Batch no puede ser nulo");
		execute();
	}
	
	/**
	 * Solicita la ejecucion del Step
	 * @throws BatchException En caso de error de lógica expecífica del Batch
	 */
  abstract void execute() throws BatchException;
	
	/**
	 * Solicita al {@link #environment} que ejecute una sentencia SQL
   * @see sibbac.business.daemons.batch.model.Query#TO_MAIL
   * @see sibbac.business.daemons.batch.model.Query#TO_BUFFER
   * @see sibbac.business.daemons.batch.model.Query#NORMAL
   * 
	 * @param query SQL a ejecutar
	 * @param type Tipo del query del Step 
	 * @throws BatchException en caso de error 
	 */
  final void executeStatement(String query, char type) throws BatchException {
	  environment.executeStatement(query, type);
	}
	
}