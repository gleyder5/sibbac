package sibbac.business.daemons.batch.model;

import java.util.List;
import java.util.ArrayList;
import static java.text.MessageFormat.format;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.common.BatchException;

class SameLineStep extends BufferedStep  {
	
	private static final Logger LOG = getLogger(SameLineStep.class);
	
	private final List<Query> queries;
	
	SameLineStep(List<Query> queries) {
	  this.queries = queries;
	}
	
  @Override
  public boolean sendToMail() {
    for(Query query : queries) {
      if(query.getType() == Query.TO_MAIL) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean sentToBuffer() {
    for(Query query : queries) {
      if(query.getType() == Query.TO_BUFFER) {
        return true;
      }
    }
    return false;
  }

  @Override
	void execute(String values[]) throws BatchException {
    if(sentToBuffer()) {
      byStatement(values);
    }
    else {
      byBatch(values);
    }
  }

  private void byBatch(final String values[]) throws BatchException {
    final List<String> sentences;
    final StringBuilder sqlBuilder;
    String sql, message;
    int i = 0;
    
    sqlBuilder = new StringBuilder();
    sentences = new ArrayList<>();
    try {
      for(final Query query : queries) {
        sqlBuilder.append(query.getQuery());
        fillParameters(sqlBuilder, query.getParameters(), values);
        sql = sqlBuilder.toString();
        sentences.add(sql);
        sqlBuilder.setLength(0);
      }
      batchExecute(sentences);
    }
    catch(BatchException sqlex) {
      message = format("Error al ejecutar el la query {0} con los valores {1} ", 
          queries.get(i).getId().toString(), values);
      LOG.error("[byBatch] {}", message, sqlex);
      throw new BatchException(message, sqlex);
    }
  }

  private void byStatement(final String values[]) throws BatchException {
    final StringBuilder sqlBuilder;
    final String message;
    int i = 0;
    
    sqlBuilder = new StringBuilder();
    try {
      for(final Query query : queries) {
        sqlBuilder.append(query.getQuery());
        fillParameters(sqlBuilder, query.getParameters(), values);
        executeStatement(sqlBuilder.toString(), query.getType());
        sqlBuilder.setLength(0);
        i++;
      }
    }
    catch(BatchException sqlex) {
      message = format("Error al ejecutar el la query {0} con los valores {1} ", 
          queries.get(i).getId().toString(), values);
      LOG.error("[byStatement] {}", message, sqlex);
      throw new BatchException(message, sqlex);
    }
  }

}