package sibbac.business.daemons.ork.model;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OrderBy;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;

import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.common.SIBBACBusinessException;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * Entidad que representa una consulta a base de datos para recuperar información de SIBBAC que no se encuentra
 * en el fichero Fidessa de MiFID II, pero que es necesario enviar al regulador. 
 * <p/>
 * En esta documentación se hace referencia a dos tipos de parametros para la Query:
 * <ul> <li> por etiqueta </li> <li> por posición </li></ul>
 * Los parámetros por etiqueta son nombres precedidos por el signo de dos puntos (':'). Se reemplazan al comienzo 
 * del proceso con valores provenientes del fichero de configuración sibbac.mercados.properties
 * <p/>
 * Los parámetros por posición son signos de interrogación ('?') que se sustituyen por cada una de las instancias 
 * de la entidad OrkFidessa.
 * @author XI326526
 */
@Entity
@Table(name = "TMCT0_ORK_BME_QUERY")
public class OrkBmeQuery implements Serializable {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 1307577819902427241L;
  
  private static final Logger LOG = getLogger(OrkBmeQuery.class);

  /**
   * Nombre como clave primaria de la consulta
   */
  @Id
  @Column(name = "NAME", length = 50)
  private String name;
  
  /**
   * Consulta SQL sin substituir las propiedades
   */
  @Lob
  @Column(name = "SQL_QUERY" )
  private String sqlQuery;
  
  /**
   * Consulta que se ejecuta en caso de que la actual falle o no regrese resultados
   */
  @OneToOne(optional = true)
  @JoinColumn(name = "FAIL")
  private OrkBmeQuery fail;
  
  @OneToMany
  @JoinColumns({@JoinColumn(name = "NAME")})
  @OrderBy("id.pos")
  private List<OrkBmeQueryParam> params;
  
  /**
   * Consulta SQL con las propiedades substituidas
   */
  @Transient
  private String finalQuery;
  
  /**
   * @param name Determina el nombre de la consulta.
   */
  public void setName(String name) {
    this.name = name;
  }
  
  /**
   * @return el nombre de la consulta.
   */
  public String getName() {
    return name;
  }

  /**
   * @return consulta SQL que se ejecutara.
   */
  public String getSqlQuery() {
    return sqlQuery;
  }

  /**
   * @param sqlQuery determina la consulta SQL que se ejecutará. No es SQL puro, ya que acepta dos tipos de parámetros.
   * Los parametros de un nombre precedido con un signo de "dos puntos" (:) busca el nombre en el fichero de 
   * configuracion sibbac.mercados.properties y lo reemplaza al principio del proceso. Los parámetros representados con
   * signos de interrogación cumplen las convenciones para los {@link java.sql.PreparedStatement} y son reemplazados 
   * por los valores obtenidos de los campos {@link #getParams()}
   */
  public void setSqlQuery(String sqlQuery) {
    this.sqlQuery = sqlQuery;
  }

  /**
   * @return Consulta fail, si existe. En caso contrario un valor nulo.
   */
  public OrkBmeQuery getFail() {
    return fail;
  }

  /**
   * @param fail Determina la consulta que se ejecutará si la presente no regresa resultados o produce un error. En 
   * caso de ser valor nulo, no se continuará la ejecución de consultas.
   */
  public void setFail(OrkBmeQuery fail) {
    this.fail = fail;
  }

  /**
   * @return colección de parámetros para esta consulta
   */
  public List<OrkBmeQueryParam> getParams() {
    return params;
  }

  /**
   * @param params Determina la colección de parámetros que reemplazarán los signos de interrogación (?) en la consulta
   */
  public void setParams(List<OrkBmeQueryParam> params) {
    this.params = params;
  }

  /**
   * @return Array de cadenas con los nombres de los campos fidessa que reemplazaran con su valor los signos 
   * de interrogación de la consulta (?).
   */
  @Transient
  public String[] getParamNames() {
    final String[] names;
    int i = 0;
    
    names = new String[params.size()];
    for(OrkBmeQueryParam p : params) {
      names[i++] = p.getOrkFidessaField().getName();
    }
    return names;
  }
  
  /**
   * @return Cadena SQL que ha sustituido por valores del fichero de configuración los parámetros por nombre.
   */
  @Transient
  public String getFinalQuery() {
    return finalQuery;
  }
  
  /**
   * Reemplaza los parámetros por nombre en la consulta {@link #sqlQuery} y asigna el resultado al campo
   * {@link finalQuery}. Es recursivo con su campo {@link #fail}
   * @param config Objetoc con la información del fichero de propiedades sibbac.mercados.properties
   * @throws SIBBACBusinessException En caso de error al acceder a las propiedades del objeto config.
   */
  @Transient
  void loadProperties(OrkMercadosConfig config) throws SIBBACBusinessException {
    final StringBuilder sqlBuilder;
    String propertyName, propertyValue;
    int pos, finalPropertyPos, subs = 0;
    
    sqlBuilder = new StringBuilder(sqlQuery);
    while((pos = sqlBuilder.indexOf(":")) >= 0) {
      finalPropertyPos = pos + sqlBuilder.substring(pos).indexOf(' ');
      propertyName = sqlBuilder.substring(pos + 1, finalPropertyPos);
      propertyValue = String.format("'%s'", config.getProperty(propertyName));
      sqlBuilder.replace(pos, finalPropertyPos, propertyValue);
      subs++;
    }
    finalQuery = sqlBuilder.toString().trim();
    LOG.info("[loadProperties] Sentencia obtenida para {} despues de {} substituciones: {}", name, subs, finalQuery);
    if(fail != null) {
      fail.loadProperties(config);
    }
  }

  /**
   * @return un conjunto de los nombres de todos los parametros de esta consulta de y de las consultas fail de manera
   * recursiva. El orden importa y es el de la primera aparición de ese nombre de parámetro.
   */
  @Transient
  Set<String> getParamNamesForKey() {
    final Set<String> pnames;
    
    pnames = new LinkedHashSet<>();
    for(OrkBmeQueryParam p : params) {
      pnames.add(p.getOrkFidessaField().getName());
    }
    if(fail != null) {
      pnames.addAll(fail.getParamNamesForKey());
    }
    LOG.debug("[getParamNamesForKey] query {} retorna los nombres {}", name, pnames);
    return pnames;
  }
  
}
