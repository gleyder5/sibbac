package sibbac.business.daemons.ork.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.StandardCopyOption;
import static java.nio.file.Files.move;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

class FidessaFilter implements FileFilter {
	
	private static final Logger LOG = getLogger(FidessaFilter.class);
	
  private static final int COMILLAS = 34;
  
  private static final String FIRST_FIELD = "EXCHANGE_ORDER_ID";
  
	private final String prefix;
	
	private final String suffix;
	
	FidessaFilter(String pattern) {
		final int pos;
		
		pos = pattern.lastIndexOf("*");
		prefix = pattern.substring(0, pos);
		suffix = pattern.substring(pos + 1);
	}

	@Override
	public boolean accept(final File originalFile) {
		final String name;
		
		name = originalFile.getName();
		if(!(name.startsWith(prefix) && name.endsWith(suffix))) {
			return false;
		}
		try {
			return validateFile(originalFile);
		}
		catch(IOException | RuntimeException ex) {
			LOG.error("Error al tratar de determinar el contenido del fichero {}. Se rechaza", 
					originalFile.getAbsolutePath(), ex);
			return false;
		}
	}
	
	private boolean validateFile(final File originalFile) throws IOException {
		final String filename;
		File transformed = null;
		String line;
		
		filename = originalFile.getAbsolutePath();
		try(InputStream is = new FileInputStream(originalFile);
				BufferedReader buffReader = new BufferedReader(new InputStreamReader(is, "UTF-8"))) {
			line = buffReader.readLine();
			if(line == null) {
				LOG.info("Fichero {} se rechaza al estar vacio", filename);
				return false;
			}
			if(line.isEmpty()) {
				LOG.info("Fichero {} se rechaza al estar su primera línea vacía", filename);
				return false;
			}
			if(line.charAt(0) == COMILLAS) {
				LOG.info("Fichero {} se acepta sin transformar", filename);
				return true;
			}
			if(!line.startsWith(FIRST_FIELD)) {
				LOG.info("Fichero {} se rechaza al no iniciar con la cadena {}", filename, FIRST_FIELD);
				return false;
			}
			transformed = doTransformation(originalFile.getParentFile(), originalFile.getName(), buffReader);
		}
		if(transformed != null) {
			move(transformed.toPath(), originalFile.toPath(), StandardCopyOption.ATOMIC_MOVE,
					StandardCopyOption.REPLACE_EXISTING);
		}
		return true;
	}
	
	private File doTransformation(File dir, String filename, BufferedReader buffReader) throws IOException {
		final File transformed;
		String line, split[];
		
		transformed = new File(dir, String.format("TEMP_%s", filename));
		try(OutputStream os = new FileOutputStream(transformed);
				PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os)))) {
			while((line = buffReader.readLine()) != null) {
				split = line.split(",");
				for(String field : split) {
					if(!field.trim().isEmpty()) {
						writer.printf("\"%s\"", field);
					}
					writer.print(';');
				}
				writer.println();
			}
		}
		return transformed;
	}
	
}