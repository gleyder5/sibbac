package sibbac.business.daemons.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_REPARTO_FICHEROS_ESTATICOS, jobType = SIBBACJobType.CRON_JOB, cronExpression = "45 * 7-23 ? * MON-FRI")
public class TaskRepartoFicherosEstaticos extends WrapperTaskConcurrencyPrevent {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(TaskRepartoFicherosEstaticos.class);

  @Value("${tactico.estaticos.folder.out:}")
  private String rutaInTactico;

  @Value("${estaticos.file.in.pattern:}")
  private String filePatternInTactico;

  @Value("${estaticos.as400.ftp.folder.out:}")
  private String rutaOutAs400;

  @Value("${estaticos.folder.in:}")
  private String rutaOutEstaticos;

  @Override
  public void executeTask() throws Exception {
    LOG.info("[executeTask] inicio buscando ficheros en {} pattern {}", rutaInTactico, filePatternInTactico);
    Path outAs400;
    int count = 0;
    ZipEntry entry;
    Path outEstaticos;

    Path pathIn = Paths.get(rutaInTactico);
    Path pathTratados = Paths.get(rutaInTactico, "tratados");
    Path pathOutAs400 = Paths.get(rutaOutAs400);
    Path pathOutEstaticos = Paths.get(rutaOutEstaticos);
    if (Files.notExists(pathOutEstaticos)) {
      Files.createDirectories(pathOutEstaticos);
    }
    if (Files.notExists(pathOutAs400)) {
      Files.createDirectories(pathOutAs400);
    }

    if (Files.notExists(pathTratados)) {
      Files.createDirectories(pathTratados);
    }
    else {

      String backupFile = String.format("tratados_%s.zip", FormatDataUtils.convertDateToConcurrentFileName(new Date()));
      Path zip = Paths.get(pathTratados.toString(), backupFile);
      LOG.trace("[executeTask] abriendo directory stream...");
      try (OutputStream zipos = Files.newOutputStream(zip, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
          ZipOutputStream zos = new ZipOutputStream(zipos);) {
        try (DirectoryStream<Path> ficheros = Files.newDirectoryStream(pathIn, filePatternInTactico)) {
          LOG.trace("[executeTask] abriendo zip para backup...");

          LOG.trace("[executeTask] recuperando ficheros con el pattern...");
          for (Path path : ficheros) {
            // para as400
            outAs400 = Paths.get(rutaOutAs400, path.getFileName().toString());
            // para los estaticos
            outEstaticos = Paths.get(rutaOutEstaticos, path.getFileName().toString());
            LOG.trace("[executeTask] creando nombres fichero para as400: {} y estaticos: {}...", outAs400.toString(),
                outEstaticos.toString());
            try {
              // as400
              LOG.info("[executeTask] generando copia del fichero para as400: {}", outAs400.toString());
              try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);
                  OutputStream os = Files.newOutputStream(outAs400, StandardOpenOption.CREATE_NEW,
                      StandardOpenOption.WRITE);) {
                IOUtils.copy(is, os);
              }

              // estaticos
              LOG.info("[executeTask] generando copia del fichero para estaticos: {}", outEstaticos.toString());
              try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);
                  OutputStream os = Files.newOutputStream(outEstaticos, StandardOpenOption.CREATE_NEW,
                      StandardOpenOption.WRITE);) {
                IOUtils.copy(is, os);
              }

              // zip
              LOG.trace("[executeTask] aniadiendo fichero: {} al zip de backup: {}", path.toString(), zip.toString());
              try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);) {
                entry = new ZipEntry(path.getFileName().toString());
                zos.putNextEntry(entry);
                IOUtils.copy(is, zos);
              }
              zos.closeEntry();
              LOG.trace("[executeTask] borrando el fichero origen: {}", path.toString());
              Files.deleteIfExists(path);
              count++;
            }
            catch (IOException e) {
              Files.deleteIfExists(outEstaticos);
              Files.deleteIfExists(outAs400);
              throw e;
            }
          }
        }
      }
      catch (Exception e) {
        throw e;
      }
      finally {
        if (count == 0) {
          LOG.info("[executeTask] borrando fichero backup: {}, no se ha enviado nada...", zip.toString());
          Files.deleteIfExists(zip);
        }
      }
    }
    LOG.info("[executeTask] fin");
  }

  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_REPARTO_FICHEROS_ESTATICOS;
  }

}
