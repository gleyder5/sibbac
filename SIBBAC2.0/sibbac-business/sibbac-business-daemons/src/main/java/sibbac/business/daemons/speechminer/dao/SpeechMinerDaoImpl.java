package sibbac.business.daemons.speechminer.dao;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.business.daemons.speechminer.dto.SpeechMinerResultDTO;

@Repository
public class SpeechMinerDaoImpl implements SpeechMinerDao {
	private static final String QUERY =  " select cli.tpfisjur, cli.cdbrocli cdbrocli, cli.descli descli, ali.cdaliass cdaliass," +
            "       ali.descrali descrali, cli.tpidenti, cli.cif cif, cli.cdcat cdcat, cli.cdkgr cdkgr," +
            "       cli.cdbdp cdbdp, cli.tpclienn tpclienn " +
            " from bsnbpsql.tmct0ali ali " +
            "       join bsnbpsql.tmct0cli cli on ali.cdbrocli=cli.cdbrocli " +
            " where ali.fhfinal>= TO_CHAR(CURRENT_DATE,'YYYYMMDD') " +
            "  and cli.cdestado<> 'B'  ORDER by ali.cdaliass";
 
    @Autowired
    private EntityManager entityManager;

    public static <T> T map(Class<T> type, Object[] tuple){
        List<Class<?>> tupleTypes = new ArrayList<>();
        for(Object field : tuple){
            tupleTypes.add(field.getClass());
        }
        try {
            Constructor<T> ctor = type.getConstructor(tupleTypes.toArray(new Class<?>[tuple.length]));
            return ctor.newInstance(tuple);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public static <T> List<T> map(Class<T> type, List<Object[]> records){
        List<T> result = new LinkedList<>();
        for(Object[] record : records){
            result.add(map(type, record));
        }
        return result;
    }

    public static <T> List<T> getResultList(Query query, Class<T> type){
        List<Object[]> records  = null;
        try {
         records = query.getResultList();
        }catch(Exception ex) {
        	ex.printStackTrace();
        }
        return map(type, records);
    }

    @Override
    public List<SpeechMinerResultDTO> getSpeechMinerResult() {              

        Query query = entityManager.createNativeQuery(QUERY);
        List<SpeechMinerResultDTO> resultDTOList  = new ArrayList<>();
        @SuppressWarnings("unchecked")
        List<Object[]> objectList = (List<Object[]>) query.getResultList();
        resultDTOList = convertList(objectList);
        //List<SpeechMinerResultDTO> resultDTOList = getResultList(entityManager.createNativeQuery(qry),SpeechMinerResultDTO.class);
        return resultDTOList;
    }

    private List<SpeechMinerResultDTO> convertList(List<Object[]> objectList) {
        List<SpeechMinerResultDTO> resultDTOList = new ArrayList();

        if ((objectList != null) && (!objectList.isEmpty())) {

            for (Object[] object : objectList) {
                final Character tpfisjur = (Character) object[0];
                final String cdbrocli = (String) object[1];
                final String descli = (String) object[2];
                final String cdaliass = (String) object[3];
                final String descrali = (String) object[4];
                final Character tpidenti = (Character) object[5];
                final String cif = (String) object[6];
                final String cdcat = (String) object[7];
                final String cdkgr = (String) object[8];
                final String cdbdp = (String) object[9];
                final Character tpclienn = (Character) object[10];
                resultDTOList.add(new SpeechMinerResultDTO(tpfisjur,cdbrocli,descli,cdaliass,descrali,cif,tpidenti,cdcat,cdkgr,cdbdp,tpclienn));
            }
        }
        return resultDTOList;
    }
}
