package sibbac.business.daemons.confirmacionminotista.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import sibbac.business.wrappers.common.StringHelper;
import sibbac.business.wrappers.database.model.RecordBean;

/**
 * Formatter
 * %-2s%-16s%-8s%-6s%-19s%-3s%-20s%-1s%-12s%-1s%-10s%-4s%-8s%-11s%-20s%-10s%-7s%-16s%-4s%-4s%-4s%-20s%-6s%-16s%-64s
 * */
public class ConfirmacionMinorista16RecordBean extends RecordBean implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 5737932783767078904L;

  private String nuorden;

  // TAM 16
  private String nbooking1;
  // TAM 8
  private Date feejecuc;
  // TAM 6
  private Date hoejecuc;
  // tam 19
  private String fillerPostHoejecuc;
  // TAM 3
  private String cdbroker;
  // TAM 20
  private String cdrefban1;
  // TAM 1
  private Character cdtpoper;
  // TAM 12
  private String cdisin;
  // TAM 1
  private String evalue = "E";
  // TAM 10
  private BigDecimal imcbmerc;
  // TAM 4
  private String cdentliq;
  // TAM 8
  private Date fevalor;
  // TAM 11
  private BigDecimal nutiteje;
  // TAM 20
  private BigDecimal fillerPostNutiteje = new BigDecimal("00000000000000008206");
  // TAM 10
  private String nurefere;
  // TAM 7
  private String fillerPostNurefere;
  // TAM 16
  private String nuejecuc;
  // TAM 4
  private String fillerPostNuejecuc;
  // TAM 4
  private String executionTradingVenue;
  // TAM 4
  private BigDecimal fillerPostExecutionTradingVenue = BigDecimal.ZERO;
  // TAM 20
  private String cdrefban2;
  // TAM 6
  private String fillerPostCdrefban2;
  // TAM 16
  private String nbooking2;
  // TAM 64
  private String filler;

  public ConfirmacionMinorista16RecordBean() {
    super();
    setTipoRegistro("16");
  }

  public ConfirmacionMinorista16RecordBean(String nuorden,
                                           String nbooking1,
                                           Date feejecuc,
                                           Date hoejecuc,
                                           String fillerPostHoejecuc,
                                           String cdbroker,
                                           String cdrefban1,
                                           Character cdtpoper,
                                           String cdisin,
                                           String evalue,
                                           BigDecimal imcbmerc,
                                           String cdentliq,
                                           Date fevalor,
                                           BigDecimal nutiteje,
                                           BigDecimal fillerPostNutiteje,
                                           String nurefere,
                                           String fillerPostNurefere,
                                           String nuejecuc,
                                           String fillerPostNuejecuc,
                                           String executionTradingVenue,
                                           BigDecimal fillerPostExecutionTradingVenue,
                                           String cdrefban2,
                                           String fillerPostCdrefban2,
                                           String nbooking2,
                                           String filler) {
    this();
    this.nuorden = nuorden;
    this.nbooking1 = nbooking1;
    this.feejecuc = feejecuc;
    this.hoejecuc = hoejecuc;
    this.fillerPostHoejecuc = fillerPostHoejecuc;
    this.cdbroker = cdbroker;
    this.cdrefban1 = cdrefban1;
    this.cdtpoper = cdtpoper;
    this.cdisin = cdisin;
    this.evalue = evalue;
    this.imcbmerc = imcbmerc;
    this.cdentliq = cdentliq;
    this.fevalor = fevalor;
    this.nutiteje = nutiteje;
    this.fillerPostNutiteje = fillerPostNutiteje;
    this.nurefere = nurefere;
    this.fillerPostNurefere = fillerPostNurefere;
    this.nuejecuc = nuejecuc;
    this.fillerPostNuejecuc = fillerPostNuejecuc;
    this.executionTradingVenue = executionTradingVenue;
    this.fillerPostExecutionTradingVenue = fillerPostExecutionTradingVenue;
    this.cdrefban2 = cdrefban2;
    this.fillerPostCdrefban2 = fillerPostCdrefban2;
    this.nbooking2 = nbooking2;
    this.filler = filler;
  }

  public String getNuorden() {
    return nuorden;
  }

  public void setNuorden(String nuorden) {
    this.nuorden = nuorden;
  }

  public String getNbooking1() {
    return nbooking1;
  }

  public Date getFeejecuc() {
    return feejecuc;
  }

  public Date getHoejecuc() {
    return hoejecuc;
  }

  public String getFillerPostHoejecuc() {
    return fillerPostHoejecuc;
  }

  public String getCdbroker() {
    return cdbroker;
  }

  public String getCdrefban1() {
    return cdrefban1;
  }

  public Character getCdtpoper() {
    return cdtpoper;
  }

  public String getCdisin() {
    return cdisin;
  }

  public String getEvalue() {
    return evalue;
  }

  public BigDecimal getImcbmerc() {
    return imcbmerc;
  }

  public Date getFevalor() {
    return fevalor;
  }

  public BigDecimal getNutiteje() {
    return nutiteje;
  }

  public BigDecimal getFillerPostNutiteje() {
    return fillerPostNutiteje;
  }

  public String getNurefere() {
    return nurefere;
  }

  public String getFillerPostNurefere() {
    return fillerPostNurefere;
  }

  public String getNuejecuc() {
    return nuejecuc;
  }

  public String getFillerPostNuejecuc() {
    return fillerPostNuejecuc;
  }

  public String getExecutionTradingVenue() {
    return executionTradingVenue;
  }

  public BigDecimal getFillerPostExecutionTradingVenue() {
    return fillerPostExecutionTradingVenue;
  }

  public String getCdrefban2() {
    return cdrefban2;
  }

  public String getFillerPostCdrefban2() {
    return fillerPostCdrefban2;
  }

  public String getNbooking2() {
    return nbooking2;
  }

  public String getFiller() {
    return filler;
  }

  public void setNbooking1(String nuorden1) {
    this.nbooking1 = nuorden1;
  }

  public void setFeejecuc(Date feejecuc) {
    this.feejecuc = feejecuc;
  }

  public void setHoejecuc(Date hoejecuc) {
    this.hoejecuc = hoejecuc;
  }

  public void setFillerPostHoejecuc(String fillerPostHoejecuc) {
    this.fillerPostHoejecuc = fillerPostHoejecuc;
  }

  public void setCdbroker(String cdbroker) {
    this.cdbroker = cdbroker;
  }

  public void setCdrefban1(String cdrefban1) {
    this.cdrefban1 = cdrefban1;
  }

  public void setCdtpoper(Character cdtpoper) {
    this.cdtpoper = cdtpoper;
  }

  public void setCdisin(String cdisin) {
    this.cdisin = cdisin;
  }

  public void setEvalue(String evalue) {
    this.evalue = evalue;
  }

  public void setImcbmerc(BigDecimal imcbmerc) {
    this.imcbmerc = imcbmerc;
  }

  public void setFevalor(Date fevalor) {
    this.fevalor = fevalor;
  }

  public void setNutiteje(BigDecimal nutiteje) {

    this.nutiteje = nutiteje;
  }

  public void setNurefere(String nurefere) {
    this.nurefere = nurefere;
  }

  public void setNuejecuc(String nuejecuc) {
    this.nuejecuc = nuejecuc;
  }

  public void setExecutionTradingVenue(String executionTradingVenue) {
    this.executionTradingVenue = executionTradingVenue;
  }

  public void setCdrefban2(String cdrefban2) {
    this.cdrefban2 = cdrefban2;
  }

  public void setNbooking2(String nuorden2) {
    this.nbooking2 = nuorden2;
  }

  public String getCdentliq() {

    return cdentliq;
  }

  public void setCdentliq(String cdentliq) {
    this.cdentliq = cdentliq;
  }

  public void setFillerPostNutiteje(BigDecimal fillerPostNutiteje) {
    this.fillerPostNutiteje = fillerPostNutiteje;
  }

  public void setFillerPostNurefere(String fillerPostNurefere) {
    this.fillerPostNurefere = fillerPostNurefere;
  }

  public void setFillerPostNuejecuc(String fillerPostNuejecuc) {
    this.fillerPostNuejecuc = fillerPostNuejecuc;
  }

  public void setFillerPostExecutionTradingVenue(BigDecimal fillerPostExecutionTradingVenue) {
    this.fillerPostExecutionTradingVenue = fillerPostExecutionTradingVenue;
  }

  public void setFillerPostCdrefban2(String fillerPostCdrefban2) {
    this.fillerPostCdrefban2 = fillerPostCdrefban2;
  }

  public void setFiller(String filler) {

    this.filler = filler;
  }

  public String getNbooking1St() {
    String value = null;
    if (nbooking1 != null) {
      value = StringUtils.substring(nbooking1.trim(), 0, 16);
    }
    return value;
  }

  public String getFeejecucSt() {
    String value = null;
    if (feejecuc != null) {
      SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
      value = sf.format(feejecuc);
    }
    return value;
  }

  public String getHoejecucSt() {
    String value = null;
    if (hoejecuc != null) {
      SimpleDateFormat sf = new SimpleDateFormat("HHmmss");
      value = sf.format(hoejecuc);
    }
    return value;
  }

  public String getFillerPostHoejecucSt() {
    String value = null;
    if (fillerPostHoejecuc != null) {
      value = StringUtils.substring(fillerPostHoejecuc.trim(), 0, 4);
    }
    return value;
  }

  public String getCdbrokerSt() {
    String value = null;
    if (cdbroker != null) {
      value = StringUtils.substring(cdbroker.trim(), 0, 3);
    }
    return value;
  }

  public String getCdrefban1St() {
    String value = null;
    if (cdrefban1 != null) {
      value = StringUtils.substring(cdrefban1.trim(), 0, 20);
    }
    return value;
  }

  public String getCdtpoperSt() {
    String value = null;
    if (cdtpoper != null) {
      value = StringUtils.substring(cdtpoper + "", 0, 1);
    }
    return value;
  }

  public String getCdisinSt() {
    String value = null;
    if (cdisin != null) {
      value = StringUtils.substring(cdisin.trim(), 0, 12);
    }
    return value;
  }

  public String getEvalueSt() {
    String value = null;
    if (evalue != null) {
      value = StringUtils.substring(evalue.trim(), 0, 1);
    }
    return value;
  }

  public String getImcbmercSt() {
    String value = null;
    if (imcbmerc != null) {
      value = StringHelper.formatBigDecimal(imcbmerc, 6, 4);
    }
    return value;
  }

  public String getFevalorSt() {
    String value = null;
    if (fevalor != null) {
      SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
      value = sf.format(fevalor);
    }
    return value;
  }

  public String getNutitejeSt() {
    String value = null;
    if (nutiteje != null) {
      value = StringHelper.formatBigDecimal(nutiteje, 11, 0);
    }
    return value;
  }

  public String getNurefereSt() {
    String value = null;
    if (nurefere != null) {
      value = StringUtils.leftPad(StringUtils.substring(nurefere.trim(), 0, 9), 9, '0');
    }
    return value;
  }

  public String getNuejecucSt() {
    String value = null;
    if (nuejecuc != null) {
      value = StringUtils.substring(nuejecuc.trim(), 0, 16);
    }
    return value;
  }

  public String getExecutionTradingVenueSt() {
    String value = null;
    if (executionTradingVenue != null) {
      value = StringUtils.substring(executionTradingVenue.trim(), 0, 4);
    }
    return value;
  }

  public String getCdrefban2St() {
    String value = null;
    if (cdrefban2 != null) {
      value = StringUtils.substring(cdrefban2.trim(), 0, 20);
    }
    return value;
  }

  public String getNbooking2St() {
    String value = null;
    if (nbooking2 != null) {
      value = StringUtils.substring(nbooking2.trim(), 0, 16);
    }
    return value;
  }

  public String getCdentliqSt() {
    String value = null;
    if (cdentliq != null) {
      value = StringUtils.substring(cdentliq.trim(), 0, 4);
    }
    return value;
  }

  public String getFillerPostNutitejeSt() {
    String value = null;
    if (fillerPostNutiteje != null) {
      value = StringHelper.formatBigDecimal(fillerPostNutiteje, 20, 0);
    }
    return value;
  }

  public String getFillerPostNurefereSt() {
    String value = null;
    if (fillerPostNurefere != null) {
      value = StringUtils.substring(fillerPostNurefere.trim(), 0, 7);
    }
    return value;
  }

  public String getFillerPostNuejecucSt() {
    String value = null;
    if (fillerPostNuejecuc != null) {
      value = StringUtils.substring(fillerPostNuejecuc.trim(), 0, 10);
    }
    return value;
  }

  public String getFillerPostExecutionTradingVenueSt() {
    String value = null;
    if (fillerPostExecutionTradingVenue != null) {
      value = StringHelper.formatBigDecimal(fillerPostExecutionTradingVenue, 4, 0);
    }
    return value;
  }

  public String getFillerPostCdrefban2St() {
    String value = null;
    if (fillerPostCdrefban2 != null) {
      value = StringUtils.substring(fillerPostCdrefban2.trim(), 0, 4);
    }
    return value;
  }

  public String getFillerSt() {
    String value = null;
    if (filler != null) {
      value = StringUtils.substring(filler.trim(), 0, 64);
    }
    return value;
  }

  @Override
  public String toString() {
    return "ConfirmacionMinorista16RecordBean [nuorden=" + nuorden + ", nbooking1=" + nbooking1 + ", feejecuc="
           + feejecuc + ", hoejecuc=" + hoejecuc + ", fillerPostHoejecuc=" + fillerPostHoejecuc + ", cdbroker="
           + cdbroker + ", cdrefban1=" + cdrefban1 + ", cdtpoper=" + cdtpoper + ", cdisin=" + cdisin + ", evalue="
           + evalue + ", imcbmerc=" + imcbmerc + ", cdentliq=" + cdentliq + ", fevalor=" + fevalor + ", nutiteje="
           + nutiteje + ", fillerPostNutiteje=" + fillerPostNutiteje + ", nurefere=" + nurefere
           + ", fillerPostNurefere=" + fillerPostNurefere + ", nuejecuc=" + nuejecuc + ", fillerPostNuejecuc="
           + fillerPostNuejecuc + ", executionTradingVenue=" + executionTradingVenue
           + ", fillerPostExecutionTradingVenue=" + fillerPostExecutionTradingVenue + ", cdrefban2=" + cdrefban2
           + ", fillerPostCdrefban2=" + fillerPostCdrefban2 + ", nbooking2=" + nbooking2 + ", filler=" + filler + "]";
  }

  
  
}
