package sibbac.business.daemons.tasks;

import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.daemons.fichero.cuco.bo.GenerateCUCOFile;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_DAEMONS.JOB_ENVIO_FICHEROS_CUCO, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 21 ? * MON-FRI")
public class TaskGeneracionCUCO extends WrapperTaskConcurrencyPrevent {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOGGGER = LoggerFactory.getLogger(TaskGeneracionCUCO.class);

  /**
   * Tamaño de la página para
   */
  @Value("${daemons.envio.fichero.cuco.db.page.size:1000}")
  private int dbPageSize;

  /**
   * Ruta donde se deposita el fichero
   */
  @Value("${daemons.envio.fichero.cuco.out.path:/sbrmv/ficheros/cuco/out}")
  private String outPutDir;

  /**
   * Nombre del fichero generado por el proceso
   */
  @Value("${daemons.envio.fichero.cuco.file:FGLB0CUCO.txt}")
  private String fileName;

  /**
   * Generador de Fichero CUCO
   */
  @Autowired
  private GenerateCUCOFile cucoFileGenerator;

  /*
   * Ejecuta la tarea CUCO
   */
  @Override
  public void executeTask() throws SIBBACBusinessException {
    try {
      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.debug("Se inicia la tarea");
      }

      /**
       * Seteamos el tamaño de las páginas
       */
      cucoFileGenerator.setDbPageSize(dbPageSize);

      /**
       * Reiniciamos los contadores del fichero
       */
      cucoFileGenerator.resetCounters();
      
      /**
       * Seteamos la fecha actual por si se alarga la tarea y se sigue ejecutando en el día siguiente
       */
      //final Date currentDate = FormatDataUtils.convertStringToDate("2018-02-12", FormatDataUtils.DATE_FORMAT_1);
      final Date currentDate = new Date();
    
      cucoFileGenerator.setCurrentDate(currentDate);
      
      /**
       * Seteamos la ruta y el fichero de salida
       */
      cucoFileGenerator.generateFile(outPutDir, fileName);

      if (LOGGGER.isDebugEnabled()) {
        LOGGGER.info("Se termina la tarea");
      }
    }
    catch (SIBBACBusinessException e) {
      throw new SIBBACBusinessException(e);
    }
  }

  /*
   * Determinamos el tipo de tarea que vamos a ejecutar (CUCO)
   */
  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_CUCO;
  }
}
