package sibbac.business.daemons.batch.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

@Embeddable
public class ParamId implements Serializable, Comparable<ParamId> {

  private static final long serialVersionUID = -7462576168943475771L;
  
  private static final Logger LOG = getLogger(ParamId.class);
  
  @Column(name = "PARAM")
  private int param;
  
  @Column(name = "CODE", length = 5)
  private String code;
  
  @Column(name = "ORDER")
  private int order;

  public String getCode() {
    return code;
  }

  public int getOrder() {
    return order;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public void setParam(int param) {
    this.param = param;
  }
  
  public int getParam() {
    return param;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((code == null) ? 0 : code.hashCode());
    result = prime * result + order;
    result = prime * result + param;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ParamId other = (ParamId) obj;
    if (code == null) {
      if (other.code != null)
        return false;
    }
    else if (!code.equals(other.code))
      return false;
    if (order != other.order)
      return false;
    if (param != other.param)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.format("Code: %s; order: %d; param: %d", code, order, param);
  }

	@Override
	public int compareTo(ParamId other) {
		if(equals(other)) {
			return 0;
		}
		if(code.equals(other.code)) {
			if(order == other.order) {
				//Esto debería ser lo normal, que solo se ordenase entre parametros del mismo query
				return Integer.compare(param, other.param);
			}
		}
		LOG.warn("[compareTo] Se están intentando comparar dos params de distintos queries: {} y {}",
				toString(), other.toString());
		return code.compareTo(other.code) + Integer.compare(order, other.order) +
				Integer.compare(param, other.param);
	}
  
}
