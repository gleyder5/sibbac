package sibbac.business.daemons.batch.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sibbac.business.daemons.batch.model.Batch;

@Repository
public interface BatchDao extends JpaRepository<Batch, String> {
  
  /**
   * Recupera todos los procesos Batch que tienen asociada una regla de periocidad y que están activos
   * @return lista de batch, puede ser una lista vacía
   */
	@Query("SELECT b FROM Batch b JOIN b.periodicidad p WHERE b.active = true and b.starts <= CURRENT_DATE "
			+ "and p.activo = true and (p.fechaDesde is null or p.fechaDesde <= CURRENT_DATE) "
			+ "and (p.fechaHasta is null or p.fechaHasta >= CURRENT_DATE) ")
  List<Batch> findAllPeriodicActiveBatchs();
	
	/**
	 * Recupera un proceso Batch con llave primaria code siempre y cuando el batch esté activo
	 * @param code Llave primaria del Batch
	 * @return Un batch activo con esa llave primaria o nulo
	 */
	@Query("SELECT b FROM Batch b WHERE b.code = :code and b.active = true and b.starts < CURRENT_DATE")
	Batch findActiveByCode(@Param("code") String code);

}
