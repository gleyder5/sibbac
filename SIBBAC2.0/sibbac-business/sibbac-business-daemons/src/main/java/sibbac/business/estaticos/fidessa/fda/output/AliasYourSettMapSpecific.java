package sibbac.business.estaticos.fidessa.fda.output;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.xml.YourSettMapSpecificMegara;
import sibbac.business.estaticos.utils.conversiones.ConvertirAs400Fidessa;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.database.megbpdta.model.Fmeg0aec;

@Service
@Scope(value = "prototype")
public class AliasYourSettMapSpecific extends YourSettMapSpecificMegara {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AliasYourSettMapSpecific.class);

  private Fmeg0aec objFmeg0aec;

  private String tipoModificacion;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private ConvertirAs400Fidessa convertirAs400Fidessa;

  public AliasYourSettMapSpecific(Fmeg0aec objFmeg0aec, String tipoModificacion) {

    super();

    this.objFmeg0aec = objFmeg0aec;

    this.tipoModificacion = tipoModificacion;

  }

  public boolean procesarXML() {

    boolean result = true;

    // obtener los datos necesarios para rellenar los campos del archivo XML
    obtenerDatos();

    /* AML 06-10-2010 */
    /*
     * Si market es MDR no se genera fichero
     */

    final String market = getEligible_cust_mkt();

    if (!"MDR".equals(market)) {

      // generar el archivo XML
      generarXML();

      RegistroControl objRegistroControl = generarRegistroControl();

      objRegistroControl.write();
    }

    return result;

  }

  private void obtenerDatos() {

    logger.debug("Inicio obtencion de datos");
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      // ----------------------------------------------------------------------------

      setTipoActualizacion(tipoModificacion);

      // ----------------------------------------------------------------------------

      setClient_mnemonic(objFmeg0aec.getId().getCdaliass().trim());

      // ----------------------------------------------------------------------------

      setEligible_cust_entity(objFmeg0aec.getCdcustod().trim());

      // ----------------------------------------------------------------------------

      setEligible_cust_mkt(objFmeg0aec.getCdmarket().trim());

      // ----------------------------------------------------------------------------

      setEligible_cust_sett_type(convertirAs400Fidessa.execConvertir("TPSETTLE", objFmeg0aec.getTpsettle().trim()));

      // ----------------------------------------------------------------------------

      tx.commit();

    }
    catch (Exception e) {
      setMensajeError(e.getMessage());
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private RegistroControl generarRegistroControl() {

    RegistroControl result = null;

    if ("".equals(getMensajeError().trim())) {
      result = ctx.getBean(RegistroControl.class, "YOUR SETT MAP SPECIFIC", getClient_mnemonic(), getNombreCortoXML(),
          "I", "", "Alias_SpecificYourMap", "FI");
    }
    else {
      logger.info("Archivo " + getNombreCortoXML() + " generado con error: " + getMensajeError());
      result = ctx.getBean(RegistroControl.class, "YOUR SETT MAP SPECIFIC", getClient_mnemonic(), getNombreCortoXML(),
          "E", getMensajeError(), "Alias_SpecificYourMap", "FI");
    }
    return result;

  }
}