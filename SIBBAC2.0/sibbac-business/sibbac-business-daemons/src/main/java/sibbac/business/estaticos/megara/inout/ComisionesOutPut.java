package sibbac.business.estaticos.megara.inout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.ComisionesChargeRules;
import sibbac.business.estaticos.fidessa.fda.output.ComisionesChargeScale;
import sibbac.business.estaticos.fidessa.fda.output.ComisionesChargeScaleBand;
import sibbac.database.megbpdta.bo.Fmeg0ctrBo;
import sibbac.database.megbpdta.model.Fmeg0com;
import sibbac.database.megbpdta.model.Fmeg0ctr;

@Service
@Scope(value = "prototype")
public class ComisionesOutPut {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ComisionesOutPut.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0ctrBo fmeg0ctrBo;

  private Fmeg0com obj1Fmeg0com, obj2Fmeg0com;

  String descripcion1;

  String descripcion2;

  public ComisionesOutPut(Fmeg0com obj1Fmeg0com, Fmeg0com obj2Fmeg0com) {

    super();

    this.obj1Fmeg0com = obj1Fmeg0com;
    this.obj2Fmeg0com = obj2Fmeg0com;

  }

  public boolean generarSalida() {

    boolean result = true;

    descripcion1 = ctx.getBean(ComisionesChargeRules.class, obj1Fmeg0com).execXML();

    if (null != obj2Fmeg0com) {
      descripcion2 = ctx.getBean(ComisionesChargeRules.class, obj2Fmeg0com).execXML();
    }

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    List<Fmeg0ctr> listCtr = null;

    try {

      tx.begin();

      listCtr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(obj1Fmeg0com.getId().getCdtpcomi().trim(),
          obj1Fmeg0com.getId().getCdcomisi().trim(), obj1Fmeg0com.getId().getCdtarifa().trim());

      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {
      if (tx.isActive()) {

        tx.rollback();
      }
      em.close();
    }

    if (0 < listCtr.size()) {
      ctx.getBean(ComisionesChargeScale.class, (Fmeg0ctr) listCtr.get(0), descripcion1).execXML();

    }

    for (int i = 0; i < listCtr.size(); i++) {
      ctx.getBean(ComisionesChargeScaleBand.class, (Fmeg0ctr) listCtr.get(i), descripcion1, i + 1).execXML();

    }

    em = emf.createEntityManager();
    tx = em.getTransaction();
    listCtr = null;

    if (null != obj2Fmeg0com) {

      try {

        tx.begin();

        listCtr = fmeg0ctrBo.findAllByIdCdtpcomiAndIdCdcomisiAndIdCdtarifa(obj2Fmeg0com.getId().getCdtpcomi().trim(),
            obj2Fmeg0com.getId().getCdcomisi().trim(), obj2Fmeg0com.getId().getCdtarifa().trim());

        tx.commit();

      }
      catch (Exception e) {
        logger.warn(e.getMessage(), e);
      }
      finally {
        if (tx.isActive()) {
          tx.rollback();
        }
        em.close();
      }

      if (0 < listCtr.size()) {
        ctx.getBean(ComisionesChargeScale.class, (Fmeg0ctr) listCtr.get(0), descripcion2).execXML();
      }

      for (int i = 0; i < listCtr.size(); i++) {
        ctx.getBean(ComisionesChargeScaleBand.class, (Fmeg0ctr) listCtr.get(i), descripcion2, i).execXML();
      }
    }

    return result;

  }

}