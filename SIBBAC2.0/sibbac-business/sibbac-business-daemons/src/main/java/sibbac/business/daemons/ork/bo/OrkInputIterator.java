package sibbac.business.daemons.ork.bo;

import java.util.concurrent.BlockingQueue;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.ork.common.ResultSetIterator;

class OrkInputIterator implements ResultSetIterator {
	
	private static final Logger LOG = getLogger(OrkInputIterator.class);
	
	private final BlockingQueue<OrkPetition> petitionQueue;
	
	private final OrkFidessaFieldCollection fieldCollection;
	
	OrkInputIterator(BlockingQueue<OrkPetition> petitionQueue, OrkFidessaFieldCollection fieldCollection) {
		this.petitionQueue = petitionQueue;
		this.fieldCollection = fieldCollection;
	}

	@Override
	public void iterate(ResultSet resultSet) throws SQLException {
		OrkPetition petition;
		boolean hasnext;
		int count = 0;
		
	  hasnext = resultSet.next();
		try {
			while(hasnext) {
				petition = new OrkPetition(fieldCollection);
				hasnext = petition.load(resultSet);
				petitionQueue.put(petition);
				count++;
			}
			petitionQueue.put(OrkPetition.endPetition());
		}
		catch(InterruptedException iex) {
			LOG.error("Se ha interrumpido la ejecución del hilo de lectura desde la base de datos", iex);
		}
		LOG.info("[iterate] Peticiones cargadas: {}", count);
	}
	
}
