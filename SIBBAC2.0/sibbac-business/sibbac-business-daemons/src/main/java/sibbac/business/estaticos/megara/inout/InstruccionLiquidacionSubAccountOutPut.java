package sibbac.business.estaticos.megara.inout;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import sibbac.business.estaticos.fidessa.fda.output.SubAccountOurSettMapGeneral;
import sibbac.business.estaticos.fidessa.fda.output.SubAccountOurSettMapSpecific;
import sibbac.business.estaticos.fidessa.fda.output.SubAccountYourSettMapGeneral;
import sibbac.business.estaticos.fidessa.fda.output.SubAccountYourSettMapSpecific;
import sibbac.database.megbpdta.bo.Fmeg0secBo;
import sibbac.database.megbpdta.bo.Fmeg0selBo;
import sibbac.database.megbpdta.model.Fmeg0sec;
import sibbac.database.megbpdta.model.Fmeg0sel;
import sibbac.database.megbpdta.model.Fmeg0sua;

/**
 * InstruccionLiquidacionSubAccount Genera los archivos xml correspondientes a
 * instrucciones de liquidacion generados por la entrada de un archivo
 * SubAccount
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 * 
 */
@Service
@Scope(value = "prototype")
public class InstruccionLiquidacionSubAccountOutPut {

  final static org.slf4j.Logger logger = org.slf4j.LoggerFactory
      .getLogger(InstruccionLiquidacionSubAccountOutPut.class);

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  @Autowired
  private Fmeg0selBo fmeg0selBo;

  @Autowired
  private Fmeg0secBo fmeg0secBo;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  private Fmeg0sua objFmeg0sua = null;

  private String tipoModificacion = "";

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Constructor de clase
   */
  public InstruccionLiquidacionSubAccountOutPut(Fmeg0sua objFmeg0sua, String tipoModificacion) {

    super();

    this.objFmeg0sua = objFmeg0sua;
    this.tipoModificacion = tipoModificacion;

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * procesarSalidaSubAccount
   * 
   * Comprueba si se la subcuenta se encuentra de baja en caso de no ser asi
   * genera los xml correspondientes a las instrucciones de liquidacion
   * 
   */
  public void procesarSalidaSubAccount() {

    if (!esBaja(tipoModificacion)) {

      generarInstruccionesLiquidacion();

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionesLiquidacion
   * 
   * genera dos listas que se corresponden con los registros de los ficheros
   * Fmeg0sel y Fmeg0sec dependientes de la subcuenta que hemos tratado, por
   * cada uno de los registros recuperados intentamos generar la instruccion de
   * liquidacion correspondiente
   * 
   */
  private void generarInstruccionesLiquidacion() {

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    List<Fmeg0sel> list_sel = null;
    List<Fmeg0sec> list_sec = null;

    try {

      String subcuenta = objFmeg0sua.getId().getCdsubcta();

      tx.begin();

      list_sel = fmeg0selBo.findAllByIdCdsubcta(subcuenta);

      list_sec = fmeg0secBo.findAllByIdCdsubcta(subcuenta);

      tx.commit();

    }
    catch (Exception e) {
      logger.warn(e.getMessage());
    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

    for (int i = 0; i < list_sel.size(); i++) {

      generarInstruccionLiquidacion((Fmeg0sel) list_sel.get(i));

    }

    for (int i = 0; i < list_sec.size(); i++) {

      generarInstruccionLiquidacion((Fmeg0sec) list_sec.get(i));

    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionLiquidacion
   * 
   * recibiendo un registro del fichero Fmeg0aec comprueba si es mercado
   * extranjero, nacional o no viene identificado, dependiendo de lo cual,
   * generara, o genera la instruccion de sociedad correspondiente.
   * 
   */
  private void generarInstruccionLiquidacion(Fmeg0sel objFmeg0sel) {

    if ("".equals(objFmeg0sel.getCdmarket().trim())) {
      generarInstruccionSociedadGeneralSubAccount(objFmeg0sel);
    }
    else {
      generarInstruccionSociedadSpecificSubAccount(objFmeg0sel);
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionLiquidacion
   * 
   * recibiendo un registro del fichero Fmeg0aec comprueba si es mercado
   * extranjero, nacional o no viene identificado, dependiendo de lo cual,
   * generara, o genera la instruccion de sociedad correspondiente.
   * 
   */
  private void generarInstruccionLiquidacion(Fmeg0sec objFmeg0sec) {

    if ("".equals(objFmeg0sec.getCdmarket().trim())) {
      generarInstruccionCustodioGeneralSubAccount(objFmeg0sec);
    }
    else {
      generarInstruccionCustodioSpecificSubAccount(objFmeg0sec);
    }

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionSociedadGeneralSubAccount
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Our
   * Sett Map General y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros Our Sett Map General se van a generar existan o no existan en
   * el fichero FMEG0CHA.
   */

  private void generarInstruccionSociedadGeneralSubAccount(Fmeg0sel objFmeg0sel) {

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    ctx.getBean(SubAccountOurSettMapGeneral.class, objFmeg0sel, tipoModificacion,
        objFmeg0sua.getId().getCdaliass().trim()).procesarXML();

    try {
      tx.begin();
      objFmeg0sel.setIdenvfid("S");
      fmeg0selBo.save(objFmeg0sel, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

    /* } */

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionSociedadSpecificSubAccount
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Our
   * Sett Map General y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros Our Sett Map Specific se van a generar existan o no existan en
   * el fichero FMEG0CHA.
   */

  private void generarInstruccionSociedadSpecificSubAccount(Fmeg0sel objFmeg0sel) {

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    ctx.getBean(SubAccountOurSettMapSpecific.class, objFmeg0sel, tipoModificacion,
        objFmeg0sua.getId().getCdaliass().trim()).procesarXML();

    try {
      tx.begin();
      objFmeg0sel.setIdenvfid("S");
      fmeg0selBo.save(objFmeg0sel, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

    /* } */

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionCustodioGeneralSubAccount
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Our
   * Sett Map General y un Client Delivery Instruction
   * 
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros your Sett Map General se van a generar existan o no existan en
   * el fichero FMEG0CHA.
   */

  private void generarInstruccionCustodioGeneralSubAccount(Fmeg0sec objFmeg0sec) {

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    ctx.getBean(SubAccountYourSettMapGeneral.class, objFmeg0sec, tipoModificacion,
        objFmeg0sua.getId().getCdaliass().trim()).procesarXML();

    try {
      tx.begin();
      objFmeg0sec.setIdenvfid("S");
      fmeg0secBo.save(objFmeg0sec, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }

      em.close();

    }

    /* } */

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * generarInstruccionCustodioSpecificSubAccount
   * 
   * busca un registro en el fichero Fmeg0cha que cumpla con las premisas
   * indicadas en el ddr en caso de encontrarlo genera dos ficheros XML un Our
   * Sett Map General y un Client Delivery Instruction
   */

  /**
   * 19/08/2009 MIGUEL A. CORCOBADO
   * 
   * Los ficheros Your Sett Map Specific se van a generar existan o no existan
   * en el fichero FMEG0CHA.
   */

  private void generarInstruccionCustodioSpecificSubAccount(Fmeg0sec objFmeg0sec) {

    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    ctx.getBean(SubAccountYourSettMapSpecific.class, objFmeg0sec, tipoModificacion,
        objFmeg0sua.getId().getCdaliass().trim()).procesarXML();

    try {
      tx.begin();
      objFmeg0sec.setIdenvfid("S");
      fmeg0secBo.save(objFmeg0sec, false);
      tx.commit();
    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
    }
    finally {

      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();

    }

    /* } */

  }

  // -----------------------------------------------------------------------------
  // -----------------------------------------------------------------------------

  /**
   * Metodo para la validacion de mercado extranjero
   * 
   * Devuelve un valor un valor booleano indicando si el estado del alias es
   * baja o no
   * 
   * @param mercado
   * @return boolean
   */
  private boolean esBaja(String estado) {
    boolean result = false;

    if ("D".equals(estado)) {

      result = true;
    }

    return result;
  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}