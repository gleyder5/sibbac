package sibbac.business.daemons.ork.bo;

import java.io.File;
import java.util.Map;

import sibbac.common.SIBBACBusinessException;

public interface OrkProcessor {

  void setCache(Map<String, Object[]> cache);

  void setOrkFidessaFieldCollection(OrkFidessaFieldCollection fieldCollection);

  OrkInputIterator getInputIterator() throws SIBBACBusinessException;
  
  File getOrkFile() throws InterruptedException, SIBBACBusinessException;

}
