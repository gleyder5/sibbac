package sibbac.business.estaticos.utils.xml;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Esta clase Lee un documento XML de disco.
 * 
 * @author Jos� Juan Escudero
 * @author Trentisa I+D
 * @version 1.0
 * @viz.diagram ReadDocument.tpx
 */
public class ReadDocument extends BuilderDocument {
  
  
  /**
   * Constructor for ReadDocument.
   */
  public ReadDocument(String file) throws NewXmlException, ReadXmlException {
    super();
    readDoc(file);
  }

  /**
   * Constructor for ReadDocument.
   */
  public ReadDocument() throws NewXmlException {
    super();
  }

  public Document readDoc(String file) throws ReadXmlException {
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setValidating(false);
      DocumentBuilder db = dbf.newDocumentBuilder();
      try (InputStream is = new FileInputStream(file);
          Reader reader = new InputStreamReader(is, StandardCharsets.ISO_8859_1);) {
        InputSource source = new InputSource(reader);
        source.setEncoding(StandardCharsets.ISO_8859_1.name());

        setDoc(db.parse(source));
      }

      DocumentType dt = getDoc().getDoctype();
      if (dt != null) {
        setPublicId(dt.getPublicId());
        setSystemId(dt.getSystemId());
        setNamespaceURI(dt.getNamespaceURI());
        setQualifiedName(dt.getName());
      }
    }
    catch (FactoryConfigurationError e) {
      throw new ReadXmlException("Error lectura  Fichero XML \n" + file + " de tipo: \n" + e + ":\n" + e.getMessage());
    }
    catch (ParserConfigurationException e) {
      throw new ReadXmlException("Error lectura  Fichero XML \n" + file + " de tipo: \n" + e + ":\n" + e.getMessage());
    }
    catch (SAXException e) {
      throw new ReadXmlException("Error lectura  Fichero XML \n" + file + " de tipo: \n" + e + ":\n" + e.getMessage());
    }
    catch (IOException e) {
      throw new ReadXmlException("Error lectura  Fichero XML \n" + file + " de tipo: \n" + e + ":\n" + e.getMessage());
    }
    return getDoc();
  }
}
