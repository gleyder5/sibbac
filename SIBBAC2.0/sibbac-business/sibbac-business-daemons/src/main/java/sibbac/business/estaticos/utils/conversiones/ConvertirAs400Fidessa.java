package sibbac.business.estaticos.utils.conversiones;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.database.megbpdta.dao.Fmeg0cfiDao;
import sibbac.database.megbpdta.model.Fmeg0cfi;
import sibbac.database.megbpdta.model.Fmeg0cfiId;

/**
 * ConvertirAs400Fidessa.
 * 
 * @version 1.0
 * @author trenti
 * @since 28 de octubre de 2008
 */
@Service
public class ConvertirAs400Fidessa {

  /** The Constant logger. */
  final static org.slf4j.Logger logger = LoggerFactory.getLogger(ConvertirAs400Fidessa.class);

  @Autowired
  private Fmeg0cfiDao fmeg0cfiDao;

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Instantiates a new convertir as400 fidessa.
   */
  public ConvertirAs400Fidessa() {

  }

  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

  /**
   * Exec convertir.
   * 
   * Realiza la conversion de datos entre el AS400 y Fidessa
   * 
   * @param idcamas4 the idcamas4
   * @param dsaas400 the dsaas400
   * 
   * @return the string
   * 
   * @throws Exception the exception
   */
  public String execConvertir(String idcamas4, String dsaas400) throws Exception {

    Fmeg0cfi objFmeg0cfi;
    String dsFidessa = "";

    objFmeg0cfi = fmeg0cfiDao.findOne(new Fmeg0cfiId(idcamas4, dsaas400));

    logger.debug("Inicio conversion de datos AS400 - Fidessa, idcamas4= " + idcamas4 + " dsaas400= " + dsaas400);

    try {

      dsFidessa = objFmeg0cfi.getDsfidessa().trim();

    }
    catch (Exception e) {

      logger.debug("Error conversion de datos AS400 - Fidessa, idcamas4= {} dsaas400= {}", idcamas4, dsaas400);
      throw new Exception(String.format("Error conversion de datos AS400 - Fidessa, idcamas4= %s dsaas400= %s",
          idcamas4, dsaas400));

    }

    logger.debug("Fin conversion datos de AS400 - Fidessa, dsFidessa= {}", dsFidessa);

    return dsFidessa;
  }
  // ------------------------------------------------------------------------------
  // ------------------------------------------------------------------------------

}