package sibbac.business.estaticos.megara.input;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sibbac.business.estaticos.megara.inout.AliasOutPut;
import sibbac.business.estaticos.utils.EstaticosMegaraUtilities;
import sibbac.business.estaticos.utils.conversiones.ConvertirBoolean;
import sibbac.business.estaticos.utils.conversiones.ConvertirMegaraAs400;
import sibbac.business.estaticos.utils.registrocontrol.RegistroControl;
import sibbac.business.estaticos.utils.sesion.DirectoriosEstaticos;
import sibbac.database.megbpdta.bo.Fmeg0aadBo;
import sibbac.database.megbpdta.bo.Fmeg0aecBo;
import sibbac.database.megbpdta.bo.Fmeg0aelBo;
import sibbac.database.megbpdta.bo.Fmeg0ahlBo;
import sibbac.database.megbpdta.bo.Fmeg0aidBo;
import sibbac.database.megbpdta.bo.Fmeg0aliBo;
import sibbac.database.megbpdta.bo.Fmeg0aotBo;
import sibbac.database.megbpdta.bo.Fmeg0apmBo;
import sibbac.database.megbpdta.bo.Fmeg0asdBo;
import sibbac.database.megbpdta.bo.Fmeg0astBo;
import sibbac.database.megbpdta.bo.Fmeg0brcBo;
import sibbac.database.megbpdta.bo.Fmeg0entBo;
import sibbac.database.megbpdta.model.Fmeg0aad;
import sibbac.database.megbpdta.model.Fmeg0aadId;
import sibbac.database.megbpdta.model.Fmeg0aec;
import sibbac.database.megbpdta.model.Fmeg0aecId;
import sibbac.database.megbpdta.model.Fmeg0ael;
import sibbac.database.megbpdta.model.Fmeg0aelId;
import sibbac.database.megbpdta.model.Fmeg0ahl;
import sibbac.database.megbpdta.model.Fmeg0ahlId;
import sibbac.database.megbpdta.model.Fmeg0aid;
import sibbac.database.megbpdta.model.Fmeg0aidId;
import sibbac.database.megbpdta.model.Fmeg0ali;
import sibbac.database.megbpdta.model.Fmeg0aot;
import sibbac.database.megbpdta.model.Fmeg0aotId;
import sibbac.database.megbpdta.model.Fmeg0apm;
import sibbac.database.megbpdta.model.Fmeg0apmId;
import sibbac.database.megbpdta.model.Fmeg0asd;
import sibbac.database.megbpdta.model.Fmeg0asdId;
import sibbac.database.megbpdta.model.Fmeg0ast;
import sibbac.database.megbpdta.model.Fmeg0astId;

@Service
@Scope(value = "prototype")
public class Alias {

  final static org.slf4j.Logger logger = LoggerFactory.getLogger(Alias.class);

  @Autowired
  private EstaticosMegaraUtilities utilities;

  @Autowired
  private Fmeg0aliBo fmeg0aliBo;

  @Autowired
  private Fmeg0entBo fmeg0entBo;

  @Autowired
  private Fmeg0brcBo fmeg0brcBo;

  @Autowired
  private Fmeg0astBo fmeg0astBo;

  @Autowired
  private Fmeg0apmBo fmeg0apmBo;

  @Autowired
  private Fmeg0ahlBo fmeg0ahlBo;

  @Autowired
  private Fmeg0aotBo fmeg0aotBo;

  @Autowired
  private Fmeg0aecBo femg0aecBo;

  @Autowired
  private Fmeg0aelBo fmeg0aelBo;

  @Autowired
  private Fmeg0asdBo fmeg0asdBo;

  @Autowired
  private Fmeg0aidBo fmeg0aidBo;

  @Autowired
  private Fmeg0aadBo fmeg0aadBo;

  @Autowired
  private ConvertirMegaraAs400 convertirMegaraAs400;

  @Autowired
  protected DirectoriosEstaticos directorios;

  @Autowired
  private ApplicationContext ctx;

  @Autowired
  private EntityManagerFactory emf;

  private final String INPUT = "I";

  private final String UPDATE = "U";

  private final String DELETE = "D";

  private final String RESEND = "R";

  private static List<String> atrData;

  private static List<String> atrStopStpException;

  private static List<String> atrPayMarketException;

  private static List<String> atrOtherId;

  private static List<String> atrOtherAddress;

  private static List<String> atrHolder;

  private static List<String> atrEligibleCustodian;

  private static List<String> atrEligibleClearer;

  private static List<String> atrSettlementDetail;

  private static List<String> atrIdBySE;

  private Document document;

  private Fmeg0ali objFmeg0ali;

  public Alias(Document dcmnt) {

    document = dcmnt;

    atrData = new ArrayList<>();
    atrData.add("broClient");
    atrData.add("alias");
    atrData.add("allocationType");
    atrData.add("expectedAllocation");
    atrData.add("marketAllocationRule");
    atrData.add("confirmationType");
    atrData.add("calculationType");
    atrData.add("calculationTypeLM");
    atrData.add("confirmToClearer");
    atrData.add("description");
    atrData.add("forOwnAccount");
    atrData.add("isDefault");
    atrData.add("payMarket");
    atrData.add("portfolio");
    atrData.add("reconcileAllocation");
    atrData.add("reconcileAllocationLM");
    atrData.add("reconcileConfirmation");
    atrData.add("reconcileConfirmationLM");
    atrData.add("requireAllocation");
    atrData.add("requireAllocationLM");
    atrData.add("requireAllocConfirmation");
    atrData.add("requireConfirmation");
    atrData.add("stp");
    atrData.add("exonerated");
    atrData.add("manualRetrocession");
    atrData.add("routing");
    atrData.add("requireRetrocession");
    atrData.add("trader");
    atrData.add("sale");
    atrData.add("manager");
    atrData.add("statisticGroup");
    atrData.add("statisticLevel0");
    atrData.add("statisticLevel1");
    atrData.add("statisticLevel2");
    atrData.add("statisticLevel3");
    atrData.add("statisticLevel4");
    atrData.add("traderLM");
    atrData.add("saleLM");
    atrData.add("managerLM");
    atrData.add("statisticGroupLM");
    atrData.add("statisticLevel0LM");
    atrData.add("statisticLevel1LM");
    atrData.add("statisticLevel2LM");
    atrData.add("statisticLevel3LM");
    atrData.add("statisticLevel4LM");

    atrStopStpException = new ArrayList<>();
    atrStopStpException.add("stockExchange");

    atrPayMarketException = new ArrayList<>();
    atrPayMarketException.add("stockExchange");

    atrOtherId = new ArrayList<>();
    atrOtherId.add("identifier");
    atrOtherId.add("idType");
    atrOtherId.add("description");
    atrOtherId.add("documentType");

    atrOtherAddress = new ArrayList<>();
    atrOtherAddress.add("adresseType");
    atrOtherAddress.add("contactFunction");
    atrOtherAddress.add("contactName");
    atrOtherAddress.add("purpose");
    atrOtherAddress.add("amailaddress");
    atrOtherAddress.add("zipCode");
    atrOtherAddress.add("location");
    atrOtherAddress.add("location2");
    atrOtherAddress.add("street");
    atrOtherAddress.add("streetNumber");
    atrOtherAddress.add("flat");
    atrOtherAddress.add("afaxnumber");
    atrOtherAddress.add("aemailaddress");
    atrOtherAddress.add("aswiftaddress");
    atrOtherAddress.add("aphonenumber");
    atrOtherAddress.add("aoasysaddress");
    atrOtherAddress.add("asequaladdress");
    atrOtherAddress.add("atelexnumber");
    atrOtherAddress.add("afileaddress");

    atrHolder = new ArrayList<>();
    atrHolder.add("externalReference");
    atrHolder.add("isMain");
    atrHolder.add("participationPercentage");
    atrHolder.add("holder");
    atrHolder.add("holderType");

    atrEligibleCustodian = new ArrayList<>();
    atrEligibleCustodian.add("settlementType");
    atrEligibleCustodian.add("mainEntityId");
    atrEligibleCustodian.add("market");
    atrEligibleCustodian.add("clearingMode");
    atrEligibleCustodian.add("custodian");

    atrEligibleClearer = new ArrayList<>();
    atrEligibleClearer.add("settlementType");
    atrEligibleClearer.add("mainEntityId");
    atrEligibleClearer.add("market");
    atrEligibleClearer.add("clearer");
    atrEligibleClearer.add("accountAtClearer");

    atrSettlementDetail = new ArrayList<>();
    atrSettlementDetail.add("clearingMode");
    atrSettlementDetail.add("custodian");
    atrSettlementDetail.add("sendCmToCH");
    atrSettlementDetail.add("cHManageCom");
    atrSettlementDetail.add("confirmSettlement");

    atrIdBySE = new ArrayList<>();
    atrIdBySE.add("identifier");
    atrIdBySE.add("stockExchange");
    atrIdBySE.add("description");

    objFmeg0ali = new Fmeg0ali();
    objFmeg0ali.setCdbrocli("");
    objFmeg0ali.setCdaliass("");
    objFmeg0ali.setTpalloca("");
    objFmeg0ali.setTpexallo("");
    objFmeg0ali.setTpmaallo("");
    objFmeg0ali.setTpconfir("");
    objFmeg0ali.setTpcalcuu("");
    objFmeg0ali.setTpcalclm("");
    objFmeg0ali.setIsconcle("");
    objFmeg0ali.setDescrali("");
    objFmeg0ali.setIsownacc("");
    objFmeg0ali.setIsdefaul("");
    objFmeg0ali.setIspaymar("");
    objFmeg0ali.setPortfoli("");
    objFmeg0ali.setIsrecall("");
    objFmeg0ali.setIsrecalm("");
    objFmeg0ali.setIsreccon("");
    objFmeg0ali.setIsrecclm("");
    objFmeg0ali.setIsreqalo("");
    objFmeg0ali.setIsreqalm("");
    objFmeg0ali.setIsreqalc("");
    objFmeg0ali.setIsstproc("");
    objFmeg0ali.setIsexoner("");
    objFmeg0ali.setIsmanret("");
    objFmeg0ali.setIsroutin("");
    objFmeg0ali.setIsreqret("");
    objFmeg0ali.setCdtrader("");
    objFmeg0ali.setCdsaleid("");
    objFmeg0ali.setCdmanage("");
    objFmeg0ali.setStgroup("");
    objFmeg0ali.setStlevel0("");
    objFmeg0ali.setStlevel1("");
    objFmeg0ali.setStlevel2("");
    objFmeg0ali.setStlevel3("");
    objFmeg0ali.setStlevel4("");
    objFmeg0ali.setCdtradlm("");
    objFmeg0ali.setCdsalelm("");
    objFmeg0ali.setCdmanalm("");
    objFmeg0ali.setStgroulm("");
    objFmeg0ali.setStlev0lm("");
    objFmeg0ali.setStlev1lm("");
    objFmeg0ali.setStlev2lm("");
    objFmeg0ali.setStlev3lm("");
    objFmeg0ali.setStlev4lm("");
    objFmeg0ali.setFhdebaja("");
    objFmeg0ali.setTpmodeid("");

  }

  public void procesarXML_In(String nombreXML) {

    try {
      final String modeIn = document.getElementsByTagName("mode").item(0).getFirstChild().getNodeValue();

      if (RESEND.equals(modeIn) || existenCondiciones()) {

        if (INPUT.equals(modeIn)) {
          input();
        }
        else if (RESEND.equals(modeIn)) {
          load();
        }
        else {
          delete();
        }

        // 27/11/2013
        // Se desactivan los envios para la

        final String alias = objFmeg0ali.getCdaliass();

        if (alias != null
            && !("22645".equals(alias.trim()) || "22671".equals(alias.trim()) || "22672".equals(alias.trim()))) {

          //utilities.createNuctapro(objFmeg0ali.getCdaliass(), objFmeg0ali.getCdbrocli(), objFmeg0ali.getTpmodeid());

          logger.debug("Archivo XML tratado correctamente");
          ctx.getBean(RegistroControl.class, "Alias", objFmeg0ali.getCdaliass(), nombreXML, "I",
              "Procesado fichero estatico", "Alias", "MG").write();
          directorios.moverDirOk(nombreXML);

          try {
            ctx.getBean(AliasOutPut.class, objFmeg0ali).execXML();
          }
          catch (Exception e) {
            logger.warn(e.getMessage(), e);
          }
        }
      }

    }
    catch (Exception e) {
      logger.warn(e.getMessage(), e);
      ctx.getBean(RegistroControl.class, "Alias", objFmeg0ali.getCdaliass(), nombreXML, "E", e.getMessage(), "Alias",
          "MG").write();
      directorios.moverDirError(nombreXML);

      // AML 2012/11/28 Se da de baja el alias si no se ha podido dar de alta la
      // cuenta en nacional
      final String exceptionMessage = e.getMessage();

      if (INPUT.equals(objFmeg0ali.getTpmodeid())
          && (exceptionMessage.indexOf("Existe ya una cuenta en nacional para la cuenta") != -1 || exceptionMessage
              .indexOf("Se ha encontrado que la cuenta de extranjero") != -1))
        try {
          delete();
        }
        catch (Exception e1) {
          logger.warn(e1.getMessage(), e1);
        }
    }

  }

  private void input() throws Exception {

    String objPrimaryKey;
    Fmeg0ali objFmeg0ali_pers = new Fmeg0ali();

    tratarCampos();

    objPrimaryKey = objFmeg0ali.getCdaliass();

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0ali_pers = fmeg0aliBo.findById(objPrimaryKey);

      if (objFmeg0ali_pers == null) {
        // INSERT
        objFmeg0ali.setTpmodeid(INPUT);
        objFmeg0ali = fmeg0aliBo.save(objFmeg0ali, true);

        tratarHijos();

        tx.commit();
      }
      else if ("".equals(objFmeg0ali_pers.getFhdebaja().trim())) {
        // UPDATE
        fmeg0aliBo.delete(objFmeg0ali_pers);

        objFmeg0ali.setTpmodeid(UPDATE);
        objFmeg0ali = fmeg0aliBo.save(objFmeg0ali, true);

        tratarHijos();

        tx.commit();
      }
      else {

        // INSERT DESPUES DE HABER DADO ESE REGISTRO DE BAJA LOGICA
        fmeg0aliBo.delete(objFmeg0ali_pers);
        objFmeg0ali.setTpmodeid(INPUT);
        objFmeg0ali = fmeg0aliBo.save(objFmeg0ali, true);

        tratarHijos();

        tx.commit();
      }

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void load() throws Exception {

    String objPrimaryKey;

    tratarCampos();

    objPrimaryKey = objFmeg0ali.getCdaliass();

    // transaccion
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0ali = fmeg0aliBo.findById(objPrimaryKey);
      objFmeg0ali.setTpmodeid(UPDATE);
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private void delete() throws Exception {

    String objPrimaryKey;
    Fmeg0ali objFmeg0ali_pers = new Fmeg0ali();

    tratarCampos();

    DateFormat fechaFormato = new SimpleDateFormat("yyyyMMdd");
    String fechaSt = fechaFormato.format(new Date());

    objPrimaryKey = objFmeg0ali.getCdaliass();
    EntityManager em = emf.createEntityManager();
    // transaccion
    EntityTransaction tx = em.getTransaction();

    try {
      tx.begin();

      objFmeg0ali_pers = fmeg0aliBo.findById(objPrimaryKey);
      if (objFmeg0ali_pers == null) {
        throw new Exception("No existe la identificacion de la entidad para darlo de baja");
      }
      else if ("".equals(objFmeg0ali_pers.getFhdebaja().trim())) {
        objFmeg0ali_pers.setFhdebaja(fechaSt);
        objFmeg0ali_pers.setTpmodeid(DELETE);
        objFmeg0ali = fmeg0aliBo.save(objFmeg0ali_pers, false);
      }
      else {
        throw new Exception("EL REGISTRO NO PUEDE DARSE DE BAJA, SE ENCUENTRA EN ESTADO DE BAJA LOGICA");
      }
      tx.commit();

    }

    catch (Exception e) {
      throw e;
    }

    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }

  }

  private boolean existenCondiciones() {

    boolean result = false;
    String broClient = "";

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    int i = 0;

    while (i < dataElement.getChildNodes().getLength() && broClient.equals("")) {

      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {
        if ("broClient".equals(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {

          broClient = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value");

        }
      }
      i++;
    }

    String pkFmeg0ent;
    String pkFmeg0brc;

    pkFmeg0ent = broClient;
    pkFmeg0brc = broClient;
    EntityManager em = emf.createEntityManager();
    EntityTransaction tx = em.getTransaction();
    try {
      tx.begin();

      if (fmeg0entBo.findById(pkFmeg0ent) != null && fmeg0brcBo.findById(pkFmeg0brc) != null) {
        result = true;
      }

    }
    catch (Exception e) {
      logger.info("No se dan las condiciones necesarias para el tratamiento");

    }
    finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      em.close();
    }
    return result;
  }

  private void tratarCampos() throws DOMException, Exception {

    Element dataElement = (Element) document.getElementsByTagName("data").item(0);

    for (int i = 0; i < dataElement.getChildNodes().getLength(); i++) {
      if ((dataElement.getChildNodes().item(i).getNodeType() == Node.ELEMENT_NODE)
          && ("field".equals(dataElement.getChildNodes().item(i).getNodeName()))) {

        switch (atrData.indexOf(((Element) dataElement.getChildNodes().item(i)).getAttribute("name"))) {
          case 0:// broClient
            objFmeg0ali.setCdbrocli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 1:// alias
            objFmeg0ali.setCdaliass(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 2:// allocationtype
            objFmeg0ali.setTpalloca(convertirMegaraAs400.execConvertir("TPALLOCA", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 3:// expectedAllocation
            objFmeg0ali.setTpexallo(convertirMegaraAs400.execConvertir("TPEXALLO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 4:// marketAllocationRule
            objFmeg0ali.setTpmaallo(convertirMegaraAs400.execConvertir("TPMAALLO", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 5:// confirmationType
            objFmeg0ali.setTpconfir(convertirMegaraAs400.execConvertir("TPCONFIR", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 6:// calculationType
            objFmeg0ali.setTpcalcuu(convertirMegaraAs400.execConvertir("TPCALCUU", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 7:// calculationtypeLM
            objFmeg0ali.setTpcalclm(convertirMegaraAs400.execConvertir("TPCALCLM", ((Element) dataElement
                .getChildNodes().item(i)).getAttribute("value")));
            break;
          case 8:// confirmToClearer
            objFmeg0ali.setIsconcle(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 9:// description
            objFmeg0ali.setDescrali(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 10:// forOwnAccount
            objFmeg0ali.setIsownacc(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 11:// isDefault
            objFmeg0ali.setIsdefaul(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 12:// payMarket
            objFmeg0ali.setIspaymar(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 13:// portfolio
            objFmeg0ali.setPortfoli(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 14:// reconcileAllocation
            objFmeg0ali.setIsrecall(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 15:// reconcileAllocationLM
            objFmeg0ali.setIsrecalm(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 16:// reconcileConfirmation
            objFmeg0ali.setIsreccon(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 17:// reconcileConfirmationLM
            objFmeg0ali.setIsrecclm(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 18:// requireAllocation
            objFmeg0ali.setIsreqalo(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 19:// requireAllocationLM
            objFmeg0ali.setIsreqalm(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 20:// requireAllocConfirmation
            objFmeg0ali.setIsreqalc(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 21:// requireConfirmation
            objFmeg0ali.setIsreqcon(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 22:// stp
            objFmeg0ali.setIsstproc(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 23:// exonerated
            objFmeg0ali.setIsexoner(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 24:// manualRetrocession
            objFmeg0ali.setIsmanret(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 25:// routing
            objFmeg0ali.setIsroutin(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 26:// requireRetrocession
            objFmeg0ali.setIsreqret(new ConvertirBoolean()
                .execConvertir(((Element) dataElement.getChildNodes().item(i)).getAttribute("value")));
            break;
          case 27:// trader
            /* AML 21-07-2011 */
            /*
             * Si la longitud de cdtrader excede de 8 se escribe cadena vac�a ""
             */
            String cdtrader = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value");
            if (cdtrader != null && cdtrader.length() > 8)
              cdtrader = "";
            objFmeg0ali.setCdtrader(cdtrader);
            break;
          case 28:// sale
            objFmeg0ali.setCdsaleid(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 29:// manager
            objFmeg0ali.setCdmanage(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 30:// statisticGroup
            objFmeg0ali.setStgroup(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 31:// statisticLevel0
            objFmeg0ali.setStlevel0(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 32:// statisticLevel1
            objFmeg0ali.setStlevel1(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 33:// statisticLevel2
            objFmeg0ali.setStlevel2(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 34:// statisticLevel3
            objFmeg0ali.setStlevel3(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 35:// statisticLevel4
            objFmeg0ali.setStlevel4(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 36:// traderLM
            /* AML 21-07-2011 */
            /*
             * Si la longitud de cdtradlm excede de 8 se escribe cadena vac�a ""
             */
            String cdtradlm = ((Element) dataElement.getChildNodes().item(i)).getAttribute("value");
            if (cdtradlm != null && cdtradlm.length() > 8)
              cdtradlm = "";
            objFmeg0ali.setCdtradlm(cdtradlm);
            break;
          case 37:// saleLM
            objFmeg0ali.setCdsalelm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 38:// managerLM
            objFmeg0ali.setCdmanalm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 39:// statisticGroupLM
            objFmeg0ali.setStgroulm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 40:// statisticLevel0LM
            objFmeg0ali.setStlev0lm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 41:// statisticLevel1LM
            objFmeg0ali.setStlev1lm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 42:// statisticLevel2LM
            objFmeg0ali.setStlev2lm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 43:// statisticLevel3LM
            objFmeg0ali.setStlev3lm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;
          case 44:// statisticLevel4LM
            objFmeg0ali.setStlev4lm(((Element) dataElement.getChildNodes().item(i)).getAttribute("value"));
            break;

          default:
            ;
        }
      }
    }

  }

  private void tratarHijos() throws DOMException, Exception {

    for (int i = 0; i < (document.getElementsByTagName("stopStpException").getLength()); i++) {

      if (document.getElementsByTagName("stopStpException").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarStopStpException(((Element) document.getElementsByTagName("stopStpException").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("payMarketException").getLength()); i++) {

      if (document.getElementsByTagName("payMarketException").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarPayMarketException(((Element) document.getElementsByTagName("payMarketException").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("holder").getLength()); i++) {

      if (document.getElementsByTagName("holder").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarHolder(((Element) document.getElementsByTagName("holder").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("otherId").getLength()); i++) {

      if (document.getElementsByTagName("otherId").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarOtherId(((Element) document.getElementsByTagName("otherId").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("eligibleCustodian").getLength()); i++) {

      if (document.getElementsByTagName("eligibleCustodian").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarEligibleCustodian(((Element) document.getElementsByTagName("eligibleCustodian").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("eligibleClearer").getLength()); i++) {

      if (document.getElementsByTagName("eligibleClearer").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarEligibleClearer(((Element) document.getElementsByTagName("eligibleClearer").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("idBySE").getLength()); i++) {

      if (document.getElementsByTagName("idBySE").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarIdBySE(((Element) document.getElementsByTagName("idBySE").item(i)));
      }

    }
    for (int i = 0; i < (document.getElementsByTagName("address").getLength()); i++) {

      if (document.getElementsByTagName("address").item(i).getNodeType() == Node.ELEMENT_NODE) {
        tratarAddress(((Element) document.getElementsByTagName("address").item(i)));
      }

    }
  }

  private void tratarStopStpException(Node stopStpException) {

    Fmeg0ast objFmeg0ast = new Fmeg0ast();
    objFmeg0ast.setId(new Fmeg0astId(objFmeg0ali.getCdaliass(), null));
    objFmeg0ast.setIdstexch("");

    NodeList listField = stopStpException.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrStopStpException.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// stockExchange
              objFmeg0ast.setIdstexch(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0ast.setFmeg0ali(objFmeg0ali);
    fmeg0astBo.save(objFmeg0ast, true);

  }

  private void tratarPayMarketException(Node payMarketException) {

    Fmeg0apm objFmeg0apm = new Fmeg0apm();
    objFmeg0apm.setId(new Fmeg0apmId(objFmeg0ali.getCdaliass(), null));
    objFmeg0apm.setIdstexch("");

    NodeList listField = payMarketException.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrPayMarketException.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// stockExchange
              objFmeg0apm.setIdstexch(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0apm.setFmeg0ali(objFmeg0ali);
    fmeg0apmBo.save(objFmeg0apm, true);

  }

  private void tratarHolder(Node holder) throws DOMException, Exception {

    Fmeg0ahl objFmeg0ahl = new Fmeg0ahl();
    objFmeg0ahl.setId(new Fmeg0ahlId(objFmeg0ali.getCdaliass(), null));
    objFmeg0ahl.setCdextref("");
    objFmeg0ahl.setIsmainal("");
    objFmeg0ahl.setPartperc("");
    objFmeg0ahl.setCdholder("");
    objFmeg0ahl.setTpholder("");

    NodeList listField = holder.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrHolder.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// externalreference
              objFmeg0ahl.setCdextref(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 1:// isMain
              objFmeg0ahl.setIsmainal(new ConvertirBoolean().execConvertir(listField.item(i).getAttributes().item(1)
                  .getNodeValue()));
              break;
            case 2:// participationPercentage
              objFmeg0ahl.setPartperc(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// holder
              objFmeg0ahl.setCdholder(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 4:// holderType
              objFmeg0ahl.setTpholder(convertirMegaraAs400.execConvertir("TPHOLDER", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0ahl.setFmeg0ali(objFmeg0ali);
    fmeg0ahlBo.save(objFmeg0ahl, true);
  }

  private void tratarOtherId(Node otherId) throws DOMException, Exception {

    Fmeg0aot objFmeg0aot = new Fmeg0aot();
    objFmeg0aot.setId(new Fmeg0aotId(objFmeg0ali.getCdaliass(), null));
    objFmeg0aot.setCdotheid("");
    objFmeg0aot.setTpidtype("");
    objFmeg0aot.setDescrip1("");
    objFmeg0aot.setTpdocume("");

    NodeList listField = otherId.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrOtherId.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// identifier
              objFmeg0aot.setCdotheid(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 1:// idType
              objFmeg0aot.setTpidtype(convertirMegaraAs400.execConvertir("TPIDTYPE", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 2:// description
              objFmeg0aot.setDescrip1(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// documentType
              String tpdocume = listField.item(i).getAttributes().item(1).getNodeValue();
              if (tpdocume != null && !"".equals(tpdocume.trim()) && tpdocume.startsWith("MIFID-")) {
                tpdocume = tpdocume.substring(6).trim();

              }
              else {
                tpdocume = convertirMegaraAs400.execConvertir("TPDOCUME", listField.item(i).getAttributes().item(1)
                    .getNodeValue());
              }
              objFmeg0aot.setTpdocume(tpdocume);
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0aot.setFmeg0ali(objFmeg0ali);
    fmeg0aotBo.save(objFmeg0aot, true);

  }

  private void tratarEligibleCustodian(Node eligibleCustodian) throws DOMException, Exception {

    Fmeg0aec objFmeg0aec = new Fmeg0aec();
    objFmeg0aec.setId(new Fmeg0aecId(objFmeg0ali.getCdaliass(), null));
    objFmeg0aec.setTpsettle("");
    objFmeg0aec.setIdentitm("");
    objFmeg0aec.setCdmarket("");
    objFmeg0aec.setTpclemod("");
    objFmeg0aec.setCdcustod("");
    objFmeg0aec.setIdenvfid("N");

    NodeList listField = eligibleCustodian.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrEligibleCustodian.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// settlementType
              objFmeg0aec.setTpsettle(convertirMegaraAs400.execConvertir("TPSETTLE", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 1:// mainEntityId
              objFmeg0aec.setIdentitm(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// market
              objFmeg0aec.setCdmarket(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// clearingMode
              objFmeg0aec.setTpclemod(convertirMegaraAs400.execConvertir("TPCLEMOD", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 4:// custodian
              objFmeg0aec.setCdcustod(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0aec.setFmeg0ali(objFmeg0ali);
    femg0aecBo.save(objFmeg0aec, true);

  }

  private void tratarEligibleClearer(Node eligibleClearer) throws DOMException, Exception {

    Fmeg0ael objFmeg0ael = new Fmeg0ael();
    objFmeg0ael.setId(new Fmeg0aelId(objFmeg0ali.getCdaliass(), null));
    objFmeg0ael.setTpsettle("");
    objFmeg0ael.setIdentitm("");
    objFmeg0ael.setCdmarket("");
    objFmeg0ael.setCdcleare("");
    objFmeg0ael.setAccatcle("");
    objFmeg0ael.setIdenvfid("N");

    NodeList listField = eligibleClearer.getChildNodes();

    objFmeg0ael.setFmeg0ali(objFmeg0ali);

    for (int i = 0; i < listField.getLength(); i++) {

      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {

        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrEligibleClearer.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// settlementType
              objFmeg0ael.setTpsettle(convertirMegaraAs400.execConvertir("TPSETTLE", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 1:// mainEntityId
              objFmeg0ael.setIdentitm(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// market
              objFmeg0ael.setCdmarket(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// clearer
              objFmeg0ael.setCdcleare(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 4:// accountAtClearer
              objFmeg0ael.setAccatcle(listField.item(i).getAttributes().item(1).getNodeValue());
              break;

            default:
              ;
          }
        }

      }

    }
    fmeg0aelBo.save(objFmeg0ael, true);

    for (int i = 0; i < eligibleClearer.getChildNodes().getLength(); i++) {

      if ("settlementDetail".equals(eligibleClearer.getChildNodes().item(i).getNodeName())) {
        tratarSettlementDetail(eligibleClearer.getChildNodes().item(i), objFmeg0ael);
      }

    }

  }

  private void tratarSettlementDetail(Node settlementDetail, Fmeg0ael objFmeg0ael) throws DOMException, Exception {

    Fmeg0asd objFmeg0asd = new Fmeg0asd();
    objFmeg0asd.setId(new Fmeg0asdId(objFmeg0ali.getCdaliass(), objFmeg0ael.getId().getNusecael(), null));

    objFmeg0asd.setTpclemod("");
    objFmeg0asd.setCdcustod("");
    objFmeg0asd.setIssendch("");
    objFmeg0asd.setIsmancom("");

    NodeList listField = settlementDetail.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {

      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {

        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrSettlementDetail.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// clearingMode
              objFmeg0asd.setTpclemod(convertirMegaraAs400.execConvertir("TPCLEMOD", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 1:// custodian
              objFmeg0asd.setCdcustod(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// sendComToCH
              objFmeg0asd.setIssendch(new ConvertirBoolean().execConvertir(listField.item(i).getAttributes().item(1)
                  .getNodeValue()));
              break;
            case 3:// cHManageCom
              objFmeg0asd.setIsmancom(new ConvertirBoolean().execConvertir(listField.item(i).getAttributes().item(1)
                  .getNodeValue()));
              break;
            case 4:// confirmSettlement

              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0asd.setFmeg0ael(objFmeg0ael);
    fmeg0asdBo.save(objFmeg0asd, true);
  }

  private void tratarIdBySE(Node idBySE) {

    Fmeg0aid objFmeg0aid = new Fmeg0aid();
    objFmeg0aid.setId(new Fmeg0aidId(objFmeg0ali.getCdaliass(), null));
    objFmeg0aid.setCdidbyse("");
    objFmeg0aid.setIdstexch("");
    objFmeg0aid.setDescrip2("");

    NodeList listField = idBySE.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrIdBySE.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// identifier
              objFmeg0aid.setCdidbyse(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 1:// stockExchange
              objFmeg0aid.setIdstexch(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// description
              objFmeg0aid.setDescrip2(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0aid.setFmeg0ali(objFmeg0ali);
    fmeg0aidBo.save(objFmeg0aid, true);
  }

  private void tratarAddress(Node address) throws DOMException, Exception {

    Fmeg0aad objFmeg0aad = new Fmeg0aad();
    objFmeg0aad.setId(new Fmeg0aadId(objFmeg0ali.getCdaliass(), null));
    objFmeg0aad.setTpaddres("");
    objFmeg0aad.setContfunc("");
    objFmeg0aad.setContname("");
    objFmeg0aad.setPurposee("");
    objFmeg0aad.setMailaddr("");
    objFmeg0aad.setCdzipcod("");
    objFmeg0aad.setNblocat1("");
    objFmeg0aad.setNblocat2("");
    objFmeg0aad.setNbstreet("");
    objFmeg0aad.setNustreet("");
    objFmeg0aad.setNuflattt("");
    objFmeg0aad.setNufaxnum("");
    objFmeg0aad.setEmailadd("");
    objFmeg0aad.setSwiftadd("");
    objFmeg0aad.setNuphonee("");
    objFmeg0aad.setIdoasysa("");
    objFmeg0aad.setIdsequal("");
    objFmeg0aad.setNutelexx("");
    objFmeg0aad.setNofilead("");
    objFmeg0aad.setCdindadd("");

    NodeList listField = address.getChildNodes();

    for (int i = 0; i < listField.getLength(); i++) {
      if (listField.item(i).getNodeType() == Node.ELEMENT_NODE) {
        if (listField.item(i).getAttributes().item(0) != null) {

          switch (atrOtherAddress.indexOf(listField.item(i).getAttributes().item(0).getNodeValue())) {
            case 0:// adresseType
              objFmeg0aad.setTpaddres(convertirMegaraAs400.execConvertir("TPADDRES", listField.item(i).getAttributes()
                  .item(1).getNodeValue()));
              break;
            case 1:// contactFunction
              objFmeg0aad.setContfunc(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 2:// contactName
              objFmeg0aad.setContname(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 3:// purpose
              objFmeg0aad.setPurposee(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 4:// amaeladdress
              objFmeg0aad.setMailaddr(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 5:// zipCode
              objFmeg0aad.setCdzipcod(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 6:// location
              objFmeg0aad.setNblocat1(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 7:// location2
              objFmeg0aad.setNblocat2(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 8:// street
              objFmeg0aad.setNbstreet(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 9:// streetNumber
              objFmeg0aad.setNustreet(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 10:// flat
              objFmeg0aad.setNuflattt(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 11:// afaxnumber
              objFmeg0aad.setNufaxnum(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 12:// aemailaddress
              objFmeg0aad.setEmailadd(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 13:// aswiftaddress
              objFmeg0aad.setSwiftadd(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 14:// aphonenumber
              objFmeg0aad.setNuphonee(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 15:// aoasysaddress
              objFmeg0aad.setIdoasysa(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 16:// asequaladdress
              objFmeg0aad.setIdsequal(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 17:// atelexnumber
              objFmeg0aad.setNutelexx(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            case 18:// afileaddress
              objFmeg0aad.setNofilead(listField.item(i).getAttributes().item(1).getNodeValue());
              break;
            default:
              ;
          }
        }
      }
    }

    objFmeg0aad.setFmeg0ali(objFmeg0ali);
    fmeg0aadBo.save(objFmeg0aad, true);

  }

}