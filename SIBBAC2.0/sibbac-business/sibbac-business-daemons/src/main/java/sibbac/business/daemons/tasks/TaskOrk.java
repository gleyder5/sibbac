package sibbac.business.daemons.tasks;

import java.io.File;

import org.slf4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.ork.bo.OrkFidessaBo;
import sibbac.business.daemons.ork.bo.OrkFidessaFieldCollection;
import sibbac.business.daemons.ork.bo.OrkBmeBo;
import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.tasks.annotations.SIBBACJob;
import static sibbac.tasks.Task.GROUP_DAEMONS.NAME;
import static sibbac.tasks.Task.GROUP_DAEMONS.JOB_ORK;
import static sibbac.tasks.enums.SIBBACJobType.CRON_JOB;
import static sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE.TASK_DAEMONS_ORK;

@DisallowConcurrentExecution
@SIBBACJob(group = NAME, name = JOB_ORK, jobType = CRON_JOB, cronExpression = "0 0/30 * ? * MON-FRI *")
public class TaskOrk extends WrapperTaskConcurrencyPrevent {
	
	private static final Logger LOG = getLogger(TaskOrk.class);
	
	private OrkFidessaBo orkFidessaBo;
	
	private OrkBmeBo orkBmeBo;
	
	private ApplicationContext ctx;

	@Override
	public void executeTask() throws Exception {
		final OrkMercadosConfig config;
		final File updateInfoFile;
		final OrkFidessaFieldCollection fieldCollection;
		final Batch batch, updateBatch;
		int i = 0;
		
		LOG.debug("[executeTask] Inicio");
		config = ctx.getBean(OrkMercadosConfig.class);
		fieldCollection = orkFidessaBo.getFieldCollection();
		batch = fieldCollection.loadOrkBatch();
		for(File origen : config.getFicherosFidessa()) {
			orkFidessaBo.lecturaFidessa(config, batch, origen);
			i++;
		}
		LOG.info("[executeTask] Se han procesado {} ficheros fidesa", i);
		if(orkFidessaBo.existenSinTratar()) {
			updateInfoFile = orkBmeBo.generaOrk(fieldCollection);
      updateBatch = fieldCollection.loadUpdateBatch();
			orkFidessaBo.updateBatch(updateBatch, updateInfoFile, config.getEscrituraTimeout());
		}
		else {
			LOG.info("No se han encontrado registros Fidessa sin tratar");
		}
		LOG.debug("[executeTask] Fin");
	}

	@Override
	public TIPO determinarTipoApunte() {
		return TASK_DAEMONS_ORK;
	}
	
	@Autowired
	void setContext(ApplicationContext ctx) {
		this.ctx = ctx;
	}
	
	@Autowired
	void setOrkFidessaBo(final OrkFidessaBo orkFidessaBo) {
		this.orkFidessaBo = orkFidessaBo;
	}
	
	@Autowired
	void setOrkBmeBo(final OrkBmeBo orkBmeBo) {
		this.orkBmeBo = orkBmeBo;
	}

}
