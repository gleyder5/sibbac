package sibbac.business.daemons.ork.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Entidad que representa un campo del Fichero Fidessa que será almacenado como parte de la entidad OrkFidessa
 * @author XI326526
 */
@Entity
@DiscriminatorValue("F")
public class OrkFidessaFixedField extends OrkFidessaField {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 4808887781405546883L;
  
  @Column(name = "KEY", length = 1)
  private char key;

  /**
   * @return el indicador de si el campo es parte de la llave primaria de OrkFidessa
   */
  public char getKey() {
    return key;
  }

  /**
   * @param key determina si el campo es parte de la llave primaria de OrkFidessa, siendo el valor 'S' el que indica
   * esta circunstancia.
   */
  public void setKey(char key) {
    this.key = key;
  }
  
  /**
   * Indica si el campo es parte de la llave primaria de la entidad OrkFidessa
   * @return verdadero en caso de que {@link #key} sea igual a 'S'
   */
  public boolean isKey() {
    return key == 'S';
  }

}
