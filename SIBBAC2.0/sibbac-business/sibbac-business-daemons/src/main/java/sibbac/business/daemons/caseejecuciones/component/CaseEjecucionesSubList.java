package sibbac.business.daemons.caseejecuciones.component;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.daemons.caseejecuciones.exception.CaseEjecucionesException;
import sibbac.business.daemons.caseejecuciones.util.CaseEjecucionesKeyMapDTO;
import sibbac.business.daemons.caseejecuciones.util.Tmct0bokKeyMapDTO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.wrappers.database.bo.Tmct0bokBo;
import sibbac.business.wrappers.database.bo.Tmct0caseoperacionejeBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseCamaraBo;
import sibbac.business.wrappers.database.bo.Tmct0desgloseclititBo;
import sibbac.business.wrappers.database.bo.Tmct0ejeBo;
import sibbac.business.wrappers.database.bo.Tmct0logBo;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.dto.caseejecuciones.DesgloseClititCaseEjecucionesInformacionMercadoDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0bokIdDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0caseoperacionejeInformacionMercadoCaseDTO;
import sibbac.business.wrappers.database.dto.caseejecuciones.Tmct0ejeInformacionMercadoCaseDTO;

/**
 * 
 * Clase encargada de hacer el case de las ejecuciones y bookings en sublistas transaccionales
 * 
 * @author xIS16630
 *
 */
@Service
public class CaseEjecucionesSubList {
  protected static final Logger LOG = LoggerFactory.getLogger(CaseEjecucionesSubList.class);

  @Autowired
  private Tmct0ejeBo tmct0ejeBo;
  @Autowired
  private Tmct0bokBo tmct0bokBo;
  @Autowired
  private Tmct0desgloseCamaraBo tmct0desglosecamaraBo;
  @Autowired
  private Tmct0desgloseclititBo desgloseclititBo;
  @Autowired
  private Tmct0caseoperacionejeBo caseoperacionejeBo;
  @Autowired
  private Tmct0logBo tmct0logBo;
  @Autowired
  private Tmct0menBo tmct0menBo;

  private Long lastUpdateSemaphore;

  /**
   * 
   * Casa la lista de ejcuciones en una sola transaccion</br>Actualiza los datos de mercado de los
   * desgloses</br>Actualiza los flag casepti
   * 
   * @param ejecuciones
   * @param cases copia de los titulos que quedan en el case, lo que contenga este map acabara actualizando el case
   * @throws CaseEjecucionesException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW,
                 rollbackFor = { CaseEjecucionesException.class, Exception.class
                 })
  public void casarEjecuciones(List<Tmct0ejeInformacionMercadoCaseDTO> ejecuciones,
                               Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) throws CaseEjecucionesException {
    LOG.debug("casarEjecuciones() ## Inicio");

    if (ejecuciones != null && !ejecuciones.isEmpty()) {
      Collection<Tmct0bokKeyMapDTO> bookingsRevisarCase = new HashSet<Tmct0bokKeyMapDTO>();

      LOG.debug("casarEjecuciones() ## Vamos a quitar los titulos del case...");
      restarTitulosCase(ejecuciones, cases, bookingsRevisarCase);

      LOG.debug("casarEjecuciones() ## Vamos a actualizar los titulos del case...");
      if (cases != null && !cases.isEmpty()) {
        updateTitulosPendientesCase(cases);
      } else {
        LOG.debug("casarEjecuciones() ## No hay titulos que actualizar del case.");
      }

      LOG.debug("casarEjecuciones() ## Vamos a actualizar los flag casepti...");
      if (bookingsRevisarCase != null && !bookingsRevisarCase.isEmpty()) {
        updateFlagsCasePti(bookingsRevisarCase);
        bookingsRevisarCase.clear();
      } else {
        LOG.debug("casarEjecuciones() ## No hay flag casepti que actualizar.");
      }
    } // if (ejecuciones != null && !ejecuciones.isEmpty())

    LOG.debug("casarEjecuciones() ## Fin");
  }

  /**
   * 
   * Resta los titulos de las ejecuciones al map cases, guarda la coleccion de bookings que se han revisado
   * 
   * @param ejecuciones
   * @param cases
   * @param bookingsRevisarCase
   * 
   * @throws CaseEjecucionesException
   */
  @Transactional
  private void restarTitulosCase(List<Tmct0ejeInformacionMercadoCaseDTO> ejecuciones,
                                 Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases,
                                 Collection<Tmct0bokKeyMapDTO> bookingsRevisarCase) throws CaseEjecucionesException {
    LOG.trace("restarTitulosCase() ## Inicio");
    CaseEjecucionesKeyMapDTO key;
    Tmct0caseoperacionejeInformacionMercadoCaseDTO caseEje;
    Tmct0bokKeyMapDTO keyBok;
    int i = 1;
    String msg;
    for (Tmct0ejeInformacionMercadoCaseDTO eje : ejecuciones) {
      updateAliveFlag();

      try {
        LOG.trace("restarTitulosCase() ## pos == {} ## sublist == {}", i++, ejecuciones.size());
        LOG.trace("restarTitulosCase() ## key == {}", eje);
        LOG.trace("restarTitulosCase() ## N.Orden == {}", eje.getNumeroOrden());
        LOG.trace("restarTitulosCase() ## N.Booking == {}", eje.getNumeroBooking());
        LOG.trace("restarTitulosCase() ## N.Ejecucion == {}", eje.getNumeroEjecucion());
        LOG.trace("restarTitulosCase() ## F.Ejecucion == {}", eje.getFechaEjecucion());
        LOG.trace("restarTitulosCase() ## ISIN == {}", eje.getIsin());
        LOG.trace("restarTitulosCase() ## Sentido == {}", eje.getSentido());
        LOG.trace("restarTitulosCase() ## Miembro == {}", eje.getMiembroMercado());
        LOG.trace("restarTitulosCase() ## Seg.Mercado == {}", eje.getSegmentoEjecucion());
        LOG.trace("restarTitulosCase() ## T.Op.Bolsa == {}", eje.getTipoOperacionBolsa());
        LOG.trace("restarTitulosCase() ## N.Orden.Mercado == {}", eje.getNumeroOperacionMercado());
        LOG.trace("restarTitulosCase() ## N.Ejecucion.Mercado == {}", eje.getNumeroEjecucionMercado());
        key = new CaseEjecucionesKeyMapDTO(eje.getIsin(), eje.getSentido(), eje.getFechaEjecucion(),
                                           eje.getMiembroMercado(), eje.getSegmentoEjecucion(),
                                           eje.getTipoOperacionBolsa(), eje.getNumeroOperacionMercado(),
                                           eje.getNumeroEjecucionMercado());
        LOG.trace("restarTitulosCase() ## Buscando case en map...");
        caseEje = cases.get(key);
        if (caseEje != null) {
          LOG.trace("restarTitulosCase() ## Encontrado case en map.");
        } else {
          LOG.trace("restarTitulosCase() ## Case no encontrado en map.");
          LOG.trace("restarTitulosCase() ## Buscando case en bbdd...");
          caseEje = this.caseoperacionejeBo.findCaseoperacionEjeInformacionMercado(eje.getNumeroOperacionMercado(),
                                                                                   eje.getNumeroEjecucionMercado(),
                                                                                   eje.getFechaEjecucion(),
                                                                                   eje.getTipoOperacionBolsa(),
                                                                                   eje.getIsin(),
                                                                                   eje.getSegmentoEjecucion(),
                                                                                   eje.getSentido(),
                                                                                   eje.getMiembroMercado());
          if (caseEje != null) {
            LOG.trace("restarTitulosCase() ## Añadiendo case al map...");
            cases.put(key, caseEje);
          }
        } // else

        if (caseEje == null) {
          LOG.warn("restarTitulosCase() ## No se ha encontrado el case para la ejecucion. {}", eje);
        } else if (caseEje.getTitulosPendientesCase().compareTo(eje.getTitulos()) >= 0) {
          LOG.trace("restarTitulosCase() ## case == {}", caseEje);

          LOG.trace("restarTitulosCase() ## Restando eje.Titulos == {}", eje.getTitulos());
          LOG.trace("restarTitulosCase() ## TitulosPendientesCase == {}", caseEje.getTitulosPendientesCase());
          caseEje.setTitulosPendientesCase(caseEje.getTitulosPendientesCase().subtract(eje.getTitulos()));
          LOG.trace("restarTitulosCase() ## TitulosPendientesCase == {} despues de restar.",
                    caseEje.getTitulosPendientesCase());
          caseEje.setUpdated(Boolean.TRUE);
          LOG.trace("restarTitulosCase() ## updated == {} para actualizar despues.", caseEje.isUpdated());

          LOG.trace("restarTitulosCase() ## Actualizando ejecucion a casada, inicio...");
          Integer updateNum = tmct0ejeBo.updateCasadoPti(eje.getNumeroOrden(), eje.getNumeroBooking(),
                                                         eje.getNumeroEjecucion(), caseEje.getIdCase());
          LOG.trace("restarTitulosCase() ## Actualizada ejecucion a casada, fin. Resultado: {}", updateNum);

          LOG.trace("restarTitulosCase() ## Cargando datos de mercado en desgloses, inicio...");
          updateNum = 0;
          updateNum = desgloseclititBo.updateDatosMercado(eje.getNumeroOrden(), eje.getNumeroBooking(),
                                                          eje.getNumeroEjecucion(), new Date(), "CASEPTI",
                                                          caseEje.getIndicadorCotizacion(),
                                                          caseEje.getCdoperacionecc(), caseEje.getSegmentoEjecucion(),
                                                          caseEje.getPlataformaNegociacion(),
                                                          caseEje.getFechaOrdenMercado(), caseEje.getHoraOrdenMercado());
          LOG.trace("restarTitulosCase() ## Cargados datos de mercado en desgloses, fin. Resultado: {}", updateNum);

          keyBok = new Tmct0bokKeyMapDTO(eje.getNumeroOrden(), eje.getNumeroBooking());
          LOG.trace("restarTitulosCase() ## Añadiendo booking para revisar si esta completamente casado. {}", keyBok);

          bookingsRevisarCase.add(keyBok);
        } else {
          LOG.warn("restarTitulosCase() ## No hay titulos suficientes, pasamos a la siguiente ejecucion. {}", eje);
        } // else
      } catch (Exception e) {
        msg = "restarTitulosCase() ## Problema con ejecucion " + eje;
        LOG.warn(msg);
        throw new CaseEjecucionesException(msg, e);
      } // catch
    } // for (Tmct0ejeInformacionMercadoCaseDTO eje : ejecuciones)
    LOG.trace("restarTitulosCase() ## Fin");
  } // restarTitulosCase

  /**
   * Hace el update de los titulos pendientes del case
   * 
   * @param cases
   * @throws CaseEjecucionesException
   */
  @Transactional
  private void updateTitulosPendientesCase(Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) throws CaseEjecucionesException {
    LOG.trace("updateTitulosPendientesCase() ## Inicio");
    String msg;
    for (Tmct0caseoperacionejeInformacionMercadoCaseDTO caseEje1 : cases.values()) {
      updateAliveFlag();
      if (caseEje1.isUpdated()) {
        try {
          LOG.trace("updateTitulosPendientesCase() ## Actualiznado case cdoperacionecc {} ...",
                    caseEje1.getCdoperacionecc());
          LOG.trace("updateTitulosPendientesCase() ## Actualiznado case id {}", caseEje1.getIdCase());

          LOG.trace("updateTitulosPendientesCase() ## Actualiznado N.Tit.Pend.Case == {}, inicio...",
                    caseEje1.getTitulosPendientesCase());
          Integer updateNum = caseoperacionejeBo.updateTitulosPendientesCase(caseEje1.getIdCase(),
                                                                             caseEje1.getTitulosPendientesCase(),
                                                                             new Date());

          LOG.trace("updateTitulosPendientesCase() ## Actualizado case id {}, fin...", caseEje1.getIdCase());
          LOG.trace("Resultado: {}", updateNum);
          caseEje1.setUpdated(Boolean.FALSE);
          LOG.trace("restarTitulosCase() ## updated == {} Se reestablece el flag", caseEje1.isUpdated());
        } catch (Exception e) {
          msg = "updateTitulosPendientesCase() ## Problema actualizando case " + caseEje1;
          LOG.warn(msg);
          throw new CaseEjecucionesException(msg, e);
        }
      }
    } // for
    LOG.trace("updateTitulosPendientesCase() ## Fin");
  } // updateTitulosPendientesCase

  /**
   * Hace el update de los flag de casepti
   * 
   * @param bookingsRevisarCase
   * @throws CaseEjecucionesException
   */
  @Transactional
  private void updateFlagsCasePti(Collection<Tmct0bokKeyMapDTO> bookingsRevisarCase) throws CaseEjecucionesException {
    LOG.trace("updateFlagsCasePti() ## Inicio");
    String msg;
    for (Tmct0bokKeyMapDTO book : bookingsRevisarCase) {
      updateAliveFlag();
      LOG.trace("updateFlagsCasePti() ## Revisando case de booking {} ", book);
      try {
        if (tmct0ejeBo.countNoCasadoPti(book.getNuorden(), book.getNbooking()) == 0) {
          LOG.trace("updateFlagsCasePti() ## Todas las ejecuciones estan casadas.");
          LOG.trace("updateFlagsCasePti() ## Marcando booking como casado, inicio...");
          Integer updateNum = tmct0bokBo.updateFlagCasadoPti(book.getNuorden(), book.getNbooking());
          LOG.trace("updateFlagsCasePti() ## Marcando booking como casado, fin. Resultado: {}", updateNum);
          LOG.trace("updateFlagsCasePti() ## Marcando desgloses camara como casado, inicio...");

          updateNum = 0;
          updateNum = tmct0desglosecamaraBo.updateFlagCasadoPti(book.getNuorden(), book.getNbooking());
          LOG.trace("updateFlagsCasePti() ## Marcando desgloses camara como casado, fin. Resultado: {}", updateNum);

          LOG.trace("updateFlagsCasePti() ## Insertando en tmct0log mensaje de casado, inicio...");
          tmct0logBo.insertarRegistro(book.getNuorden(), book.getNbooking(), "Booking casado", "", "CASEPTI");
          LOG.trace("updateFlagsCasePti() ## Insertando en tmct0log mensaje de casado, fin.");
        } else {
          LOG.trace("updateFlagsCasePti() ## El booking no esta completamente casado.");
        }
      } catch (Exception e) {
        msg = "updateFlagsCasePti() ## Problema actualizando flag casepti en booking " + book;
        LOG.warn(msg);
        throw new CaseEjecucionesException(msg, e);
      }
    } // for
    LOG.trace("updateFlagsCasePti() ## Fin");
  } // updateFlagsCasePti

  /**
   * Hace el update de los flag de casepti del desglose camara, el booking debe estar ya casado, ademas carga los datos
   * de mercado en los desgloses clitit
   * 
   * @param bookingsRevisarCase
   * @throws CaseEjecucionesException
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW,
                 rollbackFor = { CaseEjecucionesException.class, Exception.class
                 })
  public void updateFlagsCasePtiDesgloseCamara(List<Tmct0bokIdDTO> bookingsRevisarCase,
                                               Map<CaseEjecucionesKeyMapDTO, Tmct0caseoperacionejeInformacionMercadoCaseDTO> cases) throws CaseEjecucionesException {
    LOG.debug("updateFlagsCasePtiDesgloseCamara() ## Inicio");
    String msg;
    CaseEjecucionesKeyMapDTO key;
    Tmct0caseoperacionejeInformacionMercadoCaseDTO caseEje;
    List<DesgloseClititCaseEjecucionesInformacionMercadoDTO> desgloses;
    for (Tmct0bokIdDTO book : bookingsRevisarCase) {
      updateAliveFlag();
      LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Revisando case de booking {} ", book);
      try {
        if (tmct0ejeBo.countNoCasadoPti(book.getNuorden(), book.getNbooking()) == 0) {
          Integer updateNum = 0;
          updateNum = tmct0desglosecamaraBo.updateFlagCasadoPti(book.getNuorden(), book.getNbooking());
          LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Marcando desgloses camara como casado, fin. Resultado: {}",
                    updateNum);

          LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Buscando desglosesclitit sin datos de mercado...");
          desgloses = desgloseclititBo.findDesglosesSinDatosMercadoByNuordenNbooking(book.getNuorden(),
                                                                                     book.getNbooking());
          for (DesgloseClititCaseEjecucionesInformacionMercadoDTO dct : desgloses) {
            key = new CaseEjecucionesKeyMapDTO(dct.getIsin(), dct.getSentido(), dct.getFechaEjecucion(),
                                               dct.getMiembroMercado(), dct.getSegmentoEjecucion(),
                                               dct.getTipoOperacionBolsa(), dct.getNumeroOperacionMercado(),
                                               dct.getNumeroEjecucionMercado());
            caseEje = cases.get(key);
            if (caseEje == null) {
              LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Case no encontrado en map.");
              LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Buscando case en bbdd...");
              caseEje = this.caseoperacionejeBo.findCaseoperacionEjeInformacionMercado(dct.getNumeroOperacionMercado(),
                                                                                       dct.getNumeroEjecucionMercado(),
                                                                                       dct.getFechaEjecucion(),
                                                                                       dct.getTipoOperacionBolsa(),
                                                                                       dct.getIsin(),
                                                                                       dct.getSegmentoEjecucion(),
                                                                                       dct.getSentido(),
                                                                                       dct.getMiembroMercado());
              if (caseEje != null) {
                LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Añadiendo case al map...");
                cases.put(key, caseEje);
              }
            }
            if (caseEje == null) {
              LOG.warn("updateFlagsCasePtiDesgloseCamara() ## No se ha encontrado el case para el desglose. {}", dct);
              throw new CaseEjecucionesException("Incidencia, no se ha encontrado el case para el desglose. " + dct);
            } else {
              LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Cargando datos de mercado en desgloses, inicio...");
              updateNum = 0;
              updateNum = desgloseclititBo.updateDatosMercado(book.getNuorden(), book.getNbooking(), dct.getNuejecuc(),
                                                              new Date(), "CASEPTI", caseEje.getIndicadorCotizacion(),
                                                              caseEje.getCdoperacionecc(),
                                                              caseEje.getSegmentoEjecucion(),
                                                              caseEje.getPlataformaNegociacion(),
                                                              caseEje.getFechaOrdenMercado(),
                                                              caseEje.getHoraOrdenMercado());
              LOG.trace("updateFlagsCasePtiDesgloseCamara() ## Cargados datos de mercado en desgloses, fin. Resultado: {}",
                        updateNum);
            }
          }// fin for desgloses

        } else {
          LOG.trace("updateFlagsCasePtiDesgloseCamara() ## El booking no esta completamente casado.");
        }
      } catch (Exception e) {
        msg = "updateFlagsCasePtiDesgloseCamara() ## Problema actualizando flag casepti en booking " + book;
        LOG.warn(msg);
        throw new CaseEjecucionesException(msg, e);
      }
    } // for
    LOG.debug("updateFlagsCasePtiDesgloseCamara() ## Fin");
  } // updateFlagsCasePti

  /**
   * 
   */
  @Transactional
  private void updateAliveFlag() {
    try {
      long now = System.currentTimeMillis();
      if (lastUpdateSemaphore == null || lastUpdateSemaphore + 5 * 10000 < now) {
        LOG.trace("updateAliveFlag() ## Actualizando el fhaudit del semaforo {} ",
                  TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES);
        lastUpdateSemaphore = now;
        tmct0menBo.updateFhauditMEN(TIPO_TAREA.TASK_DAEMONS_CASE_EJECUCIONES);
      }
    } catch (Exception e) {
      LOG.warn("updateFlagsCasePti() ## Problema", e);
    }
  } // updateAliveFlag

} // CaseEjecucionesSubList
