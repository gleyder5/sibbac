package sibbac.business.daemons.speechminer.bo;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.io.Files;
import com.opencsv.CSVWriter;

import sibbac.business.daemons.speechminer.dao.SpeechMinerDao;
import sibbac.business.daemons.speechminer.dto.SpeechMinerResultDTO;
import sibbac.common.SIBBACBusinessException;

@Service
public class SpeechMinerBo  {

    @Value("${fichero.envio.speechminer}")//property definido en sibbac.reguladores.properties
    private String speechMinerFile;
    @Value("${fichero.envio.speechminer.charset}")//property definido en sibbac.reguladores.properties
    private String encoding;
    @Autowired
    private SpeechMinerDao speechMinerDao;
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(SpeechMinerBo.class);
    private static final String defaultFileName = "Cash_equities_"; 

    private static final String[] HEADERS= {"TPFISJUR","CDBROCLI","DESCLI","CDALIASS","DESCRALI","TPIDENTI","CIF","CDCAT","CDKGR","CDBDP","TPCLIENN"};

    private static final String fileNameRegex  = "([A-z_\\.\\-\\ 0-9]+)(\\<([A-z-\\.\\ \\-]+)\\>)?";
    private static final Character separator = ';';
    
    public void generateFile() throws SIBBACBusinessException{
      LOGGER.debug("[generateFile] Se procede a generar el fichero");

        String nombreFicheroSeparado = speechMinerFile.substring(speechMinerFile.lastIndexOf("/") + 1);
        String folder = speechMinerFile.substring(0,speechMinerFile.lastIndexOf("/") );

        String[] nombreFichero = nombreFicheroSeparado.split("\\.");
        
        final String nombreFicheroNoExtFormatted = extractNameFormatFromPattern(nombreFichero[0]);
        final String ext = nombreFichero[1];
        final String nombreFicheroTemp = "tmp_"+nombreFicheroNoExtFormatted;
        
        List<SpeechMinerResultDTO> speechMinerResultDTOList = speechMinerDao.getSpeechMinerResult();

        if(speechMinerResultDTOList!=null && speechMinerResultDTOList.size()>0) {
        	
            try {
                File tempFile = new File(folder + "/" + nombreFicheroTemp+"."+ext);
                if(tempFile.exists()) {
                  tempFile.delete();
                }
                tempFile.getParentFile().mkdirs();
                tempFile.createNewFile();
                LOGGER.debug("[generateFile] Fichero temporal creado:  {}",tempFile.getAbsolutePath());
                try (
                        Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tempFile),encoding));
                        CSVWriter csvWriter = new CSVWriter(writer,
                            separator,
                                CSVWriter.NO_QUOTE_CHARACTER,
                                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                                CSVWriter.DEFAULT_LINE_END)
                ) {
                    
                    csvWriter.writeNext(HEADERS);

                    for(SpeechMinerResultDTO obj : speechMinerResultDTOList){
                        csvWriter.writeNext(
                                new String[]{
                                obj.getTpfisjur()!=null ?    obj.getTpfisjur().toString() : "",
                                obj.getCdbrocli()!=null ?    obj.getCdbrocli().trim() : "",
                                obj.getDescli()!=null   ?    obj.getDescli().trim() : "",
                                obj.getCdaliass()!=null ?    obj.getCdaliass().trim() : "",
                                obj.getDescrali()!=null ?    obj.getDescrali().trim() : "",
                                obj.getTpidenti()!=null ?    obj.getTpidenti().toString() : "",
                                obj.getCif()!=null   ?   obj.getCif().trim() : "",
                                obj.getCdcat()!=null ?   obj.getCdcat().trim() : "",
                                obj.getCdkgr()!=null ?   obj.getCdkgr().trim() : "",
                                obj.getCdbdp()!=null ?   obj.getCdbdp().trim():"",
                                obj.getTpclienn()!=null  ?obj.getTpclienn().toString() : ""
                         });
                    }
                    csvWriter.close();

                    final File finalFile = new File(folder+"/"+nombreFicheroNoExtFormatted+"."+ext);
                    Files.copy(tempFile,finalFile);
                    if(tempFile.exists()) {
                      tempFile.delete();
                    }
                    LOGGER.info("Fichero de speechMiner generado correctamente:{} ",finalFile.getPath());
                }

            } catch (IOException e) {
              LOGGER.error(" No se ha logrado crear el fichero:{} ",e.getMessage());
              LOGGER.debug(" No se ha logrado crear el fichero:{} ",e.getCause());
              throw new SIBBACBusinessException(e.getMessage());
            }
        }else {
          LOGGER.info("No se encontraron resultados para la consultad del proceso SpeechMiner");
        }
    }

    private String extractNameFormatFromPattern(String fileName){
        Pattern p = Pattern.compile(fileNameRegex,Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher m = p.matcher(fileName);
        if(!m.matches()) {
            LOGGER.error("El formato de nombre de fichero especificado en el properties de reguladores es incorrecto, se utilizará un nombre por defecto");
            fileName =  String.format(defaultFileName);
        }else{
          if(m.groupCount()>2 && m.group(2)!=null){
                LOGGER.debug("Se incluirá la fecha en el nombre del fichero ");
                final String nameBase = m.group(1);
                try {
                  final SimpleDateFormat formatoFecha = new SimpleDateFormat(m.group(3));
                    fileName  = String.format(nameBase+"%s",  formatoFecha.format(new Date()));
                }catch(IllegalArgumentException | NullPointerException ex) {
                    LOGGER.error("El formato de fecha para el nombre del fichero especificado en el properties {} de reguladores es incorrecto, se utilizará un nombre por defecto","fichero.envio.speechminer");
                  final SimpleDateFormat formatoFecha = new SimpleDateFormat("yyy-MM-dd");
                    fileName =  String.format(nameBase+"%s",  formatoFecha.format(new Date()));             
                }
            }
        }
        return fileName;
    }
}
