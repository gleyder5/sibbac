package sibbac.business.daemons.ork.model;

import static java.util.Objects.requireNonNull;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PostLoad;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import sibbac.business.daemons.ork.common.OrkMercadosConfig;
import sibbac.common.SIBBACBusinessException;

import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;

/**
 * Entidad que representa una búsqueda de información en la base de datos de
 * SIBBAC para completar la información proveniente de Fidessa para la
 * generación del fichero ORK de MiFID II.
 * @author XI326526
 */
@Entity
@Table(name = "TMCT0_ORK_BME_SEARCH")
public class OrkBmeSearch implements Serializable {

  /**
   * Serial version
   */
  private static final long serialVersionUID = 1057742777816386188L;

  /**
   * Nombre como clave primaria de la búsqueda
   */
  @Id
  @Column(name = "NAME", length = 50)
  private String name;

  @OneToOne(optional = false)
  @JoinColumn(name = "QUERY", nullable = false)
  private OrkBmeQuery query;

  @Column(name = "KEY_FORMAT", nullable = true)
  private String keyFormat;

  /**
   * Nombres de los parámetros que identifican a esta búsqueda, que es una
   * secuencia de nombres no repetidos de los parámetros de {@link #query} y
   * recursivamente su {@link OrkBmeQuery#getFail()}
   */
  @Transient
  private String[] paramNames;

  /**
   * Bandera que indica si ya se ha ejecutado la carga de los parámetros por
   * nombre en las queries que dependen de esta búsqueda.
   */
  @Transient
  private boolean propertiesLoaded = false;

  /**
   * @param name determina el nombre de la búsqueda
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return el nombre de la búsqueda
   */
  public String getName() {
    return name;
  }

  /**
   * @return el formato para generar una llave de caché para una búsqueda
   */
  public String getKeyFormat() {
    return keyFormat;
  }

  /**
   * @param keyFormat determina el formato para generar la llave para almacenar
   * el resultado de una búsqueda en cache. Cumple con las condiciones
   * especificadas en {@link java.text.Format el formato para cadenas por
   * posición}
   */
  public void setKeyFormat(String keyFormat) {
    this.keyFormat = keyFormat;
  }

  /**
   * @return primera consulta que se intenta ejecutar para resolver esta
   * búsqueda.
   */
  public OrkBmeQuery getQuery() {
    return query;
  }

  /**
   * @param query determina la consulta que se intenta ejecutar primero para
   * resolver esta búsqueda
   * @see OrkBmeQuery
   */
  public void setQuery(OrkBmeQuery query) {
    this.query = query;
  }

  /**
   * Indica si se intenta guardar el resultado en caché.
   * @return verdadero si el campo {@link #keyFormat} no es vacío.
   */
  @Transient
  public boolean isCacheable() {
    return !keyFormat.trim().isEmpty();
  }

  /**
   * Genera una llave única para el caché para un conjunto de datos dado.
   * @param values los valores para los cuales se trata de generar la llave
   * primaria
   * @return la cadena con la llave generada.
   */
  @Transient
  public String getKey(Object[] values) {
    final StringBuilder builder;

    requireNonNull(name, "No se ha asignado valor a queryName");
    builder = new StringBuilder(name);
    return builder.append(String.format(keyFormat, values)).toString();
  }

  /**
   * @return nombres de los parámetros que identifican a esta búsqueda
   */
  @Transient
  public String[] getParamNames() {
    if (paramNames != null) {
      return paramNames.clone();
    }
    else {
      return paramNames;
    }
  }

  /**
   * Método que intenta reemplazar los parámetros etiquetados de {@link #query}
   * @param config Objeto de configuración alimentado desde
   * sibbac.mercados.properties
   * @throws SIBBACBusinessException En caso de error al leer una propiedad.
   */
  @Transient
  void loadProperties(OrkMercadosConfig config) throws SIBBACBusinessException {
    if (!propertiesLoaded && query != null) {
      query.loadProperties(config);
      propertiesLoaded = true;
    }
  }

  /**
   * Método que intenta cargar los nombres de los parámetros que se requieren
   * para identificar a la búsqueda de manera única para crear llaves del caché.
   * Se ejecuta automáticamente después de materializar el objeto.
   */
  @PostLoad
  void loadParamNames() {
    final Set<String> set;

    set = query.getParamNamesForKey();
    paramNames = new String[set.size()];
    set.toArray(paramNames);
  }

}
