package sibbac.business.daemons.ork.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sibbac.business.daemons.ork.model.OrkBmeFicheros;

@Repository
public interface OrkBmeFicherosDao extends JpaRepository<OrkBmeFicheros, Date> {

}
