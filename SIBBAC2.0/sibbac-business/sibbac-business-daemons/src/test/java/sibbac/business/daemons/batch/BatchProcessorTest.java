package sibbac.business.daemons.batch;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import java.io.*;

import org.junit.Test;
import org.junit.Before;

import sibbac.business.daemons.batch.BatchProcessor;
import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.common.TimeOutBatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.batch.model.BatchParam;
import sibbac.business.daemons.batch.model.Query;
import sibbac.business.daemons.batch.model.QueryId;
import sibbac.test.jdbc.ConnectionMock;
import sibbac.test.jdbc.DataSourceMock;
import sibbac.test.jdbc.PreparedStatementMock;

public class BatchProcessorTest {
  
  private final File workdirectory;
  
  private BatchProcessor processor;
  
  private Query query;

  private List<BatchParam> params;
  
  public BatchProcessorTest() {
    workdirectory = new File(System.getProperty("user.home"),"sibbac-test");
    workdirectory.mkdirs();
  }
  
  @Before
  public void before() throws TimeOutBatchException {
    final Batch batch;
    final QueryId id;
    
    id = new QueryId();
    id.setCode("TEST");
    id.setOrder(1);
    query = new Query();
    query.setId(id);
    params = new ArrayList<>();
    query.setParameters(params);
    query.setActive(1);
    batch = new Batch();
    batch.setMaxTime(300); //5 minutos.
    processor = new BatchProcessor();
    processor.setBatch(batch);
    processor.setWorkDirectory(workdirectory.getAbsolutePath());
    processor.startTimeCounters();
  }
  
  @Test
  public void prepareStatementTest() throws BatchException, SQLException, Exception {
    final PreparedStatementMock pstmt;
    final Connection con;
    final DataSource ds;
    final Batch batch;
    
    batch = new Batch();
    batch.setQueries(Collections.singletonList(query));
    batch.loadSteps();
    pstmt = new PreparedStatementMock() {};
    con = new ConnectionMock(){
      @Override
      public PreparedStatement prepareStatement(String sql) throws SQLException {
        return pstmt;
      }
    };
    ds = new DataSourceMock() {
    	@Override
    	public Connection getConnection() {
    		return con;
    	}
    };
    processor.setDs(ds);
    processor.setBatch(batch);
    processor.call();
    assertEquals(0, pstmt.wasExecuted());
  }
  
}
