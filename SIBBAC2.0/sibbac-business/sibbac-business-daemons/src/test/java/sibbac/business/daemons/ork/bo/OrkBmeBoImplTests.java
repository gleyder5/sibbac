package sibbac.business.daemons.ork.bo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Collections;

import org.junit.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.springframework.beans.BeansException;
import static org.junit.Assert.*;
import static org.slf4j.LoggerFactory.getLogger;

import sibbac.business.daemons.ork.common.OrkMercadosConfigImpl;
import sibbac.business.daemons.ork.common.ResultSetIterator;
import sibbac.business.daemons.ork.dao.OrkBmeFicherosDao;
import sibbac.business.daemons.ork.dao.OrkBmeFieldDao;
import sibbac.business.daemons.ork.dao.OrkFidessaDao;
import sibbac.business.daemons.ork.model.OrkBmeFicheros;
import sibbac.business.daemons.ork.model.OrkBmeField;
import sibbac.business.daemons.ork.model.OrkFidessaFixedField;
import sibbac.business.daemons.ork.model.OrkFidessaDynaField;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.SibbacConfigImpl;
import sibbac.test.ApplicationContextMock;
import sibbac.test.jdbc.ConnectionMock;
import sibbac.test.jdbc.DataSourceMock;
import sibbac.test.jdbc.PreparedStatementMock;
import sibbac.test.jdbc.ResultSetMetaDataMock;
import sibbac.test.jdbc.ResultSetMock;

public class OrkBmeBoImplTests {
	
	private static final Logger LOG = getLogger(OrkBmeBoImplTests.class);
	
	private static final String NUORDENMKT_VALUE = "TRM0001";
	
	private static final String DYNA_FIELD_NAME = "PARAM1";
	
	private static final int DYNA_FIELD_VALUE = 12300;
	
	private static OrkBmeBoImpl orkBmeBo;
	
	private OrkMercadosConfigImpl config;
	
	@BeforeClass
	public static void createBo() {
		orkBmeBo = new OrkBmeBoImpl();
	}
	
	@Before
	public void createConfig() {
		final SibbacConfigImpl sibbacConfig;
		
		sibbacConfig = new SibbacConfigImpl();
		sibbacConfig.setSibbacProperties(new Properties());
		config = new OrkMercadosConfigImpl();
		config.setSibbacConfig(sibbacConfig);
		orkBmeBo.setOrkFidessaDao(new OrkFidessaDaoMock());
	}

	@Test
	public void generaOrkTwoBuilderTest() throws SIBBACBusinessException, IOException {
		final File orkFile;
		String linea;
		
		orkBmeBo.setApplicationContext(new GenerateOrkApplicationContextMock(
		    Collections.<OrkBmeField>emptyList()));
		orkFile = orkBmeBo.generaOrk(new OrkFidessaFieldCollection(
        Collections.<OrkFidessaFixedField>emptyList(),
        Collections.<OrkFidessaDynaField>emptyList()));
		try(Reader isr = new InputStreamReader(new FileInputStream(orkFile));
				BufferedReader buffreader = new BufferedReader(isr)) {
			linea = buffreader.readLine();
			assertNotNull("Debe de leer la primer linea", linea);
			LOG.info("Línea leída: {}", linea);
			linea = buffreader.readLine();
			assertNull("Solo se debe producir una línea en esta prueba", linea);
		}
	}
	
	@Test
	public void generaOrkWithQueryTest() throws SIBBACBusinessException, IOException {
		final File orkFile;
		String linea;
		
		orkBmeBo.setApplicationContext(new GenerateOrkApplicationContextMock(
        Collections.<OrkBmeField>emptyList()));
		orkFile = orkBmeBo.generaOrk(new OrkFidessaFieldCollection(
        Collections.<OrkFidessaFixedField>emptyList(),
        Collections.<OrkFidessaDynaField>emptyList()));
		try(Reader isr = new InputStreamReader(new FileInputStream(orkFile));
				BufferedReader buffreader = new BufferedReader(isr)) {
			linea = buffreader.readLine();
			assertNotNull("Debe de leer la primer linea", linea);
			LOG.info("Línea leída: {}", linea);
			linea = buffreader.readLine();
			assertNull("Solo se debe producir una línea en esta prueba", linea);
		}
	}

	@Test
	public void getSequenceInitialTest() {
		final int sequence;
		
		orkBmeBo.setOrkBmeFicherosDao(new OrkBmeFicherosFirstOfDay());
		sequence = orkBmeBo.getSequence();
		assertEquals(1, sequence);
	}
	
	@Test
	public void getSequenceNextTest() {
		final int sequence;
		
		orkBmeBo.setOrkBmeFicherosDao(new OrkBmeFicherosSecondOfDay());
		sequence = orkBmeBo.getSequence();
		assertEquals(2, sequence);
	}

	private class OrkBmeFicherosFirstOfDay extends JpaRepositoryMock<OrkBmeFicheros, Date> implements OrkBmeFicherosDao {
		
		@Override
		public OrkBmeFicheros findOne(Date now) {
			return null;
		}
		
		@Override
		public <S extends OrkBmeFicheros> S save(S ficheros) {
			assertEquals(1, ficheros.getSecuencia());
			return ficheros;
		}
	}
	
	private class OrkBmeFicherosSecondOfDay extends JpaRepositoryMock<OrkBmeFicheros, Date> implements OrkBmeFicherosDao {
		
		@Override
		public OrkBmeFicheros findOne(Date now) {
			final OrkBmeFicheros ficheros;
			
			now = new Date(now.getTime() - 1000);
			ficheros = new OrkBmeFicheros();
			ficheros.setFecha(now);
			ficheros.setHora(now);
			ficheros.setSecuencia(1);
			return ficheros;
		}
		
		@Override
		public <S extends OrkBmeFicheros> S save(S ficheros) {
			assertEquals(2, ficheros.getSecuencia());
			assertNotEquals(ficheros.getFecha(), ficheros.getHora());
			return ficheros;
		}
		
	}
	private class OrkBmeFieldDaoMock extends JpaRepositoryMock<OrkBmeField, Integer> implements OrkBmeFieldDao {

		private final List<OrkBmeField> builders;
		
		OrkBmeFieldDaoMock(List<OrkBmeField> builders) {
			this.builders = builders;
		}	

		@Override
		public List<OrkBmeField> findAllFetchedParams() {
			return builders;
		}
		
	}
	
	private class GenerateOrkApplicationContextMock extends ApplicationContextMock {
		
		private final List<OrkBmeField> builders; 
		
		GenerateOrkApplicationContextMock(List<OrkBmeField> builders) {
			this.builders = builders;
		}
		
		@Override
		public <T> T getBean(Class<T> requiredType) throws BeansException {
			if(requiredType.getName().equals("sibbac.business.daemons.ork.bo.OrkProcessor")) {
				final OrkProcessorImpl processor;
				
				processor = new OrkProcessorImpl();
				processor.setOrkOutputBuilderDao(new OrkBmeFieldDaoMock(builders));
				processor.setDataSource(new DataSourceMock(new EchoConnection()));
				processor.setOrkConfig(config);
				processor.setApplicationContext(this);
				processor.init();
				return requiredType.cast(processor);
			}
			else if(requiredType.getName().equals("sibbac.business.daemons.ork.bo.OrkBmeBo")) {
				orkBmeBo.setOrkBmeFicherosDao(new OrkBmeFicherosFirstOfDay());
				return requiredType.cast(orkBmeBo);
			}
			fail("Clase no soportada en mock");
			return null;
		}
		
	}
	
	private class OrkFidessaDaoMock implements OrkFidessaDao {

		@Override
		public void iterateOverInsercionDate(ResultSetIterator iterator) throws SIBBACBusinessException {
			final OrkFidessaResultSet rs;
			
			rs = new OrkFidessaResultSet();
			try {
				iterator.iterate(rs);
			} catch (SQLException e) {
				fail("Se ha lanzado excepcion");
			}
		}

		@Override
		public boolean existenSinTratar() throws SIBBACBusinessException {
			return true;
		}
		
	}
	
	private class OrkFidessaResultSet extends ResultSetMock {
		
		private boolean leido = false;
		
		@Override
		public Object getObject(String fieldName) {
			if(fieldName.equals("NUORDENMKT")) {
				return NUORDENMKT_VALUE;
			}
			else if(fieldName.equals(OrkPetition.DYNA_FIELD)) {
				fail("No debería poder invocarlo");
				return DYNA_FIELD_NAME;
			}
			else if(fieldName.equals(OrkPetition.DYNA_VALUE)) {
				return new Integer(DYNA_FIELD_VALUE);
			}
			return null;
		}
		
		@Override
		public String getString(String fieldName) {
			if(fieldName.equals(OrkPetition.DYNA_FIELD)) {
				return DYNA_FIELD_NAME;
			}
			fail("Solo debería llamar a este método como String");
			return null;
		}
		
		 /**
		  * Esta implementacion permite una sola ejecución
		  */
		 @Override
		 public boolean next() {
			 return leido ? false : (leido = true);
		 }
		 
	}
	
	private class EchoConnection extends ConnectionMock {

		@Override
		public PreparedStatement prepareStatement(String sql)  {
			return new EchoPreparedStatement();
		}
		
	}
	
	private class EchoPreparedStatement extends PreparedStatementMock {
		
		private final List<Object> values = new ArrayList<>();

		@Override
		public void setInt(int parameterIndex, int x) throws SQLException {
			values.add(x);
		}

		@Override
		public void setString(int parameterIndex, String x) throws SQLException {
			values.add(x);
		}
		
		@Override
		public ResultSet executeQuery() throws SQLException {
			return new ResultSetMock() {
				
				private boolean leido = false;
				
				@Override
				public String getString(int i) {
					fail("No debe ser invocado");
					return null;
				}
				
				@Override
				public int getInt(int i) {
					fail("No debe ser invocado");
					return -1;
				}
				
				@Override
				public Object getObject(int i) {
					return values.get(i - 1);
				}
				
				@Override
				public boolean next() {
					return leido ? false : (leido = true);
				}

				@Override
				public ResultSetMetaData getMetaData()  {
					return new ResultSetMetaDataMock() {
						@Override
						public int getColumnCount() {
							return values.size();
						}
					};
				}
				
			};
		}
		
	}

}
