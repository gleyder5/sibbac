package sibbac.business.daemons.batch.model;

import static java.util.Collections.singletonList;

import org.junit.Test;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.batch.model.BatchParam;
import sibbac.business.daemons.batch.model.DirectStep;
import sibbac.business.daemons.batch.model.Query;
import sibbac.business.daemons.batch.model.QueryId;
import sibbac.business.daemons.batch.model.Step;

import org.junit.Before;
import static org.junit.Assert.*;

public class BatchTests {
	
	private Batch batch;
	
	@Before
	public void before() {
		batch = new Batch();
	}
	
	@Test
	public void loadStepsDirectTest() throws BatchException {
		final Query directQuery;
		final QueryId queryId;
		final Iterable<Step> steps;
		
		queryId = new QueryId();
		queryId.setCode("TEST");
		queryId.setOrder(1);
		directQuery = new Query();
		directQuery.setId(new QueryId());
		directQuery.setType('N');
		directQuery.setDirect(1);
		directQuery.setActive(1);
		directQuery.setParameters(singletonList(new BatchParam()));
		batch.setQueries(singletonList(directQuery));
		batch.loadSteps();
		steps = batch.getSteps();
		assertNotNull(steps);
		for(Step step : steps) {
		  assertTrue(step instanceof DirectStep);
		  return;
		}
		fail("Solo se esperaba un resultado");
	}

	@Test
	public void loadStepsInactiveQueryTest() throws BatchException {
		final Query directQuery;
		
		directQuery = new Query();
		directQuery.setType('N');
		directQuery.setDirect(1);
		directQuery.setActive(0);
		batch.setQueries(singletonList(directQuery));
		batch.loadSteps();
		assertFalse(batch.getSteps().iterator().hasNext());
	}
}
