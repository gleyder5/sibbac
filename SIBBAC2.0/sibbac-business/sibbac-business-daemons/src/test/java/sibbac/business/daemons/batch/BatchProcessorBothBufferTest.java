package sibbac.business.daemons.batch;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import sibbac.business.daemons.batch.BatchProcessor;
import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.batch.model.BatchParam;
import sibbac.business.daemons.batch.model.ParamId;
import sibbac.business.daemons.batch.model.Query;
import sibbac.business.daemons.batch.model.QueryId;
import sibbac.test.jdbc.ConnectionMock;
import sibbac.test.jdbc.DataSourceMock;
import sibbac.test.jdbc.ResultSetMock;
import sibbac.test.jdbc.ResultSetMetaDataMock;
import sibbac.test.jdbc.PreparedStatementMock;

public class BatchProcessorBothBufferTest {
  
  private static final String CODE = "BUFFER_TEST";
  
  private static final int TIME_OUT_SECONDS = 3600;
  
  private final String workdirectory;
  
  private DataSource ds;
  
  private PreparedStatementMock statement;
  
  private File directory;

  public BatchProcessorBothBufferTest() {
    directory = new File(System.getProperty("user.home"),"sibbac-test");
    directory.mkdirs();
    workdirectory = directory.getAbsolutePath();
  }
  
  @Before
  public void before() {
    statement = new PreparedStatementMock() {
      @Override
      public ResultSet executeQuery(String query) throws SQLException {
        super.executeQuery();
        return new ResultSetMock() {
          private int conteo = -1;
          @Override
          public boolean next() throws SQLException {
            return conteo++ < 1;
          }
          @Override
          public String getString(int columnIndex) throws SQLException {
            switch(conteo) {
              case 0:
                return "PRIMERO";
              case 1:
                return "SEGUNDO";
              default:
                throw new SQLException("Se ha excedido el número de resultados");
            }
          }
          @Override
          public ResultSetMetaData getMetaData() throws SQLException {
            return new ResultSetMetaDataMock(Types.VARCHAR);
          }
        };
      }
    };
    ds = new DataSourceMock() {
      @Override
      public Connection getConnection() throws SQLException {
        return new ConnectionMock() {
          @Override
          public Statement createStatement() throws SQLException {
              return statement;
          }
        };
      }
    };
  }
  
  @Test
  public void bothBufferTest() throws Exception {
    final BatchProcessor processor;
    final Batch batch;
    
    batch = batchFactory();
    processor = new BatchProcessor();
    processor.setBatch(batch);
    processor.setWorkDirectory(workdirectory);
    processor.setDs(ds);
    processor.call();
    assertEquals(3, statement.wasExecuted());
  }
  
  private Batch batchFactory() throws BatchException {
    final Batch batch;
    final List<Query> queriesList;
    final Query queryReceptor;
    final ParamId paramId;
    final BatchParam param;
    
    batch = new Batch();
    batch.setCode(CODE);
    batch.setActive(true);
    batch.setMaxTime(TIME_OUT_SECONDS);
    queriesList = new ArrayList<>();
    queriesList.add(queryFactory(1, 1, 'B', "SELECT"));
    queryReceptor = queryFactory(2, 0, 'N', "INSERT");
    paramId = new ParamId();
    paramId.setCode(CODE);
    paramId.setOrder(0);
    paramId.setParam(1);
    param = new BatchParam();
    param.setId(paramId);
    param.setType(Types.VARCHAR);
    queryReceptor.setParameters(Collections.singletonList(param));
    queriesList.add(queryReceptor);
    batch.setQueries(queriesList);
    batch.loadSteps();
    return batch;
  }
  
  private Query queryFactory(int order, int direct, char type, String sentence) {
    final Query query;
    final QueryId queryId;
    
    queryId = new QueryId();
    queryId.setCode(CODE);
    queryId.setOrder(order);
    query = new Query();
    query.setId(queryId);
    query.setActive(1);
    query.setDirect(direct);
    query.setType(type);
    query.setQuery(sentence);
    query.setParameters(Collections.<BatchParam>emptyList());
    return query;
  }

}
