package sibbac.business.daemons.ork.bo;

import java.util.List;
import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import static org.junit.Assert.fail;

abstract class JpaRepositoryMock<T, ID extends Serializable> implements JpaRepository<T, ID> {

	@Override
	public Page<T> findAll(Pageable pageable) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <S extends T> S save(S entity) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public T findOne(ID id) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public boolean exists(ID id) {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public long count() {
		fail("Método no implementado en Mock");
		return 0;
	}

	@Override
	public void delete(ID id) {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public void delete(T entity) {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public void delete(Iterable<? extends T> entities) {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public void deleteAll() {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public List<T> findAll() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public List<T> findAll(Sort sort) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public List<T> findAll(Iterable<ID> ids) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <S extends T> List<S> save(Iterable<S> entities) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public void flush() {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public <S extends T> S saveAndFlush(S entity) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<T> entities) {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public void deleteAllInBatch() {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public T getOne(ID id) {
		fail("Método no implementado en Mock");
		return null;
	}

}
