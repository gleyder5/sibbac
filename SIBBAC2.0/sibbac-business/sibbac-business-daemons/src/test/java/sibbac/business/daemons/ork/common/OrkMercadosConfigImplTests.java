package sibbac.business.daemons.ork.common;

import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.SibbacConfigImpl;

public class OrkMercadosConfigImplTests {
	
	private OrkMercadosConfigImpl orkConfig;
	
	@Before
	public void createInstance() {
		orkConfig = new OrkMercadosConfigImpl();
	}
	
	@Test
	public void getLecturaTimeoutDefaultTest() {
		final SibbacConfigImpl sibbacConfig;
		
		sibbacConfig = new SibbacConfigImpl();
		sibbacConfig.setSibbacProperties(new Properties());
		orkConfig.setSibbacConfig(sibbacConfig);
		assertEquals(OrkMercadosConfigImpl.READ_TIMEOUT_DEFAULT, orkConfig.getLecturaTimeout());
	}
	
	@Test
	public void getLecturaTimeoutTest() {
		final SibbacConfigImpl sibbacConfig;
		final Properties properties;
		
		properties = new Properties();
		properties.setProperty(OrkMercadosConfigImpl.TIMEOUT_IN, "32");
		sibbacConfig = new SibbacConfigImpl() {
			@Override public Properties loadPropertyFile(String fileName) {
				return properties;
			}
		};
		sibbacConfig.setSibbacProperties(new Properties());
		orkConfig.setSibbacConfig(sibbacConfig);
		assertEquals(32, orkConfig.getLecturaTimeout());
	}
	
	@Test
	public void nameWithDateAndHourTest() {
		final String result;
		final Calendar cal;
		
		cal = new GregorianCalendar(2017, Calendar.JUNE, 7, 10, 34);
		result = orkConfig.nameWithDateAndHour("file_name.csv", cal.getTime());
		assertNotNull(result);
		assertEquals("file_name_20170607_1034.csv", result);
	}
	
	@Test
	public void nameWithDateAndSequenceTest() throws SIBBACBusinessException {
		final String filename, result;
		final Calendar cal;
		
		filename = new StringBuilder("prefijo_")
				.append(OrkMercadosConfigImpl.BME_FILENAME_INFIX)
				.append(".ext").toString();
		cal = new GregorianCalendar(2017, Calendar.JUNE, 7);
		result = orkConfig.nameWithDateAndSequence(filename, cal.getTime(), 10);
		assertNotNull(result);
		assertEquals("TEMP_prefijo_20170607_010.ext", result);
	}
	
	@Test(expected = SIBBACBusinessException.class)
	public void nameWithDateAndSequenceWithoutPatternFailTest() throws SIBBACBusinessException {
		orkConfig.nameWithDateAndSequence("withoutPattern.ext", new Date(), 1);
	}
	
}
