package sibbac.business.daemons.batch.model;

import java.sql.SQLException;
import static java.sql.Types.VARCHAR;
import static java.sql.Types.INTEGER;
import static java.sql.Types.DATE;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import sibbac.business.daemons.batch.model.BatchParam;

public class ParamTests {
	
	private BatchParam param;
	
	@Before 
	public void before() {
	  final ParamId id;
	  
	  id = new ParamId();
	  id.setCode("TEST");
	  id.setOrder(1);
	  id.setParam(1);
		param = new BatchParam();
		param.setId(id);
	}
	
	@Test
	public void fillParameterWithStringTest() throws SQLException {
	  final StringBuilder sqlBuilder;
	  
	  sqlBuilder = new StringBuilder("A = ?");
	  param.setValue("1234");
	  param.setType(VARCHAR);
		param.fillParameterWithString(sqlBuilder, null);
		assertEquals("A = '1234'", sqlBuilder.toString());
	}

  @Test
  public void fillParameterWithStringNoDefaultTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("A = ?");
    param.setType(VARCHAR);
    param.fillParameterWithString(sqlBuilder, null);
    assertEquals("A = NULL", sqlBuilder.toString());
  }
  
  
  @Test
  public void fillParameterWithStringIntegerTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("A = ?");
    param.setValue("1234");
    param.setType(INTEGER);
    param.fillParameterWithString(sqlBuilder, null);
    assertEquals("A = 1234", sqlBuilder.toString());
  }

  @Test
  public void fillParameterWithStringIntegerNoDefaultTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("A = ?");
    param.setType(INTEGER);
    param.fillParameterWithString(sqlBuilder, null);
    assertEquals("A = NULL", sqlBuilder.toString());
  }
  
  @Test
  public void fillParameterWithStringIntegerBlankTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("A = ?");
    param.setValue("1234");
    param.setType(INTEGER);
    param.fillParameterWithString(sqlBuilder, "");
    assertEquals("A = NULL", sqlBuilder.toString());
  }

  @Test
  public void fillParameterWithStringIntegerBlanckNoDefaultTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("A = ?");
    param.setType(INTEGER);
    param.fillParameterWithString(sqlBuilder, "");
    assertEquals("A = NULL", sqlBuilder.toString());
  }

  @Test
  public void fillParameterWithStringDateTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("D = ?");
    param.setValue("2017-12-10");
    param.setType(DATE);
    param.fillParameterWithString(sqlBuilder, null);
    assertEquals("D = '2017-12-10'", sqlBuilder.toString());
  }

  @Test
  public void fillParameterWithStringDateNoDefaultTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("D = ?");
    param.setType(DATE);
    param.fillParameterWithString(sqlBuilder, null);
    assertEquals("D = NULL", sqlBuilder.toString());
  }
  
  @Test
  public void fillParameterWithStringDateBlankTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("D = ?");
    param.setValue("2018-12-10");
    param.setType(DATE);
    param.fillParameterWithString(sqlBuilder, "");
    assertEquals("D = NULL", sqlBuilder.toString());
  }

  @Test
  public void fillParameterWithStringDateBlanckNoDefaultTest() throws SQLException {
    final StringBuilder sqlBuilder;
    
    sqlBuilder = new StringBuilder("D = ?");
    param.setType(DATE);
    param.fillParameterWithString(sqlBuilder, "");
    assertEquals("D = NULL", sqlBuilder.toString());
  }
  
}
