package sibbac.business.daemons.batch.model;

import java.util.List;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;

import static java.sql.Types.VARCHAR;
import static java.sql.Types.TIMESTAMP;

import org.junit.Test;
import static org.junit.Assert.*;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.common.TimeOutBatchException;
import sibbac.business.daemons.batch.model.BatchParam;
import sibbac.business.daemons.batch.model.SameLineStep;
import sibbac.business.daemons.batch.model.ParamId;
import sibbac.business.daemons.batch.model.Query;
import sibbac.business.daemons.batch.model.QueryId;

public class ByLineStepTests {
	
	private static final String MIFFID_CODE = "MIF";
	
	private static final String LINE = 
	    "\"010000020\";\"XMCE\";\"2018-01-03\";\"2018-01-31\";\"XMAD\";\"B\";\"L\";\"ORD\";\"4716\";;\"0\";"
	    + ";\"3\";\"0\";\"AOTC\";\"0\";\"100004716\";\"RDMA\";;;\"MADRID\";\"L\";\"SAN.MA\";\"00058517769DOMA1\";"
	    + "\"00054257624ORMA0\";\n";
	
	private static final String[] CAMPOS_DINAMICOS =  new String[]{"MIC","SENTIDO","IDENCL"};
	
	private SameLineStep step;
  
  @Test
	public void executeMIFIDTest() throws BatchException {
    final List<Query> queries;
    final MockEnvironment environment;
  	Query query;
  	List<BatchParam> params;
  	
  	queries = new ArrayList<>();
  	query = queryFactory(MIFFID_CODE, 1, 0);
  	query.setQuery("INSERT");
  	params = parameterFactory(query, 0, 1, 2, 3, 4);
  	params.get(0).setType(VARCHAR);
  	params.get(1).setType(VARCHAR);
  	params.get(2).setType(VARCHAR);
  	params.get(3).setType(TIMESTAMP);
  	params.get(4).setType(TIMESTAMP);
  	queries.add(query);
  	for(int i = 1; i <= 3; i++) {
    	query = queryFactory(MIFFID_CODE, i , 2);
    	params = parameterFactory(query, 1, 21, i + 5);
    	params.get(0).setType(VARCHAR);
    	params.get(1).setType(VARCHAR);
    	params.get(1).setValue(CAMPOS_DINAMICOS[i - 1]);
    	params.get(2).setType(VARCHAR);
    	queries.add(query);
  	}
  	step = new SameLineStep(queries);
  	step.execute((environment = new MockEnvironment()));
  	assertEquals(4, environment.ejecuciones);
	}

  private Query queryFactory(String code, int order, int direct) {
  	final QueryId id;
  	final Query query;
  	
  	id = new QueryId();
  	id.setCode(code);
  	id.setOrder(order);
  	query = new Query();
  	query.setId(id);
  	query.setDirect(direct);
  	return query;
  }
  
  private List<BatchParam> parameterFactory(Query query, int ...paramPos) {
  	final QueryId id;
  	final int len;
  	final List<BatchParam> parameters;
  	BatchParam param;
  	ParamId paramId;
  	
  	id = query.getId();
  	parameters = new ArrayList<>();
  	len = paramPos.length;
  	for(int i = 0; i < len; i++) {
  		paramId = new ParamId();
  		paramId.setCode(id.getCode());
  		paramId.setOrder(id.getOrder());
  		paramId.setParam(paramPos[i]);
    	param = new BatchParam();
    	param.setId(paramId);
    	param.setValue(null);
    	parameters.add(param);
  	}
  	query.setParameters(parameters);
  	return parameters;
  }
  
  private class MockEnvironment extends BatchEnvironment {
    
    int ejecuciones = 0;

    @Override
    protected void executeStatement(String query, char type) throws BatchException {
      fail("No implementado en mock");
    }

    @Override
    protected Reader readerFromOrigin() throws BatchException, IOException {
      return new StringReader(LINE);
    }

    @Override
    protected boolean confirmAlive() throws TimeOutBatchException {
      return true;
    }

    @Override
    protected void batchExecute(Iterable<String> queries) throws BatchException {
      final Iterator<?> it;
      
      it = queries.iterator();
      while(it.hasNext()) {
        it.next();
        ejecuciones++;
      }
    }

    @Override
    protected int getThreadsForBuffer() {
      return 1;
    }
    
  }

}
