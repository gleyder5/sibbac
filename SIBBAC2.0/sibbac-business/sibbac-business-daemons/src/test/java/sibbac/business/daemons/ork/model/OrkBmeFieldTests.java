package sibbac.business.daemons.ork.model;

import java.util.List;
import java.util.ArrayList;
import static java.sql.Types.VARCHAR;
import static java.sql.Types.BIGINT;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class OrkBmeFieldTests {
	
	private static final Object[] NUORDEN = {"TRM00001"};
	
	private static final Object[] NUORDEN_OPARAM = {"TRM00001", 12300};

	private OrkBmeSearch outputBuilder;
	
	@Before
	public void createInstance() {
		outputBuilder = new OrkBmeSearch();
	}
	
	@Test
	public void getKeyOneParamTest() {
		final String generatedKey;
		
		outputBuilder.setName("BY_ORDEN");
		outputBuilder.setKeyFormat("/%s");
		generatedKey = outputBuilder.getKey(NUORDEN);
		assertEquals("BY_ORDEN/TRM00001", generatedKey);
	}
	
	@Test
	public void getKeyTwoParamsTest() {
		final String generatedKey;
		
		outputBuilder.setName("BY_ORDEN");
		outputBuilder.setKeyFormat("/%s/%d");
		generatedKey = outputBuilder.getKey(NUORDEN_OPARAM);
		assertEquals("BY_ORDEN/TRM00001/12300", generatedKey);
	}
	
	@Test
	public void getParamNamesTest() {
		final List<OrkBmeQueryParam> paramList;
		final Object[] paramNames;
		final OrkBmeQuery query;
		OrkBmeQueryParam param;
		OrkFidessaField fidessaField;

		paramList = new ArrayList<>();
		param = new OrkBmeQueryParam();
		param.setType(VARCHAR);
		fidessaField = new OrkFidessaFixedField();
		fidessaField.setName("NUORDEN");
		param.setOrkFidessaField(fidessaField);
		paramList.add(param);
		param = new OrkBmeQueryParam();
		param.setType(BIGINT);
		fidessaField = new OrkFidessaDynaField();
		fidessaField.setName("NPARAM");
		param.setOrkFidessaField(fidessaField);
		paramList.add(param);
		query = new OrkBmeQuery();
		query.setParams(paramList);
		paramNames = query.getParamNamesForKey().toArray();
		assertArrayEquals(new String[]{"NUORDEN", "NPARAM"}, paramNames);
	}
	
}
