package sibbac.business.daemons.ork.bo;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import static java.sql.Types.VARCHAR;

import org.junit.Test;
import static org.junit.Assert.*;

import sibbac.business.daemons.batch.common.BatchException;
import sibbac.business.daemons.batch.model.Batch;
import sibbac.business.daemons.batch.model.Step;
import sibbac.business.daemons.ork.model.OrkFidessaFixedField;
import sibbac.business.daemons.ork.model.OrkFidessaDynaField;
import sibbac.common.SIBBACBusinessException;

public class OrkFidessaFieldCollectionTests {
	
	private OrkFidessaFieldCollection fieldCollection;
	
	@Test
	public void loadOrkBatchTest() throws SIBBACBusinessException {
		final Batch batch;

		fieldCollection = new OrkFidessaFieldCollection(getFixedFields("NUMERKT"), getDynaFields(1, "PARAM1", "PARAM2"));
		batch = fieldCollection.loadOrkBatch();
		assertNotNull(batch);
		assertEquals(4, batch.getQueries().size());
		for(Step step : batch.getSteps()) {
	    assertNotNull(step);
	    return;
		}
		fail("Se esperaba solo un resultado");
	}
	
	@Test(expected = BatchException.class)
	public void loadOrkBatchEmtpyDynafieldTest() throws SIBBACBusinessException {
    fieldCollection = new OrkFidessaFieldCollection(
        Collections.<OrkFidessaFixedField>emptyList(), 
        Collections.<OrkFidessaDynaField>emptyList());
    fieldCollection.loadOrkBatch();
		fail("Debía soltar una exception BatchException al estar vacía la colecciona para notificar el fallo");
	}
	
	private List<OrkFidessaFixedField> getFixedFields(String... names) {
	  final List<OrkFidessaFixedField> list;
	  OrkFidessaFixedField field;
	  int i = 0;
	  
	  list = new ArrayList<>();
	  for(String name : names) {
	    field = new OrkFidessaFixedField();
	    field.setName(name);
	    field.setPosition(++i);
	    field.setKey('S');
	    field.setType(VARCHAR);
	    list.add(field);
	  }
	  return list;
	}
	
  private List<OrkFidessaDynaField> getDynaFields(final int inicial, String... names) {
    final List<OrkFidessaDynaField> list;
    OrkFidessaDynaField field;
    int i = inicial;
    
    list = new ArrayList<>();
    for(String name : names) {
      field = new OrkFidessaDynaField();
      field.setName(name);
      field.setPosition(++i);
      field.setType(VARCHAR);
      list.add(field);
    }
    return list;
  }

}
