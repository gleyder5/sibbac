package sibbac.business.mclagan.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.mclagan.bo.callable.CallableMcLaganProcessBo;
import sibbac.business.mclagan.bo.dto.ProcessResultDto;
import sibbac.business.mclagan.web.util.Constantes;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.enums.SIBBACJobType;

/**
 * The Class TaskEnvioMcLagan.
 */
@Component
@DisallowConcurrentExecution
@SIBBACJob(group = Task.GROUP_DAEMONS.NAME, name = Task.GROUP_MCLAGAN.JOB_ENVIO_INFORME_MCLAGAN, jobType = SIBBACJobType.CRON_JOB, cronExpression = "0 0 0 1 JAN ? 2099")
public class TaskGeneracionMCLAGAN extends WrapperTaskConcurrencyPrevent {

  /**
   * Referencia para la inserción de logs.
   */
  private static final Logger LOG = LoggerFactory.getLogger(TaskGeneracionMCLAGAN.class);

  @Autowired
  private CallableMcLaganProcessBo callableMcLaganGeneralProcess;

  @Value("${mclagan.gui.timeout:4}")
  private int timeout;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The trimestr. */
  private Integer trimestre;

  private String userSession;

  /** Resultado del proceso de generación de los informes */
  private ProcessResultDto processResultDto = new ProcessResultDto();

  private final Object lock = new Object();

  private Boolean isWaitting;

  /**
   * Se inicia la tarea.
   *
   * @throws Exception the exception
   */
  @Override
  public void executeTask() throws Exception {
    this.processResultDto = new ProcessResultDto();

    if (this.fechaDesde != null && this.fechaHasta != null && this.trimestre != null) {

      this.launchMacLaganProcess();

      if (!this.processResultDto.getResult()) {
        throw new SIBBACBusinessException(this.processResultDto.getResultMessage().get(0));
      }
    }
    else {
      this.processResultDto.setResult(false);
      this.processResultDto.getResultMessage().add(Constantes.ApplicationMessages.DATE_REQUIRED.value());

      if (LOG.isWarnEnabled()) {
        LOG.warn(Constantes.ApplicationMessages.DATE_REQUIRED.value());
      }
    }
  }

  /**
   * Ejecuta la tarea McLagan.
   *
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  public void launchMacLaganProcess() {
    isWaitting = false;

    callableMcLaganGeneralProcess.setTaskParams(fechaDesde, fechaHasta, trimestre);
    callableMcLaganGeneralProcess.setParent(this);

    // Iniciamos el proceso de lanzamiento de Hilos
    final List<Future<ProcessResultDto>> futureList = new ArrayList<>();

    try {
      final ExecutorService executor = Executors.newFixedThreadPool(1);

      futureList.add(executor.submit(callableMcLaganGeneralProcess));

      executor.shutdown();

      waitProcess(executor);

      Thread.sleep(1000); // Esperamos un segundo a que las tareas que han
                          // notificado el notify acaba

      // Pasado ese tiempo comprobamos si el proceso General ha terminado.
      if (executor.isTerminated()) {
        LOG.debug("El proceso de generación de informes de MacLagan ha terminado enviamos los resultados al usuario en espera");
        this.processResultDto.setResult(true);
        for (Future<ProcessResultDto> future : futureList) {
          processFuture(future);
        }
        this.processResultDto.getResultMessage().add("La ejecución ha terminado correctamente");
      }
      else {
        LOG.debug("El proceso de generación de informes de MacLagan seguirá en ejecución");
        callableMcLaganGeneralProcess.sendMailOnFinishToUser(true, userSession);
        this.processResultDto.setResult(true);
        this.processResultDto.getResultMessage().add("Su proceso esta durando más de lo esperado, se le enviará un correo cuando termine.");
      }
    }
    catch (InterruptedException exception) {
      this.processResultDto.setResult(false);
      this.processResultDto.getResultMessage().add("Se ha producido un error inesperado cuando se intentaba iniciar el proceso.");
    }
  }
  
  private void processFuture(Future<ProcessResultDto> future) {
    try {
      this.processResultDto.getResultMessage().addAll(future.get().getResultMessage());
    } catch(InterruptedException | ExecutionException exception) {
      LOG.error("Se ha producido un error en el Hilo principal de McLagan", exception);
      this.processResultDto.setResult(false);
      this.processResultDto.getResultMessage().add("Se ha producido un error inesperado durante la ejecución del hilo principal.");
    }
  }

  public void setTaskParams(Date fechaDesde, Date fechaHasta, Integer trimestre) {
    this.fechaDesde = fechaDesde;
    this.fechaHasta = fechaHasta;
    this.trimestre = trimestre;
  }

  public String getUserSession() {
    return userSession;
  }

  public void setUserSession(String userSession) {
    this.userSession = userSession;
  }

  public ProcessResultDto getProcessResultDto() {
    return processResultDto;
  }

  public void setProcessResultDto(ProcessResultDto processResultDto) {
    this.processResultDto = processResultDto;
  }

  public Boolean getIsWaitting() {
    return isWaitting;
  }

  public void setIsWaitting(Boolean isWaitting) {
    this.isWaitting = isWaitting;
  }

  /**
   * Wait del proceso, evitanto el spurious
   */
  public void waitProcess(ExecutorService executor) {
    if (!executor.isTerminated()) {
      long timeoutmillis = (long) timeout * 60L * 1000L;

      synchronized (lock) {
        try {
          isWaitting = true;

          LOG.debug("Hacemos un wait de {} milisegundos", timeoutmillis);

          // Algoritmo para evitar un spurious wakeup
          long initTimeOut = System.currentTimeMillis();
          while (isWaitting) {
            // Evitamos un spurious wakeup
            lock.wait(timeoutmillis);
            // Si nos llega un spurious wakeup debemos seguir esperando pero
            // como
            // ya ha esperado
            // debemos seguir esperando el tiempo restante hasta completar el
            // tiempo definido en
            // timeout
            long restTimeOut = System.currentTimeMillis() - initTimeOut;
            if (isWaitting && restTimeOut < timeoutmillis) {
              timeoutmillis = timeoutmillis - restTimeOut;
              if (LOG.isDebugEnabled()) {
                LOG.debug("Hemos recibido un Spurious Wakeup al proceso hemos de seguir esperando {}", timeoutmillis);
              }
            }
            else if ((System.currentTimeMillis() - initTimeOut) >= timeoutmillis) {
              if (LOG.isDebugEnabled()) {
                LOG.debug("Se ha agotado el tiempo de espera de para el usuario {}", timeoutmillis);
              }
              isWaitting = false;
            }
            else {
              if (LOG.isDebugEnabled()) {
                LOG.debug("El proceso ha terminado antes que se agotaase el tiempo de espera de {}", timeoutmillis);
              }
              isWaitting = false;
            }
          }
        }
        catch (InterruptedException e) {
          LOG.error("Error el hilo no puede esperar", e);
        }
      }
    }
  }

  /**
   * Wakeup del proces
   */
  public void wakeUpProcess() {
    synchronized (lock) {
      isWaitting = false;
      lock.notifyAll();
      LOG.debug("Se ha notificado el wakeup al proceso");
    }
  }

  public boolean isRunningInBackGround() {
    return callableMcLaganGeneralProcess.isRunning();
  }

  /**
   * Determinamos el tipo de tarea que vamos a ejecutar (MCLAGAN).
   *
   * @return the tipo
   */
  @Override
  public TIPO determinarTipoApunte() {
    return TIPO_TAREA.TASK_MCLAGAN;
  }

}