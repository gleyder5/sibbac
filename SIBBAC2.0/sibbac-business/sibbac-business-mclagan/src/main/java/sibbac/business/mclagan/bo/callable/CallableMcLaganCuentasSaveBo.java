package sibbac.business.mclagan.bo.callable;

import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.McLaganCuentasBo;
import sibbac.business.mclagan.db.model.McLaganCuentasEntity;

/**
 * The Class MclaganBo.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableMcLaganCuentasSaveBo implements Callable<Integer> {

  @Autowired
  private McLaganCuentasBo mcLaganCuentasBo;

  private List<McLaganCuentasEntity> list;

  @Override
  public Integer call() throws Exception {
    return mcLaganCuentasBo.saveBulkCuentas(list);
  }

  public List<McLaganCuentasEntity> getList() {
    return list;
  }

  public void setList(List<McLaganCuentasEntity> list) {
    this.list = list;
  }
}
