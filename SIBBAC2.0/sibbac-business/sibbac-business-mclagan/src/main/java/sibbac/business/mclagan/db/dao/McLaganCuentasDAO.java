package sibbac.business.mclagan.db.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mclagan.db.model.McLaganCuentasEntity;
import sibbac.database.DBConstants;

/**
 * The Interface McLaganCuentasDAO.
 */
@Repository
public interface McLaganCuentasDAO extends JpaRepository<McLaganCuentasEntity, String> {

  static final String PAGEABLE = " LIMIT ?1, ?2";
  
  /**
   * Elimina el contenido de la tabla de cuentas
   */
  public static final String DELETE_TABLE_MAC_LAGAN_CUENTAS = 
      "TRUNCATE TABLE " 
          + DBConstants.MCLAGAN.TMCT0_MCLAGAN_CUENTAS 
          + " IMMEDIATE";

  /**
   * Query para cuentas
   */
  public static final String MCLAGAN_CUENTAS_QUERY = 
      "SELECT "
      + "   A.CDCLIENT AS ACCOUNT_ID,"
      + "   TRIM(B.DESCLI) AS ACCOUNT_NAME,"
      + "   TRIM(COALESCE(C.NBPAIS,'SIN DESCRIPCION')) AS ACCOUNT_COUNTRY,"
      + "   CAST(B.NUCLIENT AS INTEGER) AS PARENT_ID,"
      + "   TRIM(B.DESCLI) AS PARENT_NAME"      
      + " FROM"
      + "   TMCT0_MCLAGAN A"
      + "   LEFT OUTER JOIN TMCT0CLI B ON A.NUCLIENT = B.NUCLIENT AND B.CDESTADO ='A'"
      + "   LEFT OUTER JOIN TMCT0PAISES C ON A.CDDEPAIS=C.CDHACIENDA AND C.CDESTADO ='A'";
  
  /**
   * Query para accounts
   */
  public static final String ACCOUNTS_QUERY = 
      "SELECT "
      + "   RPAD(ACCOUNT_ID, 10, ' '),"
      + "   RPAD(ACCOUNT_NAME, 60, ' '),"
      + "   RPAD(ACCOUNT_COUNTRY, 30, ' '),"
      + "   RPAD(PARENT_ID, 10, ' '),"
      + "   RPAD(PARENT_NAME, 60, ' ') "
      + " FROM"
      + "   TMCT0_MCLAGAN_CUENTAS "
      + " GROUP BY "
      + "   ACCOUNT_ID, "
      + "   ACCOUNT_NAME,"
      + "   ACCOUNT_COUNTRY, "
      + "   PARENT_ID, "
      + "   PARENT_NAME";

  public static final String MCLAGAN_CUENTAS_QUERY_COUNT = "SELECT COUNT(*) FROM (" + MCLAGAN_CUENTAS_QUERY + ")";
  
  public static final String MCLAGAN_CUENTAS_QUERY_PAGE = 
      "SELECT "
      + "   ACCOUNT_ID,"
      + "   ACCOUNT_NAME,"
      + "   ACCOUNT_COUNTRY,"
      + "   PARENT_ID,"
      + "   PARENT_NAME,"
      + "   (NEXT VALUE FOR TMCT0_MCLAGAN_CUENTAS_SEQ) AS ID"
      + " FROM ( SELECT Q2.*, ROWNUMBER() OVER(ORDER BY ORDER OF Q2) AS ROWNUMBER_ FROM ( SELECT * FROM (" 
                + MCLAGAN_CUENTAS_QUERY 
      + ") FETCH FIRST :pagesize * :page + :pagesize ROWS ONLY ) AS Q2 ) AS Q1 WHERE rownumber_ > :pagesize * :page ORDER BY rownumber_";

  public static final String MCLAGAN_CUENTAS_BULK_INSERT = "INSERT INTO TMCT0_MCLAGAN_CUENTAS "
      + "(ACCOUNT_ID, ACCOUNT_NAME, ACCOUNT_COUNTRY, PARENT_ID, PARENT_NAME ) (" + MCLAGAN_CUENTAS_QUERY + ")";
 
  public static final String ACCOUNTS_PAGE_QUERY = ACCOUNTS_QUERY + PAGEABLE;

  public static final String ACCOUNTS_COUNT_QUERY = "SELECT COUNT(*) FROM (" + ACCOUNTS_QUERY + ")";
 
  @Query(value = MCLAGAN_CUENTAS_QUERY, nativeQuery = true)
  public List<Object[]> getMcLaganCuentas();
  
  @Query(value = MCLAGAN_CUENTAS_QUERY_COUNT, nativeQuery = true)
  public Integer countMcLaganCuentas();

  @Query(value = MCLAGAN_CUENTAS_QUERY_PAGE, nativeQuery = true)
  List<Object[]> getCuentasByPage(@Param("page") Integer page,
      @Param("pagesize") Integer pagesize);
  
  @Transactional
  @Modifying
  @Query(value = MCLAGAN_CUENTAS_BULK_INSERT, nativeQuery = true)
  public void bulkInsertMcLaganCuentas();

  @Query(value = ACCOUNTS_QUERY, nativeQuery = true)
  List<McLaganCuentasEntity> findAllGroupByAccountIdAccountNameAccountCountryParentIdParentName();

  @Query(value = ACCOUNTS_PAGE_QUERY, nativeQuery = true)
  List<Object[]> findAllAccounts(Integer offset, Integer pageSize);

  @Query(value = ACCOUNTS_COUNT_QUERY, nativeQuery = true)
  Integer countAllAccounts();

  @Transactional
  @Modifying
  @Query(value = DELETE_TABLE_MAC_LAGAN_CUENTAS, nativeQuery = true)
  void cleanTable();

}