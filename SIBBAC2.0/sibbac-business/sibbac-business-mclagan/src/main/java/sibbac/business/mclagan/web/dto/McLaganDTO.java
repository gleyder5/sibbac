package sibbac.business.mclagan.web.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

import javax.validation.constraints.NotNull;

import sibbac.business.mclagan.web.dto.common.AbstractDTO;

/**
 * The Class McLaganCuentasDTO.
 */
public class McLaganDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2787896966143413158L;

  /** The id. */
  @NotNull
  private BigInteger id;

  /** The nuoprout. */
  @NotNull
  private String nuoprout;

  /** The nupareje. */
  @NotNull
  private Integer nupareje;

  /** The cdclient. */
  @NotNull
  private String cdclient;

  /** The cdproduc. */
  private String cdproduc;

  /** The nuctapro. */
  private String nuctapro;

  /** The nuclient. */
  private Integer nuclient;

  /** The cdisinvv. */
  private String cdisinvv;

  /** The cdbolsas. */
  private String cdbolsas;

  /** The cddivisa. */
  private String cddivisa;

  /** The comision. */
  private BigDecimal comision;

  /** The fhejecuc. */
  private Date fhejecuc;

  /** The imcambio. */
  private BigDecimal imcambio;

  /** The nucaneje. */
  private BigDecimal nucaneje;

  /** The tpoperac. */
  private String tpoperac;

  /** The nunivel 1. */
  private String nunivel1;

  /** The cddepais. */
  private String cddepais;

  /** The trimestre. */
  private String trimestre;

  /**
   * Instantiates a new mc lagan DTO.
   */
  public McLaganDTO() {
    super();
  }

  /**
   * Instantiates a new mc lagan DTO.
   *
   * @param id the id
   * @param nuoprout the nuoprout
   * @param nupareje the nupareje
   * @param cdclient the cdclient
   * @param cdproduc the cdproduc
   * @param nuctapro the nuctapro
   * @param nuclient the nuclient
   * @param cdisinvv the cdisinvv
   * @param cdbolsas the cdbolsas
   * @param cddivisa the cddivisa
   * @param comision the comision
   * @param fhejecuc the fhejecuc
   * @param imcambio the imcambio
   * @param nucaneje the nucaneje
   * @param tpoperac the tpoperac
   * @param nunivel1 the nunivel 1
   * @param cddepais the cddepais
   * @param trimestre the trimestre
   */
  public McLaganDTO(BigInteger id, String nuoprout, Integer nupareje, String cdclient, String cdproduc, String nuctapro,
      Integer nuclient, String cdisinvv, String cdbolsas, String cddivisa, BigDecimal comision, Date fhejecuc,
      BigDecimal imcambio, BigDecimal nucaneje, String tpoperac, String nunivel1, String cddepais, String trimestre) {
    super();
    this.id = id;
    this.nuoprout = nuoprout;
    this.nupareje = nupareje;
    this.cdclient = cdclient;
    this.cdproduc = cdproduc;
    this.nuctapro = nuctapro;
    this.nuclient = nuclient;
    this.cdisinvv = cdisinvv;
    this.cdbolsas = cdbolsas;
    this.cddivisa = cddivisa;
    this.comision = comision;
    this.fhejecuc = fhejecuc;
    this.imcambio = imcambio;
    this.nucaneje = nucaneje;
    this.tpoperac = tpoperac;
    this.nunivel1 = nunivel1;
    this.cddepais = cddepais;
    this.trimestre = trimestre;
  }

  /**
   * Gets the id.
   *
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * Gets the nuoprout.
   *
   * @return the nuoprout
   */
  public String getNuoprout() {
    return nuoprout;
  }

  /**
   * Sets the nuoprout.
   *
   * @param nuoprout the new nuoprout
   */
  public void setNuoprout(String nuoprout) {
    this.nuoprout = nuoprout;
  }

  /**
   * Gets the nupareje.
   *
   * @return the nupareje
   */
  public Integer getNupareje() {
    return nupareje;
  }

  /**
   * Sets the nupareje.
   *
   * @param nupareje the new nupareje
   */
  public void setNupareje(Integer nupareje) {
    this.nupareje = nupareje;
  }

  /**
   * Gets the cdclient.
   *
   * @return the cdclient
   */
  public String getCdclient() {
    return cdclient;
  }

  /**
   * Sets the cdclient.
   *
   * @param cdclient the new cdclient
   */
  public void setCdclient(String cdclient) {
    this.cdclient = cdclient;
  }

  /**
   * Gets the cdproduc.
   *
   * @return the cdproduc
   */
  public String getCdproduc() {
    return cdproduc;
  }

  /**
   * Sets the cdproduc.
   *
   * @param cdproduc the new cdproduc
   */
  public void setCdproduc(String cdproduc) {
    this.cdproduc = cdproduc;
  }

  /**
   * Gets the nuctapro.
   *
   * @return the nuctapro
   */
  public String getNuctapro() {
    return nuctapro;
  }

  /**
   * Sets the nuctapro.
   *
   * @param nuctapro the new nuctapro
   */
  public void setNuctapro(String nuctapro) {
    this.nuctapro = nuctapro;
  }

  /**
   * Gets the nuclient.
   *
   * @return the nuclient
   */
  public Integer getNuclient() {
    return nuclient;
  }

  /**
   * Sets the nuclient.
   *
   * @param nuclient the new nuclient
   */
  public void setNuclient(Integer nuclient) {
    this.nuclient = nuclient;
  }

  /**
   * Gets the cdisinvv.
   *
   * @return the cdisinvv
   */
  public String getCdisinvv() {
    return cdisinvv;
  }

  /**
   * Sets the cdisinvv.
   *
   * @param cdisinvv the new cdisinvv
   */
  public void setCdisinvv(String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  /**
   * Gets the cdbolsas.
   *
   * @return the cdbolsas
   */
  public String getCdbolsas() {
    return cdbolsas;
  }

  /**
   * Sets the cdbolsas.
   *
   * @param cdbolsas the new cdbolsas
   */
  public void setCdbolsas(String cdbolsas) {
    this.cdbolsas = cdbolsas;
  }

  /**
   * Gets the cddivisa.
   *
   * @return the cddivisa
   */
  public String getCddivisa() {
    return cddivisa;
  }

  /**
   * Sets the cddivisa.
   *
   * @param cddivisa the new cddivisa
   */
  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  /**
   * Gets the comision.
   *
   * @return the comision
   */
  public BigDecimal getComision() {
    return comision;
  }

  /**
   * Sets the comision.
   *
   * @param comision the new comision
   */
  public void setComision(BigDecimal comision) {
    this.comision = comision;
  }

  /**
   * Gets the fhejecuc.
   *
   * @return the fhejecuc
   */
  public Date getFhejecuc() {
    return fhejecuc;
  }

  /**
   * Sets the fhejecuc.
   *
   * @param fhejecuc the new fhejecuc
   */
  public void setFhejecuc(Date fhejecuc) {
    this.fhejecuc = fhejecuc;
  }

  /**
   * Gets the imcambio.
   *
   * @return the imcambio
   */
  public BigDecimal getImcambio() {
    return imcambio;
  }

  /**
   * Sets the imcambio.
   *
   * @param imcambio the new imcambio
   */
  public void setImcambio(BigDecimal imcambio) {
    this.imcambio = imcambio;
  }

  /**
   * Gets the nucaneje.
   *
   * @return the nucaneje
   */
  public BigDecimal getNucaneje() {
    return nucaneje;
  }

  /**
   * Sets the nucaneje.
   *
   * @param nucaneje the new nucaneje
   */
  public void setNucaneje(BigDecimal nucaneje) {
    this.nucaneje = nucaneje;
  }

  /**
   * Gets the tpoperac.
   *
   * @return the tpoperac
   */
  public String getTpoperac() {
    return tpoperac;
  }

  /**
   * Sets the tpoperac.
   *
   * @param tpoperac the new tpoperac
   */
  public void setTpoperac(String tpoperac) {
    this.tpoperac = tpoperac;
  }

  /**
   * Gets the nunivel 1.
   *
   * @return the nunivel 1
   */
  public String getNunivel1() {
    return nunivel1;
  }

  /**
   * Sets the nunivel 1.
   *
   * @param nunivel1 the new nunivel 1
   */
  public void setNunivel1(String nunivel1) {
    this.nunivel1 = nunivel1;
  }

  /**
   * Gets the cddepais.
   *
   * @return the cddepais
   */
  public String getCddepais() {
    return cddepais;
  }

  /**
   * Sets the cddepais.
   *
   * @param cddepais the new cddepais
   */
  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  /**
   * Gets the trimestre.
   *
   * @return the trimestre
   */
  public String getTrimestre() {
    return trimestre;
  }

  /**
   * Sets the trimestre.
   *
   * @param trimestre the new trimestre
   */
  public void setTrimestre(String trimestre) {
    this.trimestre = trimestre;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "McLaganDTO [id=" + id + ", nuoprout=" + nuoprout + ", nupareje=" + nupareje + ", cdclient=" + cdclient
        + ", cdproduc=" + cdproduc + ", nuctapro=" + nuctapro + ", nuclient=" + nuclient + ", cdisinvv=" + cdisinvv
        + ", cdbolsas=" + cdbolsas + ", cddivisa=" + cddivisa + ", comision=" + comision + ", fhejecuc=" + fhejecuc
        + ", imcambio=" + imcambio + ", nucaneje=" + nucaneje + ", tpoperac=" + tpoperac + ", nunivel1=" + nunivel1
        + ", cddepais=" + cddepais + ", trimestre=" + trimestre + "]";
  }
}
