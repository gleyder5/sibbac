package sibbac.business.mclagan.bo.dto;

/**
 * Conteo de los elementos del proceso
 * @author ipalacio
 *
 */
public class SelectInsertCounterDto {

  private String processName;

  private Integer selectElements;

  private Integer insertElements;

  public SelectInsertCounterDto() {
    this.insertElements = 0;
    this.selectElements = 0;
  }

  public Integer getSelectElements() {
    return selectElements;
  }

  public void setSelectElements(Integer selectElements) {
    this.selectElements = selectElements;
  }

  public Integer getInsertElements() {
    return insertElements;
  }

  public void setInsertElements(Integer insertElements) {
    this.insertElements = insertElements;
  }

  public String getProcessName() {
    return processName;
  }

  public void setProcessName(String processName) {
    this.processName = processName;
  }

  public void increment(SelectInsertCounterDto selectInsertCounterDto) {
    this.selectElements = this.selectElements + selectInsertCounterDto.getSelectElements();
    this.insertElements = this.insertElements + selectInsertCounterDto.getInsertElements();
  }
}
