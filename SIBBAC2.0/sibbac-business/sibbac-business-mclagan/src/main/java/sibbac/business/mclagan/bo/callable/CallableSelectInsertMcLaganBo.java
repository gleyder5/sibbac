package sibbac.business.mclagan.bo.callable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.McLaganBo;
import sibbac.business.mclagan.bo.dto.SelectInsertCounterDto;
import sibbac.business.mclagan.db.model.McLaganEntity;
import sibbac.business.mclagan.thread.MacLaganThreadFactory;
import sibbac.business.mclagan.web.util.Constantes.TipoOperativa;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;

/**
 * The Class MclaganBo.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableSelectInsertMcLaganBo implements Callable<SelectInsertCounterDto> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(CallableSelectInsertMcLaganBo.class);
  
  @Value("${mclagan.batch.timeout.operativa:180}")
  private int timeout; // Este time out hay que bajarlo
  
  @Value("${mclagan.db.bulk.operativa.insert.page.size:10000}")
  private int dbInsertPageSize;
  
  /** Número de Hilos de ejecucion */
  @Value("${mclagan.pool.operativa.insert.pool.number:6}")
  private int threadsForInserts;
 
  @Autowired
  private McLaganBo mcLaganBo;

  @Autowired
  private ApplicationContext ctx;

  private Calendar fechaDesde;

  private Calendar fechaHasta;

  private Integer trimestre;

  private TipoOperativa tipoOperativa;

  /**
   * Operaciones mc lagan.
   *
   * @param solicitud the solicitud
   * @param listadoMcLagan the listado mc lagan
   * @param result the result
   */
  public void setParams(Calendar fechaDesde, Calendar fechaHasta, Integer trimestre) {
    this.fechaDesde = fechaDesde;
    this.fechaHasta = fechaHasta;
    this.trimestre = trimestre;
  }

  @Override
  public SelectInsertCounterDto call() throws SIBBACBusinessException {
    if (tipoOperativa == TipoOperativa.OPERATIVA_NACIONAL) {
      return getPageOperativaNacionalAndInsert(fechaDesde, fechaHasta, trimestre);
    }
    else if (tipoOperativa == TipoOperativa.OPERATIVA_INTERNACIONAL) {
      return getPageOperativaInternacionalAndInsert(fechaDesde, fechaHasta, trimestre);
    } else {
      return new SelectInsertCounterDto();
    }
  }

  /**
   * Obtiene la pagina de Operativa nacional y la inserta de forma paginada
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   */
  private SelectInsertCounterDto getPageOperativaNacionalAndInsert(Calendar fechaDesde, Calendar fechaHasta, Integer trimestre)
      throws SIBBACBusinessException { 
    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();
   
    // Obtenemos la lista de Operativa
    long initTimeStamp;

    initTimeStamp = System.currentTimeMillis();

    // Obtenemos una pagina
    final List<Object[]> operativaNacionalList = mcLaganBo.getListadoOperativaNacionalPageByDates(fechaDesde,
        fechaHasta, trimestre);
    
   
    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Operativa Nacional (SELECT): Pagina [{} - {} trimestre({})], Elementos: {}, en {} ms",
          FormatDataUtils.convertDateToString(fechaDesde.getTime()),
          FormatDataUtils.convertDateToString(fechaHasta.getTime()), trimestre, operativaNacionalList.size(),
          System.currentTimeMillis() - initTimeStamp);
    }

    initTimeStamp = System.currentTimeMillis();

    // Insertamos la pagina de forma troceada y mediente hilos
    final Integer results = insertOperativaByPagesThread(operativaNacionalList);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Operativa Nacional (INSERT): Pagina [{} - {} trimestre({})], Elementos: {}, en {} ms",
          FormatDataUtils.convertDateToString(fechaDesde.getTime()),
          FormatDataUtils.convertDateToString(fechaHasta.getTime()), trimestre, results,
          System.currentTimeMillis() - initTimeStamp);
    }

    selectInsertCounterDto.setSelectElements(operativaNacionalList.size());
    selectInsertCounterDto.setInsertElements(results);
    
    return selectInsertCounterDto;
  }

  /**
   * Obtiene la pagina de Operativa nacional y la inserta de forma paginada
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   */
  private SelectInsertCounterDto getPageOperativaInternacionalAndInsert(Calendar fechaDesde, Calendar fechaHasta, Integer trimestre)
      throws SIBBACBusinessException {
    
    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();
    // Obtenemos la lista de Operativa
    long initTimeStamp;

    initTimeStamp = System.currentTimeMillis();

    // Obtenemos una pagina
    final List<Object[]> operativaNacionalList = mcLaganBo.getListadoOperativaInternacionalPageByDates(fechaDesde,
        fechaHasta, trimestre);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Operativa Internacional (SELECT): Pagina [{} - {} trimestre({})], Elementos: {}, en {} ms",
          FormatDataUtils.convertDateToString(fechaDesde.getTime()),
          FormatDataUtils.convertDateToString(fechaHasta.getTime()), trimestre, operativaNacionalList.size(),
          System.currentTimeMillis() - initTimeStamp);
    }

    initTimeStamp = System.currentTimeMillis();

    // Insertamos la pagina de forma troceada y mediente hilos
    final Integer results = insertOperativaByPagesThread(operativaNacionalList);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Operativa Internacional (INSERT): Pagina [{} - {} trimestre({})], Elementos: {}, en {} ms",
          FormatDataUtils.convertDateToString(fechaDesde.getTime()),
          FormatDataUtils.convertDateToString(fechaHasta.getTime()), trimestre, results,
          System.currentTimeMillis() - initTimeStamp);
    }
    
    selectInsertCounterDto.setSelectElements(operativaNacionalList.size());
    selectInsertCounterDto.setInsertElements(results);
    
    return selectInsertCounterDto;
  }

  /**
   * Divide la lista de entrada en n paginas de tamañano dbPageSize
   * @param operativaResults
   * @return
   */
  public Integer insertOperativaByPagesThread(List<Object[]> operativaResults) throws SIBBACBusinessException {
    Integer results = 0;

    try {
      final List<CallableMcLaganOperativaSaveBo> processInterfaces = new ArrayList<>();

      final List<McLaganEntity> operativaResultsEntities = toEntityList(operativaResults);

      final Integer operativaResultsSize = operativaResultsEntities.size();

      final Integer numPaginas = operativaResultsSize / dbInsertPageSize;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * dbInsertPageSize;
        Integer toIndex = ((pageNum + 1) * dbInsertPageSize);

        if (toIndex > operativaResultsSize || fromIndex > operativaResultsSize) {
          toIndex = operativaResultsSize;
        }

        final List<McLaganEntity> subList = operativaResultsEntities.subList(fromIndex, toIndex);

        final CallableMcLaganOperativaSaveBo callableBo = ctx.getBean(CallableMcLaganOperativaSaveBo.class);
        callableBo.setList(subList);

        processInterfaces.add(callableBo);
      }
      // Lanzamos los hilos
      results = launchSaveProcess(processInterfaces);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(e);
    }
    return results;
  }

  /**
   * Crea tandas de Hilos de ejecucion
   * @param processInterfaces
   * @throws InterruptedException
   * @throws SIBBACBusinessException
   */
  public Integer launchSaveProcess(List<CallableMcLaganOperativaSaveBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    Integer result = 0;

    try {
      final Integer numPaginas = processInterfaces.size() / threadsForInserts;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threadsForInserts;
        Integer toIndex = ((pageNum + 1) * threadsForInserts);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableMcLaganOperativaSaveBo> subList = processInterfaces.subList(fromIndex, toIndex);

        result += launchSaveThreadBatch(subList);
      }
    }
    catch (BeansException | InterruptedException | ExecutionException e) {
      throw new SIBBACBusinessException("Error durante el calculo de la tanda de hilos", e);
    }

    return result;
  }

  /**
   * Ejecuta la tanda de hilo
   * @param processInterfaces
   * @throws ExecutionException
   */
  public Integer launchSaveThreadBatch(List<CallableMcLaganOperativaSaveBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    Integer results = 0;

    final List<Future<Integer>> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threadsForInserts,
        new MacLaganThreadFactory("bulkSaveOperativa"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableMcLaganOperativaSaveBo callableBo : processInterfaces) {
      futureList.add(executor.submit(callableBo));
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (onTime) { // *
      for (Future<Integer> future : futureList) {
        try {
          results += future.get();
        }
        catch (InterruptedException | ExecutionException exception) {
          LOGGER.error("Se ha producido un error durante la inserción en masa en McLagan: ", exception);
        }
      }
    }
    else {
      // Si termina en tiempo
      for (Future<Integer> future : futureList) {
        if (!future.isDone()) {
          future.cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
    return results;
  }

  /**
   * Transforma una lista de row en una lista de Entidades
   * @param operativaResults
   * @return
   */
  private List<McLaganEntity> toEntityList(List<Object[]> operativaResults) {
    List<McLaganEntity> entityList = new ArrayList<>();

    for (Object[] row : operativaResults) {
      entityList.add(McLaganEntity.toEntity(row));
    }

    return entityList;
  }

  public TipoOperativa getTipoOperativa() {
    return tipoOperativa;
  }

  public void setTipoOperativa(TipoOperativa tipoOperativa) {
    this.tipoOperativa = tipoOperativa;
  }
}
