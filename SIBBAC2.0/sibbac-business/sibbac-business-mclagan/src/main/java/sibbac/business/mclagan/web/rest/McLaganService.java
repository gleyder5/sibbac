package sibbac.business.mclagan.web.rest;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.CharEncoding;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_TAREA;
import sibbac.business.mclagan.task.TaskGeneracionMCLAGAN;
import sibbac.business.mclagan.web.dto.RequestDTO;
import sibbac.business.mclagan.web.util.Constantes;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.common.CallableResultDTO;
import sibbac.common.utils.FormatDataUtils;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class McLaganService.
 */
@RestController
@RequestMapping("/mclagan")
public class McLaganService {

  /** The Constant DEFAULT_CHARSET. */
  protected static final String DEFAULT_CHARSET = CharEncoding.UTF_8;

  /** The Constant DEFAULT_APPLICATION_TYPE. */
  protected static final String DEFAULT_APPLICATION_TYPE = MediaType.APPLICATION_JSON + ";charset=" + DEFAULT_CHARSET;

  /** The tarea. */
  @Autowired
  private TaskGeneracionMCLAGAN taskGeneracionMCLAGAN;

  /** The men bo. */
  @Autowired
  protected Tmct0menBo menBo;

  @Autowired
  private HttpSession session;

  /**
   * Instantiates a new McLaganService.
   */
  public McLaganService() {
    super();
  }

  /**
   * Execute action post.
   *
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody RequestDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();
    NumberFormat formato = new DecimalFormat("000");

    final String cdMensa = formato.format(TIPO_TAREA.TASK_MCLAGAN.getTipo());

    final String estado = menBo.estadoDisponible(cdMensa);

    final boolean isRunningIBackGround = taskGeneracionMCLAGAN.isRunningInBackGround();

    if (isRunningIBackGround) {
      result.addErrorMessage(Constantes.ApplicationMessages.TASK_IN_BACKGROUND_PROGRESS.value());
    }
    else {
      if (estado.equalsIgnoreCase(Constantes.ApplicationMessages.MCLAGAN_READY.value())) {
        realizarSolicitud(solicitud, result);
        if (result.getMessage().isEmpty() && result.getErrorMessage().isEmpty()) {
          result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
        }
      }
      else if (estado.equalsIgnoreCase(Constantes.ApplicationMessages.MCLAGAN_IN_PROGRESS.value())) {
        result.addErrorMessage(Constantes.ApplicationMessages.TASK_IN_PROGRESS.value());
      }
      else if (estado.equalsIgnoreCase(Constantes.ApplicationMessages.MCLAGAN_INACTIVE.value())) {
        result.addErrorMessage(Constantes.ApplicationMessages.TASK_INACTIVE.value());
      }
      else {
        result.addErrorMessage(Constantes.ApplicationMessages.TASK_FAILED.value());
      }
    }

    taskGeneracionMCLAGAN.getProcessResultDto().setResult(null);
    taskGeneracionMCLAGAN.getProcessResultDto().setResultMessage(null);

    return result;
  }

  /**
   * Realizar solicitud.
   *
   * @param solicitud the solicitud
   * @param result the result
   */
  private void realizarSolicitud(RequestDTO solicitud, final CallableResultDTO result) {
    if (null != solicitud) {
      if (null != solicitud.getFechaDesde() && null != solicitud.getFechaHasta() && null != solicitud.getTrimestre()) {
        final Date fechaDesde = FormatDataUtils.convertStringToDate(solicitud.getFechaDesde(),
            FormatDataUtils.DATE_ES_FORMAT_SEPARATOR);
        final Date fechaHasta = FormatDataUtils.convertStringToDate(solicitud.getFechaHasta(),
            FormatDataUtils.DATE_ES_FORMAT_SEPARATOR);
        final String userSession = StringUtils.substring(((UserSession) session.getAttribute("UserSession")).getName(),
            0, 10);

        final Integer trimestre = Integer.parseInt(solicitud.getTrimestre());

        taskGeneracionMCLAGAN.setTaskParams(fechaDesde, fechaHasta, trimestre);
        taskGeneracionMCLAGAN.setUserSession(userSession);
        taskGeneracionMCLAGAN.execute();

        if (taskGeneracionMCLAGAN.getProcessResultDto().getResult()) {
          result.addAllMessage(taskGeneracionMCLAGAN.getProcessResultDto().getResultMessage());
        }
        else {
          result.addAllErrorMessage(taskGeneracionMCLAGAN.getProcessResultDto().getResultMessage());
        }
      }
      else {
        result.addErrorMessage(Constantes.ApplicationMessages.ADD_REQUEST_DATA.value());
      }
    }
  }
}
