package sibbac.business.mclagan.bo.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProcessResultDto implements Serializable {

  private static final long serialVersionUID = 1L;

  /** Es resultado correcto */
  private Boolean result;

  /** Descripcion del resultado. */
  private List<String> resultMessage;

  public ProcessResultDto() {
    this.result = true;
    this.resultMessage = new ArrayList<>();
  }

  public Boolean getResult() {
    return result;
  }

  public void setResult(Boolean result) {
    this.result = result;
  }

  public List<String> getResultMessage() {
    return resultMessage;
  }

  public void setResultMessage(List<String> resultMessage) {
    this.resultMessage = resultMessage;
  }

}
