package sibbac.business.mclagan.bo.callable;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.GenerateAccountMcLaganFile;
import sibbac.business.mclagan.bo.dto.ProcessResultDto;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGenerateAccountMcLaganFile implements Callable<ProcessResultDto> {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGenerateTradeMcLaganFile.class);

  @Autowired
  private GenerateAccountMcLaganFile generateAccountMcLaganFile;

  /** Metódo que se ejecutará en el hilo */
  @Override
  public ProcessResultDto call() throws Exception {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza el proceso en background de GenerateAccount");
    }

    return generateAccountMcLaganFile.generateAccountFile();
  }

  public GenerateAccountMcLaganFile getGenerateAccountMcLaganFile() {
    return generateAccountMcLaganFile;
  }

  public void setGenerateAccountMcLaganFile(GenerateAccountMcLaganFile generateAccountMcLaganFile) {
    this.generateAccountMcLaganFile = generateAccountMcLaganFile;
  }

}
