package sibbac.business.mclagan.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mclagan.bo.callable.CallableSelectInsertMcLaganCuentasBo;
import sibbac.business.mclagan.bo.dto.SelectInsertCounterDto;
import sibbac.business.mclagan.db.dao.McLaganCuentasDAO;
import sibbac.business.mclagan.db.model.McLaganCuentasEntity;
import sibbac.business.mclagan.thread.MacLaganThreadFactory;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.aop.logging.SIBBACLogging;
import sibbac.database.bo.AbstractBo;

/**
 * The Class MclaganBo.
 */
@Service("mcLaganCuentasBo")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class McLaganCuentasBo extends AbstractBo<McLaganCuentasEntity, String, McLaganCuentasDAO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(McLaganCuentasBo.class);

  @Autowired
  private EntityManagerFactory entityManagerFactory;
  
  /** Time out para el proceso de carga de operativa */
  @Value("${mclagan.batch.timeout.global:360}")
  private int timeoutGlobal;

  @Value("${mclagan.batch.timeout.cuentas:180}")
  private int timeoutCuentas;

  @Value("${mclagan.db.bulk.cuentas.select.page.size:20000}")
  private int dbSelectPageSize;

  @Value("${mclagan.db.bulk.cuentas.insert.page.size:10000}")
  private int dbInsertPageSize;

  @Value("${mclagan.db.bulk.cuentas.commit.interval:1000}")
  private int commitInterval;

  /** Número de Hilos de ejecucion */
  @Value("${mclagan.pool.cuentas.select.pool.number:6}")
  private int threadsForSelect;

  @Autowired
  private ApplicationContext ctx;

  @SIBBACLogging
  public Integer loadMcLaganCuentas() throws SIBBACBusinessException {
    // Limpiamos la tabla de MACLAGAN_CUENTAS
    final SelectInsertCounterDto selectInsertCounterDto;

    // Limpiamos la tabla de cuentas de MacLagan
    cleanMacLaganCuentasTable();

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Se lanza el proceso Insert/Select de MacLaganCuentas con hilos");
    }

    selectInsertCounterDto = selectInsertCuentasPreparePagination();
    selectInsertCounterDto.setProcessName("CUENTAS:");
    
    if (LOG.isInfoEnabled()) {
      LOG.info("\n{}:\n\tElementos Obtenidos: \t{}\n\tElementos Insertados: \t{}", selectInsertCounterDto.getProcessName(),
          selectInsertCounterDto.getSelectElements(), selectInsertCounterDto.getInsertElements());
    }

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Se termina el proceso Insert/Select de MacLaganCuentas");
    }

    return selectInsertCounterDto.getInsertElements();
  }

  /**
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  public SelectInsertCounterDto selectInsertCuentasPreparePagination() throws SIBBACBusinessException {
    SelectInsertCounterDto selectInsertCounterDto = null;
    try {
      final List<CallableSelectInsertMcLaganCuentasBo> processThreads = new ArrayList<>();

      final Integer countCuentas = dao.countMcLaganCuentas();

      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Cuentas: Se obtienen {} elementos {} paginas", countCuentas, (countCuentas / dbSelectPageSize));
      }

      Pageable page = new PageRequest(0, dbSelectPageSize);

      while (page.getOffset() < countCuentas) {
        final CallableSelectInsertMcLaganCuentasBo callableBo = ctx.getBean(CallableSelectInsertMcLaganCuentasBo.class);
        callableBo.setPage(page);
        processThreads.add(callableBo);
        page = page.next();
      }

      // Lanzamos los hilos
      selectInsertCounterDto = launchGetProcess(processThreads);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(e);
    }

    return selectInsertCounterDto;
  }

  /**
   * Crea tandas de Hilos de ejecucion
   * @param processInterfaces
   * @throws InterruptedException
   * @throws SIBBACBusinessException
   */
  public SelectInsertCounterDto launchGetProcess(List<CallableSelectInsertMcLaganCuentasBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();

    try {
      final Integer numPaginas = processInterfaces.size() / threadsForSelect;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threadsForSelect;
        Integer toIndex = ((pageNum + 1) * threadsForSelect);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableSelectInsertMcLaganCuentasBo> subList = processInterfaces.subList(fromIndex, toIndex);

        selectInsertCounterDto.increment(launchGetThreadBatch(subList));
      }
    }
    catch (BeansException | InterruptedException | ExecutionException e) {
      throw new SIBBACBusinessException("Error durante el calculo de la tanda de hilos", e);
    }

    return selectInsertCounterDto;
  }

  /**
   * Ejecuta la tanda de hilo
   * @param processInterfaces
   * @throws ExecutionException
   */
  public SelectInsertCounterDto launchGetThreadBatch(List<CallableSelectInsertMcLaganCuentasBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();

    final List<Future<SelectInsertCounterDto>> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threadsForSelect,
        new MacLaganThreadFactory("batchCuentas_pageSelect"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableSelectInsertMcLaganCuentasBo callableBo : processInterfaces) {
      futureList.add(executor.submit(callableBo));
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeoutCuentas, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (onTime) { // *
      for (Future<SelectInsertCounterDto> future : futureList) {
        selectInsertCounterDto.increment(future.get());
      }
    }
    else {
      // Si termina en tiempo
      for (Future<SelectInsertCounterDto> future : futureList) {
        if (!future.isDone()) {
          future.cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }
    return selectInsertCounterDto;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public List<Object[]> getListadoCuentasPage(final Pageable page) {
    return dao.getCuentasByPage(page.getPageNumber(), page.getPageSize());
  }

  /**
   * Guarda una lista de entidades en MacLagan
   * @param mcLaganCuentasEntityList
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkCuentas(List<McLaganCuentasEntity> mcLaganCuentasEntityList) throws SIBBACBusinessException {
    Integer resultCounter = 0;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (McLaganCuentasEntity mcLaganCuentasEntity : mcLaganCuentasEntityList) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          try {
            entityTransaction.commit();
            entityTransaction.begin();
            entityManager.clear();
          }
          catch (EntityExistsException | IllegalStateException | TransactionRequiredException
              | RollbackException exception) {
            LOG.error("No se pudo realizar el commit intermedio en MCLAGAN:", exception);
          }
        }

        try {
          entityManager.persist(mcLaganCuentasEntity);
          resultCounter++;
        }
        catch (EntityExistsException | IllegalStateException | TransactionRequiredException exception) {
          LOG.error("No se pudo insertar {} en MCLAGANCUENTAS:", mcLaganCuentasEntity, exception);

        }
      }
      entityTransaction.commit();
    }
    catch (IllegalStateException | RollbackException e) {
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw new SIBBACBusinessException("Error en BulkInsert en MCLAGANCUENTAS", e);
    }
    finally {
      entityManager.close();
    }

    return resultCounter;
  }

  @SIBBACLogging
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void cleanMacLaganCuentasTable() {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Borramos todos los datos de la tabla TMCT0_MCLAGAN_CUENTAS");
    }

    dao.cleanTable();
  }

  

  public List<Object[]> findAllAccounts(Integer offset, Integer pageSize) {
    return dao.findAllAccounts(offset, pageSize);
  }

  public Integer countAllAccounts() {
    return dao.countAllAccounts();
  }

}
