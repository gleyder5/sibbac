package sibbac.business.mclagan.bo.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.McLaganCuentasBo;
import sibbac.business.mclagan.bo.dto.SelectInsertCounterDto;
import sibbac.business.mclagan.db.model.McLaganCuentasEntity;
import sibbac.business.mclagan.thread.MacLaganThreadFactory;
import sibbac.common.SIBBACBusinessException;

/**
 * The Class MclaganBo.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableSelectInsertMcLaganCuentasBo implements Callable<SelectInsertCounterDto> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(CallableSelectInsertMcLaganCuentasBo.class);

  @Autowired
  private McLaganCuentasBo mcLaganCuentasBo;

  @Autowired
  private ApplicationContext ctx;

  @Value("${mclagan.db.bulk.cuentas.insert.page.size:10000}")
  private int dbInsertPageSize;

  /** Número de Hilos de ejecucion */
  @Value("${mclagan.pool.ocuentas.insert.pool.number:6}")
  private int threadsForInserts;

  @Value("${mclagan.batch.timeout.cuentas:360}")
  private int timeout;

  private Pageable page;

  @Override
  public SelectInsertCounterDto call() throws Exception {
    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();

    // Obtenemos la lista de Operativa
    long initTimeStamp;

    initTimeStamp = System.currentTimeMillis();

    final List<Object[]> cuentasList = mcLaganCuentasBo.getListadoCuentasPage(page);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Cuentas (SELECT): Pagina [{}], Elementos: {}, en {} ms", page.getPageNumber(), cuentasList.size(),
          System.currentTimeMillis() - initTimeStamp);
    }

    initTimeStamp = System.currentTimeMillis();

    // Insertamos la pagina de forma troceada y mediente hilos
    final Integer results = insertCuentasByPagesThread(cuentasList);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Cuentas (INSERT): Pagina [{}], Elementos: {}, en {} ms", page.getPageNumber(), results,
          System.currentTimeMillis() - initTimeStamp);
    }

    selectInsertCounterDto.setSelectElements(cuentasList.size());
    selectInsertCounterDto.setInsertElements(results);

    return selectInsertCounterDto;
  }

  /**
   * Divide la lista de entrada en n paginas de tamañano dbPageSize
   * @param cuentasResults
   * @return
   */
  public Integer insertCuentasByPagesThread(List<Object[]> cuentasResults) throws SIBBACBusinessException {
    Integer results = 0;

    try {
      final List<CallableMcLaganCuentasSaveBo> processInterfaces = new ArrayList<>();

      final List<McLaganCuentasEntity> cuentasResultsEntities = toEntityList(cuentasResults);

      final Integer cuentasResultsSize = cuentasResultsEntities.size();

      final Integer numPaginas = cuentasResultsSize / dbInsertPageSize;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * dbInsertPageSize;
        Integer toIndex = ((pageNum + 1) * dbInsertPageSize);

        if (toIndex > cuentasResultsSize || fromIndex > cuentasResultsSize) {
          toIndex = cuentasResultsSize;
        }

        final List<McLaganCuentasEntity> subList = cuentasResultsEntities.subList(fromIndex, toIndex);

        final CallableMcLaganCuentasSaveBo callableBo = ctx.getBean(CallableMcLaganCuentasSaveBo.class);
        callableBo.setList(subList);

        processInterfaces.add(callableBo);
      }
      // Lanzamos los hilos
      results = launchSaveProcess(processInterfaces);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException(e);
    }
    return results;
  }

  /**
   * Crea tandas de Hilos de ejecucion
   * @param processInterfaces
   * @throws InterruptedException
   * @throws SIBBACBusinessException
   */
  public Integer launchSaveProcess(List<CallableMcLaganCuentasSaveBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    Integer result = 0;

    try {
      final Integer numPaginas = processInterfaces.size() / threadsForInserts;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threadsForInserts;
        Integer toIndex = ((pageNum + 1) * threadsForInserts);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableMcLaganCuentasSaveBo> subList = processInterfaces.subList(fromIndex, toIndex);

        result += launchSaveThreadBatch(subList);
      }
    }
    catch (BeansException | InterruptedException | ExecutionException e) {
      throw new SIBBACBusinessException("Error durante el calculo de la tanda de hilos", e);
    }

    return result;
  }

  /**
   * Ejecuta la tanda de hilo
   * @param processInterfaces
   * @throws ExecutionException
   */
  public Integer launchSaveThreadBatch(List<CallableMcLaganCuentasSaveBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    Integer results = 0;

    final List<Future<Integer>> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threadsForInserts,
        new MacLaganThreadFactory("bulkSaveCuentas"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableMcLaganCuentasSaveBo callableBo : processInterfaces) {
      futureList.add(executor.submit(callableBo));
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeout, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (onTime) { // *
      for (Future<Integer> future : futureList) {
        final Integer count = future.get();
        results += count;
      }
    }
    else {
      // Si termina en tiempo
      for (Future<Integer> future : futureList) {
        if (!future.isDone()) {
          future.cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }

    return results;
  }

  /**
   * Transforma una lista de row en una lista de Entidades
   * @param cuentasResults
   * @return
   */
  private List<McLaganCuentasEntity> toEntityList(List<Object[]> cuentasResults) {
    List<McLaganCuentasEntity> entityList = new ArrayList<>();

    for (Object[] row : cuentasResults) {
      entityList.add(McLaganCuentasEntity.toEntity(row));
    }

    return entityList;
  }

  public Pageable getPage() {
    return page;
  }

  public void setPage(Pageable page) {
    this.page = page;
  }

}
