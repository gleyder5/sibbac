package sibbac.business.mclagan.web.dto;

import sibbac.business.mclagan.db.model.McLaganCuentasEntity;
import sibbac.business.mclagan.web.dto.common.AbstractDTO;

/**
 * The Class McLaganCuentasDTO.
 */
public class McLaganCuentasDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2787896966143413158L;

  /** The accountId. */
  private String accountId;

  /** The accountName revision. */
  private String accountName;

  /** The accountCountry. */
  private String accountCountry;

  /** The parentId. */
  private Long parentId;

  /** The parentName. */
  private String parentName;

  /**
   * Instantiates a new McLaganCuentasDTO.
   */
  public McLaganCuentasDTO() {
  }

  /**
   * Instantiates a new McLaganCuentasDTO.
   *
   * @param accountId the account id
   * @param accountName the account name
   * @param accountCountry the account country
   * @param parentId the parent id
   * @param parentName the parent name
   */
  public McLaganCuentasDTO(String accountId, String accountName, String accountCountry, Long parentId,
      String parentName) {
    this.accountId = accountId;
    this.accountName = accountName;
    this.accountCountry = accountCountry;
    this.parentId = parentId;
    this.parentName = parentName;
  }

  /**
   * To this dto.
   *
   * @param entity the entity
   */
  public void toThisDto(McLaganCuentasEntity entity) {
    this.accountId = entity.getAccountId();
    this.accountName = entity.getAccountName();
    this.accountCountry = entity.getAccountCountry();
    this.parentId = entity.getParentId();
    this.parentName = entity.getParentName();
  }

  /**
   * Gets the account Id.
   *
   * @return the account Id
   */
  public String getAccountId() {
    return accountId;
  }

  /**
   * Sets the account Id.
   *
   * @param accountId the new account id
   * @return account Id the new account Id
   */
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * Gets the account Name.
   *
   * @return the account Name
   */
  public String getAccountName() {
    return accountName;
  }

  /**
   * Sets the account Name.
   *
   * @param accountName the new account name
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * Gets the account Country.
   *
   * @return the account Country
   */
  public String getAccountCountry() {
    return accountCountry;
  }

  /**
   * Sets the account Country.
   *
   * @param accountCountry the new account country
   */
  public void setAccountCountry(String accountCountry) {
    this.accountCountry = accountCountry;
  }

  /**
   * Gets the parent Id.
   *
   * @return the parent Id
   */
  public Long getParentId() {
    return parentId;
  }

  /**
   * Sets the parent Id.
   *
   * @param parentId the new parent id
   */
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  /**
   * Gets the parent Name.
   *
   * @return the parent Name
   */
  public String getParentName() {
    return parentName;
  }

  /**
   * Sets the parent Name.
   *
   * @param parentName the new parent name
   */
  public void setParentName(String parentName) {
    this.parentName = parentName;
  }

  /**
   * Salida toString del objeto.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "[accountId =" + accountId + ", accountName =" + accountName + ", accountCountry =" + accountCountry
        + ", parentId=" + parentId + ", parentName=" + "parentName]";
  }

}
