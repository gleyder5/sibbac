package sibbac.business.mclagan.bo.callable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.McLaganBo;
import sibbac.business.mclagan.bo.McLaganCuentasBo;
import sibbac.business.mclagan.bo.dto.ProcessResultDto;
import sibbac.business.mclagan.task.TaskGeneracionMCLAGAN;
import sibbac.business.mclagan.web.util.Constantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.common.utils.SendMail;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;

@Component
public class CallableMcLaganProcessBo implements Callable<ProcessResultDto> {

  private static final Logger LOG = LoggerFactory.getLogger(CallableMcLaganProcessBo.class);

  /** Ruta donde se deposita el fichero. */
  @Value("${mclagan.fichero.out.dir.path:/sbrmv/ficheros/mclagan/in/tratados}")
  private String outPutDir;

  /** Nombre del fichero generado por el proceso. */
  @Value("${mclagan.fichero.trade.file:TRADE_INFORMATION}")
  private String fileNameTrade;

  /** Nombre del fichero generado por el proceso. */
  @Value("${mclagan.fichero.account.file:ACCOUNT_INFORMATION}")
  private String fileNameAccount;

  /** Nombre del fichero generado por el proceso. */
  @Value("${mclagan.fichero.instrument.file:INSTRUMENT_INFORMATION}")
  private String fileNameInstrument;
 
  /** Tamaño de la página para. */
  @Value("${mclagan.db.trade.page.size:10000}")
  private int dbTradePageSize;

  /** Tamaño de la página para. */
  @Value("${mclagan.db.account.page.size:10000}")
  private int dbAccountPageSize;

  /** Tamaño de la página para. */
  @Value("${mclagan.db.instrument.page.size:10000}")
  private int dbInstrumentsPageSize;
  
  @Value("${mclagan.batch.timeout.file:360}")
  private int fileTimeOut;
  
  @Autowired
  private McLaganBo mcLaganBo;

  @Autowired
  private McLaganCuentasBo mcLaganCuentasBo;

  @Autowired
  private ApplicationContext ctx;  
  
  @Autowired
  private Tmct0UsersBo tmct0UsersBo;
  
  @Autowired
  private SendMail sendMail;

  /** The fecha desde. */
  private Date fechaDesde;

  /** The fecha hasta. */
  private Date fechaHasta;

  /** The trimestr. */
  private Integer trimestre;

  private String codigoUsuario;

  private Boolean sendMailOnFinish = false;

  private TaskGeneracionMCLAGAN parent;

  private boolean isRunning;

  @Override
  public ProcessResultDto call() throws SIBBACBusinessException {
    final ProcessResultDto processResultDto;

    long initTime = System.currentTimeMillis();

    if (LOG.isInfoEnabled()) {
      LOG.info("Se inicia la tarea de Envio informe McLagan");
    }

    processResultDto = launchMacLaganProcess();

    if (parent.getIsWaitting()) {
      LOG.info("El proceso padre está en espera, notificamos su wakeup");
      parent.wakeUpProcess();
    }

    if (sendMailOnFinish) {
      LOG.info("Iniciamos el envio del mail al usuario que lo peticiono");
      sendFinishMail(processResultDto);
    }

    if (LOG.isInfoEnabled()) {
      LOG.info("Se finaliza la tarea de Envio informe McLagan, Tiempo de ejecucion {} milisegundos", (System.currentTimeMillis() - initTime));
    }

    return processResultDto;
  }

  /**
   * Ejecuta la tarea McLagan.
   *
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  public ProcessResultDto launchMacLaganProcess() throws SIBBACBusinessException {
    isRunning = true;

    final ProcessResultDto processResultDto = new ProcessResultDto();

    mcLaganBo.setParams(fechaDesde, fechaHasta, trimestre);

    // Lanzamos el proceso de generación de los datos paras las tablas auxiliares
    
    // Cargamos las operacione mediante hilos
    final Integer numeroOperaciones = mcLaganBo.loadMcLagan();
    
    // Cargamos la cuentas mediante hilos
    final Integer numeroCuentas = mcLaganCuentasBo.loadMcLaganCuentas();

    // Se introducen hilos en la generación de los ficheros
    if (numeroOperaciones > 0) {
      final String filePrefix = Calendar.getInstance().get(Calendar.YEAR) + ".Q" + trimestre + ".";

      // Obtenemos los bean para poder ejecutarlos en Hilos
      // Hilo para generar el Fichero de Trade
      final CallableGenerateTradeMcLaganFile generateTradeMcLaganFile = ctx.getBean(CallableGenerateTradeMcLaganFile.class);
      generateTradeMcLaganFile.getGenerateTradeMcLaganFile().setDbPageSize(dbTradePageSize);
      generateTradeMcLaganFile.getGenerateTradeMcLaganFile().setOutPutDir(outPutDir);
      generateTradeMcLaganFile.getGenerateTradeMcLaganFile().setFileName(filePrefix + fileNameTrade);
      
      // Hilo para generar el Fichero de Account 
      final CallableGenerateAccountMcLaganFile generateAccountMcLaganFile = ctx.getBean(CallableGenerateAccountMcLaganFile.class);
      generateAccountMcLaganFile.getGenerateAccountMcLaganFile().setDbPageSize(dbAccountPageSize);
      generateAccountMcLaganFile.getGenerateAccountMcLaganFile().setOutPutDir(outPutDir);
      generateAccountMcLaganFile.getGenerateAccountMcLaganFile().setFileName(filePrefix + fileNameAccount);
      
      // Hilo para generar el Fichero de Instruments
      final CallableGenerateInstrumentMcLaganFile generateInstrumentMcLaganFile = ctx.getBean(CallableGenerateInstrumentMcLaganFile.class);
      generateInstrumentMcLaganFile.getGenerateInstrumentMcLaganFile().setDbPageSize(dbInstrumentsPageSize);
      generateInstrumentMcLaganFile.getGenerateInstrumentMcLaganFile().setOutPutDir(outPutDir);
      generateInstrumentMcLaganFile.getGenerateInstrumentMcLaganFile().setFileName(filePrefix + fileNameInstrument);

      // Iniciamos el proceso de lanzamiento de Hilos
      final List<Future<ProcessResultDto>> futureList = new ArrayList<>();
      boolean onTime = true;
      boolean allRight = true;
      int cancelled = 0;

      try {
        // Creamos el pool de hilos
        final ExecutorService executor = Executors.newFixedThreadPool(3, new MacLaganThreadFactory());

        // Ejecutamos los hilos
        futureList.add(executor.submit(generateTradeMcLaganFile));
        futureList.add(executor.submit(generateAccountMcLaganFile));
        futureList.add(executor.submit(generateInstrumentMcLaganFile));

        // Hacemos que no se puedan añadir más hilos al pool
        executor.shutdown();

        // Ejecutamos los hilos y esperamos a que terminen
        onTime = executor.awaitTermination(fileTimeOut, TimeUnit.MINUTES);

        // Si esta ha superado el tiempo estipulado para la ejecucion de los
        // ficheros cancelamos los hilos
        if (onTime) {
          processResultDto.setResult(true);
          for (Future<ProcessResultDto> future : futureList) {
            processResultDto.getResultMessage().addAll(processFuture(future).getResultMessage());
          }
        }
        else {
          // Si se ha superado el tiempo permitido Cancelamos los procesos que
          // están en ejecución
          for (Future<ProcessResultDto> future : futureList) {
            if (!future.isDone()) {
              future.cancel(true);
              cancelled++;
            }
          }
          // Enviar un mensaje de Error al Usuario
          LOG.error("[sendBatch] Ha finalizado el tiempo de espera de {} minutos, se solicita cancelar {} tareas",
              fileTimeOut, cancelled);
        }
      }
      catch (InterruptedException exception) {
        LOG.error("[sendBatch] Se ha interrumpido el hilo en espera con mensaje {}", exception.getMessage());
        isRunning = true;
      }

      if (!(onTime && allRight)) {
        LOG.error("La tarea batch ha finalizado fuera de tiempo o con batch con errores");
        processResultDto.setResult(false);
        processResultDto.getResultMessage().add("La tarea batch ha finalizado fuera de tiempo o con batch con errores");

      }
    }
    else {      
      if (LOG.isDebugEnabled()) {
        LOG.debug("No se han encontrado Cuentas que volcar en MCLAGAN");
        if (numeroCuentas == 0) {
          LOG.debug("No se han encontrado Cuentas que volcar en MCLAGAN_CUENTAS");
        }
      }
      processResultDto.setResult(true);
      processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_OPERACIONES_EMPTY.value());
      processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_CUENTAS_EMPTY.value());
      processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_INSTRUMENTOS_EMPTY.value());
    }

    isRunning=false;

    return processResultDto;
  }
  
  private ProcessResultDto processFuture(Future<ProcessResultDto> future) {
    ProcessResultDto processResultDto;
    try {
      processResultDto = future.get();
    } catch(InterruptedException | ExecutionException exception) {
      processResultDto = new ProcessResultDto();
      LOG.error("Se ha producido un error en el Hilo principal de McLagan", exception);
      processResultDto.setResult(false);
      processResultDto.getResultMessage().add("Se ha producido un error inesperado durante la ejecución del hilo principal.");
    }
    
    return processResultDto;
  }

  /**
   * Enviamos el mail
   * @param processResultDto
   */
  private void sendFinishMail(ProcessResultDto processResultDto) {
    final String email = tmct0UsersBo.getByUsername(codigoUsuario).getEmail();
    final List<String> emails = new ArrayList<String>();

    emails.add(email);
    StringBuilder body = new StringBuilder(FormatDataUtils.toUTF8("Los informes de Maclagan se han generado correctamente."));

    if (!processResultDto.getResultMessage().isEmpty()) {
      body.append("<br />");
      body.append("<ul>");
      for (String string : processResultDto.getResultMessage()) {
        body.append("<li>").append(FormatDataUtils.toUTF8(string)).append("</li>");
      }
      body.append("</ul>");
      body.append("Gracias.");
    }

    try {
      sendMail.setUtf8(true);
      sendMail.sendMailHtml(emails, FormatDataUtils.toUTF8("Informes McLagan"), body.toString(), null, null, null);
    }
    catch (IllegalArgumentException | MessagingException | IOException e) {
      LOG.error("Se ha producido un error en el envío del mail", e);
    }

    sendMailOnFinish = false;
  }

  public void sendMailOnFinishToUser(Boolean sendMailOnFinish, String codigoUsuario) {
    this.sendMailOnFinish = sendMailOnFinish;
    this.codigoUsuario = codigoUsuario;
  }

  public TaskGeneracionMCLAGAN getParent() {
    return parent;
  }

  public void setParent(TaskGeneracionMCLAGAN parent) {
    this.parent = parent;
  }

  public void setTaskParams(Date fechaDesde, Date fechaHasta, Integer trimestre) {
    this.fechaDesde = fechaDesde;
    this.fechaHasta = fechaHasta;
    this.trimestre = trimestre;
  }

  public String getCodigoUsuario() {
    return codigoUsuario;
  }

  public void setCodigoUsuario(String codigoUsuario) {
    this.codigoUsuario = codigoUsuario;
  }

  public boolean isRunning() {
    return isRunning;
  }

  public void setRunning(boolean isRunning) {
    this.isRunning = isRunning;
  }

  /**
   * Clase interna para la Factoria de Hilos de MacLagan
   * @author ipalacio
   *
   */
  private class MacLaganThreadFactory implements ThreadFactory {

    private int count = 0;

    @Override
    public Thread newThread(Runnable r) {
      final Thread thread = new Thread(r);
      thread.setName(String.format("SIBBAC_MacLagan-%d", count++));
      return thread;
    }
  }

}
