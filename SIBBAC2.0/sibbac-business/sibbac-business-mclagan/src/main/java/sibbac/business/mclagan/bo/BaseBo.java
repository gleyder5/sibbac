package sibbac.business.mclagan.bo;

import java.util.List;

import sibbac.business.mclagan.web.dto.common.AbstractDTO;
import sibbac.common.CallableResultDTO;

public interface BaseBo<T extends AbstractDTO> {

  public CallableResultDTO actionFindById(T solicitud);

  public CallableResultDTO actionSave(T solicitud);

  public CallableResultDTO actionUpdate(T solicitud);

  public CallableResultDTO actionDelete(List<String> solicitud);

}