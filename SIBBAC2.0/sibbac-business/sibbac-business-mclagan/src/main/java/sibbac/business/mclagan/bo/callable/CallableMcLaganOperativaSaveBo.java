package sibbac.business.mclagan.bo.callable;

import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.McLaganBo;
import sibbac.business.mclagan.db.model.McLaganEntity;

/**
 * The Class MclaganBo.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableMcLaganOperativaSaveBo implements Callable<Integer> {

  @Autowired
  private McLaganBo mcLaganBo;

  private List<McLaganEntity> list;

  @Override
  public Integer call() throws Exception {
    return mcLaganBo.saveBulkOperativa(list);
  }

  public List<McLaganEntity> getList() {
    return list;
  }

  public void setList(List<McLaganEntity> list) {
    this.list = list;
  }
}
