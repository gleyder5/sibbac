package sibbac.business.mclagan.web.dto.common;

import java.io.Serializable;

/**
 * The Class AbstractDTO.
 */
public abstract class AbstractDTO implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5044072372221944325L;

}
