package sibbac.business.mclagan.bo;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.RollbackException;
import javax.persistence.TransactionRequiredException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mclagan.bo.callable.CallableMcLaganBo;
import sibbac.business.mclagan.bo.callable.CallableSelectInsertMcLaganBo;
import sibbac.business.mclagan.bo.dto.SelectInsertCounterDto;
import sibbac.business.mclagan.db.dao.McLaganDAO;
import sibbac.business.mclagan.db.model.McLaganEntity;
import sibbac.business.mclagan.thread.MacLaganThreadFactory;
import sibbac.business.mclagan.web.util.Constantes;
import sibbac.business.mclagan.web.util.Constantes.TipoOperativa;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.aop.logging.SIBBACLogging;
import sibbac.common.utils.DateHelper;
import sibbac.database.bo.AbstractBo;

/**
 * The Class MclaganBo.
 */
@Service("mcLaganBo")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class McLaganBo extends AbstractBo<McLaganEntity, BigInteger, McLaganDAO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(McLaganBo.class);
  
  /** Time out para el proceso de carga de operativa */
  @Value("${mclagan.batch.timeout.global:360}")
  private int timeoutGlobal;
  
  /** Time out para el proceso de carga de operativa */
  @Value("${mclagan.batch.timeout.operativa:180}")
  private int timeoutOperativa;

  /** Tamaño de página para selects */
  @Value("${mclagan.db.bulk.operativa.select.page.size:20000}")
  private int dbSelectPageSize;

  /** Intervalo de commits intermedios */
  @Value("${mclagan.db.bulk.operativa.commit.interval:1000}")
  private int commitInterval;

  /** Número de Hilos de ejecucion */
  @Value("${mclagan.pool.operativa.select.pool.number:6}")
  private int threadsForSelects;
  
  @Autowired
  private EntityManagerFactory entityManagerFactory;

  @Autowired
  private ApplicationContext ctx;

  private Date fechaDesde;

  private Date fechaHasta;

  private Integer trimestre;
  
  /**
   * Ejecuta el proceso de Carga de la Tabla MCLAGAN lanzando la carga de la operativa nacional e internacional
   * @return
   * @throws SIBBACBusinessException
   */
  @SIBBACLogging
  public Integer loadMcLagan() throws SIBBACBusinessException {
    Integer registrosInsertados = 0;

    // Chequemos las Fechas y el trimestre
    checkFechasYTrimestre(fechaDesde, fechaHasta, trimestre);

    // Limpiamos la tabla de MACLAGAN
    cleanMacLaganTable();

    // Obtenemos los beans para poder ejecurarlos en hilos independientes
    // Hilo para operativa Nacional
    final CallableMcLaganBo callableOperativaNacional = ctx.getBean(CallableMcLaganBo.class);
    callableOperativaNacional.setParams(fechaDesde, fechaHasta, trimestre);
    callableOperativaNacional.setTipoOperativa(TipoOperativa.OPERATIVA_NACIONAL);

    // Hilo para operativa Internacional
    final CallableMcLaganBo callableOperativaInternacional = ctx.getBean(CallableMcLaganBo.class);
    callableOperativaInternacional.setParams(fechaDesde, fechaHasta, trimestre);
    callableOperativaInternacional.setTipoOperativa(TipoOperativa.OPERATIVA_INTERNACIONAL);

    // Iniciamos el proceso de lanzamiento de Hilos
    final List<Future<SelectInsertCounterDto>> futureList = new ArrayList<>();

    boolean onTime = true;

    try {
      // Creamos el pool de hilos
      final ExecutorService executor = Executors.newFixedThreadPool(2, new MacLaganThreadFactory("McLaganBo"));

      futureList.add(executor.submit(callableOperativaNacional));
      futureList.add(executor.submit(callableOperativaInternacional));

      executor.shutdown();

      // Ejecutamos los hilos y esperamos a que terminen
      onTime = executor.awaitTermination(timeoutGlobal, TimeUnit.MINUTES);

      if (onTime) {
        for (Future<SelectInsertCounterDto> future : futureList) {
          if (LOG.isInfoEnabled()) {
            LOG.info("\n{}:\n\tElementos Obtenidos: \t{}\n\tElementos Insertados: \t{}", future.get().getProcessName(),
                future.get().getSelectElements(), future.get().getInsertElements());
          }
          registrosInsertados += future.get().getInsertElements();
        }
      }
      else {
        for (Future<SelectInsertCounterDto> future : futureList) {
          if (!future.isDone()) {
            future.cancel(true);
          }
        }
      }
    }
    catch (ExecutionException | InterruptedException exception) {
      LOGGER.error("[sendBatch] Se ha interrumpido el hilo en espera con mensaje {}", exception.getMessage());
    }

    return registrosInsertados;
  }

  /**
   * Limpia la tabla de MACLAGAN
   */
  @SIBBACLogging
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public void cleanMacLaganTable() {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Limpiamos los datos de la tabla TMCT0_MCLAGAN");
    }

    dao.cleanTable();
  }

  /**
   * Obtiene los registros de Operativa Internacional y los inserta en la tabla
   * (GESTION POR HILOS) de MACLAGAN
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   * @throws IOException
   */
  public SelectInsertCounterDto selectInsertOperativaNacionalPreparePagination(Date fechaDesde, Date fechaHasta,
      Integer trimestre) throws SIBBACBusinessException {
    long initTimeStamp = System.currentTimeMillis();

    final List<Calendar[]> monthPagination = DateHelper.getMonthsBetweenDate(fechaDesde, fechaHasta);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Operativa Nacional: Se va paginar {} meses", monthPagination.size());
    }

    final SelectInsertCounterDto registrosInsertados = getOperativaNacionalByDatesThreads(monthPagination, trimestre);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Operativa Nacional: Terminada en {} ms", System.currentTimeMillis() - initTimeStamp);
    }

    return registrosInsertados;
  }

  /**
   * Obtiene los registros de Operativa Internacional y los inserta en la tabla
   * (GESTION POR HILOS) de MACLAGAN
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   * @throws IOException
   */
  public SelectInsertCounterDto selectInsertOperativaInteracionalPreparePagination(Date fechaDesde, Date fechaHasta,
      Integer trimestre) throws SIBBACBusinessException {
    long initTimeStamp = System.currentTimeMillis();

    final List<Calendar[]> monthPagination = DateHelper.getMonthsBetweenDate(fechaDesde, fechaHasta);

    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Operativa Internacional: Se va paginar {} meses", monthPagination.size());
    }

    final SelectInsertCounterDto registrosInsertados = getOperativaInternacionalByDatesThreads(monthPagination,
        trimestre);

    if (LOGGER.isInfoEnabled()) {
      LOGGER.info("Operativa Internacional: Terminada en {} ms", System.currentTimeMillis() - initTimeStamp);
    }

    return registrosInsertados;
  }

  /**
   * Metodo que crea hilos por paginas para la operativa nacional
   * @param monthPagination
   * @return
   */
  private SelectInsertCounterDto getOperativaNacionalByDatesThreads(List<Calendar[]> monthPagination, Integer trimestre)
      throws SIBBACBusinessException {

    SelectInsertCounterDto selectInsertCounterDto = null;

    try {
      final List<CallableSelectInsertMcLaganBo> processThreads = new ArrayList<>();

      // Montamos la lista con los BO que ejecutaran los hilos, cada hijo tendra
      // una paginacion
      for (Calendar[] calendars : monthPagination) {
        final CallableSelectInsertMcLaganBo callableBo = ctx
            .getBean(CallableSelectInsertMcLaganBo.class);
        callableBo.setParams(calendars[0], calendars[1], trimestre);
        callableBo.setTipoOperativa(TipoOperativa.OPERATIVA_NACIONAL);
        processThreads.add(callableBo);
      }

      // Lanzamos los hilos
      selectInsertCounterDto = launchGetProcess(processThreads);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException("Se ha producido un error en los hilos de la operativa nacional", e);
    }

    return selectInsertCounterDto;
  }

  /**
   * Metodo que crea hilos por paginas para la operativa internacional
   * @param monthPagination
   * @return
   */
  private SelectInsertCounterDto getOperativaInternacionalByDatesThreads(List<Calendar[]> monthPagination,
      Integer trimestre) throws SIBBACBusinessException {

    SelectInsertCounterDto selectInsertCounterDto = null;

    try {
      final List<CallableSelectInsertMcLaganBo> processThreads = new ArrayList<>();

      // Montamos la lista con los BO que ejecutaran los hilos, cada hijo tendra
      // una paginacion
      for (Calendar[] calendars : monthPagination) {
        final CallableSelectInsertMcLaganBo callableBo = ctx
            .getBean(CallableSelectInsertMcLaganBo.class);
        callableBo.setParams(calendars[0], calendars[1], trimestre);
        callableBo.setTipoOperativa(TipoOperativa.OPERATIVA_INTERNACIONAL);
        processThreads.add(callableBo);
      }

      // Lanzamos los hilos
      selectInsertCounterDto = launchGetProcess(processThreads);
    }
    catch (BeansException | InterruptedException e) {
      throw new SIBBACBusinessException("Se ha producido un error en los hilos de la operativa internacional", e);
    }

    return selectInsertCounterDto;
  }

  /**
   * Obtiene la operativa nacional por fechas
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public List<Object[]> getListadoOperativaNacionalPageByDates(Calendar fechaDesde, Calendar fechaHasta,
      Integer trimestre) {
    return dao.getListadoOperativaNacional(fechaDesde.getTime(), fechaHasta.getTime(), trimestre);
  }

  /**
   * Obtiene la operativa internacional por fechas
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public List<Object[]> getListadoOperativaInternacionalPageByDates(Calendar fechaDesde, Calendar fechaHasta,
      Integer trimestre) {
    return dao.getListadoOperativaInternacional(fechaDesde.getTime(), fechaHasta.getTime(), trimestre);
  }

  /**
   * Crea tandas de Hilos de ejecucion
   * @param processInterfaces
   * @throws InterruptedException
   * @throws SIBBACBusinessException
   */
  public SelectInsertCounterDto launchGetProcess(List<CallableSelectInsertMcLaganBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException {

    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();

    try {
      final Integer numPaginas = processInterfaces.size() / threadsForSelects;

      for (int pageNum = 0; pageNum <= numPaginas; pageNum++) {

        final Integer fromIndex = pageNum * threadsForSelects;
        Integer toIndex = ((pageNum + 1) * threadsForSelects);

        if (toIndex > processInterfaces.size() || fromIndex > processInterfaces.size()) {
          toIndex = processInterfaces.size();
        }

        final List<CallableSelectInsertMcLaganBo> subList = processInterfaces.subList(fromIndex, toIndex);

        selectInsertCounterDto.increment(launchGetThreadBatch(subList));
      }
    }
    catch (BeansException | InterruptedException | ExecutionException e) {
      throw new SIBBACBusinessException("Error durante el calculo de la tanda de hilos", e);
    }

    return selectInsertCounterDto;
  }

  /**
   * Ejecuta la tanda de hilo
   * @param processInterfaces
   * @throws ExecutionException
   */
  public SelectInsertCounterDto launchGetThreadBatch(List<CallableSelectInsertMcLaganBo> processInterfaces)
      throws InterruptedException, SIBBACBusinessException, ExecutionException {

    final SelectInsertCounterDto selectInsertCounterDto = new SelectInsertCounterDto();

    final List<Future<SelectInsertCounterDto>> futureList = new ArrayList<>();

    final ExecutorService executor = Executors.newFixedThreadPool(threadsForSelects,
        new MacLaganThreadFactory("batchOperativa"));

    boolean onTime = true;

    // Iteramos sobre la sublist, serán 5 elementos y que se ha paginado
    // anteriormente por 5
    for (CallableSelectInsertMcLaganBo callableBo : processInterfaces) {
      futureList.add(executor.submit(callableBo));
    }

    // Hacemos que se ejecuten siempre en paralelo n threadas

    executor.shutdown();

    // Return true si executor ha terminado
    onTime = executor.awaitTermination(timeoutOperativa, TimeUnit.MINUTES);

    // Valida si ha terminado la ejecucion
    // Si no ha terminado la cancela
    if (onTime) { // *
      for (Future<SelectInsertCounterDto> future : futureList) {
        selectInsertCounterDto.increment(future.get());
      }
    }
    else {
      // Si termina en tiempo
      for (Future<SelectInsertCounterDto> future : futureList) {
        if (!future.isDone()) {
          future.cancel(true);
          throw new SIBBACBusinessException("Error, se ha agotado el tiempo de carga");
        }
      }
    }

    return selectInsertCounterDto;
  }

  /**
   * Guarda una lista de entidades en MacLagan
   * @param mcLaganEntityList
   */
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  public Integer saveBulkOperativa(List<McLaganEntity> mcLaganEntityList) throws SIBBACBusinessException {
    Integer resultCounter = 0;

    final EntityManager entityManager = entityManagerFactory.createEntityManager();
    final EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
      entityTransaction.begin();

      for (McLaganEntity mcLaganEntity : mcLaganEntityList) {
        if (resultCounter > 0 && resultCounter % commitInterval == 0) {
          try {
            entityTransaction.commit();
            entityTransaction.begin();
            entityManager.clear();
          }
          catch (EntityExistsException | IllegalStateException | TransactionRequiredException
              | RollbackException exception) {
            LOG.error("No se pudo realizar el commit intermedio en MCLAGAN:", exception);
          }
        }

        try {
          entityManager.persist(mcLaganEntity);
          resultCounter++;
        }
        catch (EntityExistsException | IllegalStateException | TransactionRequiredException exception) {
          LOG.error("No se pudo insertar {} en MCLAGAN:", mcLaganEntity, exception);

        }
      }
      entityTransaction.commit();
    }
    catch (IllegalStateException | RollbackException e) {
      if (entityTransaction.isActive()) {
        entityTransaction.rollback();
      }
      throw new SIBBACBusinessException("Error en BulkInsert en MCLAGAN", e);
    }
    finally {
      entityManager.close();
    }

    return resultCounter;
  }

  /**
   * Chequea las fechas y trimestres
   * @param fechaDesde
   * @param fechaHasta
   * @param trim
   * @throws SIBBACBusinessException
   */
  public void checkFechasYTrimestre(Date fechaDesde, Date fechaHasta, Integer trim) throws SIBBACBusinessException {
    final Calendar desde = Calendar.getInstance();
    final Calendar hasta = Calendar.getInstance();

    desde.setTime(fechaDesde);
    hasta.setTime(fechaHasta);

    int anyoDesde = desde.get(Calendar.YEAR);
    int mesDesde = desde.get(Calendar.MONTH) + 1;

    int anyoHasta = hasta.get(Calendar.YEAR);
    int mesHasta = hasta.get(Calendar.MONTH) + 1;

    if (checkFechaHastaMayorQueFechaDesde(fechaDesde, fechaHasta)) {
      if (anyoDesde == anyoHasta) {
        checkTrimestre(mesDesde, mesHasta, trimestre);
      }
      else if (trimestre != 5) {
        throw new SIBBACBusinessException(Constantes.ApplicationMessages.TRIMESTRE_KO.value());
      }
      else {
        LOGGER.debug(Constantes.ApplicationMessages.QUINTO_TRIMESTRE_OK.value());
      }
    }
    else {
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.FECHA_HASTA_MENOR.value());
    }
  }

  private void checkTrimestre(int mesDesde, int mesHasta, int trimestre) throws SIBBACBusinessException {
    // 1er trimestre
    if (mesHasta <= 3 && trimestre == 1) {
      logCheck(Constantes.ApplicationMessages.PRIMER_TRIMESTRE_OK.value());
    }
    // 2o trimestre
    else if ((mesDesde >= 4 && mesHasta <= 6) && trimestre == 2) {
      logCheck(Constantes.ApplicationMessages.SEGUNDO_TRIMESTRE_OK.value());
    }
    // 3er trimestre
    else if ((mesDesde >= 7 && mesHasta <= 9) && trimestre == 3) {
      logCheck(Constantes.ApplicationMessages.TERCER_TRIMESTRE_OK.value());
    }
    // 4o trimestre
    else if ((mesDesde >= 10 && mesHasta <= 12) && trimestre == 4) {
      logCheck(Constantes.ApplicationMessages.CUARTO_TRIMESTRE_OK.value());
    }
    // 5o trimestre
    else if (trimestre == 5) {
      logCheck(Constantes.ApplicationMessages.QUINTO_TRIMESTRE_OK.value());
    }
    // Error de trimestre
    else {
      throw new SIBBACBusinessException(Constantes.ApplicationMessages.TRIMESTRE_KO.value());
    }
  }

  private void logCheck(String mensaje) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(mensaje);
    }
  }

  private boolean checkFechaHastaMayorQueFechaDesde(Date fechaDesde, Date fechaHasta) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Comprobacion fechas que llegan del FRONT");
    }

    Boolean result = fechaHasta.after(fechaDesde) || fechaHasta.equals(fechaDesde);

    if (LOGGER.isDebugEnabled()) {
      if (result) {
        LOGGER.debug(Constantes.ApplicationMessages.DATE_OK.value());
      }
      else {
        LOGGER.debug(Constantes.ApplicationMessages.DATE_KO.value());
      }
    }

    return result;
  }

  /**
   * Operaciones mc lagan.
   *
   * @param solicitud the solicitud
   * @param listadoMcLagan the listado mc lagan
   * @param result the result
   */
  public void setParams(Date fechaDesde, Date fechaHasta, Integer trimestre) {
    this.fechaDesde = fechaDesde;
    this.fechaHasta = fechaHasta;
    this.trimestre = trimestre;
  }
}
