package sibbac.business.mclagan.bo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.dto.ProcessResultDto;
import sibbac.business.mclagan.db.dao.McLaganDAO;
import sibbac.business.mclagan.web.util.Constantes;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.AbstractGenerateFile;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GenerateInstrumentMcLaganFile extends AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(GenerateInstrumentMcLaganFile.class);

  private static final String MCLAGAN_LINE_SEPARATOR = "\n";

  /** Lógica a realizar sobre la BBDD. */
  @Autowired
  private McLaganDAO mclaganDao;
  
  /** Numero de registros totales a escribir en el fichero */
  private Integer fileCountData;

  /**
   * Constructor
   */
  public GenerateInstrumentMcLaganFile() {
    this.fileCountData = null;
  }

  /** Metódo que se ejecutará en el hilo */
  public ProcessResultDto generateInstrumentsFile() throws Exception {

    final ProcessResultDto processResultDto = new ProcessResultDto();

    if (LOG.isDebugEnabled()) {
      LOG.debug("Se inicia la generación del Fichero {}", this.getFileName());
    }

    if (getFileCountData() > 0) {
      try {
        generateFile();
        processResultDto.setResult(true);
        processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_INSTRUMENTOS_OK.value());
      }
      catch (SIBBACBusinessException sibbacBussinessException) {
        processResultDto.setResult(false);
        processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_INSTRUMENTOS_ERROR.value()
            + sibbacBussinessException.getMessage().split(":")[1].trim());
      }
    }
    else {
      processResultDto.setResult(false);
      processResultDto.getResultMessage().add(Constantes.ApplicationMessages.GENERACION_INSTRUMENTOS_EMPTY.value());
    }

    return processResultDto;
  }

  /**
   * Obtiene el separador de linea
   */
  @Override
  public String getLineSeparator() {
    return MCLAGAN_LINE_SEPARATOR;
  }

  /**
   * Obtiene los datos para el fichero
   */
  @Override
  public List<Object[]> getFileData(Pageable pageable) {
    final List<Object[]> results;

    long initTimeStamp = System.currentTimeMillis();

    results = mclaganDao.getListadoFicheroInstrumento(pageable.getOffset(), pageable.getPageSize());

    if (LOG.isDebugEnabled()) {
      LOG.debug("INSTRUMENTS: Select de pagina {} [{}, {}] en {} ms", pageable.getPageNumber(), pageable.getOffset(),
          pageable.getPageSize(), (System.currentTimeMillis() - initTimeStamp));
    }

    return results;
  }

  /**
   * Contamos los datos que vamos a recibir
   */
  @Override
  public Integer getFileCountData() {
    if (this.fileCountData == null) {
      long initTimeStamp = System.currentTimeMillis();

      this.fileCountData = mclaganDao.countListadoFicheroInstrumento();

      if (LOG.isDebugEnabled()) {
        LOG.debug("INSTRUMENTS: Se van a escribir {} elmentos;  {} ms", this.fileCountData,
            (System.currentTimeMillis() - initTimeStamp));
      }
    }

    return this.fileCountData;
  }

  /**
   * Escribe la cabecera del fichero
   */
  @Override
  public void writeHeader(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene cabecera, no aplica");
    }
  }

  /**
   * Procesa los registros del fichero
   * @throws SIBBACBusinessException
   */
  @Override
  public void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException {
    StringBuilder line = new StringBuilder();
    for (Object[] objects : fileData) {
      for (Object object : objects) {
        if (object != null) {
          line.append(object.toString());
        }
        else {
          throw new IOException(Constantes.ApplicationMessages.INSTRUMENTOS_DATO_NULL.value());
        }
      }
      writeLine(outWriter, line.toString());
      line = new StringBuilder();
    }
  }

  /**
   * Escribe el pie de página del fichero
   */
  @Override
  public void writeFooter(BufferedWriter outWriter) throws IOException {
    if (LOG.isDebugEnabled()) {
      LOG.debug("El fichero " + this.getFileName() + " no tiene pie de pagina, no aplica");
    }
  }
}
