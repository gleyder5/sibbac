
package sibbac.business.mclagan.db.model;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.database.DBConstants;

/**
 * The Class McLaganEntity.
 */
@Entity
@Table(name = DBConstants.MCLAGAN.TMCT0_MCLAGAN)
public class McLaganEntity {

  /** Código de Divisa. */
  @Id
  // @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "ID", nullable = false)
  private Integer id;

  /** Nº Operación. */
  @Column(name = "NUOPROUT", nullable = false, length = 9)
  private String nuoprout;

  /** Nº Parcial de Ejecución. */
  @Column(name = "NUPAREJE", nullable = false, length = 4)
  private Integer nupareje;

  /** Código de cliente. */
  @Column(name = "CDCLIENT", nullable = false, length = 8)
  private String cdclient;

  /** Código de producto. */
  @Column(name = "CDPRODUC", length = 3)
  private String cdproduc;

  /** SubAccount Mnemonic. */
  @Column(name = "NUCTAPRO", length = 20)
  private String nuctapro;

  /** Número de cliente. */
  @Column(name = "NUCLIENT", length = 6)
  private Integer nuclient;

  /** Código ISIN. */
  @Column(name = "CDISINVV", length = 12)
  private String cdisinvv;

  /** Descripción del ISIN. */
  @Column(name = "NBVALORS", length = 40)
  private String nbvalors;

  /** Código de Bolsa. */
  @Column(name = "CDBOLSAS", length = 3)
  private String cdbolsas;

  /** Código de Divisa. */
  @Column(name = "CDDIVISA", length = 3)
  private String cddivisa;

  /** Comisión SVB. */
  @Column(name = "COMISION", length = 22)
  private BigDecimal comision;

  /** Fecha de ejecución. */
  @Column(name = "FHEJECUC")
  private Date fhejecuc;

  /** Precio. */
  @Column(name = "IMCAMBIO", length = 17)
  private BigDecimal imcambio;

  /** Nº de títulos ejecutados. */
  @Column(name = "NUCANEJE", length = 25)
  private BigDecimal nucaneje;

  /** Tipo de operación. */
  @Column(name = "TPOPERAC", length = 1)
  private String tpoperac;

  /** Nivel estadístico. */
  @Column(name = "NUNIVEL1", length = 1)
  private String nunivel1;

  /** Código de país. */
  @Column(name = "CDDEPAIS", length = 3)
  private String cddepais;

  /**
   * Nº equivalente al trimestre para el que se está solicitando la información.
   */
  @Column(name = "TRIMESTRE", length = 1)
  private String trimestre;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Gets the nuoprout.
   *
   * @return the nuoprout
   */
  public String getNuoprout() {
    return nuoprout;
  }

  /**
   * Sets the nuoprout.
   *
   * @param nuoprout the new nuoprout
   */
  public void setNuoprout(String nuoprout) {
    this.nuoprout = nuoprout;
  }

  /**
   * Gets the nupareje.
   *
   * @return the nupareje
   */
  public Integer getNupareje() {
    return nupareje;
  }

  /**
   * Sets the nupareje.
   *
   * @param nupareje the new nupareje
   */
  public void setNupareje(Integer nupareje) {
    this.nupareje = nupareje;
  }

  /**
   * Gets the cdclient.
   *
   * @return the cdclient
   */
  public String getCdclient() {
    return cdclient;
  }

  /**
   * Sets the cdclient.
   *
   * @param cdclient the new cdclient
   */
  public void setCdclient(String cdclient) {
    this.cdclient = cdclient;
  }

  /**
   * Gets the cdproduc.
   *
   * @return the cdproduc
   */
  public String getCdproduc() {
    return cdproduc;
  }

  /**
   * Sets the cdproduc.
   *
   * @param cdproduc the new cdproduc
   */
  public void setCdproduc(String cdproduc) {
    this.cdproduc = cdproduc;
  }

  /**
   * Gets the nuctapro.
   *
   * @return the nuctapro
   */
  public String getNuctapro() {
    return nuctapro;
  }

  /**
   * Sets the nuctapro.
   *
   * @param nuctapro the new nuctapro
   */
  public void setNuctapro(String nuctapro) {
    this.nuctapro = nuctapro;
  }

  /**
   * Gets the nuclient.
   *
   * @return the nuclient
   */
  public Integer getNuclient() {
    return nuclient;
  }

  /**
   * Sets the nuclient.
   *
   * @param nuclient the new nuclient
   */
  public void setNuclient(Integer nuclient) {
    this.nuclient = nuclient;
  }

  /**
   * Gets the cdisinvv.
   *
   * @return the cdisinvv
   */
  public String getCdisinvv() {
    return cdisinvv;
  }

  /**
   * Sets the cdisinvv.
   *
   * @param cdisinvv the new cdisinvv
   */
  public void setCdisinvv(String cdisinvv) {
    this.cdisinvv = cdisinvv;
  }

  /**
   * Gets the nbvalors.
   *
   * @return the nbvalors
   */
  public String getNbvalors() {
    return nbvalors;
  }

  /**
   * Sets the nbvalors.
   *
   * @param nbvalors the new nbvalors
   */
  public void setNbvalors(String nbvalors) {
    this.nbvalors = nbvalors;
  }

  /**
   * Gets the cdbolsas.
   *
   * @return the cdbolsas
   */
  public String getCdbolsas() {
    return cdbolsas;
  }

  /**
   * Sets the cdbolsas.
   *
   * @param cdbolsas the new cdbolsas
   */
  public void setCdbolsas(String cdbolsas) {
    this.cdbolsas = cdbolsas;
  }

  /**
   * Gets the cddivisa.
   *
   * @return the cddivisa
   */
  public String getCddivisa() {
    return cddivisa;
  }

  /**
   * Sets the cddivisa.
   *
   * @param cddivisa the new cddivisa
   */
  public void setCddivisa(String cddivisa) {
    this.cddivisa = cddivisa;
  }

  /**
   * Gets the comision.
   *
   * @return the comision
   */
  public BigDecimal getComision() {
    return comision;
  }

  /**
   * Sets the comision.
   *
   * @param comision the new comision
   */
  public void setComision(BigDecimal comision) {
    this.comision = comision;
  }

  /**
   * Gets the fhejecuc.
   *
   * @return the fhejecuc
   */
  public Date getFhejecuc() {
    return fhejecuc;
  }

  /**
   * Sets the fhejecuc.
   *
   * @param fhejecuc the new fhejecuc
   */
  public void setFhejecuc(Date fhejecuc) {
    this.fhejecuc = fhejecuc;
  }

  /**
   * Gets the imcambio.
   *
   * @return the imcambio
   */
  public BigDecimal getImcambio() {
    return imcambio;
  }

  /**
   * Sets the imcambio.
   *
   * @param imcambio the new imcambio
   */
  public void setImcambio(BigDecimal imcambio) {
    this.imcambio = imcambio;
  }

  /**
   * Gets the nucaneje.
   *
   * @return the nucaneje
   */
  public BigDecimal getNucaneje() {
    return nucaneje;
  }

  /**
   * Sets the nucaneje.
   *
   * @param nucaneje the new nucaneje
   */
  public void setNucaneje(BigDecimal nucaneje) {
    this.nucaneje = nucaneje;
  }

  /**
   * Gets the tpoperac.
   *
   * @return the tpoperac
   */
  public String getTpoperac() {
    return tpoperac;
  }

  /**
   * Sets the tpoperac.
   *
   * @param tpoperac the new tpoperac
   */
  public void setTpoperac(String tpoperac) {
    this.tpoperac = tpoperac;
  }

  /**
   * Gets the nunivel 1.
   *
   * @return the nunivel 1
   */
  public String getNunivel1() {
    return nunivel1;
  }

  /**
   * Sets the nunivel 1.
   *
   * @param nunivel1 the new nunivel 1
   */
  public void setNunivel1(String nunivel1) {
    this.nunivel1 = nunivel1;
  }

  /**
   * Gets the cddepais.
   *
   * @return the cddepais
   */
  public String getCddepais() {
    return cddepais;
  }

  /**
   * Sets the cddepais.
   *
   * @param cddepais the new cddepais
   */
  public void setCddepais(String cddepais) {
    this.cddepais = cddepais;
  }

  /**
   * Gets the trimestre.
   *
   * @return the trimestre
   */
  public String getTrimestre() {
    return trimestre;
  }

  /**
   * Sets the trimestre.
   *
   * @param trimestre the new trimestre
   */
  public void setTrimestre(String trimestre) {
    this.trimestre = trimestre;
  }

  /**
   * Convierte una row en entity
   * @param row
   * @return
   */
  public static synchronized McLaganEntity toEntity(Object[] element) {
    McLaganEntity mcLagan = new McLaganEntity();
    if (null != element[0]) {
      mcLagan.setNuoprout(element[0].toString());
    }
    if (null != element[1]) {
      mcLagan.setNupareje(Integer.valueOf(element[1].toString()));
    }
    if (null != element[2]) {
      mcLagan.setCdclient(element[2].toString());
    }
    if (null != element[3]) {
      mcLagan.setCdproduc(element[3].toString());
    }
    if (null != element[4]) {
      mcLagan.setNuctapro(element[4].toString());
    }
    if (null != element[5]) {
      mcLagan.setNuclient(Integer.valueOf(element[5].toString()));
    }
    if (null != element[6]) {
      mcLagan.setCdisinvv(element[6].toString());
    }
    if (null != element[7]) {
      mcLagan.setNbvalors(element[7].toString());
    }
    if (null != element[8]) {
      mcLagan.setCdbolsas(element[8].toString());
    }
    if (null != element[9]) {
      mcLagan.setCddivisa(element[9].toString());
    }
    if (null != element[10]) {
      mcLagan.setComision(new BigDecimal(element[10].toString()));
    }
    if (null != element[11]) {
      mcLagan.setFhejecuc((Date) element[11]);
    }
    if (null != element[12]) {
      mcLagan.setImcambio(new BigDecimal(element[12].toString()));
    }
    if (null != element[13]) {
      mcLagan.setNucaneje(new BigDecimal(element[13].toString()));
    }
    if (null != element[14]) {
      mcLagan.setTpoperac(element[14].toString());
    }
    if (null != element[15]) {
      mcLagan.setNunivel1(element[15].toString());
    }
    if (null != element[16]) {
      mcLagan.setCddepais(element[16].toString());
    }
    if (null != element[17]) {
      mcLagan.setTrimestre(element[17].toString());
    }
    if (null != element[18]) {
      mcLagan.setId((Integer) element[18]);
    }
    return mcLagan;
  }

  @Override
  public String toString() {
    return "McLaganEntity [" + "id=" + id + ";nuoprout=" + nuoprout + ";nupareje=" + nupareje + ";cdclient=" + cdclient
        + ";cdproduc=" + cdproduc + ";nuctapro=" + nuctapro + ";nuclient=" + nuclient + ";cdisinvv=" + cdisinvv
        + ";nbvalors=" + nbvalors + ";cdbolsas=" + cdbolsas + ";cddivisa=" + cddivisa + ";comision=" + comision
        + ";fhejecuc=" + fhejecuc + ";imcambio=" + imcambio + ";nucaneje=" + nucaneje + ";tpoperac=" + tpoperac
        + ";nunivel1=" + nunivel1 + ";cddepais=" + cddepais + ";trimestre=" + trimestre + "]";
  }

}
