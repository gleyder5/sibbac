package sibbac.business.mclagan.web.util;

public interface Constantes<T> {

  public T value();

  /**
   * Mensajes de Logs
   *
   */
  public enum LoggingMessages implements Constantes<String> {

    /** */
    RUNTIME_EXCEPTION_MESSAGE("[executeAction] Error inesperado al intentar ejecutar la accion {}");

    private String value;

    private LoggingMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Mensajes de Aplicacion
   *
   */
  public enum ApplicationMessages implements Constantes<String> {

    UNEXPECTED_MESSAGE("Error inesperado, consulte con administrador"),

    ADD_REQUEST_DATA("Error, los datos obligatorios no han sido rellenados"),

    WRONG_CALL("Llamada a servicio equivocado, consulte con el administrador"),

    OBJECT_NULL("El objeto recibido viene nulo"),

    GENERATED_FILE("El archivo ha sido generado correctamente"),

    OPERATIVA_ERROR(
        "El listado de operativa Nacional y Operativa Internacional están nulos. Parece que ha fallado la conexión con la base de datos."),

    CUENTAS_ERROR(
        "El listado para obtener datos de Cuentas está vacio. No se puede realizar un fichero de TMCT0_MCLAGAN."),

    INSTRUMENTOS_ERROR(
        "El listado para obtener datos de instrumentos está vacio. No se puede realizar un fichero de TMCT0_MCLAGAN."),

    GENERACION_OPERACIONES_ERROR("Error en la extracción información fichero Operaciones: "),

    GENERACION_CUENTAS_ERROR("Error en la extracción información fichero Cuentas: "),

    GENERACION_INSTRUMENTOS_ERROR("Error en la extracción información fichero Instrumentos: "),

    GENERACION_OPERACIONES_OK("Extracción información fichero Operaciones: OK"),

    GENERACION_CUENTAS_OK("Extracción información fichero Cuentas: OK"),

    GENERACION_INSTRUMENTOS_OK("Extracción información fichero Instrumentos: OK"),

    GENERACION_OPERACIONES_EMPTY("Extracción información fichero Operaciones: NO REALIZADA"),

    GENERACION_CUENTAS_EMPTY("Extracción información fichero Cuentas: NO REALIZADA"),

    GENERACION_INSTRUMENTOS_EMPTY("Extracción información fichero Instrumentos: NO REALIZADA"),

    OPERATIVA_OK(
        "El listado de operativa Nacional y/o Operativa Internacional vienen rellenos. Se puede realizar el fichero de TMCT0_MCLAGAN."),

    CUENTAS_OK(
        "El listado para obtener datos de Cuentas viene relleno. Se puede realizar el fichero de TMCT0_MCLAGAN_CUENTAS."),

    INSTRUMENTOS_OK(
        "El listado para obtener datos de instrumentos viene relleno. Se puede realizar el fichero de TMCT0_MCLAGAN."),

    ERROR_PARSEO_OBJECT_TO_ENTITY("Ha ocurrido un error al parsear el listado de objetos a entidades"),

    TRIMESTRE_OK("El trimestre  que llega desde la web corresponde con el que deberia ser"),

    TRIMESTRE_KO("El trimestre que llega desde la web no corresponde con el que deberia ser"),

    FECHA_HASTA_MENOR("La \"fechaHasta\", es menor que la \"fechaDesde\""),

    DATE_OK("La \"fechaHasta\", es mayor que la \"fechaDesde\""),

    DATE_KO("La \"fechaHasta\" va antes que la \"fechaDesde\", es incorrecto."),

    DATE_REQUIRED("Esta tarea necesita que se le dé un periodo de fechas para ser utilizada."),

    DATE_KO_PARSEO("Ha ocurrido un error a la hora de parsear las fechas."),

    PRIMER_TRIMESTRE_OK("Las fechas corresponde con el primer trimestre"),

    SEGUNDO_TRIMESTRE_OK("Las fechas corresponde con el segundo trimestre"),

    TERCER_TRIMESTRE_OK("Las fechas corresponde con el tercer trimestre"),

    CUARTO_TRIMESTRE_OK("Las fechas corresponde con el cuarto trimestre"),

    QUINTO_TRIMESTRE_OK("Las fechas corresponde con el quinto trimestre"),

    OPERACIONES_DATO_NULL("Error en la escritura del fichero Operaciones, se ha encontrado un dato nulo"),

    CUENTAS_DATO_NULL("Error en la escritura del fichero Cuentas, se ha encontrado un dato nulo"),

    INSTRUMENTOS_DATO_NULL("Error en la escritura del fichero Instrumentos, se ha encontrado un dato nulo"),

    MCLAGAN_READY("010"),

    MCLAGAN_IN_PROGRESS("900"),

    MCLAGAN_INACTIVE("999"),

    MCLAGAN_FAILED("000"),

    TASK_IN_PROGRESS("La tarea está en ejecución."),
    
    TASK_IN_BACKGROUND_PROGRESS("La tarea está en segundo plano de ejecución."),

    TASK_INACTIVE("La tarea está inactiva."),

    TASK_FAILED("La ejecucion ha fallado. No se puede volver a ejecutar hasta que se cambie de nuevo el estado a "
        + Constantes.ApplicationMessages.MCLAGAN_READY.value()
        + " al acabar la ejecución actual o si persiste, de forma manual."),

    MCLAGAN_TASK("Envio ficheros McLagan");

    private String value;

    private ApplicationMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return value;
    }
  }

  /**
   * Mensajes de Aplicacion
   *
   */
  public enum TipoOperativa implements Constantes<Integer> {

    OPERATIVA_NACIONAL(0),

    OPERATIVA_INTERNACIONAL(1);

    private Integer value;

    TipoOperativa(Integer value) {
      this.value = value;
    }

    @Override
    public Integer value() {
      return value;
    }
  }
}
