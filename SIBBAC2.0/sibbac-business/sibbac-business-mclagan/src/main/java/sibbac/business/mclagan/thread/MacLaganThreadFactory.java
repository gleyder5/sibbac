package sibbac.business.mclagan.thread;

import java.util.concurrent.ThreadFactory;

public class MacLaganThreadFactory implements ThreadFactory {

    private int count = 0;

    private String namePool;

    public MacLaganThreadFactory(String namePool) {
      this.namePool = namePool;
    }

    @Override
    public Thread newThread(Runnable r) {
      final Thread thread = new Thread(r);
      thread.setName(String.format("SIBBAC_" + this.namePool + "-%d", count++));
      return thread;
    }
  }