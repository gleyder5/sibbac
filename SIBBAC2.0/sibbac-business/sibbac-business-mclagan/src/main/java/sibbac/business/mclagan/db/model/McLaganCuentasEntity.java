
package sibbac.business.mclagan.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import sibbac.database.DBConstants;

/**
 * The Class McLaganCuentasEntity.
 */
@Entity
@Table(name = DBConstants.MCLAGAN.TMCT0_MCLAGAN_CUENTAS)
public class McLaganCuentasEntity {

  /** Código de Divisa. */
  @Id
  @Column(name = "ID", nullable = false)
  private Integer id;

  /** The accountId. */
  @Column(name = "ACCOUNT_ID", nullable = false, length = 8)
  private String accountId;

  /** The accountName. */
  @Column(name = "ACCOUNT_NAME", nullable = true, length = 60)
  private String accountName;

  /** The accountCountry */
  @Column(name = "ACCOUNT_COUNTRY", nullable = true, length = 30)
  private String accountCountry;

  /** The parentId. */
  @Column(name = "PARENT_ID", nullable = false, length = 6)
  private Long parentId;

  /** The parentName. */
  @Column(name = "PARENT_NAME", nullable = true, length = 60)
  private String parentName;

  /**
   * Gets the account Id.
   *
   * @return the account Id
   */
  public String getAccountId() {
    return accountId;
  }

  /**
   * Sets the account Id.
   *
   * @return account Id the new account Id
   */
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * Gets the account Name.
   *
   * @return the account Name
   */
  public String getAccountName() {
    return accountName;
  }

  /**
   * Sets the account Name.
   *
   * @param account Name the new account Name
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * Gets the account Country.
   *
   * @return the account Country
   */
  public String getAccountCountry() {
    return accountCountry;
  }

  /**
   * Sets the account Country.
   *
   * @param account Country the new account Country
   */
  public void setAccountCountry(String accountCountry) {
    this.accountCountry = accountCountry;
  }

  /**
   * Gets the parent Id.
   *
   * @return the parent Id
   */
  public Long getParentId() {
    return parentId;
  }

  /**
   * Sets the parent Id.
   *
   * @param parent Id the new parent Id
   */
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  /**
   * Gets the parent Name.
   *
   * @return the parent Name
   */
  public String getParentName() {
    return parentName;
  }

  /**
   * Sets the parent Name.
   *
   * @param parent Name the new parent Name
   */
  public void setParentName(String parentName) {
    this.parentName = parentName;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Convierte una row en entity
   * @param row
   * @return
   */
  public static synchronized McLaganCuentasEntity toEntity(Object[] element) {
    McLaganCuentasEntity mcLaganCuentas = new McLaganCuentasEntity();
    if (null != element[0]) {
      mcLaganCuentas.setAccountId(element[0].toString());
    }
    if (null != element[1]) {
      mcLaganCuentas.setAccountName(element[1].toString());
    }
    if (null != element[2]) {
      mcLaganCuentas.setAccountCountry(element[2].toString());
    }
    if (null != element[3]) {
      mcLaganCuentas.setParentId(Long.parseLong(element[3].toString()));
    }
    if (null != element[4]) {
      mcLaganCuentas.setParentName(element[4].toString());
    }
    if (null != element[5]) {
      mcLaganCuentas.setId((Integer) element[5]);
    }
    return mcLaganCuentas;
  }

  @Override
  public String toString() {
    return "McLaganCuentasEntity [" + "id=" + id + ";accountId=" + accountId + ";accountName=" + accountName
        + ";accountCountry=" + accountCountry + ";parentId=" + parentId + "]";
  }

}
