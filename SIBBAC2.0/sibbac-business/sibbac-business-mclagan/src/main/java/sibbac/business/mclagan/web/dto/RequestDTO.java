package sibbac.business.mclagan.web.dto;

/**
 * The Class RequestDTO.
 */
public class RequestDTO {

  /** The fecha desde. */
  private String fechaDesde;

  /** The fecha hasta. */
  private String fechaHasta;

  /** The trimestr. */
  private String trimestre;

  /**
   * Instantiates a new request DTO.
   */
  public RequestDTO() {
    super();
  }

  /**
   * Instantiates a new request DTO.
   *
   * @param fechaDesde the fecha desde
   * @param fechaHasta the fecha hasta
   * @param trimestre the trimestre
   */
  public RequestDTO(String fechaDesde, String fechaHasta, String trimestre) {
    super();
    this.fechaDesde = fechaDesde;
    this.fechaHasta = fechaHasta;
    this.trimestre = trimestre;
  }

  /**
   * Gets the fecha desde.
   *
   * @return the fecha desde
   */
  public String getFechaDesde() {
    return fechaDesde;
  }

  /**
   * Sets the fecha desde.
   *
   * @param fechaDesde the new fecha desde
   */
  public void setFechaDesde(String fechaDesde) {
    this.fechaDesde = fechaDesde;
  }

  /**
   * Gets the fecha hasta.
   *
   * @return the fecha hasta
   */
  public String getFechaHasta() {
    return fechaHasta;
  }

  /**
   * Sets the fecha hasta.
   *
   * @param fechaHasta the new fecha hasta
   */
  public void setFechaHasta(String fechaHasta) {
    this.fechaHasta = fechaHasta;
  }

  /**
   * Gets the trimestre.
   *
   * @return the trimestre
   */
  public String getTrimestre() {
    return trimestre;
  }

  /**
   * Sets the trimestre.
   *
   * @param trimestre the new trimestre
   */
  public void setTrimestre(String trimestre) {
    this.trimestre = trimestre;
  }

}
