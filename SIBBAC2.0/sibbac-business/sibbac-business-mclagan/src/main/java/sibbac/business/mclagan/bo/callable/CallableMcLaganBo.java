package sibbac.business.mclagan.bo.callable;

import java.util.Date;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.McLaganBo;
import sibbac.business.mclagan.bo.dto.SelectInsertCounterDto;
import sibbac.business.mclagan.web.util.Constantes.TipoOperativa;
import sibbac.common.SIBBACBusinessException;

/**
 * The Class MclaganBo.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableMcLaganBo implements Callable<SelectInsertCounterDto> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(CallableMcLaganBo.class);

  @Autowired
  private McLaganBo mcLaganBo;

  private Date fechaDesde;

  private Date fechaHasta;

  private Integer trimestre;

  private TipoOperativa tipoOperativa;

  @Override
  public SelectInsertCounterDto call() throws SIBBACBusinessException {
      final SelectInsertCounterDto selectInsertCounterDto;
    
    if (tipoOperativa == TipoOperativa.OPERATIVA_NACIONAL) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Se lanza el proceso Insert/Select de MacLagan de la operativa Nacional con hilos");
      }

      selectInsertCounterDto = mcLaganBo.selectInsertOperativaNacionalPreparePagination(fechaDesde, fechaHasta, trimestre);
      selectInsertCounterDto.setProcessName(TipoOperativa.OPERATIVA_NACIONAL.name());
      
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Se termina el proceso Insert/Select de MacLagan de la operativa Nacional");
      }

    }
    else if (tipoOperativa == TipoOperativa.OPERATIVA_INTERNACIONAL) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Se lanza el proceso Insert/Select de MacLagan de la operativa Internacional");
      }

      selectInsertCounterDto = mcLaganBo.selectInsertOperativaInteracionalPreparePagination(fechaDesde, fechaHasta, trimestre);
      selectInsertCounterDto.setProcessName(TipoOperativa.OPERATIVA_INTERNACIONAL.name());
      
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Se termina el proceso Insert/Select de MacLagan de la operativa Internacional");
      }
    
    } else {
      selectInsertCounterDto = new SelectInsertCounterDto();
    }

    return selectInsertCounterDto;
  }

  /**
   * Operaciones mc lagan.
   *
   * @param solicitud the solicitud
   * @param listadoMcLagan the listado mc lagan
   * @param result the result
   */
  public void setParams(Date fechaDesde, Date fechaHasta, Integer trimestre) {
    this.fechaDesde = fechaDesde;
    this.fechaHasta = fechaHasta;
    this.trimestre = trimestre;
  }

  public TipoOperativa getTipoOperativa() {
    return tipoOperativa;
  }

  public void setTipoOperativa(TipoOperativa tipoOperativa) {
    this.tipoOperativa = tipoOperativa;
  }

}
