package sibbac.business.mclagan.bo.callable;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.business.mclagan.bo.GenerateInstrumentMcLaganFile;
import sibbac.business.mclagan.bo.dto.ProcessResultDto;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CallableGenerateInstrumentMcLaganFile implements Callable<ProcessResultDto> {

  private static final Logger LOG = LoggerFactory.getLogger(CallableGenerateInstrumentMcLaganFile.class);

  @Autowired
  private GenerateInstrumentMcLaganFile generateInstrumentMcLaganFile;

  /** Metódo que se ejecutará en el hilo */
  @Override
  public ProcessResultDto call() throws Exception {
    if (LOG.isDebugEnabled()) {
      LOG.debug("Se lanza el proceso en background de Instrument File");
    }

    final ProcessResultDto processResultDto = generateInstrumentMcLaganFile.generateInstrumentsFile();

    return processResultDto;
  }

  public GenerateInstrumentMcLaganFile getGenerateInstrumentMcLaganFile() {
    return generateInstrumentMcLaganFile;
  }

  public void setGenerateInstrumentMcLaganFile(GenerateInstrumentMcLaganFile generateInstrumentMcLaganFile) {
    this.generateInstrumentMcLaganFile = generateInstrumentMcLaganFile;
  }

}
