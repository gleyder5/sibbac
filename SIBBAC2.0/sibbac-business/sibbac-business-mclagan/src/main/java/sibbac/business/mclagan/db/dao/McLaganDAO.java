package sibbac.business.mclagan.db.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.mclagan.db.model.McLaganEntity;
import sibbac.database.DBConstants;

/**
 * The Interface McLaganDAO.
 */
@Repository
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public interface McLaganDAO extends JpaRepository<McLaganEntity, BigInteger> {

  static final String PAGEABLE = " LIMIT ?1, ?2";

  /**
   * Query nacional
   */
  public static final String OPERATIVA_NACIONAL_QUERY = 
      "SELECT "
      + "   SUBSTR(ALC.NBOOKING,3,9) AS NUOPROUT,"
      + "   ALC.NUCNFLIQ AS NUPAREJE," 
      + "   '008'||SUBSTR(DIGITS(DECIMAL(BOK.CDALIAS, 5, 0)), 1, 5)  AS CDCLIENT, "
      + "   '008' AS CDPRODUC, " 
      + "   BOK.CDALIAS AS NUCTAPRO, "
      + "   DIGITS(CLI.NUCLIENT) AS NUCLIENT, "
      + "   BOK.CDISIN AS CDISINVV, "
      + "   BOK.NBVALORS AS NBVALORS, "
      + "   CASE DCT.CDMIEMBROMKT WHEN '9838' THEN 'M  ' WHEN '9370' THEN 'V  ' WHEN '9472' THEN 'B  ' WHEN '9577' THEN 'BI' ELSE '    ' END AS CDBOLSAS, "
      + "   DCT.CDDIVISA, "
      + "   CASE SUM(DCT.IMCOMSVB) WHEN  0 THEN SUM(DCT.IMCOMBCO) ELSE SUM(DCT.IMCOMSVB)  END AS COMISION, "
      + "   ALC.FEEJELIQ AS FHEJECUC, " 
      + "   DCT.IMPRECIO AS IMCAMBIO, " 
      + "   DCT.IMTITULOS AS NUCANEJE, "
      + "   BOK.CDTPOPER AS TPOPERAC, " 
      + "   '0' AS NUNIVEL1, "
      + "   SUBSTR(AFI.CDDEPAIS, 1, 3) AS CDDEPAIS, " 
      + "   CAST(:trimestre AS VARCHAR(1)) AS TRIMESTRE, "
      + "   (NEXT VALUE FOR TMCT0_MCLAGAN_SEQ) AS ID" // Aumenta el rendimiento precalculamos el ID mediante una secuencia
      + " FROM "
      + "   (SELECT BOK.NUORDEN, BOK.NBOOKING, BOK.CDISIN, BOK.NBVALORS, BOK.CDTPOPER, BOK.CDALIAS FROM TMCT0BOK BOK WHERE BOK.FETRADE BETWEEN :fechaDesde AND :fechaHasta AND BOK.CDESTADOASIG > 0 AND BOK.CDALIAS NOT IN ('4716', '22645')) BOK "
      + "   INNER JOIN (SELECT ALC.NUORDEN, ALC.NBOOKING, ALC.NUCNFCLT, ALC.NUCNFLIQ, ALC.FEEJELIQ FROM TMCT0ALC ALC WHERE ALC.FEEJELIQ BETWEEN :fechaDesde AND :fechaHasta AND ALC.CDESTADOASIG > 0 AND ALC.NUTITLIQ > 0) ALC ON BOK.NUORDEN=ALC.NUORDEN AND BOK.NBOOKING=ALC.NBOOKING "
      + "   INNER JOIN (SELECT ALI.CDALIASS, CLI.NUCLIENT FROM TMCT0ALI ALI INNER JOIN TMCT0CLI CLI ON ALI.CDBROCLI=CLI.CDBROCLI WHERE ALI.FHFINAL >= TO_CHAR(CURRENT DATE,'yyyymmdd') AND CLI.NUCLIENT NOT IN (14158, 21012, 16699, 16700,20863, 55108, 55109, 16701,19211, 2125, 14193)) CLI ON CLI.CDALIASS=BOK.CDALIAS "
      + "   INNER JOIN (SELECT DCM.NUORDEN, DCM.NBOOKING, DCM.NUCNFCLT, DCM.NUCNFLIQ, DCM.IDDESGLOSECAMARA FROM TMCT0DESGLOSECAMARA DCM WHERE DCM.FECONTRATACION BETWEEN :fechaDesde AND :fechaHasta AND DCM.CDESTADOASIG >= 65) DCM ON ALC.NUORDEN=DCM.NUORDEN AND ALC.NBOOKING=DCM.NBOOKING AND ALC.NUCNFCLT=DCM.NUCNFCLT AND ALC.NUCNFLIQ=DCM.NUCNFLIQ "
      + "   INNER JOIN TMCT0DESGLOSECLITIT DCT ON DCM.IDDESGLOSECAMARA=DCT.IDDESGLOSECAMARA  "
      + "   INNER JOIN TMCT0EST EST ON BOK.CDALIAS=EST.CDALIASS AND EST.FHFINAL >=TO_CHAR(CURRENT DATE,'yyyymmdd') "
      + "   INNER JOIN (SELECT ONI.NUORDEN0, ONI.NUORDEN1, ONI.NUORDEN2, ONI.NUORDEN3, ONI.NUORDEN4 FROM TMCT0ONI ONI WHERE ONI.CDNIVEL0='RVN' AND ONI.CDNIVEL1 <>'MIN') ONI ON EST.STLEV0LM = ONI.NUORDEN0 AND EST.STLEV1LM=ONI.NUORDEN1 AND EST.STLEV2LM = ONI.NUORDEN2 AND EST.STLEV3LM = ONI.NUORDEN3 AND EST.STLEV4LM = ONI.NUORDEN4 "
      + "   INNER JOIN LATERAL(SELECT AFI.NUORDEN, AFI.NBOOKING, AFI.NUCNFCLT, AFI.NUCNFLIQ, AFI.CDDEPAIS FROM TMCT0AFI AFI WHERE ALC.NUORDEN=AFI.NUORDEN AND ALC.NBOOKING=AFI.NBOOKING AND ALC.NUCNFCLT=AFI.NUCNFCLT AND ALC.NUCNFLIQ=AFI.NUCNFLIQ AND AFI.CDINACTI='A' FETCH FIRST 1 ROW ONLY) AFI ON TRUE "
      + " GROUP BY " 
      + "   ALC.NUORDEN, "
      + "   ALC.NBOOKING," 
      + "   ALC.NUCNFCLT, " 
      + "   ALC.NUCNFLIQ, " 
      + "   BOK.CDALIAS, "
      + "   CLI.NUCLIENT, "
      + "   BOK.CDISIN, " 
      + "   BOK.NBVALORS, " 
      + "   DCT.CDDIVISA, " 
      + "   ALC.FEEJELIQ, "
      + "   DCT.CDMIEMBROMKT, " 
      + "   ALC.FEEJELIQ, " 
      + "   DCT.IMPRECIO, " 
      + "   DCT.IMTITULOS, " 
      + "   BOK.CDTPOPER, "
      + "   AFI.CDDEPAIS";

  /**
   * Query Internacional
   */
  public static final String OPERATIVA_INTERNACIONAL_QUERY = 
      "SELECT "
      + "   SUBSTR(ALO.NBOOKING,3,9) AS NUOPROUT, "
      + "   ALO.NUPAREJE AS NUPAREJE, "
      + "   SUBSTR(DIGITS(DECIMAL(BOK.CDALIAS, 5, 0)), 1, 5)  AS CDCLIENT, "
      + "   '008' AS CDPRODUC, "
      + "   BOK.CDALIAS AS NUCTAPRO, "
      + "   DIGITS(CLI.NUCLIENT) AS NUCLIENT,  "
      + "   BOK.CDISIN AS CDISINVV, "
      + "   BOK.NBVALORS AS NBVALORS, "
      + "   ORD.CDMERCAD AS CDBOLSAS, "
      + "   ALO.CDMONISO AS CDDIVISA, "
      + "   SUM(ALO.IMSVBEU) AS COMISION, "
      + "   BOK.FEEJECUC AS FHEJECUC, "
      + "   ALO.IMCAMMED AS IMCAMBIO, "
      + "   ALO.NUTITCLI AS NUCANEJE, "
      + "   BOK.CDTPOPER AS TPOPERAC, "
      + "   '0' AS NUNIVEL1, "
      + "   SUBSTR(COALESCE(AFI.CDDEPAIS,'   '), 1, 3) AS CDDEPAIS, "
      + "   CAST(:trimestre AS VARCHAR(1)) AS TRIMESTRE,  "
      + "   (NEXT VALUE FOR TMCT0_MCLAGAN_SEQ) AS ID" // Aumenta el rendimiento precalculamos el ID mediante una secuencia
      + " FROM  "
      + "   (SELECT BOK.NUORDEN, BOK.NBOOKING, BOK.CDALIAS, BOK.FEEJECUC, BOK.CDISIN, BOK.NBVALORS, BOK.CDTPOPER FROM TMCT0BOK BOK WHERE BOK.FETRADE BETWEEN :fechaDesde AND :fechaHasta AND BOK.CDESTADO BETWEEN '300' AND '450' AND BOK.CDALIAS NOT IN ('4716', '22645')) BOK "
      + "   INNER JOIN (SELECT ALO.NUORDEN, ALO.NBOOKING, ALO.NUCNFCLT, ALO.NUPAREJE, ALO.CDMONISO, ALO.IMCAMMED, ALO.NUTITCLI, ALO.IMSVBEU FROM TMCT0ALO ALO WHERE ALO.FEOPERAC BETWEEN :fechaDesde AND :fechaHasta AND ALO.CDESTADO BETWEEN '300' AND '450' ) ALO ON BOK.NUORDEN=ALO.NUORDEN AND BOK.NBOOKING=ALO.NBOOKING "
      + "   INNER JOIN (SELECT ALI.CDALIASS, CLI.NUCLIENT FROM TMCT0ALI ALI  "
      + "   INNER JOIN TMCT0CLI CLI ON ALI.CDBROCLI=CLI.CDBROCLI WHERE ALI.FHFINAL >= TO_CHAR(CURRENT DATE,'yyyymmdd') AND CLI.NUCLIENT NOT IN (14158, 21012, 16699, 16700,20863, 55108, 55109, 16701,19211, 2125, 14193)) CLI ON CLI.CDALIASS=BOK.CDALIAS "
      + "   INNER JOIN TMCT0EST EST ON BOK.CDALIAS=EST.CDALIASS AND EST.FHFINAL >=TO_CHAR(CURRENT DATE,'yyyymmdd')  "
      + "   INNER JOIN (SELECT ONI.NUORDEN0, ONI.NUORDEN1, ONI.NUORDEN2, ONI.NUORDEN3, ONI.NUORDEN4 FROM TMCT0ONI ONI WHERE ONI.CDNIVEL1 <>'MIN') ONI ON EST.STLEV0LM = ONI.NUORDEN0 AND EST.STLEV1LM=ONI.NUORDEN1 AND EST.STLEV2LM = ONI.NUORDEN2 AND EST.STLEV3LM = ONI.NUORDEN3 AND EST.STLEV4LM = ONI.NUORDEN4 "
      + "   INNER JOIN TMCT0ORD ORD ON ORD.NUORDEN = BOK.NUORDEN AND ORD.CDCLSMDO = 'E'"
      + "   LEFT JOIN LATERAL (SELECT AFI.NUORDEN, AFI.NBOOKING, AFI.NUCNFCLT, AFI.NUCNFLIQ, AFI.CDDEPAIS FROM TMCT0AFI AFI WHERE ALO.NUORDEN=AFI.NUORDEN AND ALO.NBOOKING=AFI.NBOOKING AND ALO.NUCNFCLT=AFI.NUCNFCLT AND AFI.NUCNFLIQ = 0 AND AFI.CDINACTI='A' FETCH FIRST 1 ROW ONLY) AFI ON TRUE "
      + " GROUP BY  "
      + "   ALO.NUORDEN, "
      + "   ALO.NBOOKING, "
      + "   ALO.NUCNFCLT, "
      + "   ALO.NUPAREJE, "
      + "   BOK.CDALIAS, "
      + "   CLI.NUCLIENT, "
      + "   BOK.CDISIN,  "
      + "   BOK.NBVALORS, "
      + "   ALO.CDMONISO, "
      + "   BOK.FEEJECUC, "
      + "   ALO.IMCAMMED, "
      + "   ALO.NUTITCLI, "
      + "   BOK.CDTPOPER, "
      + "   AFI.CDDEPAIS, " 
      + "   ORD.CDMERCAD ";
  
  /**
   * Query de Instrumentos
   */
  public static final String INSTRUMENTOS_QUERY = 
      "WITH TMCT0_MCLAGAN_MAX (CDISINVV, MAX_FHEJECUC) AS ( " + 
      "       SELECT " + 
      "             CDISINVV, MAX(FHEJECUC) " + 
      "       FROM " + 
      "             TMCT0_MCLAGAN " + 
      "       GROUP BY " + 
      "             CDISINVV " + 
      ") " + 
      "SELECT " + 
      "   RPAD(COALESCE(MC1.CDISINVV, ' '), 13, ' '), " + 
      "   RPAD('ISIN',18, ' '), " + 
      "   RPAD(MC1.NBVALORS, 40, ' '), " + 
      "   RPAD(COALESCE(MC1.CDISINVV, ' '), 13, ' '), " + 
      "   RPAD('ISIN',18, ' ') " + 
      "FROM " + 
      "       TMCT0_MCLAGAN MC1 " + 
      "       INNER JOIN TMCT0_MCLAGAN_MAX MC2 ON MC2.CDISINVV = MC1.CDISINVV AND MC2.MAX_FHEJECUC = MC1.FHEJECUC " + 
      "GROUP BY " +
      "       MC1.CDISINVV, MC1.NBVALORS";
  
  /**
   * Query Bulk Insert de operativa nacional
   */
  public static final String MCLAGAN_OPERATIVA_NACIONAL_BULK_INSERT = "INSERT INTO TMCT0_MCLAGAN "
      + "(NUOPROUT, NUPAREJE, CDCLIENT, CDPRODUC, NUCTAPRO, NUCLIENT, CDISINVV, NBVALORS, CDBOLSAS, CDDIVISA, COMISION, FHEJECUC, IMCAMBIO, NUCANEJE, TPOPERAC, NUNIVEL1, CDDEPAIS, TRIMESTRE, ID)"
      + "(" + OPERATIVA_NACIONAL_QUERY + ")";

  /**
   * Query Bulk Insert de operativa internacional
   */
  public static final String MCLAGAN_OPERATIVA_INTERNACIONAL_BULK_INSERT = "INSERT INTO TMCT0_MCLAGAN "
      + "(NUOPROUT, NUPAREJE, CDCLIENT, CDPRODUC, NUCTAPRO, NUCLIENT, CDISINVV, NBVALORS, CDBOLSAS, CDDIVISA, COMISION, FHEJECUC, IMCAMBIO, NUCANEJE, TPOPERAC, NUNIVEL1, CDDEPAIS, TRIMESTRE, ID)"
      + "(" + OPERATIVA_INTERNACIONAL_QUERY + ")";

  public static final String OPERATIONS_QUERY = "SELECT RPAD(CDCLIENT, 10, ' '), RPAD(CDISINVV, 13, ' '), "
      + " RPAD('ETC', 9, ' '), RPAD(CDDIVISA, 8, ' '), RPAD(REPLACE(COMISION, '.', ','), 15, ' '),"
      + " RPAD(TRIMESTRE, 7, ' '), SUBSTR(FHEJECUC, 1, 4), TO_CHAR(FHEJECUC, 'YYYYMMDD'),"
      + " RPAD(REPLACE(IMCAMBIO, '.', ','), 15, ' '), RPAD(REPLACE(NUCANEJE, '.', ','), 15, ' '),"
      + " RPAD(CASE TRIM(TPOPERAC) WHEN 'C' THEN 'BUY' WHEN 'V' THEN 'SELL' END, 8, ' '),'COMMISSION', "
      + " RPAD(NUCLIENT, 10, ' ') FROM TMCT0_MCLAGAN";

  public static final String OPERATIONS_PAGE_QUERY = OPERATIONS_QUERY + PAGEABLE;

  public static final String OPERATIONS_COUNT_QUERY = "SELECT COUNT(*) FROM (" + OPERATIONS_QUERY + ")";

  public static final String INSTRUMENTOS_PAGE_QUERY = INSTRUMENTOS_QUERY + PAGEABLE;

  public static final String INSTRUMENTOS_COUNT_QUERY =  
      "WITH TMCT0_MCLAGAN_MAX (CDISINVV, MAX_FHEJECUC) AS ( " + 
      "       SELECT " + 
      "             CDISINVV, MAX(FHEJECUC) " + 
      "       FROM " + 
      "             TMCT0_MCLAGAN " + 
      "       GROUP BY " + 
      "             CDISINVV " + 
      ") " + 
      "SELECT COUNT(*) FROM ( " +
      "SELECT " + 
      "   RPAD(COALESCE(MC1.CDISINVV, ' '), 13, ' '), " + 
      "   RPAD('ISIN',18, ' '), " + 
      "   RPAD(MC1.NBVALORS, 40, ' '), " + 
      "   RPAD(COALESCE(MC1.CDISINVV, ' '), 13, ' '), " + 
      "   RPAD('ISIN',18, ' ') " + 
      "FROM " + 
      "       TMCT0_MCLAGAN MC1 " + 
      "       INNER JOIN TMCT0_MCLAGAN_MAX MC2 ON MC2.CDISINVV = MC1.CDISINVV AND MC2.MAX_FHEJECUC = MC1.FHEJECUC " + 
      "GROUP BY MC1.CDISINVV, MC1.NBVALORS" +
      " )";
  
  public static final String DELETE_TABLE_MAC_LAGAN = "TRUNCATE TABLE " + DBConstants.MCLAGAN.TMCT0_MCLAGAN
      + " IMMEDIATE";

  /**
   * Obtiene los registros para la operativa nacional
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   */
  @Query(value = OPERATIVA_NACIONAL_QUERY, nativeQuery = true)
  List<Object[]> getListadoOperativaNacional(@Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta, @Param("trimestre") Integer trimestre);

  /**
   * Obtiene los registros para la operativa internacional
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   */
  @Query(value = OPERATIVA_INTERNACIONAL_QUERY, nativeQuery = true)
  List<Object[]> getListadoOperativaInternacional(@Param("fechaDesde") Date fechaDesde,
      @Param("fechaHasta") Date fechaHasta, @Param("trimestre") Integer trimestre);

  /**
   * Bulk Insert en MccLagan de los registros de la operativa internacional
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   */
  @Modifying
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  @Query(value = MCLAGAN_OPERATIVA_NACIONAL_BULK_INSERT, nativeQuery = true)
  Integer bulkInsertOperativaNacional(@Param("fechaDesde") Date fechaDesde,
  @Param("fechaHasta") Date fechaHasta, @Param("trimestre") Integer trimestre);

  
  /**
   * Bulk Insert en MccLagan de los registros de la operativa internacional
   * @param fechaDesde
   * @param fechaHasta
   * @param trimestre
   * @return
   */
  @Modifying
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, timeout = 10000, propagation = Propagation.REQUIRED)
  @Query(value = MCLAGAN_OPERATIVA_INTERNACIONAL_BULK_INSERT, nativeQuery = true)
  Integer bulkInsertOperativaInternacional(@Param("fechaDesde") Date fechaDesde,
  @Param("fechaHasta") Date fechaHasta, @Param("trimestre") Integer trimestre);
  
  /**
   * Obtiene el numero registros de operaciones
   * @return
   */
  @Query(value = OPERATIONS_COUNT_QUERY, nativeQuery = true)
  Integer countAllOperations();

  /**
   * Obtiene toda las operaciones
   * @param offset
   * @param pageSize
   * @return
   */
  @Query(value = OPERATIONS_PAGE_QUERY, nativeQuery = true)
  List<Object[]> findAllOperations(Integer offset, Integer pageSize);

  /**
   * Obtiene el numero de registros de instrumentos
   * @return
   */
  @Query(value = INSTRUMENTOS_COUNT_QUERY, nativeQuery = true)
  Integer countListadoFicheroInstrumento();
  
  /**
   * Obtiene los registros de instrumentos
   * @param offset
   * @param pageSize
   * @return
   */
  @Query(value = INSTRUMENTOS_PAGE_QUERY, nativeQuery = true)
  List<Object[]> getListadoFicheroInstrumento(Integer offset, Integer pageSize);

  @Transactional
  @Modifying
  @Query(value = DELETE_TABLE_MAC_LAGAN, nativeQuery = true)
  void cleanTable();
}