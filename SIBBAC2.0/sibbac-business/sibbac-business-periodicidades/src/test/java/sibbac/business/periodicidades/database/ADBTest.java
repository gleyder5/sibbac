package sibbac.business.periodicidades.database;


import javax.annotation.PostConstruct;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.periodicidades.database.bo.DiaSemanaBo;
import sibbac.business.periodicidades.database.bo.MesBo;
import sibbac.business.periodicidades.database.bo.ParametrizacionPeriodicidadBo;
import sibbac.business.periodicidades.database.bo.ReglaPeriodicidadBo;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.test.BaseTest;


public abstract class ADBTest extends BaseTest {

	protected static final Logger			LOG								= LoggerFactory.getLogger( ADBTest.class );

	protected static boolean				testDataInjected				= false;

	protected String						prefix;

	// Periodicidades
	@Autowired
	protected DiaSemanaBo					boDiaSemana						= null;

	@Autowired
	protected FestivoBo						boFestivo						= null;

	@Autowired
	protected MesBo							boMes							= null;

	@Autowired
	protected ParametrizacionPeriodicidadBo	boParametrizacionPeriodicidad	= null;

	@Autowired
	protected ReglaPeriodicidadBo			boReglaPeriodicidad				= null;

	public ADBTest() {
	}

	@PostConstruct
	public void init() {
		String prefix = "[ADBTest::init(@PostConstruct) ] ";
		if ( !testDataInjected ) {
			LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
			testDataInjected = true;
		}
	}

	@Before
	public void before() {
		this.prefix = "";
	}

}
