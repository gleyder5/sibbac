package sibbac.business.periodicidades.database;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.wrappers.database.bo.FacturaBo;
import sibbac.business.wrappers.database.model.Factura;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestCreateExcel extends ADBTest {
	@Autowired private FacturaBo facturaBo;

	@Transactional(propagation=Propagation.REQUIRED)
	@Test public void testCreateExcel() throws IOException {
		InputStream inputStream;
		OutputStream outputStream = null;
		int read;
		byte[] bytes = new byte[1024];
		for (Factura factura: facturaBo.getDao().findAll()) {
			try {
				inputStream = facturaBo.crearExcel(factura);
				outputStream = new FileOutputStream("Factura_" + factura.getId() + ".xls");
				while ((read = inputStream.read(bytes))!=-1) {
					outputStream.write(bytes, 0, read);
				}
			} finally {
				if (outputStream!=null) {
					outputStream.close();
				}
			}
		}
		Assert.assertTrue("Generados los ficheros excel con las facturas", true);
	}
}
