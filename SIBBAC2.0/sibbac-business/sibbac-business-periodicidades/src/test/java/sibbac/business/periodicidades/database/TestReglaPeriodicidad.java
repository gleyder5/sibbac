package sibbac.business.periodicidades.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.sql.Date;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestReglaPeriodicidad extends ADBTest {

	private static Long	idBetaTester	= 1L;

	@Test
	// @Transactional( propagation = Propagation.REQUIRED )
			public
			void test01AltaDeReglaPeriodicidad() {
		// Fecha
		java.util.Calendar cal = java.util.Calendar.getInstance();
		java.util.Date utilDate = cal.getTime();
		java.sql.Date sqlDate = new Date( utilDate.getTime() );
		//
		assumeNotNull( boReglaPeriodicidad );

		long total = boReglaPeriodicidad.count();
		LOG.debug( "Ahora mismo, hay {} regla(s) de periodicidad definidas.", total );

		LOG.debug( "Salvando una regla de periodicidad..." );
		idBetaTester = this.altaDeReglaPeriodicidad( "ID_REGLA " + total, "Una regla", sqlDate, sqlDate, true );
		LOG.debug( "ID ReglaPeriodicidad BetaTester: " + idBetaTester );

		LOG.debug( "Salvando otra regla de periodicidad..." );
		idBetaTester = this.altaDeReglaPeriodicidad( "ID_REGLA " + ( total + 1 ), "Otra regla", sqlDate, sqlDate, true );
		LOG.debug( "ID ReglaPeriodicidad BetaTester: " + idBetaTester );

		LOG.debug( "Mostrando la reglas de periodicidad..." );
		this.listaReglaPeriodicidades();
	}

	@Test
	// @Transactional( propagation = Propagation.REQUIRED )
			public
			void test02ReglaPeriodicidad() {
		assumeNotNull( boReglaPeriodicidad );
		LOG.debug( "Veamos los reglaPeriodicidades que hay en la base de datos..." );
		this.listaReglaPeriodicidades();

		LOG.debug( "Buscando al BetaTester por el ID ReglaPeriodicidad BetaTester: " + idBetaTester );
		ReglaPeriodicidad reglaPeriodicidad = boReglaPeriodicidad.findById( idBetaTester );
		assertNotNull( "ReglaPeriodicidad nulo", reglaPeriodicidad );
		LOG.debug( "ReglaPeriodicidad: " + reglaPeriodicidad );

		LOG.debug( "ReglaPeriodicidad[id]: " + reglaPeriodicidad.getId() );
		LOG.debug( "ReglaPeriodicidad[descripcion]: " + reglaPeriodicidad.getDescripcion() );
	}

	// ------------------------------------------------ Internal Methods

	private void listaReglaPeriodicidades() {
		assumeNotNull( boReglaPeriodicidad );
		List< ReglaPeriodicidad > reglaPeriodicidads = ( List< ReglaPeriodicidad > ) boReglaPeriodicidad.findAll();
		assertNotNull( "ReglaPeriodicidads nulo", reglaPeriodicidads );
		assertTrue( "ReglaPeriodicidads vacio", reglaPeriodicidads != null );
		LOG.debug( "Numero de reglaPeriodicidads: " + reglaPeriodicidads.size() );
	}

	private Long altaDeReglaPeriodicidad( String idReglaUsuario, String descripcion, Date fechaDesde, Date fechaHasta, boolean activo ) {
		ReglaPeriodicidad reglaPeriodicidad = new ReglaPeriodicidad();
		reglaPeriodicidad.setIdReglaUsuario( idReglaUsuario );
		reglaPeriodicidad.setDescripcion( descripcion );
		reglaPeriodicidad.setFechaDesde( fechaDesde );
		reglaPeriodicidad.setFechaHasta( fechaHasta );
		reglaPeriodicidad.setActivo( activo );
		reglaPeriodicidad.setAuditDate( new java.util.Date() );
		reglaPeriodicidad.setAuditUser( "jcgarrido" );

		LOG.debug( "SAVING ReglaPeriodicidad[" + reglaPeriodicidad.getIdReglaUsuario() + "]: " + reglaPeriodicidad );
		boReglaPeriodicidad.save( reglaPeriodicidad );
		LOG.debug( "SAVED ReglaPeriodicidad[" + reglaPeriodicidad.getIdReglaUsuario() + "]: " + reglaPeriodicidad );
		return reglaPeriodicidad.getId();
	}

}
