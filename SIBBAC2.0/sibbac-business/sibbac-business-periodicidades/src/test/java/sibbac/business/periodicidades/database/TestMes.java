package sibbac.business.periodicidades.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.periodicidades.database.model.Mes;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestMes extends ADBTest {

	private static Integer	idBetaTester	= 1;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test01AltaDeMes() {
		assumeNotNull( boMes );
		this.altaDeMes( new Integer( 1 ), "Un nuevo mes" );
		idBetaTester = this.altaDeMes( new Integer( 2 ), "Otro nuevo mes" );
		this.listaMess();
		LOG.debug( "ID Mes BetaTester: " + idBetaTester );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test02Mes() {
		assumeNotNull( boMes );
		LOG.debug( "Veamos los mes que hay en la base de datos..." );
		this.listaMess();
		LOG.debug( "Buscando al BetaTester por el ID Mes BetaTester: " + idBetaTester );
		Mes mes = boMes.findById( idBetaTester );
		assertNotNull( "Mes nulo", mes );
		LOG.debug( "Mes: " + mes );

		LOG.debug( "Mes[id]: " + mes.getId() );
		LOG.debug( "Mes[name]: " + mes.getNombreMes() );
	}

	// ------------------------------------------------ Internal Methods

	private void listaMess() {
		assumeNotNull( boMes );
		List< Mes > mess = ( List< Mes > ) boMes.findAll();
		assertNotNull( "Mess nulo", mess );
		assertTrue( "Mess vacio", mess != null );
		LOG.debug( "Numero de mess: " + mess.size() );
	}

	private Integer altaDeMes( final Integer id, final String name ) {
		Mes mes = new Mes();
		mes.setId( id );
		mes.setNombreMes( name );
		assertNotNull( "Mes[" + id + "] nulo", mes );
		assertNotNull( "Mes[" + name + "] nulo", mes );
		LOG.debug( "SAVING Mes[" + name + "]: " + mes );
		boMes.save( mes );
		LOG.debug( "SAVED Mes[" + name + "]: " + mes );
		return mes.getId();
	}

}
