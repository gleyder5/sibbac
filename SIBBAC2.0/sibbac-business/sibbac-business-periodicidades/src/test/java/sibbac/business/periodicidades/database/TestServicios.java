package sibbac.business.periodicidades.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// Java
// JUnit
// Spring

import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.tasks.Task;
import sibbac.tasks.database.JobPK;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestServicios extends ADBTest {

	private static final Logger	LOG			= LoggerFactory.getLogger( TestServicios.class );

	private static final JobPK	JOBPK		= new JobPK( Task.GROUP_FACTURACION.NAME, Task.GROUP_FACTURACION.JOB_ENVIAR_DUPLICADO_FACTURA );

	private static final Long	ID_ALIAS		= new Long( 71429 );
	private static final String	CD_ALIASS	= "4024";

	@Autowired
	private AliasBo				aliasBo;

	@Autowired
	private GestorServicios		gestorServicios;

	private Alias				alias;
	private String				cdAlias;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void Test10CheckTaskActive() {

		LOG.debug( "Starting..." );
		LOG.debug( "> [   JobPK=={}]", JOBPK );
		LOG.debug( "> [ IDAlias=={}]", ID_ALIAS );
		LOG.debug( "> [CDALIASS=={}]", CD_ALIASS );

		alias = aliasBo.findById( ID_ALIAS );
		assertNotNull( "No hay alias", alias );
		LOG.debug( "Tenemos el alias: [{}]", alias.getId() );
		cdAlias = alias.getCdaliass().trim();
		LOG.debug( "Tenemos el cdAlias: [{}]", cdAlias );
		assertTrue( "No tenemos el alias", cdAlias.equalsIgnoreCase( CD_ALIASS ) );
		LOG.debug( "Tenemos el CDalias {}: [{}]", CD_ALIASS, alias );
/*
		List< Alias > activos = gestorServicios.isActive( JOBPK );
		// assertNotNull( "No hay aliases activos", activos );
		// assertTrue( "No hay ningun alias activo", activos.size() > 0 );
		LOG.debug( "Tenemos {} aliases.", activos.size() );
*/
		List<Contacto> contactos = gestorServicios.getContactos( alias, JOBPK );
		assertNotNull( "No hay contactos", contactos );
		assertTrue( "No hay ningun contacto", contactos.size() > 0 );
		LOG.debug( "Tenemos {} contactos.", contactos.size() );
	}

}
