package sibbac.business.periodicidades.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.periodicidades.database.model.DiaSemana;
import sibbac.business.periodicidades.database.model.Mes;
import sibbac.business.periodicidades.database.model.ParametrizacionPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestParametrizacionPeriodicidad extends ADBTest {

	private static Long	idBetaTester	= 1L;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test01AltaDeParametrizacionPeriodicidad() {
		// Fecha
		java.util.Calendar cal = java.util.Calendar.getInstance();
		java.util.Date utilDate = cal.getTime();
		java.sql.Date sqlDate = new Date( utilDate.getTime() );
		//
		assumeNotNull( boParametrizacionPeriodicidad );

		this.altaDeParametrizacionPeriodicidad( 23, true, true, true, 11, 21, 12, 43, sqlDate, sqlDate, true );
		idBetaTester = this.altaDeParametrizacionPeriodicidad( 21, false, false, false, 1, 2, 3, 4, sqlDate, sqlDate, false );
		this.listaParametrizacionPeriodicidads();
		LOG.debug( "ID ParametrizacionPeriodicidad BetaTester: " + idBetaTester );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test02ParametrizacionPeriodicidad() {
		assumeNotNull( boParametrizacionPeriodicidad );
		LOG.debug( "Veamos los parametrizacionPeriodicidads que hay en la base de datos..." );
		this.listaParametrizacionPeriodicidads();
		LOG.debug( "Buscando al BetaTester por el ID ParametrizacionPeriodicidad BetaTester: " + idBetaTester );
		ParametrizacionPeriodicidad parametrizacionPeriodicidad = boParametrizacionPeriodicidad.findById( idBetaTester );
		assertNotNull( "ParametrizacionPeriodicidad nulo", parametrizacionPeriodicidad );
		LOG.debug( "ParametrizacionPeriodicidad: " + parametrizacionPeriodicidad );

		LOG.debug( "ParametrizacionPeriodicidad[id]: " + parametrizacionPeriodicidad.getId() );
		LOG.debug( "ParametrizacionPeriodicidad[diames]: " + parametrizacionPeriodicidad.getDiaMes() );

		ArrayList< ParametrizacionPeriodicidad > listParams = boParametrizacionPeriodicidad.findAllByReglaPeriodicidad( boReglaPeriodicidad
				.findByIdReglaUsuario( "11" ) );
		assertNotNull( "listParams nulo", listParams );

		LOG.debug( "Hay " + listParams.size() + " ParametrizacionPeriodicidad con regla id usuario 11" );

		listParams = boParametrizacionPeriodicidad.findAllByReglaPeriodicidadAndActivo( boReglaPeriodicidad.findByIdReglaUsuario( "11" ),
				true );
		assertNotNull( "listParams nulo", listParams );

		LOG.debug( "Hay " + listParams.size() + " ParametrizacionPeriodicidad activas con regla id usuario 11" );

	}

	// ------------------------------------------------ Internal Methods

	private void listaParametrizacionPeriodicidads() {
		assumeNotNull( boParametrizacionPeriodicidad );
		List< ParametrizacionPeriodicidad > parametrizacionPeriodicidads = ( List< ParametrizacionPeriodicidad > ) boParametrizacionPeriodicidad
				.findAll();
		assertNotNull( "ParametrizacionPeriodicidads nulo", parametrizacionPeriodicidads );
		assertTrue( "ParametrizacionPeriodicidads vacio", parametrizacionPeriodicidads != null );
		LOG.debug( "Numero de parametrizacionPeriodicidads: " + parametrizacionPeriodicidads.size() );
	}

	private Long altaDeParametrizacionPeriodicidad( final Integer diaMes, boolean excepcionarFestivos, boolean laborableAnterior,
			boolean laborablePosterior, Integer hora, Integer minuto, Integer margenAnterior, Integer margenPosterior, Date fechaDesde,
			Date fechaHasta, boolean activo ) {
		ParametrizacionPeriodicidad parametrizacionPeriodicidad = new ParametrizacionPeriodicidad();
		if ( ( ( List< DiaSemana > ) boDiaSemana.findAll() ).size() == 0 ) {
			DiaSemana diaSemana = new DiaSemana();
			diaSemana.setId( 23 );
			diaSemana.setNombreDia( "Jordan" );
			boDiaSemana.save( diaSemana );
		}
		if ( ( ( List< Mes > ) boMes.findAll() ).size() == 0 ) {
			Mes mes = new Mes();
			mes.setId( 34 );
			mes.setNombreMes( "Febriembre" );
			boMes.save( mes );
		}
		if ( ( ( List< ReglaPeriodicidad > ) boReglaPeriodicidad.findAll() ).size() == 0 ) {
			ReglaPeriodicidad reglaPeriodicidad = new ReglaPeriodicidad();
			reglaPeriodicidad.setIdReglaUsuario( "11" );
			reglaPeriodicidad.setDescripcion( "desc" );
			reglaPeriodicidad.setFechaDesde( fechaDesde );
			reglaPeriodicidad.setFechaHasta( fechaHasta );
			reglaPeriodicidad.setActivo( true );
			reglaPeriodicidad.setAuditDate( new java.util.Date() );
			reglaPeriodicidad.setAuditUser( "jcgarrido" );
			boReglaPeriodicidad.save( reglaPeriodicidad );
		}
		List< ReglaPeriodicidad > listaRegla = ( List< ReglaPeriodicidad > ) boReglaPeriodicidad.findAll();
		List< Mes > listaMes = ( List< Mes > ) boMes.findAll();
		List< DiaSemana > listaDiaSemana = ( List< DiaSemana > ) boDiaSemana.findAll();

		parametrizacionPeriodicidad.setReglaPeriodicidad( listaRegla.get( listaRegla.size() - 1 ) );
		parametrizacionPeriodicidad.setMes( listaMes.get( listaMes.size() - 1 ) );
		parametrizacionPeriodicidad.setDiaSemana( listaDiaSemana.get( listaDiaSemana.size() - 1 ) );
		parametrizacionPeriodicidad.setDiaMes( diaMes );
		parametrizacionPeriodicidad.setExcepcionarFestivos( excepcionarFestivos );
		parametrizacionPeriodicidad.setLaborableAnterior( laborableAnterior );
		parametrizacionPeriodicidad.setLaborablePosterior( laborablePosterior );
		parametrizacionPeriodicidad.setHora( hora );
		parametrizacionPeriodicidad.setMinuto( minuto );
		parametrizacionPeriodicidad.setMargenAnterior( margenAnterior );
		parametrizacionPeriodicidad.setMargenPosterior( margenPosterior );
		parametrizacionPeriodicidad.setFechaDesde( fechaDesde );
		parametrizacionPeriodicidad.setFechaHasta( fechaHasta );
		parametrizacionPeriodicidad.setActivo( activo );
		parametrizacionPeriodicidad.setAuditDate( new java.util.Date() );
		parametrizacionPeriodicidad.setAuditUser( "jcgarrido" );

		LOG.debug( "SAVING ParametrizacionPeriodicidad[" + diaMes.toString() + "]: " + parametrizacionPeriodicidad );
		boParametrizacionPeriodicidad.save( parametrizacionPeriodicidad );
		LOG.debug( "SAVED ParametrizacionPeriodicidad[" + diaMes.toString() + "]: " + parametrizacionPeriodicidad );
		return parametrizacionPeriodicidad.getId();
	}

}
