package sibbac.business.periodicidades;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sibbac.business.wrappers.database.bo.Tmct0alcBo;
import sibbac.business.wrappers.database.data.InformeOpercacionData;
import sibbac.business.wrappers.database.model.Tmct0alcId;
import sibbac.common.utils.SendMail;
import sibbac.test.BaseTest;

@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSendMail extends BaseTest {
	@Autowired private SendMail	sendMail;
	@Autowired private Tmct0alcBo alcBo;

	@Test public void sendMailWithAttach() throws IllegalArgumentException, MessagingException, IOException {
		File file;
		String subject = "Correo de prueba";
		String body = "Tomás. Este es un correo de prueba con ficheros adjuntos. Mañana, cuando termine las pruebas, subo los cambios.";
		List<String> to = new ArrayList<String>();
		List<String> cc = new ArrayList<String>();
		List<InputStream> path2attach = new ArrayList<InputStream>();
		List<String> nameDest = new ArrayList<String>();
		to.add("tacosta@vectorsf.com");
		cc.add("juanignacio.calderon@servexternos.isban.es");
		cc.add("afiguero@isban.es");
		cc.add("nachocv@gmail.com");
		List<InformeOpercacionData> lineas = new ArrayList<InformeOpercacionData>();
		InformeOpercacionData informe = new InformeOpercacionData(new Date(115, 10, 15), new Date(115, 10, 15),
				new Tmct0alcId("nbooking", "nuorden", Short.valueOf("100"), "nucnfclt"),
				'C', "", "", BigDecimal.ONE, "", BigDecimal.TEN, BigDecimal.TEN, "");
		lineas.add(informe);
		file = new File("src/test/resources", "LIQUIDACION.Estructura_norma43.pdf");
		path2attach.add(new FileInputStream(file));
		nameDest.add(file.getName());
		path2attach.add(alcBo.crearExcel(lineas, "ES"));
		nameDest.add("fichero_excel.xlsx");
		sendMail.sendMail(to, subject, body, path2attach, nameDest, cc);
		Assert.assertTrue("Enviado un correo con adjuntos de prueba", true);
	}
}
