package sibbac.business.periodicidades.database;


// Internal
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import sibbac.business.periodicidades.GestorServiciosImpl;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.tasks.database.Job;
import sibbac.tasks.database.JobBO;
// Java
// JUnit
// Spring
import sibbac.tasks.database.JobPK;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestGestorServicios extends ADBTest {

	@Autowired
	private AliasBo				aliasBo;
	@Autowired
	private GestorServiciosImpl	gestor;
	@Autowired
	private JobBO				jobBo;

	@Test
	public void testGetContactos() {

		Long idAlias = new Long( 77681 );
		Alias alias = aliasBo.findById( idAlias );

		List< Job > job = ( List< Job > ) jobBo.findAll();

		JobPK jobpk = job.get( 0 ).getId();
		gestor.getContactos( alias, jobpk );
	}

}
