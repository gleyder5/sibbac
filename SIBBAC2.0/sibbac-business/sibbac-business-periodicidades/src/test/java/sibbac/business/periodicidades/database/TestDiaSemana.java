package sibbac.business.periodicidades.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.periodicidades.database.model.DiaSemana;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestDiaSemana extends ADBTest {

	private static Integer	idBetaTester	= 1;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test01AltaDeDiaSemana() {
		assumeNotNull( boDiaSemana );
		this.altaDeDiaSemana( new Integer( 1 ), "Un nuevo diaSemana" );
		idBetaTester = this.altaDeDiaSemana( new Integer( 2 ), "Otro nuevo diaSemana" );
		this.listaDiaSemanas();
		LOG.debug( "ID DiaSemana BetaTester: " + idBetaTester );
	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	public void test02DiaSemana() {
		assumeNotNull( boDiaSemana );
		LOG.debug( "Veamos los diaSemanas que hay en la base de datos..." );
		this.listaDiaSemanas();
		LOG.debug( "Buscando al BetaTester por el ID DiaSemana BetaTester: " + idBetaTester );
		DiaSemana diaSemana = boDiaSemana.findById( idBetaTester );
		assertNotNull( "DiaSemana nulo", diaSemana );
		LOG.debug( "DiaSemana: " + diaSemana );

		LOG.debug( "DiaSemana[id]: " + diaSemana.getId() );
		LOG.debug( "DiaSemana[name]: " + diaSemana.getNombreDia() );
	}

	// ------------------------------------------------ Internal Methods

	private void listaDiaSemanas() {
		assumeNotNull( boDiaSemana );
		List< DiaSemana > diaSemanas = ( List< DiaSemana > ) boDiaSemana.findAll();
		assertNotNull( "DiaSemanas nulo", diaSemanas );
		assertTrue( "DiaSemanas vacio", diaSemanas != null );
		LOG.debug( "Numero de diaSemanas: " + diaSemanas.size() );
	}

	private Integer altaDeDiaSemana( final Integer id, final String name ) {
		DiaSemana diaSemana = new DiaSemana();
		diaSemana.setId( id );
		diaSemana.setNombreDia( name );
		assertNotNull( "DiaSemana[" + id + "] nulo", diaSemana );
		assertNotNull( "DiaSemana[" + name + "] nulo", diaSemana );
		LOG.debug( "SAVING DiaSemana[" + name + "]: " + diaSemana );
		boDiaSemana.save( diaSemana );
		LOG.debug( "SAVED DiaSemana[" + name + "]: " + diaSemana );
		return diaSemana.getId();
	}

}
