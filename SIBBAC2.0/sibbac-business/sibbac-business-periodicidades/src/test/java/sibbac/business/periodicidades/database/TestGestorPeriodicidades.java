package sibbac.business.periodicidades.database;


// Internal
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeNotNull;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.quartz.TimeOfDay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.periodicidades.database.model.DiaSemana;
import sibbac.business.periodicidades.database.model.Mes;
import sibbac.business.periodicidades.database.model.ParametrizacionPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.periodicidades.rest.SIBBACServicePeriodicidades;
import sibbac.business.wrappers.database.model.Festivo;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestGestorPeriodicidades extends ADBTest {

	protected static final Logger	LOG			= LoggerFactory.getLogger( TestGestorPeriodicidades.class );

	private static Long				idBetaTeste	= 1L;

	// @Inject
	GestorPeriodicidades			gestor		= null;

	// @Inject
	SIBBACServicePeriodicidades		service		= null;

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	@Ignore
	public void test01isReglaActiva() {
		// Inicializando los dias de la semana
		if ( boReglaPeriodicidad.findByIdReglaUsuario( "11" ) == null ) {

			// Inicializando la regla
			ReglaPeriodicidad reglaPeriodicidad = boReglaPeriodicidad.createReglaPeriodicidad( "11", "desc", Date.valueOf( "2011-04-03" ),
					Date.valueOf( "2017-04-03" ), true );

			// Damos de alta una nueva parametrizacion

			assumeNotNull( boParametrizacionPeriodicidad );

			boParametrizacionPeriodicidad.createParametrizacionPeriodicidad( null, true, false, true, 10, 34, 12, 43,
					Date.valueOf( "2011-04-03" ), Date.valueOf( "2017-04-03" ), false, null, boDiaSemana.findById( 2 ), reglaPeriodicidad );

			idBetaTeste = boParametrizacionPeriodicidad.createParametrizacionPeriodicidad( 22, true, true, false, 17, 44, 3, 4,
					Date.valueOf( "2011-04-03" ), Date.valueOf( "2017-04-03" ), true, boMes.findById( 4 ), null, reglaPeriodicidad )
					.getId();
			this.listaParametrizacionPeriodicidads();
			LOG.debug( "ID ParametrizacionPeriodicidad BetaTeste: " + idBetaTeste );
			// Fin nueva alta parametrizacion

			// Incluimos un nuevo dia festivo

			this.altaDeFestivo( new Integer( 1 ), Date.valueOf( "2015-03-31" ), "San Javi Moreno" );
			this.altaDeFestivo( new Integer( 2 ), Date.valueOf( "2015-03-30" ), "San Cholo Simeone" );
		}

		String myTextDate = "2015-04-01-10:34";
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd-hh:mm" );
		java.util.Date myDate = null;
		try {
			myDate = sdf.parse( myTextDate );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}
		gestor.isReglaActiva( "11", myDate );
	}

	@Test
	public void test02getReglasActivas() {
		// gestor.getReglasActivas(null);
	}

	@Test
	// @Transactional(propagation = Propagation.REQUIRED)
			@Ignore
			public
			void test03getLaborableAnteriorYPosterior() {

		// ///////////////////////
		// gestor = new GestorPeriodicidades(); Con Spring no hace falta esto,
		// solo se hace @Inject ariba y ya esta
		java.util.Date now = new java.util.Date();
		Date fecha = new Date( now.getTime() );
		LOG.info( "Dia: " + fecha );

		Date laborableAnterior = gestor.getLaborableAnterior( fecha );
		Date laborablePosterior = gestor.getLaborablePosterior( fecha );

		LOG.info( "Dia: " + fecha + " Laborable anterior: " + laborableAnterior + " Laborable posterior: " + laborablePosterior );

	}

	@Test
	@Ignore
	public void test04getListDatesBetween() {
		java.util.Date now = new java.util.Date();
		Date fecha = new Date( now.getTime() );
		Date fecha2 = new Date( now.getTime() );

		fecha2 = gestor.sumarDia( fecha2 );
		ArrayList< Date > fechasBetween = gestor.getListDatesBetween( fecha, fecha2 );

		for ( int i = 0; i < fechasBetween.size(); i++ ) {
			LOG.info( "La fecha<" + fechasBetween.get( i ) + "> esta entre : <" + fecha + "> y <" + fecha2 + ">" );
		}

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	@Ignore
	public void test05isMomentValid() {

		// Recuperamos de la base de datos la paremitrizacion
		ParametrizacionPeriodicidad parametrizacionPeriodicidad = boParametrizacionPeriodicidad.findById( idBetaTeste );
		assertNotNull( "ParametrizacionPeriodicidad nulo", parametrizacionPeriodicidad );
		LOG.debug( "ParametrizacionPeriodicidad: " + parametrizacionPeriodicidad );

		// Comprobamos si el momento es valido
		TimeOfDay momento = new TimeOfDay( 1, 2 );
		gestor.isMomentValid( parametrizacionPeriodicidad, momento );

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	@Ignore
	public void test07pruebaServiceCreateFestivo() {

		WebRequest request = new WebRequest();
		request.setService( "SIBBACServicePeriodicidades" );
		request.setAction( "createFestivo" );
		Map< String, String > map = new HashMap< String, String >();
		map.put( "date", "20150417" );
		map.put( "desc", "San Pep" );
		request.setFilters( map );
		service.process( request );
		Festivo festivo = boFestivo.findByFecha( Date.valueOf( "2015-04-17" ) );
		LOG.debug( "SAVED Festivo[" + festivo.getDescripcion() + "]: " + festivo );

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	@Ignore
	public void test08pruebaServiceCreateRegla() {

		WebRequest request = new WebRequest();
		request.setService( "SIBBACServicePeriodicidades" );
		request.setAction( "createRegla" );
		Map< String, String > map = new HashMap< String, String >();
		map.put( "idReglaUsuario", "20150407" );
		map.put( "descripcion", "Regla cuyo id usuario es 20150407" );
		map.put( "fechaDesde", "20110407" );
		map.put( "fechaHasta", "20180407" );
		map.put( "activo", "true" );
		request.setFilters( map );
		WebResponse response = service.process( request );
		ReglaPeriodicidad regla = boReglaPeriodicidad.findByIdReglaUsuario( "20150407" );
		LOG.debug( "SAVED Regla[" + regla.getDescripcion() + "]: " + regla );
		LOG.debug( "Response[" + response + "]: " );

	}

	@Test
	@Ignore
	public void test09pruebaServiceIsReglaActiva() {

		WebRequest request = new WebRequest();
		request.setService( "SIBBACServicePeriodicidades" );
		request.setAction( "isReglaActiva" );
		Map< String, String > map = new HashMap< String, String >();
		map.put( "id", "11" );
		map.put( "date", "201504071434" );
		request.setFilters( map );
		WebResponse response = service.process( request );
		ReglaPeriodicidad regla = boReglaPeriodicidad.findByIdReglaUsuario( "11" );
		LOG.debug( "SAVED Regla[" + regla.getDescripcion() + "]: " + regla );
		LOG.debug( "Response[" + response + "]: " );

	}

	@Test
	@Transactional( propagation = Propagation.REQUIRED )
	@Ignore
	public void test10pruebaServiceCreateParametrizacion() {

		WebRequest request = new WebRequest();
		request.setService( "SIBBACServicePeriodicidades" );
		request.setAction( "createParametrizacion" );
		Map< String, String > map = new HashMap< String, String >();
		map.put( "diaMes", "23" );
		map.put( "excepcionarFestivos", "true" );
		map.put( "laborableAnterior", "false" );
		map.put( "laborablePosterior", "true" );
		map.put( "hora", "17" );
		map.put( "minuto", "44" );
		map.put( "margenAnterior", "3" );
		map.put( "margenPosterior", "4" );
		map.put( "fechaDesde", "20110403" );
		map.put( "fechaHasta", "20190403" );
		map.put( "activo", "true" );
		map.put( "idMes", "4" );
		map.put( "idDiaSemana", null );
		map.put( "idReglaUsuario", "20150407" );
		request.setFilters( map );
		WebResponse response = service.process( request );

		ReglaPeriodicidad regla = boReglaPeriodicidad.findByIdReglaUsuario( "11" );
		LOG.debug( "SAVED Regla[" + regla.getDescripcion() + "]: " + regla );
		LOG.debug( "Response[" + response + "]: " );

	}

	@Test
	@Ignore
	public void test11pruebaServiceGetFestivos() {

		WebRequest request = new WebRequest();
		request.setService( "SIBBACServicePeriodicidades" );
		request.setAction( "getFestivos" );
		WebResponse response = service.process( request );
		LOG.debug( "Response getFestivos[" + response.getResultados() + "]: " );

	}

	@Test
	@Ignore
	public void test12pruebaServiceGetReglasActivas() {

		WebRequest request = new WebRequest();
		request.setService( "SIBBACServicePeriodicidades" );
		request.setAction( "getReglasActivas" );
		Map< String, String > map = new HashMap< String, String >();
		map.put( "date", "201504221744" );
		request.setFilters( map );
		service.process( request );

	}

	// @Test
	// public void test13pruebaCascadeSave() {
	// // Inicializando la regla
	// ReglaPeriodicidad reglaPeriodicidad = new ReglaPeriodicidad();
	// reglaPeriodicidad.setIdReglaUsuario("14");
	// reglaPeriodicidad.setDescripcion("descripcion");
	// reglaPeriodicidad.setFechaDesde(Date.valueOf("1999-04-03"));
	// reglaPeriodicidad.setFechaHasta(Date.valueOf("2057-04-03"));
	// reglaPeriodicidad.setActivo(true);
	//
	// ParametrizacionPeriodicidad param = new ParametrizacionPeriodicidad();
	//
	// param.setReglaPeriodicidad(reglaPeriodicidad);
	// param.setMes(boMes.findByIdMes(2));
	// param.setDiaMes(21);
	// param.setExcepcionarFestivos(true);
	// param.setLaborableAnterior(true);
	// param.setLaborablePosterior(false);
	// param.setHora(12);
	// param.setMinuto(32);
	// param.setMargenAnterior(57);
	// param.setMargenPosterior(41);
	// param.setFechaDesde(Date.valueOf("1999-04-03"));
	// param.setFechaHasta(Date.valueOf("2057-04-03"));
	// param.setActivo(true);
	// param.setAuditDate(new java.util.Date());
	// param.setAuditUser("SET_USER");
	// LOG.debug("SAVING Parametrizacion[" + param.getId() + "]: " + param);
	// boParametrizacionPeriodicidad.save(param);
	// LOG.debug("SAVED Parametrizacion[" + param.getId() + "]: " + param);
	//
	//
	// }
	// ------------------------------------------------ Internal Methods
	// -------------------------------------//

	private Integer altaDeFestivo( final Integer id, final Date fecha, final String descripcion ) {
		Festivo festivo = new Festivo();
		// festivo.setId( id );
		festivo.setFecha( fecha );
		festivo.setDescripcion( descripcion );
		assertNotNull( "Festivo[" + fecha + "] nulo", festivo );
		assertNotNull( "Festivo[" + descripcion + "] nulo", festivo );
		LOG.debug( "SAVING Festivo[" + descripcion + "]: " + festivo );
		boFestivo.save( festivo );
		LOG.debug( "SAVED Festivo[" + descripcion + "]: " + festivo );
		return festivo.getId();
	}

	private void listaParametrizacionPeriodicidads() {
		assumeNotNull( boParametrizacionPeriodicidad );
		List< ParametrizacionPeriodicidad > parametrizacionPeriodicidads = ( List< ParametrizacionPeriodicidad > ) boParametrizacionPeriodicidad
				.findAll();
		assertNotNull( "ParametrizacionPeriodicidads nulo", parametrizacionPeriodicidads );
		assertTrue( "ParametrizacionPeriodicidads vacio", parametrizacionPeriodicidads != null );
		LOG.debug( "Numero de parametrizacionPeriodicidads: " + parametrizacionPeriodicidads.size() );
	}

	@Transactional( propagation = Propagation.REQUIRED )
	private Long altaDeParametrizacionPeriodicidad( final Integer diaMes, boolean excepcionarFestivos, boolean laborableAnterior,
			boolean laborablePosterior, Integer hora, Integer minuto, Integer margenAnterior, Integer margenPosterior, Date fechaDesde,
			Date fechaHasta, boolean activo, Mes mes, DiaSemana diaSemana, ReglaPeriodicidad regla ) {
		ParametrizacionPeriodicidad parametrizacionPeriodicidad = new ParametrizacionPeriodicidad();

		parametrizacionPeriodicidad.setReglaPeriodicidad( regla );
		if ( mes != null ) {
			parametrizacionPeriodicidad.setMes( mes );
			parametrizacionPeriodicidad.setDiaMes( diaMes );
		}
		parametrizacionPeriodicidad.setExcepcionarFestivos( excepcionarFestivos );
		parametrizacionPeriodicidad.setLaborableAnterior( laborableAnterior );
		parametrizacionPeriodicidad.setLaborablePosterior( laborablePosterior );
		parametrizacionPeriodicidad.setHora( hora );
		parametrizacionPeriodicidad.setMinuto( minuto );
		parametrizacionPeriodicidad.setMargenAnterior( margenAnterior );
		parametrizacionPeriodicidad.setMargenPosterior( margenPosterior );
		parametrizacionPeriodicidad.setFechaDesde( fechaDesde );
		parametrizacionPeriodicidad.setFechaHasta( fechaHasta );
		parametrizacionPeriodicidad.setActivo( activo );
		parametrizacionPeriodicidad.setAuditDate( new java.util.Date() );
		parametrizacionPeriodicidad.setAuditUser( "jcgarrido" );
		if ( diaSemana != null ) {
			parametrizacionPeriodicidad.setDiaSemana( diaSemana );
		}

		LOG.debug( "SAVING ParametrizacionPeriodicidad" );
		boParametrizacionPeriodicidad.save( parametrizacionPeriodicidad );
		LOG.debug( "SAVED ParametrizacionPeriodicidad[" + parametrizacionPeriodicidad.getId() + "]" );
		return parametrizacionPeriodicidad.getId();
	}
}
