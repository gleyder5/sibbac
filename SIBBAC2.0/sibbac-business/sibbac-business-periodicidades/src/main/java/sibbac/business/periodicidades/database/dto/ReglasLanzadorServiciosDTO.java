package sibbac.business.periodicidades.database.dto;


import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios;


public class ReglasLanzadorServiciosDTO {

	private Long	idAlias;
	private String	descrAlias;
	private Long	idServicio;
	private String	descrServicio;
	private Long	idRegla;
	private String	descrRegla;

	/**
	 * Constructor ReglasLanzadorServiciosDTO
	 * 
	 * @param entity
	 */
	public ReglasLanzadorServiciosDTO( ReglasLanzadorServicios entity ) {
		this.entityToDto( entity );
	}

	/**
	 * Constructor ReglasLanzadorServiciosDTO
	 * 
	 * @param idAlias
	 * @param descrAlias
	 * @param idServicio
	 * @param descrServicio
	 * @param idRegla
	 * @param descrRegla
	 */
	public ReglasLanzadorServiciosDTO( Long idAlias, String descrAlias, Long idServicio, String descrServicio, Long idRegla,
			String descrRegla ) {
		this.idAlias = idAlias;
		this.descrAlias = descrAlias;
		this.idServicio = idServicio;
		this.descrServicio = descrServicio;
		this.idRegla = idRegla;
		this.descrRegla = descrRegla;
	}

	/**
	 * Constructor ReglasLanzadorServiciosDTO
	 * 
	 * @param idServicio
	 * @param idAlias
	 * @param idRegla
	 */
	public ReglasLanzadorServiciosDTO( Long idServicio, Long idAlias, Long idRegla ) {
		this.idAlias = idAlias;
		this.idServicio = idServicio;
		this.idRegla = idRegla;
	}

	/**
	 * entityToDto
	 * 
	 * @param entity
	 * @return
	 */
	public ReglasLanzadorServiciosDTO entityToDto( ReglasLanzadorServicios entity ) {
		this.idAlias = entity.getAlias().getId();
		this.descrAlias = entity.getAlias().getCdaliass() + entity.getAlias().getDescrali();
		this.idServicio = entity.getServicio().getId();
		this.descrServicio = entity.getServicio().getNombreServicio() + entity.getServicio().getDescripcionServicio();
		this.idRegla = entity.getRegla().getId();
		this.descrRegla = entity.getRegla().getDescripcion();

		return this;
	}

	/**
	 * getIdAlias
	 * 
	 * @return
	 */
	public Long getIdAlias() {
		return idAlias;
	}

	/**
	 * setIdAlias
	 * 
	 * @param idAlias
	 */
	public void setIdAlias( Long idAlias ) {
		this.idAlias = idAlias;
	}

	/**
	 * getDescrAlias
	 * 
	 * @return
	 */
	public String getDescrAlias() {
		return descrAlias;
	}

	/**
	 * setDescrAlias
	 * 
	 * @param descrAlias
	 */
	public void setDescrAlias( String descrAlias ) {
		this.descrAlias = descrAlias;
	}

	/**
	 * getIdServicio
	 * 
	 * @return
	 */
	public Long getIdServicio() {
		return idServicio;
	}

	/**
	 * setIdServicio
	 * 
	 * @param idServicio
	 */
	public void setIdServicio( Long idServicio ) {
		this.idServicio = idServicio;
	}

	/**
	 * getDescrServicio
	 * 
	 * @return
	 */
	public String getDescrServicio() {
		return descrServicio;
	}

	/**
	 * setDescrServicio
	 * 
	 * @param descrServicio
	 */
	public void setDescrServicio( String descrServicio ) {
		this.descrServicio = descrServicio;
	}

	/**
	 * getIdRegla
	 * 
	 * @return
	 */
	public Long getIdRegla() {
		return idRegla;
	}

	/**
	 * setIdRegla
	 * 
	 * @param idRegla
	 */
	public void setIdRegla( Long idRegla ) {
		this.idRegla = idRegla;
	}

	/**
	 * getDescrRegla
	 * 
	 * @return
	 */
	public String getDescrRegla() {
		return descrRegla;
	}

	/**
	 * setDescrRegla
	 * 
	 * @param descrRegla
	 */
	public void setDescrRegla( String descrRegla ) {
		this.descrRegla = descrRegla;
	}
}
