package sibbac.business.periodicidades.database.bo;


import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.periodicidades.database.dao.ReglaPeriodicidadDao;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.database.bo.AbstractBo;


@Service
public class ReglaPeriodicidadBo extends AbstractBo< ReglaPeriodicidad, Long, ReglaPeriodicidadDao > {

	public ReglaPeriodicidad findByIdReglaUsuario( final String id ) {
		return this.dao.findByIdReglaUsuario( id );
	}

	public List< ReglaPeriodicidad > findAllByActivo( boolean activo ) {
		return this.dao.findAllByActivo( activo );
	}

	public ReglaPeriodicidad createReglaPeriodicidad( String idReglaUsuario, String descripcion, Date fechaDesde, Date fechaHasta,
			boolean activo ) {
		ReglaPeriodicidad dto = new ReglaPeriodicidad();
		dto.setIdReglaUsuario( idReglaUsuario );
		dto.setDescripcion( descripcion );
		dto.setFechaDesde( fechaDesde );
		dto.setFechaHasta( fechaHasta );
		dto.setActivo( activo );
		return this.dao.save( dto );
	}

	public boolean deleteRegla( String idReglaUsuario ) {
		ReglaPeriodicidad toDelete = this.findByIdReglaUsuario( idReglaUsuario );
		if ( toDelete == null ) {
			return false;
		}
		try {
			this.dao.delete( toDelete );
		} catch ( Exception e ) {
			LOG.debug( e.getStackTrace().toString() );
		}
		return true;
	}

	public boolean updateRegla( Long id, String idReglaUsuario, String descripcion, Date fechaDesde, Date fechaHasta, Boolean activo ) {
		ReglaPeriodicidad toUpdate = this.findById( id );

		if ( toUpdate == null ) {
			return false;
		}

		toUpdate.setIdReglaUsuario( idReglaUsuario );
		toUpdate.setDescripcion( descripcion );
		toUpdate.setFechaDesde( fechaDesde );
		toUpdate.setFechaHasta( fechaHasta );
		toUpdate.setActivo( activo );

		this.dao.save( toUpdate );

		return true;
	}

	public boolean updateActivoRegla( Long id, Boolean activo ) {
		ReglaPeriodicidad toUpdate = this.findById( id );

		if ( toUpdate == null ) {
			return false;
		}

		toUpdate.setActivo( activo );

		this.dao.save( toUpdate );

		return true;
	}

}
