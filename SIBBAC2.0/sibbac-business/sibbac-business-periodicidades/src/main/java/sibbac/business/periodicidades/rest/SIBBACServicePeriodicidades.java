package sibbac.business.periodicidades.rest;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.quartz.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.periodicidades.Constantes;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.periodicidades.database.bo.DiaSemanaBo;
import sibbac.business.periodicidades.database.bo.MesBo;
import sibbac.business.periodicidades.database.bo.ParametrizacionPeriodicidadBo;
import sibbac.business.periodicidades.database.bo.ReglaPeriodicidadBo;
import sibbac.business.periodicidades.database.model.DiaSemana;
import sibbac.business.periodicidades.database.model.Mes;
import sibbac.business.periodicidades.database.model.ParametrizacionPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.wrappers.database.bo.FestivoBo;
import sibbac.business.wrappers.database.model.Festivo;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * Perform business operations related with: "Periodicidades".<br/>
 * This class, implementing {@link SIBBACServiceBean}, is responsible of parsing
 * incoming {@link WebRequest}'s and process them returning a {@link WebResponse} to be sent back to the client.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@SIBBACService
public class SIBBACServicePeriodicidades implements SIBBACServiceBean {

	@Autowired
	protected FestivoBo						boFestivo;
	@Autowired
	protected ReglaPeriodicidadBo			boRegla;
	@Autowired
	protected ParametrizacionPeriodicidadBo	boParam;
	@Autowired
	protected MesBo							boMes;
	@Autowired
	protected DiaSemanaBo					boDiaSemana;
	@Autowired
	GestorPeriodicidades					gestor			= null;

	protected static String					simpleName		= "SIBBACServicePeriodicidades";

	public static final String				KEY_COMMAND		= "command";

	public static final String				FIELD_JOB_NAME	= "name";
	public static final String				FIELD_INFO		= "info";
	public static final String				FIELD_ERROR		= "error";
	public static final String				FIELD_STATUS	= "status";

	// ------------------------------------------ Bean static properties

	private static final Logger				LOG				= LoggerFactory.getLogger( SIBBACServicePeriodicidades.class );

	// ------------------------------------------------- Bean properties

	@Autowired
	protected SchedulerFactoryBean			quartzSchedulerFactoryBean;

	protected Scheduler						scheduler;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACServicePeriodicidades() {
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_GET_REGLAS_AND_PARAMS );
		commands.add( Constantes.CMD_GET_REGLAS_ACTIVAS );
		commands.add( Constantes.CMD_IS_REGLA_ACTIVA );
		commands.add( Constantes.CMD_CREATE_FESTIVO );
		commands.add( Constantes.CMD_GET_FESTIVOS );
		commands.add( Constantes.CMD_CREATE_REGLA );
		commands.add( Constantes.CMD_CREATE_PARAMETRIZACION );
		commands.add( Constantes.CMD_UPDATE_PARAMETRIZACION );
		commands.add( Constantes.CMD_UPDATE_REGLA );
		commands.add( Constantes.CMD_UPDATE_FESTIVO );
		commands.add( Constantes.CMD_DELETE_PARAMETRIZACION );
		commands.add( Constantes.CMD_DELETE_REGLA );
		commands.add( Constantes.CMD_DELETE_FESTIVO );
		commands.add( Constantes.CMD_UPDATE_ACTIVO_REGLA );
		commands.add( Constantes.CMD_UPDATE_ACTIVO_PARAMETRIZACION );
		commands.add( Constantes.CMD_GET_DIAS_SEMANA );
		commands.add( Constantes.CMD_GET_MESES );
		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		List< String > fields = new ArrayList< String >();
		fields.add( FIELD_JOB_NAME );
		fields.add( FIELD_STATUS );
		return fields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#process(WebRequest)
	 */
	@Override
	public WebResponse process( final WebRequest webRequest ) {
		String prefix = "[SIBBACServicePeriodicidades::process] ";
		LOG.debug( prefix + "Processing a web request..." );
		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );
		Map< String, String > filtros = webRequest.getFilters();
		String jobName = null;
		webResponse.setRequest( webRequest );

		if ( filtros != null ) {
			jobName = filtros.get( FIELD_JOB_NAME );
		}
		LOG.debug( prefix + "+ Job Name: [{}]", jobName );

		LOG.trace( "Command: " + command );
		if ( command == null ) {
			webResponse.setError( "An action must be set. Valid actions are: " + this.getAvailableCommands() );
		} else {
			boolean entered = false;
			if ( command.equalsIgnoreCase( Constantes.CMD_GET_REGLAS_AND_PARAMS ) ) {
				entered = true;
				webResponse.setResultados( this.getReglasAndParam() );
			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_REGLAS_ACTIVAS ) ) {
				entered = true;
				if ( this.getReglasActivasCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.getReglasActivas( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_IS_REGLA_ACTIVA ) ) {
				entered = true;
				if ( this.isReglaActivaCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.isReglaActiva( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_REGLA ) ) {
				entered = true;
				if ( this.updateReglaCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateRegla( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_FESTIVO ) ) {
				entered = true;
				if ( this.updateFestivoCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateFestivo( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_DELETE_PARAMETRIZACION ) ) {
				entered = true;
				if ( this.deleteParametrizacionCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.deleteParametrizacion( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_DELETE_REGLA ) ) {
				entered = true;
				if ( this.deleteReglaCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.deleteRegla( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_DELETE_FESTIVO ) ) {
				entered = true;
				if ( this.deleteFestivoCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.deleteFestivo( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_CREATE_FESTIVO ) ) {
				entered = true;
				if ( this.createFestivoCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.createFestivo( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_FESTIVOS ) ) {
				entered = true;
				webResponse.setResultados( this.getFestivos() );
			} else if ( command.equalsIgnoreCase( Constantes.CMD_CREATE_REGLA ) ) {
				entered = true;
				if ( this.createReglaCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.createRegla( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_CREATE_PARAMETRIZACION ) ) {
				entered = true;
				if ( this.createParametrizacionCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.createParametrizacion( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_PARAMETRIZACION ) ) {
				entered = true;
				if ( this.updateParametrizacionCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateParametrizacion( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_ACTIVO_REGLA ) ) {
				entered = true;
				if ( this.updateActivoReglaCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateActivoRegla( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_ACTIVO_PARAMETRIZACION ) ) {
				entered = true;
				if ( this.updateActivoParametrizacionCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateActivoParametrizacion( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_DIAS_SEMANA ) ) {
				entered = true;
				webResponse.setResultados( this.getDiasSemana() );
			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_MESES ) ) {
				entered = true;
				webResponse.setResultados( this.getMeses() );
			} else if ( !entered ) {
				webResponse.setError( webRequest.getAction() + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
			}
		}

		return webResponse;
	}

	// ---------------------------------------GESTOR-----------------------------------//
	private Map< String, Boolean > isReglaActiva( Map< String, String > filtros ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMddHHmm" );
		Date date = null;
		try {
			date = dateFormat.parse( filtros.get( Constantes.ISREGLA_DATE ) );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}
		String idReglaUsuario = filtros.get( Constantes.ISREGLA_ID );
		Map< String, Boolean > resultadoRegla = new Hashtable< String, Boolean >();
		// GestorPeriodicidades gestor = new GestorPeriodicidades();
		Boolean activa = gestor.isReglaActiva( idReglaUsuario, date );

		resultadoRegla.put( "activa", activa );

		return resultadoRegla;
	}

	public Map< String, String > getReglasActivas( Map< String, String > filtros ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMddHHmm" );
		Date date = null;
		try {
			date = dateFormat.parse( filtros.get( "date" ) );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}
		Map< String, String > resultadosReglas = new Hashtable< String, String >();
		List< ReglaPeriodicidad > reglas = gestor.getReglasActivas( date );

		for ( ReglaPeriodicidad regla : reglas ) {
			if ( regla.getDescripcion() != null ) {
				resultadosReglas.put( regla.getIdReglaUsuario().toString(), regla.getDescripcion() );
			} else {
				resultadosReglas.put( regla.getIdReglaUsuario().toString(), "" );

			}
		}

		return resultadosReglas;
	}

	private boolean isReglaActivaCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;
		if ( filtros.get( Constantes.ISREGLA_DATE ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.ISREGLA_DATE + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.ISREGLA_DATE ).trim().length() != 12 ) {
				errorMsg = errorMsg + "The '" + Constantes.ISREGLA_DATE + "' length is not correct (yyyyMMddHHmm). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{12}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.ISREGLA_DATE ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.ISREGLA_DATE + "' is not a valid date (yyyyMMddHHmm). ";
					filtersRight = false;
				}
			}
		}
		if ( filtros.get( Constantes.ISREGLA_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.ISREGLA_ID + "' field is required. ";
			filtersRight = false;
		}
		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	private boolean getReglasActivasCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.GETREGLAS_DATE ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.GETREGLAS_DATE + "' field is required. ";
			filtersRight = false;
		} else {
			Pattern pattern = Pattern.compile( "\\d{12}" );
			Matcher matcher = pattern.matcher( filtros.get( Constantes.GETREGLAS_DATE ) );
			if ( !matcher.find() ) {
				errorMsg = errorMsg + "The '" + Constantes.GETREGLAS_DATE + "' is not a valid date (yyyyMMddHHmm). ";
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// ----------------------------------GET REGLAS AND PARAM------------------------------------------------//
	@Transactional
	public Map< String, ? extends Object > getReglasAndParam() {
		LOG.trace( "SIBBACServicePeriodicidades <getReglasAndParam>" );
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
		List< ReglaPeriodicidad > reglas = ( ArrayList< ReglaPeriodicidad > ) boRegla.findAll();
		Map< String, Object > resultadosReglas = new HashMap< String, Object >();

		for ( ReglaPeriodicidad regla : reglas ) {
			Map< String, Object > resultadoRegla = new Hashtable< String, Object >();
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_ID, regla.getId().toString() );
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_ID_REGLA_USUARIO, regla.getIdReglaUsuario() );
			if ( regla.getDescripcion() != null ) {
				resultadoRegla.put( Constantes.REGLA_AND_PARAM_DESCRIPCION, regla.getDescripcion() );
			} else {
				resultadoRegla.put( Constantes.REGLA_AND_PARAM_DESCRIPCION, "" );
			}
			String fechaDesde = dateFormat.format( regla.getFechaDesde() );
			String fechaHasta = dateFormat.format( regla.getFechaHasta() );
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_FECHADESDE, fechaDesde );
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_FECHAHASTA, fechaHasta );
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_ACTIVO, String.valueOf( regla.getActivo() ) );

			LOG.trace( "SIBBACServicePeriodicidades <getReglasAndParam> Nos traemos la regla con ID : "
					+ resultadoRegla.get( Constantes.REGLA_AND_PARAM_ID_REGLA_USUARIO ) );

			List< ParametrizacionPeriodicidad > params = regla.getParametrizaciones();
			List< Map< String, String >> resultadosParametros = new ArrayList< Map< String, String >>();
			for ( ParametrizacionPeriodicidad param : params ) {
				Map< String, String > resultadosParam = new Hashtable< String, String >();

				resultadosParam.put( Constantes.REGLA_AND_PARAM_IDPARAM, param.getId().toString() );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_EXCEPCIONARFESTIVOS, String.valueOf( param.getExcepcionarFestivos() ) );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_LABORABLEANTERIOR, String.valueOf( param.getLaborableAnterior() ) );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_LABORABLEPOSTERIOR, String.valueOf( param.getLaborablePosterior() ) );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_HORA, param.getHora().toString() );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_MINUTO, param.getMinuto().toString() );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_MARGENANTERIOR, param.getMargenAnterior().toString() );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_MARGENPOSTERIOR, param.getMargenPosterior().toString() );

				String fechaDesdeParam = dateFormat.format( param.getFechaDesde() );
				String fechaHastaParam = dateFormat.format( param.getFechaHasta() );

				resultadosParam.put( Constantes.REGLA_AND_PARAM_FECHADESDEPARAM, fechaDesdeParam );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_FECHAHASTAPARAM, fechaHastaParam );
				resultadosParam.put( Constantes.REGLA_AND_PARAM_ACTIVOPARAM, String.valueOf( param.getActivo() ) );

				if ( param.getDiaMes() != null ) {
					resultadosParam.put( Constantes.REGLA_AND_PARAM_DIAMES, String.valueOf( param.getDiaMes() ) );
				} 

				if ( param.getMes() != null ) {
					resultadosParam.put( Constantes.REGLA_AND_PARAM_MES, String.valueOf( param.getMes().getId() ) );
					resultadosParam.put( Constantes.REGLA_AND_PARAM_MES_NAME, String.valueOf( param.getMes().getNombreMes() ) );
				} 

				if ( param.getDiaSemana() != null ) {
					resultadosParam.put( Constantes.REGLA_AND_PARAM_DIASEMANA, String.valueOf( param.getDiaSemana().getId() ) );
					resultadosParam.put( Constantes.REGLA_AND_PARAM_DIASEMANA_NAME, String.valueOf( param.getDiaSemana().getNombreDia() ) );
				} 
				resultadosParametros.add( resultadosParam );

				LOG.trace( "SIBBACServicePeriodicidades <getReglasAndParam> Nos traemos la parametrizacion con ID: " + param.getId() );
			}
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_PARAMETRIZACION, resultadosParametros );
			resultadosReglas.put( regla.getIdReglaUsuario(), resultadoRegla );
			LOG.trace( "SIBBACServicePeriodicidades <getReglasAndParam> resultadosReglas : " + resultadosReglas );
		}

		return resultadosReglas;
	}

	// -----------------------------CREATE REGLA----------------------------------------//
	public Map< String, Boolean > createRegla( Map< String, String > filtros ) {
		ReglaPeriodicidad regla = new ReglaPeriodicidad();
		regla.setIdReglaUsuario( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ) );
		regla.setDescripcion( filtros.get( Constantes.REGLA_DESCRIPCION ) );

		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
		Date date = null;
		Date dateHasta = null;
		try {
			date = dateFormat.parse( filtros.get( Constantes.REGLA_FECHADESDE ) );
			dateHasta = dateFormat.parse( filtros.get( Constantes.REGLA_FECHAHASTA ) );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}
		java.sql.Date fecha = new java.sql.Date( date.getTime() );
		regla.setFechaDesde( fecha );
		java.sql.Date fechaHasta = new java.sql.Date( dateHasta.getTime() );
		regla.setFechaHasta( fechaHasta );
		regla.setActivo( Boolean.valueOf( filtros.get( Constantes.REGLA_ACTIVO ) ) );

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		Boolean result = false;
		if ( boRegla.save( regla ) != null ) {
			result = true;
		}
		resultado.put( "result", result );
		return resultado;
	}

	// ---------------------------UPDATE REGLA--------------------------------//
	public Map< String, Boolean > updateRegla( Map< String, String > filtros ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );

		Long id = Long.parseLong( filtros.get( Constantes.REGLA_ID ) );
		String descripcion = filtros.get( Constantes.REGLA_DESCRIPCION );
		String idReglaUsuario = filtros.get( Constantes.REGLA_IDREGLAUSUARIO );

		Date fechaDesde = null;
		Date fechaHasta = null;
		try {
			fechaDesde = dateFormat.parse( filtros.get( Constantes.REGLA_FECHADESDE ) );
			fechaHasta = dateFormat.parse( filtros.get( Constantes.REGLA_FECHAHASTA ) );

		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}
		java.sql.Date DesdeSql = new java.sql.Date( fechaDesde.getTime() );
		java.sql.Date HastaSql = new java.sql.Date( fechaHasta.getTime() );
		Boolean activo = Boolean.valueOf( filtros.get( Constantes.REGLA_ACTIVO ) );
		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		result = boRegla.updateRegla( id, idReglaUsuario, descripcion, DesdeSql, HastaSql, activo );

		resultado.put( "result", result );
		return resultado;
	}

	// ------------------------------UPDATE ACTIVO REGLA------------------------------//
	public Map< String, Boolean > updateActivoRegla( Map< String, String > filtros ) {

		Long id = Long.parseLong( filtros.get( Constantes.REGLA_ID ) );
		Boolean activo = Boolean.valueOf( filtros.get( Constantes.REGLA_ACTIVO ) );
		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		result = boRegla.updateActivoRegla( id, activo );

		resultado.put( "result", result );
		return resultado;
	}

	// -------------------------------DELETE REGLA-------------------------------------//
	public Map< String, Boolean > deleteRegla( Map< String, String > filtros ) {

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		String idreglaUsuario = filtros.get( Constantes.REGLA_IDREGLAUSUARIO );
		result = boRegla.deleteRegla( idreglaUsuario );

		resultado.put( "result", result );
		return resultado;
	}

	// --------------------------------CREATE REGLA CHECK FILTERS--------------------------//
	public boolean createReglaCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_IDREGLAUSUARIO + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ).length() > Constantes.SIZE_REGLA_IDREGLAUSUARIO ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_IDREGLAUSUARIO + "' has a maximum length of "
						+ Constantes.SIZE_REGLA_IDREGLAUSUARIO;
				filtersRight = false;
			}
			if ( boRegla.findByIdReglaUsuario( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ) ) != null ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_IDREGLAUSUARIO + "' already exists. ";
				filtersRight = false;
			}
		}
		if ( filtros.get( Constantes.REGLA_FECHADESDE ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.REGLA_FECHADESDE ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.REGLA_FECHADESDE ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
		}
		if ( filtros.get( Constantes.REGLA_FECHAHASTA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHAHASTA + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.REGLA_FECHAHASTA ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHAHASTA + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.REGLA_FECHAHASTA ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHAHASTA + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
			Date fechaDesde = null;
			Date fechaHasta = null;
			try {
				fechaDesde = dateFormat.parse( filtros.get( Constantes.REGLA_FECHADESDE ) );
				fechaHasta = dateFormat.parse( filtros.get( Constantes.REGLA_FECHAHASTA ) );

			} catch ( ParseException e ) {
				LOG.error("Error: ", e.getClass().getName(), e.getMessage());
			}

			if ( fechaDesde.after( fechaHasta ) ) {

				errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' is after " + Constantes.REGLA_FECHAHASTA;
				LOG.error( "createReglaCheckFilters(): " + errorMsg );
				filtersRight = false;
				webResponse.setError( errorMsg );
				return filtersRight;
			}
		}
		if ( filtros.get( Constantes.REGLA_ACTIVO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_ACTIVO + "' field is required. ";
			filtersRight = false;
		}
		if ( filtros.get( Constantes.REGLA_DESCRIPCION ) != null
				&& filtros.get( Constantes.REGLA_DESCRIPCION ).length() > Constantes.SIZE_REGLA_DESCRIPCION ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_DESCRIPCION + "' has a maximum length of " + Constantes.SIZE_REGLA_DESCRIPCION;
			filtersRight = false;
		}
		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// ------------------------------------UPDATE REGLA CHECK FILTERS--------------------------------//
	public boolean updateReglaCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.REGLA_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boRegla.findById( Long.parseLong( filtros.get( Constantes.REGLA_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.REGLA_ID + "' in the database. ";
				filtersRight = false;
			}
		}
		if ( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_IDREGLAUSUARIO + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ).length() > Constantes.SIZE_REGLA_IDREGLAUSUARIO ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_IDREGLAUSUARIO + "' has a maximum length of "
						+ Constantes.SIZE_REGLA_IDREGLAUSUARIO;
				filtersRight = false;
			}
		}
		if ( filtros.get( Constantes.REGLA_FECHADESDE ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.REGLA_FECHADESDE ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.REGLA_FECHADESDE ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
		}
		if ( filtros.get( Constantes.REGLA_FECHAHASTA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHAHASTA + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.REGLA_FECHAHASTA ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHAHASTA + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.REGLA_FECHAHASTA ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHAHASTA + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
			Date fechaDesde = null;
			Date fechaHasta = null;
			try {
				fechaDesde = dateFormat.parse( filtros.get( Constantes.REGLA_FECHADESDE ) );
				fechaHasta = dateFormat.parse( filtros.get( Constantes.REGLA_FECHAHASTA ) );

			} catch ( ParseException e ) {
				LOG.error("Error: ", e.getClass().getName(), e.getMessage());
			}

			if ( fechaDesde.after( fechaHasta ) ) {

				errorMsg = errorMsg + "The '" + Constantes.REGLA_FECHADESDE + "' is after " + Constantes.REGLA_FECHAHASTA;
				LOG.error( "updateReglaCheckFilters(): " + errorMsg );
				filtersRight = false;
				webResponse.setError( errorMsg );
				return filtersRight;
			}
		}
		if ( filtros.get( Constantes.REGLA_ACTIVO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_ACTIVO + "' field is required. ";
			filtersRight = false;
		}

		if ( filtros.get( Constantes.REGLA_DESCRIPCION ) != null
				&& filtros.get( Constantes.REGLA_DESCRIPCION ).length() > Constantes.SIZE_REGLA_DESCRIPCION ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_DESCRIPCION + "' has a maximum length of " + Constantes.SIZE_REGLA_DESCRIPCION;
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// -------------------------------UPDATE ACTIVO REGLA CHECK FILTERS---------------------//
	public boolean updateActivoReglaCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.REGLA_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boRegla.findById( Long.parseLong( filtros.get( Constantes.REGLA_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.REGLA_ID + "' in the database. ";
				filtersRight = false;
			}
		}

		if ( filtros.get( Constantes.REGLA_ACTIVO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_ACTIVO + "' field is required. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// --------------------------------DELETE REGLA CHECK FILTERS-------------------------------//
	public boolean deleteReglaCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.REGLA_IDREGLAUSUARIO + "' field is required. ";
			filtersRight = false;
		} else {
			ReglaPeriodicidad regla = boRegla.findByIdReglaUsuario( filtros.get( Constantes.REGLA_IDREGLAUSUARIO ) );
			if ( regla == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.REGLA_IDREGLAUSUARIO + "' in the database. ";
				filtersRight = false;
			} else {
				if ( ( ( List< ParametrizacionPeriodicidad > ) regla.getParametrizaciones() ).size() != 0 ) {
					errorMsg = errorMsg + "That '" + Constantes.REGLA_IDREGLAUSUARIO + "' has params and can not be deleted. ";
					filtersRight = false;
				}
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	// --------------------------------------CREATE PARAMETRIZACION......................................//

	private Map< String, Boolean > createParametrizacion( Map< String, String > filtros ) {
		ParametrizacionPeriodicidad param = new ParametrizacionPeriodicidad();
		Integer diaMes = null;
		Mes mes = null;
		DiaSemana diaSemana = null;
		Boolean excepcionarFestivos = false;
		Boolean laborableAnterior = false;
		Boolean laborablePosterior = false;
		Integer hora = null;
		Integer minutos = null;
		Integer margenAnterior = 0;
		Integer margenPosterior = 30;

		if ( filtros.get( Constantes.PARAM_DIAMES ) != null ) {
			diaMes = Integer.parseInt( filtros.get( Constantes.PARAM_DIAMES ) );
		}
		if ( filtros.get( Constantes.PARAM_IDMES ) != null ) {
			mes = boMes.findById( Integer.parseInt( filtros.get( Constantes.PARAM_IDMES ) ) );
		}
		if ( filtros.get( Constantes.PARAM_DIASEMANA ) != null ) {
			diaSemana = boDiaSemana.findById( Integer.parseInt( filtros.get( Constantes.PARAM_DIASEMANA ) ) );
		}
		if ( filtros.get( Constantes.PARAM_EXCEPCIONARFESTIVOS ) != null ) {
			excepcionarFestivos = Boolean.valueOf( filtros.get( Constantes.PARAM_EXCEPCIONARFESTIVOS ) );
		}
		if ( filtros.get( Constantes.PARAM_LABORABLEANTERIOR ) != null ) {
			laborableAnterior = Boolean.valueOf( filtros.get( Constantes.PARAM_LABORABLEANTERIOR ) );
		}
		if ( filtros.get( Constantes.PARAM_LABORABLEPOSTERIOR ) != null ) {
			laborablePosterior = Boolean.valueOf( filtros.get( Constantes.PARAM_LABORABLEPOSTERIOR ) );
		}

		hora = Integer.parseInt( filtros.get( Constantes.PARAM_HORA ) );
		minutos = Integer.parseInt( filtros.get( Constantes.PARAM_MINUTO ) );

		if ( filtros.get( Constantes.PARAM_MARGENANTERIOR ) != null ) {
			margenAnterior = Integer.parseInt( filtros.get( Constantes.PARAM_MARGENANTERIOR ) );
		}
		if ( filtros.get( Constantes.PARAM_MARGENANTERIOR ) != null ) {
			margenPosterior = Integer.parseInt( filtros.get( Constantes.PARAM_MARGENPOSTERIOR ) );
		}

		Date fechaDesde = new Date();
		Date fechaHasta = null;
		SimpleDateFormat textFormat = new SimpleDateFormat( "yyyyMMdd" );
		String paramDateAsString = "99991231";

		try {
			fechaHasta = textFormat.parse( paramDateAsString );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}

		try {
			if ( filtros.get( Constantes.PARAM_FECHADESDE ) != null ) {
				fechaDesde = textFormat.parse( filtros.get( Constantes.PARAM_FECHADESDE ) );
			}
			if ( filtros.get( Constantes.PARAM_FECHAHASTA ) != null ) {
				fechaHasta = textFormat.parse( filtros.get( Constantes.PARAM_FECHAHASTA ) );
			}
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}

		java.sql.Date desdeSql = new java.sql.Date( fechaDesde.getTime() );
		java.sql.Date hastaSql = new java.sql.Date( fechaHasta.getTime() );

		Boolean activo = Boolean.valueOf( filtros.get( Constantes.PARAM_ACTIVO ) );
		ReglaPeriodicidad regla = boRegla.findByIdReglaUsuario( filtros.get( Constantes.PARAM_IDREGLAUSUARIO ) );

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result = false;

		param = boParam.createParametrizacionPeriodicidad( diaMes, excepcionarFestivos, laborableAnterior, laborablePosterior, hora,
				minutos, margenAnterior, margenPosterior, desdeSql, hastaSql, activo, mes, diaSemana, regla );

		if ( param != null ) {
			result = true;
		}
		resultado.put( "result", result );
		return resultado;
	}
	// -------------------------------------UPDATE PARAMETRIZACION-----------------------//
	public Map< String, Boolean > updateParametrizacion( Map< String, String > filtros ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );

		Long id = Long.parseLong( filtros.get( Constantes.PARAM_ID ) );

		Integer diaMes = null;

		if ( filtros.get( Constantes.PARAM_DIAMES ) != null ) {
			diaMes = Integer.parseInt( filtros.get( Constantes.PARAM_DIAMES ) );
		}

		Boolean excepcionarFestivos = false;
		Boolean laborableAnterior = false;
		Boolean laborablePosterior = false;

		if ( filtros.get( Constantes.PARAM_EXCEPCIONARFESTIVOS ) != null ) {
			excepcionarFestivos = Boolean.valueOf( filtros.get( Constantes.PARAM_EXCEPCIONARFESTIVOS ) );
		}

		if ( filtros.get( Constantes.PARAM_LABORABLEANTERIOR ) != null ) {
			laborableAnterior = Boolean.valueOf( filtros.get( Constantes.PARAM_LABORABLEANTERIOR ) );
		}

		if ( filtros.get( Constantes.PARAM_LABORABLEPOSTERIOR ) != null ) {
			laborablePosterior = Boolean.valueOf( filtros.get( Constantes.PARAM_LABORABLEPOSTERIOR ) );
		}

		Integer hora = Integer.parseInt( filtros.get( Constantes.PARAM_HORA ) );
		Integer minuto = Integer.parseInt( filtros.get( Constantes.PARAM_MINUTO ) );

		// Estos valores vienen por defecto en la documentacion
		Integer margenAnterior = 0;
		Integer margenPosterior = 30;

		if ( filtros.get( Constantes.PARAM_MARGENANTERIOR ) != null ) {
			margenAnterior = Integer.parseInt( filtros.get( Constantes.PARAM_MARGENANTERIOR ) );
		}

		if ( filtros.get( Constantes.PARAM_MARGENANTERIOR ) != null ) {
			margenPosterior = Integer.parseInt( filtros.get( Constantes.PARAM_MARGENPOSTERIOR ) );
		}

		Date fechaDesde = new Date();
		Date fechaHasta = null;
		String paramDateAsString = "99991231";

		try {
			fechaHasta = dateFormat.parse( paramDateAsString );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}

		try {
			if ( filtros.get( Constantes.PARAM_FECHADESDE ) != null ) {
				fechaDesde = dateFormat.parse( filtros.get( Constantes.PARAM_FECHADESDE ) );
			}
			if ( filtros.get( Constantes.PARAM_FECHAHASTA ) != null ) {
				fechaHasta = dateFormat.parse( filtros.get( Constantes.PARAM_FECHAHASTA ) );
			}
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}

		java.sql.Date desdeSql = new java.sql.Date( fechaDesde.getTime() );
		java.sql.Date hastaSql = new java.sql.Date( fechaHasta.getTime() );

		Boolean activo = Boolean.valueOf( filtros.get( Constantes.PARAM_ACTIVO ) );
		String idReglaUsuario = filtros.get( Constantes.PARAM_IDREGLAUSUARIO );

		Integer idMes = null;

		if ( filtros.get( Constantes.PARAM_IDMES ) != null ) {
			idMes = Integer.parseInt( filtros.get( Constantes.PARAM_IDMES ) );
		}

		Integer idDiaSemana = null;
		if ( filtros.get( Constantes.PARAM_DIASEMANA ) != null ) {
			idDiaSemana = Integer.parseInt( filtros.get( Constantes.PARAM_DIASEMANA ) );
		}

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		result = boParam.updateParametrizacion( id, diaMes, excepcionarFestivos, laborableAnterior, laborablePosterior, hora, minuto,
				margenAnterior, margenPosterior, desdeSql, hastaSql, activo, idReglaUsuario, idMes, idDiaSemana );

		resultado.put( "result", result );
		return resultado;
	}

	// -------------------------UPDATE ACTIVO PARAMETRIZACION-----------------------------//
	public Map< String, Boolean > updateActivoParametrizacion( Map< String, String > filtros ) {

		Long id = Long.parseLong( filtros.get( Constantes.PARAM_ID ) );

		Boolean activo = Boolean.valueOf( filtros.get( Constantes.PARAM_ACTIVO ) );

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		result = boParam.updateActivoParametrizacion( id, activo );

		resultado.put( "result", result );
		return resultado;
	}

	// -----------------------------------DELETE PARAMETRIZACION------------------------------//
	public Map< String, Boolean > deleteParametrizacion( Map< String, String > filtros ) {

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		Long id = Long.parseLong( filtros.get( Constantes.PARAM_ID ) );
		result = boParam.deleteParametrizacion( id );

		resultado.put( "result", result );
		return resultado;
	}

	// --------------------------CREATE PARAMETRIZACION CHECK FILTERS------------------------//
	private boolean createParametrizacionCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;
		if ( ( filtros.get( Constantes.PARAM_DIAMES ) == null || filtros.get( Constantes.PARAM_IDMES ) == null )
				&& filtros.get( Constantes.PARAM_DIASEMANA ) == null ) {

			errorMsg = errorMsg + "The '" + Constantes.PARAM_DIAMES + "' and '" + Constantes.PARAM_IDMES
					+ "' are required, or conversely must indicate '" + Constantes.PARAM_DIASEMANA + "'. ";
			filtersRight = false;
		}
		if ( filtros.get( Constantes.PARAM_IDMES ) != null ) {
			if ( boMes.findById( Integer.parseInt( filtros.get( Constantes.PARAM_IDMES ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_IDMES + "' in the database. ";
				filtersRight = false;
			}

		}
		if ( filtros.get( Constantes.PARAM_DIASEMANA ) != null ) {
			if ( boDiaSemana.findById( Integer.parseInt( filtros.get( Constantes.PARAM_DIASEMANA ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_DIASEMANA + "' in the database. ";
				filtersRight = false;
			}

		}
		if ( filtros.get( Constantes.PARAM_FECHADESDE ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.PARAM_FECHADESDE ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' length is not correct (yyyyMMdd). ";
				webResponse.setError( errorMsg );
				return filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.PARAM_FECHADESDE ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
		}
		if ( filtros.get( Constantes.PARAM_FECHAHASTA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHAHASTA + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.PARAM_FECHAHASTA ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHAHASTA + "' length is not correct (yyyyMMdd). ";
				webResponse.setError( errorMsg );
				return filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.PARAM_FECHAHASTA ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHAHASTA + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
			Date fechaDesde = null;
			Date fechaHasta = null;
			try {
				fechaDesde = dateFormat.parse( filtros.get( Constantes.PARAM_FECHADESDE ) );
				fechaHasta = dateFormat.parse( filtros.get( Constantes.PARAM_FECHAHASTA ) );

			} catch ( ParseException e ) {
				LOG.error("Error: ", e.getClass().getName(), e.getMessage());
			}

			if ( fechaDesde.after( fechaHasta ) ) {

				errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' is after " + Constantes.PARAM_FECHAHASTA;
				LOG.error( "createParametrizacionCheckFilters(): " + errorMsg );
				filtersRight = false;
				webResponse.setError( errorMsg );
				return filtersRight;
			}
		}

		if ( filtros.get( Constantes.PARAM_HORA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_HORA + "' field is required. ";
			filtersRight = false;
		}

		if ( filtros.get( Constantes.PARAM_MINUTO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_MINUTO + "' field is required. ";
			filtersRight = false;
		}
		if ( filtros.get( Constantes.PARAM_ACTIVO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_ACTIVO + "' field is required. ";
			filtersRight = false;
		}

		if ( filtros.get( Constantes.PARAM_IDREGLAUSUARIO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_IDREGLAUSUARIO + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boRegla.findByIdReglaUsuario( filtros.get( Constantes.PARAM_IDREGLAUSUARIO ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_IDREGLAUSUARIO + "' in the database. ";
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// --------------------------------UPDATE ACTIVO PARAMETRIZACION CHECK FILTERS-------------------//
	public boolean updateActivoParametrizacionCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.PARAM_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boParam.findById( Long.parseLong( filtros.get( Constantes.PARAM_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_ID + "' in the database. ";
				filtersRight = false;
			}
		}

		if ( filtros.get( Constantes.PARAM_ACTIVO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_ACTIVO + "' field is required. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// -------------------------------UPDATE PARAMETRIZACION CHECK FILTERS------------------------------------//
	// Comprobar que los datos obligatorios vienen rellenos
	private boolean updateParametrizacionCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.PARAM_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boParam.findById( Long.parseLong( filtros.get( Constantes.PARAM_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_ID + "' in the database. ";
				filtersRight = false;
			}
		}
		if ( ( filtros.get( Constantes.PARAM_DIAMES ) == null || filtros.get( Constantes.PARAM_IDMES ) == null )
				&& filtros.get( Constantes.PARAM_DIASEMANA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_DIAMES + "' and '" + Constantes.PARAM_IDMES
					+ "' are required, or conversely must indicate " + Constantes.PARAM_DIASEMANA + "'. ";
			filtersRight = false;
		}
		if ( filtros.get( Constantes.PARAM_IDMES ) != null ) {
			if ( boMes.findById( Integer.parseInt( filtros.get( Constantes.PARAM_IDMES ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_IDMES + "' in the database. ";
				filtersRight = false;
			}

		}
		if ( filtros.get( Constantes.PARAM_DIASEMANA ) != null ) {
			if ( boDiaSemana.findById( Integer.parseInt( filtros.get( Constantes.PARAM_DIASEMANA ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_DIASEMANA + "' in the database. ";
				filtersRight = false;
			}

		}
		if ( filtros.get( Constantes.PARAM_FECHADESDE ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.PARAM_FECHADESDE ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.PARAM_FECHADESDE ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
		}
		if ( filtros.get( Constantes.PARAM_FECHAHASTA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHAHASTA + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.PARAM_FECHAHASTA ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHAHASTA + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.PARAM_FECHAHASTA ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHAHASTA + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
			SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
			Date fechaDesde = null;
			Date fechaHasta = null;
			try {
				fechaDesde = dateFormat.parse( filtros.get( Constantes.PARAM_FECHADESDE ) );
				fechaHasta = dateFormat.parse( filtros.get( Constantes.PARAM_FECHAHASTA ) );

			} catch ( ParseException e ) {
				LOG.error("Error: ", e.getClass().getName(), e.getMessage());
			}

			if ( fechaDesde.after( fechaHasta ) ) {

				errorMsg = errorMsg + "The '" + Constantes.PARAM_FECHADESDE + "' is after " + Constantes.PARAM_FECHAHASTA;
				LOG.error( "updateParametrizacionCheckFilters(): " + errorMsg );
				filtersRight = false;
				webResponse.setError( errorMsg );
				return filtersRight;
			}
		}
		if ( filtros.get( Constantes.PARAM_HORA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_HORA + "' field is required. ";
			filtersRight = false;
		} else {
			int hora = Integer.parseInt( filtros.get( Constantes.PARAM_HORA ) );
			if ( hora < 0 || hora > 23 ) {
				errorMsg = errorMsg + "The '" + Constantes.PARAM_HORA + "' has to be between 0 and 23. ";
				filtersRight = false;

			}
		}
		if ( filtros.get( Constantes.PARAM_MINUTO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_MINUTO + "' field is required. ";
			filtersRight = false;
		}
		if ( filtros.get( Constantes.PARAM_ACTIVO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_ACTIVO + "' field is required. ";
			filtersRight = false;
		}

		if ( filtros.get( Constantes.PARAM_IDREGLAUSUARIO ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_IDREGLAUSUARIO + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boRegla.findByIdReglaUsuario( filtros.get( Constantes.PARAM_IDREGLAUSUARIO ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_IDREGLAUSUARIO + "' in the database. ";
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;

	}

	// --------------------------DELETE PARAMETRIZACION CHECK FILTERS-------------------------//
	private boolean deleteParametrizacionCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;
		if ( filtros.get( Constantes.PARAM_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.PARAM_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boParam.findById( new Long( filtros.get( Constantes.PARAM_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.PARAM_ID + "' in the database. ";
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// --------------------------------------CREATE FESTIVO----------------------------------//

	public Map< String, Boolean > createFestivo( Map< String, String > filtros ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
		Date date = null;
		try {
			date = dateFormat.parse( filtros.get( Constantes.FESTIVO_FECHA ) );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}
		java.sql.Date fecha = new java.sql.Date( date.getTime() );
		String desc = filtros.get( Constantes.FESTIVO_DESC );
		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		Boolean result = false;
		if ( boFestivo.createFestivo( fecha, desc ) != null ) {
			result = true;
		}
		resultado.put( "result", result );
		return resultado;
	}

	// --------------------------------------GET FESTIVOS----------------------------------//
	private Map< String, ? extends Object > getFestivos() {
		Map< String, Object > resultadosFestivos = new Hashtable< String, Object >();
		ArrayList< Festivo > festivos = ( ArrayList< Festivo > ) boFestivo.findAll();

		for ( Festivo festivo : festivos ) {
			Map< String, String > resultadoFestivo = new Hashtable< String, String >();

			resultadoFestivo.put( Constantes.FESTIVO_FECHA, festivo.getFecha().toString() );
			resultadoFestivo.put( Constantes.FESTIVO_DESC, festivo.getDescripcion() );
			resultadosFestivos.put( festivo.getId().toString(), resultadoFestivo );
		}

		return resultadosFestivos;
	}

	// --------------------------------------UPDATE FESTIVO----------------------------------//
	public Map< String, Boolean > updateFestivo( Map< String, String > filtros ) {

		Integer id = Integer.parseInt( filtros.get( Constantes.FESTIVO_ID ) );
		String descripcion = filtros.get( Constantes.FESTIVO_DESC );

		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
		Date date = null;
		try {
			date = dateFormat.parse( filtros.get( Constantes.FESTIVO_FECHA ) );
		} catch ( ParseException e ) {
			LOG.error("Error: ", e.getClass().getName(), e.getMessage());
		}

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		java.sql.Date fechaSql = new java.sql.Date( date.getTime() );
		result = boFestivo.updateFestivo( id, fechaSql, descripcion );

		resultado.put( "result", result );
		return resultado;
	}

	// --------------------------------------DELETE FESTIVO----------------------------------//
	public Map< String, Boolean > deleteFestivo( Map< String, String > filtros ) {

		Map< String, Boolean > resultado = new Hashtable< String, Boolean >();
		boolean result;

		Integer id = Integer.parseInt( filtros.get( Constantes.FESTIVO_ID ) );
		result = boFestivo.deleteFestivo( id );

		resultado.put( "result", result );
		return resultado;
	}

	// --------------------------------------CREATE FESTIVO CHECK FILTERS----------------------------------//
	public boolean createFestivoCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.FESTIVO_FECHA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.FESTIVO_FECHA ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.FESTIVO_FECHA ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				} else {
					SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyyMMdd" );
					Date date = null;
					try {
						date = dateFormat.parse( filtros.get( Constantes.FESTIVO_FECHA ) );
					} catch ( ParseException e ) {
						LOG.error("Error: ", e.getClass().getName(), e.getMessage());
					}
					java.sql.Date dateSql = new java.sql.Date( date.getTime() );

					if ( boFestivo.findByFecha( dateSql ) != null ) {
						errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' already exists. ";
						filtersRight = false;
					}
				}
			}
		}
		if ( filtros.get( Constantes.FESTIVO_DESC ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.FESTIVO_DESC + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.FESTIVO_DESC ).length() > Constantes.SIZE_FESTIVO_DESCRIPCION ) {
				errorMsg = errorMsg + "The '" + Constantes.FESTIVO_DESC + "' has a maximum length of "
						+ Constantes.SIZE_FESTIVO_DESCRIPCION;
				filtersRight = false;
			}
		}
		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// --------------------------------------UPDATE FESTIVO CHECK FILTERS----------------------------------//
	public boolean updateFestivoCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.FESTIVO_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.FESTIVO_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boFestivo.findById( Integer.parseInt( filtros.get( Constantes.FESTIVO_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.FESTIVO_ID + "' in the database. ";
				filtersRight = false;
			}
		}
		if ( filtros.get( Constantes.FESTIVO_FECHA ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.FESTIVO_FECHA ).trim().length() != 8 ) {
				errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' length is not correct (yyyyMMdd). ";
				filtersRight = false;
			} else {
				Pattern pattern = Pattern.compile( "\\d{8}" );
				Matcher matcher = pattern.matcher( filtros.get( Constantes.FESTIVO_FECHA ) );
				if ( !matcher.find() ) {
					errorMsg = errorMsg + "The '" + Constantes.FESTIVO_FECHA + "' is not a valid date (yyyyMMdd). ";
					filtersRight = false;
				}
			}
		}
		if ( filtros.get( Constantes.FESTIVO_DESC ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.FESTIVO_DESC + "' field is required. ";
			filtersRight = false;
		} else {
			if ( filtros.get( Constantes.FESTIVO_DESC ).length() > Constantes.SIZE_FESTIVO_DESCRIPCION ) {
				errorMsg = errorMsg + "The '" + Constantes.FESTIVO_DESC + "' has a maximum length of "
						+ Constantes.SIZE_FESTIVO_DESCRIPCION;
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// --------------------------------------GET FESTIVOS----------------------------------//
	public boolean deleteFestivoCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.FESTIVO_ID ) == null ) {
			errorMsg = errorMsg + "The '" + Constantes.FESTIVO_ID + "' field is required. ";
			filtersRight = false;
		} else {
			if ( boFestivo.findById( Integer.parseInt( filtros.get( Constantes.FESTIVO_ID ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such '" + Constantes.FESTIVO_ID + "' in the database. ";
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	public static String getSimpleName() {
		return simpleName;
	}

	// ---------------------------------GET DIAS SEMANA---------------------------------------//
	private Map< String, ? extends Object > getDiasSemana() {
		Map< String, Object > resultadosSemanasFinal = new Hashtable< String, Object >();

		List< Map< String, String >> resultadosSemanas = new ArrayList< Map< String, String >>();

		List< DiaSemana > Semanas = ( List< DiaSemana > ) boDiaSemana.findAll( boDiaSemana.sortByIdAsc() );

		for ( DiaSemana semana : Semanas ) {
			Map< String, String > resultadosSemana = new Hashtable< String, String >();
			resultadosSemana.put( Constantes.SEMANA_ID, semana.getId().toString() );
			resultadosSemana.put( Constantes.SEMANA_NOMBRE, semana.getNombreDia() );
			resultadosSemanas.add( resultadosSemana );
		}

		resultadosSemanasFinal.put( Constantes.SEMANA_SEMANAS, resultadosSemanas );
		return resultadosSemanasFinal;
	}

	// ----------------------------GET MESES----------------------------------------------//
	private Map< String, ? extends Object > getMeses() {
		Map< String, Object > resultadosMesesFinal = new Hashtable< String, Object >();

		List< Map< String, String >> resultadosMeses = new ArrayList< Map< String, String >>();

		List< Mes > meses = ( List< Mes > ) boMes.findAll( boMes.sortByIdAsc() );

		for ( Mes mes : meses ) {
			Map< String, String > resultadosMes = new Hashtable< String, String >();
			resultadosMes.put( Constantes.MES_ID, mes.getId().toString() );
			resultadosMes.put( Constantes.MES_NOMBRE, mes.getNombreMes() );
			resultadosMeses.add( resultadosMes );
		}

		resultadosMesesFinal.put( Constantes.MES_MESES, resultadosMeses );
		return resultadosMesesFinal;
	}
}
