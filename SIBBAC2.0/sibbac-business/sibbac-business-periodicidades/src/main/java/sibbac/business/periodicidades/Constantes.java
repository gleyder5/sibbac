package sibbac.business.periodicidades;


public class Constantes {

	// Consulta
	public static final String	CMD_GET_REGLAS_ACTIVAS				= "getReglasActivas";
	public static final String	CMD_IS_REGLA_ACTIVA					= "isReglaActiva";
	public static final String	CMD_GET_FESTIVOS					= "getFestivos";
	public static final String	CMD_GET_REGLAS_AND_PARAMS			= "getReglasAndParams";

	// Insercion
	public static final String	CMD_CREATE_FESTIVO					= "createFestivo";
	public static final String	CMD_CREATE_REGLA					= "createRegla";
	public static final String	CMD_CREATE_PARAMETRIZACION			= "createParametrizacion";

	// Modificacion
	public static final String	CMD_UPDATE_PARAMETRIZACION			= "updateParametrizacion";
	public static final String	CMD_UPDATE_REGLA					= "updateRegla";
	public static final String	CMD_UPDATE_FESTIVO					= "updateFestivo";
	public static final String	CMD_UPDATE_ACTIVO_REGLA				= "updateActivoRegla";
	public static final String	CMD_UPDATE_ACTIVO_PARAMETRIZACION	= "updateActivoParametrizacion";
	// Borrado
	public static final String	CMD_DELETE_PARAMETRIZACION			= "deleteParametrizacion";
	public static final String	CMD_DELETE_REGLA					= "deleteRegla";
	public static final String	CMD_DELETE_FESTIVO					= "deleteFestivo";

	public static final String	CMD_GET_DIAS_SEMANA					= "getDiasSemana";
	public static final String	CMD_GET_MESES						= "getMeses";

	public static final String	PARAM_ID							= "id";
	public static final String	PARAM_DIAMES						= "diaMes";
	public static final String	PARAM_IDMES							= "idMes";
	public static final String	PARAM_DIASEMANA						= "diaSemana";
	public static final String	PARAM_EXCEPCIONARFESTIVOS			= "excepcionarFestivos";
	public static final String	PARAM_LABORABLEANTERIOR				= "laborableAnterior";
	public static final String	PARAM_LABORABLEPOSTERIOR			= "laborablePosterior";
	public static final String	PARAM_HORA							= "hora";
	public static final String	PARAM_MINUTO						= "minuto";
	public static final String	PARAM_MARGENANTERIOR				= "margenAnterior";
	public static final String	PARAM_MARGENPOSTERIOR				= "margenPosterior";
	public static final String	PARAM_FECHADESDE					= "fechaDesde";
	public static final String	PARAM_FECHAHASTA					= "fechaHasta";
	public static final String	PARAM_IDREGLAUSUARIO				= "idReglaUsuario";
	public static final String	PARAM_ACTIVO						= "activo";

	public static final String	FESTIVO_ID							= "id";
	public static final String	FESTIVO_FECHA						= "date";
	public static final String	FESTIVO_DESC						= "desc";

	public static final String	ISREGLA_DATE						= "date";
	public static final String	ISREGLA_ID							= "idReglaUsuario";

	public static final String	GETREGLAS_DATE						= "date";

	public static final String	REGLA_ID							= "id";
	public static final String	REGLA_IDREGLAUSUARIO				= "idReglaUsuario";
	public static final String	REGLA_DESCRIPCION					= "descripcion";
	public static final String	REGLA_FECHADESDE					= "fechaDesde";
	public static final String	REGLA_FECHAHASTA					= "fechaHasta";
	public static final String	REGLA_ACTIVO						= "activo";

	public static final String	REGLA_AND_PARAM_ID					= "id";
	public static final String	REGLA_AND_PARAM_ID_REGLA_USUARIO	= "idReglaUsuario";
	public static final String	REGLA_AND_PARAM_DESCRIPCION			= "descripcion";
	public static final String	REGLA_AND_PARAM_FECHADESDE			= "fechaDesde";
	public static final String	REGLA_AND_PARAM_FECHAHASTA			= "fechaHasta";
	public static final String	REGLA_AND_PARAM_ACTIVO				= "activo";
	public static final String	REGLA_AND_PARAM_PARAMETRIZACION		= "parametrizacion";
	public static final String	REGLA_AND_PARAM_IDPARAM				= "idParam";
	public static final String	REGLA_AND_PARAM_EXCEPCIONARFESTIVOS	= "excepcionarFestivos";
	public static final String	REGLA_AND_PARAM_LABORABLEANTERIOR	= "laborableAnterior";
	public static final String	REGLA_AND_PARAM_LABORABLEPOSTERIOR	= "laborablePosterior";
	public static final String	REGLA_AND_PARAM_HORA				= "hora";
	public static final String	REGLA_AND_PARAM_MINUTO				= "minuto";
	public static final String	REGLA_AND_PARAM_MARGENANTERIOR		= "margenAnterior";
	public static final String	REGLA_AND_PARAM_MARGENPOSTERIOR		= "margenPosterior";
	public static final String	REGLA_AND_PARAM_FECHADESDEPARAM		= "fechaDesdeParam";
	public static final String	REGLA_AND_PARAM_FECHAHASTAPARAM		= "fechaHastaParam";
	public static final String	REGLA_AND_PARAM_ACTIVOPARAM			= "activoParam";
	public static final String	REGLA_AND_PARAM_MES					= "mesParam";
	public static final String	REGLA_AND_PARAM_MES_NAME			= "mesParamName";
	public static final String	REGLA_AND_PARAM_DIASEMANA			= "diaSemanaParam";
	public static final String	REGLA_AND_PARAM_DIASEMANA_NAME		= "diaSemanaParamName";
	public static final String	REGLA_AND_PARAM_DIAMES				= "diaMesParam";

	public static final Integer	SIZE_REGLA_IDREGLAUSUARIO			= 16;
	public static final Integer	SIZE_REGLA_DESCRIPCION				= 255;
	public static final Integer	SIZE_FESTIVO_DESCRIPCION			= 64;

	// ------------------SERVICIOS----------------------------------------------

	// -----COMMANDS----------------------------
	public static final String	CMD_GET_COMBINACION					= "getCombinacion";
	public static final String	CMD_GET_SERVICIOS					= "getServicios";
	public static final String	CMD_GET_REGLAS						= "getReglas";
	public static final String	CMD_ALTA_SERVICIO					= "altaServicio";
	public static final String	CMD_ALTA_SERVICIOCONTACTO			= "altaServicioContacto";
	public static final String	CMD_BAJA_SERVICIO					= "bajaServicio";
	public static final String	CMD_BAJA_SERVICIOCONTACTO			= "bajaServicioContacto";
	public static final String	CMD_UPDATE_ACTIVO_SERVICIO			= "updateActivoServicio";
	public static final String	CMD_UPDATE_SERVICIO_REGLA			= "updateServicioRegla";
	public static final String	CMD_GET_CONTACTOS_BY_SERVICE		= "getContactosByService";
	public static final String	CMD_GET_CONTACTOS_SERVICIOS			= "getContactosServicios";

	// ----FIELDS--------------------------------
	public static final String	ID_SERVICIO							= "id";
	public static final String	NOMBRE_SERVICIO						= "nombreServicio";
	public static final String	DESCRIPCION_SERVICIO				= "descripcionServicio";
	public static final String	ACTIVO_SERVICIO						= "activoServicio";
	public static final String	ID_ALIAS_SERVICIO					= "idAlias";
	public static final String	ID_CONTACTO_SERVICIO				= "idContacto";
	public static final String	ID_REGLA_SERVICIO					= "idRegla";
	public static final String	ID_SERVICIO_SERVICIO				= "idServicio";
	public static final String	ID_ALIAS							= "idAlias";
	public static final String	ALIAS_DESCRIPCION					= "descAlias";
	public static final String	CONTACTO_ID							= "idContacto";
	public static final String	CONTACTO_ACTIVO						= "activoContacto";
	public static final String	CONTACTO_PAPELLIDO					= "pApellidoContacto";
	public static final String	CONTACTO_SAPELLIDO					= "sApellidoContacto";
	public static final String	CONTACTO_COMENTARIOS				= "comentariosContacto";
	public static final String	CONTACTO_DEFECTO					= "defectoContacto";
	public static final String	CONTACTO_NOMBRED					= "nombredContacto";
	public static final String	CONTACTO_EMAIL						= "emailContacto";
	public static final String	CONTACTO_FAX						= "faxContacto";
	public static final String	CONTACTO_MOVIL						= "movilContacto";
	public static final String	CONTACTO_NOMBRE						= "nombreContacto";
	public static final String	CONTACTO_POSICION					= "posicionContacto";
	public static final String	CONTACTO_TELEFONO1					= "telefonoAContacto";
	public static final String	CONTACTO_TELEFONO2					= "telefonoBContacto";
	public static final String	SERVICIO_ID							= "idServicio";
	public static final String	SERVICIO_NOMBRE						= "nombreServicio";
	public static final String	SERVICIO_DESCRIPCION				= "descripcionServicio";
	public static final String	SERVICIO_CONFIG_ALIAS				= "configuracionDeAlias";
	public static final String	SERVICIO_ACTIVO						= "activo";
	public static final String	SERVICIO_REGLAS						= "reglasServicio";
	public static final String	CONTACTO_SERVICIOS					= "servicios";
	public static final String	CONTACTOS_TIME						= "momento";
	public static final String	CONTACTOS_ID_SERVICIO				= "idServicio";
	public static final String	GETLIST_CONTACTOS					= "contactos";
	public static final String	GETLIST_REGLAS						= "reglas";
	public static final String	GETLIST_SERVICIOS					= "servicios";
	public static final String	GETLIST_COMBINACION					= "combinacion";
	// -----SIZE--------------------------------
	public static final Integer	SIZE_NOMBRE_SERVICIO				= 20;
	public static final Integer	SIZE_DESCRIPCION_SERVICIO			= 255;

	public static final String	MES_ID								= "id";
	public static final String	MES_NOMBRE							= "nombre";
	public static final String	MES_MESES							= "meses";
	public static final String	SEMANA_ID							= "id";
	public static final String	SEMANA_NOMBRE						= "nombre";
	public static final String	SEMANA_SEMANAS						= "semanas";

}
