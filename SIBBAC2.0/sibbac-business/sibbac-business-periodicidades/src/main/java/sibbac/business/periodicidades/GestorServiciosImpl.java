package sibbac.business.periodicidades;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.bo.ReglasLanzadorServiciosBo;
import sibbac.business.periodicidades.database.bo.ServiciosContactosBo;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios;
import sibbac.business.wrappers.GestorServicios;
import sibbac.business.wrappers.database.bo.ContactoBo;
import sibbac.business.wrappers.database.bo.ServiciosBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.tasks.database.Job;
import sibbac.tasks.database.JobBO;
import sibbac.tasks.database.JobPK;


@Component
public class GestorServiciosImpl implements GestorServicios {

	protected static final Logger		LOG	= LoggerFactory.getLogger( GestorServiciosImpl.class );

	/** Inject ReglasLanzadorServiciosBo */
	@Autowired
	protected ReglasLanzadorServiciosBo	lanzadorBo;

	/** Inject ServiciosBo */
	@Autowired
	protected ServiciosBo				serviciosBo;

	/** Inject GestorPeriodicidades */
	@Autowired
	protected GestorPeriodicidades		gestor;

	/** Inject JobBO */
	@Autowired
	protected JobBO						jobBo;

	/** Inject ServiciosContactosBo */
	@Autowired
	protected ServiciosContactosBo		serviciosContactosBo;

	/** Inject ContactoBo */
	@Autowired
	protected ContactoBo				contactosBo;

	public GestorServiciosImpl() {

	};

	/**
	 * isActive
	 * 
	 * @param jobPk
	 * @return List of Alias
	 */
	@Override
	public List< Alias > isActive( JobPK jobPk ) {
		String prefix = "[GestorServiciosImpl::isActive(jobPk)] [job::"+jobPk+"] ";

		Date time = new Date();
		LOG.trace( prefix + "Invocando al otro metodo con fecha: [{}]", time );
		return this.isActive( jobPk, time );
	}

	/**
	 * isActive
	 * 
	 * @param jobPk
	 * @param time
	 * @return List of Alias
	 */
	@Override
	public List< Alias > isActive( JobPK jobPk, Date time ) {
		String prefix = "[GestorServiciosImpl::isActive(jobPk,time)] [job::"+jobPk+"] ";
		LOG.trace( prefix + "INI Peticion" );
		List< Alias > alias = new ArrayList< Alias >();
		try {
			// Localizamos el Job que se nos pasa.
			Job job = this.jobBo.findById( jobPk );

			// Recuperamos el servicio que esta asociado a este Job
			Servicios servicio = serviciosBo.findServiciosByJob( job );
			if ( time == null ) {
				time = new Date();
			}

			// Recuperamos las reglas que estan activas en la fecha que
			// recibimos
			List< ReglaPeriodicidad > reglas = gestor.getReglasActivas( time );

			// Recuperamos las ternas que tiene las reglas activas en el momento
			// deseado (time) y el servicio deseado
			List< ReglasLanzadorServicios > lanzadorServicios = lanzadorBo.findAllByReglaInAndServicio( reglas, servicio );

			for ( ReglasLanzadorServicios lanzadorServicio : lanzadorServicios ) {
				alias.add( lanzadorServicio.getAlias() );
			}
		} catch ( Exception e ) {
			LOG.error( e.getCause().toString() );
		}
		LOG.trace( prefix + "Solicitados los alias para: [{}]. Devolviendo: [{}]", time, alias.size() );
		return alias;
	}

	/**
	 * getContactos
	 * 
	 * @param alias
	 * @param jobPk
	 * @return List of Contacto
	 */
	@Override
	public List< Contacto > getContactos( Alias alias, JobPK jobPk ) {
		String prefix = "[GestorServiciosImpl::getContactos(alias,jobPk)] [job::"+jobPk+"] ";

		Date time = new Date();
		LOG.trace( prefix + "Invocando al otro metodo con alias: [{}], y fecha: [{}]", alias, time );
		return this.getContactos( alias, jobPk, time );
	}


	/**
	 * getContactos
	 * 
	 * @param alias
	 * @param jobPk
	 * @param time
	 * @return List of Contacto
	 */
	@Override
	public List< Contacto > getContactos( Alias alias, JobPK jobPk, Date time ) {
		String prefix = "[GestorServiciosImpl::getContactos(alias,jobPk,time)] [job::"+jobPk+"] ";
		LOG.trace( prefix + "INI Peticion" );
		LOG.trace( prefix + "[Alias=={}]", alias );
		LOG.trace( prefix + "[JobPK=={}]", jobPk );
		LOG.trace( prefix + "[ Date=={}]", time );
		if ( time == null ) {
			time = new Date();
			LOG.trace( prefix + "[*Date=={}]", time );
		}

		List< Contacto > contactos = new ArrayList< Contacto >();

		// Localizamos el Job que se nos pasa.
		Job job = null;
		Servicios servicio = null;

		LOG.trace( prefix + "Localizando el Job..." );
		try {
			job = this.jobBo.findById( jobPk );
			if ( job == null ) {
				LOG.trace( prefix + "WARN Job no encontrado!" );
				return contactos;
			}
		} catch ( Exception e ) {
			LOG.error( prefix + "ERROR({}): Error recuperando el Job: [{}]", e.getClass().getName(), e.getCause().toString() );
		}

		LOG.trace( prefix + "Job encontrado. Localizando el servicio..." );

		// Recuperamos el servicio que esta asociado a este Job
		try {
			servicio = serviciosBo.findServiciosByJob( job );
			if ( servicio == null ) {
				LOG.trace( prefix + "WARN Servicio no encontrado!" );
				return contactos;
			}
		} catch ( Exception e ) {
			LOG.error( prefix + "ERROR({}): Error recuperando el Servicio: [{}]", e.getClass().getName(), e.getCause().toString() );
		}
		LOG.trace( prefix + "Servicio encontrado." );

		
		contactos = serviciosContactosBo.findAllContactosValidos( servicio, alias );
		if ( contactos==null ) {
			contactos = new ArrayList< Contacto >();
		}

		LOG.trace( prefix + "Solicitados los contactos validos del alias: [{}] y el momento: [{}]. Devolviendo: [{}]", alias.getCdaliass(), time, contactos.size() );
		return contactos;
	}
}
