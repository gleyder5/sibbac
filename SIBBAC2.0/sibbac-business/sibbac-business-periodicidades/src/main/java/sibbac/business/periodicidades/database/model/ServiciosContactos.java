package sibbac.business.periodicidades.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.poi.ss.formula.functions.T;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.database.DBConstants;


/**
 * Entity: "ServiciosContactos".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.SERVICIOS.SERVICIOS_CONTACTOS )
@Entity
@XmlRootElement
@Audited
public class ServiciosContactos {

	// ------------------------------------------ Static Bean properties
	@Embeddable
	public static class PkServiciosContactos implements Serializable {

		@Column( name = "CONTACTOID", nullable = false, updatable = false )
		private Long	contactoid;

		@Column( name = "SERVICIOID", nullable = false, updatable = false )
		private Long	servicioId;

		public PkServiciosContactos() {
		}

		public PkServiciosContactos( Long contactoid, Long servicioId ) {
			this.contactoid = contactoid;
			this.servicioId = servicioId;
		}

		// getters, setters, equals, hashCode
		public Long getContactoId() {
			return contactoid;
		}

		public Long getServicioId() {
			return servicioId;
		}

		public void setContactoId( Long contactoid ) {
			this.contactoid = contactoid;
		}

		public void setServicioId( Long servicioId ) {
			this.servicioId = servicioId;
		}

		public int hashCode() {
			final HashCodeBuilder hcb = new HashCodeBuilder();
			hcb.append( contactoid );
			hcb.append( servicioId );
			return hcb.toHashCode();
		}

		public String toString() {
			final ToStringBuilder tsb = new ToStringBuilder( this, ToStringStyle.SHORT_PREFIX_STYLE );
			tsb.append( this.getContactoId() );
			tsb.append( this.getServicioId() );
			return tsb.toString();
		}

		@Override
		public boolean equals( Object obj ) {
			if ( obj == null ) {
				return false;
			}
			PkServiciosContactos pk = ( PkServiciosContactos ) obj;
			if ( this.contactoid.equals( pk.contactoid ) && this.servicioId.equals( pk.servicioId ) ) {
				return true;
			}
			return false;
		}

	}

	@EmbeddedId
	private PkServiciosContactos	pkServiciosContactos;

	// --------------------------------
	@ManyToOne( targetEntity = Contacto.class )
	@JoinColumn( name = "CONTACTOID", nullable = false, referencedColumnName = "ID", insertable = false, updatable = false )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Contacto				contacto;

	@ManyToOne( targetEntity = Servicios.class )
	@JoinColumn( name = "SERVICIOID", nullable = true, referencedColumnName = "ID", insertable = false, updatable = false )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Servicios				servicio;

	// ----------------------------------------------- Bean Constructors

	public ServiciosContactos() {
		super();
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	public Contacto getContacto() {
		return contacto;
	}

	public Servicios getServicio() {
		return servicio;
	}

	public PkServiciosContactos getPk() {
		return pkServiciosContactos;
	}

	public void setContacto( Contacto contacto ) {
		this.contacto = contacto;
	}

	public void setServicio( Servicios servicio ) {
		this.servicio = servicio;
	}

	public void setPk( PkServiciosContactos pk ) {
		this.pkServiciosContactos = pk;
	}

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	public int compareTo( final ServiciosContactos other ) {
		// Entity concrete fields.
		int internal = 0;
		return ( internal == 0 ) ? 0 : -2;
	}

	/**
	 * Pk to String
	 */
	public String toString() {
		return "[SERVICIOSCONTACTOS== Contacto[" + this.contacto + "] Servicio [" + this.servicio + "]";
	}
}
