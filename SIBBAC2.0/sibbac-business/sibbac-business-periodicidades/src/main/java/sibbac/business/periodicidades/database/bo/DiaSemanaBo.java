package sibbac.business.periodicidades.database.bo;


import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.periodicidades.database.dao.DiaSemanaDao;
import sibbac.business.periodicidades.database.model.DiaSemana;
import sibbac.database.bo.AbstractBo;


@Service
public class DiaSemanaBo extends AbstractBo< DiaSemana, Integer, DiaSemanaDao > {

	public DiaSemana findById( final Integer id ) {

		return this.dao.findById( id );
	}

	public Sort sortByIdAsc() {
		return new Sort( Sort.Direction.ASC, "id" );
	}

	public DiaSemana createDiaSemana( Integer idDiaSemana, String nombreDia ) {
		DiaSemana dto = new DiaSemana();
		dto.setId( idDiaSemana );
		dto.setNombreDia( nombreDia );
		return this.dao.save( dto );
	}

	public boolean deleteDiaSemana( Integer id ) {
		DiaSemana toDelete = this.findById( id );

		if ( toDelete == null ) {
			return false;
		}
		this.dao.delete( toDelete );
		return true;

	}

}
