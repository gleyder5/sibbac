package sibbac.business.periodicidades.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.periodicidades.database.dao.ReglasLanzadorServiciosDao;
import sibbac.business.periodicidades.database.dto.ReglasLanzadorServiciosDTO;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios.Pk;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.database.bo.AbstractBo;


@Service
public class ReglasLanzadorServiciosBo extends AbstractBo< ReglasLanzadorServicios, Pk, ReglasLanzadorServiciosDao > {

	/**
	 * findAllByAliasOrderByAlias
	 * 
	 * @param alias
	 * @return List ReglasLanzadorServicios
	 */
	public List< ReglasLanzadorServicios > findAllByAliasOrderByAlias( Alias alias ) {
		return this.dao.findAllByAliasOrderByAlias( alias );
	}

	/**
	 * findAllByReglaInAndServicioAndAlias
	 * 
	 * @param reglas
	 * @param servicio
	 * @param alias
	 * @return ReglasLanzadorServicios
	 */
	public ReglasLanzadorServicios findAllByReglaInAndServicioAndAlias( List< ReglaPeriodicidad > reglas, Servicios servicio, Alias alias ) {
		return this.dao.findAllByReglaInAndServicioAndAlias( reglas, servicio, alias );
	}

	/**
	 * findAllByReglaInAndServicio
	 * 
	 * @param reglas
	 * @param servicio
	 * @return List ReglasLanzadorServicios
	 */
	public List< ReglasLanzadorServicios > findAllByReglaInAndServicio( List< ReglaPeriodicidad > reglas, Servicios servicio ) {
		return this.dao.findAllByReglaInAndServicio( reglas, servicio );
	}

	/**
	 * getListaReglasLanzadorServicios
	 * 
	 * @return List ReglasLanzadorServicios
	 */
	public List< ReglasLanzadorServiciosDTO > getListaReglasLanzadorServicios() {
		return this.dao.findAllReglasLanzadorServicios();
	}

	/**
	 * altaServicio
	 * 
	 * @param alias
	 * @param servicio
	 * @param regla
	 * @return ReglasLanzadorServicios
	 */
	public ReglasLanzadorServicios altaServicio( Alias alias, Servicios servicio, ReglaPeriodicidad regla ) {
		ReglasLanzadorServicios entity = new ReglasLanzadorServicios();
		Pk pk = new Pk( alias.getId(), servicio.getId() );
		entity.setPk( pk );
		entity.setServicio( servicio );
		entity.setAlias( alias );
		entity.setRegla( regla );

		ReglasLanzadorServicios lanzadorServicio = null;

		try {
			lanzadorServicio = this.dao.save( entity );
		} catch ( Exception e ) {
			LOG.error( "Error creating ReglasLanzadorServicios" );
		}

		return lanzadorServicio;
	}

	/**
	 * bajaServicio
	 * 
	 * @param entity
	 */
	public void bajaServicio( ReglasLanzadorServicios entity ) {
		try {
			this.dao.delete( entity );
		} catch ( Exception e ) {
			LOG.error( "Error deleting ReglasLanzadorServicios" );
		}
	}

	/**
	 * updateServicio
	 * 
	 * @param alias
	 * @param servicio
	 * @param regla
	 * @return boolean
	 */
	public boolean updateServicio( Alias alias, Servicios servicio, ReglaPeriodicidad regla ) {

		Pk pk = new Pk( alias.getId(), servicio.getId() );

		ReglasLanzadorServicios lanzadorServicio = this.findById( pk );

		if ( lanzadorServicio == null ) {
			return false;
		}
		lanzadorServicio.setRegla( regla );

		try {
			this.dao.save( lanzadorServicio );
		} catch ( Exception e ) {
			LOG.error( "ReglasLanzadorServiciosBo.java : Error updating ReglasLanzadorServicios <updateServicio()>" + e );
		}
		return true;
	}

}
