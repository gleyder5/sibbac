package sibbac.business.periodicidades.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.poi.ss.formula.functions.T;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.database.DBConstants;


/**
 * Entity: "ReglasLanzadorServicios".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.SERVICIOS.REGLAS_LANZADOR_SERVICIOS )
@Entity
// @Component
@XmlRootElement
@Audited
public class ReglasLanzadorServicios {

	// ------------------------------------------ Static Bean properties
	@Embeddable
	public static class Pk implements Serializable {

		@Column( name = "ALIASID", nullable = false, updatable = false )
		private Long	aliasId;

		@Column( name = "SERVICIOID", nullable = false, updatable = false )
		private Long	servicioId;

		public Pk() {
		}

		public Pk( Long aliasId, Long servicioId ) {
			this.aliasId = aliasId;
			this.servicioId = servicioId;
		}

		// getters, setters, equals, hashCode
		public Long getAliasId() {
			return aliasId;
		}

		public Long getServicioId() {
			return servicioId;
		}

		public void setAliasId( Long aliasId ) {
			this.aliasId = aliasId;
		}

		public void setServicioId( Long servicioId ) {
			this.servicioId = servicioId;
		}

		public int hashCode() {
			final HashCodeBuilder hcb = new HashCodeBuilder();
			hcb.append( aliasId );
			hcb.append( servicioId );
			return hcb.toHashCode();
		}

		public String toString() {
			final ToStringBuilder tsb = new ToStringBuilder( this, ToStringStyle.SHORT_PREFIX_STYLE );
			tsb.append( this.getAliasId() );
			tsb.append( this.getServicioId() );
			return tsb.toString();
		}

		@Override
		public boolean equals( Object obj ) {
			if ( obj == null ) {
				return false;
			}
			Pk pk = ( Pk ) obj;
			if ( this.aliasId.equals( pk.aliasId ) && this.servicioId.equals( pk.servicioId ) ) {
				return true;
			}
			return false;
		}

	}

	@EmbeddedId
	private Pk					pk;

	// --------------------------------
	@ManyToOne( targetEntity = Alias.class )
	@JoinColumn( name = "ALIASID", nullable = false, referencedColumnName = "ID", insertable = false, updatable = false )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Alias				alias;

	@ManyToOne( targetEntity = Servicios.class )
	@JoinColumn( name = "SERVICIOID", nullable = true, referencedColumnName = "ID", insertable = false, updatable = false )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Servicios			servicio;

	@ManyToOne( targetEntity = ReglaPeriodicidad.class )
	@JoinColumn( name = "REGLAID", nullable = true, referencedColumnName = "ID", insertable = true, updatable = true )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private ReglaPeriodicidad	regla;

	// ----------------------------------------------- Bean Constructors

	public ReglasLanzadorServicios() {
		super();
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	public Alias getAlias() {
		return alias;
	}

	public Servicios getServicio() {
		return servicio;
	}

	public ReglaPeriodicidad getRegla() {
		return regla;
	}

	public Pk getPk() {
		return pk;
	}

	public void setAlias( Alias alias ) {
		this.alias = alias;
	}

	public void setServicio( Servicios servicio ) {
		this.servicio = servicio;
	}

	public void setRegla( ReglaPeriodicidad regla ) {
		this.regla = regla;
	}

	public void setPk( Pk pk ) {
		this.pk = pk;
	}

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	public int compareTo( final ReglasLanzadorServicios other ) {
		// Entity concrete fields.
		int internal = 0;
		return ( internal == 0 ) ? 0 : -3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[REGLASLANZADORSERVICIOS== Alias[" + this.alias + "] Servicio [" + this.servicio + "] Regla [" + this.regla + "]";
	}
}
