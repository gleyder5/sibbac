package sibbac.business.periodicidades.database.dto;


import sibbac.business.periodicidades.database.model.ServiciosContactos;


public class ServiciosContactosDTO {

	private Long	idContacto;
	private String	descrContacto;
	private Long	idServicio;
	private String	descrServicio;

	public ServiciosContactosDTO( ServiciosContactos entity ) {
		this.entityToDto( entity );
	}

	public ServiciosContactosDTO( Long idContacto, String descrContacto, Long idServicio, String descrServicio ) {
		this.idContacto = idContacto;
		this.descrContacto = descrContacto;
		this.idServicio = idServicio;
		this.descrServicio = descrServicio;
	}

	public ServiciosContactosDTO entityToDto( ServiciosContactos entity ) {
		this.idContacto = entity.getContacto().getId();
		this.descrContacto = entity.getContacto().getDescripcion();
		this.idServicio = entity.getServicio().getId();
		this.descrServicio = entity.getServicio().getNombreServicio() + entity.getServicio().getDescripcionServicio();

		return this;
	}

	public Long getIdContacto() {
		return idContacto;
	}

	public void setIdContacto( Long idContacto ) {
		this.idContacto = idContacto;
	}

	public String getDescrContacto() {
		return descrContacto;
	}

	public void setDescrContacto( String descrContacto ) {
		this.descrContacto = descrContacto;
	}

	public Long getIdServicio() {
		return idServicio;
	}

	public void setIdServicio( Long idServicio ) {
		this.idServicio = idServicio;
	}

	public String getDescrServicio() {
		return descrServicio;
	}

	public void setDescrServicio( String descrServicio ) {
		this.descrServicio = descrServicio;
	}

}
