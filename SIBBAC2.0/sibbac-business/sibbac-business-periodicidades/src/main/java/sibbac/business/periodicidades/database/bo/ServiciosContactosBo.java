package sibbac.business.periodicidades.database.bo;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.periodicidades.database.dao.ServiciosContactosDao;
import sibbac.business.periodicidades.database.model.ServiciosContactos;
import sibbac.business.periodicidades.database.model.ServiciosContactos.PkServiciosContactos;
import sibbac.business.wrappers.database.bo.ContactoBo;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.database.bo.AbstractBo;


@Service
public class ServiciosContactosBo extends AbstractBo< ServiciosContactos, PkServiciosContactos, ServiciosContactosDao > {

	@Autowired
	private ContactoBo contactosBo;

	public List< Contacto > findAllContactosValidos( Servicios servicio, Alias alias ) {
		List<ServiciosContactos> serviciosContactos = this.dao.findAllContactosByServicioAndContacto_aliasAndContactoActivo( servicio, alias, Contacto.ACTIVO_S );
		if ( serviciosContactos==null || serviciosContactos.isEmpty() ) {
			return null;
		} else {
			List< Contacto > contactosValidos = new ArrayList< Contacto >();
			for ( ServiciosContactos sc : serviciosContactos ) {
				contactosValidos.add( sc.getContacto() );
			}
			return contactosValidos;
		}
	}
	
	public List< ServiciosContactos > findServiciosContactosByContactoInAndServicio( List< Contacto > contactos, Servicios servicio ) {
		return this.dao.findServiciosContactosByContactoInAndServicio( contactos, servicio );
	}

	public Map< Object, Object > findServiciosContactosByContactoIn( List< Contacto > contactos ) {
		Map< Object, Object > resultado = new HashMap< Object, Object >();
		List< ServiciosContactos > listServicios = dao.findServiciosContactosByContactoIn( contactos );
		for ( ServiciosContactos servicio : listServicios ) {
			resultado.put( servicio.getContacto(), servicio.getServicio() );
		}

		return resultado;
	}

	public ServiciosContactos altaServicioContacto( Contacto contacto, Servicios servicio ) {

		ServiciosContactos entity = new ServiciosContactos();
		PkServiciosContactos pk = new PkServiciosContactos( contacto.getId(), servicio.getId() );
		entity.setPk( pk );
		entity.setServicio( servicio );
		entity.setContacto( contacto );

		ServiciosContactos lanzadorServicio = null;

		try {
			lanzadorServicio = this.dao.save( entity );
		} catch ( Exception e ) {
			LOG.error( "Error creating ServiciosContactos" );
		}

		return lanzadorServicio;
	}

	public void bajaServicioContacto( ServiciosContactos entity ) {
		try {
			this.dao.delete( entity );
		} catch ( Exception e ) {
			LOG.error( "Error deleting ServiciosContactos" );
		}
	}

	public List< ServiciosContactos > findAllByContactoIn( List< Contacto > contactos ) {
		return this.dao.findAllByContactoIn( contactos );
	}
}
