package sibbac.business.periodicidades.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.model.Mes;


@Component
public interface MesDao extends JpaRepository< Mes, Integer > {

	Mes findById( final Integer id );

	Mes findByNombreMes( final String name );

}
