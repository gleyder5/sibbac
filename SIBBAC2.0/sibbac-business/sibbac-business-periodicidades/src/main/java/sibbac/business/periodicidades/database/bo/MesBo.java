package sibbac.business.periodicidades.database.bo;


import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import sibbac.business.periodicidades.database.dao.MesDao;
import sibbac.business.periodicidades.database.model.Mes;
import sibbac.database.bo.AbstractBo;


@Service
public class MesBo extends AbstractBo< Mes, Integer, MesDao > {

	public Mes findById( final Integer id ) {
		return this.dao.findById( id );
	}

	public Sort sortByIdAsc() {
		return new Sort( Sort.Direction.ASC, "id" );
	}

	public Mes createMes( Integer idMes, String nombreMes ) {
		Mes dto = new Mes();
		dto.setId( idMes );
		dto.setNombreMes( nombreMes );
		return this.dao.save( dto );
	}

	public boolean deleteMes( Integer id ) {

		Mes toDelete = this.findById( id );
		if ( toDelete == null ) {
			return false;
		}
		this.dao.delete( toDelete );
		return true;
	}

}
