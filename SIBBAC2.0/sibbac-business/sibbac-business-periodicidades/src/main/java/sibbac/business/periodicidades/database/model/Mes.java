package sibbac.business.periodicidades.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;


/**
 * Entity: "Mes".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = DBConstants.PERIODICIDADES.MES )
@Entity
// @Component
@XmlRootElement
@Audited
public class Mes {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties
	/**
	 * The entity id.
	 */
	@Id
	@Column( name = "ID", nullable = false, unique = true )
	@XmlAttribute
	private Integer	id;

	@Column( name = "NOMBREMES", nullable = false, unique = true, length = 25 )
	@XmlAttribute
	private String	nombreMes;

	// ----------------------------------------------- Bean Constructors

	public Mes() {

	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for id
	 *
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Getter for nombreMes
	 *
	 * @return the nombreMes
	 */
	public String getNombreMes() {
		return this.nombreMes;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for id
	 *
	 * @param id the id to set
	 */
	public void setId( Integer id ) {
		this.id = id;
	}

	/**
	 * Setter for nombreMes
	 *
	 * @param nombreMes the nombreMes to set
	 */
	public void setNombreMes( String nombreMes ) {
		this.nombreMes = nombreMes;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[Mes==" + this.id + " Nombre= " + this.nombreMes + "]";
	}

}
