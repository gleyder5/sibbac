package sibbac.business.periodicidades.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.model.ServiciosContactos;
import sibbac.business.periodicidades.database.model.ServiciosContactos.PkServiciosContactos;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Servicios;


@Component
public interface ServiciosContactosDao extends JpaRepository< ServiciosContactos, PkServiciosContactos > {

	List< ServiciosContactos > findAllContactosByServicioAndContacto_aliasAndContactoActivo( Servicios servicio, Alias alias, Character activo );

	List< ServiciosContactos > findServiciosContactosByContactoIn( List< Contacto > contactos );

	List< ServiciosContactos > findServiciosContactosByContactoInAndServicio( List< Contacto > contactos, Servicios servicio );

	List< ServiciosContactos > findAllByContactoIn( List< Contacto > contactos );
}
