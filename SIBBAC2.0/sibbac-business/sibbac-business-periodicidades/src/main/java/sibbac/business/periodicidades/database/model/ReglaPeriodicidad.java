package sibbac.business.periodicidades.database.model;


import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.poi.ss.formula.functions.T;
import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Entity: "ReglaPeriodicidad".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = DBConstants.PERIODICIDADES.REGLAS_PERIODICIDAD )
@Entity
@XmlRootElement
@Audited
public class ReglaPeriodicidad extends ATableAudited< ReglaPeriodicidad > {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4836594588556945740L;

	@Column( name = "IDREGLAUSUARIO", nullable = false, unique = true, length = 16 )
	@XmlAttribute
	private String								idReglaUsuario;

	@Column( name = "DESCRIPCION", length = 255 )
	@XmlAttribute
	private String								descripcion;

	@Column( name = "FECHADESDE" )
	@XmlAttribute
	private Date								fechaDesde;

	@Column( name = "FECHAHASTA" )
	@XmlAttribute
	private Date								fechaHasta;

	@Column( name = "ACTIVO", nullable = false )
	@XmlAttribute
	private boolean								activo;

	@OneToMany( mappedBy = "reglaPeriodicidad", fetch = FetchType.EAGER )
	private List< ParametrizacionPeriodicidad >	parametrizaciones;

	// ----------------------------------------------- Bean Constructors

	public ReglaPeriodicidad() {
		super();
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for idReglaUsuario
	 *
	 * @return the idReglaUsuario
	 */
	public String getIdReglaUsuario() {
		return this.idReglaUsuario;
	}

	/**
	 * Getter for descripcion
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return this.descripcion;
	}

	/**
	 * Getter for fechaDesde
	 *
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return this.fechaDesde;
	}

	/**
	 * Getter for fechaHasta
	 *
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return this.fechaHasta;
	}

	/**
	 * Getter for activo
	 *
	 * @return the activo
	 */
	public boolean getActivo() {
		return this.activo;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for idReglaUsuario
	 *
	 * @param idReglaUsuario the idReglaUsuario to set
	 */
	public void setIdReglaUsuario( String idReglaUsuario ) {
		this.idReglaUsuario = idReglaUsuario;
	}

	/**
	 * Setter for descripcion
	 *
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion( String descripcion ) {
		this.descripcion = descripcion;
	}

	/**
	 * Setter for fechaDesde
	 *
	 * @param fechaDesde the fechaDesde to set
	 */
	public void setFechaDesde( Date fechaDesde ) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * Setter for fechaHasta
	 *
	 * @param fechaHasta the fechaHasta to set
	 */
	public void setFechaHasta( Date fechaHasta ) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * Setter for activo
	 *
	 * @param activo the activo to set
	 */
	public void setActivo( boolean activo ) {
		this.activo = activo;
	}

	public List< ParametrizacionPeriodicidad > getParametrizaciones() {
		return parametrizaciones;
	}

	public void setParametrizaciones( List< ParametrizacionPeriodicidad > parametrizaciones ) {
		this.parametrizaciones = parametrizaciones;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	@Override
	public int compareTo( final ReglaPeriodicidad other ) {
		// Si el "padre" ya es distinto...
		int father = super.compareTo( other );
		if ( father != 0 ) {
			return father;
		}

		// Entity concrete fields.
		int internal = 0;
		return ( internal == 0 ) ? 0 : -3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[REGLA PERIODICIDAD== " + this.id + " IdReglaUsuario= " + this.idReglaUsuario + " descripcion: " + this.descripcion
				+ " fechaDesde: " + this.fechaDesde.toString() + " fechaHasta: " + this.fechaHasta + " activo: " + this.activo + "]";
	}

}
