package sibbac.business.periodicidades.database.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import sibbac.database.DBConstants;


/**
 * Entity: "DiaSemana".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = DBConstants.PERIODICIDADES.DIA_SEMANA )
@Entity( name = "DiaSemana" )
// @Component
public class DiaSemana implements Serializable {

	// ------------------------------------------ Static Bean properties

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	// ------------------------------------------------- Bean properties
	@Id
	@Column( name = "ID", nullable = false, unique = true )
	@XmlAttribute
	private Integer				id;

	@Column( name = "NOMBREDIA", nullable = false, unique = true, length = 25 )
	@XmlAttribute
	private String				nombreDia;

	// ----------------------------------------------- Bean Constructors

	public DiaSemana() {

	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for id
	 *
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Getter for nombreDia
	 *
	 * @return the nombreDia
	 */
	public String getNombreDia() {
		return this.nombreDia;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for id
	 *
	 * @param id the id to set
	 */
	public void setId( Integer id ) {
		this.id = id;
	}

	/**
	 * Setter for nombreDia
	 *
	 * @param nombreDia the nombreDia to set
	 */
	public void setNombreDia( String nombreDia ) {
		this.nombreDia = nombreDia;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[DíaSemana==" + this.id + " Día= " + this.nombreDia + "]";
	}

}
