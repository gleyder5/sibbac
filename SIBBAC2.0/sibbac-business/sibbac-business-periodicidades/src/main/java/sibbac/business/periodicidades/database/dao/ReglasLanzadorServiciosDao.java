package sibbac.business.periodicidades.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.dto.ReglasLanzadorServiciosDTO;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios.Pk;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Servicios;


@Component
public interface ReglasLanzadorServiciosDao extends JpaRepository< ReglasLanzadorServicios, Pk > {

	ReglasLanzadorServicios findAllByReglaInAndServicioAndAlias( List< ReglaPeriodicidad > reglas, Servicios servicio, Alias alias );

	List< ReglasLanzadorServicios > findAllByReglaInAndServicio( List< ReglaPeriodicidad > reglas, Servicios servico );

	@Query( "select new sibbac.business.periodicidades.database.dto.ReglasLanzadorServiciosDTO( a.pk.servicioId, a.pk.aliasId, a.regla.id) from ReglasLanzadorServicios a " )
	public
			List< ReglasLanzadorServiciosDTO > findAllReglasLanzadorServicios();

	List< ReglasLanzadorServicios > findAllByAliasOrderByAlias( Alias alias );
}
