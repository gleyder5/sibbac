package sibbac.business.periodicidades;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.quartz.TimeOfDay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.bo.ParametrizacionPeriodicidadBo;
import sibbac.business.periodicidades.database.bo.ReglaPeriodicidadBo;
import sibbac.business.periodicidades.database.model.ParametrizacionPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.wrappers.database.bo.FestivoBo;

@Component
public class GestorPeriodicidades {

  protected static final Logger LOG = LoggerFactory.getLogger(GestorPeriodicidades.class);

  @Autowired
  protected FestivoBo festivoBo;
  @Autowired
  protected ReglaPeriodicidadBo reglaPeriodicidadBo;
  @Autowired
  protected ParametrizacionPeriodicidadBo parametrizacionPeriodicidadBo;

  public GestorPeriodicidades() {
  };
  
  public boolean isReglaActiva(String idRegla, java.util.Date dateTime) {
    final ReglaPeriodicidad regla;
    
    LOG.trace("Received rule [{}] and dateTime: [{}]", idRegla, dateTime);
    regla = reglaPeriodicidadBo.findByIdReglaUsuario(idRegla);
    LOG.trace("Got rule: {}", regla);
    return isReglaActiva(regla, dateTime);
  }

  public boolean isReglaActiva(ReglaPeriodicidad regla, java.util.Date dateTime) {
    Date date = null;
    TimeOfDay time = null;
    Calendar cal = new GregorianCalendar();

    if (dateTime == null) {
      // Si el parametro date es null se considera el momento actual
      java.util.Date now = new java.util.Date();
      cal.setTime(now);
    } else {
      date = new Date(dateTime.getTime());
      cal.setTime(date);
    }
    time = new TimeOfDay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
    LOG.trace("Stablished date/time: [{}/{}]", date, time);
    // Si la regla no existe o no está activa devuelvo falso
    if (regla == null || !isReglaValid(regla, date)) {
      return false;
    } else {
      return hasParametrizacionActiva(regla, date, time);
    }
  }

  private boolean isReglaValid(ReglaPeriodicidad regla, Date time) {
    if (regla == null || time == null) {
      return false;
    }
    if (regla.getActivo() && regla.getFechaDesde().before(time) && regla.getFechaHasta().after(time)) {
      return true;
    }
    return false;
  }

  public ArrayList<ReglaPeriodicidad> getReglasActivas(java.util.Date dateTime) {
    ArrayList<ReglaPeriodicidad> reglasActivas = new ArrayList<ReglaPeriodicidad>();
    Date date = null;
    TimeOfDay time = null;
    Calendar cal = new GregorianCalendar();
    LOG.trace("Received dateTime: [{}]", dateTime);

    if (dateTime == null) {
      // Si el parametro date es null se considera el momento actual
      java.util.Date now = new java.util.Date();
      cal.setTime(now);
    } else {
      cal.setTime(dateTime);
    }
    time = new TimeOfDay(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
    date = new Date(cal.getTimeInMillis());
    LOG.trace("Stablished date/time: [{}/{}]", date, time);

    // Obtener los identificadores de todas las reglas activas
    List<ReglaPeriodicidad> reglasActivasList = reglaPeriodicidadBo.findAllByActivo(true);
    // Recorrer los parametros de las reglas llamando a isReglaActiva
    // Guardar el idRegla de las que devuelvan true para isReglaActiva
    // Si la regla no está activa devuelvo falso

    for (ReglaPeriodicidad regla : reglasActivasList) {
      LOG.trace("Checking validity [{}] of rule: {}", date, regla);
      if (isReglaValid(regla, date)) {
        LOG.trace("+ Valid rule");
        if (hasParametrizacionActiva(regla, date, time)) {
          LOG.trace("+ Has active parameters");
          reglasActivas.add(regla);
        } else {
          LOG.trace("- Has NO active parameters");
        }
      } else {
        LOG.trace("- Invalid rule");
      }
    }
    return reglasActivas;
  }

  // Recorrer los parámetros de la regla y devolver true si alguno es
  // aplicable
  private boolean hasParametrizacionActiva(ReglaPeriodicidad regla, Date date, TimeOfDay time) {
    LOG.trace("Received date/time: [{}/{}] and rule: {}", date, time, regla);
    if (date == null) {
      java.util.Date now = new java.util.Date();
      date = new Date(now.getTime());
    }

    ArrayList<ParametrizacionPeriodicidad> listParams = parametrizacionPeriodicidadBo.findAllByReglaPeriodicidadAndActivo(regla, true);
    if (listParams == null || listParams.size() == 0) {
      return false;
    }

    for (ParametrizacionPeriodicidad param : listParams) {
      if (isParametrizacionValidNow(param, date, time)) {
        return true;
      }
    }

    return false;
  }

  // public boolean seleccionFechas( ParametrizacionPeriodicidad param ) {
  // java.util.Date now = new java.util.Date();
  // Date fechaAConsultar = new Date( now.getTime() );
  // Date laborableAnterior = getLaborableAnterior( fechaAConsultar );
  // Date laborablePosterior = getLaborablePosterior( fechaAConsultar );
  //
  // // Parametrizaciones que aplican a la fecha a consultar
  // if ( !isLaborable( fechaAConsultar ) ) {
  // // Buscar las que no tengan activado "Excepcionar festivos"
  // } else {
  // // Fechas que coincidan
  // }
  //
  // // Si el laborable posterior es distinto a fechaAconsultar +1
  // if ( laborablePosterior.compareTo( sumarDia( fechaAConsultar ) ) != 0 ) {
  // int diferenciaDias = getListDatesBetween( getLaborablePosterior( fechaAConsultar ), sumarDia( fechaAConsultar ) ).size() - 1;
  // // Por cada diasDiferencia buscar las ¿Reglas? a las que aplique
  // // la parametrización que tngan activado el check
  // // "laborable anterior"
  // }
  //
  // // Si el laborable anterior es distinto de fechaAConsultar -1
  // if ( laborableAnterior.compareTo( restarDia( fechaAConsultar ) ) != 0 ) {
  //
  // }
  // return false;
  //
  // }

  // Comprueba si una parametrizacion es valida en un momento
  public boolean isParametrizacionValidNow(ParametrizacionPeriodicidad param, Date date, TimeOfDay time) {
    LOG.debug("[isParametrizacionValidNow] param/date/time: [{}/{}/{}]", param, date, time);
    
    // Primero comprobamos si el momento esta entre fechaDesde y fechaHasta
    if (date.before(param.getFechaDesde()) || date.after(param.getFechaHasta())) {
      LOG.trace("Fecha {} FUERA del rango del parámetro [{}/{}]", date, param.getFechaDesde(), param.getFechaHasta());
      return false;
    }

    // Comprobamos si va por mes
    if (param.getMes() != null && param.getDiaMes() != null) {
      Calendar calToExec = new GregorianCalendar();
      calToExec.setTime(date);
      LOG.debug("[isParametrizacionValidNow] calToExec: [{}]", calToExec);

      Integer day = param.getDiaMes();
      LOG.debug("[isParametrizacionValidNow] day: [{}]", day);
      Integer month = param.getMes().getId();
      LOG.debug("[isParametrizacionValidNow] month: [{}]", month);

      if (day == 99) {
        day = calToExec.getActualMaximum(Calendar.DAY_OF_MONTH);
      }
      if (month == 99) {
        month = calToExec.get(Calendar.MONTH) + 1;
      }
      LOG.debug("[isParametrizacionValidNow] month: [{}]", month);

      Integer year = anyoMasCercano(calToExec, date, month, day);

      calToExec.set(year, month - 1, day);
      Date diaAEjecutar = new Date(calToExec.getTimeInMillis());

      if (!isLaborable(diaAEjecutar)) {
        LOG.debug("[isParametrizacionValidNow] El dia es no laborable");
        if (param.getExcepcionarFestivos()) {
          LOG.debug("[isParametrizacionValidNow] Excepcionar festivo: true");
          if (param.getLaborableAnterior()) {
            LOG.debug("[isParametrizacionValidNow] Coger laborable anterior");
            // Calcular el laborable anterior
            diaAEjecutar = getLaborableAnterior(diaAEjecutar);
            LOG.debug("[isParametrizacionValidNow] diaAEjecutar: [{}]", diaAEjecutar);
          } else {
            if (param.getLaborablePosterior()) {
              LOG.debug("[isParametrizacionValidNow] Coger laborable posterior");
              diaAEjecutar = getLaborablePosterior(diaAEjecutar);
              LOG.debug("[isParametrizacionValidNow] diaAEjecutar: [{}]", diaAEjecutar);
            }
          }
        } else {
          LOG.trace("La parametrización no se ejecuta en festivo y es no laborable");
          return false;
        }
      }

      calToExec.setTime(diaAEjecutar);
      LOG.debug("[isParametrizacionValidNow] calToExec: [{}]", calToExec);

      Calendar calendar = new GregorianCalendar();
      calendar.setTime(date);
      LOG.debug("[isParametrizacionValidNow] calendar: [{}]", calendar);
      
      LOG.debug("[isParametrizacionValidNow] calToExec.get(Calendar.DAY_OF_MONTH): [{}]", calToExec.get(Calendar.DAY_OF_MONTH));
      LOG.debug("[isParametrizacionValidNow] calendar.get(Calendar.DAY_OF_MONTH): [{}]", calendar.get(Calendar.DAY_OF_MONTH));
      LOG.debug("[isParametrizacionValidNow] calToExec.get(Calendar.MONTH): [{}]", calToExec.get(Calendar.MONTH));
      LOG.debug("[isParametrizacionValidNow] calendar.get(Calendar.MONTH): [{}]", calendar.get(Calendar.MONTH));
      
      if (calToExec.get(Calendar.DAY_OF_MONTH) != calendar.get(Calendar.DAY_OF_MONTH)
          || calToExec.get(Calendar.MONTH) != calendar.get(Calendar.MONTH)) {
        LOG.debug("[isParametrizacionValidNow] mes o dia del mes distinto, se devuelve false");
        return false;
      }
    } else { // Si no va por mes, va por semana
      Calendar calToday = new GregorianCalendar();
      calToday.setTime(date);
      Calendar calToExec = new GregorianCalendar();

      int dayOfWeek = calToday.get(Calendar.DAY_OF_WEEK);
      int dayOfWeekToExec = param.getDiaSemana().getId();
      Date diaToExec = null;
      Date diaHoy = new Date(calToday.getTimeInMillis());

      if (dayOfWeekToExec == 99) {
        dayOfWeekToExec = dayOfWeek;
      } else if (dayOfWeekToExec == 98) {
        if (dayOfWeek != 1 && dayOfWeek != 7) {
          dayOfWeekToExec = dayOfWeek;
        } else {
          return false;
        }
      }

      if (dayOfWeek == dayOfWeekToExec) {
        // Si estamos en el día de la semana que toca ejecutar
        if (!isLaborable(diaHoy)) {
          // Excepeciona festivos?
          if (param.getExcepcionarFestivos()) {
            return false;
          }
        }
      } else {
        // Si estamos en otro dayOfWeek
        if (param.getExcepcionarFestivos()) {
          if (param.getLaborableAnterior()) {
            // Calcular el laborable anterior
            // Poner en diaToExec el siguiente dayOfWeekToExec
            // a la fecha que comprobamos
            diaToExec = getNextWeekday(date, dayOfWeekToExec);
            if (!isLaborable(diaToExec)) {
              diaToExec = getLaborableAnterior(diaToExec);
            } else {
              // Significa que la fecha no se ha movido y seguimos
              // sin estar en el dia que toca ejecutar
              return false;
            }
          } else {
            if (param.getLaborablePosterior()) {
              // Poner diaToExec el anterior
              // dayOfWeekToExec a la fecha que comprobamos
              diaToExec = getLastWeekday(date, dayOfWeekToExec);
              if (!isLaborable(diaToExec)) {
                diaToExec = getLaborablePosterior(diaToExec);
              } else {
                // Significa que la fecha no se ha movido y
                // seguimos
                // sin estar en el dia que toca ejecutar
                return false;
              }
            } else {
              // Esto significaria que tiene excepcionar festivos
              // pero no lo manda ni al laborable anterior
              // ni al posterior
              return false;
            }
          }
        } else {
          // Esto significaria que NO tiene excepcionar festivos
          // por lo que no se "mueve" el dia de ejecucion
          return false;
        }
        calToExec.setTime(diaToExec);

        if (calToExec.get(Calendar.DAY_OF_YEAR) != calToday.get(Calendar.DAY_OF_YEAR)) {
          return false;
        }
      }
    }

    // Finalmente, si se comprueba que el dia es valido, comprobamos si la
    // hora también lo es
    if (!isMomentValid(param, time)) {
      LOG.debug("[isParametrizacionValidNow] Hora no valida");
      return false;
    }
    LOG.debug("[isParametrizacionValidNow] Hora valida");
    return true;
  }

  /**
   * 
   * @param date (dia del mes al que se le quiere encontrar el siguiente dia de la semana)
   * @param dayOfWeekToFind (Dia de la semana a buscar)
   * @return
   */
  public Date getNextWeekday(Date date, int dayOfWeekToFind) {
    Calendar calToday = new GregorianCalendar();
    calToday.setTime(date);

    do {
      date = sumarDia(date);
      calToday.setTime(date);
    } while (calToday.get(Calendar.DAY_OF_WEEK) != dayOfWeekToFind);

    return date;
  }

  /**
   * 
   * @param date (dia del mes al que se le quiere encontrar el anterior dia de la semana)
   * @param dayOfWeekToFind (Dia de la semana a buscar)
   * @return
   */
  public Date getLastWeekday(Date date, int dayOfWeekToFind) {
    Calendar calToday = new GregorianCalendar();
    calToday.setTime(date);

    do {
      date = restarDia(date);
      calToday.setTime(date);
    } while (calToday.get(Calendar.DAY_OF_WEEK) != dayOfWeekToFind);

    return date;
  }

  public boolean isLaborable(Date day) {
    LOG.debug("[isLaborable] day: [{}]", day);
    Calendar c = new GregorianCalendar();
    c.setTime(day);
    int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
    LOG.debug("[isLaborable] dayOfWeek: [{}]", dayOfWeek);

    if (dayOfWeek == 7 || dayOfWeek == 1) {
      // Si es sábado o domingo no es laborable
      LOG.debug("[isLaborable] El dia es sabado o domingo");
      return false;
    }
    
    LOG.debug("[isLaborable] day: [{}]", day);
    if (festivoBo.countByFecha(day) > 0) {
      // Si se encuentra en la lista de festivos
      LOG.debug("[isLaborable] El dia es festivo");
      return false;
    }
    
    LOG.debug("[isLaborable] El dia es laborable");
    return true;
  }

  // Funcion que devuelve una lista de Fechas que va desde fechaInicio a
  // fechaFin
  public ArrayList<Date> getListDatesBetween(Date fechaInicio, Date fechaFin) {

    ArrayList<Date> lista = new ArrayList<Date>();

    if (fechaInicio.compareTo(fechaFin) != 0) {

      if (fechaInicio.before(fechaFin)) {

        lista.add(fechaInicio);

        while (fechaInicio.before(fechaFin)) {
          Date sqlDate = sumarDia(fechaInicio);
          lista.add(sqlDate);
          fechaInicio = sumarDia(fechaInicio);
        }
      } else {
        lista = null;
      }
    } else {

      lista = null;
    }

    return lista;
  }

  // Suma un dia la fecha que se le pasa y lo devuelve java.sql.date
  public Date sumarDia(Date date) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, +1);
    Date sqlDate = new Date(calendar.getTime().getTime());
    return sqlDate;
  }

  // Resta un dia la fecha que se le pasa y lo devuelve java.sql.date
  public Date restarDia(Date date) {

    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DATE, -1);
    Date sqlDate = new Date(calendar.getTime().getTime());
    return sqlDate;
  }

  // Calcula el dia laborable Anterior y devuelve java.sql.date
  public Date getLaborableAnterior(Date now) {

    boolean laborable = false;

    do {
      now = restarDia(now);
      laborable = isLaborable(now);

    } while (laborable == false);

    return now;
  }

  // Calcula el dia laborable Posterior y devuelve java.sql.date
  public Date getLaborablePosterior(Date now) {

    boolean laborable = false;

    do {
      now = sumarDia(now);
      laborable = isLaborable(now);

    } while (laborable == false);

    return now;
  }

  // Calcula si el "momento" es valido
  public boolean isMomentValid(ParametrizacionPeriodicidad parametrizacion, TimeOfDay moment) {
    LOG.debug("[isMomentValid] parametrizacion/moment: [{}/{}]", parametrizacion, moment);

    // Se pasa todo a minutos
    Integer minutesParam = new Integer(parametrizacion.getHora() * 60 + parametrizacion.getMinuto());
    LOG.debug("[isMomentValid] minutesParam: [{}]", minutesParam);
    

    Integer margenAnterior = new Integer(parametrizacion.getMargenAnterior());
    LOG.debug("[isMomentValid] margenAnterior: [{}]", margenAnterior);
    Integer margenPosterior = new Integer(parametrizacion.getMargenPosterior());
    LOG.debug("[isMomentValid] margenPosterior: [{}]", margenPosterior);

    int minutesMoment = moment.getHour() * 60 + moment.getMinute();
    LOG.debug("[isMomentValid] minutesMoment: [{}]", minutesMoment);

    LOG.trace("isMomentValid - MinutosMomento: {} MinutosParam: {} Margen ant/pos: [{}/{}]", minutesMoment, minutesParam, margenAnterior,
              margenPosterior);

    // Comrobamos que se encuentra dentro de los margenes
    if (minutesMoment >= minutesParam - margenAnterior && minutesMoment <= minutesParam + margenPosterior) {
      LOG.trace("Momento en rango");
      return true;
    }
    LOG.trace("Momento FUERA de rango");
    return false;
  }

  public Integer anyoMasCercano(Calendar calToExec, Date date, Integer month, Integer day) {
    LOG.debug("[anyoMasCercano] calToExec/date/month/day: [{}/{}/{}/{}]", calToExec, date, month, day);

    Integer year = calToExec.get(Calendar.YEAR);
    LOG.debug("[anyoMasCercano] year: [{}]", year);

    calToExec.set(year - 1, month - 1, day);
    Date ant = new Date(calToExec.getTimeInMillis());
    LOG.debug("[anyoMasCercano] ant: [{}]", ant);

    calToExec.set(year, month - 1, day);
    Date act = new Date(calToExec.getTimeInMillis());
    LOG.debug("[anyoMasCercano] act: [{}]", act);

    calToExec.set(year + 1, month - 1, day);
    Date post = new Date(calToExec.getTimeInMillis());
    LOG.debug("[anyoMasCercano] post: [{}]", post);

    // Vemos la de que año esta mas cerca
    long distanceAnt = Math.abs(date.getTime() - ant.getTime());
    LOG.debug("[anyoMasCercano] distanceAnt: [{}]", distanceAnt);

    long distanceAct = Math.abs(date.getTime() - act.getTime());
    LOG.debug("[anyoMasCercano] distanceAct: [{}]", distanceAct);

    long distancePost = Math.abs(date.getTime() - post.getTime());
    LOG.debug("[anyoMasCercano] distancePost: [{}]", distancePost);

    if (distanceAnt < distanceAct && distanceAnt < distancePost) {
      year = year - 1;
    } else if (distanceAct < distancePost && distanceAct < distanceAnt) {
      // Do nothing
    } else {
      year = year + 1;
    }

    LOG.debug("[anyoMasCercano] year: [{}]", year);
    return year;
  }

}
