package sibbac.business.periodicidades.tasks;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.tasks.SIBBACTask;


public class Delegated extends SIBBACTask {

	protected static final Logger	LOG	= LoggerFactory.getLogger( Delegated.class );

	public Delegated() {
	}

	@Override
	public void execute() {
		LOG.debug( "[Delegated::execute] [{}] time: [{}]", this.jobDataMap.get( "name" ), new Date() );
	}

}
