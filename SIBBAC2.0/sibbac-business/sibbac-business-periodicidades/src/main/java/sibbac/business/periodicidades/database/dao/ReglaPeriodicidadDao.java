package sibbac.business.periodicidades.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;


@Component
public interface ReglaPeriodicidadDao extends JpaRepository< ReglaPeriodicidad, Long > {

	ReglaPeriodicidad findById( final Long id );

	ReglaPeriodicidad findByIdReglaUsuario( final String id );

	List< ReglaPeriodicidad > findAllByActivo( boolean activo );
}
