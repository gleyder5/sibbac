package sibbac.business.periodicidades.database.dao;


import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.model.ParametrizacionPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;


@Component
public interface ParametrizacionPeriodicidadDao extends JpaRepository< ParametrizacionPeriodicidad, Long > {

	ParametrizacionPeriodicidad findById( final Long id );

	ArrayList< ParametrizacionPeriodicidad > findAllByReglaPeriodicidad( ReglaPeriodicidad reglaPeriodicidad );

	ArrayList< ParametrizacionPeriodicidad > findAllByReglaPeriodicidadAndActivo( ReglaPeriodicidad reglaPeriodicidad, boolean activo );

}
