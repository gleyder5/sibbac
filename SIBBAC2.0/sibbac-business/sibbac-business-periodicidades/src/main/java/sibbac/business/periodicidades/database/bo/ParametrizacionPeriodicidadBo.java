package sibbac.business.periodicidades.database.bo;


import java.sql.Date;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.periodicidades.database.dao.ParametrizacionPeriodicidadDao;
import sibbac.business.periodicidades.database.model.DiaSemana;
import sibbac.business.periodicidades.database.model.Mes;
import sibbac.business.periodicidades.database.model.ParametrizacionPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.database.bo.AbstractBo;


@Service
public class ParametrizacionPeriodicidadBo extends AbstractBo< ParametrizacionPeriodicidad, Long, ParametrizacionPeriodicidadDao > {

	@Autowired
	protected ReglaPeriodicidadBo	boRegla;

	@Autowired
	protected MesBo					boMes;

	@Autowired
	protected DiaSemanaBo			boDiaSemana;

	public ArrayList< ParametrizacionPeriodicidad > findAllByReglaPeriodicidad( ReglaPeriodicidad reglaPeriodicidad ) {
		return this.dao.findAllByReglaPeriodicidad( reglaPeriodicidad );
	}

	public ArrayList< ParametrizacionPeriodicidad >
			findAllByReglaPeriodicidadAndActivo( ReglaPeriodicidad reglaPeriodicidad, boolean activo ) {
		return this.dao.findAllByReglaPeriodicidadAndActivo( reglaPeriodicidad, activo );
	}

	public ParametrizacionPeriodicidad
			createParametrizacionPeriodicidad( final Integer diaMes, boolean excepcionarFestivos, boolean laborableAnterior,
					boolean laborablePosterior, Integer hora, Integer minuto, Integer margenAnterior, Integer margenPosterior,
					Date fechaDesde, Date fechaHasta, boolean activo, Mes mes, DiaSemana diaSemana, ReglaPeriodicidad regla ) {

		ParametrizacionPeriodicidad dto = new ParametrizacionPeriodicidad();
		dto.setReglaPeriodicidad( regla );
		if ( mes != null ) {
			dto.setMes( mes );
			dto.setDiaMes( diaMes );
		}
		dto.setExcepcionarFestivos( excepcionarFestivos );
		dto.setLaborableAnterior( laborableAnterior );
		dto.setLaborablePosterior( laborablePosterior );
		dto.setHora( hora );
		dto.setMinuto( minuto );
		dto.setMargenAnterior( margenAnterior );
		dto.setMargenPosterior( margenPosterior );
		dto.setFechaDesde( fechaDesde );
		dto.setFechaHasta( fechaHasta );
		dto.setActivo( activo );

		if ( diaSemana != null ) {
			dto.setDiaSemana( diaSemana );
		}
		return this.dao.save( dto );

	}

	public boolean deleteParametrizacion( Long id ) {
		ParametrizacionPeriodicidad toDelete = this.findById( id );
		if ( toDelete == null ) {
			return false;
		}
		try {
			this.dao.delete( toDelete );
		} catch ( Exception e ) {
			LOG.debug( e.getStackTrace().toString() );
		}
		return true;
	}

	public boolean updateParametrizacion( Long id, Integer diaMes, boolean excepcionarFestivos, boolean laborableAnterior,
			boolean laborablePosterior, Integer hora, Integer minuto, Integer margenAnterior, Integer margenPosterior, Date fechaDesde,
			Date fechaHasta, boolean activo, String idReglaUsuario, Integer idMes, Integer idDiaSemana ) {

		ParametrizacionPeriodicidad toUpdate = this.findById( id );

		if ( toUpdate == null ) {
			return false;
		}

		toUpdate.setDiaMes( diaMes );
		toUpdate.setExcepcionarFestivos( excepcionarFestivos );
		toUpdate.setLaborableAnterior( laborableAnterior );
		toUpdate.setLaborablePosterior( laborablePosterior );
		toUpdate.setHora( hora );
		toUpdate.setMinuto( minuto );
		toUpdate.setMargenAnterior( margenAnterior );
		toUpdate.setMargenPosterior( margenPosterior );
		toUpdate.setFechaDesde( fechaDesde );
		toUpdate.setFechaHasta( fechaHasta );
		toUpdate.setActivo( activo );
		toUpdate.setReglaPeriodicidad( boRegla.findByIdReglaUsuario( idReglaUsuario ) );
		toUpdate.setMes( boMes.findById( idMes ) );
		toUpdate.setDiaSemana( boDiaSemana.findById( idDiaSemana ) );

		this.dao.save( toUpdate );
		return true;
	}

	public boolean updateActivoParametrizacion( Long id, Boolean activo ) {
		ParametrizacionPeriodicidad toUpdate = this.findById( id );

		if ( toUpdate == null ) {
			return false;
		}

		toUpdate.setActivo( activo );
		this.dao.save( toUpdate );
		return true;
	}
}
