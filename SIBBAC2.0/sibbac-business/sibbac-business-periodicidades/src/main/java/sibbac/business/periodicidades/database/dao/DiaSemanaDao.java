package sibbac.business.periodicidades.database.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import sibbac.business.periodicidades.database.model.DiaSemana;


@Component
public interface DiaSemanaDao extends JpaRepository< DiaSemana, Integer > {

	DiaSemana findById( final Integer id );

	DiaSemana findByNombreDia( final String nombreDia );

}
