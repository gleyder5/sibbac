package sibbac.business.periodicidades.rest;


import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import sibbac.business.periodicidades.Constantes;
import sibbac.business.periodicidades.GestorPeriodicidades;
import sibbac.business.periodicidades.GestorServiciosImpl;
import sibbac.business.periodicidades.database.bo.ReglaPeriodicidadBo;
import sibbac.business.periodicidades.database.bo.ReglasLanzadorServiciosBo;
import sibbac.business.periodicidades.database.bo.ServiciosContactosBo;
import sibbac.business.periodicidades.database.model.ReglaPeriodicidad;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios;
import sibbac.business.periodicidades.database.model.ReglasLanzadorServicios.Pk;
import sibbac.business.periodicidades.database.model.ServiciosContactos;
import sibbac.business.periodicidades.database.model.ServiciosContactos.PkServiciosContactos;
import sibbac.business.wrappers.database.bo.AliasBo;
import sibbac.business.wrappers.database.bo.ContactoBo;
import sibbac.business.wrappers.database.bo.ServiciosBo;
import sibbac.business.wrappers.database.data.MatrizServiciosContactosData;
import sibbac.business.wrappers.database.dto.AliasDTO;
import sibbac.business.wrappers.database.model.Alias;
import sibbac.business.wrappers.database.model.Contacto;
import sibbac.business.wrappers.database.model.Servicios;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


/**
 * Perform business operations related with: "SIBBACServiceServicios".<br/>
 * This class, implementing {@link SIBBACServiceBean}, is responsible of parsing
 * incoming {@link WebRequest}'s and process them returning a {@link WebResponse} to be sent back to the client.
 *
 * @author Vector SF 2014.
 * @version 4.0.0
 * @since 4.0.0
 */
@SIBBACService
public class SIBBACServiceServicios implements SIBBACServiceBean {

	/** The bo servicios. */
	@Autowired
	private ServiciosBo					boServicios;

	/** The bo reglas lanzador servicios. */
	@Autowired
	private ReglasLanzadorServiciosBo	boReglasLanzadorServicios;

	/** The bo regla. */
	@Autowired
	private ReglaPeriodicidadBo			boRegla;

	/** The bo alias. */
	@Autowired
	private AliasBo						boAlias;

	/** The gestor servicios impl. */
	@Autowired
	private GestorServiciosImpl			gestorServiciosImpl;

	/** The gestor periodicidades. */
	@Autowired
	private GestorPeriodicidades		gestorPeriodicidades;

	/** The bo contacto. */
	@Autowired
	private ContactoBo					boContacto;

	/** The bo servicios contactos. */
	@Autowired
	private ServiciosContactosBo		boServiciosContactos;

	/** The simple name. */
	private static String				simpleName		= "SIBBACServiceServicios";

	/** The Constant FIELD_JOB_NAME. */
	private static final String			FIELD_JOB_NAME	= "name";

	/** The Constant FIELD_STATUS. */
	private static final String			FIELD_STATUS	= "status";

	/** The Constant FIELD_ALIAS. */
	private static final String			FIELD_ALIAS		= "alias";

	// ------------------------------------------ Bean static properties

	/** The Constant LOG. */
	private static final Logger			LOG				= LoggerFactory.getLogger( SIBBACServiceServicios.class );

	// ------------------------------------------------- Bean properties

	/** The quartz scheduler factory bean. */
	@Autowired
	private SchedulerFactoryBean		quartzSchedulerFactoryBean;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public SIBBACServiceServicios() {
	}

	// ------------------------------------------- Bean methods: Getters

	// ------------------------------------------- Bean methods: Setters

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getAvailableCommands()
	 */
	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( Constantes.CMD_GET_COMBINACION );
		commands.add( Constantes.CMD_GET_SERVICIOS );
		commands.add( Constantes.CMD_GET_REGLAS );
		commands.add( Constantes.CMD_ALTA_SERVICIO );
		commands.add( Constantes.CMD_BAJA_SERVICIO );
		commands.add( Constantes.CMD_UPDATE_ACTIVO_SERVICIO );
		commands.add( Constantes.CMD_UPDATE_SERVICIO_REGLA );
		commands.add( Constantes.CMD_GET_CONTACTOS_BY_SERVICE );
		commands.add( Constantes.CMD_ALTA_SERVICIOCONTACTO );
		commands.add( Constantes.CMD_BAJA_SERVICIOCONTACTO );
		return commands;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#getFields()
	 */
	@Override
	public List< String > getFields() {
		List< String > fields = new ArrayList< String >();
		fields.add( FIELD_JOB_NAME );
		fields.add( FIELD_STATUS );
		return fields;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see SIBBACServiceBean#process(WebRequest)
	 */
	@Override
	public WebResponse process( final WebRequest webRequest ) {
		String prefix = "[SIBBACServiceServicios.java::process] ";
		LOG.debug( prefix + "Processing a web request..." );
		WebResponse webResponse = new WebResponse();
		String command = webRequest.getAction();
		LOG.debug( prefix + "+ Command.: [{}]", command );
		Map< String, String > filtros = webRequest.getFilters();
		String jobName = null;
		webResponse.setRequest( webRequest );

		if ( filtros != null ) {
			jobName = filtros.get( FIELD_JOB_NAME );
		}
		LOG.debug( prefix + "+ Job Name: [{}]", jobName );

		LOG.trace( prefix + "Command: " + command );
		if ( command == null ) {
			webResponse.setError( "An action must be set. Valid actions are: " + this.getAvailableCommands() );
		} else {
			boolean entered = false;

			if ( command.equalsIgnoreCase( Constantes.CMD_GET_COMBINACION ) ) {
				entered = true;
				if ( this.getCombinacionCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.getCombinacion( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_SERVICIOS ) ) {
				entered = true;
				webResponse.setResultados( this.getServicios( filtros ) );

			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_REGLAS ) ) {
				entered = true;
				webResponse.setResultados( this.getReglas( filtros ) );

			} else if ( command.equalsIgnoreCase( Constantes.CMD_ALTA_SERVICIO ) ) {
				entered = true;
				if ( this.altaServicioCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.altaServicio( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_ALTA_SERVICIOCONTACTO ) ) {
				entered = true;
				if ( this.altaServicioContactoCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.altaServicioContacto( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_BAJA_SERVICIO ) ) {
				entered = true;
				if ( this.bajaServicioCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.bajaServicio( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_BAJA_SERVICIOCONTACTO ) ) {
				entered = true;
				if ( this.bajaServicioContactoCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.bajaServicioContacto( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_ACTIVO_SERVICIO ) ) {
				entered = true;
				if ( this.updateActivoServicioCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateActivoServicio( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_UPDATE_SERVICIO_REGLA ) ) {
				entered = true;
				if ( this.updateServicioReglaCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.updateServicioRegla( filtros ) );
				}
			} else if ( command.equalsIgnoreCase( Constantes.CMD_GET_CONTACTOS_BY_SERVICE ) ) {
				entered = true;
				if ( this.getContactosByServiceCheckFilters( webRequest, webResponse ) ) {
					webResponse.setResultados( this.getContactosByService( filtros ) );
				}
			} else if ( !entered ) {
				webResponse.setError( webRequest.getAction() + " is not a valid action. Valid actions are: " + this.getAvailableCommands() );
			}
		}

		return webResponse;
	}

	/**
	 * Gets the contactos by service check filters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return the contactos by service check filters
	 */
	private Boolean getContactosByServiceCheckFilters( WebRequest webRequest, WebResponse webResponse ) {
		Boolean flag = Boolean.FALSE;
		Map< String, String > filtros = webRequest.getFilters();

		if ( filtros.containsKey( FIELD_ALIAS ) ) {
			String number = filtros.get( FIELD_ALIAS );
			if ( number.matches( "[0-9]+" ) ) {
				flag = Boolean.TRUE;
			}
		}

		return flag;
	}

	/**
	 * Gets the contactos by service.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the contactos by service
	 */
	private Map< String, Object > getContactosByService( Map< String, String > filtros ) {
		Map< String, Object > resultados = new Hashtable< String, Object >();
		Long idAlias = Long.parseLong( filtros.get( FIELD_ALIAS ) );

		MatrizServiciosContactosData matrizServiciosContactosData = boContacto.getContactoyServiciosPorAlias( idAlias, Boolean.TRUE );

		resultados.put( "combinacion", matrizServiciosContactosData.getCombinacionDatas() );
		resultados.put( "servicios", matrizServiciosContactosData.getServicios() );
		resultados.put( "contactos", matrizServiciosContactosData.getContactos() );

		return resultados;
	}

	/**
	 * bajaServicio.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the map
	 */
	private Map< String, String > bajaServicio( Map< String, String > filtros ) {
		String prefix = "[SIBBACServiceServicios.java::bajaServicio] ";
		Map< String, String > resultado = new Hashtable< String, String >();

		Long idAlias = Long.parseLong( filtros.get( Constantes.ID_ALIAS_SERVICIO ) );
		Long idServicio = Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Alias alias = boAlias.findById( idAlias );
		Servicios servicio = boServicios.findById( idServicio );

		Pk pk = new Pk( alias.getId(), servicio.getId() );
		LOG.trace( prefix + "idAlias: [{}]", idAlias );
		LOG.trace( prefix + "idServicio: [{}]", idServicio );
		LOG.trace( prefix + "pk: [{}]", pk );

		ReglasLanzadorServicios lanzadorServicio = boReglasLanzadorServicios.findById( pk );
		Boolean result = false;
		LOG.trace( prefix + "Dando de baja..." );
		boReglasLanzadorServicios.bajaServicio( lanzadorServicio );
		result = true;
		resultado.put( "result", result.toString() );

		return resultado;
	}

	/**
	 * bajaServicioCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return true, if successful
	 */
	private boolean bajaServicioCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filters = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filters.get( Constantes.ID_ALIAS_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_ALIAS_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_SERVICIO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_SERVICIO_SERVICIO + " must be set. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
			return filtersRight;
		}

		Long idAlias = Long.parseLong( filters.get( Constantes.ID_ALIAS_SERVICIO ) );
		Long idServicio = Long.parseLong( filters.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Alias alias = null;
		Servicios servicio = null;
		// Comprobamos si el alias que se nos pasa existe en la tabla
		// tmct0_alais
		try {
			alias = boAlias.findById( idAlias );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Alias " + idAlias + " does not exist";
		}

		// Comprobamos si el serivicio que se nos pasa existe en la tabla
		// tmct0_servicio
		try {
			servicio = boServicios.findById( idServicio );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Service " + idServicio + " does not exist";
		}

		Pk pk = null;

		if ( alias != null && servicio != null ) {
			pk = new Pk( alias.getId(), servicio.getId() );
		} else {
			filtersRight = false;
		}

		if ( filtersRight == true ) {
			if ( boReglasLanzadorServicios.findById( pk ) == null ) {
				errorMsg = errorMsg + "This association does not exist";
				webResponse.setError( errorMsg );
				filtersRight = false;
			}
		} else {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	/**
	 * bajaServicioContacto.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the map
	 */
	private Map< String, String > bajaServicioContacto( Map< String, String > filtros ) {
		Map< String, String > resultado = new Hashtable< String, String >();

		Long idContacto = Long.parseLong( filtros.get( Constantes.ID_CONTACTO_SERVICIO ) );
		Long idServicio = Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Contacto contacto = boContacto.findById( idContacto );
		Servicios servicio = boServicios.findById( idServicio );

		PkServiciosContactos pk = new PkServiciosContactos( contacto.getId(), servicio.getId() );

		ServiciosContactos servicioContacto = boServiciosContactos.findById( pk );
		Boolean result = false;
		boServiciosContactos.bajaServicioContacto( servicioContacto );

		result = true;

		resultado.put( "result", result.toString() );

		return resultado;
	}

	/**
	 * bajaServicioContactoCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return true, if successful
	 */
	private boolean bajaServicioContactoCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filters = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filters.get( Constantes.ID_CONTACTO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_CONTACTO_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_SERVICIO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_SERVICIO_SERVICIO + " must be set. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
			return filtersRight;
		}

		Long idContacto = Long.parseLong( filters.get( Constantes.ID_CONTACTO_SERVICIO ) );
		Long idServicio = Long.parseLong( filters.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Contacto contacto = null;
		Servicios servicio = null;
		// Comprobamos si el contacto que se nos pasa existe en la tabla
		// tmct0_alais
		try {
			contacto = boContacto.findById( idContacto );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Contacto " + idContacto + " does not exist";
		}

		// Comprobamos si el serivicio que se nos pasa existe en la tabla
		// tmct0_servicio
		try {
			servicio = boServicios.findById( idServicio );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Service " + idServicio + " does not exist";
		}

		PkServiciosContactos pk = null;

		if ( contacto != null && servicio != null ) {
			pk = new PkServiciosContactos( contacto.getId(), servicio.getId() );
		} else {
			filtersRight = false;
		}

		if ( filtersRight == true ) {
			if ( boServiciosContactos.findById( pk ) == null ) {
				errorMsg = errorMsg + "This association does not exist";
				webResponse.setError( errorMsg );
				filtersRight = false;
			}
		} else {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	/**
	 * altaServicio.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the map
	 */
	private Map< String, String > altaServicio( Map< String, String > filtros ) {
		Map< String, String > resultado = new Hashtable< String, String >();

		Long idAlias = Long.parseLong( filtros.get( Constantes.ID_ALIAS_SERVICIO ) );
		Long idRegla = Long.parseLong( filtros.get( Constantes.ID_REGLA_SERVICIO ) );
		Long idServicio = Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Alias alias = boAlias.findById( idAlias );
		ReglaPeriodicidad regla = boRegla.findById( idRegla );
		Servicios servicio = boServicios.findById( idServicio );
		Boolean result = false;
		if ( boReglasLanzadorServicios.altaServicio( alias, servicio, regla ) != null ) {
			result = true;
		}
		resultado.put( "result", result.toString() );

		return resultado;
	}

	/**
	 * altaServicioCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return true, if successful
	 */
	private boolean altaServicioCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filters = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filters.get( Constantes.ID_ALIAS_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_ALIAS_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_REGLA_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_REGLA_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_SERVICIO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_SERVICIO_SERVICIO + " must be set. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
			return filtersRight;
		}
		Long idAlias = Long.parseLong( filters.get( Constantes.ID_ALIAS_SERVICIO ) );
		Long idRegla = Long.parseLong( filters.get( Constantes.ID_REGLA_SERVICIO ) );
		Long idServicio = Long.parseLong( filters.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Alias alias = null;
		ReglaPeriodicidad regla = null;
		Servicios servicio = null;
		// Comprobamos si el alais que se nos pasa existe en la tabla
		// tmct0_alias
		try {
			alias = boAlias.findById( idAlias );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Alias " + idAlias + " does not exist";
		}

		// Comprobamos si la regla que se nos pasa existe en la tabla
		// tmct0_reglasperiodicidad
		try {
			regla = boRegla.findById( idRegla );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Rule " + idRegla + " does not exist";
		}

		// Comprobamos si el servicio que se nos pasa existe en la tabla
		// tmct0_servicio
		try {
			servicio = boServicios.findById( idServicio );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Service " + idServicio + " does not exist";
		}

		Pk pk = null;

		if ( alias != null && regla != null && servicio != null ) {
			pk = new Pk( alias.getId(), servicio.getId() );
		} else {
			filtersRight = false;
		}

		if ( filtersRight == true ) {
			if ( boReglasLanzadorServicios.findById( pk ) != null ) {
				errorMsg = errorMsg + "This association already exist , you can not create another equally";
				webResponse.setError( errorMsg );
				filtersRight = false;
			}
		} else {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	/**
	 * altaServicioContacto.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the map
	 */
	private Map< String, String > altaServicioContacto( Map< String, String > filtros ) {
		Map< String, String > resultado = new Hashtable< String, String >();

		Long idContacto = Long.parseLong( filtros.get( Constantes.ID_CONTACTO_SERVICIO ) );
		Long idServicio = Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Contacto contacto = boContacto.findById( idContacto );
		Servicios servicio = boServicios.findById( idServicio );
		contacto.getEmail();
		Boolean result = false;
		if ( boServiciosContactos.altaServicioContacto( contacto, servicio ) != null ) {
			result = true;
		}
		resultado.put( "result", result.toString() );

		return resultado;
	}

	/**
	 * altaServicioContactoCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return true, if successful
	 */
	private boolean altaServicioContactoCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filters = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filters.get( Constantes.ID_CONTACTO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_CONTACTO_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_SERVICIO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_SERVICIO_SERVICIO + " must be set. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
			return filtersRight;
		}
		Long idContacto = Long.parseLong( filters.get( Constantes.ID_CONTACTO_SERVICIO ) );
		Long idServicio = Long.parseLong( filters.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Contacto contacto = null;
		Servicios servicio = null;
		// Comprobamos si el contacto que se nos pasa existe en la tabla
		// tmct0_alias
		try {
			contacto = boContacto.findById( idContacto );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Contact " + idContacto + " does not exist";
		}

		// Comprobamos si el servicio que se nos pasa existe en la tabla
		// tmct0_servicio
		try {
			servicio = boServicios.findById( idServicio );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Service " + idServicio + " does not exist";
		}

		PkServiciosContactos pk = null;

		if ( contacto != null && servicio != null ) {
			pk = new PkServiciosContactos( contacto.getId(), servicio.getId() );
		} else {
			filtersRight = false;
		}

		if ( filtersRight == true ) {
			if ( boServiciosContactos.findById( pk ) != null ) {
				errorMsg = errorMsg + "This association already exist , you can not create another equally";
				webResponse.setError( errorMsg );
				filtersRight = false;
			}
		} else {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	/**
	 * updateActivoServicio.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the map
	 */
	private Map< String, String > updateActivoServicio( Map< String, String > filtros ) {
		Map< String, String > resultado = new Hashtable< String, String >();

		Long idServicio = Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) );
		Boolean activo = Boolean.parseBoolean( filtros.get( Constantes.ACTIVO_SERVICIO ) );

		Boolean result = false;
		if ( boServicios.updateActivoServicio( idServicio, activo ) == true ) {
			result = true;
		}
		resultado.put( "result", result.toString() );

		return resultado;
	}

	/**
	 * updateActivoServicioCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return true, if successful
	 */
	private boolean updateActivoServicioCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_SERVICIO_SERVICIO + " must be selected. ";
			filtersRight = false;
		} else {
			if ( boServicios.findById( Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such " + Constantes.ID_SERVICIO_SERVICIO + " in the database. ";
				filtersRight = false;
			}
		}

		if ( filtros.get( Constantes.ACTIVO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ACTIVO_SERVICIO + " must be selected. ";
			filtersRight = false;
		}
		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}
		return filtersRight;
	}

	// ------------------GETLISTSBYALIAS----------------------//
	/**
	 * getListByAlias.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the list by alias
	 */
	// private Map< String, Object > getListByAlias( Map< String, String >
	// filtros ) {
	// Map< String, Object > resultadosFinal = new Hashtable< String, Object
	// >();
	// List< Map< String, Object >> resultadosAlias = new ArrayList< Map<
	// String, Object >>();
	// List< Map< String, String >> resultadosServicios = new ArrayList< Map<
	// String, String >>();
	// List< Map< String, Object >> resultadosReglas = new ArrayList< Map<
	// String, Object >>();
	// List< Map< String, Object >> resultadosCombinacion = new ArrayList< Map<
	// String, Object >>();
	// Long idAlias = Long.parseLong( filtros.get( Constantes.ID_ALIAS ) );
	// AliasDTO aliasConcretoDTO = null;
	//
	// Alias alias = null;
	// if ( idAlias != 0 ) {
	// alias = boAlias.getOne( idAlias );
	// aliasConcretoDTO = new AliasDTO( alias );
	// }
	//
	// List< AliasDTO > allAlias = null;
	// allAlias = boAlias.getListaAlias();
	//
	// for ( AliasDTO aliasActual : allAlias ) {
	// Map< String, Object > resultadoAlias = new Hashtable< String, Object >();
	// resultadoAlias.put( Constantes.ID_ALIAS, aliasActual.getId() );
	// resultadoAlias.put( Constantes.ALIAS_DESCRIPCION, aliasActual.getNombre()
	// );
	// resultadosAlias.add( resultadoAlias ); // Contiene todos los contactos }
	// }
	//
	// Iterable< Servicios > servicios = boServicios.findAll();
	//
	// for ( Servicios servicio : servicios ) {
	// Map< String, String > resultadoServicio = new Hashtable< String, String
	// >();
	// resultadoServicio.put( Constantes.SERVICIO_ID,
	// servicio.getId().toString() );
	// resultadoServicio.put( Constantes.SERVICIO_NOMBRE,
	// servicio.getNombreServicio() );
	// resultadoServicio.put( Constantes.SERVICIO_DESCRIPCION,
	// servicio.getDescripcionServicio() );
	// resultadosServicios.add( resultadoServicio ); // Contiene todos los
	// servicios
	// }
	//
	// List< ReglaPeriodicidad > reglas = boRegla.findAll();
	//
	// for ( ReglaPeriodicidad regla : reglas ) {
	// Map< String, Object > resultadoRegla = new Hashtable< String, Object >();
	//
	// resultadoRegla.put( Constantes.REGLA_ID, regla.getId() );
	// resultadoRegla.put( Constantes.REGLA_AND_PARAM_ID_REGLA_USUARIO,
	// regla.getIdReglaUsuario() );
	// resultadoRegla.put( Constantes.REGLA_DESCRIPCION, regla.getDescripcion()
	// );
	// resultadoRegla.put( "activa", regla.getActivo() );
	// resultadosReglas.add( resultadoRegla ); // Contiene todas las reglas
	// }
	//
	// if ( idAlias != 0 ) {
	// // Si tenemos un alias concreto solamente vamos a iterar por el
	// allAlias = new ArrayList< AliasDTO >();
	// allAlias.add( aliasConcretoDTO );
	// }
	//
	// List< ReglasLanzadorServiciosDTO > reglasLanzador =
	// boReglasLanzadorServicios.getListaReglasLanzadorServicios();
	//
	// for ( AliasDTO aliasActual : allAlias ) {
	// Map< String, Object > resultadoContacto = new Hashtable< String, Object
	// >();
	//
	// ArrayList< Map< String, String >> resultadosServicio = new ArrayList<
	// Map< String, String >>();
	//
	// List< ReglasLanzadorServiciosDTO > reglasLanzadorPorAlias =
	// getListReglasLanzadorByAlias( aliasActual, reglasLanzador );
	//
	// for ( ReglasLanzadorServiciosDTO reglaLanzador : reglasLanzadorPorAlias )
	// {
	//
	// Map< String, String > resultadoServicio = new Hashtable< String, String
	// >();
	//
	// resultadoServicio.put( Constantes.SERVICIO_ID,
	// reglaLanzador.getIdServicio().toString() );
	// resultadoServicio.put( Constantes.ID_REGLA_SERVICIO,
	// reglaLanzador.getIdRegla().toString() );
	// resultadosServicio.add( resultadoServicio );
	// }
	// if ( reglasLanzadorPorAlias != null && reglasLanzadorPorAlias.size() > 0
	// ) {
	// resultadoContacto.put( Constantes.ID_ALIAS, aliasActual.getId() );
	// resultadoContacto.put( Constantes.CONTACTO_SERVICIOS, resultadosServicio
	// );
	// resultadosCombinacion.add( resultadoContacto );
	// }
	// }
	//
	// resultadosFinal.put( "alias", resultadosAlias );
	// resultadosFinal.put( Constantes.GETLIST_SERVICIOS, resultadosServicios );
	// resultadosFinal.put( Constantes.GETLIST_REGLAS, resultadosReglas );
	// resultadosFinal.put( Constantes.GETLIST_COMBINACION,
	// resultadosCombinacion );
	//
	// return resultadosFinal;
	// }

	private Map< String, Object > getReglas( Map< String, String > filtros ) {
		Map< String, Object > resultadosFinal = new Hashtable< String, Object >();
		List< Map< String, Object >> resultadosReglas = new ArrayList< Map< String, Object > >();

		List< ReglaPeriodicidad > reglas = boRegla.findAll();

		for ( ReglaPeriodicidad regla : reglas ) {
			Map< String, Object > resultadoRegla = new Hashtable< String, Object >();

			resultadoRegla.put( Constantes.REGLA_ID, regla.getId() );
			resultadoRegla.put( Constantes.REGLA_AND_PARAM_ID_REGLA_USUARIO, regla.getIdReglaUsuario() );
			resultadoRegla.put( Constantes.REGLA_DESCRIPCION, regla.getDescripcion() );
			resultadoRegla.put( "activa", regla.getActivo() );
			resultadosReglas.add( resultadoRegla ); // Contiene todas las reglas
		}

		resultadosFinal.put( Constantes.GETLIST_REGLAS, resultadosReglas );
		return resultadosFinal;
	}

	/**
	 * getServicios
	 * 
	 * @param filtros
	 * @return
	 */
	private Map< String, Object > getServicios( Map< String, String > filtros ) {
		Iterable< Servicios > servicios = boServicios.findAll();
		Map< String, Object > resultadosFinal = new Hashtable< String, Object >();
		List< Map< String, String >> resultadosServicios = new ArrayList< Map< String, String > >();

		for ( Servicios servicio : servicios ) {
			Map< String, String > resultadoServicio = new Hashtable< String, String >();
			resultadoServicio.put( Constantes.SERVICIO_ID, servicio.getId().toString() );
			resultadoServicio.put( Constantes.SERVICIO_NOMBRE, servicio.getNombreServicio() );
			resultadoServicio.put( Constantes.SERVICIO_DESCRIPCION, servicio.getDescripcionServicio() );
			resultadoServicio.put( Constantes.SERVICIO_CONFIG_ALIAS, servicio.getConfiguracionAlias().toString() );
			resultadoServicio.put( Constantes.SERVICIO_ACTIVO, servicio.getActivo().toString() );
			resultadosServicios.add( resultadoServicio ); // Contiene todos los
															// servicios
		}

		resultadosFinal.put( Constantes.GETLIST_SERVICIOS, resultadosServicios );
		return resultadosFinal;
	}

	/**
	 * getCombinacion
	 * 
	 * @param filtros
	 * @return
	 */
	private Map< String, Object > getCombinacion( Map< String, String > filtros ) {

		Long idAlias = Long.parseLong( filtros.get( Constantes.ID_ALIAS ) );
		AliasDTO aliasConcretoDTO = null;
		Alias alias = null;
		alias = boAlias.findById( idAlias );
		aliasConcretoDTO = new AliasDTO( alias );

		Map< String, Object > resultadosFinal = new Hashtable< String, Object >();
		Map< String, Object > resultadoContacto = new Hashtable< String, Object >();
		List< Map< String, Object >> resultadosCombinacion = new ArrayList< Map< String, Object > >();

		ArrayList< Map< String, String >> resultadosServicio = new ArrayList< Map< String, String >>();

		List< ReglasLanzadorServicios > reglasLanzador = boReglasLanzadorServicios.findAllByAliasOrderByAlias( alias );

		for ( ReglasLanzadorServicios reglaLanzador : reglasLanzador ) {

			Map< String, String > resultadoServicio = new Hashtable< String, String >();

			resultadoServicio.put( Constantes.SERVICIO_ID, reglaLanzador.getServicio().getId().toString() );
			resultadoServicio.put( Constantes.ID_REGLA_SERVICIO, reglaLanzador.getRegla().getId().toString() );
			resultadosServicio.add( resultadoServicio );
		}
		if ( reglasLanzador != null && reglasLanzador.size() > 0 ) {
			resultadoContacto.put( Constantes.ID_ALIAS, aliasConcretoDTO.getId() );
			resultadoContacto.put( Constantes.CONTACTO_SERVICIOS, resultadosServicio );
			resultadosCombinacion.add( resultadoContacto );
		}

		resultadosFinal.put( Constantes.GETLIST_COMBINACION, resultadosCombinacion );

		return resultadosFinal;

	}

	/**
	 * getAliasCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return the list by alias check filters
	 */
	private boolean getCombinacionCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filtros = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		Long idAlias = Long.parseLong( filtros.get( Constantes.ID_ALIAS ) );

		if ( filtros.get( Constantes.ID_ALIAS ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_ALIAS + " must be selected. ";
			filtersRight = false;
		} else {
			if ( boAlias.findById( Long.parseLong( filtros.get( Constantes.ID_ALIAS ) ) ) == null ) {
				errorMsg = errorMsg + "There is no such " + Constantes.ID_ALIAS + ":" + idAlias + " in the database. ";
				filtersRight = false;
			}
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	/**
	 * updateServicioRegla.
	 *
	 * @param filtros
	 *            the filtros
	 * @return the map
	 */
	private Map< String, String > updateServicioRegla( Map< String, String > filtros ) {
		Map< String, String > resultado = new Hashtable< String, String >();

		Long idAlias = Long.parseLong( filtros.get( Constantes.ID_ALIAS_SERVICIO ) );
		Long idRegla = Long.parseLong( filtros.get( Constantes.ID_REGLA_SERVICIO ) );
		Long idServicio = Long.parseLong( filtros.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Alias alias = boAlias.findById( idAlias );
		ReglaPeriodicidad regla = boRegla.findById( idRegla );
		Servicios servicio = boServicios.findById( idServicio );

		Boolean result = false;
		result = boReglasLanzadorServicios.updateServicio( alias, servicio, regla );

		if ( result == true ) {
			resultado.put( "result", result.toString() );
		}

		return resultado;
	}

	/**
	 * updateServicioReglaCheckFilters.
	 *
	 * @param webRequest
	 *            the web request
	 * @param webResponse
	 *            the web response
	 * @return true, if successful
	 */
	@SuppressWarnings( "unused" )
	private boolean updateServicioReglaCheckFilters( WebRequest webRequest, WebResponse webResponse ) {

		Map< String, String > filters = webRequest.getFilters();
		String errorMsg = new String();
		boolean filtersRight = true;

		if ( filters.get( Constantes.ID_ALIAS_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_ALIAS_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_REGLA_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_REGLA_SERVICIO + " must be set. ";
			filtersRight = false;
		}
		if ( filters.get( Constantes.ID_SERVICIO_SERVICIO ) == null ) {
			errorMsg = errorMsg + "A " + Constantes.ID_SERVICIO_SERVICIO + " must be set. ";
			filtersRight = false;
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
			return filtersRight;
		}

		Long idAlias = Long.parseLong( filters.get( Constantes.ID_ALIAS_SERVICIO ) );
		Long idRegla = Long.parseLong( filters.get( Constantes.ID_REGLA_SERVICIO ) );
		Long idServicio = Long.parseLong( filters.get( Constantes.ID_SERVICIO_SERVICIO ) );

		Alias alias = null;
		ReglaPeriodicidad regla = null;
		Servicios servicio = null;

		// Comprobamos si el alias que se nos pasa existe en la tabla
		// tmct0_alias
		try {
			alias = boAlias.findById( idAlias );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Alias " + idAlias + " does not exist";
		}
		// Comprobamos si la regla que se nos pasa existe en la tabla
		// tmct0_contacto
		try {
			regla = boRegla.findById( idRegla );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Rule " + idRegla + " does not exist";
		}
		// Comprobamos si el servicio que se nos pasa existe en la tabla
		// tmct0_servicio
		try {
			servicio = boServicios.findById( idServicio );
		} catch ( Exception e ) {
			filtersRight = false;
			errorMsg = errorMsg + "Service " + idServicio + " does not exist";
		}

		if ( filtersRight == false ) {
			webResponse.setError( errorMsg );
			return filtersRight;
		}

		Pk pk = null;

		if ( alias != null && servicio != null ) {
			pk = new Pk( alias.getId(), servicio.getId() );
		} else {
			filtersRight = false;
		}

		if ( filtersRight == true ) {
			if ( boReglasLanzadorServicios.findById( pk ) == null ) {
				errorMsg = errorMsg + "This association does not exist";
				filtersRight = false;
			}
		} else {
			webResponse.setError( errorMsg );
		}

		return filtersRight;
	}

	/**
	 * Metod Internal getListReglasLanzadorByAlias
	 * 
	 * @param aliasActual
	 * @param reglasLanzador
	 * @return
	 */
	// private List< ReglasLanzadorServiciosDTO > getListReglasLanzadorByAlias(
	// AliasDTO aliasActual,
	// List< ReglasLanzadorServiciosDTO > reglasLanzador ) {
	//
	// List< ReglasLanzadorServiciosDTO > returnedReglas = new ArrayList<
	// ReglasLanzadorServiciosDTO >();
	//
	// for ( ReglasLanzadorServiciosDTO regla : reglasLanzador ) {
	// if ( aliasActual.getId().equals( regla.getIdAlias() ) ) {
	// returnedReglas.add( regla );
	// }
	// }
	//
	// return returnedReglas;
	// }

	/**
	 * Gets the simple name.
	 *
	 * @return the simple name
	 */
	public static String getSimpleName() {
		return simpleName;
	}

}
