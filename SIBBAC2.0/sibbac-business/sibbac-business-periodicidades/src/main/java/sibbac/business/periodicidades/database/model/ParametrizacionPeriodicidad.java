package sibbac.business.periodicidades.database.model;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.poi.ss.formula.functions.T;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Entity: "ParametrizacionPeriodicidad".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@Table( name = DBConstants.PERIODICIDADES.PARAMETRIZACIONES_PERIODICIDAD )
@Entity
// @Component
@XmlRootElement
@Audited
public class ParametrizacionPeriodicidad extends ATableAudited< ParametrizacionPeriodicidad > {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -2121766335744504718L;

	@Column( name = "DIAMES" )
	@XmlAttribute
	private Integer				diaMes;

	@Column( name = "EXCEPCIONARFESTIVOS" )
	@XmlAttribute
	private boolean				excepcionarFestivos;

	@Column( name = "LABORABLEANTERIOR", nullable = false )
	@XmlAttribute
	private boolean				laborableAnterior;

	@Column( name = "LABORABLEPOSTERIOR", nullable = false )
	@XmlAttribute
	private boolean				laborablePosterior;

	@Column( name = "HORA", nullable = false )
	@XmlAttribute
	private Integer				hora;

	@Column( name = "MINUTO", nullable = false )
	@XmlAttribute
	private Integer				minuto;

	@Column( name = "MARGENANTERIOR", nullable = false )
	@XmlAttribute
	private Integer				margenAnterior;

	@Column( name = "MARGENPOSTERIOR", nullable = false )
	@XmlAttribute
	private Integer				margenPosterior;

	@Column( name = "FECHADESDE" )
	@XmlAttribute
	private Date				fechaDesde;

	@Column( name = "FECHAHASTA" )
	@XmlAttribute
	private Date				fechaHasta;

	@Column( name = "ACTIVO", nullable = false )
	@XmlAttribute
	private boolean				activo;

	@ManyToOne( targetEntity = ReglaPeriodicidad.class )
	@JoinColumn( name = "IDREGLA", nullable = false, referencedColumnName = "ID" )
	@XmlAttribute
	@AuditJoinTable
	private ReglaPeriodicidad	reglaPeriodicidad;

	@ManyToOne( targetEntity = Mes.class )
	@JoinColumn( name = "MES", nullable = true, referencedColumnName = "ID" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private Mes					mes;

	@ManyToOne( targetEntity = DiaSemana.class )
	@JoinColumn( name = "DIASEMANA", nullable = true, referencedColumnName = "ID" )
	@Audited( targetAuditMode = RelationTargetAuditMode.NOT_AUDITED )
	@XmlAttribute
	private DiaSemana			diaSemana;

	// ----------------------------------------------- Bean Constructors

	public ParametrizacionPeriodicidad() {
		super();
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for diaMes
	 *
	 * @return the diaMes
	 */
	public Integer getDiaMes() {
		return this.diaMes;
	}

	/**
	 * Getter for excepcionarFestivos
	 *
	 * @return the excepcionarFestivos
	 */
	public boolean getExcepcionarFestivos() {
		return this.excepcionarFestivos;
	}

	/**
	 * Getter for laborableAnterior
	 *
	 * @return the laborableAnterior
	 */
	public boolean getLaborableAnterior() {
		return this.laborableAnterior;
	}

	/**
	 * Getter for laborablePosterior
	 *
	 * @return the laborablePosterior
	 */
	public boolean getLaborablePosterior() {
		return this.laborablePosterior;
	}

	/**
	 * Getter for hora
	 *
	 * @return the hora
	 */
	public Integer getHora() {
		return this.hora;
	}

	/**
	 * Getter for minuto
	 *
	 * @return the minuto
	 */
	public Integer getMinuto() {
		return this.minuto;
	}

	/**
	 * Getter for margenAnterior
	 *
	 * @return the margenAnterior
	 */
	public Integer getMargenAnterior() {
		return this.margenAnterior;
	}

	/**
	 * Getter for margenPosterior
	 *
	 * @return the margenPosterior
	 */
	public Integer getMargenPosterior() {
		return this.margenPosterior;
	}

	/**
	 * Getter for fechaDesde
	 *
	 * @return the fechaDesde
	 */
	public Date getFechaDesde() {
		return this.fechaDesde;
	}

	/**
	 * Getter for fechaHasta
	 *
	 * @return the fechaHasta
	 */
	public Date getFechaHasta() {
		return this.fechaHasta;
	}

	/**
	 * Getter for activo
	 *
	 * @return the activo
	 */
	public boolean getActivo() {
		return this.activo;
	}

	public ReglaPeriodicidad getReglaPeriodicidad() {
		return reglaPeriodicidad;
	}

	public Mes getMes() {
		return mes;
	}

	public DiaSemana getDiaSemana() {
		return diaSemana;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for diaMes
	 *
	 * @param diaMes the diaMes to set
	 */
	public void setDiaMes( Integer diaMes ) {
		this.diaMes = diaMes;
	}

	/**
	 * Setter for excepcionarFestivos
	 *
	 * @param excepcionarFestivos the excepcionarFestivos to set
	 */
	public void setExcepcionarFestivos( boolean excepcionarFestivos ) {
		this.excepcionarFestivos = excepcionarFestivos;
	}

	/**
	 * Setter for laborableAnterior
	 *
	 * @param laborableAnterior the laborableAnterior to set
	 */
	public void setLaborableAnterior( boolean laborableAnterior ) {
		this.laborableAnterior = laborableAnterior;
	}

	/**
	 * Setter for laborablePosterior
	 *
	 * @param laborablePosterior the laborablePosterior to set
	 */
	public void setLaborablePosterior( boolean laborablePosterior ) {
		this.laborablePosterior = laborablePosterior;
	}

	/**
	 * Setter for hora
	 *
	 * @param hora the hora to set
	 */
	public void setHora( Integer hora ) {
		this.hora = hora;
	}

	/**
	 * Setter for minuto
	 *
	 * @param minuto the minuto to set
	 */
	public void setMinuto( Integer minuto ) {
		this.minuto = minuto;
	}

	/**
	 * Setter for margenAnterior
	 *
	 * @param margenAnterior the margenAnterior to set
	 */
	public void setMargenAnterior( Integer margenAnterior ) {
		this.margenAnterior = margenAnterior;
	}

	/**
	 * Setter for margenPosterior
	 *
	 * @param margenPosterior the margenPosterior to set
	 */
	public void setMargenPosterior( Integer margenPosterior ) {
		this.margenPosterior = margenPosterior;
	}

	/**
	 * Setter for fechaDesde
	 *
	 * @param fechaDesde the fechaDesde to set
	 */
	public void setFechaDesde( Date fechaDesde ) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * Setter for fechaHasta
	 *
	 * @param fechaHasta the fechaHasta to set
	 */
	public void setFechaHasta( Date fechaHasta ) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * Setter for activo
	 *
	 * @param activo the activo to set
	 */
	public void setActivo( boolean activo ) {
		this.activo = activo;
	}

	public void setReglaPeriodicidad( ReglaPeriodicidad reglaPeriodicidad ) {
		this.reglaPeriodicidad = reglaPeriodicidad;
	}

	public void setMes( Mes mes ) {
		this.mes = mes;
	}

	public void setDiaSemana( DiaSemana diaSemana ) {
		this.diaSemana = diaSemana;
	}

	// ------------------------------------------------ Business Methods

	// ----------------------------------------------- Inherited Methods

	/**
	 * Para comparar dos entidades del mismo tipo.
	 * 
	 * @return The comparison result.
	 * @see java.lang.Comparable#compareTo(T)
	 */
	@Override
	public int compareTo( final ParametrizacionPeriodicidad other ) {
		// Si el "padre" ya es distinto...
		int father = super.compareTo( other );
		if ( father != 0 ) {
			return father;
		}

		// Entity concrete fields.
		int internal = 0;
		return ( internal == 0 ) ? 0 : -3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[PARAMETRIZACION PERIODICIDAD==" + this.id + " regla periodicidad: " + this.reglaPeriodicidad + " mes: " + this.mes
				+ " dia del mes: " + this.diaMes + " dia semana: " + this.diaSemana + " excepcionarFestivos: " + this.excepcionarFestivos
				+ " laborableAnterior: " + this.laborableAnterior + " laborablePosterior: " + this.laborablePosterior + " hora: "
				+ this.hora + " minuto: " + this.minuto + " margenAnterior: " + this.margenAnterior + "margenPosterior:" + this.margenPosterior +
				" fechaDesde: "	+ this.fechaDesde.toString() + " fechaHasta: " + this.fechaHasta.toString() + " activo: " + this.activo + "]";
	}
}
