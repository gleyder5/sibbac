package sibbac.business.fixml.database;


// Internal
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import sibbac.business.fixml.database.bo.FixmlFileBo;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@TransactionConfiguration( transactionManager = "transactionManager", defaultRollback = false )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class TestFichero extends ADBTest {

	FixmlFileBo	boFichero	= new FixmlFileBo();

	// @Test
	// @Ignore
	// @Transactional( propagation = Propagation.REQUIRED )
	// public void test01AltaDeFichero() {
	// try{
	// assumeNotNull( boFichero );
	// this.altaDeFichero( "tipo1" );
	// idBetaTester = this.altaDeFichero( "Otro nuevo fichero" );
	// //this.listaFicheros();
	// LOG.debug( "ID Fichero BetaTester: " + idBetaTester );
	// } catch (Exception e){
	// LOG.debug( "Exception: " + e.getMessage() );
	// }
	// }

	@Test
	@Ignore
	// @Transactional( propagation = Propagation.REQUIRED )
			public
			void test02task() {
		@SuppressWarnings( "unused" )
		int i = 0;

		try {
			// callTask();
			LOG.debug( "LFMM Task Test: " );
			// Thread.currentThread().sleep(1000*6*60);
			while ( true ) {
				i = 0;
			}
		} catch ( Exception e ) {
			LOG.debug( "Exception: " + e.getMessage() );
		}
	}

	/*
	 * @Test
	 * 
	 * @Transactional( propagation = Propagation.REQUIRED )
	 * public void test02Fichero() {
	 * assumeNotNull( boFichero );
	 * LOG.debug( "Veamos los ficheros que hay en la base de datos..." );
	 * this.listaFicheros();
	 * LOG.debug( "Buscando al BetaTester por el ID Fichero BetaTester: " + idBetaTester );
	 * Fichero fichero = boFichero.findById( idBetaTester );
	 * assertNotNull( "Fichero nulo", fichero );
	 * LOG.debug( "Fichero: " + fichero );
	 * 
	 * LOG.debug( "Fichero[id]: " + fichero.getId() );
	 * LOG.debug( "Fichero[name]: " + fichero.getTipo());
	 * }
	 */

	// ------------------------------------------------ Internal Methods

}
