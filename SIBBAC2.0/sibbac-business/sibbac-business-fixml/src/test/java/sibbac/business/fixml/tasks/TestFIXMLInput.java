package sibbac.business.fixml.tasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sibbac.test.BaseTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring.xml"})
@Component
public class TestFIXMLInput extends BaseTest {
	@Autowired TaskFIXMLInput task;

	@Test public void fixmlInput() {
		task.executeTask();
	}
}
