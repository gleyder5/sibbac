package sibbac.business.fixml.database.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


@Table( name = DBConstants.FIXML.ESQUEMASXSD )
@Entity
@XmlRootElement
@Audited
public class FixmlXsd extends ATableAudited< FixmlXsd > implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 7664570590942538564L;

	@Column( name = "nombreXml", nullable = false, length = 20 )
	@XmlAttribute
	private String	nombreXml;

	@Column( name = "nombreXsd", nullable = false, length = 60 )
	@XmlAttribute
	private String	nombreXsd;

	// ----------------------------------------------- Bean Constructors

	public FixmlXsd() {
		super();
	}

	// ------------------------------------------- Bean methods: Getters

	public String getNombreXml() {
		return this.nombreXml;
	}

	public String getNombreXsd() {
		return this.nombreXsd;
	}

	// ------------------------------------------- Bean methods: Setters

	public void setNombreXml( String _nombrexml ) {
		this.nombreXml = _nombrexml;
	}

	public void setNombreXsd( String _nombrexsd ) {
		this.nombreXsd = _nombrexsd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[FICHEROXSD==" + this.id + "]";
	}

}
