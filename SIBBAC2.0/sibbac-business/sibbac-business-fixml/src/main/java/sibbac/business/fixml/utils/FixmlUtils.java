package sibbac.business.fixml.utils;


import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;


public class FixmlUtils {

	private static final String	bmeFileDatepattern	= "yyyyMMddHHmmssSS";

	public static Timestamp getTimestampFromBmeFileName( final String nombreFicheroZipBme ) {
		Timestamp result;
		String[] nombreSplit = nombreFicheroZipBme.split( "_" );
		String fechaAMD = nombreSplit[ 4 ];
		String fechaHMS = nombreSplit[ 5 ].split( "\\." )[ 0 ];
		StringBuilder fechaBuilder = new StringBuilder( fechaAMD ).append( fechaHMS );

		try {
			Date fecha = DateUtils.parseDate( fechaBuilder.toString(), bmeFileDatepattern );
			result = new Timestamp( fecha.getTime() );
		} catch ( IllegalArgumentException | ParseException e ) {
			result = new Timestamp( Calendar.getInstance().getTimeInMillis() );
		}

		return result;
	}
}
