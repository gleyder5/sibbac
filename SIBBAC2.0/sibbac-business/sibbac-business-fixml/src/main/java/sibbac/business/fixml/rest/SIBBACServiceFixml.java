package sibbac.business.fixml.rest;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fixml.database.bo.FixmlFileBo;
import sibbac.business.fixml.database.bo.FixmlXsdBo;
import sibbac.business.fixml.database.model.FixmlFile;
import sibbac.business.fixml.database.model.FixmlXsd;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;


@SIBBACService
public class SIBBACServiceFixml implements SIBBACServiceBean {

	private static final Logger		LOG						= LoggerFactory.getLogger( SIBBACServiceFixml.class );

	protected static final String	DATE_TIME_FORMAT_ES		= "dd/MM/yyyy HH:mm:ss";
	protected static final String	DATE_TIME_FORMAT_EN		= "MM/dd/yyyy HH:mm:ss";

	@Autowired
	private FixmlFileBo				boFixmlFile;

	@Autowired
	private FixmlXsdBo				boFixmlXsd;

	@Value( "${sibbac.fixml.inputdir:/temp}" )
	private String					dirPath;

	protected static String			moduleleName			= "SIBBACServiceFixml";

	// public static final String KEY_COMMAND = "command";

	// Consulta
	public static final String		CMD_GET_DISTINCT_TIPO	= "getDistinctTipo";
	public static final String		CMD_GET_ALL_BY_TIPO		= "getAllByTipo";
	public static final String		CMD_GET_BY_TIPO_DATE	= "getByTipoDate";
	public static final String		CMD_GET_XSD_BY_XML		= "getXsdByXml";

	// Fields - campos de recogida de datos

	public static final String		FILETYPE				= "filetype";
	public static final String		FILEDATE				= "filedate";
	public static final String		XMLTYPE					= "xmltype";

	public SIBBACServiceFixml() {
	}

	@Override
	public WebResponse process( WebRequest webRequest ) {
		String prefix = "[" + SIBBACServiceFixml.moduleleName + "::process] ";
		LOG.debug( prefix + "Processing a web request..." );
		WebResponse webResponse = new WebResponse();

		String command = webRequest.getAction();
		if ( command == null )
			command = SIBBACServiceFixml.CMD_GET_DISTINCT_TIPO;

		LOG.debug( prefix + "+ Command.: [{}]", command );

		Map< String, String > params = webRequest.getFilters();
		Map< String, Object > response = new HashMap< String, Object >();

		switch ( command ) {
			case CMD_GET_ALL_BY_TIPO:
				List< FixmlFile > resultado = this.getAllByTipo( params );
				response.put( "resultados", resultado );
				webResponse.setResultados( response );
				break;
			case CMD_GET_BY_TIPO_DATE:
				List< FixmlFile > resultado2 = this.getByTipoDate( params );
				response.put( "resultados", resultado2 );
				webResponse.setResultados( response );
				break;
			case CMD_GET_XSD_BY_XML:
				FixmlXsd resultado3 = this.findByNombreXml( params );
				response.put( "resultados", resultado3 );
				webResponse.setResultados( response );
				break;

			case CMD_GET_DISTINCT_TIPO:
				List< String > tipos = boFixmlFile.findDistinctTipo();
				response.put( "resultados", tipos );
				webResponse.setResultados( response );
				break;
			default:
		}

		return webResponse;
	}

	@Override
	public List< String > getAvailableCommands() {
		List< String > commands = new ArrayList< String >();
		commands.add( CMD_GET_DISTINCT_TIPO );
		commands.add( CMD_GET_ALL_BY_TIPO );
		commands.add( CMD_GET_BY_TIPO_DATE );
		commands.add( CMD_GET_XSD_BY_XML );
		return commands;
	}

	@Override
	public List< String > getFields() {
		// TODO Auto-generated method stub
		return null;
	}

	public List< FixmlFile > getAllByTipo( Map< String, String > params ) {

		String tipo = params.get( FILETYPE );
		if ( tipo == null || tipo.isEmpty() )
			return null;
		else
			return boFixmlFile.findByTipo( tipo );
	}

	public FixmlXsd findByNombreXml( Map< String, String > params ) {

		String nombreXml = params.get( XMLTYPE );
		if ( nombreXml == null || nombreXml.isEmpty() )
			return null;
		else
			return boFixmlXsd.findByNombreXml( nombreXml ).get( 0 );
	}

	public List< FixmlFile > getByTipoDate( Map< String, String > params ) {

		String tipo = params.get( FILETYPE );
		String sdate = params.get( FILEDATE );

		if ( tipo == null || tipo.isEmpty() )
			return null;
		else if ( sdate == null || sdate.isEmpty() )
			return null;
		else {
			Long l = Long.valueOf( params.get( FILEDATE ) );
			// Date datetemp = new Date();
			// datetemp.setTime( l );
			// Timestamp date= Timestamp().setTime( l );;
			return boFixmlFile.findByTipoAndFerecepcion( tipo, new java.sql.Timestamp( l ) );
		}

	}

	public String getDirPath() {
		return this.dirPath;
	}

	public void setDirPath( String dirPath ) {
		this.dirPath = dirPath;
	}

	public FixmlFile findById( final String id ) {

		if ( id == null || id.trim().length() == 0 ) {
			return null;
		} else {
			return this.boFixmlFile.findById( new Long( id ) );
		}

	}

}
