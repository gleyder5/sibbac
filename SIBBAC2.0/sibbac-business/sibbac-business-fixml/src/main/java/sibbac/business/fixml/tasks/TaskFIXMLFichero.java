package sibbac.business.fixml.tasks;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
// import org.springframework.core.io.Resource;
import org.springframework.util.ResourceUtils;
import org.xml.sax.SAXException;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO;
import sibbac.business.fixml.database.bo.FixmlFileBo;
import sibbac.business.fixml.database.bo.FixmlXsdBo;
import sibbac.business.fixml.database.model.FixmlFile;
import sibbac.business.fixml.database.model.FixmlXsd;
import sibbac.business.wrappers.tasks.WrapperTaskConcurrencyPrevent;
import sibbac.common.SIBBACBusinessException;

/**
 * Task para procesar los archivos GROUP_FIXML.
 * <p/>
 * 
 * Es llamada desde {@link TaskFIXMLInput}.<br/>
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public class TaskFIXMLFichero extends WrapperTaskConcurrencyPrevent {

  protected static final Logger LOG = LoggerFactory.getLogger(TaskFIXMLFichero.class);

  static Map<String, String> xmlXSD = new HashMap<String, String>();

  @Autowired
  protected ApplicationContext applicationContext;

  @Value("${sibbac.fixml.inputdir:/temp}")
  private String dirPath;

  // private String procesados = "procesados";
  // private String temporal = "temporal";
  private String erroneos = "erroneos";
  // private String xsd = "xsd";

  @Autowired
  private FixmlFileBo registroBo;
  @Autowired
  private FixmlXsdBo xsdBo;

  public TaskFIXMLFichero() {
  }

  @Override
  public void executeTask() throws SIBBACBusinessException {
    String prefix = "[TaskFIXMLFichero::execute] ";

    /*
     * Dependiendo de como se procese esta informacion hay que saber que tipo de
     * mensaje XML se ha recibido y se corresponde con el nombre del fichero sin
     * extensiones, y habría que cruzarlo contra una tabla estatica de datos
     */
    // cargarXML_XSD();
    LOG.debug(prefix + "Iniciando... Información recibida del JobDataMap:");
    Set<String> keys = this.jobDataMap.keySet();
    if (keys != null) {
      for (String key : keys) {
        LOG.debug(prefix + "+ [{}=={}]...", key, this.jobDataMap.get(key));
      }
    }
    else {
      LOG.warn(prefix + "- No hay informacion en el JobDataMap!");
    }

    Path directoryPath = Paths.get(dirPath);
    // String procesadosDir = directoryPath + File.separator + procesados;
    String erroneosDir = directoryPath + File.separator + erroneos;
    // String temporalDir = directoryPath + File.separator + temporal;
    // Resource resource;
    LOG.debug(prefix + "Explorando la carpeta [erroneosDir=={}]...", erroneosDir);

    // ClassLoader classLoader = getClass().getClassLoader();
    File xmlTempFile = null;
    try {

      String nombreXML = "";
      // String fechaProceso = "";
      // String fechaRecepcion = "";
      String nombreFichero = "";
      String nombreFicheroXMLTemporal = "";
      String zipFichero = "";

      nombreXML = this.jobDataMap.get("name").toString();

      // fechaProceso = this.jobDataMap.get( "fproceso" ).toString();
      // fechaRecepcion = this.jobDataMap.get( "frecepcion" ).toString();
      zipFichero = this.jobDataMap.get("ficherozip").toString();
      nombreFichero = this.jobDataMap.get("fichero").toString();
      nombreFicheroXMLTemporal = this.jobDataMap.get("ficheroPath").toString();
      xmlTempFile = new File(nombreFicheroXMLTemporal);
      LOG.debug(prefix + "Procesando archivo: [{}]", nombreFicheroXMLTemporal);

      LOG.debug(prefix + "+ File type: [{}]:", getNombreFichero(nombreXML));

      // verificacion del XML contra el XSD
      LOG.debug(prefix + "Buscando la lista de archivos XSD...");
      LOG.debug(prefix + "+        nombreXML: [{}]:", nombreXML);
      LOG.debug(prefix + "+ getNombreFichero: [{}]:", getNombreFichero(nombreXML));
      List<FixmlXsd> listaArchivosXsd = xsdBo.findByNombreXml(getNombreFichero(nombreXML));

      if (listaArchivosXsd != null && !(listaArchivosXsd.isEmpty())) {

        String ficheroXSD = listaArchivosXsd.get(0).getNombreXsd();

        LOG.debug(prefix + "+ Schema: [{}]", ficheroXSD);

        if (validateXMLSchema(ficheroXSD, nombreFicheroXMLTemporal)) {
          LOG.debug(prefix + "  Schema VALIDO: [{}]", ficheroXSD);
          Timestamp timetemp = (Timestamp) this.jobDataMap.get("frecepcion");
          List<FixmlFile> lregistro = registroBo.findByTipoAndFerecepcion(nombreXML, timetemp);
          if (lregistro == null || lregistro.isEmpty()) {
            FixmlFile registro = new FixmlFile();
            registro.setAuditUser("TaskFIXMLFichero");
            registro.setAuditDate(new java.util.Date());
            registro.setVersion(new Long(0));
            registro.setTipo(nombreXML);
            registro.setZip(zipFichero);
            registro.setFerecepcion(timetemp);
            registro.setFeproceso((Timestamp) this.jobDataMap.get("fproceso"));
            registroBo.save(registro);
            LOG.debug(prefix + "  Salvada la informacion en base de datos.");
          }
          else if (lregistro.size() == 1) {
            LOG.warn(prefix + "  Registro ya existente para [{}]", nombreFicheroXMLTemporal);
          }
          else {
            LOG.error(prefix + "  Registro duplicado: tipo {} - feproceso {}", nombreXML, timetemp.toString());
          }
          // borro el fichero del directorio temporal
          LOG.debug(prefix + "  Borrando el fichero del directorio temporal [{}]...", xmlTempFile);
          if (!xmlTempFile.delete()) {
            LOG.warn(prefix + "  - Fichero NO eliminado del directorio temporal [{}].", nombreFicheroXMLTemporal);
            String erroneo = erroneosDir + File.separator + nombreFichero;
            if (!xmlTempFile.renameTo(new File(erroneo))) {
              LOG.warn(prefix + "  - Fichero NO movido al directorio de erroneos [{}].", erroneo);
            }
            else {
              LOG.debug(prefix + "  + Fichero movido al directorio de erroneos [{}].", erroneo);
            }
          }
          else {
            LOG.debug(prefix + "  + Fichero borrado del directorio temporal [{}].", xmlTempFile);
          }
        }
        else {
          LOG.debug(prefix + "  Schema NO VALIDO: [{}]", ficheroXSD);
          // muevo el fichero a la carpeta de erroneos
          String erroneo = erroneosDir + File.separator + nombreFichero;
          LOG.debug(prefix + "  Archivo no valido: [{}]. Renombrando a [{}]...", nombreFicheroXMLTemporal, erroneo);
          if (!xmlTempFile.renameTo(new File(erroneo))) {
            LOG.warn(prefix + "  - Archivo NO renombrado: [{}]", nombreFicheroXMLTemporal);
          }
          else {
            LOG.debug(prefix + "  + Archivo renombrado: [{}]", nombreFicheroXMLTemporal);
          }
        }
      }
      else {
        LOG.warn(prefix + "- No hay schema para el archivo: [{}]", nombreFicheroXMLTemporal);
      }
      LOG.debug(prefix + "[{}] time: [{}]", this.jobDataMap.get("name"), new Date());
    }
    catch (Exception e) {
      LOG.error("ERROR({}): {}", e.getClass().getName(), e.getMessage(), e);
    }

  }

  private boolean validateXMLSchema(String xsdPath, String xmlPath) {
    String prefix = "[TaskFIXMLFichero::validateXMLSchema] ";
    File file = null;
    String source = "xsd" + File.separator + xsdPath;
    try {
      LOG.debug(prefix + "Localizando el XSD: [{}] en la ruta: [classpath:xsd/{}]", xsdPath, xmlPath);
      file = ResourceUtils.getFile("classpath:" + source);
      LOG.debug(prefix + "Localizado el archivo en la ruta: [classpath:xsd/{}]", xsdPath);
    }
    catch (IOException e) {
      LOG.warn(prefix + "ERROR({}): Error accediendo al recurso mediante: ResourceUtils...: {}",
          e.getClass().getName(), e.getMessage());
    }

    if (file == null) {
      LOG.debug(prefix + "Intentando alternativas (applicationContext.getResource( 'source' )...)...");
      Resource resource = applicationContext.getResource(source);
      if (resource != null) {
        try {
          LOG.debug(prefix + "Localizado el archivo como recurso Spring: [{}]", source);
          LOG.debug(prefix + "+ [    getFile()=={}]", resource.getFile());
          LOG.debug(prefix + "+ [getFilename()=={}]", resource.getFilename());
          LOG.debug(prefix + "+ [     getURI()=={}]", resource.getURI());
          LOG.debug(prefix + "+ [     getURL()=={}]", resource.getURL());
          file = resource.getFile();
        }
        catch (IOException e) {
          LOG.warn(prefix + "ERROR({}): Error accediendo al recurso mediante Spring ApplicationContext: {}", e
              .getClass().getName(), e.getMessage());
        }
      }
      else {
        LOG.warn(prefix + "Recurso Spring no encontrado: [source=={}]", source);
      }
    }

    if (file == null) {
      LOG.debug(prefix + "Intentando alternativas (applicationContext.getResource( 'classpath:source' )...)...");
      Resource resource = applicationContext.getResource("classpath:" + source);
      if (resource != null) {
        try {
          file = resource.getFile();
          LOG.debug(prefix + "Localizado el archivo como recurso Spring: [classpath:xsd/{}]", xsdPath);
        }
        catch (IOException e) {
          LOG.warn(prefix + "ERROR({}): Error accediendo al recurso mediante Spring ApplicationContext: {}", e
              .getClass().getName(), e.getMessage());
        }
      }
      else {
        LOG.warn(prefix + "Recurso Spring no encontrado: [classpath:source==classpath:{}]", source);
      }
    }

    if (file == null) {
      LOG.debug(prefix + "Intentando alternativas (classLoader.getResource()...)...");
      try {
        ClassLoader cl = getClass().getClassLoader();
        URL url = cl.getResource(source);
        LOG.warn(prefix + "> URL: [{}]", url);
        String fileName = url.getFile();
        LOG.warn(prefix + "> fileName: [{}]", fileName);
        file = new File(fileName);
      }
      catch (NullPointerException | SecurityException e) {
        LOG.warn(prefix + "ERROR({}): {}", e.getClass().getName(), e.getMessage());
      }
    }

    if (file != null) {
      LOG.debug(prefix + "Detectado el archivo XSD en: [{}]", file);
      try {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(file);
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new File(xmlPath)));
        return true;
      }
      catch (SAXException | IOException e) {
        LOG.warn(prefix + "ERROR({}): {}", e.getClass().getName(), e.getMessage());
        List<File> listFiles = new ArrayList<File>();
        listFiles.add(file);
        listFiles.add(new File(xmlPath));
        this.sendMailWithExceptionMessage(null, e, listFiles);
        return false;
      }
    }
    else {
      return false;
    }
  }

  private String getNombreFichero(String fichero) {
    String prefix = "[TaskFIXMLFichero::getNombreFichero] ";
    String temp = fichero;
    try {
      return fichero.substring(0, fichero.indexOf('.'));
    }
    catch (Exception e) {
      LOG.error(prefix + "ERROR({}): {}", e.getClass().getName(), e.getMessage());
      return temp;
    }
  }

  @Override
  public TIPO determinarTipoApunte() {
    return null;
  }

}
