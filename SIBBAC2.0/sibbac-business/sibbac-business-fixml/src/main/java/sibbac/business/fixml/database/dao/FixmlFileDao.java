package sibbac.business.fixml.database.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.business.fixml.database.model.FixmlFile;

/**
 * Objeto para el acceso a la tabla <code>FixmlFile</code> de base de datos.
 * 
 * @author XI316153
 * @see org.springframework.data.jpa.repository.JpaRepository<FixmlFile, Long>
 */
@Repository
public interface FixmlFileDao extends JpaRepository<FixmlFile, Long> {

  public List<FixmlFile> findByTipoOrderByFerecepcionDesc(String tipo);

  public List<FixmlFile> findByTipoAndFerecepcionOrderByFerecepcion(String tipo, Timestamp ferecepcion);

  @Query("SELECT DISTINCT o.tipo FROM FixmlFile o ORDER BY o.tipo DESC")
  public List<String> findDistinctTipo();

  /**
   * Obtiene los ficheros ZIP donde hay ficheros de fallidos recibidos y validados contra su esquema correctamente.
   * <p>
   * Los tipos de ficheros buscados son:<br>
   * 
   * • CSECLENDING<br>
   * • CSECLENDVALUE<br>
   * • CSECLENDREMUN<br>
   * • CFAILPENALTIES<br>
   * • CSECLENDCANC<br>
   * • CFAILBUYIN<br>
   * • CFAILCASHSETTL<br>
   * 
   * @param tipos Colección de tipos de ficheros buscados.
   * @return <code>java.util.List<String> - </code>Lista de ficheros zip con ficheros de fallidos
   * @author XI316153
   */
  @Deprecated
  @Query("SELECT DISTINCT fixml.zip FROM FixmlFile fixml WHERE fixml.procesado=false AND fixml.tipo IN (:tipo) AND fixml.ferecepcion >= :fedesde AND fixml.ferecepcion < :fehasta ORDER BY fixml.zip ASC")
  public List<String> findByTipoAndNotProcesadoAndFerecepcionOrderByZipAsc(@Param("fedesde") Date fedesde,
                                                                           @Param("fehasta") Date fehasta,
                                                                           @Param("tipo") List<String> tipos);
  
  @Deprecated
  @Query("SELECT DISTINCT fixml.zip FROM FixmlFile fixml WHERE fixml.procesado=false AND fixml.tipo IN (:tipo) AND fixml.ferecepcion >= :fedesde ORDER BY fixml.zip ASC")
  public List<String> findByTipoAndNotProcesadoAndFerecepcionOrderByZipAsc(@Param("fedesde") Date fedesde,
                                                                           @Param("tipo") List<String> tipos);

  @Query("SELECT DISTINCT fixml.zip FROM FixmlFile fixml WHERE fixml.procesado=false AND fixml.tipo IN (:tipo) AND fixml.feproceso >= :fedesde AND fixml.feproceso < :fehasta ORDER BY fixml.zip ASC")
  public List<String> findByTipoAndNotProcesadoAndFeprocesoOrderByZipAsc(@Param("fedesde") Date fedesde,
                                                                           @Param("fehasta") Date fehasta,
                                                                           @Param("tipo") List<String> tipos);
  
  @Query("SELECT DISTINCT fixml.zip FROM FixmlFile fixml WHERE fixml.procesado=false AND fixml.tipo IN (:tipo) AND fixml.feproceso >= :fedesde ORDER BY fixml.zip ASC")
  public List<String> findByTipoAndNotProcesadoAndFeprocesoOrderByZipAsc(@Param("fedesde") Date fedesde,
                                                                           @Param("tipo") List<String> tipos);

  
  @Modifying
  @Query("UPDATE FixmlFile fixml SET fixml.procesado=true WHERE fixml.zip=:zip AND fixml.tipo=:tipo")
  public Integer updateProcesadoByZipAndTipo(@Param("zip") String zip, @Param("tipo") String tipo);

  @Modifying
  // @Query("UPDATE FixmlFile fixml SET fixml.procesado=false WHERE fixml.procesado=true AND fixml.tipo IN (:tipo) AND fixml.ferecepcion >= :fedesde AND fixml.ferecepcion < :fehasta")
  @Query("UPDATE FixmlFile fixml SET fixml.procesado=false WHERE fixml.procesado=true AND fixml.tipo IN (:tipo) AND fixml.ferecepcion >= :fedesde")
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Integer updateProcesadoByFechaRecepcionAndTipo(@Param("fedesde") Date fedesde,
                                                        @Param("tipo") List<String> tipos);

} // FixmlFileDao
