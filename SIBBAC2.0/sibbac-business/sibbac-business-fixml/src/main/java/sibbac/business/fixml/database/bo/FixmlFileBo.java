package sibbac.business.fixml.database.bo;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.fixml.database.dao.FixmlFileDao;
import sibbac.business.fixml.database.model.FixmlFile;
import sibbac.database.bo.AbstractBo;

@Service
public class FixmlFileBo extends AbstractBo<FixmlFile, Long, FixmlFileDao> {

  public List<FixmlFile> findByTipo(final String tipo) {
    return dao.findByTipoOrderByFerecepcionDesc(tipo);
  }

  public List<FixmlFile> findByTipoAndFerecepcion(final String tipo, final Timestamp ferecepcion) {
    return dao.findByTipoAndFerecepcionOrderByFerecepcion(tipo, ferecepcion);
  }

  public List<String> findDistinctTipo() {
    return dao.findDistinctTipo();
  }

} // FixmlFileBo
