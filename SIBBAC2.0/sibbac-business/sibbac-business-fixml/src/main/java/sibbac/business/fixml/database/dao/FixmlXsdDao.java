package sibbac.business.fixml.database.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sibbac.business.fixml.database.model.FixmlXsd;


public interface FixmlXsdDao extends JpaRepository< FixmlXsd, Long > {

	List< FixmlXsd > findByNombreXml( String nombreXml );

}
