package sibbac.business.fixml.database.model;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.envers.Audited;

import sibbac.database.DBConstants;
import sibbac.database.model.ATableAudited;


/**
 * Class that represents a {@link sibbac.database.model.FixmlFile }.
 * Entity: "Factura".
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings( "serial" )
@Table( name = DBConstants.FIXML.FICHEROSXML )
@Entity
@XmlRootElement
@Audited
public class FixmlFile extends ATableAudited< FixmlFile > implements java.io.Serializable {

	// ------------------------------------------ Static Bean properties

	// ------------------------------------------------- Bean properties

	/**
	 * Codigo del documento anterior
	 */
	// tipo de documento con código del sector
	@Column( name = "TIPO", nullable = false, length = 20 )
	@XmlAttribute
	private String		tipo;

	// Fecha que viene en el nombre del fichero
	@Column( name = "FERECEPCION", nullable = false )
	@XmlAttribute
	private Timestamp	ferecepcion;

	// Fecha en el que se procesa el fichero
	@Column( name = "FEPROCESO", nullable = false )
	@XmlAttribute
	private Timestamp	feproceso;

	// nombre del fichero zip que contiene el xml
	@Column( name = "ZIP", nullable = false, length = 255 )
	@XmlAttribute
	private String		zip;

	/** si ha sido procesado */
	@Column( name = "PROCESADO", nullable = false )
	@XmlAttribute
	private boolean		procesado;

	// ----------------------------------------------- Bean Constructors

	public FixmlFile() {
		super();
	}

	// ------------------------------------------- Bean methods: Getters

	public String getTipo() {
		return this.tipo;
	}

	public Timestamp getFerecepcion() {
		return this.ferecepcion;
	}

	public Timestamp getFeproceso() {
		return this.feproceso;
	}

	public String getZip() {
		return this.zip;
	}

	public boolean isProcesado() {
		return procesado;
	}

	// ------------------------------------------- Bean methods: Setters

	public void setTipo( String _tipo ) {
		this.tipo = _tipo;
	}

	public void setFerecepcion( Timestamp _ferecepcion ) {
		this.ferecepcion = _ferecepcion;
	}

	public void setFeproceso( Timestamp _feproceso ) {
		this.feproceso = _feproceso;
	}

	public void setZip( String _zip ) {
		this.zip = _zip;
	}

	public void setProcesado( boolean procesado ) {
		this.procesado = procesado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[FICHERO==" + this.id + "]";
	}

}
