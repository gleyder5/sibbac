package sibbac.business.fixml.database.bo;


import java.util.List;

import org.springframework.stereotype.Service;

import sibbac.business.fixml.database.dao.FixmlXsdDao;
import sibbac.business.fixml.database.model.FixmlXsd;
import sibbac.database.bo.AbstractBo;


@Service
public class FixmlXsdBo extends AbstractBo< FixmlXsd, Long, FixmlXsdDao > {

	public List< FixmlXsd > findByNombreXml( final String nombrexml ) {
		return this.dao.findByNombreXml( nombrexml );
	}
}
