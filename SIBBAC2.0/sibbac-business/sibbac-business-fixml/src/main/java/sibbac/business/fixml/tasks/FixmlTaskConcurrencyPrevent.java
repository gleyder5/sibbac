package sibbac.business.fixml.tasks;

import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TMCT0MSC;
import sibbac.business.wrappers.database.bo.Tmct0menBo;
import sibbac.business.wrappers.database.model.Tmct0men;
import sibbac.common.SIBBACBusinessException;
import sibbac.tasks.SIBBACTask;

public abstract class FixmlTaskConcurrencyPrevent extends SIBBACTask {
  
  
  
  // para saber que tipo de tarea se está ejecutando y poder detectar su
  // estado
  private TIPO_APUNTE tipoApunte;
  /**
   * Para cambiar el estado de la tarea.
   */
  @Autowired
  private Tmct0menBo menBo;

  public abstract void executeTask() throws Exception;

  public abstract TIPO_APUNTE determinarTipoApunte();

  @Override
  public void execute() {
    String prefix = "[" + this.getClass().getSimpleName() + "::execute] ";

    // Ejecucion de la tarea en si
    LOG.trace(prefix + ">>> LAUNCHING TASK EXECUTION.");
    // Para asegurarse de que se inicializa el tipo de apunte y el
    // nombre de la clase (para las trazas)
    tipoApunte = determinarTipoApunte();
    // Hay tareas que controlan si se están ejecutando independientemente,
    // por eso se pone if, si tipoApunte no es nulo se controla desde aquí.
    if (tipoApunte != null) {
      Tmct0men tmct0men = null;
      TMCT0MSC estadoIni = TMCT0MSC.LISTO_GENERAR;
      TMCT0MSC estadoExe = TMCT0MSC.EN_EJECUCION;
      TMCT0MSC estadoError = TMCT0MSC.EN_ERROR;
      try {
        LOG.debug(prefix + " :: execute ==> Detectando si se está ejecutando la tarea ....");
        tmct0men = menBo.putEstadoMEN(tipoApunte, estadoIni, estadoExe);
        try {
          this.executeTask();
          menBo.putEstadoMEN(tmct0men, estadoIni);
          LOG.debug(prefix + " :: execute Se acabó la tarea y se dejó en estado disponible");
        }
        catch (Exception ex) {
          menBo.putEstadoMEN(tmct0men, estadoError);
          LOG.info(prefix + " Error ejecutando la tarea. " + ex.getMessage());
        }
      }
      catch (SIBBACBusinessException ex) {
        LOG.info(ex.getMessage());
      }
      catch (Exception ex) {
        LOG.error(ex.getMessage() + " cause: " + ex.getCause(), ex);
      }
    }
    else {
      try {
        this.executeTask();
      }
      catch (Exception e) {
        LOG.error(e.getMessage(), e);
      }
    }

  }

}
