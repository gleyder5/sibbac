package sibbac.business.fixml.tasks;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.mail.MessagingException;

import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.business.fase0.database.bo.Tmct0cfgBo;
import sibbac.business.fase0.database.dao.EstadosEnumerados.TIPO_APUNTE;
import sibbac.business.fase0.database.model.Tmct0cfg;
import sibbac.common.utils.EnviaMailPUI;
import sibbac.common.utils.SibbacEnums.Tmct0cfgConfig;
import sibbac.tasks.Task;
import sibbac.tasks.annotations.SIBBACJob;

/**
 * Porcesa los FIXML recibidos de BME.
 * <p>
 * Todo este proceso es un auténtico desproposito, en algún momento habrá que meterle mano. Sólo se ha modificado lo
 * necesario para que funcionen los cambios que ha introducido BME.
 * 
 * @author XI316153
 */
@SIBBACJob(group = Task.GROUP_FIXML.NAME,
           name = Task.GROUP_FIXML.JOB_FIXML_INPUT,
           interval = 5,
           delay = 0,
           intervalUnit = IntervalUnit.MINUTE)
public class TaskFIXMLInput extends FixmlTaskConcurrencyPrevent {

  protected static final Logger LOG = LoggerFactory.getLogger(TaskFIXMLInput.class);

  @Value("${entorno.ficheros.bme:PROD}")
  private String entorno;
  @Value("${pattern.ficheros.bme:*_BMCLC0_V025_*.ZIP}")
  private String patternFicheroBme;

  /** Business object para la tabla <code>Tmct0cfg</code>. */
  @Autowired
  private Tmct0cfgBo tmct0cfgBo;

  private static final String TMCT0CFG_APPLICATION = "SBRMV";

  // private static String NOMBRE_FICHERO_ZIP = "PROD_BMCL_V025_CCPFILES_";
  // private String NOMBRE_FICHERO_ZIP = entorno + "_BMCL_V025_";
  private String NOMBRE_FICHERO_ZIP;

  // private String FIXML_FILES_MASK = NOMBRE_FICHERO_ZIP + "*.ZIP";
  private String FIXML_FILES_MASK;

  @Value("${sibbac.fixml.inputdir:/temp}")
  private String dirPath;
  @Value("${aviso.pui.fichero}")
  private String puiFichero;
  @Value("${aviso.pui.length}")
  private long puiLength;
  @Autowired
  EnviaMailPUI enviaMailPUI;
  private String procesados = "tratados";
  private String temporal = "temporal";
  private String erroneos = "erroneos";
  private String unzip = "unzip";

  public TaskFIXMLInput() {
  }

  @Override
  public void executeTask() {

    String pfx = "[TaskFIXMLInput::execute] ";

    /*
     * Proceso de lectura de los ficheros GROUP_FIXML 1.- recuperariamos los valores de configuracion para este proceso
     * - directorio de entrada de los ficheros - directorio de backup (por fecha del dia) 2.- Por cada fichero zip que
     * exista en el directorio - descomprimimos el fichero ZIP - creamos una subtarea en la que se incluye el nombre del
     * proceso a lanzar y el fichero recibido - copiamos al directorio de backup el fichero con extension timestamp y
     * extension de backup
     */

    LOG.debug(pfx + "Starting execution...");

    try {
      executeFIXMLInput(pfx);
      // LOG.debug(pfx + " tarea ejecutada correctamente.");

    } catch (Exception e) {
      LOG.error(e.getMessage(), e);
      throw e;
    }

  }

  private void executeFIXMLInput(String pfx) {
    JobDataMap jobDataMap = new JobDataMap();

    // Lectura del directorio de entrada de GROUP_FIXML
    Path directoryPath = Paths.get(dirPath);
    LOG.debug(pfx + "Exploring files in path: [{}]...", directoryPath);

    // NOMBRE_FICHERO_ZIP = entorno + "_BMCL_V025_";
    FIXML_FILES_MASK = entorno + patternFicheroBme;

    if (Files.isDirectory(directoryPath)) {
      this.crearEstructuraDirectorio();
      // String fechaHoy;
      Calendar fhnow = Calendar.getInstance();
      Timestamp tsnow = new Timestamp(new java.util.Date().getTime());
      fhnow.setTime(tsnow);
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSS");
      java.util.Date fechaTemp = new java.util.Date();
      // sdf = new SimpleDateFormat( "yyyyMMdd" );
      // fechaHoy = sdf.format( fhnow.getTime() );
      //
      // String newDir = dirPath + File.separator + fechaHoy;
      // File file = new File( newDir );
      // boolean dirCreated = file.mkdirs();
      // if ( dirCreated )
      // LOG.debug( "[FIXMLInput::execute] creating subdirectory " +
      // fechaHoy );
      LOG.debug(pfx + "Explorando archivos de tipo: [{}] en la carpeta: [{}]...", FIXML_FILES_MASK, directoryPath);

      try (DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath, FIXML_FILES_MASK)) {

        File xmlFile = null;
        String procesadosDir = directoryPath + File.separator + procesados;
        String erroneosDir = directoryPath + File.separator + erroneos;
        String temporalDir = directoryPath + File.separator + temporal;
        String unzipDir = directoryPath + File.separator + unzip;
        String nombreFicheroXMLTemporal = null;

        int numeroficheros = 0;
        for (Path zipFilePath : stream) {
          numeroficheros++;
          LOG.debug(pfx + "+ Procesando [{}]", zipFilePath);
          // LOG.debug( path );
          // LOG.debug("PATH: "+path);
          // LOG.debug("PATH filename O : "+path.getFileName().toString());
          // LOG.debug("PATH filename N : "+getNombreFichero(path.getFileName().toString()));

          descomprimirFicheroZIP(zipFilePath.toFile());
          String extensionFichero = getTimestampFichero(zipFilePath.getFileName().toString());

          if (!("error".equals(extensionFichero))) {
            LOG.debug(pfx + "  Extension [{}] valida", extensionFichero);
            fechaTemp = sdf.parse(extensionFichero);
            Path temporalPath = Paths.get(unzipDir);
            try (DirectoryStream<Path> streamXML = Files.newDirectoryStream(temporalPath, "*.XML");) {
              LOG.debug(pfx + "  Buscando archivos XML en la ruta: [{}]...", temporalPath);

              for (Path xmlFilePath : streamXML) {
                numeroficheros++;
                LOG.debug(pfx + "  Procesando archivo XML: [{}] ", xmlFilePath);
                // LOG.debug("PATH0: "+path0);
                // LOG.debug("PATH0 filename O : "+path0.getFileName().toString());
                jobDataMap = new JobDataMap();
                jobDataMap.put("name", getNombreFichero(xmlFilePath.getFileName().toString()));
                jobDataMap.put("fproceso", tsnow);

                // jobDataMap.put( "frecepcion",
                // java.sql.Timestamp.valueOf(convertToSDF(extensionFichero)));
                // jobDataMap.put( "fproceso", new
                // Timestamp(fechaTemp.getTime()));
                // jobDataMap.put("frecepcion", java.sql.Timestamp.valueOf(convertToSDF(extensionFichero)));
                jobDataMap.put("frecepcion", new java.sql.Timestamp(fechaTemp.getTime()));
                jobDataMap.put("ficherozip", (procesadosDir + File.separator + zipFilePath.getFileName().toString()));
                jobDataMap.put("fichero", (xmlFilePath.getFileName().toString() + "." + extensionFichero));
                nombreFicheroXMLTemporal = temporalDir + File.separator + (xmlFilePath.getFileName().toString()) + "."
                                           + extensionFichero;
                xmlFile = xmlFilePath.toFile();

                // jobDataMap.put( "frecepcion",
                // java.sql.Timestamp.valueOf(convertToSDF(extensionFichero)));
                jobDataMap.put("frecepcion", new Timestamp(fechaTemp.getTime()));
                jobDataMap.put("ficherozip", (procesadosDir + File.separator + zipFilePath.getFileName().toString()));
                jobDataMap.put("fichero", (xmlFilePath.getFileName().toString() + "." + extensionFichero));
                nombreFicheroXMLTemporal = temporalDir + File.separator + (xmlFilePath.getFileName().toString()) + "."
                                           + extensionFichero;
                xmlFile = xmlFilePath.toFile();

                LOG.debug(pfx + "  Renombrando archivo XML: [{}] a: [{}]", xmlFilePath, nombreFicheroXMLTemporal);
                boolean success = xmlFile.renameTo(new File(nombreFicheroXMLTemporal));
                if (success) {
                  LOG.debug(pfx + "  xml file renamed to " + nombreFicheroXMLTemporal);
                  // LOG.debug( "fichero renombrado a : " +
                  // nombreFichero );
                  jobDataMap.put("ficheroPath", nombreFicheroXMLTemporal);
                  String proceso = "FIXMLFichero" + numeroficheros;
                  LOG.debug(pfx + "  Lanzando subtask [{}]...", proceso);
                  this.launchDelegatedTask(proceso, TaskFIXMLFichero.class, jobDataMap);
                } else {
                  LOG.debug(pfx + "  xml file not renamed to " + nombreFicheroXMLTemporal);
                  // LOG.debug( "fichero no renombrado a : " +
                  // nombreFichero );
                  // jobDataMap.put( "ficheroPath", ( temporalDir
                  // + File.separator + (
                  // path0.getFileName().toString() ) ) );
                }

              }
            }

            xmlFile = zipFilePath.toFile();
            nombreFicheroXMLTemporal = procesadosDir;
          } else {
            xmlFile = zipFilePath.toFile();
            nombreFicheroXMLTemporal = erroneosDir;
            LOG.debug(pfx + "  Extension [{}] NO VALIDA. [nombreFichero=={}]", extensionFichero,
                      nombreFicheroXMLTemporal);
          }
          nombreFicheroXMLTemporal = nombreFicheroXMLTemporal + File.separator + (zipFilePath.getFileName().toString());
          LOG.debug(pfx + "  Renombrando archivo: [file=={}] a: [nombreFichero=={}]", xmlFile, nombreFicheroXMLTemporal);
          boolean success = xmlFile.renameTo(new File(nombreFicheroXMLTemporal));
          LOG.debug(pfx + "  [rename=={}]", success);
          if (success) {
            LOG.debug(pfx + "zip file moved to " + nombreFicheroXMLTemporal);
          } else {
            LOG.debug(pfx + "zip file NOT moved to " + nombreFicheroXMLTemporal);
          }

        }
        if (numeroficheros == 0) {
          LOG.debug(pfx + "No zip files to process ");
        }
      } catch (Exception e) {
        LOG.error(pfx + "ERROR({}): {}", e.getClass().getName(), e.getMessage());
      }
    } else {
      LOG.error(pfx + "ERROR: El directorio de trabajo ($sibbac.fixml.inputdir) no existe: [{}]", dirPath);
    }
  }

  public void descomprimirFicheroZIP(File zipFile) throws IllegalArgumentException, MessagingException {
    String pfx = "[TaskFIXMLInput::descomprimirFicheroZIP] ";
    String outputFolder = dirPath + File.separator + unzip;
    LOG.debug(pfx + "Descomprimiendo el archivo ZIP: [{}] en la carpeta: [{}]", zipFile, outputFolder);
    byte[] buffer = new byte[1024];
    try {
      FileOutputStream fos;
      String fileName;
      ZipEntry ze;
      File file;
      File folder = new File(outputFolder);
      ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
      List<InputStream> path2attach = new ArrayList<InputStream>();
      List<String> nameDest = new ArrayList<String>();
      folder.mkdir();
      while ((ze = zis.getNextEntry()) != null) {
        fos = new FileOutputStream(file = new File(folder, fileName = ze.getName()));
        int len;
        while ((len = zis.read(buffer)) > 0) {
          fos.write(buffer, 0, len);
        }
        if (fileName.equalsIgnoreCase(puiFichero) && file.length() > puiLength) {
          path2attach.add(new FileInputStream(file));
          nameDest.add(fileName);
        }
        fos.close();
      }
      if (!nameDest.isEmpty()) {
        try {
          enviaMailPUI.send(path2attach, nameDest);
        } catch (Exception e) {
          LOG.warn(pfx + "Incidencia enviando mail ... " + e.getMessage());
        }

        // MFG 15/09/2016 Si se ha enviado el mail, paso anterior, y se cumple una determinada hora, se tiene que pasar
        // el ultimo fichero de megara tratado
        // Para ello se copia el ultimo fichero del dia actual de //ficheros/megara/tratados a la carpeta
        // //ficheros/megara/in quitandole la fecha y la hora.
        this.moverFicheroTratadoMegara();

      } else {
        LOG.debug(pfx + "No se realiza el envio del mail, no se intenta mover el ultimo fichero Respuesta Megara ");
      }
      zis.closeEntry();
      zis.close();
    } catch (IOException ex) {
      LOG.error(pfx + "Error descomprimiendo fichero({})] : {}", zipFile.getName().toString(), ex.getMessage());
    }
  }

  private String getNombreFichero(String fichero) {
    int longitud = fichero.length() - fichero.lastIndexOf(".");
    if (fichero != null && fichero.length() > longitud) {
      return fichero.substring(0, (fichero.length() - longitud));
    } else {
      return "";
    }
  }

  private String convertToSDF(String fichero) {
    String temp = fichero.substring(0, 4) + "-" + fichero.substring(4, 6) + "-" + fichero.substring(6, 8) + " "
                  + fichero.substring(8, 10) + ":" + fichero.substring(10, 12) + ":" + fichero.substring(12, 14);
    return temp;
  }

  private void crearEstructuraDirectorio() {
    String pfx = "[TaskFIXMLInput::crearEstructuraDirectorio] ";
    Path directoryPath = Paths.get(dirPath);

    String procesadorDir = directoryPath + File.separator + procesados;
    String erroneosDir = directoryPath + File.separator + erroneos;
    String temporalDir = directoryPath + File.separator + temporal;
    String unzipDir = directoryPath + File.separator + unzip;

    File file = new File(procesadorDir);
    if (file.mkdirs()) {
      LOG.debug(pfx + "+ [procesadorDir=={}]", procesadorDir);
    }

    file = new File(erroneosDir);
    if (file.mkdirs()) {
      LOG.debug(pfx + "+ [erroneosDir=={}]", erroneosDir);
    }

    file = new File(temporalDir);
    if (file.mkdirs()) {
      LOG.debug(pfx + "+ [temporalDir=={}]", temporalDir);
    }

    file = new File(unzipDir);
    if (file.mkdirs()) {
      LOG.debug(pfx + "+ [unzipDir=={}]", unzipDir);
    }

  }

  // public static String getTimestampFichero(String fichero) {
  // String pfx = "[TaskFIXMLInput::getTimestampFichero] ";
  // String temporal = "error";
  // try {
  // int posicion = fichero.toUpperCase().indexOf(NOMBRE_FICHERO_ZIP);
  // if (posicion < 0) {
  // LOG.error(pfx + "Wrong file name: {}", fichero);
  // temporal = "error";
  // } else {
  // posicion = fichero.lastIndexOf(".");
  // temporal = fichero.substring(NOMBRE_FICHERO_ZIP.length() - 1, posicion);
  // temporal = temporal.substring(1, temporal.lastIndexOf("_")) + temporal.substring(temporal.lastIndexOf("_") + 1,
  // temporal.length());
  // }
  // } catch (Exception e) {
  // LOG.error(pfx + "Error nombre fichero({})] : {}", fichero, e.getMessage());
  // }
  // LOG.info(pfx + "Temporal: " + temporal);
  // return temporal;
  // }
  public static String getTimestampFichero(String fichero) {
    LOG.debug("[TaskFIXMLInput::getFileTimestamp] Inicio ...");
    String fechaFichero;

    try {
      String[] fileSplit = fichero.split("_");
      fechaFichero = fileSplit[4].concat(fileSplit[5].split("\\.")[0]);
    } catch (Exception e) {
      LOG.warn("[TaskFIXMLInput::getFileTimestamp] Error obteniendo el timestamp del fichero ... ", e);
      fechaFichero = "error";
    } // catch

    LOG.debug("[TaskFIXMLInput::getFileTimestamp] Timestamp obtenido: " + fechaFichero);
    LOG.debug("[TaskFIXMLInput::getFileTimestamp] Fin ...");
    return fechaFichero;
  }

  private void moverFicheroTratadoMegara() {

    String pfx = "[TaskFIXMLInput::moverFicheroTratadoMegara] ";

    LOG.debug(pfx + " Inicio funcionalidad mover el fichero de megara.");

    // Se obtiene la hora a partir de la cual se puede ejecutar.

    // Inicializaciones
    int iHoraInicio = -1;
    Tmct0cfg datosConfiguracion = new Tmct0cfg();
    Calendar fechaActual = new GregorianCalendar();
    String sRutaIn = null;
    String sRutaTratados = null;
    Integer horaActual = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

    int iPocionInicioFecha = 0;

    SimpleDateFormat formateador = new SimpleDateFormat("ddMMyyyy", new Locale("es_ES"));
    String sFiltro = "_" + formateador.format(new Date()) + "_";

    // se obtiene la hora de inicio a partir de la cual se tiene que mover el fichero
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION,
                                                                         Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_start_post_fisxml.getProcess(),
                                                                         Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_start_post_fisxml.getKeyname());
    if (datosConfiguracion == null) {
      LOG.error(pfx
                + " No se ha definido en la tabla TMCT0CFG el valor de la hora de inicio a partir de la cual se tiene que mover el fichero megara para la aplicacion: "
                + TMCT0CFG_APPLICATION + " Proceso "
                + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_start_post_fisxml.getProcess() + " Clave "
                + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_start_post_fisxml.getKeyname());
    } else {
      iHoraInicio = Integer.valueOf(datosConfiguracion.getKeyvalue());
    }

    // se obtiene la ruta de entrada del proceso de megara
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION,
                                                                         Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input.getProcess(),
                                                                         Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input.getKeyname());
    Path pDirectorioIn = null;
    if (datosConfiguracion == null) {
      LOG.error(pfx
                + " No se ha definido en la tabla TMCT0CFG el valor de la ruta de ficheros de entrada para megara para la aplicacion: "
                + TMCT0CFG_APPLICATION + " Proceso " + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input.getProcess()
                + " Clave " + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input.getKeyname());
    } else {
      sRutaIn = datosConfiguracion.getKeyvalue();

      pDirectorioIn = Paths.get(sRutaIn);

      if (!Files.isDirectory(pDirectorioIn)) {
        LOG.error(pfx
                  + " Es erronea la ruta de ficheros in obtenida de la tabla TMCT0CFG el valor de la ruta de ficheros tratados para la aplicacion: "
                  + TMCT0CFG_APPLICATION + " Proceso "
                  + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input.getProcess() + " Clave "
                  + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input.getKeyname());

        sRutaIn = null;
      }

    }

    // se obtiene la ruta de tratados del proceso de megara
    datosConfiguracion = tmct0cfgBo.findByAplicationAndProcessAndKeyname(TMCT0CFG_APPLICATION,
                                                                         Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getProcess(),
                                                                         Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getKeyname());

    Path pDirectorioTratados = null;
    if (datosConfiguracion == null) {
      LOG.error(pfx
                + " No se ha definido en la tabla TMCT0CFG el valor de la ruta de ficheros tratados para la aplicacion: "
                + TMCT0CFG_APPLICATION + " Proceso "
                + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getProcess() + " Clave "
                + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getKeyname());
    } else {
      sRutaTratados = datosConfiguracion.getKeyvalue().trim();

      pDirectorioTratados = Paths.get(sRutaTratados);

      if (!Files.isDirectory(pDirectorioTratados)) {
        LOG.error(pfx
                  + " Es erronea la ruta de ficheros tratados obtenida de la tabla TMCT0CFG el valor de la ruta de ficheros tratados para la aplicacion: "
                  + TMCT0CFG_APPLICATION + " Proceso "
                  + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getProcess() + " Clave "
                  + Tmct0cfgConfig.MEGARA_PARTENON_FOLDER_megara_input_tratados.getKeyname());

        sRutaTratados = null;
      }
    }

    // Comprobacion que tenemos todos los datos necesario para que funcione.
    if ((sRutaIn != null) && (sRutaTratados != null) && (iHoraInicio != -1)) {

      if (horaActual >= iHoraInicio) {

        String sMascara = "*" + sFiltro + "*.txt"; // Le añado la extension para quedarme solo con los de datos
        LOG.debug(pfx + " Busco los ficheros en " + pDirectorioTratados.getName(0) + " que contengan en el nombre  "
                  + sMascara);

        try {
          TreeMap<String, String> tmap = new TreeMap<String, String>();
          String sClave = null; // Contiene la fecha del fichero y la fecha de ultima modifcacion
          String sFichero = null; // Se obtiene el nombre del fichero.
          try (DirectoryStream<Path> stream = Files.newDirectoryStream(pDirectorioTratados, sMascara);) {

            for (Path zipFilePath : stream) {
              LOG.debug(pfx + "+ Encontrado Fichero  [{}]", zipFilePath);
              sFichero = zipFilePath.getFileName().toString();
              iPocionInicioFecha = sFichero.lastIndexOf(sFiltro);
              // Se utiliza como clave la fecha y la hora que añade el proceso de megara pero como puede procesar varios
              // al mismo tiempo,
              // se le añade el long de la fecha de ultima modifcacion para la ordenacion.
              sClave = sFichero.substring(iPocionInicioFecha) + "_" + zipFilePath.toFile().lastModified();
              tmap.put(sClave, sFichero);
            }
          }
          // Se obtiene el nombre del fichero a mover
          String sFicheroMover = null;

          switch (tmap.size()) {
            case 0:
              LOG.debug(pfx + "---- No se ha encontrado ningun fichero megara del dia actual:  ");
              break;
            case 1:
              LOG.debug(pfx + " ---- Se mueve el fichero encontrado: ");
              sFicheroMover = tmap.firstEntry().getValue();
              break;
            default: // Se han encontrado mas de uno en ese caso hay que determinar cual es el mas actual que será el
                     // ultimo porque los ordena de forma ascendente
              LOG.debug(pfx
                        + " -- Se han encontrado mas de un fichero del dia actual. -> Se mueve el fichero mas reciente encontrado "
                        + tmap.lastEntry().getValue());

              sFicheroMover = tmap.lastEntry().getValue();
              break;
          }

          if (sFicheroMover != null) { // Se mueve el fichero

            String sFicheroOrigen = sRutaTratados + File.separator + sFicheroMover;
            Path pOrigen = Paths.get(sFicheroOrigen);
            iPocionInicioFecha = sFicheroMover.lastIndexOf(sFiltro);

            // Se obtiene la fecha del nombre y el nombre del fichero de datos sin la fecha
            String sFecha = sFicheroMover.substring(iPocionInicioFecha).replace(".txt", "");
            String sNombreSinFecha = sFicheroMover.substring(0, iPocionInicioFecha);
            // Se forma el fichero ok con la misma fecha para comprobar su existencia.
            String sFicheroOkconFecha = sNombreSinFecha + ".ok" + sFecha;
            String sFicheroOksinFecha = sNombreSinFecha + ".ok";

            // Fichero ok con ruta para comprobar su existencia
            String sFicheroOkAsociado = sRutaTratados + File.separator + sFicheroOkconFecha;
            Path pOrigenOk = Paths.get(sFicheroOkAsociado);
            if (Files.exists(pOrigenOk)) {
              String sFicheroDestino = sRutaIn + File.separator + sNombreSinFecha;

              Path pDestino = Paths.get(sFicheroDestino);
              // Se mueve el fichero de datos.
              Files.move(pOrigen, pDestino, REPLACE_EXISTING);

              // se mueve el fichero ok
              String sFicheroOkDestino = sRutaIn + File.separator + sFicheroOksinFecha;
              pDestino = Paths.get(sFicheroOkDestino);
              Files.move(pOrigenOk, pDestino, REPLACE_EXISTING);

            } else {
              LOG.error(pfx
                        + "ERROR({}):  {} No se mueve el fichero megara porque el ultinmo fichero movido no tiene el fichero.ok correspondiente: No encontrado Fichero: "
                        + sFicheroOkAsociado);
            }

          }

        } catch (Exception e) {
          LOG.error(pfx + "ERROR({}): {}", e.getClass().getName(), e.getMessage());
        }

      } else {
        LOG.debug(pfx + " La hora actual no cumple el criterio para mover el fichero.");
      }

    }

  }

  @Override
  public TIPO_APUNTE determinarTipoApunte() {
    return TIPO_APUNTE.FIXML_INPUT;
  }
}
