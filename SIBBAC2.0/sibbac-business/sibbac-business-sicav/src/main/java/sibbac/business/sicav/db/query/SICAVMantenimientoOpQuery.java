package sibbac.business.sicav.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DateDynamicFilter;
import sibbac.database.DynamicColumn;
import sibbac.database.DynamicColumn.ColumnType;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.SimpleDynamicFilter.SimpleDynamicFilterComparatorType;
import sibbac.database.StringDynamicFilter;

/**
 * The Class SICAVMantenimientoOpQuery.
 */
@Component("SICAVMantenimientoOpQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SICAVMantenimientoOpQuery extends AbstractDynamicPageQuery {

  private static final String CDDCLAVE = "CDDCLAVE";
  private static final String TIPO_DE_OPERACION = "Tipo de Operación";
  private static final String TICOVE = "TICOVE";
  private static final String ISIN = "ISIN";
  private static final String ID_OPE = "ID_OPE";
  private static final String N_OPERACION = "Nº Operación";
  private static final String IDENTIFICADOR_DEL_TITULAR = "Identificador del Titular";
  private static final String NBTITREV = "NBTITREV";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT " + "Q1.ID," + "Q1.ID_OPE," + "Q1.ISIN," + "Q1.TINTIT," + "Q1.TICOVE,"
      + "Q1.OPPREU," + "Q1.OPEFEC," + "Q1.NBTITREV," + "Q1.NOMINAL," + "Q1.CDORDTIT," + "Q1.CDORDTITDES,"
      + "Q1.CDCUSTOD," + "Q1.CDCUSTODDES," + "Q1.DOMICILIO," + "Q1.TPTIPREP," + "Q1.DESTIT," + "Q1.TPIDENTI,"
      + "Q1.CDCLAVE," + "Q1.FECHA_OPE," + "Q1.OPTITU," + "Q1.OPPCPR," + "Q1.OPIMPC," + "Q1.INDCOM," + "Q1.FECHA_COB,"
      + "Q1.CDDCLAVE," + "Q1.PCPARTI," + "Q1.INDHAC";

  /** The Constant FROM. */
  private static final String FROM = " FROM ( "
      + " SELECT   "
      + "   OPR.ID AS ID,   OPR.ID_OPE AS ID_OPE,"
      + "   OPR.ISIN AS ISIN,   OPR.TINTIT AS TINTIT,   OPR.TICOVE AS TICOVE,   OPR.OPPREU AS OPPREU,"
      + "   OPR.OPEFEC AS OPEFEC,   MAE.NBTITREV AS NBTITREV,   MAE.NUNOMINV AS NOMINAL," 
      + "   TRIM(TIT.CDCLEARE) AS CDORDTIT,   TRIM(TIT.DSCLEARE) AS CDORDTITDES," 
      + "   TRIM(CUS.CDCLEARE) AS CDCUSTOD,   TRIM(CUS.DSCLEARE) AS CDCUSTODDES,"
      + "   FIS.TPDOMICI || ' ' || FIS.NBDOMICI || ' ' || FIS.NUDOMICI || ' ' || FIS.NBCIUDAD || ' ' || FIS.CDDEPAIS AS DOMICILIO,"
      + "   OPR.TPTIPREP AS TPTIPREP,   FIS.NBCLIENT || ' ' || FIS.NBCLIEN1 || ' ' || FIS.NBCLIEN2 AS DESTIT,"
      + "   FIS.TPIDENTI AS TPIDENTI,   FIS.CDDCLAVE AS CDCLAVE,   OPR.FECHA_OPE AS FECHA_OPE," 
      + "   OPR.OPTITU AS OPTITU,   OPR.OPPCPR AS OPPCPR,   OPR.OPIMPC AS OPIMPC," 
      + "   OPR.INDCOM AS INDCOM,   OPR.FECHA_COB AS FECHA_COB,   OPR.CDDCLAVE AS CDDCLAVE," 
      + "   OPR.PCPARTI AS PCPARTI,   OPR.INDHAC AS INDHAC,   OPR.FECHA_BAJA " 
      + " FROM   " 
      + "   TMCT0OPR AS OPR" 
      + "   INNER JOIN TMCT0FIS AS FIS ON   OPR.CDDCLAVE = FIS.CDDCLAVE   AND OPR.CDDCLAVE = FIS.CDDCLAVE" 
      + "   INNER JOIN (SELECT CDISINVV, MAX(FHINICIV) AS FHINICIV FROM TMCT0_MAESTRO_VALORES GROUP BY CDISINVV) AS MAE_MAX ON OPR.ISIN = MAE_MAX.CDISINVV " 
      + "   INNER JOIN TMCT0_MAESTRO_VALORES AS MAE ON OPR.ISIN = MAE.CDISINVV AND MAE.FHBAJACV >= CURRENT DATE AND MAE.FHINICIV = MAE_MAX.FHINICIV" 
      + "   LEFT OUTER JOIN TMCT0CLE AS TIT ON TIT.CDCLEARE = OPR.CDORDTIT AND TIT.FHFINAL >= TO_CHAR(CURRENT DATE, 'yyyymmdd') AND TIT.NUMSEC > 1" 
      + "   LEFT OUTER JOIN TMCT0CLE AS CUS ON CUS.CDCLEARE = OPR.CDCUSTOD AND CUS.FHFINAL >= TO_CHAR(CURRENT DATE, 'yyyymmdd') AND CUS.NUMSEC > 1 "
      + " ORDER BY   " 
      + "  OPR.ID_OPE"
      + " ) AS Q1 ";

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE Q1.FECHA_BAJA IS NULL ";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn(N_OPERACION, ID_OPE),
      new DynamicColumn(ISIN, ISIN), new DynamicColumn("Descripción del valor ", NBTITREV),
      new DynamicColumn("Nominal", "NOMINAL"), new DynamicColumn("Fecha Operación", "FECHA_OPE", ColumnType.DATE),
      new DynamicColumn(TIPO_DE_OPERACION, TICOVE), new DynamicColumn("Títulos", "OPTITU"),
      new DynamicColumn("Precio", "OPPREU"), new DynamicColumn("% Precio", "OPPCPR"),
      new DynamicColumn("Efectivo", "OPEFEC"), new DynamicColumn("Comisión", "OPIMPC"),
      new DynamicColumn("Cobrando comisión", "INDCOM"),
      new DynamicColumn("Fecha de Cobro", "FECHA_COB", ColumnType.DATE),
      new DynamicColumn(IDENTIFICADOR_DEL_TITULAR, CDDCLAVE),
      new DynamicColumn("Nombre y Apellidos del Titular", "DESTIT"), new DynamicColumn("Domicilio", "DOMICILIO"),
      new DynamicColumn("Tipo de Titular", "TPTIPREP"), new DynamicColumn("% de Participación", "PCPARTI"),
      new DynamicColumn("Ordenante", "CDORDTITDES"), new DynamicColumn("Custodio del titular", "CDCUSTODDES"),
      new DynamicColumn("Indicador de Hacienda", "INDHAC") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;

    res = new ArrayList<>();
    res.add(new StringDynamicFilter(ID_OPE, N_OPERACION));
    res.add(new StringDynamicFilter(ISIN, ISIN));
    res.add(new StringDynamicFilter(NBTITREV, "Descripción ISIN"));
    res.add(new StringDynamicFilter(CDDCLAVE, IDENTIFICADOR_DEL_TITULAR));
    res.add(new StringDynamicFilter(TICOVE, TIPO_DE_OPERACION));
    DateDynamicFilter dateDynamicFilter = new DateDynamicFilter("FECHA_OPE", "Fecha desde");
    dateDynamicFilter.setComparator(SimpleDynamicFilterComparatorType.BETWEEN);
    res.add(dateDynamicFilter);
    res.add(new DateDynamicFilter("FECHA_OPE2", "Fecha hasta"));
    return res;
  }

  public SICAVMantenimientoOpQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public SICAVMantenimientoOpQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public SICAVMantenimientoOpQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}