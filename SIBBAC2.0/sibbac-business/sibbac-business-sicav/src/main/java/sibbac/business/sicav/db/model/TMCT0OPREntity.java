
package sibbac.business.sicav.db.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import sibbac.database.DBConstants;

/**
 * The Class TMCT0OPREntity.
 */
@Entity
@Table(name = DBConstants.SICAV.TMCT0OPR)
public class TMCT0OPREntity {

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TMCT0OPR_ID_SEQ")
  @SequenceGenerator(name = "TMCT0OPR_ID_SEQ", sequenceName = "TMCT0OPR_ID_SEQ")
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  /** The idope. */
  @Column(name = "ID_OPE", nullable = false, length = 10)
  private Integer idope;

  /** The tintit. */
  @Column(name = "TINTIT", nullable = false, length = 10)
  private Integer tintit;

  /** The ticove. */
  @Column(name = "TICOVE", nullable = false, length = 1)
  private String ticove;

  /** The isin. */
  @Column(name = "ISIN", nullable = false, length = 12)
  private String isin;

  /** The fechaope. */
  @Column(name = "FECHA_OPE", nullable = false, length = 10)
  private Date fechaope;

  /** The optitu. */
  @Column(name = "OPTITU", nullable = false, length = 10)
  private Integer optitu;

  /** The oppreu. */
  @Column(name = "OPPREU", nullable = true, length = 18)
  private BigDecimal oppreu;

  /** The oppcpr. */
  @Column(name = "OPPCPR", nullable = true, length = 5)
  private BigDecimal oppcpr;

  /** The opefec. */
  @Column(name = "OPEFEC", nullable = false, length = 18)
  private BigDecimal opefec;

  /** The opimpc. */
  @Column(name = "OPIMPC", nullable = false, length = 18)
  private BigDecimal opimpc;

  /** The indcom. */
  @Column(name = "INDCOM", nullable = false, length = 1)
  private String indcom;

  /** The fechacob. */
  @Column(name = "FECHA_COB", nullable = true, length = 10)
  private Date fechacob;

  /** The cddclave. */
  @Column(name = "CDDCLAVE", nullable = false, length = 20)
  private String cddclave;

  /** The tpidenti. */
  @Column(name = "TPIDENTI", nullable = false, length = 20)
  private String tpidenti;

  /** The pcparti. */
  @Column(name = "PCPARTI", nullable = true, length = 5)
  private BigDecimal pcparti;

  /** The cdordtit. */
  @Column(name = "CDORDTIT", nullable = true, length = 25)
  private String cdordtit;

  /** The cdcustod. */
  @Column(name = "CDCUSTOD", nullable = true, length = 25)
  private String cdcustod;

  /** The tptiprep. */
  @Column(name = "TPTIPREP", nullable = false, length = 1)
  private String tptiprep;

  /** The indhac. */
  @Column(name = "INDHAC", nullable = false, length = 1)
  private String indhac;

  /** The fechaalta. */
  @Column(name = "FECHA_ALTA", nullable = false, length = 26)
  private Timestamp fechaalta;

  /** The fechamod. */
  @Column(name = "FECHA_MOD", nullable = true, length = 26)
  private Timestamp fechamod;

  /** The fechabaja. */
  @Column(name = "FECHA_BAJA", nullable = true, length = 26)
  private Timestamp fechabaja;

  /** The usuarioalta. */
  @Column(name = "USUARIO_ALTA", nullable = false, length = 25)
  private String usuarioalta;

  /** The usuariomod. */
  @Column(name = "USUARIO_MOD", nullable = true, length = 25)
  private String usuariomod;

  /** The usuariobaja. */
  @Column(name = "USUARIO_BAJA", nullable = true, length = 25)
  private String usuariobaja;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public Integer getIdope() {
    return idope;
  }

  public void setIdope(Integer idope) {
    this.idope = idope;
  }

  public Integer getTintit() {
    return tintit;
  }

  public void setTintit(Integer tintit) {
    this.tintit = tintit;
  }

  public String getTicove() {
    return ticove;
  }

  public void setTicove(String ticove) {
    this.ticove = ticove;
  }

  public String getIsin() {
    return isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public Date getFechaope() {
    return fechaope;
  }

  public void setFechaope(Date fechaope) {
    this.fechaope = fechaope;
  }

  public Integer getOptitu() {
    return optitu;
  }

  public void setOptitu(Integer optitu) {
    this.optitu = optitu;
  }

  public BigDecimal getOppreu() {
    return oppreu;
  }

  public void setOppreu(BigDecimal oppreu) {
    this.oppreu = oppreu;
  }

  public BigDecimal getOppcpr() {
    return oppcpr;
  }

  public void setOppcpr(BigDecimal oppcpr) {
    this.oppcpr = oppcpr;
  }

  public BigDecimal getOpefec() {
    return opefec;
  }

  public void setOpefec(BigDecimal opefec) {
    this.opefec = opefec;
  }

  public BigDecimal getOpimpc() {
    return opimpc;
  }

  public void setOpimpc(BigDecimal opimpc) {
    this.opimpc = opimpc;
  }

  public String getIndcom() {
    return indcom;
  }

  public void setIndcom(String indcom) {
    this.indcom = indcom;
  }

  public Date getFechacob() {
    return fechacob;
  }

  public void setFechacob(Date fechacob) {
    this.fechacob = fechacob;
  }

  public String getCddclave() {
    return cddclave;
  }

  public void setCddclave(String cddclave) {
    this.cddclave = cddclave;
  }

  public String getTpidenti() {
    return tpidenti;
  }

  public void setTpidenti(String tpidenti) {
    this.tpidenti = tpidenti;
  }

  public BigDecimal getPcparti() {
    return pcparti;
  }

  public void setPcparti(BigDecimal pcparti) {
    this.pcparti = pcparti;
  }

  public String getCdordtit() {
    return cdordtit;
  }

  public void setCdordtit(String cdordtit) {
    this.cdordtit = cdordtit;
  }

  public String getCdcustod() {
    return cdcustod;
  }

  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  public void setTptiprep(String tptiprep) {
    this.tptiprep = tptiprep;
  }

  public String getTptiprep() {
    return tptiprep;
  }

  public String getIndhac() {
    return indhac;
  }

  public void setIndhac(String indhac) {
    this.indhac = indhac;
  }

  public Timestamp getFechaalta() {
    return fechaalta;
  }

  public void setFechaalta(Timestamp fechaalta) {
    this.fechaalta = fechaalta;
  }

  public Timestamp getFechamod() {
    return fechamod;
  }

  public void setFechamod(Timestamp fechamod) {
    this.fechamod = fechamod;
  }

  public Timestamp getFechabaja() {
    return fechabaja;
  }

  public void setFechabaja(Timestamp fechabaja) {
    this.fechabaja = fechabaja;
  }

  public String getUsuarioalta() {
    return usuarioalta;
  }

  public void setUsuarioalta(String usuarioalta) {
    this.usuarioalta = usuarioalta;
  }

  public String getUsuariomod() {
    return usuariomod;
  }

  public void setUsuariomod(String usuariomod) {
    this.usuariomod = usuariomod;
  }

  public String getUsuariobaja() {
    return usuariobaja;
  }

  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }

}
