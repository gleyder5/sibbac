package sibbac.business.sicav.web.rest;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sibbac.business.sicav.bo.BaseBo;
import sibbac.business.sicav.bo.SICAVOperacionesBo;
import sibbac.business.sicav.bo.dto.TMCT0EmisionDocDTO;
import sibbac.business.sicav.bo.dto.TMCT0NUMDTO;
import sibbac.business.sicav.bo.dto.TMCT0OPRDTO;
import sibbac.business.sicav.db.query.SICAVMantenimientoOpQuery;
import sibbac.common.CallableResultDTO;
import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.database.AbstractDynamicPageQuery;

/**
 * The Class Sicav.
 */
@RestController
@RequestMapping("/sicav/mantenimientoOp")
public class SICAVMantenimientoOpService extends BaseService {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(SICAVMantenimientoOpService.class);

  /** The Constant DEFAULT_CHARSET. */
  protected static final String DEFAULT_CHARSET = CharEncoding.UTF_8;

  /** The Constant DEFAULT_APPLICATION_TYPE. */
  protected static final String DEFAULT_APPLICATION_TYPE = MediaType.APPLICATION_JSON + ";charset=" + DEFAULT_CHARSET;

  @Autowired
  private BaseBo<TMCT0OPRDTO> sicavOperacionesBo;

  @Autowired
  private BaseBo<TMCT0NUMDTO> sicavNumeracionesBo;

  public SICAVMantenimientoOpService() {
    super("Mantenimiento de Operaciones", "mantenimientoOp", SICAVMantenimientoOpQuery.class.getSimpleName());
  }

  /**
   * Gets the listado observaciones clientes.
   *
   * @param idCliente the id cliente
   * @return the listado observaciones clientes
   */
  @RequestMapping(value = "/numeration/{num}", method = RequestMethod.GET)
  public List<TMCT0NUMDTO> getListadoMultiplesNumeraciones(@PathVariable("num") Integer valorIdOpe) {
    return sicavNumeracionesBo.getListadoMultiplesNumeraciones(valorIdOpe);
  }

  /**
   * Execute action post.
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/numeration", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody TMCT0NUMDTO solicitud) {
    return sicavNumeracionesBo.actionSave(solicitud);
  }

  /**
   * Execute action post.
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public CallableResultDTO executeActionPost(@RequestBody TMCT0OPRDTO solicitud) {
    return sicavOperacionesBo.actionSave(solicitud);
  }

  /**
   * Execute action put.
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionUpdate(@RequestBody List<TMCT0OPRDTO> solicitud) {
    return sicavOperacionesBo.actionUpdate(solicitud);
  }

  /**
   * Execute action delete List
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/{ids}", method = RequestMethod.DELETE)
  public CallableResultDTO executeActionDelete(@PathVariable("ids") List<BigDecimal> ids) {
    return sicavOperacionesBo.actionDelete(ids);
  }

  /**
   * Execute action put.
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/numeration", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.PUT)
  public CallableResultDTO executeActionUpdate(@RequestBody TMCT0NUMDTO solicitud) {
    return sicavNumeracionesBo.actionUpdate(solicitud);
  }

  /**
   * Execute action delete List
   *
   * @param name the name
   * @param solicitud the solicitud
   * @return the callable result DTO
   */
  @RequestMapping(value = "/numeration/delete/{ids}", method = RequestMethod.DELETE)
  public CallableResultDTO executeActionDeleteNumeraciones(@PathVariable("ids") List<BigDecimal> ids) {
    return sicavNumeracionesBo.actionDelete(ids);
  }

  /**
   * Exportación a del emision del documento
   * @param name
   * @param request
   * @param servletResponse
   * @return
   */
  @RequestMapping(value = "/exportEmisionDocumento/{name}", method = RequestMethod.POST)
  @ResponseBody
  public void executeActionExportExcel(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) {
    try {
      final AbstractDynamicPageQuery adpq = getFilledDynamicQuery(name, request.getParameterMap());

      adpq.setIsoFormat(true);

      final List<TMCT0EmisionDocDTO> results = queryBo.executeQueryNativeMapped(adpq, TMCT0EmisionDocDTO.class);

      ((SICAVOperacionesBo) sicavOperacionesBo).export(results, servletResponse);
    }
    catch (SIBBACBusinessException e) {
      LOGGER.error("Se ha producido un error durante la exportación a excel", e);
    }
  }

  /**
   * Exportación del Grid a Excel
   * @param name
   * @param request
   * @param servletResponse
   * @throws SIBBACBusinessException
   */
  @RequestMapping(value = "/{name}/excel", method = RequestMethod.POST)
  public void executeActionExportGridExcel(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws SIBBACBusinessException {
    try {
      final AbstractDynamicPageQuery adpq;
      final StreamResult streamResult;

      adpq = getFilledDynamicQuery(name, request.getParameterMap());
      adpq.setIsoFormat(true);
      streamResult = new HttpStreamResultHelper(servletResponse);
      queryBo.executeQuery(adpq, FormatStyle.EXCEL, streamResult);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error ejecutando Query", e);
    }
  }

  /**
   * Exportación del Grid a CSV
   * @param name
   * @param request
   * @param servletResponse
   * @throws SIBBACBusinessException
   */
  @RequestMapping(value = "/{name}/csv", method = RequestMethod.POST)
  public void executeActionExportGridCsv(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws SIBBACBusinessException {
    try {
      final AbstractDynamicPageQuery adpq;
      final StreamResult streamResult;

      adpq = getFilledDynamicQuery(name, request.getParameterMap());
      adpq.setIsoFormat(true);
      streamResult = new HttpStreamResultHelper(servletResponse);
      queryBo.executeQuery(adpq, FormatStyle.CSV, streamResult);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error ejecutando Query", e);
    }
  }

  @RequestMapping(value = "/getNominal", consumes = DEFAULT_APPLICATION_TYPE, method = RequestMethod.POST)
  public TMCT0OPRDTO getNominal(@RequestBody TMCT0OPRDTO opr) {
    ((SICAVOperacionesBo) sicavOperacionesBo).getNominal(opr);

    return opr;
  }
}
