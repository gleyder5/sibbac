package sibbac.business.sicav.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class SICAVMantenimientoOpQuery.
 */
@Component("SICAVOperacionesIdTitularQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SICAVOperacionesIdTitularQuery extends AbstractDynamicPageQuery {

  private static final String CDDCLAVE = "Código";
  private static final String DESTIT = "Descripcion";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT " + "TRIM(CDDCLAVE) AS CDDCLAVE, "
      + "CONCAT(TRIM(NBCLIENT), CONCAT(' ', CONCAT(TRIM(NBCLIEN1), CONCAT(' ', TRIM(NBCLIEN2))))) AS DESTIT ";

  /** The Constant FROM. */
  private static final String FROM = "FROM TMCT0FIS ";

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE FHFINAL >= TO_CHAR(CURRENT DATE, 'yyyymmdd')";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn(CDDCLAVE, "CDDCLAVE"),
      new DynamicColumn(DESTIT, "DESTIT") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;
    res = new ArrayList<>();
    res.add(new StringDynamicFilter("CDDCLAVE", CDDCLAVE));
    res.add(new StringDynamicFilter("DESTIT", DESTIT));
    return res;
  }

  public SICAVOperacionesIdTitularQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public SICAVOperacionesIdTitularQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public SICAVOperacionesIdTitularQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}