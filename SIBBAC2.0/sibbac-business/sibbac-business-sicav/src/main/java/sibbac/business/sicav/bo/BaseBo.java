package sibbac.business.sicav.bo;

import java.math.BigDecimal;
import java.util.List;

import sibbac.business.sicav.bo.dto.AbstractDTO;
import sibbac.business.sicav.bo.dto.TMCT0NUMDTO;
import sibbac.common.CallableResultDTO;

public interface BaseBo<T extends AbstractDTO> {

  public CallableResultDTO actionFindById(T solicitud);

  public CallableResultDTO actionSave(T solicitud);

  public CallableResultDTO actionUpdate(T solicitud);

  public CallableResultDTO actionUpdate(List<T> solicitud);

  public CallableResultDTO actionDelete(List<BigDecimal> solicitud);

  public List<TMCT0NUMDTO> getListadoMultiplesNumeraciones(Integer idope);
}