package sibbac.business.sicav.db.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.sicav.db.model.TMCT0NUMEntity;

/**
 * The Interface TMCT0NUMDAO.
 */
@Repository
public interface TMCT0NUMDAO extends JpaRepository<TMCT0NUMEntity, BigInteger> {

  @Query(value = "select * FROM BSNBPSQL.TMCT0NUM num where num.id_ope = ?1 and num.fecha_baja is null order by ID", nativeQuery = true)
  List<Object[]> getListadoMultiplesNumeraciones(Integer idOpe);

  @Query(value = "select coalesce(sum(num.optitu), 0) from BSNBPSQL.TMCT0NUM num " 
      + "where num.id_ope = ?1 "
      + "and num.fecha_baja is null", nativeQuery = true)
  Integer getTitulosAcumulados(Integer idope);

  @Query(value = "select num.opsec1, num.opsec2 from TMCT0NUM num " + 
      "WHERE num.ID_OPE = ?1 " + 
      "and num.fecha_baja is null " + 
      "order by cast(num.opsec1 as int) asc", nativeQuery = true)
  List<Object[]> getSecuenciasTitulos(Integer idope);
  
  @Query(value = "select num.opsec1, num.opsec2 from TMCT0NUM num " + 
      "WHERE num.ID_OPE = ?1 " +
      "and num.ID <> ?2 " +
      "and num.fecha_baja is null " + 
      "order by cast(num.opsec1 as int) asc", nativeQuery = true)
  List<Object[]> getSecuenciasTitulosUpdate(Integer idope, BigInteger id);
}