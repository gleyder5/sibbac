package sibbac.business.sicav.web.util;

public interface Constantes<T> {

  /**
   * Mensajes de Logs
   * @author ipalacio
   *
   */
  public enum LoggingMessages implements Constantes<String> {

    /** */
    DEBUG_MESSAGE_OAP_REST_ENTER("Enter: En {}.{}() con los siguientes parametros = {}"),

    /** */
    DEBUG_MESSAGE_OAP_REST_EXIT("Exit: En {}.{}() return de los siguientes datos = {}"),

    /** */
    DEBUG_MESSAGE_OAP_REST_ERROR("Error: Argumento Ilegal: {} en {}.{}()"),

    /** */
    RUNTIME_EXCEPTION_MESSAGE("[executeAction] Error inesperado al intentar ejecutar la accion {}"),

    /** */
    ID_OBJECT_NOT_FIND("[executeAction] La primarykey del objeto mandado no existe"),

    /** */
    PK_OBJECT_NULL("[executeAction] La primarykey del objeto mandado no está completa."),

    /** */
    BASIC_VALIDATION_ERROR("[executeAction] Los campos obligatorios no pueden estar vacio"),

    /** */
    BASIC_VALIDATION_ERROR2("Los campos obligatorios no pueden estar vacio"),

    /** */
    OBJECT_NULL("El objeto recibido viene nulo");

    private String value;

    private LoggingMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Mensajes de Aplicacion
   * @author ipalacio
   *
   */
  public enum ApplicationMessages implements Constantes<String> {

    UNEXPECTED_MESSAGE("Error inesperado, consulte con administrador"),

    ADD_REQUEST_DATA("Error, los datos obligatorios no han sido rellenados"),

    ADDED_DATA("Registro insertado correctamente"),

    ASSO_DATA("Registro asociado correctamente"),

    MODIFIED_DATA("Registro modificado correctamente"),

    DELETED_DATA("Registro(s) borrado(s) correctamente"),

    MULTIPLE_NOT_EXIST("Error, compruebe que las ids {} existan en la base de datos"),

    DUPLICATED_ID("Error, id duplicada"),

    WRONG_CALL("Llamada a servicio equivocado, consulte con el administrador"),

    DATE_CHANGED("La fecha de revisión ha sido modificada en todos los elementos de la tabla."),

    NOEXISTENTITY(
        "[ERROR] Uno de los registros no existe en la Base de datos, por lo que se cancela la operación. Contacte con un administrador");

    private String value;

    private ApplicationMessages(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return value;
    }
  }

  /**
   * 
   * @author ipalacio
   *
   */
  public enum Session implements Constantes<String> {

    /** */
    USER_SESSION("UserSession");

    private String value;

    private Session(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }

  }

  public T value();

  /**
   * Constantes para formato fecha
   * @author jhurtagr
   *
   */
  public enum Fecha implements Constantes<String> {

    FORMATO("dd/MM/yyyy");

    private String value;

    private Fecha(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para tipo de operaciones
   * @author jhurtagr
   *
   */
  public enum TipoOperacion implements Constantes<String> {

    COMPRA("C"),

    VENTA("V"),

    ERROR("[ERROR] El valor del Tipo de Operación debe ser [C]ompra o [V]enta");

    private String value;

    private TipoOperacion(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para cobrado comision
   * @author jhurtagr
   *
   */
  public enum CobradoComision implements Constantes<String> {

    SI("S"),

    NO("N"),

    ERROR("[ERROR] El valor de Cobrado Comision debe ser [S]i o [N]o");

    private String value;

    private CobradoComision(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para Fecha de Cobro
   * @author jhurtagr
   *
   */
  public enum FechaCobro implements Constantes<String> {

    ERROR("[ERROR] El campo Fecha de Cobro debe ser informado si Cobrado Comision es [S]i");

    private String value;

    private FechaCobro(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para Tipo de Identificador
   * @author jhurtagr
   *
   */
  public enum TipoIdentificador implements Constantes<String> {

    BIC("B"),

    NIF("N"),

    CIF("C"),

    NIE("E"),

    OTROS("O"),

    ERROR("[ERROR] El tipo de identificador solo puede tener valores de [B]ic, [N]if, [C]if, [N]ie u [O]tros");

    private String value;

    private TipoIdentificador(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para Indicador Hacienda
   * @author jhurtagr
   *
   */
  public enum IndicadorHacienda implements Constantes<String> {

    SI("S"),

    NO("N"),

    ERROR("[ERROR] El valor de Indicador de Hacienda debe ser [S]i o [N]o");

    private String value;

    private IndicadorHacienda(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para Indicador Hacienda
   * @author jhurtagr
   *
   */
  public enum Campos implements Constantes<String> {

    EXCLUYENTES("[ERROR] Los campos %Precio y Precio son excluyentes. Unicamente debe rellenarse uno de los dos"),

    HOLDERS("[ERROR] El identificador de titular proporcionado no existe.");

    private String value;

    private Campos(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para modificacion
   * @author jhurtagr
   *
   */
  public enum Modificacion implements Constantes<String> {

    ERROROPERACIONMASIVA(
        "[ERROR] Los modificación solo se puede realizar sobre registros que tenga el mismo numero de operación"),

    OBLIGATORIOS("[ERROR] Los modificación no cumple con los campos obligatorios"),

    SINPERMISOS("[ERROR] EL usuario no tiene permisos para modificar un valor con indicador de hacienda igual a [S]i");

    private String value;

    private Modificacion(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para borrado
   * @author jhurtagr
   *
   */
  public enum Borrado implements Constantes<String> {

    SINPERMISOS("[ERROR] EL usuario no tiene permisos para borrar un valor con indicador de hacienda igual a [S]i");

    private String value;

    private Borrado(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para Tipo de Titular
   * @author jhurtagr
   *
   */
  public enum TipoTitular implements Constantes<String> {

    TITULAR("T"),

    REPRESENTANTE("R"),

    USUFRUCTUARIO("U"),

    NUDOPROPIETARIO("N"),

    ERROR(
        "[ERROR] El tipo de titular solo puede tener valores de [T]itular, [R]epresentante, [U]sufructuario o [N]udopropietario");

    private String value;

    private TipoTitular(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }

  /**
   * Constantes para Numeracion
   * @author jhurtagr
   *
   */
  public enum Numeracion implements Constantes<String> {

    ID_REGISTRO_ERROR("[ERROR] Si se informa el identificador de Registro, el número de títulos debe ser obligatorio"),

    TIT_ACUMULADOS_MAYOR_TOTAL(
        "[ERROR] La suma de los títulos acumulados no puede superar al total de títulos de la operación"),

    SECUENCIALES_Y_IDREGISTRO_EXCLUYENTES(
        "[ERROR] El \"Secuencial Desde\" y \"Secuencial Hasta\" e \"Identificador de Registro\" no pueden ir rellenos o vacios en la misma numeración. Son excluyentes"),

    ACUM_MAS_PEND_IGUAL_TOTAL_OPERACION(
        "[ERROR] El número de títulos acumulados mas el número de títulos pendientes superar al número de títulos de la operación"),

    SECUENCIAS_RELLENAS("[ERROR] Si se rellena una secuencia, la otra debe ir tambien rellena."),

    SECUENCIA_HASTA_MAYOR_DESDE(
        "[ERROR] El \"Secuencial Hasta\" no puede ser inferior o igual al \"Secuencial Desde\""),

    SECUENCIAS_SE_PISAN(
        "[ERROR] La secuencia que ha mandado se están pisando con otras que existen en la base de datos");

    private String value;

    private Numeracion(String value) {
      this.value = value;
    }

    @Override
    public String value() {
      return this.value;
    }
  }
}
