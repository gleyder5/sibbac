package sibbac.business.sicav.bo;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.sicav.bo.dto.TMCT0NUMDTO;
import sibbac.business.sicav.db.dao.TMCT0NUMDAO;
import sibbac.business.sicav.db.dao.TMCT0OPRDAO;
import sibbac.business.sicav.db.model.TMCT0NUMEntity;
import sibbac.business.sicav.web.util.Constantes;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.aop.logging.SIBBACLogging;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class SICAVNumeracionesBo.
 */
@Service("sicavNumeracionesBo")
public class SICAVNumeracionesBo extends AbstractBo<TMCT0NUMEntity, BigInteger, TMCT0NUMDAO>
    implements BaseBo<TMCT0NUMDTO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(SICAVNumeracionesBo.class);

  /** The session. */
  @Autowired
  private HttpSession session;

  @Autowired
  private TMCT0NUMDAO numDao;

  @Autowired
  private TMCT0OPRDAO oprDao;

  private static final Boolean ACTUALIZAR = Boolean.TRUE;

  @Override
  public CallableResultDTO actionFindById(TMCT0NUMDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @SIBBACLogging
  @Override
  public CallableResultDTO actionSave(TMCT0NUMDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    try {
      if (null != solicitud && null != solicitud.getIdope()) {
        TMCT0NUMEntity entity = new TMCT0NUMEntity();
        entity.setOpsec1("-1");
        entity.setOpsec2("-1");

        if (validaciones(solicitud, result, Boolean.FALSE)
            && secuenciasNoSePisan(solicitud, result, entity, Boolean.FALSE)) {
          solicitud.setUsuarioalta(StringUtils.substring(
              ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
          solicitud.setFechaalta(new Timestamp(System.currentTimeMillis()));

          DozerBeanMapper mapper = new DozerBeanMapper();

          entity = mapper.map(solicitud, TMCT0NUMEntity.class);

          if (LOGGER.isDebugEnabled()) {
            String entityMsg = entity.toString();
            LOGGER.debug(MessageFormat.format("Guardando [{0}] en BBDD", entityMsg));
          }

          dao.save(entity);
          result.addMessage(Constantes.ApplicationMessages.ADDED_DATA.value());
        }
      }
      else {
        throw new SIBBACBusinessException(Constantes.LoggingMessages.OBJECT_NULL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
      result.addErrorMessage(e.getMessage());
    }
    return result;
  }

  @SIBBACLogging
  @Override
  public CallableResultDTO actionUpdate(List<TMCT0NUMDTO> solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO actionUpdate(TMCT0NUMDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();

    try {
      if (null != solicitud && null != solicitud.getId()) {
        TMCT0NUMEntity entity = numDao.findOne(solicitud.getId());
        if (null != entity) {
          if (validaciones(solicitud, result, ACTUALIZAR)
              && secuenciasNoSePisan(solicitud, result, entity, ACTUALIZAR)) {

            solicitud.setUsuariomod(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
            solicitud.setFechamod(new Timestamp(System.currentTimeMillis()));

            entity = pasarDatosToEntity(solicitud, entity);

            if (LOGGER.isDebugEnabled()) {
              String entityMsg = entity.toString();
              LOGGER.debug(MessageFormat.format("Guardando [{0}] en BBDD", entityMsg));
            }

            dao.save(entity);
            result.addMessage(Constantes.ApplicationMessages.MODIFIED_DATA.value());
          }
        }
        else {
          throw new SIBBACBusinessException(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
        }
      }
      else {
        throw new SIBBACBusinessException(Constantes.LoggingMessages.OBJECT_NULL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
      result.addErrorMessage(e.getMessage());
    }
    return result;
  }

  @Override
  public CallableResultDTO actionDelete(List<BigDecimal> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();

    try {
      for (BigDecimal solicitud : solicitudList) {
        if (solicitud != null) {
          final TMCT0NUMEntity entityFind = dao.findOne(solicitud.toBigInteger());

          if (entityFind != null) {
            entityFind.setFechabaja(new Timestamp(System.currentTimeMillis()));
            entityFind.setUsuariobaja(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));

            dao.save(entityFind);
          }
          else {
            err.add(String.valueOf(solicitud));
          }
        }
      }
      if (!err.isEmpty()) {
        throw new SIBBACBusinessException(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value());
      }
      else {
        result.addMessage(Constantes.ApplicationMessages.DELETED_DATA.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage());
      result.addErrorMessage(e.getMessage());
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  private boolean secuenciasNoSePisan(TMCT0NUMDTO solicitud, CallableResultDTO result, TMCT0NUMEntity entity,
      Boolean update) {
    if (null != solicitud.getOpsec1() && null != solicitud.getOpsec2()) {
      List<Object[]> listadoSecuenciasObject;
      if (!update) {
        listadoSecuenciasObject = numDao.getSecuenciasTitulos(solicitud.getIdope());
      }
      else {
        listadoSecuenciasObject = numDao.getSecuenciasTitulosUpdate(solicitud.getIdope(), solicitud.getId());
      }
      List<String[]> listadoSecuencias = new ArrayList<>();
      convertObjectToStringArray(listadoSecuenciasObject, listadoSecuencias);

      if (null != listadoSecuenciasObject && !listadoSecuencias.isEmpty()) {
        boolean huecoEncontrado = Boolean.FALSE;
        // Guardamos en variables los valores de la secuencia que vienen en la
        // solicitud
        int seqDesdeSolicitud = Integer.parseInt(solicitud.getOpsec1());
        int seqHastaSolicitud = Integer.parseInt(solicitud.getOpsec2());
        int seqDesdeBD = 0;
        int seqHastaBD = 0;
        int tamanioLista = 0;
        String[] secuenciaBaseDatos;
        while (!huecoEncontrado) {
          // Primero dividimos el tamaño de la lista para empezar a buscar por
          // medio
          tamanioLista = listadoSecuencias.size() / 2;
          secuenciaBaseDatos = listadoSecuencias.get(tamanioLista);
          seqDesdeBD = Integer.parseInt(secuenciaBaseDatos[0]);
          seqHastaBD = Integer.parseInt(secuenciaBaseDatos[1]);
          if (tamanioLista == 0) {
            huecoEncontrado = Boolean.TRUE;
          }
          else {

            if (seqDesdeSolicitud >= seqDesdeBD || seqHastaSolicitud >= seqDesdeBD) {
              listadoSecuencias = listadoSecuencias.subList(tamanioLista, listadoSecuencias.size());
            }
            else {
              listadoSecuencias = listadoSecuencias.subList(0, tamanioLista);
            }
          }
        }
        // seqHastaSolicitud >= seqDesdeBD && seqDesdeSolicitud < seqHastaBD
        if (((seqDesdeSolicitud <= seqDesdeBD && seqHastaSolicitud >= seqDesdeBD)
            || (seqDesdeSolicitud <= seqHastaBD && seqHastaSolicitud >= seqDesdeBD))
            && !(String.valueOf(seqDesdeBD).equalsIgnoreCase(entity.getOpsec1())
                && String.valueOf(seqHastaBD).equalsIgnoreCase(entity.getOpsec2()))) {
          result.addErrorMessage(Constantes.Numeracion.SECUENCIAS_SE_PISAN.value());
        }

      }
    }
    else {
      LOG.info("No hay secuencia, no se comprueba si se está pisando a otra secuencia de títulos");
    }
    return (result.getErrorMessage().isEmpty()) ? Boolean.TRUE : Boolean.FALSE;
  }

  private void convertObjectToStringArray(List<Object[]> listadoSecuenciasObject, List<String[]> listadoSecuencias) {
    for (Object[] element : listadoSecuenciasObject) {
      String[] obs = new String[2];
      if (null != element[0]) {
        obs[0] = element[0].toString();
      }
      if (null != element[1]) {
        obs[1] = element[1].toString();
      }
      if (null != obs[0] && null != obs[1]) {
        listadoSecuencias.add(obs);
      }
    }
  }

  private boolean validaciones(TMCT0NUMDTO solicitud, CallableResultDTO result, Boolean actualizar) {
    /**
     * - Si se ha informado los campos secuencia desde/hasta, y no se ha
     * introducido nº de títulos, el nº de títulos se calculará con la
     * información introducida en la Secuencia Hasta – Secuencia Desde + 1
     */
    if ((null != solicitud.getOpsec1() && !solicitud.getOpsec1().isEmpty())
        && (null != solicitud.getOpsec2() && !solicitud.getOpsec2().isEmpty())
        && (null == solicitud.getOptitu() || solicitud.getOptitu().isEmpty())) {
      solicitud.setOptitu(
          String.valueOf(Integer.parseInt(solicitud.getOpsec2()) - Integer.parseInt(solicitud.getOpsec1()) + 1));
    }
    /**
     * - Si se informa el Identificador de Registro, el nº de títulos debe ser
     * obligatorio informarlos.
     */
    if ((null != solicitud.getNuidre() && !solicitud.getNuidre().isEmpty())
        && (null == solicitud.getOptitu() || solicitud.getOptitu().isEmpty())) {
      result.addErrorMessage(Constantes.Numeracion.ID_REGISTRO_ERROR.value());
    }

    /**
     * - Que el nº de títulos introducidos (o calculados en base a la Secuencia
     * Desde/Hasta) + nº de títulos acumulados, sea inferior o igual al total de
     * títulos de la operación.
     */
    if (null != solicitud.getOptitu() && !solicitud.getOptitu().isEmpty()) {
      Integer acumuladosInicial = numDao.getTitulosAcumulados(solicitud.getIdope());

      if (actualizar) {
        TMCT0NUMEntity titulosAnteriores = numDao.findOne(solicitud.getId());
        acumuladosInicial -= Integer.parseInt(titulosAnteriores.getOptitu());
      }
      Integer totalTitulosOperacion = oprDao.getTitulosTotal(solicitud.getIdope());

      Integer pendientes = totalTitulosOperacion - acumuladosInicial;

      Integer acumulados = Integer.parseInt(solicitud.getOptitu()) + acumuladosInicial;
      // Si los titulos acumulados superan al total de titulos de la
      // operación...ERROR
      if (acumulados > totalTitulosOperacion) {
        result.addErrorMessage(Constantes.Numeracion.TIT_ACUMULADOS_MAYOR_TOTAL.value());
      }

      /**
       * - El nº de títulos acumulados + el nº de títulos pendientes es igual al
       * nº de títulos de la operación.
       */
      if (!titulosSonIguales(solicitud, acumuladosInicial, pendientes, totalTitulosOperacion)) {
        result.addErrorMessage(Constantes.Numeracion.ACUM_MAS_PEND_IGUAL_TOTAL_OPERACION.value());
      }
    }

    /**
     * - Se informe bien Secuencial Desde y Secuencial Hasta o bien el
     * Identificador de Registro. Numeraciones vs Identificador son excluyentes.
     */
    if (((null != solicitud.getOpsec1() && !solicitud.getOpsec1().isEmpty())
        && (null != solicitud.getOpsec2() && !solicitud.getOpsec2().isEmpty())
        && (null != solicitud.getNuidre() && !solicitud.getNuidre().isEmpty()))
        || ((null == solicitud.getOpsec1() || solicitud.getOpsec1().isEmpty())
            && (null == solicitud.getOpsec2() || solicitud.getOpsec2().isEmpty())
            && (null == solicitud.getNuidre() || solicitud.getNuidre().isEmpty()))) {
      result.addErrorMessage(Constantes.Numeracion.SECUENCIALES_Y_IDREGISTRO_EXCLUYENTES.value());
    }

    /**
     * Nos aseguramos que si se rellena una secuencia, la otra tambien lo esté.
     */
    if (((null != solicitud.getOpsec1() && !solicitud.getOpsec1().isEmpty())
        && (null == solicitud.getOpsec2() || solicitud.getOpsec2().isEmpty()))
        || ((null == solicitud.getOpsec1() || solicitud.getOpsec1().isEmpty())
            && (null != solicitud.getOpsec2() && !solicitud.getOpsec2().isEmpty()))) {
      result.addErrorMessage(Constantes.Numeracion.SECUENCIAS_RELLENAS.value());
    }
    else {
      if (null != solicitud.getOpsec1() && null != solicitud.getOpsec2()) {
        int valorDesde = Integer.parseInt(solicitud.getOpsec1());
        int valorHasta = Integer.parseInt(solicitud.getOpsec2());
        if (valorHasta <= valorDesde) {
          result.addErrorMessage(Constantes.Numeracion.SECUENCIA_HASTA_MAYOR_DESDE.value());
        }
      }
    }

    if (!result.getErrorMessage().isEmpty()) {
      return Boolean.FALSE;
    }

    return Boolean.TRUE;
  }

  private boolean titulosSonIguales(TMCT0NUMDTO solicitud, Integer acumulados, Integer pendientes,
      Integer totalTitulosOperacion) {
    return (acumulados + pendientes == totalTitulosOperacion) ? Boolean.TRUE : Boolean.FALSE;
  }

  private TMCT0NUMEntity pasarDatosToEntity(TMCT0NUMDTO solicitud, TMCT0NUMEntity entity) {

    if (null != solicitud.getOpsec1() && !solicitud.getOpsec1().isEmpty()) {
      entity.setOpsec1(solicitud.getOpsec1());
    }
    if (null != solicitud.getOpsec2() && !solicitud.getOpsec2().isEmpty()) {
      entity.setOpsec2(solicitud.getOpsec2());
    }
    if (null != solicitud.getNuidre() && !solicitud.getNuidre().isEmpty()) {
      entity.setNuidre(solicitud.getNuidre());
    }
    if (null != solicitud.getOptitu() && !solicitud.getOptitu().isEmpty()) {
      entity.setOptitu(solicitud.getOptitu());
    }
    return entity;
  }

  public List<TMCT0NUMDTO> getListadoMultiplesNumeraciones(Integer idope) {
    List<TMCT0NUMDTO> listadoNumeraciones = new ArrayList<>();
    try {
      if (null != idope) {
        // Nos traemos la lista de observaciones del cliente especifico
        List<Object[]> listadoTotal = numDao.getListadoMultiplesNumeraciones(idope);

        convertObjectToObservacionesDTO(listadoTotal, listadoNumeraciones);
      }
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
    }

    return listadoNumeraciones;
  }

  private void convertObjectToObservacionesDTO(List<Object[]> listadoTotal, List<TMCT0NUMDTO> listadoNumeraciones) {
    for (Object[] element : listadoTotal) {
      TMCT0NUMDTO obs = toDTONumeraciones(element);
      listadoNumeraciones.add(obs);
    }
  }

  private TMCT0NUMDTO toDTONumeraciones(Object[] element) {
    TMCT0NUMDTO num = new TMCT0NUMDTO();
    if (null != element[0]) {
      num.setId(new BigInteger(element[0].toString()));
    }
    if (null != element[1]) {
      num.setIdope(Integer.parseInt(element[1].toString()));
    }
    if (null != element[2]) {
      num.setOpsec1(element[2].toString());
    }
    if (null != element[3]) {
      num.setOpsec2(element[3].toString());
    }
    if (null != element[4]) {
      num.setOptitu(element[4].toString());
    }
    if (null != element[5]) {
      num.setNuidre(element[5].toString());
    }
    if (null != element[6]) {
      num.setFechaalta(stringToTimestamp(element[6].toString()));
    }
    if (null != element[7]) {
      num.setFechamod(stringToTimestamp(element[7].toString()));
    }
    if (null != element[8]) {
      num.setFechabaja(stringToTimestamp(element[8].toString()));
    }
    if (null != element[9]) {
      num.setUsuarioalta(element[9].toString());
    }
    if (null != element[10]) {
      num.setUsuariomod(element[10].toString());
    }
    if (null != element[11]) {
      num.setUsuariobaja(element[11].toString());
    }

    return num;
  }

  private Timestamp stringToTimestamp(String string) {
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
      return new java.sql.Timestamp(dateFormat.parse(string).getTime());
    }
    catch (ParseException e) {
      LOG.error("Error durante el parseo de fechas", e);
      return null;
    }
  }
}
