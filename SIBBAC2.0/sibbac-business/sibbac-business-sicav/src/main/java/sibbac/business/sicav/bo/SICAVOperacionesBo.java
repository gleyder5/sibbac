package sibbac.business.sicav.bo;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import sibbac.business.mantenimientodatoscliente.database.dao.Tmct0fisDao;
import sibbac.business.mantenimientodatoscliente.database.model.Tmct0fis;
import sibbac.business.sicav.bo.dto.TMCT0EmisionDocDTO;
import sibbac.business.sicav.bo.dto.TMCT0NUMDTO;
import sibbac.business.sicav.bo.dto.TMCT0OPRDTO;
import sibbac.business.sicav.db.dao.TMCT0MaestroValoresDao;
import sibbac.business.sicav.db.dao.TMCT0OPRDAO;
import sibbac.business.sicav.db.model.TMCT0OPREntity;
import sibbac.business.sicav.web.util.Constantes;
import sibbac.common.CallableResultDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.session.UserSession;

/**
 * The Class SICAVOperacionesBo.
 */
@Service("sicavOperacionesBo")
public class SICAVOperacionesBo extends AbstractBo<TMCT0OPREntity, BigInteger, TMCT0OPRDAO>
    implements BaseBo<TMCT0OPRDTO> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOGGER = LoggerFactory.getLogger(SICAVOperacionesBo.class);

  /** The session. */
  @Autowired
  private HttpSession session;

  @Value("${sicav.excel.temp.path:/sbrmv/ficheros/temporal/}")
  private String tempDir;

  @Value("${sicav.excel.fileName.temporal:EmsionDocumento_%s.xls}")
  private String tempXlsFile;

  @Value("${sicav.users.valid.profile}")
  private String usuariosPermisos;

  @Autowired
  private Tmct0fisDao fisDao;

  @Autowired
  private TMCT0OPRDAO oprDao;

  @Autowired
  private TMCT0MaestroValoresDao masDao;

  @Override
  public CallableResultDTO actionFindById(TMCT0OPRDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO actionSave(TMCT0OPRDTO solicitud) {
    final CallableResultDTO result = new CallableResultDTO();
    Boolean asociado = Boolean.FALSE;
    try {
      if (solicitud != null) {
        // Comprobamos datos obligatorios
        if (datosObligatorios(solicitud)) {
          List<TMCT0OPRDTO> lista = new ArrayList<>();
          lista.add(solicitud);
          if (validaciones(lista, result)) {
            // Si la id de operacion es nula, es que es un alta. Por lo
            // contrario seria una asociacion de operacion.
            if (null == solicitud.getIdope()) {
              solicitud.setIdope(oprDao.getSequenceTmct0Opr());
            }
            else {
              // Vaciamos la id primaria para que no actualice el valor ya
              // existente
              asociado = Boolean.TRUE;
              solicitud.setId(null);
            }

            solicitud.setUsuarioalta(StringUtils.substring(
                ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
            solicitud.setFechaalta(new Timestamp(System.currentTimeMillis()));

            DozerBeanMapper mapper = new DozerBeanMapper();

            TMCT0OPREntity entity = mapper.map(solicitud, TMCT0OPREntity.class);

            if (LOGGER.isDebugEnabled()) {
              String entityMsg = entity.toString();
              LOGGER.debug(MessageFormat.format("Guardando [{0}] en BBDD", entityMsg));
            }

            dao.save(entity);
            if (!asociado) {
              result.addMessage(Constantes.ApplicationMessages.ADDED_DATA.value());
            }
            else {
              result.addMessage(Constantes.ApplicationMessages.ASSO_DATA.value());
            }
          }

        }
        else {
          throw new SIBBACBusinessException(Constantes.LoggingMessages.BASIC_VALIDATION_ERROR2.value());
        }
      }
      else {
        throw new SIBBACBusinessException(Constantes.LoggingMessages.OBJECT_NULL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
      result.addErrorMessage(e.getMessage());
    }
    return result;
  }

  @Override
  public CallableResultDTO actionUpdate(List<TMCT0OPRDTO> solicitud) {
    final CallableResultDTO result = new CallableResultDTO();
    try {
      if (solicitud != null && !solicitud.isEmpty()) {

        // Comprobamos datos obligatorios
        if (datosObligatoriosModificacion(solicitud, result, session)) {

          if (validaciones(solicitud, result)) {
            List<TMCT0OPREntity> entityList = new ArrayList<>();

            for (int i = 0; result.getErrorMessage().isEmpty() && i < solicitud.size(); i++) {
              TMCT0OPRDTO dto = solicitud.get(i);

              TMCT0OPREntity entity = dao.findOne(dto.getId());
              // Si es igual a null la entidad, es que NO existe en la BD, por
              // lo que cancelamos la operacion
              if (null == entity) {
                result.addErrorMessage(Constantes.ApplicationMessages.NOEXISTENTITY.value());
              }
              else {
                asignarValoresModificacion(dto, entity);

                if (LOGGER.isDebugEnabled()) {
                  String entityMsg = entity.toString();
                  LOGGER.debug(MessageFormat.format("Guardando [{0}] en BBDD", entityMsg));
                }
                entityList.add(entity);
              }

            }
            dao.save(entityList);

            result.addMessage(Constantes.ApplicationMessages.MODIFIED_DATA.value());
          }

        }
        else {
          throw new SIBBACBusinessException(Constantes.LoggingMessages.BASIC_VALIDATION_ERROR2.value());
        }
      }
      else {
        throw new SIBBACBusinessException(Constantes.LoggingMessages.OBJECT_NULL.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(e.getMessage(), e);
      result.addErrorMessage(e.getMessage());
    }
    return result;
  }

  private void asignarValoresModificacion(TMCT0OPRDTO dto, TMCT0OPREntity entity) {
    entity.setUsuariomod(StringUtils
        .substring(((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
    entity.setFechamod(new Timestamp(System.currentTimeMillis()));

    /**
     * null != dto.getTintit() && !dto.getTicove().isEmpty() && null !=
     * dto.getIsin() && null != dto.getFechaope() && null != dto.getOptitu() &&
     * null != dto.getOpimpc() && !dto.getIndcom().isEmpty() &&
     * !dto.getCddclave().isEmpty() && !dto.getTpidenti().isEmpty() &&
     * !dto.getTptiprep().isEmpty() && null != dto.getPcparti() &&
     * !dto.getIndhac().isEmpty())
     */

    /**
     * Campos obligatorios - Vienen seguro porque se ha comprobado previamente
     */
    entity.setTicove(dto.getTicove());
    entity.setOptitu(dto.getOptitu());
    entity.setFechaope(dto.getFechaope());
    entity.setIsin(dto.getIsin());
    entity.setIndcom(dto.getIndcom());
    entity.setOpimpc(dto.getOpimpc());
    entity.setTpidenti(dto.getTpidenti());
    entity.setCddclave(dto.getCddclave());
    entity.setTptiprep(dto.getTptiprep());
    entity.setPcparti(dto.getPcparti());
    entity.setIndhac(dto.getIndhac());
    /*****************************************/
    /** Campos opcionales */
    if (null != dto.getOppcpr()) {
      entity.setOppcpr(dto.getOppcpr());
    }
    else {
      entity.setOppcpr(null);
    }
    if (null != dto.getOppreu()) {
      entity.setOppreu(dto.getOppreu());
    }
    else {
      entity.setOppreu(null);
    }
    if (null != dto.getOpefec()) {
      entity.setOpefec(dto.getOpefec());
    }
    if (null != dto.getFechacob()) {
      entity.setFechacob(dto.getFechacob());
    }
    if (null != dto.getCdordtit()) {
      entity.setCdordtit(dto.getCdordtit());
    }
    entity.setCdcustod((null != dto.getCdcustod()) ? dto.getCdcustod() : null);

  }

  @Override
  public CallableResultDTO actionUpdate(TMCT0OPRDTO solicitud) {
    CallableResultDTO callableResultDTO = new CallableResultDTO();
    callableResultDTO.addErrorMessage(Constantes.ApplicationMessages.WRONG_CALL.value());
    return callableResultDTO;
  }

  @Override
  public CallableResultDTO actionDelete(List<BigDecimal> solicitudList) {
    final CallableResultDTO result = new CallableResultDTO();
    final List<String> err = new ArrayList<>();
    List<TMCT0OPREntity> listaEntities = new ArrayList<>();

    try {
      for (BigDecimal solicitud : solicitudList) {
        if (solicitud != null) {
          final TMCT0OPREntity entityFind = dao.findOne(solicitud.toBigInteger());
          if (entityFind != null) {
            if (entityFind.getIndhac().equalsIgnoreCase(Constantes.IndicadorHacienda.SI.value())
                && !userIsAdmin(session)) {
              err.add(Constantes.Borrado.SINPERMISOS.value());
            }
            else {
              entityFind.setFechabaja(new Timestamp(System.currentTimeMillis()));
              entityFind.setUsuariobaja(StringUtils.substring(
                  ((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10));
              listaEntities.add(entityFind);
            }
          }
          else {
            err.add(String.valueOf(solicitud));
          }
        }
      }
      if (!err.isEmpty()) {
        throw new SIBBACBusinessException();
      }
      else {
        dao.save(listaEntities);
        result.addMessage(Constantes.ApplicationMessages.DELETED_DATA.value());
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.ID_OBJECT_NOT_FIND.value(), e);
      result.addErrorMessage(
          MessageFormat.format(Constantes.ApplicationMessages.MULTIPLE_NOT_EXIST.value(), err.toString()));
    }
    catch (RuntimeException rex) {
      LOG.error(Constantes.LoggingMessages.RUNTIME_EXCEPTION_MESSAGE.value(), rex);
      result.addErrorMessage(Constantes.ApplicationMessages.UNEXPECTED_MESSAGE.value());
    }
    return result;
  }

  private boolean validaciones(List<TMCT0OPRDTO> lista, CallableResultDTO result) {
    for (TMCT0OPRDTO dto : lista) {
      /* Validacion TINTIT (Numero de titular): */

      /* Validacion TICOVE (Tipo de operacion): */
      if (!dto.getTicove().equals(Constantes.TipoOperacion.COMPRA.value())
          && !dto.getTicove().equals(Constantes.TipoOperacion.VENTA.value())) {
        result.addErrorMessage(Constantes.TipoOperacion.ERROR.value());
      }
      /* Validacion ISIN (codigo ISIN): */

      /* Validacion FECHA_OPE (fecha de operación): */

      /* Validacion OPTITU (Numero de titulos): */

      /* Validacion OPPREU (Precio de la operación): */
      if ((null != dto.getOppreu() && null != dto.getOppcpr())
          || (null == dto.getOppreu() && null == dto.getOppcpr())) {
        result.addErrorMessage(Constantes.Campos.EXCLUYENTES.value());
      }
      /* Validacion OPPCPR (% precio): */

      /* Validacion OPEFEC (Efectivo operación): */
      if (null == dto.getOpefec()) {
        /** Si esta relleno el PRECIO */
        if (null != dto.getOppreu() && null == dto.getOppcpr()) {
          dto.setOpefec(new BigDecimal(dto.getOptitu() * dto.getOppreu().intValue()));
        }
        /** Si esta relleno el % PRECIO */
        else if (null == dto.getOppreu() && null != dto.getOppcpr()) {
          calculoEfectivo(dto);
        }
      }
      else {
        if (null == dto.getOppcpr()) {
          dto.setOppreu(dto.getOpefec().divide(new BigDecimal(dto.getOptitu()), 6, RoundingMode.CEILING));
        }
        else {
          calculoEfectivo(dto);
        }
      }
      /* Validacion OPIMPC (Importe de la comisión): */

      /* Validacion INDCOM (Indicador cobrado comision S/N): */
      if (!dto.getIndcom().equals(Constantes.CobradoComision.SI.value())
          && !dto.getIndcom().equals(Constantes.CobradoComision.NO.value())) {
        result.addErrorMessage(Constantes.TipoOperacion.ERROR.value());
      }
      /* Validacion FECHA_COB (Fecha de cobro): */
      if (dto.getIndcom().equals(Constantes.CobradoComision.SI.value()) && null == dto.getFechacob()) {
        result.addErrorMessage(Constantes.FechaCobro.ERROR.value());
      }
      /* Validacion CDDCLAVE (Identificador del titular): */
      if (!existeIdentificadoTitular(dto, result)) {
        result.addErrorMessage(Constantes.Campos.HOLDERS.value());
      }

      /* Validacion PCPARTI (% Participacion Titular): */

      /* Validacion CDORDTIT (Ordenante titular: */

      /* Validacion CDCUSTOD (Custodio del titular): */

      /* Validacion TPTIPREP (Custodio del titular): */
      if (!dto.getTptiprep().equals(Constantes.TipoTitular.TITULAR.value())
          && !dto.getTptiprep().equals(Constantes.TipoTitular.REPRESENTANTE.value())
          && !dto.getTptiprep().equals(Constantes.TipoTitular.USUFRUCTUARIO.value())
          && !dto.getTptiprep().equals(Constantes.TipoTitular.NUDOPROPIETARIO.value())) {
        result.addErrorMessage(Constantes.TipoTitular.ERROR.value());
      }

      /* Validacion INDHAC (Indicador de hacienda S/N): */
      if (!dto.getIndhac().equals(Constantes.IndicadorHacienda.SI.value())
          && !dto.getIndhac().equals(Constantes.IndicadorHacienda.NO.value())) {
        result.addErrorMessage(Constantes.IndicadorHacienda.ERROR.value());
      }

      if (!result.getErrorMessage().isEmpty()) {
        return Boolean.FALSE;
      }
    }
    return Boolean.TRUE;
  }

  private boolean existeIdentificadoTitular(TMCT0OPRDTO dto, CallableResultDTO result) {
    Tmct0fis fisEntity = fisDao.findBycdholder(dto.getCddclave());

    if (null == fisEntity) {
      return Boolean.FALSE;
    }
    else {
      dto.setTpidenti(fisEntity.getTpidenti());
      /* Validacion TPIDENTI (Tipo de identificador B/N/C/E/O): */
      if (!dto.getTpidenti().equals(Constantes.TipoIdentificador.BIC.value())
          && !dto.getTpidenti().equals(Constantes.TipoIdentificador.NIF.value())
          && !dto.getTpidenti().equals(Constantes.TipoIdentificador.CIF.value())
          && !dto.getTpidenti().equals(Constantes.TipoIdentificador.NIE.value())
          && !dto.getTpidenti().equals(Constantes.TipoIdentificador.OTROS.value())) {
        result.addErrorMessage(Constantes.TipoIdentificador.ERROR.value());
        return Boolean.FALSE;
      }
    }
    return Boolean.TRUE;

  }

  private void calculoEfectivo(TMCT0OPRDTO solicitud) {
    /**
     * el efectivo = Títulos * el nominal unitario del valor (obtenido del
     * maestro de valores) * (el % precio introducido/100)
     */
    Double valorNominal = masDao.getValorNominal(solicitud.getIsin());
    if (null != valorNominal) {
      solicitud.setOpefec(
          BigDecimal.valueOf(solicitud.getOptitu() * valorNominal * ((solicitud.getOppcpr().doubleValue() / 100))));
    }

  }

  private boolean datosObligatorios(TMCT0OPRDTO solicitud) {
    Integer valor = 0;

    if (null == solicitud.getIdope()) {
      solicitud.setTintit(valor + 1);

      return (null != solicitud.getTintit() && !solicitud.getTicove().isEmpty() && null != solicitud.getIsin()
          && null != solicitud.getFechaope() && null != solicitud.getOptitu() && null != solicitud.getOpimpc()
          && !solicitud.getIndcom().isEmpty() && !solicitud.getCddclave().isEmpty()
          && !solicitud.getTptiprep().isEmpty() && null != solicitud.getPcparti() && !solicitud.getIndhac().isEmpty());
    }
    else {
      // Asociar operaciones
      if (null != solicitud.getIdope()) {
        /** Traemos el valor para la siguiente operacion */
        valor = oprDao.getSequenceTintitPorIdDao(solicitud.getIdope());
      }
      solicitud.setTintit(valor + 1);

      return (!solicitud.getTicove().isEmpty() && null != solicitud.getOptitu() && !solicitud.getCddclave().isEmpty()
          && !solicitud.getTptiprep().isEmpty() && null != solicitud.getPcparti());
    }
  }

  private boolean datosObligatoriosModificacion(List<TMCT0OPRDTO> solicitud, CallableResultDTO result,
      HttpSession session2) {
    // Guardamos el IDOPE del primer elemento para comparar con el resto
    Integer idOpe = solicitud.get(0).getIdope();
    for (TMCT0OPRDTO dto : solicitud) {
      if (idOpe != dto.getIdope()) {
        result.addErrorMessage(Constantes.Modificacion.ERROROPERACIONMASIVA.value());
        return Boolean.FALSE;
      }
      else {
        if (!(null != dto.getTintit() && !dto.getTicove().isEmpty() && null != dto.getIsin()
            && null != dto.getFechaope() && null != dto.getOptitu() && null != dto.getOpimpc()
            && !dto.getIndcom().isEmpty() && !dto.getCddclave().isEmpty() && !dto.getTpidenti().isEmpty()
            && !dto.getTptiprep().isEmpty() && null != dto.getPcparti() && !dto.getIndhac().isEmpty())) {
          result.addErrorMessage(Constantes.Modificacion.OBLIGATORIOS.value());
        }
        if (dto.getIndhac().equalsIgnoreCase(Constantes.IndicadorHacienda.SI.value()) && !userIsAdmin(session)) {
          result.addErrorMessage(Constantes.Modificacion.SINPERMISOS.value());
        }
        if (!result.getErrorMessage().isEmpty()) {
          return Boolean.FALSE;
        }
      }
    }
    return Boolean.TRUE;

  }

  private boolean userIsAdmin(HttpSession session) {
    String nombreUsuario = StringUtils
        .substring(((UserSession) session.getAttribute(Constantes.Session.USER_SESSION.value())).getName(), 0, 10);
    String tipoUsuario = oprDao.permisosUsuario(nombreUsuario);

    return (usuariosPermisos.contains(tipoUsuario)) ? Boolean.TRUE : Boolean.FALSE;
  }

  /**
   * Realiza la exportación
   * @param solicitud
   * @return
   */
  public void export(List<TMCT0EmisionDocDTO> listaOperaciones, HttpServletResponse response) {
    XSSFWorkbook workbook = null;
    try {
      if (null != listaOperaciones && !listaOperaciones.isEmpty()) {
        workbook = generarExcelOperaciones(listaOperaciones);
        exportFile(saveExcels(workbook), response);
      }
      else {
        throw new SIBBACBusinessException("No se puede generar el documento ya que la lista viene vacia");
      }
    }
    catch (SIBBACBusinessException e) {
      LOG.error(Constantes.LoggingMessages.BASIC_VALIDATION_ERROR2.value(), e);
    }
    finally {
      if (workbook != null) {
        try {
          workbook.close();
        }
        catch (IOException e) {
          LOG.error("No se ha podido cerrar el documento Excel", e);
        }
      }
    }
  }

  /**
   * Genera el contenido de la excel
   * @param listaOperaciones
   * @return
   */
  private XSSFWorkbook generarExcelOperaciones(List<TMCT0EmisionDocDTO> listaOperaciones) {
    XSSFWorkbook workbook = new XSSFWorkbook();

    final String celdaVacia = " ";

    // Estilo de la cabecera
    XSSFCellStyle xssfCellStyleCabecera = workbook.createCellStyle();

    xssfCellStyleCabecera.setFillForegroundColor(new XSSFColor(Color.RED));
    xssfCellStyleCabecera.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
    xssfCellStyleCabecera.setWrapText(true);

    // Crear la fuente de la cabecera
    XSSFFont xssfFont = workbook.createFont();
    xssfFont.setFontHeightInPoints((short) 8);
    xssfFont.setFontName("ARIAL");
    xssfFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
    xssfFont.setColor(new XSSFColor(Color.WHITE));
    xssfCellStyleCabecera.setFont(xssfFont);

    xssfCellStyleCabecera.setBorderBottom(XSSFCellStyle.BORDER_THIN);
    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());
    xssfCellStyleCabecera.setBorderRight(XSSFCellStyle.BORDER_THIN);
    xssfCellStyleCabecera.setBottomBorderColor(IndexedColors.BLACK.getIndex());

    xssfCellStyleCabecera.setAlignment(XSSFCellStyle.ALIGN_CENTER);
    xssfCellStyleCabecera.setVerticalAlignment(XSSFCellStyle.ALIGN_FILL);

    // Hoja con el listado de errores
    XSSFSheet registros = workbook.createSheet("OPERACIONES");
    int rowMin = 0;

    // Cabecera
    XSSFRow row = registros.createRow(rowMin++);
    row.setHeight((short) 600);
    XSSFCell cell = null;

    List<String> header = getHeaders();
    int indexHeader = 0;

    int index = 0;
    // pinta los titulos de las columnas
    for (String title : header) {
      cell = row.createCell(indexHeader++);
      cell.setCellValue(title);
      cell.setCellStyle(xssfCellStyleCabecera);
      registros.autoSizeColumn(index++);
    }

    // Recorre las operaciones

    final String operacionCompra = "C";
    final String operacionVenta = "V";

    for (int i = 0; i < listaOperaciones.size(); i++) {

      int indexContent = 0;
      TMCT0EmisionDocDTO opr = listaOperaciones.get(i);
      row = registros.createRow(rowMin++);

      Calendar cal = Calendar.getInstance();
      cal.setTime(opr.getFechaope());
      int anio = cal.get(Calendar.YEAR);
      int mes = cal.get(Calendar.MONTH);
      int dia = cal.get(Calendar.DAY_OF_MONTH);

      // DIA DE LA OPERACION
      cell = row.createCell(indexContent);
      cell.setCellValue(String.valueOf(dia));
      registros.autoSizeColumn(indexContent++);

      // MES DE LA OPERACION
      cell = row.createCell(indexContent);
      cell.setCellValue(String.valueOf(mes));
      registros.autoSizeColumn(indexContent++);

      // AÑO DE LA OPERACION
      cell = row.createCell(indexContent);
      cell.setCellValue(String.valueOf(anio));
      registros.autoSizeColumn(indexContent++);

      // CODIGO ISIN - DESCRIPCION ISIN
      /**
       * CON EL CODIGO ISIN, ATACAMOS A LA TABLA TMCT0FIS Y OBTENEMOS SU
       * DESCRIPCION, NBTITREV CONCATENAMOS 'ISIN + " - " + DESCRIPCION'
       */
      cell = row.createCell(indexContent);
      StringBuilder str = new StringBuilder();
      if (null != opr.getIsin() && null != opr.getNbtitrev()) {
        str.append(opr.getIsin().replaceAll(" +", " "));
        str.append(" - ");
        str.append(opr.getNbtitrev().replaceAll(" +", " "));
      }
      else {
        str.append(celdaVacia);
      }
      cell.setCellValue(str.toString());
      registros.autoSizeColumn(indexContent++);

      // TITULOS
      cell = row.createCell(indexContent);
      if (null != opr.getOptitu()) {
        cell.setCellValue(String.valueOf(opr.getOptitu()));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      // NOMINAL
      cell = row.createCell(indexContent);
      if (null != opr.getNunominv()) {
        cell.setCellValue(String.valueOf(opr.getNunominv()));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      // PRECIO
      cell = row.createCell(indexContent);
      if (null != opr.getOppreu()) {
        cell.setCellValue(String.valueOf(opr.getOppreu()));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      // EFECTIVO
      cell = row.createCell(indexContent);
      if (null != opr.getOpefec()) {
        cell.setCellValue(String.valueOf(opr.getOpefec()));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      // COMISION COMPRA
      cell = row.createCell(indexContent);
      if (opr.getTicove().equalsIgnoreCase(operacionCompra)) {
        cell.setCellValue(String.valueOf(opr.getOpimpc()));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      // COMISION VENTA
      cell = row.createCell(indexContent);
      if (opr.getTicove().equalsIgnoreCase(operacionVenta)) {
        cell.setCellValue(String.valueOf(opr.getOpimpc()));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      /**
       * COMISION TOTAL (La suma de todas las operaciones que tengan misma //
       * ID_OPE y fecha baja sea nula
       */
      Integer comisionTotal = oprDao.getComisionTotalOperacion(opr.getIdope());
      cell = row.createCell(indexContent);
      if (null != comisionTotal) {
        cell.setCellValue(String.valueOf(comisionTotal));
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
      registros.autoSizeColumn(indexContent++);

      double efectivo = 0;
      double comision = 0;
      if (null != opr.getOpefec()) {
        efectivo = opr.getOpefec().doubleValue();
      }
      if (null != opr.getOpimpc()) {
        comision = opr.getOpimpc().doubleValue();
      }
      // NETO COMPRA
      cell = row.createCell(indexContent);
      if (opr.getTicove().equalsIgnoreCase(operacionCompra)) {
        cell.setCellValue(String.valueOf(efectivo + comision));
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
      registros.autoSizeColumn(indexContent++);

      // NETO VENTA
      cell = row.createCell(indexContent);
      if (opr.getTicove().equalsIgnoreCase(operacionVenta)) {
        cell.setCellValue(String.valueOf(efectivo - comision));
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
      registros.autoSizeColumn(indexContent++);

      // ORDENANTE
      cell = row.createCell(indexContent);
      if (null != opr.getCdordtit()) {
        cell.setCellValue(opr.getCdordtit().replaceAll(" +", " "));
      }
      else {
        cell.setCellValue(celdaVacia);
      }
      registros.autoSizeColumn(indexContent++);

      Tmct0fis persona = fisDao.traerDatosPersona(opr.getCddclave());

      if (null != persona) {
        indexContent = datosPersona(registros, celdaVacia, row, cell, indexContent, opr, persona, operacionCompra);
        indexContent = datosPersona(registros, celdaVacia, row, cell, indexContent, opr, persona, operacionVenta);
      }

    }
    return workbook;

  }

  private int datosPersona(XSSFSheet registros, String celdaVacia, XSSFRow row, XSSFCell cell, int indexContent,
      TMCT0EmisionDocDTO opr, Tmct0fis persona, String operacion) {
    // NOMBRE
    cell = row.createCell(indexContent);
    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (persona.getTpsocied().equalsIgnoreCase("J")) {
          if (null != persona.getNbclien1()) {
            cell.setCellValue(persona.getNbclien1().replaceAll(" +", " "));
          }
          else {
            cell.setCellValue(String.valueOf(celdaVacia));
          }
        }
        else {
          // ES FISICA
          StringBuilder strPersona = new StringBuilder();
          strPersona.append("");
          if (null != persona.getNbclient()) {
            strPersona.append(persona.getNbclient().replaceAll(" +", " "));
            strPersona.append(" ");
          }
          if (null != persona.getNbclien1()) {
            strPersona.append(persona.getNbclien1().replaceAll(" +", " "));
            strPersona.append(" ");
          }
          if (null != persona.getNbclien2()) {
            strPersona.append(persona.getNbclien2().replaceAll(" +", " "));
          }
          cell.setCellValue(strPersona.toString());
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // IDENTIFICADOR
    cell = row.createCell(indexContent);
    if (opr.getTicove().equalsIgnoreCase(operacion)) {
      cell.setCellValue(opr.getCddclave().replaceAll(" +", " "));
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // TIPO DIRECCION
    cell = row.createCell(indexContent);
    if (opr.getTicove().equalsIgnoreCase(operacion)) {
      cell.setCellValue(opr.getDomicilio().substring(0, 2));
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // DOMICILIO
    cell = row.createCell(indexContent);

    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (null != persona.getNbdomici()) {
          cell.setCellValue(persona.getNbdomici().replaceAll(" +", " "));
        }
        else {
          cell.setCellValue(String.valueOf(celdaVacia));
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // NUMERO DOMICILIO
    cell = row.createCell(indexContent);

    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (null != persona.getNudomici()) {
          cell.setCellValue(persona.getNudomici().replaceAll(" +", " "));
        }
        else {
          cell.setCellValue(String.valueOf(celdaVacia));
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // CODIGO POSTAL
    cell = row.createCell(indexContent);

    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (null != persona.getCdpostal()) {
          cell.setCellValue(persona.getCdpostal().replaceAll(" +", " "));
        }
        else {
          cell.setCellValue(String.valueOf(celdaVacia));
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // CIUDAD
    cell = row.createCell(indexContent);

    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (null != persona.getNbciudad()) {
          cell.setCellValue(persona.getNbciudad().replaceAll(" +", " "));
        }
        else {
          cell.setCellValue(String.valueOf(celdaVacia));
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // PROVINCIA
    cell = row.createCell(indexContent);

    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (null != persona.getNbprovin()) {
          cell.setCellValue(persona.getNbprovin().replaceAll(" +", " "));
        }
        else {
          cell.setCellValue(String.valueOf(celdaVacia));
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);

    // PAIS RESIDENCIA
    cell = row.createCell(indexContent);

    if (null != persona) {
      if (opr.getTicove().equalsIgnoreCase(operacion)) {
        if (null != persona.getCddepais()) {
          cell.setCellValue(persona.getCddepais().replaceAll(" +", " "));
        }
        else {
          cell.setCellValue(String.valueOf(celdaVacia));
        }
      }
      else {
        cell.setCellValue(String.valueOf(celdaVacia));
      }
    }
    else {
      cell.setCellValue(String.valueOf(celdaVacia));
    }
    registros.autoSizeColumn(indexContent++);
    return indexContent;
  }

  private List<String> getHeaders() {
    List<String> header = new ArrayList<>();

    header.add("DIA");
    header.add("MES");
    header.add("AÑO");
    header.add("EMISOR");
    header.add("NUMTIT");
    // header.add("NOMUNIT");
    header.add("NOMINALT");
    header.add("PRECIO");
    header.add("EFECTIVO");
    // Si la operación es de compra
    header.add("COMISCPA");
    // Si la operación es de venta
    header.add("COMISVTA");
    header.add("COMISTOT");
    // Si la operación es de compra
    header.add("NETOCPA");
    // Si la operación es de venta
    header.add("NETOVTA");
    header.add("ORDENANTE");

    /** Si la operación es de compra */
    header.add("COMPRADOR");
    header.add("NIFC");
    header.add("TIPDIRC");
    header.add("DOMICCPA");
    header.add("NCC");
    header.add("CPC");
    header.add("CIUC");
    header.add("PROVC");
    header.add("PAISC");

    /** Si la operación es de venta */
    header.add("VENDEDOR");
    header.add("NIFV");
    header.add("TIPDIRV");
    header.add("DOMICVTA");
    header.add("NCV");
    header.add("CPV");
    header.add("CIUV");
    header.add("PROVV");
    header.add("PAISV");

    return header;
  }

  /**
   * Exporta el fichero y lo envía al front y lo borra
   * @param referencia
   * @param response
   * @throws SIBBACBusinessException
   */
  private void exportFile(File file, HttpServletResponse response) throws SIBBACBusinessException {
    final String filenameXls = file.getName();

    InputStream in = null;

    try {
      in = new FileInputStream(file);

      response.setContentType("application/vnd.ms-excel");
      response.setHeader("Content-Disposition", "attachment; filename=" + filenameXls);
      response.setHeader("Content-Length", String.valueOf(file.length()));

      FileCopyUtils.copy(in, response.getOutputStream());

      response.flushBuffer();

      boolean resultDetele = file.delete();

      if (LOG.isDebugEnabled()) {
        if (resultDetele) {
          LOG.debug(String.format("Se borro el fichero temporal '%s' del fileSystem", filenameXls));
        }
        else {
          LOG.debug(String.format("No se borro el fichero temporal '%s' del fileSystem", filenameXls));
        }
      }
    }
    catch (IOException exception) {
      throw new SIBBACBusinessException(String.format("Error durante la exportación del fichero '%s'", filenameXls));
    }
    finally {
      if (in != null) {
        try {
          in.close();
        }
        catch (IOException e) {
          LOG.error("No se pudo cerrar el inputStream de lectura del fichero", e);
        }
      }
    }
  }

  /**
   * Guarda el Excel en disco
   * @param workbook
   */
  private File saveExcels(XSSFWorkbook workbook) {
    // directory
    FileOutputStream fos = null;

    final String timeStamp = FormatDataUtils.convertDateToString(new Date(), FormatDataUtils.TIMESTAMP_FORMAT);
    final String xlsxName = String.format(this.tempXlsFile, timeStamp);
    final String xlsxPath = String.format("%s%s", this.tempDir, xlsxName);

    final File xlsxFile = new File(xlsxPath);

    try {
      fos = new FileOutputStream(xlsxFile);
      workbook.write(fos);
    }
    catch (IOException e) {
      LOG.error("Se ha producido un error durante la exportación del excel de Emision del Docuemento.", e);
    }
    finally {
      if (fos != null) {
        try {
          fos.close();
        }
        catch (IOException e) {
          LOG.error("No se ha podido cerrar el OutputStream del excel de Emision del Documento.", e);
        }
      }
    }
    return xlsxFile;
  }

  @Override
  public List<TMCT0NUMDTO> getListadoMultiplesNumeraciones(Integer id) {
    return null;
  }

  public void getNominal(TMCT0OPRDTO opr) {
    Double valorNominal = null;

    if (null != opr.getIsin()) {
      valorNominal = masDao.getValorNominal(opr.getIsin());
    }
    if (null != valorNominal && null != opr.getOptitu() && null != opr.getOppcpr()) {
      opr.setOpefec(BigDecimal.valueOf(opr.getOptitu() * valorNominal * (opr.getOppcpr().doubleValue() / 100)));
    }
  }
}
