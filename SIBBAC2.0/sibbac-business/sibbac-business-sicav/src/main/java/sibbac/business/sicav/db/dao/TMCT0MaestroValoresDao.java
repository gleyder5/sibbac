package sibbac.business.sicav.db.dao;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.sicav.db.model.MaestroValoresEntity;

@Repository
public interface TMCT0MaestroValoresDao extends JpaRepository<MaestroValoresEntity, BigInteger> {

  @Query(value = "SELECT mae.NUNOMINV FROM BSNBPSQL.TMCT0_MAESTRO_VALORES mae WHERE mae.CDISINVV = ?1 GROUP BY mae.NUNOMINV, mae.CDISINVV", nativeQuery = true)
  Double getValorNominal(String isin);

  @Query(value = "SELECT DISTINCT CDISINVV FROM BSNBPSQL.TMCT0_MAESTRO_VALORES", nativeQuery = true)
  List<String> getISINList();

  @Query(value = "SELECT distinct mae.NBTITREV FROM BSNBPSQL.TMCT0_MAESTRO_VALORES mae WHERE mae.CDISINVV = ?1", nativeQuery = true)
  String getDescription(String string);
}