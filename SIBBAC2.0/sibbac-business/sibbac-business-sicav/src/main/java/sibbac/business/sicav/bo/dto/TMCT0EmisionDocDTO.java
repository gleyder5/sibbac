package sibbac.business.sicav.bo.dto;

import java.math.BigDecimal;

public class TMCT0EmisionDocDTO extends TMCT0OPRDTO {

  private static final long serialVersionUID = 9024364404486858737L;

  private String nbtitrev;

  private String domicilio;

  private String nbapetit;

  private String cdordtitdes;

  private String cdcustoddes;

  private String destit;
  
  private String nominal;
  
  private  BigDecimal cdclave;

  public void setId(Long id) {
    setId(id);
  }

  public String getNbtitrev() {
    return nbtitrev;
  }

  public void setNbtitrev(String nbtitrev) {
    this.nbtitrev = nbtitrev;
  }

  public String getDomicilio() {
    return domicilio;
  }

  public void setDomicilio(String domicilio) {
    this.domicilio = domicilio;
  }

  public String getNbapetit() {
    return nbapetit;
  }

  public void setNbapetit(String nbapetit) {
    this.nbapetit = nbapetit;
  }

  public String getCdordtitdes() {
    return cdordtitdes;
  }

  public void setCdordtitdes(String cdordtitdes) {
    this.cdordtitdes = cdordtitdes;
  }

  public String getCdcustoddes() {
    return cdcustoddes;
  }

  public void setCdcustoddes(String cdcustoddes) {
    this.cdcustoddes = cdcustoddes;
  }

  public String getDestit() {
    return destit;
  }

  public void setDestit(String destit) {
    this.destit = destit;
  }

  public String getNominal() {
    return nominal;
  }

  public void setNominal(String nominal) {
    this.nominal = nominal;
  }

  public BigDecimal getCdclave() {
    return cdclave;
  }

  public void setCdclave(BigDecimal cdclave) {
    this.cdclave = cdclave;
  }
  
  

}
