package sibbac.business.sicav.db.dao;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import sibbac.business.sicav.db.model.TMCT0OPREntity;

/**
 * The Interface TMCT0OPRDAO.
 */
@Repository
public interface TMCT0OPRDAO extends JpaRepository<TMCT0OPREntity, BigInteger> {

  @Query(value = "SELECT NEXT VALUE FOR TMCT0OPR_IDOPE_SEQ AS CONTADOR FROM SYSIBM.SYSDUMMY1", nativeQuery = true)
  Integer getSequenceTmct0Opr();

  @Query(value = "select count(opr.tintit) from BSNBPSQL.TMCT0OPR opr where opr.id_ope = ?1 and opr.fecha_baja is null", nativeQuery = true)
  Integer getSequenceTintitPorIdDao(Integer idope);

  @Query(value = "SELECT BSNBPSQL.TMCT0_PROFILES.NAME FROM BSNBPSQL.TMCT0_USERS "
      + " INNER JOIN BSNBPSQL.TMCT0_USERS_PROFILES ON BSNBPSQL.TMCT0_USERS.ID_USER = BSNBPSQL.TMCT0_USERS_PROFILES.ID_USER"
      + " INNER JOIN BSNBPSQL.TMCT0_PROFILES ON BSNBPSQL.TMCT0_PROFILES.ID_PROFILE = BSNBPSQL.TMCT0_USERS_PROFILES.ID_PROFILE"
      + " WHERE BSNBPSQL.TMCT0_USERS.USERNAME = ?1", nativeQuery = true)
  String permisosUsuario(String nombreUsuario);

  @Query(value = "select coalesce(sum(opr.optitu), 0) from BSNBPSQL.TMCT0OPR opr where id_ope = ?1 and opr.fecha_baja is null", nativeQuery = true)
  Integer getTitulosTotal(Integer idope);

  @Query(value = "select coalesce(sum(opr.opimpc), 0) from BSNBPSQL.TMCT0OPR opr where opr.id_ope = ?1 and opr.fecha_baja is null", nativeQuery = true)
  Integer getComisionTotalOperacion(Integer idope);
}