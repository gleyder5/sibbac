package sibbac.business.sicav.web.rest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.CharEncoding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import sibbac.common.FormatStyle;
import sibbac.common.HttpStreamResultHelper;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.FilterInfo;
import sibbac.database.bo.QueryBo;

/**
 * The Class BaseService.
 */
public abstract class BaseService {

  /** The Constant DEFAULT_CHARSET. */
  protected static final String DEFAULT_CHARSET = CharEncoding.UTF_8;

  /** The Constant DEFAULT_APPLICATION_TYPE. */
  protected static final String DEFAULT_APPLICATION_TYPE = MediaType.APPLICATION_JSON + ";charset=" + DEFAULT_CHARSET;

  /** The ctx. */
  @Autowired
  protected ApplicationContext ctx;

  /** The query bo. */
  @Autowired
  protected QueryBo queryBo;

  /** The query info. */
  protected QueryInfo[] queryInfo;

  /**
   * Constructor por parámetros.
   *
   * @param descripcion the descripcion
   * @param accion the accion
   * @param nombreClase the nombre clase
   */
  protected BaseService(String descripcion, String accion, String nombreClase) {
    this.queryInfo = new QueryInfo[] { new QueryInfo(descripcion, accion, nombreClase) };
  }

  /**
   * Contructor con varios QueryInfo.
   *
   * @param queryInfo the query info
   */
  protected BaseService(QueryInfo... queryInfo) {
    this.queryInfo = queryInfo;
  }

  /**
   * Gets the queries.
   *
   * @param servletResponse the servlet response
   * @return the queries
   * @throws IOException Signals that an I/O exception has occurred.
   */
  @RequestMapping(produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public void getQueries(HttpServletResponse servletResponse) throws IOException {
    servletResponse.setCharacterEncoding(DEFAULT_CHARSET);
    servletResponse.setContentType(DEFAULT_APPLICATION_TYPE);
    try (JsonGenerator writer = new JsonFactory().createGenerator(servletResponse.getWriter())) {
      writer.writeStartArray();

      for (QueryInfo item : queryInfo) {
        writeQuery(writer, item.getNombreClase(), item.getDescripcion(), item.getAccion());
      }

      writer.writeEndArray();
    }
  }

  /**
   * Gets the filters.
   *
   * @param name the name
   * @return the filters
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @RequestMapping(value = "/{name}/filters", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<FilterInfo> getFilters(@PathVariable("name") String name) throws SIBBACBusinessException {
    final AbstractDynamicPageQuery query;

    query = getDynamicQuery(name);
    return query.getFiltersInfo();
  }

  /**
   * Gets the columns.
   *
   * @param name the name
   * @return the columns
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @RequestMapping(value = "/{name}/columns", produces = DEFAULT_APPLICATION_TYPE, method = RequestMethod.GET)
  public List<DynamicColumn> getColumns(@PathVariable("name") String name) throws SIBBACBusinessException {
    try {
      final AbstractDynamicPageQuery query = getDynamicQuery(name);
      return query.getColumns();
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error obteniendo columnas", e);
    }
  }

  /**
   * Execute query.
   *
   * @param name the name
   * @param request the request
   * @param servletResponse the servlet response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @RequestMapping(value = "/{name}", method = RequestMethod.GET)
  public void executeQuery(@PathVariable("name") String name, HttpServletRequest request,
      HttpServletResponse servletResponse) throws SIBBACBusinessException {
    try {
      final AbstractDynamicPageQuery adpq;
      final StreamResult streamResult;

      adpq = getFilledDynamicQuery(name, request.getParameterMap());
      adpq.setIsoFormat(true);
      streamResult = new HttpStreamResultHelper(servletResponse);
      queryBo.executeQuery(adpq, FormatStyle.JSON_OBJECTS, streamResult);
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error ejecutando Query", e);
    }
  }

  /**
   * Write query.
   *
   * @param writer the writer
   * @param className the class name
   * @param desc the desc
   * @param accion the accion
   * @throws IOException Signals that an I/O exception has occurred.
   */
  protected void writeQuery(JsonGenerator writer, String className, String desc, String accion) throws IOException {
    writer.writeStartObject();
    writer.writeFieldName("name");
    writer.writeString(className);
    writer.writeFieldName("desc");
    writer.writeString(desc);
    writer.writeFieldName("accion");
    writer.writeString(accion);
    writer.writeEndObject();
  }

  /**
   * Gets the dynamic query.
   *
   * @param name the name
   * @return the dynamic query
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  protected AbstractDynamicPageQuery getDynamicQuery(String name) throws SIBBACBusinessException {
    return ctx.getBean(name, AbstractDynamicPageQuery.class);
  }

  /**
   * Gets the filled dynamic query.
   *
   * @param name the name
   * @param params the params
   * @return the filled dynamic query
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  protected AbstractDynamicPageQuery getFilledDynamicQuery(String name, Map<String, String[]> params)
      throws SIBBACBusinessException {
    try {

      final AbstractDynamicPageQuery adpq;

      adpq = getDynamicQuery(name);
      adpq.fillDynamicQuery(params);

      return adpq;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException("Error obteniendo Query dinámicamente rellenada", e);
    }
  }

  /**
   * Define las quieries que se van a ejecutar.
   *
   * @author ipalacio
   */
  protected static class QueryInfo {

    /** The descripcion. */
    protected String descripcion;

    /** The accion. */
    protected String accion;

    /** The nombre clase. */
    protected String nombreClase;

    /**
     * Instantiates a new query info.
     *
     * @param descripcion the descripcion
     * @param accion the accion
     * @param nombreClase the nombre clase
     */
    public QueryInfo(String descripcion, String accion, String nombreClase) {
      super();
      this.descripcion = descripcion;
      this.accion = accion;
      this.nombreClase = nombreClase;
    }

    /**
     * Gets the descripcion.
     *
     * @return the descripcion
     */
    public String getDescripcion() {
      return descripcion;
    }

    /**
     * Sets the descripcion.
     *
     * @param descripcion the new descripcion
     */
    public void setDescripcion(String descripcion) {
      this.descripcion = descripcion;
    }

    /**
     * Gets the accion.
     *
     * @return the accion
     */
    public String getAccion() {
      return accion;
    }

    /**
     * Sets the accion.
     *
     * @param accion the new accion
     */
    public void setAccion(String accion) {
      this.accion = accion;
    }

    /**
     * Gets the nombre clase.
     *
     * @return the nombre clase
     */
    public String getNombreClase() {
      return nombreClase;
    }

    /**
     * Sets the nombre clase.
     *
     * @param nombreClase the new nombre clase
     */
    public void setNombreClase(String nombreClase) {
      this.nombreClase = nombreClase;
    }
  }

}
