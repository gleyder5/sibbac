package sibbac.business.sicav.bo.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The Class TMCT0OPRDTO.
 */
public class TMCT0OPRDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4442068969616818065L;

  /** The id. */
  private BigInteger id;

  /** The idope. */
  private Integer idope;

  /** The tintit. */
  private Integer tintit;

  /** The ticove. */
  private String ticove;

  /** The isin. */
  private String isin;

  /** The nunominv. */
  private BigDecimal nunominv;

  /** The fechaope. */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Date fechaope;

  /** The optitu. */
  private Integer optitu;

  /** The oppreu. */
  private BigDecimal oppreu;

  /** The oppcpr. */
  private BigDecimal oppcpr;

  /** The opefec. */
  private BigDecimal opefec;

  /** The opimpc. */
  private BigDecimal opimpc;

  /** The indcom. */
  private String indcom;

  /** The fechacob. */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Date fechacob;

  /** The cddclave. */
  private String cddclave;

  /** The tpidenti. */
  private String tpidenti;

  /** The pcparti. */
  private BigDecimal pcparti;

  /** The cdordtit. */
  private String cdordtit;

  /** The cdcustod. */
  private String cdcustod;

  /** The tptiprep. */
  private String tptiprep;

  /** The indhac. */
  private String indhac;

  /** The fechaalta. */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Timestamp fechaalta;

  /** The fechamod. */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Timestamp fechamod;

  /** The fechabaja. */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private Timestamp fechabaja;

  /** The usuarioalta. */
  private String usuarioalta;

  /** The usuariomod. */
  private String usuariomod;

  /** The usuariobaja. */
  private String usuariobaja;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * Gets the idope.
   *
   * @return the idope
   */
  public Integer getIdope() {
    return idope;
  }

  /**
   * Sets the idope.
   *
   * @param idope the new idope
   */
  public void setIdope(Integer idope) {
    this.idope = idope;
  }

  /**
   * Gets the tintit.
   *
   * @return the tintit
   */
  public Integer getTintit() {
    return tintit;
  }

  /**
   * Sets the tintit.
   *
   * @param tintit the new tintit
   */
  public void setTintit(Integer tintit) {
    this.tintit = tintit;
  }

  /**
   * Gets the ticove.
   *
   * @return the ticove
   */
  public String getTicove() {
    return (null == ticove) ? "" : ticove;
  }

  /**
   * Sets the ticove.
   *
   * @param ticove the new ticove
   */
  public void setTicove(String ticove) {
    this.ticove = ticove;
  }

  /**
   * Gets the isin.
   *
   * @return the isin
   */
  public String getIsin() {
    return isin;
  }

  /**
   * Sets the isin.
   *
   * @param isin the new isin
   */
  public void setIsin(String isin) {
    this.isin = isin;
  }

  /**
   * Gets the nunominv.
   *
   * @return the nunominv
   */
  public BigDecimal getNunominv() {
    return nunominv;
  }

  /**
   * Sets the nunominv.
   *
   * @param nunominv the new nunominv
   */
  public void setNunominv(BigDecimal nunominv) {
    this.nunominv = nunominv;
  }

  /**
   * Gets the fechaope.
   *
   * @return the fechaope
   */
  public Date getFechaope() {
    return fechaope;
  }

  /**
   * Sets the fechaope.
   *
   * @param fechaope the new fechaope
   */
  public void setFechaope(Date fechaope) {
    this.fechaope = fechaope;
  }

  /**
   * Gets the optitu.
   *
   * @return the optitu
   */
  public Integer getOptitu() {
    return optitu;
  }

  /**
   * Sets the optitu.
   *
   * @param optitu the new optitu
   */
  public void setOptitu(Integer optitu) {
    this.optitu = optitu;
  }

  /**
   * Gets the oppreu.
   *
   * @return the oppreu
   */
  public BigDecimal getOppreu() {
    return oppreu;
  }

  /**
   * Sets the oppreu.
   *
   * @param oppreu the new oppreu
   */
  public void setOppreu(BigDecimal oppreu) {
    this.oppreu = oppreu;
  }

  /**
   * Gets the oppcpr.
   *
   * @return the oppcpr
   */
  public BigDecimal getOppcpr() {
    return oppcpr;
  }

  /**
   * Sets the oppcpr.
   *
   * @param oppcpr the new oppcpr
   */
  public void setOppcpr(BigDecimal oppcpr) {
    this.oppcpr = oppcpr;
  }

  /**
   * Gets the opefec.
   *
   * @return the opefec
   */
  public BigDecimal getOpefec() {
    return opefec;
  }

  /**
   * Sets the opefec.
   *
   * @param opefec the new opefec
   */
  public void setOpefec(BigDecimal opefec) {
    this.opefec = opefec;
  }

  /**
   * Gets the opimpc.
   *
   * @return the opimpc
   */
  public BigDecimal getOpimpc() {
    return opimpc;
  }

  /**
   * Sets the opimpc.
   *
   * @param opimpc the new opimpc
   */
  public void setOpimpc(BigDecimal opimpc) {
    this.opimpc = opimpc;
  }

  /**
   * Gets the indcom.
   *
   * @return the indcom
   */
  public String getIndcom() {
    return (null == indcom) ? "" : indcom;
  }

  /**
   * Sets the indcom.
   *
   * @param indcom the new indcom
   */
  public void setIndcom(String indcom) {
    this.indcom = indcom;
  }

  /**
   * Gets the fechacob.
   *
   * @return the fechacob
   */
  public Date getFechacob() {
    return fechacob;
  }

  /**
   * Sets the fechacob.
   *
   * @param fechacob the new fechacob
   */
  public void setFechacob(Date fechacob) {
    this.fechacob = fechacob;
  }

  /**
   * Gets the cddclave.
   *
   * @return the cddclave
   */
  public String getCddclave() {
    return (null == cddclave) ? "" : cddclave;
  }

  /**
   * Sets the cddclave.
   *
   * @param cddclave the new cddclave
   */
  public void setCddclave(String cddclave) {
    this.cddclave = cddclave;
  }

  /**
   * Gets the tpidenti.
   *
   * @return the tpidenti
   */
  public String getTpidenti() {
    return (null == tpidenti) ? "" : tpidenti;
  }

  /**
   * Sets the tpidenti.
   *
   * @param tpidenti the new tpidenti
   */
  public void setTpidenti(String tpidenti) {
    this.tpidenti = tpidenti;
  }

  /**
   * Gets the pcparti.
   *
   * @return the pcparti
   */
  public BigDecimal getPcparti() {
    return pcparti;
  }

  /**
   * Sets the pcparti.
   *
   * @param pcparti the new pcparti
   */
  public void setPcparti(BigDecimal pcparti) {
    this.pcparti = pcparti;
  }

  /**
   * Gets the cdordtit.
   *
   * @return the cdordtit
   */
  public String getCdordtit() {
    return cdordtit;
  }

  /**
   * Sets the cdordtit.
   *
   * @param cdordtit the new cdordtit
   */
  public void setCdordtit(String cdordtit) {
    this.cdordtit = cdordtit;
  }

  /**
   * Gets the cdcustod.
   *
   * @return the cdcustod
   */
  public String getCdcustod() {
    return cdcustod;
  }

  /**
   * Sets the cdcustod.
   *
   * @param cdcustod the new cdcustod
   */
  public void setCdcustod(String cdcustod) {
    this.cdcustod = cdcustod;
  }

  /**
   * Gets the tptiprep.
   *
   * @return the tptiprep
   */
  public String getTptiprep() {
    return tptiprep;
  }

  /**
   * Sets the tptiprep.
   *
   * @param tptiprep the new tptiprep
   */
  public void setTptiprep(String tptiprep) {
    this.tptiprep = tptiprep;
  }

  /**
   * Gets the indhac.
   *
   * @return the indhac
   */
  public String getIndhac() {
    return (null == indhac) ? "" : indhac;
  }

  /**
   * Sets the indhac.
   *
   * @param indhac the new indhac
   */
  public void setIndhac(String indhac) {
    this.indhac = indhac;
  }

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Timestamp getFechaalta() {
    return fechaalta;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaalta(Timestamp fechaalta) {
    this.fechaalta = fechaalta;
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Timestamp getFechamod() {
    return fechamod;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechamod(Timestamp fechamod) {
    this.fechamod = fechamod;
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Timestamp getFechabaja() {
    return fechabaja;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechabaja(Timestamp fechabaja) {
    this.fechabaja = fechabaja;
  }

  /**
   * Gets the usuarioalta.
   *
   * @return the usuarioalta
   */
  public String getUsuarioalta() {
    return usuarioalta;
  }

  /**
   * Sets the usuarioalta.
   *
   * @param usuarioalta the new usuarioalta
   */
  public void setUsuarioalta(String usuarioalta) {
    this.usuarioalta = usuarioalta;
  }

  /**
   * Gets the usuariomod.
   *
   * @return the usuariomod
   */
  public String getUsuariomod() {
    return usuariomod;
  }

  /**
   * Sets the usuariomod.
   *
   * @param usuariomod the new usuariomod
   */
  public void setUsuariomod(String usuariomod) {
    this.usuariomod = usuariomod;
  }

  /**
   * Gets the usuariobaja.
   *
   * @return the usuariobaja
   */
  public String getUsuariobaja() {
    return usuariobaja;
  }

  /**
   * Sets the usuariobaja.
   *
   * @param usuariobaja the new usuariobaja
   */
  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }
}
