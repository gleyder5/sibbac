package sibbac.business.sicav.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class SICAVMantenimientoOpQuery.
 */
@Component("SICAVOrdenanteCustodioIdQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SICAVOrdenanteCustodioIdQuery extends AbstractDynamicPageQuery {

  private static final String CDCLEARE = "Código";
  private static final String DSCLEARE = "Descripcion";
  
  /** The Constant SELECT. */
  private static final String SELECT = "SELECT DISTINCT "
      + "TRIM(CDCLEARE) AS CDCLEARE, "
      + "TRIM(DSCLEARE) AS DSCLEARE ";

  /** The Constant FROM. */
  private static final String FROM = "FROM TMCT0CLE ";

  /** The Constant WHERE. */
  private static final String WHERE = " WHERE FHFINAL >= TO_CHAR(CURRENT DATE, 'yyyymmdd') AND NUMSEC > 1";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn(CDCLEARE, "CDCLEARE"),
      new DynamicColumn(DSCLEARE, "DSCLEARE") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;
    res = new ArrayList<>();
    res.add(new StringDynamicFilter("CDCLEARE", CDCLEARE));
    res.add(new StringDynamicFilter("DSCLEARE", DSCLEARE));
    return res;
  }

  public SICAVOrdenanteCustodioIdQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public SICAVOrdenanteCustodioIdQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public SICAVOrdenanteCustodioIdQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}