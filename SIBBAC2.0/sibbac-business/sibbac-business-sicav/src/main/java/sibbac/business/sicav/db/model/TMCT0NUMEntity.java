
package sibbac.business.sicav.db.model;

import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import sibbac.database.DBConstants;

/**
 * The Class TMCT0NUMEntity.
 */
@Entity
@Table(name = DBConstants.SICAV.TMCT0NUM)
public class TMCT0NUMEntity {

  /** The id. */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TMCT0NUM_ID_SEQ")
  @SequenceGenerator(name = "TMCT0NUM_ID_SEQ", sequenceName = "TMCT0NUM_ID_SEQ")
  @Column(name = "ID", nullable = false, length = 19)
  private BigInteger id;

  /** The idope. */
  @Column(name = "ID_OPE", nullable = false, length = 10)
  private Integer idope;

  /** The opsec1. */
  @Column(name = "OPSEC1", nullable = true, length = 15)
  private String opsec1;

  /** The opsec2. */
  @Column(name = "OPSEC2", nullable = true, length = 15)
  private String opsec2;

  /** The optitu. */
  @Column(name = "OPTITU", nullable = false, length = 10)
  private String optitu;

  /** The nuidre. */
  @Column(name = "NUIDRE", nullable = true, length = 16)
  private String nuidre;

  /** The fechaalta. */
  @Column(name = "FECHA_ALTA", nullable = false, length = 26)
  private Timestamp fechaalta;

  /** The fechamod. */
  @Column(name = "FECHA_MOD", nullable = true, length = 26)
  private Timestamp fechamod;

  /** The fechabaja. */
  @Column(name = "FECHA_BAJA", nullable = true, length = 26)
  private Timestamp fechabaja;

  /** The usuarioalta. */
  @Column(name = "USUARIO_ALTA", nullable = false, length = 25)
  private String usuarioalta;

  /** The usuariomod. */
  @Column(name = "USUARIO_MOD", nullable = true, length = 25)
  private String usuariomod;

  /** The usuariobaja. */
  @Column(name = "USUARIO_BAJA", nullable = true, length = 25)
  private String usuariobaja;

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public Integer getIdope() {
    return idope;
  }

  public void setIdope(Integer idope) {
    this.idope = idope;
  }

  public String getOpsec1() {
    return opsec1;
  }

  public void setOpsec1(String opsec1) {
    this.opsec1 = opsec1;
  }

  public String getOpsec2() {
    return opsec2;
  }

  public void setOpsec2(String opsec2) {
    this.opsec2 = opsec2;
  }

  public String getOptitu() {
    return optitu;
  }

  public void setOptitu(String optitu) {
    this.optitu = optitu;
  }

  public String getNuidre() {
    return nuidre;
  }

  public void setNuidre(String nuidre) {
    this.nuidre = nuidre;
  }

  public Timestamp getFechaalta() {
    return fechaalta;
  }

  public void setFechaalta(Timestamp fechaalta) {
    this.fechaalta = fechaalta;
  }

  public Timestamp getFechamod() {
    return fechamod;
  }

  public void setFechamod(Timestamp fechamod) {
    this.fechamod = fechamod;
  }

  public Timestamp getFechabaja() {
    return fechabaja;
  }

  public void setFechabaja(Timestamp fechabaja) {
    this.fechabaja = fechabaja;
  }

  public String getUsuarioalta() {
    return usuarioalta;
  }

  public void setUsuarioalta(String usuarioalta) {
    this.usuarioalta = usuarioalta;
  }

  public String getUsuariomod() {
    return usuariomod;
  }

  public void setUsuariomod(String usuariomod) {
    this.usuariomod = usuariomod;
  }

  public String getUsuariobaja() {
    return usuariobaja;
  }

  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }

}
