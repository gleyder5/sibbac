package sibbac.business.sicav.bo.dto;

import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * The Class TMCT0NUMDTO.
 */
public class TMCT0NUMDTO extends AbstractDTO {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4442068969616818065L;

  /** The id. */
  private BigInteger id;

  /** The idope. */
  private Integer idope;

  /** The opsec 1. */
  private String opsec1;

  /** The opsec 2. */
  private String opsec2;

  /** The optitu. */
  private String optitu;

  /** The nuidre. */
  private String nuidre;

  /** The fechaalta. */
  private Timestamp fechaalta;

  /** The fechamod. */
  private Timestamp fechamod;

  /** The fechabaja. */
  private Timestamp fechabaja;

  /** The usuarioalta. */
  private String usuarioalta;

  /** The usuariomod. */
  private String usuariomod;

  /** The usuariobaja. */
  private String usuariobaja;

  /**
   * Gets the id.
   *
   * @return the id
   */
  public BigInteger getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(BigInteger id) {
    this.id = id;
  }

  /**
   * Gets the idope.
   *
   * @return the idope
   */
  public Integer getIdope() {
    return idope;
  }

  /**
   * Sets the idope.
   *
   * @param idope the new idope
   */
  public void setIdope(Integer idope) {
    this.idope = idope;
  }

  /**
   * Gets the opsec 1.
   *
   * @return the opsec 1
   */
  public String getOpsec1() {
    return opsec1;
  }

  /**
   * Sets the opsec 1.
   *
   * @param opsec1 the new opsec 1
   */
  public void setOpsec1(String opsec1) {
    this.opsec1 = opsec1;
  }

  /**
   * Gets the opsec 2.
   *
   * @return the opsec 2
   */
  public String getOpsec2() {
    return opsec2;
  }

  /**
   * Sets the opsec 2.
   *
   * @param opsec2 the new opsec 2
   */
  public void setOpsec2(String opsec2) {
    this.opsec2 = opsec2;
  }

  /**
   * Gets the optitu.
   *
   * @return the optitu
   */
  public String getOptitu() {
    return optitu;
  }

  /**
   * Sets the optitu.
   *
   * @param optitu the new optitu
   */
  public void setOptitu(String optitu) {
    this.optitu = optitu;
  }

  /**
   * Gets the nuidre.
   *
   * @return the nuidre
   */
  public String getNuidre() {
    return nuidre;
  }

  /**
   * Sets the nuidre.
   *
   * @param nuidre the new nuidre
   */
  public void setNuidre(String nuidre) {
    this.nuidre = nuidre;
  }

  /**
   * Gets the fechaalta.
   *
   * @return the fechaalta
   */
  public Timestamp getFechaalta() {
    return fechaalta;
  }

  /**
   * Sets the fechaalta.
   *
   * @param fechaalta the new fechaalta
   */
  public void setFechaalta(Timestamp fechaalta) {
    this.fechaalta = fechaalta;
  }

  /**
   * Gets the fechamod.
   *
   * @return the fechamod
   */
  public Timestamp getFechamod() {
    return fechamod;
  }

  /**
   * Sets the fechamod.
   *
   * @param fechamod the new fechamod
   */
  public void setFechamod(Timestamp fechamod) {
    this.fechamod = fechamod;
  }

  /**
   * Gets the fechabaja.
   *
   * @return the fechabaja
   */
  public Timestamp getFechabaja() {
    return fechabaja;
  }

  /**
   * Sets the fechabaja.
   *
   * @param fechabaja the new fechabaja
   */
  public void setFechabaja(Timestamp fechabaja) {
    this.fechabaja = fechabaja;
  }

  /**
   * Gets the usuarioalta.
   *
   * @return the usuarioalta
   */
  public String getUsuarioalta() {
    return usuarioalta;
  }

  /**
   * Sets the usuarioalta.
   *
   * @param usuarioalta the new usuarioalta
   */
  public void setUsuarioalta(String usuarioalta) {
    this.usuarioalta = usuarioalta;
  }

  /**
   * Gets the usuariomod.
   *
   * @return the usuariomod
   */
  public String getUsuariomod() {
    return usuariomod;
  }

  /**
   * Sets the usuariomod.
   *
   * @param usuariomod the new usuariomod
   */
  public void setUsuariomod(String usuariomod) {
    this.usuariomod = usuariomod;
  }

  /**
   * Gets the usuariobaja.
   *
   * @return the usuariobaja
   */
  public String getUsuariobaja() {
    return usuariobaja;
  }

  /**
   * Sets the usuariobaja.
   *
   * @param usuariobaja the new usuariobaja
   */
  public void setUsuariobaja(String usuariobaja) {
    this.usuariobaja = usuariobaja;
  }

}
