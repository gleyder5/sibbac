package sibbac.business.sicav.db.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.database.AbstractDynamicPageQuery;
import sibbac.database.DynamicColumn;
import sibbac.database.SimpleDynamicFilter;
import sibbac.database.StringDynamicFilter;

/**
 * The Class SICAVMantenimientoOpQuery.
 */
@Component("SICAVIsinIdQuery")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SICAVIsinIdQuery extends AbstractDynamicPageQuery {

  private static final String CDISINVV = "Código";
  private static final String NBTITREV = "Descripcion";

  /** The Constant SELECT. */
  private static final String SELECT = "SELECT DISTINCT "
      + "TRIM(CDISINVV) AS CDISINVV, "
      + "TRIM(NBTITREV) AS NBTITREV ";

  /** The Constant FROM. */
  private static final String FROM = "FROM TMCT0_MAESTRO_VALORES ";

  /** The Constant WHERE. */
  private static final String WHERE = "";

  /** The Constant COLUMNS. */
  private static final DynamicColumn[] COLUMNS = { new DynamicColumn(CDISINVV, "CDISINVV"),
      new DynamicColumn(NBTITREV, "NBTITREV") };

  private static List<SimpleDynamicFilter<?>> makeFilters() {
    final List<SimpleDynamicFilter<?>> res;
    res = new ArrayList<>();
    res.add(new StringDynamicFilter("CDISINVV", CDISINVV));
    res.add(new StringDynamicFilter("NBTITREV", NBTITREV));
    return res;
  }

  public SICAVIsinIdQuery() {
    super(makeFilters(), Arrays.asList(COLUMNS));
  }

  public SICAVIsinIdQuery(List<DynamicColumn> columns) {
    super(makeFilters(), columns);
  }

  public SICAVIsinIdQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    super(filters, columns);
  }

  @Override
  public String getSelect() {
    return SELECT;
  }

  @Override
  public String getFrom() {
    return FROM;
  }

  @Override
  public String getWhere() {
    return WHERE;
  }

  @Override
  public String getPostWhereConditions() {
    return "";
  }

  @Override
  public String getGroup() {
    return "";
  }

  @Override
  public String getHaving() {
    return "";
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getDynamicAvailableFilters() {
    return getAppliedFilters();
  }

  @Override
  protected List<SimpleDynamicFilter<?>> getStaticAvailableFilters() {
    return new ArrayList<SimpleDynamicFilter<?>>();
  }

  @Override
  protected void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException {

  }
}