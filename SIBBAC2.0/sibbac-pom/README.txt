

________________________________________________________________________________________________
[SECURITY]
HTTP Headers to be set to bypass security:
	Name	NewUniversalToken
	Value	AC1F60F3654A1E0A767DC212#172.31.92.9#1419335265182#3600000#1419331729182#64000#PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSI/Pjxjb29raWU+PGRlZmluaXRpb25OYW1lPk5ld1VzZXJQYXNzd29yZENvb2tpZTwvZGVmaW5pdGlvbk5hbWU+PG5hbWU+RklHVUVST0EgTUFSVElOLUJVSVRSQUdPLEFMVkFSTzwvbmFtZT48dXNlcm5hbWU+bjQwMTIzPC91c2VybmFtZT48dXNlcklEPm40MDEyMzwvdXNlcklEPjwvY29va2llPg==#DESede/CBC/PKCS5Padding#v1#Isban_PRO#NOT USED#SHA1withRSA#Zkgd4YxY/wVTZQV5R2oN0jEDR/lHl41/wXVZdhDTcxwb933yKDM3ZdADHLsmxSxhfNEyWlKREzbZ+WaysKkp1BWzzdXhFH9IJ52h9NC4qmhZ3v4i+NMBsFfpRmTB7+R5sGCf+zu0DIGjLzRItpFldt8kcgVFLROhljJ/t1JIio8=

	Name	SIBBAC20UserCookie
	Value	AC1F60F3654A1E0A767DC212#172.31.92.9#1419335265182#3600000#1419331729182#64000#PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iSVNPLTg4NTktMSI/Pjxjb29raWU+PGRlZmluaXRpb25OYW1lPk5ld1VzZXJQYXNzd29yZENvb2tpZTwvZGVmaW5pdGlvbk5hbWU+PG5hbWU+RklHVUVST0EgTUFSVElOLUJVSVRSQUdPLEFMVkFSTzwvbmFtZT48dXNlcm5hbWU+bjQwMTIzPC91c2VybmFtZT48dXNlcklEPm40MDEyMzwvdXNlcklEPjwvY29va2llPg==#DESede/CBC/PKCS5Padding#v1#Isban_PRO#NOT USED#SHA1withRSA#Zkgd4YxY/wVTZQV5R2oN0jEDR/lHl41/wXVZdhDTcxwb933yKDM3ZdADHLsmxSxhfNEyWlKREzbZ+WaysKkp1BWzzdXhFH9IJ52h9NC4qmhZ3v4i+NMBsFfpRmTB7+R5sGCf+zu0DIGjLzRItpFldt8kcgVFLROhljJ/t1JIio8=



________________________________________________________________________________________________
[Software a instalar]
MySQL y configurar
Apache Maven y configurar
Apache Ant y configurar
https://svn.vectorsf.com/repos/bto_j2ee/sibbac/trunk/SIBBAC2.0

________________________________________________________________________________________________
[Driver DB2]
Es necesario incorporar el driver DB2 al repositorio Maven:

[SIBBAC 2.0 Workspace]\sibbac-pom\lib\db2jcc.jar
[SIBBAC 2.0 Workspace]\sibbac-pom\lib\db2jcc_license_cu.jar

mvn install:install-file -Dfile=libs\db2jcc4.jar -DgroupId=com.ibm.db2 -DartifactId=db2jcc -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\db2jcc_license_cu.jar -DgroupId=com.ibm.db2 -DartifactId=db2jcc_license_cu -Dversion=1.0.0 -Dpackaging=jar

JMS Obtenidas del WAS
mvn install:install-file -Dfile=libs\com.ibm.mq.pcf.jar -DgroupId=com.ibm.mq -DartifactId=pcf -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\com.ibm.mq.headers.jar -DgroupId=com.ibm.mq -DartifactId=headers -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\com.ibm.mq.jar -DgroupId=com.ibm.mq -DartifactId=mq -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\com.ibm.mq.commonservices.jar -DgroupId=com.ibm.mq -DartifactId=commonservices -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\com.ibm.mq.jmqi.jar -DgroupId=com.ibm.mq -DartifactId=jmqi -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\com.ibm.mqjms.jar -DgroupId=com.ibm.mq -DartifactId=mqjms -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=libs\dhbcore.jar -DgroupId=com.ibm.mq -DartifactId=dhbcore -Dversion=1.0.0 -Dpackaging=jar

Luego, en los "POM"'s, se usa as�:
<dependency><groupId>com.ibm.db2</groupId><artifactId>driver</artifactId><version>1.0.0</version></dependency>
<dependency><groupId>com.ibm.db2</groupId><artifactId>license</artifactId><version>1.0.0</version></dependency>

<dependency><groupId>com.ibm.db2</groupId><artifactId>db2jcc</artifactId><version>1.0.0</version></dependency>
<dependency><groupId>com.ibm.db2</groupId><artifactId>db2jcc_license_cu</artifactId><version>1.0.0</version></dependency>

mvn install:install-file -Dfile=libs\fixml-impl-1.0.jar -DgroupId=com.vector.isban -DartifactId=fixml-impl -Dversion=1.0 -Dpackaging=jar
<dependency><groupId>com.vector.isban</groupId><artifactId>fixml-impl</artifactId><version>1.0</version></dependency>

Dependencias POI
mvn install:install-file -Dfile=libs\xmlbeans-2.6.0.jar -DgroupId=org.apache.xmlbeans -DartifactId=xmlbeans -Dversion=2.6.0 -Dpackaging=jar

Dependencias JAXB Fidessa
mvn install:install-file -Dfile=libs\multicenter.jar -DgroupId=com.sv.sibbac -DartifactId=multicenter -Dversion=1.0.0 -Dpackaging=jar
<dependency><groupId>com.sv.sibbac</groupId><artifactId>multicenter</artifactId><version>1.0.0</version></dependency>
________________________________________________________________________________________________
[Para el setup de la base de datos en local (MySQL)]
REM Comandos como ROOT:
REM ------------------------
MySQL -uroot -p<root_password>
create database sibbac;
GRANT ALL PRIVILEGES ON sibbac.* TO 'sibbac'@'localhost' IDENTIFIED BY 'sibbac' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON sibbac.* TO 'sibbac'@'%' IDENTIFIED BY 'sibbac' WITH GRANT OPTION;
FLUSH PRIVILEGES;
exit;

REM Comandos como DBUser:
REM ------------------------
MySQL -usibbac -psibbac sibbac
exit;


________________________________________________________________________________________________
[Para el setup de la base de datos en MARPE/ISBAN (DB2)]
REM jdbc.driverClassName			= com.ibm.db2.jcc.DB2Driver
REM jdbc.url				= jdbc:db2://bvmngludsbd01.nngg.corp:60000/UDNGSBAC:currentSchema=BSNBPSQL;
REM jdbc.password				= T123est!
REM jdbc.username				= sibbac


________________________________________________________________________________________________
[Para el setup del env�o de correos]
SET MAVEN_REPO=D:\DESARROLLO\MavenRepo
SET TOMCAT_HOME=C:\Program Files\Apache Software Foundation\Tomcat 8.0
copy "%MAVEN_REPO%\javax\activation\activation\1.1\activation-1.1.jar" "%TOMCAT_HOME%\lib"
copy "%MAVEN_REPO%\javax\mail\mail\1.5.0-b01\mail-1.5.0-b01.jar" "%TOMCAT_HOME%\lib"


________________________________________________________________________________________________
[Para el setup de LogBack sobre Tomcat]
D:\DESARROLLO\MavenRepo\ch\qos\logback\logback-classic\1.0.13\logback-classic-1.0.13.jar, D:\DESARROLLO\MavenRepo\ch\qos\logback\logback-core\1.0.13\logback-core-1.0.13.jar
copy "%MAVEN_REPO%\ch\qos\logback\logback-classic\1.0.13\logback-classic-1.0.13.jar" "%TOMCAT_HOME%\lib"
copy "%MAVEN_REPO%\ch\qos\logback\logback-core\1.0.13\logback-core-1.0.13.jar" "%TOMCAT_HOME%\lib"



________________________________________________________________________________________________
[Para las ventanas de comando MS-DOS]

CD /D "D:\DESARROLLO\EclipseSIBBAC"
TITLE Eclipse SIBBAC
COLOR 0f


CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0"
TITLE Eclipse SIBBAC 2.0
COLOR 0f

CD /D "D:\temp\SIBBAC\SVN\sibbac-pom"
TITLE EAR Deployment
COLOR 2f





[SIBBAC Principal]

CD /D "D:\DESARROLLO\EclipseSIBBAC\"
TITLE SIBBAC
COLOR 1f

REM -- Actualizar el SVN...
ECHO Off && CLS
FOR /D %f IN (*.*) DO (
CD "%f"
ECHO SVN Updating %CD%\%f...
SVN update .
CD ..
)
ECHO On






[SIBBAC POM Principal]

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-pom"
TITLE POM
COLOR 1f

-- Actualizar el SVN...
CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0"
svn update .
CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-pom"

-- Compilar contra local
CLS && CALL MVN -Dmaven.test.skip=true -l Maven.log clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log clean install

-- Compilar contra MIGRACION / UDNGSBAC
CLS && CALL MVN -Dmaven.test.skip=true -l Maven.log -P db2 clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log -P db2 clean install

-- Compilar contra BSNBPSQL
CLS && CALL MVN -Dmaven.test.skip=true -l Maven.log -P isban clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log -P isban clean install

-- ============================================================================================
-- Compilar VALID
-- ============================================================================================
CLS && CALL MVN -Dmaven.test.skip=true -l Maven.log clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log clean install

-- Carpetas
Explorer ..
Explorer "C:\Program Files\Apache Software Foundation\Tomcat 8.0\logs"
Explorer "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\"
Explorer "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-web\sibbac-html\src\main"
Explorer C:\servicios\was\sbrmv\applogs
Explorer D:\servicios\was\sbrmv\applogs

Explorer C:\sbrmv\ficheros\conciliacion\in\
Explorer D:\sbrmv\ficheros\conciliacion\in\

SET WKS=D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0
"C:\Program Files (x86)\WinMerge\WinMergeU.exe" /r /s /dl HTML "%WKS%\sibbac-web\sibbac-html\src\main\webapp\" /dr Deployed "%WKS%\sibbac-web\sibbac-html\target\sibbac-html-4.0.0\"
"C:\Program Files (x86)\WinMerge\WinMergeU.exe" /r /s /dl Pro ".." /dr EAR "D:\Temp\SIBBAC\SVN"



[TOMCAT]

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0"
TITLE Tomcat 8
COLOR 5f
Explorer "C:\Program Files\Apache Software Foundation\Tomcat 8.0\logs"
Explorer C:\servicios\was\sbrmv\applogs

-- Tomcat
CLS && Tomcat.Clean.cmd
CLS && Tomcat.Run.cmd
CLS && NET STOP Tomcat8



CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log -Dtest=TestAjkuste clean test
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log -Dtest=TestSaldoInicialCuenta#generarSaldoPorFecha clean test



[READ File as Tail]
http://www.studytrails.com/java-io/url-reading-tailing-reverse-reading.jsp



[SIBBAC Base]

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-api"
TITLE BASE API
COLOR 1f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-common"
TITLE BASE Common
COLOR 2f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-config"
TITLE BASE Config
COLOR 3f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-database"
TITLE BASE Database
COLOR 4f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-language"
TITLE BASE Language
COLOR 5f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-pti"
TITLE BASE PTI
COLOR 6f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-fixml"
TITLE BASE FIXML
COLOR 5f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-tasks"
TITLE BASE Tasks
COLOR 5f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-base\sibbac-test"
TITLE BASE Test
COLOR 8f


[SIBBAC Business]

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-conciliacion"
TITLE Business Conciliacion
COLOR 1e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-contabilidad"
TITLE Business Contabilidad
COLOR 2e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-facturacion"
TITLE Business Facturacion
COLOR 3e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-fallidos"
TITLE Business Fallidos
COLOR 4e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-fase0"
TITLE Business Fase 0
COLOR 5e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-fixml"
TITLE Business FIXML
COLOR 6e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-periodicidades"
TITLE Business Periodicidades
COLOR 7e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-sample"
TITLE Business Sample
COLOR 8e

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business\sibbac-business-wrappers"
TITLE Business Wrappers
COLOR 4f




[SIBBAC Web]

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-web\sibbac-admin"
TITLE Web Admin
COLOR 1d

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-web\sibbac-security"
TITLE Web Security
COLOR 2d

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-web\sibbac-webapp"
TITLE Web Webapp
COLOR 2f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-web\sibbac-webapp"
TITLE Web Webapp Jetty:start
COLOR 4f

CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-web\sibbac-webapp"
TITLE Web Webapp Jetty:stop
COLOR 5d





________________________________________________________________________________________________
[Para crear un "proyecto de negocio"]

REM Iniciales: contabilidad facturacion fase0 periodicidades sample
REM Creamos estructura
CD /D "D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-business"
CLS
FOR %F IN (
contabilidad
facturacion
fase0
periodicidades
sample
) DO (
MKDIR "sibbac-business-%F"
COPY pomForChildren.xml "sibbac-business-%F\pom.xml"
CHDIR "sibbac-business-%F"
MKDIR "src\main\java\sibbac\business\%F\database\model"
MKDIR "src\main\java\sibbac\business\%F\database\dao"
MKDIR "src\main\java\sibbac\business\%F\database\bo"
MKDIR "src\main\java\sibbac\business\%F\tasks\"
MKDIR "src\main\java\sibbac\business\%F\rest\"
MKDIR "src\test\java\sibbac\business\%F\"
MKDIR "src\main\resources"
MKDIR "src\test\resources"
TREE
CHDIR ..
)
TREE /F | more


________________________________________________________________________________________________
[***WARNING*** Tener cuidado con este comando]
-- ***** La idea es compilar todo de scratch. *****
-- Localizamos el repositorio Maven local
-- (se puede ver en el "settings.xml" de Maven, si as� lo hemos configurado,
-- o en el: "~/.m2/repo" (Linus) / "%USERPROFILE%\.m2\[repository]"
-- Actualizamos la variable con la informaci�n de cada uno.
SET MAVEN_REPO=D:\DESARROLLO\MavenRepo
-- Nos situamos en la carpeta (Windows ONLY)
CD /D "%MAVEN_REPO%\com\vector\isban"
-- Eliminamos TODO.
RD /S /Q .


________________________________________________________________________________________________
[Para todo el proyecto completo]
-- El POM efectivo.
CLS && CALL MVN -X -e -U -l Maven.EffectivePOM.log help:effective-pom

-- Download sources and javadocs
CLS && CALL MVN -U -up -X -e -l Maven.Javadoc.log dependency:resolve -Dclassifier=javadoc
CLS && CALL MVN -U -up -X -e -l Maven.Sources.log dependency:resolve -Dclassifier=sources

-- Mostrar las dependencias y actualizaciones disponibles.
CLS && CALL MVN -X -e -U -l Maven.Versions.log versions:display-dependency-updates

-- Compilar y probar
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log test
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log clean test
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log -Dtest=<testname> clean test

-- Compilar y empaquetar, sin tests
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -Dmaven.test.skip=true -l Maven.log clean package
CLS && CALL MVN -Dmaven.test.skip=true clean package

-- Compilar e instalar
CLS && CALL MVN -l Maven.log clean install
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log clean install

-- Compilar e instalar, sin ejecutar los tests
CLS && CALL MVN -Dmaven.test.skip=true clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log clean install

-- Compilar e instalar, pero probando contra el DB2 de Vector
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -Denv=vsf -l Maven.log clean install

-- Preparar JAR's/EAR's con sources
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log clean assembly:directory

-- Lanzar Jetty
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log jetty:start
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -l Maven.log jetty:run

-- Lanzar Jetty compilando primero
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -Dmaven.test.skip=true -l Maven.log clean compile package jetty:start
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -Dmaven.test.skip=true -l Maven.log clean compile package jetty:run
CLS && CALL MVN -X -e -U -Dlog4j.debug=true -Dmaven.test.skip=true -l Maven.log jetty:run

-- Lanzar Poblar la base de datos
CLS && mysql -usibbac -psibbac sibbac < D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-webapp\src\test\resources\import.sql

-- Parar Jetty
CLS && CALL MVN jetty:stop

-- Generar todo el site completo (desde "sibbac-pom")
CLS && CALL MVN -X -e -U -am -amd -l Maven.log -Dlog4j.debug=true clean validate initialize generate-sources process-sources generate-resources process-resources compile process-classes generate-test-sources process-test-sources generate-test-resources process-test-resources test-compile process-test-classes test prepare-package package install source:jar source:test-jar javadoc:javadoc javadoc:jar site site:stage site:jar

CLS && CALL MVN -X -e -U -am -amd -l Maven.log -Dlog4j.debug=true clean validate initialize generate-sources process-sources generate-resources process-resources compile process-classes generate-test-sources process-test-sources generate-test-resources process-test-resources test-compile process-test-classes test prepare-package package install source:jar source:test-jar site site:stage site:jar



-- Generar el EAR contra ISBAN
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log -P db2 clean install
CLS && CALL MVN -X -e -U -Dmaven.test.skip=true -l Maven.log -P isban clean install



MVN -U -up -X -e help:describe -Ddetail -DgroupId=org.eclipse.jetty -DartifactId=jetty-maven-plugin -Dversion=9.3.0.M2 -Dmojo=start
MVN -U -up -X -e help:describe -Ddetail -DgroupId=org.eclipse.jetty -DartifactId=jetty-maven-plugin -Dversion=9.3.0.M2 -Dmojo=run
MVN -U -up -X -e -l Maven.log help:describe -Ddetail -DgroupId=org.eclipse.jetty -DartifactId=jetty-maven-plugin -Dversion=9.3.0.M2 -Dmojo=start
MVN -U -up -X -e -l Maven.log help:describe -Ddetail -DgroupId=org.eclipse.jetty -DartifactId=jetty-maven-plugin -Dversion=9.3.0.M2 -Dmojo=run



________________________________________________________________________________________________
[Spring configuration files]
[<Launchers>]
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-config\src\test\resources\spring.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-database\src\test\resources\spring.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-pti\src\test\resources\spring.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-tasks\src\test\resources\spring.xml

[<Configuration>]
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-config\src\main\resources\spring\spring.config.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-database\src\main\resources\spring\spring.database.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-pti\src\main\resources\spring\spring.pti.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-tasks\src\main\resources\spring\spring.tasks.xml

[<Admin WebApp>]
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-admin\src\main\webapp\WEB-INF\spring.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-admin\src\main\webapp\WEB-INF\spring\spring.web.xml

[<Main WebApp>]
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-webapp\src\main\webapp\WEB-INF\spring.security.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-webapp\src\main\webapp\WEB-INF\spring.xml
D:\DESARROLLO\EclipseSIBBAC\SIBBAC2.0\sibbac-webapp\src\main\webapp\WEB-INF\spring\spring.web.xml



________________________________________________________________________________________________
[Spring View Resolvers]
http://websystique.com/springmvc/spring-4-mvc-contentnegotiatingviewresolver-example/
http://theblasfrompas.blogspot.com.es/2013/10/spring-mvc-rest-content-negotiation.html
http://spring.io/blog/2013/05/11/content-negotiation-using-spring-mvc/
http://spring.io/blog/2013/06/03/content-negotiation-using-views



Hibernate Annotations:
http://docs.jboss.org/hibernate/annotations/3.5/reference/en/html/
http://docs.jboss.org/hibernate/annotations/3.5/javadoc/

Hibernate ORM (with XML):
http://docs.jboss.org/hibernate/orm/4.3/manual/en-US/html/
http://docs.jboss.org/hibernate/orm/4.3/devguide/en-US/html/
http://docs.jboss.org/hibernate/orm/4.3/javadocs/

Spring DATA:
http://projects.spring.io/spring-data-jpa/
http://docs.spring.io/spring-data/commons/docs/current/api/index.html
http://docs.spring.io/spring-data/commons/docs/current/reference/html/
http://docs.spring.io/spring-data/jpa/docs/current/reference/html/
http://docs.spring.io/spring-data/jpa/docs/current/api/
[Criteria/Predicates]
http://www.petrikainulainen.net/programming/spring-framework/spring-data-jpa-tutorial-part-four-jpa-criteria-queries/
http://spring.io/blog/2011/04/26/advanced-spring-data-jpa-specifications-and-querydsl/
http://www.javacodegeeks.com/2013/04/jpa-2-0-criteria-query-with-hibernate.html


Properties in Spring:
http://www.baeldung.com/2012/02/06/properties-with-spring/

Quartz:
http://stackoverflow.com/questions/21791853/how-are-spring-taskscheduled-objects-represented-at-runtime
http://quartz-scheduler.org/generated/2.2.1/html/qs-all/
http://www.mkyong.com/java/how-to-list-all-jobs-in-the-quartz-scheduler/
http://www.mkyong.com/java/example-to-run-multiple-jobs-in-quartz/
http://alexshabanov.com/2013/01/28/quartzspring-scheduling-on-demand-sample/

CORS @ Spring MVC:
https://gist.github.com/zeroows/80bbe076d15cb8a4f0ad
https://htet101.wordpress.com/2014/01/22/cors-with-angularjs-and-spring-rest/
https://quizzpot.com/forum/questions/angular-solicitud-de-origen-distinto-bloqueada

JPA:
http://en.wikibooks.org/wiki/Java_Persistence/OneToMany
http://en.wikibooks.org/wiki/Java_Persistence/ManyToOne
http://docs.jboss.org/hibernate/orm/4.3/manual/en-US/html/
http://docs.jboss.org/hibernate/orm/4.3/javadocs/
http://docs.jboss.org/hibernate/jpa/2.1/api/

Spring/JNDI:
http://www.journaldev.com/2597/spring-datasource-jndi-with-tomcat-example
http://stackoverflow.com/questions/9183321/how-to-use-jndi-datasource-provided-by-tomcat-in-spring

http://stackoverflow.com/questions/7690167/connection-pool-or-data-source-which-should-i-put-in-jndi
http://edwin.baculsoft.com/2012/08/setting-up-c3p0-connection-pooling-on-apache-tomcat/
http://stackoverflow.com/questions/3111992/difference-between-configuring-data-source-in-persistence-xml-and-in-spring-conf
http://docs.spring.io/spring/docs/3.0.x/spring-framework-reference/html/testing.html#mock-objects-jndi


JAXB:
http://stackoverflow.com/questions/26413431/which-artifacts-should-i-use-for-jaxb-ri-in-my-maven-project


MAIL:
http://docs.spring.io/autorepo/docs/spring/current/javadoc-api/index.html?org/springframework/mail/javamail/JavaMailSender.html
http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mail.html



LINKS Auxiliares:
http://180.106.132.70:9080/GestionLiquidaciones/consultaPrevisiones.htm
http://danielme.com/2013/06/19/persistencia-bd-con-spring-integrando-jpa-c3p0-hibernate-y-ehcache/
http://dev.w3.org/csswg/cssom/
http://docs.jboss.org/hibernate/annotations/3.5/api/
http://docs.jboss.org/hibernate/orm/4.3/devguide/en-US/html/ch15.html#envers-configuration
http://docs.jboss.org/tools/latest/en/hibernatetools/html_single/index.html#d0e4485
http://docs.jboss.org/tools/latest/en/hibernatetools/html_single/index.html#reverseengineering
http://docs.spring.io/spring/docs/current/javadoc-api/
http://docs.spring.io/spring/docs/current/spring-framework-reference/html/
http://download.oracle.com/javaee/7/api/
http://download.oracle.com/javase/7/docs/api/
http://download.oracle.com/javase/8/docs/api/
http://fasterxml.github.io/jackson-annotations/javadoc/2.5/
http://forum.spring.io/forum/spring-projects/data/49876-error-while-using-c3p0-datasource-through-jndi
http://hibernate.org/orm/envers/
http://howtodoinjava.com/2013/04/23/4-ways-to-schedule-tasks-in-spring-3-scheduled-example/
http://java.dzone.com/articles/integration-testing-spring
http://java.dzone.com/articles/multi-job-scheduling-service
http://maven.apache.org/plugins/maven-antrun-plugin/
http://maven.apache.org/plugins/maven-checkstyle-plugin/
http://maven.apache.org/plugins/maven-clean-plugin/
http://maven.apache.org/plugins/maven-compiler-plugin/
http://maven.apache.org/plugins/maven-deploy-plugin/
http://maven.apache.org/plugins/maven-eclipse-plugin/
http://maven.apache.org/plugins/maven-install-plugin/
http://maven.apache.org/plugins/maven-jar-plugin/
http://maven.apache.org/plugins/maven-javadoc-plugin/
http://maven.apache.org/plugins/maven-jxr-plugin/
http://maven.apache.org/plugins/maven-pmd-plugin/
http://maven.apache.org/plugins/maven-resources-plugin/
http://maven.apache.org/plugins/maven-site-plugin/
http://maven.apache.org/plugins/maven-source-plugin/
http://maven.apache.org/plugins/maven-surefire-plugin/
http://maven.apache.org/plugins/maven-war-plugin/
http://mojo.codehaus.org/cobertura-maven-plugin/s
http://mojo.codehaus.org/properties-maven-plugin/
http://mojo.codehaus.org/sql-maven-plugin/
http://mycuteblog.com/spring-task-scheduler-cron-job-example/
http://myjourneyonjava.blogspot.com.es/2015/01/spring-4-mvc-hibernate-4-mysql-5-maven.html
http://sizzlejs.com/
http://springinpractice.com/2012/05/11/pagination-and-sorting-with-spring-data-jpa
http://stackoverflow.com/questions/1555262/calculating-the-difference-between-two-java-date-instances
http://stackoverflow.com/questions/15720579/add-custom-beans-to-spring-context
http://stackoverflow.com/questions/3499597/javascript-jquery-to-download-file-via-post-with-json-data/8394118#8394118
http://stackoverflow.com/questions/5014651/webapproot-in-spring
http://stackoverflow.com/questions/5519066/possible-to-run-two-webapps-at-once-when-developing-with-maven-eclipse
http://stackoverflow.com/questions/7690167/connection-pool-or-data-source-which-should-i-put-in-jndi
http://stackoverflow.com/questions/7846103/customised-annotation-in-spring
http://techo-ecco.com/blog/spring-custom-annotations/
http://websystique.com/spring/spring-job-scheduling-using-xml-configuration/
http://www.baeldung.com/spring-scheduled-tasks
http://www.cronmaker.com/
http://www.eclipse.org/jetty/configure.dtd
http://www.eclipse.org/jetty/documentation/current/jetty-maven-plugin.html
http://www.hevi.info/2014/07/maven-3-hibernate-4-spring-3-ehcache-spring-cache/
http://www.mchange.com/projects/c3p0/#configuration_properties
http://www.mkyong.com/hibernate/hibernate-many-to-many-example-join-table-extra-column-annotation/
http://www.mkyong.com/java/java-custom-annotations-example/
http://www.mkyong.com/spring-batch/spring-batch-and-spring-taskscheduler-example/
http://www.quartz-scheduler.org/api/2.2.1/
http://www.quartz-scheduler.org/documentation/quartz-1.x/tutorials/crontrigger
http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
http://www.w3.org/TR/css3-selectors/#attribute-selectors
http://www.w3.org/TR/css3-selectors/#whitespace
http://www.w3.org/TR/css3-syntax/#characters
http://www.w3.org/TR/html4/loose.dtd
http://www.w3.org/TR/selectors/#attribute-selectors
http://www.w3.org/TR/selectors/#empty-pseudo
http://www.w3.org/TR/selectors/#lang-pseudo
http://www.w3.org/TR/selectors/#pseudo-classes
https://blog.safaribooksonline.com/2013/09/30/rest-hypermedia/


[i18n]
http://www.codingpedia.org/ama/spring-3-mvc-internationalization-localization-of-podcastpedia-org/
http://www.coderanch.com/t/549130/Spring/spring-internalization-working-spring-security
http://howtodoinjava.com/2013/07/26/internationalization-i18n-support-in-spring-3-example-tutorial/


Hack for static content on Jetty:
https://www.assembla.com/wiki/show/liftweb/Fix_file_locking_problem_with_jettyrun


[Return XML]
http://stackoverflow.com/questions/17685268/how-to-return-a-xml-file-with-spring-mvc
http://stackoverflow.com/questions/4856298/spring-mvc-3-returning-xml-through-responsebody
http://stackoverflow.com/questions/17916556/returning-xml-file-from-controller




[Basic info: Maven, Eclipse, JPA, JUnit y, por el mismo precio, JavaScript]
http://maven.apache.org/
http://maven.apache.org/users/index.html
http://maven.apache.org/run-maven/index.html
http://maven.apache.org/guides/index.html

http://www.chuidiang.com/java/herramientas/maven.php
http://www.javahispano.org/storage/contenidos/Tutorial_de_Maven_3_Erick_Camacho.pdf
http://www.notodocodigo.com/maven/maven-en-eclipse/

http://en.wikibooks.org/wiki/Java_Persistence/OneToMany
http://en.wikibooks.org/wiki/Java_Persistence/ManyToOne
http://docs.jboss.org/hibernate/orm/4.3/manual/en-US/html/
http://docs.jboss.org/hibernate/orm/4.3/javadocs/
http://docs.jboss.org/hibernate/jpa/2.1/api/

http://www.vogella.com/tutorials/JUnit/article.html
http://crunchify.com/simple-junit-4-tutorial-hello-world-example/

https://developer.mozilla.org/es/docs/Web/JavaScript/Introducci%C3%B3n_a_JavaScript_orientado_a_objetos
http://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/
http://www.javascriptkit.com/javatutors/oopjs.shtml
http://www.cristalab.com/tutoriales/programacion-orientada-a-objetos-oop-con-javascript-c232l/


Reload Properties in Spring:
http://www.wuenschenswert.net/wunschdenken/archives/127
http://forum.spring.io/forum/spring-projects/container/106157-reload-property-file-without-restarting-tomcat
https://docs.oracle.com/javase/tutorial/essential/io/notification.html

#ACTIVAR MULTIPLES PROFILES
#Por ejemplo activa perfiles isban y no-tasks
mvn clean package -DskipTests=true help:active-profiles -Pisban,no-tasks
Reload Properties in Spring:
http://www.wuenschenswert.net/wunschdenken/archives/127
http://forum.spring.io/forum/spring-projects/container/106157-reload-property-file-without-restarting-tomcat
https://docs.oracle.com/javase/tutorial/essential/io/notification.html
