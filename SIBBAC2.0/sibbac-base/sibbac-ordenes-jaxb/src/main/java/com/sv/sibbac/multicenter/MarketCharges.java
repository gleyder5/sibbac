//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.3 in JDK 1.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.04.06 at 02:51:38 PM CEST 
//


package com.sv.sibbac.multicenter;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketCharges complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketCharges">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cargos" type="{http://com.sv.sibbac/multicenter}Cargos"/>
 *         &lt;element name="Impuesto" type="{http://com.sv.sibbac/multicenter}Impuesto" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketCharges", namespace = "http://com.sv.sibbac/multicenter", propOrder = {
    "cargos",
    "impuesto"
})
public class MarketCharges {

    @XmlElement(name = "Cargos", required = true)
    protected Cargos cargos;
    @XmlElement(name = "Impuesto", required = false)
    protected List<Impuesto> impuesto;

    /**
     * Gets the value of the cargos property.
     * 
     * @return
     *     possible object is
     *     {@link Cargos }
     *     
     */
    public Cargos getCargos() {
        return cargos;
    }

    /**
     * Sets the value of the cargos property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cargos }
     *     
     */
    public void setCargos(Cargos value) {
        this.cargos = value;
    }

    /**
     * Gets the value of the impuesto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the impuesto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImpuesto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Impuesto }
     * 
     * 
     */
    public List<Impuesto> getImpuesto() {
        if (impuesto == null) {
            impuesto = new ArrayList<Impuesto>();
        }
        return this.impuesto;
    }

}
