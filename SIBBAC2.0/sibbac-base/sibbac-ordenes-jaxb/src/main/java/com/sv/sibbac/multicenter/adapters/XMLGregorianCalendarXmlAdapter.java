package com.sv.sibbac.multicenter.adapters;

import java.text.SimpleDateFormat;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class XMLGregorianCalendarXmlAdapter extends XmlAdapter<String, XMLGregorianCalendar> {


  @Override
  public String marshal(XMLGregorianCalendar v) throws Exception {
    if (v != null) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
      return dateFormat.format(v.toGregorianCalendar().getTime());
    }
    else {
      return null;
    }
  }

  @Override
  public XMLGregorianCalendar unmarshal(String v) throws Exception {
    if (v != null && !v.trim().isEmpty()) {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(v);
    }
    else {
      return null;
    }
  }

}
