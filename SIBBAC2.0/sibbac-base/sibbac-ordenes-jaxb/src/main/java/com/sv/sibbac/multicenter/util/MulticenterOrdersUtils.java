package com.sv.sibbac.multicenter.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBException;

import com.sv.sibbac.multicenter.Orders;

public class MulticenterOrdersUtils extends MulticenterUtils<Orders> {

	public Orders getOrders(InputStream is) throws JAXBException {
		return unmarshaller(is);
	}

	public String createOrders(InputStream is) throws JAXBException, IOException {
		return marshaller(getOrders(is));
	}

	public String createOrders(Orders orders) throws JAXBException, IOException {
		return marshaller(orders);
	}

	public InputStream createOrdersInputStream(Orders orders) throws JAXBException, IOException {
		try {
			return marshallerInputStream(orders);
		} catch (IOException e) {
			return marshallerInputStreamByTempFile(orders);
		}
	}

	public InputStream createOrdersGz(Orders orders) throws JAXBException, IOException {
		return marshallerInputStreamGz(orders);
	}

	public File createOrdersFileGz(Orders orders) throws JAXBException, IOException {
		return marshallerFileGz(orders);
	}

	public void createOrdersFileGz(Orders orders, File fileToCreate) throws JAXBException, IOException {
		marshallerFileGz(orders, fileToCreate);
	}

	public void createOrdersFile(Orders orders, File fileToCreate) throws JAXBException, IOException {
		marshallerFile(orders, fileToCreate);
	}

	public File createOrdersFile(Orders orders) throws JAXBException, IOException {
		return marshallerFile(orders);
	}

	public ByteArrayOutputStream createOrdersByteArrayOutputStream(Orders orders) throws JAXBException, IOException {
		return marshallerOutputStream(orders);
	}
	
	public ByteArrayOutputStream createOrdersByteArrayOutputStreamGz(Orders orders) throws JAXBException, IOException {
		return marshallerOutputStreamGz(orders);
	}

}
