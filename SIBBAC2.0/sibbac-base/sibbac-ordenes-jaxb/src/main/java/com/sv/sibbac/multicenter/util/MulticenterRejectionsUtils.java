package com.sv.sibbac.multicenter.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.sv.sibbac.multicenter.Rejection;

public class MulticenterRejectionsUtils extends MulticenterUtils<Rejection> {
	

	

	public String createRejection(Rejection rejection) throws JAXBException, IOException {
		return marshaller(rejection);
	}

	public String createRejection(String nuorder, String nbooking, String nucnfclt,
			String tradingVenue, String dsobserv) throws JAXBException, IOException {
		Rejection rejection = factory.createRejection();
		rejection.setTradingVenue(tradingVenue);
		rejection.setDsobserv(dsobserv);
		rejection.setNuorden(nuorder);
		rejection.setNbooking(nbooking);
		rejection.setNucnfclt(nucnfclt);
		return createRejection(rejection);
	}

	protected Rejection unmarshaller(InputStream is) throws JAXBException {
		Rejection unmarshal = (Rejection) JAXBContext.newInstance(Rejection.class).createUnmarshaller().unmarshal(is);
		try {
			if (is.available() == 0) {
				is.reset();
			}
		} catch (Exception e) {}
		return unmarshal;
	}

	public Rejection getRejection(InputStream is) throws JAXBException {
		return unmarshaller(is);
	}

	public Rejection getRejection(String s) throws JAXBException {
		return unmarshaller(new ByteArrayInputStream(s.getBytes()));
	}
}
