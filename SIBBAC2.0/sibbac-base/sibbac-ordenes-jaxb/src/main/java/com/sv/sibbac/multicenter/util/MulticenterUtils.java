package com.sv.sibbac.multicenter.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.util.zip.GZIPOutputStream;

import javax.annotation.PostConstruct;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.sv.sibbac.multicenter.ObjectFactory;
import com.sv.sibbac.multicenter.validation.MulticenterValidation;

public class MulticenterUtils<T> {

  private static boolean init = false;

  private static Schema schema;
  private static final Logger LOG = LoggerFactory.getLogger(MulticenterUtils.class);

  private boolean validateFormat = true, appendFile = true;
  private String error = "";
  private MulticenterValidation validation;

  protected ObjectFactory factory = new ObjectFactory();

  protected MulticenterUtils() {
    validation = new MulticenterValidation();
  }

  protected MulticenterValidation getValidation() {
    return validation;
  }

  private static void setProperties(Marshaller marshaller) {

    try {
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    }
    catch (JAXBException e) {
      LOG.error(e.getMessage(), e);
    }

  }

  @PostConstruct
  public static void init() {
    if (!init || schema == null) {
      try {

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        URL url = MulticenterUtils.class.getResource("/multicenter.xsd");
        schema = schemaFactory.newSchema(url);
        init = true;

      }
      catch (SAXException e) {
        LOG.error(e.getMessage(), e);
      }
    }
  }

  protected T unmarshaller(InputStream is) throws JAXBException {
    return unmarshaller2(is);
  }

  @SuppressWarnings("unchecked")
  protected T unmarshaller2(InputStream is) throws JAXBException {
    JAXBContext context = JAXBContext.newInstance("com.sv.sibbac.multicenter", MulticenterUtils.class.getClassLoader());
    Unmarshaller unmarshaller = context.createUnmarshaller();
    if (this.validateFormat) {
      unmarshaller.setSchema(schema);
      unmarshaller.setEventHandler(validation);
    }

    try {
      if (is.available() == 0) {
        is.reset();
      }
    }
    catch (Exception e) {
    }
    long timeIni = System.currentTimeMillis();
    T unmarshal = (T) unmarshaller.unmarshal(is);
    if (LOG != null) {
      long timeEnd = System.currentTimeMillis();
      LOG.debug("Unmarshal in " + (timeEnd - timeIni) + " ms.");
    }
    try {
      if (is.available() == 0) {
        is.reset();
      }
    }
    catch (Exception e) {
    }
    return unmarshal;
  }

  protected String marshaller(T objects) throws JAXBException, IOException {
    StringWriter sw = new StringWriter();
    marschaller(objects, sw);
    return sw.toString();
  }

  private void marschaller(T objects, OutputStream os) throws JAXBException, IOException {
    long timeIni = System.currentTimeMillis();
    JAXBContext context = JAXBContext.newInstance("com.sv.sibbac.multicenter", MulticenterUtils.class.getClassLoader());
    Marshaller marshaller = context.createMarshaller();
    setProperties(marshaller);
    marshaller.marshal(objects, os);
    os.flush();
    if (LOG != null) {
      long timeEnd = System.currentTimeMillis();
      LOG.debug("Marshal in " + (timeEnd - timeIni) + " ms.");
    }
  }

  private void marschaller(T objects, Writer w) throws JAXBException, IOException {
    long timeIni = System.currentTimeMillis();
    JAXBContext context = JAXBContext.newInstance("com.sv.sibbac.multicenter", MulticenterUtils.class.getClassLoader());
    Marshaller marshaller = context.createMarshaller();
    setProperties(marshaller);
    marshaller.marshal(objects, w);
    w.flush();
    if (LOG != null) {
      long timeEnd = System.currentTimeMillis();
      LOG.debug("Marshal in " + (timeEnd - timeIni) + " ms.");
    }
  }

  protected ByteArrayOutputStream marshallerOutputStream(T objects) throws JAXBException, IOException {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    marschaller(objects, os);
    return os;
  }

  protected ByteArrayInputStream marshallerByteInputStream(T objects) throws JAXBException, IOException {
    return new ByteArrayInputStream(marshallerOutputStream(objects).toByteArray());
  }

  protected ByteArrayOutputStream marshallerOutputStreamGz(T objects) throws JAXBException, IOException {
    ByteArrayOutputStream baosGz = new ByteArrayOutputStream();
    GZIPOutputStream gzos = new GZIPOutputStream(baosGz);
    marschaller(objects, gzos);
    gzos.close();
    return baosGz;
  }

  protected InputStream marshallerInputStream(T objects) throws JAXBException, IOException {
    return new ByteArrayInputStream(marshallerOutputStream(objects).toByteArray());
  }

  protected InputStream marshallerInputStreamByTempFile(T objects) throws JAXBException, IOException {
    File f = File.createTempFile("tmp_" + objects.hashCode() + "_" + System.currentTimeMillis(), ".xml.gz");
    f.deleteOnExit();
    marshallerFile(objects, f);
    FileInputStream fis = new FileInputStream(f);
    return fis;
  }

  protected InputStream marshallerInputStreamGz(T objects) throws JAXBException, IOException {
    return new ByteArrayInputStream(marshallerOutputStreamGz(objects).toByteArray());
  }

  protected File marshallerFileGz(T objects) throws JAXBException, IOException {
    File f = File.createTempFile("tmp_" + objects.hashCode() + "_" + System.currentTimeMillis(), ".xml.gz");
    marshallerFileGz(objects, f);
    return f;
  }

  protected void marshallerFileGz(T objects, File fileToCreate) throws JAXBException, IOException {
    FileOutputStream fws = new FileOutputStream(fileToCreate, appendFile);
    GZIPOutputStream gzos = new GZIPOutputStream(fws);
    marschaller(objects, gzos);
    gzos.close();
    fws.close();
  }

  protected void marshallerFile(T objects, File fileToCreate) throws JAXBException, IOException {
    FileOutputStream fws = new FileOutputStream(fileToCreate, appendFile);
    marschaller(objects, fws);
    fws.close();
  }

  protected File marshallerFile(T objects) throws JAXBException, IOException {
    File f = File.createTempFile("tmp_" + objects.hashCode() + "_" + System.currentTimeMillis(), ".xml");
    marshallerFile(objects, f);
    return f;
  }

  public String getErrorProcesing() {
    if (getValidation().hasEvents()) {
      if (error.trim().equals("")) {
        for (ValidationEvent event : getValidation().getEvents()) {
          if (event.getSeverity() == ValidationEvent.ERROR || event.getSeverity() == ValidationEvent.FATAL_ERROR) {
            if (event.getMessage().toUpperCase().contains("ATTRIBUTE")) {
              error += event.getLocator().toString().trim() + event.getMessage() + ". ";
            }
          }
        }

        for (ValidationEvent event : getValidation().getEvents()) {
          if (event.getSeverity() == ValidationEvent.ERROR || event.getSeverity() == ValidationEvent.FATAL_ERROR) {
            if (!event.getMessage().toUpperCase().contains("ATTRIBUTE")) {
              error += event.getLocator().toString().trim() + event.getMessage() + ". ";
            }
          }
        }
      }
    }
    return error;
  }

  /**
   * @return the validateFormat
   */
  public boolean isValidateFormat() {
    return validateFormat;
  }

  /**
   * @param validateFormat the validateFormat to set
   */
  public void setValidateFormat(boolean validateFormat) {
    this.validateFormat = validateFormat;
  }

  /**
   * @return the appendFile
   */
  public boolean isAppendFile() {
    return appendFile;
  }

  /**
   * @param appendFile the appendFile to set
   */
  public void setAppendFile(boolean appendFile) {
    this.appendFile = appendFile;
  }
}
