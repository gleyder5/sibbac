package sibbac.webapp.beans;

import java.io.Serializable;
import java.util.Map;
import java.util.List;

import sibbac.webapp.business.SIBBACServiceBean;

/**
 * This bean matches a Web response returned by the SIBBAC Spring REST Service.
 * <br/>
 * The bean contains:
 * <ul>
 * <li>A {@link WebRequest}, for identifying the <b>request</b>.</li>
 * <li>A {@link String}, for identifying the <b>error</b>, if happened.</li>
 * <li>A {@link Map} of <{@link String}, {@link Object}>, for the <b>results</b>
 * .</li>
 * </ul>
 *
 * Ideally, the response will be returned by a {@link SIBBACServiceBean} that
 * will perform the requested operations, according to the data received in
 * within the {@link WebRequest}.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public class WebResponse implements Serializable {

	public static final String KEY_DATA = "data";

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long serialVersionUID = 66482554681644165L;

	/**
	 * The request.
	 */
	private WebRequest request;

	/**
	 * The error string.
	 */
	private String error;

	/**
	 * The resulting data.
	 */
	private Map<String, ? extends Object> resultados;
	
	/**
	 * Lista de errores resultante.
	 */
	private Map<String, ? extends Object> resultadosError;
	
	/**
	 * The result when it is an array of array of objects
	 */
	private List<?> arrayData;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public WebResponse() {
		this(null, null);
	}

	/**
	 * The constructor for some data.
	 *
	 * @param request
	 *            The original request.
	 */
	public WebResponse(final WebRequest request) {
		this(request, null);
	}

	/**
	 * The constructor for some data.
	 *
	 * @param request
	 *            The original request.
	 * @param resultados
	 *            The resulting data.
	 */
	public WebResponse(final WebRequest request, final Map<String, ? extends Object> resultados) {
		super();
		this.request = request;
		this.resultados = resultados;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for request
	 *
	 * @return the request
	 */
	public WebRequest getRequest() {
		return this.request;
	}

	/**
	 * Getter for error
	 *
	 * @return the error
	 */
	public String getError() {
		return this.error;
	}

	/**
	 * Getter for resultados
	 *
	 * @return the resultados
	 */
	public Map<String, ? extends Object> getResultados() {
		return this.resultados;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for request
	 *
	 * @param request
	 *            the request to set
	 */
	public void setRequest(final WebRequest request) {
		this.request = request;
	}

	/**
	 * Setter for error
	 *
	 * @param error
	 *            the error to set
	 */
	public void setError(final String error) {
		this.error = error;
	}

	/**
	 * Setter for resultados
	 *
	 * @param resultados
	 *            the resultados to set
	 */
	public void setResultados(final Map<String, ? extends Object> resultados) {
		this.resultados = resultados;
	}
	
	/**
	 * Setter for resultados con error.
	 *
	 * @param resultadosError
	 *            the resultados error to set
	 */
	public void setResultadosError(final Map<String, ? extends Object> resultadosError) {
		this.resultadosError = resultadosError;
	}
	
	public Map<String, ? extends Object> getResultadosError() {
		return this.resultadosError;
	}

	public List<?> getArrayData() {
	    return arrayData;
	}
	
	public void setArrayData(List<?> arrayData) {
	    this.arrayData = arrayData;
	}

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[request==" + this.request + "] [error==" + this.error + "]";
	}

}
