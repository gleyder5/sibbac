package sibbac.webapp.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.util.SupportedMediaTypes;

/**
 * This bean matches a Web POST request received by the SIBBAC Spring REST
 * Service.<br/>
 *
 * The bean contains:
 * <ul>
 * <li>A {@link String}, for identifying the <b>action</b>.</li>
 * <li>A {@link String}, for identifying the desired <b>service</b>.</li>
 * <li>A {@link Map} of <{@link String}, {@link String}>, for the <b>filters</b>
 * , normally keypair values like: "field"=="value".</li>
 * <li>A {@link Map} of <{@link String}, {@link String}>, for the
 * <b>autoComplete</b> requests, normally keypair values like: "field"=="value".
 * </li>
 * </ul>
 *
 * Ideally, the request will be passed to a {@link SIBBACServiceBean} that will
 * perform the desired operations, according to the data received in within this
 * request.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public class WebRequest implements Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long serialVersionUID = 8090787264329100115L;

	/**
	 * The action.
	 */
	private String action;

	/**
	 * The export format string.
	 */
	private String export;

	/**
	 * The service.
	 */
	private String service;

	/**
	 * The filters.
	 */
	private Map<String, String> filters;
	/**
	 * Parametros de busqueda.
	 */
	private List<Map<String, String>> params;
	
	/**
	 * Parametros de busqueda.
	 */
	private Map<String, Object> paramsObject;
	/**
	 * Parametros de lista.
	 */
	private List<String> list;
	/**
	 * The autoComplete filters.
	 */
	private Map<String, String> autoComplete;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public WebRequest() {

	}

	/**
	 * The constructor for some data.
	 *
	 * @param action
	 *            The action.
	 * @param service
	 *            The service.
	 * @param filters
	 *            The filters.
	 */
	public WebRequest(final String action, final String service, final List<Map<String, String>> filters) {
		this(action, SupportedMediaTypes.JSON.getExtension(), service, filters, null);
	}

	/**
	 * The constructor for some data.
	 *
	 * @param action
	 *            The action.
	 * @param export
	 *            The export format.
	 * @param service
	 *            The service.
	 * @param filters
	 *            The filters.
	 */
	public WebRequest(final String action, final String export, final String service,
			final List<Map<String, String>> filters) {
		this(action, export, service, filters, null);
	}

	/**
	 * The constructor for some data.
	 *
	 * @param action
	 *            The action.
	 * @param export
	 *            The export format.
	 * @param service
	 *            The service.
	 * @param filters
	 *            The filters.
	 * @param autoComplete
	 *            The autoComplete filters.
	 */
	public WebRequest(final String action, final String export, final String service,
			final List<Map<String, String>> params, final Map<String, String> autoComplete) {
		super();
		this.action = action;
		this.export = export;
		this.service = service;
		this.params = params;
		this.autoComplete = autoComplete;
	}

	// ------------------------------------------- Bean methods: Getters

	/**
	 * Getter for action
	 *
	 * @return the action
	 */
	public String getAction() {
		return this.action;
	}

	/**
	 * Getter for export
	 *
	 * @return the export
	 */
	public String getExport() {
		return this.export;
	}

	/**
	 * Getter for service
	 *
	 * @return the service
	 */
	public String getService() {
		return this.service;
	}

	/**
	 * Getter for autoComplete
	 *
	 * @return the autoComplete
	 */
	public Map<String, String> getAutoComplete() {
		return this.autoComplete;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * Setter for action
	 *
	 * @param action
	 *            the action to set
	 */
	public void setAction(final String action) {
		this.action = action;
	}

	/**
	 * Setter for export
	 *
	 * @param export
	 *            the export to set
	 */
	public void setExport(final String export) {
		this.export = export;
	}

	/**
	 * Setter for service
	 *
	 * @param service
	 *            the service to set
	 */
	public void setService(final String service) {
		this.service = service;
	}

	/**
	 * Setter for filters
	 *
	 * @param filters
	 *            the filters to set
	 */

	/**
	 * Setter for autoComplete
	 *
	 * @param autoComplete
	 *            the autoComplete to set
	 */
	public void setAutoComplete(final Map<String, String> autoComplete) {
		this.autoComplete = autoComplete;
	}

	public Map<String, String> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, String> filters) {
		this.filters = filters;
	}

	public List<Map<String, String>> getParams() {
		return params;
	}

	public void setParams(List<Map<String, String>> params) {
		this.params = params;
	}

	public List<String> getList() {
		return this.list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}
	
	

	// ------------------------------------------------ Business Methods

	// ------------------------------------------------ Internal Methods

	// ----------------------------------------------- Inherited Methods

	public Map<String, Object> getParamsObject() {
		return paramsObject;
	}

	public void setParamsObject(Map<String, Object> paramsObject) {
		this.paramsObject = paramsObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[action==" + this.action + "] [service==" + this.service + "]";
	}

}
