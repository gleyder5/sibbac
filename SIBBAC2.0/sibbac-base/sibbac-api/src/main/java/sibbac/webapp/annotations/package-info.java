/**
 * This package include the annotations used within the SIBBAC 2.0 Web App.<br/>
 * The full web application is based mainly on Java SE 8, Spring 4 and Hibernate
 * 4.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
package sibbac.webapp.annotations;
