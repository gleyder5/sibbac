package sibbac.webapp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractWebFilter implements WebFilter {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractWebFilter.class);

  protected Map<String, String[]> params;

  protected abstract void parseParameters() throws ParseException;

  private final DateFormat invoiceDateFormat;
  
  public AbstractWebFilter() {
    invoiceDateFormat = new SimpleDateFormat(INVOICE_DATE_FORMAT);
  }
  
  @Override
  public final boolean parseParameters(Map<String, String[]> params) {
    this.params = params;
    try {
      parseParameters();
      return true;
    }
    catch (ParseException | NullPointerException e) {
      LOG.warn("Al recuperar un filtro con mensaje", e);
      return false;
    }
    finally {
      this.params = null;
    }
  }

  protected String getFirstString(String parameterName) {
    final String[] arr;

    Objects.requireNonNull(parameterName);
    arr = params.get(parameterName);
    if (arr == null)
      return null;
    if (arr.length > 0 && arr[0].length() > 0)
      return arr[0];
    return null;
  }

  protected Integer getFirstInt(String parameterName) {
    final String stringParameter;

    stringParameter = getFirstString(parameterName);
    if (stringParameter == null)
      return null;
    try {
      return Integer.valueOf(stringParameter);
    }
    catch (NumberFormatException nfex) {
      return null;
    }
  }

  protected int getMandatoryFirstInt(String parameterName) {
    final String stringParameter;

    stringParameter = Objects.requireNonNull(getFirstString(parameterName));
    return Integer.parseInt(stringParameter);
  }

  protected String getMandatoryFirstString(String parameterName) {
    return Objects.requireNonNull(getFirstString(parameterName));
  }

  protected Date getFirstDate(String parameterName) {
    final String parameterValue;

    parameterValue = getFirstString(parameterName);
    if (parameterValue == null)
      return null;
    try {
      return invoiceDateFormat.parse(parameterValue);
    }
    catch (ParseException pse) {
      return null;
    }
  }

  protected Date getMandatoryFirstDate(String parameterName) throws ParseException {
    final String parameterValue;

    parameterValue = Objects.requireNonNull(getFirstString(parameterName));
    return invoiceDateFormat.parse(parameterValue);
  }

  protected boolean getMandatoryFirstBoolean(String parameterName) {
    final String parameterValue;

    parameterValue = Objects.requireNonNull(getFirstString(parameterName));
    return Boolean.parseBoolean(parameterValue);
  }

  protected char getMandatoryFirstChar(String parameterName) throws ParseException {
    final String parameterValue;

    parameterValue = Objects.requireNonNull(getFirstString(parameterName));
    if (parameterValue.length() > 0)
      return parameterValue.charAt(0);
    throw new ParseException("Error parseando parametro caracter", 0);
  }

  protected Character getFirstChar(String parameterName) {
    final String parameterValue;

    parameterValue = getFirstString(parameterName);
    if (parameterValue == null || parameterValue.isEmpty())
      return null;
    return parameterValue.charAt(0);
  }

  protected int getMandatoryFirstInteger(String parameterName) {
    final String parameterValue;

    parameterValue = Objects.requireNonNull(getFirstString(parameterName));
    return Integer.parseInt(parameterValue);
  }

  protected Integer getFirstInteger(String parameterName) {
    final String parameterValue;

    parameterValue = getFirstString(parameterName);
    if (parameterValue != null)
      try {
        return Integer.decode(parameterValue);
      }
      catch (NumberFormatException ex) {
        LOG.warn("Error de parseo al recuperar entero: {}, valor {}", ex.getMessage(), parameterValue);
      }
    return null;
  }

  protected List<String> getList(String parameterName) {
    final String[] arr;

    Objects.requireNonNull(parameterName);
    arr = params.get(parameterName);
    if (arr == null || arr.length == 0)
      return Collections.emptyList();
    return Arrays.asList(arr);
  }

  protected boolean getEquality(String targetParameterName) {
    final String parameterName, parameterValue;
    
    parameterName = targetParameterName.concat(EQ_SUFFIX);
    parameterValue = getFirstString(parameterName);
    if(parameterValue == null || parameterValue.isEmpty())
        return true;
    return Boolean.parseBoolean(parameterValue);
  }

}
