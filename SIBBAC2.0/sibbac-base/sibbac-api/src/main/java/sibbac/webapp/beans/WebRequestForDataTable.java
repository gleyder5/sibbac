package sibbac.webapp.beans;

import java.util.List;
import java.util.Arrays;

public class WebRequestForDataTable extends WebRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* COMPONENTE DATATABLE */
	private int start;// posición inicial de los registros que se van a mostrar

	private int length; // Cuantos registros se muestran por pantalla

	private List<DataTableColumn> columns;

	private int draw;

	private String[] order;

	private DataTableSearch search;

	public WebRequestForDataTable() {
		//
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public List<DataTableColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<DataTableColumn> columns) {
		this.columns = columns;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public String[] getOrder() {
		return Arrays.copyOf(order, order.length);
	}

	public void setOrder(String[] order) {
		this.order =  Arrays.copyOf(order, order.length);
	}

	public DataTableSearch getSearch() {
		return search;
	}

	public void setSearch(DataTableSearch search) {
		this.search = search;
	}

}
