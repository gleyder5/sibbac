/**
 * This package contains the java beans for handling Web POST requests and
 * responses by the SIBBAC Spring REST Services.<br/>
 * The main classes are:
 *
 * <ul>
 * <li>{@link WebRequest}, for handling POST requests</li>
 * <li>{@link WebResponse}, for handling responses</li>
 * </ul>
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
package sibbac.webapp.beans;