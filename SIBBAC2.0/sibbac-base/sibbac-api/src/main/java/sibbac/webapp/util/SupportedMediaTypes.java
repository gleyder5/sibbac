package sibbac.webapp.util;

import org.springframework.http.MediaType;

/**
 * This enumeration contains the SIBBAC supported media types.<br/>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public enum SupportedMediaTypes {

	HTM("htm", MediaType.TEXT_HTML.toString()), HTML("html", MediaType.TEXT_HTML.toString()), JSON("json",
			MediaType.APPLICATION_JSON_VALUE.toString()), XML("xml", MediaType.TEXT_XML_VALUE.toString()), PDF("pdf",
					"application/pdf"), XLS("xls", "application/ms-excel"), XLSX("xlsx", "application/vnd.ms-excel");

	// XLSX( "xlsx",
	// "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" );

	private String extension;
	private String contentType;

	SupportedMediaTypes(final String extension, final String contentType) {
		this.extension = extension;
		this.contentType = contentType;
	}

	public String getExtension() {
		return this.extension;
	}

	public String getContentType() {
		return this.contentType;
	}

}
