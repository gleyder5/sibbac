package sibbac.webapp.view;

/**
 * Interface that allows to determinate how the view should deliver the
 * generated document, whether "inline" or as an "attachment".
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 *
 */
public interface HttpExportableView {

	/**
	 * The allowed HTTP Exportable modes.
	 */
	public enum MODE {
		INLINE, ATTACHMENT
	};

	/**
	 * Allows to set how the view should export ("HTTP") the generated document.
	 * <p/>
	 * Allowed values:
	 * <ul>
	 * <li>INLINE (<i>default</i>)</li>
	 * <li>ATTACHMENT</li>
	 * </ul>
	 * 
	 * @param exportMode
	 *            The desired export mode.
	 */
	public void setExportMode(final MODE exportMode);

}
