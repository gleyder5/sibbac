/**
 * This package include the java objects used within the SIBBAC 2.0 Web App.
 * <br/>
 * The full web application is based mainly on Java SE 8, Spring 4 and Hibernate
 * 4.
 * 
 * @see <a href="http://download.oracle.com/javase/8/docs/api/">http://download.
 *      oracle.com/javase/8/docs/api/</a>
 * @see <a href=
 *      "http://docs.spring.io/autorepo/docs/spring/current/javadoc-api/">http:/
 *      /docs.spring.io/autorepo/docs/spring/current/javadoc-api/</a>
 * @see <a href=
 *      "http://docs.jboss.org/hibernate/annotations/3.5/reference/en/html/">
 *      http://docs.jboss.org/hibernate/annotations/3.5/reference/en/html/</a>
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
package sibbac.webapp;
