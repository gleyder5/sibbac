/**
 * This package contains some java util classes.<br/>
 *
 * <ul>
 * <li>{@link SIBBACWebAppConfiguringPostProcessor}, for configuring Spring
 * beans in the WebApp module after properties injection.</li>
 * <li>{@link SIBBACCustomViewResolver}, for resolving Spring MCV views against
 * some desired types.</li>
 * <li>{@link SIBBACGlobalExceptionHandler}, for managing exceptions globally.
 * </li>
 * <li>{@link SIBBACServicesManager}, for managing SIBBAC Services.</li>
 * <li>{@link MediaType}, with the list of recognizable media types.</li>
 * <li>{@link SupportedMediaTypes}, for managing recognizable media types.</li>
 * <li>{@link SupportedModels}, for managing Spring MVC Models.</li>
 * </ul>
 *
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
package sibbac.webapp.util;
