package sibbac.webapp.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 * This annotation determines that the class being annotated is a SIBBAC
 * Service.<br/>
 *
 * A SIBBAC Service is responsible of performing the relevant business
 * operations for web requests.<br/>
 * Every discovered SIBBAC Service will be injected into the SIBBAC
 * ServicesManager and used in within the SIBBAC {@link RestController}'s.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface SIBBACService {
}
