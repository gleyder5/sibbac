package sibbac.webapp.beans;

import java.util.ArrayList;
import java.util.List;

public class WebResponseDataTable<T> extends WebResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int recordsTotal; /*
								 * Total records, before filtering (i.e. the
								 * total number of records in the database)
								 */
	private int recordsFiltered; /*
									 * Total records, after filtering (i.e. the
									 * total number of records after filtering
									 * has been applied - not just the number of
									 * records being returned for this page of
									 * data).
									 */
	private Integer draw; /*
							 * An unaltered copy of sEcho sent from the client
							 * side. This parameter will change with each draw
							 * (it is basically a draw count) - so it is
							 * important that this is implemented. Note that it
							 * strongly recommended for security reasons that
							 * you 'cast' this parameter to an integer in order
							 * to prevent Cross Site Scripting (XSS) attacks.
							 */
	private List<DataTableColumn> columns; /*
											 * Deprecated Optional - this is a
											 * string of column names, comma
											 * separated (used in combination
											 * with sName) which will allow
											 * DataTables to reorder data on the
											 * client-side if required for
											 * display. Note that the number of
											 * column names returned must
											 * exactly match the number of
											 * columns in the table. For a more
											 * flexible JSON format, please
											 * consider using mData.
											 */

	/*
	 * Note that this parameter is deprecated and will be removed in v1.10.
	 * Please now use mData.
	 */
	private List<T> data; /*
								 * The data in a 2D array. Note that you can
								 * change the name of this parameter with
								 * sAjaxDataProp.
								 */

	public WebResponseDataTable(WebRequestForDataTable webRequest) {
		super((WebRequest) webRequest);
	}

	public WebResponseDataTable() {
		super();
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public Integer getDraw() {
		return draw;
	}

	public void setDraw(Integer integer) {
		this.draw = integer;
	}

	public List<DataTableColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<DataTableColumn> columns) {
		this.columns = columns;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
	
	public void addData(List<T> data) {
		if (this.data==null) {
			this.data = new ArrayList<T>();
		}
		this.data.addAll(data);
	}
}
