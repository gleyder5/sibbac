package sibbac.webapp.business;

import java.util.Map;

import sibbac.common.FormatStyle;
import sibbac.common.StreamResult;

public interface SIBBACDataTableServiceBean {
    
    void queryDataTable(Map<String, String[]> params, StreamResult streamResult);
    
    void queryExport(Map<String, String[]> params, FormatStyle formatStyle, StreamResult streamResult);
    
}
