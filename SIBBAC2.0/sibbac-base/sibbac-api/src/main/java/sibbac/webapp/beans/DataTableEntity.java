package sibbac.webapp.beans;

import java.util.List;

public class DataTableEntity {
    
    private long recordsTotal;
    
    private long recordsFiltered;
    
    private List<?> data;
    
    private String error;
    
    public long getRecordsTotal() {
        return recordsTotal;
    }
    
    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }
    
    public long getRecordsFiltered() {
        return recordsFiltered;
    }
    
    public List<?> getData() {
        return data;
    }
   
    public void setData(List<?> data) {
        this.data = data;
        recordsFiltered = data.size();
    }
    
    public void setError(String error) {
        this.error = error;
    }
    
    public String getError() {
        return error;
    }

}
