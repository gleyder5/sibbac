/**
 * This package contains the classes that allow to resolve views for concrete
 * media types.<br/>
 *
 * Available view resolvers are:
 * <ul>
 * <li>Excel: ({@link ExcelView})</li>
 * <li>PDF: ({@link PDFView})</li>
 * </ul>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
package sibbac.webapp.view;
