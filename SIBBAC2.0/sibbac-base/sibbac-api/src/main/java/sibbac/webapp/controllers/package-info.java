/**
 * This package contains the java beans for managing Web REST Requests.<br/>
 * Normally, Web Requests are received as Web POST requests, ({@link WebRequest}
 * ), into a {@link ServiceController}. The relevant method of the
 * {@link ServiceController} will parse the incoming {@link WebRequest} and will
 * try to find out the corresponding {@link SIBBACServiceBean} to handle the
 * request, using the capabilities of the {@link SIBBACServicesManager}. Once
 * found, the {@link WebRequest} will be passed to the {@link SIBBACServiceBean}
 * that will, in turn, return an instance of a {@link WebResponse} with the
 * resulting data, that will be, in turn, passed back to the client.
 *
 * This package contains a base {@link AbstractController}, that all SIBBAC
 * Spring {@link RestController}'s must extend. This parent controller holds
 * commonly used methods, available for all child SIBBAC Spring
 * {@link RestController}'s.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
package sibbac.webapp.controllers;
