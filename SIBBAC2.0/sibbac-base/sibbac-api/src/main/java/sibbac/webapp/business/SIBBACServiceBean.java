package sibbac.webapp.business;

import java.util.List;

import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;

/**
 * This interface declares the methods that a SIBBAC Service Bean must
 * implement.<br/>
 * A SIBBAC Service is responsible of performing the relevant business
 * operations for incoming {@link WebRequest}s and to return a valid
 * {@link WebResponse}.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public interface SIBBACServiceBean {

	/**
	 * Implementing classes must process the incoming {@link WebRequest} and
	 * return a valid {@link WebResponse}.
	 *
	 * @param webRequest
	 *            The {@link WebRequest}.
	 * @return A {@link WebResponse} with the result of the operation.
	 */
	public WebResponse process(final WebRequest webRequest) throws SIBBACBusinessException;

	/**
	 * Implementing classes must declare the commands that the business bean can
	 * accept for business operations.
	 *
	 * @return A {@link List} of {@link String}'s with the recognizable business
	 *         commands.
	 */
	public List<String> getAvailableCommands();

	/**
	 * Implementing classes must declare the valid fields to be parsed within
	 * the business operations. Deprecated: Will be remove in future versions.
	 * 
	 * @return A {@link List} of {@link String}'s with the recognizable fields.
	 */
	@Deprecated
	public List<String> getFields();

}
