package sibbac.webapp;

import java.util.Map;

public interface WebFilter {
    
    public static final String INVOICE_DATE_FORMAT = "yyyyMMdd";
  
    public static final String EQ_SUFFIX = "Eq";
    
    public boolean parseParameters(Map<String, String[]> params);
    
}
