package sibbac.webapp.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import sibbac.webapp.business.SIBBACServiceBean;

/**
 * This bean matches a Web POST request received by the SIBBAC Spring REST
 * Service for XLS/PDF Exporting.<br/>
 *
 * The bean contains:
 * <ul>
 * <li>A {@link String}, for identifying the <b>action</b>.</li>
 * <li>A {@link String}, for identifying the desired <b>service</b>.</li>
 * <li>A {@link Map} of <{@link String}, {@link String}>, for the <b>filters</b>
 * , normally keypair values like: "field"=="value".</li>
 * <li>A {@link Map} of <{@link String}, {@link String}>, for the
 * <b>autoComplete</b> requests, normally keypair values like: "field"=="value".
 * </li>
 * </ul>
 *
 * Ideally, the request will be passed to a {@link SIBBACServiceBean} that will
 * perform the desired operations, according to the data received in within this
 * request.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public class WebExport implements Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 * 
	 */
	private static final long serialVersionUID = 8090787264329100115L;

	/**
	 * The WebResponse.
	 */
	private WebResponse webResponse;

	/**
	 * The columns order.
	 */
	private Map<Integer, String> columnas;

	/**
	 * The data sets.
	 */
	private List<String> dataSet;

	// ----------------------------------------------- Bean Constructors

	/**
	 * The default constructor.
	 */
	public WebExport() {

	}

	/**
	 * @return the request
	 */
	public WebResponse getWebResponse() {
		return webResponse;
	}

	/**
	 * @param webResponse
	 *            the webResponse to set
	 */
	public void setWebResponse(WebResponse webResponse) {
		this.webResponse = webResponse;
	}

	/**
	 * @return the columnas
	 */
	public Map<Integer, String> getColumnas() {
		return columnas;
	}

	/**
	 * @param columnas
	 *            the columnas to set
	 */
	public void setColumnas(Map<Integer, String> columnas) {
		this.columnas = columnas;
	}

	/**
	 * @return the request
	 */
	public List<String> getDataSet() {
		return dataSet;
	}

	/**
	 * @param dataSet
	 *            the List<String> to set
	 */
	public void setDataSet(List<String> dataSet) {
		this.dataSet = dataSet;
	}

}
