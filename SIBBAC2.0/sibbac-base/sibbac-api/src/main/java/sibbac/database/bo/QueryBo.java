package sibbac.database.bo;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.common.FormatStyle;
import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;
import sibbac.database.QueryFilter;
import sibbac.database.toformat.JDBCtoFormat;

@Service
public class QueryBo {

  private static final Logger LOG = LoggerFactory.getLogger(QueryBo.class);

  @Autowired
  private DataSource ds;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  public void executeQuery(QueryFilter queryFilter, FormatStyle formatStyle, StreamResult streamResult) {
    final String countQuery, selectQuery;
    final Map<Date, Long> countMap;
    int founded = 0;

    try {
      try (Connection con = ds.getConnection();) {
        if (queryFilter.isCountNecessary()) {
          countQuery = queryFilter.getCountQuery();
          if (countQuery != null) {
            try (Statement q = con.createStatement()) {
              LOG.info("[executeQuery] Inicio consulta count: {}", countQuery);
              countMap = new HashMap<>();
              try (ResultSet countrs = q.executeQuery(countQuery);) {
                while (countrs.next()) {
                  countMap.put(countrs.getDate(2), countrs.getLong(1));
                }
              }
              founded = queryFilter.setCountResults(countMap);
              LOG.info("[executeQuery] procesado el count con {} registro encontrados", founded);
              if (founded == 0) {
                try (JDBCtoFormat formatter = JDBCtoFormat.getFormat(formatStyle);
                    OutputStream writer = streamResult.getOutputStream(formatStyle)) {
                  formatter.generateEmpty(writer);
                }
                return;
              }

            }
          }
        }
        selectQuery = queryFilter.getSelectQuery();
        LOG.info("[executeQuery] Inicia consulta: {}", selectQuery);
        try (Statement q = con.createStatement();
            ResultSet rs = q.executeQuery(selectQuery);
            JDBCtoFormat formatter = JDBCtoFormat.getFormat(formatStyle);
            OutputStream writer = streamResult.getOutputStream(formatStyle)) {
          formatter.setSingleFilter(queryFilter.getSingleFilter());
          formatter.setIsoFormat(queryFilter.isIsoFormat());
          formatter.setDynamicColumns(queryFilter.getColumns());
          formatter.setResultSet(rs);
          formatter.generate(writer);
        }
      }
    }
    catch (SQLException sqlex) {
      LOG.error("Error de ejecución de query nativa", sqlex);
      streamResult.sendErrorMessage(sqlex.getMessage());
    }
    catch (IOException ioex) {
      LOG.error("Error al recuperar flujo de salida", ioex);
      streamResult.sendErrorMessage(ioex.getMessage());
    }
    catch (SIBBACBusinessException bex) {
      LOG.error("Error de negocio sibbac", bex);
      streamResult.sendErrorMessage(bex);
    }
    catch (RuntimeException rex) {
      LOG.error("Error inesperado al ejecutar Query", rex);
      streamResult.sendErrorMessage("Error inesperado, vuelva a intetar más tarde.");
    }
  }

  public <DEST> List<DEST> executeQueryNative(QueryFilter queryFilter, Class<DEST> clazz)
      throws SIBBACBusinessException {
    final Map<Date, Long> countMap;
    final List<?> counts;
    final TypedQuery<DEST> typedQuery;
    final Query query;
    final String countQuery, selectQuery;
    EntityManager entityManager = null;
    Object[] r;

    countMap = new HashMap<>();
    try {
      entityManager = entityManagerFactory.createEntityManager();
      if (queryFilter.isCountNecessary()) {
        countQuery = queryFilter.getCountQuery();
        LOG.info("[executeQuery] Inicia consulta de count: {}", countQuery);
        query = entityManager.createQuery(countQuery);
        counts = query.getResultList();
        for (Object o : counts) {
          r = (Object[]) o;
          countMap.put((Date) r[1], (Long) r[0]);
        }
        if (countMap.isEmpty()) {
          return Collections.emptyList();
        }
        queryFilter.setCountResults(countMap);
      }
      selectQuery = queryFilter.getSelectQuery();
      LOG.info("[executeQuery] Inicia consulta {}", selectQuery);
      typedQuery = entityManager.createQuery(selectQuery, clazz);
      return typedQuery.getResultList();
    }
    finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

  /**
   * Ejecuta una native Query y la mapea a un POJO
   * @param queryFilter
   * @param clazz
   * @return
   * @throws SIBBACBusinessException
   */
  public <T> List<T> executeQueryNativeMapped(QueryFilter queryFilter, Class<T> clazz) throws SIBBACBusinessException {
    final List<T> results = new ArrayList<>();

    final String countQuery;
    final String selectQuery;
    final Map<Date, Long> countMap;
    int founded = 0;
    final Map<String, Method> methodz = getMethodMap(clazz);

    try {

      try (Connection con = ds.getConnection();) {
        // if (queryFilter.isCountNecessary()) {
        // countQuery = queryFilter.getCountQuery();
        // if (countQuery != null) {
        // try (Statement q = con.createStatement()) {
        // LOG.info("[executeQuery] Inicio consulta count: {}", countQuery);
        // countMap = new HashMap<>();
        // try (ResultSet countrs = q.executeQuery(countQuery);) {
        // while (countrs.next()) {
        // countMap.put(countrs.getDate(2), countrs.getLong(1));
        // }
        // }
        // founded = queryFilter.setCountResults(countMap);
        // LOG.info("[executeQuery] procesado el count con {} registro
        // encontrados", founded);
        // if (founded == 0) {
        // return null;
        // }
        // }
        // }
        // }

        selectQuery = queryFilter.getSelectQuery();

        LOG.info("[executeQuery] Inicia consulta: {}", selectQuery);

        Statement q = con.createStatement();
        q.setFetchSize(1000);
        ResultSet rs = q.executeQuery(selectQuery);
        while (rs.next()) {
          final T mappedClass = resultSetToClass(rs, clazz, methodz);
          if (mappedClass != null) {
            results.add(mappedClass);
          }
        }

      }
    }
    catch (SQLException sqlex) {
      LOG.error("Error de ejecución de query nativa", sqlex);
    }
    catch (SIBBACBusinessException bex) {
      LOG.error("Error de negocio sibbac", bex);
    }
    catch (RuntimeException rex) {
      LOG.error("Error inesperado al ejecutar Query", rex);
    }
    return results;
  }

  /**
   * Mapea una ResultSet a una Clase
   * @param resultSet
   * @param clazz
   * @return
   */
  private <T> T resultSetToClass(ResultSet resultSet, Class<T> clazz, Map<String, Method> methodz) {
    T t = null;
    try {
      t = clazz.newInstance();

      final ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

      int columnCount = resultSetMetaData.getColumnCount();

      for (int i = 1; i <= columnCount; i++) {
        final String columnLabel = resultSetMetaData.getColumnLabel(i);
        final String columnName = resultSetMetaData.getColumnName(i);
        final String nameMethod = getMethodNamePojo(columnLabel);

        final Method m = methodz.get(nameMethod);
        final Object o = resultSet.getObject(columnLabel);
        if (m != null) {
          try {
            m.invoke(t, o);
          }
          catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException exception) {
            String message = String.format("No se pudo setear el Valor '%s(%s)' en el DTO de destino: Metodo '%s(%s)'",
                o.toString(), o.getClass().getName(), m.getName(), m.getParameterTypes()[0].getName());
            LOG.debug(message);
          }
        }
        else {
          if (LOG.isDebugEnabled()) {
            LOG.debug("El metodo '" + nameMethod + " no existe en la clase " + clazz.getName() + " tipo "
                + resultSetMetaData.getColumnType(i));
          }
        }
      }
    }
    catch (IllegalAccessException | InstantiationException | SQLException | IllegalArgumentException exception) {
      LOG.error("Durante el mapeo del ResultSet a " + clazz.getName(), exception);
    }

    return t;
  }

  /**
   * Obtiene el nombre del metodo a patir de la columna
   * @param columnName
   * @return
   */
  private String getMethodNamePojo(String columnName) {
    final String pojoMethod = columnName.toLowerCase().replaceAll("_", "");
    final String cap = pojoMethod.substring(0, 1).toUpperCase() + pojoMethod.substring(1);
    return "set" + cap;
  }

  /**
   * Convierte el array de metodos en un hasmap, asi nos evitamos hacer
   * iteraciones para buscar el metodo
   * @param clazz
   * @return
   */
  private Map<String, Method> getMethodMap(Class<?> clazz) {
    final Map<String, Method> methodMap = new HashMap<>();
    Method[] methodz = clazz.getMethods();

    for (int i = 0; i < methodz.length; i++) {
      methodMap.put(methodz[i].getName(), methodz[i]);
    }

    return methodMap;
  }

}
