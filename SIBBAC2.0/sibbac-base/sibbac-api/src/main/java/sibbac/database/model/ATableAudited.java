package sibbac.database.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

import org.hibernate.envers.Audited;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class that contains the implementation of the basic methods
 * available for all database entities.<br/>
 * Also, may declare abstract methods to be implemented by child classes.
 * 
 * @param <T>
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 * @param <T>
 */
@MappedSuperclass
@Audited
public abstract class ATableAudited<T extends ITableAudited<T>> extends ATable<T>
		implements ITableAudited<T>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8269690803133231877L;

	protected static final Logger LOG = LoggerFactory.getLogger(ATableAudited.class);

}
