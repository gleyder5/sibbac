package sibbac.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAttribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.database.DBConstants;

/**
 * Abstract class that contains the implementation of the basic methods
 * available for all database entities.<br/>
 * Also, may declare abstract methods to be implemented by child classes.
 * 
 * @param <T>
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@MappedSuperclass
@EntityListeners({ JPAEventListener.class })
public abstract class ATable<T extends ITable<T>> implements ITable<T>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6111087747628244997L;

	protected static final Logger LOG = LoggerFactory.getLogger(ATable.class);

	// ------------------------------------------------- Bean properties

	/**
	 * The entity id.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	@XmlAttribute
	protected Long id;

	// ----------------------------------------------- Bean Constructors

	/**
	 * Constructor.
	 */
	public ATable() {
		this.version = new Long(0);
	}

	// ------------------------------------------------- Bean properties

	/**
	 * The version of the record.
	 */
	@Version
	@Column(name = "VERSION", nullable = false)
	@XmlAttribute
	protected Long version = 0L;

	/**
	 * The user that performs the change.
	 */
	@Column(name = "AUDIT_USER", nullable = false)
	@XmlAttribute
	protected String auditUser = DBConstants.AUDIT_USER_DEFAULT;

	/**
	 * The date instance when the change is performed.
	 */
	@Column(name = "AUDIT_DATE", nullable = false)
	@XmlAttribute
	protected Date auditDate = new Date();

	// ------------------------------------------- Bean methods: Getters

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.database.model.ITable#getId()
	 */
	@Override
	public Long getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.database.model.ITable#getVersion()
	 */
	@Override
	public Long getVersion() {
		return this.version;
	}

	public String getAuditUser() {
		return auditUser;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	// ------------------------------------------- Bean methods: Setters

	/**
	 * @param id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @param auditUser
	 */
	public void setAuditUser(String auditUser) {
		this.auditUser = auditUser;
	}

	/**
	 * @param auditDate
	 */
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	/**
	 * @param version
	 */
	public void setVersion(final Long version) {
		this.version = version;
	}

	// ------------------------------------------------ Business Methods

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(final T other) {
		boolean sameId = ATable.compareLongValues(this.id, other.getId());
		boolean sameVersion = ATable.compareLongValues(this.version, other.getVersion());
		return sameId && sameVersion ? 0 : -3;
	}

	// ------------------------------------------------ Business Methods

	@PrePersist
	protected void prePersist() {
		LOG.trace("[{}::prePersist] JPA Event. BEFORE: [{}/{}]", this.getClass().getSimpleName(), this.auditUser,
				this.auditDate);
		if (this.auditUser == null) {
			this.auditUser = "sibbac20";
		}
		if (this.auditDate == null) {
			this.auditDate = new Date();
		}
		LOG.trace("[{}::prePersist] JPA Event.  AFTER: [{}/{}]", this.getClass().getSimpleName(), this.auditUser,
				this.auditDate);
	}

	// ------------------------------------------------ Internal Methods

	/**
	 * @param l1
	 * @param l2
	 * @return
	 */
	protected static boolean compareLongValues(final Long l1, final Long l2) {
		if (l1 == null && l2 != null) {
			return false;
		}
		if (l2 == null && l1 != null) {
			return false;
		}
		long idL1 = l1.longValue();
		long idL2 = l2.longValue();
		return (idL1 == idL2);
	}

	public String toString() {
		return "[" + this.getClass().getName().toUpperCase() + "==" + this.id + "]";
	}

}
