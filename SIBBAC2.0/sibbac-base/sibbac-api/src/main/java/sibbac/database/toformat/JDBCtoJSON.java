package sibbac.database.toformat;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

abstract class JDBCtoJSON extends JDBCtoFormat {
  
  JsonGenerator writer;
  
  JDBCtoJSON() {
    super(true);
  }
  
  @Override
  void prepareStream() throws IOException {
    writer = new JsonFactory().createGenerator(outputStream);
    writer.writeStartArray();
  }

  @Override
  void endStream() throws IOException {
    writer.writeEndArray();
    writer.close();
  }
  
  final void writeValue(int position) throws SQLException, IOException {
    final String value;
    
    value = resultSet.getString(position);
    if(resultSet.wasNull()) {
      writer.writeNull();
    }
    else {
      switch(metaData.getColumnType(position)){
        case Types.BIGINT:
        case Types.INTEGER:
        case Types.SMALLINT:
        case Types.TINYINT:
          writer.writeNumber(resultSet.getLong(position));
          break;
        case Types.NUMERIC:
        case Types.DECIMAL:
          writer.writeNumber(resultSet.getDouble(position));
          break;
        case Types.DATE:
        case Types.TIMESTAMP:
        case Types.TIME:
          writer.writeString(isoDateFormat.format(resultSet.getDate(position)));
          break;
        default:
          writer.writeString(value);
      }
    }
    
  }

}
