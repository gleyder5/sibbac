package sibbac.database.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
// This is a Spring bean...
@Aspect
// ... that works as an Aspect!
public class BeanAspectListener {

	protected static final Logger LOG = LoggerFactory.getLogger(BeanAspectListener.class);

	@Value("${sibbac.aspect.prefix:SIBBAC_ASPECT_PREFIX}")
	private String prefix;

	@Pointcut("within(sibbac.business..database.bo..*)")
	public void aopInBusinessBO() {
	}

	@Pointcut("within(sibbac.database.bo.*)")
	public void aopInDatabaseBO() {
	}

	@Pointcut("aopInBusinessBO() || aopInDatabaseBO()")
	public void aopInBO() {
	}

	@Pointcut("within(sibbac.business..database.dao..*)")
	public void aopInSIBBACDAO() {
	}

	@Pointcut("within(org.springframework.data.jpa.repository.JpaRepository.*)")
	public void aopInJPADAO() {
	}

	@Pointcut("aopInSIBBACDAO() || aopInJPADAO()")
	public void aopInDAO() {
	}

	@Before("aopInBO() || aopInDAO()")
	public void beforeBO(final JoinPoint joinPoint) {
		Object obj = (joinPoint != null) ? joinPoint : "<NULL> Object";
		LOG.trace("[" + this.prefix + "] beforeBO(" + obj + ")");
	}

	@After("aopInBO() || aopInDAO()")
	public void afterBO(final JoinPoint joinPoint) {
		Object obj = (joinPoint != null) ? joinPoint : "<NULL> Object";
		LOG.trace("[" + this.prefix + "] afterBO(" + obj + ")");
	}

	@After("aopInBO() || aopInDAO()")
	public void afterDAO(final JoinPoint joinPoint) {
		Object obj = (joinPoint != null) ? joinPoint : "<NULL> Object";
		LOG.trace("[" + this.prefix + "] afterBO(" + obj + ")");
	}

	@AfterReturning(pointcut = "aopInBO() || aopInDAO()", returning = "result")
	public void afterReturning(final JoinPoint joinPoint, Object result) {
		Object obj = (joinPoint != null) ? joinPoint : "<NULL> Object";
		Object res = (result != null) ? result : "<NULL> Result";
		LOG.trace("[" + this.prefix + "] afterReturning(joinPoint==" + obj + ")");
		LOG.trace("[" + this.prefix + "] afterReturning(result==" + res + ")");
	}

	@AfterThrowing(pointcut = "aopInBO() || aopInDAO()", throwing = "error")
	public void afterThrowing(final JoinPoint joinPoint, Throwable error) {
		Object obj = (joinPoint != null) ? joinPoint : "<NULL> Object";
		Object res = (error != null) ? error : "<NULL> Result";
		//quitadas las palabras "ERROR" del log al informar de casos que no son error
		//LOG.error("[" + this.prefix + "] afterThrowing(joinPoint=" + obj + ")");
		//LOG.error("[" + this.prefix + "] afterThrowing(error==" + res + ")");
		LOG.info("[" + this.prefix + "] afterThrowing(joinPoint=" + obj + ")");
		LOG.info("[" + this.prefix + "] afterThrowing(description==" + res + ")");

	}

}
