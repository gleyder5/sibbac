package sibbac.database.model;

import java.io.Serializable;

/**
 * Interface that contains the methods all database entities must implement.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public interface ITableAudited<T> extends ITable<T>, Serializable, Comparable<T> {

}
