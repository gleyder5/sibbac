/**
 * Package with all the entities related with database objects.
 *
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
package sibbac.database.model;
