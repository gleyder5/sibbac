package sibbac.database;

public class DynamicColumn {
  
  private final String name;
  
  private final String field;
  
  private final ColumnType columntype;
  
  public DynamicColumn(String name, String field) {
    this.name = name;
    this.field = field;
    this.columntype = ColumnType.STRING;
  }
  
  public DynamicColumn(String field) {
    this.name = field.toLowerCase();
    this.field = field;
    this.columntype = ColumnType.STRING;
  }
  
  public DynamicColumn(String name, String field, ColumnType type) {
    this.name = name;
    this.field = field;
    this.columntype = type;
  }
  
  public String getName() {
    return name;
  }
  
  public String getField() {
    return field;
  }
  
  public String getType() {
    return columntype.type;
  }
  
  public String getCellFilter() {
    return columntype.filter;
  }
  
  private static final String NUMBER = "number";

  public enum ColumnType { 
    STRING(null, null), 
    DATE("date", "date : 'dd/MM/yyyy'"), 
    INT(NUMBER, "number : 0"), 
    N4(NUMBER, "number : 4"), 
    N6(NUMBER, "number : 6"),
    N9(NUMBER, "number : 9"),
    N10(NUMBER, "number : 10"),
    N12(NUMBER, "number : 12");
    

    private final String type;
    
    private final String filter;
    
    private ColumnType(String type, String filter) {
      this.type = type;
      this.filter = filter;
    }
  }

}
