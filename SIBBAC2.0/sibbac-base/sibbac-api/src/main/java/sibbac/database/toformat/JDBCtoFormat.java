package sibbac.database.toformat;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Objects;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.FormatStyle;
import sibbac.database.DynamicColumn;

public abstract class JDBCtoFormat implements AutoCloseable {
  
  public static final String ISO_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm'Z'";
  
  public static final String DATE_FORMAT = "dd/MM/yyyy";
  
  private static final Logger LOG = LoggerFactory.getLogger(JDBCtoFormat.class);
  
  ResultSet resultSet;
  
  ResultSetMetaData metaData;
  
  OutputStream outputStream;
  
  String singleFilter;
  
  DateFormat isoDateFormat;

  boolean isoFormat;
  
  private Integer columns;
  
  private final Map<Integer, String> columnLabels;
  
  private final boolean fieldNames;
  
  private List<DynamicColumn> dcList;
    
  public static JDBCtoFormat getFormat(FormatStyle formatStyle) throws SQLException {
    switch(formatStyle) {
      case JSON_ARRAY:
        return new JDBCtoJSONArray();
      case JSON_OBJECTS:
        return new JDBCtoJSONObjects();
      case EXCEL:
        return new JDBCtoExcel();
      case CSV:
        return new JDBCtoCSV();
      default:
        return null;  
    }
  }

  public JDBCtoFormat(boolean fieldNames) {
    this.fieldNames = fieldNames;
    columnLabels = new HashMap<>();
  }
  
  public void setResultSet(ResultSet resultSet) throws SQLException {
    Objects.requireNonNull(resultSet);
    this.resultSet = resultSet;
    loadMetaData();
  }
  
  public final void generate(final OutputStream outputStream) throws SQLException, IOException {
    LOG.debug("[generate] Inicio...");
    Objects.requireNonNull(resultSet, "No se ha proporcionado un conjunto de resultados");
    Objects.requireNonNull(outputStream);
    isoDateFormat = new SimpleDateFormat(isoFormat ? ISO_DATE_FORMAT : DATE_FORMAT);
    this.outputStream = outputStream;
    prepareStream();
    while(resultSet.next()) {
      processRow();
    }
    endStream();
    LOG.debug("[generate] Fin");
  }
  
  public final void generateEmpty(final OutputStream outputStream) throws IOException {
    LOG.debug("[generateEmpty] Inicio...");
    Objects.requireNonNull(outputStream);
    this.outputStream = outputStream;
    prepareStream();
    endStream();
    LOG.debug("[generateEmpty] Fin");
  }
  
  public final void setDynamicColumns(List<DynamicColumn> list) {
    dcList = list;
  }
  
  public final void setIsoFormat(boolean isoFormat) {
    this.isoFormat = isoFormat;
  }
  
  public final void setSingleFilter(String singleFilter) {
    this.singleFilter = singleFilter;
  }
  
  @Override
  public final void close() throws SQLException {
    if(resultSet != null) {
      resultSet.getStatement().getConnection().close();
    }
  }
  
  abstract void prepareStream() throws IOException;
  
  abstract void processRow() throws SQLException, IOException;
  
  abstract void endStream() throws IOException;
  
  final String columnLabel(int i) {
    final DynamicColumn dc;
    
    if(columnLabels.containsKey(i)) {
      return columnLabels.get(i);
    }
    if(i <= dcList.size()) {
      dc = dcList.get(i - 1);
      return fieldNames ? dc.getField() : dc.getName();
    }
    return "#ERROR#";
  }
  
  final int getColumns() {
    if(columns != null) {
      return columns.intValue();
    }
    if(dcList != null) {
      return dcList.size();
    }
    return 0;
  }
  
  private void loadMetaData() throws SQLException {
    String jdbcName, finalName;
    
    metaData = resultSet.getMetaData();
    columns = metaData.getColumnCount();
    if(!fieldNames && dcList != null && !dcList.isEmpty()) {
      for(int i = 1; i <= columns; i++) {
        jdbcName = metaData.getColumnLabel(i);
        finalName = resolveName(jdbcName);
        columnLabels.put(i, finalName);
      }
    }
    else {
      for(int i = 1; i <= columns; i++) {
        columnLabels.put(i, metaData.getColumnLabel(i));
      }
    }
  }
  
  private String resolveName(String jdbcName) {
    for(DynamicColumn dc : dcList) {
      if(dc.getField().equals(jdbcName)) {
        return dc.getName();
      }
    }
    return jdbcName;
  }
  
}
