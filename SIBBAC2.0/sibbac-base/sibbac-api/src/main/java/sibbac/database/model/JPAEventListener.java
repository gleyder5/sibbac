package sibbac.database.model;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Interface that contains the methods all database entities must implement.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public class JPAEventListener<T extends ATable<T>> {

	protected static final Logger LOG = LoggerFactory.getLogger(JPAEventListener.class);

	public static final String PREFIX = "JPAEvent";

	@PostLoad
	public void PostLoad(T t) {
		if (t != null) {
			LOG.trace("[{}::PostLoad] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

	@PostPersist
	public void PostPersist(T t) {
		if (t != null) {
			LOG.trace("[{}::PostPersist] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

	@PostRemove
	public void PostRemove(T t) {
		if (t != null) {
			LOG.trace("[{}::PostRemove] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

	@PostUpdate
	public void PostUpdate(T t) {
		if (t != null) {
			LOG.trace("[{}::PostUpdate] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

	@PrePersist
	public void PrePersist(T t) {
		if (t != null) {
			LOG.trace("[{}::PrePersist] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

	@PreRemove
	public void PreRemove(T t) {
		if (t != null) {
			LOG.trace("[{}::PreRemove] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

	@PreUpdate
	public void PreUpdate(T t) {
		if (t != null) {
			LOG.trace("[{}::PreUpdate] JPA Event on table: [{}]", PREFIX, t.getClass().getName());
		}
	}

}
