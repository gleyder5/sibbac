package sibbac.database.toformat;

import java.io.IOException;
import java.sql.SQLException;

class JDBCtoJSONArray extends JDBCtoJSON {

  @Override
  void processRow() throws SQLException, IOException {
    final int columns;
    
    columns = getColumns();
    writer.writeStartArray();
    for (int i = 1; i <= columns; i++) {
      writeValue(i);
    }
    writer.writeEndArray();
  }

}
