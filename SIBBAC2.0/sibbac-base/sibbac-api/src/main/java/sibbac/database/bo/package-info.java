/**
 * Package with all the interfaces for database entities' BOs.
 *
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
package sibbac.database.bo;
