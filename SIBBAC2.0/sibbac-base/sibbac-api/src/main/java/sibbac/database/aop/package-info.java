/**
 * Package with all the AOP methods.
 *
 * @version 1.0
 * @since 1.0
 * @author Arturo Garcia
 */
package sibbac.database.aop;
