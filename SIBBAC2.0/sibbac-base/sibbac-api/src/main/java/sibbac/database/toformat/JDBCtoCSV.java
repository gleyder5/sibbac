package sibbac.database.toformat;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

final class JDBCtoCSV extends JDBCtoExport {
  
  private static final char COMA = ';';
  
  private static final String EOL = "\r\n";
  
  private static final String QUOTES = "\"%s\"";
  
  /** Formato decimal a utilizar en todo el proyecto: "#,##0.0#" */
  private static final String FORMATO_DECIMAL = "#,##0.0#";

  /** El separador de miles es el punto, sin importar localización */
  private static final char SEPARADOR_MILES = '.';
  
  /** El separador de decimales es la coma, sin importar localización */
  private static final char SEPARADOR_DECIMAL = ',';
  
  private static final DecimalFormatSymbols DECIMAL_SYMBOLS;
  
  private PrintWriter writer;

  private final DecimalFormat dc;
  
  static {
    DECIMAL_SYMBOLS = new DecimalFormatSymbols();
    DECIMAL_SYMBOLS.setDecimalSeparator(SEPARADOR_DECIMAL);
    DECIMAL_SYMBOLS.setGroupingSeparator(SEPARADOR_MILES);
  }

  JDBCtoCSV() throws SQLException {
    dc = new DecimalFormat(FORMATO_DECIMAL);
    dc.setDecimalFormatSymbols(DECIMAL_SYMBOLS);
  }

  @Override
  void processFilteredRow() throws SQLException, IOException {
    final int columns;
    String value;
    
    columns = getColumns();
    for(int i = 1; i <= columns; i++) {
      value = resultSet.getString(i);
      if(!resultSet.wasNull()) {
        switch(metaData.getColumnType(i)) {
          case Types.BIGINT:
          case Types.INTEGER:
          case Types.SMALLINT:
          case Types.TINYINT:
            writer.format("%d", resultSet.getInt(i));
            break;
          case Types.DECIMAL:
          case Types.DOUBLE:
            writer.format(QUOTES, dc.format(resultSet.getDouble(i)));
            break;
          default:
            writer.format(QUOTES, value);
        }
      }
      writer.print(COMA);
    }
    writer.print(EOL);
  }

  @Override
  void prepareStream() throws IOException {
    final int columns;
    
    columns = getColumns();
    writer = new PrintWriter(new BufferedOutputStream(outputStream));
    for(int i = 1; i <= columns; i++) {
      writer.format(QUOTES, columnLabel(i));
      writer.print(COMA);
    }
    writer.print(EOL);
  }

  @Override
  void endStream() throws IOException {
    writer.flush();
  }

}
