package sibbac.database.toformat;

import java.io.IOException;
import java.sql.SQLException;

class JDBCtoJSONObjects extends JDBCtoJSON {

  @Override
  void processRow() throws SQLException, IOException {
    final int columns;
    
    columns = getColumns();
    writer.writeStartObject();
    for (int i = 1; i <= columns; i++) {
      writer.writeFieldName(columnLabel(i));
      writeValue(i);
    }
    writer.writeEndObject();
  }

}
