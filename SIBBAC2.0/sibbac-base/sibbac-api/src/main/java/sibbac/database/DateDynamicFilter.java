package sibbac.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDynamicFilter extends SimpleDynamicFilter<Date> {

  private static final long serialVersionUID = 1L;

  public DateDynamicFilter(String field, String messageKey) {
    super(field, messageKey, "yyyy-mm-dd");
  }

  @Override
  public void setValues(String[] values) throws ParseException {
    final SimpleDateFormat df;
    
    df = new SimpleDateFormat("dd/mm/yyyy");
    for(String v : values) {
      if(v != null && v.trim().length() > 0) {
        this.values.add(df.parse(v));
      }
    }
     
  }

  @Override
  public String getType() {
    return "date";
  }
}
