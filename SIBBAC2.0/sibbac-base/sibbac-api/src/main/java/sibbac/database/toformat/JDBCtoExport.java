package sibbac.database.toformat;

import java.io.IOException;
import java.sql.SQLException;

abstract class JDBCtoExport extends JDBCtoFormat {

  JDBCtoExport() throws SQLException {
    super(false);
  }

  @Override
  final void processRow() throws SQLException, IOException {
    if(singleFilter != null && skipRow()) {
      return;
    }
    processFilteredRow();
  }
  
  abstract void processFilteredRow() throws SQLException, IOException;
  
  private boolean skipRow() throws SQLException {
    final int columns;
    String content;
    
    columns = getColumns();
    for(int i = 1; i <= columns; i++) {
      content = resultSet.getString(i);
      if(resultSet.wasNull()) {
        continue;
      }
      if(content.toUpperCase().contains(singleFilter)) {
        return false;
      }
    }
    return true;
  }

}
