package sibbac.database;

import java.util.Objects;
import java.util.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractQueryFilter<F> implements QueryFilter {

  private static final String QUERY_DATE_FORMAT = "yyyy-MM-dd";

  private static final Logger LOG = LoggerFactory.getLogger(AbstractQueryFilter.class);

  protected final StringBuilder selectBuilder;

  protected final StringBuilder fromBuilder;

  protected final StringBuilder whereBuilder;

  protected final StringBuilder orderByBuilder;

  protected final StringBuilder groupByBuilder;

  protected final F filter;

  private final DateFormat queryDateFormat;

  private final String mainDateField;

  private final List<Date> mainDatesFounded;

  private int recordTotal;

  protected static void likeString(String fieldName, List<String> values, StringBuilder builder) {
    final int size;

    if (values.isEmpty())
      return;
    size = values.size();
    builder.append("AND (");
    for (int i = 0; i < size; i++) {
      if (i > 0)
        builder.append(" OR ");
      builder.append(String.format("%s LIKE ?", fieldName));
    }
    builder.append(") ");
  }

  protected static void inList(String fieldName, List<String> values, boolean equality, StringBuilder builder) {
    final int size;
    final String sign;

    if (values.isEmpty())
      return;
    sign = equality ? "=" : "!=";
    size = values.size();
    builder.append("AND (");
    for (int i = 0; i < size; i++) {
      if (i > 0)
        builder.append(equality ? " OR " : " AND ");
      builder.append(String.format("%s %s '%s'", fieldName, sign, values.get(i)));
    }
    builder.append(") ");
  }

  protected static void equals(String fieldName, String value, StringBuilder builder) {
    builder.append("AND ").append(fieldName).append(" = '").append(value).append("' ");
  }

  protected static void equals(String fieldName, Number value, StringBuilder builder) {
    builder.append("AND ").append(fieldName).append(" = ").append(value).append(' ');
  }

  protected static void equals(String fieldName, String value, boolean equality, StringBuilder builder) {
    builder.append("AND ").append(fieldName).append(equality ? " = " : " != ").append("'").append(value).append("' ");
  }

  protected static void equalsIfPresent(String fieldName, String value, StringBuilder builder) {
    if (value != null)
      equals(fieldName, value, builder);
  }

  protected static void equalsIfPresent(String fieldName, Number value, StringBuilder builder) {
    if (value != null)
      equals(fieldName, value, builder);
  }

  protected static void equalsIfPresent(String fieldName, String value, boolean equality, StringBuilder builder) {
    if (value != null) {
      builder.append("AND ").append(fieldName).append(equality ? " = " : " != ").append('\'').append(value)
          .append("' ");
    }
  }

  protected static void equalsIfPresent(String fieldName, Number value, boolean equality, StringBuilder builder) {
    if (value != null) {
      builder.append("AND ").append(fieldName).append(equality ? " = " : " != ").append(value).append(' ');
    }
  }

  private static Calendar nonNullDateToCalendar(Date date, String name) {
    final Calendar cal;

    Objects.requireNonNull(date, String.format("%s no puede ser nula", name));
    cal = Calendar.getInstance();
    cal.clear();
    cal.setTime(date);
    return cal;
  }

  private static Calendar dateOrTodayCalendar(Date date) {
    final Calendar cal, base;

    cal = Calendar.getInstance();
    cal.clear();
    if (date != null) {
      cal.setTime(date);
    }
    else {
      base = Calendar.getInstance();
      cal.set(base.get(Calendar.YEAR), base.get(Calendar.MONTH), base.get(Calendar.DAY_OF_MONTH));
    }
    return cal;
  }

  protected AbstractQueryFilter(F filter, String mainDateField) {
    this.filter = filter;
    this.mainDateField = mainDateField;
    selectBuilder = new StringBuilder();
    fromBuilder = new StringBuilder();
    whereBuilder = new StringBuilder();
    orderByBuilder = new StringBuilder();
    groupByBuilder = new StringBuilder();
    mainDatesFounded = new ArrayList<>();
    queryDateFormat = new SimpleDateFormat(QUERY_DATE_FORMAT);
  }

  public final int getRecordTotal() {
    return recordTotal;
  }

  @Override
  public final String getCountQuery() {
    final String query;

    if (buildCountQuery()) {
      fixBuilders();
      query = new StringBuilder(selectBuilder).append(' ').append(fromBuilder).append(' ').append(whereBuilder)
          .append(' ').append(groupByBuilder).toString();
      LOG.debug("Select Count generado por filtro: {}", query);
      cleanBuilders();
      return query;
    }
    return null;
  }

  @Override
  public final String getSelectQuery() {
    final String query;

    buildSelectQuery();
    fixBuilders();
    query = new StringBuilder(selectBuilder).append(' ').append(fromBuilder).append(' ').append(whereBuilder)
        .append(' ').append(groupByBuilder).append(' ').append(orderByBuilder).toString();
    LOG.debug("Select generado por filtro: {}", query);
    cleanBuilders();
    return query;
  }

  @Override
  public final int setCountResults(Map<Date, Long> countResults) {
    int count = 0;

    mainDatesFounded.clear();
    for (Map.Entry<Date, Long> entry : countResults.entrySet()) {
      count += entry.getValue();
      mainDatesFounded.add(entry.getKey());
    }
    recordTotal = count;
    return count;
  }

  protected void singleDate(Date date) {
    mainDatesFounded.clear();
    mainDatesFounded.add(date);
  }

  protected void splitMainDate(Date begin, Date end) {
    final Calendar cal, term;

    cal = nonNullDateToCalendar(begin, "begin");
    term = dateOrTodayCalendar(end);
    if (term.before(cal))
      return;
    whereBuilder.append("AND (").append(mainDateField).append(" = '").append(queryDateFormat.format(begin)).append("'");
    while (cal.before(term)) {
      cal.add(Calendar.DAY_OF_YEAR, 1);
      whereBuilder.append(" OR ").append(mainDateField).append(" = '").append(queryDateFormat.format(cal.getTime()))
          .append("'");
    }
    whereBuilder.append(") ");
  }

  protected void splitMainDateWithFounded() {
    final int size;

    Objects.requireNonNull(mainDatesFounded);
    size = mainDatesFounded.size();
    if (size == 0)
      return;
    whereBuilder.append("AND (");
    for (int i = 0; i < size; i++) {
      if (i > 0)
        whereBuilder.append(" OR ");
      whereBuilder.append(mainDateField).append(" = '").append(queryDateFormat.format(mainDatesFounded.get(i)))
          .append("'");
    }
    whereBuilder.append(") ");
  }

  protected void likeString(String fieldName, List<String> values) {
    likeString(fieldName, values, whereBuilder);
  }

  protected void inList(String fieldName, List<String> values, boolean equality) {
    inList(fieldName, values, equality, whereBuilder);
  }

  protected void equals(String fieldName, String value) {
    equals(fieldName, value, whereBuilder);
  }

  protected void equals(String fieldName, Number value) {
    equals(fieldName, value, whereBuilder);
  }

  protected void equals(String fieldName, String value, boolean eq) {
    equals(fieldName, value, eq, whereBuilder);
  }

  protected void equalsIfPresent(String fieldName, String value) {
    if (value != null) {
      equals(fieldName, value, whereBuilder);
    }
  }

  protected void equalsIfPresent(String fieldName, Character value) {
    if (value != null && value.charValue() != 0) {
      whereBuilder.append("AND ").append(fieldName).append(" = '").append(value).append("' ");
    }
  }

  protected void equalsIfPresent(String fieldName, Number value) {
    if (value != null) {
      equals(fieldName, value, whereBuilder);
    }
  }

  protected void equalsIfPresent(String fieldName, String value, boolean equality) {
    equalsIfPresent(fieldName, value, equality, whereBuilder);
  }

  protected void equalsIfPresent(String fieldName, Number value, boolean equality) {
    equalsIfPresent(fieldName, value, equality, whereBuilder);
  }

  protected void equalsOrGreaterIfPresent(String parameterName, Date date) {
    if (date != null)
      whereBuilder.append(String.format("AND %s >= '%s' ", parameterName, queryDateFormat.format(date)));
  }

  protected void equalsOrLowerIfPresent(String parameterName, Date date) {
    if (date != null)
      whereBuilder.append(String.format("AND %s <= '%s' ", parameterName, queryDateFormat.format(date)));
  }

  protected abstract boolean buildCountQuery();

  protected abstract void buildSelectQuery();

  private void fixBuilders() {
    if (whereBuilder.length() > 0)
      whereBuilder.replace(0, 3, "WHERE");
    if (groupByBuilder.length() > 0)
      groupByBuilder.insert(0, "GROUP BY ");
    if (orderByBuilder.length() > 0)
      orderByBuilder.insert(0, "ORDER BY ");
  }

  private void cleanBuilders() {
    selectBuilder.setLength(0);
    fromBuilder.setLength(0);
    whereBuilder.setLength(0);
    orderByBuilder.setLength(0);
    groupByBuilder.setLength(0);
  }

}
