package sibbac.database;

public interface FilterInfo {
  
  String getName();
  
  String getType();
  
  String getDesc();

}
