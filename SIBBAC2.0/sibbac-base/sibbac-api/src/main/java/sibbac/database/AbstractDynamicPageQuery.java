package sibbac.database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import sibbac.common.SIBBACBusinessException;

/**
 * @author xIS16630-Jonatan San Andres Gil
 *
 * @param <T>
 */
public abstract class AbstractDynamicPageQuery implements QueryFilter {

  private final List<SimpleDynamicFilter<?>> appliedFilters;

  private final List<SimpleDynamicFilter<?>> staticFilterValues;
  
  private final List<FilterInfo> filtersInfo;

  private final List<DynamicColumn> columns;
  
  private String singleFilter;
  
  private boolean isoFormat;

  protected AbstractDynamicPageQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns) {
    this(filters, columns, Collections.<SimpleDynamicFilter<?>>emptyList());
  }

  protected AbstractDynamicPageQuery(List<SimpleDynamicFilter<?>> filters, List<DynamicColumn> columns,
      List<SimpleDynamicFilter<?>> staticFilters) {
    this.appliedFilters = filters;
    this.columns = columns;
    this.staticFilterValues = staticFilters;
    filtersInfo = new ArrayList<>();
    filtersInfo.addAll(filters);
  }

  public abstract String getSelect();

  public abstract String getFrom();

  public abstract String getWhere();

  public abstract String getPostWhereConditions();

  public abstract String getGroup();

  public abstract String getHaving();

  protected abstract List<SimpleDynamicFilter<?>> getDynamicAvailableFilters();

  protected abstract List<SimpleDynamicFilter<?>> getStaticAvailableFilters();

  protected abstract void checkLocalMandatoryFiltersSet() throws SIBBACBusinessException;

  protected void checkMandatoryFiltersSet() throws SIBBACBusinessException {
    StringBuilder sb = new StringBuilder();
    if (getStaticAvailableFilters() != null && !getStaticAvailableFilters().isEmpty()) {
      if (getStaticAvailableFilters().size() == staticFilterValues.size()) {
        for (SimpleDynamicFilter<?> filter : staticFilterValues) {
          if (filter.getValues() == null || filter.getValues().isEmpty()) {
            sb.append(" Filter ").append(filter.getField()).append(" is mandatory.");
          }
        }
      }
      else {
        for (SimpleDynamicFilter<?> filter : getStaticAvailableFilters()) {
          if (filter.getValues() == null || filter.getValues().isEmpty()) {
            sb.append(" Filter ").append(filter.getField()).append(" is mandatory.");
          }
        }
      }
    }
    String msg = sb.toString();
    if (msg != null && !msg.trim().isEmpty()) {
      throw new SIBBACBusinessException(msg);
    }

    this.checkLocalMandatoryFiltersSet();
  }

  @Override
  public String getSelectQuery() throws SIBBACBusinessException {
    this.checkMandatoryFiltersSet();
    final StringBuilder query, dynamicFilters;
    String where = getWhere(), postConditions;

    if (where == null || where.trim().isEmpty()) {
      where = "";
    }
    dynamicFilters = new StringBuilder();
    for (SimpleDynamicFilter<?> filter : getStaticFilterValues()) {
      if (!filter.getValues().isEmpty()) {
        dynamicFilters.append("AND (").append(filter.getFilterSqlFormat()).append(") ");
      }
    }
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      if (!filter.getValues().isEmpty()) {
        dynamicFilters.append("AND (").append(filter.getFilterSqlFormat()).append(") ");
      }
    }
    postConditions = getPostWhereConditions().trim();
    if (!postConditions.isEmpty()) {
      dynamicFilters.append("AND ").append(postConditions).append(" ");
    }
    if (where.isEmpty()) {
      if (dynamicFilters.length() != 0) {
        dynamicFilters.replace(0, 3, "WHERE ");
      }
    }
    else {
      dynamicFilters.insert(0, where);
    }
    query = new StringBuilder(getSelect()).append(" ").append(getFrom()).append(" ").append(dynamicFilters).append(" ")
        .append(getGroup()).append(" ").append(getHaving());
    return query.toString();
  }

  public List<SimpleDynamicFilter<?>> getAppliedFilters() {
    return appliedFilters;
  }

  public List<SimpleDynamicFilter<?>> getStaticFilterValues() {
    return staticFilterValues;
  }

  @Override
  public List<DynamicColumn> getColumns() {
    return columns;
  }
  
  public void setSingleFilter(String singleFilter) {
    this.singleFilter = singleFilter;
  }
  
  @Override
  public String getSingleFilter() {
    return singleFilter;
  }

  @Override
  public String getCountQuery() {
    return null;
  }

  @Override
  public int setCountResults(Map<Date, Long> dates) {
    return 0;
  }

  @Override
  public boolean isCountNecessary() {
    return false;
  }

  @Override
  public final boolean isIsoFormat() {
    return isoFormat;
  }
  
  public final void setIsoFormat(boolean isoFormat) {
    this.isoFormat = isoFormat;
  }
  
  public List<FilterInfo> getFiltersInfo() {
    return filtersInfo;
  }
  
  public void fillDynamicQuery(Map<String, String[]> params) throws Exception {
    String fieldName;
    for (SimpleDynamicFilter<?> filter : getAppliedFilters()) {
      fieldName = filter.getFieldName();
      if (params.containsKey(fieldName)) {
        filter.setValues(params.get(fieldName));
      }
    }
  }
  

}
