package sibbac.database;

import java.util.Map;

import sibbac.common.SIBBACBusinessException;

import java.util.Date;
import java.util.List;

public interface QueryFilter {
    
    String getCountQuery();
    
    String getSelectQuery() throws SIBBACBusinessException;
    
    int setCountResults(Map<Date,Long> dates);
    
    boolean isCountNecessary();
    
    boolean isIsoFormat();
    
    String getSingleFilter();
    
    List<DynamicColumn> getColumns();

}
