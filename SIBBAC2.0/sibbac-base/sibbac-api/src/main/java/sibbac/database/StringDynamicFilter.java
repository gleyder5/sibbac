package sibbac.database;

public class StringDynamicFilter extends SimpleDynamicFilter<String> {

  private static final long serialVersionUID = 8651860688765840632L;

  public StringDynamicFilter(String field, String messageKey) {
    super(field, messageKey, null);
  }

  @Override
  public void setValues(String[] values) {
    for(String v : values) {
      if(v != null && v.trim().length() > 0) {
        this.values.add(v);
      }
    }
  }

}
