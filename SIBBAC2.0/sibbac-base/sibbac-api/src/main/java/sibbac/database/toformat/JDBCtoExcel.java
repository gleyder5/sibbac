package sibbac.database.toformat;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public final class JDBCtoExcel extends JDBCtoExport {
  
  private final Workbook workbook;
  
  private final Sheet sheet;
  
  private final CellStyle dateStyle;
  
  private int rowNumber;
  
  JDBCtoExcel() throws SQLException {
    final CreationHelper createHelper;
    
    workbook = new XSSFWorkbook();
    createHelper = workbook.getCreationHelper();
    sheet = workbook.createSheet("Página 1");
    dateStyle = workbook.createCellStyle();
    dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyy"));
    rowNumber = 0;
  }

  @Override
  void prepareStream() throws IOException {
    final Row row;
    final int columns;
    
    columns = getColumns();
    row = sheet.createRow(rowNumber);
    for(int i = 0; i < columns; i++)  {
      row.createCell(i).setCellValue(columnLabel(i + 1));
    }
  }

  @Override
  void processFilteredRow() throws SQLException, IOException {
    final Row row;
    final int columns;
    Cell cell;
    String svalue;
    
    columns = getColumns();
    row = sheet.createRow(++rowNumber);
    for(int i = 0; i < columns; i++) {
      svalue = resultSet.getString(i + 1);
      if(resultSet.wasNull()) {
        continue;
      }
      cell = row.createCell(i);
      switch(metaData.getColumnType(i + 1)){
        case Types.BIGINT:
        case Types.INTEGER:
        case Types.SMALLINT:
        case Types.TINYINT:
        case Types.NUMERIC:
        case Types.DECIMAL:
          cell.setCellValue(resultSet.getDouble(i + 1));
          break;
        case Types.DATE:
        case Types.TIME:
        case Types.TIMESTAMP:
          cell.setCellStyle(dateStyle);
          cell.setCellValue(resultSet.getDate(i + 1));
          break;
        default:
          cell.setCellValue(svalue);
      }
    }
  }

  @Override
  void endStream() throws IOException {
    workbook.write(outputStream);
  }
  
}
