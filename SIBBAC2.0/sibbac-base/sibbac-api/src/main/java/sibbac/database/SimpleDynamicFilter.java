package sibbac.database;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import sibbac.common.SIBBACBusinessException;

/**
 * @author xIS16630-Jonatan San Andres Gil
 * 
 *
 * @param <T>
 */
public abstract class SimpleDynamicFilter<T> implements FilterInfo, Serializable {

  private static final String NULL = "NULL";

  public enum SimpleDynamicFilterComparatorType {
    EQ, NEQ, GT, GTE, LT, LTE, LIKE, NOT_LIKE, START_WITH, NOT_START_WITH, END_WITH, NOT_END_WITH, BETWEEN;
  }

  /**
   * 
   */
  private static final long serialVersionUID = 1956460139203824105L;

  private final String field;
  
  private final String messageKey;
  
  private final String format;
  
  protected List<T> values = new ArrayList<>();
  
  private boolean in;
  
  private boolean and;
  
  private SimpleDynamicFilterComparatorType comparator;

  public SimpleDynamicFilter(String field, String messageKey, String format) {
    this.field = field;
    this.messageKey = messageKey;
    this.format = format;
    comparator = SimpleDynamicFilterComparatorType.EQ;
  }
  
  public abstract void setValues(String[] values) throws ParseException;

  public String getField() {
    return field;
  }
  
  public String getFieldName() {
    final String[] split;
    
    if(field.contains(".")) {
      split = field.split("\\.");
      return split[1];
    }
    return field;
  }

  public String getMessageKey() {
    return messageKey;
  }

  public List<T> getValues() {
    return values;
  }

  public boolean isIn() {
    return in;
  }

  public boolean isAnd() {
    return and;
  }

  public void setValues(List<T> values) {
    this.values = values;
  }

  public void setIn(boolean in) {
    this.in = in;
  }

  public void setAnd(boolean and) {
    this.and = and;
  }

  public SimpleDynamicFilterComparatorType getComparator() {
    return comparator;
  }

  public void setComparator(SimpleDynamicFilterComparatorType comparator) {
    this.comparator = comparator;
  }

  private List<String> getStringFilterValueForSql() throws SIBBACBusinessException {
    List<String> res = new ArrayList<String>();
    Collection<String> resNormal = new HashSet<String>();
    Collection<String> resNull = new HashSet<String>();
    String val;
    SimpleDateFormat sdf = null;
    if (format != null && !format.trim().isEmpty()) {
      if (!values.isEmpty() && values.get(0) instanceof Date) {
        sdf = new SimpleDateFormat(format);
      }
    }

    if (comparator == SimpleDynamicFilterComparatorType.BETWEEN) {
      int i = 0;
      while (i < values.size()) {
        if (values.get(0) instanceof Date) {
          val = String.format("'%s' and '%s'", sdf.format(values.get(0 + i)), sdf.format(values.get(1 + i)));
        }
        else {
          if (format != null && !format.trim().isEmpty()) {
            val = String.format("'%s' and '%s'", String.format(format, values.get(0 + i)),
                String.format(format, values.get(1 + i)));
          }
          else {
            val = String.format("'%s' and '%s'", values.get(0 + i), values.get(1 + i));
          }

        }
        i += 2;
        resNormal.add(val);
      }
      res.addAll(resNormal);
    }
    else {

      for (T t : values) {
        if (t != null) {
          if (t instanceof Date) {
            val = String.format("'%s'", sdf.format(t));
          }
          else {
            if (t instanceof String) {
              if ((((String) t).trim().toUpperCase().contains("' OR ") || ((String) t).trim().toUpperCase()
                  .contains("' AND "))) {
                throw new SIBBACBusinessException(String.format(
                    "INCIDENCIA- POSIBLE INTENTO DE INYECCION DE SQL, VALOR INYECTADO ===> ---- %s ----", t));
              }
            }
            if (format != null && !format.trim().isEmpty()) {
              val = String.format("'%s'", String.format(format, t));
            }
            else {
              val = String.format("'%s'", t.toString());
            }
          }
          resNormal.add(val);
        }
        else {
          val = NULL;
          resNull.add(val);
        }

      }
      res.addAll(resNull);
      res.addAll(resNormal);
    }

    resNull.clear();
    resNormal.clear();
    return res;
  }

  public String getFilterSqlFormat() throws SIBBACBusinessException {
    String res = "";

    if (!values.isEmpty()) {
      this.checkValuesAndComparator();
      List<String> valuesSt = getStringFilterValueForSql();
      if (valuesSt.size() == 1) {
        res = getFilterValueForSql(valuesSt.get(0));
      }
      else {
        StringBuilder sb = new StringBuilder(" ( ");
        int i = 0;
        for (String value : valuesSt) {
          if (comparator != null) {
            switch (comparator) {
            // EQ, NEQ, GT, GTE, LT, LTE, LIKE, NOT_LIKE, START_WITH,
            // NOT_START_WITH, END_WITH, NOT_END_WITH, BETWEEN;
            // or's
              case EQ:
              case LIKE:
              case START_WITH:
              case END_WITH:
              case BETWEEN:
                if (i++ > 0) {
                  sb.append(" or ").append(getFilterValueForSql(value)).append(" ");
                }
                else {
                  sb.append(" ").append(getFilterValueForSql(value));
                }
                break;

              default:
                if (i++ > 0) {
                  sb.append(" and ").append(getFilterValueForSql(value)).append(" ");
                }
                else {
                  sb.append(" ").append(getFilterValueForSql(value));
                }
                break;
            }
          }
          else {
            if (i++ > 0) {
              if (in) {
                sb.append(" or ").append(getFilterValueForSql(value)).append(" ");
              }
              else {
                sb.append(" and ").append(getFilterValueForSql(value)).append(" ");
              }

            }
            else {
              sb.append(" ").append(getFilterValueForSql(value));
            }
          }
        }
        sb.append(" ) ");
        res = sb.toString();
      }
    }
    return res;
  }

  private void checkValuesAndComparator() throws SIBBACBusinessException {
    if (!values.isEmpty()) {
      T t = values.get(0);
      if (!(t instanceof String)
          && comparator != null
          && (comparator == SimpleDynamicFilterComparatorType.LIKE
              || comparator == SimpleDynamicFilterComparatorType.NOT_LIKE
              || comparator == SimpleDynamicFilterComparatorType.START_WITH
              || comparator == SimpleDynamicFilterComparatorType.NOT_START_WITH
              || comparator == SimpleDynamicFilterComparatorType.END_WITH || comparator == SimpleDynamicFilterComparatorType.NOT_END_WITH)) {
        throw new SIBBACBusinessException(String.format(
            "INCIDENCIA-Los comparadores '%s' de cadena de texto solo se pueden aplicar a cadenas de texto %s.",
            comparator, values));
      }

      if (comparator != null && comparator == SimpleDynamicFilterComparatorType.BETWEEN && values.size() >= 2
          && values.size() % 2 != 0) {
        throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s necesita pares de valores %s.",
            comparator, values));
      }
    }
  }

  private String getFilterValueForSql(String value) throws SIBBACBusinessException {
    String res = "";
    if (comparator != null) {
      switch (comparator) {
        case EQ:
          if (NULL.equals(value)) {
            res = String.format(" %s is %s ", field, NULL);
          }
          else {
            res = String.format(" %s = %s ", field, value);
          }
          break;
        case NEQ:
          if (NULL.equals(value)) {
            res = String.format(" %s not is %s ", field, NULL);
          }
          else {
            res = String.format(" %s <> %s ", field, value);
          }
          break;
        case GT:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s > %s ", field, value);
          }
          break;
        case GTE:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s >= %s ", field, value);
          }
          break;
        case LT:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s < %s ", field, value);
          }
          break;
        case LTE:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s <= %s ", field, value);
          }
          break;
        case BETWEEN:
          if (value.toUpperCase().contains(NULL)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s between %s ", field, value);
          }
          break;
        case LIKE:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s like %s ", field, String.format("'\u0025%s\u0025'", value.replace("'", "")));
          }
          break;
        case NOT_LIKE:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s not like %s ", field, String.format("'\u0025%s\u0025'", value.replace("'", "")));
          }
          break;
        case START_WITH:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s like %s ", field, String.format("'%s\u0025'", value.replace("'", "")));
          }
          break;
        case NOT_START_WITH:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s not like %s ", field, String.format("'%s\u0025'", value.replace("'", "")));
          }
          break;
        case END_WITH:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s like %s ", field, String.format("'\u0025%s'", value.replace("'", "")));
          }
          break;
        case NOT_END_WITH:
          if (NULL.equals(value)) {
            throw new SIBBACBusinessException(String.format("INCIDENCIA- El comparador %s no admite nulos", comparator));
          }
          else {
            res = String.format(" %s not like %s ", field, String.format("'\u0025%s'", value.replace("'", "")));
          }
          break;

        default:
          break;
      }
    }
    else {
      if (in) {
        if (NULL.equals(value)) {
          res = String.format(" %s is %s ", field, NULL);
        }
        else {
          res = String.format(" %s = %s ", field, value);
        }
      }
      else {
        if (NULL.equals(value)) {
          res = String.format(" %s not is %s ", field, NULL);
        }
        else {
          res = String.format(" %s <> %s ", field, value);
        }
      }
    }
    return res;
  }

  @Override
  public String toString() {
    return String.format("SimpleDynamicFilter [field=%s, messageKey=%s, values=%s, in=%s, and=%s]", field, messageKey,
        values, in, and);
  }
  
  public String getFormat() {
    return format;
  }

  @Override
  public String getName() {
    return getFieldName();
  }

  @Override
  public String getType() {
    return "text";
  }

  @Override
  public String getDesc() {
    return getMessageKey();
  }

}
