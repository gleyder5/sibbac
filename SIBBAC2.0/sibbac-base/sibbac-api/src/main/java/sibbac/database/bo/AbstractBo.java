package sibbac.database.bo;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.orm.jpa.JpaTransactionManager;

public class AbstractBo<TYPE, PK extends Serializable, DAO extends JpaRepository<TYPE, PK>> {

  protected static final Logger LOG = LoggerFactory.getLogger(AbstractBo.class);

  @Autowired
  protected DAO dao;

  @Autowired
  private EntityManagerFactory entityManagerFactory;

  private Class<?> entityClass;

  private int batchSize = 100;

  public AbstractBo() {
    try {
      ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
      Type[] types = parameterizedType.getActualTypeArguments();
      this.entityClass = (Class<?>) types[0];
    }
    catch (Exception e) {
      LOG.warn("Unable to detect TYPE class name!");
    }
  }

  // -------------------------------------------- Methods from: CrudRepository

  public List<TYPE> findAll() {
    return this.dao.findAll();
  };

  public TYPE findById(final PK id) {
    return this.dao.findOne(id);
  }

  public long count() {
    return this.dao.count();
  }

  public TYPE save(final TYPE entity) {
    return this.dao.save(entity);
  }

  public List<TYPE> save(final Iterable<TYPE> entities) {
    final List<TYPE> result;
    // Workaround de un bug de compilacion con Eclipse Oxygen.
    result = new ArrayList<>();
    for (TYPE e : entities) {
      result.add(dao.save(e));
    }
    return result;
  }

  public boolean exists(final PK id) {
    return this.dao.exists(id);
  }

  public List<TYPE> findAll(final Iterable<PK> ids) {
    return this.dao.findAll(ids);
  }

  public void delete(final PK id) {
    this.dao.delete(id);
  }

  public void delete(final TYPE entity) {
    this.dao.delete(entity);
  }

  public void delete(final Iterable<TYPE> entities) {
    this.dao.delete(entities);
  }

  public void deleteAll() {
    this.dao.deleteAll();
  }

  // ------------------ Methods from: JpaRepository/PagingAndSortingRepository
  // http://springinpractice.com/2012/05/11/pagination-and-sorting-with-spring-data-jpa

  public List<TYPE> findAll(final Sort sort) {
    return this.dao.findAll(sort);
  }

  public Page<TYPE> findAll(final Pageable pageable) {
    return this.dao.findAll(pageable);
  }

  public void flush() {
    this.dao.flush();
  }

  public TYPE saveAndFlush(final TYPE entity) {
    return this.dao.saveAndFlush(entity);
  }

  public void deleteInBatch(final Iterable<TYPE> entities) {
    this.dao.deleteInBatch(entities);
  }

  public void deleteAllInBatch() {
    this.dao.deleteAllInBatch();
  }

  // ------------------ Methods for historic data

  public DAO getDao() {
    return this.dao;
  }

  public void setDao(DAO dao) {
    this.dao = dao;
  }

  public List<Number> getAllRevisions(final PK id) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    AuditReader reader = AuditReaderFactory.get(entityManager);

    return reader.getRevisions(this.entityClass, id);
  }

  public Date getRevisionDate(final Number revision) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    AuditReader reader = AuditReaderFactory.get(entityManager);
    return reader.getRevisionDate(revision);
  }

  @SuppressWarnings("unchecked")
  public TYPE getRevisionForId(final PK id, final Number revision) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    AuditReader reader = AuditReaderFactory.get(entityManager);

    return (TYPE) reader.find(this.entityClass, id, revision);
  }

  public <T> void bulkSave(Collection<T> entities) {
    EntityManager entityManager = entityManagerFactory.createEntityManager();
    int i = 0;
    for (T t : entities) {
      entityManager.persist(t);
      i++;
      if (i % batchSize == 0) {
        // Flush a batch of inserts and release memory.
        entityManager.flush();
        entityManager.clear();
        LOG.debug("Flush de " + t.getClass().getSimpleName() + " de " + batchSize + " Elementos");
      }
    }
  }
}
