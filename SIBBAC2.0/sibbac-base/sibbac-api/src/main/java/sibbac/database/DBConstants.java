package sibbac.database;

/**
 * Interface with all constants of the project.
 *
 *
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface DBConstants {

  // ------------------------------------------------ COMMON CONSTANTS

  /**
   * For logging purposes only.
   */
  public final static String SEPARATOR = "================================================================================================";

  /**
   * The constant value for the tab character.
   *
   * Value: {@value}
   */
  public final static String TAB = "\t";

  /**
   * The constant value for the local file separator character.
   *
   * Value: {@value}
   */
  public final static String FS = System.getProperty("file.separator");

  /**
   * The constant value for the new line character.
   *
   * Value: {@value}
   */
  public final static String NL = System.getProperty("line.separator");

  /**
   * The constant value for the user local folder.
   *
   * Value: {@value}
   */
  public final static String FOLDER = System.getProperty("user.dir");

  // --------------------------------------------------- APP CONSTANTS

  /**
   * Where to look-up for localized string messages.
   */
  public final static String BUNDLE_MESSAGES = "i18n.Messages";
  
  // ------------------------------------------------- TABLE CONSTANTS

  public final static String TABLE_PREFIX = "TMCT0_";

  public final static String TABLE_PREFIX_CLI = "TMCT0CLI_";

  public final static String AUDIT_USER_DEFAULT = "process";

  public interface CONTABLES {
    public final static String CUENTA_CONTABLE = "CUENTA_CONTABLE";
    public final static String CUENTA_BANCARIA = "CUENTA_BANCARIA";
    public final static String CUENTA_AUXILIAR = "CUENTA_AUXILIAR";
    public final static String AUXILIAR = "AUXILIAR";
    public final static String APUNTE_CONTABLE = "APUNTE_CONTABLE";
    public final static String APUNTE_CONTABLE_ALC = "APUNTE_CONTABLE_ALC";
    public final static String CIERRE_CONTABLE = "CIERRE_CONTABLE";
    public final static String ALC_ESTADOCONT = "ALC_ESTADOCONT";
    public final static String APUNTE_DETALLE = "APUNTE_DETALLE";
    public final static String APUNTE_FECHA = "APUNTE_FECHA";
    public final static String APUNTE_FECHA_DETALLE = "APUNTE_FECHA_DETALLE";
  }

  /**
   * Interface with constants for entities of "GROUP_FIXML".
   */

  public interface FIXML {

    public final static String FICHEROSXML = TABLE_PREFIX + "FIXMLFILES";
    public final static String ESQUEMASXSD = TABLE_PREFIX + "FIXMLXSDS";

  }

  /**
   * Interface with constants for entities of "INTERMEDIARIO_FINANCIERO".
   */

  public interface CUADRE_ECC {

    public final static String CUADRE_ECC_EJECUCIONES = TABLE_PREFIX + "CUADRE_ECC_EJECUCIONES";
    public final static String CUADRE_ECC_REFERENCIAS = TABLE_PREFIX + "CUADRE_ECC_REFERENCIAS";
    public final static String CUADRE_ECC_TITULARES = TABLE_PREFIX + "CUADRE_ECC_TITULARES";
    public final static String CUADRE_ECC_SIBBAC = TABLE_PREFIX + "CUADRE_ECC_SIBBAC";
    public final static String CUADRE_ECC_LOG = TABLE_PREFIX + "CUADRE_ECC_LOGS";

  }

  /**
   * Interface with constants for entities of "INTERMEDIARIO_FINANCIERO".
   */

  public interface EURO_CCP {

    // 409-410
    public final static String PROCESSED_UNSETTLED_MOVEMENT = TABLE_PREFIX + "EURO_CCP_PROCESSED_UNSETTLED_MOVEMENT";
    // 411
    public final static String PROCESSED_SETTLED_MOVEMENT = TABLE_PREFIX + "EURO_CCP_PROCESSED_SETTLED_MOVEMENT";
    // 415
    public final static String AGGREGATES_NET_SETTLEMENTS = TABLE_PREFIX + "EURO_CCP_AGGREGATES_NET_SETTLEMENTS";
    // 420
    public final static String UNSETTLED_POSITION = TABLE_PREFIX + "EURO_CCP_UNSETTLED_POSITION";
    // 421
    public final static String SETTLED_POSITION = TABLE_PREFIX + "EURO_CCP_SETTLED_POSITION";
    // 450
    public final static String SETTLEMENT_INSTRUCTION = TABLE_PREFIX + "EURO_CCP_SETTLEMENT_INSTRUCTION";
    // 600
    public final static String PROCESSED_MONEY_MOVEMENT = TABLE_PREFIX + "EURO_CCP_PROCESSED_MONEY_MOVEMENT";
    // 610
    public final static String CASH_POSITION = TABLE_PREFIX + "EURO_CCP_CASH_POSITION";
    // 910
    public final static String TRAILER = TABLE_PREFIX + "EURO_CCP_TRAILER";

    public final static String FILE_SENT = TABLE_PREFIX + "EURO_CCP_FILES_SENT";

    public final static String OWNERSHIP_REFERENCE_STATIC_DATA = TABLE_PREFIX
        + "EURO_CCP_OWNERSHIP_REFERENCE_STATIC_DATA";

    public final static String OWNERSHIP_REFERENCE_STATIC_DATA_ENVIO_TITULARIDAD = TABLE_PREFIX
        + "EURO_CCP_OWNERSHIP_REFERENCE_STATIC_DATA_ENVIO_TITULARIDAD";

    public final static String EXECUTION_REALIGMENT = TABLE_PREFIX + "EURO_CCP_EXECUTION_REALIGMENT";

    public final static String FILE_TYPES = TABLE_PREFIX + "EURO_CCP_FILE_TYPES";

    public final static String OWNERSHIP_REPORTING_GROSS_EXECUTION = TABLE_PREFIX
        + "EURO_CCP_OWNERSHIP_REPORTING_GROSS_EXECUTION";

    public final static String OWNERSHIP_REPORTING_GROSS_EXECUTION_PARENT_REFERENCE = TABLE_PREFIX
        + "EURO_CCP_OWNERSHIP_REPORTING_GROSS_EXECUTION_PARENT_REFERENCE";

    public final static String ERROR_CODE = TABLE_PREFIX + "EURO_CCP_ERROR_CODE";

    public final static String CLEARED_GROSS_TRADE_SPAIN = TABLE_PREFIX + "EURO_CCP_CLEARED_GROSS_TRADE_SPAIN";

    public final static String SPANISH_SETTLEMENT_INSTRUCTION = TABLE_PREFIX
        + "EURO_CCP_SPANISH_SETTLEMENT_INSTRUCTION";
  }

  public interface SINECC {

    public final static String NUMERO_OPERACION_DCV = TABLE_PREFIX + "NUMERO_OPERACION_DCV";
  }

  /**
   * Interface with constants for entities of "Wrappers".
   */
  public interface WRAPPERS {

    public final static String ALC_ORDEN = TABLE_PREFIX + "ALC_ORDEN";
    public final static String ALC_ORDEN_MERCADO = TABLE_PREFIX + "ALC_ORDEN_MERCADO";
    public final static String ALIAS = TABLE_PREFIX + "ALIAS";
    public final static String ALIAS_DIRECCION = TABLE_PREFIX + "ALIASDIRECCION";
    public final static String CONTACTO = TABLE_PREFIX + "CONTACTO";
    public final static String ALIAS_SUBCUENTA = TABLE_PREFIX + "ALIASSUBCUENTA";
    public final static String FACTURA = TABLE_PREFIX + "FACTURA";
    public final static String FACTURA_SUGERIDA = TABLE_PREFIX + "FACTURA_SUGERIDA";
    public final static String FACTURA_RECTIFICATIVA = TABLE_PREFIX + "FACTURA_RECTIFICATIVA";
    public final static String GRUPO_IMPOSITIVO = TABLE_PREFIX + "GRUPO_IMPOSITIVO";
    public final static String IDIOMA = TABLE_PREFIX + "IDIOMA";
    public final static String IDENTIFICADOR = TABLE_PREFIX + "IDENTIFICADOR";
    public final static String FACTURA_DETALLE = TABLE_PREFIX + "FACTURA_DETALLE";
    public final static String ALC_ORDEN_FACTURA_DETALLE = TABLE_PREFIX + "ALC_ORDEN_FACTURA_DETALLE";
    public final static String MONEDA = TABLE_PREFIX + "MONEDA";
    public final static String PERIODO = TABLE_PREFIX + "PERIODO";
    public final static String TIPO_DOCUMENTO = TABLE_PREFIX + "TIPODOCUMENTO";
    public final static String ALIAS_RECORDATORIO = TABLE_PREFIX + "ALIASRECORDATORIO";
    public final static String TEXTO = TABLE_PREFIX + "TEXTO";
    public final static String TEXTO_IDIOMA = TABLE_PREFIX + "TEXTO_IDIOMA";
    public final static String FACTURAS_PENDIENTES_RECORDATORIO = TABLE_PREFIX + "FACTURASPENDIENTESRECORDATORIO";
    public final static String PLANTILLA = TABLE_PREFIX + "PLANTILLA";
    public final static String ENTIDAD_REGISTRO = TABLE_PREFIX + "ENTIDAD_REGISTRO";
    public final static String TIPO_ENTIDAD_REGISTRO = TABLE_PREFIX + "TIPO_ENTIDAD_REGISTRO";
    public final static String TIOP_CUENTA_LIQUIDACION = TABLE_PREFIX + "TIPO_CUENTA_LIQUIDACION";
    public final static String TIPO_CUENTA_CONCILIACION = TABLE_PREFIX + "TIPO_CUENTA_CONCILIACION";
    public final static String MERCADO = TABLE_PREFIX + "MERCADO";
    public final static String RELACION_CUENTA_MERCADO = TABLE_PREFIX + "RELACION_CUENTA_MERCADO";
    public final static String RELACION_CUENTACOMP_MERCADO = TABLE_PREFIX + "RELACION_CUENTACOMP_MERCADO";
    public final static String NUMERADOR = TABLE_PREFIX + "NUMERADOR";
    public final static String PARAMETRO = TABLE_PREFIX + "PARAMETRO";
    public final static String COBROS = TABLE_PREFIX + "COBRO";
    public final static String ETIQUETA = TABLE_PREFIX + "ETIQUETA";
    public final static String LINEA_DE_COBRO = TABLE_PREFIX + "COBRO_LINEA";
    public final static String CUENTAS_DE_COMPENSACION = TABLE_PREFIX + "CUENTAS_DE_COMPENSACION";
    public final static String SISTEMA_LIQUIDACION = TABLE_PREFIX + "SISTEMA_LIQUIDACION";
    public final static String CODIGO_ERROR = TABLE_PREFIX + "CODIGO_ERROR";
    public final static String CODIGO_TPIDENTI = TABLE_PREFIX + "CODIGO_TPIDENTI";
    public final static String CODIGO_TPNACTIT = TABLE_PREFIX + "CODIGO_TPNACTIT";
    public final static String CODIGO_TPSOCIED = TABLE_PREFIX + "CODIGO_TPSOCIED";
    public final static String CODIGO_TPTIPREP = TABLE_PREFIX + "CODIGO_TPTIPREP";
    public final static String AFI_ERROR = TABLE_PREFIX + "AFI_ERROR";
    public final static String AFI_ERROR_CODIGO_ERROR = TABLE_PREFIX + "AFI_ERROR_CODIGO_ERROR";
    public final static String DESGLOSE_RECORD_BEAN = TABLE_PREFIX + "DESGLOSE_RECORD_BEAN";
    public final static String PARTENON_RECORD_BEAN = TABLE_PREFIX + "PARTENON_RECORD_BEAN";
    public final static String DESGLOSE = TABLE_PREFIX + "DESGLOSE";
    public final static String DESGLOSE_TITULAR = TABLE_PREFIX + "DESGLOSE_TITULAR";
    public final static String SWI = "TMCT0SWI";
    public static final String AFI_ASCII = TABLE_PREFIX + "AFI_ASCII_PERMITIDO";
    public final static String PAISES = "TMCT0PAISES";
    public final static String PROVINCIAS = "TMCT0PROVINCIAS";
    public final static String TFI = "TMCT0TFI";

    public final static String ADDRESSES = TABLE_PREFIX + "ADDRESSES";
    public final static String MIFID = TABLE_PREFIX_CLI + "MIFID";
  }

  /**
   * Interface with constants for entities of "Periodicidades".
   */
  public interface PERIODICIDADES {

    public final static String REGLAS_PERIODICIDAD = TABLE_PREFIX + "REGLASPERIODICIDAD";
    public final static String PARAMETRIZACIONES_PERIODICIDAD = TABLE_PREFIX + "PARAMETRIZACIONESPERIODICIDAD";

    public final static String DIA_SEMANA = TABLE_PREFIX + "DIASEMANA";
    public final static String MES = TABLE_PREFIX + "MES";

    public final static String FESTIVOS = TABLE_PREFIX + "FESTIVOS";
  }

  /**
   * Interface with constants for entities of "Servicios".
   */
  public interface SERVICIOS {

    public final static String SERVICIOS = TABLE_PREFIX + "SERVICIOS";
    public final static String REGLAS_LANZADOR_SERVICIOS = TABLE_PREFIX + "REGLASLANZADORSERVICIOS";
    public final static String SERVICIOS_CONTACTOS = TABLE_PREFIX + "SERVICIOS_CONTACTOS";
  }

  /**
   * Interface with constants for entities of "Fallidos".
   */
  public interface FALLIDOS {
    public static final String FALLIDOS = TABLE_PREFIX + "FALLIDOS";
    public static final String FALLIDOS_ALC = TABLE_PREFIX + "FALLIDOS_ALC";

    // public static final String FALLIDOS_IL = TABLE_PREFIX + "FALLIDOS_IL";
    public static final String FALLIDOS_IL = TABLE_PREFIX + "INSTRUCCION_LIQUIDACION_FALLIDA";

    public static final String FALLIDOS_FIXML = TABLE_PREFIX + "FIXML_FALLIDOS";
    // public static final String FALLIDOS_FIXML = TABLE_PREFIX +
    // "FALLIDOS_FIXML";

    public static final String FALLIDOS_MOVIMIENTOS = TABLE_PREFIX + "MOVIMIENTOS_FALLIDOS";
    // public static final String FALLIDOS_MOVIMIENTOS = TABLE_PREFIX +
    // "FALLIDOS_MOVIMIENTOS";

    public static final String TIPO_MOVIMIENTOS_FALLIDOS = TABLE_PREFIX + "TIPO_MOVIMIENTOS_FALLIDOS";

    // public static final String AJUSTE = TABLE_PREFIX + "AJUSTE";

    public static final String H47_OPERACION = TABLE_PREFIX + "H47_OPERACIONES";
    public static final String H47_TITULARES = TABLE_PREFIX + "H47_TITULARES";
    public static final String H47_CONSTANTES = TABLE_PREFIX + "H47_CONSTANTES";
    public static final String H47_ERRORCNMV = TABLE_PREFIX + "H47_ERRORESCNMV";
  } // FALLIDOS

  /**
   * Interface with constants for entities of "Conciliacion"
   * 
   * @author fjarquellada
   *
   */
  public interface CONCILIACION {

    public final String TMCT0_CUENTA_VIRTUAL = TABLE_PREFIX + "CUENTA_VIRTUAL";
    public final String TMCT0_MOVIMIENTO_CUENTA_VIRTUAL = TABLE_PREFIX + "MOVIMIENTO_CUENTA_VIRTUAL";
    public final String TMCT0_MOVIMIENTO_CUENTA_VIRTUAL_S3 = TABLE_PREFIX + "MOVIMIENTO_CUENTA_VIRTUAL_S3";
    public final String TMCT0_NORMA43_FICHERO = TABLE_PREFIX + "NORMA43_FICHERO";
    public final String TMCT0_NORMA43_MOVIMIENTO = TABLE_PREFIX + "NORMA43_MOVIMIENTO";
    public final String TMCT0_NORMA43_CONCEPTO = TABLE_PREFIX + "NORMA43_CONCEPTO";
    public final String TMCT0_NORMA43_DESCUADRES = TABLE_PREFIX + "NORMA43_DESCUADRES";
    public final String TMCT0_ENTIDAD_REGISTRO = TABLE_PREFIX + "ENTIDAD_REGISTRO";
    public final String TMCT0_TIPO_ER = TABLE_PREFIX + "TIPO_ER";
    public final String TMCT0_TIPO_NETEO = TABLE_PREFIX + "TIPO_NETEO";
    public final String TMCT0_SALDO_EFECTIVO_CUENTA = TABLE_PREFIX + "SALDO_EFECTIVO_CUENTA";
    public final String TMCT0_SALDO_TITULO_CUENTA = TABLE_PREFIX + "SALDO_TITULO_CUENTA";
    public final String TMCT0_DIFERENCIA_CONCILIACION_TITULO = TABLE_PREFIX + "DIFERENCIA_CONCILIACION_TITULO";
    public final String TMCT0_DIFERENCIA_CONCILIACION_EFECTIVO = TABLE_PREFIX + "DIFERENCIA_CONCILIACION_EFECTIVO";
    public final String TMCT0_CUENTA_LIQUIDACION = TABLE_PREFIX + "CUENTA_LIQUIDACION";
    public final String TMCT0_MOVIMIENTO_MANUAL = TABLE_PREFIX + "MOVIMIENTO_MANUAL";
    public final String TMCT0_MOVIMIENTO_CUENTA_ALIAS = TABLE_PREFIX + "MOVIMIENTO_CUENTA_ALIAS";
    public final String TMCT0_SALDO_INICIAL_CUENTA = TABLE_PREFIX + "SALDO_INICIAL_CUENTA";
    public final static String TIPO_MOVIMIENTO_CONCILIACION = TABLE_PREFIX + "TIPO_MOVIMIENTO_CONCILIACION";
    public final static String FICHERO_CUENTA_ECC = TABLE_PREFIX + "FICHERO_CUENTA_ECC";

  }

  /**
   * Interface with constants for entities of "Clearing-netting".
   */
  public interface CLEARING {

    public final static String NETTING = TABLE_PREFIX + "NETTING";
    public final static String S3LIQUIDACION = TABLE_PREFIX + "S3LIQUIDACION";
    public final static String S3ERRORES = TABLE_PREFIX + "S3ERRORES";
    public final static String DATOSDISCREPANTES = TABLE_PREFIX + "DATOS_DISCREPANTES";
    public final static String S3INTERESES = TABLE_PREFIX + "S3INTERESES";
    public final static String INTERESESDEMORA = TABLE_PREFIX + "INTERESES_DEMORA";
    public final static String MINIMOIMPORTEDEMORA = TABLE_PREFIX + "MINMO_IMPORTE_DEMORA";

  }

  /**
   * Interface with constants for entities of "Tasks".
   */
  public interface TASKS {

    public final static String BLOG = TABLE_PREFIX + "JOBS_BLOG";
    public final static String JOB_INFO = TABLE_PREFIX + "JOBS_INFO";
  }
  
  /**
   * Interface with constants for entities of "PBCDMO".
   */
  public interface PBCDMO {

    public final static String TMCT0_BLANQUEO_CONTROL_EFECTIVO = TABLE_PREFIX + "BLANQUEO_CONTROL_EFECTIVO";
    public final static String TMCT0_BLANQUEO_CONTROL_ANALISIS = TABLE_PREFIX + "BLANQUEO_CONTROL_ANALISIS";
    public final static String TMCT0_BLANQUEO_CONTROL_DOCUMENTACION = TABLE_PREFIX + "BLANQUEO_CONTROL_DOCUMENTACION";
    public final static String TMCT0_PAISES = "TMCT0PAISES";
    public final static String TMCT0_BLANQUEO_CLIENTE = TABLE_PREFIX + "BLANQUEO_CLIENTE";
    public final static String TMCT0_BLANQUEO_OBSERVACION = TABLE_PREFIX + "BLANQUEO_OBSERVACION";
    public final static String TMCT0_BLANQUEO_OBSERVACION_CLIENTE = TABLE_PREFIX + "BLANQUEO_OBSERVACION_CLIENTE";
    public final static String TMCT0CLI = "TMCT0CLI";
    public final static String TMCT0_BLANQUEO_EFECTIVO_ACUMULADO = TABLE_PREFIX + "BLANQUEO_EFECTIVO_ACUMULADO";
    public final static String TMCT0_DMO_OPERACIONES = TABLE_PREFIX + "DMO_OPERACIONES";

  }

  public interface MCLAGAN {

    public final static String TMCT0_MCLAGAN = TABLE_PREFIX + "MCLAGAN";
    public final static String TMCT0_MCLAGAN_CUENTAS = TABLE_PREFIX + "MCLAGAN_CUENTAS";

  }

  public interface SICAV {

    public final static String TMCT0OPR = "TMCT0OPR";
    public final static String TMCT0NUM = "TMCT0NUM";
    public final static String TMCT0CLE = "TMCT0CLE";
  }

}