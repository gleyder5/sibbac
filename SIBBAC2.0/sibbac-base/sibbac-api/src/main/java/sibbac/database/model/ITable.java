package sibbac.database.model;

import java.io.Serializable;

/**
 * Interface that contains the methods all database entities must implement.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public interface ITable<T> extends Serializable, Comparable<T> {

	/**
	 * The record identificator.
	 * 
	 * @return A long with the record identificator.
	 */
	public Long getId();

	/**
	 * The record version.
	 * 
	 * @return A {@link Long} with the current record version.
	 */
	public Long getVersion();

}
