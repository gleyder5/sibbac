/**
 * This is a testing text.
 * <p/>
 * This is another testing text.<br/>
 * <h1>And this is a Header #1 test</h1>
 * <h2>And this is a Header #2 test</h2>
 * 
 * @since 1.0
 */
package sibbac.database;

import static sibbac.database.DBConstants.BUNDLE_MESSAGES;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SIBBACUtils {

	protected static final Logger LOG = LoggerFactory.getLogger(SIBBACUtils.class);

	protected static ResourceBundle labels = null;

	public static void loadDefaultLanguageMessages() {
		loadLanguageMessages(null, null);
	}

	public static void loadEnglishLanguageMessages() {
		loadLanguageMessages("en", "EN");
	}

	public static void loadSpanishLanguageMessages() {
		loadLanguageMessages("es", "ES");
	}

	public static void loadFrenchLanguageMessages() {
		loadLanguageMessages("fr", "FR");
	}

	public static String get(final String key) {
		String value = null;
		try {
			value = labels.getString(key);
		} catch (MissingResourceException e) {
			LOG.warn("WARN(" + e.getClass().getName() + "): " + e.getMessage());
		}
		return value;
	}

	private static void loadLanguageMessages(final String country, final String language) {
		LOG.info("Trying to load language-specific messages for: [" + country + "/" + language + "]");
		Locale locale = (country != null && language != null) ? new Locale(country, language) : null;
		LOG.debug("+ Loaded the Locale...");
		try {
			labels = (locale != null) ? ResourceBundle.getBundle(BUNDLE_MESSAGES, locale)
					: ResourceBundle.getBundle(BUNDLE_MESSAGES);
			LOG.debug("+ Loaded the ResourceBundle[" + BUNDLE_MESSAGES + ".properties]...");
			LOG.debug(labels.getString("label1"));
		} catch (MissingResourceException e) {
			LOG.warn("WARN(" + e.getClass().getName() + "): " + e.getMessage());
		}
	}
}
