package sibbac.tasks;

/**
 * A "Plan" is the basic element.
 * <p/>
 * This interface shows what the logic can do with a planned-element.
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface Plan {

	/**
	 * Run the business logic associated with this task.
	 */
	public void execute();

}
