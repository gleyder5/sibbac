package sibbac.tasks;

/**
 * Contiene los grupos de tareas existentes en Sibbac.
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 */
public interface Task {

  /**
   * Run the business logic associated with this task.
   */
  public void execute();

  /**
   * Tareas del grupo conciliación.
   */
  public interface GROUP_CONCILIACION {
    public static final String NAME = "Conciliacion";
    // Jobs
    public static final String JOB_CALCULO_SALDO_INICIAL = "CalculoSaldoInicial";
    public static final String JOB_COPY_MOVIMIENTO_MANUAL_A_MOVIMIENTO_CV = "CopyMovimientoManualAMovimientoCV";
    public static final String JOB_NORMA43_FILE_LOAD = "Norma43FileLoad";
    public static final String JOB_FICHERO_S3_INPUT = "FicheroS3Input";
    public static final String JOB_GESTION_DIFERENCIA_CONCILIACION = "GestionDiferenciaConciliacion";
    public static final String JOB_SALDOS_FIN_DIA = "gestionSaldosFinDiaECC";
    public static final String JOB_SALDOS_FIN_DIA_TABLA = "gestionSaldosFinDiaECCT";
  } // GROUP_CONCILIACION

  /**
   * Tareas del grupo contabilidad.
   */
  public interface GROUP_CONTABILIDAD {
    public static final String NAME = "Contabilidad";
    // Jobs
    public static final String JOB_APUNTES = "Apuntes";
  } // GROUP_CONTABILIDAD
  

  /**
   * Tareas del grupo contabilidad.
   */
  public interface GROUP_ACCOUNTING {
    public static final String NAME = "Accounting";
    // Jobs
    public static final String JOB_DESGLOSE = "Accounting";
    public static final String JOB_UPDATE_DATOS_COMPENSACION = "ActualizacionDatosCompensacion";
    public static final String JOB_PERIODOS_CONTABILIDAD = "PeriodosContabilidad";
    public static final String JOB_MARCAR_COBRADO_OPERACIONES_NO_COBRABLES = "MarcarCobradoOperacionesNoCobrables";
  } // GROUP_ACCOUNTING

  /**
   * Tareas del grupo interfaces emitidas.
   */
  public interface GROUP_INTEFACES_EMITIDAS {
    public static final String NAME = "interfacesemitidas";
    // Jobs
    public static final String JOB_INTERFACES_EMITIDAS = "comunicacionSII";
    public static final String JOB_SACCR_IRIS = "saccrIris";
  } // GROUP_INTEFACES_EMITIDAS

  /**
   * Tareas del grupo Saccr-Iris.
   */
  public interface GROUP_SACCR_IRIS {
    public static final String NAME = "interfacesIris";
    // Jobs
    public static final String JOB_SACCR_IRIS = "saccrIris";
  } // GROUP_SACCR_IRIS

  /**
   * Tareas del grupo Envio Ordenes Partenon.
   */
  public interface GROUP_ENVIO_ORDENES_PARTENON {
    public static final String NAME = "ordenesPartenon";
    // Jobs
    public static final String JOB_ENVIO_ORDENES_PARTENON = "envioOrdenesPartenon";
  } // GROUP_ENVIO_ORDENES_PARTENON

  /**
   * Tareas del grupo facturación.
   */
  public interface GROUP_FACTURACION {
    public static final String NAME = "Facturacion";
    // Jobs
    public static final String JOB_ENVIAR_DUPLICADO_FACTURA = "EnvioFactura";
    public static final String JOB_ENVIAR_FACTURA = "EnvioFactura";
    public static final String JOB_ENVIAR_FACTURA_RECTIFICATICA = "EnvioFacturaRectificativa";
    public static final String JOB_ENVIAR_MAIL_COBROS = "EnviarMailCobros";
    public static final String JOB_ENVIAR_RECORDATORIO_FACTURAS_PENDIENTES = "EnviarFacturasPendientesRecordatorio";
    public static final String JOB_FACTURA = "Factura";
    public static final String JOB_FACTURA_RECTIFICATICA = "FacturaRectificativa";
    public static final String JOB_FACTURA_SUGERIDA = "FacturaSugerida";
    public static final String JOB_RECORDATORIO_FACTURAS_PENDIENTES = "RecordatorioFacturaPendiente";
  } // GROUP_FACTURACION

  /**
   * Tareas del grupo fallidos.
   */
  public interface GROUP_FALLIDOS {
    public static final String NAME = "Fallidos";
    // Jobs
    public static final String JOB_AJUSTES = "AjustesJob";
    public static final String JOB_FALLIDOS = "CheckFallidos";
    public static final String JOB_PTIFILES = "PTIFilesJob";

    public static final String JOB_H47 = "GeneracionH47";
    public static final String JOB_LECTURA_H47 = "LecturaH47";

    public static final String JOB_MTF = "CreacionAutomaticaOperacionesH47Mtfs";
    public static final String JOB_FALLIDOS_NACIONAL = "CreacionAutomaticaOperacionesH47FallisNac";
    public static final String JOB_TR = "CreacionAutomaticaOperacionesTR";

  } // GROUP_FALLIDOS

  /**
   * Tareas del grupo financiero.
   */
  public interface GROUP_FINANCIERO {
    public static final String NAME = "Financiero";
    // Jobs
    public static final String JOB_INTERMEDIARIO_FINANCIERO = "Job_Intermediario_Financiero";
  } // GROUP_FINANCIERO

  /**
   * Tareas del grupo fixml.
   */
  public interface GROUP_FIXML {
    public static final String NAME = "FIXML";
    // Jobs
    public static final String JOB_FIXML_INPUT = "FIXMLInput";
  } // GROUP_FIXML

  /**
   * Tareas del grupo minijobs.
   */
  public interface GROUP_MINIJOBS {
    public static final String NAME = "Minijobs";
    // jobs
    public static final String JOB_LIQ_STATE_CHANGE = "LiqStateChange";
    public static final String JOB_FILES_CUSTOMIZE = "Files_Customize";

    public static final String JOB_INTERMEDIARIO_FINANCIERO = "IntermediarioFinanciero";
  } // GROUP_MINIJOBS

  /**
   * Tareas del grupo clearing.
   */
  public interface GROUP_CLEARING {
    public static final String NAME = "Clearing";
    // Jobs
    public static final String JOB_ENVIO_FICHERO_MEGARA = "EnvioFicheroMegara";
    public static final String JOB_FICHERO_RESPUESTA_MEGARA = "FicheroRespuestaMegara";
    public static final String JOB_INTERESES_FINANCIACION = "InteresesFinanciacion";
  } // GROUP_CLEARING

  /**
   * Tareas del grupo wrappers.
   */
  public interface GROUP_WRAPPERS {
    public static final String NAME = "Wrappers";
    // Jobs
    public static final String JOB_OPERACION_ESPECIAL_PROCESAR_FICHERO_2040 = "OperacionesEspeciales2040ProcesarFichero-0-1";
    public static final String JOB_OPERACIONES_ESPECIALES_2040_GENERAR_DESGLOSES = "OperacionesEspeciales2040GenerarDesgloses-2";
    public static final String JOB_OPERACIONES_ESPECIALES_2040_GENERAR_FICHERO_1015 = "OperacionesEspeciales2040GenerarFichero1015-3";
    public static final String JOB_SINCRONIZACION_ALIAS = "SincronizacionAlias";
    public static final String JOB_ENVIO_EXCEL_VIA_MAIL_OPERACIONES = "EnvioExcelOperacionesPorMail";

  } // GROUP_WRAPPERS

  /**
   * Tareas del grupo routing.
   */
  public interface GROUP_ROUTING {
    public static final String NAME = "Routing Minoristas";
    // Jobs
    public static final String JOB_TITULARES = "Titulares";
    public static final String JOB_TITULARES_OPEN = "TitularesOpen";
    public static final String JOB_OPERACIONES_ESPECIALES_CARGAR_TITULARES = "OperacionesEspeciales2040CargarTitulares-4";
    public static final String JOB_IMPORT_TITULARES = "ImportTitulares";
    public static final String JOB_BANCA_PRIVADA = "TitularesSPB";
    public static final String JOB_ESCRITURA_SPB = "EscrituraSPB";

  } // GROUP_ROUTING

  /**
   * Tareas del grupo comunicaciones.
   */
  public interface GROUP_COMUNICACIONES {
    public static final String NAME = "Comunicaciones";
    // Jobs
    public static final String JOB_MQ_WRITER = "MQ.partenon.out";
    public static final String JOB_MQ_READER = "MQ.partenon.in";
    public static final String JOB_ENTRADA_SAN_MQ = "MQ.partenon.in.analizar";
  } // GROUP_ROUTING

  /**
   * Tareas del grupo daemons.
   */
  public interface GROUP_DAEMONS {
    public static final String NAME = "Daemons";
    // Jobs
    public static final String JOB_REPARTO_CALCULOS_ALC_TO_DCT = "RepartoCalculosAlcToDct";
    public static final String JOB_CASE = "CaseEjecuciones";
    public static final String JOB_ENVIO_FICHERO_CORRETAJE = "EnvioFicheroCorretaje";
    public static final String JOB_CONFIRMACION_MINORISTA = "ConfirmacionMinorista";
    public static final String JOB_BATCH = "Batch";
    public static final String JOB_ORK = "Ork";
    public static final String JOB_REPARTO_FICHEROS_ESTATICOS = "EstaticosMegaraRepartoFicheros-1";
    public static final String JOB_ENVIO_FICHEROS_AS400 = "EstaticosMegaraEnvioFicherosAs400-2";
    public static final String JOB_ESTATICOS_MEGARA_INPUT = "EstaticosMegaraInput-2";
    public static final String JOB_ESTATICOS_ENVIO_FICHEROS_FDA_FIDESSA = "EstaticosMegaraEnvioFdaFidessa-3";
    public static final String JOB_ENVIO_FICHEROS_CUCO = "EnvioFicheroCUCO";
    public static final String JOB_ENVIO_FICHEROS_IRIS = "EnvioFicheroIRIS";

  } // GROUP_ROUTING

  /**
   * Tareas del grupo mantenimiento datos del cliente.
   */
  public interface GROUP_MANTENIMIENTO {
    public static final String NAME = "Mantenimiento";
    // Jobs
    public static final String JOB_CARGA_MIFID = "CargaMIFID";
    public static final String JOB_CARGA_MIFID_MULTI = "CargaMIFIDMulti";

  } // GROUP_MANTENIMIENTO

  /**
   * Tareas del grupo generar informes.
   */
  public interface GROUP_GENERAR_INFORMES {
    public static final String NAME = "Generar Informes";
    // Jobs
    public static final String JOB_BATCH_GENERARA_INFORMES = "BatchGenerarInformes";

  } // GROUP_GENERAR_INFORMES

  /**
   * Tareas del grupo comunicaciones pti.
   */
  public interface GROUP_COMUNICACIONES_ECC {
    public static final String NAME = "ComunicacionesEcc";
    public static final String JOB_ENVIO_FICHERO_MQ_PTI = "EnvioFicheroMqPti";
    public static final String JOB_ESCALADO_ASIGNACION = "EscaladoAsignacion";
    public static final String JOB_ESCALADO_ASIGNACION_SD = "EscaladoAsignacionSD";
    public static final String JOB_ESCALADO_TITULARIDAD = "EscaladoTitularidad";
    public static final String JOB_ESCALADO_TITULARIDAD_SD = "EscaladoTitularidadSD";
    public static final String JOB_ESCALADO_CONTABILIDAD = "EscaladoContabilidad";
    public static final String JOB_ESCALADO_CONTABILIDAD_SD = "EscaladoContabilidadSD";

    public static final String JOB_LECTURA_PTI = "LecturaPTI";
    public static final String JOB_LECTURA_PTI_SD = "LecturaPTISd";
    public static final String JOB_PROCESAMIENTO_MENSAJES_PTI = "ProcesamientoMensajesPTI";
    public static final String JOB_PROCESAMIENTO_MENSAJES_PTI_SD = "ProcesamientoMensajesPTISD";
    public static final String JOB_CREACION_MO_ASIGNACION = "CreacionMoAsignacion";
    public static final String JOB_ASIGNACION_ORDEN_SD = "AsignacionOrdenSd";
    public static final String JOB_IMPUTACION_S3_PTI = "ImputacionS3Pti";
    public static final String JOB_IMPUTACION_S3_EUROCCP = "ImputacionS3Euroccp";
    public static final String JOB_ENVIO_CE = "EnvioCe";
    public static final String JOB_CUADRE_ECC = "CuadreEcc";
    public static final String JOB_EURO_CCP = "EuroCcp";
    public static final String JOB_LOAD_XML_FIDESSA_MQ = "LoadXmlFidessaMq";
    public static final String JOB_DDBB_TO_PTI = "DdbbToPti";
    public static final String JOB_OPEN_PTI_SESSION = "OpenPtiSession";
    public static final String JOB_DESGLOSE_AUTOMATICO = "DesgloseAutomatico";
    public static final String JOB_DESGLOSE_AUTOMATICO_SD = "DesgloseAutomaticoSd";
    public static final String JOB_CREACION_TABLAS_TITULARES = "CreacionTablasTitulares";
    public static final String JOB_CREACION_TABLAS_TITULARES_SD = "CreacionTablasTitularesSd";
    public static final String JOB_CALCULO_CANONES = "CalculoCanones";
    public static final String JOB_CALCULO_CANONES_SD = "CalculoCanonesSd";

  }

  /**
   * Tareas del grupo DMO
   */
  public interface GROUP_DMO {
    public static final String NAME = "Dmo";
    // Jobs
    public static final String JOB_DATA_COLLECTION = "DmoCollection";
  }
  
  /**
   * Tareas PBC: Proceso de Recalculo de CCE
   */
  public interface GROUP_PBC {
    public static final String NAME = "Pbc";
    public static final String JOB_RECALCULO_CCE = "RecalculoCce";
    public static final String JOB_EFECTIVO_MENSUAL = "EfectivoMensual";
    public static final String JOB_EFECTIVO_ANUAL = "EfectivoAnual";
  }
  
  /**
   * Tareas del grupo mclagan.
   */
  public interface GROUP_MCLAGAN {
    public static final String NAME = "McLagan";
    //    // Jobs
    public static final String JOB_ENVIO_INFORME_MCLAGAN = "EnvioInformeMcLagan";

  }

  /**
   * Tareas del grupo DWH
   * @author ipalacio
   *
   */
  public interface GROUP_DWH {
    public static final String NAME = "DWH";
    // Jobs
    public static final String JOB_FICHERO_EJECUCIONES = "GenerarFicheroEjecuciones";
    public static final String JOB_FICHERO_DATOS_ESTATICOS = "GenerarFicherosDatosEstaticos";
    public static final String JOB_FICHERO_TRADER_ACCOUNT = "GenerarFicherosTraderAccounts";
    public static final String JOB_ENVIO_FICHEROS_DWH_CLIENTES = "GenerarFicherosClientes";
    public static final String JOB_ENVIO_FICHEROS_DWH_TRACACC = "GenerarFicherosTracAcc";

    // Fichero de Valores
    public static final String JOB_ENVIO_FICHEROS_DWH_VALORES = "GenerarFicherosValores";
    public static final String JOB_CARGA_VALORES = "ProcesoCargaValores";
    public static final String JOB_VALIDACION_VALORES = "ProcesoValidacionValores";
    public static final String JOB_FICHERO_NACIONAL = "GenerarFicheroNacional";
    public static final String JOB_FICHERO_PLANIFICACION_DWH = "Planificación ficheros DWH";
  }

  /**
   * Tareas del grupo Fonetic.
   */
  public interface GROUP_FONECTIC{
    public static final String NAME = "Fonetic";

    public static final String JOB_GENERAR_REPORTE_SPEECHMINER = "GenerarReporteSpeechMiner";

  }
} // Task
