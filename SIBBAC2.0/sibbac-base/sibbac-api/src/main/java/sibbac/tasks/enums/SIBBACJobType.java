package sibbac.tasks.enums;

public enum SIBBACJobType {

  CALENDAR_INTERVAL_JOB,
  CRON_JOB;

}
