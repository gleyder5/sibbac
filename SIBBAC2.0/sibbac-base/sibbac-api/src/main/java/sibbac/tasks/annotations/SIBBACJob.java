package sibbac.tasks.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.quartz.DateBuilder;
import org.springframework.stereotype.Component;

import sibbac.tasks.enums.SIBBACJobType;

/**
 * This annotation determines that the class being annotated is a SIBBAC Job.
 * <br/>
 *
 * A SIBBAC Job is responsible of performing the relevant business automated
 * operations.<br/>
 * Every discovered SIBBAC Job will be injected into the Job Controller.
 * 
 * http://stackoverflow.com/questions/15720579/add-custom-beans-to-spring-
 * context http://www.mkyong.com/java/java-custom-annotations-example/
 * http://techo-ecco.com/blog/spring-custom-annotations/
 * http://stackoverflow.com/questions/7846103/customised-annotation-in-spring
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
@Target({ ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Component
// Avoided due to inconsistencies: @DisallowConcurrentExecution
public @interface SIBBACJob {

	/**
	 * The name of the SIBBAC Job.
	 */
	public String name();
	
	public SIBBACJobType jobType() default SIBBACJobType.CALENDAR_INTERVAL_JOB;

	/**
	 * The logical group to which this SIBBAC Job belongs to. Default: {@value}
	 */
	public String group() default "SIBBACJob";

	/**
	 * The interval when to launch the task. Default: {@value}
	 */
	public int interval() default 5;

	/**
	 * The units for timing the task. Default: {@value}
	 *
	 * Allowed values:
	 * <ul>
	 * <li>DateBuilder.IntervalUnit.DAY</li>
	 * <li>DateBuilder.IntervalUnit.HOUR</li>
	 * <li>DateBuilder.IntervalUnit.MILLISECOND</li>
	 * <li>DateBuilder.IntervalUnit.MINUTE</li>
	 * <li>DateBuilder.IntervalUnit.MONTH</li>
	 * <li>DateBuilder.IntervalUnit.SECOND</li>
	 * <li>DateBuilder.IntervalUnit.WEEK</li>
	 * <li>DateBuilder.IntervalUnit.YEAR</li> </ui>
	 */
	public DateBuilder.IntervalUnit intervalUnit() default DateBuilder.IntervalUnit.SECOND;

	public String startDate() default "";

	public String startTime() default "";
	
	public String cronExpression() default "0 0/5 * ? * MON-FRI";

	/**
	 * The seconds of delay for the task to start.<br/>
	 * Default: {@value}
	 */
	public int delay() default 1;

}
