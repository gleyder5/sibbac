package sibbac.common;

public enum FormatStyle {
  JSON_OBJECTS, JSON_ARRAY, EXCEL, CSV
}
