package sibbac.common;

public class SIBBACTaskException extends  SIBBACBusinessException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6424096991498465516L;
	private  SIBBACTaskExecutionErrorType errorType;

    public SIBBACTaskExecutionErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(SIBBACTaskExecutionErrorType errorType) {
        this.errorType = errorType;
    }

    public SIBBACTaskException(SIBBACTaskExecutionErrorType errorType) {
        this.errorType = errorType;
    }

    public SIBBACTaskException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, SIBBACTaskExecutionErrorType errorType) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorType = errorType;
    }

    public SIBBACTaskException(String message, Throwable cause, SIBBACTaskExecutionErrorType errorType) {
        super(message, cause);
        this.errorType = errorType;
    }

    public SIBBACTaskException(String message, SIBBACTaskExecutionErrorType errorType) {
        super(message);
        this.errorType = errorType;
    }

    public SIBBACTaskException(Throwable cause, SIBBACTaskExecutionErrorType errorType) {
        super(cause);
        this.errorType = errorType;
    }
}
