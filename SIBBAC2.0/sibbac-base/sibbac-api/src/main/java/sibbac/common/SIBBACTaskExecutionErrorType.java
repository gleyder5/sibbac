package sibbac.common;

public enum SIBBACTaskExecutionErrorType {

    HOST_NOT_ALLOWED(100),
    NOT_IN_INTERVAL_TIME(200),
    EXECUTION_ERROR(-1);

    private int type;

    SIBBACTaskExecutionErrorType(final int type) {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }
}
