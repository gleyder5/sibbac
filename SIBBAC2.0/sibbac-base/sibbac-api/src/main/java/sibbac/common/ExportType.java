package sibbac.common;

public enum ExportType {

	// Para generar las facturas en PDF
	GENERIC("GenericPDF"),

	// Para generar las facturas en PDF
	PDF("Factura"),

	// Para generar las facturas rectificativas en PDF
	PDF_RECTIFICATIVA("FacturaRectificativa"),

	// Para generar los duplicados de las facturas en PDF
	PDF_DUPLICADO("DuplicadoDeFactura"),

	// Para exportar los ALC's de las facturas a EXCEL
	EXCEL("Ordenes"),

	EXCEL_INFORME_OPERACIONES("Informe Operaciones");

	private String fileName;

	ExportType() {
		this("GenericPDF");
	}

	ExportType(final String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return this.fileName;
	}

}
