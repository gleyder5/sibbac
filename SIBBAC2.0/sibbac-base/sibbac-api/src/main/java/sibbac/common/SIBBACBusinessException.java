package sibbac.common;

public class SIBBACBusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5600368104276607673L;

	public SIBBACBusinessException() {
		super();
	}

	public SIBBACBusinessException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SIBBACBusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public SIBBACBusinessException(String message) {
		super(message);
	}

	public SIBBACBusinessException(Throwable cause) {
		super(cause);
	}
}
