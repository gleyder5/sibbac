package sibbac.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.StreamResult;

public class HttpStreamResultHelper implements StreamResult {
  
  protected static final Logger LOG = LoggerFactory.getLogger(HttpStreamResultHelper.class);
  
  private static final String JSON_UTF8 = "application/json;charset=UTF-8";

  private static final String XSLX_MIME_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  
  private static final String TEXT_CSV = "text/csv";

  private final HttpServletResponse servletResponse;
  
  public HttpStreamResultHelper(final HttpServletResponse servletResponse) {
    this.servletResponse = servletResponse;
  }

  @Override
  public OutputStream getOutputStream(FormatStyle formatStyle) {
    String contentType;
    
    switch(formatStyle)  {
      case JSON_ARRAY: case JSON_OBJECTS:
        contentType = JSON_UTF8;
        break;
      case EXCEL:
        contentType = XSLX_MIME_TYPE;
        break;
      case CSV:
        contentType = TEXT_CSV;
        break;
      default:
        contentType = null;
    }
    if(contentType != null) {
      servletResponse.setContentType(contentType);
    }
    try {
      return servletResponse.getOutputStream();
    }
    catch (IOException e) {
      LOG.error("[getOutputStream] Error al abrir un flujo de respuesta", e);
      enviarErrorAlAbrirFlujo();
      return new MockOutputStream();
    }
  }

  @Override
  public void sendErrorMessage(String message) {
    try {
      servletResponse.setStatus(500);
      servletResponse.setContentType("text/plain");
      servletResponse.setContentLength(message.length());
      servletResponse.getWriter().println(message);
    }
    catch(IOException ioex) {
      LOG.error("Error al enviar HTTP STATUS 500 con mensaje {}. Se traza error original", message, ioex);
    }
  }

  @Override
  public void sendErrorMessage(SIBBACBusinessException ex) {
    final String message;
    
    message = ex.getMessage();
    try {
      servletResponse.setStatus(500);
      servletResponse.setContentType("text/plain");
      servletResponse.setContentLength(message.length());
      servletResponse.getWriter().println(message);
    }
    catch(IOException ioex) {
      LOG.error("Error al enviar HTTP STATUS 500 con mensaje {}. Se traza error original", ex.getMessage(), ioex);
    }
  }
  
  private void enviarErrorAlAbrirFlujo() {
    try {
      servletResponse.sendError(500, "Error al intentar responder");
    }
    catch(IOException ioex) {
      LOG.error("Error al enviar HTTP STATUS 500", ioex);
    }
  }
  
  private class MockOutputStream extends ByteArrayOutputStream {
    
    @Override
    public void close() {
      LOG.error("Se han enviado {} bytes por flujo erroneo", toByteArray().length);
    }
  }

}
