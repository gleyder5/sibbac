package sibbac.common;

import java.io.OutputStream;

public interface StreamResult {
  
  OutputStream getOutputStream(FormatStyle formatStyle);
  
  void sendErrorMessage(String message);
  
  void sendErrorMessage(SIBBACBusinessException ex);

}
