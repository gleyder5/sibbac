package sibbac.common;

public class XLSInteresesS3 {

	// Nuestra referencia
	private String nuestraReferencia;

	// Compra
	private String compra = "COMPRA";

	// ISIN
	private String isin;

	// Nombre del valor ISIN
	private String nombreValor;

	// Mercado
	private String mercado;

	// Total títulos
	private double totalTitulos;

	// Precio medio neto de la orden
	private double precioMedioOrden;

	// Importe neto liquiddo
	private double importeNetoLiquidado;

	// Titular
	private String titular;

	// Fecha operacion
	private String fechaOperacion;

	// Fecha contratacion
	private String fechaContratacion;

	// Fecha valor liquidacion
	private String fechaFiquidacion;

	// Tipo de interes aplicado
	private double tipoInteres;

	// Interes aplicado
	private double interesAplicado;

	private int difereciaDias;

	public XLSInteresesS3() {

	}

	public String getNuestraReferencia() {
		return nuestraReferencia;
	}

	public void setNuestraReferencia(String nuestraReferencia) {
		this.nuestraReferencia = nuestraReferencia;
	}

	public String getCompra() {
		return compra;
	}

	public void setCompra(String compra) {
		this.compra = compra;
	}

	public String getIsin() {
		return isin;
	}

	public void setIsin(String isin) {
		this.isin = isin;
	}

	public String getNombreValor() {
		return nombreValor;
	}

	public void setNombreValor(String nombreValor) {
		this.nombreValor = nombreValor;
	}

	public String getMercado() {
		return mercado;
	}

	public void setMercado(String mercado) {
		this.mercado = mercado;
	}

	public double getTotalTitulos() {
		return totalTitulos;
	}

	public void setTotalTitulos(double totalTitulos) {
		this.totalTitulos = totalTitulos;
	}

	public double getPrecioMedioOrden() {
		return precioMedioOrden;
	}

	public void setPrecioMedioOrden(double precioMedioOrden) {
		this.precioMedioOrden = precioMedioOrden;
	}

	public double getImporteNetoLiquidado() {
		return importeNetoLiquidado;
	}

	public void setImporteNetoLiquidado(double importeNetoLiquidado) {
		this.importeNetoLiquidado = importeNetoLiquidado;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getfechaContratacion() {
		return fechaContratacion;
	}

	public void setfechaContratacion(String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public String getFechaFiquidacion() {
		return fechaFiquidacion;
	}

	public void setFechaFiquidacion(String fechaFiquidacion) {
		this.fechaFiquidacion = fechaFiquidacion;
	}

	public double getTipoInteres() {
		return tipoInteres;
	}

	public void setTipoInteres(double tipoInteres) {
		this.tipoInteres = tipoInteres;
	}

	public double getInteresAplicado() {
		return interesAplicado;
	}

	public void setInteresAplicado(double interesAplicado) {
		this.interesAplicado = interesAplicado;
	}

	public int getDifereciaDias() {
		return difereciaDias;
	}

	public void setDifereciaDias(int difereciaDias) {
		this.difereciaDias = difereciaDias;
	}

}
