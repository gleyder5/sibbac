package sibbac.common;

import java.math.BigDecimal;
import java.util.Date;

public class PDFMail implements PDFFactura {

	private final String nombreAlias;
	private final Date fhFechaFac;
	private final String direccion;
	private final long nbDocNumero;
	private final String registroMercantil;
	private final BigDecimal importe;
	private final BigDecimal importeBase;
	private final BigDecimal impuestos;
	private final BigDecimal total;
	private final String cuentaBancaria;
	private final String iban;
	private final String bic;
	private final String empresa;
	private final Boolean duplicado;
	private boolean rectificativa;
	private static final String LOGOTIPO = "LogotipoGlobalBanking.png";
	private static final String DOWJONES = "dowJones.png";
	private static final String FTSE4GOOD = "Ftse4Good";

	public PDFMail(String nombreAlias, Date fhFechaFac, String direccion, long nbDocNumero, String registroMercantil,
			BigDecimal importe, BigDecimal importeBase, BigDecimal impuestos, BigDecimal total, String cuentaBancaria,
			String iban, String bic, String empresa, Boolean duplicado) {
		this(nombreAlias, fhFechaFac, direccion, nbDocNumero, registroMercantil, importe, importeBase, impuestos, total,
				cuentaBancaria, iban, bic, empresa, duplicado, false);
	}

	public PDFMail(String nombreAlias, Date fhFechaFac, String direccion, long nbDocNumero, String registroMercantil,
			BigDecimal importe, BigDecimal importeBase, BigDecimal impuestos, BigDecimal total, String cuentaBancaria,
			String iban, String bic, String empresa, Boolean duplicado, boolean rectificativa) {
		super();
		this.nombreAlias = nombreAlias;
		this.fhFechaFac = fhFechaFac;
		this.direccion = direccion;
		this.nbDocNumero = nbDocNumero;
		this.registroMercantil = registroMercantil;
		this.importe = importe;
		this.importeBase = importeBase;
		this.impuestos = impuestos;
		this.total = total;
		this.cuentaBancaria = cuentaBancaria;
		this.iban = iban;
		this.bic = bic;
		this.empresa = empresa;
		this.duplicado = duplicado;
		this.rectificativa = rectificativa;
	}

	public String getNombreAlias() {
		return nombreAlias;
	}

	public Date getFhFechaFac() {
		return fhFechaFac;
	}

	public String getDireccion() {
		return direccion;
	}

	public long getNbDocNumero() {
		return nbDocNumero;
	}

	public String getRegistroMercantil() {
		return registroMercantil;
	}

	public BigDecimal getImporte() {
		return importe;
	}

	public BigDecimal getImporteBase() {
		return importeBase;
	}

	public BigDecimal getImpuestos() {
		return impuestos;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public String getCuentaBancaria() {
		return cuentaBancaria;
	}

	public String getIban() {
		return iban;
	}

	public String getBic() {
		return bic;
	}

	public String getEmpresa() {
		return empresa;
	}

	public Boolean getDuplicado() {
		return duplicado;
	}

	@Override
	public boolean isRectificativa() {
		return this.rectificativa;
	}

	public void setRectificativa(boolean rectificativa) {
		this.rectificativa = rectificativa;
	}

	public String getLogotipo() {
		return LOGOTIPO;
	}

	public String getDowJones() {
		return DOWJONES;
	}

	public String getFtse4Good() {
		return FTSE4GOOD;
	}

}
