package sibbac.common;

import java.util.Queue;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import static java.util.concurrent.Executors.newFixedThreadPool;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

public abstract class LimitedThreadExecutor<T, C extends Callable<C>> {
  
  private static final Logger LOG = getLogger(LimitedThreadExecutor.class);
  
  private final Queue<T> itemQueue;
  
  private final BlockingQueue<C> callables;
  
  private final int size;
  
  private volatile SIBBACBusinessException bussinessException;
  
  private boolean finished = false;
  
  protected LimitedThreadExecutor(Queue<T> itemQueue, Collection<C> callables) {
    this.itemQueue = itemQueue;
    size = callables.size();
    this.callables = new ArrayBlockingQueue<>(size);
    this.callables.addAll(callables);
  }
  
  public void execute() throws SIBBACBusinessException {
    final String message;
    final ExecutorService exec;
    Future<C> f;
    T item;
    C callable;
    
    exec = newFixedThreadPool(size * 2, new NamedThreadFactory());
    try {
      while(bussinessException == null && notIsFinal(item = itemQueue.poll())) {
        callable = callables.take();
        assignValue(item, callable);
        f = exec.submit(callable);
        exec.execute(new Returner(f));
      }
      exec.shutdown();
    }
    catch(InterruptedException iex) {
      message = "Se ha interrumpido los mensajes de cola en el ejecutor";
      LOG.error("[execute] {}", message, iex);
      throw new SIBBACBusinessException(message, iex);
    }
    if(bussinessException != null) {
      throw bussinessException;
    }
  }
  
  protected abstract boolean notIsFinal(T item);
  
  protected abstract void assignValue(T item, C callable);
  
  protected void useValue(C callable) {
    LOG.trace("[useValue] El valor no se utiliza");
  }
  
  private class Returner implements Runnable {
    
    private final Future<C> f;
    
    private Returner(Future<C> f) {
      this.f = f;
    }

    @Override
    public void run() {
      final C callable;
      String message;
      
      try {
        callable = f.get();
        useValue(callable);
        if(!finished) {
          callables.put(callable);
        }
      } 
      catch (InterruptedException e) {
        message = "Se ha interrumpido la recuperación del valor";
        LOG.error("[Returner.run] {}", message, e);
        bussinessException = new SIBBACBusinessException(message, e);
      }
      catch(ExecutionException e) {
        message = "Error no controlado en callable";
        LOG.error("[Returner.run] {}", message, e.getCause());
        bussinessException = new SIBBACBusinessException(message, e.getCause());
      }
    }
    
  }
  
  private class NamedThreadFactory implements ThreadFactory {
    
    private final String nameFormat;
    
    private final ThreadGroup group;
    
    private int count = 0;
    
    private NamedThreadFactory() {
      final Thread current;
      
      current = Thread.currentThread();
      group = current.getThreadGroup();
      nameFormat = new StringBuilder(current.getName()).append("_%d").toString();
    }

    @Override
    public Thread newThread(Runnable runnable) {
      final String name;
      
      name = String.format(nameFormat, count++);
      return new Thread(group, runnable, name);
    }
    
  }

}
