package sibbac.common;

import java.math.BigDecimal;

public class XLSFactura {

  // Fecha Operación
  private String fechaOperacion;

  // Nuestra referencia
  private String nuestraReferencia;

  // Compra o Venta
  private String compra;

  // ISIN
  private String isin;

  // Nombre del valor
  private String nombreValor;

  // Mercado
  private String mercado;

  // Titular o subcuenta asignado a cada ejecución o desglose
  private String titular;

  // Total títulos ejecutados por orden
  private BigDecimal titulosEjecutadosPorOrden;

  // Precio Medio de la orden
  private BigDecimal precioMedioOrden;

  // Corretaje total de la orden
  private BigDecimal corretajeTotal;

  // Corretaje de cada ejecución
  // private double corretajePorEjecucion;

  private String referenciaAsignacion;

  @SuppressWarnings("unused")
  private XLSFactura() {
//    this(null, null, null, null, null, null, null, null, 0, 0, 0/* , 0 */);
    this(null, null, null, null, null, null, null, null, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO/* , 0 */);
  }

  public XLSFactura(String fechaOperacion,
                    String nuestraReferencia,
                    String compra,
                    String isin,
                    String nombreValor,
                    String mercado,
                    String titular,
                    String referenciaAsignacion,
                    BigDecimal titulosEjecutadosPorOrden,
                    BigDecimal precioMedioOrden,
                    BigDecimal corretajeTotal/* , double corretajePorEjecucion */) {
    super();
    this.fechaOperacion = fechaOperacion;
    this.nuestraReferencia = nuestraReferencia;
    this.compra = compra;
    this.isin = isin;
    this.nombreValor = nombreValor;
    this.mercado = mercado;
    this.titular = titular;
    this.titulosEjecutadosPorOrden = titulosEjecutadosPorOrden;
    this.precioMedioOrden = precioMedioOrden;
    this.corretajeTotal = corretajeTotal;
    // this.corretajePorEjecucion = corretajePorEjecucion;
    this.referenciaAsignacion = referenciaAsignacion;
  }

  public String getFechaOperacion() {
    return this.fechaOperacion;
  }

  public void setFechaOperacion(String fechaOperacion) {
    this.fechaOperacion = fechaOperacion;
  }

  public String getNuestraReferencia() {
    return this.nuestraReferencia;
  }

  public void setNuestraReferencia(String nuestraReferencia) {
    this.nuestraReferencia = nuestraReferencia;
  }

  public String getCompra() {
    return this.compra;
  }

  public void setCompra(String compra) {
    this.compra = compra;
  }

  public String getIsin() {
    return this.isin;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public String getNombreValor() {
    return this.nombreValor;
  }

  public void setNombreValor(String nombreValor) {
    this.nombreValor = nombreValor;
  }

  public String getMercado() {
    return this.mercado;
  }

  public void setMercado(String mercado) {
    this.mercado = mercado;
  }

  public String getTitular() {
    return this.titular;
  }

  public void setTitular(String titular) {
    this.titular = titular;
  }

  public BigDecimal getTitulosEjecutadosPorOrden() {
    return this.titulosEjecutadosPorOrden;
  }

  public void setTitulosEjecutadosPorOrden(BigDecimal titulosEjecutadosPorOrden) {
    this.titulosEjecutadosPorOrden = titulosEjecutadosPorOrden;
  }

  public BigDecimal getPrecioMedioOrden() {
    return this.precioMedioOrden;
  }

  public void setPrecioMedioOrden(BigDecimal precioMedioOrden) {
    this.precioMedioOrden = precioMedioOrden;
  }

  public BigDecimal getCorretajeTotal() {
    return this.corretajeTotal;
  }

  public void setCorretajeTotal(BigDecimal corretajeTotal) {
    this.corretajeTotal = corretajeTotal;
  }

  // public double getCorretajePorEjecucion() {
  // return this.corretajePorEjecucion;
  // }
  //
  // public void setCorretajePorEjecucion(double corretajePorEjecucion) {
  // this.corretajePorEjecucion = corretajePorEjecucion;
  // }

  public String getReferenciaAsignacion() {
    return referenciaAsignacion;
  }

  public void setReferenciaAsignacion(String referenciaAsignacion) {
    this.referenciaAsignacion = referenciaAsignacion;
  }

}
