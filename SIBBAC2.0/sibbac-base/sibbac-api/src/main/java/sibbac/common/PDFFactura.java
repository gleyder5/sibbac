/**
 * 
 */
package sibbac.common;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Interfaz que permite acceder a la informacion a ser utilizada a la hora de
 * generar facturas.
 * 
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 *
 */
public interface PDFFactura {

	public String getNombreAlias();

	public Date getFhFechaFac();

	public String getDireccion();

	public long getNbDocNumero();

	public String getRegistroMercantil();

	public BigDecimal getImporte();

	public BigDecimal getImporteBase();

	public BigDecimal getImpuestos();

	public BigDecimal getTotal();

	public String getCuentaBancaria();

	public String getIban();

	public String getBic();

	public String getEmpresa();

	public Boolean getDuplicado();

	public String getLogotipo();

	public String getDowJones();

	public String getFtse4Good();

	public boolean isRectificativa();

}
