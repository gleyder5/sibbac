package sibbac.pti.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sibbac.pti.PTIConstants;

public class SIBBACCommons implements PTIConstants {

  public static final boolean isValid(final String dato) {
    return (dato != null && !dato.isEmpty() && dato.trim().length() > 0);
  }

  public static final boolean isValid(final int l, final int e, final int d) {
    return (l == e && d == 0) || (l > e && l == (e + d));
  }

  public static final String convertDateToString(final Date dateNow, final String format) {
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    return sdf.format(dateNow);
  }

  /**
   * Convierte de fecha en texto a fecha en tipo {@link java.util.Date}.
   * 
   * @param strDateTime La fecha en texto.
   * @param format El formato.
   * @return Un {@link java.util.Date} con la fecha.
   */
  public static final Date convertStringToDate(final String strDateTime, final String format) {
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    Date date = null;
    try {
      date = sdf.parse(strDateTime);
    } catch (ParseException e) {
      date = null;
    }
    return date;
  }

  public static final String adjustString(final String data, final int size) {
    return adjustString(data, size, true, " ");
  }

  public static final String adjustString(final String data,
                                          final int size,
                                          final boolean fillAtTheEnd,
                                          final String fillInWith) {
    if (data == null || data.length() >= size) {
      return data;
    }

    StringBuffer sb = new StringBuffer();
    int now = data.length();

    if (!fillAtTheEnd) {
      for (int n = now; n < size; n++) {
        sb.append(fillInWith);
      }
    }
    sb.append(data);
    if (fillAtTheEnd) {
      for (int n = now; n < size; n++) {
        sb.append(fillInWith);
      }
    }
    return sb.toString();
  }

  

}
