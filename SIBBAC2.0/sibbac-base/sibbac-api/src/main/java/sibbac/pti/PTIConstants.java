package sibbac.pti;

/**
 * Interfaz con las constantes directamente relacionadas con PTI/BME.
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTIConstants {

	/**
	 * For testing purposes only!
	 */
	public static final String UNDEFINED = "UNDEFINED YET!";

	/**
	 * Codigo de "OK" general. Valor: {@value}
	 */
	public static final String NO_ERROR = "000";

	/**
	 * Identificador de Miembro PTI raiz. Valor: {@value}
	 */
	public static final String ID_BMCL = "BMCL";

	/**
	 * Formato de fecha de la cabecera de los mensajes ("AAAAMMDD"). Valor:
	 * {@value}
	 */
	public static final String DATE_FORMAT = "yyyyMMdd";

	/**
	 * Formato de hora de la cabecera de los mensajes ("HHMMSSMMM"). Valor:
	 * {@value}
	 */
	public static final String TIME_FORMAT = "HHmmssSSS";

	/**
	 * El tamanyo por defecto de cada registro del bloque de control. Valor: * *
	 * * {@value}
	 */
	public static final int SIZE_CONTROL = 2;

	public interface HEADER_CONSTANTS {

		public static final String VERSION = "  ";
		public static final String SUBAPLICACION = "  ";
		public static final String ERROR = "   ";
		public static final String ENTIDAD_ORIGEN = "SVBS";
		public static final String USUARIO_ORIGEN = "SVB";
		public static final String ENTIDAD_DESTINO = "BMCL";
		public static final String USUARIO_DESTINO = "BMC";
		public static final String MIEMBRO = "SVBS       ";
		public static final String USUARIO_MIEMBRO = "SVB";
		public static final String RESERVADO = "                                            ";
	}

	public interface MO_CONSTANTS {

		public interface COMMON_DATA {

			public static final String COMPRA = "1";
			public static final String VENTA = "2";
		}

		public interface TIPO_MOVIMIENTO {

			public static final String GIVE_UP = "15";
			public static final String TAKE_UP = "16";
		}

		public interface ESTADO {

			public static final String REJECT = " 5";
			public static final String ALLOCATION_PENDING = " 6";
			public static final String CLAIMED = " 9";
			public static final String CANCELLED = "12";
			public static final String PENDING = "13";
		}

		public interface INDICADOR_COTIZACION {

			public static final String NOMINAL = "N";
			public static final String UNIDADES_MONETARIAS = "U";
		}
	}

	public interface AN_CONSTANTS {

		public interface INDICADOR_ANOTACION {

			public static final String NUEVA = "0";
			public static final String UPDATE = "2";
		}

		public interface SENTIDO {

			public static final String COMPRA = "1";
			public static final String VENTA = "2";
		}

		public interface INDICADOR_POSICION {

			public static final String ABRE = "O";
			public static final String CIERRA = "C";
		}

		public interface INDICADOR_COTIZACION {

			public static final String NOMINAL = "N";
			public static final String UNIDADES_MONETARIAS = "U";
		}

		public interface SITUACION_OPERACION {

			public static final String NO_CANCELADA = "";
			public static final String CANCELADA = "F";
		}

		public interface ESTADO_IL {

			public static final String PENDIENTE_DE_LIQUIDAR = "";
			public static final String PREA = "H";
			public static final String LIQUIDADA = "S";
			public static final String PARCIALMENTE_LIQUIDADA = "P";
			public static final String CANCELADA = "B";
			public static final String CANCELADA_POR_FALLIDOS = "F";
		}
	}

	public interface VA_CONSTANTS {

		public interface ESTILO {

			public static final String AMERICANO = "A";
			public static final String EUROPEO = "E";
			public static final String BERMUDAS = "B";
			public static final String OTROS = "O";
		}

		public interface INDICADOR_CALL_PUT {

			public static final String CALL = "0";
			public static final String PUT = "1";
		}

		public interface INDICADOR_ADICIONAL {

			public static final String BARRERA_INFERIOR_INLINES = "100";
			public static final String FECHA_ULTIMO_DIA_NEGOCIACION = "101";
			public static final String BARRERA_SUPERIOR_INLINES = "104";
			public static final String BARRERA_INFERIOR_ACTIVACION = "105";
			public static final String BARRERA_SUPERIOR_ACTIVACION = "106";
			public static final String NUMERO_DECIMALES = "114";
			public static final String CODIGO_DCV = "120";
			public static final String BARRERA_BONUS = "122";
		}

	}

	public interface PV_CONSTANTS {

		public interface TIPO_PRECIO_CIERRE {

			public static final String PRECIO_CIERRE_SESION_ACTUAL = "1";
			public static final String PRECIO_CIERRE_SESION_ANTERIOR = "4";
		}

	}

}
