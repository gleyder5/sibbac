package sibbac.common;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ExportTypeTest {

	public static final Logger LOG = LoggerFactory.getLogger(ExportTypeTest.class);

	@Test
	public void test10ExportType() {
		ExportType e = ExportType.PDF_DUPLICADO;
		LOG.debug("ExportType: [{}=={}]", e.name(), e.getFileName());
	}

}
