package sibbac.database;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

class MockFilter extends AbstractQueryFilter<Object> {
    
    static final Date BEGIN;
    
    static final Date END;
    
    static {
        final Calendar calendar;
        
        calendar = Calendar.getInstance();
        calendar.set(2017, Calendar.JANUARY, 10);
        BEGIN = calendar.getTime();
        calendar.set(Calendar.DATE, 11);
        END = calendar.getTime();
    }
    
    MockFilter(String mainDateField) {
        super(null, mainDateField);
    }
    
    @Override
    public boolean isCountNecessary() {
      return true;
    }

    @Override
    protected boolean buildCountQuery() {
        selectBuilder.append("SELECT *");
        fromBuilder.append("FROM mock");
        splitMainDate(BEGIN, END);
        whereBuilder.append("AND fo = ?");
        return true;
    }

    @Override
    protected void buildSelectQuery() {
    }

    @Override
    public boolean isIsoFormat() {
      return false;
    }

    @Override
    public String getSingleFilter() {
      return null;
    }

    @Override
    public List<DynamicColumn> getColumns() {
      return null;
    }

}
