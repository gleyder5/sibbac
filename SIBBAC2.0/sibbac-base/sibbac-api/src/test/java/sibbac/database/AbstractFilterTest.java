package sibbac.database;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.Calendar;

import org.junit.Test;
import org.junit.Before;

public class AbstractFilterTest {
    
    private static final String DATE_FIELD = "fecha";
    
    private MockFilter mockFilter;
    
    private Calendar calendar;
    
    @Before
    public void before() {
        mockFilter = new MockFilter(DATE_FIELD);
        calendar = Calendar.getInstance();
        calendar.clear();
    }

    @Test
    public void testSplitDatesInWhereOneDay() {
        final Date begin, end;
        
        calendar.set(2017, Calendar.JANUARY, 10);
        begin = calendar.getTime();
        end = calendar.getTime();
        mockFilter.splitMainDate(begin, end);
        assertEquals("AND (fecha = '2017-01-10') ", mockFilter.whereBuilder.toString());
    }
    
    @Test
    public void testSplitDatesInWhereWrongDays() {
        final Date begin, end;
        
        calendar.set(2017, Calendar.JANUARY, 10);
        begin = calendar.getTime();
        calendar.set(2017, Calendar.JANUARY, 5);
        end = calendar.getTime();
        mockFilter.splitMainDate(begin, end);
        assertTrue(mockFilter.whereBuilder.toString().isEmpty());
    }
    
    @Test 
    public void testSplitDatesInWhereThreeDays() {
        final Date begin, end;
        
        calendar.set(2017, Calendar.JANUARY, 10);
        begin = calendar.getTime();
        calendar.set(2017, Calendar.JANUARY, 12);
        end = calendar.getTime();
        mockFilter.splitMainDate(begin, end);
        assertEquals("AND (fecha = '2017-01-10' OR fecha = '2017-01-11' OR fecha = '2017-01-12') ", 
            mockFilter.whereBuilder.toString());
    }
    
    
}
