package sibbac.common;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.pti.PTIConstants;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageField;
import sibbac.pti.PTIMessageHeader;
import sibbac.pti.messages.Field;


public class SIBBACCommons implements PTIConstants {
  
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACCommons.class);

	public static final void undefined() {
	  LOG.warn(PTIConstants.UNDEFINED);
	}

	public static final boolean isValid( final String dato ) {
		return ( dato != null && !dato.isEmpty() && dato.trim().length() > 0 );
	}

	public static final boolean isValid( final int l, final int e, final int d ) {
		return ( l == e && d == 0 ) || ( l > e && l == ( e + d ) );
	}

	public static final String convertDateToString( final Date data, final String format ) {
		if ( data==null || format==null ) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat( format );
		return sdf.format( data );
	}

	public static final String convertBigDecimalToString( final BigDecimal data, final Field field ) {
		if ( data==null || field==null ) {
			return null;
		}
		BigDecimal temp = data;

		// Determinamos el signo?
		int signum = temp.signum();

		// Lo pasamos a valor absoluto.
		temp = temp.abs();

		// Las escalas
		temp = temp.setScale( field.getDecimales(), BigDecimal.ROUND_HALF_UP );

		// El formato
		DecimalFormat df = new DecimalFormat( field.getFormatString() );
		df.setDecimalSeparatorAlwaysShown( false );
		String dato = df.format( temp.doubleValue() );
		// BUG: 13/02/2015 - Parece que aparece un punto tambien.
		dato = dato.replace( '.', ',' );
		dato = dato.replaceAll( ",", "" );

		// Check final
		if ( field.getTipo().isSigned() ) {
			if ( signum < 0 ) {
				dato = "-" + dato;
			} else {
				dato = "+" + dato;
			}
		}
		return dato;
	}

	/**
	 * Convierte de fecha en texto a fecha en tipo {@link java.util.Date}.
	 * 
	 * @param dato
	 *            La fecha en texto.
	 * @param format
	 *            El formato.
	 * @return Un {@link java.util.Date} con la fecha.
	 */
	public static final Date convertStringToDate( final String dato, final String format ) {
		SimpleDateFormat sdf = new SimpleDateFormat( format );
		Date date = null;
		try {
			date = sdf.parse( dato );
		} catch ( ParseException e ) {
			date = null;
		}
		return date;
	}

	public static String extractHeaderStringFromMessage( final String mensaje ) throws PTIMessageException {
		int max = PTIMessageHeader.HEADER_SIZE;
		if ( mensaje == null ) {
			throw new PTIMessageException( "No hay mensaje" );
		}
		if ( mensaje.length() < max ) {
			throw new PTIMessageException( "Mensaje demasiado corto (" + mensaje.length() + ")" );
		}
		return mensaje.substring( 0, max );
	}

	public static String generateFieldDefaultValue( final PTIMessageField field ) {
		int size = field.getLongitud();
		if ( field.getTipoDeCampo().isText() ) {
			return adjustString( "", size );
		} else {
			return adjustString( "0", size, false, "0" );
		}
	}

	public static final String adjustString( final String data, final int size ) {
		return adjustString( data, size, true, " " );
	}

	public static final String adjustString( final String data, final int size, final boolean fillAtTheEnd, final String fillInWith ) {
		if ( data == null || data.length() >= size ) {
			return data;
		}

		StringBuffer sb = new StringBuffer();
		int now = data.length();

		if ( !fillAtTheEnd ) {
			for ( int n = now; n < size; n++ ) {
				sb.append( fillInWith );
			}
		}
		sb.append( data );
		if ( fillAtTheEnd ) {
			for ( int n = now; n < size; n++ ) {
				sb.append( fillInWith );
			}
		}
		return sb.toString();
	}

}
