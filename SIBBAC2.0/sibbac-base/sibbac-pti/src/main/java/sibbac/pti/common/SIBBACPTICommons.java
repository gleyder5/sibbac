package sibbac.pti.common;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.pti.PTIConstants;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageField;
import sibbac.pti.PTIMessageHeader;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.Field;

public class SIBBACPTICommons implements PTIConstants {
  protected static final Logger LOG = LoggerFactory.getLogger(SIBBACPTICommons.class);

  public static final void undefined() {
    LOG.debug(PTIConstants.UNDEFINED);
  }

  public static final boolean isValid(final String dato) {
    return (dato != null && !dato.isEmpty() && dato.trim().length() > 0);
  }

  public static final boolean isValid(final int l, final int e, final int d) {
    return (l == e && d == 0) || (l > e && l == (e + d));
  }

  public static final String convertDateToString(final Date dateNow, final String format) {
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    return sdf.format(dateNow);
  }

  /**
   * Convierte de fecha en texto a fecha en tipo {@link java.util.Date}.
   * 
   * @param strDateTime La fecha en texto.
   * @param format El formato.
   * @return Un {@link java.util.Date} con la fecha.
   */
  public static final Date convertStringToDate(final String strDateTime, final String format) {
    return SIBBACCommons.convertStringToDate(strDateTime, format);
  }

  public static String extractHeaderStringFromMessage(final String mensaje) throws PTIMessageException {
    int max = PTIMessageHeader.HEADER_SIZE;
    if (mensaje == null) {
      throw new PTIMessageException("No hay mensaje");
    }
    if (mensaje.length() < max) {
      throw new PTIMessageException("Mensaje demasiado corto (" + mensaje.length() + ")");
    }
    return mensaje.substring(0, max);
  }

  public static String generateFieldDefaultValue(final PTIMessageField field) {
    int size = field.getLongitud();
    if (field.getTipoDeCampo().isText()) {
      return adjustString("", size);
    } else {
      return adjustString("0", size, false, "0");
    }
  }

  public static final String adjustString(final String data, final int size) {
    return adjustString(data, size, true, " ");
  }

  public static final String adjustString(final String data,
                                          final int size,
                                          final boolean fillAtTheEnd,
                                          final String fillInWith) {
    return SIBBACCommons.adjustString(data,size,fillAtTheEnd, fillInWith);

  }

  public static final String convertBigDecimalToString(final BigDecimal data, final Field field) {
    if (data == null || field == null) {
      return null;
    }
    BigDecimal temp = data;

    // Determinamos el signo?
    int signum = temp.signum();

    // Lo pasamos a valor absoluto.
    temp = temp.abs();

    // Las escalas
    temp = temp.setScale(field.getDecimales(), BigDecimal.ROUND_HALF_UP);

    // El formato
    DecimalFormat df = new DecimalFormat(field.getFormatString());
    df.setDecimalSeparatorAlwaysShown(false);
    String dato = df.format(temp.doubleValue());
    // BUG: 13/02/2015 - Parece que aparece un punto tambien.
    dato = dato.replace('.', ',');
    dato = dato.replaceAll(",", "");

    // Check final
    if (field.getTipo().isSigned()) {
      if (signum < 0) {
        dato = "-" + dato;
      } else {
        dato = "+" + dato;
      }
    }
    return dato;
  }

  public static void setDestinoAndUsuarioDestino(PTIMessage message, String miembro) throws PTIMessageException {
    PTIMessageType type = message.getTipo();
    switch (type) {
      case AC:
      case MO:
        break;
      default:
        message.getHeader().setEntidadDestino("PTIR");
        message.getHeader().setMiembro(miembro);
        message.getHeader().setUsuarioDestino("   ");
        break;
    }
  }

}
