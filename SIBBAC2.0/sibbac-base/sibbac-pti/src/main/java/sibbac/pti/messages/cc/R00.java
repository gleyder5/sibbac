package sibbac.pti.messages.cc;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

public class R00 extends ARecord {

  private static final String IDENTIFICADOR_FICHERO = "IDENTIFICADOR_FICHERO";
  private static final String MIEMBRO_MERCADO = "MIEMBRO_MERCADO";
  private static final String IDENTIFICADOR_CLIENTE = "IDENTIFICADOR_CLIENTE";
  private static final String DESCRIPCION_CLIENTE = "DESCRIPCION_CLIENTE";
  private static final String REFERENCIA_INFORMATIVA = "REFERENCIA_INFORMATIVA";
  private static final String IBAN = "IBAN";
  private static final String FECHA_EJECUCION = "FECHA_EJECUCION";
  private static final String FECHA_ORDEN = "FECHA_ORDEN";
  private static final String FECHA_LIQUIDACION = "FECHA_LIQUIDACION";
  private static final String ISIN = "ISIN";
  private static final String SENTIDO = "SENTIDO";
  private static final String TITULOS = "TITULOS";
  private static final String EFECTIVO = "EFECTIVO";
  private static final String ENTIDAD_PARTICIPANTE = "ENTIDAD_PARTICIPANTE";
  private static final String CORRETAJE = "CORRETAJE";
  private static final String INDICADOR_ACEPTACION = "INDICADOR_ACEPTACION";

  private static final int SIZE_IDENTIFICADOR_FICHERO = 20;
  private static final int SIZE_MIEMBRO_MERCADO = 4;
  private static final int SIZE_IDENTIFICADOR_CLIENTE = 20;
  private static final int SIZE_DESCRIPCION_CLIENTE = 40;
  private static final int SIZE_REFERENCIA_INFORMATIVA = 20;
  private static final int SIZE_IBAN = 24;
  private static final int SIZE_FECHA_EJECUCION = 8;
  private static final int SIZE_FECHA_ORDEN = 8;
  private static final int SIZE_FECHA_LIQUIDACION = 8;
  private static final int SIZE_ISIN = 12;
  private static final int SIZE_SENTIDO = 1;

  private static final int SIZE_TITULOS = 18;
  private static final int SIZE_EFECTIVO = 15;
  private static final int SIZE_ENTIDAD_PARTICIPANTE = 11;
  private static final int SIZE_CORRETAJE = 15;
  private static final int SIZE_INDICADOR_ACEPTACION = 1;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = {
                                        new Field(1, IDENTIFICADOR_FICHERO, PTIMessageFieldType.ASCII, SIZE_IDENTIFICADOR_FICHERO),
                                        new Field(2, MIEMBRO_MERCADO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_MERCADO),
                                        new Field(3, IDENTIFICADOR_CLIENTE, PTIMessageFieldType.ASCII, SIZE_IDENTIFICADOR_CLIENTE),
                                        new Field(4, DESCRIPCION_CLIENTE, PTIMessageFieldType.ASCII, SIZE_DESCRIPCION_CLIENTE),
                                        new Field(5, REFERENCIA_INFORMATIVA, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_INFORMATIVA),
                                        new Field(6, IBAN, PTIMessageFieldType.ASCII, SIZE_IBAN),
                                        new Field(7, FECHA_EJECUCION, PTIMessageFieldType.ASCII, SIZE_FECHA_EJECUCION),
                                        new Field(8, FECHA_ORDEN, PTIMessageFieldType.ASCII, SIZE_FECHA_ORDEN),
                                        new Field(9, FECHA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_LIQUIDACION),
                                        new Field(10, ISIN, PTIMessageFieldType.ASCII, SIZE_ISIN),
                                        new Field(11, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO),
                                        new Field(12, TITULOS, PTIMessageFieldType.NUMBER  , SIZE_TITULOS, 6),
                                        new Field(13, EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_EFECTIVO, 2),
                                        new Field(14, ENTIDAD_PARTICIPANTE, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_PARTICIPANTE),
                                        new Field(15, CORRETAJE, PTIMessageFieldType.SIGNED, SIZE_CORRETAJE, 2),
                                        new Field(16, INDICADOR_ACEPTACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_ACEPTACION)
                                        };

  public R00() throws PTIMessageException {
    super(PTIMessageRecordType.R00, FIELDS);
  }
  
  
  public String getIdentificadorFichero(){
    return getAsString(IDENTIFICADOR_FICHERO);
  }

  public void setIdentificadorFichero(String identificadorFichero) throws PTIMessageException{
    this.set(IDENTIFICADOR_FICHERO, identificadorFichero);
  }
  
  
  public String getMiembroMercado(){
    return getAsString(MIEMBRO_MERCADO);
  }

  public void setMiembroMercado(String miembroMercado) throws PTIMessageException{
    this.set(MIEMBRO_MERCADO, miembroMercado);
  }
  
  
  public String getIdentificadorCliente(){
    return getAsString(IDENTIFICADOR_CLIENTE);
  }

  public void setIdentificadorCliente(String identificadorCliente) throws PTIMessageException{
    this.set(IDENTIFICADOR_CLIENTE, identificadorCliente);
  }
  
  ///
  public String getDescripcionCliente(){
    return getAsString(DESCRIPCION_CLIENTE);
  }

  public void setDescripcionCliente(String descripcionCliente) throws PTIMessageException{
    this.set(DESCRIPCION_CLIENTE, descripcionCliente);
  }
  public String getReferenciaInformativa(){
    return getAsString(REFERENCIA_INFORMATIVA);
  }

  public void setReferenciaInformativa(String referenciaInformativa) throws PTIMessageException{
    this.set(REFERENCIA_INFORMATIVA, referenciaInformativa);
  }
  public String getIban(){
    return getAsString(IBAN);
  }

  public void setIban(String iban) throws PTIMessageException{
    this.set(IBAN, iban);
  }
  public Date getFechaEjecucion(){
    return getAsDate(FECHA_EJECUCION);
  }

  public void setFechaEjecucion(Date fechaEjecucion) throws PTIMessageException{
    this.set(FECHA_EJECUCION, fechaEjecucion);
  }
  
  public Date getFechaOrden(){
    return getAsDate(FECHA_ORDEN);
  }

  public void setFechaOrden(Date fechaOrden) throws PTIMessageException{
    this.set(FECHA_ORDEN, fechaOrden);
  }
  
  public Date getFechaLiquidacion(){
    return getAsDate(FECHA_LIQUIDACION);
  }

  public void setFechaLiquidacion(Date fechaLiquidacion) throws PTIMessageException{
    this.set(FECHA_LIQUIDACION, fechaLiquidacion);
  }
  
  public String getISIN(){
    return getAsString(ISIN);
  }

  public void setISIN(String isin) throws PTIMessageException{
    this.set(ISIN, isin);
  }
  public String getSentido(){
    return getAsString(SENTIDO);
  }

  public void setSentido(String sentido) throws PTIMessageException{
    this.set(SENTIDO, sentido);
  }
  public BigDecimal getTitulos(){
    return getAsNumber(TITULOS);
  }

  public void setTitulos(BigDecimal titulos) throws PTIMessageException{
    this.set(TITULOS, titulos);
  }
  public BigDecimal getEfectivo(){
    return getAsNumber(EFECTIVO);
  }

  public void setEfectivo(BigDecimal efectivo) throws PTIMessageException{
    this.set(EFECTIVO, efectivo);
  }
  public String getEntidadParticipante(){
    return getAsString(ENTIDAD_PARTICIPANTE);
  }

  public void setEntidadParticipante(String entidadParticipante) throws PTIMessageException{
    this.set(ENTIDAD_PARTICIPANTE, entidadParticipante);
  }
  public BigDecimal getCorretaje(){
    return getAsNumber(CORRETAJE);
  }

  public void setCorretaje(BigDecimal corretaje) throws PTIMessageException{
    this.set(CORRETAJE, corretaje);
  }
  public String getIndicadorAceptacion(){
    return getAsString(INDICADOR_ACEPTACION);
  }

  public void setIndicadorAceptacion(String indicadorAceptacion) throws PTIMessageException{
    this.set(INDICADOR_ACEPTACION, indicadorAceptacion);
  }
  
  
  ////
}
