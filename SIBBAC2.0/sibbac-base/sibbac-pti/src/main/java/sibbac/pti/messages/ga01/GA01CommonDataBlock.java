/**
 * 
 */
package sibbac.pti.messages.ga01;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * 
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA01CommonDataBlock extends ARecord implements PTIMessageRecord {

  // -------------------------------------------- La lista de campos.

  /**
   * Los campos del bloque de datos comunes.
   */
  public static final String ACCOUNT_SUMMARY_REPORT_ID = "ACCOUNT_SUMMARY_REPORT_ID";
  public static final String CLEARING_BUSINESS_DATE = "CLEARING_BUSINESS_DATE";
  public static final String CURRENCY = "CURRENCY";
  public static final String AGENTE_DE_PAGOS = "AGENTE_DE_PAGOS";
  public static final String EXECUTING_FIRM = "EXECUTING_FIM";
  public static final String CLEARING_FIRM = "CLEARING_FIRM";
  public static final String CREDIT_RATING = "CREDIT_RATING";
  public static final String EQTY = "EQTY";
  public static final String POSITION_ACCOUNT = "POSITION_ACCOUNT";

  public static final int SIZE_ACCOUNT_SUMMARY_REPORT_ID = 20;
  public static final int SIZE_CLEARING_BUSINESS_DATE = 8;
  public static final int SIZE_CURRENCY = 3;
  public static final int SIZE_AGENTE_DE_PAGOS = 11;
  public static final int SIZE_EXECUTING_FIRM = 4;
  public static final int SIZE_CLEARING_FIRM = 4;
  public static final int SIZE_CREDIT_RATING = 20;
  public static final int SIZE_EQTY = 15;
  public static final int SIZE_POSITION_ACCOUNT = 3;
  public static final int SIZE_NUMERO_DE_REGISTROS_R00 = 9;
  public static final int SIZE_NUMERO_DE_REGISTROS_R01 = 9;
  public static final int SIZE_NUMERO_DE_REGISTROS_R02 = 9;
  public static final int SIZE_NUMERO_DE_REGISTROS_R03 = 9;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, ACCOUNT_SUMMARY_REPORT_ID, PTIMessageFieldType.ASCII,
                                                   SIZE_ACCOUNT_SUMMARY_REPORT_ID), new Field(
                                                                                              2,
                                                                                              CLEARING_BUSINESS_DATE,
                                                                                              PTIMessageFieldType.ASCII,
                                                                                              SIZE_CLEARING_BUSINESS_DATE), new Field(
                                                                                                                                      3,
                                                                                                                                      CURRENCY,
                                                                                                                                      PTIMessageFieldType.ASCII,
                                                                                                                                      SIZE_CURRENCY), new Field(
                                                                                                                                                                4,
                                                                                                                                                                AGENTE_DE_PAGOS,
                                                                                                                                                                PTIMessageFieldType.ASCII,
                                                                                                                                                                SIZE_AGENTE_DE_PAGOS), new Field(
                                                                                                                                                                                                 5,
                                                                                                                                                                                                 EXECUTING_FIRM,
                                                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                                                 SIZE_EXECUTING_FIRM), new Field(
                                                                                                                                                                                                                                 6,
                                                                                                                                                                                                                                 CLEARING_FIRM,
                                                                                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                 SIZE_CLEARING_FIRM), new Field(
                                                                                                                                                                                                                                                                7,
                                                                                                                                                                                                                                                                CREDIT_RATING,
                                                                                                                                                                                                                                                                PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                SIZE_CREDIT_RATING), new Field(
                                                                                                                                                                                                                                                                                               8,
                                                                                                                                                                                                                                                                                               EQTY,
                                                                                                                                                                                                                                                                                               PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                               SIZE_EQTY,
                                                                                                                                                                                                                                                                                               2), new Field(
                                                                                                                                                                                                                                                                                                             9,
                                                                                                                                                                                                                                                                                                             POSITION_ACCOUNT,
                                                                                                                                                                                                                                                                                                             PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                             SIZE_POSITION_ACCOUNT)
  };

  /**
   * Constructor por defecto.
   */
  public GA01CommonDataBlock() throws PTIMessageException {
    super(PTIMessageRecordType.COMMON_DATA_BLOCK, FIELDS);
  }

  public String getAccountSummaryReportId() {
    return this.getAsString(ACCOUNT_SUMMARY_REPORT_ID);
  }

  public String getClearingBusinessDate() {
    return this.getAsString(CLEARING_BUSINESS_DATE);
  }

  public String getCurrency() {
    return this.getAsString(CURRENCY);
  }

  public String getAgenteDePagos() {
    return this.getAsString(AGENTE_DE_PAGOS);
  }

  public String getExecutingFirm() {
    return this.getAsString(EXECUTING_FIRM);
  }

  public String getClearingFirm() {
    return this.getAsString(CLEARING_FIRM);
  }

  public String getCreditRating() {
    return this.getAsString(CREDIT_RATING);
  }

  public BigDecimal getEqty() {
    return this.getAsNumber(EQTY);
  }

  public String getPositionAccount() {
    return this.getAsString(POSITION_ACCOUNT);
  }

  public void setAccountSummaryReportId(final String valor) throws PTIMessageException {
    this.set(ACCOUNT_SUMMARY_REPORT_ID, valor);
  }

  public void setClearingBusinessDate(final String valor) throws PTIMessageException {
    this.set(CLEARING_BUSINESS_DATE, valor);
  }

  public void setCurrency(final String valor) throws PTIMessageException {
    this.set(CURRENCY, valor);
  }

  public void setAgenteDePagos(final String valor) throws PTIMessageException {
    this.set(AGENTE_DE_PAGOS, valor);
  }

  public void setExecutingFirm(final String valor) throws PTIMessageException {
    this.set(EXECUTING_FIRM, valor);
  }

  public void setClearingFirm(final String valor) throws PTIMessageException {
    this.set(CLEARING_FIRM, valor);
  }

  public void setCreditRating(final String valor) throws PTIMessageException {
    this.set(CREDIT_RATING, valor);
  }

  public void setEqty(final String valor) throws PTIMessageException {
    this.set(EQTY, valor);
  }

  public void setPositionAccount(final String valor) throws PTIMessageException {
    this.set(POSITION_ACCOUNT, valor);
  }

}
