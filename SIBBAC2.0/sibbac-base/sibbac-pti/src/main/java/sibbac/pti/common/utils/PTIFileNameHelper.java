package sibbac.pti.common.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.pti.PTIFileEnvironment;
import sibbac.pti.PTIFileExtension;
import sibbac.pti.PTIFileType;

public class PTIFileNameHelper {

  private static final Logger LOG = LoggerFactory.getLogger(PTIFileNameHelper.class);

  private static final String FORMAT = "yyyyMMdd_HHmmssSS";

  /**
   * get a new valid pti file name
   * */
  public static String getNewFileName(PTIFileEnvironment entorno,
                                      String origen,
                                      String destino,
                                      PTIFileType tipo,
                                      PTIFileExtension ext) {
    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
    String date = sdf.format(new Date());
    if (date.length() > FORMAT.length()) {
      date = date.substring(0, FORMAT.length());
    }
    return entorno + "_" + origen + "_" + destino + "_" + tipo + "_" + date + "." + ext;
  }

  public static File changeFileToSendExtension(File path, File f, PTIFileExtension ext) throws IOException {
    switch (ext) {
      case DAT:
      case GZ:
      case ZIP:
        f.renameTo(new File(path, f.getName().substring(0, f.getName().indexOf(".")) + "." + ext));
        LOG.info("changeFileToSendExtension() fichero renombrado a {}", f.getAbsolutePath());
        LOG.info("changeFileToSendExtension() fichero listo para envio {}", f.getAbsolutePath());
        return f;
      default:
        throw new IOException(ext + " No es una extension valida para el envio.");

    }

  }

  public static File changeFileToSendExtension(File f, PTIFileExtension ext) throws IOException {
    return changeFileToSendExtension(f.getParentFile(), f, ext);

  }

}
