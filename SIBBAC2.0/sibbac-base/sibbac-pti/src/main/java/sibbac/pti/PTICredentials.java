package sibbac.pti;


/**
 * Clase para gestionar las credenciales de acceso a PTI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class PTICredentials {

	public static final String	KEY_PROCESS							= "PTI";

	public static final String	KEY_ORIGEN							= "Origen";
	public static final String	KEY_USUARIO_ORIGEN					= "Usuario.Origen";
	public static final String	KEY_DESTINO							= "Destino";
	public static final String	KEY_USUARIO_DESTINO					= "Usuario.Destino";
	public static final String	KEY_MIEMBRO							= "Miembro";
	public static final String	KEY_USUARIO_MIEMBRO_NORMAL			= "Usuario.Miembro.Normal";
	public static final String	KEY_USUARIO_MIEMBRO_SCRIPT_DIVIDEND	= "Usuario.Miembro.ScriptDividend";

	private String				origen;
	private String				usuarioOrigen;

	private String				destino;
	private String				usuarioDestino;

	private String				miembro;
	private String				usuarioMiembro;

	public PTICredentials() {
	}

	/**
	 * Getter for origen
	 *
	 * @return the origen
	 */
	public String getOrigen() {
		return origen;
	}

	/**
	 * Setter for origen
	 *
	 * @param origen the origen to set
	 */
	public void setOrigen( String origen ) {
		this.origen = origen;
	}

	/**
	 * Getter for usuarioOrigen
	 *
	 * @return the usuarioOrigen
	 */
	public String getUsuarioOrigen() {
		return usuarioOrigen;
	}

	/**
	 * Setter for usuarioOrigen
	 *
	 * @param usuarioOrigen the usuarioOrigen to set
	 */
	public void setUsuarioOrigen( String usuarioOrigen ) {
		this.usuarioOrigen = usuarioOrigen;
	}

	/**
	 * Getter for destino
	 *
	 * @return the destino
	 */
	public String getDestino() {
		return destino;
	}

	/**
	 * Setter for destino
	 *
	 * @param destino the destino to set
	 */
	public void setDestino( String destino ) {
		this.destino = destino;
	}

	/**
	 * Getter for usuarioDestino
	 *
	 * @return the usuarioDestino
	 */
	public String getUsuarioDestino() {
		return usuarioDestino;
	}

	/**
	 * Setter for usuarioDestino
	 *
	 * @param usuarioDestino the usuarioDestino to set
	 */
	public void setUsuarioDestino( String usuarioDestino ) {
		this.usuarioDestino = usuarioDestino;
	}

	/**
	 * Getter for miembro
	 *
	 * @return the miembro
	 */
	public String getMiembro() {
		return miembro;
	}

	/**
	 * Setter for miembro
	 *
	 * @param miembro the miembro to set
	 */
	public void setMiembro( String miembro ) {
		this.miembro = miembro;
	}

	/**
	 * Getter for usuarioMiembro
	 *
	 * @return the usuarioMiembro
	 */
	public String getUsuarioMiembro() {
		return usuarioMiembro;
	}

	/**
	 * Setter for usuarioMiembro
	 *
	 * @param usuarioMiembro the usuarioMiembro to set
	 */
	public void setUsuarioMiembro( String usuarioMiembro ) {
		this.usuarioMiembro = usuarioMiembro;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append( "PTIConfiguration:" );
		sb.append( " [origen==" + this.origen + "]" );
		sb.append( " [usuarioOrigen==" + this.usuarioOrigen + "]" );
		sb.append( " [destino==" + this.destino + "]" );
		sb.append( " [usuarioDestino==" + this.usuarioDestino + "]" );
		sb.append( " [miembro==" + this.miembro + "]" );
		sb.append( " [usuarioMiembro==" + this.usuarioMiembro + "]" );
		return sb.toString();
	}

}
