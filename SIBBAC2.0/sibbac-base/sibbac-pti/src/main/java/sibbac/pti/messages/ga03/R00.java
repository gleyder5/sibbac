package sibbac.pti.messages.ga03;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: GA01 (Garantias y Movimientos de
 * Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Marginamount
	public static final String	MARGINAMT					= "MARGINAMT";
	public static final String	MARGINAMTTYPE				= "MARGINAMTTYPE";
	public static final String	MARGINAMOUNTMARKETID		= "MARGINAMOUNTMARKETID";

	// Tamanyos de los campos.
	public static final int		SIZE_MARGINAMT				= 15;
	public static final int		SIZE_MARGINAMTTYPE			= 3;
	public static final int		SIZE_MARGINAMOUNTMARKETID	= 2;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS						= {
			new Field( 1, MARGINAMT, PTIMessageFieldType.NUMBER, SIZE_MARGINAMT, 2 ),
			new Field( 2, MARGINAMTTYPE, PTIMessageFieldType.NUMBER, SIZE_MARGINAMTTYPE, 0 ),
			new Field( 3, MARGINAMOUNTMARKETID, PTIMessageFieldType.ASCII, SIZE_MARGINAMOUNTMARKETID )
															};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public BigDecimal getMarginAmt() {
		return this.getAsNumber( MARGINAMT );
	}

	public BigDecimal getMarginAmtType() {
		return this.getAsNumber( MARGINAMTTYPE );
	}

	public String getMarginAmountMarketId() {
		return this.getAsString( MARGINAMOUNTMARKETID );
	}

	public void setMarginAmt( final String valor ) throws PTIMessageException {
		this.set( MARGINAMT, valor );
	}

	public void setMarginAmtType( final String valor ) throws PTIMessageException {
		this.set( MARGINAMTTYPE, valor );
	}

	public void setMarginAmountMarketId( final String valor ) throws PTIMessageException {
		this.set( MARGINAMOUNTMARKETID, valor );
	}

}
