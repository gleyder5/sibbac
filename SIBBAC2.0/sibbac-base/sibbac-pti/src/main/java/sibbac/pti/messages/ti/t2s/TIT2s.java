package sibbac.pti.messages.ti.t2s;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ti.R00;
import sibbac.pti.messages.ti.R01;
import sibbac.pti.messages.ti.TI;

/**
 * Mensaje de tipo TI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class TIT2s extends TI {

  /**
   * Constructor por defecto.
   */
  public TIT2s() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public TIT2s(final boolean fromPTI) throws PTIMessageException {
    super(fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException {
    switch (tipo) {
      case R00:
        return new R00();
      case R01:
        return new R01();
      case R02:
        return new R02T2s();
      case R03:
        return new R03T2s();
      case R04:
        return new R04T2s();
      default:
        return null;
    }
  }

}
