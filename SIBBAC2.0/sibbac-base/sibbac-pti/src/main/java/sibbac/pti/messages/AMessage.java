package sibbac.pti.messages;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sibbac.common.SIBBACCommons;
import sibbac.pti.PTIConstants;
import sibbac.pti.PTICredentials;
import sibbac.pti.PTIMessage;
import sibbac.pti.PTIMessageControlBlock;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFactory;
import sibbac.pti.PTIMessageHeader;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;

/**
 * Clase abstracta que representa un Mensaje PTI.
 * <p/>
 * Esta clase contiene las funcionalidades comunes a cada tipo de mensaje PTI.<br/>
 * Los diversos mensajes PTI estan en subpaquetes de este paquete, y todos extienden esta clase.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public abstract class AMessage implements PTIMessage {

	/**
	 * Los tipos de registros de conteo de un bloque de control.
	 */
	private static final PTIMessageRecordType[]	NUM_REGISTROS	= {
			PTIMessageRecordType.R00, PTIMessageRecordType.R01, PTIMessageRecordType.R02, PTIMessageRecordType.R03,
			PTIMessageRecordType.R04, PTIMessageRecordType.R05, PTIMessageRecordType.R06, PTIMessageRecordType.R07
																};
	
	
  // ------------------------------------------ Generales del Mensaje

  /**
   * Tipo de mensaje.
   */
  private PTIMessageType tipo;

  /**
   * Si el mensaje ha sido recibido desde PTI o no.
   */
  private boolean fromPTI;

  // ------------------------------------------ Secciones del Mensaje

  /**
   * El encabezado.
   */
  private PTIMessageHeader header = null;

  /**
   * El bloque de datos comunes.
   */
  protected PTIMessageRecord commonDataBlock = null;

  /**
   * El bloque de control.
   */
  protected PTIMessageControlBlock controlBlock = null;

  /**
   * La seccion con los registros R00.
   */
  private List<PTIMessageRecord> r00 = null;

  /**
   * La seccion con los registros R01.
   */
  private List<PTIMessageRecord> r01 = null;

  /**
   * La seccion con los registros R02.
   */
  private List<PTIMessageRecord> r02 = null;

  /**
   * La seccion con los registros R03.
   */
  private List<PTIMessageRecord> r03 = null;

  /**
   * La seccion con los registros R04.
   */
  private List<PTIMessageRecord> r04 = null;

  /**
   * La seccion con los registros R05.
   */
  private List<PTIMessageRecord> r05 = null;

  /**
   * La seccion con los registros R06.
   */
  private List<PTIMessageRecord> r06 = null;

  /**
   * La seccion con los registros R07.
   */
  private List<PTIMessageRecord> r07 = null;

  private String idLote = null;

  private String idCorrelacion = null;

  private boolean processed = false;

  private Integer ordenEnLote = null;

  private boolean verifiedBME = false;

  private boolean isInGroup = false;

  private boolean isLastInGroup = false;

  // -------------------------------------------------- Constructores

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public AMessage() throws PTIMessageException {
    this(PTIMessageType.UNKNOWN, false, null);
  }

  /**
   * Constructor general para un tipo de mensaje dado.
   * 
   * @param tipo Una instancia de {@link PTIMessageType}.
   * @param fromPTI TRUE o FALSE para indicar si el mensaje es generado para remitir a PTI (FALSE) o si es un parseo de
   *          un mensaje recibido de PTI (TRUE).
   * 
   * @throws PTIMessageException Si algo falla a la hora de construir el mensaje.
   */
  public AMessage(final PTIMessageType tipo, final boolean fromPTI) throws PTIMessageException {
    this(tipo, fromPTI, null);
  }

  /**
   * Constructor general para un tipo de mensaje dado.
   * <p/>
   * A diferencia de {@link #AMessage(PTIMessageType, boolean)}, este constructor permite establecer tambien la cabecera
   * del mensaje.
   * 
   * @param tipo Una instancia de {@link PTIMessageType}.
   * @param fromPTI TRUE o FALSE para indicar si el mensaje es generado para remitir a PTI (FALSE) o si es un parseo de
   *          un mensaje recibido de PTI (TRUE).
   * @param header Una instancia de {@link PTIMessageHeader} con la cabecera de datos comunes a todos los mensajes.
   * 
   * @throws PTIMessageException Si algo falla a la hora de construir el mensaje.
   */
  public AMessage(final PTIMessageType tipo, final boolean fromPTI, final PTIMessageHeader header) throws PTIMessageException {
    this.tipo = tipo;
    this.fromPTI = fromPTI;

    if (header == null) {
      PTICredentials creds = PTIMessageFactory.getCredentials();
      if (creds == null) {
        throw new PTIMessageException("No PTI credentials found!!!");
      }
      try {
        this.header = new Header();
        this.header.set(PTIMessageHeader.TIPO_DE_REGISTRO, this.tipo.name());
        this.header.set(PTIMessageHeader.ORIGEN, creds.getOrigen());
        this.header.set(PTIMessageHeader.DESTINO, creds.getDestino());
        this.header.set(PTIMessageHeader.MIEMBRO, creds.getMiembro());
        this.header.set(PTIMessageHeader.USUARIO_ORIGEN, creds.getUsuarioOrigen());
        this.header.set(PTIMessageHeader.USUARIO_DESTINO, creds.getUsuarioDestino());
        this.header.set(PTIMessageHeader.USUARIO_MIEMBRO, creds.getUsuarioMiembro());
      } catch (PTIMessageException e) {
        this.header = null;
      }
    }

    this.r00 = new ArrayList<PTIMessageRecord>();
    this.r01 = new ArrayList<PTIMessageRecord>();
    this.r02 = new ArrayList<PTIMessageRecord>();
    this.r03 = new ArrayList<PTIMessageRecord>();
    this.r04 = new ArrayList<PTIMessageRecord>();
    this.r05 = new ArrayList<PTIMessageRecord>();
    this.r06 = new ArrayList<PTIMessageRecord>();
    this.r07 = new ArrayList<PTIMessageRecord>();

    // Preparamos la definicion particularizada del mensaje.
    this.messageDefinition();
  }

  // --------------------------------------------- Metodos Abstractos

  /**
   * Para terminar de configurar el mensaje; cosas, por ejemplo, como definir el bloque de control, si tiene, o el
   * bloque de datos comunes.
   * 
   * @throws PTIMessageException Si algo falla a la hora de definir el mensaje.
   */
  public abstract void messageDefinition() throws PTIMessageException;

  // ------------------------------------------- Metodos del Interfaz

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#parseMessage(String)
   */
  @Override
  public void parseMessage(final String texto) throws PTIMessageException {
    String recordData = texto;
    String blockData = null;
    int size = 0;

    // Primero, el bloque de datos comunes, si lo hay.
    if (this.hasCommonDataBlock()) {
      size = this.commonDataBlock.getLongitud();
      blockData = recordData.substring(0, size);
      recordData = recordData.substring(size);
      this.commonDataBlock.parseRecord(blockData);
    } else {
      // No records to parse.
    }

    // Luego, el bloque de control, si lo hay.
    if (this.hasControlBlock()) {
      size = this.controlBlock.getLongitud();
      blockData = recordData.substring(0, size);
      recordData = recordData.substring(size);
      this.controlBlock.parseRecord(blockData);
    } else {
      // No records to parse.
    }

    // Luego los registros.
    if (recordData != null && recordData.trim().length() > 0) {
      this.parseRecordsData(recordData);
    } else {
      // No records to parse.
    }

  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getCommonDataBlock()
   */
  @Override
  public PTIMessageRecord getCommonDataBlock() {
    return this.commonDataBlock;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getControlBlock()
   */
  @Override
  public PTIMessageRecord getControlBlock() {
    return this.controlBlock;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#isFromPTI()
   */
  @Override
  public boolean isFromPTI() {
    return this.fromPTI;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#setFromPTI(boolean)
   */
  public void setFromPTI(final boolean fromPTI) {
    this.fromPTI = fromPTI;
    this.header.setFromPTI(this.fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#setHeader(sibbac.pti.PTIMessageHeader)
   */
  @Override
  public void setHeader(final PTIMessageHeader header) {
    this.header = header;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getHeader()
   */
  @Override
  public PTIMessageHeader getHeader() {
    return this.header;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#hasCommonDataBlock()
   */
  @Override
  public boolean hasCommonDataBlock() {
    return this.tipo.hasCommonDataBlock();
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#hasControlBlock()
   */
  @Override
  public boolean hasControlBlock() {
    return this.tipo.hasControlBlock();
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#hasRecords(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public boolean hasRecords(final PTIMessageRecordType type) {
    switch (type) {
      case CONTROL_BLOCK:
        return this.hasControlBlock();
      case COMMON_DATA_BLOCK:
        return this.hasCommonDataBlock();
      case HEADER:
        return this.header != null;
      case R00:
        return (this.r00 != null && !this.r00.isEmpty());
      case R01:
        return (this.r01 != null && !this.r01.isEmpty());
      case R02:
        return (this.r02 != null && !this.r02.isEmpty());
      case R03:
        return (this.r03 != null && !this.r03.isEmpty());
      case R04:
        return (this.r04 != null && !this.r04.isEmpty());
      case R05:
        return (this.r05 != null && !this.r05.isEmpty());
      case R06:
        return (this.r06 != null && !this.r06.isEmpty());
      case R07:
        return (this.r07 != null && !this.r07.isEmpty());
      default:
        return false;
    }
  }

  @Override
  public int getLongitud() {
    int total = 0;
    for (PTIMessageRecordType tipo : PTIMessageRecordType.values()) {
      switch (tipo) {
        case CONTROL_BLOCK:
          total += (this.hasControlBlock()) ? this.controlBlock.getLongitud() : 0;
          break;
        case COMMON_DATA_BLOCK:
          total += (this.hasCommonDataBlock()) ? this.commonDataBlock.getLongitud() : 0;
          break;
        case HEADER:
          total += this.header.getLongitud();
          break;
        case R00:
          if (this.hasRecords(PTIMessageRecordType.R00)) {
            for (PTIMessageRecord r : this.r00) {
              total += r.getLongitud();
            }
          }
          break;
        case R01:
          if (this.hasRecords(PTIMessageRecordType.R01)) {
            for (PTIMessageRecord r : this.r01) {
              total += r.getLongitud();
            }
          }
          break;
        case R02:
          if (this.hasRecords(PTIMessageRecordType.R02)) {
            for (PTIMessageRecord r : this.r02) {
              total += r.getLongitud();
            }
          }
          break;
        case R03:
          if (this.hasRecords(PTIMessageRecordType.R03)) {
            for (PTIMessageRecord r : this.r03) {
              total += r.getLongitud();
            }
          }
          break;
        case R04:
          if (this.hasRecords(PTIMessageRecordType.R04)) {
            for (PTIMessageRecord r : this.r04) {
              total += r.getLongitud();
            }
          }
          break;
        case R05:
          if (this.hasRecords(PTIMessageRecordType.R05)) {
            for (PTIMessageRecord r : this.r05) {
              total += r.getLongitud();
            }
          }
          break;
        case R06:
          if (this.hasRecords(PTIMessageRecordType.R06)) {
            for (PTIMessageRecord r : this.r06) {
              total += r.getLongitud();
            }
          }
          break;
        case R07:
          if (this.hasRecords(PTIMessageRecordType.R07)) {
            for (PTIMessageRecord r : this.r07) {
              total += r.getLongitud();
            }
          }
          break;
        default:
      }
    }
    return total;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getReferenciaECC()
   */
  @Override
  public String getReferenciaECC() {
    return null;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#setFechaDeEnvio()
   */
  @Override
  public void setFechaDeEnvio() throws PTIMessageException {
    Date dateTime = new Date();
    this.header.set(PTIMessageHeader.FECHA, SIBBACCommons.convertDateToString(dateTime, PTIConstants.DATE_FORMAT));
    this.header.set(PTIMessageHeader.HORA, SIBBACCommons.convertDateToString(dateTime, PTIConstants.TIME_FORMAT));
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getFechaDeEnvio()
   */
  @Override
  public Date getFechaDeEnvio() throws PTIMessageException {
    return this.header.getFecha();
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getFechaDeEnvio()
   */
  @Override
  public Time getHoraDeEnvio() throws PTIMessageException {
    return new Time(this.header.getAsTime(PTIMessageHeader.HORA).getTime());
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getTipo()
   */
  @Override
  public PTIMessageType getTipo() {
    return this.tipo;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getErrorGeneral()
   */
  @Override
  public String getErrorGeneral() {
    return this.header.getErrorCode();
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getErrorDeMensaje()
   */
  @Override
  public String getErrorDeMensaje() {
    // TODO Auto-generated method stub
    SIBBACCommons.undefined();
    return null;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#addRecord(sibbac.pti.PTIMessageRecordType, sibbac.pti.PTIMessageRecord)
   */
  @Override
  public void addRecord(final PTIMessageRecordType tipo, final PTIMessageRecord registro) throws PTIMessageException {
    PTIMessageControlBlock cb = (PTIMessageControlBlock) this.controlBlock;
    switch (tipo) {
      case COMMON_DATA_BLOCK:
      case UNKNOWN:
      case CONTROL_BLOCK:
      case HEADER:
        break;
      case R00:
        this.r00.add(this.r00.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R00);
        }
        break;
      case R01:
        this.r01.add(this.r01.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R01);
        }
        break;
      case R02:
        this.r02.add(this.r02.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R02);
        }
        break;
      case R03:
        this.r03.add(this.r03.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R03);
        }
        break;
      case R04:
        this.r04.add(this.r04.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R04);
        }
        break;
      case R05:
        this.r05.add(this.r05.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R05);
        }
        break;
      case R06:
        this.r06.add(this.r06.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R06);
        }
        break;
      case R07:
        this.r07.add(this.r07.size(), registro);
        if (!this.fromPTI) {
          cb.addOneRecord(PTIMessageRecordType.R07);
        }
        break;
      default:
        break;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getAll(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public List<PTIMessageRecord> getAll(final PTIMessageRecordType tipo) {
    switch (tipo) {
      case CONTROL_BLOCK:
      case COMMON_DATA_BLOCK:
      case HEADER:
        return null;
      case R00:
        return this.r00;
      case R01:
        return this.r01;
      case R02:
        return this.r02;
      case R03:
        return this.r03;
      case R04:
        return this.r04;
      case R05:
        return this.r05;
      case R06:
        return this.r06;
      case R07:
        return this.r07;
      default:
        return null;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getCount(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public int getCount(final PTIMessageRecordType type) {
    switch (type) {
      case CONTROL_BLOCK:
        return this.hasControlBlock() ? 1 : 0;
      case COMMON_DATA_BLOCK:
        return this.hasCommonDataBlock() ? 1 : 0;
      case HEADER:
        return 1;
      case R00:
        return this.r00.size();
      case R01:
        return this.r01.size();
      case R02:
        return this.r02.size();
      case R03:
        return this.r03.size();
      case R04:
        return this.r04.size();
      case R05:
        return this.r05.size();
      case R06:
        return this.r06.size();
      case R07:
        return this.r07.size();
      default:
        return 0;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTICommons#toPTIFormat()
   */
  @Override
  public String toPTIFormat() throws PTIMessageException {
    StringBuffer sb = new StringBuffer();
    sb.append(this.header.toPTIFormat());
    if (this.tipo.hasCommonDataBlock()) {
      sb.append(this.commonDataBlock.toPTIFormat());
    }
    if (this.tipo.hasControlBlock()) {
      sb.append(this.controlBlock.toPTIFormat());
    }
    if (this.hasRecords(PTIMessageRecordType.R00)) {
      for (PTIMessageRecord record : this.r00) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R01)) {
      for (PTIMessageRecord record : this.r01) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R02)) {
      for (PTIMessageRecord record : this.r02) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R03)) {
      for (PTIMessageRecord record : this.r03) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R04)) {
      for (PTIMessageRecord record : this.r04) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R05)) {
      for (PTIMessageRecord record : this.r05) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R06)) {
      for (PTIMessageRecord record : this.r06) {
        sb.append(record.toPTIFormat());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R07)) {
      for (PTIMessageRecord record : this.r07) {
        sb.append(record.toPTIFormat());
      }
    }
    return sb.toString();
  }

  /*
   * (non-Javadoc)
   * @see Object#toString()
   */
  @Override
  public String toString() {
    StringBuffer sb = new StringBuffer();
    sb.append(this.header.toString());
    if (this.tipo.hasCommonDataBlock()) {
      sb.append(this.commonDataBlock.toString());
    }
    if (this.tipo.hasControlBlock()) {
      sb.append(this.controlBlock.toString());
    }
    if (this.hasRecords(PTIMessageRecordType.R00)) {
      for (PTIMessageRecord record : this.r00) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R01)) {
      for (PTIMessageRecord record : this.r01) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R02)) {
      for (PTIMessageRecord record : this.r02) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R03)) {
      for (PTIMessageRecord record : this.r03) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R04)) {
      for (PTIMessageRecord record : this.r04) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R05)) {
      for (PTIMessageRecord record : this.r05) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R06)) {
      for (PTIMessageRecord record : this.r06) {
        sb.append(record.toString());
      }
    }
    if (this.hasRecords(PTIMessageRecordType.R07)) {
      for (PTIMessageRecord record : this.r07) {
        sb.append(record.toString());
      }
    }
    return sb.toString();
  }

  // ----------------------------------------------- Metodos Internos

  /**
   * Metodo de conveniencia para que cada mensaje pueda parsear el contenido del mensaje que hace referencia
   * exclusivamente a los datos de los registros.
   * 
   * @param data Un {@link String} con la parte del mensaje de texto que hace referencia exclusivamente a los datos de
   *          los registros PTI.
   * @throws PTIMessageException Si ocurre algun error parseando la cadena de texto.
   */
  // public abstract void parseRecordsData(final String data) throws
  // PTIMessageException;
  private void parseRecordsData(final String texto) throws PTIMessageException {
    // Si no hay texto, adios.
    if (texto != null && !texto.isEmpty()) {
      int numRegistros = 0;
      PTIMessageRecord record = null;
      int size = 0;
      int ini = 0, end = 0;
      String data = null;

      for (PTIMessageRecordType recordType : NUM_REGISTROS) {
        numRegistros = this.controlBlock.getNumeroDeRegistros(recordType);
        if (numRegistros <= 0) {
          continue;
        }

        for (int n = 0; n < numRegistros; n++) {
          record = this.getRecordInstance(recordType);
          size = record.getLongitud();
          end += size;
          try {
            data = texto.substring(ini, end);
            record.parseRecord(data);
            this.addRecord(recordType, record);
            ini += size;
          } catch (StringIndexOutOfBoundsException e) {
            String substr = texto.substring(ini);
            throw new PTIMessageException("StringIndexOutOfBoundsException: [texto(ini:" + ini + "/end:" + end
                                          + "/diff:" + (end - ini) + "):" + texto + "::substr(from:" + ini + ")==<"
                                          + substr + ">]");
          } catch (NullPointerException e) {
            String substr = texto.substring(ini);
            throw new PTIMessageException("NullPointerException: [texto(ini:" + ini + "/end:" + end + "/diff:"
                                          + (end - ini) + "):" + texto + "::substr(from:" + ini + ")==<" + substr
                                          + ">]");
          }
        }
      }
    } else {
      throw new PTIMessageException("Texto no valido: [" + texto + "]");
    }
  }

  @Override
  public boolean isParteDeUnLote() {
    return this.idLote != null && this.ordenEnLote != null;
  }

  @Override
  public boolean isPersisted() {
    return false;
  }

  @Override
  public String getIdCorrelacion() {
    return idCorrelacion;
  }

  @Override
  public String getIdLote() {
    return idLote;
  }

  @Override
  public Integer getOrdenEnLote() {
    return this.ordenEnLote;
  }

  @Override
  public boolean isProcessed() {
    return this.processed;
  }

  @Override
  public boolean isVerifiedBME() {
    return this.verifiedBME;
  }

  @Override
  public void setIdCorrelacion(String idCorrelacion) {
    this.idCorrelacion = idCorrelacion;
  }

  @Override
  public void setIdLote(String idLote) {
    this.idLote = idLote;

  }

  @Override
  public void setOrdenEnLote(Integer orden) {
    this.ordenEnLote = orden;

  }

  @Override
  public void setProcessed(boolean processed) {
    this.processed = processed;

  }

  @Override
  public void setVerifiedBME(boolean verified) {
    this.verifiedBME = verified;

  }

  @Override
  public boolean isLastInGroup() {
    return isLastInGroup;
  }

  @Override
  public void setLastInGroup(boolean isLast) {
    this.isLastInGroup = isLast;
  }

  public boolean isInGroup() {
    return isInGroup;
  }

  public void setInGroup(boolean isInGroup) {
    this.isInGroup = isInGroup;
  }

  @Override
  public PTIMessageRecord getRecordInstance(PTIMessageRecordType tipo) throws PTIMessageException {
    // TODO Auto-generated method stub
    return null;
  }

}
