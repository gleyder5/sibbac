package sibbac.pti.messages.pv;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.AControlBlock;
import sibbac.pti.messages.Field;


/**
 * Bloque de control.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class PVControlBlock extends AControlBlock {

	// -------------------------------------------- La lista de campos.
	public static final String	R00		= PTIMessageRecordType.R00.name();
	public static final String	R01		= PTIMessageRecordType.R01.name();

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS	= {
			new Field( 1, R00, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ), new Field( 2, R01, PTIMessageFieldType.NUMBER, SIZE_CONTROL )
										};

	/**
	 * Constructor por defecto.
	 * 
	 */
	public PVControlBlock() throws PTIMessageException {
		super( PTIMessageRecordType.CONTROL_BLOCK, FIELDS );
	}

	public BigDecimal getR00() {
		return this.getAsNumber( R00 );
	}

	public BigDecimal getR01() {
		return this.getAsNumber( R01 );
	}

	public void setR00( final String valor ) throws PTIMessageException {
		this.set( R00, valor );
	}

	public void setR01( final String valor ) throws PTIMessageException {
		this.set( R01, valor );
	}


}
