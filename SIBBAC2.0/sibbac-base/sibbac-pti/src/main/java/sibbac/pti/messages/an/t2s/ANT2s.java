package sibbac.pti.messages.an.t2s;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.R00;
import sibbac.pti.messages.an.R01;
import sibbac.pti.messages.an.R02;
import sibbac.pti.messages.an.R03;
import sibbac.pti.messages.an.R05;
import sibbac.pti.messages.an.R06;
import sibbac.pti.messages.an.R07;

/**
 * Mensaje de tipo AN.
 * <p/>
 * Definicion: PDF BME, pg 15.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class ANT2s extends AN {

  /**
   * Constructor por defecto.
   */
  public ANT2s() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public ANT2s(final boolean fromPTI) throws PTIMessageException {
    super(fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException {
    switch (tipo) {
      case R00:
        return new R00();
      case R01:
        return new R01();
      case R02:
        return new R02();
      case R03:
        return new R03();
      case R04:
        return new R04T2s();
      case R05:
        return new R05();
      case R06:
        return new R06();
      case R07:
        return new R07();
      default:
        return null;
    }
  }

}
