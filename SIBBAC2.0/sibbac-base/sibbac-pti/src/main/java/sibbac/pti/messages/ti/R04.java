package sibbac.pti.messages.ti;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R04 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R04 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Modo Titular/Operacion
	public static final String	ID_ECC								= "ID_ECC";
	public static final String	SEGMENTO_DE_LA_ECC					= "SEGMENTO_DE_LA_ECC";
	public static final String	MIEMBRO_ECC							= "MIEMBRO_ECC";
	public static final String	CUENTA_LIQUIDACION					= "CUENTA_LIQUIDACION";
	public static final String	FECHA_TEORICA_LIQUIDACION			= "FECHA_TEORICA_LIQUIDACION";
	public static final String	CUENTA_ECC							= "CUENTA_ECC";
	public static final String	SENTIDO								= "SENTIDO";
	public static final String	PRECIO								= "PRECIO";
	public static final String	ISIN								= "ISIN";
	public static final String	INDICADOR_COTIZACION				= "INDICADOR_COTIZACION";
	public static final String	NUM_VALORES_IMPORTE_NOMINAL			= "NUM_VALORES_IMPORTE_NOMINAL";
	public static final String	IMPORTE_EFECTIVO					= "IMPORTE_EFECTIVO";
	public static final String	NUM_VALORES_FALLIDOS				= "NUM_VALORES_FALLIDOS";
	public static final String	TIPO_OPERACION_MERCADO				= "TIPO_OPERACION_MERCADO";
	public static final String	CANON_TEORICO						= "CANON_TEORICO";
	public static final String	CANON_APLICADO						= "CANON_APLICADO";

	// Tamanyos de los campos.
	public static final int		SIZE_ID_ECC							= 11;
	public static final int		SIZE_SEGMENTO_DE_LA_ECC				= 2;
	public static final int		SIZE_MIEMBRO_ECC					= 11;
	public static final int		SIZE_CUENTA_LIQUIDACION				= 35;
	public static final int		SIZE_FECHA_TEORICA_LIQUIDACION		= 8;
	public static final int		SIZE_CUENTA_ECC						= 35;
	public static final int		SIZE_SENTIDO						= 1;
	public static final int		SIZE_PRECIO							= 13;
	public static final int		SIZE_ISIN							= 12;
	public static final int		SIZE_INDICADOR_COTIZACION			= 1;
	public static final int		SIZE_NUM_VALORES_IMPORTE_NOMINAL	= 18;
	public static final int		SIZE_IMPORTE_EFECTIVO				= 15;
	public static final int		SIZE_NUM_VALORES_FALLIDOS			= 18;
	public static final int		SIZE_TIPO_OPERACION_MERCADO			= 2;
	public static final int		SIZE_CANON_TEORICO					= 15;
	public static final int		SIZE_CANON_APLICADO					= 15;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 2, SEGMENTO_DE_LA_ECC, PTIMessageFieldType.ASCII, SIZE_SEGMENTO_DE_LA_ECC ),
			new Field( 3, MIEMBRO_ECC, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_ECC ),
			new Field( 4, CUENTA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_LIQUIDACION ),
			new Field( 5, FECHA_TEORICA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_TEORICA_LIQUIDACION ),
			new Field( 6, CUENTA_ECC, PTIMessageFieldType.ASCII, SIZE_CUENTA_ECC ),
			new Field( 5, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 6, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6 ),
			new Field( 7, ISIN, PTIMessageFieldType.ASCII, SIZE_ISIN ),
			new Field( 8, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 9, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 10, IMPORTE_EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_IMPORTE_EFECTIVO, 2 ),
			new Field( 11, NUM_VALORES_FALLIDOS, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_FALLIDOS, 6 ),
			new Field( 12, TIPO_OPERACION_MERCADO, PTIMessageFieldType.ASCII, SIZE_TIPO_OPERACION_MERCADO ),
			new Field( 13, CANON_TEORICO, PTIMessageFieldType.NUMBER, SIZE_CANON_TEORICO, 2 ),
			new Field( 14, CANON_APLICADO, PTIMessageFieldType.NUMBER, SIZE_CANON_APLICADO, 2 )
	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R04() throws PTIMessageException {
		super( PTIMessageRecordType.R04, FIELDS );
	}
	

  protected R04(final Field[] recordFields) throws PTIMessageException {
    super( PTIMessageRecordType.R04, recordFields );
  }

	public String getIdECC() {
		return this.getAsString( ID_ECC );
	}

	public String getSegmentoECC() {
		return this.getAsString( SEGMENTO_DE_LA_ECC );
	}

	public String getMiembroEcc() {
		return this.getAsString( MIEMBRO_ECC );
	}

	public String getCuentaLiquidacion() {
		return this.getAsString( CUENTA_LIQUIDACION );
	}

	public Date getFechaTeoricaLiquidacion() {
		return this.getAsDate( FECHA_TEORICA_LIQUIDACION );
	}

	public String getCuentaECC() {
		return this.getAsString( CUENTA_ECC );
	}

	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public BigDecimal getPrecio() {
		return this.getAsNumber( PRECIO );
	}

	public String getISIN() {
		return this.getAsString( ISIN );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public BigDecimal getNumValoresImporteNominal() {
		return this.getAsNumber( NUM_VALORES_IMPORTE_NOMINAL );
	}

	public BigDecimal getImporteEfectivo() {
		return this.getAsNumber( IMPORTE_EFECTIVO );
	}

	public BigDecimal getNumValoresFallidos() {
		return this.getAsNumber( NUM_VALORES_FALLIDOS );
	}

	public String getTipoOperacionMercado() {
		return this.getAsString( TIPO_OPERACION_MERCADO );
	}

	public BigDecimal getCanonTeorico() {
		return this.getAsNumber( CANON_TEORICO );
	}

	public BigDecimal getCanonAplicado() {
		return this.getAsNumber( CANON_APLICADO );
	}

	public void setIdECC( final String valor ) throws PTIMessageException {
		this.set( ID_ECC, valor );
	}

	public void setSegmentoECC( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO_DE_LA_ECC, valor );
	}

	public void setMiembroEcc( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_ECC, valor );
	}

	public void setCuentaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( CUENTA_LIQUIDACION, valor );
	}

	public void setFechaTeoricaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_TEORICA_LIQUIDACION, valor );
	}

	public void setCuentaECC( final String valor ) throws PTIMessageException {
		this.set( CUENTA_ECC, valor );
	}

	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}

	public void setPrecio( final String valor ) throws PTIMessageException {
		this.set( PRECIO, valor );
	}

	public void setPrecio( final BigDecimal valor ) throws PTIMessageException {
		this.set( PRECIO, valor );
	}

	public void setISIN( final String valor ) throws PTIMessageException {
		this.set( ISIN, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setNumValoresImporteNominal( final BigDecimal valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setNumValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setImporteEfectivo( final BigDecimal valor ) throws PTIMessageException {
		this.set( IMPORTE_EFECTIVO, valor );
	}

	public void setImporteEfectivo( final String valor ) throws PTIMessageException {
		this.set( IMPORTE_EFECTIVO, valor );
	}

	public void setNumValoresFallidos( final BigDecimal valor ) throws PTIMessageException {
		this.set( NUM_VALORES_FALLIDOS, valor );
	}

	public void setNumValoresFallidos( final String valor ) throws PTIMessageException {
		this.set( NUM_VALORES_FALLIDOS, valor );
	}
	
	public void setTipoOperacionMercado( final String valor ) throws PTIMessageException {
		this.set( TIPO_OPERACION_MERCADO, valor );
	}
	
	public void setCanonTeorico( final BigDecimal valor ) throws PTIMessageException {
		this.set( CANON_TEORICO, valor );
	}

	public void setCanonTeorico( final String valor ) throws PTIMessageException {
		this.set( CANON_TEORICO, valor );
	}

	public void setCanonAplicado( final BigDecimal valor ) throws PTIMessageException {
		this.set( CANON_APLICADO, valor );
	}

	public void setCanonAplicado( final String valor ) throws PTIMessageException {
		this.set( CANON_APLICADO, valor );
	}


}
