package sibbac.pti.messages.ce;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R00 para el mensaje de tipo: CE. (INDICADOR DE CAPACIDAD DE CUENTA / CUENTA EXENTA-NO EXENTA).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

  // -------------------------------------------- La lista de campos.

  //
  public static final String MIEMBRO_DEL_MERCADO = "MIEMBRO_DEL_MERCADO";
  public static final String FECHA_CONTRATACION = "FECHA_CONTRATACION";
  public static final String CODIGO_ISIN = "CODIGO_ISIN";
  public static final String SEGMENTO_DE_MERCADO = "SEGMENTO_DE_MERCADO";
  public static final String NUMERO_DE_EJECUCION = "NUMERO_DE_EJECUCION";
  public static final String TIPO_OPERACION = "TIPO_OPERACION";
  public static final String INICADOR_COMPRA_VENTA = "INICADOR_COMPRA_VENTA";
  public static final String INDICADOR_CAPACIDAD_T_REPORTING = "INDICADOR_CAPACIDAD_T_REPORTING";
  public static final String INDICADOR_EXENTA = "INDICADOR_EXENTA";
  public static final String CODIGO_ERROR = "CODIGO_ERROR";

  // Tamanyos de los campos.
  public static final int SIZE_MIEMBRO_DEL_MERCADO = 4;
  public static final int SIZE_FECHA_CONTRATACION = 8;
  public static final int SIZE_CODIGO_ISIN = 12;
  public static final int SIZE_SEGMENTO_DE_MERCADO = 2;
  public static final int SIZE_NUMERO_DE_EJECUCION = 9;
  public static final int SIZE_TIPO_OPERACION = 2;
  public static final int SIZE_INICADOR_COMPRA_VENTA = 1;
  public static final int SIZE_INDICADOR_CAPACIDAD_T_REPORTING = 1;
  public static final int SIZE_INDICADOR_EXENTA = 1;
  public static final int SIZE_CODIGO_ERROR = 3;
  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, MIEMBRO_DEL_MERCADO, PTIMessageFieldType.ASCII,
                                                   SIZE_MIEMBRO_DEL_MERCADO), new Field(2, FECHA_CONTRATACION,
                                                                                        PTIMessageFieldType.ASCII,
                                                                                        SIZE_FECHA_CONTRATACION), new Field(
                                                                                                                            3,
                                                                                                                            CODIGO_ISIN,
                                                                                                                            PTIMessageFieldType.ASCII,
                                                                                                                            SIZE_CODIGO_ISIN), new Field(
                                                                                                                                                         4,
                                                                                                                                                         SEGMENTO_DE_MERCADO,
                                                                                                                                                         PTIMessageFieldType.ASCII,
                                                                                                                                                         SIZE_SEGMENTO_DE_MERCADO), new Field(
                                                                                                                                                                                              5,
                                                                                                                                                                                              NUMERO_DE_EJECUCION,
                                                                                                                                                                                              PTIMessageFieldType.NUMBER,
                                                                                                                                                                                              SIZE_NUMERO_DE_EJECUCION,
                                                                                                                                                                                              0), new Field(
                                                                                                                                                                                                            6,
                                                                                                                                                                                                            TIPO_OPERACION,
                                                                                                                                                                                                            PTIMessageFieldType.ASCII,
                                                                                                                                                                                                            SIZE_TIPO_OPERACION), new Field(
                                                                                                                                                                                                                                            7,
                                                                                                                                                                                                                                            INICADOR_COMPRA_VENTA,
                                                                                                                                                                                                                                            PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                            SIZE_INICADOR_COMPRA_VENTA), new Field(
                                                                                                                                                                                                                                                                                   8,
                                                                                                                                                                                                                                                                                   INDICADOR_CAPACIDAD_T_REPORTING,
                                                                                                                                                                                                                                                                                   PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                   SIZE_INDICADOR_CAPACIDAD_T_REPORTING), new Field(
                                                                                                                                                                                                                                                                                                                                    9,
                                                                                                                                                                                                                                                                                                                                    INDICADOR_EXENTA,
                                                                                                                                                                                                                                                                                                                                    PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                    SIZE_INDICADOR_EXENTA), new Field(
                                                                                                                                                                                                                                                                                                                                                                      10,
                                                                                                                                                                                                                                                                                                                                                                      CODIGO_ERROR,
                                                                                                                                                                                                                                                                                                                                                                      PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                      SIZE_CODIGO_ERROR),
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R00() throws PTIMessageException {
    super(PTIMessageRecordType.R00, FIELDS);
  }

  public String getMiembroMercado() {
    return this.getAsString(MIEMBRO_DEL_MERCADO);
  }

  public Date getFechaContratacion() {
    return this.getAsDate(FECHA_CONTRATACION);
  }

  public String getCodigoIsin() {
    return this.getAsString(CODIGO_ISIN);
  }

  public String getSegmentoMercado() {
    return this.getAsString(SEGMENTO_DE_MERCADO);
  }

  public String getNumeroEjecucion() {
    return this.getAsString(NUMERO_DE_EJECUCION);
  }

  public String getTipoOperacion() {
    return this.getAsString(TIPO_OPERACION);
  }

  public String getIndicadorCompraVenta() {
    return this.getAsString(INICADOR_COMPRA_VENTA);
  }

  public String getIndicadorCapacidadTReporting() {
    return this.getAsString(INDICADOR_CAPACIDAD_T_REPORTING);
  }

  public String getIndicadorExenta() {
    return this.getAsString(INDICADOR_EXENTA);
  }

  public String getCodigoError() {
    return this.getAsString(CODIGO_ERROR);
  }

  public void setMiembroMercado(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_DEL_MERCADO, valor);
  }

  public void setFechaContratacion(final Date valor) throws PTIMessageException {
    this.set(FECHA_CONTRATACION, valor);
  }

  public void setCodigoIsin(final String valor) throws PTIMessageException {
    this.set(CODIGO_ISIN, valor);
  }

  public void setSegmentoMercado(final String valor) throws PTIMessageException {
    this.set(SEGMENTO_DE_MERCADO, valor);
  }

  public void setNumeroEjecucion(final BigDecimal valor) throws PTIMessageException {
    this.set(NUMERO_DE_EJECUCION, valor);
  }

  public void setTipoOperacion(final String valor) throws PTIMessageException {
    this.set(TIPO_OPERACION, valor);
  }

  public void setIndicadorCompraVenta(final String valor) throws PTIMessageException {
    this.set(INICADOR_COMPRA_VENTA, valor);
  }

  public void setIndicadorCapacidadTReporting(final String valor) throws PTIMessageException {
    this.set(INDICADOR_CAPACIDAD_T_REPORTING, valor);
  }

  public void setIndicadorExenta(final String valor) throws PTIMessageException {
    this.set(INDICADOR_EXENTA, valor);
  }

  public void setCodigoError(final String valor) throws PTIMessageException {
    this.set(CODIGO_ERROR, valor);
  }

}
