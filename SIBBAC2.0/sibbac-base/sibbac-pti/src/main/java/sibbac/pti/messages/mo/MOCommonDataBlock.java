/**
 * 
 */
package sibbac.pti.messages.mo;

import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * 
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class MOCommonDataBlock extends ARecord implements PTIMessageRecord {

  // -------------------------------------------- La lista de campos.

  /**
   * Los campos del bloque de datos comunes.
   */
  public static final String FECHA_TEORICA_LIQUIDACION = "FECHA_TEORICA_LIQUIDACION";
  public static final String SENTIDO = "SENTIDO";
  public static final int SIZE_FECHA_TEORICA_LIQUIDACION = 8;
  public static final int SIZE_SENTIDO = 1;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, FECHA_TEORICA_LIQUIDACION, PTIMessageFieldType.ASCII,
                                                   SIZE_FECHA_TEORICA_LIQUIDACION), new Field(
                                                                                              2,
                                                                                              SENTIDO,
                                                                                              PTIMessageFieldType.ASCII,
                                                                                              SIZE_SENTIDO)
  };

  /**
   * Constructor por defecto.
   */
  public MOCommonDataBlock() throws PTIMessageException {
    super(PTIMessageRecordType.COMMON_DATA_BLOCK, FIELDS);
  }

  public Date getFechaLiquidacion() {
    return this.getAsDate(FECHA_TEORICA_LIQUIDACION);
  }

  public String getSentido() {
    return this.getAsString(SENTIDO);
  }

  public void setFechaLiquidacion(final String valor) throws PTIMessageException {
    this.set(FECHA_TEORICA_LIQUIDACION, valor);
  }

  public void setFechaLiquidacion(final Date valor) throws PTIMessageException {
    this.set(FECHA_TEORICA_LIQUIDACION, valor);
  }

  public void setSentido(final String valor) throws PTIMessageException {
    this.set(SENTIDO, valor);
  }

  public boolean esCompra() {
    return this.getSentido().equalsIgnoreCase(MO_CONSTANTS.COMMON_DATA.COMPRA);
  }

}
