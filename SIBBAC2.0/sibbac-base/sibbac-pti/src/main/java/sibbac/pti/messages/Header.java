package sibbac.pti.messages;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.pti.PTIConstants;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageHeader;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;

/**
 * Cabecera estandar de mensajeria propietaria.
 * <p/>
 * 
 * La informacion de detalle de la cabecera se encuentra en el PDF de BME:<br/>
 * "Interfaz Mensajes Propietario", Noviembre 2014; pagina(pdf): 3, punto: 2.3.
 *
 * <pre>
 * Identificacion API
 * 1 Tipo de Registro A 4
 * 2 Version A 2 Reservado para uso futuro. Debe ir a espacios.
 * 3 SubAplicacion A 2 Reservado para uso futuro. Debe ir a espacios.
 * 4 Codigo de Error General A 3 A espacios en mensajes de entrada. En mensajes de salida se informara: 000 = OK Distinto de 000 - ERROR
 * 
 * Identificacion Emisor Mensaje
 * 5 Origen A 4 Identificacion del Origen del mensaje. En mensajes emitidos por la ECC se informa con: BMCL
 * 6 Usuario Origen A 3 Usuario emisor del mensaje. En mensajes emitidos por la ECC se informa con el codigo de Segmento de la ECC (Ver Tabla 1 en el documento "Tablas de Codificacion")
 * 
 * Identificacion Receptor Mensaje
 * 7 Destino A 4 Identificacion del Destino del mensaje. En mensajes enviados a la ECC se informa con: BMCL
 * 8 Usuario Destino A 3 Usuario Destino del mensaje. En mensajes enviados a la ECC se informa con el codigo de Segmento de la ECC (Ver Tabla 1 en el documento "Tablas de Codificacion")
 * 
 * Identificacion Miembro
 * 9 Miembro X(4) / BIC X(11) A 11 Identificacion del Miembro para el que se emite o recibe la mensajeria. En los mensajes generados por la ECC con destino a todos sus miembros, se rellenara con el caracter "?"
 * 10 Usuario Miembro A 3 Usuario del Miembro para el que se emite o recibe la mensajeria. En los mensajes generados por la ECC con destino a todos sus miembros, se rellenara con el caracter "?"
 * 
 * Datos Adicionales Mensaje
 * 11 Fecha envio Mensaje A 8 AAAAMMDD
 * 12 Hora envio Mensaje A 9 HHMMSSMMM
 * 13 Reservado A 44
 * </pre>
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class Header extends ARecord implements PTIMessageHeader {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(Header.class);

  /**
   * Campos del registro
   */
  private static final Field[] FIELDS = {
      new Field(1, TIPO_DE_REGISTRO, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_REGISTRO),
      new Field(2, VERSION, PTIMessageFieldType.ASCII, SIZE_VERSION),
      new Field(3, SUB_APLICACION, PTIMessageFieldType.ASCII, SIZE_SUB_APLICACION),
      new Field(4, GENERAL_ERROR_CODE, PTIMessageFieldType.ASCII, SIZE_GENERAL_ERROR_CODE),
      new Field(5, ORIGEN, PTIMessageFieldType.ASCII, SIZE_ORIGEN),
      new Field(6, USUARIO_ORIGEN, PTIMessageFieldType.ASCII, SIZE_USUARIO_ORIGEN),
      new Field(7, DESTINO, PTIMessageFieldType.ASCII, SIZE_DESTINO),
      new Field(8, USUARIO_DESTINO, PTIMessageFieldType.ASCII, SIZE_USUARIO_DESTINO),
      new Field(9, MIEMBRO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO),
      new Field(10, USUARIO_MIEMBRO, PTIMessageFieldType.ASCII, SIZE_USUARIO_MIEMBRO),
      new Field(11, FECHA, PTIMessageFieldType.ASCII, SIZE_FECHA),
      new Field(12, HORA, PTIMessageFieldType.ASCII, SIZE_HORA),
      new Field(13, RESERVADO, PTIMessageFieldType.ASCII, SIZE_RESERVADO) };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public Header() throws PTIMessageException {
    super(PTIMessageRecordType.HEADER, FIELDS);
  }

  public Header(final String strHeader) throws PTIMessageException {
    this();
    this.parseRecord(strHeader);
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.pti.messages.ARecord#setup()
   */
  @Override
  public void setup() {
    // La definicion de los campos, en orden.
    this.addFieldDefinition(TIPO_DE_REGISTRO, PTIMessageFieldType.ASCII, 4);
    this.addFieldDefinition(VERSION, PTIMessageFieldType.ASCII, 2);
    this.addFieldDefinition(SUB_APLICACION, PTIMessageFieldType.ASCII, 2);
    this.addFieldDefinition(GENERAL_ERROR_CODE, PTIMessageFieldType.ASCII, 3);
    this.addFieldDefinition(ORIGEN, PTIMessageFieldType.ASCII, 4);
    this.addFieldDefinition(USUARIO_ORIGEN, PTIMessageFieldType.ASCII, 3);
    this.addFieldDefinition(DESTINO, PTIMessageFieldType.ASCII, 4);
    this.addFieldDefinition(USUARIO_DESTINO, PTIMessageFieldType.ASCII, 3);
    this.addFieldDefinition(MIEMBRO, PTIMessageFieldType.ASCII, 11);
    this.addFieldDefinition(USUARIO_MIEMBRO, PTIMessageFieldType.ASCII, 3);
    this.addFieldDefinition(FECHA, PTIMessageFieldType.ASCII, 8);
    this.addFieldDefinition(HORA, PTIMessageFieldType.ASCII, 9);
    this.addFieldDefinition(RESERVADO, PTIMessageFieldType.ASCII, 44);

    // Los valores por defecto que toquen.
    try {
      this.setVersion(HEADER_CONSTANTS.VERSION);
      this.setSubaplicacion(HEADER_CONSTANTS.SUBAPLICACION);
      this.setError(HEADER_CONSTANTS.ERROR);
      this.setEntidadOrigen(HEADER_CONSTANTS.ENTIDAD_ORIGEN);
      this.setUsuarioOrigen(HEADER_CONSTANTS.USUARIO_ORIGEN);
      this.setEntidadDestino(HEADER_CONSTANTS.ENTIDAD_DESTINO);
      this.setUsuarioDestino(HEADER_CONSTANTS.USUARIO_DESTINO);
      this.setMiembro(HEADER_CONSTANTS.MIEMBRO);
      this.setUsuarioMiembro(HEADER_CONSTANTS.USUARIO_MIEMBRO);
      this.setReservado(HEADER_CONSTANTS.RESERVADO);
    }
    catch (PTIMessageException e) {
      LOG.error("Error de mensajería PTI: ", e.getClass().getName(), e.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.pti.PTIMessageHeader#getErrorCode()
   */
  @Override
  public String getErrorCode() {
    return (String) this.fields.get(GENERAL_ERROR_CODE).getValueAsString();
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.pti.PTIMessageHeader#hasErrors()
   */
  @Override
  public boolean hasErrors() {
    return !this.getErrorCode().equalsIgnoreCase(PTIConstants.NO_ERROR);
  }

  /**
   * Getter for: "TIPO_DE_REGISTRO"
   * 
   * @return Un {@String}
   */
  public String getTipoDeRegistro() {
    return this.fields.get(TIPO_DE_REGISTRO).getValueAsString();
  }

  /**
   * Setter for: "TIPO_DE_REGISTRO"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setTipoDeRegistro(final String x) throws PTIMessageException {
    this.set(TIPO_DE_REGISTRO, x);
  }

  /**
   * Getter for: "VERSION"
   * 
   * @return Un {@String}
   */
  public String getVersion() {
    return this.fields.get(VERSION).getValueAsString();
  }

  /**
   * Setter for: "VERSION"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setVersion(final String x) throws PTIMessageException {
    this.set(VERSION, x);
  }

  /**
   * Getter for: "SUB_APLICACION"
   * 
   * @return Un {@String}
   */
  public String getSubaplicacion() {
    return this.fields.get(SUB_APLICACION).getValueAsString();
  }

  /**
   * Setter for: "SUB_APLICACION"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setSubaplicacion(final String x) throws PTIMessageException {
    this.set(SUB_APLICACION, x);
  }

  /**
   * Getter for: "GENERAL_ERROR_CODE"
   * 
   * @return Un {@String}
   */
  public String getError() {
    return this.fields.get(GENERAL_ERROR_CODE).getValueAsString();
  }

  /**
   * Setter for: "GENERAL_ERROR_CODE"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setError(final String x) throws PTIMessageException {
    this.set(GENERAL_ERROR_CODE, x);
  }

  /**
   * Getter for: "ORIGEN"
   * 
   * @return Un {@String}
   */
  public String getEntidadOrigen() {
    return this.fields.get(ORIGEN).getValueAsString();
  }

  /**
   * Setter for: "ORIGEN"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setEntidadOrigen(final String x) throws PTIMessageException {
    this.set(ORIGEN, x);
  }

  /**
   * Getter for: "USUARIO_ORIGEN"
   * 
   * @return Un {@String}
   */
  public String getUsuarioOrigen() {
    return this.fields.get(USUARIO_ORIGEN).getValueAsString();
  }

  /**
   * Setter for: "USUARIO_ORIGEN"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setUsuarioOrigen(final String x) throws PTIMessageException {
    this.set(USUARIO_ORIGEN, x);
  }

  /**
   * Getter for: "DESTINO"
   * 
   * @return Un {@String}
   */
  @Override
  public String getEntidadDestino() {
    return this.fields.get(DESTINO).getValueAsString();
  }

  /**
   * Setter for: "DESTINO"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  @Override
  public void setEntidadDestino(final String x) throws PTIMessageException {
    this.set(DESTINO, x);
  }

  /**
   * Getter for: "USUARIO_DESTINO"
   * 
   * @return Un {@String}
   */
  @Override
  public String getUsuarioDestino() {
    return this.fields.get(USUARIO_DESTINO).getValueAsString();
  }

  /**
   * Setter for: "USUARIO_DESTINO"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  @Override
  public void setUsuarioDestino(final String x) throws PTIMessageException {
    this.set(USUARIO_DESTINO, x);
  }

  /**
   * Getter for: "MIEMBRO"
   * 
   * @return Un {@String}
   */
  @Override
  public String getMiembro() {
    return this.fields.get(MIEMBRO).getValueAsString();
  }

  /**
   * Setter for: "MIEMBRO"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  @Override
  public void setMiembro(final String x) throws PTIMessageException {
    this.set(MIEMBRO, x);
  }

  /**
   * Getter for: "USUARIO_MIEMBRO"
   * 
   * @return Un {@String}
   */
  public String getUsuarioMiembro() {
    return this.fields.get(USUARIO_MIEMBRO).getValueAsString();
  }

  /**
   * Setter for: "USUARIO_MIEMBRO"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setUsuarioMiembro(final String x) throws PTIMessageException {
    this.set(USUARIO_MIEMBRO, x);
  }

  /**
   * Getter for: "FECHA"
   * 
   * @return Un {@String}
   * @throws PTIMessageException
   */
  public Date getFechaDeEnvio() throws PTIMessageException {
    return this.fields.get(FECHA).getValueAsDate();
  }

  /**
   * Setter for: "FECHA"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setFechaDeEnvio(final Date x) throws PTIMessageException {
    this.set(FECHA, x);
  }

  /**
   * Getter for: "HORA"
   * 
   * @return Un {@String}
   * @throws PTIMessageException
   */
  public Date getHoraDeEnvio() throws PTIMessageException {
    return this.fields.get(HORA).getValueAsTime();
  }

  /**
   * Setter for: "HORA"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setHoraDeEnvio(final Date x) throws PTIMessageException {
    this.set(HORA, x);
  }

  /**
   * Getter for: "RESERVADO"
   * 
   * @return Un {@String}
   */
  public String getReservado() {
    return this.fields.get(RESERVADO).getValueAsString();
  }

  /**
   * Setter for: "RESERVADO"
   * 
   * @param x Un {@link String}
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public void setReservado(final String x) throws PTIMessageException {
    this.set(RESERVADO, x);
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.pti.PTIMessageHeader#getFecha()
   */
  @Override
  public Date getFecha() throws PTIMessageException {
    Date fecha = this.getFechaDeEnvio();
    Date hora = this.getHoraDeEnvio();
    if (fecha != null && hora != null) {
      DateFormat dfFecha = new SimpleDateFormat(DATE_FORMAT);
      DateFormat dfHora = new SimpleDateFormat(TIME_FORMAT);
      DateFormat df = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
      try {
        return df.parse(dfFecha.format(fecha) + " " + dfHora.format(hora));
      }
      catch (ParseException e) {
        throw new PTIMessageException("Errores parseando la fecha/hora: " + e.getMessage());
      }
    }
    else {
      if (this.fromPTI) {
        throw new PTIMessageException("No hay fecha de mensaje.");
      }
      else {
        return null;
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.pti.PTIMessageHeader#getTipoDeMensaje()
   */
  @Override
  public PTIMessageType getTipoDeMensaje() {
    return PTIMessageType.parse(this.getTipoDeRegistro().trim());
  }

}
