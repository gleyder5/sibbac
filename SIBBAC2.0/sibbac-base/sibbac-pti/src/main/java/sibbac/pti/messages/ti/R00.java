package sibbac.pti.messages.ti;


import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Criterio de comunicacion
	public static final String	FECHA_EJECUCION							= "FECHA_EJECUCION";
	public static final String	ENTIDAD_COMUNICADORA_REF_TITULAR		= "ENTIDAD_COMUNICADORA_REF_TITULAR";
	public static final String	REFERENCIA_TITULAR						= "REFERENCIA_TITULAR";
	public static final String	REFERENCIA_ADICIONAL					= "REFERENCIA_ADICIONAL";
	public static final String	INDICADOR_RECTIFICACION					= "INDICADOR_RECTIFICACION";

	// Tamanyos de los campos.
	public static final int		SIZE_FECHA_EJECUCION					= 8;
	public static final int		SIZE_ENTIDAD_COMUNICADORA_REF_TITULAR	= 11;
	public static final int		SIZE_REFERENCIA_TITULAR					= 20;
	public static final int		SIZE_REFERENCIA_ADICIONAL				= 20;
	public static final int		SIZE_INDICADOR_RECTIFICACION			= 1;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, FECHA_EJECUCION, PTIMessageFieldType.ASCII, SIZE_FECHA_EJECUCION ),
			new Field( 2, ENTIDAD_COMUNICADORA_REF_TITULAR, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_COMUNICADORA_REF_TITULAR ),
			new Field( 3, REFERENCIA_TITULAR, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_TITULAR ),
			new Field( 4, REFERENCIA_ADICIONAL, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_ADICIONAL ),
			new Field( 5, INDICADOR_RECTIFICACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_RECTIFICACION )
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public Date getFechaEjecucion() {
		return this.getAsDate( FECHA_EJECUCION );
	}

	public String getEntidadComunicadoraRefTitular() {
		return this.getAsString( ENTIDAD_COMUNICADORA_REF_TITULAR );
	}

	public String getReferenciaTitular() {
		return this.getAsString( REFERENCIA_TITULAR );
	}

	public String getReferenciaAdicional() {
		return this.getAsString( REFERENCIA_ADICIONAL );
	}

	public String getIndicadorRectificacion() {
		return this.getAsString( INDICADOR_RECTIFICACION );
	}

	public void setFechaEjecucion( final String valor ) throws PTIMessageException {
		this.set( FECHA_EJECUCION, valor );
	}

	public void setEntidadComunicadoraRefTitular( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_COMUNICADORA_REF_TITULAR, valor );
	}

	public void setReferenciaTitular( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_TITULAR, valor );
	}

	public void setReferenciaAdicional( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_ADICIONAL, valor );
	}

	public void setIndicadorRectificacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_RECTIFICACION, valor );
	}


}
