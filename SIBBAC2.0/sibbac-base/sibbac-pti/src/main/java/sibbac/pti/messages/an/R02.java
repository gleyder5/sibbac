package sibbac.pti.messages.an;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R02 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos ejecucion
	public static final String	IDENTIFICACION_DE_LA_PLATAFORMA			= "IDENTIFICACION_DE_LA_PLATAFORMA";
	public static final String	SEGMENTO								= "SEGMENTO";
	public static final String	FECHA_NEGOCACION						= "FECHA_NEGOCACION";
	public static final String	HORA_NEGOCIACION						= "HORA_NEGOCIACION";
	public static final String	NUMERO_DE_EJECUCION_DE_MERCADO			= "NUMERO_DE_EJECUCION_DE_MERCADO";
	public static final String	CODIGO_DE_OPERACION_MERCADO				= "CODIGO_DE_OPERACION_MERCADO";
	public static final String	MIEMBRO_MERCADO							= "MIEMBRO_MERCADO";
	public static final String	INDICADOR_COTIZACION					= "INDICADOR_COTIZACION";
	public static final String	CODIGO_DE_VALOR							= "CODIGO_DE_VALOR";
	public static final String	SENTIDO									= "SENTIDO";
	public static final String	VALORES_IMPORTE_NOMINAL					= "VALORES_IMPORTE_NOMINAL";
	public static final String	PRECIO									= "PRECIO";
	public static final String	IMPORTE_EFECTIVO						= "IMPORTE_EFECTIVO";

	// Datos de la orden
	public static final String	FECHA_DE_LA_ORDEN_DE_MERCADO			= "FECHA_DE_LA_ORDEN_DE_MERCADO";
	public static final String	HORA_DE_LA_ORDEN_DE_MERCADO				= "HORA_DE_LA_ORDEN_DE_MERCADO";
	public static final String	NUMERO_DE_LA_ORDEN_DE_MERCADO			= "NUMERO_DE_LA_ORDEN_DE_MERCADO";
	public static final String	USUARIO									= "USUARIO";
	public static final String	REFERENCIA_DE_CLIENTE					= "REFERENCIA_DE_CLIENTE";
	public static final String	REFERENCIA_EXTERNA						= "REFERENCIA_EXTERNA_DE_LA_ORDEN";
	public static final String	INDICADOR_CAPACIDAD						= "INDICADOR_CAPACIDAD";
	public static final String	INFORMACION_ADICIONAL_DE_LA_ORDEN		= "INFORMACION_ADICIONAL_DE_LA_ORDEN";

	// Tamanyos de los campos.
	public static final int		SIZE_IDENTIFICACION_DE_LA_PLATAFORMA	= 4;
	public static final int		SIZE_SEGMENTO							= 2;
	public static final int		SIZE_FECHA_NEGOCACION					= 8;
	public static final int		SIZE_HORA_NEGOCIACION					= 9;
	public static final int		SIZE_NUMERO_DE_EJECUCION_DE_MERCADO		= 9;
	public static final int		SIZE_CODIGO_DE_OPERACION_MERCADO		= 2;
	public static final int		SIZE_MIEMBRO_MERCADO					= 4;
	public static final int		SIZE_INDICADOR_COTIZACION				= 1;
	public static final int		SIZE_CODIGO_DE_VALOR					= 12;
	public static final int		SIZE_SENTIDO							= 1;
	public static final int		SIZE_VALORES_IMPORTE_NOMINAL			= 18;
	public static final int		SIZE_PRECIO								= 13;
	public static final int		SIZE_IMPORTE_EFECTIVO					= 15;
	public static final int		SIZE_FECHA_DE_LA_ORDEN_DE_MERCADO		= 8;
	public static final int		SIZE_HORA_DE_LA_ORDEN_DE_MERCADO		= 9;
	public static final int		SIZE_NUMERO_DE_LA_ORDEN_DE_MERCADO		= 9;
	public static final int		SIZE_USUARIO							= 3;
	public static final int		SIZE_REFERENCIA_DE_CLIENTE				= 16;
	public static final int		SIZE_REFERENCIA_EXTERNA					= 15;
	public static final int		SIZE_INDICADOR_CAPACIDAD				= 1;
	public static final int		SIZE_INFORMACION_ADICIONAL_DE_LA_ORDEN	= 80;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, IDENTIFICACION_DE_LA_PLATAFORMA, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION_DE_LA_PLATAFORMA ),
			new Field( 2, SEGMENTO, PTIMessageFieldType.ASCII, SIZE_SEGMENTO ),
			new Field( 3, FECHA_NEGOCACION, PTIMessageFieldType.ASCII, SIZE_FECHA_NEGOCACION ),
			new Field( 4, HORA_NEGOCIACION, PTIMessageFieldType.ASCII, SIZE_HORA_NEGOCIACION ),
			new Field( 5, NUMERO_DE_EJECUCION_DE_MERCADO, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_EJECUCION_DE_MERCADO, 0 ),
			new Field( 6, CODIGO_DE_OPERACION_MERCADO, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_OPERACION_MERCADO ),
			new Field( 7, MIEMBRO_MERCADO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_MERCADO ),
			new Field( 8, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 9, CODIGO_DE_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_VALOR ),
			new Field( 10, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 11, VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 12, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6 ),
			new Field( 13, IMPORTE_EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_IMPORTE_EFECTIVO, 2 ),
			new Field( 14, FECHA_DE_LA_ORDEN_DE_MERCADO, PTIMessageFieldType.ASCII, SIZE_FECHA_DE_LA_ORDEN_DE_MERCADO ),
			new Field( 15, HORA_DE_LA_ORDEN_DE_MERCADO, PTIMessageFieldType.ASCII, SIZE_HORA_DE_LA_ORDEN_DE_MERCADO ),
			new Field( 16, NUMERO_DE_LA_ORDEN_DE_MERCADO, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_LA_ORDEN_DE_MERCADO, 0 ),
			new Field( 17, USUARIO, PTIMessageFieldType.ASCII, SIZE_USUARIO ),
			new Field( 18, REFERENCIA_DE_CLIENTE, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_DE_CLIENTE ),
			new Field( 19, REFERENCIA_EXTERNA, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_EXTERNA ),
			new Field( 20, INDICADOR_CAPACIDAD, PTIMessageFieldType.ASCII, SIZE_INDICADOR_CAPACIDAD ),
			new Field( 21, INFORMACION_ADICIONAL_DE_LA_ORDEN, PTIMessageFieldType.ASCII, SIZE_INFORMACION_ADICIONAL_DE_LA_ORDEN )
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02() throws PTIMessageException {
		super( PTIMessageRecordType.R02, FIELDS );
	}

	public String getIdentificacionDeLaPlataforma() {
		return this.getAsString( IDENTIFICACION_DE_LA_PLATAFORMA );
	}

	public String getSegmento() {
		return this.getAsString( SEGMENTO );
	}

	public Date getFechaNegociacion() {
		return this.getAsDate( FECHA_NEGOCACION );
	}

	public Date getHoraNegociacion() {
		return this.getAsTime( HORA_NEGOCIACION );
	}

	public String getNumeroDeEjecucionDeMercado() {
		return this.getAsString( NUMERO_DE_EJECUCION_DE_MERCADO );
	}

	public String getCodigoDeOperacionMercado() {
		return this.getAsString( CODIGO_DE_OPERACION_MERCADO );
	}

	public String getMiembroMercado() {
		return this.getAsString( MIEMBRO_MERCADO );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public String getCodigoDeValor() {
		return this.getAsString( CODIGO_DE_VALOR );
	}

	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public BigDecimal getValoresImporteNominal() {
		return this.getAsNumber( VALORES_IMPORTE_NOMINAL );
	}

	public BigDecimal getPrecio() {
		return this.getAsNumber( PRECIO );
	}

	public BigDecimal getImporteEfectivo() {
		return this.getAsNumber( IMPORTE_EFECTIVO );
	}

	public Date getFechaDeLaOrdenDeMercado() {
		return this.getAsDate( FECHA_DE_LA_ORDEN_DE_MERCADO );
	}

	public Date getHoraDeLaOrdenDeMercado() {
		return this.getAsTime( HORA_DE_LA_ORDEN_DE_MERCADO );
	}

	public String getNumeroDeLaOrdenDeMercado() {
		return this.getAsString( NUMERO_DE_LA_ORDEN_DE_MERCADO );
	}

	public String getUsuario() {
		return this.getAsString( USUARIO );
	}

	public String getReferenciaDeCliente() {
		return this.getAsString( REFERENCIA_DE_CLIENTE );
	}

	public String getReferenciaExterna() {
		return this.getAsString( REFERENCIA_EXTERNA );
	}

	public String getIndicadorCapacidad() {
		return this.getAsString( INDICADOR_CAPACIDAD );
	}

	public String getInformacionAdicionalDeLaOrden() {
		return this.getAsString( INFORMACION_ADICIONAL_DE_LA_ORDEN );
	}

	public void setIdentificacionDeLaPlataforma( final String valor ) throws PTIMessageException {
		this.set( IDENTIFICACION_DE_LA_PLATAFORMA, valor );
	}

	public void setSegmento( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO, valor );
	}

	public void setFechaNegociacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_NEGOCACION, valor );
	}

	public void setHoraNegociacion( final String valor ) throws PTIMessageException {
		this.set( HORA_NEGOCIACION, valor );
	}

	public void setNumeroDeEjecucionDeMercado( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_EJECUCION_DE_MERCADO, valor );
	}

	public void setCodigoDeOperacionMercado( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_OPERACION_MERCADO, valor );
	}

	public void setMiembroMercado( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_MERCADO, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setCodigoDeValor( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_VALOR, valor );
	}

	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}

	public void setValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setPrecio( final String valor ) throws PTIMessageException {
		this.set( PRECIO, valor );
	}

	public void setImporteEfectivo( final String valor ) throws PTIMessageException {
		this.set( IMPORTE_EFECTIVO, valor );
	}

	public void setFechaDeLaOrdenDeMercado( final String valor ) throws PTIMessageException {
		this.set( FECHA_DE_LA_ORDEN_DE_MERCADO, valor );
	}

	public void setHoraDeLaOrdenDeMercado( final String valor ) throws PTIMessageException {
		this.set( HORA_DE_LA_ORDEN_DE_MERCADO, valor );
	}

	public void setNumeroDeLaOrdenDeMercado( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_LA_ORDEN_DE_MERCADO, valor );
	}

	public void setUsuario( final String valor ) throws PTIMessageException {
		this.set( USUARIO, valor );
	}

	public void setReferenciaDeCliente( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_DE_CLIENTE, valor );
	}

	public void setReferenciaExterna( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_EXTERNA, valor );
	}

	public void setIndicadorCapacidad( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_CAPACIDAD, valor );
	}

	public void setInformacionAdicionalDeLaOrden( final String valor ) throws PTIMessageException {
		this.set( INFORMACION_ADICIONAL_DE_LA_ORDEN, valor );
	}

	

}
