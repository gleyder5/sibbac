package sibbac.pti.messages.ga02;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;

/**
 * Mensaje de tipo GA02.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA02 extends AMessage {

  /**
   * Constructor por defecto.
   */
  public GA02() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public GA02(final boolean fromPTI) throws PTIMessageException {
    super(PTIMessageType.GA02, fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) {
    // NO hay definidos registros de ningun tipo para este mensaje.
    return null;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.messages.AMessage#setup()
   */
  @Override
  public void messageDefinition() throws PTIMessageException {

    // El bloque de datos comunes.
    this.commonDataBlock = new GA02CommonDataBlock();
    // El bloque de control
    this.controlBlock = null;
  }

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub

  }

}
