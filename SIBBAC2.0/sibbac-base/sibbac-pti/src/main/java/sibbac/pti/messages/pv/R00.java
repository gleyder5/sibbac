package sibbac.pti.messages.pv;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: PV. (Informacion de precios).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Identificacion del valor
	public static final String	CODIGO_DE_VALOR				= "CODIGO_DE_VALOR";
	public static final String	CODIGO_CONTRATACION			= "CODIGO_CONTRATACION";

	// Tamanyos de los campos.
	public static final int		SIZE_CODIGO_DE_VALOR		= 12;
	public static final int		SIZE_CODIGO_CONTRATACION	= 5;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS						= {
			new Field( 1, CODIGO_DE_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_VALOR ),
			new Field( 2, CODIGO_CONTRATACION, PTIMessageFieldType.ASCII, SIZE_CODIGO_CONTRATACION )
															};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getCodigoDeValor() {
		return this.getAsString( CODIGO_DE_VALOR );
	}

	public String getCodigoContratacion() {
		return this.getAsString( CODIGO_CONTRATACION );
	}

	public void setCodigoDeValor( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_VALOR, valor );
	}

	public void setCodigoContratacion( final String valor ) throws PTIMessageException {
		this.set( CODIGO_CONTRATACION, valor );
	}

}
