package sibbac.pti.messages.op;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: MO. (Datos Cuenta Origen).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Identificacion retencion/liberacion
	public static final String	REFERENCIA_MOVIMIENTO			= "REFERENCIA_MOVIMIENTO";
	public static final String	ACTUACION						= "ACTUACION";
	public static final String	FECHA_TEORICA_LIQUIDACION		= "FECHA_TEORICA_LIQUIDACION";
	public static final String	SENTIDO							= "SENTIDO";

	// Tamanyos de los campos.
	public static final int		SIZE_REFERENCIA_MOVIMIENTO		= 10;
	public static final int		SIZE_ACTUACION					= 2;
	public static final int		SIZE_FECHA_TEORICA_LIQUIDACION	= 8;
	public static final int		SIZE_SENTIDO					= 1;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS	= {
					new Field( 1, REFERENCIA_MOVIMIENTO, PTIMessageFieldType.ASCII,	SIZE_REFERENCIA_MOVIMIENTO ),
					new Field( 2, ACTUACION, PTIMessageFieldType.ASCII,	SIZE_ACTUACION ),
					new Field( 3, FECHA_TEORICA_LIQUIDACION, PTIMessageFieldType.ASCII,	SIZE_FECHA_TEORICA_LIQUIDACION ),
					new Field( 4, SENTIDO, PTIMessageFieldType.ASCII,	SIZE_SENTIDO )
																};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getReferenciaDeMovimiento() {
		return this.getAsString( REFERENCIA_MOVIMIENTO );
	}
	
	public String getActuacion() {
		return this.getAsString( ACTUACION );
	}
	
	public String getFechaTeoricaLiquidacion() {
		return this.getAsString( FECHA_TEORICA_LIQUIDACION );
	}
	
	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public void setReferenciaDeMovimiento( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO, valor );
	}
	
	public void setActuacion( final String valor ) throws PTIMessageException {
		this.set( ACTUACION, valor );
	}
	
	public void setFechaTeoricaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_TEORICA_LIQUIDACION, valor );
	}
	
	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}


}
