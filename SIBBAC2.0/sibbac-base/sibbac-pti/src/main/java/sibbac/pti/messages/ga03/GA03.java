package sibbac.pti.messages.ga03;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;

/**
 * Mensaje de tipo GA03.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA03 extends AMessage {

  /**
   * Constructor por defecto.
   */
  public GA03() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public GA03(final boolean fromPTI) throws PTIMessageException {
    super(PTIMessageType.GA03, fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException {
    switch (tipo) {
      case R00:
        return new R00();
      case R01:
        return new R01();
      default:
        return null;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.messages.AMessage#setup()
   */
  @Override
  public void messageDefinition() throws PTIMessageException {
    // El bloque de datos comunes.
    this.commonDataBlock = new GA03CommonDataBlock();

    // El bloque de datos de control.
    this.controlBlock = null;
  }

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub

  }

}
