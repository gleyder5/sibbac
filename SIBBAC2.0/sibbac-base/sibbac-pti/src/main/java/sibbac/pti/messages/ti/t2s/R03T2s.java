package sibbac.pti.messages.ti.t2s;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.ti.R03;


/**
 * Registro R03 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03T2s extends R03 {

  // -------------------------------------------- La lista de campos.
  // Modo Titular/Operacion
  public static final String NUM_VALORES_IMPORTE_NOMINAL = "NUM_VALORES_IMPORTE_NOMINAL";
  public static final String NUM_VALORES_IMPORTE_NOMINAL_FALLIDO = "NUM_VALORES_IMPORTE_NOMINAL_FALLIDO";

  // Tamanyos de los campos.
  public static final int SIZE_NUM_VALORES_IMPORTE_NOMINAL = 25;
  public static final int SIZE_NUM_VALORES_IMPORTE_NOMINAL_FALLIDO = 25;

  // Tamanyos de los campos.
  public static final int SCALE_NUM_VALORES_IMPORTE_NOMINAL = 9;
  public static final int SCALE_NUM_VALORES_IMPORTE_NOMINAL_FALLIDO = 9;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS										= {
			new Field( 1, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 2, SEGMENTO_DE_LA_ECC, PTIMessageFieldType.ASCII, SIZE_SEGMENTO_DE_LA_ECC ),
			new Field( 3, NUMERO_OPERACION, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION ),
			new Field( 4, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 5, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, SCALE_NUM_VALORES_IMPORTE_NOMINAL ),
			new Field( 6, IMPORTE_EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_IMPORTE_EFECTIVO, 2 ),
			new Field( 7, NUM_VALORES_IMPORTE_NOMINAL_FALLIDO, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL_FALLIDO, SCALE_NUM_VALORES_IMPORTE_NOMINAL_FALLIDO ),
			new Field( 8, CANON_TEORICO, PTIMessageFieldType.NUMBER, SIZE_CANON_TEORICO, 2 ),
			new Field( 9, CANON_APLICADO, PTIMessageFieldType.NUMBER, SIZE_CANON_APLICADO, 2 )
																			};

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R03T2s() throws PTIMessageException {
    super(FIELDS);
  }


}
