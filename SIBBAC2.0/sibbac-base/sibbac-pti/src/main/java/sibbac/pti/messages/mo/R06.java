package sibbac.pti.messages.mo;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R06 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	REFERENCIA_MOVIMIENTO				= "REFERENCIA_MOVIMIENTO";
	public static final String	REFERENCIA_MOVIMIENTO_ECC			= "REFERENCIA_MOVIMIENTO_ECC";
	public static final String	REF_NOTIFICACION					= "REF_NOTIFICACION";
	public static final String	MIEMBRO								= "MIEMBRO";
	public static final String	CUENTA_COMPENSACION					= "CUENTA_COMPENSACION";
	public static final String	MIEMBRO_COMPENSADOR					= "MIEMBRO_COMPENSADOR";
	public static final String	ENTIDAD_PARTICIPANTE				= "ENTIDAD_PARTICIPANTE";
	public static final String	ENTIDAD_PARTICIPANTE_BIC			= "ENTIDAD_PARTICIPANTE_BIC";
	public static final String	CUENTA_LIQUIDACION					= "CUENTA_LIQUIDACION";
	public static final String	TIPO_MOVIMIENTO						= "TIPO_MOVIMIENTO";
	public static final String	ESTADO								= "ESTADO";
	public static final String	INDICADOR_COTIZACION				= "INDICADOR_COTIZACION";
	public static final String	NUMERO_VALORES_IMPORTE_NOMINAL		= "NUMERO_VALROES_IMPORTE_NOMINAL";
	public static final String	PRECIO								= "PRECIO";
	public static final String	EFECTIVO							= "EFECTIVO";
	public static final String	NUMERO_OPERACION					= "NUMERO_OPERACION";
	public static final String	NUMERO_VALORES_NOMINAL				= "NUMERO_VALORES_NOMINAL";
	public static final String	PRECIO_OPERACION					= "PRECIO_OPERACION";

	public static final int		SIZE_REFERENCIA_MOVIMIENTO			= 30;
	public static final int		SIZE_REFERENCIA_MOVIMIENTO_ECC		= 20;
	public static final int		SIZE_REF_NOTIFICACION				= 20;
	public static final int		SIZE_MIEMBRO						= 4;
	public static final int		SIZE_CUENTA_COMPENSACION			= 3;
	public static final int		SIZE_MIEMBRO_COMPENSADOR			= 4;
	public static final int		SIZE_ENTIDAD_PARTICIPANTE			= 4;
	public static final int		SIZE_ENTIDAD_PARTICIPANTE_BIC		= 11;
	public static final int		SIZE_CUENTA_LIQUIDACION				= 35;
	public static final int		SIZE_TIPO_MOVIMIENTO				= 2;
	public static final int		SIZE_ESTADO							= 2;
	public static final int		SIZE_INDICADOR_COTIZACION			= 1;
	public static final int		SIZE_NUMERO_VALORES_IMPORTE_NOMINAL	= 18;
	public static final int		SIZE_PRECIO							= 13;
	public static final int		SIZE_EFECTIVO						= 15;
	public static final int		SIZE_NUMERO_OPERACION				= 16;
	public static final int		SIZE_NUMERO_VALORES_NOMINAL			= 18;
	public static final int		SIZE_PRECIO_OPERACION				= 13;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, REFERENCIA_MOVIMIENTO, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO ),
			new Field( 2, REFERENCIA_MOVIMIENTO_ECC, PTIMessageFieldType.NUMBER, SIZE_REFERENCIA_MOVIMIENTO_ECC ),
			new Field( 3, REF_NOTIFICACION, PTIMessageFieldType.NUMBER, SIZE_REF_NOTIFICACION ),
			new Field( 4, MIEMBRO, PTIMessageFieldType.NUMBER, SIZE_MIEMBRO ),
			new Field( 5, CUENTA_COMPENSACION, PTIMessageFieldType.NUMBER, SIZE_CUENTA_COMPENSACION ),
			new Field( 6, MIEMBRO_COMPENSADOR, PTIMessageFieldType.NUMBER, SIZE_MIEMBRO_COMPENSADOR ),
			new Field( 7, ENTIDAD_PARTICIPANTE, PTIMessageFieldType.NUMBER, SIZE_ENTIDAD_PARTICIPANTE ),
			new Field( 8, ENTIDAD_PARTICIPANTE_BIC, PTIMessageFieldType.NUMBER, SIZE_ENTIDAD_PARTICIPANTE_BIC ),
			new Field( 9, CUENTA_LIQUIDACION, PTIMessageFieldType.NUMBER, SIZE_CUENTA_LIQUIDACION ),
			new Field( 10, TIPO_MOVIMIENTO, PTIMessageFieldType.NUMBER, SIZE_TIPO_MOVIMIENTO ),
			new Field( 11, ESTADO, PTIMessageFieldType.NUMBER, SIZE_ESTADO ),
			new Field( 12, INDICADOR_COTIZACION, PTIMessageFieldType.NUMBER, SIZE_INDICADOR_COTIZACION ),
			new Field( 13, NUMERO_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUMERO_VALORES_IMPORTE_NOMINAL ),
			new Field( 14, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO ),
			new Field( 15, EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_EFECTIVO ),
			new Field( 16, NUMERO_OPERACION, PTIMessageFieldType.NUMBER, SIZE_NUMERO_OPERACION ),
			new Field( 17, NUMERO_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUMERO_VALORES_NOMINAL ),
			new Field( 18, PRECIO_OPERACION, PTIMessageFieldType.NUMBER, SIZE_PRECIO_OPERACION )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R06() throws PTIMessageException {
		super( PTIMessageRecordType.R06, FIELDS );
	}

	public String getReferenciaMovimiento() {
		return this.getAsString( REFERENCIA_MOVIMIENTO );
	}

	public String getReferenciaMovimientoEcc() {
		return this.getAsString( REFERENCIA_MOVIMIENTO_ECC );
	}

	public String getRefNotificacion() {
		return this.getAsString( REF_NOTIFICACION );
	}

	public String getMiembro() {
		return this.getAsString( MIEMBRO );
	}

	public String getCuentaCompensacion() {
		return this.getAsString( CUENTA_COMPENSACION );
	}

	public String getMiembroCompensador() {
		return this.getAsString( MIEMBRO_COMPENSADOR );
	}

	public String getEntidadParticipante() {
		return this.getAsString( ENTIDAD_PARTICIPANTE );
	}

	public String getEntidadParticipanteBic() {
		return this.getAsString( ENTIDAD_PARTICIPANTE_BIC );
	}

	public String getCuentaLiquidacion() {
		return this.getAsString( CUENTA_LIQUIDACION );
	}

	public String getTipoMovimiento() {
		return this.getAsString( TIPO_MOVIMIENTO );
	}

	public String getEstado() {
		return this.getAsString( ESTADO );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public String getNumeroValoresImporteNominal() {
		return this.getAsString( NUMERO_VALORES_IMPORTE_NOMINAL );
	}

	public String getPrecio() {
		return this.getAsString( PRECIO );
	}

	public String getEfectivo() {
		return this.getAsString( EFECTIVO );
	}

	public String getNumeroOperacion() {
		return this.getAsString( NUMERO_OPERACION );
	}

	public String getNumeroValoresNominal() {
		return this.getAsString( NUMERO_VALORES_NOMINAL );
	}

	public String getPrecioOperacion() {
		return this.getAsString( PRECIO_OPERACION );
	}

	public void setReferenciaMovimiento( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO, valor );
	}

	public void setReferenciaMovimientoEcc( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO_ECC, valor );
	}

	public void setRefNotificacion( final String valor ) throws PTIMessageException {
		this.set( REF_NOTIFICACION, valor );
	}

	public void setMiembro( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO, valor );
	}

	public void setCuentaCompensacion( final String valor ) throws PTIMessageException {
		this.set( CUENTA_COMPENSACION, valor );
	}

	public void setMiembroCompensador( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_COMPENSADOR, valor );
	}

	public void setEntidadParticipante( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_PARTICIPANTE, valor );
	}

	public void setEntidadParticipanteBic( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_PARTICIPANTE_BIC, valor );
	}

	public void setCuentaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( CUENTA_LIQUIDACION, valor );
	}

	public void setTipoMovimiento( final String valor ) throws PTIMessageException {
		this.set( TIPO_MOVIMIENTO, valor );
	}

	public void setEstado( final String valor ) throws PTIMessageException {
		this.set( ESTADO, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setNumeroValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUMERO_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setPrecio( final String valor ) throws PTIMessageException {
		this.set( PRECIO, valor );
	}

	public void setEfectivo( final String valor ) throws PTIMessageException {
		this.set( EFECTIVO, valor );
	}

	public void setNumeroOperacion( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION, valor );
	}

	public void setNumeroValoresNominal( final String valor ) throws PTIMessageException {
		this.set( NUMERO_VALORES_NOMINAL, valor );
	}

	public void setPrecioOperacion( final String valor ) throws PTIMessageException {
		this.set( PRECIO_OPERACION, valor );
	}


}
