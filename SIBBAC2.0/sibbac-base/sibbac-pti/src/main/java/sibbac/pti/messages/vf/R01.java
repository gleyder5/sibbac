package sibbac.pti.messages.vf;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	ID_ECC									= "ID_ECC";
	public static final String	SEGMENTO								= "SEGMENTO";
	public static final String	NUMERO_OPERACION_ECC					= "NUMERO_OPERACION_ECC";
	public static final String	SENTIDO									= "SENTIDO";
	public static final String	ENTIDAD_COMUNICADORA_REF_TITULAR		= "ENTIDAD_COMUNICADORA_REF_TITULAR";
	public static final String	REFERENCIA_TITULAR						= "REFERENCIA_TITULAR";
	public static final String	INDICADOR_COTIZACION					= "INDICADOR_COTIZACION";
	public static final String	NUMERO_VALORES_IMPORTE_NOMINAL			= "NUMERO_VALORES_IMPORTE_NOMINAL";
	public static final String	REFERENCIA_ECC_IL						= "REFERENCIA_ECC_IL";
	public static final String	MIEMBRO_ECC								= "MIEMBRO_ECC";
	public static final String	NUMERO_CUENTA_ECC						= "NUMERO_CUENTA_ECC";

	// Datos a Devolver
	public static final String	CODIGO_ERROR							= "CODIGO_ERROR";

	public static final int		SIZE_ID_ECC								= 11;
	public static final int		SIZE_SEGMENTO							= 2;
	public static final int		SIZE_NUMERO_OPERACION_ECC				= 35;
	public static final int		SIZE_SENTIDO							= 1;
	public static final int		SIZE_ENTIDAD_COMUNICADORA_REF_TITULAR	= 11;
	public static final int		SIZE_REFERENCIA_TITULAR					= 20;
	public static final int		SIZE_INDICADOR_COTIZACION				= 1;
	public static final int		SIZE_NUM_VALORES_IMPORTE_NOMINAL		= 18;
	public static final int		SIZE_REFERENCIA_ECC_IL					= 35;
	public static final int		SIZE_MIEMBRO_ECC						= 11;
	public static final int		SIZE_NUMERO_CUENTA_ECC					= 35;
	public static final int		SIZE_CODIGO_ERROR						= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 2, SEGMENTO, PTIMessageFieldType.ASCII, SIZE_SEGMENTO ),
			new Field( 3, NUMERO_OPERACION_ECC, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION_ECC ),
			new Field( 4, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 5, ENTIDAD_COMUNICADORA_REF_TITULAR, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_COMUNICADORA_REF_TITULAR ),
			new Field( 6, REFERENCIA_TITULAR, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_TITULAR ),
			new Field( 7, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 8, NUMERO_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 9, REFERENCIA_ECC_IL, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_ECC_IL ),
			new Field( 10, MIEMBRO_ECC, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_ECC ),
			new Field( 11, NUMERO_CUENTA_ECC, PTIMessageFieldType.ASCII, SIZE_NUMERO_CUENTA_ECC ),
			new Field( 12, CODIGO_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_ERROR )

																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}

	public String getIdEcc() {
		return this.getAsString( ID_ECC );
	}

	public String getSegmento() {
		return this.getAsString( SEGMENTO );
	}

	public String getNumeroOperacionEcc() {
		return this.getAsString( NUMERO_OPERACION_ECC );
	}

	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public String getEntidadComunicadoraRefTitular() {
		return this.getAsString( ENTIDAD_COMUNICADORA_REF_TITULAR );
	}

	public String getReferenciaTitular() {
		return this.getAsString( REFERENCIA_TITULAR );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public BigDecimal getNumeroValoresImporteNominal() {
		return this.getAsNumber( NUMERO_VALORES_IMPORTE_NOMINAL );
	}

	public String getReferenciaEccIl() {
		return this.getAsString( REFERENCIA_ECC_IL );
	}

	public String getMiembroEcc() {
		return this.getAsString( MIEMBRO_ECC );
	}

	public String getNumeroCuentaEcc() {
		return this.getAsString( NUMERO_CUENTA_ECC );
	}

	public String getCodigoError() {
		return this.getAsString( CODIGO_ERROR );
	}

	public void setIdEcc( final String valor ) throws PTIMessageException {
		this.set( ID_ECC, valor );
	}

	public void setSegmento( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO, valor );
	}

	public void setNumeroOperacionEcc( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION_ECC, valor );
	}

	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}

	public void setEntidadComunicadoraRefTitular( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_COMUNICADORA_REF_TITULAR, valor );
	}

	public void setReferenciaTitular( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_TITULAR, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setNumeroValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUMERO_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setReferenciaEccIl( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_ECC_IL, valor );
	}

	public void setMiembroEcc( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_ECC, valor );
	}

	public void setNumeroCuentaEcc( final String valor ) throws PTIMessageException {
		this.set( NUMERO_CUENTA_ECC, valor );
	}

	public void setCodigoError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_ERROR, valor );
	}


}
