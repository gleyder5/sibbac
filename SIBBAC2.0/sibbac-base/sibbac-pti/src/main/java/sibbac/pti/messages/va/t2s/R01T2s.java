package sibbac.pti.messages.va.t2s;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.va.R01;

/**
 * Registro R00 para el mensaje de tipo: VA. (Informacion de valores).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01T2s extends R01 {

  // -------------------------------------------- La lista de campos.

  // 26
  public static final String PERIODICIDAD_CUPON_RF = "PERIODICIDAD_CUPON_RF";
  // 27
  public static final String METODO_CALCULO_CUPON_CORRIDO_RF = "METODO_CALCULO_CUPON_CORRIDO_RF";

  // 26
  public static final int SIZE_PERIODICIDAD_CUPON_RF = 2;
  // 27
  public static final int SIZE_METODO_CALCULO_CUPON_CORRIDO_RF = 1;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, TIPO_INFORMACION_ADICIONAL, PTIMessageFieldType.ASCII,
                                                   SIZE_TIPO_INFORMACION_ADICIONAL), 
                                                   new Field(
                                                                                               2,
                                                                                               PRECIO,
                                                                                               PTIMessageFieldType.NUMBER,
                                                                                               SIZE_PRECIO, 6), new Field(
                                                                                                                          4,
                                                                                                                          NUMERO_DE_DECIMALES,
                                                                                                                          PTIMessageFieldType.NUMBER,
                                                                                                                          SIZE_NUMERO_DE_DECIMALES,
                                                                                                                          0), new Field(
                                                                                                                                        5,
                                                                                                                                        COD_DCV,
                                                                                                                                        PTIMessageFieldType.ASCII,
                                                                                                                                        SIZE_COD_DCV), new Field(
                                                                                                                                                                 6,
                                                                                                                                                                 FECHA_ULTIMO_DIA_NEGOCIACION,
                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                 SIZE_FECHA_ULTIMO_DIA_NEGOCIACION), new Field(
                                                                                                                                                                                                               7,
                                                                                                                                                                                                               PERIODICIDAD_CUPON_RF,
                                                                                                                                                                                                               PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                               SIZE_PERIODICIDAD_CUPON_RF,
                                                                                                                                                                                                               0), new Field(
                                                                                                                                                                                                                             8,
                                                                                                                                                                                                                             METODO_CALCULO_CUPON_CORRIDO_RF,
                                                                                                                                                                                                                             PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                             SIZE_METODO_CALCULO_CUPON_CORRIDO_RF),
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R01T2s() throws PTIMessageException {
    super(FIELDS);
  }

  public BigDecimal getPeriodicidadCuponRf() {
    return this.getAsNumber(PERIODICIDAD_CUPON_RF);
  }

  public String getMetodoCalculoCuponCorridoRf() {
    return this.getAsString(METODO_CALCULO_CUPON_CORRIDO_RF);
  }

  public void setPeriodicidadCuponRf(BigDecimal value) throws PTIMessageException {
    this.set(PERIODICIDAD_CUPON_RF, value);
  }

  public void setMetodoCalculoCuponCorridoRf(String value) throws PTIMessageException {
    this.set(METODO_CALCULO_CUPON_CORRIDO_RF, value);
  }

}
