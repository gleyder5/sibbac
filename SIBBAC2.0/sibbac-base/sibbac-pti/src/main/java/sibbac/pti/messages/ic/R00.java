package sibbac.pti.messages.ic;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R00 para el mensaje de tipo: PV. (Informacion de precios).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Identificacion del valor
  public static final String ENTIDAD_PARTICIPANTE = "ENTIDAD_PARTICIPANTE";
  public static final String CUENTA_LIQUIDACION = "CUENTA_LIQUIDACION";
  public static final String FECHA_CONTRATACION = "FECHA_CONTRATACION";
  public static final String FECHA_LIQUIDACION = "FECHA_LIQUIDACION";
  public static final String CARGO_ABONO = "CARGO_ABONO";
  public static final String ID_CORRETAJE = "ID_CORRETAJE";
  public static final String MIEMBRO_MERCADO = "MIEMBRO_MERCADO";
  public static final String IDENTIFICATIVO_FICHERO_ECC = "IDENTIFICATIVO_FICHERO_ECC";
  public static final String IMPORTE = "IMPORTE";

  // Tamanyos de los campos.
  public static final int SIZE_ENTIDAD_PARTICIPANTE = 11;
  public static final int SIZE_CUENTA_LIQUIDACION = 35;
  public static final int SIZE_FECHA_CONTRATACION = 8;
  public static final int SIZE_FECHA_LIQUIDACION = 8;
  public static final int size_CARGO_ABONO = 1;
  public static final int SIZE_ID_CORRETAJE = 2;
  public static final int SIZE_MIEMBRO_MERCADO = 4;
  public static final int SIZE_IDENTIFICATIVO_FICHERO_ECC = 20;
  public static final int SIZE_IMPORTE = 15;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, ENTIDAD_PARTICIPANTE, PTIMessageFieldType.ASCII,
                                                   SIZE_ENTIDAD_PARTICIPANTE), new Field(2, CUENTA_LIQUIDACION,
                                                                                         PTIMessageFieldType.ASCII,
                                                                                         SIZE_CUENTA_LIQUIDACION), new Field(
                                                                                                                             3,
                                                                                                                             FECHA_CONTRATACION,
                                                                                                                             PTIMessageFieldType.ASCII,
                                                                                                                             SIZE_FECHA_CONTRATACION), new Field(
                                                                                                                                                                 4,
                                                                                                                                                                 FECHA_LIQUIDACION,
                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                 SIZE_FECHA_LIQUIDACION), new Field(
                                                                                                                                                                                                    5,
                                                                                                                                                                                                    CARGO_ABONO,
                                                                                                                                                                                                    PTIMessageFieldType.ASCII,
                                                                                                                                                                                                    size_CARGO_ABONO), new Field(
                                                                                                                                                                                                                                 6,
                                                                                                                                                                                                                                 ID_CORRETAJE,
                                                                                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                 SIZE_ID_CORRETAJE), new Field(
                                                                                                                                                                                                                                                               7,
                                                                                                                                                                                                                                                               MIEMBRO_MERCADO,
                                                                                                                                                                                                                                                               PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                               SIZE_MIEMBRO_MERCADO), new Field(
                                                                                                                                                                                                                                                                                                8,
                                                                                                                                                                                                                                                                                                IDENTIFICATIVO_FICHERO_ECC,
                                                                                                                                                                                                                                                                                                PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                SIZE_IDENTIFICATIVO_FICHERO_ECC), new Field(
                                                                                                                                                                                                                                                                                                                                            9,
                                                                                                                                                                                                                                                                                                                                            IMPORTE,
                                                                                                                                                                                                                                                                                                                                            PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                                                                            SIZE_IMPORTE,
                                                                                                                                                                                                                                                                                                                                            2)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R00() throws PTIMessageException {
    super(PTIMessageRecordType.R00, FIELDS);
  }

  public String getEntidadParticipante() {
    return this.getAsString(ENTIDAD_PARTICIPANTE);
  }

  public String getCuentaLiquidacion() {
    return this.getAsString(CUENTA_LIQUIDACION);
  }

  public String getFechaContratacion() {
    return this.getAsString(FECHA_CONTRATACION);
  }

  public String getFechaLiquidacion() {
    return this.getAsString(FECHA_LIQUIDACION);
  }

  public String getCargoAbono() {
    return this.getAsString(CARGO_ABONO);
  }

  public String getIdCorretaje() {
    return this.getAsString(ID_CORRETAJE);
  }

  public String getMiembroMercado() {
    return this.getAsString(MIEMBRO_MERCADO);
  }

  public String getIdentificativoFicheroCc() {
    return this.getAsString(IDENTIFICATIVO_FICHERO_ECC);
  }

  public BigDecimal getImporte() {
    return this.getAsNumber(IMPORTE);
  }

  public void setEntidadParticipante(final String valor) throws PTIMessageException {
    this.set(ENTIDAD_PARTICIPANTE, valor);
  }

  public void setCuentaLiquidacion(final String valor) throws PTIMessageException {
    this.set(CUENTA_LIQUIDACION, valor);
  }

  public void setFechaContratacion(final String valor) throws PTIMessageException {
    this.set(FECHA_CONTRATACION, valor);
  }

  public void setFechaLiquidacion(final String valor) throws PTIMessageException {
    this.set(FECHA_LIQUIDACION, valor);
  }

  public void setCargoAbono(final String valor) throws PTIMessageException {
    this.set(CARGO_ABONO, valor);
  }

  public void setIdCorretaje(final String valor) throws PTIMessageException {
    this.set(ID_CORRETAJE, valor);
  }

  public void setMiembroMercado(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_MERCADO, valor);
  }

  public void setIdentificativoFicheroCc(final String valor) throws PTIMessageException {
    this.set(IDENTIFICATIVO_FICHERO_ECC, valor);
  }

  public void setImporte(final String valor) throws PTIMessageException {
    this.set(IMPORTE, valor);
  }

}
