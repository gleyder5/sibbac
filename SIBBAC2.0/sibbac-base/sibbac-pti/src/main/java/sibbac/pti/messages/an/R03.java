package sibbac.pti.messages.an;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R03 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Datos de liquidacion
  public static final String CODIGO_DCV = "CODIGO_DCV";
  public static final String CICLO_LIQUIDACION = "CICLO_LIQUIDACION";
  public static final String NUMERO_OPERACION_DCV = "NUMERO_OPERACION_DCV";
  public static final String TIPO_INSTRUCCION_LIQUIDACION = "TIPO_INSTRUCCION_LIQUIDACION";
  public static final String REFERENCIA_EVENTO_CORPORATIVO = "REFERENCIA_EVENTO_CORPORATIVO";

  // Tamanyos de los campos.
  public static final int SIZE_CODIGO_DCV = 11;
  public static final int SIZE_CICLO_LIQUIDACION = 4;
  public static final int SIZE_NUMERO_OPERACION_DCV = 35;
  public static final int SIZE_TIPO_INSTRUCCION_LIQUIDACION = 4;
  public static final int SIZE_REFERENCIA_EVENTO_CORPORATIVO = 35;
  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, CODIGO_DCV, PTIMessageFieldType.ASCII, SIZE_CODIGO_DCV),
      new Field(2, CICLO_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_CICLO_LIQUIDACION),
      new Field(3, NUMERO_OPERACION_DCV, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION_DCV),
      new Field(4, TIPO_INSTRUCCION_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_TIPO_INSTRUCCION_LIQUIDACION),
      new Field(5, REFERENCIA_EVENTO_CORPORATIVO, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_EVENTO_CORPORATIVO) };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public R03() throws PTIMessageException {
    super(PTIMessageRecordType.R03, FIELDS);
  }

  public String getCodigoDCV() {
    return this.getAsString(CODIGO_DCV);
  }

  public String getCicloLiquidacion() {
    return this.getAsString(CICLO_LIQUIDACION);
  }

  public String getNumeroOperacionDCV() {
    return this.getAsString(NUMERO_OPERACION_DCV);
  }

  public String getTipoInstruccionLiquidacion() {
    return this.getAsString(TIPO_INSTRUCCION_LIQUIDACION);
  }

  public String getReferenciaEventoCorporativo() {
    return this.getAsString(REFERENCIA_EVENTO_CORPORATIVO);
  }

  public void setCodigoDCV(final String valor) throws PTIMessageException {
    this.set(CODIGO_DCV, valor);
  }

  public void setCicloLiquidacion(final String valor) throws PTIMessageException {
    this.set(CICLO_LIQUIDACION, valor);
  }

  public void setNumeroOperacionDCV(final String valor) throws PTIMessageException {
    this.set(NUMERO_OPERACION_DCV, valor);
  }

  public void setTipoInstruccionLiquidacion(final String valor) throws PTIMessageException {
    this.set(TIPO_INSTRUCCION_LIQUIDACION, valor);
  }

  public void setReferenciaEventoCorporativo(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_EVENTO_CORPORATIVO, valor);
  }

}
