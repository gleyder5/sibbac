package sibbac.pti.messages.rf;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;


/**
 * Mensaje de tipo RF.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class RF extends AMessage {

	/**
	 * Constructor por defecto.
	 */
	public RF() throws PTIMessageException {
		this( false );
	}

	/**
	 * Constructor a partir de PTI.
	 * 
	 * @param fromPTI
	 *            TRUE o FALSE, dependiendo de si el registro se genera a partir
	 *            de datos de PTI.
	 */
	public RF( final boolean fromPTI ) throws PTIMessageException {
		super( PTIMessageType.RF, fromPTI );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
	 */
	@Override
	public PTIMessageRecord getRecordInstance( final PTIMessageRecordType tipo ) throws PTIMessageException {
		switch ( tipo ) {
			case R00:
				return new R00();
			case R01:
				return new R01();
			case R02:
				return new R02();
			case R03:
				return new R03();
			default:
				return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.messages.AMessage#setup()
	 */
	@Override
	public void messageDefinition() throws PTIMessageException {
		// El bloque de control.
		this.controlBlock = new RFControlBlock();

		// El bloque de datos comunes.
		this.commonDataBlock = null;
	}

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub
    
  }


}
