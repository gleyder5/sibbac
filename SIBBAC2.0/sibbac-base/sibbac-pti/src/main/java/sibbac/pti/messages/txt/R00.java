package sibbac.pti.messages.txt;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: TXT.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.
	public static final String	TEXTO_INFOMATIVO		= "TEXTO_INFOMATIVO";
	public static final int		SIZE_TEXTO_INFORMATIVO	= 78;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS					= {
															new Field( 1, TEXTO_INFOMATIVO, PTIMessageFieldType.ASCII,
																	SIZE_TEXTO_INFORMATIVO )
														};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getTextoInformativo() {
		return this.getAsString( TEXTO_INFOMATIVO );
	}

	public void setTextoInformativo( final String valor ) throws PTIMessageException {
		this.set( TEXTO_INFOMATIVO, valor );
	}

}
