package sibbac.pti.messages.an;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R07 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R07 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos de liquidacion
	public static final String	NUMERO_OPERACION_DCV				= "NUMERO_OPERACION_DCV";
	public static final String	NUMERO_OPERACION_IL_DE_LA_ECC		= "NUMERO_OPERACION_IL_DE_LA_ECC";
	public static final String	TIPO_DE_OPERACION_DCV				= "TIPO_DE_OPERACION_DCV";
	public static final String	ESTADO_IL							= "ESTADO_IL";
	public static final String	TITULOS_OP_EN_IL					= "TITULOS_OP_EN_IL";
	public static final String	EFECTIVO_OP_EN_IL					= "EFECTIVO_OP_EN_IL";
	public static final String	TITULOS_FALLIDOS_OP_EN_IL			= "TITULOS_FALLIDOS_OP_EN_IL";

	// Tamanyos de los campos.
	public static final int		SIZE_NUMERO_OPERACION_DCV			= 35;
	public static final int		SIZE_NUMERO_OPERACION_IL_DE_LA_ECC	= 35;
	public static final int		SIZE_TIPO_DE_OPERACION_DCV			= 4;
	public static final int		SIZE_ESTADO_IL						= 4;
	public static final int		SIZE_TITULOS_OP_EN_IL				= 18;
	public static final int		SIZE_EFECTIVO_OP_EN_IL				= 15;
	public static final int		SIZE_TITULOS_FALLIDOS_OP_EN_IL		= 18;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, NUMERO_OPERACION_DCV, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION_DCV ),
			new Field( 2, NUMERO_OPERACION_IL_DE_LA_ECC, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION_IL_DE_LA_ECC ),
			new Field( 3, TIPO_DE_OPERACION_DCV, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_OPERACION_DCV ),
			new Field( 4, ESTADO_IL, PTIMessageFieldType.ASCII, SIZE_ESTADO_IL ),
			new Field( 5, TITULOS_OP_EN_IL, PTIMessageFieldType.NUMBER, SIZE_TITULOS_OP_EN_IL, 6 ),
			new Field( 6, EFECTIVO_OP_EN_IL, PTIMessageFieldType.NUMBER, SIZE_EFECTIVO_OP_EN_IL, 2 ),
			new Field( 7, TITULOS_FALLIDOS_OP_EN_IL, PTIMessageFieldType.NUMBER, SIZE_TITULOS_FALLIDOS_OP_EN_IL, 6 )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R07() throws PTIMessageException {
		super( PTIMessageRecordType.R07, FIELDS );
	}

	// Getters
	public String getNumeroOperacionDCV() {
		return this.getAsString( NUMERO_OPERACION_DCV );
	}

	public String getNumeroOperacionILdeLaECC() {
		return this.getAsString( NUMERO_OPERACION_IL_DE_LA_ECC );
	}

	public String getTipoOperacionDCV() {
		return this.getAsString( TIPO_DE_OPERACION_DCV );
	}

	public String getEstadoIL() {
		return this.getAsString( ESTADO_IL );
	}

	public BigDecimal getTitulosOPenIL() {
		return this.getAsNumber( TITULOS_OP_EN_IL );
	}

	public BigDecimal getEfectivoOPenIL() {
		return this.getAsNumber( EFECTIVO_OP_EN_IL );
	}

	public BigDecimal getTitulosFallidosOPenIL() {
		return this.getAsNumber( TITULOS_FALLIDOS_OP_EN_IL );
	}

	// Setters
	public void setNumeroOperacionDCV( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION_DCV, valor );
	}

	public void setNumeroOperacionILdeLaECC( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION_IL_DE_LA_ECC, valor );
	}

	public void setTipoOperacionDCV( final String valor ) throws PTIMessageException {
		this.set( TIPO_DE_OPERACION_DCV, valor );
	}

	public void setEstadoIL( final String valor ) throws PTIMessageException {
		this.set( ESTADO_IL, valor );
	}

	public void setTitulosOPenIL( final String valor ) throws PTIMessageException {
		this.set( TITULOS_OP_EN_IL, valor );
	}

	public void setEfectivoOPenIL( final String valor ) throws PTIMessageException {
		this.set( EFECTIVO_OP_EN_IL, valor );
	}

	public void setTitulosFallidosOPenIL( final String valor ) throws PTIMessageException {
		this.set( TITULOS_FALLIDOS_OP_EN_IL, valor );
	}

	

}
