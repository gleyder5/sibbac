package sibbac.pti.messages.mo;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	NUMERO_OPERACION		= "NUMERO_OPERACION";
	public static final String	CORRETAJE				= "CORRETAJE";

	public static final int		SIZE_NUMERO_OPERACION	= 16;
	public static final int		SIZE_CORRETAJE			= 16;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS					= {
			new Field( 1, NUMERO_OPERACION, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION ),
			new Field( 2, CORRETAJE, PTIMessageFieldType.SIGNED, SIZE_CORRETAJE, 13, 2 ),
														};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}

	public String getNumeroDeOperacion() {
		return this.getAsString( NUMERO_OPERACION );
	}

	public BigDecimal getCorretaje() {
		return this.getAsNumber( CORRETAJE );
	}

	public void setNumeroDeOperacion( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION, valor );
	}

	public void setCorretaje( final BigDecimal valor ) throws PTIMessageException {
		this.set( CORRETAJE, valor );
	}


}
