package sibbac.pti.messages.rt;


import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: RT. (Asignacion Referencias
 * Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Criterio de comunicacion
	public static final String	IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION			= "IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION";
	public static final String	SEGMENTO												= "SEGMENTO";
	public static final String	ID_ECC													= "ID_ECC";
	public static final String	SEGMENTO_DE_LA_ECC										= "SEGMENTO_DE_LA_ECC";
	public static final String	ENTIDAD_COMUNICADORA_REF_TITULAR						= "ENTIDAD_COMUNICADORA_REF_TITULAR";
	public static final String	IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD		= "IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD";
	public static final String	FECHA_NEGOCIACION										= "FECHA_NEGOCIACION";
	public static final String	CRITERIO_COMUNICACION_TITULARIDAD						= "CRITERIO_COMUNICACION_TITULARIDAD";
	public static final String	SENTIDO													= "SENTIDO";
	public static final String	INDICADOR_RECTIFICACION_TITULARIAD						= "INDICADOR_RECTIFICACION_TITULARIAD";

	// Tamanyos de los campos.
	public static final int		SIZE_IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION		= 4;
	public static final int		SIZE_SEGMENTO											= 2;
	public static final int		SIZE_ID_ECC												= 11;
	public static final int		SIZE_SEGMENTO_DE_LA_ECC									= 2;
	public static final int		SIZE_ENTIDAD_COMUNICADORA_REF_TITULAR					= 11;
	public static final int		SIZE_IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD	= 1;
	public static final int		SIZE_FECHA_NEGOCIACION									= 8;
	public static final int		SIZE_CRITERIO_COMUNICACION_TITULARIDAD					= 35;
	public static final int		SIZE_SENTIDO											= 1;
	public static final int		SIZE_INDICADOR_RECTIFICACION_TITULARIAD					= 1;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS													= {
			new Field( 1, IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION ),
			new Field( 2, SEGMENTO, PTIMessageFieldType.ASCII, SIZE_SEGMENTO ),
			new Field( 3, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 4, SEGMENTO_DE_LA_ECC, PTIMessageFieldType.ASCII, SIZE_SEGMENTO_DE_LA_ECC ),
			new Field( 5, ENTIDAD_COMUNICADORA_REF_TITULAR, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_COMUNICADORA_REF_TITULAR ),
			new Field( 6, IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD ),
			new Field( 7, FECHA_NEGOCIACION, PTIMessageFieldType.ASCII, SIZE_FECHA_NEGOCIACION ),
			new Field( 8, CRITERIO_COMUNICACION_TITULARIDAD, PTIMessageFieldType.ASCII, SIZE_CRITERIO_COMUNICACION_TITULARIDAD ),
			new Field( 9, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 10, INDICADOR_RECTIFICACION_TITULARIAD, PTIMessageFieldType.ASCII, SIZE_INDICADOR_RECTIFICACION_TITULARIAD )
	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getIdentificacionDeLaPlataformaDeNegociacion() {
		return this.getAsString( IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION );
	}

	public String getSegmento() {
		return this.getAsString( SEGMENTO );
	}

	public String getIdECC() {
		return this.getAsString( ID_ECC );
	}

	public String getSegmentoDeLaECC() {
		return this.getAsString( SEGMENTO_DE_LA_ECC );
	}

	public String getEntidadComunicadoraRefTitular() {
		return this.getAsString( ENTIDAD_COMUNICADORA_REF_TITULAR );
	}

	public String getIdentificacionCriterioComunicacionTitularidad() {
		return this.getAsString( IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD );
	}

	public Date getFechaNegociacion() {
		return this.getAsDate( FECHA_NEGOCIACION );
	}

	public String getCriterioComunicacionTitularidad() {
		return this.getAsString( CRITERIO_COMUNICACION_TITULARIDAD );
	}

	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public String getIndicadorRectificacionTitularidad() {
		return this.getAsString( INDICADOR_RECTIFICACION_TITULARIAD );
	}

	public void setIdentificacionDeLaPlataformaDeNegociacion( final String valor ) throws PTIMessageException {
		this.set( IDENTIFICACION_DE_LA_PLATAFORMA_DE_NEGOCIACION, valor );
	}

	public void setSegmento( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO, valor );
	}

	public void setIdECC( final String valor ) throws PTIMessageException {
		this.set( ID_ECC, valor );
	}

	public void setSegmentoDeLaECC( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO_DE_LA_ECC, valor );
	}

	public void setEntidadComunicadoraRefTitular( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_COMUNICADORA_REF_TITULAR, valor );
	}

	public void setIdentificacionCriterioComunicacionTitularidad( final String valor ) throws PTIMessageException {
		this.set( IDENTIFICACION_CRITERIO_COMUNICACION_TITULARIDAD, valor );
	}

	public void setFechaNegociacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_NEGOCIACION, valor );
	}

	public void setCriterioComunicacionTitularidad( final String valor ) throws PTIMessageException {
		this.set( CRITERIO_COMUNICACION_TITULARIDAD, valor );
	}

	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}

	public void setIndicadorRectificacionTitularidad( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_RECTIFICACION_TITULARIAD, valor );
	}


}
