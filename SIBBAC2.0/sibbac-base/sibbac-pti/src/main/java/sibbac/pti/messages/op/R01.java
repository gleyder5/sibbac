package sibbac.pti.messages.op;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R03 para el mensaje de tipo: GA01 (Garantias y Movimientos de
 * Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos Operacion
	public static final String	NUMERO_OPERACION					= "NUMERO_OPERACION";
	public static final String	NUMERO_VALORES_IMPORTE_NOMINAL		= "NUMERO_VALORES_IMPORTE_NOMINAL";

	// Tamanyos de los campos.
	public static final int		SIZE_NUMERO_OPERACION				= 16;
	public static final int		SIZE_NUMERO_VALORES_IMPORTE_NOMINAL	= 18;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, NUMERO_OPERACION, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION ),
			new Field( 2, NUMERO_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUMERO_VALORES_IMPORTE_NOMINAL, 0 )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}

	public String getNumeroOperacion() {
		return this.getAsString( NUMERO_OPERACION );
	}

	public BigDecimal getNumeroValoresImporteNominal() {
		return this.getAsNumber( NUMERO_VALORES_IMPORTE_NOMINAL );
	}

	public void setNumeroOperacion( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION, valor );
	}

	public void setNumeroValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUMERO_VALORES_IMPORTE_NOMINAL, valor );
	}

}
