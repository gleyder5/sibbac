package sibbac.pti.messages.ti;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Datos de domicilio titularidad
	public static final String	DOMICILIO				= "DOMICILIO";
	public static final String	POBLACION				= "POBLACION";
	public static final String	CODIGO_POSTAL			= "CODIGO_POSTAL";
	public static final String	PAIS_DE_RESIDENCIA		= "PAIS_DE_RESIDENCIA";

	// Tamanyos de los campos.
	public static final int		SIZE_DOMICILIO			= 40;
	public static final int		SIZE_POBLACION			= 40;
	public static final int		SIZE_CODIGO_POSTAL		= 5;
	public static final int		SIZE_PAIS_DE_RESIDENCIA	= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS					= {
			new Field( 1, DOMICILIO, PTIMessageFieldType.ASCII, SIZE_DOMICILIO ),
			new Field( 2, POBLACION, PTIMessageFieldType.ASCII, SIZE_POBLACION ),
			new Field( 3, CODIGO_POSTAL, PTIMessageFieldType.ASCII, SIZE_CODIGO_POSTAL ),
			new Field( 4, PAIS_DE_RESIDENCIA, PTIMessageFieldType.ASCII, SIZE_PAIS_DE_RESIDENCIA )
														};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}

	public String getDomicilio() {
		return this.getAsString( DOMICILIO );
	}

	public String getPoblacion() {
		return this.getAsString( POBLACION );
	}

	public String getCodigoPostal() {
		return this.getAsString( CODIGO_POSTAL );
	}

	public String getPaisDeResidencia() {
		return this.getAsString( PAIS_DE_RESIDENCIA );
	}

	public void setDomicilio( final String valor ) throws PTIMessageException {
		this.set( DOMICILIO, valor );
	}

	public void setPoblacion( final String valor ) throws PTIMessageException {
		this.set( POBLACION, valor );
	}

	public void setCodigoPostal( final String valor ) throws PTIMessageException {
		this.set( CODIGO_POSTAL, valor );
	}

	public void setPaisDeResidencia( final String valor ) throws PTIMessageException {
		this.set( PAIS_DE_RESIDENCIA, valor );
	}


}
