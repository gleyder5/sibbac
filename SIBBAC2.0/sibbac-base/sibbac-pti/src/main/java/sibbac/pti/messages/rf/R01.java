package sibbac.pti.messages.rf;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: RF. (Gestion de Referencias y Filtros
 * de Asignacion Externa /Gestion Modulo Parametrizacion).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Datos referencias asignacion
	public static final String	TIPO_REFERENCIA_FILTRO				= "TIPO_REFERENCIA_FILTRO";
	public static final String	REFERENCIA_INTERNA_ASIGNACION		= "REFERENCIA_INTERNA_ASIGNACION";
	public static final String	REFERENCIA_ASIGNACION				= "REFERENCIA_ASIGNACION";
	public static final String	MNEMOTECNICO						= "MNEMOTECNICO";
	public static final String	MIEMBRO								= "MIEMBRO";
	public static final String	CUENTA_DE_COMPENSACION				= "CUENTA_DE_COMPENSACION";

	public static final int		SIZE_TIPO_REFERENCIA_FILTRO			= 3;
	public static final int		SIZE_REFERENCIA_INTERNA_ASIGNACION	= 18;
	public static final int		SIZE_REFERENCIA_ASIGNACION			= 18;
	public static final int		SIZE_MNEMOTECNICO					= 10;
	public static final int		SIZE_MIEMBRO						= 4;
	public static final int		SIZE_CUENTA_DE_COMPENSACION			= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, TIPO_REFERENCIA_FILTRO, PTIMessageFieldType.ASCII, SIZE_TIPO_REFERENCIA_FILTRO ),
			new Field( 2, REFERENCIA_INTERNA_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_INTERNA_ASIGNACION ),
			new Field( 3, REFERENCIA_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_ASIGNACION ),
			new Field( 4, MNEMOTECNICO, PTIMessageFieldType.ASCII, SIZE_MNEMOTECNICO ),
			new Field( 5, MIEMBRO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO ),
			new Field( 5, CUENTA_DE_COMPENSACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_DE_COMPENSACION )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}

	public String getTipoReferenciaFiltro() {
		return this.getAsString( TIPO_REFERENCIA_FILTRO );
	}

	public String getReferenciaInternaAsignacion() {
		return this.getAsString( REFERENCIA_INTERNA_ASIGNACION );
	}

	public String getReferenciaAsignacion() {
		return this.getAsString( REFERENCIA_ASIGNACION );
	}

	public String getMnemotecnico() {
		return this.getAsString( MNEMOTECNICO );
	}

	public String getMiembro() {
		return this.getAsString( MIEMBRO );
	}

	public String getCuentaDeCompensacion() {
		return this.getAsString( CUENTA_DE_COMPENSACION );
	}

	public void setTipoReferenciaFiltro( final String valor ) throws PTIMessageException {
		this.set( TIPO_REFERENCIA_FILTRO, valor );
	}

	public void setReferenciaInternaAsignacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_INTERNA_ASIGNACION, valor );
	}

	public void setReferenciaAsignacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_ASIGNACION, valor );
	}

	public void setMnemotecnico( final String valor ) throws PTIMessageException {
		this.set( MNEMOTECNICO, valor );
	}

	public void setMiembro( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO, valor );
	}

	public void setCuentaDeCompensacion( final String valor ) throws PTIMessageException {
		this.set( CUENTA_DE_COMPENSACION, valor );
	}


}
