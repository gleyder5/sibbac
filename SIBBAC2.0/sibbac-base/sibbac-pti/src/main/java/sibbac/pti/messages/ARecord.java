package sibbac.pti.messages;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

import sibbac.common.SIBBACCommons;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageField;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;


/**
 * Clase abstracta que representa un Registro de un Mensaje PTI.
 * <p/>
 * Esta clase contiene las funcionalidades comunes a cada tipo de registro de un mensaje PTI.<br/>
 * Los diversos registros posibles, dependiendo incluso de la tipologia de los mensajes PTI, estan en subpaquetes de este paquete, y todos
 * extienden esta clase.
 * 
 * @param <T>
 *            El interfaz que representa el tipo de elemento con el que se
 *            identifica este registro.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public abstract class ARecord implements PTIMessageRecord {

	/**
	 * La longitud del registro.
	 */
	private int									longitud;

	/**
	 * Si el mensaje ha sido recibido desde PTI o no.
	 */
	protected boolean							fromPTI;

	/**
	 * El tipo de registro.
	 */
	protected PTIMessageRecordType				tipo;

	/**
	 * La lista de nombres de los campos de este tipo de registro.
	 */
	protected List< String >					fieldNames;

	/**
	 * La lista de campos de este registro.
	 */
	protected Map< String, PTIMessageField >	fields;

	/**
	 * Un array de {@link Field}'s del registro.
	 */
	protected Field[]							recordFields;

	/**
	 * Constructor general protegido.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun error con el setup.
	 */
	@SuppressWarnings( "unused" )
	private ARecord() throws PTIMessageException {
		this( PTIMessageRecordType.UNKNOWN, null );
	}

	/**
	 * Constructor general a partir de una lista de campos.
	 * 
	 * @param tipo
	 *            Una instancia de {@link PTIMessageRecordType} que representa
	 *            el tipo de registro.
	 * @param recordFields
	 *            Un array de {@link Field}'s del registro.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun error con el setup.
	 */
	public ARecord( final PTIMessageRecordType tipo, final Field[] recordFieldsArg ) throws PTIMessageException {
		recordFields = Arrays.copyOf(recordFieldsArg, recordFieldsArg.length);
		int total = 0;
		if ( recordFields != null && recordFields.length > 0 ) {
			for ( Field field : recordFields ) {
				total += field.getLongitud();
			}
		}
		this.longitud = total;
		this.fromPTI = false;
		this.tipo = tipo;
		this.fields = new Hashtable< String, PTIMessageField >();
		this.fieldNames = new ArrayList< String >();

		this.setup();

		// Vemos que efectivamente estan todos los campos que deben estar...
		if ( this.fieldNames.size() != this.fields.size() ) {
			throw new PTIMessageException( "Error con los campos: [fieldNames==" + this.fieldNames.size() + "]!=[fields=="
					+ this.fields.size() + "]" );
		}
	}

	/**
	 * Metodo para terminar de definir/configurar el mensaje.
	 * <p/>
	 * Esta pensado para que cada registro pueda, por ejemplo, determinar los campos que lo componen.
	 */
	public void setup() {
		int total = this.recordFields.length;
		Field field = null;
		String nombre = null;
		PTIMessageFieldType tipo = null;
		int longitud = 0, enteros = 0, decimales = 0;
		for ( int n = 0; n < total; n++ ) {
			field = this.recordFields[ n ];
			nombre = field.getNombre();
			longitud = field.getLongitud();
			tipo = field.getTipoDeCampo();
			switch ( tipo ) {
				case SIGNED:
					enteros = field.getEnteros();
					decimales = field.getDecimales();
					this.addFieldDefinition( nombre, tipo, longitud, enteros, decimales );
					break;
				case NUMBER:
					decimales = field.getDecimales();
					this.addFieldDefinition( nombre, tipo, longitud, decimales );
					break;
				case ASCII:
					this.addFieldDefinition( nombre, tipo, longitud );
					break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#parse(String)
	 */
	@Override
	public void parseRecord( final String texto ) throws PTIMessageException {
		String data = null;
		int ini = 0;
		int end = 0;

		int total = this.recordFields.length;
		Field field = null;
		String nombre = null;
		int longitud = 0;
		for ( int n = 0; n < total; n++ ) {
			field = this.recordFields[ n ];
			nombre = field.getNombre();
			longitud = field.getLongitud();
			end += longitud;
			data = texto.substring( ini, end );
			this.set( nombre, data );
			ini = end;
		}
	}

	/**
	 * Metodo generico para agregar definiciones de campos al registro.
	 * <p/>
	 * Este metodo esta pensado para campos de tipo TEXTO.
	 * 
	 * @param name
	 *            El nombre del campo.
	 * @param type
	 *            Una instancia de {@link PTIMessageFieldType} con el tipo de
	 *            campo.
	 * @param length
	 *            El tamanyo del campo.
	 */
	protected final void addFieldDefinition( final String name, final PTIMessageFieldType type, final int length ) {
		this.addFieldDefinition( name, type, length, length, 0 );
	}

	/**
	 * Metodo generico para agregar definiciones de campos al registro.
	 * <p/>
	 * Este metodo esta pensado para campos de tipo NUMBER.
	 * 
	 * @param name
	 *            El nombre del campo.
	 * @param type
	 *            Una instancia de {@link PTIMessageFieldType} con el tipo de
	 *            campo.
	 * @param length
	 *            El tamanyo del campo.
	 * @param decimal
	 *            El numero de caracteres de la parte decimal.
	 */
	protected final void addFieldDefinition( final String name, final PTIMessageFieldType type, final int length, final int decimal ) {
		this.addFieldDefinition( name, type, length, length - decimal, decimal );
	}

	/**
	 * Metodo generico para agregar definiciones de campos al registro.
	 * <p/>
	 * Este metodo esta pensado para campos de tipo SIGNED.
	 * 
	 * @param name
	 *            El nombre del campo.
	 * @param type
	 *            Una instancia de {@link PTIMessageFieldType} con el tipo de
	 *            campo.
	 * @param length
	 *            El tamanyo del campo.
	 * @param entera
	 *            El numero de caracteres de la parte entera.
	 * @param decimal
	 *            El numero de caracteres de la parte decimal.
	 */
	protected final void addFieldDefinition( final String name, final PTIMessageFieldType type, final int length, final int entera,
			final int decimal ) {
		this.fieldNames.add( this.fieldNames.size(), name );
		PTIMessageField field = new Field( this.fields.size() + 1, name, type, length, entera, decimal );
		try {
			field.setValor( SIBBACCommons.generateFieldDefaultValue( field ) );
		} catch ( PTIMessageException e ) {
			// Nothing
		}
		this.fields.put( name, field );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#getLongitud()
	 */
	@Override
	public final int getLongitud() {
		return this.longitud;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#getFieldNames()
	 */
	@Override
	public final List< String > getFieldNames() {
		return this.fieldNames;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#setFromPTI(boolean)
	 */
	@Override
	public final void setFromPTI( final boolean fromPTI ) {
		this.fromPTI = fromPTI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#get(String)
	 */
	@Override
	public PTIMessageField get( final String name ) {
		if ( name != null ) {
			return this.fields.get( name );
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#set(String, String)
	 */
	@Override
	public final void set( final String name, final String value ) throws PTIMessageException {
		if ( name != null ) {
			PTIMessageField field = this.fields.get( name );
			field.setValor( value );
			this.fields.put( name, field );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#set(String, Date)
	 */
	@Override
	public final void set( final String name, final Date value ) throws PTIMessageException {
		if ( name != null ) {
			PTIMessageField field = this.fields.get( name );
			field.setValorAsDate( value );
			this.fields.put( name, field );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#set(String, Date)
	 */
	@Override
	public final void set( final String name, final BigDecimal value ) throws PTIMessageException {
		if ( name != null ) {
			PTIMessageField field = this.fields.get( name );
			field.setValorAsBigDecimal( value );
			this.fields.put( name, field );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#get(String)
	 */
	@Override
	public final String getAsString( final String name ) {
		if ( name != null ) {
			if ( this.fieldNames.contains( name ) ) {
				PTIMessageField o = this.fields.get( name );
				return ( o != null ) ? o.getValueAsString() : "<NO VALUE>";
			} else {
				return "<NO FIELD>";
			}
		} else {
			return "<NO NAME>";
		}
	}

	@Override
	public final BigDecimal getAsNumber( final String name ) {
		if ( name != null ) {
			if ( this.fieldNames.contains( name ) ) {
				PTIMessageField o = this.fields.get( name );
				BigDecimal d = null;
				try {
					d = o.getValueAsBigDecimal();
				} catch ( PTIMessageException e ) {
					// Nothing
				}
				return d;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public final Date getAsDate( final String name ) {
		if ( name != null ) {
			if ( this.fieldNames.contains( name ) ) {
				PTIMessageField o = this.fields.get( name );
				Date d = null;
				try {
					d = o.getValueAsDate();
				} catch ( PTIMessageException e ) {
					// Nothing
				}
				return d;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	@Override
	public final Date getAsTime( final String name ) {
		if ( name != null ) {
			if ( this.fieldNames.contains( name ) ) {
				PTIMessageField o = this.fields.get( name );
				Date d = null;
				try {
					d = o.getValueAsTime();
				} catch ( PTIMessageException e ) {
					// Nothing
				}
				return d;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTICommons#toPTIFormat()
	 */
	@Override
	public final String toPTIFormat() throws PTIMessageException {
		StringBuffer sb = new StringBuffer();
		PTIMessageField field = null;
		String valor = null;
		for ( String fieldName : this.fieldNames ) {
			field = this.fields.get( fieldName );
			valor = field.toPTIFormat();
			sb.append( valor );
		}
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Object#toString()
	 */
	@Override
	public final String toString() {
		StringBuffer sb = new StringBuffer();
		PTIMessageField field = null;
		sb.append( "[Registro(" + this.getTipo().name() + ")]" );
		for ( String fieldName : this.fieldNames ) {
			field = this.fields.get( fieldName );
			sb.append( " [" + fieldName + "==" + field.getValueAsString() + "]" );
		}
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageRecord#getTipo()
	 */
	@Override
	public final PTIMessageRecordType getTipo() {
		return this.tipo;
	}

	@Override
	public void persist() throws PTIMessageException {
		// TODO Auto-generated method stub
		SIBBACCommons.undefined();
	}

}
