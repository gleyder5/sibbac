package sibbac.pti;


/**
 * Interfaz que representa un bloque de control.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTIMessageControlBlock extends PTIMessageRecord {

	/**
	 * Metodo que permite conocer el numero de registros detectados en el
	 * mensaje PTI.
	 * 
	 * @param tipo
	 *            Una instancia de {@link PTIMessageRecordType}.
	 * @return
	 */
	public int getNumeroDeRegistros( final PTIMessageRecordType tipo );

	/**
	 * Metodo que permite establecer el numero de registros de un tipo dado que
	 * apareceran en un mensaje PTI.
	 * 
	 * @param tipo
	 *            Una instancia de {@link PTIMessageRecordType}.
	 * @param value
	 *            Un entero.
	 * @throws PTIMessageException
	 *             Si ocurre algun error con el tratamiento del dato.
	 */
	public void setNumRegistros( final PTIMessageRecordType tipo, final int value ) throws PTIMessageException;

	/**
	 * Metodo que permite adicionar una unidad mas al contador de registros de
	 * un tipo dado.
	 * 
	 * @param tipo
	 *            Una instancia de {@link PTIMessageRecordType}.
	 * @throws PTIMessageException
	 *             Si ocurre algun error con el tratamiento del dato.
	 */
	public void addOneRecord( final PTIMessageRecordType tipo ) throws PTIMessageException;

	/**
	 * Metodo que permite adicionar "n" unidades mas al contador de registros de
	 * un tipo dado.
	 * 
	 * @param tipo
	 *            Una instancia de {@link PTIMessageRecordType}.
	 * @param n
	 *            El numero de registros a agregar.
	 * @throws PTIMessageException
	 *             Si ocurre algun error con el tratamiento del dato.
	 */
	public void addNRecords( final PTIMessageRecordType tipo, final int n ) throws PTIMessageException;

}
