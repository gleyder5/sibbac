package sibbac.pti.messages.an;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R04 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R04 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Datos saldos en cuenta
  public static final String FECHA_CONTRATACION = "FECHA_CONTRATACION";
  public static final String FECHA_LIQUIDACION = "FECHA_LIQUIDACION";
  public static final String TIPO_SALDO = "TIPO_SALDO";
  public static final String DIVISA = "DIVISA";
  public static final String INDICADOR_COTIZACION = "INDICADOR_COTIZACION";
  public static final String SALDO_NETO_COMPRADOR_VALORES_NOMINAL = "SALDO_NETO_COMPRADOR_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES = "EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES";
  public static final String SALDO_NETO_VENDEDOR_VALORES_NOMINAL = "SALDO_NETO_VENDEDOR_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_SALDO_NETO_VENDEDOR_DE_VALORES = "EFECTIVO_DEL_SALDO_NETO_VENEDOR_DE_VALORES";
  public static final String SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL = "SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES = "EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES";
  public static final String SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL = "SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES = "EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES";
  public static final String VALORES_NOMINAL_RETENIDO = "VALORES_NOMINAL_RETENIDO";
  public static final String IMPORTE_EFECTIVO_RETENIDO = "IMPORTE_EFECTIVO_RETENIDO";
  public static final String SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL = "SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR = "EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR";
  public static final String SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL = "SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR = "EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR";
  public static final String SALDO_PRESTAMISTA_VALORES_NOMINAL = "SALDO_PRESTAMISTA_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA = "EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA";
  public static final String SALDO_PRESTATARIO_VALORES_NOMINAL = "SALDO_PRESTATARIO_VALORES_NOMINAL";
  public static final String EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO = "EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO";
  public static final String RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES = "RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES";
  public static final String EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES = "EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES";
  public static final String ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES = "ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES";
  public static final String EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES = "EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES";
  public static final String EFECTIVO_PENDIENTE_DE_LIQUIDAR = "EFECTIVO_PENDIENTE_DE_LIQUIDAR";
  public static final String SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL = "SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL";
  public static final String EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR = "EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR";
  public static final String SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL = "SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL";
  public static final String EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR = "EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR";
  public static final String SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL = "SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL";
  public static final String SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL = "SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL";
  public static final String EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR = "EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR";
  public static final String EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR = "EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR";

  // Tamanyos de los campos.
  public static final int SIZE_FECHA_CONTRATACION = 8;
  public static final int SIZE_FECHA_LIQUIDACION = 8;
  public static final int SIZE_TIPO_SALDO = 3;
  public static final int SIZE_DIVISA = 3;
  public static final int SIZE_INDICADOR_COTIZACION = 1;
  public static final int SIZE_SALDO_NETO_COMPRADOR_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES = 16;
  public static final int SIZE_SALDO_NETO_VENDEDOR_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_SALDO_NETO_VENEDOR_DE_VALORES = 16;
  public static final int SIZE_SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES = 16;
  public static final int SIZE_SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES = 16;
  public static final int SIZE_VALORES_NOMINAL_RETENIDO = 18;
  public static final int SIZE_IMPORTE_EFECTIVO_RETENIDO = 16;
  public static final int SIZE_SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR = 16;
  public static final int SIZE_SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR = 16;
  public static final int SIZE_SALDO_PRESTAMISTA_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA = 16;
  public static final int SIZE_SALDO_PRESTATARIO_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO = 16;
  public static final int SIZE_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES = 18;
  public static final int SIZE_EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES = 16;
  public static final int SIZE_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES = 18;
  public static final int SIZE_EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES = 16;
  public static final int SIZE_EFECTIVO_PENDIENTE_DE_LIQUIDAR = 16;
  public static final int SIZE_SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR = 16;
  public static final int SIZE_SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR = 16;
  public static final int SIZE_SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR = 16;
  public static final int SIZE_SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL = 18;
  public static final int SIZE_EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR = 16;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = {
      new Field(1, FECHA_CONTRATACION, PTIMessageFieldType.ASCII, SIZE_FECHA_CONTRATACION),
      new Field(2, FECHA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_LIQUIDACION),
      new Field(3, TIPO_SALDO, PTIMessageFieldType.ASCII, SIZE_TIPO_SALDO),
      new Field(4, DIVISA, PTIMessageFieldType.ASCII, SIZE_DIVISA),
      new Field(5, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION),
      new Field(6, SALDO_NETO_COMPRADOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_NETO_COMPRADOR_VALORES_NOMINAL, 6),
      new Field(7, EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES, 13, 2),
      new Field(8, SALDO_NETO_VENDEDOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_NETO_VENDEDOR_VALORES_NOMINAL, 6),
      new Field(9, EFECTIVO_DEL_SALDO_NETO_VENDEDOR_DE_VALORES, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_SALDO_NETO_VENEDOR_DE_VALORES, 13, 2),
      new Field(10, SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL, 6),
      new Field(11, EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES, 13, 2),
      new Field(12, SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL, PTIMessageFieldType.ASCII,
          SIZE_SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL, 6),
      new Field(13, EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES, 13, 2),
      new Field(14, VALORES_NOMINAL_RETENIDO, PTIMessageFieldType.NUMBER, SIZE_VALORES_NOMINAL_RETENIDO, 6),
      new Field(15, IMPORTE_EFECTIVO_RETENIDO, PTIMessageFieldType.SIGNED, SIZE_IMPORTE_EFECTIVO_RETENIDO, 13, 2),
      new Field(16, SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL, 6),
      new Field(17, EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR, 13, 2),
      new Field(18, SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL, 6),
      new Field(19, EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR, 13, 2),
      new Field(20, SALDO_PRESTAMISTA_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_PRESTAMISTA_VALORES_NOMINAL, 6),
      new Field(21, EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA, 13, 2),
      new Field(22, SALDO_PRESTATARIO_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_PRESTATARIO_VALORES_NOMINAL, 6),
      new Field(23, EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO, 13, 2),
      new Field(24, RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.NUMBER,
          SIZE_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, 6),
      new Field(25, EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, 13, 2),
      new Field(26, ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.NUMBER,
          SIZE_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, 6),
      new Field(27, EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, 13, 2),
      new Field(28, EFECTIVO_PENDIENTE_DE_LIQUIDAR, PTIMessageFieldType.SIGNED, SIZE_EFECTIVO_PENDIENTE_DE_LIQUIDAR,
          13, 2),
      new Field(29, SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, 6),
      new Field(30, EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR, 13, 2),
      new Field(31, SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, 6),
      new Field(32, EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR, 13, 2),
      new Field(33, SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, 6),
      new Field(34, EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR, 13, 2),
      new Field(35, SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
          SIZE_SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, 6),
      new Field(36, EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR, PTIMessageFieldType.SIGNED,
          SIZE_EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR, 13, 2) };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public R04() throws PTIMessageException {
    super(PTIMessageRecordType.R04, FIELDS);
  }

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  protected R04(final Field[] recordFields) throws PTIMessageException {
    super(PTIMessageRecordType.R04, recordFields);
  }

  public Date getFechaContratacion() {
    return this.getAsDate(FECHA_CONTRATACION);
  }

  public Date getFechaLiquidacion() {
    return this.getAsDate(FECHA_LIQUIDACION);
  }

  public String getTipoSaldo() {
    return this.getAsString(TIPO_SALDO);
  }

  public String getDivisa() {
    return this.getAsString(DIVISA);
  }

  public String getIndicadorCotizacion() {
    return this.getAsString(INDICADOR_COTIZACION);
  }

  public BigDecimal getSaldoNetoCompradorValoresNominal() {
    return this.getAsNumber(SALDO_NETO_COMPRADOR_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelSaldoNetoCompradorDeValores() {
    return this.getAsNumber(EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES);
  }

  public BigDecimal getSaldoNetoVendedorValoresNominal() {
    return this.getAsNumber(SALDO_NETO_VENDEDOR_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelSaldoNetoVendedorDeValores() {
    return this.getAsNumber(EFECTIVO_DEL_SALDO_NETO_VENDEDOR_DE_VALORES);
  }

  public BigDecimal getSaldoBrutoCompradorValoresNominal() {
    return this.getAsNumber(SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelSaldoBrutoCompradorDeValores() {
    return this.getAsNumber(EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES);
  }

  public BigDecimal getSaldoBrutoVendedorValoresNominal() {
    return this.getAsNumber(SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelSaldoBrutoVendedorDeValores() {
    return this.getAsNumber(EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES);
  }

  public BigDecimal getValoresNominalRetenido() {
    return this.getAsNumber(VALORES_NOMINAL_RETENIDO);
  }

  public BigDecimal getImporteEfectivoRetenido() {
    return this.getAsNumber(IMPORTE_EFECTIVO_RETENIDO);
  }

  public BigDecimal getSaldoFallidoVendedorValoresNominal() {
    return this.getAsNumber(SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelSaldoFallidoVendedor() {
    return this.getAsNumber(EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR);
  }

  public BigDecimal getSaldoFallidoCompradorValoresNominal() {
    return this.getAsNumber(SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelSaldoFallidoComprador() {
    return this.getAsNumber(EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR);
  }

  public BigDecimal getSaldoPrestamistaValoresNominal() {
    return this.getAsNumber(SALDO_PRESTAMISTA_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelColateralDelSaldoPrestamista() {
    return this.getAsNumber(EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA);
  }

  public BigDecimal getSaldoPrestatarioValoresNominal() {
    return this.getAsNumber(SALDO_PRESTATARIO_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoDelColateralDelSaldoPrestatario() {
    return this.getAsNumber(EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO);
  }

  public BigDecimal getRecepcionDeValoresNominalPorAjustes() {
    return this.getAsNumber(RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES);
  }

  public BigDecimal getEfectivoDeLaRecepcionDeValoresNominalPorAjustes() {
    return this.getAsNumber(EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES);
  }

  public BigDecimal getEntregaDeValoresNominalPorAjustes() {
    return this.getAsNumber(ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES);
  }

  public BigDecimal getEfectivoDeLaEntregaDeValoresNominalPorAjustes() {
    return this.getAsNumber(EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES);
  }

  public BigDecimal getEfectivoPendienteDeLiquidar() {
    return this.getAsNumber(EFECTIVO_PENDIENTE_DE_LIQUIDAR);
  }

  public BigDecimal getSaldoVendedorOperacionesEspecialesValoresNominal() {
    return this.getAsNumber(SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoOperacionesEspecialesVendedor() {
    return this.getAsNumber(EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR);
  }

  public BigDecimal getSaldoCompradorOperacionesEspecialesValoresNominal() {
    return this.getAsNumber(SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoOperacionesEspecialesComprador() {
    return this.getAsNumber(EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR);
  }

  public BigDecimal getSaldoVendedorVencimientoDeFuturosYOpcionesValoresNominal() {
    return this.getAsNumber(SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoVencimientoDeFuturosYOpcionesVendedor() {
    return this.getAsNumber(EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR);
  }

  public BigDecimal getSaldoCompradorVencimientoDeFuturosYOpcionesValoresNominal() {
    return this.getAsNumber(SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL);
  }

  public BigDecimal getEfectivoVencimientoDeFuturosYOpcionesComprador() {
    return this.getAsNumber(EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR);
  }

  public void setFechaContratacion(final String valor) throws PTIMessageException {
    this.set(FECHA_CONTRATACION, valor);
  }

  public void setFechaLiquidacion(final String valor) throws PTIMessageException {
    this.set(FECHA_LIQUIDACION, valor);
  }

  public void setTipoSaldo(final String valor) throws PTIMessageException {
    this.set(TIPO_SALDO, valor);
  }

  public void setDivisa(final String valor) throws PTIMessageException {
    this.set(DIVISA, valor);
  }

  public void setIndicadorCotizacion(final String valor) throws PTIMessageException {
    this.set(INDICADOR_COTIZACION, valor);
  }

  public void setSaldoNetoCompradorValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_NETO_COMPRADOR_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelSaldoNetoCompradorDeValores(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES, valor);
  }

  public void setSaldoNetoVendedorValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_NETO_VENDEDOR_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelSaldoNetoVendedorDeValores(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_SALDO_NETO_VENDEDOR_DE_VALORES, valor);
  }

  public void setSaldoBrutoCompradorValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelSaldoBrutoCompradorDeValores(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES, valor);
  }

  public void setSaldoBrutoVendedorValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelSaldoBrutoVendedorDeValores(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES, valor);
  }

  public void setValoresNominalRetenido(final String valor) throws PTIMessageException {
    this.set(VALORES_NOMINAL_RETENIDO, valor);
  }

  public void setImporteEfectivoRetenido(final String valor) throws PTIMessageException {
    this.set(IMPORTE_EFECTIVO_RETENIDO, valor);
  }

  public void setSaldoFallidoVendedorValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelSaldoFallidoVendedor(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR, valor);
  }

  public void setSaldoFallidoCompradorValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelSaldoFallidoComprador(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR, valor);
  }

  public void setSaldoPrestamistaValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_PRESTAMISTA_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelColateralDelSaldoPrestamista(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA, valor);
  }

  public void setSaldoPrestatarioValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_PRESTATARIO_VALORES_NOMINAL, valor);
  }

  public void setEfectivoDelColateralDelSaldoPrestatario(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO, valor);
  }

  public void setRecepcionDeValoresNominalPorAjustes(final String valor) throws PTIMessageException {
    this.set(RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, valor);
  }

  public void setEfectivoDeLaRecepcionDeValoresNominalPorAjustes(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, valor);
  }

  public void setEntregaDeValoresNominalPorAjustes(final String valor) throws PTIMessageException {
    this.set(ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, valor);
  }

  public void setEfectivoDeLaEntregaDeValoresNominalPorAjustes(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, valor);
  }

  public void setEfectivoPendienteDeLiquidar(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_PENDIENTE_DE_LIQUIDAR, valor);
  }

  public void setSaldoVendedorOperacionesEspecialesValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, valor);
  }

  public void setEfectivoOperacionesEspecialesVendedor(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR, valor);
  }

  public void setSaldoCompradorOperacionesEspecialesValoresNominal(final String valor) throws PTIMessageException {
    this.set(SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, valor);
  }

  public void setEfectivoOperacionesEspecialesComprador(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR, valor);
  }

  public void setSaldoVendedorVencimientoDeFuturosYOpcionesValoresNominal(final String valor)
      throws PTIMessageException {
    this.set(SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, valor);
  }

  public void setEfectivoVencimientoDeFuturosYOpcionesVendedor(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR, valor);
  }

  public void setSaldoCompradorVencimientoDeFuturosYOpcionesValoresNominal(final String valor)
      throws PTIMessageException {
    this.set(SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, valor);
  }

  public void setEfectivoVencimientoDeFuturosYOpcionesComprador(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR, valor);
  }

}
