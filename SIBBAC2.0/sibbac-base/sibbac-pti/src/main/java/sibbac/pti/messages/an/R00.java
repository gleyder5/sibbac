package sibbac.pti.messages.an;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R00 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Datos generales
  public static final String MIEMBRO = "MIEMBRO";
  public static final String CUENTA_DE_COMPENSACION = "CUENTA_DE_COMPENSACION";
  public static final String CODIGO_DE_VALOR = "CODIGO_DE_VALOR";
  public static final String MIEMBRO_COMPENSADOR = "MIEMBRO_COMPENSADOR";
  public static final String ENTIDAD_PARTICIPANTE_BIC = "ENTIDAD_PARTICIPANTE_BIC";
  public static final String CUENTA_LIQUIDACION = "CUENTA_LIQUIDACION";

  // Tamanyos de los campos.
  public static final int SIZE_MIEMBRO = 4;
  public static final int SIZE_CUENTA_DE_COMPENSACION = 3;
  public static final int SIZE_CODIGO_DE_VALOR = 12;
  public static final int SIZE_MIEMBRO_COMPENSADOR = 4;
  public static final int SIZE_ENTIDAD_PARTICIPANTE_BIC = 11;
  public static final int SIZE_CUENTA_LIQUIDACION = 35;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, MIEMBRO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO),
      new Field(2, CUENTA_DE_COMPENSACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_DE_COMPENSACION),
      new Field(3, CODIGO_DE_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_VALOR),
      new Field(4, MIEMBRO_COMPENSADOR, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_COMPENSADOR),
      new Field(5, ENTIDAD_PARTICIPANTE_BIC, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_PARTICIPANTE_BIC),
      new Field(6, CUENTA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_LIQUIDACION) };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public R00() throws PTIMessageException {
    super(PTIMessageRecordType.R00, FIELDS);
  }

  public String getMiembro() {
    return this.getAsString(MIEMBRO);
  }

  public String getCuentaDeCompensacion() {
    return this.getAsString(CUENTA_DE_COMPENSACION);
  }

  public String getCodigoDeValor() {
    return this.getAsString(CODIGO_DE_VALOR);
  }

  public String getMiembroCompensador() {
    return this.getAsString(MIEMBRO_COMPENSADOR);
  }

  public String getEntidadParticipanteBIC() {
    return this.getAsString(ENTIDAD_PARTICIPANTE_BIC);
  }

  public String getCuentaLiquidacion() {
    return this.getAsString(CUENTA_LIQUIDACION);
  }

  public void setMiembro(final String valor) throws PTIMessageException {
    this.set(MIEMBRO, valor);
  }

  public void setCuentaDeCompensacion(final String valor) throws PTIMessageException {
    this.set(CUENTA_DE_COMPENSACION, valor);
  }

  public void setCodigoDeValor(final String valor) throws PTIMessageException {
    this.set(CODIGO_DE_VALOR, valor);
  }

  public void setMiembroCompensador(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_COMPENSADOR, valor);
  }

  public void setEntidadParticipanteBIC(final String valor) throws PTIMessageException {
    this.set(ENTIDAD_PARTICIPANTE_BIC, valor);
  }

  public void setCuentaLiquidacion(final String valor) throws PTIMessageException {
    this.set(CUENTA_LIQUIDACION, valor);
  }

}