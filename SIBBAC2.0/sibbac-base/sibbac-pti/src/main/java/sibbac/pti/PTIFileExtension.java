package sibbac.pti;

public enum PTIFileExtension {

  GZ,
  ZIP,
  DAT,
  TMP;
}
