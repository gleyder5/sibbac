package sibbac.pti.messages.an;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.AControlBlock;
import sibbac.pti.messages.Field;


/**
 * Bloque de control.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class ANControlBlock extends AControlBlock {

	// -------------------------------------------- La lista de campos.

	public static final String	R00		= PTIMessageRecordType.R00.name();
	public static final String	R01		= PTIMessageRecordType.R01.name();
	public static final String	R02		= PTIMessageRecordType.R02.name();
	public static final String	R03		= PTIMessageRecordType.R03.name();
	public static final String	R04		= PTIMessageRecordType.R04.name();
	public static final String	R05		= PTIMessageRecordType.R05.name();
	public static final String	R06		= PTIMessageRecordType.R06.name();
	public static final String	R07		= PTIMessageRecordType.R07.name();

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS	= {
			new Field( 1, R00, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 2, R01, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 3, R02, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 4, R03, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 5, R04, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 6, R05, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 7, R06, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 8, R07, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 )
	};

	/**
	 * Constructor por defecto.
	 * 
	 */
	public ANControlBlock() throws PTIMessageException {
		super( PTIMessageRecordType.CONTROL_BLOCK, FIELDS );
	}

	public BigDecimal getR00() {
		return this.getAsNumber( R00 );
	}

	public BigDecimal getR01() {
		return this.getAsNumber( R01 );
	}

	public BigDecimal getR02() {
		return this.getAsNumber( R02 );
	}

	public BigDecimal getR03() {
		return this.getAsNumber( R03 );
	}

	public BigDecimal getR04() {
		return this.getAsNumber( R04 );
	}

	public BigDecimal getR05() {
		return this.getAsNumber( R05 );
	}

	public BigDecimal getR06() {
		return this.getAsNumber( R06 );
	}

	public BigDecimal getR07() {
		return this.getAsNumber( R07 );
	}

	public void setR00( final String valor ) throws PTIMessageException {
		this.set( R00, valor );
	}

	public void setR01( final String valor ) throws PTIMessageException {
		this.set( R01, valor );
	}

	public void setR02( final String valor ) throws PTIMessageException {
		this.set( R02, valor );
	}

	public void setR03( final String valor ) throws PTIMessageException {
		this.set( R03, valor );
	}

	public void setR04( final String valor ) throws PTIMessageException {
		this.set( R04, valor );
	}

	public void setR05( final String valor ) throws PTIMessageException {
		this.set( R05, valor );
	}

	public void setR06( final String valor ) throws PTIMessageException {
		this.set( R06, valor );
	}

	public void setR07( final String valor ) throws PTIMessageException {
		this.set( R07, valor );
	}

	
}
