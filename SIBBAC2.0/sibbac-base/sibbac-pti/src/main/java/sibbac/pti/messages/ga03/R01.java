package sibbac.pti.messages.ga03;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R03 para el mensaje de tipo: GA01 (Garantias y Movimientos de Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Risk Limit
  public static final String RISKLIMITAMOUNT = "RISKLIMITAMOUNT";
  public static final String RISKLIMITTYPE = "RISKLIMITTYPE";
  public static final String MARGINAMOUNTMARKETID = "MARGINAMOUNTMARKETID";

  // Tamanyos de los campos.
  public static final int SIZE_RISKLIMITAMOUNT = 15;
  public static final int SIZE_RISKLIMITTYPE = 3;
  public static final int SIZE_MARGINAMOUNTMARKETID = 2;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, RISKLIMITAMOUNT, PTIMessageFieldType.NUMBER,
                                                   SIZE_RISKLIMITAMOUNT, 2), new Field(2, RISKLIMITTYPE,
                                                                                       PTIMessageFieldType.NUMBER,
                                                                                       SIZE_RISKLIMITTYPE, 0), new Field(
                                                                                                                         3,
                                                                                                                         MARGINAMOUNTMARKETID,
                                                                                                                         PTIMessageFieldType.ASCII,
                                                                                                                         SIZE_MARGINAMOUNTMARKETID)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R01() throws PTIMessageException {
    super(PTIMessageRecordType.R01, FIELDS);
  }

  public BigDecimal getRiskLimitAmount() {
    return this.getAsNumber(RISKLIMITAMOUNT);
  }

  public BigDecimal getRiskLimitType() {
    return this.getAsNumber(RISKLIMITTYPE);
  }

  public String getMarginAmountMarketId() {
    return this.getAsString(MARGINAMOUNTMARKETID);
  }

  public void setRiskLimitAmount(final String valor) throws PTIMessageException {
    this.set(RISKLIMITAMOUNT, valor);
  }

  public void setRiskLimitType(final String valor) throws PTIMessageException {
    this.set(RISKLIMITTYPE, valor);
  }

  public void setMarginAmountMarketId(final String valor) throws PTIMessageException {
    this.set(MARGINAMOUNTMARKETID, valor);
  }

}
