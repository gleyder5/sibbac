package sibbac.pti.messages.rt;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: RT. (Asignacion Referencias
 * Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Datos referencia titular
	public static final String	REFERENCIA_TITULAR					= "REFERENCIA_TITULAR";
	public static final String	INDICADOR_COTIZACION				= "INDICADOR_COTIZACION";
	public static final String	NUM_VALORES_IMPORTE_NOMINAL			= "NUM_VALORES_IMPORTE_NOMINAL";

	// Datos a devolver
	public static final String	CODIGO_DE_ERROR						= "CODIGO_DE_ERROR";

	// Tamanyos de los campos.
	public static final int		SIZE_REFERENCIA_TITULAR				= 20;
	public static final int		SIZE_INDICADOR_COTIZACION			= 1;
	public static final int		SIZE_NUM_VALORES_IMPORTE_NOMINAL	= 18;
	public static final int		SIZE_CODIGO_DE_ERROR				= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, REFERENCIA_TITULAR, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_TITULAR ),
			new Field( 2, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 3, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 4, CODIGO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_ERROR )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}
	

  protected R01(final Field[] recordFields) throws PTIMessageException {
    super(PTIMessageRecordType.R01, recordFields);
  }

	public String getReferenciaTitular() {
		return this.getAsString( REFERENCIA_TITULAR );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public BigDecimal getNumValoresImporteNominal() {
		return this.getAsNumber( NUM_VALORES_IMPORTE_NOMINAL );
	}

	public String getCodigoDeError() {
		return this.getAsString( CODIGO_DE_ERROR );
	}

	public void setReferenciaTitular( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_TITULAR, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setNumValoresImporteNominal( final BigDecimal valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setNumValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setCodigoDeError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_ERROR, valor );
	}


}
