package sibbac.pti;


/**
 * Enumeracion con los distintos tipos de campos en un mensaje PTI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public enum PTIMessageFieldType {

	/**
	 * Campo alfanumerico.
	 */
	ASCII( "Alfanumerico", "A", "s" ),

	/**
	 * Campo numerico.
	 */
	NUMBER( "Numerico", "N", "d" ),

	/**
	 * Campo numerico con signo. El signo viene siempre en la primera posicion.
	 */
	SIGNED( "Numerico con signo", "NS", "d" );

	/**
	 * Una breve descripcion del codigo.
	 */
	private String	description;

	/**
	 * El codigo de texto que vendra en la documentacion del mensaje para
	 * identificar el tipo de campo.
	 */
	private String	code;

	/**
	 * El codigo de formato.
	 */
	private String	format;

	/**
	 * Constructor basico.
	 * 
	 * @param descripcion
	 *            Descripcion.
	 * @param code
	 *            Codigo.
	 * @param formatl
	 *            Formato.
	 */
	private PTIMessageFieldType( final String descripcion, final String code, final String format ) {
		this.description = descripcion;
		this.code = code;
		this.format = format;
	}

	/**
	 * Getter for code
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Getter for description
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter for format
	 *
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	public boolean isText() {
		return this.name().equalsIgnoreCase( ASCII.name() );
	}

	public boolean isSigned() {
		return this.name().equalsIgnoreCase( SIGNED.name() );
	}

}
