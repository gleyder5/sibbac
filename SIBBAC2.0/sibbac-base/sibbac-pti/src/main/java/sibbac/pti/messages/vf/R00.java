package sibbac.pti.messages.vf;


import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: MO. (Datos Cuenta Origen).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos Cuenta Origen
	public static final String	ID_INSTRUCCION_LIQUIDACION		= "ID_INSTRUCCION_LIQUIDACION";
	public static final String	INDICADOR_TIPO_AFECTACION		= "INDICADOR_TIPO_AFECTACION";
	public static final String	ACTUACION						= "ACTUACION";
	public static final String	FECHA_LIQUIDACION				= "FECHA_LIQUIDACION";

	// Tamanyos de los campos.
	public static final int		SIZE_ID_INSTRUCCION_LIQUIDACION	= 16;
	public static final int		SIZE_INDICADOR_TIPO_AFECTACION	= 1;
	public static final int		SIZE_ACTUACION					= 1;
	public static final int		SIZE_FECHA_LIQUIDACION			= 8;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS							= {
			new Field( 1, ID_INSTRUCCION_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_ID_INSTRUCCION_LIQUIDACION ),
			new Field( 2, INDICADOR_TIPO_AFECTACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_TIPO_AFECTACION ),
			new Field( 3, ACTUACION, PTIMessageFieldType.ASCII, SIZE_ACTUACION ),
			new Field( 4, FECHA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_LIQUIDACION )

																};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getIdInstruccionLiquidacion() {
		return this.getAsString( ID_INSTRUCCION_LIQUIDACION );
	}

	public String getIndicadorTipoAfectacion() {
		return this.getAsString( INDICADOR_TIPO_AFECTACION );
	}

	public String getActuacion() {
		return this.getAsString( ACTUACION );
	}

	public Date getFechaLiquidacion() {
		return this.getAsDate( FECHA_LIQUIDACION );
	}

	public void setIdInstruccionLiquidacion( final String valor ) throws PTIMessageException {
		this.set( ID_INSTRUCCION_LIQUIDACION, valor );
	}

	public void setIndicadorTipoAfectacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_TIPO_AFECTACION, valor );
	}

	public void setActuacion( final String valor ) throws PTIMessageException {
		this.set( ACTUACION, valor );
	}

	public void setFechaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_LIQUIDACION, valor );
	}


}
