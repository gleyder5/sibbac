package sibbac.pti;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Interfaz que define la operativa comun a todos los "registros".<br/>
 * El elemento: "T" representa el tipo de objeto a devolver por el metodo: " {@link #parseRecord(String)}.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTIMessageRecord extends PTICommons, PTIConstants {

  // --------------------------------------------------------- General

  /**
   * Devuelve la tipologia de un registro dado.
   * 
   * @return Un {@link PTIMessageRecordType}.
   */
  public PTIMessageRecordType getTipo();

  /**
   * Metodo de conveniencia para establecer si un mensaje es un parsing de un mensaje PTI recibido o es una instancia
   * creada internamente.<br/>
   * Este dato servira para efectuar o no validaciones internas.
   * 
   * @param fromPTI TRUE o FALSE.
   */
  public void setFromPTI(final boolean fromPTI);

  /**
   * Metodo de conveniencia que permite conocer el tamanyo total del registro.
   * 
   * @return Un entero con el tamanyo del registro.
   */
  public int getLongitud();

  /**
   * Metodo interno para parsear una cadena de texto.
   * 
   * @param texto El {@link String} a parsear.
   * @throws PTIMessageException Si hay errores durante el parseo.
   */
  public void parseRecord(final String texto) throws PTIMessageException;

  /**
   * Metodo para persistir mensajes en base de datos.
   * 
   * @throws PTIMessageException Si ocurre algun error persistiendo en base de datos.
   */
  public void persist() throws PTIMessageException;

  // ---------------------------------------------------------- Campos

  /**
   * Devuelve la lista ordenada de campos de esta entidad.
   * 
   * @return Un {@link List} de {@link String}'s con los nombres de los campos.
   */
  public List<String> getFieldNames();

  /**
   * Metodo generico para recuperar un campo dado.
   * 
   * @param name El nombre del campo.
   * @return Una instancia de {@link PTIMessageField} correspondiente al campo solicitado.
   */
  public PTIMessageField get(final String name);

  /**
   * Metodo generico para establecer el valor de un campo dado.
   * 
   * @param name El nombre del campo.
   * @param value El valor del campo.
   * @throws PTIMessageException Si ocurre algun error con el tratamiento del dato.
   */
  public void set(final String name, final String value) throws PTIMessageException;

  /**
   * Metodo generico para establecer el valor de un campo dado.
   * 
   * @param name El nombre del campo.
   * @param value El valor del campo en formato {@link Date}.
   * @throws PTIMessageException Si ocurre algun error con el tratamiento del dato.
   */
  public void set(final String name, final Date value) throws PTIMessageException;

  /**
   * Metodo generico para establecer el valor de un campo dado.
   * 
   * @param name El nombre del campo.
   * @param value El valor del campo en formato {@link BigDecimal}.
   * @throws PTIMessageException Si ocurre algun error con el tratamiento del dato.
   */
  public void set(final String name, final BigDecimal value) throws PTIMessageException;

  /**
   * Metodo que devuelve el valor de un campo como cadena de texto.
   * 
   * @param name El nombre del campo a localizar.
   * 
   * @return Un {@link String} con el valor de dicho campo.
   */
  public String getAsString(final String name);

  /**
   * Metodo que devuelve el valor de un campo como numero.
   * 
   * @param name El nombre del campo a localizar.
   * 
   * @return Un {@link BigDecimal} con el valor de dicho campo.
   */
  public BigDecimal getAsNumber(final String name);

  /**
   * Metodo que devuelve el valor de un campo como fecha.
   * 
   * @param name El nombre del campo a localizar.
   * 
   * @return Un {@link Date} con el valor de dicho campo.
   */
  public Date getAsDate(final String name);

  /**
   * Metodo que devuelve el valor de un campo como hora.
   * 
   * @param name El nombre del campo a localizar.
   * 
   * @return Un {@link Time} con el valor de dicho campo.
   */
  public Date getAsTime(final String name);

}
