/**
 * 
 */
package sibbac.pti.messages.ga03;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * 
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA03CommonDataBlock extends ARecord implements PTIMessageRecord {

  // -------------------------------------------- La lista de campos.

  /**
   * Los campos del bloque de datos comunes.
   */
  public static final String MARGINREQMTRPTID = "MARGINREQMTRPTID";
  public static final String MARGINREQMTINQID = "MARGINREQMTINQID";
  public static final String MARGINREQMTRPTTYPE = "MARGINREQMTRPTTYPE";
  public static final String CURRENCY = "CURRENCY";
  public static final String EXECUTING_FIRM = "EXECUTING_FIM";
  public static final String CLEARING_FIRM = "CLEARING_FIRM";
  public static final String POSITION_ACCOUNT = "POSITION_ACCOUNT";
  public static final String NUMERO_DE_REGISTROS_R00 = "NUMERO_DE_REGISTROS_R00";
  public static final String NUMERO_DE_REGISTROS_R01 = "NUMERO_DE_REGISTROS_R01";

  public static final int SIZE_MARGINREQMTRPTID = 20;
  public static final int SIZE_MARGINREQMTINQID = 10;
  public static final int SIZE_MARGINREQMTRPTTYPE = 1;
  public static final int SIZE_CURRENCY = 3;
  public static final int SIZE_EXECUTING_FIRM = 4;
  public static final int SIZE_CLEARING_FIRM = 4;
  public static final int SIZE_POSITION_ACCOUNT = 3;
  public static final int SIZE_NUMERO_DE_REGISTROS_R00 = 9;
  public static final int SIZE_NUMERO_DE_REGISTROS_R01 = 9;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, MARGINREQMTRPTID, PTIMessageFieldType.ASCII,
                                                   SIZE_MARGINREQMTRPTID), new Field(2, MARGINREQMTINQID,
                                                                                     PTIMessageFieldType.ASCII,
                                                                                     SIZE_MARGINREQMTINQID), new Field(
                                                                                                                       3,
                                                                                                                       CURRENCY,
                                                                                                                       PTIMessageFieldType.ASCII,
                                                                                                                       SIZE_CURRENCY), new Field(
                                                                                                                                                 4,
                                                                                                                                                 MARGINREQMTRPTTYPE,
                                                                                                                                                 PTIMessageFieldType.NUMBER,
                                                                                                                                                 SIZE_MARGINREQMTRPTTYPE,
                                                                                                                                                 0), new Field(
                                                                                                                                                               5,
                                                                                                                                                               CURRENCY,
                                                                                                                                                               PTIMessageFieldType.ASCII,
                                                                                                                                                               SIZE_CURRENCY), new Field(
                                                                                                                                                                                         6,
                                                                                                                                                                                         EXECUTING_FIRM,
                                                                                                                                                                                         PTIMessageFieldType.ASCII,
                                                                                                                                                                                         SIZE_EXECUTING_FIRM), new Field(
                                                                                                                                                                                                                         7,
                                                                                                                                                                                                                         CLEARING_FIRM,
                                                                                                                                                                                                                         PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                         SIZE_CLEARING_FIRM), new Field(
                                                                                                                                                                                                                                                        9,
                                                                                                                                                                                                                                                        POSITION_ACCOUNT,
                                                                                                                                                                                                                                                        PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                        SIZE_POSITION_ACCOUNT), new Field(
                                                                                                                                                                                                                                                                                          10,
                                                                                                                                                                                                                                                                                          NUMERO_DE_REGISTROS_R00,
                                                                                                                                                                                                                                                                                          PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                          SIZE_NUMERO_DE_REGISTROS_R00,
                                                                                                                                                                                                                                                                                          0), new Field(
                                                                                                                                                                                                                                                                                                        11,
                                                                                                                                                                                                                                                                                                        NUMERO_DE_REGISTROS_R01,
                                                                                                                                                                                                                                                                                                        PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                                        SIZE_NUMERO_DE_REGISTROS_R01,
                                                                                                                                                                                                                                                                                                        0)
  };

  /**
   * Constructor por defecto.
   */
  public GA03CommonDataBlock() throws PTIMessageException {
    super(PTIMessageRecordType.COMMON_DATA_BLOCK, FIELDS);
  }

  public String getMarginReqmtRptId() {
    return this.getAsString(MARGINREQMTRPTID);
  }

  public String getMarginReqmtInqId() {
    return this.getAsString(MARGINREQMTINQID);
  }

  public BigDecimal getMarginReqmtRptType() {
    return this.getAsNumber(MARGINREQMTRPTTYPE);
  }

  public String getCurrency() {
    return this.getAsString(CURRENCY);
  }

  public String getExecutingFirm() {
    return this.getAsString(EXECUTING_FIRM);
  }

  public String getClearingFirm() {
    return this.getAsString(CLEARING_FIRM);
  }

  public String getPositionAccount() {
    return this.getAsString(POSITION_ACCOUNT);
  }

  public BigDecimal getNumeroRegistrosR00() {
    return this.getAsNumber(NUMERO_DE_REGISTROS_R00);
  }

  public BigDecimal getNumeroRegistrosR01() {
    return this.getAsNumber(NUMERO_DE_REGISTROS_R01);
  }

  public void setMarginReqmtRptId(final String valor) throws PTIMessageException {
    this.set(MARGINREQMTRPTID, valor);
  }

  public void setMarginReqmtInqId(final String valor) throws PTIMessageException {
    this.set(MARGINREQMTINQID, valor);
  }

  public void setMarginReqmtRptType(final String valor) throws PTIMessageException {
    this.set(MARGINREQMTRPTTYPE, valor);
  }

  public void setCurrency(final String valor) throws PTIMessageException {
    this.set(CURRENCY, valor);
  }

  public void setExecutingFirm(final String valor) throws PTIMessageException {
    this.set(EXECUTING_FIRM, valor);
  }

  public void setClearingFirm(final String valor) throws PTIMessageException {
    this.set(CLEARING_FIRM, valor);
  }

  public void setPositionAccount(final String valor) throws PTIMessageException {
    this.set(POSITION_ACCOUNT, valor);
  }

  public void setNumeroRegistrosR00(final String valor) throws PTIMessageException {
    this.set(NUMERO_DE_REGISTROS_R00, valor);
  }

  public void setNumeroRegistrosR01(final String valor) throws PTIMessageException {
    this.set(NUMERO_DE_REGISTROS_R01, valor);
  }

}
