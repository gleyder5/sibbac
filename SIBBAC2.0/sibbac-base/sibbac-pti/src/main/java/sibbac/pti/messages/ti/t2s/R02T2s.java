package sibbac.pti.messages.ti.t2s;


import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.ti.R02;


/**
 * Registro R02 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02T2s extends R02 {

  // -------------------------------------------- La lista de campos.
  // Relacion cotitulares
 
  public static final String FECHA_NACIMIENTO = "FECHA_NACIMIENTO";

  // Tamanyos de los campos.
 
  public static final int SIZE_FECHA_NACIMIENTO = 8;
  
	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, NOMBRE_RAZON_SOCIAL, PTIMessageFieldType.ASCII, SIZE_NOMBRE_RAZON_SOCIAL ),
			new Field( 2, PRIMER_APELLIDO, PTIMessageFieldType.ASCII, SIZE_PRIMER_APELLIDO ),
			new Field( 3, SEGUNDO_APELLIDO, PTIMessageFieldType.ASCII, SIZE_SEGUNDO_APELLIDO ),
			new Field( 4, IDENTIFICACION, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION ),
			new Field( 5, TIPO_DE_IDENTIFICACION, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_IDENTIFICACION ),
			new Field( 6, INDICADOR_PERSONA_FISICA_JURIDICA, PTIMessageFieldType.ASCII, SIZE_INDICADOR_PERSONA_FISICA_JURIDICA ),
			new Field( 7, FECHA_NACIMIENTO, PTIMessageFieldType.ASCII, SIZE_FECHA_NACIMIENTO ),
			new Field( 8, PAIS_DE_NACIONALIDAD, PTIMessageFieldType.ASCII, SIZE_PAIS_DE_NACIONALIDAD ),
			new Field( 9, INDICADOR_DE_NACIONALIDAD, PTIMessageFieldType.ASCII, SIZE_INDICADOR_DE_NACIONALIDAD ),
			new Field( 10, TIPO_DE_TITULAR, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_TITULAR ),
			new Field( 11, PCT_DE_PARTICIPACION_EN_PROPIEDAD, PTIMessageFieldType.NUMBER, SIZE_PCT_DE_PARTICIPACION_EN_PROPIEDAD, 2 ),
			new Field( 12, PCT_DE_PARTICIPACION_EN_USUFRUCTO, PTIMessageFieldType.NUMBER, SIZE_PCT_DE_PARTICIPACION_EN_USUFRUCTO, 2 ),
			new Field( 13, CODIGO_SUSCRIPTOR, PTIMessageFieldType.NUMBER, SIZE_CODIGO_SUSCRIPTOR, 0 ),
			new Field( 14, CODIGO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_ERROR )
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02T2s() throws PTIMessageException {
	  super( FIELDS );
	}

	
	public Date getFechaNacimiento(){
	  return this.getAsDate(FECHA_NACIMIENTO);
	}
	
	public void setFechaNacimiento(Date value) throws PTIMessageException{
	  this.set(FECHA_NACIMIENTO, value);
	}
}
