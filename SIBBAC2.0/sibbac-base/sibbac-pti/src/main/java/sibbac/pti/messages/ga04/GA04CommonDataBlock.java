/**
 * 
 */
package sibbac.pti.messages.ga04;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * 
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA04CommonDataBlock extends ARecord implements PTIMessageRecord {

	// -------------------------------------------- La lista de campos.

	/**
	 * Los campos del bloque de datos comunes.
	 */
	public static final String	COLLRPTID						= "COLLRPTID";
	public static final String	EXECUTING_FIRM					= "CLEARING_FIRM";
	public static final String	CLEARING_FIRM					= "CLEARING_FIRM";
	public static final String	POSITION_ACCOUNT				= "POSITION_ACCOUNT";
	public static final String	SYMBOL							= "SYMBOL";
	public static final String	SECURITY_ID						= "SECURITY_ID";
	public static final String	INSTRREGISTRY					= "INSTRREGISTRY";
	public static final String	NOMINAL							= "NOMINAL";
	public static final String	CURRENCY						= "CURRENCY";
	public static final String	NUMERO_DE_REGISTROS_R00			= "NUMERO_DE_REGISTROS_R00";

	public static final int		SIZE_COLLRPTID					= 20;
	public static final int		SIZE_EXECUTING_FIRM				= 4;
	public static final int		SIZE_CLEARING_FIRM				= 4;
	public static final int		SIZE_POSITION_ACCOUNT			= 3;
	public static final int		SIZE_SYMBOL						= 5;
	public static final int		SIZE_SECURITY_ID				= 12;
	public static final int		SIZE_INSTRREGISTRY				= 1;
	public static final int		SIZE_NOMINAL					= 18;
	public static final int		SIZE_CURRENCY					= 3;
	public static final int		SIZE_NUMERO_DE_REGISTROS_R00	= 9;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS							= {
			new Field( 1, COLLRPTID, PTIMessageFieldType.ASCII, SIZE_COLLRPTID ),
			new Field( 2, EXECUTING_FIRM, PTIMessageFieldType.ASCII, SIZE_EXECUTING_FIRM ),
			new Field( 3, CLEARING_FIRM, PTIMessageFieldType.ASCII, SIZE_CLEARING_FIRM ),
			new Field( 4, POSITION_ACCOUNT, PTIMessageFieldType.ASCII, SIZE_POSITION_ACCOUNT ),
			new Field( 5, SYMBOL, PTIMessageFieldType.ASCII, SIZE_SYMBOL ),
			new Field( 6, SECURITY_ID, PTIMessageFieldType.ASCII, SIZE_SECURITY_ID ),
			new Field( 7, INSTRREGISTRY, PTIMessageFieldType.ASCII, SIZE_INSTRREGISTRY ),
			new Field( 8, NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NOMINAL, 6 ),
			new Field( 8, CURRENCY, PTIMessageFieldType.ASCII, SIZE_CURRENCY ),
			new Field( 10, NUMERO_DE_REGISTROS_R00, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_REGISTROS_R00, 0 )
																};

	/**
	 * Constructor por defecto.
	 */
	public GA04CommonDataBlock() throws PTIMessageException {
		super( PTIMessageRecordType.COMMON_DATA_BLOCK, FIELDS );
	}

	public String getCollRptID() {
		return this.getAsString( COLLRPTID );
	}

	public String getExecutingFirm() {
		return this.getAsString( EXECUTING_FIRM );
	}

	public String getClearingFirm() {
		return this.getAsString( CLEARING_FIRM );
	}

	public String getPositionAccount() {
		return this.getAsString( POSITION_ACCOUNT );
	}

	public String getSymbol() {
		return this.getAsString( SYMBOL );
	}

	public String getSecurityID() {
		return this.getAsString( SECURITY_ID );
	}

	public String getInstrRegistry() {
		return this.getAsString( INSTRREGISTRY );
	}

	public BigDecimal getNominal() {
		return this.getAsNumber( NOMINAL );
	}

	public String getCurrency() {
		return this.getAsString( CURRENCY );
	}

	public BigDecimal getNumeroRegistrosR00() {
		return this.getAsNumber( NUMERO_DE_REGISTROS_R00 );
	}

	public void setCollRptID( final String valor ) throws PTIMessageException {
		this.set( COLLRPTID, valor );
	}

	public void setExecutingFirm( final String valor ) throws PTIMessageException {
		this.set( EXECUTING_FIRM, valor );
	}

	public void setClearingFirm( final String valor ) throws PTIMessageException {
		this.set( CLEARING_FIRM, valor );
	}

	public void setPositionAccount( final String valor ) throws PTIMessageException {
		this.set( POSITION_ACCOUNT, valor );
	}

	public void setSymbol( final String valor ) throws PTIMessageException {
		this.set( SYMBOL, valor );
	}

	public void setSecurityId( final String valor ) throws PTIMessageException {
		this.set( SECURITY_ID, valor );
	}

	public void setInstrRegistry( final String valor ) throws PTIMessageException {
		this.set( INSTRREGISTRY, valor );
	}

	public void setNominal( final String valor ) throws PTIMessageException {
		this.set( NOMINAL, valor );
	}

	public void setCurrency( final String valor ) throws PTIMessageException {
		this.set( CURRENCY, valor );
	}

	public void setNumeroRegistrosR00( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_REGISTROS_R00, valor );
	}

}
