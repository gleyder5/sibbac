package sibbac.pti.messages.mo;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R04 extends ARecord {

  // -------------------------------------------- La lista de campos.
  // Detalle
  public static final String NUMERO_OPERACION = "NUMERO_OPERACION";
  public static final String PRECIO_OPERACION = "PRECIO_OPERACION";
  public static final String NUM_VALORES_IMPORTE_NOMINAL = "NUM_VALORES_IMPORTE_NOMINAL";
  public static final String CORRETAJE = "CORRETAJE";
  public static final String NUMERO_OPERACION_NUEVO = "NUMERO_OPERACION_NUEVO";

  public static final String IDENTIFICACION_DE_LA_PLATAFORMA = "IDENTIFICACION_DE_LA_PLATAFORMA";
  public static final String SEGMENTO = "SEGMENTO";
  public static final String FECHA_NEGOCIACION = "FECHA_NEGOCIACION";
  public static final String EJECUCION_DE_MERCADO = "EJECUCION_DE_MERCADO";
  public static final String CODIGO_DE_OPERACION_MERCADO = "CODIGO_DE_OPERACION_MERCADO";
  public static final String MIEMBRO_MERCADO = "MIEMBRO_MERCADO";
  public static final String USUARIO = "USUARIO";
  public static final String VALORES_IMPORTE_NOMINAL = "VALORES_IMPORTE_NOMINAL";

  public static final String FECHA_DE_LA_ORDEN_DE_MERCADO = "FECHA_DE_LA_ORDEN_DE_MERCADO";
  public static final String NUMERO_DE_LA_ORDEN_DE_MERCADO = "NUMERO_DE_LA_ORDEN_DE_MERCADO";
  public static final String REFERENCIA_DE_CLIENTE = "REFERENCIA_DE_CLIENTE";
  public static final String REFERENCIA_EXTERNA = "REFERENCIA_EXTERNA";
  public static final String INDICADOR_CAPACIDAD = "INDICADOR_CAPACIDAD";

  public static final int SIZE_NUMERO_OPERACION = 16;
  public static final int SIZE_PRECIO_OPERACION = 13;
  public static final int SIZE_NUM_VALORES_IMPORTE_NOMINAL = 18;
  public static final int SIZE_CORRETAJE = 16;
  public static final int SIZE_NUMERO_OPERACION_NUEVO = 16;

  public static final int SIZE_IDENTIFICACION_DE_LA_PLATAFORMA = 4;
  public static final int SIZE_SEGMENTO = 2;
  public static final int SIZE_FECHA_NEGOCIACION = 8;
  public static final int SIZE_EJECUCION_DE_MERCADO = 35;
  public static final int SIZE_CODIGO_DE_OPERACION_MERCADO = 4;
  public static final int SIZE_MIEMBRO_MERCADO = 11;
  public static final int SIZE_USUARIO = 3;
  public static final int SIZE_VALORES_IMPORTE_NOMINAL = 18;
  public static final int SIZE_FECHA_DE_LA_ORDEN_DE_MERCADO = 8;
  public static final int SIZE_NUMERO_DE_LA_ORDEN_DE_MERCADO = 9;
  public static final int SIZE_REFERENCIA_DE_CLIENTE = 16;
  public static final int SIZE_REFERENCIA_EXTERNA = 15;
  public static final int SIZE_INDICADOR_CAPACIDAD = 1;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = {
      new Field(1, NUMERO_OPERACION, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION),
      new Field(2, PRECIO_OPERACION, PTIMessageFieldType.NUMBER, SIZE_PRECIO_OPERACION, 6),
      new Field(3, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, 6),
      new Field(4, CORRETAJE, PTIMessageFieldType.SIGNED, SIZE_CORRETAJE, 13, 2),
      new Field(5, NUMERO_OPERACION_NUEVO, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION_NUEVO),
      new Field(6, IDENTIFICACION_DE_LA_PLATAFORMA, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION_DE_LA_PLATAFORMA),
      new Field(7, SEGMENTO, PTIMessageFieldType.ASCII, SIZE_SEGMENTO),
      new Field(8, FECHA_NEGOCIACION, PTIMessageFieldType.ASCII, SIZE_FECHA_NEGOCIACION),
      new Field(9, EJECUCION_DE_MERCADO, PTIMessageFieldType.NUMBER, SIZE_EJECUCION_DE_MERCADO),
      new Field(10, CODIGO_DE_OPERACION_MERCADO, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_OPERACION_MERCADO),
      new Field(11, MIEMBRO_MERCADO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_MERCADO),
      new Field(12, USUARIO, PTIMessageFieldType.ASCII, SIZE_USUARIO),
      new Field(13, VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_VALORES_IMPORTE_NOMINAL, 6),
      new Field(14, FECHA_DE_LA_ORDEN_DE_MERCADO, PTIMessageFieldType.ASCII, SIZE_FECHA_DE_LA_ORDEN_DE_MERCADO),
      new Field(15, NUMERO_DE_LA_ORDEN_DE_MERCADO, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_LA_ORDEN_DE_MERCADO),
      new Field(16, REFERENCIA_DE_CLIENTE, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_DE_CLIENTE),
      new Field(17, REFERENCIA_EXTERNA, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_EXTERNA),
      new Field(18, INDICADOR_CAPACIDAD, PTIMessageFieldType.ASCII, SIZE_INDICADOR_CAPACIDAD) };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public R04() throws PTIMessageException {
    super(PTIMessageRecordType.R04, FIELDS);
  }

  public String getNumeroOperacion() {
    return this.getAsString(NUMERO_OPERACION);
  }

  public BigDecimal getPrecioOperacion() {
    return this.getAsNumber(PRECIO_OPERACION);
  }

  public BigDecimal getNumValoresImporteNominal() {
    return this.getAsNumber(NUM_VALORES_IMPORTE_NOMINAL);
  }

  public BigDecimal getCorretaje() {
    return this.getAsNumber(CORRETAJE);
  }

  public String getNumeroOperacionNuevo() {
    return this.getAsString(NUMERO_OPERACION_NUEVO);
  }

  public String getIdentificacionDeLaPlataforma() {
    return this.getAsString(IDENTIFICACION_DE_LA_PLATAFORMA);
  }

  public String getSegmento() {
    return this.getAsString(SEGMENTO);
  }

  public Date getFechaNegociacion() {
    return this.getAsDate(FECHA_NEGOCIACION);
  }

  public BigDecimal getEjecucionDeMercado() {
    return this.getAsNumber(EJECUCION_DE_MERCADO);
  }

  public String getCodigoDeOperacionMercado() {
    return this.getAsString(CODIGO_DE_OPERACION_MERCADO);
  }

  public String getMiembroMercado() {
    return this.getAsString(MIEMBRO_MERCADO);
  }

  public BigDecimal getValoresImporteNominal() {
    return this.getAsNumber(VALORES_IMPORTE_NOMINAL);
  }

  public Date getFechaDeLaOrdenDeMercado() {
    return this.getAsDate(FECHA_DE_LA_ORDEN_DE_MERCADO);
  }

  public BigDecimal getNumerDeLaOrdenDeMercado() {
    return this.getAsNumber(NUMERO_DE_LA_ORDEN_DE_MERCADO);
  }

  public String getUsuario() {
    return this.getAsString(USUARIO);
  }

  public String getReferenciaCliente() {
    return this.getAsString(REFERENCIA_DE_CLIENTE);
  }

  public String getReferenciaExternaDeLaOrden() {
    return this.getAsString(REFERENCIA_EXTERNA);
  }

  public String getIndicadorCapacidad() {
    return this.getAsString(INDICADOR_CAPACIDAD);
  }

  public void setNumeroOperacion(final String valor) throws PTIMessageException {
    this.set(NUMERO_OPERACION, valor);
  }

  public void setPrecioOperacion(final String valor) throws PTIMessageException {
    this.set(PRECIO_OPERACION, valor);
  }

  public void setNumValoresImporteNominal(final String valor) throws PTIMessageException {
    this.set(NUM_VALORES_IMPORTE_NOMINAL, valor);
  }

  public void setCorretaje(final String valor) throws PTIMessageException {
    this.set(CORRETAJE, valor);
  }

  public void setNumOperacionNuevo(final String valor) throws PTIMessageException {
    this.set(NUMERO_OPERACION_NUEVO, valor);
  }

  public void setIdentificacionDeLaPlataforma(final String valor) throws PTIMessageException {
    this.set(IDENTIFICACION_DE_LA_PLATAFORMA, valor);
  }

  public void setSegmento(final String valor) throws PTIMessageException {
    this.set(SEGMENTO, valor);
  }

  public void setFechaNegociacion(final String valor) throws PTIMessageException {
    this.set(FECHA_NEGOCIACION, valor);
  }

  public void setEjecucionDeMercado(final String valor) throws PTIMessageException {
    this.set(EJECUCION_DE_MERCADO, valor);
  }

  public void setCodigoDeOperacionMercado(final String valor) throws PTIMessageException {
    this.set(CODIGO_DE_OPERACION_MERCADO, valor);
  }

  public void setMiembroMercado(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_MERCADO, valor);
  }

  public void setValoresImporteNominal(final String valor) throws PTIMessageException {
    this.set(VALORES_IMPORTE_NOMINAL, valor);
  }

  public void setFechaDeLaOrdenDeMercado(final String valor) throws PTIMessageException {
    this.set(FECHA_DE_LA_ORDEN_DE_MERCADO, valor);
  }

  public void setNumerDeLaOrdenDeMercado(final String valor) throws PTIMessageException {
    this.set(NUMERO_DE_LA_ORDEN_DE_MERCADO, valor);
  }

  public void setUsuario(final String valor) throws PTIMessageException {
    this.set(USUARIO, valor);
  }

  public void setReferenciaCliente(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_DE_CLIENTE, valor);
  }

  public void setReferenciaExternaDeLaOrden(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_EXTERNA, valor);
  }

  public void setIndicadorCapacidad(final String valor) throws PTIMessageException {
    this.set(INDICADOR_CAPACIDAD, valor);
  }

}
