package sibbac.pti.messages.mo;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	REF_NOTIFICACION								= "RE_NOTIFICACION";
	public static final String	REFERENCIA_MOVIMIENTO							= "REFERENCIA_MOVIMIENTO";
	public static final String	REFERENCIA_MOVIMIENTO_ECC						= "REFERENCIA_MOVIMIENTO_ECC";
	public static final String	MIEMBRO_ORIGEN									= "MIEMBRO_ORIGEN";
	public static final String	USUARIO_ORIGEN									= "USUARIO_ORIGEN";
	public static final String	REFERENCIA_ASIGNACION							= "REFERENCIA_ASIGNACION";
	public static final String	MNEMOTECNICO									= "MNEMOTECNICO";
	public static final String	MIEMBRO_DESTINO									= "MIEMBRO_DESTINO";
	public static final String	USUARIO_DESTINO									= "USUARIO_DESTINO";
	public static final String	MIEMBRO_COMPENSADOR_DESTINO						= "MIEMBRO_COMPENSADOR_DESTINO";
	public static final String	TIPO_MOVIMIENTO									= "TIPO_MOVIMIENTO";
	public static final String	ESTADO											= "ESTADO";
	public static final String	CUENTA_DE_COMPENSACION_DESTINO					= "CUENTA_DE_COMPENSACION_DESTINO";
	public static final String	ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL		= "ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL";
	public static final String	PRECIO											= "PRECIO";
	public static final String	EFECTIVO										= "EFECTIVO";
	public static final String	CODIGO_VALOR									= "CODIGO_VALOR";

	public static final int		SIZE_REF_NOTIFICACION							= 9;
	public static final int		SIZE_REFERENCIA_MOVIMIENTO						= 10;
	public static final int		SIZE_REFERENCIA_MOVIMIENTO_ECC					= 10;
	public static final int		SIZE_MIEMBRO_ORIGEN								= 4;
	public static final int		SIZE_USUARIO_ORIGEN								= 3;
	public static final int		SIZE_REFERENCIA_ASIGNACION						= 18;
	public static final int		SIZE_MNEMOTECNICO								= 10;
	public static final int		SIZE_MIEMBRO_DESTINO							= 4;
	public static final int		SIZE_USUARIO_DESTINO							= 3;
	public static final int		SIZE_MIEMBRO_COMPENSADOR_DESTINO				= 4;
	public static final int		SIZE_TIPO_MOVIMIENTO							= 2;
	public static final int		SIZE_ESTADO										= 2;
	public static final int		SIZE_CUENTA_DE_COMPENSACION_DESTINO				= 3;
	public static final int		SIZE_ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL	= 18;
	public static final int		SIZE_PRECIO										= 13;
	public static final int		SIZE_EFECTIVO									= 15;
	public static final int		SIZE_CODIGO_VALOR								= 12;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS											= {
			new Field( 1, REF_NOTIFICACION, PTIMessageFieldType.ASCII, SIZE_REF_NOTIFICACION ),
			new Field( 2, REFERENCIA_MOVIMIENTO, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO ),
			new Field( 3, REFERENCIA_MOVIMIENTO_ECC, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO_ECC ),
			new Field( 4, MIEMBRO_ORIGEN, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_ORIGEN ),
			new Field( 5, USUARIO_ORIGEN, PTIMessageFieldType.ASCII, SIZE_USUARIO_ORIGEN ),
			new Field( 6, REFERENCIA_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_ASIGNACION ),
			new Field( 7, MNEMOTECNICO, PTIMessageFieldType.ASCII, SIZE_MNEMOTECNICO ),
			new Field( 8, MIEMBRO_DESTINO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_DESTINO ),
			new Field( 9, USUARIO_DESTINO, PTIMessageFieldType.ASCII, SIZE_USUARIO_DESTINO ),
			new Field( 10, MIEMBRO_COMPENSADOR_DESTINO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_COMPENSADOR_DESTINO ),
			new Field( 11, TIPO_MOVIMIENTO, PTIMessageFieldType.ASCII, SIZE_TIPO_MOVIMIENTO ),
			new Field( 12, ESTADO, PTIMessageFieldType.ASCII, SIZE_ESTADO ),
			new Field( 13, CUENTA_DE_COMPENSACION_DESTINO, PTIMessageFieldType.ASCII, SIZE_CUENTA_DE_COMPENSACION_DESTINO ),
			new Field( 14, ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER,
					SIZE_ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 15, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6 ),
			new Field( 16, EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_EFECTIVO, 2 ),
			new Field( 17, CODIGO_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_VALOR ),
																				};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R03() throws PTIMessageException {
		super( PTIMessageRecordType.R03, FIELDS );
	}

	public String getRefNotificacion() {
		return this.getAsString( REF_NOTIFICACION );
	}

	public String getReferenciaMovimiento() {
		return this.getAsString( REFERENCIA_MOVIMIENTO );
	}

	public String getReferenciaMovimientoEcc() {
		return this.getAsString( REFERENCIA_MOVIMIENTO_ECC );
	}

	public String getMiembroOrigen() {
		return this.getAsString( MIEMBRO_ORIGEN );
	}

	public String getUsuarioOrigen() {
		return this.getAsString( USUARIO_ORIGEN );
	}

	public String getReferenciaAsignacion() {
		return this.getAsString( REFERENCIA_ASIGNACION );
	}

	public String getMnemotecnico() {
		return this.getAsString( MNEMOTECNICO );
	}

	public String getMiembroDestino() {
		return this.getAsString( MIEMBRO_DESTINO );
	}

	public String getUsuarioDestino() {
		return this.getAsString( USUARIO_DESTINO );
	}

	public String getMiembroCompensadorDestino() {
		return this.getAsString( MIEMBRO_COMPENSADOR_DESTINO );
	}

	public String getTipoMovimiento() {
		return this.getAsString( TIPO_MOVIMIENTO );
	}

	public String getEstado() {
		return this.getAsString( ESTADO );
	}

	public String getCuentaDeCompensacionDestino() {
		return this.getAsString( CUENTA_DE_COMPENSACION_DESTINO );
	}

	public BigDecimal getAcumuladoNumeroValoresImporteNominal() {
		return this.getAsNumber( ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL );
	}

	public BigDecimal getPrecio() {
		return this.getAsNumber( PRECIO );
	}

	public BigDecimal getEfectivo() {
		return this.getAsNumber( EFECTIVO );
	}

	public String getCodigoValor(){
		return this.getAsString( CODIGO_VALOR );
	}

	public void setRefNotificacion( final String valor ) throws PTIMessageException {
		this.set( REF_NOTIFICACION, valor );
	}

	public void setReferenciaMovimiento( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO, valor );
	}

	public void setReferenciaMovimientoEcc( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO_ECC, valor );
	}

	public void setMiembroOrigen( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_ORIGEN, valor );
	}

	public void setUsuarioOrigen( final String valor ) throws PTIMessageException {
		this.set( USUARIO_ORIGEN, valor );
	}

	public void setReferenciaAsignacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_ASIGNACION, valor );
	}

	public void setMnemotecnico( final String valor ) throws PTIMessageException {
		this.set( MNEMOTECNICO, valor );
	}

	public void setMiembroDestino( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_DESTINO, valor );
	}

	public void setUsuarioDestino( final String valor ) throws PTIMessageException {
		this.set( USUARIO_DESTINO, valor );
	}

	public void setMiembroCompensadorDestino( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_COMPENSADOR_DESTINO, valor );
	}

	public void setTipoMovimiento( final String valor ) throws PTIMessageException {
		this.set( TIPO_MOVIMIENTO, valor );
	}

	public void setEstado( final String valor ) throws PTIMessageException {
		this.set( ESTADO, valor );
	}

	public void setCuentaDeCompensacionDestino( final String valor ) throws PTIMessageException {
		this.set( CUENTA_DE_COMPENSACION_DESTINO, valor );
	}

	public void setAcumuladoNumeroValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( ACUMULADO_NUMERO_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setPrecio( final String valor ) throws PTIMessageException {
		this.set( PRECIO, valor );
	}

	public void setEfectivo( final String valor ) throws PTIMessageException {
		this.set( EFECTIVO, valor );
	}	

	public void setCodigoValor( final String valor ) throws PTIMessageException {
		this.set( CODIGO_VALOR, valor );
	}


}
