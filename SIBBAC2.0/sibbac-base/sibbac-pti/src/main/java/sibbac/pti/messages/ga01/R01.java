package sibbac.pti.messages.ga01;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: GA01 (Garantias y Movimientos de
 * Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Collateral Amount Group
	public static final String	COLLATERALTYPE					= "COLLATERALTYPE";
	public static final String	COLLATERALAMOUNT				= "COLLATERALAMOUNT";
	public static final String	COLLATERALAMOUNTMARKETID		= "COLLATERALAMOUNTMARKETID";

	// Tamanyos de los campos.
	public static final int		SIZE_COLLATERALTYPE				= 1;
	public static final int		SIZE_COLLATERALAMOUNT			= 15;
	public static final int		SIZE_COLLATERALAMOUNTMARKETID	= 2;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS							= {
			new Field( 1, COLLATERALTYPE, PTIMessageFieldType.ASCII, SIZE_COLLATERALTYPE ),
			new Field( 2, COLLATERALAMOUNT, PTIMessageFieldType.NUMBER, SIZE_COLLATERALAMOUNT, 2 ),
			new Field( 3, COLLATERALAMOUNTMARKETID, PTIMessageFieldType.ASCII, SIZE_COLLATERALAMOUNTMARKETID )
																};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}

	public String getCollateralType() {
		return this.getAsString( COLLATERALTYPE );
	}

	public BigDecimal getCollateralAmount() {
		return this.getAsNumber( COLLATERALAMOUNT );
	}

	public String getCollateralAmountMarketId() {
		return this.getAsString( COLLATERALAMOUNTMARKETID );
	}

	public void setCollateralType( final String valor ) throws PTIMessageException {
		this.set( COLLATERALTYPE, valor );
	}

	public void setCollateralAmount( final String valor ) throws PTIMessageException {
		this.set( COLLATERALAMOUNT, valor );
	}

	public void setCollateralAmountMarketId( final String valor ) throws PTIMessageException {
		this.set( COLLATERALAMOUNTMARKETID, valor );
	}


}
