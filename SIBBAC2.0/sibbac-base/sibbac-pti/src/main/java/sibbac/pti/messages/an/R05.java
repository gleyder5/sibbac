package sibbac.pti.messages.an;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R05 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R05 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Identificacion Ejecucion
  public static final String IDENTIFICACION_DE_LA_PLATAFORMA = "IDENTIFICACION_DE_LA_PLATAFORMA";
  public static final String FECHA_OPERACION = "FECHA_OPERACION";
  public static final String REFERENCIA_OPERACION_DCV = "REFERENCIA_OPERACION_DCV";
  public static final String ENTIDAD_PARTICIPANTE = "ENTIDAD_PARTICIPANTE";

  // Datos Ejecucion
  public static final String FECHA_DE_CASE = "FECHA_DE_CASE";
  public static final String HORA_DE_CASE = "HORA_DE_CASE";
  public static final String TIPO_DE_OPERACION_PLATAFORMA = "TIPO_DE_OPERACION_PLATAFORMA";
  public static final String CODIGO_DE_VALOR = "CODIGO_DE_VALOR";
  public static final String CUENTA_LIQUIDACION = "CUENTA_LIQUIDACION";
  public static final String INDICADOR_COTIZACION = "INDICADOR_COTIZACION";
  public static final String VALORES_IMPORTE_NOMINAL = "VALORES_IMPORTE_NOMINAL";
  public static final String PRECIO = "PRECIO";
  public static final String IMPORTE_EFECTIVO = "IMPORTE_EFECTIVO";
  public static final String DIVISA = "DIVISA";
  public static final String SENTIDO = "SENTIDO";
  public static final String ORDENANTE_PARTICIPANTE = "ORDENANTE_PARTICIPANTE";
  public static final String CODIGO_PARTICIPANTE = "CODIGO_PARTICIPANTE";
  public static final String CCV = "CCV";
  public static final String FECHA_LIQUIDACION_TEORICA = "FECHA_LIQUIDACION_TEORICA";
  public static final String LIQUIDACION_PARCIAL = "LIQUIDACION_PARCIAL";
  public static final String LIQUIDACION_TIEMPO_REAL = "LIQUIDACION_TIEMPO_REAL";
  public static final String IND_INTERVENCION_ECC = "IND_INTERVENCION_ECC";
  public static final String MIEMBRO_COMPENSADOR = "MIEMBRO_COMPENSADOR";
  public static final String CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV = "CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV";
  public static final String CODIGO_DCV = "CODIGO_DCV";

  // Tamanyos de los campos.
  public static final int SIZE_IDENTIFICACION_DE_LA_PLATAFORMA = 11;
  public static final int SIZE_FECHA_OPERACION = 8;
  public static final int SIZE_REFERENCIA_OPERACION_DCV = 35;
  public static final int SIZE_ENTIDAD_PARTICIPANTE = 11;
  public static final int SIZE_FECHA_DE_CASE = 8;
  public static final int SIZE_HORA_DE_CASE = 9;
  public static final int SIZE_TIPO_DE_OPERACION_PLATAFORMA = 4;
  public static final int SIZE_CODIGO_DE_VALOR = 12;
  public static final int SIZE_CUENTA_LIQUIDACION = 35;
  public static final int SIZE_INDICADOR_COTIZACION = 1;
  public static final int SIZE_VALORES_IMPORTE_NOMINAL = 18;
  public static final int SIZE_PRECIO = 13;
  public static final int SIZE_IMPORTE_EFECTIVO = 15;
  public static final int SIZE_DIVISA = 3;
  public static final int SIZE_SENTIDO = 1;
  public static final int SIZE_ORDENANTE_PARTICIPANTE = 35;
  public static final int SIZE_CODIGO_PARTICIPANTE = 42;
  public static final int SIZE_CCV = 35;
  public static final int SIZE_FECHA_LIQUIDACION_TEORICA = 8;
  public static final int SIZE_LIQUIDACION_PARCIAL = 4;
  public static final int SIZE_LIQUIDACION_TIEMPO_REAL = 4;
  public static final int SIZE_IND_INTERVENCION_ECC = 1;
  public static final int SIZE_MIEMBRO_COMPENSADOR = 11;
  public static final int SIZE_CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV = 35;
  public static final int SIZE_CODIGO_DCV = 11;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = {
      new Field(1, IDENTIFICACION_DE_LA_PLATAFORMA, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION_DE_LA_PLATAFORMA),
      new Field(2, FECHA_OPERACION, PTIMessageFieldType.ASCII, SIZE_FECHA_OPERACION),
      new Field(3, REFERENCIA_OPERACION_DCV, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_OPERACION_DCV),
      new Field(4, ENTIDAD_PARTICIPANTE, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_PARTICIPANTE),
      new Field(5, FECHA_DE_CASE, PTIMessageFieldType.ASCII, SIZE_FECHA_DE_CASE),
      new Field(6, HORA_DE_CASE, PTIMessageFieldType.ASCII, SIZE_HORA_DE_CASE),
      new Field(7, TIPO_DE_OPERACION_PLATAFORMA, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_OPERACION_PLATAFORMA),
      new Field(8, CODIGO_DE_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_VALOR),
      new Field(9, CUENTA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_LIQUIDACION),
      new Field(10, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION, 6),
      new Field(11, VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_VALORES_IMPORTE_NOMINAL, 6),
      new Field(12, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6),
      new Field(13, IMPORTE_EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_IMPORTE_EFECTIVO, 2),
      new Field(14, DIVISA, PTIMessageFieldType.ASCII, SIZE_DIVISA),
      new Field(15, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO),
      new Field(16, ORDENANTE_PARTICIPANTE, PTIMessageFieldType.ASCII, SIZE_ORDENANTE_PARTICIPANTE),
      new Field(17, CODIGO_PARTICIPANTE, PTIMessageFieldType.ASCII, SIZE_CODIGO_PARTICIPANTE),
      new Field(18, CCV, PTIMessageFieldType.ASCII, SIZE_CCV),
      new Field(19, FECHA_LIQUIDACION_TEORICA, PTIMessageFieldType.ASCII, SIZE_FECHA_LIQUIDACION_TEORICA),
      new Field(20, LIQUIDACION_PARCIAL, PTIMessageFieldType.ASCII, SIZE_LIQUIDACION_PARCIAL),
      new Field(21, LIQUIDACION_TIEMPO_REAL, PTIMessageFieldType.ASCII, SIZE_LIQUIDACION_TIEMPO_REAL),
      new Field(22, IND_INTERVENCION_ECC, PTIMessageFieldType.ASCII, SIZE_IND_INTERVENCION_ECC),
      new Field(23, MIEMBRO_COMPENSADOR, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_COMPENSADOR),
      new Field(24, CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV, PTIMessageFieldType.ASCII,
          SIZE_CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV),
      new Field(25, CODIGO_DCV, PTIMessageFieldType.ASCII, SIZE_CODIGO_DCV) };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la
   * cadena de texto.
   */
  public R05() throws PTIMessageException {
    super(PTIMessageRecordType.R05, FIELDS);
  }

  public String getIdentificacionDeLaPlataforma() {
    return this.getAsString(IDENTIFICACION_DE_LA_PLATAFORMA);
  }

  public Date getFechaOperacion() {
    return this.getAsDate(FECHA_OPERACION);
  }

  public String getReferenciaOperacionDCV() {
    return this.getAsString(REFERENCIA_OPERACION_DCV);
  }

  public String getEntidadParticipante() {
    return this.getAsString(ENTIDAD_PARTICIPANTE);
  }

  public Date getFechaDeCase() {
    return this.getAsDate(FECHA_DE_CASE);
  }

  public Date getHoraDeCase() {
    return this.getAsTime(HORA_DE_CASE);
  }

  public String getTipoDeOperacionPlataforma() {
    return this.getAsString(TIPO_DE_OPERACION_PLATAFORMA);
  }

  public String getCodigoDeValor() {
    return this.getAsString(CODIGO_DE_VALOR);
  }

  public String getCuentaLiquidacion() {
    return this.getAsString(CUENTA_LIQUIDACION);
  }

  public String getIndicadorCotizacion() {
    return this.getAsString(INDICADOR_COTIZACION);
  }

  public BigDecimal getValoresImporteNominal() {
    return this.getAsNumber(VALORES_IMPORTE_NOMINAL);
  }

  public BigDecimal getPrecio() {
    return this.getAsNumber(PRECIO);
  }

  public BigDecimal getImporteEfectivo() {
    return this.getAsNumber(IMPORTE_EFECTIVO);
  }

  public String getDivisa() {
    return this.getAsString(DIVISA);
  }

  public String getSentido() {
    return this.getAsString(SENTIDO);
  }

  public String getOrdenanteParticipante() {
    return this.getAsString(ORDENANTE_PARTICIPANTE);
  }

  public String getCodigoParticipante() {
    return this.getAsString(CODIGO_PARTICIPANTE);
  }

  public String getCCV() {
    return this.getAsString(CCV);
  }

  public Date getFechaLiquidacionTeorica() {
    return this.getAsDate(FECHA_LIQUIDACION_TEORICA);
  }

  public String getLiquidacionParcial() {
    return this.getAsString(LIQUIDACION_PARCIAL);
  }

  public String getLiquidacionTiempoReal() {
    return this.getAsString(LIQUIDACION_TIEMPO_REAL);
  }

  public String getIndIntervencionEcc() {
    return this.getAsString(IND_INTERVENCION_ECC);
  }

  public String getMiembroCompensador() {
    return this.getAsString(MIEMBRO_COMPENSADOR);
  }

  public String getCuentaCompensacionComunicadaDCV() {
    return this.getAsString(CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV);
  }

  public String getCodigoDCV() {
    return this.getAsString(CODIGO_DCV);
  }

  public void setIdentificacionDeLaPlataforma(final String valor) throws PTIMessageException {
    this.set(IDENTIFICACION_DE_LA_PLATAFORMA, valor);
  }

  public void setFechaOperacion(final String valor) throws PTIMessageException {
    this.set(FECHA_OPERACION, valor);
  }

  public void setReferenciaOperacionDCV(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_OPERACION_DCV, valor);
  }

  public void setEntidadParticipante(final String valor) throws PTIMessageException {
    this.set(ENTIDAD_PARTICIPANTE, valor);
  }

  public void setFechaDeCase(final Date valor) throws PTIMessageException {
    this.set(FECHA_DE_CASE, valor);
  }

  public void setHoraDeCase(final Date valor) throws PTIMessageException {
    this.set(HORA_DE_CASE, valor);
  }

  public void setTipoDeOperacionPlataforma(final String valor) throws PTIMessageException {
    this.set(TIPO_DE_OPERACION_PLATAFORMA, valor);
  }

  public void setCodigoDeValor(final String valor) throws PTIMessageException {
    this.set(CODIGO_DE_VALOR, valor);
  }

  public void setCuentaLiquidacion(final String valor) throws PTIMessageException {
    this.set(CUENTA_LIQUIDACION, valor);
  }

  public void setIndicadorCotizacion(final String valor) throws PTIMessageException {
    this.set(INDICADOR_COTIZACION, valor);
  }

  public void setValoresImporteNominal(final String valor) throws PTIMessageException {
    this.set(VALORES_IMPORTE_NOMINAL, valor);
  }

  public void setPrecio(final String valor) throws PTIMessageException {
    this.set(PRECIO, valor);
  }

  public void setImporteEfectivo(final String valor) throws PTIMessageException {
    this.set(IMPORTE_EFECTIVO, valor);
  }

  public void setDivisa(final String valor) throws PTIMessageException {
    this.set(DIVISA, valor);
  }

  public void setSentido(final String valor) throws PTIMessageException {
    this.set(SENTIDO, valor);
  }

  public void setOrdenanteParticipante(final String valor) throws PTIMessageException {
    this.set(ORDENANTE_PARTICIPANTE, valor);
  }

  public void setCodigoParticipante(final String valor) throws PTIMessageException {
    this.set(CODIGO_PARTICIPANTE, valor);
  }

  public void setCCV(final String valor) throws PTIMessageException {
    this.set(CCV, valor);
  }

  public void setFechaLiquidacionTeorica(final String valor) throws PTIMessageException {
    this.set(FECHA_LIQUIDACION_TEORICA, valor);
  }

  public void setLiquidacionParcial(final String valor) throws PTIMessageException {
    this.set(LIQUIDACION_PARCIAL, valor);
  }

  public void setLiquidacionTiempoReal(final String valor) throws PTIMessageException {
    this.set(LIQUIDACION_TIEMPO_REAL, valor);
  }

  public void setIndIntervencionECC(final String valor) throws PTIMessageException {
    this.set(IND_INTERVENCION_ECC, valor);
  }

  public void setMiembroCompensador(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_COMPENSADOR, valor);
  }

  public void setCuentaCompensacionComunicadaDCV(final String valor) throws PTIMessageException {
    this.set(CUENTA_DE_COMPENSACION_COMUNICADA_POR_EL_DCV, valor);
  }

  public void setCodigoDCV(final String valor) throws PTIMessageException {
    this.set(CODIGO_DCV, valor);
  }

}
