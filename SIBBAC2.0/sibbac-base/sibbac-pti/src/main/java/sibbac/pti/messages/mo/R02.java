package sibbac.pti.messages.mo;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	FECHA_DE_LA_ORDEN		= "FECHA_DE_LA_ORDEN";
	public static final String	NUMERO_DE_LA_ORDEN		= "NUMERO_DE_LA_ORDEN";

	public static final int		SIZE_FECHA_DE_LA_ORDEN	= 8;
	public static final int		SIZE_NUMERO_DE_LA_ORDEN	= 9;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS					= {
			new Field( 1, FECHA_DE_LA_ORDEN, PTIMessageFieldType.ASCII, SIZE_FECHA_DE_LA_ORDEN ),
			new Field( 2, NUMERO_DE_LA_ORDEN, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_LA_ORDEN )
														};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02() throws PTIMessageException {
		super( PTIMessageRecordType.R02, FIELDS );
	}

	public Date getFechaDeLaOrden() {
		return this.getAsDate( FECHA_DE_LA_ORDEN );
	}

	public BigDecimal getNumerDeLaOrden() {
		return this.getAsNumber( NUMERO_DE_LA_ORDEN );
	}

	public void setFechaDeLaOrden( final String valor ) throws PTIMessageException {
		this.set( FECHA_DE_LA_ORDEN, valor );
	}

	public void setNumerDeLaOrden( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_LA_ORDEN, valor );
	}


}
