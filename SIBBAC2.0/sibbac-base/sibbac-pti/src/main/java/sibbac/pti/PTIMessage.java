package sibbac.pti;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import sibbac.pti.messages.AMessage;
/**
 * Interfaz que describe un mensaje PTI.
 * <p/>
 * Este interfaz incluye las operativas comunes a todos y cualesquiera de los mensajes PTI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTIMessage extends PTICommons, PTIConstants {

  // --------------------------------------------------------- General

  /**
   * Metodo de conveniencia para determinar si un mensaje es un parsing de un mensaje PTI recibido o es una instancia
   * creada internamente.<br/>
   * Este dato servira para efectuar o no validaciones internas.
   * 
   * @return TRUE o FALSE.
   */
  public boolean isFromPTI();

  /**
   * Metodo de conveniencia para establecer si un mensaje es un parsing de un mensaje PTI recibido o es una instancia
   * creada internamente.<br/>
   * Este dato servira para efectuar o no validaciones internas.
   * 
   * @param fromPTI TRUE o FALSE.
   */
  public void setFromPTI(final boolean fromPTI);

  /**
   * Setter para establecer la cabecera del mensaje.
   * 
   * @param header Un {@link PTIMessageHeader} con la informacion basica del mensaje.
   */
  public void setHeader(final PTIMessageHeader header);

  /**
   * Metodo que devuelve la cabecera estandar de un mensaje.
   * 
   * @return Una instancia de {@lnk PTIMessageHeader} con la informacion de cabecera.
   */
  public PTIMessageHeader getHeader();

  /**
   * Metodo de conveniencia para determinar si un mensaje tiene declarado un bloque de datos comunes o no.
   * 
   * @return TRUE o FALSE.
   */
  public boolean hasCommonDataBlock();

  /**
   * Metodo que devuelve el bloque de datos comunes del mensaje,
   * 
   * @return Un {@link PTIMessageRecord} con el bloque de datos comunes del mensaje.
   */
  public PTIMessageRecord getCommonDataBlock();

  /**
   * Metodo que devuelve el bloque de control de datos del mensaje,
   * 
   * @return Un {@link PTIMessageRecord} con el bloque de control de datos del mensaje.
   */
  public PTIMessageRecord getControlBlock();

  /**
   * Metodo de conveniencia para determinar si un mensaje tiene declarado un bloque de control o no.
   * 
   * @return TRUE o FALSE.
   */
  public boolean hasControlBlock();

  /**
   * Metodo de conveniencia para determinar si un mensaje contiene registros de un tipo dado.
   * 
   * @return TRUE o FALSE.
   */
  public boolean hasRecords(final PTIMessageRecordType type);

  /**
   * Metodo interno para parsear una cadena de texto.
   * 
   * @param texto El {@link String} a parsear.
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public void parseMessage(final String texto) throws PTIMessageException;

  /**
   * Permite conocer la longitud total del mensaje.
   * 
   * @return Un entero.
   */
  public int getLongitud();

  /**
   * Metodo para persistir mensajes en base de datos.
   * 
   * @throws PTIMessageException Si ocurre algun error persistiendo en base de datos.
   */
  public void persist() throws PTIMessageException;

  // ----------------------------------------------------------- Lotes

  /**
   * Metodo que permite conocer si este mensaje ha llegado por la cola BATCH, como parte de un lote.
   * 
   * @return TRUE o FALSE, si el mensaje forma parte de un lote.
   */
  public boolean isParteDeUnLote();

  /**
   * Metodo que permite saber si, en caso de proceder de un lote de comprobacion, dicho mensaje ha isdo ya previamente
   * persistido en base de datos o no.
   * 
   * @return TRUE o FALSE, si el mensaje ya esta almacenado en la base de datos.
   */
  public boolean isPersisted();

  /**
   * Devuelve el ID de correlacion del lote.
   * 
   * @return Un {@link String} con el dato.
   */
  public String getIdCorrelacion();

  /**
   * Devuelve el ID del lote.
   * 
   * @return Un {@link String} con el dato.
   */
  public String getIdLote();

  /**
   * Devuelve el orden del mensaje dentro del lote.
   * 
   * @return Un entero con la posicion.
   */
  public Integer getOrdenEnLote();

  /**
   * Devuelve si el mensaje ha sido ya procesado internamente en SIBBAC.
   * 
   * @return TRUE si ha sido procesado o FALSE en caso contrario.
   */
  public boolean isProcessed();

  /**
   * Devuelve si el mensaje ha sido ya contrastado con la informacion de fin de dia de BME.
   * 
   * @return TRUE si ha sido contrastado con BME o FALSE en caso contrario.
   */
  public boolean isVerifiedBME();

  /**
   * Permite establecer el ID de correlacion del lote.
   * 
   * @param idCorrelacion Un {@link String} con el dato.
   */
  public void setIdCorrelacion(final String idCorrelacion);

  /**
   * Permite establecer el ID del lote.
   * 
   * @param idLote Un {@link String} con el dato.
   */
  public void setIdLote(final String idLote);

  /**
   * Permite establecer el orden del mensaje dentro del lote.
   * 
   * @param orden Un entero con el dato.
   */
  public void setOrdenEnLote(final Integer orden);

  /**
   * Permite establecer que el mensaje ha sido procesado internamente en SIBBAC.
   * 
   * @param processed TRUE o FALSE.
   */
  public void setProcessed(final boolean processed);

  /**
   * Permite establecer que el mensaje ha sido contrastado con la informacion de fin de dia desde BME.
   * 
   * @param verified TRUE o FALSE.
   */
  public void setVerifiedBME(final boolean verified);

  /**
   * Permite saber si un msg es el ultimo de un lote
   * */
  public void setLastInGroup(final boolean isLast);

  /**
   * Permite saber si un msg es el ultimo de un lote
   * */
  public void setInGroup(boolean isInGroup);

  // --------------------------------------------------------- Setters

  /**
   * Metodo de conveniencia para establecer la fecha y hora de envio del mensaje.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public void setFechaDeEnvio() throws PTIMessageException;

  /**
   * Metodo de conveniencia para recuperar la fecha y hora de envio del mensaje.
   * 
   * @return Una instancia de {@link Date} con la fecha de envio del mensaje.
   * @throws PTIMessageException Si hay errores de parseo de fechas/horas.
   */
  public Date getFechaDeEnvio() throws PTIMessageException;

  /**
   * Metodo de conveniencia para recuperar la fecha y hora de envio del mensaje.
   * 
   * @return Una instancia de {@link Date} con la fecha de envio del mensaje.
   * @throws PTIMessageException Si hay errores de parseo de fechas/horas.
   */
  public Time getHoraDeEnvio() throws PTIMessageException;

  /**
   * Metodo que permite conocer la referencia ECC asociada al mensaje, si la hubiera.<br/>
   * Este metodo habra de ser implementado por cada mensaje sobreescribiendo el existente en {@link AMessage} que, por
   * defecto, devuelve un NULL.
   * 
   * @return Un {@link String} con la referencia ECC.
   */
  public String getReferenciaECC();

  // --------------------------------------------------------- Getters

  /**
   * Metodo que permite conocer el {@link PTIMessageType tipo} de mensaje PTI.
   * 
   * @return Una instancia de {@link PTIMessageType tipo}.
   */
  public PTIMessageType getTipo();

  /**
   * Metodo que permite conocer el error general del mensaje, dato que se recibe en la {@link PTIMessageHeader cabecera}
   * /(header) del mismo.
   * 
   * @return Un {@link String} con el codigo de error devuelto.
   */
  public String getErrorGeneral();

  /**
   * Metodo que permite conocer el error particular del mensaje.
   * <p/>
   * Este dato, de recibirse segun la tipologia del mensaje, se recibe en los campos propios del mismo.
   * 
   * @return Un {@link String} con el codigo de error devuelto.
   */
  public String getErrorDeMensaje();

  // ---------------------------------------------- Records Management

  /**
   * Metodo de conveniencia para que un mensaje devuelva instancias Java de registros de su tipo particularizado.
   * 
   * @param tipo Una instancia de {@link PTIMessageRecordType} para determinar que tipo de registro se precisa.
   * @return Una instancia de {@link PTIMessageRecord} concreta.
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException;

  /**
   * Metodo generico para agregar registros {@link PTIMessageRecord} a un mensaje.
   * 
   * @param tipo Una instancia de {@link PTIMessageRecordType} para especificar el tipo de registro.
   * @param registro Una instancia de {@link PTIMessageRecord}.
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public void addRecord(final PTIMessageRecordType tipo, final PTIMessageRecord registro) throws PTIMessageException;

  /**
   * Metodo generico para recuperar la lista de registros (<i> {@link PTIMessageRecord}</i>) de un tipo dado (<i>
   * {@link PTIMessageRecordType}</i>) de un mensaje.
   * 
   * @param tipo Una instancia de {@link PTIMessageRecordType} para especificar el tipo de registro.
   * @return Un {@link List} de {@link PTIMessageRecord}'s.
   */
  public List<PTIMessageRecord> getAll(final PTIMessageRecordType tipo);

  /**
   * Metodo generico para conocer el numero de registros de un tipo dado (<i> {@link PTIMessageRecordType}</i>) de un
   * mensaje.
   * 
   * @param type Una instancia de {@link PTIMessageRecordType} para especificar el tipo de registro.
   * @return Un entero con el total de registros de dicho tipo en el mensaje.
   */
  public int getCount(final PTIMessageRecordType type);

  /**
   * Es el ultimo de un lote
   * */
  public boolean isLastInGroup();

  public boolean isInGroup();

}
