package sibbac.pti.messages.ga01;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.AControlBlock;
import sibbac.pti.messages.Field;

public class GA01ControlBlock extends AControlBlock {

  // -------------------------------------------- La lista de campos.

  public static final String R00 = PTIMessageRecordType.R00.name();
  public static final String R01 = PTIMessageRecordType.R01.name();
  public static final String R02 = PTIMessageRecordType.R02.name();
  public static final String R03 = PTIMessageRecordType.R03.name();

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, R00, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0), new Field(
                                                                                                                   2,
                                                                                                                   R01,
                                                                                                                   PTIMessageFieldType.NUMBER,
                                                                                                                   SIZE_CONTROL,
                                                                                                                   0), new Field(
                                                                                                                                 3,
                                                                                                                                 R02,
                                                                                                                                 PTIMessageFieldType.NUMBER,
                                                                                                                                 SIZE_CONTROL,
                                                                                                                                 0), new Field(
                                                                                                                                               4,
                                                                                                                                               R03,
                                                                                                                                               PTIMessageFieldType.NUMBER,
                                                                                                                                               SIZE_CONTROL,
                                                                                                                                               0)
  };

  /**
   * Constructor por defecto.
   * 
   */
  public GA01ControlBlock() throws PTIMessageException {
    super(PTIMessageRecordType.CONTROL_BLOCK, FIELDS);
  }

  public BigDecimal getR00() {
    return this.getAsNumber(R00);
  }

  public BigDecimal getR01() {
    return this.getAsNumber(R01);
  }

  public BigDecimal getR02() {
    return this.getAsNumber(R02);
  }

  public BigDecimal getR03() {
    return this.getAsNumber(R03);
  }

  public void setR00(final String valor) throws PTIMessageException {
    this.set(R00, valor);
  }

  public void setR01(final String valor) throws PTIMessageException {
    this.set(R01, valor);
  }

  public void setR02(final String valor) throws PTIMessageException {
    this.set(R02, valor);
  }

  public void setR03(final String valor) throws PTIMessageException {
    this.set(R03, valor);
  }

}
