package sibbac.pti.messages.va;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: VA. (Informacion de valores).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// INFORMACION GENERAL DEL VALOR
	public static final String	CODIGO_DE_VALOR						= "CODIGO_DE_VALOR";
	public static final String	DIVISA								= "DIVISA";
	public static final String	DESCRIPCION_DEL_VALOR				= "DESCRIPCION_DEL_VALOR";
	public static final String	DESCRIPCION_CORTA_DEL_VALOR			= "DESCRIPCION_CORTA_DEL_VALOR";
	public static final String	CODIGO_CONTRATACION					= "CODIGO_CONTRATACION";
	public static final String	GRUPO_DE_VALORES					= "GRUPO_DE_VALORES";
	public static final String	TIPO_DE_PRODUCTO					= "TIPO_DE_PRODUCTO";
	public static final String	FECHA_EMISION						= "FECHA_EMISION";
	public static final String	PRECIO_EJERCICIO					= "PRECIO_EJERCICIO";
	public static final String	UNIDAD_DE_CONTRATACION				= "UNIDAD_DE_CONTRATACION";
	public static final String	ESTILO_EJERCICIO					= "ESTILO_EJERCICIO";
	public static final String	FACTOR_CONVERSION					= "FACTOR_CONVERSION";
	public static final String	IND_CALL_PUT						= "IND_CALL_PUT";
	public static final String	ENTIDAD_EMISORA_GESTORA				= "ENTIDAD_EMISORA_GESTORA";

	// Tamanyos de los campos.
	public static final int		SIZE_CODIGO_DE_VALOR				= 12;
	public static final int		SIZE_DIVISA							= 3;
	public static final int		SIZE_DESCRIPCION_DEL_VALOR			= 80;
	public static final int		SIZE_DESCRIPCION_CORTA_DEL_VALOR	= 22;
	public static final int		SIZE_CODIGO_CONTRATACION			= 5;
	public static final int		SIZE_GRUPO_DE_VALORES				= 2;
	public static final int		SIZE_TIPO_DE_PRODUCTO				= 1;
	public static final int		SIZE_FECHA_EMISION					= 8;
	public static final int		SIZE_PRECIO_EJERCICIO				= 13;
	public static final int		SIZE_UNIDAD_DE_CONTRATACION			= 18;
	public static final int		SIZE_ESTILO_EJERCICIO				= 1;
	public static final int		SIZE_FACTOR_CONVERSION				= 18;
	public static final int		SIZE_IND_CALL_PUT					= 1;
	public static final int		SIZE_ENTIDAD_EMISORA_GESTORA		= 11;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, CODIGO_DE_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_VALOR ),
			new Field( 2, DIVISA, PTIMessageFieldType.ASCII, SIZE_DIVISA ),
			new Field( 4, DESCRIPCION_DEL_VALOR, PTIMessageFieldType.ASCII, SIZE_DESCRIPCION_DEL_VALOR ),
			new Field( 5, DESCRIPCION_CORTA_DEL_VALOR, PTIMessageFieldType.ASCII, SIZE_DESCRIPCION_CORTA_DEL_VALOR ),
			new Field( 6, CODIGO_CONTRATACION, PTIMessageFieldType.ASCII, SIZE_CODIGO_CONTRATACION ),
			new Field( 7, GRUPO_DE_VALORES, PTIMessageFieldType.ASCII, SIZE_GRUPO_DE_VALORES ),
			new Field( 8, TIPO_DE_PRODUCTO, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_PRODUCTO ),
			new Field( 9, FECHA_EMISION, PTIMessageFieldType.ASCII, SIZE_FECHA_EMISION ),
			new Field( 10, PRECIO_EJERCICIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO_EJERCICIO, 6 ),
			new Field( 11, UNIDAD_DE_CONTRATACION, PTIMessageFieldType.NUMBER, SIZE_UNIDAD_DE_CONTRATACION, 6 ),
			new Field( 12, ESTILO_EJERCICIO, PTIMessageFieldType.ASCII, SIZE_ESTILO_EJERCICIO ),
			new Field( 13, FACTOR_CONVERSION, PTIMessageFieldType.NUMBER, SIZE_FACTOR_CONVERSION, 6 ),
			new Field( 14, IND_CALL_PUT, PTIMessageFieldType.ASCII, SIZE_IND_CALL_PUT ),
			new Field( 15, ENTIDAD_EMISORA_GESTORA, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_EMISORA_GESTORA )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}
	

  protected R00(final Field[] recordFields ) throws PTIMessageException{
    super(PTIMessageRecordType.R00, recordFields);
  }

	public String getCodigoDeValor() {
		return this.getAsString( CODIGO_DE_VALOR );
	}

	public String getDivisa() {
		return this.getAsString( DIVISA );
	}

	public String getDescripcionDelValor() {
		return this.getAsString( DESCRIPCION_DEL_VALOR );
	}

	public String getDescripcionCortaDelValor() {
		return this.getAsString( DESCRIPCION_CORTA_DEL_VALOR );
	}

	public String getCodigoContratacion() {
		return this.getAsString( CODIGO_CONTRATACION );
	}

	public String getGrupoDeValores() {
		return this.getAsString( GRUPO_DE_VALORES );
	}

	public String getTipoDeProducto() {
		return this.getAsString( TIPO_DE_PRODUCTO );
	}

	public String getFechaEmision() {
		return this.getAsString( FECHA_EMISION );
	}

	public BigDecimal getPrecioEjercicio() {
		return this.getAsNumber( PRECIO_EJERCICIO );
	}

	public BigDecimal getUnidadDeContratacion() {
		return this.getAsNumber( UNIDAD_DE_CONTRATACION );
	}

	public String getEstiloEjercicio() {
		return this.getAsString( ESTILO_EJERCICIO );
	}

	public BigDecimal getFactorConversion() {
		return this.getAsNumber( FACTOR_CONVERSION );
	}

	public String getIndCallPut() {
		return this.getAsString( IND_CALL_PUT );
	}

	public String getEntidadEmisoraGestora() {
		return this.getAsString( ENTIDAD_EMISORA_GESTORA );
	}

	public void setCodigoDeValor( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_VALOR, valor );
	}

	public void setDivisa( final String valor ) throws PTIMessageException {
		this.set( DIVISA, valor );
	}

	public void setDescripcionDelValor( final String valor ) throws PTIMessageException {
		this.set( DESCRIPCION_DEL_VALOR, valor );
	}

	public void setDescripcionCortaDelValor( final String valor ) throws PTIMessageException {
		this.set( DESCRIPCION_CORTA_DEL_VALOR, valor );
	}

	public void setCodigoContratacion( final String valor ) throws PTIMessageException {
		this.set( CODIGO_CONTRATACION, valor );
	}

	public void setGrupoDeValores( final String valor ) throws PTIMessageException {
		this.set( GRUPO_DE_VALORES, valor );
	}

	public void setTipoDeProducto( final String valor ) throws PTIMessageException {
		this.set( TIPO_DE_PRODUCTO, valor );
	}

	public void setFechaEmision( final String valor ) throws PTIMessageException {
		this.set( FECHA_EMISION, valor );
	}

	public void setPrecioEjercicio( final String valor ) throws PTIMessageException {
		this.set( PRECIO_EJERCICIO, valor );
	}

	public void setUnidadDeContratacion( final String valor ) throws PTIMessageException {
		this.set( UNIDAD_DE_CONTRATACION, valor );
	}

	public void setEstiloEjercicio( final String valor ) throws PTIMessageException {
		this.set( ESTILO_EJERCICIO, valor );
	}

	public void setFactorConversion( final String valor ) throws PTIMessageException {
		this.set( FACTOR_CONVERSION, valor );
	}

	public void setIndCallPut( final String valor ) throws PTIMessageException {
		this.set( IND_CALL_PUT, valor );
	}

	public void setEntidadEmisoraGestora( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_EMISORA_GESTORA, valor );
	}


}
