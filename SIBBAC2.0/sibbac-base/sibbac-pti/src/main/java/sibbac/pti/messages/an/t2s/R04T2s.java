package sibbac.pti.messages.an.t2s;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.an.R04;

/**
 * Registro R04 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R04T2s extends R04 {

  // -------------------------------------------- La lista de campos.

  // ----------------------------------------------------------T2S

  // 102
  public static final String EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF = "EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF";
  // 103
  public static final String EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF = "EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF";
  // 104
  public static final String EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF = "EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF";
  // 105
  public static final String EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF = "EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF";
  // 106
  public static final String EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF = "EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF";
  // 107
  public static final String EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF = "EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF";
  // 108
  public static final String EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF = "EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF";
  // 109
  public static final String GRUPO_COMPENSACION_RF = "GRUPO_COMPENSACION_RF";
  
//102
 public static final int SIZE_EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF = 16;
 // 103
 public static final int SIZE_EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF = 16;
 // 104
 public static final int SIZE_EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF = 16;
 // 105
 public static final int SIZE_EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF = 16;
 // 106
 public static final int SIZE_EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF = 16;
 // 107
 public static final int SIZE_EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF = 16;
 // 108
 public static final int SIZE_EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF = 16;
 // 109
 public static final int SIZE_GRUPO_COMPENSACION_RF = 12;
  // -----------------------------------------------------------T2S

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = {new Field( 1, FECHA_CONTRATACION, PTIMessageFieldType.ASCII, SIZE_FECHA_CONTRATACION ),
                                        new Field( 2, FECHA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_LIQUIDACION ),
                                        new Field( 3, TIPO_SALDO, PTIMessageFieldType.ASCII, SIZE_TIPO_SALDO ),
                                        new Field( 4, DIVISA, PTIMessageFieldType.ASCII, SIZE_DIVISA ),
                                        new Field( 5, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
                                        new Field( 6, SALDO_NETO_COMPRADOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_SALDO_NETO_COMPRADOR_VALORES_NOMINAL, 6 ),
                                        new Field( 7, EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DEL_SALDO_NETO_COMPRADOR_DE_VALORES, 13, 2 ),
                                        new Field( 8, SALDO_NETO_VENDEDOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_SALDO_NETO_VENDEDOR_VALORES_NOMINAL, 6 ),
                                        new Field( 9, EFECTIVO_DEL_SALDO_NETO_VENDEDOR_DE_VALORES, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DEL_SALDO_NETO_VENEDOR_DE_VALORES, 13, 2 ),
                                        new Field( 10, SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_SALDO_BRUTO_COMPRADOR_VALORES_NOMINAL, 6 ),
                                        new Field( 11, EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DEL_SALDO_BRUTO_COMPRADOR_DE_VALORES, 13, 2 ),
                                        new Field( 12, SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL, PTIMessageFieldType.ASCII, SIZE_SALDO_BRUTO_VENDEDOR_VALORES_NOMINAL, 6 ),
                                        new Field( 13, EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DEL_SALDO_BRUTO_VENDEDOR_DE_VALORES, 13, 2 ),
                                        new Field( 14, VALORES_NOMINAL_RETENIDO, PTIMessageFieldType.NUMBER, SIZE_VALORES_NOMINAL_RETENIDO, 6 ),
                                        new Field( 15, IMPORTE_EFECTIVO_RETENIDO, PTIMessageFieldType.SIGNED, SIZE_IMPORTE_EFECTIVO_RETENIDO, 13, 2 ),
                                        new Field( 16, SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_SALDO_FALLIDO_VENDEDOR_VALORES_NOMINAL,
                                            6 ),
                                        new Field( 17, EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR, PTIMessageFieldType.SIGNED, SIZE_EFECTIVO_DEL_SALDO_FALLIDO_VENDEDOR, 13, 2 ),
                                        new Field( 18, SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
                                            SIZE_SALDO_FALLIDO_COMPRADOR_VALORES_NOMINAL, 6 ),
                                        new Field( 19, EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR, PTIMessageFieldType.SIGNED, SIZE_EFECTIVO_DEL_SALDO_FALLIDO_COMPRADOR, 13,
                                            2 ),
                                        new Field( 20, SALDO_PRESTAMISTA_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_SALDO_PRESTAMISTA_VALORES_NOMINAL, 6 ),
                                        new Field( 21, EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTAMISTA, 13, 2 ),
                                        new Field( 22, SALDO_PRESTATARIO_VALORES_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_SALDO_PRESTATARIO_VALORES_NOMINAL, 6 ),
                                        new Field( 23, EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DEL_COLATERAL_DEL_SALDO_PRESTATARIO, 13, 2 ),
                                        new Field( 24, RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.NUMBER,
                                            SIZE_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, 6 ),
                                        new Field( 25, EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DE_LA_RECEPCION_DE_VALORES_NOMINAL_POR_AJUSTES, 13, 2 ),
                                        new Field( 26, ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.NUMBER, SIZE_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES,
                                            6 ),
                                        new Field( 27, EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_DE_LA_ENTREGA_DE_VALORES_NOMINAL_POR_AJUSTES, 13, 2 ),
                                        new Field( 28, EFECTIVO_PENDIENTE_DE_LIQUIDAR, PTIMessageFieldType.SIGNED, SIZE_EFECTIVO_PENDIENTE_DE_LIQUIDAR, 13, 2 ),
                                        new Field( 29, SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
                                            SIZE_SALDO_VENDEDOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, 6 ),
                                        new Field( 30, EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_OPERACIONES_ESPECIALES_VENDEDOR, 13, 2 ),
                                        new Field( 31, SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
                                            SIZE_SALDO_COMPRADOR_OPERACIONES_ESPECIALES_VALORES_NOMINAL, 6 ),
                                        new Field( 32, EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_OPERACIONES_ESPECIALES_COMPRADOR, 13, 2 ),
                                        new Field( 33, SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
                                            SIZE_SALDO_VENDEDOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, 6 ),
                                        new Field( 34, EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VENDEDOR, 13, 2 ),
                                        new Field( 35, SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, PTIMessageFieldType.NUMBER,
                                            SIZE_SALDO_COMPRADOR_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_VALORES_NOMINAL, 6 ),
                                        new Field( 36, EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR, PTIMessageFieldType.SIGNED,
                                            SIZE_EFECTIVO_VENCIMIENTO_DE_FUTUROS_Y_OPCIONES_COMPRADOR, 13, 2 ),
                                            // T2S
                                        new Field( 37, EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF, 13, 2 ),
                                        new Field( 38, EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF, 13, 2 ),
                                        new Field( 39, EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF, 13, 2 ),
                                        new Field( 40, EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF, 13, 2 ),
                                        new Field( 41, EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF, 13, 2 ),
                                        new Field( 42, EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF, 13, 2 ),
                                        new Field( 43, EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF, PTIMessageFieldType.SIGNED,
                                                   SIZE_EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF, 13, 2 ),
                                        new Field( 44, GRUPO_COMPENSACION_RF, PTIMessageFieldType.ASCII,
                                                   SIZE_GRUPO_COMPENSACION_RF )
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R04T2s() throws PTIMessageException {
    super(FIELDS);
  }
  
  public BigDecimal getEfectivoActualSaldoNetoCompradorValoresRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF);
  }
  
  public BigDecimal getEfectivoActualSaldoNetoVendedorValoresRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF);
  }
  
  public BigDecimal getEfectivoActualSaldoFallidoCompradorValoresRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF);
  }
  
  public BigDecimal getEfectivoActualSaldoFallidoVendedorValoresRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF);
  }
  
  public BigDecimal getEfectivoActualRecepcionValoresNominalAjusteCompradorValoresRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF);
  }
  
  public BigDecimal getEfectivoActualEntregaValoresNominalAjusteRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF);
  }
  
  public BigDecimal getEfectivoActualPosicionesEfectivoPdteLiquidarRf(){
    return this.getAsNumber(EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF);
  }
  
  public String getGrupoCompensacionRf(){
    return this.getAsString(GRUPO_COMPENSACION_RF);
  }
  
  public void setEfectivoActualSaldoNetoCompradorValoresRf(BigDecimal value) throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_SALDO_NETO_COMPRADOR_VALORES_RF, value);
  }
  
  public void setEfectivoActualSaldoNetoVendedorValoresRf(BigDecimal value)  throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_SALDO_NETO_VENDEDOR_VALORES_RF, value);
  }
  
  public void setEfectivoActualSaldoFallidoCompradorValoresRf(BigDecimal value) throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_SALDO_FALLIDO_COMPRADOR_VALORES_RF, value);
  }
  
  public void setEfectivoActualSaldoFallidoVendedorValoresRf(BigDecimal value) throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_SALDO_FALLIDO_VENDEDOR_VALORES_RF, value);
  }
  
  public void setEfectivoActualRecepcionValoresNominalAjusteCompradorValoresRf(BigDecimal value) throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_RECEPCION_VALORES_NOMINAL_AJUSTE_COMPRADOR_VALORES_RF, value);
  }
  
  public void setEfectivoActualEntregaValoresNominalAjusteRf(BigDecimal value) throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_ENTREGA_VALORES_NOMINAL_AJUSTE_RF, value);
  }
  
  public void setEfectivoActualPosicionesEfectivoPdteLiquidarRf(BigDecimal value) throws PTIMessageException{
    this.set(EFECTIVO_ACTUAL_POSICIONES_EFECTIVO_PDTE_LIQUIDAR_RF, value);
  }
  
  public void setGrupoCompensacionRf(String value) throws PTIMessageException{
    this.set(GRUPO_COMPENSACION_RF, value);
  }


  
}
