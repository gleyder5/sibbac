package sibbac.pti;


/**
 * Excepcion generica de la mensajeria PTI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class PTIMessageException extends Exception {

	/**
	 * Serial.
	 */
	private static final long	serialVersionUID	= 1L;

	/**
	 * Constructor "por defecto".
	 * 
	 * @param message
	 *            El mensaje de error a mostrar.
	 */
	public PTIMessageException( final String message ) {
		super( message );
	}

	public PTIMessageException( final Exception e ) {
		super( e );
	}

}
