package sibbac.pti.messages.va;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: VA. (Informacion de valores).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Informacion adicional del valor
	public static final String	TIPO_INFORMACION_ADICIONAL			= "TIPO_INFORMACION_ADICIONAL";
	public static final String	PRECIO								= "PRECIO";
	public static final String	NUMERO_DE_DECIMALES					= "NUMERO_DE_DECIMALES";
	public static final String	COD_DCV								= "COD_DCV";
	public static final String	FECHA_ULTIMO_DIA_NEGOCIACION		= "FECHA_ULTIMO_DIA_NEGOCIACION";

	// Tamanyos de los campos.
	public static final int		SIZE_TIPO_INFORMACION_ADICIONAL		= 3;
	public static final int		SIZE_PRECIO							= 13;
	public static final int		SIZE_NUMERO_DE_DECIMALES			= 6;
	public static final int		SIZE_COD_DCV						= 11;
	public static final int		SIZE_FECHA_ULTIMO_DIA_NEGOCIACION	= 8;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, TIPO_INFORMACION_ADICIONAL, PTIMessageFieldType.ASCII, SIZE_TIPO_INFORMACION_ADICIONAL ),
			new Field( 2, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6 ),
			new Field( 4, NUMERO_DE_DECIMALES, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_DECIMALES, 0 ),
			new Field( 5, COD_DCV, PTIMessageFieldType.ASCII, SIZE_COD_DCV ),
			new Field( 6, FECHA_ULTIMO_DIA_NEGOCIACION, PTIMessageFieldType.ASCII, SIZE_FECHA_ULTIMO_DIA_NEGOCIACION )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01() throws PTIMessageException {
		super( PTIMessageRecordType.R01, FIELDS );
	}
	
	protected R01(final Field[] recordFields ) throws PTIMessageException{
    super(PTIMessageRecordType.R01, recordFields);
  }

	public String getTipoInformacionAdicional() {
		return this.getAsString( TIPO_INFORMACION_ADICIONAL );
	}

	public BigDecimal getPrecio() {
		return this.getAsNumber( PRECIO );
	}

	public BigDecimal getNumeroDeDecimales() {
		return this.getAsNumber( NUMERO_DE_DECIMALES );
	}

	public String getCodDCV() {
		return this.getAsString( COD_DCV );
	}

	public String getFechaUltimoDiaNegociacion() {
		return this.getAsString( FECHA_ULTIMO_DIA_NEGOCIACION );
	}

	public void setTipoInformacionAdicional( final String valor ) throws PTIMessageException {
		this.set( TIPO_INFORMACION_ADICIONAL, valor );
	}

	public void setPrecio( final String valor ) throws PTIMessageException {
		this.set( PRECIO, valor );
	}

	public void setNumeroDeDecimales( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_DECIMALES, valor );
	}

	public void setCodDCV( final String valor ) throws PTIMessageException {
		this.set( COD_DCV, valor );
	}

	public void setFechaUltimoDiaNegociacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_ULTIMO_DIA_NEGOCIACION, valor );
	}


}
