package sibbac.pti.messages.ti;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R02 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Relacion cotitulares
	public static final String	NOMBRE_RAZON_SOCIAL						= "NOMBRE_RAZON_SOCIAL";
	public static final String	PRIMER_APELLIDO							= "PRIMER_APELLIDO";
	public static final String	SEGUNDO_APELLIDO						= "SEGUNDO_APELLIDO";
	public static final String	IDENTIFICACION							= "IDENTIFICACION";
	public static final String	TIPO_DE_IDENTIFICACION					= "TIPO_DE_IDENTIFICACION";
	public static final String	INDICADOR_PERSONA_FISICA_JURIDICA		= "INDICADOR_PERSONA_FISICA_JURIDICA";
	public static final String	PAIS_DE_NACIONALIDAD					= "PAIS_DE_NACIONALIDAD";
	public static final String	INDICADOR_DE_NACIONALIDAD				= "INDICADOR_DE_NACIONALIDAD";
	public static final String	TIPO_DE_TITULAR							= "TIPO_DE_TITULAR";
	public static final String	PCT_DE_PARTICIPACION_EN_PROPIEDAD		= "PCT_DE_PARTICIPACION_EN_PROPIEDAD";
	public static final String	PCT_DE_PARTICIPACION_EN_USUFRUCTO		= "PCT_DE_PARTICIPACION_EN_USUFRUCTO";
	public static final String	CODIGO_SUSCRIPTOR						= "CODIGO_SUSCRIPTOR";

	// Datos a devolver
	public static final String	CODIGO_DE_ERROR							= "CODIGO_DE_ERROR";

	// Tamanyos de los campos.
	public static final int		SIZE_NOMBRE_RAZON_SOCIAL				= 140;
	public static final int		SIZE_PRIMER_APELLIDO					= 40;
	public static final int		SIZE_SEGUNDO_APELLIDO					= 40;
	public static final int		SIZE_IDENTIFICACION						= 40;
	public static final int		SIZE_TIPO_DE_IDENTIFICACION				= 1;
	public static final int		SIZE_INDICADOR_PERSONA_FISICA_JURIDICA	= 1;
	public static final int		SIZE_PAIS_DE_NACIONALIDAD				= 3;
	public static final int		SIZE_INDICADOR_DE_NACIONALIDAD			= 1;
	public static final int		SIZE_TIPO_DE_TITULAR					= 1;
	public static final int		SIZE_PCT_DE_PARTICIPACION_EN_PROPIEDAD	= 5;
	public static final int		SIZE_PCT_DE_PARTICIPACION_EN_USUFRUCTO	= 5;
	public static final int		SIZE_CODIGO_SUSCRIPTOR					= 4;
	public static final int		SIZE_CODIGO_DE_ERROR					= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, NOMBRE_RAZON_SOCIAL, PTIMessageFieldType.ASCII, SIZE_NOMBRE_RAZON_SOCIAL ),
			new Field( 2, PRIMER_APELLIDO, PTIMessageFieldType.ASCII, SIZE_PRIMER_APELLIDO ),
			new Field( 3, SEGUNDO_APELLIDO, PTIMessageFieldType.ASCII, SIZE_SEGUNDO_APELLIDO ),
			new Field( 4, IDENTIFICACION, PTIMessageFieldType.ASCII, SIZE_IDENTIFICACION ),
			new Field( 5, TIPO_DE_IDENTIFICACION, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_IDENTIFICACION ),
			new Field( 6, INDICADOR_PERSONA_FISICA_JURIDICA, PTIMessageFieldType.ASCII, SIZE_INDICADOR_PERSONA_FISICA_JURIDICA ),
			new Field( 7, PAIS_DE_NACIONALIDAD, PTIMessageFieldType.ASCII, SIZE_PAIS_DE_NACIONALIDAD ),
			new Field( 8, INDICADOR_DE_NACIONALIDAD, PTIMessageFieldType.ASCII, SIZE_INDICADOR_DE_NACIONALIDAD ),
			new Field( 9, TIPO_DE_TITULAR, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_TITULAR ),
			new Field( 10, PCT_DE_PARTICIPACION_EN_PROPIEDAD, PTIMessageFieldType.NUMBER, SIZE_PCT_DE_PARTICIPACION_EN_PROPIEDAD, 2 ),
			new Field( 11, PCT_DE_PARTICIPACION_EN_USUFRUCTO, PTIMessageFieldType.NUMBER, SIZE_PCT_DE_PARTICIPACION_EN_USUFRUCTO, 2 ),
			new Field( 12, CODIGO_SUSCRIPTOR, PTIMessageFieldType.NUMBER, SIZE_CODIGO_SUSCRIPTOR, 0 ),
			new Field( 13, CODIGO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_ERROR )
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02() throws PTIMessageException {
		super( PTIMessageRecordType.R02, FIELDS );
	}
	
	/**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException
   *             Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  protected R02(final Field[] recordFields) throws PTIMessageException {
    super( PTIMessageRecordType.R02, recordFields );
  }

	public String getNombreRazonSocial() {
		return this.getAsString( NOMBRE_RAZON_SOCIAL );
	}

	public String getPrimerApellido() {
		return this.getAsString( PRIMER_APELLIDO );
	}

	public String getSegundoApellido() {
		return this.getAsString( SEGUNDO_APELLIDO );
	}

	public String getIdentificacion() {
		return this.getAsString( IDENTIFICACION );
	}

	public String getTipoDeIdentificacion() {
		return this.getAsString( TIPO_DE_IDENTIFICACION );
	}

	public String getIndicadorPersonaFisicaJuridica() {
		return this.getAsString( INDICADOR_PERSONA_FISICA_JURIDICA );
	}

	public String getPaisDeNacionalidad() {
		return this.getAsString( PAIS_DE_NACIONALIDAD );
	}

	public String getIndicadorDeNacionalidad() {
		return this.getAsString( INDICADOR_DE_NACIONALIDAD );
	}

	public String getTipoDeTitular() {
		return this.getAsString( TIPO_DE_TITULAR );
	}

	public BigDecimal getPctDeParticipacionEnPropiedad() {
		return this.getAsNumber( PCT_DE_PARTICIPACION_EN_PROPIEDAD );
	}

	public BigDecimal getPctDeParticipacionEnUsufructo() {
		return this.getAsNumber( PCT_DE_PARTICIPACION_EN_USUFRUCTO );
	}

	public BigDecimal getCodigoSuscriptor() {
		return this.getAsNumber( CODIGO_SUSCRIPTOR );
	}

	public String getCodigoDeError() {
		return this.getAsString( CODIGO_DE_ERROR );
	}

	public void setNombreRazonSocial( final String valor ) throws PTIMessageException {
		this.set( NOMBRE_RAZON_SOCIAL, valor );
	}

	public void setPrimerApellido( final String valor ) throws PTIMessageException {
		this.set( PRIMER_APELLIDO, valor );
	}

	public void setSegundoApellido( final String valor ) throws PTIMessageException {
		this.set( SEGUNDO_APELLIDO, valor );
	}

	public void setIdentificacion( final String valor ) throws PTIMessageException {
		this.set( IDENTIFICACION, valor );
	}

	public void setTipoDeIdentificacion( final String valor ) throws PTIMessageException {
		this.set( TIPO_DE_IDENTIFICACION, valor );
	}

	public void setIndicadorPersonaFisicaJuridica( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_PERSONA_FISICA_JURIDICA, valor );
	}

	public void setPaisDeNacionalidad( final String valor ) throws PTIMessageException {
		this.set( PAIS_DE_NACIONALIDAD, valor );
	}

	public void setIndicadorDeNacionalidad( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_DE_NACIONALIDAD, valor );
	}

	public void setTipoDeTitular( final String valor ) throws PTIMessageException {
		this.set( TIPO_DE_TITULAR, valor );
	}

	public void setPctDeParticipacionEnPropiedad( final String valor ) throws PTIMessageException {
		this.set( PCT_DE_PARTICIPACION_EN_PROPIEDAD, valor );
	}

	public void setPctDeParticipacionEnUsufructo( final String valor ) throws PTIMessageException {
		this.set( PCT_DE_PARTICIPACION_EN_USUFRUCTO, valor );
	}

	public void setCodigoSuscriptor( final String valor ) throws PTIMessageException {
		this.set( CODIGO_SUSCRIPTOR, valor );
	}

	public void setPctDeParticipacionEnPropiedad( final BigDecimal valor ) throws PTIMessageException {
		this.set( PCT_DE_PARTICIPACION_EN_PROPIEDAD, valor );
	}

	public void setPctDeParticipacionEnUsufructo( final BigDecimal valor ) throws PTIMessageException {
		this.set( PCT_DE_PARTICIPACION_EN_USUFRUCTO, valor );
	}

	public void setCodigoSuscriptor( final BigDecimal valor ) throws PTIMessageException {
		this.set( CODIGO_SUSCRIPTOR, valor );
	}

	public void setCodigoDeError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_ERROR, valor );
	}

}
