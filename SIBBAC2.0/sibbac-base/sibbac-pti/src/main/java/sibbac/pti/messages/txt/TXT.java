package sibbac.pti.messages.txt;

import java.util.List;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;

/**
 * Mensaje de tipo TXT.
 * <p/>
 * Definicion: PDF BME, pg 94.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class TXT extends AMessage {

  /**
   * Constructor por defecto.
   */
  public TXT() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public TXT(final boolean fromPTI) throws PTIMessageException {
    super(PTIMessageType.TXT, fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.messages.AMessage#setup()
   */
  @Override
  public void messageDefinition() throws PTIMessageException {
    // El bloque de datos comunes.
    this.commonDataBlock = null;

    // El bloque de datos de control.
    this.controlBlock = new TXTControlBlock();
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public R00 getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException {
    switch (tipo) {
      case R00:
        return new R00();
      default:
        return null;
    }
  }

  /**
   * Metodo que devuelve todo el mensaje como suma de todos los textos de todos los registros recibidos.
   * 
   * @return Un {@link String}.
   */
  public String getMensaje() {
    List<PTIMessageRecord> records = this.getAll(PTIMessageRecordType.R00);
    if (records == null || records.isEmpty() || records.size() == 0) {
      return null;
    }

    StringBuffer msg = new StringBuffer();
    R00 record = null;
    for (PTIMessageRecord r : records) {
      record = (R00) r;
      msg.append(record.getTextoInformativo().trim());
    }
    return msg.toString();
  }

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub
    
  }

}
