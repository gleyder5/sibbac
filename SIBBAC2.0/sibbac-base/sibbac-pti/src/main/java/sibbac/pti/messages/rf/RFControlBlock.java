package sibbac.pti.messages.rf;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.AControlBlock;
import sibbac.pti.messages.Field;


/**
 * Bloque de control.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class RFControlBlock extends AControlBlock {

	// -------------------------------------------- La lista de campos.
	public static final String	R00						= PTIMessageRecordType.R00.name();
	public static final String	R01						= PTIMessageRecordType.R01.name();
	public static final String	R02						= PTIMessageRecordType.R02.name();
	public static final String	R03						= PTIMessageRecordType.R03.name();
	public static final String	CODIGO_DE_ERROR			= "CODIGO_DE_ERROR";
	public static final String	TEXTO_DE_ERROR			= "TEXTO_DE_ERROR";					;

	public static final int		SIZE_CODIGO_DE_ERROR	= 3;
	public static final int		SIZE_TEXTO_DE_ERROR		= 40;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS					= {
			new Field( 1, R00, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 2, R01, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 3, R02, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 4, R03, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0 ),
			new Field( 5, CODIGO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_ERROR ),
			new Field( 6, TEXTO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_TEXTO_DE_ERROR )
														};

	/**
	 * Constructor por defecto.
	 * 
	 */
	public RFControlBlock() throws PTIMessageException {
		super( PTIMessageRecordType.CONTROL_BLOCK, FIELDS );
	}

	public BigDecimal getR00() {
		return this.getAsNumber( R00 );
	}

	public BigDecimal getR01() {
		return this.getAsNumber( R01 );
	}

	public BigDecimal getR02() {
		return this.getAsNumber( R02 );
	}

	public BigDecimal getR03() {
		return this.getAsNumber( R03 );
	}

	public String getCodigoDeError() {
		return this.getAsString( CODIGO_DE_ERROR );
	}

	public String getTextoDeError() {
		return this.getAsString( TEXTO_DE_ERROR );
	}

	public boolean hayErrores() {
		return !this.getCodigoDeError().equalsIgnoreCase( NO_ERROR );
	}

	public void setR00( final String valor ) throws PTIMessageException {
		this.set( R00, valor );
	}

	public void setR01( final String valor ) throws PTIMessageException {
		this.set( R01, valor );
	}

	public void setR02( final String valor ) throws PTIMessageException {
		this.set( R02, valor );
	}

	public void setR03( final String valor ) throws PTIMessageException {
		this.set( R03, valor );
	}

	public void setCodigoError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_ERROR, valor );
	}

	public void setTextoError( final String valor ) throws PTIMessageException {
		this.set( TEXTO_DE_ERROR, valor );
	}


}
