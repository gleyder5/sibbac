package sibbac.pti.messages.rc;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: PT.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos Peticion
	public static final String	ENTIDAD_PARTICIPANTE		= "ENTIDAD_PARTICIPANTE";
	public static final String	CUENTA_DE_LIQUIDACION		= "CUENTA_DE_LIQUIDACION";
	public static final String	FECHA_CONTRATACION			= "FECHA_CONTRATACION";
	public static final String	FECHA_DE_LIQUIDACION		= "FECHA_DE_LIQUIDACION";
	public static final String	CARGO_ABONO					= "CARGO_ABONO";
	public static final String	ID_CANON_Y_CORRETAJE		= "ID_CANON_Y_CORRETAJE";
	public static final String	MIEMBRO_DE_MERCADO			= "MIEMBRO_DE_MERCADO";
	public static final String	IMPORTE						= "IMPORTE";

	// Tamanyos de los campos.
	public static final int		SIZE_ENTIDAD_PARTICIPANTE	= 11;
	public static final int		SIZE_CUENTA_DE_LIQUIDACION	= 35;
	public static final int		SIZE_FECHA_CONTRATACION		= 8;
	public static final int		SIZE_FECHA_DE_LIQUIDACION	= 8;
	public static final int		SIZE_CARGO_ABONO			= 1;
	public static final int		SIZE_ID_CANON_Y_CORRETAJE	= 2;
	public static final int		SIZE_MIEMBRO_DE_MERCADO		= 4;
	public static final int		SIZE_IMPORTE				= 15;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS						= {
			new Field( 1, ENTIDAD_PARTICIPANTE, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_PARTICIPANTE ),
			new Field( 2, CUENTA_DE_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_DE_LIQUIDACION ),
			new Field( 3, FECHA_CONTRATACION, PTIMessageFieldType.ASCII, SIZE_FECHA_CONTRATACION ),
			new Field( 4, FECHA_DE_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_DE_LIQUIDACION ),
			new Field( 5, CARGO_ABONO, PTIMessageFieldType.ASCII, SIZE_CARGO_ABONO ),
			new Field( 6, ID_CANON_Y_CORRETAJE, PTIMessageFieldType.ASCII, SIZE_ID_CANON_Y_CORRETAJE ),
			new Field( 7, MIEMBRO_DE_MERCADO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_DE_MERCADO ),
			new Field( 8, IMPORTE, PTIMessageFieldType.NUMBER, SIZE_IMPORTE, 2 )
															};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getEntidadParticipante() {
		return this.getAsString( ENTIDAD_PARTICIPANTE );
	}

	public String getCuentaLiquidacion() {
		return this.getAsString( CUENTA_DE_LIQUIDACION );
	}

	public Date getFechaContratacion() {
		return this.getAsDate( FECHA_CONTRATACION );
	}

	public Date getFechaLiquidacion() {
		return this.getAsDate( FECHA_DE_LIQUIDACION );
	}

	public String getCargoAbono() {
		return this.getAsString( CARGO_ABONO );
	}

	public String getIdCanonYCorretaje() {
		return this.getAsString( ID_CANON_Y_CORRETAJE );
	}

	public String getMiembroMercado() {
		return this.getAsString( MIEMBRO_DE_MERCADO );
	}

	public BigDecimal getImporte() {
		return this.getAsNumber( IMPORTE );
	}

	public void setEntidadParticipante( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_PARTICIPANTE, valor );
	}

	public void setCuentaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( CUENTA_DE_LIQUIDACION, valor );
	}

	public void setFechaContratacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_CONTRATACION, valor );
	}

	public void setFechaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_DE_LIQUIDACION, valor );
	}

	public void setCargoAbono( final String valor ) throws PTIMessageException {
		this.set( CARGO_ABONO, valor );
	}

	public void setIdCanonYCorretaje( final String valor ) throws PTIMessageException {
		this.set( ID_CANON_Y_CORRETAJE, valor );
	}

	public void setMiembroMercado( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_DE_MERCADO, valor );
	}

	public void setImporte( final String valor ) throws PTIMessageException {
		this.set( IMPORTE, valor );
	}


}
