package sibbac.pti.messages.rt.t2s;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.rt.R01;


/**
 * Registro R01 para el mensaje de tipo: RT. (Asignacion Referencias
 * Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01T2s extends R01 {

  // -------------------------------------------- La lista de campos.
  // Datos referencia titular
  public static final String NUM_VALORES_IMPORTE_NOMINAL = "NUM_VALORES_IMPORTE_NOMINAL";


  // Tamanyos de los campos.
  public static final int SIZE_NUM_VALORES_IMPORTE_NOMINAL = 25;
  
  public static final int SCALE_NUM_VALORES_IMPORTE_NOMINAL = 9;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, REFERENCIA_TITULAR, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_TITULAR ),
			new Field( 2, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 3, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, SCALE_NUM_VALORES_IMPORTE_NOMINAL ),
			new Field( 4, CODIGO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_ERROR )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01T2s() throws PTIMessageException {
		super( FIELDS);
	}
}
