package sibbac.pti.messages.ac;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R00 para el mensaje de tipo: AC. (Aceptacion/Rechazo, Cancelacion movimiento entre Cuentas de Compensacion).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

  // -------------------------------------------- La lista de campos.

  //
  public static final String REFERENCIA_MOVIMIENTO = "REFERENCIA_MOVIMIENTO";
  public static final String REFERENCIA_NOTIFICACION_PREVIA = "REFERENCIA_NOTIFICACION_PREVIA";
  public static final String REFERENCIA_MOVIMIENTO_ECC = "REFERENCIA_MOVIMIENTO_ECC";
  public static final String ACTUACION = "ACTUACION";
  public static final String FECHA_TEORICA_LIQUIDACION = "FECHA_TEORICA_LIQUIDACION";
  public static final String SENTIDO = "SENTIDO";
  public static final String NUMERO_VALORES_IMPORTE_NOMINAL = "NUMERO_VALORES_IMPORTE_NOMINAL";

  // Datos Cuenta Destino
  public static final String CUENTA_COMPENSACION_DESTINO = "CUENTA_COMPENSACION_DESTINO";

  // Tamanyos de los campos.
  public static final int SIZE_REFERENCIA_MOVIMIENTO = 10;
  public static final int SIZE_REFERENCIA_NOTIFICACION_PREVIA = 9;
  public static final int SIZE_REFERENCIA_MOVIMIENTO_ECC = 10;
  public static final int SIZE_ACTUACION = 1;
  public static final int SIZE_FECHA_TEORICA_LIQUIDACION = 8;
  public static final int SIZE_SENTIDO = 1;
  public static final int SIZE_NUMERO_VALORES_IMPORTE_NOMINAL = 18;
  public static final int SIZE_CUENTA_COMPENSACION_DESTINO = 3;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { 
		  new Field(1, REFERENCIA_MOVIMIENTO, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO), 
		  new Field(2, REFERENCIA_NOTIFICACION_PREVIA, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_NOTIFICACION_PREVIA), 
		  new Field(3, REFERENCIA_MOVIMIENTO_ECC, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO_ECC), 
		  new Field(4, ACTUACION, PTIMessageFieldType.ASCII, SIZE_ACTUACION), 
		  new Field(5, FECHA_TEORICA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_TEORICA_LIQUIDACION), 
		  new Field(6, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO), 
		  new Field(7, NUMERO_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUMERO_VALORES_IMPORTE_NOMINAL, 6), 
		  new Field(8, CUENTA_COMPENSACION_DESTINO, PTIMessageFieldType.ASCII, SIZE_CUENTA_COMPENSACION_DESTINO)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R00() throws PTIMessageException {
    super(PTIMessageRecordType.R00, FIELDS);
  }

  public String getReferenciaDeMovimiento() {
    return this.getAsString(REFERENCIA_MOVIMIENTO);
  }

  public String getReferenciaNotificacionPrevia() {
    return this.getAsString(REFERENCIA_NOTIFICACION_PREVIA);
  }

  public String getReferenciaDeMovimientoECC() {
    return this.getAsString(REFERENCIA_MOVIMIENTO_ECC);
  }

  public String getActuacion() {
    return this.getAsString(ACTUACION);
  }

  public Date getFechaTeoricaLiquidacion() {
    return this.getAsDate(FECHA_TEORICA_LIQUIDACION);
  }

  public String getSentido() {
    return this.getAsString(SENTIDO);
  }

  public BigDecimal getNumeroDeValoresImporteNominal() {
    return this.getAsNumber(NUMERO_VALORES_IMPORTE_NOMINAL);
  }

  public String getCuentaCompensacionDestino() {
    return this.getAsString(CUENTA_COMPENSACION_DESTINO);
  }

  public void setReferenciaDeMovimiento(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_MOVIMIENTO, valor);
  }

  public void setReferenciaNotificacionPrevia(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_NOTIFICACION_PREVIA, valor);
  }

  public void setReferenciaDeMovimientoECC(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_MOVIMIENTO_ECC, valor);
  }

  public void setActuacion(final String valor) throws PTIMessageException {
    this.set(ACTUACION, valor);
  }

  public void setFechaTeoricaLiquidacion(final Date valor) throws PTIMessageException {
    this.set(FECHA_TEORICA_LIQUIDACION, valor);
  }

  public void setSentido(final String valor) throws PTIMessageException {
    this.set(SENTIDO, valor);
  }

  public void setNumeroDeValoresImporteNominal(final String valor) throws PTIMessageException {
    this.set(NUMERO_VALORES_IMPORTE_NOMINAL, valor);
  }

  public void setNumeroDeValoresImporteNominal(final BigDecimal valor) throws PTIMessageException {
    this.set(NUMERO_VALORES_IMPORTE_NOMINAL, valor);
  }

  public void setCuentaCompensacionDestino(final String valor) throws PTIMessageException {
    this.set(CUENTA_COMPENSACION_DESTINO, valor);
  }

}
