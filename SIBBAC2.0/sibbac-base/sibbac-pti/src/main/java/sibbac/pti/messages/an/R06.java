package sibbac.pti.messages.an;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R06 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R06 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Datos de liquidacion
  public static final String ID_ECC = "ID_ECC";
  public static final String SEGMENTO_ECC = "SEGMENTO_ECC";
  public static final String CANON_BURSATIL_TEORICO = "CANON_BURSATIL_TEORICO";
  public static final String CANON_BURSATIL_APLICADO = "CANON_BURSATIL_APLICADO";
  // public static final String CORRETAJE = "CORRETAJE";
  public static final String SITUACION_OPERACION = "SITUACION_OPERACION";
  public static final String TITULOS_FALLIDOS_EN_OP = "TITULOS_FALLIDOS_EN_OP";

  // Tamanyos de los campos.
  public static final int SIZE_ID_ECC = 11;
  public static final int SIZE_SEGMENTO_ECC = 2;
  public static final int SIZE_CANON_BURSATIL_TEORICO = 15;
  public static final int SIZE_CANON_BURSATIL_APLICADO = 15;
  // public static final int SIZE_CORRETAJE = 15;
  public static final int SIZE_SITUACION_OPERACION = 1;
  public static final int SIZE_TITULOS_FALLIDOS_EN_OP = 18;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC), new Field(
                                                                                                                 2,
                                                                                                                 SEGMENTO_ECC,
                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                 SIZE_SEGMENTO_ECC), new Field(
                                                                                                                                               3,
                                                                                                                                               CANON_BURSATIL_TEORICO,
                                                                                                                                               PTIMessageFieldType.NUMBER,
                                                                                                                                               SIZE_CANON_BURSATIL_TEORICO,
                                                                                                                                               2), new Field(
                                                                                                                                                             4,
                                                                                                                                                             CANON_BURSATIL_APLICADO,
                                                                                                                                                             PTIMessageFieldType.NUMBER,
                                                                                                                                                             SIZE_CANON_BURSATIL_APLICADO,
                                                                                                                                                             2),
                                        // new Field( 5, CORRETAJE, PTIMessageFieldType.NUMBER, SIZE_CORRETAJE, 4 ),
  new Field(5, SITUACION_OPERACION, PTIMessageFieldType.ASCII, SIZE_SITUACION_OPERACION), new Field(
                                                                                                    6,
                                                                                                    TITULOS_FALLIDOS_EN_OP,
                                                                                                    PTIMessageFieldType.NUMBER,
                                                                                                    SIZE_TITULOS_FALLIDOS_EN_OP,
                                                                                                    6)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R06() throws PTIMessageException {
    super(PTIMessageRecordType.R06, FIELDS);
  }

  // Getters
  public String getIdECC() {
    return this.getAsString(ID_ECC);
  }

  public String getSegmentoECC() {
    return this.getAsString(SEGMENTO_ECC);
  }

  public BigDecimal getCanonBursatilTeorico() {
    return this.getAsNumber(CANON_BURSATIL_TEORICO);
  }

  public BigDecimal getCanonBursatilAplicado() {
    return this.getAsNumber(CANON_BURSATIL_APLICADO);
  }

  // public BigDecimal getCorretaje() {
  // return this.getAsNumber( CORRETAJE );
  // }

  public String getSituacionOperacion() {
    return this.getAsString(SITUACION_OPERACION);
  }

  public BigDecimal getTitulosFallidosEnOP() {
    return this.getAsNumber(TITULOS_FALLIDOS_EN_OP);
  }

  // Setters
  public void setIdECC(final String valor) throws PTIMessageException {
    this.set(ID_ECC, valor);
  }

  public void setSegmentoECC(final String valor) throws PTIMessageException {
    this.set(SEGMENTO_ECC, valor);
  }

  public void setCanonBursatilTeorico(final String valor) throws PTIMessageException {
    this.set(CANON_BURSATIL_TEORICO, valor);
  }

  public void setCanonBursatilAplicado(final String valor) throws PTIMessageException {
    this.set(CANON_BURSATIL_APLICADO, valor);
  }

  // public void setCorretaje( final String valor ) throws PTIMessageException {
  // this.set( CORRETAJE, valor );
  // }

  public void setSituacionOperacion(final String valor) throws PTIMessageException {
    this.set(SITUACION_OPERACION, valor);
  }

  public void setTitulosFallidosEnOP(final String valor) throws PTIMessageException {
    this.set(TITULOS_FALLIDOS_EN_OP, valor);
  }

}
