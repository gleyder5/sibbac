package sibbac.pti.messages.pt;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: PT.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos Peticion
	public static final String	TIPO_DE_PETICION						= "TIPO_DE_PETICION";
	public static final String	PARAMETROS_POR_TIPO_DE_PETICION			= "PARAMETROS_POR_TIPO_DE_PETICION";
	public static final String	CODIGO_DE_ERROR							= "CODIGO_DE_ERROR";
	public static final String	TEXTO_ERROR								= "TEXTO_ERROR";
	public static final String	FECHA_DE_PETICION						= "FECHA_DE_PETICION";
	public static final String	NUMERO_DE_PETICION						= "NUMERO_DE_PETICION";
	public static final String	NUMERO_DE_LOTE							= "NUMERO_DE_LOTE";
	public static final String	SITUACION_DE_PETICION					= "SITUACION_DE_PETICION";

	// Tamanyos de los campos.
	public static final int		SIZE_TIPO_DE_PETICION					= 8;
	public static final int		SIZE_PARAMETROS_POR_TIPO_DE_PETICION	= 200;
	public static final int		SIZE_CODIGO_DE_ERROR					= 3;
	public static final int		SIZE_TEXTO_ERROR						= 40;
	public static final int		SIZE_FECHA_DE_PETICION					= 8;
	public static final int		SIZE_NUMERO_DE_PETICION					= 9;
	public static final int		SIZE_NUMERO_DE_LOTE						= 9;
	public static final int		SIZE_SITUACION_DE_PETICION				= 1;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, TIPO_DE_PETICION, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_PETICION ),
			new Field( 2, PARAMETROS_POR_TIPO_DE_PETICION, PTIMessageFieldType.NUMBER, SIZE_PARAMETROS_POR_TIPO_DE_PETICION, 6 ),
			new Field( 3, CODIGO_DE_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_ERROR ),
			new Field( 4, TEXTO_ERROR, PTIMessageFieldType.ASCII, SIZE_TEXTO_ERROR ),
			new Field( 5, FECHA_DE_PETICION, PTIMessageFieldType.ASCII, SIZE_FECHA_DE_PETICION ),
			new Field( 6, NUMERO_DE_PETICION, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_PETICION, 0 ),
			new Field( 7, NUMERO_DE_LOTE, PTIMessageFieldType.NUMBER, SIZE_NUMERO_DE_LOTE, 0 ),
			new Field( 8, SITUACION_DE_PETICION, PTIMessageFieldType.ASCII, SIZE_SITUACION_DE_PETICION )
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getTipoDePeticion() {
		return this.getAsString( TIPO_DE_PETICION );
	}

	public String getParametrosPorTipoDePeticion() {
		return this.getAsString( PARAMETROS_POR_TIPO_DE_PETICION );
	}

	public String getCodigoDeError() {
		return this.getAsString( CODIGO_DE_ERROR );
	}

	public String getTextoError() {
		return this.getAsString( TEXTO_ERROR );
	}

	public Date getFechaDePeticion() {
		return this.getAsDate( FECHA_DE_PETICION );
	}

	public BigDecimal getNumeroDePeticion() {
		return this.getAsNumber( NUMERO_DE_PETICION );
	}

	public BigDecimal getNumeroDeLote() {
		return this.getAsNumber( NUMERO_DE_LOTE );
	}

	public String getSituacionDePeticion() {
		return this.getAsString( SITUACION_DE_PETICION );
	}

	public void setTipoDePeticion( final String valor ) throws PTIMessageException {
		this.set( TIPO_DE_PETICION, valor );
	}

	public void setParametrosPorTipoDePeticion( final String valor ) throws PTIMessageException {
		this.set( PARAMETROS_POR_TIPO_DE_PETICION, valor );
	}

	public void setTextoError( final String valor ) throws PTIMessageException {
		this.set( TEXTO_ERROR, valor );
	}

	public void setCodigoError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_ERROR, valor );
	}

	public void setFechaPeticion( final String valor ) throws PTIMessageException {
		this.set( FECHA_DE_PETICION, valor );
	}

	public void setNumeroDePeticion( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_PETICION, valor );
	}

	public void setNumeroDeLote( final String valor ) throws PTIMessageException {
		this.set( NUMERO_DE_LOTE, valor );
	}

	public void setSituacionDePeticion( final String valor ) throws PTIMessageException {
		this.set( SITUACION_DE_PETICION, valor );
	}


}
