package sibbac.pti;


import sibbac.common.SIBBACCommons;


/**
 * Enumeracion con la lista de tipos de mensajes PTI reconocidos por SIBBAC.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public enum PTIMessageType {

	/**
	 * Aceptacion, rechazo, cancelacion de Asignaciones Externas
	 */
	AC( true ),

	/**
	 * Anotacion de operaciones en la ECC
	 */
	AN( true ),

	/**
	 * Comunicacion de Capacidad de Cuenta
	 */
	CE( true ),

	/**
	 * Fin de sesion
	 */
	FS( true, false ),

	/**
	 * Garantias y Movimientos de Efectivo Garantias y Movimiento
	 * de Efectivos
	 */
	GA01( true, false ),

	/**
	 * Solicitud Limite de Riesgo Intradia
	 */
	GA02( true, false ),

	/**
	 * Limite de Riesgo Intradia/ Garantias Extraordinarias
	 */
	GA03( true, false ),

	/**
	 * Detalle Garantias Depositadas
	 */
	GA04( true, false ),

	/**
	 * Gestion de movimientos entre cuentas Notificacion de
	 * Retencion y Liberacion de Valores Notificacion de Cancelacion Prestamo de
	 * Valores de ultima instancia
	 */
	MO( true, true ),

	/**
	 * Gestion de operaciones
	 */
	OP( true ),

	/**
	 * Peticiones de informacion
	 */
	PT( true ),

	/**
	 * Informacion de Precios Informacion del Precio de Cierre de
	 * los Valores
	 */
	PV( true ),

	/**
	 * Resumen de canones y corretajes
	 */
	RC( true ),

	/**
	 * Gestion de Referencias y Filtros de Asignaciones / Gestion
	 * Modulo de Parametrizacion
	 */
	RF( true ),

	/**
	 * Referencias de Titularidades
	 */
	RT( true ),

	/**
	 * Titularidades
	 */
	TI( true ),

	/**
	 * Informacion de Supervision Informacion de Supervision
	 */
	TXT( true ),

	/**
	 * UNMANAGED YET! Informacion de Valores Informacion de Valores
	 */
	VA( true ),

	/**
	 * UNMANAGED YET! Detalle de fallidos
	 */
	VF( true ),

	/**
	 * Canones de Mercado
	 */
	CM( true ),

	/**
	 * Informe de corretajes
	 */
	IC( true ),

	/**
	 * Comunicacion de corretajes
	 */
	CC( true ),

	/**
	 * Desconocido
	 */
	UNKNOWN;

	private boolean	commonDataBlock;
	private boolean	controlBlock;

	private PTIMessageType() {
		this( false );
	}

	/**
	 * Constructor para el caso en el que un tipo de mensaje tenga bloque de
	 * control.
	 * 
	 * @param controlBlock
	 *            True o false, si tiene bloque de control.
	 */
	private PTIMessageType( final boolean controlBlock ) {
		this( false, controlBlock );
	}

	/**
	 * Constructor para el caso en el que un tipo de mensaje tenga bloque de
	 * control y bloque de datos comunes.
	 * 
	 * @param commonDataBlock
	 *            True o false, si tiene bloque de datos comunes.
	 * @param controlBlock
	 *            True o false, si tiene bloque de control.
	 */
	private PTIMessageType( final boolean commonDataBlock, final boolean controlBlock ) {
		this.commonDataBlock = commonDataBlock;
		this.controlBlock = controlBlock;
	}

	/**
	 * Getter for commonDataBlock
	 *
	 * @return the commonDataBlock
	 */
	public boolean hasCommonDataBlock() {
		return this.commonDataBlock;
	}

	/**
	 * Getter for controlBlock
	 *
	 * @return the controlBlock
	 */
	public boolean hasControlBlock() {
		return this.controlBlock;
	}

	/**
	 * Metodo de conveniencia para determinar el tipo de mensaje a partir de un
	 * texto.
	 * 
	 * @param tipo
	 *            Un {@link String}
	 * @return Una instancia de {@link PTIMessageType}.
	 */
	public static PTIMessageType parse( final String tipo ) {
		return ( tipo == null ) ? UNKNOWN : PTIMessageType.valueOf( tipo.toUpperCase().trim() );
	}

	/**
	 * Metodo de conveniencia para ajustar el nombre del tipo de mensaje a un
	 * tamanyo dado.
	 * 
	 * @param size
	 *            Un entero con el tamanyo deseado.
	 * @return Un {@link String} con el nombre ya ajustado.
	 */
	public String getNameSized( final int size ) {
		return SIBBACCommons.adjustString( this.name(), size );
	}
}
