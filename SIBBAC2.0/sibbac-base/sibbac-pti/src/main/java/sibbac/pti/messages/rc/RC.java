package sibbac.pti.messages.rc;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;


/**
 * Mensaje de tipo RC.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class RC extends AMessage {

	/**
	 * Constructor por defecto.
	 */
	public RC() throws PTIMessageException {
		this( false );
	}

	/**
	 * Constructor a partir de PTI.
	 * 
	 * @param fromPTI
	 *            TRUE o FALSE, dependiendo de si el registro se genera a partir
	 *            de datos de PTI.
	 */
	public RC( final boolean fromPTI ) throws PTIMessageException {
		super( PTIMessageType.RC, fromPTI );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
	 */
	@Override
	public PTIMessageRecord getRecordInstance( final PTIMessageRecordType tipo ) throws PTIMessageException {
		switch ( tipo ) {
			case R00:
				return new R00();
			default:
				return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.messages.RCessage#setup()
	 */
	@Override
	public void messageDefinition() throws PTIMessageException {
		// El bloque de datos comunes
		this.commonDataBlock = null;
		// El bloque de control.
		this.controlBlock = new RCControlBlock();
	}

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub
    
  }


}
