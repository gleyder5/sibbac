package sibbac.pti.messages.ga01;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R03 para el mensaje de tipo: GA01 (Garantias y Movimientos de
 * Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Risk Limit
	public static final String	RISKLIMITTYPE						= "RISKLIMITTYPE";
	public static final String	RISKLIMITAMOUNT						= "RISKLIMITAMOUNT";
	public static final String	MARGINAMOUNTMARKETSEGMENTID			= "MARGINAMOUNTMARKETSEGMENTID";
	public static final String	MARGINAMOUNTMARKETID				= "MARGINAMOUNTMARKETID";

	// Tamanyos de los campos.
	public static final int		SIZE_RISKLIMITTYPE					= 3;
	public static final int		SIZE_RISKLIMITAMOUNT				= 15;
	public static final int		SIZE_MARGINAMOUNTMARKETSEGMENTID	= 2;
	public static final int		SIZE_MARGINAMOUNTMARKETID			= 2;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, RISKLIMITTYPE, PTIMessageFieldType.ASCII, SIZE_RISKLIMITTYPE ),
			new Field( 2, RISKLIMITAMOUNT, PTIMessageFieldType.NUMBER, SIZE_RISKLIMITAMOUNT, 2 ),
			new Field( 3, MARGINAMOUNTMARKETSEGMENTID, PTIMessageFieldType.ASCII, SIZE_MARGINAMOUNTMARKETSEGMENTID ),
			new Field( 4, MARGINAMOUNTMARKETID, PTIMessageFieldType.ASCII, SIZE_MARGINAMOUNTMARKETID )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R03() throws PTIMessageException {
		super( PTIMessageRecordType.R03, FIELDS );
	}

	public String getRiskLimitType() {
		return this.getAsString( RISKLIMITTYPE );
	}

	public BigDecimal getRiskLimitAmount() {
		return this.getAsNumber( RISKLIMITAMOUNT );
	}

	public String getMarginAmountMarketSegmentId() {
		return this.getAsString( MARGINAMOUNTMARKETSEGMENTID );
	}

	public String getMarginAmountMarketId() {
		return this.getAsString( MARGINAMOUNTMARKETID );
	}

	public void setRiskLimitType( final String valor ) throws PTIMessageException {
		this.set( RISKLIMITTYPE, valor );
	}

	public void setRiskLimitAmount( final String valor ) throws PTIMessageException {
		this.set( RISKLIMITAMOUNT, valor );
	}

	public void setMarginAmountMarketSegmentId( final String valor ) throws PTIMessageException {
		this.set( MARGINAMOUNTMARKETSEGMENTID, valor );
	}

	public void setMarginAmountMarketId( final String valor ) throws PTIMessageException {
		this.set( MARGINAMOUNTMARKETID, valor );
	}


}
