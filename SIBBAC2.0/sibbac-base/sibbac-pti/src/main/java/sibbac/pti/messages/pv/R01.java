package sibbac.pti.messages.pv;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R01 para el mensaje de tipo: PV. (Informacion de precios).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

  // -------------------------------------------- La lista de campos.
  // Datos referencias asignacion
  public static final String TIPO_INFORMACION_DE_PRECIOS = "TIPO_INFORMACION_DE_PRECIOS";
  public static final String PRECIO = "PRECIO";
  public static final String TIPO_DE_PRECIO_DE_CIERRE = "TIPO_DE_PRECIO_DE_CIERRE";

  public static final int SIZE_TIPO_INFORMACION_DE_PRECIOS = 1;
  public static final int SIZE_PRECIO = 13;
  public static final int SIZE_TIPO_DE_PRECIO_DE_CIERRE = 1;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, TIPO_INFORMACION_DE_PRECIOS, PTIMessageFieldType.ASCII,
                                                   SIZE_TIPO_INFORMACION_DE_PRECIOS), new Field(
                                                                                                2,
                                                                                                PRECIO,
                                                                                                PTIMessageFieldType.NUMBER,
                                                                                                SIZE_PRECIO, 6), new Field(
                                                                                                                           3,
                                                                                                                           TIPO_DE_PRECIO_DE_CIERRE,
                                                                                                                           PTIMessageFieldType.ASCII,
                                                                                                                           SIZE_TIPO_DE_PRECIO_DE_CIERRE)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R01() throws PTIMessageException {
    super(PTIMessageRecordType.R01, FIELDS);
  }
  
  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException
   *             Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  protected R01(final Field[] recordFields) throws PTIMessageException {
    super( PTIMessageRecordType.R01 , recordFields );
  }

  public String getTipoInformacionPrecios() {
    return this.getAsString(TIPO_INFORMACION_DE_PRECIOS);
  }

  public BigDecimal getPrecio() {
    return this.getAsNumber(PRECIO);
  }

  public String getTipoPrecioCierre() {
    return this.getAsString(TIPO_DE_PRECIO_DE_CIERRE);
  }

  public void setTipoInformacionPrecios(final String valor) throws PTIMessageException {
    this.set(TIPO_INFORMACION_DE_PRECIOS, valor);
  }

  public void setPrecio(final String valor) throws PTIMessageException {
    this.set(PRECIO, valor);
  }

  public void setTipoPrecioCierre(final String valor) throws PTIMessageException {
    this.set(TIPO_DE_PRECIO_DE_CIERRE, valor);
  }

}
