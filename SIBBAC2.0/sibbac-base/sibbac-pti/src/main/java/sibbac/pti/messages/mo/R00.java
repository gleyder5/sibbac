package sibbac.pti.messages.mo;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: MO. (Datos Cuenta Origen).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Datos Cuenta Origen
	public static final String	REFERENCIA_MOVIMIENTO				= "REFERENCIA_MOVIMIENTO";
	public static final String	NUM_VALORES_IMPORTE_NOMINAL			= "NUM_VALORES_IMPORTE_NOMINAL";
	public static final String	REFERENCIA_INTERNA_ASIGNACION		= "REFERENCIA_INTERNA_ASIGNACION";
	public static final String	REFERENCIA_ASIGNACION				= "REFERENCIA_ASIGNACION";
	public static final String	NEMOTECNICO							= "NEMOTECNICO";

	// Datos Cuenta Destino
	public static final String	MIEMBRO_DESTINO						= "MIEMBRO_DESTINO";
	public static final String	CUENTA_COMPENSACION_DESTINO			= "CUENTA_COMPENSACION_DESTINO";

	// Datos a Devolver
	public static final String	REFERENCIA_MOVIMIENTO_ECC			= "REFERENCIA_MOVIMIENTO_ECC";
	public static final String	REFERENCIA_NOTIFICACION				= "REFERENCIA_NOTIFICACION";
	public static final String	TIPO_MOVIMIENTO						= "TIPO_MOVIMIENTO";
	public static final String	ESTADO								= "ESTADO";
	public static final String	USUARIO_ORIGEN						= "USUARIO_ORIGEN";
	public static final String	MIEMBRO_DESTINO_ASIGNACION			= "MIEMBRO_DESTINO_ASIGNACION";
	public static final String	USUARIO_DESTINO						= "USUARIO_DESTINO";
	public static final String	EFECTIVO							= "EFECTIVO";
	public static final String	CODIGO_VALOR						= "CODIGO_VALOR";

	// Tamanyos de los campos.
	public static final int		SIZE_REFERENCIA_MOVIMIENTO			= 10;
	public static final int		SIZE_NUM_VALORES_IMPORTE_NOMINAL	= 18;
	public static final int		SIZE_REFERENCIA_INTERNA_ASIGNACION	= 18;
	public static final int		SIZE_REFERENCIA_ASIGNACION			= 18;
	public static final int		SIZE_NEMOTECNICO					= 10;
	public static final int		SIZE_MIEMBRO_DESTINO				= 4;
	public static final int		SIZE_CUENTA_COMPENSACION_DESTINO	= 3;
	public static final int		SIZE_REFERENCIA_MOVIMIENTO_ECC		= 10;
	public static final int		SIZE_REFERENCIA_NOTIFICACION		= 9;
	public static final int		SIZE_TIPO_MOVIMIENTO				= 2;
	public static final int		SIZE_ESTADO							= 2;
	public static final int		SIZE_USUARIO_ORIGEN					= 3;
	public static final int		SIZE_MIEMBRO_DESTINO_ASIGNACION		= 4;
	public static final int		SIZE_USUARIO_DESTINO				= 3;
	public static final int		SIZE_EFECTIVO						= 15;
	public static final int		SIZE_CODIGO_VALOR					= 12;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, REFERENCIA_MOVIMIENTO, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO ),
			new Field( 2, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 3, REFERENCIA_INTERNA_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_INTERNA_ASIGNACION ),
			new Field( 4, REFERENCIA_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_ASIGNACION ),
			new Field( 5, NEMOTECNICO, PTIMessageFieldType.ASCII, SIZE_NEMOTECNICO ),
			new Field( 6, MIEMBRO_DESTINO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_DESTINO ),
			new Field( 7, CUENTA_COMPENSACION_DESTINO, PTIMessageFieldType.ASCII, SIZE_CUENTA_COMPENSACION_DESTINO ),
			new Field( 8, REFERENCIA_MOVIMIENTO_ECC, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_MOVIMIENTO_ECC ),
			new Field( 9, REFERENCIA_NOTIFICACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_NOTIFICACION ),
			new Field( 10, TIPO_MOVIMIENTO, PTIMessageFieldType.ASCII, SIZE_TIPO_MOVIMIENTO ),
			new Field( 11, ESTADO, PTIMessageFieldType.ASCII, SIZE_ESTADO ),
			new Field( 12, USUARIO_ORIGEN, PTIMessageFieldType.ASCII, SIZE_USUARIO_ORIGEN ),
			new Field( 13, MIEMBRO_DESTINO_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_DESTINO_ASIGNACION ),
			new Field( 14, USUARIO_DESTINO, PTIMessageFieldType.ASCII, SIZE_USUARIO_DESTINO ),
			new Field( 15, EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_EFECTIVO, 2 ),
			new Field( 16, CODIGO_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_VALOR )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getReferenciaDeMovimiento() {
		return this.getAsString( REFERENCIA_MOVIMIENTO );
	}

	public BigDecimal getNumeroDeValoresImporteNominal() {
		return this.getAsNumber( NUM_VALORES_IMPORTE_NOMINAL );
	}

	public String getReferenciaInternaDeAsignacion() {
		return this.getAsString( REFERENCIA_INTERNA_ASIGNACION );
	}

	public String getReferenciaDeAsignacion() {
		return this.getAsString( REFERENCIA_ASIGNACION );
	}

	public String getNemotecnico() {
		return this.getAsString( NEMOTECNICO );
	}

	public String getMiembroDestino() {
		return this.getAsString( MIEMBRO_DESTINO );
	}

	public String getCuentaCompensacionDestino() {
		return this.getAsString( CUENTA_COMPENSACION_DESTINO );
	}

	public String getReferenciaDeMovimientoECC() {
		return this.getAsString( REFERENCIA_MOVIMIENTO_ECC );
	}

	public String getReferenciaDeNotificacion() {
		return this.getAsString( REFERENCIA_NOTIFICACION );
	}

	public String getTipoDeMovimiento() {
		return this.getAsString( TIPO_MOVIMIENTO );
	}

	public String getEstado() {
		return this.getAsString( ESTADO );
	}

	public String getUsuarioOrigen() {
		return this.getAsString( USUARIO_ORIGEN );
	}

	public String getMiembroDestinoDeLaAsignacion() {
		return this.getAsString( MIEMBRO_DESTINO_ASIGNACION );
	}

	public String getUsuarioDestino() {
		return this.getAsString( USUARIO_DESTINO );
	}

	public BigDecimal getEfectivo() {
		return this.getAsNumber( EFECTIVO );
	}
	
	public String getCodigoValor() {
		return this.getAsString( CODIGO_VALOR );
	}

	public void setReferenciaDeMovimiento( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO, valor );
	}

	public void setNumeroDeValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setNumeroDeValoresImporteNominal( final BigDecimal valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setReferenciaInternaDeAsignacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_INTERNA_ASIGNACION, valor );
	}

	public void setReferenciaDeAsignacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_ASIGNACION, valor );
	}

	public void setNemotecnico( final String valor ) throws PTIMessageException {
		this.set( NEMOTECNICO, valor );
	}

	public void setMiembroDestino( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_DESTINO, valor );
	}

	public void setCuentaCompensacionDestino( final String valor ) throws PTIMessageException {
		this.set( CUENTA_COMPENSACION_DESTINO, valor );
	}

	public void setReferenciaDeMovimientoECC( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_MOVIMIENTO_ECC, valor );
	}

	public void setReferenciaDeNotificacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_NOTIFICACION, valor );
	}

	public void setTipoDeMovimiento( final String valor ) throws PTIMessageException {
		this.set( TIPO_MOVIMIENTO, valor );
	}

	public void setEstado( final String valor ) throws PTIMessageException {
		this.set( ESTADO, valor );
	}

	public void setUsuarioOrigen( final String valor ) throws PTIMessageException {
		this.set( USUARIO_ORIGEN, valor );
	}

	public void setMiembroDestinoDeLaAsignacion( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_DESTINO_ASIGNACION, valor );
	}

	public void setUsuarioDestino( final String valor ) throws PTIMessageException {
		this.set( USUARIO_DESTINO, valor );
	}

	public void setEfectivo( final BigDecimal valor ) throws PTIMessageException {
		this.set( EFECTIVO, valor );
	}
	
	public void setCodigoValor( final String valor ) throws PTIMessageException {
		this.set( CODIGO_VALOR, valor );
	}


}
