package sibbac.pti.messages;


import java.math.BigDecimal;

import sibbac.common.SIBBACCommons;
import sibbac.pti.PTIMessageControlBlock;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageField;
import sibbac.pti.PTIMessageRecordType;


public abstract class AControlBlock extends ARecord implements PTIMessageControlBlock {

	/**
	 * Constructor nulo.
	 */
	protected AControlBlock() throws PTIMessageException {
		this( PTIMessageRecordType.UNKNOWN, null );
	}

	public AControlBlock( final PTIMessageRecordType tipo, final Field[] recordFields ) throws PTIMessageException {
		super( tipo, recordFields );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageControlBlock#getNumeroDeRegistros(sibbac.pti.
	 * PTIMessageRecordType)
	 */
	@Override
	public final int getNumeroDeRegistros( final PTIMessageRecordType tipo ) {
		PTIMessageField field = null;
		BigDecimal bdValue = null;
		int intValue = 0;
		switch ( tipo ) {
			case UNKNOWN:
			case CONTROL_BLOCK:
			case HEADER:
				return 1;
			default:
				try {
					field = this.fields.get( tipo.name() );
					if ( field != null ) {
						bdValue = field.getValueAsBigDecimal();
						intValue = bdValue.intValue();
					} else {
						intValue = 0;
					}
					return intValue;
				} catch ( NumberFormatException e ) {
					return -1;
				} catch ( PTIMessageException e ) {
					return -1;
				}
		}
	}

	@Override
	public final void setNumRegistros( final PTIMessageRecordType tipo, final int value ) throws PTIMessageException {
		/*
		 * String recValue = this.get(tipo.name()); int numRecords =
		 * this.getNumeroDeRegistros(tipo); int intValue =
		 * Integer.parseInt(recValue); int intNew = intValue + value;
		 */
		String strNew = String.valueOf( value );
		int size = this.findField( tipo ).getLongitud();
		strNew = SIBBACCommons.adjustString( strNew, size, false, "0" );
		this.set( tipo.name(), strNew );
	}

	@Override
	public final void addOneRecord( final PTIMessageRecordType tipo ) throws PTIMessageException {
		this.addNRecords( tipo, 1 );
	}

	@Override
	public final void addNRecords( final PTIMessageRecordType tipo, final int n ) throws PTIMessageException {
		int actual = this.getNumeroDeRegistros( tipo );
		this.setNumRegistros( tipo, actual + n );
	}
	
	private Field findField( final PTIMessageRecordType tipo ) {
		Field found = null;
		for ( Field field : this.recordFields ) {
			if ( field.getNombre().equalsIgnoreCase( tipo.name() ) ) {
				found = field;
				break;
			}
		}
		return found;
	}

}
