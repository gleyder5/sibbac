/**
 * 
 */
package sibbac.pti;

/**
 * @author xIS16630
 *
 */
public class PTIException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 6152525613116842073L;

  /**
   * 
   */
  public PTIException() {
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public PTIException(String arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   */
  public PTIException(Throwable arg0) {
    super(arg0);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   */
  public PTIException(String arg0, Throwable arg1) {
    super(arg0, arg1);
    // TODO Auto-generated constructor stub
  }

  /**
   * @param arg0
   * @param arg1
   * @param arg2
   * @param arg3
   */
  public PTIException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
    // TODO Auto-generated constructor stub
  }

}
