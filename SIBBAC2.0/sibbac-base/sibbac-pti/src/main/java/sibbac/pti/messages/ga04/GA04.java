package sibbac.pti.messages.ga04;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;


/**
 * Mensaje de tipo GA04.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA04 extends AMessage {

	/**
	 * Constructor por defecto.
	 */
	public GA04() throws PTIMessageException {
		this( false );
	}

	/**
	 * Constructor a partir de PTI.
	 * 
	 * @param fromPTI
	 *            TRUE o FALSE, dependiendo de si el registro se genera a partir
	 *            de datos de PTI.
	 */
	public GA04( final boolean fromPTI ) throws PTIMessageException {
		super( PTIMessageType.GA04, fromPTI );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
	 */
	@Override
	public PTIMessageRecord getRecordInstance( final PTIMessageRecordType tipo ) throws PTIMessageException {
		switch ( tipo ) {
			case R00:
				return new R00();
			default:
				return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.messages.AMessage#setup()
	 */
	@Override
	public void messageDefinition() throws PTIMessageException {
		// El bloque de datos comunes
		this.commonDataBlock = new GA04CommonDataBlock();

		// El bloque de control
		this.controlBlock = null;
	}

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub
    
  }


}
