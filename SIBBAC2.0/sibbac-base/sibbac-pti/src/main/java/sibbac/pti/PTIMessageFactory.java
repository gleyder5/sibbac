package sibbac.pti;

import sibbac.common.SIBBACCommons;
import sibbac.pti.messages.Header;
import sibbac.pti.messages.ac.AC;
import sibbac.pti.messages.an.AN;
import sibbac.pti.messages.an.t2s.ANT2s;
import sibbac.pti.messages.ce.CE;
import sibbac.pti.messages.fs.FS;
import sibbac.pti.messages.ga01.GA01;
import sibbac.pti.messages.ga02.GA02;
import sibbac.pti.messages.ga03.GA03;
import sibbac.pti.messages.ga04.GA04;
import sibbac.pti.messages.mo.MO;
import sibbac.pti.messages.op.OP;
import sibbac.pti.messages.pt.PT;
import sibbac.pti.messages.pv.PV;
import sibbac.pti.messages.pv.t2s.PVT2s;
import sibbac.pti.messages.rc.RC;
import sibbac.pti.messages.rf.RF;
import sibbac.pti.messages.rt.RT;
import sibbac.pti.messages.rt.t2s.RTT2s;
import sibbac.pti.messages.ti.TI;
import sibbac.pti.messages.ti.t2s.TIT2s;
import sibbac.pti.messages.txt.TXT;
import sibbac.pti.messages.va.VA;
import sibbac.pti.messages.va.t2s.VAT2s;

/**
 * Clase auxiliar de factoria para facilitar la generacion de mensajes PTI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class PTIMessageFactory {

  /**
   * PTI Access Credentials.
   */
  private static PTICredentials credentials;

  /**
   * Getter for credentials
   *
   * @return the credentials
   */
  public static PTICredentials getCredentials() {
    return credentials;
  }

  /**
   * Setter for credentials
   *
   * @param credentials the credentials to set
   */
  public static void setCredentials(final PTICredentials credentials) {
    PTIMessageFactory.credentials = credentials;
  }

  /**
   * Devuelve una instancia de un objeto {@link PTIMessage} basado en el {@link PTIMessageType} solicitado para ser
   * enviado a PTI.
   * 
   * @param tipo El tipo de mensaje deseado.
   * @return Una instancia de {@link PTIMessage}
   * @throws PTIMessageException Si ocurren errores a la hora de instanciar mensajes.
   */
  public static PTIMessage getInstance(final PTIMessageVersion version, final PTIMessageType tipo) throws PTIMessageException {
    return create(version, tipo, null, null);
  }

  /**
   * Devuelve un {@link PTIMessage} basado en el mensaje de tipo {@link String} recibido desde PTI.
   * 
   * @param strMensaje El texto con el mensaje completo.
   * @return Una instancia de {@link PRIMessage}. El tipo exacto viene definido por {@link PTIMessage#getTipo()}.
   * @throws PTIMessageException Si ocurre algun error parseando la cadena de entrada a formato mensaje.
   */
  public static PTIMessage parse(PTIMessageVersion version, final String strMensaje) throws PTIMessageException {

    // Extractamos la cabecera.
    String strHeader = SIBBACCommons.extractHeaderStringFromMessage(strMensaje);

    // Extractamos la parte del mensaje que no es la cabecera.
    String strMessage = strMensaje.substring(strHeader.length());

    // Con el texto especifico de la cabecera, nos creamos una instancia de "Header".
    PTIMessageHeader header = parseHeader(strHeader);

    // Obtenemos el tipo de mensaje PTI...
    PTIMessageType tipo = header.getTipoDeMensaje();

    // Creamos un mensaje
    PTIMessage mensaje = create(version, tipo, header, strMessage);

    return mensaje;
  }

  public static PTIMessage parseJDO() {
    // if (jdo == null) {
    // throw new PTIMessageException("JDO no recibido");
    // }

    PTIMessage mensaje = null;
    // Class clazz = jdo.getClass();
    // if ( clazz instanceof TptiInAc. ) {
    //
    // }
    /*
     * switch ( clazz ) { case TptiInAc.class: mensaje = new AC(); break; case AN: mensaje = new AN(); break; case CE:
     * mensaje = new CE(); break; case FS: mensaje = new FS(); break; case GA01: mensaje = new GA01(); break; case GA02:
     * mensaje = new GA02(); break; case GA03: mensaje = new GA03(); break; case GA04: mensaje = new GA04(); break; case
     * MO: mensaje = new MO(); break; case OP: mensaje = new OP(); break; case PT: mensaje = new PT(); break; case PV:
     * mensaje = new PV(); break; case RC: mensaje = new RC(); break; case RF: mensaje = new RF(); break; case RT:
     * mensaje = new RT(); break; case TI: mensaje = new TI(); break; case TXT: mensaje = new TXT(); break; case VA:
     * mensaje = new VA(); break; default: mensaje = null; }
     */
    // if (mensaje == null) {
    // throw new PTIMessageException("Tipo de mensaje [" + clazz
    // + "] no encontrado");
    // }

    return mensaje;
  }

  /**
   * Metodo de conveniencia para crear un nuevo mensaje PTI de un tipo dado.<br/>
   * Su uso permite crear un mensaje "raw" o a partir de un texto recibido.
   * 
   * @param tipo Una instancia de {@link PTIMessageType}; el tipo de mensaje.
   * @param header Un {@link PTIMessageHeader} con la instancia de la cabecera del mensaje PTI recibido y ya parseada.
   * @param cadena Un {@link String} con el texto de un mensaje PTI recibido.
   * @return Una instancia de {@link PTIMessage}.
   * @throws PTIMessageException Si algo ocurre a la hora de "crear" el mensaje.
   */
  private static PTIMessage create(final PTIMessageVersion version,
                                   final PTIMessageType tipo,
                                   final PTIMessageHeader header,
                                   final String cadena) throws PTIMessageException {
    PTIMessage mensaje = null;
    switch (tipo) {
      case AC:
        mensaje = new AC();
        break;
      case AN:
        if (version == PTIMessageVersion.VERSION_T2S) {
          mensaje = new ANT2s();
        } else {
          mensaje = new AN();
        }
        break;
      case CE:
        mensaje = new CE();
        break;
      case FS:
        mensaje = new FS();
        break;
      case GA01:
        mensaje = new GA01();
        break;
      case GA02:
        mensaje = new GA02();
        break;
      case GA03:
        mensaje = new GA03();
        break;
      case GA04:
        mensaje = new GA04();
        break;
      case MO:
        mensaje = new MO();
        break;
      case OP:
        mensaje = new OP();
        break;
      case PT:
        mensaje = new PT();
        break;
      case PV:
        if (version == PTIMessageVersion.VERSION_T2S) {
          mensaje = new PVT2s();
        } else {
          mensaje = new PV();
        }
        break;
      case RC:
        mensaje = new RC();
        break;
      case RF:
        mensaje = new RF();
        break;
      case RT:
        if (version == PTIMessageVersion.VERSION_T2S) {
          mensaje = new RTT2s();
        } else {
          mensaje = new RT();
        }
        break;
      case TI:
        if (version == PTIMessageVersion.VERSION_T2S) {
          mensaje = new TIT2s();
        } else {
          mensaje = new TI();
        }
        break;
      case TXT:
        mensaje = new TXT();
        break;
      case VA:
        if (version == PTIMessageVersion.VERSION_T2S) {
          mensaje = new VAT2s();
        } else {
          mensaje = new VA();
        }
        break;
      default:
        mensaje = null;
    }
    if (mensaje == null) {
      throw new PTIMessageException("Tipo de mensaje [" + tipo + "] no encontrado");
    }

    if (header != null) {
      mensaje.setHeader(header);
    }

    // Establecemos un encabezado comun.
    if (cadena != null && !cadena.isEmpty()) {
      // Para parsear.
      mensaje.setFromPTI(true);
      mensaje.parseMessage(cadena);
    }

    return mensaje;
  }

  /**
   * Metodo de conveniencia para parsear el encabezado de un mensaje.
   * 
   * @param strHeader El texto de la cabecera del mensaje.
   * @return Una instancia de {@link PTIMessageHeader}
   * @throws PTIMessageException Si ocurre algun error parseando el texto.
   */
  public static PTIMessageHeader parseHeader(final String strHeader) throws PTIMessageException {
    PTIMessageHeader header = new Header(strHeader);
    return header;
  }

}
