package sibbac.pti.messages.rf;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R02 para el mensaje de tipo: RF. (Gestion de Referencias y Filtros
 * de Asignacion Externa /Gestion Modulo Parametrizacion).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Datos referencias
	public static final String	TIPO_REFERENCIA_FILTRO								= "TIPO_REFERENCIA_FILTRO";
	public static final String	MIEMBRO												= "MIEMBRO";
	public static final String	REFERENCIA_ASIGNACION								= "REFERENCIA_ASIGNACION";
	public static final String	CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA		= "CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA";
	public static final String	TAL													= "TAL";
	public static final String	SAL													= "SAL";

	public static final int		SIZE_TIPO_REFERENCIA_FILTRO							= 5;
	public static final int		SIZE_MIEMBRO										= 4;
	public static final int		SIZE_REFERENCIA_ASIGNACION							= 18;
	public static final int		SIZE_CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA	= 3;
	public static final int		SIZE_TAL											= 12;
	public static final int		SIZE_SAL											= 12;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS												= {
			new Field( 1, TIPO_REFERENCIA_FILTRO, PTIMessageFieldType.ASCII, SIZE_TIPO_REFERENCIA_FILTRO ),
			new Field( 2, MIEMBRO, PTIMessageFieldType.ASCII, SIZE_MIEMBRO ),
			new Field( 3, REFERENCIA_ASIGNACION, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_ASIGNACION ),
			new Field( 4, CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA, PTIMessageFieldType.ASCII,
					SIZE_CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA ), new Field( 5, TAL, PTIMessageFieldType.NUMBER, SIZE_SAL, 2 ),
			new Field( 6, SAL, PTIMessageFieldType.NUMBER, SIZE_SAL, 2 )
																					};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02() throws PTIMessageException {
		super( PTIMessageRecordType.R02, FIELDS );
	}

	public String getTipoReferenciaFiltro() {
		return this.getAsString( TIPO_REFERENCIA_FILTRO );
	}

	public String getMiembro() {
		return this.getAsString( MIEMBRO );
	}

	public String getReferenciaAsignacion() {
		return this.getAsString( REFERENCIA_ASIGNACION );
	}

	public String getCuentaCompensacionDestinoAsignacionExterna() {
		return this.getAsString( CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA );
	}

	public BigDecimal getTal() {
		return this.getAsNumber( TAL );
	}

	public BigDecimal getSal() {
		return this.getAsNumber( SAL );
	}

	public void setTipoReferenciaFiltro( final String valor ) throws PTIMessageException {
		this.set( TIPO_REFERENCIA_FILTRO, valor );
	}

	public void setMiembro( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO, valor );
	}

	public void setReferenciaAsignacion( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_ASIGNACION, valor );
	}

	public void setCuentaCompensacionDestinoAsignacionExterna( final String valor ) throws PTIMessageException {
		this.set( CUENTA_COMPENSACION_DESTINO_ASIGNACION_EXTERNA, valor );
	}

	public void setTal( final String valor ) throws PTIMessageException {
		this.set( TAL, valor );
	}

	public void setSal( final String valor ) throws PTIMessageException {
		this.set( SAL, valor );
	}


}
