package sibbac.pti;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;


/**
 * Interfaz que define la operativa asociada a un campo PTI dado.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTIMessageField extends PTICommons, PTIConstants {

	/**
	 * Devuelve el numero de posiciones decimales en el caso de que el fampo sea
	 * numerico o con signo.
	 * 
	 * @return Un entero.
	 */
	public int getDecimales();

	/**
	 * 
	 * Devuelve el numero de posiciones enteras en el caso de que el fampo sea
	 * numerico o con signo.
	 * 
	 * @return Un entero.
	 */
	public int getEnteros();

	/**
	 * Devuelve la longitud total del campo.
	 * <p/>
	 * Para campos numericos, la longitud incluye el signo (en el caso de campos de tipo "NS"), la parte entera y los decimales, si los
	 * hubiere.
	 * 
	 * @return Un entero.
	 */
	public int getLongitud();

	/**
	 * Devuelve el nombre del campo.
	 * 
	 * @return Un {@link String}
	 */
	public String getNombre();

	/**
	 * Devuelve la posicion del campo en el registro.
	 * 
	 * @return Un entero.
	 */
	public int getOrden();

	/**
	 * Devuelve el tipo de campo.
	 * 
	 * @return Un {@link PTIMessageFieldType}
	 */
	public PTIMessageFieldType getTipoDeCampo();

	/**
	 * Devuelve el valor original (Raw) del campo.
	 * 
	 * @return Un {@link String}
	 */
	public String getRawValue();

	/**
	 * Devuelve el del valor del campo como un String sin espacios ("trimmed").
	 * 
	 * @return Un {@link String}
	 */
	public String getValueAsString();

	/**
	 * Devuelve el valor del campo en formato fecha.
	 * 
	 * @return Un {@link Date}
	 * @throws PTIMessageException
	 *             Si hay errores de conversion
	 */
	public Date getValueAsDate() throws PTIMessageException;

	/**
	 * Devuelve el valor del campo en formato hora.
	 * 
	 * @return Un {@link Date}
	 * @throws PTIMessageException
	 *             Si hay errores de conversion
	 */
	public Date getValueAsTime() throws PTIMessageException;

	/**
	 * Devuelve el valor del campo en formato numerico.
	 * 
	 * @return Un {@link BigDecimal}
	 * @throws PTIMessageException
	 *             Si hay errores de conversion
	 */
	public BigDecimal getValueAsBigDecimal() throws PTIMessageException;

	/**
	 * Establece el valor de este campo.
	 * 
	 * @param valor Un {@link String}
	 * @throws PTIMessageException Si ocurre algun error con el tratamiento del dato.
	 */
	public void setValor( final String valor ) throws PTIMessageException;

	/**
	 * Establece el valor de este campo, como Date {@link PTIConstants#DATE_FORMAT}.
	 * 
	 * @param valor El valor.
	 * @throws PTIMessageException Si ocurre algun error convirtiendo el dato.
	 */
	public void setValorAsDate( final Date valor ) throws PTIMessageException;

	/**
	 * Establece el valor de este campo, como Time {@link PTIConstants#TIME_FORMAT}.
	 * 
	 * @param valor El valor.
	 * @throws PTIMessageException Si ocurre algun error convirtiendo el dato.
	 */
	public void setValorAsTime( final Date valor ) throws PTIMessageException;

	/**
	 * Establece el valor de este campo, como {@link BigDecimal}. (ver {@link DecimalFormat}).
	 * 
	 * @param valor El valor.
	 * @throws PTIMessageException Si ocurre algun error convirtiendo el dato.
	 */
	public void setValorAsBigDecimal( final BigDecimal valor ) throws PTIMessageException;

}
