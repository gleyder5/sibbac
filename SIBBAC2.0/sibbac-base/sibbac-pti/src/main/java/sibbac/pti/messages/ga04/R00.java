package sibbac.pti.messages.ga04;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: GA01 (Garantias y Movimientos de
 * Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Marginamount
	public static final String	MARGIN_INST			= "MARGIN_INST";
	public static final String	ASSET_TYPE			= "ASSET_TYPE";
	public static final String	HAIRCUT				= "HAIRCUT";
	public static final String	ASSET_PRICE			= "ASSET_PRICE";
	public static final String	ASSET_VALUE			= "ASSET_VALUE";

	// Tamanyos de los campos.
	public static final int		SIZE_MARGIN_INST	= 1;
	public static final int		SIZE_ASSET_TYPE		= 3;
	public static final int		SIZE_HAIRCUT		= 11;
	public static final int		SIZE_ASSET_PRICE	= 11;
	public static final int		SIZE_ASSET_VALUE	= 18;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS				= {
			new Field( 1, MARGIN_INST, PTIMessageFieldType.ASCII, SIZE_MARGIN_INST ),
			new Field( 2, ASSET_TYPE, PTIMessageFieldType.ASCII, SIZE_ASSET_TYPE ),
			new Field( 3, HAIRCUT, PTIMessageFieldType.NUMBER, SIZE_HAIRCUT, 4 ),
			new Field( 4, ASSET_PRICE, PTIMessageFieldType.NUMBER, SIZE_ASSET_PRICE, 4 ),
			new Field( 5, ASSET_VALUE, PTIMessageFieldType.NUMBER, SIZE_ASSET_VALUE, 6 )
													};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getMarginInst() {
		return this.getAsString( MARGIN_INST );
	}

	public String getAssetType() {
		return this.getAsString( ASSET_TYPE );
	}

	public BigDecimal getHaircut() {
		return this.getAsNumber( HAIRCUT );
	}

	public BigDecimal getAssetPrice() {
		return this.getAsNumber( ASSET_PRICE );
	}

	public BigDecimal getAssetValue() {
		return this.getAsNumber( ASSET_VALUE );
	}

	public void setMarginInst( final String valor ) throws PTIMessageException {
		this.set( MARGIN_INST, valor );
	}

	public void setAssetType( final String valor ) throws PTIMessageException {
		this.set( ASSET_TYPE, valor );
	}

	public void setHaircut( final String valor ) throws PTIMessageException {
		this.set( HAIRCUT, valor );
	}

	public void setAssetPrice( final String valor ) throws PTIMessageException {
		this.set( ASSET_PRICE, valor );
	}

	public void setAssetValue( final String valor ) throws PTIMessageException {
		this.set( ASSET_VALUE, valor );
	}


}
