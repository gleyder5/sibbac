/**
 * 
 */
package sibbac.pti.messages.fs;

import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * 
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class FSCommonDataBlock extends ARecord implements PTIMessageRecord {

  // -------------------------------------------- La lista de campos.

  /**
   * Los campos del bloque de datos comunes.
   */
  public static final String FECHA_SESION = "FECHA_SESION";
  public static final String HORA_FIN_SESION_ONLINE = "HORA_FIN_SESION_ONLINE";
  public static final String RESERVADO = "RESERVADO";

  public static final int SIZE_FECHA_SESION = 8;
  public static final int SIZE_HORA_FIN_SESION_ONLINE = 9;
  public static final int SIZE_RESERVADO = 34;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, FECHA_SESION, PTIMessageFieldType.ASCII, SIZE_FECHA_SESION),
      new Field(2, HORA_FIN_SESION_ONLINE, PTIMessageFieldType.ASCII, SIZE_HORA_FIN_SESION_ONLINE),
      new Field(3, RESERVADO, PTIMessageFieldType.ASCII, SIZE_RESERVADO) };

  /**
   * Constructor por defecto.
   */
  public FSCommonDataBlock() throws PTIMessageException {
    super(PTIMessageRecordType.COMMON_DATA_BLOCK, FIELDS);
  }

  public Date getFechaDeSesion() {
    return this.getAsDate(FECHA_SESION);
  }

  public String getFechaDeSesionAsString() {
    return this.getAsString(FECHA_SESION);
  }

  public Date getHoraFinDeSesionOnline() {
    return this.getAsTime(HORA_FIN_SESION_ONLINE);
  }

  public String getHoraFinDeSesionOnlineAsString() {
    return this.getAsString(HORA_FIN_SESION_ONLINE);
  }

  public String getReservado() {
    return this.getAsString(RESERVADO);
  }

  public void setFechaDeSesion(final String valor) throws PTIMessageException {
    this.set(FECHA_SESION, valor);
  }

  public void setHoraFinDeSesionOnline(final String valor) throws PTIMessageException {
    this.set(HORA_FIN_SESION_ONLINE, valor);
  }

  public void setReservado(final String valor) throws PTIMessageException {
    this.set(RESERVADO, valor);
  }

}
