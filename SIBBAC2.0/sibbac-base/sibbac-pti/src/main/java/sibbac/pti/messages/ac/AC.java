package sibbac.pti.messages.ac;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;

public class AC extends AMessage {

  public AC() throws PTIMessageException {
    this(false);
  }

  public AC(final boolean fromPTI) throws PTIMessageException {
    super(PTIMessageType.AC, fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException {
    switch (tipo) {
      case R00:
        return new R00();
      default:
        return null;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.messages.AMessage#setup()
   */
  @Override
  public void messageDefinition() throws PTIMessageException {
    // El bloque de control.
    this.controlBlock = new ACControlBlock();
    // El bloque de datos comunes.
    this.commonDataBlock = null;
  }

  @Override
  public void persist() throws PTIMessageException {
    
  }

}
