package sibbac.pti;




/**
 * Enumeracion con la lista de tipos de mensajes PTI reconocidos por SIBBAC.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public enum PTIMessageVersion {
  VERSION_1,
  VERSION_T2S,
  VERSION_MIFID2;
}
