package sibbac.pti.messages.rf;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R02 para el mensaje de tipo: RF. (Gestion de Referencias y Filtros
 * de Asignacion Externa /Gestion Modulo Parametrizacion).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Modulo parametrizacion
	public static final String	TIPO_CONFIGURACION					= "TIPO_CONFIGURACION";
	public static final String	IDENTIFICACION_DE_LA_PLATAFORMA		= "IDENTIFICACION_DE_LA_PLATAFORMA";
	public static final String	SEGMENTO							= "SEGMENTO";
	public static final String	INDICADOR_CAPACIDAD					= "INDICADOR_CAPACIDAD";
	public static final String	REFERENCIA_CLIENTE					= "REFERENCIA_CLIENTE";
	public static final String	REFERENCIA_EXTERNA_DE_LA_ORDEN		= "REFERENCIA_EXTERNA_DE_LA_ORDEN";
	public static final String	MIEMBRO_PLATAFORMA					= "MIEMBRO_PLATAFORMA";
	public static final String	CODIGO_DE_USUARIO					= "CODIGO_DE_USUARIO";
	public static final String	MNEMOTECNICO						= "MNEMOTECNICO";
	public static final String	CUENTA_DE_COMPENSACION				= "CUENTA_DE_COMPENSACION";

	public static final int		SIZE_TIPO_CONFIGURACION				= 3;
	public static final int		SIZE_IDENIFICACION_DE_LA_PLATAFORMA	= 4;
	public static final int		SIZE_SEGMENTO						= 2;
	public static final int		SIZE_INDICADOR_CAPACIDAD			= 1;
	public static final int		SIZE_REFERENCIA_CLIENTE				= 16;
	public static final int		SIZE_REFERENCIA_EXTERNA_DE_LA_ORDEN	= 15;
	public static final int		SIZE_MIEMBRO_PLATAFORMA				= 4;
	public static final int		SIZE_CODIGO_DE_USUARIO				= 3;
	public static final int		SIZE_MNEMOTECNICO					= 10;
	public static final int		SIZE_CUENTA_DE_COMPENSACION			= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, IDENTIFICACION_DE_LA_PLATAFORMA, PTIMessageFieldType.ASCII, SIZE_IDENIFICACION_DE_LA_PLATAFORMA ),
			new Field( 2, SEGMENTO, PTIMessageFieldType.ASCII, SIZE_SEGMENTO ),
			new Field( 3, INDICADOR_CAPACIDAD, PTIMessageFieldType.ASCII, SIZE_INDICADOR_CAPACIDAD ),
			new Field( 4, REFERENCIA_CLIENTE, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_CLIENTE ),
			new Field( 5, REFERENCIA_EXTERNA_DE_LA_ORDEN, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_EXTERNA_DE_LA_ORDEN ),
			new Field( 5, MIEMBRO_PLATAFORMA, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_PLATAFORMA ),
			new Field( 6, CODIGO_DE_USUARIO, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_USUARIO ),
			new Field( 7, MNEMOTECNICO, PTIMessageFieldType.ASCII, SIZE_MNEMOTECNICO ),
			new Field( 8, CUENTA_DE_COMPENSACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_DE_COMPENSACION )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R03() throws PTIMessageException {
		super( PTIMessageRecordType.R03, FIELDS );
	}

	public String getTipoConfiguracion() {
		return this.getAsString( TIPO_CONFIGURACION );
	}

	public String getIdentificacionDeLaPlataforma() {
		return this.getAsString( IDENTIFICACION_DE_LA_PLATAFORMA );
	}

	public String getSegmento() {
		return this.getAsString( SEGMENTO );
	}

	public String getIndicadorCapacidad() {
		return this.getAsString( INDICADOR_CAPACIDAD );
	}

	public String getReferenciaCliente() {
		return this.getAsString( REFERENCIA_CLIENTE );
	}

	public String getReferenciaExternaDeLaOrden() {
		return this.getAsString( REFERENCIA_EXTERNA_DE_LA_ORDEN );
	}

	public String getMiembroPlataforma() {
		return this.getAsString( MIEMBRO_PLATAFORMA );
	}
	
	public String getCodigoDeUsuario() {
		return this.getAsString( CODIGO_DE_USUARIO );
	}

	public String getNemotecnico() {
		return this.getAsString( MNEMOTECNICO );
	}

	public String getCuentaDeCompensacion() {
		return this.getAsString( CUENTA_DE_COMPENSACION );
	}

	public void setTipoConfiguracion( final String valor ) throws PTIMessageException {
		this.set( TIPO_CONFIGURACION, valor );
	}
	
	public void setIdentificacionDeLaPlataforma( final String valor ) throws PTIMessageException {
		this.set( IDENTIFICACION_DE_LA_PLATAFORMA, valor );
	}

	public void setSegmento( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO, valor );
	}

	public void setIndicadorCapacidad( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_CAPACIDAD, valor );
	}

	public void setReferenciaCliente( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_CLIENTE, valor );
	}

	public void setReferenciaExternaDeLaOrden( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_EXTERNA_DE_LA_ORDEN, valor );
	}

	public void setMiembroPlataforma( final String valor ) throws PTIMessageException {
		this.set( MIEMBRO_PLATAFORMA, valor );
	}

	public void setCodigoDeUsuario( final String valor ) throws PTIMessageException {
		this.set( CODIGO_DE_USUARIO, valor );
	}

	public void setNemotecnico( final String valor ) throws PTIMessageException {
		this.set( MNEMOTECNICO, valor );
	}

	public void setCuentaDeCompensacion( final String valor ) throws PTIMessageException {
		this.set( CUENTA_DE_COMPENSACION, valor );
	}


}
