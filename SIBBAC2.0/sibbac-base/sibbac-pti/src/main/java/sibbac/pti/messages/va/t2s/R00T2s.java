package sibbac.pti.messages.va.t2s;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.va.R00;


/**
 * Registro R00 para el mensaje de tipo: VA. (Informacion de valores).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00T2s extends R00 {

	// -------------------------------------------- La lista de campos.
  
  // 10
  public static final String TIPO_DE_PRODUCTO = "TIPO_DE_PRODUCTO";
  // 18
  public static final String CUPON_RF = "CUPON_RF";
  // 119
  public static final String FECHA_INICIO_DEVENGO_RF = "FECHA_INICIO_DEVENGO_RF";
  // 20
  public static final String GRUPO_COMPENSACION_RF = "GRUPO_COMPENSACION_RF";
  
  // 10
  public static final int SIZE_TIPO_DE_PRODUCTO = 5;
  // 18
  public static final int SIZE_CUPON_RF = 5;
  // 119
  public static final int SIZE_FECHA_INICIO_DEVENGO_RF = 8;
  // 20
  public static final int SIZE_GRUPO_COMPENSACION_RF = 12;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, CODIGO_DE_VALOR, PTIMessageFieldType.ASCII, SIZE_CODIGO_DE_VALOR ),
			new Field( 2, DIVISA, PTIMessageFieldType.ASCII, SIZE_DIVISA ),
			new Field( 4, DESCRIPCION_DEL_VALOR, PTIMessageFieldType.ASCII, SIZE_DESCRIPCION_DEL_VALOR ),
			new Field( 5, DESCRIPCION_CORTA_DEL_VALOR, PTIMessageFieldType.ASCII, SIZE_DESCRIPCION_CORTA_DEL_VALOR ),
			new Field( 6, CODIGO_CONTRATACION, PTIMessageFieldType.ASCII, SIZE_CODIGO_CONTRATACION ),
			new Field( 7, GRUPO_DE_VALORES, PTIMessageFieldType.ASCII, SIZE_GRUPO_DE_VALORES ),
			new Field( 8, TIPO_DE_PRODUCTO, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_PRODUCTO ),
			new Field( 9, FECHA_EMISION, PTIMessageFieldType.ASCII, SIZE_FECHA_EMISION ),
			new Field( 10, PRECIO_EJERCICIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO_EJERCICIO, 6 ),
			new Field( 11, UNIDAD_DE_CONTRATACION, PTIMessageFieldType.NUMBER, SIZE_UNIDAD_DE_CONTRATACION, 6 ),
			new Field( 12, ESTILO_EJERCICIO, PTIMessageFieldType.ASCII, SIZE_ESTILO_EJERCICIO ),
			new Field( 13, FACTOR_CONVERSION, PTIMessageFieldType.NUMBER, SIZE_FACTOR_CONVERSION, 6 ),
			new Field( 14, IND_CALL_PUT, PTIMessageFieldType.ASCII, SIZE_IND_CALL_PUT ),
			new Field( 15, ENTIDAD_EMISORA_GESTORA, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_EMISORA_GESTORA ),
			new Field( 16, CUPON_RF, PTIMessageFieldType.NUMBER, SIZE_CUPON_RF, 3 ),
			new Field( 17, FECHA_INICIO_DEVENGO_RF, PTIMessageFieldType.ASCII, SIZE_FECHA_INICIO_DEVENGO_RF),
			new Field( 18, GRUPO_COMPENSACION_RF, PTIMessageFieldType.ASCII, SIZE_GRUPO_COMPENSACION_RF)
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00T2s() throws PTIMessageException {
		super( FIELDS );
	}

	
  public BigDecimal getCuponRf() {
    return this.getAsNumber(CUPON_RF);
  }

  public Date getFechaInicioDevengoRf() {
    return this.getAsDate(FECHA_INICIO_DEVENGO_RF);
  }

  public String getGrupoCompensacionRf() {
    return this.getAsString(GRUPO_COMPENSACION_RF);
  }

  public void setCuponRf(BigDecimal value) throws PTIMessageException {
    this.set(CUPON_RF, value);
  }

  public void setFechaInicioDevengoRf(Date value) throws PTIMessageException {
    this.set(FECHA_INICIO_DEVENGO_RF, value);
  }

  public void setGrupoCompensacionRf(String value) throws PTIMessageException {
    this.set(GRUPO_COMPENSACION_RF, value);
  }

}
