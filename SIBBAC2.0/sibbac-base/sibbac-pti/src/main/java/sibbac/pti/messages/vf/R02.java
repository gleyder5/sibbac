package sibbac.pti.messages.vf;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	SENTIDO									= "SENTIDO";
	public static final String	ENTIDAD_COMUNICADORA_RE_TITULAR			= "ENTIDAD_COMUNICADORA_RE_TITULAR";
	public static final String	REFERENCIA_TITULAR						= "REFERENCIA_TITULAR";
	public static final String	INDICADOR_COTIZACION					= "INDICADOR_COTIZACION";
	public static final String	NUMERO_VALORES_IMPORTE_NOMINAL			= "NUMERO_VALORES_IMPORTE_NOMINAL";
	public static final String	CODIGO_ERROR							= "CODIGO_ERROR";

	public static final int		SIZE_SENTIDO							= 1;
	public static final int		SIZE_ENTIDAD_COMUNICADORA_RE_TITULAR	= 11;
	public static final int		SIZE_REFERENCIA_TITULAR					= 20;
	public static final int		SIZE_INDICADOR_COTIZACION				= 1;
	public static final int		SIZE_NUMERO_VALORES_IMPORTE_NOMINAL		= 18;
	public static final int		SIZE_CODIGO_ERROR						= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 2, ENTIDAD_COMUNICADORA_RE_TITULAR, PTIMessageFieldType.ASCII, SIZE_ENTIDAD_COMUNICADORA_RE_TITULAR ),
			new Field( 3, REFERENCIA_TITULAR, PTIMessageFieldType.ASCII, SIZE_REFERENCIA_TITULAR ),
			new Field( 4, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 5, NUMERO_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUMERO_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 6, CODIGO_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_ERROR ),
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02() throws PTIMessageException {
		super( PTIMessageRecordType.R02, FIELDS );
	}

	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public String getEntidadComunicadoraReTitular() {
		return this.getAsString( ENTIDAD_COMUNICADORA_RE_TITULAR );
	}

	public String getReferenciaTitular() {
		return this.getAsString( REFERENCIA_TITULAR );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public BigDecimal getNumeroValoresImporteNominal() {
		return this.getAsNumber( NUMERO_VALORES_IMPORTE_NOMINAL );
	}

	public String getCodigoError() {
		return this.getAsString( CODIGO_ERROR );
	}

	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}

	public void setEntidadComunicadoraReTitular( final String valor ) throws PTIMessageException {
		this.set( ENTIDAD_COMUNICADORA_RE_TITULAR, valor );
	}

	public void setReferenciaTitular( final String valor ) throws PTIMessageException {
		this.set( REFERENCIA_TITULAR, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setNumeroValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUMERO_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setCodigoError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_ERROR, valor );
	}


}
