/**
 * 
 */
package sibbac.pti.messages.ga02;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * 
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class GA02CommonDataBlock extends ARecord implements PTIMessageRecord {

  // -------------------------------------------- La lista de campos.

  /**
   * Los campos del bloque de datos comunes.
   */
  public static final String MARGINREQMTLNQID = "MARGINREQMTLNQID";
  public static final String CLEARING_FIRM = "CLEARING_FIRM";
  public static final String ORDER_ORIGINATION_FIRM = "ORDER_ORIGINATION_FIRM";
  public static final String POSITION_ACCOUNT = "POSITION_ACCOUNT";
  public static final String CODIGO_ERROR = "POSITION_ACCOUNT";
  public static final String TEXTO_ERROR = "POSITION_ACCOUNT";

  public static final int SIZE_MARGINREQMTLNQID = 10;
  public static final int SIZE_CLEARING_FIRM = 4;
  public static final int SIZE_ORDER_ORIGINATION_FIRM = 4;
  public static final int SIZE_POSITION_ACCOUNT = 3;
  public static final int SIZE_CODIGO_ERROR = 3;
  public static final int SIZE_TEXTO_ERROR = 40;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, MARGINREQMTLNQID, PTIMessageFieldType.ASCII,
                                                   SIZE_MARGINREQMTLNQID), new Field(2, CLEARING_FIRM,
                                                                                     PTIMessageFieldType.ASCII,
                                                                                     SIZE_CLEARING_FIRM), new Field(
                                                                                                                    3,
                                                                                                                    ORDER_ORIGINATION_FIRM,
                                                                                                                    PTIMessageFieldType.ASCII,
                                                                                                                    SIZE_ORDER_ORIGINATION_FIRM), new Field(
                                                                                                                                                            4,
                                                                                                                                                            POSITION_ACCOUNT,
                                                                                                                                                            PTIMessageFieldType.ASCII,
                                                                                                                                                            SIZE_POSITION_ACCOUNT), new Field(
                                                                                                                                                                                              5,
                                                                                                                                                                                              CODIGO_ERROR,
                                                                                                                                                                                              PTIMessageFieldType.ASCII,
                                                                                                                                                                                              SIZE_CODIGO_ERROR), new Field(
                                                                                                                                                                                                                            6,
                                                                                                                                                                                                                            TEXTO_ERROR,
                                                                                                                                                                                                                            PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                            SIZE_TEXTO_ERROR)
  };

  /**
   * Constructor por defecto.
   */
  public GA02CommonDataBlock() throws PTIMessageException {
    super(PTIMessageRecordType.COMMON_DATA_BLOCK, FIELDS);
  }

  public String getMarginReqmtInqID() {
    return this.getAsString(MARGINREQMTLNQID);
  }

  public String getClearingFirm() {
    return this.getAsString(CLEARING_FIRM);
  }

  public String getOrderOriginationFirm() {
    return this.getAsString(ORDER_ORIGINATION_FIRM);
  }

  public String getPositionAccount() {
    return this.getAsString(POSITION_ACCOUNT);
  }

  public String getCodigoError() {
    return this.getAsString(CODIGO_ERROR);
  }

  public String getTextoError() {
    return this.getAsString(TEXTO_ERROR);
  }

  public void setMarginReqmtInqID(final String valor) throws PTIMessageException {
    this.set(MARGINREQMTLNQID, valor);
  }

  public void setClearingFirm(final String valor) throws PTIMessageException {
    this.set(CLEARING_FIRM, valor);
  }

  public void setOrderOriginationFirm(final String valor) throws PTIMessageException {
    this.set(ORDER_ORIGINATION_FIRM, valor);
  }

  public void getPositionAccount(final String valor) throws PTIMessageException {
    this.set(POSITION_ACCOUNT, valor);
  }

  public void setCodigoError(final String valor) throws PTIMessageException {
    this.set(CODIGO_ERROR, valor);
  }

  public void setTextoError(final String valor) throws PTIMessageException {
    this.set(TEXTO_ERROR, valor);
  }

}
