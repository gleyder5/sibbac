package sibbac.pti.messages.ti;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R03 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Modo Titular/Operacion
	public static final String	ID_ECC										= "ID_ECC";
	public static final String	SEGMENTO_DE_LA_ECC							= "SEGMENTO_DE_LA_ECC";
	public static final String	NUMERO_OPERACION							= "NUMERO_OPERACION";
	public static final String	INDICADOR_COTIZACION						= "INDICADOR_COTIZACION";
	public static final String	NUM_VALORES_IMPORTE_NOMINAL					= "NUM_VALORES_IMPORTE_NOMINAL";
	public static final String	IMPORTE_EFECTIVO							= "IMPORTE_EFECTIVO";
	public static final String	NUM_VALORES_IMPORTE_NOMINAL_FALLIDO			= "NUM_VALORES_IMPORTE_NOMINAL_FALLIDO";
	public static final String	CANON_TEORICO								= "CANON_TEORICO";
	public static final String	CANON_APLICADO								= "CANON_APLICADO";

	// Tamanyos de los campos.
	public static final int		SIZE_ID_ECC									= 11;
	public static final int		SIZE_SEGMENTO_DE_LA_ECC						= 2;
	public static final int		SIZE_NUMERO_OPERACION						= 35;
	public static final int		SIZE_INDICADOR_COTIZACION					= 1;
	public static final int		SIZE_NUM_VALORES_IMPORTE_NOMINAL			= 18;
	public static final int		SIZE_IMPORTE_EFECTIVO						= 15;
	public static final int		SIZE_NUM_VALORES_IMPORTE_NOMINAL_FALLIDO	= 18;
	public static final int		SIZE_CANON_TEORICO							= 15;
	public static final int		SIZE_CANON_APLICADO							= 15;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS										= {
			new Field( 1, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 2, SEGMENTO_DE_LA_ECC, PTIMessageFieldType.ASCII, SIZE_SEGMENTO_DE_LA_ECC ),
			new Field( 3, NUMERO_OPERACION, PTIMessageFieldType.ASCII, SIZE_NUMERO_OPERACION ),
			new Field( 4, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 5, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, 6 ),
			new Field( 6, IMPORTE_EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_IMPORTE_EFECTIVO, 2 ),
			new Field( 7, NUM_VALORES_IMPORTE_NOMINAL_FALLIDO, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL_FALLIDO, 6 ),
			new Field( 8, CANON_TEORICO, PTIMessageFieldType.NUMBER, SIZE_CANON_TEORICO, 2 ),
			new Field( 9, CANON_APLICADO, PTIMessageFieldType.NUMBER, SIZE_CANON_APLICADO, 2 )
																			};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R03() throws PTIMessageException {
		super( PTIMessageRecordType.R03, FIELDS );
	}
	

  protected R03(final Field[] recordFields) throws PTIMessageException {
    super(PTIMessageRecordType.R03, recordFields);
  }


	public String getIdECC() {
		return this.getAsString( ID_ECC );
	}

	public String getSegmentoECC() {
		return this.getAsString( SEGMENTO_DE_LA_ECC );
	}

	public String getNumeroOperacion() {
		return this.getAsString( NUMERO_OPERACION );
	}

	public String getIndicadorCotizacion() {
		return this.getAsString( INDICADOR_COTIZACION );
	}

	public BigDecimal getNumValoresImporteNominal() {
		return this.getAsNumber( NUM_VALORES_IMPORTE_NOMINAL );
	}

	public BigDecimal getImporteEfectivo() {
		return this.getAsNumber( IMPORTE_EFECTIVO );
	}

	public BigDecimal getNumValoresImporteNominalFallido() {
		return this.getAsNumber( NUM_VALORES_IMPORTE_NOMINAL_FALLIDO );
	}

	public BigDecimal getCanonTeorico() {
		return this.getAsNumber( CANON_TEORICO );
	}

	public BigDecimal getCanonAplicado() {
		return this.getAsNumber( CANON_APLICADO );
	}

	public void setIdECC( final String valor ) throws PTIMessageException {
		this.set( ID_ECC, valor );
	}

	public void setSegmentoECC( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO_DE_LA_ECC, valor );
	}

	public void setNumeroOperacion( final String valor ) throws PTIMessageException {
		this.set( NUMERO_OPERACION, valor );
	}

	public void setIndicadorCotizacion( final String valor ) throws PTIMessageException {
		this.set( INDICADOR_COTIZACION, valor );
	}

	public void setNumValoresImporteNominal( final String valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setNumValoresImporteNominal( final BigDecimal valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL, valor );
	}

	public void setImporteEfectivo( final String valor ) throws PTIMessageException {
		this.set( IMPORTE_EFECTIVO, valor );
	}

	public void setImporteEfectivo( final BigDecimal valor ) throws PTIMessageException {
		this.set( IMPORTE_EFECTIVO, valor );
	}

	public void setNumValoresImporteNominalFallido( final String valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL_FALLIDO, valor );
	}

	public void setNumValoresImporteNominalFallido( final BigDecimal valor ) throws PTIMessageException {
		this.set( NUM_VALORES_IMPORTE_NOMINAL_FALLIDO, valor );
	}

	public void setCanonTeorico( final String valor ) throws PTIMessageException {
		this.set( CANON_TEORICO, valor );
	}

	public void setCanonTeorico( final BigDecimal valor ) throws PTIMessageException {
		this.set( CANON_TEORICO, valor );
	}

	public void setCanonAplicado( final String valor ) throws PTIMessageException {
		this.set( CANON_APLICADO, valor );
	}

	public void setCanonAplicado( final BigDecimal valor ) throws PTIMessageException {
		this.set( CANON_APLICADO, valor );
	}


}
