package sibbac.pti.messages.rf;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R00 para el mensaje de tipo: RF. (Gestion de Referencias y Filtros
 * de Asignacion Externa /Gestion Modulo Parametrizacion).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

	// -------------------------------------------- La lista de campos.

	//
	public static final String	ACCION									= "ACCION";
	public static final String	ID_REFERENCIA_FILTRO					= "ID_REFERENCIA_FILTRO";
	public static final String	ID_REFERENCIA_FILTRO_ECC				= "ID_REFERENCIA_FILTRO_ECC";
	public static final String	ID_REFERENCIA_FILTRO_A_MODIFICAR		= "ID_REFERENCIA_FILTRO_A_MODIFICAR";

	// Tamanyos de los campos.
	public static final int		SIZE_ACCION								= 1;
	public static final int		SIZE_ID_REFERENCIA_FILTRO				= 10;
	public static final int		SIZE_ID_REFERENCIA_FILTRO_ECC	= 30;
	public static final int		SIZE_ID_REFERENCIA_FILTRO_A_MODIFICAR	= 30;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS									= {
			new Field( 1, ACCION, PTIMessageFieldType.ASCII, SIZE_ACCION ),
			new Field( 2, ID_REFERENCIA_FILTRO, PTIMessageFieldType.ASCII, SIZE_ID_REFERENCIA_FILTRO ),
			new Field( 3, ID_REFERENCIA_FILTRO_ECC, PTIMessageFieldType.ASCII, SIZE_ID_REFERENCIA_FILTRO_ECC ),
			new Field( 4, ID_REFERENCIA_FILTRO_A_MODIFICAR, PTIMessageFieldType.ASCII, SIZE_ID_REFERENCIA_FILTRO_A_MODIFICAR )
																		};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R00() throws PTIMessageException {
		super( PTIMessageRecordType.R00, FIELDS );
	}

	public String getAccion() {
		return this.getAsString( ACCION );
	}

	public String getIdReferenciaFiltro() {
		return this.getAsString( ID_REFERENCIA_FILTRO );
	}

	public String getIdReferenciaFiltroEcc() {
		return this.getAsString( ID_REFERENCIA_FILTRO_ECC );
	}

	public String getIdReferenciaFiltroAModificar() {
		return this.getAsString( ID_REFERENCIA_FILTRO_A_MODIFICAR );
	}

	public void setAccion( final String valor ) throws PTIMessageException {
		this.set( ACCION, valor );
	}

	public void setIdReferenciaFiltro( final String valor ) throws PTIMessageException {
		this.set( ID_REFERENCIA_FILTRO, valor );
	}
	
	public void setIdReferenciaFiltroEcc( final String valor ) throws PTIMessageException {
		this.set( ID_REFERENCIA_FILTRO_ECC, valor );
	}

	public void setIdReferenciaFiltroAModificar( final String valor ) throws PTIMessageException {
		this.set( ID_REFERENCIA_FILTRO_A_MODIFICAR, valor );
	}

}
