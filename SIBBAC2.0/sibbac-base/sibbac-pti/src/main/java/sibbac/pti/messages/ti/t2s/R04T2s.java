package sibbac.pti.messages.ti.t2s;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.ti.R04;


/**
 * Registro R04 para el mensaje de tipo: TI. (Titularidad).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R04T2s extends R04 {

	// -------------------------------------------- La lista de campos.
	// Modo Titular/Operacion
  public static final String NUM_VALORES_IMPORTE_NOMINAL = "NUM_VALORES_IMPORTE_NOMINAL";
  public static final String NUM_VALORES_FALLIDOS = "NUM_VALORES_FALLIDOS";

  // Tamanyos de los campos.
  public static final int SIZE_NUM_VALORES_IMPORTE_NOMINAL = 25;
  public static final int SIZE_NUM_VALORES_FALLIDOS = 25;

  
  public static final int SCALE_NUM_VALORES_IMPORTE_NOMINAL = 9;
  public static final int SCALE_NUM_VALORES_FALLIDOS = 9;
	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 2, SEGMENTO_DE_LA_ECC, PTIMessageFieldType.ASCII, SIZE_SEGMENTO_DE_LA_ECC ),
			new Field( 3, MIEMBRO_ECC, PTIMessageFieldType.ASCII, SIZE_MIEMBRO_ECC ),
			new Field( 4, CUENTA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_CUENTA_LIQUIDACION ),
			new Field( 5, FECHA_TEORICA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_TEORICA_LIQUIDACION ),
			new Field( 6, CUENTA_ECC, PTIMessageFieldType.ASCII, SIZE_CUENTA_ECC ),
			new Field( 5, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 6, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6 ),
			new Field( 7, ISIN, PTIMessageFieldType.ASCII, SIZE_ISIN ),
			new Field( 8, INDICADOR_COTIZACION, PTIMessageFieldType.ASCII, SIZE_INDICADOR_COTIZACION ),
			new Field( 9, NUM_VALORES_IMPORTE_NOMINAL, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_IMPORTE_NOMINAL, SCALE_NUM_VALORES_IMPORTE_NOMINAL ),
			new Field( 10, IMPORTE_EFECTIVO, PTIMessageFieldType.NUMBER, SIZE_IMPORTE_EFECTIVO, 2 ),
			new Field( 11, NUM_VALORES_FALLIDOS, PTIMessageFieldType.NUMBER, SIZE_NUM_VALORES_FALLIDOS, SCALE_NUM_VALORES_FALLIDOS ),
			new Field( 12, TIPO_OPERACION_MERCADO, PTIMessageFieldType.ASCII, SIZE_TIPO_OPERACION_MERCADO ),
			new Field( 13, CANON_TEORICO, PTIMessageFieldType.NUMBER, SIZE_CANON_TEORICO, 2 ),
			new Field( 14, CANON_APLICADO, PTIMessageFieldType.NUMBER, SIZE_CANON_APLICADO, 2 )
	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R04T2s() throws PTIMessageException {
		super( FIELDS );
	}


}
