package sibbac.pti.messages.pv.t2s;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.messages.Field;
import sibbac.pti.messages.pv.R01;


/**
 * Registro R01 para el mensaje de tipo: PV. (Informacion de precios).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01T2s extends R01 {

	// -------------------------------------------- La lista de campos.
	
  // 9
  public static final String CUPON_CORRIDO_RF = "CUPON_CORRIDO_RF";
  // 9
  public static final int SIZE_CUPON_CORRIDO_RF = 8;
  public static final int SCALE_CUPON_CORRIDO_RF = 6;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS								= {
			new Field( 1, TIPO_INFORMACION_DE_PRECIOS, PTIMessageFieldType.ASCII, SIZE_TIPO_INFORMACION_DE_PRECIOS ),
			new Field( 2, PRECIO, PTIMessageFieldType.NUMBER, SIZE_PRECIO, 6 ),
			new Field( 3, TIPO_DE_PRECIO_DE_CIERRE, PTIMessageFieldType.ASCII, SIZE_TIPO_DE_PRECIO_DE_CIERRE ),
			new Field( 4, CUPON_CORRIDO_RF, PTIMessageFieldType.NUMBER, SIZE_CUPON_CORRIDO_RF, SCALE_CUPON_CORRIDO_RF )
																	};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R01T2s() throws PTIMessageException {
		super( FIELDS);
	}
	
	public BigDecimal getCuponCorridoRf(){
	  return this.getAsNumber(CUPON_CORRIDO_RF);
	}
	
	public void setCuponCorridoRf(BigDecimal value) throws PTIMessageException{
	  this.set(CUPON_CORRIDO_RF, value);
	}


}
