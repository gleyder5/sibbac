package sibbac.pti.messages.ce;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.AControlBlock;
import sibbac.pti.messages.Field;

/**
 * Bloque de control.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class CEControlBlock extends AControlBlock {

  // -------------------------------------------- La lista de campos.
  public static final String R00 = PTIMessageRecordType.R00.name();
  public static final String CODIGO_DE_ERROR = "CODIGO_DE_ERROR";
  public static final String TEXTO_DE_ERROR = "TEXTO_DE_ERROR";

  public static final int SIZE_CODIGO_DE_ERROR = 3;
  public static final int SIZE_TEXTO_DE_ERROR = 40;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, R00, PTIMessageFieldType.NUMBER, SIZE_CONTROL, 0), new Field(
                                                                                                                   2,
                                                                                                                   CODIGO_DE_ERROR,
                                                                                                                   PTIMessageFieldType.ASCII,
                                                                                                                   SIZE_CODIGO_DE_ERROR), new Field(
                                                                                                                                                    3,
                                                                                                                                                    TEXTO_DE_ERROR,
                                                                                                                                                    PTIMessageFieldType.ASCII,
                                                                                                                                                    SIZE_TEXTO_DE_ERROR)
  };

  /**
   * Constructor por defecto.
   * 
   */
  public CEControlBlock() throws PTIMessageException {
    super(PTIMessageRecordType.CONTROL_BLOCK, FIELDS);
  }

  public BigDecimal getR00() {
    return this.getAsNumber(R00);
  }

  public String getCodigoDeError() {
    return this.getAsString(CODIGO_DE_ERROR);
  }

  public String getTextoDeError() {
    return this.getAsString(TEXTO_DE_ERROR);
  }

  public void setR00(final String valor) throws PTIMessageException {
    this.set(R00, valor);
  }

  public void setCodigoDeError(final String valor) throws PTIMessageException {
    this.set(CODIGO_DE_ERROR, valor);
  }

  public void setTextoDeError(final String valor) throws PTIMessageException {
    this.set(TEXTO_DE_ERROR, valor);
  }

  public boolean hayErrores() {
    return !this.getCodigoDeError().equalsIgnoreCase(NO_ERROR);
  }

}
