package sibbac.pti.messages.pv;


import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;


/**
 * Mensaje de tipo PV.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class PV extends AMessage {

	/**
	 * Constructor por defecto.
	 */
	public PV() throws PTIMessageException {
		this( false );
	}

	/**
	 * Constructor a partir de PTI.
	 * 
	 * @param fromPTI
	 *            TRUE o FALSE, dependiendo de si el registro se genera a partir
	 *            de datos de PTI.
	 */
	public PV( final boolean fromPTI ) throws PTIMessageException {
		super( PTIMessageType.PV, fromPTI );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
	 */
	@Override
	public PTIMessageRecord getRecordInstance( final PTIMessageRecordType tipo ) throws PTIMessageException {
		switch ( tipo ) {
			case R00:
				return new R00();
			case R01:
				return new R01();
			default:
				return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.messages.AMessage#setup()
	 */
	@Override
	public void messageDefinition() throws PTIMessageException {

		// El bloque de control.
		this.controlBlock = new PVControlBlock();

		// El bloque de control.
		this.commonDataBlock = null;
	}

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub
    
  }


}
