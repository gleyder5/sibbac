package sibbac.pti.messages.vf;


import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R01 para el mensaje de tipo: MO. (Datos Cuenta Destino).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R03 extends ARecord {

	// -------------------------------------------- La lista de campos.
	// Detalle
	public static final String	FECHA_TEORICA_LIQUIDACION		= "FECHA_TEORICA_LIQUIDACION";
	public static final String	ID_ECC							= "ID_ECC";
	public static final String	SEGMENTO_ECC					= "SEGMENTO_ECC";
	public static final String	CTA_ECC							= "CTA_ECC";
	public static final String	ISIN							= "ISIN";
	public static final String	SENTIDO							= "SENTIDO";
	public static final String	ID_INSTRUCCION_LIQUIDACION		= "ID_INSTRUCCION_LIQUIDACION";
	public static final String	CODIGO_ERROR					= "CODIGO_ERROR";

	public static final int		SIZE_FECHA_TEORICA_LIQUIDACION	= 8;
	public static final int		SIZE_ID_ECC						= 11;
	public static final int		SIZE_SEGMENTO_ECC				= 2;
	public static final int		SIZE_CTA_ECC					= 35;
	public static final int		SIZE_ISIN						= 12;
	public static final int		SIZE_SENTIDO					= 1;
	public static final int		SIZE_ID_INSTRUCCION_LIQUIDACION	= 16;
	public static final int		SIZE_CODIGO_ERROR				= 3;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS							= {
			new Field( 1, FECHA_TEORICA_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_FECHA_TEORICA_LIQUIDACION ),
			new Field( 2, ID_ECC, PTIMessageFieldType.ASCII, SIZE_ID_ECC ),
			new Field( 3, SEGMENTO_ECC, PTIMessageFieldType.ASCII, SIZE_SEGMENTO_ECC ),
			new Field( 4, CTA_ECC, PTIMessageFieldType.ASCII, SIZE_CTA_ECC ), new Field( 5, ISIN, PTIMessageFieldType.ASCII, SIZE_ISIN ),
			new Field( 6, SENTIDO, PTIMessageFieldType.ASCII, SIZE_SENTIDO ),
			new Field( 7, ID_INSTRUCCION_LIQUIDACION, PTIMessageFieldType.ASCII, SIZE_ID_INSTRUCCION_LIQUIDACION ),
			new Field( 8, CODIGO_ERROR, PTIMessageFieldType.ASCII, SIZE_CODIGO_ERROR )
																};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R03() throws PTIMessageException {
		super( PTIMessageRecordType.R03, FIELDS );
	}

	public Date getFechaTeoricaLiquidacion() {
		return this.getAsDate( FECHA_TEORICA_LIQUIDACION );
	}

	public String getIdEcc() {
		return this.getAsString( ID_ECC );
	}

	public String getSegmentoEcc() {
		return this.getAsString( SEGMENTO_ECC );
	}

	public String getCtaEcc() {
		return this.getAsString( CTA_ECC );
	}

	public String getIsin() {
		return this.getAsString( ISIN );
	}

	public String getSentido() {
		return this.getAsString( SENTIDO );
	}

	public String getIdInstruccionLiquidacion() {
		return this.getAsString( ID_INSTRUCCION_LIQUIDACION );
	}

	public String getCodigoError() {
		return this.getAsString( CODIGO_ERROR );
	}

	public void setFechaTeoricaLiquidacion( final String valor ) throws PTIMessageException {
		this.set( FECHA_TEORICA_LIQUIDACION, valor );
	}

	public void setIdEcc( final String valor ) throws PTIMessageException {
		this.set( ID_ECC, valor );
	}

	public void setSegmentoEcc( final String valor ) throws PTIMessageException {
		this.set( SEGMENTO_ECC, valor );
	}

	public void setCtaEcc( final String valor ) throws PTIMessageException {
		this.set( CTA_ECC, valor );
	}

	public void setIsin( final String valor ) throws PTIMessageException {
		this.set( ISIN, valor );
	}

	public void setSentido( final String valor ) throws PTIMessageException {
		this.set( SENTIDO, valor );
	}

	public void setIdInstruccionLiquidacion( final String valor ) throws PTIMessageException {
		this.set( ID_INSTRUCCION_LIQUIDACION, valor );
	}

	public void setCodigoError( final String valor ) throws PTIMessageException {
		this.set( CODIGO_ERROR, valor );
	}

}
