package sibbac.pti;


/**
 * Enumeracion con los tipos de registros PTI reconocidos por SIBBAC.
 * <p/>
 * Esta enumeracion tan solo indica la tipologia generica; la tipologia especifica de cada registro en concreto depende de cada tipo de
 * mensaje
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public enum PTIMessageRecordType {

	/**
	 * Cabecera comun a todos los mensajes.
	 */
	HEADER,

	/**
	 * Bloque de datos comunes.
	 */
	COMMON_DATA_BLOCK,

	/**
	 * Bloque de control.
	 */
	CONTROL_BLOCK,

	/**
	 * Registro de tipo R00
	 */
	R00,

	/**
	 * Registro de tipo R01
	 */
	R01,

	/**
	 * Registro de tipo R02
	 */
	R02,

	/**
	 * Registro de tipo R03
	 */
	R03,

	/**
	 * Registro de tipo R04
	 */
	R04,

	/**
	 * Registro de tipo R05
	 */
	R05,

	/**
	 * Registro de tipo R06
	 */
	R06,

	/**
	 * Registro de tipo R07
	 */
	R07,

	/**
	 * Registro de tipo UNKNOWN
	 */
	UNKNOWN;

	/**
	 * Metodo de conveniencia para determinar el tipo de registro a partir de un
	 * texto.
	 * 
	 * @param tipo
	 *            Un {@link String}
	 * @return Una instancia de {@link PTIMessageRecordType}.
	 */
	public static PTIMessageRecordType parse( final String tipo ) {
		return ( tipo == null ) ? UNKNOWN : PTIMessageRecordType.valueOf( tipo.toUpperCase().trim() );
	}

}
