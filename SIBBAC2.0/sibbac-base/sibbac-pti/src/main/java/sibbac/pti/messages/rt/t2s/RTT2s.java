package sibbac.pti.messages.rt.t2s;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.rt.R00;
import sibbac.pti.messages.rt.RT;

/**
 * Mensaje de tipo RT.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class RTT2s extends RT {

  /**
   * Constructor por defecto.
   */
  public RTT2s() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public RTT2s(final boolean fromPTI) throws PTIMessageException {
    super(fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) throws PTIMessageException {
    switch (tipo) {
      case R00:
        return new R00();
      case R01:
        return new R01T2s();
      default:
        return null;
    }
  }

}
