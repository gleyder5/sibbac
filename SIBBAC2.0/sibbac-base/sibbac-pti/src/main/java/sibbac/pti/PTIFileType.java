package sibbac.pti;

/**
 * Formato Aplicación(1 .. 8 chars) SFTP MM EP MC DESCRIPCION<br>
 * CCPFILES CCPFILES MC ZIP con los ficheros de datos generados por la ECC<br>
 * CC CC MM EP Comunicación corretajes<br>
 * CC CCFD MM EP Comunicación corretajes a fin de día<br>
 * CM CM MC Cánones mercado<br>
 * CE CE MM Comunicaciones indicador de capacidad y exento TR<br>
 * RC RC MM Remisión complementaria de ejecuciones<br>
 * TE TE MM Rectificación excepcional de ejecuciones<br>
 * TR TR MM Comunicaciones de TR<br>
 * TRCNMV TRCNMV MM Comunicaciones a miembro del TR enviado a CNMV<br>
 * AN AN MM Anotación de operaciones<br>
 * AN ANFDOP MM AN (y TI) - Fin de día - modo operación<br>
 * AN ANFDEX MM AN (y TI) - Fin de día - modo ejecución<br>
 * AN ANMSOP MM AN (y TI) - Media sesión - modo operación<br>
 * IC IC MM EP Informe de corretajes<br>
 * PT PT MM EP Peticiones de información<br>
 * RT RT MM Asignación de referencias de titularidad<br>
 * RT RTPT MM Resultado petición RT<br>
 * TI TI MM Titularidad<br>
 * TI TIFDTI MM TI - Fin de día - modo titular<br>
 * TI TIFDTP MM TI - Fin de día - modo titular y precio<br>
 * TI TIPT MM Resultado petición TI<br>
 * VF VF EP Volúmenes fallidos<br>
 * HCOMCCV HCOMCCV EP Comunicación de cambio CCV<br>
 * HCOMCCV HCCVFD EP Fin de día - Comunicación de cambio CCV<br>
 * HTITU01 HTI1TI EP Comunicación de titularidades<br>
 * HTITU03 HTITU03 EP Comunicación y recepción de titularidades<br>
 * HTITU03 HTI3FDOP EP HTITU03 - Fin de día - modo operación<br>
 * HTITU03 HTI3FDTI EP HTITU03 - Fin de día - modo titular<br>
 * HTITU03 HTI3FDTP EP HTITU03 - Fin de día - modo titular y precio<br>
 * HTITU03 HTI3MSOP EP HTITU03 - Media sesión - modo operación<br>
 * HTITU03 HTI3PT EP Resultado petición HTITU03<br>
 * HVINCOPE HVINCOPE EP Vinculación de operaciones auxiliares<br>
 * HVINCOPE HVINFD EP HVINCOPE - Fin de día - Operaciones auxiliares vinculadas<br>
 * HVINCOPE HVINPT EP Resultado petición HVINCOPE<br>
 * HECCPUI HECCPUI EP Aportación y retirada de saldo<br>
 * HNUME01 HNUME01 EP Comunicación de numeración a entidades<br>
 * HNUME02 HNUME02 EP Comunicación de numeraciones agraciadas en sorteo<br>
 * HTITU01 HTI1DI EP Comunicación de discrepancias<br>
 * HTITU02 HTITU02 EP Comunicación de titularidades a solicitud del emisor<br>
 * HTITU04 HTITU04 EP Comunicación de titularidades p. jur. no residentes<br>
 * */
public enum PTIFileType {
  CCPFILES,
  CC,
  CCFD,
  CM,
  CE,
  RC,
  TE,
  TR,
  TRCNMV,
  AN,
  ANFDOP,
  ANFDEX,
  ANMSOP,
  IC,
  PT,
  RT,
  RTPT,
  TI,
  TIFDTI,
  TIFDTP,
  TIPT,
  VF,
  HCOMCCV,
  HCCVFD,
  HTI1TI,
  HTITU03,
  HTI3FDOP,
  HTI3FDTI,
  HTI3FDTP,
  HTI3MSOP,
  HTI3PT,
  HVINCOPE,
  HVINFD,
  HVINPT,
  HECCPUI,
  HNUME01,
  HNUME02,
  HTI1DI,
  HTITU02,
  HTITU04,
  ISO15022;
  
  // CCPFILES("CCPFILES"),
  // CC("CC"),
  // CCFD("CCFD"),
  // CM("CM"),
  // CE("CE"),
  // RC("RC"),
  // TE("TE"),
  // TR("TR"),
  // TRCNMV("TRCNMV"),
  // AN("AN"),
  // ANFDOP("ANFDOP"),
  // ANFDEX("ANFDEX"),
  // ANMSOP("ANMSOP"),
  // IC("IC"),
  // PT("PT"),
  // RT("RT"),
  // RTPT("RTPT"),
  // TI("TI"),
  // TIFDTI("TIFDTI"),
  // TIFDTP("TIFDTP"),
  // TIPT("TIPT"),
  // VF("VF"),
  // HCOMCCV("HCOMCCV"),
  // HCCVFD("HCCVFD"),
  // HTI1TI("HTI1TI"),
  // HTITU03("HTITU03"),
  // HTI3FDOP("HTI3FDOP"),
  // HTI3FDTI("HTI3FDTI"),
  // HTI3FDTP("HTI3FDTP"),
  // HTI3MSOP("HTI3MSOP"),
  // HTI3PT("HTI3PT"),
  // HVINCOPE("HVINCOPE"),
  // HVINFD("HVINFD"),
  // HVINPT("HVINPT"),
  // HECCPUI("HECCPUI"),
  // HNUME01("HNUME01"),
  // HNUME02("HNUME02"),
  // HTI1DI("HTI1DI"),
  // HTITU02("HTITU02"),
  // HTITU04("HTITU04"),
  // ISO15022("ISO15022");

}
