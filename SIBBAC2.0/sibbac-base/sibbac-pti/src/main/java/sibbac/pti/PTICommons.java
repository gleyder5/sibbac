package sibbac.pti;


/**
 * Interfaz con las especificaciones directamente relacionadas con PTI/BME.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTICommons {

	// --------------------------- General.
	/**
	 * Metodo de conveniencia para convertir todas las entidades a formato
	 * PTI/BME.
	 * 
	 * @return Un {@link String} con la conversion del objeto a formato PTI/BME.
	 * @throws PTIMessageException
	 *             Si ocurren errores de parseo.
	 */
	public String toPTIFormat() throws PTIMessageException;

}
