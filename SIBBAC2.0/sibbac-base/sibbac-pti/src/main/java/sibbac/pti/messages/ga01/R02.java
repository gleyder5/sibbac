package sibbac.pti.messages.ga01;


import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;


/**
 * Registro R02 para el mensaje de tipo: GA01 (Garantias y Movimientos de
 * Efectivo).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R02 extends ARecord {

	// -------------------------------------------- La lista de campos.

	// Pay Collect Group
	public static final String	PAYCOLLECTTYPE					= "PAYCOLLECTTYPE";
	public static final String	PAYAMOUNT						= "PAYAMOUNT";
	public static final String	PAYCOLLECTMARKETSEGMENTID		= "PAYCOLLECTMARKETSEGMENTID";
	public static final String	PAYCOLLECTMARKETID				= "PAYCOLLECTMARKETID";

	// Tamanyos de los campos.
	public static final int		SIZE_PAYCOLLECTTYPE				= 3;
	public static final int		SIZE_PAYAMOUNT					= 15;
	public static final int		SIZE_PAYCOLLECTMARKETSEGMENTID	= 2;
	public static final int		SIZE_PAYCOLLECTMARKETID			= 2;

	/**
	 * La lista de campos de este registro.
	 */
	private static final Field[]	FIELDS							= {
			new Field( 1, PAYCOLLECTTYPE, PTIMessageFieldType.ASCII, SIZE_PAYCOLLECTTYPE ),
			new Field( 2, PAYAMOUNT, PTIMessageFieldType.NUMBER, SIZE_PAYAMOUNT, 2 ),
			new Field( 3, PAYCOLLECTMARKETSEGMENTID, PTIMessageFieldType.ASCII, SIZE_PAYCOLLECTMARKETSEGMENTID ),
			new Field( 4, PAYCOLLECTMARKETID, PTIMessageFieldType.ASCII, SIZE_PAYCOLLECTMARKETID )
																};

	/**
	 * Constructor por defecto.
	 * 
	 * @throws PTIMessageException
	 *             Si ocurre algun tipo de error al parsear la cadena de texto.
	 */
	public R02() throws PTIMessageException {
		super( PTIMessageRecordType.R02, FIELDS );
	}

	public String getPayCollectType() {
		return this.getAsString( PAYCOLLECTTYPE );
	}

	public BigDecimal getPayAmount() {
		return this.getAsNumber( PAYAMOUNT );
	}

	public String getPayCollectMarketSegmentId() {
		return this.getAsString( PAYCOLLECTMARKETSEGMENTID );
	}

	public String getPayCollectMarketId() {
		return this.getAsString( PAYCOLLECTMARKETID );
	}

	public void setPayCollectType( final String valor ) throws PTIMessageException {
		this.set( PAYCOLLECTTYPE, valor );
	}

	public void setPayAmount( final String valor ) throws PTIMessageException {
		this.set( PAYAMOUNT, valor );
	}

	public void setPayCollectMarketSegmentId( final String valor ) throws PTIMessageException {
		this.set( PAYCOLLECTMARKETSEGMENTID, valor );
	}

	public void setPayCollectMarketId( final String valor ) throws PTIMessageException {
		this.set( PAYCOLLECTMARKETID, valor );
	}


}
