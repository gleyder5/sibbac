package sibbac.pti.messages;


import java.math.BigDecimal;
import java.util.Date;

import sibbac.common.SIBBACCommons;
import sibbac.pti.PTIConstants;
import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageField;
import sibbac.pti.PTIMessageFieldType;


/**
 * Un campo de un registro PTI.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class Field implements PTIMessageField {

	/**
	 * El orden del campo, segun la documentacion.
	 */
	private int					orden;

	/**
	 * El nombre del campo.
	 */
	private String				nombre;

	/**
	 * El tipo de campo. Se tipifica utilizando una enumeracion auxiliar: {@link PTIMessageFieldType}.
	 */
	private PTIMessageFieldType	tipo;

	/**
	 * La longitud del campo.
	 */
	private int					longitud;

	/**
	 * El numero de posiciones enteras del dato, si es necesario.
	 */
	private int					enteros;

	/**
	 * El numero de posiciones decimales del dato, si es necesario.
	 */
	private int					decimales;

	/**
	 * El valor del campo.
	 */
	private String				valor;

	/**
	 * El valor del campo (si es un BigDecimal).
	 */
	private BigDecimal			valorAsBigDecimal;

	private String				parteSigno;
	private String				parteEntera;
	private String				parteDecimal;
	private String				formatString;

	/**
	 * Constructor para textos.
	 */
	public Field( final int orden, final String nombre, final PTIMessageFieldType tipoDeCampo, final int longitud ) {
		this( orden, nombre, tipoDeCampo, longitud, longitud, 0 );
	}

	/**
	 * Constructor para numeros "NO SIGNED".
	 */
	public Field( final int orden, final String nombre, final PTIMessageFieldType tipoDeCampo, final int longitud, final int decimales ) {
		this( orden, nombre, tipoDeCampo, longitud, longitud - decimales, decimales );
	}

	/**
	 * Constructor para numeros "SIGNED".
	 */
	public Field( final int orden, final String nombre, final PTIMessageFieldType tipoDeCampo, final int longitud, final int enteros,
			final int decimales ) {
		this.orden = orden;
		this.nombre = nombre;
		this.tipo = tipoDeCampo;
		this.longitud = longitud;
		this.enteros = enteros;
		this.decimales = decimales;

		// Default value
		this.setDefaultValue();

		// BigDecimal format string
		this.setBigDecimalFormatString();
	}

	/**
	 * Establece el valor por defecto del campo.
	 * 
	 * Hack: Change Request Enero'15.
	 * "...los campos no informados se rellenaran a espacios en su totalidad,
	 * independientemente de si estan definidos como alfanumericos o numericos."
	 */
	public void setDefaultValue() {
		this.valor = SIBBACCommons.adjustString( "", this.longitud );
		this.parteDecimal = "";
		this.parteEntera = "";
		this.parteSigno = "";
	}

	/**
	 * El formato para convertir BigDecimal's a PTI.
	 * Tratamos el dato como "absoluto"; el signo va por otra parte.
	 */
	private void setBigDecimalFormatString() {
		StringBuffer sb = new StringBuffer();
		int total = this.enteros;
		// Los enteros.
		for ( int n = 0; n < total; n++ ) {
			sb.append( "0" );
		}
		sb.append( "." );
		// Los decimales.
		for ( int n = 0; n < this.decimales; n++ ) {
			sb.append( "0" );
		}
		this.formatString = sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getOrden()
	 */
	public int getOrden() {
		return orden;
	}

	public String getFormatString() {
		return this.formatString;
	}

	public PTIMessageFieldType getTipo() {
		return this.tipo;
	}

	/**
	 * Setter for orden
	 *
	 * @param orden
	 *            the orden to set
	 */
	public void setOrden( final int orden ) {
		this.orden = orden;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getNombre()
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Setter for nombre
	 *
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre( final String nombre ) {
		this.nombre = nombre;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getTipoDeCampo()
	 */
	public PTIMessageFieldType getTipoDeCampo() {
		return tipo;
	}

	/**
	 * Setter for tipoDeCampo
	 *
	 * @param tipoDeCampo
	 *            the tipoDeCampo to set
	 */
	public void setTipoDeCampo( final PTIMessageFieldType tipoDeCampo ) {
		this.tipo = tipoDeCampo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getLongitud()
	 */
	public int getLongitud() {
		return longitud;
	}

	/**
	 * Setter for longitud
	 *
	 * @param longitud
	 *            the longitud to set
	 */
	public void setLongitud( final int longitud ) {
		this.longitud = longitud;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getEnteros()
	 */
	public int getEnteros() {
		return enteros;
	}

	/**
	 * Setter for enteros
	 *
	 * @param enteros
	 *            the enteros to set
	 */
	public void setEnteros( final int enteros ) {
		this.enteros = enteros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getDecimales()
	 */
	public int getDecimales() {
		return decimales;
	}

	/**
	 * Setter for decimales
	 *
	 * @param decimales
	 *            the decimales to set
	 */
	public void setDecimales( final int decimales ) {
		this.decimales = decimales;
	}

	public boolean tieneDecimales() {
		return this.decimales != 0;
	}

	@Override
	public void setValorAsDate( final Date valor ) throws PTIMessageException {
		// Convertimos a String...
		this.setValor( SIBBACCommons.convertDateToString( valor, PTIConstants.DATE_FORMAT ) );
	}

	@Override
	public void setValorAsTime( final Date valor ) throws PTIMessageException {
		// Convertimos a String...
		this.setValor( SIBBACCommons.convertDateToString( valor, PTIConstants.TIME_FORMAT ) );
	}

	@Override
	public void setValorAsBigDecimal( final BigDecimal valor ) throws PTIMessageException {

		// Almacenamos el valor recibido.
		this.valorAsBigDecimal = valor;

		// Convertimos a String...
		this.setValor( SIBBACCommons.convertBigDecimalToString( valor, this ) );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#setValor(String)
	 */
	public void setValor( final String valor ) throws PTIMessageException {
		// Primero, almacenamos el valor recibido. Es un String, ojo.
		this.valor = valor;

		if ( valor == null || valor.isEmpty() ) {
			// Si no hay dato, ponemos el valor por defecto.
			this.setDefaultValue();
			return;
		}

		// Tenemos algo.
		int total = this.valor.length();

		// Luego, intentamos "parsear" segun el contenido.
		if ( this.tipo.isText() ) {
			// Nada que hacer salvo comprobaciones.
			if ( total != this.longitud ) {
				if ( total < this.longitud ) {
					// Menos... Rellenamos.
					this.valor = SIBBACCommons.adjustString( this.valor, this.longitud );
				} else {
					this.valor = this.valor.substring( 0, this.longitud );
				}
			}
		} else {
			// Es un dato numerico.
			int nSigno = 0, nEnteros = 0, nDecimales = 0; // Para las posiciones "substring" en "valor".

			nDecimales = this.decimales;
			nEnteros = total - nDecimales;
			nSigno = 0;
			if ( this.tipo.isSigned() ) {
				// Primero verificamos el primer caracter.
				char signo = this.valor.charAt( 0 );
				if ( signo == ' ' ) {
					this.valor = "+" + this.valor.substring( 1 );
				} else if ( signo == '+' ) {
					// Se queda
				} else if ( signo == '-' ) {
					// Se queda
				} else {
					throw new PTIMessageException( "Formato de dato no valido: [" + valor + "]: No contiene signo en la primera posición." );
				}
				nSigno++;
				nEnteros--;
			}

			if ( ( nEnteros < 0 ) || ( nEnteros != this.enteros ) ) {
				throw new PTIMessageException( "Error con el dato: [" + valor + "] para el campo: [" + this.nombre + " (tipo=" + tipo
						+ "/longitud=" + longitud + "/enteros=" + enteros + "/decimales=" + decimales + "/valor=" + valor
						+ ")]. No hay parte entera o no coincide: [nEnteros:" + nEnteros + "/enteros:" + this.enteros + "]." );
			}

			// Ahora, extractamos.
			this.parteDecimal = this.valor.substring( this.longitud - this.decimales );
			this.parteEntera = this.valor.substring( nSigno, nSigno + nEnteros );
			if ( this.tipo.isSigned() ) {
				this.parteSigno = this.valor.substring( 0, 1 );
			} else {
				this.parteSigno = "";
			}

			if ( ( this.parteDecimal.contains( "." ) || this.parteDecimal.contains( "," ) )
					|| ( this.parteEntera.contains( "." ) || this.parteEntera.contains( "," ) )
					|| ( this.parteSigno.contains( "." ) || this.parteSigno.contains( "," ) ) ) {
				throw new PTIMessageException( "El dato contiene signos de puntuacion no autorizados [" + this.valor + "]" );
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getValor()
	 */
	public String getRawValue() {
		return valor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getValueAsString()
	 */
	@Override
	public String getValueAsString() {
		if ( this.tipo.compareTo( PTIMessageFieldType.ASCII ) == 0 ) {
			return ( this.valor != null ) ? this.valor.trim() : "";
		} else {
			/*
			 * // Construimos el numero...
			 * StringBuffer sb = new StringBuffer();
			 * if ( this.tipo.isSigned() ) {
			 * sb.append( this.parteSigno );
			 * }
			 * sb.append( ( this.parteEntera != null ) ? this.parteEntera.trim() : "" );
			 * if ( this.tieneDecimales() ) {
			 * sb.append( "." + ( ( this.parteDecimal != null ) ? this.parteDecimal.trim() : "" ) );
			 * }
			 * // return sb.toString();
			 */
			return this.getNumberValue( this.tipo.isSigned(), this.parteSigno, this.parteEntera, this.tieneDecimales(), this.parteDecimal );
		}
	}

	protected String getNumberValue( final boolean isSigned, final String pSigno, final String pEntera, final boolean hasDecimals,
			final String pDecimal ) {
		// Construimos el numero...
		StringBuffer sb = new StringBuffer();
		if ( isSigned ) {
			sb.append( pSigno );
		}
		sb.append( ( pEntera != null && pEntera.trim().length() > 0 ) ? pEntera.trim() : "0" );
		if ( this.tieneDecimales() ) {
			sb.append( "." );
			sb.append( ( pDecimal != null && pDecimal.trim().length() > 0 ) ? pDecimal.trim() : "0" );
		}
		return sb.toString().trim();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getValueAsDate()
	 */
	@Override
	public Date getValueAsDate() throws PTIMessageException {
		return this.getValueAsParsedDate( PTIConstants.DATE_FORMAT );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getValueAsTime()
	 */
	@Override
	public Date getValueAsTime() throws PTIMessageException {
		return this.getValueAsParsedDate( PTIConstants.TIME_FORMAT );
	}

	public Date getValueAsParsedDate( final String format ) throws PTIMessageException {
		if ( this.tipo.compareTo( PTIMessageFieldType.ASCII ) != 0 ) {
			throw new PTIMessageException( "Formato incorrecto: [" + format + "]" );
		}
		// if ( this.valor==null || ( this.valor!=null && this.parteEntera.trim().length()==0 ) ) {
		if ( this.valor == null ) {
			return null;
		} else {
			return SIBBACCommons.convertStringToDate( this.valor.trim(), format );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTIMessageField#getValueAsBigDecimal()
	 */
	@Override
	public BigDecimal getValueAsBigDecimal() throws PTIMessageException {
		if ( this.tipo.compareTo( PTIMessageFieldType.ASCII ) == 0 ) {
			throw new PTIMessageException( "Formato incorrecto" );
		}

		// Hack: Campos vacios.
		/*
		 * if ( this.parteEntera==null || ( this.parteEntera!=null && this.parteEntera.trim().length()==0 ) ) {
		 * this.parteEntera = "0";
		 * }
		 * if ( this.parteDecimal==null || ( this.parteDecimal!=null && this.parteDecimal.trim().length()==0 ) ) {
		 * this.parteDecimal = "0";
		 * }
		 * return new BigDecimal( this.getValueAsString() );
		 */
		String strNumber = this.getNumberValue( this.tipo.isSigned(), this.parteSigno, this.parteEntera, this.tieneDecimales(),
				this.parteDecimal );
		BigDecimal bd = null;
		try {
			bd = new BigDecimal( strNumber );
		} catch ( NumberFormatException e ) {
			throw new PTIMessageException( "NumberFormatException [" + strNumber + "]" );
		} catch ( Exception e ) {
			throw new PTIMessageException( "Exception [" + strNumber + "]" );
		}
		return bd;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.pti.PTICommons#toPTIFormat()
	 */
	@Override
	public String toPTIFormat() throws PTIMessageException {
		// Dependiendo del tipo de campo...

		// if ( this.tipo.isText() ) { return (this.valor==null) ? "" : this.valor; }

		if ( this.tipo.isText() ) { // TEXTO.

			String resultante = null;
			if ( this.valor == null || this.valor.length() != this.longitud ) {
				StringBuffer format = new StringBuffer();
				format.append( "%-" + this.longitud + "s" );
				resultante = String.format( format.toString(), ( this.valor != null ) ? this.valor.trim() : " " );
			} else {
				resultante = this.valor;
			}

			return resultante;
		} else { // NUMBER.
			if ( this.parteEntera != null && !this.parteEntera.trim().isEmpty() ) {
				StringBuffer sb = new StringBuffer();
				sb.append( this.parteSigno + this.parteEntera + this.parteDecimal );
				return sb.toString();
			} else {
				String resultante = null;
				StringBuffer format = new StringBuffer();
				format.append( "%-" + this.longitud + "s" );
				resultante = String.format( format.toString(), " " );
				return resultante;
			}
		}

		/*
		 * StringBuffer format = new StringBuffer(); String resultante = null;
		 * if (this.tipo.compareTo(PTIMessageFieldType.ASCII) == 0) { // TEXTO.
		 * format.append("%-" + this.longitud + "s"); resultante =
		 * String.format(format.toString(), this.valor.trim()); } else if
		 * (this.tipo.compareTo(PTIMessageFieldType.NUMBER) == 0) { // NUMBER.
		 * if (!SIBBACCommons.isValid(this.longitud, this.enteros,
		 * this.decimales)) { throw new PTIMessageException(
		 * "Error con los datos numericos: [" + this.longitud + "/" +
		 * this.enteros + "/" + this.decimales + "]"); }
		 * 
		 * // Si no hay decimales... if (this.decimales == 0) { // No hay
		 * decimales. format.append("%0" + this.longitud + "d"); resultante =
		 * String.format(format.toString(), Long.valueOf(this.valor.trim())); }
		 * else { // Con decimales format.append("%0" + this.longitud +
		 * DECIMAL_SEPARATOR + this.decimales + "f"); resultante =
		 * String.format(format.toString(), Double.valueOf(this.valor.trim()));
		 * } } else { // SIGNED if (!SIBBACCommons.isValid(this.longitud,
		 * this.enteros, this.decimales)) { throw new PTIMessageException(
		 * "Error con los datos numericos: [" + this.longitud + "/" +
		 * this.enteros + "/" + this.decimales + "]"); } Formatter formatter =
		 * new Formatter();
		 * 
		 * // Si no hay decimales... if (this.decimales == 0) { // No hay
		 * decimales. format.append("%+0" + this.longitud + "d"); resultante =
		 * formatter.format(format.toString(),
		 * Long.valueOf(this.valor.trim())).toString(); } else { // Con
		 * decimales format.append("%+0" + this.longitud + DECIMAL_SEPARATOR +
		 * this.decimales + "f"); resultante =
		 * formatter.format(format.toString(),
		 * Double.valueOf(this.valor.trim())).toString(); } formatter.close(); }
		 * return resultante;
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append( "Field:" );
		sb.append( " [orden=" + orden + "]" );
		sb.append( " [nombre=" + nombre + "]" );
		sb.append( " [tipoDeCampo=" + tipo + "]" );
		sb.append( " [longitud=" + longitud + "]" );
		sb.append( " [enteros=" + enteros + "]" );
		sb.append( " [decimales=" + decimales + "]" );
		sb.append( " [valor=" + valor + "]" );
		return sb.toString();
	}

}
