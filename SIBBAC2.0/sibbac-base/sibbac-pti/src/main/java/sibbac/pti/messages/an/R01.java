package sibbac.pti.messages.an;

import java.math.BigDecimal;
import java.util.Date;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R01 para el mensaje de tipo: AN. (Anotacion de operaciones).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R01 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Detalle de los datos del registro de operaciones
  public static final String INDICADOR_ANOTACION = "INDICADOR_ANOTACION";
  public static final String NUMERO_OPERACION = "NUMERO_OPERACION";
  public static final String SENTIDO = "SENTIDO";
  public static final String INDICADOR_POSICION = "INDICADOR_POSICION";
  public static final String CODIGO_OPERACION = "CODIGO_OPERACION";
  public static final String REFERENCIA_ASIGNACION_EXTERNA = "REFERENCIA_ASIGNACION_EXTERNA";
  public static final String MNEMOTECNICO = "MNEMOTECNICO";
  public static final String FECHA_CONTRATACION = "FECHA_CONTRATACION";
  public static final String FECHA_LIQUIDACION_TEORICA = "FECHA_LIQUIDACION_TEORICA";
  public static final String FECHA_DE_REGISTRO_EN_LA_ECC = "FECHA_DE_REGISTRO_EN_LA_ECC";
  public static final String HORA_REGISTRO = "HORA_REGISTRO";
  public static final String INDICADOR_COTIZACION = "INDICADOR_COTIZACION";
  public static final String VALORES_IMPORTE_NOMINAL = "VALORES_IMPORTE_NOMINAL";
  public static final String DIVISA = "DIVISA";
  public static final String PRECIO = "PRECIO";
  public static final String EFECTIVO = "EFECTIVO";
  public static final String VALORES_IMPORTE_NOMINAL_DISPONIBLES = "VALORES_IMPORTE_NOMINAL_DISPONIBLES";
  public static final String EFECTIVO_DISPONIBLE = "EFECTIVO_DISPONIBLE";
  public static final String VALORES_IMPORTE_NOMINAL_RETENIDOS = "VALORES_IMPORTE_NOMINAL_RETENIDOS";
  public static final String EFECTIVO_RETENIDO = "EFECTIVO_RETENIDO";
  public static final String NUMERO_OPERACION_PREVIA = "NUMERO_OPERACION_PREVIA";
  public static final String NUMERO_OPERACION_INICIAL = "NUMERO_OPERACION_INICIAL";
  public static final String REFERENCIA_COMUN = "REFERENCIA_COMUN";
  public static final String CORRETAJE = "CORRETAJE";
  // Tamanyos de los campos.
  public static final int SIZE_INDICADOR_ANOTACION = 1;
  public static final int SIZE_NUMERO_OPERACION = 16;
  public static final int SIZE_SENTIDO = 1;
  public static final int SIZE_INDICADOR_POSICION = 1;
  public static final int SIZE_CODIGO_OPERACION = 1;
  public static final int SIZE_REFERENCIA_ASIGNACION_EXTERNA = 18;
  public static final int SIZE_MNEMOTECNICO = 10;
  public static final int SIZE_FECHA_CONTRATACION = 8;
  public static final int SIZE_FECHA_LIQUIDACION_TEORICA = 8;
  public static final int SIZE_FECHA_DE_REGISTRO_EN_LA_ECC = 8;
  public static final int SIZE_HORA_REGISTRO = 9;
  public static final int SIZE_INDICADOR_COTIZACION = 1;
  public static final int SIZE_VALORES_IMPORTE_NOMINAL = 18;
  public static final int SIZE_DIVISA = 3;
  public static final int SIZE_PRECIO = 14;
  public static final int SIZE_EFECTIVO = 16;
  public static final int SIZE_VALORES_IMPORTE_NOMINAL_DISPONIBLES = 18;
  public static final int SIZE_EFECTIVO_DISPONIBLE = 16;
  public static final int SIZE_VALORES_IMPORTE_NOMINAL_RETENIDOS = 18;
  public static final int SIZE_EFECTIVO_RETENIDO = 16;
  public static final int SIZE_NUMERO_OPERACION_PREVIA = 16;
  public static final int SIZE_NUMERO_OPERACION_INICIAL = 16;
  public static final int SIZE_REFERENCIA_COMUN = 16;
  public static final int SIZE_CORRETAJE = 16;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, INDICADOR_ANOTACION, PTIMessageFieldType.ASCII,
                                                   SIZE_INDICADOR_ANOTACION), new Field(2, NUMERO_OPERACION,
                                                                                        PTIMessageFieldType.ASCII,
                                                                                        SIZE_NUMERO_OPERACION), new Field(
                                                                                                                          3,
                                                                                                                          SENTIDO,
                                                                                                                          PTIMessageFieldType.ASCII,
                                                                                                                          SIZE_SENTIDO), new Field(
                                                                                                                                                   4,
                                                                                                                                                   INDICADOR_POSICION,
                                                                                                                                                   PTIMessageFieldType.ASCII,
                                                                                                                                                   SIZE_INDICADOR_POSICION), new Field(
                                                                                                                                                                                       5,
                                                                                                                                                                                       CODIGO_OPERACION,
                                                                                                                                                                                       PTIMessageFieldType.ASCII,
                                                                                                                                                                                       SIZE_CODIGO_OPERACION), new Field(
                                                                                                                                                                                                                         6,
                                                                                                                                                                                                                         REFERENCIA_ASIGNACION_EXTERNA,
                                                                                                                                                                                                                         PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                         SIZE_REFERENCIA_ASIGNACION_EXTERNA), new Field(
                                                                                                                                                                                                                                                                        7,
                                                                                                                                                                                                                                                                        MNEMOTECNICO,
                                                                                                                                                                                                                                                                        PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                        SIZE_MNEMOTECNICO), new Field(
                                                                                                                                                                                                                                                                                                      8,
                                                                                                                                                                                                                                                                                                      FECHA_CONTRATACION,
                                                                                                                                                                                                                                                                                                      PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                      SIZE_FECHA_CONTRATACION), new Field(
                                                                                                                                                                                                                                                                                                                                          9,
                                                                                                                                                                                                                                                                                                                                          FECHA_LIQUIDACION_TEORICA,
                                                                                                                                                                                                                                                                                                                                          PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                          SIZE_FECHA_LIQUIDACION_TEORICA), new Field(
                                                                                                                                                                                                                                                                                                                                                                                     10,
                                                                                                                                                                                                                                                                                                                                                                                     FECHA_DE_REGISTRO_EN_LA_ECC,
                                                                                                                                                                                                                                                                                                                                                                                     PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                     SIZE_FECHA_DE_REGISTRO_EN_LA_ECC), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                  11,
                                                                                                                                                                                                                                                                                                                                                                                                                                  HORA_REGISTRO,
                                                                                                                                                                                                                                                                                                                                                                                                                                  PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                                                                  SIZE_HORA_REGISTRO), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 12,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 INDICADOR_COTIZACION,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 SIZE_INDICADOR_COTIZACION), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       13,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       VALORES_IMPORTE_NOMINAL,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       SIZE_VALORES_IMPORTE_NOMINAL,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       6), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     14,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     DIVISA,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     SIZE_DIVISA), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             15,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             PRECIO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             PTIMessageFieldType.SIGNED,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             SIZE_PRECIO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             7,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             6), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           16,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           EFECTIVO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           PTIMessageFieldType.SIGNED,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           SIZE_EFECTIVO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           13,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         17,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         VALORES_IMPORTE_NOMINAL_DISPONIBLES,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         SIZE_VALORES_IMPORTE_NOMINAL_DISPONIBLES,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         6), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       18,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       EFECTIVO_DISPONIBLE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       PTIMessageFieldType.SIGNED,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       SIZE_EFECTIVO_DISPONIBLE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       13,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       2), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     19,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     VALORES_IMPORTE_NOMINAL_RETENIDOS,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     PTIMessageFieldType.NUMBER,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     SIZE_VALORES_IMPORTE_NOMINAL_RETENIDOS,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     6), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   20,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   EFECTIVO_RETENIDO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   PTIMessageFieldType.SIGNED,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   SIZE_EFECTIVO_RETENIDO,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   13,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   2), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 21,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 NUMERO_OPERACION_PREVIA,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 SIZE_NUMERO_OPERACION_PREVIA), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          22,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          NUMERO_OPERACION_INICIAL,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          SIZE_NUMERO_OPERACION_INICIAL), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    23,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    REFERENCIA_COMUN,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    SIZE_REFERENCIA_COMUN), new Field(
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      24,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      CORRETAJE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      PTIMessageFieldType.SIGNED,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      SIZE_CORRETAJE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      13,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      2)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R01() throws PTIMessageException {
    super(PTIMessageRecordType.R01, FIELDS);
  }

  public String getIndicadorAnotacion() {
    return this.getAsString(INDICADOR_ANOTACION);
  }

  public String getNumeroOperacion() {
    return this.getAsString(NUMERO_OPERACION);
  }

  public String getSentido() {
    return this.getAsString(SENTIDO);
  }

  public String getIndicadorPosicion() {
    return this.getAsString(INDICADOR_POSICION);
  }

  public String getCodigoOperacion() {
    return this.getAsString(CODIGO_OPERACION);
  }

  public String getReferenciaAsignacionExterna() {
    return this.getAsString(REFERENCIA_ASIGNACION_EXTERNA);
  }

  public String getMnemotecnico() {
    return this.getAsString(MNEMOTECNICO);
  }

  public Date getFechaContratacion() {
    return this.getAsDate(FECHA_CONTRATACION);
  }

  public Date getFechaLiquidacionTeorica() {
    return this.getAsDate(FECHA_LIQUIDACION_TEORICA);
  }

  public Date getFechaDeRegistroEnLaECC() {
    return this.getAsDate(FECHA_DE_REGISTRO_EN_LA_ECC);
  }

  public Date getHoraRegistro() {
    return this.getAsTime(HORA_REGISTRO);
  }

  public String getIndicadorCotizacion() {
    return this.getAsString(INDICADOR_COTIZACION);
  }

  public BigDecimal getValoresImportNominal() {
    return this.getAsNumber(VALORES_IMPORTE_NOMINAL);
  }

  public String getDivisa() {
    return this.getAsString(DIVISA);
  }

  public BigDecimal getPrecio() {
    return this.getAsNumber(PRECIO);
  }

  public BigDecimal getEfectivo() {
    return this.getAsNumber(EFECTIVO);
  }

  public BigDecimal getValoresImportNominalDisponibles() {
    return this.getAsNumber(VALORES_IMPORTE_NOMINAL_DISPONIBLES);
  }

  public BigDecimal getEfectivoDisponible() {
    return this.getAsNumber(EFECTIVO_DISPONIBLE);
  }

  public BigDecimal getValoresImporteNominalRetenidos() {
    return this.getAsNumber(VALORES_IMPORTE_NOMINAL_RETENIDOS);
  }

  public BigDecimal getEfectivoRetenido() {
    return this.getAsNumber(EFECTIVO_RETENIDO);
  }

  public String getNumeroOperacionPrevia() {
    return this.getAsString(NUMERO_OPERACION_PREVIA);
  }

  public String getNumeroOperacionInicial() {
    return this.getAsString(NUMERO_OPERACION_INICIAL);
  }

  public String getReferenciaComun() {
    return this.getAsString(REFERENCIA_COMUN);
  }

  public BigDecimal getCorretaje() {
    return this.getAsNumber(CORRETAJE);
  }

  public void setIndicadorAnotacion(final String valor) throws PTIMessageException {
    this.set(INDICADOR_ANOTACION, valor);
  }

  public void setNumeroOperacion(final String valor) throws PTIMessageException {
    this.set(NUMERO_OPERACION, valor);
  }

  public void setSentido(final String valor) throws PTIMessageException {
    this.set(SENTIDO, valor);
  }

  public void setIndicadorPosicion(final String valor) throws PTIMessageException {
    this.set(INDICADOR_POSICION, valor);
  }

  public void setCodigoOperacion(final String valor) throws PTIMessageException {
    this.set(CODIGO_OPERACION, valor);
  }

  public void setReferenciaAsignacionExterna(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_ASIGNACION_EXTERNA, valor);
  }

  public void setMnemotecnico(final String valor) throws PTIMessageException {
    this.set(MNEMOTECNICO, valor);
  }

  public void setFechaContratacion(final String valor) throws PTIMessageException {
    this.set(FECHA_CONTRATACION, valor);
  }

  public void setFechaLiquidacionTeorica(final String valor) throws PTIMessageException {
    this.set(FECHA_LIQUIDACION_TEORICA, valor);
  }

  public void setFechaDeRegistroEnLaECC(final String valor) throws PTIMessageException {
    this.set(FECHA_DE_REGISTRO_EN_LA_ECC, valor);
  }

  public void setHoraRegistro(final String valor) throws PTIMessageException {
    this.set(HORA_REGISTRO, valor);
  }

  public void setIndicadorCotizacion(final String valor) throws PTIMessageException {
    this.set(INDICADOR_COTIZACION, valor);
  }

  public void setValoresImportNominal(final String valor) throws PTIMessageException {
    this.set(VALORES_IMPORTE_NOMINAL, valor);
  }

  public void setDivisa(final String valor) throws PTIMessageException {
    this.set(DIVISA, valor);
  }

  public void setPrecio(final String valor) throws PTIMessageException {
    this.set(PRECIO, valor);
  }

  public void setEfectivo(final String valor) throws PTIMessageException {
    this.set(EFECTIVO, valor);
  }

  public void setValoresImportNominalDisponibles(final String valor) throws PTIMessageException {
    this.set(VALORES_IMPORTE_NOMINAL_DISPONIBLES, valor);
  }

  public void setEfectivoDisponible(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_DISPONIBLE, valor);
  }

  public void setValoresImporteNominalRetenidos(final String valor) throws PTIMessageException {
    this.set(VALORES_IMPORTE_NOMINAL_RETENIDOS, valor);
  }

  public void setEfectivoRetenido(final String valor) throws PTIMessageException {
    this.set(EFECTIVO_RETENIDO, valor);
  }

  public void setNumeroOperacionPrevia(final String valor) throws PTIMessageException {
    this.set(NUMERO_OPERACION_PREVIA, valor);
  }

  public void setNumeroOperacionInicial(final String valor) throws PTIMessageException {
    this.set(NUMERO_OPERACION_INICIAL, valor);
  }

  public void setReferenciaComun(final String valor) throws PTIMessageException {
    this.set(REFERENCIA_COMUN, valor);
  }

  public void setCorretaje(final String valor) throws PTIMessageException {
    this.set(CORRETAJE, valor);
  }

}
