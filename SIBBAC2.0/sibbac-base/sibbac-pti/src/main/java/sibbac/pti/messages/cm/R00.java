package sibbac.pti.messages.cm;

import java.math.BigDecimal;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageFieldType;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.messages.ARecord;
import sibbac.pti.messages.Field;

/**
 * Registro R00 para el mensaje de tipo: PV. (Informacion de precios).
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class R00 extends ARecord {

  // -------------------------------------------- La lista de campos.

  // Identificacion del valor
  public static final String MIEMBRO_COMPENSADOR = "MIEMBRO_COMPENSADOR";
  public static final String FECHA_LIQUIDACION = "FECHA_LIQUIDACION";
  public static final String CARGO_ABONO = "CARGO_ABONO";
  public static final String MES_ANO_CANON = "MES_ANO_CANON";
  public static final String MIEMBRO_MERCADO = "MIEMBRO_MERCADO";
  public static final String IMPORTE = "IMPORTE";

  // Tamanyos de los campos.
  public static final int SIZE_MIEMBRO_COMPENSADOR = 11;
  public static final int SIZE_FECHA_LIQUIDACION = 8;
  public static final int SIZE_CARGO_ABONO = 1;
  public static final int SIZE_MES_ANO_CANON = 6;
  public static final int SIZE_MIEMBRO_MERCADO = 4;
  public static final int SIZE_IMPORTE = 15;

  /**
   * La lista de campos de este registro.
   */
  private static final Field[] FIELDS = { new Field(1, MIEMBRO_COMPENSADOR, PTIMessageFieldType.ASCII,
                                                   SIZE_MIEMBRO_COMPENSADOR), new Field(2, FECHA_LIQUIDACION,
                                                                                        PTIMessageFieldType.ASCII,
                                                                                        SIZE_FECHA_LIQUIDACION), new Field(
                                                                                                                           3,
                                                                                                                           CARGO_ABONO,
                                                                                                                           PTIMessageFieldType.ASCII,
                                                                                                                           SIZE_CARGO_ABONO), new Field(
                                                                                                                                                        4,
                                                                                                                                                        MES_ANO_CANON,
                                                                                                                                                        PTIMessageFieldType.ASCII,
                                                                                                                                                        SIZE_MES_ANO_CANON), new Field(
                                                                                                                                                                                       5,
                                                                                                                                                                                       MIEMBRO_MERCADO,
                                                                                                                                                                                       PTIMessageFieldType.ASCII,
                                                                                                                                                                                       SIZE_MIEMBRO_MERCADO), new Field(
                                                                                                                                                                                                                        6,
                                                                                                                                                                                                                        IMPORTE,
                                                                                                                                                                                                                        PTIMessageFieldType.ASCII,
                                                                                                                                                                                                                        SIZE_IMPORTE)
  };

  /**
   * Constructor por defecto.
   * 
   * @throws PTIMessageException Si ocurre algun tipo de error al parsear la cadena de texto.
   */
  public R00() throws PTIMessageException {
    super(PTIMessageRecordType.R00, FIELDS);
  }

  public String getMiembroCompensador() {
    return this.getAsString(MIEMBRO_COMPENSADOR);
  }

  public String getFechaLiquidacion() {
    return this.getAsString(FECHA_LIQUIDACION);
  }

  public String getCargoAbono() {
    return this.getAsString(CARGO_ABONO);
  }

  public String getMesAnoCanon() {
    return this.getAsString(MES_ANO_CANON);
  }

  public String getMiembroMercado() {
    return this.getAsString(MIEMBRO_MERCADO);
  }

  public BigDecimal getImporte() {
    return this.getAsNumber(IMPORTE);
  }

  public void setMiembroCompensador(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_COMPENSADOR, valor);
  }

  public void setFechaLiquidacion(final String valor) throws PTIMessageException {
    this.set(FECHA_LIQUIDACION, valor);
  }

  public void setCargoAbono(final String valor) throws PTIMessageException {
    this.set(CARGO_ABONO, valor);
  }

  public void setMesAnoCanon(final String valor) throws PTIMessageException {
    this.set(MES_ANO_CANON, valor);
  }

  public void setMiembroMercado(final String valor) throws PTIMessageException {
    this.set(MIEMBRO_MERCADO, valor);
  }

  public void setImporte(final String valor) throws PTIMessageException {
    this.set(IMPORTE, valor);
  }

}
