package sibbac.pti.messages.fs;

import sibbac.pti.PTIMessageException;
import sibbac.pti.PTIMessageRecord;
import sibbac.pti.PTIMessageRecordType;
import sibbac.pti.PTIMessageType;
import sibbac.pti.messages.AMessage;

/**
 * Mensaje de tipo FS.
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public class FS extends AMessage {

  /**
   * Constructor por defecto.
   */
  public FS() throws PTIMessageException {
    this(false);
  }

  /**
   * Constructor a partir de PTI.
   * 
   * @param fromPTI TRUE o FALSE, dependiendo de si el registro se genera a partir de datos de PTI.
   */
  public FS(final boolean fromPTI) throws PTIMessageException {
    super(PTIMessageType.FS, fromPTI);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.PTIMessage#getRecordInstance(sibbac.pti.PTIMessageRecordType)
   */
  @Override
  public PTIMessageRecord getRecordInstance(final PTIMessageRecordType tipo) {
    // NO hay definidos registros de ningun tipo para este mensaje.
    return null;
  }

  /*
   * (non-Javadoc)
   * @see sibbac.pti.messages.AMessage#setup()
   */
  @Override
  public void messageDefinition() throws PTIMessageException {
    // El bloque de datos comunes.
    this.commonDataBlock = new FSCommonDataBlock();

    // El bloque de datos de control.
    this.controlBlock = null;
  }

  @Override
  public void persist() throws PTIMessageException {
    // TODO Auto-generated method stub
    
  }

}
