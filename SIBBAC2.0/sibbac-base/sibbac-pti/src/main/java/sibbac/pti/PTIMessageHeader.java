package sibbac.pti;

import java.util.Date;

/**
 * Interfaz que describe un header; una cabecera comun de los mensajes PTI.
 * <p/>
 * 
 * 
 * /** Cabecera estandar de mensajeria propietaria.
 * <p/>
 * 
 * La informacion de detalle de la cabecera se encuentra en el PDF de BME:<br/>
 * "Interfaz Mensajes Propietario", Noviembre 2014; pagina(pdf): 3, punto: 2.3.
 *
 * <pre>
 * Identificacion API
 * 1 Tipo de Registro A 4
 * 2 Version A 2 Reservado para uso futuro. Debe ir a espacios.
 * 3 SubAplicacion A 2 Reservado para uso futuro. Debe ir a espacios.
 * 4 Codigo de Error General A 3
 *   - A espacios en mensajes de entrada.
 *   - En mensajes de salida se informara:
 *     000: OK
 *     Distinto de 000: ERROR
 * 
 * Identificacion Emisor Mensaje
 * 5 Origen A 4 Identificacion del Origen del mensaje. En mensajes emitidos por la ECC se informa con: BMCL
 * 6 Usuario Origen A 3 Usuario emisor del mensaje. En mensajes emitidos por la ECC se informa con el codigo de Segmento de la ECC (Ver Tabla 1 en el documento "Tablas de Codificacion")
 * 
 * Identificacion Receptor Mensaje
 * 7 Destino A 4 Identificacion del Destino del mensaje. En mensajes enviados a la ECC se informa con: BMCL
 * 8 Usuario Destino A 3 Usuario Destino del mensaje. En mensajes enviados a la ECC se informa con el codigo de Segmento de la ECC (Ver Tabla 1 en el documento "Tablas de Codificacion")
 * 
 * Identificacion Miembro
 * 9 Miembro X(4) / BIC X(11) A 11 Identificacion del Miembro para el que se emite o recibe la mensajeria. En los mensajes generados por la ECC con destino a todos sus miembros, se rellenara con el caracter "?"
 * 10 Usuario Miembro A 3 Usuario del Miembro para el que se emite o recibe la mensajeria. En los mensajes generados por la ECC con destino a todos sus miembros, se rellenara con el caracter "?"
 * 
 * Datos Adicionales Mensaje
 * 11 Fecha envio Mensaje A 8 AAAAMMDD
 * 12 Hora envio Mensaje A 9 HHMMSSMMM
 * 13 Reservado A 44
 * </pre>
 * 
 * @author Vector IRC Group
 * @version 1.0
 * @since 1.0
 *
 */
public interface PTIMessageHeader extends PTIMessageRecord {

	/**
	 * Tamanyo maximo de la cabecera comun de los mensajes. <br/>
	 * Valor: {@value}
	 */
	public static final int		HEADER_SIZE				= 100;

	// -------------------------------------------- La lista de campos.

	public static final String	TIPO_DE_REGISTRO		= "TIPO_DE_REGISTRO";
	public static final String	VERSION					= "VERSION";
	public static final String	SUB_APLICACION			= "SUB_APLICACION";
	public static final String	GENERAL_ERROR_CODE		= "GENERAL_ERROR_CODE";
	public static final String	ORIGEN					= "ORIGEN";
	public static final String	USUARIO_ORIGEN			= "USUARIO_ORIGEN";
	public static final String	DESTINO					= "DESTINO";
	public static final String	USUARIO_DESTINO			= "USUARIO_DESTINO";
	public static final String	MIEMBRO					= "MIEMBRO";
	public static final String	USUARIO_MIEMBRO			= "USUARIO_MIEMBRO";
	public static final String	FECHA					= "FECHA";
	public static final String	HORA					= "HORA";
	public static final String	RESERVADO				= "RESERVADO";

	public static final int		SIZE_TIPO_DE_REGISTRO	= 4;
	public static final int		SIZE_VERSION			= 2;
	public static final int		SIZE_SUB_APLICACION		= 2;
	public static final int		SIZE_GENERAL_ERROR_CODE	= 3;
	public static final int		SIZE_ORIGEN				= 4;
	public static final int		SIZE_USUARIO_ORIGEN		= 3;
	public static final int		SIZE_DESTINO			= 4;
	public static final int		SIZE_USUARIO_DESTINO	= 3;
	public static final int		SIZE_MIEMBRO			= 11;
	public static final int		SIZE_USUARIO_MIEMBRO	= 3;
	public static final int		SIZE_FECHA				= 8;
	public static final int		SIZE_HORA				= 9;
	public static final int		SIZE_RESERVADO			= 44;

	/**
	 * Metodo que permite conocer el {@link PTIMessageType tipo} de mensaje PTI.
	 * 
	 * @return Una instancia de {@link PTIMessageType tipo}.
	 */
	public PTIMessageType getTipoDeMensaje();

	/**
	 * Metodo que permite conocer el error general del mensaje.
	 * 
	 * @return Un {@link String} con el codigo general de error.
	 */
	public String getErrorCode();

	/**
	 * Metodo que permite saber si el mensaje indica errores.
	 * 
	 * @return TRUE o FALSE.
	 */
	public boolean hasErrors();

	/**
	 * Metodo que permite obtener la fecha de envio.
	 * 
	 * @return Un {@link Date} con la fecha de envio del mensaje.
	 * @throws PTIMessageException
	 *             Si hay errores de parseo de fechas/horas.
	 */
	public Date getFecha() throws PTIMessageException;
	
	
	public void setEntidadDestino(String destino) throws PTIMessageException ;
	
	public String getEntidadDestino();
	
	public void setUsuarioDestino(String usuarioDestino) throws PTIMessageException ;
	
	public String getUsuarioDestino();
	
	public void setMiembro(String miembro) throws PTIMessageException;
	public String getMiembro();

}
