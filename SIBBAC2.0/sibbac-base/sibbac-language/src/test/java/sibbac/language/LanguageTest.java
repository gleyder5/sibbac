package sibbac.language;


import static org.junit.Assert.assertTrue;

import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sibbac.test.BaseTest;


@RunWith( SpringJUnit4ClassRunner.class )
@ContextConfiguration( locations = {
	"classpath:/spring.xml"
} )
@FixMethodOrder( MethodSorters.NAME_ASCENDING )
public class LanguageTest extends BaseTest {

	protected static final Logger	LOG	= LoggerFactory.getLogger( LanguageTest.class );

	@Autowired
	protected ApplicationContext	context;

	/**
	 * The Spring {@link MessageSource} for accesing applcation properties.
	 */
	@Autowired
	private MessageSource			messageSource;

	private String					appName;
	private String					country;
	private String					reportsExportMode;

	public LanguageTest() {
	}

	@PostConstruct
	public void init() {
		String prefix = "[LanguageTest::init(@PostConstruct)] ";
		LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
	}

	@BeforeClass
	public static void beforeClass() {
		String prefix = "[LanguageTest::beforeClass] ";
		LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
	}

	@AfterClass
	public static void afterClass() {
		String prefix = "[LanguageTest::afterClass] ";
		LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
	}

	@Before
	public void before() {
		String prefix = "[LanguageTest::before] ";
		LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
	}

	@After
	public void after() {
		String prefix = "[LanguageTest::after] ";
		LOG.debug( prefix + "For testing purposes, we can perform any operation here." );
	}

	@Test
	public void test11ShowDefault() {
		String prefix = "[LanguageTest::test11ShowDefault] ";
		this.showPerLocale( prefix, Locale.getDefault() );
		assertTrue( "Bad APP Name", this.appName.equalsIgnoreCase( "SIBBAC" ) );
		// assertTrue( "Bad Country", this.country.equalsIgnoreCase( "USA" ) );
		assertTrue( "Bad Reports Export Mode: [" + this.reportsExportMode + "]", this.reportsExportMode.equalsIgnoreCase( "INLINE" ) );
	}

	@Test
	public void test12ShowES() {
		String prefix = "[LanguageTest::test12ShowES] ";
		this.showPerLocale( prefix, new Locale( "es" ) );
		assertTrue( "Bad APP Name", this.appName.equalsIgnoreCase( "SIBBAC" ) );
		assertTrue( "Bad Country", this.country.equalsIgnoreCase( "SPAIN" ) );
		assertTrue( "Bad Reports Export Mode: [" + this.reportsExportMode + "]", this.reportsExportMode.equalsIgnoreCase( "INLINE" ) );
	}

	@Test
	public void test13ShowUK() {
		String prefix = "[LanguageTest::test13ShowUK] ";
		this.showPerLocale( prefix, Locale.UK );
		assertTrue( "Bad APP Name", this.appName.equalsIgnoreCase( "SIBBAC" ) );
		assertTrue( "Bad Country", this.country.equalsIgnoreCase( "ENGLAND" ) );
		assertTrue( "Bad Reports Export Mode: [" + this.reportsExportMode + "]", this.reportsExportMode.equalsIgnoreCase( "INLINE" ) );
	}

	@Test
	public void test14ShowUS() {
		String prefix = "[LanguageTest::test14ShowUS] ";
		this.showPerLocale( prefix, Locale.US );
		assertTrue( "Bad APP Name", this.appName.equalsIgnoreCase( "SIBBAC" ) );
		assertTrue( "Bad Country", this.country.equalsIgnoreCase( "USA" ) );
		assertTrue( "Bad Reports Export Mode: [" + this.reportsExportMode + "]", this.reportsExportMode.equalsIgnoreCase( "INLINE" ) );
	}

	@Test
	public void test15ShowZZ() {
		String prefix = "[LanguageTest::test15ShowZZ] ";
		this.showPerLocale( prefix, new Locale( "zz" ) );
		assertTrue( "Bad APP Name", this.appName.equalsIgnoreCase( "SIBBAC" ) );
		// assertTrue( "Bad Country", this.country.equalsIgnoreCase( "USA" ) );
		assertTrue( "Bad Reports Export Mode: [" + this.reportsExportMode + "]", this.reportsExportMode.equalsIgnoreCase( "INLINE" ) );
	}

	@Test
	public void test20ShowAll() {
		String prefix = "[LanguageTest::test20ShowAll] ";
		this.loadI18NPerLocale( prefix, Locale.getDefault() );
		this.loadI18NPerLocale( prefix, Locale.US );
		this.loadI18NPerLocale( prefix, Locale.UK );
		this.loadI18NPerLocale( prefix, new Locale( "fr_FR" ) );
		this.loadI18NPerLocale( prefix, new Locale( "es" ) );
	}

	private void loadI18NPerLocale( final String prefix, final Locale locale ) {
		LOG.debug( prefix + "+ [locale=={}]", locale );

		String language = locale.getLanguage();
		LOG.debug( prefix + "+ [language=={}]", language );

		Properties properties = ( ( CustomReloadableResourceBundleMessageSource ) messageSource ).getAllProperties( locale );
		LOG.debug( prefix + "+ [properties=={}]", properties );

		Set< String > keySet = properties.stringPropertyNames();
		LOG.debug( prefix + "+ [keySet=={}]", keySet );

		for ( String key : keySet ) {
			if ( !key.startsWith( "i18n" ) ) {
				continue;
			}
			LOG.debug( prefix + "  [{}=={}]", key, properties.get( key ) );
		}
	}

	private void showPerLocale( final String prefix, final Locale locale ) {
		this.appName = this.getProperty( "appName", locale );
		this.country = this.getProperty( "country", locale );
		this.reportsExportMode = this.getProperty( "reports.export.mode", locale );
		LOG.debug( prefix + "[appName=={}] [country=={}] [reportsExportMode=={}]", this.appName, this.country, this.reportsExportMode );
	}

	private String getProperty( final String key, final Locale locale ) {
		String prefix = "[LanguageTest::getProperty] ";
		String value = null;
		try {
			value = this.context.getMessage( key, null, locale );
		} catch ( Exception e ) {
			LOG.error( prefix + "ERROR({}): {}", e.getClass().getName(), e.getMessage() );
		}
		return value;
	}

}
