package sibbac.language;


import java.util.Locale;
import java.util.Properties;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;


public class CustomReloadableResourceBundleMessageSource extends ReloadableResourceBundleMessageSource {

	// http://thinkinginsoftware.blogspot.com.es/2013/02/exposing-all-spring-i18n-messages-to.html

	public Properties getAllProperties( final Locale locale ) {
		this.clearCacheIncludingAncestors();
		PropertiesHolder propertiesHolder = this.getMergedProperties( locale );
		Properties properties = propertiesHolder.getProperties();
		return properties;
	}

}
