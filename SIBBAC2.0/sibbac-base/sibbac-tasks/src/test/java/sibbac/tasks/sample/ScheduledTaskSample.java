package sibbac.tasks.sample;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ScheduledTaskSample {

	protected static final Logger	LOG	= LoggerFactory.getLogger( ScheduledTaskSample.class );

	public ScheduledTaskSample() {
		LOG.info( "<constructor>" );
	}

	public void runFixedDelay() {
		LOG.info( "[runFixedDelay] Date: [{}]", new Date() );
	}

	public void runFixedRate() {
		LOG.info( "[runFixedRate] Date: [{}]", new Date() );
	}

	public void runCron() {
		LOG.info( "[runCron] Date: [{}]", new Date() );
	}
}
