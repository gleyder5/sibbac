package sibbac.tasks;

// Internal
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CalendarIntervalTriggerImpl;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sibbac.tasks.beans.JobInformation;
import sibbac.tasks.database.Blog;
import sibbac.tasks.database.BlogBO;
import sibbac.tasks.database.Job;
import sibbac.tasks.database.JobBO;
import sibbac.tasks.database.JobPK;
import sibbac.tasks.enums.SIBBACJobType;

// import sibbac.tasks.business.Facturacion;

// See: quartz-scheduler.org/api/2.2.0/org/quartz/Scheduler.html

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring.xml"
})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSamples extends TasksBaseTest {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(TestSamples.class);

  // @Autowired
  // private ApplicationContext applicationContext;

  @Autowired
  private BlogBO blogBo;

  @Autowired
  private JobBO jobBo;

  // objectMapper
  // @Autowired
  // private ObjectMapper objectMapper;

  @Test
  public void test01Sample() {
    LOG.debug("[test01Sample]");
    try {
      LOG.debug("[test01Sample] Tasks information...");
      this.showTasksInfo();
      LOG.debug("[test01Sample] Waiting 10 seconds...");
      Thread.sleep(10000);
      LOG.debug("[test01Sample] Tasks information...");
      this.showTasksInfo();
      this.saveJobsInfo();
    } catch (InterruptedException e) {
      LOG.error("ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
    } finally {
      // Thread.currentThread().interrupt();
    }
    LOG.debug("[test01Sample] Out.");
  }

  @SuppressWarnings("unused")
  @Test
  // @Ignore
  public void test02JobExecutions() {
    LOG.debug("[test02JobExecutions]");

    // Try the DAO...
    // Buscamos el Job...
    String jobGroup = "Samples";
    String jobName = "Sample";
    JobPK jobPk = new JobPK(jobGroup, jobName);
    Job job = this.jobBo.findById(jobPk);

    if (job == null) {
      LOG.error("[test02JobExecutions] No jobs found for id: [{}]", jobPk.getJobKey());
      Assert.fail("[test02JobExecutions] No jobs found for id: [" + jobPk.getJobKey() + "]");
    }

    // Tenemos el internalJob. Miremos cuantas veces se ha ejecutado.
    LOG.debug("[test02JobExecutions] GOT a Job: [{}]", job);
    List<Blog> jobExecutions = this.blogBo.getAllJobExecutions(job);

    if (jobExecutions == null) {
      LOG.error("[test02JobExecutions] No jobs executions found for id: [{}]", jobPk.getJobKey());
      Assert.fail("[test02JobExecutions] No jobs executions found for id: [" + jobPk.getJobKey() + "]");
    }

    // Tenemos varias ejecuciones. Veamos una a una.
    LOG.debug("[test02JobExecutions] GOT Job Executions: [{}]", jobExecutions.size());
    List<Blog> history = null;
    List<Blog> blogEntries = null;
    String processId = null;
    Long executionId = null;
    // Tenemos todos los "INI"...
    for (Blog jobExecution : jobExecutions) {
      LOG.trace("[test02JobExecutions] Historico de: {}", jobExecution);
      processId = jobExecution.getProcessId();
      executionId = jobExecution.getExecutionId();

      // Miramos la historia de cada ejecucion...
      blogEntries = this.blogBo.getJobHistory(job, executionId);
      if (blogEntries != null && !blogEntries.isEmpty()) {
        for (Blog blogEntry : blogEntries) {
          // Para cada uno de ellos, las mostramos...
          LOG.trace("[test02JobExecutions] + {}", blogEntry);
        }
      } else {
        LOG.warn("[test02JobExecutions] No blog entries for this execution id ({})", executionId);
      }
    }

    LOG.debug("[test02JobExecutions] Out.");
  }

  /*
   * @Test public void test02Delegated() { LOG.debug( "[test02Delegated]" ); JobDetailImpl bean = ( JobDetailImpl )
   * applicationContext.getBean( "FacturacionTask" ); LOG.debug( "[test02Delegated] Bean class: [{}]", bean.getClass()
   * ); LOG.debug( "[test02Delegated] Job: [{}]", bean.getFullName() ); // Job facturacion = ( Job ) bean.getJobClass();
   * // LOG.debug( "[test02Delegated] Job facturacion achieved!" ); Facturacion facturacion = ( Facturacion )
   * applicationContext.getBean( "facturacion" ); LOG.debug( "[test02Delegated] Job facturacion achieved!" ); }
   */

  @SuppressWarnings("unused")
  private void resumeJob(final String n, final String g) {
    LOG.trace("Resuming internalJob...");
    JobKey jobKey = new JobKey(n, g);
    try {
      this.scheduler = this.quartzSchedulerFactoryBean.getScheduler();
      LOG.trace("BEFORE: {}", this.scheduler.getJobDetail(jobKey));
      this.scheduler.resumeJob(jobKey);
      LOG.trace(" AFTER: {}", this.scheduler.getJobDetail(jobKey));
    } catch (SchedulerException e) {
      LOG.error("ERROR({}) pausing internalJob [{}]: [{}]", e.getClass().getName(), jobKey, e.getMessage());
    }
  }

  private void showTasksInfo() {
    try {
      this.scheduler = this.quartzSchedulerFactoryBean.getScheduler();
      LOG.debug("[test01Sample] Scheduler ID: [{}]", this.scheduler.getSchedulerInstanceId());
      LOG.debug("[test01Sample] Scheduler Name: [{}]", this.scheduler.getSchedulerName());

      SchedulerContext context = this.scheduler.getContext();
      LOG.debug("[test01Sample] Current Context: [{}]", context);

      List<JobExecutionContext> currentJobs = this.scheduler.getCurrentlyExecutingJobs();
      if (currentJobs != null && !currentJobs.isEmpty()) {
        for (JobExecutionContext jeContext : currentJobs) {
          LOG.debug("[test01Sample] + Job: [{}]", jeContext);
        }
      } else {
        LOG.debug("[test01Sample] No current Jobs.");
      }

      List<String> groupNames = this.scheduler.getJobGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        JobDetail jobDetail = null;
        for (String name : groupNames) {
          Set<JobKey> jobKeys = this.scheduler.getJobKeys(GroupMatcher.jobGroupEquals(name));
          if (jobKeys != null && !jobKeys.isEmpty()) {
            for (JobKey jobKey : jobKeys) {
              jobDetail = this.scheduler.getJobDetail(jobKey);
              LOG.debug("[test01Sample] + Job[{}]: [{}]", name, jobDetail);
            }
          } else {
            LOG.debug("[test01Sample] No JobKeys.");
          }
        }
      } else {
        LOG.debug("[test01Sample] No Jobs.");
      }

      groupNames = this.scheduler.getTriggerGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        Trigger trigger = null;
        for (String name : groupNames) {
          Set<TriggerKey> triggerKeys = this.scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(name));
          if (triggerKeys != null && !triggerKeys.isEmpty()) {
            for (TriggerKey triggerKey : triggerKeys) {
              trigger = this.scheduler.getTrigger(triggerKey);
              LOG.debug("[test01Sample] + Trigger[{}]: [{}]", name, trigger);
            }
          } else {
            LOG.debug("[test01Sample] No TriggerKeys.");
          }
        }
      } else {
        LOG.debug("[test01Sample] No Triggers.");
      }

    } catch (SchedulerException e) {
      LOG.error("ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
    }
  }

  private void saveJobsInfo() {
    Set<JobInformation> jobsInfo = new TreeSet<JobInformation>();
    JobInformation jobInfo = null;

    this.scheduler = this.quartzSchedulerFactoryBean.getScheduler();

    try {
      LOG.trace(prefix + "Scheduler ID: [{}]", this.scheduler.getSchedulerInstanceId());
      LOG.trace(prefix + "Scheduler Name: [{}]", this.scheduler.getSchedulerName());

      SchedulerContext context = this.scheduler.getContext();
      LOG.trace(prefix + "Current Context: [{}]", context);
      List<String> groupNames = this.scheduler.getTriggerGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        Trigger trigger = null;
        TriggerState triggerState = null;
        JobDetail jobDetail = null;
        JobKey jobKey = null;
        for (String name : groupNames) {
          Set<TriggerKey> triggerKeys = this.scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(name));
          if (triggerKeys != null && !triggerKeys.isEmpty()) {
            for (TriggerKey triggerKey : triggerKeys) {
              jobInfo = new JobInformation();
              jobInfo.setSchedulerInstanceId(this.scheduler.getSchedulerInstanceId());
              jobInfo.setSchedulerName(this.scheduler.getSchedulerName());
              trigger = this.scheduler.getTrigger(triggerKey);
              jobKey = trigger.getJobKey();
              jobDetail = this.scheduler.getJobDetail(jobKey);
              triggerState = this.scheduler.getTriggerState(triggerKey);

              // http://quartz-scheduler.org/documentation/quartz-2.x/cookbook/UpdateTrigger
              /*
               * // To change launching time. tb = trigger.getTriggerBuilder(); cisb =
               * CalendarIntervalScheduleBuilder.calendarIntervalSchedule(); // cisb.withIn newTrigger =
               * tb.withSchedule( cisb ).build(); this.scheduler.rescheduleJob( triggerKey, newTrigger );
               */

              jobInfo.setCalendarName(trigger.getCalendarName());
              jobInfo.setTriggerDescription(trigger.getDescription());
              jobInfo.setEndTime(trigger.getEndTime());
              jobInfo.setFinalFireTime(trigger.getFinalFireTime());
              jobInfo.setMisfireInstruction(trigger.getMisfireInstruction());
              jobInfo.setNextFireTime(trigger.getNextFireTime());
              jobInfo.setPreviousFireTime(trigger.getPreviousFireTime());
              jobInfo.setPriority(trigger.getPriority());
              jobInfo.setStartTime(trigger.getStartTime());
              jobInfo.setGroup(jobKey.getGroup());
              jobInfo.setName(jobKey.getName());
              jobInfo.setJobDetailDescription(jobDetail.getDescription());
              jobInfo.setTriggerState(triggerState.name());
              jobInfo.setJobType(SIBBACJobType.CALENDAR_INTERVAL_JOB);
              if (trigger instanceof CalendarIntervalTriggerImpl) {

                jobInfo.setRepeatInterval(((CalendarIntervalTriggerImpl) trigger).getRepeatInterval());
                jobInfo.setRepeatIntervalUnit(((CalendarIntervalTriggerImpl) trigger).getRepeatIntervalUnit());

                // LOG.trace( prefix + "+ Trigger[{}]: [{}]", name, trigger );
                jobsInfo.add(jobInfo);
              } else if (trigger instanceof CronTriggerImpl) {
                jobInfo.setCronExpression(((CronTriggerImpl) trigger).getCronExpression());
                jobInfo.setJobType(SIBBACJobType.CRON_JOB);
                jobsInfo.add(jobInfo);
              }
            }
          } else {
            LOG.trace(prefix + "No TriggerKeys.");
          }
        }
      } else {
        LOG.trace(prefix + "No Triggers.");
      }
      // Almacenamos en un "properties" el estado de los jobs.
      SIBBACTask.storeJobsInformation(jobsInfo);
    } catch (SchedulerException e) {
      LOG.error(prefix + " ERROR({}): {}", e.getClass().getName(), e.getMessage());
    }

  }

}
