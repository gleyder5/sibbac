package sibbac.tasks.sample;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.tasks.Task;


public class SecondTask implements Task {

	protected static final Logger	LOG	= LoggerFactory.getLogger( SecondTask.class );

	public SecondTask() {
		LOG.debug( "[SecondTask<constructor>]" );
	}

	public void execute() {
		LOG.debug( "[SecondTask<execute>] time: " + new Date() );
	}

}
