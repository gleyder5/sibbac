package sibbac.tasks.sample;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import sibbac.tasks.SIBBACTask;


//@SIBBACJob( group = "Vector", name = "Second", interval = 30, intervalUnit=DateBuilder.IntervalUnit.SECOND, delay = 2 )
public class SecondSIBBACJob extends SIBBACTask {

	protected static final Logger	LOG	= LoggerFactory.getLogger( SecondSIBBACJob.class );

	@Value( "${ftp.url:PEPE}" )
	private String					url;

	@Value( "${ftp.port:PEPE}" )
	private String					port;

	@Value( "${ftp.username:PEPE}" )
	private String					username;

	@Value( "${ftp.password:PEPE}" )
	private String					password;

	public SecondSIBBACJob() {
	}

	public void execute() {
		// LOG.debug( "[FirstSIBBACJob<execute>] time: [{}], url: [{}], port: [{}], username: [{}], password: [{}]", new Date(), url, port,
		// username, password );
	}

}
