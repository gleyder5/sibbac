package sibbac.tasks;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import sibbac.test.BaseTest;


public abstract class TasksBaseTest extends BaseTest {

	protected String				prefix;

	@Autowired
	protected SchedulerFactoryBean	quartzSchedulerFactoryBean;

	protected Scheduler				scheduler;

	public TasksBaseTest() {
	}

	@BeforeClass
	public static void beforeClass() {
		LOG.info( "[beforeClass]" );
	}

	@Before
	public void before() {
		LOG.info( "[before]" );
	}

	@After
	public void after() {
		LOG.info( "[after]" );
	}

	@AfterClass
	public static void afterClass() {
		LOG.info( "[afterClass]" );
	}

}
