package sibbac.tasks.sample;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sibbac.tasks.Task;


@Component
public class ScheduledTasksService {

	protected static final Logger	LOG	= LoggerFactory.getLogger( ScheduledTasksService.class );

	private Task					firstTask;
	private Task					secondTask;

//	@Autowired
//	private ApplicationContext		applicationContext;

	// objectMapper
//	@Autowired
//	private ObjectMapper			objectMapper;

	@Value( "#{sibbacProps['sibbac20.configuration.folder']}" )
	protected String				configFolder;

	public ScheduledTasksService() {
		LOG.info( "ScheduledTasksService started. [configFolder=={}]", this.configFolder );
	}

	@PostConstruct
	public void init() {
		LOG.info( "ScheduledTasksService init method called. [configFolder=={}]", this.configFolder );
	}

	@PreDestroy
	public void destroy() {
		LOG.info( "ScheduledTasksService destroy method called" );
	}

	/**
	 * Execute First Task
	 * 
	 */
	public void executeFirstTask() {
		getFirstTask().execute();
	}

	/**
	 * Execute Second Task
	 * 
	 */
	public void executeSecondTask() {
		getSecondTask().execute();
	}

	/**
	 * Get First Task
	 * 
	 * @return FirstTask
	 */
	public Task getFirstTask() {
		return firstTask;
	}

	/**
	 * Set First Task
	 * 
	 * @param firstTask First Task
	 */
	public void setFirstTask( Task firstTask ) {
		this.firstTask = firstTask;
	}

	/**
	 * Get Second Task
	 * 
	 * @return SecondTask
	 */
	public Task getSecondTask() {
		return secondTask;
	}

	/**
	 * Set Second Task
	 * 
	 * @param secondTask Second Task
	 */
	public void setSecondTask( Task secondTask ) {
		this.secondTask = secondTask;
	}

	public void doIt() {
		// do the actual work
	}
}
