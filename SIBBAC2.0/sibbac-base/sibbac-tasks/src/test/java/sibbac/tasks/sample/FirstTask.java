package sibbac.tasks.sample;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.tasks.Task;


public class FirstTask implements Task {

	protected static final Logger	LOG	= LoggerFactory.getLogger( FirstTask.class );

	public FirstTask() {
		LOG.debug( "[FirstTask<constructor>]" );
	}

	public void execute() {
		LOG.debug( "[FirstTask<execute>] time: " + new Date() );
	}

}
