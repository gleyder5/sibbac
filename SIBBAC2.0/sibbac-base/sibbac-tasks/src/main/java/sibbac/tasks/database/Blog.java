package sibbac.tasks.database;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import sibbac.database.DBConstants;


@SuppressWarnings( "serial" )
@Entity
@Table( name = DBConstants.TASKS.BLOG )
// @EntityListeners( { JPAEventListener.class } )
public class Blog implements Serializable {

	public static final String	EVENT_START		= "INI";
	public static final String	EVENT_STOP		= "END";
	public static final String	EVENT_ERROR		= "ERR";
	public static final String	EVENT_MESSAGE	= "MSG";

	public static final String	TEXT_START		= "Start Job Execution";
	public static final String	TEXT_STOP		= "Stop Job Execution. Ellapsed time: ";

	// ------------------------------------------------- Bean properties

	/**
	 * The entity id.
	 */
	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	@Column( name = "ID", nullable = false, unique = true )
	@XmlAttribute
	protected Long				id;

	/**
	 * The related internalJob.
	 */
	@ManyToOne( targetEntity = Job.class )
	@JoinColumns( {
			@JoinColumn( name = "JOB_GROUP", referencedColumnName = "JOB_GROUP", nullable = false ),
			@JoinColumn( name = "JOB_NAME", referencedColumnName = "JOB_NAME", nullable = false )
	} )
	@XmlAttribute
	protected Job				job;

	/**
	 * The execution ID.
	 */
	@Column( name = "EXECUTION_ID", nullable = false )
	@XmlAttribute
	protected Long				executionId;

	/**
	 * The running process ID.
	 */
	@Column( name = "EVENT", nullable = false )
	@XmlAttribute
	protected String			event;

	/**
	 * The running process ID.
	 */
	@Column( name = "PROCESS_ID", nullable = false )
	@XmlAttribute
	protected String			processId;

	/**
	 * The timestamp of the entry.
	 */
	@Column( name = "MOMENT", nullable = false )
	@XmlAttribute
	protected Date				timestamp;

	/**
	 * The timestamp of the entry.
	 */
	@Column( name = "MESSAGE", nullable = false, length=1024 )
	@XmlAttribute
	protected String			message;

	// ----------------------------------------------- Bean Constructors

	public Blog() {
		this( null, null, null, null, null );
	}

	public Blog( final Job job, final Long executionId, final String processId, final Date timestamp, final String message ) {
		this( job, EVENT_MESSAGE, executionId, processId, timestamp, message );
	}

	public Blog( final Job job, final String event, final Long executionId, final String processId, final Date timestamp,
			final String message ) {
		this.job = job;
		this.event = event;
		this.executionId = executionId;
		this.processId = processId;
		this.timestamp = timestamp;
		this.message = message;
	}

	// ------------------------------- Bean methods: Getters and Setters

	public Long getId() {
		return this.id;
	}

	public void setId( Long id ) {
		this.id = id;
	}

	public Job getJob() {
		return this.job;
	}

	public void setJob( Job job ) {
		this.job = job;
	}

	public String getEvent() {
		return this.event;
	}

	public void setEvent( String event ) {
		this.event = event;
	}

	public Long getExecutionId() {
		return this.executionId;
	}

	public void setExecutionId( Long executionId ) {
		this.executionId = executionId;
	}

	public String getProcessId() {
		return this.processId;
	}

	public void setProcessId( String processId ) {
		this.processId = processId;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp( Date timestamp ) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage( String message ) {
		this.message = message;
	}

	// ----------------------------------------------- Inherited Methods

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append( "BLOG Entry:" );
		sb.append( " [id==" + this.id + "]" );
		sb.append( " [job==" + this.job.getId().getJobKey() + "]" );
		sb.append( " [executionId==" + this.executionId + "]" );
		sb.append( " [event==" + this.event + "]" );
		sb.append( " [processId==" + this.processId + "]" );
		sb.append( " [timestamp==" + this.timestamp + "]" );
		sb.append( " [message==" + this.message + "]" );
		return sb.toString();
	}

	public boolean complains() {
		boolean ok = true;
		ok = ok && ( this.job != null );
		ok = ok && ( this.executionId != null );
		ok = ok && ( this.event != null && !this.event.isEmpty() );
		ok = ok && ( this.processId != null && !this.processId.isEmpty() );
		ok = ok && ( this.timestamp != null );
		ok = ok && ( this.message != null && !this.message.isEmpty() );
		return ok;
	}

}
