/**
 * 
 */
/**
 * 
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
package sibbac.tasks.database;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;

import org.quartz.DateBuilder.IntervalUnit;

import sibbac.database.DBConstants;


@SuppressWarnings( "serial" )
@Entity
@Table( name = DBConstants.TASKS.JOB_INFO )
public class Job implements Serializable {

	// ------------------------------------------------- Bean properties

	/**
	 * The entity id.
	 */
	@EmbeddedId
	@XmlAttribute
	protected JobPK			id;

	/**
	 * The active flag.
	 */
	@Column( name = "ACTIVE", nullable = false )
	@XmlAttribute
	protected boolean		active;

	/**
	 * The interval.
	 */
	@Column( name = "NUM_TIMES" )
	@XmlAttribute
	protected int			interval;

	/**
	 * The interval unit.
	 */
	@Column( name = "INTERVAL_UNIT", nullable = false )
	@XmlAttribute
	protected IntervalUnit	intervalUnit;

	/**
	 * The delay.
	 */
	@Column( name = "DELAY", nullable = false )
	@XmlAttribute
	protected int			delay;

	/**
	 * The start date.
	 */
	@Column( name = "START_DATE", nullable = false )
	@XmlAttribute
	protected Date			startDate;

	// ----------------------------------------------- Bean Constructors

	public Job() {
		super();
	}

	// ------------------------------- Bean methods: Getters and Setters

	public JobPK getId() {
		return this.id;
	}

	public void setId( JobPK id ) {
		this.id = id;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive( boolean active ) {
		this.active = active;
	}

	public int getInterval() {
		return this.interval;
	}

	public void setInterval( int interval ) {
		this.interval = interval;
	}

	public IntervalUnit getIntervalUnit() {
		return this.intervalUnit;
	}

	public void setIntervalUnit( IntervalUnit intervalUnit ) {
		this.intervalUnit = intervalUnit;
	}

	public int getDelay() {
		return this.delay;
	}

	public void setDelay( int delay ) {
		this.delay = delay;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate( Date startDate ) {
		this.startDate = startDate;
	}

	// ----------------------------------------------- Inherited Methods

	public void updateJob( final Job job ) {
		// Apart from the ID...
		this.setActive( job.isActive() );
		this.setDelay( job.getDelay() );
		this.setInterval( job.getInterval() );
		this.setIntervalUnit( job.getIntervalUnit() );
		this.setStartDate( job.getStartDate() );
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append( "Job Entry:" );
		sb.append( " [id==" + this.id.getJobKey() + "]" );
		sb.append( " [active==" + this.active + "]" );
		sb.append( " [interval==" + this.interval + " " + this.intervalUnit + "]" );
		sb.append( " [start==" + this.startDate + " (delay: " + this.delay + " ms)]" );
		return sb.toString();
	}

}
