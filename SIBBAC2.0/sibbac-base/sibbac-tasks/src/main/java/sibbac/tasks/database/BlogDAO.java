package sibbac.tasks.database;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;


@Component
public interface BlogDAO extends JpaRepository< Blog, Long > {

	public List< Blog > findByJobOrderByTimestampDesc( final Job job );

	public List< Blog > findByEventOrderByTimestampDesc( final String event );

	public List< Blog > findDistinctByJobAndEventOrderByTimestampDesc( final Job job, final String event );

	public List< Blog > findByJobAndExecutionIdOrderByTimestampAsc( final Job job, final Long executionId );

	// Paginated

	public Page< Blog > findDistinctByJobAndEventOrderByTimestampDesc( final Job job, final String event, Pageable pageable );

	public Page< Blog > findByJobAndExecutionIdOrderByTimestampAsc( final Job job, final Long executionId, Pageable pageable );

}
