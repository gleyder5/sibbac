package sibbac.tasks;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.quartz.CronTrigger;
import org.quartz.DateBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.InterruptableJob;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.ObjectAlreadyExistsException;
import org.quartz.Scheduler;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerKey;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CalendarIntervalTriggerImpl;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.FormatDataUtils;
import sibbac.tasks.annotations.SIBBACJob;
import sibbac.tasks.beans.JobInformation;
import sibbac.tasks.database.BlogBO;
import sibbac.tasks.database.BlogDAO;
import sibbac.tasks.database.Job;
import sibbac.tasks.database.JobBO;
import sibbac.tasks.database.JobPK;
import sibbac.tasks.enums.SIBBACJobType;
import sibbac.tasks.utils.AutowiringSpringBeanJobFactory;
import sibbac.webapp.annotations.SIBBACService;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * An abstract class that implements the {@link Task} interface and gives some
 * more capabilities.
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 *
 */
public abstract class SIBBACTask extends QuartzJobBean implements Task, InitializingBean, InterruptableJob {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(SIBBACTask.class);

  protected static final String DATE_FORMAT = "yyyy-MM-dd";

  protected static final String TIME_FORMAT = "HH:mm:ss";

  protected static final String DATE_TIME_FORMAT = DATE_FORMAT + " " + TIME_FORMAT;

  @Resource(name = "sibbacProps")
  private Properties taskProps;

  @Autowired
  private BlogBO blogBO;

  @Autowired
  private BlogDAO blogDAO;

  @Autowired
  private JobBO jobBO;

  /**
   * The scheduler to which this task is wired to.
   */
  @Autowired
  protected Scheduler scheduler;

  /**
   * The Spring {@link ApplicationContext}.
   */
  @Autowired
  private ApplicationContext applicationContext;

  /**
   * The Quartz {@link JobDetail}.
   */
  protected JobDetail jobDetail;

  /**
   * The Quartz {@link JobDataMap}. A {@link Map} with key-value pairs.
   */
  protected JobDataMap jobDataMap;

  /**
   * The name of the Quartz-scheduled group of tasks that this one belong to.
   */
  protected String group;

  /**
   * The name of the Quartz-scheduled task.
   */
  protected String name;

  /**
   * The ID of one internalJob execution.
   */
  protected long executionId;

  /**
   * The ID of one internalJob execution.
   */
  protected String processId;

  /**
   * The PK of the internalJob.
   */
  protected JobPK jobPk;

  public JobPK getJobPk() {
    return this.jobPk;
  }

  /**
   * The Quartz internalJob context.
   */
  protected JobExecutionContext jobContext;

  /**
   * The internal internalJob.
   */
  protected Job internalJob;

  @Autowired
  protected SchedulerFactoryBean quartzSchedulerFactoryBean;

  @Autowired
  protected AutowiringSpringBeanJobFactory awsbjf;

  /**
   * The 'interrupt requested' flag.
   */
  protected boolean interruptRequested;

  private static ObjectMapper objectMapper;

  protected static String configFolder;

  protected static String jobsBackupFilename;

  @Autowired
  private ObjectMapper tObjectMapper;

  @Value("#{sibbacProps['sibbac20.configuration.folder']}")
  protected String tConfigFolder;

  @Value("#{sibbacProps['sibbac.jobs.backup.filename'] ?: 'jobs.json'}")
  protected String tJobsBackupFilename;

  /** Indica si esta activa parar las tareas en un intervalo determinado. */
  @Value("#{sibbacProps['tasks.ParadaDiaria']}")
  private boolean bParadaDiariaActiva;

  @Value("#{sibbacProps['tasks.HoraInicioParadaDiaria']}")
  private String sHoraInicioParadaDiaria;

  @Value("#{sibbacProps['tasks.HoraFinParadaDiaria']}")
  private String sHoraFinParadaDiaria;

  // ------------------------------------------------- Startup Methods
  @PostConstruct
  public void init() {
    String prefix = "[SIBBACTask::init] ";
    LOG.trace(prefix + "SIBBACTask init method called.");
    LOG.trace(prefix + "+ [      configFolder=={}]", this.tConfigFolder);
    LOG.trace(prefix + "+ [jobsBackupFilename=={}]", this.tJobsBackupFilename);
    String fullPath = SIBBACTask.configFolder + "/" + SIBBACTask.jobsBackupFilename;
    LOG.trace(prefix + "+ [          fullPath=={}]", fullPath);
    SIBBACTask.configFolder = this.tConfigFolder;
    SIBBACTask.jobsBackupFilename = this.tJobsBackupFilename;
    LOG.trace(prefix + "+ [      configFolder=={}]", SIBBACTask.configFolder);
    LOG.trace(prefix + "+ [jobsBackupFilename=={}]", SIBBACTask.jobsBackupFilename);

    if (SIBBACTask.objectMapper == null) {
      SIBBACTask.objectMapper = tObjectMapper;
    }
  }

  /**
   * This methods reads the information stored in the jobs.json file
   * @return
   */
  public static Set<JobInformation> loadJobsConfiguration() {
    String prefix = "[loadJSON] ";
    LOG.info(prefix + "+ [      configFolder=={}]", configFolder);
    LOG.info(prefix + "+ [jobsBackupFilename=={}]", jobsBackupFilename);
    String fullPath = configFolder + "/" + jobsBackupFilename;
    LOG.info(prefix + "+ [          fullPath=={}]", fullPath);

    Set<JobInformation> jobsInfo = new TreeSet<JobInformation>();

    // READ Json
    LOG.info(prefix + "Reading data from jobs backup file...");
    try {
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      jobsInfo = objectMapper.readValue(new File(fullPath), new TypeReference<Set<JobInformation>>() {
      });
      LOG.info(prefix + "GOT jobs information backup data from jobs backup file.");
    }
    catch (IOException e) {
      LOG.error(prefix + " ERROR({}): {}", e.getClass().getName(), e.getMessage());
    }
    return jobsInfo;
  }

  /**
   * Allows to prepare a Spring bean AFTER properties processing.<br/>
   *
   * Within the application, after bean processing, some beans are
   * post-configured:
   *
   * <ul>
   * <li>{@link MappingJackson2HttpMessageConverter}: For pretty printing</li>
   * <li>{@link SIBBACService}: For SIBBAC Services managment injection</li>
   * </ul>
   */
  public final void afterPropertiesSet() throws Exception {
    String prefix = "[SIBBACTask::afterPropertiesSet] ";
    LOG.trace(prefix + "Received bean: [class=={}]", this.getClass().getName());

    Class<?> myClass = null;
    SIBBACJob annotation = null;

    // Manual "autowire"...
    this.scheduler = this.quartzSchedulerFactoryBean.getScheduler();

    myClass = this.getClass();
    LOG.trace(prefix + "+ Detected bean annotated with: \"SIBBACJob\" [bean=={}]", myClass.getName());
    annotation = myClass.getAnnotation(SIBBACJob.class);
    if (annotation != null) {
      LOG.trace(prefix + "> Identified bean annotated with: \"SIBBACJob\" [class=={}]", myClass.getName());
      this.scheduleJob(this, annotation);
    }

  }

  /*
   * Method for saving job information in an external properties file.
   */
  public final static void storeJobsInformation(final Set<JobInformation> jobsInfo) {
    String prefix = "[SIBBACTask::storeJobsInformation] ";
    String fullPath = configFolder + "/" + jobsBackupFilename;
    // LOG.info( prefix + "+ [          fullPath=={}]", fullPath );
    LOG.debug(prefix + "Saving current job information into file: [file=={}]", fullPath);

    // WRITE Json
    try {
      SIBBACTask.objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(fullPath), jobsInfo);
    }
    catch (IOException e) {
      LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.quartz.InterruptableJob#interrupt
   */
  @Override
  public final void interrupt() throws UnableToInterruptJobException {
    String prefix = "[SIBBACTask::interrupt] ";
    LOG.trace(prefix + "Requested to interrupt: [processId=={}] [group=={}] [name=={}]", this.getProcessId(),
        this.group, this.name);
    this.interruptRequested = true;
  }

  /**
   * Convenience method to check whether a job has been requested to be
   * interrupted.
   * 
   * @return TRUE or FALSE depending whether the user has requested the process
   * to be interrupted.
   */
  protected boolean isInterruptRequested() {
    return this.interruptRequested;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org
   * .quartz.JobExecutionContext)
   */
  @Override
  public final void executeInternal(final JobExecutionContext context) throws JobExecutionException {
    String prefix = "[" + this.getClass().getSimpleName() + "::execute] ";
    LOG.debug(prefix + "Starting job execution...");

    // Control de la ejecución
    // Por de fecto no se para la ejecucion de los jobs
    boolean bPararEjecucion = false;
    if (bParadaDiariaActiva) {
      LOG.debug(prefix + "Parada Diaria Activada:  Se comprueba si se puede ejecutar el job ");

      if ((sHoraInicioParadaDiaria != null) && (sHoraFinParadaDiaria != null)) {

        LOG.debug(prefix + " Parada Establecida entre " + sHoraInicioParadaDiaria + " - " + sHoraFinParadaDiaria);

        // Se comprueba la hora actual con el rango horario establecido.
        bPararEjecucion = hayQuePararTarea();

      }
      else {
        LOG.debug(prefix + "  HORA INICIO O DE FIN DE PARADA NO ESTABLECIDA...- SE EJECUTAN LAS TAREAS ");

      }

    }
    else {
      LOG.debug(prefix + " Parada diaria no activa... - SE EJECUTAN LAS TAREAS");
    }

    // Informacion del Job Quartz
    this.jobContext = context;
    this.jobDetail = context.getJobDetail();
    this.jobDataMap = this.jobDetail.getJobDataMap();
    this.group = context.getJobDetail().getKey().getGroup();
    this.name = context.getJobDetail().getKey().getName();
    this.scheduler = context.getScheduler();

    // Informacion del Job SIBBAC
    this.jobPk = new JobPK(this.group, this.name);
    this.executionId = context.getFireTime().getTime();
    this.internalJob = this.jobBO.findById(this.jobPk);
    this.processId = Thread.currentThread().getName();

    if (!bPararEjecucion) {

      LOG.debug(prefix + ">>> Starting job execution [JobPK: {} / ExecID: {} / ProcID: {}]...", this.jobPk,
          this.executionId, this.processId);

      // Interrupt Request
      this.interruptRequested = false;

      // Sanity check: blogDao.
      BlogDAO blogDao = this.blogBO.getDao();
      if (blogDao == null) {
        LOG.trace(prefix + ">>> SETTING BLOG DAO (this.blogDAO=={})...", this.blogDAO == null);
        this.blogBO.setDao(this.blogDAO);
      }

      LOG.trace(prefix + ">>> START Job Execution.");
      long ini = System.currentTimeMillis();

      try {
        LOG.trace(prefix + ">>> LOGGING START EVENT [blogBO!=null {}].", (this.blogBO != null));
        if (!this.isDelegated()) {
          this.blogBO.start(this.blogDAO, this.executionId, this.internalJob, this.processId);
        }
      }
      catch (Exception e) {
        LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
        if (!this.isDelegated()) {
          this.blogBO.error(this.blogDAO, this.executionId, this.internalJob, this.processId,
              "Error almacenando el business log: " + this.internalJob.getId().getJobKey(), e);
        }
        Throwable t = e.getCause();
        if (t != null) {
          LOG.error(prefix + "  caused by: [{}]", t.getClass().getName(), t.getMessage());
          if (!this.isDelegated()) {
            this.blogBO.error(this.blogDAO, this.executionId, this.internalJob, this.processId,
                "  caused by: " + t.getMessage(), e);
          }
        }
        StackTraceElement[] st = e.getStackTrace();
        if (st.length > 0) {
          for (StackTraceElement ste : st) {
            LOG.error(prefix + "  [{}]", ste);
          }
        }
      }

      try {
        // Ejecucion de la tarea en si
        LOG.trace(prefix + ">>> LAUNCHING TASK EXECUTION.");
        this.execute();
      }
      catch (Exception e) {
        LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
        if (!this.isDelegated()) {
          this.blogBO.error(this.blogDAO, this.executionId, this.internalJob, this.processId,
              "Error ejecutando el job: " + this.internalJob.getId().getJobKey(), e);
        }
        Throwable t = e.getCause();
        if (t != null) {
          LOG.error(prefix + "  caused by: [{}]: {}", t.getClass().getName(), t.getMessage());
          if (!this.isDelegated()) {
            this.blogBO.error(this.blogDAO, this.executionId, this.internalJob, this.processId,
                "  caused by: " + t.getMessage(), e);
          }
        }
        StackTraceElement[] st = e.getStackTrace();
        if (st.length > 0) {
          for (StackTraceElement ste : st) {
            LOG.error(prefix + "  [{}]", ste);
          }
        }
      }

      // Interrupt Request: Set to false after job's execution.
      this.interruptRequested = false;

      long end = System.currentTimeMillis();
      LOG.trace(prefix + "<<< END Job Execution. Elapsed time: [{} ms]. Next planned execution: [{}]", end - ini,
          this.jobContext.getNextFireTime());
      try {
        LOG.trace(prefix + ">>> LOGGING END EVENT.");
        if (!this.isDelegated()) {
          this.blogBO.end(this.blogDAO, this.executionId, this.internalJob, this.processId, end - ini,
              this.jobContext.getNextFireTime());
        }
      }
      catch (Exception e) {
        LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
        if (!this.isDelegated()) {
          this.blogBO.error(this.blogDAO, this.executionId, this.internalJob, this.processId,
              "Error almacenando el business log: " + this.internalJob.getId().getJobKey(), e);
        }
        Throwable t = e.getCause();
        if (t != null) {
          LOG.error(prefix + "  caused by: [{}]", t.getClass().getName(), t.getMessage());
          if (!this.isDelegated()) {
            this.blogBO.error(this.blogDAO, this.executionId, this.internalJob, this.processId,
                "  caused by: " + t.getMessage(), e);
          }
        }
        StackTraceElement[] st = e.getStackTrace();
        if (st.length > 0) {
          for (StackTraceElement ste : st) {
            LOG.error(prefix + "  [{}]", ste);
          }
        }
      }
    }
    else {
      LOG.warn(
          prefix
              + "<<< SUSPEND Job Execution : Intento de ejecucion dentro de la parada diaria programada  [JobPK: {} / ExecID: {} / ProcID: {}]...",
          this.jobPk, this.executionId, this.processId);

    }

  }

  /*
   * (non-Javadoc)
   * 
   * @see sibbac.tasks.Task#execute()
   */
  @Override
  public abstract void execute();

  public final String getProcessId() {
    return Thread.currentThread().getName();
  }

  /**
   * Convenience method to allow tasks to invoke "sub"-tasks.
   * 
   * @param name The name of the internalJob to be launched.
   * @param jobClass The object class that will be launched.
   */
  protected final void launchDelegatedTask(final String jobName, final Class<? extends SIBBACTask> jobClass) {
    this.launchDelegatedTask(jobName, jobClass, 0, null);
  }

  /**
   * Convenience method to allow tasks to invoke "sub"-tasks.
   * <p/>
   * This method is similar to {@link #launchDelegatedTask(String, Class)} but
   * also includes internalJob data as a parameter.
   * 
   * @param name The name of the internalJob to be launched.
   * @param jobClass The object class that will be launched.
   * @param jobDataMap A {@link JobDataMap} with key-value pairs.
   */
  protected final void launchDelegatedTask(final String jobName, final Class<? extends SIBBACTask> jobClass,
      final JobDataMap jobDataMap) {
    this.launchDelegatedTask(jobName, jobClass, 0, jobDataMap);
  }

  /**
   * Convenience method to allow tasks to invoke "sub"-tasks.
   * <p/>
   * This method is similar to {@link #launchDelegatedTask(String, Class)} but
   * also includes an initial delay and internalJob data as parameters.
   * 
   * @param name The name of the internalJob to be launched.
   * @param jobClass The object class that will be launched.
   * @param delay An initial delay in milliseconds.
   * @param jobDataMap A {@link JobDataMap} with key-value pairs.
   */
  protected final void launchDelegatedTask(final String jobName, final Class<? extends SIBBACTask> jobClass,
      final long delay, final JobDataMap jobDataMap) {
    if (jobName != null && jobClass != null && this.scheduler != null) {
      try {
        this.scheduler.scheduleJob(createDelegatedJobDetail(jobName + "Task", jobClass, jobDataMap),
            createDelegatedJobTrigger(jobName + "Trigger", delay));
      }
      catch (SchedulerException e) {
        LOG.error("[SIBBACTask::launchDelegatedTask] ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
      }
    }
    else {
      if (jobName == null) {
        LOG.warn("[SIBBACTask::launchDelegatedTask] No name!");
      }
      if (jobClass == null) {
        LOG.warn("[SIBBACTask::launchDelegatedTask] No jobClass!");
      }
      if (this.scheduler == null) {
        LOG.warn("[SIBBACTask::launchDelegatedTask] No scheduler!");
      }
    }
  }

  /**
   * Grabs a message in the Business log.
   * 
   * @param message The message
   */
  protected final void blog(final String message) {
    this.blogBO.log(this.blogDAO, this.executionId, this.internalJob, this.processId, message);
  }

  // ------------------------------------------------ Internal methods

  private void scheduleJob(final Object bean, final SIBBACJob tag) {
    String prefix = "[SIBBACTask::scheduleJob] ";

    Class<?> jobClass = bean.getClass();
    this.group = tag.group();
    this.name = tag.name();
    this.jobPk = new JobPK(this.group, this.name);

    String triggerName = this.name + "Trigger";
    int interval = tag.interval();
    DateBuilder.IntervalUnit intervalUnit = tag.intervalUnit();
    int delay = tag.delay() * 1000;
    String tagStartDate = tag.startDate();
    String tagStartTime = tag.startTime();
    String tagCronExpression = tag.cronExpression();
    SIBBACJobType jobType = tag.jobType();
    LOG.trace(prefix + "  TAG information...");
    LOG.trace(prefix + "  + [        this.group=={}]", this.group);
    LOG.trace(prefix + "  + [         this.name=={}]", this.name);
    LOG.trace(prefix + "  + [        this.jobPk=={}]", this.jobPk);
    LOG.trace(prefix + "  + [       triggerName=={}]", triggerName);
    LOG.trace(prefix + "  + [          interval=={}]", interval);
    LOG.trace(prefix + "  + [      intervalUnit=={}]", intervalUnit);
    LOG.trace(prefix + "  + [             delay=={}]", delay);
    LOG.trace(prefix + "  + [      tagStartDate=={}]", tagStartDate);
    LOG.trace(prefix + "  + [      tagStartTime=={}]", tagStartTime);
    LOG.trace(prefix + "  + [ tagCronExpression=={}]", tagCronExpression);

    if (this.name != null && jobClass != null && this.scheduler != null) {
      try {
        // Localizamos valores en un archivo externo
        LOG.trace(prefix + "  Localizando propiedades en el archivo externo...");
        String propertyPrefix = "tasks." + this.group + "." + name + ".";
        String startDateKey = propertyPrefix + "startDate";
        String startTimeKey = propertyPrefix + "startTime";
        String activeKey = propertyPrefix + "active";
        String cronExpressionKey = propertyPrefix + "cronExpression";
        String startDate = ((String) taskProps.get(startDateKey));
        String startTime = ((String) taskProps.get(startTimeKey));
        String active = ((String) taskProps.get(activeKey));
        String cronExpresion = ((String) taskProps.get(cronExpressionKey));

        if (tagCronExpression != null) {
          cronExpresion = tagCronExpression;
        }

        LOG.trace(prefix + "  + [   propertyPrefix=={}]", propertyPrefix);
        LOG.trace(prefix + "  + [     startDateKey=={}]", startDateKey);
        LOG.trace(prefix + "  + [     startTimeKey=={}]", startTimeKey);
        LOG.trace(prefix + "  + [        activeKey=={}]", activeKey);
        LOG.trace(prefix + "  + [        startDate=={}]", startDate);
        LOG.trace(prefix + "  + [        startTime=={}]", startTime);
        LOG.trace(prefix + "  + [           active=={}]", active);
        LOG.trace(prefix + "  + [   cronExpression=={}]", cronExpresion);

        LOG.trace(
            prefix
                + "  + [jobClass=={}] [internalJob=={}] [triggerName=={}] [interval=={} ({})] [delay=={} (secs)] [startDate=={}] [startTime=={}] [active=={}] [cronExpresion=={}]",
            jobClass.getName(), this.jobPk.getJobKey(), triggerName, interval, intervalUnit, delay, startDate,
            startTime, active, cronExpresion);

        // Localizamos valores previamente almacenados...
        // Retrieve saved job information
        LOG.trace(prefix + "Searching for saved Job Information for job: [{}-{}]", jobPk.getGroup(), jobPk.getName());
        JobInformation info = this.awsbjf.findSavedJobInformation(this.jobPk);
        if (info != null) {
          LOG.trace(prefix + "GOT saved information for Job: [{}-{}]", jobPk.getGroup(), jobPk.getName());

          // Activo?
          active = info.getTriggerState().equalsIgnoreCase(TriggerState.NORMAL.name()) ? null : "false";
        }
        else {
          LOG.trace(prefix + "No saved information for Job: [{}-{}]", jobPk.getGroup(), jobPk.getName());
        }

        // Check Date...
        Date jobInitialDate = this.parseDate(tagStartDate, tagStartTime, startDate, startTime, this.jobPk.getJobKey());
        LOG.trace(prefix + "    [jobInitialDate=={} ({} {}) - ({})]", jobInitialDate, startDate, startTime,
            this.jobPk.getJobKey());

        // Check Active...
        boolean jobInitialActive = this.parseActive(active);
        LOG.trace(prefix + "  + [jobInitialActive=={}]", jobInitialActive);

        LOG.trace(prefix + "Scheduling a new task:");

        this.scheduler.scheduleJob(createJobDetail(jobClass),
            createJobTrigger(jobType, info, triggerName, jobInitialDate, interval, intervalUnit, delay, cronExpresion));

        // Active?
        LOG.trace(prefix + "    [jobInitialActive=={} ({}) - ({})]", jobInitialActive, active, this.jobPk);
        if (!jobInitialActive
            || (this.getClass().getName() != null && this.getClass().getName()
                .equals("sibbac.business.accounting.tasks.TaskUpdateDatosCompensacionClitit"))) {
          this.pauseJob();
        }

        // Save here internalJob details into database
        LOG.trace(prefix + "Defining and saving a SIBBAC Job Instance...");
        Job jobInstance = new Job();
        jobInstance.setId(this.jobPk);
        jobInstance.setActive(jobInitialActive);
        jobInstance.setInterval(interval);
        jobInstance.setIntervalUnit(intervalUnit);
        jobInstance.setDelay(delay);
        jobInstance.setStartDate(jobInitialDate);

        try {
          LOG.trace(prefix + "Trying to save SIBBAC Job instance: [{}]", jobInstance);
          this.internalJob = this.jobBO.save(jobInstance);
          LOG.trace(prefix + "Saved job instance: [{}]", jobInstance);
        }
        catch (Exception e) {
          LOG.warn(prefix + "Unable to save job!!!: [{}]", jobInstance);
        }

        LOG.trace(prefix + "  + JOB......: [internalJob=={}] [jobClass=={}]", this.jobPk.getJobKey(), jobClass);
        LOG.trace(prefix + "  + TRIGGER..: [trigger=={}] [interval=={} {}] [delay=={} ms]", triggerName, interval,
            intervalUnit, delay);
        LOG.trace(prefix + "  + INNER JOB: {}", this.internalJob);
        this.showSchedulerInfo();
        if (this.internalJob != null && this.internalJob.getId() != null) {
          LOG.info(prefix + "Scheduled job: [{} (active::{})], start date: [{} (delay {}ms)], at intervals: [{} {}]",
              this.internalJob.getId().getJobKey(), jobInitialActive, this.internalJob.getStartDate(),
              this.internalJob.getDelay(), this.internalJob.getInterval(), this.internalJob.getIntervalUnit());
        }
      }
      catch (ObjectAlreadyExistsException e) {
        Throwable cause = e.getCause();
        Throwable underlyingException = e.getUnderlyingException();
        if (cause != null) {
          LOG.warn(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
          LOG.warn(prefix + "> cause({}): [{}]", cause.getClass().getName(), cause.getMessage());
        }
        if (underlyingException != null) {
          LOG.warn(prefix + "> underlyingException({}): [{}]", underlyingException.getClass().getName(),
              underlyingException.getMessage());
        }
      }
      catch (SchedulerException e) {
        LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
      }
    }
    else {
      if (this.name == null) {
        LOG.warn(prefix + "No name!");
      }
      if (jobClass == null) {
        LOG.warn(prefix + "No jobClass!");
      }
      if (this.scheduler == null) {
        LOG.warn(prefix + "No scheduler!");
      }
    }
  }

  private JobDetail createJobDetail(final Class<?> jobClass) {
    LOG.trace("[createJobDetail] + Job detail...");
    final JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
    factoryBean.setApplicationContext(this.applicationContext);
    factoryBean.setName(this.jobPk.getName());
    factoryBean.setGroup(this.jobPk.getGroup());
    factoryBean.setJobClass(jobClass);
    factoryBean.setDescription("Job for " + jobClass.getSimpleName());
    factoryBean.afterPropertiesSet();
    JobDetail jobDetail = factoryBean.getObject();
    return jobDetail;
  }

  private Trigger createJobTrigger(final SIBBACJobType jobType, final JobInformation info, final String triggerName,
      final Date initDate, final int interval, final DateBuilder.IntervalUnit intervalUnit, final long delay,
      final String cronExpression) {
    LOG.trace("[createJobTrigger] + Job trigger...");
    if (info != null) {
      info.setJobType(jobType);
    }
    if (SIBBACJobType.CRON_JOB == jobType) {
      final CronTriggerImpl cronTrigger = new CronTriggerImpl();
      cronTrigger.setName(triggerName);
      if (StringUtils.isNotBlank(cronExpression)) {
        try {
          cronTrigger.setCronExpression(cronExpression);
        }
        catch (ParseException e) {
          LOG.warn("[createJobTrigger] ", e);
        }
      }
      cronTrigger.setJobName(this.jobPk.getName());
      cronTrigger.setJobGroup(this.jobPk.getGroup());
      cronTrigger.setDescription("Trigger for " + this.jobPk.getJobKey());
      cronTrigger.setMisfireInstruction(CronTrigger.MISFIRE_INSTRUCTION_DO_NOTHING);

      if (info != null) {

        LOG.trace("[createJobTrigger]   Tunning Job trigger with 'saved' job information...");
        long savedInitDate = info.getStartTime().getTime();
        Date savedEndTime = info.getEndTime();
        // int savedMisfireInstruction = info.getMisfireInstruction();

        cronTrigger.setStartTime(new Date(savedInitDate + delay));
        cronTrigger.setEndTime(savedEndTime);
        // cronTrigger.setMisfireInstruction(savedMisfireInstruction);
        if (StringUtils.isNotBlank(info.getCronExpression())) {
          try {
            cronTrigger.setCronExpression(info.getCronExpression());
          }
          catch (ParseException e) {
            LOG.warn("[createJobTrigger] ", e);
          }
        }
      }

      return cronTrigger;
    }
    else {
      final CalendarIntervalTriggerImpl trigger = new CalendarIntervalTriggerImpl();
      trigger.setName(triggerName);
      trigger.setJobName(this.jobPk.getName());
      trigger.setJobGroup(this.jobPk.getGroup());
      trigger.setDescription("Trigger for " + this.jobPk.getJobKey());

      // Desde aqui hay que mirar tambien en el "info".
      trigger.setStartTime(new Date(initDate.getTime() + delay));
      trigger.setEndTime(null);
      trigger.setRepeatInterval(interval);
      trigger.setRepeatIntervalUnit(intervalUnit);
      trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_SMART_POLICY);

      if (info != null) {
        LOG.trace("[createJobTrigger]   Tunning Job trigger with 'saved' job information...");
        long savedInitDate = info.getStartTime().getTime();
        Date savedEndTime = info.getEndTime();
        int savedRepeatInterval = info.getRepeatInterval();
        IntervalUnit savedRepeatIntervalUnit = info.getRepeatIntervalUnit();
        int savedMisfireInstruction = info.getMisfireInstruction();

        trigger.setStartTime(new Date(savedInitDate + delay));
        trigger.setEndTime(savedEndTime);
        trigger.setRepeatInterval(savedRepeatInterval);
        trigger.setRepeatIntervalUnit(savedRepeatIntervalUnit);
        trigger.setMisfireInstruction(savedMisfireInstruction);
      }

      // NEORIS: Cambio para solucionar incidencia 'Las tareas programadas para
      // ejecutarse a una hora deben hacerlo a
      // esa
      // hora
      // y no ejecutarse cada vez que se reinicia el servidor por cualquier
      // motivo.'
      Date now = new Date();
      if (initDate.after(now)) {
        // Se establece la fecha de inicio como la fecha calculada y se programa
        // la ejecución para ese momento
        trigger.setStartTime(new Date(initDate.getTime() + delay));
      }
      return trigger;
    }
  }

  private JobDetail createDelegatedJobDetail(final String jobName, final Class<?> jobClass, final JobDataMap jobDataMap) {
    final JobDetailFactoryBean factoryBean = new JobDetailFactoryBean();
    factoryBean.setApplicationContext(this.applicationContext);
    factoryBean.setName(jobName);
    factoryBean.setGroup(this.group);
    factoryBean.setJobClass(jobClass);
    factoryBean.setJobDataAsMap(jobDataMap);
    factoryBean.setJobDataMap(jobDataMap);
    factoryBean.afterPropertiesSet();
    return factoryBean.getObject();
  }

  private Trigger createDelegatedJobTrigger(final String triggerName, final long delay) {
    final SimpleTriggerImpl trigger = new SimpleTriggerImpl();
    trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
    trigger.setStartTime(new Date(System.currentTimeMillis() + delay));
    trigger.setRepeatCount(0);
    trigger.setName(triggerName);
    trigger.setGroup(this.group);
    trigger.setRepeatCount(0);
    return trigger;
  }

  private void pauseJob() {
    String prefix = "[SIBBACTask::pauseJob] ";
    LOG.trace(prefix + "  Pausing internalJob...");
    JobKey jobKey = new JobKey(this.jobPk.getName(), this.jobPk.getGroup());
    try {
      LOG.trace(prefix + "  BEFORE: {}", this.scheduler.getJobDetail(jobKey));
      this.scheduler.pauseJob(jobKey);

      LOG.trace(prefix + "   AFTER: {}", this.scheduler.getJobDetail(jobKey));
    }
    catch (SchedulerException e) {
      LOG.error(prefix + "ERROR({}) pausing internalJob [{}]: [{}]", e.getClass().getName(), jobKey, e.getMessage());
    }
  }

  private Date parseDate(final String tagStartDate, final String tagStartTime, final String startDate,
      final String startTime, final String jobKeyName) {
    String prefix = "[SIBBACTask::parseDate] ";
    Calendar calendar = Calendar.getInstance();
    Calendar auxCalendar = Calendar.getInstance();

    Date dateStartDate = new Date();
    Date dateStartTime = new Date();
    if (tagStartDate != null && tagStartDate.length() > 0) {
      dateStartDate = FormatDataUtils.convertStringToDate(tagStartDate, DATE_FORMAT);
    }
    if (tagStartTime != null && tagStartTime.length() > 0) {
      dateStartTime = FormatDataUtils.convertStringToTime(tagStartTime, TIME_FORMAT);
    }
    LOG.trace(prefix + "  + [dateStartDate=={}]", dateStartDate);
    LOG.trace(prefix + "  + [dateStartDate=={}]", dateStartDate);
    auxCalendar.setTime(dateStartDate);
    calendar.set(Calendar.YEAR, auxCalendar.get(Calendar.YEAR));
    calendar.set(Calendar.MONTH, auxCalendar.get(Calendar.MONTH));
    calendar.set(Calendar.DAY_OF_MONTH, auxCalendar.get(Calendar.DAY_OF_MONTH));
    auxCalendar.setTime(dateStartTime);
    calendar.set(Calendar.HOUR_OF_DAY, auxCalendar.get(Calendar.HOUR_OF_DAY));
    calendar.set(Calendar.MINUTE, auxCalendar.get(Calendar.MINUTE));
    calendar.set(Calendar.SECOND, auxCalendar.get(Calendar.SECOND));
    LOG.trace(prefix + "Received TAG calendar: [{}]", FormatDataUtils.toHuman(calendar.getTime()));

    // ========================================================================
    // Hay que inferir una fecha a partir de "startDate" y "startTime".
    // ========================================================================

    if ((startDate != null) && !startDate.equalsIgnoreCase("now") && !startDate.equalsIgnoreCase("null")) {
      // Tenemos una fecha: DATE_FORMAT
      SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
      Date received = null;
      Calendar newDate = null;
      try {
        received = formatter.parse(startDate);
        LOG.trace(prefix + "Recibida la fecha: [{}]", received);
        // Ya tenemos la "fecha". Actualizamos el calendar con esa fecha.
        newDate = Calendar.getInstance();
        newDate.setTime(received);
        // Y ajustamos...
        calendar.set(Calendar.YEAR, newDate.get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, newDate.get(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, newDate.get(Calendar.DAY_OF_MONTH));
        LOG.trace(prefix + "Updated calendar: [{}]", FormatDataUtils.toHuman(calendar.getTime()));
      }
      catch (ParseException e) {
        LOG.warn(prefix + "Error({}=={}) parseando la fecha: [{}] para el SIBBACJob: [{}]. Valid format: [{}]", e
            .getClass().getName(), e.getMessage(), startDate, jobKeyName, DATE_FORMAT);
      }
    }

    if ((startTime != null) && !startTime.equalsIgnoreCase("now") && !startTime.equalsIgnoreCase("null")) {
      // Tenemos una hora: TIME_FORMAT
      SimpleDateFormat formatter = new SimpleDateFormat(TIME_FORMAT);
      Date received = null;
      Calendar newDate = null;
      try {
        received = formatter.parse(startTime);
        LOG.trace(prefix + "Recibida la hora: [{}]", received);
        // Ya tenemos la "hora". Actualizamos el calendar con esa hora.
        newDate = Calendar.getInstance();
        newDate.setTime(received);
        // Y ajustamos...
        calendar.set(Calendar.HOUR_OF_DAY, newDate.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, newDate.get(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, newDate.get(Calendar.SECOND));
        LOG.trace(prefix + "Updated calendar: [{}]", FormatDataUtils.toHuman(calendar.getTime()));
      }
      catch (ParseException e) {
        LOG.warn(prefix + "Error({}=={}) parseando la fecha: [{}] para el SIBBACJob: [{}]. Valid format: [{}]", e
            .getClass().getName(), e.getMessage(), startDate, jobKeyName, DATE_FORMAT);
      }
    }

    // Pasado?
    Calendar now = Calendar.getInstance();
    if (calendar.before(now)) {
      // NEORIS: Cambio para solucionar incidencia 'Las tareas programadas para
      // ejecutarse a una hora deben hacerlo a
      // esa hora
      // y no ejecutarse cada vez que se reinicia el servidor por cualquier
      // motivo.'
      // Si el proceso tenía establecido una fecha de ejecución esta no se
      // cambia
      if ((tagStartDate != null && tagStartDate.length() > 0)
          || ((startDate != null) && !startDate.equalsIgnoreCase("now") && !startDate.equalsIgnoreCase("null"))) {
        LOG.trace(prefix + " tagStartDate or startDate from Task passed.");
      }
      else {
        // Si el proceso tenía establecido una hora de ejecución, se pone la
        // misma hora pero del día siguiente
        if ((tagStartTime != null && tagStartTime.length() > 0)
            || ((startTime != null) && !startTime.equalsIgnoreCase("now") && !startTime.equalsIgnoreCase("null"))) {
          LOG.trace(prefix + "\"Passed\" hour. Set to tomorrow, same hour.");
          calendar.add(Calendar.DAY_OF_MONTH, +1);
          LOG.trace(prefix + "Updated calendar: [{}]", FormatDataUtils.toHuman(calendar.getTime()));
        }
        else {
          // Le ponemos la fecha de hoy.
          LOG.trace(prefix + "\"Passed\" date. Set to \"now\".");
          calendar = now;
          LOG.trace(prefix + "Updated calendar: [{}]", FormatDataUtils.toHuman(calendar.getTime()));
        }
      }
    }

    return calendar.getTime();
  }

  private boolean parseActive(final String active) {
    if (active == null) {
      // True
      return true;
    }

    return Boolean.parseBoolean(active);
  }

  private void showSchedulerInfo() {
    String prefix = "[SIBBACTask::showSchedulerInfo] ";
    try {
      // this.scheduler = this.quartzSchedulerFactoryBean.getScheduler();
      LOG.trace(prefix + "Scheduler ID: [{}]", this.scheduler.getSchedulerInstanceId());
      LOG.trace(prefix + "Scheduler Name: [{}]", this.scheduler.getSchedulerName());

      SchedulerContext context = this.scheduler.getContext();
      LOG.trace(prefix + "Current Context: [{}]", context);

      List<JobExecutionContext> currentJobs = this.scheduler.getCurrentlyExecutingJobs();
      if (currentJobs != null && !currentJobs.isEmpty()) {
        for (JobExecutionContext jeContext : currentJobs) {
          LOG.trace(prefix + "+ Job: [{}]", jeContext);
        }
      }
      else {
        LOG.trace(prefix + "No current Jobs.");
      }

      List<String> groupNames = this.scheduler.getJobGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        JobDetail jobDetail = null;
        for (String name : groupNames) {
          Set<JobKey> jobKeys = this.scheduler.getJobKeys(GroupMatcher.jobGroupEquals(name));
          if (jobKeys != null && !jobKeys.isEmpty()) {
            for (JobKey jobKey : jobKeys) {
              jobDetail = this.scheduler.getJobDetail(jobKey);
              LOG.trace(prefix + "+ Job[{}/{}]: [{}]", name, jobKey, jobDetail);
            }
          }
          else {
            LOG.trace(prefix + "No JobKeys.");
          }
        }
      }
      else {
        LOG.trace(prefix + "No Jobs.");
      }

      groupNames = this.scheduler.getTriggerGroupNames();
      if (groupNames != null && !groupNames.isEmpty()) {
        Trigger trigger = null;
        for (String name : groupNames) {
          Set<TriggerKey> triggerKeys = this.scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(name));
          if (triggerKeys != null && !triggerKeys.isEmpty()) {
            for (TriggerKey triggerKey : triggerKeys) {
              trigger = this.scheduler.getTrigger(triggerKey);
              LOG.trace(prefix + "+ Trigger[{}]: [{}] [next=={}] [final=={}]", name, trigger,
                  trigger.getNextFireTime(), trigger.getFinalFireTime());
            }
          }
          else {
            LOG.trace(prefix + "No TriggerKeys.");
          }
        }
      }
      else {
        LOG.trace(prefix + "No Triggers.");
      }

    }
    catch (SchedulerException e) {
      LOG.error(prefix + "ERROR({}): [{}]", e.getClass().getName(), e.getMessage());
    }
  }

  protected boolean isDelegated() {
    return this.internalJob == null;
  }

  // Metodo que comprueba los valores de configuración y a partir de la hora
  // actual y las horas de parada indica si se
  // puede o no ejecutar la tarea.
  // true -> Hay que parar la tarea porque estamos dentro del intervalo de
  // parada.
  // false -> No hay que pararla
  private boolean hayQuePararTarea() {
    String prefix = "[SIBBACTask::hayQuePararTarea] ";

    // Por defecto no se paran los procesos
    boolean bPararProceso = false;

    try {

      // Se trasforma las horas del fichero de configurancion en dos calendarios
      // con el dia actual y las horas
      // establecidas. Se hace asi para comparar directamente los calendarios.
      // De esta forma, tambien se controlan errores en el formato del fichero
      // de configuración

      Date dateStartTime = new Date();
      Date dateEndTime = new Date();
      String sDiaActual = FormatDataUtils.convertDateToString(new Date(), DATE_FORMAT);
      String sHoraActual = FormatDataUtils.convertDateToString(new Date(), TIME_FORMAT);

      Calendar calInicioParada = Calendar.getInstance();

      Calendar calFinParada = Calendar.getInstance();

      SimpleDateFormat FormatoHora = new SimpleDateFormat(TIME_FORMAT);

      Date horaInicio = FormatoHora.parse(sHoraInicioParadaDiaria);
      Date horaFin = FormatoHora.parse(sHoraFinParadaDiaria);
      Date horaActual = FormatoHora.parse(sHoraActual);

      // Si la hora de Fin es mayor que la hora de inicio - las horas de inicio
      // y fin son del mismo dia.

      if (horaFin.getTime() > horaInicio.getTime()) {
        dateStartTime = FormatDataUtils.convertStringToDate(sDiaActual + " " + sHoraInicioParadaDiaria,
            DATE_TIME_FORMAT);
        dateEndTime = FormatDataUtils.convertStringToDate(sDiaActual + " " + sHoraFinParadaDiaria, DATE_TIME_FORMAT);
        calInicioParada.setTime(dateStartTime);
        calFinParada.setTime(dateEndTime);
      }
      else { // Las horas de inicio y fin son de dias distintos. Hay que formar
             // el dia de la fecha de inicio y el dia
             // de la fecha de fin
        // Este valor va a depender de la hora actual:
        // 1 - Se es mayor que la hora de inicio O BIEN es menor a la hora de
        // inicio pero mayor que la hora de fin. ->
        // (hora de inicio es del dia actual y hora de fin es del dia siguiente)
        // 2 - Si es menor a la hora de inicio y tambien menor a la hora de
        // fin.-> (hora de inicio es del dia ANTERIOR y
        // hora de fin es del dia ACTUAL)
        // Si la hora actual es > que la hora de inicio y la de fin, o bien la
        // hora actual es menor a la hora de inicio
        // y a la hora de fin:
        // el dia actual es el dia de la fecha de inicio y la fecha de fin es
        // del dia siguiente
        if ((horaActual.getTime() > horaInicio.getTime())
            || (horaActual.getTime() < horaInicio.getTime() && horaActual.getTime() > horaFin.getTime())) { // Situacion
                                                                                                            // 1
          dateStartTime = FormatDataUtils.convertStringToDate(sDiaActual + " " + sHoraInicioParadaDiaria,
              DATE_TIME_FORMAT);
          dateEndTime = FormatDataUtils.convertStringToDate(sDiaActual + " " + sHoraFinParadaDiaria, DATE_TIME_FORMAT);
          calInicioParada.setTime(dateStartTime);
          calFinParada.setTime(dateEndTime);
          calFinParada.add(Calendar.DAY_OF_MONTH, 1);

        }
        else { // Situación 2
          dateStartTime = FormatDataUtils.convertStringToDate(sDiaActual + " " + sHoraInicioParadaDiaria,
              DATE_TIME_FORMAT);
          dateEndTime = FormatDataUtils.convertStringToDate(sDiaActual + " " + sHoraFinParadaDiaria, DATE_TIME_FORMAT);
          calInicioParada.setTime(dateStartTime);
          calFinParada.setTime(dateEndTime);
          calInicioParada.add(Calendar.DAY_OF_MONTH, -1);

        }

      }

      Calendar calDiaHoraActualAux = Calendar.getInstance();

      // Si la fecha actual este entre la de iniciio y fin se para
      if (calDiaHoraActualAux.after(calInicioParada) && calDiaHoraActualAux.before(calFinParada)) {
        LOG.debug(prefix + " Se para la ejecucion de la tarea porque esta dentro de la ventana temporal");
        bPararProceso = true;
      }
    }
    catch (Exception e) {

      LOG.error(prefix + "ERROR NO CONTROLADO AL OBTENER EL INTERVALO DE PARADA DE LOS JOGS [{}]", e.getClass()
          .getName(), e.getMessage());
    }
    return bPararProceso;
  }

  protected synchronized boolean canExecuteNow(String initHours, String endHours, String datePattern)
      throws SIBBACBusinessException {
    LOG.debug("[canExecuteNow] initHours: {} endHours: {} datePattern {}", initHours, endHours, datePattern);
    boolean canExecute = true;
    try {
      canExecute = this.canExecuteNowWithTimes(initHours, endHours);
      // if (datePattern == null) {
      // canExecute = this.canExecuteNowWithTimes(initHours, endHours);
      // }
      // else {
      // canExecute = this.canExecuteNowWithDates(initHours, endHours,
      // datePattern);
      // }
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }

    return canExecute;
  }

  // TODO no funciona bien el tema de los time zones
  private boolean canExecuteNowWithDates(String initHours, String endHours, String datePattern)
      throws SIBBACBusinessException {
    boolean canExecute = true;
    List<Date> listEndHours = null;
    List<Date> listInitHours = null;

    if (initHours != null) {
      listInitHours = this.getListDates(initHours.split(","), datePattern);
    }

    if (endHours != null) {
      listEndHours = this.getListDates(endHours.split(","), datePattern);
    }
    if (listInitHours != null && listEndHours != null && listInitHours.size() == listEndHours.size()
        || listInitHours == null && listEndHours != null || listInitHours != null && listEndHours == null) {
      canExecute = this.nowBetweenTwoDates(listInitHours, listEndHours);
    }
    return canExecute;
  }

  private boolean canExecuteNowWithTimes(String initHours, String endHours) throws SIBBACBusinessException {
    boolean canExecute = true;
    List<BigDecimal> listEndHours = null;
    List<BigDecimal> listInitHours = null;

    if (initHours != null) {
      listInitHours = this.getListDates(initHours.split(","));
    }

    if (endHours != null) {
      listEndHours = this.getListDates(endHours.split(","));
    }
    if (listInitHours != null && listEndHours != null && listInitHours.size() == listEndHours.size()
        || listInitHours == null && listEndHours != null || listInitHours != null && listEndHours == null) {
      canExecute = this.nowBetweenTwoTimes(listInitHours, listEndHours);
    }
    else if (listInitHours != null && listEndHours != null && listInitHours.size() != listEndHours.size()) {
      throw new SIBBACBusinessException(String.format(
          "El numero de horas de inicio '%s' no coincide con el numero de horas de fin '%s' configuradas.", initHours,
          endHours));
    }
    return canExecute;
  }

  private List<Date> getListDates(String[] tokens, String pattern) throws SIBBACBusinessException {
    try {
      SimpleDateFormat sdf = new SimpleDateFormat(pattern);
      // sdf.setCalendar(Calendar.getInstance());

      sdf.setTimeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
      List<Date> res = new ArrayList<Date>();
      if (tokens != null) {
        for (String s : tokens) {
          res.add(sdf.parse(s.trim()));
        }
      }
      return res;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
  }

  private List<BigDecimal> getListDates(String[] tokens) throws SIBBACBusinessException {
    try {
      List<BigDecimal> res = new ArrayList<BigDecimal>();
      if (tokens != null) {
        for (String s : tokens) {
          res.add(new BigDecimal(s.trim()));
        }
      }
      return res;
    }
    catch (Exception e) {
      throw new SIBBACBusinessException(e.getMessage(), e);
    }
  }

  private boolean nowBetweenTwoDates(List<Date> listInitHours, List<Date> listEndHours) {
    Date now = new Date();

    boolean between = false;
    if (listInitHours != null && listEndHours != null) {
      for (int i = 0; i < listInitHours.size(); i++) {
        if (this.compareTimes(listInitHours.get(i), now) <= 0 && this.compareTimes(listEndHours.get(i), now) >= 0) {
          between = true;
          break;
        }
      }
    }
    else if (listInitHours != null) {
      for (int i = 0; i < listInitHours.size(); i++) {
        if (this.compareTimes(listInitHours.get(i), now) <= 0) {
          between = true;
          break;
        }
      }
    }
    else if (listEndHours != null) {
      for (int i = 0; i < listEndHours.size(); i++) {
        if (this.compareTimes(listEndHours.get(i), now) >= 0) {
          between = true;
          break;
        }
      }
    }
    return between;
  }

  private boolean nowBetweenTwoTimes(List<BigDecimal> listInitHours, List<BigDecimal> listEndHours)
      throws SIBBACBusinessException {
    BigDecimal now = FormatDataUtils.convertStringToBigDecimal(FormatDataUtils.convertTimeToString(new Date()));
    boolean between = false;
    if (listInitHours != null && listEndHours != null) {
      for (int i = 0; i < listInitHours.size(); i++) {
        if (listInitHours.get(i).compareTo(now) <= 0 && listEndHours.get(i).compareTo(now) >= 0) {
          between = true;
          break;
        }
      }
    }
    else if (listInitHours != null) {
      for (int i = 0; i < listInitHours.size(); i++) {
        if (listInitHours.get(i).compareTo(now) <= 0) {
          between = true;
          break;
        }
      }
    }
    else if (listEndHours != null) {
      for (int i = 0; i < listEndHours.size(); i++) {
        if (listEndHours.get(i).compareTo(now) >= 0) {
          between = true;
          break;
        }
      }
    }
    return between;
  }

  private int compareTimes(Date d1, Date d2) {
    int t1;
    int t2;

    t1 = (int) (d1.getTime() % (24 * 60 * 60 * 1000L));
    t2 = (int) (d2.getTime() % (24 * 60 * 60 * 1000L));
    return (t1 - t2);
  }

}
