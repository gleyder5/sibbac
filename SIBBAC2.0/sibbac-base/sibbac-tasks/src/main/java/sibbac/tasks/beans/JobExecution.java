package sibbac.tasks.beans;


import java.util.List;

import sibbac.tasks.database.Blog;


public class JobExecution {

	private Blog			execution;
	private List< Blog >	history;

	public JobExecution() {
		this( null, null );
	}

	public JobExecution( Blog execution, List< Blog > history ) {
		super();
		this.execution = execution;
		this.history = history;
	}

	public Blog getExecution() {
		return this.execution;
	}

	public void setExecution( Blog execution ) {
		this.execution = execution;
	}

	public List< Blog > getHistory() {
		return this.history;
	}

	public void setHistory( List< Blog > history ) {
		this.history = history;
	}

}
