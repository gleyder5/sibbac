package sibbac.tasks.utils;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.TriggerFiredBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import sibbac.tasks.beans.JobInformation;
import sibbac.tasks.database.JobPK;

/**
 * 
 * See: http://stackoverflow.com/questions/6990767/inject-bean-reference-into-a-quartz-job-in-spring
 * 
 */
public final class AutowiringSpringBeanJobFactory extends SpringBeanJobFactory implements ApplicationContextAware {

	// ------------------------------------------ Bean static properties

	private static final Logger		LOG					= LoggerFactory.getLogger( AutowiringSpringBeanJobFactory.class );

	private transient AutowireCapableBeanFactory	beanFactory;

	@Autowired
	private ObjectMapper			objectMapper;

	@Value( "#{sibbacProps['sibbac20.configuration.folder']}" )
	protected String				configFolder;

	@Value( "#{sibbacProps['sibbac.jobs.backup.filename'] ?: 'jobs.json'}" )
	protected String				jobsBackupFilename;

	@Autowired
	protected SchedulerFactoryBean	quartzSchedulerFactoryBean;

	protected Scheduler				scheduler;

	protected Set< JobInformation > jobsInfo = null;
	
	@Override
	public void setApplicationContext( final ApplicationContext context ) {
		beanFactory = context.getAutowireCapableBeanFactory();
	}

	@PostConstruct
	public void init() {
		String prefix = "[AutowiringSpringBeanJobFactory::init] ";
		LOG.info( prefix + "AutowiringSpringBeanJobFactory init method called." );
		LOG.info( prefix + "+ [      configFolder=={}]", this.configFolder );
		LOG.info( prefix + "+ [jobsBackupFilename=={}]", this.jobsBackupFilename );
		String fullPath = this.configFolder + "/" + this.jobsBackupFilename;
		LOG.info( prefix + "+ [          fullPath=={}]", fullPath );

		this.jobsInfo = new TreeSet< JobInformation >();

		// READ Json
		LOG.info( prefix + "Reading data from jobs backup file..." );
		try {
			this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			this.jobsInfo = this.objectMapper.readValue( new File( fullPath ), new TypeReference<Set<JobInformation>>(){} );
			LOG.info( prefix + "GOT jobs information backup data from jobs backup file." );
		} catch ( IOException e ) {
			LOG.error( prefix + " ERROR({}): {}", e.getClass().getName(), e.getMessage() );
		}
	}

	@PreDestroy
	public void destroy() {
		String prefix = "[AutowiringSpringBeanJobFactory::destroy] ";
		LOG.info( prefix + "Destroy method called" );
		this.scheduler = this.quartzSchedulerFactoryBean.getScheduler();
		try {
			List<String> jobGroupNames = this.scheduler.getJobGroupNames();
			if ( !jobGroupNames.isEmpty() ) {
				for ( String jobGroupName : jobGroupNames ) {
					LOG.info( prefix + "+ [      jobGroupName=={}]", jobGroupName );
				}
			}
		} catch ( SchedulerException e ) {
			LOG.error( prefix + " ERROR({}): {}", e.getClass().getName(), e.getMessage() );
		}
	}

	@Override
	protected Object createJobInstance( final TriggerFiredBundle bundle ) throws Exception {
		String prefix = "[AutowiringSpringBeanJobFactory::createJobInstance] ";
		final Object job = super.createJobInstance( bundle );
		beanFactory.autowireBean( job );
		LOG.info( prefix + "Autowired Job: [{} ({})]", job, job.getClass().getName() );
		return job;
	}

	public final JobInformation findSavedJobInformation( JobPK jobPk ) {
		String prefix = "[AutowiringSpringBeanJobFactory::findSavedJobInformation] ";
		if ( this.jobsInfo== null ) {
			LOG.info( prefix + "NULL Jobs Information" );
			return null;
		}
		
		if ( this.jobsInfo.isEmpty() ) {
			LOG.info( prefix + "EMPTY Jobs Information" );
			return null;
		}

		JobInformation info = null;
		String group = jobPk.getGroup();
		String name  = jobPk.getName();
		LOG.trace( prefix + "Looking for: [{}-{}]", group, name );
		for ( JobInformation jobInfo : this.jobsInfo ) {
			if ( !jobInfo.getGroup().equalsIgnoreCase( group ) ) {
				continue;
			}
			if ( !jobInfo.getName().equalsIgnoreCase( name ) ) {
				continue;
			}
			LOG.trace( prefix + "Found!" );
			info = jobInfo;
			break;
		}
		return info;
	}
	
	/**
	 * This method searches through the jobsInfo List and replaces the item 
	 * identified by the jobPk with the value input
	 * @param jobPk
	 * @param input
	 */
	 public final void updateJobInformation( JobPK jobPk, JobInformation input ) {
	    String prefix = "[AutowiringSpringBeanJobFactory::findSavedJobInformation] ";
	    if ( this.jobsInfo== null ) {
	      LOG.info( prefix + "NULL Jobs Information" );	      
	    }
	    
	    if ( this.jobsInfo.isEmpty() ) {
	      LOG.info( prefix + "EMPTY Jobs Information" );
	    }

	    String group = jobPk.getGroup();
	    String name  = jobPk.getName();
	    LOG.trace( prefix + "Looking for: [{}-{}]", group, name );
	    for ( JobInformation jobInfo : this.jobsInfo ) {
	      if ( !jobInfo.getGroup().equalsIgnoreCase( group ) ) {
	        continue;
	      }
	      if ( !jobInfo.getName().equalsIgnoreCase( name ) ) {
	        continue;
	      }
	      LOG.trace( prefix + "Found!" );
	      
	      this.jobsInfo.remove(jobInfo);
	      this.jobsInfo.add(input);
	      
	      break;
	    }
	  }
}
