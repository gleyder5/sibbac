package sibbac.tasks.database;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;


@Component
public interface JobDAO extends JpaRepository< Job, JobPK > {
}
