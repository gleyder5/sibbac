package sibbac.tasks.database;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;


@SuppressWarnings( "serial" )
@Embeddable
public class JobPK implements Serializable {

	/**
	 * The process group.
	 */
	@Column( name = "JOB_GROUP", nullable = false )
	@XmlAttribute
	protected String	group;

	/**
	 * The process name.
	 */
	@Column( name = "JOB_NAME", nullable = false )
	@XmlAttribute
	protected String	name;

	public JobPK() {
	}

	public JobPK( final String group, final String name ) {
		super();
		this.group = group;
		this.name = name;
	}

	public String getGroup() {
		return this.group;
	}

	public void setGroup( String group ) {
		this.group = group;
	}

	public String getName() {
		return this.name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return this.getJobKey().hashCode();
	}

	public String getJobKey() {
		return this.group + "." + this.name;
	}

	@Override
	public boolean equals( final Object obj ) {
		if ( obj == null ) {
			return false;
		}
		if ( this.getJobKey().equalsIgnoreCase( ( ( JobPK ) obj ).getJobKey() ) ) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "PK: [" + this.group + "." + this.name + "]";
	}

}
