package sibbac.tasks.database;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.bo.AbstractBo;


@Service
public class JobBO extends AbstractBo< Job, JobPK, JobDAO > {

	public List< Job > getAllJobs() {
		return this.dao.findAll();
	}

	/**
	 * Save a internalJob information entry in the database.
	 * 
	 * @param internalJob The internalJob information.
	 * @return
	 */
	@Transactional
	public Job save( final Job job ) {
		String prefix = "[JobBO::save] ";
		JobPK id = job.getId();
		// First, let's see whether the internalJob exists or not in the database...
		Job existing = null;
		if ( this.dao.exists( id ) ) {
			// Retrieve the internalJob and modify accordingly.
			existing = this.dao.findOne( id );
			existing.updateJob( job );
		} else {
			existing = job;
			LOG.warn( prefix + "Job not found in database: [{}]", job );
		}

		try {
			LOG.trace( prefix + "Trying to save a Job: [{}]", job );
			existing = this.dao.save( existing );
			LOG.trace( prefix + "Saved Job: [{}]", job );
			return existing;
		} catch ( Exception e ) {
			LOG.error( prefix + "Unable to save Job (saving)!!! [{}] [{}]", job, e.getMessage() );
			return job;
		}
	}

}
