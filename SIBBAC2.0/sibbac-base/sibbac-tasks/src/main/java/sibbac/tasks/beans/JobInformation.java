package sibbac.tasks.beans;

import java.util.Date;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.quartz.DateBuilder.IntervalUnit;
import org.quartz.Trigger;

import sibbac.tasks.enums.SIBBACJobType;

/**
 * This bean holds all the information about the running SIBBAC Jobs for web purposes.<br/>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public class JobInformation implements Comparable<JobInformation> {

  private String schedulerInstanceId;
  private String schedulerName;

  private String calendarName;
  private String triggerDescription;
  private Date endTime;
  private Date finalFireTime;
  private int misfireInstruction;
  private Date nextFireTime;
  private Date previousFireTime;
  private int priority;
  private Date startTime;
  private String group;
  private String name;
  private String jobDetailDescription;
  private String triggerState;
  private int repeatInterval;
  private IntervalUnit repeatIntervalUnit;
  
  private SIBBACJobType jobType;
  private String cronExpression;

  public JobInformation() {
  }

  public String getSchedulerInstanceId() {
    return this.schedulerInstanceId;
  }

  public void setSchedulerInstanceId(String schedulerInstanceId) {
    this.schedulerInstanceId = schedulerInstanceId;
  }

  public String getSchedulerName() {
    return this.schedulerName;
  }

  public void setSchedulerName(String schedulerName) {
    this.schedulerName = schedulerName;
  }

  public String getCalendarName() {
    return this.calendarName;
  }

  public void setCalendarName(String calendarName) {
    this.calendarName = calendarName;
  }

  public String getTriggerDescription() {
    return this.triggerDescription;
  }

  public void setTriggerDescription(String triggerDescription) {
    this.triggerDescription = triggerDescription;
  }

  public Date getEndTime() {
    return this.endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Date getFinalFireTime() {
    return this.finalFireTime;
  }

  public void setFinalFireTime(Date finalFireTime) {
    this.finalFireTime = finalFireTime;
  }

  public int getMisfireInstruction() {
    return this.misfireInstruction;
  }

  public String getMisfireInstructionAsString() {
    return (this.misfireInstruction == Trigger.MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY) ? "IGNORE_MISFIRE_POLICY"
                                                                                         : "SMART_POLICY";
  }

  public void setMisfireInstruction(int misfireInstruction) {
    this.misfireInstruction = misfireInstruction;
  }

  public Date getNextFireTime() {
    return this.nextFireTime;
  }

  public void setNextFireTime(Date nextFireTime) {
    this.nextFireTime = nextFireTime;
  }

  public Date getPreviousFireTime() {
    return this.previousFireTime;
  }

  public void setPreviousFireTime(Date previousFireTime) {
    this.previousFireTime = previousFireTime;
  }

  public int getPriority() {
    return this.priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }

  public Date getStartTime() {
    return this.startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public String getGroup() {
    return this.group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getJobDetailDescription() {
    return this.jobDetailDescription;
  }

  public void setJobDetailDescription(String jobDetailDescription) {
    this.jobDetailDescription = jobDetailDescription;
  }

  public String getTriggerState() {
    return this.triggerState;
  }

  public void setTriggerState(String triggerState) {
    this.triggerState = triggerState;
  }

  public int getRepeatInterval() {
    return this.repeatInterval;
  }

  public void setRepeatInterval(int repeatInterval) {
    this.repeatInterval = repeatInterval;
  }

  public IntervalUnit getRepeatIntervalUnit() {
    return this.repeatIntervalUnit;
  }

  public void setRepeatIntervalUnit(IntervalUnit repeatIntervalUnit) {
    this.repeatIntervalUnit = repeatIntervalUnit;
  }

  public String getCronExpression() {
    return cronExpression;
  }

  public void setCronExpression(String cronExpression) {
    this.cronExpression = cronExpression;
  }

  public SIBBACJobType getJobType() {
    return jobType;
  }

  public void setJobType(SIBBACJobType jobType) {
    this.jobType = jobType;
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    final HashCodeBuilder hcb = new HashCodeBuilder();
    hcb.append(getGroup());
    hcb.append(getName());
    return hcb.toHashCode();
  }

  /*
   * (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    final JobInformation runningJobInformation = (JobInformation) obj;
    final EqualsBuilder eqb = new EqualsBuilder();
    eqb.append(this.getGroup(), runningJobInformation.getGroup());
    eqb.append(this.getName(), runningJobInformation.getName());
    return eqb.isEquals();
  }
  
  /**
   * Compares the fields related with the job schedule configuration
   * @param obj
   * @return
   */
  public boolean equalsConfiguration(Object obj) {
    final JobInformation runningJobInformation = (JobInformation) obj;
    final EqualsBuilder eqb = new EqualsBuilder();
    eqb.append(this.getCronExpression(), runningJobInformation.getCronExpression());
    eqb.append(this.getRepeatInterval(), runningJobInformation.getRepeatInterval());
    eqb.append(this.getRepeatIntervalUnit(), runningJobInformation.getRepeatIntervalUnit());
    eqb.append(this.getJobType(), runningJobInformation.getJobType());
    return eqb.isEquals();
  }
  
  

  @Override
  public int compareTo(JobInformation jobInformation) {
    final CompareToBuilder ctb = new CompareToBuilder();
    ctb.append(this.getGroup(), jobInformation.getGroup());
    ctb.append(this.getName(), jobInformation.getName());
    return ctb.toComparison();
  }

}
