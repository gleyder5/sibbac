package sibbac.tasks.database;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.bo.AbstractBo;


@Service
@Component
public class BlogBO extends AbstractBo< Blog, Long, BlogDAO > {

//	@Autowired
//	private JobBO	jobBo;

	/**
	 * Save an entry in the database.
	 * 
	 * @param name The internalJob name.
	 * @param jobId The internalJob id.
	 * @param message The message.
	 */
	@Transactional
	public void start( BlogDAO blogDAO, long executionId, Job job, String processId ) {
		Blog entry = this.newBlog( executionId, job, processId, Blog.EVENT_START, Blog.TEXT_START );
		this.logMessage( blogDAO, entry );
	}

	/**
	 * Save an entry in the database.
	 * 
	 * @param name The internalJob name.
	 * @param jobId The internalJob id.
	 * @param message The message.
	 */
	@Transactional
	public void end( BlogDAO blogDAO, long executionId, Job job, String processId, long duration, Date next ) {
		Blog entry = this.newBlog( executionId, job, processId, Blog.EVENT_STOP, Blog.TEXT_STOP + "[" + duration + "]" );
		this.logMessage( blogDAO, entry );
	}

	/**
	 * Save an entry in the database.
	 * 
	 * @param name The internalJob name.
	 * @param jobId The internalJob id.
	 * @param message The message.
	 */
	@Transactional
	public void error( BlogDAO blogDAO, long executionId, Job job, String processId, String message, Exception e ) {
		String errMessage = "";
		if ( e != null ) {
			errMessage = "ERROR(" + e.getClass().getName() + "): [" + e.getMessage() + "]. ";
		}
		errMessage += message;
		Blog entry = this.newBlog( executionId, job, processId, Blog.EVENT_ERROR, errMessage );
		this.logMessage( blogDAO, entry );
	}

	/**
	 * Save an entry in the database.
	 * 
	 * @param name The internalJob name.
	 * @param jobId The internalJob id.
	 * @param message The message.
	 */
	@Transactional
	public void log( BlogDAO blogDAO, long executionId, Job job, String processId, String message ) {
		Blog entry = this.newBlog( executionId, job, processId, Blog.EVENT_MESSAGE, message );
		this.logMessage( blogDAO, entry );
	}

	private Blog newBlog( long executionId, Job job, String processId, String eventType, String msg ) {
		Blog blogEntry = new Blog();
		blogEntry.setExecutionId( executionId );
		blogEntry.setJob( job );
		blogEntry.setProcessId( processId );
		blogEntry.setEvent( eventType );
		blogEntry.setMessage( msg );
		blogEntry.setTimestamp( new Date() );
		return blogEntry;
	}

	/**
	 * Save an entry in the database.
	 * 
	 * @param name The internalJob name.
	 * @param jobId The internalJob id.
	 * @param message The message.
	 */
	@Transactional
	private void logMessage( BlogDAO blogDAO, Blog entry ) {
		String prefix = "[BlogBO::logMessage] ";
		LOG.trace( prefix + "Received a Blog Entry: [{}]", entry );
		Job job = entry.getJob();
		if ( job == null ) {
			LOG.warn( prefix + "No \"job\" detected in entry!" );
		} else {
			JobPK pk = job.getId();
			if ( entry.complains() ) {
				try {
					LOG.trace( prefix + "Trying to save a Blog entry: [{}]", entry );
					blogDAO.save( entry );
					LOG.trace( prefix + "Saved a Blog entry." );
				} catch ( Exception e ) {
					LOG.error( prefix + "Error saving a Blog message!!! [{}]. Message: [{}::{}]", e.getMessage(), pk, entry );
				}
			} else {
				LOG.warn( prefix + "The entry does not complains!: [()::{}]", pk, entry );
			}
		}
	}

	/**
	 * Retrieve blog entries for a given Job.
	 * 
	 * @param pageRequest
	 * 
	 * @param group The internalJob group.
	 * @param name The internalJob name.
	 * @return A {@link List} of internalJob id's.
	 */
	public List< Blog > getAllJobExecutions( Job job ) {
		return this.dao.findDistinctByJobAndEventOrderByTimestampDesc( job, Blog.EVENT_START );
	}

	/**
	 * Retrieve blog entries for a given Job.
	 * 
	 * @param pageRequest
	 * 
	 * @param group The internalJob group.
	 * @param name The internalJob name.
	 * @return A {@link List} of internalJob id's.
	 */
	public Page< Blog > getAllJobExecutions( Job job, PageRequest pageRequest ) {
		return this.dao.findDistinctByJobAndEventOrderByTimestampDesc( job, Blog.EVENT_START, pageRequest );
	}

	/**
	 * Retrieve blog entries for a given Job ID.
	 * 
	 * @param jobId The internalJob id.
	 * @return A {@link List} of entries.
	 */
	public List< Blog > getJobHistory( Job job, Long executionId ) {
		return this.dao.findByJobAndExecutionIdOrderByTimestampAsc( job, executionId );
	}

}
