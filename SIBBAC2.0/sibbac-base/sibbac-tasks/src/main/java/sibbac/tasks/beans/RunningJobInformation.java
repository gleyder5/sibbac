package sibbac.tasks.beans;


import java.util.Date;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


/**
 * This bean holds all the information about the running SIBBAC Jobs for web purposes.<br/>
 *
 * @version 4.0.0
 * @since 4.0.0
 * @author Vector SF 2014.
 */
public class RunningJobInformation implements Comparable< RunningJobInformation > {

	private String	fireInstanceId;
	private Date	fireTime;
	private String	jobDescription;
	private String	group;
	private String	name;
	private long	jobRunTime;
	private Date	scheduledFireTime;
	private Date	nextFireTime;
	private Date	previousFireTime;
	private int		refireCount;
	private String	triggerState;

	public RunningJobInformation() {
	}

	public String getFireInstanceId() {
		return this.fireInstanceId;
	}

	public void setFireInstanceId( String fireInstanceId ) {
		this.fireInstanceId = fireInstanceId;
	}

	public Date getFireTime() {
		return this.fireTime;
	}

	public void setFireTime( Date fireTime ) {
		this.fireTime = fireTime;
	}

	public String getJobDescription() {
		return this.jobDescription;
	}

	public void setJobDescription( String jobDescription ) {
		this.jobDescription = jobDescription;
	}

	public String getGroup() {
		return this.group;
	}

	public void setGroup( String group ) {
		this.group = group;
	}

	public String getName() {
		return this.name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public long getJobRunTime() {
		return this.jobRunTime;
	}

	public void setJobRunTime( long jobRunTime ) {
		this.jobRunTime = jobRunTime;
	}

	public Date getScheduledFireTime() {
		return this.scheduledFireTime;
	}

	public void setScheduledFireTime( Date scheduledFireTime ) {
		this.scheduledFireTime = scheduledFireTime;
	}

	public Date getNextFireTime() {
		return this.nextFireTime;
	}

	public void setNextFireTime( Date nextFireTime ) {
		this.nextFireTime = nextFireTime;
	}

	public Date getPreviousFireTime() {
		return this.previousFireTime;
	}

	public void setPreviousFireTime( Date previousFireTime ) {
		this.previousFireTime = previousFireTime;
	}

	public int getRefireCount() {
		return this.refireCount;
	}

	public void setRefireCount( int refireCount ) {
		this.refireCount = refireCount;
	}

	public String getTriggerState() {
		return this.triggerState;
	}

	public void setTriggerState( String triggerState ) {
		this.triggerState = triggerState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append( getGroup() );
		hcb.append( getName() );
		return hcb.toHashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		final RunningJobInformation runningJobInformation = ( RunningJobInformation ) obj;
		final EqualsBuilder eqb = new EqualsBuilder();
		eqb.append( this.getGroup(), runningJobInformation.getGroup() );
		eqb.append( this.getName(), runningJobInformation.getName() );
		return eqb.isEquals();
	}

	@Override
	public int compareTo( RunningJobInformation runningJobInformation ) {
		final CompareToBuilder ctb = new CompareToBuilder();
		ctb.append( this.getGroup(), runningJobInformation.getGroup() );
		ctb.append( this.getName(), runningJobInformation.getName() );
		return ctb.toComparison();
	}

}
