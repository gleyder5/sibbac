package sibbac.test;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public abstract class BaseTest implements TestConstants {

	protected static final Logger	LOG				= LoggerFactory.getLogger( BaseTest.class );

	protected String				txtMessage;

	protected ObjectMapper			mapper			= new ObjectMapper();

	@Rule
	public JUnitTestWatcher			theTestWatcher	= new JUnitTestWatcher();

	@BeforeClass
	public static void beforeClass() {
	}

	@AfterClass
	public static void afterClass() {
	}

	@Before
	public void before() {
	}

	@After
	public void after() {
	}

	public String toPrettyJson( Object obj ) {
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString( obj );
		} catch ( JsonProcessingException e ) {
			throw new RuntimeException();
		}
	}

	public String toJson( Object obj ) {
		try {
			return mapper.writeValueAsString( obj );
		} catch ( JsonProcessingException e ) {
			throw new RuntimeException();
		}
	}

}
