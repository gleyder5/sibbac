package sibbac.test;


public interface TestConstants {

	public static final String	NL			= System.getProperty( "line.separator" );

	public static final String	LINE_START	= "================================================================";

	public static final String	LINE_END	= "----------------------------------------------------------------";

}
