package sibbac.test;


import org.junit.rules.TestName;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SuppressWarnings( "rawtypes" )
public class JUnitTestWatcher extends TestName implements TestConstants {

	protected static final Logger	LOG		= LoggerFactory.getLogger( JUnitTestWatcher.class );

	private static boolean			SHOW	= false;

	private String					prefix	= null;

	private String					clazz	= null;

	private String					method	= null;

	@Override
	public Statement apply( final Statement base, final Description d ) {
		this.method = d.getMethodName();
		this.prefix = "[" + this.getMethod() + "::apply] ";
		Class c = d.getTestClass();
		this.clazz = ( c != null ) ? c.getName() : "<void>";
		this.showDescription( "apply", d );
		return super.apply( base, d );
	}

	public String getClazz() {
		return this.clazz;
	}

	public String getMethod() {
		return this.method;
	}

	public String getPrefix() {
		return this.prefix;
	}

	@Override
	protected void starting( final Description d ) {
		this.prefix = "[" + this.getMethod() + "::starting] ";
		this.showDescription( "starting", d );
		LOG.debug( this.prefix + LINE_START );
	}

	@Override
	protected void finished( final Description d ) {
		this.prefix = "[" + this.getMethod() + "::finished] ";
		this.showDescription( "finished", d );
		LOG.debug( this.prefix + LINE_START + NL );
		super.finished( d );
	}

	@Override
	protected void succeeded( final Description d ) {
		this.prefix = "[" + this.getMethod() + "::succeeded] ";
		this.showDescription( "succeeded", d );
		LOG.debug( this.prefix + LINE_END );
		LOG.debug( this.prefix + "[succeeded]" );
		super.succeeded( d );
	}

	@Override
	protected void failed( final Throwable e, final Description d ) {
		this.prefix = "[" + this.getMethod() + "::failed] ";
		this.showDescription( "failed", d );
		LOG.debug( this.prefix + LINE_END );
		LOG.error( this.prefix + "[failed::WHY==" + e.getMessage() + "]" );
		super.failed( e, d );
	}

	protected void showDescription( final String m, final Description d ) {
		if ( SHOW ) {
			LOG.debug( this.prefix + "    [IsSuite==" + d.isSuite() + "]" );
			LOG.debug( this.prefix + "    [IsEmpty==" + d.isEmpty() + "]" );
			LOG.debug( this.prefix + "    [IsTest==" + d.isTest() + "]" );
			LOG.debug( this.prefix + "    [ClassName==" + d.getClassName() + "]" );
			LOG.debug( this.prefix + "    [DisplayName==" + d.getDisplayName() + "]" );
			LOG.debug( this.prefix + "    [TestClass==" + d.getTestClass() + "]" );
			LOG.debug( this.prefix + "    [MethodName==" + d.getMethodName() + "]" );
			LOG.debug( this.prefix + "    [TestCount==" + d.testCount() + "]" );
			LOG.debug( this.prefix + "    [Children==" + d.getChildren() + "]" );
			LOG.debug( this.prefix + "    [Annotations==" + d.getAnnotations() + "]" );
		}
	}

}
