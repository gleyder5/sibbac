package sibbac.test.jdbc;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

public class DataSourceMock implements DataSource {
	
	private final Connection con;
	
	public DataSourceMock(Connection con) {
		this.con = con;
	}
	
	public DataSourceMock() {
		this.con = null;
	}

  @Override
  public PrintWriter getLogWriter() throws SQLException {
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public void setLogWriter(PrintWriter out) throws SQLException {
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public void setLoginTimeout(int seconds) throws SQLException {
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public int getLoginTimeout() throws SQLException { 
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
  	throw new IllegalAccessError("No implementado en mock");
  }

  @Override
  public Connection getConnection() throws SQLException {
    return con;
  }

  @Override
  public Connection getConnection(String username, String password) throws SQLException {
    return con;
  }

}
