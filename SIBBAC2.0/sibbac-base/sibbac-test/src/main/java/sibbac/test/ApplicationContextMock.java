package sibbac.test;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.fail;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

public abstract class ApplicationContextMock implements ApplicationContext {

	@Override
	public Environment getEnvironment() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public boolean containsBeanDefinition(String beanName) {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public int getBeanDefinitionCount() {
		fail("Método no implementado en Mock");
		return 0;
	}

	@Override
	public String[] getBeanDefinitionNames() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String[] getBeanNamesForType(Class<?> type) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String[] getBeanNamesForType(Class<?> type, boolean includeNonSingletons, boolean allowEagerInit) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <T> Map<String, T> getBeansOfType(Class<T> type, boolean includeNonSingletons, boolean allowEagerInit)
	    throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> annotationType) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType)
	    throws NoSuchBeanDefinitionException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public Object getBean(String name) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <T> T getBean(Class<T> requiredType) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public Object getBean(String name, Object... args) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public <T> T getBean(Class<T> requiredType, Object... args) throws BeansException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public boolean containsBean(String name) {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public boolean isPrototype(String name) throws NoSuchBeanDefinitionException {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public boolean isTypeMatch(String name, Class<?> targetType) throws NoSuchBeanDefinitionException {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public Class<?> getType(String name) throws NoSuchBeanDefinitionException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String[] getAliases(String name) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public BeanFactory getParentBeanFactory() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public boolean containsLocalBean(String name) {
		fail("Método no implementado en Mock");
		return false;
	}

	@Override
	public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public void publishEvent(ApplicationEvent event) {
		fail("Método no implementado en Mock");
		
	}

	@Override
	public Resource[] getResources(String locationPattern) throws IOException {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public Resource getResource(String location) {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public ClassLoader getClassLoader() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String getId() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String getApplicationName() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public String getDisplayName() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public long getStartupDate() {
		fail("Método no implementado en Mock");
		return 0;
	}

	@Override
	public ApplicationContext getParent() {
		fail("Método no implementado en Mock");
		return null;
	}

	@Override
	public AutowireCapableBeanFactory getAutowireCapableBeanFactory() throws IllegalStateException {
		fail("Método no implementado en Mock");
		return null;
	}

}
