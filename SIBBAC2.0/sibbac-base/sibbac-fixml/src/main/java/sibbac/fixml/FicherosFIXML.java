package sibbac.fixml;


public interface FicherosFIXML {

	// FIXMLEstaticos
	public static final String	CACCOUNTSETTL	= "CACCOUNTSETTL";
	public static final String	CALLOCATION		= "CALLOCATION";
	public static final String	CCASHMOVCLM		= "CCASHMOVCLM";
	public static final String	CENTITIES		= "CENTITIES";
	public static final String	CFAILBUYIN		= "CFAILBUYIN";
	public static final String	CFAILCASHSETTL	= "CFAILCASHSETTL";
	public static final String	CFAILPENALTIES	= "CFAILPENALTIES";
	public static final String	CFAILSADJUST	= "CFAILSADJUST";
	public static final String	CMARGINCALC		= "CMARGINCALC";
	public static final String	CPLEDGES		= "CPLEDGES";
	public static final String	CSECLENDREMUN	= "CSECLENDREMUN";
	public static final String	CSECLENDVALUE	= "CSECLENDVALUE";
	public static final String	CSETTLINSTNEXT	= "CSETTLINSTNEXT";
	public static final String	CCASHMOVTREAS	= "CCASHMOVTREAS";
	public static final String	CMARGINSCLM		= "CMARGINSCLM";
	public static final String	CBACKTESTING	= "CBACKTESTING";
	public static final String	CSTRESSTESTING	= "CSTRESSTESTING";

	// FIXMLDinamicos
	public static final String	CACCOUNTS		= "CACCOUNTS";
	public static final String	CALLTRADES		= "CALLTRADES";
	public static final String	CBALANCEBYDATE	= "CBALANCEBYDATE";
	public static final String	CGIVEIN			= "CGIVEIN";
	public static final String	CGIVEINCLM		= "CGIVEINCLM";
	public static final String	CGIVEINFILT		= "CGIVEINFILT";
	public static final String	CGIVEINFILTCLM	= "CGIVEINFILTCLM";
	public static final String	CGIVEINREF		= "CGIVEINREF";
	public static final String	CGIVEOUT		= "CGIVEOUT";
	public static final String	CGIVEOUTREF		= "CGIVEOUTREF";
	public static final String	CHELDTRADES		= "CHELDTRADES";
	public static final String	CHOLDRELEASE	= "CHOLDRELEASE";
	public static final String	CHISTTRADES		= "CHISTTRADES";
	public static final String	CISINCODES		= "CISINCODES";
	public static final String	CISINDATA		= "CISINDATA";
	public static final String	CPARMODULE		= "CPARMODULE";
	public static final String	CPENDINGINST	= "CPENDINGINST";
	public static final String	CSECLENDCANC	= "CSECLENDCANC";
	public static final String	CSECLENDING		= "CSECLENDING";
	public static final String	CSETTLEDINST	= "CSETTLEDINST";
	public static final String	CSETTLINSTDET	= "CSETTLINSTDET";
	public static final String	CSETTLINSTREL	= "CSETTLINSTREL";
	public static final String	CSTATUS			= "CSTATUS";
	public static final String	CTRADES			= "CTRADES";
	public static final String	CTRANSFTRADES	= "CTRANSFTRADES";
	public static final String	CMONTHLYFEES	= "CMONTHLYFEES";

}
