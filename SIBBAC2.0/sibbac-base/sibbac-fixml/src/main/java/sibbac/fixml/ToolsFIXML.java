package sibbac.fixml;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

// import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.fixml.classes.AbstractMessageT;
import sibbac.fixml.classes.AccountSummaryReportMessageT;
import sibbac.fixml.classes.AllocationReportMessageT;
import sibbac.fixml.classes.BatchT;
import sibbac.fixml.classes.CollateralReportMessageT;
import sibbac.fixml.classes.FIXML;
import sibbac.fixml.classes.MarketDataSnapshotFullRefreshMessageT;
import sibbac.fixml.classes.PartyDetailsListReportMessageT;
import sibbac.fixml.classes.PositionReportMessageT;
import sibbac.fixml.classes.RegistrationInstructionsMessageT;
import sibbac.fixml.classes.SecurityListMessageT;
import sibbac.fixml.classes.TradeCaptureReportMessageT;


public class ToolsFIXML {

	// ------------------------------------------ Bean static properties

	protected static final Logger	LOG	= LoggerFactory.getLogger( ToolsFIXML.class );

	/***
	 * Función que, dado un String por parametro, devuelve otro String coincidente al nombre de ficheros FIXML que se tienen
	 * o null en caso de que mo se encuentre
	 * 
	 * @param name
	 * @return
	 */
	public static String tipoFichero( String name ) {
		// Estaticos
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CACCOUNTSETTL ) ) {
			return FicherosFIXML.CACCOUNTSETTL;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CALLOCATION ) ) {
			return FicherosFIXML.CALLOCATION;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CCASHMOVCLM ) ) {
			return FicherosFIXML.CCASHMOVCLM;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CENTITIES ) ) {
			return FicherosFIXML.CENTITIES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CFAILBUYIN ) ) {
			return FicherosFIXML.CFAILBUYIN;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CFAILCASHSETTL ) ) {
			return FicherosFIXML.CFAILCASHSETTL;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CFAILPENALTIES ) ) {
			return FicherosFIXML.CFAILPENALTIES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CFAILSADJUST ) ) {
			return FicherosFIXML.CFAILSADJUST;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CMARGINCALC ) ) {
			return FicherosFIXML.CMARGINCALC;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CPLEDGES ) ) {
			return FicherosFIXML.CPLEDGES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSECLENDREMUN ) ) {
			return FicherosFIXML.CSECLENDREMUN;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSECLENDVALUE ) ) {
			return FicherosFIXML.CSECLENDVALUE;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSETTLINSTNEXT ) ) {
			return FicherosFIXML.CSETTLINSTNEXT;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CCASHMOVTREAS ) ) {
			return FicherosFIXML.CCASHMOVTREAS;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CMARGINSCLM ) ) {
			return FicherosFIXML.CMARGINSCLM;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CBACKTESTING ) ) {
			return FicherosFIXML.CBACKTESTING;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSTRESSTESTING ) ) {
			return FicherosFIXML.CSTRESSTESTING;
		}
		// Dinamicos
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CACCOUNTS ) ) {
			return FicherosFIXML.CACCOUNTS;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CALLTRADES ) ) {
			return FicherosFIXML.CALLTRADES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CBALANCEBYDATE ) ) {
			return FicherosFIXML.CBALANCEBYDATE;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEIN ) ) {
			return FicherosFIXML.CGIVEIN;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEINFILT ) ) {
			return FicherosFIXML.CGIVEINFILT;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEINFILTCLM ) ) {
			return FicherosFIXML.CGIVEINFILTCLM;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEINREF ) ) {
			return FicherosFIXML.CGIVEINREF;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEINCLM ) ) {
			return FicherosFIXML.CGIVEINCLM;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEOUT ) ) {
			return FicherosFIXML.CGIVEOUT;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CGIVEOUTREF ) ) {
			return FicherosFIXML.CGIVEOUTREF;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CHELDTRADES ) ) {
			return FicherosFIXML.CHELDTRADES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CHOLDRELEASE ) ) {
			return FicherosFIXML.CHOLDRELEASE;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CHISTTRADES ) ) {
			return FicherosFIXML.CHISTTRADES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CISINCODES ) ) {
			return FicherosFIXML.CISINCODES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CISINDATA ) ) {
			return FicherosFIXML.CISINDATA;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CPARMODULE ) ) {
			return FicherosFIXML.CPARMODULE;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CPENDINGINST ) ) {
			return FicherosFIXML.CPENDINGINST;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSECLENDCANC ) ) {
			return FicherosFIXML.CSECLENDCANC;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSECLENDING ) ) {
			return FicherosFIXML.CSECLENDING;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSETTLEDINST ) ) {
			return FicherosFIXML.CSETTLEDINST;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSETTLINSTDET ) ) {
			return FicherosFIXML.CSETTLINSTDET;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSETTLINSTREL ) ) {
			return FicherosFIXML.CSETTLINSTREL;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CSTATUS ) ) {
			return FicherosFIXML.CSTATUS;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CTRADES ) ) {
			return FicherosFIXML.CTRADES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CTRANSFTRADES ) ) {
			return FicherosFIXML.CTRANSFTRADES;
		}
		if ( name.trim().toUpperCase().equals( FicherosFIXML.CMONTHLYFEES ) ) {
			return FicherosFIXML.CMONTHLYFEES;
		}
		return null;
	}

	/***
	 * Función que devuelve un fichero XML del tipo que se indique en el primer parametro
	 * y que se supone está dentro del .zip cuya ruta se pasa como segundo paramaetro
	 * 
	 * @param tipoXML: tipo FIXML que se quiere recuperar
	 * @param rutaZip: ruta en que se encuentra el .zip que contiene al fichero FIXML
	 * @return el Fichero físico con el XML del tipo tipoXML
	 */
	public static File getFIXMLFromZip( String tipoXML, String ruta, String rutaZip ) {
		final int BUFFER = 2048;
		String NL = System.getProperty( "file.separator" );
		String rutaTemp = ruta + NL + "tmp"; // ruta temporal donde dejar el fichero XML
		String xmlTmpFileName = rutaTemp + tipoXML.toUpperCase() + "_tmp.xml";
		BufferedOutputStream dest = null;

		try {
			FileInputStream fis = new FileInputStream( rutaZip ); // Stream de entrada (.zip)
			Boolean xmlFound = false; // Indica si se encontró el nombre del fichero FIXML dentro del .zip
			FileOutputStream fos = null;
			File fileXML = new File( xmlTmpFileName ); // Fichero físico xml donde recoger el mensaje FIXML
			ZipEntry entry;
			ZipInputStream zis = new ZipInputStream( new BufferedInputStream( fis ) ); // Puede generar FileNotFoundException

			// Buscamos en el .zip el .xml que corresponda
			while ( ( entry = zis.getNextEntry() ) != null && !xmlFound ) { // Puede generar IOException
				// Normalmente "entry" viene con el postfijo .C0 / .CM / etc. asi q se lo quitamos
				String fixmlType = entry.getName().substring( 0, entry.getName().lastIndexOf( "." ) ); // Tipo de fichero FIXML
				String nameXML = fixmlType.substring( 0, fixmlType.lastIndexOf( "." ) );
				int count;
				byte data[] = new byte[ BUFFER ];
				if ( nameXML.trim().equals( tipoXML.trim().toUpperCase() ) ) {
					xmlFound = true;
					// Creamos un fichero temporal con el .xml que vamos a tratar y que acabamos de encontrar dentro del .zip
					fileXML = new File( xmlTmpFileName );
					// Comprobamos que el archivo no existe (por posibles tratamientos anteriores del mismo tipo de fichero xml). Si existe,
					// lo borramos y creamos una nuevo
					if ( fileXML.exists() ) {
						fileXML.delete();
						if ( !fileXML.getParentFile().exists() )
							fileXML.getParentFile().mkdir();

						fileXML.createNewFile();
					} else {
						fileXML.getParentFile().mkdir();
						fileXML.createNewFile();
					}
					// Creamos un archivo físico temporal con el .xml encontrado
					fos = new FileOutputStream( xmlTmpFileName );
					dest = new BufferedOutputStream( fos, BUFFER );
					while ( ( count = zis.read( data, 0, BUFFER ) ) != -1 ) {
						dest.write( data, 0, count );
					}
					dest.flush();
					dest.close();
				}
			}
			zis.close();
			return fileXML;

		} catch ( Exception e ) {
			LOG.error( "[ToolsFIXML:getFIXMLFromZip] ERROR({}): {}", e.getClass().getName(), e.getMessage() );
			StackTraceElement[] st = e.getStackTrace();
			if ( st != null && st.length > 0 ) {
				for ( StackTraceElement ste : st ) {
					LOG.error( "[ToolsFIXML:getFIXMLFromZip] - {}", ste );
				}
			}
			return null;
		}

	}

	/***
	 * Función que devuelve una estructura Java completa a partir un mensaje completo FIXML (XML) que se pasa como parametro
	 * 
	 * @param fileXML fichero donde está el XML completo
	 * @return la estructura Java que contiene el XML completo
	 */
	public static FIXML geWholetJavaStructureFromFIXML( File fileXML ) {
		try {
			if ( fileXML != null ) {
				// En este punto tenemos el Fichero FIXML en un File fileXML
				JAXBContext jaxbContext = JAXBContext.newInstance( FIXML.class );
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				// Creamos el objeto FIXML (Java) a partir del objeto fileXML recien creado.
				return ( FIXML ) jaxbUnmarshaller.unmarshal( fileXML );
			} else {
				// No se encontró el nombre del FIXML en el .zip
				return null;
			}
		} catch ( Exception e ) {
			return null;
		}

	}

	/***
	 * Funcion que muestra por la salida estandar la estructura FIXML (de Java) formateada a JSON
	 * 
	 * @param fixml
	 */
	public static void printWholeFIXML( FIXML fixml ) {
		JAXBContext jc;
		Marshaller marshaller;
		try {
			jc = JAXBContext.newInstance( FIXML.class );
			marshaller = jc.createMarshaller();
			marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
			marshaller.setProperty( Marshaller.JAXB_ENCODING, "UTF-8" );
			marshaller.marshal( fixml, System.out );
		} 
		catch (JAXBException e ) {
		    LOG.error("[printWholeFIXML] Error en el parseo", e);
		}
		catch(RuntimeException e) {
		    LOG.error("[printWholeFIXML] Error inesperado", e);
		}
	}


	/***
	 * Funcion que devuelve la lista de submensajes de que consta la estrucutra Java FIXML que se pasa como parametro
	 * 
	 * @param fixml
	 * @return
	 */
	public static List< JAXBElement< ? extends AbstractMessageT >> getSubListFromFIXML( FIXML fixml ) {
		List< JAXBElement< ? extends AbstractMessageT >> list = new ArrayList< JAXBElement< ? extends AbstractMessageT >>();
		if ( fixml != null ) {
			List< BatchT > batchList = fixml.getBatch(); // El FIXML siempre tiene un etiqueta <batch>
			// Una y sólo una etiqueta <batch>, que contiene ya la lista de objetos propios del FIXML
			if ( batchList != null )
				list = batchList.get( 0 ).getMessage();
			if ( list.size() == 0 ) {
				// El Fichero FIXML no tiene mensajes (viene vacio)
				return list;
			}
		} else {
			// La estructura Java del FIXML completo fue NULL
			return list;
		}
		return list;
	}

	/**
	 * Funcion que dada una lista de submensajes AbstractMessageT y un tipo FIXML, devuelve la lista casteada
	 * al tipo de elementos que utiliza el tipo FIXML
	 * 
	 * @param list de elementos AbstractMessageT
	 * @param tipo del mensaje FIXML
	 * @return la lista de elementos Java que maneja el tipo FIXML
	 */
	public static List< AbstractMessageT > getSubListFromFIXML_Typed( List< JAXBElement< ? extends AbstractMessageT >> list, String tipo ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		if ( list == null || list.size() == 0 ) {
			// Si la lista viene nula o vacia, devolvemos una lista vacia
			return new ArrayList< AbstractMessageT >();
		} else {
			switch ( tipo ) {
				case FicherosFIXML.CACCOUNTSETTL: {
					ret = getSubListFromFIXML_CACCOUNTSETTL( list );
					break;
				}
				case FicherosFIXML.CALLOCATION: {
					ret = getSubListFromFIXML_CALLOCATION( list );
					break;
				}
				case FicherosFIXML.CCASHMOVCLM: {
					ret = getSubListFromFIXML_CCASHMOVCLM( list );
					break;
				}
				case FicherosFIXML.CENTITIES: {
					ret = getSubListFromFIXML_CENTITIES( list );
					break;
				}
				case FicherosFIXML.CFAILBUYIN: {
					ret = getSubListFromFIXML_CFAILBUYIN( list );
					break;
				}
				case FicherosFIXML.CFAILCASHSETTL: {
					ret = getSubListFromFIXML_CFAILCASHSETTL( list );
					break;
				}
				case FicherosFIXML.CFAILPENALTIES: {
					ret = getSubListFromFIXML_CFAILPENALTIES( list );
					break;
				}
				case FicherosFIXML.CFAILSADJUST: {
					ret = getSubListFromFIXML_CFAILSADJUST( list );
					break;
				}
				case FicherosFIXML.CMARGINCALC: {
					ret = getSubListFromFIXML_CMARGINCALC( list );
					break;
				}
				case FicherosFIXML.CPLEDGES: {
					ret = getSubListFromFIXML_CPLEDGES( list );
					break;
				}
				case FicherosFIXML.CSECLENDREMUN: {
					ret = getSubListFromFIXML_CSECLENDREMUN( list );
					break;
				}
				case FicherosFIXML.CSECLENDVALUE: {
					ret = getSubListFromFIXML_CSECLENDVALUE( list );
					break;
				}
				case FicherosFIXML.CSETTLINSTNEXT: {
					ret = getSubListFromFIXML_CSETTLINSTNEXT( list );
					break;
				}
				case FicherosFIXML.CCASHMOVTREAS: {
					ret = getSubListFromFIXML_CCASHMOVTREAS( list );
					break;
				}
				case FicherosFIXML.CMARGINSCLM: {
					ret = getSubListFromFIXML_CMARGINSCLM( list );
					break;
				}
				case FicherosFIXML.CBACKTESTING: {
					ret = getSubListFromFIXML_CBACKTESTING( list );
					break;
				}
				case FicherosFIXML.CSTRESSTESTING: {
					ret = getSubListFromFIXML_CSTRESSTESTING( list );
					break;
				}
				case FicherosFIXML.CACCOUNTS: {
					ret = getSubListFromFIXML_CACCOUNTS( list );
					break;
				}
				case FicherosFIXML.CALLTRADES: {
					ret = getSubListFromFIXML_CALLTRADES( list );
					break;
				}
				case FicherosFIXML.CBALANCEBYDATE: {
					ret = getSubListFromFIXML_CBALANCEBYDATE( list );
					break;
				}
				case FicherosFIXML.CGIVEIN: {
					ret = getSubListFromFIXML_CGIVEIN( list );
					break;
				}
				case FicherosFIXML.CGIVEINFILT: {
					ret = getSubListFromFIXML_CGIVEINFILT( list );
					break;
				}
				case FicherosFIXML.CGIVEINFILTCLM: {
					ret = getSubListFromFIXML_CGIVEINFILTCLM( list );
					break;
				}
				case FicherosFIXML.CGIVEINREF: {
					ret = getSubListFromFIXML_CGIVEINREF( list );
					break;
				}
				case FicherosFIXML.CGIVEINCLM: {
					ret = getSubListFromFIXML_CGIVEINCLM( list );
					break;
				}
				case FicherosFIXML.CGIVEOUT: {
					ret = getSubListFromFIXML_CGIVEOUT( list );
					break;
				}
				case FicherosFIXML.CGIVEOUTREF: {
					ret = getSubListFromFIXML_CGIVEOUTREF( list );
					break;
				}
				case FicherosFIXML.CHELDTRADES: {
					ret = getSubListFromFIXML_CHELDTRADES( list );
					break;
				}
				case FicherosFIXML.CHOLDRELEASE: {
					ret = getSubListFromFIXML_CHOLDRELEASE( list );
					break;
				}
				case FicherosFIXML.CHISTTRADES: {
					ret = getSubListFromFIXML_CHISTTRADES( list );
					break;
				}
				case FicherosFIXML.CISINCODES: {
					ret = getSubListFromFIXML_CISINCODES( list );
					break;
				}
				case FicherosFIXML.CISINDATA: {
					ret = getSubListFromFIXML_CISINDATA( list );
					break;
				}
				case FicherosFIXML.CPARMODULE: {
					ret = getSubListFromFIXML_CPARMODULE( list );
					break;
				}
				case FicherosFIXML.CPENDINGINST: {
					ret = getSubListFromFIXML_CPENDINGINST( list );
					break;
				}
				case FicherosFIXML.CSECLENDCANC: {
					ret = getSubListFromFIXML_CSECLENDCANC( list );
					break;
				}
				case FicherosFIXML.CSECLENDING: {
					ret = getSubListFromFIXML_CSECLENDING( list );
					break;
				}
				case FicherosFIXML.CSETTLEDINST: {
					ret = getSubListFromFIXML_CSETTLEDINST( list );
					break;
				}
				case FicherosFIXML.CSETTLINSTDET: {
					ret = getSubListFromFIXML_CSETTLINSTDET( list );
					break;
				}
				case FicherosFIXML.CSETTLINSTREL: {
					ret = getSubListFromFIXML_CSETTLINSTREL( list );
					break;
				}
				case FicherosFIXML.CSTATUS: {
					ret = getSubListFromFIXML_CSTATUS( list );
					break;
				}
				case FicherosFIXML.CTRADES: {
					ret = getSubListFromFIXML_CTRADES( list );
					break;
				}
				case FicherosFIXML.CTRANSFTRADES: {
					ret = getSubListFromFIXML_CTRANSFTRADES( list );
					break;
				}
				case FicherosFIXML.CMONTHLYFEES: {
					ret = getSubListFromFIXML_CMONTHLYFEES( list );
					break;
				}
				default:
					return new ArrayList< AbstractMessageT >();
			}
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CACCOUNTSETTL
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CACCOUNTSETTL( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo CollateralReportMessageT -> para mensajes FIXML_CALLOCATION
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes CollateralReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CALLOCATION( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			CollateralReportMessageT aux = ( CollateralReportMessageT ) elementAbstract.getValue();
			ret.add( ( CollateralReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CCASHMOVCLM
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CCASHMOVCLM( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo PartyDetailsListReportMessageT -> para mensajes FIXML_CENTITIES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes PartyDetailsListReportMessageT
	 */
	public static List< AbstractMessageT > getSubListFromFIXML_CENTITIES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			PartyDetailsListReportMessageT aux = ( PartyDetailsListReportMessageT ) elementAbstract.getValue();
			ret.add( ( PartyDetailsListReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CFAILBUYIN
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CFAILBUYIN( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CFAILCASHSETTL
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CFAILCASHSETTL( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo ?? -> para mensajes FIXML_CFAILPENALTIES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes ??
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CFAILPENALTIES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CFAILSADJUST
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CFAILSADJUST( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo PositionReportMessageT -> para mensajes FIXML_CMARGINCALC
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes PositionReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CMARGINCALC( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			PositionReportMessageT aux = ( PositionReportMessageT ) elementAbstract.getValue();
			ret.add( ( PositionReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo CollateralReportMessageT -> para mensajes FIXML_CPLEDGES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes CollateralReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CPLEDGES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			CollateralReportMessageT aux = ( CollateralReportMessageT ) elementAbstract.getValue();
			ret.add( ( CollateralReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CSECLENDREMUN
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSECLENDREMUN( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CSECLENDVALUE
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSECLENDVALUE( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CSETTLINSTNEXT
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSETTLINSTNEXT( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CCASHMOVTREAS
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CCASHMOVTREAS( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CMARGINSCLM
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CMARGINSCLM( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CBACKTESTING
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CBACKTESTING( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CSTRESSTESTING
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSTRESSTESTING( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo PartyDetailsListReportMessageT -> para mensajes FIXML_CACCOUNTS
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes PartyDetailsListReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CACCOUNTS( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			PartyDetailsListReportMessageT aux = ( PartyDetailsListReportMessageT ) elementAbstract.getValue();
			ret.add( ( PartyDetailsListReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CALLTRADES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CALLTRADES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo PositionReportMessageT -> para mensajes FIXML_CBALANCEBYDATE
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes PositionReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CBALANCEBYDATE( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			PositionReportMessageT aux = ( PositionReportMessageT ) elementAbstract.getValue();
			ret.add( ( PositionReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AllocationReportMessageT -> para mensajes FIXML_CGIVEIN
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AllocationReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEIN( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AllocationReportMessageT aux = ( AllocationReportMessageT ) elementAbstract.getValue();
			ret.add( ( AllocationReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AllocationReportMessageT -> para mensajes FIXML_CGIVEINCLM
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AllocationReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEINCLM( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AllocationReportMessageT aux = ( AllocationReportMessageT ) elementAbstract.getValue();
			ret.add( ( AllocationReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CGIVEINFILT
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEINFILT( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CGIVEINFILTCLM
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEINFILTCLM( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CGIVEINREF
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEINREF( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AllocationReportMessageT -> para mensajes FIXML_CGIVEOUT
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AllocationReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEOUT( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AllocationReportMessageT aux = ( AllocationReportMessageT ) elementAbstract.getValue();
			ret.add( ( AllocationReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CGIVEOUTREF
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CGIVEOUTREF( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CHELDTRADES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CHELDTRADES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AllocationReportMessageT -> para mensajes FIXML_CHOLDRELEASE
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AllocationReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CHOLDRELEASE( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AllocationReportMessageT aux = ( AllocationReportMessageT ) elementAbstract.getValue();
			ret.add( ( AllocationReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CHISTTRADES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CHISTTRADES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo SecurityListMessageT -> para mensajes FIXML_CISINCODES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes SecurityListMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CISINCODES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			SecurityListMessageT aux = ( SecurityListMessageT ) elementAbstract.getValue();
			ret.add( ( SecurityListMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo MarketDataSnapshotFullRefreshMessageT -> para mensajes FIXML_CISINDATA
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes MarketDataSnapshotFullRefreshMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CISINDATA( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			MarketDataSnapshotFullRefreshMessageT aux = ( MarketDataSnapshotFullRefreshMessageT ) elementAbstract.getValue();
			ret.add( ( MarketDataSnapshotFullRefreshMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CPARMODULE
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CPARMODULE( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CPENDINGINST
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CPENDINGINST( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CSECLENDCANC
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSECLENDCANC( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CSECLENDING
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSECLENDING( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CSETTLEDINST
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSETTLEDINST( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CSETTLINSTDET
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSETTLINSTDET( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes CSETTLINSTREL
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSETTLINSTREL( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo RegistrationInstructionsMessageT -> para mensajes FIXML_CSTATUS
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes RegistrationInstructionsMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CSTATUS( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			RegistrationInstructionsMessageT aux = ( RegistrationInstructionsMessageT ) elementAbstract.getValue();
			ret.add( ( RegistrationInstructionsMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo TradeCaptureReportMessageT -> para mensajes FIXML_CTRADES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes TradeCaptureReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CTRADES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			TradeCaptureReportMessageT aux = ( TradeCaptureReportMessageT ) elementAbstract.getValue();
			ret.add( ( TradeCaptureReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AllocationReportMessageT -> para mensajes FIXML_CTRANSFTRADES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AllocationReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CTRANSFTRADES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AllocationReportMessageT aux = ( AllocationReportMessageT ) elementAbstract.getValue();
			ret.add( ( AllocationReportMessageT ) aux );
		}
		return ret;
	}

	/***
	 * Funcion que "castea" desde el tipo AbstractMessageT al tipo AccountSummaryReportMessageT -> para mensajes FIXML_CMONTHLYFEES
	 * 
	 * @param list de mensajes AbstractMessageT
	 * @return list de mensajes AccountSummaryReportMessageT
	 */
	private static List< AbstractMessageT > getSubListFromFIXML_CMONTHLYFEES( List< JAXBElement< ? extends AbstractMessageT >> list ) {
		List< AbstractMessageT > ret = new ArrayList< AbstractMessageT >();
		for ( JAXBElement< ? extends AbstractMessageT > elementAbstract : list ) {
			AccountSummaryReportMessageT aux = ( AccountSummaryReportMessageT ) elementAbstract.getValue();
			ret.add( ( AccountSummaryReportMessageT ) aux );
		}
		return ret;
	}

}
