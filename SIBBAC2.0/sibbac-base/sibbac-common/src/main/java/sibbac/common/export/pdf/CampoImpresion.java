package sibbac.common.export.pdf;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;

import sibbac.common.utils.FormatDataUtils;

/**
 * Arropa un objeto PdfContentByte para eliminar código repetido en la clase PDFBuilder
 * @author Neoris
 *
 */
class CampoImpresion {

    /**
     * Instancia de Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(CampoImpresion.class);

    /**
     * Fuente en la que se realizarán las impresiones en el PDF
     */
    private final BaseFont bf;

    /**
     * Objeto arropado
     */
    private final PdfContentByte under;

    /**
     * Diccionario con textos literales que se imprimirán en el PDF
     */
    private final Map<String, String> literales;

    /**
     * Formato de impresión para BigDecimal
     */
    private final DecimalFormat df;

    /**
     * Crea un arropador para el objeto PdfContentByte pasado como primer argumento
     * @param under Objeto arropado
     * @param bf Fuente ya cargada que se usará para la impresión en el PDF
     * @param literales Diccionario con textos literales.
     */
    CampoImpresion(PdfContentByte under, BaseFont bf, Map<String, String> literales) {
        this.under = under;
        this.bf = bf;
        this.literales = literales;
        df = FormatDataUtils.builDecimalFormat(2);
    }

    /**
     * Imprime un BigDecimal en la posición especificada en el PDF
     * @param cantidad Cantidad a formatear e imprimir
     * @param descripcion Descripción del valor para dejar constancia en los y como valor por defecto.
     * @param x Posición x en el PDF
     * @param y Posición y en el PDF
     * @param size Tamaño utilizado por la fuente.
     */
    void imprimeEuros(BigDecimal cantidad, String descripcion, float x, float y, float size) {
        String textoaImprimir = null;

        if (cantidad != null) {
            textoaImprimir = String.format("%s €", df.format(cantidad.setScale(2, BigDecimal.ROUND_HALF_UP)));
            LOG.debug("[buildPdfFactura] Estableciendo {}: {}", descripcion, textoaImprimir);
        }
        imprime(textoaImprimir, descripcion, x, y, size);
    }

    /**
     * Imprime en el PDF un literal contenido en el diccionario "literales"
     * @param llaveLiteral Llave del literal en el diccionario
     * @param x Posición x en el PDF
     * @param y Posición y en el PDF
     * @param size Tamaño utilizado por la fuente.
     */
    void imprimeLiteral(String llaveLiteral, float x, float y, float size) {
        final String textoaImprimir, descripcion;

        Objects.requireNonNull(llaveLiteral);
        textoaImprimir = literales.get(llaveLiteral);
        descripcion = String.format("Literal con clave '%s'", llaveLiteral);
        imprime(textoaImprimir, descripcion, x, y, size);
    }

    /**
     * Imprime un texto en el PDF a partir del objeto arropado "under"
     * @param texto Texto a imprimir, puede ser nulo
     * @param descripcion Descripción a mostrar en caso de que el parametro texto sea nulo. No puede ser nulo o se
     * lanzará excepción
     * @param x Posición x en el PDF
     * @param y Posición y en el PDF
     * @param size Tamaño utilizado por la fuente.
     */
    void imprime(String texto, String descripcion, float x, float y, float size) {
        Objects.requireNonNull(descripcion);
        under.saveState();
        under.beginText();
        under.moveText(x, y);
        under.setFontAndSize(bf, size);
        if (texto == null) {
            under.showText(descripcion);
            LOG.warn("[CampoImpresion.imprime] Texto nulo para el campo {}", descripcion);
        } else
            under.showText(texto);
        under.endText();
        under.restoreState();
    }

}