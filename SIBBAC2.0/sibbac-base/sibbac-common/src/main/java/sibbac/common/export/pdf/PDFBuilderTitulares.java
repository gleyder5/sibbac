package sibbac.common.export.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import sibbac.common.export.ExportBuilder;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFBuilderTitulares extends ExportBuilder {

	private static final Logger   LOG = LoggerFactory.getLogger(PDFBuilderTitulares.class);
	private static final int      INITIAL_CAPACITY = 4096;
	private static final String	  RESULT_TITULARES_LIST_EXPORT		= "listTitulares";
	private static final Integer  SIZE_HEADERS = 25;
	private ByteArrayOutputStream baos;
	
	private Font normalFont;
	private Font headerFont;
	private Font redHeaderFont;
	private Font blueHeaderFont;
	
	@Autowired
	protected ApplicationContext context;

	public PDFBuilderTitulares() {
		this.baos = new ByteArrayOutputStream(INITIAL_CAPACITY);
		this.normalFont = new Font(FontFamily.HELVETICA, 8);
		this.headerFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE);
		this.redHeaderFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.RED);
		this.blueHeaderFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE);
	}
	
	@Override
	public Object generate(String name, Object target,
			Map<String, ? extends Object> model) throws IOException {
		Document document = (Document) target;
		document.setPageSize(PageSize.LETTER.rotate());
		//document = new Document(PageSize.LETTER.rotate());
		PdfWriter writer = null;
		try {
			writer = this.newWriter(document, this.baos);
			prepareWriter(model, writer);

			// Open the PDF document.
			document.open();

			// Build PDF document.
			if (model != null) {
				LOG.trace("Nombre del pdf [name=={}]", name);

				// Partimos del LinesSet "root".
				// Primero localizamos el "root" para pintarlo en la primera
				// hoja.
				
				List<HashMap<String,Object>> listaTitulares = (List<HashMap<String, Object>>) model.get(RESULT_TITULARES_LIST_EXPORT);
				this.pintaDatos(name, document,  listaTitulares);
			} else {
				document.add(new Paragraph("No data!"));
			}

			// Close the PDF document.			
			document.close();
			writer.flush();
			writer.close();
		} catch (DocumentException e) {
			LOG.error("> ERROR({}): Errors generating the PDF: {}", e.getClass().getName(), e.getMessage());
		}
		return this.getBaos(null);
	}
	
	private void pintaDatos(final String name, Document document, final List<HashMap<String,Object>> listaTitulares)
			throws DocumentException {
		
		LOG.trace("> Nombre PDF [name=={}]", name);
		
		PdfPTable table = null;

		table = new PdfPTable(SIZE_HEADERS);
		PdfPCell cell = null;
		
		//Cabecera
		cell = new PdfPCell(new Phrase("Error", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Booking", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Ref. Cliente", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("CCV", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Sentido", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Isin", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Titulos", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Mercado", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Nombre", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("1erApellido", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("2ºApellido", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("T.Documento", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Documento", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("T.Persona", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("P.Residencia", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("T.Nacionalidad", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Domicilio", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Poblacion", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Provincia", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("C.Postal", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("T.Titular", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("%Participación", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("F.Contratación", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Alias", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("N.Alias", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);

		// Datos
		
		for (HashMap<String, Object> titular : listaTitulares) {
			
			Object obj = titular.get("Error");
			List<HashMap<String,String>> listaErrores = null;
			if (obj instanceof String)
			{
				cell = new PdfPCell(new Phrase(obj.toString(), this.normalFont));
				table.addCell(cell);// 1
			}//if (obj instanceof String)
			else
			{
				cell = new PdfPCell(new Phrase("ERROR", this.normalFont));
				table.addCell(cell);
				listaErrores = (List<HashMap<String,String>>)obj;
			}//else
			
			cell = new PdfPCell(new Phrase((String) titular.get("Booking"), this.normalFont));
			table.addCell(cell);// 2
			cell = new PdfPCell(new Phrase((String) titular.get("Ref_Cliente"), this.normalFont));
			table.addCell(cell);// 3
			cell = new PdfPCell(new Phrase((String) titular.get("CCV"), this.normalFont));
			table.addCell(cell);// 4
			cell = new PdfPCell(new Phrase((String) titular.get("Sentido"), this.normalFont));
			table.addCell(cell);// 5
			cell = new PdfPCell(new Phrase((String) titular.get("Isin"), this.normalFont));
			table.addCell(cell);// 6
			cell = new PdfPCell(new Phrase((String) titular.get("Titulos"), this.normalFont));
			table.addCell(cell);// 7
			cell = new PdfPCell(new Phrase((String) titular.get("Mercado"), this.normalFont));
			table.addCell(cell);// 8
			cell = new PdfPCell(new Phrase((String) titular.get("Nombre"), this.normalFont));
			table.addCell(cell);// 9
			cell = new PdfPCell(new Phrase((String) titular.get("PrimerApellido"), this.normalFont));
			table.addCell(cell);//10
			cell = new PdfPCell(new Phrase((String) titular.get("SegundorApellido"), this.normalFont));
			table.addCell(cell);//11
			cell = new PdfPCell(new Phrase((String) titular.get("T_Documento"), this.normalFont));
			table.addCell(cell);//12
			cell = new PdfPCell(new Phrase((String) titular.get("Documento"), this.normalFont));
			table.addCell(cell);//13
			cell = new PdfPCell(new Phrase((String) titular.get("T_Persona"), this.normalFont));
			table.addCell(cell);//14
			cell = new PdfPCell(new Phrase((String) titular.get("P_Residencia"), this.normalFont));
			table.addCell(cell);//15
			cell = new PdfPCell(new Phrase((String) titular.get("Nacionalidad"), this.normalFont));
			table.addCell(cell);//16
			cell = new PdfPCell(new Phrase((String) titular.get("Domicilio"), this.normalFont));
			table.addCell(cell);//17
			cell = new PdfPCell(new Phrase((String) titular.get("Poblacion"), this.normalFont));
			table.addCell(cell);//18
			cell = new PdfPCell(new Phrase((String) titular.get("Provincia"), this.normalFont));
			table.addCell(cell);//19
			cell = new PdfPCell(new Phrase((String) titular.get("C_Postal"), this.normalFont));
			table.addCell(cell);//20
			cell = new PdfPCell(new Phrase((String) titular.get("T_Titular"), this.normalFont));
			table.addCell(cell);//21
			cell = new PdfPCell(new Phrase((String) titular.get("Participacion_Por"), this.normalFont));
			table.addCell(cell);//22
			cell = new PdfPCell(new Phrase((String) titular.get("F_Contratacion"), this.normalFont));
			table.addCell(cell);//23
			cell = new PdfPCell(new Phrase((String) titular.get("Alias"), this.normalFont));
			table.addCell(cell);//24
			cell = new PdfPCell(new Phrase((String) titular.get("N_Alias"), this.normalFont));
			table.addCell(cell);//25
			if (listaErrores != null)
			{
				//hay errores se va a generar una fila por cada error.
				//Cabeceras
				cell = new PdfPCell(new Phrase("", this.normalFont));
				table.addCell(cell);//1
				cell = new PdfPCell(new Phrase("Código", this.redHeaderFont));
				table.addCell(cell);//2
				cell = new PdfPCell(new Phrase("Descripción", this.redHeaderFont));
				cell.setColspan(23);
				table.addCell(cell);//23
				//datos 
				for (HashMap<String, String> error : listaErrores)
				{					
					cell = new PdfPCell(new Phrase("", this.normalFont));
					table.addCell(cell);//1
					cell = new PdfPCell(new Phrase(error.get("codigo"), this.blueHeaderFont));
					table.addCell(cell);//2
					cell = new PdfPCell(new Phrase(error.get("descripcion"), this.blueHeaderFont));
					cell.setColspan(23);
					table.addCell(cell);//23
				}//for (HashMap<String, String> error : listaErrores)
			}//if (listaErrores != null)
		}//for (HashMap<String, Object> titular : listaTitulares) {

		document.add(table);
	}//private void pintaDatos(final String name, Document document, final List<HashMap<String,Object>> listaTitulares)
	
	public void prepareWriter(Map<String, ? extends Object> model, PdfWriter writer) throws DocumentException {
		writer.setViewerPreferences(this.getViewerPreferences(model));
	}
	
	public int getViewerPreferences(Map<String, ? extends Object> model) {
		return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
	}

	private PdfWriter newWriter(Document document, ByteArrayOutputStream baos) throws DocumentException {
		return PdfWriter.getInstance(document, baos);
	}

	@Override
	public Object generate(String name, Object target,
			Map<String, ? extends Object> model, List<String> dataSets)
			throws IOException {
		return this.generate(name, target, model);
	}
	
	@Override
	public ByteArrayOutputStream getBaos(final Object target) {
		return this.baos;
	}

}//public class PDFBuilderTitulares extends ExportBuilder {
