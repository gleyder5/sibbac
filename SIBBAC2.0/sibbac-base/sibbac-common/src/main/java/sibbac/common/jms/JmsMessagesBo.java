package sibbac.common.jms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import sibbac.common.SIBBACBusinessException;

import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.jms.MQDestination;
import com.ibm.mq.jms.MQQueue;
import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsConstants;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;

@Service(value = "JmsMessagesBo")
public class JmsMessagesBo {

  private static final Logger LOG = LoggerFactory.getLogger(JmsMessagesBo.class);

  public JmsMessagesBo() {
  }

  public void setBasicJmsMQMDProperties(Message jmsMessage) throws JMSException {
    this.setBasicJmsMQMDProperties(jmsMessage, 819);
  }

  public void setBasicJmsMQMDProperties(Message jmsMessage, int charset) throws JMSException {
    jmsMessage.setIntProperty(WMQConstants.JMS_IBM_MQMD_MSGTYPE, MQConstants.MQMT_DATAGRAM);
    jmsMessage.setIntProperty(WMQConstants.JMS_IBM_MQMD_CODEDCHARSETID, charset);
    jmsMessage.setStringProperty(WMQConstants.JMS_IBM_MQMD_FORMAT, MQConstants.MQFMT_STRING);
  }

  public InputStream getInputStream(Message message) throws SIBBACBusinessException, IOException, JMSException {
    InputStream is = null;
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    byte[] buffer = new byte[2 * 1024];
    int len;
    if (message instanceof TextMessage) {
      baos.write(((TextMessage) message).getText().getBytes());
      is = new ByteArrayInputStream(baos.toByteArray());
      baos.close();
    }
    else if (message instanceof StreamMessage) {
      while ((len = ((StreamMessage) message).readBytes(buffer)) > 0) {
        baos.write(buffer, 0, len);
      }
      is = new ByteArrayInputStream(baos.toByteArray());
      baos.close();
    }
    else if (message instanceof BytesMessage) {
      while ((len = ((BytesMessage) message).readBytes(buffer)) > 0) {
        baos.write(buffer, 0, len);
      }
      is = new ByteArrayInputStream(baos.toByteArray());
      baos.close();
    }
    else if (message instanceof ObjectMessage) {
      throw new SIBBACBusinessException("ObjectMessage no es un mensaje valido.");
    }
    else {
      throw new SIBBACBusinessException(message.getClass() + " no es un mensaje valido.");
    }
    return is;
  }

  public String getText(Message message) throws SIBBACBusinessException, IOException, JMSException {
    String txt = null;
    if (message instanceof TextMessage) {
      txt = ((TextMessage) message).getText();
    }
    else if (message instanceof StreamMessage) {
      throw new SIBBACBusinessException("ObjectMessage no es un mensaje valido.");
    }
    else if (message instanceof BytesMessage) {
      throw new SIBBACBusinessException("ObjectMessage no es un mensaje valido.");
    }
    else if (message instanceof ObjectMessage) {
      throw new SIBBACBusinessException("ObjectMessage no es un mensaje valido.");
    }
    else {
      throw new SIBBACBusinessException(message.getClass() + " no es un mensaje valido.");
    }
    return txt;
  }

  public String getGroupId(Message message) throws JMSException {
    try {
      return message.getStringProperty(JmsConstants.JMSX_GROUPID);
    }
    catch (JMSException e) {
      return null;
    }
  }

  public Integer getGroupSeq(Message message) throws JMSException {
    try {
      return message.getIntProperty(JmsConstants.JMSX_GROUPSEQ);
    }
    catch (JMSException | NumberFormatException e) {
      return null;
    }
  }

  public boolean getIsLast(Message message) throws JMSException {
    try {
      return message.getBooleanProperty(JmsConstants.JMS_IBM_LAST_MSG_IN_GROUP);
    }
    catch (JMSException e) {
      return false;
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public ConnectionFactory getNewJmsConnectionFactory(String host, int port, String channel, String manager,
      String user, String pass) throws JMSException {
    LOG.debug("[getNewJmsConnectionFactory] HOST: '{}':({}) MANAGER:'{}' CHANNEL: '{}'", host, port, manager, channel);

    ConnectionFactory cf;
    JmsConnectionFactory jcf = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER).createConnectionFactory();
    jcf.setStringProperty(WMQConstants.WMQ_HOST_NAME, StringUtils.trim(host));
    jcf.setIntProperty(WMQConstants.WMQ_PORT, port);
    jcf.setStringProperty(WMQConstants.WMQ_CHANNEL, StringUtils.trim(channel));
    jcf.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
    jcf.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, StringUtils.trim(manager));
    jcf.setStringProperty(WMQConstants.USERID, StringUtils.trim(user));
    jcf.setStringProperty(WMQConstants.PASSWORD, pass);
    jcf.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, true);
    jcf.setIntProperty(WMQConstants.ACKNOWLEDGE_MODE, WMQConstants.CLIENT_ACKNOWLEDGE);
    jcf.setIntProperty(WMQConstants.WMQ_CLIENT_RECONNECT_TIMEOUT, WMQConstants.WMQ_CLIENT_RECONNECT_TIMEOUT_DEFAULT);
    jcf.setIntProperty(WMQConstants.WMQ_CLEANUP_LEVEL, WMQConstants.WMQ_CLEANUP_DEFAULT);
    jcf.setIntProperty(WMQConstants.WMQ_CLEANUP_INTERVAL, 18000);
    cf = jcf;
    LOG.trace("[getNewJmsConnectionFactory] CF: {}", cf);
    return cf;
  }

  public ConnectionFactory getConnectionFactoryFromContext(String jndiName) throws NamingException {
    LOG.trace("[getConnectionFactoryFromContext] jndi: {} ", jndiName);
    InitialContext ic = new InitialContext();

    ConnectionFactory jcf;
    try {
      jcf = (ConnectionFactory) ic.lookup(String.format("java:comp/env/jms/%s", jndiName));
    }
    catch (NamingException e) {
      LOG.warn("[getConnectionFactoryFromContext] jndi: {} --- NO ENCONTRADO ---", jndiName);
      throw e;
    }
    LOG.trace("[getConnectionFactoryFromContext] jndi: {} CF: {}", jndiName, jcf);
    return jcf;
    // CachingConnectionFactory ccf = new CachingConnectionFactory(jcf);
    // ccf.setSessionCacheSize(180);
    // ccf.setReconnectOnException(true);
    // ccf.setClientId("SIBBAC20");
    // return jcf;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public JmsTemplate getNewJmsTemplate(ConnectionFactory cf, boolean sesionTransacted) {
    JmsTemplate jmsTemplate = new JmsTemplate(cf);
    jmsTemplate.setSessionTransacted(sesionTransacted);
    if (sesionTransacted) {
      jmsTemplate.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
    }

    return jmsTemplate;
  }

  public Connection getNewConnection(ConnectionFactory cf) throws JMSException {
    Connection c = cf.createConnection();
    // c.start();
    return c;
  }

  public Session getNewSession(Connection c, boolean sessionTransacted) throws JMSException {
    if (sessionTransacted) {
      return c.createSession(sessionTransacted, Session.CLIENT_ACKNOWLEDGE);
    }
    else {
      return c.createSession(sessionTransacted, Session.AUTO_ACKNOWLEDGE);
    }
  }

  private Queue getNewQueue(Session session, String destination) throws JMSException {
    return session.createQueue(destination);

  }

  public MessageConsumer getNewConsumer(Session session, Destination destination, String messageSelector)
      throws JMSException {
    return session.createConsumer(destination, messageSelector);
  }

  public MessageConsumer getNewConsumer(Session session, Destination destination) throws JMSException {
    return this.getNewConsumer(session, destination, null);
  }

  public MessageConsumer getNewConsumer(Session session, String destination) throws JMSException {
    return this.getNewConsumer(session, destination, null);
  }

  public MessageConsumer getNewConsumer(Session session, String destination, String messageSelector)
      throws JMSException {
    Destination queue = this.getNewQueue(session, destination);
    return this.getNewConsumer(session, queue, messageSelector);
  }

  public MessageProducer getNewProducer(Session session, Destination destination) throws JMSException {
    return session.createProducer(destination);
  }

  public MessageProducer getNewProducer(Session session, String destination) throws JMSException {
    Destination queue = this.getNewQueue(session, destination);
    return this.getNewProducer(session, queue);
  }

  public MessageProducer getNewMqMessageProduccer(Session session, String destinationSt) throws JMSException {
    MQQueue destination = (MQQueue) this.getNewQueue(session, destinationSt);
    // Activamos la posibilidad de cambiar la cabecera del msg MQMD
    ((MQDestination) destination).setMQMDWriteEnabled(true);
    // Le decimos al que el gestor destino no es jms si no mq para que no
    // ponga cabeceras jms
    ((MQDestination) destination).setMessageBodyStyle(WMQConstants.WMQ_MESSAGE_BODY_MQ);
    ((MQDestination) destination).setTargetClient(WMQConstants.WMQ_TARGET_DEST_MQ);
    // le decimos que nosotros manualmente vamos a poner el campos
    // JMS_IBM_MQMD_ApplIdentityData
    ((MQDestination) destination).setMQMDMessageContext(WMQConstants.WMQ_MDCTX_SET_IDENTITY_CONTEXT);
    return session.createProducer(destination);
  }

  public MessageProducer getNewPtiMessageProducer(Session session, String destinationSt) throws JMSException {
    return getNewMqMessageProduccer(session, destinationSt);

  }

  public Message createNewTextMessage(Session session, String messageSt) throws JMSException {
    TextMessage message = session.createTextMessage();
    message.setText(messageSt);
    return message;
  }

  @Transactional
  public Message createMessage(final Session session, final JmsMessageType type, final InputStream is)
      throws JMSException {

    Message message = null;
    try {

      if (type == JmsMessageType.TEXT) {

        message = session.createTextMessage(IOUtils.toString(is));

      }
      else if (type == JmsMessageType.BYTES) {
        message = session.createBytesMessage();
        ((BytesMessage) message).writeBytes(IOUtils.toByteArray(is));
      }
      else if (type == JmsMessageType.STREAM) {
        message = session.createStreamMessage();
        ((StreamMessage) message).writeBytes(IOUtils.toByteArray(is));
      }
      else {
        throw new JMSException("JmsMessageType no soportado == " + type);
      }
    }
    catch (IOException e) {
      LOG.warn("Incidencia leyendo is", e);
      throw new JMSException("Incidencia leyendo is " + e.getMessage());
    }
    if (message == null) {
      throw new JMSException("Incidencia no se ha creado el JmsMessage.");
    }
    LOG.trace("[FilesystemToPtiMqBo :: sendPtiJmsTextMessage] JMSMessage: {}.", message);

    return message;

  }

  public void sendJmsMessage(MessageProducer messageProducer, Message message) throws JMSException {
    messageProducer.send(message);
  }

  @SuppressWarnings("unchecked")
  public Integer countQueueMessages(Session session, String destination) throws JMSException {
    Destination queue = this.getNewQueue(session, destination);
    Integer val = 0;
    try (QueueBrowser queueBrowser = session.createBrowser((Queue) queue, null);) {
      val = Collections.list(queueBrowser.getEnumeration()).size();
    }
    return val;
  }

  public Message receiveMessage(MessageConsumer messageConsumer, long timeout) throws JMSException {
    if (timeout < 0) {
      return messageConsumer.receiveNoWait();
    }
    else {
      return messageConsumer.receive(timeout);
    }
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED)
  public JmsTemplate getNewJmsTemplate(String host, int port, String channel, String manager, String user, String pass,
      boolean sesionTransacted) throws JMSException {
    return this.getNewJmsTemplate(getNewJmsConnectionFactory(host, port, channel, manager, user, pass),
        sesionTransacted);
  }

  public void closeAll(MessageConsumer consumer, MessageProducer producer, Session session, Connection connection)
      throws JMSException {
    JMSException ex = null;
    if (consumer != null) {
      LOG.trace("[closeAll] cerrando consumer...");
      try {
        consumer.close();
      }
      catch (JMSException e) {
        LOG.warn(e.getMessage(), e);
        ex = e;
      }
    }
    if (producer != null) {
      LOG.trace("[closeAll] cerrando produccer...");
      try {
        producer.close();
      }
      catch (JMSException e) {
        LOG.warn(e.getMessage(), e);
        ex = e;
      }
    }

    if (session != null) {
      try {
        if (session.getTransacted()) {
          LOG.trace("[closeAll] rollback transaction...");
          session.rollback();
        }
      }
      catch (JMSException e) {
        LOG.warn(e.getMessage(), e);
      }
      try {
        session.close();
      }
      catch (JMSException e) {
        LOG.warn(e.getMessage(), e);
        ex = e;
      }
    }
    if (connection != null) {
      LOG.trace("[closeAll] close connection...");
      try {
        connection.close();
      }
      catch (JMSException e) {
        LOG.warn(e.getMessage(), e);
        ex = e;
      }
    }
    if (ex != null) {
      throw ex;
    }
  }

}
