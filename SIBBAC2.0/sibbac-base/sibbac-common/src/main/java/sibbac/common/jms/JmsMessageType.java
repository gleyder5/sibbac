package sibbac.common.jms;

public enum JmsMessageType {
  TEXT, BYTES, STREAM, OBJECT;
}
