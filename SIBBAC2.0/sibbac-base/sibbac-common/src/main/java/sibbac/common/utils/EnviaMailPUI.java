package sibbac.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EnviaMailPUI {
    protected static final Logger LOG = LoggerFactory.getLogger(EnviaMailPUI.class);

    @Value("${aviso.pui.body}") private String puiBody;
    @Value("${aviso.pui.subject}") private String subject;
	@Value("${aviso.pui.emails}") private String puiEmails;
	@Autowired private SendMail	sendMail;

	public void send(List<InputStream> path2attach, List<String> nameDest)
			throws IllegalArgumentException, MessagingException, IOException {
		sendMail.sendMail(Arrays.asList(puiEmails.split(";")), subject, puiBody, path2attach, nameDest, null);
		LOG.debug("Se envía el correo");
	}
}
