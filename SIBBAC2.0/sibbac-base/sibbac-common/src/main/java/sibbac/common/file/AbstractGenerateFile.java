package sibbac.common.file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.file.constants.FileResult;

public abstract class AbstractGenerateFile {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractGenerateFile.class);

  /**
   * Tamaño de la paginación
   */
  private int dbPageSize;

  /**
   * Ruta física en disco donde se depositarán los ficheros
   */
  private String outPutDir;

  /**
   * Nombre del fichero
   */
  private String fileName;

  /**
   * Parámetro para activar generacion de fichero sin datos
   */
  private Boolean generateIfEmpty = false;

  /**
   * Resultado de la generación del fichero
   */
  private FileResult fileResult;

  /**
   * Método abstracto a implementar por las claes hijas que define los saltos de
   * linea del fichero a generar
   * @param outWriter
   * @throws IOException
   */
  public abstract String getLineSeparator();

  /**
   * Método abstracto a implementar por las clase hijas para la obtención
   * específica de datos para cualquier tipo de fichero
   * @return
   */
  public abstract List<Object[]> getFileData(Pageable pageable);

  /**
   * Método abstracto a implementar por las clase hijas para el conteo de datos
   * @return
   */
  public abstract Integer getFileCountData();

  /**
   * Método abstracto a implementar por las claes hijas que define la cabecera
   * del fichero a generar
   * @param outWriter
   * @throws IOException
   */
  public abstract void writeHeader(BufferedWriter outWriter) throws IOException;

  /**
   * Método abstracto a implementar por las claes hijas que define el pie de
   * página del fichero a generar
   * @param outWriter
   * @throws IOException
   */
  public abstract void writeFooter(BufferedWriter outWriter) throws IOException;

  /**
   * Método abstracto a implementar por las clases hijas que define el registro
   * del fichero a generar
   * @param outWriter fileData
   * @throws IOException
   */
  public abstract void processRegistrer(BufferedWriter outWriter, List<Object[]> fileData) throws IOException;

  /**
   * Método que rella o escribe en disco el contenido del fichero
   * @param outWriter
   * @param fileData
   * @throws IOException
   */
  public void writeContent(BufferedWriter outWriter) throws IOException {
    recursePageAndWriteContent(outWriter, new PageRequest(0, dbPageSize));
  }

  /**
   * Genera el fichero con el nombre definido por parámetros. Este método se
   * conecta a la base de datos, obtiene los registros, pagina, y genera el
   * fichero
   * @param fileName
   * @throws SIBBACBusinessException
   */
  public void generateFile() throws SIBBACBusinessException {
    // Eleva una exception si los campos obligatorios no están informados.
    validateFields();

    createOutPutDir();

    if (getFileCountData() > 0) {

      BufferedWriter outWriter = null;
      FileWriter fileWriter = null;

      try {
        fileWriter = new FileWriter(getFullFileAccesPath());

        outWriter = new BufferedWriter(fileWriter);

        if (LOG.isDebugEnabled()) {
          LOG.debug("Escritura de la cabecera del fichero " + fileName);
        }
        writeHeader(outWriter);

        if (LOG.isDebugEnabled()) {
          LOG.debug("Escritura del contenido del fichero " + fileName);
        }
        writeContent(outWriter);

        if (LOG.isDebugEnabled()) {
          LOG.debug("Escritura del pie del fichero " + fileName);
        }
        writeFooter(outWriter);
        fileResult = FileResult.GENETARATED_OK;
        LOG.info("Fichero generado correctamente en la ruta " + getFullFileAccesPath());
      }
      catch (IOException e) {
        fileResult = FileResult.GENETARATED_KO;
        throw new SIBBACBusinessException(e);
      }
      finally {
        if (outWriter != null) {
          try {
            outWriter.close();
          }
          catch (IOException e) {
            LOG.error("No se puede cerrar el buffer de escritura", e);
          }
        }

        if (fileWriter != null) {
          try {
            fileWriter.close();
          }
          catch (IOException e) {
            LOG.error("No se puede cerrar el writer de escritura", e);
          }
        }
      }
    }
    else {
      if (generateIfEmpty == true) {
        BufferedWriter outWriter = null;
        FileWriter fileWriter = null;

        try {
          fileWriter = new FileWriter(getFullFileAccesPath());
          outWriter = new BufferedWriter(fileWriter);
          fileResult = FileResult.GENETARATED_EMPTY;
          LOG.info("Fichero vacío generado correctamente en la ruta " + getFullFileAccesPath());
        }
        catch (IOException e) {
          throw new SIBBACBusinessException(e);
        }
        finally {
          if (outWriter != null) {
            try {
              outWriter.close();
            }
            catch (IOException e) {
              LOG.error("No se puede cerrar el buffer de escritura", e);
            }
          }

          if (fileWriter != null) {
            try {
              fileWriter.close();
            }
            catch (IOException e) {
              LOG.error("No se puede cerrar el writer de escritura", e);
            }
          }
        }
      }
      else {
        fileResult = FileResult.NOT_GENERATED;
        if (LOG.isInfoEnabled()) {
          LOG.info("El fichero " + fileName + " no tiene datos y no se ha generado");
        }
      }

    }
  }

  /**
   * Genera el fichero con el nombre definido por parámetros. Este método se
   * conecta a la base de datos, obtiene los registros, pagina, y genera el
   * fichero
   * @param fileName
   * @throws SIBBACBusinessException
   */
  public void generateFile(String outPutDir, String fileName) throws SIBBACBusinessException {
    this.outPutDir = outPutDir;
    this.fileName = fileName;

    generateFile();
  }

  /**
   * Valida los campos obligatorios para el proceso
   */
  private void validateFields() throws SIBBACBusinessException {
    // Comprobaciones nombre de fichero obligatorio
    if (StringUtils.isEmpty(fileName)) {
      throw new SIBBACBusinessException("El nombre del fichero es obligatorio informarlo");
    }

    // Comprobaciones ruta de fichero obligatorio
    if (StringUtils.isEmpty(this.outPutDir)) {
      throw new SIBBACBusinessException("La ruta de del fichero es obligatorio informarlo");
    }
  }

  /**
   * Crea la estructura de directorios necesaria para la generación del fichero
   * @param outPutDirFile
   */
  private void createOutPutDir() {
    final File outDir = new File(this.outPutDir);
    if (outDir.mkdirs()) {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Se ha creado la estructura de directorios " + this.outPutDir);
      }
    }
    else {
      if (LOG.isDebugEnabled()) {
        LOG.debug("Ya existe la estructura de directorios " + this.outPutDir);
      }
    }
  }

  /**
   * Itera sobre todas las paginas y forma el fichero, de forma paginada
   * @param pageable
   */
  private void recursePageAndWriteContent(BufferedWriter outWriter, Pageable pageable) throws IOException {
    final List<Object[]> fileData = getFileData(pageable);

    if (fileData != null && CollectionUtils.isNotEmpty(fileData)) {
      // La clase hija genera los registros según su formato
      processRegistrer(outWriter, fileData);

      // Recurrimos de nuevo al método, e incrementa el numero de la página,
      // actualizando el offset page * pageSize
      if (LOG.isDebugEnabled()) {
        LOG.debug("Fin escritura pagina " + pageable.getPageNumber());
      }
      recursePageAndWriteContent(outWriter, pageable.next());
    }

  }

  /**
   * Escribe una línea en el fichero
   * @param outWriter
   * @param line
   * @throws IOException
   */
  protected void writeLine(BufferedWriter outWriter, String line) throws IOException {
    outWriter.write(line);
    outWriter.write(getLineSeparator());
  }

  /**
   * Método auxiliar que construye correctamente la ruta de acceso a disco del
   * fichero a generar
   * @return
   */
  private String getFullFileAccesPath() {
    if (outPutDir.endsWith(File.separator)) {
      return outPutDir + this.fileName;
    }
    else {
      return outPutDir + File.separator + this.fileName;
    }
  }

  /**
   * Getter de la propiedad
   * @return
   */
  public String getOutPutDir() {
    return outPutDir;
  }

  /**
   * Setter de la propiedad
   * @param fileName
   */
  public void setOutPutDir(String outPutDir) {
    this.outPutDir = outPutDir;
  }

  /**
   * Getter de la propiedad
   * @return
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * Setter de la propiedad
   * @param fileName
   */
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  /**
   * Getter de la propiedad
   * @return
   */
  public int getDbPageSize() {
    return dbPageSize;
  }

  public FileResult getFileGenerationResult() {
    return fileResult;
  }

  public Boolean getGenerateIfEmpty() {
    return generateIfEmpty;
  }

  public void setGenerateIfEmpty(Boolean generateIfEmpty) {
    this.generateIfEmpty = generateIfEmpty;
  }

  /**
   * Setter de la propiedad
   * @param fileName
   */
  public void setDbPageSize(int dbPageSize) {
    this.dbPageSize = dbPageSize;
    LOG.info("Paginación de Fichero establecida a: " + this.dbPageSize);
  }

}
