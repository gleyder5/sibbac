package sibbac.common.economic;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class RepartoDatosEconomicosBigDecimalComparator implements Comparator<Object> {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(RepartoDatosEconomicosBigDecimalComparator.class);

  private Method metodoComparacion;

  private boolean asc;

  RepartoDatosEconomicosBigDecimalComparator(Method metodoComparacion, boolean asc) {
    this.metodoComparacion = metodoComparacion;
    this.asc = asc;
  }

  @Override
  public int compare(Object arg0, Object arg1) {
    int res = 0;
    try {
      res = ((BigDecimal) metodoComparacion.invoke(arg0)).compareTo(((BigDecimal) metodoComparacion.invoke(arg1)));
      if (!asc) {
        res *= -1;
      }
    }
    catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      LOG.warn(e.getMessage());
      LOG.trace(e.getMessage(), e);
    }

    return res;
  }
}
