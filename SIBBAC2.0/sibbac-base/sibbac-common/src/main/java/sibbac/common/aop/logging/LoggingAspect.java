package sibbac.common.aop.logging;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.CallableResultDTO;
import sibbac.common.utils.SibbacEnums;

/**
 * 
 * @author ipalacio
 *
 */
@Aspect
public class LoggingAspect {

  @Pointcut("@annotation(sibbac.common.aop.logging.SIBBACLogging)")
  public void loggableMethods() {
    
  }
  
  /**
   * 
   * @param joinPoint
   * @return
   * @throws Throwable
   */
  @Around("loggableMethods()")
  public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
    final Logger log = LoggerFactory.getLogger(joinPoint.getTarget().getClass());

    // Trazamos el mensaje al entrar en el método
    if (log.isDebugEnabled()) {
      log.debug(SibbacEnums.SIBBACLoggingMessages.LOGGING_ENTER_MESSAGE.value(),
          joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(),
          Arrays.toString(joinPoint.getArgs()));
    }
    try {
      final Object result = joinPoint.proceed();

      // Trazamos los argumentos al salir del método
      if (log.isDebugEnabled()) {
        log.debug(SibbacEnums.SIBBACLoggingMessages.LOGGING_EXIT_MESSAGE.value(),
            joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), result);
      }
      return result;
    }
    catch (Throwable throwable) {
      log.error(SibbacEnums.SIBBACLoggingMessages.LOGGING_ERROR_MESSAGE.value(), Arrays.toString(joinPoint.getArgs()),
          joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(), throwable);
   
      // Capturamos cualquier error que se produzca durante la invocacion
      final Class<?> returnType = ((MethodSignature) joinPoint.getSignature()).getReturnType();
    
      // Si el metodo devuelve un CallableResultDTO es que estamos dentro de servicio Rest y el error debemos presentarlo
      // al usuario
      if (CallableResultDTO.class.isAssignableFrom(returnType)) {
        final CallableResultDTO result = new CallableResultDTO();
        result.addErrorMessage(SibbacEnums.SIBBACLoggingMessages.ERROR_MESSAGE.value());
        return result;
      }
      else {
        throw throwable;
      }
    }
  }
}