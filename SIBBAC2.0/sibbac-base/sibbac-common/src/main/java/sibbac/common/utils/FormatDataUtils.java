package sibbac.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.SIBBACBusinessException;

/**
 * Constantes y método de uso común en SIBBAC para el tratamientos de fechas.
 * 
 * @author XI316153
 *
 */
public class FormatDataUtils {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(FormatDataUtils.class);

  public final static String DATE_FORMAT_FICHERO_EURO_CCP = "MMdd";
  public final static String DATE_FORMAT = "yyyyMMdd";
  public final static String DATE_FORMAT_1 = "yyyy-MM-dd";
  public final static String TIME_FORMAT = "HHmmss";
  public final static String TIME_FORMAT_1 = "HH:mm:ss";
  public final static String DATE_FORMAT_REF_TIT_OP_ESPECIAL = "yyyyMMddHHmm";
  public static final String TIMESTAMP_FORMAT = "yyyyMMddHHmmss";
  public final static String DATETIME_FORMAT = DATE_FORMAT + " " + TIME_FORMAT;
  public final static String FILE_DATETIME_FORMAT = DATE_FORMAT + "_" + TIME_FORMAT;
  public final static String CONCURRENT_FILE_DATETIME_FORMAT = DATE_FORMAT + "_" + TIME_FORMAT + "_SSS";
  public final static String HUMAN = "yyyy-MM-dd HH:mm:ss";
  public final static String DATE_FORMAT_SEPARATOR = "yyyy/MM/dd";
  public final static String DATE_ES_FORMAT_SEPARATOR = "dd/MM/yyyy";
  public static final String DATE_FORMAT_DASH_SEPARATOR = "yyyy-MM-dd";
  public static final String ES_DATE_FORMAT = "ddMMyyyy";
  
  /** Formato decimal a utilizar en todo el proyecto: "#,##0.0#" */
  public static final String FORMATO_DECIMAL = "#,##0.0#";
  
  /** El separador de miles es el punto, sin importar localización */
  public static final char SEPARADOR_MILES = '.';
  
  /** El separador de decimales es la coma, sin importar localización */
  public static final char SEPARADOR_DECIMAL = ',';
  
  public static final DecimalFormatSymbols DECIMAL_SYMBOLS;
  
  static {
      DECIMAL_SYMBOLS = new DecimalFormatSymbols();
      DECIMAL_SYMBOLS.setDecimalSeparator(SEPARADOR_DECIMAL);
      DECIMAL_SYMBOLS.setGroupingSeparator(SEPARADOR_MILES);
  }

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS ESTÁTICOS
  
  /**
   * Crea una instancia de DecimalFormat con el formato y símbolos comunes al proyecto
   * @return DecimalFormat ya inicializado 
   */
  public static DecimalFormat buildDecimalFormat() {
      final DecimalFormat dc;
      
      dc = new DecimalFormat(FORMATO_DECIMAL);
      dc.setDecimalFormatSymbols(DECIMAL_SYMBOLS);
      return dc;
  }
  
  /**
   * Crea una instancia de DecimalFormat con el formato y símbolos comunes al proyecto, limitando a un número de
   * decimales
   * @param scale Límite de decimales
   * @return DecimalFormat ya inicializado 
   */
  public static DecimalFormat builDecimalFormat(int scale) {
      final DecimalFormat dc;
      
      dc = buildDecimalFormat();
      dc.setMaximumFractionDigits(scale);
      dc.setMinimumFractionDigits(scale);
      return dc;
  }

  public static String toHuman(final Date fecha) {
    return convertDateToString(fecha, HUMAN);
  }

  /**
   * @param fecha
   * @param format
   * @return
   */
  public static String convertDateToString(final Date fecha, final String format) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }

  public static String convertDateToStringSeparator(final Date fecha) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_SEPARATOR);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }

  public static String convertDateToString(final Date fecha) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }

  public static String convertDateToFileName(final Date fecha) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(FILE_DATETIME_FORMAT);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }
  
  public static String convertDateToConcurrentFileName(final Date fecha) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(CONCURRENT_FILE_DATETIME_FORMAT);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }

  public static String convertTimeToString(final Date fecha) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }
  
  public static Date convertStringToTime(final String fecha) throws ParseException {
    Date fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
      fechaString = sdf.parse(fecha);
    }
    return fechaString;
  }

  public static String convertDateTimeToString(final Date fecha) {
    String fechaString = null;
    if (fecha != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT);
      fechaString = sdf.format(fecha);
    }
    return fechaString;
  }

  public static Date convertStringToDate(final String fechaString) {
    return convertStringToDate(fechaString, DATE_FORMAT);
  }

  public static Date convertStringToDate(final String fechaString, final String format) {
    final DateFormat df = new SimpleDateFormat(format);
    df.setLenient(false);
    Date fecha = null;
    try {
      if (fechaString != null) {
        fecha = df.parse(fechaString);
      }
    } catch (ParseException e) {
      LOG.warn("ERROR({}): Error convirtiendo la fecha: \"{}\": {}", e.getClass().getName(), fechaString,
               e.getMessage());
    }
    return fecha;
  }

  /**
   * Method that converts a String into a Date forcing CEST as TimeZone
   * @param fechaString
   * @param format
   * @return
   */
  public static Date convertStringToDateCEST(final String fechaString, final String format) {
    Date date = null;
    try {
      if (fechaString != null) {
        SimpleDateFormat isoFormat = new SimpleDateFormat(format);
        isoFormat.setTimeZone(TimeZone.getTimeZone("CEST"));
        date = isoFormat.parse(fechaString);
      }
    } catch (ParseException e) {
      LOG.warn("ERROR({}): Error convirtiendo la fecha: \"{}\": {}", e.getClass().getName(), fechaString,
               e.getMessage());
    }
    return date;
  }  

  public static Time convertStringToTime(final String timeString, final String format) {
    final DateFormat df = new SimpleDateFormat(format);

    // df.setLenient( false );
    Time time = null;
    try {
      if (timeString != null) {

        time = new Time(df.parse(timeString).getTime());

      }
    } catch (NullPointerException | ParseException e) {
      LOG.error(e.getMessage());
    }

    return time;
  }

  /**
   * @param str
   * @return
   * @throws SIBBACBusinessException
   */
  public static Integer convertStringToInteger(final String str) throws SIBBACBusinessException {
    Integer id = null;
    if (StringUtils.isNotBlank(str)) {
      try {
        id = NumberUtils.createInteger(str);
        if (id.equals(NumberUtils.INTEGER_ZERO)) {
          id = null;
        }
      } catch (NumberFormatException e) {
        throw new SIBBACBusinessException("Error convirtiendo a entero el valor: [" + str + "] ");
      }
    }
    return id;
  }

  /**
   * @param str
   * @return
   * @throws SIBBACBusinessException
   */
  public static Long convertStringToLong(final String str) throws SIBBACBusinessException {
    Long id = null;
    if (StringUtils.isNotBlank(str)) {
      try {
        id = NumberUtils.createLong(str);
        if (id.equals(NumberUtils.LONG_ZERO)) {
          id = null;
        }
      } catch (NumberFormatException e) {
        throw new SIBBACBusinessException("Error convirtiendo a long el valor: [" + str + "] ");
      }
    }
    return id;
  }

  /**
   * @param str
   * @return
   * @throws SIBBACBusinessException
   */
  public static BigDecimal convertStringToBigDecimal(String str) throws SIBBACBusinessException {
    BigDecimal id = null;
    if (StringUtils.isNotBlank(str)) {
      str = str.replace(".", "");
      str = str.replace(",", ".");
      try {
        id = NumberUtils.createBigDecimal(str);
      } catch (NumberFormatException e) {
        throw new SIBBACBusinessException("Error convirtiendo a BigDecimal el valor: [" + str + "] ");
      }
    }
    return id;
  }
  
  /**
   * Convierte cadena de caracteres a BigDecimal tomando el "." como separador decimal.
   * Suprime las ","
   * 
   * @param str
   * @return
   * @throws SIBBACBusinessException
   */
  public static BigDecimal convertStringToBigDecimalDotAsDecimal(String str) throws SIBBACBusinessException {
    BigDecimal id = null;
    if (StringUtils.isNotBlank(str)) {
      str = str.replace(",", "");
      try {
        id = NumberUtils.createBigDecimal(str);
      } catch (NumberFormatException e) {
        throw new SIBBACBusinessException("Error convirtiendo a BigDecimal el valor: [" + str + "] ");
      }
    }
    return id;
  }

  /**
   * @param str
   * @return
   * @throws SIBBACBusinessException
   */
  public static Boolean convertStringToBoolean(final String str) throws SIBBACBusinessException {
    Boolean booleanValue = null;
    if (StringUtils.isNotBlank(str)) {

      booleanValue = BooleanUtils.toBoolean(StringUtils.lowerCase(str));
      if (StringUtils.isBlank(str)) {
        throw new SIBBACBusinessException("Error convirtiendo a boolean el valor: [" + str + "] ");
      }
    }
    return booleanValue;
  }

  public static String toUTF8(final String txt) {
    if (txt != null) {
      return new String(txt.getBytes(StandardCharsets.UTF_8));
    } else {
      return null;
    }
  }

  public static BigDecimal roundedScale(BigDecimal value, int scale) {
    return value.setScale(scale, RoundingMode.HALF_UP);
  }

  public static String convertListToString(final List<String> lista) {
    StringBuilder listaString = new StringBuilder();
    if (lista != null && !lista.isEmpty()) {
      for (String value : lista) {
        if (listaString.length() > 0) {
          listaString.append(",");
        }
        listaString.append("'").append(value).append("'");
      }

    }
    return listaString.toString();
  }

  /**
   * 
   * Compares onli hour minute seconds miliseconds
   * */
  public static int compareTimeOnly(Date d1, Date d2) throws NullPointerException {
    Calendar c1 = Calendar.getInstance();
    c1.setTime(d1);

    Calendar c2 = Calendar.getInstance();
    c2.setTime(d2);

    c1.set(Calendar.DAY_OF_MONTH, c2.get(Calendar.DAY_OF_MONTH));
    c1.set(Calendar.MONTH, c2.get(Calendar.MONTH));
    c1.set(Calendar.YEAR, c2.get(Calendar.YEAR));
    Long c1m = c1.getTimeInMillis();
    Long c2m = c2.getTimeInMillis();
    return c1m.compareTo(c2m);

  }

  /**
   * 
   * Compares only year-day
   * */
  public static int compareDateOnly(Date d1, Date d2) throws NullPointerException {

    Calendar c1 = Calendar.getInstance();
    c1.setTime(d1);

    Calendar c2 = Calendar.getInstance();
    c2.setTime(d2);

    c1.set(Calendar.HOUR_OF_DAY, c2.get(Calendar.HOUR_OF_DAY));
    c1.set(Calendar.MINUTE, c2.get(Calendar.MINUTE));
    c1.set(Calendar.SECOND, c2.get(Calendar.SECOND));
    c1.set(Calendar.MILLISECOND, c2.get(Calendar.MILLISECOND));
    Long c1m = c1.getTimeInMillis();
    Long c2m = c2.getTimeInMillis();
    return c1m.compareTo(c2m);

  }

  /**
   * 	Devuelve true si el dato respeta el formato que se pase como 2do
   * 	argumento.
   * 	@param date
   * 	@param dateFormat
   */
  public static boolean isDateValid(String date, String dateFormat) {
	  try {
		  DateFormat df = new SimpleDateFormat(dateFormat);
		  df.setLenient(false);
		  df.parse(date);
		  return true;
	  } catch (ParseException e) {
		  return false;
	  }
}
	  public static String comprobarFechaVeranoInvierno(Date fEjecucion, Date hEjecucion) {
	  
	  String resultado = null;
	  String horaEjecucion = null;
	  TimeZone tz = TimeZone.getDefault();
	  int offsetActualUTC = tz.getRawOffset();
	  
	  if(tz.inDaylightTime(fEjecucion)) {
		  offsetActualUTC += tz.getDSTSavings();
	  }

	  Date auxUTC = new Date(hEjecucion.getTime() - offsetActualUTC);
	  horaEjecucion = convertDateToString(auxUTC, FormatDataUtils.TIME_FORMAT_1);
	  
	  resultado = convertDateToString(fEjecucion, FormatDataUtils.DATE_FORMAT_1).concat("T").concat(horaEjecucion).concat("Z");

	  return resultado;
  }

} // FormatDataUtils
