/**
 * # DateHelper.java
 * # Fecha de creación: 22/10/2015
 */
package sibbac.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Clase de utilidad para fechas.
 * 
 * @author XI316153
 */
public class DateHelper {

  private static final Logger LOG = LoggerFactory.getLogger(DateHelper.class);
  public static final String ISO = "dd/MM/yyyy";
  public static final String FORMAT_DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

  public enum DateHelperMovement {
    NEXT_DATE, PREVIOUS_DATE;
  }

  /**
   * This method returns true if the input date is the same as current date. Otherwise, returns false
   * @param fecha
   * @return
   */
  public static boolean isDateSameToCurrent(Date fecha) {

    final String currentDate = sdf.format(new Date(System.currentTimeMillis()));
    String menDate = sdf.format(fecha);

    return menDate.equals(currentDate);
  }

  public static Calendar getPreviousWorkDate(Calendar cal, List<Integer> workDaysOfWeekConfigList,
      List<Date> holidaysConfigList) {
    moveWorkDate(cal, 1, DateHelperMovement.PREVIOUS_DATE, workDaysOfWeekConfigList, holidaysConfigList);
    return cal;
  }

  /**
   * move cal to a non weekend-holyday day
   * 
   * @param cal
   * @param plus
   * @param workDaysOfWeekConfigList
   * @param holidaysConfigList
   */
  private static void skipWeekEnd(Calendar cal, DateHelperMovement movement, List<Integer> workDaysOfWeekConfigList,
      List<Date> holidaysConfigList) {
    int sentido = movement == DateHelperMovement.NEXT_DATE ? 1 : -1;
    while (isWeekend(cal.getTime(), workDaysOfWeekConfigList) || isHoliday(cal, holidaysConfigList)) {
      cal.add(Calendar.DAY_OF_YEAR, sentido);
    }
  }

  /**
   * 
   * Move cal date days to future or past
   * 
   * @param cal
   * @param days
   * @param movement
   * @param workDaysOfWeekConfigList
   * @param holidaysConfigList
   * @return
   */
  private static Calendar moveWorkDate(Calendar cal, Integer days, DateHelperMovement movement,
      List<Integer> workDaysOfWeekConfigList, List<Date> holidaysConfigList) {

    DateUtils.truncate(cal, Calendar.DATE);
    int sentido = movement == DateHelperMovement.NEXT_DATE ? 1 : -1;
    skipWeekEnd(cal, movement, workDaysOfWeekConfigList, holidaysConfigList);
    for (int i = 0; i < days; i++) {
      cal.add(Calendar.DAY_OF_YEAR, sentido);
      skipWeekEnd(cal, movement, workDaysOfWeekConfigList, holidaysConfigList);
    }
    return cal;
  }

  public static Calendar getPreviousWorkDate(Calendar cal, Integer previousDays,
      List<Integer> workDaysOfWeekConfigList, List<Date> holidaysConfigList) {
    return moveWorkDate(cal, previousDays, DateHelperMovement.PREVIOUS_DATE, workDaysOfWeekConfigList,
        holidaysConfigList);
  }

  public static Calendar getNextWorkDate(Calendar cal, Integer days, List<Integer> workDaysOfWeekConfigList,
      List<Date> holidaysConfigList) {
    return moveWorkDate(cal, days, DateHelperMovement.NEXT_DATE, workDaysOfWeekConfigList, holidaysConfigList);
  }

  /**
   * Devuelve el día habil obtenido a partir de la fecha especificada sumando el número de días especificado.
   * 
   * @param date Fecha a partir de la que empezar a contar.
   * @param previousDays Número de días a sumar.
   * @return <code>java.util.Date - </code>Fecha habil.
   */
  public static Date getNextWorkDate(Date date, Integer previousDays, List<Integer> workDaysOfWeekConfigList,
      List<Date> holidaysConfigList) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return getNextWorkDate(cal, previousDays, workDaysOfWeekConfigList, holidaysConfigList).getTime();
  } // getPreviousWorkDate

  public static Boolean isWorkDayOfWeek(Calendar dateCalendar, List<Integer> workDaysOfWeekConfigList) {
    return workDaysOfWeekConfigList.contains(dateCalendar.get(Calendar.DAY_OF_WEEK));
  } // isWorkDayOfWeek

  /**
   * Devuelve el día habil obtenido a partir de la fecha especificada restando el número de días especificado.
   * 
   * @param date Fecha a partir de la que empezar a contar.
   * @param previousDays Número de días a restar.
   * @return <code>java.util.Date - </code>Fecha habil.
   */
  public static Date getPreviousWorkDate(Date date, Integer previousDays, List<Integer> workDaysOfWeekConfigList,
      List<Date> holidaysConfigList) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    return getPreviousWorkDate(cal, previousDays, workDaysOfWeekConfigList, holidaysConfigList).getTime();
  } // getPreviousWorkDate

  /**
   * Establece la hora del <code>java.util.Calendar</code> especificado a <code>00:00:00.000</code>.
   * 
   * @param calendar <code>java.util.Calendar</code> a modificar.
   */
  private static void clearTime(Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
  } // clearTime

  /**
   * Indica si la fecha especificada es sábado o domingo.
   * 
   * @param date Fecha a comprobar.
   * @return <code>java.lang.Boolean - </code><code>true</code> si la fecha es sábado o domingo;
   * <code>false</code> en caso contrario.
   */
  public static Boolean isWeekend(Date date, List<Integer> workDaysOfWeekConfigList) {
    if (!isWorkDayOfWeek(date, workDaysOfWeekConfigList)) {
      return true;
    }
    else {
      return false;
    } // else
  } // isWeekend

  /**
   * Indica si el día de la fecha especificada es un día laborable.
   * 
   * @param date Fecha a comprobar.
   * @param workDaysOfWeekConfigList Lista de días laborables.
   * @return <code>java.lang.Boolean - </code><code>true</code> si el día es laborable; <code>false</code> en
   * caso contrario.
   */
  public static Boolean isWorkDayOfWeek(Date date, List<Integer> workDaysOfWeekConfigList) {
    Calendar dateCalendar = Calendar.getInstance();
    dateCalendar.setTime(date);
    return workDaysOfWeekConfigList == null
        || workDaysOfWeekConfigList.contains(dateCalendar.get(Calendar.DAY_OF_WEEK));
  } // isWorkDayOfWeek

  /**
   * Indica si la fecha especificada es un día de vacaciones.
   * 
   * @param date Fecha a comprobar.
   * @param holidaysConfigList Lista de días de vacaciones.
   * @return <code>java.lang.Boolean - </code><code>true</code> si la fecha es un día de vacaciones;
   * <code>false</code> en caso contrario.
   */
  public static Boolean isHoliday(Calendar date, List<Date> holidaysConfigList) {
    Calendar date2 = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
    Calendar holidayCalendar = Calendar.getInstance();
    if (CollectionUtils.isNotEmpty(holidaysConfigList)) {
      for (Date holiday : holidaysConfigList) {
        holidayCalendar.setTime(holiday);
        clearTime(holidayCalendar);
        if (date2.compareTo(holidayCalendar) == 0) {
          return true;
        } // if
      } // for
    }
    return false;
  } // isHoliday

  /**
   * Helper provisorio para convertir Date a Integer y almacenarlo en la DB.
   * 
   * @param fecha fecha
   * @return Integer Integer
   */
  public static Integer convertDateToInteger(Date fecha) {
    if (fecha != null) {
      StringBuilder fechaString = new StringBuilder();
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha);
      fechaString.append(calendar.get(Calendar.YEAR));
      int month = calendar.get(Calendar.MONTH) + 1;
      if (month < 10) {
        fechaString.append("0");
      }

      fechaString.append(month);
      int day = calendar.get(Calendar.DAY_OF_MONTH) - 1;
      if (day < 10) {
        fechaString.append("0");
      }
      fechaString.append(day);
      return Integer.valueOf(fechaString.toString());
    }
    return null;
  }

  /**
   * Se pasa un fecha como String y formateador y se devuelve un Date.
   * @param date date
   * @param format format
   * @return Date Date
   */
  public static Date convertStringToDate(final String date, final String format) {
    if (StringUtils.isNotBlank(date)) {
      SimpleDateFormat formateador = new SimpleDateFormat(format);
      try {
        return formateador.parse(date);
      }
      catch (java.text.ParseException e) {
        LOG.error("Error al parsear fecha" + e.getMessage());
      }
    }
    return null;
  }

  /**
   * Se pasa una fecha en Date y se devuelve un String formateado.
   * @param date: fecha a procesar
   * @param format: formato
   */
  public static final String convertDateToString(final Date date, final String format) {
    if (date != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(format);
      return sdf.format(date);
    }
    else {
      return null;
    }
  }

  /**
   * Obtiene fecha de inicio del dia (00:00:00.000).
   * @param date
   * @return Date
   */
  public static Date getStartOfDay(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  /**
   * Obtiene fecha de fin del dia (23:59:59.999).
   * @param date
   * @return Date
   */
  public static Date getEndOfDay(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);
    return calendar.getTime();
  }

  /**
   * Devuelve true si la fecha es fin de semana.
   * @param cal cal
   * @return boolean boolean
   */
  public static boolean isFechaFinDeSemana(Calendar cal) {
    if (cal != null && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
        || (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)) {
      return true;
    }
    return false;
  }

  /**
   * Obtiene una lista de fechas con los dias.
   *
   * @param dFechaInicio - Fecha de inicio
   * @param dFechaFin - Fecha de Fin - Si es nula se obtiene hasta el dia actual
   * @return Lista con todos los dias que hay entre las fechas dadas
   * @throws Exception
   */
  public static List<Date> getListaFechas(Date dFechaInicio, Date dFechaFin) throws Exception {
    LOG.debug("Entra en getListaFechas");

    List<Date> lResultado = new ArrayList<Date>();

    Date diaSiguiente;
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

    try {
      // Tratamiento de las fechas de inicio y final.
      // Se tiene que incluir la fecha de inicio que no puede ser nula.
      lResultado.add(dFechaInicio);

      // Si la fecha final es nula, se inicializa con la fecha actual sin horas minutos y segundos
      if (dFechaFin == null) {

        dFechaFin = formatoFecha.parse(formatoFecha.format(new Date()));
      }

      diaSiguiente = dFechaInicio;

      // Calendar calendar = Calendar.getInstance();
      while (diaSiguiente.before(dFechaFin)) {

        diaSiguiente = sumaDias(diaSiguiente, 1);
        lResultado.add(diaSiguiente);
      }

    }
    catch (Exception e) {
      LOG.error("getListaFechas - Se produjo un error al obtener las fechas de ejecucion " + e.getMessage());
      throw e;
    }

    LOG.debug("Fin en getListaFechas");

    return lResultado;

  }

  /**
   * Suma dias a una fecha dada.
   *
   * @param dFecha - Fecha inicial
   * @param iNumdias - Numero de dias a sumar
   * @return Fecha resultado de la suma de los dias a la fecha dada.
   * @throws Exception
   */

  public static Date sumaDias(Date dFecha, int iNumdias) throws Exception {

    Date dResultado;
    Calendar calendar = Calendar.getInstance();

    try {
      calendar.setTime(dFecha);
      calendar.add(Calendar.DAY_OF_YEAR, iNumdias);
      dResultado = calendar.getTime();

    }
    catch (Exception e) {
      LOG.error("getListaFechas - Se produjo un error al obtener las fechas de ejecucion " + e.getMessage());
      throw e;
    }

    return dResultado;

  }

  /**
   * Resta dias a una fecha dada.
   *
   * @param dFecha - Fecha inicial
   * @param iNumdias - Numero de dias a restar
   * @return Fecha resultado de la resta de los dias a la fecha dada.
   * @throws Exception
   */

  public static Date restaDias(Date dFecha, int iNumdias) throws Exception {
    LOG.debug("Entra en restaDias");

    Date dResultado;
    Calendar calendar = Calendar.getInstance();

    try {
      calendar.setTime(dFecha);
      calendar.add(Calendar.DAY_OF_YEAR, -iNumdias);
      dResultado = calendar.getTime();

    }
    catch (Exception e) {
      LOG.error("restaDias - Se produjo un error al obtener las fechas de ejecucion " + e.getMessage());
      throw e;
    }

    LOG.debug("Fin en restaDias");

    return dResultado;

  }

  /**
   * Se convierte secuencia de cadena de caracteres a Date.
   * @param cadena: formato ddMMyy
   * @return Date: fecha formateada
   */
  public static Date convertStringChainToDate(String cadena) {
    Date fecha = null;
    if (StringUtils.isNotBlank(cadena)) {
      SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy");
      try {
        fecha = formatter.parse(cadena);
      }
      catch (ParseException e) {
        LOG.error("Error al convertir cadena de caracteres a fecha.", e);
      }
    }
    return fecha;
  }
  
  /**
   * Obtiene un rango de fechas por meses
   * @param startDate
   * @param endDate
   * @return
   */
  public static List<Calendar[]> getMonthsBetweenDate(Date startDate, Date endDate) {
    final List<Calendar[]> datesInRange = new ArrayList<Calendar[]>();
    
    final Calendar calendar = new GregorianCalendar();
    final Calendar endCalendar = new GregorianCalendar();

    calendar.setTime(startDate);
    endCalendar.setTime(endDate);

    while (calendar.before(endCalendar) || calendar.equals(endCalendar)) {   
      final Calendar[] range = getRange(calendar); 
           
      calendar.set(Calendar.DAY_OF_MONTH, 1);
      calendar.add(Calendar.MONTH, 1);
      
      if (endCalendar.before(range[1])) {
        range[1] = endCalendar;
      }      
      datesInRange.add(range); 
    }     
    
    return datesInRange;
  }
  
  /**
   * Obtiene el primer dia del mes y el ultimo del mismo
   * @param calendar
   * @return
   */
  private static Calendar[] getRange(Calendar calendar) {
    final Calendar startMonthDateDay = Calendar.getInstance();
    final Calendar endMonthDateDay = Calendar.getInstance();
    
    startMonthDateDay.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
    startMonthDateDay.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
    startMonthDateDay.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
    startMonthDateDay.set(Calendar.HOUR_OF_DAY, 0);
    startMonthDateDay.set(Calendar.MINUTE, 0);
    startMonthDateDay.set(Calendar.SECOND, 0);
    
    endMonthDateDay.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    endMonthDateDay.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
    endMonthDateDay.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
    endMonthDateDay.set(Calendar.HOUR_OF_DAY, 0);
    endMonthDateDay.set(Calendar.MINUTE, 0);
    endMonthDateDay.set(Calendar.SECOND, 0);
    
   return new Calendar[] {startMonthDateDay, endMonthDateDay}; 
  }

} // DateHelper
