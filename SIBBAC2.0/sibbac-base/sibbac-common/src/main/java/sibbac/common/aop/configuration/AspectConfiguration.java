package sibbac.common.aop.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import sibbac.common.aop.logging.LoggingAspect;

@Configuration
@EnableAspectJAutoProxy
public class AspectConfiguration {

  @Bean
  public LoggingAspect loggingAspect() {
    return new LoggingAspect();
  }
}
