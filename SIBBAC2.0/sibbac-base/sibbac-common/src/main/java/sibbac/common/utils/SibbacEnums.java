package sibbac.common.utils;

import java.text.MessageFormat;

/**
 * Clase con las enumeraciones utilizadas en sibbac.
 * 
 * @author XI316153
 */
public class SibbacEnums {

  public enum SIBBACLoggingMessages {

    /** */
    LOGGING_ENTER_MESSAGE("Entrando en {}.{}() con los siguientes parametros = {}"),

    /** */
    LOGGING_EXIT_MESSAGE("Saliendo de {}.{}() return de los siguientes datos = {}"),

    /** */
    LOGGING_ERROR_MESSAGE("Error inesperado: {} en {}.{}()"),
    
    /** */
    ERROR_MESSAGE("Error inesperado consulte con el administrador");

    private String value;

    private SIBBACLoggingMessages(String value) {
      this.value = value;
    }

    public String value() {
      return this.value;
    }
  }
  

  public static enum YES_NO {
    YES("1"), NO("0"), TRUE("1"), FALSE("0");

    private String value;

    private YES_NO(final String value) {
      this.value = value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  public static enum SI_NO {
    SI('S'), NO('N');

    private Character value;

    private SI_NO(final Character value) {
      this.value = value;
    }

    public Character getValue() {
      return value;
    }

    @Override
    public String toString() {

      return value + "";
    }
  }

  public static enum TypeBrokerProcess {
    CANCEL_GROUP("Cancel group to broker"), CANCEL_CONFIRM("Cancel confirmation to broker");

    private String nameProcess;

    private TypeBrokerProcess(String _nameProcess) {
      nameProcess = _nameProcess;
    }

    public String getNameProcess() {
      return nameProcess;
    }
  }

  public static enum TypeClearerProcess {
    CONFIRM_SEND("Confirm and send to clearer"), CHANGE_STATUS("Change Status Confirmed"), INPUT_SETTLEMENT_MARKET(
        "Input Settlement Market"), INPUT_SETTLEMENT_CLIENT("Input Settlement Client"), SEND(
            "Send to clearer"), DELETE_SEND("Delete send to clearer"), RESEND(
                "Resend to clearer"), CANCEL_CONFIRM("Cancel confirmation to clearer");

    private String nameProcess;

    private TypeClearerProcess(String _nameProcess) {
      nameProcess = _nameProcess;
    }

    public String getNameProcess() {
      return nameProcess;
    }
  }

  public static enum TypeResultProcess {
    FINISH_OK, FINISH_ERR
  }

  public static enum TypeCdestado {

    CANCEL_REGISTER("999", "999"), NEW_REGISTER("000", "000"),

    REJECT_BOOKING("002", "002"), REJECT_ALLOCATION("003", "003"),

    MTF_NETEO_PENDING("015", "016"), MTF_NETEO_PENDING_DATA("016", "016"), MTF_NETEO_OK("100", "100"),

    FOREING_PENDIENTE_RECONCILIAR("010", "010"), FOREING_PENDIENTE_NETEO("015", "015"), FOREING_WAIT_CONFIRM_BROKER(
        "100", "100"), STATE_TMCT0PKB_CONFIRM_BOOKING_PARTIALLY("120", "120"), MTF_PENDING_CLEARER("300",
            "300"), FOREING_PENDING_CLEARER("300", "300"), CONFIR_SEND_CLEARER("310", "300"), IN_PROCESS("315",
                "315"), FOREING_WAIT_RESPONSE_CLEARER("320",
                    "315"), FOREING_CLEARER_DIFFERENCES("370", "390"), FOREING_CONTABILIZADO("400", "390"),

    N_ALLOCATE_MANUAL("500", "510"), N_ALLOCATE_NO_SETTLEMENT_DATA("505", "510"), N_ALLOCATE_IN_PROCESS("510",
        "510"), N_SEND_PENDING_PARTENON("515", "517"), N_SEND_PARTENON("516", "517"), N_PARTIAL_PARTENON("517",
            "517"), N_ERROR_PARTENON("519", "517"), N_RECEIVED_PARTENON("520", "517"), N_ALLOCATE_PENDIENTE_MANUAL(
                "518",
                "518"), N_ALLOCATE_FINAL("520", "510"), N_ALLOCATE_SENT_PARTIAL_AUD("523", "523"), N_ALLOCATE_SENT_AUD(
                    "531", "523"), N_ALLOCATE_REJECTED_AUD("532", "525"), N_ALLOCATE_REJECTED_PARTIAL_AUD("525",
                        "525"), N_ALLOCATE_PROVISIONAL_AUD("533", "530"), N_ALLOCATE_PROVISIONAL_PARTIAL_AUD("530",
                            "530"), N_ALLOCATE_ACEPTED_AUD_PEND_LIQ_PEND_SEND_TR("535",
                                "536"), N_ALLOCATE_FINAL_MAB("535", "510"), N_ALLOCATE_ACEPTED_AUD_INPROCESS_LIQ("536",
                                    "536"), N_ALLOCATE_ACEPTED_AUD_IMPUTADO("540",
                                        "527"), N_ALLOCATE_IN_DIFFERENCES_TITTLES_FOR_SVB("541", "533"),

    N_ALLOCATE_ACEPTED_SI_SEND_TR("545", "535"), N_ALLOCATE_ACEPTED_SI_REJECTED_TR("547",
        "547"), N_ALLOCATE_ACEPTED_SI_ACEPTED_TR("548",
            "545"), N_ALLOCATE_ACEPTED_SI_ACEPTED_TR_FINAL_PEND_LIQ("549", "548"), @Deprecated
    N_ALLOCATE_SENDED_CLEARER("550", "536"), N_ALLOCATE_ORDER_TYPE_SEND("570", "570"), N_ALLOCATE_ACEPTED_LIQ_AUD("600",
        "549"), N_ALLOCATE_ACEPTED_LIQ_MTF("600", "595"), N_ALLOCATE_LIQ_ACCOUNTING_PENDING_AUD("595",
            "549"), N_ALLOCATE_LIQ_ACCOUNTING_PENDING("595",
                "595"), N_ALLOCATE_ACCOUNTING_INPROCESS("635", "635"), N_ALLOCATE_ACCOUNTING_OK("640", "635"),

    PROCESS_EXECUTE("900", "900"), MSG_INACTIVO("999", "999"), MSG_ERROR("000", "000"), MSG_GENERADO("010",
        "010"), MSG_LINEA_GENERADO("015", "015"), MSG_VALIDADO_ERROR("070", "070"), MSG_RECIBIDO("040",
            "040"), MSG_PENDIENTE_VALIDAR("050", "050"), MSG_VALIDADO("060", "060"), MSG_NOEXISTE_REFERENCIA("030",
                "030"), MSG_ERROR_PKB_ESTADO("088", "088"), MSG_ERROR_CTD_ESTADO("089",
                    "089"), MSG_ERROR_INTEGRIDAD("090", "090"), MSG_ERROR_UPDATE_ESTADOS("092",
                        "092"), MSG_ERROR_CONTABILIDAD("095", "095"), MSG_ERROR_ADI_INSERT("513", "513"),

    CANCEL_DONE_ACCOUNTING("391", "390"), LIQUIDADO_PARCIALMENTE("390",
        "390"), FOREING_WAIT_RESPONSE_CLEARER_BACK_CANCEL_DONE_ACCOUNTING("320",
            "390"), CLIENT_PARCIALMENTE_LIQUIDADO("392", "392"), DIFERENCIAS_LIQUIDADORA("370",
                "320"), MARKET_PARCIALMENTE_LIQUIDADO("393", "393"), CLIENT_PENDIENTE_RESPUESTA_LIQUIDADORA("394",
                    "394"), MARKET_PENDIENTE_RESPUESTA_LIQUIDADORA("395", "395"),

    AUD_PENDIENTE_ENVIO("P", "P"), AUD_PENDIENTE_RESPUESTA("E", "P"), AUD_RECHAZADO_R("R", "E"), AUD_RECHAZADO_B("B",
        "E"), AUD_CONFIRMADO("C", "E"), AUD_PENDIENTE_TITULAR("P", ""), AUD_PENDIENTE_RESPUESTA_TITULAR("E",
            "P"), AUD_MODIFICADO("M", "P"), AUD_FUERA_PLAZO("T", "P"), AUD_IMPUTADO("I",
                "P"), AUD_CAMBIO_LIQUIDADORA("Q", "E"), AUD_ENVIADO_VOLUMEN_RIESGO("V",
                    " "), AUD_DESGLOSE_EXCEPCIONAL("A", " "), AUD_PROBLEMAS_CON_TITULARES("F",
                        " "), AUD_TR_PENDIENTE_RESPUESTA("106", "000"), AUD_TR_CONFIRMADO("107",
                            "106"), AUD_TR_RECHAZADO("108", "106"), AUD_HOLDERS_MODIFICADOS("M", " "),

    FISCAL_DATA_HOLDERS_ACTIVOS("A", "I"), FISCAL_DATA_HOLDERS_INACTIVOS("I", "A"),

    MEGARA_NO_ENVIAR_LIQ("0", " "), MEGARA_PENDIENTE_ENVIO_LIQ("1", " "), MEGARA_ENVIADO_LIQ("2",
        " "), MEGARA_ERROR_ENVIO_LIQ("3", " "), MEGARA_RECIBIDO_LIQ("4", " ")

    ;

    private String value;
    private String valueInProcess;

    private TypeCdestado(String _value, String _valueInProcess) {
      value = _value;
      valueInProcess = _valueInProcess;
    }

    public String getValue() {
      return value.trim();
    }

    public String getValueInProcess() {
      return valueInProcess.trim();
    }

    public TypeCdestado valueOf2(String value) {
      for (TypeCdestado status : values()) {
        if (status.getValue().equals(value.trim())) {
          return status;
        }
      }
      return null;
    }

  }
  

  /**
   * Mensajes de Logs
   * @author ipalacio
   *
   */
  public enum LoggingMessages {
    
  /** */
  USER_SESSION("UserSession"),

  /** */
  DEBUG_MESSAGE_OAP_REST_ENTER("Enter: En {}.{}() con los siguientes parametros = {}"),

  /** */
  DEBUG_MESSAGE_OAP_REST_EXIT("Exit: En {}.{}() return de los siguientes datos = {}"),

  /** */
  DEBUG_MESSAGE_OAP_REST_ERROR("Error: Argumento Ilegal: {} en {}.{}()"),

  /** */
  RUNTIME_EXCEPTION_MESSAGE("[executeAction] Error inesperado al intentar ejecutar la accion {}"),
  
  /** */
  ILLEGAL_ACCESS_EXCEPTION_MESSAGE("[executeAction] Error inesperado al intentar acceder a el método {}"),
  
  /** */
  COUNTRY_NOT_FOUND("[executeAction] El pais seleccionado no existe"),

  /** */
  ID_OBJECT_NOT_FIND("[executeAction] La primarykey del objeto mandado no existe"),

  /** */
  PK_OBJECT_NULL("[executeAction] La primarykey del objeto mandado no está completa."),

  /** */
  PK_NOT_COMPLETE("[executeAction] Compruebe que los datos del pais existen o se han rellenado correctamente."),

  /** */
  DOCUMENT_NOT_EXIST("[executeAction] No existe este documento"),

  /** */
  ANALYSIS_NOT_EXIST("[executeAction] No existe este análisis"),

  /** */
  MULTIPLE_ID_OBJECT_NOT_FIND("[executeAction] Los IDS del objeto mandado no existen"),

  /** */
  DOCUMENTACION_NULL("[executeAction] El ID de la documentacion mandado no existe"),

  /** */
  EFECTIVO_NULL("[executeAction] El ID del efectivo mandado no existe"),

  /** */
  ANALISIS_NULL("[executeAction] El ID del analisis mandado no existe"),

  /** */
  OBSERVACION_NULL("[executeAction] El ID de la observacion mandado no existe"),

  /** */
  OBSERVACION_DUPLICATED("[executeAction] El ID de la observacion mandado ya existe"),

  /** */
  BASIC_VALIDATION_ERROR("[executeAction] Los campos ID y Descripción no pueden estar vacio"),

  /** */
  OBJECT_NULL("El objeto recibido viene nulo");

    private String value;

    private LoggingMessages(String value) {
      this.value = value;
    }

    public String value() {
      return this.value;
    }
  }
  
  /**
   * Mensajes de Aplicacion
   * @author ipalacio
   *
   */
  public enum ApplicationMessages {

    UNEXPECTED_MESSAGE("Error inesperado, consulte con administrador"),

    ADD_REQUEST_DATA("Error, los datos obligatorios no han sido rellenados"),

    ERROR_DATE("Error, la Fecha de Inicio no puede ser superior a la Fecha de Fin"),

    ADDED_DATA("Registro insertado correctamente"),

    MODIFIED_DATA("Registro modificado correctamente"),

    DELETED_DATA("Registro(s) borrado(s) correctamente"),
    
    PARSE_FAILED("Erro, se ha parseado un valor incorrecto"),

    MULTIPLE_NOT_EXIST("Error, compruebe que las ids {} existan en la base de datos"),

    DUPLICATED_ID("Error, id duplicada"),

    WRONG_CALL("Llamada a servicio equivocado, consulte con el administrador"),

    EMPTY_CALL("Solicitud vacia"),

    INVALID_VALIDATION("Se ha dado un error en la validacion de los datos obligatorios"),

    DATE_CHANGED("La fecha de revisión ha sido modificada en todos los elementos de la tabla.");

    private String value;

    private ApplicationMessages(String value) {
      this.value = value;
    }

    public String value() {
      return value;
    }
  }
  
  /**
   * Mensajes de traders sales
   * @author ipalacio
   *
   */
  public enum TradersSalesMessages {
    
    CDEMPLEADO_INCORRECT("Usuario incorrecto"), 
    
    INVALID_IDMIFID("- El campo [ID MIFID] no ha pasado las validaciones del Tipo ID MIFID seleccionado."), 
    
    INVALID_COUNTRY("- El campo [Pais nacionalidad] no es un país valido, consulte con el administrador."), 
    
    INVALID_COUNTRY_MIFID("El campo [ID MIFID] no respeta la validación del Tipo ID MIFID escogido."), 
    
    DUPLICATED_ID("Id duplicada")
    
    ;
    
    private String value;
    
    private TradersSalesMessages(String value) {
      this.value = value;
    }
    
    public String value() {
      return value;
    }
  }

  /**
   * Clase de Enumerados que contiene las claves necesarias para obtener
   * registros de la tabla tmct0cfg.
   */
  public static enum TypeKeysCdEstado {
    PENDIENTE_ENVIO("P", "CORBTSQL_TD_MCTALD_01_PENDIENTE_ENVIO_TRANSACTION_LABEL"), PENDIENTE_RESPUESTA("E",
        "CORBTSQL_TD_MCTALD_01_PENDIENTE_RESPUESTA_TRANSACTION_LABEL"), RECHAZADO_R("R",
            "CORBTSQL_TD_MCTALD_01_RECHAZADO_TRANSACTION_LABEL"), RECHAZADO_B("B",
                "CORBTSQL_TD_MCTALD_01_RECHAZADO_TRANSACTION_LABEL"), CONFIRMADO("C",
                    "CORBTSQL_TD_MCTALD_01_CONFIRMADO_TRANSACTION_LABEL"), ACEPTADO("",
                        "CORBTSQL_TD_MCTALD_01_ACEPTADO_TRANSACTION_LABEL"), IMPUTADO("I",
                            "CORBTSQL_TD_MCTALD_01_IMPUTADO_TITULAR_TRANSACTION_LABEL"), CAMBIO_LIQUIDADORA("Q",
                                "CORBTSQL_TD_MCTALD_01_CAMBIO_LIQUIDADORA_TRANSACTION_LABEL"), ENVIADO_VOLUMEN_RIESGO(
                                    "V",
                                    "CORBTSQL_TD_MCTALD_01_ENVIADO_VOLUMEN_RIESGO_TRANSACTION_LABEL"), DESGLOSE_EXCEPCIONAL(
                                        "A",
                                        "CORBTSQL_TD_MCTALD_01_DESGLOSE_EXCEPCIONAL_TRANSACTION_LABEL"), MODIFICADO("M",
                                            "CORBTSQL_TD_MCTALD_01_MODIFICADO_REENVIAR_TRANSACTION_LABEL"), FUERA_PLAZO(
                                                "T",
                                                "CORBTSQL_TD_MCTALD_01_MODIFICADO_FUERA_PLAZO_TRANSACTION_LABEL"), VALIDATION_ERRORS(
                                                    "F",
                                                    "CORBTSQL_TD_MCTALD_01_FISCAL_DATA_VALIDATION_ERROR_REENVIAR_TRANSACTION_LABEL"),

    FISCAL_DATA_HOLDERS_ACTIVOS("A", "Activo"), FISCAL_DATA_HOLDERS_INACTIVOS("I", "Inactivo"),;

    private String keyLabel, key;

    private TypeKeysCdEstado(String _key, String _keyLabel) {
      keyLabel = _keyLabel;
      key = _key;
    }

    public String getKeyLabel() {
      return keyLabel.trim();
    }

    public String getKey() {
      return key.trim();
    }

    @Override
    public String toString() {
      return "<typekeycdestado keyname=\"" + getKey() + "\" keylabel=\"" + getKeyLabel() + "\"/>";
    }

  }

  /**
   * Clase enumerada que contiene las claves necesarias para obtener registros
   * de la tabla tmct0cfg.
   */
  public static enum Tmct0cfgConfig {
    CONFIG_HOLIDAYS_DAYS("CONFIG", "holydays.anual.days"), CONFIG_WEEK_WORK_DAYS("CONFIG",
        "week.work.days"), CONFIG_WEEK_PROCESS_ACTIVATION_DAYS("CONFIG",
            "week.process.activation.days"), CONFIG_RANGE_HOURS_OF_DAY_PROCESS_STOP("CONFIG",
                "range.hours.of.day.process.stop"), CONFIG_LOAD_XML_INTERNACIONAL("CONFIG",
                    "xml.load.internacional"), MTF_NETEO_NACIONAL("MTF", "neteo.nacional"), MTF_NETEO_EXTRANJERO("MTF",
                        "neteo.extranjero"), AUD_SESSION_HOUR_BEGIN("AUD",
                            "national.session.hour.begin"), AUD_SESSION_HOUR_END("AUD",
                                "national.session.hour.end"), AUD_PATH_RECEIVE_MESSAGES_ERROR("AUD",
                                    "national.path.receive.error"), AUD_NUMBER_LIMIT_REPROCESS_RECEIVE_MESSAGES_ERROR(
                                        "AUD",
                                        "national.number.limit.reprocess.receive.error.file"), AUD_PATH_RECEIVE_PROCESS_LOG_ERROR(
                                            "AUD",
                                            "national.path.receive.error.process"), AUD_VR_VOLUMEN_CDCLEARER("AUD",
                                                "national.vr.volumen.cdclearer"), AUD_VR_VOLUMEN_SEND_HOUR_BEGIN("AUD",
                                                    "national.vr.volumen.send.hour.begin"), AUD_VR_VOLUMEN_SEND_HOUR_END(
                                                        "AUD",
                                                        "national.vr.volumen.send.hour.end"), AUD_INIT_DAY_HOUR_BEGIN(
                                                            "AUD",
                                                            "national.process.init.day.hour.begin"), AUD_INIT_DAY_HOUR_END(
                                                                "AUD",
                                                                "national.process.init.day.hour.end"), AUD_PATH_RECEIVE__MESSAGES_SI_MAB(
                                                                    "AUD",
                                                                    "national.receive.si.mab.save.path"), AUD_SI_TITULARES_SEND_HOUR_END(
                                                                        "AUD",
                                                                        "national.si.titular.send.hour.end"), AUD_SI_R11_VERSION(
                                                                            "AUD",
                                                                            "national.si.r11.version"), AUD_MAIL_PROCESS_END_DAY_SEND_TO(
                                                                                "MAIL",
                                                                                "national.mail.end.process.day.send.to."), AUD_MAIL_PROCESS_END_DAY_SEND_CC(
                                                                                    "MAIL",
                                                                                    "national.mail.end.process.day.send.cc."), AUD_MAIL_PROCESS_END_DAY_SEND_CCO(
                                                                                        "MAIL",
                                                                                        "national.mail.end.process.day.send.cco."), AUD_MAIL_PROCESS_END_DAY_SEND_FROM(
                                                                                            "MAIL",
                                                                                            "national.mail.end.process.day.send.from"), AUD_SEND_TO_STRATUS_EX(
                                                                                                "AUD",
                                                                                                "national.send.to.stratus.ex"), AUD_SEND_TO_FIDESSA_EX(
                                                                                                    "AUD",
                                                                                                    "national.send.to.fidessa.ex"), AUD_SEND_TO_STRATUS_OTHER_MESSAGES(
                                                                                                        "AUD",
                                                                                                        "national.send.to.stratus.other.messages"), AUD_REJECT_ORDER_MODE_MORE_THAN_50_EXECUTION_GROUP(
                                                                                                            "AUD",
                                                                                                            "national.reject.order.mode.more.than.50.executions.groups"), AUD_MARKS_X_CDENVTIT_PROCESS_ACCEPTED_AUD(
                                                                                                                "AUD",
                                                                                                                "national.marks.x.cdenvtit.process.accept.aud"), AUD_ALLOCATE_MANUAL_WHEN_ORDER_MODE_MORE_THAN_50_PRICES(
                                                                                                                    "AUD",
                                                                                                                    "national.allocate.manual.when.order.mode.more.than.50.prices"), AUD_RECEIVE_PT_PATH(
                                                                                                                        "AUD",
                                                                                                                        "national.receive.pt.path"), AUD_CALCULATE_EFFECTIVE(
                                                                                                                            "AUD",
                                                                                                                            "national.calculate.effective"), AUD_SEND_AUD_AUTOMATIC_PREVIOUS_WORKS_DATE(
                                                                                                                                "AUD",
                                                                                                                                "national.send.aud.automatic.previous.works.date"), AUD_SEND_AUD_AUTOMATIC_UP_STATUS_PREVIOUS_DAYS(
                                                                                                                                    "AUD",
                                                                                                                                    "national.automatic.up.status.previous.days"), AUD_SEND_AUD_AUTOMATIC_UP_STATUS_PREVIOUS_SECURITY_SECONDS(
                                                                                                                                        "AUD",
                                                                                                                                        "national.automatic.up.status.previous.security.seconds"),

    AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_DELETE_OLD_ALLOCATION_TO_CLEARER("AUD",
        "national.allocate.semiautomatic.excel.delete.old.allocation.to.clearer"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_MAX_ROWS_ONLINE(
            "AUD",
            "national.allocate.semiautomatic.excel.max.rows.online"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_NUOPROUT(
                "AUD",
                "national.allocate.semiautomatic.excel.column.index.nuoprout"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_NBOOKING(
                    "AUD",
                    "national.allocate.semiautomatic.excel.column.index.nbooking"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_TITTLES(
                        "AUD",
                        "national.allocate.semiautomatic.excel.column.index.tittles"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_HOLDER(
                            "AUD",
                            "national.allocate.semiautomatic.excel.column.index.holder"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_TYPE_HOLDER(
                                "AUD",
                                "national.allocate.semiautomatic.excel.column.index.type.holder"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CDREFBAN(
                                    "AUD",
                                    "national.allocate.semiautomatic.excel.column.index.cdrefban"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_ACCTATCLE(
                                        "AUD",
                                        "national.allocate.semiautomatic.excel.column.index.acctatcle"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_POSITION_BREAKDOWN(
                                            "AUD",
                                            "national.allocate.semiautomatic.excel.column.index.position.breakdown"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_TPDSALDO(
                                                "AUD",
                                                "national.allocate.semiautomatic.excel.column.index.tpdsaldo"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CDENTLIQ(
                                                    "AUD",
                                                    "national.allocate.semiautomatic.excel.column.index.cdentliq"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CDMODORD(
                                                        "AUD",
                                                        "national.allocate.semiautomatic.excel.column.index.cdmodord"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CDCOMISN(
                                                            "AUD",
                                                            "national.allocate.semiautomatic.excel.column.index.cdcomisn"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_PCCOMBCO(
                                                                "AUD",
                                                                "national.allocate.semiautomatic.excel.column.index.pccombco"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_PCCOMBRK(
                                                                    "AUD",
                                                                    "national.allocate.semiautomatic.excel.column.index.pccombrk"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CDCUSTOD(
                                                                        "AUD",
                                                                        "national.allocate.semiautomatic.excel.column.index.cdcustod"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CLEARING_RULE(
                                                                            "AUD",
                                                                            "national.allocate.semiautomatic.excel.column.index.idregla"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_MNEMONIC(
                                                                                "AUD",
                                                                                "national.allocate.semiautomatic.excel.column.index.idnemotecnico"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CLEARING_MEMBER(
                                                                                    "AUD",
                                                                                    "national.allocate.semiautomatic.excel.column.index.idcompensador"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_GIVEUP_REFERENCE(
                                                                                        "AUD",
                                                                                        "national.allocate.semiautomatic.excel.column.index.cdrefasignacion"), AUD_ALLOCATE_SEMIAUTOMATIC_EXCEL_COLUMN_INDEX_CLEARING_ACCOUNT(
                                                                                            "AUD",
                                                                                            "national.allocate.semiautomatic.excel.column.index.idcuentacompensacion"),

    BANIF_FISCAL_DATA_RECEIVE("BANIF", "banif.fiscal.data.receive.path"), BANIF_LAST_MODIFIED_DATE_FROM_PROCESSED_FILES(
        "BANIF", "banif.last.modified.date.from.processed.files"), BANIF_LAST_LINE_LOG_DATE_FROM_PROCESSED_FILES(
            "BANIF", "banif.last.log.date.from.processed.files"),

    SECURITY_COOKIE_NAME_PROPERTY("SECURITY", "authenticator.securityObject.cookieName"), SECURITY_TOKEN_NAME_PROPERTY(
        "SECURITY", "authenticator.securityObject.tokenName"),

    SECURITY_LOGIN_PAGE_PROPERTY("SECURITY",
        "authenticator.securityObject.loginPage"), SECURITY_LOGIN_PAGE_FLAG_PROPERTY("SECURITY",
            "authenticator.securityObject.loginPageFlag"),

    DATAWAREHOUSE_OUTPUT_FOLDER("DATAWAREHOUSE", "output_folder"), DATAWAREHOUSE_0("DATAWAREHOUSE",
        "DATAWAREHOUSE_0"), DATAWAREHOUSE_1("DATAWAREHOUSE", "DATAWAREHOUSE_1"), DATAWAREHOUSE_2("DATAWAREHOUSE",
            "DATAWAREHOUSE_2"), DATAWAREHOUSE_3("DATAWAREHOUSE", "DATAWAREHOUSE_3"), DATAWAREHOUSE_4("DATAWAREHOUSE",
                "DATAWAREHOUSE_4"), DATAWAREHOUSE_5("DATAWAREHOUSE", "DATAWAREHOUSE_5"), DATAWAREHOUSE_6(
                    "DATAWAREHOUSE",
                    "DATAWAREHOUSE_6"), DATAWAREHOUSE_END_HOUR("DATAWAREHOUSE", "end_hour"), DATAWAREHOUSE_DAYS_INS_INI(
                        "DATAWAREHOUSE", "days_ins_ini"), DATAWAREHOUSE_DAYS_INS_FIN("DATAWAREHOUSE",
                            "days_ins_fin"), DATAWAREHOUSE_START_HOUR("DATAWAREHOUSE",
                                "start_hour"), DATAWAREHOUSE_NATIONAL_TAXES("DATAWAREHOUSE",
                                    "DATAWAREHOUSE_NATIONAL_TAXES"), EXCEPTION_DATAWAREHOUSE_NATIONAL_TAXES(
                                        "DATAWAREHOUSE",
                                        "EXCEPTION_DATAWAREHOUSE_NATIONAL_TAXES"), MAIL_CLEARING_PARTENON_CC(
                                            "MAIL_CLEARING_PARTENON",
                                            "CC"), MAIL_CLEARING_PARTENON_MAIL_CONFIRM("MAIL_CLEARING_PARTENON",
                                                "MAIL_CONFIRM"), MAIL_CLEARING_PARTENON_MESSAGE_BODY(
                                                    "MAIL_CLEARING_PARTENON",
                                                    "MESSAGE_BODY"), MAIL_CLEARING_PARTENON_FROM(
                                                        "MAIL_CLEARING_PARTENON",
                                                        "FROM"), MAIL_CLEARING_PARTENON_SUBJECT(
                                                            "MAIL_CLEARING_PARTENON",
                                                            "SUBJECT"), MEGARA_PARTENON_FOLDER_partenon_output(
                                                                "MEGARA_PARTENON_FOLDER",
                                                                "partenon_output"), MEGARA_PARTENON_FOLDER_partenon_input(
                                                                    "MEGARA_PARTENON_FOLDER",
                                                                    "partenon_input"), MEGARA_PARTENON_FOLDER_megara_input(
                                                                        "MEGARA_PARTENON_FOLDER",
                                                                        "megara_input"), MEGARA_PARTENON_FOLDER_megara_output(
                                                                            "MEGARA_PARTENON_FOLDER",
                                                                            "megara_output"), MEGARA_PARTENON_FOLDER_megara_input_tratados(
                                                                                "MEGARA_PARTENON_FOLDER",
                                                                                "megara_input_tratados"), MEGARA_PARTENON_FOLDER_megara_start_post_fisxml(
                                                                                    "MEGARA_PARTENON_FOLDER",
                                                                                    "megara_start_hour_reinicio_post_fisxml"), MEGARA_PARTENON_FOLDER_megara_output_tratados(
                                                                                        "MEGARA_PARTENON_FOLDER",
                                                                                        "megara_output_tratados"), MEGARA_PARTENON_FOLDER_partenon_input_tratados(
                                                                                            "MEGARA_PARTENON_FOLDER",
                                                                                            "partenon_input_tratados"), MEGARA_PARTENON_FOLDER_partenon_output_tratados(
                                                                                                "MEGARA_PARTENON_FOLDER",
                                                                                                "partenon_output_tratados"), MEGARA_PARTENON_FOLDER_IN_FISCAL_DATA(
                                                                                                    "MEGARA_PARTENON_FOLDER",
                                                                                                    "partenon.fiscal.data.in"), MEGARA_PREVIOUS_DAYS_SINCE_SENTS_IN_CURRENT_FILE(
                                                                                                        "MEGARA_NACIONAL",
                                                                                                        "megara.previous.days.since.sents.in.current.file"), MEGARA_AUTOUPDATE_FLAGLIQ(
                                                                                                            "MEGARA_NACIONAL",
                                                                                                            "megara.autoupdate.flagliq"), MEGARA_NACIONAL_begin_hour_megara_nacional(
                                                                                                                "MEGARA_NACIONAL",
                                                                                                                "begin_hour_megara_nacional"), MEGARA_NACIONAL_end_hour_megara_nacional(
                                                                                                                    "MEGARA_NACIONAL",
                                                                                                                    "end_hour_megara_nacional"), MAIL_CONFIRM_CC(
                                                                                                                        "MAIL_CONFIRM",
                                                                                                                        "CC"), MAIL_CONFIRM_MAIL_CONFIRM(
                                                                                                                            "MAIL_CONFIRM",
                                                                                                                            "MAIL_CONFIRM"), MAIL_CONFIRM_MESSAGE_BODY(
                                                                                                                                "MAIL_CONFIRM",
                                                                                                                                "MESSAGE_BODY"), MAIL_CONFIRM_FROM(
                                                                                                                                    "MAIL_CONFIRM",
                                                                                                                                    "FROM"), MAIL_CONFIRM_SUBJECT(
                                                                                                                                        "MAIL_CONFIRM",
                                                                                                                                        "SUBJECT"), MAIL_CONFIRM_start_hour_broker(
                                                                                                                                            "MAIL_CONFIRM",
                                                                                                                                            "start_hour_broker"), MAIL_CONFIRM_end_hour_broker(
                                                                                                                                                "MAIL_CONFIRM",
                                                                                                                                                "end_hour_broker"), MARGENES_MEGARA_MARGEN_IMSVBEU(
                                                                                                                                                    "MARGENES",
                                                                                                                                                    "MEGARA_MARGEN_IMSVBEU"), MARGENES_PARTENON_MARGEN_IMSVBEU(
                                                                                                                                                        "MARGENES",
                                                                                                                                                        "PARTENON_MARGEN_IMSVBEU"), MAIL_SESSION_WEBSHPERE_RESOURCE(
                                                                                                                                                            "RESOURCE",
                                                                                                                                                            "resource.websphere.session.mail"),

    MTF_NETEO("MTF", "neteo"), MTF_MAIL_SEND_HOUR_BEGIN("MTF",
        "national.mail.mtf.send.hour.begin"), MTF_MAIL_SEND_HOUR_END("MTF",
            "national.mail.mtf.send.hour.end"), MTF_MAIL_SEND_DAYS("MTF",
                "national.mail.mtf.send.days"), MTF_MAIL_SEND_STATUS("MTF",
                    "national.mail.mtf.send.status"), MTF_MAIL_SEND_TO("MTF",
                        "national.mail.mtf.send.to."), MTF_MAIL_SEND_CC("MTF",
                            "national.mail.mtf.send.cc."), MTF_MAIL_SEND_CCO("MTF",
                                "national.mail.mtf.send.cco."), MTF_MAIL_SEND_FROM("MTF",
                                    "national.mail.mtf.send.from"), MTF_MAIL_SEND_SUBJECT("MTF",
                                        "national.mail.mtf.send.subject"), MTF_MAIL_SEND_BODY("MTF",
                                            "national.mail.mtf.send.body"), MTF_MAIL_SEND_PATH("MTF",
                                                "national.mail.mtf.path"),

    PARTENON_FILCAL_DATA_MAIL_SEND_TO("PARTENON_FILCAL_DATA",
        "partenon.receive.fiscal.data.mail.send.to."), PARTENON_FILCAL_DATA_MAIL_SEND_CC("PARTENON_FILCAL_DATA",
            "partenon.receive.fiscal.data.mail.send.cc."), PARTENON_FILCAL_DATA_MAIL_SEND_CCO("PARTENON_FILCAL_DATA",
                "partenon.receive.fiscal.data.mail.send.cco."), PARTENON_FILCAL_DATA_MAIL_SEND_FROM(
                    "PARTENON_FILCAL_DATA",
                    "partenon.receive.fiscal.data.mail.send.from"), PARTENON_FILCAL_DATA_MAIL_SEND_SUBJECT(
                        "PARTENON_FILCAL_DATA",
                        "partenon.receive.fiscal.data.mail.send.subject"), PARTENON_FILCAL_DATA_MAIL_SEND_BODY(
                            "PARTENON_FILCAL_DATA", "partenon.receive.fiscal.data.mail.send.body"),

    PARTENON_FISCAL_DATA_FILE_NAME_INPUT("PARTENON_FILCAL_DATA",
        "partenon.receive.fiscal.data.file.name.input"), PARTENON_FISCAL_DATA_FILE_NAME_MARK("PARTENON_FILCAL_DATA",
            "partenon.receive.fiscal.data.file.name.mark"), PARTENON_FISCAL_DATA_FILE_CHARSET_ENCODING(
                "PARTENON_FILCAL_DATA", "partenon.receive.fiscal.data.file.charset.encoding"), SVB_CIF("SVB",
                    "svb.cif"), XML_IDESI("XML_TR", "transaction.reporting.id.entidad"), XML_TR_SEND_HOUR_BEGIN(
                        "XML_TR", "transaction.reporting.send.hour.begin"), XML_TR_SEND_HOUR_END("XML_TR",
                            "transaction.reporting.send.hour.end"), XML_TR_SEND_LIST_MAIL("XML_TR",
                                "transaction.reporting.send.list.mail."), XML_FOLDER_OUTPUT("XML_TR",
                                    "xml_output"), XSD_FOLDER("XML_TR", "xsd_folder"),

    REPLICATE_TGRL("REPLICATE", "TGRL0XXX"), REPLICATE_START_HOUR("REPLICATE", "start_hour"), REPLICATE_END_HOUR(
        "REPLICATE", "end_hour"), SCRIPT_DIVIDEN("SCRIPT_DIVIDEN", ""), SCRIPT_DIVIDEN_USER_CONFIG("SCRIPT_DIVIDEN",
            "script.dividen.config.user"), SCRIPT_DIVIDEN_ADMIN_CONFIG("SCRIPT_DIVIDEN",
                "script.dividen.config.admin"), SCRIPT_DIVIDEN_LAST_USER_CONFIG("SCRIPT_DIVIDEN",
                    "script.dividen.config.user.last.user.config"), SCRIPT_DIVIDEN_LAST_TIME_CONFIG("SCRIPT_DIVIDEN",
                        "script.dividen.config.user.last.time.config"),
    // SCRIPT_DIVIDEN_IMCAMBIO_CONFIG("SCRIPT_DIVIDEN","script.dividen.config.user.imcambio","script.dividen.config.user.imcambio"),
    // SCRIPT_DIVIDEN_TPOPEBOL_CONFIG("SCRIPT_DIVIDEN","script.dividen.config.user.tpopebol","script.dividen.config.user.tpopebol"),
    SCRIPT_DIVIDEN_NUM_ORDERS_CONFIG("SCRIPT_DIVIDEN",
        "script.dividen.config.user.num.orders"), SCRIPT_DIVIDEN_ALIAS_CONFIG("SCRIPT_DIVIDEN",
            "script.dividen.config.user.alias"), SCRIPT_DIVIDEN_BROKER_CONFIG("SCRIPT_DIVIDEN",
                "script.dividen.config.user.broker"), SCRIPT_DIVIDEN_FETRADE_CONFIG("SCRIPT_DIVIDEN",
                    "script.dividen.config.user.fetrade"), SCRIPT_DIVIDEN_CDISIN_CONFIG("SCRIPT_DIVIDEN",
                        "script.dividen.config.user.cdisin"), SCRIPT_DIVIDEN_COMPLETED_CONFIG("SCRIPT_DIVIDEN",
                            "script.dividen.config.user.is.completed.configuration"), SCRIPT_DIVIDEN_LINES_FILE_CONFIG(
                                "SCRIPT_DIVIDEN",
                                "script.dividen.config.user.num.lines.file"), SCRIPT_DIVIDEN_SEND_PATH_CONFIG(
                                    "SCRIPT_DIVIDEN",
                                    "script.dividen.config.admin.send.path"), SCRIPT_DIVIDEN_HOST_NAME_CONFIG(
                                        "SCRIPT_DIVIDEN",
                                        "script.dividen.config.admin.host.name"), SCRIPT_DIVIDEND_SEND_ALL_FILES_AT_ONCE(
                                            "SCRIPT_DIVIDEN", "script.dividen.config.user.send.all.files.at.once"),

    SIBBAC_READ_XML_PATH("SIBBAC_READ", "sibbac.read.xml.path"), ECONOMIC_CUPON_ISIN_REGEX("ECONOMIC",
        "economic.cupon.isin.regex"), ECONOMIC_CUPON_MANAGEMENT_RIGHTS("ECONOMIC",
            "economic.cupon.imderges"), ECONOMIC_CUPON_EFFECTIVE_LESS_THAN("ECONOMIC",
                "economic.cupon.imefeagr.less.than.value"), ECONOMIC_SPLIT_TMCT0ALO_IMTOTBRU("ECONOMIC",
                    "economic.split.tmct0alo.imtotbru"), ECONOMIC_SHARE_OUT_FIRST_DATE("ECONOMIC",
                        "economic.share.out.first.date"), ECONOMIC_SHARE_OUT_LAST_DATE("ECONOMIC",
                            "economic.share.out.last.date"),

    MIFID_FOLDER_input("MIFID", "mifid_input"), MIFID_FOLDER_input_tratados("MIFID",
        "mifid_input_tratados"), MIFID_RECEPTION_HOUR_BEGIN("MIFID",
            "mifid.reception.hour.begin"), MIFID_RECEPTION_HOUR_END("MIFID",
                "mifid.reception.hour.end"), FIDESSA_SEND_HOUR_BEGIN("FIDESSA",
                    "fidessa.reception.hour.begin"), FIDESSA_SEND_HOUR_END("FIDESSA", "fidessa.reception.hour.end"),

    SIBBAC_MAIL_PROCESS_CONTROL_SEND_TO("PROCESSCONTROL",
        "sibbac.management.processcontrol.send.to."), SIBBAC_MAIL_PROCESS_CONTROL_SEND_CC("PROCESSCONTROL",
            "sibbac.management.processcontrol.send.cc."), SIBBAC_MAIL_PROCESS_CONTROL_SEND_CCO("PROCESSCONTROL",
                "sibbac.management.processcontrol.send.cco."), SIBBAC_MAIL_PROCESS_CONTROL_SEND_FROM("PROCESSCONTROL",
                    "sibbac.management.processcontrol.send.from"),

    RULES_FOLDER_INPUT("RULES_FOLDER", "sibbac.rules.folder"),

    PTI_FECHA_INICIO_VERSION_MENSAJERIA_PTI_T2S("PTI", "pti.message.t2s.version.init.date"),

    PTI_ESTADOS_ECC_ACTIVOS("PTI", "pti.mo.ecc.status.active"), PTI_ESTADOS_ASIGNACION_BLOQUEAN_ACTIVIDAD_USUARIO("PTI",
        "pti.cdestadoasig.lock.user.activity"), PTI_ESTADOS_ASIGNACION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO("PTI",
            "pti.cdestadoasig.gt.lock.user.activity"), PTI_ESTADOS_TITULARIDAD_BLOQUEAN_ACTIVIDAD_USUARIO("PTI",
                "pti.cdestadotit.lock.user.activity"), PTI_ESTADOS_TITULARIDAD_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO(
                    "PTI",
                    "pti.cdestadotit.gt.lock.user.activity"), PTI_ESTADOS_ENTREGA_RECEPCION_MAYOR_QUE_BLOQUEAN_ACTIVIDAD_USUARIO(
                        "PTI", "pti.cdestadoentrec.gt.lock.user.activity"),

    PTI_ESCALADOBOK_MINUTOS_NO_ACTIVOS("ESCALADOBOK", "MINUTOS"), PTI_ESCALADOALO_MINUTOS_NO_ACTIVOS("ESCALADOALO",
        "MINUTOS"), PTI_ESCALADOALC_MINUTOS_NO_ACTIVOS("ESCALADOALC", "MINUTOS"), PTI_ESCALADOBOKTIT_MINUTOS_NO_ACTIVOS(
            "ESCALADOBOKTIT", "MINUTOS"), PTI_ESCALADOALOTIT_MINUTOS_NO_ACTIVOS("ESCALADOALOTIT",
                "MINUTOS"), PTI_ESCALADOALCTIT_MINUTOS_NO_ACTIVOS("ESCALADOALCTIT", "MINUTOS"), PTI_CAMARA_ECC("PTI",
                    "CAMARAECC"), PTI_ASIGNACION_MAX_MO_R01("PTI", "pti.asignacion.max.mo.r01"),

    PTI_APPIDENTITYDATA_ENTIDAD("PTI", "pti.appidentitydata.entidad"), PTI_APPIDENTITYDATA_ORIGEN("PTI",
        "pti.appidentitydata.origen"), EUROCCP_IDECC("EUROCCP",
            "euroccp.idecc"), EUROCCP_FTL_DIAS_LIMITE_ENVIO_TITULARES("EUROCCP",
                "euroccp.ftl.dias.limite.envio.titulares"), PTI_IDECC("PTI",
                    "pti.idecc"), PTI_FTL_DIAS_LIMITE_ENVIO_TITULARES("PTI",
                        "pti.ftl.dias.limite.envio.titulares"), PTI_SEGMENTOECC("PTI",
                            "pti.segmentoecc"), PTI_IDCRITERIOCOMTITULAR("PTI",
                                "pti.idcriteriocomtitular"), PTI_INDICADOREXENTA("PTI",
                                    "pti.indicadorexenta"), SBRMV_PTI_TXT_NUMBER_OF_MESSAGES("PTI",
                                        "process.pti.txt.numberofmsg"), SBRMV_PTI_TXT_RELOAD_TIME("PTI",
                                            "process.pti.txt.reloadtime"), PTI_IMPUTACIONES_S3_DELAY("IMPUTACIONES",
                                                "pti.process.works.imputacion"), PTI_IMPUTACIONES_S3_FULL_EXECUTE(
                                                    "IMPUTACIONES",
                                                    "pti.process.works.imputacion.s3.full.execute"), PTI_IMPUTACIONES_S3_EMERGENCY_STOP(
                                                        "IMPUTACIONES",
                                                        "pti.process.works.imputacion.s3.emergency.stop"), PTI_IMPUTACIONES_HORAINI(
                                                            "IMPUTACIONES",
                                                            "HORAINI"), PTI_IMPUTACIONES_HORAINI_EUROCCP("IMPUTACIONES",
                                                                "HORAINI_EUROCCP"), PTI_IMPUTACIONES_HORAINI_MAB(
                                                                    "IMPUTACIONES",
                                                                    "HORAINI_MAB"), PTI_IMPUTACIONES_HORAEND(
                                                                        "IMPUTACIONES",
                                                                        "HORAEND"), PTI_IMPUTACIONES_HORAEND_EUROCCP(
                                                                            "IMPUTACIONES",
                                                                            "HORAEND_EUROCCP"), PTI_IMPUTACIONES_HORAEND_MAB(
                                                                                "IMPUTACIONES",
                                                                                "HORAEND_MAB"), PTI_IMPUTACIONES_DAILY_DEACTIVATE(
                                                                                    "IMPUTACIONES", "DAILY_DEACTIVATE"),

    PTI_DESGLOSE_AUTOMATICO_SCAN_DAYS_PREV("PTI",
        "pti.automatic.allocate.scan.days.previous"), PTI_ASIGNACION_DESDE_ORDEN_SCAN_DAYS_PREV("PTI",
            "pti.assignment.from.order.scan.days.previous"), PTI_IMPUTACION_PROPIA_INIT_HOUR("PTI",
                "pti.own.imputation.init.hour"), PTI_USUARIO_ORIGEN_IMPUTACION_PROPIA("PTI",
                    "pti.own.imputacion.origin.user"), PTI_IMPUTACION_PROPIA_SCAN_DAYS_PREV("PTI",
                        "pti.own.imputation.scan.days.previous"),

    PTI_ENVIO_RECTIFICACION_IN_DAY("PTI", "pti.holder.rectification.in.day"), PTI_ENVIO_RT_RECTIFICACION_SCAN_DAYS_PREV(
        "PTI", "pti.send.holder.rectification.rt.scan.days.previous"), PTI_ENVIO_RT_SCAN_DAYS_PREV("PTI",
            "pti.send.holder.rt.scan.days.previous"), PTI_ENVIO_TI_RECTIFICACION_SCAN_DAYS_PREV("PTI",
                "pti.send.holder.rectification.ti.scan.days.previous"), PTI_ENVIO_TI_SCAN_DAYS_PREV("PTI",
                    "pti.send.holder.ti.scan.days.previous"),

    PTI_ENVIO_CANCELACION_SCAN_DAYS_PREV("PTI", "pti.cancel.scan.days.previous"),

    PTI_CHECK_ESTADOS_MESSAGES_IN_ASIGNACION_SCAN_DAYS_PREV("PTI",
        "pti.check.status.assigments.message.in.scan.days.previous"),

    PTI_CHECK_ESTADOS_MESSAGES_IN_TITULARES_SCAN_DAYS_PREV("PTI",
        "pti.check.status.holders.message.in.scan.days.previous"),

    PTI_ENVIO_TRASPASOS_SCAN_DAYS_PREV("PTI", "pti.send.transfer.scan.days.previous"),

    PTI_CASE_SCAN_DAYS_PREV("PTI", "pti.case.scan.days.previous"), PTI_DATABASE_MESSAGE_PROCESS_SCAN_DAYS_PREV("PTI",
        "pti.database.message.process.scan.days.previous"), PTI_DATABASE_MESSAGE_PROCESS_MESSAGE_TYPES_IN_ORDER("PTI",
            "pti.database.message.process.message.types.in.order"), PTI_DATABASE_MESSAGE_SEND_ORDER("PTI",
                "pti.databse.message.send.order"), PTI_DATABASE_MESSAGE_SEND_AUTOMATIC_SCAN_DAYS_PREV("PTI",
                    "pti.database.message.send.automatic.scan.days.previous"), PTI_DATABASE_MESSAGE_SEND_AUTOMATIC_INIT_HOUR(
                        "PTI", "pti.database.message.send.automatic.init.hour"), PTI_ENVIO_CE_SCAN_DAYS_PREV("PTI",
                            "pti.send.ce.scan.days.previous"), PTI_ENVIO_MO_SCAN_DAYS_PREV("PTI",
                                "pti.send.mo.scan.days.previous"), PTI_ENVIO_MO_SCRIPT_DIVIDEND_BATCH_MESSAGES("PTI",
                                    "pti.send.mo.script.dividend.batch.messages"), PTI_CDOPERACIONMKT_SIN_ECC("PTI",
                                        "pti.cdoperacionmkt.sin.ecc"), PTI_ENVIO_FICHERO_CC_FTL_D1_LIMIT_HOUR("PTI",
                                            "pti.envio.fichero.cc.ftl.d1.limit.hour"), PTI_ENVIO_FICHERO_CC_FINAL_DAY_FILE_LIMIT_HOUR(
                                                "PTI", "pti.envio.fichero.cc.final.day.file.limit.hour"),

    CONFIRMACION_MINORISTA_SCAN_DAYS_PREV("SIBBAC20", "confirmacion.minorista.scan.days.previous"),

    PTI_RT_REVISA_CE("PTI", "pti.rt.mark.ce"), PTI_TI_REVISA_CE("PTI", "pti.ti.mark.ce"),

    SIBBAC20_FALLIDOS_MANUAL_DATE("SIBBAC20", "megara.clearingnetting.manual.date"),

    SIBBAC20_SPB_HORAS_EJECUCION("FICHERO_SPB", "envio.1819.horas.ejecucion"), FICHERO_SPB_ALIAS_CODE("FICHERO_SPB",
        "alias.code"), FICHERO_SPB_ALIAS_CODE_CTM("FICHERO_SPB", "alias.code.ctm"),

    CONTABILIDAD_AUXILIAR_CONTABLE_ROUTING("CONTABILIDAD", "clientes.devengo.routing.auxiliar"),
    
    ESTATICOS_CAMPOS_MIFID_FLAG("ESTATICOS", "estaticos.envio.campos.mifid"),
    
    ALIAS_OPERACIONES_NO_COBRABLES("ALIAS_OPERACIONES_NO_COBRABLES", "alias.operaciones.no.cobrables"),
    DATE_INI_OPERACIONES_NO_COBRABLES("DATE_INI_OPERACIONES_NO_COBRABLES", "date.ini.operaciones.no.cobrables"), 
    OPERACIONES_NO_COBRABLES_REF_ASIGNACION("OPERACIONES_NO_COBRABLES_REF_ASIGNACION", "operaciones.no.cobrables.ref.asignacion"), 
    OPERACIONES_NO_COBRABLES_MIEMBROS_COMP_DESTINO("OPERACIONES_NO_COBRABLES_MIEMBROS_COMP_DESTINO", "operaciones.no.cobrables.miembros.comp.destino"), 
    OPERACIONES_NO_COBRABLES_EST_CORRETAJE("OPERACIONES_NO_COBRABLES_EST_CORRETAJE", "operaciones.no.cobrables.est.corretaje"), 
    OPERACIONES_NO_COBRABLES_ESTADOS_CANON("OPERACIONES_NO_COBRABLES_ESTADOS_CANON", "operaciones.no.cobrables.estados.canon");     ;


    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

    /**
     * Campo <code>PROCESS</code> de la tabla <code>TMCT0CFG</code> para las
     * búsquedas.
     */
    private String process;

    /**
     * Campo <code>KEYNAME</code> de la tabla <code>TMCT0CFG</code> para las
     * búsquedas.
     */
    private String keyname;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

    /** Constructor I. Inicializa los atributos de la clase. */
    private Tmct0cfgConfig(String process, String keyname) {
      this.process = process;
      this.keyname = keyname;
    } // Tmct0cfgConfig

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS SOBRESCRITOS

    @Override
    public String toString() {
      return "Tmct0cfgConfig [process='" + process + "' keyname=" + keyname + "']";
    } // toString

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GETTERS

    public String getProcess() {
      return process;
    } // getProcess

    public String getKeyname() {
      return keyname;
    } // getKeyname
  } // Tmct0cfgConfig

  public static enum TypeCdtipest {
    TMCT0ORD("TMCT0ORD"), TMCT0BOK("TMCT0BOK"), TMCT0ALO("TMCT0ALO"), TMCT0EJE("TMCT0EJE"), TMCT0GRE(
        "TMCT0GRE"), TMCT0GAE("TMCT0GAE"), TMCT0GAL("TMCT0GAL"), TMCT0CTO("TMCT0CTO"), TMCT0CTG("TMCT0CTG"), TMCT0CTA(
            "TMCT0CTA"), TMCT0CTD(
                "TMCT0CTD"), TMCT0MSC("TMCT0MSC"), TMCT0BRI("TMCT0BRI"), TMCT0PKB("TMCT0PKB"), ASIGNACION("ASIGNACION");

    private String value;

    private TypeCdtipest(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  public static enum TypeMsgLoger {
    MSG_500("ALLOCATION, MANUAL ALLOCATE"), MSG_520("ALLOCATION, AUTOMATIC ALLOCATE");

    private String msg;

    private TypeMsgLoger(String msg) {
      this.msg = msg;
    }

    public String getValue() {
      return msg;
    }
  }

  public static enum TypeMarket {
    NACIONAL("MMNN"), EXTRANJERO("MMEE"), MTF_NACIONAL("MTF_NAC"), MTF_EXTRANJERO("MTF_EXT");

    private String value;

    private TypeMarket(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas - Para ir
   * migrando las clases constantes y Constantes Ordenes
   */
  public static enum Constantes {
    MERCADO_NO_ENVIAR_CLIENTE("NOCLIENTE"), PARTENON("0049"), NEW_RECORD("NEW"), DELETE_RECORD("DELETE"), UPDATE_RECORD(
        "UPDATE"),

    CDCOMISN_RECTORA_O_GOVERNING_SOCIETY("R"),

    // FIXME Comentado al pasar esta clase de WAS7 a WAS8
    // REPLICATE_TGRL(String.valueOf(EstadosAsignacion.RECHAZADA)),

    BOOKING_DIRECTIVOS("D"), BOOKING_SEND("S"), BOOKING_MAB("C"),

    ORDER_SCRIPT_DIVIDEND("S")

    ;
    private String value;

    private Constantes(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas de error para
   * ACCOUNTING.
   */
  public static enum ConstantesError {
    ERROR_DIVIDIR_PLANTILLAS("Error al dividir plantillas"), ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX_NULL(
        "No se pudo recuperar el valor de la cuenta para la orden/importe"), ERROR_DIVIDIR_PLANTILLAS_CUENTAS_AUX(
            "No hay mismo número de cuentas y auxiliares en tabla TMCT0_CONTA_PLANTILLAS"), ERROR_QUERY_BLOQUEO(
                "Error al ejecutar query de bloqueo"), ERROR_QUERY_ORDEN(
                    "Error al ejecutar query de orden"), ERROR_QUERY_IMPORTES(
                        "Error al ejecutar query para obtener importes"), ERROR_QUERY_DESBLOQUEO(
                            "Error al ejecutar query de desbloqueo"), ERROR_QUERY_COBRO(
                                "Error al ejecutar query de cobro"), ERROR_QUERY_FINAL(
                                    "Error al ejecutar query final"), ERROR_APUNTES_CONTABLES(
                                        "Error al generar apuntes contables"), ERROR_DESCUADRES(
                                            "Error al generar descuadres"), ERROR_APUNTES_DETALLE(
                                                "Error al insertar apuntes de detalle"), ERROR_ENVIO_MAIL(
                                                    "Error al enviar mail."), ERROR_PROCESAR_FICHERO(
                                                        "Error al procesar fichero"), ERROR_EJECUCION_HILOS(
                                                            "Error al ejecutar hilos"), ERROR_SIN_PLANTILLA(
                                                                "No se ha encontrado la plantilla"), ERROR_PROCESAR_PLANTILLA(
                                                                    "Error al procesar plantilla"), ERROR_REGISTROS_DETALLE_NORMA43(
                                                                        "Error al generar registros de detalle Norma 43"), ERROR_SIN_DATOS_FICHERO_EXCEL(
                                                                            "No se encontraron datos a procesar en el fichero Excel."), ERROR_DATOS_FICHERO_EXCEL(
                                                                                "Error al rellenar datos en el fichero Excel."), ERROR_GENERACION_FICHERO(
                                                                                    "Error en la generación del fichero: {0}");

    private String value;

    private ConstantesError(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas de tipo de
   * queries para ACCOUNTING.
   */
  public static enum ConstantesQueries {
    CANTIDAD_REGISTROS_OBTENIDOS("Cantidad de registros obtenidos "), QUERY_APUNTE_DETALLE(
        "Query_Apunte_Detalle"), QUERY_BLOQUEO("Query_Bloqueo"), QUERY_COBRO(
            "Query_Cobro"), QUERY_DESBLOQUEO("Query_Desbloqueo"), QUERY_ORDEN("Query_Orden");

    private String value;

    private ConstantesQueries(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas de persistencia
   * para ACCOUNTING.
   */
  public static enum ConstantesPersistencia {
    CREADO_APUNTE_ID("Se ha creado apunte contable con ID: "), CREADO_APUNTE_DIFERENCIA_ID(
        "Se ha creado apunte contable diferencia con ID: "), CREADO_DESCUADRE_ID(
            "Se ha creado descuadre con ID: "), CREADO_NORMA43_DETALLE_ID(
                "Se ha creado registro Detalle Norma43 con ID: "), COBRO_PROCESADO_ID_MOVIMIENTO(
                    "Pasado cobro a procesado con idMovimiento = ");

    private String value;

    private ConstantesPersistencia(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase enumerada que contiene los tipos de nacionalidad.
   */
  public static enum TipoNacionalidad {
    NACIONAL("N"), EXTRANJERO("E"), NACIONAL_COD_ISO("724"), NACIONAL_COD_OTHER("011");
    private String value;

    private TipoNacionalidad(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }
  } // TipoNacionalidad

  /**
   * FISICA, JURIDICA
   */
  public static enum TipoIdentificacion {
    FISICA("F"), JURIDICA("J");
    private String value;

    private TipoIdentificacion(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * NIF, NIE, CIF, BIC, OTROS
   * 
   */
  public static enum TipoCodigoIdentificacion {
    NIF("N"), NIE("E"), CIF("C"), BIC("B"), OTROS("O");
    private String value;

    private TipoCodigoIdentificacion(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas para XML
   * TRANSACTION REPORTING
   */
  public static enum ConstantesXML_TR {
    ID_ESI("BSCHESMMXXX"),
    XML_TR("250"),
    COD_TRAMITE("CDT"),
    VERSION_XML("02.40"),
    TIPO_ESI("B"),
    NOTACION_CANTIDAD("U"),
    ID_HORARIO("+01"),
    CDTYP("T"),
    CAPACIDAD_NEGOCIACION("A"),
    START_NIE1("X"),
    START_NIE2("K"),
    START_3("Z"),
    CIF(TipoCodigoIdentificacion.CIF.getValue()),
    NIE(TipoCodigoIdentificacion.NIE.getValue()),
    NIF(TipoCodigoIdentificacion.NIF.getValue()),
    OTRO(TipoCodigoIdentificacion.OTROS.getValue()),
    
    BIC(TipoCodigoIdentificacion.BIC.getValue()), COMPRA("B"), VENTA("S"),
    // BIC("BIC-"),
    MIC("MIC-")

    ;
    private String value;

    private ConstantesXML_TR(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene los estados para Transaction Reporting
   */
  public static enum StatusXML_TR {
    XML_SEND("010", "XML Transaction Reporting Enviado"), XML_NO_SEND("050",
        "XML Transaction Reporting Pendiente Enviar"), XML_ERROR("000",
            "XML Transaction Reporting con Errores"), XML_CONFIRM("040",
                "XML Transaction Reporting Confirmado"), OPERACION_MODIFICADA("M",
                    "Operacion Modificada"), OPERACION_NUEVA("A",
                        "Alta de Operacion"), OPERACION_REJECT("R", "Reject Operacion"),;

    private String status, descripcion;

    private StatusXML_TR(String _status, String _descripcion) {
      status = _status;
      descripcion = _descripcion;
    }

    public String getStatus() {
      return status.trim();
    }

    public String getDescripcion() {
      return descripcion.trim();
    }

    @Override
    public String toString() {
      return "<statusxml_tr=\"" + getStatus() + "\" keyname=\"" + getDescripcion() + "\"/>";
    }
  }

  /**
   * Clase de Enumerados que contiene los estados para Transaction Reporting
   */
  public static enum StatusHostScriptDividen {
    ACTIVE("A", "script.dividen.config.admin.node.status.a"), READY_TO_SEND("P",
        "script.dividen.config.admin.node.status.p"), SENDING("S",
            "script.dividen.config.admin.node.status.s"), SENT_OK("C",
                "script.dividen.config.admin.node.status.c"), SENT_ERR("R",
                    "script.dividen.config.admin.node.status.r"),;

    private String status, msgbundlekey;

    private StatusHostScriptDividen(String _status, String _msgbundlekey) {
      status = _status;
      msgbundlekey = _msgbundlekey;
    }

    public String getStatus() {
      return status.trim();
    }

    public String getMsgbundlekey() {
      return msgbundlekey.trim();
    }

    @Override
    public String toString() {
      return "<StatusHostScriptDividen status=\"" + getStatus() + "\" keyname=\"" + getMsgbundlekey() + "\"/>";
    }
  }

  public static enum TipoTitular {
    TITULAR("T"), REPRESENTANTE("R"), USUFRUTUARIO("U"), SIN_USUFRUCTO("N"), MENOR("D");
    private String value;

    private TipoTitular(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  public static enum TipoIndicadorExenta {
    EXENTA("E", "EXENTA"), RETROCESION("R", "RETROCESION OPERACION DECLARADA EXENTA");
    private String value, description;

    private TipoIndicadorExenta(String _value, String _description) {
      this.value = _value;
      this.description = _description;
    }

    public String getValue() {
      return value;
    }

    public String getDescription() {
      return description;
    }
  }

  public static enum TipoComision {
    RECTORA_GOBERNING_SOCIETY("R", "PQ_3520_Rectora"), SOCIETY("S", "PQ_3520_Sociedad"), ORDER_MODE("O",
        "PQ_3520_Modo_Orden");
    private String value, description;

    private TipoComision(String value, String description) {
      this.value = value;
      this.description = description;
    }

    public String getValue() {
      return value;
    }

    public String getDescription() {
      return description;
    }
  }

  /**
   * Client Pay Rigth values 'S','N'
   */
  public static enum TipoClienteAsumeGastos {
    SI("S", "TIPO_CLIENTE_ASUME_GASTOS_SI"), NO("N", "TIPO_CLIENTE_ASUME_GASTOS_NO");
    private String value, description;

    private TipoClienteAsumeGastos(String value, String description) {
      this.value = value;
      this.description = description;
    }

    public String getValue() {
      return value;
    }

    public String getDescription() {
      return description;
    }
  }

  public static enum TypeLoadExcelOperation {
    NEW("A"), UPDATE("M"), DELETE("B");

    private String value;

    private TypeLoadExcelOperation(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas para MIFID
   */
  public static enum ConstantesMifid {
    MIFID_CATEGORY("260"), MIFID_INPUT_PARAM("MIFID_INPUT"), MIFID_INPUT_PARAM_TRATADOS(
        "MIFID_INPUT_TRATADOS"), MIFID_USER("MIFID"), MIFID_PREFIX_FILE("MIFIDCATEGORIA")

    ;
    private String value;

    private ConstantesMifid(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * Clase de Enumerados que contiene los Tipos de Categorias MIFID
   */
  public static enum Mifid_Categories {
    MI("MI", "R"), CE("CE", "C"), PF("PF", "P"), DEFAULT(" ", "R");

    private String type, type_cnv;

    private Mifid_Categories(String _status, String _type_cnv) {
      type = _status;
      type_cnv = _type_cnv;
    }

    public String getType() {
      return type.trim();
    }

    public String getType_cnv() {
      return type_cnv.trim();
    }

    @Override
    public String toString() {
      return "<Mifid_Categories=\"" + getType() + "\" keyname=\"" + getType_cnv() + "\"/>";
    }
  }

  /**
   * Clase de Enumerados que contiene las constantes genericas para FIDESSA
   */
  public static enum ConstantesFidessa {
    FIDESSA_SEND("270"), FIDESSA_USER("FIDESSA")

    ;
    private String value;

    private ConstantesFidessa(String _value) {
      value = _value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /**
   * 
   * @author xIS15780</BR>
   * El campo NBCAMXML de la tabla TMCT0XAS es de 10 posiciones condicionando el
   * tamaño del nombre.</BR>
   * <B>Nomenclatura:</B> </BR>
   * Consta de una parte Fija MEGARAAC (MEGARA ACCOUNT TO CLEARER) y dos
   * posiciones variables XY</BR>
   * X-> Valores Y o N (Yes,No) Se envía o no se envía si cumple las
   * condiciones.</BR>
   * Y-> Valores P,C,S(Prefix,Conteins,Suffix), si comienza, contiene o
   * termina.</BR>
   * Ejemplos MEGARAAC<B>S</B>->Significa que se marcará para enviar, si
   * coincide todo el Acount to Clearer.</BR>
   * MEGARAAC<B>SP</B>->Significa Se marcará para enviar si tiene el
   * prefijo</BR>
   * </BR>
   * 
   * MEGARA_ACCTATCLE_SI -> Si está el Account to Clearer, se marcará para
   * enviar a Megara</BR>
   * MEGARA_ACCTATCLE_NO -> Si está el Account to Clearer al completo No se
   * marcará para enviar a Megara</BR>
   * MEGARA_ACCTATCLE_SI_PREFIX -> Si el Account to Clearer comienza por el
   * prefijo, se marcará para enviar.</BR>
   * MEGARA_ACCTATCLE_NO_PREFIX -> Si el Account to Clearer comienza por el
   * prefijo, No se marcará para enviar.</BR>
   * MEGARA_ACCTATCLE_SI_CONTEINS -> Si el Account to Clearer contiene el valor
   * se marcará para enviar.</BR>
   * MEGARA_ACCTATCLE_NO_CONTEINS -> Si el Account to Clearer contiene el valor
   * No se marcará para enviar.</BR>
   * MEGARA_ACCTATCLE_SI_SUFFIX -> Si el Account to Clearer termina por el
   * valor, se marcará para enviar.</BR>
   * MEGARA_ACCTATCLE_NO_SUFFIX -> Si el Account to Clearer temina por el valor,
   * No se marcará para enviar.</BR>
   */
  public static enum TypeConfigAcctacleSendMegara {
    MEGARA_ACCTATCLE_YES("MEGARAACY"), MEGARA_ACCTATCLE_NO("MEGARAACN"), MEGARA_ACCTATCLE_YES_PREFIX(
        "MEGARAACYP"), MEGARA_ACCTATCLE_NO_PREFIX("MEGARAACNP"), MEGARA_ACCTATCLE_YES_CONTEINS(
            "MEGARAACYC"), MEGARA_ACCTATCLE_NO_CONTEINS(
                "MEGARAACNC"), MEGARA_ACCTATCLE_YES_SUFFIX("MEGARAACYS"), MEGARA_ACCTATCLE_NO_SUFFIX("MEGARAACNS");

    private String value;

    private TypeConfigAcctacleSendMegara(String value) {
      this.value = value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  public static enum TypeMessageAud {
    SI("SI"), TR("TR"), CP("CP"), VR("VR"), PT("PT"), EX("EX"), CO("CO");
    private String value;

    private TypeMessageAud(String value) {
      this.value = value;
    }

    public String getValue() {
      return value.trim();
    }
  }

  /** Tipos de movimientos fallidos. */
  public enum MovimientosFallidos {
    AFA("AFA", "ALTA AFECTACION FALLIDO"), BFA("BFA", "BAJA AFECTACION FALLIDO"), APU("APU", "ALTA PRESTAMO PUI"), BPU(
        "BPU", "BAJA PRESTAMO PUI"), ACO("ACO", "ALTA COLATERAL PRESTAMO"), BCO("BCO",
            "BAJA COLATERAL PRESTAMO"), INC("INC", "INTERESES COLATERAL"), INP("INP", "INTERESES PRESTAMO"), RPA("RPA",
                "RECOMPRA"), APA("APA", "ANULACION COMPRA"), CPU("CPU", "CANCELACION PUI EN EFECTIVO"), RPE("RPE",
                    "RECOMPRA EN EFECTIVO"), CCP("CCP", "COMISION CONSTITUCION PRESTAMO"), CAR("CAR",
                        "COSTE ADMINISTRATIVO RECOMPRA"), PDF("PDF", "PENALIZACION DIARIA FALLIDO"),

    ADE("ADE", "AJUSTE DISTRIBUCION EFECTIVO"), EDE("EDE", "ENTREGA DISTRIBUCION EN EFECTIVO"), ADV("ADV",
        "AJUSTE DISTRIBUCION VALORES"), EDV("EDV", "ENTREGA DISTRIBUCION VALORES"), ADP("ADP",
            "AJUSTE DISTRIBUCION PRESTAMO"), APJ("APJ", "ALTA PRESTAMO DE AJUSTE"), DPA("DPA",
                "DEVOLUCION PRESTAMO DE AJUSTE"), BPC("BPC", "BAJA PRESTAMO POR CONVERSION"), ACP("ACP",
                    "ALTA CONVERSION PRESTAMO"), FDV("FDV",
                        "FRACCIONES DISTRIBUCION VALORES"), AFV("AFV", "AJUSTE FRACCIONES DISTRIBUCION VALORES");

    /** Tipo de operación. */
    private String tipo;

    /** Descripción de la operación. */
    private String descripcion;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param tipo Tipo de operación.
     * @param descripcion Descripción de la operación.
     */
    private MovimientosFallidos(String tipo, String descripcion) {
      this.tipo = tipo;
      this.descripcion = descripcion;
    } // MovimientosFallidos

    public String getTipo() {
      return tipo;
    } // getTipo

    public String getDescripcion() {
      return descripcion;
    } // getDescripcion

  } // MovimientosFallidos

  public static enum Tmct0xasConversionKey {
    CDBROKER_CDBOLSAS("CDBROKER", "CDBOLSAS"), CDBROKER_CDMIEMBRO("CDBROKER", "CDMIEMBRO"), CDBROKER_MTF("CDBROKER",
        "MTF"), CDCANAL_CDCANENT("CDCANAL", "CDCANENT"), CDCLEARE_CLEARER("CDCLEARE", "CLEARER"), CDCLENTE_CDORIGEN(
            "CDCLENTE", "CDORIGEN"), CDCUENTA_CDORIGEN("CDCUENTA", "CDORIGEN"), CDORIGEN_CDORIGEN("CDORIGEN",
                "CDORIGEN"), CDTIPORD_TPORDENS("CDTIPORD", "TPORDENS"), CLEARING_PLATFORM_TO_CLEARING_PLATFORM(
                    "CLEARINGPLATFORM", "CLEARINGPLATFORM"), SETTLEMENT_EXCHANGE_CDMERCAD("SettlementExchange",
                        "CDMERCAD"), CDMERCAD_CDMERCAD("CDMERCAD", "CDMERCAD"), CDMERCAD_MTF("CDMERCAD",
                            "MTF"), CDMERCAD_TPMERCAD("CDMERCAD", "TPMERCAD"), CDMERCAD_TPSUBMER("CDMERCAD",
                                "TPSUBMER"), CDTPCAMB_TPCONTRA("CDTPCAMB", "TPCONTRA"), CDTPCAMB_TPCAMBIO("CDTPCAMB",
                                    "TPCAMBIO"), SENTIDO_SIBBAC_SENTIDO_PTI("CDTPOPER",
                                        "SENTIDOPTI"), SENTIDO_PTI_SENTIDO_SIBBAC("SENTIDOPTI",
                                            "CDTPOPER"), SENTIDO_SIBBAC_SENTIDO_EUROCCP("CDTPOPER",
                                                "SENTIDOEUROCCP"), SENTIDO_EUROCCP_SENTIDO_SIBBAC("SENTIDOEUROCCP",
                                                    "CDTPOPER"), IND_CAPACIDAD_PTI_IND_CAPACIDAD("INDCAPACIDADPTI",
                                                        "INDCAPACIDAD"), IND_CAPACIDAD_IND_CAPACIDAD_PTI("INDCAPACIDAD",
                                                            "INDCAPACIDADPTI"), GENPKB_DESGLOSE("GENPKB",
                                                                "DESGLOSE"), DARK_POOL_MIC_TO_MIC("DARKPOOLMIC",
                                                                    "MIC"), TXT_ORS_SIBBAC_TXT_ORS_EUROCCP("TXTSIBBAC",
                                                                        "TXTEUROCCP");

    private final String nbcamxml;
    private final String nbcamas4;

    private Tmct0xasConversionKey(String nbcamxml, String nbcamas4) {
      this.nbcamxml = nbcamxml;
      this.nbcamas4 = nbcamas4;
    }

    public String getNbcamxml() {
      return nbcamxml;
    }

    public String getNbcamas4() {
      return nbcamas4;
    }

    @Override
    public String toString() {
      return MessageFormat.format("nbcamxml:{0}##nbcamas4:{1}", nbcamxml, nbcamas4);
    }

  }

  /** Tipos de apuntes. */
  public static enum TipoApunte {

    DEBE("DEBE"), HABER("HABER");

    /** Tipo de conta plantilla. */
    private String tipo;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param tipo Tipo de conta plantillas.
     */
    private TipoApunte(String tipo) {
      this.tipo = tipo;
    }

    public String getTipo() {
      return tipo;
    }

  }

  public static enum CuentasBancarias {

    CUENTA_COBRO("1021101");

    /** Tipo de conta plantilla. */
    private String cuenta;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param tipo Tipo de conta plantillas.
     */
    private CuentasBancarias(String cuenta) {
      this.cuenta = cuenta;
    }

    public String getCuenta() {
      return cuenta;
    }

  }

  /** Tipos de conta plantillas. */
  public static enum TipoContaPlantillas {

    DEVENGO("DEVENGO"), ANULACION("ANULACION"), COBRO_BARRIDOS("COBRO BARRIDOS"), APUNTE_MANUAL("APUNTE MANUAL"), COBRO(
        "COBRO"), RETROCESION("RETROCESION"), CLIENTES_CLEARING("CLIENTES CLEARING"), PERDIDAS_GANANCIAS(
            "PyG"), DEVENGO_FALLIDOS("DEVENGO FALLIDOS"), EMISION_FACTURA(
                "EMISION FACTURA"), DEVENGO_FACTURAS("DEVENGO FACTURAS"), DOTACION("DOTACION"), DESDOTACION(
                    "DESDOTACION"), LIQUIDACION_FALLIDOS("LIQUIDACION FALLIDOS"), LIQUIDACION_PARCIAL(
                        "LIQUIDACION PARCIAL"), LIQUIDACION_TOTAL("LIQUIDACION TOTAL");

    /** Tipo de conta plantilla. */
    private String tipo;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param tipo Tipo de conta plantillas.
     */
    private TipoContaPlantillas(String tipo) {
      this.tipo = tipo;
    }

    public String getTipo() {
      return tipo;
    }

  }

  /** Tipos de conta plantillas. */
  public static enum SubTipoContaPlantillas {

    EFECTIVO_MERCADO("EFECTIVO MERCADO"), NETO_CLIENTE("NETO CLIENTE");

    /** Tipo de conta plantilla. */
    private String tipo;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param tipo Tipo de conta plantillas.
     */
    private SubTipoContaPlantillas(String tipo) {
      this.tipo = tipo;
    }

    public String getTipo() {
      return tipo;
    }

  }

  /** Tipos de conta plantillas. */
  public static enum TextosSustitucionPlantillas {

    ENTRE("ENTRE:"), DESDE_INICIO_ANIO("DESDE_INICIO_ANIO"), DESDE("DESDE_"), HASTA("HASTA:"), SUSTITUIR_IMPORTE(
        "SUSTITUIR_IMPORTE"), DIAS_A_EJECUTAR("DIAS_A_EJECUTAR:");

    /** Tipo de conta plantilla. */
    private String texto;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param tipo Tipo de conta plantillas.
     */
    private TextosSustitucionPlantillas(String texto) {
      this.texto = texto;
    }

    public String getTexto() {
      return texto;
    }

  }

  /**
   * Activacion de Tipos de conta plantillas.
   */
  public static enum ActivacionContaPlantillas {

    ACTIVO(1), ERROR(2), INACTIVO(0);

    /** Activacion conta plantilla. */
    private Integer activado;

    /**
     * Constructor I. Inicializa los atributos.
     * 
     * @param activado.
     */
    private ActivacionContaPlantillas(Integer activado) {
      this.activado = activado;
    }

    public Integer getActivado() {
      return this.activado;
    }

  }

  /** Estados de contabilidad. */
  public static enum EstadosContabilidadEnum {

    CDESTADOCONT("CDESTADOCONT"), ESTADOCONT("ESTADOCONT");

    /** Tipo de conta plantilla. */
    private String texto;

    /**
     * @param texto.
     */
    private EstadosContabilidadEnum(String texto) {
      this.texto = texto;
    }

    public String getTexto() {
      return texto;
    }

  }

  /** Estados generales. */
  public static enum EstadosEnum {

    OK("OK"), KO("KO");

    /** Tipo de conta plantilla. */
    private String texto;

    /**
     * @param texto.
     */
    private EstadosEnum(String texto) {
      this.texto = texto;
    }

    public String getTexto() {
      return texto;
    }

  }

  /** Tipos de datos posibles para las columnas de las tablas. */
  public static enum TiposDatosColsTblsEnum {

    BIGINT("BIGINT"), DECIMAL("DECIMAL"), SMALLINT("SMALLINT"), TIMESTMP("TIMESTMP"), VARCHAR("VARCHAR");

    /** Tipo de conta plantilla. */
    private String tipo;

    /**
     * @param tipo.
     */
    private TiposDatosColsTblsEnum(String tipo) {
      this.tipo = tipo;
    }

    public String getTipo() {
      return tipo;
    }

  }

  /** Estados. */
  public static enum EstadosSemaforoEnum {

    ACTIVO("Activo"), INACTIVO("Inactivo");

    /** Estado. */
    private String estado;

    /**
     * @param estado.
     */
    private EstadosSemaforoEnum(String estado) {
      this.estado = estado;
    }

    public String getEstado() {
      return estado;
    }

  }

} // SibbacEnums
