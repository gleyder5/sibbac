package sibbac.common;

import java.io.File;
import java.util.Properties;

public interface SibbacConfig {
	
	static final String CONFIGURATON_FOLDER_PROPERTY = "sibbac20.configuration.folder";
	
	static final String SIBBAC_FOLDER_PROPERTY = "sibbac20.folder";
	
	File getConfigurationFolder();
	
	File getSIBBACFolder();
	
	Properties loadPropertyFile(String fileName);

}
