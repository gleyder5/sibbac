package sibbac.common.helper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class CloneObjectHelper {

  /**
   * @param o
   * @param d
   * @param notSetThisFieldNames
   */
  public static void copyBasicFields(Object o, Object d, List<String> notSetThisFieldNames) {
    if (o != null) {
      Field[] fields = d.getClass().getDeclaredFields();
      String fieldMethodName = null;
      Method methodGet = null;
      Method methodSet = null;
      Object value = null;
      if (fields != null) {
        for (Field field : fields) {
          try {
            if (notSetThisFieldNames != null && notSetThisFieldNames.contains(field.getName())) {
              continue;
            }
            fieldMethodName = field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);

            methodGet = o.getClass().getMethod("get" + fieldMethodName);
            if (!methodGet.getReturnType().equals(field.getType())) {
              continue;
            }
            value = methodGet.invoke(o);

            methodSet = d.getClass().getMethod("set" + fieldMethodName, field.getType());
            if (methodSet != null) {
              methodSet.invoke(d, value);

            } else {
              methodSet = d.getClass().getMethod("set" + fieldMethodName, String.class);
              if (methodSet == null) {
                continue;
              } else {
                if (value == null) {
                  methodSet.invoke(d, (String) null);
                } else {
                  methodSet.invoke(d, value.toString().trim());
                }
              }
            }

          } catch (SecurityException e) {
          } catch (NoSuchMethodException e) {
          } catch (IllegalArgumentException e) {
          } catch (IllegalAccessException e) {
          } catch (InvocationTargetException e) {
          }
        }// fin for;
      }
    }
  }

  public static Object clone(Object o) {
    Object d = null;
    try {
      d = o.getClass().newInstance();
      copyBasicFields(o, d, null);
    } catch (InstantiationException | IllegalAccessException e) {

    }
    return d;
  }

}
