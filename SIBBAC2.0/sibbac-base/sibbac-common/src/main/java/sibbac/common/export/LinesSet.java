package sibbac.common.export;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class LinesSet {

	private List<String> headers;
	private List<List<String>> lines;
	private Map<Integer, Child> children;
	private long idPadre;
	private boolean root;

	public class Child {

		private boolean children;
		private long idLinea;
		private long idPadre;

		public Child(boolean children, long idLinea, long idPadre) {
			super();
			this.children = children;
			this.idLinea = idLinea;
			this.idPadre = idPadre;
		}

		public boolean hasChildren() {
			return this.children;
		}

		public void setChildren(boolean children) {
			this.children = children;
		}

		public long getIdLinea() {
			return this.idLinea;
		}

		public void setIdLinea(long idLinea) {
			this.idLinea = idLinea;
		}

		public long getIdPadre() {
			return this.idPadre;
		}

		public void setIdPadre(long idPadre) {
			this.idPadre = idPadre;
		}

		@Override
		public String toString() {
			return "[hasChildren==" + this.hasChildren() + "] [idLinea==" + this.getIdLinea() + "] [idPadre=="
					+ this.getIdPadre() + "]";
		}
	}

	public LinesSet() {
		this(false);
	}

	public LinesSet(final boolean isRoot) {
		this.headers = new ArrayList<String>();
		this.lines = new ArrayList<List<String>>();
		this.children = new Hashtable<Integer, Child>();
		this.root = isRoot;
	}

	public List<String> getHeaders() {
		return this.headers;
	}

	public void setHeaders(final List<String> headers) {
		this.headers = headers;
	}

	public void addHeader(final String header) {
		if (!this.headers.contains(header)) {
			this.headers.add(this.headers.size(), header);
		}
	}

	public void addHeaders(final List<String> headers) {
		this.headers.addAll(headers);
	}

	public boolean hasHeaders() {
		return !this.headers.isEmpty();
	}

	public List<List<String>> getLines() {
		return this.lines;
	}

	public void setLines(final List<List<String>> lines) {
		this.lines = lines;
	}

	public void addLine(final List<String> line, final boolean hasChildren, final long idLinea, final long idPadre) {
		int actual = this.lines.size();
		this.lines.add(actual, line);
		this.children.put(new Integer(actual), new Child(hasChildren, idLinea, idPadre));
	}

	public void addLines(final List<List<String>> lines) {
		this.lines.addAll(lines);
	}

	public boolean hasLines() {
		return !this.lines.isEmpty();
	}

	public boolean hasSomething() {
		return this.hasHeaders() || this.hasLines();
	}

	public long getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(long idPadre) {
		this.idPadre = idPadre;
	}

	public void setRoot(final boolean root) {
		this.root = root;
	}

	public boolean isRoot() {
		return this.root;
	}

	public Map<Integer, Child> getChildren() {
		return this.children;
	}

	public Child getChildren(int i) {
		if (this.children == null || this.children.isEmpty()) {
			return null;
		}
		Integer nLinea = new Integer(i);
		if (this.children.containsKey(nLinea)) {
			Child child = this.children.get(nLinea);
			return child;
		} else {
			return null;
		}
	}

}
