package sibbac.common.file.constants;

public enum FileResult {

  GENETARATED_OK(0, "Generado Ok"),

  GENETARATED_EMPTY(1, "Generado Vacio"),

  NOT_GENERATED(2, "Generado Vacio"),

  GENETARATED_KO(3, "Generado con fallos");

  private Integer value;

  private String desc;

  private FileResult(Integer value, String desc) {
    this.value = value;
    this.desc = desc;
  }

  public Integer value() {
    return this.value;
  }

  public String toString() {
    return this.desc;
  }

}