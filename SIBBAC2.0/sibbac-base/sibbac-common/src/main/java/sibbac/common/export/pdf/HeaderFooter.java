package sibbac.common.export.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Inner class to add a header and a footer.
 * 
 * @see http://itextpdf.com/examples/iia.php?id=104
 */
public class HeaderFooter extends PdfPageEventHelper {

	/** Alternating phrase for the header. */
	String header;

	/** Current page number (will be reset for every chapter). */
	int pagenumber;

	/** The template with the total number of pages. */
	PdfTemplate total;
	/*
	 * Font for header and footer part.
	 */
	private static Font headerFont = new Font(Font.FontFamily.COURIER, 9, Font.NORMAL, BaseColor.RED);
	private static Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD, BaseColor.RED);

	public HeaderFooter(String header) {
		super();
		this.header = header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * Creates the PdfTemplate that will hold the total number of pages.
	 * 
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onOpenDocument(PdfWriter writer, Document document) {
		total = writer.getDirectContent().createTemplate(30, 16);
	}

	/**
	 * Fills out the total number of pages before the document is closed.
	 * 
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onCloseDocument(PdfWriter writer, Document document) {
		ColumnText.showTextAligned(total, Element.ALIGN_LEFT, new Phrase(String.valueOf(writer.getPageNumber() - 1)), 2,
				2, 0);
	}

	/**
	 * Increase the page number.
	 * 
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onStartPage(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onStartPage(PdfWriter writer, Document document) {
		pagenumber++;
	}

	/**
	 * Adds the header and the footer.
	 * 
	 * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(com.itextpdf.text.pdf.PdfWriter,
	 *      com.itextpdf.text.Document)
	 */
	public void onEndPage(PdfWriter writer, Document document) {

		int right = Element.ALIGN_RIGHT;
		int left = Element.ALIGN_LEFT;
		int center = Element.ALIGN_CENTER;
		PdfContentByte cb = writer.getDirectContent();

		Phrase headerPhrase = new Phrase(this.header, headerFont);
		Phrase footerPhrase = new Phrase(String.format(" %d ", writer.getPageNumber()), footerFont);

		float rightHeaderPosition = document.leftMargin() - 2;
		float topHeaderPosition = document.top() + 20;

		float rightFooterPosition = document.right() - 2;
		float bottomFooterPosition = document.bottom() - 20;

		/*
		 * Header
		 */
		ColumnText.showTextAligned(cb, center, headerPhrase, rightHeaderPosition, topHeaderPosition, 0);

		/*
		 * Footer
		 */
		int oddPage = 0;
		switch (writer.getPageNumber() % 2) {
		case 0:
			oddPage = right;
			break;
		case 1:
			oddPage = left;
			break;
		}
		ColumnText.showTextAligned(cb, oddPage, footerPhrase, rightFooterPosition, bottomFooterPosition, 0);
	}
}
