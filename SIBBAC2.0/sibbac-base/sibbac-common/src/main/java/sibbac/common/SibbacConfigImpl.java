package sibbac.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.util.Properties;
import static java.util.Objects.requireNonNull;

import javax.annotation.Resource;

import org.slf4j.Logger;
import static org.slf4j.LoggerFactory.getLogger;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON;

@Component
@Scope(SCOPE_SINGLETON)
public class SibbacConfigImpl implements SibbacConfig {
	
	private static final Logger LOG = getLogger(SibbacConfigImpl.class);
	
	private static final String UTF8_COD = "UTF8";
	
	private static final String SIBBAC_FOLDER_DEFAULT_PATH = "sbrmv/ficheros";
	
	private static final String CONFIGURATION_FOLDER_DEFAULT_PATH = "SIBBAC20Configuration";
	
	private Properties sibbacProperties;
	
	private File configurationFolder;
	
	private File sibbacFolder;

	@Override
	public File getConfigurationFolder() {
		return requireNonNull(configurationFolder, "El folder de configuración no está inicializado");
	}

	@Override
	public File getSIBBACFolder() {
		return requireNonNull(sibbacFolder, "El folder sibbac no está inicializado");
	}

	@Override
	public Properties loadPropertyFile(String fileName) {
		final Properties props;
		final File propsFile;
		
		props = new Properties();
		propsFile = new File(configurationFolder, fileName);
		if(propsFile.exists()) {
			try(Reader istream = new BufferedReader(new InputStreamReader(new FileInputStream(propsFile), UTF8_COD))) {
				props.load(istream);
			}
			catch(IOException ex) {
				LOG.error("[loadPropertyFile] Error en la lectura del fichero de configuracion {}. Usando defaults", 
						fileName, ex);
			}
		}
		else {
			LOG.warn("[loadPropertyFile] El fichero de configuracion {} no existe. Usando defaults", fileName);
		}
		return props;
	}
	
	@Resource(name = "sibbacProps")
	public void setSibbacProperties(Properties sibbacProperties) {
		this.sibbacProperties = sibbacProperties;
		locateSibbacFolder();
		locateConfigFolder();
	}
	
	private void locateSibbacFolder() {
		final File roots[], firstRoot;
		
		if(sibbacProperties.contains(SIBBAC_FOLDER_PROPERTY)) {
			sibbacFolder = new File(sibbacProperties.getProperty(SIBBAC_FOLDER_PROPERTY));
			return;
		}
		roots = File.listRoots();
		firstRoot = (roots.length == 0) ? new File("/") : roots[0];
		sibbacFolder = new File(firstRoot, SIBBAC_FOLDER_DEFAULT_PATH);
	}
	
	private void locateConfigFolder() {
		if(sibbacProperties.containsKey(CONFIGURATON_FOLDER_PROPERTY)) {
			configurationFolder = new File(sibbacProperties.getProperty(CONFIGURATON_FOLDER_PROPERTY));
			return;
		}
		configurationFolder = new File(sibbacFolder, CONFIGURATION_FOLDER_DEFAULT_PATH);
	}

}
