package sibbac.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.io.CopyStreamException;
import org.apache.commons.net.io.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FtpSibbacContabilidad {
	protected static final Logger LOG = LoggerFactory.getLogger(FtpSibbacContabilidad.class);

	@Value("${ftp.accounting.url}") private String url;
	@Value("${ftp.accounting.port}") private String port;
	@Value("${ftp.accounting.username}") private String username;
	@Value("${ftp.accounting.password}") private String password;
	private FTPClient ftp;

	@Transactional public boolean connect(String folderDestino) throws IOException {
		ftp = new FTPClient();
		try {
			ftp.connect(url, Integer.parseInt(port));
		} catch (Exception e) {
			LOG.error("No se pudo conectar al servidor FTP ");
			return false;
		}
		if (!FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
			LOG.error("Exception in connecting to FTP Server");
			try {
				ftp.disconnect();
			} catch (Exception e) {
				LOG.error("Exception in disconnect FTP Server", e);
			}
			throw new IOException("Exception in connecting to FTP Server");
		}
		ftp.login(username, password);
		ftp.mkd(folderDestino);
		ftp.changeWorkingDirectory(folderDestino);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		ftp.enterLocalPassiveMode();
		return true;
	}

	@Transactional public boolean send(List<Object[]> list, String folderTratado) throws IOException {
		File fileTratado;
		if (!send(list)) {
			return false;
		}
		for (Object[] objects: list) {
			fileTratado = new File(folderTratado, (String) objects[2]);
			fileTratado.getParentFile().mkdirs();
			Files.move(((File) objects[0]).toPath(), fileTratado.toPath());
		}
		return true;
	}

	@Transactional public boolean send(List<Object[]> list) throws IOException {
		File fileOrigen;
		String nameDestino;
		OutputStream oS;
		InputStream iS;
		for (Object[] objects: list) {
			fileOrigen = (File) objects[0];
			nameDestino = (String) objects[1];
			try {
				oS = ftp.storeFileStream(nameDestino);
			} catch (IOException e) {
				LOG.error("No crea el fichero " + nameDestino + " en el FTP Server", e);
				throw e;
			}
			try {
				iS = new FileInputStream(fileOrigen);
			} catch (FileNotFoundException e) {
				LOG.error("No abre para lectura el local " + fileOrigen.getAbsolutePath(), e);
				throw e;
			}
			try {
				Util.copyStream(iS, oS);
			} catch (CopyStreamException e) {
				LOG.error("No copia el fichero " + fileOrigen.getName() + " al FTP Server", e);
				throw e;
			} finally {
				try {
					oS.close();
				} catch (IOException e) {
					LOG.error("No cierra el OutputStream", e);
					throw e;
				}
				try {
					iS.close();
				} catch (IOException e) {
					LOG.error("No cierra el fichero local", e);
					throw e;
				}
			}
			if (!ftp.completePendingCommand()) {
				LOG.error("Problemas al escribir en el FTP los ficheros de contabilidad");
				throw new IOException("Problemas al escribir en el FTP los ficheros de contabilidad. No hace completePendingCommand");
			}
		}
		return true;
	}
}
