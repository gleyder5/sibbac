package sibbac.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CompressFileUtil {
    
    private static final String TAG = CompressFileUtil.class.getName();
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    private static final String BACKUP_ALL_FILES_GIVEN_MONTHS_AMOUNT_BEFORE_IN = "backupAllFilesGivenMonthsAmoungBeforeIn";
    private static final String SERIALIZED_PATH_SEPARATOR = "__";

    /**
     * Comprime una lista de ficheros encontrados dentro del directorio pasado
     * por parámetros en un mismo fichero zip
     * 
     * @param fileList
     * @param zipFile
     */
    public void compressDirectory(List<String> fileList, String zipFile) throws IOException {
        byte[] buffer = new byte[1024];
        int length;

        final String suffix = "compressDirectory";
        if (new File(zipFile).exists()) {
            appendEntriesToExistingZip(zipFile, fileList);
        }
        try (FileOutputStream fos = new FileOutputStream(zipFile);
                ZipOutputStream zos = new ZipOutputStream(fos)){
            for (String filePath : fileList) {
                LOG.info(TAG + "::" + suffix + " Compressing: " + filePath);
                LOG.debug(TAG + "::" + suffix + " Compressing: {} in {}", filePath, zipFile);
                // Creates a zip entry.
                String name = filePath.substring(filePath.lastIndexOf(File.separator) + 1, filePath.length());
                ZipEntry zipEntry = new ZipEntry(name);
                zos.putNextEntry(zipEntry);
                // Read file content and write to zip output stream.
                try(FileInputStream fis = new FileInputStream(filePath)) {
                    while ((length = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, length);
                    }
                }
            }
        } 
    }

    /**
     * Añade una lista de ficheros a un zip existente
     * @param zipFile
     * @param newFileList
     * @return
     */
    private void appendEntriesToExistingZip(String zipFile, List<String> newFileList) throws IOException {
        /* Define ZIP File System Properies in HashMap */
        Map<String, String> zip_properties = new HashMap<>();
        String processingEntry = null;
        /* We want to read an existing ZIP File, so we set this to False */
        zip_properties.put("create", "false");
        /* Specify the encoding as UTF -8 */
        zip_properties.put("encoding", "UTF-8");
        /*
         * Specify the path to the ZIP File that you want to read as a File
         * System
         */
        if(zipFile.startsWith("/")) {
          zipFile = zipFile.substring(1, zipFile.length());
        }
        URI zip_disk = URI.create("jar:file:/" + zipFile.replace('\\', '/'));
        LOG.info("[appendEntriesToExistingZip] zipFile: {}, uri del fichero: {}", zipFile, zip_disk.toString());
        /* Create ZIP file System */
        try (FileSystem zipfs = FileSystems.newFileSystem(zip_disk, zip_properties)) {
            for (String entry : newFileList) {
                /* Create a Path in ZIP File */
                processingEntry = new String(entry);
                Path ZipFilePath = zipfs
                        .getPath(entry.substring(entry.lastIndexOf(File.separator) + 1, entry.length()));
                LOG.debug("[appendEntriesToExistingZip] append entry: {}", ZipFilePath.getFileName());
                /* Path where the file to be added resides */
                Path addNewFile = Paths.get(entry);
                /* Append file to ZIP File */
                if(addNewFile.toFile().exists()) {
                  LOG.warn("[appendEntriesToExistingZip] entrada {} ya existe. No se agrega", addNewFile.toString());
                  continue;
                }
                Files.copy(addNewFile, ZipFilePath);
                LOG.debug("[appendEntriesToExistingZip] appended entry: {}", ZipFilePath.getFileName());
            }
        } 
        catch(FileAlreadyExistsException fex) {
          LOG.error("El fichero {} ya existe en el comprimido y no se ha detectado su presencia.", processingEntry);
          throw fex;
        }
        catch (IOException e) {
          LOG.error("[appendEntriesToExistingZip] Error de entrada/salida insertando el fichero {} en el zip {}", 
              processingEntry, zipFile, e);
          throw e;
        }
        LOG.debug("[appendEntriesToExistingZip] Fin");
    }

    /**
     * Comprime el fichero pasado en el primer parámetro y lo guarda en el
     * fichero cuya ruta, nombre incluido es definida por el segundo parámetro
     * 
     * @param toZip
     * @param zipName
     */
    public void compressFile(File toZip, String zipName) {
        final String suffix = "compressFile";
        byte[] buffer = new byte[1024];
        int len;
        ZipEntry ze;

        try (FileOutputStream fos = new FileOutputStream(zipName);
                ZipOutputStream zos = new ZipOutputStream(fos)){
            ze = new ZipEntry(toZip.getName());
            zos.putNextEntry(ze);
            try(FileInputStream in = new FileInputStream(toZip.getAbsolutePath())) {
                LOG.debug("[compressFile] comprimiendo fichero: {} en: {} ", toZip.getAbsolutePath(), toZip);
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
            }
            LOG.debug(TAG + "::" + suffix + " fichero comprimido con éxito.");

        } catch (IOException ex) {
            LOG.error(ex.getMessage() + ". Cause: " + ex.getCause(), ex);
        } 
    }

    /**
     * Comprime todos los ficheros modificados hace más de 2 meses y los deja en
     * un directorio de backup, luego borra los ficheros que se han comprimido
     * en tal carpeta.
     * @param instance
     * @param pathToSearch
     * @param pathToSave
     */
    public List<String> backupAllFilesGivenMonthsAmoungBeforeIn(String pathToSearch, String pathToSave,
            String[] pathsToExclude, int monthsAmoung) throws IOException {
        final String suffix = "backupAllFilesGivenMonthsAmoungBeforeIn";
        String path; // Ruta del fichero que se va a comprimir 
        String name; // Nombre del fichero que se va a comprimir 
        String zipName = null; // Nombre del fichero zip resultante
        
        List<String> zippedList = new ArrayList<>();
        File rootFolder = new File(pathToSearch);
        /* Lista de ficheros que se van a comprimir */
        List<String> fileList = new ArrayList<>();
        getFileList(rootFolder, fileList);
        File foundFile = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
        /* Coletilla de fecha para añadir al nombre del fichero */
        SimpleDateFormat sdfName = new SimpleDateFormat("YYYY_MM_dd");
        Date date;
        /* Fecha actual */
        final Date now = Calendar.getInstance().getTime();
        /* Instancia de calendario para manipular fechas */
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, monthsAmoung);
        LOG.debug(TAG + "::" + suffix + " dos meses antes de ahora: " + sdf.format(cal.getTime()));
        final Date twoMonthBefore = cal.getTime();

        /* Posicion del separador antes del nombre del fichero */
        int lastSlashPos = 0;
        /*
         * Se van guardando los ficheros que se van a comprimir en un solo zip
         */
        List<String> entriesToZip = new ArrayList<>();
        /* Nombre del zip donde se van a comprimir los ficheros */
        String newZipFile = "";
        for (String fich : fileList) {
            foundFile = new File(fich);
            if (foundFile.canRead() && !foundFile.isHidden() && !foundFile.getName().startsWith(".")) {
                lastSlashPos = fich.lastIndexOf(File.separator);
                path = fich.substring(0, lastSlashPos);
                /*
                 * Hay carpetas que deben ignorarse, por ejemplo configuracion
                 */
                if (isInExcludePath(path, pathsToExclude)) {
                    continue;
                }
                path = path.replace(":", "");
                name = fich.substring(lastSlashPos + 1);
                date = new Date(foundFile.lastModified());
                zipName = pathToSave + File.separator + path.replace(File.separator, SERIALIZED_PATH_SEPARATOR) + "_"
                        + sdfName.format(now) + "_" + Calendar.getInstance().get(Calendar.YEAR) + ".zip";

                /*
                 * Si el fichero se modificó hace dos meses o antes, se comprime
                 * y se borra
                 */
                if (date.before(twoMonthBefore) || date.equals(twoMonthBefore)) {
                    LOG.debug(TAG + "::" + suffix + " se va a comprimir el fichero " + name + " como: " + zipName);
                    if (zipName.toString().equals(newZipFile.toString())) {
                        entriesToZip.add(foundFile.getAbsolutePath());
                    } else if (!"".equals(newZipFile.toString()) && !zipName.equals(newZipFile.toString())) {
                        LOG.debug(TAG + ":: {} se van a comprimir {} ficheros en la ruta: {}",
                                BACKUP_ALL_FILES_GIVEN_MONTHS_AMOUNT_BEFORE_IN, entriesToZip.size(), newZipFile);
                        /*
                         * Comprime la lista de ficheros en la ruta de
                         * newZipFile
                         */
                        compressDirectory(entriesToZip, newZipFile);
                        entriesToZip.clear();
                        entriesToZip.add(foundFile.getAbsolutePath());
                        newZipFile = zipName;
                    } else {
                        /*
                         * Se han guardado archivos de un mismo directorio en un
                         * zip, y ahora se cambia el nombre del fichero zip,
                         * dónde se guardarían los siguientes, pertenecientes a
                         * otro directorio en común para todos.
                         */
                        newZipFile = zipName;
                        entriesToZip.add(foundFile.getAbsolutePath());
                    }

                    zippedList.add(fich);
                }
            }
        }
        /* Si quedan entradas por comprimir, se comprimen */
        if (CollectionUtils.isNotEmpty(entriesToZip)) {
            compressDirectory(entriesToZip, zipName);
            entriesToZip.clear();
            zipName = "";
        }
        return zippedList;
    }

    /**
     * Comprueba si el directorio a procesar está en la lista de excluidos
     * @param path
     * @param pathsToExclude
     * @return
     */
    private boolean isInExcludePath(String path, String[] pathsToExclude) {
        for (String exclude : pathsToExclude) {
            if (path.startsWith(exclude)) {
                LOG.debug(TAG + "::isInExcludePath " + "Esta carpeta está en la lista de exluidas " + path);
                return true;
            }
        }
        return false;
    }

    /**
     * Get files list from the directory recursive to the sub directory.
     */
    static List<String> getFileList(File directory, List<String> fileList) {

        File[] files = directory.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isFile()) {
                    fileList.add(file.getAbsolutePath());
                } else {
                    getFileList(file, fileList);
                }
            }
        }
        return fileList;
    }

}