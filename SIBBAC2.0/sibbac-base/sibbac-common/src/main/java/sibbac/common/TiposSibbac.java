package sibbac.common;

public interface TiposSibbac {

  public interface CodigosOperacion {

    // Tipos de operacion en mercado

    public static final String COMPRA_VENTA_DE_MERCADO = "M";

    public static final String BLOQUE = "Q";

    public static final String OPERACION_ESPECIAL = "A";

    public static final String OPERACIONES_DELTA = "L";

    public static final String OPERACIONES_A_VALOR_LIQUIDATIVO_SICAVS = "O";

    public static final String EJERCICIOS_Y_VENCIMIENTOS_DEL_SEGMENTO_DE_DERIVADOS = "E";

    public static final String BILATERAL_PROCEDENTE_DE_IBERCLEAR = "U";

    // Movimientos de asignacion

    public static final String OPERACION_DE_ASIGNACION_IMTERNA = "D";

    public static final String OPERACION_DE_ASIGNACION_EXTERNA = "G";

    public static final String TRASPASO = "T";

    // Fase 2

    public static final String GENERACION_INSTRUCCION_DE_LIQUIDACION = "3";

    public static final String PRESTAMO_IDA = "4";

    public static final String PRESTAMO_VUELTA = "5";

    public static final String RECOMPRA = "6";

    public static final String LIQUIDACION_EN_EFECTIVO = "7";

    public static final String ACTUALIZACION_DE_COLATERAL_DE_PRESTAMO = "8";

    public static final String PRESTAMO_DE_AJUSTE = "9";

    public static final String LIQUIDACION_DE_IL = "B";

    public static final String AJUSTE_POR_EVENTOS = "C";

    public static final String NETEO_AGREGACION = "N";

    public static final String CANCELACION = "X";

  }

  public interface IndicadoresOperacion {

    public static final String NUEVA_OPERACION = "0";

    public static final String ACTUALIZACION_OPERACION = "2";

  }

  public interface TiposMovimiento {

    public static final String MO = "MO";

    public static final String MO_IT = "IT";

    public static final String AN = "AN";

    public static final String AC = "AC";

    public static final String ASIGNACION_INTERNA = "16";

    public static final String ASIGNACION_EXTERNA = "15";

  }

  public interface ReferenciasActuacion {

    public static final String ACEPTADA = "A"; // Aceptada la asignacion por
                                               // destino.

    public static final String RECHAZADA = "R"; // Rechazada la asignacion por
                                                // destino.

    public static final String ACEPTADA_COMPENSADOR_DESTINO = "O"; // Aceptada
                                                                   // la
                                                                   // Asignacion
                                                                   // por el
                                                                   // Compensador
                                                                   // del
                                                                   // Destino

    public static final String RECHAZADA_COMPENSADOR_DESTINO = "N"; // Rechazada
                                                                    // la
                                                                    // Asignacion
                                                                    // por el
                                                                    // Compensador
                                                                    // del
                                                                    // Destino

    public static final String CANCELADO = "C"; // Cancelada la solicitud de
                                                // Asignaci?n (esta acci?n s?lo
                                                // la
    // puede realizar el Miembro que solicita la Asignación)

  }

  public interface CriteriosComunicacionTitulares {

    public static final String NUMERO_OPERACION_ECC = "1";
    public static final String NUMERO_OPERACION_DCV = "2";
    public static final String NUMERO_ORDEN_MERCADO = "5";
    public static final String NUMERO_REFERENCIA_CLIENTE = "6";
    public static final String EUROCCP_EXECUTION_ID = "E";
  }

  public interface IndicadorCapacidad {

    public static final String PROPIO = "P";
    public static final String AJENO = "A";
  }

  public interface TiposCuentaCompensacion {

    public static final String PROPIA = "CP";
    public static final String DIARIA = "CD";
    public static final String TERCEROS_NETA = "CTN";
    public static final String TERCEROS_BRUTA = "CTB";
    public static final String INDIVIDUAL = "CI";
    public static final String INDIVIDUAL_INTERMEDIARIO_FINANCIERO_NETA = "CIEN";
    public static final String INDIVIDUAL_INTERMEDIARIO_FINANCIERO_BRUTA = "CIEB";
    public static final String INDIVIDUAL_INTERMEDIARIO_FINANCIERO_BRUTAS = "ASSI";

  }

  public interface TiposPositionBreakDown {

    public static final String MANUAL = "M";
    public static final String PROVISIONAL = "P";
    public static final String DEFINIFIVE = "D";

  }

  public interface TiposDesgloseBBDD {

    public static final String MANUAL = "MANUAL";
    public static final String EXCEL = "EXCEL";
    public static final String INTERMEDIARIO_FINANCIERO = "IF";

  }

  public interface TiposComision {

    public static final String PARTENON_ROUTING = "R";
    public static final String CORRETAJE_PTI = "O";
    public static final String CLEARING = "C";
    public static final String FACTURACION = "F";

  }

  public interface TiposEnvioComisionPti {

    public static final char MESSAGE_PTI = 'M';
    public static final char FINAL_DAY_FILE = 'F';
    public static final char NOT_ALLOWED = 'N';
    public static final char ALL_METHOD_ALLOWED = 'A';

  }

  public interface TiposSentidoOrden {

    public static final String COMPRA_XML = "C";
    public static final String VENTA_XML = "V";

    public static final String COMPRA_ECC = "1";
    public static final String VENTA_ECC = "2";

  }

  public interface TiposProductoComisionDWH {
    public static final String NACIONAL = "008";
  }

  public interface TipoIdMifid {
    public static char LEI = 'L';
    public static char IDENTIFICACION_NACIONAL = 'I';
    public static char PASAPORTE = 'P';
    public static char CONCAT = 'C';
  }
  
  public interface TipoRouting {
    public static String CDCLENTE = "ROUTING_BCH";
    public static String CDORIGEN = "ROUTING";
  }

}
