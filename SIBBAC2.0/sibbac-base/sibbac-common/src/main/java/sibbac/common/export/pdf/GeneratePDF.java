package sibbac.common.export.pdf;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class GeneratePDF {
	public static ByteArrayOutputStream createPdf(String html, String... css) throws IOException, DocumentException {
		Document document = new Document();
		ByteArrayOutputStream salida = new ByteArrayOutputStream();
		PdfWriter writer = PdfWriter.getInstance(document, salida);
		CSSResolver cssResolver = new StyleAttrCSSResolver();
		HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);

		writer.setInitialLeading(12.5f);
		document.open();
		for (String file : css) {
			cssResolver.addCss(XMLWorkerHelper.getCSS(new FileInputStream(file)));
		}
		htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
		PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
		HtmlPipeline htmlPipeline = new HtmlPipeline(htmlContext, pdf);
		CssResolverPipeline cssResolverPipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
		XMLWorker worker = new XMLWorker(cssResolverPipeline, true);
		XMLParser p = new XMLParser(worker);
		p.parse(new FileInputStream(html));
		document.close();
		return salida;
	}
}