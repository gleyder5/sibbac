package sibbac.common.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigUtil {
    private static final String GET_FACTURACION_CONFIG = "getFacturacionConfig";
    private static final String GET_MINIJOBS_CONFIG = "getMinijobsConfig";
    private static ConfigUtil instance = null;
    private static final String TAG = ConfigUtil.class.getName();
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    private static PropertiesConfiguration filesystemConfig = null;
    /* configuracion para facturacion */
    private static PropertiesConfiguration facturacionConfig = null;
    /* configuracion para minijobs */
    private static PropertiesConfiguration minijobsConfig = null;
    /**/
    private static final String CONFIG_FILES_PATH = "/sbrmv/ficheros/SIBBAC20Configuration";
    private static final String CONFIG_FILE_NAME = "sibbac.filesystem.properties";
    private static final String FACTURACION_FILE_NAME = "sibbac.facturacion.properties";
    private static final String MINIJOBS_FILE_NAME = "sibbac.minijobs.properties";
   
    
    private ConfigUtil() {
	//
    }

    public static ConfigUtil getInstance() {
	if (instance == null) {
	    instance = new ConfigUtil();
	}
	return instance;
    }

    public PropertiesConfiguration getConfig() {
	if (filesystemConfig == null) {
	    String filename = CONFIG_FILE_NAME;
	    filesystemConfig = config(filename);
	    filesystemConfig
		    .setReloadingStrategy(new FileChangedReloadingStrategy());
	}
	return filesystemConfig;
    }

    /**
     * Obtiene un fichero de configuración dada su ruta
     * 
     * @param ruta
     * @return PropertiesConfiguration
     */
    public PropertiesConfiguration getConfig(String ruta) {
	PropertiesConfiguration configFile = null;
	try {
	    InputStream is = ConfigUtil.class.getResourceAsStream(ruta);
	    configFile = new PropertiesConfiguration();
	    configFile.load(is);
	} catch (ConfigurationException ex) {
	    LOG.error(ex.getMessage(), ex);
	}
	return configFile;
    }

    /**
     * 
     * @param filename
     * @return
     */
    public PropertiesConfiguration config(String filename) {
	PropertiesConfiguration config = null;
	// hay que prevenir que en windows los separadores son \
	String fileName = filename.substring(filename.lastIndexOf("/") + 1);
	//Ruta donde se va a guardar el fichero de propiedades
	String pathToConfigAppFile =  CONFIG_FILES_PATH + File.separator + fileName;
	
	LOG.debug("pathToConfigAppFile: " + pathToConfigAppFile);
	/*Fichero de propiedades que se quiere obtener*/
	File fConfigUser = new File(pathToConfigAppFile);
	/*Si el fichero no está creado en la carpeta de destino, entonces se crea*/
	if (!fConfigUser.exists()) {
	    /*Se crea el arbol de directorios si no existe*/
	    if (fConfigUser.getParentFile() != null
		    && !fConfigUser.getParentFile().exists()) {
		fConfigUser.getParentFile().mkdirs();
	    }
	    /*Se obtiene el fichero properites que se desea obtener en forma de stream*/
	    InputStream in = ConfigUtil.class.getResourceAsStream("/"+filename);
	    FileOutputStream out = null;
	    try {

		out = new FileOutputStream(fConfigUser);
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
		    out.write(buf, 0, len);
		}
		/*Se obtiene el fichero que se acaba de crear en un objeto PropertiesConfiguration*/
		config = new PropertiesConfiguration(pathToConfigAppFile);
	    } catch (FileNotFoundException e) {
		LOG.error(e.getMessage(), e);
	    } catch (ConfigurationException e) {
		LOG.error(e.getMessage(), e);
	    } catch (IOException ex) {
		LOG.error(ex.getMessage(), ex);
	    }finally{
		try {
		    if(in != null){
			in.close();
		    }
		    if(out != null){
			out.close();
		    }
		} catch (IOException e) {
		    LOG.error(e.getMessage(), e);
		}
	    }
	} else {
	    try {
		config = new PropertiesConfiguration(pathToConfigAppFile);
	    } catch (ConfigurationException ex) {
		LOG.error(ex.getMessage(), ex);
	    }
	}
	return config;
    }

    public void saveConfig() {
	saveConfig(CONFIG_FILE_NAME, filesystemConfig);
    }
    /**
     * Guarda el fichero de configuración dado su nombre y el objeto que lo contiene
     * @param filename
     * @param config
     * @return boolean
     */
    public boolean saveConfig(String filename, PropertiesConfiguration config) {
	boolean res = true;
	String pathToConfigAppFile = CONFIG_FILES_PATH + File.separator
		+ filename;
	try {
	    config.save(pathToConfigAppFile);
	} catch (ConfigurationException e) {
	    LOG.error(e.getMessage(), e);
	    res = false;
	}
	return res;
    }
    /**
     * Obtiene las propiedades del fichero sibbac.facturacion.properties
     * @return
     */
    public PropertiesConfiguration getFacturacionConfig() {
	LOG.debug("{}::{} Recuperando fichero de propiedades para facturación en un objeto config ...", TAG, GET_FACTURACION_CONFIG);
	if (facturacionConfig == null) {
	    String filename = FACTURACION_FILE_NAME;
	    facturacionConfig = config(filename);
	    facturacionConfig
		    .setReloadingStrategy(new FileChangedReloadingStrategy());
	}
	LOG.debug("{}::{} Devolviendo objeto config de facturación ...", TAG, GET_FACTURACION_CONFIG);
	return facturacionConfig;
    }
    /**
     * Obtienes las propiedades del fichero sibbac.minijobs.properties
     * @return
     */
    public PropertiesConfiguration getMinijobsConfig(){
	LOG.debug("{}::{} Recuperando fichero de propiedades para minijobs en un objeto config ...", TAG, GET_MINIJOBS_CONFIG);
	if (minijobsConfig == null) {
	    String filename = MINIJOBS_FILE_NAME;
	    minijobsConfig = config(filename);
	    minijobsConfig
		    .setReloadingStrategy(new FileChangedReloadingStrategy());
	}
	LOG.debug("{}::{} Devolviendo objeto config de minijobs ...", TAG, GET_MINIJOBS_CONFIG);
	return minijobsConfig;
    }

}
