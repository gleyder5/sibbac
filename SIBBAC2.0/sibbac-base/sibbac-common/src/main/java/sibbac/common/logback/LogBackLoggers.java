package sibbac.common.logback;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;

/**
 * Class for controlling LogBack logging level.
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public class LogBackLoggers {

	protected static List<Logger> TOP_LEVEL_LOGGERS = null;
	protected static Map<Logger, Level> DEFAULT_LEVEL_LOGGERS = null;
	protected static LoggerContext LC = (LoggerContext) LoggerFactory.getILoggerFactory();
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(LogBackLoggers.class);

	static {
		TOP_LEVEL_LOGGERS = new ArrayList<Logger>();
		DEFAULT_LEVEL_LOGGERS = new Hashtable<Logger, Level>();
		List<Logger> loggers = LC.getLoggerList();
		if (loggers != null && !loggers.isEmpty()) {
			for (Logger logger : loggers) {
				if (logger.getLevel() != null) {
					TOP_LEVEL_LOGGERS.add(logger);
					DEFAULT_LEVEL_LOGGERS.put(logger, logger.getLevel());
				}
			}
		}
	}

	public static final List<Logger> getLoggers() {
		return TOP_LEVEL_LOGGERS;
	}

	public static final void setLoggerLevel(String n, String l) {
		if (n != null && !n.isEmpty() && l != null && !l.isEmpty()) {
			// Something
			Level level = Level.toLevel(l);
			Logger logger = findLogger(n);
			if (logger != null && level != null) {
				Level oldLevel = logger.getLevel();
				logger.setLevel(level);
				LOG.info("[{}::setLoggerLevel] Changed log level of logger({}): [{} -> {}]",
						LogBackLoggers.class.getName(), n, oldLevel, level);
			}
		}
	}

	public static final void setLoggerLevel(Logger logger, Level level) {
		if (TOP_LEVEL_LOGGERS.contains(logger)) {
			logger.setLevel(level);
		}
	}

	public static final void resetDefault() {
		for (Logger logger : TOP_LEVEL_LOGGERS) {
			logger.setLevel(DEFAULT_LEVEL_LOGGERS.get(logger));
		}
	}

	private static Logger findLogger(final String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Logger found = null;
		for (Logger logger : TOP_LEVEL_LOGGERS) {
			if (logger.getName().equalsIgnoreCase(name)) {
				found = logger;
				break;
			}
		}
		return found;
	}

}
