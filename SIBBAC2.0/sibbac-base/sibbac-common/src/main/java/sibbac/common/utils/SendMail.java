package sibbac.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import sibbac.common.SIBBACBusinessException;

@Service
public class SendMail {

	private static final Logger LOG = LoggerFactory.getLogger(SendMail.class);

	@Value("${mail.source.folder}")
	public String TEMPLATES_FOLDER;

	@Value("${mail.from}")
	private String from;

	@Value("${mail.utf8:false}")
	private boolean utf8;

	@Autowired
	protected ApplicationContext context;

	@Autowired
	private JavaMailSender sender;

	public boolean isUtf8() {
		return this.utf8;
	}

	public void setUtf8(boolean utf8) {
		this.utf8 = utf8;
	}

	public void setSender(JavaMailSender sender) {
		this.sender = sender;
	}

	public void sendMail(final MailTemplate mailTemplate) throws SIBBACBusinessException {
		String prefix = "[SendMail::sendMail] ";
		LOG.trace(prefix + "Usando el template: {}", mailTemplate);
		LOG.trace(prefix + "Carpeta de plantillas: [{}]", TEMPLATES_FOLDER);
		BufferedReader reader = null;
		StringBuilder out = new StringBuilder();
		String body = null;
		String templateName = mailTemplate.getTemplateName();

		// Hay que identificar el "template" como nombre de archivo externo.
		if (templateName == null || templateName.isEmpty()) {
			throw new SIBBACBusinessException("No se ha recibido plantilla");
		}
		String templatePath = "file:" + TEMPLATES_FOLDER + File.separator + templateName;
		// String templatePath = "classpath:" + TEMPLATES_FOLDER +
		// File.separator + templateName;
		LOG.trace(prefix + "Buscando la plantilla en la ruta: [{}]", templatePath);

		// Y ver que tenemos mappings
		Map<String, String> mappings = mailTemplate.getMapping();
		if (mappings == null) {
			throw new SIBBACBusinessException("No hay mappings!");
		}

		// Localizamos el template que nos indiquen.
		Resource resource = null;
		try {

			// Spring?
			resource = context.getResource(templatePath);
			// LOG.debug( prefix + "Identificada la plantilla [{}] en la ruta:
			// [{}]", resource.getFilename(), resource.getFile() );
			InputStream in = resource.getInputStream();
			reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
		} catch (IOException e) {
			LOG.error("ERROR({}): Error leyendo la plantilla [{}/{}]: [{}]", e.getClass().getName(), templateName,
					templatePath, e.getMessage());
			throw new SIBBACBusinessException("Error leyendo la plantilla [" + templateName + "/" + templatePath + "]",
					e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// Nothing
				}
			}
		}

		if (out != null && out.length() > 0) {
			LOG.trace(prefix + "Inyectando la informacion recibida en el template: {}", templateName);

			// El body
			body = out.toString();

			// Los elementos a sustituir
			Set<String> keys = mappings.keySet();
			String value = null;
			for (String key : keys) {
				value = mappings.get(key);
				body = body.replace("${" + key + "}", value);
				LOG.trace(prefix + "+ Reemplazado [${{}}] por [{}].", key, value);
			}

			try {
				LOG.trace( prefix + "Enviando el correo..." );
				this.innerSendMail( mailTemplate.getTo(), mailTemplate.getSubject(), body, mailTemplate.getAttachments(),
						mailTemplate.getAttachmentNames(), mailTemplate.getCc() );
			} catch ( IllegalArgumentException | MessagingException | IOException e ) {
				throw new SIBBACBusinessException( "Error enviando el correo", e );
			}
		} else {
			throw new SIBBACBusinessException(
					"Error con la plantilla [" + templateName + "]: No hay \"body\" detectado");
		}

	}

  public void sendMail(final String to, final String subject, final String body)
      throws IllegalArgumentException, MessagingException, IOException {
    if (!to.isEmpty()) {
      List<String> destinatarios = new ArrayList<String>();
      destinatarios.add(to);
      this.innerSendMail(destinatarios, subject, body, null, null, null);
    }
  }

	public void sendMail( final List< String > to, final String subject, final String body )
			throws IllegalArgumentException, MessagingException, IOException {
		this.innerSendMail( to, subject, body, null, null, null );
	}

	public void sendMail(final List<String> to, final String subject, final String body, final List<String> cc)
			throws IllegalArgumentException, MessagingException, IOException {
		this.innerSendMail(to, subject, body, null, null, cc);
	}

	public void sendMail( final List< String > to, final String subject, final String body,
			final List< InputStream > path2attach, final List< String > nameDest, final List< String > cc )
			throws IllegalArgumentException, MessagingException, IOException {
		String prefix = "[SendMail::sendMail(attachments)] ";
		if (path2attach != null && !path2attach.isEmpty()) {
			LOG.debug(prefix + "Adjuntando {} archivos al correo...", nameDest.size());
			for (String n : nameDest) {
				LOG.debug(prefix + "> File: {}", n);
			}
		}
		this.innerSendMail(to, subject, body, path2attach, nameDest, cc);
	}
	
	public void sendMailHtml( final List< String > to, final String subject, final String body,
			final List< InputStream > path2attach, final List< String > nameDest, final List< String > cc )
			throws IllegalArgumentException, MessagingException, IOException {
		String prefix = "[SendMail::sendMail(attachments)] ";
		if (path2attach != null && !path2attach.isEmpty()) {
			LOG.debug(prefix + "Adjuntando {} archivos al correo...", nameDest.size());
			for (String n : nameDest) {
				LOG.debug(prefix + "> File: {}", n);
			}
		}
		this.innerSendMailHtml(to, subject, body, path2attach, nameDest, cc);
	}

	private void innerSendMail(final List< String > to, final String subject, final String body,
			final List< InputStream > path2attach, final List< String > nameDest, final List< String > cc )
			throws IllegalArgumentException, MessagingException, IOException {
		StringBuffer sBTo = new StringBuffer("{");
		InputStream iS = null;
		String prefix = "[SendMail::innerSendMail] ";

		if (to.get(0) == null || to.get(0).trim().length() == 0) {
			LOG.error(prefix + "'to' not received");
			throw new IllegalArgumentException("No \"to\" received!");
		}
		if (subject == null || subject.trim().length() == 0) {
			LOG.error(prefix + "'subject' not received");
			throw new IllegalArgumentException("No \"subject\" received!");
		}
		if (body == null || body.trim().length() == 0) {
			LOG.error(prefix + "'body' not received");
			throw new IllegalArgumentException("No \"body\" received!");
		}

		// Nivelamos las direcciones.
		NormalizedAddressLists resultado = this.normalizarDestinatarios(to, cc);
		List<String> innerTo = resultado.getTo();
		List<String> innerCc = resultado.getCc();

		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		try {
			// Seteamos el emisor
			helper.setFrom((utf8) ? FormatDataUtils.toUTF8(from) : from);
			// Incluimos los distintos destinatarios
			for (String item : innerTo) {
				helper.addTo((utf8) ? FormatDataUtils.toUTF8(item) : item);
				sBTo.append(item + ",");
			}
			sBTo.replace(sBTo.length() - 1, sBTo.length(), "}");
			// Seteamos el asunto
			helper.setSubject((utf8) ? FormatDataUtils.toUTF8(subject) : subject);
			// Seteamos el cuerpo del mensaje
			helper.setText((utf8) ? FormatDataUtils.toUTF8(body) : body);
			// Si la copia viene rellena la incluimos
			if (innerCc != null && innerCc.size() > 0) {
				for (String item : innerCc) {
					helper.addCc(FormatDataUtils.toUTF8(item));
					LOG.trace(prefix + "including copy contacts");
				}
			}

			// Ficheros adjuntos

			if (path2attach != null && path2attach.size() > 0) {
				if (nameDest.size() == path2attach.size()) {
					try {
						for (int i = 0; i < nameDest.size(); i++) {
							helper.addAttachment(nameDest.get(i), 
									new ByteArrayResource(IOUtils.toByteArray(iS = path2attach.get(i))));
						}
					} finally {
						try {
							iS.close();
						} catch (IOException e) {
							LOG.error("Error al adjuntar ficheros en el correo", e);
						}
					}
				} else {
					LOG.error(prefix + "El numero de inputStream es diferente al numero de nameDest");
				}
			}

			// Envio del mensaje
			sender.send(message);
			LOG.trace(prefix + "Enviado correo a " + sBTo);
		} catch (MessagingException e) {
			throw new IllegalArgumentException("Error en envio de correo a " + sBTo, e);
		}

	}
	
	private void innerSendMailHtml(final List< String > to, final String subject, final String body,
			final List< InputStream > path2attach, final List< String > nameDest, final List< String > cc )
			throws IllegalArgumentException, MessagingException, IOException {
		StringBuffer sBTo = new StringBuffer("{");
		InputStream iS = null;
		String prefix = "[SendMail::innerSendMail] ";

		if (to.get(0) == null || to.get(0).trim().length() == 0) {
			LOG.error(prefix + "'to' not received");
			throw new IllegalArgumentException("No \"to\" received!");
		}
		if (subject == null || subject.trim().length() == 0) {
			LOG.error(prefix + "'subject' not received");
			throw new IllegalArgumentException("No \"subject\" received!");
		}
		if (body == null || body.trim().length() == 0) {
			LOG.error(prefix + "'body' not received");
			throw new IllegalArgumentException("No \"body\" received!");
		}

		// Nivelamos las direcciones.
		NormalizedAddressLists resultado = this.normalizarDestinatarios(to, cc);
		List<String> innerTo = resultado.getTo();
		List<String> innerCc = resultado.getCc();

		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);

		try {
			// Seteamos el emisor
			helper.setFrom((utf8) ? FormatDataUtils.toUTF8(from) : from);
			// Incluimos los distintos destinatarios
			for (String item : innerTo) {
				helper.addTo((utf8) ? FormatDataUtils.toUTF8(item) : item);
				sBTo.append(item + ",");
			}
			sBTo.replace(sBTo.length() - 1, sBTo.length(), "}");
			// Seteamos el asunto
			helper.setSubject((utf8) ? FormatDataUtils.toUTF8(subject) : subject);
			// Seteamos el cuerpo del mensaje
			helper.setText((utf8) ? FormatDataUtils.toUTF8(body) : body, true);
			// Si la copia viene rellena la incluimos
			if (innerCc != null && innerCc.size() > 0) {
				for (String item : innerCc) {
					helper.addCc(FormatDataUtils.toUTF8(item));
					LOG.trace(prefix + "including copy contacts");
				}
			}

			// Ficheros adjuntos

			if (path2attach != null && path2attach.size() > 0) {
				if (nameDest.size() == path2attach.size()) {
					try {
						for (int i = 0; i < nameDest.size(); i++) {
							helper.addAttachment(nameDest.get(i), 
									new ByteArrayResource(IOUtils.toByteArray(iS = path2attach.get(i))));
						}
					} finally {
						try {
							iS.close();
						} catch (IOException e) {
							LOG.error("Error al adjuntar ficheros en el correo", e);
						}
					}
				} else {
					LOG.error(prefix + "El numero de inputStream es diferente al numero de nameDest");
				}
			}

			// Envio del mensaje
			sender.send(message);
			LOG.trace(prefix + "Enviado correo a " + sBTo);
		} catch (MessagingException e) {
			throw new IllegalArgumentException("Error en envio de correo a " + sBTo, e);
		}

	}

	private NormalizedAddressLists normalizarDestinatarios(List<String> to, List<String> cc) {
		// Recibimos dos listas que hay que sustituir al final.

		List<String> innerTo = new ArrayList<String>();
		List<String> innerCc = new ArrayList<String>();
		List<String> innerCcFinal = new ArrayList<String>();

		// Normalizamos los "TO".
		if (to != null && !to.isEmpty()) {
			for (String person : to) {
				if (!innerTo.contains(person.trim())) {
					innerTo.add(person.trim());
				}
			}
		}

		// Normalizamos los "CC".
		if (cc != null && !cc.isEmpty()) {
			for (String person : cc) {
				if (!innerCc.contains(person.trim())) {
					innerCc.add(person.trim());
				}
			}
		}

		// Quitamos de los "CC" los que ya esten en "TO".
		if (innerCc != null && !innerCc.isEmpty()) {
			InternetAddress address = null;
			for (String person : innerCc) {
				try {
					address = new InternetAddress(person);
					LOG.trace("[SendMail::normalizarDestinatarios] + Added: [{}] == [{}]", address.getAddress(),
							address.getPersonal());
					if (!innerTo.contains(person.trim()) && !innerTo.contains(address.getAddress())) {
						innerCcFinal.add(person.trim());
					}
				} catch (AddressException e) {
					LOG.warn(
							"[SendMail::normalizarDestinatarios] Error({}): Error normalizando la direccion de correo electrónico[{}]: {}",
							e.getClass().getName(), person, e.getMessage());
				}
			}
		}

		// Finalmente, reasignamos.
		to = innerTo;
		cc = innerCcFinal;
		return new NormalizedAddressLists(innerTo, innerCcFinal);
	}

	private class NormalizedAddressLists {

		private List<String> to;
		private List<String> cc;

		public NormalizedAddressLists(List<String> to, List<String> cc) {
			super();
			this.to = to;
			this.cc = cc;
		}

		public List<String> getTo() {
			return this.to;
		}

		public List<String> getCc() {
			return this.cc;
		}

	}
}
