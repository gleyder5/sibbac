package sibbac.common.export.excel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.common.export.ExportBuilder;
import sibbac.common.export.LinesSet;

@Component
public class ExcelBuilderTitulares extends ExportBuilder {

	private static final Logger LOG = LoggerFactory.getLogger(ExcelBuilderTitulares.class);
	
	private static final String		RESULT_TITULARES_LIST_EXPORT		= "listTitulares";

	public ExcelBuilderTitulares() {
	}

	@Override
	public Object generate(final String name, Object target, final Map<String, ? extends Object> model,
			final List<String> dataSets)
			throws IOException {
		return this.generate(name, target, model, null);
	}

	@Override
	public Object generate(final String name, Object target, final Map<String, ? extends Object> model) throws IOException {

		HSSFWorkbook workbook = (HSSFWorkbook) target;
		if (model != null) {
	
			// El "name" es el "servicio.comando" que esperamos,
			// normalmente.
			LOG.trace("Nombre de la hoja excel principal [name=={}]", name);

			// Partimos del LinesSet "root".
			// Primero localizamos el "root" para pintarlo en la primera
			// hoja.
			
			List<HashMap<String,Object>> listaTitulares = (List<HashMap<String, Object>>) model.get(RESULT_TITULARES_LIST_EXPORT);
			String sheetName = null;
			int n = 0;
			sheetName = "Sheet " + n + " " + name;
			this.pintaDatos(workbook, workbook.createSheet(sheetName),  listaTitulares);
		} else {
			LOG.trace("No data!");
			Sheet sheet = workbook.createSheet(getNameForSheet(name));
			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellValue("No se han recibido datos");
		}
		return workbook;
	}
	protected void pintaDatos(HSSFWorkbook workbook, Sheet sheet, LinesSet set, List<LinesSet> lineas) {
		
	}
	
	protected void pintaDatos(HSSFWorkbook workbook, Sheet sheet, List<HashMap<String,Object>> listaTitulares) {
		LOG.trace("> Nombre Hoja Excel [name=={}]", sheet.getSheetName());


		LOG.trace("Generating an excel file with a sheet named: [{}]", sheet.getSheetName());
		LOG.trace("> Sheet: [{}]", sheet.getSheetName());

		
		int rowMin = 0;
		//Cabecera
		Row row = sheet.createRow(rowMin++);
		Cell cell = null;
		
		// Estilo de la cabecera  
		HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();  
		hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
		hssfCellStyleCabecera.setFillBackgroundColor(new HSSFColor.BLACK()  
		   .getIndex());  
		  
		 // Crear la fuente de la cabecera  
		HSSFFont hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.WHITE.index);  
		hssfCellStyleCabecera.setFont(hssfFont);
		
		
		int cellnum = 0;
		cell = row.createCell(cellnum);
		cell.setCellValue("Error");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 1
		cell = row.createCell(cellnum);
		cell.setCellValue("Booking");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 2
		cell = row.createCell(cellnum);
		cell.setCellValue("Ref. Cliente");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 3
		cell = row.createCell(cellnum);
		cell.setCellValue("CCV");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 4
		cell = row.createCell(cellnum);
		cell.setCellValue("Sentido");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 5
		cell = row.createCell(cellnum);
		cell.setCellValue("Isin");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 6
		cell = row.createCell(cellnum);
		cell.setCellValue("Titulos");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 7
		cell = row.createCell(cellnum);
		cell.setCellValue("Mercado");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 8
		cell = row.createCell(cellnum);
		cell.setCellValue("Nombre");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 9
		cell = row.createCell(cellnum);
		cell.setCellValue("1erApellido");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//10
		cell = row.createCell(cellnum);
		cell.setCellValue("2ºApellido");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//11
		cell = row.createCell(cellnum);
		cell.setCellValue("T.Documento");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//12
		cell = row.createCell(cellnum);
		cell.setCellValue("Documento");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//13
		cell = row.createCell(cellnum);
		cell.setCellValue("T.Persona");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//14
		cell = row.createCell(cellnum);
		cell.setCellValue("P.Residencia");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//15
		cell = row.createCell(cellnum);
		cell.setCellValue("T.Nacionalidad");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//16
		cell = row.createCell(cellnum);
		cell.setCellValue("Domicilio");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//17
		cell = row.createCell(cellnum);
		cell.setCellValue("Poblacion");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//18
		cell = row.createCell(cellnum);
		cell.setCellValue("Provincia");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//19
		cell = row.createCell(cellnum);
		cell.setCellValue("C.Postal");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//20
		cell = row.createCell(cellnum);
		cell.setCellValue("T.Titular");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//21
		cell = row.createCell(cellnum);
		cell.setCellValue("%Participación");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//22
		cell = row.createCell(cellnum);
		cell.setCellValue("F.Contratación");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//23
		cell = row.createCell(cellnum);
		cell.setCellValue("Alias");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//24
		cell = row.createCell(cellnum);
		cell.setCellValue("N.Alias");
		cell.setCellStyle(hssfCellStyleCabecera);
		
		HSSFCellStyle styleGroupRojo = workbook.createCellStyle();  
		
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.RED.index);  
		styleGroupRojo.setFont(hssfFont);
		
		HSSFCellStyle styleGroupAzul = workbook.createCellStyle();  
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.BLUE.index);  
		styleGroupAzul.setFont(hssfFont);
		
		rowMin = 1;
		for (HashMap<String, Object> titular : listaTitulares) {
			int incremento = 1;	
			row = sheet.createRow(rowMin);
			Object obj = titular.get("Error");
			cellnum = 0;// 0
			if (obj instanceof String)
			{
				cell = row.createCell(cellnum);
				cell.setCellValue(obj.toString());
			}//if (obj instanceof String)
			else
			{
				cell = row.createCell(cellnum);
				cell.setCellValue("ERROR");
				List<HashMap<String,String>> listaErrores = (List<HashMap<String,String>>)obj;
				Integer numRegistros = listaErrores.size();
				incremento+= numRegistros +1;
				int numFilaError = rowMin;
				Row rowAux = sheet.createRow(++numFilaError);
				cell = rowAux.createCell(1);
				cell.setCellValue("Código");
				cell.setCellStyle(styleGroupRojo);
				cell = rowAux.createCell(2);
				cell.setCellValue("Descripción");
				cell.setCellStyle(styleGroupRojo);
				CellRangeAddress region = new CellRangeAddress(numFilaError, numFilaError, 2, 24);
				sheet.addMergedRegion(region);
				
				for (HashMap<String, String> error : listaErrores)
				{
					rowAux = sheet.createRow(++numFilaError);
					cell = rowAux.createCell(1);
					cell.setCellValue(error.get("codigo"));
					cell.setCellStyle(styleGroupAzul);
					cell = rowAux.createCell(2);
					cell.setCellValue(error.get("descripcion"));
					cell.setCellStyle(styleGroupAzul);
					region = new CellRangeAddress(numFilaError, numFilaError, 2, 24);
					sheet.addMergedRegion(region);
				}//for (HashMap<String, String> error : listaErrores)
			}//else
			cellnum++;// 1
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Booking"));	
			cellnum++;// 2
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Ref_Cliente"));	
			cellnum++;// 3
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("CCV"));	
			cellnum++;// 4
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Sentido"));	
			cellnum++;// 5
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Isin"));	
			cellnum++;// 6
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Titulos"));
			cellnum++;// 7
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Mercado"));
			cellnum++;// 8
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Nombre"));
			cellnum++;// 9
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("PrimerApellido"));
			cellnum++;//10
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("SegundorApellido"));
			cellnum++;//11
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("T_Documento"));
			cellnum++;//12
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Documento"));
			cellnum++;//13
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("T_Persona"));
			cellnum++;//14
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("P_Residencia"));
			cellnum++;//15
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Nacionalidad"));
			cellnum++;//16
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Domicilio"));
			cellnum++;//17
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Poblacion"));
			cellnum++;//18
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Provincia"));
			cellnum++;//19
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("C_Postal"));
			cellnum++;//20
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("T_Titular"));
			cellnum++;//21
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Participacion_Por"));
			cellnum++;//22
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("F_Contratacion"));
			cellnum++;//23
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("Alias"));
			cellnum++;//24
			cell = row.createCell(cellnum);
			cell.setCellValue((String) titular.get("N_Alias"));
			rowMin+=incremento;
		}
		 short numeroCeldas = sheet.getRow(0).getLastCellNum();
		 for (int i=0; i<numeroCeldas;i++)
		 {
			 sheet.autoSizeColumn(i);
		 }
		
	}

	@Override
	public ByteArrayOutputStream getBaos(final Object target) {
		if (target == null) {
			return null;
		}

		HSSFWorkbook workbook = (HSSFWorkbook) target;
		ByteArrayOutputStream baos = new ByteArrayOutputStream(INITIAL_BAOS_CAPACITY);
		try {
			workbook.write(baos);
			baos.flush();
			baos.close();
		} catch (Exception e) {
			LOG.error("> ERROR({}): Errors generating the XLS: {}", e.getClass().getName(), e.getMessage());
		} finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e) {
					// Nothing
				}
			}
		}
		return baos;
	}

}
