package sibbac.common.export.excel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.XLSFactura;
import sibbac.common.XLSInteresesS3;
import sibbac.common.export.ExportBuilder;
import sibbac.common.export.ExportLines;
import sibbac.common.export.LinesSet;

@Component
public class ExcelBuilder extends ExportBuilder {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para escribir en el log. */
  private static final Logger LOG = LoggerFactory.getLogger(ExcelBuilder.class);

  /** Formato para los número sin decimales. */
  private static final String NUMBER_FORMAT = "#,##0";

  /** Formato para los número con decimales. */
  private static final String DECIMAL_FORMAT = "#,##0.00######";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÉTODOS IMPLEMENTADOS

  /*
   * @see sibbac.common.export.ExportBuilder#generate(java.lang.String, java.lang.Object, java.util.Map)
   */
  @Override
  public Object generate(final String name, Object target, final Map<String, ? extends Object> model) throws IOException {
    return this.generate(name, target, model, null);
  } // generate

  /*
   * @see sibbac.common.export.ExportBuilder#generate(java.lang.String, java.lang.Object, java.util.Map, java.util.List)
   */
  @Override
  public Object generate(final String name, Object target, final Map<String, ? extends Object> model, final List<String> dataSets) throws IOException {
    HSSFWorkbook workbook = (HSSFWorkbook) target;
    if (model != null) {
      List<List<LinesSet>> lineas = ExportLines.getLinesFor(model, dataSets);

      if (lineas == null) {
        LOG.trace("No data!");
        Sheet sheet = workbook.createSheet(getNameForSheet(name));
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("No se han recibido datos");
      } else {
        // El "name" es el "servicio.comando" que esperamos, normalmente
        LOG.trace("Nombre de la hoja excel principal [name=={}]", name);

        // Partimos del LinesSet "root".
        // Primero localizamos el "root" para pintarlo en la primera hoja.
        LinesSet root = null;
        String sheetName = null;
        int n = 0;
        for (List<LinesSet> conjunto : lineas) {
          root = ExportLines.findRootLineSet(conjunto);
          if (root != null) {
            n++;
            // A pintar en la hoja.
            sheetName = "Sheet " + n + " " + name;
            this.pintaDatos(workbook, workbook.createSheet(sheetName), root, conjunto);
          }
        }
      } // else
    } else {
      LOG.trace("No data!");
      Sheet sheet = workbook.createSheet(getNameForSheet(name));
      Row row = sheet.createRow(0);
      Cell cell = row.createCell(0);
      cell.setCellValue("No se han recibido datos");
    } // else
    return workbook;
  } // generate

  /*
   * @see sibbac.common.export.ExportBuilder#getBaos(java.lang.Object)
   */
  @Override
  public ByteArrayOutputStream getBaos(final Object target) {
    if (target == null) {
      return null;
    }

    HSSFWorkbook workbook = (HSSFWorkbook) target;
    ByteArrayOutputStream baos = new ByteArrayOutputStream(INITIAL_BAOS_CAPACITY);

    try {
      workbook.write(baos);
      baos.flush();
      baos.close();
    } catch (Exception e) {
      LOG.error("> ERROR({}): Errors generating the XLS: {}", e.getClass().getName(), e.getMessage());
    } finally {
      if (workbook != null) {
        try {
          workbook.close();
        } catch (IOException e) {
          // Se ignora
        }
      }
    } // finally

    return baos;
  } // getBaos

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ FACTURA

  /**
   * Genera la excel de una factura.
   * 
   * @param nombreHojaExcel Nombre de la hoja.
   * @param lineas Lineas a escribir.
   * @param lineasCabeceras Mapa de textos localizados de la cabecera.
   * @param locale Locale del destinatario de la Excel.
   * @return <code>java.io.ByteArrayOutputStream - </code>Array de bytes que contienen la excel formada.
   * @throws SIBBACBusinessException Si ocurre algún error durante el proceso.
   */
  public ByteArrayOutputStream generaFactura(final String nombreHojaExcel,
                                             final List<XLSFactura> lineas,
                                             final Map<String, String> lineasCabeceras,
                                             Locale locale) throws SIBBACBusinessException {
    if (lineas == null || lineas.isEmpty()) {
      throw new SIBBACBusinessException("No data received!");
    }

    ByteArrayOutputStream os = null;
    HSSFWorkbook workbook = new HSSFWorkbook();

    // HSSFDataFormatter hssfdf = new HSSFDataFormatter(locale);
    // hssfdf.addFormat(NUMBER_FORMAT, new DecimalFormat(NUMBER_FORMAT));
    // hssfdf.addFormat(DECIMAL_FORMAT, new DecimalFormat(DECIMAL_FORMAT));

    // La hoja
    String name = (nombreHojaExcel != null && nombreHojaExcel.length() > 0) ? nombreHojaExcel : "Datos de Factura";
    final HSSFSheet sheet = workbook.createSheet(name);

    // Headers...
    int rowNum = escribirCabeceraFacturas(lineasCabeceras, sheet);
    escribirLineasFactura(lineas, rowNum, sheet, workbook);

    // Se ajusta el ancho de las columnas al contenido
    for (int i = 0; i < lineasCabeceras.size() - 1; i++) {
      sheet.autoSizeColumn(i);
    }

    // Export...
    os = this.getBaos(workbook);
    if (workbook != null) {
      try {
        workbook.close();
      } catch (IOException e) {
        // Se ignora
      } // catch
    } // if
    return os;
  } // generaFactura

  /**
   * Escribe la línea de cabecera de la Excel de la factura.
   * 
   * @param lineasCabeceras Mapa con los textos localizados.
   * @param sheet Hoja de la Excel el donde crear la cabecera.
   * @return Número de columnas escritas.
   */
  private int escribirCabeceraFacturas(final Map<String, String> lineasCabeceras, final HSSFSheet sheet) {

    int rowNum = 0;
    int cellNum = 0;
    final Row fila = sheet.createRow(rowNum++);
    if (MapUtils.isNotEmpty(lineasCabeceras)) {
      String header = lineasCabeceras.get("FACT_EXCL_FECHA_OPERACION");
      Cell celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_REFERENCIA");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_COMPRA_VENTA");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_ISIN");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_NB_VALOR");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_MERCADO");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_SUBCUENTA");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_NEMOTECNICO_GU");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_TOTAL_TITULOS");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_PRECIO_MEDIO");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("FACT_EXCL_CORRETAJE_TOTAL");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      // header = lineasCabeceras.get("FACT_EXCL_CORRETAJE_ORDEN");
      // celda = fila.createCell(cellNum++);
      // celda.setCellValue(header);
    }
    return rowNum;
  } // escribirCabeceraFacturas

  /**
   * Escribe las líneas de la Excel.
   * 
   * @param lineas Líneas con los datos a escribir.
   * @param rowNum Número de fila a partir de la que empezar a escribir.
   * @param sheet Hoja de la Excel el donde escribir.
   * @param workbook Libro que contiene la hoja.
   */
  private void escribirLineasFactura(final List<XLSFactura> lineas, int rowNum, final HSSFSheet sheet, HSSFWorkbook workbook) {

    // poi-3.12-javadocs/org/apache/poi/ss/usermodel/BuiltinFormats.html
    HSSFDataFormat hSSFDataFormat = workbook.createDataFormat();
    hSSFDataFormat.getFormat(NUMBER_FORMAT); // Ya existe por defecto, es el 3
    hSSFDataFormat.getFormat(DECIMAL_FORMAT); // Se crea nuevo

    CellStyle cellStyleNum = workbook.createCellStyle();
    cellStyleNum.setDataFormat(hSSFDataFormat.getFormat(NUMBER_FORMAT));
    CellStyle cellStyleDecimal = workbook.createCellStyle();
    cellStyleDecimal.setDataFormat(hSSFDataFormat.getFormat(DECIMAL_FORMAT));

    // Lineas...
    for (final XLSFactura linea : lineas) {
      int cellnum = 0;
      Row fila = sheet.createRow(rowNum++);

      Cell celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getFechaOperacion());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getNuestraReferencia());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getCompra());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getIsin());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getNombreValor());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getMercado());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getTitular());

      celda = fila.createCell(cellnum++);
      celda.setCellValue(linea.getReferenciaAsignacion());

      celda = fila.createCell(cellnum++);
      celda.setCellStyle(cellStyleNum);
      celda.setCellValue(linea.getTitulosEjecutadosPorOrden().doubleValue());

      celda = fila.createCell(cellnum++);
      celda.setCellStyle(cellStyleDecimal);
      celda.setCellValue(linea.getPrecioMedioOrden().doubleValue());

      celda = fila.createCell(cellnum++);
      celda.setCellStyle(cellStyleDecimal);
      celda.setCellValue(linea.getCorretajeTotal().doubleValue());

      // celda = fila.createCell(cellnum++);
      // celda.setCellValue(linea.getCorretajePorEjecucion());
    } // for
  } // escribirLineasFactura

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INFORME INTERESES

  /**
   * Genera la excel de intereses.
   * 
   * @param nombreHojaExcel Nombre de la hoja.
   * @param lineas Lineas a escribir.
   * @param lineasCabeceras Mapa de textos localizados de la cabecera.
   * @return <code>java.io.ByteArrayOutputStream - </code>Array de bytes que contienen la excel formada.
   * @throws SIBBACBusinessException Si ocurre algún error durante el proceso.
   */
  public ByteArrayOutputStream generaInformeIntereses(final String nombreHojaExcel,
                                                      final List<XLSInteresesS3> lineas,
                                                      final Map<String, String> lineasCabeceras) throws SIBBACBusinessException {
    // Check data...
    if (lineas == null || lineas.isEmpty()) {
      throw new SIBBACBusinessException("No data received!");
    }

    ByteArrayOutputStream os = null;
    final HSSFWorkbook workbook = new HSSFWorkbook();

    // La hoja
    String name = (nombreHojaExcel != null && nombreHojaExcel.length() > 0) ? nombreHojaExcel : "Datos de Factura";
    final HSSFSheet sheet = workbook.createSheet(name);

    // Headers...
    int rowNum = escribirCabeceraIntereses(lineasCabeceras, sheet);

    escribirLineasIntereses(lineas, rowNum, sheet);
    // Export...
    os = this.getBaos(workbook);

    if (workbook != null) {
      try {
        workbook.close();
      } catch (IOException e) {
        // Nothing
      }
    }

    return os;
  } // generaInformeIntereses

  /**
   * Escribe la línea de cabecera de la Excel de intereses.
   * 
   * @param lineasCabeceras Mapa con los textos localizados.
   * @param sheet Hoja de la Excel el donde crear la cabecera.
   * @return Número de columnas escritas.
   */
  private int escribirCabeceraIntereses(final Map<String, String> lineasCabeceras, final HSSFSheet sheet) {
    int rowNum = 0;
    int cellNum = 0;
    final Row fila = sheet.createRow(rowNum++);
    if (MapUtils.isNotEmpty(lineasCabeceras)) {

      String header = lineasCabeceras.get("INTERESES_EXCL_REFERENCIA");
      Cell celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_SENTIDO");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_ISIN");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_NB_VALOR");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_MERCADO");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_TOTAL_TITULOS");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_PRECIO_MEDIO");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_IMPORTE_NETO_TOTAL");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_SUBCUENTA");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_FECHA_CONTRATACION");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_FECHA_LIQUIDACION");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_DIAS_DIFERENCIA");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_TIPO_INTERES");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);

      header = lineasCabeceras.get("INTERESES_EXCL_INTERES_DEUDORES");
      celda = fila.createCell(cellNum++);
      celda.setCellValue(header);
    }
    return rowNum;
  } // escribirCabeceraIntereses

  /**
   * Escribe las líneas de la Excel.
   * 
   * @param lineas Líneas con los datos a escribir.
   * @param rowNum Número de fila a partir de la que empezar a escribir.
   * @param sheet Hoja de la Excel el donde escribir.
   */
  private void escribirLineasIntereses(final List<XLSInteresesS3> lineas, int rownum, final HSSFSheet sheet) {
    for (final XLSInteresesS3 linea : lineas) {
      final Row fila = sheet.createRow(rownum++);
      int cellNum = 0;
      Cell celda = null;
      if (linea.getNuestraReferencia().toUpperCase().contains("TOTAL")) {
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getNuestraReferencia() + ":" + linea.getInteresAplicado());
      } else {
        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getNuestraReferencia());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getCompra());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getIsin());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getNombreValor());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getMercado());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getTotalTitulos());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getPrecioMedioOrden());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getImporteNetoLiquidado());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getTitular());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getfechaContratacion());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getFechaOperacion());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getFechaFiquidacion());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getDifereciaDias());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getTipoInteres());

        celda = fila.createCell(cellNum++);
        celda.setCellValue(linea.getInteresAplicado());
      }
    } // for
  } // escribirLineasIntereses

} // ExcelBuilder
