package sibbac.common.export;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.export.LinesSet.Child;

public abstract class ExportBuilder {
	private static final Logger LOG = LoggerFactory.getLogger(ExportBuilder.class.getName());

	public static final String MODEL_KEY_NAME = "name";

	public static final String MODEL_KEY_HEADER = "header";

	public static final String MODEL_KEY_DATA = "data";

	public static final String MODEL_KEY_DATASETS = "datasets";

	public static final int INITIAL_BAOS_CAPACITY = 4096;

	protected static final String DEFAULT_NAME_SHEET = "default";

	public static final String MODEL_DTO_TYPE = "dtoType";

	public static final String MODEL_KEY_MODEL = "keyModel";

	/**
	 * Method to retrieve an object (<i>{#link List} or a {#link Map}</i>) from
	 * a model and returns an object of type <i>{#link HSSFWorkbook}, for Excel
	 * files, or {#link Document} fpr PDF documents.
	 */
	public abstract Object generate(final String name, Object target, final Map<String, ? extends Object> model)
			throws IOException;

	public abstract Object generate(final String name, Object target, final Map<String, ? extends Object> model,
			final List<String> dataSets) throws IOException;

	/**
	 * Method to retrieve the generated object (<i>{#link HSSFWorkbook} or a
	 * {#link Document}</i>) into an {#link ByteArrayOutputStream}.
	 */
	public abstract ByteArrayOutputStream getBaos(final Object target);

	protected String getNameForSheet(String name) {
		String result;
		if ("".equals(name) || name == null) {
			result = DEFAULT_NAME_SHEET;
		} else {
			result = name;
		}
		return result;

	}

	protected void pintaDatos(HSSFWorkbook workbook, Sheet sheet, LinesSet set, List<LinesSet> lineas) {
		LOG.trace("> Nombre Hoja Excel [name=={}]", sheet.getSheetName());

		List<String> header = set.getHeaders();
		List<List<String>> data = set.getLines();
		Map<Integer, Child> children = set.getChildren();

		if (children != null) {
			LOG.trace("Pintando la información de hijos...");
			Set<Integer> keys = children.keySet();
			Child hijo = null;
			for (Integer key : keys) {
				hijo = children.get(key);
				LOG.trace("+ Data: [{}]: {}", key, hijo);
			}
		}

		LOG.trace("Generating an excel file with a sheet named: [{}]", sheet.getSheetName());
		LOG.trace("> Headers: [{}]", header);
		LOG.trace("> Data: [{}]", data);

		// keep "MAX_ROWS_IN_MEMORY" rows in memory, exceeding rows will
		// be flushed to disk

		LOG.trace("> Sheet: [{}]", sheet.getSheetName());

		// Header?
		int rowMin = 0;
		if (header != null) {
			LOG.trace("+ Generating headers...");
			Row row = sheet.createRow(rowMin++);
			Cell cell = null;
			Child hijo = null;
			int cellnum = 0;
			for (String h : header) {
				hijo = children.get(new Integer(cellnum));
				cell = row.createCell(cellnum++);
				cell.setCellValue((h != null) ? h : "");
				LOG.trace("> Cell: [{}/{}: {}]", cellnum, cell.getRowIndex(), cell.getStringCellValue());
			}
			if (hijo != null && hijo.hasChildren()) {
				cell = row.createCell(cellnum);
				cell.setCellValue("Children");
			}
		}

		// Data?
		if (data != null) {
			LOG.trace("+ Generating rows...");
			Row row = null;
			Cell cell = null;
			int nLinea = 0;
			long idLinea = 0;
			Child hijo = null;
			rowMin = 1;
			for (List<String> objs : data) {
				hijo = children.get(new Integer(nLinea++));
				row = sheet.createRow(rowMin++);
				int cellnum = 0;
				for (Object obj : objs) {
					cell = row.createCell(cellnum++);
					cell.setCellValue((obj != null) ? obj.toString() : "");
					// cell.setCellValue( new HSSFRichTextString(
					// (obj!=null)?obj.toString():"" ) );
					LOG.trace("> Cell: [{}/{}: {}]", cellnum, cell.getRowIndex(), cell.getStringCellValue());
				}

				// Si tiene hijos, los pintamos en otra hoja
				if (hijo.hasChildren()) {
					// Tenemos una linea padre.
					idLinea = hijo.getIdLinea();
					cell = row.createCell(cellnum++);
					cell.setCellValue((idLinea == 0) ? "" : "" + idLinea);
					// Ahora, para cada LinesSet que no sea "root", tambien lo
					// pintamos pero en otra hoja.
					Sheet newSheet = workbook.createSheet("" + idLinea);
					// Hay que buscar el LinesSet que tenga como idPadre, el
					// idLinea actual.
					LinesSet lineSetHijo = this.findLinesSetHijo(idLinea, lineas);
					if (lineSetHijo != null) {
						this.pintaDatos(workbook, newSheet, lineSetHijo, lineas);
					}
				}
			}
		}
	}

	private LinesSet findLinesSetHijo(long idLineaPadre, List<LinesSet> lineas) {
		LinesSet found = null;
		Map<Integer, Child> hijos = null;
		int total = 0;
		Child lineaHija = null;
		long idPadre = 0;
		List<String> headers, data;
		for (LinesSet ls : lineas) {
			// Hay que buscar en el children, las lineas que tienen este ID
			// Padre y devolverlas.
			hijos = ls.getChildren();
			if (hijos == null || hijos.isEmpty()) {
				continue;
			}
			total = hijos.size();
			for (int n = 0; n < total; n++) {
				lineaHija = hijos.get(new Integer(n));
				idPadre = lineaHija.getIdPadre();
				// Si coincide, cogemos los headers y esta linea.
				if (idPadre == idLineaPadre) {
					headers = ls.getHeaders();
					data = ls.getLines().get(n);

					if (found == null) {
						found = new LinesSet();
						found.addHeaders(headers);
					}
					found.addLine(data, lineaHija.hasChildren(), lineaHija.getIdLinea(), lineaHija.getIdPadre());
				}
			}
		}
		return found;
	}

}
