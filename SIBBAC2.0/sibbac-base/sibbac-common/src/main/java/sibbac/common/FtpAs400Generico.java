package sibbac.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FtpAs400Generico {

  protected static final Logger LOG = LoggerFactory.getLogger(FtpAs400Generico.class);

  public FTPClient connect(String url, String port, String username, String password, String folderDestino)
      throws IOException {
    FTPClient ftp = new FTPClient();
    try {
      ftp.connect(url, Integer.parseInt(port));
    }
    catch (Exception e) {
      LOG.error("No se puede ejecutar esta tarea porque no se conecta al servidor FTP ");
      return null;
    }
    if (!FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
      LOG.error("Exception in connecting to FTP Server");
      try {
        ftp.disconnect();
      }
      catch (Exception e) {
        LOG.error("Exception in disconnect FTP Server", e);
      }
      throw new IOException("Exception in connecting to FTP Server");
    }
    ftp.login(username, password);
    ftp.mkd(folderDestino);
    ftp.changeWorkingDirectory(folderDestino);
    ftp.setFileType(FTP.BINARY_FILE_TYPE);
    ftp.enterLocalPassiveMode();
    return ftp;
  }

  public boolean send(FTPClient ftp, List<FtpSendFileDTO> list) throws IOException {
    boolean enviado;
    Path fileTratados;

    for (FtpSendFileDTO file : list) {
      if (Files.exists(file.getFile())) {
        enviado = false;
        try (InputStream iS = Files.newInputStream(file.getFile(), StandardOpenOption.READ);) {

          try {
            LOG.info("[send] enviando fichero ftp: {}", file.getNombreDestino());
            enviado = ftp.storeFile(file.getNombreDestino(), iS);

          }
          catch (IOException e) {
            LOG.error("No copia el fichero {} al FTP Server", file.getFile().toString(), e);
            throw e;
          }
        }
        catch (FileNotFoundException e) {
          LOG.error("No abre para lectura el local {}", file.getFile().toString(), e);
          throw e;
        }
        if (enviado) {
          if (file.getRutaTratados() != null) {
            fileTratados = Paths.get(file.getRutaTratados().toString(), file.getFile().getFileName().toString());
            Files.move(file.getFile(), fileTratados, StandardCopyOption.ATOMIC_MOVE);
          }
        }
      }
    }
    return true;
  }
}
