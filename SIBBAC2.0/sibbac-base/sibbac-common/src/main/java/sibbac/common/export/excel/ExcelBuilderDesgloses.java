package sibbac.common.export.excel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.common.export.ExportBuilder;

@Component
public class ExcelBuilderDesgloses extends ExportBuilder {
	
	private static final Logger LOG = LoggerFactory.getLogger(ExcelBuilderDesglosesHuerfanos.class);
	private static final String	RESULT_DESGLOSES_LIST_EXPORT = "desglosesListExport";
	
	public ExcelBuilderDesgloses() {
	}

	@Override
	public Object generate(final String name, Object target, final Map<String, ? extends Object> model,
			final List<String> dataSets)
			throws IOException {
		return this.generate(name, target, model, null);
	}

	@Override
	public Object generate(final String name, Object target, final Map<String, ? extends Object> model) throws IOException {

		HSSFWorkbook workbook = (HSSFWorkbook) target;
		if (model != null) {
	
			// El "name" es el "servicio.comando" que esperamos,
			// normalmente.
			LOG.trace("Nombre de la hoja excel principal [name=={}]", name);

			// Partimos del LinesSet "root".
			// Primero localizamos el "root" para pintarlo en la primera
			// hoja.
			
			List<HashMap<String,Object>> listaDesgloses = (List<HashMap<String, Object>>) model.get(RESULT_DESGLOSES_LIST_EXPORT);
			String sheetName = null;
			int n = 0;
			sheetName = "Sheet " + n + " " + name;
			this.pintaDatos(workbook, workbook.createSheet(sheetName),  listaDesgloses);
		} else {
			LOG.trace("No data!");
			Sheet sheet = workbook.createSheet(getNameForSheet(name));
			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellValue("No se han recibido datos");
		}
		return workbook;
	}
	
	protected void pintaDatos(HSSFWorkbook workbook, Sheet sheet, List<HashMap<String,Object>> listaDesgloses) {
		LOG.trace("> Nombre Hoja Excel [name=={}]", sheet.getSheetName());


		LOG.trace("Generating an excel file with a sheet named: [{}]", sheet.getSheetName());
		LOG.trace("> Sheet: [{}]", sheet.getSheetName());

		
		int rowMin = 0;
		//Cabecera
		Row row = sheet.createRow(rowMin++);
		Cell cell = null;
		
		// Estilo de la cabecera  
		HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();  
		hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
		hssfCellStyleCabecera.setFillBackgroundColor(new HSSFColor.BLACK()  
		   .getIndex());  
		  
		 // Crear la fuente de la cabecera  
		HSSFFont hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.WHITE.index);  
		hssfCellStyleCabecera.setFont(hssfFont);
		
		
		int cellnum = 0;
		cell = row.createCell(cellnum);
		cell.setCellValue("N. Orden");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 1
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod. SV");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 2
		cell = row.createCell(cellnum);
		cell.setCellValue("F. ejecución");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 3
		cell = row.createCell(cellnum);
		cell.setCellValue("Tipo operación");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 4
		cell = row.createCell(cellnum);
		cell.setCellValue("Titulos");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 5
		cell = row.createCell(cellnum);
		cell.setCellValue("Titulos SV");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 6
		cell = row.createCell(cellnum);
		cell.setCellValue("Nominal");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 7
		cell = row.createCell(cellnum);
		cell.setCellValue("Isin");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 8
		cell = row.createCell(cellnum);
		cell.setCellValue("descripcion");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 9
		cell = row.createCell(cellnum);
		cell.setCellValue("Bolsa negociación");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//10
		cell = row.createCell(cellnum);
		cell.setCellValue("Tipo saldo");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//11
		cell = row.createCell(cellnum);
		cell.setCellValue("E. Liquidadora");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//12
		cell = row.createCell(cellnum);
		cell.setCellValue("Tipo cambio");
		cell.setCellStyle(hssfCellStyleCabecera);
		
		HSSFCellStyle styleGroupRojo = workbook.createCellStyle();  
		
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.RED.index);  
		styleGroupRojo.setFont(hssfFont);
		
		HSSFCellStyle styleGroupVerde = workbook.createCellStyle();  
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.GREEN.index);  
		styleGroupVerde.setFont(hssfFont);
		
		rowMin = 1;
		short numeroCeldas = 12;
		for (HashMap<String, Object> desglose : listaDesgloses) {	
			row = sheet.createRow(rowMin);
			
			cellnum = 0;// 0
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("numOrden"));
			
			cellnum++;// 1
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("codsv"));
			
			cellnum++;// 2
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("fejecucion"));
			
			cellnum++;// 3
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("tipoOperacion"));
			if ((boolean) desglose.get("bOkTipoOperacion"))
			{
				cell.setCellStyle(styleGroupVerde);
			}
			else
			{
				cell.setCellStyle(styleGroupRojo);
			}
			
			float titulos = (float) desglose.get("titulos");
			float titulosSV = (float) desglose.get("titulosSV");
			cellnum++;// 4
			cell = row.createCell(cellnum);
			cell.setCellValue(titulos);
			if (titulos == titulosSV)
			{
				cell.setCellStyle(styleGroupVerde);
			}
			else
			{
				cell.setCellStyle(styleGroupRojo);
			}
			
			cellnum++;// 5
			cell = row.createCell(cellnum);
			cell.setCellValue(titulosSV);
			
			cellnum++;// 6
			cell = row.createCell(cellnum);
			cell.setCellValue((float) desglose.get("nominal"));
			
			cellnum++;// 7
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("isin"));
			
			if ((boolean) desglose.get("bOkIsin"))
			{
				cell.setCellStyle(styleGroupVerde);
			}
			else
			{
				cell.setCellStyle(styleGroupRojo);
			}
			
			cellnum++;// 8
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("desIsin"));
			
			cellnum++;// 9
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("bolsa"));
			
			if ((boolean) desglose.get("bOkBolsa"))
			{
				cell.setCellStyle(styleGroupVerde);
			}
			else
			{
				cell.setCellStyle(styleGroupRojo);
			}
			
			cellnum++;//10
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("tipoSaldo"));
			
			if ((boolean) desglose.get("bOkTipoSaldo"))
			{
				cell.setCellStyle(styleGroupVerde);
			}
			else
			{
				cell.setCellStyle(styleGroupRojo);
			}
			
			cellnum++;//11
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("entidad"));
			
			cellnum++;//12
			cell = row.createCell(cellnum);
			cell.setCellValue((String) desglose.get("tipoCambio"));
			
			if ((boolean) desglose.get("bOkTipoCambio"))
			{
				cell.setCellStyle(styleGroupVerde);
			}
			else
			{
				cell.setCellStyle(styleGroupRojo);
			}
			
			List<HashMap< String, Object >> listaHashMapDesgloses = (List<HashMap< String, Object >>)desglose.get("listaDesgloses");
			if (listaHashMapDesgloses != null)
			{
				Iterator<HashMap<String, Object>> iterator = listaHashMapDesgloses.iterator();
				rowMin++;
				row = sheet.createRow(rowMin);
				cellnum = 0;
				cell = row.createCell(cellnum);
				cell.setCellValue("Fecha");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 1
				cell = row.createCell(cellnum);
				cell.setCellValue("N. referencia");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 2
				cell = row.createCell(cellnum);
				cell.setCellValue("Sentido");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 3
				cell = row.createCell(cellnum);
				cell.setCellValue("Bolsa negociación");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 4
				cell = row.createCell(cellnum);
				cell.setCellValue("Isin");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 5
				cell = row.createCell(cellnum);
				cell.setCellValue("Descripción");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 6
				cell = row.createCell(cellnum);
				cell.setCellValue("Titulos");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 7
				cell = row.createCell(cellnum);
				cell.setCellValue("Precio");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 8
				cell = row.createCell(cellnum);
				cell.setCellValue("Tipo Saldo");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;// 9
				cell = row.createCell(cellnum);
				cell.setCellValue("CCV");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;//10
				cell = row.createCell(cellnum);
				cell.setCellValue("Titular");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;//11
				cell = row.createCell(cellnum);
				cell.setCellValue("Entidad liquidadora");
				cell.setCellStyle(hssfCellStyleCabecera);
				cellnum++;//12
				cell = row.createCell(cellnum);
				cell.setCellValue("Tipo cambio");
				cell.setCellStyle(hssfCellStyleCabecera);
				while (iterator.hasNext())
				{
					HashMap<String, Object> desgloseHashMap = iterator.next();
					rowMin++;
					row = sheet.createRow(rowMin);
					cellnum = 0;// 0
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("fejecucion"));
					cellnum++;// 1
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("numReferencia"));
					cellnum++;// 2
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("tipoOperacion"));
					cellnum++;// 3
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("bolsa"));
					cellnum++;// 4
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("isin"));
					cellnum++;// 5
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("desIsin"));
					cellnum++;// 6
					cell = row.createCell(cellnum);
					cell.setCellValue((float) desgloseHashMap.get("titulos"));
					cellnum++;// 7
					cell = row.createCell(cellnum);
					cell.setCellValue((float) desgloseHashMap.get("precio"));
					cellnum++;// 8
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("tipoSaldo"));
					cellnum++;// 9
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("ccv"));
					cellnum++;//10
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("titular"));
					cellnum++;//11
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("entidad"));
					cellnum++;//12
					cell = row.createCell(cellnum);
					cell.setCellValue((String) desgloseHashMap.get("tipoCambio"));
				}
			}
			rowMin++;
		}
		 for (int i=0; i<numeroCeldas;i++)
		 {
			 sheet.autoSizeColumn(i);
		 }
		
	}

	@Override
	public ByteArrayOutputStream getBaos(Object target) {
		// TODO Auto-generated method stub
		return null;
	}

}
