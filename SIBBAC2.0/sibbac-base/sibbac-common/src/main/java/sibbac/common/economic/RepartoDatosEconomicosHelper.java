package sibbac.common.economic;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sibbac.common.SIBBACBusinessException;

/**
 * @author xIS16630, jonatan san andres gil
 *
 */
public class RepartoDatosEconomicosHelper {

  /** Referencia para la inserción de logs. */
  private static final Logger LOG = LoggerFactory.getLogger(RepartoDatosEconomicosHelper.class);

  public void repartirBigDecimals(Object origen, List<Object> listDestino, String nombreCampoOrigenProcentaje,
      String nombreCampoDestinoPorcentaje, List<String> listNombreCampoOrigen, List<String> listNombreCampoDestino,
      int decimalesReparto, RoundingMode roundingMode) throws SIBBACBusinessException {
    if (listNombreCampoOrigen.size() == listNombreCampoDestino.size()) {
      for (int i = 0; i < listNombreCampoOrigen.size(); i++) {
        this.repartirBigDecimals(origen, listDestino, nombreCampoOrigenProcentaje, nombreCampoDestinoPorcentaje,
            listNombreCampoOrigen.get(i), listNombreCampoDestino.get(i), decimalesReparto, roundingMode);

      }
    }
  }

  public BigDecimal repartirBigDecimals(Object origen, List<Object> listaReparto, String nombreCampoOrigenProcentaje,
      String nombreCampoDestinoPorcentaje, String nombreCampoOrigen, String nombreCampoDestino, int decimalesReparto,
      RoundingMode roundingMode) throws SIBBACBusinessException {
    try {
      Method getValorOrigen = origen.getClass().getMethod(
          String.format("get%s%s", nombreCampoOrigen.substring(0, 1).toUpperCase(), nombreCampoOrigen.substring(1)));
      BigDecimal valorOrigen = ((BigDecimal) getValorOrigen.invoke(origen)).setScale(decimalesReparto, roundingMode);
      return this.repartirBigDecimals(origen, listaReparto, valorOrigen, nombreCampoOrigenProcentaje,
          nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto, roundingMode);
    }
    catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido repartir el campo: '%s'", nombreCampoOrigen), e);
    }
  }

  public BigDecimal repartirBigDecimals(Object origen, List<?> listaReparto, BigDecimal valorRepartirOrigen,
      String nombreCampoOrigenProcentaje, String nombreCampoDestinoPorcentaje, String nombreCampoDestino,
      int decimalesReparto, RoundingMode roundingMode) throws SIBBACBusinessException {
    LOG.trace("[repartirBigDecimals] inicio campo destino: {}", nombreCampoDestino);
    BigDecimal porcentaje;

    BigDecimal valorRepartirDestino;
    BigDecimal valorDestinoPorcentaje;
    BigDecimal repartoAcumulado = BigDecimal.ZERO;

    int decimalesPorcentajeReparto = 16;

    try {
      
      valorRepartirOrigen = valorRepartirOrigen.setScale(decimalesReparto, roundingMode);

      if (origen != null && listaReparto != null && !listaReparto.isEmpty()) {
        if (listaReparto.contains(origen)) {
          throw new SIBBACBusinessException("El registro origen no debe estar contenido entre los registros destino.");
        }
        List<Object> listDestino = new ArrayList<Object>(listaReparto);
        final Method setValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("set%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)), BigDecimal.class);

        final Method getValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)));

        final Method getPorcentajeOrigen = origen.getClass().getMethod(
            String.format("get%s%s", nombreCampoOrigenProcentaje.substring(0, 1).toUpperCase(),
                nombreCampoOrigenProcentaje.substring(1)));

        final Method getPorcentajeDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestinoPorcentaje.substring(0, 1).toUpperCase(),
                    nombreCampoDestinoPorcentaje.substring(1)));

        if (setValorDestino == null || getValorDestino == null || getPorcentajeOrigen == null
            || getPorcentajeDestino == null) {
          throw new SIBBACBusinessException(
              String
                  .format(
                      "Los nombres de los campos origen, destino y de calculo de porcentaje no son correctos[origen: %s, destino: %s, origen.porcentaje:%s, destino.porcentaje:%s, destino.campo:%s ]",
                      origen.getClass(), listDestino.get(0).getClass(), nombreCampoOrigenProcentaje,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino));
        }

        BigDecimal valorOrigenPorcentaje = (BigDecimal) getPorcentajeOrigen.invoke(origen);

        if (valorRepartirOrigen != null) {

          if (listDestino.size() == 1) {
            LOG.trace("[repartirBigDecimals] solo hay un elemento en la lista destino destino.valor.repartir == {}",
                valorRepartirOrigen);
            setValorDestino.invoke(listDestino.get(0), valorRepartirOrigen);
          }
          else {
            if (valorRepartirOrigen.compareTo(BigDecimal.ZERO) == 0) {
              valorRepartirDestino = BigDecimal.ZERO;
              for (Object o : listDestino) {
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                setValorDestino.invoke(o, valorRepartirDestino);
              }
            }
            else {
              Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, true));
              boolean repartoPositivo = valorRepartirOrigen.compareTo(BigDecimal.ZERO) >= 0;
              for (Object o : listDestino) {

                valorDestinoPorcentaje = (BigDecimal) getPorcentajeDestino.invoke(o);
                porcentaje = valorDestinoPorcentaje.divide(valorOrigenPorcentaje, decimalesPorcentajeReparto,
                    roundingMode);

                valorRepartirDestino = valorRepartirOrigen.multiply(porcentaje)
                    .setScale(decimalesReparto, roundingMode);
                if (repartoPositivo) {
                  if (repartoAcumulado.add(valorRepartirDestino).compareTo(valorRepartirOrigen) > 0) {
                    valorRepartirDestino = valorRepartirOrigen.subtract(repartoAcumulado);
                  }
                }
                else {
                  if (repartoAcumulado.add(valorRepartirDestino).compareTo(valorRepartirOrigen) < 0) {
                    valorRepartirDestino = valorRepartirOrigen.subtract(repartoAcumulado);
                  }
                }
                repartoAcumulado = repartoAcumulado.add(valorRepartirDestino);
                setValorDestino.invoke(o, valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] origen.valor.porcentaje == {}", valorOrigenPorcentaje);
                LOG.trace("[repartirBigDecimals] destino.valor.porcentaje == {}", valorDestinoPorcentaje);
                LOG.trace("[repartirBigDecimals] porcentaje == {}", porcentaje);
                LOG.trace("[repartirBigDecimals] origen.valor.repartir == {}", valorRepartirOrigen);
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] acumulado == {}", repartoAcumulado);
              }
              if (repartoAcumulado.compareTo(valorRepartirOrigen) > 0) {
                if (repartoPositivo)
                  this.quitarExcesoCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto);
                else
                  this.repartirCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, false, decimalesReparto);
              }
              else if (repartoAcumulado.compareTo(valorRepartirOrigen) < 0) {
                if (repartoPositivo)
                  this.repartirCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, false, decimalesReparto);
                else
                  this.quitarExcesoCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto);

              }
            }
          }
        }
        listDestino.clear();
      }
    }
    catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido repartir el campo: '%s'", nombreCampoDestino), e);
    }
    LOG.trace("[repartirBigDecimals] fin campo destino: {}", nombreCampoDestino);
    return repartoAcumulado;
  }

  public BigDecimal repartirBigDecimals(List<?> listaReparto, BigDecimal valorRepartirOrigen,
      BigDecimal valorOrigenPorcentaje, String nombreCampoDestinoPorcentaje, String nombreCampoDestino,
      int decimalesReparto, RoundingMode roundingMode) throws SIBBACBusinessException {
    LOG.trace("[repartirBigDecimals] inicio campo destino: {}", nombreCampoDestino);
    BigDecimal porcentaje;

    BigDecimal valorRepartirDestino;
    BigDecimal valorDestinoPorcentaje;
    BigDecimal repartoAcumulado = BigDecimal.ZERO;

    int decimalesPorcentajeReparto = 16;

    try {

      if (listaReparto != null && !listaReparto.isEmpty()) {
        List<Object> listDestino = new ArrayList<Object>(listaReparto);
        final Method setValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("set%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)), BigDecimal.class);

        final Method getValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)));

        final Method getPorcentajeDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestinoPorcentaje.substring(0, 1).toUpperCase(),
                    nombreCampoDestinoPorcentaje.substring(1)));

        if (setValorDestino == null || getValorDestino == null || getPorcentajeDestino == null) {
          throw new SIBBACBusinessException(
              String
                  .format(
                      "Los nombres de los campos origen, destino y de calculo de porcentaje no son correctos[ destino: %s,  destino.porcentaje:%s, destino.campo:%s ]",
                      listDestino.get(0).getClass(), nombreCampoDestinoPorcentaje, nombreCampoDestino));
        }

        if (valorRepartirOrigen != null) {

          if (listDestino.size() == 1) {
            LOG.trace("[repartirBigDecimals] solo hay un elemento en la lista destino destino.valor.repartir == {}",
                valorRepartirOrigen);
            setValorDestino.invoke(listDestino.get(0), valorRepartirOrigen);
          }
          else {
            if (valorRepartirOrigen.compareTo(BigDecimal.ZERO) == 0) {
              valorRepartirDestino = BigDecimal.ZERO;
              for (Object o : listDestino) {
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                setValorDestino.invoke(o, valorRepartirDestino);
              }
            }
            else {
              Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, true));
              boolean repartoPositivo = valorRepartirOrigen.compareTo(BigDecimal.ZERO) >= 0;
              for (Object o : listDestino) {

                valorDestinoPorcentaje = (BigDecimal) getPorcentajeDestino.invoke(o);
                porcentaje = valorDestinoPorcentaje.divide(valorOrigenPorcentaje, decimalesPorcentajeReparto,
                    roundingMode);

                valorRepartirDestino = valorRepartirOrigen.multiply(porcentaje)
                    .setScale(decimalesReparto, roundingMode);
                if (repartoPositivo) {
                  if (repartoAcumulado.add(valorRepartirDestino).compareTo(valorRepartirOrigen) > 0) {
                    valorRepartirDestino = valorRepartirOrigen.subtract(repartoAcumulado);
                  }
                }
                else {
                  if (repartoAcumulado.add(valorRepartirDestino).compareTo(valorRepartirOrigen) < 0) {
                    valorRepartirDestino = valorRepartirOrigen.subtract(repartoAcumulado);
                  }
                }
                repartoAcumulado = repartoAcumulado.add(valorRepartirDestino);
                setValorDestino.invoke(o, valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] origen.valor.porcentaje == {}", valorOrigenPorcentaje);
                LOG.trace("[repartirBigDecimals] destino.valor.porcentaje == {}", valorDestinoPorcentaje);
                LOG.trace("[repartirBigDecimals] porcentaje == {}", porcentaje);
                LOG.trace("[repartirBigDecimals] origen.valor.repartir == {}", valorRepartirOrigen);
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] acumulado == {}", repartoAcumulado);
              }
              if (repartoAcumulado.compareTo(valorRepartirOrigen) > 0) {
                if (repartoPositivo)
                  this.quitarExcesoCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto);
                else
                  this.repartirCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, false, decimalesReparto);
              }
              else if (repartoAcumulado.compareTo(valorRepartirOrigen) < 0) {
                if (repartoPositivo)
                  this.repartirCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, false, decimalesReparto);
                else
                  this.quitarExcesoCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto);

              }
            }
          }
        }
        listDestino.clear();
      }
    }
    catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido repartir el campo: '%s'", nombreCampoDestino), e);
    }
    LOG.trace("[repartirBigDecimals] fin campo destino: {}", nombreCampoDestino);
    return repartoAcumulado;
  }

  public BigDecimal acumulaBigDecimals(List<?> listaReparto, BigDecimal valorRepartirOrigen,
      BigDecimal valorOrigenPorcentaje, String nombreCampoDestinoPorcentaje, String nombreCampoDestino,
      int decimalesReparto, RoundingMode roundingMode) throws SIBBACBusinessException {
    LOG.trace("[repartirBigDecimals] inicio campo destino: {}", nombreCampoDestino);
    BigDecimal porcentaje;

    BigDecimal valorDestinoInicial;
    BigDecimal valorRepartirDestino;
    BigDecimal valorDestinoPorcentaje;
    BigDecimal repartoAcumulado = BigDecimal.ZERO;

    int decimalesPorcentajeReparto = 16;

    try {

      if (listaReparto != null && !listaReparto.isEmpty()) {
        List<Object> listDestino = new ArrayList<Object>(listaReparto);
        final Method setValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("set%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)), BigDecimal.class);

        final Method getValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)));

        final Method getPorcentajeDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestinoPorcentaje.substring(0, 1).toUpperCase(),
                    nombreCampoDestinoPorcentaje.substring(1)));

        if (setValorDestino == null || getValorDestino == null || getPorcentajeDestino == null) {
          throw new SIBBACBusinessException(
              String
                  .format(
                      "Los nombres de los campos origen, destino y de calculo de porcentaje no son correctos[ destino: %s,  destino.porcentaje:%s, destino.campo:%s ]",
                      listDestino.get(0).getClass(), nombreCampoDestinoPorcentaje, nombreCampoDestino));
        }

        if (valorRepartirOrigen != null) {

          if (listDestino.size() == 1) {
            LOG.trace("[repartirBigDecimals] solo hay un elemento en la lista destino destino.valor.repartir == {}",
                valorRepartirOrigen);
            setValorDestino.invoke(listDestino.get(0), valorRepartirOrigen);
          }
          else {
            if (valorRepartirOrigen.compareTo(BigDecimal.ZERO) == 0) {
              valorRepartirDestino = BigDecimal.ZERO;
              for (Object o : listDestino) {
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                setValorDestino.invoke(o, valorRepartirDestino);
              }
            }
            else {
              Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, true));
              boolean repartoPositivo = valorRepartirOrigen.compareTo(BigDecimal.ZERO) >= 0;
              for (Object o : listDestino) {

                valorDestinoPorcentaje = (BigDecimal) getPorcentajeDestino.invoke(o);
                porcentaje = valorDestinoPorcentaje.divide(valorOrigenPorcentaje, decimalesPorcentajeReparto,
                    roundingMode);

                valorRepartirDestino = valorRepartirOrigen.multiply(porcentaje)
                    .setScale(decimalesReparto, roundingMode);
                if (repartoPositivo) {
                  if (repartoAcumulado.add(valorRepartirDestino).compareTo(valorRepartirOrigen) > 0) {
                    valorRepartirDestino = valorRepartirOrigen.subtract(repartoAcumulado);
                  }
                }
                else {
                  if (repartoAcumulado.add(valorRepartirDestino).compareTo(valorRepartirOrigen) < 0) {
                    valorRepartirDestino = valorRepartirOrigen.subtract(repartoAcumulado);
                  }
                }
                valorDestinoInicial = (BigDecimal) getValorDestino.invoke(o);
                repartoAcumulado = repartoAcumulado.add(valorRepartirDestino);
                setValorDestino.invoke(o, valorRepartirDestino.add(valorDestinoInicial));
                LOG.trace("[repartirBigDecimals] origen.valor.porcentaje == {}", valorOrigenPorcentaje);
                LOG.trace("[repartirBigDecimals] destino.valor.porcentaje == {}", valorDestinoPorcentaje);
                LOG.trace("[repartirBigDecimals] porcentaje == {}", porcentaje);
                LOG.trace("[repartirBigDecimals] origen.valor.repartir == {}", valorRepartirOrigen);
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] acumulado == {}", repartoAcumulado);
              }
              if (repartoAcumulado.compareTo(valorRepartirOrigen) > 0) {
                if (repartoPositivo)
                  this.quitarExcesoCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto);
                else
                  this.repartirCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, false, decimalesReparto);
              }
              else if (repartoAcumulado.compareTo(valorRepartirOrigen) < 0) {
                if (repartoPositivo)
                  this.repartirCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, false, decimalesReparto);
                else
                  this.quitarExcesoCentimos(valorRepartirOrigen, listDestino, repartoAcumulado,
                      nombreCampoDestinoPorcentaje, nombreCampoDestino, decimalesReparto);

              }
            }
          }
        }
        listDestino.clear();
      }
    }
    catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido repartir el campo: '%s'", nombreCampoDestino), e);
    }
    LOG.trace("[repartirBigDecimals] fin campo destino: {}", nombreCampoDestino);
    return repartoAcumulado;
  }

  public BigDecimal quitarExcesoCentimos(BigDecimal valorRepartirOrigen, List<?> listDestino,
      BigDecimal repartoAcumulado, String nombreCampoDestinoPorcentaje, String nombreCampoDestino, int decimalesReparto)
      throws SIBBACBusinessException {
    LOG.trace("[repartirBigDecimals] inicio campo destino: {}", nombreCampoDestino);
    int i = 0;
    BigDecimal diferenciaReparto;
    int maxIteracionesDiferenciaReparto;
    BigDecimal valorAjustarReparto;
    BigDecimal valorRepartirDestino;
    BigDecimal acumulado = BigDecimal.ZERO;

    try {

      if (listDestino != null && !listDestino.isEmpty()) {

        final Method setValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("set%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)), BigDecimal.class);

        final Method getValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)));

        final Method getPorcentajeDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestinoPorcentaje.substring(0, 1).toUpperCase(),
                    nombreCampoDestinoPorcentaje.substring(1)));

        if (setValorDestino == null || getValorDestino == null || getPorcentajeDestino == null) {
          throw new SIBBACBusinessException(
              String
                  .format(
                      "Los nombres de los campos origen, destino y de calculo de porcentaje no son correctos[destino: %s,  destino.porcentaje:%s, destino.campo:%s ]",
                      listDestino.get(0).getClass(), nombreCampoDestinoPorcentaje, nombreCampoDestino));
        }

        if (valorRepartirOrigen != null) {

          if (repartoAcumulado == null) {
            for (Object o : listDestino) {
              valorRepartirDestino = (BigDecimal) getValorDestino.invoke(o);
              acumulado = acumulado.add(valorRepartirDestino);
              LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
              LOG.trace("[repartirBigDecimals] acumulado == {}", acumulado);
            }
          }
          else {
            acumulado = repartoAcumulado;
          }
          if (acumulado.compareTo(valorRepartirOrigen) != 0) {

            valorAjustarReparto = BigDecimal.ONE.movePointLeft(decimalesReparto);
            diferenciaReparto = acumulado.subtract(valorRepartirOrigen).abs();
            LOG.debug("[repartirBigDecimals] hay que ajustar el reparto, diferencia == {}, ajuste == {}",
                diferenciaReparto, valorAjustarReparto);
            maxIteracionesDiferenciaReparto = diferenciaReparto.divide(valorAjustarReparto, 0, RoundingMode.HALF_UP)
                .intValue();

            Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, false));

            while (acumulado.compareTo(valorRepartirOrigen) != 0) {
              i++;
              for (Object o : listDestino) {

                valorRepartirDestino = (BigDecimal) getValorDestino.invoke(o);
                if (acumulado.compareTo(BigDecimal.ZERO) > 0) {
                  if (valorRepartirDestino.compareTo(valorAjustarReparto) >= 0) {
                    valorRepartirDestino = valorRepartirDestino.subtract(valorAjustarReparto);
                    setValorDestino.invoke(o, valorRepartirDestino);
                    acumulado = acumulado.subtract(valorAjustarReparto);
                  }
                }
                else if (acumulado.compareTo(BigDecimal.ZERO) < 0) {
                  if (valorRepartirDestino.compareTo(valorAjustarReparto) <= 0) {
                    valorRepartirDestino = valorRepartirDestino.add(valorAjustarReparto);
                    setValorDestino.invoke(o, valorRepartirDestino);
                    acumulado = acumulado.add(valorAjustarReparto);
                  }
                }
                diferenciaReparto = acumulado.subtract(valorRepartirOrigen).abs();
                LOG.debug("[repartirBigDecimals] despues del ajuste, diferencia == {}", diferenciaReparto);
                if (acumulado.compareTo(valorRepartirOrigen) == 0) {
                  break;
                }
              }

              if (i > maxIteracionesDiferenciaReparto) {
                LOG.warn("[repartirBigDecimals] salida bucle reparto por maximas iteraciones ajuste reparto...");
                break;
              }

            }

            Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, true));
          }
        }
      }
    }
    catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido repartir el campo destino: '%s'",
          nombreCampoDestino), e);
    }
    LOG.trace("[repartirBigDecimals] fin campo  destino: {}", nombreCampoDestino, nombreCampoDestino);
    return acumulado;
  }

  public BigDecimal repartirCentimos(BigDecimal valorRepartirOrigen, List<?> listDestino, BigDecimal repartoAcumulado,
      String nombreCampoDestinoPorcentaje, String nombreCampoDestino, boolean limpiarRepartoDestino,
      int decimalesReparto) throws SIBBACBusinessException {
    LOG.trace("[repartirBigDecimals] inicio campo destino: {}", nombreCampoDestino);
    BigDecimal valorAjustarReparto;
    BigDecimal valorRepartirDestino;

    try {

      if (listDestino != null && !listDestino.isEmpty()) {

        final Method setValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("set%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)), BigDecimal.class);

        final Method getValorDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestino.substring(0, 1).toUpperCase(),
                    nombreCampoDestino.substring(1)));

        final Method getPorcentajeDestino = listDestino
            .get(0)
            .getClass()
            .getMethod(
                String.format("get%s%s", nombreCampoDestinoPorcentaje.substring(0, 1).toUpperCase(),
                    nombreCampoDestinoPorcentaje.substring(1)));

        if (setValorDestino == null || getValorDestino == null || getPorcentajeDestino == null) {
          throw new SIBBACBusinessException(
              String
                  .format(
                      "Los nombres de los campos origen, destino y de calculo de porcentaje no son correctos[destino: %s,  destino.porcentaje:%s, destino.campo:%s ]",
                      listDestino.get(0).getClass(), nombreCampoDestinoPorcentaje, nombreCampoDestino));
        }

        if (valorRepartirOrigen != null) {

          boolean repartoPositivo = valorRepartirOrigen.compareTo(BigDecimal.ZERO) >= 0;

          valorAjustarReparto = BigDecimal.ONE.movePointLeft(decimalesReparto).multiply(
              (repartoPositivo ? BigDecimal.ONE : BigDecimal.ONE.negate()));
          if (valorAjustarReparto.compareTo(BigDecimal.ZERO) != 0) {
            if (limpiarRepartoDestino) {
              repartoAcumulado = BigDecimal.ZERO;
              for (Object o : listDestino) {
                setValorDestino.invoke(o, BigDecimal.ZERO);
              }
            }
            else if (repartoAcumulado == null) {
              repartoAcumulado = BigDecimal.ZERO;
              for (Object o : listDestino) {
                valorRepartirDestino = (BigDecimal) getValorDestino.invoke(o);
                repartoAcumulado = repartoAcumulado.add(valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] acumulado == {}", repartoAcumulado);
              }
            }

            Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, false));

            while (repartoAcumulado.compareTo(valorRepartirOrigen) != 0) {
              for (Object o : listDestino) {

                valorRepartirDestino = (BigDecimal) getValorDestino.invoke(o);
                valorRepartirDestino = valorRepartirDestino.add(valorAjustarReparto);

                repartoAcumulado = repartoAcumulado.add(valorAjustarReparto);
                setValorDestino.invoke(o, valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] origen.valor.repartir == {}", valorRepartirOrigen);
                LOG.trace("[repartirBigDecimals] destino.valor.repartir == {}", valorRepartirDestino);
                LOG.trace("[repartirBigDecimals] acumulado == {}", repartoAcumulado);

                if (repartoAcumulado.compareTo(valorRepartirOrigen) == 0) {
                  break;
                }
              }
            }// fin while

            Collections.sort(listDestino, new RepartoDatosEconomicosBigDecimalComparator(getPorcentajeDestino, true));
          }
        }
      }
    }
    catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new SIBBACBusinessException(String.format("No se ha podido repartir el campo: '%s'", nombreCampoDestino), e);
    }
    LOG.trace("[repartirBigDecimals] fin campo origen: {} destino: {}", nombreCampoDestino, nombreCampoDestino);
    return repartoAcumulado;
  }
}
