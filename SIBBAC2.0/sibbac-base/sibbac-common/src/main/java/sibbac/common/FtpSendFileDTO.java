package sibbac.common;

import java.io.Serializable;
import java.nio.file.Path;

public class FtpSendFileDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 3563155048248565236L;
  private Path file;
  private String nombreDestino;
  private Path rutaTratados;

  public FtpSendFileDTO() {

  }

  public FtpSendFileDTO(Path file, String nombreDestino, Path rutaTratados) {
    super();
    this.file = file;
    this.nombreDestino = nombreDestino;
    this.rutaTratados = rutaTratados;
  }

  public Path getFile() {
    return file;
  }

  public String getNombreDestino() {
    return nombreDestino;
  }

  public Path getRutaTratados() {
    return rutaTratados;
  }

  public void setFile(Path file) {
    this.file = file;
  }

  public void setNombreDestino(String nombreDestino) {
    this.nombreDestino = nombreDestino;
  }

  public void setRutaTratados(Path rutaTratados) {
    this.rutaTratados = rutaTratados;
  }

  @Override
  public String toString() {
    return String.format("FtpSendFileDTO [file=%s, nombreDestino=%s, rutaTratados=%s]", file, nombreDestino,
        rutaTratados);
  }

}
