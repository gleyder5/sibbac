package sibbac.common.export.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import sibbac.common.export.ExportBuilder;

public class PDFBuilderDesglosesHuerfanos extends ExportBuilder {
	
	private static final Logger   LOG = LoggerFactory.getLogger(PDFBuilderDesglosesHuerfanos.class);
	private static final int      INITIAL_CAPACITY = 4096;
	private static final String	  RESULT_DESGLOSES_LIST_EXPORT		= "desglosesListExport";
	private static final Integer  SIZE_HEADERS = 13;
	private ByteArrayOutputStream baos;
	
	private Font normalFont;
	private Font headerFont;
	private Font redHeaderFont;
	private Font greenHeaderFont;
	
	@Autowired
	protected ApplicationContext context;

	public PDFBuilderDesglosesHuerfanos() {
		this.baos = new ByteArrayOutputStream(INITIAL_CAPACITY);
		this.normalFont = new Font(FontFamily.HELVETICA, 8);
		this.headerFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE);
		this.redHeaderFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.RED);
		this.greenHeaderFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.GREEN);
	}
	
	@Override
	public Object generate(String name, Object target,
			Map<String, ? extends Object> model) throws IOException {
		Document document = (Document) target;
		document.setPageSize(PageSize.LETTER.rotate());
		//document = new Document(PageSize.LETTER.rotate());
		PdfWriter writer = null;
		try {
			writer = this.newWriter(document, this.baos);
			prepareWriter(model, writer);

			// Open the PDF document.
			document.open();

			// Build PDF document.
			if (model != null) {
				LOG.trace("Nombre del pdf [name=={}]", name);

				// Partimos del LinesSet "root".
				// Primero localizamos el "root" para pintarlo en la primera
				// hoja.
				
				List<HashMap<String,Object>> listaDesgloses = (List<HashMap<String, Object>>) model.get(RESULT_DESGLOSES_LIST_EXPORT);
				this.pintaDatos(name, document,  listaDesgloses);
			} else {
				document.add(new Paragraph("No data!"));
			}

			// Close the PDF document.			
			document.close();
			writer.flush();
			writer.close();
		} catch (DocumentException e) {
			LOG.error("> ERROR({}): Errors generating the PDF: {}", e.getClass().getName(), e.getMessage());
		}
		return this.getBaos(null);
	}//public Object generate(String name, Object target,
	 //Map<String, ? extends Object> model) throws IOException {
	
	private void pintaDatos(final String name, Document document, final List<HashMap<String,Object>> listaDesgloses)
			throws DocumentException {
		
		LOG.trace("> Nombre PDF [name=={}]", name);
		
		PdfPTable table = null;

		table = new PdfPTable(SIZE_HEADERS);
		PdfPCell cell = null;
		

		
		//Cabecera
		cell = new PdfPCell(new Phrase("N. Orden", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Cod. SV", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("F. ejecución", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Tipo operación", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Titulos", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Nominal", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Isin", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("descripcion", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Bolsa negociación", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Tipo saldo", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("E. Liquidadora", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		table.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Tipo cambio", this.headerFont));
		cell.setBackgroundColor(BaseColor.BLACK);
		cell.setColspan(2);
		table.addCell(cell);

		// Datos
		
		for (HashMap<String, Object> desglose : listaDesgloses) {
			
			cell = new PdfPCell(new Phrase(desglose.get("numOrden").toString(), this.normalFont));
			table.addCell(cell);// 0
			
			cell = new PdfPCell(new Phrase(desglose.get("codsv").toString(), this.normalFont));
			table.addCell(cell);// 1
			
			cell = new PdfPCell(new Phrase(desglose.get("fejecucion").toString(), this.normalFont));
			table.addCell(cell);// 2
			
			Font font = this.redHeaderFont;
			if ((boolean) desglose.get("bOkTipoOperacion"))
			{
				font = this.greenHeaderFont;
			}//if ((boolean) desglose.get("bOkTipoOperacion"))
			cell = new PdfPCell(new Phrase(desglose.get("tipoOperacion").toString(), font));
			table.addCell(cell);// 3
			
			cell = new PdfPCell(new Phrase(desglose.get("titulos").toString(), this.normalFont));
			table.addCell(cell);// 4
			
			cell = new PdfPCell(new Phrase(desglose.get("nominal").toString(), this.normalFont));
			table.addCell(cell);// 5
			
			font = this.redHeaderFont;
			if ((boolean) desglose.get("bOkIsin"))
			{
				font = this.greenHeaderFont;
			}//if ((boolean) desglose.get("bOkIsin"))
			cell = new PdfPCell(new Phrase(desglose.get("isin").toString(), font));
			table.addCell(cell);// 6
			
			cell = new PdfPCell(new Phrase(desglose.get("desIsin").toString(), this.normalFont));
			table.addCell(cell);// 7
			
			font = this.redHeaderFont;
			if ((boolean) desglose.get("bOkBolsa"))
			{
				font = this.greenHeaderFont;
			}//if ((boolean) desglose.get("bOkIsin"))
			cell = new PdfPCell(new Phrase(desglose.get("bolsa").toString(), font));
			table.addCell(cell);// 8
			
			font = this.redHeaderFont;
			if ((boolean) desglose.get("bOkTipoSaldo"))
			{
				font = this.greenHeaderFont;
			}//if ((boolean) desglose.get("bOkIsin"))
			cell = new PdfPCell(new Phrase(desglose.get("tipoSaldo").toString(), font));
			table.addCell(cell);// 9
			
			cell = new PdfPCell(new Phrase(desglose.get("entidad").toString(), this.normalFont));
			table.addCell(cell);//10
			
			font = this.redHeaderFont;
			if ((boolean) desglose.get("bOkTipoCambio"))
			{
				font = this.greenHeaderFont;
			}//if ((boolean) desglose.get("bOkTipoCambio"))
			cell = new PdfPCell(new Phrase(desglose.get("tipoCambio").toString(), font));
			cell.setColspan(2);
			table.addCell(cell);//11-12
			
			List<HashMap< String, Object >> listaHashMapDesgloses = (List<HashMap< String, Object >>)desglose.get("listaDesgloses");
			if (listaHashMapDesgloses != null)
			{
				Iterator<HashMap<String, Object>> iterator = listaHashMapDesgloses.iterator();
				
				//Cabecera subtabla
				cell = new PdfPCell(new Phrase("Fecha", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("N. referencia", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Sentido", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Bolsa negociación", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Isin", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Descripción", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Titulos", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Precio", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Tipo Saldo", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("CCV", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Titular", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Entidad liquidadora", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				cell = new PdfPCell(new Phrase("Tipo cambio", this.headerFont));
				cell.setBackgroundColor(BaseColor.BLACK);
				table.addCell(cell);
				
				//Datos subtabla
			
				while (iterator.hasNext())
				{
					HashMap<String, Object> desgloseHashMap = iterator.next();
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("fejecucion").toString(), this.normalFont));
					table.addCell(cell);// 0
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("numReferencia").toString(), this.normalFont));
					table.addCell(cell);// 1
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("tipoOperacion").toString(), this.normalFont));
					table.addCell(cell);// 2
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("bolsa").toString(), this.normalFont));
					table.addCell(cell);// 3
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("isin").toString(), this.normalFont));
					table.addCell(cell);// 4
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("desIsin").toString(), this.normalFont));
					table.addCell(cell);// 5
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("titulos").toString(), this.normalFont));
					table.addCell(cell);// 6
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("precio").toString(), this.normalFont));
					table.addCell(cell);// 7
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("tipoSaldo").toString(), this.normalFont));
					table.addCell(cell);// 8
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("ccv").toString(), this.normalFont));
					table.addCell(cell);// 9
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("titular").toString(), this.normalFont));
					table.addCell(cell);//10
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("entidad").toString(), this.normalFont));
					table.addCell(cell);//11
					
					cell = new PdfPCell(new Phrase(desgloseHashMap.get("tipoCambio").toString(), this.normalFont));
					table.addCell(cell);//12
				}
			}
			
		}//for (HashMap<String, Object> desglose : listaDesgloses) {

		document.add(table);
	}//private void pintaDatos(final String name, Document document, final List<HashMap<String,Object>> listaDesgloses)
	
	public void prepareWriter(Map<String, ? extends Object> model, PdfWriter writer) throws DocumentException {
		writer.setViewerPreferences(this.getViewerPreferences(model));
	}
	
	public int getViewerPreferences(Map<String, ? extends Object> model) {
		return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
	}
	
	private PdfWriter newWriter(Document document, ByteArrayOutputStream baos) throws DocumentException {
		return PdfWriter.getInstance(document, baos);
	}
	
	@Override
	public Object generate(String name, Object target,
			Map<String, ? extends Object> model, List<String> dataSets)
			throws IOException {
		return this.generate(name, target, model);
	}
	
	@Override
	public ByteArrayOutputStream getBaos(final Object target) {
		return this.baos;
	}

}//public class PDFBuilderDesglosesHuerfanos extends ExportBuilder {
