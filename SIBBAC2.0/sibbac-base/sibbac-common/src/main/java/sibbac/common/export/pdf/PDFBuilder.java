package sibbac.common.export.pdf;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import sibbac.common.PDFFactura;
import sibbac.common.export.ExportBuilder;
import sibbac.common.export.ExportLines;
import sibbac.common.export.LinesSet;
import sibbac.common.utils.FormatDataUtils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Component
public class PDFBuilder extends ExportBuilder {

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  /** Referencia para escribir en el log. */
  private static final Logger LOG = LoggerFactory.getLogger(PDFBuilder.class);

  /** Capacidad inicial de buffer donde ir creando el documento pdf. */
  private static final int INITIAL_CAPACITY = 4096;

  private static final String RESULT_TITULARES_LIST_EXPORT = "listTitulares";
  private static final Integer SIZE_HEADERS = 25;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS

  @Autowired
  private ApplicationContext context;

  @Value("#{sibbacProps['pdf.copyright']}")
  private String copyright;

  @Value("#{sibbacProps['pdf.factura.author']}")
  private String facturaAuthor;

  @Value("#{sibbacProps['pdf.factura.creator']}")
  private String facturaCreator;

  @Value("#{sibbacProps['pdf.factura.title']}")
  private String facturaTitle;

  @Value("#{sibbacProps['pdf.factura.header']}")
  private String facturaHeader;

  @Value("#{sibbacProps['pdf.factura.subject']}")
  private String facturaSubject;

  @Value("#{sibbacProps['pdf.factura.keywords']}")
  private String facturaKeywords;

  @Value("${mail.source.folder}")
  private String templatesFolder;

  private ByteArrayOutputStream baos;
  private Font normalFont;
  private Font headerFont;
  private Font redHeaderFont;
  private Font blueHeaderFont;

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR

  /**
   * Constructor I. Inicializa los atributos de la clase.
   */
  public PDFBuilder() {
    baos = new ByteArrayOutputStream(INITIAL_CAPACITY);
    normalFont = new Font(FontFamily.HELVETICA, 8);
    headerFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.WHITE);
    redHeaderFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.RED);
    blueHeaderFont = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE);
  } // PDFBuilder

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ MÃ‰TODOS IMPLEMENTADOS

  /*
   * @see sibbac.common.export.ExportBuilder#generate(java.lang.String, java.lang.Object, java.util.Map)
   */
  @Override
  public Object generate(final String name, Object target, final Map<String, ? extends Object> model) throws IOException {
    return this.generate(name, target, model, null);
  } // generate

  /*
   * @see sibbac.common.export.ExportBuilder#generate(java.lang.String, java.lang.Object, java.util.Map, java.util.List)
   */
  @Override
  public Object generate(final String name, Object target, final Map<String, ? extends Object> model, final List<String> dataSets) throws IOException {

    // Apply preferences and build metadata.
    Document document = (Document) target;

    try {
      PdfWriter writer = PdfWriter.getInstance(document, this.baos);
      prepareWriter(model, writer);

      // Open the PDF document.
      document.open();

      // Build PDF document.
      if (model != null) {
        this.buildPdfDocument(name, dataSets, model, document);
      } else {
        document.add(new Paragraph("No data!"));
      }

      // Close the PDF document.
      document.close();
    } catch (DocumentException e) {
      LOG.error("> ERROR({}): Errors generating the PDF: {}", e.getClass().getName(), e.getMessage());
    }
    return document;
  } // generate

  /*
   * @see sibbac.common.export.ExportBuilder#getBaos(java.lang.Object)
   */
  @Override
  public ByteArrayOutputStream getBaos(final Object target) {
    return this.baos;
  } // getBaos

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ FACTURA

  /**
   * Metodo que permite acceder a la funcionalidad de generar una factura para cliente.
   * 
   * @param factura Un objeto {@link PDFFactura} con la informacion a imprimir.
   * @return Una instancia de {@link ByteArrayOutputStream}.
   */
  public ByteArrayOutputStream generaFactura(final PDFFactura factura, Map<String, String> literales) {
    ByteArrayOutputStream os = null;
    Document document = null;
    PdfWriter writer = null;

    // Apply preferences and build metadata.
    try {
      os = new ByteArrayOutputStream(INITIAL_CAPACITY);
      document = newItextpdfDocument(Boolean.TRUE);
      writer = PdfWriter.getInstance(document, os);
      prepareWriterFactura(null, writer);
      buildPdfMetadata(null, document, factura.getNombreAlias());

      // Build PDF document.
      document.open();
      buildPdfFactura(factura, document, writer, literales);
    } catch (DocumentException e) {
    	LOG.error("> ERROR({}): Errors generating the PDF: {}", e.getClass().getName(), e.getMessage());
    } finally {
      if (document != null) {
        document.close();
      }
    }
    return os;
  } // generaFactura

  /**
   * Crea un nuevo documento <code>itextpdf</code> de tamaÃ±o <code>A4</code>.
   * 
   * @param landscape Indica si el documento es apaisado o no.
   * @return Documento creado.
   */
  private Document newItextpdfDocument(final Boolean landscape) {
    if (landscape) {
      return new Document(PageSize.A4.rotate());
    } else {
      return new Document(PageSize.A4);
    } // else
  } // newItextpdfDocument

  /**
   * Prepara el objeto que escribe en el documento PDF.
   * 
   * @param model ParÃ¡metros de configuraciÃ³n.
   * @param writer Objeto.
   * @throws DocumentException Si ocurre algÃºn error al preparar el objeto.
   */
  private void prepareWriterFactura(Map<String, ? extends Object> model, PdfWriter writer) throws DocumentException {
    writer.setViewerPreferences(this.getViewerPreferences(model));

    // Un "controller" para los headers y los footers.
    HeaderFooterFactura event = new HeaderFooterFactura(this.facturaHeader);
    writer.setPageEvent(event);
  } // prepareWriterFactura

  /**
   * Rellena los metadatos del documento. (author, title, etc.).
   *
   * @param model the model Map
   * @param document the PDF Document to complete
   * @param name TÃ­tulo del documento.
   */
  public void buildPdfMetadata(Map<String, ? extends Object> model, Document document, final String name) {
    /*
     * HeaderFooter footer = new HeaderFooter( new Phrase( copyright ), false ); footer.setAlignment( Element.ALIGN_CENTER ); footer.setBorder(
     * Rectangle.TOP ); document.setFooter( footer );
     */

    // The size
    document.setPageSize(PageSize.A4);

    // The metadata
    document.addAuthor(this.facturaAuthor);
    document.addCreationDate();
    document.addCreator(this.facturaCreator);
    document.addTitle(this.facturaTitle + name);
    document.addSubject(this.facturaSubject);
    document.addKeywords(this.facturaKeywords);
  } // buildPdfMetadata

  /**
   * Genera el documento PDF de la factura.
   * 
   * @param factura Datos de la factura.
   * @param document Document.
   * @param writer Referencia para escribir en el documento pdf.
   * @param literales
   * @throws DocumentException
   */
  private void buildPdfFactura(PDFFactura factura, Document document, PdfWriter writer, Map<String, String> literales) throws DocumentException {

    String prefix = "[PDFBuilder::buildPdfFactura] ";

    // String pathPrefix = "sibbac/common/export/pdf/";
    String templateName = templatesFolder + File.separator + "plantilla.jpg";
    String templateNameDuplicate = templatesFolder + File.separator + "plantillaDuplicado.jpg";
    String templateNameRectificativa = templatesFolder + File.separator + "plantillaRectificativa.jpg";
    String logoName = templatesFolder + File.separator + "LogotipoGlobalBanking.png";
    String dowJonesName = templatesFolder + File.separator + "dowJones.png";
    String ftse4Name = templatesFolder + File.separator + "Ftse4Good.png";

    Image plantilla = null;
    Image logotipo = null;
    Image dowJones = null;
    Image ftse4Good = null;

    PdfContentByte under = null;
    LOG.trace(prefix + "Localizando las imagenes de la plantilla en la carpeta [{}]...", templatesFolder);

    try {
      under = writer.getDirectContentUnder();

      // La imagen de fondo
      Resource resourceNorm = context.getResource("file:" + templateName);
      Resource resourceDupl = context.getResource("file:" + templateNameDuplicate);
      Resource resourceRect = context.getResource("file:" + templateNameRectificativa);

      if (factura.getDuplicado()) {
        LOG.trace(prefix + "+ Cargando la plantilla de: [DUPLICADO]");
        plantilla = Image.getInstance(context.getResource("file:" + templateNameDuplicate).getURL());
      } else if (factura.isRectificativa()) {
        LOG.trace(prefix + "+ Cargando la plantilla de: [RECTIFICATIVA]");
        plantilla = Image.getInstance(context.getResource("file:" + templateNameRectificativa).getURL());
      } else {
        LOG.trace(prefix + "+ Cargando la plantilla de: [NORMAL]");
        plantilla = Image.getInstance(context.getResource("file:" + templateName).getURL());
      }
      LOG.trace(prefix + "+ Duplicado: [{}]", factura.getDuplicado());
      LOG.trace(prefix + "+ Rectificativa: [{}]", factura.isRectificativa());

      LOG.trace(prefix + "+ templateName: [{}]", "file:" + templateName);
      LOG.trace(prefix + "+ templateNameDuplicate: [{}]", "file:" + templateNameDuplicate);
      LOG.trace(prefix + "+ templateNameRectificativa: [{}]", "file:" + templateNameDuplicate);

      LOG.trace(prefix + "+ Resource Norm: [{}]", resourceNorm);
      LOG.trace(prefix + "+ Resource Dupl: [{}]", resourceDupl);
      LOG.trace(prefix + "+ Resource Rect: [{}]", resourceRect);

      LOG.trace(prefix + "+ URL Resource Norm: [{}]", (resourceNorm != null) ? resourceNorm.getURL() : "<null>");
      LOG.trace(prefix + "+ URL Resource Dupl: [{}]", (resourceDupl != null) ? resourceDupl.getURL() : "<null>");
      LOG.trace(prefix + "+ URL Resource Rect: [{}]", (resourceRect != null) ? resourceRect.getURL() : "<null>");

      LOG.trace(prefix + "+ Plantilla: [{}]", plantilla);

      // Logotipo de Santander
      LOG.trace(prefix + "Localizando la imagen (recurso) del logo Santander...");
      logotipo = Image.getInstance(context.getResource("file:" + logoName).getURL());
      LOG.trace(prefix + "+ Recurso: [{}]", "file:" + logoName);
      Resource resourceLogo = context.getResource("file:" + logoName);
      LOG.trace(prefix + "+ Resource: [{}]", resourceLogo);
      LOG.trace(prefix + "+ URL Resource: [{}]", (resourceLogo != null) ? resourceLogo.getURL() : "<null>");
      LOG.trace(prefix + "+ Logotipo: [{}]", logotipo);

      // Logotipo Dow Jones
      dowJones = Image.getInstance(context.getResource("file:" + dowJonesName).getURL());

      // Logotipo de FTSE
      ftse4Good = Image.getInstance(context.getResource("file:" + ftse4Name).getURL());

      // Ajustamos la plantilla como imagen de fondo
      plantilla.scaleAbsolute(525, 775);
      plantilla.setAbsolutePosition((PageSize.A4.getWidth() - plantilla.getScaledWidth()) / 2,
                                    (PageSize.A4.getHeight() - plantilla.getScaledHeight()) / 2);
      under.addImage(plantilla);
      under.fill();

      // Ajustamos el Logotipo de Santander
      logotipo.scaleAbsolute(160, 44);
      logotipo.setAbsolutePosition(61, 764);
      document.add(logotipo);

      // Logotipo DOW JONES
      dowJones.scaleAbsolute(200, 70);
      dowJones.setAbsolutePosition(320, 30);
      document.add(dowJones);

      // Logotipo FTS4GOOD
      ftse4Good.scaleAbsolute(60, 50);
      ftse4Good.setAbsolutePosition(480, 45);
      document.add(ftse4Good);

    } catch (Exception e) {
      LOG.warn(prefix + "Error leyendo la imagen [{} / {}]: [{}]", templateName, logoName, e);
    }

    try {
      BaseFont bf;
      bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

      // Fecha Factura
      under.saveState();
      under.beginText();
      under.moveText(390, 725);
      under.setFontAndSize(bf, 10);
      if (factura.getFhFechaFac() != null) {
        under.showText(FormatDataUtils.convertDateToStringSeparator(factura.getFhFechaFac()));
      } else {
        under.showText("Fecha factura...");
      }
      under.endText();
      under.restoreState();

      // SAU
      under.saveState();
      under.beginText();
      under.moveText(390, 710);
      under.setFontAndSize(bf, 10);
      under.showText("Banco Santander S.A.");
      under.endText();
      under.restoreState();

      // CIF SV
      under.saveState();
      under.beginText();
      under.moveText(390, 700);
      under.setFontAndSize(bf, 10);
      under.showText("CIF: A39000013");
      under.endText();
      under.restoreState();

      // Nombre Alias
      under.saveState();
      under.beginText();
      under.moveText(95, 660);
      under.setFontAndSize(bf, 10);
      if (factura.getNombreAlias() != null) {
        under.showText(factura.getNombreAlias());
      } else {
        under.showText("Nombre del Alias...");
      }
      under.endText();
      under.restoreState();

      String[] dirFields;
      if (factura.getDireccion() != null) {
        dirFields = factura.getDireccion().split(System.getProperty("line.separator"));
        if (dirFields.length != 3) {
          LOG.warn(prefix + "El nÃºmero de campos de la direcciÃ³n del alias ha cambiado. Visualizando solo 3 ...");
          dirFields = new String[] { "Direccion", "Codigo postal", "Pais"
          };
        } // else
      } else {
        dirFields = new String[] { "Direccion", "Codigo postal", "Pais"
        };
      } // else

      // Direccion
      under.saveState();
      under.beginText();
      under.moveText(95, 645);
      under.setFontAndSize(bf, 10);
      under.showText(dirFields[0]);
      under.endText();
      under.restoreState();

      // CÃ³digo postal
      under.saveState();
      under.beginText();
      under.moveText(95, 635);
      under.setFontAndSize(bf, 10);
      under.showText(dirFields[1]);
      under.endText();
      under.restoreState();

      // PaÃ­s
      under.saveState();
      under.beginText();
      under.moveText(95, 625);
      under.setFontAndSize(bf, 10);
      under.showText(dirFields[2]);
      under.endText();
      under.restoreState();

      // Numero Factura LABEL
      under.saveState();
      under.beginText();
      under.moveText(330, 547);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("M"));
      under.endText();
      under.restoreState();

      // Numero Factura
      under.saveState();
      under.beginText();
      under.moveText(450, 547);
      under.setFontAndSize(bf, 10);
      if (String.valueOf(factura.getNbDocNumero()) != null) {
        under.showText(String.valueOf(factura.getNbDocNumero()));
      } else {
        under.showText("NÂº Factura...");
      }
      under.endText();
      under.restoreState();

      // Concepto LABEL
      under.saveState();
      under.beginText();
      under.moveText(235, 495);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("N"));
      under.endText();
      under.restoreState();

      // Concepto
      under.saveState();
      under.beginText();
      under.moveText(95, 480);
      under.setFontAndSize(bf, 10);
      under.showText(literales.get("FACT_CONCEPTO_DETALLE"));
      under.endText();
      under.restoreState();

      // Importe LABEL
      under.saveState();
      under.beginText();
      under.moveText(450, 495);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("O"));
      under.endText();
      under.restoreState();

      // Importe
      under.saveState();
      under.beginText();
      under.moveText(450, 480);
      under.setFontAndSize(bf, 10);
      if (factura.getImporte() != null) {
        String importe = formatDecimal(factura.getImporte());
        LOG.debug("[buildPdfFactura] Estableciendo importe: {}", importe);
        under.showText(importe + " €");
      } else {
        under.showText("Importe...");
      }
      under.endText();
      under.restoreState();

      // Referencia Factura LABEL
      under.saveState();
      under.beginText();
      under.moveText(95, 222);
      under.setFontAndSize(bf, 10);
      under.showText(literales.get("W"));
      under.endText();
      under.restoreState();

      // Importe Base LABEL
      under.saveState();
      under.beginText();
      under.moveText(95, 423);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("P"));
      under.endText();
      under.restoreState();

      // Importe Base
      under.saveState();
      under.beginText();
      under.moveText(95, 408);
      under.setFontAndSize(bf, 10);
      if (factura.getImporteBase() != null) {
        String importeBase = formatDecimal(factura.getImporteBase());
        LOG.debug("[buildPdfFactura] Estableciendo importeBase: {}", importeBase);
        under.showText(importeBase + " €");
      } else {
        under.showText("Importe Base...");
      }
      under.endText();
      under.restoreState();

      // Impuestos LABEL
      under.saveState();
      under.beginText();
      under.moveText(275, 423);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("Q"));
      under.endText();
      under.restoreState();

      // Impuestos
      under.saveState();
      under.beginText();
      under.moveText(275, 408);
      under.setFontAndSize(bf, 10);
      if (factura.getImpuestos() != null) {
        String impuestos = formatDecimal(factura.getImpuestos());
        LOG.debug("[buildPdfFactura] Estableciendo impuestos: {}", impuestos);
        under.showText(impuestos + " €");
      } else {
        under.showText("Impuestos...");
      }
      under.endText();
      under.restoreState();

      // Total LABEL
      under.saveState();
      under.beginText();
      under.moveText(450, 423);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("R"));
      under.endText();
      under.restoreState();

      // Total
      under.saveState();
      under.beginText();
      under.moveText(450, 408);
      under.setFontAndSize(bf, 10);
      if (factura.getTotal() != null) {
        String total = formatDecimal(factura.getTotal());
        under.showText(total + " €");
      } else {
        under.showText("Total...");
      }
      under.endText();
      under.restoreState();

      // Cuenta Bancaria LABEL
      under.saveState();
      under.beginText();
      under.moveText(95, 184);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("S"));
      under.endText();
      under.restoreState();

      // Cuenta Bancaria
      under.saveState();
      under.beginText();
      under.moveText(230, 184);
      under.setFontAndSize(bf, 10);
      if (factura.getCuentaBancaria() != null) {
        under.showText(factura.getCuentaBancaria());
      } else {
        under.showText("Cuenta Bancaria...");
      }
      under.endText();
      under.restoreState();

      // IBAN LABEL
      under.saveState();
      under.beginText();
      under.moveText(95, 171);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("T"));
      under.endText();
      under.restoreState();

      // IBAN
      under.saveState();
      under.beginText();
      under.moveText(230, 171);
      under.setFontAndSize(bf, 10);
      if (factura.getIban() != null) {
        under.showText(factura.getIban());
      } else {
        under.showText("IBAN...");
      }
      under.endText();
      under.restoreState();

      // Codigo BIC LABEL
      under.saveState();
      under.beginText();
      under.moveText(95, 159);
      under.setFontAndSize(bf, 11);
      under.showText(literales.get("U"));
      under.endText();
      under.restoreState();

      // Codigo BIC
      under.saveState();
      under.beginText();
      under.moveText(230, 158);
      under.setFontAndSize(bf, 10);
      if (factura.getBic() != null) {
        under.showText(factura.getBic());
      } else {
        under.showText("Codigo BIC...");
      }
      under.endText();
      under.restoreState();

      // Registro Mercantil
      under.saveState();
      under.beginText();
      under.setFontAndSize(bf, 8);
      if (factura.getRegistroMercantil() != null) {
        under.showTextAligned(0, factura.getRegistroMercantil(), 53, 105, 90);
      } else {
        under.showTextAligned(0, "Registro Mercantil...", 53, 120, 90);
      }
      under.endText();
      under.restoreState();

      // Nombre Empresa
      under.saveState();
      under.beginText();
      under.moveText(95, 65);
      under.setFontAndSize(bf, 10);
      if (factura.getEmpresa() != null) {
        under.showText(factura.getEmpresa());
      } else {
        under.showText("Nombre de la empresa...");
      }
      under.endText();
      under.restoreState();
    } catch (Exception e) {
      LOG.warn("Error seteando datos en el PDF [{} / {}]: [{}]", logoName, e.getCause());
    }
  } // buildPdfFactura

  /**
   * Formatea los nÃºmero decimales a escribir en el PDF.
   * 
   * @param bigDecimal NÃºmero a formatear.
   * @return <code>java.lang.String - </code>NÃºmero formateado.
   */
  private String formatDecimal(BigDecimal num) {
    DecimalFormat df = new DecimalFormat();
    df.setMaximumFractionDigits(2);
    df.setMinimumFractionDigits(2);
    df.setGroupingUsed(Boolean.TRUE);

    return df.format(num.setScale(2, BigDecimal.ROUND_HALF_UP));
  } // formatDecimal

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ TITULARES

  /**
   * 
   * @param name
   * @param target
   * @param model
   * @return
   * @throws IOException
   */
  public Object generateTitulares(final String name, Object target, final Map<String, ? extends Object> model) throws IOException {

    // Apply preferences and build metadata.
    Document document = (Document) target;

    try {

      PdfWriter writer = PdfWriter.getInstance(document, this.baos);
      prepareWriter(model, writer);

      // Open the PDF document.
      document.open();

      // Build PDF document.
      if (model != null) {
        LOG.trace("Nombre del pdf [name=={}]", name);

        // Partimos del LinesSet "root".
        // Primero localizamos el "root" para pintarlo en la primera
        // hoja.

        List<HashMap<String, Object>> listaTitulares = (List<HashMap<String, Object>>) model.get(RESULT_TITULARES_LIST_EXPORT);
        this.pintaDatos(name, document, listaTitulares);
      } else {
        document.add(new Paragraph("No data!"));
      }

      // Close the PDF document.
      document.close();
    } catch (DocumentException e) {
      LOG.error("> ERROR({}): Errors generating the PDF: {}", e.getClass().getName(), e.getMessage());
    }
    return document;
  } // generateTitulares

  /**
   * Prepara el objeto que escribe en el documento PDF.
   * 
   * @param model ParÃ¡metros de configuraciÃ³n.
   * @param writer Objeto.
   * @throws DocumentException Si ocurre algÃºn error al preparar el objeto.
   */
  public void prepareWriter(Map<String, ? extends Object> model, PdfWriter writer) throws DocumentException {
    writer.setViewerPreferences(this.getViewerPreferences(model));

    // Un "controller" para los headers y los footers.
    HeaderFooter event = new HeaderFooter(this.facturaHeader);
    writer.setPageEvent(event);
  } // prepareWriter

  /**
   * 
   * @param name
   * @param document
   * @param listaTitulares
   * @throws DocumentException
   */
  private void pintaDatos(final String name, Document document, final List<HashMap<String, Object>> listaTitulares) throws DocumentException {
    LOG.trace("> Nombre PDF [name=={}]", name);

    PdfPTable table = null;

    table = new PdfPTable(SIZE_HEADERS);
    PdfPCell cell = null;

    // Cabecera
    cell = new PdfPCell(new Phrase("Error", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Booking", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Ref. Cliente", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("CCV", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Sentido", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Isin", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Titulos", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Mercado", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Nombre", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("1erApellido", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("2ÂºApellido", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("T.Documento", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Documento", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("T.Persona", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("P.Residencia", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("T.Nacionalidad", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Domicilio", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Poblacion", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Provincia", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("C.Postal", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("T.Titular", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("%ParticipaciÃ³n", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("F.ContrataciÃ³n", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("Alias", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    cell = new PdfPCell(new Phrase("N.Alias", this.headerFont));
    cell.setBackgroundColor(BaseColor.BLACK);
    table.addCell(cell);

    // Datos

    for (HashMap<String, Object> titular : listaTitulares) {
      Object obj = titular.get("Error");
      List<HashMap<String, String>> listaErrores = null;
      if (obj instanceof String) {
        cell = new PdfPCell(new Phrase(obj.toString(), this.normalFont));
        table.addCell(cell);// 1
      }// if (obj instanceof String)
      else {
        cell = new PdfPCell(new Phrase("ERROR", this.normalFont));
        table.addCell(cell);
        listaErrores = (List<HashMap<String, String>>) obj;
      }// else

      cell = new PdfPCell(new Phrase((String) titular.get("Booking"), this.normalFont));
      table.addCell(cell);// 2
      cell = new PdfPCell(new Phrase((String) titular.get("Ref_Cliente"), this.normalFont));
      table.addCell(cell);// 3
      cell = new PdfPCell(new Phrase((String) titular.get("CCV"), this.normalFont));
      table.addCell(cell);// 4
      cell = new PdfPCell(new Phrase((String) titular.get("Sentido"), this.normalFont));
      table.addCell(cell);// 5
      cell = new PdfPCell(new Phrase((String) titular.get("Isin"), this.normalFont));
      table.addCell(cell);// 6
      cell = new PdfPCell(new Phrase((String) titular.get("Titulos"), this.normalFont));
      table.addCell(cell);// 7
      cell = new PdfPCell(new Phrase((String) titular.get("Mercado"), this.normalFont));
      table.addCell(cell);// 8
      cell = new PdfPCell(new Phrase((String) titular.get("Nombre"), this.normalFont));
      table.addCell(cell);// 9
      cell = new PdfPCell(new Phrase((String) titular.get("PrimerApellido"), this.normalFont));
      table.addCell(cell);// 10
      cell = new PdfPCell(new Phrase((String) titular.get("SegundorApellido"), this.normalFont));
      table.addCell(cell);// 11
      cell = new PdfPCell(new Phrase((String) titular.get("T_Documento"), this.normalFont));
      table.addCell(cell);// 12
      cell = new PdfPCell(new Phrase((String) titular.get("Documento"), this.normalFont));
      table.addCell(cell);// 13
      cell = new PdfPCell(new Phrase((String) titular.get("T_Persona"), this.normalFont));
      table.addCell(cell);// 14
      cell = new PdfPCell(new Phrase((String) titular.get("P_Residencia"), this.normalFont));
      table.addCell(cell);// 15
      cell = new PdfPCell(new Phrase((String) titular.get("Nacionalidad"), this.normalFont));
      table.addCell(cell);// 16
      cell = new PdfPCell(new Phrase((String) titular.get("Domicilio"), this.normalFont));
      table.addCell(cell);// 17
      cell = new PdfPCell(new Phrase((String) titular.get("Poblacion"), this.normalFont));
      table.addCell(cell);// 18
      cell = new PdfPCell(new Phrase((String) titular.get("Provincia"), this.normalFont));
      table.addCell(cell);// 19
      cell = new PdfPCell(new Phrase((String) titular.get("C_Postal"), this.normalFont));
      table.addCell(cell);// 20
      cell = new PdfPCell(new Phrase((String) titular.get("T_Titular"), this.normalFont));
      table.addCell(cell);// 21
      cell = new PdfPCell(new Phrase((String) titular.get("Participacion_Por"), this.normalFont));
      table.addCell(cell);// 22
      cell = new PdfPCell(new Phrase((String) titular.get("F_Contratacion"), this.normalFont));
      table.addCell(cell);// 23
      cell = new PdfPCell(new Phrase((String) titular.get("Alias"), this.normalFont));
      table.addCell(cell);// 24
      cell = new PdfPCell(new Phrase((String) titular.get("N_Alias"), this.normalFont));
      table.addCell(cell);// 25
      if (listaErrores != null) {
        // hay errores se va a generar una fila por cada error.
        // Cabeceras
        cell = new PdfPCell(new Phrase("", this.normalFont));
        table.addCell(cell);// 1
        cell = new PdfPCell(new Phrase("CÃ³digo", this.redHeaderFont));
        table.addCell(cell);// 2
        cell = new PdfPCell(new Phrase("DescripciÃ³n", this.redHeaderFont));
        cell.setColspan(23);
        table.addCell(cell);// 23
        // datos
        for (HashMap<String, String> error : listaErrores) {
          cell = new PdfPCell(new Phrase("", this.normalFont));
          table.addCell(cell);// 1
          cell = new PdfPCell(new Phrase(error.get("codigo"), this.blueHeaderFont));
          table.addCell(cell);// 2
          cell = new PdfPCell(new Phrase(error.get("descripcion"), this.blueHeaderFont));
          cell.setColspan(23);
          table.addCell(cell);// 23
        } // for (HashMap<String, String> error : listaErrores)
      } // if (listaErrores != null)
    } // for (HashMap<String, Object> titular : listaTitulares) {

    document.add(table);
  } // pintaDatos

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ UTILIDADES

  /**
   * Crea un nuevo documento PDF.
   * 
   * @return <code>com.itextpdf.text.Document - </code>Documento creado.
   */
  public Document newDocument() {
    return newItextpdfDocument(true);
  } // newDocument

  /**
   * Establece las preferencias del documento.
   * 
   * @param model Preferencias.
   * @return
   */
  private int getViewerPreferences(Map<String, ? extends Object> model) {
    return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
  } // getViewerPreferences

  /**
   * 
   * @param name
   * @param dataSets
   * @param model
   * @param document
   * @throws DocumentException
   */
  private void buildPdfDocument(final String name, final List<String> dataSets, final Map<String, ? extends Object> model, Document document) throws DocumentException {
    // Explore the model for things...
    List<List<LinesSet>> lineas = ExportLines.getLinesFor(model, dataSets);

    if (lineas == null) {
      LOG.trace("No data!");
      document.add(new Paragraph("No data!"));
    } else {
      // El "name" es el "servicio.comando" que esperamos, normalmente.
      LOG.trace("Nombre del pdf [name=={}]", name);

      // Partimos del LinesSet "root".
      // Primero localizamos el "root" para pintarlo en la primera hoja.
      LinesSet root = null;
      for (List<LinesSet> conjunto : lineas) {
        root = ExportLines.findRootLineSet(conjunto);
        if (root != null) {
          // A pintar en la hoja.
          this.pintaDatos(name, document, root, conjunto);
        }
      }
    }
  } // buildPdfDocument

  /**
   * 
   * @param name
   * @param document
   * @param root
   * @param lineas
   * @throws DocumentException
   */
  private void pintaDatos(final String name, Document document, final LinesSet root, final List<LinesSet> lineas) throws DocumentException {
    List<String> headers = root.getHeaders();
    List<List<String>> rows = root.getLines();
    PdfPTable table = null;

    // Headers?
    if (headers != null) {
      table = new PdfPTable(headers.size());
      PdfPCell cell = null;
      for (String h : headers) {
        cell = new PdfPCell(new Phrase(h, this.headerFont));
        cell.setBackgroundColor(BaseColor.BLACK);
        table.addCell(cell);
      }
    }

    // Data?
    if (rows != null) {
      for (List<String> row : rows) {
        if (table == null) {
          table = new PdfPTable(row.size());
        }
        for (String dato : row) {
          table.addCell(new PdfPCell(new Phrase((dato != null) ? dato.toString() : "", this.normalFont)));
        }
      }
    }

    document.add(table);
  } // pintaDatos

} // PDFBuilder
