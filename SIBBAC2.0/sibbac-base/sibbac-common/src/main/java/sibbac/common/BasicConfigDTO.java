package sibbac.common;

import java.io.Closeable;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class BasicConfigDTO implements Serializable, Closeable {

  /**
   * 
   */
  private static final long serialVersionUID = -7477977785879227993L;

  protected List<Date> holidays;
  protected List<Integer> workDaysOfWeek;
  protected String auditUser;

  public List<Date> getHolidays() {
    return holidays;
  }

  public List<Integer> getWorkDaysOfWeek() {
    return workDaysOfWeek;
  }

  public void setHolidays(List<Date> holidays) {
    this.holidays = holidays;
  }

  public void setWorkDaysOfWeek(List<Integer> workDaysOfWeek) {
    this.workDaysOfWeek = workDaysOfWeek;
  }

  public String getAuditUser() {
    return auditUser;
  }

  public void setAuditUser(String auditUser) {
    this.auditUser = auditUser;
  }

  @Override
  public void close() {
    if (holidays != null) {
      holidays.clear();
    }
    if (workDaysOfWeek != null) {
      workDaysOfWeek.clear();
    }

  }

  @Override
  public String toString() {
    return String.format("BasicConfigDTO [holidays=%s, workDaysOfWeek=%s, auditUser=%s]", holidays, workDaysOfWeek,
        auditUser);
  }

}
