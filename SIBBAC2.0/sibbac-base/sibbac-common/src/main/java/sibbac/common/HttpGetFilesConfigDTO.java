package sibbac.common;

import java.io.Serializable;
import java.util.Date;

public class HttpGetFilesConfigDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -4448384330768477297L;

  private final String url, username, password, filePattern, fileNameEndMark, outPutPath, formatDatePattern;
  private final Date fechaUltimoFicheroDescargar;
  private final int maxGetFiles;
  private final boolean compress;

  public HttpGetFilesConfigDTO(String url, String username, String password, String filePattern,
      String fileNameEndMark, String outputpath, Date fechaUltimoFicheroDescargar, int maxGetFiles,
      String formatDatePattern, boolean compress) {
    super();
    this.url = url;
    this.username = username;
    this.password = password;
    this.filePattern = filePattern;
    this.fileNameEndMark = fileNameEndMark;
    this.fechaUltimoFicheroDescargar = fechaUltimoFicheroDescargar;
    this.outPutPath = outputpath;
    this.maxGetFiles = maxGetFiles;
    this.formatDatePattern = formatDatePattern;
    this.compress = compress;

  }

  protected String getUrl() {
    return url;
  }

  protected String getUsername() {
    return username;
  }

  protected String getPassword() {
    return password;
  }

  protected String getFilePattern() {
    return filePattern;
  }

  protected String getFileNameEndMark() {
    return fileNameEndMark;
  }

  protected Date getFechaUltimoFicheroDescargar() {
    return fechaUltimoFicheroDescargar;
  }

  protected String getOutPutPath() {
    return outPutPath;
  }

  protected int getMaxGetFiles() {
    return maxGetFiles;
  }

  protected String getFormatDatePattern() {
    return formatDatePattern;
  }

  public boolean isCompress() {
    return compress;
  }

}
