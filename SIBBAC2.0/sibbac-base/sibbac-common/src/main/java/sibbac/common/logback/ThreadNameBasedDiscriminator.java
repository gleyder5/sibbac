package sibbac.common.logback;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.sift.Discriminator;

/**
 * To separate logs per thread.
 * 
 * @author Vector ITC Group
 * @version 1.0
 * @since 1.0
 * @see http://java.dzone.com/articles/siftingappender-logging
 */
public class ThreadNameBasedDiscriminator implements Discriminator<ILoggingEvent> {

	private static final String KEY = "threadName";

	private boolean started;

	@Override
	public void start() {
		started = true;
	}

	@Override
	public void stop() {
		started = false;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	public String getDiscriminatingValue(ILoggingEvent e) {
		return Thread.currentThread().getName();
	}

	@Override
	public String getKey() {
		return KEY;
	}

}
