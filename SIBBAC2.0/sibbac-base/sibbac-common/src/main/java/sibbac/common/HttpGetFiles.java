package sibbac.common;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.util.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HttpGetFiles {

  /**
   * A common logger.
   */
  protected static final Logger LOG = LoggerFactory.getLogger(HttpGetFiles.class);

  public List<Path> donwloadFiles(HttpGetFilesConfigDTO config) throws IOException, ParseException {
    LOG.trace("[execute] inicio.");
    Date lastModified;
    String fechaSt = null;
    String url = config.getUrl();

    byte[] buffer = new byte[2048];

    SimpleDateFormat sdf = new SimpleDateFormat(config.getFormatDatePattern(), Locale.ENGLISH);
    List<Path> descargados = new ArrayList<Path>();

    LOG.info("******* url                  {}              ***********", url);

    String urlWithOutParams = url;

    if (url.contains("?")) {
      urlWithOutParams = url.substring(0, url.indexOf("?"));
    }

    String rutaDescarga = config.getOutPutPath();
    LOG.info("******* download file path   {}              ***********", rutaDescarga);

    String username = config.getUsername();
    LOG.info("******* username             {}              ***********", username);
    String password = config.getPassword();
    LOG.info("******* password             {}              ***********",
        StringUtils.rightPad(StringUtils.substring(password, 0, 1), password.length(), '#'));

    String filePattern = config.getFilePattern();
    LOG.info("******* file pattern         {}              ***********", filePattern);

    String fileNameEndMark = config.getFileNameEndMark();
    LOG.info("******* file end mark        {}              ***********", fileNameEndMark);

    String regex = filePattern.replace("?", ".?").replace("*", ".*?");

    String login = String.format("%s:%s", username, password);

    String base64login = new String(Base64.encodeBase64(login.getBytes()));

    String basicAuth = String.format("Basic %s", base64login);

    Path rutaDescargaPath = Paths.get(rutaDescarga);
    if (Files.notExists(rutaDescargaPath)) {
      LOG.info("*******  create download path {}              ***********", rutaDescargaPath.toString());
      Files.createDirectories(rutaDescargaPath);
    }

    LOG.trace("*******  get index file list              ***********");
    Document doc = null;
    if (StringUtils.isBlank(username) && StringUtils.isBlank(password)) {
      doc = Jsoup.connect(url).get();
    }
    else {
      doc = Jsoup.connect(url).header("Authorization", basicAuth).get();
    }

    try (CloseableHttpClient httpclient = HttpClients.custom().build();) {

      for (Element element : doc.select("a")) {

        String fileNameHref = element.attr("href");

        LOG.trace("*******  found a href {}              ***********", fileNameHref);

        if (fileNameHref.matches(regex)) {
          LOG.trace("*******  found a href {} and download it              ***********", fileNameHref);
          HttpGet httpget = new HttpGet(String.format("%s%s", urlWithOutParams, fileNameHref));
          if (StringUtils.isNotBlank(username) || StringUtils.isNotBlank(password)) {
            httpget.setHeader("Authorization", basicAuth);
          }

          httpget.setHeader("Accept-Language", Locale.ENGLISH.toString());
          try (CloseableHttpResponse response = httpclient.execute(httpget);) {
            if (response.getStatusLine().getStatusCode() == 200) {
              lastModified = null;
              if (config.getFormatDatePattern() != null && !config.getFormatDatePattern().trim().isEmpty()) {
                Header[] lastModifiedHeaders = response.getHeaders("Last-Modified");

                if (lastModifiedHeaders != null && lastModifiedHeaders.length > 0) {
                  fechaSt = lastModifiedHeaders[0].getValue();
                  if (fechaSt != null && !fechaSt.trim().isEmpty()) {
                    lastModified = sdf.parse(fechaSt);
                  }

                }
                lastModifiedHeaders = response.getAllHeaders();
              }

              if (config.getMaxGetFiles() > 0 && descargados.size() >= config.getMaxGetFiles()) {
                break;
              }
              else if (lastModified != null && config.getFechaUltimoFicheroDescargar() != null
                  && lastModified.getTime() <= config.getFechaUltimoFicheroDescargar().getTime()) {
                continue;
              }

              String fileNameOutput = fileNameHref;
              if (StringUtils.isNotBlank(fileNameEndMark) && !fileNameOutput.endsWith(fileNameEndMark)
                  && fileNameOutput.contains(fileNameEndMark)) {
                fileNameOutput = StringUtils.substring(fileNameOutput, 0, fileNameOutput.indexOf(fileNameEndMark)
                    + fileNameEndMark.length());
              }
              Path ficheroDescargadoOutPath = Paths.get(rutaDescargaPath.toString(),
                  (config.isCompress() ? String.format("%s.gz", fileNameOutput) : fileNameOutput));
              LOG.trace("*******  open output file {}              ***********", ficheroDescargadoOutPath.toString());
              if (config.isCompress()) {
                try (OutputStream fileOutputStream = Files.newOutputStream(ficheroDescargadoOutPath,
                    StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
                    GZIPOutputStream goz = new GZIPOutputStream(fileOutputStream);) {
                  IOUtils.copy(response.getEntity().getContent(), goz);
                }
              }
              else {
                try (OutputStream fileOutputStream = Files.newOutputStream(ficheroDescargadoOutPath,
                    StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);) {
                  IOUtils.copy(response.getEntity().getContent(), fileOutputStream);
                }
              }

              if (lastModified != null) {
                Files.setLastModifiedTime(ficheroDescargadoOutPath, FileTime.fromMillis(lastModified.getTime()));
              }
              descargados.add(ficheroDescargadoOutPath);
              if (config.getMaxGetFiles() > 0 && descargados.size() >= config.getMaxGetFiles() || lastModified != null
                  && config.getFechaUltimoFicheroDescargar() != null
                  && lastModified.getTime() < config.getFechaUltimoFicheroDescargar().getTime()) {
                break;
              }
            }

          }
        }

      }
    }
    sortByFechaModificacionAndNombre(descargados);
    return descargados;
  }

  private void sortByFechaModificacionAndNombre(List<Path> ficheros) {
    Collections.sort(ficheros, new Comparator<Path>() {

      @Override
      public int compare(Path arg0, Path arg1) {

        try {
          int lastModified = Files.getLastModifiedTime(arg0).compareTo(Files.getLastModifiedTime(arg1));
          if (lastModified == 0) {
            return arg0.getFileName().toString().compareTo(arg1.getFileName().toString());
          }
          else {
            return lastModified;
          }
        }
        catch (IOException e) {
          LOG.warn(e.getMessage(), e);
        }
        return 0;
      }
    });
  }
}
