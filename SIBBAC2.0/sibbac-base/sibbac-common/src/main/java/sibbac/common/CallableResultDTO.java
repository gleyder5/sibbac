package sibbac.common;

import java.util.ArrayList;
import java.util.List;

public class CallableResultDTO {
  private List<String> messages = new ArrayList<>();
  private List<String> errorMessages = new ArrayList<>();
  private boolean ok = true;

  public boolean isOk() {
    return ok;
  }

  public void addMessage(String msg) {
    if (msg != null && !msg.trim().isEmpty()) {
      messages.add(msg);
    }
  }

  public void addAllMessage(List<String> msgs) {
    if (msgs != null && !msgs.isEmpty()) {
      messages.addAll(msgs);
    }
  }

  public List<String> getMessage() {
    return messages;
  }

  public void addErrorMessage(String msg) {
    if (msg != null && !msg.trim().isEmpty()) {
      ok = false;
      errorMessages.add(msg);
    }
  }

  public void addAllErrorMessage(List<String> msgs) {
    if (msgs != null && !msgs.isEmpty()) {
      for (String msg : msgs) {
        addErrorMessage(msg);
      }
    }
  }

  public List<String> getErrorMessage() {
    return errorMessages;
  }

  public void append(CallableResultDTO o) {
    addAllMessage(o.getMessage());
    addAllErrorMessage(o.getErrorMessage());
    ok &= o.isOk();
  }
  
  
}
