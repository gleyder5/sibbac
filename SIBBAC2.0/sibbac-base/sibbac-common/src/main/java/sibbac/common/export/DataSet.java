package sibbac.common.export;

import java.util.ArrayList;
import java.util.List;

public class DataSet {

	private List<String> headers;
	private List<String> values;
	private long idLinea;
	private long idPadre;
	private boolean root;
	private boolean children;

	public DataSet() {
		this(false);
	}

	public DataSet(final boolean isRoot) {
		this.headers = new ArrayList<String>();
		this.values = new ArrayList<String>();
		this.idLinea = System.currentTimeMillis();
		this.root = isRoot;
	}

	public List<String> getHeaders() {
		return this.headers;
	}

	public List<String> getValues() {
		return this.values;
	}

	public long getIdLinea() {
		return idLinea;
	}

	public void setIdLinea(long idLinea) {
		this.idLinea = idLinea;
	}

	public long getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(long idPadre) {
		this.idPadre = idPadre;
	}

	public void setHeaders(final List<String> headers) {
		this.headers = headers;
	}

	public void setValues(final List<String> values) {
		this.values = values;
	}

	public void setHasChildren(final boolean children) {
		this.children = children;
	}

	public void setRoot(final boolean root) {
		this.root = root;
	}

	public void addHeader(final String header) {
		if (!this.headers.contains(header)) {
			this.headers.add(this.headers.size(), header);
		}
	}

	public void addValue(final String value) {
		this.values.add(this.values.size(), value);
	}

	public void addHeaders(final List<String> headers) {
		this.headers.addAll(headers);
	}

	public void addValues(final List<String> values) {
		this.values.addAll(values);
	}

	public boolean hasHeaders() {
		return !this.headers.isEmpty();
	}

	public boolean hasValues() {
		return !this.values.isEmpty();
	}

	public boolean hasSomething() {
		return this.hasHeaders() || this.hasValues();
	}

	public boolean isRoot() {
		return this.root;
	}

	public boolean hasChildren() {
		return this.children;
	}
}
