package sibbac.common.export.excel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import sibbac.common.export.ExportBuilder;


@Component
public class ExcelBuilderParametrizacion  extends ExportBuilder {
	
	private static final Logger LOG = LoggerFactory.getLogger(ExcelBuilderParametrizacion.class);
	private static final String	RESULT_DESGLOSES_LIST_EXPORT = "ParametrizacionListExport";
	
	public ExcelBuilderParametrizacion() {
	}
	
	@Override
	public Object generate(final String name, Object target, final Map<String, ? extends Object> model,	final List<String> dataSets) throws IOException {
		return this.generate(name, target, model, null);
	}
	
	@Override
	public Object generate(final String name, Object target, final Map<String, ? extends Object> model) throws IOException {

		HSSFWorkbook workbook = (HSSFWorkbook) target;
		if (model != null) {
	
			// El "name" es el "servicio.comando" que esperamos,
			// normalmente.
			LOG.trace("Nombre de la hoja excel principal [name=={}]", name);

			// Partimos del LinesSet "root".
			// Primero localizamos el "root" para pintarlo en la primera
			// hoja.
			
			List<HashMap<String,Object>> listaParametrizaciones = (List<HashMap<String, Object>>) model.get("listaParametrizacionExport");
			String sheetName = null;
			int n = 0;
//			sheetName = "Sheet " + n + " " + name;
			sheetName = "Reglas";
			this.pintaDatosPrestamizaciones(workbook, workbook.createSheet(sheetName),  listaParametrizaciones);
			
			List<HashMap<String,Object>> listaAlias = (List<HashMap<String, Object>>) model.get("listaAliasExport");
			sheetName = "Alias";
			this.pintaDatosAlias(workbook, workbook.createSheet(sheetName),  listaAlias);
			String nombrePestana = "";
			for(int i= 1; i <= 1000; i ++ ){
				nombrePestana = "listaSubCtaExport" + String.valueOf(i);
				if((List<HashMap<String, Object>>) model.get(nombrePestana) != null ){
	 				List<HashMap<String,Object>> listaSubCta = (List<HashMap<String, Object>>) model.get(nombrePestana);
					sheetName = "SubCta" + String.valueOf(i);
					
					this.pintaDatosSubCta(workbook, workbook.createSheet(sheetName),  listaSubCta);
				}else{
					break;
				}
			}
		} else {
			LOG.trace("No data!");
			Sheet sheet = workbook.createSheet(getNameForSheet(name));
			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellValue("No se han recibido datos");
		}
		return workbook;
	}
	
	protected void pintaDatosPrestamizaciones(HSSFWorkbook workbook, Sheet sheet, List<HashMap<String,Object>> listaParametrizaciones) {
		LOG.trace("> Nombre Hoja Excel [name=={}]", sheet.getSheetName());


		LOG.trace("Generating an excel file with a sheet named: [{}]", sheet.getSheetName());
		LOG.trace("> Sheet: [{}]", sheet.getSheetName());

		
		int rowMin = 0;
		//Cabecera
		Row row = sheet.createRow(rowMin++);
		Cell cell = null;
		
		// Estilo de la cabecera  
		HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();  
//		hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
		hssfCellStyleCabecera.setFillBackgroundColor(new HSSFColor.WHITE().getIndex());  
		  
		 // Crear la fuente de la cabecera  
		HSSFFont hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.BLACK.index);  
		hssfCellStyleCabecera.setFont(hssfFont);

		int cellnum = 0;
		cell = row.createCell(cellnum);
		cell.setCellValue("Id. Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 1
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod. Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 2
		cell = row.createCell(cellnum);
		cell.setCellValue("Descripción Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 3
		cell = row.createCell(cellnum);
		cell.setCellValue("Id Parametrización");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 4
		cell = row.createCell(cellnum);
		cell.setCellValue("Camara");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 5
		cell = row.createCell(cellnum);
		cell.setCellValue("Segmento");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 6
		cell = row.createCell(cellnum);
		cell.setCellValue("Ref. Cliente");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 7
		cell = row.createCell(cellnum);
		cell.setCellValue("Ref Externa");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 8
		cell = row.createCell(cellnum);
		cell.setCellValue("Capacidad");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 9
		cell = row.createCell(cellnum);
		cell.setCellValue("Modelo Negocio");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//10
		cell = row.createCell(cellnum);
		cell.setCellValue("Nemotecnico");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//11
		cell = row.createCell(cellnum);
		cell.setCellValue("Miembro Compensador");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//12
		cell = row.createCell(cellnum);
		cell.setCellValue("Cta Compensación");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//13
		cell = row.createCell(cellnum);
		cell.setCellValue("Ref. Asignación");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;//14
		cell = row.createCell(cellnum);
		cell.setCellValue("Fecha Fin");
		cell.setCellStyle(hssfCellStyleCabecera);
		
		HSSFCellStyle styleGroupRojo = workbook.createCellStyle();  
		
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.RED.index);  
		styleGroupRojo.setFont(hssfFont);
		
		HSSFCellStyle styleGroupVerde = workbook.createCellStyle();  
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.GREEN.index);  
		styleGroupVerde.setFont(hssfFont);
		
		rowMin = 1;
		short numeroCeldas = 14;
		for (HashMap<String, Object> parame : listaParametrizaciones) {	
			row = sheet.createRow(rowMin);
			
			cellnum = 0;// 0
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("idRegla").toString());
			
			cellnum++;// 1
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("cdCodigoRegla"));
			
			cellnum++;// 2
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("descripcion"));
			
			cellnum++;// 3
			cell = row.createCell(cellnum);
			cell.setCellValue((Long) parame.get("idParam"));

			cellnum++;// 4
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("camara"));

			
			cellnum++;// 5
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("segmento"));
			
			cellnum++;// 6
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("refCliente"));
			
			cellnum++;// 7
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("refExterna"));
			
			cellnum++;// 8
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("capacidad"));
			
			cellnum++;// 9
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("modelNegocio"));
			
			cellnum++;//10
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("nemotecnico"));
			
			cellnum++;//11
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("miembro"));
			
			cellnum++;//12
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("ctaCompe"));
			
			cellnum++;//13
			cell = row.createCell(cellnum);
			cell.setCellValue((String) parame.get("refAsignacion"));
			
			cellnum++;//14
			cell = row.createCell(cellnum);
			if(parame.get("fechafin") != null){
				DateFormat fechaFormat = new SimpleDateFormat("dd-MM-yyyy");
				String convertido = fechaFormat.format(parame.get("fechafin"));
				cell.setCellValue((String) convertido);
			}else{
				cell.setCellValue("");
			}

			rowMin++;
		}

		 for (int i=0; i<=numeroCeldas;i++)
		 {
			 sheet.autoSizeColumn(i);
		 }
		
	}

	protected void pintaDatosAlias(HSSFWorkbook workbook, Sheet sheet, List<HashMap<String,Object>> listaAlias) {
		LOG.trace("> Nombre Hoja Excel [name=={}]", sheet.getSheetName());


		LOG.trace("Generating an excel file with a sheet named: [{}]", sheet.getSheetName());
		LOG.trace("> Sheet: [{}]", sheet.getSheetName());

		
		int rowMin = 0;
		//Cabecera
		Row row = sheet.createRow(rowMin++);
		Cell cell = null;

		// Estilo de la cabecera  
		HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();  
	//	hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
		hssfCellStyleCabecera.setFillBackgroundColor(new HSSFColor.WHITE().getIndex());  
		  
		 // Crear la fuente de la cabecera  
		HSSFFont hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.BLACK.index);  
		hssfCellStyleCabecera.setFont(hssfFont);

		int cellnum = 0;
		cell = row.createCell(cellnum);
		cell.setCellValue("Id. Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 1
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod. Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 2
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod. Alias");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 3
		cell = row.createCell(cellnum);
		cell.setCellValue("Nombre Alias");
		cell.setCellStyle(hssfCellStyleCabecera);

		
		HSSFCellStyle styleGroupRojo = workbook.createCellStyle();  
		
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.RED.index);  
		styleGroupRojo.setFont(hssfFont);
		
		HSSFCellStyle styleGroupVerde = workbook.createCellStyle();  
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.GREEN.index);  
		styleGroupVerde.setFont(hssfFont);
		
		rowMin = 1;
		short numeroCeldas = 3;
		for (HashMap<String, Object> alias : listaAlias) {	
			row = sheet.createRow(rowMin);
			
			cellnum = 0;// 0
			cell = row.createCell(cellnum);
			cell.setCellValue((String) alias.get("idRegla").toString());
			
			cellnum++;// 1
			cell = row.createCell(cellnum);
			cell.setCellValue((String) alias.get("cdCodigoRegla"));
			
			cellnum++;// 2
			cell = row.createCell(cellnum);
			cell.setCellValue((String) alias.get("cdAliass"));
			
			cellnum++;// 3
			cell = row.createCell(cellnum);
			cell.setCellValue((String) alias.get("nombre"));
		
			rowMin++;
		}

		 for (int i=0; i<=numeroCeldas;i++)
		 {
			 sheet.autoSizeColumn(i);
		 }
		
	}		

	protected void pintaDatosSubCta(HSSFWorkbook workbook, Sheet sheet, List<HashMap<String,Object>> listaSubCta) {
		LOG.trace("> Nombre Hoja Excel [name=={}]", sheet.getSheetName());


		LOG.trace("Generating an excel file with a sheet named: [{}]", sheet.getSheetName());
		LOG.trace("> Sheet: [{}]", sheet.getSheetName());

		
		int rowMin = 0;
		//Cabecera
		Row row = sheet.createRow(rowMin++);
		Cell cell = null;
		
		// Estilo de la cabecera  
		HSSFCellStyle hssfCellStyleCabecera = workbook.createCellStyle();  
	//	hssfCellStyleCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
		hssfCellStyleCabecera.setFillBackgroundColor(new HSSFColor.WHITE().getIndex());  
		  
		 // Crear la fuente de la cabecera  
		HSSFFont hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.BLACK.index);  
		hssfCellStyleCabecera.setFont(hssfFont);

		int cellnum = 0;
		cell = row.createCell(cellnum);
		cell.setCellValue("Id. Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 1
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod. Regla");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 2
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod. Alias");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 3
		cell = row.createCell(cellnum);
		cell.setCellValue("Cod SubCta");
		cell.setCellStyle(hssfCellStyleCabecera);
		cellnum++;// 4
		cell = row.createCell(cellnum);
		cell.setCellValue("Nombre SubCta");
		cell.setCellStyle(hssfCellStyleCabecera);

		
		HSSFCellStyle styleGroupRojo = workbook.createCellStyle();  
		
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.RED.index);  
		styleGroupRojo.setFont(hssfFont);
		
		HSSFCellStyle styleGroupVerde = workbook.createCellStyle();  
		hssfFont = workbook.createFont();  
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
		hssfFont.setColor(HSSFColor.GREEN.index);  
		styleGroupVerde.setFont(hssfFont);
		
		rowMin = 1;
		short numeroCeldas = 4;
		for (HashMap<String, Object> subCta : listaSubCta) {	
			row = sheet.createRow(rowMin);
			
			cellnum = 0;// 0
			cell = row.createCell(cellnum);
			cell.setCellValue((String) subCta.get("idRegla").toString());
			
			cellnum++;// 1
			cell = row.createCell(cellnum);
			cell.setCellValue((String) subCta.get("cdCodigoRegla"));
			
			cellnum++;// 2
			cell = row.createCell(cellnum);
			cell.setCellValue((String) subCta.get("cdAliass"));
			
			cellnum++;// 3
			cell = row.createCell(cellnum);
			cell.setCellValue((String) subCta.get("cdSubCta"));
			
			cellnum++;// 4
			cell = row.createCell(cellnum);
			cell.setCellValue((String) subCta.get("nombre"));
				
			rowMin++;
		}

		 for (int i=0; i<=numeroCeldas;i++)
		 {
			 sheet.autoSizeColumn(i);
		 }
		
	}		
	
	@Override
	public ByteArrayOutputStream getBaos(Object target) {
		// TODO Auto-generated method stub
		return null;
	}

}
