package sibbac.common.utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class MailTemplate {

	public static final String TEMPLATE_FACTURA = "simpleTemplateMail.txt";

	private List<String> to;
	private List<String> cc;
	private List<String> bcc;

	private String subject;
	private String templateName;
	private List<InputStream> attachments;
	private List<String> attachmentNames;
	private Map<String, String> mapping;

	public MailTemplate() {
		this(null, null, null, null, null, null, null, null);
	}

	public MailTemplate(List<String> to, List<String> cc, List<String> bcc, String subject, String templateName,
			List<InputStream> attachments, List<String> attachmentNames, Map<String, String> mapping) {
		super();

		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.subject = subject;
		this.templateName = (templateName != null) ? templateName : TEMPLATE_FACTURA;
		this.attachments = attachments;
		this.attachmentNames = attachmentNames;
		this.mapping = mapping;
	}

	public String put(String key, String value) {
		return getMapping().put(key, value);
	}

	public List<String> getTo() {
		return this.to;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public List<String> getCc() {
		return this.cc;
	}

	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public List<String> getBcc() {
		return this.bcc;
	}

	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTemplateName() {
		return this.templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public List<InputStream> getAttachments() {
		return this.attachments;
	}

	public void setAttachments(List<InputStream> attachments) {
		this.attachments = attachments;
	}

	public List<String> getAttachmentNames() {
		return this.attachmentNames;
	}

	public void setAttachmentNames(List<String> attachmentNames) {
		this.attachmentNames = attachmentNames;
	}

	public Map<String, String> getMapping() {
		if (mapping == null) {
			mapping = new HashMap<String, String>();
		}
		return this.mapping;
	}

	public void setMapping(Map<String, String> mapping) {
		this.mapping = mapping;
	}

	@Override
	public String toString() {
		return "Template: [" + this.templateName + "] a: [" + ((this.to != null) ? this.to.size() : 0)
				+ "] destinatarios, con la plantilla [" + this.templateName + "]";
	}

}
