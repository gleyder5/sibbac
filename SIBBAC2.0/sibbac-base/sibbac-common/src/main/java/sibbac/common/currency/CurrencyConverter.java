package sibbac.common.currency;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CurrencyConverter {

	private static final Logger LOG = LoggerFactory.getLogger(CurrencyConverter.class);

	public static final int DEFAULT_DECIMALES = 2;

	/**
	 * Convierte un BigDecimal (un importe) a formato EURO con 2 decimales.
	 * 
	 * @param amount
	 * @return
	 */
	public static final String convertToEuro(final BigDecimal amount) {
		return convertToCurrency(amount, DEFAULT_DECIMALES, Locale.GERMANY);
	}

	/**
	 * Convierte un BigDecimal (un importe) a formato EURO con <N> decimales.
	 */
	public static final String convertToEuro(final BigDecimal amount, final int decimales) {
		return convertToCurrency(amount, decimales, Locale.GERMANY);
	}

	/**
	 * Convierte un BigDecimal (un importe) a formato UK con 2 decimales.
	 * 
	 * @param amount
	 * @return
	 */
	public static final String convertToPounds(final BigDecimal amount) {
		return convertToCurrency(amount, DEFAULT_DECIMALES, Locale.UK);
	}

	/**
	 * Convierte un BigDecimal (un importe) a formato UK con <N> decimales.
	 * 
	 * @param amount
	 * @return
	 */
	public static final String convertToPounds(final BigDecimal amount, final int decimales) {
		return convertToCurrency(amount, decimales, Locale.UK);
	}

	/**
	 * Convierte un BigDecimal (un importe) a formato US Dollar con 2 decimales.
	 * 
	 * @param amount
	 * @return
	 */
	public static final String convertToUSDollars(final BigDecimal amount) {
		return convertToCurrency(amount, DEFAULT_DECIMALES, Locale.US);
	}

	/**
	 * Convierte un BigDecimal (un importe) a formato US Dollar con
	 * <N> decimales.
	 * 
	 * @param amount
	 * @return
	 */
	public static final String convertToUSDollars(final BigDecimal amount, final int decimales) {
		return convertToCurrency(amount, decimales, Locale.US);
	}

	private static final String convertToCurrency(final BigDecimal amount, final int decimales, final Locale locale) {
		if (amount == null) {
			LOG.warn("NO hay cantidad!");
			return null;
		}
		if (locale == null) {
			LOG.warn("NO hay \"locale\"!");
			return null;
		}
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		int min = (decimales > 0) ? 1 : 0;
		currencyFormatter.setMinimumFractionDigits(min);
		currencyFormatter.setMaximumFractionDigits(decimales);
		return currencyFormatter.format(amount.doubleValue());
	}

}
