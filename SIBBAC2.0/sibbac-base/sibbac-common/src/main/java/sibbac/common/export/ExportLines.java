package sibbac.common.export;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import sibbac.webapp.beans.WebResponse;

@SuppressWarnings("unused")
public class ExportLines {

	private static final Logger LOG = LoggerFactory.getLogger(ExportLines.class);

	private ExportLines() {
	}

	public static List<List<LinesSet>> getLinesFor(final Map<String, ? extends Object> resultados,
			final List<String> dataSets) {
		String prefix = "[ExportLines:getLinesFor] ";
		if (resultados == null || resultados.isEmpty()) {
			LOG.warn(prefix + "No resultados!");
			return null;
		}

		// Exploramos lo que ha venido.
		LOG.trace(prefix + "Hemos recibido un objeto de tipo: [{}]...", resultados.getClass().getName());

		// Miramos si tenemos "dataSets"...
		List<List<LinesSet>> lineas = new ArrayList<List<LinesSet>>();
		List<LinesSet> parsed = null;
		List<Object> data = null;
		if (dataSets != null && !dataSets.isEmpty()) {
			LOG.trace(prefix + "Hemos recibido tambien \"dataSets\"... [{}]", dataSets);
			for (String dataSet : dataSets) {
				LOG.trace(prefix + "+ Localizando datos bajo [{}]...", dataSet);
				data = (List<Object>) guessData(resultados, dataSet);
				if (data == null || data.isEmpty()) {
					LOG.warn(prefix + "  No data under {}!", dataSet);
					continue;
				}
				// Devolvemos la lista de datos...
				parsed = getLinesForModel(data);
				lineas.add(parsed);
			}
		} else {
			data = (List<Object>) guessData(resultados, null);
			if (data == null || data.isEmpty()) {
				LOG.warn(prefix + "No data!");
			} else {
				// Devolvemos la lista de datos...
				parsed = getLinesForModel(data);
				lineas.add(parsed);
			}
		}
		return lineas;
	}

	private static List<LinesSet> getLinesForModel(final List<Object> data) {
		String prefix = "[ExportLines::getLinesForModel] ";

		List<DataSet> lineas = new ArrayList<DataSet>();
		DataSet linea = new DataSet(true);

		// Map<String,Object> describe(Object bean)
		Class<?> clz = null;
		String name = null;
		boolean esArray = false;
		boolean esMap = false;
		boolean esList = false;
		boolean esSet = false;
		boolean esMulti = false;
		List<DataSet> explored = null;

		// Exploramos cada entrada, que es un List de Objetos.
		LOG.trace(prefix + "Explorando...");

		LOG.trace(prefix + "--------------------------------------------");
		for (Object item : data) {
			clz = (item != null) ? item.getClass() : null;
			if (clz == null) {
				continue;
			}
			name = clz.getName();
			esArray = clz.isArray();
			esMap = Map.class.isAssignableFrom(clz);
			esList = List.class.isAssignableFrom(clz);
			esSet = Set.class.isAssignableFrom(clz);
			esMulti = esArray || esMap || esList || esSet;
			LOG.trace(prefix + "+ Explorando un elemento de tipo [{} ({})]", name, clz);
			LOG.trace(prefix + "  [{}({}:{}/{}/{}/{})]", item, esMulti, esArray, esMap, esList, esSet);
			try {
				LOG.trace(prefix + "===========================================================");
				explored = explore(item, true);
				if (explored.size() > 0) {
					// Tiene algo especial; es un tipo "complejo".
					lineas.addAll(lineas.size(), explored);
					LOG.trace(prefix + "  COMPLEX");
				} else {
					// NO tiene nada especial; debe ser un tipo basico. Se
					// agrega.
					LOG.trace(prefix + "  NORMAL");
					linea.addValue(item.toString());
				}
			} catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
				LOG.error(prefix + "ERROR({}): {}", e.getClass().getName(), e.getMessage());
			}

			LOG.trace(prefix + "--------------------------------------------");
		}

		if (linea.hasSomething()) {
			lineas.add(linea);
		}

		// Cleanup
		LOG.trace(prefix + "Limpiando...");
		List<LinesSet> definitivas = new ArrayList<LinesSet>();
		List<String> headers = null;
		List<String> values = null;
		LinesSet linesSet = null;
		long idLinea = 0;
		long idPadre = 0;
		for (DataSet l : lineas) {
			if (l.hasSomething()) {
				LOG.trace(prefix + "+ Un DataSet...");
				headers = l.getHeaders();
				values = l.getValues();
				idLinea = l.getIdLinea(); // El ID de "padre", por si tiene
											// hijos.
				idPadre = l.getIdPadre(); // El ID de "padre", por si tiene
											// hijos.
				// Miramos si tenemos ya los headers en algun sitio.
				LOG.trace(prefix + "  Buscando un LinesSet para [{}/{}]...", idLinea, idPadre);
				linesSet = findDataSet(definitivas, headers);
				if (linesSet == null) {
					LOG.trace(prefix + "  Nuevo LinesSet...");
					// Aun no esta. Lo llenamos y lo agregamos.
					linesSet = new LinesSet(l.isRoot());
					linesSet.setHeaders(headers);
					linesSet.addLine(values, l.hasChildren(), idLinea, idPadre);
					definitivas.add(linesSet);
				} else {
					// Ya esta en la lista.
					LOG.trace(prefix + "  LinesSet ya existente...");
					definitivas.remove(linesSet);
					linesSet.addLine(values, l.hasChildren(), idLinea, idPadre);
					definitivas.add(linesSet);
				}
			}
		}
		return definitivas;
	}

	public static LinesSet findRootLineSet(final List<LinesSet> linesSet) {
		if (linesSet == null || linesSet.isEmpty()) {
			return null;
		}

		LinesSet root = null;
		for (LinesSet ls : linesSet) {
			if (ls.isRoot()) {
				root = ls;
				break;
			}
		}

		return root;
	}

	private static LinesSet findDataSet(final List<LinesSet> dataSet, final List<String> headers) {
		if (dataSet == null || dataSet.isEmpty()) {
			return null;
		}
		if (headers == null || headers.isEmpty()) {
			return null;
		}
		List<String> h = null;
		LinesSet found = null;
		for (LinesSet ds : dataSet) {
			h = ds.getHeaders();
			if (sameHeaders(h, headers)) {
				found = ds;
				break;
			}
		}
		return found;
	}

	private static boolean sameHeaders(final List<String> ya, final List<String> hs) {
		List<String> _ya = new ArrayList<String>(ya);
		List<String> _hs = new ArrayList<String>(hs);

		Collections.copy(_ya, ya);
		Collections.copy(_hs, hs);
		Collections.sort(_ya);
		Collections.sort(_hs);
		if (ya.size() != hs.size()) {
			return false;
		}
		int total = ya.size();
		boolean ok = true;
		for (int n = 0; n < total; n++) {
			ok = _ya.get(n).equalsIgnoreCase(_hs.get(n));
			if (!ok) {
				break;
			}
		}
		return ok;
	}

	private static List<DataSet> exploreAsList(final List<?> lista)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		if (lista == null || lista.isEmpty()) {
			return null;
		}

		String prefix = "[ExportLines:exploreAsList] ";
		LOG.trace(prefix + "Explorando...");
		List<DataSet> lineas = new ArrayList<DataSet>();
		DataSet linea = new DataSet(false);
		List<DataSet> explored = null;
		for (Object item : lista) {
			LOG.trace(prefix + "  > [item] [{} ({})]", item, (item != null) ? item.getClass().getName() : null);
			explored = explore(item);
			if (explored.size() == 1) {
				lineas.addAll(lineas.size(), explored);
				LOG.trace(prefix + "    Added as \"explored\"");
			} else {
				if (item != null) {
					linea.addValue(item.toString());
				} else {
					linea.addValue(null);
				}
				LOG.trace(prefix + "    Added as value");
			}
		}
		lineas.add(lineas.size(), linea);

		return lineas;
	}

	private static List<DataSet> exploreAsMap(final Map<String, ?> lista)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		if (lista == null || lista.isEmpty()) {
			return null;
		}

		String prefix = "[ExportLines:exploreAsMap] ";
		LOG.trace(prefix + "Explorando...");
		List<DataSet> lineas = new ArrayList<DataSet>();
		DataSet linea = new DataSet(false);
		Set<String> keys = lista.keySet();
		Object val = null;
		List<DataSet> explored = null;
		for (String key : keys) {
			val = lista.get(key);
			LOG.trace(prefix + "  > [item] [{}=={} ({})]", key, val, (val != null) ? val.getClass().getName() : null);
			explored = explore(val);
			linea.addHeader(key);
			if (explored.size() == 1) {
				DataSet ds = explored.get(0);
				List<String> values = ds.getValues();

				lineas.addAll(lineas.size(), explored);
				LOG.trace(prefix + "    Added as \"explored\"");
			} else {
				if (val != null) {
					linea.addValue(val.toString());
				} else {
					linea.addValue(null);
				}
				LOG.trace(prefix + "    Added as value");
			}
		}
		lineas.add(lineas.size(), linea);

		return lineas;
	}

	private static List<DataSet> explore(final Object value)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		return explore(value, false, "> ");
	}

	private static List<DataSet> explore(final Object data, final boolean root)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		return explore(data, root, "> ");
	}

	@SuppressWarnings("unchecked")
	private static List<DataSet> explore(final Object data, final boolean root, final String pfx)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		if (data == null) {
			return null;
		}

		Class<?> clazz = data.getClass();
		
		String prefix = "[ExportLines:explore] " + pfx;
		List<DataSet> lineas = new ArrayList<DataSet>();
		DataSet linea = new DataSet(root);
		Object val = null;
		List<DataSet> explored = null;
		DataSet aux = null;
		LOG.trace(prefix + "Explorando el objeto [{} ({})]...", data, clazz);
		Map<String, ? extends Object> descripcion = BeanUtils.describe(data);
		Set<String> keys = descripcion.keySet();

		// Tenemos un "data"...
		if (List.class.isAssignableFrom(clazz) || Map.class.isAssignableFrom(clazz)) {
			// Es un objeto complejo.
			if (List.class.isAssignableFrom(data.getClass())) {
				LOG.trace(prefix + "  Is a List");
				explored = exploreAsList((List<Object>) data);
			} else if (Map.class.isAssignableFrom(data.getClass())) {
				LOG.trace(prefix + "  Is a Map");
				explored = exploreAsMap((Map<String, Object>) data);
			}
			for (DataSet aa : explored) {
				aa.setIdPadre(linea.getIdLinea());
				lineas.add(aa);
				LOG.trace(prefix + "    Added one line.");
			}
			linea.setHasChildren(true);
			LOG.trace(prefix + "    This line has children.");
		} else {
			// LOG.trace( prefix + " Is other thing" );
			if (Date.class.isAssignableFrom(clazz) || Number.class.isAssignableFrom(clazz)
					|| String.class.isAssignableFrom(clazz)) {
				LOG.trace(prefix + "  Is a normal thing");
				if (Date.class.isAssignableFrom(clazz)) {
					LOG.trace(prefix + "  Is a date");
				} else if (Number.class.isAssignableFrom(clazz)) {
					LOG.trace(prefix + "  Is a number");
				} else if (String.class.isAssignableFrom(clazz)) {
					LOG.trace(prefix + "  Is a string");
				}
				linea.addValue(String.valueOf(data));
				lineas.add(linea);
				return lineas;
			} else {
				LOG.trace(prefix + "  Is other thing (a \"bean\" pf type: {})", clazz);
				for (String key : keys) {
					val = PropertyUtils.getProperty(data, key);
					if (key.equalsIgnoreCase("bytes")) {
						continue;
					}
					if (key.equalsIgnoreCase("empty")) {
						continue;
					}
					if (key.equalsIgnoreCase("class")) {
						continue;
					}
					// Tenemos un par "key"/"value"...
					if (val != null) {
						LOG.trace(prefix + "+ [key=={}/value=={} ({})]", key, val, val.getClass().getName());
					} else {
						LOG.trace(prefix + "+ [key=={}/value=={} ({})]", key, val, "<null>");
					}
				}
			}
		}

		for (String key : keys) {
			// val = descripcion.get( k );
			val = PropertyUtils.getProperty(data, key);
			LOG.trace(prefix + "+ [key=={}/value=={}]", key, val);
			if (key.equalsIgnoreCase("bytes")) {
				continue;
			}
			if (key.equalsIgnoreCase("empty")) {
				continue;
			}
			if (key.equalsIgnoreCase("class")) {
				continue;
			}

			if (val instanceof List || val instanceof Map) {
				// No header
				LOG.trace(prefix + "  No header");
			} else {
				linea.addHeader(key);
				LOG.trace(prefix + "  Added header [{}]", key);
			}

			if (val != null) {
				LOG.trace(prefix + "  With value: [{}], Exploring...", val);
				if (val instanceof List) {
					// El valor es un "List".
					LOG.trace(prefix + "  > Value is a List.");
					explored = exploreAsList((List<Object>) val);
					if (explored == null)
					{
						continue;
					}
					for (DataSet aa : explored) {
						aa.setIdPadre(linea.getIdLinea());
						lineas.add(aa);
						LOG.trace(prefix + "    Added one line.");
					}
					linea.setHasChildren(true);
					LOG.trace(prefix + "    This line has children.");
				} else if (val instanceof Map) {
					// El valor es un "Map".
					LOG.trace(prefix + "  > Value is a Map.");
					explored = exploreAsMap((Map<String, Object>) val);
					for (DataSet aa : explored) {
						aa.setIdPadre(linea.getIdLinea());
						lineas.add(aa);
						LOG.trace(prefix + "    Added one line.");
					}
					linea.setHasChildren(true);
					LOG.trace(prefix + "    This line has children.");
				} else if (val instanceof Date) {
					LOG.trace(prefix + "  > Value is a date.");
					linea.addValue(val.toString());
				} else {
					LOG.trace(prefix + "  > Value is a normal value.");
					explored = explore(val, false, pfx + "> ");
					LOG.trace(prefix + "    This line has {} elements.", explored.size());
					LOG.trace(prefix + "    Added one line.");
					if (explored.size() > 0) {
						// Tiene algo especial; es un tipo "complejo".
						LOG.trace(prefix + "    COMPLEX.");
						if (explored.size() == 1) {
							aux = explored.get(0);
							LOG.trace(prefix + "    Added one line.");
							if (aux.getHeaders().size() != 0) {
								lineas.add(lineas.size(), aux);
								LOG.trace(prefix + "    + Added the header line.");
							} else {
								if (aux.getValues().size() != 0) {
									linea.addValues(aux.getValues());
									LOG.trace(prefix + "    + Added the data lines.");
								}
							}
						} else {
							lineas.addAll(lineas.size(), explored);
							LOG.trace(prefix + "    Added {} lines.", lineas.size());
						}
					} else {
						// NO tiene nada especial; debe ser un tipo basico.
						// Se agrega.
						LOG.trace(prefix + "    NORMAL.");
						linea.addValue(val.toString());
					}
				}
			} else {
				LOG.trace(prefix + "  Without value.");
				linea.addValue(null);
			}
		}

		if (!linea.hasSomething()) {
			linea.addValue(data.toString());
			LOG.trace(prefix + "Added the value {} to the current line.", data);
		}
		lineas.add(lineas.size(), linea);
		LOG.trace(prefix + "Added {} lines to the LinesSet.", lineas.size());

		return lineas;
	}

	@SuppressWarnings("unchecked")
	private static List<Object> guessData(final Map<String, ? extends Object> resultados, final String dataSet) {
		String prefix = "[ExportLines:guessData] ";
		List<Object> data = null;

		// Si nos han dado el nombre del "dataSet", mejor que mejor...
		if (dataSet != null) {
			LOG.trace(prefix + "Received dataSet: '{}'.", dataSet);
			data = (List<Object>) resultados.get(dataSet);
			if (data != null && !data.isEmpty()) {
				LOG.trace(prefix + "Found data under the key '{}'.", dataSet);
				return data;
			}
		}

		// Luego, lo intentamos con KEY_DATA.
		data = (List<Object>) resultados.get(WebResponse.KEY_DATA);
		if (data != null && !data.isEmpty()) {
			LOG.trace(prefix + "Found data under the key '{}'.", WebResponse.KEY_DATA);
			return data;
		}

		// Luego, miramos si alguna empieza por algun prefijo conocido
		Set<String> keys = resultados.keySet();
		if (keys != null && !keys.isEmpty())
			;
		{
			for (String key : keys) {
				if (key.startsWith("list") || key.startsWith("result") || key.startsWith("data")) {
					try {
						data = (List<Object>) resultados.get(key);
					} catch (ClassCastException ex) {
						LOG.error(ex.getMessage(), ex);
					}
					if (data != null && !data.isEmpty()) {
						LOG.trace(prefix + "Found data under the key '{}'.", key);
						return data;
					}
				}
			}
		}

		// Y, si no, devolvemos el propio array...
		LOG.trace(prefix + "Unable to find amy data under any the key, so returning the root model.");
		return new ArrayList<Object>(resultados.values());
	}

}
