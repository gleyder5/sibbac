package sibbac.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *	Este servicio contiene metodos de utilidad para la generacion / manipulacion
 *	de ficheros de texto plano. 
 */
@Service
public class FileTextPlainUtil {
	
	private static final Logger LOG = LoggerFactory.getLogger(FileTextPlainUtil.class);
	
	public boolean exists(String file) {
		File f = new File(file);
		return f.exists();
	}
	
	/**
	 *	Generacion de ficheros de texto plano.
	 *	@param fqnFile: nombre de fichero completamente calificado. Ej.: /sbrmv/interfacesemitidas/out/test.txt
	 *	@param content: contenido de datos del fichero
	 *	@throws Exception 
	 */
	public boolean generateTextPlain(String fqnFile, String data) {
		final File file;
		
		try {
			file = new File(fqnFile);
			if(file.createNewFile()) {
				try(OutputStream os =  new FileOutputStream(file);
						PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os, "UTF-8")))) {
					if (!data.isEmpty()) {
						writer.println(data);
					}
					writer.close();
				}
				return true;
			}
			LOG.warn("No se ha podido crear el fichero {} de manera normal",fqnFile);
		} 
		catch (UnsupportedEncodingException e) {
			LOG.error("Error de codigo fuente: no se reconoce la codificacion UTF-8");
			throw new RuntimeException("Codificacion erronea al generar fichero plano", e);
		}
		catch(IOException ioex) {
			LOG.error("No se ha creado el fichero {} por excepción", fqnFile, ioex);
		}
		return false;
	}

}
