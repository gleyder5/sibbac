package sibbac.business.canones.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CriterioAgrupacionCanonDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -1056264572999054529L;
  // CRITERIOS AGRUPACION
  private Date fecha;
  private BigDecimal precio;
  private Character sentido;
  private String mercadoContratacion, titularFinal, isin, tipoOperacionBolsa, ordenMercado, ejecucionMercado,
      miembroMercado, tipoSubasta;

  public Date getFecha() {
    return fecha;
  }

  public String getMercadoContratacion() {
    return mercadoContratacion;
  }

  public String getTitularFinal() {
    return titularFinal;
  }

  public String getIsin() {
    return isin;
  }

  public String getTipoOperacionBolsa() {
    return tipoOperacionBolsa;
  }

  public Character getSentido() {
    return sentido;
  }

  public BigDecimal getPrecio() {
    return precio;
  }

  public String getOrdenMercado() {
    return ordenMercado;
  }

  public String getEjecucionMercado() {
    return ejecucionMercado;
  }

  public String getTipoSubasta() {
    return tipoSubasta;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public void setMercadoContratacion(String mercadoContratacion) {
    this.mercadoContratacion = mercadoContratacion;
  }

  public void setTitularFinal(String titularFinal) {
    this.titularFinal = titularFinal;
  }

  public void setIsin(String isin) {
    this.isin = isin;
  }

  public void setTipoOperacionBolsa(String tipoOperacionBolsa) {
    this.tipoOperacionBolsa = tipoOperacionBolsa;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setPrecio(BigDecimal precio) {
    this.precio = precio;
  }

  public void setOrdenMercado(String ordenMercado) {
    this.ordenMercado = ordenMercado;
  }

  public void setEjecucionMercado(String ejecucionMercado) {
    this.ejecucionMercado = ejecucionMercado;
  }

  public void setTipoSubasta(String tipoSubasta) {
    this.tipoSubasta = tipoSubasta;
  }
  
  
  

  public String getMiembroMercado() {
    return miembroMercado;
  }

  public void setMiembroMercado(String miembroMercado) {
    this.miembroMercado = miembroMercado;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((ejecucionMercado == null) ? 0 : ejecucionMercado.hashCode());
    result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
    result = prime * result + ((isin == null) ? 0 : isin.hashCode());
    result = prime * result + ((mercadoContratacion == null) ? 0 : mercadoContratacion.hashCode());
    result = prime * result + ((miembroMercado == null) ? 0 : miembroMercado.hashCode());
    result = prime * result + ((ordenMercado == null) ? 0 : ordenMercado.hashCode());
    result = prime * result + ((precio == null) ? 0 : precio.hashCode());
    result = prime * result + ((sentido == null) ? 0 : sentido.hashCode());
    result = prime * result + ((tipoOperacionBolsa == null) ? 0 : tipoOperacionBolsa.hashCode());
    result = prime * result + ((tipoSubasta == null) ? 0 : tipoSubasta.hashCode());
    result = prime * result + ((titularFinal == null) ? 0 : titularFinal.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CriterioAgrupacionCanonDTO other = (CriterioAgrupacionCanonDTO) obj;
    if (ejecucionMercado == null) {
      if (other.ejecucionMercado != null)
        return false;
    }
    else if (!ejecucionMercado.equals(other.ejecucionMercado))
      return false;
    if (fecha == null) {
      if (other.fecha != null)
        return false;
    }
    else if (!fecha.equals(other.fecha))
      return false;
    if (isin == null) {
      if (other.isin != null)
        return false;
    }
    else if (!isin.equals(other.isin))
      return false;
    if (mercadoContratacion == null) {
      if (other.mercadoContratacion != null)
        return false;
    }
    else if (!mercadoContratacion.equals(other.mercadoContratacion))
      return false;
    if (miembroMercado == null) {
      if (other.miembroMercado != null)
        return false;
    }
    else if (!miembroMercado.equals(other.miembroMercado))
      return false;
    if (ordenMercado == null) {
      if (other.ordenMercado != null)
        return false;
    }
    else if (!ordenMercado.equals(other.ordenMercado))
      return false;
    if (precio == null) {
      if (other.precio != null)
        return false;
    }
    else if (!precio.equals(other.precio))
      return false;
    if (sentido == null) {
      if (other.sentido != null)
        return false;
    }
    else if (!sentido.equals(other.sentido))
      return false;
    if (tipoOperacionBolsa == null) {
      if (other.tipoOperacionBolsa != null)
        return false;
    }
    else if (!tipoOperacionBolsa.equals(other.tipoOperacionBolsa))
      return false;
    if (tipoSubasta == null) {
      if (other.tipoSubasta != null)
        return false;
    }
    else if (!tipoSubasta.equals(other.tipoSubasta))
      return false;
    if (titularFinal == null) {
      if (other.titularFinal != null)
        return false;
    }
    else if (!titularFinal.equals(other.titularFinal))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String
        .format(
            "CriterioAgrupacionCanonDTO [fecha=%s, precio=%s, sentido=%s, mercadoContratacion=%s, titularFinal=%s, isin=%s, tipoOperacionBolsa=%s, ordenMercado=%s, ejecucionMercado=%s, miembroMercado=%s, tipoSubasta=%s]",
            fecha, precio, sentido, mercadoContratacion, titularFinal, isin, tipoOperacionBolsa, ordenMercado,
            ejecucionMercado, miembroMercado, tipoSubasta);
  }

}
