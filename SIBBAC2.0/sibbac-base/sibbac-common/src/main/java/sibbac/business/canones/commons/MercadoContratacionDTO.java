package sibbac.business.canones.commons;

import java.io.Serializable;

public class MercadoContratacionDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 6490513992263688563L;
  private String id;
  private String descripcion;
  private boolean reglas;

  public String getId() {
    return id;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public boolean isReglas() {
    return reglas;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public void setReglas(boolean reglas) {
    this.reglas = reglas;
  }

  @Override
  public String toString() {
    return String.format("MercadoContratacionDTO [id=%s, descripcion=%s, reglas=%s]", id, descripcion, reglas);
  }
  
  

}
