package sibbac.business.canones.commons;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class ReglasCanonesDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 6971184649249197754L;
  private String nombreRegla;
  private TipoCanon tipoCanon;
  private TipoCalculo tipoCalculo;

  private BigInteger idReglaCanon, idReglaCanonParametrizacion;

  private Date fechaDesde, fechaHasta;

  // CRITERIOS MATCHING
  private String mercadoContratacion;
  private SimpleComparatorCanonCriteria listaSegmentoMercadoComparator;
  private List<String> listSegmentoMercadoContratacion;
  private Character sentido;
  private SimpleComparatorCanonCriteria listaTipoOperacionBolsaComparator;
  private List<String> listaTipoOperacionBolsa;
  private BigDecimal efectivoDesde, efectivoHasta;

  // private BigDecimal canonDesde, canonHasta;
  private List<String> listaIsin;
  private SimpleComparatorCanonCriteria listaIsinComparator;
  private List<String> listaAlias;
  private SimpleComparatorCanonCriteria listaAliasComparator;

  // COINDICIONES ESPECIALES
  private boolean ordenPuntoMedio = false, ordenOculta = false, ordenBloqueCombinado = false,
      ordenVolumenOculto = false, restriccionOrden = false, ejecucionSubasta = false, ordenCesta = false;

  // CRITERIOS AGRUPACION
  private boolean criterioAgrupacionFecha = false, criterioAgrupacionMercadoContratacion = false,
      criterioAgrupacionMiembroMercado = false, criterioAgrupacionTitularFinal = false, criterioAgrupacionIsin = false,
      criterioAgrupacionTipoOperacionBolsa = false, criterioAgrupacionSentido = false,
      criterioAgrupacionPrecio = false, criterioAgrupacionOrdenMercado = false,
      criterioAgrupacionEjecucionMercado = false, criterioAgrupacionTipoSubasta = false;

  private String bonificacionComparator;
  private BigDecimal importeCanonMinimoBonificacion, importeCanonMaximoBonificacion;
  // CALCULO
  private BigDecimal puntoBasicoEfectivo, importeFijo, puntoBasicoMinimo, puntoBasicoMaximo, importeMinimoFijo,
      importeMaximoFijo;

  public String getNombreRegla() {
    return nombreRegla;
  }

  public TipoCanon getTipoCanon() {
    return tipoCanon;
  }

  public TipoCalculo getTipoCalculo() {
    return tipoCalculo;
  }

  public Date getFechaDesde() {
    return fechaDesde;
  }

  public Date getFechaHasta() {
    return fechaHasta;
  }

  public String getMercadoContratacion() {
    return mercadoContratacion;
  }

  public SimpleComparatorCanonCriteria getListaSegmentoMercadoComparator() {
    return listaSegmentoMercadoComparator;
  }

  public List<String> getListSegmentoMercadoContratacion() {
    return listSegmentoMercadoContratacion;
  }

  public Character getSentido() {
    return sentido;
  }

  public SimpleComparatorCanonCriteria getListaTipoOperacionBolsaComparator() {
    return listaTipoOperacionBolsaComparator;
  }

  public List<String> getListaTipoOperacionBolsa() {
    return listaTipoOperacionBolsa;
  }

  public BigDecimal getEfectivoDesde() {
    return efectivoDesde;
  }

  public BigDecimal getEfectivoHasta() {
    return efectivoHasta;
  }

  // public BigDecimal getCanonDesde() {
  // return canonDesde;
  // }
  //
  // public BigDecimal getCanonHasta() {
  // return canonHasta;
  // }

  public List<String> getListaIsin() {
    return listaIsin;
  }

  public SimpleComparatorCanonCriteria getListaIsinComparator() {
    return listaIsinComparator;
  }

  public List<String> getListaAlias() {
    return listaAlias;
  }

  public SimpleComparatorCanonCriteria getListaAliasComparator() {
    return listaAliasComparator;
  }

  public boolean isOrdenPuntoMedio() {
    return ordenPuntoMedio;
  }

  public boolean isOrdenOculta() {
    return ordenOculta;
  }

  public boolean isOrdenBloqueCombinado() {
    return ordenBloqueCombinado;
  }

  public boolean isOrdenVolumenOculto() {
    return ordenVolumenOculto;
  }

  public boolean isRestriccionOrden() {
    return restriccionOrden;
  }

  public boolean isEjecucionSubasta() {
    return ejecucionSubasta;
  }

  public boolean isCriterioAgrupacionFecha() {
    return criterioAgrupacionFecha;
  }

  public boolean isCriterioAgrupacionMercadoContratacion() {
    return criterioAgrupacionMercadoContratacion;
  }

  public boolean isCriterioAgrupacionTitularFinal() {
    return criterioAgrupacionTitularFinal;
  }

  public boolean isCriterioAgrupacionIsin() {
    return criterioAgrupacionIsin;
  }

  public boolean isCriterioAgrupacionTipoOperacionBolsa() {
    return criterioAgrupacionTipoOperacionBolsa;
  }

  public boolean isCriterioAgrupacionSentido() {
    return criterioAgrupacionSentido;
  }

  public boolean isCriterioAgrupacionPrecio() {
    return criterioAgrupacionPrecio;
  }

  public boolean isCriterioAgrupacionOrdenMercado() {
    return criterioAgrupacionOrdenMercado;
  }

  public boolean isCriterioAgrupacionEjecucionMercado() {
    return criterioAgrupacionEjecucionMercado;
  }

  public boolean isCriterioAgrupacionTipoSubasta() {
    return criterioAgrupacionTipoSubasta;
  }

  public String getBonificacionComparator() {
    return bonificacionComparator;
  }

  public BigDecimal getPuntoBasicoEfectivo() {
    return puntoBasicoEfectivo;
  }

  public BigDecimal getImporteFijo() {
    return importeFijo;
  }

  public BigDecimal getPuntoBasicoMinimo() {
    return puntoBasicoMinimo;
  }

  public BigDecimal getPuntoBasicoMaximo() {
    return puntoBasicoMaximo;
  }

  public BigDecimal getImporteMinimoFijo() {
    return importeMinimoFijo;
  }

  public BigDecimal getImporteMaximoFijo() {
    return importeMaximoFijo;
  }

  public void setNombreRegla(String nombreRegla) {
    this.nombreRegla = nombreRegla;
  }

  public void setTipoCanon(TipoCanon tipoCanon) {
    this.tipoCanon = tipoCanon;
  }

  public void setTipoCalculo(TipoCalculo tipoCalculo) {
    this.tipoCalculo = tipoCalculo;
  }

  public void setFechaDesde(Date fechaDesde) {
    this.fechaDesde = fechaDesde;
  }

  public void setFechaHasta(Date fechaHasta) {
    this.fechaHasta = fechaHasta;
  }

  public void setMercadoContratacion(String mercadoContratacion) {
    this.mercadoContratacion = mercadoContratacion;
  }

  public void setListaSegmentoMercadoComparator(SimpleComparatorCanonCriteria listaSegmentoMercadoComparator) {
    this.listaSegmentoMercadoComparator = listaSegmentoMercadoComparator;
  }

  public void setListSegmentoMercadoContratacion(List<String> listSegmentoMercadoContratacion) {
    this.listSegmentoMercadoContratacion = listSegmentoMercadoContratacion;
  }

  public void setSentido(Character sentido) {
    this.sentido = sentido;
  }

  public void setListaTipoOperacionBolsaComparator(SimpleComparatorCanonCriteria listaTipoOperacionBolsaComparator) {
    this.listaTipoOperacionBolsaComparator = listaTipoOperacionBolsaComparator;
  }

  public void setListaTipoOperacionBolsa(List<String> listaTipoOperacionBolsa) {
    this.listaTipoOperacionBolsa = listaTipoOperacionBolsa;
  }

  public void setEfectivoDesde(BigDecimal efectivoDesde) {
    this.efectivoDesde = efectivoDesde;
  }

  public void setEfectivoHasta(BigDecimal efectivoHasta) {
    this.efectivoHasta = efectivoHasta;
  }

  // public void setCanonDesde(BigDecimal canonDesde) {
  // this.canonDesde = canonDesde;
  // }
  //
  // public void setCanonHasta(BigDecimal canonHasta) {
  // this.canonHasta = canonHasta;
  // }

  public void setListaIsin(List<String> listaIsin) {
    this.listaIsin = listaIsin;
  }

  public void setListaIsinComparator(SimpleComparatorCanonCriteria listaIsinComparator) {
    this.listaIsinComparator = listaIsinComparator;
  }

  public void setListaAlias(List<String> listaAlias) {
    this.listaAlias = listaAlias;
  }

  public void setListaAliasComparator(SimpleComparatorCanonCriteria listaAliasComparator) {
    this.listaAliasComparator = listaAliasComparator;
  }

  public void setOrdenPuntoMedio(boolean ordenPuntoMedio) {
    this.ordenPuntoMedio = ordenPuntoMedio;
  }

  public void setOrdenOculta(boolean ordenOculta) {
    this.ordenOculta = ordenOculta;
  }

  public void setOrdenBloqueCombinado(boolean ordenBloqueCombinado) {
    this.ordenBloqueCombinado = ordenBloqueCombinado;
  }

  public void setOrdenVolumenOculto(boolean ordenVolumenOculto) {
    this.ordenVolumenOculto = ordenVolumenOculto;
  }

  public void setRestriccionOrden(boolean restriccionOrden) {
    this.restriccionOrden = restriccionOrden;
  }

  public void setEjecucionSubasta(boolean ejecucionSubasta) {
    this.ejecucionSubasta = ejecucionSubasta;
  }

  public void setCriterioAgrupacionFecha(boolean criterioAgrupacionFecha) {
    this.criterioAgrupacionFecha = criterioAgrupacionFecha;
  }

  public void setCriterioAgrupacionMercadoContratacion(boolean criterioAgrupacionMercadoContratacion) {
    this.criterioAgrupacionMercadoContratacion = criterioAgrupacionMercadoContratacion;
  }

  public void setCriterioAgrupacionTitularFinal(boolean criterioAgrupacionTitularFinal) {
    this.criterioAgrupacionTitularFinal = criterioAgrupacionTitularFinal;
  }

  public void setCriterioAgrupacionIsin(boolean criterioAgrupacionIsin) {
    this.criterioAgrupacionIsin = criterioAgrupacionIsin;
  }

  public void setCriterioAgrupacionTipoOperacionBolsa(boolean criterioAgrupacionTipoOperacionBolsa) {
    this.criterioAgrupacionTipoOperacionBolsa = criterioAgrupacionTipoOperacionBolsa;
  }

  public void setCriterioAgrupacionSentido(boolean criterioAgrupacionSentido) {
    this.criterioAgrupacionSentido = criterioAgrupacionSentido;
  }

  public void setCriterioAgrupacionPrecio(boolean criterioAgrupacionPrecio) {
    this.criterioAgrupacionPrecio = criterioAgrupacionPrecio;
  }

  public void setCriterioAgrupacionOrdenMercado(boolean criterioAgrupacionOrdenMercado) {
    this.criterioAgrupacionOrdenMercado = criterioAgrupacionOrdenMercado;
  }

  public void setCriterioAgrupacionEjecucionMercado(boolean criterioAgrupacionEjecucionMercado) {
    this.criterioAgrupacionEjecucionMercado = criterioAgrupacionEjecucionMercado;
  }

  public void setCriterioAgrupacionTipoSubasta(boolean criterioAgrupacionTipoSubasta) {
    this.criterioAgrupacionTipoSubasta = criterioAgrupacionTipoSubasta;
  }

  public void setBonificacionComparator(String bonificacionComparator) {
    this.bonificacionComparator = bonificacionComparator;
  }

  public void setPuntoBasicoEfectivo(BigDecimal puntoBasicoEfectivo) {
    this.puntoBasicoEfectivo = puntoBasicoEfectivo;
  }

  public void setImporteFijo(BigDecimal importeFijo) {
    this.importeFijo = importeFijo;
  }

  public void setPuntoBasicoMinimo(BigDecimal puntoBasicoMinimo) {
    this.puntoBasicoMinimo = puntoBasicoMinimo;
  }

  public void setPuntoBasicoMaximo(BigDecimal puntoBasicoMaximo) {
    this.puntoBasicoMaximo = puntoBasicoMaximo;
  }

  public void setImporteMinimoFijo(BigDecimal importeMinimoFijo) {
    this.importeMinimoFijo = importeMinimoFijo;
  }

  public void setImporteMaximoFijo(BigDecimal importeMaximoFijo) {
    this.importeMaximoFijo = importeMaximoFijo;
  }

  public BigDecimal getImporteCanonMinimoBonificacion() {
    return importeCanonMinimoBonificacion;
  }

  public BigDecimal getImporteCanonMaximoBonificacion() {
    return importeCanonMaximoBonificacion;
  }

  public void setImporteCanonMinimoBonificacion(BigDecimal importeCanonMinimoBonificacion) {
    this.importeCanonMinimoBonificacion = importeCanonMinimoBonificacion;
  }

  public void setImporteCanonMaximoBonificacion(BigDecimal importeCanonMaximoBonificacion) {
    this.importeCanonMaximoBonificacion = importeCanonMaximoBonificacion;
  }

  public boolean isOrdenCesta() {
    return ordenCesta;
  }

  public void setOrdenCesta(boolean ordenCesta) {
    this.ordenCesta = ordenCesta;
  }

  public BigInteger getIdReglaCanon() {
    return idReglaCanon;
  }

  public BigInteger getIdReglaCanonParametrizacion() {
    return idReglaCanonParametrizacion;
  }

  public void setIdReglaCanon(BigInteger idReglaCanon) {
    this.idReglaCanon = idReglaCanon;
  }

  public void setIdReglaCanonParametrizacion(BigInteger idReglaCanonParametrizacion) {
    this.idReglaCanonParametrizacion = idReglaCanonParametrizacion;
  }

  public boolean isCriterioAgrupacionMiembroMercado() {
    return criterioAgrupacionMiembroMercado;
  }

  public void setCriterioAgrupacionMiembroMercado(boolean criterioAgrupacionMiembroMercado) {
    this.criterioAgrupacionMiembroMercado = criterioAgrupacionMiembroMercado;
  }

  @Override
  public String toString() {
    return String
        .format(
            "ReglasCanonesDTO [nombreRegla=%s, tipoCanon=%s, tipoCalculo=%s, fechaDesde=%s, fechaHasta=%s, mercadoContratacion=%s, efectivoDesde=%s, efectivoHasta=%s]",
            nombreRegla, tipoCanon, tipoCalculo, fechaDesde, fechaHasta, mercadoContratacion, efectivoDesde,
            efectivoHasta);
  }

}
