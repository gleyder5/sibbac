package sibbac.business.canones.commons;

import java.io.Closeable;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CanonesConfigDTO implements Serializable, Closeable {

  /**
   * 
   */
  private static final long serialVersionUID = -4971796999523399230L;

  private List<ReglasCanonesDTO> listReglasCanon;

  private Map<String, MercadoContratacionDTO> mapMercadosContratacion;

  private boolean writeTmct0log = true;

  public List<ReglasCanonesDTO> getListReglasCanon() {
    return listReglasCanon;
  }

  public void setListReglasCanon(List<ReglasCanonesDTO> listReglasCanon) {
    this.listReglasCanon = listReglasCanon;
  }

  public Map<String, MercadoContratacionDTO> getMapMercadosContratacion() {
    return mapMercadosContratacion;
  }

  public void setMapMercadosContratacion(Map<String, MercadoContratacionDTO> mapMercadosContratacion) {
    this.mapMercadosContratacion = mapMercadosContratacion;
  }

  public boolean isWriteTmct0log() {
    return writeTmct0log;
  }

  public void setWriteTmct0log(boolean writeTmct0log) {
    this.writeTmct0log = writeTmct0log;
  }

  @Override
  public void close() {
    listReglasCanon.clear();
    mapMercadosContratacion.clear();
  }

}
