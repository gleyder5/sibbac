package sibbac.business.canones.commons;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AgrupacionCanonByAlcDTO {

  private final ReglasCanonesDTO reglaCanon;
  private final CriterioAgrupacionCanonDTO agrupacion;
  private final List<CanonByDesgloseDTO> listDesgloses;
  private BigDecimal sumatorioCanon;
  private BigDecimal sumatorioEfectivo;
  private BigDecimal canonRepartir;

  public AgrupacionCanonByAlcDTO(ReglasCanonesDTO reglaCanon, CriterioAgrupacionCanonDTO agrupacion) {
    super();
    this.reglaCanon = reglaCanon;
    this.agrupacion = agrupacion;
    this.listDesgloses = new ArrayList<CanonByDesgloseDTO>();
    this.sumatorioCanon = BigDecimal.ZERO;
    this.sumatorioEfectivo = BigDecimal.ZERO;
  }

  public ReglasCanonesDTO getReglaCanon() {
    return reglaCanon;
  }

  public CriterioAgrupacionCanonDTO getAgrupacion() {
    return agrupacion;
  }

  public List<CanonByDesgloseDTO> getListDesgloses() {
    return listDesgloses;
  }

  public BigDecimal getSumatorioCanon() {
    return sumatorioCanon;
  }

  public BigDecimal getSumatorioEfectivo() {
    return sumatorioEfectivo;
  }

  public void setSumatorioCanon(BigDecimal sumatorioCanon) {
    this.sumatorioCanon = sumatorioCanon;
  }

  public void setSumatorioEfectivo(BigDecimal sumatorioEfectivo) {
    this.sumatorioEfectivo = sumatorioEfectivo;
  }

  public BigDecimal getCanonRepartir() {
    return canonRepartir;
  }

  public void setCanonRepartir(BigDecimal canonRepartir) {
    this.canonRepartir = canonRepartir;
  }

  @Override
  public String toString() {
    return String.format(
        "AgrupacionCanonByAlcDTO [reglaCanon=%s, agrupacion=%s, listDesgloses.size=%s,efectivo=%s, canon=%s]",
        reglaCanon, agrupacion, listDesgloses.size(), sumatorioEfectivo, sumatorioCanon);
  }

}
