package sibbac.business.canones.commons;

import java.math.BigDecimal;

public class CanonByDesgloseDTO {

  private long nudesglose;
  private BigDecimal canon = BigDecimal.ZERO;
  private BigDecimal efectivo = BigDecimal.ZERO;

  public long getNudesglose() {
    return nudesglose;
  }

  public BigDecimal getCanon() {
    return canon;
  }

  public BigDecimal getEfectivo() {
    return efectivo;
  }

  public void setNudesglose(long nudesglose) {
    this.nudesglose = nudesglose;
  }

  public void setCanon(BigDecimal canon) {
    this.canon = canon;
  }

  public void setEfectivo(BigDecimal efectivo) {
    this.efectivo = efectivo;
  }

}
