package com.isban.sibbac.velocity.data;

import com.isban.sibbac.db.dao.QueryDAO;

public class SettMap extends GeneralData {
	
	public String settlementTypeCLG ="";
	public String clearerCLG ="";
	public String accountAtClearerCLG ="";
	public String clearingModeCLG ="";
	public String custodianCLG ="";
	public String settlementTypeCLM ="";
	public String clearerCLM ="";
	public String accountAtClearerCLM ="";
	public String custodianCLM ="";
	public String marketCLM ="";
	public String settlementTypeCUG ="";
	public String custodianCUG ="";
	public String settlementTypeCUM ="";
	public String custodianCUM ="";
	public String marketCUM ="";	
	public String mode="";
	public String op="";	
	public String subAccount = "";
	public String alias="";
	
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}	
	
	public String getSubAccount() {
		return subAccount;
	}
	public void setSubAccount(String identifier) {
		this.subAccount = identifier;
	}
	public String getSettlementTypeCLG() {
		return settlementTypeCLG;
	}
	public void setSettlementTypeCLG(String settlementTypeCLG) {
		this.settlementTypeCLG = settlementTypeCLG;
	}
	public String getClearerCLG() {
		return clearerCLG;
	}
	public void setClearerCLG(String clearerCLG) {
		this.clearerCLG = clearerCLG;
	}
	public String getAccountAtClearerCLG() {
		return accountAtClearerCLG;
	}
	public void setAccountAtClearerCLG(String accountAtClearerCLG) {
		this.accountAtClearerCLG = accountAtClearerCLG;
	}
	public String getClearingModeCLG() {
		return clearingModeCLG;
	}
	public void setClearingModeCLG(String clearingModeCLG) {
		this.clearingModeCLG = clearingModeCLG;
	}
	public String getCustodianCLG() {
		return custodianCLG;
	}
	public void setCustodianCLG(String custodianCLG) {
		this.custodianCLG = custodianCLG;
	}
	public String getSettlementTypeCLM() {
		return settlementTypeCLM;
	}
	public void setSettlementTypeCLM(String settlementTypeCLM) {
		this.settlementTypeCLM = settlementTypeCLM;
	}
	public String getClearerCLM() {
		return clearerCLM;
	}
	public void setClearerCLM(String clearerCLM) {
		this.clearerCLM = clearerCLM;
	}
	public String getAccountAtClearerCLM() {
		return accountAtClearerCLM;
	}
	public void setAccountAtClearerCLM(String accountAtClearerCLM) {
		this.accountAtClearerCLM = accountAtClearerCLM;
	}
	public String getCustodianCLM() {
		return custodianCLM;
	}
	public void setCustodianCLM(String custodianCLM) {
		this.custodianCLM = custodianCLM;
	}
	public String getMarketCLM() {
		return marketCLM;
	}
	public void setMarketCLM(String marketCLM) {
		this.marketCLM = marketCLM;
	}
	public String getSettlementTypeCUG() {
		return settlementTypeCUG;
	}
	public void setSettlementTypeCUG(String settlementTypeCUG) {
		this.settlementTypeCUG = settlementTypeCUG;
	}
	public String getCustodianCUG() {
		return custodianCUG;
	}
	public void setCustodianCUG(String custodianCUG) {
		this.custodianCUG = custodianCUG;
	}
	public String getSettlementTypeCUM() {
		return settlementTypeCUM;
	}
	public void setSettlementTypeCUM(String settlementTypeCUM) {
		this.settlementTypeCUM = settlementTypeCUM;
	}
	public String getCustodianCUM() {
		return custodianCUM;
	}
	public void setCustodianCUM(String custodianCUM) {
		this.custodianCUM = custodianCUM;
	}
	public String getMarketCUM() {
		return marketCUM;
	}
	public void setMarketCUM(String marketCUM) {
		this.marketCUM = marketCUM;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	
	public void complete()  throws Exception
	{
		settlementTypeCLG = QueryDAO.getSettlementTypeValue(settlementTypeCLG);
		settlementTypeCLM = QueryDAO.getSettlementTypeValue(settlementTypeCLM);
		settlementTypeCUG = QueryDAO.getSettlementTypeValue(settlementTypeCUG);
		settlementTypeCUM = QueryDAO.getSettlementTypeValue(settlementTypeCUM);
		
		if (settlementTypeCLM == null || "".equals(settlementTypeCLM.trim()))
		{
			marketCLM="";
			clearerCLM="";
			accountAtClearerCLM="";
		}
		
		if (settlementTypeCUM == null || "".equals(settlementTypeCUM.trim()))
		{
			marketCUM="";
			custodianCUM="";
			

		}
		
	}

}
