package com.isban.sibbac.velocity;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.velocity.data.AliasSubAccount;

public class AliasSubAccountGenerator2 extends Generator {

  /**
   * @param args
   */

  public AliasSubAccountGenerator2(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    template = "PlantillaAliasSubAccount.xml";
    objectName = "aliasSubAccount";

    object = loadFields();

    AliasSubAccount aliasSubAccount = (AliasSubAccount) object;

    final String alias = aliasSubAccount.getAlias();

    final HashMap<String, String> subAccountMap = QueryDAO.getSubaccountList(alias);

    Set<String> keys = subAccountMap.keySet();

    final Object[] clavesO = keys.toArray();

    for (Object subAccountO : clavesO) {

      aliasSubAccount.setSubAccount((String) subAccountO);

      initVelocity();
      generateHTML();
    }

    return true;
  }

}
