package com.isban.sibbac.velocity;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import com.isban.sibbac.db.dao.SettMapDAO;
import com.isban.sibbac.velocity.data.SettMap;

public class SettMapGenerator extends Generator {

  /**
   * @param args
   */

  public SettMapGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    objectName = "settMap";
    object = loadFields();
    final SettMap settMap = (SettMap) object;
    saveSettMap(settMap);

    if ("".equals(settMap.getSubAccount())) {
      template = "PlantillaRegrabarAlias.xml";
      objectName = "settMap";

    }

    else {

      template = "PlantillaRegrabarSubcuenta.xml";
      objectName = "settMap";

    }

    initVelocity();
    generateHTML();

    return true;
  }

  private void saveSettMap(SettMap settMap) throws SQLException {
    final String settMapCLG = settMap.getSettlementTypeCLG();
    final String settMapCLM = settMap.getSettlementTypeCLM();
    final String settMapCUG = settMap.getSettlementTypeCUG();
    final String settMapCUM = settMap.getSettlementTypeCUM();

    if (settMapCLG != null && !"".equals(settMapCLG.trim())) {
      saveSettMapClearer(settMap, "");
    }

    if (settMapCLM != null && !"".equals(settMapCLM.trim())) {
      final String cdmercad = settMap.getMarketCLM();
      saveSettMapClearer(settMap, cdmercad);
    }

    if (settMapCUG != null && !"".equals(settMapCUG.trim())) {
      saveSettMapCustodian(settMap, "");

    }

    if (settMapCUM != null && !"".equals(settMapCUM.trim())) {
      final String cdmercad = settMap.getMarketCUM();
      saveSettMapCustodian(settMap, cdmercad);

    }

  }

  private void saveSettMapClearer(SettMap settMap, String cdmercad) throws SQLException {
    // 1. Pa buscar es cdaliass,cdsubcta,cdmarket,cdaliass,cdsubcta => devuelve
    // secuencia actual (pa dbs)
    String seq = getActualSequenceClearer(settMap, cdmercad);

    // 2. Si no es nada insert, si no update
    if (seq == null || "".equals(seq)) {

      // 4. Si es insert, opbtener secuencia (pa dbs) m�x (cdaliass,cdsubcta)
      // Ej: select max(nusecael) from megbpdta.fmeg0ael where cdaliass =
      // '23402'

      // 5. Insert: idenvfid a S, secuencia, cdaliass,cdsubcta,cdmarket,
      // cdcustod, cdcleare, acctacle
      seq = insertSettMapClearer(settMap, cdmercad);
    }
    else {
      // 3 Si es update, update por cdaliass,cdsubcta,cdmarket, cdcustod,
      // cdcleare, acctacle
      updateSettMapClearer(settMap, seq, cdmercad);
    }
    //
    // 6. Insert en la dbs: tabla, cdsubcta,cdaliass, secuencia
    insertDbs(settMap, seq, "CLEARER");

  }

  private void saveSettMapCustodian(SettMap settMap, String cdmercad) throws SQLException {
    // 1. Pa buscar es cdaliass,cdsubcta,cdmarket,cdaliass,cdsubcta => devuelve
    // secuencia actual (pa dbs)
    String seq = getActualSequenceCustodian(settMap, cdmercad);

    // 2. Si no es nada insert, si no update
    if (seq == null || "".equals(seq)) {

      // 4. Si es insert, opbtener secuencia (pa dbs) m�x (cdaliass,cdsubcta)
      // Ej: select max(nusecael) from megbpdta.fmeg0ael where cdaliass =
      // '23402'

      // 5. Insert: idenvfid a S, secuencia, cdaliass,cdsubcta,cdmarket,
      // cdcustod, cdcleare, acctacle
      seq = insertSettMapCustodian(settMap, cdmercad);
    }
    else {
      // 3 Si es update, update por cdaliass,cdsubcta,cdmarket, cdcustod,
      // cdcleare, acctacle
      updateSettMapCustodian(settMap, seq, cdmercad);
    }
    //
    // 6. Insert en la dbs: tabla, cdsubcta,cdaliass, secuencia
    insertDbs(settMap, seq, "CUSTODIO");

  }

  private String getNextSequenceCustodian(SettMap settMap) throws SQLException {
    String sequence = null;
    final String cdsubcta = settMap.getSubAccount();
    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael
      final String alias = settMap.getAlias();

      sequence = SettMapDAO.getNextSequenceCustodianAccount(alias);
    }
    else {
      // Obtener de fmeg0sel
      sequence = SettMapDAO.getNextSequenceCustodianSubAccount(cdsubcta);

    }

    return sequence;
  }

  private String getActualSequenceCustodian(SettMap settMap, String cdmercad) throws SQLException {
    String sequence = null;
    final String cdsubcta = settMap.getSubAccount();

    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael
      final String alias = settMap.getAlias();

      sequence = SettMapDAO.getActualSequenceCustodianAccount(alias, cdmercad);
    }
    else {
      // Obtener de fmeg0sel

      sequence = SettMapDAO.getActualSequenceCustodianSubAccount(cdsubcta, cdmercad);

    }

    return sequence;
  }

  private String insertSettMapCustodian(SettMap settMap, String cdmarket) throws SQLException {
    final String cdsubcta = settMap.getSubAccount();

    final String nusecael = getNextSequenceCustodian(settMap);
    final String tpsettle = ("".equals(cdmarket)) ? settMap.getSettlementTypeCUG() : settMap.getSettlementTypeCUM();
    final String cdcustod = ("".equals(cdmarket)) ? settMap.getCustodianCUG() : settMap.getCustodianCUM();
    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael

      final String cdaliass = settMap.getAlias();

      SettMapDAO.insertCustodianAccount(cdaliass, nusecael, tpsettle, cdmarket, cdcustod);

    }
    else {

      // Obtener de fmeg0sel
      SettMapDAO.insertCustodianSubAccount(cdsubcta, nusecael, tpsettle, cdmarket, cdcustod);

    }

    return nusecael;
  }

  private void updateSettMapCustodian(SettMap settMap, String seq, String cdmarket) throws SQLException {
    final String cdsubcta = settMap.getSubAccount();

    final String tpsettle = ("".equals(cdmarket)) ? settMap.getSettlementTypeCUG() : settMap.getSettlementTypeCUM();
    final String custodian = ("".equals(cdmarket)) ? settMap.getCustodianCUG() : settMap.getCustodianCUM();
    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael

      final String cdaliass = settMap.getAlias();

      SettMapDAO.updateCustodianAccount(tpsettle, cdmarket, custodian, cdaliass, seq);

    }
    else {

      // Obtener de fmeg0sel
      SettMapDAO.updateCustodianSubAccount(tpsettle, cdmarket, custodian, cdsubcta, seq);

    }

  }

  private String getNextSequenceClearer(SettMap settMap) throws SQLException {
    String sequence = null;
    final String cdsubcta = settMap.getSubAccount();
    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael
      final String alias = settMap.getAlias();

      sequence = SettMapDAO.getNextSequenceClearerAccount(alias);
    }
    else {
      // Obtener de fmeg0sel
      sequence = SettMapDAO.getNextSequenceClearerSubAccount(cdsubcta);

    }

    return sequence;
  }

  private String getActualSequenceClearer(SettMap settMap, String cdmercad) throws SQLException {
    String sequence = null;
    final String cdsubcta = settMap.getSubAccount();

    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael
      final String alias = settMap.getAlias();

      sequence = SettMapDAO.getActualSequenceClearerAccount(alias, cdmercad);
    }
    else {
      // Obtener de fmeg0sel

      sequence = SettMapDAO.getActualSequenceClearerSubAccount(cdsubcta, cdmercad);

    }

    return sequence;
  }

  private String insertSettMapClearer(SettMap settMap, String cdmarket) throws SQLException {

    final String cdsubcta = settMap.getSubAccount();

    String nusecael = getNextSequenceClearer(settMap);
    final String tpsettle = ("".equals(cdmarket)) ? settMap.getSettlementTypeCLG() : settMap.getSettlementTypeCLM();
    final String cdcleare = ("".equals(cdmarket)) ? settMap.getClearerCLG() : settMap.getClearerCLM();
    final String acctacle = ("".equals(cdmarket)) ? settMap.getAccountAtClearerCLG() : settMap.getAccountAtClearerCLM();
    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael

      final String cdaliass = settMap.getAlias();

      SettMapDAO.insertClearerAccount(cdaliass, nusecael, tpsettle, cdmarket, cdcleare, acctacle);

    }
    else {

      // Obtener de fmeg0sel
      SettMapDAO.insertClearerSubAccount(cdsubcta, nusecael, tpsettle, cdmarket, cdcleare, acctacle);

    }
    return nusecael;

  }

  private void updateSettMapClearer(SettMap settMap, String seq, String cdmarket) throws SQLException {
    final String cdsubcta = settMap.getSubAccount();

    final String tpsettle = ("".equals(cdmarket)) ? settMap.getSettlementTypeCLG() : settMap.getSettlementTypeCLM();
    final String cdcleare = ("".equals(cdmarket)) ? settMap.getClearerCLG() : settMap.getClearerCLM();
    final String acctacle = ("".equals(cdmarket)) ? settMap.getAccountAtClearerCLG() : settMap.getAccountAtClearerCLM();
    if (cdsubcta == null || "".equals(cdsubcta.trim())) {
      // Obtener de fmeg0ael

      final String cdaliass = settMap.getAlias();

      SettMapDAO.updateClearerAccount(tpsettle, cdmarket, cdcleare, acctacle, cdaliass, seq);

    }
    else {

      // Obtener de fmeg0sel
      SettMapDAO.updateClearerSubAccount(tpsettle, cdmarket, cdcleare, acctacle, cdsubcta, seq);

    }

  }

  private void insertDbs(SettMap settMap, String seq, String type) throws SQLException {

    final String cdsubcta = settMap.getSubAccount();
    final String aliass = settMap.getAlias();

    final String tabla = ("CLEARER".equals(type)) ? (("".equals(cdsubcta)) ? "FMEG0AEL" : "FMEG0SEL") : ((""
        .equals(cdsubcta)) ? "FMEG0AEC" : "FMEG0SEC");
    final String id = ("".equals(cdsubcta)) ? aliass : cdsubcta;
    final String auxiliar = id.trim() + "|" + seq;

    final SimpleDateFormat sdf1 = new SimpleDateFormat("HHmmssSSS");

    final Date d = new Date();

    final String nuoprout = sdf1.format(d);

    SettMapDAO.insertDBS(tabla, nuoprout, auxiliar);

  }

}
