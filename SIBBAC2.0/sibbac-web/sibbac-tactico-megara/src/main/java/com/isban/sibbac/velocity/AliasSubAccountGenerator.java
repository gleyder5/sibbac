package com.isban.sibbac.velocity;

import java.util.Map;
import java.util.Properties;

public class AliasSubAccountGenerator extends Generator {

  /**
   * @param args
   */

  public AliasSubAccountGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    template = "PlantillaAliasSubAccount.xml";
    objectName = "aliasSubAccount";
    object = loadFields();
    initVelocity();
    generateHTML();

    return true;
  }

}
