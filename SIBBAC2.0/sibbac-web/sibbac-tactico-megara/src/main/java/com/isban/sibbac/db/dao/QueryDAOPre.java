package com.isban.sibbac.db.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.isban.sibbac.velocity.Generator;

public class QueryDAOPre extends GenericDAO {
  private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(QueryDAOPre.class.getName());

  private static final String GET_BROCLIENT_AVAILABLE = "select e.cdident1, trim(e.cdident1)|| '-' ||trim(e.nbnameid)  from megbtdta.fmeg0ent e, megbtdta.fmeg0ead a where cdentro1 = '' and cdentro2= '' and e.tpmodeid <> 'D' and e.fhdebaja = '' and e.cdident1 = a.cdident1 and cdindadd= 'OFFICIAL' and e.cdident1 not in (select cdbrocli from megbtdta.fmeg0brc where tpmodeid <> 'D' and fhdebaja = '')";
  private static final String GET_BROCLIENT_DESCRIPTION = "select nbnameid from megbtdta.fmeg0ent where cdident1 = ?";
  private static final String GET_BROCLIENT_LIST = "select trim(cdbrocli), trim(cdbrocli)||'-'||trim(descript) from megbtdta.fmeg0brc where tpmodeid <> 'D'"; // order
                                                                                                                                                              // by
                                                                                                                                                              // descript";
  private static final String GET_ALIAS_LIST = "select trim(cdaliass),TRIM(cdaliass) ||'-' ||TRIM(DESCRALI) || '(' || trim(cdbrocli) || ')' from megbtdta.FMEG0ALI WHERE TPMODEID <> 'D'"; // order
                                                                                                                                                                                           // by
                                                                                                                                                                                           // cdaliass";
  private static final String GET_SUBACCOUNT_LIST = "select trim(c.CDSUBCTA),TRIM(c.cdsubcta) ||'-' ||TRIM(c.NBNAMEID) from megbtdta.FMEG0SUC c, megbtdta.FMEG0SUA S WHERE c.TPMODEID <> 'D' AND C.CDSUBCTA = S.CDSUBCTA AND S.TPMODEID<>'D'   AND CDALIASS = ? "; // order
                                                                                                                                                                                                                                                                   // by
                                                                                                                                                                                                                                                                   // c.cdsubcta";
  private static final String GET_SUBACCOUNT_AVAILABLE_LIST = "select trim(c.CDSUBCTA),TRIM(c.cdsubcta) ||'-' ||TRIM(c.NBNAMEID) from megbtdta.FMEG0SUC c WHERE c.TPMODEID <> 'D'  and cdsubcta not in ( select cdsubcta from megbtdta.fmeg0sua where tpmodeid<> 'D'  AND CDALIASS = ? ) "; // order
                                                                                                                                                                                                                                                                                            // by
                                                                                                                                                                                                                                                                                            // c.cdsubcta";
  private static final String GET_MARKET_LIST = "select distinct cdmarket, cdmarket from megbtdta.fmeg0cha order by cdmarket";
  private static final String GET_ENTITIES_LIST = "select trim(cdident1),trim(CDBICCID)||'-'||trim(nbnameid)||'(' || trim(cdident1) || ')', nbnameid from megbtdta.fmeg0ent where CDENTRO1= 'C' AND tpmodeid<> 'D'"; // order
                                                                                                                                                                                                                     // by
                                                                                                                                                                                                                     // nbnameid
                                                                                                                                                                                                                     // ";
  private static final String GET_ENTITIES_LIST2 = "select trim(cdident1),trim(cdident1)||'-'||trim(nbnameid)||'(' || trim(CDBICCID) || ')', nbnameid from megbtdta.fmeg0ent where CDENTRO1= 'C' AND tpmodeid<> 'D'"; // order
                                                                                                                                                                                                                      // by
                                                                                                                                                                                                                      // nbnameid
                                                                                                                                                                                                                      // ";
  private static final String GET_ENTITIES_LIST3 = "select trim(cdident1),trim(cdident1)||'-'||trim(nbnameid)||'(' || trim(CDBICCID) || ')', nbnameid from megbtdta.fmeg0ent where tpmodeid<> 'D'"; // order
                                                                                                                                                                                                    // by
                                                                                                                                                                                                    // nbnameid
                                                                                                                                                                                                    // ";
  private static final String GET_BROCLIENT_ID_BY_ALIAS = "select cdbrocli from megbtdta.fmeg0ali where cdaliass = ?";
  private static final String GET_TRADERS_LIST = "select distinct cdtrader,cdtrader from megbtdta.fmeg0ali where tpmodeid <> 'D' and cdtrader<> ''";
  private static final String GET_TRADERS_lM_LIST = "select distinct cdtradlm,cdtradlm from megbtdta.fmeg0ali where tpmodeid <> 'D' and cdtradlm<>''";
  private static final String GET_SALES_LIST = "select distinct cdsaleid,cdsaleid from megbtdta.fmeg0ali where tpmodeid <> 'D' and cdsaleid <> ''";
  private static final String GET_SALES_LM_LIST = "select distinct cdsalelm,cdsalelm from megbtdta.fmeg0ali where tpmodeid <> 'D' and cdsalelm <> ''";
  private static final String GET_MANAGERS_LIST = "select distinct cdmanage,cdmanage from megbtdta.fmeg0ali where tpmodeid <> 'D' and cdmanage <> ''";
  private static final String GET_MANAGERS_LM_LIST = "select distinct cdmanalm,cdmanalm from megbtdta.fmeg0ali where tpmodeid <> 'D' and cdmanalm <> ''";
  private static final String GET_DWH_LIST_LEVEL0 = "select distinct nunivel0, nunivel0 || '-' || trim (nbnivel0) from bsnbpdta.fdwh0oni";
  private static final String GET_DWH_LIST_LEVEL1 = "select distinct nunivel1, nunivel1 || '-' || trim (nbnivel1) from bsnbpdta.fdwh0oni where nunivel0 = ?";
  private static final String GET_DWH_LIST_LEVEL2 = "select distinct nunivel2, nunivel2 || '-' || trim (nbnivel2) from bsnbpdta.fdwh0oni where nunivel1 = ?";
  private static final String GET_DWH_LIST_LEVEL3 = "select distinct nunivel3, nunivel3 || '-' || trim (nbnivel3) from bsnbpdta.fdwh0oni where nunivel2 = ?";
  private static final String GET_DWH_LIST_LEVEL4 = "select distinct nunivel4, nunivel4 || '-' || trim (nbnivel4) from bsnbpdta.fdwh0oni where nunivel3 = ?";
  // Sequences
  private static final String GET_SETTLEMENT_INSTRUCTION_ID_INIT_VALUE = "SELECT MAX(DECIMAL(SUBSTR(CDINSLIQ,1,6))) + 1  FROM megbtdta.FMEG0CHA  WHERE FHDATETO > 20101212 and FHDAFROM > 20101212 AND CDINSLIQ <> '' AND CDINSLIQ IS NOT NULL AND LENGTH(TRIM(CDINSLIQ)) >= 6";
  private static int settlementInstructionId = 0;
  private static final String GET_ALIAS_ID_INIT_VALUE = "SELECT MAX(DECIMAL(SUBSTR(CDALIASS,1,5))) + 1  FROM megbtdta.FMEG0ALI  WHERE TPMODEID <> 'D' AND CDALIASS <> '' AND CDALIASS IS NOT NULL AND LENGTH(TRIM(CDALIASS)) = 5";
  private static int aliasId = 0;
  private static final String GET_SUBACCOUNT_ID_INIT_VALUE = "SELECT MAX(DECIMAL(SUBSTR(CDSUBCTA,1,6))) + 1  FROM megbtdta.FMEG0SUC  WHERE TPMODEID <> 'D' AND CDSUBCTA <> '' AND CDSUBCTA IS NOT NULL AND LENGTH(TRIM(CDSUBCTA)) = 6";
  private static int subAccountId = 0;
  private static final String GET_DWH_ID_INIT_VALUE = "select max(decimal(trim(cdidenti))) + 1 from megbtdta.fmeg0eot  where tpidtype = 'D'";
  private static int dwhId = 0;
  public static HashMap<Integer, Integer> aliasTable = new HashMap<Integer, Integer>();

  public static String rutaLogs;

  // Queries obtenci�n id settlement Chain
  private static final String GET_BENEFICIARY_ID = "select distinct idbenefi, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and cdbenefi = ? and idbenefi <> '' order by fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_LOCAL_CUSTODIAN_ID_1 = "select distinct idloccus, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and idloccus <> '' AND cdloccus = ? and cdmarket = ? and idcountp = ? and cdbenefi = ? order BY  fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_LOCAL_CUSTODIAN_ID_2 = "select distinct idloccus, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and idloccus <> '' AND cdloccus = ? and cdmarket = ? and idcountp = ? order BY  fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_LOCAL_CUSTODIAN_ID_3 = "select distinct idloccus, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and idloccus <> '' AND cdloccus = ? and cdmarket = ? order BY  fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_GLOBAL_CUSTODIAN_ID_1 = "select distinct idglocus, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and cdglocus <> '' and cdglocus=? and cdbenefi = ? order by fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_GLOBAL_CUSTODIAN_ID_2 = "select distinct idglocus, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and cdglocus <> '' and cdglocus=? order by fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_PLACE_OF_SETTLEMENT_ID_1 = "select distinct cdbicmar, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and cdbicmar <> ''  and cdmarket = ? AND cdloccus = ? and idcountp = ? and cdbenefi = ? order BY  fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_PLACE_OF_SETTLEMENT_ID_2 = "select distinct cdbicmar, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and cdbicmar <> ''  and cdmarket = ? AND cdloccus = ? order BY  fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_PLACE_OF_SETTLEMENT_ID_3 = "select distinct cdbicmar, fhdafrom from megbtdta.fmeg0cha where tpmodeid <> 'D'  and fhdafrom < 30000101 and cdbicmar <> ''  and cdmarket = ?  order BY  fhdafrom desc FETCH FIRST 1 ROWS ONLY";
  private static final String GET_PLACE_OF_SETTLEMENT_ID_NEW = "select distinct cdbicmar, cdbicmar from CORBTSQL.TMCT0PLA WHERE CDMERCAD = ? AND TPSETTLE = ?";

  private static final String GET_SETTYPE_VALUE = "select DSAAS400 from megbtdta.fmeg0cmg where idcamas4 = 'TPSETTLE' AND DSMEGARA = ?";
  private static final String GET_OTHER_SUBACCOUNT_SAME_ALERT_ALTA = "select a.cdsubcta, a.cdaliass from megbtdta.fmeg0sua a, megbtdta.fmeg0sot c where c.cdotheid = ?   and a.cdaliass = ? and a.tpmodeid <> 'D' and  c.cdsubcta = a.cdsubcta and c.cdotheid <> '' ";
  private static final String GET_OTHER_SUBACCOUNT_SAME_ALERT = "select distinct  s.cdsubcta, s.cdsubcta from megbtdta.fmeg0sua a, megbtdta.fmeg0sot c, megbtdta.fmeg0suc s where c.cdotheid = ?    and a.cdsubcta = ? and a.cdsubcta <> s.cdsubcta and a.tpmodeid <> 'D' and  c.cdsubcta = a.cdsubcta  and c.cdotheid <> ''  and s.tpmodeid <> 'D' and a.cdsubcta = s.cdsubcta and c.tpidtype = 'AC'";
  private static final String GET_OTHER_ALIAS_SAME_SUBACCOUNT = "select a.cdaliass from megbtdta.fmeg0sua a where a.cdsubcta = ?   and a.cdaliass <> ? and a.tpmodeid <> 'D' ";
  private static final String GET_SUBACCOUNT = "SELECT  TRIM(CDSUBCTA) || '---' || TRIM(coalesce(CDALIASS,'')) || '---' || TRIM(CDBROCLI) || '---' || TRIM(NBNAMEID) || '---' || TRIM(coalesce(CDOTHEID,'')) FROM ( SELECT TABLA1.*, C.CDOTHEID FROM (  SELECT B.CDALIASS, A.CDSUBCTA , CDBROCLI ,NBNAMEID FROM megbtdta.FMEG0SUC A LEFT JOIN  megbtdta.FMEG0SUA B ON A.CDSUBCTA = B.CDSUBCTA where A.TPMODEID <> 'D' AND B.TPMODEID <> 'D'  and A.CDSUBCTA =  ?   ) TABLA1 LEFT JOIN megbtdta.fmeg0sOT C ON TABLA1.CDSUBCTA = C.CDSUBCTA  ORDER BY CDALIASS DESC ) TABLA2 FETCH FIRST 1 ROWS ONLY";
  private static final String IS_NEW_ALIAS = "SELECT CDALIASS FROM megbtdta.FMEG0ALI WHERE CDALIASS = ? AND TPMODEID <> 'D'";
  // Solo ten�a en cuenta las activas
  // private static final String GET_NEW_SETT_INST =
  // "select trim(cdinsliq) || '---' || fhdafrom || '---' || fhdateto|| '---' || trim(cdbenefi)|| '---' ||trim(idbenefi)|| '---' ||trim(acbenefi)|| '---' || trim(cdloccus)|| '---' ||trim(idloccus)|| '---' || trim(acloccus)|| '---' || trim(cdglocus)|| '---' || trim(idglocus)|| '---' || trim(acglocus)|| '---' || trim(cdbicmar)  from megbtdta.fmeg0cha where  cdaliass = ? and cdsubcta = ? and cdmarket = ? and idcountp = ? and tpsettle = ? and tpmodeid <> 'D' and fhdateto > replace(substr(char(current timestamp),1,10),'-','') order by cdinsliq desc ";
  // private static final String GET_NEW_SETT_INST_BY_DATE =
  // "select trim(cdinsliq) || '---' || fhdafrom || '---' || fhdateto|| '---' || trim(cdbenefi)|| '---' ||trim(idbenefi)|| '---' ||trim(acbenefi)|| '---' || trim(cdloccus)|| '---' ||trim(idloccus)|| '---' || trim(acloccus)|| '---' || trim(cdglocus)|| '---' || trim(idglocus)|| '---' || trim(acglocus)|| '---' || trim(cdbicmar)  from megbtdta.fmeg0cha where  cdaliass = ? and cdsubcta = ? and cdmarket = ? and idcountp = ? and tpsettle = ? and fhdafrom = ? and fhdateto = ? and tpmodeid <> 'D' and fhdateto > replace(substr(char(current timestamp),1,10),'-','') order by cdinsliq desc ";
  private static final String GET_NEW_SETT_INST = "select trim(cdinsliq) || '---' || fhdafrom || '---' || fhdateto|| '---' || trim(cdbenefi)|| '---' ||trim(idbenefi)|| '---' ||trim(acbenefi)|| '---' || trim(cdloccus)|| '---' ||trim(idloccus)|| '---' || trim(acloccus)|| '---' || trim(cdglocus)|| '---' || trim(idglocus)|| '---' || trim(acglocus)|| '---' || trim(cdbicmar)  from megbtdta.fmeg0cha where  cdaliass = ? and cdsubcta = ? and cdmarket = ? and idcountp = ? and tpsettle = ? and tpmodeid <> 'D'  order by cdinsliq desc ";
  private static final String GET_NEW_SETT_INST_BY_DATE = "select trim(cdinsliq) || '---' || fhdafrom || '---' || fhdateto|| '---' || trim(cdbenefi)|| '---' ||trim(idbenefi)|| '---' ||trim(acbenefi)|| '---' || trim(cdloccus)|| '---' ||trim(idloccus)|| '---' || trim(acloccus)|| '---' || trim(cdglocus)|| '---' || trim(idglocus)|| '---' || trim(acglocus)|| '---' || trim(cdbicmar)  from megbtdta.fmeg0cha where  cdaliass = ? and cdsubcta = ? and cdmarket = ? and idcountp = ? and tpsettle = ? and fhdafrom = ? and fhdateto = ? and tpmodeid <> 'D' order by cdinsliq desc ";
  private static final String GET_NEW_ALIAS = "select trim(a.cdbrocli) || '---' || trim(a.cdaliass) || '---' || a.tpalloca || '---' || a.tpexallo || '---' || a.tpmaallo || '---' || a.tpconfir || '---' || a.tpcalcuu || '---' || a.tpcalclm || '---' || ISOWNACC || '---' || ISPAYMAR || '---' || ISRECALL || '---' || ISRECALM || '---' || ISRECCON || '---' || ISRECCLM || '---' || ISREQALO || '---' || ISREQALM || '---' || ISREQALC || '---' || ISREQCON || '---' || ISSTPROC || '---' || ISEXONER || '---' || ISMANRET || '---' || ISROUTIN || '---' || ISREQRET || '---' || CDTRADER || '---' || CDSALEID || '---' || CDMANAGE || '---' || STGROUP || '---' || STLEVEL0 || '---' || STLEVEL1 || '---' || STLEVEL2 || '---' || STLEVEL3 || '---' || STLEVEL4 || '---' || CDTRADLM || '---' || CDSALELM || '---' || CDMANALM || '---' || STGROULM || '---' || STLEV0LM || '---' || STLEV1LM || '---' || STLEV2LM || '---' || STLEV3LM || '---' || STLEV4LM || '---' || trim(a.descrali)  from megbtdta.fmeg0ali a where a.cdaliass = ? and a.tpmodeid <> 'D'";
  private static final String GET_CONVERSION_VALUE = "select dsmegara from megbtdta.fmeg0cmg where idcamas4 = ? and dsaas400 = ?";
  private static final String GET_OASYS_CODE = "select IDOASYSA from megbtdta.fmeg0aad where cdaliass = ? AND TPADDRES = 'OA'";
  private static final String GET_ACCOUNTING_SYSTEM = "select CDOTHEID from megbtdta.fmeg0aot where cdaliass = ? AND TPIDTYPE = ?";
  private static final String GET_ACCOUNTING_SYSTEM_DESCRIPTION = "select DESCRIP1 from megbtdta.fmeg0aot where cdaliass = ? AND TPIDTYPE = ?";
  private static final String GET_ACCOUNT_CLEARER_SETTMAP_GENERIC = "select trim(tpsettle) || '---' || trim(cdcleare)|| '---' || trim(coalesce(accatcle,'')) from megbtdta.fmeg0ael where cdaliass = ? and cdmarket = ''";
  private static final String GET_SUBACCOUNT_CLEARER_SETTMAP_GENERIC = "select trim(tpsettle) || '---' || trim(cdcleare)|| '---' || trim(coalesce(accatcle,'')) from megbtdta.fmeg0sel where cdsubcta = ? and cdmarket = ''";
  private static final String GET_ACCOUNT_CLEARER_SETTMAP_MERCAD = "select trim(tpsettle) || '---' || trim(cdcleare)|| '---' || trim(coalesce(accatcle,'')) || '---' || trim(coalesce(cdmarket,'')) from megbtdta.fmeg0ael  where cdmarket not in ('', 'MDR', 'BIL', 'VAL', 'BAR', 'MAD') AND cdaliass = ?  order by nusecael";
  private static final String GET_SUBACCOUNT_CLEARER_SETTMAP_MERCAD = "select trim(tpsettle) || '---' || trim(cdcleare)|| '---' || trim(coalesce(accatcle,'')) || '---' || trim(coalesce(cdmarket,'')) from megbtdta.fmeg0sel  where cdmarket not in ('', 'MDR', 'BIL', 'VAL', 'BAR', 'MAD') AND cdsubcta = ? order by nusecuen";
  private static final String GET_ACCOUNT_CUSTODIAN_SETTMAP_GENERIC = "select trim(tpsettle) || '---' || trim(cdcustod) from megbtdta.fmeg0aec where cdaliass = ? and cdmarket = ''";
  private static final String GET_SUBACCOUNT_CUSTODIAN_SETTMAP_GENERIC = "select trim(tpsettle) || '---' || trim(cdcustod) from megbtdta.fmeg0sec where cdsubcta = ? and cdmarket = ''";
  private static final String GET_ACCOUNT_CUSTODIAN_SETTMAP_MERCAD = "select trim(tpsettle) || '---' || trim(cdcustod) || '---' || trim(coalesce(cdmarket,'')) from megbtdta.fmeg0aec  where cdmarket not in ('', 'MDR', 'BIL', 'VAL', 'BAR', 'MAD') AND cdaliass = ? order by nusecuen";
  private static final String GET_SUBACCOUNT_CUSTODIAN_SETTMAP_MERCAD = "select trim(tpsettle) || '---' || trim(cdcustod) || '---' || trim(coalesce(cdmarket,'')) from megbtdta.fmeg0sec  where cdmarket not in ('', 'MDR', 'BIL', 'VAL', 'BAR', 'MAD') AND cdsubcta = ? order by nusecuen";
  private static final String GET_ENTITY = "select trim(cdident1) || '---' || trim(tpmodeid) from megbtdta.fmeg0ent where cdident1 = ?";
  private static final String GET_NEW_BROCLIENT = "select trim(cdbrocli) || '---' || trim(tpreside) || '---' || trim(tpmancat) || '---' || trim(ctfiscal) || '---' || trim(cdrefbnk) || '---' || trim(ctgmifid) || '---' || trim (tpclienn) from megbtdta.fmeg0brc  where cdbrocli = ?";
  private static final String GET_ORIGINAL_BROCLIENT_VALUE = "select TRIM(COALESCE(DSMEGARA, '')) from megbtdta.fmeg0cmg where idcamas4 = ? AND DSAAS400= ?";
  private static final String GET_NEW_ENTITY = "select trim(cdentro1) || '---' || trim(cdentro2)|| '---' || trim(cdident1) || '---' || trim(nbnameid)|| '---' || trim(cdbiccid)|| '---' || trim(cdreside)|| '---' ||trim(cdnation) || '---' || case tpentity when 'PM' then 'PersonMoral' else 'PersonPhysical' end from megbtdta.fmeg0ent where cdident1 = ?";
  private static final String GET_OTHER_ENTITY_IDENTIFIER = "select cdidenti from megbtdta.fmeg0eot where tpidtype = ? and cdident1 = ?";
  private static final String GET_NEW_ENTITY_ADDRESS = "SELECT TRIM(NBSTREET)|| '---' ||TRIM(NUSTREET)|| '---' ||TRIM(NBLOCAT2)|| '---' ||TRIM(NBLOCAT1)|| '---' || TRIM(CDZIPCOD) FROM megbtdta.FMEG0EAD WHERE CDIDENT1 = ? AND CDINDADD = 'OFFICIAL'";
  private static final String GET_PROVINCE_NAME = "SELECT nbprovin FROM BSNVASQL.TVAL0PRO where cdprovin = ?";
  private static final String GET_PROVINCE_NAME_FOREIGN = "select nbhppais from bsnvasql.tval0ps1 where cdiso2po = ? and fhdebaja = 0";
  // private static final String GET_NIVELS =
  // "select trim(cdnivel0)|| '---' ||trim(cdnivel1)|| '---' ||trim(cdnivel2)|| '---' ||trim(cdnivel3)|| '---' ||trim(cdnivel4) FROM BSNBPDTA.fdwh0oni , megbtdta.FMEG0ALI WHERE CDALIASS = ? AND char(nunivel0) = char(STLEV0LM) and CHAR(nunivel1) = char(STLEV1LM) and char(nunivel2) = char(STLEV2LM) and char(nunivel3) = char(STLEV3LM) and char(nunivel4) = char(STLEV4LM)";
  private static final String GET_NIVELS = "select trim(cdnivel0)|| '---' ||trim(cdnivel1)|| '---' ||trim(cdnivel2)|| '---' ||trim(cdnivel3)|| '---' ||trim(cdnivel4) FROM BSNBPDTA.fdwh0oni WHERE nunivel0 = ?  and nunivel1 = ?  and nunivel2 = ? and nunivel3 = ? and nunivel4 = ?";
  private static final String UPDATE_NIVELS = "UPDATE CORBTSQL.TCLI0EST SET CDNIVEL0=?,CDNIVEL1=?,CDNIVEL2=?,CDNIVEL3=?,CDNIVEL4=?,CDUSRMOD =  'MegaraSu', fhmodifi = replace(substr(char(current timestamp),1,10),'-','') WHERE CDPRODUC='008' AND NUCTAPRO=?";
  private static final String UPDATE_NIVELS_H = "insert into CORBTSQL.TCLI0hes (CDPRODUC, NUCTAPRO, CDNIVEL0, CDNIVEL1, CDNIVEL2, CDNIVEL3, CDNIVEL4, CDGRUPOO, NBGRUPOO, CDHPPAIS, CDPROTRA, NUCTATRA, CDPROSAL, NUCTASAL, CDPROACM, NUCTAACM, FHENTHIS, HOENTHIS, CDUSRHIS, CDACCION) select CDPRODUC, NUCTAPRO, CDNIVEL0, CDNIVEL1, CDNIVEL2, CDNIVEL3, CDNIVEL4, CDGRUPOO, NBGRUPOO, CDHPPAIS, CDPROTRA, NUCTATRA, CDPROSAL, NUCTASAL, CDPROACM, NUCTAACM, substr(replace(char(current timestamp), '-', ''),1,8), replace(char(current time), ':', ''), 'MegaraSu', 'M'  from CORBTSQL.tcli0est where cdproduc = '008' and nuctapro = 'NUCTAPRO_VALUE'";

  private static final String GET_NATIONAL_ACCOUNT = "select 1 from CORBTSQL.tcli0pcl where cdproduc='008' and nuctapro = ?";
  private static final String UPDATE_KGR_CODE = "update CORBTSQL.tcli0com set cdglobal = ?, CDUSRMOD =  'MegaraSu', fhmodifi = replace(substr(char(current timestamp),1,10),'-','') where cdproduc = '008' and nuctapro = ?";
  private static final String INSERT_TCLI0HCO = "INSERT INTO CORBTSQL.TCLI0HCO (CDACCION,CDCAMMED,CDCOMISN,CDDERLIQ,CDGLOBAL,CDIVAPAR,CDPRODUC,CDPROTER,CDTASPAR,CDTIPCAL,CDUSRHIS,FHENTHIS,HOENTHIS,IDEJELIQ,IMMINCOM,IMRIESCO,IMRIESUT,IMTRAMO1,IMTRAMO2,IMTRAMO3,NUCTAPRO,PCBANSIS,PCCOMIS1,PCCOMIS2,PCCOMIS3,PCCOMIS4,PCCOMSIS,PCSVBSIS,TPCONFIR,TPRIESGO) SELECT 'CDACCION_VALUE' ,CDCAMMED,CDCOMISN,CDDERLIQ,CDGLOBAL,CDIVAPAR,CDPRODUC,CDPROTER,CDTASPAR,CDTIPCAL,CDUSRMOD,substr(replace(char(current timestamp), '-', ''),1,8),replace(char(current time), ':', ''),IDEJELIQ,IMMINCOM,IMRIESCO,IMRIESUT,IMTRAMO1,IMTRAMO2,IMTRAMO3,NUCTAPRO,PCBANSIS,PCCOMIS1,PCCOMIS2,PCCOMIS3,PCCOMIS4,PCCOMSIS,PCSVBSIS,TPCONFIR,TPRIESGO FROM CORBTSQL.TCLI0COM WHERE CDPRODUC = '008' AND NUCTAPRO = NUCTAPRO_VALUE";
  private static final String GET_TCLI0HCO = "SELECT 1 FROM CORBTSQL.TCLI0HCO WHERE CDPRODUC = '008' AND NUCTAPRO = ?";
  private static final String GET_ALIAS_BY_BROCLIENT = "SELECT CDALIASS FROM megbtdta.FMEG0ALI WHERE CDBROCLI = ? and tpmodeid <> 'D' order by cdaliass desc";

  public QueryDAOPre() {

  }

  public static HashMap<String, String> getAvailableBroClient() throws SQLException {
    return getElementList(GET_BROCLIENT_AVAILABLE);
  }

  public static HashMap<String, String> getBroClientList() throws SQLException {
    return getElementList(GET_BROCLIENT_LIST);
  }

  public static HashMap<String, String> getAliasList() throws SQLException {
    return getElementList(GET_ALIAS_LIST);
  }

  public static HashMap<String, String> getSubaccountList(String cdaliass) throws SQLException {
    return getElementList(GET_SUBACCOUNT_LIST, cdaliass);
  }

  public static HashMap<String, String> getSubaccountAvailableList(String cdaliass) throws SQLException {
    return getElementList(GET_SUBACCOUNT_AVAILABLE_LIST, cdaliass);
  }

  public static HashMap<String, String> getMarketList() throws SQLException {
    return getElementList(GET_MARKET_LIST);
  }

  public static HashMap<String, String> getEntitiesList() throws SQLException {
    return getElementList(GET_ENTITIES_LIST);
  }

  public static HashMap<String, String> getEntitiesList2() throws SQLException {
    return getElementList(GET_ENTITIES_LIST2);
  }

  public static HashMap<String, String> getEntitiesList3() throws SQLException {
    return getElementList(GET_ENTITIES_LIST3);
  }

  public static HashMap<String, String> getAliasTradersList() throws SQLException {

    return getElementList(GET_TRADERS_LIST);
  }

  public static HashMap<String, String> getAliasSalesList() throws SQLException {

    return getElementList(GET_SALES_LIST);
  }

  public static HashMap<String, String> getAliasManagersList() throws SQLException {
    return getElementList(GET_MANAGERS_LIST);
  }

  public static HashMap<String, String> getAliasTradersLMList() throws SQLException {
    return getElementList(GET_TRADERS_lM_LIST);

  }

  public static HashMap<String, String> getAliasSalesLMList() throws SQLException {
    return getElementList(GET_SALES_LM_LIST);
  }

  public static HashMap<String, String> getAliasManagerslMList() throws SQLException {
    return getElementList(GET_MANAGERS_LM_LIST);
  }

  public static HashMap<String, String> getStatisticLevel0() throws SQLException {
    return getElementList(GET_DWH_LIST_LEVEL0);
  }

  public static HashMap<String, String> getStatisticLevel1(String previousLevel) throws SQLException {
    return getElementList(GET_DWH_LIST_LEVEL1, previousLevel);
  }

  public static HashMap<String, String> getStatisticLevel2(String previousLevel) throws SQLException {
    return getElementList(GET_DWH_LIST_LEVEL2, previousLevel);
  }

  public static HashMap<String, String> getStatisticLevel3(String previousLevel) throws SQLException {
    return getElementList(GET_DWH_LIST_LEVEL3, previousLevel);
  }

  public static HashMap<String, String> getStatisticLevel4(String previousLevel) throws SQLException {
    return getElementList(GET_DWH_LIST_LEVEL4, previousLevel);
  }

  public static String getBeneficiaryId(String elementCode) throws SQLException {

    return getValue(GET_BENEFICIARY_ID, elementCode);
  }

  public static String getLocalCustodianId(String elementCode1, String elementCode2, String elementCode3,
      String elementCode4) throws SQLException {

    String value = "";
    if (!"".equals(elementCode4)) {
      value = getValue(GET_LOCAL_CUSTODIAN_ID_1, elementCode1, elementCode2, elementCode3, elementCode4);
      if ("".equals(value))
        value = getValue(GET_LOCAL_CUSTODIAN_ID_2, elementCode1, elementCode2, elementCode3);
      if ("".equals(value))
        value = getValue(GET_LOCAL_CUSTODIAN_ID_3, elementCode1, elementCode2);
    }
    else if (!"".equals(elementCode3)) {
      value = getValue(GET_LOCAL_CUSTODIAN_ID_2, elementCode1, elementCode2, elementCode3);
      if ("".equals(value))
        value = getValue(GET_LOCAL_CUSTODIAN_ID_3, elementCode1, elementCode2);
    }
    else if (!"".equals(elementCode2)) {

      value = getValue(GET_LOCAL_CUSTODIAN_ID_3, elementCode1, elementCode2);
    }

    return value;
  }

  public static String getPlaceOfSettlement(String elementCode1, String elementCode2, String elementCode3,
      String elementCode4) throws SQLException {

    String value = "";
    if (!"".equals(elementCode4)) {
      value = getValue(GET_PLACE_OF_SETTLEMENT_ID_1, elementCode1, elementCode2, elementCode3, elementCode4);
      if ("".equals(value))
        value = getValue(GET_PLACE_OF_SETTLEMENT_ID_2, elementCode1, elementCode2);
      if ("".equals(value))
        value = getValue(GET_PLACE_OF_SETTLEMENT_ID_3, elementCode1);
    }
    else if (!"".equals(elementCode2)) {
      value = getValue(GET_PLACE_OF_SETTLEMENT_ID_2, elementCode1, elementCode2);
      if ("".equals(value))
        value = getValue(GET_PLACE_OF_SETTLEMENT_ID_3, elementCode1);
    }
    else if (!"".equals(elementCode1)) {

      value = getValue(GET_PLACE_OF_SETTLEMENT_ID_3, elementCode1);
    }

    return value;
  }

  public static String getPlaceOfSettlementNew(String elementCode1, String elementCode2) throws SQLException {

    String value = "";

    value = getValue(GET_PLACE_OF_SETTLEMENT_ID_NEW, elementCode1, elementCode2);

    return value;
  }

  public static String getGlobalCustodianId(String elementCode1, String elementCode2) throws SQLException {

    String value = "";

    if (!"".equals(elementCode2)) {

      value = getValue(GET_GLOBAL_CUSTODIAN_ID_1, elementCode1, elementCode2);
      if ("".equals(value))
        value = getValue(GET_GLOBAL_CUSTODIAN_ID_2, elementCode1);
    }
    else {
      value = getValue(GET_GLOBAL_CUSTODIAN_ID_2, elementCode1);

    }

    return value;
  }

  public static String getInternalSettlementInstructionId() throws SQLException {
    // return getValue(GET_SETTLEMENT_INSTRUCTION_ID);
    return new Integer(settlementInstructionId++).toString();
  }

  public static String getAliasId() throws SQLException {
    // return getValue(GET_ALIAS_ID_INIT_VALUE);

    Integer newAlias = new Integer(getValue(GET_ALIAS_ID_INIT_VALUE)).intValue();

    while (existsNationalAccount(newAlias.toString()) || aliasTable.containsKey(newAlias)) {
      newAlias = newAlias.intValue() + 1;

    }
    // aliasTable.put(newAlias, newAlias);
    // return new Integer(aliasId++).toString();
    return newAlias.toString();
  }

  public static String getSubAccountId() throws SQLException {
    // return getValue(GET_SUBACCOUNT_ID_INIT_VALUE);
    return new Integer(subAccountId++).toString();
  }

  public static String getDwhCode() throws SQLException {
    // return getValue(GET_DWH_ID_INIT_VALUE);
    return new Integer(dwhId++).toString();
  }

  public static void setInternalSettlementInstructionIdInitValue() throws SQLException {
    settlementInstructionId = new Integer(getValue(GET_SETTLEMENT_INSTRUCTION_ID_INIT_VALUE)).intValue();

  }

  public static void setAliasIdInitValue() throws SQLException {
    aliasId = new Integer(getValue(GET_ALIAS_ID_INIT_VALUE)).intValue();

  }

  public static void setSubAccountIdInString() throws SQLException {
    subAccountId = new Integer(getValue(GET_SUBACCOUNT_ID_INIT_VALUE)).intValue();

  }

  public static void setDwhCodeInitValue() throws SQLException {
    dwhId = new Integer(getValue(GET_DWH_ID_INIT_VALUE)).intValue();

  }

  public static String getBroclientDescription(String identifier) throws SQLException {
    return Generator.escapeXml(getElementById(GET_BROCLIENT_DESCRIPTION, identifier));

  }

  public static String getBroClientIdByAlias(String aliasId) throws SQLException {
    return getElementById(GET_BROCLIENT_ID_BY_ALIAS, aliasId);

  }

  public static void initSequences() throws SQLException {
    setInternalSettlementInstructionIdInitValue();
    setAliasIdInitValue();
    setSubAccountIdInString();
    setDwhCodeInitValue();
  }

  public static String getSettlementTypeValue(String settlementType) throws SQLException {
    return getValue(GET_SETTYPE_VALUE, settlementType);
    // return settlementType;
  }

  public static String getOtherAlertCode(String identifier, String alias) throws SQLException {

    final String otherSubaccount = getValue(GET_OTHER_SUBACCOUNT_SAME_ALERT, identifier, alias);

    return otherSubaccount;
  }

  public static String getOtherAlertCodeAlta(String identifier, String alias) throws SQLException {

    final String otherSubaccount = getValue(GET_OTHER_SUBACCOUNT_SAME_ALERT_ALTA, identifier, alias);

    return otherSubaccount;
  }

  public static String getOtherSubAccount(String identifier, String alias) throws SQLException {
    return getValue(GET_OTHER_ALIAS_SAME_SUBACCOUNT, identifier, alias);
  }

  public static boolean isNewSubAccount(String identifier) throws SQLException {
    final String isNewSubAccount = getValue(GET_SUBACCOUNT, identifier);
    return "".equals(isNewSubAccount);
  }

  public static boolean isNewAlias(String alias) throws SQLException {
    final String isNewAlias = getValue(IS_NEW_ALIAS, alias);
    return "".equals(isNewAlias);
  }

  public static boolean isNewSettInst(String alias, String subaccount, String market, String counterpartIdentifier,
      String settlementType) throws SQLException {
    final String isNewSettInst = getValue(GET_NEW_SETT_INST, alias, subaccount, market, counterpartIdentifier,
        settlementType);
    return "".equals(isNewSettInst);
  }

  public static String getNewSettInst(String alias, String subaccount, String market, String counterpartIdentifier,
      String settlementType) throws SQLException {
    return getValue(GET_NEW_SETT_INST, alias, subaccount, market, counterpartIdentifier, settlementType);

  }

  public static String getNewSettInstByDate(String alias, String subaccount, String market,
      String counterpartIdentifier, String settype, String dateFrom, String dateTo) throws SQLException {
    return getValue(GET_NEW_SETT_INST_BY_DATE, alias, subaccount, market, counterpartIdentifier, settype, dateFrom,
        dateTo);
  }

  public static String getNewAlias(String alias) throws SQLException {

    return getValue(GET_NEW_ALIAS, alias);
  }

  public static String getConversionValue(String key, String value) throws SQLException {

    return getValue(GET_CONVERSION_VALUE, key, value);
  }

  public static String getAccountingSystem(String alias, String type) throws SQLException {

    return getValue(GET_ACCOUNTING_SYSTEM, alias, type);
  }

  public static String getAccountingSystemDescription(String alias, String type) throws SQLException {

    return getValue(GET_ACCOUNTING_SYSTEM_DESCRIPTION, alias, type);
  }

  public static String getOasysCode(String alias) throws SQLException {

    return getValue(GET_OASYS_CODE, alias);
  }

  public static String getAccountClearerSettMapGeneric(String alias) throws SQLException {

    return getValue(GET_ACCOUNT_CLEARER_SETTMAP_GENERIC, alias);
  }

  public static String getSubAccountClearerSettMapGeneric(String alias) throws SQLException {

    return getValue(GET_SUBACCOUNT_CLEARER_SETTMAP_GENERIC, alias);
  }

  /*
   * public static String getAccountClearerSettMapMercad(String alias) throws
   * SQLException {
   * 
   * return getValue(GET_ACCOUNT_CLEARER_SETTMAP_MERCAD,alias); }
   */

  public static List<String> getAccountClearerSettMapMercad(String alias) throws SQLException {

    return getValues(GET_ACCOUNT_CLEARER_SETTMAP_MERCAD, alias);
  }

  public static List<String> getSubAccountClearerSettMapMercad(String alias) throws SQLException {

    return getValues(GET_SUBACCOUNT_CLEARER_SETTMAP_MERCAD, alias);
  }

  public static String getAccountCustodianSettMapGeneric(String alias) throws SQLException {
    return getValue(GET_ACCOUNT_CUSTODIAN_SETTMAP_GENERIC, alias);
  }

  public static String getSubAccountCustodianSettMapGeneric(String alias) throws SQLException {
    return getValue(GET_SUBACCOUNT_CUSTODIAN_SETTMAP_GENERIC, alias);
  }

  /*
   * public static String getAccountCustodianSettMapMercad(String alias) throws
   * SQLException { return getValue(GET_ACCOUNT_CUSTODIAN_SETTMAP_MERCAD,alias);
   * }
   */

  public static List<String> getAccountCustodianSettMapMercad(String alias) throws SQLException {
    return getValues(GET_ACCOUNT_CUSTODIAN_SETTMAP_MERCAD, alias);
  }

  public static List<String> getSubAccountCustodianSettMapMercad(String alias) throws SQLException {
    return getValues(GET_SUBACCOUNT_CUSTODIAN_SETTMAP_MERCAD, alias);
  }

  public static String getSubAccount(String subAccount) throws SQLException {

    return getValue(GET_SUBACCOUNT, subAccount);
  }

  public static String getEntity(String identifier) throws SQLException {
    return getValue(GET_ENTITY, identifier);
  }

  public static String getNewBroclient(String broClient) throws SQLException {
    return getValue(GET_NEW_BROCLIENT, broClient);
  }

  public static String getOriginalBroclientValue(String type, String value) throws SQLException {

    return getValue(GET_ORIGINAL_BROCLIENT_VALUE, type, value);
  }

  public static String getNewEntity(String entity) throws SQLException {

    return getValue(GET_NEW_ENTITY, entity);

  }

  public static String getOtherIdentities(String type, String identifier) throws SQLException {

    return getValue(GET_OTHER_ENTITY_IDENTIFIER, type, identifier);
  }

  public static String getNewEntityAddress(String identifier) throws SQLException {
    return getValue(GET_NEW_ENTITY_ADDRESS, identifier);
  }

  public static String getProvinceName(String provinceCode) throws SQLException {

    return getValue(GET_PROVINCE_NAME, provinceCode);
  }

  public static String getProvinceNameForeign(String provinceCode) throws SQLException {

    return getValue(GET_PROVINCE_NAME_FOREIGN, provinceCode);
  }

  /*
   * public static String getCdNivels(String alias) throws SQLException {
   * 
   * return getValue(GET_NIVELS, alias); }
   */

  public static String getCdNivels(String statisticLevel0LM, String statisticLevel1LM, String statisticLevel2LM,
      String statisticLevel3LM, String statisticLevel4LM) throws SQLException {

    return getValue(GET_NIVELS, statisticLevel0LM, statisticLevel1LM, statisticLevel2LM, statisticLevel3LM,
        statisticLevel4LM);
  }

  private static void updateStatisticLevelsTcli0est(String cdnivel0, String cdnivel1, String cdnivel2, String cdnivel3,
      String cdnivel4, String alias) throws SQLException {
    final String updateNivelsH = UPDATE_NIVELS_H.replaceAll("NUCTAPRO_VALUE", alias);
    getValue(updateNivelsH);
    getValue(UPDATE_NIVELS, cdnivel0, cdnivel1, cdnivel2, cdnivel3, cdnivel4, alias);

  }

  public static boolean existsNationalAccount(String alias) throws SQLException {

    final String nationalAccount = getValue(GET_NATIONAL_ACCOUNT, alias);
    return (!"".equals(nationalAccount));
  }

  public static void updateStatisticLevels(String alias, String statisticLevel0LM, String statisticLevel1LM,
      String statisticLevel2LM, String statisticLevel3LM, String statisticLevel4LM) throws SQLException {

    String out = getCdNivels(statisticLevel0LM, statisticLevel1LM, statisticLevel2LM, statisticLevel3LM,
        statisticLevel4LM);
    out += "---xxx";

    final String[] commands = out.split("---");
    final String cdnivel0 = commands[0];
    final String cdnivel1 = commands[1];
    final String cdnivel2 = commands[2];
    final String cdnivel3 = commands[3];
    final String cdnivel4 = commands[4];

    updateStatisticLevelsTcli0est(cdnivel0, cdnivel1, cdnivel2, cdnivel3, cdnivel4, alias);

  }

  public static void updateKgrCode(String cdbrocli, String kgrCode) throws SQLException {
    List<String> cdaliassList = getCdAliassByBroclient(cdbrocli);

    if (cdaliassList == null || cdaliassList.size() == 0) {

      cdaliassList = getCdAliassByBroclient(cdbrocli.replaceAll("&amp;", "&"));
    }

    for (String cdaliass : cdaliassList) {

      insertHistoricoTcli0co(cdaliass);

      getValue(UPDATE_KGR_CODE, kgrCode, cdaliass);
    }

  }

  private static void insertHistoricoTcli0co(String cdaliass) throws SQLException {

    final String cdaccion = ("".equals(getValue(GET_TCLI0HCO, cdaliass))) ? "A" : "M";

    final String sentence = INSERT_TCLI0HCO.replace("CDACCION_VALUE", cdaccion).replace("NUCTAPRO_VALUE", cdaliass);
    getValue(sentence);

  }

  private static List<String> getCdAliassByBroclient(String cdbrocli) throws SQLException {

    return getValues(GET_ALIAS_BY_BROCLIENT, cdbrocli);
  }

}
