package com.isban.sibbac.db.dao;

import java.sql.SQLException;

public class SettMapDAO extends GenericDAO {
	
	private static final String GET_ACTUAL_SEQUENCE_CLEARER_ACCOUNT = "select NUSECAEL from megbpdta.fmeg0AEL WHERE CDALIASS = ? AND CDMARKET = ? ";
	private static final String GET_ACTUAL_SEQUENCE_CLEARER_SUBACCOUNT = "select NUSECUEN from megbpdta.fmeg0SEL WHERE CDSUBCTA = ? AND CDMARKET = ? ";
	private static final String GET_ACTUAL_SEQUENCE_CUSTODIAN_ACCOUNT = "select NUSECUEN from megbpdta.fmeg0AEC WHERE CDALIASS = ? AND CDMARKET = ? ";
	private static final String GET_ACTUAL_SEQUENCE_CUSTODIAN_SUBACCOUNT = "select NUSECUEN from megbpdta.fmeg0SEC WHERE CDSUBCTA = ? AND CDMARKET = ? ";
	private static final String GET_NEXT_SEQUENCE_CLEARER_ACCOUNT = "select coalesce(max(nusecael),0) + 1 from megbpdta.fmeg0ael where cdaliass = ?";
	private static final String GET_NEXT_SEQUENCE_CLEARER_SUBACCOUNT = "select coalesce(max(nusecuen),0) + 1 from megbpdta.fmeg0sec where cdsubcta = ?";
	private static final String GET_NEXT_SEQUENCE_CUSTODIAN_ACCOUNT = "select coalesce(max(nusecuen),0) + 1 from megbpdta.fmeg0aec where cdaliass = ?";
	private static final String GET_NEXT_SEQUENCE_CUSTODIAN_SUBACCOUNT = "select coalesce(max(nusecuen),0) + 1 from megbpdta.fmeg0sec where cdsubcta = ?";
	private static final String INSERT_SETT_MAP_CLEARER_ACCOUNT = "insert into megbpdta.fmeg0ael (CDALIASS, NUSECAEL, TPSETTLE, IDENTITM, CDMARKET, CDCLEARE, ACCATCLE, IDENVFID) values (?, ?, ?, '0036', ?, ?, ?, 'S')";
	private static final String INSERT_SETT_MAP_CLEARER_SUBACCOUNT = "insert into megbpdta.fmeg0sel (CDSUBCTA, NUSECUEN, TPSETTLE, IDENTITM, CDMARKET, CDCLEARE, ACCATCLE, IDENVFID, TPCLEMOD) values (?, ?, ?, '0036', ?, ?, ?, 'S', '')";
	private static final String INSERT_SETT_MAP_CUSTODIAN_ACCOUNT = "insert into megbpdta.fmeg0aec (CDALIASS, NUSECUEN, TPSETTLE, IDENTITM, CDMARKET, CDCUSTOD, IDENVFID) values (?, ?, ?, '0036', ?, ?, 'S')";
	private static final String INSERT_SETT_MAP_CUSTODIAN_SUBACCOUNT = "insert into megbpdta.fmeg0sec (CDSUBCTA, NUSECUEN, TPSETTLE, IDENTITM, CDMARKET, CDCUSTOD, IDENVFID) values (?, ?, ?, '0036', ?, ?, 'S')";
	private static final String UPDATE_SETT_MAP_CLEARER_ACCOUNT = "UPDATE megbpdta.fmeg0ael SET TPSETTLE = ?,  CDMARKET  = ?, CDCLEARE  = ?,ACCATCLE = ?  WHERE CDALIASS = ? AND NUSECAEL = ? ";
	private static final String UPDATE_SETT_MAP_CLEARER_SUBACCOUNT = "UPDATE megbpdta.fmeg0sel SET TPSETTLE = ?, CDMARKET  = ?, CDCLEARE  = ?, ACCATCLE = ? WHERE CDSUBCTA = ? AND NUSECUEN = ?";
	private static final String UPDATE_SETT_MAP_CUSTODIAN_ACCOUNT = "UPDATE megbpdta.fmeg0aec SET TPSETTLE = ?,  CDMARKET  = ? ,CDCUSTOD = ?  WHERE CDALIASS = ? AND NUSECUEN = ? ";
	private static final String UPDATE_SETT_MAP_CUSTODIAN_SUBACCOUNT = "UPDATE megbpdta.fmeg0sec SET TPSETTLE = ?, CDMARKET  = ?, CDCUSTOD = ? WHERE CDSUBCTA = ? AND NUSECUEN = ?";
	private static final String INSERT_DBS = "insert into bsnbpsql.tmct0dbs (NTABLA, FECHA, TIPO, NUOPROUT, NUPAREJE, NSEC1, NSEC2, AUXILIAR, FHAUDIT, CDUSUAUD, CDESTADO) values (?, CURRENT DATE, 'M', ?, 1, 1, 0, ?, current timestamp, 'JDO', 'N')";

	public static String getActualSequenceClearerAccount(String alias, String cdmarket) throws SQLException {		
		String value = null;
		 value = getValue(GET_ACTUAL_SEQUENCE_CLEARER_ACCOUNT, alias, cdmarket);
		return value;
	}
	public static String getActualSequenceCustodianAccount(String alias, String cdmarket) throws SQLException {		
		String value = null;
		 value = getValue(GET_ACTUAL_SEQUENCE_CUSTODIAN_ACCOUNT, alias, cdmarket);
		return value;
	}

	public static String getActualSequenceClearerSubAccount(String cdsubcta, String cdmarket) throws SQLException {
		String value = null;
		 value = getValue(GET_ACTUAL_SEQUENCE_CLEARER_SUBACCOUNT, cdsubcta, cdmarket);
		return value;
	}
	
	public static String getActualSequenceCustodianSubAccount(String cdsubcta, String cdmarket) throws SQLException {
		String value = null;
		 value = getValue(GET_ACTUAL_SEQUENCE_CUSTODIAN_SUBACCOUNT, cdsubcta, cdmarket);
		return value;
	}

	public static String getNextSequenceClearerAccount(String alias) throws SQLException {
		String value = null;
		 value = getValue(GET_NEXT_SEQUENCE_CLEARER_ACCOUNT, alias);
		return value;
	}
	
	public static String getNextSequenceCustodianAccount(String alias) throws SQLException {
		String value = null;
		 value = getValue(GET_NEXT_SEQUENCE_CUSTODIAN_ACCOUNT, alias);
		return value;
	}

	public static String getNextSequenceClearerSubAccount(String cdsubcta) throws SQLException {
		String value = null;
		 value = getValue(GET_NEXT_SEQUENCE_CLEARER_SUBACCOUNT, cdsubcta);
		return value;
	}
	
	public static String getNextSequenceCustodianSubAccount(String cdsubcta) throws SQLException {
		String value = null;
		 value = getValue(GET_NEXT_SEQUENCE_CUSTODIAN_SUBACCOUNT, cdsubcta);
		return value;
	}

	public static void insertClearerAccount(String cdaliass, String nusecael, String tpsettle, String cdmarket,
			String cdcleare, String acctacle) throws SQLException {
		getValue(INSERT_SETT_MAP_CLEARER_ACCOUNT, cdaliass, nusecael, tpsettle, cdmarket, cdcleare, acctacle);
		
	}

	public static void insertClearerSubAccount(String cdsubcta, String nusecael, String tpsettle, String cdmarket,
			String cdcleare, String acctacle) throws SQLException {
		getValue(INSERT_SETT_MAP_CLEARER_SUBACCOUNT, cdsubcta, nusecael, tpsettle, cdmarket, cdcleare, acctacle);
		
	}
	
	public static void insertCustodianAccount(String cdaliass, String nusecael, String tpsettle, String cdmarket,
			String cdcleare) throws SQLException {
		getValue(INSERT_SETT_MAP_CUSTODIAN_ACCOUNT, cdaliass, nusecael, tpsettle, cdmarket, cdcleare);
		
	}

	public static void insertCustodianSubAccount(String cdsubcta, String nusecael, String tpsettle, String cdmarket,
			String cdcleare) throws SQLException {
		getValue(INSERT_SETT_MAP_CUSTODIAN_SUBACCOUNT, cdsubcta, nusecael, tpsettle, cdmarket, cdcleare);
		
	}


	public static void updateClearerAccount(String tpsettle, String cdmarket, String cdcleare, String acctacle,
			String cdaliass, String nusecael) throws SQLException {
		getValue(UPDATE_SETT_MAP_CLEARER_ACCOUNT,  tpsettle, cdmarket, cdcleare, acctacle, cdaliass, nusecael);
		
	}

	public static void updateClearerSubAccount(String tpsettle, String cdmarket, String cdcleare, String acctacle,
			String cdsubcta, String nusecael) throws SQLException {
		getValue(UPDATE_SETT_MAP_CLEARER_SUBACCOUNT,  tpsettle, cdmarket, cdcleare, acctacle, cdsubcta, nusecael);
		
	}
	
	public static void updateCustodianAccount(String tpsettle, String cdmarket, String cdcleare,
			String cdaliass, String nusecael) throws SQLException {
		getValue(UPDATE_SETT_MAP_CUSTODIAN_ACCOUNT,  tpsettle, cdmarket, cdcleare,cdaliass, nusecael);
		
	}

	public static void updateCustodianSubAccount(String tpsettle, String cdmarket, String cdcleare, 
			String cdsubcta, String nusecael) throws SQLException {
		getValue(UPDATE_SETT_MAP_CUSTODIAN_SUBACCOUNT,  tpsettle, cdmarket, cdcleare, cdsubcta, nusecael);
		
	}
	public static void insertDBS(String tabla, String nuoprout, String auxiliar) throws SQLException {
		// TODO Auto-generated method stub
		
		getValue(INSERT_DBS, tabla,nuoprout,auxiliar);
		
		;
		
		
		
		
	}

}
