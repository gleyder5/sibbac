package com.isban.sibbac.velocity;

import java.util.Map;
import java.util.Properties;

import com.isban.sibbac.velocity.data.AliasSubAccount;
import com.isban.sibbac.velocity.data.SubAccount;

public class SubAccountGenerator extends Generator {

  /**
   * @param args
   */

  public SubAccountGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {
    template = "PlantillaSubAccount.xml";
    objectName = "subAccount";
    object = loadFields();
    initVelocity();
    generateHTML();

    final String mode = ((SubAccount) object).getMode();

     if (!"H".equals(mode))

    {

      template = "PlantillaAliasSubAccount.xml";
      objectName = "aliasSubAccount";
      final String subAccount = ((SubAccount) object).getIdentifier();
      final String alias = ((SubAccount) object).getAlias();
      final AliasSubAccount aliasSubAccount = new AliasSubAccount();
      aliasSubAccount.setSubAccount(subAccount);
      aliasSubAccount.setMode(mode);
      aliasSubAccount.setAlias(alias);
      object = aliasSubAccount;
      initVelocity();
      generateHTML();

      /*
       * template= "PlantillaSubAccount.xml"; objectName = "subAccount"; object
       * = loadFields(); initVelocity(); generateHTML();
       */

    }

    return true;
  }

}
