package com.isban.sibbac.velocity.data;

import java.util.HashMap;

import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.velocity.Generator;

public class SettInst extends GeneralData {
	
	public String identifier="";
	public String identifier2="";
	public String description="";
	public String dateFrom="";
	public String dateTo="";
	public String counterpartIdentifier="";
	public String beneficiary="";
	public String beneficiaryAccount="";
	public String beneficiaryId="";
	public String globalCustodian="";
	public String globalCustodianAccount="";
	public String globalCustodianId="";
	public String localCustodian="";
	public String localCustodianAccount="";
	public String localCustodianId="";
	public String sendBeneficiary="";
	public String settlementType="";
	public String eligibleSubAccByAlias="";
	public String alias="";
	public String subaccount = "";
	public String market="";
	public String placeOfSettlement="";
	public String mode = "";
	public String op = "";	
	
	private static HashMap<String,String> cachedIdentifierValues = new HashMap<String ,String>();
	
	public String getSubaccount() {
		return subaccount;
	}

	public void setSubaccount(String subaccount) {
		this.subaccount = subaccount;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getIdentifier2() {
		return identifier2;
	}

	public void setIdentifier2(String identifier2) {
		this.identifier2 = identifier2;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getCounterpartIdentifier() {
		return counterpartIdentifier;
	}

	public void setCounterpartIdentifier(String counterpartIdentifier) {
		this.counterpartIdentifier = counterpartIdentifier;
	}

	public String getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}

	public String getBeneficiaryAccount() {
		return beneficiaryAccount.trim();
	}

	public void setBeneficiaryAccount(String beneficiaryAccount) {
		this.beneficiaryAccount = beneficiaryAccount.trim();
	}

	public String getBeneficiaryId() {
		return beneficiaryId;
	}

	public void setBeneficiaryId(String beneficiaryId) {
		this.beneficiaryId = beneficiaryId;
	}

	public String getGlobalCustodian() {
		return globalCustodian;
	}

	public void setGlobalCustodian(String globalCustodian) {
		this.globalCustodian = globalCustodian;
	}

	public String getGlobalCustodianAccount() {
		return globalCustodianAccount;
	}

	public void setGlobalCustodianAccount(String globalCustodianAccount) {
		this.globalCustodianAccount = globalCustodianAccount;
	}

	public String getGlobalCustodianId() {
		return globalCustodianId;
	}

	public void setGlobalCustodianId(String globalCustodianId) {
		this.globalCustodianId = globalCustodianId;
	}

	public String getLocalCustodian() {
		return localCustodian;
	}

	public void setLocalCustodian(String localCustodian) {
		this.localCustodian = localCustodian;
	}

	public String getLocalCustodianAccount() {
		return localCustodianAccount;
	}

	public void setLocalCustodianAccount(String localCustodianAccount) {
		this.localCustodianAccount = localCustodianAccount;
	}

	public String getLocalCustodianId() {
		return localCustodianId;
	}

	public void setLocalCustodianId(String localCustodianId) {
		this.localCustodianId = localCustodianId;
	}

	public String getSendBeneficiary() {
		return sendBeneficiary;
	}

	public void setSendBeneficiary(String sendBeneficiary) {
		this.sendBeneficiary = sendBeneficiary;
	}

	public String getSettlementType() {
		return settlementType;
	}

	public void setSettlementType(String settlementType) {
		this.settlementType = settlementType;
	}

	public String getEligibleSubAccByAlias() {
		return eligibleSubAccByAlias;
	}

	public void setEligibleSubAccByAlias(String eligibleSubAccByAlias) {
		this.eligibleSubAccByAlias = eligibleSubAccByAlias;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getPlaceOfSettlement() {
		return placeOfSettlement;
	}

	public void setPlaceOfSettlement(String placeOfSettlement) {
		this.placeOfSettlement = placeOfSettlement;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public void complete() throws Exception
	{
		
		description = counterpartIdentifier.trim() + market.trim();
		String internalId = QueryDAO.getInternalSettlementInstructionId();
		
		
		if(alias!= null && alias.trim().length()>0 && subaccount != null && subaccount.trim().length() >0)
		{
			String broClientId = QueryDAO.getBroClientIdByAlias(alias.trim());
			eligibleSubAccByAlias = subaccount.trim()+"|"+broClientId.trim()+"|"+alias.trim();
			
		}
		
		if ("".equals(identifier) )
		{
		if(alias!= null && alias.trim().length()>0 && subaccount != null && subaccount.trim().length() >0)
		{		
			 
			identifier = internalId.trim()+"-"+eligibleSubAccByAlias;	
			
			
		}
		else //if (alias!= null && alias.trim().length()>0)
		{
			identifier = internalId;
		}
		}	
		
		
		
		if ("N".equals(mode))
		{
			final String settlementTypeFormateado = QueryDAO.getSettlementTypeValue(settlementType);
			if (!QueryDAO.isNewSettInst(alias,subaccount, market,counterpartIdentifier,settlementTypeFormateado))
			{
				throw new Exception("Ya existe esta instrucci�n de liquidaci�n dada de alta. Tiene que hacer una modificaci�n");
			}
			
			else
			{
				final String cacheIdentifier = getCacheInternalId(settlementTypeFormateado);
				if (cacheIdentifier==null)
				{
					setCacheInternalId(settlementTypeFormateado);
				}
				else
				{
					identifier = cacheIdentifier;
					
					
				}
				
			}
		}		
		
		
		eligibleSubAccByAlias = Generator.escapeXml(eligibleSubAccByAlias);		
		identifier = Generator.escapeXml(identifier);
	
	}

	private String getCacheInternalId(String settlementTypeFormateado) {
		final String key = getCacheInternalIdKey(settlementTypeFormateado);
		return cachedIdentifierValues.get(key);
	}

	private String getCacheInternalIdKey(String settlementTypeFormateado) {
		// TODO Auto-generated method stub
		return alias.trim()+subaccount.trim()+market.trim()+counterpartIdentifier.trim()+settlementTypeFormateado.trim();
	}

	private void setCacheInternalId(String settlementTypeFormateado) {
		final String key = getCacheInternalIdKey(settlementTypeFormateado);
		if (cachedIdentifierValues.get(key)== null)
			cachedIdentifierValues.put(key, identifier);
		
	}



}
