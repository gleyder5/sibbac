package com.isban.sibbac.velocity.data;

import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.velocity.Generator;

public class BroClient extends GeneralData {
	
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier.trim();
	}

	public String getActiveClient() {
		return activeClient;
	}

	public void setActiveClient(String activeClient) {
		this.activeClient = activeClient;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	public String getHasAuthorization() {
//		return hasAuthorization;
//	}
//
//	public void setHasAuthorization(String hasAuthorization) {
//		this.hasAuthorization = hasAuthorization;
//	}
//
//	public String getHasRiskAuthorization() {
//		return hasRiskAuthorization;
//	}
//
//	public void setHasRiskAuthorization(String hasRiskAuthorization) {
//		this.hasRiskAuthorization = hasRiskAuthorization;
//	}

	public String getResidenceType() {
		return residenceType;
	}

	public void setResidenceType(String residenceType) {
		this.residenceType = residenceType;
	}

	public String getManagementCategory() {
		return managementCategory;
	}

	public void setManagementCategory(String managementCategory) {
		this.managementCategory = managementCategory;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getFiscalCategory() {
		return fiscalCategory;
	}

	public void setFiscalCategory(String fiscalCategory) {
		this.fiscalCategory = fiscalCategory;
	}

	public String getBankReference() {
		return bankReference;
	}

	public void setBankReference(String bankReference) {
		this.bankReference = bankReference;
	}

	public String getMiFIDCategory() {
		return miFIDCategory;
	}

	public void setMiFIDCategory(String miFIDCategory) {
		this.miFIDCategory = miFIDCategory;
	}

	public String getClientNature() {
		return clientNature;
	}

	public void setClientNature(String clientNature) {
		this.clientNature = clientNature;
	}

	public String identifier=""; 
	public String activeClient=""; 
	public String description=""; 
	//public String hasAuthorization=""; 
	//public String hasRiskAuthorization=""; 
	public String residenceType=""; 
	public String managementCategory=""; 
	public String office=""; 
	public String fiscalCategory=""; 
	public String bankReference=""; 
	public String miFIDCategory=""; 
	public String clientNature = "";
	public String op = "";
	public String mode = "";

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	@Override
	public void complete() throws Exception {
		identifier = identifier.trim();
		description = QueryDAO.getBroclientDescription(Generator.descapeXml(identifier));
		description = Generator.escapeXml(description.trim());		
		
		
	}
	
	

}
