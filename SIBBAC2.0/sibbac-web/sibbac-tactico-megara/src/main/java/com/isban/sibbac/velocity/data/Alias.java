package com.isban.sibbac.velocity.data;

import java.util.HashMap;

import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.velocity.Generator;

public class Alias extends GeneralData {

  public String broClient = "";
  public String alias = "";
  public String oasysCode = "";
  public String allocationType = "";
  public String expectedAllocation = "";
  public String marketAllocationRule = "";
  public String confirmationType = "";
  public String calculationType = "";
  public String calculationTypeLM = "";
  public String payMarket = "false";
  public String reconcileAllocation = "false";
  public String reconcileAllocationLM = "false";
  public String reconcileConfirmation = "false";
  public String reconcileConfirmationLM = "false";
  public String requireAllocation = "false";
  public String requireAllocationLM = "false";
  public String requireAllocConfirmation = "false";
  public String requireConfirmation = "false";
  public String stp = "false";
  public String exonerated = "false";
  public String manualRetrocession = "false";
  public String routing = "false";
  public String requireRetrocesion = "false";
  public String forOwnAccount = "false";
  public String traders = "";
  public String sales = "";
  public String managers = "";
  public String statisticGroup = "";
  public String statisticLevel0 = "";
  public String statisticLevel1 = "";
  public String statisticLevel2 = "";
  public String statisticLevel3 = "";
  public String statisticLevel4 = "";
  public String tradersLM = "";
  public String salesLM = "";
  public String managersLM = "";
  public String statisticGroupLM = "";
  public String statisticLevel0LM = "";
  public String statisticLevel1LM = "";
  public String statisticLevel2LM = "";
  public String statisticLevel3LM = "";
  public String statisticLevel4LM = "";
  public String accountingSystem = "";
  public String accountingSystemLM = "";
  public String settlementTypeCLG = "";
  public String clearerCLG = "";
  public String accountAtClearerCLG = "";
  public String clearingModeCLG = "";
  public String custodianCLG = "";
  public String settlementTypeCLM[] = null;
  public String clearerCLM[] = null;
  public String accountAtClearerCLM[] = null;
  public String custodianCLM[] = null;
  public String marketCLM[] = null;
  public String settlementTypeCUG = "";
  public String custodianCUG = "";
  public String[] settlementTypeCUM = null;
  public String[] custodianCUM = null;
  public String[] marketCUM = null;
  public String mode = "";
  public String op = "";
  public String description = "";
  public String accountingBroker = "";
  public String accountingBrokerDescription = "";
  public String id_regla = "";
  public String cdTipoAlias = "";
  public String reportableMifid = "";
  public String motivoNoReportable = "";
  public String delegaTransmMifid = "N";
  public String fDesdeDelegacion = "";
  public String fHastaDelegacion = "";
  public String leiTransmisora = "";
  public String poderRepresentacion = "N";
  public String fDesdePoder = "";
  public String fHastaPoder = "";
  public String contratoBasico = "N";
  public String consentOtc = "N";
  public String consentPublVolTot = "N";

  public String getCdTipoAlias() {
    return cdTipoAlias;
  }

  public void setCdTipoAlias(String cdTipoAlias) {
    this.cdTipoAlias = cdTipoAlias;
  }

  public String getReportableMifid() {
    return reportableMifid;
  }

  public void setReportableMifid(String reportableMifid) {
    this.reportableMifid = reportableMifid;
  }

  public String getMotivoNoReportable() {
    return motivoNoReportable;
  }

  public void setMotivoNoReportable(String motivoNoReportable) {
    this.motivoNoReportable = motivoNoReportable;
  }

  public String getDelegaTransmMifid() {
    return delegaTransmMifid;
  }

  public void setDelegaTransmMifid(String delegaTransmMifid) {
    this.delegaTransmMifid = delegaTransmMifid;
  }

  public String getfDesdeDelegacion() {
    return fDesdeDelegacion;
  }

  public void setfDesdeDelegacion(String fDesdeDelegacion) {
    this.fDesdeDelegacion = fDesdeDelegacion;
  }

  public String getfHastaDelegacion() {
    return fHastaDelegacion;
  }

  public void setfHastaDelegacion(String fHastaDelegacion) {
    this.fHastaDelegacion = fHastaDelegacion;
  }

  public String getLeiTransmisora() {
    return leiTransmisora;
  }

  public void setLeiTransmisora(String leiTransmisora) {
    this.leiTransmisora = leiTransmisora;
  }

  public String getPoderRepresentacion() {
    return poderRepresentacion;
  }

  public void setPoderRepresentacion(String poderRepresentacion) {
    this.poderRepresentacion = poderRepresentacion;
  }

  public String getfDesdePoder() {
    return fDesdePoder;
  }

  public void setfDesdePoder(String fDesdePoder) {
    this.fDesdePoder = fDesdePoder;
  }

  public String getfHastaPoder() {
    return fHastaPoder;
  }

  public void setfHastaPoder(String fHastaPoder) {
    this.fHastaPoder = fHastaPoder;
  }

  public String getContratoBasico() {
    return contratoBasico;
  }

  public void setContratoBasico(String contratoBasico) {
    this.contratoBasico = contratoBasico;
  }

  public String getConsentOtc() {
    return consentOtc;
  }

  public void setConsentOtc(String consentOtc) {
    this.consentOtc = consentOtc;
  }

  public String getConsentPublVolTot() {
    return consentPublVolTot;
  }

  public void setConsentPublVolTot(String consentPublVolTot) {
    this.consentPublVolTot = consentPublVolTot;
  }

  public String getId_regla() {
    return id_regla;
  }

  public void setId_regla(String id_regla) {
    this.id_regla = id_regla;
  }

  public String getAuxiliar() {
    return auxiliar;
  }

  public void setAuxiliar(String auxiliar) {
    this.auxiliar = auxiliar;
  }

  public String getBonificado() {
    return bonificado;
  }

  public void setBonificado(String bonificado) {
    this.bonificado = bonificado;
  }

  public String getDisociar() {
    return disociar;
  }

  public void setDisociar(String disociar) {
    this.disociar = disociar;
  }

  public String getInd_neting() {
    return ind_neting;
  }

  public void setInd_neting(String ind_neting) {
    this.ind_neting = ind_neting;
  }

  public String getOwn_account() {
    return own_account;
  }

  public void setOwn_account(String own_account) {
    this.own_account = own_account;
  }

  public String getRetail_special_op() {
    return retail_special_op;
  }

  public void setRetail_special_op(String retail_special_op) {
    this.retail_special_op = retail_special_op;
  }

  public String auxiliar = "";
  public String bonificado = "";
  public String disociar = "";
  public String ind_neting = "";
  public String own_account = "";
  public String retail_special_op = "";

  public String getAccountingBrokerDescription() {
    return accountingBrokerDescription;
  }

  public void setAccountingBrokerDescription(String accountingBrokerDescription) {
    this.accountingBrokerDescription = accountingBrokerDescription;
  }

  public String getAccountingBroker() {
    return accountingBroker;
  }

  public void setAccountingBroker(String accountingBroker) {
    this.accountingBroker = accountingBroker;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getBroClient() {
    return broClient;
  }

  public void setBroClient(String broClient) {
    this.broClient = broClient;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getOasysCode() {
    return oasysCode;
  }

  public void setOasysCode(String oasysCode) {
    this.oasysCode = oasysCode;
  }

  public String getAllocationType() {
    return allocationType;
  }

  public void setAllocationType(String allocationType) {
    this.allocationType = allocationType;
  }

  public String getExpectedAllocation() {
    return expectedAllocation;
  }

  public void setExpectedAllocation(String expectedAllocation) {
    this.expectedAllocation = expectedAllocation;
  }

  public String getMarketAllocationRule() {
    return marketAllocationRule;
  }

  public void setMarketAllocationRule(String marketAllocationRule) {
    this.marketAllocationRule = marketAllocationRule;
  }

  public String getConfirmationType() {
    return confirmationType;
  }

  public void setConfirmationType(String confirmationType) {
    this.confirmationType = confirmationType;
  }

  public String getCalculationType() {
    return calculationType;
  }

  public void setCalculationType(String calculationType) {
    this.calculationType = calculationType;
  }

  public String getCalculationTypeLM() {
    return calculationTypeLM;
  }

  public void setCalculationTypeLM(String calculationTypeLM) {
    this.calculationTypeLM = calculationTypeLM;
  }

  public String getPayMarket() {
    return payMarket;
  }

  public void setPayMarket(String payMarket) {
    this.payMarket = payMarket;
  }

  public String getReconcileAllocation() {
    return reconcileAllocation;
  }

  public void setReconcileAllocation(String reconcileAllocation) {
    this.reconcileAllocation = reconcileAllocation;
  }

  public String getReconcileAllocationLM() {
    return reconcileAllocationLM;
  }

  public void setReconcileAllocationLM(String reconcileAllocationLM) {
    this.reconcileAllocationLM = reconcileAllocationLM;
  }

  public String getReconcileConfirmation() {
    return reconcileConfirmation;
  }

  public void setReconcileConfirmation(String reconcileConfirmation) {
    this.reconcileConfirmation = reconcileConfirmation;
  }

  public String getReconcileConfirmationLM() {
    return reconcileConfirmationLM;
  }

  public void setReconcileConfirmationLM(String reconcileConfirmationLM) {
    this.reconcileConfirmationLM = reconcileConfirmationLM;
  }

  public String getRequireAllocation() {
    return requireAllocation;
  }

  public void setRequireAllocation(String requireAllocation) {
    this.requireAllocation = requireAllocation;
  }

  public String getRequireAllocationLM() {
    return requireAllocationLM;
  }

  public void setRequireAllocationLM(String requireAllocationLM) {
    this.requireAllocationLM = requireAllocationLM;
  }

  public String getRequireAllocConfirmation() {
    return requireAllocConfirmation;
  }

  public void setRequireAllocConfirmation(String requireAllocConfirmation) {
    this.requireAllocConfirmation = requireAllocConfirmation;
  }

  public String getRequireConfirmation() {
    return requireConfirmation;
  }

  public void setRequireConfirmation(String requireConfirmation) {
    this.requireConfirmation = requireConfirmation;
  }

  public String getStp() {
    return stp;
  }

  public void setStp(String stp) {
    this.stp = stp;
  }

  public String getExonerated() {
    return exonerated;
  }

  public void setExonerated(String exonerated) {
    this.exonerated = exonerated;
  }

  public String getManualRetrocession() {
    return manualRetrocession;
  }

  public void setManualRetrocession(String manualRetrocession) {
    this.manualRetrocession = manualRetrocession;
  }

  public String getRouting() {
    return routing;
  }

  public void setRouting(String routing) {
    this.routing = routing;
  }

  public String getRequireRetrocesion() {
    return requireRetrocesion;
  }

  public void setRequireRetrocesion(String requireRetrocesion) {
    this.requireRetrocesion = requireRetrocesion;
  }

  public String getTraders() {
    return traders;
  }

  public void setTraders(String trader) {
    this.traders = trader;
  }

  public String getSales() {
    return sales;
  }

  public void setSales(String sale) {
    this.sales = sale;
  }

  public String getManagers() {
    return managers;
  }

  public void setManagers(String manager) {
    this.managers = manager;
  }

  public String getStatisticLevel0() {
    return statisticLevel0;
  }

  public void setStatisticLevel0(String statisticLevel0) {
    this.statisticLevel0 = statisticLevel0;
  }

  public String getStatisticLevel1() {
    return statisticLevel1;
  }

  public void setStatisticLevel1(String statisticLevel1) {
    this.statisticLevel1 = statisticLevel1;
  }

  public String getStatisticLevel2() {
    return statisticLevel2;
  }

  public void setStatisticLevel2(String statisticLevel2) {
    this.statisticLevel2 = statisticLevel2;
  }

  public String getStatisticLevel3() {
    return statisticLevel3;
  }

  public void setStatisticLevel3(String statisticLevel3) {
    this.statisticLevel3 = statisticLevel3;
  }

  public String getStatisticLevel4() {
    return statisticLevel4;
  }

  public void setStatisticLevel4(String statisticLevel4) {
    this.statisticLevel4 = statisticLevel4;
  }

  public String getTradersLM() {
    return tradersLM;
  }

  public void setTradersLM(String traderLM) {
    this.tradersLM = traderLM;
  }

  public String getSalesLM() {
    return salesLM;
  }

  public void setSalesLM(String saleLM) {
    this.salesLM = saleLM;
  }

  public String getManagersLM() {
    return managersLM;
  }

  public void setManagersLM(String managerLM) {
    this.managersLM = managerLM;
  }

  public String getStatisticLevel0LM() {
    return statisticLevel0LM;
  }

  public void setStatisticLevel0LM(String statisticLevel0LM) {
    this.statisticLevel0LM = statisticLevel0LM;
  }

  public String getStatisticLevel1LM() {
    return statisticLevel1LM;
  }

  public void setStatisticLevel1LM(String statisticLevel1LM) {
    this.statisticLevel1LM = statisticLevel1LM;
  }

  public String getStatisticLevel2LM() {
    return statisticLevel2LM;
  }

  public void setStatisticLevel2LM(String statisticLevel2LM) {
    this.statisticLevel2LM = statisticLevel2LM;
  }

  public String getStatisticLevel3LM() {
    return statisticLevel3LM;
  }

  public void setStatisticLevel3LM(String statisticLevel3LM) {
    this.statisticLevel3LM = statisticLevel3LM;
  }

  public String getStatisticLevel4LM() {
    return statisticLevel4LM;
  }

  public void setStatisticLevel4LM(String statisticLevel4LM) {
    this.statisticLevel4LM = statisticLevel4LM;
  }

  public String getAccountingSystem() {
    return accountingSystem;
  }

  public void setAccountingSystem(String accountingSystem) {
    this.accountingSystem = accountingSystem;
  }

  public String getAccountingSystemLM() {
    return accountingSystemLM;
  }

  public void setAccountingSystemLM(String accountingSystemLM) {
    this.accountingSystemLM = accountingSystemLM;
  }

  public String getSettlementTypeCLG() {
    return settlementTypeCLG;
  }

  public void setSettlementTypeCLG(String settlementTypeCLG) {
    this.settlementTypeCLG = settlementTypeCLG;
  }

  public String getClearerCLG() {
    return clearerCLG;
  }

  public void setClearerCLG(String clearerCLG) {
    this.clearerCLG = clearerCLG;
  }

  public String getAccountAtClearerCLG() {
    return accountAtClearerCLG;
  }

  public void setAccountAtClearerCLG(String accountAtClearerCLG) {
    this.accountAtClearerCLG = accountAtClearerCLG;
  }

  public String getClearingModeCLG() {
    return clearingModeCLG;
  }

  public void setClearingModeCLG(String clearingModeCLG) {
    this.clearingModeCLG = clearingModeCLG;
  }

  public String getCustodianCLG() {
    return custodianCLG;
  }

  public void setCustodianCLG(String custodianCLG) {
    this.custodianCLG = custodianCLG;
  }

  public String[] getSettlementTypeCLM() {
    return settlementTypeCLM == null ? null : settlementTypeCLM.clone();
  }

  public void setSettlementTypeCLM(String[] settlementTypeCLM) {
    this.settlementTypeCLM = settlementTypeCLM;
  }

  public String[] getClearerCLM() {
    return clearerCLM;
  }

  public void setClearerCLM(String[] clearerCLM) {
    this.clearerCLM = clearerCLM;
  }

  public String[] getAccountAtClearerCLM() {
    return accountAtClearerCLM;
  }

  public void setAccountAtClearerCLM(String[] accountAtClearerCLM) {
    this.accountAtClearerCLM = accountAtClearerCLM;
  }

  public String[] getCustodianCLM() {
    return custodianCLM == null ? null : custodianCLM.clone();
  }

  public void setCustodianCLM(String[] custodianCLM) {
    this.custodianCLM = custodianCLM;
  }

  public String[] getMarketCLM() {
    return marketCLM;
  }

  public void setMarketCLM(String[] marketCLM) {
    this.marketCLM = marketCLM;
  }

  public String getSettlementTypeCUG() {
    return settlementTypeCUG;
  }

  public void setSettlementTypeCUG(String settlementTypeCUG) {
    this.settlementTypeCUG = settlementTypeCUG;
  }

  public String getCustodianCUG() {
    return custodianCUG;
  }

  public void setCustodianCUG(String custodianCUG) {
    this.custodianCUG = custodianCUG;
  }

  public String[] getSettlementTypeCUM() {
    return settlementTypeCUM == null ? null : settlementTypeCUM.clone();
  }

  public void setSettlementTypeCUM(String[] settlementTypeCUM) {
    this.settlementTypeCUM = settlementTypeCUM;
  }

  public String[] getCustodianCUM() {
    return custodianCUM;
  }

  public void setCustodianCUM(String[] custodianCUM) {
    this.custodianCUM = custodianCUM;
  }

  public String[] getMarketCUM() {
    return marketCUM == null ? null : marketCUM.clone();
  }

  public void setMarketCUM(String[] marketCUM) {
    this.marketCUM = marketCUM;
  }

  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public String getOp() {
    return op;
  }

  public void setOp(String op) {
    this.op = op;
  }

  public String getForOwnAccount() {
    return forOwnAccount;
  }

  public void setForOwnAccount(String forOwnAccount) {
    this.forOwnAccount = forOwnAccount;
  }

  public String getStatisticGroup() {
    return statisticGroup;
  }

  public void setStatisticGroup(String statisticGroup) {
    this.statisticGroup = statisticGroup;
  }

  public String getStatisticGroupLM() {
    return statisticGroupLM;
  }

  public void setStatisticGroupLM(String statisticGroupLM) {
    this.statisticGroupLM = statisticGroupLM;
  }

  public void complete() throws Exception {

    if ("N".equals(mode)) {
      description = QueryDAO.getBroclientDescription(broClient);
      if (!QueryDAO.isNewAlias(alias)) {
        throw new Exception("Ya existe este alias dado de alta. Tiene que hacer una modificación");
      }

      if (QueryDAO.existsNationalAccount(alias)) {
        throw new Exception(
            "Existe una cuenta en nacional con el mismo alias. Incrementar manualmente en 1 el aliasId, y revisar los valores del AccountingSystem y AccountingSystemLM, que por defecto tienen el mismo valor que el alias");
      }

      if (QueryDAO.aliasTable.containsKey(new Integer(alias))) {
        throw new Exception(
            "Se está procesando un alta con el mismo número de alias. Tiene que volver a elegir el número de alias");

      }
      else {
        QueryDAO.aliasTable.put(new Integer(alias), new Integer(alias));

      }

      QueryDAO.updateStatisticLevels(alias, statisticLevel0LM, statisticLevel1LM, statisticLevel2LM, statisticLevel3LM,
          statisticLevel4LM, tradersLM, salesLM, managersLM);

    }
    else if ("U".equals(mode)) {
      // QueryDAO.updateStatisticLevels(alias);
      QueryDAO.updateStatisticLevels(alias, statisticLevel0LM, statisticLevel1LM, statisticLevel2LM, statisticLevel3LM,
          statisticLevel4LM, tradersLM, salesLM, managersLM);

    }
    else if ("D".equals(mode)) {

      HashMap<String, String> subaccounts = QueryDAO.getSubaccountList(alias);

      if (!(subaccounts == null) && !subaccounts.isEmpty())
        throw new Exception(
            "No se puede dar de baja el alias, ya que tiene asociado subcuentas. Desvincularlas antes de dar de baja el alias");
    }

    broClient = Generator.escapeXml(broClient);
    description = Generator.escapeXml(description);

  }

}
