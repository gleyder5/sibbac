package com.isban.sibbac.velocity;

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.velocity.utilities.UtilsConversiones;

public class Generator {

  private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Generator.class.getName());

  private Properties configProperties;

  /**
   * @param args
   */

  protected Map<String, String[]> parametros;
  private VelocityEngine engine;
  protected String template;
  protected Object object;
  protected String objectName;

  public Generator(Properties configProperties, Map<String, String[]> parametrosIn) {
    parametros = parametrosIn;
    this.configProperties = configProperties;
    engine = new VelocityEngine();
  }

  protected void initVelocity() throws Exception {
    URL urlVelocityProperties = null;
    if (configProperties != null && configProperties.get("tactico.megara.config.folder") != null
        && !((String) configProperties.get("tactico.megara.config.folder")).trim().isEmpty()) {
      Path path = Paths.get(String.format("%s/%s", configProperties.get("tactico.megara.config.folder"),
          "tacticoMegaraVelocity.properties"));
      if (Files.exists(path)) {
        URL urlVelocityPropertiesAux = path.toUri().toURL();
        if (urlVelocityPropertiesAux.getContent() != null) {
          urlVelocityProperties = urlVelocityPropertiesAux;
        }
      }

    }

    if (urlVelocityProperties == null) {
      urlVelocityProperties = this.getClass().getClassLoader().getResource("tacticoMegaraVelocity.properties");
    }

    Properties p = new Properties();
    try (InputStream is = (InputStream) urlVelocityProperties.getContent();) {
      p.load(is);
    }

    // Properties props = new Properties();
    // // THIS PATH CAN BE HARDCODED BUT IDEALLY YOUD GET IT FROM A PROPERTIES
    // FILE
    // String path = "/absolute/path/to/templates/dir/on/your/machine";
    // props.put("file.resource.loader.path", path);
    // props.setProperty("runtime.log.logsystem.class",
    // "org.apache.velocity.runtime.log.NullLogSystem");
    // ve.init(props);

    engine.init(p);

  }

  protected void generateHTML() throws Exception {
    String htmlDocumentFormatted = null;

    Template irsTemplate = null;
    try {
      irsTemplate = engine.getTemplate(template);
    }
    catch (Exception e) {
      irsTemplate = engine.getTemplate(String.format("/templates/%s", template));
    }

    // Files.deleteIfExists(fileTemplate);

    VelocityContext context = new VelocityContext();

    context.put(objectName, object);

    StringWriter sw = new StringWriter();

    irsTemplate.merge(context, sw);
    htmlDocumentFormatted = sw.toString();
    String sequence = QueryDAO.getNextValueFromNewFileSequence();
    String fileNameOut = String.format("M%s", sequence);

    final File ficheroOut = new File((String) configProperties.get("tactico.megara.xml.out.folder"), fileNameOut);
    log.debug("*********************");
    log.debug("*********************");
    log.debug("Generada plantilla entity " + fileNameOut);
    log.debug(htmlDocumentFormatted);
    log.debug("*********************");
    log.debug("*********************");

    UtilsConversiones.copyContenido(htmlDocumentFormatted, ficheroOut, false);

  }

  protected Object loadFields() throws Exception {

    String ClassName = objectName.substring(0, 1).toUpperCase() + objectName.substring(1);

    Class<?> clase = Class.forName("com.isban.sibbac.velocity.data." + ClassName);
    Object objecto = clase.newInstance();

    Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry<String, String[]> e = it.next();
      // log.debug("Clave: " + e.getKey());
      String fieldNameBeforeConversion = e.getKey();
      String fieldValueBeforeConversion = e.getValue()[0];

      String fieldName = java.net.URLDecoder.decode(fieldNameBeforeConversion, "UTF-8");
      String fieldValue = java.net.URLDecoder.decode(fieldValueBeforeConversion, "UTF-8");
      String escapedFieldValue = escapeXml(fieldValue);

      log.debug(escapedFieldValue);

      Field campo = clase.getField(fieldName);

      if (!campo.getType().isArray()) {
        campo.set(objecto, escapedFieldValue);
      }
      else {
        campo.set(objecto, e.getValue());

      }

    } // wh

    Method complete = clase.getMethod("complete", null);
    complete.invoke(objecto, null);

    // log.debug(entity.residence);
    return objecto;

  }

  public static String escapeXml(String s) {
    String newValue = s;
    if ((s != null)
        && !(s.contains("&amp;") || s.contains("&gt;") || s.contains("&lt;") || s.contains("&quot;") || s
            .contains("&apos;")))
      newValue = s.replaceAll("&", "&amp;").replaceAll(">", "&gt;").replaceAll("<", "&lt;").replaceAll("\"", "&quot;")
          .replaceAll("'", "&apos;");
    return newValue;

  }

  public static String descapeXml(String s) {
    String newValue = s;
    if (s != null)
      newValue = s.replaceAll("&amp;", "&").replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&quot;", "\"")
          .replaceAll("&apos;", "'");
    return newValue;

  }

}
