package com.isban.sibbac.velocity.data;


public class AliasSubAccount extends GeneralData {
	
	public String alias = "";
	public String subAccount ="";
	public String subAccount2="";	

	public String op="";
	public String mode = "";
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getSubAccount() {
		return subAccount;
	}
	public void setSubAccount(String subAccount) {
		this.subAccount = subAccount;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getSubAccount2() {
		return subAccount2;
	}
	public void setSubAccount2(String subAccount2) {
		this.subAccount2 = subAccount2;
	}
	
	public void complete() throws Exception
	{
		if (subAccount == null || "".equals(subAccount))
			subAccount=subAccount2;
		
		/*
		
		if ("N".equals(mode))
		{			
			
			
			
			final String otherAlertCode  = QueryDAO.getOtherAlertCodeAlta(alertCode,alias);
			if (!"".equals(otherAlertCode) && !identifier.equals(otherAlertCode))		
				throw new Exception("La subcuenta " + otherAlertCode + " de este Alias tiene el mismo alertCode. Elegir otro o dar de baja la otra subcuenta");
		}
		
		*/
	}

}
