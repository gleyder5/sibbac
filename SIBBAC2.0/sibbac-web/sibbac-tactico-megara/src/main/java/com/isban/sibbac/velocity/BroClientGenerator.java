package com.isban.sibbac.velocity;

import java.util.Map;
import java.util.Properties;

public class BroClientGenerator extends Generator {

  /**
   * @param args
   */

  public BroClientGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    template = "PlantillaBroClients.xml";
    objectName = "broClient";
    object = loadFields();
    initVelocity();
    generateHTML();

    return true;
  }

}
