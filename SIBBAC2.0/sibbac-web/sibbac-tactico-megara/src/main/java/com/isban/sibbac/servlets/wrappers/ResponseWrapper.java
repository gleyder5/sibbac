package com.isban.sibbac.servlets.wrappers;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class ResponseWrapper extends HttpServletResponseWrapper {
	private String operation;
	private String data;
	private boolean compressed;
	private HttpServletResponse response;

	public ResponseWrapper(HttpServletResponse response) {
		super(response);
		this.response = response;
	}
	
	public void setMode(boolean compressed){
		this.compressed = compressed;
	}
	
	
	public void writeData(String data){
		this.data = data;
		GZIPOutputStream gos = null;
		ServletOutputStream sos = null;
		try {
			sos = super.getOutputStream();
			gos = new GZIPOutputStream(sos);
			if(compressed){
				response.setHeader("Content-Encoding","gzip");
				gos.write(data.getBytes(Charset.forName("ISO-8859-1")));
			}else{
				sos.write(data.getBytes(Charset.forName("ISO-8859-1")));
			}
			gos.close();
			sos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String toString(){
		
		return (data == null) ? "" : (data.length()>150) ? data.substring(0, 149) + "..." : data;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	
}
