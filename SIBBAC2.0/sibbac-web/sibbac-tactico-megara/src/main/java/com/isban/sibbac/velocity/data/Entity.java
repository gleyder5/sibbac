package com.isban.sibbac.velocity.data;

import com.isban.sibbac.db.dao.QueryDAO;

public class Entity extends GeneralData {

	/**
	 * @param args
	 */
	
	public String getMode() {
		return mode;
	}


	public void setMode(String mode) {
		this.mode = mode;
	}


	public String getIdentifier() {
		return identifier;
	}


	public void setIdentifier(String identifier) {
		this.identifier = identifier.trim();
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	public String getResidence() {
		return residence;
	}


	public void setResidence(String residence) {
		this.residence = residence;
	}


	public String getNudnicif() {
		return nudnicif;
	}


	public void setNudnicif(String nudnicif) {
		this.nudnicif = nudnicif;
	}


	public String getBic() {
		return bic;
	}


	public void setBic(String bic) {
		this.bic = bic;
	}


	public String mode;
	public String identifier;	
	public String name;
	public String nationality;
	public String residence;
	public String nudnicif = "";	
	public String bic = "";
	public String type;	
	public String city;
	public String street;
	public String streetNumber;
	public String zipCode;
	public String amailaddress;
	public String op;
	public String dwhCode="";
	public String kgrCode="";
	public String entityType="";
	public String getEntityType() {
		return entityType;
	}


	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}


	public String getDwhCode() {
		return dwhCode;
	}


	public void setDwhCode(String dwhCode) {
		this.dwhCode = dwhCode;
	}


	public String getKgrCode() {
		return kgrCode;
	}


	public void setKgrCode(String kgrCode) {
		this.kgrCode = kgrCode;
	}


	public String getOp() {
		return op;
	}


	public void setOp(String op) {
		this.op = op;
	}


	public String getAmailaddress() {
		return amailaddress;
	}


	public void setAmailaddress(String amailaddress) {
		this.amailaddress = amailaddress;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public String location;
	public String province;
	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getStreet() {
		return street;
	}


	public void setStreet(String street) {
		this.street = street;
	}


	public String getStreetNumber() {
		return streetNumber;
	}


	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}


	public String getZipCode() {
		return zipCode;
	}


	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}


	

	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;		
	}


	public Entity(String mode, String identifier, String name, String nationality, String residence, String nudnicif, String bic, String type, String city, String street, String streetNumber, String zipCode, String province)
	{
	this.mode =mode;
	this.identifier = identifier;	
	this.name = name;
	this.nationality = nationality;
	this.residence = residence;
	if (nudnicif != null)
		this.nudnicif = nudnicif;
	if (bic != null)
		this.bic = bic.trim();
	this.type = type;
	this.city=city;
	this.street=street;
	this.streetNumber=streetNumber;
	this.zipCode=zipCode;
	this.amailaddress = street.trim() + " " + streetNumber.trim() +  " " + city.trim() + " " + zipCode.trim() + " " + province.trim() + " " + residence.trim();			
	this.location = (zipCode!=null && zipCode.length()>= 2) ? zipCode.substring(0,2) +"|" + residence.trim(): "";		
	
		
	}
	
	public void complete() throws Exception
	{
		
		if (bic != null)
			bic = bic.trim();
		
		if (street!= null && streetNumber != null && city != null && zipCode != null  )
		{
			this.amailaddress = street.trim() + " " + streetNumber.trim() +  " " + city.trim() + " " + zipCode.trim() + " " + province.trim() + " " + residence.trim();
			this.location = (zipCode!=null && zipCode.length()>= 2) ? zipCode.substring(0,2) +"|" + residence.trim(): "";
		}
		
		else {
			this.amailaddress="";
			this.location="";
		}		
		
		
		//if (nudnicif.length() > 10)
			//nudnicif = nudnicif.substring(0,10);
			//throw new Exception("DNI/CIF superior a 10 posiciones. Escriba una con esa limitaci�n");
		
		if ("N".equals(mode))
		{
			int limit = (identifier.indexOf("&amp;")>-1) ? 24 : 20;
			if ( identifier.length() > limit) 
				//identifier = identifier.substring(0,20);
				throw new Exception("Identificador superior a 20 posiciones. Escriba una con esa limitación");
			
			final String entity =QueryDAO.getEntity(identifier);
			
			if (!"".equals(entity))
			{
				final String[] datos = entity.split("---");
				final String modo = datos[1];
				
				if ("D".equals(modo))				
					throw new Exception("Ya existe este entity dado de baja");
				else 
					throw new Exception("Ya existe este entity dado de alta");
			}
			
			
		}
		
		if ("U".equals(mode))
		{
			if (kgrCode!= null && !"".equals(kgrCode.trim()))
				QueryDAO.updateKgrCode(identifier,kgrCode);
		}
			
		
	}


	public Entity() {
		// TODO Auto-generated constructor stub
	}


}
