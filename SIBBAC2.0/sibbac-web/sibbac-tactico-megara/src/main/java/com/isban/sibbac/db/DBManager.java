package com.isban.sibbac.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import com.isban.sibbac.utils.Utils;

public class DBManager {

  private static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(DBManager.class);

  private static DataSource dsSibbac = null;

  static void doDs() {
    try {
      final InitialContext initialContext = new InitialContext();

      if (dsSibbac == null) {
        String jndi = "jdbc/SIBBAC";
        try {
          Properties config = Utils.getConfig();
          if (config.get("tactico.megara.jdbc.datasource") != null
              && !((String) config.get("tactico.megara.jdbc.datasource")).trim().isEmpty()) {
            jndi = ((String) config.get("tactico.megara.jdbc.datasource")).trim();
          }
        }
        catch (Exception e) {
          log.warn(e.getMessage(), e);
        }

        try {
          dsSibbac = (DataSource) initialContext.lookup(String.format("java:comp/env/%s", jndi));
        }
        catch (final Exception exx) {
          log.warn("Error accediendo de la forma: dsEFS = (DataSource) initialContext.lookup(\"java:comp/env/jdbc/SIBBAC\"); ");
          log.warn(exx.getMessage(), exx);
        }
      }

    }
    catch (final Exception ex) {
      log.warn(ex.getMessage(), ex);
    }
  }

  public static Connection getConnection() throws SQLException {
    Connection conn = null;

    if (dsSibbac == null)
      doDs();

    conn = dsSibbac.getConnection();

    if (!conn.getAutoCommit())
      conn.setAutoCommit(true);

    return conn;
  }
}
