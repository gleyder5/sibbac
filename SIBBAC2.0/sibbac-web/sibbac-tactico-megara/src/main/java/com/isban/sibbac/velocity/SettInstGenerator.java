package com.isban.sibbac.velocity;

import java.util.Map;
import java.util.Properties;

public class SettInstGenerator extends Generator {

  /**
   * @param args
   */

  public SettInstGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    template = "PlantillaSettlementInstructions.xml";
    objectName = "settInst";
    object = loadFields();
    // SettInst settinst = (SettInst)object;

    initVelocity();
    generateHTML();

    return true;
  }

}
