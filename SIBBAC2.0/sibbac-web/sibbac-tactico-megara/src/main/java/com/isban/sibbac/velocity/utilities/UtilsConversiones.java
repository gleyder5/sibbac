package com.isban.sibbac.velocity.utilities;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class UtilsConversiones {

  private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(UtilsConversiones.class);

  private static final int BLOQUE_BYTES_LECTURA = 1024;

  public static void generateWarningSibbac(String message, String warningPath, String warningFileName) throws Exception {
    if (message != null) {

      File tempFile = null;

      final SimpleDateFormat sd1 = new SimpleDateFormat("yyyyMMddHHmmssSSS");

      final Date d = new Date();
      final String extension = sd1.format(d);

      final String warningFileTmp = warningFileName + extension + ".tmp";
      final String warningFileFinally = warningFileName + extension + ".txt";

      tempFile = new File(warningPath, warningFileTmp);
      final File finallyFile = new File(warningPath, warningFileFinally);

      try (FileOutputStream fo = new FileOutputStream(tempFile); PrintStream ps = new PrintStream(fo);) {
        {
          /*
           * final String osName = System.getProperty("os.name").toLowerCase();
           * 
           * if (osName.indexOf("windows") == -1) message = new
           * String(message.getBytes(), "Cp1047");
           */

          ps.println(message);
          ps.flush();
        }
      }
      catch (Exception e) {
        log.warn(e.getMessage(), e);
      }

      if (tempFile.length() > 0) {

        // final String mandato = "CHGAUT OBJ('" +
        // tempFile.getAbsolutePath() +
        // "') USER(*PUBLIC) DTAAUT(*RWX) OBJAUT(*OBJMGT *OBJEXIST *OBJALTER *OBJREF)";
        // executeCommand(mandato);
        tempFile.renameTo(finallyFile);
      }
      else {
        try {
          tempFile.delete();
        }
        catch (Exception ex) {
          log.warn(ex.getMessage(), ex);
        }
      }
    }
  }

  public static void tratarFicheroErrores(String archivoName, String pathControlFile, String pathWarnings,
      String pathFilesIn) {
    // 1�. Comprueba que el fichero CONTROL.TXT existe si no espera 3
    // segundos, hasta un minuto
    // que lanzar�a error

    File ficheroControl = new File(pathControlFile, "CONTROL.TXT");

    int contador = 0;
    for (contador = 0; contador < 21; contador++) {
      if (ficheroControl.exists()) {
        break;
      }

      try {
        Thread.sleep(3000);
      }
      catch (InterruptedException e) {
        log.warn(e.getMessage(), e);
      }
    }

    if (contador == 21) {
      final String message1 = " No est� en estado correcto el fichero de control CONTROL.TXT para el env�o de titulares en el procesamiento del fichero de errores";
      String warningFileName1 = "AlarmaSibbac";
      String warningFileName2 = "AlarmaMegara";
      try {
        generateWarningSibbac(message1, pathWarnings, warningFileName1);
        generateWarningSibbac(message1, pathWarnings, warningFileName2);
        return;
      }
      catch (Exception e) {
        log.warn(e.getMessage(), e);
      }

    }

    // Coge el fichero CONTROL.TXT y lo cambiaria a CONTROL.AS400

    File ficheroControlAS400 = new File(pathControlFile, "CONTROL.AS400");
    moverFicheros(ficheroControl, ficheroControlAS400);

    Properties paramEjecucion = new Properties();
    InputStream is;
    try {
      is = new FileInputStream(ficheroControlAS400);

      // Va a buscar la propiedad correspondiente a la bolsa

      paramEjecucion.load(is);

      // Si no existe generar alarrma

      // Si no est� en estado RUNNING genera alarma

      // Si existe le cambia el estado a END OK

      String bolsa = archivoName.substring(6);
      String propiedad = getPropiedad(bolsa);
      String valor = paramEjecucion.getProperty(propiedad);
      boolean convertir = false;

      if (valor == null || !valor.trim().startsWith("RUNNING")) {
        final String message1 = " No existe o no est� en estado RUNNING la propiedad " + propiedad
            + " en el fichero CONTROL.AS400 en el procesamiento del fichero de errores";
        String warningFileName1 = "AlarmaSibbac";

        try {
          generateWarningSibbac(message1, pathWarnings, warningFileName1);

        }
        catch (Exception e) {
          log.warn(e.getMessage(), e);
        }

        is.close();
      }

      else {
        convertir = true;
        String valorPropiedad = valor.replaceAll("RUNNING", "END OK");
        valor = obtenerValor(archivoName, pathFilesIn);

        if (!(valor == null)) {
          valorPropiedad = "END KO " + valor;

        }
        paramEjecucion.setProperty(propiedad, String.valueOf(valorPropiedad));

        try {
          final FileOutputStream fos = new FileOutputStream(ficheroControlAS400);
          paramEjecucion.store(fos, null);
          fos.flush();
          fos.close();
          is.close();
        }
        catch (IOException e) {
          log.warn(e.getMessage(), e);
        }
      }

      // Cambia el nombre del fichero a CONTROL.TXT

      moverFicheros(ficheroControlAS400, ficheroControl, convertir);

    }
    catch (FileNotFoundException e) {

      log.warn(e.getMessage(), e);
    }
    catch (IOException e) {
      log.warn(e.getMessage(), e);
    }

  }

  public static void tratarFicheroErroresGenerico(String propiedad, String value, String pathControlFile,
      String pathWarnings) {
    // 1�. Comprueba que el fichero CONTROL.TXT existe si no espera 3
    // segundos, hasta un minuto
    // que lanzar�a error

    if (propiedad.startsWith("PET_TRX") || propiedad.startsWith("PET_TRI") || propiedad.startsWith("PET_TRT"))
      return;

    File ficheroControl = new File(pathControlFile, "CONTROL.TXT");

    int contador = 0;
    for (contador = 0; contador < 21; contador++) {
      if (ficheroControl.exists()) {
        break;
      }

      try {
        Thread.sleep(3000);
      }
      catch (InterruptedException e) {
        log.warn(e.getMessage(), e);
      }
    }

    if (contador == 21) {
      final String message1 = " No est� en estado correcto el fichero de control CONTROL.TXT para el env�o de titulares en el procesamiento del fichero de errores";
      String warningFileName1 = "AlarmaSibbac";

      try {
        generateWarningSibbac(message1, pathWarnings, warningFileName1);

        return;
      }
      catch (Exception e) {
        log.warn(e.getMessage(), e);
      }

    }

    // Coge el fichero CONTROL.TXT y lo cambiaria a CONTROL.AS400

    File ficheroControlAS400 = new File(pathControlFile, "CONTROL.AS400");
    moverFicheros(ficheroControl, ficheroControlAS400);

    Properties paramEjecucion = new Properties();
    InputStream is;
    try {
      is = new FileInputStream(ficheroControlAS400);

      // Va a buscar la propiedad correspondiente a la bolsa

      paramEjecucion.load(is);

      // Si no existe generar alarrma

      // Si no est� en estado RUNNING genera alarma

      // Si existe le cambia el estado a END OK

      String valor = paramEjecucion.getProperty(propiedad);
      boolean convertir = false;

      if (valor == null || !valor.trim().startsWith("RUNNING")) {
        final String message1 = " No existe o no est� en estado RUNNING la propiedad " + propiedad
            + " en el fichero CONTROL.AS400 en el procesamiento del fichero de errores";
        String warningFileName1 = "AlarmaSibbac";

        try {
          generateWarningSibbac(message1, pathWarnings, warningFileName1);

        }
        catch (Exception e) {
          log.warn(e.getMessage(), e);
        }

        is.close();
      }

      else {
        convertir = true;
        valor = value;
        String valorPropiedad = "END OK";
        if (!(valor == null)) {
          valorPropiedad = "END KO " + valor;

        }
        paramEjecucion.setProperty(propiedad, String.valueOf(valorPropiedad));

        try {
          final FileOutputStream fos = new FileOutputStream(ficheroControlAS400);
          paramEjecucion.store(fos, null);
          fos.flush();
          fos.close();
          is.close();
        }
        catch (IOException e) {
          log.warn(e.getMessage(), e);
        }
      }

      // Cambia el nombre del fichero a CONTROL.TXT

      moverFicheros(ficheroControlAS400, ficheroControl, convertir);

    }
    catch (FileNotFoundException e) {
      log.warn(e.getMessage(), e);
    }
    catch (IOException e) {
      log.warn(e.getMessage(), e);
    }

  }

  public static void tratarFicheroEnvios(String archivoName, String pathControlFile, String pathWarnings,
      int numeroLineas, String errorValue) {
    // 1�. Comprueba que el fichero CONTROL.TXT existe si no espera 3
    // segundos, hasta un minuto
    // que lanzar�a error

    File ficheroControl = new File(pathControlFile, "CONTROL.TXT");

    int contador = 0;
    for (contador = 0; contador < 21; contador++) {
      if (ficheroControl.exists()) {
        break;
      }

      try {
        Thread.sleep(3000);
      }
      catch (InterruptedException e) {

        log.warn(e.getMessage(), e);
      }
    }

    if (contador == 21) {
      final String message1 = " No est� en estado correcto el fichero de control CONTROL.TXT para el env�o de titulares en el proceso de env�o";
      String warningFileName1 = "AlarmaSibbac";

      try {
        generateWarningSibbac(message1, pathWarnings, warningFileName1);

        return;
      }
      catch (Exception e) {

        log.warn(e.getMessage(), e);
      }

    }

    // Coge el fichero CONTROL.TXT y lo cambiaria a CONTROL.AS400

    File ficheroControlAS400 = new File(pathControlFile, "CONTROL.AS400");
    moverFicheros(ficheroControl, ficheroControlAS400);

    Properties paramEjecucion = new Properties();
    InputStream is;
    try {
      is = new FileInputStream(ficheroControlAS400);

      // Va a buscar la propiedad correspondiente a la bolsa

      paramEjecucion.load(is);

      // Si no existe generar alarrma

      // Si no est� en estado RUNNING genera alarma

      // Si existe le cambia el estado a END OK

      String bolsa = archivoName.substring(6);
      String propiedad = getPropiedad(bolsa);
      String valor = paramEjecucion.getProperty(propiedad);

      if (valor == null || !valor.trim().equals("RUNNING")) {
        final String message1 = " No existe o no est� en estado RUNNING la propiedad " + propiedad
            + " en el fichero CONTROL.AS400 en el proceso de env�o ";
        String warningFileName1 = "AlarmaSibbac";

        try {
          generateWarningSibbac(message1, pathWarnings, warningFileName1);

        }
        catch (Exception e) {

          log.warn(e.getMessage(), e);
        }

        is.close();
      }

      else if (errorValue != null) {
        String valorPropiedad = "END KO " + errorValue;

        paramEjecucion.setProperty(propiedad, String.valueOf(valorPropiedad));

        try {
          final FileOutputStream fos = new FileOutputStream(ficheroControlAS400);
          paramEjecucion.store(fos, null);
          fos.flush();
          fos.close();
          is.close();
        }
        catch (IOException e) {

          log.warn(e.getMessage(), e);
        }

        final File file = new File(pathControlFile, propiedad);
        if (file != null && file.exists())
          file.delete();
      }

      else {
        String valorPropiedad = valor + " " + new Integer(numeroLineas).toString();

        paramEjecucion.setProperty(propiedad, String.valueOf(valorPropiedad));

        try {
          final FileOutputStream fos = new FileOutputStream(ficheroControlAS400);
          paramEjecucion.store(fos, null);
          fos.flush();
          fos.close();
          is.close();
        }
        catch (IOException e) {

          log.warn(e.getMessage(), e);
        }
      }

      // Cambia el nombre del fichero a CONTROL.TXT

      moverFicheros(ficheroControlAS400, ficheroControl, true);

    }
    catch (FileNotFoundException e) {

      log.warn(e.getMessage(), e);
    }
    catch (IOException e) {

      log.warn(e.getMessage(), e);
    }

  }

  private static String obtenerValor(String archivoName, String pathFilesIn) {
    // TODO Auto-generated method stub
    String valor = null;

    final File f = new File(pathFilesIn, archivoName);

    try {
      final BufferedReader br = new BufferedReader(new FileReader(f));

      String line = br.readLine();

      if (line.length() > 84) {
        String message = line.substring(83).trim();
        if (!message.startsWith("ERRORES PROCESADOS CORRECTAMENTE")) {
          valor = message;
        }
      }

      br.close();

    }
    catch (FileNotFoundException e) {

      log.warn(e.getMessage(), e);
    }
    catch (IOException e) {

      log.warn(e.getMessage(), e);
    }

    return valor;
  }

  public static void moverFicheros(File ficheroIn, File ficheroOut) {

    moverFicheros(ficheroIn, ficheroOut, false);

  }

  public static void moverFicheros(File ficheroIn, File ficheroOut, boolean convertir) {

    copyFicheros(ficheroIn, ficheroOut, convertir);
    if (ficheroIn.exists())
      ficheroIn.delete();

  }

  public static void copyFicheros(File ficheroIn, File ficheroOut, boolean convertir) {
    try {
      FileInputStream fis = new FileInputStream(ficheroIn);
      FileOutputStream fos = new FileOutputStream(ficheroOut);

      copyStream(fis, fos, convertir);

    }
    catch (FileNotFoundException e) {

      log.warn(e.getMessage(), e);
    }
    catch (IOException e) {

      log.warn(e.getMessage(), e);
    }

  }

  public static void copyContenido(String contenido, File ficheroOut, boolean convertir) throws IOException {
    try {
      final ByteArrayInputStream bais = new ByteArrayInputStream(contenido.getBytes(StandardCharsets.ISO_8859_1));

      FileOutputStream fos = new FileOutputStream(ficheroOut);

      copyStream(bais, fos, convertir);

    }
    catch (FileNotFoundException e) {
      throw e;
    }
    catch (IOException e) {
      throw e;
    }

  }

  public static String getPropiedad(String bolsa) {
    String propiedad = null;
    if (bolsa.equals("BA"))
      propiedad = "GEN_TIT_BAR";
    else if (bolsa.equals("VA"))
      propiedad = "GEN_TIT_VAL";
    else if (bolsa.equals("MA"))
      propiedad = "GEN_TIT_MAD";
    else if (bolsa.equals("BI"))
      propiedad = "GEN_TIT_BIL";

    return propiedad;
  }

  public static void main(String[] args) {
    String archivoName = "FILS0NBA";
    String pathControlFile = "ficherosControl";
    String pathWarnings = "Alarmas";
    // String pathFilesIn = "AUTAS400/out";
    // tratarFicheroErrores(archivoName, pathControlFile,
    // pathWarnings,pathFilesIn);
    // tratarFicheroEnvios(archivoName, pathControlFile, pathWarnings,5,
    // pathFilesIn, "No existen ficheos");
    tratarFicheroEnvios(archivoName, pathControlFile, pathWarnings, 5, "no exsiten");
  }

  public static void copyStream(final InputStream inputStream, OutputStream baos, boolean convertir) throws IOException {

    int read;
    byte[] buff = new byte[1024];

    while ((read = inputStream.read(buff)) != -1) {

      if (convertir) {
        final String osName = System.getProperty("os.name").toLowerCase();

        if (osName.indexOf("windows") == -1) {

          String newValue = new String(buff);
          newValue = newValue.replaceAll("\r\n", "XXXYYYZZZ");
          int ocurrencias = getOcurrencias(newValue, "\n");
          newValue = newValue.replaceAll("\n", "\r\n");
          newValue = newValue.replaceAll("XXXYYYZZZ", "\r\n");
          buff = newValue.getBytes();
          read = read + ocurrencias;
        }
      }
      baos.write(buff, 0, read);

    }

    baos.close();
    inputStream.close();

  }

  public static String getContenido(final File file, boolean convertir) {
    String contenido = null;
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();

    try {
      final FileInputStream fis = new FileInputStream(file);
      copyStream(fis, baos, convertir);
      if (baos != null)
        contenido = new String(baos.toByteArray());
      fis.close();
    }
    catch (IOException e) {
      log.warn(e.getMessage(), e);
    }

    return contenido;

  }

  private static int getOcurrencias(String sTexto, String sTextoBuscado) {
    int contador = 0;
    while (sTexto.indexOf(sTextoBuscado) > -1) {
      sTexto = sTexto.substring(sTexto.indexOf(sTextoBuscado) + sTextoBuscado.length(), sTexto.length());
      contador++;
    }
    return contador;
  }

  public static String getFirstLine(File peti) {

    String content = null;

    FileReader fr = null;
    BufferedReader br = null;

    try {
      fr = new FileReader(peti);
      br = new BufferedReader(fr);

      content = br.readLine().trim();

    }
    catch (Exception ex) {
      log.warn(ex.getMessage(), ex);
    }
    finally {
      if (fr != null)
        try {
          fr.close();
        }
        catch (IOException e) {

        }

      if (br != null)
        try {
          br.close();
        }
        catch (IOException e) {
          log.warn(e.getMessage(), e);

        }

    }

    return content;
  }

  public static String getPropiedadNombreFichero(String fileName) {
    String propiedad = null;

    final String bolsaIn = fileName.substring(6);
    final String typeIn = fileName.substring(5, 6);
    final String bolsaOut = getBolsaPropiedad(bolsaIn);
    final String typeOut = getTipoPropiedad(typeIn);
    final String tipoFicher = getTipoFichero(typeIn);
    propiedad = tipoFicher + "_" + typeOut + "_" + bolsaOut;

    return propiedad;
  }

  private static String getTipoFichero(String typeIn) {
    String propiedad = null;
    if (typeIn.equals("N"))
      propiedad = "GEN";
    else
      propiedad = "PET";
    return propiedad;
  }

  public static String getBolsaPropiedad(String bolsa) {
    String propiedad = null;
    if (bolsa.equals("BA"))
      propiedad = "BAR";
    else if (bolsa.equals("VA"))
      propiedad = "VAL";
    else if (bolsa.equals("MA"))
      propiedad = "MAD";
    else if (bolsa.equals("BI"))
      propiedad = "BIL";

    return propiedad;
  }

  public static String getTipoPropiedad(String type) {
    String propiedad = null;
    if (type.equals("C"))
      propiedad = "CON";
    else if (type.equals("O"))
      propiedad = "OPA";
    else if (type.equals("S"))
      propiedad = "SAL";
    else if (type.equals("X"))
      propiedad = "TRX";
    else if (type.equals("I"))
      propiedad = "TRI";
    else if (type.equals("T"))
      propiedad = "TRT";

    return propiedad;
  }

  public static String getGeneralProperty(File propertyFile, String property) {
    String value = null;
    Properties paramEjecucion = new Properties();
    InputStream is;
    try {
      is = new FileInputStream(propertyFile);

      paramEjecucion.load(is);

      value = paramEjecucion.getProperty(property);

      is.close();
    }
    catch (Exception ex) {
      log.warn(ex.getMessage(), ex);
    }
    return value;
  }

  public static String getInputStream(final InputStream inputStream) throws IOException {
    final ByteArrayOutputStream baos = new ByteArrayOutputStream();

    int read;
    byte[] buff = new byte[BLOQUE_BYTES_LECTURA];

    while ((read = inputStream.read(buff)) != -1) {
      baos.write(buff, 0, read);

    }

    return new String(baos.toByteArray());
  }

  public static String getGeneralProperty(String propertyContent, String property) {
    String value = null;
    Properties paramEjecucion = new Properties();
    InputStream is;
    try {
      is = new ByteArrayInputStream(propertyContent.getBytes());

      paramEjecucion.load(is);

      value = paramEjecucion.getProperty(property);

      is.close();
    }
    catch (Exception ex) {
      log.warn(ex.getMessage(), ex);
    }
    return value;
  }

  public static String getValueFromUrl(String urlPath, String property) {
    String valor = null;
    URL url;
    try {
      url = new URL(urlPath);
      final InputStream is = (url.openConnection()).getInputStream();
      final String propiedadesFichero = getInputStream(is);
      // final String property = "timerListenerAUDReadBatch";

      valor = getGeneralProperty(propiedadesFichero, property);
    }
    catch (MalformedURLException e) {

      log.warn(e.getMessage(), e);
    }
    catch (IOException e) {

      log.warn(e.getMessage(), e);
    }

    return valor;
  }

  public static void modificarPropiedadConfiguracion(File file, String property, String newValue) {

    try {

      Properties paramEjecucion = new Properties();
      InputStream is;
      is = new FileInputStream(file);
      // Va a buscar la propiedad correspondiente a la bolsa
      paramEjecucion.load(is);
      paramEjecucion.setProperty(property, newValue);

      final FileOutputStream fos = new FileOutputStream(file);
      paramEjecucion.store(fos, null);
      fos.flush();
      fos.close();
      is.close();
    }
    catch (IOException e) {

      log.warn(e.getMessage(), e);
    }
  }

  public static String remove1(String input) {
    // Cadena de caracteres original a sustituir.
    String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜçÇ";
    // Cadena de caracteres ASCII que reemplazarán los originales.
    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUcC";
    String output = input;
    for (int i = 0; i < original.length(); i++) {
      // Reemplazamos los caracteres especiales.
      output = output.replace(original.charAt(i), ascii.charAt(i));
    }// for i
    return output;
  }// remove1

}
