package com.isban.sibbac.servlets.wrappers;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class RequestWrapper extends HttpServletRequestWrapper {

  private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(RequestWrapper.class.getName());

  protected HttpServletRequest reqOrig = null;
  // protected HttpCompressionRequestStream httpCompressionResponseStream =
  // null;
  protected Hashtable parameters = null;

  public RequestWrapper(HttpServletRequest req) {
    super(req);
    this.reqOrig = req;
  }

  public ServletRequest getRequest() {
    return this;
  }

  public void parsePostData(int length, ServletInputStream instream) {
    // método que devolverá una tabla hash (nombre, valor) con los parámetros
    // enviados
    // en un formulario con el método POST
    // a este método se le pasan como parámetros la longitud y el contenido de
    // la petición recibida
    // por el servidor
    String valArray[] = null;
    int inputLen, offset;
    byte[] postedBytes = null;
    boolean dataRemaining = true;
    String postedBody;
    Hashtable ht = new Hashtable();
    Vector paramOrder = new Vector(100);
    StringBuffer sb = new StringBuffer();

    if (length <= 0) {

    }
    // se extraen los datos enviados en la petición
    postedBytes = new byte[length];
    try {
      offset = 0;
      while (dataRemaining) {
        inputLen = instream.read(postedBytes, offset, length - offset);
        if (inputLen <= 0) {
          throw new IOException("read error");

        }
        offset += inputLen;
        if ((length - offset) == 0) {
          dataRemaining = false;
        }
      }
    }
    catch (IOException e) {
      log.warn(e.getMessage(), e);

    }
    postedBody = new String(postedBytes);
    // se separan los datos por parejas
    StringTokenizer st = new StringTokenizer(postedBody, "&");

    String key = null;
    String val = null;

    // se colocan en la tabla los nombres de los parámetros junto a sus
    // correspondientes valores
    // se tiene en cuenta que puede haber una lista de parámetros con el mismo
    // nombre (ej: checkbox)
    while (st.hasMoreTokens()) {
      String pair = (String) st.nextToken();
      int pos = pair.indexOf('=');
      if (pos == -1) {
        throw new IllegalArgumentException();
      }
      try {
        // extrae de cada par el nombre y valor , aplicando la descodificación
        // URL
        key = java.net.URLDecoder.decode(pair.substring(0, pos), "UTF-8");
        val = java.net.URLDecoder.decode(pair.substring(pos + 1, pair.length()), "UTF-8");
      }
      catch (Exception e) {
        throw new IllegalArgumentException(e.getMessage(), e);
      }
      if (ht.containsKey(key)) {
        String oldVals[] = (String[]) ht.get(key);
        valArray = new String[oldVals.length + 1];
        for (int i = 0; i < oldVals.length; i++) {
          valArray[i] = oldVals[i];
        }
        valArray[oldVals.length] = val;
      }
      else {
        valArray = new String[1];
        valArray[0] = val;
      }
      // inserta el par (nombre, valor) en la tabla hash
      ht.put(key, valArray);
      paramOrder.addElement(key);
    }
    this.parameters = ht; // se asigna la tabla de pares (nombre,valor)

  }

  public String getParameter(String name) {
    // se genera un nuevo método para obtener los parámetros del formulario
    // de esta forma se evitan fallos que provocaba el método de
    // HttpServletRequest
    // (no descoficaba bien las tildes y ñ's cuando se enviaban desde navegador
    // Firefox)

    // se obtiene de la tabla hash el valor del campo cuyo valor
    // se ha pasado a la función como parámetro
    String vals[] = (String[]) this.parameters.get(name);
    if (vals == null) {
      return null;
    }
    String vallist = vals[0];
    for (int i = 1; i < vals.length; i++) {
      vallist = vallist + "," + vals[i];
    }
    return vallist;
  }

  public java.lang.String[] getParameterValues(String name) {
    // se genera un nuevo método para obtener los parámetros del formulario
    // de esta forma se evitan fallos que provocaba el método de
    // HttpServletRequest
    // (no descodificaba bien las tildes y ñ's cuando se enviaban desde
    // navegador Firefox)

    // se obtiene de la tabla hash los valores del campo cuyo valor
    // se ha pasado a la función como parámetro
    String vals[] = (String[]) this.parameters.get(name);
    String vacio[] = { " ", " " };
    if (vals == null) {
      return null;
    }

    return vals;
  }

  public Map<String, String[]> getParameterMap() {
    return this.parameters;
  }
}
