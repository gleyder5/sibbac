package com.isban.sibbac.velocity;

import java.util.Map;
import java.util.Properties;

public class EntityGenerator extends Generator {

  /**
   * @param args
   */

  public EntityGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    template = "PlantillaEntities.xml";
    objectName = "entity";
    object = loadFields();
    initVelocity();
    generateHTML();

    return true;
  }

}
