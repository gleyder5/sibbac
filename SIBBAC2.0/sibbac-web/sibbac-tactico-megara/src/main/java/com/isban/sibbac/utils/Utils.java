/**
 * 
 */
package com.isban.sibbac.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * M�todos de utilidad para el proyecto.
 * 
 * @author Alberto Horn�s
 */
public class Utils {

  public static String getCurrentDate() {
    Date date = new Date();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    return formatter.format(date);
  }

  /**
   * Devuelve la lista de paises disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de paises.
   */
  public static Map<String, String> getCountries() {
    HashMap<String, String> countryList = null;

    try {
      countryList = new HashMap<String, String>();

      countryList.put("AD", "ANDORRA");
      countryList.put("AE", "EMIRATOS ARABES UNIDOS");
      countryList.put("AF", "AFGANISTAN");
      countryList.put("AG", "ANTIGUA Y BARBUDA");
      countryList.put("AI", "ANGUILLA");
      countryList.put("AL", "ALBANIA");
      countryList.put("AM", "ARMENIA");
      countryList.put("AN", "ANTILLAS NEERLANDESAS");
      countryList.put("AO", "ANGOLA (Incluye Cabinda)");
      countryList.put("AR", "ARGENTINA");
      countryList.put("AT", "AUSTRIA");
      countryList.put("AU", "AUSTRALIA");
      countryList.put("AW", "ARUBA");
      countryList.put("AZ", "AZERBAIJAN");
      countryList.put("BA", "BOSNIA-HERZEGOVINA");
      countryList.put("BB", "BARBADOS");
      countryList.put("BD", "BANGLADESH");
      countryList.put("BE", "BELGICA");
      countryList.put("BF", "BURKINA-FASO (Alto Volta)");
      countryList.put("BG", "BULGARIA");
      countryList.put("BH", "BAHREIN");
      countryList.put("BI", "BURUNDI");
      countryList.put("BJ", "BENIN");
      countryList.put("BM", "BERMUDAS");
      countryList.put("BN", "BRUNEI");
      countryList.put("BO", "BOLIVIA");
      countryList.put("BR", "BRASIL");
      countryList.put("BS", "BAHAMAS");
      countryList.put("BT", "BHOUTAN");
      countryList.put("BW", "BOTSWANA");
      countryList.put("BY", "BIELORRUSIA");
      countryList.put("BZ", "BELICE");
      countryList.put("CA", "CANADA");
      countryList.put("CD", "REPUBLICA DEMOCRATICA CONGO");
      countryList.put("CF", "REPUBLICA CENTROAFRICANA");
      countryList.put("CG", "CONGO");
      countryList.put("CH", "SUIZA, LIECHTENSTEIN");
      countryList.put("CH", "SUIZA");
      countryList.put("CI", "COSTA DE MARFIL");
      countryList.put("CK", "ISLAS COOK");
      countryList.put("CL", "CHILE");
      countryList.put("CM", "CAMERUN");
      countryList.put("CN", "CHINA");
      countryList.put("CO", "COLOMBIA");
      countryList.put("CR", "COSTA RICA");
      countryList.put("CS", "CHECOSLOVAQUIA");
      countryList.put("CS", "SERBIA Y MONTENEGRO");
      countryList.put("CU", "CUBA");
      countryList.put("CV", "REPUBLICA DE CABO VERDE");
      countryList.put("CY", "CHIPRE");
      countryList.put("CZ", "REPUBLICA CHECA");
      countryList.put("DD", "REPUBLICA DEMOCRATICA ALEMANA");
      countryList.put("DE", "ALEMANIA");
      countryList.put("DJ", "DJIBOUTI");
      countryList.put("DK", "DINAMARCA");
      countryList.put("DM", "DOMINICA");
      countryList.put("DO", "REPUBLICA DOMINICANA");
      countryList.put("DZ", "ARGELIA");
      countryList.put("EC", "ECUADOR (Incluye Islas Galapagos)");
      countryList.put("EE", "ESTONIA");
      countryList.put("EG", "EGIPTO");
      countryList.put("ER", "ERITREA");
      countryList.put("ES", "ESPAÑA");
      countryList.put("ET", "ETIOPIA");
      countryList.put("FI", "FINLANDIA");
      countryList.put("FJ", "FIDJI");
      countryList.put("FK", "ISLAS MALVINAS (FALKLLANDS)");
      countryList.put("FM", "EST MICRONESIA(YAP,KOSRAE,TRUK,POHNPEI");
      countryList.put("FO", "ISLAS FEROE");
      countryList.put("FR", "FRANCIA");
      countryList.put("GA", "GABON");
      countryList.put("GB", "REINO UNIDO");
      countryList.put("GD", "GRANADA");
      countryList.put("GE", "GEORGIA");
      countryList.put("GF", "GUAYANA FRANCESA");
      countryList.put("GG", "I.ANGLO NORMAND(I.CANAL,GUERNESEY-JERSEY");
      countryList.put("GH", "GHANA");
      countryList.put("GI", "GIBRALTAR");
      countryList.put("GL", "GROENLANDIA");
      countryList.put("GM", "GAMBIA");
      countryList.put("GN", "GUINEA");
      countryList.put("GP", "GUADALUPE");
      countryList.put("GQ", "GUINEA ECUATORIAL");
      countryList.put("GR", "GRECIA");
      countryList.put("GT", "GUATEMALA");
      countryList.put("GW", "GUINEA-BISSAU");
      countryList.put("GY", "GUAYANA");
      countryList.put("HK", "HONG-KONG");
      countryList.put("HN", "HONDURAS (Incluye Islas Swan)");
      countryList.put("HR", "CROACIA");
      countryList.put("HT", "HAITI");
      countryList.put("HU", "HUNGRIA");
      countryList.put("ID", "INDONESIA");
      countryList.put("IE", "IRLANDA");
      countryList.put("IL", "ISRAEL");
      countryList.put("IN", "INDIA (Incluye Sikkim)");
      countryList.put("IO", "TERRITORIO BRITANICO DEL OCEANO INDICO");
      countryList.put("IQ", "IRAQ");
      countryList.put("IR", "IRAN");
      countryList.put("IS", "ISLANDIA");
      countryList.put("IT", "ITALIA");
      countryList.put("JM", "JAMAICA");
      countryList.put("JO", "JORDANIA");
      countryList.put("JP", "JAPON");
      countryList.put("KE", "KENYA");
      countryList.put("KG", "KIRGUIZISTAN");
      countryList.put("KH", "KAMPUCHEA (CAMBOYA)");
      countryList.put("KI", "KIRIBATI");
      countryList.put("KM", "CAMORES");
      countryList.put("KN", "SAN CRISTOBAL Y NEVIS");
      countryList.put("KP", "COREA DEL NORTE");
      countryList.put("KR", "COREA DEL SUR");
      countryList.put("KW", "KUWAIT");
      countryList.put("KY", "ISLAS CAIMAN");
      countryList.put("KZ", "KAZAJSTAN");
      countryList.put("LA", "LAOS");
      countryList.put("LB", "LIBANO");
      countryList.put("LC", "SANTA LUCIA");
      countryList.put("LI", "LIETCHENSTIEN");
      countryList.put("LK", "SRI LANKA");
      countryList.put("LR", "LIBERIA");
      countryList.put("LS", "LESOTHO");
      countryList.put("LT", "LITUANIA");
      countryList.put("LU", "LUXEMBURGO");
      countryList.put("LV", "LETONIA");
      countryList.put("LY", "LIBIA");
      countryList.put("MA", "MARRUECOS");
      countryList.put("MC", "MONACO");
      countryList.put("MD", "MOLDAVIA");
      countryList.put("MG", "MADAGASCAR");
      countryList.put("MH", "ISLAS MARSHALL");
      countryList.put("MK", "ANTIGUA REPUBLICA YUGOSLAVA DE MACEDONIA");
      countryList.put("ML", "MALI");
      countryList.put("MM", "MYANMAR (Antigua Birmania)");
      countryList.put("MN", "MONGOLIA");
      countryList.put("MO", "MACAO");
      countryList.put("MP", "ISLAS MARIANAS DEL NORTE");
      countryList.put("MQ", "MARTINICA");
      countryList.put("MR", "MAURITANIA");
      countryList.put("MS", "MONTSERRAT");
      countryList.put("MT", "MALTA");
      countryList.put("MU", "MAURICIO");
      countryList.put("MV", "ISLAS MALDIVAS");
      countryList.put("MW", "MALAWI");
      countryList.put("MX", "MEXICO");
      countryList.put("MY", "MALASIA OCCIDENTAL Y ORIENTAL");
      countryList.put("MZ", "MOZAMBIQUE");
      countryList.put("NA", "NAMIBIA");
      countryList.put("NC", "NUEVA CALEDONIA");
      countryList.put("NE", "NIGER");
      countryList.put("NG", "NIGERIA");
      countryList.put("NI", "NICARAGUA (Incluye Islas Corn)");
      countryList.put("NL", "PAISES BAJOS");
      countryList.put("NO", "NORUEGA (Incluye Jan Mayen y Svalbard)");
      countryList.put("NP", "NEPAL");
      countryList.put("NR", "NAURU");
      countryList.put("NZ", "NUEVA ZELANDA");
      countryList.put("OM", "OMAN");
      countryList.put("PA", "PANAMA (Incluye la antigua zona del canal)");
      countryList.put("PA", "PANAMA");
      countryList.put("PE", "PERU");
      countryList.put("PF", "POLINESIA FRANCESA");
      countryList.put("PG", "PAPUA-NUEVA GUINEA");
      countryList.put("PH", "FILIPINAS");
      countryList.put("PK", "PAKISTAN");
      countryList.put("PL", "POLONIA");
      countryList.put("PM", "SAN PEDRO Y MIQUELON");
      countryList.put("PN", "PITCAIM (Incluye I.Henderson, Duciet y Oeno)");
      countryList.put("PR", "PUERTO RICO");
      countryList.put("PS", "GAZA Y JERICO");
      countryList.put("PT", "PORTUGAL (Incluye Azores y Madeira)");
      countryList.put("PT", "PORTUGAL");
      countryList.put("PW", "PALAU");
      countryList.put("PY", "PARAGUAY");
      countryList.put("PZ", "ZONA DEL CANAL DE PANAMA");
      countryList.put("QA", "QATAR");
      countryList.put("QU", "OTROS PAISES/TERRITORIOS NO RELACIONADOS");
      countryList.put("RE", "REUNION");
      countryList.put("RO", "RUMANIA");
      countryList.put("RU", "RUSIA");
      countryList.put("RW", "RWANDA");
      countryList.put("SA", "ARABIA SAUDITA");
      countryList.put("SB", "ISLAS SALOMON");
      countryList.put("SC", "SEYCHELLES Y DEPENDENCIAS");
      countryList.put("SD", "SUDAN");
      countryList.put("SE", "SUECIA");
      countryList.put("SG", "SINGAPUR");
      countryList.put("SH", "SANTA HELENA Y DEPENDENCIAS");
      countryList.put("SI", "ESLOVENIA");
      countryList.put("SJ", "ARCHIPIELAGO SVALBARD");
      countryList.put("SK", "REPUBLICA ESLOVACA");
      countryList.put("SL", "SIERRA LEONA");
      countryList.put("SM", "SAN MARINO");
      countryList.put("SN", "SENEGAL");
      countryList.put("SO", "SOMALIA");
      countryList.put("SR", "SURINAM");
      countryList.put("ST", "SANTO TOME Y PRINCIPE");
      countryList.put("SU", "UNION SOVIETICA");
      countryList.put("SV", "EL SALVADOR");
      countryList.put("SY", "SIRIA");
      countryList.put("SZ", "SWAZILAND");
      countryList.put("TC", "ISLAS TURQUESAS, CAICOS");
      countryList.put("TD", "TCHAD");
      countryList.put("TG", "TOGO");
      countryList.put("TH", "TAHILANDIA");
      countryList.put("TJ", "TAJIKISTAN");
      countryList.put("TM", "TURKMENISTAN");
      countryList.put("TN", "TUNEZ");
      countryList.put("TO", "TONGA");
      countryList.put("TR", "TURQUIA");
      countryList.put("TT", "TRINIDAD Y TOBAGO (Inc.I.Granadinas Sur)");
      countryList.put("TV", "TUVALU");
      countryList.put("TW", "TAIWAN");
      countryList.put("TZ", "TANZANIA");
      countryList.put("UA", "UCRANIA");
      countryList.put("UG", "UGANDA");
      countryList.put("US", "ESTADOS UNIDOS DE AMERICA");
      countryList.put("UY", "URUGUAY");
      countryList.put("UZ", "UZBEKISTAN");
      countryList.put("VA", "CIUDAD DEL VATICANO");
      countryList.put("VC", "SAN VICENTE (Incluye I.Granadinas del Norte)");
      countryList.put("VE", "VENEZUELA");
      countryList.put("VG", "ISLA VIRGENES BRITANICAS");
      countryList.put("VI", "ISLAS VIRGENES DE LOS ESTADOS UNIDOS");
      countryList.put("VN", "VIETNAM");
      countryList.put("VU", "VANUATU");
      countryList.put("WF", "ISLAS WALLIS Y FORTUNA (Inc.Isla Alofi)");
      countryList.put("WS", "SAMOA OCCIDENTAL");
      countryList.put("XA", "OCEANIA AMERICANA");
      countryList.put("XN", "ORG.INTERNAC.DISTINTOS DE U.E. Y B.C.E.");
      countryList.put("YD", "YEMEN DEL SUR");
      countryList.put("YE", "YEMEN DEL NORTE");
      countryList.put("YE", "YEMEN");
      countryList.put("YT", "MAYOTTE");
      countryList.put("YU", "YUGOSLAVIA");
      countryList.put("ZA", "REPUBLICA DE AFRICA DEL SUR, NAMIBIA");
      countryList.put("ZA", "SURAFRICA");
      countryList.put("ZM", "ZAMBIA");
      countryList.put("ZW", "ZIMBABWIE");

      countryList.put("success", "true");
    }
    catch (Exception e) {
      countryList.put("success", "false");
      countryList.put("errorMessage", e.getLocalizedMessage());
    }
    finally {

    } // finally

    return orderMapByValues(countryList);
  } // getCountries

  /**
   * Devuelve la lista de SettlementType disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de SettlementType.
   */
  public static Map<String, String> getSettlementTypes() {
    HashMap<String, String> settlementTypes = null;

    try {
      settlementTypes = new HashMap<String, String>();

      settlementTypes.put("Domestic / Cedel", "Domestic / Cedel");
      settlementTypes.put("Domestic / Domestic", "Domestic / Domestic");
      settlementTypes.put("Domestic / Euroclear", "Domestic / Euroclear");
      settlementTypes.put("Euroclear / Cedel", "Euroclear / Cedel");
      settlementTypes.put("Euroclear / Domestic", "Euroclear / Domestic");
      settlementTypes.put("Euroclear / Euroclear", "Euroclear / Euroclear");

      settlementTypes.put("success", "true");
    }
    catch (Exception e) {
      settlementTypes.put("success", "false");
      settlementTypes.put("errorMessage", e.getLocalizedMessage());
    }
    finally {

    } // finally

    return Utils.orderMapByValues(settlementTypes);
  } // getSettlementTypes

  /**
   * Ordena los elementos de un Map por los valores.
   * 
   * @param map mapa a ordenar.
   * @return Mapa ordenado.
   */
  public static <K, V extends Comparable<? super V>> Map<K, V> orderMapByValues(Map<K, V> map) {
    List<Map.Entry<K, V>> entries = new LinkedList<Map.Entry<K, V>>(map.entrySet());

    Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {

      // JDK 1.6 @Override
      public int compare(Entry<K, V> o1, Entry<K, V> o2) {
        return o1.getValue().compareTo(o2.getValue());
      } // compare
    }); // Collections.sort

    // LinkedHashMap will keep the keys in the order they are inserted
    // which is currently sorted on natural ordering
    Map<K, V> sortedMap = new LinkedHashMap<K, V>();

    for (Map.Entry<K, V> entry : entries) {
      sortedMap.put(entry.getKey(), entry.getValue());
    } // for

    return sortedMap;
  } // orderMapByValues

  public static Properties getConfig() throws IOException {

    URL configUrl = Utils.class.getClassLoader().getResource("tacticoMegaraConfig.properties");

    Properties config = new Properties();

    try (InputStream is = (InputStream) configUrl.getContent();) {
      config.load(is);
    }

    if (config.get("tactico.megara.config.folder") != null
        && !((String) config.get("tactico.megara.config.folder")).trim().isEmpty()) {
      Path configPath = Paths.get(String.format("%s/tacticoMegaraConfig.properties",
          config.get("tactico.megara.config.folder")));
      if (Files.exists(configPath)) {
        Properties config2 = new Properties();
        try (InputStream is = Files.newInputStream(configPath)) {
          config2.load(is);
        }
        config = config2;
      }
    }
    return config;
  }

} // Utils

