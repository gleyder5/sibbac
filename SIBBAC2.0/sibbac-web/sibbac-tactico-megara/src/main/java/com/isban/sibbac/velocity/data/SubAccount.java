package com.isban.sibbac.velocity.data;

import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.velocity.Generator;

public class SubAccount extends GeneralData {
	
	public String settlementTypeCLG ="";
	public String clearerCLG ="";
	public String accountAtClearerCLG ="";
	public String clearingModeCLG ="";
	public String custodianCLG ="";
	public String settlementTypeCLM[] =null;
	public String clearerCLM[] =null;
	public String accountAtClearerCLM[] =null;
	public String custodianCLM[] =null;
	public String marketCLM[] =null;
	public String settlementTypeCUG ="";
	public String custodianCUG ="";
	public String settlementTypeCUM[] =null;
	public String custodianCUM[] =null;
	public String marketCUM[] =null;	
	public String mode="";
	public String op="";
	public String broClient="";
	public String name = "";
	public String alertCode = "";
	public String identifier = "";
	public String alias="";
	
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getBroClient() {
		return broClient;
	}
	public void setBroClient(String broClient) {
		this.broClient = broClient;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlertCode() {
		return alertCode;
	}
	public void setAlertCode(String alertCode) {
		this.alertCode = alertCode;
	}
	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getSettlementTypeCLG() {
		return settlementTypeCLG;
	}
	public void setSettlementTypeCLG(String settlementTypeCLG) {
		this.settlementTypeCLG = settlementTypeCLG;
	}
	public String getClearerCLG() {
		return clearerCLG;
	}
	public void setClearerCLG(String clearerCLG) {
		this.clearerCLG = clearerCLG;
	}
	public String getAccountAtClearerCLG() {
		return accountAtClearerCLG;
	}
	public void setAccountAtClearerCLG(String accountAtClearerCLG) {
		this.accountAtClearerCLG = accountAtClearerCLG;
	}
	public String getClearingModeCLG() {
		return clearingModeCLG;
	}
	public void setClearingModeCLG(String clearingModeCLG) {
		this.clearingModeCLG = clearingModeCLG;
	}
	public String getCustodianCLG() {
		return custodianCLG;
	}
	public void setCustodianCLG(String custodianCLG) {
		this.custodianCLG = custodianCLG;
	}
	public String[] getSettlementTypeCLM() {
	  return settlementTypeCLM == null ? null : settlementTypeCLM.clone();
	}
	public void setSettlementTypeCLM(String[] settlementTypeCLM) {
		this.settlementTypeCLM = settlementTypeCLM;
	}
	public String[] getClearerCLM() {
	  return clearerCLM == null ? null : clearerCLM.clone();
	}
	public void setClearerCLM(String[] clearerCLM) {
		this.clearerCLM = clearerCLM;
	}
	public String[] getAccountAtClearerCLM() {
	  return accountAtClearerCLM == null ? null : accountAtClearerCLM.clone();
	}
	public void setAccountAtClearerCLM(String[] accountAtClearerCLM) {
		this.accountAtClearerCLM = accountAtClearerCLM;
	}
	public String[] getCustodianCLM() {
	  return custodianCLM == null ? null : custodianCLM.clone();
	}
	public void setCustodianCLM(String[] custodianCLM) {
		this.custodianCLM = custodianCLM;
	}
	public String[] getMarketCLM() {
	  return marketCLM == null ? null : marketCLM.clone();
	}
	public void setMarketCLM(String[] marketCLM) {
		this.marketCLM = marketCLM;
	}
	public String getSettlementTypeCUG() {
		return settlementTypeCUG;
	}
	public void setSettlementTypeCUG(String settlementTypeCUG) {
		this.settlementTypeCUG = settlementTypeCUG;
	}
	public String getCustodianCUG() {
		return custodianCUG;
	}
	public void setCustodianCUG(String custodianCUG) {
		this.custodianCUG = custodianCUG;
	}
	public String[] getSettlementTypeCUM() {
	  return settlementTypeCUM == null ? null : settlementTypeCUM.clone();
	}
	public void setSettlementTypeCUM(String[] settlementTypeCUM) {
		this.settlementTypeCUM = settlementTypeCUM;
	}
	public String[] getCustodianCUM() {
	  return custodianCUM == null ? null : custodianCUM.clone();
	}
	public void setCustodianCUM(String[] custodianCUM) {
		this.custodianCUM = custodianCUM;
	}
	public String[] getMarketCUM() {
	  return marketCUM == null ? null : marketCUM.clone();
	}
	public void setMarketCUM(String[] marketCUM) {
		this.marketCUM = marketCUM;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	
	
	
	public void complete()  throws Exception
	{
		if (alertCode.length()> 20)
			throw new Exception("Ha introducido un AlertCode superior a 20 posiciones. Ajustelo a ese tama�o y luego cambielo por el valor correcto en el FDA");		
		
		
		
	/*	final String otherAliasSameSubAccount = QueryDAO.getOtherSubAccount(identifier,alias);
		
		if (!"".equals(otherAliasSameSubAccount))		
			throw new Exception("Esta subcuenta ya estaba asignado al alias  " + otherAliasSameSubAccount + " as� que no se puede dar de alta esta subcuebta por duplicarse. Si se quiere que esta subcuenta tb cuelque de ese alias habr� que ir a la pantalla Activate y asociarlo");
		*/
		
		
		if ("N".equals(mode))
		{			
			
			if (!QueryDAO.isNewSubAccount(identifier) )
			{
				throw new Exception("Ya existe esta subcuenta dada de alta. Tiene que hacer una modificación");
			}
			
			
				
			
			final String otherAlertCode  = QueryDAO.getOtherAlertCodeAlta(alertCode,alias);
			if (!"".equals(otherAlertCode) && !identifier.equals(otherAlertCode))		
				throw new Exception("La subcuenta " + otherAlertCode + " de este Alias tiene el mismo alertCode. Elegir otro o dar de baja la otra subcuenta");
			
			if (QueryDAO.aliasTable.containsKey(new Integer("99"+identifier)))
			{
				throw new Exception("Se está procesando un alta con el mismo número de subcuenta. Tiene que volver a elegir el número de subcuenta");
				
			}
			else
			{
				QueryDAO.aliasTable.put(new Integer("99"+identifier), new Integer("99"+identifier));
				
			}
			
		}
		
		else if ( "U".equals(mode))
		{
			final String otherAlertCode  = QueryDAO.getOtherAlertCode(alertCode,identifier);
			if (!"".equals(otherAlertCode) )		
				throw new Exception("La subcuenta " + otherAlertCode + " de este Alias tiene el mismo alertCode. Elegir otro o dar de baja la otra subcuenta");
			
			
		}
		broClient = Generator.escapeXml(broClient);	
		name = Generator.escapeXml(name);	
		
		
	}

}
