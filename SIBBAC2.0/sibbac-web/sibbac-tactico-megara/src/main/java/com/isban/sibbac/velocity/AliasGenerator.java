package com.isban.sibbac.velocity;

import java.util.Map;
import java.util.Properties;

public class AliasGenerator extends Generator {

  /**
   * @param args
   */

  public AliasGenerator(Properties prop, Map<String, String[]> parametrosIn) {
    super(prop, parametrosIn);

  }

  public boolean generateTemplate() throws Exception {

    template = "PlantillaAlias.xml";
    objectName = "alias";
    object = loadFields();
    initVelocity();
    generateHTML();

    return true;
  }

}
