package com.isban.sibbac.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.isban.sibbac.db.DBManager;

public class GenericDAO {
  private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(GenericDAO.class);
  public static String rutaLogs;

  public GenericDAO() {
  }

  protected static String getValue(String sentence, String... params) throws SQLException {

    log.debug("[getValue] Sentence: {} params: {}", sentence, params);
    String out = "";

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    try {
      conn = DBManager.getConnection();
      ps = conn.prepareStatement(sentence);
      int ctr = 1;
      for (String param : params) {
        if (param != null) {
          ps.setString(ctr, param.trim());
          ctr++;
        }
      }

      final boolean ok = ps.execute();
      if (ok) {
        rs = ps.getResultSet();
        if (rs.next()) {
          out = rs.getString(1);
          if (out != null) {
            out = out.trim();
          }
          else {
            out = "";
          }
        }
      }
    }
    catch (final SQLException ex) {
      log.warn("Hubo un error al obtener el valor [" + sentence + "]", ex);
      throw ex;
    }
    finally {
      close(rs);
      close(ps);
      close(conn);
    }

    return out;
  }

  protected static List<String> getValues(String sentence, String... params) throws SQLException {
    log.debug("[getValues] Sentence: {} params: {}", sentence, params);
    ArrayList<String> outList = new ArrayList<String>();

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    try {
      conn = DBManager.getConnection();
      ps = conn.prepareStatement(sentence);
      int ctr = 1;
      for (String param : params) {
        if (param != null) {
          ps.setString(ctr, param.trim());
          ctr++;
        }
      }

      final boolean ok = ps.execute();
      if (ok) {
        rs = ps.getResultSet();
        while (rs.next()) {
          String out = rs.getString(1);
          if (out != null) {
            out = out.trim();
            outList.add(out);
          }
          else {

          }

        }
      }
    }
    catch (final SQLException ex) {
      log.warn("Hubo un error al obtener el valor [" + sentence + "]", ex);
      throw ex;
    }
    finally {
      close(rs);
      close(ps);
      close(conn);
    }

    return outList;
  }

  protected synchronized static void close(Object o) {
    if (o != null) {
      try {
        if (o instanceof ResultSet) {
          try {
            ((ResultSet) o).close();
          }
          catch (Exception exx) {
          }
        }
        else if (o instanceof Statement) {
          try {
            ((Statement) o).close();
          }
          catch (Exception exx) {
          }
        }
        else if (o instanceof PreparedStatement) {
          try {
            ((PreparedStatement) o).close();
          }
          catch (Exception exx) {
          }
        }
        if (o instanceof Connection) {
          try {
            ((Connection) o).close();
          }
          catch (Exception exx) {
          }
        }
        else
          log.info("............>" + o.getClass().getName());
      }
      catch (Exception ignore) {
        log.warn(ignore.getMessage(), ignore);
      }
    }
  }

  protected static HashMap<String, String> getElementList(String sentence, String... params) throws SQLException {
    log.debug("[getElementList] Sentence: {} params: {}", sentence, params);
    HashMap<String, String> list = null;
    list = new HashMap<String, String>();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    try {
      conn = DBManager.getConnection();

      ps = conn.prepareStatement(sentence);

      int ctr = 1;
      if (params != null) {
        for (String param : params) {
          if (param != null) {
            ps.setString(ctr, param.trim());
            ctr++;
          }
        }
      }

      final boolean ok = ps.execute();
      if (ok) {
        rs = ps.getResultSet();
        while (rs.next()) {
          final String key = rs.getString(1).trim();
          final String value = rs.getString(2).trim();
          list.put(key, value);
        }
      }
    }
    catch (final SQLException ex) {
      log.warn("Hubo un error al obtener la lista de elementos [" + sentence + "]\n\tP1:" + params, ex);
      throw ex;
    }
    finally {
      close(rs);
      close(ps);
      close(conn);
    }

    return list;
  }

  protected static HashMap<String, String> getElementList(String sentence) throws SQLException {
    return getElementList(sentence, null);
  }

  /*
   * protected static HashMap<String, String> getElementList(String sentence)
   * throws SQLException { HashMap<String, String> list = null;
   * 
   * 
   * { list = new HashMap<String, String>(); Connection conn = null;
   * PreparedStatement ps = null; ResultSet rs = null; try { conn =
   * DBManager.getConnection();
   * 
   * ps = conn.prepareStatement(sentence);
   * 
   * 
   * 
   * 
   * final boolean ok = ps.execute(); if (ok) { rs = ps.getResultSet(); while
   * (rs.next()) { final String key = rs.getString(1).trim(); final String value
   * = rs.getString(2).trim(); list.put(key, value); } } } catch (final
   * SQLException ex) { ex.printStackTrace(); throw ex; } finally { close(rs);
   * close(ps); close(conn); } }
   * 
   * return list; }
   */

  protected static String getElementById(String sentence, String identifier) throws SQLException {
    log.debug("[getElementById] Sentence: {} identifier: {}", sentence, identifier);
    String out = "";

    {

      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      try {
        conn = DBManager.getConnection();

        ps = conn.prepareStatement(sentence);
        ps.setString(1, identifier.trim());

        final boolean ok = ps.execute();
        if (ok) {
          rs = ps.getResultSet();
          if (rs.next()) {
            out = rs.getString(1);
          }
        }
      }
      catch (final SQLException ex) {
        log.warn("Hubo un error al obtener el elemento [" + sentence + "]\n\tP1:" + identifier, ex);
        throw ex;
      }
      finally {
        close(rs);
        close(ps);
        close(conn);
      }
    }

    return out;
  }

}
