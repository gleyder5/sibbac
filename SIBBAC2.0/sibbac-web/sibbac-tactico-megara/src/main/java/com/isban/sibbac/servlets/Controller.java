package com.isban.sibbac.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.isban.sibbac.db.dao.QueryDAO;
import com.isban.sibbac.servlets.wrappers.RequestWrapper;
import com.isban.sibbac.servlets.wrappers.ResponseWrapper;
import com.isban.sibbac.utils.Utils;
import com.isban.sibbac.velocity.AliasGenerator;
import com.isban.sibbac.velocity.AliasSubAccountGenerator;
import com.isban.sibbac.velocity.AliasSubAccountGenerator2;
import com.isban.sibbac.velocity.BroClientGenerator;
import com.isban.sibbac.velocity.EntityGenerator;
import com.isban.sibbac.velocity.SettInstGenerator;
import com.isban.sibbac.velocity.SettMapGenerator;
import com.isban.sibbac.velocity.SubAccountGenerator;

/**
 * Servlet implementation class Controller
 * 
 * @author Alberto Horn�s
 */
// @WebServlet(asyncSupported = true, description =
// "Servlet que recibe las peticiones del menu de la aplicacion", urlPatterns =
// { "/Controller" })
public class Controller extends HttpServlet {

  private final static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Controller.class.getName());

  /* ~~~~~~~~~~~~~~~~~~~~ ATRIBUTOS ~~~~~~~~~~~~~~~~~~~~ */

  /** Identificador para la serializaci�n de objetos. */
  private static final long serialVersionUID = 1L;

  /* ~~~~~~~~~~~~~~~~~~~~ CONSTRUCTOR ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Controller() {
    super();

  } // Controller

  /* ~~~~~~~~~~~~~~~~~~~~ M�TODOS SOBRESCRITOS ~~~~~~~~~~~~~~~~~~~~ */

  public void init(ServletConfig config) throws ServletException {
    // Always call super.init(config) first (servlet mantra #1)

    super.init(config);

    try {
      QueryDAO.initSequences();
    }
    catch (SQLException ex) {
      log.warn("Hubo un error al iniciar las secuencias del DAO", ex);
    }
  }

  /**
   * @see Servlet#getServletInfo()
   */
  public String getServletInfo() {
    return "Servlet que recibe las peticiones del menu de la aplicacion";
  } // getServletInfo

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    this.handleRequest(request, response);
  } // doGet

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   * response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    this.handleRequest(request, response);
  } // doPost

  /* ~~~~~~~~~~~~~~~~~~~~ M�TODOS PRIVADOS ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void handleRequest(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {

    Properties config = Utils.getConfig();

    HttpServletRequest request = req;
    if (req.getMethod().equalsIgnoreCase("POST")) {
      request = new RequestWrapper(req);
      ((RequestWrapper) request).parsePostData(req.getContentLength(), req.getInputStream());

    }

    // parsePostData(request.getContentLength(), request.getInputStream());

    traceRequest(request);

    ResponseWrapper wrapper = new ResponseWrapper(response);

    // log.info("Entra en SIBBAC");
    if (request.getParameter("op") != null) {
      wrapper.setOperation(request.getParameter("op"));
      boolean aceptGzip = (request.getHeader("accept-encoding").toLowerCase().indexOf("") != -1) ? true : false;
      wrapper.setMode(aceptGzip);
      response.setContentType("application/json;charset=ISO-8859-1");
      if (request.getParameter("op").equals("getCountries")) { // getCountries
        wrapper.writeData(new Gson().toJson(Utils.getCountries()));
      }
      else if (request.getParameter("op").equals("getSettlementTypes")) { // getSettlementTypes
        wrapper.writeData(new Gson().toJson(Utils.getSettlementTypes()));
      }
      else if (request.getParameter("op").equals("getBroClientIdentifiers")) { // getBroClientIdentifiers
        wrapper.writeData(new Gson().toJson(getAvailableBroClientIdentifiers()));
      }
      else if (request.getParameter("op").equals("getBroClientManagementCategory")) { // getBroClientManagementCategory
        wrapper.writeData(new Gson().toJson(getBroClientManagementCategory()));
      }
      else if (request.getParameter("op").equals("getBroClientFiscalCategory")) { // getBroClientFiscalCategory
        wrapper.writeData(new Gson().toJson(getBroClientFiscalCategory()));
      }
      else if (request.getParameter("op").equals("getAliasBroClient")) { // getAliasBroClient
        wrapper.writeData(new Gson().toJson(getAliasBroClient()));
      }
      else if (request.getParameter("op").equals("getAliasAlias")) { // getAliasAlias
        wrapper.writeData(new Gson().toJson(getAliasAlias()));

      }
      else if (request.getParameter("op").equals("getSubAccountId")) { // getSubAccountId
        wrapper.writeData(new Gson().toJson(getSubAccountId()));

      }

      else if (request.getParameter("op").equals("getDwhCode")) { // getDwhCode
        wrapper.writeData(new Gson().toJson(getDwhCode()));

      }
      else if (request.getParameter("op").equals("getAliasTraders")) { // getAliasTraders

        wrapper.writeData(new Gson().toJson(getAliasTraders()));
      }
      else if (request.getParameter("op").equals("getAliasSales")) { // getAliasSales
        wrapper.writeData(new Gson().toJson(getAliasSales()));
      }
      
      else if (request.getParameter("op").equals("getTipoAlias")) { // getTipoAlias
        wrapper.writeData(new Gson().toJson(getTipoAlias()));
      }
      else if (request.getParameter("op").equals("getAliasManagers")) { // getAliasManagers
        wrapper.writeData(new Gson().toJson(getAliasManagers()));
      }
      else if (request.getParameter("op").equals("getAliasTradersLM")) { // getAliasTradersLM
        wrapper.writeData(new Gson().toJson(getAliasTradersLM()));
      }
      else if (request.getParameter("op").equals("getAliasSalesLM")) { // getAliasSalesLM
        wrapper.writeData(new Gson().toJson(getAliasSalesLM()));
      }
      else if (request.getParameter("op").equals("getAliasManagersLM")) { // getAliasManagersLM
        wrapper.writeData(new Gson().toJson(getAliasManagersLM()));
      }
      else if (request.getParameter("op").equals("getAsrAlias")) { // getAsrAlias
        wrapper.writeData(new Gson().toJson(getAsrAlias()));
      }
      else if (request.getParameter("op").equals("getAsrSubaccounts")) { // getAsrSubaccounts

        String alias = request.getParameter("alias");
        wrapper.writeData(new Gson().toJson(getAsrSubaccounts(alias)));
      }
      else if (request.getParameter("op").equals("asr")) { // entities

        try {

          if (request.getParameter("subAccount2").equals("TODAS"))
            wrapper.writeData(new Gson().toJson(processAsr2(config, request, response)));
          else

            wrapper.writeData(new Gson().toJson(processAsr(config, request, response)));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

          log.warn("Hubo un error al procesar una relaci�n alias-subcuenta (" + request.getParameter("alias") + "-"
              + request.getParameter("subAccount") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");

        } // catch

      }
      else if (request.getParameter("op").equals("asr2")) { // entities

        try {
          wrapper.writeData(new Gson().toJson(processAsr2(config, request, response)));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

          log.warn("Hubo un error al procesar una relaci�n alias-subcuenta (" + request.getParameter("alias") + "-"
              + request.getParameter("subAccount") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");

        } // catch

      }
      else if (request.getParameter("op").equals("getSettleInstAlias")) { // getSettleInstAlias
        wrapper.writeData(new Gson().toJson(getSettleInstAlias()));
      }
      else if (request.getParameter("op").equals("getSettleInstSubaccount")) {// getSettleInstSubaccount
        String alias = request.getParameter("alias");
        wrapper.writeData(new Gson().toJson(getSettleInstSubaccount(alias)));
      }
      else if (request.getParameter("op").equals("getSettleInstMarket")) { // getSettleInstMarket
        wrapper.writeData(new Gson().toJson(getSettleInstMarket()));
      }
      else if (request.getParameter("op").equals("getSettleInstCounterpartIdentifier")) { // getSettleInstCounterpartIdentifier
        wrapper.writeData(new Gson().toJson(getSettleInstCounterpartIdentifier()));
      }
      else if (request.getParameter("op").equals("getEntitiesList")) { // getSettleInstCounterpartIdentifier
        wrapper.writeData(new Gson().toJson(getEntitiesList()));
      }
      else if (request.getParameter("op").equals("getSettleInstBeneficiaryId")) { // getSettleInstBeneficiaryId
        String elementCode = request.getParameter("elementCode").trim();
        wrapper.writeData(new Gson().toJson(getSettleInstBeneficiaryId(elementCode)));
      }
      else if (request.getParameter("op").equals("getSettleInstBeneficiary")) { // getSettleInstBeneficiary
        wrapper.writeData(new Gson().toJson(getSettleInstBeneficiary()));
      }
      else if (request.getParameter("op").equals("getSettleInstLocalCustodianId")) { // getSettleInstLocalCustodianId

        String elementCode1 = request.getParameter("elementCode1").trim();
        String elementCode2 = request.getParameter("elementCode2").trim();
        String elementCode3 = request.getParameter("elementCode3").trim();
        String elementCode4 = request.getParameter("elementCode4").trim();
        wrapper.writeData(new Gson().toJson(getSettleInstLocalCustodianId(elementCode1, elementCode2, elementCode3,
            elementCode4)));
      }
      else if (request.getParameter("op").equals("getSettleInstLocalCustodian")) { // getSettleInstLocalCustodian

        wrapper.writeData(new Gson().toJson(getSettleInstLocalCustodian()));
      }
      else if (request.getParameter("op").equals("getSettleInstGlobalCustodianId")) { // getSettleInstGlobalCustodianId

        String elementCode1 = request.getParameter("elementCode1").trim();
        String elementCode2 = request.getParameter("elementCode2").trim();

        wrapper.writeData(new Gson().toJson(getSettleInstGlobalCustodianId(elementCode1, elementCode2)));
      }
      else if (request.getParameter("op").equals("getSettleInstGlobalCustodian")) { // getSettleInstGlobalCustodian
        wrapper.writeData(new Gson().toJson(getSettleInstGlobalCustodian()));
      }
      else if (request.getParameter("op").equals("getSettleInstPlaceOfSettlement")) { // getSettleInstPlaceOfSettlement

        String elementCode1 = request.getParameter("elementCode1").trim();
        String elementCode2 = request.getParameter("elementCode2").trim();
        String elementCode3 = request.getParameter("elementCode3").trim();
        String elementCode4 = request.getParameter("elementCode4").trim();
        wrapper.writeData(new Gson().toJson(getSettleInstPlaceOfSettlement(elementCode1, elementCode2, elementCode3,
            elementCode4)));
      }

      else if (request.getParameter("op").equals("getSettleInstPlaceOfSettlementNew")) { // getSettleInstPlaceOfSettlement

        String elementCode1 = request.getParameter("elementCode1").trim();
        String elementCode2 = request.getParameter("elementCode2").trim();
        wrapper.writeData(new Gson().toJson(getSettleInstPlaceOfSettlementNew(elementCode1, elementCode2)));
      }

      else if (request.getParameter("op").equals("getStatisticLevel0")) { // getStatisticLevel0

        wrapper.writeData(new Gson().toJson(getStatisticLevel0()));
      }
      else if (request.getParameter("op").equals("getStatisticLevel1")) { // getStatisticLevel1

        String previousLevel = request.getParameter("previousLevel");
        wrapper.writeData(new Gson().toJson(getStatisticLevel1(previousLevel)));
      }
      else if (request.getParameter("op").equals("getStatisticLevel2")) { // getStatisticLevel2

        String previousLevel = request.getParameter("previousLevel");
        wrapper.writeData(new Gson().toJson(getStatisticLevel2(previousLevel)));
      }
      else if (request.getParameter("op").equals("getStatisticLevel3")) { // getStatisticLevel3

        String previousLevel = request.getParameter("previousLevel");
        wrapper.writeData(new Gson().toJson(getStatisticLevel3(previousLevel)));
      }
      else if (request.getParameter("op").equals("getStatisticLevel4")) { // getStatisticLevel4

        String previousLevel = request.getParameter("previousLevel");
        wrapper.writeData(new Gson().toJson(getStatisticLevel4(previousLevel)));
      }
      else if (request.getParameter("op").equals("entities")) { // entities

        try {
          final Map<String, String> entityMap = processEntity(config, request, response);
          wrapper.writeData(new Gson().toJson(entityMap));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          log.warn("Hubo un error al procesar una entidad (" + request.getParameter("identifier") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");
        } // catch
      }
      else if (request.getParameter("op").equals("broclient")) { // broclient

        try {
          wrapper.writeData(new Gson().toJson(processBroClient(config, request, response)));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          log.warn("Hubo un error al procesar un bloclient (" + request.getParameter("identifier") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");
        } // catch
      }

      else if (request.getParameter("op").equals("alias")) { // alias

        try {
          wrapper.writeData(new Gson().toJson(processAlias(config, request, response)));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          log.warn("Hubo un error al procesar un alias (" + request.getParameter("alias") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");
        } // catch
      }

      else if (request.getParameter("op").equals("subAccount")) { // subAccount

        try {
          wrapper.writeData(new Gson().toJson(processSubAccount(config, request, response)));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          log.warn("Hubo un error al procesar una subcuenta (" + request.getParameter("identifier") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");
        } // catch
      }

      else if (request.getParameter("op").equals("settleinst")) { // SettleInst

        try {
          final Map<String, String> settInstMap = processSettleInst(config, request, response);
          wrapper.writeData(new Gson().toJson(settInstMap));
        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          log.warn("Hubo un error al procesar una instruccion (Alias: " + request.getParameter("alias")
              + ";Subcuenta: " + request.getParameter("subaccount") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");
        } // catch

      }

      else if (request.getParameter("op").equals("settMap")) { // SettleInst

        try {
          final Map<String, String> settInstMap = processSettMap(config, request, response);
          wrapper.writeData(new Gson().toJson(settInstMap));

        }
        catch (Exception e) {
          // response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
          log.warn("Hubo un error al procesar una relaci�n alias-subcuenta (" + request.getParameter("alias") + "-"
              + request.getParameter("subAccount") + ")", e);
          wrapper.writeData("Error procesando petici�n ... [" + HttpServletResponse.SC_INTERNAL_SERVER_ERROR + "] ... "
              + e.getLocalizedMessage() + "\n");
        } // catch

      }
    }
    else {
      try {
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
            "Parametro necesario 'op' no presente en la peticion");
      }
      catch (IOException ioe) {
        log.warn("Hubo un error internod", ioe);
      } // catch
    } // else

    traceResponse(wrapper);
  } // handleRequest

  /* Funciones de traceo de peticiones */
  private void traceRequest(HttpServletRequest request) {

    Enumeration<String> paramNames = request.getParameterNames();
    StringBuilder sb = new StringBuilder("[REQUEST] {");
    while (paramNames.hasMoreElements()) {
      String name = paramNames.nextElement();
      String value = request.getParameter(name);
      sb.append(name + " : " + value + " ;");
    }
    sb.append("}");
    log.info(sb.toString());
  }

  /* Fin Funciones de traceo de peticiones */

  private void traceResponse(ResponseWrapper response) {
    log.info("[RESPONSE] (" + response.getOperation() + ")" + response.toString());
  }

  /* ~~~~~~~~~~~~~~~~~~~~ OPERACIONES PANTALLA ENTITY ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Procesa las peticiones de operaciones sobre entidades.
   * 
   * @param request
   * @param response
   */
  private Map<String, String> processEntity(Properties config, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    HashMap<String, String> salida = null;
    try {

      salida = new HashMap<String, String>();

      log.debug(" # processEntity # ");

      if (request.getParameter("mode").equals("S")) {

        log.debug(" # processEntity #  Search");

        final String entity = request.getParameter("identifier");

        String out = QueryDAO.getNewEntity(entity);

        String command = ("".equals(out)) ? "" : getEntityCommand(out);

        salida.put("success", "true");
        salida.put("message", command);

      }
      else { // New
        log.debug(" # processEntity #  New");

        log.debug("ruta servlet es " + getServletContext().getRealPath("/"));

        Map<String, String[]> parametros = request.getParameterMap();

        // LLamada al m�todo de Generar xmls de Entities
        final EntityGenerator entityGenerator = new EntityGenerator(config, parametros);
        entityGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

        // salida.put("success", "false");
        // salida.put("errorMessage", "error");

      } // else
    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar una entidad (" + request.getParameter("identifier") + ")", e);
      salida.put("success", "false");
      final String exceptionMessage = e.getLocalizedMessage();
      if (exceptionMessage == null) {
        salida.put("errorMessage", "Error ..." + e.getCause().getMessage());
        log.info("Error en process Entity" + e.getCause().getMessage());
      }

      else {
        salida.put("errorMessage", "Error ..." + exceptionMessage);
        log.info("Error en process Entity" + exceptionMessage);
      }

    }

    return salida;
  } // processEntity

  /* ~~~~~~~~~~~~~~~~~~~~ OPERACIONES PANTALLA BROCLIENT ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Devuelve la lista de identificadores de broClient disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClient IDs.
   */
  private Map<String, String> getAvailableBroClientIdentifiers() {
    HashMap<String, String> broClientIdsList = new HashMap<String, String>();

    try {
      /*
       * broClientIdsList = new HashMap<String, String>();
       * 
       * broClientIdsList.put("LM", "Lionel Messi"); broClientIdsList.put("CR",
       * "Cristiano Ronaldo"); broClientIdsList.put("DM", "Diego Maradona");
       */
      broClientIdsList = QueryDAO.getAvailableBroClient();

      broClientIdsList.put("success", "true");

    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener los broclients disponibles", e);
      broClientIdsList.put("success", "false");
      broClientIdsList.put("errorMessage", "Error ...");
    }
    finally {

    } // finally

    return Utils.orderMapByValues(broClientIdsList);
  } // getBroClientIdentifiers

  /**
   * Devuelve la lista de ManagementCategory de broClient disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClient
   * ManagementCategory.
   */
  private Map<String, String> getBroClientManagementCategory() {
    HashMap<String, String> broClientManagementCategories = null;

    try {
      broClientManagementCategories = new HashMap<String, String>();

      broClientManagementCategories.put("HF", "Fondo cobertura");
      broClientManagementCategories.put("IC", "Seguros");
      broClientManagementCategories.put("MF", "Fondos");
      broClientManagementCategories.put("PB", "Banco");
      broClientManagementCategories.put("PT", "Contratado");
      broClientManagementCategories.put("TB", "Tienda bolsa");
      broClientManagementCategories.put("BK", "Broker");
      broClientManagementCategories.put("TE", "Tesorer�a");
      broClientManagementCategories.put("IN", "Institucional");
      broClientManagementCategories.put("OT", "Otros");
      broClientManagementCategories.put("OP", "Cartera");
      broClientManagementCategories.put("SG", "Grupo SCH");

      broClientManagementCategories.put("success", "true");
    }
    catch (Exception e) {
      log.warn("Hubo un error al generar las categorias de gestion de broclients", e);
      broClientManagementCategories.put("success", "false");
      broClientManagementCategories.put("errorMessage", "Error ...");
    }
    finally {

    } // finally

    return Utils.orderMapByValues(broClientManagementCategories);
  } // getBroClientManagementCategory

  /**
   * Devuelve la lista de Fiscal Category de broClient disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClient Fiscal
   * Category.
   */
  private Map<String, String> getBroClientFiscalCategory() {
    HashMap<String, String> broClientFiscalCategories = null;
    try {
      broClientFiscalCategories = new HashMap<String, String>();

      broClientFiscalCategories.put("CLP", "CLP");
      broClientFiscalCategories.put("EPSV", "EPSV");
      broClientFiscalCategories.put("FIL", "FIL");
      broClientFiscalCategories.put("FIM", "FIM");
      broClientFiscalCategories.put("FP", "FP");
      broClientFiscalCategories.put("GES", "GES");
      broClientFiscalCategories.put("IB", "IB");
      broClientFiscalCategories.put("NR", "NR");
      broClientFiscalCategories.put("SDAD", "SDAD");
      broClientFiscalCategories.put("SEGUROS", "SEGUROS");
      broClientFiscalCategories.put("SIM", "SIM");
      broClientFiscalCategories.put("SIMCAV", "SIMCAV");

      broClientFiscalCategories.put("success", "true");
    }
    catch (Exception e) {
      log.warn("Hubo un error al generar las categorias fiscales de broclients", e);
      broClientFiscalCategories.put("success", "false");
      broClientFiscalCategories.put("errorMessage", "Error ...");
    }
    finally {

    } // finally

    return Utils.orderMapByValues(broClientFiscalCategories);
  } // getBroClientFiscalCategory

  /**
   * Procesa las peticiones de operaciones sobre broclients.
   * 
   * @param request
   * @param response
   */
  private Map<String, String> processBroClient(Properties config, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    HashMap<String, String> salida = null;
    try {
      salida = new HashMap<String, String>();

      log.debug(" # processBroClient # ");

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processBroClient #  Search");

        final String broClient = request.getParameter("identifier");

        String out = QueryDAO.getNewBroclient(broClient);

        String command = ("".equals(out)) ? "" : getBroclientCommand(out);

        salida.put("success", "true");
        salida.put("message", command);
      }
      else { // New
        log.debug(" # processBroClient #  New");

        Map<String, String[]> parametros = request.getParameterMap();
        // Map<String, String[]> parametros = parameters;

        // LLamada al m�todo de Generar xmls de Entities
        final BroClientGenerator broClientGenerator = new BroClientGenerator(config, parametros);
        broClientGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

      }
    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar un broclient (" + request.getParameter("identifier") + ")", e);
      salida.put("success", "false");
      final String exceptionMessage = e.getLocalizedMessage();
      if (exceptionMessage == null) {
        salida.put("errorMessage", "Error ..." + e.getCause().getMessage());
        log.info("Error en process BroClient" + e.getCause().getMessage());
      }

      else {
        salida.put("errorMessage", "Error ..." + exceptionMessage);
        log.info("Error en process BroClient" + exceptionMessage);
      }

    }

    return salida;
  } // processBroClient

  /* ~~~~~~~~~~~~~~~~~~~~ OPERACIONES PANTALLA ALIAS ~~~~~~~~~~~~~~~~~~~~ */

  /**
   * Procesa las peticiones de operaciones sobre Alias.
   * 
   * @param request
   * @param response
   */
  private Map<String, String> processAlias(Properties config, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    HashMap<String, String> salida = null;
    try {
      salida = new HashMap<String, String>();

      log.debug(" # processAlias # ");

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processAlias #  Search");

        final String alias = request.getParameter("alias");

        String out = QueryDAO.getNewAlias(alias);
        String command = ("".equals(out)) ? "" : getAliasCommand(out);

        salida.put("success", "true");
        salida.put("message", command);
      }
      else { // New
        log.debug(" # processAlias #  New");

        Map<String, String[]> parametros = request.getParameterMap();

        // LLamada al m�todo de Generar xmls de Entities
        final AliasGenerator aliasGenerator = new AliasGenerator(config, parametros);
        aliasGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

      }

    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar un alias (" + request.getParameter("alias") + ")", e);
      salida.put("success", "false");
      final String exceptionMessage = e.getLocalizedMessage();
      if (exceptionMessage == null) {
        salida.put("errorMessage", "Error ..." + e.getCause().getMessage());
        log.info("Error en process Alias" + e.getCause().getMessage());
      }

      else {
        salida.put("errorMessage", "Error ..." + exceptionMessage);
        log.info("Error en process Alias" + exceptionMessage);
      }

    }

    return salida;
  } // processAlias

  private String getAliasCommand(String out) throws ParseException, SQLException {
    String outCommand = "";

    out += "---xxx";

    final String[] commands = out.split("---");

    final String broClient = commands[0].trim();
    final String alias = commands[1].trim();

    final String allocationType = commands[2].trim();
    String allocationTypeConverted = QueryDAO.getConversionValue("TPALLOCA", allocationType);
    if ("".equals(allocationTypeConverted))
      allocationTypeConverted = " ";

    final String expectedAllocation = commands[3].trim();
    String expectedAllocationConverted = QueryDAO.getConversionValue("TPEXALLO", expectedAllocation);
    if ("".equals(expectedAllocationConverted))
      expectedAllocationConverted = " ";

    final String marketAllocationRule = commands[4].trim();
    String marketAllocationRuleConverted = QueryDAO.getConversionValue("TPMAALLO", marketAllocationRule);
    if ("".equals(marketAllocationRuleConverted))
      marketAllocationRuleConverted = " ";

    final String confirmationType = commands[5].trim();
    String confirmationTypeConverted = QueryDAO.getConversionValue("TPCONFIR", confirmationType);
    if ("".equals(confirmationTypeConverted))
      confirmationTypeConverted = " ";

    final String calculationType = commands[6].trim();
    String calculationTypeConverted = QueryDAO.getConversionValue("TPCALCUU", calculationType);
    if ("".equals(calculationTypeConverted))
      calculationTypeConverted = " ";

    final String calculationTypeLM = commands[7].trim();
    String calculationTypeLMConverted = QueryDAO.getConversionValue("TPCALCLM", calculationTypeLM);
    if ("".equals(calculationTypeLMConverted))
      calculationTypeLMConverted = " ";

    final String forOwnAccount = getBooleanValue(commands[8].trim());
    final String payMarket = getBooleanValue(commands[9].trim());
    final String reconcileAllocation = getBooleanValue(commands[10].trim());
    final String reconcileAllocationLM = getBooleanValue(commands[11].trim());
    final String reconcileConfirmation = getBooleanValue(commands[12].trim());
    final String reconcileConfirmationLM = getBooleanValue(commands[13].trim());
    final String requireAllocation = getBooleanValue(commands[14].trim());
    final String requireAllocationLM = getBooleanValue(commands[15].trim());
    final String requireAllocConfirmation = getBooleanValue(commands[16].trim());
    final String requireConfirmation = getBooleanValue(commands[17].trim());
    final String stp = getBooleanValue(commands[18].trim());
    final String exonerated = getBooleanValue(commands[19].trim());
    final String manualRetrocession = getBooleanValue(commands[20].trim());
    final String routing = getBooleanValue(commands[21].trim());
    final String requireRetrocession = getBooleanValue(commands[22].trim());

    String trader = commands[23].trim();
    if ("".equals(trader))
      trader = " ";

    String sale = commands[24].trim();
    if ("".equals(sale))
      sale = " ";

    String manager = commands[25].trim();
    if ("".equals(manager))
      manager = " ";

    final String statisticGroup = commands[26].trim();
    final String statisticLevel0 = commands[27].trim();
    final String statisticLevel1 = commands[28].trim();
    final String statisticLevel2 = commands[29].trim();
    final String statisticLevel3 = commands[30].trim();
    final String statisticLevel4 = commands[31].trim();

    String traderLM = commands[32].trim();
    if ("".equals(traderLM))
      traderLM = " ";

    String saleLM = commands[33].trim();
    if ("".equals(saleLM))
      saleLM = " ";

    String managerLM = commands[34].trim();
    if ("".equals(managerLM))
      managerLM = " ";

    final String statisticGroupLM = commands[35].trim();
    final String statisticLevel0LM = commands[36].trim();
    final String statisticLevel1LM = commands[37].trim();
    final String statisticLevel2LM = commands[38].trim();
    final String statisticLevel3LM = commands[39].trim();
    final String statisticLevel4LM = commands[40].trim();
    final String description = commands[41].trim();
    final String oasysCode = QueryDAO.getOasysCode(alias);
    final String accountingSystem = QueryDAO.getAccountingSystem(alias, "A");
    final String accountingSystemLM = QueryDAO.getAccountingSystem(alias, "AL");
    final String accountingBroker = QueryDAO.getAccountingSystem(alias, "B");
    final String accountingBrokerDescription = QueryDAO.getAccountingSystemDescription(alias, "B");

    // Obtenc�o settMap

    String clearerSettMapGeneric = QueryDAO.getAccountClearerSettMapGeneric(alias);

    String settlementTypeCLG = "";
    String clearerCLG = "";
    String accountAtClearerCLG = "";

    if (!"".equals(clearerSettMapGeneric)) {
      clearerSettMapGeneric += "---xxx";

      final String clearerSettMapGenericValues[] = clearerSettMapGeneric.split("---");

      settlementTypeCLG = convertSettleTypes2(clearerSettMapGenericValues[0]);

      clearerCLG = clearerSettMapGenericValues[1];
      accountAtClearerCLG = clearerSettMapGenericValues[2];

    }

    String custodianSettMapGeneric = QueryDAO.getAccountCustodianSettMapGeneric(alias);
    String settlementTypeCUG = "";
    String custodianCUG = "";

    if (!"".equals(custodianSettMapGeneric)) {
      custodianSettMapGeneric += "---xxx";

      final String custodianSettMapGenericValues[] = custodianSettMapGeneric.split("---");

      settlementTypeCUG = convertSettleTypes2(custodianSettMapGenericValues[0]);

      custodianCUG = custodianSettMapGenericValues[1];

    }

    outCommand += "$('#broClient').val(\"" + broClient + "\");";
    outCommand += "$('#alias').val(\"" + alias + "\");";

    outCommand += "$('#allocationType').val(\"" + allocationTypeConverted + "\");";
    outCommand += "$('#expectedAllocation').val(\"" + expectedAllocationConverted + "\");";
    outCommand += "$('#marketAllocationRule').val(\"" + marketAllocationRuleConverted + "\");";
    outCommand += "$('#confirmationType').val(\"" + confirmationTypeConverted + "\");";
    outCommand += "$('#calculationType').val(\"" + calculationTypeConverted + "\");";
    outCommand += "$('#calculationTypeLM').val(\"" + calculationTypeLMConverted + "\");";

    outCommand += "$('#forOwnAccount').prop('checked'," + forOwnAccount + ");";
    outCommand += "$('#payMarket').prop('checked'," + payMarket + ");";
    outCommand += "$('#reconcileAllocation').prop('checked'," + reconcileAllocation + ");";
    outCommand += "$('#reconcileAllocationLM').prop('checked'," + reconcileAllocationLM + ");";
    outCommand += "$('#reconcileConfirmation').prop('checked'," + reconcileConfirmation + ");";
    outCommand += "$('#reconcileConfirmationLM').prop('checked'," + reconcileConfirmationLM + ");";
    outCommand += "$('#requireAllocation').prop('checked'," + requireAllocation + ");";
    outCommand += "$('#requireAllocationLM').prop('checked'," + requireAllocationLM + ");";
    outCommand += "$('#requireAllocConfirmation').prop('checked'," + requireAllocConfirmation + ");";
    outCommand += "$('#requireConfirmation').prop('checked'," + requireConfirmation + ");";
    outCommand += "$('#stp').prop('checked'," + stp + ");";
    outCommand += "$('#exonerated').prop('checked'," + exonerated + ");";
    outCommand += "$('#manualRetrocession').prop('checked'," + manualRetrocession + ");";
    outCommand += "$('#routing').prop('checked'," + routing + ");";
    outCommand += "$('#requireRetrocession').prop('checked'," + requireRetrocession + ");";

    outCommand += "$('#traders').val(\"" + trader + "\");";
    outCommand += "$('#sales').val(\"" + sale + "\");";
    outCommand += "$('#managers').val(\"" + manager + "\");";

    outCommand += "$('#statisticGroup').val(\"" + statisticGroup + "\");";

    outCommand += "$('#statisticLevel0').val(\"" + statisticLevel0 + "\");";

    outCommand += "$('#statisticLevel1').find('option').remove();";
    final String statisticLevel1Value = (QueryDAO.getStatisticLevel1(statisticLevel0)).get(statisticLevel1);
    outCommand += "$('<option>').val(\"" + statisticLevel1 + "\").text(\"" + statisticLevel1Value
        + "\").appendTo($('#statisticLevel1'));";

    outCommand += "$('#statisticLevel2').find('option').remove();";
    final String statisticLevel2Value = (QueryDAO.getStatisticLevel2(statisticLevel1)).get(statisticLevel2);
    outCommand += "$('<option>').val(\"" + statisticLevel2 + "\").text(\"" + statisticLevel2Value
        + "\").appendTo($('#statisticLevel2'));";

    outCommand += "$('#statisticLevel3').find('option').remove();";
    final String statisticLevel3Value = ("".equals(statisticLevel2)) ? "" : (QueryDAO
        .getStatisticLevel3(statisticLevel2)).get(statisticLevel3);
    outCommand += "$('<option>').val(\"" + statisticLevel3 + "\").text(\"" + statisticLevel3Value
        + "\").appendTo($('#statisticLevel3'));";

    outCommand += "$('#statisticLevel4').find('option').remove();";
    final String statisticLevel4Value = (QueryDAO.getStatisticLevel4(statisticLevel3)).get(statisticLevel4);
    outCommand += "$('<option>').val(\"" + statisticLevel4 + "\").text(\"" + statisticLevel4Value
        + "\").appendTo($('#statisticLevel4'));";

    outCommand += "$('#tradersLM').val(\"" + traderLM + "\");";
    outCommand += "$('#salesLM').val(\"" + saleLM + "\");";
    outCommand += "$('#managersLM').val(\"" + managerLM + "\");";

    outCommand += "$('#statisticGroupLM').val(\"" + statisticGroupLM + "\");";

    outCommand += "$('#statisticLevel0LM').val(\"" + statisticLevel0LM + "\");";

    outCommand += "$('#statisticLevel1LM').find('option').remove();";
    final String statisticLevel1ValueLM = (QueryDAO.getStatisticLevel1(statisticLevel0LM)).get(statisticLevel1LM);
    outCommand += "$('<option>').val(\"" + statisticLevel1LM + "\").text(\"" + statisticLevel1ValueLM
        + "\").appendTo($('#statisticLevel1LM'));";

    outCommand += "$('#statisticLevel2LM').find('option').remove();";
    final String statisticLevel2ValueLM = (QueryDAO.getStatisticLevel2(statisticLevel1LM)).get(statisticLevel2LM);
    outCommand += "$('<option>').val(\"" + statisticLevel2LM + "\").text(\"" + statisticLevel2ValueLM
        + "\").appendTo($('#statisticLevel2LM'));";

    outCommand += "$('#statisticLevel3LM').find('option').remove();";
    final String statisticLevel3ValueLM = (QueryDAO.getStatisticLevel3(statisticLevel2LM)).get(statisticLevel3LM);
    outCommand += "$('<option>').val(\"" + statisticLevel3LM + "\").text(\"" + statisticLevel3ValueLM
        + "\").appendTo($('#statisticLevel3LM'));";

    outCommand += "$('#statisticLevel4LM').find('option').remove();";
    final String statisticLevel4ValueLM = (QueryDAO.getStatisticLevel4(statisticLevel3LM)).get(statisticLevel4LM);
    outCommand += "$('<option>').val(\"" + statisticLevel4LM + "\").text(\"" + statisticLevel4ValueLM
        + "\").appendTo($('#statisticLevel4LM'));";

    outCommand += "$('#description').val(\"" + description + "\");";
    outCommand += "$('#oasysCode').val(\"" + oasysCode + "\");";
    outCommand += "$('#accountingSystem').val(\"" + accountingSystem + "\");";
    outCommand += "$('#accountingSystemLM').val(\"" + accountingSystemLM + "\");";
    outCommand += "$('#accountingBroker').val(\"" + accountingBroker + "\");";
    outCommand += "$('#accountingBrokerDescription').val(\"" + accountingBrokerDescription + "\");";

    if (!"".equals(clearerSettMapGeneric)) {
      outCommand += "$('#settlementTypeCLG').val(\"" + settlementTypeCLG + "\");";
      outCommand += "$('#clearerCLG').val(\"" + clearerCLG + "\");";
      outCommand += "$('#accountAtClearerCLG').val(\"" + accountAtClearerCLG + "\");";
    }

    if (!"".equals(custodianSettMapGeneric)) {
      outCommand += "$('#settlementTypeCUG').val(\"" + settlementTypeCUG + "\");";
      outCommand += "$('#custodianCUG').val(\"" + custodianCUG + "\");";

    }

    // Obtención de los settMap por mercado

    outCommand += getAliasSettMap(alias);
    outCommand += getAliasMifidFields(alias);

    /*
     * settlementTypeCLG settlementTypeCLM clearerCLG clearerCLM
     * accountAtClearerCLG accountAtClearerCLM marketCLM
     * 
     * settlementTypeCUG settlementTypeCUM
     * 
     * custodianCUG custodianCUM
     * 
     * marketCUM
     */

    return outCommand;

  }

  private String getAliasMifidFields(String alias) throws SQLException {
    String out = "";
    
    HashMap<String, String> mifidAliasdata =  QueryDAO.getAliasMifidFields(alias);
    
    final List<String> checkedFields = Arrays.asList("delegaTransmMifid", "poderRepresentacion", "contratoBasico", "consentOtc","consentPublVolTot");
    
    for (Map.Entry<String, String> entry : mifidAliasdata.entrySet()) {
      final String clave = entry.getKey();
      final String value = entry.getValue();
      
      
      if (checkedFields.contains(clave))
          {
      out += "$('#"+clave+"').prop('checked'," + !("N".equals(value)) + ");";
          }
      else
      {
      out += "$('#"+clave+"').val(\"" + value + "\");";
      }
            
      
      
  }
    
    out += "validaCamposMifid();";
    
    
    
    
     return out;
  }

  private String getAliasSettMap(String alias) throws SQLException {
    String out = "";
    out += getAliasSettMapClearer(alias);
    out += getAliasSettMapCustodian(alias);

    return out;
  }

  private String getAliasSettMapClearer(String alias) throws SQLException {

    String out = "";

    final List<String> list = QueryDAO.getAccountClearerSettMapMercad(alias);
    // String clearerSettMapMercad =
    // QueryDAO.getAccountClearerSettMapMercad(alias);

    for (int i = 1; i <= 10; i++)
      out += "$('#clearerMercadoAddMarketCUM" + i + "').click();";

    int count = 0;
    for (String clearerSettMapMercad : list) {

      String settlementTypeCLM = "";
      String clearerCLM = "";
      String accountAtClearerCLM = "";
      String marketCLM = "";

      if (!"".equals(clearerSettMapMercad)) {
        clearerSettMapMercad += "---xxx";

        final String clearerSettMapMercadValues[] = clearerSettMapMercad.split("---");

        settlementTypeCLM = convertSettleTypes2(clearerSettMapMercadValues[0]);

        clearerCLM = clearerSettMapMercadValues[1];
        accountAtClearerCLM = clearerSettMapMercadValues[2];
        marketCLM = clearerSettMapMercadValues[3];

      }

      if (!"".equals(clearerSettMapMercad)) {
        count++;
        out += "$('#clearerMercadoAddMarketCUM').click();";

        out += "$('#settlementTypeCLM" + count + "').val(\"" + settlementTypeCLM + "\");";
        out += "$('#clearerCLM" + count + "').val(\"" + clearerCLM + "\");";
        out += "$('#accountAtClearerCLM" + count + "').val(\"" + accountAtClearerCLM + "\");";
        out += "$('#marketCLM" + count + "').val(\"" + marketCLM + "\");";
      }
    }
    return out;
  }

  private String getAliasSettMapCustodian(String alias) throws SQLException {
    String out = "";

    List<String> list = QueryDAO.getAccountCustodianSettMapMercad(alias);
    // String custodianSettMapMercad =
    // QueryDAO.getAccountCustodianSettMapMercad(alias);

    for (int i = 1; i <= 10; i++)
      out += "$('#custodioMercadoAddMarketCUM" + i + "').click();";

    int count = 0;
    for (String custodianSettMapMercad : list) {
      String settlementTypeCUM = "";
      String custodianCUM = "";
      String marketCUM = "";

      if (!"".equals(custodianSettMapMercad)) {
        custodianSettMapMercad += "---xxx";

        final String custodianSettMapMercadValues[] = custodianSettMapMercad.split("---");

        settlementTypeCUM = convertSettleTypes2(custodianSettMapMercadValues[0]);

        custodianCUM = custodianSettMapMercadValues[1];
        marketCUM = custodianSettMapMercadValues[2];

      }

      if (!"".equals(custodianSettMapMercad)) {
        count++;
        out += "$('#custodioMercadoAddMarketCUM').click();";

        out += "$('#settlementTypeCUM" + count + "').val(\"" + settlementTypeCUM + "\");";
        out += "$('#custodianCUM" + count + "').val(\"" + custodianCUM + "\");";
        out += "$('#marketCUM" + count + "').val(\"" + marketCUM + "\");";
      }

    }

    return out;

  }

  /* ~~~~~~~~~~~~~~~~~~~~ OPERACIONES PANTALLA SUBACCOUNT ~~~~~~~~~~~~~~~~~~~~ */

  private String getBooleanValue(String in) {
    String out = "false";
    if ("1".equals(in))
      out = "true";
    return out;
  }

  /**
   * Procesa las peticiones de operaciones sobre SubAccount.
   * 
   * @param request
   * @param response
   */
  private Map<String, String> processSubAccount(Properties config, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    HashMap<String, String> salida = null;
    try {
      salida = new HashMap<String, String>();

      log.debug(" # processSubAccount # ");

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processAlias #  Search");

        final String subAccount = request.getParameter("identifier");

        String out = QueryDAO.getSubAccount(subAccount);
        String command = ("".equals(out)) ? "" : getSubAccountCommand(out);

        salida.put("success", "true");
        salida.put("message", command);
      }
      else { // New
        log.debug(" # processSubAccount #  New");

        Map<String, String[]> parametros = request.getParameterMap();

        // LLamada al m�todo de Generar xmls de Entities
        final SubAccountGenerator subAccountGenerator = new SubAccountGenerator(config, parametros);
        subAccountGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

      }

    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar una subcuenta (" + request.getParameter("identifier") + ")", e);
      salida.put("success", "false");
      final String exceptionMessage = e.getLocalizedMessage();
      if (exceptionMessage == null) {
        salida.put("errorMessage", "Error ..." + e.getCause().getMessage());
        log.info("Error en process subAccount" + e.getCause().getMessage());
      }

      else {
        salida.put("errorMessage", "Error ..." + exceptionMessage);
        log.info("Error en process subAccount" + exceptionMessage);
      }

    }

    return salida;
  } // processSubAccount

  private String getSubAccountCommand(String out) throws ParseException, SQLException {
    String outCommand = "";

    out += "---xxx";

    final String[] commands = out.split("---");

    final String identifier = commands[0].trim();
    final String alias = commands[1].trim();
    final String broClient = commands[2].trim();
    final String name = commands[3].trim();
    final String alertCode = commands[4].trim();

    // Obtenc�o settMap

    String clearerSettMapGeneric = QueryDAO.getSubAccountClearerSettMapGeneric(identifier);

    String settlementTypeCLG = "";
    String clearerCLG = "";
    String accountAtClearerCLG = "";

    if (!"".equals(clearerSettMapGeneric)) {
      clearerSettMapGeneric += "---xxx";

      final String clearerSettMapGenericValues[] = clearerSettMapGeneric.split("---");

      settlementTypeCLG = convertSettleTypes2(clearerSettMapGenericValues[0]);

      clearerCLG = clearerSettMapGenericValues[1];
      accountAtClearerCLG = clearerSettMapGenericValues[2];

    }

    String custodianSettMapGeneric = QueryDAO.getSubAccountCustodianSettMapGeneric(identifier);

    String settlementTypeCUG = "";
    String custodianCUG = "";

    if (!"".equals(custodianSettMapGeneric)) {
      custodianSettMapGeneric += "---xxx";

      final String custodianSettMapGenericValues[] = custodianSettMapGeneric.split("---");

      settlementTypeCUG = convertSettleTypes2(custodianSettMapGenericValues[0]);

      custodianCUG = custodianSettMapGenericValues[1];

    }

    outCommand += "$('#identifier').val(\"" + identifier + "\");";
    outCommand += "$('#broClient').val(\"" + broClient + "\");";
    outCommand += "$('#alertCode').val(\"" + alertCode + "\");";
    outCommand += "$('#name').val(\"" + name + "\");";
    outCommand += "$('#alias').val(\"" + alias + "\");";

    if (!"".equals(clearerSettMapGeneric)) {
      outCommand += "$('#settlementTypeCLG').val(\"" + settlementTypeCLG + "\");";
      outCommand += "$('#clearerCLG').val(\"" + clearerCLG + "\");";
      outCommand += "$('#accountAtClearerCLG').val(\"" + accountAtClearerCLG + "\");";
    }

    if (!"".equals(custodianSettMapGeneric)) {
      outCommand += "$('#settlementTypeCUG').val(\"" + settlementTypeCUG + "\");";
      outCommand += "$('#custodianCUG').val(\"" + custodianCUG + "\");";

    }

    outCommand += getSubAccountSettMap(identifier);

    return outCommand;

  }

  private String getSubAccountSettMap(String cdsubcta) throws SQLException {
    String out = "";
    out += getSubAccountSettMapClearer(cdsubcta);
    out += getSubAccountSettMapCustodian(cdsubcta);

    return out;
  }

  private String getSubAccountSettMapClearer(String cdsubcta) throws SQLException {

    String out = "";

    final List<String> list = QueryDAO.getSubAccountClearerSettMapMercad(cdsubcta);
    // String clearerSettMapMercad =
    // QueryDAO.getAccountClearerSettMapMercad(alias);

    for (int i = 1; i <= 10; i++)
      out += "$('#clearerMercadoAddMarketCUM" + i + "').click();";

    int count = 0;
    for (String clearerSettMapMercad : list) {

      String settlementTypeCLM = "";
      String clearerCLM = "";
      String accountAtClearerCLM = "";
      String marketCLM = "";

      if (!"".equals(clearerSettMapMercad)) {
        clearerSettMapMercad += "---xxx";

        final String clearerSettMapMercadValues[] = clearerSettMapMercad.split("---");

        settlementTypeCLM = convertSettleTypes2(clearerSettMapMercadValues[0]);

        clearerCLM = clearerSettMapMercadValues[1];
        accountAtClearerCLM = clearerSettMapMercadValues[2];
        marketCLM = clearerSettMapMercadValues[3];

      }

      if (!"".equals(clearerSettMapMercad)) {
        count++;
        out += "$('#clearerMercadoAddMarketCUM').click();";

        out += "$('#settlementTypeCLM" + count + "').val(\"" + settlementTypeCLM + "\");";
        out += "$('#clearerCLM" + count + "').val(\"" + clearerCLM + "\");";
        out += "$('#accountAtClearerCLM" + count + "').val(\"" + accountAtClearerCLM + "\");";
        out += "$('#marketCLM" + count + "').val(\"" + marketCLM + "\");";
      }
    }
    return out;
  }

  private String getSubAccountSettMapCustodian(String cdsubcta) throws SQLException {
    String out = "";

    List<String> list = QueryDAO.getSubAccountCustodianSettMapMercad(cdsubcta);
    // String custodianSettMapMercad =
    // QueryDAO.getAccountCustodianSettMapMercad(alias);

    for (int i = 1; i <= 10; i++)
      out += "$('#custodioMercadoAddMarketCUM" + i + "').click();";

    int count = 0;
    for (String custodianSettMapMercad : list) {
      String settlementTypeCUM = "";
      String custodianCUM = "";
      String marketCUM = "";

      if (!"".equals(custodianSettMapMercad)) {
        custodianSettMapMercad += "---xxx";

        final String custodianSettMapMercadValues[] = custodianSettMapMercad.split("---");

        settlementTypeCUM = convertSettleTypes2(custodianSettMapMercadValues[0]);

        custodianCUM = custodianSettMapMercadValues[1];
        marketCUM = custodianSettMapMercadValues[2];

      }

      if (!"".equals(custodianSettMapMercad)) {
        count++;
        out += "$('#custodioMercadoAddMarketCUM').click();";

        out += "$('#settlementTypeCUM" + count + "').val(\"" + settlementTypeCUM + "\");";
        out += "$('#custodianCUM" + count + "').val(\"" + custodianCUM + "\");";
        out += "$('#marketCUM" + count + "').val(\"" + marketCUM + "\");";
      }

    }

    return out;

  }

  private String getEntityCommand(String out) throws ParseException, SQLException {
    String outCommand = "";

    out += "---xxx";

    final String[] commands = out.split("---");

    final String cdentro1 = commands[0].trim();
    final String cdentro2 = commands[1].trim();

    String type = "BroClient";

    if (cdentro1 != null && "C".equals(cdentro1)) {
      if (cdentro2 != null && "B".equals(cdentro2))
        type = "Broker";
      else
        type = "Clearer";

    }

    final String identifier = commands[2].trim();
    final String name = commands[3].trim();
    final String bic = commands[4].trim();
    final String residence = commands[5].trim();
    final String nationality = commands[6].trim();

    final String nudnicif = QueryDAO.getOtherIdentities("F", identifier);
    final String dwhCode = QueryDAO.getOtherIdentities("D", identifier);

    outCommand += "$('#type').val(\"" + type + "\");";
    outCommand += "$('#identifier').val(\"" + identifier + "\");";
    outCommand += "$('#name').val(\"" + name + "\");";
    outCommand += "$('#bic').val(\"" + bic + "\");";
    outCommand += "$('#residence').val(\"" + residence + "\");";
    outCommand += "$('#nationality').val(\"" + nationality + "\");";
    outCommand += "$('#nudnicif').val(\"" + nudnicif + "\");";
    outCommand += "$('#dwhCode').val(\"" + dwhCode + "\");";

    if ("BroClient".equals(type)) {

      final String entityType = commands[7].trim();

      String outAddress = QueryDAO.getNewEntityAddress(identifier);

      if (outAddress.equals("")) {
        outAddress = "------------";
      }

      outAddress += "---xxx";

      final String[] commands2 = outAddress.split("---");

      final String street = commands2[0];
      final String streetNumber = commands2[1];
      final String city = commands2[2];
      String provinceCode = commands2[3];
      final String zipCode = commands2[4];
      String province = "";

      if (nationality.equalsIgnoreCase("ES")) {
        if (provinceCode != null && provinceCode.length() > 1) {
          provinceCode = provinceCode.substring(0, 2);
          province = QueryDAO.getProvinceName(provinceCode);

        }
      }
      else {
        province = QueryDAO.getProvinceNameForeign(nationality);
      }

      final String kgrCode = QueryDAO.getOtherIdentities("K", identifier);

      outCommand += "$('#street').val(\"" + street + "\");";
      outCommand += "$('#streetNumber').val(\"" + streetNumber + "\");";
      outCommand += "$('#city').val(\"" + city + "\");";
      outCommand += "$('#province').val(\"" + province + "\");";
      outCommand += "$('#zipCode').val(\"" + zipCode + "\");";
      outCommand += "$('#kgrCode').val(\"" + kgrCode + "\");";
      outCommand += "$('#entityType').val(\"" + entityType + "\");";

    }

    return outCommand;

  }

  private String getBroclientCommand(String out) throws ParseException, SQLException {
    String outCommand = "";

    out += "---xxx";

    final String[] commands = out.split("---");

    final String identifier = commands[0].trim();
    final String residenceType = QueryDAO.getOriginalBroclientValue("TPRESIDE", commands[1].trim());
    final String managementCategory = QueryDAO.getOriginalBroclientValue("TPMANCAT", commands[2].trim());
    final String fiscalCategory = commands[3].trim();
    final String bankReference = commands[4].trim();
    final String mifidCategory = QueryDAO.getOriginalBroclientValue("CTGMIFID", commands[5].trim());
    final String clientNature = QueryDAO.getOriginalBroclientValue("TPCLIENN", commands[6].trim());

    outCommand += "$('#identifier').val(\"" + identifier + "\");";
    outCommand += "$('#residenceType').val(\"" + residenceType + "\");";
    outCommand += "$('#managementCategory').val(\"" + managementCategory + "\");";
    outCommand += "$('#fiscalCategory').val(\"" + fiscalCategory + "\");";
    outCommand += "$('#bankReference').val(\"" + bankReference + "\");";
    outCommand += "$('#miFIDCategory').val(\"" + mifidCategory + "\");";
    outCommand += "$('#clientNature').val(\"" + clientNature + "\");";

    return outCommand;

  }

  /**
   * Devuelve la lista de BroClientS disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasBroClient() {
    HashMap<String, String> aliasBroClientList = new HashMap<String, String>();

    try {
      /*
       * broClientIdsList = new HashMap<String, String>();
       * 
       * aliasBroClientList.put("LM", "Lionel Messi");
       * aliasBroClientList.put("CR", "Cristiano Ronaldo");
       * aliasBroClientList.put("DM", "Diego Maradona");
       */
      aliasBroClientList = QueryDAO.getBroClientList();

      // aliasBroClientList.put("success", "true");

    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de broclients disponibles", e);
      aliasBroClientList.put("success", "false");
      aliasBroClientList.put("errorMessage", "Error ...");
    }
    finally {

    }
    return Utils.orderMapByValues(aliasBroClientList);
  } // getAliasBroClient

  /**
   * Devuelve el alias.
   * 
   * @return <code>HashMap<String, String> - </code>Alias.
   */
  private synchronized Map<String, String> getAliasAlias() {
    HashMap<String, String> aliasAlias = new HashMap<String, String>();

    try {

      // aliasAlias.put("alias", "Alias recibido");
      String cdaliass = QueryDAO.getAliasId();
      aliasAlias.put("alias", cdaliass);

      // Si ha ido bien
      aliasAlias.put("success", "true");
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener el alias", e);

      // Si ha ido mal
      aliasAlias.put("success", "false");
      aliasAlias.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(aliasAlias);
  } // getAliasAlias

  // GET_DWH_ENTITIES_ID
  private synchronized Map<String, String> getDwhCode() {

    HashMap<String, String> dwhCodeMap = new HashMap<String, String>();

    try {

      // aliasAlias.put("alias", "Alias recibido");
      String dwhCode = QueryDAO.getDwhCode();
      dwhCodeMap.put("dwhCode", dwhCode);

      // Si ha ido bien
      dwhCodeMap.put("success", "true");
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener DWH Code", e);

      // Si ha ido mal
      dwhCodeMap.put("success", "false");
      dwhCodeMap.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(dwhCodeMap);
  }

  /**
   * Devuelve el subaccount.
   * 
   * @return <code>HashMap<String, String> - </code>Subaccount.
   */
  private synchronized Map<String, String> getSubAccountId() {

    HashMap<String, String> subAccountSubAccount = new HashMap<String, String>();

    try {

      // aliasAlias.put("alias", "Alias recibido");
      String subAccountId = QueryDAO.getSubAccountId();
      subAccountSubAccount.put("subAccountId", subAccountId);

      // Si ha ido bien
      subAccountSubAccount.put("success", "true");
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener el id de una subcuenta", e);

      // Si ha ido mal
      subAccountSubAccount.put("success", "false");
      subAccountSubAccount.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(subAccountSubAccount);
  } // getAliasAlias

  /**
   * Devuelve la lista de traders disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasTraders() {
    HashMap<String, String> aliasTradersList = new HashMap<String, String>();

    try {

      /*
       * aliasTradersList.put("AT1", "Alias Trader 1");
       * aliasTradersList.put("AT2", "Alias Trader 2");
       * aliasTradersList.put("AT3", "Alias Trader 3");
       */
      aliasTradersList = QueryDAO.getAliasTradersList();

      // Si ha ido bien
      aliasTradersList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Error al obtener los traders disponibles", e);

      // Si ha ido mal
      aliasTradersList.put("success", "false");
      aliasTradersList.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(aliasTradersList);
  } // getAliasTraders

  /**
   * Devuelve la lista de sales disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasSales() {
    HashMap<String, String> aliasSalesList = new HashMap<String, String>();

    try {

      aliasSalesList = QueryDAO.getAliasSalesList();

      // Si ha ido bien
      aliasSalesList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Hubo un error al obtener los alias disponibles", e);

      // Si ha ido mal
      aliasSalesList.put("success", "false");
      aliasSalesList.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(aliasSalesList);
  } // getAliasSales

  
  /**
   * Devuelve la lista de tipos de Alias disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getTipoAlias() {
    HashMap<String, String> tipoAliasList = new HashMap<String, String>();

    try {

      tipoAliasList = QueryDAO.getTipoAliasList();

      // Si ha ido bien
      tipoAliasList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Hubo un error al obtener los tipos de alias disponibles", e);

      // Si ha ido mal
      tipoAliasList.put("success", "false");
      tipoAliasList.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(tipoAliasList);
  } // getTipoAlias

  
  
  /**
   * Devuelve la lista de managers disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasManagers() {
    HashMap<String, String> aliasManagersList = new HashMap<String, String>();

    try {

      aliasManagersList = QueryDAO.getAliasManagersList();

      // Si ha ido bien
      aliasManagersList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Hubo un error al obtener los managers disponibles", e);

      // Si ha ido mal
      aliasManagersList.put("success", "false");
      aliasManagersList.put("errorMessage", "Error ...");
    }
    return Utils.orderMapByValues(aliasManagersList);
  } // getAliasManagers

  /**
   * Devuelve la lista de traders LM disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasTradersLM() {
    HashMap<String, String> aliasTradersLMList = new HashMap<String, String>();

    try {

      /*
       * aliasTradersList.put("AT1", "Alias Trader LM 1");
       * aliasTradersList.put("AT2", "Alias Trader LM 2");
       * aliasTradersList.put("AT3", "Alias Trader LM 3");
       */
      aliasTradersLMList = QueryDAO.getAliasTradersLMList();

      // Si ha ido bien
      aliasTradersLMList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de traders LM", e);

      // Si ha ido mal
      aliasTradersLMList.put("success", "false");
      aliasTradersLMList.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(aliasTradersLMList);
  } // getAliasTradersLM

  /**
   * Devuelve la lista de sales LM disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasSalesLM() {
    HashMap<String, String> aliasSalesLMList = new HashMap<String, String>();

    try {

      aliasSalesLMList = QueryDAO.getAliasSalesLMList();

      // Si ha ido bien
      aliasSalesLMList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Hubo un error al obtener los sales LM", e);

      // Si ha ido mal
      aliasSalesLMList.put("success", "false");
      aliasSalesLMList.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(aliasSalesLMList);
  } // getAliasSalesLM

  /**
   * Devuelve la lista de managers LM disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BroClientS.
   */
  private Map<String, String> getAliasManagersLM() {
    HashMap<String, String> aliasManagersLMList = new HashMap<String, String>();

    try {

      aliasManagersLMList = QueryDAO.getAliasManagerslMList();

      // Si ha ido bien
      aliasManagersLMList.put("success", "true");
    }

    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de managers LM", e);

      // Si ha ido mal
      aliasManagersLMList.put("success", "false");
      aliasManagersLMList.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(aliasManagersLMList);
  } // getAliasManagersLM

  /* ~~~~~~~~~~~ OPERACIONES PANTALLA ALIAS-SUBACCOUNT RELATIONSHIP ~~~~~~~~~~~ */

  /**
   * Devuelve el alias.
   * 
   * @return <code>HashMap<String, String> - </code>Alias.
   */
  private Map<String, String> getAsrAlias() {
    HashMap<String, String> asrAlias = new HashMap<String, String>();

    asrAlias.put("ASRA1", "Alias recibido 1");
    asrAlias.put("ASRA2", "Alias recibido 2");
    asrAlias.put("ASRA3", "Alias recibido 3");

    // Si ha ido bien
    asrAlias.put("success", "true");

    // Si ha ido mal
    // aliasAlias.put("success", "false");
    // aliasAlias.put("errorMessage", "Error ...");

    return Utils.orderMapByValues(asrAlias);
  } // getAsrAlias

  /**
   * Devuelve el subaccount.
   * 
   * @param alias
   * 
   * @return <code>HashMap<String, String> - </code>Subaccount.
   */
  private Map<String, String> getAsrSubaccounts(String alias) {
    HashMap<String, String> asrSubaccount = new HashMap<String, String>();

    /*
     * asrSubaccount.put("ASRS1", "Subaccount recibido 1");
     * asrSubaccount.put("ASRS2", "Subaccount recibido 2");
     * asrSubaccount.put("ASRS3", "Subaccount recibido 3");
     */

    try {

      asrSubaccount = QueryDAO.getSubaccountAvailableList(alias);
    }

    // Si ha ido bien
    // asrSubaccount.put("success", "true");

    catch (Exception ex) {
      log.warn("Error al obtener la subcuenta para el alias: " + alias, ex);
      // Si ha ido mal
      asrSubaccount.put("success", "false");
      asrSubaccount.put("errorMessage", "Error ...");
    }

    return Utils.orderMapByValues(asrSubaccount);
  } // getAsrSubaccount

  /**
   * Procesa las peticiones de operaciones sobre entidades.
   * 
   * @param request
   * @param response
   */
  private Map<String, String> processAsr(Properties config, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    HashMap<String, String> salida = null;

    try {
      salida = new HashMap<String, String>();

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processAsr #  Search");
        salida.put("success", "true");
      }
      else { // New
        log.debug(" # processAliasSubbaccountRelation #  New");

        log.debug("ruta servlet es " + getServletContext().getRealPath("/"));

        Map<String, String[]> parametros = request.getParameterMap();

        // LLamada al m�todo de Generar xmls de Entities
        final AliasSubAccountGenerator aliasSubAccountGenerator = new AliasSubAccountGenerator(config, parametros);
        aliasSubAccountGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

        // salida.put("success", "false");
        // salida.put("errorMessage", "error");

      } // else
    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar un asr", e);
      salida.put("success", "false");
      salida.put("errorMessage", e.getLocalizedMessage());
      log.info(e.getLocalizedMessage());
    }
    finally {
      // TODO
    } // finally

    return salida;
  } // processEntity

  /**
   * Procesa las peticiones de operaciones sobre entidades.
   * 
   * @param request
   * @param response
   */
  private Map<String, String> processAsr2(Properties config, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    HashMap<String, String> salida = null;

    try {
      salida = new HashMap<String, String>();

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processAsr #  Search");
        salida.put("success", "true");
      }
      else { // New
        log.debug(" # processAliasSubbaccountRelation #  New");

        log.debug("ruta servlet es " + getServletContext().getRealPath("/"));

        Map<String, String[]> parametros = request.getParameterMap();

        // LLamada al m�todo de Generar xmls de Entities
        final AliasSubAccountGenerator2 aliasSubAccountGenerator = new AliasSubAccountGenerator2(config, parametros);
        aliasSubAccountGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

        // salida.put("success", "false");
        // salida.put("errorMessage", "error");

      } // else
    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar un asr", e);
      salida.put("success", "false");
      salida.put("errorMessage", e.getLocalizedMessage());
      log.info(e.getLocalizedMessage());
    }
    finally {
      // TODO
    } // finally

    return salida;
  } // processEntity

  /* ~~~~~~~~~~~ OPERACIONES PANTALLA SETTLEMENT INSTRUCTIONS ~~~~~~~~~~~ */

  /**
   * Devuelve la lista de alias de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de alias de si.
   */
  private Map<String, String> getSettleInstAlias() {
    HashMap<String, String> settleInstAlias = null;

    try {
      /*
       * settleInstAlias.put("SIA1", "SettleInstAlias 1");
       * settleInstAlias.put("SIA2", "SettleInstAlias 2");
       * settleInstAlias.put("SIA3", "SettleInstAlias 3");
       */

      settleInstAlias = QueryDAO.getAliasList();

      // settleInstAlias.put("", "Empty");
      // settleInstAlias.put("success", "true");

    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de alias para SI", e);
      settleInstAlias.put("success", "false");
      settleInstAlias.put("errorMessage", "Error ...");
    }
    finally {

    } // finally

    return Utils.orderMapByValues(settleInstAlias);
  } // getSettleInstAlias

  /**
   * Devuelve la lista de subcuentas de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de subcuentas de si.
   */
  private Map<String, String> getSettleInstSubaccount(String alias) {
    HashMap<String, String> settleInstSubaccount = null;

    try {
      /*
       * settleInstAlias.put("SIA1", "SettleInstSubAccount 1");
       * settleInstAlias.put("SIA2", "SettleInstSubAccount 2");
       * settleInstAlias.put("SIA3", "SettleInstSubAccount 3");
       */

      settleInstSubaccount = QueryDAO.getSubaccountList(alias);

      // settleInstSubaccount.put("", "Empty");
      // settleInstSubaccount.put("success", "true");

    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de subcuentas para SI", e);
      settleInstSubaccount.put("success", "false");
      settleInstSubaccount.put("errorMessage", "Error ...");
    }
    finally {

    } // finally

    return Utils.orderMapByValues(settleInstSubaccount);
  } // getSettleInstSubaccount

  /**
   * Devuelve la lista de makets de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de markets de si.
   */
  private Map<String, String> getSettleInstMarket() {
    HashMap<String, String> settleInstMarket = null;

    try {
      /*
       * settleInstAlias.put("SIA1", "SettleInstMarket 1");
       * settleInstAlias.put("SIA2", "SettleInstMarket 2");
       * settleInstAlias.put("SIA3", "SettleInstMarket 3");
       */

      settleInstMarket = QueryDAO.getMarketList();

      // settleInstMarket.put("", "Empty");
      // settleInstMarket.put("success", "true");

    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de mercados para SI", e);
      settleInstMarket.put("success", "false");
      settleInstMarket.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(settleInstMarket);
  } // getSettleInstMarket

  /**
   * Devuelve la lista de CounterpartIdentifier de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de
   * CounterpartIdentifier de si.
   */
  private Map<String, String> getSettleInstCounterpartIdentifier() {
    HashMap<String, String> settleInstCounterpartIdentifier = null;

    /*
     * settleInstCounterpartIdentifier.put("SICP1", "CounterpartIdentifier 1");
     * settleInstCounterpartIdentifier.put("SICP2", "CounterpartIdentifier 2");
     * settleInstCounterpartIdentifier.put("SICP3", "CounterpartIdentifier 3");
     */

    try {
      settleInstCounterpartIdentifier = QueryDAO.getEntitiesList2();
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de Counterparts para SI", e);
      settleInstCounterpartIdentifier.put("success", "false");
      settleInstCounterpartIdentifier.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(settleInstCounterpartIdentifier);
  } // getSettleInstCounterpartIdentifier

  private Map<String, String> getEntitiesList() {
    HashMap<String, String> settleInstCounterpartIdentifier = null;

    /*
     * settleInstCounterpartIdentifier.put("SICP1", "CounterpartIdentifier 1");
     * settleInstCounterpartIdentifier.put("SICP2", "CounterpartIdentifier 2");
     * settleInstCounterpartIdentifier.put("SICP3", "CounterpartIdentifier 3");
     */

    try {
      settleInstCounterpartIdentifier = QueryDAO.getEntitiesList3();
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de entidades", e);
      settleInstCounterpartIdentifier.put("success", "false");
      settleInstCounterpartIdentifier.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(settleInstCounterpartIdentifier);
  } // getSettleInstCounterpartIdentifier

  /**
   * Devuelve la lista de BeneficiaryId de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de BeneficiaryId de
   * si.
   */
  private Map<String, String> getSettleInstBeneficiaryId(String elementCode) {
    HashMap<String, String> settleInstBeneficiaryId = new HashMap<String, String>();

    // settleInstBeneficiaryId.put("SIBId1", "Beneficiary Id 1");
    // settleInstBeneficiaryId.put("SIBId2", "Beneficiary Id 2");
    try {
      String beneficiaryId = QueryDAO.getBeneficiaryId(elementCode);
      settleInstBeneficiaryId.put("beneficiaryId", beneficiaryId);
      settleInstBeneficiaryId.put("sucess", "true");
    }
    catch (Exception ex) {
      log.warn("Hubo un error al obtener la lista de BeneficiaryIds para SI", ex);
      settleInstBeneficiaryId.put("success", "false");
      settleInstBeneficiaryId.put("errorMessage", "Error .." + ex.getLocalizedMessage());

    }

    return Utils.orderMapByValues(settleInstBeneficiaryId);
  } // getSettleInstBeneficiaryId

  /**
   * Devuelve la lista de Beneficiary de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de Beneficiary de si.
   */
  private Map<String, String> getSettleInstBeneficiary() {
    HashMap<String, String> settleInstBeneficiary = new HashMap<String, String>();

    /*
     * settleInstBeneficiary.put("SIB1", "Beneficiary 1");
     * settleInstBeneficiary.put("SIB2", "Beneficiary 2");
     * settleInstBeneficiary.put("SIB3", "Beneficiary 3");
     */

    try {
      settleInstBeneficiary = QueryDAO.getEntitiesList();
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de Beneficiary para SI", e);
      settleInstBeneficiary.put("success", "false");
      settleInstBeneficiary.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(settleInstBeneficiary);
  } // getSettleInstBeneficiary

  /**
   * Devuelve la lista de LocalCustodian Id de si disponibles.
   * 
   * @param elementCode
   * 
   * @return <code>HashMap<String, String> - </code>Lista de LocalCustodian Id
   * de si.
   */
  private Map<String, String> getSettleInstLocalCustodianId(String elementCode1, String elementCode2,
      String elementCode3, String elementCode4) {
    HashMap<String, String> settleInstLocalCustodianId = new HashMap<String, String>();

    /*
     * settleInstLocalCustodianId.put("SILCId1", "LocalCustodian Id 1");
     * settleInstLocalCustodianId.put("SILCId2", "LocalCustodian Id 2");
     * settleInstLocalCustodianId.put("SILCId3", "LocalCustodian Id 3");
     */

    try {
      //String beneficiaryId = QueryDAO.getLocalCustodianId(elementCode1, elementCode2, elementCode3, elementCode4);
      String beneficiaryId = "";
      settleInstLocalCustodianId.put("localCustodianId", beneficiaryId);      
      settleInstLocalCustodianId.put("sucess", "true");
    }
    catch (Exception ex) {
      log.warn("Hubo un error al obtener la lista de LocalCustodianIds para SI", ex);
      settleInstLocalCustodianId.put("success", "false");
      settleInstLocalCustodianId.put("errorMessage", "Error .." + ex.getLocalizedMessage());

    }

    return Utils.orderMapByValues(settleInstLocalCustodianId);
  } // getSettleInstLocalCustodianId

  /**
   * Devuelve la lista de LocalCustodian de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de LocalCustodian de
   * si.
   */
  private Map<String, String> getSettleInstLocalCustodian() {
    HashMap<String, String> settleInstLocalCustodian = new HashMap<String, String>();

    /*
     * settleInstLocalCustodian.put("SILC1", "LocalCustodian 1");
     * settleInstLocalCustodian.put("SILC2", "LocalCustodian 2");
     * settleInstLocalCustodian.put("SILC3", "LocalCustodian 3");
     */

    try {
      settleInstLocalCustodian = QueryDAO.getEntitiesList();
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de LocalCustodian para SI", e);
      settleInstLocalCustodian.put("success", "false");
      settleInstLocalCustodian.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(settleInstLocalCustodian);
  } // getSettleInstLocalCustodian

  /**
   * Devuelve la lista de GlobalCustodian Id de si disponibles.
   * 
   * @param elementCode2
   * @param elementCode1
   * 
   * @return <code>HashMap<String, String> - </code>Lista de GlobalCustodian Id
   * de si.
   */
  private Map<String, String> getSettleInstGlobalCustodianId(String elementCode1, String elementCode2) {
    HashMap<String, String> settleInstGlobalCustodianId = new HashMap<String, String>();

    // settleInstGlobalCustodianId.put("SIGCId1", "GlobalCustodian Id 1");
    // settleInstGlobalCustodianId.put("SIGCId2", "GlobalCustodian Id 2");
    // settleInstGlobalCustodianId.put("SIGCId3", "GlobalCustodian Id 3");

    try {
      String beneficiaryId = QueryDAO.getGlobalCustodianId(elementCode1, elementCode2);
      settleInstGlobalCustodianId.put("globalCustodianId", beneficiaryId);
      settleInstGlobalCustodianId.put("sucess", "true");
    }
    catch (Exception ex) {
      log.warn("Hubo un error al obtener la lista de GlobalCustodianIds para SI", ex);
      settleInstGlobalCustodianId.put("success", "false");
      settleInstGlobalCustodianId.put("errorMessage", "Error .." + ex.getLocalizedMessage());

    }

    return Utils.orderMapByValues(settleInstGlobalCustodianId);
  } // getSettleInstGlobalCustodianId

  /**
   * Devuelve la lista de GlobalCustodian de si disponibles.
   * 
   * @return <code>HashMap<String, String> - </code>Lista de GlobalCustodian de
   * si.
   */
  private Map<String, String> getSettleInstGlobalCustodian() {
    HashMap<String, String> settleInstGlobalCustodian = new HashMap<String, String>();

    /*
     * settleInstGlobalCustodian.put("SIGC1", "GlobalCustodian 1");
     * settleInstGlobalCustodian.put("SIGC2", "GlobalCustodian 2");
     * settleInstGlobalCustodian.put("SIGC3", "GlobalCustodian 3");
     */

    try {
      settleInstGlobalCustodian = QueryDAO.getEntitiesList();
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener la lista de GlobalCustodian para SI", e);
      settleInstGlobalCustodian.put("success", "false");
      settleInstGlobalCustodian.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(settleInstGlobalCustodian);
  } // getSettleInstGlobalCustodian

  /**
   * Devuelve la lista de SettlementType de si disponibles.
   * 
   * @param elementCode4
   * @param elementCode3
   * @param elementCode2
   * @param elementCode1
   * 
   * @return <code>HashMap<String, String> - </code>Lista de SettlementType de
   * si.
   */
  private Map<String, String> getSettleInstPlaceOfSettlement(String elementCode1, String elementCode2,
      String elementCode3, String elementCode4) {
    HashMap<String, String> settleInstPlaceOfSettlement = new HashMap<String, String>();

    // settleInstPlaceOfSettlement.put("SIPoS1", "PlaceOfSettlement 1");
    // settleInstPlaceOfSettlement.put("SIPoS2", "PlaceOfSettlement 2");
    // settleInstPlaceOfSettlement.put("SIPoS3", "PlaceOfSettlement 3");

    try {
      String placeOfSettlement = QueryDAO.getPlaceOfSettlement(elementCode1, elementCode2, elementCode3, elementCode4);
      settleInstPlaceOfSettlement.put("placeOfSettlement", placeOfSettlement);
      settleInstPlaceOfSettlement.put("sucess", "true");
    }
    catch (Exception ex) {
      log.warn("Hubo un error al obtener la lista de SettlementType para SI", ex);
      settleInstPlaceOfSettlement.put("success", "false");
      settleInstPlaceOfSettlement.put("errorMessage", "Error .." + ex.getLocalizedMessage());

    }

    return Utils.orderMapByValues(settleInstPlaceOfSettlement);
  } // getSettleInstPlaceOfSettlement

  private Map<String, String> getSettleInstPlaceOfSettlementNew(String elementCode1, String elementCode2) {
    HashMap<String, String> settleInstPlaceOfSettlement = new HashMap<String, String>();

    // settleInstPlaceOfSettlement.put("SIPoS1", "PlaceOfSettlement 1");
    // settleInstPlaceOfSettlement.put("SIPoS2", "PlaceOfSettlement 2");
    // settleInstPlaceOfSettlement.put("SIPoS3", "PlaceOfSettlement 3");

    try {
      final String settType = QueryDAO.getSettlementTypeValue(elementCode2);

      String settTypeNew = convertSettleTypes(settType);

      String placeOfSettlement = QueryDAO.getPlaceOfSettlementNew(elementCode1, settTypeNew);
      settleInstPlaceOfSettlement.put("placeOfSettlement", placeOfSettlement);
      settleInstPlaceOfSettlement.put("sucess", "true");
    }
    catch (Exception ex) {
      log.warn("Hubo un error al obtener el Place Of Settlement", ex);
      settleInstPlaceOfSettlement.put("success", "false");
      settleInstPlaceOfSettlement.put("errorMessage", "Error .." + ex.getLocalizedMessage());

    }

    return Utils.orderMapByValues(settleInstPlaceOfSettlement);
  } // getSettleInstPlaceOfSettlement

  private String convertSettleTypes(String settType) {
    String letter1 = settType.substring(0, 1);
    String letter2 = settType.substring(1, 2);

    return convertSettleType(letter1) + "/" + convertSettleType(letter2);
  }

  private String convertSettleType(String letter) {
    String out = "";

    if ("D".equals(letter))
      out = "DOM";
    else if ("E".equals(letter))
      out = "EOC";
    else if ("C".equals(letter))
      out = "CED";
    return out;
  }

  private String convertSettleTypes2(String settType) {
    String out = "";
    try {
      String letter1 = settType.substring(0, 1);
      String letter2 = settType.substring(1, 2);
      out = convertSettleType2(letter1) + " / " + convertSettleType2(letter2);
    }
    catch (Exception ex) {
      log.warn("Hubo un error al convertir el tipo de Settlement: " + settType, ex);
    }

    return out;
  }

  private String convertSettleType2(String letter) {
    String out = "";

    if ("D".equals(letter))
      out = "Domestic";
    else if ("E".equals(letter))
      out = "Euroclear";
    else if ("C".equals(letter))
      out = "Cedel";
    return out;
  }

  /**
   * Procesa las peticiones de operaciones sobre instrucciones de liquidaci�n.
   * 
   * @param request
   * @param response
   */

  private Map<String, String> processSettleInst(Properties config, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    HashMap<String, String> salida = null;
    try {
      salida = new HashMap<String, String>();

      log.debug(" # processSettleInst # ");

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processSettleInst #  Search");
        // TODO Se busca por olos parametros recibidos
        final String alias = request.getParameter("alias");
        final String subaccount = request.getParameter("subaccount");
        final String market = request.getParameter("market");
        final String counterpartIdentifier = request.getParameter("counterpartIdentifier");
        final String settlementType = request.getParameter("settlementType");

        final String dateFrom1 = request.getParameter("dateFrom");
        final String dateTo1 = request.getParameter("dateTo");

        final String settype = QueryDAO.getSettlementTypeValue(settlementType);

        String out = "";

        if (!"".equals(dateFrom1) && !"".equals(dateTo1)) {
          final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
          final SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

          final String dateFrom = sdf1.format(sdf2.parse(dateFrom1));
          final String dateTo = sdf1.format(sdf2.parse(dateTo1));

          out = QueryDAO.getNewSettInstByDate(alias, subaccount, market, counterpartIdentifier, settype, dateFrom,
              dateTo);
        }
        else {
          out = QueryDAO.getNewSettInst(alias, subaccount, market, counterpartIdentifier, settype);
        }

        String command = ("".equals(out)) ? "" : getSettleInstCommand(out);

        salida.put("success", "true");
        salida.put("message", command);
      }
      else if (request.getParameter("mode").equals("N") || request.getParameter("mode").equals("U")) { // New
        log.debug(" # processSettleInst #  New");

        Map<String, String[]> parametros = request.getParameterMap();

        // LLamada al m�todo de Generar xmls de Entities
        final SettInstGenerator broClientGenerator = new SettInstGenerator(config, parametros);
        broClientGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

      }
      else if (request.getParameter("mode").equals("U")) { // New
        log.debug(" # processSettleInst #  Update");

      }
      else if (request.getParameter("mode").equals("D")) { // New
        log.debug(" # processSettleInst #  Delete");

      } // else

    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar una instrcuccion de liquidacion (Alias: " + request.getParameter("alias")
          + ";Subcuenta: " + request.getParameter("subaccount") + ")", e);
      salida.put("success", "false");
      final String exceptionMessage = e.getLocalizedMessage();
      if (exceptionMessage == null) {
        salida.put("errorMessage", "Error ..." + e.getCause().getMessage());
        log.info("Error en process SettlementInstruction" + e.getCause().getMessage());
      }

      else {
        salida.put("errorMessage", "Error ..." + exceptionMessage);
        log.info("Error en process SettlementInstruction" + exceptionMessage);
      }

    }

    return salida;
  } // processSettleInst

  private String getSettleInstCommand(String out) throws ParseException {
    String outCommand = "";

    out += "---xxx";

    final String[] commands = out.split("---");

    final String identifier = commands[0];
    final String dateFrom1 = commands[1];
    final String dateTo1 = commands[2];
    final String beneficiary = commands[3];
    final String beneficiaryId = commands[4];
    final String beneficiaryAccount = commands[5];
    final String localCustodian = commands[6];
    final String localCustodianAccount = commands[8];
    final String localCustodianId = commands[7];
    final String globalCustodian = commands[9];
    final String globalCustodianAccount = commands[11];
    final String globalCustodianId = commands[10];
    final String placeOfSettlement = commands[12];

    final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
    final SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

    final String dateFrom = sdf2.format(sdf1.parse(dateFrom1));
    final String dateTo = sdf2.format(sdf1.parse(dateTo1));

    outCommand += "$('#identifier').val(\"" + identifier + "\");";
    outCommand += "$('#identifier2').val(\"" + identifier + "\");";
    outCommand += "$('#dateFrom').val(\"" + dateFrom + "\");";
    outCommand += "$('#dateTo').val(\"" + dateTo + "\");";
    outCommand += "$('#beneficiary').val(\"" + beneficiary + "\");";
    outCommand += "$('#beneficiaryAccount').val(\"" + beneficiaryAccount + "\");";
    outCommand += "$('#beneficiaryId').val(\"" + beneficiaryId + "\");";
    outCommand += "$('#globalCustodian').val(\"" + globalCustodian + "\");";
    outCommand += "$('#globalCustodianAccount').val(\"" + globalCustodianAccount + "\");";
    outCommand += "$('#globalCustodianId').val(\"" + globalCustodianId + "\");";
    outCommand += "$('#localCustodian').val(\"" + localCustodian + "\");";
    outCommand += "$('#localCustodianAccount').val(\"" + localCustodianAccount + "\");";
    outCommand += "$('#localCustodianId').val(\"" + localCustodianId + "\");";
    outCommand += "$('#placeOfSettlement').val(\"" + placeOfSettlement + "\");";

    return outCommand;

  }

  private Map<String, String> processSettMap(Properties config, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    HashMap<String, String> salida = null;
    try {
      salida = new HashMap<String, String>();

      log.debug(" # processSettMap # ");

      if (request.getParameter("mode").equals("S")) { // Search
        log.debug(" # processSettMap #  Search");
        // TODO Se busca por olos parametros recibidos

        salida.put("success", "true");
      }
      else if (request.getParameter("mode").equals("N")) { // New
        log.debug(" # processSettMap #  New");

        Map<String, String[]> parametros = request.getParameterMap();

        // String xmlPath =
        // getServletContext().getInitParameter(xmlPathParameter);

        // LLamada al m�todo de Generar xmls de Entities
        final SettMapGenerator broClientGenerator = new SettMapGenerator(config, parametros);
        broClientGenerator.generateTemplate();

        Iterator<Entry<String, String[]>> it = parametros.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry<String, String[]> e = it.next();
          log.debug("Clave: " + e.getKey());
          log.debug("Valor: " + request.getParameter(e.getKey().toString()));
          salida.put(e.getKey(), request.getParameter(e.getKey().toString()));
        } // while

        salida.put("success", "true");

      }
      else if (request.getParameter("mode").equals("U")) { // New
        log.debug(" # processSettMap #  Update");

      }
      else if (request.getParameter("mode").equals("D")) { // New
        log.debug(" # processSettMap #  Delete");

      } // else

    }
    catch (Exception e) {
      log.warn("Hubo un error al procesar un SettMap", e);
      salida.put("success", "false");
      salida.put("errorMessage", "Error ..." + e.getLocalizedMessage());
      log.info("Error procesando Settmap " + e.getLocalizedMessage());
    }

    return salida;
  } // processSettMap

  private Map<String, String> getStatisticLevel0() {
    HashMap<String, String> statisticLevels = new HashMap<String, String>();

    try {
      statisticLevels = QueryDAO.getStatisticLevel0();
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener los niveles estadisticos 0", e);
      statisticLevels.put("success", "false");
      statisticLevels.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(statisticLevels);
  }

  private Map<String, String> getStatisticLevel1(String previousLevel) {
    HashMap<String, String> statisticLevels = new HashMap<String, String>();

    try {
      statisticLevels = QueryDAO.getStatisticLevel1(previousLevel);
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener los niveles estadisticos 1", e);
      statisticLevels.put("success", "false");
      statisticLevels.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(statisticLevels);
  }

  private Map<String, String> getStatisticLevel2(String previousLevel) {
    HashMap<String, String> statisticLevels = new HashMap<String, String>();

    try {
      statisticLevels = QueryDAO.getStatisticLevel2(previousLevel);
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener los niveles estadisticos 2", e);
      statisticLevels.put("success", "false");
      statisticLevels.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(statisticLevels);
  }

  private Map<String, String> getStatisticLevel3(String previousLevel) {
    HashMap<String, String> statisticLevels = new HashMap<String, String>();

    try {
      statisticLevels = QueryDAO.getStatisticLevel3(previousLevel);
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener los niveles estadisticos 0", 3);
      statisticLevels.put("success", "false");
      statisticLevels.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(statisticLevels);
  }

  private Map<String, String> getStatisticLevel4(String previousLevel) {
    HashMap<String, String> statisticLevels = new HashMap<String, String>();

    try {
      statisticLevels = QueryDAO.getStatisticLevel4(previousLevel);
    }
    catch (Exception e) {
      log.warn("Hubo un error al obtener los niveles estadisticos 4", e);
      statisticLevels.put("success", "false");
      statisticLevels.put("errorMessage", "Error ...");
    }
    finally {

    }

    return Utils.orderMapByValues(statisticLevels);
  }

} // Controller
