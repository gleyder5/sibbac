/*
 * Establece el alto del div con el contenido de cada página.
 */
function setContentHeight() {
  var totalHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

  var appHeaderContainer = document.getElementById("app_header_container").scrollHeight;

  var appFooterContainer = 35;

  var appContentContainer = document.getElementById("content").scrollHeight;

  document.getElementById("content").style.height = (totalHeight - appHeaderContainer - appContentContainer - appFooterContainer)
      + "px";
} // setContentHeight

/*
 * Establece el ancho del div con el contenido de cada página.
 */
function setContentWidth() {
  var totalWidth = window.innerWidth || document.documentElement.clientWidth || document.body.offsetWidth;

  document.getElementById("content").style.width = (totalWidth - 40) + "px";
} // setContentWidth

/*
 * 
 */
$(document).ready(function() {

  // $.urlParam = function(url, name){
  // alert("ruta: " + url);
  // var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
  // alert("resultado: " + results);
  // if (results==null){
  // return null;
  // }
  // else{
  // return results[1] || 0;
  // }
  // }

});

/*
 * Configuación del idioma español para los input boxes de fechas
 */
$.datepicker.regional['es'] = {
  closeText : 'Cerrar',
  prevText : '<Ant',
  nextText : 'Sig>',
  currentText : 'Hoy',
  monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
      'Noviembre', 'Diciembre' ],
  monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
  dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
  dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb' ],
  dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá' ],
  weekHeader : 'Sm',
  dateFormat : 'dd/mm/yy',
  firstDay : 1,
  isRTL : false,
  showMonthAfterYear : false,
  yearSuffix : ''
};

/*
 * Se establece el idioma español por defecto para los input boxes de fechas
 */
$.datepicker.setDefaults($.datepicker.regional['es']);

/*
 * 
 */
$("input:reset").click(function() {
  this.form.reset();
  $("form").resetForm();
  return false; // prevent reset button from resetting again
});
