package cn.bluejoe.elfinder.controller.executor;


import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.bluejoe.elfinder.service.FsServiceFactory;


public class CommandExecutionContextImpl implements CommandExecutionContext {

	private FsServiceFactory	_fsServiceFactory;
	private HttpServletRequest	finalRequest;
	private HttpServletResponse	response;

	public CommandExecutionContextImpl( FsServiceFactory _fsServiceFactory, HttpServletRequest finalRequest, HttpServletResponse response ) {
		this._fsServiceFactory = _fsServiceFactory;
		this.finalRequest = finalRequest;
		this.response = response;
	}

	@Override
	public FsServiceFactory getFsServiceFactory() {
		return _fsServiceFactory;
	}

	@Override
	public HttpServletRequest getRequest() {
		return finalRequest;
	}

	@Override
	public HttpServletResponse getResponse() {
		return response;
	}

	@Override
	public ServletContext getServletContext() {
		return finalRequest.getSession().getServletContext();
	}

}
