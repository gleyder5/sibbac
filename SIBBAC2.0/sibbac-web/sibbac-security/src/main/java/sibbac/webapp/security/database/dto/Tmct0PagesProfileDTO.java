package sibbac.webapp.security.database.dto;

import java.util.List;

/**
 * DTO para la gestion de las paginas y las acciones dentro de las mismas a las que tiene acceso un perfil
 * 
 * @author Neoris
 *
 */
public class Tmct0PagesProfileDTO {

  /** idPage - Identificador de la página */
  private Long idPage;

  /** idProfile - Identificador deL Perfil */
  private Long idProfile;

  /** name - Nombre de la página */
  private String name;

  /** description - Descripcion de la página */
  private String description;

  /**
   * listaAccionesPaginaProfile - Lista con todas las paginas y sus acciones indicando si puede o no acceder a su
   * funcionalidad el perfil
   */
  private List<Tmct0ActionPageProfileDTO> listaAccionesPaginaProfile;

  public Tmct0PagesProfileDTO() {
  }

  public Tmct0PagesProfileDTO(Long idPage,
                              Long idProfile,
                              String name,
                              String description,
                              List<Tmct0ActionPageProfileDTO> listaAccionesPaginaProfile) {
    this.idPage = idPage;
    this.idProfile = idProfile;
    this.name = name;
    this.description = description;
    this.listaAccionesPaginaProfile = listaAccionesPaginaProfile;
  }

  public Long getIdPage() {
    return idPage;
  }

  public void setIdPage(Long idPage) {
    this.idPage = idPage;
  }

  public Long getIdProfile() {
    return idProfile;
  }

  public void setIdProfile(Long idProfile) {
    this.idProfile = idProfile;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Tmct0ActionPageProfileDTO> getListaAccionesPaginaProfile() {
    return listaAccionesPaginaProfile;
  }

  public void setListaAccionesPaginaProfile(List<Tmct0ActionPageProfileDTO> listaAccionesPaginaProfile) {
    this.listaAccionesPaginaProfile = listaAccionesPaginaProfile;
  }

}
