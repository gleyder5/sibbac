package sibbac.webapp.security.database.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO para la gestion de menu
 * 
 * @author Neoris
 *
 */
public class Tmct0MenuDTO {

  private Long id;
  private String opcion;
  private String url;
  private List<Tmct0MenuDTO> submenu;

  public Tmct0MenuDTO() {
    super();
    submenu = new ArrayList<Tmct0MenuDTO>();
  }

  public Tmct0MenuDTO(Long id, String opcion, String url) {
    super();
    submenu = new ArrayList<Tmct0MenuDTO>();
    this.id = id;
    this.opcion = String.valueOf(opcion).trim();
    if (url != null) {
      this.url = String.valueOf(url).trim();
    } else {
      this.url = url;
    }

    ;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getOpcion() {
    return opcion;
  }

  public void setOpcion(String opcion) {
    this.opcion = opcion;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public List<Tmct0MenuDTO> getSubmenu() {
    return submenu;
  }

  public void setSubmenu(List<Tmct0MenuDTO> submenu) {
    this.submenu = submenu;
  }

}
