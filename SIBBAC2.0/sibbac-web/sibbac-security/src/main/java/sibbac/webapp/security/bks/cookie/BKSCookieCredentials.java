package sibbac.webapp.security.bks.cookie;

import sibbac.webapp.security.bks.BKSCredentialsException;
import sibbac.webapp.security.bks.BKSGenericCredentials;


/**
 * Clase que identifica un usuario BKS a partir de una credencial de acceso recibida en una cookie.
 * <p/>
 * 
 * Para garantizar la seguridad de BankSphere y el correcto funcionamiento del SSO con entidades externas las cookies tendrán la siguiente
 * estructura:
 * 
 * <pre>
 * <strong>ID#IP#FC#VA#FR#RE#XML#TC#VC#EC#XML_CIFRADO#TF#FIRMA</strong>
 * </pre>
 * 
 * La cookie contiene una serie de campos separados por el carácter #. Los campos que la componen son:
 * <dl>
 * <dt>ID</dt>
 * <dd>Identificador único de las sesión de seguridad.</dd>
 * <dt>IP</dt>
 * <dd>IP de la maquina para la cual se generó la cookie (pc del usuario). Garantiza una mayor seguridad al unir las cookies a una IP en
 * concreto.</dd>
 * <dt>FC</dt>
 * <dd>Fecha y hora de caducidad en milisegundos. Se usa para comprobar la validez de la cookie. Con hacer la comparación
 * "hora actual en milisegundos &lt; hora de caducidad" se puede confirmar que la cookie no esta caducada.</dd>
 * <dt>VA</dt>
 * <dd>Periodo de validez en milisegundos. Se usa durante la regeneración de la cookie. Se añade el número de milisegundos indicado a la
 * hora actual para obtener la nueva fecha de caducidad.</dd>
 * <dt>FR</dt>
 * <dd>Fecha y hora de regeneración en milisegundos. Se usa para determinar si hay que regenerar la cookie y evitar hacerlo en todas las
 * peticiones. Para ello hay que hacer la comprobación de que "hora de regeneración &lt; hora actual &lt; hora de caducidad"</dd>
 * <dt>RE</dt>
 * <dd>Periodo de regeneración en milisegundos. Se usa durante la regeneración de la cookie. Se añade el número de milisegundos indicado a
 * la hora actual para obtener la nueva fecha de regeneración.</dd>
 * <dt>XML</dt>
 * <dd>XML con los datos del usuario en claro. Aquí se pondrán la mayoría de los datos que identifiquen al usuario: uid, nif, nombre, etc.
 * Este XML va codificado en BASE64.</dd>
 * <dt>TC</dt>
 * <dd>Indica el tipo de cifrado que se va a utilizar para el XML cifrado.</dd>
 * <dt>VC</dt>
 * <dd>Versión de la cookie</dd>
 * <dt>EC</dt>
 * <dd>Emisor de la cookie</dd>
 * <dt>XML_CIFRADO</dt>
 * <dd>XML con los datos del usuario que necesiten ir cifrados. Son datos sensibles y que no deben ser accesibles por cualquiera.</dd>
 * <dt>TF</dt>
 * <dd>Indica el tipo de firma digital que se va a utilizar.</dd>
 * <dt>FIRMA</dt>
 * <dd>Firma digital de todo lo anterior (hasta el ultimo #). Garantiza la autenticidad de la cookie.</dd>
 * </dl>
 * 
 * El xml (campo XML de la cookie) que contiene la información del usuario se encuentra codificado en Base64, por lo que se deberá
 * decodificar para poder acceder a la información. Este es el campo que contiene la información del usuario y de él se deberán obtener sus
 * datos.<br/>
 * En función de la procedencia del usuario este campo puede variar, por lo que la aplicación que gestiona la verificación de la cookie
 * deberá poder entender tanto la cookie local como la corporativa.<br/>
 * <br/>
 * <br/>
 * <strong>XML Local</strong> El emisor de esta cookie generará un xml que describirá al usuario local del sistema y, por tanto, su uid o
 * identificación único. El campo de la cookie que contiene el UID del usuario es el userID.<br/>
 * Un ejemplo de XML es:<br/>
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
 * &lt;cookie&gt;
 * 	&lt;definitionName&gt;NewUserPasswordCookie&lt;/definitionName&gt;
 * 	&lt;userID&gt;n123456&lt;/userID&gt;
 * 	&lt;alias&gt;n123456&lt;/alias&gt;
 * 	&lt;name&gt;Antonio Perez Martinez&lt;/name&gt;
 * &lt;/cookie&gt;
 * </pre>
 * 
 * <br/>
 * <strong>XML Corporativo</strong> El emisor de esta cookie generará un xml que describirá al usuario corporativo, que tendrá como emisor
 * “CorpIntranet” y por tanto su uid o identificador único será el corporativo. El campo de la cookie que contiene el UID del usuario es el
 * userCorp.<br/>
 * Un ejemplo de XML es:<br/>
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="ISO-8859-1"?&gt;
 * &lt;cookie&gt;
 * 	&lt;definitionName&gt;NewUserPasswordCookie&lt;/definitionName&gt;
 * 	&lt;name&gt;Antonio Perez Martinez&lt;/name&gt;
 * 	&lt;userCorp&gt;bte00672&lt;/userCorp&gt;
 * 	&lt;alias&gt;bt_n123456&lt;/alias&gt;
 * 	&lt;localEmitter&gt;BanestoIntranet&lt;/localEmitter&gt;
 * 	&lt;userID&gt;n123456&lt;/userID&gt;
 * &lt;/cookie&gt;
 * </pre>
 * 
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public class BKSCookieCredentials extends BKSGenericCredentials {

	public BKSCookieCredentials() {
		super( CREDENTIALS_TYPE.COOKIE );
	}

	public static BKSCookieCredentials parse( final String cookie ) throws BKSCredentialsException {
		BKSCookieCredentials bks = new BKSCookieCredentials();
		bks.examinar( cookie );
		return bks;
	}
}
