package sibbac.webapp.security.utils;

import java.security.MessageDigest;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/*
 * Clase con los métodos para encriptar y desencriptar la información que se necesite de la bbdd, como puede ser password 
 */
public class Utils {
  // clave para encriptar/desencriptar datos
  private static String secretKey = "Neoris2016";
  // método encriptación
  private static String metodoEncrypt = "MD5";
  // codificación
  private static String codifEncrypt = "utf-8";
  // algorithm
  private static String algorithm = "DESede";

  /**
   * Encripta el texto pasado como parametro
   * 
   * @param texto a encriptar
   * 
   */
  public static String encriptar(String texto) {

    String base64EncryptedString = "";

    try {

      MessageDigest md = MessageDigest.getInstance(metodoEncrypt);
      byte[] digestOfPassword = md.digest(secretKey.getBytes(codifEncrypt));
      byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

      SecretKey key = new SecretKeySpec(keyBytes, algorithm);
      Cipher cipher = Cipher.getInstance(algorithm);
      cipher.init(Cipher.ENCRYPT_MODE, key);

      byte[] plainTextBytes = texto.getBytes(codifEncrypt);
      byte[] buf = cipher.doFinal(plainTextBytes);
      byte[] base64Bytes = Base64.encodeBase64(buf);
      base64EncryptedString = new String(base64Bytes);

    } catch (Exception ex) {
    }
    return base64EncryptedString;
  }

  /**
   * Desencripta el texto pasado como parametro
   * 
   * @param texto a desencriptar
   * 
   */
  public static String desencriptar(String textoEncriptado) throws Exception {

    String base64EncryptedString = "";

    try {
      byte[] message = Base64.decodeBase64(textoEncriptado.getBytes(codifEncrypt));
      MessageDigest md = MessageDigest.getInstance(metodoEncrypt);
      byte[] digestOfPassword = md.digest(secretKey.getBytes(codifEncrypt));
      byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
      SecretKey key = new SecretKeySpec(keyBytes, algorithm);

      Cipher decipher = Cipher.getInstance(algorithm);
      decipher.init(Cipher.DECRYPT_MODE, key);

      byte[] plainText = decipher.doFinal(message);

      base64EncryptedString = new String(plainText, codifEncrypt); // ponía "UTF-8"

    } catch (Exception ex) {
    }
    return base64EncryptedString;
  }
}
