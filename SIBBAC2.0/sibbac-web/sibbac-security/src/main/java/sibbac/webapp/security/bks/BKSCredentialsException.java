package sibbac.webapp.security.bks;


@SuppressWarnings( "serial" )
public class BKSCredentialsException extends Exception {

	public BKSCredentialsException( String message, Throwable cause ) {
		super( message, cause );
	}

	public BKSCredentialsException( String message ) {
		super( message );
	}

}
