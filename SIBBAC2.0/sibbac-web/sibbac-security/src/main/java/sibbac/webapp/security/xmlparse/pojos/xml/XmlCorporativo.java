/**
 * 
 */
package sibbac.webapp.security.xmlparse.pojos.xml;

import sibbac.webapp.security.xmlparse.parsers.XmlParser;

/**
 * @author dortega
 *
 */
public class XmlCorporativo extends XmlLocal {
	
	/**
	 * 
	 */
	private String userCorp;
	
	/**
	 * 
	 */
	private String localEmitter;

	/**
	 * 
	 */
	public XmlCorporativo() {
		super();
	}

	/**
	 * @return the userCorp
	 */
	public String getUserCorp() {
		return userCorp;
	}

	/**
	 * @return the localEmitter
	 */
	public String getLocalEmitter() {
		return localEmitter;
	}

	/**
	 * @param userCorp the userCorp to set
	 */
	public void setUserCorp(String userCorp) {
		this.userCorp = userCorp;
	}

	/**
	 * @param localEmitter the localEmitter to set
	 */
	public void setLocalEmitter(String localEmitter) {
		this.localEmitter = localEmitter;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = XmlParser.getInstance().parse(this);
		return result;
		
	} 
	
	
	
	

}
