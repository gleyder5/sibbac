package sibbac.webapp.security.bks;

public abstract class BKSGenericCredentials implements BKSCredential {

//	private static final Logger	LOG	= LoggerFactory.getLogger( BKSCredential.class );

	// Common fields
	private String				id;
	private String				ip;
	private long				fechaDeCaducidad;
	private long				validez;												// Cookie
	private long				fechaDeRegeneracion;									// Cookie
	private long				periodoDeRegeneracion;									// Cookie
	private String				xml;
	private String				tipoDeCifrado;
	private String				version;
	private String				emisor;
	private String				xmlCifrado;
	private String				tipoDeFirma;
	private String				firma;

	private CREDENTIALS_TYPE	type;

	protected BKSGenericCredentials() {
	}

	protected BKSGenericCredentials( CREDENTIALS_TYPE type ) {
		this();
		this.type = type;
	}

	/*
	 * Como el caracter de separacion ("#") puede formar parte del valor de alguno de los campos, no es posible utilizar la clase:
	 * "{@link StringTokenizer}" para separar los campos. En su lugar, se debe emplear una separacion por ocurrencia de aparicion.
	 */
	protected void examinar( final String value ) throws BKSCredentialsException {
//		String prefix = "[BKSGenericCredentials::examinar] ";

		// Sanity check
		if ( this.type != CREDENTIALS_TYPE.COOKIE && this.type != CREDENTIALS_TYPE.TOKEN ) {
			throw new BKSCredentialsException( "Tipo de credencial nula o no reconocida: [" + this.type + "]" );
		}
		// More sanity check
		if ( value == null || value.isEmpty() ) {
			throw new BKSCredentialsException( "Cadena de identificacion nula o no valida: [" + value + "]" );
		}
		// Even more sanity check
		if ( !value.contains( SEPARATOR ) ) {
			throw new BKSCredentialsException( "La cadena de identificacion recibida no contiene el caracter de separacion(\"" + SEPARATOR
					+ "\"): [" + value + "]" );
		}

		String remaining = value;
		@SuppressWarnings("unused")
		String token = null;
		int pos = 0;
//		LOG.trace( prefix + "Examinando la cadena de texto recibida..." );
		while ( ( pos = remaining.indexOf( SEPARATOR ) ) > 0 ) {
			token = remaining.substring( 0, pos );
			remaining = remaining.substring( pos+1 );
//			LOG.trace( prefix + "+ [token=={}] [remaining=={}]", token, remaining );
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getIp()
	 */
	@Override
	public String getIp() {
		return this.ip;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getFechaDeCaducidad()
	 */
	@Override
	public long getFechaDeCaducidad() {
		return this.fechaDeCaducidad;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getValidez()
	 */
	@Override
	public long getValidez() {
		return this.validez;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getFechaDeRegeneracion()
	 */
	@Override
	public long getFechaDeRegeneracion() {
		return this.fechaDeRegeneracion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getPeriodoDeRegeneracion()
	 */
	@Override
	public long getPeriodoDeRegeneracion() {
		return this.periodoDeRegeneracion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getXml()
	 */
	@Override
	public String getXml() {
		return this.xml;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getTipoDeCifrado()
	 */
	@Override
	public String getTipoDeCifrado() {
		return this.tipoDeCifrado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getVersion()
	 */
	@Override
	public String getVersion() {
		return this.version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getEmisor()
	 */
	@Override
	public String getEmisor() {
		return this.emisor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getXmlCifrado()
	 */
	@Override
	public String getXmlCifrado() {
		return this.xmlCifrado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getTipoDeFirma()
	 */
	@Override
	public String getTipoDeFirma() {
		return this.tipoDeFirma;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getFirma()
	 */
	@Override
	public String getFirma() {
		return this.firma;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#getType()
	 */
	@Override
	public CREDENTIALS_TYPE getType() {
		return this.type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#isFromCookie()
	 */
	@Override
	public boolean isFromCookie() {
		return this.type.equals( CREDENTIALS_TYPE.COOKIE );
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSCredential#isXMLCorporativo()
	 */
	@Override
	public boolean isXMLCorporativo() {
		// TODO Auto-generated method stub
		return false;
	}

}
