package sibbac.webapp.security.database.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.database.dao.Tmct0PasswordsDao;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.dto.Tmct0UsersDTO;
import sibbac.webapp.security.database.dto.assembler.Tmct0UsersAssembler;
import sibbac.webapp.security.database.model.Tmct0ActionPage;
import sibbac.webapp.security.database.model.Tmct0Passwords;
import sibbac.webapp.security.database.model.Tmct0Profiles;
import sibbac.webapp.security.database.model.Tmct0Users;
import sibbac.webapp.security.utils.Utils;

@Service
public class Tmct0UsersBo extends AbstractBo<Tmct0Users, Long, Tmct0UsersDao> {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0UsersBo.class);

  @PersistenceContext
  private EntityManager em;

  @Autowired
  Tmct0UsersDao tmct0UsersDao;

  @Autowired
  Tmct0UsersAssembler tmct0UsersAssembler;

  @Autowired
  Tmct0PasswordsDao tmct0PasswordsDao;

  public boolean isUserExistente(String username) {
    Tmct0Users usuario = tmct0UsersDao.getByUsername(username);
    return usuario != null;
  }

  public boolean isUserValido(String username, String password) {
    Tmct0Users usuario = tmct0UsersDao.getByUsernameAndPassword(username, Utils.encriptar(password));
    if (usuario != null) {
      return true;
    } else {
      return false;
    }
  }

  public boolean updateUserPassword(String username, String password, String nuevoPassword) {
    String nuevoPasswordEncriptado;
    Tmct0Users usuario = tmct0UsersDao.getByUsernameAndPassword(username, Utils.encriptar(password));
    Tmct0Passwords passNuevo = null;
    if (usuario != null) {
      usuario.setNum_intentos(0);
      usuario.setLastlogin(new Date());
      
      if (isPasswordReseteado(usuario)  ) {
        
        if (nuevoPassword != null){  // Ha habido casos que ha llegado aqui un password nuevo nulo.  No se puede permitir.
          nuevoPasswordEncriptado = Utils.encriptar(nuevoPassword);
          usuario.setPassword(nuevoPasswordEncriptado);
          usuario.setfPassword(new Date());
          usuario.setReset(false);
          
          passNuevo = new Tmct0Passwords();
          passNuevo.setIdUser(usuario.getIdUser());
          passNuevo.setPassword(nuevoPasswordEncriptado);
          passNuevo.setfPassword(new Date());
          
        }else{
          return false;
        }
      }

      tmct0UsersDao.saveAndFlush(usuario);

      if (passNuevo != null) {
        tmct0PasswordsDao.save(passNuevo);
      }

      return true;

    } else {
      return false;
    }

  }

  @Transactional
  public void updateReseteoPassword(Long idUsuario) {
    LOG.debug("Entra updateReseteoPassword");
    Tmct0Users usuario = this.getUser(idUsuario);
    usuario.setReset(true);
    tmct0UsersDao.saveAndFlush(usuario);
    LOG.debug("Termina updateReseteoPassword");
  }

  public boolean isPasswordReseteado(Tmct0Users usuario) {
    return usuario != null && usuario.isReset();
  }

  public boolean isPasswordReseteado(String username) {
    Tmct0Users usuario = tmct0UsersDao.getByUsername(username);
    return usuario != null && usuario.isReset();
  }

  public Tmct0Users getByUsername(String username) {

    Tmct0Users usuario = tmct0UsersDao.getByUsername(username);
    return usuario;

  }

  /**
   * Recupera un usuario dado su identificador único
   * 
   * @param identificador del usuario que se quiere recuperar
   * 
   */
  public Tmct0Users getUser(Long id) {
    return tmct0UsersDao.getOne(id);
  }

  /**
   * Graba los datos de la usuario existente a partir de los datos del dto recibido.
   * 
   * @param dto - con los datos del usuario a modificar
   *
   */
  @Transactional
  public void saveExistingUser(Tmct0UsersDTO dto) {

    LOG.debug("Entra saveExistingUser");
    Tmct0Users tmct0User = tmct0UsersAssembler.getTmct0UsersEntidad(dto);
    Tmct0Users tmct0UserTemp = tmct0UsersDao.getByUsername(tmct0User.getUsername());
    
    tmct0User.setPassword(Utils.encriptar(dto.getPassword()));
    
    if (tmct0UserTemp != null) {
      // Se recupera la información que no se trata en la pantalla
      tmct0User.setfPassword(tmct0UserTemp.getfPassword());
      tmct0User.setLastlogin(tmct0UserTemp.getLastlogin());
      if (!tmct0UserTemp.getPassword().equals(tmct0User.getPassword())) {
        tmct0User.setReset(true);
      }

    } else {
      tmct0User.setfPassword(new Date());
      tmct0User.setReset(true);
      tmct0User.setPassword(Utils.encriptar(dto.getPassword()));
    }
   
    tmct0User.setNum_intentos(0);

    // Graba y persiste en bbdd
    tmct0UsersDao.saveAndFlush(tmct0User);
    LOG.debug("Termina saveUser");

  }
  
  /**
   * Graba los datos del usuario nuevo a partir de los datos del dto recibido.
   * 
   * @param dto - con los datos del usuario a crear
   *
   */
  @Transactional
  public void saveNewUser(Tmct0UsersDTO dto) {

    LOG.debug("Entra saveNewUser");
    Tmct0Users tmct0User = tmct0UsersAssembler.getTmct0UsersEntidad(dto);

    tmct0User.setfPassword(new Date());
    tmct0User.setReset(true);
    tmct0User.setPassword(Utils.encriptar(dto.getPassword()));
    tmct0User.setNum_intentos(0);

    // Graba y persiste en bbdd
    tmct0UsersDao.saveAndFlush(tmct0User);
    LOG.debug("Termina saveUser");

  }

  public boolean isUserInSibbac(Long userId) {
    return (tmct0UsersDao.findOne(userId.longValue()) != null);
  }

  public Set<String> getUrlsByUsuario(Long id) {
    Set<String> pages = new HashSet<String>();
    Tmct0Users usuario = this.getUser(id);
    if (usuario.getProfiles() != null && !usuario.getProfiles().isEmpty()) {
      for (Tmct0Profiles profile : usuario.getProfiles()) {
        if (profile.getActionPages() != null && !profile.getActionPages().isEmpty()) {
          for (Tmct0ActionPage actionPage : profile.getActionPages()) {
            pages.add(actionPage.getPage().getUrl());
          }
        }
      }
    }
    return pages;
  }

  public List<Tmct0ActionPage> getActionsByUsuario(Long id) {
    List<Tmct0ActionPage> actions = new ArrayList<Tmct0ActionPage>();
    Tmct0Users usuario = this.getUser(id);
    if (usuario.getProfiles() != null && !usuario.getProfiles().isEmpty()) {
      for (Tmct0Profiles profile : usuario.getProfiles()) {
        if (profile.getActionPages() != null && !profile.getActionPages().isEmpty()) {
          actions.addAll(profile.getActionPages());
        }
      }
    }
    return actions;
  }

  public boolean isPermisoAcceso(Long id, String url) {
    return getUrlsByUsuario(id).contains(url);
  }

}
