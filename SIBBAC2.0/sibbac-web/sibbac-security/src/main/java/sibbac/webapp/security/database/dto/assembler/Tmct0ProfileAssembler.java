package sibbac.webapp.security.database.dto.assembler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.webapp.security.database.dao.Tmct0ActionPageDao;
import sibbac.webapp.security.database.dao.Tmct0PagesDao;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.dto.Tmct0PerfilFilterDTO;
import sibbac.webapp.security.database.dto.Tmct0ProfilesDTO;
import sibbac.webapp.security.database.model.Tmct0ActionPage;
import sibbac.webapp.security.database.model.Tmct0Profiles;
import sibbac.webapp.security.database.model.Tmct0Users;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_PAGES
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0ProfileAssembler {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ProfileAssembler.class);

  @Autowired
  Tmct0PagesDao tmct0PagesDao;

  @Autowired
  Tmct0ActionPageDao tmct0ActionPageDao;

  @Autowired
  Tmct0UsersDao tmct0UsersDao;

  /**
   * Realiza la conversion entre la entidad y el dto
   * 
   * @param datosProfile -> Entidad que se quiere trasformar en DTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0ProfilesDTO getTmct0ProfileDTO(Tmct0Profiles datosProfile) {

    Tmct0ProfilesDTO profileDTO = new Tmct0ProfilesDTO();
    List<SelectDTO> listaUsuarios = new ArrayList<SelectDTO>();
    List<Long> listaPaginasAccion = new ArrayList<Long>();

    LOG.debug("Inicio de la transformación getTmct0ProfileDTO con Id " + datosProfile.getIdProfile());

    // Asignación de valores directos.
    profileDTO.setIdProfile(datosProfile.getIdProfile());
    profileDTO.setName(datosProfile.getName());
    profileDTO.setDescription(datosProfile.getDescription());

    // Se obtienen los usuarios
    List<Tmct0Users> listaEntidadesUsuario = datosProfile.getUsers();

    SelectDTO datoUsuario;
    for (Tmct0Users tmct0Users : listaEntidadesUsuario) {
      datoUsuario = new SelectDTO(
                                  String.valueOf(tmct0Users.getIdUser()),
                                  (tmct0Users.getUsername() + " - " + tmct0Users.getName() + " " + tmct0Users.getLastname()).toUpperCase());
      listaUsuarios.add(datoUsuario);
    }

    profileDTO.setListaUsuarios(listaUsuarios);

    // Se obtienen los identificadores de las paginas a los que tiene permiso.
    List<Long> listaEntidadesPaginasAccion = new ArrayList<Long>();

    List<Tmct0ActionPage> listActionPages = datosProfile.getActionPages();
    for (Tmct0ActionPage tmct0ActionPage : listActionPages) {
      listaEntidadesPaginasAccion.add(tmct0ActionPage.getIdActionPage());
    }

    profileDTO.setListaPaginasAccion(listaEntidadesPaginasAccion);

    LOG.debug("Fin de la transformación getTmct0ProfileDTO");

    return profileDTO;

  }

  /**
   * Realiza la conversion entre los datos del front y la entity a gestionar en back
   * 
   * @param paramsObjects -> datos del front
   * @return Entity - Entity a gestionar en la parte back
   */
  public Tmct0Profiles getTmct0ProfileEntity(Map<String, Object> paramsObjects) throws ParseException {
    LOG.debug("Inicio de la transformación getTmct0ProfileEntity");

    Tmct0Profiles miEntity = new Tmct0Profiles();

    List<Tmct0ActionPage> listActionPages = new ArrayList<Tmct0ActionPage>();

    List<Tmct0Users> listUsers = new ArrayList<Tmct0Users>();

    if (paramsObjects.get("datosPerfil") != null) {
      // Se comprueba si se tiene el idPerfil que determina si es una entidad nueva o ya existente
      Map<String, Object> datosPerfil = (Map<String, Object>) paramsObjects.get("datosPerfil");
      if (datosPerfil.get("idProfile") != null) {
        Long lIdProfile = Long.valueOf(datosPerfil.get("idProfile").toString());
        if (lIdProfile != 0) {
          miEntity.setIdProfile(lIdProfile);
        }
      }
      if (datosPerfil.get("name") != null) {
        miEntity.setName((String) datosPerfil.get("name"));
      }

      if (datosPerfil.get("description") != null) {
        miEntity.setDescription((String) datosPerfil.get("description"));
      }

      // Se obtienen los usuarios

      List<Map<String, String>> usuariosSeleccionados = (List<Map<String, String>>) datosPerfil.get("listaUsuarios");
      for (Map<String, String> usuario : usuariosSeleccionados) {
        Tmct0Users tmct0UsersPerfil = tmct0UsersDao.findOne(Long.valueOf(usuario.get("key").toString()));
        listUsers.add(tmct0UsersPerfil);
      }

      // Se añaden a la entidad.
      miEntity.setUsers(listUsers);

      // Se obtienen y se añaden las paginas a las que tiene permiso el perfil

      List<Object> ListPaginasAccionPerfil = (List<Object>) datosPerfil.get("listaPaginasAccion");

      for (Object idPaginaAccionSelect : ListPaginasAccionPerfil) {

        Long idPaginaAccion = Long.parseLong(String.valueOf(idPaginaAccionSelect));

        Tmct0ActionPage accionPage = tmct0ActionPageDao.findOne(idPaginaAccion);
        listActionPages.add(accionPage);

      }
      // Se añaden a la entidad
      miEntity.setActionPages(listActionPages);
    }
    LOG.debug("Fin de la transformación getTmct0ProfileEntity");
    return miEntity;

  }

  /**
   * Realiza la conversion entre el filtro del front y el filtroDTO que se utiliza en back
   * 
   * @param paramsObjects -> Filtro del front
   * @return DTO - Tmct0PerfilFilterDTO - Objeto filtro que se emplea en back
   */
  public Tmct0PerfilFilterDTO getTmct0PerfilFilterDTO(Map<String, Object> paramsObjects) throws ParseException {
    LOG.debug("Inicio de la transformación Tmct0PagesFilterDTO");

    Tmct0PerfilFilterDTO miFilterDTO = new Tmct0PerfilFilterDTO();

    miFilterDTO.setIdPerfil(Integer.valueOf(paramsObjects.get("idPerfil").toString()));
    miFilterDTO.setNotPerfil(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notPerfil"))));

    LOG.debug("Fin de la transformación Tmct0PagesFilterDTO");

    return miFilterDTO;
  }

}
