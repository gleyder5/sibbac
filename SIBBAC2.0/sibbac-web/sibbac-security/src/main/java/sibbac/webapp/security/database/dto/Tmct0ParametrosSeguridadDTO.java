package sibbac.webapp.security.database.dto;

public class Tmct0ParametrosSeguridadDTO {

  /** mayusculas - Numero de mayusculas que tiene que tener la contraseña */
  private Integer mayusculas;

  /** numeros - Numero de numeros que tiene que tener la contraseña */
  private Integer numeros;

  /** caracteresExtraños - Numero de caracteres extraños que tiene que tener la contraseña */
  private Integer caracteresExtranos;

  /** longitud - Longitud minima de la contraseña */
  private Integer longitud;

  /** tiempoExpiracion - Numero de dias de vigencia */
  private Integer tiempoExpiracion;

  /** avisoExpiracion - Numero de dias anteriores al aviso de que va a expirar */
  private Integer avisoExpiracion;

  /** intentosBloqueo - Numero de intentos de bloqueo */
  private Integer intentosBloqueo;

  /** cotraseñasRepetidas - Numero de contraseñas repetidas */
  private Integer cotrasenasRepetidas;

  public Tmct0ParametrosSeguridadDTO() {
  }

  public Tmct0ParametrosSeguridadDTO(Integer mayusculas,
                                     Integer numeros,
                                     Integer caracteresExtranos,
                                     Integer longitud,
                                     Integer tiempoExpiracion,
                                     Integer avisoExpiracion,
                                     Integer intentosBloqueo,
                                     Integer cotrasenasRepetidas) {

    this.mayusculas = mayusculas;
    this.numeros = numeros;
    this.caracteresExtranos = caracteresExtranos;
    this.longitud = longitud;
    this.tiempoExpiracion = tiempoExpiracion;
    this.avisoExpiracion = avisoExpiracion;
    this.intentosBloqueo = intentosBloqueo;
    this.cotrasenasRepetidas = cotrasenasRepetidas;
  }

  public Integer getMayusculas() {
    return mayusculas;
  }

  public void setMayusculas(Integer mayusculas) {
    this.mayusculas = mayusculas;
  }

  public Integer getNumeros() {
    return numeros;
  }

  public void setNumeros(Integer numeros) {
    this.numeros = numeros;
  }

  public Integer getCaracteresExtranos() {
    return caracteresExtranos;
  }

  public void setCaracteresExtranos(Integer caracteresExtranos) {
    this.caracteresExtranos = caracteresExtranos;
  }

  public Integer getLongitud() {
    return longitud;
  }

  public void setLongitud(Integer longitud) {
    this.longitud = longitud;
  }

  public Integer getTiempoExpiracion() {
    return tiempoExpiracion;
  }

  public void setTiempoExpiracion(Integer tiempoExpiracion) {
    this.tiempoExpiracion = tiempoExpiracion;
  }

  public Integer getAvisoExpiracion() {
    return avisoExpiracion;
  }

  public void setAvisoExpiracion(Integer avisoExpiracion) {
    this.avisoExpiracion = avisoExpiracion;
  }

  public Integer getIntentosBloqueo() {
    return intentosBloqueo;
  }

  public void setIntentosBloqueo(Integer intentosBloqueo) {
    this.intentosBloqueo = intentosBloqueo;
  }

  public Integer getCotrasenasRepetidas() {
    return cotrasenasRepetidas;
  }

  public void setCotraseñasRepetidas(Integer cotrasenasRepetidas) {
    this.cotrasenasRepetidas = cotrasenasRepetidas;
  }

}
