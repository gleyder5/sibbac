package sibbac.webapp.security.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import sibbac.common.SIBBACBusinessException;
import sibbac.common.utils.DateHelper;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.bo.Tmct0PagesBo;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;
import sibbac.webapp.security.database.dao.Tmct0ParametrosSeguridadDao;
import sibbac.webapp.security.database.dao.Tmct0PasswordsDao;
import sibbac.webapp.security.database.dto.Tmct0MenuDTO;
import sibbac.webapp.security.database.dto.Tmct0ParametrosSeguridadDTO;
import sibbac.webapp.security.database.dto.Tmct0UsersDTO;
import sibbac.webapp.security.database.dto.assembler.Tmct0UsersAssembler;
import sibbac.webapp.security.database.model.Tmct0ActionPage;
import sibbac.webapp.security.database.model.Tmct0Pages;
import sibbac.webapp.security.database.model.Tmct0ParametrosSeguridad;
import sibbac.webapp.security.database.model.Tmct0Passwords;
import sibbac.webapp.security.database.model.Tmct0Users;
import sibbac.webapp.security.session.UserSession;
import sibbac.webapp.security.utils.Utils;

@SIBBACService
public class SIBBACServiceSecurity implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceSecurity.class);

  public static final String USER_SESSION = "UserSession";

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  private final static String MESSAGE_ERROR = "No se ha podido validar su usuario y contraseña";

  @Value("${environment}")
  private String environment;

  @Autowired
  Tmct0UsersBo usersBo;

  @Autowired
  Tmct0PagesBo pagesBo;

  @Autowired
  Tmct0UsersAssembler assembler;

  @Autowired
  HttpSession session;

  @Autowired
  Tmct0ParametrosSeguridadDao tct0ParametrosSeguridadDao;

  @Autowired
  Tmct0PasswordsDao tmct0PasswordsDao;

  @Autowired
  SIBBACServiceParametrosSeguridad parametrosSeguridad;

  /**
   * The Enum Comandos.
   */
  private enum Comandos {

    LOGIN("Login"),
    LOGOUT("Logout"),
    AUTENTICADO("Autenticado"),
    PERMISOS("Permisos"),
    TIENE_PERMISO("Tiene permiso"),
    SIN_COMANDO("Sin comando"),
    MENU("Menu"),
    RESETEAR_PASSWORD("resetearPassword"),
    PASSWORD_RESETEADO("PasswordReseteado"),
    VALIDA_CONTRASENIA_ACTUAL("validaContraseniaActual"),
    PARAMETROS_COMPLEJIDAD("parametrosComplejidad"),
    VALIDA_HISTORICO_CONTRASENIAS("validarHistoricoCrontrasenias");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private Comandos(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * The Enum Campos plantilla .
   */
  private enum Fields {

    SEGURIDAD("seguridad"),
    AUTENTICADO("autenticado");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private Fields(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) {
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados;
    Comandos command = getCommand(webRequest.getAction());

    try {
      LOG.trace("Entro al servicio SIBBACServiceSecurity");
      switch (command) {
        case LOGIN:
          result.setResultados(login(paramsObjects));
          break;
        case LOGOUT:
          result.setResultados(logout(paramsObjects));
          break;
        case AUTENTICADO:
          result.setResultados(autenticado(paramsObjects));
          break;
        case RESETEAR_PASSWORD:
          result.setResultados(resetearPassword(paramsObjects));
          break;
        case PARAMETROS_COMPLEJIDAD:
          result.setResultados(parametrosComplejidad(paramsObjects));
          break;
        case PASSWORD_RESETEADO:
          result.setResultados(passwordReseteado(paramsObjects));
          break;
        case TIENE_PERMISO:
          result.setResultados(tienePermiso(paramsObjects));
          break;
        case PERMISOS:
          resultados = permisos(paramsObjects);
          if(resultados != null) {
            result.setResultados(resultados);
          }
          else {
            resultados = Collections.<String, Object>singletonMap("status", "KO");
            result.setResultados(resultados);
            result.setError(MESSAGE_ERROR);
          }
          break;
        case MENU:
          result.setResultados(menu(paramsObjects));
          break;
        case VALIDA_CONTRASENIA_ACTUAL:
          result.setResultados(validarContraseniaActual(paramsObjects));
          break;
        case VALIDA_HISTORICO_CONTRASENIAS:
          result.setResultados(validarHistoricoCrontrasenias(paramsObjects));
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (Exception e) {
      LOG.error("[process] Error en el servicio de seguridad", e);
      resultados = Collections.<String,Object>singletonMap("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }
    LOG.trace("Salgo del servicio SIBBACServiceSecurity");
    return result;
  }

  private Map<String, Object> autenticado(Map<String, Object> paramsObjects) {
    LOG.trace("Iniciando autenticado");
    Map<String, Object> result = new HashMap<String, Object>();
    UserSession user = (UserSession) session.getAttribute(USER_SESSION);
    result.put("autenticado", user != null);
    result.put("user", user);
    result.put("environment", this.environment);
    LOG.trace("Fin autenticado");
    return result;
  }

  private Map<String, Object> resetearPassword(Map<String, Object> paramsObjects) {
    LOG.debug("Iniciando resetearPassword");
    Map<String, Object> result = new HashMap<String, Object>();

    result.put("reseteado", false);

    Tmct0UsersDTO dto = assembler.getTmct0UsersDTO(paramsObjects);
    Tmct0Users user = usersBo.getByUsername(dto.getUsername());

    Tmct0Passwords passNuevo = new Tmct0Passwords();

    passNuevo.setIdUser(user.getIdUser());
    
    passNuevo.setPassword(Utils.encriptar((String) paramsObjects.get("password")));
    passNuevo.setfPassword(new Date());

    user.setPassword(passNuevo.getPassword());
    user.setfPassword(new Date());

    usersBo.saveAndFlush(user);
    tmct0PasswordsDao.save(passNuevo);

    result.put("reseteado", true);

    LOG.debug("Fin resetearPassword");
    return result;

  }

  private Map<String, Object> passwordReseteado(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
    LOG.debug("Iniciando passwordReseteado");
    Map<String, Object> result = new HashMap<String, Object>();

    Tmct0UsersDTO dto = assembler.getTmct0UsersDTO(paramsObjects);

    if (usersBo.isUserExistente(dto.getUsername())) {
      result.put("passwordReseteado", usersBo.isPasswordReseteado(dto.getUsername()));
    } else {
      result.put("passwordReseteado", false);
    }

    LOG.debug("Fin passwordReseteado");
    return result;

  }

  private Map<String, Object> parametrosComplejidad(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
    LOG.debug("Iniciando parametrosComplejidad");
    Map<String, Object> result = new HashMap<String, Object>();
    Tmct0ParametrosSeguridad parametrosSeguridad = null;
    Tmct0ParametrosSeguridadDTO miDto = new Tmct0ParametrosSeguridadDTO();

    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.mayusculas);
    miDto.setMayusculas(parametrosSeguridad.getValor());

    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.numeros);
    miDto.setNumeros(parametrosSeguridad.getValor());

    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.caracteresExtranos);
    miDto.setCaracteresExtranos(parametrosSeguridad.getValor());

    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.longitud);
    miDto.setLongitud(parametrosSeguridad.getValor());

    result.put("parametros", miDto);

    LOG.debug("Fin parametrosComplejidad");
    return result;

  }

  private Map<String, Object> tienePermiso(Map<String, Object> paramsObjects) {

    LOG.trace("Iniciando tienePermiso");
    Map<String, Object> result = new HashMap<String, Object>();
    UserSession user = (UserSession) session.getAttribute(USER_SESSION);
    Boolean isPermisoAccesso = usersBo.isPermisoAcceso(user.getUserId(), (String) paramsObjects.get("url"));
    result.put("permiso", isPermisoAccesso);
    LOG.trace("Fin tienePermiso");
    return result;

  }

  private Map<String, Object> menu(Map<String, Object> paramsObjects) {

    LOG.trace("Iniciando carga de menú");
    Map<String, Object> result = new HashMap<String, Object>();

    List<Tmct0MenuDTO> dto = new ArrayList<Tmct0MenuDTO>();
    List<Tmct0Pages> pages = pagesBo.getPages();
    List<Tmct0Pages> opcionesPrincipales = this.getOpcionesPrincipales(pages);

    if (opcionesPrincipales != null && !opcionesPrincipales.isEmpty()) {
      for (Tmct0Pages page : opcionesPrincipales) {
        dto.add(this.getOpcion(page, pages));
      }
    }
    result.put("menu", dto);

    LOG.trace("Fin carga de menú");
    return result;

  }

  private List<Tmct0Pages> getOpcionesPrincipales(List<Tmct0Pages> pages) {
    List<Tmct0Pages> opcionesPrincipales = new ArrayList<Tmct0Pages>();
    for (Tmct0Pages page : pages) {
      if (page.getIdParent() == null) {
        opcionesPrincipales.add(page);
      }
    }
    return opcionesPrincipales;
  }

  private List<Tmct0Pages> getOpcionesHijas(Tmct0Pages page, List<Tmct0Pages> pages) {
    List<Tmct0Pages> opcionesHijas = new ArrayList<Tmct0Pages>();
    for (Tmct0Pages pageHija : pages) {
      if (pageHija.getIdParent() != null) {
        if (page.getIdPage().equals(Long.parseLong(pageHija.getIdParent().toString()))) {
          opcionesHijas.add(pageHija);
        }
      }
    }
    return opcionesHijas;
  }

  private Tmct0MenuDTO getOpcion(Tmct0Pages page, List<Tmct0Pages> pages) {

    String sPageUrl = page.getUrl();

    Tmct0MenuDTO opcion = new Tmct0MenuDTO(page.getIdPage(), page.getName(), page.getUrl());
    List<Tmct0Pages> pagesHijas = getOpcionesHijas(page, pages);
    if (pagesHijas != null && !pagesHijas.isEmpty()) {
      for (Tmct0Pages pageHija : pagesHijas) {
        opcion.getSubmenu().add(this.getOpcion(pageHija, pages));
      }
    }
    // MFG 04/10/2014 Si la pagina es de acceso a sibbac, que empieza por / se la concatena el caracter #
    if (sPageUrl != null && sPageUrl.startsWith("/")) {
      opcion.setUrl("#" + sPageUrl.trim());
    }

    return opcion;
  }

  private Map<String, Object> permisos(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
    LOG.trace("Iniciando permisos");
    Map<String, Object> result = new HashMap<String, Object>();
    UserSession user = (UserSession) session.getAttribute(USER_SESSION);
    if (user == null) {
      LOG.warn("No se encuentra la sesión de usuario");
      return null;
    }
    List<Tmct0ActionPage> actions = usersBo.getActionsByUsuario(user.getUserId());
    result.put("permisos", actions);
    LOG.trace("Fin permisos");
    return result;

  }

  private Map<String, Object> logout(Map<String, Object> paramsObjects) {

    LOG.trace("Iniciando logout");
    Map<String, Object> result = new HashMap<String, Object>();

    result.put("autenticado", false);
    session.setAttribute(USER_SESSION, null);

    LOG.trace("Fin logout");
    return result;

  }

  private Map<String, Object> login(Map<String, Object> paramsObjects) throws ParseException {

    LOG.trace("Iniciando login");
    Map<String, Object> result = new HashMap<String, Object>();

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    Tmct0UsersDTO dto = assembler.getTmct0UsersDTO(paramsObjects);
    Tmct0Users user = usersBo.getByUsername(dto.getUsername());
    Tmct0ParametrosSeguridad parametrosSeguridad = null;

    // Inicializaciones restrictirvas
    result.put("usuarioBloqueado", false);
    result.put("passwordVigente", true);
    result.put("existeUsuario", true);
    result.put("autenticado", false);
    result.put("numIntentos", 0);
    result.put("diasExpira", -1);

    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.intentosBloqueo);
    Integer intentosBloqueo = parametrosSeguridad.getValor();
    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.tiempoExpiracion);
    Integer tiempoExpiracion = parametrosSeguridad.getValor();
    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.avisoExpiracion);
    Integer nDiasAviso = parametrosSeguridad.getValor();

    if (user != null) {
      // Se comprueba si esta bloqueado.
      if (intentosBloqueo <= user.getNum_intentos()) {
        result.put("usuarioBloqueado", true);
        return result;
      }

      // Se comprueba si la contraseña ha expirado para eso se restan las fechas sin las horas y se obtiene el numero de
      // dias.
      Calendar cExpira = Calendar.getInstance();
      cExpira.setTime(user.getfPassword()); // Configuramos la fecha que se recibe
      cExpira.add(Calendar.DAY_OF_YEAR, tiempoExpiracion);

      Calendar cHoy = Calendar.getInstance();
      String sHoy = DateHelper.convertDateToString(cHoy.getTime(), "dd/MM/yyyy");
      String sExpira = DateHelper.convertDateToString(cExpira.getTime(), "dd/MM/yyyy");
      Date dHoy = dateFormat.parse(sHoy);
      Date dExpira = dateFormat.parse(sExpira);
      // Se obtienen los dias de diferencia entre la fechaactual y la que expira.
      long diftime = dExpira.getTime() - dHoy.getTime();
      long ldias = diftime / (1000 * 60 * 60 * 24);

      if (ldias < 0) { // ya ha expirado
        result.put("passwordVigente", false);
        return result;
      } else {

        // Si los dias que falta para expirar es menor a los dias de aviso se avisa.
        if (ldias < nDiasAviso) {
          result.put("diasExpira", ldias);
        }

      }

      // Usuario al que se le permite entrar: Datos validos y no esta bloqueado y tiene el passVigente
      if (usersBo.updateUserPassword(dto.getUsername(), dto.getPassword(), dto.getNuevoPassword())) {
        UserSession userSession = new UserSession(user.getIdUser(), user.getUsername());
        LOG.info("[login] Usuario iniciando sesion: {}", userSession.toString());
        session.setAttribute(USER_SESSION, userSession);
        result.put("autenticado", true);
      } else { // Usuario valido pero el password insertado no es valido. Se controlan los intentos y el bloqueo
        // Se actualizan los intentos realizados
        Integer iNumIntentos = user.getNum_intentos() + 1;
        user.setNum_intentos(iNumIntentos);
        
        usersBo.saveAndFlush(user);
        result.put("numIntentos", intentosBloqueo - iNumIntentos);

      }

    } else {
      result.put("existeUsuario", false);
    }
    LOG.trace("Fin login");

    return result;

  }

  private Map<String, Object> validarContraseniaActual(Map<String, Object> paramsObjects) throws ParseException {

    LOG.trace("Iniciando validaContrasenia");
    Map<String, Object> result = new HashMap<String, Object>();

    Tmct0UsersDTO dto = assembler.getTmct0UsersDTO(paramsObjects);

    if (usersBo.isUserValido(dto.getUsername(), dto.getPassword())) {
      result.put("passwordActual", true);
    } else {
      result.put("passwordActual", false);
    }

    LOG.trace("Fin validaContrasenia");

    return result;

  }

  private Map<String, Object> validarHistoricoCrontrasenias(Map<String, Object> paramsObjects) throws ParseException {
    LOG.trace("Iniciando validarHistoricoCrontrasenias");
    String passwordDesencriptada;
    Map<String, Object> result = new HashMap<String, Object>();

    Tmct0ParametrosSeguridad parametrosSeguridad = null;
    parametrosSeguridad = tct0ParametrosSeguridadDao.findOne(SIBBACServiceParametrosSeguridad.cotrasenasRepetidas);
    Integer iContraseniasRepetidas = parametrosSeguridad.getValor();

    Tmct0UsersDTO dto = assembler.getTmct0UsersDTO(paramsObjects);
    Tmct0Users user = usersBo.getByUsername(dto.getUsername());

    List<Tmct0Passwords> tmct0PasswordsList = user.getTmct0PasswordsList();

    Boolean bRepetida = false;

    int iContador = 0;
    for (Tmct0Passwords tmct0Passwords : tmct0PasswordsList) {
      iContador++;
      try {
        passwordDesencriptada = Utils.desencriptar(tmct0Passwords.getPassword());
        if (iContador <= iContraseniasRepetidas
            && passwordDesencriptada.equals((String) paramsObjects.get("nuevoPassword"))) {
          bRepetida = true;
        }
      }
      catch (Exception e) {
        LOG.warn("No se ha podido desencriptar la password, {}", e.getMessage());
        LOG.trace(e.getMessage(), e);
      }
    }
    result.put("repetido", bRepetida);

    LOG.trace("Fin validarHistoricoCrontrasenias");
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();
    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private Comandos getCommand(String command) {
    Comandos[] commands = Comandos.values();
    Comandos result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = Comandos.SIN_COMANDO;
    }
    LOG.trace("Se selecciona la accion: {}", result.getCommand());
    return result;
  }

  /**
   * Gets the fields.
   * 
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    Fields[] fields = Fields.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  public String getEnvironment() {
    return environment;
  }

  public void setEnvironment(String environment) {
    this.environment = environment;
  }

}
