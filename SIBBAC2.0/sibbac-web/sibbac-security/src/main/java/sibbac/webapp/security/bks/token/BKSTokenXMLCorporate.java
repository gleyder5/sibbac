package sibbac.webapp.security.bks.token;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import sibbac.webapp.security.bks.BKSXMLCredential;


@XmlRootElement( name = "tokenDefinition" )
@XmlAccessorType( XmlAccessType.FIELD )
public class BKSTokenXMLCorporate implements BKSXMLCredential {

	private String	name;
	private String	userCorp;
	private String	alias;
	private String	localEmitter;
	private String	userID;

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getUserCorp()
	 */
	@Override
	public String getUserCorp() {
		return this.userCorp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getAlias()
	 */
	@Override
	public String getAlias() {
		return this.alias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getLocalEmitter()
	 */
	@Override
	public String getLocalEmitter() {
		return this.localEmitter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getUserID()
	 */
	@Override
	public String getUserID() {
		return this.userID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getDefinitionName()
	 */
	@Override
	public String getDefinitionName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sibbac.webapp.security.bks.BKSXMLCredential#getType()
	 */
	@Override
	public XML_TYPE getType() {
		return XML_TYPE.TOKEN_CORP;
	}

}
