package sibbac.webapp.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import sibbac.webapp.security.bks.BKSCredential;
import sibbac.webapp.security.bks.BKSCredentialsException;
import sibbac.webapp.security.bks.cookie.BKSCookieCredentials;
import sibbac.webapp.security.bks.token.BKSTokenCredentials;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;
import sibbac.webapp.security.session.UserSession;
import sibbac.webapp.security.xmlparse.parsers.XmlParser;
import sibbac.webapp.security.xmlparse.pojos.xml.XmlCorporativo;

@Component
public class SecurityFilter extends OncePerRequestFilter {

  private Logger LOG = LoggerFactory.getLogger(SecurityFilter.class);

  public static final String USER_SESSION = "UserSession";

  @Autowired
  Tmct0UsersBo usersBo;

  @Value("${web.security.sibbac20.cookie.name ?: SIBBAC20UserCookie}")
  private String sibbac20CookieName;

  @Value("${web.security.bks.cookie.name ?: NewUniversalCookie}")
  private String bksCookieName;

  @Value("${web.security.bks.token.name ?: NewUniversalToken}")
  private String bksTokenName;

  @Value("${web.security.cookie.xml.position ?: 6}")
  private String cookieXmlPosition;

  @Value("${web.security.execution ?: Filtro de seguridad - control de acceso:}")
  private String prefix;

  @Value("${web.security.anonymous ?: Anónimo}")
  private String anonymous;

  @Value("${web.security.user.session ?: UserSession}")
  private String userSession;

  @Value("${web.security.error.page ?: /views/security/error.html}")
  private String errorPage;
  // private String errorPage="/views/security/error.html";

  BKSCredential bks = null;

  public SecurityFilter() {
    super();
    if (sibbac20CookieName == null)
      sibbac20CookieName = "SIBBAC20UserCookie";
    if (bksCookieName == null)
      bksCookieName = "NewUniversalCookie";
    if (bksTokenName == null)
      bksTokenName = "NewUniversalToken";
    if (cookieXmlPosition == null)
      cookieXmlPosition = "6";
    if (prefix == null)
      prefix = "Filtro de seguridad - control de acceso:";
    if (anonymous == null)
      anonymous = "Anónimo";
    if (userSession == null)
      userSession = "UserSession";
    if (errorPage == null)
      errorPage = "/views/security/error.html";
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws ServletException, IOException {

    LOG.info("Security Filter::Checking cookies {}", this.getFilterName());

    try {

      HttpServletRequest properRequest = ((HttpServletRequest) request);
      RequestWrapper wrappedRequest = new RequestWrapper(properRequest);
      request = wrappedRequest;

      String post = this.getPostData(request);
      JSONObject obj = new JSONObject(post);
      String action = obj.getString("action");
      String service = obj.getString("service");

      if (checkValidActions(action, service) && "SIBBACServiceSecurity".equals(service)) {
        chain.doFilter(request, response);
      }
      else {

        UserSession userSession = (UserSession) request.getSession().getAttribute(this.userSession);

        if (userSession != null) {

          chain.doFilter(request, response);

        }
        else {

          if (this.checkCredentials(request)) {

            String token = this.getTokenFromCredentials(request);
            XmlCorporativo xmlCorporativo = null;

            try {

              xmlCorporativo = (token != null && !token.equals("")) ? this.getXmlCorporativoFromToken(token) : null;

            }
            catch (IndexOutOfBoundsException | NumberFormatException | NullPointerException ex) {
              LOG.error(ex.getMessage());
              this.refuseConnection(request, response);
            }

            String userId = xmlCorporativo != null ? xmlCorporativo.getUserID() : null;

            if (!"".equals(userId) && userId != null) {

              /*
               * String userName = (xmlCorporativo != null &&
               * xmlCorporativo.getName() != null &&
               * !xmlCorporativo.getName().equals("")) ?
               * xmlCorporativo.getName() : this.anonymous.trim();
               */

              userSession = new UserSession(Long.parseLong(xmlCorporativo.getUserID()), xmlCorporativo.getName());

              LOG.info(userSession.toString());

              request.getSession().setAttribute(this.userSession, userSession);

              chain.doFilter(request, response);

            }

          }
          else {

            this.refuseConnection(request, response);

          }
        }

      }
    }
    catch (Exception e) {
      chain.doFilter(request, response);
    }

  }

  private boolean checkValidActions(String action, String service) {
    return ("LOGIN".equals(String.valueOf(action).toUpperCase())
        || "LOGOUT".equals(String.valueOf(action).toUpperCase())
        || "PASSWORDRESETEADO".equals(String.valueOf(action).toUpperCase())
        || "PARAMETROSCOMPLEJIDAD".equals(String.valueOf(action).toUpperCase())
        || "VALIDARHISTORICOCRONTRASENIAS".equals(String.valueOf(action).toUpperCase())
        || "VALIDARCRONTRASENIAACTUAL".equals(String.valueOf(action).toUpperCase())
        || "AUTENTICADO".equals(String.valueOf(action).toUpperCase())
        || "PERMISOS".equals(String.valueOf(action).toUpperCase()) || "TIENEPERMISO".equals(String.valueOf(action)
        .toUpperCase())) && "SIBBACServiceSecurity".equals(service);
  }

  private String getPostData(HttpServletRequest req) {
	    StringBuilder sb = new StringBuilder();
	    try {
	      BufferedReader reader = req.getReader();
	      reader.mark(10000);
	      String line;
	      do {
	        line = reader.readLine();
	        // LOG.info("LONGITUD LINEA LEIDA -> " + line.length() + " <- LONGITUD LINEA LEIDA");
	        // Si la linea con el contenido del request es muy larga se modifica la marca.
	        // Fix para evitar el error del LOG 'Mark Invalid'
	        if (StringUtils.isNotBlank(line) && line.length() > 2000000) {
	        	reader.mark(5000000);
	        }
	        sb.append(line).append("\n");
	      } while (line != null);
	      reader.reset();
	      // do NOT close the reader here, or you won't be able to get the
	      // post data twice
	    } catch (IOException e) {
	      logger.warn("getPostData couldn't.. get the post data", e); // This
	      // has
	      // happened
	      // if
	      // the
	      // request's
	      // reader
	      // is
	      // closed
	    }
	    return sb.toString();
	  }

  private void refuseConnection(HttpServletRequest request, HttpServletResponse response) throws ServletException,
      IOException {

    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    request.getRequestDispatcher(this.errorPage.trim()).forward(request, response);

  }

  private String getTokenFromCredentials(HttpServletRequest request) {

    String token = null;

    Cookie[] cookies = request.getCookies();
    Cookie credentialCookie = this.getSibbac20OrBKSCookie(cookies);

    token = (credentialCookie != null && credentialCookie.getValue() != null && credentialCookie.getValue() != "") ? credentialCookie
        .getValue() : null;

    if (token == null) {

      String hToken = request.getHeader(this.bksTokenName.trim());
      token = (hToken != null && !hToken.equals("")) ? hToken : null;

    }

    return token;

  }

  public boolean checkCredentials(HttpServletRequest httpServletRequest) {

    return this.examinaCookies(httpServletRequest) || this.examinaToken(httpServletRequest);

  }

  @Override
  public void destroy() {
  }

  private boolean examinaCookies(HttpServletRequest req) {

    Cookie[] cookies = req.getCookies();

    if (cookies != null && cookies.length > 0) {
      String name = null;
      for (Cookie cookie : cookies) {
        name = cookie.getName();
        LOG.info(prefix + "> Cookie: [{}]", this.printCookie(cookie));
        if (name.equalsIgnoreCase(this.sibbac20CookieName.trim())) {
          return this.checkSIBBACCookie(cookie);
        }
        else if (name.equalsIgnoreCase(this.bksCookieName.trim())) {
          return this.checkBKSCookie(cookie);
        }
      }
    }

    return false;
  }

  private Cookie getSibbac20OrBKSCookie(Cookie[] cookies) {

    String cookieName = "";

    try {

      for (Cookie cookie : cookies) {

        cookieName = cookie.getName().trim();

        if (this.sibbac20CookieName.trim().equalsIgnoreCase(cookieName)
            || this.bksCookieName.trim().equalsIgnoreCase(cookieName)) {

          return cookie;
        }
      }

    }
    catch (NullPointerException e) {

      return null;
    }

    return null;
  }

  private boolean checkSIBBACCookie(Cookie cookie) {

    String prefix = "[" + this.prefix + "::checkSIBBACCookie] ";
    LOG.info(prefix + "Examinando una cookie con \"name\" de SIBBAC ({})...", this.sibbac20CookieName);

    try {

      this.bks = BKSCookieCredentials.parse(cookie.getValue());
      return true;

    }
    catch (BKSCredentialsException e) {

      LOG.error(prefix + "ERROR({}): Error parseando la cookie: [{}]", e.getClass().getName(), e.getMessage());
      return false;
    }
  }

  private boolean checkBKSCookie(Cookie cookie) {

    String prefix = "[" + this.prefix + "::checkBKSCookie] ";
    LOG.info(prefix + "Examinando una cookie con \"name\" de BKS ({})...", this.bksCookieName);

    try {

      this.bks = BKSCookieCredentials.parse(cookie.getValue());
      return true;

    }
    catch (BKSCredentialsException e) {
      LOG.error(prefix + "ERROR({}): Error parseando la cookie: [{}]", e.getClass().getName(), e.getMessage());
      return false;
    }
  }

  private String printCookie(Cookie c) {
    StringBuffer sb = new StringBuffer();
    sb.append(c.getName() + ":" + c.getValue());
    sb.append(", version: " + c.getVersion());
    sb.append(", URL: " + c.getDomain() + "/" + c.getPath());
    sb.append(", Time: " + c.getMaxAge());
    sb.append(", Secure: " + c.getSecure());
    sb.append(", Comment: " + c.getComment());
    return sb.toString();
  }

  private boolean examinaToken(HttpServletRequest req) {

    Enumeration<String> headerNames = req.getHeaderNames();

    if (headerNames != null) {

      String headerName = null;
      String header = null;

      while (headerNames.hasMoreElements()) {
        headerName = headerNames.nextElement();
        header = req.getHeader(headerName);

        LOG.info(this.prefix + "> Header({}): [{}]", headerName, header);

        if (headerName.trim().equalsIgnoreCase(this.bksTokenName.trim())) {

          LOG.info(this.prefix + "Examinando un token con \"name\" de SIBBAC ({})...", this.bksTokenName);

          try {

            this.bks = BKSTokenCredentials.parse(header);

            return true;

          }
          catch (BKSCredentialsException e) {
            LOG.error(this.prefix + "ERROR({}): Error parseando el token: [{}]", e.getClass().getName(), e.getMessage());
            return false;
          }
        }
      }
    }

    return false;
  }

  @SuppressWarnings("unused")
  private String getUserIdFromCookie(Cookie cookie) throws IndexOutOfBoundsException, NumberFormatException,
      NullPointerException {

    String encryptedXml = "";
    String decryptedXml = "";
    XmlCorporativo xmlCorporativo = null;
    String[] cookieStructuredValue = (cookie != null && cookie.getValue() != null) ? cookie.getValue().split("#")
        : null;

    if (cookieStructuredValue != null) {

      encryptedXml = cookieStructuredValue[Integer.valueOf(cookieXmlPosition.trim())];

      if (encryptedXml != null && !"".equals(encryptedXml)) {

        decryptedXml = new String(Base64.decodeBase64(encryptedXml.getBytes()));

        if (decryptedXml != null & !"".equals(decryptedXml)) {

          xmlCorporativo = XmlParser.getInstance().parse(decryptedXml);

          if (xmlCorporativo != null) {

            return xmlCorporativo.getUserID();

          }

        }

      }

    }

    return null;
  }

  private XmlCorporativo getXmlCorporativoFromToken(String token) throws IndexOutOfBoundsException,
      NumberFormatException, NullPointerException {

    String encryptedXml = "";
    String decryptedXml = "";
    XmlCorporativo xmlCorporativo = null;
    String[] tokenStructuredValue = (token != null && !"".equals(token)) ? token.split("#") : null;

    if (tokenStructuredValue != null) {

      encryptedXml = tokenStructuredValue[Integer.valueOf(cookieXmlPosition.trim())];

      if (encryptedXml != null && !"".equals(encryptedXml)) {

        decryptedXml = new String(Base64.decodeBase64(encryptedXml.getBytes()));

        if (decryptedXml != null & !"".equals(decryptedXml)) {

          xmlCorporativo = XmlParser.getInstance().parse(decryptedXml);

          return xmlCorporativo;

        }

      }

    }

    return null;

  }

  @SuppressWarnings("unused")
  private void printXmlCorporativo(XmlCorporativo xmlCorporativo) {
    LOG.debug("XmlCorporativo:" + "\n\t Alias: " + xmlCorporativo.getAlias() + "\n\t LocalEmiter: "
        + xmlCorporativo.getLocalEmitter() + "\n\t Name: " + xmlCorporativo.getName() + "\n\t UserCorp: "
        + xmlCorporativo.getUserCorp() + "\n\t UserID: " + xmlCorporativo.getUserID());
  }

  public String getSibbac20CookieName() {
    return sibbac20CookieName;
  }

  public void setSibbac20CookieName(String sibbac20CookieName) {
    this.sibbac20CookieName = sibbac20CookieName;
  }

  public String getBksCookieName() {
    return bksCookieName;
  }

  public void setBksCookieName(String bksCookieName) {
    this.bksCookieName = bksCookieName;
  }

  public String getBksTokenName() {
    return bksTokenName;
  }

  public void setBksTokenName(String bksTokenName) {
    this.bksTokenName = bksTokenName;
  }

  public String getCookieXmlPosition() {
    return cookieXmlPosition;
  }

  public void setCookieXmlPosition(String cookieXmlPosition) {
    this.cookieXmlPosition = cookieXmlPosition;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public String getAnonymous() {
    return anonymous;
  }

  public void setAnonymous(String anonymous) {
    this.anonymous = anonymous;
  }

  public String getUserSession() {
    return userSession;
  }

  public void setUserSession(String userSession) {
    this.userSession = userSession;
  }

  public String getErrorPage() {
    return errorPage;
  }

  public void setErrorPage(String errorPage) {
    this.errorPage = errorPage;
  }

  public BKSCredential getBks() {
    return bks;
  }

  public void setBks(BKSCredential bks) {
    this.bks = bks;
  }

}
