package sibbac.webapp.security.database.dto.assembler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.webapp.security.database.dao.Tmct0ProfilesDao;
import sibbac.webapp.security.database.dto.Tmct0UsersDTO;
import sibbac.webapp.security.database.dto.Tmct0UsersFilterDTO;
import sibbac.webapp.security.database.model.Tmct0Profiles;
import sibbac.webapp.security.database.model.Tmct0Users;

/**
 * Clase que realiza la transformacion entre la entidad y el dto de la tabla TMCT0_USERS
 * 
 * @author Neoris
 *
 */
@Service
public class Tmct0UsersAssembler {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0UsersAssembler.class);

  @Autowired
  Tmct0ProfilesDao tmct0ProfilesDao;

  /**
   * Realiza la conversion entre la entidad y el dto
   * 
   * @param datosUsuario -> Entidad que se quiere trasformar en DTO
   * @return DTO con los datos de la entidad tratados
   */
  public Tmct0UsersDTO getTmct0UsersDTO(Tmct0Users datosUsuario) {

    Tmct0UsersDTO miUsuarioDTO = new Tmct0UsersDTO();
    List<SelectDTO> listaProfiles = new ArrayList<SelectDTO>();

    // Asignación de valores directos.
    miUsuarioDTO.setIdUser(datosUsuario.getIdUser());
    miUsuarioDTO.setUsername(datosUsuario.getUsername());
    miUsuarioDTO.setName(datosUsuario.getName());
    miUsuarioDTO.setLastname(datosUsuario.getLastname());
    miUsuarioDTO.setPassword(datosUsuario.getPassword());
    miUsuarioDTO.setEmail(datosUsuario.getEmail());

    SelectDTO profile;
    for (Tmct0Profiles profileUser : datosUsuario.getProfiles()) {
      // Se controla la excepción cuando se crea un nuevo usuario y se le asignan roles
      if (profileUser.getName() == null) {
        Tmct0Profiles profiUser = tmct0ProfilesDao.getOne(profileUser.getIdProfile());
        profile = new SelectDTO(profiUser.getIdProfile().toString(), (profiUser.getName() + " ("
                                                                      + profiUser.getDescription() + ")").toUpperCase());
      } else {
        profile = new SelectDTO(profileUser.getIdProfile().toString(),
                                (profileUser.getName() + " (" + profileUser.getDescription() + ")").toUpperCase());
      }
      listaProfiles.add(profile);
    }

    miUsuarioDTO.setListaProfiles(listaProfiles);

    return miUsuarioDTO;

  }

  /**
   * Realiza la conversion entre los valores recogidos de la pantalla y el DTO
   * 
   * @param paramsObjects -> Valores que recibe el service de la pantalla y que se quieren transformar en DTO.
   * @return Entidad con los datos del DTO tratados
   */
  public Tmct0UsersDTO getTmct0UsersDTO(Map<String, Object> paramsObjects) {

    LOG.info("getTmct0UsersDTO");
    Tmct0UsersDTO dto = new Tmct0UsersDTO(String.valueOf(paramsObjects.get("username")),
                                          String.valueOf(paramsObjects.get("password")),
                                          String.valueOf(paramsObjects.get("nuevoPassword")));
    return dto;

  }

  /**
   * Realiza la conversion entre los valores recogidos de la pantalla y el DTO
   * 
   * @param paramsObjects -> Valores que recibe el service de la pantalla y que se quieren transformar en DTO.
   * @return Entidad con los datos del DTO tratados
   */
  public Tmct0UsersDTO getTmct0UserDTO(Map<String, Object> paramsObjects) {

    LOG.debug("Entra en getTmct0UserDTO");
    Tmct0UsersDTO dto = new Tmct0UsersDTO();
    if (paramsObjects.get("usuarioSelect") != null) {
      // de la lista de parametros nos quedamos solo con los que estan en el usuarioSelect
      Map<String, Object> usuarioSelect = (Map<String, Object>) paramsObjects.get("usuarioSelect");
      if (usuarioSelect.get("idUser") != null) {
        Long idUser = Long.parseLong(String.valueOf(usuarioSelect.get("idUser")));
        dto.setIdUser(idUser);
      }
      if (usuarioSelect.get("username") != null) {
        dto.setUsername((String) usuarioSelect.get("username"));
      }
      if (usuarioSelect.get("password") != null) {
        dto.setPassword((String) usuarioSelect.get("password"));
      }
      if (usuarioSelect.get("name") != null) {
        dto.setName((String) usuarioSelect.get("name"));
      }
      if (usuarioSelect.get("lastname") != null) {
        dto.setLastname((String) usuarioSelect.get("lastname"));
      }
      if (usuarioSelect.get("email") != null) {
        dto.setEmail((String) usuarioSelect.get("email"));
      }
      List<SelectDTO> listaProfiles = new ArrayList<SelectDTO>();

      SelectDTO selectDTO;
      List<Map<String, String>> perfilesSeleccionados = (List<Map<String, String>>) usuarioSelect.get("listaProfiles");
      for (Map<String, String> perfil : perfilesSeleccionados) {
        selectDTO = new SelectDTO(String.valueOf(perfil.get("key")), String.valueOf(perfil.get("value")));
        listaProfiles.add(selectDTO);
      }
      dto.setListaProfiles(listaProfiles);
    }
    LOG.debug("Sale de getTmct0UserDTO");

    return dto;
  }

  /**
   * Realiza la conversion entre los valores recogidos en el filtro de la pantalla y el DTO
   * 
   * @param paramsObjects -> Valores que recibe el service de la pantalla y que se quieren transformar en DTO.
   * @return Entidad con los datos del DTO tratados
   */
  public Tmct0UsersFilterDTO getTmct0UsersFilterDto(Map<String, Object> paramsObjects) {

    Tmct0UsersFilterDTO miFilterDTO = new Tmct0UsersFilterDTO();

    if (paramsObjects.get("usuario") != null) {
      miFilterDTO.setIdUsuario(Long.parseLong((String) paramsObjects.get("usuario")));
    }
    if (paramsObjects.get("notUsuario") != null) {
      miFilterDTO.setNotUsuario(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notUsuario"))));
    }
    if (paramsObjects.get("perfil") != null) {
      miFilterDTO.setIdProfile(Long.parseLong((String) paramsObjects.get("perfil")));
    }
    if (paramsObjects.get("notPerfil") != null) {
      miFilterDTO.setNotProfile(Boolean.parseBoolean(String.valueOf(paramsObjects.get("notPerfil"))));
    }

    return miFilterDTO;

  }

  /**
   * Realiza la conversion entre el DTO y la entidad
   * 
   * @param datosUsuarioDTO -> Dto que se quiere trasformar en Entidad
   * @return Entidad con los datos del DTO tratados
   */
  public Tmct0Users getTmct0UsersEntidad(Tmct0UsersDTO datosUserDTO) {

    Tmct0Users miEntidad = new Tmct0Users(datosUserDTO.getIdUser(), datosUserDTO.getUsername(),
                                          datosUserDTO.getPassword(), datosUserDTO.getName(),
                                          datosUserDTO.getLastname(), datosUserDTO.getEmail());

    List<Tmct0Profiles> tmct0ProfilesList = new ArrayList<Tmct0Profiles>();

    // Se recorre la lista de perfiles asignados y se forman los campos que componen el listado.
    int iPosicion = 1;
    for (SelectDTO campoPerfil : datosUserDTO.getListaProfiles()) {
      Tmct0Profiles tmct0Profile = new Tmct0Profiles();
      tmct0Profile.setIdProfile(Long.parseLong(campoPerfil.getKey()));
      iPosicion++;
      tmct0ProfilesList.add(tmct0Profile);
    }
    // Asigno la lista de campos a la entidad
    miEntidad.setProfiles(tmct0ProfilesList);

    if (miEntidad.getPassword() == null || miEntidad.getPassword().isEmpty()) {
      miEntidad.setReset(true);
    } else {
      miEntidad.setReset(false);
    }

    return miEntidad;

  }
}
