package sibbac.webapp.security.bks;

import sibbac.webapp.security.bks.cookie.BKSCookieCredentials;
import sibbac.webapp.security.bks.token.BKSTokenCredentials;



/**
 * Interfaz que representa unas credenciales BKS/Santander.
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public interface BKSCredential {

	/**
	 * Caracter de separador de campos en la cadena de acreditacion.
	 */
	public static final String	SEPARATOR	= "#";

	/**
	 * Enumeracion de los diferentes tipos de credenciales soportados.
	 * 
	 * @author Vector ITC Group
	 * @version 4.0
	 * @since 4.0
	 *
	 */
	public static enum CREDENTIALS_TYPE {
		COOKIE, TOKEN
	};

	/**
	 * Enumeracion con los diferentes campos que pueden aparecer en una Cookie BKS.
	 * 
	 * @author Vector ITC Group
	 * @version 4.0
	 * @since 4.0
	 *
	 */
	public static enum FIELDS_COOKIE {
		// Sesion
		ID, IP, FC,
		
		// Exclusivos de las cookies
		VA, FR, RE,
		
		// Datos
		XML, TC, VC, EC, XML_CIFRADO, TF, FIRMA
	};

	/**
	 * Enumeracion con los diferentes campos que pueden aparecer en una Cookie BKS.
	 * 
	 * @author Vector ITC Group
	 * @version 4.0
	 * @since 4.0
	 *
	 */
	public static enum FIELDS_TOKEN {
		// Sesion
		ID, IP, FC,
		
		// Datos
		XML, TC, VT, ET, XML_CIFRADO, TF, FIRMA
	};

	/**
	 * Devuelve el identificador de seguridad.
	 * 
	 * @return Un {@link String} con el ID.
	 */
	public String getId();

	/**
	 * Devuelve la IP para la cual se ha generado la credencial.
	 * 
	 * @return Un {@link String} con la IP.
	 */
	public String getIp();

	/**
	 * Fecha y hora de caducidad en milisegundos.
	 * <p/>
	 * Se usa para comprobar la validez de la cookie.<br/>
	 * Con hacer la comparación: "hora actual en milisegundos &lt; hora de caducidad" se puede confirmar que la credencial no esta caducada.
	 * 
	 * @return Un long con los milisegundos.
	 */
	public long getFechaDeCaducidad();

	/**
	 * Periodo de validez en milisegundos.
	 * <p/>
	 * <strong>SOLO PARA COOKIES</strong> Se usa durante la regeneración de la cookie.<br/>
	 * Se añade el número de milisegundos indicado a la hora actual para obtener la nueva fecha de caducidad.
	 * 
	 * @return Un long con los milisegundos.
	 */
	public long getValidez();

	/**
	 * Fecha y hora de regeneración en milisegundos.
	 * <p/>
	 * <strong>SOLO PARA COOKIES</strong> Se usa para determinar si hay que regenerar la cookie y evitar hacerlo en todas las peticiones.<br/>
	 * Para ello hay que hacer la comprobación de que "hora de regeneración &lt; hora actual &lt; hora de caducidad".
	 * 
	 * @return Un long con los milisegundos.
	 */
	public long getFechaDeRegeneracion();

	/**
	 * Periodo de regeneración en milisegundos.
	 * <p/>
	 * <strong>SOLO PARA COOKIES</strong> Se usa durante la regeneración de la cookie.<br/>
	 * Se añade el número de milisegundos indicado a la hora actual para obtener la nueva fecha de regeneración.
	 * 
	 * @return Un long con los milisegundos.
	 */
	public long getPeriodoDeRegeneracion();

	/**
	 * XML con los datos del usuario en claro.
	 * <p/>
	 * Aquí se pondrán la mayoría de los datos que identifiquen al usuario: uid, nif, nombre, etc.<br/>
	 * Este XML va codificado en BASE64.
	 * 
	 * @return Un {@link String} con la representaci&oacute;n de los datos de usuario.
	 * @see {@link BKSCookieCredentials} y {@link BKSTokenCredentials} para m&aacute;s informaci&oacute;n.
	 */
	public String getXml();

	public String getTipoDeCifrado();

	public String getVersion();

	public String getEmisor();

	public String getXmlCifrado();

	public String getTipoDeFirma();

	public String getFirma();

	public CREDENTIALS_TYPE getType();

	public boolean isFromCookie();

	public boolean isXMLCorporativo();

}
