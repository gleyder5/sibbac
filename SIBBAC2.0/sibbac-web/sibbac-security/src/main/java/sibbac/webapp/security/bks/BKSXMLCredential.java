package sibbac.webapp.security.bks;


/**
 * Interfaz de los diferentes tipos de formatos de XML de datos de usuario recibidos en una {@link BKSCredential}.
 * 
 * @author Vector ITC Group
 * @version 4.0
 * @since 4.0
 *
 */
public interface BKSXMLCredential {

	/**
	 * Enumeracion de los diferentes tipos de credenciales soportados.
	 * 
	 * @author Vector ITC Group
	 * @version 4.0
	 * @since 4.0
	 *
	 */
	public static enum XML_TYPE {
		COOKIE_LOCAL, COOKIE_CORP, TOKEN_LOCAL, TOKEN_CORP
	};

	/**
	 * El campo: "alias" de la credencial.
	 * 
	 * @return Un {@link String} con el valor del campo.
	 */
	public String getAlias();

	/**
	 * El campo: "definitionName" de la credencial.
	 * 
	 * @return Un {@link String} con el valor del campo.
	 */
	public String getDefinitionName();

	/**
	 * El campo: "localEmitter" de la credencial.
	 * 
	 * @return Un {@link String} con el valor del campo.
	 */
	public String getLocalEmitter();

	/**
	 * El campo: "name" de la credencial.
	 * 
	 * @return Un {@link String} con el valor del campo.
	 */
	public String getName();

	/**
	 * El campo: "userCorp" de la credencial.
	 * 
	 * @return Un {@link String} con el valor del campo.
	 */
	public String getUserCorp();

	/**
	 * El campo: "userID" de la credencial.
	 * 
	 * @return Un {@link String} con el valor del campo.
	 */
	public String getUserID();

	/**
	 * El tipo de XML recibido.
	 * 
	 * @return Un {@link XML_TYPE} que indica el tipo de dato recibido.
	 */
	public XML_TYPE getType();

}
