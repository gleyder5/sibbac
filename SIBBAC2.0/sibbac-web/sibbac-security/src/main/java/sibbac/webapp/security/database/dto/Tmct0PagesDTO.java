package sibbac.webapp.security.database.dto;

import java.util.List;

import sibbac.business.wrappers.common.SelectDTO;


/**
 * DTO para la gestion de TMCT0_PAGES
 * @author Neoris
 *
 */
public class Tmct0PagesDTO {

  /** idPage - Identificador de la página */ 
  private Long  idPage;
  /** name - Nombre de la página */
  private String       name;
  /** description - Descripcion de la página */
  private String       description;
  /** url - url de la página */
  private String       url;
  /** idParent - Identificador de la página padre dentro del menu */
  private Integer  idParent;
  
  /** sRoles - Cadena con todos los roles separados por comas */
  private String sProfiles;
  
  /** listaProfiles - Perfiles que tienen permiso para acceder a esta página */
  private List<SelectDTO> listaProfiles;
  
  
  public Tmct0PagesDTO(){
    
  }


  public Tmct0PagesDTO(Long idPage, String name, String description, String url, Integer idParent, List<SelectDTO> roles) {
    this.idPage = idPage;
    this.name = name;
    this.description = description;
    this.url = url;
    this.idParent = idParent;
    this.listaProfiles = roles;
  }


  public Long getIdPage() {
    return idPage;
  }


  public void setIdPage(Long idPage) {
    this.idPage = idPage;
  }


  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public String getDescription() {
    return description;
  }


  public void setDescription(String description) {
    this.description = description;
  }


  public String getUrl() {
    return url;
  }


  public void setUrl(String url) {
    this.url = url;
  }


  public Integer getIdParent() {
    return idParent;
  }


  public void setIdParent(Integer idparent) {
    this.idParent = idparent;
  }


  public List<SelectDTO> getListaProfiles() {
    return listaProfiles;
  }


  public void setListaProfiles(List<SelectDTO> listaProfiles) {
    this.listaProfiles = listaProfiles;
  }


  public String getsProfiles() {
    return sProfiles;
  }


  public void setsProfiles(String sProfiles) {
    this.sProfiles = sProfiles;
  };
  
  
  
}
