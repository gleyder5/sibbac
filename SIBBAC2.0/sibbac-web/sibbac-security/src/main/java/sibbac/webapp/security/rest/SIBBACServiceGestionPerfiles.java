package sibbac.webapp.security.rest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.bo.Tmct0ProfilesBo;
import sibbac.webapp.security.database.dao.Tmct0ActionDao;
import sibbac.webapp.security.database.dao.Tmct0ActionPageDao;
import sibbac.webapp.security.database.dao.Tmct0PagesDao;
import sibbac.webapp.security.database.dao.Tmct0ProfilesDao;
import sibbac.webapp.security.database.dao.Tmct0ProfilesDaoImp;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.dto.Tmct0ActionPageProfileDTO;
import sibbac.webapp.security.database.dto.Tmct0PagesProfileDTO;
import sibbac.webapp.security.database.dto.Tmct0PerfilFilterDTO;
import sibbac.webapp.security.database.dto.Tmct0ProfilesDTO;
import sibbac.webapp.security.database.dto.assembler.Tmct0ProfileAssembler;
import sibbac.webapp.security.database.model.Tmct0Actions;
import sibbac.webapp.security.database.model.Tmct0Pages;
import sibbac.webapp.security.database.model.Tmct0Profiles;
import sibbac.webapp.security.database.model.Tmct0Users;

@SIBBACService
public class SIBBACServiceGestionPerfiles implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGestionPerfiles.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0UsersDao tmct0UsersDao;

  @Autowired
  Tmct0ProfilesDao tmct0ProfilesDao;

  @Autowired
  Tmct0PagesDao tmct0PagesDao;

  @Autowired
  Tmct0ProfileAssembler tmct0PerfilAssembler;

  @Autowired
  Tmct0ProfilesDaoImp tmct0PerfilesDaoImp;

  @Autowired
  Tmct0ProfilesBo tmct0ProfilesBo;

  @Autowired
  Tmct0ActionDao tmct0ActionDao;

  @Autowired
  Tmct0ActionPageDao tmct0ActionPageDao;

  /**
   * The Enum ComandosPermisosAccesos.
   */
  private enum ComandosPermisosAccesos {

    /** Recupera los profiles para cargar los datos del filtro */
    RECUPERAR_PERFILES("RecuperarPerfiles"),

    /** Recupera la lista de usuarios */
    RECUPERAR_USUARIOS("RecuperarUsuarios"),

    /** Recupera la lista de acciones */
    RECUPERAR_ACCIONES("RecuperarAcciones"),

    /** Recupera la lista de paginas con las acciones que se pueden hacer en cada una de ellas */
    RECUPERAR_PAGINAS_ACCIONES("RecuperarPaginasAcciones"),

    /** Inicializa el grid con todas las plantillas actuales */
    RECUPERAR_PERFILES_FILTRO("RecuperarPerfilesFiltro"),

    /** Crea un nuevo perfil con los datos recibidos */
    CREAR_PERFIL("CrearPerfil"),

    /** Modifica el perfil */
    MODIFICAR_PERFIL("ModificarPerfil"),

    /** Eliminar perfiles */
    ELIMINAR_PERFILES("EliminarPerfiles"),

    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Plantillas.
     *
     * @param command the command
     */
    private ComandosPermisosAccesos(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * The Enum Campos plantilla .
   */
  private enum FieldsPlantilla {
    PLANTILLA("plantilla");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsPlantilla(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {

    Map<String, String> filters = webRequest.getFilters();
    List<Map<String, String>> parametros = webRequest.getParams();
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosPermisosAccesos command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceGestionPerfiles");

      switch (command) {
        case RECUPERAR_PERFILES:
          result.setResultados(cargarPerfiles());
          break;
        case RECUPERAR_USUARIOS:
          result.setResultados(cargarUsuarios());
          break;
        case RECUPERAR_ACCIONES:
          result.setResultados(cargarAcciones());
          break;
        case RECUPERAR_PAGINAS_ACCIONES:
          result.setResultados(cargarPaginasAcciones());
          break;

        case RECUPERAR_PERFILES_FILTRO:
          result.setResultados(getPerfilesFiltro(paramsObjects));
          break;
        case CREAR_PERFIL:
          result.setResultados(crearPerfil(paramsObjects));
          break;
        case MODIFICAR_PERFIL:
          result.setResultados(modificarPerfil(paramsObjects));
          break;
        case ELIMINAR_PERFILES:
          result.setResultados(eliminarPerfiles(paramsObjects));
          break;

        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceGestionPerfiles");

    return result;
  }

  /**
   * Obtencion de los profiles actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPerfiles() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarPerfiles");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaPerfiles = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Tmct0Profiles> tmct0Profiles = tmct0ProfilesDao.findAll();
      for (Tmct0Profiles tmct0Profile : tmct0Profiles) {
        select = new SelectDTO(String.valueOf(tmct0Profile.getIdProfile()), (tmct0Profile.getName()) + " "
                                                                            + tmct0Profile.getDescription());
        listaPerfiles.add(select);
      }
      mSalida.put("listaPerfiles", listaPerfiles);

    } catch (Exception e) {
      LOG.error("Error metodo init - cargarPerfiles " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.trace("Fin recuperación datos para la lista cargarPerfiles");

    return mSalida;
  }

  /**
   * Obtencion de los usuarios actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarUsuarios() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarUsuarios");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaUsuarios = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Tmct0Users> tmct0Users = tmct0UsersDao.findUsersOrderByUsername();
      for (Tmct0Users tmct0User : tmct0Users) {
        select = new SelectDTO(
                               String.valueOf(tmct0User.getIdUser()),
                               (tmct0User.getUsername() + " - " + tmct0User.getName() + " " + tmct0User.getLastname()).toUpperCase());
        listaUsuarios.add(select);
      }
      mSalida.put("listaUsuarios", listaUsuarios);

    } catch (Exception e) {
      LOG.error("Error metodo init - cargarUsuarios " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.trace("Fin recuperación datos para la lista cargarUsuarios");

    return mSalida;
  }

  /**
   * Obtencion de la lista de acciones actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarAcciones() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarAcciones");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaAcciones = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Tmct0Actions> listaAccionesActuales = tmct0ActionDao.findActionsOrderByOrderAction();
      for (Tmct0Actions tmct0Action : listaAccionesActuales) {
        select = new SelectDTO(String.valueOf(tmct0Action.getIdAction()), tmct0Action.getName(),
                               tmct0Action.getDescription());
        listaAcciones.add(select);
      }
      mSalida.put("listaAcciones", listaAcciones);

    } catch (Exception e) {
      LOG.error("Error metodo init - cargarUsuarios " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales - cargarAcciones");
    }

    LOG.trace("Fin recuperación datos para la lista cargarAcciones");

    return mSalida;
  }

  /**
   * Obtencion de la lista de paginas con las acciones que se pueden hacer en cada una de ellas - Necesaria para poder
   * asignar a un rol nuevo las acciones sobre las paginas.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPaginasAcciones() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarPaginasAcciones");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<Tmct0PagesProfileDTO> listaPaginasAcciones = new ArrayList<Tmct0PagesProfileDTO>();

      List<Tmct0Pages> listaPaginas = tmct0PagesDao.getPagesWithUrl();

      for (Tmct0Pages tmct0Pages : listaPaginas) {

        Tmct0PagesProfileDTO paginaProfileDTO = new Tmct0PagesProfileDTO();
        List<Tmct0ActionPageProfileDTO> listaActionPageProfileDTO = new ArrayList<Tmct0ActionPageProfileDTO>();

        paginaProfileDTO.setIdPage(tmct0Pages.getIdPage());
        // paginaProfileDTO.setIdProfile(datosProfile.getIdProfile());
        paginaProfileDTO.setName(tmct0Pages.getName());
        paginaProfileDTO.setDescription(tmct0Pages.getDescription());

        // Se recuperan todas las acciones y si la pagina la contiene o no, si el idactionpage es 0 no la contiene
        List<Object[]> listaAccionesPagina = tmct0ActionPageDao.getActionsByPage(tmct0Pages.getIdPage());

        for (Object[] tmct0ActionPage : listaAccionesPagina) {
          Tmct0ActionPageProfileDTO actionPageProfileDTO = new Tmct0ActionPageProfileDTO();

          BigInteger idActionPage = (BigInteger) tmct0ActionPage[0];
          BigInteger idAction = (BigInteger) tmct0ActionPage[1];

          actionPageProfileDTO.setIdAction(idAction.longValue());
          actionPageProfileDTO.setIdActionPage(idActionPage.longValue());
          actionPageProfileDTO.setbAplicaProfile(false); // Esta estructura es generia para que el usuario pueda empezar
                                                         // asignar los permisos.

          listaActionPageProfileDTO.add(actionPageProfileDTO);

        }

        paginaProfileDTO.setListaAccionesPaginaProfile(listaActionPageProfileDTO);

        listaPaginasAcciones.add(paginaProfileDTO);

      }
      mSalida.put("listaPaginasAcciones", listaPaginasAcciones);

    } catch (Exception e) {
      LOG.error("Error metodo init - cargarPaginasAcciones " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales - cargarPaginasAcciones");
    }

    LOG.trace("Fin recuperación datos para la lista cargarPaginasAcciones");

    return mSalida;
  }

  /**
   * Obtencion de todas las perfiles actuales que cumplen el filtro para la carga del grid inicial
   * 
   * @param filters
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> getPerfilesFiltro(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Entra en  getPerfilesFiltro ");

    Map<String, Object> result = new HashMap<String, Object>();
    List<Object> listaPerfilessFiltroDto = new ArrayList<Object>();

    try {

      // Se recuperan los perfiles que cumplen con los criterios establecidos

      Tmct0PerfilFilterDTO filters = tmct0PerfilAssembler.getTmct0PerfilFilterDTO(paramsObjects);

      List<Tmct0Profiles> listaPerfilesFiltro = tmct0PerfilesDaoImp.findPerfilesFiltro(filters);

      // Se transforman a lista de DTO
      for (Tmct0Profiles tmct0Perfil : listaPerfilesFiltro) {
        try {
          Tmct0ProfilesDTO miDTO = tmct0PerfilAssembler.getTmct0ProfileDTO(tmct0Perfil);
          listaPerfilessFiltroDto.add(miDTO);
        } catch (Exception e) {
          LOG.error("Error metodo getPaginasFiltro -  miAssembler.getTmct0InformeDto " + e.getMessage(), e);
          // TODO: handle exception
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo getPaginasFiltro -  " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la peticion de datos con las opciones de busqueda seleccionadas");
    }

    result.put("listaPerfilesFiltroDto", listaPerfilessFiltroDto);

    LOG.debug("Fin getPerfilesFiltro");

    return result;

  }

  /**
   * Crear el perfil con los datos recibidos como parametro
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> crearPerfil(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Inicio crearPerfil");

    try {

      // Se convierte el objeto en entidad
      Tmct0Profiles tmct0Profile = tmct0PerfilAssembler.getTmct0ProfileEntity(paramsObjects);

      // Se guarda la información
      tmct0ProfilesBo.saveProfile(tmct0Profile);

      LOG.debug("Perfil creado correctamente");

    } catch (Exception e) {
      LOG.error("Error metodo init - crearPerfil " + e.getMessage(), e);
    }

    LOG.debug("Fin crearPerfil");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return getPerfilesFiltro(paramsObjects);
  }

  /**
   * Modificación del perfil pasado como parametro
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> modificarPerfil(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Inicio modificarPerfil");

    try {

      // Se convierte el objeto en entidad
      Tmct0Profiles tmct0Profile = tmct0PerfilAssembler.getTmct0ProfileEntity(paramsObjects);

      // Se guarda la información
      tmct0ProfilesBo.saveProfile(tmct0Profile);

      LOG.debug("Perfil modificado correctamente");

    } catch (Exception e) {
      LOG.error("Error metodo init - modificarPerfil " + e.getMessage(), e);
    }

    LOG.debug("Fin modificarPerfil");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return getPerfilesFiltro(paramsObjects);
  }

  /**
   * Elimina las plantillas seleccionadas.
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> eliminarPerfiles(Map<String, Object> paramsObject) throws SIBBACBusinessException {

    LOG.debug("Inicio eliminarPerfiles");

    Map<String, Object> map = new HashMap<String, Object>();

    List<Object> ListInformesBorrar = (List<Object>) paramsObject.get("listaPerfilesBorrar");

    for (Object idPerfilBorrarObject : ListInformesBorrar) {

      Long idPerfilBorrar = Long.parseLong(String.valueOf(idPerfilBorrarObject));
      LOG.info("Borramos el perfil con id " + idPerfilBorrar.toString());
      try {
        tmct0ProfilesDao.delete(idPerfilBorrar);
      } catch (Exception e) {
        LOG.error("Error al borrar el informe " + idPerfilBorrar);
        LOG.error(e.getMessage());
        throw new SIBBACBusinessException("No se ha podido borrar el perfil seleccionado - Consulte con informatica ");
      }
    }

    LOG.info("Pefiles eliminados correctamente");

    LOG.debug("Fin eliminarPerfiles");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return getPerfilesFiltro(paramsObject);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    FieldsPlantilla[] fields = FieldsPlantilla.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosPermisosAccesos getCommand(String command) {
    ComandosPermisosAccesos[] commands = ComandosPermisosAccesos.values();
    ComandosPermisosAccesos result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosPermisosAccesos.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

}
