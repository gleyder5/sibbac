package sibbac.webapp.security.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.dao.Tmct0ParametrosSeguridadDao;
import sibbac.webapp.security.database.dto.Tmct0ParametrosSeguridadDTO;
import sibbac.webapp.security.database.model.Tmct0ParametrosSeguridad;

@SIBBACService
public class SIBBACServiceParametrosSeguridad implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceParametrosSeguridad.class);

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  // Constantes con las claves de los parametros
  final static String longitud = "LONGITUD_PASSWORD";
  final static String mayusculas = "NMAYUSCULAS";
  final static String cotrasenasRepetidas = "NPASSWORD_REPE";
  final static String intentosBloqueo = "NINTENTOS_BLOQUEO";
  final static String avisoExpiracion = "NDIAS_AVISO";
  final static String tiempoExpiracion = "NDIAS_VALIDEZ";
  final static String caracteresExtranos = "NCARAC_ESPECIALES";
  final static String numeros = "NDIGITOS";

  @Autowired
  Tmct0ParametrosSeguridadDao tct0ParametrosSeguridadDao;

  /**
   * The Enum ComandosParametrosSeguridad.
   */
  private enum ComandosParametrosSeguridad {

    CONSULTARPARAMETROS("consultarParametros"),
    GUARDARPARAMETROS("guardarParametros"),
    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos ParametrosSeguridad.
     *
     * @param command the command
     */
    private ComandosParametrosSeguridad(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */

  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {

    Map<String, String> filters = webRequest.getFilters();
    List<Map<String, String>> parametros = webRequest.getParams();
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosParametrosSeguridad command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceParametrosSeguridad");

      switch (command) {
        case CONSULTARPARAMETROS:
          result.setResultados(consultarParametros());
          break;
        case GUARDARPARAMETROS:
          result.setResultados(guardarParametros(paramsObjects));
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceParametrosSeguridad");

    return result;
  }

  /**
   * Obtencion de los parametros actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  public Map<String, Object> consultarParametros() throws SIBBACBusinessException {

    LOG.trace("Inicio consultarParametros");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<Tmct0ParametrosSeguridad> lTmct0ParametrosSeguridad = tct0ParametrosSeguridadDao.findAll();

      Tmct0ParametrosSeguridadDTO miDto = new Tmct0ParametrosSeguridadDTO();

      for (Tmct0ParametrosSeguridad tmct0ParametrosSeguridad : lTmct0ParametrosSeguridad) {
        switch (tmct0ParametrosSeguridad.getClave()) {
          case longitud:
            miDto.setLongitud(tmct0ParametrosSeguridad.getValor());
            break;
          case caracteresExtranos:
            miDto.setCaracteresExtranos(tmct0ParametrosSeguridad.getValor());
            break;
          case avisoExpiracion:
            miDto.setAvisoExpiracion(tmct0ParametrosSeguridad.getValor());
            break;
          case tiempoExpiracion:
            miDto.setTiempoExpiracion(tmct0ParametrosSeguridad.getValor());
            break;
          case numeros:
            miDto.setNumeros(tmct0ParametrosSeguridad.getValor());
            break;
          case intentosBloqueo:
            miDto.setIntentosBloqueo(tmct0ParametrosSeguridad.getValor());
            break;
          case mayusculas:
            miDto.setMayusculas(tmct0ParametrosSeguridad.getValor());
            break;
          case cotrasenasRepetidas:
            miDto.setCotraseñasRepetidas(tmct0ParametrosSeguridad.getValor());
            break;

          default:
            break;
        }
      }
      mSalida.put("parametros", miDto);

    } catch (Exception e) {
      LOG.error("Error metodo init - consultarParametros " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al buscar los valores de los parametros actuales");
    }

    LOG.trace("Fin Inicio consultarParametros");

    return mSalida;
  }

  /**
   * Guarda los nuevos valores de los parametros
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> guardarParametros(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.trace("Inicio guardarParametros");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    Tmct0ParametrosSeguridad miEntidad = null;
    Integer iValor = 0;

    try {
      // Se actualiza TODOS LOS PARAMETROS RECIBIDOS
      // mayusculas
      iValor = (Integer) paramsObjects.get("mayusculas");
      miEntidad = tct0ParametrosSeguridadDao.findOne(mayusculas);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // numeros
      iValor = (Integer) paramsObjects.get("numeros");
      miEntidad = tct0ParametrosSeguridadDao.findOne(numeros);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // caracteresExtranos
      iValor = (Integer) paramsObjects.get("caracteresExtranos");
      miEntidad = tct0ParametrosSeguridadDao.findOne(caracteresExtranos);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // ongitud
      iValor = (Integer) paramsObjects.get("longitud");
      miEntidad = tct0ParametrosSeguridadDao.findOne(longitud);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // tiempoExpiracion :
      iValor = (Integer) paramsObjects.get("tiempoExpiracion");
      miEntidad = tct0ParametrosSeguridadDao.findOne(tiempoExpiracion);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // avisoExpiracion
      iValor = (Integer) paramsObjects.get("avisoExpiracion");
      miEntidad = tct0ParametrosSeguridadDao.findOne(avisoExpiracion);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // intentosBloqueo
      iValor = (Integer) paramsObjects.get("intentosBloqueo");
      miEntidad = tct0ParametrosSeguridadDao.findOne(intentosBloqueo);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

      // cotrasenasRepetidas
      iValor = (Integer) paramsObjects.get("cotrasenasRepetidas");
      miEntidad = tct0ParametrosSeguridadDao.findOne(cotrasenasRepetidas);
      miEntidad.setValor(iValor);
      tct0ParametrosSeguridadDao.save(miEntidad);

    } catch (Exception e) {
      LOG.error("Error metodo init - guardarParametros " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error al actualizar los valores de los parametros");
    }

    LOG.trace("Fin Inicio guardarParametros");

    return consultarParametros();
  }

  @Override
  public List<String> getAvailableCommands() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<String> getFields() {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosParametrosSeguridad getCommand(String command) {
    ComandosParametrosSeguridad[] commands = ComandosParametrosSeguridad.values();
    ComandosParametrosSeguridad result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosParametrosSeguridad.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

}
