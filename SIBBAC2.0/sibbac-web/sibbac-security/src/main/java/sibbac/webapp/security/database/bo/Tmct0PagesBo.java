package sibbac.webapp.security.database.bo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.database.dao.Tmct0PagesDao;
import sibbac.webapp.security.database.model.Tmct0Pages;

@Service
public class Tmct0PagesBo extends AbstractBo<Tmct0Pages, Long, Tmct0PagesDao> {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Tmct0PagesBo.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	Tmct0PagesDao tmct0PagesDao;

	public List<Tmct0Pages> getOpcionesPrincipales() {
		return tmct0PagesDao.getPagesWithoutUrl();
	}
	
	public List<Tmct0Pages> getPagesByIdParent(Integer idParent) {
		return tmct0PagesDao.getPagesByIdParent(idParent);
	}
	
	public List<Tmct0Pages> getPages() {
		return tmct0PagesDao.getPages();
	}

}
