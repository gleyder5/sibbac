/**
 * 
 */
package sibbac.webapp.security.xmlparse.parsers;

import java.io.Reader;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.Format.TextMode;
import org.jdom.output.XMLOutputter;

//import com.trenti.halee.logging.HaleeLogger;
import sibbac.webapp.security.xmlparse.pojos.xml.XmlCorporativo;

/**
 * Parsea una string que contiene un xml en texto plano en el formato XML Local o XML Corporativo
 * @author dortega
 * @version 1.0
 * 
 */
public class XmlParser {

	/**
	 * Private constructor prevents instantiation from other classes
	 */
	private XmlParser() {
	}

	/**
	 * SingletonHolder is loaded on the first execution of
	 * xmlparse.getInstance() or the first access to
	 * SingletonHolder.INSTANCE, not before.
	 */
	private static class SingletonHolder {
		public static final XmlParser INSTANCE = new XmlParser();
	}

	/**
	 * 
	 * @return
	 */
	public static XmlParser getInstance() {
		return SingletonHolder.INSTANCE;
	}

	/**
	 * 
	 * @param plainXml
	 * @return XmlCorporativo o null en caso de error
	 */
	public XmlCorporativo parse(String plainXml) {
		XmlCorporativo result = new XmlCorporativo();
		SAXBuilder builder = new SAXBuilder();
		Reader in = new StringReader(plainXml);
		Document doc = null;
		Element tokenDefinition = null;
		try {
			doc = builder.build(in);
			tokenDefinition = doc.getRootElement();
			
			//XML Local
			result.setUserID(tokenDefinition.getChildText("userID"));
			result.setName(tokenDefinition.getChildText("name"));
			result.setAlias(tokenDefinition.getChildText("alias"));
			
			//XML Corporativo
			result.setUserCorp(tokenDefinition.getChildText("userCorp"));
			result.setLocalEmitter(tokenDefinition.getChildText("localEmitter"));

		} catch (Exception e) {
			result = null;
		}
		return result;
	}
	
	/**
	 * 
	 * @param xmlCorporativo
	 * @return
	 */
	public String parse(XmlCorporativo xmlCorporativo){
		String result = "";
		Element tokenDefinition = new Element("tokenDefinition");
		Document myDocument = new Document(tokenDefinition);
		
		Element name = new Element("name");		
		name.addContent(xmlCorporativo.getName());
		tokenDefinition.addContent(name);
		
		Element userID = new Element("userID");		
		userID.addContent(xmlCorporativo.getUserID());
		tokenDefinition.addContent(userID);
		
		Element alias = new Element("alias");		
		alias.addContent(xmlCorporativo.getAlias());
		tokenDefinition.addContent(alias);
		
		if(xmlCorporativo.getUserCorp() != null){
			Element userCorp = new Element("userCorp");		
			userCorp.addContent(xmlCorporativo.getUserCorp());
			tokenDefinition.addContent(userCorp);
			
			Element localEmitter = new Element("localEmitter");		
			localEmitter.addContent(xmlCorporativo.getLocalEmitter());
			tokenDefinition.addContent(localEmitter);
		}
		try {
			Format format = Format.getCompactFormat();
			format.setEncoding("ISO-8859-1");
			format.setLineSeparator("");
			format.setTextMode(TextMode.NORMALIZE);
		    XMLOutputter xmlOutputter = new XMLOutputter(format);
		    
		    result = xmlOutputter.outputString(myDocument);
		} catch (Exception e) {
//		    HaleeLogger.error(this, e);
		}
		return result;
	}
	
}
