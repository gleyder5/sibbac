package sibbac.webapp.security.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import sibbac.business.wrappers.common.SelectDTO;
import sibbac.common.SIBBACBusinessException;
import sibbac.webapp.annotations.SIBBACService;
import sibbac.webapp.beans.WebRequest;
import sibbac.webapp.beans.WebResponse;
import sibbac.webapp.business.SIBBACServiceBean;
import sibbac.webapp.security.database.bo.Tmct0UsersBo;
import sibbac.webapp.security.database.dao.Tmct0ProfilesDao;
import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.dao.Tmct0UsersDaoImp;
import sibbac.webapp.security.database.dto.Tmct0UsersDTO;
import sibbac.webapp.security.database.dto.Tmct0UsersFilterDTO;
import sibbac.webapp.security.database.dto.assembler.Tmct0UsersAssembler;
import sibbac.webapp.security.database.model.Tmct0Profiles;
import sibbac.webapp.security.database.model.Tmct0Users;

@SIBBACService
public class SIBBACServiceGestionUsuarios implements SIBBACServiceBean {

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(SIBBACServiceGestionUsuarios.class);

  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONSTANTES

  public static final String RESULT_LISTA = "lista";

  private final static String MESSAGE_ERROR = "Se ha producido un error interno.";

  @Autowired
  Tmct0UsersDao tmct0UsersDao;

  @Autowired
  Tmct0ProfilesDao tmct0ProfilesDao;

  @Autowired
  Tmct0UsersAssembler tmct0UsersAssembler;

  @Autowired
  Tmct0UsersDaoImp tmct0UsersDaoImp;

  @Autowired
  Tmct0UsersBo tmct0UsersBo;

  /**
   * The Enum ComandosUsuarios.
   */
  private enum ComandosUsuarios {

    BUSCARUSUSARIOS("BuscarUsuarios"),
    RECUPERARUSUARIOS("RecuperarUsuarios"),
    RECUPERARPERFILES("RecuperarPerfiles"),
    BORRARUSUARIOS("BorrarUsuarios"),
    MODIFICARUSUARIO("ModificarUsuario"),
    CREARUSUARIO("CrearUsuario"),
    RESETEARPASSWORD("ResetearPassword"),
    SIN_COMANDO("");

    /** The command. */
    private String command;

    /**
     * Instantiates a new comandos Usuarios.
     *
     * @param command the command
     */
    private ComandosUsuarios(String command) {
      this.command = command;
    }

    /**
     * Gets the command.
     *
     * @return the command
     */
    public String getCommand() {
      return command;
    }

  }

  /**
   * The Enum Campos Usuario .
   */
  private enum FieldsUsuario {
    ID_USER("ID_USER"),
    USERNAME("USERNAME"),
    PASSWORD("PASSWORD"),
    NAME("NAME"),
    LASTNAME("LASTNAME");

    /** The field. */
    private String field;

    /**
     * Instantiates a new fields.
     *
     * @param field the field
     */
    private FieldsUsuario(String field) {
      this.field = field;
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public String getField() {
      return field;
    }
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#process(sibbac.webapp.beans. WebRequest)
   */
  /**
   * Process.
   *
   * @param webRequest the web request
   * @return the web response
   * @throws SIBBACBusinessException the SIBBAC business exception
   */
  @Override
  public WebResponse process(WebRequest webRequest) throws SIBBACBusinessException {

    Map<String, String> filters = webRequest.getFilters();
    List<Map<String, String>> parametros = webRequest.getParams();
    Map<String, Object> paramsObjects = webRequest.getParamsObject();
    WebResponse result = new WebResponse();
    Map<String, Object> resultados = new HashMap<String, Object>();
    ComandosUsuarios command = getCommand(webRequest.getAction());

    try {
      LOG.debug("Entro al servicio SIBBACServiceGestionUsuarios");

      switch (command) {
        case BUSCARUSUSARIOS:
          result.setResultados(buscarUsuarios(paramsObjects));
          break;
        case RECUPERARUSUARIOS:
          result.setResultados(cargarUsuarios());
          break;
        case RECUPERARPERFILES:
          result.setResultados(cargarPerfiles());
          break;
        case BORRARUSUARIOS:
          result.setResultados(borrarUsuarios(paramsObjects));
          break;
        case RESETEARPASSWORD:
          result.setResultados(resetearPassword(paramsObjects));
          break;
        case MODIFICARUSUARIO:
          result.setResultados(grabarUsuarioExistente(paramsObjects));
          break;
        case CREARUSUARIO:
          result.setResultados(grabarNuevoUsuario(paramsObjects));
          break;
        default:
          result.setError("An action must be set. Valid actions are: " + command);
      } // switch
    } catch (SIBBACBusinessException e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(e.getMessage());
    } catch (Exception e) {
      resultados.put("status", "KO");
      result.setResultados(resultados);
      result.setError(MESSAGE_ERROR);
    }

    LOG.debug("Salgo del servicio SIBBACServiceGestionUsuarios");

    return result;
  }

  /**
   * Obtencion de los usuarios actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarUsuarios() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarUsuarios");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaUsuarios = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Tmct0Users> tmct0Users = tmct0UsersDao.findUsersOrderByUsername();
      for (Tmct0Users tmct0User : tmct0Users) {
        select = new SelectDTO(
                               String.valueOf(tmct0User.getIdUser()),
                               (tmct0User.getUsername() + " " + tmct0User.getName() + " " + tmct0User.getLastname()).toUpperCase());
        listaUsuarios.add(select);
      }
      mSalida.put("listaUsuarios", listaUsuarios);

    } catch (Exception e) {
      LOG.error("Error metodo init - cargarUsuarios " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.trace("Fin recuperación datos para la lista cargarUsuarios");

    return mSalida;
  }

  /**
   * Obtencion de los roles actuales
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> cargarPerfiles() throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista cargarPerfiles");

    Map<String, Object> mSalida = new HashMap<String, Object>();

    try {

      List<SelectDTO> listaPerfiles = new ArrayList<SelectDTO>();
      SelectDTO select = null;

      List<Tmct0Profiles> tmct0Profiles = tmct0ProfilesDao.findAll();
      for (Tmct0Profiles tmct0Profile : tmct0Profiles) {
        select = new SelectDTO(String.valueOf(tmct0Profile.getIdProfile()),
                               (tmct0Profile.getName() + " (" + tmct0Profile.getDescription()).toUpperCase() + ")");
        listaPerfiles.add(select);
      }
      mSalida.put("listaPerfiles", listaPerfiles);

    } catch (Exception e) {
      LOG.error("Error metodo init - cargarPerfiles " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    LOG.trace("Fin recuperación datos para la lista cargarPerfiles");

    return mSalida;
  }

  /**
   * Búsqueda de los usuarios que cumplan con los parametros
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> buscarUsuarios(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.trace("Iniciando datos para la lista buscarUsuarios");

    Map<String, Object> mSalida = new HashMap<String, Object>();
    List<Object> listaUsuariosFiltroDto = new ArrayList<Object>();

    try {

      // Se recuperan los parametros
      Tmct0UsersFilterDTO filters = tmct0UsersAssembler.getTmct0UsersFilterDto(paramsObjects);

      // Se recuperan los usuarios que cumplen con los criterios estableceidos
      List<Tmct0Users> listaUsuarios = tmct0UsersDaoImp.findUsuariosFiltro(filters);

      LOG.debug("Numero de usuarios disponibles " + listaUsuarios.size());

      // Se transforman a lista de DTO
      for (Tmct0Users tmct0Users : listaUsuarios) {
        try {
          Tmct0UsersDTO miDTO = tmct0UsersAssembler.getTmct0UsersDTO(tmct0Users);
          listaUsuariosFiltroDto.add(miDTO);
        } catch (Exception e) {
          LOG.error("Error metodo buscarUsuarios -  tmct0UsersAssembler.getTmct0UsersDTO " + e.getMessage(), e);
        }
      }

    } catch (Exception e) {
      LOG.error("Error metodo init - buscarUsuarios " + e.getMessage(), e);
      throw new SIBBACBusinessException("Error en la carga de datos iniciales de las opciones de busqueda");
    }

    mSalida.put("listaUsuarios", listaUsuariosFiltroDto);
    LOG.trace("Fin recuperación datos para la lista buscarUsuarios");

    return mSalida;
  }

  /**
   * Borrado de los usuarios que cumplan con los parametros
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> borrarUsuarios(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Inicio borrarUsuarios");

    if (paramsObjects.get("listaUsuariosBorrar") != null) {
      @SuppressWarnings("unchecked")
      List<Object> ListUsuariosBorrar = (List<Object>) paramsObjects.get("listaUsuariosBorrar");

      for (Object object : ListUsuariosBorrar) {

        LOG.debug("Borramos el usuario con id '" + object + "'");
        Long idUser = Long.parseLong(String.valueOf(object));

        try {
          tmct0UsersDao.delete(idUser);
        } catch (Exception e) {
          LOG.error("Error al borrar el usuario " + idUser);
          LOG.error(e.getMessage());
          throw new SIBBACBusinessException("No se ha podido borrar el usuario seleccionado");
        }
      }

    }
    LOG.debug("Usuarios eliminados correctamente");

    LOG.debug("Fin borrarUsuarios");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return buscarUsuarios(paramsObjects);
  }

  /**
   * Resetear Password de los usuarios que cumplan con los parametros
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> resetearPassword(Map<String, Object> paramsObjects) throws SIBBACBusinessException {

    LOG.debug("Inicio resetearPassword");

    if (paramsObjects.get("listaUsuariosBorrar") != null) {
      @SuppressWarnings("unchecked")
      List<Object> ListUsuariosBorrar = (List<Object>) paramsObjects.get("listaUsuariosBorrar");

      for (Object object : ListUsuariosBorrar) {

        LOG.debug("resetearPassword el usuario con id '" + object + "'");
        Long idUser = Long.parseLong(String.valueOf(object));

        try {
          tmct0UsersBo.updateReseteoPassword(idUser);
        } catch (Exception e) {
          LOG.error("Error al resetear el password para el usuario " + idUser);
          LOG.error(e.getMessage());
          throw new SIBBACBusinessException("No se ha podido resetear el password para el usuario seleccionado");
        }
      }

    }
    LOG.debug("Usuarios eliminados correctamente");

    LOG.debug("Fin resetearPassword");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return buscarUsuarios(paramsObjects);
  }

  /**
   * Método para grabar los datos del usuario pasado como parametro
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> grabarUsuarioExistente(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
    LOG.debug("Inicio grabarUsuario");

    // Se recuperan los parametros
    Tmct0UsersDTO miDTO = tmct0UsersAssembler.getTmct0UserDTO(paramsObjects);
    Tmct0Users usuario = null;

    // Comprobacion que no exista ya un usuario con el mismo username y que no sea el que se esta intentando modificar
    usuario = tmct0UsersDao.getByUsernameNotUserId(miDTO.getUsername(), miDTO.getIdUser());
    
    if (usuario != null) {
      LOG.error("Se esta intentanto modificar un usuario con el mismo username que uno existente -  "
                + miDTO.getUsername());
      throw new SIBBACBusinessException("Ya existe un usuario con el username " + miDTO.getUsername());
    }

    // Se guarda la información tratada en la pantalla
    tmct0UsersBo.saveExistingUser(miDTO);

    LOG.debug("Usuario grabado correctamente");

    LOG.debug("Fin grabarUsuario");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return buscarUsuarios(paramsObjects);
  }
  
  /**
   * Método para grabar los datos del usuario pasado como parametro
   * 
   * @return
   * @throws SIBBACBusinessException
   */
  private Map<String, Object> grabarNuevoUsuario(Map<String, Object> paramsObjects) throws SIBBACBusinessException {
    LOG.debug("Inicio grabarUsuario");

    // Se recuperan los parametros
    Tmct0UsersDTO miDTO = tmct0UsersAssembler.getTmct0UserDTO(paramsObjects);
    Tmct0Users usuario = null;

    usuario = tmct0UsersDao.getByUsername(miDTO.getUsername());
    
    if (usuario != null) {
      LOG.error("Se esta intentanto modificar un usuario con el mismo username que uno existente -  "
                + miDTO.getUsername());
      throw new SIBBACBusinessException("Ya existe un usuario con el username " + miDTO.getUsername());
    }

    // Se guarda la información tratada en la pantalla
    tmct0UsersBo.saveNewUser(miDTO);

    LOG.debug("Usuario grabado correctamente");

    LOG.debug("Fin grabarUsuario");

    // Se devuelve la lista de usuarios aplicando de nuevo los filtros
    return buscarUsuarios(paramsObjects);
  }

  /*
   * (non-Javadoc)
   * @see sibbac.webapp.business.SIBBACServiceBean#getFields()
   */
  /**
   * Gets the fields.
   *
   * @return the fields
   */
  @Override
  public List<String> getFields() {
    List<String> result = new ArrayList<String>();
    FieldsUsuario[] fields = FieldsUsuario.values();
    for (int i = 0; i < fields.length; i++) {
      result.add(fields[i].getField());
    }
    return result;
  }

  @Override
  public List<String> getAvailableCommands() {
    List<String> result = new ArrayList<String>();

    return result;
  }

  /**
   * Gets the command.
   *
   * @param command the command
   * @return the command
   */
  private ComandosUsuarios getCommand(String command) {
    ComandosUsuarios[] commands = ComandosUsuarios.values();
    ComandosUsuarios result = null;
    for (int i = 0; i < commands.length; i++) {
      if (commands[i].getCommand().equalsIgnoreCase(command)) {
        result = commands[i];
      }
    }
    if (result == null) {
      result = ComandosUsuarios.SIN_COMANDO;
    }
    LOG.debug(new StringBuffer("Se selecciona la accion: ").append(result.getCommand()).toString());
    return result;
  }

}
