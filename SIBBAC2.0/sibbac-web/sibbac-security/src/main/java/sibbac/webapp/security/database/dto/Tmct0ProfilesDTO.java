package sibbac.webapp.security.database.dto;

import java.util.List;

import sibbac.business.wrappers.common.SelectDTO;

/**
 * DTO para la gestion de TMCT0_ROLES
 * 
 * @author Neoris
 *
 */
public class Tmct0ProfilesDTO {

  /** idProfile - Identificador deL Perfil */
  private Long idProfile;
  /** name - nombre deL Perfil */
  private String name;
  /** description - descripción deL Perfil */
  private String description;

  /** listaUsuarios - Listado de usuarios que tienen ese perfil */
  private List<SelectDTO> listaUsuarios;

  /** listaPaginasAccion - Listado de acciones sobre paginas a las que tiene acceso perfil */
  private List<Long> listaPaginasAccion;

  public Tmct0ProfilesDTO() {

  }

  public Tmct0ProfilesDTO(Long idProfile,
                          String name,
                          String description,
                          List<SelectDTO> listaUsuarios,
                          List<Long> listaPaginasAccion) {
    this.idProfile = idProfile;
    this.name = name;
    this.description = description;
    this.listaUsuarios = listaUsuarios;
    this.listaPaginasAccion = listaPaginasAccion;
  }

  public Long getIdProfile() {
    return idProfile;
  }

  public void setIdProfile(Long idProfile) {
    this.idProfile = idProfile;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<SelectDTO> getListaUsuarios() {
    return listaUsuarios;
  }

  public void setListaUsuarios(List<SelectDTO> listaUsuarios) {
    this.listaUsuarios = listaUsuarios;
  }

  public List<Long> getListaPaginasAccion() {
    return listaPaginasAccion;
  }

  public void setListaPaginasAccion(List<Long> listaPaginasAccion) {
    this.listaPaginasAccion = listaPaginasAccion;
  }

}
