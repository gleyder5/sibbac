package sibbac.webapp.security.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.dao.Tmct0UsersDao;
import sibbac.webapp.security.database.dto.Tmct0UsersFilterDTO;
import sibbac.webapp.security.database.model.Tmct0Users;

/*
 * Clase Tmct0UsersDaoImpl para la consulta de los usuarios con el filtro
 */
@Repository
public class Tmct0UsersDaoImp {

  @Autowired
  Tmct0UsersDao tmct0UsersDao;

  private static final Logger LOG = LoggerFactory.getLogger(Tmct0UsersDaoImp.class);

  @PersistenceContext
  private EntityManager em;

  public List<Tmct0Users> findUsuariosFiltro(Tmct0UsersFilterDTO datosFiltro) {

    List<Tmct0Users> listaUsuariosFinal = new ArrayList<Tmct0Users>();

    Map<String, Object> parameters = new HashMap<String, Object>();
    LOG.debug("findUsuariosFiltro - Entro en el metodo");

    // Se forma la consulta
    final StringBuilder select = new StringBuilder(" SELECT distinct usr ");
    final StringBuilder from = new StringBuilder(" FROM Tmct0Users usr ");
    final StringBuilder where = new StringBuilder(" WHERE 1=1  ");
    final StringBuilder order = new StringBuilder(" ORDER BY usr.name  ");

    // Filtro por nombre
    if (datosFiltro.getIdUsuario() != null) {
      if (datosFiltro.isNotUsuario()) {
        LOG.debug("Filtro Recibido - Usuario distinto: " + datosFiltro.getIdUsuario());
        where.append(" AND usr.idUser != :ID_USER");
      } else {
        LOG.debug("Filtro Recibido - Usuario igual: " + datosFiltro.getIdUsuario());
        where.append(" AND usr.idUser = :ID_USER");
      }
      parameters.put("ID_USER", Long.parseLong(String.valueOf(datosFiltro.getIdUsuario())));
    }

    final StringBuilder select2 = new StringBuilder(select.toString());
    final StringBuilder from2 = new StringBuilder(from.toString());
    final StringBuilder where2 = new StringBuilder(where.toString());
    final StringBuilder order2 = new StringBuilder(order.toString());
    Map<String, Object> parameters2 = parameters;
    String sQuery = select.append(from.append(where.append(order))).toString();
    Query query = null;
    query = em.createQuery(sQuery, Tmct0Users.class);
    for (Entry<String, Object> entry : parameters.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }

    // se aplica el filtro por rol
    if (datosFiltro.getIdProfile() != null) {

      from2.append(" join usr.profiles per");
      where2.append(" AND per.idProfile = :PROFILE");
      // }
      parameters2.put("PROFILE", Long.parseLong(String.valueOf(datosFiltro.getIdProfile())));

      String sQuery2 = select2.append(from2.append(where2.append(order2))).toString();
      Query query2 = null;

      query2 = em.createQuery(sQuery2, Tmct0Users.class);

      for (Entry<String, Object> entry : parameters2.entrySet()) {
        query2.setParameter(entry.getKey(), entry.getValue());
      }

      // lista de usuarios aplicando los roles
      LOG.debug("Se ejecuta la query: " + sQuery2);
      List<Tmct0Users> listaUsuarios2 = query2.getResultList();

      if (datosFiltro.isNotProfile()) {
        LOG.debug("Filtro Recibido - Rol distinto: " + datosFiltro.getIdProfile());
        LOG.debug("Se ejecuta la query: " + sQuery);
        // lista de usuarios sin aplicar los roles
        List<Tmct0Users> listaUsuarios1 = query.getResultList();
        // nos quedamos con los usuarios de listaUsuarios1 que no esten en listaUsuarios2
        listaUsuariosFinal = (List<Tmct0Users>) CollectionUtils.disjunction(listaUsuarios1, listaUsuarios2);
      } else {
        LOG.debug("Filtro Recibido - Rol igual: " + datosFiltro.getIdProfile());
        listaUsuariosFinal = listaUsuarios2;
      }
    } else {
      // lista de usuarios sin aplicar los roles
      LOG.debug("Se ejecuta la query: " + sQuery);
      List<Tmct0Users> listaUsuarios1 = query.getResultList();
      listaUsuariosFinal = listaUsuarios1;
    }

    return listaUsuariosFinal;

  }
}
