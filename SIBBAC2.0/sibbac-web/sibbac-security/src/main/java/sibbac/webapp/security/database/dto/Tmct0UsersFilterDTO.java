package sibbac.webapp.security.database.dto;

public class Tmct0UsersFilterDTO {

  private Long idUsuario;
  private boolean notUsuario;
  private Long idProfile;
  private boolean notProfile;

  public Long getIdUsuario() {
    return idUsuario;
  }

  public void setIdUsuario(Long usuario) {
    this.idUsuario = usuario;
  }

  public boolean isNotUsuario() {
    return notUsuario;
  }

  public void setNotUsuario(boolean notUsuario) {
    this.notUsuario = notUsuario;
  }

  public Long getIdProfile() {
    return idProfile;
  }

  public void setIdProfile(Long profile) {
    this.idProfile = profile;
  }

  public boolean isNotProfile() {
    return notProfile;
  }

  public void setNotProfile(boolean notProfile) {
    this.notProfile = notProfile;
  }

}
