/**
 * 
 */
package sibbac.webapp.security.xmlparse.pojos.xml;

/**
 * @author dortega
 *
 */
public class XmlLocal {
	
	/**
	 * Name
	 */
	private String name;
	
	/**
	 * userID
	 */
	private String userID;
	
	/**
	 * alias
	 */
	private String alias;
	
	/**
	 * Constructor por defecto.
	 */
	public XmlLocal(){
		
	}
	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the userID
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param userID the userID to set
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

}
