package sibbac.webapp.security.database.dto;


/**
 * DTO para la gestion de las acciones dentro de una pagina que puede realizar un perfil
 * @author Neoris
 *
 */
public class Tmct0ActionPageProfileDTO {

  
  /** idAction - Identificador de la accion */ 
  private Long  idAction;
  
  /** idActionPage - Identificador de la página y accion a la que hacer referencia */ 
  private Long  idActionPage;
  
  /** bAplicaProfile - Indica si el perfil puede acceder a esa action page */
  private boolean bAplicaProfile;

  public Tmct0ActionPageProfileDTO() {
    
  }

  
  public Tmct0ActionPageProfileDTO(Long idAction, Long idActionPage, boolean bAplicaProfile) {
    
    this.idAction = idAction;
    this.idActionPage = idActionPage;
    this.bAplicaProfile = bAplicaProfile;
  }

  public Long getIdActionPage() {
    return idActionPage;
  }

  public void setIdActionPage(Long idActionPage) {
    this.idActionPage = idActionPage;
  }

  public boolean isbAplicaProfile() {
    return bAplicaProfile;
  }

  public void setbAplicaProfile(boolean bAplicaProfile) {
    this.bAplicaProfile = bAplicaProfile;
  }

  public Long getIdAction() {
    return idAction;
  }

  public void setIdAction(Long idAction) {
    this.idAction = idAction;
  }

  
}
