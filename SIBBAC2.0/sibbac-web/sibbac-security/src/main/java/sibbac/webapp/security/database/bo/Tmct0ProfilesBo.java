package sibbac.webapp.security.database.bo;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sibbac.database.bo.AbstractBo;
import sibbac.webapp.security.database.dao.Tmct0ProfilesDao;
import sibbac.webapp.security.database.model.Tmct0Profiles;

@Service
public class Tmct0ProfilesBo extends AbstractBo<Tmct0Profiles, Long, Tmct0ProfilesDao> {

  @Transactional
  public void saveProfile(Tmct0Profiles tmct0Profiles) {
    this.saveAndFlush(tmct0Profiles);
  }

}
