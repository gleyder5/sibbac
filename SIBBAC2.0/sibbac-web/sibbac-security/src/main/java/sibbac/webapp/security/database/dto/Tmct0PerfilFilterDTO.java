package sibbac.webapp.security.database.dto;

public class Tmct0PerfilFilterDTO {

  private Integer idPerfil;
  private boolean notPerfil;
  
  public Integer getIdPerfil() {
    return idPerfil;
  }
  public void setIdPerfil(Integer idPerfil) {
    this.idPerfil = idPerfil;
  }
  public boolean isNotPerfil() {
    return notPerfil;
  }
  public void setNotPerfil(boolean notPerfil) {
    this.notPerfil = notPerfil;
  }
     
   
  
}
