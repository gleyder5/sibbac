package sibbac.webapp.security.database.dto;

import java.util.Date;
import java.util.List;

import sibbac.business.wrappers.common.SelectDTO;

/**
 * DTO para la gestion de TMCT0_USERS
 * 
 * @author Neoris
 *
 */
public class Tmct0UsersDTO {

  /** id_user - Identificador del usuario */
  private Long idUser;
  /** username - username del usuario */
  private String username;

  /** password - password del usuario */
  private String password;
  
  /** password - password del usuario */
  private String nuevoPassword;

  /** name - nombre del usuario */
  private String name;

  /** lastname - apellidos del usuario */
  private String lastname;

  /** email - correo electronico del usuario */
  private String email;

  /** roles - Roles que tiene asignado el usuario */
  private List<SelectDTO> listaProfiles;

  /** lastlogin- Última conexión usuario */
  private Date lastlogin;

  public Tmct0UsersDTO() {

  };

  public Tmct0UsersDTO(Long idUser,
                       String username,
                       String password,
                       String name,
                       String lastname,
                       List<SelectDTO> listaProfiles) {
    super();
    this.idUser = idUser;
    this.username = username;
    this.password = password;
    this.name = name;
    this.lastname = lastname;
    this.listaProfiles = listaProfiles;
  }

  public String getNuevoPassword() {
	return nuevoPassword;
}

public void setNuevoPassword(String nuevoPassword) {
	this.nuevoPassword = nuevoPassword;
}

public Tmct0UsersDTO(String username, String password) {
    this.username = username;
    this.password = password;
  }

public Tmct0UsersDTO(String username, String password, String nuevoPassword) {
    this.username = username;
    this.password = password;
    this.nuevoPassword = nuevoPassword;
  }

  public Long getIdUser() {
    return idUser;
  }

  public void setIdUser(Long idUser) {
    this.idUser = idUser;
  }

  public String getUsername() {
    // se quitan los espacios
    return username.trim();
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    // se quitan los espacios
    return password.trim();
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public List<SelectDTO> getListaProfiles() {
    return listaProfiles;
  }

  public void setListaProfiles(List<SelectDTO> listaProfiles) {
    this.listaProfiles = listaProfiles;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getLastlogin() {
    return lastlogin;
  }

  public void setLastlogin(Date lastlogin) {
    this.lastlogin = lastlogin;
  }

}
