package sibbac.webapp.security.database.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sibbac.webapp.security.database.dao.Tmct0ProfilesDao;
import sibbac.webapp.security.database.dto.Tmct0PerfilFilterDTO;
import sibbac.webapp.security.database.model.Tmct0Profiles;

/*
 * the class Tmct0PagesDaoImp para la consulta de los paginas con el filtro
 */
@Repository
public class Tmct0ProfilesDaoImp {

  @Autowired
  Tmct0ProfilesDao tmct0ProfileDao;

  /** The Constant LOG. */
  private static final Logger LOG = LoggerFactory.getLogger(Tmct0ProfilesDaoImp.class);

  @PersistenceContext
  private EntityManager em;

  public List<Tmct0Profiles> findPerfilesFiltro(Tmct0PerfilFilterDTO datosFiltro) {

    List<Tmct0Profiles> listaPerfiles = new ArrayList<Tmct0Profiles>();

    Map<String, Object> parameters = new HashMap<String, Object>();
    LOG.debug("findPerfilesFiltro - Entro en el metodo");

    LOG.debug("Filtro Recibido: ");

    // Se forma la consulta

    final StringBuilder select = new StringBuilder("SELECT  per ");
    final StringBuilder from = new StringBuilder(" FROM Tmct0Profiles per ");
    final StringBuilder where = new StringBuilder(" WHERE 1=1");
    final StringBuilder order = new StringBuilder(" ORDER BY per.name  ");

    // Filtro por perfil
    if (datosFiltro.getIdPerfil() != 0) {

      if (datosFiltro.isNotPerfil()) {
        LOG.debug("Filtro Recibido - Perfil distinto: " + datosFiltro.getIdPerfil());
        where.append(" AND per.idProfile != :idPerfil ");
      } else {
        LOG.debug("Filtro Recibido - Perfil igual: " + datosFiltro.getIdPerfil());
        where.append(" AND per.idProfile = :idPerfil ");
      }
      parameters.put("idPerfil", datosFiltro.getIdPerfil().longValue());

    }

    String sQuery = select.append(from.append(where.append(order))).toString();
    Query query = null;

    LOG.debug("Creamos y ejecutamos la query: " + sQuery);

    query = em.createQuery(sQuery, Tmct0Profiles.class);

    for (Entry<String, Object> entry : parameters.entrySet()) {
      query.setParameter(entry.getKey(), entry.getValue());
    }

    listaPerfiles = query.getResultList();

    return listaPerfiles;

  }

}
