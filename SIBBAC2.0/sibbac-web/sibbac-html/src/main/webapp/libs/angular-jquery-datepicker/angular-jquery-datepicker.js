'use strict';
angular.module('angular-jquery-datepicker', []).factory('datepicker', [function () {
    return $.datepicker;
}]).provider('$datepicker', function () {
    return {
        setDefaults: function (language) {
            $.datepicker.setDefaults($.datepicker.regional[language]);
        },
        $get: function () {
            return {

            };
        }
    };
}).directive('datepicker', function () {
    return {
        restrict: 'A',
        scope : {
            dateHasta : '@'
        },
        link: function (scope, elem, attr, ctrl) {
            //elem.css('background', 'url(images/calendar_icon_x32.png) no-repeat right center');
            elem.datepicker(
                {
                    //defaultDate: "-1w"
                    
                });
        }
    };
});