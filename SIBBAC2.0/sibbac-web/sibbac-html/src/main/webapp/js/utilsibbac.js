/*
*funciones utiles para la aplicacion.
*
*revisión 05/05/2015
*/

/*
 * ajax
 *
 * ejemplo
 *   se crea Databean con los parametros necesarios para la llamada
 *   var datos = new DataBean();
 *   datos.setService(servicio);
 *   datos.setAction(accion);
 *   datos.setFilters(filters);
 *   datos.setParams(params);
 *   datos.setList(list);
 *   datos.setAutocomplete(autocomplete);
 *   llamamos al requestSIBBAC(datos)
 *   var respuesta = requestSIBBAC(datos);
 *   y configuramos el
 *   respuesta.done(function(json){
 *      aqui la parte del success.
 *   });
 *   si hay errores salta un alert.
 *
 *
 */

/*
 * Estas variables hay que prepararlas convenientemente.
 */
var SERVER			= "";		// El servidor y el puerto de escucha de la aplicación web "back" (Java).
var CONTEXT			= SERVER + "/sibbac20back";		// El "path" de la aplicación web "back" (java).

//funcion para excel
function SendAsXLS() {
    SendAsExport("xls");
}
//funcion para pdf
function SendAsPDF() {
    SendAsExport("pdf");
}


function mensajeError(mensaje) {
    emergeFrame("mensaje_error", mensaje);
}

function mensajeInformacion(mensaje) {
    alert(mensaeje);
}

function mensajeConfirmacion(mensaje) {
    return confirm(mensaje);
}

function emergeFrame(marco, text) {
    var fade = $("div#frade");
    var div = $("div#" + marco);
    var noDiv = div.length == 0;
    div = $("<div id='" + marco + "' class='emergente_s'><img src='img/error.png' align='left'/>"
            + text + "<br/><br/>Pincha fuera de este marco o pulsa Esc</div>");
    if (noDiv) {
        $("body").append(div);
    }
    if (fade.length == 0) {
        fade = $("<div id='fade' class='black_overlay'/>");
        $("body").append(fade);
    }
    div.css("display", "block");
    div.css("marginTop", (-div.height() / 2) + "px");
    fade.css("display", "block");
    fade.click(function (event) {
        if ("fade" === event.target.id) {
            removeFrame(div);
        }
    });
    $(document).keyup(function (tecla) {
        if (tecla.which == 27) {
            removeFrame(div);
        }
    });
}

function removeFrame(div) {
    div.remove();
    $("#fade").remove();
}

/*
 *
 * objeto a pasar datos
 *
 */


function DataBean() {
    this.service = "";
    this.action = "";
    this.filters = "";
    this.list = "";
    this.params = "";
    this.autocomplete = "";
    this.url = "";
    this.exportTo = "";

    this.setService = function (service) {
        this.service = service;
    };
    this.setAction = function (action) {
        this.action = action;
    };
    this.setFilters = function (filters) {
        this.filters = filters;
    };
    this.setList = function (list) {
        this.list = list;
    };
    this.setParams = function (params) {
        this.params = params;
    };
    this.setAutocomplete = function (autocomplete) {
        this.autocomplete = autocomplete;
    };
    this.setURL = function (url) {
        this.url = url;
    };
    this.setExportTo = function (format) {
        this.exportTo = format;
    }

    this.toString = function () {
        var txt = "DataBean:";
        txt += " [service==" + this.service + "]";
        txt += " [action==" + this.action + "]";
        txt += " [filters==" + this.filters + "]";
        txt += " [list==" + this.list + "]";
        txt += " [params==" + this.params + "]";
        txt += " [autocomplete==" + this.autocomplete + "]";
        txt += " [url==" + this.url + "]";
        txt += " [exportTo==" + this.exportTo + "]";
        return txt;
    }
}
/*metodo con bean*/
function requestSIBBAC(datos) {
    // para mejorar el exportar tablas
    //var webRequestVar = (datos.exportTo === "" || datos.exportTo === undefined) ? "" : "webRequest : ";
    var data = "{\"service\" :\"" + datos.service + "\", \"action\"  :\"" + datos.action + "\"";
    if (datos.filters !== '') {
        data += ", \"filters\" :" + datos.filters;
    }
    if (datos.list !== '') {
        data += ", \"list\" :" + datos.list;
    }
    if (datos.params !== '') {
        data += ", \"params\"  : " + datos.params;
    }
    if (datos.autocomplete !== '') {
        data += ", \"autocomplete\"  : " + datos.autocomplete;
    }
    data += "}";
    $.blockUI({
        message: '<h1><img src="images/loading.gif" /> Cargando datos...</h1>',
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#FFF',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#666'
        }
    });
    var result = '';
    var result = $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service/",
        data: data,
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true
    });
    result.fail(function () {
        alert("No se ha podido conectar, inténtelo mas tarde o consulte con el administrador");
    });
    result.always(function () {
        $.unblockUI();
    });
    return result;
}

function requestSIBBACSinBlockUI(datos) {
    // para mejorar el exportar tablas
    //var webRequestVar = (datos.exportTo === "" || datos.exportTo === undefined) ? "" : "webRequest : ";
    var data = "{\"service\" :\"" + datos.service + "\", \"action\"  :\"" + datos.action + "\"";
    if (datos.filters !== '') {
        data += ", \"filters\" :" + datos.filters;
    }
    if (datos.list !== '') {
        data += ", \"list\" :" + datos.list;
    }
    if (datos.params !== '') {
        data += ", \"params\"  : " + datos.params;
    }
    if (datos.autocomplete !== '') {
        data += ", \"autocomplete\"  : " + datos.autocomplete;
    }
    data += "}";
    var result = $.ajax({
        type: "POST",
        dataType: "json",
        url: "/sibbac20back/rest/service/",
        data: data,
        beforeSend: function (x) {
            if (x && x.overrideMimeType) {
                x.overrideMimeType("application/json");
            }
            // CORS Related
            x.setRequestHeader("Accept", "application/json");
            x.setRequestHeader("Content-Type", "application/json");
            x.setRequestHeader("X-Requested-With", "HandMade");
            x.setRequestHeader("Access-Control-Allow-Origin", "*");
            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        },
        async: true
    });
    result.fail(function () {
        alert("No se ha podido conectar, inténtelo mas tarde o consulte con el administrador");
    });
    return result;
}

/*
 *validar fecha
 */

function validarCampoFecha(campoFecha, obligatorio) {
    var resultado = true;
    var date = $('#' + campoFecha).val();
    if (date === undefined || date === '') {
        if (obligatorio) {
            console.log("[utilsibbac.validarFecha] ERROR [Campo vacio]");
            resultado = false;
        }
    } else {
        if (resultado) {
            console.log(campoFecha + " " + date);
            var fechaCompleta = date.split('/');
            var dia = fechaCompleta[0];
            var mes = fechaCompleta[1];
            var anio = fechaCompleta[2];

            if ((dia.length != 2) || (mes.length != 2) || (anio.length != 4)) {
                console.log("[utilsibbac.validarFecha] ERROR [tamaño de campos incorrectos]");
                resultado = false;
            }
            if (resultado) {
                if (dia < 1 || dia > 31) {
                    console.log("[utilsibbac.validarFecha] ERROR [El valor del día debe estar comprendido entre 1 y 31.]");
                    resultado = false;
                }
            }
            if (resultado) {
                if (mes < 1 || mes > 12) {
                    console.log("[utilsibbac.validarFecha] ERROR [El valor del mes debe estar comprendido entre 1 y 12.]");
                    resultado = false;
                }
            }
            if (resultado) {
                if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) {
                    console.log("[utilsibbac.validarFecha] ERROR [El mes " + mes + " no tiene 31 días!]");
                    resultado = false;
                }
            }
            if (resultado) {
                if (mes == 2) { // bisiesto
                    var bisiesto = (anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0));
                    if (dia > 29 || (dia == 29 && !bisiesto)) {
                        console.log("[utilsibbac.validarFecha] ERROR [Febrero del " + anio + " no contiene " + dia + " dias!]");
                        resultado = false;
                    }
                }
            }
        }
    }
    return resultado;
}


function validarFecha(campoFecha) {
    var date = $('#' + campoFecha).val();
    if (date == '') {
        console.log("campo vacio");
        return false
    }
    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var fechaCompleta = date.split('/');
    var dia = fechaCompleta[0];
    var mes = fechaCompleta[1];
    var anio = fechaCompleta[2];

    if ((dia.length != 2) || (mes.length != 2) || (anio.length != 4)) {
        console.log("tamaño de campos incorrectos");
        return false;
    }
    if (dia < 1 || dia > 31) {
        console.log("El valor del día debe estar comprendido entre 1 y 31.");
        return false;
    }
    if (mes < 1 || mes > 12) {
        console.log("El valor del mes debe estar comprendido entre 1 y 12.");
        return false;
    }
    if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) {
        console.log("El mes " + mes + " no tiene 31 días!");
        return false;
    }
    if (mes == 2) { // bisiesto
        var bisiesto = (anio % 4 == 0 && (anio % 100 != 0 || anio % 400 == 0));
        if (dia > 29 || (dia == 29 && !bisiesto)) {
            console.log("Febrero del " + anio + " no contiene " + dia + " dias!");
            return false;
        }
    }
    return true;
}


function validaMesDia(mes, dia)
{
    if (dia < 1 || dia > 31) {
        console.log("El valor del día debe estar comprendido entre 1 y 31.");
        return false;
    }
    if (mes == 99)
    {
    	if (dia > 28) {
            console.log("Febrero no contiene " + dia + " dias!");
            return false;
        }
    }
    else
    {
    	if (mes < 1 || mes > 12) {
    		console.log("El valor del mes debe estar comprendido entre 1 y 12.");
    		return false;
    	}
    	if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) {
    		console.log("El mes " + mes + " no tiene 31 días!");
    		return false;
    	}
    	if (mes == 2) { // bisiesto
    		if (dia > 28) {
    			console.log("Febrero no contiene " + dia + " dias!");
    			return false;
    		}
    	}
    }
    return true;
}

function justNumbers(e) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if (keynum == 8)
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function justNumbersD(e) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 8) || (keynum == 46))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function validarEmail(email) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!expr.test(email))
    {
        alert("Error: La dirección de correo " + email + " es incorrecta.");
        return false;
    }
    else
    {
        return true;
    }
}

function compare_dates(fecha, fecha2)
{

    var xMonth = fecha.substring(3, 5);
    var xDay = fecha.substring(0, 2);
    var xYear = fecha.substring(6, 10);
    var yMonth = fecha2.substring(3, 5);
    var yDay = fecha2.substring(0, 2);
    var yYear = fecha2.substring(6, 10);
    if (xYear > yYear)
    {
        return(true)
    }
    else
    {
        if (xYear == yYear)
        {
            if (xMonth > yMonth)
            {
                return(true)
            }
            else
            {
                if (xMonth == yMonth)
                {
                    if (xDay > yDay)
                    {
                        return(true);
                    }

                    else
                    {
                        return(false);
                    }

                }
                else
                {
                    return(false);
                }

            }
        }
        else
        {
            return(false);
        }

    }
}


//formatea un importe con dos decimales a xxx.xxx.xxx.xxx,xx y sin decimales
function a2digitos(cantidad) {
    if (cantidad === null) {
        cantidad = 0;
    }
    var esNegativo = (cantidad >= 0) ? false : true;
    if (esNegativo) {
        //se cambia a positivo.
        cantidad = cantidad * -1;
    }
    var result = cantidad + "";
    if (result.indexOf('.') !== -1) {
        result = result.split(".");
        if (result.length !== 2) {
            console.log('formato no correcto ' + result);
            return cantidad;
        } else {
            var entero = result[0];
            var decimal = result[1];
            if (decimal.length > 2) {
                decimal = decimal.substring(0, 2);
            } else if (decimal.length === 1) {
                decimal = decimal + '0';
            }

            if (entero.length > 3) {
                entero = entero.substring(0, entero.length - 3) + "." + entero.substring((entero.length - 3));
                if (entero.length > 7) {
                    entero = entero.substring(0, entero.length - 7) + "." + entero.substring(entero.length - 7);
                }
                if (entero.length > 11) {
                    entero = entero.substring(0, entero.length - 11) + "." + entero.substring(entero.length - 11);
                }
                if (entero.length > 15) {
                    entero = entero.substring(0, entero.length - 15) + "." + entero.substring(entero.length - 15);
                }
            }
            if (esNegativo) {
                entero = "-" + entero;
            }
            return entero + ',' + decimal;
        }
    }
    else if (result.indexOf(',') !== -1) {
        result = result.split(",");
        if (result.length !== 2) {
            console.log('formato no correcto ' + result);
            return cantidad;
        } else {
            var entero = result[0];
            var decimal = result[1];
            if (decimal.length > 2) {
                decimal = decimal.substring(0, 2);
            }
            if (entero.length > 3) {
                entero = entero.substring(0, entero.length - 3) + "." + entero.substring((entero.length - 3));
                if (entero.length > 7) {
                    entero = entero.substring(0, entero.length - 7) + "." + entero.substring(entero.length - 7);
                }
                if (entero.length > 11) {
                    entero = entero.substring(0, entero.length - 11) + "." + entero.substring(entero.length - 11);
                }
                if (entero.length > 15) {
                    entero = entero.substring(0, entero.length - 15) + "." + entero.substring(entero.length - 15);
                }
            }
            if (esNegativo) {
                entero = "-" + entero;
            }
            return entero + ',' + decimal;
        }
    } else {
        var entero = result;
        if (entero.length > 3) {
            entero = entero.substring(0, entero.length - 3) + "." + entero.substring((entero.length - 3));
            if (entero.length > 7) {
                entero = entero.substring(0, entero.length - 7) + "." + entero.substring(entero.length - 7);
            }
            if (entero.length > 11) {
                entero = entero.substring(0, entero.length - 11) + "." + entero.substring(entero.length - 11);
            }
            if (entero.length > 15) {
                entero = entero.substring(0, entero.length - 15) + "." + entero.substring(entero.length - 15);
            }
        }
        if (esNegativo) {
            entero = "-" + entero;
        }
        return entero + ",00";
    }

    return cantidad;
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t)
            + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

//formatea precios a 6 digitos
function a6digitos(cantidad) {
    var result = cantidad + "";
    if (result.indexOf('.') !== -1) {
        result = result.split(".");

        var entero = result[0];
        var decimal = result[1];
        if (decimal.length > 6) {
            decimal = decimal.substring(0, 6);
        } else if (decimal.length < 6) {
            for (var i = decimal.length; i < 6; i++) {
                decimal += '0'; //añadimos 0 hasta completar a 6 digitos de decimales
            }
        }
        if (entero.length > 3) {
            entero = entero.substring(0, entero.length - 3) + "." + entero.substring((entero.length - 3));
            if (entero.length > 7) {
                entero = entero.substring(0, entero.length - 7) + "." + entero.substring(entero.length - 7);
            }
            if (entero.length > 11) {
                entero = entero.substring(0, entero.length - 11) + "." + entero.substring(entero.length - 11);
            }
            if (entero.length > 15) {
                entero = entero.substring(0, entero.length - 15) + "." + entero.substring(entero.length - 15);
            }
        }
        return entero + ',' + decimal;

    }
    else if (result.indexOf(',') !== -1) {
        result = result.split(",");

        var entero = result[0];
        var decimal = result[1];
        if (decimal.length > 6) {
            decimal = decimal.substring(0, 6);
        } else if (decimal.length < 6) {
            for (var i = decimal.length; i < 6; i++) {
                decimal += '0'; //añadimos 0 hasta completar a 6 digitos de decimales
            }
        }
        if (entero.length > 3) {
            entero = entero.substring(0, entero.length - 3) + "." + entero.substring((entero.length - 3));
            if (entero.length > 7) {
                entero = entero.substring(0, entero.length - 7) + "." + entero.substring(entero.length - 7);
            }
            if (entero.length > 11) {
                entero = entero.substring(0, entero.length - 11) + "." + entero.substring(entero.length - 11);
            }
            if (entero.length > 15) {
                entero = entero.substring(0, entero.length - 15) + "." + entero.substring(entero.length - 15);
            }
        }
        return entero + ',' + decimal;

    } else {
        var entero = result;
        if (entero.length > 3) {
            entero = entero.substring(0, entero.length - 3) + "." + entero.substring((entero.length - 3));
            if (entero.length > 7) {
                entero = entero.substring(0, entero.length - 7) + "." + entero.substring(entero.length - 7);
            }
            if (entero.length > 11) {
                entero = entero.substring(0, entero.length - 11) + "." + entero.substring(entero.length - 11);
            }
            if (entero.length > 15) {
                entero = entero.substring(0, entero.length - 15) + "." + entero.substring(entero.length - 15);
            }
        }
        entero += ',000000'; //y le metemos los 6 decimales.
        return entero;
    }
    return cantidad;
}

function getfechaHoy() {
    var fecha = new Date();
    var mes = fecha.getMonth() + 1;

    if ((mes + '').length != 2) {
        mes = '0' + mes;
    }
    var año = fecha.getFullYear();
    var dia = fecha.getDate();

    if ((dia + '').length != 2) {
        dia = '0' + dia;
    }
    return dia + "/" + mes + "/" + año;
}

/**
 * Pliega/despliega el formulario de búsqueda.
 */
function collapseSearchForm() {
//  $(".collapser_search").parents('.title_section').next().slideToggle();
//  $(".collapser_search").parents('.title_section').next().next('.button_holder').slideToggle();
//  $(".collapser_search").toggleClass('active');
//  if ($(".collapser_search").text().indexOf('Ocultar') !== -1) {
//    $(".collapser_search").text("Mostrar opciones de búsqueda");
//  } else {
//    $(".collapser_search").text("Ocultar opciones de búsqueda");
//  }
//  return false;
  $('.collapser_search').click();
} // collapseSearchForm

/**
 * Prepara los elementos del dom en el formulario de búsqueda para que adopten el comportamiento de plegar y desplegar
 * la capa del formulario.
 *
 * Al plegar y desplegar el formulario de búsqueda si existe el elemento 'mensajeBusqueda' lo oculta cuando el
 * formulario está desplegado y lo muestra al plegarlo.
 */
function prepareCollapsion() {
  $('.collapser').click(function (event) {
    event.preventDefault();
    $(this).parents('.title_section').next().slideToggle();
    $(this).toggleClass('active');
    $(this).toggleText('Clic para mostrar', 'Clic para ocultar');
    return false;
  });

  $('.collapser_search').click(function (event) {
    event.preventDefault();
    $(this).parents('.title_section').next().slideToggle();
    $(this).parents('.title_section').next().next('.button_holder').slideToggle();
    $(this).toggleClass('active');
    if ($(this).text().indexOf('Ocultar') !== -1) {
      $(this).text("Mostrar opciones de búsqueda");
      $('.mensajeBusqueda').show(750);
    } else {
      $(this).text("Ocultar opciones de búsqueda");
      $('.mensajeBusqueda').hide(1000);
    }
    return false;
  });
} // prepareCollapsion

function ajustarAnchoDivTable() {
    /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
    if ($('.contenedorTabla').length >= 1) {
        var anchoBotonera;
        $('.contenedorTabla').each(function (i) {
            anchoBotonera = $(this).find('table').outerWidth();
            $(this).find('.botonera').css('width', anchoBotonera + 'px');
            $(this).find('.resumen').css('width', anchoBotonera + 'px');
            $('.resultados').hide();
        });
    }
}
/*Eva:Función para cambiar texto*/
jQuery.fn.extend({
    toggleText: function (a, b) {
        var that = this;
        if (that.text() != a && that.text() != b) {
            that.text(a);
        } else if (that.text() == a) {
            that.text(b);
        } else if (that.text() == b) {
            that.text(a);
        }
        return this;
    }
});
/*FIN Función para cambiar texto*/




function loadAutocompletar(campo, servicio, accion, idsOcultos, sourceTags, fnOnSelect) {
    var data = new DataBean();
    data.setService(servicio);
    data.setAction(accion);
    var request = requestSIBBAC(data);
    request.success(function (json) {
        var datos = {};
        if (json.resultados === null || json.resultados.listado === null) {
            alert(json.error);
        } else {
            datos = json.resultados.listado;

        }
        for (var k in datos) {
            var item = datos[k];
            idsOcultos.push(item.value);
            var tag = item.label;
            sourceTags.push(tag);
        }
        var configuration = {};
        if (fnOnSelect === undefined) {
            configuration = {
                source: sourceTags
            }
        } else {
            configuration = {
                source: sourceTags,
                select: fnOnSelect
            }
        }
        $(campo).autocomplete({
            source: sourceTags
        });
    });
}
;
