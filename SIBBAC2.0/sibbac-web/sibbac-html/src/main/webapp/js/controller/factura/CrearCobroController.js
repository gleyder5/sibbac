sibbac20.controller('CrearCobroController', ['$scope', '$compile', '$document', 'AliasService', 'growl', '$rootScope', 'SecurityService', '$location', function ($scope, $compile, $document, AliasService, growl, $rootScope, SecurityService, $location) {
        var idOcultos = [];
        var availableTags = [];
        var marcarChecks = [];
        var idsFacturas = [];

        var datepickerCheck = 0;// carga de datepickers
        var checkTime = 0;// carga de tabla
        var oTable = undefined;

        $scope.idAlias = '';

        $scope.arrImporteAplicado = [];
        $scope.arrImportePendiente = [];
        $scope.arrImporteTotal = [];
        $scope.importeCobro = 0;
        $scope.importeVencido = 0;
        $scope.saldoPendiente = 0;

        //modificar importe del cobro de cada factura
        $scope.mod = {};
        $scope.mod.idFactura = '';
        $scope.mod.importeTotal = 0;
        $scope.mod.importePendiente = 0;
        $scope.mod.importeAplicado = 0;
        $scope.mod.saldoPendienteCobro = 0;
        $scope.mod.importeVencidoCobro = 0;
        $scope.cobro;

        $document.ready(function () {

            oTable = $("#tblCrearCobro").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "columns": [{"width": "auto"},
                    {"width": "auto"},
                    {"width": "auto"},
                    {"width": "auto", type: "date-eu"},
                    {"width": "auto", sClass: "monedaR", type: "formatted-num"},
                    {"width": "auto", sClass: "monedaR", type: "formatted-num"},
                    {"width": "auto", sClass: "monedaR", type: "formatted-num"},
                    {"width": "auto", sClass: "monedaR", type: "formatted-num"},
                    {"width": "auto", sClass: "monedaR", type: "formatted-num"},
                    {"width": "auto"}
                ],
                "scrollY": "480px",
                "scrollCollapse": true,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    $compile(nRow)($scope);
                }
            });

            /* BUSCADOR */
            $('#cargarTabla').submit(function (event) {
                event.preventDefault();
                var idAlias = getIdAlias();
                if (idAlias !== "") {
                    cargarTabla(idAlias);
                    collapseSearchForm();
                }
                return false;
            });

            $("td.editable span").click(clickEditable);
            /*
             * FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA
             * TABLA
             */
            if ($('.contenedorTabla').length >= 1) {
                var anchoBotonera;
                $('.contenedorTabla').each(function (i) {
                    anchoBotonera = $(this).find('table').outerWidth();
                    $(this).find('.botonera').css('width', anchoBotonera + 'px');
                    $(this).find('.resumen').css('width', anchoBotonera + 'px');
                });
            }
            prepareCollapsion();
            // insertar cobro
            $('#crearCobro').submit(function (event) {
                event.preventDefault();
                crearCobro();
            });
            initialize();
        });

        function initialize() {
            var fechas = ['fechaContabilizacion'];
            loadpicker(fechas);
            AliasService.getAliasFactura(cargarAlias, showErrorHandler);
        }
        function showErrorHandler(data, status, headers, config) {
            growl.addErrorMessage("Error! " + data);
        }

        function cargarAlias(datos) {
            var item = null;
            var ponerTag = "";
            angular.forEach(datos, function (val, key) {
                idOcultos.push(val.id);
                ponerTag = val.nombre.trim();
                availableTags.push(ponerTag);
            });
            // código de autocompletar
            $("#textAlias").empty();
            $("#textAlias").autocomplete({
                source: availableTags
            });
        }



        $scope.calculosInicialesCobro = function (event) {

            var importeCobro = $scope.cobro;
            if ((importeCobro !== null) && (importeCobro !== ''))
            {
                importeCobro = importeCobro.replace(/\./g,'');
                importeCobro = importeCobro.replace(",",".");
                importeCobro = parseFloat(importeCobro);
                console.log("importeCobro: " + importeCobro);
            }
            if (isNaN(importeCobro) || (importeCobro === null) || (importeCobro === '')) {
                console.log("No es un número ..");
                // vaciamos los datos y bloqueamos las filas
                $scope.cobro = '';
                angular.element('#importeVencido').empty();
                angular.element('#saldoPendiente').empty();
                angular.element('#saldoPendiente').val('');
                $scope.importeCobro = 0;
                $scope.importeVencido = 0;
                $scope.saldoPendiente = 0;
                if (idsFacturas.length > 0) {
                    angular.forEach(idsFacturas, function (val, key) {
                        angular.element('#ckFila' + val).attr('disabled', true);
                    });
                }
            } else {
                // inicializamos los valores y desbloqueamos las filas
                var importeVencido = 0;
                $scope.importeCobro = importeCobro;
                var saldoPendiente = (importeCobro - importeVencido);
                /*Se añade el formato al importe introducido*/
                $scope.cobro = $.number($scope.cobro, 2, ',', '.');
                angular.element('#saldoPendiente').val($.number(saldoPendiente, 2, ',', '.'));

                $scope.importeVencido = parseFloat(importeVencido);
                $scope.saldoPendiente = parseFloat(saldoPendiente);
                $scope.importeCobro = parseFloat(importeCobro);

                if (idsFacturas.length > 0) {
                    angular.forEach(idsFacturas, function (val, key) {
                        angular.element('#ckFila' + val).removeAttr('disabled');
                    });
                }
                console.log("importeCobro: " + $scope.importeCobro);
            }
        }



        function crearCobro() {
            var idAlias = getIdAlias();
            //var importeCobrado = parseFloat($scope.importeCobro);
            var importeCobrado = $('#importeCobro').val();
            var fechaContabilizacion = transformaFechaInv($('#fechaContabilizacion').val());

            var filters = '{"idAlias" : "' + idAlias + '" , "importeCobrado" : "' + importeCobrado + '" ,"fechaContabilizacion" : "' + fechaContabilizacion + '"}';
            var params = [];
            var param = {};
            var cierre = "";
            for (z = 0; z < marcarChecks.length; z++) {
                var idFactura = marcarChecks[z];

                // accedo al contenido del div
                var posArray = getPosicionArray(idFactura);
                var importeAplicado = $scope.arrImporteAplicado[posArray];
                var importeAplicado = $('#tblCrearCobro').find('#importeAplicado' + idFactura).text();
                cierre = $('#tblCrearCobro').find('#ckCerrar' + idFactura).is(':checked');

                param = {idFactura: idFactura, importeAplicado: importeAplicado, cierre: cierre};
                params[z] = param;
            }

            // fin del for, le resto la última coma
            var data = new DataBean();
            data.setService('SIBBACServiceCobros');
            data.setAction('insertarCobro');
            data.setFilters(filters);
            data.setParams(JSON.stringify(params));
            var request = requestSIBBAC(data);
            request.success(function (json) {
                if (json.error !== null && json !== undefined) {
                    alert(json.error);
                } else {
                    angular.element('#importeCobro').val('');
                    angular.element('#importeVencido').val('');
                    angular.element('#saldoPendiente').val('');
                    angular.element('#fechaContabilizacion').val('');
                    cargarTabla($scope.idAlias);
                    angular.element('#importeCobro').prop('disabled', false);
                }
            });
        }

        function getPosicionArray(idFactura) {
            var posicion = -1;
            var encontrado = false;
            for (var i = 0; i < idsFacturas.length && encontrado !== true; i++)
            {
                if (idsFacturas[i] == idFactura)
                {
                    posicion = i;
                    encontrado = true;
                }
            }
            return posicion;
        }

        $scope.modificarImporteAplicado = function () {

            var importeAplicado = $('#importeAplicado').val();
            if ((importeAplicado !== null) && (importeAplicado !== '')){
                importeAplicado = importeAplicado.replace(/\./g, '');
                importeAplicado = importeAplicado.replace(",", ".");
                importeAplicado = parseFloat(importeAplicado);
            }
            if (isNaN(importeAplicado) || (importeAplicado === null) || (importeAplicado === '')) {
                alert('debe insertar un valor númerico ');
            }
            var importePendiente = $scope.mod.importePendiente;
            var saldoPendiente = $scope.mod.saldoPendienteCobro;
            var importeVencido = $scope.mod.importeVencidoCobro;
            if (importeAplicado > importePendiente) {
                alert("El importe aplicado es superior al importe pendiente de la factura");
                return false;
            }
            if (importeAplicado > saldoPendiente) {
                alert("El importe aplicado es superior al saldo pendiente del cobro");
                return false;
            }
            var idFactura = $('#idFactura').val();
            var posArray = getPosicionArray(idFactura);
            $scope.arrImporteAplicado[posArray] = importeAplicado;
            $('#importeAplicado' + idFactura).text($.number(importeAplicado, 2, ',', '.'));
            if ($('#ckCerrar').is(':checked')) {
                $('#ckCerrar' + idFactura).attr('checked', true);
            } else {
                $('#ckCerrar' + idFactura).removeAttr('checked');
            }

            $scope.saldoPendiente = saldoPendiente - importeAplicado;
            $scope.importeVencido = importeVencido + importeAplicado;
            $('#saldoPendiente').val($.number(($scope.saldoPendiente), 2, ',', '.'));
            $('#importeVencido').val($.number(($scope.importeVencido), 2, ',', '.'));
            $scope.mod.importeAplicado = importeAplicado;
            document.getElementById('formulariosModificarCobro').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        };

        function render() {
            console.log("se usa el render??");
        }

        function consultar(params) {
            console.log("se usa el consultar??");
        }

        function clickEditable(event) {
            event.preventDefault();
            var td, campo, valor, id;
            $("td:not(.id)").removeClass("editable");
            td = $(this).closest("td");
            campo = $(this).closest("td").data("campo");
            valor = $(this).text();
            id = $(this).closest("tr").find(".id").text();
            td.text("").html(
                    "<input type='text' name='" + campo + "' value='" + valor
                    + "'><a class='enlace guardar' class=\"btn\">Guardar</a><a class='enlace cancelar' >Cancelar</a>");
        }

        function clickCancelar(event) {
            var td, campo, valor, id;
            td.html("<span>" + valor + "</span>");
            $("td:not(.id)").addClass("editable");
        }

        function clickGuardar(event) {
            event.preventDefault();
            var td, campo, valor, id;
            $(".mensaje").html("<img src='images/loading.gif'>");
            nuevovalor = $(this).closest("td").find("input").val();
            $.ajax({
                type: "POST",
                url: "editinplace.php",
                data: {
                    campo: campo,
                    valor: nuevovalor,
                    id: id
                }
            }).done(function (msg) {
                $(".mensaje").html(msg);
                td.html("<span>" + nuevovalor + "</span>");
                $("td:not(.id)").addClass("editable");
                setTimeout(function () {
                    $('.ok,.ko').fadeOut('fast');
                }, 3000);
            });
        }

        function getIdAlias() {
            var idAlias = "";
            var textAlias = document.getElementById("textAlias").value;
            if ((textAlias !== null) && (textAlias !== '')) {
                idAlias = idOcultos[availableTags.indexOf(textAlias)];
                if (idAlias === undefined) {
                    idAlias = "";
                }
            }
            if (idAlias === "") {
                alert("El alias introducido no existe");
            }
            return idAlias;
        }


        function cargarTabla(idAlias) {
            /* parte ajax */
            $scope.idAlias = idAlias;
            var data = new DataBean();
            data.setService('SIBBACServiceFactura');
            data.setAction('getListadoFacturasPendienteCobros');
            var filters = "{ \"idAlias\" : \"" + idAlias + "\" }";
            data.setFilters(filters);

            var request = requestSIBBAC(data);
            request
                    .success(function (json) {
                        var datos = undefined;
                        if (json === undefined || json.resultados === undefined || json.resultados.listadoFacturas === undefined) {
                            datos = {};
                        } else {
                            datos = json.resultados.listadoFacturas;
                        }

                        oTable.fnClearTable();
                        marcarChecks = [];
                        $scope.arrImporteAplicado = [];
                        $scope.arrImportePendiente = [];
                        $scope.arrImporteTotal = [];
                        idsFacturas = [];
                        var factura = null;
                        var idChk;
                        for (var k in datos) {
                            factura = datos[k];
                            var idFactura = factura.idFactura;
                            var importePendiente = factura.importePendiente;
                            idsFacturas.push(idFactura);
                            $scope.arrImporteAplicado.push(0);
                            $scope.arrImportePendiente.push(parseFloat(factura.importePendiente));
                            $scope.arrImporteTotal.push(parseFloat(factura.importeTotal));
                            idChk = "ckFila" + idFactura;
                            oTable.fnAddData([
                                "<input id='" + idChk + "' type='checkbox' disabled class='checkTabla' idFactura='" + idFactura + "' numeroFactura='" + factura.numeroFactura + "' fechaFactura='" + transformaFecha(factura.fechaCreacion) + "' importePendiente='" + $.number(factura.importePendiente, 2, ',', '.') + "' importeTotal='" + $.number(factura.importeTotal, 2, ',', '.') + "'  />",
                                "<input id='ckCerrar" + idFactura + "' type='checkbox' class='checkTabla' disabled />",
                                "<span>" + factura.numeroFactura + "</span>",
                                "<span>" + transformaFecha(factura.fechaCreacion) + "</span>",
                                "<span>" + $.number(factura.baseImponible, 2, ',', '.') + "</span>",
                                "<span>" + $.number(factura.importeImpuestos, 2, ',', '.') + "</span>",
                                "<span>" + $.number(factura.importeTotal, 2, ',', '.') + "</span>",
                                "<div id=importePendiente" + idFactura + ">" + $.number(factura.importePendiente, 2, ',', '.') + "</div>",
                                "<div id=importeAplicado" + idFactura + ">" + $.number('0,00', 2, ',', '.') + "</div>",
                                "<a class=\"btn\" id='modificarImporte" + idFactura + "'><img id='img" + idFactura + "' src='img/editp.png' title='Editar'/></a>"]);
                            $("#" + idChk).click(function () {
                                var idFactura = $(this).attr('idFactura');
                                var importeCobro = $scope.importeCobro;
                                var importeVencido = $scope.importeVencido;
                                var saldoPendiente = parseFloat(importeCobro - importeVencido);
                                var posArray = getPosicionArray(idFactura);
                                var importeAplicado = $scope.arrImporteAplicado[posArray];
                                var importePendiente = $scope.arrImportePendiente[posArray];
                                if ($(this).is(':checked')) {
                                    // Hemos seleccionado la factura
                                    if (saldoPendiente <= importePendiente) {
                                        importeAplicado = saldoPendiente;
                                    } else {
                                        importeAplicado = importePendiente;
                                    }
                                    importeVencido = importeVencido + importeAplicado;
                                    $scope.arrImporteAplicado[posArray] = importeAplicado;
                                    marcarChecks.push(idFactura);
                                    $('#modificarImporte' + idFactura).click(function (e) {
                                        e.preventDefault();
                                        cargarInputsCobro(idFactura);
                                    });
                                } else {
                                    // Hemos deseleccionado la factura
                                    importeVencido = importeVencido - importeAplicado;
                                    importeAplicado = 0, 00;
                                    $scope.arrImporteAplicado[posArray] = 0;
                                    marcarChecks.splice($.inArray(idFactura, marcarChecks), 1);
                                    $('#modificarImporte' + idFactura).unbind('click');
                                }
                                if (marcarChecks.length === 0) {
                                    $('#importeCobro').removeAttr('disabled');
                                } else {
                                    $('#importeCobro').attr('disabled', true);
                                }

                                saldoPendiente = importeCobro - importeVencido;
                                $('#importeVencido').val($.number(importeVencido, 2, ',', '.'));
                                $('#saldoPendiente').val($.number(saldoPendiente, 2, ',', '.'));
                                $('#importeAplicado' + idFactura).text($.number(importeAplicado, 2, ',', '.'));
                                $scope.importeVencido = importeVencido;
                                $scope.saldoPendiente = saldoPendiente;
                                $scope.arrImporteAplicado[posArray] = importeAplicado;

                            });
                        }
                    });
        }


        function cargarInputsCobro(idFactura) {
            document.getElementById('formulariosModificarCobro').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            $('#idFactura').val(idFactura);
            var checkIdFactura = $('#ckFila' + idFactura);
            $('#numeroFactura').val(checkIdFactura.attr('numeroFactura'));
            $scope.mod.idFactura = idFactura;
            $('#fechaFactura').val(checkIdFactura.attr('fechaFactura'));
            var posArray = getPosicionArray(idFactura);
            $scope.mod.importeTotal = $scope.arrImporteTotal[posArray];
            $('#importeTotal').val(checkIdFactura.attr('importeTotal'));

            $('#importePendiente').val(checkIdFactura.attr('importePendiente'));
            $scope.mod.importePendiente = $scope.arrImportePendiente[posArray];
            $scope.mod.importeAplicado = $scope.arrImporteAplicado[posArray];
            var importeAplicado = $scope.mod.importeAplicado;
            $('#importeAplicado').val($.number(importeAplicado, 2, ',', '.'));

            var ckCerrarTabla = $('#ckCerrar' + idFactura).prop('checked');
            $('#ckCerrar').prop('checked', ckCerrarTabla);
            var saldoPendiente = $scope.saldoPendiente;
            var importeVencido = $scope.importeVencido;
            $scope.mod.saldoPendienteCobro = saldoPendiente + importeAplicado;
            $scope.mod.importeVencidoCobro = importeVencido - importeAplicado;
            $('#saldoPendienteCobro').val($scope.mod.saldoPendienteCobro);
            $('#importeVencidoCobro').val($scope.mod.importeVencidoCobro);
        }

        function SendAsExport(format) {
            var c = this.document.forms['crearCobro'];
            var f = this.document.forms["export"];
            console.log("Forms: " + f.name + "/" + c.name);
            f.action = "/sibbac20back/rest/export." + format;
            var json = {
                "service": "SIBBACServiceFactura",
                "action": "getListadoFacturasPendienteCobros"
            };
            if (json != null && json != undefined && json.length == 0) {
                alert("Introduzca datos");
            } else {
                f.webRequest.value = JSON.stringify(json);
                f.submit();
            }
        }


    }]);

