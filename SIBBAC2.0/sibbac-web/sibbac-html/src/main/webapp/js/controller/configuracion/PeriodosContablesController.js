'use strict';
sibbac20
    .controller(
                'PeriodosContablesController',
                [
                 '$scope',
                 '$http',
                 '$rootScope',
                 '$compile',
                 'IsinService',
                 '$location',
                 'SecurityService',
                 function ($scope, $http, $rootScope, $compile, IsinService, $location, SecurityService) {

                   var datepickerCheck = 0;// carga de datepickers
                   var checkTime = 0;// carga de tabla
                   var oTable = undefined;

                   $scope.safeApply = function (fn) {
                     var phase = this.$root.$$phase;
                     if (phase === '$apply' || phase === '$digest') {
                       if (fn && (typeof (fn) === 'function')) {
                         fn();
                       }
                     } else {
                       this.$apply(fn);
                     }
                   };

                   $(document).ready(
                                     function () {

                                       oTable = $("#tablaPeriodosContables").dataTable({
                                         "dom" : 'T<"clear">lfrtip',
                                         "tableTools" : {
                                           "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                                         },
                                         "scrollY" : "480px",
                                         "scrollCollapse" : true,
                                         "createdRow" : function (row, data, index) {
                                           $compile(row)($scope);
                                         },
                                         "language" : {
                                           "url" : "i18n/Spanish.json"
                                         },
                                         columnDefs : [ {
                                           type : 'date-eu',
                                           targets : [ 2, 3 ]
                                         } ]
                                       });
                                       /* Eva:Función para cambiar texto */
                                       jQuery.fn.extend({
                                         toggleText : function (a, b) {
                                           var that = this;
                                           if (that.text() != a && that.text() != b) {
                                             that.text(a);
                                           } else if (that.text() == a) {
                                             that.text(b);
                                           } else if (that.text() == b) {
                                             that.text(a);
                                           }
                                           return this;
                                         }
                                       });
                                       /* FIN Función para cambiar texto */

                                       $('a[href="#release-history"]').toggle(function () {
                                         $('#release-wrapper').animate({
                                           marginTop : '0px'
                                         }, 600, 'linear');
                                       }, function () {
                                         $('#release-wrapper').animate({
                                           marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
                                         }, 600, 'linear');
                                       });

                                       $('#download a').mousedown(function () {
                                         _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
                                       });

                                       $('.collapser').click(function () {
                                         $(this).parents('.title_section').next().slideToggle();
                                         // $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                                         $(this).toggleClass('active');
                                         $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

                                         return false;
                                       });
                                       $('.collapser_search').click(function () {
                                         $(this).parents('.title_section').next().slideToggle();
                                         $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                                         $(this).toggleClass('active');
                                         // $(this).toggleText( "Mostrar Alta de Período Contable", "Ocultar Alta de
                                         // Período
                                         // Contable" );
                                         if ($(this).text() == "Ocultar Alta de Período Contable") {
                                           $(this).text("Mostrar Alta de Período Contable");
                                         } else {
                                           $(this).text("Ocultar Alta de Período Contable");
                                         }
                                         return false;
                                       });

                                       /* BUSCADOR */
                                       $('.btn.buscador').click(
                                                                function () {
                                                                  // event.preventDefault();
                                                                  $('.resultados').show();
                                                                  $('.collapser_search').parents('.title_section')
                                                                      .next().slideToggle();
                                                                  $('.collapser_search').toggleClass('active');
                                                                  $('.collapser_search')
                                                                      .toggleText('Mostrar opciones de búsqueda',
                                                                                  'Ocultar opciones de búsqueda');
                                                                  return false;
                                                                });
                                       /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
                                       if ($('.contenedorTabla').length >= 1) {
                                         var anchoBotonera;
                                         $('.contenedorTabla').each(function (i) {
                                           anchoBotonera = $(this).find('table').outerWidth();
                                           $(this).find('.botonera').css('width', anchoBotonera + 'px');
                                           $(this).find('.resumen').css('width', anchoBotonera + 'px');

                                         });
                                       }

                                       $scope.altaPeridoContable = function () {
                                         var nombre = document.getElementById('textNombre').value;

                                         var estado = document.getElementById("textEstado");
                                         var estadoIndiceSeleccionado = estado.selectedIndex;
                                         var estadoOpcionSeleccionada = estado.options[estadoIndiceSeleccionado];
                                         var estadoTextoSeleccionado = estadoOpcionSeleccionada.text;
                                         var estadoValorSeleccionado = estadoOpcionSeleccionada.value;

                                         var lista = document.getElementById("textIdentificador");
                                         var indiceSeleccionado = lista.selectedIndex;
                                         var opcionSeleccionada = lista.options[indiceSeleccionado];
                                         var textoSeleccionado = opcionSeleccionada.text;
                                         var valorSeleccionado = opcionSeleccionada.value;

                                         var fechaInicio = document.getElementById('fecInicio').value;
                                         var fechaHasta = document.getElementById('fecFin').value;

                                         if (nombre == "") {
                                           alert("El nombre es obligatorio");
                                           return false;
                                         } else if (estadoValorSeleccionado == "0") {
                                           alert("El estado es obligario");
                                           return false;
                                         } else if (valorSeleccionado == "0") {
                                           alert("El identificador es obligatorio");
                                           return false;
                                         } else if (!validarFecha('fecInicio')) {
                                           alert("La fecha de inicio no tiene el formato correcto (dd/mm/yyyy)");
                                           return false;
                                         } else if (!validarFecha('fecFin')) {
                                           alert("La fecha de fin no tiene el formato correcto (dd/mm/yyyy)");
                                           return false;
                                         } else {
                                           crearPeriodoContable(nombre, estadoValorSeleccionado, valorSeleccionado,
                                                                fechaInicio, fechaHasta);
                                         }
                                       };

                                       // cargarTabla();

                                     });

                   function crearPeriodoContable (nombre, estadoValorSeleccionado, valorSeleccionado, fechaInicio,
                                                  fechaHasta) {

                     var filtro = "{\"nombre\" : \"" + nombre + "\"," + "\"estado\" : \"" + estadoValorSeleccionado
                                  + "\"," + "\"identificadores\" : \"" + valorSeleccionado + "\","
                                  + "\"fechaFinalPeriodo\" : \"" + transformaFechaInv(fechaHasta) + "\","
                                  + "\"fechaInicioPeriodo\" : \"" + transformaFechaInv(fechaInicio) + "\" }";

                     var jsonData = "{" + "\"service\" : \"SIBBACServicePeriodos\", "
                                    + "\"action\"  : \"createPeriodos\", " + "\"filters\"  : " + filtro + "}";

                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : jsonData,
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                         cargarTabla();
                         //document.getElementById("cargarTabla").reset();
                       },
                       error : function (c) {
                         console.log("ERROR: " + c);
                       }

                     });
                   }

                   function cargarTablaBody () {
                     console.log("entro en el método");
                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : "{\"service\" : \"SIBBACServicePeriodos\", \"action\"  : \"getPeriodos\"}",
                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                         var datos = [];
                         if (json === undefined || json.resultados === undefined) {
                           datos = {};
                         } else {
                           datos = json.resultados;
                         }

                         oTable.fnClearTable();
                         for ( var k in datos) {
                           var item = datos[k];
                           var stateName = (item.estado != null) ? item.estado.nombre : "&lt;No data&gt;";

                           // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                           var enlace = "";
                           if (SecurityService.inicializated) {
                             if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location.path())) {
                               enlace = " <a class=\"btn\"   ng-click=\"borrarPeriodo('" + item.id + "','"
                                        + item.nombre + "');\"><img src='img/del.png'  title='Eliminar'/></a> "
                             }
                           }

                           oTable.fnAddData([ item.nombre, stateName, transformaFechaGuion(item.fechaInicio),
                                             transformaFechaGuion(item.fechaFinal), enlace ]);

                         }
                         $('.resultados').show();
                         oTable.fnDraw();

                         // var resultados = json.resultados;
                         // var item = null;
                         // var contador = 0;
                         // var stateName = null;
                         //
                         // for ( var k in resultados ) {
                         // item = resultados[ k ];
                         // stateName = ( item.estado!=null ) ? item.estado.nombre : "&lt;No data&gt;";
                         // if (contador%2==0) {
                         // estilo = "even";
                         // } else {
                         // estilo = "odd";
                         // }
                         //
                         // // Pintamos la tabla "padre". La cabecera...
                         // $('.tablaPrueba').append(
                         // "<tr class='"+ estilo +"'>" +
                         // "<td class='taleft'>"+item.nombre + "</td>" +
                         // "<td class='taleft'>"+stateName + "</td>" +
                         // "<td class='taleft'>"+transformaFechaGuion(item.fechaInicio) + "</td>" +
                         // "<td class='taleft'>"+transformaFechaGuion(item.fechaFinal) + "</td>" +
                         // "<td class='taleft' ><a class=\"btn\" onclick=\"borrarPeriodo('"+item.id+"','"+
                         // item.nombre+"');\"><img src='img/del.png' title='Eliminar'/></a></td>" +
                         // "</tr>"
                         //
                         // );
                         // contador++;
                         // }
                         //
                         // // "Footer" de la tabla "padre".
                         // $('.tablaPrueba').append( "</table>" );

                       },
                       error : function (c) {
                         console.log("ERROR: " + c);
                       }
                     });
                     $scope.safeApply();
                     $.unblockUI();
                   }

                   // funcion para cargar la tabla
                   function cargarTabla () {
                     inicializarLoading();
                     cargarTablaBody();

                     // setTimeout("cargarTablaBody()", 3000);

                   }

                   $scope.refrescarPeriodosContables = function () {
                     cargarTabla();
                   }

                   $scope.borrarPeriodo = function (id, nombre) {
                     var filtro = "{\"id\" : \"" + id + "\" }";
                     var jsonData = "{" + "\"service\" : \"SIBBACServicePeriodos\", "
                                    + "\"action\"  : \"deletePeriodos\", " + "\"filters\"  : " + filtro + "}";

                     if (confirm("¿Desea borrar realmente el periodo " + nombre + "?") == true) {
                       $.ajax({
                         type : "POST",
                         dataType : "json",
                         url : "/sibbac20back/rest/service",
                         data : jsonData,
                         beforeSend : function (x) {
                           if (x && x.overrideMimeType) {
                             x.overrideMimeType("application/json");
                           }
                           // CORS Related
                           x.setRequestHeader("Accept", "application/json");
                           x.setRequestHeader("Content-Type", "application/json");
                           x.setRequestHeader("X-Requested-With", "HandMade");
                           x.setRequestHeader("Access-Control-Allow-Origin", "*");
                           x.setRequestHeader('Access-Control-Allow-Headers',
                                              'Origin, X-Requested-With, Content-Type, Accept')
                         },
                         async : true,
                         success : function (json) {

                           if (json.resultados == null) {
                             alert(json.error);
                           } else {
                             cargarTabla();
                           }

                         },
                         error : function (c) {
                           console.log("ERROR: " + c);
                         }
                       });
                     } else {
                       // alert("You pressed Cancel!");
                     }

                   }

                   function cargarIdentificador () {
                     $
                         .ajax({
                           type : "POST",
                           dataType : "json",
                           url : "/sibbac20back/rest/service",
                           data : "{\"service\" : \"SIBBACServiceIdentificadores\", \"action\"  : \"getListaIdentificadores\"}",

                           beforeSend : function (x) {
                             if (x && x.overrideMimeType) {
                               x.overrideMimeType("application/json");
                             }
                             // CORS Related
                             x.setRequestHeader("Accept", "application/json");
                             x.setRequestHeader("Content-Type", "application/json");
                             x.setRequestHeader("X-Requested-With", "HandMade");
                             x.setRequestHeader("Access-Control-Allow-Origin", "*");
                             x.setRequestHeader('Access-Control-Allow-Headers',
                                                'Origin, X-Requested-With, Content-Type, Accept')
                           },
                           async : true,
                           success : function (json) {
                             var resultados = json.resultados;
                             var item = null;

                             for ( var k in resultados) {
                               item = resultados[k];
                               // Rellenamos el combo
                               $('#textIdentificador').append(
                                                              "<option value='" + item.id + "'>"
                                                                  + item.indentificadores + "</option>"

                               );

                             }
                           },
                           error : function (c) {
                             console.log("ERROR: " + c);
                           }
                         });

                   }

                   // cargar estados
                   function cargarEstado () {
                     $.ajax({
                       type : "POST",
                       dataType : "json",
                       url : "/sibbac20back/rest/service",
                       data : "{\"service\" : \"SIBBACServicePeriodos\", \"action\"  : \"getEstados\"}",

                       beforeSend : function (x) {
                         if (x && x.overrideMimeType) {
                           x.overrideMimeType("application/json");
                         }
                         // CORS Related
                         x.setRequestHeader("Accept", "application/json");
                         x.setRequestHeader("Content-Type", "application/json");
                         x.setRequestHeader("X-Requested-With", "HandMade");
                         x.setRequestHeader("Access-Control-Allow-Origin", "*");
                         x.setRequestHeader('Access-Control-Allow-Headers',
                                            'Origin, X-Requested-With, Content-Type, Accept')
                       },
                       async : true,
                       success : function (json) {
                         var resultados = json.resultados;
                         var item = null;

                         for ( var k in resultados) {
                           item = resultados[k];
                           // Rellenamos el combo
                           $('#textEstado').append("<option value='" + item.idestado + "'>" + item.nombre + "</option>"

                           );

                         }
                       },
                       error : function (c) {
                         console.log("ERROR: " + c);
                       }
                     });

                   }

                   function initialize () {
                     // Si se ha llamado initialize desde un setTimeout,
                     // se comprueba y se elimina
                     var fechas = [ 'fecInicio', 'fecFin' ];
                     loadpicker(fechas);
                   }
                   $(document).ready(function () {
                     initialize();
                     cargarTabla();
                     cargarIdentificador();
                     cargarEstado();
                   });

                   function render () {

                   }

                 } ]);
