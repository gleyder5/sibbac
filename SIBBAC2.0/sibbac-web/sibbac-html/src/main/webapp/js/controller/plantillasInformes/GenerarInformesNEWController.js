'use strict';
sibbac20.controller('GenerarInformesNEWController',

[
 '$scope',
 '$document',
 'growl',
 'GenerarInformesNEWService',
 '$compile',
 '$location',
 'commonHelper',
 'DirectivasService',
 'CONSTANTES',
 'camposDinamicosHelper',
 '$rootScope',
 function ($scope, $document, growl, GenerarInformesNEWService, $compile, $location, commonHelper, DirectivasService, CONSTANTES, camposDinamicosHelper, $rootScope) {

   $scope.safeApply = function (fn) {
     var phase = this.$root.$$phase;
     if (phase === '$apply' || phase === '$digest') {
       if (fn && (typeof (fn) === 'function')) {
         fn();
       }
     } else {
       this.$apply(fn);
     }
   };

   /*******************************************************************************************************************
     * ** INICIALIZACION DE DATOS ***
     *
     ******************************************************************************************************************/

   // Variables y listas general
   $scope.plantilla = {};

   $scope.listPlantillas = [];
   $scope.clasesRouting = [];

   $scope.mostrarBuscador = true;
   $scope.mostrarErrores = false;
   $scope.mensajeError = "";
   $scope.mensajeErrorQuery = "";
   $scope.mostrardescargarexcel = false;
   $scope.mostrardescargarexcelSimple = false;
   $scope.activeAlert = false;
   $scope.listClasesFiltro = [];
   $scope.listMercados = [];
   $scope.errorValidarFechaBase = false;
   $scope.errorValidarFechaBaseNoEncontrada = false;
   $scope.file = "";
   $scope.fileSimpleXLS = "";
   $scope.controlDias = false;

   // Reset del filtro
   $scope.resetFiltro = function () {
	 /** Inicio - Componente filtros dinamicos. */
	 camposDinamicosHelper.vaciarFiltrosComp($scope);  
	 /** Fin - Componente filtros dinamicos. */

     $scope.filtro = {
       clase : "",
       mercado : {key: "", value: ""},
       plantilla : "0",
       especiales : "EXCLROUTING",
       ocultarEspeciales : true,
       /** Inicio - Componente filtros dinamicos. */
       listaFiltrosDinamicos: []
       /** Fin - Componente filtros dinamicos. */
     };

     $scope.plantilla = {
       key : "0",
       value : ""
     };

     $scope.activeAlert = false;

     $scope.limiteRegistrosExcel = 0;
     $scope.limiteRegistrosPantalla = 0;
     $scope.limiteDias = 0;
     $scope.superaLimite = false;

   };

   $scope.resetFiltro();

   /*******************************************************************************************************************
     * ** INICIALIZACION DE DATEPICKERS ***
     *
     ******************************************************************************************************************/
   var datepickerCheck = 0;
   $(document).ready(function () {
     renderDatePickers();
   });
   function renderDatePickers () {
     if (datepickerCheck !== undefined && datepickerCheck > 0) {
       clearTimeout(datepickerCheck);
     }
     if ($.datepicker !== undefined) {
       cargarCalendarioEsp();
       // se activan los datepickers necesarios en la página

     } else {
       datepickerCheck = setTimeout("renderDatePickers()", 300);
     }
   }
   
   var datepickerCheckFilter = 0;
   $scope.renderDatePickersFilter = function (indentificador) {
     if (datepickerCheckFilter !== undefined && datepickerCheckFilter > 0) {
       clearTimeout(datepickerCheckFilter);
     }
     if ($.datepicker !== undefined) {
       cargarCalendarioEsp();
       // se activan los datepickers necesarios en la página

       $("#" + indentificador).datepicker({
         dateFormat : 'dd/mm/yy'
       });
     } else {
    	 datepickerCheckFilter = setTimeout("renderDatePickersFilter()", 300);
     }
   }

   /*******************************************************************************************************************
     * ** FUNCIONES GENERALES ***
     *
     ******************************************************************************************************************/
   $scope.showingFilter = true;

   // Control para ocultar o presentar el panel de busqueda
   $scope.mostrarBusqueda = function() {
     $('.mensajeBusqueda').empty();
     var cadenaFiltros = "";
     if ($scope.plantilla.value !== "") {
       cadenaFiltros += " Plantilla: " + $scope.plantilla.value;
     }

     /** Inicio - Componente filtros dinamicos. */
     if (!commonHelper.isUndefinedOrNullOrEmpty($scope.filtro.listaFiltrosDinamicos)) {
    	 cadenaFiltros += camposDinamicosHelper.getCadenaFiltroCamposDinam($scope);
     }
     /** Fin - Componente filtros dinamicos. */
     $('.mensajeBusqueda').append(cadenaFiltros);
   }

   // Borrar el DataTable y los resultados de la página
   $scope.borrarResultadosAnt = function () {
     $scope.mostrarErrores = false;
     if ($scope.oTable != null) {
       $scope.oTable.fnClearTable();
       var table = $('#datosConsultaInforme').DataTable();
       table.destroy();

     }
   }

   // Valida los campos obligatorios de la pantilla de Generacion de informes
   $scope.validacion = function () {
	   
	 var result = false;
     $scope.borrarResultadosAnt();

     $('#datosConsultaInforme').remove();
     
     
     if ($scope.plantilla.key !== "0" && $scope.errorValidarFechaBase===false && $scope.errorValidarFechaBaseNoEncontrada === false) {

       $scope.filtro.plantilla = $scope.plantilla.key;

       /** Inicio - Componente filtros dinamicos. */
       // Convertir fechas numericas en caso de haber
//       camposDinamicosHelper.getFechasFormateadas($scope.arrComponenteDirectiva);
//       if ($scope.arrComponenteDirectiva !== "" && $scope.arrComponenteDirectiva != null) {
//    	   $scope.filtro.arrComponenteDirectiva = $scope.arrComponenteDirectiva;
//       } else {
//    	   $scope.filtro.arrComponenteDirectiva = [];
//       }
       /** Fin - Componente filtros dinamicos. */
       
       // Se oculta el panel de búsqueda y se muestra el detalle
       $scope.showingFilter = false;
       // Se muestra la capa cargando
       inicializarLoading();
       $scope.activeAlert = false;
       result = true;
     } else {
       $scope.activeAlert = true;
     }
     return result;
   }

   $scope.controlError = function (txterror) {
     if (txterror.length == 0) {
       txterror = "La generación del informe ha devuelto un error, por favor contacte con Soporte IT.";
     }
     fErrorTxt(txterror, 1);
     $scope.mensajeError = txterror;
     $scope.mostrarErrores = true;
   }

   var pupulateAutocomplete = function (input, availableTags) {
     $(input).autocomplete({
       minLength : 0,
       source : availableTags,
       focus : function (event, ui) {
         return false;
       },
       select : function (event, ui) {

         var option = {
           key : ui.item.key,
           value : ui.item.value
         };

         $scope.safeApply();
         return false;
       }
     });
   };
   
   $scope.pupulateAutocompleteDinamic = function (input, availableTags, index, index2) {
	   $(input).autocomplete({
           minLength : 0,
           source : availableTags,
           focus : function (event, ui) {
             return false;
           },
           select : function (event, ui) {

             var option = {
               key : ui.item.key,
               value : ui.item.value
             };
             
             switch (input) {
             case '#' + $scope.filtro.listaFiltrosDinamicos[index][index2].identificador:
            	 	var existe = false;
                 	angular.forEach($scope.filtro.listaFiltrosDinamicos[index][index2].listaSeleccSelectDTO, function (campo) {
                 		if (campo.value === option.value.trim()) {
                 			existe = true;
                 		}
                 	});
                 	$scope.filtro.listaFiltrosDinamicos[index][index2].valor = "";
                 	if (!existe) {
                 		$scope.filtro.listaFiltrosDinamicos[index][index2].listaSeleccSelectDTO.push(option);
                 	}
             	default: break;
             }
             
             $scope.safeApply();

             return false;
           }
       });
    };

   /*******************************************************************************************************************
     * ** CARGA DE DATOS INICIAL ***
     *
     *
     ******************************************************************************************************************/
   // Carga del combo Plantillas
   $scope.cargaPlantillas = function(){
	   GenerarInformesNEWService.cargaPlantillas(function (data) {
	     $scope.listPlantillas = data.resultados.listaInformes;
	     // Se inicializa tambien el limite total de registros
	     $scope.limiteRegistrosExcel = data.resultados.limite_total;
	     $scope.limiteRegistrosPantalla = data.resultados.limite_total_pantalla;
	     $scope.limiteDias = data.resultados.limite_dias;
	     //Se recuperan las clases que pueden hacer routing
	     $scope.clasesRouting = data.resultados.clasesRouting;
	   }, function (error) {
	     console.log("Error en la carga del combo listPlantillas: " + error);
	   });
   };
   
   $scope.cargaPlantillas();
   
   // Carga del combo Clases
   $scope.cargarClases = function(){
	   GenerarInformesNEWService.cargarClases(function (data) {
		   $scope.listClasesFiltro = data.resultados["listClases"];
	   }, function (error) {
		   fErrorTxt("Se produjo un error en la carga del combo de clases.", 1);
	   });
   };
   
   $scope.cargarClases();


   // Se carga inicialmente el usuario en el filtro
   // ya que se necesita para la generacion del informe
   $scope.filtro.userName = $rootScope.userName;

   /*******************************************************************************************************************
     * ** ACCIONES ***
     *
     ******************************************************************************************************************/
   // Descarga un archivo excel con el contenido pasado como parametro
   $scope.descargarExcel = function (ExcelBytes) {
     var byteNumbers = new Array(ExcelBytes.length);
     for (var i = 0; i < ExcelBytes.length; i++) {
       byteNumbers[i] = ExcelBytes.charCodeAt(i);
     }

     var byteArray = new Uint8Array(byteNumbers);

     var blob = new Blob([ byteArray ], {
       type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
     });

     var nav = navigator.userAgent.toLowerCase();

     if (navigator.msSaveBlob) {
       navigator.msSaveBlob(blob, "reporte.xlsx");
     } else {
       var blobUrl = URL.createObjectURL(blob);
       var link = document.createElement('a');
       link.href = blobUrl = URL.createObjectURL(blob);
       link.download = "reporte.xlsx";
       document.body.appendChild(link);
       link.click();
       document.body.removeChild(link);
     }

   }

   // Funcion general para pintar datatable y tratar errores
   $scope.tratarDatos = function (data, descargarExcelTerceros, descargarExcelSimple) {
     $scope.mensajeErrorQuery = "";
     if (data.hasOwnProperty("resultados")) {
       if (data.resultados.status === 'KO') {
         $scope.controlError("El informe termino de forma inesperada. Pongase en contacto con el Administrador");
       } else {

         // se obtienen los registros del informe. Se espera una lista de listas de ColumnaInformeDTO
         // (columna-valor)
         var list = data.resultados.informe.resultados;
         if (list.length > 0) {
           $scope.listRegistros = list;

           var contenidoColumnas = [];
           var contenidoFilas = [];

           // se recorren las filas devueltas por la consulta
           $.each($scope.listRegistros, function (index_i, registro) {
             var contenidoFila = [];
             // se recorren las columnas de cada fila
             $.each(registro, function (index_j, contenido) {
               // para la primera fila nos quedamos con las keys como cabeceras
               if (index_i == 0) {
                 var ao = {
                   sTitle : contenido.columna
                 };
                 contenidoColumnas.push(ao);
               }
               contenidoFila.push(contenido.valor);
             });
             contenidoFilas.push(contenidoFila);
           });

           $('#contenedordatos')
               .append('<table id="datosConsultaInforme" class="fullTable ancha" cellspacing="0"></table>');
           // Se pintan las columnas en la tabla
           $scope.oTable = $("#datosConsultaInforme").dataTable({
             "dom" : 'T<"clear">lfrtip',
             "tableTools" : {
               "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf",
               "aButtons" : []
             },
             "aoColumns" : contenidoColumnas,
             "order" : [],
             "scrollY" : "480px",
             "scrollX" : "100%",
             "scrollCollapse" : true,
             "language" : {
               "url" : "i18n/Spanish.json"
             }
           });

           // Se pinta el contenido en la tabla
           $scope.oTable.fnAddData(contenidoFilas, false);

           // Se controla que haya resultados pero no se haya generado la excel
           if (data.resultados.informe.errores.length > 0) {
             $scope.controlError(data.resultados.informe.errores[0]);
           } else {
             if (descargarExcelTerceros) {
               // se obtiene el contenido en excel de terceros y se descarga
               var decodedString = atob([ data.resultados.file ]);
               $scope.descargarExcel(decodedString);
             } else {
               $scope.file = data.resultados.file;
             }

             if (descargarExcelSimple) {
               // se obtiene el contenido en excel de terceros y se descarga
               var decodedString = atob([ data.resultados.fileSimpleXLS ]);
               $scope.descargarExcel(decodedString);
             } else {
               $scope.fileSimpleXLS = data.resultados.fileSimpleXLS;
             }
           }
         } else {
           if (data.resultados.informe.errores.length > 0) {
             // Error controlado
             $scope.controlError(data.resultados.informe.errores[0]);
           } else {
             $scope.controlError("Los filtros seleccionados no han generado resultados.");
           }
         }
       }
       // Se desbloquea la página
       $.unblockUI();
     } else {
       // Se hace una llamada para capturar la query que ha dado el problema
       GenerarInformesNEWService.obtenerQueryInforme(function (data) {
         // Se obtienen los errores controlados
         $scope.mensajeErrorQuery = data.resultados.informe.query;
         $scope.controlError("");

         // Se desbloquea la página
         $.unblockUI();
       }, function (error) {
         $.unblockUI();
         $scope.controlError("");
       }, $scope.filtro);
     }
   }
   
   // Se valida generacion del informe Excel
   $scope.validaGenerarInformeExcel = function () {
	   
	   if ($scope.plantilla.fechaBase!="") {
		   var campo = "";
//		   var alias = "";
		   var campoFecha = "";
//		   var aliasFecha = "";
		   if (null!=$scope.filtro.listaFiltrosDinamicos && $scope.filtro.listaFiltrosDinamicos.length>0) {
			   for (var i=0; i<$scope.filtro.listaFiltrosDinamicos.length; i++) {
				   for (var j=0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++) {
					   if ($scope.filtro.listaFiltrosDinamicos[i][j].tipo == CONSTANTES.TIPO_FECHA) {
						   // Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
						   if ($scope.filtro.listaFiltrosDinamicos[i][j].campo.indexOf('/') != -1) {
							   campo = $scope.filtro.listaFiltrosDinamicos[i][j].campo.split('/', -1);
							   campoFecha = campo[$scope.plantilla.posicion];
						   } else {
							   campo = $scope.filtro.listaFiltrosDinamicos[i][j].campo;
							   campoFecha = campo;
						   }
						   // Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
//						   if ($scope.filtro.listaFiltrosDinamicos[i][j].tabla.indexOf('/') != -1) {
//							   alias = $scope.filtro.listaFiltrosDinamicos[i][j].tabla.split('/', -1);
//							   aliasFecha = alias[$scope.plantilla.posicion];
//						   } else {
//							   alias = $scope.filtro.listaFiltrosDinamicos[i][j].tabla;
//							   aliasFecha = alias;
//						   }
//						   var fechaBaseCampo = aliasFecha + '.' + campoFecha;
						   if (campoFecha === $scope.plantilla.fechaBase) {
							   $scope.labelFechaObligatoria = $scope.filtro.listaFiltrosDinamicos[i][j].nombre;
							   $scope.errorValidarFechaBase = true;
							   if ((null!=$scope.filtro.listaFiltrosDinamicos[i][j].valorCampo && $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo !="") || 
									   (null!=$scope.filtro.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde && $scope.filtro.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde !="")) {
								   // Si se cumple condicion de fecha base se toma como valida.
								   $scope.errorValidarFechaBase = false;
								   $scope.errorValidarFechaBaseNoEncontrada = false;
								   i = $scope.filtro.listaFiltrosDinamicos.length;
								   break;
							   }
						   } else {
							   $scope.errorValidarFechaBaseNoEncontrada = true;
						   }
					   }
				   }
			   }
			   if ($scope.errorValidarFechaBase) {
				   $scope.errorRangoFechas = true;
				   $scope.errorValidarFechaBaseNoEncontrada = false;
			   } else if ($scope.errorValidarFechaBaseNoEncontrada) {
				   $scope.errorRangoFechas = true;
			   }
		   }
	   } else {
		   $scope.errorValidarFechaBase = false;
		   $scope.errorValidarFechaBaseNoEncontrada = false;
	   }
	   
	   if ($scope.validacion()) {
		   // Se debe validar si se confirma la generacion del Excel en base
		   // a la fecha de fin de generacion del informe
		   GenerarInformesNEWService.validarGeneracionExcel(function(data) {
			   if (!commonHelper.isUndefinedOrNull(data.resultadosError)
					   && data.resultadosError.msgConfirmGeneraExcel != null
					   && data.resultadosError.msgConfirmGeneraExcel.length) {
				   var msgConfirmacion = data.resultadosError.msgConfirmGeneraExcel;
				   if(msgConfirmacion.length > 0) {
					   if (!commonHelper.isUndefinedOrNull(msgConfirmacion)) {
						   $scope.confirmaGeneracion(msgConfirmacion);
					   } 
				   }
			   } else {
				   $scope.generarInforme();
			   }
		   },
		   function(error) {
			   fErrorTxt("Se produjo un error al validar la generación del Excel.", 1);
			   $.unblockUI();
		   }, $scope.filtro);
	   }
   };

   // Hace la llamada a generar informe
   $scope.generarInforme = function () {
     if ($scope.validacion()) {
    	 if ($scope.filtro.ocultarEspeciales) {
    		 $scope.filtro.especiales="";
    	 }
    	 $scope.validarNumeroDias(); 
    	 if ($scope.controlDias) {
    		 $scope.filtro.userName = $rootScope.userName;
    		 GenerarInformesNEWService.generarInforme(function (data) {
    			 inicializarLoading();

    			 if(data){
    				 if (undefined != data.resultados && (data.resultados.status === "KO" || undefined != data.resultados.timeout)) {
    					 if(data.resultados.status === "KO"){
    						 $scope.controlError("El informe termino de forma inesperada. Pongase en contacto con el Administrador");
    					 }else{
    						 $scope.controlError(data.resultados.timeout);
    					 }
    					 $.unblockUI();
    				 } else {
    					 for(var i = 0; i<$scope.filtro.listaFiltrosDinamicos.length; i++){
    						 for(var j = 0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++){
    							 if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampo === "01/01/1900") {
    								 $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo = "";
    							 }
    							 if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta === "31/12/9999") {
    								 $scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta = "";
    							 }
    						 }
    					 }

    					 //Si el limite de registros por pantalla se ha superado y el de excel no, no mostramos el grid y permitimos la descarga de ambas excels
    					 if ($scope.limiteRegistrosPantalla < data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel >= data.resultados.informe.resultados.length) {
    						 $scope.mostrardescargarexcel = true;
    						 $scope.mostrardescargarexcelSimple = true;
    						 $scope.fileSimpleXLS = data.resultados.fileSimpleXLS;
    						 $scope.file = data.resultados.file;
    						 $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se muestren en pantalla. Limite maximo establecido " + $scope.limiteRegistrosPantalla);
    						 $.unblockUI();
    						 //Si el limite de registros por pantalla se ha superado y el de excel tambien, no mostramos el grid ni permitimos la descarga de ambas excels
    					 } else if ($scope.limiteRegistrosPantalla < data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
    						 $scope.mostrardescargarexcel = false;
    						 $scope.mostrardescargarexcelSimple = false;
    						 $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se muestren en pantalla ni la generacion de la excel. Limite maximo establecido por excel: " + $scope.limiteRegistrosExcel + " . Limite maximo establecido por pantalla: " + $scope.limiteRegistrosPantalla);
    						 $.unblockUI();
    						 //Si el limite de registros por pantalla no se ha superado y el de excel si, mostramos el grid pero no permitimos la descarga del excel
    					 } else if ($scope.limiteRegistrosPantalla >= data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
    						 $scope.mostrardescargarexcel = false;
    						 $scope.mostrardescargarexcelSimple = false;
    						 $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se genere la excel. Limite maximo establecido " + $scope.limiteRegistrosExcel);
    						 $scope.tratarDatos(data, false, false);
    						 //Si el limite de registros por pantalla no se ha superado y el de excel tampoco, mostramos el grid y permitimos la descarga de excel
    					 } else {
    						 $scope.mostrardescargarexcel = true;
    						 $scope.mostrardescargarexcelSimple = true;
    						 $scope.tratarDatos(data, false, false);
    					 }
    				 }
    			 }else{
    				 $scope.controlError("El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
    				 $.unblockUI();
    			 }
    		 }, function (error) {
    			 $scope.mostrardescargarexcel = false;
    			 $scope.mostrardescargarexcelSimple = false;
    			 $.unblockUI();
    			 $scope.controlError("Error en la peticion de datos. Pongase en contacto con el Administrador");

    		 }, $scope.filtro);
    	 }
     }
   };

   // Hace la llamada a la generación de informe en excel
   $scope.generarInformeExcel = function () {
	   
	   if ($scope.plantilla.fechaBase!="") {
		   var campo = "";
//		   var alias = "";
		   var campoFecha = "";
//		   var aliasFecha = "";
		   if (null!=$scope.filtro.listaFiltrosDinamicos && $scope.filtro.listaFiltrosDinamicos.length>0) {
			   for (var i=0; i<$scope.filtro.listaFiltrosDinamicos.length; i++) {
				   for (var j=0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++) {
					   if ($scope.filtro.listaFiltrosDinamicos[i][j].tipo == CONSTANTES.TIPO_FECHA) {
						   // Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
						   if ($scope.filtro.listaFiltrosDinamicos[i][j].campo.indexOf('/') != -1) {
							   campo = $scope.filtro.listaFiltrosDinamicos[i][j].campo.split('/', -1);
							   campoFecha = campo[$scope.plantilla.posicion];
						   } else {
							   campo = $scope.filtro.listaFiltrosDinamicos[i][j].campo;
							   campoFecha = campo;
						   }
						   // Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
//						   if ($scope.filtro.listaFiltrosDinamicos[i][j].tabla.indexOf('/') != -1) {
//							   alias = $scope.filtro.listaFiltrosDinamicos[i][j].tabla.split('/', -1);
//							   aliasFecha = alias[$scope.plantilla.posicion];
//						   } else {
//							   alias = $scope.filtro.listaFiltrosDinamicos[i][j].tabla;
//							   aliasFecha = alias;
//						   }
//						   var fechaBaseCampo = aliasFecha + '.' + campoFecha;
						   if (campoFecha === $scope.plantilla.fechaBase) {
							   $scope.labelFechaObligatoria = $scope.filtro.listaFiltrosDinamicos[i][j].nombre;
							   $scope.errorValidarFechaBase = true;
							   if ((null!=$scope.filtro.listaFiltrosDinamicos[i][j].valorCampo && $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo !="") || 
									   (null!=$scope.filtro.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde && $scope.filtro.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde !="")) {
								   // Si se cumple condicion de fecha base se toma como valida.
								   $scope.errorValidarFechaBase = false;
								   $scope.errorValidarFechaBaseNoEncontrada = false;
								   i = $scope.filtro.listaFiltrosDinamicos.length;
								   break;
							   }
						   } else {
							   $scope.errorValidarFechaBaseNoEncontrada = true;
						   }
					   }
				   }
			   }
			   if ($scope.errorValidarFechaBase) {
				   $scope.errorRangoFechas = true;
				   $scope.errorValidarFechaBaseNoEncontrada = false;
			   } else if ($scope.errorValidarFechaBaseNoEncontrada) {
				   $scope.errorRangoFechas = true;
			   }
		   }
	   } else {
		   $scope.errorValidarFechaBase = false;
		   $scope.errorValidarFechaBaseNoEncontrada = false;
	   }
	   if ($scope.validacion()) {
		   if ($scope.filtro.ocultarEspeciales) {
			   $scope.filtro.especiales="";
		   }
		   $scope.filtro.userName = $rootScope.userName;
		   GenerarInformesNEWService.generarInformeExcel(function (data) {

			   if(data){
				   if (undefined != data.resultados && (data.resultados.status === "KO" || undefined != data.resultados.timeout)) {
					   if(data.resultados.status === "KO"){
						   $scope.controlError("El informe termino de forma inesperada. Pongase en contacto con el Administrador");
					   }else{
						   $scope.controlError(data.resultados.timeout);
					   }
					   $.unblockUI();
				   } else {
					   for(var i = 0; i<$scope.filtro.listaFiltrosDinamicos.length; i++){
						   for(var j = 0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++){
							   if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampo === "01/01/1900") {
								   $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo = "";
							   }
							   if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta === "31/12/9999") {
								   $scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta = "";
							   }
						   }
					   }

					   //Si el limite de registros por pantalla se ha superado y el de excel no, no mostramos el grid y permitimos la descarga de ambas excels
					   if ($scope.limiteRegistrosPantalla < data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel >= data.resultados.informe.resultados.length) {
						   $scope.mostrardescargarexcel = true;
						   $scope.mostrardescargarexcelSimple = false;
						   var decodedString = atob([ data.resultados.fileSimpleXLS ]);
						   $scope.descargarExcel(decodedString);
						   $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se muestren en pantalla. Limite maximo establecido " + $scope.limiteRegistrosPantalla);
						   $.unblockUI();
						   //Si el limite de registros por pantalla se ha superado y el de excel tambien, no mostramos el grid ni permitimos la descarga de ambas excels
					   } else if ($scope.limiteRegistrosPantalla < data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
						   $scope.mostrardescargarexcel = false;
						   $scope.mostrardescargarexcelSimple = false;
						   $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se muestren en pantalla ni la generacion de la excel. Limite maximo establecido por excel: " + $scope.limiteRegistrosExcel + " . Limite maximo establecido por pantalla: " + $scope.limiteRegistrosPantalla);
						   $.unblockUI();
						   //Si el limite de registros por pantalla no se ha superado y el de excel si, mostramos el grid pero no permitimos la descarga del excel
					   } else if ($scope.limiteRegistrosPantalla >= data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
						   $scope.mostrardescargarexcel = false;
						   $scope.mostrardescargarexcelSimple = false;
						   $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se genere la excel. Limite maximo establecido " + $scope.limiteRegistrosExcel);
						   $scope.tratarDatos(data, false, false);
						   //Si el limite de registros por pantalla no se ha superado y el de excel tampoco, mostramos el grid y permitimos la descarga de excel
					   } else {
						   $scope.mostrardescargarexcel = true;
						   $scope.mostrardescargarexcelSimple = false;
						   $scope.tratarDatos(data, false, true);
					   }
				   }
			   }else{
				   $scope.controlError("El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
				   $.unblockUI();
			   }
		   }, function (error) {
			   $.unblockUI();
			   $scope.controlError("Error en la peticion de datos. Pongase en contacto con el Administrador");
		   }, $scope.filtro);
	   }
   };

   // Hace la llamada a la generación de informe en excel para terceros
   $scope.generarInformeExcelTerceros = function () {
	   if ($scope.validacion()) {
		   if ($scope.filtro.ocultarEspeciales) {
			   $scope.filtro.especiales="";
		   }
		   $scope.filtro.userName = $rootScope.userName;
		   GenerarInformesNEWService.generarInformeExcelTerceros(function (data) {

			   if(data){
				   if (undefined != data.resultados && (data.resultados.status === "KO" || undefined != data.resultados.timeout)) {
					   if(data.resultados.status === "KO"){
						   $scope.controlError("El informe termino de forma inesperada. Pongase en contacto con el Administrador");
					   }else{
						   $scope.controlError(data.resultados.timeout);
					   }
					   $.unblockUI();
				   } else {
					   for(var i = 0; i<$scope.filtro.listaFiltrosDinamicos.length; i++){
						   for(var j = 0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++){
							   if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampo === "01/01/1900") {
								   $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo = "";
							   }
							   if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta === "31/12/9999") {
								   $scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta = "";
							   }
						   }
					   }

					   //Si el limite de registros por pantalla se ha superado y el de excel no, no mostramos el grid y permitimos la descarga de ambas excels
					   if ($scope.limiteRegistrosPantalla < data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel >= data.resultados.informe.resultados.length) {
						   $scope.mostrardescargarexcel = false;
						   $scope.mostrardescargarexcelSimple = true;
						   var decodedString = atob([ data.resultados.file ]);
						   $scope.descargarExcel(decodedString);
						   $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se muestren en pantalla. Limite maximo establecido " + $scope.limiteRegistrosPantalla);
						   $.unblockUI();
						   //Si el limite de registros por pantalla se ha superado y el de excel tambien, no mostramos el grid ni permitimos la descarga de ambas excels
					   } else if ($scope.limiteRegistrosPantalla < data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
						   $scope.mostrardescargarexcel = false;
						   $scope.mostrardescargarexcelSimple = false;
						   $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se muestren en pantalla ni la generacion de la excel. Limite maximo establecido por excel: " + $scope.limiteRegistrosExcel + " . Limite maximo establecido por pantalla: " + $scope.limiteRegistrosPantalla);
						   $.unblockUI();
						   //Si el limite de registros por pantalla no se ha superado y el de excel si, mostramos el grid pero no permitimos la descarga del excel
					   } else if ($scope.limiteRegistrosPantalla >= data.resultados.informe.resultados.length && $scope.limiteRegistrosExcel < data.resultados.informe.resultados.length) {
						   $scope.mostrardescargarexcel = false;
						   $scope.mostrardescargarexcelSimple = false;
						   $scope.controlError("El volumen de registros " + data.resultados.informe.resultados.length + " recuperados no permite que se genere la excel. Limite maximo establecido " + $scope.limiteRegistrosExcel);
						   $scope.tratarDatos(data, true, false);
						   //Si el limite de registros por pantalla no se ha superado y el de excel tampoco, mostramos el grid y permitimos la descarga de excel
					   } else {
						   $scope.mostrardescargarexcel = false;
						   $scope.mostrardescargarexcelSimple = true;
						   $scope.tratarDatos(data, true, false);
					   }
				   }
			   }else{
				   $scope.controlError("El timeout del servidor cortó la petición. El resultado del informe le llegará por email en formato Excel.");
				   $.unblockUI();
			   }
		   }, function (error) {
			   $.unblockUI();
			   $scope.controlError("Error en la peticion de datos. Pongase en contacto con el Administrador");

		   }, $scope.filtro);
	   }
   };

   // Hace la llamada a la generación de informe en excel para terceros pasansole los datos del datatb
   $scope.generarInformeExcelTercerosDatatable = function () {

     // Si el fichero tiene un valor, se descarga.
     if ($scope.file != "" && $scope.file !== undefined) {
       // Se muestra la capa cargando
       inicializarLoading();

       // se obtiene el contenido en excel y se descarga
       var decodedString = atob([ $scope.file ]);
       $scope.descargarExcel(decodedString);

       // Se desbloquea la página
       $.unblockUI();

     } else {
       $scope.generarInformeExcelTerceros();

       $scope.showingFilter = true;
       // Si es menor y el fichero esta vacio, no se ha generado en la consulta hay que generarlo.
       $scope.mostrardescargarexcel = true;
       $scope.mostrardescargarexcelSimple = true;

     }

   }

   // Hace la llamada a la generación de informe en excel para terceros pasansole los datos del datatb
   $scope.generarInformeExcelSimpleDatatable = function () {

     // Si el fichero tiene un valor, se descarga.
     if ($scope.fileSimpleXLS != "" && $scope.fileSimpleXLS !== undefined) {
       // Se muestra la capa cargando
       inicializarLoading();
       // se obtiene el contenido en excel y se descarga
       var decodedString = atob([ $scope.fileSimpleXLS ]);
       $scope.descargarExcel(decodedString);

       // Se desbloquea la página
       $.unblockUI();

     } else {
       $scope.generarInformeExcel();
       // Si es menor y el fichero esta vacio, no se ha generado en la consulta hay que generarlo.
       $scope.mostrardescargarexcel = true;
       $scope.mostrardescargarexcelSimple = true;
       // Oculto la busqueda
       $scope.showingFilter = true;
     }
   }
   
   /**
    *	Cuando un usuario lance un informe, se debe verificar en la tabla TMCT0_INFORMES que el campo GEN_FIN 
    *	es distinto de nulo, si es nulo, se debe dar un warning al usuario  
    */
   $scope.confirmaGeneracion = function(mensaje) {
	   $.unblockUI();
	   angular.element("#dialog-confirm").html(mensaje);
	   angular.element("#dialog-confirm").dialog({
		   resizable : true,
		   modal : true,
		   title : "Mensaje de Confirmación",
		   height : 215,
		   width : 360,
		   buttons : {
			   " Sí " : function () {
				   $scope.generarInforme();
				   $(this).dialog('close');
			   }," No " : function () {
				   $(this).dialog('close');
			   }
		   }
	   });
	   $('.ui-dialog-titlebar-close').remove();
   };
   
   
   $scope.cargarAutocompletables = function(){
	   // Se inicializan datos de $scope.
	   camposDinamicosHelper.inicializarScope($scope);
	   for (var x = 0; x < $scope.filtro.listaFiltrosDinamicos.length; x++) {
		   for (var v = 0; v < $scope.filtro.listaFiltrosDinamicos[x].length; v++) {
			   if($scope.filtro.listaFiltrosDinamicos[x][v].tipo == CONSTANTES.TIPO_AUTORRELLENABLE){
				   $scope.pupulateAutocompleteDinamic('#' + $scope.filtro.listaFiltrosDinamicos[x][v].identificador, $scope.filtro.listaFiltrosDinamicos[x][v].listaSelectDTO, x, v);
			   }
			   if($scope.filtro.listaFiltrosDinamicos[x][v].subTipo == CONSTANTES.SUBTIPO_DESDE_HASTA){
				   $scope.filtro.listaFiltrosDinamicos[x][v].mostrarFechaDesde = true;
			   }
		   }
	   }
   }
   
   $scope.operarLista = function(valor, elemento) {
	   switch (valor) {
		   case 'vaciar':
			   elemento.listaSeleccSelectDTO = [];
			   break;
		   case 'subir':
			   var listLength = elemento.listaSeleccSelectDTO.length;
			   for (var i = 0; i < listLength; i++) {
               if (elemento.listaSeleccSelectDTO[i].value === elemento.valorCampo.value && i > 0) {
            	   var elementList = elemento.listaSeleccSelectDTO[i];
                   elemento.listaSeleccSelectDTO[i] = elemento.listaSeleccSelectDTO[i - 1];
                   elemento.listaSeleccSelectDTO[i - 1] = elementList;
               }
           }
			   break;
		   case 'bajar':
           var listLength = elemento.listaSeleccSelectDTO.length;
           for (var i = listLength - 1; i >= 0; i--) {
             if (elemento.listaSeleccSelectDTO[i].value === elemento.valorCampo.value && i < (listLength - 1)) {
               var elementList = elemento.listaSeleccSelectDTO[i];
               elemento.listaSeleccSelectDTO[i] = elemento.listaSeleccSelectDTO[i + 1];
               elemento.listaSeleccSelectDTO[i + 1] = elementList;
             }
           }
			   break;
		   case 'eliminar':
           for (var i = 0; i < elemento.listaSeleccSelectDTO.length; i++) {
        	   if (elemento.listaSeleccSelectDTO[i].value === elemento.valorCampo.value) {
        		   elemento.listaSeleccSelectDTO.splice(i, 1);
        	   }
           }
			   break;
		   case 'agregar':
			   var option = {key : elemento.valor, value :elemento.valor};
           var existe = false;
           angular.forEach(elemento.listaSeleccSelectDTO, function (campo) {
             if (campo.value === elemento.valor) {
            	 existe = true;
             }
           });
           if (!existe) {
        	   elemento.listaSeleccSelectDTO.push(option);
           }
           elemento.valor = "";
			   break;
   			default:
   			   break;
   		}
	} 
   /**
    *	Precarga de datos del servicio. 
    */
   /** Inicio - Componente filtros dinamicos. */
   $scope.precargarFiltroDinamico = function($element) {
	   $scope.filtro.ocultarEspeciales = true;
	   for (var i=0;i<$scope.clasesRouting.length;i++) {
		   if ($scope.plantilla.clase === $scope.clasesRouting[i]) {
			   $scope.filtro.ocultarEspeciales = false;
			   break;
		   }
	   }
	   inicializarLoading();
	   var idPlantilla = $scope.plantilla != null ? $scope.plantilla.key : 0;
	   
	   /** Componente filtros dinamicos. */
	   DirectivasService.devolverElementosByParams(function (data) {
		   //Revisar chapuza
		   $scope.filtro.listaFiltrosDinamicos = data.resultados["datosFiltroInformeByIdInforme"];
		   DirectivasService.devolverElementosByParams(function (data) {
			   $scope.cargarAutocompletables();
			   $.unblockUI();
		   }, function (error) {
			   fErrorTxt("Se produjo un error al cargar ids de elementos.", 1);
		   }, {
			   "idInforme" : Number(idPlantilla)
		   });
		   
		  
	   }, function (error) {
		   fErrorTxt("Se produjo un error al cargar ids de elementos.", 1);
	   }, {
		   "idInforme" : Number(idPlantilla)
	   });
	   
	   
   }
   
//   $scope.mostrarFechaHasta = (datoFiltroInforme.subTipo === 'DESDE-HASTA');
   
   /****************************************
    *	Se procesa campo tipo FECHA dinamico *
    ****************************************/
   $scope.operarFecha = function(valor, elemento) {
	   camposDinamicosHelper.operarFecha($scope, valor, elemento);
   }
   /** Fin - Componente filtros dinamicos. */
   
   $scope.operarListaTexto = function(valor, elemento) {
	   camposDinamicosHelper.operarListaTexto($scope, valor, elemento);
   };
   
   $scope.limpiarFiltrosDinamicos = function(){
	   $scope.filtro.clase="";
	   $scope.plantilla="";
	   $scope.filtro.mercado="";
	    for(var i = 0; i<$scope.filtro.listaFiltrosDinamicos.length; i++){
	    	for(var j = 0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++){
	    		$scope.filtro.listaFiltrosDinamicos[i][j].listaSeleccSelectDTO = [];
	    		$scope.filtro.listaFiltrosDinamicos[i][j].valorCampo = "";
	    		$scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta = "";
	    		$scope.filtro.listaFiltrosDinamicos[i][j].lblBtnSentidoFiltro="+";
	    		$scope.filtro.listaFiltrosDinamicos[i][j].lblBtnSentidoFiltroHasta="+";
	    	}
	    }
	    $scope.cargaPlantillas();
  };
   
  $scope.cargarPlantillasMercadosFiltro = function (consultarMercados) {
	  GenerarInformesNEWService.cargaPlantillasFiltro(function (data) {
		  $scope.listPlantillas = data.resultados["listaPlantillasFiltro"];
		  if (consultarMercados) {
			  $scope.listMercados = data.resultados["listaMercados"];
		  }
	  }, function (error) {
		  fErrorTxt("Se produjo un error en la carga del combo de plantillas.", 1);
	  }, {
		  "clase" : $scope.filtro.clase,
		  "mercado" : $scope.filtro.mercado
	  });
  }; 
  
  $scope.validarNumeroDias = function () {
	   if ($scope.plantilla.fechaBase!="") {
		   var campo = "";
//		   var alias = "";
		   var campoFecha = "";
//		   var aliasFecha = "";
		   if (null!=$scope.filtro.listaFiltrosDinamicos && $scope.filtro.listaFiltrosDinamicos.length>0) {
			   for (var i=0; i<$scope.filtro.listaFiltrosDinamicos.length; i++) {
				   for (var j=0; j<$scope.filtro.listaFiltrosDinamicos[i].length; j++) {
					   if ($scope.filtro.listaFiltrosDinamicos[i][j].tipo == CONSTANTES.TIPO_FECHA) {
						   // Si solo tiene un elemento sin delimitador '/' se toma el 1er elemento.
						   if ($scope.filtro.listaFiltrosDinamicos[i][j].campo.indexOf('/') != -1) {
							   campo = $scope.filtro.listaFiltrosDinamicos[i][j].campo.split('/', -1);
							   campoFecha = campo[$scope.plantilla.posicion];
						   } else {
							   campo = $scope.filtro.listaFiltrosDinamicos[i][j].campo;
							   campoFecha = campo;
						   }
						   if (campoFecha === $scope.plantilla.fechaBase) {
							   $scope.errorValidarFechaBase = true;
							   if ((null!=$scope.filtro.listaFiltrosDinamicos[i][j].valorCampo && $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo !="") || 
									   (null!=$scope.filtro.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde && $scope.filtro.listaFiltrosDinamicos[i][j].valorNumeroFechaDesde !="")) {
								   // Si se cumple condicion de fecha base se toma como valida.
								   var firstDateSplit = $scope.filtro.listaFiltrosDinamicos[i][j].valorCampo.split('/');
								   var secondDateSplit = $scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta.split('/');
								   
								   var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
								   var firstDate = new Date(firstDateSplit[2],firstDateSplit[1]-1,firstDateSplit[0]);
								   if ($scope.filtro.listaFiltrosDinamicos[i][j].valorCampoHasta == "31/12/9999") {
									   var secondDate = new Date();
								   } else {
									   var secondDate = new Date(secondDateSplit[2],secondDateSplit[1]-1,secondDateSplit[0]); 
								   }
								   var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
								   
								   if (diffDays>=$scope.limiteDias) {
									   $scope.confirmaGeneracionNumeroDias("Va a solicitar un informe de " + diffDays + " días de información, ¿desea continuar?");
								   } else {
									   $scope.controlDias = true;
								   }
								   break;
							   }
						   }
					   }
				   }
			   }
		   }
	   } else {
		   $scope.controlDias = true; 
	   }
  };
  
  $scope.confirmaGeneracionNumeroDias = function(mensaje) {
	   $.unblockUI();
	   angular.element("#dialog-confirm").html(mensaje);
	   angular.element("#dialog-confirm").dialog({
		   resizable : true,
		   modal : true,
		   title : "Mensaje de Confirmación",
		   height : 160,
		   width : 360,
		   buttons : {
			   " Sí " : function () {
				   $scope.controlDias = true;
				   $scope.generarInforme();
				   $(this).dialog('close');
			   }," No " : function () {
				   $scope.controlDias = false;
				   $(this).dialog('close');
			   }
		   }
	   });
	   $('.ui-dialog-titlebar-close').remove();
  };
  
 } ]);