'use strict';
sibbac20
    .controller(
                'PeriodicidadController',
                [
                 '$scope',
                 '$rootScope',
                 '$compile',
                 'growl',
                 'PeriodicidadService',
                 '$document',
                 'ngDialog',
                 '$location',
                 'SecurityService',
                 function ($scope, $rootScope, $compile, growl, PeriodicidadService, $document, ngDialog, $location,
                           SecurityService) {

                   var oTable = undefined;
                   var tablasParams = [];
                   var SHOW_LOG = false;
                   $scope.mesesArray = [];
                   // pop-up
                   $scope.dialogTitle = "";
                   $scope.needRefresh = false;

                   // alta regla
                   $scope.textCodigo = "";
                   $scope.textDesc = "";
                   $scope.fechaDesde = "";
                   $scope.fechaHasta = "";
                   $scope.chkActiv = false;

                   // alta parametrizacion
                   $scope.textIdRegla = "";
                   $scope.fDesdeParam = "";
                   $scope.fHastaParam = "";
                   $scope.labAnterior = false;
                   $scope.labPosterior = false;
                   $scope.exFestivo = false;
                   $scope.margenAnterior = "";
                   $scope.margenPosterior = "";
                   $scope.horaParam = "";
                   $scope.minParam = "";
                   $scope.activaParam = false;
                   $scope.diaMes = "";
                   $scope.idMes = "";
                   $scope.diaSemana = "";

                   // modificar regla
                   $scope.textIdReg = "";
                   $scope.idReglaFormulario = "";
                   $scope.descReglaFormulario = "";
                   $scope.fIniReglaFormulario = "";
                   $scope.fFinReglaFormulario = "";
                   $scope.activaReglaFormulario = false;

                   // modificar parametrizacion
                   $scope.editParam = {
                     textIdRegla1 : "",
                     horaParam1 : "",
                     minParam1 : "",
                     diaMes1 : "",
                     idMes1 : "",
                     diaSemana1 : "",
                     margenPosterior1 : "",
                     margenAnterior1 : "",
                     labPosterior1 : false,
                     labAnterior1 : false,
                     exFestivo1 : false,
                     fDesdeParam1 : "",
                     textIdParam1 : "",
                     fHastaParam1 : "",
                     activoParam1 : false,
                   };

                   var calcDataTableHeight = function (sY) {
                     console.log("calcDataTableHeight: " + $('#tblPeriocidad').height());
                     console.log("sY: " + sY);
                     var numeroRegistros = infoTabla(oTable.api().page.info());
                     var tamanno = 50 * numeroRegistros;
                     if ($('#tblPeriocidad').height() > 510) {
                       return 480;
                     } else {
                       if ($('#tblPeriocidad').height() > tamanno) {
                         return $('#tblPeriocidad').height() + 30;
                       } else {
                         if (tamanno > 510) {
                           return 480;
                         } else {
                           return tamanno;
                         }
                       }
                     }
                   };

                   function ajustarAltoTabla () {
                     console.log("ajustando inicio");
                     var oSettings = oTable.fnSettings();
                     console.log("ajustando 1");
                     var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
                     console.log("cuanto: " + scrill);
                     oSettings.oScroll.sY = scrill;
                     console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
                     $('.dataTables_scrollBody').height(scrill);
                     console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
                     console.log("ajustando 2");
                     // oTable.fnDraw();

                     // if ($('#tablaEntregaRecepcion').height() > 480) {
                     // $('.dataTables_scrollBody').height( 480 );
                     // } else {
                     // $('.dataTables_scrollBody').height( $('#tablaEntregaRecepcion').height() + 20 );
                     // }
                     console.log("ajustando fin");
                   }

                   function initializaTable () {
                     oTable = $("#tblPeriocidad").dataTable({
                       "dom" : 'T<"clear">lfrtip',
                       "tableTools" : {
                         "sSwfPath" : "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
                       },
                       "language" : {
                         "url" : "i18n/Spanish.json"
                       },
                       /* Disable initial sort */
                       "order" : [],
                       /* "bSort" : false, Deshabilita todos los sort */
                       "columns" : [ {
                         "width" : "auto",
                         sClass : "taleft"
                       }, {
                         "width" : "auto",
                         sClass : "taleft"
                       }, {
                         "width" : "auto",
                         sClass : "taleft"
                       }, {
                         "width" : "auto",
                         sClass : "taleft"
                       }, {
                         "width" : "auto",
                         sClass : "taleft"
                       }, {
                         "width" : "auto",
                         sClass : "taleft"
                       }, {
                         "width" : "auto",
                         sClass : "taleft"
                       } ],
                       "fnCreatedRow" : function (nRow, aData, iDataIndex) {
                         $compile(nRow)($scope);
                       },
                       "scrollY" : "480px",
                       "scrollCollapse" : true,
                       "filter" : false,
                       drawCallback : function () {

                         ajustarAltoTabla();
                       }

                     });
                   }

                   // Devuelve información de la tabla-.
                   function infoTabla (info) {
                     var regTotales = info.recordsTotal;
                     var paginas = info.pages;
                     var regPagina = info.length;
                     var regInicio = info.start;
                     var regFin = info.end;
                     return regFin - regInicio;
                   }

                   $scope.visibilidad = [];

                   function getVisibilidad (idVisibilidad, tablA) {
                     var visible = "none";
                     var encontrado = false;
                     angular.forEach($scope.visibilidad, function (val, key) {
                       if (idVisibilidad == val.id) {
                         encontrado = true;
                         visible = val.visible;
                         val.visible = 'none';
                         if (visible === 'none') {
                           val.visible = 'block';
                         }

                         return false;
                       }
                     });
                     if (!encontrado) {
                       $scope.visibilidad.push({
                         id : idVisibilidad,
                         visible : "block"
                       });
                       tablA.find('tbody tr').each(function () {
                         $compile(this)($scope);
                       });

                     }
                     return visible;
                   }
                   ;

                   // funcion para desplegar la tabla de parametrizacion
                   $scope.desplegar = function (tabla_a_desplegar, e1, estadoTfila, row) {
                     $(oTable.fnGetNodes(row)).after(tablasParams[row]);
                     var tablA = angular.element('#' + tabla_a_desplegar);
                     var estadOt = angular.element('#' + e1);
                     var fila = angular.element('#' + estadoTfila);
                     console.log(angular.element(tablA).css('display'));
                     var visibilidad = getVisibilidad(row, tablA);
                     if (visibilidad === 'none') {
                       angular.element(tablA).show();
                       console.log('foco en tabla ' + tabla_a_desplegar);
                       ajustarAltoTabla();
                       document.getElementById(tabla_a_desplegar).scrollIntoView();
                     } else {
                       angular.element(tablA).hide();
                     }
                     tablasParams[row] = tablA;
                   }

                   function successUpdateActivoRegla (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("actualizado el estado de la regla...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.cargarTabla();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   $scope.cambioActivoRegla = function (id, activoRegla) {
                     var activo = true;
                     if (activoRegla == 'true') {
                       activo = false;
                     } else {
                       activo = true;
                     }
                     var filtro = {
                       id : id,
                       activo : activo
                     };

                     angular.element("#dialog-confirm").html("¿Desea cambiar el estado de la regla?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm").dialog({
                       resizable : false,
                       modal : true,
                       title : "Mensaje de Confirmación",
                       height : 120,
                       width : 360,
                       buttons : {
                         "Sí" : function () {
                           $(this).dialog('close');
                           PeriodicidadService.updateActivoRegla(successUpdateActivoRegla, showError, filtro);
                         },
                         "No" : function () {
                           $(this).dialog('close');
                         }
                       }
                     });

                   };

                   function successUpdateActivoParametrizacion (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("actualizado el estado de la parametrizacion...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.cargarTabla();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("actualizado el estado de la parametrizacion ...");
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   $scope.cambioActivoParametrizacion = function (id, activoParam) {
                     var activo = true;
                     if (activoParam == true) {
                       activo = false;
                     }

                     var filtro = {
                       id : id,
                       activo : activo
                     };

                     angular.element("#dialog-confirm").html("¿Desea cambiar el estado de la parametrización?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm")
                         .dialog(
                                 {
                                   resizable : false,
                                   modal : true,
                                   title : "Mensaje de Confirmación",
                                   height : 120,
                                   width : 360,
                                   buttons : {
                                     "Sí" : function () {
                                       $(this).dialog('close');
                                       PeriodicidadService
                                           .updateActivoParametrizacion(successUpdateActivoParametrizacion, showError,
                                                                        filtro);
                                     },
                                     "No" : function () {
                                       $(this).dialog('close');
                                     }
                                   }
                                 });

                   };

                   function successDeleteRegla (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("borrar regla ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.cargarTabla();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error borrando regla ...");
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   $scope.borrarRegla = function (id) {
                     var filtro = {
                       idReglaUsuario : id
                     };

                     angular.element("#dialog-confirm").html("¿Desea borrar realmente la regla?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm").dialog({
                       resizable : false,
                       modal : true,
                       title : "Mensaje de Confirmación",
                       height : 120,
                       width : 360,
                       buttons : {
                         "Sí" : function () {
                           $(this).dialog('close');
                           PeriodicidadService.deleteRegla(successDeleteRegla, showError, filtro);
                         },
                         "No" : function () {
                           $(this).dialog('close');
                         }
                       }
                     });

                   }

                   function successGetDiasSemana (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("cargando dias de la semana ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         var resultados = json.resultados.semanas;
                         var item = null;
                         var contador = 0;
                         var nParametrizaciones = 0;
                         var diaSemana = $scope.diaSemana;
                         angular.element('#diaSemana').empty();
                         angular.element('#diaSemana').append("<option value='0'>Seleccione un d&iacute;a</option>");
                         for ( var k in resultados) {
                           item = resultados[k];
                           if (diaSemana === item.id) {
                             angular.element("#diaSemana").append(
                                                                  "<option value='" + item.id + "' selected>"
                                                                      + item.nombre + "</option>");
                           } else {
                             angular.element("#diaSemana").append(
                                                                  "<option value='" + item.id + "'>" + item.nombre
                                                                      + "</option>");
                           }

                         }
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error dias de la semana ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function successGetMeses (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("cargando meses ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         var resultados = json.resultados.meses;
                         var item = null;
                         angular.element("#idMes").empty();
                         angular.element("#idMes").append("<option value='0'>Seleccione mes</option>");
                         $scope.mesesList = [];
                         var mes = $scope.idMes;
                         for ( var k in resultados) {
                           var entidadMeses = new EntidadMeses();
                           item = resultados[k];
                           entidadMeses.idMes = item.id;
                           entidadMeses.textoMes = item.nombre;
                           $scope.mesesArray.push(entidadMeses);

                           if (mes == item.id) {
                             angular.element("#idMes").append(
                                                              "<option value='" + item.id + "' selected>" + item.nombre
                                                                  + "</option>");
                           } else {
                             angular.element("#idMes").append(
                                                              "<option value='" + item.id + "'>" + item.nombre
                                                                  + "</option>");
                           }
                         }
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error meses ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function openAltaParametrizacionForm () {
                     ngDialog.open({
                       template : 'template/configuracion/periodicidad/altaParametrizacion.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           $scope.needRefresh = false;
                           $scope.cargarTabla();
                           limpiarParametrizacion();
                         }
                       }
                     });
                   }

                   $scope.cargoIdRegla = function (id) {
                     $scope.dialogTitle = 'Alta Parametrización';
                     $scope.textIdRegla = id;
                     openAltaParametrizacionForm();
                     PeriodicidadService.getDiasSemana(successGetDiasSemana, showError);
                     PeriodicidadService.getMeses(successGetMeses, showError);

                   }

                   function openEditReglaForm () {
                     ngDialog.open({
                       template : 'template/configuracion/periodicidad/editRegla.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-240',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           $scope.needRefresh = false;
                           $scope.cargarTabla();
                           limpiarModificarRegla();
                         }
                       }
                     });
                   }

                   $scope.cargoInputsReglas = function (id, idRegla, descr, fInicio, fFin, activa) {
                     $scope.dialogTitle = 'Editar Regla';
                     $scope.textIdReg = id;
                     $scope.idReglaFormulario = idRegla;
                     $scope.descReglaFormulario = descr;
                     $scope.fIniReglaFormulario = fInicio;
                     $scope.fFinReglaFormulario = fFin;
                     $scope.activaReglaFormulario = (activa === 'true');
                     openEditReglaForm();
                   }

                   function successGetDiasSemana1 (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("cargando dias de la semana ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         var resultados = json.resultados.semanas;
                         var item = null;
                         var contador = 0;
                         var nParametrizaciones = 0;
                         var diaSemana = $scope.editParam.diaSemana1;
                         angular.element('#diaSemana1').empty();
                         angular.element('#diaSemana1').append("<option value='0'>Seleccione un d&iacute;a</option>");
                         for ( var k in resultados) {
                           item = resultados[k];
                           if (diaSemana === item.id) {
                             angular.element("#diaSemana1").append(
                                                                   "<option value='" + item.id + "' selected>"
                                                                       + item.nombre + "</option>");
                           } else {
                             angular.element("#diaSemana1").append(
                                                                   "<option value='" + item.id + "'>" + item.nombre
                                                                       + "</option>");
                           }

                         }
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error dias de la semana ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function successGetMeses1 (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("cargando meses ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         var resultados = json.resultados.meses;
                         var item = null;
                         angular.element("#idMes1").empty();
                         angular.element("#idMes1").append("<option value='0'>Seleccione mes</option>");
                         $scope.mesesArray = [];
                         var mes = $scope.editParam.idMes1;
                         for ( var k in resultados) {
                           var entidadMeses = new EntidadMeses();
                           item = resultados[k];
                           entidadMeses.idMes = item.id;
                           entidadMeses.textoMes = item.nombre;
                           $scope.mesesArray.push(entidadMeses);

                           if (mes == item.id) {
                             angular.element("#idMes1").append(
                                                               "<option value='" + item.id + "' selected>"
                                                                   + item.nombre + "</option>");
                           } else {
                             angular.element("#idMes1").append(
                                                               "<option value='" + item.id + "'>" + item.nombre
                                                                   + "</option>");
                           }
                         }
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error meses ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function openEditParametrizacionForm () {
                     $scope.editDialog = ngDialog.open({
                       template : 'template/configuracion/periodicidad/editParametrizacion.html',
                       className : 'ngdialog-theme-plain custom-width custom-height-480',
                       scope : $scope,
                       preCloseCallback : function () {
                         if ($scope.needRefresh) {
                           $scope.needRefresh = false;
                           $scope.cargarTabla();
                           limpiarParametrizacion1();
                         }
                       }
                     });
                   }

                   $scope.cargoInputs = function (id, idRegla, fDesde, fHasta, excep, labAnt, labPos, diaMes, mes,
                                                  diaSemana, hora, min, mAnt, mPos, activo) {

                     $scope.dialogTitle = 'Editar Parametrización de la regla';

                     var excepcionar = (excep == "true");
                     var laborablePosterior = labPos == "true";
                     var laborableAnterior = labAnt == "true";
                     var activoAux = activo == 'true';

                     $scope.editParam.textIdRegla1 = idRegla;
                     $scope.editParam.horaParam1 = hora;
                     $scope.editParam.minParam1 = min;
                     $scope.editParam.diaMes1 = !diaMes ? null : diaMes;
                     $scope.editParam.idMes1 = !mes ? null : mes;
                     $scope.editParam.diaSemana1 = !diaSemana ? null : diaSemana;
                     $scope.editParam.margenPosterior1 = mPos;
                     $scope.editParam.margenAnterior1 = mAnt;
                     $scope.editParam.labPosterior1 = laborablePosterior;
                     $scope.editParam.labAnterior1 = laborableAnterior;
                     $scope.editParam.exFestivo1 = excepcionar;
                     $scope.editParam.fDesdeParam1 = fDesde;
                     $scope.editParam.fHastaParam1 = fHasta;
                     $scope.editParam.textIdParam1 = id;
                     $scope.editParam.activaParam1 = activoAux;

                     // llamada a ajax
                     PeriodicidadService.getDiasSemana(successGetDiasSemana1, showError);
                     PeriodicidadService.getMeses(successGetMeses1, showError);

                     openEditParametrizacionForm();

                   }

                   function successCargarTabla (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("Listando de reglas...");
                     var datos = {};
                     var imagenactivo;
                     var titleactivo;
                     var estilo;
                     var estiloParam;
                     var imagenactivoParam;
                     var titleactivoParam;
                     var excepcionar;
                     var laborablePosterior;
                     var laborableAnterior;
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         var resultados = json.resultados;
                         var item = null;
                         var contador = 0;
                         var nParametrizaciones = 0;
                         oTable.fnClearTable();
                         var rownum = 0;
                         tablasParams = [];
                         if (resultados.length > 1) {
                           controlarResultados = false;
                         }
                         for ( var k in resultados) {

                           // En "k" tengo lo de la izquierda, por ejemplo: "Id_Regla_1".
                           // En "item" tengo todo lo de la derecha.
                           // En "item.parametrizacion.length" tenemos el numero de "hijos".
                           item = resultados[k];
                           nParametrizaciones = item.parametrizacion.length;

                           if (item.activo == "true") {
                             imagenactivo = "activo";
                             titleactivo = "Desactivar";
                           } else {
                             imagenactivo = "desactivo";
                             titleactivo = "Activar";
                           }
                           if (contador % 2 == 0) {
                             estilo = "even";
                           } else {
                             estilo = "odd";
                           }

                           var enlace = "";
                           if (nParametrizaciones > 0) {
                             // Tenemos "nParametrizaciones" parametrizaciones.

                             enlace = "ng-click=\"desplegar(\'tabla_a_desplegar" + contador + "\',\'estadoT" + contador
                                      + "\', \'estadoTfila" + contador + "\', " + contador + ")\" ";
                             enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";
                           }

                           // Pintamos la tabla "padre". La cabecera...
                           // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                           var modificarPeriodicidad = "";
                           var altaParametro = "";
                           var eliminarPeriodicidad = "";
                           var activarPeriodicidad = "";

                           if (SecurityService.inicializated) {
                             if (SecurityService
                                 .isPermisoAccion($rootScope.SecurityActions.MODIFICAR, $location.path())) {
                               modificarPeriodicidad = "<a class=\"btn\" ng-click= \"cargoInputsReglas('" + item.id
                                                       + "','" + item.idReglaUsuario + "','" + item.descripcion + "','"
                                                       + transformaFecha(item.fechaDesde) + "','"
                                                       + transformaFecha(item.fechaHasta) + "','" + item.activo
                                                       + "'); \"><img src='img/editp.png' title='Editar'/> </a>&nbsp;";
                             }
                             if (SecurityService.isPermisoAccion($rootScope.SecurityActions.ALTA_SUBLISTADO, $location
                                 .path())) {

                               altaParametro = "<div class='tacenter' ><a class=\"btn\" ng-click= \"cargoIdRegla('"
                                               + item.idReglaUsuario
                                               + "'); \"><img src='img/addParam.png'  title='AltaParametro' /></a></div> ";
                             }

                             if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR, $location.path())) {
                               eliminarPeriodicidad = "<a class=\"btn\" ng-click=\"borrarRegla('" + item.idReglaUsuario
                                                      + "');\"><img src='img/del.png'  title='Eliminar'/></a>";
                             }
                             if (SecurityService.isPermisoAccion($rootScope.SecurityActions.ACTIVAR_DESACTIVAR,
                                                                 $location.path())) {
                               activarPeriodicidad = "<div class='tacenter' ><a class=\"btn\" ng-click=\"cambioActivoRegla('"
                                                     + item.id
                                                     + "','"
                                                     + item.activo
                                                     + "')\"><img src='img/"
                                                     + imagenactivo + ".png'  title='" + titleactivo + "'/></a></div> ";
                             }
                           }

                           oTable.fnAddData([
                                             "<div class='taleft' " + enlace + " >" + item.idReglaUsuario + "</div> ",
                                             "<div class='taleft' >" + item.descripcion + "</div> ",
                                             "<div class='taleft' >" + transformaFecha(item.fechaDesde) + "</div> ",
                                             "<div class='taleft' >" + transformaFecha(item.fechaHasta) + "</div> ",
                                             activarPeriodicidad,
                                             altaParametro,
                                             "<div class='taleft' > " + modificarPeriodicidad + eliminarPeriodicidad
                                                 + " </div> " ]);
                           $(oTable.fnGetNodes(rownum)).addClass(estilo);

                           if (nParametrizaciones > 0) {
                             // Tenemos "nParametrizaciones" parametrizaciones. Pintamos cabecera.
                             var row = '';

                             row += "<tr id='tabla_a_desplegar"
                                    + contador
                                    + "' >"
                                    + "<td colspan='7' style='border-width:0 !important;background-color:#dfdfdf;'>"
                                    + "<table id='tablaParam"
                                    + contador
                                    + "' style=width:100%; margin-top:5px;'>"
                                    + "<tr>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Nombre <input type='hidden' id=visibleParam'"
                                    + contador
                                    + "' value='none' /></th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Fecha desde</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Fecha hasta</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Excepcionar Festivos</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Laborable Anterior</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Laborable Posterior</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Día Semana</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Mes</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Día Mes</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Hora</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Minuto</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Margen Anterior</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Margen Posterior</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Activa</th>"
                                    + "<th class='taleft' style='background-color: #000 !important;'>Acciones</th>"
                                    + "</tr>";
                             var parametrizacion = null;

                             // Pintamos el cuerpo.
                             var contParm = 0;
                             for ( var p in item.parametrizacion) {
                               var filaSubtabla = "";
                               parametrizacion = item.parametrizacion[p];
                               estiloParam = (contParm % 2 == 0) ? "even" : "odd";

                               imagenactivoParam = (parametrizacion.activoParam == "true") ? "activo" : "desactivo";
                               titleactivoParam = (parametrizacion.activoParam == "true") ? "Desactivar" : "Activar";
                               excepcionar = (parametrizacion.excepcionarFestivos == "true") ? "checked='checked'" : "";
                               laborablePosterior = (parametrizacion.laborablePosterior == "true") ? "checked='checked'"
                                                                                                  : "";
                               laborableAnterior = (parametrizacion.laborableAnterior == "true") ? "checked='checked'"
                                                                                                : "";

                               var diaSemana = parametrizacion.diaSemanaParamName;

                               if (!diaSemana) {
                                 diaSemana = "";
                               }

                               var mes = parametrizacion.mesParamName;

                               if (!mes) {
                                 mes = "";
                               }

                               var diaMes = parametrizacion.diaMesParam;

                               if (!diaMes) {
                                 diaMes = "";
                               }

                               // se controlan las acciones, se muestra el enlace cuando el usuario tiene el permiso.
                               var activarRegla = "";
                               var modificarParametrizacion = "";
                               var eliminarParametrizacion = "";

                               if (SecurityService.inicializated) {
                                 if (SecurityService.isPermisoAccion($rootScope.SecurityActions.ACTIVAR_DESACTIVAR,
                                                                     $location.path())) {
                                   activarRegla = "<a class=\"btn\" ng-click=\"cambioActivoParametrizacion('"
                                                  + parametrizacion.idParam + "'," + parametrizacion.activoParam
                                                  + ");\"><img src='img/" + imagenactivoParam + ".png'  title='"
                                                  + titleactivoParam + "'/></a>";
                                 }

                                 if (SecurityService.isPermisoAccion($rootScope.SecurityActions.EDITAR_SUBLISTADO,
                                                                     $location.path())) {
                                   modificarParametrizacion = "<a class=\"btn\" ng-click = \"cargoInputs('"
                                                              + parametrizacion.idParam
                                                              + "','"
                                                              + item.idReglaUsuario
                                                              + "','"
                                                              + transformaFecha(parametrizacion.fechaDesdeParam)
                                                              + "','"
                                                              + transformaFecha(parametrizacion.fechaHastaParam)
                                                              + "','"
                                                              + parametrizacion.excepcionarFestivos
                                                              + "','"
                                                              + parametrizacion.laborableAnterior
                                                              + "','"
                                                              + parametrizacion.laborablePosterior
                                                              + "','"
                                                              + diaMes
                                                              + "','"
                                                              + parametrizacion.mesParam
                                                              + "','"
                                                              + parametrizacion.diaSemanaParam
                                                              + "','"
                                                              + parametrizacion.hora
                                                              + "','"
                                                              + parametrizacion.minuto
                                                              + "','"
                                                              + parametrizacion.margenAnterior
                                                              + "','"
                                                              + parametrizacion.margenPosterior
                                                              + "','"
                                                              + parametrizacion.activoParam
                                                              + "' ); \"><img src='img/editp.png' title='Editar'/></a>&nbsp;";
                                 }
                                 if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR_SUBLISTADO,
                                                                     $location.path())) {
                                   eliminarParametrizacion = "      <a class=\"btn\" ng-click=eliminarParametrizacion("
                                                             + parametrizacion.idParametrizacion
                                                             + ");><img src='img/del.png'  title='Eliminar'/></a>";
                                 }

                               }

                               filaSubtabla += "<tr class='"
                                               + estiloParam
                                               + "' id='editDivR"
                                               + contador
                                               + "P"
                                               + contParm
                                               + "'>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + parametrizacion.idParam
                                               + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + transformaFecha(parametrizacion.fechaDesdeParam)
                                               + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + transformaFecha(parametrizacion.fechaHastaParam)
                                               + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'><input type='checkbox' class='checkTabla' "
                                               + excepcionar
                                               + " disabled/></td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'><input type='checkbox' class='checkTabla' "
                                               + laborableAnterior
                                               + "  disabled/></td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'><input type='checkbox' class='checkTabla' "
                                               + laborablePosterior + "  disabled/></td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + diaSemana + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + mes + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + diaMes + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + parametrizacion.hora + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + parametrizacion.minuto + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + parametrizacion.margenAnterior + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + parametrizacion.margenPosterior + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + activarRegla + "</td>"
                                               + "<td class='taleft' style='background-color: #f5ebc5 !important;'>"
                                               + modificarParametrizacion + eliminarParametrizacion + " </td>"
                                               + "</tr>";
                               row += filaSubtabla;
                               contParm++;

                             }
                           }

                           // Pintamos el pie.
                           row += "</table></td></tr>";

                           tablasParams.push(row);
                           contador++;
                           rownum++;
                         }

                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("Error cargando tabla ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                     $scope.visibilidad = [];
                   }
                   ;

                   function ocultarAlta () {
                     angular.element('.altaregla').hide();
                     // angular.element('.collapser_search').text("Ocultar Alta de Regla");
                     $scope.textCodigo = "";
                     $scope.textDesc = "";
                     $scope.fechaDesde = "";
                     $scope.fechaHasta = "";
                     $scope.chkActiv = "";
                     return false;
                   }
                   ;

                   $('.collapser_search').click(function () {

                     $(this).parents('.title_section').next().slideToggle();
                     $(this).parents('.title_section').next().next('.button_holder').slideToggle();
                     $(this).toggleClass('active');
                     // $(this).toggleText( "Mostrar Alta de Regla", "Ocultar Alta de Regla" );
                     if ($(this).text() == "Ocultar Alta de Regla") {
                       $(this).text("Mostrar Alta de Regla");
                     } else {
                       $(this).text("Ocultar Alta de Regla");
                     }
                     return false;
                   });

                   function showError (data, status, headers, config) {
                     $.unblockUI();
                     growl.addErrorMessage("Error: " + data);
                   }
                   ;

                   $scope.cargarTabla = function () {
                     console.log("entro en el método cargartabla nuevo");
                     PeriodicidadService.getReglasAndParams(successCargarTabla, showError);
                   };

                   $(document).ready(function () {
                     initializaTable();
                     $scope.cargarTabla();
                   });

                   /* Eva:Función para cambiar texto */
                   jQuery.fn.extend({
                     toggleText : function (a, b) {
                       var that = this;
                       if (that.text() != a && that.text() != b) {
                         that.text(a);
                       } else if (that.text() == a) {
                         that.text(b);
                       } else if (that.text() == b) {
                         that.text(a);
                       }
                       return this;
                     }
                   });
                   /* FIN Función para cambiar texto */

                   $('a[href="#release-history"]').toggle(function () {
                     $('#release-wrapper').animate({
                       marginTop : '0px'
                     }, 600, 'linear');
                   }, function () {
                     $('#release-wrapper').animate({
                       marginTop : '-' + ($('#release-wrapper').height() + 20) + 'px'
                     }, 600, 'linear');
                   });

                   $('#download a').mousedown(function () {
                     _gaq.push([ '_trackEvent', 'download-button', 'clicked' ])
                   });

                   function limpiarParametrizacion () {
                     $scope.textIdRegla = "";
                     $scope.fDesdeParam = "";
                     $scope.fHastaParam = "";
                     $scope.labAnterior = false;
                     $scope.labPosterior = false;
                     $scope.exFestivo = false;
                     $scope.margenAnterior = "";
                     $scope.margenPosterior = "";
                     $scope.horaParam = "";
                     $scope.minParam = "";
                     $scope.activaParam = false;
                     $scope.diaMes = "";
                     $scope.idMes = "";
                     $scope.diaSemana = "";
                   }

                   function successCrearParametrizacion (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("parametrizacion creada...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.needRefresh = true;
                         ngDialog.closeAll();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("Error parametrizacion creada ...");
                         growl.addErrorMessage(json.error);
                       }
                     }
                   }
                   ;

                   function crearParametrizacion () {
                     var idRegla = $scope.textIdRegla;
                     var fechaDesde = $scope.fDesdeParam;
                     var fechaHasta = $scope.fHastaParam;
                     var labAnt = $scope.labAnterior;
                     var labPost = $scope.labPosterior;
                     var exFestivo = $scope.exFestivo;
                     var margenAnt = $scope.margenAnterior;
                     var margenPost = $scope.margenPosterior;
                     var hora = $scope.horaParam;
                     var minuto = $scope.minParam;
                     var activa = $scope.activaParam;
                     var diaMes = $scope.diaMes;
                     var idMes = $scope.idMes;
                     var diaSemana = $scope.diaSemana;

                     var listaIdMes = angular.element('#idMes');
                     var valorSeleccionadoidMes = listaIdMes.val();

                     if (fechaHasta == "" || fechaHasta === undefined) {
                       fechaHasta = "31/12/9999";
                     }

                     if (hora == "") {
                       hora = "12";
                     }

                     if (minuto == "") {
                       minuto = "0";
                     }

                     if (margenAnt == "") {
                       margenAnt = "0";
                     }

                     if (margenPost == "") {
                       margenPost = "30";
                     }

                     var filtro = {
                       excepcionarFestivos : exFestivo,
                       laborableAnterior : labAnt,
                       laborablePosterior : labPost,
                       hora : hora,
                       minuto : minuto,
                       margenAnterior : margenAnt,
                       margenPosterior : margenPost,
                       fechaDesde : transformaFechaInv(fechaDesde),
                       fechaHasta : transformaFechaInv(fechaHasta),
                       activo : activa,
                       idReglaUsuario : idRegla
                     };

                     if (diaSemana != "0") {
                       filtro.diaSemana = diaSemana;
                     }

                     if (valorSeleccionadoidMes != "0") {
                       filtro.diaMes = diaMes;
                       filtro.idMes = valorSeleccionadoidMes;
                     } else {
                       if (diaSemana == "0" && valorSeleccionadoidMes == "0" && (diaMes == "0" || diaMes == "")) {
                         alert("Debe seleccionar un dia de la semana o un mes y su día");
                         PeriodicidadService.getDiasSemana(successGetDiasSemana, showError);
                         PeriodicidadService.getMeses(successGetMeses, showError);
                         return false;
                       }
                     }

                     PeriodicidadService.createParametrizacion(successCrearParametrizacion, showError, filtro);
                   }

                   function getTextoMes (idMes) {
                     var textoMes = "";
                     angular.forEach($scope.mesesArray, function (val, key) {

                       if (idMes == val.idMes) {
                         textoMes = val.textoMes;
                         return false;
                       }
                     });
                     return textoMes;
                   }
                   ;

                   $scope.altaParametrizacion = function () {

                     $scope.fDesdeParam = angular.element("#fDesdeParam").val();
                     $scope.fHastaParam = angular.element("#fHastaParam").val();
                     $scope.margenAnterior = angular.element("#margenAnterior").val();
                     if ($scope.margenAnterior == '') {
                       $scope.margenAnterior = 0;
                     }
                     $scope.margenPosterior = angular.element("#margenPosterior").val();
                     if ($scope.margenPosterior == '') {
                       $scope.margenPosterior = 30;
                     }
                     var hora = angular.element("#horaParam").val();
                     if (hora == '') {
                       hora = 12;
                     }
                     $scope.horaParam = hora;
                     var minuto = angular.element("#minParam").val();
                     if (minuto == '') {
                       minuto = '00';
                     }
                     $scope.minParam = minuto;
                     $scope.diaMes = angular.element("#diaMes").val();

                     var fechaDesde = $scope.fDesdeParam;
                     var fechaHasta = $scope.fHastaParam;
                     var margenAnt = $scope.margenAnterior;

                     var margenPost = $scope.margenPosterior;
                     var diaMes = $scope.diaMes;

                     var listaIdMes = angular.element('#idMes');
                     var valorSeleccionadoidMes = listaIdMes.val();
                     var textoSeleccionadoidMes = getTextoMes(valorSeleccionadoidMes);

                     // if mes = 0 { compruebo día y si lo hay error } else {
                     if ((valorSeleccionadoidMes == "0") && (diaMes != "")) {
                       alert("Debe seleccionar un mes");
                       return false;
                     }
                     if (!validaMesDia(valorSeleccionadoidMes, diaMes) && (valorSeleccionadoidMes != "0")) {
                       alert("El mes seleccionado no tiene " + diaMes + " dias");
                       return false;
                     }

                     if (hora > 24) {
                       alert("No existen mas de 24 horas");
                       return false;
                     }

                     if (minuto > 60) {
                       alert("No existen mas de 60 minutos");
                       return false;
                     } else {
                       $scope.labAnterior = angular.element("#labAnterior").prop("checked");
                       $scope.labPosterior = angular.element("#labPosterior").prop("checked");
                       $scope.exFestivo = angular.element("#exFestivo").prop("checked");
                       $scope.activaParam = angular.element("#activaParam").prop("checked");
                       $scope.idMes = angular.element("#idMes").val();
                       $scope.diaSemana = angular.element("#diaSemana").val();
                       crearParametrizacion();
                     }

                   };

                   $scope.cambioFestivo = function () {
                     $scope.exFestivo = angular.element("#exFestivo").prop("checked");
                     if ($scope.exFestivo === true) {
                       angular.element('#cambioExcepcionar').show();
                     } else {
                       angular.element('#cambioExcepcionar').hide();
                       $scope.labAnterior = false;
                       $scope.labPosterior = false;
                     }
                   };

                   $scope.cambioFestivo1 = function () {

                     if ($scope.editParam.exFestivo1 === true) {
                       angular.element('#cambioExcepcionar1').show();
                     } else {
                       angular.element('#cambioExcepcionar1').hide();
                       $scope.editParam.labAnterior1 = false;
                       $scope.editParam.labPosterior1 = false;
                     }
                   };

                   $scope.cambioLabAnterior = function () {

                     if ($scope.labAnterior === true) {
                       $scope.labPosterior = false;
                     }
                   };

                   $scope.cambioLabAnterior1 = function () {

                     if ($scope.editParam.labAnterior1 === true) {
                       $scope.editParam.labPosterior1 = false;
                     }
                   };

                   $scope.cambioLabPosterior = function () {

                     if ($scope.labPosterior === true) {
                       $scope.labAnterior = false;
                     }
                   };

                   $scope.cambioLabPosterior1 = function () {

                     if ($scope.editParam.labPosterior1 === true) {
                       $scope.editParam.labAnterior1 = false;
                     }
                   };

                   $scope.cambiaDiaSemana = function (param) {

                     // cojo el valor del id y si es distinto de cero cambio lo de mes
                     /*
                       * var lista = angular.element("#diaSemana"+param); var indiceSeleccionado = lista.selectedIndex;
                       * var opcionSeleccionada = lista.options[indiceSeleccionado]; var textoSeleccionado =
                       * opcionSeleccionada.text; var valorSeleccionado = opcionSeleccionada.value;
                       * if(valorSeleccionado!="0"){ angular.element("#diaMes"+param).val('');
                       * angular.element("#idMes"+param).selectedIndex="0"; }
                       */
                   }

                   $scope.cambioMes = function (param) {
                     /*
                       * var lista = angular.element("#idMes"+param); var indiceSeleccionado = lista.selectedIndex; var
                       * opcionSeleccionada = lista.options[indiceSeleccionado]; var textoSeleccionado =
                       * opcionSeleccionada.text; var valorSeleccionado = opcionSeleccionada.value;
                       * if(valorSeleccionado!="0"){ angular.element("#diaSemana"+param).selectedIndex="0"; }
                       */

                   }

                   function limpiarModificarRegla () {
                     $scope.textIdReg = "";
                     $scope.idReglaFormulario = "";
                     $scope.descReglaFormulario = "";
                     $scope.fIniReglaFormulario = "";
                     $scope.fFinReglaFormulario = "";
                     $scope.activaReglaFormulario = false;
                   }

                   function successModificarRegla (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("Regla modificada ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.needRefresh = true;
                         growl.addInfoMessage("La regla ha sido actualizada correctamente.");
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   $scope.modificarRegla = function () {
                     $scope.descReglaFormulario = angular.element("#descReglaFormulario").val();
                     $scope.fIniReglaFormulario = angular.element("#fIniReglaFormulario").val();
                     $scope.fFinReglaFormulario = angular.element("#fFinReglaFormulario").val();
                     $scope.activada = angular.element("#activaReglaFormulario").prop("checked");

                     var codigo = $scope.idReglaFormulario;
                     var descripcion = $scope.descReglaFormulario;
                     var fechaInicio = $scope.fIniReglaFormulario;
                     var fechaFin = $scope.fFinReglaFormulario;
                     var id = $scope.textIdReg;
                     var activada = $scope.activaReglaFormulario;

                     var filtro = {
                       id : id,
                       idReglaUsuario : codigo,
                       descripcion : descripcion,
                       fechaDesde : transformaFechaInv(fechaInicio),
                       fechaHasta : transformaFechaInv(fechaFin),
                       activo : activada
                     };
                     PeriodicidadService.updateRegla(successModificarRegla, showError, filtro);

                   };

                   function limpiarParametrizacion1 () {
                     $scope.editParam.textIdRegla1 = "";
                     $scope.editParam.textIdParam1 = "";
                     $scope.editParam.horaParam1 = "";
                     $scope.editParam.minParam1 = "";
                     $scope.editParam.diaMes1 = "";
                     $scope.editParam.idMes1 = "";
                     $scope.editParam.activaParam1 = false;
                     $scope.editParam.diaSemana1 = "";
                     $scope.editParam.margenPosterior1 = "";
                     $scope.editParam.margenAnterior1 = "";
                     $scope.editParam.labPosterior1 = false;
                     $scope.editParam.labAnterior1 = false;
                     $scope.editParam.exFestivo1 = false;
                     $scope.editParam.fDesdeParam1 = "";
                     $scope.editParam.fHastaParam1 = "";
                   }
                   ;

                   function successModParametrizacion (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("parametrizacion modificada ...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.needRefresh = true;
                         $scope.editDialog.close();
                         growl.addInfoMessage("La parametrización ha sido actualizada correctamente.");
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("Error modificando parametrizacion ...");
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   $scope.modParametrizacion = function () {


                     if (!$scope.editParam.horaParam1) {
                       $scope.editParam.horaParam1 = 12;
                     } else {
                       if ($scope.editParam.horaParam1 > 24) {
                         alert("No existen mas de 24 horas");
                         return false;
                       }
                     }

                     if (!$scope.editParam.minParam1) {
                       $scope.editParam.minParam1 = '00';
                     } else {
                       if ($scope.editParam.minParam1 > 60) {
                         alert("No existen mas de 60 minutos");
                         return false;
                       }
                     }

                     if (!$scope.editParam.margenPosterior1) {
                       $scope.editParam.margenPosterior1 = 30;
                     }

                     if (!$scope.editParam.margenAnterior1) {
                       $scope.editParam.margenPosterior1 = 0;
                     }

                     // var textoSeleccionadoidMes = getTextoMes(valorSeleccionadoidMes);

                     var filtro = {

                       id : $scope.editParam.textIdParam1,
                       excepcionarFestivos : $scope.editParam.exFestivo1,
                       laborableAnterior : $scope.editParam.labAnterior1,
                       laborablePosterior : $scope.editParam.labPosterior1,
                       hora : $scope.editParam.horaParam1,
                       minuto : $scope.editParam.minParam1,
                       margenAnterior : $scope.editParam.margenAnterior1,
                       margenPosterior : $scope.editParam.margenPosterior1,
                       fechaDesde : transformaFechaInv($scope.editParam.fDesdeParam1),
                       fechaHasta : transformaFechaInv($scope.editParam.fHastaParam1),
                       activo : $scope.editParam.activaParam1,
                       idReglaUsuario : $scope.editParam.textIdRegla1
                     };


                     if ($scope.editParam.diaSemana1 != "0") {
                       filtro.diaSemana = $scope.editParam.diaSemana1;
                     }

                     if ($scope.editParam.idMes1 != "0") {
                       filtro.diaMes = $scope.editParam.diaMes1;

                       filtro.idMes = $scope.editParam.idMes1;
                     } else {

                       if ($scope.editParam.diaSemana1 == "0" && $scope.editParam.idMes1 == "0"
                           && ($scope.editParam.diaMes1 == "0" || $scope.editParam.diaMes1 == "")) {
                         alert("Debe seleccionar un dia de la semana o un mes y su día");
                         PeriodicidadService.getDiasSemana(successGetDiasSemana1, showError);
                         PeriodicidadService.getMeses(successGetMeses1, showError);
                         return false;
                       }
                     }

                     PeriodicidadService.updateParametrizacion(successModParametrizacion, showError, filtro);

                   };

                   function successAltaRegla (json, status, headers, config) {

                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         if (SHOW_LOG)
                           console.log("Error dando la regla de alta ...");
                         growl.addErrorMessage(json.error);
                       } else {
                         if (SHOW_LOG)
                           console.log("regla dada de alta ...");
                         ocultarAlta();
                         $scope.cargarTabla();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("Error dando la regla de alta ...");
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   // Funcion para el alta de la regla
                   function altaRegla (codigo, descripcion, fechaInicio, fechaFin, activada) {
                     if (fechaFin === "" || fechaFin === undefined) {
                       fechaFin = "31/12/9999";
                     }
                     var filtro = {
                       idReglaUsuario : codigo,
                       descripcion : descripcion,
                       fechaDesde : transformaFechaInv(fechaInicio),
                       fechaHasta : transformaFechaInv(fechaFin),
                       activo : activada
                     };

                     PeriodicidadService.createRegla(successAltaRegla, showError, filtro);
                   }

                   $scope.alta = function () {

                     var codigo = $scope.textCodigo;
                     var descripcion = $scope.textDesc;
                     var fechaInicio = $scope.fechaDesde;
                     var fechaFin = $scope.fechaHasta;
                     var activada = $scope.chkActiv;

                     if (codigo == "" || descripcion == "" || fechaInicio == "") {
                       alert("Alguno de los campos es vacío. Debe rellenarlo para poder dar de alta");
                       return false;
                     }
                     if (!validarFecha('fechaDesde')) {
                       alert("La fecha introducida no tiene el formato correcto: dd/mm/yyyy");
                       return false;
                     } else {
                       altaRegla(codigo, descripcion, fechaInicio, fechaFin, activada);
                     }

                   };

                   function successDeleteParametrizacion (json, status, headers, config) {
                     if (SHOW_LOG)
                       console.log("borrando la parametrizacion de la regla...");
                     var datos = {};
                     if (json !== undefined && json.resultados !== undefined && json.resultados !== null) {
                       if (json.error !== null) {
                         growl.addErrorMessage(json.error);
                       } else {
                         $scope.cargarTabla();
                       }
                     } else {
                       if (json !== undefined && json.error !== null) {
                         if (SHOW_LOG)
                           console.log("error borrando la parametrizacion de la regla ...");
                         growl.addErrorMessage(json.error);
                       }
                     }

                   }
                   ;

                   $scope.borrarParam = function (id) {
                     var filtro = {
                       id : id
                     };

                     angular.element("#dialog-confirm").html("¿Desea borrar realmente la parametrización?");

                     // Define the Dialog and its properties.
                     angular.element("#dialog-confirm").dialog({
                       resizable : false,
                       modal : true,
                       title : "Mensaje de Confirmación",
                       height : 120,
                       width : 360,
                       buttons : {
                         "Sí" : function () {
                           $(this).dialog('close');
                           PeriodicidadService.deleteParametrizacion(successDeleteRegla, showError, filtro);
                         },
                         "No" : function () {
                           $(this).dialog('close');
                         }
                       }
                     });

                   };

                 } ]);
