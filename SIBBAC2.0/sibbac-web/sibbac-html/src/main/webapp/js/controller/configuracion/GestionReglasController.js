'use strict';
sibbac20.controller('GestionReglasController', ['$scope', '$rootScope', '$document', 'growl', 'GestionReglasService', 'IsinService', '$compile', 'SecurityService', '$location',
    function($scope, $rootScope, $document, growl, GestionReglasService, IsinService, $compile, SecurityService, $location) {
        var oTable = undefined;


        $scope.visibilidad = [];
        var tablasParams = [];
        var tablasAlias = [];
        var tablasSubCta = [];
        var activoListaParam = [];
        var activoListaAlias = [];
        var activoListaSubCta = [];

        $scope.listaModi = [];
        $scope.listaChk = [];
        $scope.listaId = [];
        var controlaLosCombos = true;
        var nParametrizaciones = 0;
        var carga = true;
        var imagenactivo;
        var titleactivo;

        var idOcultosAlias = [];
        var availableTagsAlias = [];
        var idOcultosSubCta = [];
        var availableTagsSubCta = [];

        var idOcultosCodigo = [];
        var availableTagsCodigo = [];

        var availableTagsSubCtaAsig = [];

        var posicion
        var lMarcar = true;
        var control = true;
        var creaModifica = true;
        var creaModificaParame = true;
        var contador = 0;

        // Esto se deberia borrar.
        var id;
        var idRegla;
        var cdCodigo;
        var descripcion;
        var fecha_creacion;
        var audit_usuario;
        var audit_fecha;
        // Hasta aqui.


        var hoy = new Date();
        var dd = hoy.getDate();
        var mm = hoy.getMonth() + 1;
        var yyyy = hoy.getFullYear();
        hoy = yyyy + "_" + mm + "_" + dd;

        $scope.followSearch = ""; // contiene las migas de los filtros de búsqueda
        var alPaginas = [];
        var pagina = 0;
        var paginasTotales = 0;
        var ultimaPagina = 0;

        // Mirar para que vale lo siguiente.
        $scope.idComp = 0;
        $scope.idCuentaComp = 0;
        $scope.idMiembroComp = 0;
        $scope.idNemotecnico = 0;
        $scope.idSegmento = 0;
        $scope.idCapacidad = 0;
        $scope.idModeloNegocio = 0;
        $scope.numero_fila = null;
        $scope.numero_fila_alias = null;
        $scope.numero_fila_subcta = null;
        $scope.filter = {
            codigoDescripcion: "",
            miembroCompesador: "",
            segmento: "",
            capacidad: "",
            camaraCompensacion: "",
            referenciaAsignacion: "",
            referenciaCliente: "",
            modeloNegocio: "",
            nemotecnico: "",
            referenciaExterna: "",
            estado: "1"

        }

        $scope.datosModi = {
            idRegla: "",
            cdCodigo: "",
            descripcion: "",
            fecha_creacion: "",
            audit_usuario: "",
            audit_fecha: ""
        }

        $scope.filtroaAsignacion = {};

        $scope.asignar = {
            idRegla: "",
            aliasAsig: "",
            subCtaAsig: ""
        }

        var IdCamaraCompensacion;

        $(document).ready(function() {

            initialize();

            oTable = $("#tablaGestionReglas").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                    "aButtons": ["copy",
                        {
                            "sExtends": "csv",
                            "sFileName": "Listado_gestión_de_reglas_" + hoy + ".csv"
                        },
                        {
                            "sExtends": "xls",
                            "sFileName": "Listado_gestión_de_reglas_" + hoy + ".xls"
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfOrientation": "landscape",
                            "sTitle": " ",
                            "sPdfSize": "A4",
                            "sPdfMessage": "Listado gestion de Reglas",
                            "sFileName": "Listado_gestión_de_reglas_" + hoy + ".pdf",
                            "mColumns": "visible"
                        },
                        "print"
                    ]
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },

                "aoColumns": [

                    {
                        sClass: "centrar",
                        bSortable: false,
                        width: 20
                    }, // Check
                    {
                        sClass: "centrar",
                        bSortable: false,
                        width: 20
                    }, // Alta
                    // Parametrizaciones
                    {
                        sClass: "align-left"
                    }, // Id
                    {
                        sClass: "align-left"
                    }, // Code
                    {
                        sClass: "align-left"
                    }, // Description
                    {
                        sClass: "align-left"
                    }, // Creation Date
                    {
                        sClass: "centrar",
                        bSortable: false,
                        width: 20
                    }, // regla activa
                    {
                        sClass: "centrar"
                    }, // Nº Parametrizaciones.
                    {
                        sClass: "centrar"
                    }, // Nº Alias
                    {
                        sClass: "centrar"
                    }, // Nº de Subcuentas
                    {
                        sClass: "centrar",
                        visible: false
                    }, // Código
                    {
                        sClass: "centrar",
                        visible: false
                    }, // idRegla
                    {
                        sClass: "centrar",
                        visible: false
                    }, // estado
                ],
                "fnCreatedRow": function(nRow, aData, iDataIndex) {

                    $compile(nRow)($scope);
                },
                "order": [],
                "scrollY": "480px",
                "scrollX": "100%",
                "scrollCollapse": true,

                drawCallback: function() {

                    $scope.visibilidad = [];
                    ajustarAltoTabla();
                    processInfo(this.api().page.info());
                    if (alPaginas.length !== paginasTotales) {
                        alPaginas = new Array(paginasTotales);
                        for (var i = 0; i < alPaginas.length; i++) {
                            alPaginas[i] = true;
                        }
                    }

                    if (ultimaPagina !== pagina) {

                        var valor = alPaginas[pagina]

                        if (valor) {
                            document.getElementById("MarcarPag").value = "Marcar Página";
                        } else {
                            document.getElementById("MarcarPag").value = "Desmarcar Página";
                        }

                    }
                    ultimaPagina = pagina;
                }


            });

            $("#textAlias").change(function(event) {
                if ($("#textAlias").empty()) {
                    $("#textSubCta").val("");
                    $('#textSubCta').prop('disabled', true);
                }
            });
            $('#selecFicheroReglas').change(function(event) {
                console.log(this.files[0].mozFullPath);
                event.preventDefault();
                var files = event.target.files;
                cargarFichero(files);
            });



        }); 

        function initialize() {

            seguimientoBusqueda();
            cargaDatosGestionReglas();

            cargarComboNemotecnico();
            cargarComboCapacidad();
            cargarComboModeloNegocio();
            cargarComboMiembroCompensador();
            cargarComboCamaraCompensacion();
            cargarComboCuentaCompensacion();
            cargarComboSegmento();
            cargaAlias();
            cargaCodigo();
            prepareCollapsion();

            $('#textSubCta').prop('disabled', true);
            $('#selectedSubCta').prop('disabled', true);

            $('#textSubCtaAsig').prop('disabled', true);
            $('#selectedSubCtaAsig').prop('disabled', true);

        }

        function cargaDatosGestionReglas() {

            var tablasParams = [];
            if ($scope.filter.estado === "0") {
                document.getElementById("titleTitu").innerHTML = "Reglas Inactivas";
            } else if ($scope.filter.estado === "1") {
                document.getElementById("titleTitu").innerHTML = "Reglas Activas";
            } else {
                document.getElementById("titleTitu").innerHTML = "Reglas";
            }

            console.log("cargaDatosGestionReglas");

            var cdCodigo = $("#idReg").val();

            if ($('#codigoDescripcion').val() === "" || $('#codigoDescripcion').val() === undefined) {

                cdCodigo = "";

            } else {
                cdCodigo = obtenerIdSeleccionadoText($("#codigoDescripcion").val());
            }

            var camaraCompensacion = $("#camaraCompensacion").val();
            var segmento = $("#segmento").val();
            var capacidad = $("#capacidad").val();
            var miembroCompensador = $("#miembroCompensador").val();
            var referenciaAsignacion = $("#referenciaAsignacion").val();
            var referenciaCliente = $("#referenciaCliente").val();
            var modeloNegocio = $("#modeloNegocio").val();
            var cuentaCompensacion = $("#cuentaCompensacion").val();
            var nemotecnico = $("#nemotecnico").val();
            var referenciaExterna = $("#referenciaExterna").val();
            var valorSeleccionadoAlias = obtenerIdSeleccionadoText($('#textAlias').val());
            var valorSeleccionadoSubCta = obtenerIdSeleccionadoText($('#textSubCta').val());


            if ($('#cuentaCompensacion').val() === "" || $('#cuentaCompensacion').val() === undefined || $('#cuentaCompensacion').val() === null) {
                cuentaCompensacion = ""
            }

            var filtro = {
                "cdCodigo": cdCodigo,
                "camaraCompensacion": camaraCompensacion,
                "cuentaCompensacion": cuentaCompensacion,
                "miembroCompensador": miembroCompensador,
                "referenciaAsignacion": referenciaAsignacion,
                "nemotecnico": nemotecnico,
                "segmento": segmento,
                "referenciaCliente": referenciaCliente,
                "referenciaExterna": referenciaExterna,
                "capacidad": capacidad,
                "modeloNegocio": modeloNegocio,
                estado: $scope.filter.estado,
                "alias": valorSeleccionadoAlias,
                "subCta": valorSeleccionadoSubCta
            };

            console.log(filtro);
            seguimientoBusqueda();
            $("#tablaGestionReglas > tbody").empty();

            GestionReglasService.listaReglas(onSuccessListaReglas, onErrorRequest, filtro);

        }

        function onSuccessListaReglas(json) {

            console.log(json);
            console.log(json.resultados);
            var item = null;
            var rownum = 0;
            var estilo;
            var estiloParam;
            var contador1 = 0;

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.getListRules !== undefined) {


                var item = null;
                var datos = json.resultados.getListRules;

                var tbl = $("#tablaGestionReglas > tbody");
                $(tbl).html("");

                console.log('datos' + datos);
                oTable.fnClearTable();
                for (var k in datos) {

                    if (contador1 % 2 == 0) {
                        estilo = "even";
                    } else {
                        estilo = "odd";
                    }
                    var enlace = "";


                    posicion = contador;
                    var idName = "check_" + contador;
                    var txtMarcar = '<input style= "width:20px" ng-model="listaChk[' + k + ']" type="checkbox"  class=' + (contador) + '  id="' + idName + '" name="' + idName + '"bvalue="N" class="editor-active"/>';

                    item = datos[k];

                    if (item.activo == "1") {
                        imagenactivo = "activo";
                        titleactivo = "Desactivar";
                    } else {
                        imagenactivo = "desactivo";
                        titleactivo = "Activar";
                    }
                    if (contador % 2 == 0) {
                        estilo = "even";
                    } else {
                        estilo = "odd";
                    }
                    contador++;
                    if (item.nParametrizacionesActivas > 0) {

                        enlace = "ng-click=\"desplegarParam(\'tabla_a_desplegar" + contador1 + "\'," + item.id + "," + k + ")\" ";
                        enlace = enlace + " style=\"cursor: pointer; color: #ff0000;\"";

                    } else {
                        enlace = "";
                    }

                    idRegla = item.id != null ? item.id : '';

                    $scope.listaId.push(idRegla);

                    var fechaCreaFormato = item.fecha_creacion;
                    if (fechaCreaFormato != null && fechaCreaFormato != undefined && fechaCreaFormato !== '') {
                        var fechaCreaFormatoVector = fechaCreaFormato.split("-");
                        fechaCreaFormato = fechaCreaFormatoVector[2] + "/" + fechaCreaFormatoVector[1] + "/" + fechaCreaFormatoVector[0];
                    }

                    $scope.listaChk[k] = false;

                    // se controlan las acciones, se muestra el enlace cuando el usuario
                    // tiene el permiso.
                    var altaParametro = "";


                    if (SecurityService.inicializated) {
                        if (SecurityService.isPermisoAccion($rootScope.SecurityActions.ALTA_SUBLISTADO,
                                $location.path())) {
                            altaParametro = "<div class='taleft' ><a class=\"btn\" ng-click= \"crearParametrizacion('" + idRegla + "'); \"><img src='img/addParam.png'  title='AltaParametro' /></a></div> ";
                        }
                    }


                    var rowIndex = oTable.fnAddData([
                        txtMarcar,
                        altaParametro,
                        idRegla,
                        "<div id='idParam" + contador1 + "' class='taleft' " + enlace + " >" + item.cd_codigo + "</div>",
                        item.nb_descripcion,
                        fechaCreaFormato,
                        "<img src='img/" + imagenactivo + ".png'  title='" + titleactivo + "'/>", // "<div

                        item.nParametrizacionesActivas,
                        item.nAliasAsociados > 0 ? "<div id='idAlias" + contador1 + "' class='taleft' ng-click=\"desplegarAlias(\'tabla_a_alias" + contador1 + "\',\'" + idRegla + "\'," + contador1 + ")\" style=\"cursor: pointer; color: #ff0000;\">" + item.nAliasAsociados + "</div>" : 0,
                        item.nSubcuentasAsociadas > 0 ? "<div id='idSubCta" + contador1 + "' class='taleft' ng-click=\"desplegarSubCta(\'tabla_a_subcta" + contador1 + "\',\'" + idRegla + "\'," + contador1 + ")\" style=\"cursor: pointer; color: #ff0000;\">" + item.nSubcuentasAsociadas + "</div>" : 0,
                        item.cd_codigo,
                        idRegla,
                        item.activo
                    ], false);


                    $(oTable.fnGetNodes(rownum)).addClass(estilo);


                    // Tenemos "nParametrizaciones" parametrizaciones. Pintamos
                    // la cabecera
                    var row = '';
                    row += "<tr id='tabla_a_desplegar" + contador1 + "' style='display:none;'>" +
                        "<td colspan='10' style='border-width:0 !important;'>" + 
                        "<table id='tablaParam" + contador1 + "' style= margin-top:5px;'><tbody>";
                    row += "</tbody></table></td></tr>";
                    activoListaParam[contador1] = false;

                    var row2 = "<tr id='tabla_a_alias" + contador1 + "' style='display:none;'>" +
                        "<td colspan='7' ></td>" +
                        "<td colspan='3' style='border-width:0 !important;'>" + 
                        "<table id='tablaAlias" + contador1 + "' style= margin-top:5px;'><tbody>" +
                        "</tbody></table></td></tr>";
                    activoListaAlias[contador1] = false;

                    var row3 = "<tr id='tabla_a_subcta" + contador1 + "' style='display:none;'>" +
                        "<td colspan='7' ></td>" +
                        "<td colspan='3' style='border-width:0 !important;'>" + 
                        "<table id='tablaSubCta" + contador1 + "' style= margin-top:5px;'><tbody>" +
                        "</tbody></table></td></tr>";
                    activoListaSubCta[contador1] = false;

                    tablasParams.push(row);
                    tablasAlias.push(row2)
                    tablasSubCta.push(row3)

                    contador1++;
                    rownum++;


                } // for

                oTable.fnDraw();

            } else {
                $("section").load("views/configuracion/GestionReglas.html");
            }
            seguimientoBusqueda();

        }; // json


        // Botón de ejecutar la consulta.
        $scope.Consultar = function() {

            // título de pantalla emergente si es modificar regla
            document.getElementById("titleTitu").innerHTML = "Reglas";


            $('.collapser_search').parents('.title_section').next().slideToggle();
            $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
            $('.collapser_search').toggleClass('active');
            if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
                $('.collapser_search').text("Mostrar opciones de búsqueda");
            } else {
                $('.collapser_search').text("Ocultar opciones de búsqueda");
            }

            cargaDatosGestionReglas();
            seguimientoBusqueda();
            return false;
        }

        $scope.modificar = false;
        // Botón de crear Regla.
        $scope.crearRegla = function() {
            $scope.modificar = false;
            $scope.cerrar();
            creaModifica = true;
            // título de pantalla emergente si es crear regla
            document.getElementById("titleTitulo").innerHTML = "Crear Regla";
            $("#formularios").attr('class', 'modal modalRegla');
            $("#cargarNuevaRegla").css('display', 'block');

            crearNuevaRegla(oTable);
        }

        function crearNuevaRegla(oTable) {


            var codigoNew = "";
            var descripcionNew = "";
            var fechaCreacionNew = "";


            $('#codigoNuevo').val(codigoNew);
            $('#descripcionNuevo').val(descripcionNew);
            $('#fechaCreacionNuevo').val(fechaCreacionNew);

            $('#formularios').css('display', 'block');
            $("#cargarNuevaRegla").css('display', 'block');
            $('fade').css('display', 'block');


            $('div#formularios input.mybutton').attr('data', 'crear');

        }

        $scope.modificarRegla = function() {
            $scope.modificar = true;
            // título de pantalla emergente si es modificar regla
            $scope.cerrar();
            document.getElementById("titleTitulo").innerHTML = "Modificar Regla";
            $("#formularios").attr('class', 'modal modalRegla');
            $("#cargarNuevaRegla").css('display', 'inline');
            $("#cargarNuevaRegla").css('display', 'block');

            creaModifica = false;

            modificarReglaDatos(oTable);
        }


        // Declara los datos de modificaciones
        function modificarReglaDatos(oTable) {

            console.log("Modificación de regla");

            $scope.listaModi = [];
            var aData = oTable.fnGetData();
            var input;
            var idName;
            var inputSel;

            for (var i = 0, j = aData.length; i < j; i++) {
                if ($scope.listaChk[i] == true) {
                    $scope.listaModi.push(aData[i]);
                    console.log("seleccionado: " + aData[i]);
                }
            }
            if ($scope.listaModi.length > 1) {
                fErrorTxt("Solo se puede seleccionar una regla para ser modificada.", 1);
                return false;
            }
            console.log($scope.listaModi);

            if ($scope.listaModi.length == 0) {
                fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                return;
            } else {
                $('#formularios').css('display', 'block');
                $("#cargarNuevaRegla").css('display', 'block');
                $('fade').css('display', 'block');
                $('div#formularios input.mybutton').attr('data', 'modificar');
            }

            $scope.datosModi = {
                idRegla: "",
                cdCodigo: "",
                descripcion: "",
                fecha_creacion: "",
                audit_usuario: "",
                audit_fecha: ""
            }

            $scope.datosModi.cdCodigo = ($scope.listaModi[0][10] !== null && $scope.listaModi[0][10] !== undefined) ? $scope.listaModi[0][10].trim() : "";
            $scope.datosModi.descripcion = ($scope.listaModi[0][4] !== null && $scope.listaModi[0][4] !== undefined) ? $scope.listaModi[0][4].trim() : "";
            $scope.datosModi.fecha_creacion = ($scope.listaModi[0][5] !== null && $scope.listaModi[0][5] !== undefined) ? $scope.listaModi[0][5].trim() : "";
            $scope.datosModi.idRegla = ($scope.listaModi[0][11] !== null && $scope.listaModi[0][11] !== undefined) ? $scope.listaModi[0][11] : 0;

            $('#codigoNuevo').val($scope.datosModi.cdCodigo);
            $('#descripcionNuevo').val($scope.datosModi.descripcion);
            $('#fechaCreacionNuevo').val($scope.datosModi.fecha_creacion);
            $('#idRegla').val($scope.datosModi.idRegla);
        }

        $scope.guardarReglaNueva = function() {

            $scope.datosModi.cdCodigo = $("#codigoNuevo").val();
            $scope.datosModi.descripcion = $("#descripcionNuevo").val();

            var filtro;

            if (creaModifica != true) {

                filtro = {
                    "idRegla": $scope.datosModi.idRegla,
                    "cdCodigo": $scope.datosModi.cdCodigo,
                    "descripcion": $scope.datosModi.descripcion,
                    "fecha_creacion": $scope.datosModi.fecha_creacion
                }

                GestionReglasService.modificarRegla(onSuccessModificarRegla, onErrorRequest, filtro);

            } else {

                filtro = {
                    "cdCodigo": $scope.datosModi.cdCodigo,
                    "descripcion": $scope.datosModi.descripcion,
                    "fecha_creacion": $scope.datosModi.fecha_creacion
                }
                GestionReglasService.altaRegla(onSuccessAltaRegla, onErrorRequest, filtro);
            }

        }

        function onSuccessModificarRegla(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_modificado !== undefined) {
                var datos = json.resultados.resultado_modificado;
                fErrorTxt("La regla ha sido modificada con éxito. ", 3);

            } else {

                fErrorTxt(json.error, 2);
            }

            $scope.cerrar();
            cargaDatosGestionReglas();
        }

        function onSuccessAltaRegla(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados !== undefined) {
                var datos = json.resultados;
                fErrorTxt("La regla ha sido creada con éxito. ", 3);

            } else {
                fErrorTxt(json.error, 2);
            }
            $scope.cerrar();
            cargaDatosGestionReglas();
        }

        // Botón de eliminar reglas.
        $scope.eliminarRegla = function() {

            console.log("Eliminar reglas");

            $scope.listaBorr = [];
            var aData = oTable.fnGetData();

            var prue1 = [];
            var params = [];

            // seleccionar checkbox
            for (var i = 0, j = aData.length; i < j; i++) {
                if ($scope.listaChk[i] == true) {
                    $scope.listaBorr.push(aData[i]);
                }
            }

            if ($scope.listaBorr.length == 0) {
                fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                return false;
            }

            var paramIndi = null


            for (var i = 0, j = $scope.listaBorr.length; i < j; i++) {
                paramIndi = {
                    "idRegla": $scope.listaBorr[i][11],
                    "estado": $scope.listaBorr[i][12]
                };
                params.push(paramIndi);
            }

            angular.element("#dialog-confirm").html("¿Desea activar/desactivar estas reglas?");

            // Define the Dialog and its properties.
            angular.element("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "Mensaje de Confirmación",
                height: 125,
                width: 360,
                buttons: {
                    " Sí ": function() {
                        GestionReglasService.eliminarRegla(onSuccessEliminarRegla, onErrorRequest, params);
                        $(this).dialog('close');
                        cargaDatosGestionReglas();

                    },
                    " No ": function() {
                        $(this).dialog('close');
                    }
                }
            });
            $('.ui-dialog-titlebar-close').remove();


        }

        function onSuccessEliminarRegla(json) {

            console.log(json.resultados);
            if (json.resultados !== null && json.resultados !== undefined && json.resultados !== undefined) {
                var datos = json.resultados;
                fErrorTxt(json.resultados.getEliminarRegla, 3);

            } else {

                fErrorTxt(json.error, 2);
            }

            $scope.cerrar();
            cargaDatosGestionReglas();

        }

        $scope.cambioActivoRegla = function(idRegla, regla_activa) {


            var filtro = {
                "idRegla": idRegla,
                "regla_activa": regla_activa
            };

            angular.element("#dialog-confirm").html("¿Desea cambiar el estado de la regla?");

            // Define the Dialog and its properties.
            angular.element("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "Mensaje de Confirmación",
                height: 120,
                width: 360,
                buttons: {
                    " Sí ": function() {

                        GestionReglasService.actualizarActivoRegla(onSuccessActualizarActivoRegla, onErrorRequest, filtro);
                        $(this).dialog('close');
                        cargaDatosGestionReglas();

                    },
                    " No ": function() {
                        $(this).dialog('close');
                    }
                }
            });

        };

        function onSuccessActualizarActivoRegla(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_modificado !== undefined) {
                var datos = json.resultados.resultado_modificado;
                fErrorTxt("La regla ha sido cambiada. ", 3);

            } else {
                fErrorTxt(json.error, 2);
            }
            cargaDatosGestionReglas();
        }

        // Botón de crear Parametrización.
        $scope.crearParametrizacion = function(idRegla) {

            creaModificaParame = true;
            $scope.nuevaParametrizacion = true;

            document.getElementById("titleTitulo1").innerHTML = "Crear Parametrización";
            $("#formulariosPara").attr('class', 'modal modalalto365');
            $("#cargarNuevaParametrizacion").css('display', 'block');
            $('#idReg1').val(idRegla);
            crearNuevaParametrizacion(oTable);
        }

        function crearNuevaParametrizacion(oTable) {


            var descripcionNew = "";
            var camaraCompensacionNew = "";
            var nemotecnicoNew = "";

            var cuentaCompensacionNew = "";
            var segmentoNew = "";
            var capacidadNew = "";

            var miembroCompensadorNew = "";
            var referenciaClienteNew = "";
            var modeloNegocioNew = "";

            var referenciaAsignacionNew = "";
            var referenciaExternaNew = "";
            var fechaFinNew = "";
            var fechaInicioNew = "";

            $('#descripcion1').val(descripcionNew);
            $('#camaraCompensacion1').val(camaraCompensacionNew);
            $('#nemotecnico1').val(nemotecnicoNew);

            $('#cuentaCompensacion1').val(cuentaCompensacionNew);
            $('#segmento1').val(segmentoNew);
            $('#capacidad1').val(capacidadNew);

            $('#miembroCompensador1').val(miembroCompensadorNew);
            $('#referenciaCliente1').val(referenciaClienteNew);
            $('#modeloNegocio1').val(modeloNegocioNew);

            $('#referenciaAsignacion1').val(referenciaAsignacionNew);
            $('#referenciaExterna1').val(referenciaExternaNew);
            $('#fechaFin1').val(fechaFinNew);
            $('#fechaInicio1').val(fechaInicioNew);

            $('#formulariosPara').css('display', 'block');
            $("#cargarNuevaParametrizacion").css('display', 'block');
            $('fade').css('display', 'block');

            $('div#formulariosPara input.mybutton').attr('data', 'crear');
            controlaLosCombos = false;
            creaModificaParame = true;
        }

        $scope.guardarParametrizacionNueva = function() {

            if (creaModificaParame != true) {

                var idParametrizacion = $("#idReg1").val();
                var descripcionParametrizacion = $("#descripcion1").val();
                var idCamaraCompensacion = $("#camaraCompensacion1").val();
                var id_nemotecnico = $("#nemotecnico1").val();
                var idCuentaCompensacion = $("#cuentaCompensacion1").val();

                if ($('#cuentaCompensacion1').val() === "" || $('#cuentaCompensacion1').val() === undefined || $('#cuentaCompensacion1').val() === null) {
                    idCuentaCompensacion = "";
                }

                var id_segmento = $("#segmento1").val();
                var id_indicador_capacidad = $("#capacidad1").val();
                var idCompensador = $("#miembroCompensador1").val();
                var referenciaCliente = $("#referenciaCliente1").val();

                var id_modelo_de_negocio = $("#modeloNegocio1").val();
                var referenciaAsignacion = $("#referenciaAsignacion1").val();
                var referenciaExterna = $("#referenciaExterna1").val();
                var fecha_fin = $("#fechaFin1").val();
                var fecha_inicio = $("#fechaInicio1").val();
                var referenciaCliente = $("#referenciaCliente1").val();

                var filtro = {
                    "idParametrizacion": idParametrizacion,
                    "descripcionParametrizacion": descripcionParametrizacion,
                    "idCamaraCompensacion": idCamaraCompensacion,
                    "id_nemotecnico": id_nemotecnico,
                    "idCuentaCompensacion": idCuentaCompensacion,
                    "id_segmento": id_segmento,
                    "id_indicador_capacidad": id_indicador_capacidad,
                    "idCompensador": idCompensador,
                    "referenciaCliente": referenciaCliente,
                    "id_modelo_de_negocio": id_modelo_de_negocio,
                    "referenciaAsignacion": referenciaAsignacion,
                    "referenciaExterna": referenciaExterna,
                    "fecha_fin": fecha_fin,
                    "fecha_inicio": fecha_inicio
                };

                GestionReglasService.modificarParametrizacion(onSuccessmodificarParametrizacion, onErrorRequest, filtro);

            } else {
                var descripcion1 = $("#descripcion1").val();
                var camaraCompensacion1 = $("#camaraCompensacion1").val();
                var nemotecnico1 = $("#nemotecnico1").val();
                var cuentaCompensacion1 = $("#cuentaCompensacion1").val();

                var segmento1 = $("#segmento1").val();
                var capacidad1 = $("#capacidad1").val();
                var miembroCompensador1 = $("#miembroCompensador1").val();
                var referenciaCliente1 = $("#referenciaCliente1").val();

                var modeloNegocio1 = $("#modeloNegocio1").val();
                var referenciaAsignacion1 = $("#referenciaAsignacion1").val();
                var referenciaExterna1 = $("#referenciaExterna1").val();
                var fecha_fin = $("#fechaFin1").val();
                var fecha_inicio = $("#fechaInicio1").val();
                var idRegla = $("#idReg1").val();
                // falta meter fecha fin en el filtro

                var filtro = {
                    "idRegla": idRegla,
                    "descripcion1": descripcion1,
                    "camaraCompensacion1": camaraCompensacion1,
                    "nemotecnico1": nemotecnico1,
                    "cuentaCompensacion1": cuentaCompensacion1,
                    "segmento1": segmento1,
                    "capacidad1": capacidad1,
                    "miembroCompensador1": miembroCompensador1,
                    "referenciaCliente1": referenciaCliente1,
                    "fecha_fin": fecha_fin,
                    "fecha_inicio": fecha_inicio,
                    "modeloNegocio1": modeloNegocio1,
                    "referenciaAsignacion1": referenciaAsignacion1,
                    "referenciaExterna1": referenciaExterna1
                };

                GestionReglasService.altaParametrizacion(onSuccessaltaParametrizacion, onErrorRequest, filtro);

            }

            $scope.cerrar();


        }

        function onSuccessaltaParametrizacion(json) {
            console.log(json);
            console.log(json.resultados);
            if (json.resultados !== null && json.resultados !== undefined && json.resultados.getAltaParametrizaciones !== undefined) {
                var datos = json.resultados.getAltaParametrizaciones;
                fErrorTxt("La parametrización ha sido creada con éxito. ", 3);
                cargaDatosGestionReglas();

            } else {

                fErrorTxt(json.error, 2);
            }
        }

        function onSuccessmodificarParametrizacion(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_modificado !== undefined) {
                var datos = json.resultados.resultado_modificado;
                fErrorTxt("La parametrización ha sido modificada con éxito. ", 3);
                console.log("numero fila : " + $scope.numero_fila);
                console.log("idRegla : " + idRegla);
                $scope.desplegarParameFilas(idRegla, $scope.numero_fila);
            } else {

                fErrorTxt(json.error, 2);
            }

        }

        $scope.asigAlias = function() {

            console.log("Asignación de regla a Alias");

            $scope.cerrar();

            $scope.listaModi = [];
            var aData = oTable.fnGetData();
            var input;
            var idName;
            var inputSel;

            for (var i = 0, j = aData.length; i < j; i++) {
                if ($scope.listaChk[i] == true) {
                    $scope.listaModi.push(aData[i]);
                    console.log("seleccionado: " + aData[i]);
                }
            }

            if ($scope.listaModi.length == 0) {
                fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                return false;
            }

            if ($scope.listaModi.length > 1) {
                fErrorTxt("Solo se puede seleccionar una regla para ser Asignada.", 1);
                return false;
            } else {
                $('#formAlias').css('display', 'block');
                $("#fAsigAlias").css('display', 'block');
                $('fade').css('display', 'block');

                $('div#formularios input.mybutton').attr('data', 'modificar');
                $("#selectedAliasAsig").html("");

                var filtro = {
                    "idRegla": $scope.listaModi[0][11]
                };

                inicializarLoading();
                GestionReglasService.getAliasList(onSuccessAsigAliasList, onErrorRequest, filtro);
            }


            $scope.datosModi.cdCodigo = ($scope.listaModi[0][10] !== null && $scope.listaModi[0][10] !== undefined) ? $scope.listaModi[0][10].trim() : "";
            $scope.datosModi.idRegla = ($scope.listaModi[0][11] !== null && $scope.listaModi[0][11] !== undefined) ? $scope.listaModi[0][11] : 0;


            $('#idReglaAsigAlias').val($scope.datosModi.idRegla);
            $("#idReglaAsigAlias").attr('disabled', true);
            $('#codAsigAlias').val($scope.datosModi.cdCodigo);
            $("#codAsigAlias").attr('disabled', true);


            $scope.asignar.idRegla = $scope.listaModi[0][11]

        }

        function onSuccessAsigAliasList(json) {


            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultados_lista_alias !== undefined) {

                var lista = "#selectedAliasAsig";
                var datos = json.resultados.resultados_lista_alias;

                for (var p in datos) {

                    var alias = datos[p];
                    if (!valueExistInList(lista, alias.cdaliass)) {
                        $(lista).append("<option value=\"" + alias.cdaliass + "\" >" + alias.cdaliass + " - " + alias.descrali + "</option>");
                    }
                }

                $.unblockUI();
            }
        }

        $scope.asignarAlias = function() {

            $scope.cerrar();

            var valorSeleccionadoAlias = obtenerIdsSeleccionados('#selectedAliasAsig');

            var code = $("#codAsigAlias").val();
            var filtro = {
                "idRegla": $scope.asignar.idRegla,
                "code": code,
                "alias": valorSeleccionadoAlias
            };
            GestionReglasService.asignarAlias(onSuccessAsignarAlias, onErrorRequest, filtro);

        }

        function onSuccessAsignarAlias(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_asignacionAlias !== undefined) {
                fErrorTxt(json.resultados.resultado_asignacionAlias, 3);
                cargaDatosGestionReglas();
            } else {

                fErrorTxt(json.error, 2);
            }

        }


        function cargaAlias() {
            var availableTagsAlias = [];
            $(function() {
                var data = new DataBean();
                data.setService('SIBBACServiceTmct0ali');
                data.setAction('getAlias');
                var request = requestSIBBAC(data);
                request.success(function(json) {
                    var datos = undefined;
                    if (json === undefined || json.resultados === undefined ||
                        json.resultados.result_alias === undefined) {

                        datos = {};
                    } else {
                        datos = json.resultados.result_alias;

                    }
                    var obj;
                    $(datos).each(function(key, val) {
                        obj = {
                            label: val.alias + " - " + val.descripcion,
                            value: val.alias
                        };
                        availableTagsAlias.push(obj);
                    });
                    $("#btnDelAlias").click(function(event) {
                        event.preventDefault();
                        $("#selectedAlias").find("option:selected").remove().end();
                    });
                    $("#btnDelAliasAsig").click(function(event) {
                        event.preventDefault();
                        $("#selectedAliasAsig").find("option:selected").remove().end();
                    });
                    pupulateAliasAutocompleteInput("#textAlias", "#idTxAlias", availableTagsAlias);
                    pupulateAliasAutocompleteInput("#textAliasSubCta", "#idTxAliasSubCta", availableTagsAlias);
                    pupulateAliasAutocomplete("#textAliasAsig", "#idTxAliasAsig", availableTagsAlias, "#selectedAliasAsig");
                });
            });
            // fin
        }

        function pupulateAliasAutocompleteInput(input, idContainer, availableTagsAlias, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsAlias,
                focus: function(event, ui) {
                    return false;
                },
                select: function(event, ui) {
                    $(input).val(ui.item.label);
                    if (idContainer === "#idTxAlias") {
                        $('#textSubCta').prop('disabled', false);
                    } else if (idContainer === "#idTxAliasSubCta") {
                        $('#textSubCtaAsig').prop('disabled', false);
                        $('#selectedSubCtaAsig').prop('disabled', false);
                    }
                    cargaSubCta(ui.item.value);

                    return false;
                }
            });
        }

        function pupulateAliasAutocomplete(input, idContainer, availableTagsAlias, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsAlias,
                focus: function(event, ui) {
                    return false;
                },
                select: function(event, ui) {
                    $(idContainer).val(ui.item.label);

                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                    }

                    return false;
                }
            });
        }

        function cargaCodigo() {
            var availableTagsAlias = [];
            $(function() {
                var data = new DataBean();
                data.setService('SIBBACServiceGestionReglas');
                data.setAction('getCodigoList');
                var request = requestSIBBAC(data);
                request.success(function(json) {
                    var datos = undefined;
                    if (json === undefined || json.resultados === undefined ||
                        json.resultados.result_codigo === undefined) {

                        datos = {};
                    } else {
                        datos = json.resultados.result_codigo;

                    }
                    var obj;
                    $(datos).each(function(key, val) {
                        obj = {
                            label: val.cdCodigo + " - " + val.nbDescripcion,
                            value: val.cdCodigo
                        };
                        availableTagsCodigo.push(obj);
                    });
                    pupulateAliasAutocompleteInput("#codigoDescripcion", "#idRegl", availableTagsCodigo);

                });
            });
        }

        function populatreCodigoAutocompleteInput() {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsAlias,
                focus: function(event, ui) {
                    return false;
                },
                select: function(event, ui) {
                    $(input).val(ui.item.label);
                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                    }

                    return false;
                }
            });
        }

        $scope.asigSubCta = function() {

            $scope.cerrar();

            console.log("Asignación de regla a SubCtas");

            $scope.listaModi = [];
            var aData = oTable.fnGetData();
            var input;
            var idName;
            var inputSel;

            for (var i = 0, j = aData.length; i < j; i++) {
                if ($scope.listaChk[i] == true) {
                    $scope.listaModi.push(aData[i]);
                    console.log("seleccionado: " + aData[i]);
                }
            }

            if ($scope.listaModi.length == 0) {
                fErrorTxt("Debe seleccionar al menos uno de ellos.", 2)
                return false;
            }

            if ($scope.listaModi.length > 1) {
                fErrorTxt("Solo se puede seleccionar una regla para ser Asignada.", 1);
                return false;
            } else {
                $('#formSubCta').css('display', 'block');
                $("#fAsigSubCta").css('display', 'block');
                $('fade').css('display', 'block');

                $('div#formularios input.mybutton').attr('data', 'modificar');
            }

            // Se asignan los valores de la regla seleccionada
            $scope.datosModi.cdCodigo = ($scope.listaModi[0][10] !== null && $scope.listaModi[0][10] !== undefined) ? $scope.listaModi[0][10].trim() : "";
            $scope.datosModi.idRegla = ($scope.listaModi[0][11] !== null && $scope.listaModi[0][11] !== undefined) ? $scope.listaModi[0][11] : 0;

            // Se limmpian el alias y las subcuentas que pudieran tener
            $scope.LimpiarSubCtaAsig();

            $('#idReglaAsigSubCta').val($scope.datosModi.idRegla);
            $("#idReglaAsigSubCta").attr('disabled', true);
            $('#codAsigSubCta').val($scope.datosModi.cdCodigo);
            $("#codAsigSubCta").attr('disabled', true);
            $scope.asignar.idRegla = $scope.listaModi[0][11]
        }

        $scope.asignarSubCta = function() {

            $scope.cerrar();

            var valorSeleccionadoSubCta = obtenerIdsSeleccionados('#selectedSubCtaAsig');

            var code = $("#codAsigAlias").val();

            var code = $("#codAsigSubCta").val();

            var alias = obtenerIdSeleccionadoText($("#textAliasSubCta").val());
            var filtro = {
                "idRegla": $scope.asignar.idRegla,
                "code": code,
                "alias": alias,
                "subCta": valorSeleccionadoSubCta
            };
            GestionReglasService.asignarSubSta(onSuccessAsignarSubCta, onErrorRequest, filtro);

        }


        function cargaSubCta(alias) {
            var availableTagsSubCta = [];
            $(function() {

                var filtro = "{\"alias\" : \"" + alias + "\"} ";
                var data = new DataBean();
                data.setService('SIBBACServiceTmct0Act');
                data.setAction('getListaSubCta');
                data.setFilters(filtro);
                var request = requestSIBBAC(data);
                request.success(function(json) {
                    var datos = undefined;
                    if (json === undefined || json.resultados === undefined || json.resultados.listaSubCta === undefined) {

                        datos = {};
                    } else {
                        datos = json.resultados.listaSubCta;

                    }
                    var obj;
                    $(datos).each(function(key, val) {
                        obj = {
                            label: val.cdSubCta.trim() + " - " + val.desacrali,
                            value: val.cdSubCta
                        };
                        availableTagsSubCta.push(obj);
                    });
                    $("#btnDelSubCta").click(function(event) {
                        event.preventDefault();
                        $("#selectedSubCta").find("option:selected").remove().end();
                    });
                    $("#btnDelSubCtaAsig").click(function(event) {
                        event.preventDefault();
                        $("#selectedSubCtaAsig").find("option:selected").remove().end();
                    });
                    pupulateSubCtaAutocompleteInput("#textSubCta", "#idTxSubCta", availableTagsSubCta, "#selectedSubCta");
                    pupulateSubCtaAutocomplete("#textSubCtaAsig", "#idTxSubCtaAsig", availableTagsSubCta, "#selectedSubCtaAsig");
                });
            });
            // fin
        }

        function pupulateSubCtaAutocompleteInput(input, idContainer, availableTagsSubCta, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsSubCta,
                focus: function(event, ui) {
                    // $( this ).val( ui.item.label );
                    return false;
                },
                select: function(event, ui) {
                    $(input).val(ui.item.label);

                    return false;
                }
            });
        }

        function pupulateSubCtaAutocomplete(input, idContainer, availableTagsSubCta, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsSubCta,
                focus: function(event, ui) {
                    return false;
                },
                select: function(event, ui) {
                    $(idContainer).val(ui.item.label);
                    if (!valueExistInList(lista, ui.item.value)) {
                        $(lista).append("<option value=\"" + ui.item.value + "\" >" + ui.item.label + "</option>");
                        $(input).val("");
                        $(lista + "> option").attr('selected', 'selected');
                    }
                    return false;
                }
            });
        }

        function onSuccessAsignarSubCta(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_asignacionSubCta !== undefined) {
                var datos = json.resultados.resultado_asignacionSubCta;


                fErrorTxt(json.resultados.resultado_asignacionSubCta, 3);
                cargaDatosGestionReglas();

            } else {

                fErrorTxt(json.error, 2);
            }


        }

        /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
        function valueExistInList(lista, valor) {
            var inList = false;
            $(lista + ' option').each(function(index) {

                if (this.value == valor) {
                    inList = true;
                    return inList;
                }
            });
            return inList;
        }

        function obtenerIdsSeleccionados(lista) {
            var selected = ""
            var i = 0;
            $(lista + ' option').each(function() {
                if (i > 0) {
                    selected += ";";
                }
                selected += $(this).val();
                i++;
            });
            return selected;
        }
        // Revisado hasta aqui.


        // Carga el combo de Cámara de Compensación(CCP).
        function cargarComboCamaraCompensacion() {

            var data = new DataBean();
            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getCamaraCompensacionList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {

                var datos = json.resultados.resultados_CCP;

                for (var k in datos) {

                    item = datos[k];

                    $('#camaraCompensacion1').append("<option value='" + item.idCamaraCompensacion + "'>(" + item.cdCodigo + ") " + item.nbDescripcion + "</option>");
                    $('#camaraCompensacion').append("<option value='" + item.idCamaraCompensacion + "'>(" + item.cdCodigo + ") " + item.nbDescripcion + "</option>");
                }

            });

        }

        // Carga el combo Miembro Compensador.
        function cargarComboMiembroCompensador() {

            var data = new DataBean();

            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getMiembrosCompensacionList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {


                var datos = json.resultados.compensador_list;

                for (var k in datos) {

                    item = datos[k];

                    $('#miembroCompensador1').append("<option value='" + item.idCompensador + "'>(" + item.nombre + ") " + item.descripcion + "</option>");
                    $('#miembroCompensador').append("<option value='" + item.idCompensador + "'>(" + item.nombre + ") " + item.descripcion + "</option>");

                }

            });
        }


        // Carga el combo Modelo Negocio.
        function cargarComboModeloNegocio() {

            var data = new DataBean();

            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getListModeloDeNegocio');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {


                var datos = json.resultados.modelo_de_negocio_list;

                for (var k in datos) {

                    item = datos[k];

                    $('#modeloNegocio1').append("<option value='" + item.id_modelo_de_negocio + "'>(" + item.cd_codigo + ") " + item.nbDescripcion + "</option>");
                    $('#modeloNegocio').append("<option value='" + item.id_modelo_de_negocio + "'>(" + item.cd_codigo + ") " + item.nbDescripcion + "</option>");


                }

            });
        }


        // Carga el combo Clearing account.
        function cargarComboCuentaCompensacion() {

            var data = new DataBean();

            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getCuentasCompensacionList');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {


                var datos = json.resultados.cuentas_compensacion_list;

                for (var k in datos) {

                    item = datos[k];

                    $('#cuentaCompensacion1').append("<option value='" + item.idCuentaCompensacion + "'> " + item.cdCodigo + "</option>");
                    $('#cuentaCompensacion').append("<option value='" + item.idCuentaCompensacion + "'> " + item.cdCodigo + "</option>");

                }

            });
        }

        // Carga el combo de Segmento.
        function cargarComboSegmento() {

            var data = new DataBean();

            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getListSegmento');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {


                var datos = json.resultados.segment_list;

                for (var k in datos) {

                    item = datos[k];

                    $('#segmento1').append("<option value='" + item.id_segmento + "'>(" + item.cd_codigo + ") " + item.nbDescripcion + "</option>");
                    $('#segmento').append("<option value='" + item.id_segmento + "'>(" + item.cd_codigo + ") " + item.nbDescripcion + "</option>");

                }

            });
        }


        // Carga el combo Nemotécnico.
        function cargarComboNemotecnico() {

            var data = new DataBean();

            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getListNemotecnicos');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {


                var datos = json.resultados.nemotecnicos_list;

                for (var k in datos) {

                    item = datos[k];

                    $('#nemotecnico1').append("<option value='" + item.id_nemotecnico + "'>(" + item.nbnombre + ") " + item.nbDescripcion + "</option>");
                    $('#nemotecnico').append("<option value='" + item.id_nemotecnico + "'>(" + item.nbnombre + ") " + item.nbDescripcion + "</option>");

                }

            });
        }


        // Carga el combo de Capacidad.
        function cargarComboCapacidad() {

            var data = new DataBean();
            data.setService('SIBBACServiceGestionReglas');
            data.setAction('getListCapacidad');
            var request = requestSIBBAC(data);
            var item;
            request.success(function(json) {

                var datos = json.resultados.capacidad_list;

                for (var k in datos) {

                    item = datos[k];

                    $('#capacidad1').append("<option value='" + item.id_indicador_capacidad + "'>" + item.descripcion + "</option>");
                    $('#capacidad').append("<option value='" + item.id_indicador_capacidad + "'>" + item.descripcion + "</option>");

                }

            });
        }

        // Funcion en la que captura el filtro y muestra todos titulares que cumplan
        // dicha condición
        function buscarPorFiltroSeleccionado() {

            console.log("cargaDatosGestionReglas");
            carga = false;

            document.getElementById("MarcarTod").value = "Marcar Todos";
            document.getElementById("titleTitu").innerHTML = "Reglas";
            cargaDatosGestionReglas();
        }


        // desplegar el subgrid o subtabla
        $scope.desplegar = function(tabla_a_desplegar, e1, estadoTfila, row) {
            $(oTable.fnGetNodes(row)).after(tablasParams[row]);
            var tablA = angular.element('#' + tabla_a_desplegar);
            var estadOt = angular.element('#' + e1);
            var fila = angular.element('#' + estadoTfila);
            console.log(angular.element(tablA).css('display'));
            var visibilidad = getVisibilidad(row, tablA);
            if (visibilidad === 'none') {
                angular.element(tablA).show();
                console.log('foco en tabla ' + tabla_a_desplegar);
                ajustarAltoTabla();
                document.getElementById(tabla_a_desplegar).scrollIntoView();

            } else {
                angular.element(tablA).hide();
            }
            tablasParams[row] = tablA;
        }



        $scope.modificarParametrizacion = function(idParametrizacion, descripcionParametrizacion, IdCamaraCompensacion, codigoCamaraComp, descripcionCamaraComp, ctaCompensacionCdCodigo,
            IdCuentaCompensacion, miembroCompensador, IdMiembroCompensador, referenciaAsignacion, nemotecnicoNbNombre, IdNemotecnico, segmentoCdCodigo,
            segmentoNbDescripcion, IdSegmento, referenciaCliente, referenciaExterna, capacidadDescripcion, IdCapacidad, modeloNegocioCdCodigo,
            modeloNegocioNbDescripcion, IdModeloNegocio, fecha_fin, fecha_inicio) {

            $scope.nuevaParametrizacion = false;
            creaModificaParame = false;

            if (IdCamaraCompensacion != null) {
                $scope.idComp = parseInt(IdCamaraCompensacion);
            }
            if (IdCuentaCompensacion != null) {
                $scope.idCuentaComp = parseInt(IdCuentaCompensacion);
            }

            if (IdMiembroCompensador != null) {
                $scope.idMiembroComp = parseInt(IdMiembroCompensador);
            }

            if (IdNemotecnico != null) {
                $scope.idNemotecnico = parseInt(IdNemotecnico);
            }

            if (IdSegmento != null) {
                $scope.idSegmento = parseInt(IdSegmento);
            }

            if (IdCapacidad != null) {
                $scope.idCapacidad = parseInt(IdCapacidad);
            }

            if (IdModeloNegocio != null) {
                $scope.idModeloNegocio = parseInt(IdModeloNegocio);
            }

            $('#descripcion1').val(descripcionParametrizacion);
            $('#referenciaAsignacion1').val(referenciaAsignacion);
            $('#referenciaExterna1').val(referenciaExterna);

            $('#camaraCompensacion1').val(parseInt(IdCamaraCompensacion));
            $('#cuentaCompensacion1').val(parseInt(IdCuentaCompensacion));
            $('#miembroCompensador1').val(parseInt(IdMiembroCompensador));
            $('#nemotecnico1').val(parseInt(IdNemotecnico));
            $('#segmento1').val(parseInt(IdSegmento));
            $('#capacidad1').val(parseInt(IdCapacidad));
            $('#modeloNegocio1').val(parseInt(IdModeloNegocio));
            $('#referenciaCliente1').val(referenciaCliente);
            $('#fechaFin1').val(fecha_fin);
            $('#fechaInicio1').val(fecha_inicio);

            document.getElementById("titleTitulo1").innerHTML = "Modificar Parametrización";

            $("#formulariosPara").css('display', 'inline');
            $("#cargarNuevaParametrizacion").css('display', 'inline');
            $('#idReg1').val(idParametrizacion);

        }


        // Botón que limpia las condiciones del filtro.
        $scope.LimpiarFiltros = function() {
            $('input[type=text]').val('');
            document.getElementById("codigoDescripcion").value = '';
            document.getElementById("miembroCompensador").value = '';
            document.getElementById("segmento").value = '';
            document.getElementById("capacidad").value = '';
            document.getElementById("camaraCompensacion").value = '';
            document.getElementById("referenciaAsignacion").value = '';
            document.getElementById("referenciaCliente").value = '';
            document.getElementById("modeloNegocio").value = '';
            document.getElementById("cuentaCompensacion").value = '';
            document.getElementById("nemotecnico").value = '';
            document.getElementById("referenciaExterna").value = '';
            document.getElementById("textSubCta").value = '';
            $('#textSubCta').prop('disabled', true);
            document.getElementById("idTxSubCta").value = '';

            lMarcar = true;
            document.getElementById("MarcarTod").value = "Marcar Todos";
        }

        $scope.LimpiarAliasAsig = function() {
            console.log("Limpiar Alias");
            document.getElementById("textAliasAsig").value = '';
            var x = document.getElementById("selectedAliasAsig");
            var y = x.length;
            for (var b = 0; b < y; b++) {
                x.remove(0);
            }
        }

        $scope.LimpiarSubCtaAsig = function() {
            console.log("Limpiar SubCta");
            document.getElementById("textAliasSubCta").value = '';
            document.getElementById("textSubCtaAsig").value = '';
            var x = document.getElementById("selectedSubCtaAsig");
            var y = x.length;
            for (var b = 0; b < y; b++) {
                x.remove(0);
            }

        }

        // Boton de marcar todos
        $scope.selececionarCheck = function() {
            if (lMarcar) {
                lMarcar = false;
                document.getElementById("MarcarTod").value = "Desmarcar Todos";
                toggleCheckBoxes(true);
            } else {
                lMarcar = true;
                document.getElementById("MarcarTod").value = "Marcar Todos";
                toggleCheckBoxes(false);
            }
        };


        // Boton de marcar página
        $scope.selececionarCheckPag = function() {

            var valor = alPaginas[pagina]

            toggleCheckBoxesPag(valor);

            if (valor) {
                document.getElementById("MarcarPag").value = "Desmarcar Página";
                alPaginas[pagina] = false;
            } else {
                document.getElementById("MarcarPag").value = "Marcar Página";
                alPaginas[pagina] = true;
            }
        };


        // Botón X que cierra formulario crear.
        $('#cerrarRegla').click(function(event) {
            event.preventDefault();
            $('#formularios').css('display', 'none');
            $('#cargarNuevaRegla').css('display', 'none');
            $scope.cerrar();
        });

        // Botón X que cierra formulario crear.
        $('#cerrarParametrizacion').click(function(event) {
            event.preventDefault();
            $('#formulariosPara').css('display', 'none');
            $('#cargarNuevaParametrizacion').css('display', 'none');
            $scope.cerrar();
        });

        // cierra los formularios de creacion/modificacion reglas
        $scope.cerrar = function() {

            $('#formularios').css('display', 'none');
            $('#cargarNuevaRegla').css('display', 'none');

            $('#formulariosPara').css('display', 'none');
            $('#cargarNuevaParametrizacion').css('display', 'none');

            $('#formAlias').css('display', 'none');
            $('#fAsigAlias').css('display', 'none');

            $('#formSubCta').css('display', 'none');
            $('#fAsigSubCta').css('display', 'none');
            $('#cargarFicheroReglas').css('display', 'none');
        }

        function lookBotones(tipo) {
            if (tipo) {

                if ($scope.listaChk.length > 0) {

                    $('#MarcarTod').css('display', 'inline');
                    $('#MarcarPag').css('display', 'inline');
                    $('#cargarNuevaRegla').css('display', 'inline');
                    $('#modificarRegla').css('display', 'inline');
                } else {

                    $('#CrearTitulares').css('display', 'inline');
                }
            } else {

                $('#MarcarTod').css('display', 'none');
                $('#MarcarPag').css('display', 'none');
                $('#cargarNuevaRegla').css('display', 'none');
                $('#modificarRegla').css('display', 'none');

            }
            return
        }


        function obtenerIdsSeleccionados(lista) {
            var selected = ""
            var i = 0;
            $(lista + ' option').each(function() {
                if (i > 0) {
                    selected += ";";
                }
                selected += $(this).val();
                i++
            });

            return selected;
        }




        function cargaRegla() {
            var availableTagsIsin = [];
            $(function() {
                var data = new DataBean();
                data.setService('SIBBACServiceGestionReglas');
                data.setAction('getCodigoList');
                var request = requestSIBBAC(data);
                request.success(function(json) {
                    var datos = undefined;
                    if (json === undefined || json.resultados === undefined ||
                        json.resultados.getListRules === undefined) {
                        datos = {};
                    } else {
                        datos = json.resultados.getListRules;
                    }
                    var obj;
                    $(datos).each(function(key, val) {
                        obj = {
                            label: val.cd_codigo + " - " + val.nb_descripcion,
                            value: val.cd_codigo,
                            idRegla: val.id
                        };
                        availableTagsCodigo.push(obj);
                    });

                    pupulateReglaAutocomplete("#codigoDescripcion", "#idTxRegla", availableTagsCodigo, '#selectedRegla');

                });
            });
            // fin
        }


        function pupulateReglaAutocomplete(input, idContainer, availableTagsIsin, lista) {
            $(input).autocomplete({
                minLength: 0,
                source: availableTagsIsin,
                focus: function(event, ui) {

                    return false;
                },
                select: function(event, ui) {

                    if (!valueExistInList(lista, ui.item.value, ui.item.idRegla)) {

                        $('#lista').append("<option value='" + ui.item.idRegla + "'>" + ui.item.label + " - " + ui.item.value + "</option>");
                        $('#idReg').val(ui.item.idRegla);

                        $(input).val(ui.item.label);

                        $(lista + "> option").attr('selected', 'selected');
                    }
                    return false;
                }
            });
            /** COMPRUEBA QUE UN VALOR NO ESTE DENTRO DE LA LISTA * */
            function valueExistInList(lista, valor) {
                var inList = false;
                $(lista + ' option').each(function(index) {

                    if (this.value == valor) {
                        inList = true;

                        return inList;
                    }
                });
                return inList;
            }
        }

        function onErrorRequest(data) {
            $.unblockUI();
            fErrorTxt(
                "Ocurrió un error durante la petición de datos.",
                1);
        } // onErrorRequest


        function cargaoTableParametrizacion() {

            oTable = $("#tablaParametrizaciones").dataTable({
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": ["/sibbac20/js/swf/copy_csv_xls_pdf.swf"],
                    "aButtons": ["copy",
                        {
                            "sExtends": "csv",
                            "sFileName": "Listado_de_parametrizaciones_" + hoy + ".csv"
                        },
                        {
                            "sExtends": "xls",
                            "sFileName": "Listado_de_parametrizaciones_" + hoy + ".xls"
                        },
                        {
                            "sExtends": "pdf",
                            "sPdfOrientation": "landscape",
                            "sTitle": " ",
                            "sPdfSize": "A4",
                            "sPdfMessage": "Listado gestion de Reglas",
                            "sFileName": "Listado_de_parametrizaciones_" + hoy + ".pdf",
                            "mColumns": "visible"
                        },
                        "print"
                    ]
                },
                "language": {
                    "url": "i18n/Spanish.json"
                },
                "scrollX": "100%",
                "scrollY": "480px",
                "scrollCollapse": true,
                "aoColumns": [

                    {
                        sClass: "centrar",
                        bSortable: false
                    }, // check
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left"
                    },
                    {
                        sClass: "align-left",
                        width: 200
                    },
                    {
                        sClass: "align-left",
                        visible: false
                    },

                ],
                "fnCreatedRow": function(nRow, aData, iDataIndex) {

                    $compile(nRow)($scope);
                },

            });
        } // cargaOtableParametrizacion


        // función para la subtabla que se desplega
        function getVisibilidad(idVisibilidad, tablA) {
            var visible = "none";
            var encontrado = false;
            angular.forEach($scope.visibilidad, function(val, key) {
                if (idVisibilidad == val.id) {
                    encontrado = true;
                    visible = val.visible;
                    val.visible = 'none';
                    if (visible === 'none') {
                        val.visible = 'block';
                    }

                    return false;
                }
            });
            if (!encontrado) {
                $scope.visibilidad.push({
                    id: idVisibilidad,
                    visible: "block"
                });
                tablA.find('tbody tr').each(function() {
                    $compile(this)($scope);
                });

            }
            return visible;
        };


        $scope.eliminarParametrizacion = function(idParametrizacion) {
            var filtro = "{\"idParametrizacion\" : \"" + idParametrizacion + "\" } ";

            angular.element("#dialog-confirm").html("¿Desea eliminar realmente la parametrización?");
            // Define the Dialog and its properties.
            angular.element("#dialog-confirm").dialog({
                resizable: false,
                modal: true,
                title: "Mensaje de Confirmación",
                height: 120,
                width: 380,

                buttons: {
                    "Sí": function() {
                        $(this).dialog('close');

                        var data = new DataBean();
                        data.setService('SIBBACServiceGestionReglas');
                        data.setAction('getEliminarParametrizacion');
                        data.setFilters(filtro);
                        var request = requestSIBBAC(data);
                        request.success(function(json) {
                            console.log(json);
                            console.log(json.resultados);


                            if (json.resultados !== null && json.resultados !== undefined) {
                                var datos = json.resultados;


                                fErrorTxt("La parametrización ha sido eliminada con éxito. ", 3);
                                console.log("numero fila : " + $scope.numero_fila);
                                console.log("idRegla : " + idRegla);
                                $scope.desplegarParameFilas(idRegla, $scope.numero_fila);

                            } else {

                                fErrorTxt(json.error, 2);
                            }
                        });
                    },
                    "No": function() {
                        $(this).dialog('close');
                    }
                }
            });

        };


        // Exportación de las parametrizaciones a excel.
        $scope.exportarParam = function() {

            var paramIndi = null;
            var params = [];
            var aData = oTable.fnGetData();


            for (var i = 0, j = aData.length; i < j; i++) {
                console.log(aData[i][4])
                var codigo = quitaAcentos(aData[i][10]);
                var texto = quitaAcentos(aData[i][4]);
                paramIndi = {
                    "idRegla": aData[i][11],
                    "cdCodigo": codigo,
                    "descripcion": texto,
                    "estado": aData[i][12]
                };
                console.log(paramIndi)
                params.push(paramIndi);
            }

            params.sort(function(a, b) {
                if (a.estado > b.estado) {
                    return -1;
                }
                if (a.estado < b.estado) {
                    return 1;
                }
                // a must be equal to b
                return 0;
            });

            var data = {
                action: "getExportarParam",
                service: "SIBBACServiceGestionReglas",
                params: params
            };
            console.log("data: " + data);

            var iframe = $('<iframe/>', {
                id: "RemotingIFrame"
            }).css({
                border: 'none',
                width: 0,
                height: 0
            }).appendTo('body');

            var contentWindow = iframe[0].contentWindow;
            contentWindow.document.open();
            contentWindow.document.close();
            var form = contentWindow.document.createElement('form');
            form.setAttribute('method', "POST");
            form.setAttribute('action', "/sibbac20back/rest/export.xls");

            var input = contentWindow.document.createElement('input');
            input.name = 'webRequest';
            input.value = JSON.stringify(data);
            form.appendChild(input);
            contentWindow.document.body.appendChild(form);
            form.submit();
        }

        var calcDataTableHeight = function(sY) {
            console.log("calcDataTableHeight: " + $('#tablaGestionReglas').height());
            console.log("sY: " + sY);
            var numeroRegistros = processInfo(oTable.api().page.info());
            var tamanno = 50 * numeroRegistros;
            if ($('#tablaGestionReglas').height() > 510) {
                return 480;
            } else {
                if ($('#tablaGestionReglas').height() > tamanno) {
                    return $('#tablaGestionReglas').height() + 30;
                } else {
                    if (tamanno > 510) {
                        return 480;
                    } else {
                        return tamanno;
                    }
                }
            }
        };

        /* FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA */
        if ($('.contenedorTabla').length >= 1) {
            var anchoBotonera;
            $('.contenedorTabla').each(function(i) {
                anchoBotonera = $(this).find('table').outerWidth();
                $(this).find('.botonera').css('width', anchoBotonera + 'px');
                $(this).find('.resumen').css('width', anchoBotonera + 'px');
                $('.resultados').hide();
            });
        }

        function ajustarAltoTabla() {
            console.log("ajustando inicio");
            var oSettings = oTable.fnSettings();
            console.log("ajustando 1");
            var scrill = calcDataTableHeight(oTable.fnSettings().oScroll.sY);
            console.log("cuanto: " + scrill);
            oSettings.oScroll.sY = scrill;
            console.log("tamanio antes: " + $('.dataTables_scrollBody').height());
            $('.dataTables_scrollBody').height(scrill);
            console.log("tamanio despues: " + $('.dataTables_scrollBody').height());
            console.log("ajustando 2");
            seguimientoBusqueda();

            console.log("ajustando fin");
        }

        // Devuelve información de la tabla-.
        function processInfo(info) {

            pagina = info.page;
            paginasTotales = info.pages;
            var regInicio = info.start;
            var regFin = info.end;
            return regFin - regInicio;

        }

        function infoTabla(info) {
            var regTotales = info.recordsTotal;
            var paginas = info.pages;
            var regPagina = info.length;
            var regInicio = info.start;
            var regFin = info.end;
            return regFin - regInicio;
        }


        // marca/desmarca todos los registros
        function toggleCheckBoxes(b) {

            for (var i = 0; i < $scope.listaChk.length; i++) {
                $scope.listaChk[i] = b;
            }

            for (var i = 0; i < alPaginas.length; i++) {
                if (b) {
                    alPaginas[i] = false;
                } else {
                    alPaginas[i] = true;
                }
            }
            if (b) {
                document.getElementById("MarcarPag").value = "Desmarcar Página";

            } else {
                document.getElementById("MarcarPag").value = "Marcar Página";

            }
        }

        // marca/desmarca los registros de la pagina
        function toggleCheckBoxesPag(b) {
            var inputs = angular.element('input[type=checkbox]');
            var cb = null;
            for (var i = 0; i < inputs.length; i++) {
                cb = inputs[i];

                var clase = $(cb).attr('class');
                var n = clase.indexOf(" ");
                var fila = clase.substr(0, n);

                $scope.listaChk[fila] = b;

            }

        }

        function obtenerIdSeleccionadoText(texto) {
            if (texto.indexOf(" - ") !== -1) {
                return texto.substr(0, texto.indexOf(" - "))
            }
            return ""
        }

        /** Muesta la leyenda con los filtros aplicados a la búsqueda. */
        function seguimientoBusqueda() {
            $scope.followSearch = "";

            if (document.getElementById("codigoDescripcion").value !== "") {
                $scope.followSearch += " Recla(Codigo): " + document.getElementById("codigoDescripcion").value;
            }

            if (document.getElementById("miembroCompensador").value !== "") {
                $scope.followSearch += " Miembro compesador: " + $('#miembroCompensador option:selected').text();
            }

            if (document.getElementById("segmento").value !== "") {
                $scope.followSearch += " Segmento: " + $('#segmento option:selected').text();;
            }

            if (document.getElementById("capacidad").value !== "") {
                $scope.followSearch += " Capacidad: " + $('#capacidad option:selected').text();
            }

            if (document.getElementById("textAlias").value !== "") {
                $scope.followSearch += " Alias: " + document.getElementById("textAlias").value;
            }

            if (document.getElementById("camaraCompensacion").value !== "") {
                $scope.followSearch += " Camara Compensación: " + $('#camaraCompensacion option:selected').text();
            }

            if (document.getElementById("referenciaAsignacion").value !== "") {
                $scope.followSearch += " Refencia Asignación: " + document.getElementById("referenciaAsignacion").value;
            }

            if (document.getElementById("referenciaCliente").value !== "") {
                $scope.followSearch += " Refencia Cliente: " + document.getElementById("referenciaCliente").value;
            }

            if (document.getElementById("modeloNegocio").value !== "") {
                $scope.followSearch += " Modelo Negocio: " + $('#modeloNegocio option:selected').text();
            }

            if (document.getElementById("textSubCta").value !== "") {
                $scope.followSearch += " SubCuenta: " + document.getElementById("textSubCta").value;
            }

            if (document.getElementById("cuentaCompensacion").value !== "") {
                $scope.followSearch += " Cuenta Compensación: " + $('#cuentaCompensacion option:selected').text();
            }

            if (document.getElementById("nemotecnico").value !== "") {
                $scope.followSearch += " Nemotecnico: " + $('#nemotecnico option:selected').text();
            }

            if (document.getElementById("referenciaExterna").value !== "") {
                $scope.followSearch += " Referencia Externa: " + document.getElementById("referenciaExterna").value;
            }

            if ($scope.filter.estado === "0") {
                $scope.followSearch += " Estado : Reglas Inactivas"
            } else if ($scope.filter.estado === "1") {
                $scope.followSearch += " Estado : Reglas Activas"
            } else {
                $scope.followSearch += " Estado : Todas las Reglas"
            }
            $('.mensajeBusqueda').show(750);
        } // seguimientoBusqueda

        function quitaAcentos(text) {

            var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
            var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
            for (var i = 0; i < acentos.length; i++) {
                text = text.replace(acentos.charAt(i), original.charAt(i));
            }
            return text;

        }

        // desplegar el subgrid o subtabla
        $scope.desplegarParam = function(tabla_a_desplegar, id, nTr) {

            $scope.numero_fila = nTr;

            if (!activoListaAlias[$scope.numero_fila] && !activoListaSubCta[$scope.numero_fila]) {
                var nombreDeTablaborrar = "#tablaParam" + $scope.numero_fila + " tbody";
                $(nombreDeTablaborrar).empty();

                $scope.desplegarParameFilas(id, nTr);


                $(oTable.fnGetNodes(nTr)).after(tablasParams[nTr]);
                var tablA = angular.element('#' + tabla_a_desplegar);
                var estadOt = angular.element('#estadoTfila' + nTr);

                console.log(angular.element(tablA).css('display'));
                var visibilidad = getVisibilidad(nTr, tablA);
                if (visibilidad === 'none') {
                    $("#idAlias" + $scope.numero_fila).css('color', 'black');
                    $("#idSubCta" + $scope.numero_fila).css('color', 'black');
                    $("#idAlias" + $scope.numero_fila).css('cursor', 'default');
                    $("#idSubCta" + $scope.numero_fila).css('cursor', 'default');
                    activoListaParam[$scope.numero_fila] = true;
                    angular.element(tablA).show();
                    console.log('foco en tabla ' + tabla_a_desplegar);
                    ajustarAltoTabla();

                    document.getElementById(tabla_a_desplegar).scrollIntoView();

                } else {
                    $("#idAlias" + $scope.numero_fila).css('color', '#ff0000');
                    $("#idSubCta" + $scope.numero_fila).css('color', '#ff0000');
                    $("#idAlias" + $scope.numero_fila).css('cursor', 'pointer');
                    $("#idSubCta" + $scope.numero_fila).css('cursor', 'pointer');
                    activoListaParam[$scope.numero_fila] = false;
                    angular.element(tablA).hide();
                }
                tablasParams[nTr] = tablA;
            }
        }


        $scope.desplegarParameFilas = function(id, nTr) {


            idRegla = id;
            var filtro = {
                "idRegla": id
            };
            GestionReglasService.listaParametrosRegla(onSuccessListaParametros, onErrorRequest, filtro);

        }

        function onSuccessListaParametros(json) {

            console.log("json: " + json);
            console.log("json.resultados: " + json.resultados);
            var item = null;
            var rownum = 0;
            var estilo;
            var estiloParam;
            var contador1 = 0;

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultados_parametrizacion !== undefined) {

                var item = null;
                var datos = json.resultados.resultados_parametrizacion;
                console.log('datos' + datos);

                var contParm = 0;
                var parametrizacion = null;

                var nombreDeTablaborrar = "#tablaParam" + $scope.numero_fila + " tbody";
                $(nombreDeTablaborrar).empty();

                var nombreDeTabla = "#tablaParam" + $scope.numero_fila;
                var rowP = "<tbody><tr>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Id</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Camara Compensaci&oacute;n</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Cta. de Compensaci&oacute;n</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Miembro Compensador</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Referencia de Asignaci&oacute;n</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Nemot&eacute;cnico</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Segmento</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Referencia Cliente</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Referencia Externa</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Capacidad</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Modelo de Negocio</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Fecha Fin</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Modificar</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Eliminar</th>" +
                    "</tr>";


                for (var p in datos) {
                    parametrizacion = datos[p];

                    // se controlan las acciones, se muestra el enlace cuando el usuario
                    // tiene el permiso.
                    var modificarParametrizacion = "";
                    var eliminarParametrizacion = "";
                    if (SecurityService.inicializated) {
                        if (SecurityService.isPermisoAccion($rootScope.SecurityActions.EDITAR_SUBLISTADO, $location.path())) {
                            modificarParametrizacion = "<a class=\"btn\" ng-click = \"modificarParametrizacion('" + parametrizacion.idParametrizacion + "','" + parametrizacion.descripcionParametri + "','" + parametrizacion.idCamaraComp + "','" + parametrizacion.cd_codigoCamaraComp + "','" + parametrizacion.nb_descripcionCamaraComp + "','" + parametrizacion.ctaCompensacionCdCodigo + "'," + parametrizacion.idCuentaCompensacion + ",'" + parametrizacion.miembroCompensador + "','" + parametrizacion.idCompensador + "','" + parametrizacion.referenciaAsignacion + "','" + parametrizacion.nb_nombre + "','" + parametrizacion.idNemotecnico + "','" + parametrizacion.cd_codigoSegmento + "','" + parametrizacion.nb_descripcionSegmento + "','" + parametrizacion.idSegmento + "','" + parametrizacion.referenciaCliente + "','" + parametrizacion.referenciaExterna + "','" + parametrizacion.nb_descripcionIndicCapacidad + "','" + parametrizacion.idCapacidad + "','" + parametrizacion.cd_codigoModeloNegocio + "','" + parametrizacion.nb_descripcionModeloNegocio + "','" + parametrizacion.idModeloNegocio + "','" + transformaFecha(parametrizacion.fecha_fin) + "','" + transformaFecha(parametrizacion.fecha_inicio) + "'); \">" + "<img src='img/editp.png' title='Editar'/></a>";
                        }
                        if (SecurityService.isPermisoAccion($rootScope.SecurityActions.BORRAR_SUBLISTADO, $location.path())) {
                            eliminarParametrizacion = "      <a class=\"btn\" ng-click=eliminarParametrizacion(" + parametrizacion.idParametrizacion + ");><img src='img/del.png'  title='Eliminar'/></a>";
                        }
                    }

                    estiloParam = (contParm % 2 == 0) ? "even" : "odd";

                    rowP += "<tr class='" + estiloParam + "' id='editDiv'>";

                    //ID
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + parametrizacion.idParametrizacion + "</td>";
                    
                    //NOT VISIBLE
                    rowP += "<td class='taleft' style=display:none;' style='background-color: #f5ebc5 !important;'>" + parametrizacion.nb_descripcionParametrizacion + "</td>";

                    //Camara Compensación
                    if (parametrizacion.cd_codigoCamaraComp != null && parametrizacion.nb_descripcionCamaraComp != null) {
                        rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>(" + parametrizacion.cd_codigoCamaraComp + ")" + parametrizacion.nb_descripcionCamaraComp + "</td>";
                    } else {
                        rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";
                    }

                    //Cta. de Compensación                   
                    if(parametrizacion.cd_codigoCuentaCompensacion!=null)
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + parametrizacion.cd_codigoCuentaCompensacion + "</td>";
                    else
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";

                    //Miembro Compensador
                    if ((parametrizacion.nbNombreCompensador != null) && (parametrizacion.nb_descripcionCompensador)) {
                        rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>(" + parametrizacion.nbNombreCompensador + ")" + parametrizacion.nb_descripcionCompensador + "</td>";
                    } else {
                        rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";
                    }
                    
                    //Referencia de Asignación
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + parametrizacion.referenciaAsignacion + "</td>";

                    //Nemotécnico
                    if(parametrizacion.nb_nombreNmotecnico!=null)
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + parametrizacion.nb_nombreNmotecnico + "</td>";
                    else
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";
                    
                    //Segmento
                    if(parametrizacion.cd_codigoSegmento!=null && parametrizacion.nb_descripcionSegmento!=null)
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>(" + parametrizacion.cd_codigoSegmento + ")" + parametrizacion.nb_descripcionSegmento + "</td>";
                    else
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";
                    
                    //Referencia Cliente	
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + parametrizacion.referenciaCliente + "</td>";
                    //Referencia Externa	
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + parametrizacion.referenciaExterna + "</td>";

                    //Capacidad
                    if(parametrizacion.cd_codigoIndicCapacidad!=null && parametrizacion.nb_descripcionIndicCapacidad!=null)
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>(" + parametrizacion.cd_codigoIndicCapacidad + ")" + parametrizacion.nb_descripcionIndicCapacidad + "</td>";
                    else 
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";
                    
                    //Modelo de Negocio	
                    if(parametrizacion.cd_codigoModeloNegocio!=null && parametrizacion.nb_descripcionModeloNegocio!=null) 
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>(" + parametrizacion.cd_codigoModeloNegocio + ")" + parametrizacion.nb_descripcionModeloNegocio + "</td>";
                    else
                    	rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'></td>";
                    
                    //Fecha Fin	
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + transformaFecha(parametrizacion.fecha_fin) + "</td>";

                    //MODIFICAR Y ELIMINAR
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'> " + modificarParametrizacion + "</td> ";
                    rowP += "<td class='taleft' style='background-color: #f5ebc5 !important;'> " + eliminarParametrizacion + "</td>";

                    "</tr>";
                    contParm++;
                }

            }
            $(nombreDeTabla).append("</tbody></table></div>");


            angular.element(nombreDeTabla).append($compile(rowP)($scope));

            ajustarAltoTabla();
        }


        $scope.desplegarAlias = function(tabla_a_desplegar, idRegla, nTr) {
            var filtro = {
                "idRegla": idRegla
            };
            $scope.numero_fila_alias = nTr;

            if (!activoListaParam[$scope.numero_fila_alias] && !activoListaSubCta[$scope.numero_fila_alias]) {
                GestionReglasService.getAliasList(onSuccessAliasList, onErrorRequest, filtro);

                $(oTable.fnGetNodes(nTr)).after(tablasAlias[nTr]);
                var tablA = angular.element('#' + tabla_a_desplegar);
                var estadOt = angular.element('#estadoTfila' + nTr);

                console.log(angular.element(tablA).css('display'));
                var visibilidad = getVisibilidad(nTr, tablA);
                if (visibilidad === 'none') {
                    $("#idParam" + $scope.numero_fila_alias).css('color', 'black');
                    $("#idSubCta" + $scope.numero_fila_alias).css('color', 'black');
                    $("#idParam" + $scope.numero_fila_alias).css('cursor', 'default');
                    $("#idSubCta" + $scope.numero_fila_alias).css('cursor', 'default');
                    activoListaAlias[$scope.numero_fila_alias] = true;
                    angular.element(tablA).show();
                    console.log('foco en tabla ' + tabla_a_desplegar);
                    ajustarAltoTabla();
                    var prueba = document.getElementById('tablaAlias' + nTr)
                    document.getElementById('tablaAlias' + nTr).scrollIntoView();


                } else {
                    $("#idParam" + $scope.numero_fila_alias).css('color', '#ff0000');
                    $("#idSubCta" + $scope.numero_fila_alias).css('color', '#ff0000');
                    $("#idParam" + $scope.numero_fila_alias).css('cursor', 'pointer');
                    $("#idSubCta" + $scope.numero_fila_alias).css('cursor', 'pointer');
                    activoListaAlias[$scope.numero_fila_alias] = false;
                    angular.element(tablA).hide();
                }
                tablasAlias[nTr] = tablA;
            }

        }


        function onSuccessAliasList(json) {
            console.log(json);
            console.log(json.resultados);
            var item = null;
            var rownum = 0;
            var estilo;
            var estiloParam;
            var contador1 = 0;

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultados_lista_alias !== undefined) {


                var item = null;
                var datos = json.resultados.resultados_lista_alias;
                console.log('datos' + datos);

                var contParm = 0;
                var parametrizacion = null;

                var nombreDeTablaborrar = "#tablaAlias" + $scope.numero_fila_alias + " tbody";
                $(nombreDeTablaborrar).empty();

                var nombreDeTabla = "#tablaAlias" + $scope.numero_fila_alias;
                var row = "<tbody><tr>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Alias</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Descripción</th>" +
                    "</tr>";

                for (var p in datos) {
                    var alias = datos[p];
                    var estiloAlias = (contParm % 2 == 0) ? "even" : "odd";

                    row += "<tr class='" + estiloAlias + "' id='editDiv'>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + alias.cdaliass + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + alias.descrali + "</td>" +
                        "</tr>";
                    contParm++;
                }
                angular.element(nombreDeTabla).append($compile(row)($scope));

            }
            $(nombreDeTabla).append("</tbody></table></div>");
            ajustarAltoTabla();

        }

        $scope.desplegarSubCta = function(tabla_a_desplegar, idRegla, nTr) {
            var filtro = {
                "idRegla": idRegla
            };
            $scope.numero_fila_subcta = nTr;


            if (!activoListaParam[$scope.numero_fila_subcta] && !activoListaAlias[$scope.numero_fila_subcta]) {
                GestionReglasService.getSubCtaList(onSuccessSubCtaList, onErrorRequest, filtro);

                $(oTable.fnGetNodes(nTr)).after(tablasSubCta[nTr]);
                var tablA = angular.element('#' + tabla_a_desplegar);
                var estadOt = angular.element('#estadoTfila' + nTr);

                console.log(angular.element(tablA).css('display'));
                var visibilidad = getVisibilidad(nTr, tablA);
                if (visibilidad === 'none') {
                    $("#idParam" + $scope.numero_fila_subcta).css('color', 'black');
                    $("#idAlias" + $scope.numero_fila_subcta).css('color', 'black');
                    $("#idParam" + $scope.numero_fila_subcta).css('cursor', 'default');
                    $("#idAlias" + $scope.numero_fila_subcta).css('cursor', 'default');
                    activoListaSubCta[$scope.numero_fila_subcta] = true;
                    angular.element(tablA).show();
                    console.log('foco en tabla ' + tabla_a_desplegar);
                    ajustarAltoTabla();
                    var prueba = document.getElementById('tablaSubCta' + nTr)
                    document.getElementById('tablaSubCta' + nTr).scrollIntoView();


                } else {
                    $("#idParam" + $scope.numero_fila_subcta).css('color', '#ff0000');
                    $("#idAlias" + $scope.numero_fila_subcta).css('color', '#ff0000');
                    $("#idParam" + $scope.numero_fila_subcta).css('cursor', 'pointer');
                    $("#idAlias" + $scope.numero_fila_subcta).css('cursor', 'pointer');
                    activoListaSubCta[$scope.numero_fila_subcta] = false;
                    angular.element(tablA).hide();
                }
                tablasSubCta[nTr] = tablA;
            }
        }

        function onSuccessSubCtaList(json) {
            console.log(json);
            console.log(json.resultados);
            var item = null;
            var rownum = 0;
            var estilo;
            var estiloParam;
            var contador1 = 0;

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultados_lista_subcta !== undefined) {


                var item = null;
                var datos = json.resultados.resultados_lista_subcta;
                console.log('datos' + datos);

                var contParm = 0;
                var parametrizacion = null;

                var nombreDeTablaborrar = "#tablaSubCta" + $scope.numero_fila_subcta + " tbody";
                $(nombreDeTablaborrar).empty();

                var nombreDeTabla = "#tablaSubCta" + $scope.numero_fila_subcta;
                var row = "<tbody><tr>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Alias</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Subcuenta</th>" +
                    "<th class='taleft' style='background-color: #000 !important;'>Descripción</th>" +
                    "</tr>";

                for (var p in datos) {
                    var subcta = datos[p];
                    var estiloAlias = (contParm % 2 == 0) ? "even" : "odd";

                    row += "<tr class='" + estiloAlias + "' id='editDiv'>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + subcta.cdaliass + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + subcta.cdsubcta + "</td>" +
                        "<td class='taleft' style='background-color: #f5ebc5 !important;'>" + subcta.descrali + "</td>" +
                        "</tr>";
                    contParm++;
                }
                angular.element(nombreDeTabla).append($compile(row)($scope));

            }
            $(nombreDeTabla).append("</tbody></table></div>");
            ajustarAltoTabla();

        }

        $scope.importarRegla = function() {

            $("#cargarFicheroReglas").css('display', 'block');
        }

        function cargarFichero(files) {
            console.log('Entra en enviarFichero....');
            var file = files[0];
            console.log('file ..: ' + file)

            if (files && file) {
                var reader = new FileReader();
                var btoaTxt;
                var nombre = file.name;
                var ruta = file.path;
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    btoaTxt = btoa(binaryString);
                    importarfichero(btoaTxt, nombre);
                };
                reader.readAsBinaryString(file);
            }
            $('#cargarFicheroReglas').css('display', 'none');
            if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
                fErrorTxt('The File APIs are not fully supported in this browser.', 1);
            }

        }


        function importarfichero(Text, nombre) {

            var ficheroName = $('#selecFicheroReglas').val();
            $('#cargarFicheroReglas').css('display', 'none');

            var filtro = {
                file: Text,
                filename: nombre
            };
            GestionReglasService.importarRegla(onSuccessImportarRegla, onErrorRequest, filtro);
        }

        function onSuccessImportarRegla(json) {
            console.log(json);
            console.log(json.resultados);

            if (json.resultados !== null && json.resultados !== undefined && json.resultados.resultado_importarRegla !== undefined) {

                fErrorTxt(json.resultados.resultado_importarRegla, 3);
                cargaDatosGestionReglas();

            } else {

                fErrorTxt(json.error, 2);
            }
            $('#selecFicheroReglas').val('');

        }

    }
]); // GestionReglasController