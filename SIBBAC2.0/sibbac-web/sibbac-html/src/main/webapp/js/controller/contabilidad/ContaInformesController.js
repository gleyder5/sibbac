'use strict';
sibbac20.controller('ContaInformesController',['$scope','$document','growl','ContaInformesService','IsinService','$compile',
function($scope, $document, growl, ContaInformesService,	IsinService, $compile) {

	var oTable = undefined;
	var idOcultosAlias = [];
	var availableTagsAlias = [];
	var cuenta;
	var aliasSelected;

	$(function () {
	    iniContaInformes();
	    aliasSelected = "";
	    function split(val) {
	        return val.split(",\s*");
	    }
	    function extractLast(text) {
	        return split(text).pop();
	    }
	    function log(message) {
	        $("<div style=\"width:100%\">").text(message).prependTo("div#divAlias");
	        $("div#divAlias").scrollTop(0);
	    }

	    $("input#textAlias")
	            // don't navigate away from the field on tab when selecting an item
	            .bind("keydown", function (event) {
	                if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete("instance").menu.active) {
	                    event.preventDefault();
	                }
	            })
	            .autocomplete({
	                source: function (request, response) {
	                    var data = "{\"service\":\"SIBBACServiceTmct0ali\",\"action\":\"getAlias2\",\"filters\":{\"text\":\""
	                            + request.term + "\",\"aliasSelected\":\"" + aliasSelected + "\"}}";
	                    $.ajax({
	                        type: "POST",
	                        dataType: "json",
	                        url: "/sibbac20back/rest/service",
	                        data: data,
	                        beforeSend: function (x) {
	                            if (x && x.overrideMimeType) {
	                                x.overrideMimeType("application/json");
	                            }
	                            // CORS Related
	                            x.setRequestHeader("Accept", "application/json");
	                            x.setRequestHeader("Content-Type", "application/json");
	                            x.setRequestHeader("X-Requested-With", "HandMade");
	                            x.setRequestHeader("Access-Control-Allow-Origin", "*");
	                            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	                        },
	                        async: true,
	                        success: function (json) {
	                            var datos = [];
	                            if (json !== undefined && json.resultados !== undefined) {
	                                var alias = json.resultados.alias;
	                                for (var i in alias) {
	                                    datos.push({
	                                        value: alias[i].cdalias,
	                                        label: alias[i].cdalias + "-" + alias[i].descrali
	                                    });
	                                }
	                            }
	                            response(datos);
	                        },
	                        error: function (c) {
	                            mensajeError.log("ERROR: " + c);
	                        }
	                    });
	                },
	                search: function () {
	                    // custom minLength
	                    var text = extractLast(this.value);
	                    if (text.length < 2) {
	                        return false;
	                    }
	                },
	                focus: function () {
	                    // prevent value inserted on focus
	                    return false;
	                },
	                select: function (event, ui) {
	                    if (ui.item) {
	                        log(ui.item.label);
	                        this.value = "";
	                        aliasSelected += ui.item.value + ",";
	                    }
	                    return false;
	                }
	            });
	});

	function iniContaInformes() {
	    $('#tituloHeader').empty();
	    $('#tituloHeader').append("Informes de Contabilidad");

	    $('a[href="#release-history"]').toggle(function () {
	        $('#release-wrapper').animate({
	            marginTop: '0px'
	        }, 600, 'linear');
	    }, function () {
	        $('#release-wrapper').animate({
	            marginTop: '-' + ($('#release-wrapper').height() + 20) + 'px'
	        }, 600, 'linear');
	    });

	    $('#download a').mousedown(function () {
	        _gaq.push(['_trackEvent', 'download-button', 'clicked'])
	    });

	    $('.collapser').click(function () {
	        $(this).parents('.title_section').next().slideToggle();
	        //$(this).parents('.title_section').next().next('.button_holder').slideToggle();
	        $(this).toggleClass('active');
	        $(this).toggleText('Clic para mostrar', 'Clic para ocultar');

	        $('editarParamPop').bPopup({
	            modalClose: false,
	            opacity: 0.6,
	            positionStyle: 'fixed' //'fixed' or 'absolute'
	        });
	        return false;
	    });
	    $('.collapser_search').click(function () {
	        $(this).parents('.title_section').next().slideToggle();
	        $(this).parents('.title_section').next().next('.button_holder').slideToggle();
	        $(this).toggleClass('active');
	        $(this).toggleText('Mostrar opciones de búsqueda', 'Ocultar opciones de búsqueda');
	        return false;
	    });
	    /*FUNCIÓN PARA AJUSTAR EL ANCHO DEL DIV QUE CONTIENE LOS BOTONES DE LA TABLA*/
	    if ($('.contenedorTabla').length >= 1) {
	        var anchoBotonera;
	        $('.contenedorTabla').each(function (i) {
	            anchoBotonera = $(this).find('table').outerWidth();
	            $(this).find('.botonera').css('width', anchoBotonera + 'px');
	            $(this).find('.resumen').css('width', anchoBotonera + 'px');
	            //  $('.resultados').hide();
	        });
	    }
	    $('#altaParam').click(function () {
	        crearParametrizacion();
	    });
	    oTable = $("#tblContaInformes").dataTable({
	        "dom": 'T<"clear">lfrtip',
	        "tableTools": {
	            "sSwfPath": "/sibbac20/js/swf/copy_csv_xls_pdf.swf"
	        },
	        "language": {
	            "url": "i18n/Spanish.json"
	        },
	        "scrollY": "480px",
	        "scrollCollapse": true,
	        "scrollX": "100%",
	        "aoColumns": [
	            {sClass: "centrar"},
	            {sClass: "centrar"},
	            {sClass: "centrar"},
	            {sClass: "centrar", type: "date-eu"},
	            {sClass: "centrar", type: "date-eu"},
	            {sClass: "centrar", type: "date-eu"},
	            {sClass: "centrar"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "centrar"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "monedaR", type: "formatted-num"},
	            {sClass: "centrar"},
	            {sClass: "centrar"},
	            {sClass: "centrar"},
	            {sClass: "centrar"},
	            {sClass: "centrar"},
	            {sClass: "centrar"}
	        ]

	    });
	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: "/sibbac20back/rest/service",
	        data: "{\"service\":\"SIBBACServiceContaInformes\",\"action\":\"init\"}",
	        beforeSend: function (x) {
	            if (x && x.overrideMimeType) {
	                x.overrideMimeType("application/json");
	            }
	            // CORS Related
	            x.setRequestHeader("Accept", "application/json");
	            x.setRequestHeader("Content-Type", "application/json");
	            x.setRequestHeader("X-Requested-With", "HandMade");
	            x.setRequestHeader("Access-Control-Allow-Origin", "*");
	            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	        },
	        async: true,
	        success: function (json) {
	            var filtro = json.resultados.datosFiltro;
	            var select = $("select#prefijadas");
	            $.each(filtro.prefijadas, function (key, value) {
	                select.append("<option value='" + key + "'>" + value + "</option>");
	            });
	            select = $("select#tipoOrden");
	            $.each(filtro.tipoOrden, function (key, value) {
	                select.append("<option value='" + key + "'>" + value + "</option>");
	            });
	            select = $("select#cuenta");
	            cuenta = {};
	            $.each(filtro.cuentas, function (key, value) {
	                select.append("<option value='" + key + "'>" + key + "</option>");
	                cuenta[key] = value;
	            });
	            select = $("select#tipologia");
	            $.each(filtro.tipologia, function (key, value) {
	                select.append("<option value='" + key + "'>" + value + "</option>");
	            });
	            select = $("select#tipoApunte");
	            $.each(filtro.tipoApunte, function (key, value) {
	                select.append("<option value='" + key + "'>" + value + "</option>");
	            });
	            select = $("select#estadoContable");
	            $.each(filtro.estadoContable, function (key, value) {
	                select.append("<option value='" + key + "'>" + value + "</option>");
	            });
	            select = $("select#estadoLiquidacion");
	            $.each(filtro.estadoLiquidacion, function (key, value) {
	                select.append("<option value='" + key + "'>" + value + "</option>");
	            });
	        },
	        error: function (c) {
	            mensajeError.log("ERROR: " + c);
	        }
	    });
	}

	$("input#buscar").click(function (event) {
	    event.preventDefault();
	    oTable.fnClearTable();

	    $('.collapser_search').parents('.title_section').next().slideToggle();
	    $('.collapser_search').parents('.title_section').next().next('.button_holder').slideToggle();
	    $('.collapser_search').toggleClass('active');
	    if ($('.collapser_search').text().indexOf('Ocultar') !== -1) {
	        $('.collapser_search').text("Mostrar opciones de búsqueda");
	    } else {
	        $('.collapser_search').text("Ocultar opciones de búsqueda");
	    }
	    obtenerDatos();
	    seguimientoBusqueda();
	    return false;
	});

	function obtenerDatos() {
	    var alias = "";
	    var posicionAlias = availableTagsAlias.indexOf($("#textAlias").val());
	    if (posicionAlias != -1) {
	        alias = idOcultosAlias[posicionAlias];
	    }
	    var params = {
	        "service": "SIBBACServiceContaInformes",
	        "action": "filtra",
	        "filters": {
	            "fechTradeIni": $("input#fechTradeIni").val(),
	            "fechTradeFin": $("input#fechTradeFin").val(),
	            "settlementIni": $("input#settlementIni").val(),
	            "settlementFin": $("input#settlementFin").val(),
	            "accountingIni": $("input#accountingIni").val(),
	            "accountingFin": $("input#accountingFin").val(),
	            "prefijadas": $("select#prefijadas").val(),
	            "cuenta": getValuesCuenta(),
	            "alias": aliasSelected,
	            "tipologia": getValues("tipologia"),
	            "tipoOrden": getValues("tipoOrden"),
	            "tipoApunte": getValues("tipoApunte"),
	            "estadoContable": getValues("estadoContable"),
	            "estadoLiquidacion": getValues("estadoLiquidacion")
	        }};
	    oTable.fnClearTable();

	    inicializarLoading();
	    $.ajax({
	        type: 'POST',
	        dataType: "json",
	        contentType: "application/json; charset=utf-8",
	        url: "/sibbac20back/rest/service",
	        data: JSON.stringify(params),
	        beforeSend: function (x) {
	            if (x && x.overrideMimeType) {
	                x.overrideMimeType("application/json");
	            }
	            // CORS Related
	            x.setRequestHeader("Accept", "application/json");
	            x.setRequestHeader("Content-Type", "application/json");
	            x.setRequestHeader("X-Requested-With", "HandMade");
	            x.setRequestHeader("Access-Control-Allow-Origin", "*");
	            x.setRequestHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
	        },
	        success: function (json) {
	            var datos = undefined;
	            var dato;
	            var row;
	            if (json === undefined || json.resultados === undefined) {
	                datos = {};
	            } else {
//					request= json;
	                datos = json.resultados.datosCuenta;
	            }
//				datos = json.resultados.datosCuenta;
	            $.each(datos, function (key, val) {
	                dato = datos[key];
	                row = oTable.fnAddData([
	                    dato.estadoCont,
	                    dato.alias,
	                    dato.descrali,
	                    transformaFecha(dato.fhContratacion),
	                    transformaFecha(dato.fhLiquidacion),
	                    transformaFecha(dato.fhDevengo),
	                    dato.cv,
	                    dato.titulos.formatMoney(0, ',', '.'),
	                    dato.cambio.formatMoney(6, ',', '.'),
	                    dato.efecBruto.formatMoney(2, ',', '.'),
	                    dato.efecNeto.formatMoney(2, ',', '.'),
	                    dato.imcombco.formatMoney(2, ',', '.'),
	                    dato.imfinsvb.formatMoney(2, ',', '.'),
	                    dato.ajusteComision.formatMoney(2, ',', '.'),
	                    dato.imcomisn.formatMoney(2, ',', '.'),
	                    dato.canon.formatMoney(2, ',', '.'),
	                    dato.gastos,
	                    dato.imcomdvo.formatMoney(2, ',', '.'),
	                    dato.imcombrk.formatMoney(2, ',', '.'),
	                    dato.imcombco.formatMoney(2, ',', '.'),
	                    dato.cobrado.formatMoney(2, ',', '.'),
	                    dato.isin,
	                    dato.booking,
	                    dato.orden,
	                    dato.desglose,
	                    dato.desgTitular,
	                    dato.nureford
	                ], false);
	            });
	            oTable.fnDraw();
	            $.unblockUI();
	        },
	        error: function (c) {
	            $.unblockUI();
	            alert("ERROR al ejecutar SIBBACServiceContaInformes#filtra: " + c);
	        }
	    });
	}

	function getValues(select) {
	    var values = "";
	    $("select#" + select).children('option:selected').each(function () {
	        values += $(this).val() + ",";
	    });
	    if (values != "") {
	        values = values.substr(0, values.length - 1);
	    }
	    return values;
	}

	function getValuesCuenta() {
	    var values = "";
	    $("select#cuenta option:selected").each(function () {
	        values += cuenta[$(this).val()] + ",";
	    });
	    if (values != "") {
	        values = values.substr(0, values.length - 1);
	    }
	    return values;
	}

	function getValuesAlias() {
	    var values = "";
	    $("div#divAlias div").each(function () {
	        values += cuenta[$(this).val()] + ",";
	    });
	    if (values != "") {
	        values = values.substr(0, values.length - 1);
	    }
	    return values;
	}

	function seguimientoBusqueda() {
	    $('.mensajeBusqueda').empty();
	    var cadenaFiltros = "";
	    if ($('#prefijadas').val() !== "" && $('#prefijadas').val() !== null) {
	        cadenaFiltros += " prefijadas: " + $('#prefijadas').val();
	    }
	    if ($('#fechTradeIni').val() !== "" && $('#fechTradeIni').val() !== null) {
	        cadenaFiltros += " fechTradeIni: " + $('#fechTradeIni').val();
	    }
	    if ($('#fechTradeFin').val() !== "" && $('#fechTradeFin').val() !== null) {
	        cadenaFiltros += " fechTradeFin: " + $('#fechTradeFin').val();
	    }
	    if ($('#settlementIni').val() !== "" && $('#settlementIni').val() !== null) {
	        cadenaFiltros += " settlementIni: " + $('#settlementIni').val();
	    }
	    if ($('#settlementFin').val() !== "" && $('#settlementFin').val() !== null) {
	        cadenaFiltros += " settlementFin: " + $('#settlementFin').val();
	    }
	    if ($('#tipoOrden').val() !== "" && $('#tipoOrden').val() !== null) {
	        cadenaFiltros += " tipoOrden: " + $('#tipoOrden').val();
	    }
	    if ($('#textAlias').val() !== "" && $('#textAlias').val() !== null) {
	        cadenaFiltros += " alias: " + $('#textAlias').val();
	    }
	    if ($('#tipologia').val() !== "" && $('#tipologia').val() !== null) {
	        cadenaFiltros += " tipologia: " + $('#tipologia').val();
	    }
	    if ($('#cuenta').val() !== "" && $('#cuenta').val() !== null) {
	        cadenaFiltros += " cuenta: " + $('#cuenta').val();
	    }
	    if ($('#tipoApunte').val() !== "" && $('#tipoApunte').val() !== null) {
	        cadenaFiltros += " tipoApunte: " + $('#tipoApunte').val();
	    }
	    if ($('#estadoContable').val() !== "" && $('#estadoContable').val() !== null) {
	        cadenaFiltros += " estadoContable: " + $('#estadoContable').val();
	    }
	    if ($('#estadoLiquidacion').val() !== "" && $('#estadoLiquidacion').val() !== null) {
	        cadenaFiltros += " estadoLiquidacion: " + $('#estadoLiquidacion').val();
	    }
	    $('.mensajeBusqueda').append(cadenaFiltros);
	}

	$("select#prefijadas").change(function () {
	    var value = $(this).children('option:selected').val();
	    clean();
	    if ("ROUT" == value) {
	        var filtro = $("select#tipologia");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='70']").prop('selected', true);
	        filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='1122106']").prop('selected', true);
	        filtro = $("input#textAlias");
	        filtro.val("");
	        filtro = $("select#tipoOrden");
	        filtro.children("option[value='TER']").prop('selected', true);
	        filtro = $("select#estadoContable");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='DV']").prop('selected', true);
	        filtro = $("select#estadoLiquidacion");
	        filtro.children("option:selected").removeAttr("selected");
	    } else if ("CDPF" == value) {
	        var filtro = $("select#tipologia");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='72']").prop('selected', true);
	        filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='1122107']").prop('selected', true);
	        filtro = $("input#textAlias");
	        filtro.val("");
	        filtro = $("select#tipoOrden");
	        filtro.children("option[value='TER']").prop('selected', true);
	        filtro = $("select#estadoContable");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='DV']").prop('selected', true);
	        filtro = $("select#estadoLiquidacion");
	        filtro.children("option:selected").removeAttr("selected");
	    } else if ("CFPC" == value) {
	        var filtro = $("select#tipologia");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='72']").prop('selected', true);
	        filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='1032103']").prop('selected', true);
	        filtro = $("input#textAlias");
	        filtro.val("");
	        filtro = $("select#tipoOrden");
	        filtro.children("option[value='TER']").prop('selected', true);
	        filtro = $("select#estadoContable");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='FC']").prop('selected', true);
	        filtro = $("select#estadoLiquidacion");
	        filtro.children("option:selected").removeAttr("selected");
	    } else if ("CLDP" == value) {
	        var filtro = $("select#tipologia");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='75']").prop('selected', true);
	        filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='1122108']").prop('selected', true);
	        filtro = $("input#textAlias");
	        filtro.val("");
	        filtro = $("select#estadoContable");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='DV']").prop('selected', true);
	        filtro = $("select#estadoLiquidacion");
	        filtro.children("option:selected").removeAttr("selected");
	    } else if ("CLLM" == value) {
	        var filtro = $("select#tipologia");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='75']").prop('selected', true);
	        filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='1021101']").prop('selected', true);
	        filtro.children("option[value='1133104']").prop('selected', true);
	        filtro.children("option[value='2144601']").prop('selected', true);
	        filtro = $("input#textAlias");
	        filtro.val("");
	        filtro = $("select#tipoOrden");
	        filtro.children("option[value='TER']").prop('selected', true);
	        filtro = $("select#estadoContable");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro = $("select#estadoLiquidacion");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='MRC']").prop('selected', true);
	    } else if ("EPLO" == value) {
	        var filtro = $("select#tipologia");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='6021103']").prop('selected', true);
	        filtro.children("option[value='6031103']").prop('selected', true);
	        filtro.children("option[value='8011101']").prop('selected', true);
	        filtro = $("input#textAlias");
	        filtro.val("");
	        filtro = $("select#tipoOrden");
	        filtro.children("option[value='TER']").prop('selected', true);
	        filtro = $("select#estadoContable");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro = $("select#estadoLiquidacion");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='SLQ']").prop('selected', true);
	    } else if ("DCG" == value) {
	        var filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='2015207']").prop('selected', true);
	    } else if ("DCNG" == value) {
	        var filtro = $("select#cuenta");
	        filtro.children("option:selected").removeAttr("selected");
	        filtro.children("option[value='2015208']").prop('selected', true);
	    } else if ("COLV" == value) {
	    } else if ("INTC" == value) {
	    } else if ("INDP" == value) {
	    } else if ("INAP" == value) {
	    } else if ("RCPP" == value) {
	    } else if ("RCPA" == value) {
	    } else if ("AECC" == value) {
	    } else if ("AECA" == value) {
	    }
	}).change();

	function putDatepicker(campo) {
	    var inputIni = $("input#" + campo + "Ini");
	    var inputFin;
	    inputIni.datepicker("option", "maxDate", "")
	    inputIni.datepicker({
	        onClose: function (selectedDate) {
	            inputFin = $("input#" + campo + "Fin");
	            inputFin.datepicker("option", "minDate", selectedDate);
	            if (inputFin.val()!=""){
                var selectedDate_aux = selectedDate.split("/");
                var selectedDateParse = new Date(parseInt(selectedDate_aux[2]),parseInt(selectedDate_aux[1]-1),parseInt(selectedDate_aux[0]));

                var inputFin_aux = inputFin.val().split("/");
                var inputFinDateParse = new Date(parseInt(inputFin_aux[2]),parseInt(inputFin_aux[1]-1),parseInt(inputFin_aux[0]));

                if (inputFinDateParse<selectedDateParse) {
                  inputFin.val(selectedDate);
                }
              }
	        }
	    });
	    inputFin = $("input#" + campo + "Fin");
	    inputFin.datepicker("option", "minDate", "")
	    inputFin.datepicker({
	        onClose: function (selectedDate) {
	            inputIni = $("input#" + campo + "Ini");
	            inputIni.datepicker("option", "maxDate", selectedDate);
	            if (inputIni.val()!=""){
	              var selectedDate_aux = selectedDate.split("/");
	              var selectedDateParse = new Date(parseInt(selectedDate_aux[2]),parseInt(selectedDate_aux[1]-1),parseInt(selectedDate_aux[0]));

	              var inputIni_aux = inputIni.val().split("/");
                var inputIniDateParse = new Date(parseInt(inputIni_aux[2]),parseInt(inputIni_aux[1]-1),parseInt(inputIni_aux[0]));

	              if (inputIniDateParse>selectedDateParse) {
	                inputIni.val(selectedDate);
	              }
	            }
	        }
	    });
	}

	function clean() {
	    $("select.entrada").children("option:selected").removeAttr("selected");
	    $("input.entrada").val("");
	    $("#ui-datepicker-div").remove();
	    putDatepicker("fechTrade");
	    putDatepicker("settlement");
	    putDatepicker("accounting");
	    limpiaAlias();
	}

	$("input#limpiar").click(function (event) {
		//ALEX 08feb optimizar la limpieza de estos valores usando el datepicker
        $( "#fechTradeFin" ).datepicker( "option", "minDate", "");
        $( "#fechTradeIni" ).datepicker( "option", "maxDate", "");
        $( "#settlementFin" ).datepicker( "option", "minDate", "");
        $( "#settlementIni" ).datepicker( "option", "maxDate", "");
        $( "#accountingFin" ).datepicker( "option", "minDate", "");
        $( "#accountingIni" ).datepicker( "option", "maxDate", "");
        //
	    limpiaAlias();
	});

	function limpiaAlias() {
	    $("div#divAlias div").remove();
	    aliasSelected = "";
	}

} ]);
